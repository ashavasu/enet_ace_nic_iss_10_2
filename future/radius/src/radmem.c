/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: radmem.c,v 1.11 2015/04/28 12:26:24 siva Exp $ 
 *
 * Description: This file contains all the modules for
 *              memory creation and memory release for
 *              various purposes.
 *******************************************************************/

#include   "radcom.h"

#ifdef __STDC__
tRADIUS_REQ_ENTRY  *
radiusCreateUserEntry (INT4 i4_UserPoolId)
#else
tRADIUS_REQ_ENTRY  *
radiusCreateUserEntry (i4_UserPoolId)
     INT4                i4_UserPoolId;
#endif
{
    tRADIUS_REQ_ENTRY  *p_BufPtr;

    if ((p_BufPtr =
         (tRADIUS_REQ_ENTRY *) MemAllocMemBlk ((UINT4) i4_UserPoolId)) == NULL)
    {
        RAD_ERR_PRINT ("\nInsufficient memory.\n");
        return NULL;
    }
    MEMSET (p_BufPtr, 0, sizeof (tRADIUS_REQ_ENTRY));
    return (p_BufPtr);

}

#ifdef __STDC__
UINT1              *
radiusCreatePacket (INT4 i4_PacketPoolId)
#else
UINT1              *
radiusCreatePacket (i4_PacketPoolId)
     INT4                i4_PacketPoolId;
#endif
{
    UINT1              *p_BufPtr;

    if ((p_BufPtr = (UINT1 *) MemAllocMemBlk (i4_PacketPoolId)) == NULL)
    {
        RAD_ERR_PRINT ("\nInsufficient memory.\n");
        return NULL;
    }

    return (p_BufPtr);
}

#ifdef __STDC__
INT4
radiusReleaseUserEntry (INT4 i4_UserPoolId, UINT1 *p_BufPtr)
#else
INT4
radiusReleaseUserEntry (i4_UserPoolId, p_BufPtr)
     INT4                i4_UserPoolId;
     UINT1              *p_BufPtr;
#endif
{
    UINT4               i4_ReturnValue;

    /* Release - radius user queue entry */
    i4_ReturnValue = MemReleaseMemBlock (i4_UserPoolId, p_BufPtr);

    if (i4_ReturnValue == MEM_FAILURE)
        radiusInformFault (FAULT_MEM_RELEASE);

    return (i4_ReturnValue);
}

#ifdef __STDC__
INT4
radiusReleasePacket (INT4 i4_PacketPoolId, UINT1 *p_BufPtr)
#else
INT4
radiusReleasePacket (i4_PacketPoolId, p_BufPtr)
     INT4                i4_PacketPoolId;
     UINT1              *p_BufPtr;
#endif
{
    UINT4               i4_ReturnValue;

    i4_ReturnValue = MemReleaseMemBlock (i4_PacketPoolId, p_BufPtr);
    if (i4_ReturnValue == MEM_FAILURE)
        radiusInformFault (FAULT_MEM_RELEASE);

    return (i4_ReturnValue);
}

#ifdef __STDC__
tRADIUS_SERVER     *
radiusCreateRadiusServer (INT4 i4_ServerPoolId)
#else
tRADIUS_SERVER     *
radiusCreateRadiusServer (i4_ServerPoolId)
     INT4                i4_ServerPoolId;
#endif
{
    tRADIUS_SERVER     *p_BufPtr;

    if ((p_BufPtr = (tRADIUS_SERVER *)
         MemAllocMemBlk ((UINT4) i4_ServerPoolId)) == NULL)
    {
        RAD_ERR_PRINT ("\nInsufficient memory.\n");
        return NULL;
    }

    return (p_BufPtr);

}

#ifdef __STDC__
INT4
radiusReleaseRadiusServer (INT4 i4_ServerPoolId, UINT1 *p_BufPtr)
#else
INT4
radiusReleaseRadiusServer (i4_ServerPoolId, p_BufPtr)
     INT4                i4_ServerPoolId;
     UINT1              *p_BufPtr;
#endif
{
    UINT4               i4_ReturnValue;

    i4_ReturnValue = MemReleaseMemBlock (i4_ServerPoolId, p_BufPtr);
    if (i4_ReturnValue == MEM_FAILURE)
        radiusInformFault (FAULT_MEM_RELEASE);

    return (i4_ReturnValue);
}

/**************************** END OF FILE "radmem.c" ************************/
