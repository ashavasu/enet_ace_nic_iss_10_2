/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: radtxmt.c,v 1.20 2018/02/14 10:04:08 siva Exp $ 
 *
 * Description: This file contains the modules for Packet 
 *              transmission and timeout handling.
 *
 *******************************************************************/

#include   "radcom.h"
#include "ip6util.h"
/*----------------------------------------------------------------------------
Procedure    : radiusTransmitPacket()
Description    : This module transmits a Radius packet to a Radius server.
Input(s)    : p_u1RadiusPacketAuth or p_u1RadiusPacketAcc,
              i4_AuthSocketId,
              i4_AccSocketId,
              p_RadiusServer
Output        : None
Returns        : The Identifier of the packet transmitted, if the packet is
              transmitted successfully
              -1, if failure occured in transmitting packet
Called By    : radiusAuthentication()
              radiusAccounting()
              radiusTimeoutHandler()
Calling        : None
Author        : G.Vedavinayagam
Date        : 11/14/97
odifications    :
-----------------------------------------------------------------------------*/
#ifdef __STDC__
INT4
radiusTransmitPacket (UINT1 a_u1RadiusPacket[],
                      INT4 i4_AuthSocketId,
                      INT4 i4_AccSocketId, tRADIUS_SERVER * p_RadiusServer)
#else
INT4
radiusTransmitPacket (a_u1RadiusPacket, i4_AuthSocketId, i4_AccSocketId,
                      p_RadiusServer)
     UINT1               a_u1RadiusPacket[];
     INT4                i4_AuthSocketId;
     INT4                i4_AccSocketId;
     tRADIUS_SERVER     *p_RadiusServer;
#endif
{
    INT4                i4_NoOfWrittenCh = 0;
    UINT2               u2_PktLen;
    UINT2               i;
    UINT4               u4_ServerIPAddress = 0;
#ifdef IP6_WANTED
    UINT4               u4IfIndex = 0;
#endif
    struct sockaddr_in  RadiusServerAddrAuth, RadiusServerAddrAcc;
    struct sockaddr_in6 RadiusServerAddrAuth6, RadiusServerAddrAcc6;

    MEMCPY (&u2_PktLen, a_u1RadiusPacket + PKT_LEN, 2);
    u2_PktLen = OSIX_NTOHS (u2_PktLen);
    if (*(a_u1RadiusPacket + PKT_TYPE) == ACCESS_REQUEST)
    {
        p_RadiusServer->u4_PendingRequests++;

        if (p_RadiusServer->u1_ServerEnabled != RADIUS_ENABLED)
        {
            p_gRadiusErrorLog->u4_ErrorServerDisabledAuth++;
            RAD_ERR_PRINT
                ("Trying to send Access Request when server not enabled");
            return (RAD_SERVER_DISABLED);
        }

        if (p_RadiusServer->ServerAddress.u1Afi == IPVX_ADDR_FMLY_IPV4)
        {

            MEMSET ((UINT1 *) &RadiusServerAddrAuth, 0,
                    sizeof (RadiusServerAddrAuth));

            RadiusServerAddrAuth.sin_family = AF_INET;
            RadiusServerAddrAuth.sin_port =
                OSIX_HTONS (p_RadiusServer->u4_DestinationPortAuth);
            PTR_FETCH4 (u4_ServerIPAddress,
                        p_RadiusServer->ServerAddress.au1Addr);
            RadiusServerAddrAuth.sin_addr.s_addr =
                OSIX_HTONL (u4_ServerIPAddress);

            RAD_EVENT_PRINT2 ("\nSending to Server Address: %x, Port No = %d",
                              OSIX_NTOHL (RadiusServerAddrAuth.sin_addr.s_addr),
                              OSIX_NTOHS (RadiusServerAddrAuth.sin_port));

            /*This check is done in order to ensure that ip address for the
               server and the interface is not same */
            if (NetIpv4IfIsOurAddress
                (OSIX_NTOHL (RadiusServerAddrAuth.sin_addr.s_addr)) ==
                NETIPV4_SUCCESS)

            {
                RAD_ERR_PRINT
                    ("The ip address is same as the interface ip address ");
                return (NOT_OK);
            }

            i4_NoOfWrittenCh = sendto ((INT4) i4_AuthSocketId,
                                       (UINT1 *) a_u1RadiusPacket,
                                       (INT4) u2_PktLen, (INT4) 0,
                                       (struct sockaddr *)
                                       &RadiusServerAddrAuth,
                                       (INT4) sizeof (RadiusServerAddrAuth));
        }
        else if (p_RadiusServer->ServerAddress.u1Afi == IPVX_ADDR_FMLY_IPV6)
        {
            MEMSET ((UINT1 *) &RadiusServerAddrAuth6, 0,
                    sizeof (RadiusServerAddrAuth6));

            RadiusServerAddrAuth6.sin6_family = AF_INET6;
            RadiusServerAddrAuth6.sin6_port =
                OSIX_HTONS (p_RadiusServer->u4_DestinationPortAuth);
            MEMCPY (RadiusServerAddrAuth6.sin6_addr.s6_addr,
                    &(p_RadiusServer->ServerAddress.au1Addr),
                    IPVX_IPV6_ADDR_LEN);

            RAD_EVENT_PRINT2 ("\nSending to Server Address: %s, Port No = %d",
                              Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                            RadiusServerAddrAuth6.
                                            sin6_addr.s6_addr),
                              OSIX_NTOHS (RadiusServerAddrAuth6.sin6_port));

            /*Checks whether IP belongs to Local Interface .
               If RADIUS PROXY is to be activated in the future this should be taken into account */
#ifdef IP6_WANTED
            if (NetIpv6IsOurAddress
                ((tIp6Addr *) (VOID *) RadiusServerAddrAuth6.sin6_addr.s6_addr,
                 &u4IfIndex) == NETIPV6_SUCCESS)
            {
                RAD_ERR_PRINT
                    ("The ip address is same as the interface ip address ");

                return (NOT_OK);
            }
#endif

            i4_NoOfWrittenCh = sendto ((INT4) i4_AuthSocketId,
                                       (UINT1 *) a_u1RadiusPacket,
                                       (INT4) u2_PktLen, (INT4) 0,
                                       (struct sockaddr *)
                                       &RadiusServerAddrAuth6,
                                       (INT4) sizeof (RadiusServerAddrAuth6));
        }

        if (i4_NoOfWrittenCh < 0)
        {
            p_gRadiusErrorLog->u4_ErrorTransmissionAuth++;
            RAD_ERR_PRINT ("\nSENDTO failure\n");
            return (NOT_OK);
        }

        RAD_EVENT_PRINT ("\nPacket Transmitted (AUTH):\n");
        RAD_PACKET_PRINT ("\n-------------------------- \n");

        for (i = 0; i < u2_PktLen; i++)
        {
            RAD_PACKET_PRINT1 ("%02x ", *(a_u1RadiusPacket + i));
        }

        RAD_PACKET_PRINT ("\n-------------------------- \n");

    }                            /* if ACCESS_REQUEST */
    else
    {

        if (p_RadiusServer->u1_ServerEnabled != RADIUS_ENABLED)
        {
            p_gRadiusErrorLog->u4_ErrorServerDisabledAcc++;
            return (RAD_SERVER_DISABLED);
        }

        if (p_RadiusServer->ServerAddress.u1Afi == IPVX_ADDR_FMLY_IPV4)
        {

            MEMSET ((UINT1 *) &RadiusServerAddrAcc, 0,
                    sizeof (RadiusServerAddrAcc));

            RadiusServerAddrAcc.sin_family = AF_INET;
            RadiusServerAddrAcc.sin_port =
                OSIX_HTONS (p_RadiusServer->u4_DestinationPortAcc);

            PTR_FETCH4 (u4_ServerIPAddress,
                        p_RadiusServer->ServerAddress.au1Addr);
            RadiusServerAddrAcc.sin_addr.s_addr =
                OSIX_HTONL (u4_ServerIPAddress);

            i4_NoOfWrittenCh = sendto ((INT4) i4_AccSocketId,
                                       (UINT1 *) a_u1RadiusPacket,
                                       (INT4) u2_PktLen, (INT4) 0,
                                       (struct sockaddr *) &RadiusServerAddrAcc,
                                       (INT4) sizeof (RadiusServerAddrAcc));
        }
        else
        {
            MEMSET ((UINT1 *) &RadiusServerAddrAcc6, 0,
                    sizeof (RadiusServerAddrAcc6));

            RadiusServerAddrAcc6.sin6_family = AF_INET6;
            RadiusServerAddrAcc6.sin6_port =
                OSIX_HTONS (p_RadiusServer->u4_DestinationPortAcc);
            MEMCPY (RadiusServerAddrAcc6.sin6_addr.s6_addr,
                    &(p_RadiusServer->ServerAddress), IPVX_IPV6_ADDR_LEN);

            RAD_EVENT_PRINT2 ("\nSending to Server Address: %s, Port No = %d",
                              Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                            RadiusServerAddrAcc6.
                                            sin6_addr.s6_addr),
                              OSIX_NTOHS (RadiusServerAddrAcc6.sin6_port));

            i4_NoOfWrittenCh = sendto ((INT4) i4_AccSocketId,
                                       (UINT1 *) a_u1RadiusPacket,
                                       (INT4) u2_PktLen, (INT4) 0,
                                       (struct sockaddr *)
                                       &RadiusServerAddrAcc6,
                                       (INT4) sizeof (RadiusServerAddrAcc6));
        }

        if (i4_NoOfWrittenCh < 0)
        {
            p_gRadiusErrorLog->u4_ErrorTransmissionAcc++;
            return (NOT_OK);
        }

        RAD_EVENT_PRINT ("\nPacket Transmitted (ACCT):\n");

        for (i = 0; i < u2_PktLen; i++)
        {
            RAD_PACKET_PRINT1 ("%02x ", *(a_u1RadiusPacket + i));
        }

    }                            /* if ACCOUNTING_REQUEST */

    return OK;
}

/*-----------------------------------------------------------------------------
Procedure    : radiusTerminateAuthentication()
Description    : This module teraminates an authentication process. This 
              module removes the user from the queue and stops the timer
              for that authentication process.
Input(s)    : Identifier of the Packet transmitted
Output(s)    : None
Return(s)    : 0, if successful
              -1, if failed
Called By    : The User
Calling        : radiusReleasePacket()
              radiusReleaseUserEntry()
              radiusStopTimer()
Author        : G.Vedavinayagam
Date        : 12/4/97
odifications    :
-----------------------------------------------------------------------------*/
#ifdef __STDC__
INT1
radiusTerminateAuthentication (UINT1 u1_Identifier)
#else
INT1
radiusTerminateAuthentication (u1_Identifier)
     UINT1               u1_Identifier;
#endif
{
    tRADIUS_REQ_ENTRY  *p_UserQEntry;
    INT4                i4_RetVal;
    tTmrAppTimer       *pAppTimer;

    /* Take the reference timer ptr */
    pAppTimer = a_gUserTable[u1_Identifier];

    if (pAppTimer == NULL)
        return (NOT_OK);

    p_UserQEntry = (tRADIUS_REQ_ENTRY *) pAppTimer->u4Data;

    if (p_UserQEntry == NULL)
        return (NOT_OK);        /* if the user enty's pointer 
                                   is NULL return error */

    /* Release the memory alloted for packet transmitted */

    i4_RetVal = radiusReleasePacket (gRadMemPoolId.u4PacketPoolId,
                                     p_UserQEntry->p_u1PacketTransmitted);

    /* Release the memory allocated for authentication 
     * protocol structure and pServices structure in 
     * auth info of user queue entry (p_UserQEntry->userInfoAuth) */
    RadiusReleaseUserInfoAuth (&(p_UserQEntry->userInfoAuth));

    /* Release the memory alloted for the user entry in the
       user queue */
    i4_RetVal = radiusReleaseUserEntry (gRadMemPoolId.u4UserEntryPoolId,
                                        (UINT1 *) p_UserQEntry);

    UNUSED_PARAM (i4_RetVal);

    if (rad_tmrh_stop_timer (pAppTimer) == RAD_FAILURE)
        p_gRadiusErrorLog->u4_ErrorTimerNotStoped++;

    MemReleaseMemBlock (gRadMemPoolId.u4RadTimerPoolId, (UINT1 *) pAppTimer);

    pAppTimer = NULL;            /* intialise the table */
    a_gUserTable[u1_Identifier] = NULL;
    return (OK);
}

/*----------------------------------------------------------------------------
Procedure    : radiusTimeoutHandler()
Description    : This module takes action for a time out occurred for a 
              transmitted packet. It retransmits the packet to the same
              server for a no of times mentioned in the respective server's
              information structure. 
Input(s)    : TimerListId for which the timeout has occurred
Output(s)    : None
Returns        : None
Called By    : Kernel Timer handler
Author        : Selvaraj. B
Date        : 06/12/99
odifications    :
-----------------------------------------------------------------------------*/

VOID
radiusTimeoutHandler (tTimerListId p_RadTimerListId)
{
    UINT1               u1_ID;
    UINT2               u2_RadiusPacketLength = LEN_PKT_MIN;
    INT4                i4_ID = 0;
    INT4                i4_RetVal;
    UINT1              *pa_u1RadiusPacketAuth = NULL;
    UINT4               u4ServersContacted = 0;
    UINT4               u4NoOfServers = 0;
    tRADIUS_SERVER     *p_RadiusServer;
    tRADIUS_REQ_ENTRY  *p_UserQEntry;
    tTmrAppTimer       *p_AppTimer;
    tRadInterface      *pInterface;

    UNUSED_PARAM (i4_RetVal);

    pa_u1RadiusPacketAuth = MemAllocMemBlk (gRadMemPoolId.u4RadPacketPoolId);

    if (NULL == pa_u1RadiusPacketAuth)
    {
        RAD_ERR_PRINT
            ("Error in allocating memory for pa_u1RadiusPacketAuth\n");
        UNUSED_PARAM (pInterface);
        return;
    }
    MEMSET (pa_u1RadiusPacketAuth, 0, sizeof (tu1RadiusPacketAuth));

    /* Get the expired timers from the timer list */
    while ((p_AppTimer = TmrGetNextExpiredTimer (p_RadTimerListId)) != NULL)
    {
        p_UserQEntry = (tRADIUS_REQ_ENTRY *) p_AppTimer->u4Data;

        if (p_UserQEntry == NULL)
        {
            MemReleaseMemBlock (gRadMemPoolId.u4RadPacketPoolId,
                                (UINT1 *) pa_u1RadiusPacketAuth);
            UNUSED_PARAM (pInterface);
            return;
        }

        if (p_UserQEntry->p_u1PacketTransmitted == NULL)
        {
            MemReleaseMemBlock (gRadMemPoolId.u4RadPacketPoolId,
                                (UINT1 *) pa_u1RadiusPacketAuth);
            UNUSED_PARAM (pInterface);
            return;
        }

        if (p_UserQEntry->ptRadiusServer == NULL)
        {
            MemReleaseMemBlock (gRadMemPoolId.u4RadPacketPoolId,
                                (UINT1 *) pa_u1RadiusPacketAuth);
            UNUSED_PARAM (pInterface);
            return;
        }

        RAD_TIMER_PRINT ("\nRetransmit TIMER Expired...\n");
        p_RadiusServer = p_UserQEntry->ptRadiusServer;

        p_UserQEntry->u1_ReTxCount++;

        if (*(p_UserQEntry->p_u1PacketTransmitted + PKT_TYPE) == ACCESS_REQUEST)
        {

            p_RadiusServer->u4_TimeOuts++;

            if (p_UserQEntry->u1_ReTxCount >
                p_RadiusServer->u1_MaxRetransmissions)
            {
                /* Get Next Reachable Radius Server in  External Server Table */
                p_RadiusServer =
                    RadGetNextServerEntry (p_RadiusServer->ifIndex);

                /* Get the number of Radius servers contacted */
                u4ServersContacted = p_UserQEntry->u4NumServersContacted;

                /* Get the number of Radius Server present in the External 
                 * Radius server table */
                u4NoOfServers = RadGetNumAuthServers ();

                /* Retransmit the request until the number of Radius servers 
                 * contacted is equal to the number of Radius servers configured
                 * or the pointer to the Radius  server is NULL  */
                for (u4ServersContacted++;
                     (p_RadiusServer != NULL &&
                      u4ServersContacted <= u4NoOfServers);
                     p_RadiusServer =
                     RadGetNextServerEntry (p_RadiusServer->ifIndex),
                     u4ServersContacted++)
                {

                    RAD_TIMER_PRINT1
                        ("\nTransmission Over for Server %d, Sending"
                         "the Request to next server.\n",
                         (p_RadiusServer->ifIndex - 1));

                    /* Copy the first 20 bytes (i.e) 
                     * code(1), id(1), len(2) and request authenticator(16) */

                    MEMCPY (pa_u1RadiusPacketAuth,
                            p_UserQEntry->p_u1PacketTransmitted,
                            u2_RadiusPacketLength);

                    /* Form Radius Auth Pkt and Retransmit the req.to
                     * next Radius Server. Use the same memory for app
                     * timer data. AppTimer should be freed only after
                     * all retransmissions are over to all radius servers
                     */
                    i4_ID = RadiusTransmitAuth (&(p_UserQEntry->userInfoAuth),
                                                pa_u1RadiusPacketAuth,
                                                p_RadiusServer,
                                                p_UserQEntry->CallBackfPtr,
                                                p_UserQEntry->ifIndex,
                                                u4ServersContacted);
                    if (i4_ID == NOT_OK)
                    {
                        p_gRadiusErrorLog->u4_ErrorTransmissionAuth++;

                        RAD_ERR_PRINT (" Error in transmitting the packet.\n");
                        /* Radius server cannot be reached. 
                         * Try next radius server */
                    }
                    else
                    {
                        /* Data has been sent successfully.
                         * Wait for the response from radius server. 
                         */
                        break;
                    }
                }

                if ((p_RadiusServer == NULL)
                    || (u4ServersContacted > u4NoOfServers))
                {
                    RAD_TIMER_PRINT
                        ("\nRetransmission Over ...\n\tDropping the Request.\n");

                    if (p_UserQEntry->CallBackfPtr != NULL)
                    {
                        if ((pInterface =
                             (tRadInterface *) (VOID *)
                             UtlShMemAllocRadInterface ()) == NULL)
                        {
                            MemReleaseMemBlock (gRadMemPoolId.u4RadPacketPoolId,
                                                (UINT1 *)
                                                pa_u1RadiusPacketAuth);
                            UNUSED_PARAM (pInterface);
                            return;
                        }
                        MEMSET (pInterface, 0, sizeof (tRadInterface));
                        pInterface->ifIndex = p_UserQEntry->ifIndex;
                        pInterface->Access = RAD_TIMEOUT;
                        pInterface->RequestId =
                            p_UserQEntry->u1_PacketIdentifier;
                        pInterface->TaskId = p_UserQEntry->userInfoAuth.TaskId;
#ifdef WLC_WANTED
                        pInterface->u1EAPType =
                            p_UserQEntry->userInfoAuth.u1_ProtocolType;
                        MEMCPY (pInterface->au1StaMac, p_UserQEntry->au1StaMac,
                                MAC_ADDR_LEN);
#endif
                        RAD_EVENT_PRINT
                            ("\n Passing the Information to USER Modules.\n");
                        (*(p_UserQEntry->CallBackfPtr)) ((VOID *) pInterface);

                    }
                    /* Release Timer reference memory */
                    u1_ID = p_UserQEntry->u1_PacketIdentifier;
                    MemReleaseMemBlock (gRadMemPoolId.u4RadTimerPoolId,
                                        (UINT1 *) a_gUserTable[u1_ID]);
                    a_gUserTable[u1_ID] = NULL;
                }

                /* Release memory allocated earlier */
                i4_RetVal = radiusReleasePacket (gRadMemPoolId.u4PacketPoolId,
                                                 p_UserQEntry->
                                                 p_u1PacketTransmitted);

                /* Release the memory allocated for userInfoAuth */
                RadiusReleaseUserInfoAuth (&(p_UserQEntry->userInfoAuth));

                /* Release the memory allocated for user Auth Q entry */
                i4_RetVal =
                    radiusReleaseUserEntry (gRadMemPoolId.u4UserEntryPoolId,
                                            (UINT1 *) p_UserQEntry);
                MemReleaseMemBlock (gRadMemPoolId.u4RadPacketPoolId,
                                    (UINT1 *) pa_u1RadiusPacketAuth);
                UNUSED_PARAM (pInterface);
                return;

            }                    /* if MaxRetransmissions */
            else
            {
                RAD_TIMER_PRINT2
                    ("\nRetransmitting Access Request.\n\tCOUNT:[%d]\n Id: %d",
                     p_UserQEntry->u1_ReTxCount,
                     p_UserQEntry->u1_PacketIdentifier);
                if (p_RadiusServer->ServerAddress.u1Afi == IPVX_ADDR_FMLY_IPV4)
                {
                    i4_ID =
                        radiusTransmitPacket (p_UserQEntry->
                                              p_u1PacketTransmitted,
                                              i4_gRadiusAuthSocketId,
                                              i4_gRadiusAccSocketId,
                                              p_RadiusServer);
                }

#ifdef IP6_WANTED
                else if (p_RadiusServer->ServerAddress.u1Afi ==
                         IPVX_ADDR_FMLY_IPV6)
                {
                    i4_ID =
                        radiusTransmitPacket (p_UserQEntry->
                                              p_u1PacketTransmitted,
                                              i4_gRadiusAuthSocketId6,
                                              i4_gRadiusAccSocketId6,
                                              p_RadiusServer);
                }
#endif
                if ((i4_ID == OK) || (i4_ID == RAD_SERVER_DISABLED))
                {
                    MEMSET (p_AppTimer, 0, sizeof (tTmrAppTimer));
                    p_AppTimer->u4Data = (FS_ULONG) p_UserQEntry;

                    if (rad_tmrh_start_timer (p_AppTimer,
                                              p_RadiusServer->
                                              u4_ResponseTime) == RAD_FAILURE)
                        p_gRadiusErrorLog->u4_ErrorTimerNotStartedAuth++;

                    p_RadiusServer->u4_AccessReTxs++;

                }
                else if (i4_ID == NOT_OK)
                {
                    /* if the reason for packet transmission failure
                     * is NOT_OK(sendto fail) then,
                     * release the memory allocated for packet, userInfoAuth,
                     * userQentry and timer */

                    if (p_UserQEntry->CallBackfPtr != NULL)
                    {
                        if ((pInterface =
                             (tRadInterface *) (VOID *)
                             UtlShMemAllocRadInterface ()) == NULL)
                        {
                            MemReleaseMemBlock (gRadMemPoolId.u4RadPacketPoolId,
                                                (UINT1 *)
                                                pa_u1RadiusPacketAuth);
                            return;
                        }
                        MEMSET (pInterface, 0, sizeof (tRadInterface));
                        pInterface->ifIndex = p_UserQEntry->ifIndex;
                        pInterface->Access = RAD_TIMEOUT;
                        pInterface->RequestId =
                            p_UserQEntry->u1_PacketIdentifier;
                        pInterface->TaskId = p_UserQEntry->userInfoAuth.TaskId;
                        RAD_EVENT_PRINT
                            ("\n Passing the Information to USER Modules.\n");
                        (*(p_UserQEntry->CallBackfPtr)) ((VOID *) pInterface);

                    }
                    /* Release memory allocated earlier */
                    i4_RetVal =
                        radiusReleasePacket (gRadMemPoolId.u4PacketPoolId,
                                             p_UserQEntry->
                                             p_u1PacketTransmitted);

                    /* Release the memory allocated for userInfoAuth */
                    RadiusReleaseUserInfoAuth (&(p_UserQEntry->userInfoAuth));

                    u1_ID = p_UserQEntry->u1_PacketIdentifier;
                    /* Release the memory allocated for user Auth Q entry */
                    i4_RetVal =
                        radiusReleaseUserEntry (gRadMemPoolId.u4UserEntryPoolId,
                                                (UINT1 *) p_UserQEntry);
                    /* Release Timer reference memory */
                    MemReleaseMemBlock (gRadMemPoolId.u4RadTimerPoolId,
                                        (UINT1 *) a_gUserTable[u1_ID]);
                    a_gUserTable[u1_ID] = NULL;
                }
            }                    /* if MaxRetransmissions */
        }
        else                    /* Accounting retransmissoin */
        {
            p_RadiusServer->u4_TimeOuts++;
            if (p_UserQEntry->u1_ReTxCount >
                p_RadiusServer->u1_MaxRetransmissions)
            {
                RAD_TIMER_PRINT
                    ("\nRetransmission Over...\n\tDropping the Request.\n");
                if (p_UserQEntry->CallBackfPtr != NULL)
                {
                    pInterface = RadFillInterfaceStructure (NULL, p_UserQEntry);
                }

                i4_RetVal = radiusReleasePacket (gRadMemPoolId.u4PacketPoolId,
                                                 p_UserQEntry->
                                                 p_u1PacketTransmitted);

                i4_RetVal =
                    radiusReleaseUserEntry (gRadMemPoolId.u4UserEntryPoolId,
                                            (UINT1 *) p_UserQEntry);

                /* Release Timer reference memory */
                u1_ID = p_UserQEntry->u1_PacketIdentifier;
                MemReleaseMemBlock (gRadMemPoolId.u4RadTimerPoolId,
                                    (UINT1 *) a_gUserTable[u1_ID]);
                a_gUserTable[u1_ID] = NULL;
                MemReleaseMemBlock (gRadMemPoolId.u4RadPacketPoolId,
                                    (UINT1 *) pa_u1RadiusPacketAuth);
                UNUSED_PARAM (pInterface);
                return;
            }
            else
            {
                RAD_TIMER_PRINT1
                    ("\nRetransmitting Accounting Request.\n\tCOUNT:[%d]\n",
                     p_UserQEntry->u1_ReTxCount);
                i4_ID =
                    radiusTransmitPacket (p_UserQEntry->p_u1PacketTransmitted,
                                          i4_gRadiusAuthSocketId,
                                          i4_gRadiusAccSocketId,
                                          p_RadiusServer);
                if (!i4_ID)
                {
                    MEMSET (p_AppTimer, 0, sizeof (tTmrAppTimer));
                    p_AppTimer->u4Data = (FS_ULONG) p_UserQEntry;

                    if (rad_tmrh_start_timer (p_AppTimer,
                                              p_RadiusServer->
                                              u4_ResponseTime) == RAD_FAILURE)
                        p_gRadiusErrorLog->u4_ErrorTimerNotStartedAuth++;

                    p_RadiusServer->u4_AcctReTxs++;
                }                /* if u1_ID */
            }                    /* if MaxRetransmissions */
        }                        /* if ACCT_REQUEST */
    }                            /* While loop */
    UNUSED_PARAM (pInterface);
    MemReleaseMemBlock (gRadMemPoolId.u4RadPacketPoolId,
                        (UINT1 *) pa_u1RadiusPacketAuth);

}

/*******************************************/

/*************************** END OF FILE "radtxmt.c" *************************/
