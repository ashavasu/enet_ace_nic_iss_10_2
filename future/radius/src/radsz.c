/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: radsz.c,v 1.1 2015/04/28 12:26:24 siva Exp $
 *
 * Description: This files for RADIUS.
 *******************************************************************/

#define _RADSZ_C
#include "radcom.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
RadSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < RAD_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal = MemCreateMemPool (FsRADSizingParams[i4SizingId].u4StructSize,
                                     FsRADSizingParams[i4SizingId].
                                     u4PreAllocatedUnits,
                                     MEM_DEFAULT_MEMORY_TYPE,
                                     &(RADMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            RadSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
RadSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsRADSizingParams);
    IssSzRegisterModulePoolId (pu1ModName, RADMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
RadSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < RAD_MAX_SIZING_ID; i4SizingId++)
    {
        if (RADMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (RADMemPoolIds[i4SizingId]);
            RADMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
