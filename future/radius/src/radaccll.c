/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: radaccll.c,v 1.10 2015/04/28 12:26:23 siva Exp $ 
 *
 * Description: This file contains the low level routines.
 *
 *******************************************************************/

# include "snmctdfs.h"
# include "snmccons.h"

# include "radsnmp.h"
# include "radcom.h"
# include "fsradecli.h"

/*******GLOBAL for Dummy index *******/
UINT4               u4TempAcc;
/*******GLOBAL for Dummy index *******/

/* LOW LEVEL Routines for Table : RadiusAccServerTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceRadiusAccServerTable
 Input       :  The Indices
                RadiusAccServerIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhValidateIndexInstanceRadiusAccServerTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhValidateIndexInstanceRadiusAccServerTable (INT4 i4RadiusAccServerIndex)
#else
INT1
nmhValidateIndexInstanceRadiusAccServerTable (i4RadiusAccServerIndex)
     INT4                i4RadiusAccServerIndex;
#endif
{
    return
        nmhValidateIndexInstanceFsRadExtAccServerTable (i4RadiusAccServerIndex);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexRadiusAccServerTable
 Input       :  The Indices
                RadiusAccServerIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhGetFirstRadiusAccServerTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFirstIndexRadiusAccServerTable (INT4 *pi4RadiusAccServerIndex)
#else
INT1
nmhGetFirstIndexRadiusAccServerTable (pi4RadiusAccServerIndex)
     INT4               *pi4RadiusAccServerIndex;
#endif
{
    return nmhGetFirstIndexFsRadExtAccServerTable (pi4RadiusAccServerIndex);
}

/****************************************************************************
 Function    :  nmhGetNextIndexRadiusAccServerTable
 Input       :  The Indices
                RadiusAccServerIndex
                nextRadiusAccServerIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
 /*** $$TRACE_PROCEDURE_NAME = nmhGetNextRadiusAccServerTable ***/
 /*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetNextIndexRadiusAccServerTable (INT4 i4RadiusAccServerIndex,
                                     INT4 *pi4NextRadiusAccServerIndex)
#else
INT1
nmhGetNextIndexRadiusAccServerTable (i4RadiusAccServerIndex,
                                     pi4NextRadiusAccServerIndex)
     INT4                i4RadiusAccServerIndex;
     INT4               *pi4NextRadiusAccServerIndex;
#endif
{
    return nmhGetNextIndexFsRadExtAccServerTable (i4RadiusAccServerIndex,
                                                  pi4NextRadiusAccServerIndex);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetRadiusAccServerAddress
 Input       :  The Indices
                RadiusAccServerIndex

                The Object 
                retValRadiusAccServerAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetRadiusAccServerAddress ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetRadiusAccServerAddress (INT4 i4RadiusAccServerIndex,
                              UINT4 *pu4RetValRadiusAccServerAddress)
#else
INT1
nmhGetRadiusAccServerAddress (i4RadiusAccServerIndex,
                              pu4RetValRadiusAccServerAddress)
     INT4                i4RadiusAccServerIndex;
     UINT4              *pu4RetValRadiusAccServerAddress;
#endif
{
    INT4                i4AddrType;
    tSNMP_OCTET_STRING_TYPE ServerIpAddress;
    UINT1               au1TmpArray[IPVX_IPV6_ADDR_LEN];

    MEMSET (au1TmpArray, 0, IPVX_IPV6_ADDR_LEN);

    ServerIpAddress.pu1_OctetList = au1TmpArray;

    if (nmhGetFsRadExtServerAddrType (i4RadiusAccServerIndex, &i4AddrType) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (i4AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        nmhGetFsRadExtServerAddress (i4RadiusAccServerIndex, &ServerIpAddress);

        MEMCPY (pu4RetValRadiusAccServerAddress,
                ServerIpAddress.pu1_OctetList, IPVX_IPV4_ADDR_LEN);
    }
    else
    {
        *pu4RetValRadiusAccServerAddress = 0;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetRadiusAccClientServerPortNumber
 Input       :  The Indices
                RadiusAccServerIndex

                The Object 
                retValRadiusAccClientServerPortNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetRadiusAccClientServerPortNumber ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetRadiusAccClientServerPortNumber (INT4 i4RadiusAccServerIndex,
                                       INT4
                                       *pi4RetValRadiusAccClientServerPortNumber)
#else
INT1
nmhGetRadiusAccClientServerPortNumber (i4RadiusAccServerIndex,
                                       pi4RetValRadiusAccClientServerPortNumber)
     INT4                i4RadiusAccServerIndex;
     INT4               *pi4RetValRadiusAccClientServerPortNumber;
#endif
{
    return nmhGetFsRadExtAccClientServerPortNumber (i4RadiusAccServerIndex,
                                                    pi4RetValRadiusAccClientServerPortNumber);
}

/****************************************************************************
 Function    :  nmhGetRadiusAccClientRoundTripTime
 Input       :  The Indices
                RadiusAccServerIndex

                The Object 
                retValRadiusAccClientRoundTripTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetRadiusAccClientRoundTripTime ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetRadiusAccClientRoundTripTime (INT4 i4RadiusAccServerIndex,
                                    UINT4
                                    *pu4RetValRadiusAccClientRoundTripTime)
#else
INT1
nmhGetRadiusAccClientRoundTripTime (i4RadiusAccServerIndex,
                                    pu4RetValRadiusAccClientRoundTripTime)
     INT4                i4RadiusAccServerIndex;
     UINT4              *pu4RetValRadiusAccClientRoundTripTime;
#endif
{
    return nmhGetFsRadExtAccClientRoundTripTime (i4RadiusAccServerIndex,
                                                 pu4RetValRadiusAccClientRoundTripTime);
}

/****************************************************************************
 Function    :  nmhGetRadiusAccClientRequests
 Input       :  The Indices
                RadiusAccServerIndex

                The Object 
                retValRadiusAccClientRequests
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetRadiusAccClientRequests ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetRadiusAccClientRequests (INT4 i4RadiusAccServerIndex,
                               UINT4 *pu4RetValRadiusAccClientRequests)
#else
INT1
nmhGetRadiusAccClientRequests (i4RadiusAccServerIndex,
                               pu4RetValRadiusAccClientRequests)
     INT4                i4RadiusAccServerIndex;
     UINT4              *pu4RetValRadiusAccClientRequests;
#endif
{
    return nmhGetFsRadExtAccClientRequests (i4RadiusAccServerIndex,
                                            pu4RetValRadiusAccClientRequests);
}

/****************************************************************************
 Function    :  nmhGetRadiusAccClientRetransmissions
 Input       :  The Indices
                RadiusAccServerIndex

                The Object 
                retValRadiusAccClientRetransmissions
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetRadiusAccClientRetransmissions ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetRadiusAccClientRetransmissions (INT4 i4RadiusAccServerIndex,
                                      UINT4
                                      *pu4RetValRadiusAccClientRetransmissions)
#else
INT1
nmhGetRadiusAccClientRetransmissions (i4RadiusAccServerIndex,
                                      pu4RetValRadiusAccClientRetransmissions)
     INT4                i4RadiusAccServerIndex;
     UINT4              *pu4RetValRadiusAccClientRetransmissions;
#endif
{
    return nmhGetFsRadExtAccClientRetransmissions (i4RadiusAccServerIndex,
                                                   pu4RetValRadiusAccClientRetransmissions);
}

/****************************************************************************
 Function    :  nmhGetRadiusAccClientResponses
 Input       :  The Indices
                RadiusAccServerIndex

                The Object 
                retValRadiusAccClientResponses
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetRadiusAccClientResponses ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetRadiusAccClientResponses (INT4 i4RadiusAccServerIndex,
                                UINT4 *pu4RetValRadiusAccClientResponses)
#else
INT1
nmhGetRadiusAccClientResponses (i4RadiusAccServerIndex,
                                pu4RetValRadiusAccClientResponses)
     INT4                i4RadiusAccServerIndex;
     UINT4              *pu4RetValRadiusAccClientResponses;
#endif

{
    return nmhGetFsRadExtAccClientResponses (i4RadiusAccServerIndex,
                                             pu4RetValRadiusAccClientResponses);
}

/****************************************************************************
 Function    :  nmhGetRadiusAccClientMalformedResponses
 Input       :  The Indices
                RadiusAccServerIndex

                The Object 
                retValRadiusAccClientMalformedResponses
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetRadiusAccClientMalformedResponses ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetRadiusAccClientMalformedResponses (INT4 i4RadiusAccServerIndex,
                                         UINT4
                                         *pu4RetValRadiusAccClientMalformedResponses)
#else
INT1
nmhGetRadiusAccClientMalformedResponses (i4RadiusAccServerIndex,
                                         pu4RetValRadiusAccClientMalformedResponses)
     INT4                i4RadiusAccServerIndex;
     UINT4              *pu4RetValRadiusAccClientMalformedResponses;
#endif
{
    return nmhGetFsRadExtAccClientMalformedResponses (i4RadiusAccServerIndex,
                                                      pu4RetValRadiusAccClientMalformedResponses);
}

/****************************************************************************
 Function    :  nmhGetRadiusAccClientBadAuthenticators
 Input       :  The Indices
                RadiusAccServerIndex

                The Object 
                retValRadiusAccClientBadAuthenticators
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetRadiusAccClientBadAuthenticators ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetRadiusAccClientBadAuthenticators (INT4 i4RadiusAccServerIndex,
                                        UINT4
                                        *pu4RetValRadiusAccClientBadAuthenticators)
#else
INT1
nmhGetRadiusAccClientBadAuthenticators (i4RadiusAccServerIndex,
                                        pu4RetValRadiusAccClientBadAuthenticators)
     INT4                i4RadiusAccServerIndex;
     UINT4              *pu4RetValRadiusAccClientBadAuthenticators;
#endif
{
    return nmhGetFsRadExtAccClientBadAuthenticators (i4RadiusAccServerIndex,
                                                     pu4RetValRadiusAccClientBadAuthenticators);
}

/****************************************************************************
 Function    :  nmhGetRadiusAccClientPendingRequests
 Input       :  The Indices
                RadiusAccServerIndex

                The Object 
                retValRadiusAccClientPendingRequests
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetRadiusAccClientPendingRequests ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetRadiusAccClientPendingRequests (INT4 i4RadiusAccServerIndex,
                                      UINT4
                                      *pu4RetValRadiusAccClientPendingRequests)
#else
INT1
nmhGetRadiusAccClientPendingRequests (i4RadiusAccServerIndex,
                                      pu4RetValRadiusAccClientPendingRequests)
     INT4                i4RadiusAccServerIndex;
     UINT4              *pu4RetValRadiusAccClientPendingRequests;
#endif
{
    return nmhGetFsRadExtAccClientPendingRequests (i4RadiusAccServerIndex,
                                                   pu4RetValRadiusAccClientPendingRequests);
}

/****************************************************************************
 Function    :  nmhGetRadiusAccClientTimeouts
 Input       :  The Indices
                RadiusAccServerIndex

                The Object 
                retValRadiusAccClientTimeouts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetRadiusAccClientTimeouts ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetRadiusAccClientTimeouts (INT4 i4RadiusAccServerIndex,
                               UINT4 *pu4RetValRadiusAccClientTimeouts)
#else
INT1
nmhGetRadiusAccClientTimeouts (i4RadiusAccServerIndex,
                               pu4RetValRadiusAccClientTimeouts)
     INT4                i4RadiusAccServerIndex;
     UINT4              *pu4RetValRadiusAccClientTimeouts;
#endif
{
    return nmhGetFsRadExtAccClientTimeouts (i4RadiusAccServerIndex,
                                            pu4RetValRadiusAccClientTimeouts);
}

/****************************************************************************
 Function    :  nmhGetRadiusAccClientUnknownTypes
 Input       :  The Indices
                RadiusAccServerIndex

                The Object 
                retValRadiusAccClientUnknownTypes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetRadiusAccClientUnknownTypes ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetRadiusAccClientUnknownTypes (INT4 i4RadiusAccServerIndex,
                                   UINT4 *pu4RetValRadiusAccClientUnknownTypes)
#else
INT1
nmhGetRadiusAccClientUnknownTypes (i4RadiusAccServerIndex,
                                   pu4RetValRadiusAccClientUnknownTypes)
     INT4                i4RadiusAccServerIndex;
     UINT4              *pu4RetValRadiusAccClientUnknownTypes;
#endif
{
    return nmhGetFsRadExtAccClientUnknownTypes (i4RadiusAccServerIndex,
                                                pu4RetValRadiusAccClientUnknownTypes);
}

/****************************************************************************
 Function    :  nmhGetRadiusAccClientPacketsDropped
 Input       :  The Indices
                RadiusAccServerIndex

                The Object 
                retValRadiusAccClientPacketsDropped
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetRadiusAccClientPacketsDropped ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetRadiusAccClientPacketsDropped (INT4 i4RadiusAccServerIndex,
                                     UINT4
                                     *pu4RetValRadiusAccClientPacketsDropped)
#else
INT1
nmhGetRadiusAccClientPacketsDropped (i4RadiusAccServerIndex,
                                     pu4RetValRadiusAccClientPacketsDropped)
     INT4                i4RadiusAccServerIndex;
     UINT4              *pu4RetValRadiusAccClientPacketsDropped;
#endif
{
    return nmhGetFsRadExtAccClientPacketsDropped (i4RadiusAccServerIndex,
                                                  pu4RetValRadiusAccClientPacketsDropped);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetRadiusAccClientInvalidServerAddresses
 Input       :  The Indices

                The Object 
                retValRadiusAccClientInvalidServerAddresses
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetRadiusAccClientInvalidServerAddresses ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetRadiusAccClientInvalidServerAddresses (UINT4
                                             *pu4RetValRadiusAccClientInvalidServerAddresses)
#else
INT1 
     
     
     
     
     
     
     
    nmhGetRadiusAccClientInvalidServerAddresses
    (pu4RetValRadiusAccClientInvalidServerAddresses)
     UINT4              *pu4RetValRadiusAccClientInvalidServerAddresses;
#endif
{
    return
        nmhGetFsRadExtAccClientInvalidServerAddresses
        (pu4RetValRadiusAccClientInvalidServerAddresses);
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetRadiusAccClientIdentifier
 Input       :  The Indices

                The Object 
                retValRadiusAccClientIdentifier
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetRadiusAccClientIdentifier ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetRadiusAccClientIdentifier (tSNMP_OCTET_STRING_TYPE *
                                 pRetValRadiusAccClientIdentifier)
#else
INT1
nmhGetRadiusAccClientIdentifier (pRetValRadiusAccClientIdentifier)
     tSNMP_OCTET_STRING_TYPE *pRetValRadiusAccClientIdentifier;
#endif
{
    return nmhGetFsRadExtAccClientIdentifier (pRetValRadiusAccClientIdentifier);
}
