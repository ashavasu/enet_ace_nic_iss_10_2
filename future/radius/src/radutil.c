/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: radutil.c,v 1.1 2015/04/28 12:26:24 siva Exp $ 
 *
 * Description: This file contains utility function for radius
 *
 *******************************************************************/
#ifndef RADUTIL_C
#define RADUTIL_C

#include "radcom.h"

unRadCallBackEntry  gaRadCallBack[RADIUS_MAX_CALLBACK_EVENTS];

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RadUtilReadSecretFromNVRAM                         */
/*                                                                           */
/*     DESCRIPTION      : This function reads  the shared secret key         */
/*                        from the file and loads it into the tRADIUSSERVER  */
/*                        structure.                                         */
/*     INPUT            : NONE                                               */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : RAD_SUCCESS or RAD_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
RadUtilReadSecretFromNVRAM (VOID)
{
    INT4                i4Fd = -1;
    UINT1               au1FileName[ISS_CONFIG_FILE_NAME_LEN];
    UINT1               au1Buf[RADIUS_KEY_LINE_LEN];
    UINT1              *pu1SharedSecret;
    INT4                i4Index = 0;
    INT1               *pi1AddrType = NULL;
    INT1                i1AddrType = 0;
    INT1               *pi1ServAddr = NULL;

    MEMSET (au1Buf, 0, RADIUS_KEY_LINE_LEN);
    MEMSET (au1FileName, 0, sizeof (au1FileName));

    STRNCPY (au1FileName, RADIUS_KEY_SAVE_FILE, STRLEN(RADIUS_KEY_SAVE_FILE));
    au1FileName[STRLEN(RADIUS_KEY_SAVE_FILE)] = '\0';

    RadUtilCallBack (RAD_ACCESS_SECURE_MEMORY, ISS_SRAM_FILENAME_MAPPING,
                     RADIUS_KEY_SAVE_FILE, au1FileName);

    i4Fd = FileOpen (au1FileName, OSIX_FILE_RO);
    if (i4Fd < 0)
    {
        gu1RadConfigRestored = RADIUS_TRUE;
        RADIUS_UNLOCK ();
        return RAD_SUCCESS;
    }

    while (FsUtlReadLine (i4Fd, (INT1 *) au1Buf) == OSIX_SUCCESS)
    {
        i4Index = 0;
        pi1AddrType = (INT1 *) au1Buf;
        while (au1Buf[i4Index] != ',')
        {
            i4Index++;
        }
        au1Buf[i4Index++] = '\0';
        pi1ServAddr = (INT1 *) au1Buf + i4Index;
        while (au1Buf[i4Index] != ',')
        {
            i4Index++;
        }
        au1Buf[i4Index++] = '\0';
        pu1SharedSecret = au1Buf + i4Index;
        while ((au1Buf[i4Index] != ' ') && (au1Buf[i4Index] != '\n'))
        {
            i4Index++;
        }
        au1Buf[i4Index] = '\0';
        i1AddrType = ATOI (pi1AddrType);
        if (RadUtilRestoreSharedSecret (i1AddrType,
                                        pi1ServAddr,
                                        pu1SharedSecret) == RAD_FAILURE)
        {
            FileClose (i4Fd);
            gu1RadConfigRestored = RADIUS_TRUE;
            return RAD_FAILURE;
        }
        MEMSET (au1Buf, 0, RADIUS_KEY_LINE_LEN);
    }
    FileClose (i4Fd);
    gu1RadConfigRestored = RADIUS_TRUE;
    return RAD_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RadUtilRestoreSharedSecret                         */
/*                                                                           */
/*     DESCRIPTION      : This function writes the radius shared secret key  */
/*                      : into the data structure tRADIUSSERVER              */
/*                                                                           */
/*     INPUT            : pu1ServAddr  - Address of Server                   */
/*                      : pu1SharedSecret - Shared Secret Key                */
/*                      : i1AddrType - Version 4 or Version 6 Address        */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : RAD_SUCCESS or RAD_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
RadUtilRestoreSharedSecret (INT1 i1AddrType,
                            INT1 *pi1ServAddr, UINT1 *pu1SharedSecret)
{
    tIPvXAddr           ServerAddress;
    tIp6Addr            Ip6ServerAddr;
    tSNMP_OCTET_STRING_TYPE Password;
    tSNMP_OCTET_STRING_TYPE ServerIpAddress;
    UINT4               u4ServIndex = 0;
    UINT4               u4ServAddr;
    UINT1               au1TmpArray[IPVX_MAX_INET_ADDR_LEN];

    MEMSET (au1TmpArray, 0, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (&ServerAddress, 0, sizeof (tIPvXAddr));

    if (i1AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        u4ServAddr = ATOI (pi1ServAddr);
        MEMCPY (ServerAddress.au1Addr, &u4ServAddr, IPVX_IPV4_ADDR_LEN);
        ServerAddress.u1AddrLen = IPVX_IPV4_ADDR_LEN;
    }
    else
    {
        MEMSET (&Ip6ServerAddr, 0, sizeof (tIp6Addr));
        INET_ATON6 (pi1ServAddr, &Ip6ServerAddr);
    IPVX_ADDR_INIT_FROMV6 (ServerAddress, (VOID *) &Ip6ServerAddr)}
    /*Ip Address */
    ServerAddress.u1Afi = i1AddrType;
    ServerIpAddress.pu1_OctetList = au1TmpArray;
    MEMCPY (ServerIpAddress.pu1_OctetList, ServerAddress.au1Addr,
            ServerAddress.u1AddrLen);
    ServerIpAddress.i4_Length = ServerAddress.u1AddrLen;

    /*Shared Secret */
    Password.i4_Length = STRLEN (pu1SharedSecret);
    Password.pu1_OctetList = pu1SharedSecret;

    u4ServIndex = RadGetServerIndex (&ServerAddress);

    /*If entry already exists, set the radius key */
    if (RAD_ERROR != u4ServIndex)
    {
        if (nmhSetFsRadExtServerEntryStatus (u4ServIndex,
                                             NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            return (RAD_FAILURE);
        }
        if (nmhSetFsRadExtServerSharedSecret (u4ServIndex, &Password)
            == SNMP_FAILURE)
        {
            return (RAD_FAILURE);
        }
    }
    else
    {
        u4ServIndex = GetNewRadServerIndex ();
        if ((u4ServIndex == 0) || (u4ServIndex > MAX_RAD_SERVERS_LIMIT))
        {
            return RAD_FAILURE;
        }
        /* Create a new entry */
        if (nmhSetFsRadExtServerEntryStatus (u4ServIndex,
                                             CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            return (RAD_FAILURE);
        }
        /* Set the type of server address - ipv4 or ipv6 */
        if (nmhSetFsRadExtServerAddrType
            (u4ServIndex,
             RADIUS_IPVX_TYPE_FROM_LENGTH (ServerAddress.u1AddrLen))
            == SNMP_FAILURE)
        {
            return (RAD_FAILURE);
        }
        /* Set the server address */
        if (nmhSetFsRadExtServerAddress (u4ServIndex, &ServerIpAddress)
            == SNMP_FAILURE)
        {
            return (RAD_FAILURE);
        }
        /* Set the secret key */
        if (nmhSetFsRadExtServerSharedSecret (u4ServIndex, &Password)
            == SNMP_FAILURE)
        {
            return (RAD_FAILURE);
        }
    }
    if (nmhSetFsRadExtServerEntryStatus (u4ServIndex, ACTIVE) == SNMP_FAILURE)
    {
        return RAD_FAILURE;
    }
    return RAD_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RadUtilWriteSecretToNVRAM                          */
/*                                                                           */
/*     DESCRIPTION      : This function writes the shared secret key         */
/*                        in radiuskey file                                  */
/*                                                                           */
/*     INPUT            : NONE                                               */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : RAD_SUCCESS or RAD_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
RadUtilWriteSecretToNVRAM (VOID)
{
    tRADIUS_SERVER     *ptRadiusServer = NULL;
    tIPvXAddr           ServAddr;
    tSNMP_OCTET_STRING_TYPE ServerIpAddress;
    UINT4               u4IpAddress = 0;
    UINT4               u4RadIndex = 0;
    INT1                i1Length = 0;
    UINT1               au1Buff[MAX_RAD_SERVERS_LIMIT * RADIUS_KEY_LINE_LEN];
    UINT1               au1FileName[ISS_CONFIG_FILE_NAME_LEN];
    UINT1               au1TempBuff[RADIUS_KEY_LINE_LEN];
    UINT1               au1TempArray[IPVX_MAX_INET_ADDR_LEN];
    UINT4               u4Offset = 0;

    MEMSET (au1FileName, 0, sizeof (au1FileName));
    MEMSET (au1Buff, 0, sizeof (au1Buff));

    STRNCPY (au1FileName, RADIUS_KEY_SAVE_FILE, STRLEN(RADIUS_KEY_SAVE_FILE));
    au1FileName[STRLEN(RADIUS_KEY_SAVE_FILE)] = '\0';

    RadUtilCallBack (RAD_ACCESS_SECURE_MEMORY, ISS_SRAM_FILENAME_MAPPING,
                     RADIUS_KEY_SAVE_FILE, au1FileName);

    ServerIpAddress.pu1_OctetList = au1TempArray;
    RADIUS_LOCK ();

    for (u4RadIndex = 0; u4RadIndex < MAX_RAD_SERVERS_LIMIT; u4RadIndex++)
    {
        ptRadiusServer = a_gServerTable[u4RadIndex];
        if ((ptRadiusServer != NULL) && (*ptRadiusServer->a_u1Secret) != '\0')
        {
            /* Retrieve the key and IP address of the radius server */
            MEMSET (au1TempBuff, 0, sizeof (au1TempBuff));
            MEMSET (ServerIpAddress.pu1_OctetList, 0, IPVX_MAX_INET_ADDR_LEN);

            MEMCPY (ServerIpAddress.pu1_OctetList,
                    ptRadiusServer->ServerAddress.au1Addr,
                    ptRadiusServer->ServerAddress.u1AddrLen);
            ServerIpAddress.i4_Length = ptRadiusServer->ServerAddress.u1AddrLen;

            IPVX_ADDR_INIT (ServAddr,
                            ptRadiusServer->ServerAddress.u1Afi,
                            ServerIpAddress.pu1_OctetList);

            /* Write the server address type, radius key and address of server */
            if (ptRadiusServer->ServerAddress.u1Afi == IPVX_ADDR_FMLY_IPV4)
            {
                MEMCPY (&u4IpAddress, ServAddr.au1Addr, IPVX_IPV4_ADDR_LEN);
                i1Length = (INT1) SNPRINTF ((CHR1 *) au1TempBuff,
                                            RADIUS_KEY_LINE_LEN, "%d,%d,%s\r\n",
                                            ptRadiusServer->ServerAddress.u1Afi,
                                            u4IpAddress,
                                            ptRadiusServer->a_u1Secret);
            }
            else
            {
                i1Length = (INT1) SNPRINTF ((CHR1 *) au1TempBuff,
                                            RADIUS_KEY_LINE_LEN, "%d,%s,%s\r\n",
                                            ptRadiusServer->ServerAddress.u1Afi,
                                            Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                                          (ServAddr.au1Addr)),
                                            ptRadiusServer->a_u1Secret);
            }

            STRNCPY (au1Buff + u4Offset, au1TempBuff, sizeof (au1TempBuff));
            u4Offset += i1Length;

        }
    }

    au1Buff[u4Offset] = '\0';

    if ((0 != u4Offset) &&
        (RAD_SUCCESS !=
         RadUtilCallBack (RAD_ACCESS_SECURE_MEMORY, ISS_SECURE_MEM_SAVE,
                          RADIUS_KEY_SAVE_FILE, ISS_CHAR_BUFFER, au1Buff)))
    {
        RADIUS_UNLOCK ();
        return RAD_FAILURE;
    }

    RADIUS_UNLOCK ();
    return RAD_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RadUtilCallBack                                    */
/*                                                                           */
/*     DESCRIPTION      : This function calls the registered custom function */
/*                                                                           */
/*     INPUT            : NONE                                               */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
INT4
RadUtilCallBack (UINT4 u4Event, ...)
{
    va_list             vaList;
    UINT4              *apu4VaArgs[RAD_CALLBACK_MAX_ARGS];
    INT4                i4RetVal = 0;
    UINT4               u4SecMemAction = 0;
    UINT1               u1Index = 0;

    MEMSET (&vaList, 0, sizeof (vaList));
    MEMSET (apu4VaArgs, 0, sizeof (apu4VaArgs));

    switch (u4Event)
    {
        case RADIUS_READ_SECRET_FROM_NVRAM:
            if (RAD_CALLBACK_FN[u4Event].pRadUtilReadSecretFromNVRAM != NULL)
            {
                i4RetVal = RAD_CALLBACK_FN[u4Event].
                    pRadUtilReadSecretFromNVRAM ();
            }
            break;

        case RADIUS_WRITE_SECRET_TO_NVRAM:
            if (RAD_CALLBACK_FN[u4Event].pRadUtilWriteSecretToNVRAM != NULL)
            {
                i4RetVal = RAD_CALLBACK_FN[u4Event].
                    pRadUtilWriteSecretToNVRAM ();
            }
            break;

        case RAD_ACCESS_SECURE_MEMORY:
        {
            va_start (vaList, u4Event);
            /* Walk through the arguements and store in args array.  */
            for (u1Index = 0; u1Index < RAD_CALLBACK_MAX_ARGS; u1Index += 1)
            {
                apu4VaArgs[u1Index] = va_arg (vaList, UINT4 *);
            }
            va_end (vaList);

            if (NULL != RAD_CALLBACK_FN[u4Event].pRadCustSecureMemAccess)
            {
                u4SecMemAction = PTR_TO_U4 (apu4VaArgs[0]);
                if (ISS_SRAM_FILENAME_MAPPING == u4SecMemAction)
                {
                    i4RetVal =
                        RAD_CALLBACK_FN[u4Event].pRadCustSecureMemAccess
                        (u4SecMemAction, ISS_FILE,
                         apu4VaArgs[1], apu4VaArgs[2]);
                }
                else if (ISS_SECURE_MEM_SAVE == u4SecMemAction)
                {
                    i4RetVal =
                        RAD_CALLBACK_FN[u4Event].pRadCustSecureMemAccess
                        (u4SecMemAction, PTR_TO_U4 (apu4VaArgs[2]),
                         (CHR1 *) apu4VaArgs[1], (UINT1 *) apu4VaArgs[3]);
                    /* Action, Type of content, Filename, Buffer */

                }
                else if (ISS_SECURE_MEM_DELETE == u4SecMemAction)
                {
                    i4RetVal =
                        RAD_CALLBACK_FN[u4Event].pRadCustSecureMemAccess
                        (u4SecMemAction, ISS_FILE, (CHR1 *) apu4VaArgs[1]);
                }
                if (ISS_FAILURE == i4RetVal)
                {
                    i4RetVal = RAD_FAILURE;
                }
                else
                {
                    i4RetVal = RAD_SUCCESS;
                }

            }
            break;
        }
        default:
            i4RetVal = RAD_FAILURE;
            break;
    }
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : RadUtilRegCallBackforRADModule                       */
/*                                                                           */
/* Description        : This function is invoked to register the call backs  */
/*                      from  application.                                   */
/*                                                                           */
/* Input(s)           : u4Event - Event for which callback is registered     */
/*                      pFsCbInfo - Call Back Function                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RAD__SUCCESS/RAD__FAILURE                            */
/*****************************************************************************/
INT4
RadUtilRegCallBackforRADModule (UINT4 u4Event, tFsCbInfo * pFsCbInfo)
{
    INT4                i4RetVal = RAD_SUCCESS;

    switch (u4Event)
    {
        case RADIUS_READ_SECRET_FROM_NVRAM:
            RAD_CALLBACK_FN[u4Event].pRadUtilReadSecretFromNVRAM =
                pFsCbInfo->pRadUtilReadSecretFromNVRAM;
            break;

        case RADIUS_WRITE_SECRET_TO_NVRAM:
            RAD_CALLBACK_FN[u4Event].pRadUtilWriteSecretToNVRAM =
                pFsCbInfo->pRadUtilWriteSecretToNVRAM;
            break;

        case RAD_ACCESS_SECURE_MEMORY:
            RAD_CALLBACK_FN[u4Event].pRadCustSecureMemAccess =
                pFsCbInfo->pIssCustSecureProcess;
            break;

        default:
            i4RetVal = RAD_FAILURE;
            break;
    }
    return i4RetVal;
}
#endif
