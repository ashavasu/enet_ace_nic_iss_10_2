/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: radmain.c,v 1.22 2015/04/28 12:26:24 siva Exp $ 
 *
 * Description: This file contains the system Initialisation.
 *
 *******************************************************************/
#define _RADMAIN_C

#include "radcom.h"
#include "arMD5_inc.h"
#include "utilalgo.h"

#ifdef SNMP_2_WANTED
#include "snmctdfs.h"
#include "fsradiwr.h"
#include "fsradewr.h"
#include "radaccwr.h"
#include "radautwr.h"
#endif

tTimerListId        p_TimerListId;    /* Timer List ID is used in FSAP2 */
UINT4               gRadDebugMask;
INT4                gi4RadiusProxyStatus;
UINT1               gau1RadiusClientIdentifier[64];
INT4                gi4RadMaxNoofUserEntries;
tIPvXAddr           gRadActiveServer;
UINT1               gau1RadActiveServerHostName[DNS_MAX_QUERY_LEN];
tOsixTaskId         gu4RadTaskId;
tOsixSemId          gRadSemId;
UINT1               gau1RadBuf[RAD_RECV_BUF_SIZE];
UINT1               gau1ZeroHostName[DNS_MAX_QUERY_LEN];
/*****************************/
INT4
RadiusStart (VOID)
{
    if (OsixTskCrt (TASK_NAME, /*15 */ 45, OSIX_DEFAULT_STACK_SIZE * 2,
                    (OsixTskEntry) RadiusMain, 0,
                    &gu4RadTaskId) != OSIX_SUCCESS)
    {
        RAD_ERR_PRINT ("\nError in Spawning the Task...\n");
    }
    return gu4RadTaskId;
}

/*****************************/

void
RadiusMain (INT1 *pParam)
{
    UINT4               u4_events = 0;
    UINT4               u4SrcServerAddress = 0;
    INT4                i4LargeSocketId = 0;
    INT4                i4_select = 0;
    INT4                i4_msglength = 0;
    INT4                i4_SocketAddressLength = 0;
    fd_set              readfds;
    struct sockaddr_in  RadiusServerAddress;
    struct sockaddr_in  RadClientSoc;
    struct timeval      stimeval;
    tIPvXAddr           ServerAddress;
#ifdef IP6_WANTED
    INT4                i4_SocketAddressLength6 = 0;
    struct sockaddr_in6 RadiusServerAddress6;
#endif

    MEMSET (&readfds, 0, sizeof (fd_set));

    UNUSED_PARAM (pParam);

    if (radiusInitGlobals () == RAD_FAILURE)
    {
        RAD_ERR_PRINT ("\n RADIUS GLOBALS Init failed");
        /* Indicate the status of initialization to the main routine */
        RAD_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
    else
    {
        RAD_EVENT_PRINT ("\n RADIUS GLOBALS Init successful");
    }

    if (OsixTskIdSelf (&gu4RadTaskId) == OSIX_FAILURE)
    {
        RAD_ERR_PRINT ("\n RADIUS OsixTskIdSelf failed");
        /* Indicate the status of initialization to the main routine */
        RAD_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    if (OsixCreateSem (RADIUS_SEM_NAME, 1, 0, &gRadSemId) != OSIX_SUCCESS)
    {
        RAD_ERR_PRINT ("\n RADIUS Semaphore creation failed");
        RAD_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    if (radiusInitAuth () == RAD_FAILURE)
    {
        RAD_ERR_PRINT ("\n RADIUS AUTH Init failed");
        /* Indicate the status of initialization to the main routine */
        RAD_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
    else
    {
        RAD_EVENT_PRINT ("\n RADIUS AUTH Init successful");
    }

    if (radiusInitAcc () == RAD_FAILURE)
    {
        RAD_ERR_PRINT ("\n RADIUS ACC Init failed");
        /* Indicate the status of initialization to the main routine */
        RAD_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
    else
    {
        RAD_EVENT_PRINT ("\n RADIUS ACC Init successful");
    }

    if (TmrCreateTimerList (TASK_NAME,    /* OWNER task (DUMMY) */
                            EXP01_EVENT,    /* Event - timer list (DUMMY) */
                            NULL,    /* CallBack fn. - priority */
                            &p_TimerListId) == TMR_FAILURE)
    {
        RAD_ERR_PRINT ("\n TmrCreateTimerList failed");
        /* Indicate the status of initialization to the main routine */
        RAD_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    MEMSET ((UINT1 *) &RadiusServerAddress, 0, sizeof (RadiusServerAddress));
    i4_SocketAddressLength = sizeof (RadiusServerAddress);

    if (i4_gRadiusAuthSocketId > i4_gRadiusAccSocketId)
    {
        i4LargeSocketId = i4_gRadiusAuthSocketId;
    }
    else
    {
        i4LargeSocketId = i4_gRadiusAccSocketId;
    }
    i4LargeSocketId = (i4LargeSocketId > gi4ProxyToClientSocketId) ?
        i4LargeSocketId : gi4ProxyToClientSocketId;
    i4LargeSocketId = (i4LargeSocketId > gi4ProxyToServerSocketId) ?
        i4LargeSocketId : gi4ProxyToServerSocketId;
#ifdef IP6_WANTED
    i4LargeSocketId = (i4LargeSocketId > i4_gRadiusAuthSocketId6) ?
        i4LargeSocketId : i4_gRadiusAuthSocketId6;
    i4LargeSocketId = (i4LargeSocketId > i4_gRadiusAccSocketId6) ?
        i4LargeSocketId : i4_gRadiusAccSocketId6;
#endif

    /* Indicate the status of initialization to the main routine */
    RAD_INIT_COMPLETE (OSIX_SUCCESS);
#ifdef SNMP_2_WANTED
    /* Register the protocol MIBs with SNMP */
    RegisterFSRADI ();
    RegisterRADACC ();
    RegisterRADAUT ();
    RegisterFSRADE ();
#endif

    while (1)
    {
        FD_ZERO (&readfds);
        FD_SET (i4_gRadiusAuthSocketId, &readfds);
        FD_SET (i4_gRadiusAccSocketId, &readfds);
#ifdef IP6_WANTED
        FD_SET (i4_gRadiusAuthSocketId6, &readfds);
        FD_SET (i4_gRadiusAccSocketId6, &readfds);
#endif
        FD_SET (gi4ProxyToClientSocketId, &readfds);
        FD_SET (gi4ProxyToServerSocketId, &readfds);

        /* Don't rely on stimeval after return from select,
         * hence set stimeval each time inside the loop
         */
        stimeval.tv_sec = 1;
        stimeval.tv_usec = 0;

        i4_select = select ((i4LargeSocketId + 1), &readfds,
                            (fd_set *) 0, (fd_set *) 0, &stimeval);

        MEMSET (gau1RadBuf, 0, RAD_RECV_BUF_SIZE);
        if (i4_select > 0)
        {
            if (FD_ISSET (i4_gRadiusAuthSocketId, &readfds))
            {
                i4_msglength =
                    recvfrom (i4_gRadiusAuthSocketId, gau1RadBuf,
                              RAD_RECV_BUF_SIZE, 0,
                              (struct sockaddr *) &RadiusServerAddress,
                              (INT4 *) &i4_SocketAddressLength);

                FD_CLR (i4_gRadiusAuthSocketId, &readfds);

                if (i4_msglength > 0)
                {
                    u4SrcServerAddress =
                        OSIX_NTOHL (RadiusServerAddress.sin_addr.s_addr);
                    IPVX_ADDR_INIT_FROMV4 (ServerAddress, u4SrcServerAddress);

                    RADIUS_LOCK ();
                    radiusCheckIncomingPacketAuth (gau1RadBuf, &ServerAddress);
                    RADIUS_UNLOCK ();
                }
            }

            if (FD_ISSET (i4_gRadiusAccSocketId, &readfds))
            {
                i4_msglength =
                    recvfrom (i4_gRadiusAccSocketId, gau1RadBuf,
                              RAD_RECV_BUF_SIZE, 0,
                              (struct sockaddr *) &RadiusServerAddress,
                              (INT4 *) &i4_SocketAddressLength);

                FD_CLR (i4_gRadiusAccSocketId, &readfds);

                if (i4_msglength > 0)
                {
                    u4SrcServerAddress =
                        OSIX_NTOHL (RadiusServerAddress.sin_addr.s_addr);

                    IPVX_ADDR_INIT_FROMV4 (ServerAddress, u4SrcServerAddress);

                    RADIUS_LOCK ();
                    radiusCheckIncomingPacketAcc (gau1RadBuf, &ServerAddress);
                    RADIUS_UNLOCK ();
                }
            }
#ifdef IP6_WANTED
            i4_SocketAddressLength6 = sizeof (RadiusServerAddress6);
            if (FD_ISSET (i4_gRadiusAuthSocketId6, &readfds))
            {
                i4_msglength =
                    recvfrom (i4_gRadiusAuthSocketId6, gau1RadBuf,
                              RAD_RECV_BUF_SIZE, 0,
                              (struct sockaddr *) &RadiusServerAddress6,
                              (INT4 *) &i4_SocketAddressLength6);

                FD_CLR (i4_gRadiusAuthSocketId6, &readfds);

                if (i4_msglength > 0)
                {
                    IPVX_ADDR_INIT_FROMV6 (ServerAddress,
                                           RadiusServerAddress6.sin6_addr.
                                           s6_addr);

                    RADIUS_LOCK ();
                    radiusCheckIncomingPacketAuth (gau1RadBuf, &ServerAddress);
                    RADIUS_UNLOCK ();
                }
            }

            if (FD_ISSET (i4_gRadiusAccSocketId6, &readfds))
            {
                i4_msglength =
                    recvfrom (i4_gRadiusAccSocketId6, gau1RadBuf,
                              RAD_RECV_BUF_SIZE, 0,
                              (struct sockaddr *) &RadiusServerAddress6,
                              (INT4 *) &i4_SocketAddressLength6);

                FD_CLR (i4_gRadiusAccSocketId6, &readfds);

                if (i4_msglength > 0)
                {
                    IPVX_ADDR_INIT_FROMV6 (ServerAddress,
                                           RadiusServerAddress6.sin6_addr.
                                           s6_addr);

                    RADIUS_LOCK ();
                    radiusCheckIncomingPacketAcc (gau1RadBuf, &ServerAddress);
                    RADIUS_UNLOCK ();
                }
            }
#endif
            if (FD_ISSET (gi4ProxyToClientSocketId, &readfds))
            {

                i4_msglength =
                    recvfrom (gi4ProxyToClientSocketId, gau1RadBuf,
                              RAD_RECV_BUF_SIZE, 0,
                              (struct sockaddr *) &RadClientSoc,
                              (INT4 *) &i4_SocketAddressLength);

                FD_CLR (gi4ProxyToClientSocketId, &readfds);

                RadProxyProcessPktFromClient (gau1RadBuf, RadClientSoc);

            }
            if (FD_ISSET (gi4ProxyToServerSocketId, &readfds))
            {
                i4_msglength =
                    recvfrom (gi4ProxyToServerSocketId, gau1RadBuf,
                              RAD_RECV_BUF_SIZE, 0,
                              (struct sockaddr *) &RadiusServerAddress,
                              (INT4 *) &i4_SocketAddressLength);

                FD_CLR (gi4ProxyToServerSocketId, &readfds);

                RadProxyProcessPktFromServer (gau1RadBuf);
            }
        }

        if (OsixEvtRecv
            (gu4RadTaskId, (EXP01_EVENT), (OSIX_NO_WAIT | OSIX_EV_ANY),
             &u4_events) == OSIX_SUCCESS)
        {
            if (u4_events & EXP01_EVENT)
            {
                RADIUS_LOCK ();
                radiusTimeoutHandler (p_TimerListId);
                RADIUS_UNLOCK ();
            }
        }
    }
}

#ifdef __STDC__
VOID
md5_calc (unsigned char *output, unsigned char *input, unsigned int inlen)
#else
VOID
md5_calc (output, input, inlen)
     unsigned char      *output;
     unsigned char      *input;    /* input block */
     unsigned int        inlen;    /* length of input block */
#endif
{

    unUtilAlgo          UtilAlgo;
    MEMSET (&UtilAlgo, 0, sizeof (unUtilAlgo));

    UtilAlgo.UtilMd5Algo.pu1Md5InBuf = input;
    UtilAlgo.UtilMd5Algo.u4Md5InBufLen = inlen;
    UtilAlgo.UtilMd5Algo.pu1Md5OutDigest = output;

    UtilMac (ISS_UTIL_ALGO_MD5_ALGO, &UtilAlgo);

}

/*****************************************************************************/
/* Function Name      : RadiusLock ()                                        */
/*                                                                           */
/* Description        : This function is used to take the Radius mutual      */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS or SNMP_FAILURE                         */
/*****************************************************************************/
INT4
RadiusLock (VOID)
{
    if (OsixSemTake (gRadSemId) != OSIX_SUCCESS)
    {
        RAD_ERR_PRINT ("\nSemaphore Take failed.\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RadiusUnLock ()                                      */
/*                                                                           */
/* Description        : This function is used to give the Radius mutual      */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS or SNMP_FAILURE                         */
/*****************************************************************************/
INT4
RadiusUnLock (VOID)
{
    if (OsixSemGive (gRadSemId) != OSIX_SUCCESS)
    {
        RAD_ERR_PRINT ("\nSemaphore Give failed.\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}
