/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: radprint.c,v 1.10 2015/04/28 12:26:24 siva Exp $ 
 *
 * Description: This file contains modules to print the debud strings.
 *
 *******************************************************************/

#define RADPRINT_C

#include "radcom.h"

/* extern FILE * fp; */

PRIVATE INT4        get_args_in_format (UINT1 *);

#ifdef __STDC__
PRIVATE INT4
get_args_in_format (UINT1 *p_u1_format)
#else
PRIVATE INT4
get_args_in_format (p_u1_format)
     UINT1              *p_u1_format;
#endif
{
    INT4                i4_args;

    for (i4_args = 0; *p_u1_format; p_u1_format++)
        if (*p_u1_format == '%')
            i4_args++;

    return (i4_args);
}
