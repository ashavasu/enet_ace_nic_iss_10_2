/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: radautll.c,v 1.10 2015/04/28 12:26:24 siva Exp $ 
 *
 * Description: This file contains the low level routines.
 *
 *******************************************************************/

# include "snmctdfs.h"
# include "snmccons.h"

# include "radsnmp.h"
# include "radcom.h"

/*******GLOBAL for Dummy index *******/
UINT4               u4TempAuth;
/*******GLOBAL for Dummy index *******/

/* LOW LEVEL Routines for Table : RadiusAuthServerTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceRadiusAuthServerTable
 Input       :  The Indices
                RadiusAuthServerIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhValidateIndexInstanceRadiusAuthServerTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhValidateIndexInstanceRadiusAuthServerTable (INT4 i4RadiusAuthServerIndex)
#else
INT1
nmhValidateIndexInstanceRadiusAuthServerTable (i4RadiusAuthServerIndex)
     INT4                i4RadiusAuthServerIndex;
#endif
{
    return
        nmhValidateIndexInstanceFsRadExtAuthServerTable
        (i4RadiusAuthServerIndex);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexRadiusAuthServerTable
 Input       :  The Indices
                RadiusAuthServerIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhGetFirstRadiusAuthServerTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFirstIndexRadiusAuthServerTable (INT4 *pi4RadiusAuthServerIndex)
#else
INT1
nmhGetFirstIndexRadiusAuthServerTable (pi4RadiusAuthServerIndex)
     INT4               *pi4RadiusAuthServerIndex;
#endif
{
    return nmhGetFirstIndexFsRadExtAuthServerTable (pi4RadiusAuthServerIndex);
}

/****************************************************************************
 Function    :  nmhGetNextIndexRadiusAuthServerTable
 Input       :  The Indices
                RadiusAuthServerIndex
                nextRadiusAuthServerIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
 /*** $$TRACE_PROCEDURE_NAME = nmhGetNextRadiusAuthServerTable ***/
 /*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetNextIndexRadiusAuthServerTable (INT4 i4RadiusAuthServerIndex,
                                      INT4 *pi4NextRadiusAuthServerIndex)
#else
INT1
nmhGetNextIndexRadiusAuthServerTable (i4RadiusAuthServerIndex,
                                      pi4NextRadiusAuthServerIndex)
     INT4                i4RadiusAuthServerIndex;
     INT4               *pi4NextRadiusAuthServerIndex;
#endif
{
    return nmhGetNextIndexFsRadExtAuthServerTable (i4RadiusAuthServerIndex,
                                                   pi4NextRadiusAuthServerIndex);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetRadiusAuthServerAddress
 Input       :  The Indices
                RadiusAuthServerIndex

                The Object 
                retValRadiusAuthServerAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetRadiusAuthServerAddress ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetRadiusAuthServerAddress (INT4 i4RadiusAuthServerIndex,
                               UINT4 *pu4RetValRadiusAuthServerAddress)
#else
INT1
nmhGetRadiusAuthServerAddress (i4RadiusAuthServerIndex,
                               pu4RetValRadiusAuthServerAddress)
     INT4                i4RadiusAuthServerIndex;
     UINT4              *pu4RetValRadiusAuthServerAddress;
#endif
{
    INT4                i4AddrType;
    tSNMP_OCTET_STRING_TYPE ServerIpAddress;
    UINT1               au1TmpArray[IPVX_IPV6_ADDR_LEN];

    MEMSET (&ServerIpAddress, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1TmpArray, 0, IPVX_IPV6_ADDR_LEN);
    ServerIpAddress.pu1_OctetList = au1TmpArray;

    if (nmhGetFsRadExtAuthServerAddressType
        (i4RadiusAuthServerIndex, &i4AddrType) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (i4AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        nmhGetFsRadExtAuthServerAddress (i4RadiusAuthServerIndex,
                                         &ServerIpAddress);

        MEMCPY (pu4RetValRadiusAuthServerAddress,
                ServerIpAddress.pu1_OctetList, IPVX_IPV4_ADDR_LEN);
    }
    else
    {
        *pu4RetValRadiusAuthServerAddress = 0;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetRadiusAuthClientServerPortNumber
 Input       :  The Indices
                RadiusAuthServerIndex

                The Object 
                retValRadiusAuthClientServerPortNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetRadiusAuthClientServerPortNumber ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetRadiusAuthClientServerPortNumber (INT4 i4RadiusAuthServerIndex,
                                        INT4
                                        *pi4RetValRadiusAuthClientServerPortNumber)
#else
INT1
nmhGetRadiusAuthClientServerPortNumber (i4RadiusAuthServerIndex,
                                        pi4RetValRadiusAuthClientServerPortNumber)
     INT4                i4RadiusAuthServerIndex;
     INT4               *pi4RetValRadiusAuthClientServerPortNumber;
#endif
{
    return nmhGetFsRadExtAuthClientServerPortNumber (i4RadiusAuthServerIndex,
                                                     pi4RetValRadiusAuthClientServerPortNumber);
}

/****************************************************************************
 Function    :  nmhGetRadiusAuthClientRoundTripTime
 Input       :  The Indices
                RadiusAuthServerIndex

                The Object 
                retValRadiusAuthClientRoundTripTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetRadiusAuthClientRoundTripTime ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetRadiusAuthClientRoundTripTime (INT4 i4RadiusAuthServerIndex,
                                     UINT4
                                     *pu4RetValRadiusAuthClientRoundTripTime)
#else
INT1
nmhGetRadiusAuthClientRoundTripTime (i4RadiusAuthServerIndex,
                                     pu4RetValRadiusAuthClientRoundTripTime)
     INT4                i4RadiusAuthServerIndex;
     UINT4              *pu4RetValRadiusAuthClientRoundTripTime;
#endif
{
    return nmhGetFsRadExtAuthClientRoundTripTime (i4RadiusAuthServerIndex,
                                                  pu4RetValRadiusAuthClientRoundTripTime);
}

/****************************************************************************
 Function    :  nmhGetRadiusAuthClientAccessRequests
 Input       :  The Indices
                RadiusAuthServerIndex

                The Object 
                retValRadiusAuthClientAccessRequests
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetRadiusAuthClientAccessRequests ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetRadiusAuthClientAccessRequests (INT4 i4RadiusAuthServerIndex,
                                      UINT4
                                      *pu4RetValRadiusAuthClientAccessRequests)
#else
INT1
nmhGetRadiusAuthClientAccessRequests (i4RadiusAuthServerIndex,
                                      pu4RetValRadiusAuthClientAccessRequests)
     INT4                i4RadiusAuthServerIndex;
     UINT4              *pu4RetValRadiusAuthClientAccessRequests;
#endif
{
    return nmhGetFsRadExtAuthClientAccessRequests (i4RadiusAuthServerIndex,
                                                   pu4RetValRadiusAuthClientAccessRequests);
}

/****************************************************************************
 Function    :  nmhGetRadiusAuthClientAccessRetransmissions
 Input       :  The Indices
                RadiusAuthServerIndex

                The Object 
                retValRadiusAuthClientAccessRetransmissions
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetRadiusAuthClientAccessRetransmissions ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetRadiusAuthClientAccessRetransmissions (INT4 i4RadiusAuthServerIndex,
                                             UINT4
                                             *pu4RetValRadiusAuthClientAccessRetransmissions)
#else
INT1
nmhGetRadiusAuthClientAccessRetransmissions (i4RadiusAuthServerIndex,
                                             pu4RetValRadiusAuthClientAccessRetransmissions)
     INT4                i4RadiusAuthServerIndex;
     UINT4              *pu4RetValRadiusAuthClientAccessRetransmissions;
#endif
{
    return
        nmhGetFsRadExtAuthClientAccessRetransmissions (i4RadiusAuthServerIndex,
                                                       pu4RetValRadiusAuthClientAccessRetransmissions);
}

/****************************************************************************
 Function    :  nmhGetRadiusAuthClientAccessAccepts
 Input       :  The Indices
                RadiusAuthServerIndex

                The Object 
                retValRadiusAuthClientAccessAccepts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetRadiusAuthClientAccessAccepts ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetRadiusAuthClientAccessAccepts (INT4 i4RadiusAuthServerIndex,
                                     UINT4
                                     *pu4RetValRadiusAuthClientAccessAccepts)
#else
INT1
nmhGetRadiusAuthClientAccessAccepts (i4RadiusAuthServerIndex,
                                     pu4RetValRadiusAuthClientAccessAccepts)
     INT4                i4RadiusAuthServerIndex;
     UINT4              *pu4RetValRadiusAuthClientAccessAccepts;
#endif
{
    return nmhGetFsRadExtAuthClientAccessAccepts (i4RadiusAuthServerIndex,
                                                  pu4RetValRadiusAuthClientAccessAccepts);
}

/****************************************************************************
 Function    :  nmhGetRadiusAuthClientAccessRejects
 Input       :  The Indices
                RadiusAuthServerIndex

                The Object 
                retValRadiusAuthClientAccessRejects
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetRadiusAuthClientAccessRejects ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetRadiusAuthClientAccessRejects (INT4 i4RadiusAuthServerIndex,
                                     UINT4
                                     *pu4RetValRadiusAuthClientAccessRejects)
#else
INT1
nmhGetRadiusAuthClientAccessRejects (i4RadiusAuthServerIndex,
                                     pu4RetValRadiusAuthClientAccessRejects)
     INT4                i4RadiusAuthServerIndex;
     UINT4              *pu4RetValRadiusAuthClientAccessRejects;
#endif
{
    return nmhGetFsRadExtAuthClientAccessRejects (i4RadiusAuthServerIndex,
                                                  pu4RetValRadiusAuthClientAccessRejects);
}

/****************************************************************************
 Function    :  nmhGetRadiusAuthClientAccessChallenges
 Input       :  The Indices
                RadiusAuthServerIndex

                The Object 
                retValRadiusAuthClientAccessChallenges
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetRadiusAuthClientAccessChallenges ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetRadiusAuthClientAccessChallenges (INT4 i4RadiusAuthServerIndex,
                                        UINT4
                                        *pu4RetValRadiusAuthClientAccessChallenges)
#else
INT1
nmhGetRadiusAuthClientAccessChallenges (i4RadiusAuthServerIndex,
                                        pu4RetValRadiusAuthClientAccessChallenges)
     INT4                i4RadiusAuthServerIndex;
     UINT4              *pu4RetValRadiusAuthClientAccessChallenges;
#endif
{
    return nmhGetFsRadExtAuthClientAccessChallenges (i4RadiusAuthServerIndex,
                                                     pu4RetValRadiusAuthClientAccessChallenges);
}

/****************************************************************************
 Function    :  nmhGetRadiusAuthClientMalformedAccessResponses
 Input       :  The Indices
                RadiusAuthServerIndex

                The Object 
                retValRadiusAuthClientMalformedAccessResponses
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetRadiusAuthClientMalformedAccessResponses ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetRadiusAuthClientMalformedAccessResponses (INT4 i4RadiusAuthServerIndex,
                                                UINT4
                                                *pu4RetValRadiusAuthClientMalformedAccessResponses)
#else
INT1
nmhGetRadiusAuthClientMalformedAccessResponses (i4RadiusAuthServerIndex,
                                                pu4RetValRadiusAuthClientMalformedAccessResponses)
     INT4                i4RadiusAuthServerIndex;
     UINT4              *pu4RetValRadiusAuthClientMalformedAccessResponses;
#endif
{
    return
        nmhGetFsRadExtAuthClientMalformedAccessResponses
        (i4RadiusAuthServerIndex,
         pu4RetValRadiusAuthClientMalformedAccessResponses);
}

/****************************************************************************
 Function    :  nmhGetRadiusAuthClientBadAuthenticators
 Input       :  The Indices
                RadiusAuthServerIndex

                The Object 
                retValRadiusAuthClientBadAuthenticators
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetRadiusAuthClientBadAuthenticators ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetRadiusAuthClientBadAuthenticators (INT4 i4RadiusAuthServerIndex,
                                         UINT4
                                         *pu4RetValRadiusAuthClientBadAuthenticators)
#else
INT1
nmhGetRadiusAuthClientBadAuthenticators (i4RadiusAuthServerIndex,
                                         pu4RetValRadiusAuthClientBadAuthenticators)
     INT4                i4RadiusAuthServerIndex;
     UINT4              *pu4RetValRadiusAuthClientBadAuthenticators;
#endif
{
    return nmhGetFsRadExtAuthClientBadAuthenticators (i4RadiusAuthServerIndex,
                                                      pu4RetValRadiusAuthClientBadAuthenticators);
}

/****************************************************************************
 Function    :  nmhGetRadiusAuthClientPendingRequests
 Input       :  The Indices
                RadiusAuthServerIndex

                The Object 
                retValRadiusAuthClientPendingRequests
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetRadiusAuthClientPendingRequests ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetRadiusAuthClientPendingRequests (INT4 i4RadiusAuthServerIndex,
                                       UINT4
                                       *pu4RetValRadiusAuthClientPendingRequests)
#else
INT1
nmhGetRadiusAuthClientPendingRequests (i4RadiusAuthServerIndex,
                                       pu4RetValRadiusAuthClientPendingRequests)
     INT4                i4RadiusAuthServerIndex;
     UINT4              *pu4RetValRadiusAuthClientPendingRequests;
#endif
{
    return nmhGetFsRadExtAuthClientPendingRequests (i4RadiusAuthServerIndex,
                                                    pu4RetValRadiusAuthClientPendingRequests);
}

/****************************************************************************
 Function    :  nmhGetRadiusAuthClientTimeouts
 Input       :  The Indices
                RadiusAuthServerIndex

                The Object 
                retValRadiusAuthClientTimeouts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetRadiusAuthClientTimeouts ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetRadiusAuthClientTimeouts (INT4 i4RadiusAuthServerIndex,
                                UINT4 *pu4RetValRadiusAuthClientTimeouts)
#else
INT1
nmhGetRadiusAuthClientTimeouts (i4RadiusAuthServerIndex,
                                pu4RetValRadiusAuthClientTimeouts)
     INT4                i4RadiusAuthServerIndex;
     UINT4              *pu4RetValRadiusAuthClientTimeouts;
#endif
{
    return nmhGetFsRadExtAuthClientTimeouts (i4RadiusAuthServerIndex,
                                             pu4RetValRadiusAuthClientTimeouts);
}

/****************************************************************************
 Function    :  nmhGetRadiusAuthClientUnknownTypes
 Input       :  The Indices
                RadiusAuthServerIndex

                The Object 
                retValRadiusAuthClientUnknownTypes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetRadiusAuthClientUnknownTypes ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetRadiusAuthClientUnknownTypes (INT4 i4RadiusAuthServerIndex,
                                    UINT4
                                    *pu4RetValRadiusAuthClientUnknownTypes)
#else
INT1
nmhGetRadiusAuthClientUnknownTypes (i4RadiusAuthServerIndex,
                                    pu4RetValRadiusAuthClientUnknownTypes)
     INT4                i4RadiusAuthServerIndex;
     UINT4              *pu4RetValRadiusAuthClientUnknownTypes;
#endif
{
    return nmhGetFsRadExtAuthClientUnknownTypes (i4RadiusAuthServerIndex,
                                                 pu4RetValRadiusAuthClientUnknownTypes);
}

/****************************************************************************
 Function    :  nmhGetRadiusAuthClientPacketsDropped
 Input       :  The Indices
                RadiusAuthServerIndex

                The Object 
                retValRadiusAuthClientPacketsDropped
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhGetRadiusAuthClientPacketsDropped (INT4 i4RadiusAuthServerIndex,
                                      UINT4
                                      *pu4RetValRadiusAuthClientPacketsDropped)
#else
INT1
nmhGetRadiusAuthClientPacketsDropped (i4RadiusAuthServerIndex,
                                      pu4RetValRadiusAuthClientPacketsDropped)
     INT4                i4RadiusAuthServerIndex;
     UINT4              *pu4RetValRadiusAuthClientPacketsDropped;
#endif
{
    return nmhGetFsRadExtAuthClientPacketsDropped (i4RadiusAuthServerIndex,
                                                   pu4RetValRadiusAuthClientPacketsDropped);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetRadiusAuthClientInvalidServerAddresses
 Input       :  The Indices

                The Object 
                retValRadiusAuthClientInvalidServerAddresses
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhGetRadiusAuthClientInvalidServerAddresses (UINT4
                                              *pu4RetValRadiusAuthClientInvalidServerAddresses)
#else
INT1 
     
     
     
     
     
     
     
    nmhGetRadiusAuthClientInvalidServerAddresses
    (pu4RetValRadiusAuthClientInvalidServerAddresses)
     UINT4              *pu4RetValRadiusAuthClientInvalidServerAddresses;
#endif
{
    return
        nmhGetFsRadExtAuthClientInvalidServerAddresses
        (pu4RetValRadiusAuthClientInvalidServerAddresses);
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetRadiusAuthClientIdentifier
 Input       :  The Indices

                The Object 
                retValRadiusAuthClientIdentifier
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhGetRadiusAuthClientIdentifier (tSNMP_OCTET_STRING_TYPE *
                                  pRetValRadiusAuthClientIdentifier)
#else
INT1
nmhGetRadiusAuthClientIdentifier (pRetValRadiusAuthClientIdentifier)
     tSNMP_OCTET_STRING_TYPE *pRetValRadiusAuthClientIdentifier;
#endif
{
    return
        nmhGetFsRadExtAuthClientIdentifier (pRetValRadiusAuthClientIdentifier);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : GetNewRadServerIndex                               */
/*                                                                           */
/*     DESCRIPTION      : This function gets new index to store radius       */
/*                        server  entry                                      */
/*                                                                           */
/*     INPUT            : NONE.                                              */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : u4Index                                            */
/*                                                                           */
/*****************************************************************************/
UINT4
GetNewRadServerIndex (VOID)
{
    UINT4               u4RadIndex = 1;
    UINT4               u4Index;
    tRADIUS_SERVER     *ptRadiusServer;

    for (u4Index = 0; u4Index < MAX_RAD_SERVERS_LIMIT; u4Index++)
    {
        ptRadiusServer = a_gServerTable[u4Index];
        if (ptRadiusServer != NULL)
        {
            if (ptRadiusServer->ifIndex == u4RadIndex)
            {
                u4RadIndex++;
                u4Index = 0;
            }
        }
    }

    return u4RadIndex;
}
