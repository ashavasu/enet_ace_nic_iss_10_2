/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: radpropll.c,v 1.16 2018/02/14 10:04:08 siva Exp $ 
 *
 * Description: This file contains the low level routines.
 *
 *******************************************************************/
# include "lr.h"
# include "fssnmp.h"
# include "snmccons.h"

# include "radsnmp.h"
# include "radcom.h"
# include "ip.h"
#include "radcli.h"
# include "fsradecli.h"

#ifdef WLAN_WANTED
#include "ipcnp.h"
#include "wlannp.h"
#include "wlan.h"

#endif

/*******GLOBAL for Dummy index *******/
UINT4               u4TempProp;
/*******GLOBAL for Dummy index *******/

/* LOW LEVEL Routines for Table : RadiusExtServerTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceRadiusExtServerTable
 Input       :  The Indices
                RadiusExtServerIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

#ifdef __STDC__
INT1
nmhValidateIndexInstanceRadiusExtServerTable (INT4 i4RadiusExtServerIndex)
#else
INT1
nmhValidateIndexInstanceRadiusExtServerTable (i4RadiusExtServerIndex)
     INT4                i4RadiusExtServerIndex;
#endif
{
    return nmhValidateIndexInstanceFsRadExtServerTable (i4RadiusExtServerIndex);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexRadiusExtServerTable
 Input       :  The Indices
                RadiusExtServerIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

#ifdef __STDC__
INT1
nmhGetFirstIndexRadiusExtServerTable (INT4 *pi4RadiusExtServerIndex)
#else
INT1
nmhGetFirstIndexRadiusExtServerTable (pi4RadiusExtServerIndex)
     INT4               *pi4RadiusExtServerIndex;
#endif
{
    return nmhGetFirstIndexFsRadExtServerTable (pi4RadiusExtServerIndex);
}

/****************************************************************************
 Function    :  nmhGetNextIndexRadiusExtServerTable
 Input       :  The Indices
                RadiusExtServerIndex
                nextRadiusExtServerIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
#ifdef __STDC__
INT1
nmhGetNextIndexRadiusExtServerTable (INT4 i4RadiusExtServerIndex,
                                     INT4 *pi4NextRadiusExtServerIndex)
#else
INT1
nmhGetNextIndexRadiusExtServerTable (i4RadiusExtServerIndex,
                                     pi4NextRadiusExtServerIndex)
     INT4                i4RadiusExtServerIndex;
     INT4               *pi4NextRadiusExtServerIndex;
#endif
{
    return nmhGetNextIndexFsRadExtServerTable (i4RadiusExtServerIndex,
                                               pi4NextRadiusExtServerIndex);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetRadiusExtServerAddress
 Input       :  The Indices
                RadiusExtServerIndex

                The Object 
                retValRadiusExtServerAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhGetRadiusExtServerAddress (INT4 i4RadiusExtServerIndex,
                              UINT4 *pu4RetValRadiusExtServerAddress)
#else
INT1
nmhGetRadiusExtServerAddress (i4RadiusExtServerIndex,
                              pu4RetValRadiusExtServerAddress)
     INT4                i4RadiusExtServerIndex;
     UINT4              *pu4RetValRadiusExtServerAddress;
#endif
{
    INT4                i4AddrType = 0;
    tSNMP_OCTET_STRING_TYPE ServerIpAddress;
    UINT1               au1TmpArray[IPVX_IPV6_ADDR_LEN];

    ServerIpAddress.pu1_OctetList = au1TmpArray;

    nmhGetFsRadExtServerAddrType (i4RadiusExtServerIndex, &i4AddrType);

    if (i4AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        nmhGetFsRadExtServerAddress (i4RadiusExtServerIndex, &ServerIpAddress);

        MEMCPY (pu4RetValRadiusExtServerAddress,
                ServerIpAddress.pu1_OctetList, IPVX_IPV4_ADDR_LEN);

        *pu4RetValRadiusExtServerAddress =
            OSIX_HTONL (*pu4RetValRadiusExtServerAddress);
    }
    else
    {
        *pu4RetValRadiusExtServerAddress = 0;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetRadiusExtServerType
 Input       :  The Indices
                RadiusExtServerIndex

                The Object 
                retValRadiusExtServerType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhGetRadiusExtServerType (INT4 i4RadiusExtServerIndex,
                           INT4 *pi4RetValRadiusExtServerType)
#else
INT1
nmhGetRadiusExtServerType (i4RadiusExtServerIndex, pi4RetValRadiusExtServerType)
     INT4                i4RadiusExtServerIndex;
     INT4               *pi4RetValRadiusExtServerType;
#endif
{
    return nmhGetFsRadExtServerType (i4RadiusExtServerIndex,
                                     pi4RetValRadiusExtServerType);
}

/****************************************************************************
 Function    :  nmhGetRadiusExtServerSharedSecret
 Input       :  The Indices
                RadiusExtServerIndex

                The Object 
                retValRadiusExtServerSharedSecret
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhGetRadiusExtServerSharedSecret (INT4 i4RadiusExtServerIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pRetValRadiusExtServerSharedSecret)
#else
INT1
nmhGetRadiusExtServerSharedSecret (i4RadiusExtServerIndex,
                                   pRetValRadiusExtServerSharedSecret)
     INT4                i4RadiusExtServerIndex;
     tSNMP_OCTET_STRING_TYPE *pRetValRadiusExtServerSharedSecret;
#endif
{
    return nmhGetFsRadExtServerSharedSecret (i4RadiusExtServerIndex,
                                             pRetValRadiusExtServerSharedSecret);
}

/****************************************************************************
 Function    :  nmhGetRadiusExtServerEnabled
 Input       :  The Indices
                RadiusExtServerIndex

                The Object 
                retValRadiusExtServerEnabled
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhGetRadiusExtServerEnabled (INT4 i4RadiusExtServerIndex,
                              INT4 *pi4RetValRadiusExtServerEnabled)
#else
INT1
nmhGetRadiusExtServerEnabled (i4RadiusExtServerIndex,
                              pi4RetValRadiusExtServerEnabled)
     INT4                i4RadiusExtServerIndex;
     INT4               *pi4RetValRadiusExtServerEnabled;
#endif
{
    return nmhGetFsRadExtServerEnabled (i4RadiusExtServerIndex,
                                        pi4RetValRadiusExtServerEnabled);
}

/****************************************************************************
 Function    :  nmhGetRadiusExtServerResponseTime
 Input       :  The Indices
                RadiusExtServerIndex

                The Object 
                retValRadiusExtServerResponseTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhGetRadiusExtServerResponseTime (INT4 i4RadiusExtServerIndex,
                                   INT4 *pi4RetValRadiusExtServerResponseTime)
#else
INT1
nmhGetRadiusExtServerResponseTime (i4RadiusExtServerIndex,
                                   pi4RetValRadiusExtServerResponseTime)
     INT4                i4RadiusExtServerIndex;
     INT4               *pi4RetValRadiusExtServerResponseTime;
#endif
{
    return nmhGetFsRadExtServerResponseTime (i4RadiusExtServerIndex,
                                             pi4RetValRadiusExtServerResponseTime);
}

/****************************************************************************
 Function    :  nmhGetRadiusExtServerMaximumRetransmission
 Input       :  The Indices
                RadiusExtServerIndex

                The Object 
                retValRadiusExtServerMaximumRetransmission
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhGetRadiusExtServerMaximumRetransmission (INT4 i4RadiusExtServerIndex,
                                            INT4
                                            *pi4RetValRadiusExtServerMaximumRetransmission)
#else
INT1
nmhGetRadiusExtServerMaximumRetransmission (i4RadiusExtServerIndex,
                                            pi4RetValRadiusExtServerMaximumRetransmission)
     INT4                i4RadiusExtServerIndex;
     INT4               *pi4RetValRadiusExtServerMaximumRetransmission;
#endif
{
    return nmhGetFsRadExtServerMaximumRetransmission (i4RadiusExtServerIndex,
                                                      pi4RetValRadiusExtServerMaximumRetransmission);
}

/****************************************************************************
 Function    :  nmhGetRadiusExtServerEntryStatus
 Input       :  The Indices
                RadiusExtServerIndex

                The Object 
                retValRadiusExtServerEntryStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhGetRadiusExtServerEntryStatus (INT4 i4RadiusExtServerIndex,
                                  INT4 *pi4RetValRadiusExtServerEntryStatus)
#else
INT1
nmhGetRadiusExtServerEntryStatus (i4RadiusExtServerIndex,
                                  pi4RetValRadiusExtServerEntryStatus)
     INT4                i4RadiusExtServerIndex;
     INT4               *pi4RetValRadiusExtServerEntryStatus;
#endif
{
    return nmhGetFsRadExtServerEntryStatus (i4RadiusExtServerIndex,
                                            pi4RetValRadiusExtServerEntryStatus);
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetRadiusExtServerAddress
 Input       :  The Indices
                RadiusExtServerIndex

                The Object 
                setValRadiusExtServerAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhSetRadiusExtServerAddress (INT4 i4RadiusExtServerIndex,
                              UINT4 u4SetValRadiusExtServerAddress)
#else
INT1
nmhSetRadiusExtServerAddress (i4RadiusExtServerIndex,
                              u4SetValRadiusExtServerAddress)
     INT4                i4RadiusExtServerIndex;
     UINT4               u4SetValRadiusExtServerAddress;

#endif
{
    tSNMP_OCTET_STRING_TYPE ServerIpAddress;
    UINT1               au1TmpArray[IPVX_IPV6_ADDR_LEN];
    ServerIpAddress.pu1_OctetList = au1TmpArray;

    if (nmhSetFsRadExtServerAddrType (i4RadiusExtServerIndex,
                                      IPVX_ADDR_FMLY_IPV4) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    /* Copy the UINT4 Ip Address into the octet list */
    u4SetValRadiusExtServerAddress =
        OSIX_NTOHL (u4SetValRadiusExtServerAddress);

    MEMSET (ServerIpAddress.pu1_OctetList, 0, IPVX_IPV6_ADDR_LEN);
    MEMCPY (ServerIpAddress.pu1_OctetList, &u4SetValRadiusExtServerAddress,
            IPVX_IPV4_ADDR_LEN);
    ServerIpAddress.i4_Length = IPVX_IPV4_ADDR_LEN;

    return nmhSetFsRadExtServerAddress (i4RadiusExtServerIndex,
                                        &ServerIpAddress);
}

/****************************************************************************
 Function    :  nmhSetRadiusExtServerType
 Input       :  The Indices
                RadiusExtServerIndex

                The Object 
                setValRadiusExtServerType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhSetRadiusExtServerType (INT4 i4RadiusExtServerIndex,
                           INT4 i4SetValRadiusExtServerType)
#else
INT1
nmhSetRadiusExtServerType (i4RadiusExtServerIndex, i4SetValRadiusExtServerType)
     INT4                i4RadiusExtServerIndex;
     INT4                i4SetValRadiusExtServerType;

#endif
{
    return nmhSetFsRadExtServerType (i4RadiusExtServerIndex,
                                     i4SetValRadiusExtServerType);
}

/****************************************************************************
 Function    :  nmhSetRadiusExtServerSharedSecret
 Input       :  The Indices
                RadiusExtServerIndex

                The Object 
                setValRadiusExtServerSharedSecret
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhSetRadiusExtServerSharedSecret (INT4 i4RadiusExtServerIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pSetValRadiusExtServerSharedSecret)
#else
INT1
nmhSetRadiusExtServerSharedSecret (i4RadiusExtServerIndex,
                                   pSetValRadiusExtServerSharedSecret)
     INT4                i4RadiusExtServerIndex;
     tSNMP_OCTET_STRING_TYPE *pSetValRadiusExtServerSharedSecret;

#endif
{
    return nmhSetFsRadExtServerSharedSecret (i4RadiusExtServerIndex,
                                             pSetValRadiusExtServerSharedSecret);
}

/****************************************************************************
 Function    :  nmhSetRadiusExtServerEnabled
 Input       :  The Indices
                RadiusExtServerIndex

                The Object 
                setValRadiusExtServerEnabled
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhSetRadiusExtServerEnabled (INT4 i4RadiusExtServerIndex,
                              INT4 i4SetValRadiusExtServerEnabled)
#else
INT1
nmhSetRadiusExtServerEnabled (i4RadiusExtServerIndex,
                              i4SetValRadiusExtServerEnabled)
     INT4                i4RadiusExtServerIndex;
     INT4                i4SetValRadiusExtServerEnabled;

#endif
{
    return nmhSetFsRadExtServerEnabled (i4RadiusExtServerIndex,
                                        i4SetValRadiusExtServerEnabled);
}

/****************************************************************************
 Function    :  nmhSetRadiusExtServerResponseTime
 Input       :  The Indices
                RadiusExtServerIndex

                The Object 
                setValRadiusExtServerResponseTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhSetRadiusExtServerResponseTime (INT4 i4RadiusExtServerIndex,
                                   INT4 i4SetValRadiusExtServerResponseTime)
#else
INT1
nmhSetRadiusExtServerResponseTime (i4RadiusExtServerIndex,
                                   i4SetValRadiusExtServerResponseTime)
     INT4                i4RadiusExtServerIndex;
     INT4                i4SetValRadiusExtServerResponseTime;

#endif
{
    return nmhSetFsRadExtServerResponseTime (i4RadiusExtServerIndex,
                                             i4SetValRadiusExtServerResponseTime);
}

/****************************************************************************
 Function    :  nmhSetRadiusExtServerMaximumRetransmission
 Input       :  The Indices
                RadiusExtServerIndex

                The Object 
                setValRadiusExtServerMaximumRetransmission
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhSetRadiusExtServerMaximumRetransmission (INT4 i4RadiusExtServerIndex,
                                            INT4
                                            i4SetValRadiusExtServerMaximumRetransmission)
#else
INT1
nmhSetRadiusExtServerMaximumRetransmission (i4RadiusExtServerIndex,
                                            i4SetValRadiusExtServerMaximumRetransmission)
     INT4                i4RadiusExtServerIndex;
     INT4                i4SetValRadiusExtServerMaximumRetransmission;

#endif
{
    return nmhSetFsRadExtServerMaximumRetransmission (i4RadiusExtServerIndex,
                                                      i4SetValRadiusExtServerMaximumRetransmission);
}

/****************************************************************************
 Function    :  nmhSetRadiusExtServerEntryStatus
 Input       :  The Indices
                RadiusExtServerIndex

                The Object 
                setValRadiusExtServerEntryStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhSetRadiusExtServerEntryStatus (INT4 i4RadiusExtServerIndex,
                                  INT4 i4SetValRadiusExtServerEntryStatus)
#else
INT1
nmhSetRadiusExtServerEntryStatus (i4RadiusExtServerIndex,
                                  i4SetValRadiusExtServerEntryStatus)
     INT4                i4RadiusExtServerIndex;
     INT4                i4SetValRadiusExtServerEntryStatus;

#endif
{
    return nmhSetFsRadExtServerEntryStatus (i4RadiusExtServerIndex,
                                            i4SetValRadiusExtServerEntryStatus);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2RadiusExtServerAddress
 Input       :  The Indices
                RadiusExtServerIndex

                The Object 
                testValRadiusExtServerAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhTestv2RadiusExtServerAddress (UINT4 *pu4ErrorCode,
                                 INT4 i4RadiusExtServerIndex,
                                 UINT4 u4TestValRadiusExtServerAddress)
#else
INT1
nmhTestv2RadiusExtServerAddress (*pu4ErrorCode, i4RadiusExtServerIndex,
                                 u4TestValRadiusExtServerAddress)
     UINT4              *pu4ErrorCode;
     INT4                i4RadiusExtServerIndex;
     UINT4               u4TestValRadiusExtServerAddress;
#endif
{
    tRADIUS_SERVER     *ptRadiusServer = NULL;
    tIPvXAddr           TestServerAddress;
    UINT4               u4ServerId = 0;
    INT4                i4RetServIndex = 0;

    u4TestValRadiusExtServerAddress =
        OSIX_NTOHL (u4TestValRadiusExtServerAddress);

    IPVX_ADDR_INIT_IPV4 (TestServerAddress, IPVX_ADDR_FMLY_IPV4,
                         (VOID *) &u4TestValRadiusExtServerAddress);

    if ((u4TestValRadiusExtServerAddress == 0x00000000) ||
        (u4TestValRadiusExtServerAddress == 0xffffffff))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    /*Checks whether IP belongs to Local Interface .
       If RADIUS PROXY is to be activated in the future this should be taken into      account */

    if (NetIpv4IfIsOurAddress (u4TestValRadiusExtServerAddress) ==
        NETIPV4_SUCCESS)
    {
        CLI_SET_ERR (CLI_RAD_INVALID_SERVER_IP);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    ptRadiusServer = RadGetServerEntry (i4RadiusExtServerIndex, &u4ServerId);

    if (ptRadiusServer == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (ACTIVE == ptRadiusServer->Status)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    i4RetServIndex = RadGetServerIndex (&TestServerAddress);
    /* Check if the same ip address is configured for any other entry */
    if ((RAD_ERROR != i4RetServIndex)
        && (i4RetServIndex != i4RadiusExtServerIndex))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2RadiusExtServerType
 Input       :  The Indices
                RadiusExtServerIndex

                The Object 
                testValRadiusExtServerType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhTestv2RadiusExtServerType (UINT4 *pu4ErrorCode, INT4 i4RadiusExtServerIndex,
                              INT4 i4TestValRadiusExtServerType)
#else
INT1
nmhTestv2RadiusExtServerType (*pu4ErrorCode, i4RadiusExtServerIndex,
                              i4TestValRadiusExtServerType)
     UINT4              *pu4ErrorCode;
     INT4                i4RadiusExtServerIndex;
     INT4                i4TestValRadiusExtServerType;
#endif
{
    return nmhTestv2FsRadExtServerType (pu4ErrorCode, i4RadiusExtServerIndex,
                                        i4TestValRadiusExtServerType);
}

/****************************************************************************
 Function    :  nmhTestv2RadiusExtServerSharedSecret
 Input       :  The Indices
                RadiusExtServerIndex

                The Object 
                testValRadiusExtServerSharedSecret
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhTestv2RadiusExtServerSharedSecret (UINT4 *pu4ErrorCode,
                                      INT4 i4RadiusExtServerIndex,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pTestValRadiusExtServerSharedSecret)
#else
INT1
nmhTestv2RadiusExtServerSharedSecret (*pu4ErrorCode, i4RadiusExtServerIndex,
                                      pTestValRadiusExtServerSharedSecret)
     UINT4              *pu4ErrorCode;
     INT4                i4RadiusExtServerIndex;
     tSNMP_OCTET_STRING_TYPE *pTestValRadiusExtServerSharedSecret;
#endif
{
    return nmhTestv2FsRadExtServerSharedSecret (pu4ErrorCode,
                                                i4RadiusExtServerIndex,
                                                pTestValRadiusExtServerSharedSecret);
}

/****************************************************************************
 Function    :  nmhTestv2RadiusExtServerEnabled
 Input       :  The Indices
                RadiusExtServerIndex

                The Object 
                testValRadiusExtServerEnabled
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhTestv2RadiusExtServerEnabled (UINT4 *pu4ErrorCode,
                                 INT4 i4RadiusExtServerIndex,
                                 INT4 i4TestValRadiusExtServerEnabled)
#else
INT1
nmhTestv2RadiusExtServerEnabled (*pu4ErrorCode, i4RadiusExtServerIndex,
                                 i4TestValRadiusExtServerEnabled)
     UINT4              *pu4ErrorCode;
     INT4                i4RadiusExtServerIndex;
     INT4                i4TestValRadiusExtServerEnabled;
#endif
{
    return nmhTestv2FsRadExtServerEnabled (pu4ErrorCode, i4RadiusExtServerIndex,
                                           i4TestValRadiusExtServerEnabled);
}

/****************************************************************************
 Function    :  nmhTestv2RadiusExtServerResponseTime
 Input       :  The Indices
                RadiusExtServerIndex

                The Object 
                testValRadiusExtServerResponseTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhTestv2RadiusExtServerResponseTime (UINT4 *pu4ErrorCode,
                                      INT4 i4RadiusExtServerIndex,
                                      INT4 i4TestValRadiusExtServerResponseTime)
#else
INT1
nmhTestv2RadiusExtServerResponseTime (*pu4ErrorCode, i4RadiusExtServerIndex,
                                      i4TestValRadiusExtServerResponseTime)
     UINT4              *pu4ErrorCode;
     INT4                i4RadiusExtServerIndex;
     INT4                i4TestValRadiusExtServerResponseTime;
#endif
{
    return nmhTestv2FsRadExtServerResponseTime (pu4ErrorCode,
                                                i4RadiusExtServerIndex,
                                                i4TestValRadiusExtServerResponseTime);
}

/****************************************************************************
 Function    :  nmhTestv2RadiusExtServerMaximumRetransmission
 Input       :  The Indices
                RadiusExtServerIndex

                The Object 
                testValRadiusExtServerMaximumRetransmission
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhTestv2RadiusExtServerMaximumRetransmission (UINT4 *pu4ErrorCode,
                                               INT4 i4RadiusExtServerIndex,
                                               INT4
                                               i4TestValRadiusExtServerMaximumRetransmission)
#else
INT1
nmhTestv2RadiusExtServerMaximumRetransmission (*pu4ErrorCode,
                                               i4RadiusExtServerIndex,
                                               i4TestValRadiusExtServerMaximumRetransmission)
     UINT4              *pu4ErrorCode;
     INT4                i4RadiusExtServerIndex;
     INT4                i4TestValRadiusExtServerMaximumRetransmission;
#endif
{
    return nmhTestv2FsRadExtServerMaximumRetransmission (pu4ErrorCode,
                                                         i4RadiusExtServerIndex,
                                                         i4TestValRadiusExtServerMaximumRetransmission);
}

/****************************************************************************
 Function    :  nmhTestv2RadiusExtServerEntryStatus
 Input       :  The Indices
                RadiusExtServerIndex

                The Object 
                testValRadiusExtServerEntryStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhTestv2RadiusExtServerEntryStatus (UINT4 *pu4ErrorCode,
                                     INT4 i4RadiusExtServerIndex,
                                     INT4 i4TestValRadiusExtServerEntryStatus)
#else
INT1
nmhTestv2RadiusExtServerEntryStatus (*pu4ErrorCode, i4RadiusExtServerIndex,
                                     i4TestValRadiusExtServerEntryStatus)
     UINT4              *pu4ErrorCode;
     INT4                i4RadiusExtServerIndex;
     INT4                i4TestValRadiusExtServerEntryStatus;
#endif
{
    return nmhTestv2FsRadExtServerEntryStatus (pu4ErrorCode,
                                               i4RadiusExtServerIndex,
                                               i4TestValRadiusExtServerEntryStatus);
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2RadiusExtServerTable
 Input       :  The Indices
                RadiusExtServerIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2RadiusExtServerTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetRadiusExtDebugMask
 Input       :  The Indices

                The Object 
                retValRadiusExtDebugMask
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhGetRadiusExtDebugMask (INT4 *pi4RetValRadiusExtDebugMask)
#else
INT1
nmhGetRadiusExtDebugMask (pi4RetValRadiusExtDebugMask)
     INT4               *pi4RetValRadiusExtDebugMask;
#endif
{
    return nmhGetFsRadExtDebugMask (pi4RetValRadiusExtDebugMask);
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetRadiusExtDebugMask
 Input       :  The Indices

                The Object 
                setValRadiusExtDebugMask
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhSetRadiusExtDebugMask (INT4 i4SetValRadiusExtDebugMask)
#else
INT1
nmhSetRadiusExtDebugMask (i4SetValRadiusExtDebugMask)
     INT4                i4SetValRadiusExtDebugMask;

#endif
{
    return nmhSetFsRadExtDebugMask (i4SetValRadiusExtDebugMask);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2RadiusExtDebugMask
 Input       :  The Indices

                The Object 
                testValRadiusExtDebugMask
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhTestv2RadiusExtDebugMask (UINT4 *pu4ErrorCode,
                             INT4 i4TestValRadiusExtDebugMask)
#else
INT1
nmhTestv2RadiusExtDebugMask (*pu4ErrorCode, i4TestValRadiusExtDebugMask)
     UINT4              *pu4ErrorCode;
     INT4                i4TestValRadiusExtDebugMask;
#endif
{
    return nmhTestv2FsRadExtDebugMask (pu4ErrorCode,
                                       i4TestValRadiusExtDebugMask);
}

/****************************************************************************
 Function    :  nmhGetRadiusMaxNoOfUserEntries
 Input       :  The Indices

                The Object 
                retValRadiusMaxNoOfUserEntries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhGetRadiusMaxNoOfUserEntries (INT4 *pi4RetValRadiusMaxNoOfUserEntries)
#else
INT1
nmhGetRadiusMaxNoOfUserEntries (pi4RetValRadiusMaxNoOfUserEntries)
     INT4               *pi4RetValRadiusMaxNoOfUserEntries;
#endif
{
    return nmhGetFsRadExtMaxNoOfUserEntries (pi4RetValRadiusMaxNoOfUserEntries);
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetRadiusMaxNoOfUserEntries
 Input       :  The Indices

                The Object 
                setValRadiusMaxNoOfUserEntries
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhSetRadiusMaxNoOfUserEntries (INT4 i4SetValRadiusMaxNoOfUserEntries)
#else
INT1
nmhSetRadiusMaxNoOfUserEntries (i4SetValRadiusMaxNoOfUserEntries)
     INT4                i4SetValRadiusMaxNoOfUserEntries;

#endif
{
    return nmhSetFsRadExtMaxNoOfUserEntries (i4SetValRadiusMaxNoOfUserEntries);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2RadiusMaxNoOfUserEntries
 Input       :  The Indices

                The Object 
                testValRadiusMaxNoOfUserEntries
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhTestv2RadiusMaxNoOfUserEntries (UINT4 *pu4ErrorCode,
                                   INT4 i4TestValRadiusMaxNoOfUserEntries)
#else
INT1
nmhTestv2RadiusMaxNoOfUserEntries (*pu4ErrorCode,
                                   i4TestValRadiusMaxNoOfUserEntries)
     UINT4              *pu4ErrorCode;
     INT4                i4TestValRadiusMaxNoOfUserEntries;
#endif
{
    return nmhTestv2FsRadExtMaxNoOfUserEntries (pu4ErrorCode,
                                                i4TestValRadiusMaxNoOfUserEntries);
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2RadiusExtDebugMask
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2RadiusExtDebugMask (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2RadiusMaxNoOfUserEntries
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2RadiusMaxNoOfUserEntries (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
