/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: radrecv.c,v 1.23 2018/02/14 10:04:08 siva Exp $ 
 *
 * Description: This file contains all the module for 
 *              taking action on a received radius 
 *              packet form the server.              
 *******************************************************************/
#include   "radcom.h"
#include   "arHmac_api.h"
#include   "arMD5_inc.h"
#include   "arMD5_api.h"
#include   "utilalgo.h"
PRIVATE UINT1      *RadGetVendorAttr (UINT1 *pu1VendorSubtypePos,
                                      UINT1 u1SubType, UINT4 *pu4AttrLen);
PRIVATE VOID        RadUtilHexDump (UINT1 *pu1Bytes, UINT4 u4Len);
PRIVATE UINT1      *RadProxyGetVendorAttr (UINT1 *pu1VendorSubtypePos,
                                           UINT1 u1SubType, UINT4 *pu4AttrLen);
UINT4               UTLInetAddr (const char *);
UINT1               ga_u1ProxyClientSecret[LEN_SECRET];
/*----------------------------------------------------------------------------
Procedure    : radiusTakeAction()
Description    : This module Validates the received Radius packet.
              The validation of packet is done in two steps. The validation
              of the whole packet and the valildation of the attributes.
              After the validation the packe is processed for the
              attributes.
Input(s)    : p_u1RadiusReceivedPacket
              p_gRadiusUserQFirstEntry (globally declared)
Output(s)    : None
Returns        : 0, if the packet validation passess
              -1, if the packet validation fails
Called By    : The User 
Calling        : radiusValidatePacket()
              radiusValidateAttributes()
              radiusProcessPacket();
Author        : G.Vedavinayagam
Date        : 11/13/97
odifications    :
-----------------------------------------------------------------------------*/
#ifdef __STDC__
INT1
radiusTakeAction (UINT1 *p_u1RadiusReceivedPacket)
#else
INT1
radiusTakeAction (p_u1RadiusReceivedPacket)
     UINT1              *p_u1RadiusReceivedPacket;
#endif
{
    tRADIUS_REQ_ENTRY  *p_RadiusRequest = NULL;
    tTmrAppTimer       *p_AppTimer = NULL;
    UINT1               u1_pktId = 0;

    if (radiusValidatePacket (p_u1RadiusReceivedPacket, &p_RadiusRequest)
        == NOT_OK)
    {
        RAD_ERR_PRINT ("\nValidation of the Received packet failed\n");
        return (NOT_OK);
    }

    if (radiusValidateAttributes (p_u1RadiusReceivedPacket, &p_RadiusRequest)
        == NOT_OK)
    {

        u1_pktId = *(p_u1RadiusReceivedPacket + PKT_ID);
        p_AppTimer = a_gUserTable[u1_pktId];
        if (p_AppTimer)
        {
            p_AppTimer->u4Data = 0;
        }
        RAD_ERR_PRINT
            ("\nValidation of the Attributes in the Received packet failed\n");
        p_RadiusRequest->ptRadiusServer->u4_PktsDropped++;
        return (NOT_OK);
    }

    radiusProcessPacket (p_u1RadiusReceivedPacket, &p_RadiusRequest);
    return (OK);
}

/*-----------------------------------------------------------------------------
Porcedure    : radiusValidatePacket()
Description    : This module validate the received radius packet. The packet
              identifier, the length of the packet are validated. Then 
              response authenticator is validated.
Input(s)    : p_u1RadiusReceivedPacket
            : p_gRadiusUserQFirstEntry (globally declared)
Output(s)    : p_RadiusRequest 
Returns        : 0, if the validation passes
              -1, if the validation fails
Called By    : radiusTakeAction()
Calling        : radiusValidateIdentifier()
              radiusMakeResAuth()
              radiusValidateResAuth()
Author        : G.Vedavinayagam
Date        : 11/13/97
odifications    :
-----------------------------------------------------------------------------*/
#ifdef __STDC__
INT1
radiusValidatePacket (UINT1 *p_u1RadiusReceivedPacket,
                      tRADIUS_REQ_ENTRY ** p_RadiusRequest)
#else
INT1
radiusValidatePacket (p_u1RadiusReceivedPacket, p_RadiusRequest)
     UINT1              *p_u1RadiusReceivedPacket;
     tRADIUS_REQ_ENTRY **p_RadiusRequest;
#endif
{

    UINT1               a_u1ResponseAuth[LEN_RES_AUTH] = { 0 };
    UINT1               u1_pktId = 0;
    tTmrAppTimer       *p_AppTimer = NULL;
    tRADIUS_REQ_ENTRY  *p_CurrentRadiusReq = NULL;
    tRadInterface      *pInterface = NULL;

    u1_pktId = *(p_u1RadiusReceivedPacket + PKT_ID);
    p_AppTimer = a_gUserTable[u1_pktId];
    p_CurrentRadiusReq = NULL;

    MEMSET (a_u1ResponseAuth, 0, LEN_RES_AUTH);
    if (p_AppTimer)
        p_CurrentRadiusReq = (tRADIUS_REQ_ENTRY *) p_AppTimer->u4Data;

    if (radiusValidateIdentifier (p_u1RadiusReceivedPacket, p_RadiusRequest)
        == NOT_OK)
    {
        return (NOT_OK);
    }
    radiusMakeResAuth (p_u1RadiusReceivedPacket, p_RadiusRequest,
                       a_u1ResponseAuth);
    if (radiusValidateResAuth
        (p_u1RadiusReceivedPacket, a_u1ResponseAuth, p_RadiusRequest) == NOT_OK)
    {

        if (p_AppTimer)
        {
            /* Free will be done by CLI module */
            pInterface =
                (tRadInterface *) (VOID *) UtlShMemAllocRadInterface ();
            if (pInterface != NULL)
            {
                RadInitInterface (pInterface);
                pInterface->TaskId = (*p_RadiusRequest)->userInfoAuth.TaskId;
                (*p_RadiusRequest)->ptRadiusServer->u4_AccessRejects++;
                RAD_EVENT_PRINT ("\n Received ACCESS_REJECT.\n");

                pInterface->Access = ACCESS_REJECT;
                (*((*p_RadiusRequest)->CallBackfPtr)) ((VOID *) pInterface);
            }

            p_AppTimer->u4Data = 0;
            radiusReleasePacket (gRadMemPoolId.u4PacketPoolId,
                                 p_CurrentRadiusReq->p_u1PacketTransmitted);
            radiusReleaseUserEntry (gRadMemPoolId.u4UserEntryPoolId,
                                    (UINT1 *) p_CurrentRadiusReq);
        }
        RAD_EVENT_PRINT
            ("\nReceived packet with WRONG response authenticator\n");
        return (NOT_OK);
    }

    /* releasing of userentry moved from validate 
     * identifier*/

    u1_pktId = *(p_u1RadiusReceivedPacket + PKT_ID);
    p_AppTimer = a_gUserTable[u1_pktId];
    p_CurrentRadiusReq = NULL;
    if (p_AppTimer)
    {
        p_CurrentRadiusReq = (tRADIUS_REQ_ENTRY *) p_AppTimer->u4Data;
        if (p_CurrentRadiusReq)
        {
            p_CurrentRadiusReq->u1_ReTxCount = 0;
            if (rad_tmrh_stop_timer (p_AppTimer) == NOT_OK)
            {
                RAD_TIMER_PRINT ("\nError in Stopping TIMER");
                p_gRadiusErrorLog->u4_ErrorTimerNotStoped++;
            }
        }
    }

    return (OK);
}

/*-----------------------------------------------------------------------------
Procedure    : radiusValidateIdentifier()
Description    : The module checks the identifier of the received whether it
              matches with any of the requests present in the radius
              request queue. If it is not matching with any of the user
              requests, it returns -1. If it is matching with one of the 
              user requests, it validates the lenght of the packet. If the
              length validation fails it returns -1. If the length is valid
              it returns 0.
Input(s)    : p_u1RadiusReceivedPacket 
Output(s)    : p_RadiusRequest
Return(s)    : 0, if the validation passes
              -1, if the validation fails
Called By    : radiusValidatePacket()
Calling        : None
Author        : G.Vedavinayagam
Date        : 11/13/97
odifications    :
-----------------------------------------------------------------------------*/
#ifdef __STDC__
INT1
radiusValidateIdentifier (UINT1 *p_u1RadiusReceivedPacket,
                          tRADIUS_REQ_ENTRY ** p_UserRequest)
#else
INT1
radiusValidateIdentifier (p_u1RadiusReceivedPacket, p_UserRequest)
     UINT1              *p_u1RadiusReceivedPacket;
     tRADIUS_REQ_ENTRY **p_UserRequest;
#endif
{
    UINT1               u1_pktId = 0;
    UINT2               u2_pktlen = 0;
    tRADIUS_REQ_ENTRY  *p_CurrentRadiusReq = NULL;
    tRADIUS_SERVER     *p_RadiusServer = NULL;
    tTmrAppTimer       *p_AppTimer = NULL;

    u1_pktId = *(p_u1RadiusReceivedPacket + PKT_ID);

    p_AppTimer = a_gUserTable[u1_pktId];
    p_CurrentRadiusReq = NULL;
    if (p_AppTimer)
        p_CurrentRadiusReq = (tRADIUS_REQ_ENTRY *) p_AppTimer->u4Data;

    if (p_CurrentRadiusReq == NULL)
    {
        p_gRadiusErrorLog->u4_ErrorPacketIdentifier++;
        return (NOT_OK);
    }

    if (p_CurrentRadiusReq->ptRadiusServer == NULL)
    {
        p_gRadiusErrorLog->u4_ErrorPacketIdentifier++;
        p_AppTimer->u4Data = 0;
        if (p_CurrentRadiusReq->p_u1PacketTransmitted != NULL)
        {
            radiusReleasePacket (gRadMemPoolId.u4PacketPoolId,
                                 p_CurrentRadiusReq->p_u1PacketTransmitted);
        }
        radiusReleaseUserEntry (gRadMemPoolId.u4UserEntryPoolId,
                                (UINT1 *) p_CurrentRadiusReq);
        return (NOT_OK);
    }

    if (p_CurrentRadiusReq->p_u1PacketTransmitted == NULL)
    {
        p_gRadiusErrorLog->u4_ErrorPacketIdentifier++;
        p_AppTimer->u4Data = 0;
        radiusReleaseUserEntry (gRadMemPoolId.u4UserEntryPoolId,
                                (UINT1 *) p_CurrentRadiusReq);
        return (NOT_OK);
    }

    p_RadiusServer = p_CurrentRadiusReq->ptRadiusServer;

    if (*(p_u1RadiusReceivedPacket + PKT_TYPE) != ACCESS_ACCEPT &&
        *(p_u1RadiusReceivedPacket + PKT_TYPE) != ACCESS_REJECT &&
        *(p_u1RadiusReceivedPacket + PKT_TYPE) != ACCESS_CHALLENGE &&
        *(p_u1RadiusReceivedPacket + PKT_TYPE) != ACCOUNTING_RESPONSE)
    {
        p_RadiusServer->u4_UnknownTypes++;
        p_AppTimer->u4Data = 0;
        radiusReleasePacket (gRadMemPoolId.u4PacketPoolId,
                             p_CurrentRadiusReq->p_u1PacketTransmitted);
        radiusReleaseUserEntry (gRadMemPoolId.u4UserEntryPoolId,
                                (UINT1 *) p_CurrentRadiusReq);
        RAD_ERR_PRINT ("\n Received Packet of Unknown Type !.\n");
        return (NOT_OK);
    }

    *p_UserRequest = p_CurrentRadiusReq;

    MEMCPY (&u2_pktlen, p_u1RadiusReceivedPacket + PKT_LEN, 2);
    u2_pktlen = OSIX_NTOHS (u2_pktlen);

    if (u2_pktlen < LEN_PKT_MIN || u2_pktlen > LEN_PKT_MAX)
    {
        p_gRadiusErrorLog->u4_ErrorPacketLength++;
        if (*(p_u1RadiusReceivedPacket + PKT_TYPE) == ACCESS_ACCEPT ||
            *(p_u1RadiusReceivedPacket + PKT_TYPE) == ACCESS_REJECT ||
            *(p_u1RadiusReceivedPacket + PKT_TYPE) == ACCESS_CHALLENGE)
        {
            p_RadiusServer->u4_MalAccessResps++;
        }

        if (*(p_u1RadiusReceivedPacket + PKT_TYPE) == ACCOUNTING_RESPONSE)
            p_RadiusServer->u4_MalAcctAccessResps++;

        p_AppTimer->u4Data = 0;
        radiusReleasePacket (gRadMemPoolId.u4PacketPoolId,
                             p_CurrentRadiusReq->p_u1PacketTransmitted);
        radiusReleaseUserEntry (gRadMemPoolId.u4UserEntryPoolId,
                                (UINT1 *) p_CurrentRadiusReq);
        return (NOT_OK);
    }

    return (OK);
}

/*---------------------------------------------------------------------------
Procedure    : radiusMakeResAuth()
Description    : This module makes the reponse authenticator from the packet
              received and the packet transmited corresponding to the
              received packet. This module concatenates Code, Identifier,
              Length, Request authentiator transmitted, attributes and the
              shared secret. The digest value of the concatenated string is
              the Response authenticator.
Input(s)    : p_u1RadiusReceivedPacket
              p_RadiusRequest
Output(s)    : a_u1ResponseAuth
Returns        : None
Called By    : radiusValidatePacket()
Calling        : radiusMakeStringVal()
              RADIUS_MESSAGE_DIGEST()
Author        : G.Vedavinayagam
Date        : 11/13/97
odifications    :
-----------------------------------------------------------------------------*/
#ifdef __STDC__
VOID
radiusMakeResAuth (UINT1 *p_u1RadiusReceivedPacket,
                   tRADIUS_REQ_ENTRY ** p_UserRequest, UINT1 a_u1ResponseAuth[])
#else
VOID
radiusMakeResAuth (p_u1RadiusReceivedPacket, p_UserRequest, a_u1ResponseAuth)
     UINT1              *p_u1RadiusReceivedPacket;
     tRADIUS_REQ_ENTRY **p_UserRequest;
     UINT1               a_u1ResponseAuth[];
#endif
{
    UINT1              *pa_u1Concatenated = NULL;
    INT4                i4_StrLen = 0;

    pa_u1Concatenated = MemAllocMemBlk (gRadMemPoolId.u4RadConcatPoolId);

    if (NULL == pa_u1Concatenated)
    {
        RAD_ERR_PRINT ("Error in allocating memory for pa_u1Concatenated\n");
        return;
    }
    MEMSET (pa_u1Concatenated, 0, sizeof (tu1Concatenated));
    radiusMakeStringVal (p_u1RadiusReceivedPacket, p_UserRequest,
                         pa_u1Concatenated, &i4_StrLen);
    RADIUS_MESSAGE_DIGEST (a_u1ResponseAuth, pa_u1Concatenated, i4_StrLen);

    MemReleaseMemBlock (gRadMemPoolId.u4RadConcatPoolId,
                        (UINT1 *) pa_u1Concatenated);

}

/*----------------------------------------------------------------------------
Procedure    : radiusMakeStringVal()
Decription    : This module concatenates the received packet with response
              authenticator replaced by request authenticator transmitted
              and the shared secret.
Input(s)    : p_Radiusrequest
              p_u1RadiusReceivedPacket
Output(s)    : a_u1Concatenated
              i4_StrLen
Returns        : None
Called By    : radiusMakeResAuth()
Calling        : None
Author        : G.Vedavinayagam
Date        : 11/13/97
odifications    :
----------------------------------------------------------------------------*/
#ifdef __STDC__
VOID
radiusMakeStringVal (UINT1 *p_u1RadiusReceivedPacket,
                     tRADIUS_REQ_ENTRY ** p_UserRequest,
                     UINT1 a_u1Concatenated[], INT4 *i4_Length)
#else
VOID
radiusMakeStringVal (p_u1RadiusReceivedPacket, p_UserRequest,
                     a_u1Concatenated, i4_Length)
     UINT1              *p_u1RadiusReceivedPacket;
     tRADIUS_REQ_ENTRY **p_UserRequest;
     UINT1               a_u1Concatenated[];
     INT4               *i4_Length;
#endif
{
    INT4                i4_seclen = 0;
    UINT2               u2_pktlen = 0;
    tRADIUS_REQ_ENTRY  *p_RadiusRequest = NULL;
    UINT1              *p_u1Secret = NULL;
    tRADIUS_SERVER     *p_RadiusServer = NULL;

    p_RadiusRequest = *p_UserRequest;
    p_RadiusServer = p_RadiusRequest->ptRadiusServer;

    p_u1Secret = p_RadiusServer->a_u1Secret;

    MEMSET (a_u1Concatenated, 0, LEN_RX_PKT + LEN_SECRET);

    MEMCPY (&u2_pktlen, p_u1RadiusReceivedPacket + PKT_LEN, 2);
    u2_pktlen = OSIX_NTOHS (u2_pktlen);

    MEMCPY (a_u1Concatenated, p_u1RadiusReceivedPacket, u2_pktlen);
    MEMCPY (a_u1Concatenated + PKT_REQA,
            p_RadiusRequest->a_u1RequestAuth, LEN_REQ_AUTH_AUTH);
    i4_seclen = STRLEN ((char *) p_u1Secret);
    MEMCPY (a_u1Concatenated + u2_pktlen, p_u1Secret, i4_seclen);
    *i4_Length = u2_pktlen + i4_seclen;
}

/*-----------------------------------------------------------------------------
Procedure    : radiusValidateResAuth()
Description    : This module checks whether the calculated response
              authenticator and the response authenticator from the
              received packet are same or not. If the response auths are
              not matching it returns -1.
Input(s)    : p_u1RadiusReceivedPacket
              p_RadiusRequest
              p_u1ResponseAuth
Output(s)    : None
Returns        : 0, if the validation passes
              -1, if the validation fails
Called By    : radiusValidatePacket()
Calling        : None
Author        : G.Vedavinayagam
Date        : 11/13/97
odifications    :
-----------------------------------------------------------------------------*/
#ifdef __STDC__
INT1
radiusValidateResAuth (UINT1 *p_u1RadiusReceivedPacket,
                       UINT1 a_u1ResponseAuth[],
                       tRADIUS_REQ_ENTRY ** p_UserRequest)
#else
INT1
radiusValidateResAuth (p_u1RadiusReceivedPacket, a_u1ResponseAuth,
                       p_UserRequest)
     UINT1              *p_u1RadiusReceivedPacket;
     UINT1               a_u1ResponseAuth[];
     tRADIUS_REQ_ENTRY **p_UserRequest;
#endif
{

    if (MEMCMP (a_u1ResponseAuth,
                p_u1RadiusReceivedPacket + PKT_RESA, LEN_RES_AUTH))
    {
        if (*(p_u1RadiusReceivedPacket + PKT_TYPE) == ACCESS_ACCEPT ||
            *(p_u1RadiusReceivedPacket + PKT_TYPE) == ACCESS_REJECT ||
            *(p_u1RadiusReceivedPacket + PKT_TYPE) == ACCESS_CHALLENGE)
        {
            (*p_UserRequest)->ptRadiusServer->u4_AuthBadAuths++;

        }
        else
        {
            (*p_UserRequest)->ptRadiusServer->u4_AcctBadAuths++;
        }
        return NOT_OK;
    }
    return (OK);
}

/*----------------------------------------------------------------------------
Procedure    : radiusValidateAttributes()
Description    : This modules the validates the attributes. The validation 
              is based on the lengths of the attributes.
Input(s)    : p_u1RadiusReceivedPacket
              p_RadiusRequest
Output(s)    : None
Returns        : 0, if the validaion passes
              -1, if the validation fails
Called By    : radiusTakeAction()
Calling        : None
Author        : G.Vedavinayagam
Date        : 11/13/97
odifications    :
----------------------------------------------------------------------------*/
#ifdef __STDC__
INT1
radiusValidateAttributes (UINT1 *p_u1RadiusReceivedPacket,
                          tRADIUS_REQ_ENTRY ** p_UserRequest)
#else
INT1
radiusValidateAttributes (p_u1RadiusReceivedPacket, p_UserRequest)
     UINT1              *p_u1RadiusReceivedPacket;
     tRADIUS_REQ_ENTRY **p_UserRequest;
#endif
{
    unUtilAlgo          UtilAlgo;
    tRADIUS_REQ_ENTRY  *p_RadiusRequest = NULL;
    UINT2               u2_pktoffset = 0, u2_pktlen = 0, u2_attrlen = 0;
    UINT1               u1_attrtype = 0;
    UINT1               a_u1RecvSignature[LEN_MESSAGE_AUTHENTICATOR] = { 0 };
    UINT1               a_u1CalSignature[LEN_MESSAGE_AUTHENTICATOR] = { 0 };
    UINT2               text_len = 0;
    INT4                i4_SecLen = 0;
    tRADIUS_SERVER     *p_RadiusServer = NULL;
    UINT1               a_u1ResponseAuth[LEN_REQ_AUTH_AUTH];
    UINT1               a_u1AttrLenTable[] =
        { 0, 0, 0, LEN_CHAP_PASSWORD, LEN_NAS_IP_ADDRESS,
        LEN_NAS_PORT, LEN_SERVICE_TYPE,
        LEN_FRAMED_PROTOCOL, LEN_FRAMED_IP_ADDRESS,
        LEN_FRAMED_IP_NETMASK, LEN_FRAMED_ROUTING,
        0, LEN_FRAMED_MTU, LEN_FRAMED_COMPRESSION,
        LEN_LOGIN_IP_HOST, LEN_LOGIN_SERVICE,
        LEN_LOGIN_TCP_PORT, 0, 0, 0, 0, 0, 0,
        LEN_FRAMED_IPX_NETWORK, 0, 0, 0,
        LEN_SESSION_TIMEOUT, LEN_IDLE_TIMEOUT,
        LEN_TERMINATION_ACTION, 0, 0, 0, 0, 0, 0,
        LEN_LOGIN_LAT_GROUP,
        LEN_FRAMED_APPLETALK_LINK,
        LEN_FRAMED_APPLETALK_NETWORK,
        0, LEN_ACCT_STATUS_TYPE, LEN_ACCT_DELAY_TIME,
        LEN_ACCT_INPUT_OCTETS, LEN_ACCT_OUTPUT_OCTETS,
        0, LEN_ACCT_AUTHENTIC, LEN_ACCT_SESSION_TIME,
        LEN_ACCT_INPUT_PACKETS,
        LEN_ACCT_OUTPUT_PACKETS,
        LEN_ACCT_TERMINATE_CAUSE, 0,
        LEN_ACCT_LINK_COUNT, 0, 0, 0, 0, 0, 0, 0, 0,
        0, LEN_NAS_PORT_TYPE, LEN_PORT_LIMIT, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
    };

    MEMSET (&UtilAlgo, 0, sizeof (unUtilAlgo));
    p_RadiusRequest = *p_UserRequest;

    MEMCPY (&u2_pktlen, p_u1RadiusReceivedPacket + PKT_LEN, 2);
    u2_pktlen = OSIX_NTOHS (u2_pktlen);
    u2_pktoffset = PKT_ATTR;

    for (;;)
    {
        if (u2_pktoffset >= u2_pktlen)
            break;
        u1_attrtype = *(p_u1RadiusReceivedPacket + u2_pktoffset);

        if (u1_attrtype > A_MESSAGE_AUTHENTICATOR || u1_attrtype < A_USER_NAME)
        {
            u2_pktoffset += *(p_u1RadiusReceivedPacket + u2_pktoffset + 1);
            continue;
        }
        if ((u2_attrlen = a_u1AttrLenTable[u1_attrtype]))
        {
            if (u2_attrlen != *(p_u1RadiusReceivedPacket + u2_pktoffset + 1))
            {

                radiusReleasePacket (gRadMemPoolId.u4PacketPoolId,
                                     p_RadiusRequest->p_u1PacketTransmitted);
                RadiusReleaseUserInfoAuth (&(p_RadiusRequest->userInfoAuth));
                radiusReleaseUserEntry (gRadMemPoolId.u4UserEntryPoolId,
                                        (UINT1 *) p_RadiusRequest);

                return (NOT_OK);
            }
            u2_pktoffset += u2_attrlen;
        }
        else
        {
            switch (u1_attrtype)
            {
                case A_FILTER_ID:
                case A_REPLY_MESSAGE:
                case A_CALLBACK_NUMBER:
                case A_CALLBACK_ID:
                case A_FRAMED_ROUTE:
                case A_STATE:
                case A_CLASS:
                case A_PROXY_STATE:
                case A_LOGIN_LAT_SERVICE:
                case A_LOGIN_LAT_NODE:
                case A_FRAMED_APPLETALK_ZONE:
                case A_LOGIN_LAT_PORT:
                    if (*(p_u1RadiusReceivedPacket + u2_pktoffset + 1) < 3)
                    {

                        radiusReleasePacket (gRadMemPoolId.u4PacketPoolId,
                                             p_RadiusRequest->
                                             p_u1PacketTransmitted);
                        RadiusReleaseUserInfoAuth
                            (&(p_RadiusRequest->userInfoAuth));
                        radiusReleaseUserEntry (gRadMemPoolId.u4UserEntryPoolId,
                                                (UINT1 *) p_RadiusRequest);

                        return (NOT_OK);
                    }
                    break;
                case A_VENDOR_SPECIFIC:
                    if (*(p_u1RadiusReceivedPacket + u2_pktoffset + 1) < 7)
                    {

                        radiusReleasePacket (gRadMemPoolId.u4PacketPoolId,
                                             p_RadiusRequest->
                                             p_u1PacketTransmitted);
                        RadiusReleaseUserInfoAuth
                            (&(p_RadiusRequest->userInfoAuth));
                        radiusReleaseUserEntry (gRadMemPoolId.u4UserEntryPoolId,
                                                (UINT1 *) p_RadiusRequest);

                        return (NOT_OK);
                    }
                    break;
                case A_EAP_MESSAGE:
                    if (*(p_u1RadiusReceivedPacket + u2_pktoffset + 1) <
                        LEN_RAD_ATTRIBUTE)
                    {

                        radiusReleasePacket (gRadMemPoolId.u4PacketPoolId,
                                             p_RadiusRequest->
                                             p_u1PacketTransmitted);
                        RadiusReleaseUserInfoAuth
                            (&(p_RadiusRequest->userInfoAuth));
                        radiusReleaseUserEntry (gRadMemPoolId.u4UserEntryPoolId,
                                                (UINT1 *) p_RadiusRequest);

                        return (NOT_OK);
                    }
                    break;

                case A_MESSAGE_AUTHENTICATOR:
                    if (*(p_u1RadiusReceivedPacket + u2_pktoffset + 1) <
                        LEN_MESSAGE_AUTHENTICATOR)
                    {

                        radiusReleasePacket (gRadMemPoolId.u4PacketPoolId,
                                             p_RadiusRequest->
                                             p_u1PacketTransmitted);
                        RadiusReleaseUserInfoAuth
                            (&(p_RadiusRequest->userInfoAuth));
                        radiusReleaseUserEntry (gRadMemPoolId.u4UserEntryPoolId,
                                                (UINT1 *) p_RadiusRequest);

                        return (NOT_OK);
                    }
                    MEMCPY (&a_u1RecvSignature, p_u1RadiusReceivedPacket +
                            u2_pktoffset + LEN_RAD_ATTRIBUTE,
                            LEN_MESSAGE_AUTHENTICATOR);
                    MEMSET (p_u1RadiusReceivedPacket + u2_pktoffset +
                            LEN_RAD_ATTRIBUTE, 0, LEN_MESSAGE_AUTHENTICATOR);
                    MEMCPY (&a_u1ResponseAuth,
                            p_u1RadiusReceivedPacket + PKT_REQA,
                            LEN_REQ_AUTH_AUTH);
                    MEMCPY (p_u1RadiusReceivedPacket + PKT_REQA,
                            p_RadiusRequest->a_u1RequestAuth,
                            LEN_REQ_AUTH_AUTH);
                    MEMCPY ((char *) &text_len, p_u1RadiusReceivedPacket + 2,
                            2);
                    text_len = OSIX_HTONS (text_len);
                    p_RadiusRequest = *p_UserRequest;
                    p_RadiusServer = p_RadiusRequest->ptRadiusServer;
                    i4_SecLen = STRLEN (p_RadiusServer->a_u1Secret);

                    UtilAlgo.UtilHmacAlgo.pu1HmacPktDataPtr =
                        (unsigned char *) p_u1RadiusReceivedPacket;
                    UtilAlgo.UtilHmacAlgo.i4HmacPktLength = text_len;
                    UtilAlgo.UtilHmacAlgo.pu1HmacKey =
                        p_RadiusServer->a_u1Secret;
                    UtilAlgo.UtilHmacAlgo.u4HmacKeyLen = i4_SecLen;
                    UtilAlgo.UtilHmacAlgo.pu1HmacOutDigest =
                        (unsigned char *) &a_u1CalSignature;

                    UtilHash (ISS_UTIL_ALGO_HMAC_MD5, &UtilAlgo);

                    if (MEMCMP
                        (&a_u1CalSignature, &a_u1RecvSignature,
                         LEN_MESSAGE_AUTHENTICATOR))
                    {
                        radiusReleasePacket (gRadMemPoolId.u4PacketPoolId,
                                             p_RadiusRequest->
                                             p_u1PacketTransmitted);
                        RadiusReleaseUserInfoAuth
                            (&(p_RadiusRequest->userInfoAuth));

                        radiusReleaseUserEntry (gRadMemPoolId.u4UserEntryPoolId,
                                                (UINT1 *) p_RadiusRequest);

                        return (NOT_OK);
                    }

                    MEMCPY (p_u1RadiusReceivedPacket + PKT_REQA,
                            &a_u1ResponseAuth, LEN_REQ_AUTH_AUTH);
                    break;

                default:
                    break;
            }                    /* switch */
            u2_pktoffset += *(p_u1RadiusReceivedPacket + u2_pktoffset + 1);
        }                        /* else */
    }                            /* for */

    /* releasing of userentry moved from validate 
     * identifier and then from radiusValidatePacket */

    /*u1_pktId = *(p_u1RadiusReceivedPacket + PKT_ID);
       p_AppTimer = a_gUserTable[u1_pktId];
       p_CurrentRadiusReq = NULL;
       if (p_AppTimer)
       {
       p_CurrentRadiusReq = (tRADIUS_REQ_ENTRY *) p_AppTimer->u4Data;
       if (p_CurrentRadiusReq)
       {
       p_CurrentRadiusReq->u1_ReTxCount = 0;
       if (rad_tmrh_stop_timer (p_AppTimer) == NOT_OK)
       {
       RAD_TIMER_PRINT ("\nError in Stopping TIMER");
       p_gRadiusErrorLog->u4_ErrorTimerNotStoped++;
       }
       }
       } */

    return (OK);
}

/*----------------------------------------------------------------------------
Procedure    : radiusProcessPacket()
Description    : This module processes the packet received as response for 
              a radius request.
Input(s)    : p_u1RadiusReceivedPacket
Output(s)    :
Returns        : None
Called By    : radiusTakeAction()
Calling        : None
Author        : G.Vedavinayagam
Date        : 11/13/97
odifications    :
-----------------------------------------------------------------------------*/
#ifdef __STDC__
VOID
radiusProcessPacket (UINT1 *p_u1RadiusReceivedPacket,
                     tRADIUS_REQ_ENTRY ** p_UserRequest)
#else
VOID
radiusProcessPacket (p_u1RadiusReceivedPacket, p_UserRequest)
     UINT1              *p_u1RadiusReceivedPacket;
     tRADIUS_REQ_ENTRY **p_UserRequest;
#endif
{
    UINT1               u1_ID = 0;
    UINT2               u2_PktLen = 0;
    tRADIUS_SERVER     *p_RadiusServer = NULL;
    tRADIUS_REQ_ENTRY  *p_RadiusRequest = NULL;
    INT4                i4_Index = 0;
    UINT4               u4TimeDiff = 0;
    tRadInterface      *pInterface = NULL;
    UINT1               au1UserName[RAD_USER_NAME_SIZE + 1];

    p_RadiusRequest = *p_UserRequest;
    p_RadiusServer = p_RadiusRequest->ptRadiusServer;

    MEMSET (au1UserName, 0, RAD_USER_NAME_SIZE + 1);

    MEMCPY (&u2_PktLen, p_u1RadiusReceivedPacket + PKT_LEN, 2);
    u2_PktLen = OSIX_NTOHS (u2_PktLen);

    RAD_EVENT_PRINT ("\nPrinting the Packet Received:\n");
    RAD_PACKET_PRINT ("\n-------------------------- \n");
    for (i4_Index = 0; i4_Index < u2_PktLen; i4_Index++)
        RAD_PACKET_PRINT1 ("%02x ", *(p_u1RadiusReceivedPacket + i4_Index));
    RAD_PACKET_PRINT ("\n-------------------------- \n");

    OsixGetSysTime ((tOsixSysTime *) & u4TimeDiff);
    u4TimeDiff -= p_RadiusRequest->u4SendTime;
    u4TimeDiff /= SYS_NUM_OF_TIME_UNITS_IN_A_SEC;
    RAD_EVENT_PRINT1 ("\n Received the Response after %d Seconds\n",
                      u4TimeDiff);
    p_RadiusServer->u4_RoundTripTime = u4TimeDiff;

    p_RadiusServer->u4_PendingRequests--;

    if (*(p_u1RadiusReceivedPacket + PKT_TYPE) == ACCESS_ACCEPT)
    {
        p_RadiusServer->u4_AccessAccepts++;
        RAD_EVENT_PRINT ("\n Received ACCESS_ACCEPT.\n");
    }
    if (*(p_u1RadiusReceivedPacket + PKT_TYPE) == ACCESS_REJECT)
    {
        p_RadiusServer->u4_AccessRejects++;
        RAD_EVENT_PRINT ("\n Received ACCESS_REJECT.\n");
    }
    if (*(p_u1RadiusReceivedPacket + PKT_TYPE) == ACCESS_CHALLENGE)
    {
        p_RadiusServer->u4_AccessChallenges++;
        RAD_EVENT_PRINT ("\n Received ACCESS_CHALLENGE.\n");
    }
    if (*(p_u1RadiusReceivedPacket + PKT_TYPE) == ACCOUNTING_RESPONSE)
    {
        p_RadiusServer->u4_AcctResps++;
        RAD_EVENT_PRINT ("\n Received ACCOUNTING_RESPONSE.\n");
    }

    radiusReleasePacket (gRadMemPoolId.u4PacketPoolId,
                         p_RadiusRequest->p_u1PacketTransmitted);

    if ((*(p_u1RadiusReceivedPacket + PKT_TYPE) != ACCOUNTING_RESPONSE) &&
        (p_RadiusRequest->CallBackfPtr != NULL))
    {
        pInterface =
            RadFillInterfaceStructure (p_u1RadiusReceivedPacket,
                                       p_RadiusRequest);
        if (pInterface)
        {
            pInterface->ifIndex = p_RadiusRequest->ifIndex;
            pInterface->TaskId = p_RadiusRequest->userInfoAuth.TaskId;
            MEMCPY (pInterface->au1StaMac, p_RadiusRequest->au1StaMac,
                    MAC_ADDR_LEN);
            pInterface->i4NasPort = p_RadiusRequest->userInfoAuth.u4_NasPort;

            if (*(p_u1RadiusReceivedPacket + PKT_TYPE) == ACCESS_ACCEPT)
            {
                /* This varaibale is needed in the case of WSS-RADIUS interaction
                 * to signify that an Radius ACCESS-ACCEPT packet is recvieved*/
                pInterface->bAccessAcceptRcvd = OSIX_TRUE;
            }

            /* In the case of WSS Captive portal-web authentication, user name
             * is not being sent in access-accept packets. So the user name is
             * retrived from the radius module and is being passed to the Registered
             * modules*/

            /* In the case of RSNA with 802.1X Authentication, the user name 
             * will be sent in access-accept packet itself, so explicit 
             * copying of the user name is not required. It will be filled in 
             * RadFillInterfaceStructure function called above*/
            if (MEMCMP
                (pInterface->au1Username, au1UserName,
                 RAD_USER_NAME_SIZE + 1) == 0)
            {
                MEMCPY (pInterface->au1Username, p_RadiusRequest->a_u1UserName,
                        sizeof (pInterface->au1Username));
            }
            RAD_EVENT_PRINT ("\n Passing the Information to USER Modules.\n");

            (*(p_RadiusRequest->CallBackfPtr)) ((VOID *) pInterface);
        }

    }
    /* Release the Timer reference */
    u1_ID = p_RadiusRequest->u1_PacketIdentifier;

    /* Release the memory allocated for userInfoAuth */
    RadiusReleaseUserInfoAuth (&(p_RadiusRequest->userInfoAuth));

    /* Release the memory allocated for user Auth Q entry */
    radiusReleaseUserEntry (gRadMemPoolId.u4UserEntryPoolId,
                            (UINT1 *) p_RadiusRequest);

    MemReleaseMemBlock (gRadMemPoolId.u4RadTimerPoolId,
                        (UINT1 *) a_gUserTable[u1_ID]);
    a_gUserTable[u1_ID] = NULL;

    /* BMW, Interface routing should be called here to send this information to
       the User. Refer FutureRADIUS Porting Giudelines document */

    return;
}

/*-----------------------------------------------------------------------------
Procedure    : radiusCheckIncomingPacketAuth()
Description    : This module polls the authentication socket for any packet
              reception. If any packet found TakeAction module is called.
Input(s)    : None
Output(s)    : None
Returns        : None
Called By    : The application using RADIUS.
Calling        : radiusTakeAction()
Author        : G.Vedavinayagam
Date        : 13/3/98
odifications    :
-----------------------------------------------------------------------------*/

VOID
radiusCheckIncomingPacketAuth (UINT1 *a_u1Buf, tIPvXAddr * pSrcServerAddress)
{
    INT4                i4Index = 0;
    INT4                i4_RetVal = 0;

    RAD_EVENT_PRINT ("\nReceived packet on the Authentication Port.\n");
    RAD_EVENT_PRINT1 ("\nAuth Server Address %s", pSrcServerAddress->au1Addr);

    for (i4Index = 0; i4Index < MAX_RAD_SERVERS_LIMIT; i4Index++)
    {
        if (a_gServerTable[i4Index])
        {
            if (MEMCMP (pSrcServerAddress->au1Addr,
                        a_gServerTable[i4Index]->ServerAddress.au1Addr,
                        IPVX_IPV6_ADDR_LEN) == 0)
                break;
        }
    }
    if (i4Index >= MAX_RAD_SERVERS_LIMIT)
    {
        RAD_ERR_PRINT ("\nINVALID SERVER\n");
        u4_gRadAuthClientInvalidServers++;
    }

    i4_RetVal = radiusTakeAction (a_u1Buf);

    /* BMW, added this check */
    if (i4_RetVal == OK)
    {
        RAD_EVENT_PRINT1 ("\nAuth response received and processed%d",
                          i4_RetVal);
    }
    else
    {
        RAD_EVENT_PRINT ("\nAuthentication response received is not proper");
    }
}

/*-----------------------------------------------------------------------------
Procedure    : radiusCheckIncomingPacketAcc()
Description    : This module polls the accounting socket for any packet
              reception. If any packet found TakeAction module is called.
Input(s)    : None
Output(s)    : None
Returns        : None
Called By    : The application using RADIUS.
Calling        : radiusTakeAction()
Author        : G.Vedavinayagam
Date        : 13/3/98
odifications    :
-----------------------------------------------------------------------------*/

VOID
radiusCheckIncomingPacketAcc (UINT1 *a_u1Buf, tIPvXAddr * pSrcServerAddress)
{
    INT4                i4Index = 0;
    INT4                i4_RetVal = 0;

    RAD_EVENT_PRINT ("\nReceived packet on the Accounting Port.\n");
    RAD_EVENT_PRINT1 ("\nAcc Server Address %s", pSrcServerAddress->au1Addr);

    for (i4Index = 0; i4Index < MAX_RAD_SERVERS_LIMIT; i4Index++)
    {
        if (a_gServerTable[i4Index])
        {
            if (MEMCMP (pSrcServerAddress->au1Addr,
                        a_gServerTable[i4Index]->ServerAddress.au1Addr,
                        IPVX_IPV6_ADDR_LEN) == 0)
                break;
        }
    }
    if (i4Index >= MAX_RAD_SERVERS_LIMIT)
        u4_gRadAcctClientInvalidServers++;

    i4_RetVal = radiusTakeAction (a_u1Buf);

    /* BMW, added this check */
    if (i4_RetVal == OK)
    {
        RAD_EVENT_PRINT1 ("\nAcct response received and processed%d",
                          i4_RetVal);
    }
    else
    {
        RAD_EVENT_PRINT ("\nAcct response received is not proper");
    }
}

/*-----------------------------------------------------------------------------
Procedure    : RadFillInterfaceStructures()
Description    : This function fill the interfcae structure with the 
          response from the server.
Input(s)    : None
Output(s)    : None
Returns        : None
Called By    : The application using RADIUS.
Calling        : Callback Function() provided by the user module.
Author        : C.Shijith 
Date        : 17/7/2000
odifications    :
-----------------------------------------------------------------------------*/

tRadInterface      *
RadFillInterfaceStructure (UINT1 *pu1RespPkt,
                           tRADIUS_REQ_ENTRY * p_RadiusRequest)
{
    tRadInterface      *pIface = NULL;
    tRADIUS_SERVER     *p_RadiusServer = NULL;
    tRAD_MS_MPPE_KEYS  *pKeys = NULL;
    UINT1              *pu1Key = NULL;
    UINT1              *pu1SharedSecret = NULL;
    UINT4               u4SharedSecretLen = 0;
    UINT4               u4KeyLen = 0;
    UINT2               u2RadPktLen = 0;
    UINT4               u4RadPktOffset = 0;
    UINT4               u4Data = 0;
    UINT4               u4EapPktOffset = 0;
    UINT1               u1RadAttrType = 0;
    UINT1               u1RadAttrLen = 0;
    UINT1               u1VendorSubtypeValue = 0;
    UINT4               u4Index = 0;

#ifdef L2TP_WANTED
    UINT2               u2Data = 0;
    UINT1              *pu1Addr = NULL;
#endif

    if ((pu1RespPkt == NULL) || (p_RadiusRequest == NULL))
    {
        return (NULL);
    }

    if ((p_RadiusServer = p_RadiusRequest->ptRadiusServer) != NULL)
    {
        pu1SharedSecret = p_RadiusServer->a_u1Secret;
        u4SharedSecretLen = STRLEN (pu1SharedSecret);
    }
    else
    {
        return (NULL);
    }

    pIface = (tRadInterface *) (VOID *) UtlShMemAllocRadInterface ();
    if (pIface == NULL)
    {
        return NULL;
    }
    RadInitInterface (pIface);

    pIface->Access = pu1RespPkt[0];    /* Access ACCEPT/REJECT/CHALLENGE  */
    pIface->RequestId = p_RadiusRequest->u1_PacketIdentifier;
    pIface->u1EAPType = p_RadiusRequest->userInfoAuth.u1_ProtocolType;

    MEMCPY (&u2RadPktLen, pu1RespPkt + PKT_LEN, 2);
    u2RadPktLen = OSIX_NTOHS (u2RadPktLen);
    u4RadPktOffset = PKT_ATTR;

    pKeys = &pIface->MsMppeKeys;    /* WSS ADD */
    RAD_EVENT_PRINT ("\nATTRIBUTES in the Received Packet:-\n");
    for (;;)
    {
        if (u4RadPktOffset >= u2RadPktLen)
            break;

        u1RadAttrType = *(pu1RespPkt + u4RadPktOffset);
        u1RadAttrLen = *(pu1RespPkt + u4RadPktOffset + 1);

        switch (u1RadAttrType)
        {

            case A_VENDOR_SPECIFIC:

            {
                UINT4               u4VendorId = 0;
                UINT1              *pu1VendorSubtypePos = NULL;

                if (u1RadAttrLen < RAD_ATTR_LEN)
                    break;

/* BYTE         1               2               3               4
        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
        |     Type      |  Length       |            Vendor-Id
        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
        Vendor-Id (cont)           |  String...
        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-
*/

                /* Get the Vendor ID */
                MEMCPY (&u4VendorId, pu1RespPkt + u4RadPktOffset + BYTE_LEN_2,
                        BYTE_LEN_4);

                /* Check for Microsoft Vendor Id */
                if (OSIX_NTOHL ((u4VendorId)) == MS_CHAP_VENDOR_ID)
                {
                    RAD_RESPONSE_PRINT
                        ("Microsoft Vendor-id attr received\r\n");

                    /* Vendor specific data starts at the following offset:
                     * Attr Type(1) + Atr Len(1) + Vendor Id (4) = 6 Bytes
                     */
                    pu1VendorSubtypePos =
                        (pu1RespPkt + u4RadPktOffset + BYTE_LEN_2 + BYTE_LEN_4);

                    /* Supported Microsoft Vendor Specific RADIUS Attribute:
                     * MS-MPPE-Recv-Key */

                    /* Get MS-MPPE-Recv-Key */
                    pu1Key = RadGetVendorAttr (pu1VendorSubtypePos,
                                               SA_MS_MPPE_RECV_KEY, &u4KeyLen);

                    if (pu1Key)
                    {
                        /* Decrypt receive key */
                        pKeys->pu1MppeRecvKey =
                            RadDecryptMsMppeKey (pu1Key, u4KeyLen,
                                                 p_RadiusRequest->
                                                 a_u1RequestAuth,
                                                 pu1SharedSecret,
                                                 u4SharedSecretLen,
                                                 &pKeys->u2MppeRecvKeyLen);

                        if (pKeys->pu1MppeRecvKey)
                        {
                            RAD_RESPONSE_PRINT1 ("MS-MPPE-Recv-Key Decrypt "
                                                 "Successful. Length - %u "
                                                 "Bytes\r\n",
                                                 pKeys->u2MppeRecvKeyLen);
                            RadUtilHexDump (pKeys->pu1MppeRecvKey,
                                            pKeys->u2MppeRecvKeyLen);
                        }
                        MemReleaseMemBlock (gRadMemPoolId.u4RadKeyPoolId,
                                            (UINT1 *) pu1Key);

                        KW_FALSEPOSITIVE_FIX (pKeys->pu1MppeRecvKey);
                        break;
                    }
                }
                else if ((OSIX_NTOHL ((u4VendorId)) == ARICENT_VENDOR_ID))
                {

                    MEMCPY (&u1VendorSubtypeValue,
                            pu1RespPkt + u4RadPktOffset + BYTE_LEN_6,
                            BYTE_LEN_1);

                    switch (u1VendorSubtypeValue)
                    {
                        case RAD_BANDWIDTH:
                            MEMCPY (&u4Data, (pu1RespPkt + u4RadPktOffset + 8),
                                    BYTE_LEN_4);
                            pIface->u4Bandwidth = OSIX_NTOHL (u4Data);
                            break;
                        case RAD_DLBANDWIDTH:
                            MEMCPY (&u4Data, (pu1RespPkt + u4RadPktOffset + 8),
                                    BYTE_LEN_4);
                            pIface->u4DLBandwidth = OSIX_NTOHL (u4Data);
                            break;
                        case RAD_ULBANDWIDTH:
                            MEMCPY (&u4Data, (pu1RespPkt + u4RadPktOffset + 8),
                                    BYTE_LEN_4);
                            pIface->u4ULBandwidth = OSIX_NTOHL (u4Data);
                            break;
                        case RAD_VOLUME:
                            MEMCPY (&u4Data, (pu1RespPkt + u4RadPktOffset + 8),
                                    BYTE_LEN_4);
                            pIface->u4Volume = OSIX_NTOHL (u4Data);
                            break;
                        case RAD_TIME:
                            MEMCPY (&u4Data, (pu1RespPkt + u4RadPktOffset + 8),
                                    BYTE_LEN_4);
                            pIface->u4Timeout = OSIX_NTOHL (u4Data);
                            break;
                        default:
                            break;
                    }

                }
                break;
            }

            case A_USER_NAME:
                pIface->u1UsernameLen = (UINT1) (u1RadAttrLen - 2);
                if (pIface->u1UsernameLen > RAD_USER_NAME_SIZE)
                {
                    RAD_ERR_PRINT (" Error in Radius User Name.\n");
                    break;
                }
                MEMSET (pIface->au1Username, 0, sizeof (pIface->au1Username));
                STRNCPY (pIface->au1Username,
                         pu1RespPkt + u4RadPktOffset + BYTE_LEN_2,
                         MEM_MAX_BYTES (pIface->u1UsernameLen,
                                        RAD_USER_NAME_SIZE));
                pIface->au1Username[pIface->u1UsernameLen] = '\0';
                break;

            case A_EAP_MESSAGE:
                MEMCPY (&pIface->au1EapPkt[u4EapPktOffset],
                        pu1RespPkt + u4RadPktOffset + LEN_RAD_ATTRIBUTE,
                        (u1RadAttrLen - LEN_RAD_ATTRIBUTE));
                u4EapPktOffset += (UINT4) (u1RadAttrLen - LEN_RAD_ATTRIBUTE);
                break;

            case A_SERVICE_TYPE:
                MEMCPY (&u4Data, (pu1RespPkt + u4RadPktOffset + 2), 4);
                pIface->Service_Type = OSIX_NTOHL (u4Data);
                RAD_RESPONSE_PRINT1 ("\n\tSERVICE_TYPE:\t%d \n",
                                     OSIX_NTOHL (u4Data));
                break;

            case A_FRAMED_PROTOCOL:
                MEMCPY (&u4Data, (pu1RespPkt + u4RadPktOffset + 2), 4);
                pIface->Framed_Protocol = OSIX_NTOHL (u4Data);
                RAD_RESPONSE_PRINT1 ("\n\tFRAMED_PROTOCOL:\t%d \n",
                                     OSIX_NTOHL (u4Data));
                break;

            case A_FRAMED_IP_ADDRESS:
                MEMCPY (&u4Data, (pu1RespPkt + u4RadPktOffset + 2), 4);
                pIface->Framed_IP_Address = OSIX_NTOHL (u4Data);
                RAD_RESPONSE_PRINT1 ("\n\tFRAMED_IP_ADDRESS:\t%d \n",
                                     OSIX_NTOHL (u4Data));
                break;

            case A_FRAMED_IP_NETMASK:
                MEMCPY (&u4Data, (pu1RespPkt + u4RadPktOffset + 2), 4);
                pIface->Framed_IP_Netmask = OSIX_NTOHL (u4Data);
                RAD_RESPONSE_PRINT1 ("\n\tFRAMED_IP_NETMASK:\t%d \n",
                                     OSIX_NTOHL (u4Data));
                break;

            case A_FRAMED_MTU:
                MEMCPY (&u4Data, (pu1RespPkt + u4RadPktOffset + 2), 4);
                pIface->Framed_MTU = OSIX_NTOHL (u4Data);
                RAD_RESPONSE_PRINT1 ("\n\tFRAMED_MTU:\t%d \n",
                                     OSIX_NTOHL (u4Data));
                break;

            case A_FRAMED_COMPRESSION:
                MEMCPY (&u4Data, (pu1RespPkt + u4RadPktOffset + 2), 4);
                pIface->Framed_Compression = OSIX_NTOHL (u4Data);
                RAD_RESPONSE_PRINT1 ("\n\tFRAMED_COMPRESSION:\t%d \n",
                                     OSIX_NTOHL (u4Data));
                break;

            case A_FRAMED_ROUTING:
                MEMCPY (&u4Data, (pu1RespPkt + u4RadPktOffset + 2), 4);
                pIface->Framed_Routing = OSIX_NTOHL (u4Data);
                RAD_RESPONSE_PRINT1 ("\n\tFRAMED_ROUTING:\t%d \n",
                                     OSIX_NTOHL (u4Data));
                break;

            case A_REPLY_MESSAGE:
                if (p_RadiusRequest->u1_IsUtf8EncodingReqd == UTF8_ENABLE)
                {
                    MEMSET (pIface->Utf8ReplyMsg, 0,
                            sizeof (pIface->Utf8ReplyMsg));
                    /* Decode UTF-8 to mulilingual byte sequences */
                    if (Utf8ToUnicode ((pu1RespPkt + u4RadPktOffset + 2),
                                       u1RadAttrLen,
                                       pIface->Utf8ReplyMsg,
                                       u1RadAttrLen) != UTF8_SUCCESS)
                    {
                        RAD_ERR_PRINT (" Error in UTF-8 encoding.\n");
                        break;
                    }
                    for (u4Index = 0;
                         (pIface->Utf8ReplyMsg[u4Index] != L'\0') &&
                         (u4Index < u1RadAttrLen); u4Index++)
                    {
                        RAD_RESPONSE_PRINT1 ("\n\tA_REPLY_MESSAGE::%x\n",
                                             pIface->Utf8ReplyMsg[u4Index]);
                    }
                }
                else
                {
                    MEMSET (pIface->Reply_Message, 0,
                            sizeof (pIface->Reply_Message));
                    MEMCPY (pIface->Reply_Message,
                            (pu1RespPkt + u4RadPktOffset + 2),
                            (u1RadAttrLen - 2));
                    RAD_RESPONSE_PRINT1 ("\n\tREPLY_MESSAGE:%s\n",
                                         pIface->Reply_Message);
                }
                break;

            case A_CALLBACK_NUMBER:

                MEMSET (pIface->Callback_Number, 0,
                        sizeof (pIface->Callback_Number));
                MEMCPY (pIface->Callback_Number,
                        (pu1RespPkt + u4RadPktOffset + 2), (u1RadAttrLen - 2));
                RAD_RESPONSE_PRINT1 ("\n\tCALLBACK_NUM:%s\n",
                                     pIface->Callback_Number);
                break;

            case A_STATE:
                pIface->u1StateLen = (UINT1) (u1RadAttrLen - 2);
                MEMSET (pIface->State, 0, sizeof (pIface->State));
                MEMCPY (pIface->State, (pu1RespPkt + u4RadPktOffset + 2),
                        pIface->u1StateLen);
                RAD_RESPONSE_PRINT1 ("\n\tSTATE:%s\n", pIface->State);
                break;

            case A_CLASS:
                MEMSET (pIface->Class, 0, sizeof (pIface->Class));
                MEMCPY (pIface->Class, (pu1RespPkt + u4RadPktOffset + 2),
                        (u1RadAttrLen - 2));
                RAD_RESPONSE_PRINT1 ("\n\tCLASS:%s\n", pIface->Class);
                break;

            case A_SESSION_TIMEOUT:
                MEMCPY (&u4Data, (pu1RespPkt + u4RadPktOffset + 2), 4);
                pIface->Session_Timeout = OSIX_NTOHL (u4Data);
                RAD_RESPONSE_PRINT1 ("\n\tSESSION_TIMEOUT:\t%d \n",
                                     OSIX_NTOHL (u4Data));
                break;

            case A_IDLE_TIMEOUT:
                MEMCPY (&u4Data, (pu1RespPkt + u4RadPktOffset + 2), 4);
                pIface->Idle_Timeout = OSIX_NTOHL (u4Data);
                RAD_RESPONSE_PRINT1 ("\n\tIDLE_TIMEOUT:\t%d \n",
                                     OSIX_NTOHL (u4Data));
                break;

            case A_TERMINATION_ACTION:
                MEMCPY (&u4Data, (pu1RespPkt + u4RadPktOffset + 2), 4);
                pIface->Termination_Action = OSIX_NTOHL (u4Data);
                RAD_RESPONSE_PRINT1 ("\n\tTERMINATION_ACTION:\t%d \n",
                                     OSIX_NTOHL (u4Data));
                break;

            case A_FRAMED_ROUTE:
                if (p_RadiusRequest->u1_IsUtf8EncodingReqd == UTF8_ENABLE)
                {
                    MEMSET (pIface->Utf8Framed_Route, 0,
                            sizeof (pIface->Utf8Framed_Route));
                    /* Decode UTF-8 to mulilingual byte sequences */
                    if (Utf8ToUnicode ((pu1RespPkt + u4RadPktOffset + 2),
                                       u1RadAttrLen,
                                       pIface->Utf8Framed_Route,
                                       u1RadAttrLen) != UTF8_SUCCESS)
                    {
                        RAD_ERR_PRINT (" Error in UTF-8 encoding.\n");
                        break;
                    }
                    for (u4Index = 0;
                         (pIface->Utf8Framed_Route[u4Index] != L'\0') &&
                         (u4Index < u1RadAttrLen); u4Index++)
                    {
                        RAD_RESPONSE_PRINT1 ("\n\tFRAMED_ROUTE::%x\n",
                                             pIface->Utf8Framed_Route);
                    }
                }
                else
                {
                    MEMSET (pIface->Framed_Route, 0,
                            sizeof (pIface->Framed_Route));
                    MEMCPY (pIface->Framed_Route,
                            (pu1RespPkt + u4RadPktOffset + 2),
                            (u1RadAttrLen - 2));
                    RAD_RESPONSE_PRINT1 ("\n\tFRAMED_ROUTE::%s\n",
                                         pIface->Framed_Route);
                }
                break;
            case A_FILTER_ID:
                if (p_RadiusRequest->u1_IsUtf8EncodingReqd == UTF8_ENABLE)
                {
                    MEMSET (pIface->Utf8Filter_Id, 0,
                            sizeof (pIface->Utf8Filter_Id));
                    /* Decode UTF-8 to mulilingual byte sequences */
                    if (Utf8ToUnicode ((pu1RespPkt + u4RadPktOffset + 2),
                                       u1RadAttrLen,
                                       pIface->Utf8Filter_Id,
                                       u1RadAttrLen) != UTF8_SUCCESS)
                    {
                        RAD_ERR_PRINT (" Error in UTF-8 encoding.\n");
                        break;
                    }
                    for (u4Index = 0;
                         (pIface->Utf8Filter_Id[u4Index] != L'\0') &&
                         (u4Index < u1RadAttrLen); u4Index++)
                    {
                        RAD_RESPONSE_PRINT1 ("\n\tA_FILTER_ID::%x\n",
                                             pIface->Utf8Filter_Id[u4Index]);
                    }
                }
                else
                {
                    MEMSET (pIface->Filter_Id, 0, sizeof (pIface->Filter_Id));
                    MEMCPY (pIface->Filter_Id,
                            (pu1RespPkt + u4RadPktOffset + 2),
                            (u1RadAttrLen - 2));
                    RAD_RESPONSE_PRINT1 ("\n\tFRAMED_ROUTE::%s\n",
                                         pIface->Filter_Id);
                }
            case A_LOGIN_IP_HOST:
                MEMCPY (&u4Data, (pu1RespPkt + u4RadPktOffset + 2), 4);
                pIface->Login_IP_Host = OSIX_NTOHL (u4Data);
                RAD_RESPONSE_PRINT1 ("\n\tLOGIN_IP_HOST:\t%d \n",
                                     OSIX_NTOHL (u4Data));
                break;

            case A_LOGIN_TCP_PORT:
                MEMCPY (&u4Data, (pu1RespPkt + u4RadPktOffset + 2), 4);
                pIface->Login_TCP_Port = OSIX_NTOHL (u4Data);
                RAD_RESPONSE_PRINT1 ("\n\tLOGIN_TCP_PORT:\t%d \n",
                                     OSIX_NTOHL (u4Data));
                break;

            case A_CALLBACK_ID:
                MEMSET (pIface->Callback_ID, 0, sizeof (pIface->Callback_ID));
                MEMCPY (pIface->Callback_ID,
                        (pu1RespPkt + u4RadPktOffset + 2), (u1RadAttrLen - 2));

                RAD_RESPONSE_PRINT1 ("\n\tCALLBACK_ID::%s\n",
                                     pIface->Callback_ID);
                break;

            case A_FRAMED_IPX_NETWORK:
                MEMCPY (&u4Data, (pu1RespPkt + u4RadPktOffset + 2), 4);
                pIface->Framed_IPX_Network = OSIX_NTOHL (u4Data);
                RAD_RESPONSE_PRINT1 ("\n\tFRAMED_IPX_NETWORK:\t%d \n",
                                     OSIX_NTOHL (u4Data));
                break;

            case A_LOGIN_LAT_GROUP:
                MEMCPY (&u4Data, (pu1RespPkt + u4RadPktOffset + 2), 4);
                pIface->Login_LAT_Group = OSIX_NTOHL (u4Data);
                RAD_RESPONSE_PRINT1 ("\n\tLOGIN_LAT_GROUP:\t%d \n",
                                     OSIX_NTOHL (u4Data));
                break;

            case A_FRAMED_APPLETALK_LINK:
                MEMCPY (&u4Data, (pu1RespPkt + u4RadPktOffset + 2), 4);
                pIface->Framed_Appletalk_Link = OSIX_NTOHL (u4Data);
                RAD_RESPONSE_PRINT1 ("\n\tFRAMED_APPLETALK_LINK:\t%d \n",
                                     OSIX_NTOHL (u4Data));
                break;

            case A_PORT_LIMIT:
                MEMCPY (&u4Data, (pu1RespPkt + u4RadPktOffset + 2), 4);
                pIface->Port_Limit = OSIX_NTOHL (u4Data);
                RAD_RESPONSE_PRINT1 ("\n\tPORT_LIMIT::\t%d \n",
                                     OSIX_NTOHL (u4Data));
                break;
#ifdef L2TP_WANTED

            case A_TUNNEL_TYPE:

                /* Tunnel Type gives tunnel is a l2tp or l2f or pptp ... */

                MEMCPY (&(pIface->Tunnel_Type_Tag),
                        (pu1RespPkt + u4RadPktOffset + 2), 1);

                MEMCPY (&u4Data, (pu1RespPkt + u4RadPktOffset + 2), 4);

                pIface->Tunnel_Type = OSIX_NTOHL (u4Data);

                RAD_RESPONSE_PRINT1 ("\n TUNNEL_TYPE :\t %d \n",
                                     OSIX_NTOHL (u4Data));

                break;

            case A_TUNNEL_MEDIUM_TYPE:

                MEMCPY (&(pIface->Tunnel_Medium_Type_Tag),
                        (pu1RespPkt + u4RadPktOffset + 2), 1);

                MEMCPY (&u4Data, (pu1RespPkt + u4RadPktOffset + 2), 4);

                pIface->Tunnel_Medium_Type = OSIX_NTOHL (u4Data);

                RAD_RESPONSE_PRINT1 ("\n TUNNEL MEDIUM TYPE :\t %d \n",
                                     OSIX_NTOHL (u4Data));

                break;
            case A_TUNNEL_CLIENT_ENDPOINT:
                /* LAC IP Addr */
                MEMCPY (&(pIface->Tunnel_Client_Endpoint_Tag),
                        (pu1RespPkt + u4RadPktOffset + 2), 1);
                MEMSET (au1Address, 0, sizeof (au1Address));
                MEMCPY (au1Address, (pu1RespPkt + u4RadPktOffset + 2),
                        (u1RadAttrLen - 2));

                u4Data = OSIX_NTOHL (UTLInetAddr ((const char *) au1Address));
                pIface->Tunnel_Client_Endpoint = OSIX_NTOHL (u4Data);

                RAD_RESPONSE_PRINT1 ("\n\t TUNNEL CLIENT END POINT : \t%d\n",
                                     OSIX_NTOHL (u4Data));

                break;
            case A_TUNNEL_SERVER_ENDPOINT:
                /* LNS IP Addr */
                MEMCPY (&(pIface->Tunnel_Server_Endpoint_Tag),
                        (pu1RespPkt + u4RadPktOffset + 2), 1);
                MEMSET (au1Address, 0, sizeof (au1Address));
                MEMCPY (au1Address, (pu1RespPkt + u4RadPktOffset + 2),
                        (u1RadAttrLen - 2));

                u4Data = OSIX_NTOHL (UTLInetAddr ((const char *) au1Address));

                pIface->Tunnel_Server_Endpoint = OSIX_NTOHL (u4Data);

                RAD_RESPONSE_PRINT1 ("\n\t TUNNEL SERVER END POINT : \t%d\n",
                                     OSIX_NTOHL (u4Data));

                break;
            case A_TUNNEL_PASSWORD:
                /*  MEMCPY(&(pIface->Tunnel_Password_Tag),(pu1RespPkt+u4RadPktOffset+2),1); */
                MEMCPY (&u2Data, (pu1RespPkt + u4RadPktOffset + 3), 2);

                pIface->Tunnel_Password_Salt = OSIX_NTOHL (u2Data);

                MEMSET (pIface->Tunnel_Password, 0, sizeof (UINT1));
                MEMCPY (pIface->Tunnel_Password,
                        (pu1RespPkt + u4RadPktOffset + 2), (u1RadAttrLen - 2));

                RAD_RESPONSE_PRINT1 ("\n TUNNEL PASSSWORD :\t %d \n",
                                     OSIX_NTOHL (u4Data));
                break;
            case A_TUNNEL_PRIVATE_GROUP_ID:

                MEMCPY (&(pIface->Tunnel_Private_Group_ID_Tag),
                        (pu1RespPkt + u4RadPktOffset + 2), 1);
                MEMSET (pIface->Tunnel_Private_Group_ID, 0, sizeof (UNIT1));
                MEMCPY (pIface->Tunnel_Private_Group_ID,
                        (pu1RespPkt + u4RadPktOffset + 3), (u1RadAttrLen - 3));

                RAD_RESPONSE_PRINT1 ("\n\t TUNNEL PRIVATE GROUP ID : \t%d\n",
                                     OSIX_NTOHL (u4Data));

                break;

            case A_TUNNEL_ASSIGNMENT_ID:

                MEMCPY (&(pIface->Tunnel_Assignment_ID_Tag),
                        (pu1RespPkt + u4RadPktOffset + 2), 1);

                MEMCPY (u4Data, (pu1RespPkt + u4RadPktOffset + 2),
                        (u1RadAttrLen - 2));
                pIface->Tunnel_Assignment_ID = OSIX_NTOHL (u4Data);
                RAD_RESPONSE_PRINT1 ("\n\t TUNNEL ASSIGNMENT ID : \t%d\n",
                                     OSIX_NTOHL (u4Data));
                break;

            case A_TUNNEL_PREFERENCE:

                MEMCPY (&(pIface->Tunnel_Preference_Tag),
                        (pu1RespPkt + u4RadPktOffset + 2), 1);

                MEMCPY (&u4Data, (pu1RespPkt + u4RadPktOffset + 3), 3);

                pIface->Tunnel_Preference = OSIX_NTOHL (u4Data);

                RAD_RESPONSE_PRINT1 ("\n TUNNEL PASSSWORD :\t %d \n",
                                     OSIX_NTOHL (u4Data));
                break;

            case A_TUNNEL_CLIENT_ATUH_ID:
                /* LAC id */
                MEMCPY (&(pIface->Tunnel_Client_Auth_ID_Tag),
                        (pu1RespPkt + u4RadPktOffset + 2), 1);
                MEMSET (pIface->Tunnel_Client_Auth_ID, 0, sizeof (UINT1);
                    )MEMCPY (pIface->Tunnel_Client_Auth_ID,
                             (pu1RespPkt + u4RadPktOffset +
                              3), (u1RadAttrLen - 3));

                RAD_RESPONSE_PRINT1 ("\n\t TUNNEL CLIENT AUTH ID : \t%d\n",
                                     OSIX_NTOHL (u4Data));
                break;

            case A_TUNNEL_SERVER_AUTH_ID:
                /* LNS Id */
                MEMCPY (&(pIface->Tunnel_Server_Auth_ID_Tag),
                        (pu1RespPkt + u4RadPktOffset + 2), 1);

                MEMCPY (u4Data, (pu1RespPkt + u4RadPktOffset + 3),
                        (u1RadAttrLen - 3));
                pIface->Tunnel_Server_Auth_ID_Tag = OSIX_NTOHL (u4Data);
                RAD_RESPONSE_PRINT1 ("\n\t TUNNEL SERVER AUTH ID : \t%d\n",
                                     OSIX_NTOHL (u4Data));
                break;
#endif
            default:
                break;

        }                        /* end of switch */

        u4RadPktOffset += u1RadAttrLen;
    }                            /* for loop */

    return pIface;

}

VOID
RadInitInterface (tRadInterface * pIface)
{
    MEMSET (pIface, 0, sizeof (tRadInterface));
    pIface->Service_Type = RAD_INTERFACE_DEFVAL;
    pIface->Framed_Protocol = RAD_INTERFACE_DEFVAL;
    pIface->Framed_IP_Address = RAD_INTERFACE_DEFVAL;
    pIface->Framed_IP_Netmask = RAD_INTERFACE_DEFVAL;
    pIface->Framed_MTU = RAD_INTERFACE_DEFVAL;
    pIface->Framed_Compression = RAD_INTERFACE_DEFVAL;
    pIface->Session_Timeout = RAD_INTERFACE_DEFVAL;
    pIface->Idle_Timeout = RAD_INTERFACE_DEFVAL;
    pIface->Termination_Action = RAD_INTERFACE_DEFVAL;

}

UINT4
UTLInetAddr (const char *instring)
{

#define RADIUS_DELIM "."

    char               *tok;
    char                ptr[16];

    UINT4               u4Addr = 0;
    UINT4               Temp = 0;

    strncpy (ptr, instring, 15);
    ptr[15] = 0;
    tok = strtok (ptr, RADIUS_DELIM);
    if (tok)
        Temp = atoi (tok);
    if (Temp > 255)
        return 0;
    u4Addr = Temp << 24;

    tok = strtok (NULL, RADIUS_DELIM);
    if (tok)
        Temp = atoi (tok);
    else
        return 0;

    if (Temp > 255)
        return 0;
    u4Addr = u4Addr | Temp << 16;

    tok = strtok (NULL, RADIUS_DELIM);
    if (tok)
        Temp = atoi (tok);
    else
        return 0;

    if (Temp > 255)
        return 0;
    u4Addr = u4Addr | Temp << 8;

    tok = strtok (NULL, RADIUS_DELIM);
    if (tok)
        Temp = atoi (tok);
    else
        return 0;

    if (Temp > 255)
        return 0;
    u4Addr = u4Addr | Temp;

    if (strtok (NULL, RADIUS_DELIM))
        return 0;

    u4Addr = OSIX_HTONL (u4Addr);

    return u4Addr;

}

/*----------------------------------------------------------------------------
Procedure    : RadGetVendorAttr()
Description  : This module Validates the received Radius packet for vendor
               specific attribute for Microsoft MPPE Keys.
               It extracts Send and Recv keys from the attribute data based
               on the vendor subtype
               
Input(s)     : pu1VendorSubtypePos - Start of MS Vendor subtype in Radius msg
               u1SubType  - Supported type by the vendor
               pu4AttrLen - Length of the vendor subtype attribute
               
               
Output(s)    : pu4AttrLen

Returns      : Pointer to the subtype key (send or recv)

Called By    : RadFillInterfaceStructure()

Calling      : 
              
-----------------------------------------------------------------------------*/

PRIVATE UINT1      *
RadGetVendorAttr (UINT1 *pu1VendorSubtypePos, UINT1 u1SubType,
                  UINT4 *pu4AttrLen)
{
    UINT1              *pu1Pos = NULL;
    UINT1              *pu1Key = NULL;
    UINT1               u1VendorLength = 0;
    UINT1               u1VendorType = 0;

    if ((pu1VendorSubtypePos == NULL) || (pu4AttrLen == NULL))
    {
        return (NULL);
    }

    /* Get the vendor-type */
    pu1Pos = pu1VendorSubtypePos;
    MEMCPY (&u1VendorType, pu1Pos, BYTE_LEN_1);

    if (u1VendorType != u1SubType)
    {
        return (NULL);
    }

    /* Get the vendor-length */
    pu1Pos += BYTE_LEN_1;
    MEMCPY (&u1VendorLength, pu1Pos, BYTE_LEN_1);

    if (u1VendorLength <= BYTE_LEN_4)    /* RFC 2548, Sec 2.4.2 Page 20 */
    {
        return (NULL);
    }

    u1VendorLength -= BYTE_LEN_2;

    /* Allocate memory for data with attr length - attr_hdr_size */
    pu1Key = MemAllocMemBlk (gRadMemPoolId.u4RadKeyPoolId);

    if (pu1Key == NULL)
    {
        return (NULL);
    }

    /* Get the vendor attr data */
    pu1Pos += BYTE_LEN_1;
    MEMCPY (pu1Key, pu1Pos, u1VendorLength);

    *pu4AttrLen = u1VendorLength;

    return (pu1Key);
}

/*----------------------------------------------------------------------------
Procedure    : RadDecryptMsMppeKey()
Description  : This module decrypts the MPPE Recv key sent by Radius
               server as per RFC 2548 Sec 2.4.3              
               
Input(s)     : pu1Key - Encrypted key 
               u4KeyLen - Encrypted key length          
               pu1ReqAuth - Request Authenticator field in the radius packet
               pu1SharedSecret - Shared secret between radius client and server
               u4SharedSecretLen - Shared secret length 
               
               
Output(s)    : pu4DecryptedKeyLen - Length of decrypted key

Returns      : Pointer to the decrypted key

Called By    : RadFillInterfaceStructure()

Calling      : arMD5_start(), arMD5_update(), arMD5_finish()
              
-----------------------------------------------------------------------------*/

UINT1              *
RadDecryptMsMppeKey (UINT1 *pu1Key, UINT4 u4KeyLen, UINT1 *pu1ReqAuth,
                     UINT1 *pu1SharedSecret, UINT4 u4SharedSecretLen,
                     UINT2 *pu2DecryptedKeyLen)
{
    unArCryptoHash      context;
    UINT1               au1MD5Hash[16];
    UINT1              *pu1PlainText = NULL;
    UINT1              *pu1PlainPos = NULL;
    UINT1              *pu1DecryptedMppeKey = NULL;
    UINT1              *pu1Pos = NULL;
    UINT4               u4Remaining;
    UINT4               u4PlainTextLen;
    INT4                i4Index;
    INT4                i4FirstTime = TRUE;

    /* RFC 2548: Page 22
     * pu1Key contains: Salt + Encrypted Key (2 Bytes + keylen)
     * Encrypted Key length should be >= 16 
     */
    if (u4KeyLen < (LEN_SA_MS_MPPE_SALT + 16))
    {
        RAD_ERR_PRINT1 ("RadDecryptMsMppeKey(): MS Key length (%d) "
                        "too short!\r\n", u4KeyLen);
        return (NULL);
    }

    MEMSET (au1MD5Hash, 0, sizeof (au1MD5Hash));

    pu1Pos = pu1Key + LEN_SA_MS_MPPE_SALT;
    u4Remaining = u4KeyLen - LEN_SA_MS_MPPE_SALT;

    if (u4Remaining % 16)
    {
        RAD_ERR_PRINT1 ("RadDecryptMsMppeKey(): Invalid MS Key length (%d) "
                        "too short!\r\n", u4Remaining);
        return (NULL);
    }

    u4PlainTextLen = u4Remaining;

    pu1PlainPos = pu1PlainText = MemAllocMemBlk (gRadMemPoolId.u4Radu2PoolId);

    if (pu1PlainText == NULL)
    {
        RAD_ERR_PRINT ("RadDecryptMsMppeKey(): Mem alloc failure\r\n");
        return (NULL);
    }

    while (u4Remaining > 0)
    {
        /* ---------------------------------- 
         * RFC 2548: Refer Sec 2.4.2 or 2.4.3
         * ----------------------------------
         * b(1) = MD5(Shared-Secret + Req-Authenticator + Salt)
         * C(1) = p(1) xor b(1)
         *
         * b(2) = MD5(Shared-Secret + C(1))
         * C(2) = p(2) xor b(2)
         * 
         * b(i) = MD5(Shared-Secret + Cipher(i-1)
         * C(i) = C(1) + C(2) + ... + C(i)
         */

        arMD5_start (&context);
        arMD5_update (&context, pu1SharedSecret, u4SharedSecretLen);
        if (i4FirstTime == TRUE)
        {
            arMD5_update (&context, pu1ReqAuth, 16);
            arMD5_update (&context, pu1Key, LEN_SA_MS_MPPE_SALT);    /* Salt */
            i4FirstTime = FALSE;
        }
        else
        {
            arMD5_update (&context, pu1Pos - 16, 16);
        }

        arMD5_finish (&context, au1MD5Hash);

        for (i4Index = 0; i4Index < 16; i4Index++)
        {
            *pu1PlainPos++ = *pu1Pos++ ^ au1MD5Hash[i4Index];
        }

        u4Remaining -= 16;

    }                            /* while ( u4Remaining > 0 ) */

    if (pu1PlainText[0] > u4PlainTextLen - 1)
    {
        RAD_ERR_PRINT ("RadDecryptMsMppeKey(): MPPE key decrypt failure\r\n");
        MemReleaseMemBlock (gRadMemPoolId.u4Radu2PoolId,
                            (UINT1 *) pu1PlainText);
        return (NULL);
    }

    pu1DecryptedMppeKey = UtilRadKeyAllocBufferSize ();
    if (pu1DecryptedMppeKey == NULL)
    {
        MemReleaseMemBlock (gRadMemPoolId.u4Radu2PoolId,
                            (UINT1 *) pu1PlainText);
        return (NULL);
    }

    MEMCPY (pu1DecryptedMppeKey, pu1PlainText + 1, pu1PlainText[0]);

    if (pu2DecryptedKeyLen)
    {
        *pu2DecryptedKeyLen = pu1PlainText[0];
    }

    MemReleaseMemBlock (gRadMemPoolId.u4Radu2PoolId, (UINT1 *) pu1PlainText);
    return (pu1DecryptedMppeKey);

}

/* 
 * RadUtilHexDump():
 * Util to dump bytes of specified length in hex format 
 */
VOID
RadUtilHexDump (UINT1 *pu1Bytes, UINT4 u4Len)
{
    UINT4               u4Index;

    for (u4Index = 0; u4Index < u4Len; u4Index++)
    {
        RAD_PACKET_PRINT1 (" %02x ", *(pu1Bytes + u4Index));
    }
    RAD_PACKET_PRINT ("\r\n");
}

/*----------------------------------------------------------------------------
Procedure    : RadClientToProxyCalculateMsgAuth()
Description  : This routine re-calculates the message authenticator.
Input(s)     : p_u1RadiusReceivedPacket
Returns      : None
-----------------------------------------------------------------------------*/
#ifdef __STDC__
VOID
RadClientToProxyCalculateMsgAuth (UINT1 *p_u1RadiusReceivedPacket,
                                  UINT1 *pu1Secret)
#else
VOID
RadClientToProxyCalculateMsgAuth (p_u1RadiusReceivedPacket, pu1Secret)
     UINT1              *p_u1RadiusReceivedPacket;
     UINT1              *pu1Secret;
#endif
{
    unUtilAlgo          UtilAlgo;
    UINT2               u2_pktoffset, u2_pktlen, u2_attrlen;
    UINT1               u1_attrtype;
    UINT1               a_u1CalSignature[LEN_MESSAGE_AUTHENTICATOR];
    UINT2               text_len;
    INT4                i4_SecLen;

    UINT1               a_u1AttrLenTable[] =
        { 0, 0, 0, LEN_CHAP_PASSWORD, LEN_NAS_IP_ADDRESS,
        LEN_NAS_PORT, LEN_SERVICE_TYPE,
        LEN_FRAMED_PROTOCOL, LEN_FRAMED_IP_ADDRESS,
        LEN_FRAMED_IP_NETMASK, LEN_FRAMED_ROUTING,
        0, LEN_FRAMED_MTU, LEN_FRAMED_COMPRESSION,
        LEN_LOGIN_IP_HOST, LEN_LOGIN_SERVICE,
        LEN_LOGIN_TCP_PORT, 0, 0, 0, 0, 0, 0,
        LEN_FRAMED_IPX_NETWORK, 0, 0, 0,
        LEN_SESSION_TIMEOUT, LEN_IDLE_TIMEOUT,
        LEN_TERMINATION_ACTION, 0, 0, 0, 0, 0, 0,
        LEN_LOGIN_LAT_GROUP,
        LEN_FRAMED_APPLETALK_LINK,
        LEN_FRAMED_APPLETALK_NETWORK,
        0, LEN_ACCT_STATUS_TYPE, LEN_ACCT_DELAY_TIME,
        LEN_ACCT_INPUT_OCTETS, LEN_ACCT_OUTPUT_OCTETS,
        0, LEN_ACCT_AUTHENTIC, LEN_ACCT_SESSION_TIME,
        LEN_ACCT_INPUT_PACKETS,
        LEN_ACCT_OUTPUT_PACKETS,
        LEN_ACCT_TERMINATE_CAUSE, 0,
        LEN_ACCT_LINK_COUNT, 0, 0, 0, 0, 0, 0, 0, 0,
        0, LEN_NAS_PORT_TYPE, LEN_PORT_LIMIT, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
    };

    MEMSET (a_u1CalSignature, 0, LEN_MESSAGE_AUTHENTICATOR);
    MEMSET (&UtilAlgo, 0, sizeof (unUtilAlgo));
    MEMCPY (&u2_pktlen, p_u1RadiusReceivedPacket + PKT_LEN, 2);
    u2_pktlen = OSIX_NTOHS (u2_pktlen);
    u2_pktoffset = PKT_ATTR;

    for (;;)
    {
        if (u2_pktoffset >= u2_pktlen)
        {
            break;
        }
        u1_attrtype = *(p_u1RadiusReceivedPacket + u2_pktoffset);

        if (u1_attrtype > A_MESSAGE_AUTHENTICATOR || u1_attrtype < A_USER_NAME)
        {
            u2_pktoffset += *(p_u1RadiusReceivedPacket + u2_pktoffset + 1);
            continue;
        }
        if ((u2_attrlen = a_u1AttrLenTable[u1_attrtype]))
        {
            if (u2_attrlen != *(p_u1RadiusReceivedPacket + u2_pktoffset + 1))
            {
                return;
            }
            u2_pktoffset += u2_attrlen;
        }
        else
        {
            switch (u1_attrtype)
            {
                case A_MESSAGE_AUTHENTICATOR:
                    if (*(p_u1RadiusReceivedPacket + u2_pktoffset + 1) <
                        LEN_MESSAGE_AUTHENTICATOR)
                    {

                        return;
                    }
                    MEMSET (p_u1RadiusReceivedPacket + u2_pktoffset +
                            LEN_RAD_ATTRIBUTE, 0, LEN_MESSAGE_AUTHENTICATOR);
                    MEMCPY ((char *) &text_len, p_u1RadiusReceivedPacket + 2,
                            2);
                    text_len = OSIX_NTOHS (text_len);
                    i4_SecLen = STRLEN (pu1Secret);

                    UtilAlgo.UtilHmacAlgo.pu1HmacPktDataPtr =
                        (unsigned char *) p_u1RadiusReceivedPacket;
                    UtilAlgo.UtilHmacAlgo.i4HmacPktLength = text_len;
                    UtilAlgo.UtilHmacAlgo.pu1HmacKey = pu1Secret;
                    UtilAlgo.UtilHmacAlgo.u4HmacKeyLen = i4_SecLen;
                    UtilAlgo.UtilHmacAlgo.pu1HmacOutDigest =
                        (unsigned char *) &a_u1CalSignature;

                    UtilHash (ISS_UTIL_ALGO_HMAC_MD5, &UtilAlgo);

                    MEMCPY (p_u1RadiusReceivedPacket + u2_pktoffset + 2,
                            a_u1CalSignature, LEN_MESSAGE_AUTHENTICATOR);

                    break;

                default:
                    break;
            }                    /* switch */
            u2_pktoffset += *(p_u1RadiusReceivedPacket + u2_pktoffset + 1);
        }                        /* else */
    }                            /* for */
    return;
}

/*----------------------------------------------------------------------------
Procedure    : RadServerToProxyCalculateMsgAuth()
Description    : This module fills the attributes corresponding to the input
              present in the Radius input.
Input(s)    : a_u1RadHiddenPassword
              i4_PLength
              &u2_RadiusPacketLength
              p_u1RadiusPacketAuth
              p_RadiusInputAuth
Output(s)    : p_u1RadiusPacketAuth
Returns        : None
Called By    : radiusAuthentication
Calling        : None
Author        : G.Vedavinayagam
Date        : 11/11/97
odifications    :
-----------------------------------------------------------------------------*/
#ifdef __STDC__
VOID
RadServerToProxyCalculateMsgAuth (UINT1 *p_u1RadiusReceivedPacket,
                                  UINT1 *pu1ReqAuth)
#else
VOID
RadServerToProxyCalculateMsgAuth (p_u1RadiusReceivedPacket, pu1ReqAuth)
     UINT1              *p_u1RadiusReceivedPacket;
     UINT1              *pu1ReqAuth;
#endif
{
    unUtilAlgo          UtilAlgo;
    UINT2               u2_pktoffset, u2_pktlen, u2_attrlen, u2len;
    UINT1               u1_attrtype;
    UINT1               a_u1CalSignature[LEN_MESSAGE_AUTHENTICATOR];
    UINT2               text_len;
    INT4                i4_SecLen;

    UINT1               a_u1ResponseAuth[LEN_REQ_AUTH_AUTH];
    UINT1               a_u1AttrLenTable[] =
        { 0, 0, 0, LEN_CHAP_PASSWORD, LEN_NAS_IP_ADDRESS,
        LEN_NAS_PORT, LEN_SERVICE_TYPE,
        LEN_FRAMED_PROTOCOL, LEN_FRAMED_IP_ADDRESS,
        LEN_FRAMED_IP_NETMASK, LEN_FRAMED_ROUTING,
        0, LEN_FRAMED_MTU, LEN_FRAMED_COMPRESSION,
        LEN_LOGIN_IP_HOST, LEN_LOGIN_SERVICE,
        LEN_LOGIN_TCP_PORT, 0, 0, 0, 0, 0, 0,
        LEN_FRAMED_IPX_NETWORK, 0, 0, 0,
        LEN_SESSION_TIMEOUT, LEN_IDLE_TIMEOUT,
        LEN_TERMINATION_ACTION, 0, 0, 0, 0, 0, 0,
        LEN_LOGIN_LAT_GROUP,
        LEN_FRAMED_APPLETALK_LINK,
        LEN_FRAMED_APPLETALK_NETWORK,
        0, LEN_ACCT_STATUS_TYPE, LEN_ACCT_DELAY_TIME,
        LEN_ACCT_INPUT_OCTETS, LEN_ACCT_OUTPUT_OCTETS,
        0, LEN_ACCT_AUTHENTIC, LEN_ACCT_SESSION_TIME,
        LEN_ACCT_INPUT_PACKETS,
        LEN_ACCT_OUTPUT_PACKETS,
        LEN_ACCT_TERMINATE_CAUSE, 0,
        LEN_ACCT_LINK_COUNT, 0, 0, 0, 0, 0, 0, 0, 0,
        0, LEN_NAS_PORT_TYPE, LEN_PORT_LIMIT, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
    };

    MEMCPY (&u2_pktlen, p_u1RadiusReceivedPacket + PKT_LEN, 2);
    u2_pktlen = OSIX_NTOHS (u2_pktlen);
    u2_pktoffset = PKT_ATTR;
    MEMSET (&UtilAlgo, 0, sizeof (unUtilAlgo));
    MEMSET (a_u1ResponseAuth, 0, LEN_REQ_AUTH_AUTH);
    MEMSET (a_u1CalSignature, 0, LEN_MESSAGE_AUTHENTICATOR);
    for (;;)
    {
        if (u2_pktoffset >= u2_pktlen)
        {
            break;
        }
        u1_attrtype = *(p_u1RadiusReceivedPacket + u2_pktoffset);

        if (u1_attrtype > A_MESSAGE_AUTHENTICATOR || u1_attrtype < A_USER_NAME)
        {
            u2_pktoffset += *(p_u1RadiusReceivedPacket + u2_pktoffset + 1);
            continue;
        }
        if ((u2_attrlen = a_u1AttrLenTable[u1_attrtype]))
        {
            if (u2_attrlen != *(p_u1RadiusReceivedPacket + u2_pktoffset + 1))
            {

                return;
            }
            u2_pktoffset += u2_attrlen;
        }
        else
        {
            switch (u1_attrtype)
            {

                case A_MESSAGE_AUTHENTICATOR:
                    if (*(p_u1RadiusReceivedPacket + u2_pktoffset + 1) <
                        LEN_MESSAGE_AUTHENTICATOR)
                    {
                        return;
                    }

                    MEMSET (p_u1RadiusReceivedPacket + u2_pktoffset +
                            LEN_RAD_ATTRIBUTE, 0, LEN_MESSAGE_AUTHENTICATOR);

                    MEMCPY (p_u1RadiusReceivedPacket + PKT_REQA, pu1ReqAuth,
                            LEN_REQ_AUTH_AUTH);

                    MEMCPY ((char *) &text_len, p_u1RadiusReceivedPacket + 2,
                            2);
                    text_len = OSIX_HTONS (text_len);

                    STRNCPY (ga_u1ProxyClientSecret, "FutureWLANRD",
                             STRLEN ("FutureWLANRD"));
                    ga_u1ProxyClientSecret[STRLEN ("FutureWLANRD")] = '\0';

                    i4_SecLen =
                        (UINT1) STRLEN ((char *) ga_u1ProxyClientSecret);

                    UtilAlgo.UtilHmacAlgo.pu1HmacPktDataPtr =
                        (unsigned char *) p_u1RadiusReceivedPacket;
                    UtilAlgo.UtilHmacAlgo.i4HmacPktLength = text_len;
                    UtilAlgo.UtilHmacAlgo.pu1HmacKey = ga_u1ProxyClientSecret;
                    UtilAlgo.UtilHmacAlgo.u4HmacKeyLen = i4_SecLen;
                    UtilAlgo.UtilHmacAlgo.pu1HmacOutDigest =
                        (unsigned char *) &a_u1CalSignature;

                    UtilHash (ISS_UTIL_ALGO_HMAC_MD5, &UtilAlgo);

                    MEMCPY (p_u1RadiusReceivedPacket + u2_pktoffset + 2,
                            a_u1CalSignature, LEN_MESSAGE_AUTHENTICATOR);
                    MEMCPY (&u2len, p_u1RadiusReceivedPacket + PKT_LEN, 2);
                    u2len = OSIX_NTOHS (u2len);
                    radiusProxyMakeResAuth (p_u1RadiusReceivedPacket,
                                            a_u1ResponseAuth);
                    MEMCPY (p_u1RadiusReceivedPacket + PKT_REQA,
                            &a_u1ResponseAuth, LEN_REQ_AUTH_AUTH);
                    break;

                default:
                    break;
            }                    /* switch */
            u2_pktoffset += *(p_u1RadiusReceivedPacket + u2_pktoffset + 1);
        }                        /* else */
    }                            /* for */

    return;
}

/*---------------------------------------------------------------------------
Procedure    : radiusProxyMakeResAuth()
Description    : This module makes the reponse authenticator from the packet
              received and the packet transmited corresponding to the
              received packet. This module concatenates Code, Identifier,
              Length, Request authentiator transmitted, attributes and the
              shared secret. The digest value of the concatenated string is
              the Response authenticator.
Input(s)    : p_u1RadiusReceivedPacket
              p_RadiusRequest
Output(s)    : a_u1ResponseAuth
Returns        : None
              RADIUS_MESSAGE_DIGEST()
-----------------------------------------------------------------------------*/
#ifdef __STDC__
VOID
radiusProxyMakeResAuth (UINT1 *p_u1RadiusReceivedPacket,
                        UINT1 a_u1ResponseAuth[])
#else
VOID
radiusProxyMakeResAuth (p_u1RadiusReceivedPacket, a_u1ResponseAuth)
     UINT1              *p_u1RadiusReceivedPacket;
     UINT1               a_u1ResponseAuth[];
#endif
{

    UINT1              *pa_u1Concatenated = NULL;
    INT4                i4_StrLen;

    pa_u1Concatenated = MemAllocMemBlk (gRadMemPoolId.u4RadConcatPoolId);

    if (NULL == pa_u1Concatenated)
    {
        RAD_ERR_PRINT ("Error in allocating memory for pa_u1Concatenated\n");
        return;
    }
    MEMSET (pa_u1Concatenated, 0, sizeof (tu1Concatenated));

    radiusMakeProxyStringVal (p_u1RadiusReceivedPacket,
                              pa_u1Concatenated, &i4_StrLen);
    RADIUS_MESSAGE_DIGEST (a_u1ResponseAuth, pa_u1Concatenated, i4_StrLen);

    MemReleaseMemBlock (gRadMemPoolId.u4RadConcatPoolId,
                        (UINT1 *) pa_u1Concatenated);

}

/*----------------------------------------------------------------------------
Procedure    : radiusMakeProxyStringVal()
Decription    : This module concatenates the received packet with response
              authenticator replaced by request authenticator transmitted
              and the shared secret.
Input(s)    : p_Radiusrequest
              p_u1RadiusReceivedPacket
Output(s)    : a_u1Concatenated
              i4_StrLen
Returns        : None
Called By    : radiusMakeResAuth()
Calling        : None
Author        : G.Vedavinayagam
Date        : 11/13/97
odifications    :
----------------------------------------------------------------------------*/
#ifdef __STDC__
VOID
radiusMakeProxyStringVal (UINT1 *p_u1RadiusReceivedPacket,
                          UINT1 a_u1Concatenated[], INT4 *i4_Length)
#else
VOID
radiusMakeProxyStringVal (p_u1RadiusReceivedPacket, a_u1Concatenated, i4_Length)
     UINT1              *p_u1RadiusReceivedPacket;
     UINT1               a_u1Concatenated[];
     INT4               *i4_Length;
#endif
{
    INT4                i4_seclen;
    UINT2               u2_pktlen;

    MEMSET (a_u1Concatenated, 0, LEN_RX_PKT + LEN_SECRET);

    MEMCPY (&u2_pktlen, p_u1RadiusReceivedPacket + PKT_LEN, 2);
    u2_pktlen = OSIX_NTOHS (u2_pktlen);

    MEMCPY (a_u1Concatenated, p_u1RadiusReceivedPacket, u2_pktlen);
    i4_seclen = STRLEN ((char *) "FutureWLANRD");
    MEMCPY (a_u1Concatenated + u2_pktlen, "FutureWLANRD", i4_seclen);
    *i4_Length = u2_pktlen + i4_seclen;
}

/*-----------------------------------------------------------------------------
Procedure    : RadProxyFillInterfaceStructure()
Description    : This function fill the interfcae structure with the 
          response from the server.
Input(s)    : None
Output(s)    : None
Returns        : None
Called By    : The application using RADIUS.
Calling        : Callback Function() provided by the user module.
-----------------------------------------------------------------------------*/

VOID
RadProxyFillInterfaceStructure (UINT1 *pu1RespPkt, UINT1 *pu1ReqAuth,
                                UINT1 *pu1Secret)
{
    tRadInterface      *pIface = NULL;
    tRAD_MS_MPPE_KEYS  *pKeys = NULL;
    UINT1              *pu1EncryptedKey = NULL;
    UINT1              *pu1Key = NULL;
    UINT4               u4KeyLen = 0;
    UINT2               u2RadPktLen = 0;
    UINT4               u4RadPktOffset = 0;
    UINT1               u1RadAttrType = 0;
    UINT1               u1RadAttrLen = 0;
    UINT1               u1SubType = 0;

    if ((pu1RespPkt == NULL))
    {
        return;
    }

    pIface = (tRadInterface *) (VOID *) UtlShMemAllocRadInterface ();
    if (pIface == NULL)
    {
        return;
    }

    RadInitInterface (pIface);

    pIface->Access = pu1RespPkt[0];    /* Access ACCEPT/REJECT/CHALLENGE  */
    pIface->RequestId = pu1RespPkt[1];

    MEMCPY (&u2RadPktLen, pu1RespPkt + PKT_LEN, 2);
    u2RadPktLen = OSIX_NTOHS (u2RadPktLen);
    u4RadPktOffset = PKT_ATTR;

    pKeys = &pIface->MsMppeKeys;    /* WSS ADD */
    RAD_EVENT_PRINT ("\nATTRIBUTES in the Received Packet:-\n");
    for (;;)
    {
        if (u4RadPktOffset >= u2RadPktLen)
        {
            break;
        }

        u1RadAttrType = *(pu1RespPkt + u4RadPktOffset);
        u1RadAttrLen = *(pu1RespPkt + u4RadPktOffset + 1);

        switch (u1RadAttrType)
        {

            case A_VENDOR_SPECIFIC:

            {
                UINT4               u4VendorId = 0;
                UINT1              *pu1VendorSubtypePos = NULL;

                if (u1RadAttrLen < RAD_ATTR_LEN)
                {
                    break;
                }

                /* BYTE         1               2               3               4
                   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
                   |     Type      |  Length       |            Vendor-Id
                   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
                   Vendor-Id (cont)           |  String...
                   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-
                 */

                /* Get the Vendor ID */
                MEMCPY (&u4VendorId, pu1RespPkt + u4RadPktOffset + BYTE_LEN_2,
                        BYTE_LEN_4);

                /* Check for Microsoft Vendor Id */
                if (OSIX_NTOHL ((u4VendorId)) != MS_CHAP_VENDOR_ID)
                {
                    break;
                }

                RAD_RESPONSE_PRINT ("Microsoft Vendor-id attr received\r\n");

                /* Vendor specific data starts at the following offset:
                 * Attr Type(1) + Atr Len(1) + Vendor Id (4) = 6 Bytes
                 */
                pu1VendorSubtypePos = (pu1RespPkt + u4RadPktOffset +
                                       BYTE_LEN_2 + BYTE_LEN_4);

                /* Supported Microsoft Vendor Specific RADIUS Attribute:
                 * MS-MPPE-Recv-Key */
                u1SubType = *pu1VendorSubtypePos;

                /* Get MS-MPPE-Recv-Key */
                pu1Key = RadProxyGetVendorAttr (pu1VendorSubtypePos,
                                                u1SubType, &u4KeyLen);

                if (pu1Key)
                {
                    /* Decrypt receive key */
                    pKeys->pu1MppeRecvKey =
                        RadDecryptMsMppeKey (pu1Key, u4KeyLen,
                                             pu1ReqAuth,
                                             pu1Secret,
                                             STRLEN (pu1Secret),
                                             &pKeys->u2MppeRecvKeyLen);

                    if (pKeys->pu1MppeRecvKey == NULL)
                    {
                        MemReleaseMemBlock (gRadMemPoolId.u4RadKeyPoolId,
                                            (UINT1 *) pu1Key);
                        break;
                    }

                    pu1EncryptedKey =
                        RadRecvEncryptMsMppeKey (pKeys->pu1MppeRecvKey,
                                                 pKeys->u2MppeRecvKeyLen,
                                                 pu1ReqAuth,
                                                 (UINT1 *) "FutureWLANRD", 12,
                                                 &pKeys->u2MppeRecvKeyLen,
                                                 pu1Key);
                    if (NULL == pu1EncryptedKey)
                    {
                        MemReleaseMemBlock (gRadMemPoolId.u4RadKeyPoolId,
                                            (UINT1 *) pu1Key);
                        UtilRadKeyReleaseBufferSize ((UINT1 *) pKeys->
                                                     pu1MppeRecvKey);
                        break;
                    }

                    MEMCPY ((pu1VendorSubtypePos + LEN_SA_MS_MPPE_SALT +
                             BYTE_LEN_2), pu1EncryptedKey,
                            pKeys->u2MppeRecvKeyLen);
                    MemReleaseMemBlock (gRadMemPoolId.u4Radu2PoolId,
                                        (UINT1 *) pu1EncryptedKey);
                    MemReleaseMemBlock (gRadMemPoolId.u4RadKeyPoolId,
                                        (UINT1 *) pu1Key);
                    KW_FALSEPOSITIVE_FIX (pKeys->pu1MppeRecvKey);
                }

                break;
            }

            default:
                break;

        }                        /* end of switch */

        u4RadPktOffset += u1RadAttrLen;
    }                            /* for loop */
    UtlShMemFreeRadInterface ((UINT1 *) pIface);
    return;
}

/*----------------------------------------------------------------------------
Procedure    : RadProxyGetVendorAttr()
Description  : This module Validates the received Radius packet for vendor
               specific attribute for Microsoft MPPE Keys.
               It extracts Send and Recv keys from the attribute data based
               on the vendor subtype
               
Input(s)     : pu1VendorSubtypePos - Start of MS Vendor subtype in Radius msg
               u1SubType  - Supported type by the vendor
               pu4AttrLen - Length of the vendor subtype attribute
               
               
Output(s)    : pu4AttrLen

Returns      : Pointer to the subtype key (send or recv)

Called By    : RadFillInterfaceStructure()

Calling      : 
              
-----------------------------------------------------------------------------*/

PRIVATE UINT1      *
RadProxyGetVendorAttr (UINT1 *pu1VendorSubtypePos, UINT1 u1SubType,
                       UINT4 *pu4AttrLen)
{
    UINT1              *pu1Pos = NULL;
    UINT1              *pu1Key = NULL;
    UINT1               u1VendorLength = 0;
    UINT1               u1VendorType = 0;

    if ((pu1VendorSubtypePos == NULL) || (pu4AttrLen == NULL))
    {
        return (NULL);
    }

    /* Get the vendor-type */
    pu1Pos = pu1VendorSubtypePos;
    MEMCPY (&u1VendorType, pu1Pos, BYTE_LEN_1);

    if (u1VendorType != u1SubType)
    {
        return (NULL);
    }

    /* Get the vendor-length */
    pu1Pos += BYTE_LEN_1;
    MEMCPY (&u1VendorLength, pu1Pos, BYTE_LEN_1);

    if (u1VendorLength <= BYTE_LEN_4)    /* RFC 2548, Sec 2.4.2 Page 20 */
    {
        return (NULL);
    }

    u1VendorLength -= BYTE_LEN_2;

    /* Allocate memory for data with attr length - attr_hdr_size */
    pu1Key = MemAllocMemBlk (gRadMemPoolId.u4RadKeyPoolId);

    if (pu1Key == NULL)
    {
        return (NULL);
    }

    /* Get the vendor attr data */
    pu1Pos += BYTE_LEN_1;
    MEMCPY (pu1Key, pu1Pos, u1VendorLength);

    *pu4AttrLen = u1VendorLength;

    return (pu1Key);
}

/*----------------------------------------------------------------------------
Procedure    : RadRecvEncryptMsMppeKey()
Description  : This module encrypt  the MPPE Recv key sent by Radius
               server as per RFC 2548 Sec 2.4.3              
               
Input(s)     : pu1Key - Decrypte  key 
               u4KeyLen - Decrypted key length          
               pu1ReqAuth - Request Authenticator field in the radius packet
               pu1SharedSecret - Shared secret between radius client and server
               u4SharedSecretLen - Shared secret length 
               
               
Output(s)    : pu2EncryptedKeyLen - Length of encrypted key

Returns      : Pointer to the encrypted key

Called By    : RadFillInterfaceStructure()

Calling      : arMD5_start(), arMD5_update(), arMD5_finish()
              
-----------------------------------------------------------------------------*/
UINT1              *
RadRecvEncryptMsMppeKey (UINT1 *pu1Key, UINT4 u4KeyLen, UINT1 *pu1ReqAuth,
                         UINT1 *pu1SharedSecret, UINT4 u4SharedSecretLen,
                         UINT2 *pu2EncryptedKeyLen, UINT1 *pu1Salt)
{

    UINT1              *pu1EncrBuf = NULL;
    UINT1              *pu1EncrBufPos = NULL;
    UINT4               u4EncrLen = 0;    /* No of Bytes to be encryped */
    unArCryptoHash      Context;    /* Stores MD5 Context */
    INT4                i4FirstTime = TRUE;
    UINT1               au1MD5Hash[16];
    INT4                i4Index = 0;

    *pu2EncryptedKeyLen = 0;
    MEMSET (au1MD5Hash, 0, sizeof (au1MD5Hash));

    /* The extra one byte is for storing the length of Key */
    /* It be taken into account for calculating the Padding */
    /* Padding done to make size even multiple of 16 */
    u4EncrLen = 1 + u4KeyLen;
    if (u4EncrLen & 0x0f)
    {
        /* Padding is done to make the encrypted */
        /* an even multiple of 16 Octets */
        u4EncrLen = (u4EncrLen & 0xf0) + 16;

    }

    pu1EncrBuf = MemAllocMemBlk (gRadMemPoolId.u4Radu2PoolId);

    if (pu1EncrBuf == NULL)
    {
        return (NULL);
    }

    /* Initialized to Zero for Padding Bytes to be Zero */
    MEMSET (pu1EncrBuf, 0, u4EncrLen);

    /* Store the Key Length in Buffer */
    pu1EncrBuf[0] = (UINT1) u4KeyLen;

    MEMCPY ((pu1EncrBuf + 1), pu1Key, u4KeyLen);
    /* Store the Key Len of the encrypted text */
    *pu2EncryptedKeyLen = (UINT2) u4EncrLen;

    pu1EncrBufPos = pu1EncrBuf;

    while (u4EncrLen > 0)
    {
        arMD5_start (&Context);
        arMD5_update (&Context, pu1SharedSecret, u4SharedSecretLen);
        if (i4FirstTime == TRUE)
        {
            arMD5_update (&Context, pu1ReqAuth, 16);
            arMD5_update (&Context, pu1Salt, LEN_SA_MS_MPPE_SALT);
            i4FirstTime = FALSE;
        }
        else
        {
            arMD5_update (&Context, (pu1EncrBufPos - 16), 16);
        }
        arMD5_finish (&Context, au1MD5Hash);
        for (i4Index = 0; i4Index < 16; i4Index++)
        {
            *pu1EncrBufPos = *pu1EncrBufPos ^ au1MD5Hash[i4Index];
            pu1EncrBufPos++;
        }

        u4EncrLen -= 16;
    }

    return (pu1EncrBuf);
}

/*----------------------------------------------------------------------------
Procedure    : RadFormNewPacket()
Description  : This routine forms a new radius packet with the proxy-state
               attribute removed
Input(s)     : pu1RadiusReceivedPacket
             : pu2DestPort
             : pu4ClientIpAddr
             : pu1ReqAuth
Output(s)    : pu1NewRadiusPacket
Returns      : None
-----------------------------------------------------------------------------*/
#ifdef __STDC__
VOID
RadFormNewPacket (UINT1 *pu1RadiusReceivedPacket, UINT1 *pu1NewRadiusPacket,
                  UINT2 *pu2DestPort, UINT4 *pu4ClientIpAddr, UINT1 *pu1ReqAuth)
#else
VOID
RadFormNewPacket (pu1RadiusReceivedPacket, pu1NewRadiusPacket,
                  pu2DestPort, pu4ClientIpAddr, pu1ReqAuth)
     UINT1              *pu1RadiusReceivedPacket;
     UINT1              *pu1NewRadiusPacket;
     UINT2              *pu2DestPort;
     UINT4              *pu4ClientIpAddr;
     UINT1              *pu1ReqAuth;
#endif
{
    UINT2               u2_pktoffset, u2PktLen, u2_attrlen, u2NewPktLen;
    UINT1               u1_attrtype;
    UINT1               u1ProxyStateLen;
    UINT2               u2PktLenTillProxyAttr;

    UINT1               a_u1AttrLenTable[] =
        { 0, 0, 0, LEN_CHAP_PASSWORD, LEN_NAS_IP_ADDRESS,
        LEN_NAS_PORT, LEN_SERVICE_TYPE,
        LEN_FRAMED_PROTOCOL, LEN_FRAMED_IP_ADDRESS,
        LEN_FRAMED_IP_NETMASK, LEN_FRAMED_ROUTING,
        0, LEN_FRAMED_MTU, LEN_FRAMED_COMPRESSION,
        LEN_LOGIN_IP_HOST, LEN_LOGIN_SERVICE,
        LEN_LOGIN_TCP_PORT, 0, 0, 0, 0, 0, 0,
        LEN_FRAMED_IPX_NETWORK, 0, 0, 0,
        LEN_SESSION_TIMEOUT, LEN_IDLE_TIMEOUT,
        LEN_TERMINATION_ACTION, 0, 0, 0, 0, 0, 0,
        LEN_LOGIN_LAT_GROUP,
        LEN_FRAMED_APPLETALK_LINK,
        LEN_FRAMED_APPLETALK_NETWORK,
        0, LEN_ACCT_STATUS_TYPE, LEN_ACCT_DELAY_TIME,
        LEN_ACCT_INPUT_OCTETS, LEN_ACCT_OUTPUT_OCTETS,
        0, LEN_ACCT_AUTHENTIC, LEN_ACCT_SESSION_TIME,
        LEN_ACCT_INPUT_PACKETS,
        LEN_ACCT_OUTPUT_PACKETS,
        LEN_ACCT_TERMINATE_CAUSE, 0,
        LEN_ACCT_LINK_COUNT, 0, 0, 0, 0, 0, 0, 0, 0,
        0, LEN_NAS_PORT_TYPE, LEN_PORT_LIMIT, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
    };

    MEMCPY (&u2PktLen, pu1RadiusReceivedPacket + PKT_LEN, 2);
    u2PktLen = OSIX_NTOHS (u2PktLen);
    MEMCPY (pu1NewRadiusPacket, pu1RadiusReceivedPacket, PKT_ATTR);
    u2_pktoffset = PKT_ATTR;
    u2NewPktLen = 0;

    for (;;)
    {
        if (u2_pktoffset >= u2PktLen)
        {
            break;
        }
        u1_attrtype = *(pu1RadiusReceivedPacket + u2_pktoffset);

        if (u1_attrtype > A_MESSAGE_AUTHENTICATOR || u1_attrtype < A_USER_NAME)
        {
            u2_pktoffset += *(pu1RadiusReceivedPacket + u2_pktoffset + 1);
            continue;
        }
        if ((u2_attrlen = a_u1AttrLenTable[u1_attrtype]))
        {
            if (u2_attrlen != *(pu1RadiusReceivedPacket + u2_pktoffset + 1))
            {

                return;
            }
            u2_pktoffset += u2_attrlen;
        }
        else
        {
            switch (u1_attrtype)
            {

                case A_PROXY_STATE:
                    if (*(pu1RadiusReceivedPacket + u2_pktoffset + 1) <
                        LEN_MIN_PROXY_STATE)
                    {
                        return;
                    }
                    /* Proxy State Attribute Format 
                     * Attr Type(1) + AttrLen(1) + u2DestPort(2) + 
                     * u4ClientIpAddr(4) + 
                     * Request Authenticator(16) = 24Bytes */
                    MEMCPY (pu2DestPort,
                            pu1RadiusReceivedPacket + u2_pktoffset + BYTE_LEN_2,
                            2);
                    MEMCPY (pu4ClientIpAddr,
                            pu1RadiusReceivedPacket + u2_pktoffset + BYTE_LEN_4,
                            4);
                    MEMCPY (pu1ReqAuth,
                            pu1RadiusReceivedPacket + u2_pktoffset +
                            BYTE_LEN_4 + BYTE_LEN_4, LEN_REQ_AUTH_AUTH);

                    MEMCPY (&u1ProxyStateLen, pu1RadiusReceivedPacket +
                            u2_pktoffset + 1, 1);
                    u2PktLenTillProxyAttr = (UINT2) (u2_pktoffset - PKT_ATTR);

                    MEMCPY (pu1NewRadiusPacket + PKT_ATTR,
                            pu1RadiusReceivedPacket + PKT_ATTR,
                            u2PktLenTillProxyAttr);
                    u2NewPktLen = (UINT2) (u2PktLen - u1ProxyStateLen);
                    u2NewPktLen = OSIX_HTONS (u2NewPktLen);
                    break;

                default:
                    break;
            }                    /* switch */
            u2_pktoffset += *(pu1RadiusReceivedPacket + u2_pktoffset + 1);
        }                        /* else */
    }                            /* for */
    MEMCPY (pu1NewRadiusPacket + PKT_LEN, &u2NewPktLen, 2);
    return;
}

/************************** END OF FILE "radrecv.c" **************************/
