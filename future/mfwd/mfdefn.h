/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mfdefn.h,v 1.13 2014/08/23 11:58:43 siva Exp $
 *
 ********************************************************************/
#ifndef _MFWD_DEFN_H_
#define _MFWD_DEFN_H_


#define MFWD_CMDB_STATUS_DISABLED   2
#define MFWD_CMDB_STATUS_ENABLED    1

#define  MFWD_UPD_CMD_BITMAP  \
       (MFWD_ENTRY_UPDATE_IIF_CMD | MFWD_ENTRY_DELETE_OIF_CMD |  MFWD_ENTRY_ADD_OIF_CMD | MFWD_ENTRY_OIF_STATE_CMD | MFWD_ENTRY_UPD_DELIVERMDP_FLAG | MFWD_ENTRY_UPDATE_IIFLIST_CMD) 


#define       MFWD_MDP_MESSAGE      1
#define       MFWD_MRP_MESSAGE      2
#define       MFWD_MDP6_MESSAGE     3
#define       MFWD_REG_MESSAGE      4   
#define       MFWD_DEREG_MESSAGE    5   


/* Definitions for the NextHop Nodes in the OifNod of the MRT */
#define  MFWD_OIF_NEXTHOP_STATE_PRUNED        0x01
#define  MFWD_OIF_NEXTHOP_STATE_FORWARDING    0x02

#define  MFWD_OIF_PRUNED        0x01
#define  MFWD_OIF_FORWARDING    0x02

/* Mappings for the Q IDs */
#define  MFWD_IP_Q_ID   gpMfwdContext.MfwdQId

#define  MFWD_MAX_MSGHDLR_FUNPTR   4

#define  MFWD_MSGQ_POOLID          gMfwdContext.MsgQPool.poolId
#define  MFWD_Q_ID                 gMfwdContext.MfwdQId
#define  MFWD_CQ_ID                gMfwdContext.MfwdCQId
#define  MFWD_TASK_ID              gMfwdContext.MfwdTaskId

#define  MFWD_HASH_TABLE_SIZE                  256

#define  MFWD_MCAST_WC_GROUP                 ((UINT4) 0xe0000000)
#define  MFWD_FIRST_HOP_ROUTER                  2
#define  MFWD_DEFAULT_TTL_VALUE                 255

#define  MFWD_BAD_MCAST_SOCKET                   -1
#define  MFWD_INVALID_MCAST_IF                   0xFFFFFFFF

/* defintions for mapping into the correct pool ids to the appropriate
 * pools and to the block sizes of the pools.
 */
#define  MFWD_OWNER_NODE_POOL_ID        gMfwdContext.OwnerPool.poolId
#define  CACHE_MISS_DB_NODE_POOLID      gMfwdContext.CMDBNodePool.poolId
#define  CACHE_MISS_DATA_PKTS_POOLID    gMfwdContext.CMDataPktsPool.poolId
#define  MFWD_IF_POOL_ID                 gMfwdContext.IfNodePool.poolId
#define   MFWD_SRC_NODE_PID              gMfwdContext.SrcInfoPool.poolId
#define   MFWD_GRP_NODE_PID            gMfwdContext.GrpNodePool.poolId
#define   MFWD_RT_NODE_PID             gMfwdContext.RtEntryPool.poolId
#define   MFWD_OIF_NODE_PID            gMfwdContext.OifNodePool.poolId
#define   MFWD_IIF_NODE_PID            gMfwdContext.IifNodePool.poolId
#define   MFWD_NEXTHOP_NODE_PID        gMfwdContext.NextHopNodePool.poolId
#define  MFWD_OWNER_INFO_TABLE_POOL_ID  gMfwdContext.OwnerInfoTablePool.poolId

/* Macros to access the block sizes of the various memory pools created */
         
#define  MFWD_MSGQ_POOL_BLOCK_SIZE   gMfwdContext.MsgQPool.i4BlockSize

#define  MFWD_OWNER_NODE_POOL_BLOCK_SIZE \
                                   gMfwdContext.OwnerPool.i4BlockSize

#define  CACHE_MISS_DB_POOL_BLOCK_SIZE  \
                                   gMfwdContext.CMDBNodePool.i4BlockSize

#define  CACHE_MISS_DATA_PKTS_POOL_BLOCK_SIZE \
                                   gMfwdContext.CMDataPktsPool.i4BlockSize

#define  MFWD_IF_POOL_BLOCK_SIZE \
                                   gMfwdContext.IfNodePool.i4BlockSize

#define  MFWD_GRP_NODE_POOL_BLOCK_SIZE\
                                   gMfwdContext.GrpNodePool.i4BlockSize
             
#define  MFWD_SRC_NODE_POOL_BLOCK_SIZE \
                                   gMfwdContext.SrcInfoPool.i4BlockSize

#define  MFWD_RT_NODE_POOL_BLOCK_SIZE \
                                   gMfwdContext.RtEntryPool.i4BlockSize

#define  MFWD_OIF_NODE_POOL_BLOCK_SIZE \
                                   gMfwdContext.OifNodePool.i4BlockSize

#define  MFWD_IIF_NODE_POOL_BLOCK_SIZE \
                                   gMfwdContext.IifNodePool.i4BlockSize

#define  MFWD_NEXTHOP_NODE_POOL_BLOCK_SIZE \
                                   gMfwdContext.NextHopNodePool.i4BlockSize

#define  MFWD_OWNER_INFO_TABLE_POOL_BLOCK_SIZE \
                                         gMfwdContext.OwnerInfoTablePool.i4BlockSize

#define  MAX_MRP_INSTANCES         (gMfwdContext.u2MaxMrps)
#define  MFWD_MAX_INTERFACES       (gMfwdContext.u4MaxIfaces)

/* for the present it is a constant value */
#define  MAX_CACHE_MISS_DB_PKTS         100

#define  MFWD_MEMALLOC MEM_MALLOC
#define  MFWD_MEMFREE  MEM_FREE
#define  MFWD_MEMSET   MEMSET
#define MFWD_MEMCPY    MEMCPY

#define  MFWD_MRP_UPD_HEADER_SIZE      sizeof (tMrpUpdMesgHdr)
#define  MFWD_CMDB_TIMER_DUR           10 * (SYS_TIME_TICKS_IN_A_SEC)
#define  MFWD_CMDB_TIMER_LIST_ID       gMfwdContext.CMDBTmrListId

#define  MFWD_GET_IF_NODE(IfIndex, u1AddrType)  \
         MfwdOimGetInterfaceNode(IfIndex, u1AddrType)

#define  MFWD_LINEAR_IF_TABLE (gMfwdContext.InterfaceTbl).pLinIfTbl

#define  MFWD_IF_HASH_TABLE (gMfwdContext.InterfaceTbl).pHashIfTbl
                         
#define  MFWD_IF_HASH_KEY (gMfwdContext.InterfaceTbl).u4HashKey

#define  MFWD_MAX_IF_HASH_TABLE_SIZE      255


#define  MFWD_MUTEX_SEMA4             "MFD"
#define  MFWD_MUTEX_SEMID             gMfwdContext.MfwdMutex.semId

/* macros for easier access to the owner information node data */
#define  MFWD_OWNER(u2OwnerId)    (gMfwdContext.pOwnerInfoTbl)[u2OwnerId]

#define  MFWD_OWNER_MODE(u2OwnerId)  \
      (gMfwdContext.pOwnerInfoTbl)[u2OwnerId]->u1OwnerMode


#define  MFWD_OWNER_MAX_GROUPS(u2OwnerId)  \
      (gMfwdContext.pOwnerInfoTbl)[u2OwnerId]->u4MaxGrps

#define  MFWD_OWNER_MAX_IFACES(u2OwnerId)  \
      (gMfwdContext.pOwnerInfoTbl)[u2OwnerId]->u4MaxIfaces

#define  MFWD_OWNER_MRT(u2OwnerId)  \
       (gMfwdContext.pOwnerInfoTbl[u2OwnerId])->pMrtTable

#define  MFWD_IF_CMDB(pIfNode)  pIfNode->CmdbList

/* Its not commented */
#define  MFWD_OWNER_SEMA4(u2OwnerId) \
          gMfwdContext.pOwnerInfoTbl[u2OwnerId]->OwnerNodeSema4.au1SemName
#define  MFWD_OWNERID_FROM_TMR(tmrnode) ((UINT2) tmrnode.u4Data)

#define  IF_UPD_DATA_OFFSET       sizeof (tMrpUpdMesgHdr)
#define  MRT_UPD_DATA_OFFSET      sizeof (tMrpUpdMesgHdr)

#define  MFWD_STAR_STAR_ENTRY    1
#define  MFWD_STARG_ENTRY        2
#define  MFWD_SG_ENTRY           3

#define  MFWD_ADDRESS_EXACT_MASK    0xFFFFFFFF
#define  MFWD_TRUE               1
#define  MFWD_FALSE              0
   


#define MFWD_IP_HDR_LEN              20
#define MFWD_IP_PKT_OFF_CKSUM    10
#define MFWD_IP_VERS(Ver4Hlen4)            ((UINT1)(Ver4Hlen4 >> 4))
#define MFWD_IP_OLEN(Ver4Hlen4)\
          ((UINT1)(((Ver4Hlen4 & 0x0f) * 4) - MFWD_IP_HDR_LEN))

#define   MFWD_IP_VERS_AND_HLEN(i1Version, i2Opt_len) \
         ((UINT1)((i1Version << 4) | (UINT1)((i2Opt_len + MFWD_IP_HDR_LEN)>>2)))
#define  MFWD_EXTRACT_IP_HEADER   MfwdExtractIpHdr

#define  MFWD_EXTRACT_IPV6_HEADER   MfwdExtractIp6Hdr

#define MFWD_ZERO                0
#define MFWD_IO_MODULE       1
#define MFWD_MDH_MODULE      2
#define MFWD_MRP_MODULE      4
#define MFWD_MGMT_MODULE     8
#define MFWD_ALL_MODULEs     65535
#define MFWD_MAX_INT4                 0x7fffffff
#define CLI_MFWD_TRACE_DIS_FLAG       0x80000000
#define MFWD_MAX_SHORT_INT_VALUE      0xffff
   
#define MFWD_INIT_COMPLETE(u4Status) lrInitComplete(u4Status)

#define MFWD_STAR_G_ROUTE               4

#endif
