# include  "mfinc.h"
# include  "include.h"
# include  "fsmfwlow.h"
# include  "mfwdcmlow.h"

/* Global Context Variable */
extern tMfwdContextStructure gMfwdContext;

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIpMRouteEnable
 Input       :  The Indices

                The Object 
                retValIpMRouteEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpMRouteEnable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIpMRouteEnable (INT4 *pi4RetValIpMRouteEnable)
#else
INT1
nmhGetIpMRouteEnable (pi4RetValIpMRouteEnable)
     INT4               *pi4RetValIpMRouteEnable;
#endif
{
    return (nmhGetIpCmnMRouteEnable (pi4RetValIpMRouteEnable));

}

/****************************************************************************
 Function    :  nmhGetIpMRouteEntryCount
 Input       :  The Indices

                The Object 
                retValIpMRouteEntryCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhGetIpMRouteEntryCount (UINT4 *pu4RetValIpMRouteEntryCount)
#else
INT1
nmhGetIpMRouteEntryCount (pu4RetValIpMRouteEntryCount)
     UINT4              *pu4RetValIpMRouteEntryCount;
#endif
{
    return (nmhGetIpCmnMRouteEntryCount (pu4RetValIpMRouteEntryCount));
}

/****************************************************************************
 Function    :  nmhGetIpMRouteEnableCmdb
 Input       :  The Indices

                The Object 
                retValIpMRouteEnableCmdb
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhGetIpMRouteEnableCmdb (INT4 *pi4RetValIpMRouteEnableCmdb)
#else
INT1
nmhGetIpMRouteEnableCmdb (pi4RetValIpMRouteEnableCmdb)
     INT4               *pi4RetValIpMRouteEnableCmdb;
#endif
{
    return (nmhGetIpCmnMRouteEnableCmdb (pi4RetValIpMRouteEnableCmdb));
}

/****************************************************************************
 Function    :  nmhGetMfwdGlobalTrace
 Input       :  The Indices

                The Object 
                retValMfwdGlobalTrace
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetMfwdGlobalTrace ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetMfwdGlobalTrace (INT4 *pi4RetValMfwdGlobalTrace)
#else
INT1
nmhGetMfwdGlobalTrace (pi4RetValMfwdGlobalTrace)
     INT4               *pi4RetValMfwdGlobalTrace;
#endif
{
    return (nmhGetMfwdCmnGlobalTrace (pi4RetValMfwdGlobalTrace));
}

/****************************************************************************
 Function    :  nmhGetMfwdGlobalDebug
 Input       :  The Indices

                The Object 
                retValMfwdGlobalDebug
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetMfwdGlobalDebug ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetMfwdGlobalDebug (INT4 *pi4RetValMfwdGlobalDebug)
#else
INT1
nmhGetMfwdGlobalDebug (pi4RetValMfwdGlobalDebug)
     INT4               *pi4RetValMfwdGlobalDebug;
#endif
{
    return (nmhGetMfwdCmnGlobalDebug (pi4RetValMfwdGlobalDebug));
}

/****************************************************************************
 Function    :  nmhGetIpMRouteDiscardedPkts
 Input       :  The Indices

                The Object 
                retValIpMRouteDiscardedPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpMRouteDiscardedPkts ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIpMRouteDiscardedPkts (UINT4 *pu4RetValIpMRouteDiscardedPkts)
#else
INT1
nmhGetIpMRouteDiscardedPkts (pu4RetValIpMRouteDiscardedPkts)
     UINT4              *pu4RetValIpMRouteDiscardedPkts;
#endif
{
    return (nmhGetIpCmnMRouteDiscardedPkts (pu4RetValIpMRouteDiscardedPkts));
}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetMfwdAvgDataRate
 Input       :  The Indices

                The Object 
                retValMfwdAvgDataRate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhGetMfwdAvgDataRate (INT4 *pi4RetValMfwdAvgDataRate)
#else
INT1
nmhGetMfwdAvgDataRate (pi4RetValMfwdAvgDataRate)
     INT4               *pi4RetValMfwdAvgDataRate;
#endif
{
    return (nmhGetMfwdCmnAvgDataRate (pi4RetValMfwdAvgDataRate));
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIpMRouteEnable
 Input       :  The Indices

                The Object 
                setValIpMRouteEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetIpMRouteEnable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetIpMRouteEnable (INT4 i4SetValIpMRouteEnable)
#else
INT1
nmhSetIpMRouteEnable (i4SetValIpMRouteEnable)
     INT4                i4SetValIpMRouteEnable;

#endif
{
    return (nmhSetIpCmnMRouteEnable (i4SetValIpMRouteEnable));
}

/****************************************************************************
 Function    :  nmhSetIpMRouteEnableCmdb
 Input       :  The Indices

                The Object 
                setValIpMRouteEnableCmdb
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetIpMRouteEnableCmdb ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetIpMRouteEnableCmdb (INT4 i4SetValIpMRouteEnableCmdb)
#else
INT1
nmhSetIpMRouteEnableCmdb (i4SetValIpMRouteEnableCmdb)
     INT4                i4SetValIpMRouteEnableCmdb;

#endif
{
    return (nmhSetIpCmnMRouteEnableCmdb (i4SetValIpMRouteEnableCmdb));
}

/****************************************************************************
 Function    :  nmhSetMfwdGlobalTrace
 Input       :  The Indices

                The Object 
                setValMfwdGlobalTrace
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetMfwdGlobalTrace ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetMfwdGlobalTrace (INT4 i4SetValMfwdGlobalTrace)
#else
INT1
nmhSetMfwdGlobalTrace (i4SetValMfwdGlobalTrace)
     INT4                i4SetValMfwdGlobalTrace;

#endif
{

    return (nmhSetMfwdCmnGlobalTrace (i4SetValMfwdGlobalTrace));
}

/****************************************************************************
 Function    :  nmhSetMfwdGlobalDebug
 Input       :  The Indices

                The Object 
                setValMfwdGlobalDebug
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetMfwdGlobalDebug ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetMfwdGlobalDebug (INT4 i4SetValMfwdGlobalDebug)
#else
INT1
nmhSetMfwdGlobalDebug (i4SetValMfwdGlobalDebug)
     INT4                i4SetValMfwdGlobalDebug;

#endif
{
    return (nmhSetMfwdCmnGlobalDebug (i4SetValMfwdGlobalDebug));
}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhSetMfwdAvgDataRate
 Input       :  The Indices

                The Object 
                setValMfwdAvgDataRate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhSetMfwdAvgDataRate (INT4 i4SetValMfwdAvgDataRate)
#else
INT1
nmhSetMfwdAvgDataRate (i4SetValMfwdAvgDataRate)
     INT4                i4SetValMfwdAvgDataRate;

#endif
{
    return (nmhSetMfwdCmnAvgDataRate (i4SetValMfwdAvgDataRate));
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2IpMRouteEnable
 Input       :  The Indices

                The Object 
                testValIpMRouteEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2IpMRouteEnable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2IpMRouteEnable (UINT4 *pu4ErrorCode, INT4 i4TestValIpMRouteEnable)
#else
INT1
nmhTestv2IpMRouteEnable (pu4ErrorCode, i4TestValIpMRouteEnable)
     UINT4              *pu4ErrorCode;
     INT4                i4TestValIpMRouteEnable;
#endif
{
    return (nmhTestv2IpCmnMRouteEnable (pu4ErrorCode, i4TestValIpMRouteEnable));
}

/****************************************************************************
 Function    :  nmhTestv2IpMRouteEnableCmdb
 Input       :  The Indices

                The Object 
                testValIpMRouteEnableCmdb
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2IpMRouteEnableCmdb ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2IpMRouteEnableCmdb (UINT4 *pu4ErrorCode,
                             INT4 i4TestValIpMRouteEnableCmdb)
#else
INT1
nmhTestv2IpMRouteEnableCmdb (pu4ErrorCode, i4TestValIpMRouteEnableCmdb)
     UINT4              *pu4ErrorCode;
     INT4                i4TestValIpMRouteEnableCmdb;
#endif
{
    return (nmhTestv2IpCmnMRouteEnableCmdb
            (pu4ErrorCode, i4TestValIpMRouteEnableCmdb));
}

/****************************************************************************
 Function    :  nmhTestv2MfwdGlobalTrace
 Input       :  The Indices

                The Object 
                testValMfwdGlobalTrace
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2MfwdGlobalTrace ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2MfwdGlobalTrace (UINT4 *pu4ErrorCode, INT4 i4TestValMfwdGlobalTrace)
#else
INT1
nmhTestv2MfwdGlobalTrace (pu4ErrorCode, i4TestValMfwdGlobalTrace)
     UINT4              *pu4ErrorCode;
     INT4                i4TestValMfwdGlobalTrace;
#endif
{
    return (nmhTestv2MfwdCmnGlobalTrace
            (pu4ErrorCode, i4TestValMfwdGlobalTrace));
}

/****************************************************************************
 Function    :  nmhTestv2MfwdGlobalDebug
 Input       :  The Indices

                The Object 
                testValMfwdGlobalDebug
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2MfwdGlobalDebug ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2MfwdGlobalDebug (UINT4 *pu4ErrorCode, INT4 i4TestValMfwdGlobalDebug)
#else
INT1
nmhTestv2MfwdGlobalDebug (pu4ErrorCode, i4TestValMfwdGlobalDebug)
     UINT4              *pu4ErrorCode;
     INT4                i4TestValMfwdGlobalDebug;
#endif
{
    return (nmhTestv2MfwdCmnGlobalDebug
            (pu4ErrorCode, i4TestValMfwdGlobalDebug));
}

/* LOW LEVEL Routines for Table : IpMRouteTable. */
/****************************************************************************
 Function    :  nmhTestv2MfwdAvgDataRate
 Input       :  The Indices

                The Object 
                testValMfwdAvgDataRate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhTestv2MfwdAvgDataRate (UINT4 *pu4ErrorCode, INT4 i4TestValMfwdAvgDataRate)
#else
INT1
nmhTestv2MfwdAvgDataRate (pu4ErrorCode, i4TestValMfwdAvgDataRate)
     UINT4              *pu4ErrorCode;
     INT4                i4TestValMfwdAvgDataRate;
#endif
{
    return (nmhTestv2MfwdCmnAvgDataRate
            (pu4ErrorCode, i4TestValMfwdAvgDataRate));
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2IpMRouteEnable
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IpMRouteEnable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2IpMRouteEnableCmdb
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IpMRouteEnableCmdb (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2MfwdGlobalTrace
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2MfwdGlobalTrace (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2MfwdGlobalDebug
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2MfwdGlobalDebug (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2MfwdAvgDataRate
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2MfwdAvgDataRate (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : IpMRouteTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIpMRouteTable
 Input       :  The Indices
                IpMRouteOwnerId
                IpMRouteGroup
                IpMRouteSource
                IpMRouteSourceMask
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhValidateIndexInstanceIpMRouteTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhValidateIndexInstanceIpMRouteTable (INT4 i4IpMRouteOwnerId,
                                       UINT4 u4IpMRouteGroup,
                                       UINT4 u4IpMRouteSource,
                                       UINT4 u4IpMRouteSourceMask)
#else
INT1
nmhValidateIndexInstanceIpMRouteTable (i4IpMRouteOwnerId, u4IpMRouteGroup,
                                       u4IpMRouteSource, u4IpMRouteSourceMask)
     INT4                i4IpMRouteOwnerId;
     UINT4               u4IpMRouteGroup;
     UINT4               u4IpMRouteSource;
     UINT4               u4IpMRouteSourceMask;
#endif
{
    tSNMP_OCTET_STRING_TYPE IpMSource;
    tSNMP_OCTET_STRING_TYPE IpMGroup;
    UINT1               au1GrpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1SrcAddr[IPVX_MAX_INET_ADDR_LEN];

    memset (au1SrcAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    memset (au1GrpAddr, 0, IPVX_MAX_INET_ADDR_LEN);

    IpMSource.pu1_OctetList = au1SrcAddr;
    IpMSource.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    IpMGroup.pu1_OctetList = au1GrpAddr;
    IpMGroup.i4_Length = IPVX_MAX_INET_ADDR_LEN;
    u4IpMRouteSource = OSIX_NTOHL (u4IpMRouteSource);
    memcpy (IpMSource.pu1_OctetList,
            (UINT1 *) &u4IpMRouteSource, sizeof (UINT4));

    u4IpMRouteGroup = OSIX_NTOHL (u4IpMRouteGroup);
    memcpy (IpMGroup.pu1_OctetList, (UINT1 *) &u4IpMRouteGroup, sizeof (UINT4));

    return (nmhValidateIndexInstanceIpCmnMRouteTable (i4IpMRouteOwnerId,
                                                      IPVX_ADDR_FMLY_IPV4,
                                                      &IpMGroup,
                                                      &IpMSource,
                                                      (INT4)
                                                      u4IpMRouteSourceMask));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIpMRouteTable
 Input       :  The Indices
                IpMRouteOwnerId
                IpMRouteGroup
                IpMRouteSource
                IpMRouteSourceMask
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhGetFirstIpMRouteTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFirstIndexIpMRouteTable (INT4 *pi4IpMRouteOwnerId,
                               UINT4 *pu4IpMRouteGroup,
                               UINT4 *pu4IpMRouteSource,
                               UINT4 *pu4IpMRouteSourceMask)
#else
INT1
nmhGetFirstIndexIpMRouteTable (pi4IpMRouteOwnerId, pu4IpMRouteGroup,
                               pu4IpMRouteSource, pu4IpMRouteSourceMask)
     INT4               *pi4IpMRouteOwnerId;
     UINT4              *pu4IpMRouteGroup;
     UINT4              *pu4IpMRouteSource;
     UINT4              *pu4IpMRouteSourceMask;
#endif
{

    tSNMP_OCTET_STRING_TYPE IpMRouteGroup;
    tSNMP_OCTET_STRING_TYPE IpMRouteSource;
    UINT1               au1GrpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1SrcAddr[IPVX_MAX_INET_ADDR_LEN];
    INT4                i4AddrType = 0;
    UINT4               u4RetGrpIpAddr = 0;
    UINT4               u4RetSrcIpAddr = 0;

    memset (au1SrcAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    memset (au1GrpAddr, 0, IPVX_MAX_INET_ADDR_LEN);

    IpMRouteSource.pu1_OctetList = au1SrcAddr;
    IpMRouteSource.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    IpMRouteGroup.pu1_OctetList = au1GrpAddr;
    IpMRouteGroup.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    memcpy (IpMRouteSource.pu1_OctetList,
            (UINT1 *) pu4IpMRouteSource, sizeof (UINT4));

    memcpy (IpMRouteGroup.pu1_OctetList,
            (UINT1 *) pu4IpMRouteGroup, sizeof (UINT4));

    if (nmhGetFirstIndexIpCmnMRouteTable (pi4IpMRouteOwnerId,
                                          &i4AddrType,
                                          &IpMRouteGroup,
                                          &IpMRouteSource,
                                          (INT4 *) pu4IpMRouteSourceMask) ==
        SNMP_SUCCESS)
    {

        PTR_FETCH4 (u4RetGrpIpAddr, IpMRouteGroup.pu1_OctetList);
        PTR_FETCH4 (u4RetSrcIpAddr, IpMRouteSource.pu1_OctetList);

        u4RetGrpIpAddr = OSIX_NTOHL (u4RetGrpIpAddr);
        memcpy (pu4IpMRouteGroup, (UINT1 *) &u4RetGrpIpAddr, sizeof (UINT4));
        u4RetSrcIpAddr = OSIX_NTOHL (u4RetSrcIpAddr);
        memcpy (pu4IpMRouteSource, (UINT1 *) &u4RetSrcIpAddr, sizeof (UINT4));

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetNextIndexIpMRouteTable
 Input       :  The Indices
                IpMRouteOwnerId
                nextIpMRouteOwnerId
                IpMRouteGroup
                nextIpMRouteGroup
                IpMRouteSource
                nextIpMRouteSource
                IpMRouteSourceMask
                nextIpMRouteSourceMask
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
 /*** $$TRACE_PROCEDURE_NAME = nmhGetNextIpMRouteTable ***/
 /*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetNextIndexIpMRouteTable (INT4 i4IpMRouteOwnerId,
                              INT4 *pi4NextIpMRouteOwnerId,
                              UINT4 u4IpMRouteGroup,
                              UINT4 *pu4NextIpMRouteGroup,
                              UINT4 u4IpMRouteSource,
                              UINT4 *pu4NextIpMRouteSource,
                              UINT4 u4IpMRouteSourceMask,
                              UINT4 *pu4NextIpMRouteSourceMask)
#else
INT1
nmhGetNextIndexIpMRouteTable (i4IpMRouteOwnerId, pi4NextIpMRouteOwnerId,
                              u4IpMRouteGroup, pu4NextIpMRouteGroup,
                              u4IpMRouteSource, pu4NextIpMRouteSource,
                              u4IpMRouteSourceMask, pu4NextIpMRouteSourceMask)
     INT4                i4IpMRouteOwnerId;
     INT4               *pi4NextIpMRouteOwnerId;
     UINT4               u4IpMRouteGroup;
     UINT4              *pu4NextIpMRouteGroup;
     UINT4               u4IpMRouteSource;
     UINT4              *pu4NextIpMRouteSource;
     UINT4               u4IpMRouteSourceMask;
     UINT4              *pu4NextIpMRouteSourceMask;
#endif
{

    INT4                i4NextAddrType = 0;
    tSNMP_OCTET_STRING_TYPE NextIpMRouteGroup;
    tSNMP_OCTET_STRING_TYPE IpMRouteGroup;
    tSNMP_OCTET_STRING_TYPE NextIpMRouteSource;
    tSNMP_OCTET_STRING_TYPE IpMRouteSource;
    UINT1               au1MRouteSource[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1NextMRouteSource[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1MRouteGroup[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1NextMRouteGroup[IPVX_MAX_INET_ADDR_LEN];
    UINT4               u4RetNextGrpIpAddr = 0;
    UINT4               u4RetNextSrcIpAddr = 0;

    memset (au1MRouteSource, 0, IPVX_MAX_INET_ADDR_LEN);
    memset (au1NextMRouteSource, 0, IPVX_MAX_INET_ADDR_LEN);
    memset (au1MRouteGroup, 0, IPVX_MAX_INET_ADDR_LEN);
    memset (au1NextMRouteGroup, 0, IPVX_MAX_INET_ADDR_LEN);

    IpMRouteSource.pu1_OctetList = au1MRouteSource;
    IpMRouteSource.i4_Length = IPVX_MAX_INET_ADDR_LEN;
    NextIpMRouteSource.pu1_OctetList = au1NextMRouteSource;
    NextIpMRouteSource.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    IpMRouteGroup.pu1_OctetList = au1MRouteGroup;
    IpMRouteGroup.i4_Length = IPVX_MAX_INET_ADDR_LEN;
    NextIpMRouteGroup.pu1_OctetList = au1NextMRouteGroup;
    NextIpMRouteGroup.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    u4IpMRouteSource = OSIX_NTOHL (u4IpMRouteSource);
    memcpy (IpMRouteSource.pu1_OctetList,
            (UINT1 *) &u4IpMRouteSource, sizeof (UINT4));

    memcpy (NextIpMRouteSource.pu1_OctetList,
            (UINT1 *) pu4NextIpMRouteSource, sizeof (UINT4));

    u4IpMRouteGroup = OSIX_NTOHL (u4IpMRouteGroup);
    memcpy (IpMRouteGroup.pu1_OctetList,
            (UINT1 *) &u4IpMRouteGroup, sizeof (UINT4));

    memcpy (NextIpMRouteGroup.pu1_OctetList,
            (UINT1 *) pu4NextIpMRouteGroup, sizeof (UINT4));

    if (nmhGetNextIndexIpCmnMRouteTable (i4IpMRouteOwnerId,
                                         pi4NextIpMRouteOwnerId,
                                         IPVX_ADDR_FMLY_IPV4,
                                         &i4NextAddrType,
                                         &IpMRouteGroup,
                                         &NextIpMRouteGroup,
                                         &IpMRouteSource,
                                         &NextIpMRouteSource,
                                         u4IpMRouteSourceMask,
                                         (INT4 *) pu4NextIpMRouteSourceMask) ==
        SNMP_SUCCESS)
    {

        if (i4NextAddrType == IPVX_ADDR_FMLY_IPV6)
        {

            return SNMP_FAILURE;

        }

        PTR_FETCH4 (u4RetNextGrpIpAddr, NextIpMRouteGroup.pu1_OctetList);
        PTR_FETCH4 (u4RetNextSrcIpAddr, NextIpMRouteSource.pu1_OctetList);

        u4RetNextGrpIpAddr = OSIX_NTOHL (u4RetNextGrpIpAddr);
        memcpy (pu4NextIpMRouteGroup,
                (UINT1 *) &u4RetNextGrpIpAddr, sizeof (UINT4));

        u4RetNextSrcIpAddr = OSIX_NTOHL (u4RetNextSrcIpAddr);
        memcpy (pu4NextIpMRouteSource,
                (UINT1 *) &u4RetNextSrcIpAddr, sizeof (UINT4));

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIpMRouteUpstreamNeighbor
 Input       :  The Indices
                IpMRouteOwnerId
                IpMRouteGroup
                IpMRouteSource
                IpMRouteSourceMask

                The Object 
                retValIpMRouteUpstreamNeighbor
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpMRouteUpstreamNeighbor ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIpMRouteUpstreamNeighbor (INT4 i4IpMRouteOwnerId, UINT4 u4IpMRouteGroup,
                                UINT4 u4IpMRouteSource,
                                UINT4 u4IpMRouteSourceMask,
                                UINT4 *pu4RetValIpMRouteUpstreamNeighbor)
#else
INT1
nmhGetIpMRouteUpstreamNeighbor (i4IpMRouteOwnerId, u4IpMRouteGroup,
                                u4IpMRouteSource, u4IpMRouteSourceMask,
                                pu4RetValIpMRouteUpstreamNeighbor)
     INT4                i4IpMRouteOwnerId;
     UINT4               u4IpMRouteGroup;
     UINT4               u4IpMRouteSource;
     UINT4               u4IpMRouteSourceMask;
     UINT4              *pu4RetValIpMRouteUpstreamNeighbor;
#endif
{

    tSNMP_OCTET_STRING_TYPE IpMRouteUpStreamNeighbor;
    tSNMP_OCTET_STRING_TYPE IpMRouteSource;
    tSNMP_OCTET_STRING_TYPE IpMRouteGroup;
    UINT1               au1GrpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1SrcAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1UpstreamAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT4               u4NextUpStreamNeighbor = 0;

    memset (au1SrcAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    memset (au1UpstreamAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    memset (au1GrpAddr, 0, IPVX_MAX_INET_ADDR_LEN);

    IpMRouteSource.pu1_OctetList = au1SrcAddr;
    IpMRouteSource.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    IpMRouteGroup.pu1_OctetList = au1GrpAddr;
    IpMRouteGroup.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    IpMRouteUpStreamNeighbor.pu1_OctetList = au1UpstreamAddr;
    IpMRouteUpStreamNeighbor.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    u4IpMRouteSource = OSIX_NTOHL (u4IpMRouteSource);
    memcpy (IpMRouteSource.pu1_OctetList,
            (UINT1 *) &u4IpMRouteSource, sizeof (UINT4));

    u4IpMRouteGroup = OSIX_NTOHL (u4IpMRouteGroup);
    memcpy (IpMRouteGroup.pu1_OctetList,
            (UINT1 *) &u4IpMRouteGroup, sizeof (UINT4));

    if (nmhGetIpCmnMRouteUpstreamNeighbor (i4IpMRouteOwnerId,
                                           IPVX_ADDR_FMLY_IPV4,
                                           &IpMRouteGroup,
                                           &IpMRouteSource,
                                           u4IpMRouteSourceMask,
                                           &IpMRouteUpStreamNeighbor) ==
        SNMP_SUCCESS)
    {

        PTR_FETCH4 (u4NextUpStreamNeighbor,
                    IpMRouteUpStreamNeighbor.pu1_OctetList);
        memcpy (pu4RetValIpMRouteUpstreamNeighbor,
                (UINT1 *) &u4NextUpStreamNeighbor, sizeof (UINT4));
        return SNMP_SUCCESS;

    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIpMRouteInIfIndex
 Input       :  The Indices
                IpMRouteOwnerId
                IpMRouteGroup
                IpMRouteSource
                IpMRouteSourceMask

                The Object 
                retValIpMRouteInIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpMRouteInIfIndex ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIpMRouteInIfIndex (INT4 i4IpMRouteOwnerId, UINT4 u4IpMRouteGroup,
                         UINT4 u4IpMRouteSource, UINT4 u4IpMRouteSourceMask,
                         INT4 *pi4RetValIpMRouteInIfIndex)
#else
INT1
nmhGetIpMRouteInIfIndex (i4IpMRouteOwnerId, u4IpMRouteGroup, u4IpMRouteSource,
                         u4IpMRouteSourceMask, pi4RetValIpMRouteInIfIndex)
     INT4                i4IpMRouteOwnerId;
     UINT4               u4IpMRouteGroup;
     UINT4               u4IpMRouteSource;
     UINT4               u4IpMRouteSourceMask;
     INT4               *pi4RetValIpMRouteInIfIndex;
#endif
{

    tSNMP_OCTET_STRING_TYPE IpMRouteSource;
    tSNMP_OCTET_STRING_TYPE IpMRouteGroup;
    UINT1               au1GrpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1SrcAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1UpstreamAddr[IPVX_MAX_INET_ADDR_LEN];

    memset (au1SrcAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    memset (au1UpstreamAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    memset (au1GrpAddr, 0, IPVX_MAX_INET_ADDR_LEN);

    IpMRouteSource.pu1_OctetList = au1SrcAddr;
    IpMRouteSource.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    IpMRouteGroup.pu1_OctetList = au1GrpAddr;
    IpMRouteGroup.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    u4IpMRouteSource = OSIX_NTOHL (u4IpMRouteSource);
    memcpy (IpMRouteSource.pu1_OctetList,
            (UINT1 *) &u4IpMRouteSource, sizeof (UINT4));

    u4IpMRouteGroup = OSIX_NTOHL (u4IpMRouteGroup);
    memcpy (IpMRouteGroup.pu1_OctetList,
            (UINT1 *) &u4IpMRouteGroup, sizeof (UINT4));

    return (nmhGetIpCmnMRouteInIfIndex (i4IpMRouteOwnerId,
                                        IPVX_ADDR_FMLY_IPV4,
                                        &IpMRouteGroup,
                                        &IpMRouteSource,
                                        u4IpMRouteSourceMask,
                                        pi4RetValIpMRouteInIfIndex));
}

/****************************************************************************
 Function    :  nmhGetIpMRouteUpTime
 Input       :  The Indices
                IpMRouteOwnerId
                IpMRouteGroup
                IpMRouteSource
                IpMRouteSourceMask

                The Object 
                retValIpMRouteUpTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpMRouteUpTime ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIpMRouteUpTime (INT4 i4IpMRouteOwnerId, UINT4 u4IpMRouteGroup,
                      UINT4 u4IpMRouteSource, UINT4 u4IpMRouteSourceMask,
                      UINT4 *pu4RetValIpMRouteUpTime)
#else
INT1
nmhGetIpMRouteUpTime (i4IpMRouteOwnerId, u4IpMRouteGroup, u4IpMRouteSource,
                      u4IpMRouteSourceMask, pu4RetValIpMRouteUpTime)
     INT4                i4IpMRouteOwnerId;
     UINT4               u4IpMRouteGroup;
     UINT4               u4IpMRouteSource;
     UINT4               u4IpMRouteSourceMask;
     UINT4              *pu4RetValIpMRouteUpTime;
#endif
{

    tSNMP_OCTET_STRING_TYPE IpMRouteSource;
    tSNMP_OCTET_STRING_TYPE IpMRouteGroup;
    UINT1               au1GrpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1SrcAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1UpstreamAddr[IPVX_MAX_INET_ADDR_LEN];

    memset (au1SrcAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    memset (au1UpstreamAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    memset (au1GrpAddr, 0, IPVX_MAX_INET_ADDR_LEN);

    IpMRouteSource.pu1_OctetList = au1SrcAddr;
    IpMRouteSource.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    IpMRouteGroup.pu1_OctetList = au1GrpAddr;
    IpMRouteGroup.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    u4IpMRouteSource = OSIX_NTOHL (u4IpMRouteSource);
    memcpy (IpMRouteSource.pu1_OctetList,
            (UINT1 *) &u4IpMRouteSource, sizeof (UINT4));

    u4IpMRouteGroup = OSIX_NTOHL (u4IpMRouteGroup);
    memcpy (IpMRouteGroup.pu1_OctetList,
            (UINT1 *) &u4IpMRouteGroup, sizeof (UINT4));

    return (nmhGetIpCmnMRouteUpTime (i4IpMRouteOwnerId,
                                     IPVX_ADDR_FMLY_IPV4,
                                     &IpMRouteGroup,
                                     &IpMRouteSource,
                                     (INT4) u4IpMRouteSourceMask,
                                     pu4RetValIpMRouteUpTime));

}

/****************************************************************************
 Function    :  nmhGetIpMRoutePkts
 Input       :  The Indices
                IpMRouteOwnerId
                IpMRouteGroup
                IpMRouteSource
                IpMRouteSourceMask

                The Object 
                retValIpMRoutePkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpMRoutePkts ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIpMRoutePkts (INT4 i4IpMRouteOwnerId, UINT4 u4IpMRouteGroup,
                    UINT4 u4IpMRouteSource, UINT4 u4IpMRouteSourceMask,
                    UINT4 *pu4RetValIpMRoutePkts)
#else
INT1
nmhGetIpMRoutePkts (i4IpMRouteOwnerId, u4IpMRouteGroup, u4IpMRouteSource,
                    u4IpMRouteSourceMask, pu4RetValIpMRoutePkts)
     INT4                i4IpMRouteOwnerId;
     UINT4               u4IpMRouteGroup;
     UINT4               u4IpMRouteSource;
     UINT4               u4IpMRouteSourceMask;
     UINT4              *pu4RetValIpMRoutePkts;
#endif
{

    tSNMP_OCTET_STRING_TYPE IpMRouteSource;
    tSNMP_OCTET_STRING_TYPE IpMRouteGroup;
    UINT1               au1GrpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1SrcAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1UpstreamAddr[IPVX_MAX_INET_ADDR_LEN];

    memset (au1SrcAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    memset (au1UpstreamAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    memset (au1GrpAddr, 0, IPVX_MAX_INET_ADDR_LEN);

    IpMRouteSource.pu1_OctetList = au1SrcAddr;
    IpMRouteSource.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    IpMRouteGroup.pu1_OctetList = au1GrpAddr;
    IpMRouteGroup.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    u4IpMRouteSource = OSIX_NTOHL (u4IpMRouteSource);
    memcpy (IpMRouteSource.pu1_OctetList,
            (UINT1 *) &u4IpMRouteSource, sizeof (UINT4));

    u4IpMRouteGroup = OSIX_NTOHL (u4IpMRouteGroup);
    memcpy (IpMRouteGroup.pu1_OctetList,
            (UINT1 *) &u4IpMRouteGroup, sizeof (UINT4));

    return (nmhGetIpCmnMRoutePkts (i4IpMRouteOwnerId,
                                   IPVX_ADDR_FMLY_IPV4,
                                   &IpMRouteGroup,
                                   &IpMRouteSource,
                                   (INT4) u4IpMRouteSourceMask,
                                   pu4RetValIpMRoutePkts));

}

/****************************************************************************
 Function    :  nmhGetIpMRouteDifferentInIfPackets
 Input       :  The Indices
                IpMRouteOwnerId
                IpMRouteGroup
                IpMRouteSource
                IpMRouteSourceMask

                The Object 
                retValIpMRouteDifferentInIfPackets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpMRouteDifferentInIfPackets ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIpMRouteDifferentInIfPackets (INT4 i4IpMRouteOwnerId,
                                    UINT4 u4IpMRouteGroup,
                                    UINT4 u4IpMRouteSource,
                                    UINT4 u4IpMRouteSourceMask,
                                    UINT4
                                    *pu4RetValIpMRouteDifferentInIfPackets)
#else
INT1
nmhGetIpMRouteDifferentInIfPackets (i4IpMRouteOwnerId, u4IpMRouteGroup,
                                    u4IpMRouteSource, u4IpMRouteSourceMask,
                                    pu4RetValIpMRouteDifferentInIfPackets)
     INT4                i4IpMRouteOwnerId;
     UINT4               u4IpMRouteGroup;
     UINT4               u4IpMRouteSource;
     UINT4               u4IpMRouteSourceMask;
     UINT4              *pu4RetValIpMRouteDifferentInIfPackets;
#endif
{

    tSNMP_OCTET_STRING_TYPE IpMRouteSource;
    tSNMP_OCTET_STRING_TYPE IpMRouteGroup;
    UINT1               au1GrpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1SrcAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1UpstreamAddr[IPVX_MAX_INET_ADDR_LEN];

    memset (au1SrcAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    memset (au1UpstreamAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    memset (au1GrpAddr, 0, IPVX_MAX_INET_ADDR_LEN);

    IpMRouteSource.pu1_OctetList = au1SrcAddr;
    IpMRouteSource.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    IpMRouteGroup.pu1_OctetList = au1GrpAddr;
    IpMRouteGroup.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    u4IpMRouteSource = OSIX_NTOHL (u4IpMRouteSource);
    memcpy (IpMRouteSource.pu1_OctetList,
            (UINT1 *) &u4IpMRouteSource, sizeof (UINT4));

    u4IpMRouteGroup = OSIX_NTOHL (u4IpMRouteGroup);
    memcpy (IpMRouteGroup.pu1_OctetList,
            (UINT1 *) &u4IpMRouteGroup, sizeof (UINT4));

    return (nmhGetIpCmnMRouteDifferentInIfPackets (i4IpMRouteOwnerId,
                                                   IPVX_ADDR_FMLY_IPV4,
                                                   &IpMRouteGroup,
                                                   &IpMRouteSource,
                                                   u4IpMRouteSourceMask,
                                                   pu4RetValIpMRouteDifferentInIfPackets));
}

/****************************************************************************
 Function    :  nmhGetIpMRouteProtocol
 Input       :  The Indices
                IpMRouteOwnerId
                IpMRouteGroup
                IpMRouteSource
                IpMRouteSourceMask

                The Object 
                retValIpMRouteProtocol
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpMRouteProtocol ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIpMRouteProtocol (INT4 i4IpMRouteOwnerId, UINT4 u4IpMRouteGroup,
                        UINT4 u4IpMRouteSource, UINT4 u4IpMRouteSourceMask,
                        INT4 *pi4RetValIpMRouteProtocol)
#else
INT1
nmhGetIpMRouteProtocol (i4IpMRouteOwnerId, u4IpMRouteGroup, u4IpMRouteSource,
                        u4IpMRouteSourceMask, pi4RetValIpMRouteProtocol)
     INT4                i4IpMRouteOwnerId;
     UINT4               u4IpMRouteGroup;
     UINT4               u4IpMRouteSource;
     UINT4               u4IpMRouteSourceMask;
     INT4               *pi4RetValIpMRouteProtocol;
#endif
{

    tSNMP_OCTET_STRING_TYPE IpMRouteSource;
    tSNMP_OCTET_STRING_TYPE IpMRouteGroup;
    UINT1               au1GrpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1SrcAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1UpstreamAddr[IPVX_MAX_INET_ADDR_LEN];

    memset (au1SrcAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    memset (au1UpstreamAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    memset (au1GrpAddr, 0, IPVX_MAX_INET_ADDR_LEN);

    IpMRouteSource.pu1_OctetList = au1SrcAddr;
    IpMRouteSource.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    IpMRouteGroup.pu1_OctetList = au1GrpAddr;
    IpMRouteGroup.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    u4IpMRouteSource = OSIX_NTOHL (u4IpMRouteSource);
    memcpy (IpMRouteSource.pu1_OctetList,
            (UINT1 *) &u4IpMRouteSource, sizeof (UINT4));

    u4IpMRouteGroup = OSIX_NTOHL (u4IpMRouteGroup);
    memcpy (IpMRouteGroup.pu1_OctetList,
            (UINT1 *) &u4IpMRouteGroup, sizeof (UINT4));

    return (nmhGetIpCmnMRouteProtocol (i4IpMRouteOwnerId,
                                       IPVX_ADDR_FMLY_IPV4,
                                       &IpMRouteGroup,
                                       &IpMRouteSource,
                                       u4IpMRouteSourceMask,
                                       pi4RetValIpMRouteProtocol));

}

/****************************************************************************
 Function    :  nmhGetIpMRouteRtAddress
 Input       :  The Indices
                IpMRouteOwnerId
                IpMRouteGroup
                IpMRouteSource
                IpMRouteSourceMask

                The Object 
                retValIpMRouteRtAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpMRouteRtAddress ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIpMRouteRtAddress (INT4 i4IpMRouteOwnerId, UINT4 u4IpMRouteGroup,
                         UINT4 u4IpMRouteSource, UINT4 u4IpMRouteSourceMask,
                         UINT4 *pu4RetValIpMRouteRtAddress)
#else
INT1
nmhGetIpMRouteRtAddress (i4IpMRouteOwnerId, u4IpMRouteGroup, u4IpMRouteSource,
                         u4IpMRouteSourceMask, pu4RetValIpMRouteRtAddress)
     INT4                i4IpMRouteOwnerId;
     UINT4               u4IpMRouteGroup;
     UINT4               u4IpMRouteSource;
     UINT4               u4IpMRouteSourceMask;
     UINT4              *pu4RetValIpMRouteRtAddress;
#endif
{

    tSNMP_OCTET_STRING_TYPE IpMRouteRtAddress;
    tSNMP_OCTET_STRING_TYPE IpMRouteSource;
    tSNMP_OCTET_STRING_TYPE IpMRouteGroup;
    UINT1               au1GrpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1SrcAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1UpstreamAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1RtAddress[IPVX_MAX_INET_ADDR_LEN];
    UINT4               u4NextRtAddress = 0;

    memset (au1SrcAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    memset (au1UpstreamAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    memset (au1GrpAddr, 0, IPVX_MAX_INET_ADDR_LEN);

    IpMRouteSource.pu1_OctetList = au1SrcAddr;
    IpMRouteSource.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    IpMRouteGroup.pu1_OctetList = au1GrpAddr;
    IpMRouteGroup.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    IpMRouteRtAddress.pu1_OctetList = au1RtAddress;
    IpMRouteRtAddress.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    u4IpMRouteSource = OSIX_NTOHL (u4IpMRouteSource);
    memcpy (IpMRouteSource.pu1_OctetList,
            (UINT1 *) &u4IpMRouteSource, sizeof (UINT4));

    u4IpMRouteGroup = OSIX_NTOHL (u4IpMRouteGroup);
    memcpy (IpMRouteGroup.pu1_OctetList,
            (UINT1 *) &u4IpMRouteGroup, sizeof (UINT4));

    if (nmhGetIpCmnMRouteUpstreamNeighbor (i4IpMRouteOwnerId,
                                           IPVX_ADDR_FMLY_IPV4,
                                           &IpMRouteGroup,
                                           &IpMRouteSource,
                                           u4IpMRouteSourceMask,
                                           &IpMRouteRtAddress) == SNMP_SUCCESS)
    {

        PTR_FETCH4 (u4NextRtAddress, IpMRouteRtAddress.pu1_OctetList);
        memcpy (pu4RetValIpMRouteRtAddress, (UINT1 *) &u4NextRtAddress,
                sizeof (UINT4));
        return SNMP_SUCCESS;

    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIpMRouteRtMask
 Input       :  The Indices
                IpMRouteOwnerId
                IpMRouteGroup
                IpMRouteSource
                IpMRouteSourceMask

                The Object 
                retValIpMRouteRtMask
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpMRouteRtMask ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIpMRouteRtMask (INT4 i4IpMRouteOwnerId, UINT4 u4IpMRouteGroup,
                      UINT4 u4IpMRouteSource, UINT4 u4IpMRouteSourceMask,
                      UINT4 *pu4RetValIpMRouteRtMask)
#else
INT1
nmhGetIpMRouteRtMask (i4IpMRouteOwnerId, u4IpMRouteGroup, u4IpMRouteSource,
                      u4IpMRouteSourceMask, pu4RetValIpMRouteRtMask)
     INT4                i4IpMRouteOwnerId;
     UINT4               u4IpMRouteGroup;
     UINT4               u4IpMRouteSource;
     UINT4               u4IpMRouteSourceMask;
     UINT4              *pu4RetValIpMRouteRtMask;
#endif
{

    tSNMP_OCTET_STRING_TYPE IpMRouteSource;
    tSNMP_OCTET_STRING_TYPE IpMRouteGroup;
    UINT1               au1GrpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1SrcAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1UpstreamAddr[IPVX_MAX_INET_ADDR_LEN];

    memset (au1SrcAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    memset (au1UpstreamAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    memset (au1GrpAddr, 0, IPVX_MAX_INET_ADDR_LEN);

    IpMRouteSource.pu1_OctetList = au1SrcAddr;
    IpMRouteSource.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    IpMRouteGroup.pu1_OctetList = au1GrpAddr;
    IpMRouteGroup.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    u4IpMRouteSource = OSIX_NTOHL (u4IpMRouteSource);
    memcpy (IpMRouteSource.pu1_OctetList,
            (UINT1 *) &u4IpMRouteSource, sizeof (UINT4));

    u4IpMRouteGroup = OSIX_NTOHL (u4IpMRouteGroup);
    memcpy (IpMRouteGroup.pu1_OctetList,
            (UINT1 *) &u4IpMRouteGroup, sizeof (UINT4));

    return (nmhGetIpCmnMRouteRtMask (i4IpMRouteOwnerId,
                                     IPVX_ADDR_FMLY_IPV4,
                                     &IpMRouteGroup,
                                     &IpMRouteSource,
                                     (INT4) u4IpMRouteSourceMask,
                                     (INT4 *) pu4RetValIpMRouteRtMask));

}

/****************************************************************************
 Function    :  nmhGetIpMRouteRtType
 Input       :  The Indices
                IpMRouteOwnerId
                IpMRouteGroup
                IpMRouteSource
                IpMRouteSourceMask

                The Object 
                retValIpMRouteRtType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpMRouteRtType ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIpMRouteRtType (INT4 i4IpMRouteOwnerId, UINT4 u4IpMRouteGroup,
                      UINT4 u4IpMRouteSource, UINT4 u4IpMRouteSourceMask,
                      INT4 *pi4RetValIpMRouteRtType)
#else
INT1
nmhGetIpMRouteRtType (i4IpMRouteOwnerId, u4IpMRouteGroup, u4IpMRouteSource,
                      u4IpMRouteSourceMask, pi4RetValIpMRouteRtType)
     INT4                i4IpMRouteOwnerId;
     UINT4               u4IpMRouteGroup;
     UINT4               u4IpMRouteSource;
     UINT4               u4IpMRouteSourceMask;
     INT4               *pi4RetValIpMRouteRtType;
#endif
{
    tSNMP_OCTET_STRING_TYPE IpMRouteSource;
    tSNMP_OCTET_STRING_TYPE IpMRouteGroup;
    UINT1               au1GrpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1SrcAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1UpstreamAddr[IPVX_MAX_INET_ADDR_LEN];

    memset (au1SrcAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    memset (au1UpstreamAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    memset (au1GrpAddr, 0, IPVX_MAX_INET_ADDR_LEN);

    IpMRouteSource.pu1_OctetList = au1SrcAddr;
    IpMRouteSource.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    IpMRouteGroup.pu1_OctetList = au1GrpAddr;
    IpMRouteGroup.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    u4IpMRouteSource = OSIX_NTOHL (u4IpMRouteSource);
    memcpy (IpMRouteSource.pu1_OctetList,
            (UINT1 *) &u4IpMRouteSource, sizeof (UINT4));

    u4IpMRouteGroup = OSIX_NTOHL (u4IpMRouteGroup);
    memcpy (IpMRouteGroup.pu1_OctetList,
            (UINT1 *) &u4IpMRouteGroup, sizeof (UINT4));

    return (nmhGetIpCmnMRouteRtType (i4IpMRouteOwnerId,
                                     IPVX_ADDR_FMLY_IPV4,
                                     &IpMRouteGroup,
                                     &IpMRouteSource,
                                     u4IpMRouteSourceMask,
                                     pi4RetValIpMRouteRtType));

}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIpMRouteNextHopTable
 Input       :  The Indices
                IpMRouteNextHopOwnerId
                IpMRouteNextHopGroup
                IpMRouteNextHopSource
                IpMRouteNextHopSourceMask
                IpMRouteNextHopIfIndex
                IpMRouteNextHopAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhValidateIndexInstanceIpMRouteNextHopTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhValidateIndexInstanceIpMRouteNextHopTable (INT4 i4IpMRouteNextHopOwnerId,
                                              UINT4 u4IpMRouteNextHopGroup,
                                              UINT4 u4IpMRouteNextHopSource,
                                              UINT4 u4IpMRouteNextHopSourceMask,
                                              INT4 i4IpMRouteNextHopIfIndex,
                                              UINT4 u4IpMRouteNextHopAddress)
#else
INT1
nmhValidateIndexInstanceIpMRouteNextHopTable (i4IpMRouteNextHopOwnerId,
                                              u4IpMRouteNextHopGroup,
                                              u4IpMRouteNextHopSource,
                                              u4IpMRouteNextHopSourceMask,
                                              i4IpMRouteNextHopIfIndex,
                                              u4IpMRouteNextHopAddress)
     INT4                i4IpMRouteNextHopOwnerId;
     UINT4               u4IpMRouteNextHopGroup;
     UINT4               u4IpMRouteNextHopSource;
     UINT4               u4IpMRouteNextHopSourceMask;
     INT4                i4IpMRouteNextHopIfIndex;
     UINT4               u4IpMRouteNextHopAddress;
#endif
{

    tSNMP_OCTET_STRING_TYPE IpMRouteSource;
    tSNMP_OCTET_STRING_TYPE IpMRouteGroup;
    tSNMP_OCTET_STRING_TYPE IpMRouteNxtHop;
    UINT1               au1GrpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1SrcAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1NxtHop[IPVX_MAX_INET_ADDR_LEN];

    memset (au1SrcAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    memset (au1GrpAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    memset (au1NxtHop, 0, IPVX_MAX_INET_ADDR_LEN);

    IpMRouteSource.pu1_OctetList = au1SrcAddr;
    IpMRouteSource.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    IpMRouteGroup.pu1_OctetList = au1GrpAddr;
    IpMRouteGroup.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    IpMRouteNxtHop.pu1_OctetList = au1GrpAddr;
    IpMRouteNxtHop.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    u4IpMRouteNextHopSource = OSIX_NTOHL (u4IpMRouteNextHopSource);
    u4IpMRouteNextHopGroup = OSIX_NTOHL (u4IpMRouteNextHopGroup);
    u4IpMRouteNextHopAddress = OSIX_NTOHL (u4IpMRouteNextHopAddress);
    memcpy (IpMRouteSource.pu1_OctetList,
            (UINT1 *) &u4IpMRouteNextHopSource, sizeof (UINT4));

    memcpy (IpMRouteGroup.pu1_OctetList,
            (UINT1 *) &u4IpMRouteNextHopGroup, sizeof (UINT4));

    memcpy (IpMRouteNxtHop.pu1_OctetList,
            (UINT1 *) &u4IpMRouteNextHopAddress, sizeof (UINT4));

    return (nmhValidateIndexInstanceIpCmnMRouteNextHopTable
            (i4IpMRouteNextHopOwnerId, IPVX_ADDR_FMLY_IPV4, &IpMRouteSource,
             &IpMRouteGroup, u4IpMRouteNextHopSourceMask,
             i4IpMRouteNextHopIfIndex, &IpMRouteNxtHop));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIpMRouteNextHopTable
 Input       :  The Indices
                IpMRouteNextHopOwnerId
                IpMRouteNextHopGroup
                IpMRouteNextHopSource
                IpMRouteNextHopSourceMask
                IpMRouteNextHopIfIndex
                IpMRouteNextHopAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhGetFirstIpMRouteNextHopTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFirstIndexIpMRouteNextHopTable (INT4 *pi4IpMRouteNextHopOwnerId,
                                      UINT4 *pu4IpMRouteNextHopGroup,
                                      UINT4 *pu4IpMRouteNextHopSource,
                                      UINT4 *pu4IpMRouteNextHopSourceMask,
                                      INT4 *pi4IpMRouteNextHopIfIndex,
                                      UINT4 *pu4IpMRouteNextHopAddress)
#else
INT1
nmhGetFirstIndexIpMRouteNextHopTable (pi4IpMRouteNextHopOwnerId,
                                      pu4IpMRouteNextHopGroup,
                                      pu4IpMRouteNextHopSource,
                                      pu4IpMRouteNextHopSourceMask,
                                      pi4IpMRouteNextHopIfIndex,
                                      pu4IpMRouteNextHopAddress)
     INT4               *pi4IpMRouteNextHopOwnerId;
     UINT4              *pu4IpMRouteNextHopGroup;
     UINT4              *pu4IpMRouteNextHopSource;
     UINT4              *pu4IpMRouteNextHopSourceMask;
     INT4               *pi4IpMRouteNextHopIfIndex;
     UINT4              *pu4IpMRouteNextHopAddress;
#endif
{

    tSNMP_OCTET_STRING_TYPE IpMRouteGroup;
    tSNMP_OCTET_STRING_TYPE IpMRouteSource;
    tSNMP_OCTET_STRING_TYPE IpMRouteNxtHop;
    UINT1               au1GrpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1SrcAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1NxtHop[IPVX_MAX_INET_ADDR_LEN];
    INT4                i4AddrType = 0;
    UINT4               u4RetGrpIpAddr = 0;
    UINT4               u4RetSrcIpAddr = 0;
    UINT4               u4RetNxtHop = 0;

    memset (au1SrcAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    memset (au1GrpAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    memset (au1NxtHop, 0, IPVX_MAX_INET_ADDR_LEN);

    IpMRouteSource.pu1_OctetList = au1SrcAddr;
    IpMRouteSource.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    IpMRouteGroup.pu1_OctetList = au1GrpAddr;
    IpMRouteGroup.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    IpMRouteNxtHop.pu1_OctetList = au1GrpAddr;
    IpMRouteNxtHop.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    memcpy (IpMRouteSource.pu1_OctetList,
            (UINT1 *) pu4IpMRouteNextHopSource, sizeof (UINT4));

    memcpy (IpMRouteGroup.pu1_OctetList,
            (UINT1 *) pu4IpMRouteNextHopGroup, sizeof (UINT4));

    memcpy (IpMRouteNxtHop.pu1_OctetList,
            (UINT1 *) pu4IpMRouteNextHopAddress, sizeof (UINT4));

    if (nmhGetFirstIndexIpCmnMRouteNextHopTable (pi4IpMRouteNextHopOwnerId,
                                                 &i4AddrType,
                                                 &IpMRouteGroup,
                                                 &IpMRouteSource,
                                                 (INT4 *)
                                                 pu4IpMRouteNextHopSourceMask,
                                                 (INT4 *)
                                                 pi4IpMRouteNextHopIfIndex,
                                                 &IpMRouteNxtHop) ==
        SNMP_SUCCESS)
    {

        PTR_FETCH4 (u4RetGrpIpAddr, IpMRouteGroup.pu1_OctetList);
        PTR_FETCH4 (u4RetSrcIpAddr, IpMRouteSource.pu1_OctetList);
        PTR_FETCH4 (u4RetNxtHop, IpMRouteNxtHop.pu1_OctetList);

        memcpy (pu4IpMRouteNextHopGroup,
                (UINT1 *) &u4RetGrpIpAddr, sizeof (UINT4));
        memcpy (pu4IpMRouteNextHopSource,
                (UINT1 *) &u4RetSrcIpAddr, sizeof (UINT4));
        memcpy (pu4IpMRouteNextHopAddress,
                (UINT1 *) &u4RetNxtHop, sizeof (UINT4));

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetNextIndexIpMRouteNextHopTable
 Input       :  The Indices
                IpMRouteNextHopOwnerId
                nextIpMRouteNextHopOwnerId
                IpMRouteNextHopGroup
                nextIpMRouteNextHopGroup
                IpMRouteNextHopSource
                nextIpMRouteNextHopSource
                IpMRouteNextHopSourceMask
                nextIpMRouteNextHopSourceMask
                IpMRouteNextHopIfIndex
                nextIpMRouteNextHopIfIndex
                IpMRouteNextHopAddress
                nextIpMRouteNextHopAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
 /*** $$TRACE_PROCEDURE_NAME = nmhGetNextIpMRouteNextHopTable ***/
 /*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetNextIndexIpMRouteNextHopTable (INT4 i4IpMRouteNextHopOwnerId,
                                     INT4 *pi4NextIpMRouteNextHopOwnerId,
                                     UINT4 u4IpMRouteNextHopGroup,
                                     UINT4 *pu4NextIpMRouteNextHopGroup,
                                     UINT4 u4IpMRouteNextHopSource,
                                     UINT4 *pu4NextIpMRouteNextHopSource,
                                     UINT4 u4IpMRouteNextHopSourceMask,
                                     UINT4 *pu4NextIpMRouteNextHopSourceMask,
                                     INT4 i4IpMRouteNextHopIfIndex,
                                     INT4 *pi4NextIpMRouteNextHopIfIndex,
                                     UINT4 u4IpMRouteNextHopAddress,
                                     UINT4 *pu4NextIpMRouteNextHopAddress)
#else
INT1
nmhGetNextIndexIpMRouteNextHopTable (i4IpMRouteNextHopOwnerId,
                                     pi4NextIpMRouteNextHopOwnerId,
                                     u4IpMRouteNextHopGroup,
                                     pu4NextIpMRouteNextHopGroup,
                                     u4IpMRouteNextHopSource,
                                     pu4NextIpMRouteNextHopSource,
                                     u4IpMRouteNextHopSourceMask,
                                     pu4NextIpMRouteNextHopSourceMask,
                                     i4IpMRouteNextHopIfIndex,
                                     pi4NextIpMRouteNextHopIfIndex,
                                     u4IpMRouteNextHopAddress,
                                     pu4NextIpMRouteNextHopAddress)
     INT4                i4IpMRouteNextHopOwnerId;
     INT4               *pi4NextIpMRouteNextHopOwnerId;
     UINT4               u4IpMRouteNextHopGroup;
     UINT4              *pu4NextIpMRouteNextHopGroup;
     UINT4               u4IpMRouteNextHopSource;
     UINT4              *pu4NextIpMRouteNextHopSource;
     UINT4               u4IpMRouteNextHopSourceMask;
     UINT4              *pu4NextIpMRouteNextHopSourceMask;
     INT4                i4IpMRouteNextHopIfIndex;
     INT4               *pi4NextIpMRouteNextHopIfIndex;
     UINT4               u4IpMRouteNextHopAddress;
     UINT4              *pu4NextIpMRouteNextHopAddress;
#endif
{

    tSNMP_OCTET_STRING_TYPE NextIpMRouteGroup;
    tSNMP_OCTET_STRING_TYPE IpMRouteGroup;
    tSNMP_OCTET_STRING_TYPE NextIpMRouteSource;
    tSNMP_OCTET_STRING_TYPE IpMRouteSource;
    tSNMP_OCTET_STRING_TYPE NextIpMRouteNxtHop;
    tSNMP_OCTET_STRING_TYPE IpMRouteNxtHop;
    UINT1               au1MRouteSource[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1NextMRouteSource[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1MRouteGroup[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1NextMRouteGroup[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1MRouteNxtHop[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1NextMRouteNxtHop[IPVX_MAX_INET_ADDR_LEN];
    UINT4               u4RetGrpIpAddr = 0;
    UINT4               u4RetSrcIpAddr = 0;
    UINT4               u4RetNxtHop = 0;
    INT4                i4NextIpMRouteNextHopAddrType = 0;

    memset (au1MRouteSource, 0, IPVX_MAX_INET_ADDR_LEN);
    memset (au1NextMRouteSource, 0, IPVX_MAX_INET_ADDR_LEN);
    memset (au1MRouteGroup, 0, IPVX_MAX_INET_ADDR_LEN);
    memset (au1NextMRouteGroup, 0, IPVX_MAX_INET_ADDR_LEN);
    memset (au1NextMRouteNxtHop, 0, IPVX_MAX_INET_ADDR_LEN);

    IpMRouteSource.pu1_OctetList = au1MRouteSource;
    IpMRouteSource.i4_Length = IPVX_MAX_INET_ADDR_LEN;
    NextIpMRouteSource.pu1_OctetList = au1NextMRouteSource;
    NextIpMRouteSource.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    IpMRouteGroup.pu1_OctetList = au1MRouteGroup;
    IpMRouteGroup.i4_Length = IPVX_MAX_INET_ADDR_LEN;
    NextIpMRouteGroup.pu1_OctetList = au1NextMRouteGroup;
    NextIpMRouteGroup.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    IpMRouteNxtHop.pu1_OctetList = au1MRouteNxtHop;
    IpMRouteNxtHop.i4_Length = IPVX_MAX_INET_ADDR_LEN;
    NextIpMRouteNxtHop.pu1_OctetList = au1NextMRouteNxtHop;
    NextIpMRouteNxtHop.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    u4IpMRouteNextHopSource = OSIX_NTOHL (u4IpMRouteNextHopSource);
    u4IpMRouteNextHopGroup = OSIX_NTOHL (u4IpMRouteNextHopGroup);
    u4IpMRouteNextHopAddress = OSIX_NTOHL (u4IpMRouteNextHopAddress);

    memcpy (IpMRouteSource.pu1_OctetList,
            (UINT1 *) &u4IpMRouteNextHopSource, sizeof (UINT4));

    memcpy (NextIpMRouteSource.pu1_OctetList,
            (UINT1 *) pu4NextIpMRouteNextHopSource, sizeof (UINT4));

    memcpy (IpMRouteGroup.pu1_OctetList,
            (UINT1 *) &u4IpMRouteNextHopGroup, sizeof (UINT4));

    memcpy (NextIpMRouteGroup.pu1_OctetList,
            (UINT1 *) pu4NextIpMRouteNextHopGroup, sizeof (UINT4));

    memcpy (IpMRouteNxtHop.pu1_OctetList,
            (UINT1 *) &u4IpMRouteNextHopAddress, sizeof (UINT4));

    memcpy (NextIpMRouteNxtHop.pu1_OctetList,
            (UINT1 *) pu4NextIpMRouteNextHopAddress, sizeof (UINT4));

    if (nmhGetNextIndexIpCmnMRouteNextHopTable (i4IpMRouteNextHopOwnerId,
                                                pi4NextIpMRouteNextHopOwnerId,
                                                IPVX_ADDR_FMLY_IPV4,
                                                &i4NextIpMRouteNextHopAddrType,
                                                &IpMRouteGroup,
                                                &NextIpMRouteGroup,
                                                &IpMRouteSource,
                                                &NextIpMRouteSource,
                                                u4IpMRouteNextHopSourceMask,
                                                (INT4 *)
                                                pu4NextIpMRouteNextHopSourceMask,
                                                i4IpMRouteNextHopIfIndex,
                                                pi4NextIpMRouteNextHopIfIndex,
                                                &IpMRouteNxtHop,
                                                &NextIpMRouteNxtHop) ==
        SNMP_SUCCESS)
    {
        if (i4NextIpMRouteNextHopAddrType == IPVX_ADDR_FMLY_IPV6)
        {

            return SNMP_FAILURE;

        }
        PTR_FETCH4 (u4RetGrpIpAddr, NextIpMRouteGroup.pu1_OctetList);
        PTR_FETCH4 (u4RetSrcIpAddr, NextIpMRouteSource.pu1_OctetList);
        PTR_FETCH4 (u4RetNxtHop, NextIpMRouteNxtHop.pu1_OctetList);

        memcpy (pu4NextIpMRouteNextHopGroup,
                (UINT1 *) &u4RetGrpIpAddr, sizeof (UINT4));
        memcpy (pu4NextIpMRouteNextHopSource,
                (UINT1 *) &u4RetSrcIpAddr, sizeof (UINT4));
        memcpy (pu4NextIpMRouteNextHopAddress,
                (UINT1 *) &u4RetNxtHop, sizeof (UINT4));

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIpMRouteNextHopState
 Input       :  The Indices
                IpMRouteNextHopOwnerId
                IpMRouteNextHopGroup
                IpMRouteNextHopSource
                IpMRouteNextHopSourceMask
                IpMRouteNextHopIfIndex
                IpMRouteNextHopAddress

                The Object 
                retValIpMRouteNextHopState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpMRouteNextHopState ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIpMRouteNextHopState (INT4 i4IpMRouteNextHopOwnerId,
                            UINT4 u4IpMRouteNextHopGroup,
                            UINT4 u4IpMRouteNextHopSource,
                            UINT4 u4IpMRouteNextHopSourceMask,
                            INT4 i4IpMRouteNextHopIfIndex,
                            UINT4 u4IpMRouteNextHopAddress,
                            INT4 *pi4RetValIpMRouteNextHopState)
#else
INT1
nmhGetIpMRouteNextHopState (i4IpMRouteNextHopOwnerId,
                            u4IpMRouteNextHopGroup,
                            u4IpMRouteNextHopSource,
                            u4IpMRouteNextHopSourceMask,
                            i4IpMRouteNextHopIfIndex,
                            u4IpMRouteNextHopAddress,
                            pi4RetValIpMRouteNextHopState)
     INT4                i4IpMRouteNextHopOwnerId;
     UINT4               u4IpMRouteNextHopGroup;
     UINT4               u4IpMRouteNextHopSource;
     UINT4               u4IpMRouteNextHopSourceMask;
     INT4                i4IpMRouteNextHopIfIndex;
     UINT4               u4IpMRouteNextHopAddress;
     INT4               *pi4RetValIpMRouteNextHopState;
#endif
{

    tSNMP_OCTET_STRING_TYPE IpMRouteSource;
    tSNMP_OCTET_STRING_TYPE IpMRouteGroup;
    tSNMP_OCTET_STRING_TYPE IpMRouteNxtHop;
    UINT1               au1GrpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1SrcAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1NxtHop[IPVX_MAX_INET_ADDR_LEN];

    memset (au1SrcAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    memset (au1GrpAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    memset (au1NxtHop, 0, IPVX_MAX_INET_ADDR_LEN);

    IpMRouteSource.pu1_OctetList = au1SrcAddr;
    IpMRouteSource.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    IpMRouteGroup.pu1_OctetList = au1GrpAddr;
    IpMRouteGroup.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    IpMRouteNxtHop.pu1_OctetList = au1GrpAddr;
    IpMRouteNxtHop.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    u4IpMRouteNextHopSource = OSIX_NTOHL (u4IpMRouteNextHopSource);
    u4IpMRouteNextHopGroup = OSIX_NTOHL (u4IpMRouteNextHopGroup);
    u4IpMRouteNextHopAddress = OSIX_NTOHL (u4IpMRouteNextHopAddress);

    memcpy (IpMRouteSource.pu1_OctetList,
            (UINT1 *) &u4IpMRouteNextHopSource, sizeof (UINT4));

    memcpy (IpMRouteGroup.pu1_OctetList,
            (UINT1 *) &u4IpMRouteNextHopGroup, sizeof (UINT4));

    memcpy (IpMRouteNxtHop.pu1_OctetList,
            (UINT1 *) &u4IpMRouteNextHopAddress, sizeof (UINT4));

    return (nmhGetIpCmnMRouteNextHopState (i4IpMRouteNextHopOwnerId,
                                           IPVX_ADDR_FMLY_IPV4,
                                           &IpMRouteGroup,
                                           &IpMRouteSource,
                                           u4IpMRouteNextHopSourceMask,
                                           i4IpMRouteNextHopIfIndex,
                                           &IpMRouteNxtHop,
                                           pi4RetValIpMRouteNextHopState));

}

/****************************************************************************
 Function    :  nmhGetIpMRouteNextHopUpTime
 Input       :  The Indices
                IpMRouteNextHopOwnerId
                IpMRouteNextHopGroup
                IpMRouteNextHopSource
                IpMRouteNextHopSourceMask
                IpMRouteNextHopIfIndex
                IpMRouteNextHopAddress

                The Object 
                retValIpMRouteNextHopUpTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpMRouteNextHopUpTime ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIpMRouteNextHopUpTime (INT4 i4IpMRouteNextHopOwnerId,
                             UINT4 u4IpMRouteNextHopGroup,
                             UINT4 u4IpMRouteNextHopSource,
                             UINT4 u4IpMRouteNextHopSourceMask,
                             INT4 i4IpMRouteNextHopIfIndex,
                             UINT4 u4IpMRouteNextHopAddress,
                             UINT4 *pu4RetValIpMRouteNextHopUpTime)
#else
INT1
nmhGetIpMRouteNextHopUpTime (i4IpMRouteNextHopOwnerId,
                             u4IpMRouteNextHopGroup,
                             u4IpMRouteNextHopSource,
                             u4IpMRouteNextHopSourceMask,
                             i4IpMRouteNextHopIfIndex,
                             u4IpMRouteNextHopAddress,
                             pu4RetValIpMRouteNextHopUpTime)
     INT4                i4IpMRouteNextHopOwnerId;
     UINT4               u4IpMRouteNextHopGroup;
     UINT4               u4IpMRouteNextHopSource;
     UINT4               u4IpMRouteNextHopSourceMask;
     INT4                i4IpMRouteNextHopIfIndex;
     UINT4               u4IpMRouteNextHopAddress;
     UINT4              *pu4RetValIpMRouteNextHopUpTime;
#endif
{

    tSNMP_OCTET_STRING_TYPE IpMRouteSource;
    tSNMP_OCTET_STRING_TYPE IpMRouteGroup;
    tSNMP_OCTET_STRING_TYPE IpMRouteNxtHop;
    UINT1               au1GrpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1SrcAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1NxtHop[IPVX_MAX_INET_ADDR_LEN];

    memset (au1SrcAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    memset (au1GrpAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    memset (au1NxtHop, 0, IPVX_MAX_INET_ADDR_LEN);

    IpMRouteSource.pu1_OctetList = au1SrcAddr;
    IpMRouteSource.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    IpMRouteGroup.pu1_OctetList = au1GrpAddr;
    IpMRouteGroup.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    IpMRouteNxtHop.pu1_OctetList = au1GrpAddr;
    IpMRouteNxtHop.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    u4IpMRouteNextHopSource = OSIX_NTOHL (u4IpMRouteNextHopSource);
    u4IpMRouteNextHopGroup = OSIX_NTOHL (u4IpMRouteNextHopGroup);
    u4IpMRouteNextHopAddress = OSIX_NTOHL (u4IpMRouteNextHopAddress);

    memcpy (IpMRouteSource.pu1_OctetList,
            (UINT1 *) &u4IpMRouteNextHopSource, sizeof (UINT4));

    memcpy (IpMRouteGroup.pu1_OctetList,
            (UINT1 *) &u4IpMRouteNextHopGroup, sizeof (UINT4));

    memcpy (IpMRouteNxtHop.pu1_OctetList,
            (UINT1 *) &u4IpMRouteNextHopAddress, sizeof (UINT4));

    return (nmhGetIpCmnMRouteNextHopUpTime (i4IpMRouteNextHopOwnerId,
                                            IPVX_ADDR_FMLY_IPV4,
                                            &IpMRouteGroup,
                                            &IpMRouteSource,
                                            u4IpMRouteNextHopSourceMask,
                                            i4IpMRouteNextHopIfIndex,
                                            &IpMRouteNxtHop,
                                            pu4RetValIpMRouteNextHopUpTime));
}

/* Low Level SET Routine for All Objects  */

/* LOW LEVEL Routines for Table : IpMRouteInterfaceTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIpMRouteInterfaceTable
 Input       :  The Indices
                IpMRouteInterfaceIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhValidateIndexInstanceIpMRouteInterfaceTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhValidateIndexInstanceIpMRouteInterfaceTable (INT4 i4IpMRouteInterfaceIfIndex)
#else
INT1
nmhValidateIndexInstanceIpMRouteInterfaceTable (i4IpMRouteInterfaceIfIndex)
     INT4                i4IpMRouteInterfaceIfIndex;
#endif
{
    return (nmhValidateIndexInstanceIpCmnMRouteInterfaceTable
            (i4IpMRouteInterfaceIfIndex, IPVX_ADDR_FMLY_IPV4));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIpMRouteInterfaceTable
 Input       :  The Indices
                IpMRouteInterfaceIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhGetFirstIpMRouteInterfaceTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFirstIndexIpMRouteInterfaceTable (INT4 *pi4IpMRouteInterfaceIfIndex)
#else
INT1
nmhGetFirstIndexIpMRouteInterfaceTable (pi4IpMRouteInterfaceIfIndex)
     INT4               *pi4IpMRouteInterfaceIfIndex;
#endif
{
    INT4                i4AddrType = 0;
    return (nmhGetFirstIndexIpCmnMRouteInterfaceTable
            (pi4IpMRouteInterfaceIfIndex, &i4AddrType));
}

/****************************************************************************
 Function    :  nmhGetNextIndexIpMRouteInterfaceTable
 Input       :  The Indices
                IpMRouteInterfaceIfIndex
                nextIpMRouteInterfaceIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
 /*** $$TRACE_PROCEDURE_NAME = nmhGetNextIpMRouteInterfaceTable ***/
 /*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetNextIndexIpMRouteInterfaceTable (INT4 i4IpMRouteInterfaceIfIndex,
                                       INT4 *pi4NextIpMRouteInterfaceIfIndex)
#else
INT1
nmhGetNextIndexIpMRouteInterfaceTable (i4IpMRouteInterfaceIfIndex,
                                       pi4NextIpMRouteInterfaceIfIndex)
     INT4                i4IpMRouteInterfaceIfIndex;
     INT4               *pi4NextIpMRouteInterfaceIfIndex;
#endif
{
    INT4                i4NextIpMRouteInterfaceAddrType = 0;

    if (nmhGetNextIndexIpCmnMRouteInterfaceTable (i4IpMRouteInterfaceIfIndex,
                                                  pi4NextIpMRouteInterfaceIfIndex,
                                                  IPVX_ADDR_FMLY_IPV4,
                                                  &i4NextIpMRouteInterfaceAddrType)
        == SNMP_SUCCESS)
    {

        if (i4NextIpMRouteInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
        {

            return SNMP_FAILURE;

        }
        else
        {
            return SNMP_SUCCESS;
        }

    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIpMRouteInterfaceOwnerId
 Input       :  The Indices
                IpMRouteInterfaceIfIndex

                The Object 
                retValIpMRouteInterfaceOwnerId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpMRouteInterfaceOwnerId ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIpMRouteInterfaceOwnerId (INT4 i4IpMRouteInterfaceIfIndex,
                                INT4 *pi4RetValIpMRouteInterfaceOwnerId)
#else
INT1
nmhGetIpMRouteInterfaceOwnerId (i4IpMRouteInterfaceIfIndex,
                                pi4RetValIpMRouteInterfaceOwnerId)
     INT4                i4IpMRouteInterfaceIfIndex;
     INT4               *pi4RetValIpMRouteInterfaceOwnerId;
#endif
{

    return (nmhGetIpCmnMRouteInterfaceOwnerId (i4IpMRouteInterfaceIfIndex,
                                               IPVX_ADDR_FMLY_IPV4,
                                               pi4RetValIpMRouteInterfaceOwnerId));
}

/****************************************************************************
 Function    :  nmhGetIpMRouteInterfaceTtl
 Input       :  The Indices
                IpMRouteInterfaceIfIndex

                The Object 
                retValIpMRouteInterfaceTtl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpMRouteInterfaceTtl ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIpMRouteInterfaceTtl (INT4 i4IpMRouteInterfaceIfIndex,
                            INT4 *pi4RetValIpMRouteInterfaceTtl)
#else
INT1
nmhGetIpMRouteInterfaceTtl (i4IpMRouteInterfaceIfIndex,
                            pi4RetValIpMRouteInterfaceTtl)
     INT4                i4IpMRouteInterfaceIfIndex;
     INT4               *pi4RetValIpMRouteInterfaceTtl;
#endif
{
    return (nmhGetIpCmnMRouteInterfaceTtl (i4IpMRouteInterfaceIfIndex,
                                           IPVX_ADDR_FMLY_IPV4,
                                           pi4RetValIpMRouteInterfaceTtl));
}

/****************************************************************************
 Function    :  nmhGetIpMRouteInterfaceProtocol
 Input       :  The Indices
                IpMRouteInterfaceIfIndex

                The Object 
                retValIpMRouteInterfaceProtocol
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpMRouteInterfaceProtocol ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIpMRouteInterfaceProtocol (INT4 i4IpMRouteInterfaceIfIndex,
                                 INT4 *pi4RetValIpMRouteInterfaceProtocol)
#else
INT1
nmhGetIpMRouteInterfaceProtocol (i4IpMRouteInterfaceIfIndex,
                                 pi4RetValIpMRouteInterfaceProtocol)
     INT4                i4IpMRouteInterfaceIfIndex;
     INT4               *pi4RetValIpMRouteInterfaceProtocol;
#endif
{
    return (nmhGetIpCmnMRouteInterfaceProtocol (i4IpMRouteInterfaceIfIndex,
                                                IPVX_ADDR_FMLY_IPV4,
                                                pi4RetValIpMRouteInterfaceProtocol));
}

/****************************************************************************
 Function    :  nmhGetIpMRouteInterfaceRateLimit
 Input       :  The Indices
                IpMRouteInterfaceIfIndex

                The Object 
                retValIpMRouteInterfaceRateLimit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpMRouteInterfaceRateLimit ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIpMRouteInterfaceRateLimit (INT4 i4IpMRouteInterfaceIfIndex,
                                  INT4 *pi4RetValIpMRouteInterfaceRateLimit)
#else
INT1
nmhGetIpMRouteInterfaceRateLimit (i4IpMRouteInterfaceIfIndex,
                                  pi4RetValIpMRouteInterfaceRateLimit)
     INT4                i4IpMRouteInterfaceIfIndex;
     INT4               *pi4RetValIpMRouteInterfaceRateLimit;
#endif
{
    return (nmhGetIpCmnMRouteInterfaceRateLimit (i4IpMRouteInterfaceIfIndex,
                                                 IPVX_ADDR_FMLY_IPV4,
                                                 pi4RetValIpMRouteInterfaceRateLimit));

}

/****************************************************************************
 Function    :  nmhGetIpMRouteInterfaceCmdbPktCnt
 Input       :  The Indices
                IpMRouteInterfaceIfIndex

                The Object 
                retValIpMRouteInterfaceCmdbPktCnt
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpMRouteInterfaceCmdbPktCnt ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIpMRouteInterfaceCmdbPktCnt (INT4 i4IpMRouteInterfaceIfIndex,
                                   UINT4 *pu4RetValIpMRouteInterfaceCmdbPktCnt)
#else
INT1
nmhGetIpMRouteInterfaceCmdbPktCnt (i4IpMRouteInterfaceIfIndex,
                                   pu4RetValIpMRouteInterfaceCmdbPktCnt)
     INT4                i4IpMRouteInterfaceIfIndex;
     UINT4              *pu4RetValIpMRouteInterfaceCmdbPktCnt;
#endif
{

    return (nmhGetIpCmnMRouteInterfaceCmdbPktCnt (i4IpMRouteInterfaceIfIndex,
                                                  IPVX_ADDR_FMLY_IPV4,
                                                  pu4RetValIpMRouteInterfaceCmdbPktCnt));
}

/****************************************************************************
 Function    :  nmhGetIpMRouteInterfaceInMcastOctets
 Input       :  The Indices
                IpMRouteInterfaceIfIndex

                The Object 
                retValIpMRouteInterfaceInMcastOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpMRouteInterfaceInMcastOctets ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIpMRouteInterfaceInMcastOctets (INT4 i4IpMRouteInterfaceIfIndex,
                                      UINT4
                                      *pu4RetValIpMRouteInterfaceInMcastOctets)
#else
INT1
nmhGetIpMRouteInterfaceInMcastOctets (i4IpMRouteInterfaceIfIndex,
                                      pu4RetValIpMRouteInterfaceInMcastOctets)
     INT4                i4IpMRouteInterfaceIfIndex;
     UINT4              *pu4RetValIpMRouteInterfaceInMcastOctets;
#endif
{
    return (nmhGetIpCmnMRouteInterfaceInMcastOctets (i4IpMRouteInterfaceIfIndex,
                                                     IPVX_ADDR_FMLY_IPV4,
                                                     pu4RetValIpMRouteInterfaceInMcastOctets));
}

/****************************************************************************
 Function    :  nmhGetIpMRouteInterfaceOutMcastOctets
 Input       :  The Indices
                IpMRouteInterfaceIfIndex

                The Object 
                retValIpMRouteInterfaceOutMcastOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpMRouteInterfaceOutMcastOctets ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIpMRouteInterfaceOutMcastOctets (INT4 i4IpMRouteInterfaceIfIndex,
                                       UINT4
                                       *pu4RetValIpMRouteInterfaceOutMcastOctets)
#else
INT1
nmhGetIpMRouteInterfaceOutMcastOctets (i4IpMRouteInterfaceIfIndex,
                                       pu4RetValIpMRouteInterfaceOutMcastOctets)
     INT4                i4IpMRouteInterfaceIfIndex;
     UINT4              *pu4RetValIpMRouteInterfaceOutMcastOctets;
#endif
{
    return (nmhGetIpCmnMRouteInterfaceOutMcastOctets
            (i4IpMRouteInterfaceIfIndex, IPVX_ADDR_FMLY_IPV4,
             pu4RetValIpMRouteInterfaceOutMcastOctets));
}
