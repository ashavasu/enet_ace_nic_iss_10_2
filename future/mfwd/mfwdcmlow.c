/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: mfwdcmlow.c,v 1.11 2014/03/18 11:55:25 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "mfinc.h"
# include  "fssnmp.h"
# include  "mfwdcmlow.h"

extern tMfwdContextStructure gMfwdContext;

#ifdef TRACE_WANTED
static UINT2        u2MfwdTrcModule = MFWD_MGMT_MODULE;
#endif
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIpCmnMRouteEnable
 Input       :  The Indices

                The Object 
                retValIpCmnMRouteEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpCmnMRouteEnable (INT4 *pi4RetValIpCmnMRouteEnable)
{
    INT1                i1Status = SNMP_SUCCESS;

    *pi4RetValIpCmnMRouteEnable = gMfwdContext.u1MfwdStatus;
    MFWD_TRC_ARG1 (MFWD_TRC_FLAG, MFWD_MGMT_TRC, MFWD_MOD_NAME,
                   "The status of MFWD is %d\n", *pi4RetValIpCmnMRouteEnable);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetIpCmnMRouteEntryCount
 Input       :  The Indices

                The Object 
                retValIpCmnMRouteEntryCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpCmnMRouteEntryCount (UINT4 *pu4RetValIpCmnMRouteEntryCount)
{

    INT1                i1Status = SNMP_SUCCESS;

    *pu4RetValIpCmnMRouteEntryCount = gMfwdContext.u4RtEntryCount;
    MFWD_TRC_ARG1 (MFWD_TRC_FLAG, MFWD_MGMT_TRC, MFWD_MOD_NAME,
                   "The Number of route entries MFWD is %d\n",
                   *pu4RetValIpCmnMRouteEntryCount);
    return i1Status;

}

/****************************************************************************
 Function    :  nmhGetIpCmnMRouteEnableCmdb
 Input       :  The Indices

                The Object 
                retValIpCmnMRouteEnableCmdb
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpCmnMRouteEnableCmdb (INT4 *pi4RetValIpCmnMRouteEnableCmdb)
{
    INT1                i1Status = SNMP_SUCCESS;

    *pi4RetValIpCmnMRouteEnableCmdb = gMfwdContext.u1CacheMissDBStat;
    MFWD_TRC_ARG1 (MFWD_TRC_FLAG, MFWD_MGMT_TRC, MFWD_MOD_NAME,
                   "The status of CMDB in MFWD is %d\n",
                   *pi4RetValIpCmnMRouteEnableCmdb);
    return i1Status;

}

/****************************************************************************
 Function    :  nmhGetMfwdCmnGlobalTrace
 Input       :  The Indices

                The Object 
                retValMfwdCmnGlobalTrace
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMfwdCmnGlobalTrace (INT4 *pi4RetValMfwdCmnGlobalTrace)
{

    INT1                i1Status = SNMP_SUCCESS;

    *pi4RetValMfwdCmnGlobalTrace = gMfwdContext.u4GlobalTrace;
    MFWD_TRC_ARG1 (MFWD_TRC_FLAG, MFWD_MGMT_TRC, MFWD_MOD_NAME,
                   "The Value of Global Trace flags is %x\n",
                   *pi4RetValMfwdCmnGlobalTrace);
    return i1Status;

}

/****************************************************************************
 Function    :  nmhGetMfwdCmnGlobalDebug
 Input       :  The Indices

                The Object 
                retValMfwdCmnGlobalDebug
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMfwdCmnGlobalDebug (INT4 *pi4RetValMfwdCmnGlobalDebug)
{

    INT1                i1Status = SNMP_SUCCESS;

    *pi4RetValMfwdCmnGlobalDebug = gMfwdContext.u4GlobalDebug;
    MFWD_TRC_ARG1 (MFWD_TRC_FLAG, MFWD_MGMT_TRC, MFWD_MOD_NAME,
                   "The Value of Global Debug flags is %x",
                   *pi4RetValMfwdCmnGlobalDebug);
    return i1Status;

}

/****************************************************************************
 Function    :  nmhGetIpCmnMRouteDiscardedPkts
 Input       :  The Indices

                The Object 
                retValIpCmnMRouteDiscardedPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpCmnMRouteDiscardedPkts (UINT4 *pu4RetValIpCmnMRouteDiscardedPkts)
{

    INT1                i1Status = SNMP_SUCCESS;

    *pu4RetValIpCmnMRouteDiscardedPkts = gMfwdContext.u4DiscPkts;
    MFWD_TRC_ARG1 (MFWD_TRC_FLAG, MFWD_MGMT_TRC, MFWD_MOD_NAME,
                   "Number of Packets discarded are %x\n",
                   *pu4RetValIpCmnMRouteDiscardedPkts);
    return i1Status;

}

/****************************************************************************
 Function    :  nmhGetMfwdCmnAvgDataRate
 Input       :  The Indices

                The Object 
                retValMfwdCmnAvgDataRate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMfwdCmnAvgDataRate (INT4 *pi4RetValMfwdCmnAvgDataRate)
{

    INT1                i1Status = SNMP_SUCCESS;

    *pi4RetValMfwdCmnAvgDataRate = gMfwdContext.u4QProcessCnt;
    MFWD_TRC_ARG1 (MFWD_TRC_FLAG, MFWD_MGMT_TRC, MFWD_MOD_NAME,
                   "Number of Packets discarded are %x\n",
                   *pi4RetValMfwdCmnAvgDataRate);
    return i1Status;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIpCmnMRouteEnable
 Input       :  The Indices

                The Object 
                setValIpCmnMRouteEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIpCmnMRouteEnable (INT4 i4SetValIpCmnMRouteEnable)
{

    INT1                i1Status = SNMP_SUCCESS;
    UINT4               u4Event;

    u4Event = (i4SetValIpCmnMRouteEnable == MFWD_STATUS_ENABLED) ?
        MFWD_STATUS_ENABLE_EVENT : MFWD_STATUS_DISABLE_EVENT;
    /* Send a EVENT to MFWD */
    OsixEvtSend (MFWD_TASK_ID, u4Event);
    return i1Status;

}

/****************************************************************************
 Function    :  nmhSetIpCmnMRouteEnableCmdb
 Input       :  The Indices

                The Object 
                setValIpCmnMRouteEnableCmdb
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIpCmnMRouteEnableCmdb (INT4 i4SetValIpCmnMRouteEnableCmdb)
{

    INT1                i1Status = SNMP_SUCCESS;
    UINT4               u4Event;

    u4Event = (i4SetValIpCmnMRouteEnableCmdb == MFWD_CMDB_STATUS_ENABLED) ?
        MFWD_CMDB_ENABLE_EVENT : MFWD_CMDB_DISABLE_EVENT;
    /* Send a EVENT to MFWD */
    OsixEvtSend (MFWD_TASK_ID, u4Event);
    return i1Status;

}

/****************************************************************************
 Function    :  nmhSetMfwdCmnGlobalTrace
 Input       :  The Indices

                The Object 
                setValMfwdCmnGlobalTrace
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMfwdCmnGlobalTrace (INT4 i4SetValMfwdCmnGlobalTrace)
{

    INT1                i1Status = SNMP_SUCCESS;

    gMfwdContext.u4GlobalTrace = i4SetValMfwdCmnGlobalTrace;
    MFWD_TRC_ARG1 (MFWD_TRC_FLAG, MFWD_MGMT_TRC, MFWD_MOD_NAME,
                   "New Value for Global Trace flags is %d\n",
                   i4SetValMfwdCmnGlobalTrace);
    return i1Status;

}

/****************************************************************************
 Function    :  nmhSetMfwdCmnGlobalDebug
 Input       :  The Indices

                The Object 
                setValMfwdCmnGlobalDebug
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMfwdCmnGlobalDebug (INT4 i4SetValMfwdCmnGlobalDebug)
{

    INT1                i1Status = SNMP_SUCCESS;

    gMfwdContext.u4GlobalDebug = i4SetValMfwdCmnGlobalDebug;
    MFWD_TRC_ARG1 (MFWD_TRC_FLAG, MFWD_MGMT_TRC, MFWD_MOD_NAME,
                   "New Value for Global Debug flags is %d",
                   i4SetValMfwdCmnGlobalDebug);
    return i1Status;

}

/****************************************************************************
 Function    :  nmhSetMfwdCmnAvgDataRate
 Input       :  The Indices

                The Object 
                setValMfwdCmnAvgDataRate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMfwdCmnAvgDataRate (INT4 i4SetValMfwdCmnAvgDataRate)
{

    INT1                i1Status = SNMP_SUCCESS;
    gMfwdContext.u4QProcessCnt = i4SetValMfwdCmnAvgDataRate;
    MFWD_TRC_ARG1 (MFWD_TRC_FLAG, MFWD_MGMT_TRC, MFWD_MOD_NAME,
                   "New Value for Global Debug flags is %d",
                   i4SetValMfwdCmnAvgDataRate);
    return i1Status;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2IpCmnMRouteEnable
 Input       :  The Indices

                The Object 
                testValIpCmnMRouteEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IpCmnMRouteEnable (UINT4 *pu4ErrorCode,
                            INT4 i4TestValIpCmnMRouteEnable)
{

    INT1                i1Status = SNMP_SUCCESS;

    if ((gMfwdContext.u1MfwdStatus == i4TestValIpCmnMRouteEnable) ||
        ((i4TestValIpCmnMRouteEnable != MFWD_STATUS_DISABLED) &&
         (i4TestValIpCmnMRouteEnable != MFWD_STATUS_ENABLED)))
    {
        MFWD_TRC_ARG1 (MFWD_TRC_FLAG, MFWD_MGMT_TRC, MFWD_MOD_NAME,
                       "TEST : MFWD Status value is same or invalid %d",
                       i4TestValIpCmnMRouteEnable);
        i1Status = SNMP_FAILURE;
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }
    return i1Status;

}

/****************************************************************************
 Function    :  nmhTestv2IpCmnMRouteEnableCmdb
 Input       :  The Indices

                The Object 
                testValIpCmnMRouteEnableCmdb
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IpCmnMRouteEnableCmdb (UINT4 *pu4ErrorCode,
                                INT4 i4TestValIpCmnMRouteEnableCmdb)
{

    INT1                i1Status = SNMP_SUCCESS;

    if ((i4TestValIpCmnMRouteEnableCmdb != MFWD_CMDB_STATUS_DISABLED) &&
        (i4TestValIpCmnMRouteEnableCmdb != MFWD_CMDB_STATUS_ENABLED))
    {
        MFWD_TRC_ARG1 (MFWD_TRC_FLAG, MFWD_MGMT_TRC, MFWD_MOD_NAME,
                       "TEST : MFWD Status value is same or invalid %d",
                       i4TestValIpCmnMRouteEnableCmdb);
        i1Status = SNMP_FAILURE;
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }
    return i1Status;

}

/****************************************************************************
 Function    :  nmhTestv2MfwdCmnGlobalTrace
 Input       :  The Indices

                The Object 
                testValMfwdCmnGlobalTrace
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MfwdCmnGlobalTrace (UINT4 *pu4ErrorCode,
                             INT4 i4TestValMfwdCmnGlobalTrace)
{

    INT1                i1Status = SNMP_SUCCESS;

    if ((i4TestValMfwdCmnGlobalTrace < 0) ||
        (i4TestValMfwdCmnGlobalTrace > MFWD_MAX_SHORT_INT_VALUE))
    {

        MFWD_TRC_ARG1 (MFWD_TRC_FLAG, MFWD_MGMT_TRC, MFWD_MOD_NAME,
                       "TEST : Invalid global Trace flags %d",
                       i4TestValMfwdCmnGlobalTrace);
        i1Status = SNMP_FAILURE;
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }
    return i1Status;

}

/****************************************************************************
 Function    :  nmhTestv2MfwdCmnGlobalDebug
 Input       :  The Indices

                The Object 
                testValMfwdCmnGlobalDebug
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MfwdCmnGlobalDebug (UINT4 *pu4ErrorCode,
                             INT4 i4TestValMfwdCmnGlobalDebug)
{

    INT1                i1Status = SNMP_SUCCESS;

    if ((i4TestValMfwdCmnGlobalDebug < 0) ||
        (i4TestValMfwdCmnGlobalDebug > MFWD_MAX_SHORT_INT_VALUE))
    {

        MFWD_TRC_ARG1 (MFWD_TRC_FLAG, MFWD_MGMT_TRC, MFWD_MOD_NAME,
                       "TEST : Invalid global Debug flags %d",
                       i4TestValMfwdCmnGlobalDebug);
        i1Status = SNMP_FAILURE;
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }
    return i1Status;

}

/****************************************************************************
 Function    :  nmhTestv2MfwdCmnAvgDataRate
 Input       :  The Indices

                The Object 
                testValMfwdCmnAvgDataRate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MfwdCmnAvgDataRate (UINT4 *pu4ErrorCode,
                             INT4 i4TestValMfwdCmnAvgDataRate)
{

    INT1                i1Status = SNMP_SUCCESS;
    if ((i4TestValMfwdCmnAvgDataRate < 0)
        || (i4TestValMfwdCmnAvgDataRate > 65535))
    {

        MFWD_TRC_ARG1 (MFWD_TRC_FLAG, MFWD_MGMT_TRC, MFWD_MOD_NAME,
                       "TEST : Invalid global Trace flags %d",
                       i4TestValMfwdCmnAvgDataRate);
        i1Status = SNMP_FAILURE;
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }
    return i1Status;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2IpCmnMRouteEnable
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IpCmnMRouteEnable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2IpCmnMRouteEnableCmdb
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IpCmnMRouteEnableCmdb (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2MfwdCmnGlobalTrace
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2MfwdCmnGlobalTrace (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2MfwdCmnGlobalDebug
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2MfwdCmnGlobalDebug (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2MfwdCmnAvgDataRate
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2MfwdCmnAvgDataRate (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : IpCmnMRouteTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIpCmnMRouteTable
 Input       :  The Indices
                IpCmnMRouteOwnerId
                IpCmnMRouteAddrType
                IpCmnMRouteGroup
                IpCmnMRouteSource
                IpCmnMRouteSourceMask
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIpCmnMRouteTable (INT4 i4IpCmnMRouteOwnerId,
                                          INT4 i4IpCmnMRouteAddrType,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pIpCmnMRouteGroup,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pIpCmnMRouteSource,
                                          INT4 i4IpCmnMRouteSourceMask)
{

    INT1                i1Status = SNMP_FAILURE;

    tMfwdRtEntry       *pRtEntry;
    tIPvXAddr           RouteGroup;
    tIPvXAddr           RouteSource;
    UINT2               u2OwnerId;
    u2OwnerId = (UINT2) i4IpCmnMRouteOwnerId;

    if (u2OwnerId >= MAX_MRP_INSTANCES)
    {
        return SNMP_FAILURE;
    }

    MEMSET (&RouteGroup, 0, sizeof (tIPvXAddr));
    MEMSET (&RouteSource, 0, sizeof (tIPvXAddr));

    RouteGroup.u1Afi = i4IpCmnMRouteAddrType;
    RouteGroup.u1AddrLen = pIpCmnMRouteGroup->i4_Length;
    MEMCPY (RouteGroup.au1Addr, pIpCmnMRouteGroup->pu1_OctetList,
            pIpCmnMRouteGroup->i4_Length);

    RouteSource.u1Afi = i4IpCmnMRouteAddrType;
    RouteSource.u1AddrLen = pIpCmnMRouteSource->i4_Length;
    MEMCPY (RouteSource.au1Addr, pIpCmnMRouteSource->pu1_OctetList,
            pIpCmnMRouteSource->i4_Length);

    if (gMfwdContext.u1MfwdStatus != MFWD_STATUS_DISABLED)
    {
        MFWD_LOCK ();

        pRtEntry = MfwdMrtGetExactRtEntry (u2OwnerId,
                                           RouteGroup, RouteSource,
                                           i4IpCmnMRouteSourceMask);
        if (pRtEntry == NULL)
        {
            MFWD_TRC (MFWD_TRC_FLAG, MFWD_MGMT_TRC, MFWD_MOD_NAME,
                      "Invalid indices for Multicast Routing Table \n");
        }
        else
        {
            MFWD_TRC (MFWD_TRC_FLAG, MFWD_MGMT_TRC, MFWD_MOD_NAME,
                      "validate indices for Multicast Routing Table :-"
                      "Success full\n");
            i1Status = SNMP_SUCCESS;
        }
        MFWD_TRC_ARG4 (MFWD_TRC_FLAG, MFWD_MGMT_TRC, MFWD_MOD_NAME,
                       "OwnerId %d, Group %s, Source %s, Source Mask %x\n",
                       u2OwnerId, RouteGroup.au1Addr, RouteSource.au1Addr,
                       i4IpCmnMRouteSourceMask);

        MFWD_UNLOCK ();
    }
    return i1Status;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexIpCmnMRouteTable
 Input       :  The Indices
                IpCmnMRouteOwnerId
                IpCmnMRouteAddrType
                IpCmnMRouteGroup
                IpCmnMRouteSource
                IpCmnMRouteSourceMask
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIpCmnMRouteTable (INT4 *pi4IpCmnMRouteOwnerId,
                                  INT4 *pi4IpCmnMRouteAddrType,
                                  tSNMP_OCTET_STRING_TYPE * pIpCmnMRouteGroup,
                                  tSNMP_OCTET_STRING_TYPE * pIpCmnMRouteSource,
                                  INT4 *pi4IpCmnMRouteSourceMask)
{

    INT1                i1Status = SNMP_FAILURE;
    tMfwdRtEntry       *pRtEntry;
    tIPvXAddr           RouteGroup;
    tIPvXAddr           RouteSource;
    tTMO_SLL_NODE      *pRtNode;

    MEMSET (&RouteGroup, 0, sizeof (tIPvXAddr));
    MEMSET (&RouteSource, 0, sizeof (tIPvXAddr));

    RouteGroup.u1Afi = IPVX_ADDR_FMLY_IPV4;
    RouteGroup.u1AddrLen = pIpCmnMRouteGroup->i4_Length;
    MEMCPY (RouteGroup.au1Addr, pIpCmnMRouteGroup->pu1_OctetList,
            pIpCmnMRouteGroup->i4_Length);

    RouteSource.u1Afi = IPVX_ADDR_FMLY_IPV4;
    RouteSource.u1AddrLen = pIpCmnMRouteSource->i4_Length;
    MEMCPY (RouteSource.au1Addr, pIpCmnMRouteSource->pu1_OctetList,
            pIpCmnMRouteSource->i4_Length);

    if (gMfwdContext.u1MfwdStatus != MFWD_STATUS_DISABLED)
    {
        MFWD_LOCK ();
        pRtNode = TMO_SLL_First (&(gMfwdContext.MrtGetNextList));
        if (pRtNode != NULL)
        {
            pRtEntry = MFWD_GET_BASE_PTR (tMfwdRtEntry, GetNextLink, pRtNode);
            *pi4IpCmnMRouteOwnerId = pRtEntry->u2OwnerId;
            *pi4IpCmnMRouteAddrType = pRtEntry->GrpAddr.u1Afi;
            if (pRtEntry->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
            {
                MEMCPY (pIpCmnMRouteGroup->pu1_OctetList,
                        pRtEntry->GrpAddr.au1Addr, IPVX_IPV4_ADDR_LEN);
                pIpCmnMRouteGroup->i4_Length = IPVX_IPV4_ADDR_LEN;
                MEMCPY (pIpCmnMRouteSource->pu1_OctetList,
                        pRtEntry->pSrcDescNode->SrcAddr.au1Addr,
                        IPVX_IPV4_ADDR_LEN);
                pIpCmnMRouteSource->i4_Length = IPVX_IPV4_ADDR_LEN;
            }

            if (pRtEntry->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
            {
                MEMCPY (pIpCmnMRouteGroup->pu1_OctetList,
                        pRtEntry->GrpAddr.au1Addr, IPVX_IPV6_ADDR_LEN);
                pIpCmnMRouteGroup->i4_Length = IPVX_IPV6_ADDR_LEN;
                MEMCPY (pIpCmnMRouteSource->pu1_OctetList,
                        pRtEntry->pSrcDescNode->SrcAddr.au1Addr,
                        IPVX_IPV6_ADDR_LEN);
                pIpCmnMRouteSource->i4_Length = IPVX_IPV6_ADDR_LEN;
            }
            *pi4IpCmnMRouteSourceMask = pRtEntry->pSrcDescNode->i4RtMask;

            MFWD_TRC (MFWD_TRC_FLAG, MFWD_MGMT_TRC, MFWD_MOD_NAME,
                      "First index for the MRT is \n");
            MFWD_TRC_ARG4 (MFWD_TRC_FLAG, MFWD_MGMT_TRC, MFWD_MOD_NAME,
                           " OwnerId %d, Group %s, Source %s, Source Mask %x\n",
                           pRtEntry->u2OwnerId, pRtEntry->GrpAddr.au1Addr,
                           pRtEntry->pSrcDescNode->RtAddr.au1Addr,
                           pRtEntry->pSrcDescNode->i4RtMask);
            i1Status = SNMP_SUCCESS;

        }
        else
        {
            MFWD_TRC (MFWD_TRC_FLAG, MFWD_MGMT_TRC, MFWD_MOD_NAME,
                      "MRT is  Empty\n");
        }
        MFWD_UNLOCK ();
    }
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIpCmnMRouteTable
 Input       :  The Indices
                IpCmnMRouteOwnerId
                nextIpCmnMRouteOwnerId
                IpCmnMRouteAddrType
                nextIpCmnMRouteAddrType
                IpCmnMRouteGroup
                nextIpCmnMRouteGroup
                IpCmnMRouteSource
                nextIpCmnMRouteSource
                IpCmnMRouteSourceMask
                nextIpCmnMRouteSourceMask
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIpCmnMRouteTable (INT4 i4IpCmnMRouteOwnerId,
                                 INT4 *pi4NextIpCmnMRouteOwnerId,
                                 INT4 i4IpCmnMRouteAddrType,
                                 INT4 *pi4NextIpCmnMRouteAddrType,
                                 tSNMP_OCTET_STRING_TYPE * pIpCmnMRouteGroup,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pNextIpCmnMRouteGroup,
                                 tSNMP_OCTET_STRING_TYPE * pIpCmnMRouteSource,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pNextIpCmnMRouteSource,
                                 INT4 i4IpCmnMRouteSourceMask,
                                 INT4 *pi4NextIpCmnMRouteSourceMask)
{

    INT1                i1Status = SNMP_FAILURE;
    tMfwdRtEntry       *pRtEntry = NULL;
    tTMO_SLL_NODE      *pRtNode = NULL;
    tIPvXAddr           RouteGroup;
    tIPvXAddr           RouteSource;

    if (gMfwdContext.u1MfwdStatus == MFWD_STATUS_DISABLED)
    {
        return i1Status;
    }

    MEMSET (&RouteGroup, 0, sizeof (tIPvXAddr));
    MEMSET (&RouteSource, 0, sizeof (tIPvXAddr));

    RouteGroup.u1Afi = i4IpCmnMRouteAddrType;
    RouteGroup.u1AddrLen = pIpCmnMRouteGroup->i4_Length;
    MEMCPY (RouteGroup.au1Addr, pIpCmnMRouteGroup->pu1_OctetList,
            pIpCmnMRouteGroup->i4_Length);

    RouteSource.u1Afi = i4IpCmnMRouteAddrType;
    RouteSource.u1AddrLen = pIpCmnMRouteSource->i4_Length;
    MEMCPY (RouteSource.au1Addr, pIpCmnMRouteSource->pu1_OctetList,
            pIpCmnMRouteSource->i4_Length);

    MFWD_LOCK ();
    TMO_SLL_Scan (&gMfwdContext.MrtGetNextList, pRtNode, tTMO_SLL_NODE *)
    {
        pRtEntry = MFWD_GET_BASE_PTR (tMfwdRtEntry, GetNextLink, pRtNode);
        if ((pRtEntry->u2OwnerId == i4IpCmnMRouteOwnerId) &&
            (IPVX_ADDR_COMPARE (pRtEntry->GrpAddr, RouteGroup) == 0) &&
            (IPVX_ADDR_COMPARE (pRtEntry->pSrcDescNode->SrcAddr, RouteSource) ==
             0)
            && (pRtEntry->pSrcDescNode->i4RtMask == i4IpCmnMRouteSourceMask))
        {
            pRtNode = TMO_SLL_Next (&gMfwdContext.MrtGetNextList,
                                    (tTMO_SLL_NODE *) & pRtEntry->GetNextLink);
            pRtEntry = MFWD_GET_BASE_PTR (tMfwdRtEntry, GetNextLink, pRtNode);
            break;
        }
    }

    if (pRtNode != NULL)
    {
        *pi4NextIpCmnMRouteOwnerId = pRtEntry->u2OwnerId;
        if (pRtEntry->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
        {
            MEMCPY (pNextIpCmnMRouteGroup->pu1_OctetList,
                    pRtEntry->GrpAddr.au1Addr, IPVX_IPV4_ADDR_LEN);
            pNextIpCmnMRouteGroup->i4_Length = IPVX_IPV4_ADDR_LEN;

            MEMCPY (pNextIpCmnMRouteSource->pu1_OctetList,
                    pRtEntry->pSrcDescNode->SrcAddr.au1Addr,
                    IPVX_IPV4_ADDR_LEN);
            pNextIpCmnMRouteSource->i4_Length = IPVX_IPV4_ADDR_LEN;

            *pi4NextIpCmnMRouteSourceMask = pRtEntry->pSrcDescNode->i4RtMask;
            *pi4NextIpCmnMRouteAddrType = pRtEntry->GrpAddr.u1Afi;
        }

        if (pRtEntry->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
        {
            MEMCPY (pNextIpCmnMRouteGroup->pu1_OctetList,
                    pRtEntry->GrpAddr.au1Addr, IPVX_IPV6_ADDR_LEN);
            pNextIpCmnMRouteGroup->i4_Length = IPVX_IPV6_ADDR_LEN;

            MEMCPY (pNextIpCmnMRouteSource->pu1_OctetList,
                    pRtEntry->pSrcDescNode->SrcAddr.au1Addr,
                    IPVX_IPV6_ADDR_LEN);
            pNextIpCmnMRouteSource->i4_Length = IPVX_IPV6_ADDR_LEN;

            *pi4NextIpCmnMRouteSourceMask = pRtEntry->pSrcDescNode->i4RtMask;
            *pi4NextIpCmnMRouteAddrType = pRtEntry->GrpAddr.u1Afi;
        }

        i1Status = SNMP_SUCCESS;

        MFWD_TRC (MFWD_TRC_FLAG, MFWD_MGMT_TRC, MFWD_MOD_NAME,
                  "Next index for the MRT is \n");

        MFWD_TRC_ARG4 (MFWD_TRC_FLAG, MFWD_MGMT_TRC, MFWD_MOD_NAME,
                       " OwnerId %d, Group %s, Source %s, Source Mask %x\n",
                       pRtEntry->u2OwnerId, pRtEntry->GrpAddr.au1Addr,
                       pRtEntry->pSrcDescNode->SrcAddr.au1Addr,
                       pRtEntry->pSrcDescNode->i4RtMask);
    }
    else
    {

        MFWD_TRC (MFWD_TRC_FLAG, MFWD_MGMT_TRC, MFWD_MOD_NAME,
                  "END of Get Next List for the MRT\n");
    }
    MFWD_UNLOCK ();
    return i1Status;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIpCmnMRouteUpstreamNeighbor
 Input       :  The Indices
                IpCmnMRouteOwnerId
                IpCmnMRouteAddrType
                IpCmnMRouteGroup
                IpCmnMRouteSource
                IpCmnMRouteSourceMask

                The Object 
                retValIpCmnMRouteUpstreamNeighbor
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpCmnMRouteUpstreamNeighbor (INT4 i4IpCmnMRouteOwnerId,
                                   INT4 i4IpCmnMRouteAddrType,
                                   tSNMP_OCTET_STRING_TYPE * pIpCmnMRouteGroup,
                                   tSNMP_OCTET_STRING_TYPE * pIpCmnMRouteSource,
                                   INT4 i4IpCmnMRouteSourceMask,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pRetValIpCmnMRouteUpstreamNeighbor)
{
    INT1                i1Status = SNMP_FAILURE;
    tMfwdRtEntry       *pRtEntry;
    tIPvXAddr           RouteGroup;
    tIPvXAddr           RouteSource;
    UINT2               u2OwnerId;
    u2OwnerId = (UINT2) i4IpCmnMRouteOwnerId;

    if (u2OwnerId >= MAX_MRP_INSTANCES)
    {
        return SNMP_FAILURE;
    }

    MEMSET (pRetValIpCmnMRouteUpstreamNeighbor->pu1_OctetList, 0,
            IPVX_MAX_INET_ADDR_LEN);
    MEMSET (&RouteGroup, 0, sizeof (tIPvXAddr));
    MEMSET (&RouteSource, 0, sizeof (tIPvXAddr));

    RouteGroup.u1Afi = i4IpCmnMRouteAddrType;
    RouteGroup.u1AddrLen = pIpCmnMRouteGroup->i4_Length;
    MEMCPY (RouteGroup.au1Addr, pIpCmnMRouteGroup->pu1_OctetList,
            pIpCmnMRouteGroup->i4_Length);

    RouteSource.u1Afi = i4IpCmnMRouteAddrType;
    RouteSource.u1AddrLen = pIpCmnMRouteSource->i4_Length;
    MEMCPY (RouteSource.au1Addr, pIpCmnMRouteSource->pu1_OctetList,
            pIpCmnMRouteSource->i4_Length);

    MFWD_LOCK ();

    pRtEntry = MfwdMrtGetExactRtEntry (u2OwnerId,
                                       RouteGroup,
                                       RouteSource, i4IpCmnMRouteSourceMask);
    if (pRtEntry != NULL)
    {
        MEMCPY (pRetValIpCmnMRouteUpstreamNeighbor->pu1_OctetList,
                pRtEntry->pSrcDescNode->UpStrmNbr.au1Addr,
                pRtEntry->pSrcDescNode->UpStrmNbr.u1AddrLen);
        pRetValIpCmnMRouteUpstreamNeighbor->i4_Length =
            pRtEntry->pSrcDescNode->UpStrmNbr.u1AddrLen;

        MFWD_TRC_ARG1 (MFWD_TRC_FLAG, MFWD_MGMT_TRC, MFWD_MOD_NAME,
                       "The Up Stream Neighbor for the MRT is %s\n",
                       pRetValIpCmnMRouteUpstreamNeighbor);

        MFWD_TRC_ARG4 (MFWD_TRC_FLAG, MFWD_MGMT_TRC, MFWD_MOD_NAME,
                       " OwnerId %d, Group %s, Source %s, Source Mask %d\n",
                       u2OwnerId, RouteGroup.au1Addr, RouteSource.au1Addr,
                       i4IpCmnMRouteSourceMask);
        i1Status = SNMP_SUCCESS;
    }

    MFWD_UNLOCK ();
    return i1Status;

}

/****************************************************************************
 Function    :  nmhGetIpCmnMRouteInIfIndex
 Input       :  The Indices
                IpCmnMRouteOwnerId
                IpCmnMRouteAddrType
                IpCmnMRouteGroup
                IpCmnMRouteSource
                IpCmnMRouteSourceMask

                The Object 
                retValIpCmnMRouteInIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpCmnMRouteInIfIndex (INT4 i4IpCmnMRouteOwnerId,
                            INT4 i4IpCmnMRouteAddrType,
                            tSNMP_OCTET_STRING_TYPE * pIpCmnMRouteGroup,
                            tSNMP_OCTET_STRING_TYPE * pIpCmnMRouteSource,
                            INT4 i4IpCmnMRouteSourceMask,
                            INT4 *pi4RetValIpCmnMRouteInIfIndex)
{

    INT1                i1Status = SNMP_FAILURE;
    tMfwdRtEntry       *pRtEntry;
    UINT2               u2OwnerId;
    tIPvXAddr           RouteGroup;
    tIPvXAddr           RouteSource;
    tIfConfigRecord     IpInfo;

    u2OwnerId = (UINT2) i4IpCmnMRouteOwnerId;

    if (u2OwnerId >= MAX_MRP_INSTANCES)
    {
        return SNMP_FAILURE;
    }

    MEMSET (&RouteGroup, 0, sizeof (tIPvXAddr));
    MEMSET (&RouteSource, 0, sizeof (tIPvXAddr));

    RouteGroup.u1Afi = i4IpCmnMRouteAddrType;
    RouteGroup.u1AddrLen = pIpCmnMRouteGroup->i4_Length;
    MEMCPY (RouteGroup.au1Addr, pIpCmnMRouteGroup->pu1_OctetList,
            pIpCmnMRouteGroup->i4_Length);

    RouteSource.u1Afi = i4IpCmnMRouteAddrType;
    RouteSource.u1AddrLen = pIpCmnMRouteSource->i4_Length;
    MEMCPY (RouteSource.au1Addr, pIpCmnMRouteSource->pu1_OctetList,
            pIpCmnMRouteSource->i4_Length);

    MFWD_LOCK ();
    pRtEntry = MfwdMrtGetExactRtEntry (u2OwnerId,
                                       RouteGroup,
                                       RouteSource, i4IpCmnMRouteSourceMask);
    if (pRtEntry != NULL)
    {
        if (i4IpCmnMRouteAddrType == IPVX_ADDR_FMLY_IPV4)
        {
            if (IpGetIfConfigRecord ((UINT2) pRtEntry->pSrcDescNode->u4Iif,
                                     &IpInfo) != IP_SUCCESS)
            {
                MFWD_UNLOCK ();
                return SNMP_FAILURE;
            }
            *pi4RetValIpCmnMRouteInIfIndex =
                (INT4) IpInfo.InterfaceId.u4IfIndex;
        }

        MFWD_TRC_ARG1 (MFWD_TRC_FLAG, MFWD_MGMT_TRC, MFWD_MOD_NAME,
                       "The incoming interface for the route entry"
                       " is %d\n", *pi4RetValIpCmnMRouteInIfIndex);

        MFWD_TRC_ARG4 (MFWD_TRC_FLAG, MFWD_MGMT_TRC, MFWD_MOD_NAME,
                       "OwnerId %d, Group %s, Source %s, Source Mask %d\n",
                       u2OwnerId, RouteGroup, RouteSource,
                       i4IpCmnMRouteSourceMask);
        i1Status = SNMP_SUCCESS;
    }

    MFWD_UNLOCK ();
    return i1Status;

}

/****************************************************************************
 Function    :  nmhGetIpCmnMRouteUpTime
 Input       :  The Indices
                IpCmnMRouteOwnerId
                IpCmnMRouteAddrType
                IpCmnMRouteGroup
                IpCmnMRouteSource
                IpCmnMRouteSourceMask

                The Object 
                retValIpCmnMRouteUpTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpCmnMRouteUpTime (INT4 i4IpCmnMRouteOwnerId,
                         INT4 i4IpCmnMRouteAddrType,
                         tSNMP_OCTET_STRING_TYPE * pIpCmnMRouteGroup,
                         tSNMP_OCTET_STRING_TYPE * pIpCmnMRouteSource,
                         INT4 i4IpCmnMRouteSourceMask,
                         UINT4 *pu4RetValIpCmnMRouteUpTime)
{
    INT1                i1Status = SNMP_FAILURE;
    tMfwdRtEntry       *pRtEntry;
    tIPvXAddr           RouteGroup;
    tIPvXAddr           RouteSource;
    UINT2               u2OwnerId;

    u2OwnerId = (UINT2) i4IpCmnMRouteOwnerId;

    if (u2OwnerId >= MAX_MRP_INSTANCES)
    {
        return SNMP_FAILURE;
    }

    MEMSET (&RouteGroup, 0, sizeof (tIPvXAddr));
    MEMSET (&RouteSource, 0, sizeof (tIPvXAddr));

    RouteGroup.u1Afi = i4IpCmnMRouteAddrType;
    RouteGroup.u1AddrLen = pIpCmnMRouteGroup->i4_Length;
    MEMCPY (RouteGroup.au1Addr, pIpCmnMRouteGroup->pu1_OctetList,
            pIpCmnMRouteGroup->i4_Length);

    RouteSource.u1Afi = i4IpCmnMRouteAddrType;
    RouteSource.u1AddrLen = pIpCmnMRouteSource->i4_Length;
    MEMCPY (RouteSource.au1Addr, pIpCmnMRouteSource->pu1_OctetList,
            pIpCmnMRouteSource->i4_Length);

    MFWD_LOCK ();
    pRtEntry = MfwdMrtGetExactRtEntry (u2OwnerId,
                                       RouteGroup,
                                       RouteSource, i4IpCmnMRouteSourceMask);
    if (pRtEntry != NULL)
    {
        *pu4RetValIpCmnMRouteUpTime = pRtEntry->u4EntryUpTime;

        MFWD_TRC_ARG1 (MFWD_TRC_FLAG, MFWD_MGMT_TRC, MFWD_MOD_NAME,
                       "The Entry Up Time Value is %d",
                       *pu4RetValIpCmnMRouteUpTime);

        MFWD_TRC_ARG4 (MFWD_TRC_FLAG, MFWD_MGMT_TRC, MFWD_MOD_NAME,
                       " OwnerId %d, Group %s, Source %s, Source Mask %d\n",
                       u2OwnerId, RouteGroup, RouteSource,
                       i4IpCmnMRouteSourceMask);
        i1Status = SNMP_SUCCESS;
    }

    MFWD_UNLOCK ();
    return i1Status;

}

/****************************************************************************
 Function    :  nmhGetIpCmnMRoutePkts
 Input       :  The Indices
                IpCmnMRouteOwnerId
                IpCmnMRouteAddrType
                IpCmnMRouteGroup
                IpCmnMRouteSource
                IpCmnMRouteSourceMask

                The Object 
                retValIpCmnMRoutePkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpCmnMRoutePkts (INT4 i4IpCmnMRouteOwnerId,
                       INT4 i4IpCmnMRouteAddrType,
                       tSNMP_OCTET_STRING_TYPE * pIpCmnMRouteGroup,
                       tSNMP_OCTET_STRING_TYPE * pIpCmnMRouteSource,
                       INT4 i4IpCmnMRouteSourceMask,
                       UINT4 *pu4RetValIpCmnMRoutePkts)
{
    INT1                i1Status = SNMP_FAILURE;
    tMfwdRtEntry       *pRtEntry;
    tIPvXAddr           RouteGroup;
    tIPvXAddr           RouteSource;
    UINT2               u2OwnerId;
    u2OwnerId = (UINT2) i4IpCmnMRouteOwnerId;

    if (u2OwnerId >= MAX_MRP_INSTANCES)
    {
        return SNMP_FAILURE;
    }

    MEMSET (&RouteGroup, 0, sizeof (tIPvXAddr));
    MEMSET (&RouteSource, 0, sizeof (tIPvXAddr));

    RouteGroup.u1Afi = i4IpCmnMRouteAddrType;
    RouteGroup.u1AddrLen = pIpCmnMRouteGroup->i4_Length;
    MEMCPY (RouteGroup.au1Addr, pIpCmnMRouteGroup->pu1_OctetList,
            pIpCmnMRouteGroup->i4_Length);

    RouteSource.u1Afi = i4IpCmnMRouteAddrType;
    RouteSource.u1AddrLen = pIpCmnMRouteSource->i4_Length;
    MEMCPY (RouteSource.au1Addr, pIpCmnMRouteSource->pu1_OctetList,
            pIpCmnMRouteSource->i4_Length);

    MFWD_LOCK ();
    pRtEntry = MfwdMrtGetExactRtEntry (u2OwnerId,
                                       RouteGroup,
                                       RouteSource, i4IpCmnMRouteSourceMask);
    if (pRtEntry != NULL)
    {
        *pu4RetValIpCmnMRoutePkts = pRtEntry->u4FwdCnt;

        MFWD_TRC_ARG1 (MFWD_TRC_FLAG, MFWD_MGMT_TRC, MFWD_MOD_NAME,
                       "The forward count for the Entry is %d\n",
                       *pu4RetValIpCmnMRoutePkts);

        MFWD_TRC_ARG4 (MFWD_TRC_FLAG, MFWD_MGMT_TRC, MFWD_MOD_NAME,
                       "OwnerId %d, Group %s, Source %s, Source Mask %d\n",
                       u2OwnerId, RouteGroup, RouteSource,
                       i4IpCmnMRouteSourceMask);
        i1Status = SNMP_SUCCESS;
    }

    MFWD_UNLOCK ();
    return i1Status;

}

/****************************************************************************
 Function    :  nmhGetIpCmnMRouteDifferentInIfPackets
 Input       :  The Indices
                IpCmnMRouteOwnerId
                IpCmnMRouteAddrType
                IpCmnMRouteGroup
                IpCmnMRouteSource
                IpCmnMRouteSourceMask

                The Object 
                retValIpCmnMRouteDifferentInIfPackets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpCmnMRouteDifferentInIfPackets (INT4 i4IpCmnMRouteOwnerId,
                                       INT4 i4IpCmnMRouteAddrType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pIpCmnMRouteGroup,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pIpCmnMRouteSource,
                                       INT4 i4IpCmnMRouteSourceMask,
                                       UINT4
                                       *pu4RetValIpCmnMRouteDifferentInIfPackets)
{

    INT1                i1Status = SNMP_FAILURE;
    tMfwdRtEntry       *pRtEntry;
    tIPvXAddr           RouteGroup;
    tIPvXAddr           RouteSource;
    UINT2               u2OwnerId;
    u2OwnerId = (UINT2) i4IpCmnMRouteOwnerId;

    if (u2OwnerId >= MAX_MRP_INSTANCES)
    {
        return SNMP_FAILURE;
    }

    MEMSET (&RouteGroup, 0, sizeof (tIPvXAddr));
    MEMSET (&RouteSource, 0, sizeof (tIPvXAddr));

    RouteGroup.u1Afi = i4IpCmnMRouteAddrType;
    RouteGroup.u1AddrLen = pIpCmnMRouteGroup->i4_Length;
    MEMCPY (RouteGroup.au1Addr, pIpCmnMRouteGroup->pu1_OctetList,
            pIpCmnMRouteGroup->i4_Length);

    RouteSource.u1Afi = i4IpCmnMRouteAddrType;
    RouteSource.u1AddrLen = pIpCmnMRouteSource->i4_Length;
    MEMCPY (RouteSource.au1Addr, pIpCmnMRouteSource->pu1_OctetList,
            pIpCmnMRouteSource->i4_Length);

    MFWD_LOCK ();
    pRtEntry = MfwdMrtGetExactRtEntry (u2OwnerId,
                                       RouteGroup,
                                       RouteSource, i4IpCmnMRouteSourceMask);
    if (pRtEntry != NULL)
    {
        *pu4RetValIpCmnMRouteDifferentInIfPackets = pRtEntry->u4RpfFailCnt;

        MFWD_TRC_ARG1 (MFWD_TRC_FLAG, MFWD_MGMT_TRC, MFWD_MOD_NAME,
                       "The RPF check failure count for the Entry is %d",
                       *pu4RetValIpCmnMRouteDifferentInIfPackets);

        MFWD_TRC_ARG4 (MFWD_TRC_FLAG, MFWD_MGMT_TRC, MFWD_MOD_NAME,
                       " OwnerId %d, Group %s, Source %s, Source Mask %d\n",
                       u2OwnerId, RouteGroup, RouteSource,
                       i4IpCmnMRouteSourceMask);
        i1Status = SNMP_SUCCESS;
    }

    MFWD_UNLOCK ();
    return i1Status;

}

/****************************************************************************
 Function    :  nmhGetIpCmnMRouteProtocol
 Input       :  The Indices
                IpCmnMRouteOwnerId
                IpCmnMRouteAddrType
                IpCmnMRouteGroup
                IpCmnMRouteSource
                IpCmnMRouteSourceMask

                The Object 
                retValIpCmnMRouteProtocol
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpCmnMRouteProtocol (INT4 i4IpCmnMRouteOwnerId,
                           INT4 i4IpCmnMRouteAddrType,
                           tSNMP_OCTET_STRING_TYPE * pIpCmnMRouteGroup,
                           tSNMP_OCTET_STRING_TYPE * pIpCmnMRouteSource,
                           INT4 i4IpCmnMRouteSourceMask,
                           INT4 *pi4RetValIpCmnMRouteProtocol)
{

    INT1                i1Status = SNMP_FAILURE;
    tMfwdRtEntry       *pRtEntry;
    tIPvXAddr           RouteGroup;
    tIPvXAddr           RouteSource;
    UINT2               u2OwnerId;
    u2OwnerId = (UINT2) i4IpCmnMRouteOwnerId;

    if (u2OwnerId >= MAX_MRP_INSTANCES)
    {
        return SNMP_FAILURE;
    }

    MEMSET (&RouteGroup, 0, sizeof (tIPvXAddr));
    MEMSET (&RouteSource, 0, sizeof (tIPvXAddr));

    RouteGroup.u1Afi = i4IpCmnMRouteAddrType;
    RouteGroup.u1AddrLen = pIpCmnMRouteGroup->i4_Length;
    MEMCPY (RouteGroup.au1Addr, pIpCmnMRouteGroup->pu1_OctetList,
            pIpCmnMRouteGroup->i4_Length);

    RouteSource.u1Afi = i4IpCmnMRouteAddrType;
    RouteSource.u1AddrLen = pIpCmnMRouteSource->i4_Length;
    MEMCPY (RouteSource.au1Addr, pIpCmnMRouteSource->pu1_OctetList,
            pIpCmnMRouteSource->i4_Length);

    MFWD_LOCK ();
    pRtEntry = MfwdMrtGetExactRtEntry (u2OwnerId,
                                       RouteGroup,
                                       RouteSource, i4IpCmnMRouteSourceMask);
    if (pRtEntry != NULL)
    {
        *pi4RetValIpCmnMRouteProtocol = MFWD_OWNER (u2OwnerId)->u1ProtoId;

        MFWD_TRC_ARG1 (MFWD_TRC_FLAG, MFWD_MGMT_TRC, MFWD_MOD_NAME,
                       "The Protocol ID for the Entry is %d",
                       *pi4RetValIpCmnMRouteProtocol);

        MFWD_TRC_ARG4 (MFWD_TRC_FLAG, MFWD_MGMT_TRC, MFWD_MOD_NAME,
                       " OwnerId %d, Group %s, Source %s, Source Mask %d\n",
                       u2OwnerId, RouteGroup, RouteSource,
                       i4IpCmnMRouteSourceMask);
        i1Status = SNMP_SUCCESS;
    }

    MFWD_UNLOCK ();
    return i1Status;

}

/****************************************************************************
 Function    :  nmhGetIpCmnMRouteRtAddress
 Input       :  The Indices
                IpCmnMRouteOwnerId
                IpCmnMRouteAddrType
                IpCmnMRouteGroup
                IpCmnMRouteSource
                IpCmnMRouteSourceMask

                The Object 
                retValIpCmnMRouteRtAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpCmnMRouteRtAddress (INT4 i4IpCmnMRouteOwnerId,
                            INT4 i4IpCmnMRouteAddrType,
                            tSNMP_OCTET_STRING_TYPE * pIpCmnMRouteGroup,
                            tSNMP_OCTET_STRING_TYPE * pIpCmnMRouteSource,
                            INT4 i4IpCmnMRouteSourceMask,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValIpCmnMRouteRtAddress)
{

    INT1                i1Status = SNMP_FAILURE;
    tMfwdRtEntry       *pRtEntry;
    tIPvXAddr           RouteGroup;
    tIPvXAddr           RouteSource;
    UINT2               u2OwnerId;
    u2OwnerId = (UINT2) i4IpCmnMRouteOwnerId;

    if (u2OwnerId >= MAX_MRP_INSTANCES)
    {
        return SNMP_FAILURE;
    }

    MEMSET (pRetValIpCmnMRouteRtAddress->pu1_OctetList, 0,
            IPVX_MAX_INET_ADDR_LEN);
    MEMSET (&RouteGroup, 0, sizeof (tIPvXAddr));
    MEMSET (&RouteSource, 0, sizeof (tIPvXAddr));

    RouteGroup.u1Afi = i4IpCmnMRouteAddrType;
    RouteGroup.u1AddrLen = pIpCmnMRouteGroup->i4_Length;
    MEMCPY (RouteGroup.au1Addr, pIpCmnMRouteGroup->pu1_OctetList,
            pIpCmnMRouteGroup->i4_Length);

    RouteSource.u1Afi = i4IpCmnMRouteAddrType;
    RouteSource.u1AddrLen = pIpCmnMRouteSource->i4_Length;
    MEMCPY (RouteSource.au1Addr, pIpCmnMRouteSource->pu1_OctetList,
            pIpCmnMRouteSource->i4_Length);

    MFWD_LOCK ();
    pRtEntry = MfwdMrtGetExactRtEntry (u2OwnerId,
                                       RouteGroup,
                                       RouteSource, i4IpCmnMRouteSourceMask);
    if (pRtEntry != NULL)
    {

        MEMCPY (pRetValIpCmnMRouteRtAddress->pu1_OctetList,
                pRtEntry->pSrcDescNode->RtAddr.au1Addr,
                pRtEntry->pSrcDescNode->RtAddr.u1AddrLen);
        pRetValIpCmnMRouteRtAddress->i4_Length =
            pRtEntry->pSrcDescNode->RtAddr.u1AddrLen;

        MFWD_TRC_ARG1 (MFWD_TRC_FLAG, MFWD_MGMT_TRC, MFWD_MOD_NAME,
                       "The route address for the Entry is %s",
                       *pRetValIpCmnMRouteRtAddress->pu1_OctetList);

        MFWD_TRC_ARG4 (MFWD_TRC_FLAG, MFWD_MGMT_TRC, MFWD_MOD_NAME,
                       " OwnerId %d, Group %s, Source %s, Source Mask %d\n",
                       u2OwnerId, RouteGroup, RouteSource,
                       i4IpCmnMRouteSourceMask);
        i1Status = SNMP_SUCCESS;
    }

    MFWD_UNLOCK ();
    return i1Status;

}

/****************************************************************************
 Function    :  nmhGetIpCmnMRouteRtMask
 Input       :  The Indices
                IpCmnMRouteOwnerId
                IpCmnMRouteAddrType
                IpCmnMRouteGroup
                IpCmnMRouteSource
                IpCmnMRouteSourceMask

                The Object 
                retValIpCmnMRouteRtMask
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpCmnMRouteRtMask (INT4 i4IpCmnMRouteOwnerId,
                         INT4 i4IpCmnMRouteAddrType,
                         tSNMP_OCTET_STRING_TYPE * pIpCmnMRouteGroup,
                         tSNMP_OCTET_STRING_TYPE * pIpCmnMRouteSource,
                         INT4 i4IpCmnMRouteSourceMask,
                         INT4 *pRetValIpCmnMRouteRtMask)
{

    INT1                i1Status = SNMP_FAILURE;
    tMfwdRtEntry       *pRtEntry;
    tIPvXAddr           RouteGroup;
    tIPvXAddr           RouteSource;
    UINT2               u2OwnerId;
    u2OwnerId = (UINT2) i4IpCmnMRouteOwnerId;

    if (u2OwnerId >= MAX_MRP_INSTANCES)
    {
        return SNMP_FAILURE;
    }

    MEMSET (&RouteGroup, 0, sizeof (tIPvXAddr));
    MEMSET (&RouteSource, 0, sizeof (tIPvXAddr));

    RouteGroup.u1Afi = i4IpCmnMRouteAddrType;
    RouteGroup.u1AddrLen = pIpCmnMRouteGroup->i4_Length;
    MEMCPY (RouteGroup.au1Addr, pIpCmnMRouteGroup->pu1_OctetList,
            pIpCmnMRouteGroup->i4_Length);

    RouteSource.u1Afi = i4IpCmnMRouteAddrType;
    RouteSource.u1AddrLen = pIpCmnMRouteSource->i4_Length;
    MEMCPY (RouteSource.au1Addr, pIpCmnMRouteSource->pu1_OctetList,
            pIpCmnMRouteSource->i4_Length);

    MFWD_LOCK ();
    pRtEntry = MfwdMrtGetExactRtEntry (u2OwnerId,
                                       RouteGroup,
                                       RouteSource, i4IpCmnMRouteSourceMask);
    if (pRtEntry != NULL)
    {
        *pRetValIpCmnMRouteRtMask = pRtEntry->pSrcDescNode->i4RtMask;

        MFWD_TRC_ARG1 (MFWD_TRC_FLAG, MFWD_MGMT_TRC, MFWD_MOD_NAME,
                       "The route Mask for the Entry is %d",
                       *pRetValIpCmnMRouteRtMask);

        MFWD_TRC_ARG4 (MFWD_TRC_FLAG, MFWD_MGMT_TRC, MFWD_MOD_NAME,
                       " OwnerId %d, Group %s, Source %s, Source Mask %d\n",
                       u2OwnerId, RouteGroup, RouteSource,
                       i4IpCmnMRouteSourceMask);
        i1Status = SNMP_SUCCESS;
    }

    MFWD_UNLOCK ();
    return i1Status;

}

/****************************************************************************
 Function    :  nmhGetIpCmnMRouteRtType
 Input       :  The Indices
                IpCmnMRouteOwnerId
                IpCmnMRouteAddrType
                IpCmnMRouteGroup
                IpCmnMRouteSource
                IpCmnMRouteSourceMask

                The Object 
                retValIpCmnMRouteRtType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpCmnMRouteRtType (INT4 i4IpCmnMRouteOwnerId,
                         INT4 i4IpCmnMRouteAddrType,
                         tSNMP_OCTET_STRING_TYPE * pIpCmnMRouteGroup,
                         tSNMP_OCTET_STRING_TYPE * pIpCmnMRouteSource,
                         INT4 i4IpCmnMRouteSourceMask,
                         INT4 *pi4RetValIpCmnMRouteRtType)
{

    INT1                i1Status = SNMP_FAILURE;
    tMfwdRtEntry       *pRtEntry;
    tIPvXAddr           RouteGroup;
    tIPvXAddr           RouteSource;
    UINT2               u2OwnerId;
    u2OwnerId = (UINT2) i4IpCmnMRouteOwnerId;

    if (u2OwnerId >= MAX_MRP_INSTANCES)
    {
        return SNMP_FAILURE;
    }

    MEMSET (&RouteGroup, 0, sizeof (tIPvXAddr));
    MEMSET (&RouteSource, 0, sizeof (tIPvXAddr));

    RouteGroup.u1Afi = i4IpCmnMRouteAddrType;
    RouteGroup.u1AddrLen = pIpCmnMRouteGroup->i4_Length;
    MEMCPY (RouteGroup.au1Addr, pIpCmnMRouteGroup->pu1_OctetList,
            pIpCmnMRouteGroup->i4_Length);

    RouteSource.u1Afi = i4IpCmnMRouteAddrType;
    RouteSource.u1AddrLen = pIpCmnMRouteSource->i4_Length;
    MEMCPY (RouteSource.au1Addr, pIpCmnMRouteSource->pu1_OctetList,
            pIpCmnMRouteSource->i4_Length);

    MFWD_LOCK ();
    pRtEntry = MfwdMrtGetExactRtEntry (u2OwnerId,
                                       RouteGroup,
                                       RouteSource, i4IpCmnMRouteSourceMask);
    if (pRtEntry != NULL)
    {
        *pi4RetValIpCmnMRouteRtType = 2;

        MFWD_TRC_ARG1 (MFWD_TRC_FLAG, MFWD_MGMT_TRC, MFWD_MOD_NAME,
                       "The route Type for the Entry is %d",
                       *pi4RetValIpCmnMRouteRtType);

        MFWD_TRC_ARG4 (MFWD_TRC_FLAG, MFWD_MGMT_TRC, MFWD_MOD_NAME,
                       " OwnerId %d, Group %s, Source %s, Source Mask %d\n",
                       u2OwnerId, RouteGroup, RouteSource,
                       i4IpCmnMRouteSourceMask);
        i1Status = SNMP_SUCCESS;
    }

    MFWD_UNLOCK ();
    return i1Status;

}

/* LOW LEVEL Routines for Table : IpCmnMRouteNextHopTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIpCmnMRouteNextHopTable
 Input       :  The Indices
                IpCmnMRouteNextHopOwnerId
                IpCmnMRouteNextHopAddrType
                IpCmnMRouteNextHopGroup
                IpCmnMRouteNextHopSource
                IpCmnMRouteNextHopSourceMask
                IpCmnMRouteNextHopIfIndex
                IpCmnMRouteNextHopAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIpCmnMRouteNextHopTable (INT4
                                                 i4IpCmnMRouteNextHopOwnerId,
                                                 INT4
                                                 i4IpCmnMRouteNextHopAddrType,
                                                 tSNMP_OCTET_STRING_TYPE *
                                                 pIpCmnMRouteNextHopGroup,
                                                 tSNMP_OCTET_STRING_TYPE *
                                                 pIpCmnMRouteNextHopSource,
                                                 INT4
                                                 i4IpCmnMRouteNextHopSourceMask,
                                                 INT4
                                                 i4IpCmnMRouteNextHopIfIndex,
                                                 tSNMP_OCTET_STRING_TYPE *
                                                 pIpCmnMRouteNextHopAddress)
{

    INT1                i1Status = SNMP_FAILURE;
    UINT2               u2OwnerId;
    UINT4               u4IfIndex;
    UINT2               u2Port;
    tMfwdRtEntry       *pRtEntry = NULL;
    tMfwdOifNode       *pOifNode = NULL;
    tMfwdOifNextHopNode *pNextHopNode = NULL;
    tIPvXAddr           RouteGroup;
    tIPvXAddr           RouteSource;
    tIPvXAddr           RouteNxtHop;

    MFWD_GET_PORT_FROM_IF_INDEX ((UINT2) i4IpCmnMRouteNextHopIfIndex, &u2Port);
    u4IfIndex = (UINT4) u2Port;
    u2OwnerId = (UINT2) i4IpCmnMRouteNextHopOwnerId;

    if (u2OwnerId >= MAX_MRP_INSTANCES)
    {
        return SNMP_FAILURE;
    }

    MFWD_LOCK ();

    MEMSET (&RouteGroup, 0, sizeof (tIPvXAddr));
    MEMSET (&RouteSource, 0, sizeof (tIPvXAddr));
    MEMSET (&RouteNxtHop, 0, sizeof (tIPvXAddr));

    RouteGroup.u1Afi = i4IpCmnMRouteNextHopAddrType;
    RouteGroup.u1AddrLen = pIpCmnMRouteNextHopGroup->i4_Length;
    MEMCPY (RouteGroup.au1Addr, pIpCmnMRouteNextHopGroup->pu1_OctetList,
            pIpCmnMRouteNextHopGroup->i4_Length);

    RouteSource.u1Afi = i4IpCmnMRouteNextHopAddrType;
    RouteSource.u1AddrLen = pIpCmnMRouteNextHopSource->i4_Length;
    MEMCPY (RouteSource.au1Addr, pIpCmnMRouteNextHopSource->pu1_OctetList,
            pIpCmnMRouteNextHopSource->i4_Length);

    RouteNxtHop.u1Afi = i4IpCmnMRouteNextHopAddrType;
    RouteNxtHop.u1AddrLen = pIpCmnMRouteNextHopAddress->i4_Length;
    MEMCPY (RouteNxtHop.au1Addr, pIpCmnMRouteNextHopAddress->pu1_OctetList,
            pIpCmnMRouteNextHopAddress->i4_Length);

    if (MFWD_OWNER_MRT (u2OwnerId) != NULL)
    {
        pRtEntry = MfwdMrtGetExactRtEntry (u2OwnerId,
                                           RouteGroup,
                                           RouteSource,
                                           i4IpCmnMRouteNextHopSourceMask);
    }
    if (pRtEntry != NULL)
    {
        TMO_SLL_Scan (&pRtEntry->OifList, pOifNode, tMfwdOifNode *)
        {
            if (pOifNode->u4OifIndex != (UINT4) u4IfIndex)
            {
                continue;
            }
            i1Status = SNMP_SUCCESS;

            TMO_SLL_Scan (&pOifNode->NextHopList, pNextHopNode,
                          tMfwdOifNextHopNode *)
            {
                if (IPVX_ADDR_COMPARE (pNextHopNode->NextHopAddr, RouteNxtHop)
                    == 0)
                {
                    break;
                }
            }
            break;
        }
    }
    MFWD_TRC_ARG6 (MFWD_TRC_FLAG, MFWD_MGMT_TRC, MFWD_MOD_NAME,
                   "OwnerId %d, Group %s, Source %s, Source Mask %x\n"
                   "      Next Hop If %x, Next HopAddr %s\n",
                   u2OwnerId, RouteGroup.au1Addr,
                   RouteSource.au1Addr, i4IpCmnMRouteNextHopSourceMask,
                   i4IpCmnMRouteNextHopIfIndex, RouteNxtHop.au1Addr);

    if (i1Status != SNMP_SUCCESS)
    {
        MFWD_TRC (MFWD_TRC_FLAG, MFWD_MGMT_TRC, MFWD_MOD_NAME,
                  "Invalid indices for Next Hop Table \n");
    }
    else
    {
        MFWD_TRC (MFWD_TRC_FLAG, MFWD_MGMT_TRC, MFWD_MOD_NAME,
                  "validate indices for Next Hop Table Success full\n");
        i1Status = SNMP_SUCCESS;
    }
    MFWD_UNLOCK ();

    return i1Status;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexIpCmnMRouteNextHopTable
 Input       :  The Indices
                IpCmnMRouteNextHopOwnerId
                IpCmnMRouteNextHopAddrType
                IpCmnMRouteNextHopGroup
                IpCmnMRouteNextHopSource
                IpCmnMRouteNextHopSourceMask
                IpCmnMRouteNextHopIfIndex
                IpCmnMRouteNextHopAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIpCmnMRouteNextHopTable (INT4 *pi4IpCmnMRouteNextHopOwnerId,
                                         INT4 *pi4IpCmnMRouteNextHopAddrType,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pIpCmnMRouteNextHopGroup,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pIpCmnMRouteNextHopSource,
                                         INT4 *pi4IpCmnMRouteNextHopSourceMask,
                                         INT4 *pi4IpCmnMRouteNextHopIfIndex,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pIpCmnMRouteNextHopAddress)
{

    INT1                i1Status = SNMP_FAILURE;
    tMfwdRtEntry       *pRtEntry = NULL;
    tTMO_SLL_NODE      *pRtNode;
    tMfwdOifNode       *pOifNode = NULL;
    tMfwdOifNextHopNode *pNextHopNode = NULL;
    tIPvXAddr           RouteGroup;
    tIPvXAddr           RouteSource;
    tIPvXAddr           RouteNxtHop;
    tIfConfigRecord     IpInfo;
    tNetIpv6IfInfo      NetIpv6IfInfo;

    MFWD_LOCK ();

    MEMSET (&RouteGroup, 0, sizeof (tIPvXAddr));
    MEMSET (&RouteSource, 0, sizeof (tIPvXAddr));
    MEMSET (&RouteNxtHop, 0, sizeof (tIPvXAddr));

    RouteGroup.u1Afi = IPVX_ADDR_FMLY_IPV4;
    RouteGroup.u1AddrLen = pIpCmnMRouteNextHopGroup->i4_Length;
    MEMCPY (RouteGroup.au1Addr, pIpCmnMRouteNextHopGroup->pu1_OctetList,
            pIpCmnMRouteNextHopGroup->i4_Length);

    RouteSource.u1Afi = IPVX_ADDR_FMLY_IPV4;
    RouteSource.u1AddrLen = pIpCmnMRouteNextHopSource->i4_Length;
    MEMCPY (RouteSource.au1Addr, pIpCmnMRouteNextHopSource->pu1_OctetList,
            pIpCmnMRouteNextHopSource->i4_Length);

    RouteNxtHop.u1Afi = IPVX_ADDR_FMLY_IPV4;
    RouteNxtHop.u1AddrLen = pIpCmnMRouteNextHopSource->i4_Length;
    MEMCPY (RouteNxtHop.au1Addr, pIpCmnMRouteNextHopSource->pu1_OctetList,
            pIpCmnMRouteNextHopSource->i4_Length);

    pRtNode = TMO_SLL_First (&(gMfwdContext.MrtGetNextList));
    if (pRtNode != NULL)
    {
        pRtEntry = MFWD_GET_BASE_PTR (tMfwdRtEntry, GetNextLink, pRtNode);

        TMO_SLL_Scan (&pRtEntry->OifList, pOifNode, tMfwdOifNode *)
        {
            pNextHopNode = (tMfwdOifNextHopNode *)
                TMO_SLL_First (&pOifNode->NextHopList);
            if (pNextHopNode != NULL)
            {
                break;
            }
        }

        if (pNextHopNode != NULL)
        {
            *pi4IpCmnMRouteNextHopOwnerId = pRtEntry->u2OwnerId;
            if (pRtEntry->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
            {
                MEMCPY (pIpCmnMRouteNextHopGroup->pu1_OctetList,
                        pRtEntry->GrpAddr.au1Addr, IPVX_IPV4_ADDR_LEN);
                pIpCmnMRouteNextHopGroup->i4_Length = IPVX_IPV4_ADDR_LEN;
                MEMCPY (pIpCmnMRouteNextHopSource->pu1_OctetList,
                        pRtEntry->pSrcDescNode->SrcAddr.au1Addr,
                        IPVX_IPV4_ADDR_LEN);
                pIpCmnMRouteNextHopSource->i4_Length = IPVX_IPV4_ADDR_LEN;
                *pi4IpCmnMRouteNextHopAddrType = pRtEntry->GrpAddr.u1Afi;

                *pi4IpCmnMRouteNextHopSourceMask =
                    pRtEntry->pSrcDescNode->i4RtMask;
                if (IpGetIfConfigRecord ((UINT2) pOifNode->u4OifIndex, &IpInfo)
                    != IP_SUCCESS)
                {
                    MFWD_UNLOCK ();
                    return SNMP_FAILURE;
                }
                *pi4IpCmnMRouteNextHopIfIndex =
                    (INT4) IpInfo.InterfaceId.u4IfIndex;
                MEMCPY (pIpCmnMRouteNextHopAddress->pu1_OctetList,
                        pNextHopNode->NextHopAddr.au1Addr, IPVX_IPV4_ADDR_LEN);
                pIpCmnMRouteNextHopAddress->i4_Length = IPVX_IPV4_ADDR_LEN;
            }

            if (pRtEntry->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
            {
                MEMCPY (pIpCmnMRouteNextHopGroup->pu1_OctetList,
                        pRtEntry->GrpAddr.au1Addr, IPVX_IPV6_ADDR_LEN);
                pIpCmnMRouteNextHopGroup->i4_Length = IPVX_IPV6_ADDR_LEN;
                MEMCPY (pIpCmnMRouteNextHopSource->pu1_OctetList,
                        pRtEntry->pSrcDescNode->SrcAddr.au1Addr,
                        IPVX_IPV6_ADDR_LEN);
                pIpCmnMRouteNextHopSource->i4_Length = IPVX_IPV6_ADDR_LEN;
                *pi4IpCmnMRouteNextHopAddrType = pRtEntry->GrpAddr.u1Afi;

                *pi4IpCmnMRouteNextHopSourceMask =
                    pRtEntry->pSrcDescNode->i4RtMask;
                if (MfwdIpv6GetIfInfo
                    ((UINT2) pOifNode->u4OifIndex,
                     &NetIpv6IfInfo) != IP_SUCCESS)
                {
                    MFWD_UNLOCK ();
                    return SNMP_FAILURE;
                }
                *pi4IpCmnMRouteNextHopIfIndex = (INT4) NetIpv6IfInfo.u4IfIndex;
                MEMCPY (pIpCmnMRouteNextHopAddress->pu1_OctetList,
                        pNextHopNode->NextHopAddr.au1Addr, IPVX_IPV6_ADDR_LEN);
                pIpCmnMRouteNextHopAddress->i4_Length = IPVX_IPV6_ADDR_LEN;
            }

            MFWD_TRC (MFWD_TRC_FLAG, MFWD_MGMT_TRC, MFWD_MOD_NAME,
                      "First index for the Next Hop Table is \n");
            MFWD_TRC_ARG6 (MFWD_TRC_FLAG, MFWD_MGMT_TRC,
                           MFWD_MOD_NAME,
                           " OwnerId %d, Group %s, Source %s, Source Mask %x\n"
                           "       Next Hop If %x Next Hop Address %s\n",
                           pRtEntry->u2OwnerId, pRtEntry->GrpAddr.au1Addr,
                           pRtEntry->pSrcDescNode->RtAddr.au1Addr,
                           pRtEntry->pSrcDescNode->i4RtMask,
                           pOifNode->u4OifIndex,
                           pNextHopNode->NextHopAddr.au1Addr);
            i1Status = SNMP_SUCCESS;
        }
    }
    else
    {
        MFWD_TRC (MFWD_TRC_FLAG, MFWD_MGMT_TRC, MFWD_MOD_NAME,
                  "MRT is  Empty cannot look into next hop table\n");
    }
    MFWD_UNLOCK ();
    return i1Status;

}

/****************************************************************************
 Function    :  nmhGetNextIndexIpCmnMRouteNextHopTable
 Input       :  The Indices
                IpCmnMRouteNextHopOwnerId
                nextIpCmnMRouteNextHopOwnerId
                IpCmnMRouteNextHopAddrType
                nextIpCmnMRouteNextHopAddrType
                IpCmnMRouteNextHopGroup
                nextIpCmnMRouteNextHopGroup
                IpCmnMRouteNextHopSource
                nextIpCmnMRouteNextHopSource
                IpCmnMRouteNextHopSourceMask
                nextIpCmnMRouteNextHopSourceMask
                IpCmnMRouteNextHopIfIndex
                nextIpCmnMRouteNextHopIfIndex
                IpCmnMRouteNextHopAddress
                nextIpCmnMRouteNextHopAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIpCmnMRouteNextHopTable (INT4 i4IpCmnMRouteNextHopOwnerId,
                                        INT4 *pi4NextIpCmnMRouteNextHopOwnerId,
                                        INT4 i4IpCmnMRouteNextHopAddrType,
                                        INT4 *pi4NextIpCmnMRouteNextHopAddrType,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pIpCmnMRouteNextHopGroup,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pNextIpCmnMRouteNextHopGroup,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pIpCmnMRouteNextHopSource,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pNextIpCmnMRouteNextHopSource,
                                        INT4 i4IpCmnMRouteNextHopSourceMask,
                                        INT4
                                        *pi4NextIpCmnMRouteNextHopSourceMask,
                                        INT4 i4IpCmnMRouteNextHopIfIndex,
                                        INT4 *pi4NextIpCmnMRouteNextHopIfIndex,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pIpCmnMRouteNextHopAddress,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pNextIpCmnMRouteNextHopAddress)
{

    INT1                i1Status = SNMP_FAILURE;
    tMfwdRtEntry       *pRtEntry = NULL;
    tMfwdOifNode       *pOifNode = NULL;
    tTMO_SLL_NODE      *pRtNode = NULL;
    tMfwdOifNextHopNode *pNextHopNode = NULL;
    UINT1               u1FindNext = 0;
    UINT2               u2OwnerId;
    UINT4               u4IfIndex;
    UINT2               u2Port;
    tIPvXAddr           RouteGroup;
    tIPvXAddr           RouteSource;
    tIPvXAddr           RouteNxtHop;
    tNetIpv6IfInfo      NetIpv6IfInfo;
    tIfConfigRecord     IpInfo;

    MFWD_GET_PORT_FROM_IF_INDEX ((UINT2) i4IpCmnMRouteNextHopIfIndex, &u2Port);
    u4IfIndex = (UINT4) u2Port;
    u2OwnerId = (UINT2) i4IpCmnMRouteNextHopOwnerId;

    if (u2OwnerId >= MAX_MRP_INSTANCES)
    {
        return SNMP_FAILURE;
    }

    MFWD_LOCK ();

    MEMSET (&RouteGroup, 0, sizeof (tIPvXAddr));
    MEMSET (&RouteSource, 0, sizeof (tIPvXAddr));
    MEMSET (&RouteNxtHop, 0, sizeof (tIPvXAddr));

    RouteGroup.u1Afi = i4IpCmnMRouteNextHopAddrType;
    RouteGroup.u1AddrLen = pIpCmnMRouteNextHopGroup->i4_Length;
    MEMCPY (RouteGroup.au1Addr, pIpCmnMRouteNextHopGroup->pu1_OctetList,
            pIpCmnMRouteNextHopGroup->i4_Length);

    RouteSource.u1Afi = i4IpCmnMRouteNextHopAddrType;
    RouteSource.u1AddrLen = pIpCmnMRouteNextHopSource->i4_Length;
    MEMCPY (RouteSource.au1Addr, pIpCmnMRouteNextHopSource->pu1_OctetList,
            pIpCmnMRouteNextHopSource->i4_Length);

    RouteNxtHop.u1Afi = i4IpCmnMRouteNextHopAddrType;
    RouteNxtHop.u1AddrLen = pIpCmnMRouteNextHopAddress->i4_Length;
    MEMCPY (RouteNxtHop.au1Addr, pIpCmnMRouteNextHopAddress->pu1_OctetList,
            pIpCmnMRouteNextHopAddress->i4_Length);

    if (MFWD_OWNER_MRT (u2OwnerId) != NULL)
    {
        pRtEntry = MfwdMrtGetExactRtEntry (u2OwnerId,
                                           RouteGroup,
                                           RouteSource,
                                           i4IpCmnMRouteNextHopSourceMask);
    }
    if (pRtEntry == NULL)
    {
        MFWD_UNLOCK ();
        return i1Status;
    }

    TMO_SLL_Scan (&pRtEntry->OifList, pOifNode, tMfwdOifNode *)
    {
        if (u1FindNext)
        {
            pNextHopNode = (tMfwdOifNextHopNode *)
                TMO_SLL_First (&pOifNode->NextHopList);
            if (pNextHopNode != NULL)
                break;
        }

        if (pOifNode->u4OifIndex == u4IfIndex)
        {
            TMO_SLL_Scan (&pOifNode->NextHopList, pNextHopNode,
                          tMfwdOifNextHopNode *)
            {

                if (IPVX_ADDR_COMPARE (pNextHopNode->NextHopAddr, RouteNxtHop)
                    == 0)
                {
                    pNextHopNode = (tMfwdOifNextHopNode *)
                        TMO_SLL_Next (&(pOifNode->NextHopList),
                                      &pNextHopNode->NextHopNode);
                    if (pNextHopNode == NULL)
                        u1FindNext = 1;
                    break;
                }

            }                    /* end of if (pOifNode->u4OifIndex == */

        }                        /* end of TMO_SLL_Scan (&pOifNode->NextHopList */

        if (pNextHopNode != NULL)
        {
            break;
        }

    }                            /* end of TMO_SLL_Scan (&pRtEntry->OifList */

    /* If the next node is not part of the oif list then see  in the
     * other route entries
     */
    if (pNextHopNode == NULL)
    {
        pRtNode = TMO_SLL_Next (&gMfwdContext.MrtGetNextList,
                                &pRtEntry->GetNextLink);
        if (pRtNode != NULL)
        {
            pRtEntry = MFWD_GET_BASE_PTR (tMfwdRtEntry, GetNextLink, pRtNode);
            TMO_SLL_Scan (&pRtEntry->OifList, pOifNode, tMfwdOifNode *)
            {
                pNextHopNode = (tMfwdOifNextHopNode *)
                    TMO_SLL_First (&pOifNode->NextHopList);
                if (pNextHopNode != NULL)
                {
                    break;
                }
            }
        }                        /* end of if (pRtEntry != NULL) */
    }                            /* end of if (pNextHopNode == NULL) */

    if (pNextHopNode != NULL)
    {
        *pi4NextIpCmnMRouteNextHopOwnerId = pRtEntry->u2OwnerId;
        if (pRtEntry->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
        {
            memcpy (pNextIpCmnMRouteNextHopGroup->pu1_OctetList,
                    pRtEntry->GrpAddr.au1Addr, IPVX_IPV4_ADDR_LEN);
            memcpy (pNextIpCmnMRouteNextHopSource->pu1_OctetList,
                    pRtEntry->pSrcDescNode->SrcAddr.au1Addr,
                    IPVX_IPV4_ADDR_LEN);
            *pi4NextIpCmnMRouteNextHopSourceMask =
                pRtEntry->pSrcDescNode->i4RtMask;
            *pi4NextIpCmnMRouteNextHopAddrType = pRtEntry->GrpAddr.u1Afi;
            if (IpGetIfConfigRecord ((UINT2) pOifNode->u4OifIndex, &IpInfo)
                != IP_SUCCESS)
            {
                MFWD_UNLOCK ();
                return SNMP_FAILURE;
            }

            *pi4NextIpCmnMRouteNextHopIfIndex =
                (INT4) IpInfo.InterfaceId.u4IfIndex;
            memcpy (pNextIpCmnMRouteNextHopAddress->pu1_OctetList,
                    pNextHopNode->NextHopAddr.au1Addr, IPVX_IPV4_ADDR_LEN);
            i1Status = SNMP_SUCCESS;
        }

        if (pRtEntry->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
        {
            memcpy (pNextIpCmnMRouteNextHopGroup->pu1_OctetList,
                    pRtEntry->GrpAddr.au1Addr, IPVX_IPV6_ADDR_LEN);
            memcpy (pNextIpCmnMRouteNextHopSource->pu1_OctetList,
                    pRtEntry->pSrcDescNode->SrcAddr.au1Addr,
                    IPVX_IPV6_ADDR_LEN);
            *pi4NextIpCmnMRouteNextHopSourceMask =
                pRtEntry->pSrcDescNode->i4RtMask;
            *pi4NextIpCmnMRouteNextHopAddrType = pRtEntry->GrpAddr.u1Afi;

            if (MfwdIpv6GetIfInfo ((UINT2) pOifNode->u4OifIndex, &NetIpv6IfInfo)
                != IP_SUCCESS)
            {
                MFWD_UNLOCK ();
                return SNMP_FAILURE;
            }
            *pi4NextIpCmnMRouteNextHopIfIndex = (INT4) NetIpv6IfInfo.u4IfIndex;
            memcpy (pNextIpCmnMRouteNextHopAddress->pu1_OctetList,
                    pNextHopNode->NextHopAddr.au1Addr, IPVX_IPV6_ADDR_LEN);
            i1Status = SNMP_SUCCESS;
        }

    }
    MFWD_UNLOCK ();
    return i1Status;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIpCmnMRouteNextHopState
 Input       :  The Indices
                IpCmnMRouteNextHopOwnerId
                IpCmnMRouteNextHopAddrType
                IpCmnMRouteNextHopGroup
                IpCmnMRouteNextHopSource
                IpCmnMRouteNextHopSourceMask
                IpCmnMRouteNextHopIfIndex
                IpCmnMRouteNextHopAddress

                The Object 
                retValIpCmnMRouteNextHopState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpCmnMRouteNextHopState (INT4 i4IpCmnMRouteNextHopOwnerId,
                               INT4 i4IpCmnMRouteNextHopAddrType,
                               tSNMP_OCTET_STRING_TYPE *
                               pIpCmnMRouteNextHopGroup,
                               tSNMP_OCTET_STRING_TYPE *
                               pIpCmnMRouteNextHopSource,
                               INT4 i4IpCmnMRouteNextHopSourceMask,
                               INT4 i4IpCmnMRouteNextHopIfIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pIpCmnMRouteNextHopAddress,
                               INT4 *pi4RetValIpCmnMRouteNextHopState)
{

    INT1                i1Status = SNMP_FAILURE;
    UINT2               u2OwnerId;
    UINT4               u4IfIndex;
    UINT2               u2Port;
    tMfwdRtEntry       *pRtEntry = NULL;
    tMfwdOifNode       *pOifNode = NULL;
    tMfwdOifNextHopNode *pNextHopNode = NULL;
    tIPvXAddr           RouteNextHopGroup;
    tIPvXAddr           RouteNextHopSource;

    MFWD_GET_PORT_FROM_IF_INDEX ((UINT2) i4IpCmnMRouteNextHopIfIndex, &u2Port);
    u4IfIndex = (UINT4) u2Port;

    u2OwnerId = (UINT2) i4IpCmnMRouteNextHopOwnerId;
    *pi4RetValIpCmnMRouteNextHopState = MFWD_OIF_NEXTHOP_STATE_FORWARDING;

    if (u2OwnerId >= MAX_MRP_INSTANCES)
    {
        return SNMP_FAILURE;
    }

    MEMSET (&RouteNextHopGroup, 0, sizeof (tIPvXAddr));
    MEMSET (&RouteNextHopSource, 0, sizeof (tIPvXAddr));

    RouteNextHopGroup.u1Afi = i4IpCmnMRouteNextHopAddrType;
    RouteNextHopGroup.u1AddrLen = pIpCmnMRouteNextHopGroup->i4_Length;
    MEMCPY (RouteNextHopGroup.au1Addr, pIpCmnMRouteNextHopGroup->pu1_OctetList,
            pIpCmnMRouteNextHopGroup->i4_Length);

    RouteNextHopSource.u1Afi = i4IpCmnMRouteNextHopAddrType;
    RouteNextHopSource.u1AddrLen = pIpCmnMRouteNextHopSource->i4_Length;
    MEMCPY (RouteNextHopSource.au1Addr,
            pIpCmnMRouteNextHopSource->pu1_OctetList,
            pIpCmnMRouteNextHopSource->i4_Length);

    MFWD_LOCK ();
    pRtEntry = MfwdMrtGetExactRtEntry (u2OwnerId,
                                       RouteNextHopGroup,
                                       RouteNextHopSource,
                                       i4IpCmnMRouteNextHopSourceMask);
    if (pRtEntry != NULL)
    {
        TMO_SLL_Scan (&pRtEntry->OifList, pOifNode, tMfwdOifNode *)
        {
            if (pOifNode->u4OifIndex != u4IfIndex)
            {
                continue;
            }
            i1Status = SNMP_SUCCESS;

            *pi4RetValIpCmnMRouteNextHopState = pOifNode->u4OifState;
            TMO_SLL_Scan (&pOifNode->NextHopList, pNextHopNode,
                          tMfwdOifNextHopNode *)
            {
                if (MEMCMP
                    (pNextHopNode->NextHopAddr.au1Addr,
                     pIpCmnMRouteNextHopAddress->pu1_OctetList,
                     pNextHopNode->NextHopAddr.u1AddrLen) == 0)
                {
                    break;
                }
            }
        }
    }
    MFWD_TRC_ARG6 (MFWD_TRC_FLAG, MFWD_MGMT_TRC, MFWD_MOD_NAME,
                   "OwnerId %d, Group %s, Source %s, Source Mask %d\n"
                   "   Next Hop If %s, Next HopAddr %s\n",
                   u2OwnerId, RouteNextHopGroup,
                   RouteNextHopSource, i4IpCmnMRouteNextHopSourceMask,
                   i4IpCmnMRouteNextHopIfIndex,
                   pIpCmnMRouteNextHopAddress->pu1_OctetList);

    if (i1Status != SNMP_SUCCESS)
    {
        MFWD_TRC (MFWD_TRC_FLAG, MFWD_MGMT_TRC, MFWD_MOD_NAME,
                  "Invalid indices for Next Hop Table \n");
    }
    else
    {
        MFWD_TRC (MFWD_TRC_FLAG, MFWD_MGMT_TRC, MFWD_MOD_NAME,
                  "validate indices for Next Hop Table Success full\n");
    }
    MFWD_UNLOCK ();

    return i1Status;

}

/****************************************************************************
 Function    :  nmhGetIpCmnMRouteNextHopUpTime
 Input       :  The Indices
                IpCmnMRouteNextHopOwnerId
                IpCmnMRouteNextHopAddrType
                IpCmnMRouteNextHopGroup
                IpCmnMRouteNextHopSource
                IpCmnMRouteNextHopSourceMask
                IpCmnMRouteNextHopIfIndex
                IpCmnMRouteNextHopAddress

                The Object 
                retValIpCmnMRouteNextHopUpTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpCmnMRouteNextHopUpTime (INT4 i4IpCmnMRouteNextHopOwnerId,
                                INT4 i4IpCmnMRouteNextHopAddrType,
                                tSNMP_OCTET_STRING_TYPE *
                                pIpCmnMRouteNextHopGroup,
                                tSNMP_OCTET_STRING_TYPE *
                                pIpCmnMRouteNextHopSource,
                                INT4 i4IpCmnMRouteNextHopSourceMask,
                                INT4 i4IpCmnMRouteNextHopIfIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pIpCmnMRouteNextHopAddress,
                                UINT4 *pu4RetValIpCmnMRouteNextHopUpTime)
{
    INT1                i1Status = SNMP_FAILURE;
    UINT2               u2OwnerId;
    UINT4               u4IfIndex;
    UINT2               u2Port;
    tMfwdRtEntry       *pRtEntry = NULL;
    tMfwdOifNode       *pOifNode = NULL;
    tMfwdOifNextHopNode *pNextHopNode = NULL;
    tIPvXAddr           RouteNextHopGroup;
    tIPvXAddr           RouteNextHopSource;

    MFWD_GET_PORT_FROM_IF_INDEX ((UINT2) i4IpCmnMRouteNextHopIfIndex, &u2Port);
    u4IfIndex = (UINT4) u2Port;
    u2OwnerId = (UINT2) i4IpCmnMRouteNextHopOwnerId;

    if (u2OwnerId >= MAX_MRP_INSTANCES)
    {
        return SNMP_FAILURE;
    }

    MEMSET (&RouteNextHopGroup, 0, sizeof (tIPvXAddr));
    MEMSET (&RouteNextHopSource, 0, sizeof (tIPvXAddr));

    RouteNextHopGroup.u1Afi = i4IpCmnMRouteNextHopAddrType;
    RouteNextHopGroup.u1AddrLen = pIpCmnMRouteNextHopGroup->i4_Length;
    MEMCPY (RouteNextHopGroup.au1Addr, pIpCmnMRouteNextHopGroup->pu1_OctetList,
            pIpCmnMRouteNextHopGroup->i4_Length);

    RouteNextHopSource.u1Afi = i4IpCmnMRouteNextHopAddrType;
    RouteNextHopSource.u1AddrLen = pIpCmnMRouteNextHopSource->i4_Length;
    MEMCPY (RouteNextHopSource.au1Addr,
            pIpCmnMRouteNextHopSource->pu1_OctetList,
            pIpCmnMRouteNextHopSource->i4_Length);

    MFWD_LOCK ();
    pRtEntry = MfwdMrtGetExactRtEntry (u2OwnerId,
                                       RouteNextHopGroup,
                                       RouteNextHopSource,
                                       i4IpCmnMRouteNextHopSourceMask);
    if (pRtEntry != NULL)
    {
        TMO_SLL_Scan (&pRtEntry->OifList, pOifNode, tMfwdOifNode *)
        {
            if (pOifNode->u4OifIndex != (UINT4) u4IfIndex)
            {
                continue;
            }

            TMO_SLL_Scan (&pOifNode->NextHopList, pNextHopNode,
                          tMfwdOifNextHopNode *)
            {
                if (MEMCMP
                    (pNextHopNode->NextHopAddr.au1Addr,
                     pIpCmnMRouteNextHopAddress->pu1_OctetList,
                     pNextHopNode->NextHopAddr.u1AddrLen) == 0)
                {
                    break;
                }
            }
        }
    }
    MFWD_TRC_ARG6 (MFWD_TRC_FLAG, MFWD_MGMT_TRC, MFWD_MOD_NAME,
                   "OwnerId %d, Group %s, Source %s, Source Mask %d\n"
                   " Next Hop If %x, Next HopAddr %s\n",
                   u2OwnerId, RouteNextHopGroup,
                   RouteNextHopSource, i4IpCmnMRouteNextHopSourceMask,
                   i4IpCmnMRouteNextHopIfIndex, pIpCmnMRouteNextHopAddress);

    if (pNextHopNode == NULL)
    {
        MFWD_TRC (MFWD_TRC_FLAG, MFWD_MGMT_TRC, MFWD_MOD_NAME,
                  "Invalid indices for Next Hop Table \n");
    }
    else
    {
        MFWD_TRC (MFWD_TRC_FLAG, MFWD_MGMT_TRC, MFWD_MOD_NAME,
                  "validate indices for Next Hop Table Success full\n");
        *pu4RetValIpCmnMRouteNextHopUpTime = pNextHopNode->u4NextHopUpTime;
        i1Status = SNMP_SUCCESS;
    }
    MFWD_UNLOCK ();

    return i1Status;

}

/* LOW LEVEL Routines for Table : IpCmnMRouteInterfaceTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIpCmnMRouteInterfaceTable
 Input       :  The Indices
                IpCmnMRouteInterfaceIfIndex
                IpCmnMRouteInterfaceAddrType
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIpCmnMRouteInterfaceTable (INT4
                                                   i4IpCmnMRouteInterfaceIfIndex,
                                                   INT4
                                                   i4IpCmnMRouteInterfaceAddrType)
{

    INT1                i1Status = SNMP_FAILURE;
    tMfwdInterfaceNode *pIfNode = NULL;
    UINT4               u4IfIndex;
    UINT2               u2Port;

    MFWD_GET_PORT_FROM_IF_INDEX ((UINT2) i4IpCmnMRouteInterfaceIfIndex,
                                 &u2Port);
    u4IfIndex = (UINT4) u2Port;

    if (gMfwdContext.u1MfwdStatus != MFWD_STATUS_DISABLED)
    {
        MFWD_LOCK ();
        pIfNode = MFWD_GET_IF_NODE (u4IfIndex, i4IpCmnMRouteInterfaceAddrType);

        if (pIfNode != NULL)
        {
            i1Status = SNMP_SUCCESS;
            MFWD_TRC_ARG2 (MFWD_TRC_FLAG, MFWD_MGMT_TRC, MFWD_MOD_NAME,
                           "Validating indices for interface table "
                           "Succeeded If Index %d Addr Type %d\n",
                           i4IpCmnMRouteInterfaceIfIndex,
                           i4IpCmnMRouteInterfaceAddrType);
        }
        else
        {
            MFWD_TRC_ARG2 (MFWD_TRC_FLAG, MFWD_MGMT_TRC, MFWD_MOD_NAME,
                           "Validating indices for interface table "
                           "Failed If Index %d Addr Type %d\n",
                           i4IpCmnMRouteInterfaceIfIndex,
                           i4IpCmnMRouteInterfaceAddrType);
        }
        MFWD_UNLOCK ();
    }
    return i1Status;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexIpCmnMRouteInterfaceTable
 Input       :  The Indices
                IpCmnMRouteInterfaceIfIndex
                IpCmnMRouteInterfaceAddrType
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIpCmnMRouteInterfaceTable (INT4 *pi4IpCmnMRouteInterfaceIfIndex,
                                           INT4
                                           *pi4IpCmnMRouteInterfaceAddrType)
{

    return (nmhGetNextIndexIpCmnMRouteInterfaceTable
            (0, pi4IpCmnMRouteInterfaceIfIndex, 1,
             pi4IpCmnMRouteInterfaceAddrType));

}

/****************************************************************************
 Function    :  nmhGetNextIndexIpCmnMRouteInterfaceTable
 Input       :  The Indices
                IpCmnMRouteInterfaceIfIndex
                nextIpCmnMRouteInterfaceIfIndex
                IpCmnMRouteInterfaceAddrType
                nextIpCmnMRouteInterfaceAddrType
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIpCmnMRouteInterfaceTable (INT4 i4IpCmnMRouteInterfaceIfIndex,
                                          INT4
                                          *pi4NextIpCmnMRouteInterfaceIfIndex,
                                          INT4 i4IpCmnMRouteInterfaceAddrType,
                                          INT4
                                          *pi4NextIpCmnMRouteInterfaceAddrType)
{

    UINT1               u1NextAddrType = 0;
    INT1                i1Status = SNMP_FAILURE;
    tMfwdInterfaceNode *pIfNode = NULL;
    tTMO_SLL_NODE      *pSllNode = NULL;
    UINT4               u4IfIndex = 0;
    UINT2               u2OwnerId = 0;
    UINT1               u1FindNext = 0;
    UINT4               u4NextIndex = CFA_MAX_INTERFACES_IN_SYS;
    tIfConfigRecord     IpInfo;

    if (i4IpCmnMRouteInterfaceIfIndex < 0)
    {
        return SNMP_FAILURE;
    }

    MFWD_LOCK ();
    for (; u2OwnerId < gMfwdContext.u2MaxMrps; u2OwnerId++)
    {
        if (MFWD_OWNER (u2OwnerId) == NULL)
        {
            continue;
        }
        TMO_SLL_Scan (&(MFWD_OWNER (u2OwnerId)->OwnerIfList), pSllNode,
                      tTMO_SLL_NODE *)
        {
            pIfNode = MFWD_GET_BASE_PTR (tMfwdInterfaceNode,
                                         OwnerIfLink, pSllNode);
            if (i4IpCmnMRouteInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
            {
                if (IpGetIfConfigRecord ((UINT2) pIfNode->u4IfIndex, &IpInfo)
                    != IP_SUCCESS)
                {
                    MFWD_UNLOCK ();
                    return SNMP_FAILURE;
                }
                u4IfIndex = IpInfo.InterfaceId.u4IfIndex;
                if ((u4IfIndex > (UINT4) i4IpCmnMRouteInterfaceIfIndex) &&
                    (u4IfIndex <= u4NextIndex))
                {
                    u1FindNext = 1;
                    u4NextIndex = u4IfIndex;
                    u1NextAddrType = pIfNode->u1AddrType;
                }

            }
        }                        /* End of TMO_SLL_Scan */
    }                            /* End of For Loop */

    if ((pIfNode != NULL) && (u1FindNext))
    {
        i1Status = SNMP_SUCCESS;
        *pi4NextIpCmnMRouteInterfaceIfIndex = u4NextIndex;
        *pi4NextIpCmnMRouteInterfaceAddrType = u1NextAddrType;
        MFWD_TRC_ARG2 (MFWD_TRC_FLAG, MFWD_MGMT_TRC,
                       MFWD_MOD_NAME,
                       "Next index, AddreType for interface table %d %d\n",
                       u4IfIndex, u1NextAddrType);
    }
    MFWD_UNLOCK ();

    return i1Status;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIpCmnMRouteInterfaceOwnerId
 Input       :  The Indices
                IpCmnMRouteInterfaceIfIndex
                IpCmnMRouteInterfaceAddrType

                The Object 
                retValIpCmnMRouteInterfaceOwnerId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpCmnMRouteInterfaceOwnerId (INT4 i4IpCmnMRouteInterfaceIfIndex,
                                   INT4 i4IpCmnMRouteInterfaceAddrType,
                                   INT4 *pi4RetValIpCmnMRouteInterfaceOwnerId)
{

    tMfwdInterfaceNode *pIfNode = NULL;
    UINT4               u4IfIndex = 0;
    UINT2               u2Port;

    if (i4IpCmnMRouteInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        MFWD_GET_PORT_FROM_IF_INDEX ((UINT2) i4IpCmnMRouteInterfaceIfIndex,
                                     &u2Port);
        u4IfIndex = (UINT4) u2Port;
    }

    pIfNode = MFWD_GET_IF_NODE (u4IfIndex, i4IpCmnMRouteInterfaceAddrType);
    if (pIfNode != NULL)
    {
        MFWD_LOCK ();
        /* Alter this line when TTL constraint is implemented
         */
        *pi4RetValIpCmnMRouteInterfaceOwnerId = 0;
        MFWD_TRC_ARG2 (MFWD_TRC_FLAG, MFWD_MGMT_TRC,
                       MFWD_MOD_NAME,
                       "The OwnerId value of the interface %d is %d\n",
                       u4IfIndex, *pi4RetValIpCmnMRouteInterfaceOwnerId);
        MFWD_UNLOCK ();
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIpCmnMRouteInterfaceTtl
 Input       :  The Indices
                IpCmnMRouteInterfaceIfIndex
                IpCmnMRouteInterfaceAddrType

                The Object 
                retValIpCmnMRouteInterfaceTtl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpCmnMRouteInterfaceTtl (INT4 i4IpCmnMRouteInterfaceIfIndex,
                               INT4 i4IpCmnMRouteInterfaceAddrType,
                               INT4 *pi4RetValIpCmnMRouteInterfaceTtl)
{

    tMfwdInterfaceNode *pIfNode = NULL;
    UINT4               u4IfIndex = 0;
    UINT2               u2Port;

    if (i4IpCmnMRouteInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        MFWD_GET_PORT_FROM_IF_INDEX ((UINT2) i4IpCmnMRouteInterfaceIfIndex,
                                     &u2Port);
        u4IfIndex = (UINT4) u2Port;
    }

    pIfNode = MFWD_GET_IF_NODE (u4IfIndex, i4IpCmnMRouteInterfaceAddrType);

    if (pIfNode != NULL)
    {
        MFWD_LOCK ();
        /* Alter this line when TTL constraint is implemented
         */
        *pi4RetValIpCmnMRouteInterfaceTtl = 0;
        MFWD_TRC_ARG2 (MFWD_TRC_FLAG, MFWD_MGMT_TRC,
                       MFWD_MOD_NAME,
                       "The TTL value of the interface %d is %d\n",
                       u4IfIndex, *pi4RetValIpCmnMRouteInterfaceTtl);

        MFWD_UNLOCK ();
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIpCmnMRouteInterfaceProtocol
 Input       :  The Indices
                IpCmnMRouteInterfaceIfIndex
                IpCmnMRouteInterfaceAddrType

                The Object 
                retValIpCmnMRouteInterfaceProtocol
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpCmnMRouteInterfaceProtocol (INT4 i4IpCmnMRouteInterfaceIfIndex,
                                    INT4 i4IpCmnMRouteInterfaceAddrType,
                                    INT4 *pi4RetValIpCmnMRouteInterfaceProtocol)
{

    tMfwdInterfaceNode *pIfNode = NULL;
    UINT4               u4IfIndex = 0;
    UINT2               u2Port;

    if (i4IpCmnMRouteInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        MFWD_GET_PORT_FROM_IF_INDEX ((UINT2) i4IpCmnMRouteInterfaceIfIndex,
                                     &u2Port);
        u4IfIndex = (UINT4) u2Port;
    }

    pIfNode = MFWD_GET_IF_NODE (u4IfIndex, i4IpCmnMRouteInterfaceAddrType);

    if (pIfNode != NULL)
    {
        MFWD_LOCK ();

        *pi4RetValIpCmnMRouteInterfaceProtocol =
            MFWD_OWNER (pIfNode->u2OwnerId)->u1ProtoId;

        MFWD_TRC_ARG2 (MFWD_TRC_FLAG, MFWD_MGMT_TRC,
                       MFWD_MOD_NAME,
                       "The TTL value of the interface %d is %d\n",
                       u4IfIndex, *pi4RetValIpCmnMRouteInterfaceProtocol);

        MFWD_UNLOCK ();
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIpCmnMRouteInterfaceRateLimit
 Input       :  The Indices
                IpCmnMRouteInterfaceIfIndex
                IpCmnMRouteInterfaceAddrType

                The Object 
                retValIpCmnMRouteInterfaceRateLimit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpCmnMRouteInterfaceRateLimit (INT4 i4IpCmnMRouteInterfaceIfIndex,
                                     INT4 i4IpCmnMRouteInterfaceAddrType,
                                     INT4
                                     *pi4RetValIpCmnMRouteInterfaceRateLimit)
{

    tMfwdInterfaceNode *pIfNode = NULL;
    UINT4               u4IfIndex = 0;
    UINT2               u2Port;

    if (i4IpCmnMRouteInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        MFWD_GET_PORT_FROM_IF_INDEX ((UINT2) i4IpCmnMRouteInterfaceIfIndex,
                                     &u2Port);
        u4IfIndex = (UINT4) u2Port;
    }

    pIfNode = MFWD_GET_IF_NODE (u4IfIndex, i4IpCmnMRouteInterfaceAddrType);
    if (pIfNode != NULL)
    {
        MFWD_LOCK ();

        *pi4RetValIpCmnMRouteInterfaceRateLimit = pIfNode->u4IfRtLmt;

        MFWD_TRC_ARG2 (MFWD_TRC_FLAG, MFWD_MGMT_TRC,
                       MFWD_MOD_NAME,
                       "The Rate Limit value of the interface %d is %d\n",
                       u4IfIndex, *pi4RetValIpCmnMRouteInterfaceRateLimit);

        MFWD_UNLOCK ();
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIpCmnMRouteInterfaceInMcastOctets
 Input       :  The Indices
                IpCmnMRouteInterfaceIfIndex
                IpCmnMRouteInterfaceAddrType

                The Object 
                retValIpCmnMRouteInterfaceInMcastOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpCmnMRouteInterfaceInMcastOctets (INT4 i4IpCmnMRouteInterfaceIfIndex,
                                         INT4 i4IpCmnMRouteInterfaceAddrType,
                                         UINT4
                                         *pu4RetValIpCmnMRouteInterfaceInMcastOctets)
{

    tMfwdInterfaceNode *pIfNode = NULL;
    UINT4               u4IfIndex = 0;
    UINT2               u2Port;

    if (i4IpCmnMRouteInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        MFWD_GET_PORT_FROM_IF_INDEX ((UINT2) i4IpCmnMRouteInterfaceIfIndex,
                                     &u2Port);
        u4IfIndex = (UINT4) u2Port;
    }

    pIfNode = MFWD_GET_IF_NODE (u4IfIndex, i4IpCmnMRouteInterfaceAddrType);
    if (pIfNode != NULL)
    {
        MFWD_LOCK ();

        *pu4RetValIpCmnMRouteInterfaceInMcastOctets = pIfNode->u4InMdpOct;

        MFWD_TRC_ARG2 (MFWD_TRC_FLAG, MFWD_MGMT_TRC,
                       MFWD_MOD_NAME,
                       "The Number of MDP Octets received on IF %d is %d\n",
                       u4IfIndex, pIfNode->u4InMdpOct);

        MFWD_UNLOCK ();
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIpCmnMRouteInterfaceCmdbPktCnt
 Input       :  The Indices
                IpCmnMRouteInterfaceIfIndex
                IpCmnMRouteInterfaceAddrType

                The Object 
                retValIpCmnMRouteInterfaceCmdbPktCnt
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpCmnMRouteInterfaceCmdbPktCnt (INT4 i4IpCmnMRouteInterfaceIfIndex,
                                      INT4 i4IpCmnMRouteInterfaceAddrType,
                                      UINT4
                                      *pu4RetValIpCmnMRouteInterfaceCmdbPktCnt)
{

    tMfwdInterfaceNode *pIfNode = NULL;
    UINT4               u4IfIndex = 0;
    UINT2               u2Port;

    if (i4IpCmnMRouteInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        MFWD_GET_PORT_FROM_IF_INDEX ((UINT2) i4IpCmnMRouteInterfaceIfIndex,
                                     &u2Port);
        u4IfIndex = (UINT4) u2Port;
    }

    pIfNode = MFWD_GET_IF_NODE (u4IfIndex, i4IpCmnMRouteInterfaceAddrType);
    if (pIfNode != NULL)
    {
        MFWD_LOCK ();

        *pu4RetValIpCmnMRouteInterfaceCmdbPktCnt = pIfNode->u4CmdpCount;

        MFWD_TRC_ARG2 (MFWD_TRC_FLAG, MFWD_MGMT_TRC,
                       MFWD_MOD_NAME,
                       "Number of Number of packets in CMDB %d is %d\n",
                       u4IfIndex, *pu4RetValIpCmnMRouteInterfaceCmdbPktCnt);

        MFWD_UNLOCK ();
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIpCmnMRouteInterfaceOutMcastOctets
 Input       :  The Indices
                IpCmnMRouteInterfaceIfIndex
                IpCmnMRouteInterfaceAddrType

                The Object 
                retValIpCmnMRouteInterfaceOutMcastOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpCmnMRouteInterfaceOutMcastOctets (INT4 i4IpCmnMRouteInterfaceIfIndex,
                                          INT4 i4IpCmnMRouteInterfaceAddrType,
                                          UINT4
                                          *pu4RetValIpCmnMRouteInterfaceOutMcastOctets)
{

    INT1                i1Status = SNMP_FAILURE;
    tMfwdInterfaceNode *pIfNode = NULL;
    UINT4               u4IfIndex = 0;
    UINT2               u2Port;

    if (i4IpCmnMRouteInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        MFWD_GET_PORT_FROM_IF_INDEX ((UINT2) i4IpCmnMRouteInterfaceIfIndex,
                                     &u2Port);
        u4IfIndex = (UINT4) u2Port;
    }

    pIfNode = MFWD_GET_IF_NODE (u4IfIndex, i4IpCmnMRouteInterfaceAddrType);

    if (pIfNode != NULL)
    {
        MFWD_LOCK ();
        i1Status = SNMP_SUCCESS;

        *pu4RetValIpCmnMRouteInterfaceOutMcastOctets = pIfNode->u4OutMdpOct;

        MFWD_TRC_ARG2 (MFWD_TRC_FLAG, MFWD_MGMT_TRC,
                       MFWD_MOD_NAME,
                       "The Number of MDP Octets Sent on IF %d is %d\n",
                       u4IfIndex, pIfNode->u4OutMdpOct);

        MFWD_UNLOCK ();
    }
    return i1Status;

}
