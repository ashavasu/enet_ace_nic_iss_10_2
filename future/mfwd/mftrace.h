#ifndef MFWD_TRACE
#define MFWD_TRACE_


/* Trace and debug flags */
#define  MFWD_TRC_FLAG  gMfwdContext.u4GlobalTrace



/* Trace definitions */

#define  MFWD_MOD_NAME          MfwdGetModule(u2MfwdTrcModule)
#define  MFWD_TRC(flg, mod, modname, fmt) \
             MOD_TRC(flg, u2MfwdTrcModule, modname, fmt)
#define  MFWD_TRC_ARG1(flg, mod, modname, fmt, arg1) \
             MOD_TRC_ARG1(flg, u2MfwdTrcModule, modname, fmt, arg1)
#define  MFWD_TRC_ARG2(flg, mod, modname, fmt ,arg1 ,arg2) \
             MOD_TRC_ARG2(flg, u2MfwdTrcModule, modname, fmt, arg1, arg2)
#define  MFWD_TRC_ARG3(flg, mod, modname, fmt ,arg1 ,arg2, arg3) \
             MOD_TRC_ARG3(flg, u2MfwdTrcModule, modname, fmt, arg1, arg2, arg3)
#define  MFWD_TRC_ARG4(flg, mod, modname, fmt ,arg1 ,arg2, arg3, arg4) \
             MOD_TRC_ARG4(flg, u2MfwdTrcModule, modname, fmt, arg1, arg2, arg3, arg4)
#define  MFWD_TRC_ARG5(flg, mod, modname, fmt ,arg1 ,arg2, arg3, arg4, arg5) \
             MOD_TRC_ARG5(flg, u2MfwdTrcModule, modname, fmt, arg1, arg2, arg3, arg4, arg5)
#define  MFWD_TRC_ARG6(flg, mod, modname, fmt ,arg1 ,arg2, arg3, arg4, arg5, arg6) \
             MOD_TRC_ARG6(flg, u2MfwdTrcModule, modname, fmt, arg1, arg2, arg3, arg4, arg5, arg6)


#define  MFWD_INIT_SHUT_TRC     INIT_SHUT_TRC     
#define  MFWD_MGMT_TRC          MGMT_TRC          
#define  MFWD_DATA_PATH_TRC     DATA_PATH_TRC     
#define  MFWD_CONTROL_PATH_TRC  CONTROL_PLANE_TRC 
#define  MFWD_DUMP_TRC          DUMP_TRC          
#define  MFWD_OS_RESOURCE_TRC   OS_RESOURCE_TRC   
#define  MFWD_ALL_FAILURE_TRC   ALL_FAILURE_TRC   
#define  MFWD_BUFFER_TRC        BUFFER_TRC        

/* This trace value is not provided by LR and should be taken care if 
 * the LR creates one with the trace value given below.
 */
#define  MFWD_TMR_EXP_TRC       0x00008000

#endif /* _TRACE_H_ */

/****************************************************************************/
/*           How to use the Trace Statement                                 */
/*                                                                          */
/* 1) Define a UINT4 Trace variable. (eg :u4xxx)                            */
/*                                                                          */
/* 2) The Trace statements can be used as                                   */
/*                                                                          */
/*           MOD_TRC(u4DbgVar, mask, module name, fmt)                      */
/*           MOD_TRC(u4xxx, INIT_SHUT_TRC, XXX, "Trace Statement")          */
/*                                                                          */
/****************************************************************************/
