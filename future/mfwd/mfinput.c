/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * 
 * $Id: mfinput.c,v 1.23 2014/08/23 11:58:43 siva Exp $
 * 
 ********************************************************************/
#include "mfinc.h"

#ifdef SNMP_2_WANTED
#include "snmctdfs.h"
#include "fsmfwdwr.h"
#endif

#include "mfwdcmwr.h"
/* Global Context Variable */
tMfwdContextStructure gMfwdContext;

#ifdef TRACE_WANTED
static UINT2        u2MfwdTrcModule = MFWD_IO_MODULE;
#endif

/****************************************************************************
 * Function Name    :  MfwdCreateTask
 *
 * Description      :  This function initiates the MFWD and creates the
 *                     necessary operating system resources required for
 *                     MFWD operation and spawns the RPH and MDH tasks
 * Input(s)         :  None
 *
 * Output(s)        :  None
 *
 * Global Variables
 * Referred         :  None
 *
 * Global Variables
 * Modified         :  None
 *
 * Return(s)        :  MFWD_FAILURE or MFWD_SUCCESS
 ****************************************************************************/
INT4
MfwdCreateTask (VOID)
{

    /* Allocate the memory for the context structure and initialise the
     * the data
     */
    MEMSET (&gMfwdContext, 0, sizeof (tMfwdContextStructure));

    gMfwdContext.u1MfwdStatus = MFWD_STATUS_ENABLED;
    gMfwdContext.u1CacheMissDBStat = MFWD_CMDB_STATUS_DISABLED;
    gMfwdContext.i4DataSockId = MFWD_BAD_MCAST_SOCKET;
    TMO_SLL_Init (&(gMfwdContext.MrtGetNextList));

    /* Register with IP for the multicast data packets receiving if this
     * registration fails 
     */

    return MFWD_SUCCESS;
}                                /* end of function mfwd create task */

/*************************************************************************
 * Function Name    :  MfwdInit
 *
 * Description      :  This function creates Qs and creates
 *                     necessary memory for the MFWD Startup. This includes
 *                     OwnerInformationTable, GlobalInterfaceTable 
 *                     initialisation, creating a memory pool for the 
 *                     Owner information nodes. 
 * Input(s)         :  None
 *
 * Output(s)        :  None
 *
 * Global Variables
 * Referred         :  gMfwdContext
 *
 * Global Variables
 * Modified         :  gMfwdContext
 *
 * Return(s)        :  MFWD_FAILURE or MFWD_SUCCESS
 ****************************************************************************/
INT4
MfwdInit (void)
{

    MFWD_LOAD_MAXMRP_AND_MAXIFACE_VALUES ();
    INT4                i4Status = MFWD_FAILURE;

    if (MfwdInitSocketInfo () == MFWD_FAILURE)
    {
        return MFWD_FAILURE;
    }
    if (OsixQueCrt (MFWD_CQ_NAME, OSIX_MAX_Q_MSG_LEN, MFWD_CQ_DEPTH,
                    &(MFWD_CQ_ID)) != OSIX_SUCCESS)
    {
        MFWD_DBG (MFWD_DBG_ERROR,
                  "Failure in intialising the MRP->MFWD Queue\n");
        return MFWD_FAILURE;
    }
    gMfwdContext.u4QProcessCnt = MFWD_DEF_QPROCESS_CNT;

    if (OsixQueCrt (MFWD_Q_NAME, OSIX_MAX_Q_MSG_LEN, MAX_MFWD_Q_DEPTH,
                    &(MFWD_Q_ID)) != OSIX_SUCCESS)
    {
        MFWD_DBG (MFWD_DBG_ERROR,
                  "Failure in intialising the MRP->MFWD Queue\n");
        return MFWD_FAILURE;
    }

    /* Initialise the global tables the owner information tables and the
     * global interface tables
     */
    if (MfwdInitInterfaceInfo () == MFWD_FAILURE)
    {
        return MFWD_FAILURE;
    }

    if (OsixCreateSem ((const UINT1 *) MFWD_MUTEX_SEMA4, 1, OSIX_GLOBAL,
                       &(MFWD_MUTEX_SEMID)) != OSIX_SUCCESS)
    {
        MFWD_TRC (MFWD_TRC_FLAG, MFWD_OS_RESOURCE_TRC, MFWD_MOD_NAME,
                  "Failure in creating the semaphore for MFWD\n");
        return MFWD_FAILURE;
    }                            /* end of if to see if semaphore is created */

    /* Create Mempools required by the module */
    if (MfwdSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        MFWD_DBG (MFWD_DBG_ERROR, "Error Allocating the memory for"
                  " the Owner Information Table\n");
        MfwdSizingMemDeleteMemPools ();
        return MFWD_FAILURE;
    }

    /* Mempool identifier Variable assignment */
    MFWD_OWNER_INFO_TABLE_POOL_ID =
        MFWDMemPoolIds[MAX_MFWD_OWNER_INFO_TABLE_SIZING_ID];
    MFWD_OWNER_NODE_POOL_ID = MFWDMemPoolIds[MAX_MFWD_OWNER_NODE_SIZING_ID];
    CACHE_MISS_DB_NODE_POOLID =
        MFWDMemPoolIds[MAX_MFWD_CACHE_MISS_DB_NODES_SIZING_ID];
    MFWD_MSGQ_POOLID = MFWDMemPoolIds[MAX_MFWD_Q_DEPTH_SIZING_ID];
    CACHE_MISS_DATA_PKTS_POOLID =
        MFWDMemPoolIds[MAX_MFWD_CMD_DATA_PKT_SIZING_ID];
    MFWD_IF_POOL_ID = MFWDMemPoolIds[MAX_MFWD_INTERFACES_NODE_SIZING_ID];
    MFWD_GRP_NODE_PID = MFWDMemPoolIds[MAX_MFWD_GRP_NODE_SIZING_ID];
    MFWD_SRC_NODE_PID = MFWDMemPoolIds[MAX_MFWD_SRC_NODE_SIZING_ID];
    MFWD_RT_NODE_PID = MFWDMemPoolIds[MAX_MFWD_OWNER_RTENTRIES_SIZING_ID];
    MFWD_OIF_NODE_PID = MFWDMemPoolIds[MAX_MFWD_OWNER_OIFS_SIZING_ID];
    MFWD_IIF_NODE_PID = MFWDMemPoolIds[MAX_MFWD_OWNER_IIFS_SIZING_ID];
    MFWD_NEXTHOP_NODE_PID =
        MFWDMemPoolIds[MAX_MFWD_OWNER_OIF_NEXTHOP_SIZING_ID];

    /* allocate memory for the owner information table pointer array */
    MFWD_MEMPOOL_ALLOC (MFWD_OWNER_INFO_TABLE_POOL_ID, tMfwdOwnerInfoNode **,
                        gMfwdContext.pOwnerInfoTbl, i4Status);

    if (i4Status == MFWD_FAILURE)
    {
        MFWD_DBG (MFWD_DBG_ERROR, "Error Allocating the memory for"
                  " the Owner Information Table\n");
        return MFWD_FAILURE;
    }
    MFWD_OWNER_INFO_TABLE_POOL_BLOCK_SIZE = sizeof (tMfwdOwnerInfoTable);
    MFWD_MEMSET (gMfwdContext.pOwnerInfoTbl, 0, sizeof (tMfwdOwnerInfoTable));
    MFWD_OWNER_NODE_POOL_BLOCK_SIZE = sizeof (tMfwdOwnerInfoNode);
    CACHE_MISS_DATA_PKTS_POOL_BLOCK_SIZE = sizeof (tCMDataPktNode);
    CACHE_MISS_DB_POOL_BLOCK_SIZE = sizeof (tCacheMissDBNode);
    MFWD_IF_POOL_BLOCK_SIZE = sizeof (tMfwdInterfaceNode);
    MFWD_GRP_NODE_POOL_BLOCK_SIZE = sizeof (tMfwdGrpNode);
    MFWD_SRC_NODE_POOL_BLOCK_SIZE = sizeof (tMfwdSrcNode);
    MFWD_RT_NODE_POOL_BLOCK_SIZE = sizeof (tMfwdRtEntry);
    MFWD_OIF_NODE_POOL_BLOCK_SIZE = sizeof (tMfwdOifNode);
    MFWD_IIF_NODE_POOL_BLOCK_SIZE = sizeof (tMfwdIifNode);
    MFWD_NEXTHOP_NODE_POOL_BLOCK_SIZE = sizeof (tMfwdOifNextHopNode);
    MFWD_MSGQ_POOL_BLOCK_SIZE = sizeof (tMfwdQData);

    /* Create the timer list for the CMDB and start a timer on the buffer */
    if (TmrCreateTimerList (MFWD_TASK_NAME, MFWD_CMDB_TMR_EXP_EVENT,
                            NULL, &(MFWD_CMDB_TIMER_LIST_ID)) == TMR_FAILURE)
    {
        MFWD_DBG (MFWD_DBG_ERROR,
                  "Error Creating the TIMER list for the CMDB\n");
        return MFWD_FAILURE;
    }
    return MFWD_SUCCESS;
}                                /* end of function mfwd init */

/*************************************************************************
 * Function Name    :  MfwdInitInterfaceInfo
 *
 * Description      :  This function initialises the interface table by 
 *                     calculating the size of the linear table and the 
 *                     hash table creates the hash table and allocates 
 *                     memory for the linear interface table 
 * Input(s)         :  None
 *
 * Output(s)        :  None
 *
 * Global Variables
 * Referred         :  gMfwdContext
 *
 * Global Variables
 * Modified         :  gMfwdContext
 *
 * Return(s)        :  MFWD_FAILURE or MFWD_SUCCESS
 ****************************************************************************/

INT4
MfwdInitInterfaceInfo ()
{

    MFWD_IF_HASH_KEY = MFWD_MAX_IF_HASH_TABLE_SIZE;

    MFWD_IF_HASH_TABLE = TMO_HASH_Create_Table (MFWD_IF_HASH_KEY, NULL, FALSE);

    if (MFWD_IF_HASH_TABLE == NULL)
    {
        MFWD_DBG (MFWD_DBG_ERROR, "Error creating the hash table for "
                  "the Global Interface Table\n");
        return MFWD_FAILURE;
    }

    /* We maintain a single semaphore for the whole hash table create one
     * the name of the semaphore will be made out of the defenition used
     * below
     */
    return MFWD_SUCCESS;
}

/*************************************************************************
 * Function Name    :  MfwdShutdown
 *
 * Description      :  This function clears all the memory pools previously 
 *                     created during the normal action and deregisters the 
 *                     MRPs. 
 * Input(s)         :  None
 *
 * Output(s)        :  None
 *
 * Global Variables
 * Referred         :  gMfwdContext
 *
 * Global Variables
 * Modified         :  gMfwdContext
 *
 * Return(s)        :  MFWD_FAILURE or MFWD_SUCCESS
 ****************************************************************************/

INT4
MfwdShutdown (void)
{
    UINT2               u2OwnerId;

    /* Close the socket if it was created and free the memory used to forward
     * multicast data packets
     */
    if (gMfwdContext.i4DataSockId < 0)
    {
        close (gMfwdContext.i4DataSockId);
    }

    /* Deregister all the MRPs so that the memory pools acquired by them is 
     * deleted
     */
    if (gMfwdContext.pOwnerInfoTbl != NULL)
    {
        for (u2OwnerId = 0; u2OwnerId < MAX_MRP_INSTANCES; u2OwnerId++)
        {
            if (MFWD_OWNER (u2OwnerId) != NULL)
            {
                MfwdInputHandleDeRegistration (u2OwnerId);
            }
        }
        if (MFWD_OWNER_INFO_TABLE_POOL_BLOCK_SIZE != 0)
        {
            MemDeleteMemPool (MFWD_OWNER_INFO_TABLE_POOL_ID);
        }
    }
    MfwdOimDeActivateOwnerMemPool ();
    OsixQueDel (MFWD_Q_ID);
    OsixQueDel (MFWD_CQ_ID);

    /* release the interface table (both linear and hash tables) 
     * The Memory pool of the interface nodes is not released 
     * here because it is allocate during the owner registration process
     */

    if (MFWD_IF_HASH_TABLE != NULL)
    {
        TMO_HASH_Delete_Table (MFWD_IF_HASH_TABLE, NULL);
    }

    /* Release the owner information nodes to the memory pool */
    if (MFWD_OWNER_NODE_POOL_BLOCK_SIZE != 0)
    {
        MemDeleteMemPool (MFWD_OWNER_NODE_POOL_ID);
    }

    if (CACHE_MISS_DB_POOL_BLOCK_SIZE != 0)
    {
        MemDeleteMemPool (CACHE_MISS_DB_NODE_POOLID);
    }

    OsixDeleteSem (MFWD_TASK_NODE, (const UINT1 *) MFWD_MUTEX_SEMA4);

    if (NULL != MFWD_CMDB_TIMER_LIST_ID)
    {
        if (TmrDeleteTimerList (MFWD_CMDB_TIMER_LIST_ID) != TMR_SUCCESS)
        {
            MFWD_DBG (MFWD_DBG_ERROR,
                      "Error deleting the Timer list of the CMDB nodes\n");
        }
    }
    /* Release the context of the MFWD module */
    return MFWD_SUCCESS;
}                                /* end of function Mfwd shut down */

/*************************************************************************
 * Function Name    :  MfwdInputHandleMfwdStatusChg
 *
 * Description      :  This function deletes all the memory pools acquired
 *                     by the MRPs if the MFWD should be disabled and activates
 *                     owners if the MFWD should be enabled
 *             
 * Input(s)         :  None
 *
 * Output(s)        :  None
 *
 * Global Variables
 * Referred         :  None         
 *
 * Global Variables
 * Modified         :  None         
 *
 * Return(s)        :  MFWD_FAILURE or MFWD_SUCCESS
 ****************************************************************************/

VOID
MfwdInputHandleMfwdStatusChg ()
{
    UINT2               u2OwnerId = 0;
    INT4                i4RetCode = MFWD_SUCCESS;

    MFWD_DBG (MFWD_DBG_ENTRY, "Entered : Handling the MFWD status change\n");

    if (gMfwdContext.u1MfwdStatus == MFWD_STATUS_ENABLED)
    {
        MFWD_TRC (MFWD_TRC_FLAG, MFWD_CONTROL_PATH_TRC, MFWD_MOD_NAME,
                  "Mfwd status disabled, trying to DISABLE MFWD\n");

        /* MFWD status is now changed to disabled, so deactivate all the
         * owners
         */
        gMfwdContext.u1MfwdStatus = MFWD_STATUS_DISABLED;

        /* Deactivate all the owners that are regitered */
        for (u2OwnerId = 0; u2OwnerId < MAX_MRP_INSTANCES; u2OwnerId++)
        {
            if (MFWD_OWNER (u2OwnerId) != NULL)
            {
                i4RetCode = MfwdOimDeActivateOwner (u2OwnerId);
                (*((MFWD_OWNER (u2OwnerId))->MrpInformMfwdStatus))
                    (MFWD_TO_MRP_STATUS_DISABLED_EVENT);

            }

            if (i4RetCode != MFWD_SUCCESS)
            {
                MFWD_TRC_ARG1 (MFWD_TRC_FLAG, MFWD_ALL_FAILURE_TRC,
                               MFWD_MOD_NAME,
                               "Failure in deactivating the owner %d\n",
                               u2OwnerId);
            }
        }                        /* end of for loop to deactivate the owners */

        TMO_SLL_Init (&(gMfwdContext.MrtGetNextList));

        if (i4RetCode == MFWD_FAILURE)
        {
            MFWD_TRC (MFWD_TRC_FLAG, MFWD_ALL_FAILURE_TRC, MFWD_MOD_NAME,
                      "FAILURE in trying to DISABLE MFWD, All the owners could"
                      "not be deactivated\n");
        }
        else
        {
            MFWD_TRC (MFWD_TRC_FLAG, MFWD_CONTROL_PATH_TRC, MFWD_MOD_NAME,
                      "MFWD is DISABLED now\n");
        }

    }                            /* end of if (gMfwdContext.u1MfwdStatus == MFWD_STATUS_ENABLED) */
    else
    {
        MFWD_TRC (MFWD_TRC_FLAG, MFWD_CONTROL_PATH_TRC, MFWD_MOD_NAME,
                  "Mfwd status enabled, trying to ENABLE MFWD\n");

        gMfwdContext.u1MfwdStatus = MFWD_STATUS_ENABLED;

        /* Now activate all the owners that were registered previously */
        for (u2OwnerId = 0; u2OwnerId < MAX_MRP_INSTANCES; u2OwnerId++)
        {
            if (MFWD_OWNER (u2OwnerId) != NULL)
            {

                i4RetCode = MfwdOimActivateOwner (u2OwnerId);
                if (i4RetCode == MFWD_FAILURE)
                {
                    MFWD_TRC_ARG1 (MFWD_TRC_FLAG, MFWD_ALL_FAILURE_TRC,
                                   MFWD_MOD_NAME,
                                   "Failure in activating the owner %d\n",
                                   u2OwnerId);
                }
                else
                {
                    MFWD_TRC_ARG1 (MFWD_TRC_FLAG, MFWD_CONTROL_PATH_TRC,
                                   MFWD_MOD_NAME,
                                   "owner %d is now active\n", u2OwnerId);
                    (*((MFWD_OWNER (u2OwnerId))->MrpInformMfwdStatus))
                        (MFWD_TO_MRP_STATUS_ENABLED_EVENT);

                }
            }
        }

        if (i4RetCode == MFWD_FAILURE)
        {
            MFWD_TRC (MFWD_TRC_FLAG, MFWD_ALL_FAILURE_TRC, MFWD_MOD_NAME,
                      "FAILURE in trying to ENABLE MFWD, All the owners could "
                      "not be activated\n");
        }
        else
        {
            MFWD_TRC (MFWD_TRC_FLAG, MFWD_CONTROL_PATH_TRC, MFWD_MOD_NAME,
                      "MFWD is Successfully REENABLED\n");
        }
    }                            /* End of else for check if MFWD status is enabled */

    MFWD_DBG (MFWD_DBG_EXIT, "Exiting : Handling the MFWD status change \n");
}                                /* End of the function handle MFWD status change */

/*************************************************************************
 * Function Name    :  MfwdTaskMain
 *
 * Description      :  This function waits forever on the events and
 *                     calls appropriate functiosn for processing the 
 *                     routing protocol updates and snmp events and timer 
 *                     expiry events.  
 *             
 * Input(s)         :  None
 *
 * Output(s)        :  None
 *
 * Global Variables
 * Referred         :  gMfwdContext
 *
 * Global Variables
 * Modified         :  None         
 *
 * Return(s)        :  MFWD_FAILURE or MFWD_SUCCESS
 ****************************************************************************/
VOID
MfwdTaskMain (INT1 *pi1TaskParam)
{
    INT4                i4Status;
    UINT4               u4Event;
    UINT2               u2OwnerId;
    UINT4               u4MfwdEvents;
    UINT4               u4MfwdPktCnt = 0;
    tTmrAppTimer       *pExpTmr;
    tMsgHandleFunPtr    pMsgHdlrFunPtr[MFWD_MAX_MSGHDLR_FUNPTR];
    tMfwdQData         *pMfwdQData;

    UNUSED_PARAM (*pi1TaskParam);

    MfwdCreateTask ();
    if (OsixTskIdSelf (&MFWD_TASK_ID) == OSIX_FAILURE)
    {
        /* Indicate the status of initialization to the main routine */
        MFWD_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    /* Register with IP for the multicast data packets receiving if this
     * registration fails 
     */
    gMfwdContext.u4IpRegnId = REGISTER_MFWD_WITH_IP (MfwdHandleMcastDataPkt);
    if (gMfwdContext.u4IpRegnId == (UINT4) IP_REG_FAILURE)
    {
        MFWD_TRC (MFWD_TRC_FLAG, MFWD_CONTROL_PATH_TRC, MFWD_MOD_NAME,
                  "Unable to Register with IP for mcast data pkts \n");
        MfwdShutdown ();
        /* Indicate the status of initialization to the main routine */
        MFWD_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }                            /* End of if  == IP_REG_FAILURE */

#ifdef IP6_WANTED
    gMfwdContext.u4IpRegnId =
        REGISTER_MFWD_WITH_IPV6 (MfwdHandleV6McastDataPkt);
    if (gMfwdContext.u4IpRegnId == (UINT4) IPV6_REG_FAILURE)
    {
        MFWD_TRC (MFWD_TRC_FLAG, MFWD_CONTROL_PATH_TRC, MFWD_MOD_NAME,
                  "Unable to Register with IPv6 for mcast data pkts \n");
        MfwdShutdown ();
        /* Indicate the status of initialization to the main routine */
        MFWD_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }                            /* End of if  == IP_REG_FAILURE */
#endif

    if (MfwdInit () == MFWD_FAILURE)
    {
        MFWD_DBG (MFWD_DBG_ERROR, "Error Initializing MFWD, Shutting Down\n");
#ifdef VX_IP_WANTED
        gu1TaskStatus = FAILURE;
#endif
        MfwdShutdown ();
        /* Indicate the status of initialization to the main routine */
        MFWD_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    u4MfwdEvents = (MFWD_MSGQ_EVENT | MFWD_STATUS_DISABLE_EVENT |
                    MFWD_STATUS_ENABLE_EVENT | MFWD_CMDB_ENABLE_EVENT |
                    MFWD_CMDB_DISABLE_EVENT | MFWD_CMDB_TMR_EXP_EVENT);

    pMsgHdlrFunPtr[0] = NULL;
    pMsgHdlrFunPtr[1] = (tMsgHandleFunPtr) MfwdInputHandleMdpMesg;
    pMsgHdlrFunPtr[2] = (tMsgHandleFunPtr) MfwdInputHandleMrpMesg;
    pMsgHdlrFunPtr[3] = (tMsgHandleFunPtr) MfwdInputHandleMdp6Mesg;

    /* Indicate the status of initialization to the main routine */
    MFWD_INIT_COMPLETE (OSIX_SUCCESS);
#ifdef SNMP_2_WANTED
    /* Register the MIB with SNMP */
    RegisterFSMFWD ();
    RegisterMFWDCM ();
#endif

    while (1)
    {
        /* wait until an event is received from the extern interface */
        if (OsixEvtRecv (MFWD_TASK_ID, u4MfwdEvents, MFWD_EVENT_WAIT_FLAGS,
                         &u4Event) == OSIX_FAILURE)
        {
            continue;
        }

        MFWD_LOCK ();

        if (u4Event & MFWD_MSGQ_EVENT)
        {
            while (OsixQueRecv
                   (MFWD_Q_ID, (UINT1 *) (&pMfwdQData), OSIX_DEF_MSG_LEN,
                    MFWD_Q_WAIT_MODE) == OSIX_SUCCESS)
            {
                if (gMfwdContext.u1MfwdStatus != MFWD_STATUS_DISABLED)
                {
                    u4MfwdPktCnt++;
                    pMsgHdlrFunPtr[pMfwdQData->u1MsgId] (pMfwdQData->pMsg);
                    MFWD_MEMRELEASE (MFWD_MSGQ_POOLID, pMfwdQData, i4Status);

                    if (u4MfwdPktCnt >= gMfwdContext.u4QProcessCnt)
                    {
                        u4MfwdPktCnt = 0;
                        break;
                    }
                }
                else
                {
                    u4MfwdPktCnt = 0;
                    CRU_BUF_Release_MsgBufChain (pMfwdQData->pMsg, FALSE);
                    MFWD_MEMRELEASE (MFWD_MSGQ_POOLID, pMfwdQData, i4Status);
                }
            }

            while (OsixQueRecv
                   (MFWD_CQ_ID, (UINT1 *) (&pMfwdQData), OSIX_DEF_MSG_LEN,
                    MFWD_Q_WAIT_MODE) == OSIX_SUCCESS)
            {
                if (gMfwdContext.u1MfwdStatus != MFWD_STATUS_DISABLED)
                {
                    if (pMfwdQData->u1MsgId == MFWD_REG_MESSAGE)
                    {
                        MfwdInputRegisterMRP (pMfwdQData->pMsg);
                    }
                    else if (pMfwdQData->u1MsgId == MFWD_DEREG_MESSAGE)
                    {
                        MfwdInputDeRegisterMRP (pMfwdQData->pMsg);
                    }
                    else
                    {
                        if (pMfwdQData->u1MsgId < MFWD_MAX_MSGHDLR_FUNPTR)
                        {
                            pMsgHdlrFunPtr[pMfwdQData->u1MsgId] (pMfwdQData->
                                                                 pMsg);
                        }
                    }
                }
                else
                {
                    CRU_BUF_Release_MsgBufChain (pMfwdQData->pMsg, FALSE);
                }
                MFWD_MEMRELEASE (MFWD_MSGQ_POOLID, pMfwdQData, i4Status);
            }                    /* end of while osix receive from Q ............ */
        }                        /* end of if event is MFWD_IP_MDH_ARRIVAL_EVENT */

        if (u4Event & MFWD_STATUS_DISABLE_EVENT)
        {
            if (gMfwdContext.u1MfwdStatus == MFWD_STATUS_ENABLED)
            {
                MFWD_TRC (MFWD_TRC_FLAG, MFWD_CONTROL_PATH_TRC,
                          MFWD_MOD_NAME, "Disabling the MFWD\n");
                MfwdInputHandleMfwdStatusChg ();
                DEREGISTER_MFWD_FROM_IP (gMfwdContext.u4IpRegnId);
#ifdef IP6_WANTED
                DEREGISTER_MFWD_FROM_IPV6 (gMfwdContext.u4Ipv6RegnId);
#endif

            }
        }

        if (u4Event & MFWD_STATUS_ENABLE_EVENT)
        {
            if (gMfwdContext.u1MfwdStatus == MFWD_STATUS_DISABLED)
            {
                MfwdInputHandleMfwdStatusChg ();
                gMfwdContext.u4IpRegnId =
                    REGISTER_MFWD_WITH_IP (MfwdHandleMcastDataPkt);
                if (gMfwdContext.u4IpRegnId == (UINT4) IP_REG_FAILURE)
                {
                    MFWD_TRC (MFWD_TRC_FLAG, MFWD_ALL_FAILURE_TRC,
                              MFWD_MOD_NAME,
                              "Unable to Register with IP for MDPs after reenable\n");
                }

#ifdef IP6_WANTED
                gMfwdContext.u4Ipv6RegnId =
                    REGISTER_MFWD_WITH_IPV6 (MfwdHandleV6McastDataPkt);
                if (gMfwdContext.u4Ipv6RegnId == (UINT4) IPV6_REG_FAILURE)
                {
                    MFWD_TRC (MFWD_TRC_FLAG, MFWD_ALL_FAILURE_TRC,
                              MFWD_MOD_NAME,
                              "Unable to Register with IPv6 for MDPs after reenable\n");
                }
#endif

            }
        }

        if ((gMfwdContext.u1MfwdStatus == MFWD_STATUS_ENABLED) &&
            (u4Event & MFWD_CMDB_DISABLE_EVENT))
        {
            if (gMfwdContext.u1CacheMissDBStat != MFWD_STATUS_DISABLED)
            {
                MfwdMdhDisableCmdb ();
            }
        }                        /* end of if CMDB Status disable event */

        if ((gMfwdContext.u1MfwdStatus != MFWD_STATUS_DISABLED) &&
            (u4Event & MFWD_CMDB_ENABLE_EVENT))
        {
            if (gMfwdContext.u1CacheMissDBStat == MFWD_CMDB_STATUS_DISABLED)
            {
                /* enable the cace miss data packets pool id */
                MfwdEnableCmdbStatus ();
            }
        }                        /* end of if cmdb status enable event */

        /* If the cache miss data buffer timer expires then call the timer
         * expiry routine to flush the CMDB packets
         */
        if ((gMfwdContext.u1MfwdStatus != MFWD_STATUS_DISABLED) &&
            (u4Event & MFWD_CMDB_TMR_EXP_EVENT))
        {
            /* Get each of the expired timers and call the expiry routine
             */
            while ((pExpTmr =
                    TmrGetNextExpiredTimer (gMfwdContext.CMDBTmrListId))
                   != NULL)
            {
                u2OwnerId = (UINT2) pExpTmr->u4Data;
                pExpTmr->u4Data = 0;
                MfwdMdhHandleCmdbTmrExp (u2OwnerId);
            }
        }
        MFWD_UNLOCK ();
    }                            /* End of infinite while loop */

}                                /* End of function RPH task main */

/*************************************************************************
 * Function Name    :  MfwdInputRegisterMRP
 *
 * Description      :  This function registers an MRP if such an owner does 
 *                     not exists and registration information is valid then
 *                     allocates required memory pools for the owner and
 *                     initialises the owner information node.
 *             
 * Input(s)         :  pMrpBuffer - Pointer to the buffer holding registration
 *                     information
 *
 * Output(s)        :  None
 *
 * Global Variables
 * Referred         :  None         
 *
 * Global Variables
 * Modified         :  None         
 *
 * Return(s)        :  MFWD_FAILURE or MFWD_SUCCESS
 ****************************************************************************/

INT4
MfwdInputRegisterMRP (tCRU_BUF_CHAIN_HEADER * pMrpBuffer)
{
    INT4                i4Status;
    tMrpRegnInfo        RegnInfo;
    tMfwdOwnerInfoNode *pNewOwnerNode;
    UINT2               u2OwnerId;

    if (CRU_BUF_Copy_FromBufChain (pMrpBuffer, (UINT1 *) &RegnInfo, 0,
                                   sizeof (tMrpRegnInfo)) == CRU_FAILURE)
    {
        CRU_BUF_Release_MsgBufChain (pMrpBuffer, FALSE);
        MFWD_DBG (MFWD_DBG_BUF_IF,
                  "Failure in Copying the MRP registration info\n");
        MFWD_DBG (MFWD_DBG_EXIT, "Exiting from Handling MRP registration"
                  "function\n");
        return MFWD_FAILURE;
    }

    CRU_BUF_Release_MsgBufChain (pMrpBuffer, FALSE);
    pNewOwnerNode = NULL;
    i4Status = MFWD_SUCCESS;
    u2OwnerId = RegnInfo.u2OwnerId;

    if (u2OwnerId >= MAX_MRP_INSTANCES)
    {
        MFWD_TRC_ARG1 (MFWD_TRC_FLAG, MFWD_CONTROL_PATH_TRC, MFWD_MOD_NAME,
                       "Inavalid Owner Id %d cannot handle registration\n",
                       u2OwnerId);
        return MFWD_FAILURE;
    }

    if (MFWD_OWNER (u2OwnerId) == NULL)
    {
        /* Validate the owner registration information */
        MFWD_IS_OWNER_DATA_VALID (RegnInfo, i4Status);

        if (i4Status != MFWD_FAILURE)
        {
            /* Allocate the owner information node for the new owner */
            MFWD_MEMPOOL_ALLOC (MFWD_OWNER_NODE_POOL_ID, tMfwdOwnerInfoNode *,
                                pNewOwnerNode, i4Status);

            if (i4Status == MFWD_FAILURE)
            {
                MFWD_DBG1 (MFWD_DBG_MEM_IF,
                           "Failure in allocating memory for the new"
                           " owner %d\n", u2OwnerId);
                i4Status = MFWD_FAILURE;
            }
            else
            {
                /* assign the registration information into the owner node */
                pNewOwnerNode->MrpInformMfwdStatus =
                    RegnInfo.MrpInformMfwdStatus;
                pNewOwnerNode->MrpDataPktCallBkFn =
                    RegnInfo.MrpHandleMcastDataPkt;
                pNewOwnerNode->MrpV6DataPktCallBkFn =
                    RegnInfo.MrpHandleV6McastDataPkt;
                pNewOwnerNode->u4MaxSrcs = RegnInfo.u4MaxSrcs;
                pNewOwnerNode->u1OwnerMode = RegnInfo.u1Mode;
                pNewOwnerNode->u4MaxGrps = RegnInfo.u4MaxGroups;
                pNewOwnerNode->u4MaxIfaces = RegnInfo.u4MaxIfaces;
                pNewOwnerNode->u2OwnerId = RegnInfo.u2OwnerId;
                pNewOwnerNode->u1ProtoId = RegnInfo.u1ProtoId;

                TMO_SLL_Init (&(pNewOwnerNode->OwnerIfList));

                /* now create a semaphore for the owner information data.
                 */
                MFWD_OWNER (u2OwnerId) = pNewOwnerNode;

                /* Now activate the owner */
                i4Status = MfwdOimActivateOwner (u2OwnerId);

                if (i4Status == MFWD_FAILURE)
                {
                    MFWD_DBG1 (MFWD_DBG_FAILURE,
                               "FAILURE in activating the newly registered owner %d \n",
                               u2OwnerId);

                    /* Since there was  failure in the activation of the owner
                     * deactivate the owner
                     */
                    MfwdInputHandleDeRegistration (u2OwnerId);
                }
                else
                {
                    MFWD_TRC_ARG1 (MFWD_TRC_FLAG, MFWD_CONTROL_PATH_TRC,
                                   MFWD_MOD_NAME,
                                   "New Owner %d now registered\n", u2OwnerId);
                }

            }                    /* end of else for if i4status == MFWD_FAILURE */

        }
        else
        {
            MFWD_TRC (MFWD_TRC_FLAG, MFWD_ALL_FAILURE_TRC, MFWD_MOD_NAME,
                      "Invalid registration info supplied by the new owner\n");
        }
    }
    else
    {
        MFWD_DBG1 (MFWD_DBG_FAILURE,
                   "Registration request from an alreader registered "
                   "owner %d\n", u2OwnerId);
        i4Status = MFWD_FAILURE;
    }

    MFWD_DBG (MFWD_DBG_EXIT, "Exiting registration of new owner function\n");
    return i4Status;
}                                /* end of function input handling registration */

/***************************************************************************
 * Function Name    :  MfwdInputDeRegisterMRP
 *
 * Description      :  This function registers an MRP if such an owner does 
 *             
 * Input(s)         :  pMrpBuffer - Pointer to the buffer holding 
 *                     de-registration information 
 *
 * Output(s)        :  None
 *
 * Global Variables
 * Referred         :  None         
 *
 * Global Variables
 * Modified         :  None         
 *
 * Return(s)        :  MFWD_FAILURE or MFWD_SUCCESS
 ****************************************************************************/

INT4
MfwdInputDeRegisterMRP (tCRU_BUF_CHAIN_HEADER * pMrpBuffer)
{
    INT4                i4Status;
    UINT2               u2OwnerId;
    tMrpRegnInfo        RegnInfo;

    i4Status = MFWD_SUCCESS;

    MFWD_DBG (MFWD_DBG_ENTRY, "Entering deregistration of MRP function\n");

    if (CRU_BUF_Copy_FromBufChain (pMrpBuffer, (UINT1 *) &RegnInfo, 0,
                                   sizeof (tMrpRegnInfo)) == CRU_FAILURE)
    {
        CRU_BUF_Release_MsgBufChain (pMrpBuffer, FALSE);
        MFWD_DBG (MFWD_DBG_BUF_IF,
                  "Failure in Copying the MRP de-registration info\n");
        MFWD_DBG (MFWD_DBG_EXIT, "Exiting from Handling MRP de-registration"
                  "function\n");
        return MFWD_FAILURE;
    }
    u2OwnerId = RegnInfo.u2OwnerId;

    CRU_BUF_Release_MsgBufChain (pMrpBuffer, FALSE);
    /* See if the owner is registered with MFWD only then it can be 
     * deregistered
     */
    if (u2OwnerId >= MAX_MRP_INSTANCES)
    {
        MFWD_TRC_ARG1 (MFWD_TRC_FLAG, MFWD_CONTROL_PATH_TRC, MFWD_MOD_NAME,
                       "Inavalid Owner Id %d cannot handle registration\n",
                       u2OwnerId);
        return MFWD_FAILURE;
    }

    if (MFWD_OWNER (u2OwnerId) != NULL)
    {
        i4Status = MfwdOimDeActivateOwner (u2OwnerId);
        if (i4Status != MFWD_SUCCESS)
        {
            MFWD_TRC_ARG1 (MFWD_TRC_FLAG, MFWD_ALL_FAILURE_TRC,
                           MFWD_MOD_NAME,
                           "Failure in deactivating the owner %d\n", u2OwnerId);
        }

        /* first take owner node sema4 and then deregister the owner 
         * This sema4 can not be given back as it is deleted when 
         * deactivating the owner.
         */

        if (i4Status != MFWD_SUCCESS)
        {
            MFWD_TRC (MFWD_TRC_FLAG, MFWD_OS_RESOURCE_TRC, MFWD_MOD_NAME,
                      "Sem Take Failure for deregistration\n");
        }
        /* release the owner node to the memory pool */
        MFWD_MEMRELEASE ((gMfwdContext.OwnerPool).poolId,
                         (UINT1 *) MFWD_OWNER (u2OwnerId), i4Status);

        if (i4Status == MFWD_FAILURE)
        {
            MFWD_DBG (MFWD_DBG_MEM_IF,
                      "Failure Releasing the owner info node to the pool\n");
            i4Status = MFWD_FAILURE;
        }

        MFWD_OWNER (u2OwnerId) = NULL;
    }
    else
    {
        MFWD_DBG1 (MFWD_DBG_FAILURE,
                   "Deregistration request from an unknown owner %d\n",
                   u2OwnerId);
        i4Status = MFWD_FAILURE;
    }

    MFWD_DBG (MFWD_DBG_EXIT, "Exiting Deregistration of new owner function\n");
    return i4Status;
}                                /* end of function input handling deregistration */

/*************************************************************************
 * Function Name    :  MfwdInputHandleRegistration
 *
 * Description      :  This function posts a message regarding MRP registration
 *             
 * Input(s)         :  regninfo   :- information required for registration 
 *
 * Output(s)        :  None
 *
 * Global Variables
 * Referred         :  None         
 *
 * Global Variables
 * Modified         :  None         
 *
 * Return(s)        :  MFWD_FAILURE or MFWD_SUCCESS
 ****************************************************************************/

INT4
MfwdInputHandleRegistration (tMrpRegnInfo regninfo)
{
    INT4                i4Status;
    tCRU_BUF_CHAIN_HEADER *pRegnBuf = NULL;
    tMfwdQData         *pMfwdQData;

    MFWD_DBG (MFWD_DBG_ENTRY,
              "Entering Function to post MRP Registration event to MFWD\n");

    pRegnBuf = CRU_BUF_Allocate_MsgBufChain (sizeof (tMrpRegnInfo), 0);
    if (pRegnBuf == NULL)
    {
        MFWD_TRC (MFWD_TRC_FLAG, MFWD_CONTROL_PATH_TRC, MFWD_MOD_NAME,
                  "Buffer alloction failed to intimate MRP registration\r\n");
        return MFWD_FAILURE;
    }

    CRU_BUF_Copy_OverBufChain (pRegnBuf, (UINT1 *) &regninfo, 0,
                               sizeof (tMrpRegnInfo));

    MFWD_MEMPOOL_ALLOC (MFWD_MSGQ_POOLID, tMfwdQData *, pMfwdQData, i4Status);

    if (pMfwdQData == NULL)
    {
        MFWD_TRC (MFWD_TRC_FLAG, MFWD_CONTROL_PATH_TRC, MFWD_MOD_NAME,
                  "Failure in allocating memory for the MFWD Q Data\n");
        /* Free the CRU buffer */
        CRU_BUF_Release_MsgBufChain (pRegnBuf, FALSE);
        return MFWD_FAILURE;
    }
    pMfwdQData->u1MsgId = MFWD_REG_MESSAGE;
    pMfwdQData->pMsg = pRegnBuf;

    /* Enqueue the buffer to MFWD task */
    if (OsixQueSend (MFWD_CQ_ID,
                     (UINT1 *) &pMfwdQData, OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        /* Free the CRU buffer */
        MFWD_TRC (MFWD_TRC_FLAG, MFWD_CONTROL_PATH_TRC, MFWD_MOD_NAME,
                  "Enqueuing MRP registration to MFWD - FAILED \n");
        MFWD_MEMRELEASE (MFWD_MSGQ_POOLID, pMfwdQData, i4Status);
        CRU_BUF_Release_MsgBufChain (pRegnBuf, FALSE);
        return MFWD_FAILURE;
    }
    else
    {
        MFWD_DBG (MFWD_DBG_CTRL_FLOW,
                  "Successfully posted an MRP registration to MFWD \n");
    }

    /* Send a EVENT to MFWD */
    OsixEvtSend (MFWD_TASK_ID, MFWD_MSGQ_EVENT);
    MFWD_DBG (MFWD_DBG_EXIT,
              "Exiting Function to post MRP registration to MFWD\n");
    return MFWD_SUCCESS;

}

/***************************************************************************
 * Function Name    :  MfwdInputHandleDeRegistration
 *
 * Description      :  This function posts a message regarding MRP 
 *                     de-registration
 *             
 * Input(s)         :  u2OwnerId  :- The owner id of the deregistering MRP. 
 *
 * Output(s)        :  None
 *
 * Global Variables
 * Referred         :  None         
 *
 * Global Variables
 * Modified         :  None         
 *
 * Return(s)        :  MFWD_FAILURE or MFWD_SUCCESS
 ****************************************************************************/

INT4
MfwdInputHandleDeRegistration (UINT2 u2OwnerId)
{
    INT4                i4Status;
    tMrpRegnInfo        regninfo;
    tCRU_BUF_CHAIN_HEADER *pRegnBuf = NULL;
    tMfwdQData         *pMfwdQData;

    regninfo.u2OwnerId = u2OwnerId;

    MFWD_DBG (MFWD_DBG_ENTRY,
              "Entering Function to post MRP Registration event to MFWD\n");

    pRegnBuf = CRU_BUF_Allocate_MsgBufChain (sizeof (tMrpRegnInfo), 0);
    if (pRegnBuf == NULL)
    {
        MFWD_TRC (MFWD_TRC_FLAG, MFWD_CONTROL_PATH_TRC, MFWD_MOD_NAME,
                  "Buffer alloction failed to intimate MRP registration\r\n");
        return MFWD_FAILURE;
    }

    CRU_BUF_Copy_OverBufChain (pRegnBuf, (UINT1 *) &regninfo, 0,
                               sizeof (tMrpRegnInfo));

    MFWD_MEMPOOL_ALLOC (MFWD_MSGQ_POOLID, tMfwdQData *, pMfwdQData, i4Status);

    if (pMfwdQData == NULL)
    {
        MFWD_TRC (MFWD_TRC_FLAG, MFWD_CONTROL_PATH_TRC, MFWD_MOD_NAME,
                  "Failure in allocating memory for the MFWD Q Data\n");
        /* Free the CRU buffer */
        CRU_BUF_Release_MsgBufChain (pRegnBuf, FALSE);
        return MFWD_FAILURE;
    }
    pMfwdQData->u1MsgId = MFWD_DEREG_MESSAGE;
    pMfwdQData->pMsg = pRegnBuf;

    /* Enqueue the buffer to MFWD task */

    if (OsixQueSend (MFWD_CQ_ID,
                     (UINT1 *) &pMfwdQData, OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        /* Free the CRU buffer */
        MFWD_TRC (MFWD_TRC_FLAG, MFWD_CONTROL_PATH_TRC, MFWD_MOD_NAME,
                  "Enqueuing MRP registration to MFWD - FAILED \n");
        CRU_BUF_Release_MsgBufChain (pRegnBuf, FALSE);
        return MFWD_FAILURE;
    }
    else
    {
        MFWD_DBG (MFWD_DBG_CTRL_FLOW,
                  "Successfully posted an MRP registration to MFWD \n");
    }

    /* Send a EVENT to MFWD */
    OsixEvtSend (MFWD_TASK_ID, MFWD_MSGQ_EVENT);

    MFWD_DBG (MFWD_DBG_EXIT,
              "Exiting Function to post MRP registration to MFWD\n");

    UNUSED_PARAM (i4Status);

    return MFWD_SUCCESS;
}

/*************************************************************************
 * Function Name    :  MfwdInputHandleMrpMesg
 *
 * Description      :  This function finds out the type of MRP update and 
 *                     and calls the interface functions in the MRP module 
 *                     and OIM module for the necessary update if the ownerID
 *                     is a valid one. 
 *             
 * Input(s)         :  pMrpBuffer :- This buffer contains the message sent
 *                                   by the MRP with the update data
 *
 * Output(s)        :  None
 *
 * Global Variables
 * Referred         :  None         
 *
 * Global Variables
 * Modified         :  None         
 *
 * Return(s)        :  MFWD_FAILURE or MFWD_SUCCESS
 ****************************************************************************/

INT4
MfwdInputHandleMrpMesg (tCRU_BUF_CHAIN_HEADER * pMrpBuffer)
{
    tMrpUpdMesgHdr      MrpHdr;
    INT4                i4Status = MFWD_FAILURE;

    MFWD_DBG (MFWD_DBG_ENTRY,
              "Entering the function to handle the MRP update message\n");

    /* Copy the mrp update message hdr from pBuffer to MrpHdr */
    if (CRU_BUF_Copy_FromBufChain (pMrpBuffer, (UINT1 *) &MrpHdr, 0,
                                   MFWD_MRP_UPD_HEADER_SIZE) == CRU_FAILURE)
    {
        CRU_BUF_Release_MsgBufChain (pMrpBuffer, FALSE);
        MFWD_DBG (MFWD_DBG_BUF_IF,
                  "Failure in Copying the MRP update buffer\n");
        MFWD_DBG (MFWD_DBG_EXIT, "Exiting from Handling MRP Update Message\n");
        return MFWD_FAILURE;
    }

    /* Move the Buffer pointer to extract the data */
    CRU_BUF_Move_ValidOffset (pMrpBuffer, sizeof (tMrpUpdMesgHdr));

    /* if the MRP which is trying to make the udpate is not registered
     * need not perform the update. Process the update from the registered
     * MRPs only
     */

    if (MFWD_OWNER (MrpHdr.u2OwnerId) != NULL)
    {
        switch (MrpHdr.u1UpdType)
        {

            case MFWD_MRP_OIM_UPDATE:
                i4Status = MfwdOimHandleUpd (MrpHdr, pMrpBuffer);

                if (i4Status == MFWD_FAILURE)
                {
                    MFWD_TRC (MFWD_TRC_FLAG, MFWD_ALL_FAILURE_TRC,
                              MFWD_MOD_NAME, "Failure in handling the "
                              "Interface Table update\n");
                }
                break;

            case MFWD_MRP_MRT_UPDATE:

                i4Status = MfwdMrtHandleUpd (MrpHdr, pMrpBuffer);
                if (i4Status == MFWD_FAILURE)
                {
                    MFWD_TRC (MFWD_TRC_FLAG, MFWD_ALL_FAILURE_TRC,
                              MFWD_MOD_NAME,
                              "Failure in handling the MRT update\n");
                }
                break;
            default:
                MFWD_TRC (MFWD_TRC_FLAG, MFWD_ALL_FAILURE_TRC, MFWD_MOD_NAME,
                          "Invalid update request from the MRP\n");
                break;
        }                        /* end of switch */

    }                            /* end of if owner information node is not NULL */
    else
    {
        MFWD_TRC (MFWD_TRC_FLAG, MFWD_ALL_FAILURE_TRC, MFWD_MOD_NAME,
                  "Update request from the MRP that is not registered\n");
    }

    CRU_BUF_Release_MsgBufChain (pMrpBuffer, FALSE);
    MFWD_DBG (MFWD_DBG_EXIT,
              "Exiting the function to handle the MRP update message\n");
    return i4Status;
}                                /* end of function MfwdInputHandleMrpMesg */

/*************************************************************************
 * Function Name    :  MfwdInputHandleMdpMesg
 *
 * Description      :  This function finds out if the MDP was received on 
 *                     a valid interface. After finding out the ownerid of the
 *                     interface it calls the interface function in the MDH 
 *                     module to process the MDP.
 *             
 * Input(s)         :  pMrpBuffer :- This buffer contains the Multicast data
 *                                   packet to be forwarded
 *
 * Output(s)        :  None
 *
 * Global Variables
 * Referred         :  gMfwdContext        
 *
 * Global Variables
 * Modified         :  None         
 *
 * Return(s)        :  MFWD_FAILURE or MFWD_SUCCESS
 ****************************************************************************/

INT4
MfwdInputHandleMdpMesg (tCRU_BUF_CHAIN_HEADER * pMdpBuf)
{
    INT4                i4Status = MFWD_SUCCESS;
    UINT4               u4IfIndex;
    tMfwdInterfaceNode *pInterfaceNode = NULL;
    UINT2               u2OwnerId = 0;
    MFWDIPHEADER        IpHdr;

    tIPvXAddr           SrcAddr;
    tIPvXAddr           DestAddr;
    UINT4               u4SrcAddr;
    UINT4               u4DestAddr;

    MFWD_DBG (MFWD_DBG_ENTRY, "Entering the function to handle the MDP\n");

    MFWD_GET_IFINDEX (pMdpBuf, IPVX_ADDR_FMLY_IPV4, u4IfIndex);
    if (u4IfIndex == CFA_INVALID_INDEX)
    {
        return MFWD_FAILURE;
    }

    pInterfaceNode = MFWD_GET_IF_NODE (u4IfIndex, IPVX_ADDR_FMLY_IPV4);

    /* If InterfaceNode is not empty */
    if (pInterfaceNode != NULL)
    {
        u2OwnerId = pInterfaceNode->u2OwnerId;
        if (MFWD_EXTRACT_IP_HEADER (&IpHdr, pMdpBuf) == MFWD_FAILURE)
        {
            CRU_BUF_Release_MsgBufChain (pMdpBuf, FALSE);
            MFWD_UNLOCK ();
            return MFWD_FAILURE;
        }

        u4SrcAddr = OSIX_NTOHL (IpHdr.u4Src);
        u4DestAddr = OSIX_NTOHL (IpHdr.u4Dest);
        IPVX_ADDR_INIT_IPV4 (SrcAddr, IPVX_ADDR_FMLY_IPV4,
                             (UINT1 *) &(u4SrcAddr));
        IPVX_ADDR_INIT_IPV4 (DestAddr, IPVX_ADDR_FMLY_IPV4,
                             (UINT1 *) &(u4DestAddr));

        i4Status = MfwdMdhForwardMDP (u2OwnerId, pMdpBuf, pInterfaceNode,
                                      SrcAddr, DestAddr);
        if (i4Status == MFWD_FAILURE)
        {
            MFWD_DBG4 (MFWD_DBG_BUF_IF,
                       "Failure in processing data packet for %d to %d"
                       "received on interface %d for the MRP %d\n",
                       IpHdr.u4Src, IpHdr.u4Dest, u4IfIndex, u2OwnerId);
        }
    }                            /* end of if pInterfaceNode != NULL */
    else
    {
        CRU_BUF_Release_MsgBufChain (pMdpBuf, FALSE);
        MFWD_TRC_ARG1 (MFWD_TRC_FLAG, MFWD_ALL_FAILURE_TRC, MFWD_MOD_NAME,
                       "Multicast data packet on unknown interface %d\n",
                       u4IfIndex);
        gMfwdContext.u4DiscPkts++;
        i4Status = MFWD_FAILURE;
    }

    MFWD_DBG (MFWD_DBG_EXIT, "Exiting the function to handle the MDP\n");
    return i4Status;
}

/*************************************************************************
 * Function Name    :  MfwdInputHandleMdp6Mesg
 *
 * Description      :  This function finds out if the IPv6 MDP was received on 
 *                     a valid interface. After finding out the ownerid of the
 *                     interface it calls the interface function in the MDH 
 *                     module to process the IPv6 MDP.
 *             
 * Input(s)         :  pMrpBuffer :- This buffer contains the Multicast data
 *                                   packet to be forwarded
 *
 * Output(s)        :  None
 *
 * Global Variables
 * Referred         :  gMfwdContext        
 *
 * Global Variables
 * Modified         :  None         
 *
 * Return(s)        :  MFWD_FAILURE or MFWD_SUCCESS
 ****************************************************************************/
INT4
MfwdInputHandleMdp6Mesg (tCRU_BUF_CHAIN_HEADER * pMdpBuf)
{
    INT4                i4Status = MFWD_SUCCESS;
    UINT4               u4IfIndex;
    tMfwdInterfaceNode *pInterfaceNode = NULL;
    UINT2               u2OwnerId = 0;
    tIp6Hdr             Ip6Hdr;
    tIPvXAddr           SrcAddr;
    tIPvXAddr           DestAddr;

    MFWD_DBG (MFWD_DBG_ENTRY, "Entering the function to handle the MDP\n");

    MFWD_GET_IFINDEX (pMdpBuf, IPVX_ADDR_FMLY_IPV6, u4IfIndex);
    pInterfaceNode = MFWD_GET_IF_NODE (u4IfIndex, IPVX_ADDR_FMLY_IPV6);

    /* If InterfaceNode is not empty */
    if (pInterfaceNode != NULL)
    {
        u2OwnerId = pInterfaceNode->u2OwnerId;
        MFWD_EXTRACT_IPV6_HEADER (&Ip6Hdr, pMdpBuf);

        IPVX_ADDR_INIT_IPV6 (SrcAddr, IPVX_ADDR_FMLY_IPV6,
                             (UINT1 *) &Ip6Hdr.srcAddr);
        IPVX_ADDR_INIT_IPV6 (DestAddr, IPVX_ADDR_FMLY_IPV6,
                             (UINT1 *) &Ip6Hdr.dstAddr);

        i4Status = MfwdMdhForwardMDP (u2OwnerId, pMdpBuf, pInterfaceNode,
                                      SrcAddr, DestAddr);

        if (i4Status == MFWD_FAILURE)
        {
            MFWD_DBG4 (MFWD_DBG_BUF_IF,
                       "Failure in processing data packet for %s to %s"
                       "received on interface %d for the MRP %d\n",
                       SrcAddr.au1Addr, DestAddr.au1Addr, u4IfIndex, u2OwnerId);
        }
    }                            /* end of if pInterfaceNode != NULL */
    else
    {
        CRU_BUF_Release_MsgBufChain (pMdpBuf, FALSE);
        MFWD_TRC_ARG1 (MFWD_TRC_FLAG, MFWD_ALL_FAILURE_TRC, MFWD_MOD_NAME,
                       "IPv6 Multicast data packet on unknown interface %d\n",
                       u4IfIndex);
        gMfwdContext.u4DiscPkts++;
        i4Status = MFWD_FAILURE;
    }

    MFWD_DBG (MFWD_DBG_EXIT, "Exiting the function to handle the IPv6 MDP\n");
    return i4Status;
}

/*************************************************************************
 * Function Name    :  MfwdGetModule
 *
 * Description      :  This function gets the Module name which is used 
 *                     for printing the trace
 *             
 * Input(s)         :  u2TraceModule - Trace module name
 *
 * Output(s)        :  None
 *
 * Global Variables
 * Referred         :  gMfwdContext        
 *
 * Global Variables
 * Modified         :  None         
 *
 * Return(s)        :  pointer to the trace module
 ****************************************************************************/

const char         *
MfwdGetModule (UINT2 u2TraceModule)
{
    UINT1               au1ModName[6][10] =
        { "MFWD-IO", "MFWD-MDH", "MFWD-MRP", "MFWD-MGMT" };
    MEMSET (gMfwdContext.au1MfwdTrcMode, 0, 12);
    switch (u2TraceModule)
    {
        case MFWD_IO_MODULE:
            STRCPY (gMfwdContext.au1MfwdTrcMode, au1ModName[0]);
            break;
        case MFWD_MDH_MODULE:
            STRCPY (gMfwdContext.au1MfwdTrcMode, au1ModName[1]);
            break;
        case MFWD_MRP_MODULE:
            STRCPY (gMfwdContext.au1MfwdTrcMode, au1ModName[3]);
            break;
        case MFWD_MGMT_MODULE:
            STRCPY (gMfwdContext.au1MfwdTrcMode, au1ModName[4]);
            break;
        default:
            MEMSET (gMfwdContext.au1MfwdTrcMode, 0, 12);
            break;
    }
    return ((const char *) gMfwdContext.au1MfwdTrcMode);
}

                         /* end of function inpu handling data data packet */
