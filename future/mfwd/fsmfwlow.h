/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmfwlow.h,v 1.5 2008/08/20 15:13:52 iss Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIpMRouteEnable ARG_LIST((INT4 *));

INT1
nmhGetIpMRouteEntryCount ARG_LIST((UINT4 *));

INT1
nmhGetIpMRouteEnableCmdb ARG_LIST((INT4 *));

INT1
nmhGetMfwdGlobalTrace ARG_LIST((INT4 *));

INT1
nmhGetMfwdGlobalDebug ARG_LIST((INT4 *));

INT1
nmhGetIpMRouteDiscardedPkts ARG_LIST((UINT4 *));

INT1
nmhGetMfwdAvgDataRate ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIpMRouteEnable ARG_LIST((INT4 ));

INT1
nmhSetIpMRouteEnableCmdb ARG_LIST((INT4 ));

INT1
nmhSetMfwdGlobalTrace ARG_LIST((INT4 ));

INT1
nmhSetMfwdGlobalDebug ARG_LIST((INT4 ));

INT1
nmhSetMfwdAvgDataRate ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IpMRouteEnable ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2IpMRouteEnableCmdb ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2MfwdGlobalTrace ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2MfwdGlobalDebug ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2MfwdAvgDataRate ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2IpMRouteEnable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IpMRouteEnableCmdb ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2MfwdGlobalTrace ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2MfwdGlobalDebug ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2MfwdAvgDataRate ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for IpMRouteTable. */
INT1
nmhValidateIndexInstanceIpMRouteTable ARG_LIST((INT4  , UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for IpMRouteTable  */

INT1
nmhGetFirstIndexIpMRouteTable ARG_LIST((INT4 * , UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIpMRouteTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIpMRouteUpstreamNeighbor ARG_LIST((INT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetIpMRouteInIfIndex ARG_LIST((INT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetIpMRouteUpTime ARG_LIST((INT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetIpMRoutePkts ARG_LIST((INT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetIpMRouteDifferentInIfPackets ARG_LIST((INT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetIpMRouteProtocol ARG_LIST((INT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetIpMRouteRtAddress ARG_LIST((INT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetIpMRouteRtMask ARG_LIST((INT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetIpMRouteRtType ARG_LIST((INT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

/* Proto Validate Index Instance for IpMRouteNextHopTable. */
INT1
nmhValidateIndexInstanceIpMRouteNextHopTable ARG_LIST((INT4  , UINT4  , UINT4  , UINT4  , INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for IpMRouteNextHopTable  */

INT1
nmhGetFirstIndexIpMRouteNextHopTable ARG_LIST((INT4 * , UINT4 * , UINT4 * , UINT4 * , INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIpMRouteNextHopTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIpMRouteNextHopState ARG_LIST((INT4  , UINT4  , UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetIpMRouteNextHopUpTime ARG_LIST((INT4  , UINT4  , UINT4  , UINT4  , INT4  , UINT4 ,UINT4 *));

/* Proto Validate Index Instance for IpMRouteInterfaceTable. */
INT1
nmhValidateIndexInstanceIpMRouteInterfaceTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for IpMRouteInterfaceTable  */

INT1
nmhGetFirstIndexIpMRouteInterfaceTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIpMRouteInterfaceTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIpMRouteInterfaceOwnerId ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIpMRouteInterfaceTtl ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIpMRouteInterfaceProtocol ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIpMRouteInterfaceRateLimit ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIpMRouteInterfaceInMcastOctets ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpMRouteInterfaceCmdbPktCnt ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpMRouteInterfaceOutMcastOctets ARG_LIST((INT4 ,UINT4 *));
