/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * 
 * $Id: mfmacs.h,v 1.10 2011/10/13 14:07:00 siva Exp $
 * 
 ********************************************************************/
#ifndef _MFWD_MACROS_
#define _MFWD_MACROS_


#define  MFWD_MEMPOOL_ALLOC(poolID, type, pu1Block, i4Status) \
{ \
  pu1Block = (type)MemAllocMemBlk (poolID); \
  i4Status = (pu1Block == NULL)?MFWD_FAILURE:MFWD_SUCCESS; \
}
    
#define  MFWD_MEMRELEASE(poolId, pRelBlock, i4Status) \
{ \
   i4Status = MemReleaseMemBlock (poolId, (UINT1 *) pRelBlock); \
   i4Status = (i4Status == (INT4) MEM_FAILURE)?MFWD_FAILURE:MFWD_SUCCESS; \
}

#define MFWD_GET_NEW_SEMNAME   MfwdGetNewSemName

#define MFWD_STORE_SEM_NAME    MfwdStoreSemName

#define MFWD_GET_IF_HASH_INDEX(IfIndex, IfHash) \
       IfHash = (IfIndex % MFWD_IF_HASH_KEY)

/* macro to find base address of structure given the member address */
#define MFWD_GET_BASE_PTR(type, memberName, pMember) \
          (type *) ((FS_ULONG)(pMember) - (FS_ULONG)(&((type *)0)->memberName))

#define  MFWD_OWNER_MAX_SOURCES(u2OwnerId) \
    ((gMfwdContext.pOwnerInfoTbl)[u2OwnerId]->u4MaxSrcs)* \
    ((gMfwdContext.pOwnerInfoTbl)[u2OwnerId]->u4MaxGrps)

/* Expression calculating the maximum number of route entries for an owner */
#define  MFWD_OWNER_MAX_RTENTRIES(u2OwnerId)  \
     ((gMfwdContext.pOwnerInfoTbl)[u2OwnerId]->u4MaxGrps)

/* Expression calculating the maximum number of ougoing interfaces
 * for an owner 
 */
#define  MFWD_OWNER_MAX_OIFS(u2OwnerId)  \
    MFWD_OWNER_MAX_RTENTRIES(u2OwnerId)  *\
    ((gMfwdContext.pOwnerInfoTbl)[u2OwnerId]->u4MaxIfaces - 1)

#define  MFWD_OWNER_MAX_NEXTHOP_NODES(u2OwnerId) \
    ((gMfwdContext.pOwnerInfoTbl)[u2OwnerId]->u4MaxSrcs)* \
    ((gMfwdContext.pOwnerInfoTbl)[u2OwnerId]->u4MaxGrps) * \
     (MFWD_MAX_NEXTHOPS_PER_RTENTRY) 

/* Owner registration information validation macro */
#define  MFWD_IS_OWNER_DATA_VALID(regninfo, i4Status) \
{ \
  if (regninfo.u2OwnerId > gMfwdContext.u2MaxMrps) i4Status = MFWD_FAILURE; \
  if (regninfo.u1Mode != MFWD_MRP_DENSE_MODE && \
      regninfo.u1Mode != MFWD_MRP_SPARSE_MODE)  i4Status = MFWD_FAILURE;\
  if (regninfo.u4MaxSrcs == 0) i4Status = MFWD_FAILURE; \
  if (regninfo.u4MaxGroups == 0)  i4Status = MFWD_FAILURE; \
  if (regninfo.u4MaxIfaces == 0)  i4Status = MFWD_FAILURE; \
}


#define  MFWD_LOCK() \
    OsixSemTake (MFWD_MUTEX_SEMID);\

#define  MFWD_UNLOCK() \
    OsixSemGive (MFWD_MUTEX_SEMID);\


/* Obtain the interface index of the multicast data packet from the CRU 
 * buffer
 */
#define  MFWD_GET_IFINDEX(pBuf, u1AddrType,  u4IfIndex) \
{ \
   tIpParms *pIpParms;\
if (u1AddrType == IPVX_ADDR_FMLY_IPV4) {\
   pIpParms = (tIpParms *)(&pBuf->ModuleData);\
   u4IfIndex = pIpParms->u2Port; \
}\
if (u1AddrType == IPVX_ADDR_FMLY_IPV6) {\
        u4IfIndex = CfaGetIfIpPort ((UINT2) pBuf->ModuleData.InterfaceId.u4IfIndex); \
}\
}

#define MFWD_INIT_IF_NODE(pNewIfNode, u4IfIndex, u1AddrType, OwnerId) \
     pNewIfNode->u2OwnerId = OwnerId; \
     pNewIfNode->u4IfIndex = u4IfIndex; \
     pNewIfNode->u1AddrType = u1AddrType; \
     TMO_SLL_Init_Node (&(pNewIfNode->OwnerIfLink)); \
     TMO_SLL_Init (&(pNewIfNode->CmdbList)); \
     pNewIfNode->u4IfRtLmt = 0; \
     pNewIfNode->u4DscdPktCnt = 0;

#define  MFWD_GET_HASH_INDEX(GrpAddr, u1HashIndex) \
{ \
    UINT1 u1Index = 0;\
    for (u1Index=0; u1Index < 16;u1Index++) {\
           u1HashIndex = (UINT1) ((GrpAddr.au1Addr[u1Index] ^ u1HashIndex)); \
    }\
    u1HashIndex = u1HashIndex ^ GrpAddr.u1Afi;\
    u1HashIndex = (UINT1)(u1HashIndex % MFWD_HASH_TABLE_SIZE); \
}

 /* based on the source mask and the group mask given find out the
  * entry type
  */
/* TO Check */

#define MFWD_GET_ENTRY_TYPE(u4GrpMask, u4SrcMask, u1EntryType) \
{ \
   if (u4GrpMask != MFWD_ADDRESS_EXACT_MASK) \
   { \
       u1EntryType = MFWD_STAR_STAR_ENTRY; \
   } \
   else \
   { \
       if (u4SrcMask != MFWD_ADDRESS_EXACT_MASK) \
       { \
            u1EntryType = MFWD_STARG_ENTRY;  \
       } \
       else   \
       { \
            u1EntryType = MFWD_SG_ENTRY; \
       } \
   } \
}


/* This macro releases a group node to the memory pool */
#define  MFWD_REL_GRP_NODE(OwnerId, GrpNode, RtTable, HashIndex, i4Status) \
{ \
   TMO_HASH_Delete_Node (RtTable, GrpNode, (UINT4)  HashIndex); \
   MFWD_MEMRELEASE (MFWD_GRP_NODE_POOLID (OwnerId), GrpNode, i4Status); \
   if (i4Status != MEM_SUCCESS) \
   { \
        MFWD_DBG (MFWD_DBG_MEM_IF, \
                  "Failure in releasing the group node to the memory"); \
   } \
   else \
   { \
        MFWD_DBG (MFWD_DBG_MEM_IF, \
                  "Released the group node to the memory pool"); \
   } \
   i4Status=?(i4Status!=MEM_SUCCESS)MFWD_FAILURE:MFWD_SUCCESS; \
}

#define  MFWD_MRT_UPDATE_IIF(pUpdEntry, pUpdData) \
     pUpdEntry->pSrcDescNode->u4Iif    = pUpdData->u4Iif; \
     pUpdEntry->u1ChkRpf               = pUpdData->u1ChkRpfFlag;



/* macro to check whether the router is a last Hop Router */
#define MFWD_CHK_IF_FIRST_HOP_RTR(IfIndex, SrcAddr, RetCode) \
{\
   UINT4    NxtHopAddr;\
   UINT4       MetPref;\
   UINT4    IfaceIndex;\
   INT4        Metrics;\
   INT4         RetVal;\
\
   MFWD_IP_GET_UCAST_INFO(SrcAddr, &NxtHopAddr, &Metrics, \
                                   &IfaceIndex, &MetPref, RetVal);\
   if ((IP_SUCCESS == RetVal) && (SrcAddr == NxtHopAddr)) \
   {\
         if (IfIndex == IfaceIndex) \
         { \
                 RetCode = MFWD_FIRST_HOP_ROUTER;\
         } \
   } \
   else  \
   {\
        RetCode = 0;\
   }\
}


/* macro to check whether the router is a last Hop Router */
#define MFWD_CHK_IF_IPv6_FIRST_HOP_RTR(IfIndex, SrcAddr, RetCode) \
{\
   tIPvXAddr  NxtHopAddr;\
   UINT4       MetPref;\
   UINT4    IfaceIndex;\
   INT4        Metrics;\
   INT4         RetVal;\
\
   MFWD_IPV6_GET_UCAST_INFO(SrcAddr, &NxtHopAddr, &Metrics, \
                                   &IfaceIndex, &MetPref, RetVal);\
   if ((IP_SUCCESS == RetVal) && (IPVX_ADDR_COMPARE(SrcAddr,NxtHopAddr) == 0) \
   {\
         if (IfIndex == IfaceIndex) \
         { \
                 RetCode = MFWD_FIRST_HOP_ROUTER;\
         } \
   } \
   else  \
   {\
        RetCode = 0;\
   }\
}




#define  MFWD_INIT_DUMMY_OWNER() \
{ \
    gMfwdContext.DummyOwner.OwnerNodeSema4.u4SemName = 0x00ffffff;  \
    gMfwdContext.DummyOwner.OwnerNodeSema4.semId = 0x00ffffff;  \
    gMfwdContext.DummyOwner.u2OwnerId = 0xffff;  \
\
    OsixCreateSem ( \
             (UINT1 *) &((gMfwdContext.DummyOwner).OwnerNodeSema4.u4SemName),\
              1, OSIX_GLOBAL, \
              &((gMfwdContext.DummyOwner).OwnerNodeSema4.semId));\
}

#define  MFWD_INIT_DUMMY_IF() \
{ \
     gMfwdContext.DummyIf.IfNodeSema4.u4SemName = 0x00ffffff;  \
     gMfwdContext.DummyIf.IfNodeSema4.semId     = 0x00ffffff;  \
     gMfwdContext.DummyIf.u4IfIndex             = 0xffffffff;  \
     OsixCreateSem ( \
               (UINT1 *) &((gMfwdContext.DummyIf).IfNodeSema4.u4SemName),\
               1, OSIX_GLOBAL, \
               &((gMfwdContext.DummyIf).IfNodeSema4.semId)); \
}

#define  MFWD_UPD_GRP_GETNEXT_LIST(pGrpNode) \
/* PENDING */

#define MFWD_GET_IF_NAME(port, u1AddrType, name) \
{ \
  UINT4 u4IfId; \
  UINT4 u4IfIndex; \
  tIfConfigRecord    IpInfo; \
  tNetIpv6IfInfo Ipv6Info;\
  if (u1AddrType == IPVX_ADDR_FMLY_IPV4) {\
      if (IpGetIfConfigRecord (port, &IpInfo) != IP_SUCCESS)\
      {\
         return CLI_SUCCESS;\
      }\
      u4IfId = IpInfo.InterfaceId.u4IfIndex; \
      CfaGetInterfaceNameFromIndex(u4IfId, name); \
  }\
  if (u1AddrType == IPVX_ADDR_FMLY_IPV6) {\
      if (NetIpv4GetCfaIfIndexFromPort (port, &u4IfIndex) != NETIPV4_SUCCESS)\
      {\
         return CLI_SUCCESS;\
      }\
      if (MfwdIpv6GetIfInfo(u4IfIndex, &Ipv6Info) != IP_SUCCESS)\
      {\
         return CLI_SUCCESS;\
      }\
      u4IfId = Ipv6Info.u4IfIndex; \
      CfaGetInterfaceNameFromIndex(u4IfId, name); \
  }\
}
#endif
