/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: mfoim.c,v 1.14 2014/01/29 13:16:10 siva Exp $
*
*********************************************************************/
#include "mfinc.h"

/* Global Context Variable */
extern tMfwdContextStructure gMfwdContext;

#ifdef TRACE_WANTED
static UINT2        u2MfwdTrcModule = MFWD_MRP_MODULE;
#endif

/****************************************************************************
 * Function Name    :  MfwdOimActivateOwner
 *
 * Description      :  This function allocates the memory required for the 
 *                     owner (MRP) to operate. and then sends an MFWD enabled
 *                     event to the MRP.
 *                     
 * Input(s)         :  u2OwnerId :- The owner id of the MRP that is to be 
 *                     activated
 *
 * Output(s)        :  None
 *
 * Global Variables
 * Referred         :  gMfwdContext ( Refered via the macro MFWD_OWNER)
 *
 * Global Variables
 * Modified         :  None
 *
 * Return(s)        :  MFWD_FAILURE or MFWD_SUCCESS
 ****************************************************************************/

INT4
MfwdOimActivateOwner (UINT2 u2OwnerId)
{
    INT4                i4Status = MFWD_SUCCESS;

    MFWD_DBG1 (MFWD_DBG_ENTRY,
               "Entering the Function to Activate the owner %d\n", u2OwnerId);

    /* Create a memory pool for the maximum number of interfaces required for
     * the MRP
     */
    MFWD_IF_POOL_BLOCK_SIZE = sizeof (tMfwdInterfaceNode);

    /* Create a memory pool for the maximum number of Group nodes required for
     * the MRP
     */
    MFWD_GRP_NODE_POOL_BLOCK_SIZE = sizeof (tMfwdGrpNode);

    /* Create a memory pool for the maximum number of Source nodes required for
     * the MRP
     */
    MFWD_SRC_NODE_POOL_BLOCK_SIZE = sizeof (tMfwdSrcNode);

    /* Create a memory pool for the maximum number of route entries required for
     * the MRP
     */
    MFWD_RT_NODE_POOL_BLOCK_SIZE = sizeof (tMfwdRtEntry);

    /* Create a memory pool for the maximum number of outgoing itnterfaces 
     * required for the MRP.
     */

    MFWD_OIF_NODE_POOL_BLOCK_SIZE = sizeof (tMfwdOifNode);

    /* If MRP is a sparse mode protocol then create a memory pool for the 
     * maximum next hop nodes to be stored aggregated over all the route entries
     */
    MFWD_NEXTHOP_NODE_POOL_BLOCK_SIZE = sizeof (tMfwdOifNextHopNode);

    MFWD_OWNER_MRT (u2OwnerId) = NULL;
    if (i4Status != MFWD_FAILURE)
    {
        MFWD_OWNER_MRT (u2OwnerId) =
            TMO_HASH_Create_Table (MFWD_HASH_TABLE_SIZE, MrtGrpNodeAddFn,
                                   FALSE);
    }

    /* Start the timer for the cache miss data packet belonging to this
     * MRP only if all the memory operations above were success ful and if the
     * MRP is not a sparse mode protocol
     */

    if ((i4Status != MFWD_FAILURE) &&
        (MFWD_OWNER_MODE (u2OwnerId) != MFWD_MRP_SPARSE_MODE))
    {
        if (TmrStartTimer (gMfwdContext.CMDBTmrListId,
                           &MFWD_OWNER (u2OwnerId)->CmdbTimer,
                           MFWD_CMDB_TIMER_DUR) != TMR_SUCCESS)
        {
            MFWD_TRC (MFWD_TRC_FLAG, MFWD_OS_RESOURCE_TRC, MFWD_MOD_NAME,
                      "Failure in Starting the timer for Cache miss data"
                      "packets\n");
            MFWD_OWNER (u2OwnerId)->CmdbTimer.u4Data = 0;
        }
        else
        {
            MFWD_OWNER (u2OwnerId)->CmdbTimer.u4Data = 0;
            MFWD_OWNER (u2OwnerId)->CmdbTimer.u4Data = (0xFFFF0000 |
                                                        (UINT4) u2OwnerId);
        }
    }

    /* Since all the memory pools are now active send an event to the MRP saying
     * that MFWD is active for that owner.
     */

    MFWD_DBG1 (MFWD_DBG_EXIT,
               "Exiting the Function to Activate the owner %d\n", u2OwnerId);
    return i4Status;
}                                /* end of function MfwdOimActivateOwner */

/****************************************************************************
 * Function Name    :  MfwdOimDeActivateOwner
 *
 * Description      :  This function deletes the memory pools created for the 
 *                     owner (MRP) to operate. and then sends an MFWD disabled
 *                     event to the MRP.
 *                     
 * Input(s)         :  u2OwnerId :- The owner id of the MRP that is to be 
 *                     deactivated
 *
 * Output(s)        :  None
 *
 * Global Variables
 * Referred         :  gMfwdContext ( Refered via the macro MFWD_OWNER)
 *
 * Global Variables
 * Modified         :  None
 *
 * Return(s)        :  MFWD_FAILURE or MFWD_SUCCESS
 ****************************************************************************/

INT4
MfwdOimDeActivateOwner (UINT2 u2OwnerId)
{
    INT4                i4Status = MFWD_SUCCESS;
    tMfwdInterfaceNode *pIfNode = NULL;
    tTMO_SLL_NODE      *pSllNode = NULL;

    MFWD_DBG1 (MFWD_DBG_ENTRY,
               "Entering the Function to Deactivate the owner %d\n", u2OwnerId);

    /* Take the semaphore before entering the crital section data */

    /* delete the MRT and CMDB hash tables if created */
    if (MFWD_OWNER_MRT (u2OwnerId) != NULL)
    {
        TMO_HASH_Delete_Table (MFWD_OWNER_MRT (u2OwnerId), GrpNodeFreeFn);
        MFWD_OWNER_MRT (u2OwnerId) = NULL;
    }

    /* delete all the mempory pools created, not the ones which were failed to 
     * be created.
     */
    if (MFWD_IF_POOL_BLOCK_SIZE != 0)
    {
        /* Delete all the interfaces from the owner if list and free the
         * memory pool of the if nodes
         */

        while ((pSllNode =
                TMO_SLL_First (&(MFWD_OWNER (u2OwnerId)->OwnerIfList))) != NULL)

        {
            pIfNode = MFWD_GET_BASE_PTR (tMfwdInterfaceNode, OwnerIfLink,
                                         pSllNode);
            MfwdDeleteInterfaceNode (pIfNode);
        }

        MFWD_DBG1 (MFWD_DBG_MEM_IF,
                   "Deleted the memory pool for the interface"
                   " nodes of the Owner %d\n", u2OwnerId);
    }

    /* Stop the timer if it is running */
    if ((MFWD_OWNER (u2OwnerId)->CmdbTimer.u4Data != 0) &&
        (TmrStopTimer (gMfwdContext.CMDBTmrListId,
                       &(MFWD_OWNER (u2OwnerId)->CmdbTimer)) != TMR_SUCCESS))
    {
        MFWD_TRC (MFWD_TRC_FLAG, MFWD_OS_RESOURCE_TRC, MFWD_MOD_NAME,
                  "Failure in Stopping the timer for Cache miss data packets\n");
        i4Status = MFWD_FAILURE;
    }

    /* Since all the memory pools are now deleted send an event to the MRP 
     * saying that MFWD is disabled for that owner
     */

    MFWD_DBG1 (MFWD_DBG_EXIT,
               "Exiting the Function to Deactivate the owner %d\n", u2OwnerId);
    return i4Status;
}                                /* end of function MfwdOimActivateOwner */

VOID
GrpNodeFreeFn (tTMO_HASH_NODE * pHashNode)
{
    pHashNode = NULL;
    UNUSED_PARAM (pHashNode);
}

/****************************************************************************
 * Function Name    :  MfwdOimHandleUpd
 *
 * Description      :  This function handles the update message received from 
 *                     the MRP for the interface ownership. This function based
 *                     on the update command invokes the delete or add functions
 *                     
 * Input(s)         :  u2OwnerId :- The owner id of the MRP that is to be 
 *                     deactivated
 *
 * Output(s)        :  None
 *
 * Global Variables
 * Referred         :  gMfwdContext ( Refered via the macro MFWD_OWNER)
 *
 * Global Variables
 * Modified         :  None
 *
 * Return(s)        :  MFWD_FAILURE or MFWD_SUCCESS
 ****************************************************************************/

INT4
MfwdOimHandleUpd (tMrpUpdMesgHdr MrpUpdHdr, tCRU_BUF_CHAIN_HEADER * pIfUpdBuf)
{
    INT4                i4Status = MFWD_SUCCESS;
    INT4                i4RetCode = MFWD_SUCCESS;
    UINT4               u4IfCount = 0;
    UINT1               u1AddrType = 0;

    MFWD_DBG1 (MFWD_DBG_ENTRY,
               "Entering the Function to Handle the If Update %d\n",
               MrpUpdHdr.u2OwnerId);

    /* See how many number of interfaces are to be updated and then 
     * call the corresponding update function based on the update type
     */

    if (CRU_BUF_Copy_FromBufChain (pIfUpdBuf, (UINT1 *) &u4IfCount,
                                   0, sizeof (UINT4)) != sizeof (UINT4))
    {
        MFWD_DBG (MFWD_DBG_BUF_IF,
                  "Invalid Update Buffer, Could not read the number of"
                  "interfaces to be updated value \n");
        i4Status = MFWD_FAILURE;
    }
    else
    {
        switch (MrpUpdHdr.u1UpdCmd)
        {
            case MFWD_DELETE_OWNERIF_CMD:
            case MFWD_DELETE_OWNERIFV6_CMD:
                u1AddrType = (MrpUpdHdr.u1UpdCmd == MFWD_DELETE_OWNERIF_CMD)
                    ? IPVX_ADDR_FMLY_IPV4 : IPVX_ADDR_FMLY_IPV6;
                i4RetCode = MfwdOimDeleteIfaces (MrpUpdHdr.u2OwnerId,
                                                 u4IfCount, u1AddrType,
                                                 pIfUpdBuf);
                MFWD_TRC_ARG1 (MFWD_TRC_FLAG, MFWD_OS_RESOURCE_TRC,
                               MFWD_MOD_NAME,
                               "Deleted all of the specified"
                               " interfaces of the owner %d\n",
                               MrpUpdHdr.u2OwnerId);
                break;

            case MFWD_ADD_OWNERIF_CMD:
            case MFWD_ADD_OWNERIFV6_CMD:

                u1AddrType = (MrpUpdHdr.u1UpdCmd == MFWD_ADD_OWNERIF_CMD)
                    ? IPVX_ADDR_FMLY_IPV4 : IPVX_ADDR_FMLY_IPV6;
                i4RetCode =
                    MfwdOimAddIfaces (MrpUpdHdr.u2OwnerId, u4IfCount,
                                      u1AddrType, pIfUpdBuf);

                MFWD_TRC_ARG1 (MFWD_TRC_FLAG, MFWD_OS_RESOURCE_TRC,
                               MFWD_MOD_NAME,
                               "Added all the interfaces"
                               " for the owner %d\n", MrpUpdHdr.u2OwnerId);
                break;

            default:
                i4Status = MFWD_FAILURE;
                MFWD_TRC_ARG1 (MFWD_TRC_FLAG, MFWD_OS_RESOURCE_TRC,
                               MFWD_MOD_NAME,
                               "Invalid update command for the interface"
                               "table update by %d\n", MrpUpdHdr.u2OwnerId);
        }                        /* end of switch MrpHdr.u1UpdCmd */

    }                            /* end of else CRU_bufcopyfrombufchain == MFWD_FAILURE */

    MFWD_DBG1 (MFWD_DBG_EXIT,
               "Exiting the Function to Handle If Update %d\n",
               MrpUpdHdr.u2OwnerId);

    UNUSED_PARAM (i4RetCode);

    return i4Status;

}                                /* end of function MfwdOimHandleUpd */

/****************************************************************************
 * Function Name    :  MfwdOimDeleteIfaces
 *
 * Description      :  This function deletes the interfaces specified by the  
 *                     update message from the interface table and owner 
 *                     information node.
 *                     
 * Input(s)         :  u2OwnerId :- The owner id of the MRP for which the 
 *                     interfaces are to be deleted
 *                     u4IfCount :- Number of interface to be deleted
 *                     pIfUpdBuf :- the buffer containing the interface indices
 *                     values which have to be deleted
 *
 * Output(s)        :  None
 *
 * Global Variables
 * Referred         :  gMfwdContext ( Refered via the macro MFWD_OWNER)
 *
 * Global Variables
 * Modified         :  None
 *
 * Return(s)        :  MFWD_FAILURE or MFWD_SUCCESS
 ****************************************************************************/

INT4
MfwdOimDeleteIfaces (UINT2 u2OwnerId, UINT4 u4IfCount, UINT1 u1AddrType,
                     tCRU_BUF_CHAIN_HEADER * pIfUpdBuf)
{
    INT4                i4Status = MFWD_SUCCESS;
    UINT4               u4IfCtr = 0;
    UINT4               u4IfIndex = 0;
    tMfwdInterfaceNode *pIfNode = NULL;

    MFWD_DBG1 (MFWD_DBG_ENTRY,
               "Entering the Function to Delete Ifaces for owner %d\n",
               u2OwnerId);

    /* Take the semaphore before entering the crital section data */

    while ((u4IfCtr < u4IfCount) &&
           (CRU_BUF_Copy_FromBufChain (pIfUpdBuf, (UINT1 *) &u4IfIndex,
                                       MFWD_IFINDEX_MESSAGE_OFFSET (u4IfCtr),
                                       sizeof (UINT4)) == sizeof (UINT4)))
    {

        MFWD_TRC_ARG2 (MFWD_TRC_FLAG, MFWD_CONTROL_PATH_TRC, MFWD_MOD_NAME,
                       "Deleting the interface %d of the owner %d\n",
                       u4IfIndex, u2OwnerId);
        u4IfCtr++;

        /* Take the semaphore on the interface node which is to be deleted 
         * from the interface table On failure return.
         */
        pIfNode = MFWD_GET_IF_NODE (u4IfIndex, u1AddrType);
        if (pIfNode == NULL)
        {
            MFWD_TRC_ARG2 (MFWD_TRC_FLAG, MFWD_ALL_FAILURE_TRC,
                           MFWD_MOD_NAME,
                           "The interface %d of the owner %d to be deleted"
                           " does not exist\n", u4IfIndex, u2OwnerId);
            continue;
        }

        /* Delink all the links of the interface node and release the 
         * node to the memory pool
         */
        MfwdDeleteInterfaceNode (pIfNode);

        /* If the semaphore was taken from the hash table then give the 
         * semaphore back so that it can be taken back in the loop
         */

    }                            /* End of while (CRU_COPY_FROM_BUFCHAIN) */

    /* give away the semaphore of the owner information node */

    MFWD_DBG1 (MFWD_DBG_EXIT,
               "Exiting the Function Delete Ifaces of owner %d\n", u2OwnerId);
    return i4Status;

}                                /* End of the function MfwdOimDeleteIfaces */

/*************************************************************************
 * Function Name    :  MfwdDeleteInterfaceNode 
 *
 * Description      :  This function deletes the given interface from the 
 *                     owner interface list, and the hash table if present
 *                     and releases the node to the memory pool
 *                     
 * Input(s)         :  pIfNode - The interface node that is to be deleted
 *
 *
 * Output(s)        :  None
 *
 * Global Variables
 * Referred         :  gMfwdContext ( Refered via the macro MFWD_OWNER)
 *
 * Global Variables
 * Modified         :  None
 *
 * Return(s)        :  None
 *************************************************************************/

void
MfwdDeleteInterfaceNode (tMfwdInterfaceNode * pIfNode)
{

    INT4                i4RetCode = MFWD_SUCCESS;
    UINT4               u4HashIndex = 0;
    UINT2               u2OwnerId = pIfNode->u2OwnerId;

    /* delete the link in the owner interface list */
    TMO_SLL_Delete (&(MFWD_OWNER (u2OwnerId)->OwnerIfList),
                    &(pIfNode->OwnerIfLink));
    MFWD_GET_IF_HASH_INDEX (pIfNode->u4IfIndex, u4HashIndex);
    TMO_HASH_Delete_Node (MFWD_IF_HASH_TABLE,
                          &pIfNode->IfHashLink, u4HashIndex);
    /* release the interface node to the memory pool */
    MFWD_MEMRELEASE (MFWD_IF_POOL_ID, (UINT1 *) pIfNode, i4RetCode);

}

/****************************************************************************
 * Function Name    :  MfwdOimAddIfaces
 *
 * Description      :  This function allocates memory for the each of the 
 *                     new interfaces specified by the update message 
 *                     by the update message   
 *                     information node.
 *                     
 * Input(s)         :  u2OwnerId :- The owner id of the MRP for which the 
 *                     interfaces are to be added
 *                     u4IfCount :- Number of interface to be added
 *                     pIfUpdBuf :- the buffer containing the interface indices
 *                     values which have to be created and added
 *
 * Output(s)        :  None
 *
 * Global Variables
 * Referred         :  gMfwdContext ( Refered via the macro MFWD_OWNER)
 *
 * Global Variables
 * Modified         :  None
 *
 * Return(s)        :  MFWD_FAILURE or MFWD_SUCCESS
 ****************************************************************************/

INT4
MfwdOimAddIfaces (UINT2 u2OwnerId, UINT4 u4IfCount, UINT1 u1AddrType,
                  tCRU_BUF_CHAIN_HEADER * pIfUpdBuf)
{
    INT4                i4Status = MFWD_SUCCESS;
    INT4                i4RetCode = MFWD_SUCCESS;
    UINT4               u4IfCtr = 0;
    UINT4               u4IfIndex = 0;
    tMfwdInterfaceNode *pNewIfNode = NULL;

    MFWD_DBG1 (MFWD_DBG_ENTRY,
               "Entering the Function to Adding Ifaces to owner %d\n",
               u2OwnerId);

    /* try to create each interface specified in the buffer if already not 
     * present and add it to the global inteface table
     */
    while ((u4IfCtr < u4IfCount) &&
           (CRU_BUF_Copy_FromBufChain (pIfUpdBuf, (UINT1 *) &u4IfIndex,
                                       MFWD_IFINDEX_MESSAGE_OFFSET (u4IfCtr),
                                       sizeof (UINT4)) == sizeof (UINT4)))
    {

        MFWD_TRC_ARG2 (MFWD_TRC_FLAG, MFWD_CONTROL_PATH_TRC, MFWD_MOD_NAME,
                       "Adding interface %d to the owner %d\n",
                       u4IfIndex, u2OwnerId);
        u4IfCtr++;

        if (MFWD_GET_IF_NODE (u4IfIndex, u1AddrType) != NULL)
        {
            MFWD_TRC_ARG2 (MFWD_TRC_FLAG, MFWD_ALL_FAILURE_TRC,
                           MFWD_MOD_NAME,
                           "The interface %d of the owner %d to be added"
                           " already exist\n", u4IfIndex, u2OwnerId);
            continue;
        }

        /* allocate memory block for the Interface node from its 
         * memory pool 
         */
        MFWD_MEMPOOL_ALLOC (MFWD_IF_POOL_ID, tMfwdInterfaceNode *,
                            pNewIfNode, i4RetCode);

        if (i4RetCode == MFWD_FAILURE)
        {
            MFWD_DBG2 (MFWD_DBG_MEM_IF,
                       "Failure in allocating the memory block for the"
                       " Interface Node %d of the owner %d\n", u4IfIndex,
                       u2OwnerId);
            continue;
        }
        KW_FALSEPOSITIVE_FIX (pNewIfNode);
        MFWD_DBG2 (MFWD_DBG_MEM_IF,
                   "Allocated memory block for the Interface Node %d of"
                   "the owner %d\n", u4IfIndex, u2OwnerId);

        /* Initialise the interface node with its values */
        MFWD_INIT_IF_NODE (pNewIfNode, u4IfIndex, u1AddrType, u2OwnerId);

        MfwdOimAddNewIfToIfTable (u2OwnerId, pNewIfNode);
    }                            /* end of while u4IfCtr < u4Ifcount */

    MFWD_DBG1 (MFWD_DBG_ENTRY,
               "Exiting the Function Adding Ifaces to owner %d\n", u2OwnerId);

    UNUSED_PARAM (i4RetCode);

    return i4Status;
}                                /* end of function add interfaces for the owner */

/****************************************************************************
 * Function Name    :  MfwdOimAddNewIfToIfTable 
 *
 * Description      :  This function adds a new interface to the interface
 *                     table based on the interface index and updates
 *                     the owner interface list
 *                     
 * Input(s)         :  u2OwnerId :- The owner id of the MRP for which the 
 *                     interfaces are to be added
 *                     pNewIfNode :- The node that is to be added
 *
 * Output(s)        :  None
 *
 * Global Variables
 * Referred         :  gMfwdContext referred via the macros
 *                     MFWD_LIN_IF_TABLE, MFWD_IF_HASH_TABLE
 *
 * Global Variables
 * Modified         :  None
 *
 * Return(s)        :  None
 ****************************************************************************/

void
MfwdOimAddNewIfToIfTable (UINT2 u2OwnerId, tMfwdInterfaceNode * pNewIfNode)
{
    tTMO_SLL_NODE      *pPrevNode = NULL;
    tMfwdInterfaceNode *pIfNode = NULL;
    tTMO_SLL_NODE      *pSllNode = NULL;
    UINT4               u4IfHash;

    MFWD_GET_IF_HASH_INDEX (pNewIfNode->u4IfIndex, u4IfHash);

    TMO_HASH_Add_Node (MFWD_IF_HASH_TABLE, &pNewIfNode->IfHashLink,
                       u4IfHash, (UINT1 *) &(pNewIfNode->u4IfIndex));

    /* Now add this node to the interface list of the owner in
     * the ascending order of the interface index
     */
    TMO_SLL_Scan (&((MFWD_OWNER (u2OwnerId))->OwnerIfList),
                  pSllNode, tTMO_SLL_NODE *)
    {
        pIfNode = MFWD_GET_BASE_PTR (tMfwdInterfaceNode, OwnerIfLink, pSllNode);
        if ((pIfNode->u4IfIndex > pNewIfNode->u4IfIndex) ||
            (pIfNode->u4IfIndex == pNewIfNode->u4IfIndex
             && pIfNode->u1AddrType != pNewIfNode->u1AddrType))
        {
            break;
        }
        pPrevNode = pSllNode;

    }                            /* end of TMO_SLL_Scan */

    TMO_SLL_Insert (&(MFWD_OWNER (u2OwnerId))->OwnerIfList, pPrevNode,
                    (tTMO_SLL_NODE *) & (pNewIfNode->OwnerIfLink));

    MFWD_TRC_ARG3 (MFWD_TRC_FLAG, MFWD_ALL_FAILURE_TRC,
                   MFWD_MOD_NAME,
                   "Added a new interface %d of type %d for the owner %d "
                   " in the global interface table\n",
                   pNewIfNode->u4IfIndex, pNewIfNode->u1AddrType, u2OwnerId);

}

/****************************************************************************
 * Function Name    :  MfwdOimGetInterfaceNode 
 *
 * Description      :  This functions searches through the hash table for
 *                     the interface node and returns the node if found
 *
 *                     
 * Input(s)         :  u4IfIndex :- The interface index of the interface 
 *                     node
 *
 * Output(s)        :  None
 *
 * Global Variables
 * Referred         :  gMfwdContext referred via the macros
 *                     MFWD_IF_HASH_TABLE
 *
 * Global Variables
 * Modified         :  None
 *
 * Return(s)        :  Interface Node if found. Null otherwise 
 ****************************************************************************/

tMfwdInterfaceNode *
MfwdOimGetInterfaceNode (UINT4 u4IfIndex, UINT1 u1AddrType)
{
    tMfwdInterfaceNode *pIfNode = NULL;
    UINT4               u4IfHash;

    MFWD_DBG (MFWD_DBG_ENTRY,
              "Searching for the interface in the hash table\n");

    MFWD_GET_IF_HASH_INDEX (u4IfIndex, u4IfHash);

    TMO_HASH_Scan_Bucket (MFWD_IF_HASH_TABLE, u4IfHash, pIfNode,
                          tMfwdInterfaceNode *)
    {
        if ((pIfNode->u4IfIndex == u4IfIndex)
            && (pIfNode->u1AddrType == u1AddrType))
        {
            MFWD_DBG (MFWD_DBG_ENTRY, "Found the interface node "
                      "exiting searching hash table\n");
            return pIfNode;
        }
    }

    MFWD_DBG (MFWD_DBG_ENTRY, "Interface not present in the hash table "
              "exiting search\n");
    return NULL;
}

/****************************************************************************
 * Function Name    :  MfwdOimDeActivateOwnerMemPool
 *
 * Description      :  This function deletes the memory pools created for the
 *                     owner (MRP) to operate.
 *
 * Input(s)         :  None
 *
 * Output(s)        :  None
 *
 * Global Variables
 * Referred         :  gMfwdContext ( Refered via the macro MFWD_OWNER)
 *
 * Global Variables
 * Modified         :  None
 *
 * Return(s)        :  None
 ****************************************************************************/
VOID
MfwdOimDeActivateOwnerMemPool (VOID)
{

    /* delete all the mempory pools created, not the ones which were failed to
     * be created.
     */
    if (MFWD_IF_POOL_BLOCK_SIZE != 0)
    {
        MemDeleteMemPool (MFWD_IF_POOL_ID);
        MFWD_IF_POOL_BLOCK_SIZE = 0;
    }

    if (MFWD_GRP_NODE_POOL_BLOCK_SIZE != 0)
    {
        /* Release all the memory blocks created for the group nodes */
        MemDeleteMemPool (MFWD_GRP_NODE_PID);
        MFWD_GRP_NODE_POOL_BLOCK_SIZE = 0;
    }

    if (MFWD_SRC_NODE_POOL_BLOCK_SIZE != 0)
    {
        /* Release all the memory blocks created for the sorce descriptor
         * nodes
         */
        MemDeleteMemPool (MFWD_SRC_NODE_PID);
        MFWD_SRC_NODE_POOL_BLOCK_SIZE = 0;
    }

    if (MFWD_RT_NODE_POOL_BLOCK_SIZE != 0)
    {
        /* Release all the memory blocks created for the Route entry
         * nodes
         */
        MemDeleteMemPool (MFWD_RT_NODE_PID);
        MFWD_RT_NODE_POOL_BLOCK_SIZE = 0;
    }

    if (MFWD_OIF_NODE_POOL_BLOCK_SIZE != 0)
    {
        MemDeleteMemPool (MFWD_OIF_NODE_PID);
        MFWD_OIF_NODE_POOL_BLOCK_SIZE = 0;
    }

    if (MFWD_NEXTHOP_NODE_POOL_BLOCK_SIZE != 0)
    {
        MemDeleteMemPool (MFWD_NEXTHOP_NODE_PID);
        MFWD_NEXTHOP_NODE_POOL_BLOCK_SIZE = 0;
    }
}                                /* end of function MfwdOimDeActivateOwnerMemPool */
