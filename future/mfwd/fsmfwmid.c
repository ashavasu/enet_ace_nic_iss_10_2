# include  "include.h"
# include  "fsmfwmid.h"
# include  "fsmfwlow.h"
# include  "fsmfwcon.h"
# include  "fsmfwogi.h"
# include  "extern.h"
# include  "midconst.h"
# include  "fsmfwdcli.h"

/****************************************************************************
 Function   : mfwdScalarsGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
tSNMP_VAR_BIND     *
mfwdScalarsGet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                UINT1 u1_arg, UINT1 u1_search_type)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for Scalar get routine. */

    UINT1               i1_ret_val = FALSE;
    INT4                LEN_mfwdScalars_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;
    UINT4               u4_counter_val = FALSE;
    tSNMP_COUNTER64_TYPE u8_counter_val = { 0, 0 };
    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;
    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  This Variable is declared for being used in the
     *  FOR Loop for extracting Indices from OID Given.
     */

/*** DECLARATION_END ***/

    LEN_mfwdScalars_INDEX = p_in_db->u4_Length;

    /*  Incrementing the Length for the Extract of Scalar Tables. */
    LEN_mfwdScalars_INDEX++;
    if (u1_search_type == SNMP_SEARCH_TYPE_EXACT)
    {
        if ((LEN_mfwdScalars_INDEX != (INT4) p_incoming->u4_Length)
            || (p_incoming->pu4_OidList[p_incoming->u4_Length - 1] != 0))
        {
            return ((tSNMP_VAR_BIND *) NULL);
        }
    }
    else
    {
        /*  Get Next Operation on the Scalar Variable.  */
        if ((INT4) p_incoming->u4_Length >= LEN_mfwdScalars_INDEX)
        {
            return ((tSNMP_VAR_BIND *) NULL);
        }
    }
    switch (u1_arg)
    {
        case IPMROUTEENABLE:
        {
            i1_ret_val = nmhGetIpMRouteEnable (&i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case IPMROUTEENTRYCOUNT:
        {
            i1_ret_val = nmhGetIpMRouteEntryCount (&u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_GAUGE32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case IPMROUTEENABLECMDB:
        {
            i1_ret_val = nmhGetIpMRouteEnableCmdb (&i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case MFWDGLOBALTRACE:
        {
            i1_ret_val = nmhGetMfwdGlobalTrace (&i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case MFWDGLOBALDEBUG:
        {
            i1_ret_val = nmhGetMfwdGlobalDebug (&i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case IPMROUTEDISCARDEDPKTS:
        {
            i1_ret_val = nmhGetIpMRouteDiscardedPkts (&u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_COUNTER32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case MFWDAVGDATARATE:
        {
            i1_ret_val = nmhGetMfwdAvgDataRate (&i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);
    }                            /* End Of SWITCH Case for All Objects. */

    /* Incrementing the Length of the p_in_db. */
    p_in_db->u4_Length++;
    /* Adding the .0 to the p_in_db for scalar Objects. */
    p_in_db->pu4_OidList[p_in_db->u4_Length - 1] = ZERO;

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */

/****************************************************************************
 Function   : mfwdScalarsSet
 Description: This routine returns the value of the requestedMIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Output     : The actual OID for which the operation is
              performed is returned in p_in_db.
 Returns    : Returns one of the following error codes
           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for set_routine. */
INT4
mfwdScalarsSet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for set routine */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    /*
     *  This Variable is used to extract the Value in The
     *  MULTI DATA TYPE which is sent by the Manager.
     */

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
    }                            /* End Of Else Condition. */
    switch (u1_arg)
    {
        case IPMROUTEENABLE:
        {
            i1_ret_val = nmhSetIpMRouteEnable (p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case IPMROUTEENABLECMDB:
        {
            i1_ret_val = nmhSetIpMRouteEnableCmdb (p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case MFWDGLOBALTRACE:
        {
            i1_ret_val = nmhSetMfwdGlobalTrace (p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case MFWDGLOBALDEBUG:
        {
            i1_ret_val = nmhSetMfwdGlobalDebug (p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case MFWDAVGDATARATE:
        {
            i1_ret_val = nmhSetMfwdAvgDataRate (p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
            /*  Read Only Variables. */
        case IPMROUTEENTRYCOUNT:
            /*  Read Only Variables. */
        case IPMROUTEDISCARDEDPKTS:
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*   THE SET ROUTINES GET OVER. */

/****************************************************************************
 Function   : mfwdScalarsTest
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Returns    : Returns one of the following error codes

           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_INCONSISTNT_VALUE
           SNMP_ERR_WRONG_VALUE
           SNMP_ERR_WRONG_LENGTH
           SNMP_ERR_NOT_WRITABLE
           SNMP_ERR_NO_CREATION
           SNMP_ERR_WRONG_TYPE
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for test_routine */

INT4
mfwdScalarsTest (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                 UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for Test routine . */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    UINT4               u4ErrorCode = 0;
    /*
     *  This Variable is used to extract the Value in The
     *  Multi Data Type which is sent by the Manager
     */

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
    }
    if (p_incoming->pu4_OidList[p_incoming->u4_Length - 1] != 0)
    {
        return (SNMP_ERR_GEN_ERR);
    }
    switch (u1_arg)
    {

        case IPMROUTEENABLE:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2IpMRouteEnable (&u4ErrorCode, p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case IPMROUTEENABLECMDB:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2IpMRouteEnableCmdb (&u4ErrorCode,
                                             p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case MFWDGLOBALTRACE:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2MfwdGlobalTrace (&u4ErrorCode, p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case MFWDGLOBALDEBUG:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2MfwdGlobalDebug (&u4ErrorCode, p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case MFWDAVGDATARATE:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2MfwdAvgDataRate (&u4ErrorCode, p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }
            /*  Read Only Variables */

        case IPMROUTEENTRYCOUNT:
        case IPMROUTEDISCARDEDPKTS:
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*  THE TEST ROUTINES ARE OVER . */

/****************************************************************************
 Function   : ipMRouteEntryGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
tSNMP_VAR_BIND     *
ipMRouteEntryGet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                  UINT1 u1_arg, UINT1 u1_search_type)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for get routine . */
    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_partial_index_len = FALSE;
    INT4                i4_partial_index_flag = TRUE;
    INT4                i4_size_offset = FALSE;
    INT4                LEN_ipMRouteTable_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;
    UINT4               u4_counter_val = FALSE;
    tSNMP_COUNTER64_TYPE u8_counter_val = { 0, 0 };
    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    UINT1               u1_octet_string[MAX_OID_LENGTH] = NULL_STRING;
    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;
    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  These Variable are declared for being used in the
     *  FOR Loop for extracting Indices from the given OID.
     */
    INT4                i4_count = FALSE;
    INT4                i4_ipMRouteOwnerId = FALSE;
    INT4                i4_next_ipMRouteOwnerId = FALSE;

    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_ipMRouteGroup = FALSE;
    UINT4               u4_addr_next_ipMRouteGroup = FALSE;
    UINT1               u1_addr_ipMRouteGroup[ADDR_LEN] = NULL_STRING;
    UINT1               u1_addr_next_ipMRouteGroup[ADDR_LEN] = NULL_STRING;

    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_ipMRouteSource = FALSE;
    UINT4               u4_addr_next_ipMRouteSource = FALSE;
    UINT1               u1_addr_ipMRouteSource[ADDR_LEN] = NULL_STRING;
    UINT1               u1_addr_next_ipMRouteSource[ADDR_LEN] = NULL_STRING;

    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_ipMRouteSourceMask = FALSE;
    UINT4               u4_addr_next_ipMRouteSourceMask = FALSE;
    UINT1               u1_addr_ipMRouteSourceMask[ADDR_LEN] = NULL_STRING;
    UINT1               u1_addr_next_ipMRouteSourceMask[ADDR_LEN] = NULL_STRING;

    UINT4               u4_addr_ret_val_ipMRouteUpstreamNeighbor;
    UINT4               u4_addr_ret_val_ipMRouteRtAddress;
    UINT4               u4_addr_ret_val_ipMRouteRtMask;

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    switch (u1_search_type)
    {

        case SNMP_SEARCH_TYPE_EXACT:
        {
            /*
             *  Initializing the i4_size_offset Variable to the Length of the
             *  OID of the Object i.e the Length present in the p_in_db.
             */
            i4_size_offset = p_in_db->u4_Length;
            i4_size_offset += INTEGER_LEN;
            i4_size_offset += ADDR_LEN;
            i4_size_offset += ADDR_LEN;
            i4_size_offset += ADDR_LEN;

            if (p_incoming->u4_Length != (UINT4) i4_size_offset)
            {
                return (NULL);
            }
            /*
             *  Finding the length of the Index for extracting
             *  Index from the structure given by the Manager.
             */
            /*
             *  If LEN_OF_VARIABLE_LEN_INDEX is Present refer Header File
             *  where it is Defined for Explanation of the Variable.
             */
            LEN_ipMRouteTable_INDEX =
                p_in_db->u4_Length + INTEGER_LEN + ADDR_LEN + ADDR_LEN +
                ADDR_LEN;

            if (LEN_ipMRouteTable_INDEX == (INT4) p_incoming->u4_Length)
            {
                /* Extracting The Integer Index. */
                i4_ipMRouteOwnerId =
                    (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                   i4_offset];
                i4_offset++;

                /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                for (i4_count = FALSE; i4_count < ADDR_LEN;
                     i4_count++, i4_offset++)
                    u1_addr_ipMRouteGroup[i4_count] =
                        (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                        i4_offset];

                /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                u4_addr_ipMRouteGroup =
                    OSIX_NTOHL (*((UINT4 *) (u1_addr_ipMRouteGroup)));

                /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                for (i4_count = FALSE; i4_count < ADDR_LEN;
                     i4_count++, i4_offset++)
                    u1_addr_ipMRouteSource[i4_count] =
                        (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                        i4_offset];

                /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                u4_addr_ipMRouteSource =
                    OSIX_NTOHL (*((UINT4 *) (u1_addr_ipMRouteSource)));

                /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                for (i4_count = FALSE; i4_count < ADDR_LEN;
                     i4_count++, i4_offset++)
                    u1_addr_ipMRouteSourceMask[i4_count] =
                        (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                        i4_offset];

                /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                u4_addr_ipMRouteSourceMask =
                    OSIX_NTOHL (*((UINT4 *) (u1_addr_ipMRouteSourceMask)));

                /*  Low Level Routine Which Validates the Indices. */
                if ((i1_ret_val =
                     nmhValidateIndexInstanceIpMRouteTable (i4_ipMRouteOwnerId,
                                                            u4_addr_ipMRouteGroup,
                                                            u4_addr_ipMRouteSource,
                                                            u4_addr_ipMRouteSourceMask))
                    != SNMP_SUCCESS)
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
                /*  Storing the Extracted Index in p_in_db. */
                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    (UINT4) i4_ipMRouteOwnerId;
                for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        u1_addr_ipMRouteGroup[i4_count];

                for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        u1_addr_ipMRouteSource[i4_count];

                for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        u1_addr_ipMRouteSourceMask[i4_count];

                i4_partial_index_flag = FALSE;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case SNMP_SEARCH_TYPE_NEXT:
        {
            /*  The Manager Has Not Given the Indices. */
            if (p_incoming->u4_Length <= p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Low Level Get First Operation. */
                i4_partial_index_flag = FALSE;
                if ((i1_ret_val
                     =
                     nmhGetFirstIndexIpMRouteTable (&i4_ipMRouteOwnerId,
                                                    &u4_addr_ipMRouteGroup,
                                                    &u4_addr_ipMRouteSource,
                                                    &u4_addr_ipMRouteSourceMask))
                    == SNMP_SUCCESS)
                {
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_ipMRouteOwnerId;
                    *((UINT4 *) (u1_addr_ipMRouteGroup)) =
                        OSIX_HTONL (u4_addr_ipMRouteGroup);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            u1_addr_ipMRouteGroup[i4_count];

                    *((UINT4 *) (u1_addr_ipMRouteSource)) =
                        OSIX_HTONL (u4_addr_ipMRouteSource);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            u1_addr_ipMRouteSource[i4_count];

                    *((UINT4 *) (u1_addr_ipMRouteSourceMask)) =
                        OSIX_HTONL (u4_addr_ipMRouteSourceMask);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            u1_addr_ipMRouteSourceMask[i4_count];

                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else if (p_incoming->u4_Length > p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Get First Operation. */
                i4_partial_index_flag = TRUE;

                /*  Initializing the Partial Index Length. */
                i4_partial_index_len = p_in_db->u4_Length;

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += INTEGER_LEN;
                    /* Extracting The Integer Index. */
                    i4_ipMRouteOwnerId =
                        (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                       i4_offset];
                    i4_offset++;

                }
                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += ADDR_LEN;
                    /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                    for (i4_count = FALSE; i4_count < ADDR_LEN;
                         i4_count++, i4_offset++)
                        u1_addr_ipMRouteGroup[i4_count] =
                            (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                            i4_offset];

                    /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                    u4_addr_ipMRouteGroup =
                        OSIX_NTOHL (*((UINT4 *) (u1_addr_ipMRouteGroup)));

                }
                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += ADDR_LEN;
                    /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                    for (i4_count = FALSE; i4_count < ADDR_LEN;
                         i4_count++, i4_offset++)
                        u1_addr_ipMRouteSource[i4_count] =
                            (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                            i4_offset];

                    /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                    u4_addr_ipMRouteSource =
                        OSIX_NTOHL (*((UINT4 *) (u1_addr_ipMRouteSource)));

                }
                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += ADDR_LEN;
                    /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                    for (i4_count = FALSE; i4_count < ADDR_LEN;
                         i4_count++, i4_offset++)
                        u1_addr_ipMRouteSourceMask[i4_count] =
                            (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                            i4_offset];

                    /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                    u4_addr_ipMRouteSourceMask =
                        OSIX_NTOHL (*((UINT4 *) (u1_addr_ipMRouteSourceMask)));

                }

                /*
                 *  Get the value of the variable with the new index
                 *  By Calling the Low Level GET_NEXT routine.
                 */
                if ((i1_ret_val
                     =
                     nmhGetNextIndexIpMRouteTable (i4_ipMRouteOwnerId,
                                                   &i4_next_ipMRouteOwnerId,
                                                   u4_addr_ipMRouteGroup,
                                                   &u4_addr_next_ipMRouteGroup,
                                                   u4_addr_ipMRouteSource,
                                                   &u4_addr_next_ipMRouteSource,
                                                   u4_addr_ipMRouteSourceMask,
                                                   &u4_addr_next_ipMRouteSourceMask))
                    == SNMP_SUCCESS)
                {
                    i4_ipMRouteOwnerId = i4_next_ipMRouteOwnerId;
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_next_ipMRouteOwnerId;
                    u4_addr_ipMRouteGroup = u4_addr_next_ipMRouteGroup;
                    *((UINT4 *) (u1_addr_next_ipMRouteGroup)) =
                        OSIX_HTONL (u4_addr_ipMRouteGroup);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            (UINT4) u1_addr_next_ipMRouteGroup[i4_count];
                    u4_addr_ipMRouteSource = u4_addr_next_ipMRouteSource;
                    *((UINT4 *) (u1_addr_next_ipMRouteSource)) =
                        OSIX_HTONL (u4_addr_ipMRouteSource);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            (UINT4) u1_addr_next_ipMRouteSource[i4_count];
                    u4_addr_ipMRouteSourceMask =
                        u4_addr_next_ipMRouteSourceMask;
                    *((UINT4 *) (u1_addr_next_ipMRouteSourceMask)) =
                        OSIX_HTONL (u4_addr_ipMRouteSourceMask);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            (UINT4) u1_addr_next_ipMRouteSourceMask[i4_count];
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);

    }                            /* End of SWITCH statement. */

    /*
     *  The SWITCH CASE statement for calling the Low Level fn
     *  for all objects Low Level Routines for all Objects.
     */
    switch (u1_arg)
    {
        case IPMROUTEOWNERID:
        {
            i2_type = SNMP_DATA_TYPE_INTEGER;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                i4_return_val = i4_ipMRouteOwnerId;
            }
            else
            {
                i4_return_val = i4_next_ipMRouteOwnerId;
            }
            break;
        }
        case IPMROUTEGROUP:
        {
            i2_type = SNMP_DATA_TYPE_IP_ADDR_PRIM;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_ipMRouteGroup);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            else
            {
                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_next_ipMRouteGroup);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            break;
        }
        case IPMROUTESOURCE:
        {
            i2_type = SNMP_DATA_TYPE_IP_ADDR_PRIM;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_ipMRouteSource);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            else
            {
                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_next_ipMRouteSource);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            break;
        }
        case IPMROUTESOURCEMASK:
        {
            i2_type = SNMP_DATA_TYPE_IP_ADDR_PRIM;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_ipMRouteSourceMask);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            else
            {
                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_next_ipMRouteSourceMask);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            break;
        }
        case IPMROUTEUPSTREAMNEIGHBOR:
        {
            i1_ret_val =
                nmhGetIpMRouteUpstreamNeighbor (i4_ipMRouteOwnerId,
                                                u4_addr_ipMRouteGroup,
                                                u4_addr_ipMRouteSource,
                                                u4_addr_ipMRouteSourceMask,
                                                &u4_addr_ret_val_ipMRouteUpstreamNeighbor);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_IP_ADDR_PRIM;
                /* This part of the Code converts the ADDR to Octet String. */
                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_ret_val_ipMRouteUpstreamNeighbor);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case IPMROUTEINIFINDEX:
        {
            i1_ret_val =
                nmhGetIpMRouteInIfIndex (i4_ipMRouteOwnerId,
                                         u4_addr_ipMRouteGroup,
                                         u4_addr_ipMRouteSource,
                                         u4_addr_ipMRouteSourceMask,
                                         &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case IPMROUTEUPTIME:
        {
            i1_ret_val =
                nmhGetIpMRouteUpTime (i4_ipMRouteOwnerId, u4_addr_ipMRouteGroup,
                                      u4_addr_ipMRouteSource,
                                      u4_addr_ipMRouteSourceMask,
                                      &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_TIME_TICKS;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case IPMROUTEPKTS:
        {
            i1_ret_val =
                nmhGetIpMRoutePkts (i4_ipMRouteOwnerId, u4_addr_ipMRouteGroup,
                                    u4_addr_ipMRouteSource,
                                    u4_addr_ipMRouteSourceMask,
                                    &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_COUNTER32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case IPMROUTEDIFFERENTINIFPACKETS:
        {
            i1_ret_val =
                nmhGetIpMRouteDifferentInIfPackets (i4_ipMRouteOwnerId,
                                                    u4_addr_ipMRouteGroup,
                                                    u4_addr_ipMRouteSource,
                                                    u4_addr_ipMRouteSourceMask,
                                                    &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_COUNTER32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case IPMROUTEPROTOCOL:
        {
            i1_ret_val =
                nmhGetIpMRouteProtocol (i4_ipMRouteOwnerId,
                                        u4_addr_ipMRouteGroup,
                                        u4_addr_ipMRouteSource,
                                        u4_addr_ipMRouteSourceMask,
                                        &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case IPMROUTERTADDRESS:
        {
            i1_ret_val =
                nmhGetIpMRouteRtAddress (i4_ipMRouteOwnerId,
                                         u4_addr_ipMRouteGroup,
                                         u4_addr_ipMRouteSource,
                                         u4_addr_ipMRouteSourceMask,
                                         &u4_addr_ret_val_ipMRouteRtAddress);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_IP_ADDR_PRIM;
                /* This part of the Code converts the ADDR to Octet String. */
                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_ret_val_ipMRouteRtAddress);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case IPMROUTERTMASK:
        {
            i1_ret_val =
                nmhGetIpMRouteRtMask (i4_ipMRouteOwnerId, u4_addr_ipMRouteGroup,
                                      u4_addr_ipMRouteSource,
                                      u4_addr_ipMRouteSourceMask,
                                      &u4_addr_ret_val_ipMRouteRtMask);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_IP_ADDR_PRIM;
                /* This part of the Code converts the ADDR to Octet String. */
                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_ret_val_ipMRouteRtMask);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case IPMROUTERTTYPE:
        {
            i1_ret_val =
                nmhGetIpMRouteRtType (i4_ipMRouteOwnerId, u4_addr_ipMRouteGroup,
                                      u4_addr_ipMRouteSource,
                                      u4_addr_ipMRouteSourceMask,
                                      &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);
    }                            /* End Of SWITCH Case for All Objects. */

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */

/****************************************************************************
 Function   : ipMRouteNextHopEntryGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
tSNMP_VAR_BIND     *
ipMRouteNextHopEntryGet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                         UINT1 u1_arg, UINT1 u1_search_type)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for get routine . */
    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_partial_index_len = FALSE;
    INT4                i4_partial_index_flag = TRUE;
    INT4                i4_size_offset = FALSE;
    INT4                LEN_ipMRouteNextHopTable_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;
    UINT4               u4_counter_val = FALSE;
    tSNMP_COUNTER64_TYPE u8_counter_val = { 0, 0 };
    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    UINT1               u1_octet_string[MAX_OID_LENGTH] = NULL_STRING;
    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;
    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  These Variable are declared for being used in the
     *  FOR Loop for extracting Indices from the given OID.
     */
    INT4                i4_count = FALSE;
    INT4                i4_ipMRouteNextHopOwnerId = FALSE;
    INT4                i4_next_ipMRouteNextHopOwnerId = FALSE;

    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_ipMRouteNextHopGroup = FALSE;
    UINT4               u4_addr_next_ipMRouteNextHopGroup = FALSE;
    UINT1               u1_addr_ipMRouteNextHopGroup[ADDR_LEN] = NULL_STRING;
    UINT1               u1_addr_next_ipMRouteNextHopGroup[ADDR_LEN] =
        NULL_STRING;

    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_ipMRouteNextHopSource = FALSE;
    UINT4               u4_addr_next_ipMRouteNextHopSource = FALSE;
    UINT1               u1_addr_ipMRouteNextHopSource[ADDR_LEN] = NULL_STRING;
    UINT1               u1_addr_next_ipMRouteNextHopSource[ADDR_LEN] =
        NULL_STRING;

    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_ipMRouteNextHopSourceMask = FALSE;
    UINT4               u4_addr_next_ipMRouteNextHopSourceMask = FALSE;
    UINT1               u1_addr_ipMRouteNextHopSourceMask[ADDR_LEN] =
        NULL_STRING;
    UINT1               u1_addr_next_ipMRouteNextHopSourceMask[ADDR_LEN] =
        NULL_STRING;

    INT4                i4_ipMRouteNextHopIfIndex = FALSE;
    INT4                i4_next_ipMRouteNextHopIfIndex = FALSE;

    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_ipMRouteNextHopAddress = FALSE;
    UINT4               u4_addr_next_ipMRouteNextHopAddress = FALSE;
    UINT1               u1_addr_ipMRouteNextHopAddress[ADDR_LEN] = NULL_STRING;
    UINT1               u1_addr_next_ipMRouteNextHopAddress[ADDR_LEN] =
        NULL_STRING;

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    switch (u1_search_type)
    {

        case SNMP_SEARCH_TYPE_EXACT:
        {
            /*
             *  Initializing the i4_size_offset Variable to the Length of the
             *  OID of the Object i.e the Length present in the p_in_db.
             */
            i4_size_offset = p_in_db->u4_Length;
            i4_size_offset += INTEGER_LEN;
            i4_size_offset += ADDR_LEN;
            i4_size_offset += ADDR_LEN;
            i4_size_offset += ADDR_LEN;
            i4_size_offset += INTEGER_LEN;
            i4_size_offset += ADDR_LEN;

            if (p_incoming->u4_Length != (UINT4) i4_size_offset)
            {
                return (NULL);
            }
            /*
             *  Finding the length of the Index for extracting
             *  Index from the structure given by the Manager.
             */
            /*
             *  If LEN_OF_VARIABLE_LEN_INDEX is Present refer Header File
             *  where it is Defined for Explanation of the Variable.
             */
            LEN_ipMRouteNextHopTable_INDEX =
                p_in_db->u4_Length + INTEGER_LEN + ADDR_LEN + ADDR_LEN +
                ADDR_LEN + INTEGER_LEN + ADDR_LEN;

            if (LEN_ipMRouteNextHopTable_INDEX == (INT4) p_incoming->u4_Length)
            {
                /* Extracting The Integer Index. */
                i4_ipMRouteNextHopOwnerId =
                    (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                   i4_offset];
                i4_offset++;

                /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                for (i4_count = FALSE; i4_count < ADDR_LEN;
                     i4_count++, i4_offset++)
                    u1_addr_ipMRouteNextHopGroup[i4_count] =
                        (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                        i4_offset];

                /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                u4_addr_ipMRouteNextHopGroup =
                    OSIX_NTOHL (*((UINT4 *) (u1_addr_ipMRouteNextHopGroup)));

                /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                for (i4_count = FALSE; i4_count < ADDR_LEN;
                     i4_count++, i4_offset++)
                    u1_addr_ipMRouteNextHopSource[i4_count] =
                        (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                        i4_offset];

                /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                u4_addr_ipMRouteNextHopSource =
                    OSIX_NTOHL (*((UINT4 *) (u1_addr_ipMRouteNextHopSource)));

                /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                for (i4_count = FALSE; i4_count < ADDR_LEN;
                     i4_count++, i4_offset++)
                    u1_addr_ipMRouteNextHopSourceMask[i4_count] =
                        (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                        i4_offset];

                /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                u4_addr_ipMRouteNextHopSourceMask =
                    OSIX_NTOHL (*
                                ((UINT4
                                  *) (u1_addr_ipMRouteNextHopSourceMask)));

                /* Extracting The Integer Index. */
                i4_ipMRouteNextHopIfIndex =
                    (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                   i4_offset];
                i4_offset++;

                /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                for (i4_count = FALSE; i4_count < ADDR_LEN;
                     i4_count++, i4_offset++)
                    u1_addr_ipMRouteNextHopAddress[i4_count] =
                        (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                        i4_offset];

                /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                u4_addr_ipMRouteNextHopAddress =
                    OSIX_NTOHL (*((UINT4 *) (u1_addr_ipMRouteNextHopAddress)));

                /*  Low Level Routine Which Validates the Indices. */
                if ((i1_ret_val =
                     nmhValidateIndexInstanceIpMRouteNextHopTable
                     (i4_ipMRouteNextHopOwnerId, u4_addr_ipMRouteNextHopGroup,
                      u4_addr_ipMRouteNextHopSource,
                      u4_addr_ipMRouteNextHopSourceMask,
                      i4_ipMRouteNextHopIfIndex,
                      u4_addr_ipMRouteNextHopAddress)) != SNMP_SUCCESS)
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
                /*  Storing the Extracted Index in p_in_db. */
                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    (UINT4) i4_ipMRouteNextHopOwnerId;
                for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        u1_addr_ipMRouteNextHopGroup[i4_count];

                for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        u1_addr_ipMRouteNextHopSource[i4_count];

                for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        u1_addr_ipMRouteNextHopSourceMask[i4_count];

                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    (UINT4) i4_ipMRouteNextHopIfIndex;
                for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        u1_addr_ipMRouteNextHopAddress[i4_count];

                i4_partial_index_flag = FALSE;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case SNMP_SEARCH_TYPE_NEXT:
        {
            /*  The Manager Has Not Given the Indices. */
            if (p_incoming->u4_Length <= p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Low Level Get First Operation. */
                i4_partial_index_flag = FALSE;
                if ((i1_ret_val
                     =
                     nmhGetFirstIndexIpMRouteNextHopTable
                     (&i4_ipMRouteNextHopOwnerId, &u4_addr_ipMRouteNextHopGroup,
                      &u4_addr_ipMRouteNextHopSource,
                      &u4_addr_ipMRouteNextHopSourceMask,
                      &i4_ipMRouteNextHopIfIndex,
                      &u4_addr_ipMRouteNextHopAddress)) == SNMP_SUCCESS)
                {
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_ipMRouteNextHopOwnerId;
                    *((UINT4 *) (u1_addr_ipMRouteNextHopGroup)) =
                        OSIX_HTONL (u4_addr_ipMRouteNextHopGroup);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            u1_addr_ipMRouteNextHopGroup[i4_count];

                    *((UINT4 *) (u1_addr_ipMRouteNextHopSource)) =
                        OSIX_HTONL (u4_addr_ipMRouteNextHopSource);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            u1_addr_ipMRouteNextHopSource[i4_count];

                    *((UINT4 *) (u1_addr_ipMRouteNextHopSourceMask)) =
                        OSIX_HTONL (u4_addr_ipMRouteNextHopSourceMask);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            u1_addr_ipMRouteNextHopSourceMask[i4_count];

                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_ipMRouteNextHopIfIndex;
                    *((UINT4 *) (u1_addr_ipMRouteNextHopAddress)) =
                        OSIX_HTONL (u4_addr_ipMRouteNextHopAddress);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            u1_addr_ipMRouteNextHopAddress[i4_count];

                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else if (p_incoming->u4_Length > p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Get First Operation. */
                i4_partial_index_flag = TRUE;

                /*  Initializing the Partial Index Length. */
                i4_partial_index_len = p_in_db->u4_Length;

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += INTEGER_LEN;
                    /* Extracting The Integer Index. */
                    i4_ipMRouteNextHopOwnerId =
                        (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                       i4_offset];
                    i4_offset++;

                }
                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += ADDR_LEN;
                    /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                    for (i4_count = FALSE; i4_count < ADDR_LEN;
                         i4_count++, i4_offset++)
                        u1_addr_ipMRouteNextHopGroup[i4_count] =
                            (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                            i4_offset];

                    /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                    u4_addr_ipMRouteNextHopGroup =
                        OSIX_NTOHL (*
                                    ((UINT4 *) (u1_addr_ipMRouteNextHopGroup)));

                }
                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += ADDR_LEN;
                    /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                    for (i4_count = FALSE; i4_count < ADDR_LEN;
                         i4_count++, i4_offset++)
                        u1_addr_ipMRouteNextHopSource[i4_count] =
                            (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                            i4_offset];

                    /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                    u4_addr_ipMRouteNextHopSource =
                        OSIX_NTOHL (*
                                    ((UINT4
                                      *) (u1_addr_ipMRouteNextHopSource)));

                }
                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += ADDR_LEN;
                    /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                    for (i4_count = FALSE; i4_count < ADDR_LEN;
                         i4_count++, i4_offset++)
                        u1_addr_ipMRouteNextHopSourceMask[i4_count] =
                            (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                            i4_offset];

                    /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                    u4_addr_ipMRouteNextHopSourceMask =
                        OSIX_NTOHL (*
                                    ((UINT4
                                      *) (u1_addr_ipMRouteNextHopSourceMask)));

                }
                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += INTEGER_LEN;
                    /* Extracting The Integer Index. */
                    i4_ipMRouteNextHopIfIndex =
                        (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                       i4_offset];
                    i4_offset++;

                }
                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += ADDR_LEN;
                    /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                    for (i4_count = FALSE; i4_count < ADDR_LEN;
                         i4_count++, i4_offset++)
                        u1_addr_ipMRouteNextHopAddress[i4_count] =
                            (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                            i4_offset];

                    /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                    u4_addr_ipMRouteNextHopAddress =
                        OSIX_NTOHL (*
                                    ((UINT4
                                      *) (u1_addr_ipMRouteNextHopAddress)));

                }

                /*
                 *  Get the value of the variable with the new index
                 *  By Calling the Low Level GET_NEXT routine.
                 */
                if ((i1_ret_val
                     =
                     nmhGetNextIndexIpMRouteNextHopTable
                     (i4_ipMRouteNextHopOwnerId,
                      &i4_next_ipMRouteNextHopOwnerId,
                      u4_addr_ipMRouteNextHopGroup,
                      &u4_addr_next_ipMRouteNextHopGroup,
                      u4_addr_ipMRouteNextHopSource,
                      &u4_addr_next_ipMRouteNextHopSource,
                      u4_addr_ipMRouteNextHopSourceMask,
                      &u4_addr_next_ipMRouteNextHopSourceMask,
                      i4_ipMRouteNextHopIfIndex,
                      &i4_next_ipMRouteNextHopIfIndex,
                      u4_addr_ipMRouteNextHopAddress,
                      &u4_addr_next_ipMRouteNextHopAddress)) == SNMP_SUCCESS)
                {
                    i4_ipMRouteNextHopOwnerId = i4_next_ipMRouteNextHopOwnerId;
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_next_ipMRouteNextHopOwnerId;
                    u4_addr_ipMRouteNextHopGroup =
                        u4_addr_next_ipMRouteNextHopGroup;
                    *((UINT4 *) (u1_addr_next_ipMRouteNextHopGroup)) =
                        OSIX_HTONL (u4_addr_ipMRouteNextHopGroup);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            (UINT4) u1_addr_next_ipMRouteNextHopGroup[i4_count];
                    u4_addr_ipMRouteNextHopSource =
                        u4_addr_next_ipMRouteNextHopSource;
                    *((UINT4 *) (u1_addr_next_ipMRouteNextHopSource)) =
                        OSIX_HTONL (u4_addr_ipMRouteNextHopSource);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            (UINT4)
                            u1_addr_next_ipMRouteNextHopSource[i4_count];
                    u4_addr_ipMRouteNextHopSourceMask =
                        u4_addr_next_ipMRouteNextHopSourceMask;
                    *((UINT4 *) (u1_addr_next_ipMRouteNextHopSourceMask)) =
                        OSIX_HTONL (u4_addr_ipMRouteNextHopSourceMask);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            (UINT4)
                            u1_addr_next_ipMRouteNextHopSourceMask[i4_count];
                    i4_ipMRouteNextHopIfIndex = i4_next_ipMRouteNextHopIfIndex;
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_next_ipMRouteNextHopIfIndex;
                    u4_addr_ipMRouteNextHopAddress =
                        u4_addr_next_ipMRouteNextHopAddress;
                    *((UINT4 *) (u1_addr_next_ipMRouteNextHopAddress)) =
                        OSIX_HTONL (u4_addr_ipMRouteNextHopAddress);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            (UINT4)
                            u1_addr_next_ipMRouteNextHopAddress[i4_count];
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);

    }                            /* End of SWITCH statement. */

    /*
     *  The SWITCH CASE statement for calling the Low Level fn
     *  for all objects Low Level Routines for all Objects.
     */
    switch (u1_arg)
    {
        case IPMROUTENEXTHOPOWNERID:
        {
            i2_type = SNMP_DATA_TYPE_INTEGER;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                i4_return_val = i4_ipMRouteNextHopOwnerId;
            }
            else
            {
                i4_return_val = i4_next_ipMRouteNextHopOwnerId;
            }
            break;
        }
        case IPMROUTENEXTHOPGROUP:
        {
            i2_type = SNMP_DATA_TYPE_IP_ADDR_PRIM;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_ipMRouteNextHopGroup);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            else
            {
                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_next_ipMRouteNextHopGroup);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            break;
        }
        case IPMROUTENEXTHOPSOURCE:
        {
            i2_type = SNMP_DATA_TYPE_IP_ADDR_PRIM;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_ipMRouteNextHopSource);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            else
            {
                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_next_ipMRouteNextHopSource);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            break;
        }
        case IPMROUTENEXTHOPSOURCEMASK:
        {
            i2_type = SNMP_DATA_TYPE_IP_ADDR_PRIM;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_ipMRouteNextHopSourceMask);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            else
            {
                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_next_ipMRouteNextHopSourceMask);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            break;
        }
        case IPMROUTENEXTHOPIFINDEX:
        {
            i2_type = SNMP_DATA_TYPE_INTEGER;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                i4_return_val = i4_ipMRouteNextHopIfIndex;
            }
            else
            {
                i4_return_val = i4_next_ipMRouteNextHopIfIndex;
            }
            break;
        }
        case IPMROUTENEXTHOPADDRESS:
        {
            i2_type = SNMP_DATA_TYPE_IP_ADDR_PRIM;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_ipMRouteNextHopAddress);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            else
            {
                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_next_ipMRouteNextHopAddress);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            break;
        }
        case IPMROUTENEXTHOPSTATE:
        {
            i1_ret_val =
                nmhGetIpMRouteNextHopState (i4_ipMRouteNextHopOwnerId,
                                            u4_addr_ipMRouteNextHopGroup,
                                            u4_addr_ipMRouteNextHopSource,
                                            u4_addr_ipMRouteNextHopSourceMask,
                                            i4_ipMRouteNextHopIfIndex,
                                            u4_addr_ipMRouteNextHopAddress,
                                            &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case IPMROUTENEXTHOPUPTIME:
        {
            i1_ret_val =
                nmhGetIpMRouteNextHopUpTime (i4_ipMRouteNextHopOwnerId,
                                             u4_addr_ipMRouteNextHopGroup,
                                             u4_addr_ipMRouteNextHopSource,
                                             u4_addr_ipMRouteNextHopSourceMask,
                                             i4_ipMRouteNextHopIfIndex,
                                             u4_addr_ipMRouteNextHopAddress,
                                             &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_TIME_TICKS;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);
    }                            /* End Of SWITCH Case for All Objects. */

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */

/****************************************************************************
 Function   : ipMRouteInterfaceEntryGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
tSNMP_VAR_BIND     *
ipMRouteInterfaceEntryGet (tSNMP_OID_TYPE * p_in_db,
                           tSNMP_OID_TYPE * p_incoming, UINT1 u1_arg,
                           UINT1 u1_search_type)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for get routine . */
    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_partial_index_len = FALSE;
    INT4                i4_partial_index_flag = TRUE;
    INT4                i4_size_offset = FALSE;
    INT4                LEN_ipMRouteInterfaceTable_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;
    UINT4               u4_counter_val = FALSE;
    tSNMP_COUNTER64_TYPE u8_counter_val = { 0, 0 };
    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;
    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  These Variable are declared for being used in the
     *  FOR Loop for extracting Indices from the given OID.
     */
    INT4                i4_ipMRouteInterfaceIfIndex = FALSE;
    INT4                i4_next_ipMRouteInterfaceIfIndex = FALSE;

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    switch (u1_search_type)
    {

        case SNMP_SEARCH_TYPE_EXACT:
        {
            /*
             *  Initializing the i4_size_offset Variable to the Length of the
             *  OID of the Object i.e the Length present in the p_in_db.
             */
            i4_size_offset = p_in_db->u4_Length;
            i4_size_offset += INTEGER_LEN;

            if (p_incoming->u4_Length != (UINT4) i4_size_offset)
            {
                return (NULL);
            }
            /*
             *  Finding the length of the Index for extracting
             *  Index from the structure given by the Manager.
             */
            /*
             *  If LEN_OF_VARIABLE_LEN_INDEX is Present refer Header File
             *  where it is Defined for Explanation of the Variable.
             */
            LEN_ipMRouteInterfaceTable_INDEX = p_in_db->u4_Length + INTEGER_LEN;

            if (LEN_ipMRouteInterfaceTable_INDEX ==
                (INT4) p_incoming->u4_Length)
            {
                /* Extracting The Integer Index. */
                i4_ipMRouteInterfaceIfIndex =
                    (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                   i4_offset];
                i4_offset++;

                /*  Low Level Routine Which Validates the Indices. */
                if ((i1_ret_val =
                     nmhValidateIndexInstanceIpMRouteInterfaceTable
                     (i4_ipMRouteInterfaceIfIndex)) != SNMP_SUCCESS)
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
                /*  Storing the Extracted Index in p_in_db. */
                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    (UINT4) i4_ipMRouteInterfaceIfIndex;
                i4_partial_index_flag = FALSE;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case SNMP_SEARCH_TYPE_NEXT:
        {
            /*  The Manager Has Not Given the Indices. */
            if (p_incoming->u4_Length <= p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Low Level Get First Operation. */
                i4_partial_index_flag = FALSE;
                if ((i1_ret_val
                     =
                     nmhGetFirstIndexIpMRouteInterfaceTable
                     (&i4_ipMRouteInterfaceIfIndex)) == SNMP_SUCCESS)
                {
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_ipMRouteInterfaceIfIndex;
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else if (p_incoming->u4_Length > p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Get First Operation. */
                i4_partial_index_flag = TRUE;

                /*  Initializing the Partial Index Length. */
                i4_partial_index_len = p_in_db->u4_Length;

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += INTEGER_LEN;
                    /* Extracting The Integer Index. */
                    i4_ipMRouteInterfaceIfIndex =
                        (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                       i4_offset];
                    i4_offset++;

                }

                /*
                 *  Get the value of the variable with the new index
                 *  By Calling the Low Level GET_NEXT routine.
                 */
                if ((i1_ret_val
                     =
                     nmhGetNextIndexIpMRouteInterfaceTable
                     (i4_ipMRouteInterfaceIfIndex,
                      &i4_next_ipMRouteInterfaceIfIndex)) == SNMP_SUCCESS)
                {
                    i4_ipMRouteInterfaceIfIndex =
                        i4_next_ipMRouteInterfaceIfIndex;
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_next_ipMRouteInterfaceIfIndex;
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);

    }                            /* End of SWITCH statement. */

    /*
     *  The SWITCH CASE statement for calling the Low Level fn
     *  for all objects Low Level Routines for all Objects.
     */
    switch (u1_arg)
    {
        case IPMROUTEINTERFACEIFINDEX:
        {
            i2_type = SNMP_DATA_TYPE_INTEGER;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                i4_return_val = i4_ipMRouteInterfaceIfIndex;
            }
            else
            {
                i4_return_val = i4_next_ipMRouteInterfaceIfIndex;
            }
            break;
        }
        case IPMROUTEINTERFACEOWNERID:
        {
            i1_ret_val =
                nmhGetIpMRouteInterfaceOwnerId (i4_ipMRouteInterfaceIfIndex,
                                                &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case IPMROUTEINTERFACETTL:
        {
            i1_ret_val =
                nmhGetIpMRouteInterfaceTtl (i4_ipMRouteInterfaceIfIndex,
                                            &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case IPMROUTEINTERFACEPROTOCOL:
        {
            i1_ret_val =
                nmhGetIpMRouteInterfaceProtocol (i4_ipMRouteInterfaceIfIndex,
                                                 &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case IPMROUTEINTERFACERATELIMIT:
        {
            i1_ret_val =
                nmhGetIpMRouteInterfaceRateLimit (i4_ipMRouteInterfaceIfIndex,
                                                  &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case IPMROUTEINTERFACEINMCASTOCTETS:
        {
            i1_ret_val =
                nmhGetIpMRouteInterfaceInMcastOctets
                (i4_ipMRouteInterfaceIfIndex, &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_COUNTER32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case IPMROUTEINTERFACECMDBPKTCNT:
        {
            i1_ret_val =
                nmhGetIpMRouteInterfaceCmdbPktCnt (i4_ipMRouteInterfaceIfIndex,
                                                   &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_COUNTER32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case IPMROUTEINTERFACEOUTMCASTOCTETS:
        {
            i1_ret_val =
                nmhGetIpMRouteInterfaceOutMcastOctets
                (i4_ipMRouteInterfaceIfIndex, &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_COUNTER32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);
    }                            /* End Of SWITCH Case for All Objects. */

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */
