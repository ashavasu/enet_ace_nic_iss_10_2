/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: mfmrt.c,v 1.18 2014/09/29 12:35:09 siva Exp $
*
*********************************************************************/
#include "mfinc.h"

/* Global Context Variable */
extern tMfwdContextStructure gMfwdContext;

#ifdef TRACE_WANTED
static UINT2        u2MfwdTrcModule = MFWD_MRP_MODULE;
#endif

UINT1               gau1WcV6Group[IPVX_MAX_INET_ADDR_LEN] =
    { 0xff, 0x0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0
};

/****************************************************************************
 * Function Name    :  MfwdMrtHandleUpd
 *
 * Description      :  This function parses through the update message and   
 *                     invokes the corresponding functions for the creation,
 *                     deletion or update of the route entry.
 *                     
 * Input(s)         :  MrpUpdHdr :- The header of the update message which 
 *                     contains the owner for which the update is to be 
 *                     performed and the update command indicating the updates
 *                     to be performed.
 *                     pBuffer :- the buffer containing the update data
 *
 * Output(s)        :  None
 *
 * Global Variables
 * Referred         :  gMfwdContext.pMrtTable 
 *                     (Refered via the macro MFWD_OWNER)
 *
 * Global Variables
 * Modified         :  None
 *
 * Return(s)        :  MFWD_FAILURE or MFWD_SUCCESS
 ****************************************************************************/

INT4
MfwdMrtHandleUpd (tMrpUpdMesgHdr MrpUpdHdr, tCRU_BUF_CHAIN_HEADER * pBuffer)
{
    INT4                i4Status = MFWD_SUCCESS;
    tMrtUpdData         UpdData;
    UINT2               u2OwnerId;
    UINT1               u1EntryCnt;

    UINT1               u1AddrType = 0;
    UINT4               u4GrpAddr = 0;

    u2OwnerId = MrpUpdHdr.u2OwnerId;
    u1EntryCnt = MrpUpdHdr.u1NumEntries;

    MFWD_DBG1 (MFWD_DBG_ENTRY,
               "Entering the Function to Handle the MRT Update for the "
               "owner %d\n", u2OwnerId);

    /* See how many number of interfaces are to be updated and then 
     * call the corresponding update function based on the update type
     */

    while (u1EntryCnt != 0)
    {
        u1EntryCnt--;
        if (CRU_BUF_Copy_FromBufChain (pBuffer, (UINT1 *) &UpdData,
                                       0,
                                       sizeof (tMrtUpdData)) !=
            sizeof (tMrtUpdData))
        {
            MFWD_DBG (MFWD_DBG_BUF_IF,
                      "Invalid Update Buffer, Could not read the required data "
                      "For the entry update\n");
            return MFWD_FAILURE;
        }

        if ((UpdData.u1RtType != MFWD_STAR_G_ROUTE) || (UpdData.u4IifCnt == 0))
        {
            u1AddrType = UpdData.GrpAddr.u1Afi;
            if (MFWD_GET_IF_NODE (UpdData.u4Iif, u1AddrType) == NULL)
            {
                return MFWD_FAILURE;
            }
        }

        MFWD_TRC_ARG2 (MFWD_TRC_FLAG, MFWD_CONTROL_PATH_TRC, MFWD_MOD_NAME,
                       "Updating the MRT for the source %s and group %s\n",
                       UpdData.SrcAddr.au1Addr, UpdData.GrpAddr.au1Addr);

        if (MrpUpdHdr.u1UpdCmd & MFWD_ENTRY_DELETE_CMD)
        {
            if (UpdData.GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
                PTR_FETCH4 (u4GrpAddr, UpdData.GrpAddr.au1Addr);

            if (((UpdData.GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4) &&
                 (u4GrpAddr == MFWD_MCAST_WC_GROUP)) ||
                ((UpdData.GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6) &&
                 (memcmp (UpdData.GrpAddr.au1Addr, gau1WcV6Group, 16) == 0)))
            {
                MfwdMrtDeleteWcRtEntries (MrpUpdHdr.u2OwnerId, UpdData.RtAddr);
            }
            else
            {
                i4Status =
                    MfwdMrtDeleteRtEntry (MrpUpdHdr.u2OwnerId,
                                          UpdData.GrpAddr,
                                          UpdData.SrcAddr, UpdData.RtAddr);
            }

        }

        if (MrpUpdHdr.u1UpdCmd & MFWD_ENTRY_CREATE_CMD)
        {
            i4Status =
                MfwdMrtCreateRtEntry (MrpUpdHdr.u2OwnerId, &UpdData, pBuffer);

            if (i4Status == MFWD_FAILURE)
            {
                MFWD_TRC_ARG1 (MFWD_TRC_FLAG, MFWD_CONTROL_PATH_TRC,
                               MFWD_MOD_NAME, "Failure in creating the route "
                               "Entry for the owner %d\n", u2OwnerId);
            }
            else
            {
                MFWD_TRC_ARG1 (MFWD_TRC_FLAG, MFWD_CONTROL_PATH_TRC,
                               MFWD_MOD_NAME,
                               "Successfully created route Entry for the "
                               "owner %d\n", u2OwnerId);
            }
        }

        if (MrpUpdHdr.u1UpdCmd & MFWD_UPD_CMD_BITMAP)
        {

            if (UpdData.GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
                PTR_FETCH4 (u4GrpAddr, UpdData.GrpAddr.au1Addr);

            if (((UpdData.GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4) &&
                 (u4GrpAddr == MFWD_MCAST_WC_GROUP)) ||
                ((UpdData.GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6) &&
                 (memcmp (UpdData.GrpAddr.au1Addr, gau1WcV6Group, 16) == 0)))

            {
                MfwdMrtUpdateWcRtEntries (MrpUpdHdr.u2OwnerId,
                                          MrpUpdHdr.u1UpdCmd, &UpdData,
                                          pBuffer);
            }
            else
            {
                MfwdMrtUpdateRtEntry (MrpUpdHdr.u2OwnerId, MrpUpdHdr.u1UpdCmd,
                                      &UpdData, pBuffer, MrpUpdHdr.u2UpdSubCmd);
            }

        }                        /* end of if (MrpHdr.u1UpdCmd & MFWD_UPD_CMD_BITMAP) */

        CRU_BUF_Move_ValidOffset (pBuffer, (sizeof (tMrtUpdData) +
                                            (UpdData.u4OifCnt *
                                             sizeof (tMrpOifNode)) +
                                            (UpdData.u4IifCnt *
                                             sizeof (tMrpIifNode))));
    }

    MFWD_DBG1 (MFWD_DBG_EXIT,
               "Exiting the Function to Handle MRT Update for owner %d\n",
               u2OwnerId);
    return i4Status;

}                                /* end of function MfwdMrtHandleUpd */

/****************************************************************************
 * Function Name    :  MfwdMrtDeleteWcRtEntries
 *
 * Description      :  This function Deletes the Wild Card Route Entries
 *                     
 * Input(s)         :  u2OwnerId :- Owner Idendifier
 *                     u4RtAddr :- Route Addreess 
 *
 * Output(s)        :  None
 *
 * Global Variables
 * Referred         : None 
 *                    
 *
 * Global Variables
 * Modified         :  None
 *
 * Return(s)        :  VOID
 ****************************************************************************/

VOID
MfwdMrtDeleteWcRtEntries (UINT2 u2OwnerId, tIPvXAddr RtAddr)
{
    UINT4               u4HashIndex = 0;
    tMfwdGrpNode       *pGrpNode = NULL;
    tMfwdGrpNode       *pNxtGrpNode = NULL;

    tIPvXAddr           DummySrcAddr;

    /* for every group node in the routing table delete the SG entries 
     * which have the route address as RtAddr
     */

    TMO_HASH_Scan_Table (MFWD_OWNER_MRT (u2OwnerId), u4HashIndex)
    {
        for (pGrpNode = (tMfwdGrpNode *)
             TMO_HASH_Get_First_Bucket_Node (MFWD_OWNER_MRT (u2OwnerId),
                                             u4HashIndex); pGrpNode != NULL;)
        {
            pNxtGrpNode = (tMfwdGrpNode *)
                TMO_HASH_Get_Next_Bucket_Node (MFWD_OWNER_MRT (u2OwnerId),
                                               u4HashIndex,
                                               &pGrpNode->GrpNodeHashLink);

            memset (DummySrcAddr.au1Addr, 0, 16);
            MfwdMrtDeleteRtEntry (u2OwnerId, pGrpNode->GrpAddr, DummySrcAddr,
                                  RtAddr);
            pGrpNode = pNxtGrpNode;

        }                        /* End of  for loop */

    }                            /* End of TMO_HASH_Scan_Table ...... */
}

/****************************************************************************
 * Function Name    :  MfwdMrtDeleteRtEntry
 *
 * Description      :  This function deletes the route entry if it exists in the
 *                     MRT table.
 *                     If the source address is not supplied then
 *                     it deletes all the route entries which match with the
 *                     given rt address.
 *                     If the source address is supplied then that specific 
 *                     route entry is deleted.
 *                     
 * Input(s)         :  u2OwnerId :- The Identification value of the owner  
 *                     in which the entry is to be deleted 
 *                     u4GrpAddr :- The group address of the entry to be deleted
 *                     u4SrcAddr :- The source address of the entry
 *                     u4SrcMask :- The source mask which indicates (*, G) or 
 *                     (S, G) entry.
 *                     u4RtAddr  :- The route address used to look up the 
 *                     incoming interface towards the source.  This value is
 *                     used if the source address is zero.
 *
 * Output(s)        :  None
 *
 * Global Variables
 * Referred         :  gMfwdContext.pMrtTable 
 *                     (Refered via the macro MFWD_OWNER)
 *
 * Global Variables
 * Modified         :  None
 *
 * Return(s)        :  MFWD_FAILURE or MFWD_SUCCESS
 ****************************************************************************/

INT4
MfwdMrtDeleteRtEntry (UINT2 u2OwnerId, tIPvXAddr GrpAddr,
                      tIPvXAddr SrcAddr, tIPvXAddr RtAddr)
{
    INT4                i4Status = MFWD_SUCCESS;
    INT4                i4RetCode = MFWD_SUCCESS;
    UINT1               u1HashIndex = 0;
    tMfwdGrpNode       *pGrpNode = NULL;
    tMfwdGrpNode       *pDelGrpNode = NULL;
    tMfwdRtEntry       *pDelRtEntry = NULL;
    tMfwdRtEntry       *pRtEntry = NULL;
    UINT1               au1NullAddr[16];

    MFWD_DBG (MFWD_DBG_ENTRY,
              "Entering the function to delete the route entry\n");

    /* find the hash index of the group address */
    MFWD_GET_HASH_INDEX (GrpAddr, u1HashIndex);
    MEMSET (au1NullAddr, 0, sizeof (au1NullAddr));

    /* find the group node corresponding to the group address given */

    TMO_HASH_Scan_Bucket (MFWD_OWNER_MRT (u2OwnerId), (UINT4) u1HashIndex,
                          pGrpNode, tMfwdGrpNode *)
    {
        if (IPVX_ADDR_COMPARE (GrpAddr, pGrpNode->GrpAddr) < 0)
        {
            /* It means we have crossed the point before which the
             * group node is expected.
             */
            pGrpNode = NULL;    /* just to avoid the unnecessary del_grp_node */
            break;
        }

        /*Compare both GrpAddr and GrpMaskLen to support GroupAddresses with Multiple Mask */
        if ((IPVX_ADDR_COMPARE (GrpAddr, pGrpNode->GrpAddr) == 0) &&
            (GrpAddr.u1AddrLen == pGrpNode->u4GrpMaskLen))
        {
            /* find the route entry in the list of (S, G) entries
             * of the group node
             */
            for (pDelRtEntry = (tMfwdRtEntry *)
                 TMO_SLL_First (&pGrpNode->SgEntryList); pDelRtEntry != NULL;)
            {
                pRtEntry = (tMfwdRtEntry *)
                    TMO_SLL_Next (&pGrpNode->SgEntryList,
                                  &pDelRtEntry->SgEntryLink);

                if ((MEMCMP (SrcAddr.au1Addr, au1NullAddr, 16) == 0) &&
                    (IPVX_ADDR_COMPARE
                     (pDelRtEntry->pSrcDescNode->RtAddr, RtAddr) == 0))
                {
                    TMO_SLL_Delete (&(pGrpNode->SgEntryList),
                                    &pDelRtEntry->SgEntryLink);
                    MfwdMrtReleaseRtEntry (pDelRtEntry);
                }
                else if (IPVX_ADDR_COMPARE
                         (pDelRtEntry->pSrcDescNode->SrcAddr, SrcAddr) == 0)
                {
                    TMO_SLL_Delete (&(pGrpNode->SgEntryList),
                                    &pDelRtEntry->SgEntryLink);
                    MfwdMrtReleaseRtEntry (pDelRtEntry);
                    break;
                }

                pDelRtEntry = pRtEntry;

            }                    /* end of TMO_SLL_SCAN */
            pDelGrpNode = pGrpNode;

            break;
            /* This group node if it does not have any more entries
             * it can be deleted
             */

        }                        /* end of if pGrpNode->u4GrpAddr == pUpdData.u4SrcAddr */

    }                            /* end of hash scan bucket */

    /* If the group node does not contain any more entries there is 
     * no use with it, delete it.
     */
    if (pDelGrpNode != NULL)
    {
        if (TMO_SLL_Count (&(pDelGrpNode->SgEntryList)) == 0)
        {
            /* Delete the group node from the hash table and the 
             * get next list
             */
            TMO_HASH_Delete_Node (MFWD_OWNER_MRT (u2OwnerId),
                                  &pDelGrpNode->GrpNodeHashLink, u1HashIndex);

            /* Release the group node to the memory pool */
            i4RetCode = (INT4) MemReleaseMemBlock (MFWD_GRP_NODE_PID,
                                                   (UINT1 *) pDelGrpNode);

            if (i4RetCode != MEM_SUCCESS)
            {
                i4RetCode = MFWD_FAILURE;
                MFWD_DBG (MFWD_DBG_MEM_IF,
                          "Failure in releasing group node to the memory pool\n");
            }
            else
            {
                MFWD_DBG (MFWD_DBG_MEM_IF, "Released group node to the "
                          "memory pool as it contains no entries\n");
            }
        }                        /* end of if (pGrpNode->pWcardEntry == NULL && ..... */
    }                            /* end of if (pDelGrpNode != NULL) */

    if (i4RetCode != MFWD_SUCCESS)
    {
        MFWD_TRC (MFWD_TRC_FLAG, MFWD_OS_RESOURCE_TRC, MFWD_MOD_NAME,
                  "Failure in giving the semaphore after the deletion of route\n");
        i4Status = MFWD_FAILURE;
    }

    MFWD_DBG (MFWD_DBG_EXIT,
              "Exiting the function to delete the route entry\n");
    return i4Status;
}                                /* end of the function MfwdMrtDeleteRtEntry */

/****************************************************************************
 * Function Name    :  MfwdMrtReleaseRtEntry
 *
 * Description      :  This function Releases the rotue entry to the memory
 *                     pool along with the oifnodes, active source nodes,
 *                     source descriptor node.
 *                     
 * Input(s)         :  pRtEntry :- Pointer to the route entry that is to be 
 *                     freed
 *
 * Output(s)        :  None
 *
 * Global Variables
 * Referred         :  None                    
 *
 * Global Variables
 * Modified         :  None
 *
 * Return(s)        :  None
 ****************************************************************************/

VOID
MfwdMrtReleaseRtEntry (tMfwdRtEntry * pRtEntry)
{
    INT4                i4Status = 0;
    tMfwdOifNode       *pOifNode = NULL;
    tMfwdIifNode       *pIifNode = NULL;

    MFWD_DBG (MFWD_DBG_ENTRY,
              "Entering the function to release the route entry\n");

    /* De link the route entry from the MRT get next list */
    TMO_SLL_Delete (&(gMfwdContext.MrtGetNextList), &pRtEntry->GetNextLink);

    /* Release all the Iif Nodes of the route entry */
    if (pRtEntry->u1RtType == MFWD_STAR_G_ROUTE)
    {
        while ((pIifNode = (tMfwdIifNode *)
                TMO_SLL_First (&(pRtEntry->IifList))) != NULL)
        {
            i4Status = MfwdMrtDeleteIif (pRtEntry, pIifNode);
            if (i4Status == MFWD_FAILURE)
            {
                MFWD_TRC_ARG1 (MFWD_TRC_FLAG, MFWD_CONTROL_PATH_TRC,
                               MFWD_MOD_NAME,
                               "Failure in deleting the IIF Node %d\n",
                               pIifNode->u4IifIndex);
            }
        }
    }

    /* Release all the Oif Nodes of the route entry */
    while ((pOifNode = (tMfwdOifNode *)
            TMO_SLL_First (&(pRtEntry->OifList))) != NULL)
    {
        i4Status = MfwdMrtDeleteOif (pRtEntry, pOifNode);
        if (i4Status == MFWD_FAILURE)
        {
            MFWD_TRC_ARG1 (MFWD_TRC_FLAG, MFWD_CONTROL_PATH_TRC,
                           MFWD_MOD_NAME,
                           "Failure in deleting the OIF Node %d\n",
                           pOifNode->u4OifIndex);
        }
    }

    /* Now release the source descriptor node */
    MFWD_MEMRELEASE (MFWD_SRC_NODE_PID, pRtEntry->pSrcDescNode, i4Status);

    MFWD_DBG (MFWD_DBG_MEM_IF, "source descriptor node released to mempool\n");

    MFWD_MEMRELEASE (MFWD_RT_NODE_PID, pRtEntry, i4Status);

    gMfwdContext.u4RtEntryCount--;
    MFWD_TRC (MFWD_TRC_FLAG, MFWD_CONTROL_PATH_TRC, MFWD_MOD_NAME,
              "Deleted the route entry and release to the Memory Pool\n");

    MFWD_DBG (MFWD_DBG_EXIT,
              "Exiting the function to release the route entry\n");

}                                /* end of the function MfwdReleaseRtEntry */

/****************************************************************************
 * Function Name    :  MfwdMrtCreateRtEntry
 *
 * Description      :  This function Creates a new route entry in the MRT if it
 *                     does not already exists and updates the MRT get next
 *                     list. It also adds oifs and Iifs to the created route entry.
 *                     
 * Input(s)         :  u2OwnerId :- The Identification value of the owner  
 *                     in which the entry is to be created 
 *                     pUpdData :- pointer to the update data containing the 
 *                     information to be filled in the new route entry.
 *                     pUpdBuf :- Containing the information relating to the
 *                     outgoing interfaces.
 *
 * Output(s)        :  None
 *
 * Global Variables
 * Referred         :  gMfwdContext.pOwnerInfoTbl 
 *                     (Refered via the macro MFWD_OWNER)
 *
 * Global Variables
 * Modified         :  None
 *
 * Return(s)        :  MFWD_FAILURE or MFWD_SUCCESS
 ****************************************************************************/
INT4
MfwdMrtCreateRtEntry (UINT2 u2OwnerId, tMrtUpdData * pUpdData,
                      tCRU_BUF_CHAIN_HEADER * pUpdBuf)
{
    INT4                i4Status = MFWD_SUCCESS;
    INT4                i4RetCode = MFWD_SUCCESS;
    UINT1               u1HashIndex = 0;
    UINT1               u1EntryType = 0;
    UINT1               u1NewGrpNodeFlg = FALSE;
    tIPvXAddr           SrcAddr;

    tMfwdGrpNode       *pGrpNode = NULL;
    tMfwdRtEntry       *pSgEntry = NULL;
    tMfwdRtEntry       *pNewRtEntry = NULL;
    tMfwdRtEntry       *pPrevRtEntry = NULL;

    MFWD_DBG (MFWD_DBG_ENTRY,
              "Entering the function to create the route entry\n");

    MEMSET (&SrcAddr, 0, sizeof (tIPvXAddr));
    /* find the entry type that is to be created */
    MFWD_GET_ENTRY_TYPE (pUpdData->u4GrpMaskLen, pUpdData->u4SrcMaskLen,
                         u1EntryType);
    /* find the hash index of the group address */
    MFWD_GET_HASH_INDEX (pUpdData->GrpAddr, u1HashIndex);
    /*If STAR_G Route, then replace the recevied SrcAddr with Empty address
     * This is to create a *,G route */
    if ((pUpdData->u1RtType == MFWD_STAR_G_ROUTE) && (pUpdData->u4IifCnt > 0))
    {
        IPVX_ADDR_COPY (&pUpdData->SrcAddr, &SrcAddr);
    }

    /* find the group node corresponding to the group address given */
    TMO_HASH_Scan_Bucket (MFWD_OWNER_MRT (u2OwnerId), (UINT4) u1HashIndex,
                          pGrpNode, tMfwdGrpNode *)
    {
        /* If the group node itself does not exist we need to create one
         * and then do the operation
         */
        if (IPVX_ADDR_COMPARE (pUpdData->GrpAddr, pGrpNode->GrpAddr) < 0)
        {
            pGrpNode = NULL;
            break;

        }                        /* end of if pUpdData.u4GrpAddr < pGrpNode->u4GrpAddr */

        /* If the group address matches then see if the entry already 
         * exists if it does not exist then carry the insertion address out
         */
        if ((IPVX_ADDR_COMPARE (pGrpNode->GrpAddr, pUpdData->GrpAddr) == 0) &&
            (pUpdData->u4GrpMaskLen == pGrpNode->u4GrpMaskLen))
        {
            /* find the route entry in the list of (S, G) entries
             * of the group node
             */
            TMO_SLL_Scan (&(pGrpNode->SgEntryList), pSgEntry, tMfwdRtEntry *)
            {
                if (IPVX_ADDR_COMPARE
                    (pSgEntry->pSrcDescNode->SrcAddr, pUpdData->SrcAddr) == 0)
                {
                    /* entry already exists return failure */
                    i4Status = MFWD_FAILURE;
                    break;
                }
                else if (IPVX_ADDR_COMPARE (pUpdData->SrcAddr,
                                            pSgEntry->pSrcDescNode->SrcAddr) >
                         0)
                {
                    pPrevRtEntry = pSgEntry;
                    continue;
                }
                else
                {
                    break;
                }
            }                    /* end of TMO_SLL_SCAN */
            break;
        }                        /* end of if pGrpNode->u4GrpAddr == pUpdData->u4GrpAddr */

    }                            /* end of hash scan bucket */

    /* allocate a new mempool block for the new group node */
    if (pGrpNode == NULL)
    {
        MFWD_TRC (MFWD_TRC_FLAG, MFWD_CONTROL_PATH_TRC, MFWD_MOD_NAME,
                  "Group Node does not exist trying to add a new group node\n");
        MFWD_MEMPOOL_ALLOC (MFWD_GRP_NODE_PID, tMfwdGrpNode *,
                            pGrpNode, i4RetCode);

        if (i4RetCode != MFWD_SUCCESS)
        {
            MFWD_TRC (MFWD_TRC_FLAG, MFWD_CONTROL_PATH_TRC, MFWD_MOD_NAME,
                      "Failure in allocating memory for new Group Node\n");
        }
        else
        {
            /* Initialise the new group node data */
            TMO_SLL_Init (&(pGrpNode->SgEntryList));
            IPVX_ADDR_COPY (&pGrpNode->GrpAddr, &pUpdData->GrpAddr);
            pGrpNode->u4GrpMaskLen = pUpdData->u4GrpMaskLen;
            u1NewGrpNodeFlg = TRUE;
        }                        /* end of else of if i4retcode != MFWD_SUCCESS */

    }                            /* end of check if grpnode == NULL */

    /* If an entry insertion point is found only then we can go ahead with the
     * insertion
     */
    if (i4Status != MFWD_FAILURE && pGrpNode != NULL)
    {
        /* Allocate the memory block for the route entry */
        MFWD_MEMPOOL_ALLOC (MFWD_RT_NODE_PID, tMfwdRtEntry *,
                            pNewRtEntry, i4RetCode);
        if (i4RetCode != MFWD_SUCCESS)
        {
            MFWD_DBG (MFWD_DBG_MEM_IF,
                      "Cannot Allocate memory for the new route entry\n");
            i4Status = MFWD_FAILURE;
        }
        else
        {
            /* Allocate the memory pool for the source descriptor node */
            MFWD_MEMPOOL_ALLOC (MFWD_SRC_NODE_PID,
                                tMfwdSrcNode *,
                                (pNewRtEntry->pSrcDescNode), i4RetCode);
            if (i4RetCode != MFWD_SUCCESS)
            {
                MFWD_DBG (MFWD_DBG_MEM_IF, "Cannot Allocate memory for the"
                          "Source node in the new route entry\n");
                /* Release the route entry also to the memory pool */
                MFWD_MEMRELEASE (MFWD_RT_NODE_PID, pNewRtEntry, i4RetCode);
                i4Status = MFWD_FAILURE;
            }
            else
            {
                /* initialise the route entry with the supplied data */
                MfwdMrtInitRtEntry (u2OwnerId, pNewRtEntry, pUpdData);

                /* Add all the oifs that have been specified in the 
                 * oif list
                 */
                i4Status = MfwdMrtAddOifList (pNewRtEntry,
                                              pUpdData->u4OifCnt, pUpdBuf);

                if (i4Status == MFWD_FAILURE)
                {
                    MFWD_TRC (MFWD_TRC_FLAG, MFWD_ALL_FAILURE_TRC,
                              MFWD_MOD_NAME, "Failure in adding the oifs to "
                              "the New entry, Releasing the created entry\n");
                    MfwdMrtReleaseRtEntry (pNewRtEntry);
                }
                else
                {
                    MFWD_TRC (MFWD_TRC_FLAG, MFWD_CONTROL_PATH_TRC,
                              MFWD_MOD_NAME,
                              "Successfully added all the oifs to the New entry\n");
                }
                if (pNewRtEntry->u1RtType == MFWD_STAR_G_ROUTE)
                {
                    /* Add all the iifs that have been specified in the 
                     * iif list
                     */
                    i4Status = MfwdMrtAddIifList (pNewRtEntry,
                                                  pUpdData->u4OifCnt,
                                                  pUpdData->u4IifCnt, pUpdBuf);

                    if (i4Status == MFWD_FAILURE)
                    {
                        MFWD_TRC (MFWD_TRC_FLAG, MFWD_ALL_FAILURE_TRC,
                                  MFWD_MOD_NAME,
                                  "Failure in adding the iifs to "
                                  "the New entry, Releasing the created entry\n");
                        /*MfwdMrtReleaseRtEntry (pNewRtEntry); */
                    }
                    else
                    {
                        MFWD_TRC (MFWD_TRC_FLAG, MFWD_CONTROL_PATH_TRC,
                                  MFWD_MOD_NAME,
                                  "Successfully added all the iifs to the New entry\n");
                    }
                }

            }                    /* end of else i4Status !=  MFWD_SUCCESS */

        }                        /* end of else i4RetCode != MFWD_SUCCESS */

    }                            /* end of if i4Status != MFWD_FAILURE  && ............ */

    if ((i4Status != MFWD_FAILURE) && (pGrpNode != NULL))
    {

        /*
         * if pSgEntry == NULL , it means it is greater than all the route entries
         * present in the SG-Entry List and hence it should be added to the last.
         * and if pSgEntry != NULL, it means the New route entry 's SRC-address is
         *  greater is less than SRC-Address of pSgEntry  and hence it should be added
         *  after the PREVIOUS OF (pSgEntry)
         */
        if (pSgEntry == NULL)
        {
            TMO_SLL_Add (&(pGrpNode->SgEntryList), &pNewRtEntry->SgEntryLink);
        }
        else
        {
            TMO_SLL_Insert (&(pGrpNode->SgEntryList),
                            &pPrevRtEntry->SgEntryLink,
                            &pNewRtEntry->SgEntryLink);
        }

        gMfwdContext.u4RtEntryCount++;

        /* Now add the new group node if created to the hash table */
        if (u1NewGrpNodeFlg)
        {
            /* Add the group node to the hash table */
            TMO_HASH_Add_Node (MFWD_OWNER_MRT (u2OwnerId),
                               &pGrpNode->GrpNodeHashLink,
                               (UINT4) u1HashIndex,
                               (UINT1 *) &(pUpdData->GrpAddr.au1Addr));
            MFWD_TRC (MFWD_TRC_FLAG, MFWD_ALL_FAILURE_TRC, MFWD_MOD_NAME,
                      "Added a new group node to the routing table\n");

        }

        MFWD_TRC (MFWD_TRC_FLAG, MFWD_ALL_FAILURE_TRC, MFWD_MOD_NAME,
                  "Added a new route entry to the group node\n");
        MfwdMdhHandleCacheMissData (u2OwnerId, &pNewRtEntry->OifList,
                                    pUpdData->u4Iif, pUpdData->SrcAddr,
                                    pUpdData->GrpAddr);

    }                            /* end of if i4Status != MFWD_FAILURE  && ............ */

    if (i4RetCode != MFWD_SUCCESS)
    {
        MFWD_TRC (MFWD_TRC_FLAG, MFWD_OS_RESOURCE_TRC, MFWD_MOD_NAME,
                  "Failure in giving the semaphore after the creation "
                  "of route\n");
    }

    /* Check if a new group node was allocated and free it if the entry creation
     * failed
     */
    if ((u1NewGrpNodeFlg) && i4Status == MFWD_FAILURE)
    {
        MFWD_MEMRELEASE (MFWD_GRP_NODE_PID, pGrpNode, i4Status);
    }

    if (i4Status != MFWD_FAILURE && pNewRtEntry != NULL)
    {
        MfwdMrtUpdGetNextList (pNewRtEntry);
    }

    MFWD_DBG (MFWD_DBG_EXIT,
              "Exiting the function to create the route entry\n");

    UNUSED_PARAM (u1EntryType);
    KW_FALSEPOSITIVE_FIX (pNewRtEntry);
    return i4Status;
}                                /* end of the function MfwdMrtCreateRtEntry */

/****************************************************************************
 * Function Name    :  MfwdMrtInitRtEntry
 *
 * Description      :  This function Initialises the route entry 
 *                     
 * Input(s)         :  u2OwnerId :- The Identification value of the owner  
 *                     in which the entry is to be created 
 *                     pNewRtEntry :- pointer to the new Route Entry created 
 *                     pUpdBuf :- Containing the information relating to the
 *                     outgoing interfaces.
 *
 * Output(s)        :  None
 *
 * Global Variables
 * Referred         :  None
 *
 * Global Variables
 * Modified         :  None
 *
 * Return(s)        :  VOID
 ****************************************************************************/

VOID
MfwdMrtInitRtEntry (UINT2 u2OwnerId, tMfwdRtEntry * pNewRtEntry,
                    tMrtUpdData * pUpdData)
{
    pNewRtEntry->u2OwnerId = u2OwnerId;
    IPVX_ADDR_COPY (&pNewRtEntry->GrpAddr, &pUpdData->GrpAddr);
    pNewRtEntry->u4GrpMaskLen = pUpdData->u4GrpMaskLen;
    pNewRtEntry->u1ChkRpf = pUpdData->u1ChkRpfFlag;

    if (pUpdData->u1DeliverMDP == MFWD_MRP_DELIVER_MDP)
    {
        pNewRtEntry->u1DeliverMDP = 1;
    }
    else if (pUpdData->u1DeliverMDP == MFWD_MRP_DONT_DELIVER_MDP)
    {
        pNewRtEntry->u1DeliverMDP = 0;
    }

    pNewRtEntry->u4FwdCnt = 0;
    pNewRtEntry->u4RpfFailCnt = 0;
    pNewRtEntry->pSrcDescNode->u4Iif = pUpdData->u4Iif;
    IPVX_ADDR_COPY (&pNewRtEntry->pSrcDescNode->SrcAddr, &pUpdData->SrcAddr);
    IPVX_ADDR_COPY (&pNewRtEntry->pSrcDescNode->RtAddr, &pUpdData->RtAddr);
    pNewRtEntry->pSrcDescNode->i4RtMask = pUpdData->u4SrcMaskLen;
    IPVX_ADDR_COPY (&pNewRtEntry->pSrcDescNode->UpStrmNbr,
                    &pUpdData->UpStrmNbr);
    OsixGetSysTime (&(pNewRtEntry->u4LastFwdTime));
    TMO_SLL_Init (&(pNewRtEntry->OifList));
    TMO_SLL_Init (&(pNewRtEntry->IifList));
    OsixGetSysTime (&(pNewRtEntry->u4EntryUpTime));
    pNewRtEntry->u1RtType = pUpdData->u1RtType;
}

/****************************************************************************
 * Function Name    :  MfwdMrtUpdateWcRtEntries
 *
 * Description      :  This function Updates the Wild card Route Entries 
 *                     created
 *                     
 * Input(s)         :  u2OwnerId :- The Identification value of the owner  
 *                     in which the entry is to be created 
 *                     u1UpdCmd  :- Command to update the Route Entry
 *                     pUpdBuf :- Containing the information relating to the
 *                     outgoing interfaces.
 *
 * Output(s)        :  None
 *
 * Global Variables
 * Referred         :  None
 *
 * Global Variables
 * Modified         :  None
 *
 * Return(s)        :  VOID
 ****************************************************************************/
VOID
MfwdMrtUpdateWcRtEntries (UINT2 u2OwnerId, UINT1 u1UpdCmd,
                          tMrtUpdData * pUpdData,
                          tCRU_BUF_CHAIN_HEADER * pUpdBuf)
{
    UINT4               u4HashIndex = 0;
    tMfwdGrpNode       *pGrpNode = NULL;

    /* for every group node in the routing table delete the SG entries 
     * which have the route address as RtAddr
     */

    TMO_HASH_Scan_Table (MFWD_OWNER_MRT (u2OwnerId), u4HashIndex)
    {

        TMO_HASH_Scan_Bucket (MFWD_OWNER_MRT (u2OwnerId), u4HashIndex,
                              pGrpNode, tMfwdGrpNode *)
        {

            MfwdMrtUpdateRtEntry (u2OwnerId, u1UpdCmd, pUpdData, pUpdBuf, 0);
            IPVX_ADDR_COPY (&pUpdData->GrpAddr, &pGrpNode->GrpAddr);

        }                        /* End of  for TMO_SLL_Scan */

    }                            /* End of TMO_HASH_Scan_Table ...... */
}

/****************************************************************************
 * Function Name    :  MfwdMrtUpdateRtEntry
 *
 * Description      :  This function deletes the route entry if it exists in 
 *                     the MRT table.
 *                     
 * Input(s)         :  u2OwnerId :- The Identification value of the owner  
 *                     in which the entry is to be updated 
 *                     pUpdData :- pointer to the update data containing the 
 *                     information required for the update.
 *                     pUpdBuf :- Containing the information relating to the
 *                     outgoing interfaces.
 *
 * Output(s)        :  None
 *
 * Global Variables
 * Referred         :  gMfwdContext.pMrtTable 
 *                     (Refered via the macro MFWD_OWNER)
 *
 * Global Variables
 * Modified         :  None
 *
 * Return(s)        :  MFWD_FAILURE or MFWD_SUCCESS
 ****************************************************************************/

INT4
MfwdMrtUpdateRtEntry (UINT2 u2OwnerId, UINT1 u1UpdCmd,
                      tMrtUpdData * pUpdData, tCRU_BUF_CHAIN_HEADER * pUpdBuf,
                      UINT2 u2UpdSubCmd)
{
    INT4                i4Status = MFWD_SUCCESS;
    UINT1               u1HashIndex = 0;
    UINT1               u1EntryType = 0;
    tMfwdGrpNode       *pGrpNode = NULL;
    tMfwdRtEntry       *pSgEntry = NULL;
    tMfwdRtEntry       *pUpdEntry = NULL;
    UINT1               au1NullAddr[16];

    MFWD_DBG (MFWD_DBG_ENTRY,
              "Entering the function to update the route entry\n");

    MEMSET (au1NullAddr, 0, 16);

    /* find the entry type that is to be created */
    MFWD_GET_ENTRY_TYPE (pUpdData->u4GrpMaskLen, pUpdData->u4SrcMaskLen,
                         u1EntryType);
    /* find the hash index of the group address */
    MFWD_GET_HASH_INDEX (pUpdData->GrpAddr, u1HashIndex);

    /* find the group node corresponding to the group address given */
    TMO_HASH_Scan_Bucket (MFWD_OWNER_MRT (u2OwnerId), (UINT4) u1HashIndex,
                          pGrpNode, tMfwdGrpNode *)
    {
        /* If the group node itself does not exist we need to create one
         * and then do the operation
         */
        if (IPVX_ADDR_COMPARE (pUpdData->GrpAddr, pGrpNode->GrpAddr) < 0)
        {
            MFWD_TRC (MFWD_TRC_FLAG, MFWD_CONTROL_PATH_TRC, MFWD_MOD_NAME,
                      "Failure in entry update, Group Node does not exist\n");
            i4Status = MFWD_FAILURE;
            break;
        }                        /* end of if pUpdData.u4GrpAddr < pGrpNode->u4GrpAddr */

        /* If the group address matches then see if the entry exists if it 
         * does then perform the update on this entry
         */
        if ((IPVX_ADDR_COMPARE (pUpdData->GrpAddr, pGrpNode->GrpAddr) != 0) &&
            (pUpdData->u4GrpMaskLen != pGrpNode->u4GrpMaskLen))
        {
            continue;
        }

        /* find the route entry in the list of (S, G) entries
         * of the group node
         */
        TMO_SLL_Scan (&(pGrpNode->SgEntryList), pSgEntry, tMfwdRtEntry *)
        {
            /* If the source address is zero do the updates based on the 
             * rtaddress otherwise based on the source address do the update
             */

            if ((IPVX_ADDR_COMPARE
                 (pSgEntry->pSrcDescNode->SrcAddr, pUpdData->SrcAddr) == 0)
                || ((MEMCMP (pUpdData->SrcAddr.au1Addr, au1NullAddr, 16) == 0)
                    &&
                    (IPVX_ADDR_COMPARE
                     (pSgEntry->pSrcDescNode->RtAddr, pUpdData->RtAddr) == 0)))
            {
                /* entry already exists return failure */
                pUpdEntry = pSgEntry;
            }
            else
            {
                continue;
            }

            if (pUpdEntry != NULL)
            {
                if (u1UpdCmd & MFWD_ENTRY_UPD_DELIVERMDP_FLAG)
                {
                    if (pUpdData->u1DeliverMDP == MFWD_MRP_DELIVER_MDP)
                    {
                        pUpdEntry->u1DeliverMDP++;
                    }
                    else if (pUpdData->u1DeliverMDP ==
                             MFWD_MRP_DONT_DELIVER_MDP)
                    {
                        if (pUpdEntry->u1DeliverMDP != 0)
                        {
                            pUpdEntry->u1DeliverMDP--;
                        }
                    }

                }

                /* delete oif only can be a combination with the update 
                 * iif so check if update iif is also set along with 
                 * delete oif and update the iif
                 */
                if (u1UpdCmd & MFWD_ENTRY_UPDATE_IIF_CMD)
                {
                    MfwdMrtUpdateIif (u2OwnerId, pUpdEntry, pUpdData, pUpdBuf);
                }

                /* If the delete oif command is set then call the 
                 * delete oif list 
                 */

                if (u1UpdCmd & MFWD_ENTRY_DELETE_OIF_CMD)
                {
                    i4Status = MfwdMrtDeleteOifList (pUpdEntry,
                                                     pUpdData->u4OifCnt,
                                                     pUpdBuf);
                }                /* if (u1UpdCmd & MFWD_ENTRY_DELETE_OIF_CMD) */
                else
                {
                    if (u1UpdCmd & MFWD_ENTRY_ADD_OIF_CMD)
                    {
                        i4Status = MfwdMrtAddOifList (pUpdEntry,
                                                      pUpdData->u4OifCnt,
                                                      pUpdBuf);

                    }
                    else
                    {
                        if (u1UpdCmd & MFWD_ENTRY_OIF_STATE_CMD)
                        {
                            i4Status = MfwdMrtSetOifState (pUpdEntry,
                                                           pUpdData->u4OifCnt,
                                                           pUpdBuf);

                        }        /* end of if (u1UpdCmd & MFWD_ENTRY_OIF_... */

                    }            /* end of else u1UpdCmd & MFWD_ENTRY_ADD_OIF_.. */

                }                /* end of if (u1UpdCmd & MFWD_ENTRY_DELETE_OIF_CMD) */
                /* If the delete iif command is set then call the 
                 * delete iif list 
                 */
                if ((u1UpdCmd & MFWD_ENTRY_UPDATE_IIFLIST_CMD) &&
                    (pUpdEntry->u1RtType == MFWD_STAR_G_ROUTE))
                {
                    if (u2UpdSubCmd & MFWD_ENTRY_DELETE_IIF_CMD)
                    {

                        i4Status = MfwdMrtDeleteIifList (pUpdEntry, 0,
                                                         pUpdData->u4IifCnt,
                                                         pUpdBuf);
                    }
                    if (u2UpdSubCmd & MFWD_ENTRY_ADD_IIF_CMD)
                    {
                        i4Status = MfwdMrtAddIifList (pUpdEntry, 0,
                                                      pUpdData->u4IifCnt,
                                                      pUpdBuf);
                    }            /* if (u1UpdCmd & MFWD_ENTRY_DELETE_IIF_CMD) */
                }
                if (MEMCMP (pUpdData->SrcAddr.au1Addr, au1NullAddr, 16) != 0)
                {
                    break;
                }

            }                    /* end of if i4Status != MFWD_FAILURE */

        }                        /* end of TMO_SLL_SCAN */

    }                            /* end of hash scan bucket */

    MFWD_DBG (MFWD_DBG_EXIT,
              "Exiting the function to update the route entry\n");

    UNUSED_PARAM (u1EntryType);

    return i4Status;
}                                /* end of the function MfwdMrtCreateRtEntry */

/****************************************************************************
 * Function Name    :  MfwdMrtUpdateIif
 *
 * Description      :  This function updates the incoming interface of the 
 *                     route entry with the new incoming interface
 *                     It also updates the network processor for the route 
 *                     entry if the mode of MRP is dense mode.
 *                     
 * Input(s)         :  u2OwnerId :- The Identification value of the owner  
 *                     in which the entry is to be updated 
 *
 *                     pUpdEntry :- Pointer to the entry which is to be updated
 *
 *                     pUpdBuf :- Containing the information relating to the
 *                     update date.
 *                     
 *
 * Output(s)        :  None
 *
 * Global Variables
 * Referred         :  gMfwdContext
 *                     (Refered via the macro MFWD_OWNER)
 *
 * Global Variables
 * Modified         :  None
 *
 * Return(s)        :  MFWD_FAILURE or MFWD_SUCCESS
 ****************************************************************************/

void
MfwdMrtUpdateIif (UINT2 u2OwnerId, tMfwdRtEntry * pUpdEntry,
                  tMrtUpdData * pUpdData, tCRU_BUF_CHAIN_HEADER * pUpdBuf)
{
    tMrtIifUpdData      IifData;
    INT4                i4Status;

    MFWD_DBG (MFWD_DBG_ENTRY,
              "Entering the function to update the IIF of route entry\n");

    i4Status = CRU_BUF_Copy_FromBufChain (pUpdBuf, (UINT1 *) &IifData,
                                          sizeof (tMrtUpdData),
                                          sizeof (tMrtIifUpdData));

    if (i4Status == CRU_FAILURE)
    {
        return;
    }

    if (MFWD_OWNER_MODE (u2OwnerId) == MFWD_MRP_SPARSE_MODE)
    {
        IPVX_ADDR_COPY (&pUpdEntry->pSrcDescNode->RtAddr, &IifData.NewSrcAddr);
    }

    IPVX_ADDR_COPY (&pUpdEntry->pSrcDescNode->UpStrmNbr, &IifData.UpStrmNewNbr);

    pUpdEntry->pSrcDescNode->u4Iif = IifData.u4Iif;
    pUpdEntry->u1ChkRpf = pUpdData->u1ChkRpfFlag;

    MFWD_DBG (MFWD_DBG_EXIT,
              "Exiting the function to update the IIF of route entry\n");

}                                /* end of the function MfwdMrtUpdateIif */

/****************************************************************************
 * Function Name    :  MfwdMrtAddOifList
 *
 * Description      :  This function adds a list of oifs to the route entry and
 *                     adds nexthop node to the oif.
 *                     
 * Input(s)         :  pUpdEntry :- The route entry to be updated
 *                     urOifCount :- Number of oifs present in the update 
 *                     message
 *                     pUpdBuf :- Containing the information relating to the
 *                     outgoing interfaces.
 *
 * Output(s)        :  None
 *
 * Global Variables
 * Referred         :  
 *
 * Global Variables
 * Modified         :  None
 *
 * Return(s)        :  MFWD_FAILURE or MFWD_SUCCESS
 ****************************************************************************/

#ifdef __STDC__
INT4
MfwdMrtAddOifList (tMfwdRtEntry * pUpdEntry,
                   UINT4 u4OifCount, tCRU_BUF_CHAIN_HEADER * pUpdBuf)
#else
INT4
MfwdMrtAddOifList (pUpdEntry, u4OifCount, pUpdBuf)
     tMfwdRtEntry       *pUpdEntry;
     UINT4               u4OifCount;
     tCRU_BUF_CHAIN_HEADER *pUpdBuf;
#endif
{
    INT4                i4Status = MFWD_SUCCESS;
    INT4                i4RetCode = MFWD_SUCCESS;
    UINT4               u4OifCtr = 0;
    tMrpOifNode         OifUpdData;
    tMfwdOifNode       *pOifNode = NULL;
    tMfwdOifNextHopNode *pNextHopNode = NULL;

    MFWD_DBG (MFWD_DBG_ENTRY,
              "Entering the function to add oifs to the route entry\n");

    /* Add each of the oif nodes present in the  update buffer to the route 
     * entry
     */

    while ((u4OifCtr < u4OifCount) &&
           (CRU_BUF_Copy_FromBufChain (pUpdBuf, (UINT1 *) &OifUpdData,
                                       MFWD_OIFINFO_MESSAGE_OFFSET (u4OifCtr),
                                       sizeof (tMrpOifNode)) ==
            sizeof (tMrpOifNode)))
    {
        /* Check and see if such an oif already does not exist */
        TMO_SLL_Scan (&(pUpdEntry->OifList), pOifNode, tMfwdOifNode *)
        {
            if (pOifNode->u4OifIndex == OifUpdData.u4OifIndex)
            {
                i4RetCode = MFWD_FAILURE;
                break;
            }
        }

        if (i4RetCode == MFWD_FAILURE)
        {
            MFWD_TRC_ARG1 (MFWD_TRC_FLAG, MFWD_CONTROL_PATH_TRC, MFWD_MOD_NAME,
                           "Oif with the index %d already exists in the entry \n",
                           OifUpdData.u4OifIndex);
            i4RetCode = MFWD_SUCCESS;
            u4OifCtr++;
            continue;
        }
        /* Allocate a memory block for the new oif node that is to be added
         */
        MFWD_MEMPOOL_ALLOC (MFWD_OIF_NODE_PID, tMfwdOifNode *,
                            pOifNode, i4RetCode);
        if (i4RetCode != MFWD_SUCCESS)
        {
            MFWD_DBG1 (MFWD_DBG_MEM_IF, "Failure in allocating memory block"
                       " for the oif node %d\n", OifUpdData.u4OifIndex);
            i4Status = MFWD_FAILURE;
            break;
        }
        else
        {
            MFWD_DBG1 (MFWD_DBG_MEM_IF, "Allocated memory block for the oif"
                       " node %d\n", OifUpdData.u4OifIndex);

            /* Allocate the memory block for the next hop node */
            MFWD_MEMPOOL_ALLOC (MFWD_NEXTHOP_NODE_PID,
                                tMfwdOifNextHopNode *, pNextHopNode, i4RetCode);
            if (i4RetCode != MFWD_SUCCESS)
            {
                MFWD_MEMRELEASE (MFWD_OIF_NODE_PID, pOifNode, i4RetCode);
                MFWD_DBG1 (MFWD_DBG_MEM_IF, "Failure in allocating memory"
                           "block for the NextHop node of the oif %d\n",
                           OifUpdData.u4OifIndex);
                i4Status = MFWD_FAILURE;
                break;
            }                    /* end of i4Status != MFWD_SUCCESS */

            pOifNode->u4OifState = OifUpdData.u4NextHopState;
            pOifNode->u4OifIndex = OifUpdData.u4OifIndex;
            pOifNode->u4FwdCnt = 0;
            IPVX_ADDR_COPY (&pNextHopNode->NextHopAddr,
                            &OifUpdData.NextHopAddr);
            pNextHopNode->u4NextHopState = OifUpdData.u4NextHopState;

            OsixGetSysTime (&pNextHopNode->u4NextHopUpTime);

            /* Initialise the next hop list of the oif node and add the 
             * next hop node to the next hop list
             */
            TMO_SLL_Init (&(pOifNode->NextHopList));
            TMO_SLL_Add (&(pOifNode->NextHopList), &pNextHopNode->NextHopNode);

            /* Add the oif node to the oif list of the route entry */
            TMO_SLL_Add (&(pUpdEntry->OifList), &pOifNode->NextOifNode);

            MFWD_TRC_ARG1 (MFWD_TRC_FLAG, MFWD_CONTROL_PATH_TRC, MFWD_MOD_NAME,
                           "Added a new oif %d to the entry \n",
                           OifUpdData.u4OifIndex);
        }                        /* end of i4Status != MFWD_SUCCESS */

        /* Increment the oif counter for the next oif update info in the message
         */
        u4OifCtr++;
    }                            /* end of while loop */

    MFWD_DBG (MFWD_DBG_EXIT,
              "Exiting the function to add oifs to the route entry\n");
    return i4Status;
}                                /* end of function MfwdMrtAddOifs */

/****************************************************************************
 * Function Name    :  MfwdMrtAddIifList
 *
 * Description      :  This function adds a list of iifs to the route entry and
 *                     adds nexthop node to the iif.
 *                     
 * Input(s)         :  pUpdEntry :- The route entry to be updated
 *                     urIifCount :- Number of iifs present in the update 
 *                     message
 *                     pUpdBuf :- Containing the information relating to the
 *                     incoming interfaces.
 *
 * Output(s)        :  None
 *
 * Global Variables
 * Referred         :  
 *
 * Global Variables
 * Modified         :  None
 *
 * Return(s)        :  MFWD_FAILURE or MFWD_SUCCESS
 ****************************************************************************/

#ifdef __STDC__
INT4
MfwdMrtAddIifList (tMfwdRtEntry * pUpdEntry,
                   UINT4 u4OifCount, UINT4 u4IifCount,
                   tCRU_BUF_CHAIN_HEADER * pUpdBuf)
#else
INT4
MfwdMrtAddIifList (pUpdEntry, u4OifCount, u4IifCount, pUpdBuf)
     tMfwdRtEntry       *pUpdEntry;
     UINT4               u4IifCount;
     tCRU_BUF_CHAIN_HEADER *pUpdBuf;
#endif
{
    INT4                i4Status = MFWD_SUCCESS;
    INT4                i4RetCode = MFWD_SUCCESS;
    UINT4               u4IifCtr = 0;
    tMrpIifNode         IifUpdData;
    tMfwdIifNode       *pIifNode = NULL;

    MFWD_DBG (MFWD_DBG_ENTRY,
              "Entering the function to add iifs to the route entry\n");

    /* Add each of the iif nodes present in the  update buffer to the route 
     * entry
     */

    while ((u4IifCtr < u4IifCount) &&
           (CRU_BUF_Copy_FromBufChain (pUpdBuf, (UINT1 *) &IifUpdData,
                                       MFWD_IIFINFO_MESSAGE_OFFSET (u4OifCount,
                                                                    u4IifCtr),
                                       sizeof (tMrpIifNode)) ==
            sizeof (tMrpIifNode)))
    {
        /* Check and see if such an iif already does not exist */
        TMO_SLL_Scan (&(pUpdEntry->IifList), pIifNode, tMfwdIifNode *)
        {
            if (pIifNode->u4IifIndex == IifUpdData.u4IifIndex)
            {
                i4RetCode = MFWD_FAILURE;
                break;
            }
        }

        if (i4RetCode == MFWD_FAILURE)
        {
            MFWD_TRC_ARG1 (MFWD_TRC_FLAG, MFWD_CONTROL_PATH_TRC, MFWD_MOD_NAME,
                           "Iif with the index %d already exists in the entry \n",
                           IifUpdData.u4IifIndex);
            i4RetCode = MFWD_SUCCESS;
            u4IifCtr++;
            continue;
        }
        /* Allocate a memory block for the new iif node that is to be added
         */
        MFWD_MEMPOOL_ALLOC (MFWD_IIF_NODE_PID, tMfwdIifNode *,
                            pIifNode, i4RetCode);
        if (i4RetCode != MFWD_SUCCESS)
        {
            MFWD_DBG1 (MFWD_DBG_MEM_IF, "Failure in allocating memory block"
                       " for the Iif node %d\n", IifUpdData.u4IifIndex);
            i4Status = MFWD_FAILURE;
            break;
        }
        else
        {
            MFWD_DBG1 (MFWD_DBG_MEM_IF, "Allocated memory block for the iif"
                       " node %d\n", IifUpdData.u4IifIndex);

            pIifNode->u4IifIndex = IifUpdData.u4IifIndex;
            pIifNode->u4FwdCnt = 0;

            /* Add the iif node to the iif list of the route entry */
            TMO_SLL_Add (&(pUpdEntry->IifList), &pIifNode->NextIifNode);

            MFWD_TRC_ARG1 (MFWD_TRC_FLAG, MFWD_CONTROL_PATH_TRC, MFWD_MOD_NAME,
                           "Added a new iif %d to the entry \n",
                           IifUpdData.u4IifIndex);
        }                        /* end of i4Status != MFWD_SUCCESS */

        /* Increment the Iif counter for the next Iif update info in the message
         */
        u4IifCtr++;
    }                            /* end of while loop */

    MFWD_DBG (MFWD_DBG_EXIT,
              "Exiting the function to add iifs to the route entry\n");
    return i4Status;
}                                /* end of function MfwdMrtAddIifs */

/****************************************************************************
 * Function Name    :  MfwdMrtDeleteOifList
 *
 * Description      :  This function deletes the oifs from the route entry and
 *                     also frees their next hop nodes to the memory pools
 *                     
 * Input(s)         :  pUpdEntry :- The route entry to be updated
 *                     u4OifCount :- Number of oifs present in the update 
 *                     message
 *                     pUpdBuf :- Containing the information relating to the
 *                     outgoing interfaces.
 *
 * Output(s)        :  None
 *
 * Global Variables
 * Referred         :  
 *
 * Global Variables
 * Modified         :  None
 *
 * Return(s)        :  MFWD_FAILURE or MFWD_SUCCESS
 ****************************************************************************/

#ifdef __STDC__
INT4
MfwdMrtDeleteOifList (tMfwdRtEntry * pUpdEntry,
                      UINT4 u4OifCount, tCRU_BUF_CHAIN_HEADER * pUpdBuf)
#else
INT4
MfwdMrtDeleteOifList (pUpdEntry, u4OifCount, pUpdBuf)
     UINT2               u2OwnerId;
     tMfwdRtEntry       *pUpdEntry;
     UINT4               u4OifCount;
     tCRU_BUF_CHAIN_HEADER *pUpdBuf;
#endif
{
    INT4                i4Status = MFWD_SUCCESS;
    INT4                i4RetCode = MFWD_SUCCESS;
    UINT4               u4OifCtr = 0;
    tMrpOifNode         OifUpdData;
    tMfwdOifNode       *pOifNode = NULL;

    MFWD_DBG (MFWD_DBG_ENTRY,
              "Entering the function to delete a list of oifs from the route entry\n");

    /* delete each of the oif nodes present in the  update buffer from the route 
     * entry
     */

    while ((u4OifCtr < u4OifCount) &&
           (CRU_BUF_Copy_FromBufChain (pUpdBuf, (UINT1 *) &OifUpdData,
                                       MFWD_OIFINFO_MESSAGE_OFFSET (u4OifCtr),
                                       sizeof (tMrpOifNode)) ==
            sizeof (tMrpOifNode)))
    {
        TMO_SLL_Scan (&(pUpdEntry->OifList), pOifNode, tMfwdOifNode *)
        {
            if (pOifNode->u4OifIndex == OifUpdData.u4OifIndex)
            {
                break;
            }
        }                        /* end of TMO_SLL_SCAN of oif list */

        if (pOifNode != NULL)
        {
            i4RetCode = MfwdMrtDeleteOif (pUpdEntry, pOifNode);

            if (i4RetCode == MFWD_FAILURE)
            {
                /* Deletion failed in slow path shouldnt cause deletion in
                 * fast path. */

                MFWD_TRC_ARG1 (MFWD_TRC_FLAG, MFWD_CONTROL_PATH_TRC,
                               MFWD_MOD_NAME,
                               "Failure in deleting the OIF Node %d",
                               pOifNode->u4OifIndex);
                i4Status = MFWD_FAILURE;
            }

        }                        /* end of if pPrevOifNode != NULL */
        else
        {
            MFWD_TRC_ARG1 (MFWD_TRC_FLAG, MFWD_CONTROL_PATH_TRC, MFWD_MOD_NAME,
                           "Oif with the index %d does not exist in the entry \n",
                           OifUpdData.u4OifIndex);
        }

        /* Increment the oif counter for the next oif update info in the message
         */
        u4OifCtr++;

    }                            /* end of while loop */

    if (u4OifCtr < u4OifCount)
    {
        MFWD_TRC_ARG2 (MFWD_TRC_FLAG, MFWD_CONTROL_PATH_TRC, MFWD_MOD_NAME,
                       "Failure in reading buffer for the update read %d "
                       "oifs out of %d\n", u4OifCtr, u4OifCount);
        i4Status = MFWD_FAILURE;
    }

    MFWD_DBG (MFWD_DBG_EXIT,
              "Entering the function to delete a list of oifs from the route entry\n");
    return i4Status;
}                                /* end of function MfwdMrtDeletOifList */

/****************************************************************************
 * Function Name    :  MfwdMrtDeleteOif
 *
 * Description      :  This function deletes the given oif node from the oif 
 *                     list of the route entry and also frees its next hop
 *                     list.
 *                     
 * Input(s)         :  pRtEntry :- The route entry from which the oif is to
 *                     be deleted
 *                     pOifNode :- The oif node that is to be deleted
 *
 * Output(s)        :  None
 *
 * Global Variables
 * Referred         :  
 *
 * Global Variables
 * Modified         :  None
 *
 * Return(s)        :  MFWD_FAILURE or MFWD_SUCCESS
 ****************************************************************************/

#ifdef __STDC__
INT4
MfwdMrtDeleteOif (tMfwdRtEntry * pRtEntry, tMfwdOifNode * pOifNode)
#else
INT4
MfwdMrtDeleteOif (pRtEntry, pOifNode)
     tMfwdRtEntry       *pRtEntry;
     tMfwdOifNode       *pOifNode;
#endif
{
    INT4                i4RetCode = MFWD_SUCCESS;
    INT4                i4Status = MFWD_SUCCESS;
    tMfwdOifNextHopNode *pNextHopNode = NULL;

    MFWD_DBG1 (MFWD_DBG_ENTRY,
               "Entering the function to delete oif %d from the route entry\n",
               pOifNode->u4OifIndex);

    /* Free all the next hop nodes in the oif list to the memory pool */
    while ((pNextHopNode =
            (tMfwdOifNextHopNode *) TMO_SLL_First (&pOifNode->NextHopList)) !=
           NULL)
    {
        TMO_SLL_Delete ((&pOifNode->NextHopList), &pNextHopNode->NextHopNode);
        MFWD_MEMRELEASE (MFWD_NEXTHOP_NODE_PID, pNextHopNode, i4RetCode);
        if (i4RetCode != MFWD_SUCCESS)
        {
            MFWD_DBG1 (MFWD_DBG_MEM_IF, "Failure in releasing memory "
                       "block of the NextHop node of the oif %d\n",
                       pOifNode->u4OifIndex);
        }
    }                            /* end of while loop */

    /* Now delete the oif node from the oif list of the route entry and 
     * free it to the memory pool
     */
    TMO_SLL_Delete (&pRtEntry->OifList, &pOifNode->NextOifNode);
    MFWD_MEMRELEASE (MFWD_OIF_NODE_PID, pOifNode, i4RetCode);
    if (i4RetCode != MFWD_SUCCESS)
    {
        MFWD_DBG1 (MFWD_DBG_MEM_IF, "Failure in releasing memory "
                   "block of the oif node %d\n", pOifNode->u4OifIndex);
        i4Status = MFWD_FAILURE;
    }

    MFWD_DBG (MFWD_DBG_EXIT,
              "Exiting the function to delete a oif from the route entry\n");
    return i4Status;
}                                /* end of function MfwdMrtAddOifs */

/****************************************************************************
 * Function Name    :  MfwdMrtDeleteIifList
 *
 * Description      :  This function deletes the Iifs from the route entry 
 *                     
 * Input(s)         :  pUpdEntry :- The route entry to be updated
 *                     u4IifCount :- Number of iifs present in the update 
 *                     message
 *                     pUpdBuf :- Containing the information relating to the
 *                     outgoing interfaces.
 *
 * Output(s)        :  None
 *
 * Global Variables
 * Referred         :  
 *
 * Global Variables
 * Modified         :  None
 *
 * Return(s)        :  MFWD_FAILURE or MFWD_SUCCESS
 ****************************************************************************/

#ifdef __STDC__
INT4
MfwdMrtDeleteIifList (tMfwdRtEntry * pUpdEntry,
                      UINT4 u4OifCount, UINT4 u4IifCount,
                      tCRU_BUF_CHAIN_HEADER * pUpdBuf)
#else
INT4
MfwdMrtDeleteIifList (pUpdEntry, u4OifCount, u4IifCount, pUpdBuf)
     UINT2               u2OwnerId;
     tMfwdRtEntry       *pUpdEntry;
     UINT4               u4IifCount;
     tCRU_BUF_CHAIN_HEADER *pUpdBuf;
#endif
{
    INT4                i4Status = MFWD_SUCCESS;
    INT4                i4RetCode = MFWD_SUCCESS;
    UINT4               u4IifCtr = 0;
    tMrpIifNode         IifUpdData;
    tMfwdIifNode       *pIifNode = NULL;

    MFWD_DBG (MFWD_DBG_ENTRY,
              "Entering the function to delete a list of iifs from the route entry\n");

    /* delete each of the iif nodes present in the  update buffer from the route 
     * entry
     */

    while ((u4IifCtr < u4IifCount) &&
           (CRU_BUF_Copy_FromBufChain (pUpdBuf, (UINT1 *) &IifUpdData,
                                       MFWD_IIFINFO_MESSAGE_OFFSET (u4OifCount,
                                                                    u4IifCtr),
                                       sizeof (tMrpIifNode)) ==
            sizeof (tMrpIifNode)))
    {
        TMO_SLL_Scan (&(pUpdEntry->IifList), pIifNode, tMfwdIifNode *)
        {
            if (pIifNode->u4IifIndex == IifUpdData.u4IifIndex)
            {
                break;
            }
        }                        /* end of TMO_SLL_SCAN of iif list */

        if (pIifNode != NULL)
        {
            i4RetCode = MfwdMrtDeleteIif (pUpdEntry, pIifNode);

            if (i4RetCode == MFWD_FAILURE)
            {
                /* Deletion failed in slow path shouldnt cause deletion in
                 * fast path. */

                MFWD_TRC_ARG1 (MFWD_TRC_FLAG, MFWD_CONTROL_PATH_TRC,
                               MFWD_MOD_NAME,
                               "Failure in deleting the IIF Node %d",
                               pIifNode->u4IifIndex);
                i4Status = MFWD_FAILURE;
            }

        }                        /* end of if pPrevIifNode != NULL */
        else
        {
            MFWD_TRC_ARG1 (MFWD_TRC_FLAG, MFWD_CONTROL_PATH_TRC, MFWD_MOD_NAME,
                           "Iif with the index %d does not exist in the entry \n",
                           IifUpdData.u4IifIndex);
        }

        /* Increment the Iif counter for the next iif update info in the message
         */
        u4IifCtr++;

    }                            /* end of while loop */

    if (u4IifCtr < u4IifCount)
    {
        MFWD_TRC_ARG2 (MFWD_TRC_FLAG, MFWD_CONTROL_PATH_TRC, MFWD_MOD_NAME,
                       "Failure in reading buffer for the update read %d "
                       "iifs out of %d\n", u4IifCtr, u4IifCount);
        i4Status = MFWD_FAILURE;
    }

    MFWD_DBG (MFWD_DBG_EXIT,
              "Entering the function to delete a list of iifs from the route entry\n");
    return i4Status;
}                                /* end of function MfwdMrtDeletIifList */

/****************************************************************************
 * Function Name    :  MfwdMrtDeleteIif
 *
 * Description      :  This function deletes the given iif node from the iif 
 *                     list of the route entry
 *                     
 * Input(s)         :  pRtEntry :- The route entry from which the iif is to
 *                     be deleted
 *                     pIifNode :- The iif node that is to be deleted
 *
 * Output(s)        :  None
 *
 * Global Variables
 * Referred         :  
 *
 * Global Variables
 * Modified         :  None
 *
 * Return(s)        :  MFWD_FAILURE or MFWD_SUCCESS
 ****************************************************************************/

#ifdef __STDC__
INT4
MfwdMrtDeleteIif (tMfwdRtEntry * pRtEntry, tMfwdIifNode * pIifNode)
#else
INT4
MfwdMrtDeleteIif (pRtEntry, pIifNode)
     tMfwdRtEntry       *pRtEntry;
     tMfwdIifNode       *pIifNode;
#endif
{
    INT4                i4RetCode = MFWD_SUCCESS;
    INT4                i4Status = MFWD_SUCCESS;

    MFWD_DBG1 (MFWD_DBG_ENTRY,
               "Entering the function to delete Iif %d from the route entry\n",
               pIifNode->u4IifIndex);

    /* Now delete the iif node from the iif list of the route entry and 
     * free it to the memory pool
     */
    TMO_SLL_Delete (&pRtEntry->IifList, &pIifNode->NextIifNode);
    MFWD_MEMRELEASE (MFWD_IIF_NODE_PID, pIifNode, i4RetCode);
    if (i4RetCode != MFWD_SUCCESS)
    {
        MFWD_DBG1 (MFWD_DBG_MEM_IF, "Failure in releasing memory "
                   "block of the iif node %d\n", pIifNode->u4IifIndex);
        i4Status = MFWD_FAILURE;
    }

    MFWD_DBG (MFWD_DBG_EXIT,
              "Exiting the function to delete a iif from the route entry\n");
    return i4Status;
}                                /* end of function MfwdMrtDeleteIif */

/****************************************************************************
 * Function Name    :  MfwdMrtSetOifState
 *
 * Description      :  This function updates the state of the oif to the 
 *                     downstream members.
 *                     
 * Input(s)         :  pUpdEntry :- The route entry to be updated
 *                     u4OifCount :- Number of oifs present in the update 
 *                     message
 *                     pUpdBuf :- buffer Containing the information relating 
 *                     to the outgoing interfaces.
 *
 * Output(s)        :  None
 *
 * Global Variables
 * Referred         :  None
 *
 * Global Variables
 * Modified         :  None
 *
 * Return(s)        :  MFWD_FAILURE or MFWD_SUCCESS
 ****************************************************************************/

#ifdef __STDC__
INT4
MfwdMrtSetOifState (tMfwdRtEntry * pUpdEntry,
                    UINT4 u4OifCount, tCRU_BUF_CHAIN_HEADER * pUpdBuf)
#else
INT4
MfwdMrtSetOifState (pUpdEntry, u4OifCount, pUpdBuf)
     tMfwdRtEntry       *pUpdEntry;
     UINT4               u4OifCount;
     tCRU_BUF_CHAIN_HEADER *pUpdBuf;
#endif
{
    INT4                i4Status = MFWD_SUCCESS;
    INT4                i4RetCode = MFWD_SUCCESS;
    UINT4               u4OifCtr = 0;
    tMrpOifNode         OifUpdData;
    tMfwdOifNode       *pOifNode = NULL;
    tMfwdOifNextHopNode *pNextHopNode = NULL;
    tMfwdOifNextHopNode *pPrevNextHopNode = NULL;

    MFWD_DBG (MFWD_DBG_ENTRY,
              "Entering the function to update next hop stae of oifs\n");

    /* delete each of the oif nodes present in the  update buffer from the route 
     * entry
     */
    while ((u4OifCtr < u4OifCount) &&
           (CRU_BUF_Copy_FromBufChain (pUpdBuf, (UINT1 *) &OifUpdData,
                                       MFWD_OIFINFO_MESSAGE_OFFSET (u4OifCtr),
                                       sizeof (tMrpOifNode)) ==
            sizeof (tMrpOifNode)))
    {
        /* scan the oif list for each oif update present in the buffer
         * and update the buffer accordingly
         */
        TMO_SLL_Scan (&pUpdEntry->OifList, pOifNode, tMfwdOifNode *)
        {

            if (pOifNode->u4OifIndex == OifUpdData.u4OifIndex)
            {
                /* this is the interface where the nexthop node is to be
                 * updated, see if the nexthop node already exists if it does
                 * not exist then update that node. If it does not exist
                 * then create a new nexthop node and add to the list
                 * of nexthops for this oif
                 */
                pOifNode->u4OifState = OifUpdData.u4NextHopState;
                TMO_SLL_Scan (&pOifNode->NextHopList, pNextHopNode,
                              tMfwdOifNextHopNode *)
                {
                    if (IPVX_ADDR_COMPARE
                        (pNextHopNode->NextHopAddr, OifUpdData.NextHopAddr) > 0)
                    {
                        break;
                    }

                    if (IPVX_ADDR_COMPARE
                        (pNextHopNode->NextHopAddr,
                         OifUpdData.NextHopAddr) == 0)
                    {
                        pNextHopNode->u4NextHopState =
                            OifUpdData.u4NextHopState;
                        i4Status = MFWD_SUCCESS;
                        break;
                    }
                    pPrevNextHopNode = pNextHopNode;
                }

                /* If the next hop node does not exist create one and add */
                if ((pNextHopNode == NULL) ||
                    (IPVX_ADDR_COMPARE
                     (pNextHopNode->NextHopAddr, OifUpdData.NextHopAddr) != 0))
                {
                    MFWD_MEMPOOL_ALLOC (MFWD_NEXTHOP_NODE_PID,
                                        tMfwdOifNextHopNode *,
                                        pNextHopNode, i4RetCode);
                    if (i4RetCode != MFWD_SUCCESS)
                    {
                        MFWD_DBG1 (MFWD_DBG_MEM_IF,
                                   "Failure in allocating memory block"
                                   "for new NextHop node of the oif %d",
                                   OifUpdData.u4OifIndex);
                        i4Status = MFWD_FAILURE;
                    }
                    else
                    {
                        IPVX_ADDR_COPY (&pNextHopNode->NextHopAddr,
                                        &OifUpdData.NextHopAddr);
                        pNextHopNode->u4NextHopState =
                            OifUpdData.u4NextHopState;

                        OsixGetSysTime (&pNextHopNode->u4NextHopUpTime);

                        TMO_SLL_Insert (&pOifNode->NextHopList,
                                        &pPrevNextHopNode->NextHopNode,
                                        &pNextHopNode->NextHopNode);
                        i4Status = MFWD_SUCCESS;
                    }
                }                /* if ((pNextHopNode == NULL) || ......... */

            }                    /* end of if pOifNode->u4OifState == OifUpdData.u4OifIndex */

        }                        /* end of TMO_SLL_SCAN of oif list */

        /* Increment the oif counter for the next oif update info in the message
         */
        u4OifCtr++;

    }                            /* end of while loop */

    MFWD_DBG (MFWD_DBG_EXIT,
              "Exiting the function to update next hop stae of oifs\n");
    return i4Status;
}                                /* end of function MfwdMrtDeletOifList */

/****************************************************************************
 * Function Name    :  MfwdMrtGetFwdInfo
 *
 * Description      :  This function finds out a longest matching route entry
 *                     based on the given source address and group address.
 *                     If such an entry exists it matches the incoming
 *                     interface with the incoming interface supplied to the 
 *                     function based on the result it returns a cache-miss
 *                     wrong-iif or a cache-hit to the calling function.
 *                     !!!!!NOTE!!!!!
 *                     THIS FUNCTION DOES NOT TAKE A LOCK ON THE OWNER 
 *                     INFORMATION NODE IT IS THE RESPONSIBILTY OF THE 
 *                     CALLING FUNCTION TO TAKE THE SEMAPHORE.
 *                     
 * Input(s)         :  u2OwnerId :- The Identification value of the owner  
 *                     within whose MRT the entry is to be located.
 *                     u4SrcAddr :- The source address of the Multicast Data
 *                     Packet
 *                     u4GrpAddr :- The group address of the Multicast Data
 *                     packet.
 *                     u4Iif :- The incoming interface through which the 
 *                     Multicast data packet was received.
 *
 * Output(s)        :  pMfwdFwdInfo :- Strucure containing the information 
 *                     required for forwarding the MDP.
 *
 * Global Variables
 * Referred         :  gMfwdContext.pOwnerInfoTbl 
 *                     (Refered via the macro MFWD_OWNER)
 *
 * Global Variables
 * Modified         :  None
 *
 * Return(s)        :  MFWD_FAILURE or MFWD_SUCCESS
 ****************************************************************************/

INT4
MfwdMrtGetFwdInfo (UINT2 u2OwnerId, tIPvXAddr SrcAddr, tIPvXAddr GrpAddr,
                   UINT4 u4Iif, tMfwdFwdInfo * pMfwdFwdInfo)
{
    INT4                i4Status = MFWD_FAILURE;
    INT4                i4RetCode = MFWD_SUCCESS;
    UINT1               u1EntryState = MFWD_OIF_PRUNED;
    UINT1               u1HashIndex = 0;
    tMfwdGrpNode       *pGrpNode = NULL;
    tMfwdRtEntry       *pSgEntry = NULL;
    tMfwdRtEntry       *pFwdEntry = NULL;
    tMfwdOifNode       *pOifNode = NULL;
    tMfwdIifNode       *pIifNode = NULL;
    tIPvXAddr           TmpGrpAddr;
    UINT1               u1InIntFound = MFWD_FAILURE;    /*Flag to check Incoming Interface */
    tTMO_SLL_NODE      *pRtNode = NULL;
    tTMO_SLL_NODE      *pNxtRtNode = NULL;
    tMfwdRtEntry       *pRtEntry = NULL;

    MFWD_DBG (MFWD_DBG_ENTRY,
              "Entering the function to Obtain the Forwarding information\n");

    MEMSET (&TmpGrpAddr, 0, sizeof (tIPvXAddr));
    IPVX_ADDR_COPY (&TmpGrpAddr, &GrpAddr);
    /* find the hash index of the group address */
    MFWD_GET_HASH_INDEX (GrpAddr, u1HashIndex);

    i4RetCode = MFWD_SUCCESS;
    /* find the group node corresponding to the group address given */
    TMO_HASH_Scan_Bucket (MFWD_OWNER_MRT (u2OwnerId), (UINT4) u1HashIndex,
                          pGrpNode, tMfwdGrpNode *)
    {
        /* If the group node itself does not exist we need to create one
         * and then do the operation
         */
        if (IPVX_ADDR_COMPARE (GrpAddr, pGrpNode->GrpAddr) < 0)
        {
            MFWD_TRC (MFWD_TRC_FLAG, MFWD_CONTROL_PATH_TRC, MFWD_MOD_NAME,
                      "No (S, G) or (*, G), trying to see if (*, *) exists\n");
            break;
        }                        /* end of if pUpdData.u4GrpAddr < pGrpNode->u4GrpAddr */

        /* If the group address matches then see if the entry already 
         * exists if it does not exist then carry the insertion address out
         */
        if (IPVX_ADDR_COMPARE (pGrpNode->GrpAddr, GrpAddr) == 0)
        {
            /* see if we can find a (S, G) entry directly */
            TMO_SLL_Scan (&(pGrpNode->SgEntryList), pSgEntry, tMfwdRtEntry *)
            {
                /*No need to Compare SrcAddr for STAR_G Route */
                if ((pSgEntry->u1RtType != MFWD_STAR_G_ROUTE)
                    || (TMO_SLL_Count (&(pSgEntry->IifList)) == 0))
                {
                    if (IPVX_ADDR_COMPARE
                        (pSgEntry->pSrcDescNode->SrcAddr, SrcAddr) == 0)
                    {
                        /* entry already exists return failure */
                        pFwdEntry = pSgEntry;
                        i4Status = MFWD_SUCCESS;
                        break;
                    }
                }
                else
                {
                    /* entry already exists return failure */
                    pFwdEntry = pSgEntry;
                    i4Status = MFWD_SUCCESS;
                    break;
                }

            }                    /* end of TMO_SLL_SCAN */

        }                        /* end of if pGrpNode->u4GrpAddr == pUpdData->u4GrpAddr */
    }                            /* end of hash scan bucket */

    /*Search for Group specific route has failed 
     * Now search, if there is any other route with better GroupMask match*/
    if (i4Status == MFWD_FAILURE)
    {
        pRtNode = TMO_SLL_First (&gMfwdContext.MrtGetNextList);
        if (pRtNode != NULL)
        {
            pRtEntry = MFWD_GET_BASE_PTR (tMfwdRtEntry, GetNextLink, pRtNode);
            while (pRtEntry != NULL)
            {
                if (MEMCMP
                    (pRtEntry->GrpAddr.au1Addr, GrpAddr.au1Addr,
                     (pRtEntry->u4GrpMaskLen) / 8) == 0)
                {
                    if (pRtEntry->u1RtType == MFWD_STAR_G_ROUTE)
                    {
                        pFwdEntry = pRtEntry;
                        i4Status = MFWD_SUCCESS;
                        break;
                    }
                }
                pNxtRtNode =
                    TMO_SLL_Next ((&gMfwdContext.MrtGetNextList), pRtNode);
                if (pNxtRtNode == NULL)
                {
                    pRtEntry = NULL;
                }
                else
                {
                    pRtEntry =
                        MFWD_GET_BASE_PTR (tMfwdRtEntry, GetNextLink,
                                           pNxtRtNode);
                    pRtNode = pNxtRtNode;
                }

            }
        }
    }
    if (i4Status != MFWD_FAILURE)
    {
        if ((pFwdEntry->u1RtType == MFWD_STAR_G_ROUTE)
            && (TMO_SLL_Count (&(pFwdEntry->IifList)) > 0))
        {
            TMO_SLL_Scan (&(pFwdEntry->IifList), pIifNode, tMfwdIifNode *)
            {
                if (pIifNode->u4IifIndex == u4Iif)
                {
                    u1InIntFound = MFWD_SUCCESS;
                    MFWD_TRC_ARG3 (MFWD_TRC_FLAG, MFWD_CONTROL_PATH_TRC,
                                   MFWD_MOD_NAME,
                                   "The MC is received from Valid Source %s and group %s is"
                                   ", Incoming interface Index is Iif %d\n",
                                   SrcAddr.au1Addr, GrpAddr.au1Addr,
                                   pIifNode->u4IifIndex);
                    break;
                }
            }
        }
        else
        {
            u1InIntFound = MFWD_SUCCESS;
        }
        if (u1InIntFound == MFWD_FAILURE)
        {
            pMfwdFwdInfo->u1SearchStat = MFWD_MDP_WRONG_IIF;
            MFWD_TRC (MFWD_TRC_FLAG, MFWD_CONTROL_PATH_TRC, MFWD_MOD_NAME,
                      "Iif List For the Entry Is NULL or Data received in Invalid interface \n");
        }
        else
        {
            if ((pFwdEntry->u1ChkRpf != MFWD_MRP_CHK_RPF_IF) ||
                (pFwdEntry->pSrcDescNode->u4Iif == u4Iif) ||
                (pFwdEntry->u1RtType == MFWD_STAR_G_ROUTE))
            {
                OsixGetSysTime (&(pFwdEntry->u4LastFwdTime));

                pMfwdFwdInfo->u1DeliverMDP = pFwdEntry->u1DeliverMDP;

                pMfwdFwdInfo->u1SearchStat = MFWD_MDP_CACHE_HIT;
                pMfwdFwdInfo->u4Iif = pFwdEntry->pSrcDescNode->u4Iif;

                TMO_SLL_Scan (&(pFwdEntry->OifList), pOifNode, tMfwdOifNode *)
                {
                    if ((pOifNode->u4OifState == MFWD_OIF_FORWARDING) ||
                        (pOifNode->u4OifState == MFWD_ZERO))
                    {
                        u1EntryState = MFWD_OIF_FORWARDING;
                        break;
                    }
                }

                if (u1EntryState == MFWD_OIF_FORWARDING)
                {
                    pMfwdFwdInfo->pOifList = &pFwdEntry->OifList;
                    pFwdEntry->u4FwdCnt++;
                    MFWD_TRC_ARG4 (MFWD_TRC_FLAG, MFWD_CONTROL_PATH_TRC,
                                   MFWD_MOD_NAME,
                                   "The forward count For Source %s and group %s is"
                                   " %u Oif is %d\n", SrcAddr.au1Addr,
                                   GrpAddr.au1Addr, pFwdEntry->u4FwdCnt,
                                   pOifNode->u4OifIndex);
                }
                else
                {
                    pMfwdFwdInfo->pOifList = NULL;
                    MFWD_TRC (MFWD_TRC_FLAG, MFWD_CONTROL_PATH_TRC,
                              MFWD_MOD_NAME,
                              "Oif List For the Entry Is NULL or Entry is in Negative Cache"
                              "State\n");
                }
            }                    /* end of if (pFwdEntry->pSrcDescNode->u4Iif == u4Iif) */
            else
            {
                pMfwdFwdInfo->u1SearchStat = MFWD_MDP_WRONG_IIF;
            }
        }
    }                            /* end of i4Status != MFWD_FAILURE */
    else
    {
        pMfwdFwdInfo->u1SearchStat = MFWD_MDP_CACHE_MISS;
    }

    MFWD_DBG (MFWD_DBG_EXIT,
              "Exiting the function to Obtain forwarding info\n");

    return i4RetCode;
}                                /* end of the function MfwdMrtCreateRtEntry */

/****************************************************************************
 * Function Name    :  MfwdMrtGetFwdCount
 *
 * Description      :  This function finds returns the information about number
 *                     packets that have been forwarded for a particular source
 *                     and group address on an entry.
 *                     
 * Input(s)         :  u2OwnerId :- The Identification value of the owner  
 *                     within whose MRT the entry is to be located.
 *                     u4SrcAddr :- The source address of the query data.
 *                     u4GrpAddr :- The group address of the query data.
 *
 * Output(s)        :  u4FwdCount :- The value containing number of packets 
 *                     forwarded.
 *
 * Global Variables
 * Referred         :  gMfwdContext.pOwnerInfoTbl 
 *                     (Refered via the macro MFWD_OWNER)
 *
 * Global Variables
 * Modified         :  None
 *
 * Return(s)        :  MFWD_FAILURE or MFWD_SUCCESS
 ****************************************************************************/

INT4
MfwdMrtGetFwdCount (UINT2 u2OwnerId, tIPvXAddr SrcAddr, tIPvXAddr GrpAddr,
                    UINT4 *pu4FwdCnt)
{
    UINT4               u4FwdCnt = 0;
    INT4                i4Status = MFWD_FAILURE;
    UINT1               u1HashIndex = 0;
    tMfwdGrpNode       *pGrpNode = NULL;
    tMfwdRtEntry       *pSgEntry = NULL;
    tMfwdRtEntry       *pReqdEntry = NULL;

    MFWD_DBG2 (MFWD_DBG_ENTRY,
               "Entering the function to Obtain the Forwarding count for \n"
               " the source %s and group %s\n", SrcAddr.au1Addr,
               GrpAddr.au1Addr);

    /* find the hash index of the group address */
    MFWD_GET_HASH_INDEX (GrpAddr, u1HashIndex);

    /* Take the semaphore on the owner information node before creating  the 
     * route entry.
     */
    MFWD_LOCK ();
    *pu4FwdCnt = 0;

    /* find the group node corresponding to the group address given */
    TMO_HASH_Scan_Bucket (MFWD_OWNER_MRT (u2OwnerId), (UINT4) u1HashIndex,
                          pGrpNode, tMfwdGrpNode *)
    {
        /* If the group node itself does not exist there is no way we can give
         * any forward count return failure
         */
        if (IPVX_ADDR_COMPARE (GrpAddr, pGrpNode->GrpAddr) < 0)
        {
            MFWD_TRC (MFWD_TRC_FLAG, MFWD_CONTROL_PATH_TRC, MFWD_MOD_NAME,
                      "No (S, G) or (*, G), trying to see if (*, *) exists\n");
            break;
        }                        /* end of if pUpdData.u4GrpAddr < pGrpNode->u4GrpAddr */

        /* If the group address matches then see if the entry exists
         */
        if ((IPVX_ADDR_COMPARE (pGrpNode->GrpAddr, GrpAddr) == 0) &&
            (GrpAddr.u1AddrLen == pGrpNode->u4GrpMaskLen))
        {
            /* see if we can find a (S, G) entry directly */
            TMO_SLL_Scan (&(pGrpNode->SgEntryList), pSgEntry, tMfwdRtEntry *)
            {
                if (IPVX_ADDR_COMPARE (pSgEntry->pSrcDescNode->SrcAddr, SrcAddr)
                    == 0)
                {
                    /* entry exists so we can use this to return value */
                    MFWD_TRC (MFWD_TRC_FLAG, MFWD_CONTROL_PATH_TRC,
                              MFWD_MOD_NAME, "Found an (S, G) entry \n");
                    pReqdEntry = pSgEntry;
                    i4Status = MFWD_SUCCESS;
                    break;
                }                /* end of if (pSgEntry->pSrcDescNode->u4RtAddr ..... */
                if (IPVX_ADDR_COMPARE (pSgEntry->pSrcDescNode->RtAddr, SrcAddr)
                    == 0)
                {
                    u4FwdCnt += pSgEntry->u4FwdCnt;
                }
            }                    /* end of TMO_SLL_SCAN */

            if (i4Status == MFWD_SUCCESS)
            {
                break;
            }

        }                        /* end of if pGrpNode->u4GrpAddr == pUpdData->u4GrpAddr */

    }                            /* end of hash scan bucket */

    if (i4Status != MFWD_FAILURE)
    {
        if (IPVX_ADDR_COMPARE (pReqdEntry->pSrcDescNode->SrcAddr, SrcAddr) == 0)
        {
            MFWD_TRC (MFWD_TRC_FLAG, MFWD_CONTROL_PATH_TRC, MFWD_MOD_NAME,
                      "Source address matches with the entry\n");
            *pu4FwdCnt = pReqdEntry->u4FwdCnt;
            i4Status = MFWD_SUCCESS;
        }
    }                            /* end of i4Status != MFWD_FAILURE */
    else
    {
        *pu4FwdCnt = u4FwdCnt;
        i4Status = MFWD_SUCCESS;
    }

    MFWD_UNLOCK ();
    MFWD_DBG2 (MFWD_DBG_EXIT, "Exiting the function to get the forward count \n"
               "    for the source %s and group %s\n", SrcAddr.au1Addr,
               GrpAddr.au1Addr);
    return i4Status;

}                                /* end of the function MfwdMrtGetFwdCount */

/****************************************************************************
 * Function Name    :  MfwdMrtGetLastFwdTime
 *
 * Description      :  This function gives the time in stups about when last
 *                     route entry was referred for  the packet forwarding. 
 *                     
 * Input(s)         :  u2OwnerId :- The Identification value of the owner  
 *                     within whose MRT the entry is to be located.
 *                     u4SrcAddr :- The source address of the query data.
 *                     u4GrpAddr :- The group address of the query data.
 *
 * Output(s)        :  u4FwdTime :- The value containing time 
 *
 * Global Variables
 * Referred         :  gMfwdContext.pOwnerInfoTbl 
 *                     (Refered via the macro MFWD_OWNER)
 *
 * Global Variables
 * Modified         :  None
 *
 * Return(s)        :  MFWD_FAILURE or MFWD_SUCCESS
 ****************************************************************************/
INT4
MfwdMrtGetLastFwdTime (UINT2 u2OwnerId, tIPvXAddr SrcAddr, tIPvXAddr GrpAddr,
                       UINT4 *pu4FwdTime)
{
    INT4                i4Status = MFWD_FAILURE;
    UINT1               u1HashIndex = 0;
    tMfwdRtEntry       *pReqdEntry = NULL;

    MFWD_DBG2 (MFWD_DBG_ENTRY,
               "Entering the function to Obtain the Forwarding count for \n"
               " the source %s and group %s\n", SrcAddr.au1Addr,
               GrpAddr.au1Addr);

    /* find the hash index of the group address */
    MFWD_GET_HASH_INDEX (GrpAddr, u1HashIndex);

    /* Take the semaphore on the owner information node before creating  the 
     * route entry.
     */
    MFWD_LOCK ();
    *pu4FwdTime = 0;
    if (gMfwdContext.u1MfwdStatus == MFWD_STATUS_DISABLED)
    {
        MFWD_UNLOCK ();
        return MFWD_FAILURE;
    }
    pReqdEntry = MfwdMrtGetExactRtEntry (u2OwnerId, GrpAddr, SrcAddr, 0);

    if (pReqdEntry != NULL)
    {
        if (IPVX_ADDR_COMPARE (pReqdEntry->pSrcDescNode->SrcAddr, SrcAddr) == 0)
        {
            MFWD_TRC (MFWD_TRC_FLAG, MFWD_CONTROL_PATH_TRC, MFWD_MOD_NAME,
                      "Source address matches with the entry\n");
            *pu4FwdTime = pReqdEntry->u4LastFwdTime;
            i4Status = MFWD_SUCCESS;
        }
    }                            /* end of i4Status != MFWD_FAILURE */

    MFWD_UNLOCK ();

    MFWD_DBG2 (MFWD_DBG_EXIT, "Exiting the function to get the forward count \n"
               "    for the source %s and group %s\n", SrcAddr.au1Addr,
               GrpAddr.au1Addr);
    return i4Status;
}                                /* end of the function MfwdMrtGetFwdCount */

/****************************************************************************
 * Function Name    :  MrtGrpNodeAddFn
 *
 * Description      :  This function adds the Group Node
 *                     
 * Input(s)         :  pCurNode  :- Pointer to the Current Node in the Hash 
 *                                  Table
 *                     pu1GrpAddr :- Pointer to  group address
 *
 * Global Variables
 * Referred         :  None
 *
 * Global Variables
 * Modified         :  None
 *
 * Return(s)        :  INSERT_PRIORTO or MATCH_NOT_FOUND
 ****************************************************************************/

UINT4
MrtGrpNodeAddFn (tTMO_HASH_NODE * pCurNode, UINT1 *pu1GrpAddr)
{
    tMfwdGrpNode       *pGrpNode;

    pGrpNode = (tMfwdGrpNode *) pCurNode;

    if (memcmp (pGrpNode->GrpAddr.au1Addr, pu1GrpAddr, 16) > 0)
    {
        return INSERT_PRIORTO;
    }
    return MATCH_NOT_FOUND;
}

/****************************************************************************
 * Function Name    :  MfwdMrtGetExactRtEntry
 *
 * Description      :  This function Returns an exact matching entry in the 
 *                     routing table for the given indices.
 *                     NOTE:- THIS FUNCTIONS DOES NOT TAKE SEMAPHORE
 *                     IT IS THE RESPONSIBILTY OF THE CALLING FUNCTION TO
 *                     TAKE THE SEMAPHORE AND OBVIOUSLY SHOULD GIVE IT
 *
 *                     
 * Input(s)         :  u2OwnerId :- The Identification value of the owner  
 *                     within whose MRT the entry is to be located.
 *                     u4SrcAddr :- The source address of the query data.
 *                     u4GrpAddr :- The group address of the query data.
 *                     u4SrcMask :- The mask value of the source address
 *
 * Output(s)        :  pRtEntry :- The Exactly matching route entry in the
 *                     routing table.
 *
 * Global Variables
 * Referred         :   
 *
 * Global Variables
 * Modified         :  None
 *
 * Return(s)        : pReqdEntry 
 ****************************************************************************/

tMfwdRtEntry       *
MfwdMrtGetExactRtEntry (UINT2 u2OwnerId, tIPvXAddr GrpAddr, tIPvXAddr SrcAddr,
                        UINT4 u4SrcMask)
{
    INT4                i4Status = MFWD_FAILURE;
    UINT1               u1HashIndex = 0;
    tMfwdGrpNode       *pGrpNode = NULL;
    tMfwdRtEntry       *pSgEntry = NULL;
    tMfwdRtEntry       *pReqdEntry = NULL;

    MFWD_DBG (MFWD_DBG_ENTRY,
              "Entering the function to Obtain the Exact matching entry\n");
    UNUSED_PARAM (u4SrcMask);

    /* find the hash index of the group address */
    MFWD_GET_HASH_INDEX (GrpAddr, u1HashIndex);

    /* find the group node corresponding to the group address given */
    TMO_HASH_Scan_Bucket (MFWD_OWNER_MRT (u2OwnerId), (UINT4) u1HashIndex,
                          pGrpNode, tMfwdGrpNode *)
    {
        /* If the group node itself does not exist there is no way we can give
         * a matching entry
         */
        if (IPVX_ADDR_COMPARE (GrpAddr, pGrpNode->GrpAddr) < 0)
        {
            break;
        }                        /* end of if pUpdData.u4GrpAddr < pGrpNode->u4GrpAddr */

        /* If the group address matches then see if the entry exists
         */
        if ((IPVX_ADDR_COMPARE (pGrpNode->GrpAddr, GrpAddr) == 0) &&
            (GrpAddr.u1AddrLen == pGrpNode->u4GrpMaskLen))
        {

            TMO_SLL_Scan (&(pGrpNode->SgEntryList), pSgEntry, tMfwdRtEntry *)
            {
                if (IPVX_ADDR_COMPARE (pSgEntry->pSrcDescNode->SrcAddr, SrcAddr)
                    == 0)
                {
                    pReqdEntry = pSgEntry;
                    i4Status = MFWD_SUCCESS;
                    break;
                }                /* end of if (pSgEntry->pSrcDescNode->u4RtAddr ..... */
            }                    /* end of TMO_SLL_SCAN */

        }                        /* end of if pGrpNode->u4GrpAddr == pUpdData->u4GrpAddr */

    }                            /* end of hash scan bucket */

    if (i4Status == MFWD_FAILURE)
    {
        MFWD_TRC (MFWD_TRC_FLAG, MFWD_CONTROL_PATH_TRC, MFWD_MOD_NAME,
                  "Matching route Entry does not exist\n");
    }                            /* end of if i4Status == MFWD_FAILURE */
    else
    {
        MFWD_TRC (MFWD_TRC_FLAG, MFWD_CONTROL_PATH_TRC, MFWD_MOD_NAME,
                  "Found a Matching Entry in the routing table\n");
    }

    MFWD_DBG (MFWD_DBG_EXIT,
              "Exiting the function to get the exact Matching Entry\n");

    return pReqdEntry;

}                                /* end of the function MfwdMrtGetFwdCount */

/****************************************************************************
 * Function Name    :  MfwdMrtUpdGetNextList
 *
 * Description      :  This function Update the get next list of the MRT for 
 *                     the new entry's insertion.
 *                     
 * Input(s)         :  pNewRtEntry :- New route entry that is to be inserted 
 *
 * Output(s)        :  None
 *
 * Global Variables
 * Referred         :   
 *
 * Global Variables
 * Modified         :  None
 *
 * Return(s)        :  None 
 ****************************************************************************/

void
MfwdMrtUpdGetNextList (tMfwdRtEntry * pNewRtEntry)
{
    tTMO_SLL_NODE      *pRtNode = NULL;
    tMfwdRtEntry       *pRtEntry = NULL;
    tMfwdRtEntry       *pPrevRtEntry = NULL;

    MFWD_DBG (MFWD_DBG_EXIT, "Entering the function to Update MRT Get Next "
              "List\n");

    TMO_SLL_Scan (&(gMfwdContext.MrtGetNextList), pRtNode, tTMO_SLL_NODE *)
    {
        pRtEntry = MFWD_GET_BASE_PTR (tMfwdRtEntry, GetNextLink, pRtNode);
        if (pNewRtEntry->u2OwnerId > pRtEntry->u2OwnerId)
        {
            pPrevRtEntry = pRtEntry;
            continue;
        }
        if (pNewRtEntry->u2OwnerId < pRtEntry->u2OwnerId)
        {
            break;
        }
        else
        {
            if ((IPVX_ADDR_COMPARE (pNewRtEntry->GrpAddr, pRtEntry->GrpAddr) >
                 0) && (pNewRtEntry->u4GrpMaskLen == pRtEntry->u4GrpMaskLen))
            {
                pPrevRtEntry = pRtEntry;
                continue;
            }
            else if ((IPVX_ADDR_COMPARE
                      (pNewRtEntry->GrpAddr, pRtEntry->GrpAddr) < 0)
                     && (pNewRtEntry->u4GrpMaskLen == pRtEntry->u4GrpMaskLen))
            {

                break;
            }
            else
            {
                if (IPVX_ADDR_COMPARE (pNewRtEntry->pSrcDescNode->SrcAddr,
                                       pRtEntry->pSrcDescNode->SrcAddr) > 0)
                {
                    pPrevRtEntry = pRtEntry;
                    continue;
                }
                else if (IPVX_ADDR_COMPARE (pNewRtEntry->pSrcDescNode->SrcAddr,
                                            pRtEntry->pSrcDescNode->SrcAddr) <
                         0)
                {
                    break;
                }
                else
                {
                    if (pNewRtEntry->pSrcDescNode->i4RtMask >=
                        pRtEntry->pSrcDescNode->i4RtMask)
                    {
                        pPrevRtEntry = pRtEntry;
                        continue;
                    }
                    else if (pNewRtEntry->pSrcDescNode->i4RtMask <
                             pRtEntry->pSrcDescNode->i4RtMask)
                    {
                        break;
                    }
                    else
                    {
                        return;
                    }
                }                /* end of else */
            }

        }                        /* end of if (pNewRtEntry->u2OwnerId == pRtEntry->u2OwnerId) */
    }                            /* end of TMO_SLL_Scan (&(gMfwdContext.MrtGetNextList) */

    if (pRtNode == NULL)
    {
        TMO_SLL_Add (&gMfwdContext.MrtGetNextList,
                     (tTMO_SLL_NODE *) & pNewRtEntry->GetNextLink);
    }
    else
    {
        if (pPrevRtEntry == NULL)
        {
            TMO_SLL_Insert (&gMfwdContext.MrtGetNextList,
                            (tTMO_SLL_NODE *) NULL,
                            (tTMO_SLL_NODE *) & pNewRtEntry->GetNextLink);
        }
        else
        {
            TMO_SLL_Insert (&gMfwdContext.MrtGetNextList,
                            (tTMO_SLL_NODE *) & pPrevRtEntry->GetNextLink,
                            (tTMO_SLL_NODE *) & pNewRtEntry->GetNextLink);
        }
    }
    MFWD_DBG (MFWD_DBG_EXIT, "Exiting the function to Update MRT Get Next "
              "List\n");
    return;
}
