#ifndef _MFWDCMWR_H
#define _MFWDCMWR_H

VOID RegisterMFWDCM(VOID);

VOID UnRegisterMFWDCM(VOID);
INT4 IpCmnMRouteEnableGet(tSnmpIndex *, tRetVal *);
INT4 IpCmnMRouteEntryCountGet(tSnmpIndex *, tRetVal *);
INT4 IpCmnMRouteEnableCmdbGet(tSnmpIndex *, tRetVal *);
INT4 MfwdCmnGlobalTraceGet(tSnmpIndex *, tRetVal *);
INT4 MfwdCmnGlobalDebugGet(tSnmpIndex *, tRetVal *);
INT4 IpCmnMRouteDiscardedPktsGet(tSnmpIndex *, tRetVal *);
INT4 MfwdCmnAvgDataRateGet(tSnmpIndex *, tRetVal *);
INT4 IpCmnMRouteEnableSet(tSnmpIndex *, tRetVal *);
INT4 IpCmnMRouteEnableCmdbSet(tSnmpIndex *, tRetVal *);
INT4 MfwdCmnGlobalTraceSet(tSnmpIndex *, tRetVal *);
INT4 MfwdCmnGlobalDebugSet(tSnmpIndex *, tRetVal *);
INT4 MfwdCmnAvgDataRateSet(tSnmpIndex *, tRetVal *);
INT4 IpCmnMRouteEnableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IpCmnMRouteEnableCmdbTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MfwdCmnGlobalTraceTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MfwdCmnGlobalDebugTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MfwdCmnAvgDataRateTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IpCmnMRouteEnableDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 IpCmnMRouteEnableCmdbDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 MfwdCmnGlobalTraceDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 MfwdCmnGlobalDebugDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 MfwdCmnAvgDataRateDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);





INT4 GetNextIndexIpCmnMRouteTable(tSnmpIndex *, tSnmpIndex *);
INT4 IpCmnMRouteUpstreamNeighborGet(tSnmpIndex *, tRetVal *);
INT4 IpCmnMRouteInIfIndexGet(tSnmpIndex *, tRetVal *);
INT4 IpCmnMRouteUpTimeGet(tSnmpIndex *, tRetVal *);
INT4 IpCmnMRoutePktsGet(tSnmpIndex *, tRetVal *);
INT4 IpCmnMRouteDifferentInIfPacketsGet(tSnmpIndex *, tRetVal *);
INT4 IpCmnMRouteProtocolGet(tSnmpIndex *, tRetVal *);
INT4 IpCmnMRouteRtAddressGet(tSnmpIndex *, tRetVal *);
INT4 IpCmnMRouteRtMaskGet(tSnmpIndex *, tRetVal *);
INT4 IpCmnMRouteRtTypeGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexIpCmnMRouteNextHopTable(tSnmpIndex *, tSnmpIndex *);
INT4 IpCmnMRouteNextHopStateGet(tSnmpIndex *, tRetVal *);
INT4 IpCmnMRouteNextHopUpTimeGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexIpCmnMRouteInterfaceTable(tSnmpIndex *, tSnmpIndex *);
INT4 IpCmnMRouteInterfaceOwnerIdGet(tSnmpIndex *, tRetVal *);
INT4 IpCmnMRouteInterfaceTtlGet(tSnmpIndex *, tRetVal *);
INT4 IpCmnMRouteInterfaceProtocolGet(tSnmpIndex *, tRetVal *);
INT4 IpCmnMRouteInterfaceRateLimitGet(tSnmpIndex *, tRetVal *);
INT4 IpCmnMRouteInterfaceInMcastOctetsGet(tSnmpIndex *, tRetVal *);
INT4 IpCmnMRouteInterfaceCmdbPktCntGet(tSnmpIndex *, tRetVal *);
INT4 IpCmnMRouteInterfaceOutMcastOctetsGet(tSnmpIndex *, tRetVal *);
#endif /* _MFWDCMWR_H */
