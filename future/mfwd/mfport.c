/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mfport.c,v 1.32 2015/11/05 13:21:47 siva Exp $
 * 
 ********************************************************************/

#include "mfinc.h"

/* Global Context Variable */
extern tMfwdContextStructure gMfwdContext;

#ifdef TRACE_WANTED
static UINT2        u2MfwdTrcModule = MFWD_IO_MODULE;
#endif

/****************************************************************************
 * Function Name    :  RegisterFSMFWDwithSNMP
 *
 * Description      :  This function should be ported to respective stacks
 *                     for registration with SNMP
 *
 * Input(s)         :  None
 *
 * Output(s)        :  None
 *
 * Global Variables
 * Referred         :  None
 *
 * Global Variables
 * Modified         :  None
 *
 * Return(s)        :  None
 ****************************************************************************/

#ifdef __STDC__
INT4
RegisterFSMFWDwithSNMP (VOID)
#else
INT4
RegisterFSMFWDwithSNMP ()
#endif
{
    return SUCCESS;
}

/****************************************************************************
 * Function Name    :  RegisterFSMFWDwithSNMP
 *
 * Description      :  This function initialises the socket needed for sending
 *                     the multicast data packets out.
 *
 * Input(s)         :  None
 *
 * Output(s)        :  None
 *
 * Global Variables
 * Referred         :  gMfwdContext
 *
 * Global Variables
 * Modified         :  gMfwdContext 
 *
 * Return(s)        :  MFWD_SUCCESS or MFWD_FAILURE 
 ****************************************************************************/

INT4
MfwdInitSocketInfo ()
{
    INT4                i4DataSockId;
    INT4                i4OptionWanted;

    if ((i4DataSockId = socket (AF_INET, SOCK_RAW, IPPROTO_RAW)) < 0)
    {
        return MFWD_FAILURE;
    }

    i4OptionWanted = TRUE;
    if (setsockopt (i4DataSockId, IPPROTO_IP, IP_HDRINCL,
                    &i4OptionWanted, sizeof (i4OptionWanted)) == -1)
    {
        close (i4DataSockId);
        return MFWD_FAILURE;
    }
    gMfwdContext.i4DataSockId = i4DataSockId;

    return MFWD_SUCCESS;
}

/****************************************************************************
 * Function Name    :  MfwdSendMcastDataPktToIp
 *
 * Description      :  This function should be ported to respective stacks
 *                     for forwarding the multicast data packet on the list of
 *                     outgoing interfaces
 *
 * Input(s)         :  None
 *
 * Output(s)        :  None
 *
 * Global Variables
 * Referred         :  None
 *
 * Global Variables
 * Modified         :  None
 *
 * Return(s)        :  None
 ****************************************************************************/

INT4
MfwdSendMcastDataPkt (tCRU_BUF_CHAIN_HEADER * pBuffer, UINT4 u4GrpAddr,
                      tTMO_SLL * pOifList, UINT4 u4IifIndex)
{
    tMfwdInterfaceNode *pIfNode = NULL;
    tMfwdOifNode       *pOifNode = NULL;
    UINT2               u2BufLen;
    INT4                i4Status = MFWD_SUCCESS;
    UINT4               u4Oif;
    UINT4               u4Sum;
    UINT1              *pu1SendBuf;
    t_IP_HEADER        *pIpHdr;
    struct ip_mreqn     IpMreq;
    struct sockaddr_in  DestAddr;

    MFWD_MEMSET (&IpMreq, 0, sizeof (struct ip_mreqn));

    /* Allocate a linear buffer for the packet to be forwarded  and copy the
     * CRU buffer over the buffer chain 
     */

    pu1SendBuf = gMfwdContext.au1SendBuf;
    u2BufLen = (UINT2) CRU_BUF_Get_ChainValidByteCount (pBuffer);
    if (CRU_BUF_Copy_FromBufChain (pBuffer, pu1SendBuf, 0, u2BufLen)
        == CRU_FAILURE)
    {
        return MFWD_FAILURE;
    }

    pIpHdr = (t_IP_HEADER *) (VOID *) pu1SendBuf;

    /* Recalculate the checksum after decrementing the TTL value */
    pIpHdr->u1Ttl--;

    if (pIpHdr->u1Ttl == 0)
    {
        MFWD_TRC (MFWD_TRC_FLAG, MFWD_DATA_PATH_TRC, MFWD_MOD_NAME,
                  "Multicast data packet's TTL Expired \n");
    }

    u4Sum = OSIX_NTOHS ((pIpHdr->u2Cksum)) + 0x100;
    pIpHdr->u2Cksum = (UINT2) OSIX_HTONS ((u4Sum + (u4Sum >> 16)));

    DestAddr.sin_addr.s_addr = OSIX_HTONL (u4GrpAddr);
    DestAddr.sin_family = AF_INET;
    DestAddr.sin_port = 0;

    TMO_SLL_Scan (pOifList, pOifNode, tMfwdOifNode *)
    {
        if ((pOifNode->u4OifState == MFWD_OIF_PRUNED) ||
            (pOifNode->u4OifIndex == u4IifIndex))
        {
            continue;
        }

        u4Oif = pOifNode->u4OifIndex;

        IpMreq.imr_ifindex = u4Oif;

        if (setsockopt (gMfwdContext.i4DataSockId, IPPROTO_IP,
                        IP_MULTICAST_IF, (INT1 *) &IpMreq,
                        sizeof (struct ip_mreqn)) < 0)
        {
            MFWD_TRC_ARG1 (MFWD_TRC_FLAG, MFWD_DATA_PATH_TRC,
                           MFWD_MOD_NAME, "Failure in setting "
                           "IP_MULTICAST_IF option on IF %d\n", u4Oif);
            continue;
        }

        if (sendto (gMfwdContext.i4DataSockId, pu1SendBuf, u2BufLen, 0,
                    (struct sockaddr *) &DestAddr, sizeof (DestAddr)) <= 0)
        {
            MFWD_TRC_ARG1 (MFWD_TRC_FLAG, MFWD_DATA_PATH_TRC,
                           MFWD_MOD_NAME,
                           "Failure in sending multicast packet on IF %d\n",
                           u4Oif);
        }
        pOifNode->u4FwdCnt++;
        pIfNode = MFWD_GET_IF_NODE (u4Oif, IPVX_ADDR_FMLY_IPV4);
        if (NULL == pIfNode)
        {
            continue;
        }
        pIfNode->u4OutMdpOct += u2BufLen;

    }

    MFWD_DBG (MFWD_DBG_EXIT, "Exiting Function to send Data packet to IP \n");
    return i4Status;
}                                /* end of function   MfwdSendMcastDataPktToIp */

#ifdef PIMV6_WANTED
/****************************************************************************
 * Function Name    :  MfwdSendIpv6McastDataPkt
 *
 * Description      :  This function should be ported to respective stacks
 *                     for forwarding the IPv6 multicast data packet on the list of
 *                     outgoing interfaces
 *
 * Input(s)         :  None
 *
 * Output(s)        :  None
 *
 * Global Variables
 * Referred         :  None
 *
 * Global Variables
 * Modified         :  None
 *
 * Return(s)        :  None
 ****************************************************************************/

INT4
MfwdSendIpv6McastDataPkt (tCRU_BUF_CHAIN_HEADER * pBuffer, UINT1 *pu1GrpAddr,
                          tTMO_SLL * pOifList, UINT4 u4IifIndex)
{
    tCRU_BUF_CHAIN_HEADER *pDupBuf = NULL;
    tHlToIp6McastParams params;
    tMfwdOifNode       *pOifNode = NULL;
    tIp6Hdr             Ip6Hdr;
    UINT4              *pIp6OifList = NULL;
    UINT4              *pTmpOifList = NULL;
    UINT2               u2BufLen;
    UINT2               u2OifCnt;
    UINT4               u4IfIndex;

    /* u2OifCnt = (TMO_SLL_Count (&(pRouteEntry->OifList)) + 2),
     * 2 is for iif and no. of oif */

    u2OifCnt = (UINT2) (TMO_SLL_Count (pOifList) + 2);
    pIp6OifList = UtlShMemAllocOifList ();

    if (pIp6OifList == NULL)
    {
        MFWD_DBG (MFWD_DBG_MEM_IF, "Cannot Allocate memory for the"
                  "IP6 Outgoing List\n");
        /* Release the route entry also to the memory pool */
        UtlShMemFreeOifList (pIp6OifList);
        return MFWD_FAILURE;
    }

    u2OifCnt = 0;
    pTmpOifList = pIp6OifList;
    *pTmpOifList++ = 0;
    MfwdGetIp6IfIndexFromPort (u4IifIndex, &u4IfIndex);
    *pTmpOifList++ = u4IfIndex;

    TMO_SLL_Scan (pOifList, pOifNode, tMfwdOifNode *)
    {
        if ((pOifNode->u4OifState == MFWD_OIF_PRUNED) ||
            (pOifNode->u4OifIndex == u4IifIndex))
        {
            continue;
        }
        MfwdGetIp6IfIndexFromPort (pOifNode->u4OifIndex, &u4IfIndex);
        *pTmpOifList++ = u4IfIndex;
        u2OifCnt++;
        pOifNode->u4FwdCnt++;
    }

    if (u2OifCnt == 0)
    {
        UtlShMemFreeOifList (pIp6OifList);
        return MFWD_SUCCESS;
    }
    *pIp6OifList = u2OifCnt;

    if (pBuffer == NULL)
    {
        UtlShMemFreeOifList (pIp6OifList);
        return MFWD_FAILURE;
    }

    u2BufLen = (UINT2) CRU_BUF_Get_ChainValidByteCount (pBuffer);
    MfwdExtractIpv6Hdr (&Ip6Hdr, pBuffer);
    Ip6Hdr.u1Hlim--;

    if (Ip6Hdr.u1Hlim == 0)
    {
        MFWD_TRC (MFWD_TRC_FLAG, MFWD_DATA_PATH_TRC, MFWD_MOD_NAME,
                  "IPV6 Multicast data packet's Hop limit  Expired \n");
    }
    params.pu4OIfList = pIp6OifList;
    params.u1Cmd = IP6_MCAST_DATA_PACKET_FROM_MRM;
    params.u1Hlim = Ip6Hdr.u1Hlim;
    params.u1Proto = Ip6Hdr.u1Nh;
    params.u4Len = u2BufLen;
    params.u4Len -= IPV6_HEADER_LEN;
    MEMCPY (&params.Ip6DstAddr, pu1GrpAddr, IP6_ADDR_SIZE);
    MEMCPY (&params.Ip6SrcAddr, &(Ip6Hdr.srcAddr), IP6_ADDR_SIZE);
    if (pBuffer != NULL)
    {
        /*pBuffer may be null */
        pDupBuf = CRU_BUF_Duplicate_BufChain (pBuffer);
    }
    if (pDupBuf == NULL)
    {
        MFWD_TRC (MFWD_TRC_FLAG, MFWD_PATH_TRC, MFWD_MOD_NAME,
                  "Failed in copying pBuffer \n");
        MFWD_DBG (MFWD_DBG_EXIT, "MfwdSendIpv6McastDataPkt \n");
        UtlShMemFreeOifList (pIp6OifList);
        return MFWD_FAILURE;
    }
#ifdef IP6_WANTED
    if (Ip6RcvMcastPktFromHl (pDupBuf, &params) == IP6_FAILURE)
    {
        MFWD_TRC (MFWD_TRC_FLAG, MFWD_PATH_TRC, MFWD_MOD_NAME,
                  "Queuing Mcast pkts to IPV6 FAILED \n");
        MFWD_DBG (MFWD_DBG_EXIT, "MfwdSendIpv6McastDataPkt \n");
        UtlShMemFreeOifList (pIp6OifList);
        return MFWD_FAILURE;
    }
#endif
    MFWD_DBG (MFWD_DBG_EXIT, "MfwdSendIpv6McastDataPkt \n");
    return MFWD_SUCCESS;
}

INT4
MfwdIpv6GetIfInfo (UINT4 u4IfIndex, tNetIpv6IfInfo * pNetIpv6IfInfo)
{
    INT4                i4RetVal = NETIPV6_FAILURE;
#ifdef IP6_WANTED
    i4RetVal = NetIpv6GetIfInfo (u4IfIndex, pNetIpv6IfInfo);
#else
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pNetIpv6IfInfo);
#endif
    {
        return i4RetVal;
    }
}

#else
INT4
MfwdSendIpv6McastDataPkt (tCRU_BUF_CHAIN_HEADER * pBuffer, UINT1 *pu1GrpAddr,
                          tTMO_SLL * pOifList, UINT4 u4IifIndex)
{
    UNUSED_PARAM (pBuffer);
    UNUSED_PARAM (pu1GrpAddr);
    UNUSED_PARAM (pOifList);
    UNUSED_PARAM (u4IifIndex);
    return MFWD_FAILURE;
}

INT4
MfwdIpv6GetIfInfo (UINT4 u4IfIndex, tNetIpv6IfInfo * pNetIpv6IfInfo)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pNetIpv6IfInfo);
    return MFWD_FAILURE;
}
#endif

/***************************************************************************
 * Function Name    :  MfwdHandleMcastPkt
 *
 * Description      :  This function is registered with IP, so that when IP
 *                     receives multicast Data packet, it can enqueue the
 *                     buffer to MFWD's MDH queueue queue.
 *
 * Global Variables 
 * Referred         :  None
 *
 * Global Variables 
 * Modified         :  None
 *              
 * Input (s)        :  pBuf - Pointer to the Data buffer
 *
 * Output (s)       :  None
 *
 * Returns          :  None
 ****************************************************************************/

VOID
MfwdHandleMcastDataPkt (tCRU_BUF_CHAIN_HEADER * pBuffer)
{
    INT4                i4RetCode = MFWD_FAILURE;
    tMfwdQData         *pQMsg = NULL;

    MFWD_DBG (MFWD_DBG_ENTRY, "Entering Function to post MDP to MFWD\n");

    MFWD_MEMPOOL_ALLOC (MFWD_MSGQ_POOLID, tMfwdQData *, pQMsg, i4RetCode);

    if (pQMsg == NULL)
    {
        MFWD_TRC (MFWD_TRC_FLAG, MFWD_CONTROL_PATH_TRC, MFWD_MOD_NAME,
                  "Failure in allocating memory for the MFWD Q Data\n");
        /* Free the CRU buffer */
        CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
        return;
    }

    pQMsg->u1MsgId = MFWD_MDP_MESSAGE;
    pQMsg->pMsg = pBuffer;

    /* Enqueue the buffer to MFWD task */
    if (OsixQueSend (MFWD_Q_ID,
                     (UINT1 *) &pQMsg, OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        MFWD_TRC (MFWD_TRC_FLAG, MFWD_CONTROL_PATH_TRC, MFWD_MOD_NAME,
                  "Enqueuing Multicast Data pkts from IP to MFWD - FAILED \n");
        /* Free the CRU buffer */
        MFWD_MEMRELEASE (MFWD_MSGQ_POOLID, pQMsg, i4RetCode);
        CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
        return;
    }
    else
    {
        MFWD_DBG (MFWD_DBG_CTRL_FLOW,
                  "A Multicast data packet enqueued to MFWD task \n");
    }

    /* Send a EVENT to MFWD */
    OsixEvtSend (MFWD_TASK_ID, MFWD_MSGQ_EVENT);

    MFWD_DBG (MFWD_DBG_EXIT, "Exiting Function MFWDHandleMcastDataPkt \n");
}

/***************************************************************************
 * Function Name    :  MfwdHandleV6McastDataPkt
 *
 * Description      :  This function is registered with IPv6, so that when IPv6
 *                     receives multicast Data packet, it can enqueue the
 *                     buffer to MFWD's MDH queueue queue.
 *
 * Global Variables 
 * Referred         :  None
 *
 * Global Variables 
 * Modified         :  None
 *              
 * Input (s)        :  pBuf - Pointer to the Data buffer
 *
 * Output (s)       :  None
 *
 * Returns          :  None
 ****************************************************************************/

VOID
MfwdHandleV6McastDataPkt (tCRU_BUF_CHAIN_HEADER * pBuffer)
{
    INT4                i4RetCode = MFWD_FAILURE;
    tMfwdQData         *pQMsg = NULL;

    MFWD_DBG (MFWD_DBG_ENTRY, "Entering Function to post IPv6 MDP to MFWD\n");

    MFWD_MEMPOOL_ALLOC (MFWD_MSGQ_POOLID, tMfwdQData *, pQMsg, i4RetCode);

    if (pQMsg == NULL)
    {
        MFWD_TRC (MFWD_TRC_FLAG, MFWD_CONTROL_PATH_TRC, MFWD_MOD_NAME,
                  "Failure in allocating memory for the MFWD Q Data\n");
        /* Free the CRU buffer */
        CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
        return;
    }

    pQMsg->u1MsgId = MFWD_MDP6_MESSAGE;
    pQMsg->pMsg = pBuffer;

    /* Enqueue the buffer to MFWD task */

    if (OsixQueSend (MFWD_Q_ID,
                     (UINT1 *) &pQMsg, OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        MFWD_TRC (MFWD_TRC_FLAG, MFWD_CONTROL_PATH_TRC, MFWD_MOD_NAME,
                  "Enqueuing Multicast Data pkts from IPv6 to MFWD - FAILED \n");
        /* Free the CRU buffer */
        MFWD_MEMRELEASE (MFWD_MSGQ_POOLID, pQMsg, i4RetCode);
        CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
        return;
    }
    else
    {
        MFWD_DBG (MFWD_DBG_CTRL_FLOW,
                  "A Multicast data packet enqueued to MFWD task \n");
    }

    /* Send a EVENT to MFWD */
    OsixEvtSend (MFWD_TASK_ID, MFWD_MSGQ_EVENT);

    MFWD_DBG (MFWD_DBG_EXIT, "Exiting Function MFWDHandleV6McastDataPkt \n");
}

/***************************************************************************
 * Function Name    :  MfwdPostMrpMsgToMfwd
 *
 * Description      :  This function is posts the mRP update messages on to
 *                     MFWD's MRP and sends an MRP update event to MFWD
 *
 * Global Variables 
 * Referred         :  None
 *
 * Global Variables 
 * Modified         :  None
 *              
 * Input (s)        :  pBuf - Pointer to the Data buffer
 *
 * Output (s)       :  None
 *
 * Returns          :  None
 ****************************************************************************/

INT4
MfwdPostMrpMsgToMfwd (tCRU_BUF_CHAIN_HEADER * pBuffer)
{
    INT4                i4Status = MFWD_FAILURE;
    tMfwdQData         *pQMsg = NULL;

    MFWD_DBG (MFWD_DBG_ENTRY, "Entering Function to post MRP Update to MFWD\n");

    MFWD_MEMPOOL_ALLOC (MFWD_MSGQ_POOLID, tMfwdQData *, pQMsg, i4Status);

    if (pQMsg == NULL)
    {
        MFWD_TRC (MFWD_TRC_FLAG, MFWD_CONTROL_PATH_TRC, MFWD_MOD_NAME,
                  "Failure in allocating memory for the MFWD Q Data\n");
        /* Free the CRU buffer */
        CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
        return MFWD_FAILURE;
    }

    pQMsg->u1MsgId = MFWD_MRP_MESSAGE;
    pQMsg->pMsg = pBuffer;

    /* Enqueue the buffer to MFWD task */

    if (OsixQueSend (MFWD_CQ_ID,
                     (UINT1 *) &pQMsg, OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        /* Free the CRU buffer */
        MFWD_TRC (MFWD_TRC_FLAG, MFWD_CONTROL_PATH_TRC, MFWD_MOD_NAME,
                  "Enqueuing MRP Update from MRP to MFWD - FAILED \n");
        MFWD_MEMRELEASE (MFWD_MSGQ_POOLID, pQMsg, i4Status);
        CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
        return MFWD_SUCCESS;
    }
    else
    {
        MFWD_DBG (MFWD_DBG_CTRL_FLOW,
                  "Successfully posted an MRP update to MFWD \n");
    }

    /* Send a EVENT to MFWD */
    OsixEvtSend (MFWD_TASK_ID, MFWD_MSGQ_EVENT);

    MFWD_DBG (MFWD_DBG_EXIT, "Exiting Function to post MRP Update to MFWD\n");
    return MFWD_SUCCESS;
}

/* LAKSHMIVM changed on June1 */
INT4
MfwdGetIp6IfIndexFromPort (UINT4 u4Port, UINT4 *pu4IfIndex)
{
    if (NetIpv4GetCfaIfIndexFromPort (u4Port,
                                      pu4IfIndex) == NETIPV4_FAILURE)
    {
        return MFWD_FAILURE;
    }
    return MFWD_SUCCESS;
}

/*-------------------------------------------------------------------+
 *
 * Function           : MfwdExtractIpv6Hdr
 *
 * Input(s)           : pIp6Hdr, pBuf
 *
 * Output(s)          : None
 *
 * Returns            : MFWD_SUCCESS, MFWD_FAILURE
 *
 * Action :
 * This routine is used to get the IPv6 header in the packet.
 *
+-------------------------------------------------------------------*/
#ifdef __STDC__
INT4
MfwdExtractIpv6Hdr (tIp6Hdr * pIp6Hdr, tCRU_BUF_CHAIN_HEADER * pBuf)
#else
INT4
MfwdExtractIpv6Hdr (pIp6Hdr, pBuf)
     tIp6Hdr            *pIp6Hdr;
     tCRU_BUF_CHAIN_HEADER *pBuf;
#endif
{

    tIp6Hdr             TmpIp6Hdr;
    tIp6Hdr            *pTmpIp6Hdr;
    MEMSET (&TmpIp6Hdr, 0, sizeof (tIp6Hdr));

    pTmpIp6Hdr =
        (tIp6Hdr *) (VOID *) CRU_BUF_Get_DataPtr_IfLinear (pBuf, 0,
                                                           sizeof (tIp6Hdr));
    if (pTmpIp6Hdr == NULL)
    {
        /* The header is not contiguous in the buffer */
        pTmpIp6Hdr = &TmpIp6Hdr;
        /* Copy the header */
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) pTmpIp6Hdr,
                                   0, sizeof (tIp6Hdr));
    }

    MEMCPY (pIp6Hdr, pTmpIp6Hdr, sizeof (tIp6Hdr));
    pIp6Hdr->u2Len = OSIX_NTOHS (pIp6Hdr->u2Len);
    return MFWD_SUCCESS;

}
