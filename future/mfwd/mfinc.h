/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mfinc.h,v 1.11 2014/10/10 11:59:51 siva Exp $
 *
 ********************************************************************/
#ifndef _MFINC_H_
#define _MFINC_H_

#include "lr.h"
#include "ip.h"
#include "cfa.h"
#include "fssocket.h"

#ifdef  NPAPI_WANTED
#include "ipnp.h"
#endif

#include "utilipvx.h"
#include "ipv6.h"
#include "utlshinc.h"

#include "mftdfs.h"
#include "mfwd.h"
#include "mfmrp.h"
#include "mfdefn.h"
#include "mfport.h"
#include "mfmacs.h"
#include "mfproto.h"
#include "mfdebug.h"
#include "mftrace.h"
#include "mfwdsz.h"
#endif
