#ifndef __MFWDCLI_C__
#define __MFWDCLI_C__
/* SOURCE FILE HEADER :
 *
 *  ---------------------------------------------------------------------------
 * |  FILE NAME             : mfwdcli.c                                        |
 * |                                                                           |
 * |  PRINCIPAL AUTHOR      :                                                  |
 * |                                                                           |
 * |  SUBSYSTEM NAME        : CLI                                              |
 * |                                                                           |
 * |  MODULE NAME           : MFWD configuration                               |
 * |                                                                           |
 * |  LANGUAGE              : C                                                |
 * |                                                                           |
 * |  TARGET ENVIRONMENT    :                                                  |
 * |                                                                           |
 * |  DATE OF FIRST RELEASE :                                                  |
 * |                                                                           |
 * |  DESCRIPTION           : Action routines for set/get objects in           | 
 * |                          fsmfwd.mib                                       |
 * |   $Id: mfwdcli.c,v 1.19 2014/12/10 12:53:00 siva Exp $                                                                           |
 *  ---------------------------------------------------------------------------
 *
 */

/********************************************************/
/*            HEADER FILES                              */
/********************************************************/

#include "mfinc.h"
#include "mfwdcli.h"
#include "mfwdclipt.h"
#include "fsmfwlow.h"
#include "mfwdcmlow.h"
#include "mfwdcmwr.h"
#include "snmcdefn.h"
#include "mfwdcmcli.h"

#ifdef MFWD_WANTED

/* Cli command action routine for PIM */
extern tMfwdContextStructure gMfwdContext;
/*********************************************************************
*  Function Name : cli_process_mfwd_cmd() 
*  Description   : This function processes the MFWD Related Commands ,
*                  Pre-validates the Inputs before sending to the
*                  MFWD-CLI Module , Fills up the MFWD Config Params
*                  Structure with the input values given by the User,
*                  fills up the Command in the Input Message, and
*                  calls MfwdCliCmdActionRoutine() for further Processing 
*                  of the User Commands/Inputs.
*  Input(s)      : UserInputs/Command in va_alist. 
*  Output(s)     : None.
*  Return Values : CLI_SUCCESS/CLI_FAILURE
*********************************************************************/

INT1
cli_process_mfwd_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT1              *args[MFWD_MAX_ARGS];
    UINT1              *pu1Inst = NULL;
    INT1                argno = 0;
    INT4                i4Status = 0;
    INT4                i4RetStatus = 0;
    UINT4               u4IfIndex;
    UINT4               u4ErrCode;

    va_start (ap, u4Command);

    /* third arguement is always interface name/index */
    pu1Inst = va_arg (ap, UINT1 *);

    if (pu1Inst != NULL)
    {
        u4IfIndex = CLI_PTR_TO_U4 (pu1Inst);
    }

    /* Walk through the rest of the arguements and store in args array. 
     * Store 10 arguements at the max. This is because mfwd commands do not
     * take more than sixteen inputs from the command line. Another reason to
     * store is in some cases first input may be optional but user may give 
     * second input. In that case first arg will be null and second arg only 
     * has value */
    while (1)
    {
        args[argno++] = va_arg (ap, UINT1 *);
        if (argno == MFWD_MAX_ARGS)
            break;
    }
    va_end (ap);

    switch (u4Command)
    {
        case MFWD_CLI_IP_MULTICAST_ROUTING:
            /* args[0] Multicast Routing Enable (1) , Disable (2) */

            i4Status = (CLI_PTR_TO_I4 (args[0]) == CLI_ENABLE) ?
                MFWD_STATUS_ENABLED : MFWD_STATUS_DISABLED;
            i4RetStatus = MfwdIpMulticastRouting (CliHandle, i4Status);

            break;

        case MFWD_CLI_SHOW_IP_MULTICAST_ROUTE:
            i4RetStatus = MfwdShowMulticastRoute (CliHandle);
            break;

        case MFWD_CLI_SHOW_IPV6_MULTICAST_ROUTE:
            i4RetStatus = MfwdShowIPv6MulticastRoute (CliHandle);
            break;

        case MFWD_CLI_TRACE:
            i4RetStatus = MfwdSetTrace (CliHandle, CLI_PTR_TO_I4 (args[0]));
            break;

        default:
            CliPrintf (CliHandle, "%% Unknown command \r\n");
            return CLI_FAILURE;
    }

    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_MFWD_MAX_ERR))
        {
            CliPrintf (CliHandle, "%% %s", MfwdCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }

    UNUSED_PARAM (u4IfIndex);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : MfwdIpMulticastRouting                             */
/*                                                                           */
/*     DESCRIPTION      : This function will enable or disable IP multicast  */
/*                        forwarding                                         */
/*                                                                           */
/*     INPUT            : i4MfwdStatus                                       */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
MfwdIpMulticastRouting (tCliHandle CliHandle, INT4 i4MfwdStatus)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2IpCmnMRouteEnable (&u4ErrorCode, i4MfwdStatus) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetIpCmnMRouteEnable (i4MfwdStatus) == SNMP_FAILURE)
    {

        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : MfwdShowIPv6MulticastRoute                         */
/*                                                                           */
/*     DESCRIPTION      : This function will display IPV6 multicast          */
/*                        forwarding table                                   */
/*                                                                           */
/*     INPUT            : None                                               */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
MfwdShowIPv6MulticastRoute (tCliHandle CliHandle)
{
    UINT4               u4PagingStatus = CLI_SUCCESS;
    tTMO_SLL_NODE      *pRtNode = NULL;
    tTMO_SLL_NODE      *pNxtRtNode = NULL;
    tMfwdRtEntry       *pRtEntry = NULL;
    tMfwdOifNode       *pOifNode = NULL;
    UINT1               au1InterfaceName[MAX_ADDR_BUFFER];
    UINT1               au1SrcAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1GrpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1RtAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1NbrAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1OifState[3][MAX_OIFSTATE_BUFFER] =
        { "\0", "Pruned\0", "Forwarding\0" };

    MEMSET (au1SrcAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1GrpAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1RtAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1NbrAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    pRtNode = TMO_SLL_First (&gMfwdContext.MrtGetNextList);
    if (pRtNode == NULL)
    {
        CliPrintf (CliHandle, "No Entries Found\r\n");
        return CLI_SUCCESS;
    }
    else
    {
        CliPrintf (CliHandle, "MFWD Multicast Forwarding Table\r\n");
        CliPrintf (CliHandle, "---------------------------------\r\n");
    }

    pRtEntry = MFWD_GET_BASE_PTR (tMfwdRtEntry, GetNextLink, pRtNode);

    while (pRtEntry != NULL)
    {
        if(pRtEntry->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
        {
	    MEMCPY (au1SrcAddr, pRtEntry->pSrcDescNode->SrcAddr.au1Addr,
		    IPVX_IPV6_ADDR_LEN);
	    CliPrintf (CliHandle, "(%s",
		    Ip6PrintNtop ((tIp6Addr *) (VOID *) au1SrcAddr));

	    MEMCPY (au1GrpAddr, pRtEntry->GrpAddr.au1Addr, IPVX_IPV6_ADDR_LEN);
	    CliPrintf (CliHandle, ", %s)\r\n",
		    Ip6PrintNtop ((tIp6Addr *) (VOID *) au1GrpAddr));

	    if (pRtEntry->pSrcDescNode->i4RtMask == 0)
	    {
		MEMCPY (au1RtAddr, pRtEntry->pSrcDescNode->RtAddr.au1Addr,
			IPVX_IPV6_ADDR_LEN);
		CliPrintf (CliHandle, " RT Address: %s ",
			Ip6PrintNtop ((tIp6Addr *) (VOID *) au1RtAddr));
	    }

	    MFWD_GET_IF_NAME ((UINT2) pRtEntry->pSrcDescNode->u4Iif,
		    pRtEntry->pSrcDescNode->SrcAddr.u1Afi,
		    au1InterfaceName);

	    MEMCPY (au1NbrAddr, pRtEntry->pSrcDescNode->UpStrmNbr.au1Addr,
		    IPVX_IPV6_ADDR_LEN);
	    CliPrintf (CliHandle, " Incoming Interface: %s, RPF nbr: %s\r\n",
		    au1InterfaceName,
		    Ip6PrintNtop ((tIp6Addr *) (VOID *) au1NbrAddr));

	    CliPrintf (CliHandle, "  Pkts On IIF: %d, RPF Failure Pkts %d\r\n",
		    pRtEntry->u4FwdCnt, pRtEntry->u4RpfFailCnt);

	    if (TMO_SLL_Count (&(pRtEntry->OifList)) != 0)
	    {
		CliPrintf (CliHandle, "    Outgoing InterfaceList: \r\n");

		TMO_SLL_Scan (&(pRtEntry->OifList), pOifNode, tMfwdOifNode *)
		{
		    MFWD_GET_IF_NAME ((UINT2) pOifNode->u4OifIndex,
			    pRtEntry->pSrcDescNode->SrcAddr.u1Afi,
			    au1InterfaceName);

		    CliPrintf (CliHandle, "      %s, %s, "
			    "Packets Sent %d\r\n",
			    au1InterfaceName,
			    au1OifState[pOifNode->u4OifState],
			    pOifNode->u4FwdCnt);
		}
	    }
	    else
	    {
		CliPrintf (CliHandle, "   Outgoing InterfaceList:NULL\r\n");
	    }
        }
        pNxtRtNode = TMO_SLL_Next ((&gMfwdContext.MrtGetNextList), pRtNode);
        if (pNxtRtNode == NULL)
        {
            pRtEntry = NULL;
        }
        else
        {
            pRtEntry =
                MFWD_GET_BASE_PTR (tMfwdRtEntry, GetNextLink, pNxtRtNode);
            pRtNode = pNxtRtNode;
            u4PagingStatus = CliPrintf (CliHandle, "\r\n");
        }

        if (u4PagingStatus == CLI_FAILURE)
        {
            break;
        }
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : MfwdShowMulticastRoute                             */
/*                                                                           */
/*     DESCRIPTION      : This function will display multicast               */
/*                        forwarding table                                   */
/*                                                                           */
/*     INPUT            : None                                               */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
MfwdShowMulticastRoute (tCliHandle CliHandle)
{
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT4               u4Addr = CLI_SUCCESS;
    tTMO_SLL_NODE      *pRtNode = NULL;
    tTMO_SLL_NODE      *pNxtRtNode = NULL;
    tMfwdRtEntry       *pRtEntry = NULL;
    tMfwdOifNode       *pOifNode = NULL;
    tMfwdIifNode       *pIifNode = NULL;
    CHR1               *pu1String = NULL;
    UINT1               au1InterfaceName[MAX_ADDR_BUFFER];
    UINT1               au1OifState[3][MAX_OIFSTATE_BUFFER] =
        { "\0", "Pruned\0", "Forwarding\0" };

    pRtNode = TMO_SLL_First (&gMfwdContext.MrtGetNextList);
    if (pRtNode == NULL)
    {
        CliPrintf (CliHandle, "No Entries Found\r\n");
        return CLI_SUCCESS;
    }
    else
    {
        CliPrintf (CliHandle, "MFWD Multicast Forwarding Table\r\n");
        CliPrintf (CliHandle, "---------------------------------\r\n");
    }

    pRtEntry = MFWD_GET_BASE_PTR (tMfwdRtEntry, GetNextLink, pRtNode);

    while (pRtEntry != NULL)
    {
        if(pRtEntry->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
        {
	    if ((pRtEntry->u1RtType != MFWD_STAR_G_ROUTE)
		    || (TMO_SLL_Count (&(pRtEntry->IifList)) == 0))
	    {
		PTR_FETCH4 (u4Addr, pRtEntry->pSrcDescNode->SrcAddr.au1Addr);
		CLI_CONVERT_IPADDR_TO_STR (pu1String, u4Addr);
		CliPrintf (CliHandle, "(%s,", pu1String);
	    }
	    else
	    {
		CliPrintf (CliHandle, "(*,");
	    }

	    PTR_FETCH4 (u4Addr, pRtEntry->GrpAddr.au1Addr);
	    CLI_CONVERT_IPADDR_TO_STR (pu1String, u4Addr);

	    CliPrintf (CliHandle, "%s)", pu1String);

	    if ((pRtEntry->u1RtType != MFWD_STAR_G_ROUTE)
		    || (TMO_SLL_Count (&(pRtEntry->IifList)) == 0))
	    {
		if (pRtEntry->pSrcDescNode->i4RtMask == 0)
		{
		    PTR_FETCH4 (u4Addr, pRtEntry->pSrcDescNode->RtAddr.au1Addr);
		    CLI_CONVERT_IPADDR_TO_STR (pu1String, u4Addr);
		    CliPrintf (CliHandle, " RT Address: %s ", pu1String);
		}

		MFWD_GET_IF_NAME ((UINT2) pRtEntry->pSrcDescNode->u4Iif,
			pRtEntry->pSrcDescNode->SrcAddr.u1Afi,
			au1InterfaceName);

		PTR_FETCH4 (u4Addr, pRtEntry->pSrcDescNode->UpStrmNbr.au1Addr);
		CLI_CONVERT_IPADDR_TO_STR (pu1String, u4Addr);
		CliPrintf (CliHandle, " Incoming Interface: %s, RPF nbr %s\r\n",
			au1InterfaceName, pu1String);
		CliPrintf (CliHandle, "  Pkts On IIF: %d, RPF Failure Pkts %d\r\n",
			pRtEntry->u4FwdCnt, pRtEntry->u4RpfFailCnt);
	    }
	    else
	    {
		if (TMO_SLL_Count (&(pRtEntry->IifList)) != 0)
		{
		    CliPrintf (CliHandle, "    Incoming InterfaceList: \r\n");

		    TMO_SLL_Scan (&(pRtEntry->IifList), pIifNode, tMfwdIifNode *)
		    {
			MFWD_GET_IF_NAME ((UINT2) pIifNode->u4IifIndex,
				pRtEntry->GrpAddr.u1Afi,
				au1InterfaceName);
			CliPrintf (CliHandle, "     %s\r\n ", au1InterfaceName);
		    }
		}
		else
		{
		    CliPrintf (CliHandle, "   Incoming InterfaceList:NULL\r\n");
		}
	    }

	    if (TMO_SLL_Count (&(pRtEntry->OifList)) != 0)
	    {
		CliPrintf (CliHandle, "    Outgoing InterfaceList: \r\n");

		TMO_SLL_Scan (&(pRtEntry->OifList), pOifNode, tMfwdOifNode *)
		{
		    MFWD_GET_IF_NAME ((UINT2) pOifNode->u4OifIndex,
			    pRtEntry->GrpAddr.u1Afi, au1InterfaceName);

		    CliPrintf (CliHandle, "      %s, %s, "
			    "Packets Sent %d\r\n",
			    au1InterfaceName,
			    au1OifState[pOifNode->u4OifState],
			    pOifNode->u4FwdCnt);
		}
	    }
	    else
	    {
		CliPrintf (CliHandle, "   Outgoing InterfaceList:NULL\r\n");
	    }
        }

        pNxtRtNode = TMO_SLL_Next ((&gMfwdContext.MrtGetNextList), pRtNode);
        if (pNxtRtNode == NULL)
        {
            pRtEntry = NULL;
        }
        else
        {
            pRtEntry =
                MFWD_GET_BASE_PTR (tMfwdRtEntry, GetNextLink, pNxtRtNode);
            pRtNode = pNxtRtNode;
            u4PagingStatus = CliPrintf (CliHandle, "\r\n");
        }

        if (u4PagingStatus == CLI_FAILURE)
        {
            break;
        }
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : MfwdSetTrace                                       */
/*                                                                           */
/*     DESCRIPTION      : This function will enable or disable trace         */
/*                                                                           */
/*     INPUT            : i4TraceStatus  - Trace Flag                        */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
MfwdSetTrace (tCliHandle CliHandle, INT4 i4TraceStatus)
{

    if (i4TraceStatus >= MFWD_ZERO)
    {
        gMfwdContext.u4GlobalTrace = (UINT4) i4TraceStatus;
    }
    else
    {
        i4TraceStatus &= MFWD_MAX_INT4;
        gMfwdContext.u4GlobalTrace &= (~i4TraceStatus);
    }

    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : IssMfwdShowDebugging                               */
/*                                                                           */
/*     DESCRIPTION      : This function prints the MFWD  debug level         */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
VOID
IssMfwdShowDebugging (tCliHandle CliHandle)
{
    INT4                i4DbgLevel = 0;

    i4DbgLevel = gMfwdContext.u4GlobalTrace;
    if (i4DbgLevel == 0)
    {
        return;
    }
    CliPrintf (CliHandle, "\rMFWD :");

    if ((i4DbgLevel & MFWD_IO_MODULE) != 0)
    {
        CliPrintf (CliHandle, "\r\n  MFWD Input/Output debugging is on");
    }
    if ((i4DbgLevel & MFWD_MRP_MODULE) != 0)
    {
        CliPrintf (CliHandle,
                   "\r\n  MFWD Multicast routing table debugging is on");
    }
    if ((i4DbgLevel & MFWD_MDH_MODULE) != 0)
    {
        CliPrintf (CliHandle,
                   "\r\n  MFWD Multicast data handling debugging is on");
    }
    if ((i4DbgLevel & MFWD_MGMT_MODULE) != 0)
    {
        CliPrintf (CliHandle, "\r\n  MFWD Management debugging is on");
    }

    CliPrintf (CliHandle, "\r\n");
    return;
}

#endif /* MFWD_WANTED */
#endif
