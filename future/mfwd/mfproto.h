/*$Id: mfproto.h,v 1.12 2014/08/23 11:58:42 siva Exp $*/
#ifndef _MFWD_PROTO_H_
#define _MFWD_PROTO_H_

/* prototypes for the functions in the mfinput.c file */



INT4 MfwdInit ARG_LIST ((VOID));

INT4 MfwdInitInterfaceInfo ARG_LIST ((VOID)); 

INT4 MfwdInitSocketInfo ARG_LIST ((VOID));

INT4 MfwdShutdown ARG_LIST ((VOID));

VOID  MfwdInputHandleMfwdStatusChg ARG_LIST ((VOID));


INT4 MfwdInputDeRegisterMRP ARG_LIST ((tCRU_BUF_CHAIN_HEADER *));
INT4 MfwdInputRegisterMRP ARG_LIST ((tCRU_BUF_CHAIN_HEADER *)); 

INT4  MfwdInputHandleMrpMesg ARG_LIST ((tCRU_BUF_CHAIN_HEADER *pMrpBuffer));

INT4  MfwdInputHandleMdpMesg ARG_LIST ((tCRU_BUF_CHAIN_HEADER *pMdpBuf));
INT4  MfwdInputHandleMdp6Mesg ARG_LIST ((tCRU_BUF_CHAIN_HEADER *pMdpBuf));

const char* MfwdGetModule(UINT2 u2TraceModule);

/* Prototypes for the functions in the oim module */


INT4 MfwdOimActivateOwner ARG_LIST ((UINT2 u2OwnerId)); 
        
INT4 MfwdOimDeActivateOwner ARG_LIST ((UINT2 u2OwnerId)); 

VOID MfwdOimDeActivateOwnerMemPool ARG_LIST ((VOID));

INT4 MfwdOimHandleUpd ARG_LIST ((tMrpUpdMesgHdr MrpUpdHdr,
                       tCRU_BUF_CHAIN_HEADER *pIfUpdBuf));

INT4 MfwdOimDeleteIfaces ARG_LIST ((UINT2 u2OwnerId, UINT4 u4IfCount, UINT1 u1AddrType,
                   tCRU_BUF_CHAIN_HEADER *pIfUpdBuf));
VOID
MfwdDeleteInterfaceNode ARG_LIST ((tMfwdInterfaceNode *pIfNode));

INT4 MfwdOimAddIfaces ARG_LIST ((UINT2 u2OwnerId, UINT4 u4IfCount, UINT1 u1AddrType,
                   tCRU_BUF_CHAIN_HEADER *pIfUpdBuf));

tMfwdInterfaceNode *MfwdGetInterfaceNode ARG_LIST ((UINT4 u4IfIndex));

VOID
MfwdOimAddNewIfToIfTable ARG_LIST ((UINT2 u2OwnerId, 
                                    tMfwdInterfaceNode *pNewIfNode));

tMfwdInterfaceNode *
MfwdOimGetInterfaceNode ARG_LIST ((UINT4 u4IfIndex, UINT1 u1AddrType));

UINT4  
MrtGrpNodeAddFn ARG_LIST ((tTMO_HASH_NODE *pCurNode, UINT1 *pu1GrpAddr));

VOID GrpNodeFreeFn (tTMO_HASH_NODE *pHashNode);

VOID
MfwdGetNewSemName (UINT1 *au1semName);

VOID
MfwdStoreSemName(UINT1 *au1SemName);

/* prototypes for the functions in the MDH module */
/*------------------------------------------------*/


INT4 MfwdMdhForwardMDP ARG_LIST ((UINT2 u2OwnerId, 
                                  tCRU_BUF_CHAIN_HEADER *pMdpBuf,
                                  tMfwdInterfaceNode *pIfNode, 
                                  tIPvXAddr SrcAddr, tIPvXAddr Dest));

INT4 MfwdMdhHandleWrongIif ARG_LIST ((UINT2 u2OwnerId, tMfwdMrpMDPMsg *pMrpMesg,
                                      tCRU_BUF_CHAIN_HEADER *pMdpBuf, UINT1 u1AddrType));

INT4 MfwdMdhHandleCacheMiss ARG_LIST ((UINT2 u2OwnerId, 
                                       tMfwdInterfaceNode *pIfNode,
                                       tCRU_BUF_CHAIN_HEADER *pMdpBuf,
                                       tMfwdMrpMDPMsg *pMrpMesg));

INT4 MfwdMdhHandleCacheMissData ARG_LIST ((UINT2 u2OwnerId, 
                                           tTMO_SLL *pOifList, 
                                           UINT4 u4Iif, tIPvXAddr SrcAddr, 
                                           tIPvXAddr GrpAddr));

INT4  MfwdMdhInformMrp ARG_LIST (
                        (VOID (*DataPktCallBkFn)(tCRU_BUF_CHAIN_HEADER *), 
                          tMfwdMrpMDPMsg *pMrpMsg, 
                          tCRU_BUF_CHAIN_HEADER *pMdpBuf));

void MfwdMdhDisableCmdb ARG_LIST ((VOID));
void MfwdMdhHandleCmdbTmrExp ARG_LIST ((UINT2 u2OwnerID)); 

void MfwdEnableCmdbStatus ARG_LIST ((void));

/* prototypes for the MRT module */
/*------------------------------*/

INT4 MfwdMrtHandleUpd ARG_LIST ((tMrpUpdMesgHdr MrpUpdHdr,
                       tCRU_BUF_CHAIN_HEADER *pBuffer));

VOID MfwdMrtDeleteWcRtEntries ARG_LIST ((UINT2 u2OwnerId, tIPvXAddr RtAddr));
        
INT4 MfwdMrtDeleteRtEntry ARG_LIST ((UINT2 u2OwnerId, tIPvXAddr GrpAddr, 
                                     tIPvXAddr SrcAddr, tIPvXAddr RtAddr));

VOID  MfwdMrtReleaseRtEntry ARG_LIST ((tMfwdRtEntry *pRtEntry));

INT4 
MfwdMrtCreateRtEntry ARG_LIST ((UINT2 u2OwnerId, tMrtUpdData *pUpdData,
                                tCRU_BUF_CHAIN_HEADER *pUpdBuf));

VOID
MfwdMrtInitRtEntry ARG_LIST ((UINT2 u2OwnerId, tMfwdRtEntry *pNewRtEntry, 
                              tMrtUpdData *pUpdData));
                    
VOID
MfwdMrtUpdateWcRtEntries ARG_LIST ((UINT2 u2OwnerId, UINT1 u2UpdCmd, 
                       tMrtUpdData *pUpdData, tCRU_BUF_CHAIN_HEADER *pUpdBuf));

INT4 MfwdMrtUpdateRtEntry ARG_LIST ((UINT2 u2OwnerId, UINT1 u2UpdCmd,
                                     tMrtUpdData *pUpdData,
                            tCRU_BUF_CHAIN_HEADER *pUpdBuf, UINT2 u2UpdSubCmd));

VOID MfwdMrtUpdateIif ARG_LIST ((UINT2 u2OwnerId, tMfwdRtEntry *pUpdEntry,
                  tMrtUpdData *pUpdData, tCRU_BUF_CHAIN_HEADER *pUpdBuf));

INT4 MfwdMrtAddOifList ARG_LIST ((tMfwdRtEntry *pUpdEntry,
                        UINT4 u4OifCount,
                    tCRU_BUF_CHAIN_HEADER *pUpdBuf));

INT4 MfwdMrtAddIifList ARG_LIST ((tMfwdRtEntry *pUpdEntry,
                        UINT4 u4OifCount, UINT4 u4IifCount,tCRU_BUF_CHAIN_HEADER *pUpdBuf));

INT4 MfwdMrtDeleteOifList ARG_LIST ((tMfwdRtEntry *pUpdEntry,
                                     UINT4 u4OifCount,
                   tCRU_BUF_CHAIN_HEADER *pUpdBuf));

INT4 MfwdMrtDeleteIifList ARG_LIST ((tMfwdRtEntry *pUpdEntry,
                                     UINT4 u4OifCount, UINT4 u4IifCount,
                   tCRU_BUF_CHAIN_HEADER *pUpdBuf));

INT4 MfwdMrtDeleteOif ARG_LIST ((tMfwdRtEntry *pRtEntry, 
                                tMfwdOifNode *pOifNode));

INT4 MfwdMrtDeleteIif ARG_LIST ((tMfwdRtEntry *pRtEntry, 
                                tMfwdIifNode *pIifNode));

INT4 MfwdMrtSetOifState ARG_LIST ((tMfwdRtEntry *pUpdEntry, 
                                   UINT4 u4OifCount,
                    tCRU_BUF_CHAIN_HEADER *pUpdBuf));

INT4 MfwdMrtGetFwdInfo ARG_LIST ((UINT2 u2OwnerId, tIPvXAddr SrcAddr, 
                                 tIPvXAddr GrpAddr, 
                        UINT4 u4Iif, tMfwdFwdInfo *pMfwdFwdInfo));

void MfwdMrtUpdFwdCount ARG_LIST ((UINT2 u2OwnerId, UINT1 u1EntryType, 
                   tMfwdRtEntry *pFwdEntry, tIPvXAddr SrcAddr, tIPvXAddr GrpAddr));

INT4 MfwdMrtGetStStEntry ARG_LIST ((UINT2 u2OwnerId, UINT4 u4Iif, 
                                            tMfwdRtEntry **pRtEntry));


INT4 MfwdSendMcastDataPkt ARG_LIST ((tCRU_BUF_CHAIN_HEADER * pBuffer,
                                     UINT4 u4GrpAddr,
                                     tTMO_SLL *pOifList, UINT4 u4IfIndex));


INT4
MfwdSendIpv6McastDataPkt (tCRU_BUF_CHAIN_HEADER * pBuffer, UINT1 *pu1GrpAddr,
                      tTMO_SLL * pOifList, UINT4 u4IifIndex);

VOID
MfwdHandleMcastDataPkt ARG_LIST ((tCRU_BUF_CHAIN_HEADER * pBuffer));
VOID
MfwdHandleV6McastDataPkt ARG_LIST ((tCRU_BUF_CHAIN_HEADER * pBuffer));

tMfwdRtEntry *
MfwdMrtGetExactRtEntry ARG_LIST ((UINT2 u2OwnerId, tIPvXAddr GrpAddr, 
                                 tIPvXAddr SrcAddr, UINT4 u4SrcMask));

void
MfwdMrtUpdGetNextList ARG_LIST ((tMfwdRtEntry *pnewRtEntry));

INT4
MfwdExtractIpHdr (t_IP * pIp, tCRU_BUF_CHAIN_HEADER * pBuf);


INT4
MfwdExtractIp6Hdr (tIp6Hdr * pIp6, tCRU_BUF_CHAIN_HEADER * pBuf);

INT4
MfwdGetIp6IfIndexFromPort(UINT4 u4Port, UINT4 *pu4IfIndex);

INT4
MfwdExtractIpv6Hdr (tIp6Hdr * pIp6Hdr, tCRU_BUF_CHAIN_HEADER * pBuf);

extern UINT1 * Ip6PrintNtop (tIp6Addr * pAddr);

INT4
MfwdIpv6GetIfInfo (UINT4 u4IfIndex, tNetIpv6IfInfo * pNetIpv6IfInfo);
#endif
