#ifndef DEBUG_MFWD
#define DEBUG_MFWD

#define MFWD_DBG_FLAG	gMfwdContext.u4GlobalDebug

#define MFWD_DBG(Value, Fmt)           UtlTrcLog(MFWD_DBG_FLAG, \
                                                Value,        \
                                                "MFWD",        \
                                                Fmt);

#define MFWD_DBG1(Value, Fmt, Arg)     UtlTrcLog(MFWD_DBG_FLAG, \
                                                Value,        \
                                                "MFWD",        \
                                                Fmt,          \
                                                Arg);

#define MFWD_DBG2(Value, Fmt, Arg1, Arg2)                      \
                                      UtlTrcLog(MFWD_DBG_FLAG, \
                                                Value,        \
                                                "MFWD",        \
                                                Fmt,          \
                                                Arg1, Arg2);

#define MFWD_DBG3(Value, Fmt, Arg1, Arg2, Arg3)                \
                                      UtlTrcLog(MFWD_DBG_FLAG, \
                                                Value,        \
                                                "MFWD",        \
                                                Fmt,          \
                                                Arg1, Arg2, Arg3);

#define MFWD_DBG4(Value, Fmt, Arg1, Arg2, Arg3, Arg4)          \
                                      UtlTrcLog(MFWD_DBG_FLAG, \
                                                Value,        \
                                                "MFWD",        \
                                                Fmt,          \
                                                Arg1, Arg2,   \
                                                Arg3, Arg4);

#define MFWD_DBG5(Value, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5)    \
                                      UtlTrcLog(MFWD_DBG_FLAG, \
                                                Value,        \
                                                "MFWD",        \
                                                Fmt,          \
                                                Arg1, Arg2, Arg3, \
                                                Arg4, Arg5);

#define MFWD_DBG6(Value, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)  \
                                      UtlTrcLog(MFWD_DBG_FLAG, \
                                                Value,        \
                                                "MFWD",        \
                                                Fmt,          \
                                                Arg1, Arg2, Arg3, \
                                                Arg4, Arg5, Arg6);

#else /* If the DEBUG is OFF */

#define MFWD_DBG(Value, Fmt)
#define MFWD_DBG1(Value, Fmt, Arg)
#define MFWD_DBG2(Value, Fmt, Arg1, Arg2)
#define MFWD_DBG3(Value, Fmt, Arg1, Arg2, Arg3)
#define MFWD_DBG4(Value, Fmt, Arg1, Arg2, Arg3, Arg4)
#define MFWD_DBG5(Value, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5)
#define MFWD_DBG6(Value, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)
#endif /* DEBUG_MFWD */

/* Debug categories */
#define MFWD_DBG_OFF                   UTL_DBG_OFF
#define MFWD_DBG_ALL                   UTL_DBG_ALL
#define MFWD_DBG_INIT_SHUTDN           UTL_DBG_INIT_SHUTDN
#define MFWD_DBG_CTRL_IF               UTL_DBG_CTRL_IF
#define MFWD_DBG_STATUS_IF             UTL_DBG_STATUS_IF
#define MFWD_DBG_MGMT                  UTL_DBG_SNMP_IF
#define MFWD_DBG_BUF_IF                UTL_DBG_BUF_IF
#define MFWD_DBG_MEM_IF                UTL_DBG_MEM_IF
#define MFWD_DBG_RX                    UTL_DBG_RX
#define MFWD_DBG_TX                    UTL_DBG_TX
#define MFWD_DBG_TMR_IF                UTL_DBG_TMR_IF

#define MFWD_DBG_ENTRY                  0x00010000
#define MFWD_DBG_EXIT                   0x00020000
#define MFWD_DBG_PKT_EXTRACT            0x00080000
#define MFWD_DBG_CTRL_FLOW              0x00100000
#define MFWD_DBG_IF                     0x00200000
#define MFWD_DBG_ERROR                  0x04000000
#define MFWD_DBG_MEM                    0x08000000
#define MFWD_DBG_FAILURE                0x10000000

/************************* END OF DBG ***************************************/
