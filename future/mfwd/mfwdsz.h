/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mfwdsz.h,v 1.2 2014/08/23 11:58:43 siva Exp $
 *
 ********************************************************************/
enum {
    MAX_MFWD_CACHE_MISS_DB_NODES_SIZING_ID,
    MAX_MFWD_CMD_DATA_PKT_SIZING_ID,
    MAX_MFWD_GRP_NODE_SIZING_ID,
    MAX_MFWD_INTERFACES_NODE_SIZING_ID,
    MAX_MFWD_OWNER_INFO_TABLE_SIZING_ID,
    MAX_MFWD_OWNER_NODE_SIZING_ID,
    MAX_MFWD_OWNER_OIF_NEXTHOP_SIZING_ID,
    MAX_MFWD_OWNER_OIFS_SIZING_ID,
    MAX_MFWD_OWNER_RTENTRIES_SIZING_ID,
    MAX_MFWD_Q_DEPTH_SIZING_ID,
    MAX_MFWD_SRC_NODE_SIZING_ID,
    MAX_MFWD_OWNER_IIFS_SIZING_ID,
    MFWD_MAX_SIZING_ID
};


#ifdef  _MFWDSZ_C
tMemPoolId MFWDMemPoolIds[ MFWD_MAX_SIZING_ID];
INT4  MfwdSizingMemCreateMemPools(VOID);
VOID  MfwdSizingMemDeleteMemPools(VOID);
INT4  MfwdSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _MFWDSZ_C  */
extern tMemPoolId MFWDMemPoolIds[ ];
extern INT4  MfwdSizingMemCreateMemPools(VOID);
extern VOID  MfwdSizingMemDeleteMemPools(VOID);
#endif /*  _MFWDSZ_C  */


#ifdef  _MFWDSZ_C
tFsModSizingParams FsMFWDSizingParams [] = {
{ "tCacheMissDBNode", "MAX_MFWD_CACHE_MISS_DB_NODES", sizeof(tCacheMissDBNode),MAX_MFWD_CACHE_MISS_DB_NODES, MAX_MFWD_CACHE_MISS_DB_NODES,0 },
{ "tCMDataPktNode", "MAX_MFWD_CMD_DATA_PKT", sizeof(tCMDataPktNode),MAX_MFWD_CMD_DATA_PKT, MAX_MFWD_CMD_DATA_PKT,0 },
{ "tMfwdGrpNode", "MAX_MFWD_GRP_NODE", sizeof(tMfwdGrpNode),MAX_MFWD_GRP_NODE, MAX_MFWD_GRP_NODE,0 },
{ "tMfwdInterfaceNode", "MAX_MFWD_INTERFACES_NODE", sizeof(tMfwdInterfaceNode),MAX_MFWD_INTERFACES_NODE, MAX_MFWD_INTERFACES_NODE,0 },
{ "tMfwdOwnerInfoTable", "MAX_MFWD_OWNER_INFO_TABLE", sizeof(tMfwdOwnerInfoTable),MAX_MFWD_OWNER_INFO_TABLE, MAX_MFWD_OWNER_INFO_TABLE,0 },
{ "tMfwdOwnerInfoNode", "MAX_MFWD_OWNER_NODE", sizeof(tMfwdOwnerInfoNode),MAX_MFWD_OWNER_NODE, MAX_MFWD_OWNER_NODE,0 },
{ "tMfwdOifNextHopNode", "MAX_MFWD_OWNER_OIF_NEXTHOP", sizeof(tMfwdOifNextHopNode),MAX_MFWD_OWNER_OIF_NEXTHOP, MAX_MFWD_OWNER_OIF_NEXTHOP,0 },
{ "tMfwdOifNode", "MAX_MFWD_OWNER_OIFS", sizeof(tMfwdOifNode),MAX_MFWD_OWNER_OIFS, MAX_MFWD_OWNER_OIFS,0 },
{ "tMfwdRtEntry", "MAX_MFWD_OWNER_RTENTRIES", sizeof(tMfwdRtEntry),MAX_MFWD_OWNER_RTENTRIES, MAX_MFWD_OWNER_RTENTRIES,0 },
{ "tMfwdQData", "MAX_MFWD_Q_DEPTH", sizeof(tMfwdQData),MAX_MFWD_Q_DEPTH, MAX_MFWD_Q_DEPTH,0 },
{ "tMfwdSrcNode", "MAX_MFWD_SRC_NODE", sizeof(tMfwdSrcNode),MAX_MFWD_SRC_NODE, MAX_MFWD_SRC_NODE,0 },
{ "tMfwdIifNode", "MAX_MFWD_OWNER_IIFS", sizeof(tMfwdIifNode),MAX_MFWD_OWNER_IIFS, MAX_MFWD_OWNER_IIFS,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _MFWDSZ_C  */
extern tFsModSizingParams FsMFWDSizingParams [];
#endif /*  _MFWDSZ_C  */


