/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mfmrp.h,v 1.4 2014/08/23 11:58:42 siva Exp $
 *
 * Description: This file contains the exported defines, types and
 *              functions for the Multicast Forwarding module.
 *
 *******************************************************************/

#ifndef _MFMRP_H
#define _MFMRP_H

/* Data that is to be given by MRP when registering with MFWD */
typedef struct MrpRegnInfo {
    VOID (*MrpHandleMcastDataPkt)(tCRU_BUF_CHAIN_HEADER *);
    VOID (*MrpHandleV6McastDataPkt)(tCRU_BUF_CHAIN_HEADER *);
    INT4 (*MrpInformMfwdStatus)(UINT4 u4Event);
    UINT4 u4MaxGroups;
    UINT4 u4MaxSrcs;
    UINT4 u4MaxIfaces;
    UINT2 u2OwnerId;
    UINT1 u1Mode;
    UINT1 u1ProtoId;
} tMrpRegnInfo;

/* Data that is to be sent to MRP when informing MRP of the following
 * 1. Forwarding of MDP
 * 2. MDP Arriving on wrong-iif
 * 3. Cache-miss for an MDP
 */

typedef struct MfwdMrpMDPMsg {
    UINT2 u2OwnerId; /* The identification value of the owner to whom the
                      * update is being sent
                      */
    UINT1 u1SearchStat; /* The search status value indicating the above
                         * listed types
                         */
    UINT1 u1AlignByte; 
    tIPvXAddr SrcAddr; /* The source address of the MDP for which the information
                      * is being sent
                      */
    tIPvXAddr GrpAddr; /* The group address for which the MDP was destined */
    UINT4 u4Iif; /* The incoming interface through which the MDP arrived */
} tMfwdMrpMDPMsg;

/* The header portion of the MRP update message */

typedef struct MrpUpdMesgHdr {
    UINT2 u2OwnerId;
    UINT1 u1UpdType;
    UINT1 u1UpdCmd;     /* The update command which indicates the what  
                         * updates are to be performed
                         */
    UINT1 u1NumEntries;
    UINT1 u1Reserved;
    UINT2 u2UpdSubCmd;
} tMrpUpdMesgHdr;

/* Data that is to be sent by the MRP when informing MFWD of the routing
 * table updates.
 */

typedef struct MrtUpdData {
    UINT1 u1ChkRpfFlag; /* Flag indicating if the RPF check should be done */
    UINT1 u1DeliverMDP;
    UINT2 u2AlignByte;


    tIPvXAddr SrcAddr;    /* source address of the route entry */
    UINT4 u4SrcMaskLen;    /* Mask value indicating if the entry is (*, G) or 
                         * (S, G)
                         */
    tIPvXAddr GrpAddr;    /* Group address of the route entry */
    UINT4 u4GrpMaskLen;    /* The group mask value indicating if the entry (*, *) 
                         * entry
                         */
    tIPvXAddr UpStrmNbr;  /* The upstream neighbor for the source address that 
                         * is used to reach source address
                         */
    tIPvXAddr RtAddr;
 
    UINT4 u4Iif;        /* The incoming interface that is used to reach the 
                         * source or perform RPF check
                         */
    UINT4 u4OifCnt;     /* A value indicating number of oifs present in the
                         * oif list
                         */
    UINT4 u4IifCnt;     /* A value indicating number of oifs present in the */
    UINT1         u1RtType; /*Value representing the Multicast Route type */
    UINT1         au1Pad[3];

} tMrtUpdData;


typedef struct MrtIifUpdData 
{

    tIPvXAddr  NewSrcAddr; /* The new source address if changed.
                          * If the source does not change but only the IIF
                          * changes it is the responsibilty of the MRP to see
                          * that this value is equal to Old source address
                          */
    UINT4      u4NewSrcMaskLen; /* This value generally does not changed */

    tIPvXAddr  UpStrmNewNbr; /* the new upstream neighbor towards the source */
    UINT4 u4Iif; /* The new incoming interface if changed If this value
                  * does not change it is the responsibilty of the MRP
                  * to see that this value is equal to old value.
                  */

} tMrtIifUpdData;
            

/* The data to be contained in each of the oif nodes that is to be updated */

typedef struct MrpOifNode {
    UINT4 u4OifIndex;    /* The out going interface that is to be updated */
    tIPvXAddr NextHopAddr; /* The Next Hop address for which the state is to 
                          * be updated
                          */
    UINT4 u4NextHopState;/* The NextHop state for the next hop addr */
} tMrpOifNode;

/* The data to be contained in each of the iif nodes that is to be updated */

typedef struct MrpIifNode {
    UINT4 u4IifIndex;    /* The out going interface that is to be updated */
} tMrpIifNode;



/* The data to be given for updating the owner interface table */

typedef struct OwnerIfUpdData {
    UINT4 u4Ifcnt;   /* Number of interface to be updated */
    UINT4 *pu4IfList;/* List of interface indices which have to be updated */
} tOwnerIfUpdData;

#define MFWD_FAILURE                0
#define MFWD_SUCCESS                1

#define MFWD_STATUS_DISABLED        2
#define MFWD_STATUS_ENABLED         1

/* Some definitions which are common to both the MRP and MFWD */
#define  MFWD_MRP_MRT_UPDATE         0x01
#define  MFWD_MRP_OIM_UPDATE         0x02

/* Definitions for the update commands in the MRP update message for the 
 * MRT updates
 */
#define  MFWD_ENTRY_DELETE_CMD         0x01
#define  MFWD_ENTRY_CREATE_CMD         0x02
#define  MFWD_ENTRY_UPDATE_IIF_CMD     0x04
#define  MFWD_ENTRY_DELETE_OIF_CMD     0x08
#define  MFWD_ENTRY_ADD_OIF_CMD        0x10
#define  MFWD_ENTRY_OIF_STATE_CMD      0x20
#define  MFWD_ENTRY_UPD_DELIVERMDP_FLAG 0x40
#define  MFWD_ENTRY_UPDATE_IIFLIST_CMD 0x80

/* Definitions for the IIF List Update commands in MRP message 
 * */
#define  MFWD_ENTRY_DELETE_IIF_CMD     0x01
#define  MFWD_ENTRY_ADD_IIF_CMD        0x02

/* Definitions for the update commands in the MRP update message for the 
 * interface table update.
 */
#define  MFWD_DELETE_OWNERIF_CMD       0x01
#define  MFWD_ADD_OWNERIF_CMD          0x02
#define  MFWD_DELETE_OWNERIFV6_CMD       0x03
#define  MFWD_ADD_OWNERIFV6_CMD          0x04

#define  MFWD_MRP_DELIVER_MDP          0x01
#define  MFWD_MRP_DELIVER_FULLMDP      0x02
#define  MFWD_MRP_DONT_DELIVER_MDP     0x03

#define  MFWD_MRP_SPARSE_MODE          0x02
#define  MFWD_MRP_DENSE_MODE           0x01

#define  MFWD_TO_MRP_STATUS_DISABLED_EVENT 0x40
#define  MFWD_TO_MRP_STATUS_ENABLED_EVENT  0x80

/* Definitions for the MDP status after searching the routing table for
 * forwarding it
 */
#define  MFWD_MDP_WRONG_IIF            0x01
#define  MFWD_MDP_CACHE_MISS           0x02
#define  MFWD_MDP_CACHE_HIT            0x04

#define  MFWD_MRP_CHK_RPF_IF           0x01

/* This macro is used to copy the interface index for update from the update
 * message. This macro provides an offset into nth interface index in message
 */
#define  MFWD_IFINDEX_MESSAGE_OFFSET(u4IfCtr) \
     (sizeof (UINT4) + (u4IfCtr * sizeof (UINT4)))

#define  MFWD_OIFINFO_MESSAGE_OFFSET(u4OifCtr) \
     (sizeof (tMrtUpdData) + (u4OifCtr * sizeof (tMrpOifNode)))

#define  MFWD_OIFINFO_OFFSET(u4OifCtr) \
     (sizeof (tMrpUpdMesgHdr) + sizeof (tMrtUpdData) + (u4OifCtr * sizeof (tMrpOifNode)))

#define  MFWD_IIFINFO_MESSAGE_OFFSET(u4OifCtr, u4IifCtr) \
     (sizeof (tMrtUpdData) + (u4OifCtr * sizeof (tMrpOifNode)) + (u4IifCtr * sizeof(tMrpIifNode)))

#define  MFWD_IIFINFO_OFFSET(u4OifCtr,u4IifCtr) \
     (sizeof (tMrpUpdMesgHdr) + sizeof (tMrtUpdData) + (u4OifCtr * sizeof (tMrpOifNode)) + (u4IifCtr * sizeof (tMrpIifNode))  )

PUBLIC INT4 MfwdMrtGetFwdCount ARG_LIST ((UINT2 u2OwnerId, tIPvXAddr SrcAddr, 
                                          tIPvXAddr GrpAddr, UINT4 *pu4FwdInfo));
INT4
MfwdMrtGetLastFwdTime (UINT2 u2OwnerId, tIPvXAddr SrcAddr, tIPvXAddr GrpAddr,
                       UINT4 *pu4FwdTime);

PUBLIC INT4
MfwdPostMrpMsgToMfwd ARG_LIST ((tCRU_BUF_CHAIN_HEADER * pBuffer));

PUBLIC INT4  MfwdInputHandleRegistration ARG_LIST ((tMrpRegnInfo regninfo));

PUBLIC INT4  MfwdInputHandleDeRegistration ARG_LIST ((UINT2 u2OwnerId));

#endif
