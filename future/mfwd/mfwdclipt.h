/* FILE HEADER :
 *
 *  ---------------------------------------------------------------------------
 * |  FILE NAME             : fsmfcli.h                                        |
 * |                                                                           |
 * |  PRINCIPAL AUTHOR      :                                                  |
 * |                                                                           |
 * |  SUBSYSTEM NAME        : CLI                                              |
 * |                                                                           |
 * |  MODULE NAME           : MFWD configuration                               |
 * |                                                                           |
 * |  LANGUAGE              : C                                                |
 * |                                                                           |
 * |  TARGET ENVIRONMENT    :                                                  |
 * |                                                                           |
 * |  DATE OF FIRST RELEASE :                                                  |
 * |                                                                           |
 * |  DESCRIPTION           : This file includes all the MFWD CLI related
 *                            definitions and declarations.                    |
 * |                                                                           |
 * |                                                                           |
 *  ---------------------------------------------------------------------------
 *
 */

#ifndef FSMFCLI_H
#define FSMFCLI_H

INT4
MfwdIpMulticastRouting (tCliHandle CliHandle, INT4 i4Status);

INT4
MfwdShowMulticastRoute (tCliHandle CliHandle);

INT4
MfwdShowIPv6MulticastRoute (tCliHandle CliHandle);
INT4
MfwdSetTrace (tCliHandle CliHandle, INT4 i4TraceStatus);
VOID IssMfwdShowDebugging (tCliHandle);
#endif
