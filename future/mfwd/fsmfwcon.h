
# ifndef fsmfwOCON_H
# define fsmfwOCON_H
/*
 *  The Constant Declarations for
 *  mfwdScalars
 */

# define IPMROUTEENABLE                                    (1)
# define IPMROUTEENTRYCOUNT                                (2)
# define IPMROUTEENABLECMDB                                (3)
# define MFWDGLOBALTRACE                                   (4)
# define MFWDGLOBALDEBUG                                   (5)
# define IPMROUTEDISCARDEDPKTS                             (6)
# define MFWDAVGDATARATE                                   (7)

/*
 *  The Constant Declarations for
 *  ipMRouteTable
 */

# define IPMROUTEOWNERID                                   (1)
# define IPMROUTEGROUP                                     (2)
# define IPMROUTESOURCE                                    (3)
# define IPMROUTESOURCEMASK                                (4)
# define IPMROUTEUPSTREAMNEIGHBOR                          (5)
# define IPMROUTEINIFINDEX                                 (6)
# define IPMROUTEUPTIME                                    (7)
# define IPMROUTEPKTS                                      (8)
# define IPMROUTEDIFFERENTINIFPACKETS                      (9)
# define IPMROUTEPROTOCOL                                  (10)
# define IPMROUTERTADDRESS                                 (11)
# define IPMROUTERTMASK                                    (12)
# define IPMROUTERTTYPE                                    (13)

/*
 *  The Constant Declarations for
 *  ipMRouteNextHopTable
 */

# define IPMROUTENEXTHOPOWNERID                            (1)
# define IPMROUTENEXTHOPGROUP                              (2)
# define IPMROUTENEXTHOPSOURCE                             (3)
# define IPMROUTENEXTHOPSOURCEMASK                         (4)
# define IPMROUTENEXTHOPIFINDEX                            (5)
# define IPMROUTENEXTHOPADDRESS                            (6)
# define IPMROUTENEXTHOPSTATE                              (7)
# define IPMROUTENEXTHOPUPTIME                             (8)

/*
 *  The Constant Declarations for
 *  ipMRouteInterfaceTable
 */

# define IPMROUTEINTERFACEIFINDEX                          (1)
# define IPMROUTEINTERFACEOWNERID                          (2)
# define IPMROUTEINTERFACETTL                              (3)
# define IPMROUTEINTERFACEPROTOCOL                         (4)
# define IPMROUTEINTERFACERATELIMIT                        (5)
# define IPMROUTEINTERFACEINMCASTOCTETS                    (6)
# define IPMROUTEINTERFACECMDBPKTCNT                       (7)
# define IPMROUTEINTERFACEOUTMCASTOCTETS                   (8)

#endif /*  fsmfwdOCON_H  */
