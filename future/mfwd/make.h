#
# Copyright (C) 2006 Aricent Inc . All Rights Reserved
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : make.h                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       : Aricent Inc.                               |
# |                                                                          |
# |   MAKE TOOL(S) USED      : Eg: GNU MAKE                                  |
# |                                                                          |
# |   TARGET ENVIRONMENT     : LINUX                                         |
# |                                                                          |
# |   DATE                   : 05 January 2002                               |
# |                                                                          |
# |   DESCRIPTION            : This file contains all the dependencies       |
# |                            that are used for building this module        |
# +--------------------------------------------------------------------------+


#######################################################################
####     Common Directory Structure                                 ###
#######################################################################

MFWD_BASE_DIR    = ${BASE_DIR}/mfwd

GLOBAL_INCLUDES  = ${COMMON_INCLUDE_DIRS}

MFWD_INCLUDE_FILES= \
    ${COMN_INCL_DIR}/mfwd.h\
    ${MFWD_BASE_DIR}/mfinc.h\
    ${MFWD_BASE_DIR}/mftdfs.h\
    ${MFWD_BASE_DIR}/mfdefn.h\
    ${MFWD_BASE_DIR}/mfmacs.h\
    ${MFWD_BASE_DIR}/mfport.h\
    ${MFWD_BASE_DIR}/mfdebug.h\
    ${MFWD_BASE_DIR}/mftrace.h\
    ${MFWD_BASE_DIR}/mfproto.h\
    $(COMMON_DEPENDENCIES)

ifeq (${CLI}, YES)
CLI_INC = $(BASE_DIR)/inc/cli

MFWD_INCLUDE_FILES+=         \
    $(MFWD_BASE_DIR)/mfwdclipt.h \
    $(CLI_INC)/mfwdcli.h

endif

