/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: mfwdcmdb.h,v 1.7 2008/08/27 10:52:42 premap-iss Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _MFWDCMDB_H
#define _MFWDCMDB_H

UINT1 IpCmnMRouteTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER32};
UINT1 IpCmnMRouteNextHopTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 IpCmnMRouteInterfaceTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER};

UINT4 mfwdcm [] ={1,3,6,1,4,1,2076,126};
tSNMP_OID_TYPE mfwdcmOID = {8, mfwdcm};


UINT4 IpCmnMRouteEnable [ ] ={1,3,6,1,4,1,2076,126,1,1,1};
UINT4 IpCmnMRouteEntryCount [ ] ={1,3,6,1,4,1,2076,126,1,1,2};
UINT4 IpCmnMRouteEnableCmdb [ ] ={1,3,6,1,4,1,2076,126,1,1,3};
UINT4 MfwdCmnGlobalTrace [ ] ={1,3,6,1,4,1,2076,126,1,1,4};
UINT4 MfwdCmnGlobalDebug [ ] ={1,3,6,1,4,1,2076,126,1,1,5};
UINT4 IpCmnMRouteDiscardedPkts [ ] ={1,3,6,1,4,1,2076,126,1,1,6};
UINT4 MfwdCmnAvgDataRate [ ] ={1,3,6,1,4,1,2076,126,1,1,7};
UINT4 IpCmnMRouteOwnerId [ ] ={1,3,6,1,4,1,2076,126,1,2,1,1,1};
UINT4 IpCmnMRouteAddrType [ ] ={1,3,6,1,4,1,2076,126,1,2,1,1,2};
UINT4 IpCmnMRouteGroup [ ] ={1,3,6,1,4,1,2076,126,1,2,1,1,3};
UINT4 IpCmnMRouteSource [ ] ={1,3,6,1,4,1,2076,126,1,2,1,1,4};
UINT4 IpCmnMRouteSourceMask [ ] ={1,3,6,1,4,1,2076,126,1,2,1,1,5};
UINT4 IpCmnMRouteUpstreamNeighbor [ ] ={1,3,6,1,4,1,2076,126,1,2,1,1,6};
UINT4 IpCmnMRouteInIfIndex [ ] ={1,3,6,1,4,1,2076,126,1,2,1,1,7};
UINT4 IpCmnMRouteUpTime [ ] ={1,3,6,1,4,1,2076,126,1,2,1,1,8};
UINT4 IpCmnMRoutePkts [ ] ={1,3,6,1,4,1,2076,126,1,2,1,1,9};
UINT4 IpCmnMRouteDifferentInIfPackets [ ] ={1,3,6,1,4,1,2076,126,1,2,1,1,10};
UINT4 IpCmnMRouteProtocol [ ] ={1,3,6,1,4,1,2076,126,1,2,1,1,11};
UINT4 IpCmnMRouteRtAddress [ ] ={1,3,6,1,4,1,2076,126,1,2,1,1,12};
UINT4 IpCmnMRouteRtMask [ ] ={1,3,6,1,4,1,2076,126,1,2,1,1,13};
UINT4 IpCmnMRouteRtType [ ] ={1,3,6,1,4,1,2076,126,1,2,1,1,14};
UINT4 IpCmnMRouteNextHopOwnerId [ ] ={1,3,6,1,4,1,2076,126,1,2,2,1,1};
UINT4 IpCmnMRouteNextHopAddrType [ ] ={1,3,6,1,4,1,2076,126,1,2,2,1,2};
UINT4 IpCmnMRouteNextHopGroup [ ] ={1,3,6,1,4,1,2076,126,1,2,2,1,3};
UINT4 IpCmnMRouteNextHopSource [ ] ={1,3,6,1,4,1,2076,126,1,2,2,1,4};
UINT4 IpCmnMRouteNextHopSourceMask [ ] ={1,3,6,1,4,1,2076,126,1,2,2,1,5};
UINT4 IpCmnMRouteNextHopIfIndex [ ] ={1,3,6,1,4,1,2076,126,1,2,2,1,6};
UINT4 IpCmnMRouteNextHopAddress [ ] ={1,3,6,1,4,1,2076,126,1,2,2,1,7};
UINT4 IpCmnMRouteNextHopState [ ] ={1,3,6,1,4,1,2076,126,1,2,2,1,8};
UINT4 IpCmnMRouteNextHopUpTime [ ] ={1,3,6,1,4,1,2076,126,1,2,2,1,9};
UINT4 IpCmnMRouteInterfaceIfIndex [ ] ={1,3,6,1,4,1,2076,126,1,2,3,1,1};
UINT4 IpCmnMRouteInterfaceAddrType [ ] ={1,3,6,1,4,1,2076,126,1,2,3,1,2};
UINT4 IpCmnMRouteInterfaceOwnerId [ ] ={1,3,6,1,4,1,2076,126,1,2,3,1,3};
UINT4 IpCmnMRouteInterfaceTtl [ ] ={1,3,6,1,4,1,2076,126,1,2,3,1,4};
UINT4 IpCmnMRouteInterfaceProtocol [ ] ={1,3,6,1,4,1,2076,126,1,2,3,1,5};
UINT4 IpCmnMRouteInterfaceRateLimit [ ] ={1,3,6,1,4,1,2076,126,1,2,3,1,6};
UINT4 IpCmnMRouteInterfaceInMcastOctets [ ] ={1,3,6,1,4,1,2076,126,1,2,3,1,7};
UINT4 IpCmnMRouteInterfaceCmdbPktCnt [ ] ={1,3,6,1,4,1,2076,126,1,2,3,1,8};
UINT4 IpCmnMRouteInterfaceOutMcastOctets [ ] ={1,3,6,1,4,1,2076,126,1,2,3,1,9};


tMbDbEntry mfwdcmMibEntry[]= {

{{11,IpCmnMRouteEnable}, NULL, IpCmnMRouteEnableGet, IpCmnMRouteEnableSet, IpCmnMRouteEnableTest, IpCmnMRouteEnableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,IpCmnMRouteEntryCount}, NULL, IpCmnMRouteEntryCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,IpCmnMRouteEnableCmdb}, NULL, IpCmnMRouteEnableCmdbGet, IpCmnMRouteEnableCmdbSet, IpCmnMRouteEnableCmdbTest, IpCmnMRouteEnableCmdbDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,MfwdCmnGlobalTrace}, NULL, MfwdCmnGlobalTraceGet, MfwdCmnGlobalTraceSet, MfwdCmnGlobalTraceTest, MfwdCmnGlobalTraceDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{11,MfwdCmnGlobalDebug}, NULL, MfwdCmnGlobalDebugGet, MfwdCmnGlobalDebugSet, MfwdCmnGlobalDebugTest, MfwdCmnGlobalDebugDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{11,IpCmnMRouteDiscardedPkts}, NULL, IpCmnMRouteDiscardedPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,MfwdCmnAvgDataRate}, NULL, MfwdCmnAvgDataRateGet, MfwdCmnAvgDataRateSet, MfwdCmnAvgDataRateTest, MfwdCmnAvgDataRateDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "1000"},

{{13,IpCmnMRouteOwnerId}, GetNextIndexIpCmnMRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, IpCmnMRouteTableINDEX, 5, 0, 0, NULL},

{{13,IpCmnMRouteAddrType}, GetNextIndexIpCmnMRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, IpCmnMRouteTableINDEX, 5, 0, 0, NULL},

{{13,IpCmnMRouteGroup}, GetNextIndexIpCmnMRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, IpCmnMRouteTableINDEX, 5, 0, 0, NULL},

{{13,IpCmnMRouteSource}, GetNextIndexIpCmnMRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, IpCmnMRouteTableINDEX, 5, 0, 0, NULL},

{{13,IpCmnMRouteSourceMask}, GetNextIndexIpCmnMRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, IpCmnMRouteTableINDEX, 5, 0, 0, NULL},

{{13,IpCmnMRouteUpstreamNeighbor}, GetNextIndexIpCmnMRouteTable, IpCmnMRouteUpstreamNeighborGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, IpCmnMRouteTableINDEX, 5, 0, 0, NULL},

{{13,IpCmnMRouteInIfIndex}, GetNextIndexIpCmnMRouteTable, IpCmnMRouteInIfIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, IpCmnMRouteTableINDEX, 5, 0, 0, NULL},

{{13,IpCmnMRouteUpTime}, GetNextIndexIpCmnMRouteTable, IpCmnMRouteUpTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, IpCmnMRouteTableINDEX, 5, 0, 0, NULL},

{{13,IpCmnMRoutePkts}, GetNextIndexIpCmnMRouteTable, IpCmnMRoutePktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IpCmnMRouteTableINDEX, 5, 0, 0, NULL},

{{13,IpCmnMRouteDifferentInIfPackets}, GetNextIndexIpCmnMRouteTable, IpCmnMRouteDifferentInIfPacketsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IpCmnMRouteTableINDEX, 5, 0, 0, NULL},

{{13,IpCmnMRouteProtocol}, GetNextIndexIpCmnMRouteTable, IpCmnMRouteProtocolGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, IpCmnMRouteTableINDEX, 5, 0, 0, NULL},

{{13,IpCmnMRouteRtAddress}, GetNextIndexIpCmnMRouteTable, IpCmnMRouteRtAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, IpCmnMRouteTableINDEX, 5, 0, 0, NULL},

{{13,IpCmnMRouteRtMask}, GetNextIndexIpCmnMRouteTable, IpCmnMRouteRtMaskGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, IpCmnMRouteTableINDEX, 5, 0, 0, NULL},

{{13,IpCmnMRouteRtType}, GetNextIndexIpCmnMRouteTable, IpCmnMRouteRtTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, IpCmnMRouteTableINDEX, 5, 0, 0, NULL},

{{13,IpCmnMRouteNextHopOwnerId}, GetNextIndexIpCmnMRouteNextHopTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, IpCmnMRouteNextHopTableINDEX, 7, 0, 0, NULL},

{{13,IpCmnMRouteNextHopAddrType}, GetNextIndexIpCmnMRouteNextHopTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, IpCmnMRouteNextHopTableINDEX, 7, 0, 0, NULL},

{{13,IpCmnMRouteNextHopGroup}, GetNextIndexIpCmnMRouteNextHopTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, IpCmnMRouteNextHopTableINDEX, 7, 0, 0, NULL},

{{13,IpCmnMRouteNextHopSource}, GetNextIndexIpCmnMRouteNextHopTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, IpCmnMRouteNextHopTableINDEX, 7, 0, 0, NULL},

{{13,IpCmnMRouteNextHopSourceMask}, GetNextIndexIpCmnMRouteNextHopTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, IpCmnMRouteNextHopTableINDEX, 7, 0, 0, NULL},

{{13,IpCmnMRouteNextHopIfIndex}, GetNextIndexIpCmnMRouteNextHopTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, IpCmnMRouteNextHopTableINDEX, 7, 0, 0, NULL},

{{13,IpCmnMRouteNextHopAddress}, GetNextIndexIpCmnMRouteNextHopTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, IpCmnMRouteNextHopTableINDEX, 7, 0, 0, NULL},

{{13,IpCmnMRouteNextHopState}, GetNextIndexIpCmnMRouteNextHopTable, IpCmnMRouteNextHopStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, IpCmnMRouteNextHopTableINDEX, 7, 0, 0, NULL},

{{13,IpCmnMRouteNextHopUpTime}, GetNextIndexIpCmnMRouteNextHopTable, IpCmnMRouteNextHopUpTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, IpCmnMRouteNextHopTableINDEX, 7, 0, 0, NULL},

{{13,IpCmnMRouteInterfaceIfIndex}, GetNextIndexIpCmnMRouteInterfaceTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, IpCmnMRouteInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,IpCmnMRouteInterfaceAddrType}, GetNextIndexIpCmnMRouteInterfaceTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, IpCmnMRouteInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,IpCmnMRouteInterfaceOwnerId}, GetNextIndexIpCmnMRouteInterfaceTable, IpCmnMRouteInterfaceOwnerIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, IpCmnMRouteInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,IpCmnMRouteInterfaceTtl}, GetNextIndexIpCmnMRouteInterfaceTable, IpCmnMRouteInterfaceTtlGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, IpCmnMRouteInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,IpCmnMRouteInterfaceProtocol}, GetNextIndexIpCmnMRouteInterfaceTable, IpCmnMRouteInterfaceProtocolGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, IpCmnMRouteInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,IpCmnMRouteInterfaceRateLimit}, GetNextIndexIpCmnMRouteInterfaceTable, IpCmnMRouteInterfaceRateLimitGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, IpCmnMRouteInterfaceTableINDEX, 2, 0, 0, "0"},

{{13,IpCmnMRouteInterfaceInMcastOctets}, GetNextIndexIpCmnMRouteInterfaceTable, IpCmnMRouteInterfaceInMcastOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IpCmnMRouteInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,IpCmnMRouteInterfaceCmdbPktCnt}, GetNextIndexIpCmnMRouteInterfaceTable, IpCmnMRouteInterfaceCmdbPktCntGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IpCmnMRouteInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,IpCmnMRouteInterfaceOutMcastOctets}, GetNextIndexIpCmnMRouteInterfaceTable, IpCmnMRouteInterfaceOutMcastOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IpCmnMRouteInterfaceTableINDEX, 2, 0, 0, NULL},
};
tMibData mfwdcmEntry = { 39, mfwdcmMibEntry };
#endif /* _MFWDCMDB_H */

