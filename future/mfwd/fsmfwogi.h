
# ifndef fsmfwOGP_H
# define fsmfwOGP_H

 /* The Definitions of the OGP Index Constants.  */

# define SNMP_OGP_INDEX_MFWDSCALARS                                  (0)
# define SNMP_OGP_INDEX_IPMROUTETABLE                                (1)
# define SNMP_OGP_INDEX_IPMROUTENEXTHOPTABLE                         (2)
# define SNMP_OGP_INDEX_IPMROUTEINTERFACETABLE                       (3)

#endif /*  fsmfwdOGP_H  */
