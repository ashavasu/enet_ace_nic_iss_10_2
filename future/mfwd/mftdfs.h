/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mftdfs.h,v 1.11 2014/08/23 11:58:43 siva Exp $
 *
 ********************************************************************/

#ifndef MFWD_TDFS
#define MFWD_TDFS


typedef struct MemPoolDesc {
    INT4       i4BlockSize;
    tMemPoolId    poolId;
} tMfwdMemPoolDesc;

#define  MFWD_MAX_SEMNAME_LEN      8
#define MAX_MFWD_OWNER_INFO_TABLE_SIZE    MFWD_DEF_MAX_MRPS * 4  /* 4 - sizeof(UINT4)*/
#define  MFWD_MAX_DATA_PKT_LEN         1500
typedef struct SemDesc {
    UINT1      au1SemName[MFWD_MAX_SEMNAME_LEN];
    tOsixSemId semId;
} tSemDesc;

/* This structure contains the details of an interface registered by the *
 * MRP.                                                                  *
 *-----------------------------------------------------------------------*/

typedef struct MfwdInterfaceInfo {
    struct MfwdInterfaceNode **pLinIfTbl;
    tTMO_HASH_TABLE          *pHashIfTbl;
    UINT4                     u4HashKey;
    tSemDesc                  HashSem; 
} tMfwdInterfaceInfo;

typedef struct MfwdInterfaceNode {
    tTMO_HASH_NODE IfHashLink;
    tTMO_SLL_NODE OwnerIfLink;
    tSemDesc      IfNodeSema4;
    UINT2         u2OwnerId;
    UINT1         u1AddrType;
    UINT1         u1AlignByte;
    UINT4         u4IfIndex;
    UINT4         u4IfRtLmt;
    UINT4         u4DscdPktCnt;
    UINT4         u4InMdpOct;
    UINT4         u4OutMdpOct;
    UINT4         u4CmdpCount;
    tTMO_SLL      CmdbList;
} tMfwdInterfaceNode;


/* This data structure contains the details of a particular MRP */
/*--------------------------------------------------------------*/
typedef struct MfwdOwnerInfoNode {
    tTMO_SLL         OwnerIfList;
    tSemDesc         OwnerNodeSema4; /* sema4 to be taken when doing 
                                      * operation on owner node an operation
                                      * can be a read on the MRP table of the 
                                      * owner or write on it. It can be access
                                      * CMDB buffer
                                      */
    UINT2            u2OwnerId;
    UINT1            u1OwnerMode;
    UINT1            u1ProtoId;
    VOID           (*MrpDataPktCallBkFn)(tCRU_BUF_CHAIN_HEADER *pMfwdToMrpMsg);
    VOID           (*MrpV6DataPktCallBkFn)(tCRU_BUF_CHAIN_HEADER *pMfwdToMrpMsg);
    INT4           (*MrpInformMfwdStatus)(UINT4 u4Event);
    UINT4            u4RtEntryCount;
    UINT4            u4MaxGrps;
    UINT4            u4MaxSrcs;
    UINT4            u4MaxIfaces;
    tTMO_HASH_TABLE  *pMrtTable;
    tTmrAppTimer     CmdbTimer;
    
    /* Memory pools required */
} tMfwdOwnerInfoNode;



/* END OF DATA STRUCTURES CONTAINING BASIC MFWD INFORMATION */

/* ROUTING TABLE DATA STRUCTURES */

/* This structure contains the data required for the source of a particular
 * entry
 *------------------------------------------------------------------------*/
typedef struct MfwdSrcNode {
    tIPvXAddr  SrcAddr; /* The source address of the route entry */
    tIPvXAddr  RtAddr; /* The address that is used to lookup the RPF 
                     * Neighbor towards the source
                     */
    INT4 i4RtMask; /* The Mask value of the RtAddr */
 
    UINT4 u4Iif;    /* The incoming interface through which the MDP should
                     * be received so that it can be forwarded
                     */
    tIPvXAddr UpStrmNbr;
} tMfwdSrcNode;

/*This structure contains the data required for the route entry  */
/*---------------------------------------------------------------*/
typedef struct MfwdRtEntry {
    tTMO_SLL_NODE SgEntryLink;
    tTMO_SLL_NODE GetNextLink;
    UINT2         u2OwnerId;
    UINT1         u1DeliverMDP;
    UINT1         u1ChkRpf;
    tIPvXAddr     GrpAddr;
    UINT4         u4GrpMaskLen;
    tMfwdSrcNode  *pSrcDescNode;
    tTMO_SLL      OifList;
    tTMO_SLL      IifList;
    UINT4         u4FwdCnt;
    UINT4         u4RpfFailCnt;     
    UINT4         u4EntryUpTime;
    UINT4         u4LastFwdTime;
    UINT1         u1RtType;
    UINT1         au1Pad[3];
 } tMfwdRtEntry;

/* This structure contains the source specific entries for a group and
 * wild card entry for a group
 *--------------------------------------------------------------------*/
typedef struct MfwdGrpNode {
    tTMO_HASH_NODE   GrpNodeHashLink;
    tTMO_SLL_NODE    GrpGetNextLink;
    tTMO_SLL         SgEntryList;
    tIPvXAddr        GrpAddr;
    UINT4         u4GrpMaskLen;
} tMfwdGrpNode;

/* This structure contains a set of sources active for a particular wild
 * card group
 *----------------------------------------------------------------------*/
typedef struct MfwdActiveSrcNode {
    tTMO_SLL_NODE NextSrcEntry;
    tIPvXAddr     SrcAddr;
    UINT4         u4FwdCnt;
} tMfwdActiveSrcNode;


/* This structure contains the details about the status of the outgoing 
 * nodes
 *---------------------------------------------------------------------*/
typedef struct MfwdOifNode {
    tTMO_SLL_NODE    NextOifNode;
    UINT4            u4OifIndex;
    UINT4            u4FwdCnt;
    UINT4            u4OifState;
    tTMO_SLL         NextHopList;
}  tMfwdOifNode;

 /* This structure contains the details about the status of the incoming 
 * nodes
 *---------------------------------------------------------------------*/
typedef struct MfwdIifNode {
    tTMO_SLL_NODE    NextIifNode;
    UINT4            u4IifIndex;
    UINT4            u4FwdCnt;
}  tMfwdIifNode;
  
  
/* This data structure contains the details about the oif node's state for
 * forwarding to a next hop node
 *------------------------------------------------------------------------*/
typedef struct MfwdOifNextHopNode {
    tTMO_SLL_NODE    NextHopNode;
    tIPvXAddr        NextHopAddr;
    UINT4            u4NextHopState;
    UINT4            u4NextHopUpTime;
} tMfwdOifNextHopNode;

/* END OF ROUTING TABLE DATA STRUCTURES */


/* CACHE MISS DATA BUFFER DATA STRUCTURES */

/* This structure contains the details about the multicast data packets for
 * which the cachemiss has been informed
 *-------------------------------------------------------------------------*/
typedef struct CacheMissDBNode {
    tTMO_SLL_NODE    NextDbNode;
    tIPvXAddr        SrcAddr;
    tIPvXAddr        GrpAddr;
    tTMO_SLL         McastDataPkts; /* List of Multicast data packets that
                                     * have suffered a cachemiss for this 
                                     * source address and group address.
                                     */
} tCacheMissDBNode;


/* This structure contains details about a multicast data packet that has 
 * suffered cache-miss and is stored for forwarding in future
 *------------------------------------------------------------------------*/
typedef struct CMDataPktNode {
    tTMO_SLL_NODE           NextPkt;
    UINT4                   u4Iif;
    tCRU_BUF_CHAIN_HEADER   *pMcastDataPkt;
} tCMDataPktNode;

/* The MFWD Context Structure */
/*----------------------------*/
typedef struct MfwdContextStructure {
    tMfwdInterfaceInfo   InterfaceTbl;
    tMfwdOwnerInfoNode   **pOwnerInfoTbl;
    tTMO_SLL             MrtGetNextList;
    
    tMfwdMemPoolDesc     OwnerPool;
    tMfwdMemPoolDesc     OwnerInfoTablePool;
    tMfwdMemPoolDesc     CMDBNodePool;    /* tCacheMissDBNode Data Pool */
    tMfwdMemPoolDesc     CMDataPktsPool;  /* tCMDataPktNode  Data Pool */
    tMfwdMemPoolDesc     MsgQPool;
    tMfwdMemPoolDesc     IfNodePool;      /* tMfwdInterfaceNode data Pool */
    tMfwdMemPoolDesc     RtEntryPool;     /* tMfwdRtEntry data Pool */
    tMfwdMemPoolDesc     GrpNodePool;     /* tMfwdGrpNode data Pool */
    tMfwdMemPoolDesc     SrcInfoPool;     /* tMfwdSrcNode data Pool */
    tMfwdMemPoolDesc     OifNodePool;     /* tMfwdOifNode Data Pool */
    tMfwdMemPoolDesc     IifNodePool;     /* tMfwdIifNode Data Pool */
    tMfwdMemPoolDesc     NextHopNodePool; /* tMfwdNextHopNode Data Pool */

    tTimerListId         CMDBTmrListId;
    
    tOsixTaskId          MfwdTaskId;
    tSemDesc             MfwdMutex;
    tOsixQId             MfwdQId;
    tOsixQId             MfwdCQId;
    INT4                 i4DataSockId;
    UINT1                au1SendBuf[MFWD_MAX_DATA_PKT_LEN];
    
    UINT2                u2MaxMrps;
    UINT1                u1MfwdStatus;
    UINT1                u1CacheMissDBStat;
    UINT4                u4MaxIfaces;
    UINT4                u4GlobalDebug;
    UINT4                u4GlobalTrace;
    UINT4                u4IpRegnId;
    UINT4                u4Ipv6RegnId;
    UINT4                u4RtEntryCount;
    UINT4                u4DiscPkts;
    UINT4                u4QProcessCnt;
    UINT1      au1MfwdTrcMode[12];

    tMfwdInterfaceNode   DummyIf;       /* Dummy interface node in the table */
    tMfwdOwnerInfoNode   DummyOwner;  /* Dummy owner node in the table */
} tMfwdContextStructure;



/* END OF CACHE MISS DATA BUFFER DATA STRUCTURES */

typedef struct MfwdFwdInfo {
    tTMO_SLL *pOifList;
    tTMO_SLL *pIifList;
    UINT4 u4Iif;
    UINT2 u2Reserved;
    UINT1 u1DeliverMDP;
    UINT1 u1SearchStat;
} tMfwdFwdInfo;

typedef struct MfwdSemNode {
    tTMO_SLL_NODE NextNode;
    UINT1         au1SemName [4];
} tMfwdSemNode;
            
typedef struct _MfwdQData
{
    UINT1 u1MsgId;
    UINT1 u1Reserved;
    UINT2 u2Reserved;
    VOID  *pMsg;
} tMfwdQData;

typedef  int (*tMsgHandleFunPtr)(VOID *);

typedef struct _MfwdOwnerInfoTable {
    UINT1   au1MfwdOwnerTable[MAX_MFWD_OWNER_INFO_TABLE_SIZE];
} tMfwdOwnerInfoTable;

#endif
