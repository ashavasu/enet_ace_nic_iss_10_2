/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: mfdbg.c,v 1.5 2010/11/24 13:57:32 siva Exp $
*
*********************************************************************/
#include "mfinc.h"

/* Global Context Variable */
extern tMfwdContextStructure gMfwdContext;

VOID                MfwDbgPrintRt (tMfwdRtEntry * pRt);

VOID                MfwDbgDisplayRoute (UINT2 u2OwnerId, tIPvXAddr GrpAddr,
                                        tIPvXAddr SrcAddr);

VOID
MfwDbgDisplayRoute (UINT2 u2OwnerId, tIPvXAddr GrpAddr, tIPvXAddr SrcAddr)
{
    UINT1               u1HashIndex = 0;
    tMfwdGrpNode       *pGrpNode = NULL;
    tMfwdRtEntry       *pDelRtEntry = NULL;
    tMfwdRtEntry       *pRtEntry = NULL;
    UINT1               u1NullSrcAddr[16];

    MEMSET (u1NullSrcAddr, 0, 16);

    MFWD_GET_HASH_INDEX (GrpAddr, u1HashIndex);
    /* find the group node corresponding to the group address given */

    TMO_HASH_Scan_Bucket (MFWD_OWNER_MRT (u2OwnerId), (UINT4) u1HashIndex,
                          pGrpNode, tMfwdGrpNode *)
    {
        if (IPVX_ADDR_COMPARE (GrpAddr, pGrpNode->GrpAddr) < 0)
        {
            /* It means we have crossed the point before which the
             * group node is expected.
             */
            PRINTF ("No Entry Exists for the given Indices\n");
            return;
        }

        if (IPVX_ADDR_COMPARE (pGrpNode->GrpAddr, GrpAddr) == 0)
        {
            /* find the route entry in the list of (S, G) entries
             * of the group node
             */
            for (pDelRtEntry = (tMfwdRtEntry *)
                 TMO_SLL_First (&pGrpNode->SgEntryList); pDelRtEntry != NULL;)
            {
                pRtEntry = (tMfwdRtEntry *)
                    TMO_SLL_Next (&pGrpNode->SgEntryList,
                                  &pDelRtEntry->SgEntryLink);

                if ((IPVX_ADDR_COMPARE
                     (pDelRtEntry->pSrcDescNode->SrcAddr, SrcAddr) == 0)
                    || (memcmp (SrcAddr.au1Addr, &u1NullSrcAddr, 16)))
                {
                    MfwDbgPrintRt (pDelRtEntry);
                }

                pDelRtEntry = pRtEntry;

            }
        }
    }
}

VOID
MfwDbgPrintRt (tMfwdRtEntry * pRt)
{

    tMfwdOifNode       *pOifNode = NULL;
    UINT1               au1Flag[5][20] = { "\0", "PktToPIM",
        "FullPktToPIM", "PktNotToPIM"
    };
    UINT1               au1OifState[3] = { ' ', 'P', 'F' };

    MFWD_DBG (MFWD_DBG_ALL, "Oif List (");
    PRINTF ("Entry Flag State: %s\n", au1Flag[pRt->u1DeliverMDP]);
    TMO_SLL_Scan (&pRt->OifList, pOifNode, tMfwdOifNode *)
    {
        PRINTF (" (0x%x/%c) ", pOifNode->u4OifIndex,
                au1OifState[pOifNode->u4OifState]);
    }

    PRINTF (")\n");
}
