/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: mfwdcmlow.h,v 1.6 2008/08/20 15:13:52 iss Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIpCmnMRouteEnable ARG_LIST((INT4 *));

INT1
nmhGetIpCmnMRouteEntryCount ARG_LIST((UINT4 *));

INT1
nmhGetIpCmnMRouteEnableCmdb ARG_LIST((INT4 *));

INT1
nmhGetMfwdCmnGlobalTrace ARG_LIST((INT4 *));

INT1
nmhGetMfwdCmnGlobalDebug ARG_LIST((INT4 *));

INT1
nmhGetIpCmnMRouteDiscardedPkts ARG_LIST((UINT4 *));

INT1
nmhGetMfwdCmnAvgDataRate ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIpCmnMRouteEnable ARG_LIST((INT4 ));

INT1
nmhSetIpCmnMRouteEnableCmdb ARG_LIST((INT4 ));

INT1
nmhSetMfwdCmnGlobalTrace ARG_LIST((INT4 ));

INT1
nmhSetMfwdCmnGlobalDebug ARG_LIST((INT4 ));

INT1
nmhSetMfwdCmnAvgDataRate ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IpCmnMRouteEnable ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2IpCmnMRouteEnableCmdb ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2MfwdCmnGlobalTrace ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2MfwdCmnGlobalDebug ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2MfwdCmnAvgDataRate ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2IpCmnMRouteEnable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IpCmnMRouteEnableCmdb ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2MfwdCmnGlobalTrace ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2MfwdCmnGlobalDebug ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2MfwdCmnAvgDataRate ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for IpCmnMRouteTable. */
INT1
nmhValidateIndexInstanceIpCmnMRouteTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ));

/* Proto Type for Low Level GET FIRST fn for IpCmnMRouteTable  */

INT1
nmhGetFirstIndexIpCmnMRouteTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *  , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIpCmnMRouteTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIpCmnMRouteUpstreamNeighbor ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIpCmnMRouteInIfIndex ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetIpCmnMRouteUpTime ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

INT1
nmhGetIpCmnMRoutePkts ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

INT1
nmhGetIpCmnMRouteDifferentInIfPackets ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

INT1
nmhGetIpCmnMRouteProtocol ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetIpCmnMRouteRtAddress ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIpCmnMRouteRtMask ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4  * ));

INT1
nmhGetIpCmnMRouteRtType ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

/* Proto Validate Index Instance for IpCmnMRouteNextHopTable. */
INT1
nmhValidateIndexInstanceIpCmnMRouteNextHopTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for IpCmnMRouteNextHopTable  */

INT1
nmhGetFirstIndexIpCmnMRouteNextHopTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *  , INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIpCmnMRouteNextHopTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIpCmnMRouteNextHopState ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetIpCmnMRouteNextHopUpTime ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto Validate Index Instance for IpCmnMRouteInterfaceTable. */
INT1
nmhValidateIndexInstanceIpCmnMRouteInterfaceTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for IpCmnMRouteInterfaceTable  */

INT1
nmhGetFirstIndexIpCmnMRouteInterfaceTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIpCmnMRouteInterfaceTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIpCmnMRouteInterfaceOwnerId ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetIpCmnMRouteInterfaceTtl ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetIpCmnMRouteInterfaceProtocol ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetIpCmnMRouteInterfaceRateLimit ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetIpCmnMRouteInterfaceInMcastOctets ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetIpCmnMRouteInterfaceCmdbPktCnt ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetIpCmnMRouteInterfaceOutMcastOctets ARG_LIST((INT4  , INT4 ,UINT4 *));
