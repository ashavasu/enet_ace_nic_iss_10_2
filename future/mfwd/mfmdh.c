/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: mfmdh.c,v 1.12 2014/10/10 11:59:51 siva Exp $
*
*********************************************************************/
#include "mfinc.h"

/* Global Context Variable */
extern tMfwdContextStructure gMfwdContext;

#ifdef TRACE_WANTED
static UINT2        u2MfwdTrcModule = MFWD_MDH_MODULE;
#endif

/****************************************************************************
 * Function Name    :  MfwdMdhForwardMDP
 *
 * Description      :  This function forwards the multicast data packet upon 
 *                     finding a route entry other wise it calls the other 
 *                     functions to handle the cachemiss or wrongiif.
 *                     In case of cache hit if the MRP explicitly requests the
 *                     forwarding information it informs MRP about the same.
 *                     
 * Input(s)         :  u2OwnerId :- The Identification value of the owner  
 *                     for which the MDP is to be forwarded.
 *                     pMdpBuf :- pointer to the multicast data packet buffer
 *                     u4IfIndex :- The interface through which the multicast
 *                     data packet is received.
 *                     u4SrcAddr :- The address of the sender who originated
 *                     the MDP.
 *                     u4Dest :- The group address to which the multicast 
 *                     data packet is destined.
 *
 * Output(s)        :  None
 *
 * Global Variables
 * Referred         :  gMfwdContext.pOwnerInfoTbl 
 *                     (Refered via the macro MFWD_OWNER)
 *
 * Global Variables
 * Modified         :  None
 *
 * Return(s)        :  MFWD_FAILURE or MFWD_SUCCESS
 ****************************************************************************/

INT4
MfwdMdhForwardMDP (UINT2 u2OwnerId, tCRU_BUF_CHAIN_HEADER * pMdpBuf,
                   tMfwdInterfaceNode * pIfNode, tIPvXAddr SrcAddr,
                   tIPvXAddr GrpAddr)
{
    INT4                i4Status = MFWD_SUCCESS;
    tMfwdFwdInfo        FwdInfo;
    tMfwdMrpMDPMsg      MrpMesg;
    UINT4               u4IfIndex = pIfNode->u4IfIndex;
    UINT4               u4TempGrpAddr = 0;

    MFWD_DBG (MFWD_DBG_ENTRY, "Entering the function to forward MDP\n");

    /* Get the information required for forwarding of the multicast 
     * data packet 
     */
    MfwdMrtGetFwdInfo (u2OwnerId, SrcAddr, GrpAddr, u4IfIndex, &FwdInfo);

    MrpMesg.u1SearchStat = FwdInfo.u1SearchStat;
    IPVX_ADDR_COPY (&MrpMesg.SrcAddr, &SrcAddr);
    IPVX_ADDR_COPY (&MrpMesg.GrpAddr, &GrpAddr);
    MrpMesg.u4Iif = u4IfIndex;

    /* based on the search status take appropriate actions for forwarding,
     * informing a cache miss or a wrong iif
     */

    if (FwdInfo.u1SearchStat == MFWD_MDP_CACHE_HIT)
    {
        /* duplicate the buffer chain so that we will not lose the buffer
         * because of lower layer releasing it
         */
        if (FwdInfo.pOifList != NULL)
        {
            if (GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
            {
                PTR_FETCH4 (u4TempGrpAddr, GrpAddr.au1Addr);
                i4Status = MfwdSendMcastDataPkt (pMdpBuf, u4TempGrpAddr,
                                                 FwdInfo.pOifList, u4IfIndex);
            }

            if (GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
            {
                i4Status = MfwdSendIpv6McastDataPkt (pMdpBuf, GrpAddr.au1Addr,
                                                     FwdInfo.pOifList,
                                                     u4IfIndex);
            }
        }
        else
        {
            i4Status = MFWD_SUCCESS;
        }

        if (i4Status == MFWD_SUCCESS)
        {
            /* If the deliverMDP flag is set then inform the MRP of the 
             * forwarding after copying the information onto the CRU
             * buffer
             */
            pIfNode->u4InMdpOct += CRU_BUF_Get_ChainValidByteCount (pMdpBuf);
            if (FwdInfo.u1DeliverMDP == 0)
            {
                CRU_BUF_Release_MsgBufChain (pMdpBuf, FALSE);
            }

            if (FwdInfo.u1DeliverMDP != 0)
            {

                /* Deliver the multicast data packet also along with
                 * the forwarding information
                 */
                if (pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
                {
                    i4Status = MfwdMdhInformMrp ((MFWD_OWNER (u2OwnerId)->
                                                  MrpDataPktCallBkFn),
                                                 (&MrpMesg), pMdpBuf);
                }

                if (pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV6)
                {
                    i4Status = MfwdMdhInformMrp (MFWD_OWNER (u2OwnerId)->
                                                 MrpV6DataPktCallBkFn,
                                                 (&MrpMesg), pMdpBuf);
                }

                if (i4Status == MFWD_FAILURE)
                {
                    MFWD_TRC_ARG1 (MFWD_TRC_FLAG, MFWD_ALL_FAILURE_TRC,
                                   MFWD_MOD_NAME,
                                   "Failure in passing the data packet to "
                                   "to the MRP %d\n", u2OwnerId);
                }
            }                    /* end of if if ((FwdInfo.u1DeliverMDP == ....... */
        }                        /* end of if i4Status == MFWD_SUCCESS */
        else
        {
            CRU_BUF_Release_MsgBufChain (pMdpBuf, FALSE);
            MFWD_TRC (MFWD_TRC_FLAG, MFWD_ALL_FAILURE_TRC, MFWD_MOD_NAME,
                      "Failure in sending multicast data packet to IP\n");

        }

    }                            /* if (FwdInfo.u1SearchStat ==  MFWD_MDP_CACHE_HIT) */

    if (FwdInfo.u1SearchStat == MFWD_MDP_WRONG_IIF)
    {
        /* Multicast data packet needs to be given to MRP as a sparsemode MRP
         * tries to put the packet in the shared path until the Shortest
         * path tree is complete
         * TODO can be optimized to give the packet only incase of a sparse
         * mode MRP otherwise can be released.
         */
        i4Status = MfwdMdhHandleWrongIif (u2OwnerId, (&MrpMesg), pMdpBuf,
                                          pIfNode->u1AddrType);
    }

    if (FwdInfo.u1SearchStat == MFWD_MDP_CACHE_MISS)
    {
        i4Status = MfwdMdhHandleCacheMiss (u2OwnerId, pIfNode,
                                           pMdpBuf, (&MrpMesg));
    }

    MFWD_DBG (MFWD_DBG_EXIT, "Exiting the function to forward MDP\n");
    return i4Status;
}                                /* end of function MfwdForwardMDP */

/****************************************************************************
 * Function Name    :  MfwdMdhHandleWrongIif
 *
 * Description      :  This function informs about a packet arriving on an  
 *                     interface which is not equal to the value specified 
 *                     in the matching route entry.
 *                     
 * Input(s)         :  u2OwnerId :- The Identification value of the owner  
 *                     for which the MDP arrived on a wrong interface.
 *                     pMrpMesg :- pointer to the buffer that is to be sent to
 *                     the MRP.
 *
 * Output(s)        :  None
 *
 * Global Variables
 * Referred         :  gMfwdContext.pOwnerInfoTbl 
 *                     (Refered via the macro MFWD_OWNER)
 *
 * Global Variables
 * Modified         :  None
 *
 * Return(s)        :  MFWD_FAILURE or MFWD_SUCCESS
 ****************************************************************************/

INT4
MfwdMdhHandleWrongIif (UINT2 u2OwnerId, tMfwdMrpMDPMsg * pMrpMesg,
                       tCRU_BUF_CHAIN_HEADER * pMdpBuf, UINT1 u1AddrType)
{
    INT4                i4Status = MFWD_SUCCESS;

    MFWD_DBG (MFWD_DBG_ENTRY,
              "Entering the function to inform MRP of wrong iif\n");

    if (u1AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        i4Status = MfwdMdhInformMrp (MFWD_OWNER (u2OwnerId)->MrpDataPktCallBkFn,
                                     pMrpMesg, pMdpBuf);
    }

    if (u1AddrType == IPVX_ADDR_FMLY_IPV6)
    {
        i4Status =
            MfwdMdhInformMrp (MFWD_OWNER (u2OwnerId)->MrpV6DataPktCallBkFn,
                              pMrpMesg, pMdpBuf);
    }

    if (i4Status != MFWD_SUCCESS)
    {
        MFWD_TRC_ARG1 (MFWD_TRC_FLAG, MFWD_ALL_FAILURE_TRC, MFWD_MOD_NAME,
                       "Failure in informing the wring-iif message to the "
                       "MRP %d\n", u2OwnerId);
    }
    else
    {
        MFWD_TRC_ARG1 (MFWD_TRC_FLAG, MFWD_ALL_FAILURE_TRC, MFWD_MOD_NAME,
                       "Sent a wrong-iif message to the MRP %d\n", u2OwnerId);
    }

    MFWD_DBG (MFWD_DBG_ENTRY,
              "Exiting the function to inform MRP of wrong iif\n");
    return i4Status;

}                                /* end of function MfwdMdhHandlwrongIif */

/****************************************************************************
 * Function Name    :  MfwdMdhHandleCacheMiss
 *
 * Description      :  This function informs the cache-miss to the MRP if it 
 *                     is not a sparse mode MRP. If the cache miss data buffer
 *                     is enabled it adds the packet to the cache miss data 
 *                     buffer. If the MRP is sparse and it is received from
 *                     a directly connected source then it delivers MDP to the
 *                     MRP other wise it discards the MDP.
 *                     
 * Input(s)         :  u2OwnerId :- The Identification value of the owner  
 *                     for which the MDP did not have a route entry.
 *                     pMdpBuf :- pointer to the multicast data packet buffer
 *                     pMrpMsg :- Pointer to the message header that is sent
 *                     to the MRP.
 *
 * Output(s)        :  None
 *
 * Global Variables
 * Referred         :  gMfwdContext.pOwnerInfoTbl 
 *                     (Refered via the macro MFWD_OWNER)
 *
 * Global Variables
 * Modified         :  None
 *
 * Return(s)        :  MFWD_FAILURE or MFWD_SUCCESS
 ****************************************************************************/

INT4
MfwdMdhHandleCacheMiss (UINT2 u2OwnerId, tMfwdInterfaceNode * pIfNode,
                        tCRU_BUF_CHAIN_HEADER * pMdpBuf,
                        tMfwdMrpMDPMsg * pMrpMesg)
{
    INT4                i4Status = MFWD_FAILURE;
    INT4                i4RetCode;
    tCacheMissDBNode   *pCmdbNode = NULL;
    tCMDataPktNode     *pCmMdpNode = NULL;

    MFWD_DBG (MFWD_DBG_ENTRY, "Entering the function to handle CACHEMISS\n");

    /* Inform cache miss only if it is necessary */

    if (pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        i4Status = MfwdMdhInformMrp (MFWD_OWNER (u2OwnerId)->MrpDataPktCallBkFn,
                                     pMrpMesg, pMdpBuf);
    }
    else
    {
        i4Status =
            MfwdMdhInformMrp (MFWD_OWNER (u2OwnerId)->MrpV6DataPktCallBkFn,
                              pMrpMesg, pMdpBuf);
    }

    if (i4Status == MFWD_SUCCESS)
    {
        if ((gMfwdContext.u1CacheMissDBStat == MFWD_CMDB_STATUS_ENABLED) &&
            (MFWD_OWNER_MODE (u2OwnerId) != MFWD_MRP_SPARSE_MODE))
        {
            MFWD_TRC (MFWD_TRC_FLAG, MFWD_DATA_PATH_TRC, MFWD_MOD_NAME,
                      "Cache MISS data buffer feature enabled\n");

            /* See if the CMDB Node already exists if it exists add the MDP
             * to that node
             * if it does not exists create one and then add 
             */

            TMO_SLL_Scan (&MFWD_IF_CMDB (pIfNode), pCmdbNode,
                          tCacheMissDBNode *)
            {
                if ((IPVX_ADDR_COMPARE (pCmdbNode->SrcAddr, pMrpMesg->SrcAddr)
                     == 0)
                    &&
                    (IPVX_ADDR_COMPARE (pCmdbNode->GrpAddr, pMrpMesg->GrpAddr)
                     == 0))
                {
                    break;
                }
            }                    /* end of TMO_HASH_Scan_Bucket */

            if (pCmdbNode == NULL)
            {
                /* Create a new CMDB Node */
                MFWD_MEMPOOL_ALLOC (CACHE_MISS_DB_NODE_POOLID,
                                    tCacheMissDBNode *, pCmdbNode, i4RetCode);
                if (i4RetCode != MFWD_SUCCESS)
                {
                    MFWD_DBG (MFWD_DBG_MEM_IF,
                              "Failure in allocating memory block for the"
                              "cache miss data buffer nodes\n");
                }
                else
                {
                    /* Initialise the CMDB Node */
                    TMO_SLL_Add (&MFWD_IF_CMDB (pIfNode),
                                 &pCmdbNode->NextDbNode);
                    TMO_SLL_Init (&pCmdbNode->McastDataPkts);
                    IPVX_ADDR_COPY (&pCmdbNode->SrcAddr, &pMrpMesg->SrcAddr);
                    IPVX_ADDR_COPY (&pCmdbNode->GrpAddr, &pMrpMesg->GrpAddr);
                    MFWD_DBG (MFWD_DBG_MEM_IF,
                              "Cachemiss DB Node not found allocated memory"
                              "block for the CMDB Node\n");
                }

            }                    /* end of if pCmdbNode == NULL */

            if (pCmdbNode != NULL)
            {
                /* Create a new CM Data Packet Node */
                MFWD_MEMPOOL_ALLOC (CACHE_MISS_DATA_PKTS_POOLID,
                                    tCMDataPktNode *, pCmMdpNode, i4RetCode);
                if (i4RetCode != MFWD_SUCCESS)
                {
                    MFWD_DBG (MFWD_DBG_MEM_IF,
                              "Failure in allocating memory block for the"
                              "cache miss data Packet nodes\n");
                }
                else
                {
                    /* Initialise the CM Data packet node */
                    TMO_SLL_Add (&pCmdbNode->McastDataPkts,
                                 &pCmMdpNode->NextPkt);
                    pCmMdpNode->pMcastDataPkt = pMdpBuf;
                    pCmMdpNode->u4Iif = pMrpMesg->u4Iif;
                    pIfNode->u4CmdpCount++;
                    MFWD_DBG (MFWD_DBG_MEM_IF,
                              "Successfully added Mcast Data packet Cache"
                              "buffer list\n");
                }
            }                    /* end of if pCmdbNode != NULL */

        }                        /* end of CMDB status disabled */

        MFWD_TRC_ARG1 (MFWD_TRC_FLAG, MFWD_ALL_FAILURE_TRC,
                       MFWD_MOD_NAME,
                       "Informed Cache-miss to MRP %d\n", u2OwnerId);
    }
    else
    {
        MFWD_TRC (MFWD_TRC_FLAG, MFWD_ALL_FAILURE_TRC, MFWD_MOD_NAME,
                  "Failure in Delivering the cache miss info to MRP\n");
    }

    MFWD_DBG (MFWD_DBG_EXIT, "Exiting the function to Handle CACHEMISS\n");
    return i4Status;
}                                /* end of function MfwdForwardMDP */

/****************************************************************************
 * Function Name    :  MfwdMdhHandleCacheMissData
 *
 * Description      :  This function clears all the multicast data packets for
 *                     which an entry is created and forwards the packets
 *                     that have arrived on the correct iif through the oif 
 *                     list supplied otherwise discards the packets.
 *                     
 * Input(s)         :  u2OwnerId :- The Identification value of the owner  
 *                     for which the MDP did not have a route entry.
 *                     pu4OifList :- The list of interfaces through 
 *                     which the packets are to be forwarded if arrived on 
 *                     correct iif.
 *                     Iif :- The incoming interface through which the packet
 *                     is to arrive.
 *                     u4SrcAddr:- The source address through which the packet
 *                     is supposed to arrive.
 *                     u4GrpAddr :- The group address of the entry.
 *
 * Output(s)        :  None
 *
 * Global Variables
 * Referred         :  None
 *
 * Global Variables
 * Modified         :  None
 *
 * Return(s)        :  MFWD_FAILURE or MFWD_SUCCESS
 ****************************************************************************/

INT4
MfwdMdhHandleCacheMissData (UINT2 u2OwnerId, tTMO_SLL * pOifList,
                            UINT4 u4Iif, tIPvXAddr SrcAddr, tIPvXAddr GrpAddr)
{
    INT4                i4Status = MFWD_SUCCESS;
    tCacheMissDBNode   *pCmdbNode = NULL;
    tMfwdInterfaceNode *pIfNode = NULL;
    tCMDataPktNode     *pCmMdpNode = NULL;
    UINT4               u4TempGrpAddr = 0;

    MFWD_DBG (MFWD_DBG_ENTRY,
              "Entering the function to handle CACHEMISS data packets\n");

    UNUSED_PARAM (u2OwnerId);

    /* GRAB A LOCK ON THE INTERFACE */
    pIfNode = MFWD_GET_IF_NODE (u4Iif, GrpAddr.u1Afi);
    if (NULL == pIfNode)
    {
        MFWD_TRC (MFWD_TRC_FLAG, MFWD_ALL_FAILURE_TRC,
                  MFWD_MOD_NAME, "Invalid incoming interface\n");
        return MFWD_FAILURE;
    }

    /* find the CMDB Node with the source and group address supplied 
     * by the entry creation routine
     */
    TMO_SLL_Scan (&(MFWD_IF_CMDB (pIfNode)), pCmdbNode, tCacheMissDBNode *)
    {
        if ((IPVX_ADDR_COMPARE (pCmdbNode->SrcAddr, SrcAddr) == 0) &&
            (IPVX_ADDR_COMPARE (pCmdbNode->GrpAddr, GrpAddr) == 0))
        {
            break;
        }
    }                            /* end of TMO_SLL_Scan */

    if (pCmdbNode != NULL)
    {
        /* Try to forward each cachemiss data packet in the buffer if possible
         */

        pIfNode->u4CmdpCount -= TMO_SLL_Count (&(pCmdbNode->McastDataPkts));
        while ((pCmMdpNode = (tCMDataPktNode *)
                TMO_SLL_First (&(pCmdbNode->McastDataPkts))) != NULL)
        {
            /* If there exists atleast one oif then we can forward the 
             * packet through the oif list  
             */
            if (pOifList != NULL)
            {
                if (GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
                {
                    PTR_FETCH4 (u4TempGrpAddr, GrpAddr.au1Addr);
                    i4Status = MfwdSendMcastDataPkt (pCmMdpNode->pMcastDataPkt,
                                                     u4TempGrpAddr, pOifList,
                                                     u4Iif);
                }

                if (GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
                {
                    i4Status =
                        MfwdSendIpv6McastDataPkt (pCmMdpNode->pMcastDataPkt,
                                                  GrpAddr.au1Addr, pOifList,
                                                  u4Iif);
                }

                if (i4Status == MFWD_SUCCESS)
                {
                    MFWD_TRC (MFWD_TRC_FLAG, MFWD_DATA_PATH_TRC,
                              MFWD_MOD_NAME, "Successfully forwarded "
                              "MDP from the CMDB through the oifs\n");
                }
                else
                {
                    MFWD_TRC (MFWD_TRC_FLAG, MFWD_ALL_FAILURE_TRC,
                              MFWD_MOD_NAME,
                              "Failure in forwarding MDP from"
                              "the CMDB through the oifs\n");
                }

                CRU_BUF_Release_MsgBufChain (pCmMdpNode->pMcastDataPkt, TRUE);
            }                    /* end of if (pCmMdpNode->u4Iif == u4Iif) */
            else
            {
                /* It is our duty to release if the oifs do not exist for the
                 * created route entry
                 */
                CRU_BUF_Release_MsgBufChain (pCmMdpNode->pMcastDataPkt, TRUE);
            }

            /* delete the node from the SLL and release to the pool */
            TMO_SLL_Delete (&pCmdbNode->McastDataPkts, &pCmMdpNode->NextPkt);

            MFWD_MEMRELEASE (CACHE_MISS_DATA_PKTS_POOLID, pCmMdpNode, i4Status);

        }                        /* end of while loop */

        TMO_SLL_Delete (&MFWD_IF_CMDB (pIfNode), &pCmdbNode->NextDbNode);
        MFWD_MEMRELEASE (CACHE_MISS_DB_NODE_POOLID, pCmdbNode, i4Status);

    }                            /* end of if (pCmdbNode != NULL) */
    else
    {
        MFWD_TRC (MFWD_TRC_FLAG, MFWD_DATA_PATH_TRC, MFWD_MOD_NAME,
                  "No data packets in the cache miss data buffer to forward\n");
    }

    if (i4Status == MFWD_FAILURE)
    {
        MFWD_TRC (MFWD_TRC_FLAG, MFWD_CONTROL_PATH_TRC, MFWD_MOD_NAME,
                  "Failure in Giving the interface node semaphore after "
                  "handling Cache Miss data\n");
    }

    MFWD_DBG (MFWD_DBG_EXIT, "Exiting the function to Handle CACHEMISS\n");

    return i4Status;
}                                /* end of function MfwdMdhHandleCacheMissData */

/****************************************************************************
 * Function Name    :  MfwdMdhHandlecmdbtmrExp
 *
 * Description      :  This function clears all the multicast data packets 
 *                     stored for this owner in the cache miss data buffer
 *                     
 * Input(s)         :  None  
 *
 * Output(s)        :  None
 *
 * Global Variables
 * Referred         :  None
 *
 * Global Variables
 * Modified         :  None
 *
 * Return(s)        :  MFWD_FAILURE or MFWD_SUCCESS
 ****************************************************************************/
void
MfwdMdhHandleCmdbTmrExp (UINT2 u2OwnerId)
{
    tMfwdInterfaceNode *pIfNode = NULL;
    tCacheMissDBNode   *pCmdbNode = NULL;
    tTMO_SLL_NODE      *pSllNode = NULL;
    INT4                i4RetCode;
    INT4                i4Status = MFWD_SUCCESS;

    MFWD_DBG (MFWD_DBG_ENTRY, "Entering the function to Handle CMDB timer "
              "expiry\n");

    if (MFWD_OWNER (u2OwnerId)->CmdbTimer.u4Data != 0)
    {
        i4RetCode = TmrStopTimer (gMfwdContext.CMDBTmrListId,
                                  &MFWD_OWNER (u2OwnerId)->CmdbTimer);
        if (i4RetCode != TMR_SUCCESS)
        {
            MFWD_DBG (MFWD_DBG_TMR_IF, "Failure in stopping the CMDB timer\n");
            i4Status = MFWD_FAILURE;
        }
        MFWD_OWNER (u2OwnerId)->CmdbTimer.u4Data = 0;
    }
    if (i4Status != MFWD_FAILURE)
    {
        TMO_SLL_Scan (&MFWD_OWNER (u2OwnerId)->OwnerIfList, pSllNode,
                      tTMO_SLL_NODE *)
        {
            pIfNode = MFWD_GET_BASE_PTR (tMfwdInterfaceNode,
                                         OwnerIfLink, pSllNode);

            MFWD_TRC_ARG1 (MFWD_TRC_FLAG, MFWD_TMR_EXP_TRC, MFWD_MOD_NAME,
                           "Clearing the cache miss data buffer on the "
                           "interface %d\n", pIfNode->u4IfIndex);
            while ((pCmdbNode = (tCacheMissDBNode *)
                    TMO_SLL_First (&pIfNode->CmdbList)) != NULL)
            {
                MfwdMdhHandleCacheMissData (u2OwnerId, NULL,
                                            pIfNode->u4IfIndex,
                                            pCmdbNode->SrcAddr,
                                            pCmdbNode->GrpAddr);
            }
        }
    }

    if (MFWD_OWNER (u2OwnerId)->CmdbTimer.u4Data == 0)
    {
        MFWD_OWNER (u2OwnerId)->CmdbTimer.u4Data = (0xFFFF0000 |
                                                    (UINT4) u2OwnerId);
        i4RetCode = TmrStartTimer (gMfwdContext.CMDBTmrListId,
                                   &MFWD_OWNER (u2OwnerId)->CmdbTimer,
                                   MFWD_CMDB_TIMER_DUR);
    }

    MFWD_DBG (MFWD_DBG_EXIT, "Exiting the function to Handle CMDB timer "
              "expiry\n");
    return;
}

/****************************************************************************
 * Function Name    :  MfwdMdhInformMrp
 *
 * Description      :  This function posts the message that is to be sent  
 *                     to the MRP about the cachemiss, wrong iif or forwarding
 *                     of an MDP
 *                     
 * Input(s)         :  None  
 *
 * Output(s)        :  None
 *
 * Global Variables
 * Referred         :  None
 *
 * Global Variables
 * Modified         :  None
 *
 * Return(s)        :  MFWD_FAILURE or MFWD_SUCCESS
 ****************************************************************************/

INT4
MfwdMdhInformMrp (VOID (*DataPktCallBkFn) (tCRU_BUF_CHAIN_HEADER *),
                  tMfwdMrpMDPMsg * pMrpMsg, tCRU_BUF_CHAIN_HEADER * pMdpBuf)
{
    INT4                i4RetCode = MFWD_SUCCESS;
    INT4                i4Status = MFWD_SUCCESS;

    MFWD_DBG (MFWD_DBG_ENTRY,
              "Entering the function to Inform MRP of the MDP\n");

    if (pMdpBuf != NULL)
    {
        /* If the multicast data packet is also to be sent then 
         * prepend the MFWD message to the MDP and send it
         */
        i4RetCode = CRU_BUF_Prepend_BufChain (pMdpBuf, (UINT1 *) pMrpMsg,
                                              sizeof (tMfwdMrpMDPMsg));
        if (i4RetCode != CRU_SUCCESS)
        {
            MFWD_DBG (MFWD_DBG_BUF_IF, "Failure in prepending MFWD message"
                      "to the Multicast data packet\n");
            CRU_BUF_Release_MsgBufChain (pMdpBuf, FALSE);
        }
        else
        {
            (*DataPktCallBkFn) (pMdpBuf);
        }
    }                            /* end of if mdpbuf != NULL */
    else
    {
        /* If only the MFWD message is to be sent make a buffer chain of it
         * and send it to the MRP
         */
        pMdpBuf = CRU_BUF_Allocate_MsgBufChain (sizeof (tMfwdMrpMDPMsg), 0);
        if (pMdpBuf == NULL)
        {
            MFWD_DBG (MFWD_DBG_BUF_IF, "Failure in Allocating buffer for "
                      "MFWD to MRP message\n");
            i4Status = MFWD_FAILURE;
        }
        else
        {
            CRU_BUF_Copy_OverBufChain (pMdpBuf, (UINT1 *) pMrpMsg, 0,
                                       sizeof (tMfwdMrpMDPMsg));
            (*DataPktCallBkFn) (pMdpBuf);
        }                        /* end of else for if pMdpBuf == NULL */

    }                            /* end of else for if pMdpBuf != NULL */

    MFWD_DBG (MFWD_DBG_EXIT, "Exiting the function to Inform MRP of the MDP\n");
    return i4Status;
}                                /* end of function MfwdMdhInformMrp */

/****************************************************************************
 * Function Name    :  MfwdMdhdisableCmdb
 *
 * Description      :  This function clears the Cache miss data packets from
 *                     the interfaces of the owner and deletes the cache miss 
 *                     data packets pool
 *                     
 * Input(s)         :  None  
 *
 * Output(s)        :  None
 *
 * Global Variables
 * Referred         :  None
 *
 * Global Variables
 * Modified         :  None
 *
 * Return(s)        :  MFWD_FAILURE or MFWD_SUCCESS
 ****************************************************************************/

void
MfwdMdhDisableCmdb ()
{
    UINT2               u2OwnerId;

    /* flush all the previously stored packets and then delete
     * the memory pool allocated for them and disable CMDB
     */
    MFWD_TRC (MFWD_TRC_FLAG, MFWD_CONTROL_PATH_TRC, MFWD_MOD_NAME,
              "CMDB Status disabled so freeing all the current packets\n");

    gMfwdContext.u1CacheMissDBStat = MFWD_CMDB_STATUS_DISABLED;
    for (u2OwnerId = 0; u2OwnerId < MAX_MRP_INSTANCES; u2OwnerId++)
    {
        if (MFWD_OWNER (u2OwnerId) == NULL)
        {
            continue;
        }

        if (MFWD_OWNER_MODE (u2OwnerId) != MFWD_MRP_SPARSE_MODE)
        {
            /* Lock is released intially because HandleCmdbTmrpExp tries
             * to obtain the lock again
             */
            MfwdMdhHandleCmdbTmrExp (u2OwnerId);
            continue;
        }

    }                            /* end of for loop */

    /* Delete both the cache miss data packets and data buffer node pool
     * ids
     */
    MemDeleteMemPool (CACHE_MISS_DATA_PKTS_POOLID);

    MFWD_TRC (MFWD_TRC_FLAG, MFWD_CONTROL_PATH_TRC, MFWD_MOD_NAME,
              "MFWD status is now DISABLED \n");

    MFWD_DBG (MFWD_DBG_EXIT, "Exiting the function disable MFWD\n");
}                                /* end of the function MfwdMdhDisbaleMfwd */

/****************************************************************************
 * Function Name    :  MfwdMdhdisableCmdb
 *
 * Description      :  This function creates memory pool for the cache miss data
 *                     packets  and makes the status of the CMDB as enabled.
 *                     
 * Input(s)         :  None  
 *
 * Output(s)        :  None
 *
 * Global Variables
 * Referred         :  None
 *
 * Global Variables
 * Modified         :  None
 *
 * Return(s)        :  MFWD_FAILURE or MFWD_SUCCESS
 ****************************************************************************/

void
MfwdEnableCmdbStatus ()
{

    if (MemCreateMemPool (sizeof (tCMDataPktNode),
                          MAX_CACHE_MISS_DB_PKTS, MFWD_MEM_DEFAULT_MEMORY_TYPE,
                          &(CACHE_MISS_DATA_PKTS_POOLID)) != MEM_SUCCESS)
    {
        MFWD_DBG (MFWD_DBG_ERROR | MFWD_DBG_MEM_IF,
                  "Error Creating the memory pool for the CM Data packets\n");
    }
    else
    {
        gMfwdContext.u1CacheMissDBStat = MFWD_CMDB_STATUS_ENABLED;
        MFWD_TRC (MFWD_TRC_FLAG, MFWD_CONTROL_PATH_TRC, MFWD_MOD_NAME,
                  "CMDB Status is now ENABLED\n");
    }
}

/*-------------------------------------------------------------------+
 *
 * Function           : MfwdExtractIpHdr
 *
 * Input(s)           : pIp, pBuf
 *
 * Output(s)          : None
 *
 * Returns            : MFWD_SUCCESS, MFWD_FAILURE
 *
 * Action :
 * This routine is used to get the IP header in the packet.
 *
+-------------------------------------------------------------------*/
#ifdef __STDC__
INT4
MfwdExtractIpHdr (t_IP * pIp, tCRU_BUF_CHAIN_HEADER * pBuf)
#else
INT4
MfwdExtractIpHdr (pIp, pBuf)
     t_IP               *pIp;
     tCRU_BUF_CHAIN_HEADER *pBuf;
#endif
{
    t_IP_HEADER        *pIpHdr = NULL;
    t_IP_HEADER         TmpIpHdr;
    UINT1               u1Tmp;
    UINT1              *pu1Options;
    UINT4               u4TempSrc = 0, u4TempDest = 0;

    pIpHdr =
        (t_IP_HEADER *) (VOID *) CRU_BUF_Get_DataPtr_IfLinear (pBuf, 0,
                                                               IP_HDR_LEN);

    if (pIpHdr == NULL)
    {

        /* The header is not contiguous in the buffer */

        pIpHdr = &TmpIpHdr;
        /* Copy the header */
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) pIpHdr, 0, IP_HDR_LEN);
    }

    u1Tmp = pIpHdr->u1Ver_hdrlen;

    pIp->u1Version = MFWD_IP_VERS (u1Tmp);
    pIp->u1Hlen = (UINT1) ((u1Tmp & 0x0f) << 2);
    pIp->u1Tos = pIpHdr->u1Tos;
    pIp->u2Len = OSIX_NTOHS (pIpHdr->u2Totlen);

    if (pIp->u1Hlen < MFWD_IP_HDR_LEN || pIp->u2Len < pIp->u1Hlen)
    {

        MFWD_TRC_ARG1 (MFWD_TRC_FLAG, MFWD_ALL_FAILURE_TRC, MFWD_MOD_NAME,
                       "Discarding pkt rcvd with header length %d.\n",
                       pIp->u1Hlen);
        return MFWD_FAILURE;
    }

    pIp->u2Olen = MFWD_IP_OLEN (u1Tmp);
    pIp->u2Id = OSIX_NTOHS (pIpHdr->u2Id);
    pIp->u2Fl_offs = OSIX_NTOHS (pIpHdr->u2Fl_offs);
    pIp->u1Ttl = pIpHdr->u1Ttl;
    pIp->u1Proto = pIpHdr->u1Proto;
    pIp->u2Cksum = OSIX_NTOHS (pIpHdr->u2Cksum);

    MFWD_MEMCPY (&u4TempSrc, &pIpHdr->u4Src, sizeof (UINT4));
    MFWD_MEMCPY (&u4TempDest, &pIpHdr->u4Dest, sizeof (UINT4));

    u4TempSrc = OSIX_NTOHL (u4TempSrc);
    u4TempDest = OSIX_NTOHL (u4TempDest);

    MFWD_MEMCPY (&pIp->u4Src, &u4TempSrc, sizeof (UINT4));
    MFWD_MEMCPY (&pIp->u4Dest, &u4TempDest, sizeof (UINT4));

    if (pIp->u2Olen)
    {
        pu1Options =
            CRU_BUF_Get_DataPtr_IfLinear (pBuf, IP_HDR_LEN, pIp->u2Olen);
        if (pu1Options == NULL)
        {
            CRU_BUF_Copy_FromBufChain (pBuf, pIp->au1Options, IP_HDR_LEN,
                                       pIp->u2Olen);
        }
        else
        {
            MFWD_MEMCPY (pIp->au1Options, pu1Options, pIp->u2Olen);
        }
    }
    return MFWD_SUCCESS;
}

/*-------------------------------------------------------------------+
 *
 * Function           : MfwdExtractIp6Hdr
 *
 * Input(s)           : pIp6, pBuf
 *
 * Output(s)          : None
 *
 * Returns            : MFWD_SUCCESS, MFWD_FAILURE
 *
 * Action :
 * This routine is used to get the IP header in the packet.
 *
+-------------------------------------------------------------------*/
#ifdef __STDC__
INT4
MfwdExtractIp6Hdr (tIp6Hdr * pIp6, tCRU_BUF_CHAIN_HEADER * pBuf)
#else
INT4
MfwdExtractIp6Hdr (pIp, pBuf)
     tIp6Hdr            *pIp6;
     tCRU_BUF_CHAIN_HEADER *pBuf;
#endif
{
    tIp6Hdr             TmpIp6Hdr;
    tIp6Hdr            *pTmpIp6Hdr;
    MEMSET (&TmpIp6Hdr, 0, sizeof (tIp6Hdr));

    pTmpIp6Hdr =
        (tIp6Hdr *) (VOID *) CRU_BUF_Get_DataPtr_IfLinear (pBuf, 0,
                                                           sizeof (tIp6Hdr));
    if (pTmpIp6Hdr == NULL)
    {
        /* The header is not contiguous in the buffer */
        pTmpIp6Hdr = &TmpIp6Hdr;
        /* Copy the header */
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) pTmpIp6Hdr,
                                   0, sizeof (tIp6Hdr));
    }

    MEMCPY (pIp6, pTmpIp6Hdr, sizeof (tIp6Hdr));
    pIp6->u2Len = OSIX_NTOHS (pIp6->u2Len);
    return MFWD_SUCCESS;
}
