/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmfwddb.h,v 1.4 2008/08/20 15:13:52 iss Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSMFWDDB_H
#define _FSMFWDDB_H

UINT1 IpMRouteTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 IpMRouteNextHopTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 IpMRouteInterfaceTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};

UINT4 fsmfwd [] ={1,3,6,1,4,1,2076,71};
tSNMP_OID_TYPE fsmfwdOID = {8, fsmfwd};


UINT4 IpMRouteEnable [ ] ={1,3,6,1,4,1,2076,71,1,1,1};
UINT4 IpMRouteEntryCount [ ] ={1,3,6,1,4,1,2076,71,1,1,2};
UINT4 IpMRouteEnableCmdb [ ] ={1,3,6,1,4,1,2076,71,1,1,3};
UINT4 MfwdGlobalTrace [ ] ={1,3,6,1,4,1,2076,71,1,1,4};
UINT4 MfwdGlobalDebug [ ] ={1,3,6,1,4,1,2076,71,1,1,5};
UINT4 IpMRouteDiscardedPkts [ ] ={1,3,6,1,4,1,2076,71,1,1,6};
UINT4 MfwdAvgDataRate [ ] ={1,3,6,1,4,1,2076,71,1,1,7};
UINT4 IpMRouteOwnerId [ ] ={1,3,6,1,4,1,2076,71,1,2,1,1,1};
UINT4 IpMRouteGroup [ ] ={1,3,6,1,4,1,2076,71,1,2,1,1,2};
UINT4 IpMRouteSource [ ] ={1,3,6,1,4,1,2076,71,1,2,1,1,3};
UINT4 IpMRouteSourceMask [ ] ={1,3,6,1,4,1,2076,71,1,2,1,1,4};
UINT4 IpMRouteUpstreamNeighbor [ ] ={1,3,6,1,4,1,2076,71,1,2,1,1,5};
UINT4 IpMRouteInIfIndex [ ] ={1,3,6,1,4,1,2076,71,1,2,1,1,6};
UINT4 IpMRouteUpTime [ ] ={1,3,6,1,4,1,2076,71,1,2,1,1,7};
UINT4 IpMRoutePkts [ ] ={1,3,6,1,4,1,2076,71,1,2,1,1,8};
UINT4 IpMRouteDifferentInIfPackets [ ] ={1,3,6,1,4,1,2076,71,1,2,1,1,9};
UINT4 IpMRouteProtocol [ ] ={1,3,6,1,4,1,2076,71,1,2,1,1,10};
UINT4 IpMRouteRtAddress [ ] ={1,3,6,1,4,1,2076,71,1,2,1,1,11};
UINT4 IpMRouteRtMask [ ] ={1,3,6,1,4,1,2076,71,1,2,1,1,12};
UINT4 IpMRouteRtType [ ] ={1,3,6,1,4,1,2076,71,1,2,1,1,13};
UINT4 IpMRouteNextHopOwnerId [ ] ={1,3,6,1,4,1,2076,71,1,2,2,1,1};
UINT4 IpMRouteNextHopGroup [ ] ={1,3,6,1,4,1,2076,71,1,2,2,1,2};
UINT4 IpMRouteNextHopSource [ ] ={1,3,6,1,4,1,2076,71,1,2,2,1,3};
UINT4 IpMRouteNextHopSourceMask [ ] ={1,3,6,1,4,1,2076,71,1,2,2,1,4};
UINT4 IpMRouteNextHopIfIndex [ ] ={1,3,6,1,4,1,2076,71,1,2,2,1,5};
UINT4 IpMRouteNextHopAddress [ ] ={1,3,6,1,4,1,2076,71,1,2,2,1,6};
UINT4 IpMRouteNextHopState [ ] ={1,3,6,1,4,1,2076,71,1,2,2,1,7};
UINT4 IpMRouteNextHopUpTime [ ] ={1,3,6,1,4,1,2076,71,1,2,2,1,8};
UINT4 IpMRouteInterfaceIfIndex [ ] ={1,3,6,1,4,1,2076,71,1,2,3,1,1};
UINT4 IpMRouteInterfaceOwnerId [ ] ={1,3,6,1,4,1,2076,71,1,2,3,1,2};
UINT4 IpMRouteInterfaceTtl [ ] ={1,3,6,1,4,1,2076,71,1,2,3,1,3};
UINT4 IpMRouteInterfaceProtocol [ ] ={1,3,6,1,4,1,2076,71,1,2,3,1,4};
UINT4 IpMRouteInterfaceRateLimit [ ] ={1,3,6,1,4,1,2076,71,1,2,3,1,5};
UINT4 IpMRouteInterfaceInMcastOctets [ ] ={1,3,6,1,4,1,2076,71,1,2,3,1,6};
UINT4 IpMRouteInterfaceCmdbPktCnt [ ] ={1,3,6,1,4,1,2076,71,1,2,3,1,7};
UINT4 IpMRouteInterfaceOutMcastOctets [ ] ={1,3,6,1,4,1,2076,71,1,2,3,1,8};


tMbDbEntry fsmfwdMibEntry[]= {

{{11,IpMRouteEnable}, NULL, IpMRouteEnableGet, IpMRouteEnableSet, IpMRouteEnableTest, IpMRouteEnableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,IpMRouteEntryCount}, NULL, IpMRouteEntryCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,IpMRouteEnableCmdb}, NULL, IpMRouteEnableCmdbGet, IpMRouteEnableCmdbSet, IpMRouteEnableCmdbTest, IpMRouteEnableCmdbDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,MfwdGlobalTrace}, NULL, MfwdGlobalTraceGet, MfwdGlobalTraceSet, MfwdGlobalTraceTest, MfwdGlobalTraceDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{11,MfwdGlobalDebug}, NULL, MfwdGlobalDebugGet, MfwdGlobalDebugSet, MfwdGlobalDebugTest, MfwdGlobalDebugDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{11,IpMRouteDiscardedPkts}, NULL, IpMRouteDiscardedPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,MfwdAvgDataRate}, NULL, MfwdAvgDataRateGet, MfwdAvgDataRateSet, MfwdAvgDataRateTest, MfwdAvgDataRateDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1000"},

{{13,IpMRouteOwnerId}, GetNextIndexIpMRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, IpMRouteTableINDEX, 4, 0, 0, NULL},

{{13,IpMRouteGroup}, GetNextIndexIpMRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, IpMRouteTableINDEX, 4, 0, 0, NULL},

{{13,IpMRouteSource}, GetNextIndexIpMRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, IpMRouteTableINDEX, 4, 0, 0, NULL},

{{13,IpMRouteSourceMask}, GetNextIndexIpMRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, IpMRouteTableINDEX, 4, 0, 0, NULL},

{{13,IpMRouteUpstreamNeighbor}, GetNextIndexIpMRouteTable, IpMRouteUpstreamNeighborGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, IpMRouteTableINDEX, 4, 0, 0, NULL},

{{13,IpMRouteInIfIndex}, GetNextIndexIpMRouteTable, IpMRouteInIfIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, IpMRouteTableINDEX, 4, 0, 0, NULL},

{{13,IpMRouteUpTime}, GetNextIndexIpMRouteTable, IpMRouteUpTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, IpMRouteTableINDEX, 4, 0, 0, NULL},

{{13,IpMRoutePkts}, GetNextIndexIpMRouteTable, IpMRoutePktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IpMRouteTableINDEX, 4, 0, 0, NULL},

{{13,IpMRouteDifferentInIfPackets}, GetNextIndexIpMRouteTable, IpMRouteDifferentInIfPacketsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IpMRouteTableINDEX, 4, 0, 0, NULL},

{{13,IpMRouteProtocol}, GetNextIndexIpMRouteTable, IpMRouteProtocolGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, IpMRouteTableINDEX, 4, 0, 0, NULL},

{{13,IpMRouteRtAddress}, GetNextIndexIpMRouteTable, IpMRouteRtAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, IpMRouteTableINDEX, 4, 0, 0, NULL},

{{13,IpMRouteRtMask}, GetNextIndexIpMRouteTable, IpMRouteRtMaskGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, IpMRouteTableINDEX, 4, 0, 0, NULL},

{{13,IpMRouteRtType}, GetNextIndexIpMRouteTable, IpMRouteRtTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, IpMRouteTableINDEX, 4, 0, 0, NULL},

{{13,IpMRouteNextHopOwnerId}, GetNextIndexIpMRouteNextHopTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, IpMRouteNextHopTableINDEX, 6, 0, 0, NULL},

{{13,IpMRouteNextHopGroup}, GetNextIndexIpMRouteNextHopTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, IpMRouteNextHopTableINDEX, 6, 0, 0, NULL},

{{13,IpMRouteNextHopSource}, GetNextIndexIpMRouteNextHopTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, IpMRouteNextHopTableINDEX, 6, 0, 0, NULL},

{{13,IpMRouteNextHopSourceMask}, GetNextIndexIpMRouteNextHopTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, IpMRouteNextHopTableINDEX, 6, 0, 0, NULL},

{{13,IpMRouteNextHopIfIndex}, GetNextIndexIpMRouteNextHopTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, IpMRouteNextHopTableINDEX, 6, 0, 0, NULL},

{{13,IpMRouteNextHopAddress}, GetNextIndexIpMRouteNextHopTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, IpMRouteNextHopTableINDEX, 6, 0, 0, NULL},

{{13,IpMRouteNextHopState}, GetNextIndexIpMRouteNextHopTable, IpMRouteNextHopStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, IpMRouteNextHopTableINDEX, 6, 0, 0, NULL},

{{13,IpMRouteNextHopUpTime}, GetNextIndexIpMRouteNextHopTable, IpMRouteNextHopUpTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, IpMRouteNextHopTableINDEX, 6, 0, 0, NULL},

{{13,IpMRouteInterfaceIfIndex}, GetNextIndexIpMRouteInterfaceTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, IpMRouteInterfaceTableINDEX, 1, 0, 0, NULL},

{{13,IpMRouteInterfaceOwnerId}, GetNextIndexIpMRouteInterfaceTable, IpMRouteInterfaceOwnerIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, IpMRouteInterfaceTableINDEX, 1, 0, 0, NULL},

{{13,IpMRouteInterfaceTtl}, GetNextIndexIpMRouteInterfaceTable, IpMRouteInterfaceTtlGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, IpMRouteInterfaceTableINDEX, 1, 0, 0, NULL},

{{13,IpMRouteInterfaceProtocol}, GetNextIndexIpMRouteInterfaceTable, IpMRouteInterfaceProtocolGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, IpMRouteInterfaceTableINDEX, 1, 0, 0, NULL},

{{13,IpMRouteInterfaceRateLimit}, GetNextIndexIpMRouteInterfaceTable, IpMRouteInterfaceRateLimitGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, IpMRouteInterfaceTableINDEX, 1, 0, 0, "0"},

{{13,IpMRouteInterfaceInMcastOctets}, GetNextIndexIpMRouteInterfaceTable, IpMRouteInterfaceInMcastOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IpMRouteInterfaceTableINDEX, 1, 0, 0, NULL},

{{13,IpMRouteInterfaceCmdbPktCnt}, GetNextIndexIpMRouteInterfaceTable, IpMRouteInterfaceCmdbPktCntGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IpMRouteInterfaceTableINDEX, 1, 0, 0, NULL},

{{13,IpMRouteInterfaceOutMcastOctets}, GetNextIndexIpMRouteInterfaceTable, IpMRouteInterfaceOutMcastOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IpMRouteInterfaceTableINDEX, 1, 0, 0, NULL},
};
tMibData fsmfwdEntry = { 36, fsmfwdMibEntry };
#endif /* _FSMFWDDB_H */

