#ifndef _FSMFWDWR_H
#define _FSMFWDWR_H

VOID RegisterFSMFWD(VOID);
INT4 IpMRouteEnableGet(tSnmpIndex *, tRetVal *);
INT4 IpMRouteEntryCountGet(tSnmpIndex *, tRetVal *);
INT4 IpMRouteEnableCmdbGet(tSnmpIndex *, tRetVal *);
INT4 MfwdGlobalTraceGet(tSnmpIndex *, tRetVal *);
INT4 MfwdGlobalDebugGet(tSnmpIndex *, tRetVal *);
INT4 IpMRouteDiscardedPktsGet(tSnmpIndex *, tRetVal *);
INT4 MfwdAvgDataRateGet(tSnmpIndex *, tRetVal *);
INT4 IpMRouteEnableSet(tSnmpIndex *, tRetVal *);
INT4 IpMRouteEnableCmdbSet(tSnmpIndex *, tRetVal *);
INT4 MfwdGlobalTraceSet(tSnmpIndex *, tRetVal *);
INT4 MfwdGlobalDebugSet(tSnmpIndex *, tRetVal *);
INT4 MfwdAvgDataRateSet(tSnmpIndex *, tRetVal *);
INT4 IpMRouteEnableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IpMRouteEnableCmdbTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MfwdGlobalTraceTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MfwdGlobalDebugTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MfwdAvgDataRateTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IpMRouteEnableDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 IpMRouteEnableCmdbDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 MfwdGlobalTraceDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 MfwdGlobalDebugDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 MfwdAvgDataRateDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);





INT4 GetNextIndexIpMRouteTable(tSnmpIndex *, tSnmpIndex *);
INT4 IpMRouteOwnerIdGet(tSnmpIndex *, tRetVal *);
INT4 IpMRouteGroupGet(tSnmpIndex *, tRetVal *);
INT4 IpMRouteSourceGet(tSnmpIndex *, tRetVal *);
INT4 IpMRouteSourceMaskGet(tSnmpIndex *, tRetVal *);
INT4 IpMRouteUpstreamNeighborGet(tSnmpIndex *, tRetVal *);
INT4 IpMRouteInIfIndexGet(tSnmpIndex *, tRetVal *);
INT4 IpMRouteUpTimeGet(tSnmpIndex *, tRetVal *);
INT4 IpMRoutePktsGet(tSnmpIndex *, tRetVal *);
INT4 IpMRouteDifferentInIfPacketsGet(tSnmpIndex *, tRetVal *);
INT4 IpMRouteProtocolGet(tSnmpIndex *, tRetVal *);
INT4 IpMRouteRtAddressGet(tSnmpIndex *, tRetVal *);
INT4 IpMRouteRtMaskGet(tSnmpIndex *, tRetVal *);
INT4 IpMRouteRtTypeGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexIpMRouteNextHopTable(tSnmpIndex *, tSnmpIndex *);
INT4 IpMRouteNextHopOwnerIdGet(tSnmpIndex *, tRetVal *);
INT4 IpMRouteNextHopGroupGet(tSnmpIndex *, tRetVal *);
INT4 IpMRouteNextHopSourceGet(tSnmpIndex *, tRetVal *);
INT4 IpMRouteNextHopSourceMaskGet(tSnmpIndex *, tRetVal *);
INT4 IpMRouteNextHopIfIndexGet(tSnmpIndex *, tRetVal *);
INT4 IpMRouteNextHopAddressGet(tSnmpIndex *, tRetVal *);
INT4 IpMRouteNextHopStateGet(tSnmpIndex *, tRetVal *);
INT4 IpMRouteNextHopUpTimeGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexIpMRouteInterfaceTable(tSnmpIndex *, tSnmpIndex *);
INT4 IpMRouteInterfaceIfIndexGet(tSnmpIndex *, tRetVal *);
INT4 IpMRouteInterfaceOwnerIdGet(tSnmpIndex *, tRetVal *);
INT4 IpMRouteInterfaceTtlGet(tSnmpIndex *, tRetVal *);
INT4 IpMRouteInterfaceProtocolGet(tSnmpIndex *, tRetVal *);
INT4 IpMRouteInterfaceRateLimitGet(tSnmpIndex *, tRetVal *);
INT4 IpMRouteInterfaceInMcastOctetsGet(tSnmpIndex *, tRetVal *);
INT4 IpMRouteInterfaceCmdbPktCntGet(tSnmpIndex *, tRetVal *);
INT4 IpMRouteInterfaceOutMcastOctetsGet(tSnmpIndex *, tRetVal *);
#endif /* _FSMFWDWR_H */
