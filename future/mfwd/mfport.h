/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mfport.h,v 1.14 2012/01/24 14:45:22 siva Exp $
 *
 ********************************************************************/
#ifndef _MFWPORT_H_
#define _MFWPORT_H_


/* Definitions required for Qs */

#define  MFWD_Q_NAME        ((UINT1 *) "MDQ")
#define  MFWD_CQ_DEPTH       30
#define  MFWD_CQ_NAME        ((UINT1 *) "MCQ")
#define  MFWD_DEF_QPROCESS_CNT 1000
#define  MFWD_Q_MODE        OSIX_GLOBAL
#define  MFWD_Q_WAIT_MODE   OSIX_NO_WAIT
#define  MFWD_Q_RCV_TIMEOUT 0


#define  MFWD_TASK_NAME       ((UINT1 *) "MFW")
#define  MFWD_TASK_PRIORITY   58
#define  MFWD_TASK_ST_SIZE    OSIX_DEFAULT_STACK_SIZE
#define  MFWD_TASK_MODE       OSIX_GLOBAL
#define  MFWD_TASK_NODE       SELF



#define MFWD_MEM_DEFAULT_MEMORY_TYPE  MEM_DEFAULT_MEMORY_TYPE

#define  MFWD_MSGQ_EVENT                 0x01
#define  MFWD_STATUS_DISABLE_EVENT       0x02
#define  MFWD_STATUS_ENABLE_EVENT        0x04
#define  MFWD_CMDB_TMR_EXP_EVENT         0x08
#define  MFWD_IP_MDH_ARRIVAL_EVENT       0x10
#define  MFWD_CMDB_ENABLE_EVENT          0x20
#define  MFWD_CMDB_DISABLE_EVENT         0x40

#define  MFWD_EVENT_WAIT_FLAGS           (OSIX_WAIT | OSIX_EV_ANY)
#define  MFWD_EVENT_WAIT_TIMEOUT         0
#define  IP_REG_FAILURE                  FAILURE
#define  IPV6_REG_FAILURE                  FAILURE


/* Structure for the IP header that is returned by EXTRACT_IP_HEADER */
#define  MFWDIPHEADER       t_IP

/* Interface functions mapping provided IP */
#define  MFWD_IP_GET_UCAST_INFO(u4Dest, pu4NextHop, pi4Metrics, pu4Port, pu4Preference, RetCode)\
{\
    tRtInfo RtInfo;\
    RetCode = IpGetAdvancedRouteInfo (u4Dest, &RtInfo, pu4Preference);\
    *pi4Metrics = RtInfo.i4Metric1;\
    *pu4NextHop = (RtInfo.u4NextHop == 0)?u4Dest:RtInfo.u4NextHop;\
    *pu4Port    = RtInfo.u4RtIfIndx;\
}

#define  MFWD_IPV6_GET_UCAST_INFO(Dest, pNextHop, pi4Metrics, pu4Port, pu4Preference, RetCode)\
{\
    tNetIpv6RtInfo RtInfo;\
    UINT1 au1NullAddr[16];\
    tNetIpv6RtInfoQueryMsg QryMsg;\
    MEMCPY(QryMsg.Ip6Dst.u1Addr, Dest.au1Addr, 16);\
    MEMSET(au1NullAddr,0,16);\
    QryMsg.u1PrefixLen = 16;\
    RetCode = NetIpv6GetRoute (&QryMsg, &RtInfo);\
    *pi4Metrics = RtInfo.u4Metric;\
    if (MEMCMP(RtInfo.NextHop, au1NullAddr, 16) == 0)\
      MEMCPY(pNextHop.au1Addr,Dest.au1Addr,16)\
    else\
      MEMCPY(pNextHop.au1Addr,RtInfo.NextHop.u1Addr, 16);\
    *pu4Port    = RtInfo.u4Index;\
}
 
 
#define  REGISTER_MFWD_WITH_IP(CallBkFunction) \
               IpRegHLProtocolForMCastPkts (CallBkFunction)

#define  DEREGISTER_MFWD_FROM_IP(u4IpRegnId) \
               IpDeRegHLProtocolForMCastPkts ((UINT1) u4IpRegnId)

#define  MFWD_GET_PORT_FROM_IF_INDEX(IfIndex, Port) \
                     IpGetPortFromIfIndex(IfIndex, Port);

#define  MFWD_GET_VRID(ownerid) (ownerid)


#ifdef IP6_WANTED
#define  REGISTER_MFWD_WITH_IPV6(CallBkFunction) \
               NetIpv6RegisterHLProtocolForMCastPkts (CallBkFunction)


#define  DEREGISTER_MFWD_FROM_IPV6(u4IpRegnId) \
               NetIpv6DeRegisterHLProtocolForMCastPkts ((UINT1) u4IpRegnId)


#endif



        

#define MFWD_LOAD_MAXMRP_AND_MAXIFACE_VALUES() \
{ \
    tMfwdSystemSize      MfwdSystemSizeVals; \
    GetMfwdSizingParams (&MfwdSystemSizeVals); \
    MAX_MRP_INSTANCES = MfwdSystemSizeVals.u2MfwdMaxMrps; \
    MFWD_MAX_INTERFACES = IPIF_MAX_LOGICAL_IFACES;\
}

#define MFWD_INFORM_MRP(CallBkFn, pBuffer, i4Status)  \
   i4Status = (*CallBkFn) (pBuffer); \
   i4Status = (i4Status == MRP_SUCCESS)?MFWD_SUCCESS:MFWD_FAILURE;
 
#define MFWD_NP_FILL_DSTREAM_IF_INFO(downStreamIf, u4IfIndex, DstreamOif) \
do { \
      downStreamIf.u4IfIndex = u4IfIndex; \
      downStreamIf.u2TtlThreshold = (UINT2)MFWD_DEFAULT_TTL_VALUE; \
      downStreamIf.u4Mtu = MFWD_GET_IF_MTU (DstreamOif); \
} while (0)
#endif
