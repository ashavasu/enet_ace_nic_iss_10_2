
/* SOURCE FILE HEADER :
 *
 *  ---------------------------------------------------------------------------
 * |  FILE NAME             : poecli.c                                         |
 * |                                                                           |
 * |  PRINCIPAL AUTHOR      : Aricent Inc.                              |
 * |                                                                           |
 * |  SUBSYSTEM NAME        : CLI                                              |
 * |                                                                           |
 * |  MODULE NAME           : POE                                              |
 * |                                                                           |
 * |  LANGUAGE              : C                                                |
 * |                                                                           |
 * |  TARGET ENVIRONMENT    :                                                  |
 * |                                                                           |
 * |  DATE OF FIRST RELEASE :                                                  |
 * |                                                                           |
 * |  DESCRIPTION           : Action routines for poe CLI commands.            |
 * |                          This module has functions to  create             |
 * |                          interfaces, modify interface parameters,         |
 * |                          bring up/down the interfaces, display statistics |
 * |                                                                           |
 * |---------------------------------------------------------------------------|
 *
 */
#ifndef __POECLI_C__
#define __POECLI_C__

#ifdef POE_WANTED
#include "poecli.h"
#include "poehdrs.h"
#include "fspoecli.h"
#include "stdpoecli.h"

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : cli_process_poe_cmd                                */
/*                                                                           */
/*     DESCRIPTION      : This function takes in variable no. of arguments   */
/*                        and process the commands for the POE module as     */
/*                        defined in poecmd.h                                */
/*                                                                           */
/*     INPUT            : CliHandle -  CLIHandler                            */
/*                        u4Command -  Command Identifier                    */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/

INT4
cli_process_poe_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT1              *args[CLI_MAX_ARGS];
    INT1                argno = 0;
    INT4                i4IfaceIndex = 0;
    INT4                i4RetStatus = CLI_SUCCESS;
    tMacAddr            PdMac;
    UINT4               u4ErrCode = 0;

    /* If POE is not Started, then all commands are considered as invalid,
     * except enable/disable command
     */
    if ((u4Command != POE_CLI_STATE) && (POE_IS_SHUTDOWN ()))
    {
        CliPrintf (CliHandle, "\r%% Power Over Ethernet is shutdown\r\n");
        return CLI_FAILURE;
    }

    va_start (ap, u4Command);

    /* third argument is always interface name/index */
    i4IfaceIndex = va_arg (ap, INT4);

    if (!(i4IfaceIndex))
    {
        i4IfaceIndex = CLI_GET_IFINDEX ();
    }

    /* Walk through the rest of the arguements and store in args array. 
     * Store two arguments at the max. This is because poe
     * commands do not take more than two inputs from the command line.
     */

    while (1)
    {
        args[argno++] = va_arg (ap, UINT1 *);
        if (argno == 3)
            break;
    }
    va_end (ap);

    CliRegisterLock (CliHandle, PoeLock, PoeUnLock);

    POE_LOCK ();

    switch (u4Command)
    {
        case POE_CLI_STATE:
            /* args[0] - contains the global admin status of PoE */
            i4RetStatus =
                PoeGlobalAdminState (CliHandle, CLI_PTR_TO_I4 (args[0]));
            break;

        case POE_ADD_PD_MAC:
            /* args[0] - contains the mac address string */
            if (OctetLen (args[0]) != POE_MAC_ADDR_SIZE)
            {
                CliPrintf (CliHandle, "\nPD mac address not matching"
                           " aa:aa:aa:aa:aa:aa pattern\n");
                return CLI_FAILURE;
            }
            StrToMac (args[0], PdMac);
            i4RetStatus = PoeConfigMacList (CliHandle, ADD_PD_MAC, PdMac);
            break;

        case POE_DEL_PD_MAC:
            /* args[1] - contains the mac address string */
            if (OctetLen (args[0]) != POE_MAC_ADDR_SIZE)
            {
                CliPrintf (CliHandle, "\nPD mac address not matching"
                           " aa:aa:aa:aa:aa:aa pattern\n");
                return CLI_FAILURE;
            }
            StrToMac (args[0], PdMac);
            i4RetStatus = PoeConfigMacList (CliHandle, DEL_PD_MAC, PdMac);
            break;

        case POE_CLI_INT_STATE:
            /* args[0] - contains the PoE port admin status */
            i4RetStatus = PoePortAdminState (CliHandle, i4IfaceIndex,
                                             CLI_PTR_TO_I4 (args[0]));
            break;

        case POE_PORT_PRIORITY:
            /* args[0] - contains the PoE port priority */
            i4RetStatus = PoeSetPortPriority (CliHandle, i4IfaceIndex,
                                              CLI_PTR_TO_I4 (args[0]));
            break;

        case SHOW_PSE_STATUS:
            ShowPoePseStatus (CliHandle);
            break;

        case SHOW_ALL_POE_INTERFACE:
            ShowAllPoeInterface (CliHandle);
            break;

        case SHOW_POE_INTERFACE:
            ShowPoeInterfaceInfo (CliHandle, i4IfaceIndex);
            break;

        case SHOW_POE_MAC_LIST_INFO:
            ShowPdMacAddrsListInfo (CliHandle);
            break;

        default:
            CliPrintf (CliHandle, "\r\nUnknown command\r\n");
            break;
    }

    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_POE_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%s", PoeCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS (i4RetStatus);

    CliUnRegisterLock (CliHandle);

    POE_UNLOCK ();

    return i4RetStatus;
}

/*****************************************************************************
*     Function Name    : PoeGlobalAdminState
*     Description      : This function enables or disables the PoE global
*                        admin status .
*     Input            : i4PoeStatus 
*     Output           : 
*     Returns          : NONE                                          
*****************************************************************************/
INT4
PoeGlobalAdminState (tCliHandle CliHandle, INT4 i4PoeStatus)
{
    UINT4               u4ErrCode;

    if (nmhTestv2FsPoeGlobalAdminStatus (&u4ErrCode, i4PoeStatus)
        != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsPoeGlobalAdminStatus (i4PoeStatus) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************
*     Function Name    : PoeConfigMacList 
*     Description      : This function adds or deletes a Mac entry 
*                        corresponding to a powered device
*     Input            : macAction - Adds or deletes Powered Device Mac
*                        PdMac - The Mac Address corresponding to a PD 
*     Output           : 
*     Returns          : NONE                                          
*****************************************************************************/
INT4
PoeConfigMacList (tCliHandle CliHandle, UINT4 macAction, tMacAddr PdMac)
{
    UINT4               u4ErrCode;

    switch (macAction)
    {
        case POE_PD_MAC_ADD:

            if (nmhTestv2FsPoePdMacRowStatus
                (&u4ErrCode, PdMac, CREATE_AND_WAIT) != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }
            if (nmhSetFsPoePdMacRowStatus (PdMac, CREATE_AND_WAIT)
                != SNMP_SUCCESS)
            {
                CliPrintf (CliHandle, "\r\nUnable to create PD Entry\r\n");
                return CLI_FAILURE;
            }
            if (nmhTestv2FsPoePdMacRowStatus (&u4ErrCode, PdMac, ACTIVE)
                != SNMP_SUCCESS)
            {
                /*if (u4ErrCode != SNMP_ERR_GEN_ERR)
                   PoeErrDisp (u4ErrCode); */
                return CLI_FAILURE;
            }
            if (nmhSetFsPoePdMacRowStatus (PdMac, ACTIVE) != SNMP_SUCCESS)
            {
                CliPrintf (CliHandle, "\r\nUnable to fill PD Entry\r\n");
                return CLI_FAILURE;
            }
            break;

        case POE_PD_MAC_DEL:
            if (nmhTestv2FsPoePdMacRowStatus (&u4ErrCode, PdMac, DESTROY)
                != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }
            if (nmhSetFsPoePdMacRowStatus (PdMac, DESTROY) != SNMP_SUCCESS)
            {
                CliPrintf (CliHandle, "\r\nUnable to delete PD Entry\r\n");
                return CLI_FAILURE;
            }
            break;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************
*     Function Name    : PoePortAdminState
*     Description      : This function enables or disables the PoE port 
*                        admin status .
*     Input            : PortIndex - port whose poe admin status is to be 
*                                    enabled or disabled
*                        PortAdminStatus - Admin Status of the port to be set  
*     Output           : 
*     Returns          : NONE                                          
*****************************************************************************/
INT4
PoePortAdminState (tCliHandle CliHandle, INT4 PortIndex, INT4 PortAdminStatus)
{
    UINT4               u4ErrCode;
    INT4                i4GrpIndex = 1;

    /* Since only one group is supported group index is always 1 */
    if (nmhTestv2PethPsePortAdminEnable (&u4ErrCode, i4GrpIndex, PortIndex,
                                         PortAdminStatus) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetPethPsePortAdminEnable (i4GrpIndex, PortIndex, PortAdminStatus)
        != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************
*     Function Name    : PoeSetPortPriority 
*     Description      : This function sets the port priority of the port.
*     Input            : PortIndex - port whose poe priority is to be set
*                        PortPriority - Port priority  can take the 
*                        following 3 values :-
*                        critical - 1
*                        high - 1
*                        low - 2
*     Output           : 
*     Returns          : NONE                                          
*****************************************************************************/
INT4
PoeSetPortPriority (tCliHandle CliHandle, INT4 PortIndex, INT4 PortPriority)
{
    UINT4               u4ErrCode;
    INT4                i4GrpIndex = 1;

    /* Since only one group is supported group index is always 1 */
    if (nmhTestv2PethPsePortPowerPriority (&u4ErrCode, i4GrpIndex, PortIndex,
                                           PortPriority) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetPethPsePortPowerPriority (i4GrpIndex, PortIndex, PortPriority)
        != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************
*     Function Name    : ShowPoePseStatus 
*     Description      : This function shows Poe Pse Status 
*     Input            : CliHandle - CLI Handler 
*     Output           : Displays the show output for PoE Pse Status
*     Returns          : None
*****************************************************************************/
INT4
ShowPoePseStatus (tCliHandle CliHandle)
{
    INT4                i4RetVal = 0;
    INT4                i4GrpIndex = 1;

    CliPrintf (CliHandle, "\r\nPSE Status \r\n");

    CliPrintf (CliHandle, "---------- \r\n");

    /*PoE Global Admin Status */
    nmhGetFsPoeGlobalAdminStatus (&i4RetVal);

    if (i4RetVal == 1)
    {
        CliPrintf (CliHandle, "PoE Global Admin State : Enabled \r\n");
    }
    else if (i4RetVal == 2)
    {
        CliPrintf (CliHandle, "PoE Global Admin State : Disabled \r\n");
    }
    else
    {
        CliPrintf (CliHandle, "PoE Global Admin State : Unknown \r\n");
    }

    i4RetVal = 0;

    /*PSE Operational Status */
    /*Group Index is always set to 1 since it is not supported currently */
    nmhGetPethMainPseOperStatus (i4GrpIndex, &i4RetVal);

    if (i4RetVal == 1)
    {
        CliPrintf (CliHandle, "PSE Oper Status        : On \r\n");
    }
    else if (i4RetVal == 2)
    {
        CliPrintf (CliHandle, "PSE Oper Status        : Off \r\n");
    }
    else if (i4RetVal == 3)
    {
        CliPrintf (CliHandle, "PSE Oper Status        : Faulty \r\n");
    }
    else
    {
        CliPrintf (CliHandle, "PSE Oper Status        : Unknown \r\n");
    }

    i4RetVal = 0;

    /*Total number of power supply modules */
    CliPrintf (CliHandle, "Max Power Supply       : %d \r\n",
               gPoeInfo.u2MaxPwrSupplyMod);
    i4RetVal = 0;

    /*Total power in the system */
    /*Group Index is always set to 1 since it is not supported currently */
    if (nmhGetPethMainPsePower (i4GrpIndex, (UINT4 *) &i4RetVal) ==
        SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "Total Power in (watts) : %d \r\n", i4RetVal);
    }
    else
    {
        CliPrintf (CliHandle, "Total Power in (watts) : Unknown \r\n");
    }

    /*Total power consumed */
    /*Group Index is always set to 1 since it is not supported currently */
    if (nmhGetPethMainPseConsumptionPower (i4GrpIndex, (UINT4 *) &i4RetVal)
        == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "Total Power Consumed   : %d \r\n", i4RetVal);
    }
    else
    {
        CliPrintf (CliHandle, "Total Power Consumed   : Unknown \r\n");
    }

    CliPrintf (CliHandle, "\r\n");
    return CLI_SUCCESS;
}

/*****************************************************************************
*     Function Name    : ShowAllPoeInterface 
*     Description      : This function shows Poe Port Information for all ports
*     Input            : CliHandle - CLI Handler 
*     Output           : Displays the show output for all PoE Ports
*     Returns          : None                                          
*****************************************************************************/
INT4
ShowAllPoeInterface (tCliHandle CliHandle)
{
    INT4                i4RetVal = 0;
    INT4                i4Class;
    INT4                i4OutCome;
    UINT4               u4Count = 0;
    UINT1               u1isShowAll = TRUE;
    INT4                i4NextPort = 0;
    INT4                i4CurrentPort = 0;
    INT4                i4CurrGrp = 1;
    INT4                i4GrpIndex = 1;

    if (POE_IS_SHUTDOWN ())
    {
        CliPrintf (CliHandle, "\r\nPoE module not Up! \r\n");
        return CLI_FAILURE;
    }

    i4OutCome = (INT4) (nmhGetFirstIndexPethPsePortTable (&i4GrpIndex,
                                                          &i4NextPort));

    if (i4OutCome == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    CliPrintf (CliHandle, "\r\nPoE Port Info\r\n");

    CliPrintf (CliHandle, "-------------\r\n");

    CliPrintf (CliHandle,
               "\r\nPort-Index  PoeAdminState  DetectionStatus  powerClass  priority \r\n");

    CliPrintf (CliHandle,
               "----------  -------------  ---------------  ----------  -------- \r\n");

    do
    {
        u4Count++;

        /* Port Number */
        CliPrintf (CliHandle, "%-12d", i4NextPort);

        /* Port Detection Status */
        /*Group Index is always set to 1 since it is not supported currently */
        nmhGetPethPsePortAdminEnable (i4GrpIndex, i4NextPort, &i4RetVal);
        switch (i4RetVal)
        {
            case POE_TRUE:
                CliPrintf (CliHandle, "up             ");
                break;

            case POE_FALSE:
                CliPrintf (CliHandle, "down           ");
                break;

            default:
                CliPrintf (CliHandle, "Unknown        ");
                break;

        }

        /* Port Detection Status */
        /*Group Index is always set to 1 since it is not supported currently */
        nmhGetPethPsePortDetectionStatus (i4GrpIndex, i4NextPort, &i4RetVal);
        switch (i4RetVal)
        {
            case POE_DISABLED:
                CliPrintf (CliHandle, "Disabled         ");
                break;

            case POE_SEARCHING:
                CliPrintf (CliHandle, "Searching        ");
                break;

            case POE_DELIVERING_POWER:
                CliPrintf (CliHandle, "Delivering Power ");
                break;

            case POE_FAULT:
                CliPrintf (CliHandle, "Fault            ");
                break;

            case POE_TEST:
                CliPrintf (CliHandle, "Test             ");
                break;

            case POE_OTHERFAULT:
                CliPrintf (CliHandle, "OtherFault       ");
                break;

            default:
                CliPrintf (CliHandle, "Unknown          ");
                break;
        }

        /* Port Power Class */
        /*Group Index is always set to 1 since it is not supported currently */
        nmhGetPethPsePortPowerClassifications (i4GrpIndex, i4NextPort,
                                               &i4RetVal);
        i4Class = i4RetVal - 1;
        CliPrintf (CliHandle, "class %d     ", i4Class);

        /* Port Power priority */
        /*Group Index is always set to 1 since it is not supported currently */
        nmhGetPethPsePortPowerPriority (i4GrpIndex, i4NextPort, &i4RetVal);
        switch (i4RetVal)
        {
            case CRITICAL:
                CliPrintf (CliHandle, "critical         \r\n");
                break;

            case HIGH:
                CliPrintf (CliHandle, "high             \r\n");
                break;

            case LOW:
                CliPrintf (CliHandle, "low              \r\n");
                break;

            default:
                CliPrintf (CliHandle, "Unknown          \r\n");
                break;
        }

        i4CurrentPort = i4NextPort;

        i4OutCome = (INT4) (nmhGetNextIndexPethPsePortTable
                            (i4CurrGrp, &i4GrpIndex, i4CurrentPort,
                             &i4NextPort));

        if (i4OutCome == SNMP_FAILURE)
        {
            u1isShowAll = FALSE;
        }
    }
    while (u1isShowAll);

    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/*****************************************************************************
*     Function Name    : ShowPoeInterfaceInfo 
*     Description      : This function shows Poe Port Information for a given
*                        port
*     Input            : CliHandle - CLI Handler, i4Port - Interface Index
*     Output           : Displays the show output for a PoE Port
*     Returns          : None                                          
*****************************************************************************/
INT4
ShowPoeInterfaceInfo (tCliHandle CliHandle, INT4 i4Port)
{
    UINT4               i4RetVal = 0;
    INT4                i4GrpIndex = 1;
    INT4                i4Class;

    if (POE_IS_SHUTDOWN ())
    {
        CliPrintf (CliHandle, "\r\nPoE module not Up! \r\n");
        return CLI_FAILURE;
    }

    CliPrintf (CliHandle, "\r\nPoE Port Info\r\n");

    CliPrintf (CliHandle, "-------------\r\n");

    /* Port Number */
    CliPrintf (CliHandle, "Port Number       : %d\r\n", i4Port);

    /* Port Detection Status */
    /*Group Index is always set to 1 since it is not supported currently */
    nmhGetPethPsePortAdminEnable (i4GrpIndex, i4Port, (INT4 *) &i4RetVal);
    switch (i4RetVal)
    {
        case POE_TRUE:
            CliPrintf (CliHandle, "PoeAdminStatus    : Up\r\n");
            break;

        case POE_FALSE:
            CliPrintf (CliHandle, "PoeAdminStatus    : Down\r\n");
            break;

        default:
            CliPrintf (CliHandle, "PoeAdminStatus    : Unknown\r\n");
            break;
    }

    /* Port Detection Status */
    /*Group Index is always set to 1 since it is not supported currently */
    nmhGetPethPsePortDetectionStatus (i4GrpIndex, i4Port, (INT4 *) &i4RetVal);
    switch (i4RetVal)
    {
        case POE_DISABLED:
            CliPrintf (CliHandle, "PoeDetectionState : Disabled\r\n");
            break;

        case POE_SEARCHING:
            CliPrintf (CliHandle, "PoeDetectionState : Searching\r\n");
            break;

        case POE_DELIVERING_POWER:
            CliPrintf (CliHandle, "PoeDetectionState : Delivering Power\r\n");
            break;

        case POE_FAULT:
            CliPrintf (CliHandle, "PoeDetectionState : Fault\r\n");
            break;

        case POE_TEST:
            CliPrintf (CliHandle, "PoeDetectionState : Test\r\n");
            break;

        case POE_OTHERFAULT:
            CliPrintf (CliHandle, "PoeDetectionState : Other Fault\r\n");
            break;

        default:
            CliPrintf (CliHandle, "PoeDetectionState : Unknown\r\n");
            break;
    }

    /* Port Power Class */
    /*Group Index is always set to 1 since it is not supported currently */
    nmhGetPethPsePortPowerClassifications (i4GrpIndex, i4Port,
                                           (INT4 *) &i4RetVal);
    i4Class = i4RetVal - 1;
    CliPrintf (CliHandle, "class             : %d    \r\n", i4Class);

    /* Port Power priority */
    /*Group Index is always set to 1 since it is not supported currently */
    nmhGetPethPsePortPowerPriority (i4GrpIndex, i4Port, (INT4 *) &i4RetVal);
    switch (i4RetVal)
    {
        case CRITICAL:
            CliPrintf (CliHandle, "Priority          : critical\r\n");
            break;

        case HIGH:
            CliPrintf (CliHandle, "Priority          : high\r\n");
            break;

        case LOW:
            CliPrintf (CliHandle, "Priority          : low\r\n");
            break;

        default:
            CliPrintf (CliHandle, "Priority          : unknown\r\n");
            break;
    }

    CliPrintf (CliHandle, "\r\n");
    return CLI_SUCCESS;
}

/*****************************************************************************
*     Function Name    : ShowPdMacAddrsListInfo 
*     Description      : This function shows Poe Powered Device Mac 
*                        Information
*     Input            : CliHandle - CLI Handler
*     Output           : Displays the show output for all PoE Ports
*     Returns          : None                                          
*****************************************************************************/
INT4
ShowPdMacAddrsListInfo (tCliHandle CliHandle)
{
    tMacAddr            CurrentAddr = "";
    tMacAddr            NextAddr = "";
    UINT1               au1String[21];
    INT4                i4OutCome;
    INT4                i4RetVal = 0;
    UINT1               u1isShowAll = TRUE;
    UINT4               u4Count = 0;
    UINT4               i4Port = 0;
    INT4                i4GrpIndex = 1;
    INT4                i4Class;

    if (POE_IS_SHUTDOWN ())
    {
        CliPrintf (CliHandle, "\r\nPoE module not Up! \r\n");
        return CLI_FAILURE;
    }

    i4OutCome = (INT4) nmhGetFirstIndexFsPoeMacTable ((tMacAddr *) & NextAddr);

    if (i4OutCome == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\nNo PD mac Entry! \r\n");
        return CLI_FAILURE;
    }

    CliPrintf (CliHandle, "\r\nPD Mac Entries\r\n");

    CliPrintf (CliHandle, "-------------- \r\n");

    CliPrintf (CliHandle,
               "PD mac-address       Port     powerConsumed   DetectionState\r\n");

    CliPrintf (CliHandle,
               "--------------       ----     -------------   -------------- \r\n");

    do
    {
        u4Count++;

        PrintMacAddress (NextAddr, au1String);
        CliPrintf (CliHandle, "%-21s", au1String);

        nmhGetFsPoePdMacPort (NextAddr, (INT4 *) &i4Port);

        CliPrintf (CliHandle, "%-9d", i4Port);

        /* Port Power Class */
        /*Group Index is always set to 1 since it is not supported currently */
        if (nmhGetPethPsePortPowerClassifications (i4GrpIndex, i4Port,
                                                   &i4RetVal) != SNMP_SUCCESS)
            CliPrintf (CliHandle, "Unknown         ");
        else
        {
            i4Class = i4RetVal - 1;
            CliPrintf (CliHandle, "class %d         ", i4Class);
        }

        /* Port Detection Status */
        /*Group Index is always set to 1 since it is not supported currently */
        nmhGetPethPsePortDetectionStatus (i4GrpIndex, i4Port, &i4RetVal);
        switch (i4RetVal)
        {
            case POE_DISABLED:
                CliPrintf (CliHandle, "Disabled         \r\n");
                break;

            case POE_SEARCHING:
                CliPrintf (CliHandle, "Searching        \r\n");
                break;

            case POE_DELIVERING_POWER:
                CliPrintf (CliHandle, "Delivering Power \r\n");
                break;

            case POE_FAULT:
                CliPrintf (CliHandle, "Fault            \r\n");
                break;

            case POE_TEST:
                CliPrintf (CliHandle, "Test             \r\n");
                break;

            case POE_OTHERFAULT:
                CliPrintf (CliHandle, "OtherFault       \r\n");
                break;

            default:
                CliPrintf (CliHandle, "Unknown          \r\n");
                break;
        }

        MEMCPY (CurrentAddr, NextAddr, sizeof (tMacAddr));

        i4OutCome =
            (INT4) (nmhGetNextIndexFsPoeMacTable (CurrentAddr, &NextAddr));

        if (i4OutCome == SNMP_FAILURE)
        {
            u1isShowAll = FALSE;
        }
    }
    while (u1isShowAll);
    CliPrintf (CliHandle, "\r\n");
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PoeShowRunningConfig                                 */
/*                                                                           */
/* Description        : Displays configurations done in POE module           */
/*                                                                           */
/* Input(s)           : CliHandle     -  CLI context                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS or CLI_FAILURE                           */
/*****************************************************************************/
INT4
PoeShowRunningConfig (tCliHandle CliHandle, UINT4 u4Module)
{
    if (PoeShowRunningConfigScalar (CliHandle) == CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }
    PoeShowRunningConfigTable (CliHandle);
    if (u4Module == ISS_POE_SHOW_RUNNING_CONFIG)
    {
        PoeShowRunningConfigInterface (CliHandle);
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : PoeShowRunningConfigScalar                           */
/*                                                                           */
/* Description        : Displays current configurations done in POE scalar   */
/*                        objects                                            */
/*                                                                           */
/* Input(s)           : CliHandle     -  CLI context                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS                                          */
/*****************************************************************************/
INT4
PoeShowRunningConfigScalar (tCliHandle CliHandle)
{
    INT4                i4PoeGlobalAdminStatus = 0;
    if (nmhGetFsPoeGlobalAdminStatus (&i4PoeGlobalAdminStatus) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (i4PoeGlobalAdminStatus == POE_START)
    {
        CliPrintf (CliHandle, "set poe enable\r\n");
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PoeShowRunningConfigTable                            */
/*                                                                           */
/* Description        : Displays current configurations done in POE table    */
/*                        objects                                            */
/*                                                                           */
/* Input(s)           : CliHandle     -  CLI context                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS                                          */
/*****************************************************************************/
INT4
PoeShowRunningConfigTable (tCliHandle CliHandle)
{
    tMacAddr            NextAddr = "";
    tMacAddr            CurrentAddr = "";
    UINT1               au1String[21];

    if (!(POE_IS_SHUTDOWN ()))
    {
        if (nmhGetFirstIndexFsPoeMacTable ((tMacAddr *) & NextAddr)
            != SNMP_FAILURE)
        {
            do
            {
                PrintMacAddress (NextAddr, au1String);
                CliPrintf (CliHandle, "power inline mac-address ");
                CliPrintf (CliHandle, "%s\r\n", au1String);
                MEMCPY (CurrentAddr, NextAddr, sizeof (tMacAddr));

            }
            while ((nmhGetNextIndexFsPoeMacTable (CurrentAddr, &NextAddr))
                   != SNMP_FAILURE);
        }
    }

    return CLI_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : PoeShowRunningConfigInterface                        */
/*                                                                           */
/* Description        : Displays current configurations in POE for           */
/*                      specified  interface                                 */
/*                                                                           */
/* Input(s)           : CliHandle     -  CLI context                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS or CLI_FAILURE                           */
/*****************************************************************************/
INT4
PoeShowRunningConfigInterface (tCliHandle CliHandle)
{

    INT4                i4NextGrpIndex = 1;
    INT4                i4CurrGrpIndex = 1;
    INT4                i4NextPort = 0;
    INT4                i4CurrentPort = 0;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];

    if (!(POE_IS_SHUTDOWN ()))
    {
        if ((nmhGetFirstIndexPethPsePortTable (&i4NextGrpIndex, &i4NextPort))
            != SNMP_FAILURE)
        {
            do
            {
                if (i4NextPort == i4CurrentPort)
                {
                    continue;
                }
                MEMSET (&au1IfName[0], 0, CFA_MAX_PORT_NAME_LENGTH);
                CfaCliConfGetIfName ((UINT4) i4NextPort,
                                     (INT1 *) &au1IfName[0]);
                CliPrintf (CliHandle, "interface %s\r\n", au1IfName);

                PoeShowRunningConfigInterfaceDetails (CliHandle, i4NextPort);

                CliPrintf (CliHandle, "!\r\n");

                i4CurrGrpIndex = i4NextGrpIndex;
                i4CurrentPort = i4NextPort;
            }
            while ((nmhGetNextIndexPethPsePortTable (i4CurrGrpIndex,
                                                     &i4NextGrpIndex,
                                                     i4CurrentPort,
                                                     &i4NextPort)) !=
                   SNMP_FAILURE);
        }
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PoeShowRunningConfigInterfaceDetails                 */
/*                                                                           */
/* Description        : This function displays the currently operating       */
/*                      POE configurations for a particular interface.       */
/*                                                                           */
/* Input(s)           : CliHandle, i4IfIndex                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS or CLI_FAILURE                           */
/*****************************************************************************/
INT4
PoeShowRunningConfigInterfaceDetails (tCliHandle CliHandle, INT4 i4IfIndex)
{
    INT4                i4NextGrpIndex = 1;
    INT4                i4CurrGrpIndex = 1;
    INT4                i4NextPort = 0;
    INT4                i4CurrentPort = 0;
    INT4                i4RetVal = 0;

    if (!(POE_IS_SHUTDOWN ()))
    {
        if ((nmhGetFirstIndexPethPsePortTable (&i4NextGrpIndex, &i4NextPort))
            != SNMP_FAILURE)
        {
            do
            {
                if (i4NextPort != i4IfIndex)
                {
                    i4CurrGrpIndex = i4NextGrpIndex;
                    i4CurrentPort = i4NextPort;
                    continue;
                }

                nmhGetPethPsePortAdminEnable (i4NextGrpIndex, i4NextPort,
                                              &i4RetVal);
                if (i4RetVal == POE_TRUE)
                {
                    CliPrintf (CliHandle, "power inline auto\r\n");
                }

                nmhGetPethPsePortPowerPriority (i4NextGrpIndex, i4NextPort,
                                                &i4RetVal);
                if (i4RetVal == CRITICAL)
                {
                    CliPrintf (CliHandle, "power inline priority critical\r\n");
                }
                if (i4RetVal == HIGH)
                {
                    CliPrintf (CliHandle, "power inline priority high\r\n");
                }

                i4CurrGrpIndex = i4NextGrpIndex;
                i4CurrentPort = i4NextPort;
            }
            while ((nmhGetNextIndexPethPsePortTable (i4CurrGrpIndex,
                                                     &i4NextGrpIndex,
                                                     i4CurrentPort,
                                                     &i4NextPort)) !=
                   SNMP_FAILURE);
        }
    }
    return CLI_SUCCESS;

}
#endif /* POE_WANTED */
#endif /* __POECLI_C__ */
