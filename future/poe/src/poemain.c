/* $Id: poemain.c,v 1.13 2014/04/11 09:17:19 siva Exp $ */
/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2001-2002                                          */
/*****************************************************************************/
/*    FILE  NAME            : poemain.c                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : POE  module                                    */
/*    MODULE NAME           : POE                                            */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 27 Jan 2005                                    */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           :                                                */
/*---------------------------------------------------------------------------*/
#ifndef _POEMAIN_C
#define _POEMAIN_C
#include "poehdrs.h"
#include "poeglob.h"

#ifdef NPAPI_WANTED
#include "poenp.h"
#endif

/*****************************************************************************/
/* Function Name      : PoeSnmpLowCreatePdMacNode                            */
/*                                                                           */
/* Description        : This function Adds a Valid PD MAC adress in Mac      */
/*                      hash table                                           */
/*                                                                           */
/* Input(s)           : macAddr - MAC address                                */
/*                                                                           */
/* Output(s)          : ppPdMacNode - Pointer to the pointer to the nod e in */
/*                      PD Mac table                                         */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : POE_SUCCESS or POE_FAILURE                           */
/*****************************************************************************/

INT4
PoeSnmpLowCreatePdMacNode (tMacAddr macAddr, tPoeMacEntry ** ppPdMacNode)
{
    UINT4               u4HashIndex = POE_INIT_VAL;
    UINT4               u4FdbId = 0;
    tPoeMacEntry       *pPoeMacEntry = NULL;

    UINT2               u2Port = 0;
    UINT1               u1EntryType = 0;

    if ((POE_MACENTRY_ALLOC_MEM_BLOCK (pPoeMacEntry, tPoeMacEntry)) == NULL)
    {
        POE_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                 " Create: Allocate PD mac table node failed \n");
        return POE_FAILURE;
    }
    MEMSET (pPoeMacEntry, 0, sizeof (tPoeMacEntry));

    /*Adding the new Mac entry into the PD mac hash table */

    /* The port on which the Mac is learnt can be got only from
     * the GF hardware.Initializing it to 0 otherwise*/
    pPoeMacEntry->u4PsePortIndex = POE_INIT_VAL;
    MEMCPY (pPoeMacEntry->pdMacAddress, macAddr, POE_MAC_ADDR_SIZE);

    if (VlanGetFdbEntryDetails (u4FdbId, macAddr, &u2Port, &u1EntryType)
        == VLAN_FAILURE)
    {
        /* FDB entry get failure */
        POE_MACENTRY_FREE_MEM_BLOCK (pPoeMacEntry);
        return POE_FAILURE;
    }
    if (u1EntryType == POE_STATIC_MAC_ENTRY)
    {
        /* PD Mac is Static !! Mac will be ignored */
        POE_MACENTRY_FREE_MEM_BLOCK (pPoeMacEntry);
        return POE_SUCCESS;
    }
    pPoeMacEntry->u4PsePortIndex = u2Port;

    PoeCalcPdMacTblIndex (macAddr, &u4HashIndex);
    POE_HASH_ADD_NODE (POE_MACENTRY_TABLE, pPoeMacEntry, u4HashIndex, NULL);
    *ppPdMacNode = pPoeMacEntry;
    return POE_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PoeGetFirstValidPdMacAddress                         */
/*                                                                           */
/* Description        : This function gets the first Valid PD MAC            */
/*                      address in Mac hash table                            */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : macAddr - MAC address                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : POE_SUCCESS or POE_FAILURE                           */
/*****************************************************************************/

INT4
PoeGetFirstValidPdMacAddress (tMacAddr macAddr)
{
    UINT4               u4Index = POE_INIT_VAL;

    tPoeMacEntry       *pNode = NULL;
    POE_HASH_SCAN_TBL (POE_PD_MAC_ADDRS_TABLE, u4Index)
    {
        pNode = (tPoeMacEntry *) POE_HASH_GET_FIRST_BUCKET_NODE
            (POE_PD_MAC_ADDRS_TABLE, u4Index);
        while (pNode != NULL)
        {
            MEMCPY (macAddr, pNode->pdMacAddress, POE_MAC_ADDR_SIZE);
            /*printf ("\nHashIndex is %d\n", u4Index);  */
            return POE_SUCCESS;
        }
    }
    return POE_FAILURE;
}

/*****************************************************************************/
/* Function Name      : PoeCalcPdMacTblIndex                                 */
/*                                                                           */
/* Description        : This function calculates PD mac  Hash table index    */
/*                                                                           */
/* Input(s)           : pAddr - MAC address to be searched                   */
/*                                                                           */
/* Output(s)          : pu4HashIdx - Pointer to Hash index                   */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
PoeCalcPdMacTblIndex (tMacAddr macAddr, UINT4 *pu4HashIdx)
{
    UINT4               u2MacAddr = 0;
    /*  Computation of Hash Index can be modified here */

    POE_GET_TWOBYTE (u2MacAddr, &(macAddr[4]));
    *pu4HashIdx = (UINT4) (u2MacAddr % POE_MAC_TABLE_SIZE);
    return;
}

/*****************************************************************************/
/* Function Name      : PoeGetNextValidPdMacAddress                          */
/*                                                                           */
/* Description        : This function gets the next valid PD's MAC           */
/*                      address occurring to a reference MAC address         */
/*                                                                           */
/* Input(s)           : refMacAddr - reference PD address                    */
/*                                                                           */
/* Output(s)          : nextMacAddr - next PD's MAC address                  */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : POE_SUCCESS or POE_FAILURE                         */
/*****************************************************************************/

INT4
PoeGetNextValidPdMacAddress (tMacAddr refMacAddr, tMacAddr nextMacAddr)
{
    tPoeMacEntry       *pNode = NULL;
    UINT4               u4Index = POE_INIT_VAL;

    PoeCalcPdMacTblIndex (refMacAddr, &u4Index);
    pNode = (tPoeMacEntry *) POE_HASH_GET_FIRST_BUCKET_NODE
        (POE_PD_MAC_ADDRS_TABLE, u4Index);
    while (pNode != NULL)
    {
        if (POE_COMPARE_MAC_ADDR (pNode->pdMacAddress, refMacAddr))
        {
            /* Reference Node is found now - So now get the next node */
            pNode = (tPoeMacEntry *) POE_HASH_GET_NEXT_BUCKET_NODE
                (POE_PD_MAC_ADDRS_TABLE, u4Index, (tPoeHashNode *) pNode);
            while (pNode == NULL)
            {
                if (++u4Index >= POE_MAC_TABLE_SIZE)
                {
                    return POE_FAILURE;
                }
                /* Since the next node is null get the first node
                 * in the next bucket 
                 */
                pNode = (tPoeMacEntry *)
                    POE_HASH_GET_FIRST_BUCKET_NODE (POE_PD_MAC_ADDRS_TABLE,
                                                    u4Index);
            }                    /* While pNode == NULL */
            MEMCPY (nextMacAddr, pNode->pdMacAddress, POE_MAC_ADDR_SIZE);
            return POE_SUCCESS;
        }                        /* end if */
        pNode = (tPoeMacEntry *) POE_HASH_GET_NEXT_BUCKET_NODE
            (POE_PD_MAC_ADDRS_TABLE, u4Index, (tPoeHashNode *) pNode);
    }
    POE_TRC (CONTROL_PLANE_TRC, " Unable to Find Next PD Node ");
    return POE_FAILURE;
}

/*****************************************************************************/
/* Function Name      : PoeGetPdMacTblEntryByMac                             */
/*                                                                           */
/* Description        : This function gets Pd Mac table entry by MAC addr    */
/*                                                                           */
/* Input(s)           : macAddr - MAC address                                */
/*                                                                           */
/* Output(s)          : ppPdMacNode    - Pointer to the Pointer to Pd Mac    */
/*                                 table entry to be got                     */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : POE_SUCCESS / POE_FAILURE                            */
/*****************************************************************************/

INT4
PoeGetPdMacTblEntryByMac (tMacAddr macAddr, tPoeMacEntry ** ppPdMacNode)
{
    tPoeMacEntry       *pNode = NULL;
    UINT4               u4Index = POE_INIT_VAL;

    PoeCalcPdMacTblIndex (macAddr, &u4Index);
    pNode = (tPoeMacEntry *) POE_HASH_GET_FIRST_BUCKET_NODE
        (POE_PD_MAC_ADDRS_TABLE, u4Index);
    while (pNode != NULL)
    {
        if (POE_COMPARE_MAC_ADDR (pNode->pdMacAddress, macAddr))
        {
            *ppPdMacNode = pNode;
            return POE_SUCCESS;
        }
        pNode = (tPoeMacEntry *) POE_HASH_GET_NEXT_BUCKET_NODE
            (POE_PD_MAC_ADDRS_TABLE, u4Index, (tPoeHashNode *) pNode);
    }

    return POE_FAILURE;
}

/*****************************************************************************/
/* Function Name      : PoeGetPowerFromClass                                 */
/*                                                                           */
/* Description        : This function gives the power equivalent of the class*/
/*                                                                           */
/*                                                                           */
/* Input(s)           : u2powerClass - class of the Powered device           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : POE_SUCCESS / POE_FAILURE                            */
/*****************************************************************************/
INT4
PoeGetPowerFromClass (UINT2 u2PowerClass, UINT2 *u2PowerReq)
{
    switch (u2PowerClass)
    {
        case CLASS_0:
            *u2PowerReq = CLASS_0_POWER;
            break;
        case CLASS_1:
            *u2PowerReq = CLASS_1_POWER;
            break;
        case CLASS_2:
            *u2PowerReq = CLASS_2_POWER;
            break;
        case CLASS_3:
            *u2PowerReq = CLASS_3_POWER;
            break;
        case CLASS_4:
            *u2PowerReq = CLASS_4_POWER;
            break;
        default:
            POE_TRC (CONTROL_PLANE_TRC, "unknown PD class ...");
            return POE_FAILURE;
            break;
    }
    return POE_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PoeGetPowerAvailableStatus                           */
/*                                                                           */
/* Description        : This function gives the power availability status    */
/*                      given the powered device classification information. */
/*                                                                           */
/*                                                                           */
/* Input(s)           : u2powerClass - class of the Powered device           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPoeInfo.u4CurrPsePower                              */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : POE_SUCCESS / POE_FAILURE                            */
/*****************************************************************************/
INT4
PoeGetPowerAvailableStatus (UINT2 u2PowerClass)
{
    UINT2               u2PowerReq;

    /* Getting the actual power required from the class */
    if (PoeGetPowerFromClass (u2PowerClass, &u2PowerReq) == POE_SUCCESS)
    {
        /* Checking if enough power is available for applying the same on the PI */
        if (gPoeInfo.u2CurrPsePower >= u2PowerReq)
        {
            return POE_SUCCESS;
        }
    }
    return POE_FAILURE;
}

/*****************************************************************************/
/* Function Name      : PoeGetTotalPowerAvailable                            */
/*                                                                           */
/* Description        : This function gives the total power available with   */
/*                      the system based on the chassis                      */
/*                                                                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPoeInfo.u4CurrPsePower                              */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : POE_SUCCESS / POE_FAILURE                            */
/*****************************************************************************/
INT4
PoeGetTotalPowerAvailable ()
{
    tPoeMainPseEntry   *pPseEntry;
    UINT2               u2PwrConsumption;
    UINT2               u2PwrAvailable;
    UINT2               u2PseIndex = 1;

    u2PwrConsumption = (gPoeInfo.u2MaxDnLnkMod *
                        PWR_CONSUM_DOWN_LINK_MODULE) +
        (gPoeInfo.u2MaxUpLnkMod * PWR_CONSUM_UP_LINK_MODULE) +
        (gPoeInfo.u2MaxSuprvisrMod *
         PWR_CONSUM_SUPRVISR_MODULE) +
        (gPoeInfo.u2MaxFanMod * PWR_CONSUM_FAN_MODULE);

    u2PwrAvailable = (gPoeInfo.u2MaxPwrSupplyMod *
                      PWR_AVAIL_PWR_SUPPLY_MODULE) - u2PwrConsumption;

    /* Currently only one group is supported */
    if (PoeGetMainPseEntry (u2PseIndex, &pPseEntry) != POE_SUCCESS)
    {
        POE_TRC (INIT_SHUT_TRC, "Pse Entry could not be obtained\n");
        return POE_FAILURE;
    }
    pPseEntry->u4MainPsePower = (UINT4) u2PwrAvailable;
    /* Setting the current available power */
    gPoeInfo.u2CurrPsePower = u2PwrAvailable;
    return POE_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PoeUpdatePowerAvailable                              */
/*                                                                           */
/* Description        : This function updates the total power available with */
/*                      the system                                           */
/*                                                                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPoeInfo.u4CurrPsePower                              */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : POE_SUCCESS / POE_FAILURE                            */
/*****************************************************************************/
INT4
PoeUpdatePowerAvailable (UINT2 u2PowerClass, UINT2 u2PowerUpdate)
{
    tPoeMainPseEntry   *pPseEntry;
    UINT2               u2PowerReq;
    UINT2               u2PseIndex = 1;

    /* Getting the actual power required from the class */
    if (PoeGetPowerFromClass (u2PowerClass, &u2PowerReq) == POE_SUCCESS)
    {
        /* updating current power available */
        switch (u2PowerUpdate)
        {
            case POE_SUBTRACT:
                gPoeInfo.u2CurrPsePower = gPoeInfo.u2CurrPsePower - u2PowerReq;
                break;

            case POE_ADD:
                gPoeInfo.u2CurrPsePower = gPoeInfo.u2CurrPsePower + u2PowerReq;
                break;

            default:
                POE_TRC (CONTROL_PLANE_TRC, "unknown update type ...");
                return POE_FAILURE;
                break;
        }
        /* Currently only one group is supported */
        if (PoeGetMainPseEntry (u2PseIndex, &pPseEntry) != POE_SUCCESS)
        {
            POE_TRC (INIT_SHUT_TRC, "Pse Entry could not be obtained\n");
            return POE_FAILURE;
        }
        pPseEntry->u4MainPseConsumptionPower =
            pPseEntry->u4MainPsePower - gPoeInfo.u2CurrPsePower;
        return POE_SUCCESS;
    }
    return POE_FAILURE;
}

/*****************************************************************************/
/* Function Name      : PoeGetMainPseEntry                                   */
/*                                                                           */
/* Description        : This function returns the pointer to the Power Supply*/
/*                      Info from the Global structure.                      */
/*                                                                           */
/*                                                                           */
/* Input(s)           : u2PseIndex - Index of Power Supply for whose entry, a*/
/*                                   pointer is to be returned               */
/*                                                                           */
/* Output(s)          : ppPseEntry  - Address of the Pointer to the           */
/*                                       Power supply Entry                  */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPoeInfo.apPoeMainPseEntry                            */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
PoeGetMainPseEntry (UINT2 u2PseIndex, tPoeMainPseEntry ** ppPseEntry)
{
    if ((u2PseIndex < POE_MIN_PS) || (u2PseIndex > POE_MAX_PS))
    {
        *ppPseEntry = NULL;
        return POE_FAILURE;
    }

    if ((*ppPseEntry = gPoeInfo.apPoeMainPseEntry[u2PseIndex - 1]) == NULL)
    {
        *ppPseEntry = NULL;
        return POE_FAILURE;
    }
    return POE_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PoeGetNextPseEntry                                  */
/*                                                                           */
/* Description        : This function returns the pointer to the Next Pse    */
/*                      entry from the Global structure.                     */
/*                                                                           */
/*                                                                           */
/* Input(s)           : pPseEntry - Pointer to the Present PseEntry          */
/*                                                                           */
/* Output(s)          : ppNextPseEntry - Address of Pointer to NextPse       */
/*                      Entry                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPoeInfo.apPoeMainPseEntry                           */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : POE_SUCCESS / POE_FAILURE                            */
/*****************************************************************************/

INT4
PoeGetNextMainPseEntry (tPoeMainPseEntry * pPseEntry,
                        tPoeMainPseEntry ** ppNextPseEntry)
{
    UINT2               u2PseIndex;

    if (pPseEntry == NULL)
    {
        u2PseIndex = 0;
    }
    else
    {
        u2PseIndex = pPseEntry->u2MainPseGroupIndex;
    }
    u2PseIndex++;

    *ppNextPseEntry = NULL;
    for (; u2PseIndex <= POE_MAX_PS; u2PseIndex++)
    {
        if ((gPoeInfo.apPoeMainPseEntry[u2PseIndex - 1]) != NULL)
        {
            *ppNextPseEntry = gPoeInfo.apPoeMainPseEntry[u2PseIndex - 1];
            return POE_SUCCESS;
        }
    }
    return POE_FAILURE;
}

/*****************************************************************************/
/* Function Name      : PoeInitialisePseEntry                               */
/*                                                                           */
/* Description        : This function initialises the default valus for the  */
/*                      elements in port entry table                         */
/*                                                                           */
/* Input(s)           : tPoeMainPseEntry *pPseEntry                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
PoeInitialisePseEntry (tPoeMainPseEntry * pPseEntry)
{
    /* Initialise the pointer to next node  to NULL
     * NOTE: TMO_SLL_Node just contains the pointer to the next node and is the
     * first field in PoePsePortEntry Since, pPortEntry points to a port
     * entry, it actually points to TMO_SLL_node, the first field in the
     * structure. So,a typecast of the pointer enables the access of
     * TMO_SLL_Node which contains a pointer to the next Port entry
     */
    POE_SLL_INIT_NODE (&pPseEntry->NextPseEntry);

    pPseEntry->u4MainPsePower = 0;
    pPseEntry->MainPseOperStatus = OFF;
    pPseEntry->u4MainPseConsumptionPower = 0;
    return;
}

/*****************************************************************************/
/* Function Name      : PoeAddEntryToPseTable                                */
/*                                                                           */
/* Description        : This function adds a port into the global structure  */
/*                      It is called whenever a new port is created          */
/*                                                                           */
/* Input(s)           : pPseEntry - Pointer to the PseEntry to be added      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPoeInfo.apPoePortEntry                              */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : POE_SUCCESS / POE_FAILURE                            */
/*****************************************************************************/

INT4
PoeAddEntryToPseTable (tPoeMainPseEntry * pPseEntry)
{
    UINT2               u2PseIndex = 0;

    u2PseIndex = pPseEntry->u2MainPseGroupIndex;
    if ((u2PseIndex < POE_MIN_PS) || (u2PseIndex > POE_MAX_PS))
    {
        POE_TRC (INIT_SHUT_TRC, "Invalid Pse Index value\n");
        return POE_FAILURE;
    }
    else
    {
        if (gPoeInfo.apPoeMainPseEntry[u2PseIndex - 1] != NULL)
        {
            return POE_FAILURE;
        }
    }
    gPoeInfo.apPoeMainPseEntry[u2PseIndex - 1] = pPseEntry;

    return POE_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PoeDeleteEntryFromPseTable                           */
/*                                                                           */
/* Description        : This function removed a port into the globalstructure*/
/*                      It is called whenever a new port is deleted          */
/*                                                                           */
/* Input(s)           : pPseEntry - Pointer to the PseEntry to be deleted    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPoeInfo.apPoeMainPseEntry                           */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : POE_SUCCESS / POE_FAILURE                            */
/*****************************************************************************/

INT4
PoeDeleteEntryFromPseTable (tPoeMainPseEntry * pPseEntry)
{
    UINT2               u2PseIndex;

    u2PseIndex = pPseEntry->u2MainPseGroupIndex;

    if ((u2PseIndex < POE_MIN_PS) || (u2PseIndex > POE_MAX_PS))
    {
        POE_TRC (INIT_SHUT_TRC, "Invalid Pse Index value\n");
        return POE_FAILURE;
    }
    else
    {
        if (gPoeInfo.apPoeMainPseEntry[u2PseIndex - 1] == NULL)
        {
            return POE_FAILURE;
        }
    }
    gPoeInfo.apPoeMainPseEntry[u2PseIndex - 1] = NULL;

    return POE_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PoeCreatePseTable                                     */
/*                                                                            */
/* Description        : This function is fills up the Pse Table               */
/*                                                                            */
/* Input(s)           : u2PortIndex                                           */
/*                                                                            */
/* Output(s)          : None                                                  */
/*                                                                            */
/* Global Variables                                                           */
/* Referred           : None                                                  */
/*                                                                            */
/* Global Variables                                                           */
/* Modified           :                                                       */
/*                                                                            */
/*                                                                            */
/* Return Value(s)    : POE_SUCCESS - On success                              */
/*                      POE_FAILURE - On failure                              */
/*                      POE_ERR_INVALID_VALUE - when invalid values are passed*/
/*                      POE_ERR_MEM_FAILURE - On memory allocation failure.   */
/*****************************************************************************/
INT4
PoeCreatePseTable (UINT2 u2PseIndex)
{
    tPoeMainPseEntry   *pPoePseEntry = NULL;

    if ((u2PseIndex < POE_MIN_PS) || (u2PseIndex > POE_MAX_PS))
    {

        POE_TRC (INIT_SHUT_TRC, "Invalid Pse Index value\n");
        return POE_ERR_INVALID_VALUE;
    }

    /* Check if an entry for pse index exists */
    PoeGetMainPseEntry (u2PseIndex, &pPoePseEntry);
    if (pPoePseEntry != NULL)
    {
        POE_TRC (INIT_SHUT_TRC,
                 "Pse with same Index EXISTS Another Entry with"
                 " same Index cannot be created\n");
        return POE_FAILURE;
    }

    /* Allocate a block for the pse entry */
    if ((POE_PSEENTRY_ALLOC_MEM_BLOCK (pPoePseEntry, tPoeMainPseEntry)) == NULL)
    {
        POE_TRC (INIT_SHUT_TRC, "POE_PSEENTRY_ALLOC_MEM_BLOCK FAILED\n");
        return POE_ERR_MEM_FAILURE;
    }

    /* Memset the pse entry to zero */
    MEMSET (pPoePseEntry, 0, sizeof (tPoeMainPseEntry));

    pPoePseEntry->u2MainPseGroupIndex = u2PseIndex;

    /*Initializing the Pse entry */
    PoeInitialisePseEntry (pPoePseEntry);

    /* Enter the Pse Entry in the pse table. */
    PoeAddEntryToPseTable (pPoePseEntry);

    return POE_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PoeGetPortEntry                                      */
/*                                                                           */
/* Description        : This function returns the pointer to the Port entry  */
/*                      from the Global structure.                           */
/*                                                                           */
/*                                                                           */
/* Input(s)           : u2PortIndex - Index of port for whose entry, a       */
/*                                    pointer is to be returned              */
/*                                                                           */
/* Output(s)          : ppPortEntry - Address of Pointer to Port Entry       */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPoeInfo.apPoePortEntry                              */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : POE_SUCCESS / POE_FAILURE                            */
/*****************************************************************************/
INT4
PoeGetPortEntry (UINT2 u2PortIndex, tPoePsePortEntry ** ppPortEntry)
{
    if ((u2PortIndex < POE_MIN_PORTS) || (u2PortIndex > POE_MAX_PORTS))
    {
        *ppPortEntry = NULL;
        return POE_FAILURE;
    }

    if ((*ppPortEntry = gPoeInfo.apPoePortEntry[u2PortIndex - 1]) == NULL)
    {
        *ppPortEntry = NULL;
        return POE_FAILURE;
    }
    return POE_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PoeGetNextPortEntry                                  */
/*                                                                           */
/* Description        : This function returns the pointer to the Next Port   */
/*                      entry from the Global structure.                     */
/*                                                                           */
/*                                                                           */
/* Input(s)           : pPortEntry - Pointer to the Present PortEntry        */
/*                                                                           */
/* Output(s)          : ppNextPortEntry - Address of Pointer to NextPort     */
/*                      Entry                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPoeInfo.apPoePortEntry                              */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : POE_SUCCESS / POE_FAILURE                            */
/*****************************************************************************/

INT4
PoeGetNextPortEntry (tPoePsePortEntry * pPortEntry,
                     tPoePsePortEntry ** ppNextPortEntry)
{
    UINT2               u2PortIndex;

    if (pPortEntry == NULL)
    {
        u2PortIndex = 0;
    }
    else
    {
        u2PortIndex = pPortEntry->u2PsePortIndex;
    }
    u2PortIndex++;

    *ppNextPortEntry = NULL;
    for (; u2PortIndex <= POE_MAX_PORTS; u2PortIndex++)
    {
        if ((gPoeInfo.apPoePortEntry[u2PortIndex - 1]) != NULL)
        {
            *ppNextPortEntry = gPoeInfo.apPoePortEntry[u2PortIndex - 1];
            return POE_SUCCESS;
        }
    }
    return POE_FAILURE;
}

/*****************************************************************************/
/* Function Name      : PoeAddEntryToPortTable                               */
/*                                                                           */
/* Description        : This function adds a port into the global structure  */
/*                      It is called whenever a new port is created          */
/*                                                                           */
/* Input(s)           : pPortEntry - Pointer to the PortEntry to be added    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPoeInfo.apPoePortEntry                              */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : POE_SUCCESS / POE_FAILURE                            */
/*****************************************************************************/

INT4
PoeAddEntryToPortTable (tPoePsePortEntry * pPortEntry)
{
    UINT2               u2PortIndex;

    u2PortIndex = pPortEntry->u2PsePortIndex;
    if ((u2PortIndex < POE_MIN_PORTS) || (u2PortIndex > POE_MAX_PORTS))
    {

        POE_TRC (INIT_SHUT_TRC, "Invalid Port Index value\n");
        return POE_FAILURE;
    }
    else
    {
        if (gPoeInfo.apPoePortEntry[u2PortIndex - 1] != NULL)
        {
            return POE_FAILURE;
        }
    }
    gPoeInfo.apPoePortEntry[u2PortIndex - 1] = pPortEntry;

    return POE_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PoeDeleteEntryFromPortTable                          */
/*                                                                           */
/* Description        : This function removed a port into the globalstructure*/
/*                      It is called whenever a new port is deleted          */
/*                                                                           */
/* Input(s)           : pPortEntry - Pointer to the PortEntry to be deleted  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPoeInfo.apPoePortEntry                              */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : POE_SUCCESS / POE_FAILURE                            */
/*****************************************************************************/

INT4
PoeDeleteEntryFromPortTable (tPoePsePortEntry * pPortEntry)
{
    UINT2               u2PortIndex;

    u2PortIndex = pPortEntry->u2PsePortIndex;
    if ((u2PortIndex < POE_MIN_PORTS) || (u2PortIndex > POE_MAX_PORTS))
    {

        POE_TRC (INIT_SHUT_TRC, "Invalid Port Index value\n");
        return POE_FAILURE;
    }
    else
    {
        if (gPoeInfo.apPoePortEntry[u2PortIndex - 1] == NULL)
        {
            return POE_FAILURE;
        }
    }
    gPoeInfo.apPoePortEntry[u2PortIndex - 1] = NULL;

    return POE_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PoeInitialisePortEntry                               */
/*                                                                           */
/* Description        : This function initialises the default valus for the  */
/*                      elements in port entry table                         */
/*                                                                           */
/* Input(s)           : tPoePsePortEntry *pPortEntry                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
PoeInitialisePortEntry (tPoePsePortEntry * pPortEntry)
{
    /* Initialise the pointer to next node  to NULL
     * NOTE: TMO_SLL_Node just contains the pointer to the next node and is the
     * first field in PoePsePortEntry Since, pPortEntry points to a port
     * entry, it actually points to TMO_SLL_node, the first field in the
     * structure. So,a typecast of the pointer enables the access of
     * TMO_SLL_Node which contains a pointer to the next Port entry
     */
    POE_SLL_INIT_NODE (&pPortEntry->NextPortEntry);

    /* Currently Only one group is supported hence it is always 1 */
    pPortEntry->u4PseGroupIndex = 1;
    pPortEntry->PsePortAdminEnable = POE_FALSE;
    pPortEntry->PsePortDetectionStatus = POE_DISABLED;
    pPortEntry->PsePortPowerPriority = LOW;
    pPortEntry->PsePortPowerClassification = CLASS_0;
    return;
}

/*****************************************************************************/
/* Function Name      : PoeCreatePort                                         */
/*                                                                            */
/* Description        : This function is called by CFA whenever a new         */
/*                      physical port is created.It allocates memory blocks   */
/*                      for the port, its default aggregtor's config entry    */
/*                      and interface entry.                                  */
/*                                                                            */
/* Input(s)           : u2PortIndex                                           */
/*                                                                            */
/* Output(s)          : None                                                  */
/*                                                                            */
/* Global Variables                                                           */
/* Referred           : None                                                  */
/*                                                                            */
/* Global Variables                                                           */
/* Modified           :                                                       */
/*                                                                            */
/*                                                                            */
/* Return Value(s)    : POE_SUCCESS - On success                              */
/*                      POE_FAILURE - On failure                              */
/*                      POE_ERR_INVALID_VALUE - when invalid values are passed*/
/*                      POE_ERR_MEM_FAILURE - On memory allocation failure.   */
/*****************************************************************************/
INT4
PoeCreatePort (UINT2 u2PortIndex)
{
    tPoePsePortEntry   *pPoePortEntry = NULL;

    if ((u2PortIndex < POE_MIN_PORTS) || (u2PortIndex > POE_MAX_PORTS))
    {

        POE_TRC (INIT_SHUT_TRC, "Invalid Port Index value\n");
        return POE_ERR_INVALID_VALUE;
    }

    /* Check if an entry for port index exists */
    PoeGetPortEntry (u2PortIndex, &pPoePortEntry);
    if (pPoePortEntry != NULL)
    {
        POE_TRC (INIT_SHUT_TRC,
                 "Port with same Index EXISTS Another Entry with"
                 " same Index cannot be created\n");
        return POE_FAILURE;
    }

    /* Allocate a block for the port entry */
    if ((POE_PORTENTRY_ALLOC_MEM_BLOCK (pPoePortEntry, tPoePsePortEntry)) ==
        NULL)
    {
        POE_TRC (INIT_SHUT_TRC, "POE_PORTENTRY_ALLOC_MEM_BLOCK FAILED\n");
        return POE_ERR_MEM_FAILURE;
    }

    /* Memset the port entry to zero */
    MEMSET (pPoePortEntry, 0, sizeof (tPoePsePortEntry));

    pPoePortEntry->u2PsePortIndex = u2PortIndex;

    /*Initializing the Port entry */
    PoeInitialisePortEntry (pPoePortEntry);

    /* Enter the Port Entry in the port table. */
    PoeAddEntryToPortTable (pPoePortEntry);

    return POE_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PoeReleaseMacMemBlock                                */
/*                                                                           */
/* Description        : This function gives the memory block back to the pool*/
/*                                                                           */
/* Input(s)           : pPoeHashEntry.                                       */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
PoeReleaseMacMemBlock (tPoeHashNode * pPoeHashEntry)
{
    if (POE_MACENTRY_FREE_MEM_BLOCK (pPoeHashEntry) != POE_MEM_SUCCESS)
    {
        POE_TRC (DATA_PATH_TRC | CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                 "SYS: Mac Entry Memory Block Release FAILED\n");
    }
}

/*****************************************************************************/
/* Function Name      : PoeHandleInitFailure.                                */
/*                                                                           */
/* Description        : This function is called when the initialization of   */
/*                      PoE fails at any point during initialization.        */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : All memory pools.                                    */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : All memory pools deleted.                            */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
PoeHandleInitFailure (VOID)
{
    tPoeInfo           *pgPoeInfo;

    pgPoeInfo = &gPoeInfo;

    /* Deleting memory pool for PSE table */
    if (POE_PSEENTRY_POOL_ID != POE_INVALID_VAL)
    {
        if (POE_DELETE_MEM_POOL (POE_PSEENTRY_POOL_ID) != POE_MEM_SUCCESS)
        {
            POE_TRC (INIT_SHUT_TRC, "Pse Entry Memory Pool Release FAILED\n");
        }
    }

    /* Deleting memory pool for Port table */
    if (POE_PORTENTRY_POOL_ID != POE_INVALID_VAL)
    {
        if (POE_DELETE_MEM_POOL (POE_PORTENTRY_POOL_ID) != POE_MEM_SUCCESS)
        {
            POE_TRC (INIT_SHUT_TRC, "Port Entry Memory Pool Release FAILED\n");
        }
    }

    /* Deleting memory pool for PD mac table */
    if (POE_MACENTRY_POOL_ID != POE_INVALID_VAL)
    {
        if (POE_DELETE_MEM_POOL (POE_MACENTRY_POOL_ID) != POE_MEM_SUCCESS)
        {
            POE_TRC (INIT_SHUT_TRC, "Mac Entry Memory Pool Release FAILED\n");
        }
    }

    /* Deleting memory pool for Input Queue */
    if (POE_INPUTQ_POOL_ID != POE_INVALID_VAL)
    {
        if (POE_DELETE_MEM_POOL (POE_INPUTQ_POOL_ID) != POE_MEM_SUCCESS)
        {
            POE_TRC (INIT_SHUT_TRC, "Input Q Memory Pool Release FAILED\n");
        }
    }

    /* Deleting PD mac hash table */
    if (pgPoeInfo->pPoeMacTable != NULL)
    {
        POE_HASH_DELETE_TABLE (pgPoeInfo->pPoeMacTable, PoeReleaseMacMemBlock);
    }

    /* Deleting Input Queue */
    if (gPoeInfo.PoeInputQId != POE_INIT_VAL)
    {
        if (OsixDeleteQ (SELF, (const UINT1 *) POE_QUE) != OSIX_SUCCESS)
        {
            POE_TRC (INIT_SHUT_TRC, "Queue Deletion FAILED\n");
        }
        gPoeInfo.PoeInputQId = POE_INIT_VAL;
    }

    /* Deleting Semaphore */
    if (POE_SEMAPHORE_ID != POE_INVALID_VAL)
    {
        if (POE_DELETE_SEMAPHORE (POE_PROTOCOL_SEMAPHORE, POE_SEMAPHORE_ID)
            != POE_OSIX_SUCCESS)
        {
            POE_TRC (INIT_SHUT_TRC, "Semaphore Release FAILED\n");
        }
    }

    /* Setting module init flag to false */
    gPoeInfo.u2PoeModInit = POE_FALSE;
}

/*****************************************************************************/
/* Function Name      : PoeModuleInit                                        */
/*                                                                           */
/* Description        : This function allocates memory pools &               */
/*                      initialises data structures used by POE              */
/*                      module                                               */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPoeInfo                                             */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gPoeInfo                                             */
/*                                                                           */
/* Return Value(s)    : POE_SUCCESS / POE_FAILURE                            */
/*****************************************************************************/
INT4
PoeModuleInit (VOID)
{

    tCfaIfInfo          CfaIfInfo;
    UINT2               u2PortIndex;
    UINT2               u2PseIndex;

    /* Initialise the global structure variable */
    MEMSET (&gPoeInfo, 0, sizeof (tPoeInfo));

    if (POE_CREATE_SEMAPHORE (POE_PROTOCOL_SEMAPHORE, POE_BINARY_SEM,
                              POE_SEM_FLAGS,
                              &POE_SEMAPHORE_ID) != POE_OSIX_SUCCESS)
    {
        POE_TRC (INIT_SHUT_TRC | ALL_FAILURE_TRC,
                 "PoeTaskInit: POE Protocol semaphore creation failed in PoeModuleInit\n");
        return POE_FAILURE;
    }

    /*By default POE is globally disabled */
    POE_SYSTEM_CONTROL = POE_SHUTDOWN;

    /* Assign Trace Option */
    gPoeInfo.u4TraceOption = 0;

    /* Set the mac processing flag (learnt or aged mac) to false */
    gPoeInfo.u2PoeMacAddrsProcess = POE_FALSE;

    /* Initialise current available power */
    gPoeInfo.u2CurrPsePower = 0;

    /* Initialise Maximum Power Supply, Downlink, Uplink, Supervisor, Fan modules */
    gPoeInfo.u2MaxPwrSupplyMod = MAX_PWR_SUPPLY_MODULE;
    gPoeInfo.u2MaxDnLnkMod = MAX_DOWNLNK_MODULE;
    gPoeInfo.u2MaxUpLnkMod = MAX_UPLNK_MODULE;
    gPoeInfo.u2MaxSuprvisrMod = MAX_SUPRVISR_MODULE;
    gPoeInfo.u2MaxFanMod = MAX_FAN_MODULE;

    /* Initializing input Q count to 0 */
    POE_INPUTQ_MSG_CNT = 0;

    /* Allocate memory pool for Poe Pse table */
    if (POE_CREATE_MEM_POOL (POE_PSEENTRY_MEMBLK_SIZE,
                             POE_PSEENTRY_MEMBLK_COUNT,
                             &(POE_PSEENTRY_POOL_ID)) != POE_MEM_SUCCESS)
    {

        POE_TRC (INIT_SHUT_TRC, "Pse Entry Memory Pool Creation FAILED\n");
        PoeHandleInitFailure ();
        return POE_FAILURE;
    }

    /* Allocate memory pool for Poe port table */
    if (POE_CREATE_MEM_POOL (POE_PORTENTRY_MEMBLK_SIZE,
                             POE_PORTENTRY_MEMBLK_COUNT,
                             &(POE_PORTENTRY_POOL_ID)) != POE_MEM_SUCCESS)
    {

        POE_TRC (INIT_SHUT_TRC, "Port Entry Memory Pool Creation FAILED\n");
        PoeHandleInitFailure ();
        return POE_FAILURE;

    }

    /* Allocate Memory Pool for Poe Mac Hash table */
    if (POE_CREATE_MEM_POOL (POE_MACENTRY_MEMBLK_SIZE,
                             POE_MACENTRY_MEMBLK_COUNT,
                             &(POE_MACENTRY_POOL_ID)) != POE_MEM_SUCCESS)
    {

        POE_TRC (INIT_SHUT_TRC, "Mac Entry Memory Pool Creation FAILED\n");
        PoeHandleInitFailure ();
        return POE_FAILURE;
    }

    /* Allocate Memory Pool for Poe Input Q */
    if (POE_CREATE_MEM_POOL (POE_INPUTQ_MEMBLK_SIZE,
                             POE_INPUTQ_MEMBLK_COUNT,
                             &(POE_INPUTQ_POOL_ID)) != POE_MEM_SUCCESS)
    {

        POE_TRC (INIT_SHUT_TRC, "Poe Input Q Memory Pool Creation FAILED\n");
        PoeHandleInitFailure ();
        return POE_FAILURE;
    }

    /* Creating Queue */
    if (OsixCreateQ ((const UINT1 *) POE_QUE,
                     POE_Q_DEPTH, POE_Q_MODE, &(gPoeInfo.PoeInputQId))
        != OSIX_SUCCESS)
    {
        PoeHandleInitFailure ();
        POE_TRC (INIT_SHUT_TRC, "Poe Input Q Creation FAILED\n");
        return POE_FAILURE;
    }

    /* Creation of Mac Hash Table */
    gPoeInfo.pPoeMacTable = POE_HASH_CREATE_TABLE (POE_MAC_TABLE_SIZE, NULL, 0);
    if (gPoeInfo.pPoeMacTable == NULL)
    {

        POE_TRC (INIT_SHUT_TRC, "PoE Mac Hash Table Creation FAILED\n");
        PoeHandleInitFailure ();
        return POE_FAILURE;
    }

#ifdef NPAPI_WANTED
    if (FNP_FAILURE == FsPoeHwInit ())
    {
        POE_TRC (INIT_SHUT_TRC, "FsPoeHwInit() FAILED\n");
        PoeHandleInitFailure ();
        return POE_FAILURE;
    }
#endif

    /*For Creating Port Table entries */
    for (u2PortIndex = 1; u2PortIndex <= POE_MAX_PORTS; u2PortIndex++)
    {
        if (((CfaGetIfInfo (u2PortIndex, &CfaIfInfo) != POE_SUCCESS) ||
             (CfaIfInfo.u1IfType != POE_ETHERNET_TYPE)) ||
            (CfaIfInfo.u1BridgedIface == CFA_DISABLED))
        {
            continue;
        }

        POE_TRC_ARG1 (CONTROL_PLANE_TRC,
                      "PoE Port: %u :: Physical Port Exists. Creating an Entry... \n",
                      u2PortIndex);

        if (PoeCreatePort (u2PortIndex) != POE_SUCCESS)
        {
            POE_TRC_ARG1 (INIT_SHUT_TRC,
                          "PoE Port: %u :: Port Entry Creation FAILED\n ",
                          u2PortIndex);
        }
        POE_TRC_ARG1 (CONTROL_PLANE_TRC, " Port %u created\n", u2PortIndex);
    }

    /*For Creating Pse Table entries */
    for (u2PseIndex = 1; u2PseIndex <= POE_MAX_PS; u2PseIndex++)
    {
        if (PoeCreatePseTable (u2PseIndex) != POE_SUCCESS)
        {
            POE_TRC_ARG1 (INIT_SHUT_TRC,
                          "PoE Pse: %u :: Pse Entry Creation FAILED\n ",
                          u2PseIndex);
        }
        POE_TRC_ARG1 (CONTROL_PLANE_TRC, " Pse %u created\n", u2PseIndex);
    }
    /* Setting module Init flag to true */
    gPoeInfo.u2PoeModInit = POE_TRUE;

    POE_TRC (CONTROL_PLANE_TRC, " PoE Module Inititalized !! \n");
    return POE_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PoeShutDown.                                         */
/*                                                                           */
/* Description        : This function is called during system shut down or   */
/*                      when the manager wants to shut the PoE module down.  */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : All memory pools.                                    */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : All memory pools released.                           */
/*                                                                           */
/* Return Value(s)    : POE_SUCCESS - On success                             */
/*                      POE_FAILURE - On failure                             */
/*****************************************************************************/
INT4
PoeShutDown (VOID)
{
    tPoePsePortEntry   *pPortEntry = NULL;
    tPoeMainPseEntry   *pPseEntry = NULL;
    UINT2               u2PortIndex;
    UINT2               u2PseIndex;
    UINT4               i4ErrStatus = 0;

    /* If the POE module is shut down return FAILURE */
    if (POE_SYSTEM_CONTROL != POE_START)
    {
        POE_TRC (INIT_SHUT_TRC, "System is not running. Cannot Shut down.\n");
        return POE_SUCCESS;
    }

    /* Deleting all Pse port entries */
    for (u2PortIndex = 1; u2PortIndex <= POE_MAX_PORTS; u2PortIndex++)
    {
        PoeGetPortEntry (u2PortIndex, &pPortEntry);
        if (pPortEntry == NULL)
        {
            continue;
        }

        pPortEntry->PsePortAdminEnable = POE_FALSE;

        /*Calling Power Removal API for all PoE enabled ports */
#ifdef NPAPI_WANTED
        if (FsPoeRemovePowerFromPI (pPortEntry, i4ErrStatus) != FNP_SUCCESS)
        {
            POE_TRC_ARG1 (CONTROL_PLANE_TRC,
                          "Remove Power API failed on Port - %d\n",
                          u2PortIndex);
        }
#else
        UNUSED_PARAM (i4ErrStatus);
#endif
        /* Delete the Port Entry pointer from the array  of Port entry 
         * pointers in the global info */
        PoeDeleteEntryFromPortTable (pPortEntry);
        POE_PORTENTRY_FREE_MEM_BLOCK (pPortEntry);

    }

    /* Deleting all Main Pse entries */
    for (u2PseIndex = 1; u2PseIndex <= POE_MAX_PS; u2PseIndex++)
    {
        PoeGetMainPseEntry (u2PseIndex, &pPseEntry);
        if (pPseEntry == NULL)
        {
            continue;
        }

        /* Delete the Pse Entry pointer from the array  of Pse entry 
         * pointers in the global info */
        PoeDeleteEntryFromPseTable (pPseEntry);
        POE_PSEENTRY_FREE_MEM_BLOCK (pPseEntry);
    }

    /* Delete Mac Entry Table */
    POE_HASH_DELETE_TABLE (gPoeInfo.pPoeMacTable, PoeReleaseMacMemBlock);
    gPoeInfo.pPoeMacTable = NULL;

    /* Delete Pse Entry Mem Pool */
    if (POE_DELETE_MEM_POOL (POE_PSEENTRY_POOL_ID) != POE_MEM_SUCCESS)
    {
        POE_TRC (INIT_SHUT_TRC, "Pse Entry Memory Pool Release FAILED\n");
    }
    POE_PSEENTRY_POOL_ID = 0;

    /* Delete Port Entry Mem pool */
    if (POE_DELETE_MEM_POOL (POE_PORTENTRY_POOL_ID) != POE_MEM_SUCCESS)
    {

        POE_TRC (INIT_SHUT_TRC, "Port Entry Memory Pool Release FAILED\n");
    }
    POE_PORTENTRY_POOL_ID = 0;

    /* Delete Mac Entry Mem Pool */
    if (POE_DELETE_MEM_POOL (POE_MACENTRY_POOL_ID) != POE_MEM_SUCCESS)
    {
        POE_TRC (INIT_SHUT_TRC, "Mac Entry Memory Pool Release FAILED\n");
    }
    POE_MACENTRY_POOL_ID = 0;

    /* Delete Input Q Mem Pool */
    if (POE_DELETE_MEM_POOL (POE_INPUTQ_POOL_ID) != POE_MEM_SUCCESS)
    {
        POE_TRC (INIT_SHUT_TRC, "Input Q Memory Pool Release FAILED\n");
    }
    POE_INPUTQ_POOL_ID = 0;

    /* Deleting Semaphore */
    if (POE_DELETE_SEMAPHORE (POE_PROTOCOL_SEMAPHORE, POE_SEMAPHORE_ID)
        != POE_OSIX_SUCCESS)
    {
        POE_TRC (INIT_SHUT_TRC, "Semaphore Release FAILED\n");
    }
    POE_SEMAPHORE_ID = 0;

#ifdef NPAPI_WANTED
    if (FNP_FAILURE == FsPoeHwDeInit ())
    {
        POE_TRC (INIT_SHUT_TRC, "Hardware DeInitialisation of POE FAILED\n");
    }
#endif

    /* Deleting Input Queue */
    if (gPoeInfo.PoeInputQId != POE_INIT_VAL)
    {
        if (OsixDeleteQ (SELF, (const UINT1 *) POE_QUE) != OSIX_SUCCESS)
        {
            POE_TRC (INIT_SHUT_TRC, "Queue Deletion FAILED\n");
        }
        gPoeInfo.PoeInputQId = POE_INIT_VAL;
    }

    /* Setting module Init flag to false */
    gPoeInfo.u2PoeModInit = POE_FALSE;

    POE_SYSTEM_CONTROL = POE_SHUTDOWN;
    POE_TRC (INIT_SHUT_TRC, "PoE Module shut down\n");

    return POE_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PoeProcessNewMacLearnt                               */
/*                                                                           */
/* Description        : This function processes the new MAC learnt event     */
/*                      message                                              */
/* Input(s)           : PoeMacPortPair - has the mac and the port on which   */
/*                      it was learnt.                                       */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPoeInfo.u2PoeMacAddrsProcess                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : POE_SUCCESS - On success                             */
/*                      POE_FAILURE - On failure                             */
/*****************************************************************************/
INT4
PoeProcessNewMacLearnt (tPoeMacPortMsg PoeMacPortPair)
{
    tPoeMacEntry       *pPdMacNode = NULL;
    tPoePsePortEntry   *pPortEntry;
    INT4                i4GrpIndex = 1;
    UINT4               u4ErrCode;
#ifdef NPAPI_WANTED
    tPoePsePortEntry   *pPortEntryRemove;
    INT4                i4ErrStatus = 0;
#endif
    UINT2               u2Port;

    /*Getting the PD node */
    if (PoeSnmpLowGetPdMacNode (PoeMacPortPair.MacAddr,
                                &pPdMacNode) != POE_SUCCESS)
    {
        POE_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                 " SNMP: PD Mac not present ...\n");
        return POE_SUCCESS;
    }

    /*Setting the port Index through which the mac has been learnt */
    pPdMacNode->u4PsePortIndex = (UINT4) PoeMacPortPair.u2PortIndex;

    /* Setting the Port Priority to high since the mac is leanrt and also
     * is present in the PD Mac table .The priority is set to high so that
     * if there arises a need to remove power from low priority ports to 
     * provide for the high priority ports these ports are not disturbed 
     * for the same */

    /* Setting the gPoeInfo.u2PoeMacAddrsProcess to true to indicate
     * to the PoE module that MAC processing is going on*/
    gPoeInfo.u2PoeMacAddrsProcess = POE_TRUE;

    /* Group Index is always set to 1 since its not supported */
    /* Calling the test routine to validate the port priority */
    if (nmhTestv2PethPsePortPowerPriority (&u4ErrCode, i4GrpIndex,
                                           pPdMacNode->u4PsePortIndex,
                                           HIGH) != SNMP_SUCCESS)
    {
        POE_TRC (CONTROL_PLANE_TRC,
                 "Test Routine for setting priority failed ");
        return POE_FAILURE;
    }

    /* Calling the set routine to set the port priority to high */
    if (nmhSetPethPsePortPowerPriority (i4GrpIndex,
                                        pPdMacNode->u4PsePortIndex,
                                        HIGH) != SNMP_SUCCESS)
    {
        POE_TRC (CONTROL_PLANE_TRC, "\r\nUnable to set PoE Port Priority to \
                                      High for mac\r\n");
        return POE_FAILURE;
    }
    /* Getting Port Entry */
    PoeGetPortEntry (pPdMacNode->u4PsePortIndex, &pPortEntry);
    if (pPortEntry == NULL)
    {
        POE_TRC (CONTROL_PLANE_TRC, "Unable to get Port Entry for PD Mac");
        return SNMP_FAILURE;
    }

#ifdef NPAPI_WANTED
    if (pPortEntry->PsePortDetectionStatus != POE_DELIVERING_POWER &&
        pPortEntry->PsePortAdminEnable == POE_TRUE)
    {
        /*Detecting and Classifying power */
        if (FsPoeDetectAndClassifyPD (pPortEntry, i4ErrStatus) != FNP_SUCCESS)
        {
            POE_TRC_ARG1 (CONTROL_PLANE_TRC,
                          "\tPD detection and classification failed on Port - %d\n",
                          pPdMacNode->u4PsePortIndex);
            pPortEntry->PsePortDetectionStatus = POE_FAULT;
            return SNMP_FAILURE;
        }
        else
        {
            /*Getting the power availability status */
            if (PoeGetPowerAvailableStatus
                (pPortEntry->PsePortPowerClassification) == POE_SUCCESS)
            {
                /*Applying power on the powered Interface */
                if (FsPoeApplyPowerOnPI (pPortEntry, i4ErrStatus) !=
                    FNP_SUCCESS)
                {
                    POE_TRC_ARG1 (CONTROL_PLANE_TRC,
                                  "\tApply Power failed on Port - %d\n",
                                  pPdMacNode->u4PsePortIndex);
                    pPortEntry->PsePortDetectionStatus = POE_FAULT;
                    return SNMP_FAILURE;
                }
                pPortEntry->PsePortDetectionStatus = POE_DELIVERING_POWER;
                PoeUpdatePowerAvailable (pPortEntry->PsePortPowerClassification,
                                         POE_SUBTRACT);
            }
            else
            {
                POE_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                         "Insufficient power !! Removing power from low priority PI(s)\n");
                /*Getting port entries one by one */
                for (u2Port = 1; u2Port <= POE_MAX_PORTS; u2Port++)
                {
                    PoeGetPortEntry (u2Port, &pPortEntryRemove);
                    if (pPortEntryRemove == NULL)
                    {
                        continue;
                    }
                    /* Checking if the port priority is low */
                    if (pPortEntryRemove->PsePortPowerPriority == LOW &&
                        pPortEntryRemove->PsePortDetectionStatus ==
                        POE_DELIVERING_POWER)
                    {
                        /* Calling Power Removal API for the PoE enabled low 
                         * priority port*/
                        if (FsPoeRemovePowerFromPI
                            (pPortEntryRemove, i4ErrStatus) != FNP_SUCCESS)
                        {
                            POE_TRC_ARG1 (CONTROL_PLANE_TRC,
                                          "\tCalling Remove Power API on Port - %d Failed!!\n",
                                          u2Port);
                        }
                        else
                        {
                            pPortEntryRemove->PsePortDetectionStatus =
                                POE_DISABLED;
                            PoeUpdatePowerAvailable (pPortEntryRemove->
                                                     PsePortPowerClassification,
                                                     POE_ADD);
                        }
                    }
                    /* Getting the power availability status to see if 
                     * power is available*/
                    if (PoeGetPowerAvailableStatus
                        (pPortEntry->PsePortPowerClassification) == POE_SUCCESS)
                    {
                        /*Applying power on the powered Interface */
                        if (FsPoeApplyPowerOnPI (pPortEntry, i4ErrStatus)
                            != FNP_SUCCESS)
                        {
                            POE_TRC_ARG1 (CONTROL_PLANE_TRC,
                                          "\tApply Power failed on Port - %d\n",
                                          pPdMacNode->u4PsePortIndex);
                            pPortEntry->PsePortDetectionStatus = POE_FAULT;
                            return SNMP_FAILURE;
                        }
                        else
                        {
                            pPortEntry->PsePortDetectionStatus =
                                POE_DELIVERING_POWER;
                            PoeUpdatePowerAvailable (pPortEntry->
                                                     PsePortPowerClassification,
                                                     POE_SUBTRACT);
                            break;
                        }
                    }
                }
            }
        }
    }
#else
    UNUSED_PARAM (u2Port);
#endif

    return POE_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PoeProcessAgedOutMac                                 */
/*                                                                           */
/* Description        : This function processes the MAC Aging event     */
/*                      message                                              */
/* Input(s)           : PoeMacPortPair - has the mac and the port on which   */
/*                      it was Aged Out.                                     */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPoeInfo.u2PoeMacAddrsProcess                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : POE_SUCCESS - On success                             */
/*                      POE_FAILURE - On failure                             */
/*****************************************************************************/
INT4
PoeProcessAgedOutMac (tPoeMacPortMsg PoeMacPortPair)
{
    tPoeMacEntry       *pPdMacNode = NULL;
    tPoePsePortEntry   *pPortEntry;
    INT4                i4GrpIndex = 1;
    UINT4               u4ErrCode;

    /* Getting the PD node */
    if (PoeSnmpLowGetPdMacNode (PoeMacPortPair.MacAddr,
                                &pPdMacNode) != POE_SUCCESS)
    {
        POE_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                 " SNMP: PD Mac not present ...\n");
        return POE_SUCCESS;
    }

    if (pPdMacNode->u4PsePortIndex != (UINT4) PoeMacPortPair.u2PortIndex)
    {
        POE_TRC (CONTROL_PLANE_TRC, "2 different ports for same PD Mac...\n");
    }

    /* Setting the Port Priority to low since the mac is aged out though still
     * its present in the PD Mac table .The priority is set to low so that
     * if there arises a need to remove power from low priority ports to provide
     * for the high priority ports these ports can also be used for the same*/

    /* Setting the gPoeInfo.u2PoeMacAddrsProcess to true to indicate
     * to the PoE module that MAC processing is going on*/
    gPoeInfo.u2PoeMacAddrsProcess = POE_TRUE;

    /* Group Index is always set to 1 since its not supported */
    /* Calling the test routine to validate the port priority */
    if (nmhTestv2PethPsePortPowerPriority (&u4ErrCode, i4GrpIndex,
                                           pPdMacNode->u4PsePortIndex,
                                           LOW) != SNMP_SUCCESS)
    {
        POE_TRC (CONTROL_PLANE_TRC,
                 "Test Routine for setting priority failed ");
        return POE_FAILURE;
    }

    /* Calling the set routine to set the port priority to low */
    if (nmhSetPethPsePortPowerPriority (i4GrpIndex,
                                        pPdMacNode->u4PsePortIndex,
                                        LOW) != SNMP_SUCCESS)
    {
        POE_TRC (CONTROL_PLANE_TRC,
                 "Unable to set PoE Port Priority to Low for mac");
        return POE_FAILURE;
    }

    /* Getting Port Entry */
    PoeGetPortEntry (pPdMacNode->u4PsePortIndex, &pPortEntry);
    if (pPortEntry == NULL)
    {
        POE_TRC (CONTROL_PLANE_TRC, "Unable to get Port Entry for PD Mac");
        return SNMP_FAILURE;
    }

#ifdef NPAPI_WANTED
    /* Removing power */
    if (pPortEntry->PsePortDetectionStatus == POE_DELIVERING_POWER)
    {
        if (FsPoeRemovePowerFromPI (pPortEntry, (INT4) u4ErrCode) !=
            FNP_SUCCESS)
        {
            POE_TRC_ARG1 (CONTROL_PLANE_TRC,
                          "Power removal failed on Port - %d\n",
                          pPdMacNode->u4PsePortIndex);
            return SNMP_FAILURE;
        }
    }
    pPortEntry->PsePortDetectionStatus = POE_DISABLED;
    PoeUpdatePowerAvailable (pPortEntry->PsePortPowerClassification, POE_ADD);
    /*Setting the port Index to 0 since its aged out */
    pPdMacNode->u4PsePortIndex = 0;
#endif

    return POE_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PoeProcessQMsg                                       */
/*                                                                           */
/* Description        : This function processes the messages recvd in the POE*/
/*                      input                                                */
/* Input(s)           : Qname - The input Q from which messages need to be   */
/*                              deQd                                         */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : POE_SUCCESS - On success                             */
/*                      POE_FAILURE - On failure                             */
/*****************************************************************************/

INT4
PoeProcessQMsg (const UINT1 *pu1Qname)
{
    tPoeQMsg           *pMsg = NULL;
    INT4                i4RetVal = POE_SUCCESS;
    tOsixMsg           *pOsixMsg = NULL;

    while (OsixReceiveFromQ (0,
                             (const UINT1 *) pu1Qname,
                             OSIX_NO_WAIT, 0, &pOsixMsg) == OSIX_SUCCESS)
    {
        pMsg = (tPoeQMsg *) pOsixMsg;
        POE_TRC_ARG1 (CONTROL_PLANE_TRC, "\tReceived Message Event - %d\n",
                      pMsg->u4MsgType);

        switch (pMsg->u4MsgType)
        {
            case POE_MAC_LEARNING:
                i4RetVal = PoeProcessNewMacLearnt (pMsg->PoeMacPortPair);
                if (i4RetVal == POE_FAILURE)
                {
                    i4RetVal = POE_ERR_MAC_LEARNING_PROCESS;
                    POE_TRC (ALL_FAILURE_TRC,
                             "\tError Applying power on Pd Mac port!!!\n");
                }
                break;

            case POE_MAC_AGING:
                i4RetVal = PoeProcessAgedOutMac (pMsg->PoeMacPortPair);
                if (i4RetVal == POE_FAILURE)
                {
                    i4RetVal = POE_ERR_MAC_AGING_PROCESS;
                    POE_TRC (ALL_FAILURE_TRC,
                             "Error Removing power on Aged out Mac port!!!\n");
                }
                break;

            default:
                POE_TRC (ALL_FAILURE_TRC,
                         "\tUnknown PoE queue message type!!!\n");
                i4RetVal = POE_FAILURE;
                break;
        }
        POE_QMSG_FREE (pMsg);
    }
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : SendMsgToQAndEventToPoE                              */
/*                                                                           */
/* Description        : This function sends messages to the POE Queue and    */
/*                      posts an event to PoE task                           */
/* Input(s)           :                                                      */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : POE_SUCCESS - On success                             */
/*                      POE_FAILURE - On failure                             */
/*****************************************************************************/
INT4
SendMsgToQAndEventToPoE (UINT2 u2Port, tMacAddr macAddr, UINT4 u4Event)
{
    UINT1               au1Qname[POE_Q_NAME_SIZE] = POE_QUE;
    tPoeQMsg           *pQMsg = NULL;
    tPoeMacEntry       *pPdMacNode = NULL;

    if (POE_IS_SHUTDOWN ())
    {
        POE_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                 "POE should be enabled for it to process mac entries !!\n");
        return POE_SUCCESS;
    }

    /* Getting the PD node */
    if (PoeSnmpLowGetPdMacNode (macAddr, &pPdMacNode) != POE_SUCCESS)
    {
        POE_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                 " SNMP: PD Mac not present ...\n");
        return POE_SUCCESS;
    }

    /* Allocate a block for the Q Msg */
    POE_QMSG_ALLOC_MEM_BLOCK (pQMsg, tPoeQMsg);
    if (pQMsg == NULL)
    {
        return POE_FAILURE;
    }

    /* Forming the Message to be sent to Queue */
    pQMsg->u4MsgType = u4Event;
    pQMsg->PoeMacPortPair.u2PortIndex = u2Port;
    MEMCPY (pQMsg->PoeMacPortPair.MacAddr, macAddr, POE_MAC_ADDR_SIZE);

    /* Posting the message which holds the port-mac pair to the PoE Q */
    if (OsixSendToQ (0, au1Qname, (tOsixMsg *) (VOID *) pQMsg, OSIX_MSG_NORMAL)
        != OSIX_SUCCESS)
    {
        POE_TRC (ALL_FAILURE_TRC | MGMT_TRC,
                 "\tPoE Error - Posting message to Queue failed!!!\n");
        POE_QMSG_FREE (pQMsg);
        return (POE_FAILURE);
    }

    /* Send event accordingly to the PoE Task */
    if (OsixSendEvent (0, (const UINT1 *) POE_TASK_NAME, POE_Q_EVENT)
        != OSIX_SUCCESS)
    {
        POE_TRC (ALL_FAILURE_TRC | MGMT_TRC,
                 "\tPoE Error - Posting event to PoE Task failed!!!\n");
        return POE_FAILURE;
    }
    return POE_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PoeTaskMain                                          */
/*                                                                           */
/* Description        : This function is the entry point function of the     */
/*                      POE Task. This function receives  and                */
/*                      processes events.                                    */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
PoeTaskMain (INT1 *pi1Param)
{
    UINT4               u4Event = 0;
    INT4                i4RetStat;
    UINT1               au1Qname[POE_Q_NAME_SIZE] = POE_QUE;

    UNUSED_PARAM (pi1Param);

    gPoeInfo.poeTaskId = OsixGetCurTaskId ();

    if (PoeModuleInit () != POE_SUCCESS)
    {
        /* Indicate the status of initialization to the main routine */
        POE_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
    else
    {
        /* Indicate the status of initialization to the main routine */
        POE_INIT_COMPLETE (OSIX_SUCCESS);
#ifdef SNMP_2_WANTED
        /* Register the protocol MIBs with SNMP */
        RegisterFSPOE ();
        RegisterSTDPOE ();
#endif
    }

    /* Always Wait For An Event to Occur */
    POE_TRC (ALL_FAILURE_TRC | OS_RESOURCE_TRC |
             CONTROL_PLANE_TRC, " POE: Waiting For Event...\n");
    while (1)
    {
        if (POE_RECEIVE_EVENT (POE_PSE_STATUS_EVENT |
                               POE_HW_ERROR_EVENT |
                               POE_LINK_STATUS_EVENT |
                               POE_Q_EVENT,
                               OSIX_WAIT, 0, &u4Event) == OSIX_SUCCESS)
        {
            POE_TRC (OS_RESOURCE_TRC | CONTROL_PLANE_TRC,
                     "POE: Event Received , processing ...\n");
            if ((u4Event & POE_PSE_STATUS_EVENT) != 0)
            {
                /*TO DO depending on the event either shut down poe module
                 * or reschedule the power given to the ports
                 * make sure you  call PoeUpdatePowerAvailable with
                 * appropriate params.
                 * Rescedule the power given to the PDs.?? */

                POE_TRC (OS_RESOURCE_TRC | CONTROL_PLANE_TRC,
                         " POE: Event Received from PSE...\n");
            }
            if ((u4Event & POE_HW_ERROR_EVENT) != 0)
            {
                POE_TRC (OS_RESOURCE_TRC | CONTROL_PLANE_TRC,
                         "POE: Error Event Received from POE H/W..Shutting Down PoE.\n");
                /*TO DO depending on the event either shut down poe module
                 * or reschedule the power given to the ports
                 * make sure you  call PoeUpdatePowerAvailable with
                 * appropriate params.
                 * Rescedule the power given to the PDs.?? */
            }
            if ((u4Event & POE_LINK_STATUS_EVENT) != 0)
            {
                POE_TRC (OS_RESOURCE_TRC | CONTROL_PLANE_TRC,
                         "POE: Link Status Change Event..Shutting down PoE.\n");
                /*Handle  Link down status and Link up status */
            }
            if ((u4Event & POE_Q_EVENT) != 0)
            {
                POE_TRC (OS_RESOURCE_TRC | CONTROL_PLANE_TRC,
                         " POE: Received Q Event...\n");
                i4RetStat = PoeProcessQMsg (au1Qname);

                if (i4RetStat == POE_ERR_MAC_LEARNING_PROCESS)
                {
                    POE_TRC (OS_RESOURCE_TRC | CONTROL_PLANE_TRC,
                             "POE: Error Processing Mac Learning event\n");
                }
                else if (i4RetStat == POE_ERR_MAC_AGING_PROCESS)
                {
                    POE_TRC (OS_RESOURCE_TRC | CONTROL_PLANE_TRC,
                             "POE: Error Processing Mac Aging event\n");
                }
                else if (i4RetStat == POE_FAILURE)
                {
                    POE_TRC (OS_RESOURCE_TRC | CONTROL_PLANE_TRC,
                             "POE: Processing Q event failed\n");
                }
            }
            POE_TRC (OS_RESOURCE_TRC | CONTROL_PLANE_TRC,
                     " POE: Event Processing done...\n");
            POE_TRC (OS_RESOURCE_TRC | CONTROL_PLANE_TRC,
                     " POE: Waiting For Next Event ...\n");
        }
    }
}

/*****************************************************************************/
/* Function Name      : PoeLock                                              */
/*                                                                           */
/* Description        : This function is called to Lock the semaphore for    */
/*                      proper processing of multiple events at a time       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPoeInfo                                             */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : ProtocolLocked is changed to TRUE                    */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS - On success                            */
/*                      SNMP_FAILURE - On failure of creation of semaphore   */
/*****************************************************************************/
INT4
PoeLock ()
{
    if (POE_SEMAPHORE_ID != POE_INVALID_VAL)
    {
        POE_TAKE_PROTOCOL_SEMAPHORE ();

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/*****************************************************************************/
/* Function Name      : PoeUnLock                                            */
/*                                                                           */
/* Description        : This function is called to Unlock the semaphore for  */
/*                      proper processing of multiple events at a time       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPoeInfo                                             */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : ProtocolLocked is changed to FALSE                   */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS - On success                            */
/*                      SNMP_FAILURE - On failure bcoz of no semaphore.      */
/*****************************************************************************/
INT4
PoeUnLock ()
{
    if (POE_SEMAPHORE_ID != POE_INVALID_VAL)
    {
        POE_RELEASE_PROTOCOL_SEMAPHORE ();

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/*****************************************************************************/
/* Function Name      : PoeGetModuleStatus                                   */
/*                                                                           */
/* Description        : This function is called to get the module status     */
/*                      of poe module                                        */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : pi4RetValFsPoeGlobalAdminStatus                      */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : POE_SYSTEM_CONTROL                                   */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : POE_SUCCESS                                          */
/*****************************************************************************/

INT4
PoeGetModuleStatus (INT4 *pi4RetValFsPoeGlobalAdminStatus)
{
    PoeLock ();
    *pi4RetValFsPoeGlobalAdminStatus = (INT4) POE_SYSTEM_CONTROL;
    PoeUnLock ();
    return POE_SUCCESS;
}

#endif
