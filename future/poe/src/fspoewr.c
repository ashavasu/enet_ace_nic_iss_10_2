#include "poehdrs.h"
#include "fspoedb.h"

VOID
RegisterFSPOE ()
{
    SNMPRegisterMib (&fspoeOID, &fspoeEntry, SNMP_MSR_TGR_TRUE);
    SNMPAddSysorEntry (&fspoeOID, (const UINT1 *) "fspoe");
}

INT4
FsPoeGlobalAdminStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsPoeGlobalAdminStatus (&(pMultiData->i4_SLongValue)));
}

INT4
FsPoeGlobalAdminStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsPoeGlobalAdminStatus (pMultiData->i4_SLongValue));
}

INT4
FsPoeGlobalAdminStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsPoeGlobalAdminStatus
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsPoeGlobalAdminStatusDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsPoeGlobalAdminStatus
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsPoeMacTable (tSnmpIndex * pFirstMultiIndex,
                           tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsPoeMacTable ((tMacAddr *) pNextMultiIndex->
                                           pIndex[0].pOctetStrValue->
                                           pu1_OctetList) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsPoeMacTable
            (*(tMacAddr *) pFirstMultiIndex->pIndex[0].pOctetStrValue->
             pu1_OctetList,
             (tMacAddr *) pNextMultiIndex->pIndex[0].pOctetStrValue->
             pu1_OctetList) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[0].pOctetStrValue->i4_Length = 6;
    return SNMP_SUCCESS;
}

INT4
FsPoePdMacAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPoeMacTable ((*(tMacAddr *) pMultiIndex->
                                                pIndex[0].pOctetStrValue->
                                                pu1_OctetList)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MEMCPY (pMultiData->pOctetStrValue->pu1_OctetList,
            pMultiIndex->pIndex[0].pOctetStrValue->pu1_OctetList,
            pMultiIndex->pIndex[0].pOctetStrValue->i4_Length);
    pMultiData->pOctetStrValue->i4_Length =
        pMultiIndex->pIndex[0].pOctetStrValue->i4_Length;

    return SNMP_SUCCESS;

}

INT4
FsPoePdMacPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPoeMacTable ((*(tMacAddr *) pMultiIndex->
                                                pIndex[0].pOctetStrValue->
                                                pu1_OctetList)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsPoePdMacPort ((*(tMacAddr *) pMultiIndex->pIndex[0].
                                   pOctetStrValue->pu1_OctetList),
                                  &(pMultiData->i4_SLongValue)));

}

INT4
FsPoePdMacRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPoeMacTable ((*(tMacAddr *) pMultiIndex->
                                                pIndex[0].pOctetStrValue->
                                                pu1_OctetList)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsPoePdMacRowStatus ((*(tMacAddr *) pMultiIndex->pIndex[0].
                                        pOctetStrValue->pu1_OctetList),
                                       &(pMultiData->i4_SLongValue)));

}

INT4
FsPoePdMacRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsPoePdMacRowStatus ((*(tMacAddr *) pMultiIndex->pIndex[0].
                                        pOctetStrValue->pu1_OctetList),
                                       pMultiData->i4_SLongValue));

}

INT4
FsPoePdMacRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2FsPoePdMacRowStatus (pu4Error,
                                          (*(tMacAddr *) pMultiIndex->pIndex[0].
                                           pOctetStrValue->pu1_OctetList),
                                          pMultiData->i4_SLongValue));

}

INT4
FsPoeMacTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                  tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsPoeMacTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
