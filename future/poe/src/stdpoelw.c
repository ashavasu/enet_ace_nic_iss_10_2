/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdpoelw.c,v 1.9 2014/01/25 13:55:50 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
#include "poehdrs.h"
#include "poecli.h"
#include "poenp.h"

/****************************************************************************
 * Function    :  PoeSnmpLowValidatePortIndex
 * Input       :  i4Dot3afGroupIndex i4Dot3afPortIndex 
 *
 * Output      :  None
 * Returns     :  POE_SUCCESS / POE_FAILURE
 *****************************************************************************/
INT4
PoeSnmpLowValidatePortIndex (INT4 i4Dot3afGroupIndex, INT4 i4Dot3afPortIndex)
{
    tPoePsePortEntry   *pPortEntry;

    /* Group Index in not supported . Its always 1 */
    if (i4Dot3afGroupIndex != 1)
    {
        return POE_FAILURE;
    }

    if (i4Dot3afPortIndex > POE_MAX_PORTS)
    {
        return POE_FAILURE;
    }
    PoeGetPortEntry ((UINT2) i4Dot3afPortIndex, &pPortEntry);

    if (pPortEntry == NULL)
    {
        return POE_FAILURE;
    }
    return POE_SUCCESS;
}

/****************************************************************************
 * Function    :  PoeSnmpLowGetFirstValidPortIndex
 * Input       :  None
 *
 * Output      :  pi4Dot3afFirstIndex (PortIndex)
 * Returns     :  POE_SUCCESS / POE_FAILURE
 *****************************************************************************/

INT4
PoeSnmpLowGetFirstValidPortIndex (INT4 *pi4Dot3afFirstIndex)
{
    tPoePsePortEntry   *pPortEntry;

    if (PoeGetNextPortEntry (NULL, &pPortEntry) != POE_SUCCESS)
    {
        return POE_FAILURE;
    }
    *pi4Dot3afFirstIndex = pPortEntry->u2PsePortIndex;
    return POE_SUCCESS;
}

/****************************************************************************
 * Function    :  PoeSnmpLowGetNextValidPortIndex
 * Input       :  i4Dot3afIndex (PortIndex)
 *
 * Output      :  pi4Dot3afNextIndex (PortIndex)
 * Returns     :  POE_SUCCESS / POE_FAILURE
 *****************************************************************************/

INT4
PoeSnmpLowGetNextValidPortIndex (INT4 i4Dot3afIndex, INT4 *pi4Dot3afNextIndex)
{
    tPoePsePortEntry   *pPortEntry;
    tPoePsePortEntry   *pNextPortEntry;

    if (PoeGetPortEntry (i4Dot3afIndex, &pPortEntry) != POE_SUCCESS)
    {
        return POE_FAILURE;
    }
    if (PoeGetNextPortEntry (pPortEntry, &pNextPortEntry) != POE_SUCCESS)
    {
        return POE_FAILURE;
    }
    *pi4Dot3afNextIndex = (INT4) pNextPortEntry->u2PsePortIndex;
    return POE_SUCCESS;
}

/****************************************************************************
 * Function    :  PoeSnmpLowValidateMainPseGroupIndex
 * Input       :  i4Dot3afMainPseIndex (GroupIndex)
 *
 * Output      :  None
 * Returns     :  POE_SUCCESS / POE_FAILURE
 *****************************************************************************/

INT4
PoeSnmpLowValidateMainPseGroupIndex (INT4 i4Dot3afMainPseIndex)
{
    tPoeMainPseEntry   *pPseEntry;

    if ((i4Dot3afMainPseIndex < POE_MIN_PS) ||
        (i4Dot3afMainPseIndex > POE_MAX_PS))

    {
        return POE_FAILURE;
    }
    if (PoeGetMainPseEntry ((UINT2) i4Dot3afMainPseIndex, &pPseEntry)
        != POE_SUCCESS)
    {
        return POE_FAILURE;
    }
    return POE_SUCCESS;
}

/****************************************************************************
 * Function    :  PoeSnmpLowGetFirstValidPseGroupIndex
 * Input       :  None
 *
 * Output      :  pi4Dot3afFirstIndex (GroupIndex)
 * Returns     :  POE_SUCCESS / POE_FAILURE
 *****************************************************************************/

INT4
PoeSnmpLowGetFirstValidPseGroupIndex (INT4 *pi4Dot3afFirstIndex)
{
    tPoeMainPseEntry   *pPseEntry;

    if (PoeGetNextMainPseEntry (NULL, &pPseEntry) != POE_SUCCESS)
    {
        return POE_FAILURE;
    }
    *pi4Dot3afFirstIndex = pPseEntry->u2MainPseGroupIndex;
    return POE_SUCCESS;
}

/****************************************************************************
 * Function    :  PoeSnmpLowGetNextValidPseGroupIndex
 * Input       :  i4Dot3afIndex (PortIndex)
 *
 * Output      :  pi4Dot3afNextIndex (PortIndex)
 * Returns     :  POE_SUCCESS / POE_FAILURE
 *****************************************************************************/

INT4
PoeSnmpLowGetNextValidPseGroupIndex (INT4 i4Dot3afIndex,
                                     INT4 *pi4Dot3afNextIndex)
{
    tPoeMainPseEntry   *pPseEntry;
    tPoeMainPseEntry   *pNextPseEntry;

    if (PoeGetMainPseEntry (i4Dot3afIndex, &pPseEntry) != POE_SUCCESS)
    {
        return POE_FAILURE;
    }
    if (PoeGetNextMainPseEntry (pPseEntry, &pNextPseEntry) != POE_SUCCESS)
    {
        return POE_FAILURE;
    }
    *pi4Dot3afNextIndex = (INT4) pNextPseEntry->u2MainPseGroupIndex;
    return POE_SUCCESS;
}

/* LOW LEVEL Routines for Table : PethPsePortTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePethPsePortTable
 Input       :  The Indices
                PethPsePortGroupIndex
                PethPsePortIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePethPsePortTable (INT4 i4PethPsePortGroupIndex,
                                          INT4 i4PethPsePortIndex)
{
    if (POE_IS_SHUTDOWN ())
    {
        POE_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                 "POE should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }

    if (PoeSnmpLowValidatePortIndex (i4PethPsePortGroupIndex,
                                     i4PethPsePortIndex) == POE_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPethPsePortTable
 Input       :  The Indices
                PethPsePortGroupIndex
                PethPsePortIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPethPsePortTable (INT4 *pi4PethPsePortGroupIndex,
                                  INT4 *pi4PethPsePortIndex)
{
    *pi4PethPsePortGroupIndex = 1;

    if (POE_IS_SHUTDOWN ())
    {
        POE_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                 "POE should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }

    if (PoeSnmpLowGetFirstValidPortIndex (pi4PethPsePortIndex) == POE_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhGetNextIndexPethPsePortTable
 Input       :  The Indices
                PethPsePortGroupIndex
                nextPethPsePortGroupIndex
                PethPsePortIndex
                nextPethPsePortIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPethPsePortTable (INT4 i4PethPsePortGroupIndex,
                                 INT4 *pi4NextPethPsePortGroupIndex,
                                 INT4 i4PethPsePortIndex,
                                 INT4 *pi4NextPethPsePortIndex)
{
    i4PethPsePortGroupIndex = 1;
    *pi4NextPethPsePortGroupIndex = 1;

    UNUSED_PARAM (i4PethPsePortGroupIndex);

    if (POE_IS_SHUTDOWN ())
    {
        POE_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                 "POE should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }

    if (PoeSnmpLowGetNextValidPortIndex (i4PethPsePortIndex,
                                         pi4NextPethPsePortIndex) ==
        POE_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPethPsePortAdminEnable
 Input       :  The Indices
                PethPsePortGroupIndex
                PethPsePortIndex

                The Object 
                retValPethPsePortAdminEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPethPsePortAdminEnable (INT4 i4PethPsePortGroupIndex,
                              INT4 i4PethPsePortIndex,
                              INT4 *pi4RetValPethPsePortAdminEnable)
{
    tPoePsePortEntry   *pPortEntry = NULL;

    if (POE_IS_SHUTDOWN ())
    {
        POE_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                 "POE should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    if (PoeSnmpLowValidatePortIndex (i4PethPsePortGroupIndex,
                                     i4PethPsePortIndex) == POE_SUCCESS)
    {
        PoeGetPortEntry ((UINT2) i4PethPsePortIndex, &pPortEntry);

        if (pPortEntry == NULL)
        {
            return SNMP_FAILURE;
        }
        *pi4RetValPethPsePortAdminEnable = pPortEntry->PsePortAdminEnable;

        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhGetPethPsePortPowerPairsControlAbility
 Input       :  The Indices
                PethPsePortGroupIndex
                PethPsePortIndex

                The Object 
                retValPethPsePortPowerPairsControlAbility
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPethPsePortPowerPairsControlAbility (INT4 i4PethPsePortGroupIndex,
                                           INT4 i4PethPsePortIndex,
                                           INT4
                                           *pi4RetValPethPsePortPowerPairsControlAbility)
{
    UNUSED_PARAM (i4PethPsePortGroupIndex);
    UNUSED_PARAM (i4PethPsePortIndex);
    UNUSED_PARAM (pi4RetValPethPsePortPowerPairsControlAbility);
    /*Currently not supported */
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetPethPsePortPowerPairs
 Input       :  The Indices
                PethPsePortGroupIndex
                PethPsePortIndex

                The Object 
                retValPethPsePortPowerPairs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPethPsePortPowerPairs (INT4 i4PethPsePortGroupIndex,
                             INT4 i4PethPsePortIndex,
                             INT4 *pi4RetValPethPsePortPowerPairs)
{
    UNUSED_PARAM (i4PethPsePortGroupIndex);
    UNUSED_PARAM (i4PethPsePortIndex);
    UNUSED_PARAM (pi4RetValPethPsePortPowerPairs);
    /*Currently not supported */
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetPethPsePortDetectionStatus
 Input       :  The Indices
                PethPsePortGroupIndex
                PethPsePortIndex

                The Object 
                retValPethPsePortDetectionStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPethPsePortDetectionStatus (INT4 i4PethPsePortGroupIndex,
                                  INT4 i4PethPsePortIndex,
                                  INT4 *pi4RetValPethPsePortDetectionStatus)
{
    tPoePsePortEntry   *pPortEntry = NULL;

    if (POE_IS_SHUTDOWN ())
    {
        POE_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                 "POE should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    if (PoeSnmpLowValidatePortIndex (i4PethPsePortGroupIndex,
                                     i4PethPsePortIndex) == POE_SUCCESS)
    {
        PoeGetPortEntry ((UINT2) i4PethPsePortIndex, &pPortEntry);

        if (pPortEntry == NULL)
        {
            return SNMP_FAILURE;
        }
        *pi4RetValPethPsePortDetectionStatus =
            pPortEntry->PsePortDetectionStatus;

        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhGetPethPsePortPowerPriority
 Input       :  The Indices
                PethPsePortGroupIndex
                PethPsePortIndex

                The Object 
                retValPethPsePortPowerPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPethPsePortPowerPriority (INT4 i4PethPsePortGroupIndex,
                                INT4 i4PethPsePortIndex,
                                INT4 *pi4RetValPethPsePortPowerPriority)
{
    tPoePsePortEntry   *pPortEntry = NULL;

    if (POE_IS_SHUTDOWN ())
    {
        POE_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                 "POE should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    if (PoeSnmpLowValidatePortIndex (i4PethPsePortGroupIndex,
                                     i4PethPsePortIndex) == POE_SUCCESS)
    {
        PoeGetPortEntry ((UINT2) i4PethPsePortIndex, &pPortEntry);

        if (pPortEntry == NULL)
        {
            return SNMP_FAILURE;
        }
        *pi4RetValPethPsePortPowerPriority = pPortEntry->PsePortPowerPriority;

        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhGetPethPsePortMPSAbsentCounter
 Input       :  The Indices
                PethPsePortGroupIndex
                PethPsePortIndex

                The Object 
                retValPethPsePortMPSAbsentCounter
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPethPsePortMPSAbsentCounter (INT4 i4PethPsePortGroupIndex,
                                   INT4 i4PethPsePortIndex,
                                   UINT4 *pu4RetValPethPsePortMPSAbsentCounter)
{
    UNUSED_PARAM (i4PethPsePortGroupIndex);
    UNUSED_PARAM (i4PethPsePortIndex);
    UNUSED_PARAM (pu4RetValPethPsePortMPSAbsentCounter);
    /*Currently not supported */
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetPethPsePortType
 Input       :  The Indices
                PethPsePortGroupIndex
                PethPsePortIndex

                The Object 
                retValPethPsePortType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPethPsePortType (INT4 i4PethPsePortGroupIndex,
                       INT4 i4PethPsePortIndex,
                       tSNMP_OCTET_STRING_TYPE * pRetValPethPsePortType)
{
    UNUSED_PARAM (i4PethPsePortGroupIndex);
    UNUSED_PARAM (i4PethPsePortIndex);
    UNUSED_PARAM (pRetValPethPsePortType);
    /*Currently not supported */
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetPethPsePortPowerClassifications
 Input       :  The Indices
                PethPsePortGroupIndex
                PethPsePortIndex

                The Object 
                retValPethPsePortPowerClassifications
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPethPsePortPowerClassifications (INT4 i4PethPsePortGroupIndex,
                                       INT4 i4PethPsePortIndex,
                                       INT4
                                       *pi4RetValPethPsePortPowerClassifications)
{
    tPoePsePortEntry   *pPortEntry = NULL;

    if (POE_IS_SHUTDOWN ())
    {
        POE_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                 "POE should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    if (PoeSnmpLowValidatePortIndex (i4PethPsePortGroupIndex,
                                     i4PethPsePortIndex) == POE_SUCCESS)
    {
        PoeGetPortEntry ((UINT2) i4PethPsePortIndex, &pPortEntry);

        if (pPortEntry == NULL)
        {
            return SNMP_FAILURE;
        }
        *pi4RetValPethPsePortPowerClassifications =
            pPortEntry->PsePortPowerClassification;

        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhGetPethPsePortInvalidSignatureCounter
 Input       :  The Indices
                PethPsePortGroupIndex
                PethPsePortIndex

                The Object 
                retValPethPsePortInvalidSignatureCounter
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPethPsePortInvalidSignatureCounter (INT4 i4PethPsePortGroupIndex,
                                          INT4 i4PethPsePortIndex,
                                          UINT4
                                          *pu4RetValPethPsePortInvalidSignatureCounter)
{
    UNUSED_PARAM (i4PethPsePortGroupIndex);
    UNUSED_PARAM (i4PethPsePortIndex);
    UNUSED_PARAM (pu4RetValPethPsePortInvalidSignatureCounter);
    /*Currently not supported */
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetPethPsePortPowerDeniedCounter
 Input       :  The Indices
                PethPsePortGroupIndex
                PethPsePortIndex

                The Object 
                retValPethPsePortPowerDeniedCounter
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPethPsePortPowerDeniedCounter (INT4 i4PethPsePortGroupIndex,
                                     INT4 i4PethPsePortIndex,
                                     UINT4
                                     *pu4RetValPethPsePortPowerDeniedCounter)
{
    UNUSED_PARAM (i4PethPsePortGroupIndex);
    UNUSED_PARAM (i4PethPsePortIndex);
    UNUSED_PARAM (pu4RetValPethPsePortPowerDeniedCounter);
    /*Currently not supported */
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetPethPsePortOverLoadCounter
 Input       :  The Indices
                PethPsePortGroupIndex
                PethPsePortIndex

                The Object 
                retValPethPsePortOverLoadCounter
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPethPsePortOverLoadCounter (INT4 i4PethPsePortGroupIndex,
                                  INT4 i4PethPsePortIndex,
                                  UINT4 *pu4RetValPethPsePortOverLoadCounter)
{
    UNUSED_PARAM (i4PethPsePortGroupIndex);
    UNUSED_PARAM (i4PethPsePortIndex);
    UNUSED_PARAM (pu4RetValPethPsePortOverLoadCounter);
    /*Currently not supported */
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetPethPsePortShortCounter
 Input       :  The Indices
                PethPsePortGroupIndex
                PethPsePortIndex

                The Object 
                retValPethPsePortShortCounter
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPethPsePortShortCounter (INT4 i4PethPsePortGroupIndex,
                               INT4 i4PethPsePortIndex,
                               UINT4 *pu4RetValPethPsePortShortCounter)
{
    UNUSED_PARAM (i4PethPsePortGroupIndex);
    UNUSED_PARAM (i4PethPsePortIndex);
    UNUSED_PARAM (pu4RetValPethPsePortShortCounter);
    /*Currently not supported */
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetPethPsePortAdminEnable
 Input       :  The Indices
                PethPsePortGroupIndex
                PethPsePortIndex

                The Object 
                setValPethPsePortAdminEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPethPsePortAdminEnable (INT4 i4PethPsePortGroupIndex,
                              INT4 i4PethPsePortIndex,
                              INT4 i4SetValPethPsePortAdminEnable)
{
    tPoePsePortEntry   *pPortEntry;
    INT4                i4ErrStatus = 0;
    INT4                i4CurrentAdminStatus = 0;

    if (POE_IS_SHUTDOWN ())
    {
        POE_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                 "POE should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }

    PoeGetPortEntry (i4PethPsePortIndex, &pPortEntry);
    if (pPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    nmhGetPethPsePortAdminEnable (i4PethPsePortGroupIndex,
                                  i4PethPsePortIndex, &i4CurrentAdminStatus);

    if (i4SetValPethPsePortAdminEnable == i4CurrentAdminStatus)
    {
        return SNMP_SUCCESS;
    }

#ifdef NPAPI_WANTED
    switch (i4SetValPethPsePortAdminEnable)
    {
        case POE_TRUE:

            if (pPortEntry->PsePortDetectionStatus != POE_DELIVERING_POWER)
            {
                /*Detecting and Classifying power */
                if (FsPoeDetectAndClassifyPD (pPortEntry, i4ErrStatus)
                    != FNP_SUCCESS)
                {
                    POE_TRC_ARG1 (CONTROL_PLANE_TRC,
                                  "\tPD detection and classification failed on Port - %d\n",
                                  i4PethPsePortIndex);
                    pPortEntry->PsePortDetectionStatus = POE_FAULT;
                    return SNMP_FAILURE;
                }
                else
                {
                    /*Getting the power availability status */
                    if (PoeGetPowerAvailableStatus
                        (pPortEntry->PsePortPowerClassification) == POE_SUCCESS)
                    {
                        /*Applying power on the powered Interface */
                        if (FsPoeApplyPowerOnPI (pPortEntry, i4ErrStatus) !=
                            FNP_SUCCESS)
                        {
                            POE_TRC_ARG1 (CONTROL_PLANE_TRC,
                                          "Apply Power failed on Port - %d\n",
                                          i4PethPsePortIndex);
                            pPortEntry->PsePortDetectionStatus = POE_FAULT;
                            return SNMP_FAILURE;
                        }
                        pPortEntry->PsePortDetectionStatus =
                            POE_DELIVERING_POWER;
                        PoeUpdatePowerAvailable (pPortEntry->
                                                 PsePortPowerClassification,
                                                 POE_SUBTRACT);
                    }
                    else
                    {
                        POE_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                                 " PSE has insufficient power !! Cannot Apply power\n");
                    }
                }
            }
            break;

        case POE_FALSE:

            /*Remove power from the powered Interface */
            if (pPortEntry->PsePortDetectionStatus == POE_DELIVERING_POWER)
            {
                if (FsPoeRemovePowerFromPI (pPortEntry, i4ErrStatus) !=
                    FNP_SUCCESS)
                {
                    POE_TRC_ARG1 (CONTROL_PLANE_TRC,
                                  "Power removal failed on Port - %d\n",
                                  i4PethPsePortIndex);
                    return SNMP_FAILURE;
                }
            }
            pPortEntry->PsePortDetectionStatus = POE_DISABLED;
            PoeUpdatePowerAvailable (pPortEntry->PsePortPowerClassification,
                                     POE_ADD);
            break;

        default:
            POE_TRC (CONTROL_PLANE_TRC, "unknown Admin State ...");
            return SNMP_FAILURE;
            break;

    }
#else
    UNUSED_PARAM (i4ErrStatus);
#endif

    pPortEntry->PsePortAdminEnable = (UINT4) i4SetValPethPsePortAdminEnable;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetPethPsePortPowerPairs
 Input       :  The Indices
                PethPsePortGroupIndex
                PethPsePortIndex

                The Object 
                setValPethPsePortPowerPairs
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPethPsePortPowerPairs (INT4 i4PethPsePortGroupIndex,
                             INT4 i4PethPsePortIndex,
                             INT4 i4SetValPethPsePortPowerPairs)
{
    UNUSED_PARAM (i4PethPsePortGroupIndex);
    UNUSED_PARAM (i4PethPsePortIndex);
    UNUSED_PARAM (i4SetValPethPsePortPowerPairs);
    /*Currently not supported */
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetPethPsePortPowerPriority
 Input       :  The Indices
                PethPsePortGroupIndex
                PethPsePortIndex

                The Object 
                setValPethPsePortPowerPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPethPsePortPowerPriority (INT4 i4PethPsePortGroupIndex,
                                INT4 i4PethPsePortIndex,
                                INT4 i4SetValPethPsePortPowerPriority)
{
    tPoePsePortEntry   *pPortEntry;
    tPoePsePortEntry   *pPortEntryRemove;
    INT4                i4ErrStatus = 0;
    UINT2               u2PortIndex;
    INT4                i4PoeSuccess = POE_FALSE;

    UNUSED_PARAM (i4PethPsePortGroupIndex);

    if (POE_IS_SHUTDOWN ())
    {
        POE_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                 "POE should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }

    PoeGetPortEntry (i4PethPsePortIndex, &pPortEntry);
    if (pPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

#ifdef NPAPI_WANTED
    switch (i4SetValPethPsePortPowerPriority)
    {
        case CRITICAL:
        case HIGH:
            if (pPortEntry->PsePortDetectionStatus != POE_DELIVERING_POWER &&
                pPortEntry->PsePortAdminEnable == POE_TRUE)
            {
                /*Detecting and Classifying power */
                if (FsPoeDetectAndClassifyPD (pPortEntry, i4ErrStatus) !=
                    FNP_SUCCESS)
                {
                    POE_TRC_ARG1 (CONTROL_PLANE_TRC,
                                  "\tPD detection and classification failed on Port - %d\n",
                                  i4PethPsePortIndex);
                    pPortEntry->PsePortDetectionStatus = POE_FAULT;
                    return SNMP_FAILURE;
                }
                else
                {
                    /*Getting the power availability status */
                    if (PoeGetPowerAvailableStatus
                        (pPortEntry->PsePortPowerClassification) == POE_SUCCESS)
                    {
                        /*Applying power on the powered Interface */
                        if (FsPoeApplyPowerOnPI (pPortEntry, i4ErrStatus) !=
                            FNP_SUCCESS)
                        {
                            POE_TRC_ARG1 (CONTROL_PLANE_TRC,
                                          "\tApply Power failed on Port - %d\n",
                                          i4PethPsePortIndex);
                            pPortEntry->PsePortDetectionStatus = POE_FAULT;
                            return SNMP_FAILURE;
                        }
                        pPortEntry->PsePortDetectionStatus =
                            POE_DELIVERING_POWER;
                        PoeUpdatePowerAvailable (pPortEntry->
                                                 PsePortPowerClassification,
                                                 POE_SUBTRACT);
                        /*i4PoeSuccess = POE_TRUE; */
                    }
                    else
                    {
                        POE_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                                 "Insufficient power !! Removing power from low priority PI(s)\n");
                        /*Getting port entries one by one */
                        for (u2PortIndex = 1; u2PortIndex <= POE_MAX_PORTS;
                             u2PortIndex++)
                        {
                            PoeGetPortEntry (u2PortIndex, &pPortEntryRemove);
                            if (pPortEntryRemove == NULL)
                            {
                                continue;
                            }
                            /* Checking if the port priority is low */
                            if (pPortEntryRemove->PsePortPowerPriority == LOW &&
                                pPortEntryRemove->PsePortDetectionStatus ==
                                POE_DELIVERING_POWER)
                            {
                                /* Calling Power Removal API for the PoE enabled low 
                                 * priority port*/
                                if (FsPoeRemovePowerFromPI
                                    (pPortEntryRemove,
                                     i4ErrStatus) != FNP_SUCCESS)
                                {
                                    POE_TRC_ARG1 (CONTROL_PLANE_TRC,
                                                  "\tCalling Remove Power API on Port - %d Failed!!\n",
                                                  u2PortIndex);
                                }
                                else
                                {
                                    pPortEntryRemove->PsePortDetectionStatus =
                                        POE_DISABLED;
                                    PoeUpdatePowerAvailable (pPortEntryRemove->
                                                             PsePortPowerClassification,
                                                             POE_ADD);
                                }
                            }
                            /* Getting the power availability status to see if 
                             * power is available*/
                            if (PoeGetPowerAvailableStatus
                                (pPortEntry->PsePortPowerClassification) ==
                                POE_SUCCESS)
                            {
                                /*Applying power on the powered Interface */
                                if (FsPoeApplyPowerOnPI
                                    (pPortEntry, i4ErrStatus) != FNP_SUCCESS)
                                {
                                    POE_TRC_ARG1 (CONTROL_PLANE_TRC,
                                                  "\tApply Power failed on Port - %d\n",
                                                  i4PethPsePortIndex);
                                    pPortEntry->PsePortDetectionStatus =
                                        POE_FAULT;
                                    return SNMP_FAILURE;
                                }
                                else
                                {
                                    pPortEntry->PsePortDetectionStatus =
                                        POE_DELIVERING_POWER;
                                    PoeUpdatePowerAvailable (pPortEntry->
                                                             PsePortPowerClassification,
                                                             POE_SUBTRACT);
                                    /*i4PoeSuccess = POE_TRUE; */
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            break;

        case LOW:
            i4PoeSuccess = POE_TRUE;
            break;

        default:
            POE_TRC (CONTROL_PLANE_TRC, "unknown Admin State ...");
            return SNMP_FAILURE;
            break;

    }

    if (pPortEntry->PsePortDetectionStatus == POE_DELIVERING_POWER &&
        pPortEntry->PsePortAdminEnable == POE_TRUE)
    {
        i4PoeSuccess = POE_TRUE;
    }

    if (i4PoeSuccess == POE_TRUE)
    {
        pPortEntry->PsePortPowerPriority =
            (UINT4) i4SetValPethPsePortPowerPriority;
    }
    else
    {
        CLI_SET_ERR (CLI_POE_PORT_PRIORITY_ERR);
        return SNMP_FAILURE;
    }
#else
    pPortEntry->PsePortPowerPriority = (UINT4) i4SetValPethPsePortPowerPriority;
    UNUSED_PARAM (i4PoeSuccess);
    UNUSED_PARAM (i4ErrStatus);
    UNUSED_PARAM (u2PortIndex);
    UNUSED_PARAM (pPortEntryRemove);
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetPethPsePortType
 Input       :  The Indices
                PethPsePortGroupIndex
                PethPsePortIndex

                The Object 
                setValPethPsePortType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPethPsePortType (INT4 i4PethPsePortGroupIndex,
                       INT4 i4PethPsePortIndex,
                       tSNMP_OCTET_STRING_TYPE * pSetValPethPsePortType)
{
    UNUSED_PARAM (i4PethPsePortGroupIndex);
    UNUSED_PARAM (i4PethPsePortIndex);
    UNUSED_PARAM (pSetValPethPsePortType);
    /*Currently not supported */
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2PethPsePortAdminEnable
 Input       :  The Indices
                PethPsePortGroupIndex
                PethPsePortIndex

                The Object 
                testValPethPsePortAdminEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PethPsePortAdminEnable (UINT4 *pu4ErrorCode,
                                 INT4 i4PethPsePortGroupIndex,
                                 INT4 i4PethPsePortIndex,
                                 INT4 i4TestValPethPsePortAdminEnable)
{
    if (PoeSnmpLowValidatePortIndex (i4PethPsePortGroupIndex,
                                     i4PethPsePortIndex) != POE_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        return SNMP_FAILURE;
    }

    if (i4TestValPethPsePortAdminEnable != POE_TRUE &&
        i4TestValPethPsePortAdminEnable != POE_FALSE)
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValPethPsePortAdminEnable == POE_TRUE) ||
        (i4TestValPethPsePortAdminEnable == POE_FALSE))
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2PethPsePortPowerPairs
 Input       :  The Indices
                PethPsePortGroupIndex
                PethPsePortIndex

                The Object 
                testValPethPsePortPowerPairs
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PethPsePortPowerPairs (UINT4 *pu4ErrorCode,
                                INT4 i4PethPsePortGroupIndex,
                                INT4 i4PethPsePortIndex,
                                INT4 i4TestValPethPsePortPowerPairs)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4PethPsePortGroupIndex);
    UNUSED_PARAM (i4PethPsePortIndex);
    UNUSED_PARAM (i4TestValPethPsePortPowerPairs);
    /*Currently not supported */
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2PethPsePortPowerPriority
 Input       :  The Indices
                PethPsePortGroupIndex
                PethPsePortIndex

                The Object 
                testValPethPsePortPowerPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PethPsePortPowerPriority (UINT4 *pu4ErrorCode,
                                   INT4 i4PethPsePortGroupIndex,
                                   INT4 i4PethPsePortIndex,
                                   INT4 i4TestValPethPsePortPowerPriority)
{
    tPoePsePortEntry   *pPortEntry;

    if (PoeSnmpLowValidatePortIndex (i4PethPsePortGroupIndex,
                                     i4PethPsePortIndex) != POE_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    PoeGetPortEntry ((UINT2) i4PethPsePortIndex, &pPortEntry);
    if (pPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    /* Checking if PoE is enabled on the port */
    if (pPortEntry->PsePortAdminEnable == POE_FALSE)
    {
        if (gPoeInfo.u2PoeMacAddrsProcess == POE_FALSE)
        {
            POE_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                     "POE should be enabled on port before setting priority !!\n");
            CLI_SET_ERR (CLI_POE_PORT_ENABLE_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        /* Setting the gPoeInfo.u2PoeMacAddrsProcess to false to indicate
         * to the PoE module that MAC processing is finished*/
        gPoeInfo.u2PoeMacAddrsProcess = POE_FALSE;
    }

    if (i4TestValPethPsePortPowerPriority != CRITICAL &&
        i4TestValPethPsePortPowerPriority != HIGH &&
        i4TestValPethPsePortPowerPriority != LOW)
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (i4TestValPethPsePortPowerPriority == CRITICAL ||
        i4TestValPethPsePortPowerPriority == HIGH ||
        i4TestValPethPsePortPowerPriority == LOW)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2PethPsePortType
 Input       :  The Indices
                PethPsePortGroupIndex
                PethPsePortIndex

                The Object 
                testValPethPsePortType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PethPsePortType (UINT4 *pu4ErrorCode,
                          INT4 i4PethPsePortGroupIndex,
                          INT4 i4PethPsePortIndex,
                          tSNMP_OCTET_STRING_TYPE * pTestValPethPsePortType)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4PethPsePortGroupIndex);
    UNUSED_PARAM (i4PethPsePortIndex);
    UNUSED_PARAM (pTestValPethPsePortType);
    /*Currently not supported */
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2PethPsePortTable
 Input       :  The Indices
                PethPsePortGroupIndex
                PethPsePortIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2PethPsePortTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : PethMainPseTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePethMainPseTable
 Input       :  The Indices
                PethMainPseGroupIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePethMainPseTable (INT4 i4PethMainPseGroupIndex)
{
    if (POE_IS_SHUTDOWN ())
    {
        POE_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                 "POE should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }

    if (PoeSnmpLowValidateMainPseGroupIndex (i4PethMainPseGroupIndex)
        == POE_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPethMainPseTable
 Input       :  The Indices
                PethMainPseGroupIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPethMainPseTable (INT4 *pi4PethMainPseGroupIndex)
{
    if (POE_IS_SHUTDOWN ())
    {
        POE_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                 "POE should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }

    if (PoeSnmpLowGetFirstValidPseGroupIndex (pi4PethMainPseGroupIndex) ==
        POE_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhGetNextIndexPethMainPseTable
 Input       :  The Indices
                PethMainPseGroupIndex
                nextPethMainPseGroupIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPethMainPseTable (INT4 i4PethMainPseGroupIndex,
                                 INT4 *pi4NextPethMainPseGroupIndex)
{
    if (POE_IS_SHUTDOWN ())
    {
        POE_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                 "POE should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }

    if (PoeSnmpLowGetNextValidPseGroupIndex (i4PethMainPseGroupIndex,
                                             pi4NextPethMainPseGroupIndex) ==
        POE_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPethMainPsePower
 Input       :  The Indices
                PethMainPseGroupIndex

                The Object 
                retValPethMainPsePower
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPethMainPsePower (INT4 i4PethMainPseGroupIndex,
                        UINT4 *pu4RetValPethMainPsePower)
{
    tPoeMainPseEntry   *pPseEntry = NULL;

    if (POE_IS_SHUTDOWN ())
    {
        POE_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                 "POE should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    if (PoeSnmpLowValidateMainPseGroupIndex (i4PethMainPseGroupIndex)
        == POE_SUCCESS)
    {
        PoeGetMainPseEntry ((UINT2) i4PethMainPseGroupIndex, &pPseEntry);
        if (pPseEntry == NULL)
        {
            return SNMP_FAILURE;
        }
        *pu4RetValPethMainPsePower = pPseEntry->u4MainPsePower;

        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhGetPethMainPseOperStatus
 Input       :  The Indices
                PethMainPseGroupIndex

                The Object 
                retValPethMainPseOperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPethMainPseOperStatus (INT4 i4PethMainPseGroupIndex,
                             INT4 *pi4RetValPethMainPseOperStatus)
{
    tPoeMainPseEntry   *pPseEntry = NULL;

    if (POE_IS_SHUTDOWN ())
    {
        POE_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                 "POE should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    if (PoeSnmpLowValidateMainPseGroupIndex (i4PethMainPseGroupIndex)
        == POE_SUCCESS)
    {
        PoeGetMainPseEntry ((UINT2) i4PethMainPseGroupIndex, &pPseEntry);

        if (pPseEntry == NULL)
        {
            return SNMP_FAILURE;
        }
        *pi4RetValPethMainPseOperStatus = pPseEntry->MainPseOperStatus;

        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhGetPethMainPseConsumptionPower
 Input       :  The Indices
                PethMainPseGroupIndex

                The Object 
                retValPethMainPseConsumptionPower
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPethMainPseConsumptionPower (INT4 i4PethMainPseGroupIndex,
                                   UINT4 *pu4RetValPethMainPseConsumptionPower)
{
    tPoeMainPseEntry   *pPseEntry = NULL;

    if (POE_IS_SHUTDOWN ())
    {
        POE_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                 "POE should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    if (PoeSnmpLowValidateMainPseGroupIndex (i4PethMainPseGroupIndex)
        == POE_SUCCESS)
    {
        PoeGetMainPseEntry ((UINT2) i4PethMainPseGroupIndex, &pPseEntry);
        if (pPseEntry == NULL)
        {
            return SNMP_FAILURE;
        }

        *pu4RetValPethMainPseConsumptionPower =
            pPseEntry->u4MainPseConsumptionPower;

        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
    return POE_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetPethMainPseUsageThreshold
 Input       :  The Indices
                PethMainPseGroupIndex

                The Object 
                retValPethMainPseUsageThreshold
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPethMainPseUsageThreshold (INT4 i4PethMainPseGroupIndex,
                                 INT4 *pi4RetValPethMainPseUsageThreshold)
{
    UNUSED_PARAM (i4PethMainPseGroupIndex);
    UNUSED_PARAM (pi4RetValPethMainPseUsageThreshold);
    /*Currently not supported */
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetPethMainPseUsageThreshold
 Input       :  The Indices
                PethMainPseGroupIndex

                The Object 
                setValPethMainPseUsageThreshold
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPethMainPseUsageThreshold (INT4 i4PethMainPseGroupIndex,
                                 INT4 i4SetValPethMainPseUsageThreshold)
{
    UNUSED_PARAM (i4PethMainPseGroupIndex);
    UNUSED_PARAM (i4SetValPethMainPseUsageThreshold);
    /*Currently not supported */
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2PethMainPseUsageThreshold
 Input       :  The Indices
                PethMainPseGroupIndex

                The Object 
                testValPethMainPseUsageThreshold
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PethMainPseUsageThreshold (UINT4 *pu4ErrorCode,
                                    INT4 i4PethMainPseGroupIndex,
                                    INT4 i4TestValPethMainPseUsageThreshold)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4PethMainPseGroupIndex);
    UNUSED_PARAM (i4TestValPethMainPseUsageThreshold);
    /*Currently not supported */
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2PethMainPseTable
 Input       :  The Indices
                PethMainPseGroupIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2PethMainPseTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : PethNotificationControlTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePethNotificationControlTable
 Input       :  The Indices
                PethNotificationControlGroupIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePethNotificationControlTable (INT4
                                                      i4PethNotificationControlGroupIndex)
{
    UNUSED_PARAM (i4PethNotificationControlGroupIndex);
    /*Currently not supported */
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPethNotificationControlTable
 Input       :  The Indices
                PethNotificationControlGroupIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPethNotificationControlTable (INT4
                                              *pi4PethNotificationControlGroupIndex)
{
    UNUSED_PARAM (pi4PethNotificationControlGroupIndex);
    /*Currently not supported */
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexPethNotificationControlTable
 Input       :  The Indices
                PethNotificationControlGroupIndex
                nextPethNotificationControlGroupIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPethNotificationControlTable (INT4
                                             i4PethNotificationControlGroupIndex,
                                             INT4
                                             *pi4NextPethNotificationControlGroupIndex)
{
    UNUSED_PARAM (i4PethNotificationControlGroupIndex);
    UNUSED_PARAM (pi4NextPethNotificationControlGroupIndex);
    /*Currently not supported */
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPethNotificationControlEnable
 Input       :  The Indices
                PethNotificationControlGroupIndex

                The Object 
                retValPethNotificationControlEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPethNotificationControlEnable (INT4 i4PethNotificationControlGroupIndex,
                                     INT4
                                     *pi4RetValPethNotificationControlEnable)
{
    UNUSED_PARAM (i4PethNotificationControlGroupIndex);
    UNUSED_PARAM (pi4RetValPethNotificationControlEnable);
    /*Currently not supported */
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetPethNotificationControlEnable
 Input       :  The Indices
                PethNotificationControlGroupIndex

                The Object 
                setValPethNotificationControlEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPethNotificationControlEnable (INT4 i4PethNotificationControlGroupIndex,
                                     INT4 i4SetValPethNotificationControlEnable)
{
    UNUSED_PARAM (i4PethNotificationControlGroupIndex);
    UNUSED_PARAM (i4SetValPethNotificationControlEnable);
    /*Currently not supported */
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2PethNotificationControlEnable
 Input       :  The Indices
                PethNotificationControlGroupIndex

                The Object 
                testValPethNotificationControlEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PethNotificationControlEnable (UINT4 *pu4ErrorCode,
                                        INT4
                                        i4PethNotificationControlGroupIndex,
                                        INT4
                                        i4TestValPethNotificationControlEnable)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4PethNotificationControlGroupIndex);
    UNUSED_PARAM (i4TestValPethNotificationControlEnable);
    /*Currently not supported */
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2PethNotificationControlTable
 Input       :  The Indices
                PethNotificationControlGroupIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2PethNotificationControlTable (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
