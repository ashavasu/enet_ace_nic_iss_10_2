/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fspoelw.c,v 1.7 2009/09/29 15:57:10 prabuc Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
#include "poehdrs.h"
#include "poecli.h"

extern UINT4        stdpoe[7];
extern UINT4        fspoe[8];
extern UINT4        FsPoeGlobalAdminStatus[10];
PRIVATE VOID        PoeNotifyProtocolShutdownStatus (VOID);
/****************************************************************************
 Function    :  PoeSnmpLowValidateSuppAddress
 Input       :  FsPoePdMacAddress - PD's MAC address

 Output      :  None
 Returns     :  POE_SUCCESS or POE_FAILURE
****************************************************************************/

INT4
PoeSnmpLowValidateSuppAddress (tMacAddr FsPoePdMacAddress)
{
    tMacAddr            testAddr;

    MEMCPY (testAddr, FsPoePdMacAddress, POE_MAC_ADDR_SIZE);

    if (POE_IS_BROADCAST_ADDR (testAddr) == POE_TRUE)
    {
        POE_TRC (ALL_FAILURE_TRC,
                 " SNMPPROP: Broadcast address is not supported\n");
        return POE_FAILURE;
    }
    return POE_SUCCESS;
}

/****************************************************************************
 Function    :  PoeSnmpLowGetFirstValidPdMacAddress
 Input       :  None

 Output      :  This Routine Takes the argument FsPnacAuthSessionSuppAddress
                & stores the first Valid Supplicant's MAC address in the
                argument.
 Returns     :  POE_SUCCESS or POE_FAILURE
****************************************************************************/

INT4
PoeSnmpLowGetFirstValidPdMacAddress (tMacAddr FsPoePdMacAddress)
{
    tMacAddr            macAddr;

    if (POE_IS_SHUTDOWN ())
    {
        POE_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                 "POE should be started before accessing its MIB objects !!\n");
        return POE_FAILURE;
    }
    if (PoeGetFirstValidPdMacAddress (macAddr) == POE_SUCCESS)
    {
        MEMCPY (FsPoePdMacAddress, macAddr, POE_MAC_ADDR_SIZE);
        return POE_SUCCESS;
    }

    return POE_FAILURE;
}

/****************************************************************************
 Function    :  PoeSnmpLowGetNextValidPdMacAddress
 Input       :  FsPoePdMacAddress  - PD's MAC address

 Output      :  This Routine Takes the argument *pNextFsPoePdMacAddress
                & stores in it, the Valid PD's MAC address that occurs
                next to FsPoePdMacAddress.
 Returns     :  POE_SUCCESS or POE_FAILURE
****************************************************************************/

INT4
PoeSnmpLowGetNextValidPdMacAddress (tMacAddr FsPoePdMacAddress,
                                    tMacAddr * pNextFsPoePdMacAddress)
{
    tMacAddr            refAddr;
    tMacAddr            nextAddr;

    if (POE_IS_SHUTDOWN ())
    {
        POE_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                 "POE should be started before accessing its MIB objects !!\n");
        return POE_FAILURE;
    }
    MEMCPY (refAddr, FsPoePdMacAddress, POE_MAC_ADDR_SIZE);

    if (PoeGetNextValidPdMacAddress (refAddr, nextAddr) == POE_SUCCESS)
    {
        MEMCPY (pNextFsPoePdMacAddress, nextAddr, POE_MAC_ADDR_SIZE);
        return POE_SUCCESS;
    }

    return POE_FAILURE;
}

/****************************************************************************
 Function    :  PoeSnmpLowGetPdMacNode
 Input       :  FsPoePdMacAddress - PD's MAC address

 Output      :  This Routine Takes the argument *pPdMacNode  stores in it,
                the PD Mac node corresponding to the inputted PD's MAC address.
 Returns     :  POE_SUCCESS or POE_FAILURE
****************************************************************************/

INT4
PoeSnmpLowGetPdMacNode (tMacAddr FsPoePdMacAddress, tPoeMacEntry ** ppPdMacNode)
{
    tMacAddr            refAddr;

    MEMCPY (refAddr, FsPoePdMacAddress, POE_MAC_ADDR_SIZE);

    if (PoeGetPdMacTblEntryByMac (refAddr, ppPdMacNode) != POE_SUCCESS)
    {
        POE_TRC (ALL_FAILURE_TRC, " SNMPPROP: Getting tPoeMacEntry"
                 " by PD's MAC address failed\n");
        return POE_FAILURE;
    }

    return POE_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPoeGlobalAdminStatus
 Input       :  The Indices

                The Object 
                retValFsPoeGlobalAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPoeGlobalAdminStatus (INT4 *pi4RetValFsPoeGlobalAdminStatus)
{
    *pi4RetValFsPoeGlobalAdminStatus = (INT4) POE_SYSTEM_CONTROL;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsPoeGlobalAdminStatus
 Input       :  The Indices

                The Object 
                setValFsPoeGlobalAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPoeGlobalAdminStatus (INT4 i4SetValFsPoeGlobalAdminStatus)
{
    tPoeMainPseEntry   *pPseEntry;
    UINT2               u2PseIndex = 1;

    if (i4SetValFsPoeGlobalAdminStatus == (INT4) POE_SYSTEM_CONTROL)
    {
        POE_TRC (MGMT_TRC,
                 "SNMP: POE is already in the same System Control state\n");
        return SNMP_SUCCESS;
    }

    if (i4SetValFsPoeGlobalAdminStatus == POE_START)
    {
        if (gPoeInfo.u2PoeModInit == POE_FALSE)
        {
            if (PoeModuleInit () != POE_SUCCESS)
            {
                POE_TRC (INIT_SHUT_TRC, "Unable to Initialize PoE module\n");
                return POE_FAILURE;
            }
        }
        /* Currently only one group is supported so u2PseIndex is always 1 */
        if (PoeGetMainPseEntry (u2PseIndex, &pPseEntry) != POE_SUCCESS)
        {
            POE_TRC (INIT_SHUT_TRC, "Pse Entry could not be obtained\n");
            return POE_FAILURE;
        }
#ifdef NPAPI_WANTED
        /*Getting PowerSupply Status */
        if (FsPoeGetPseStatus () != FNP_SUCCESS)
        {
            POE_TRC (ALL_FAILURE_TRC,
                     "Pse Oper Status is not up !! cannot enable PoE module\n");
            pPseEntry->MainPseOperStatus = FAULTY;
        }
        else
        {
            pPseEntry->MainPseOperStatus = ON;
            /*Computing the entire power available */
            PoeGetTotalPowerAvailable ();
        }
#else
        UNUSED_PARAM (u2PseIndex);
        UNUSED_PARAM (pPseEntry);
#endif
    }
    else
    {
        if (PoeShutDown () != POE_SUCCESS)
        {
            POE_TRC (MGMT_TRC | ALL_FAILURE_TRC, "SNMP: POE Shutdown FAILED\n");
            return SNMP_FAILURE;
        }
        /* Notify MSR with the poe oids */
        PoeNotifyProtocolShutdownStatus ();
    }
    POE_SYSTEM_CONTROL = i4SetValFsPoeGlobalAdminStatus;
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsPoeGlobalAdminStatus
 Input       :  The Indices

                The Object 
                testValFsPoeGlobalAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPoeGlobalAdminStatus (UINT4 *pu4ErrorCode,
                                 INT4 i4TestValFsPoeGlobalAdminStatus)
{
    if ((i4TestValFsPoeGlobalAdminStatus == POE_START) ||
        (i4TestValFsPoeGlobalAdminStatus == POE_SHUTDOWN))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsPoeGlobalAdminStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPoeGlobalAdminStatus (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsPoeMacTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPoeMacTable
 Input       :  The Indices
                FsPoePdMacAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsPoeMacTable (tMacAddr FsPoePdMacAddress)
{
    if (POE_IS_SHUTDOWN ())
    {
        POE_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                 "POE should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    if (PoeSnmpLowValidateSuppAddress (FsPoePdMacAddress) != POE_SUCCESS)
    {
        CLI_SET_ERR (CLI_POE_MAC_ADDR_ERR);
        return SNMP_FAILURE;
    }
    else
    {
        return SNMP_SUCCESS;
    }
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPoeMacTable
 Input       :  The Indices
                FsPoePdMacAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsPoeMacTable (tMacAddr * pFsPoePdMacAddress)
{
    if (POE_IS_SHUTDOWN ())
    {
        POE_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                 "POE should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    if (PoeSnmpLowGetFirstValidPdMacAddress
        ((UINT1 *) pFsPoePdMacAddress) != POE_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    else
    {
        return SNMP_SUCCESS;
    }
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPoeMacTable
 Input       :  The Indices
                FsPoePdMacAddress
                nextFsPoePdMacAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsPoeMacTable (tMacAddr FsPoePdMacAddress,
                              tMacAddr * pNextFsPoePdMacAddress)
{
    if (POE_IS_SHUTDOWN ())
    {
        POE_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                 "POE should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    if (PoeSnmpLowGetNextValidPdMacAddress (FsPoePdMacAddress,
                                            pNextFsPoePdMacAddress)
        != POE_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    else
    {
        return SNMP_SUCCESS;
    }
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPoePdMacPort
 Input       :  The Indices
                FsPoePdMacAddress
                

                The Object 
                retValFsPoePdMacPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPoePdMacPort (tMacAddr FsPoePdMacAddress, INT4 *pi4RetValFsPoePdMacPort)
{
    tPoeMacEntry       *pPdMacNode = NULL;

    if (POE_IS_SHUTDOWN ())
    {
        POE_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                 "POE should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    if (PoeSnmpLowGetPdMacNode (FsPoePdMacAddress, &pPdMacNode) != POE_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPoePdMacPort = pPdMacNode->u4PsePortIndex;
        return SNMP_SUCCESS;
    }
}

/****************************************************************************
 Function    :  nmhGetFsPoePdMacRowStatus
 Input       :  The Indices
                FsPoePdMacAddress
                

                The Object 
                retValFsPoePdMacRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPoePdMacRowStatus (tMacAddr FsPoePdMacAddress,
                           INT4 *pi4RetValFsPoePdMacRowStatus)
{
    tPoeMacEntry       *pPdMacNode = NULL;

    if (POE_IS_SHUTDOWN ())
    {
        POE_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                 "POE should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    if (PoeSnmpLowGetPdMacNode (FsPoePdMacAddress, &pPdMacNode) != POE_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPoePdMacRowStatus = (INT4) pPdMacNode->u1RowStatus;
        return SNMP_SUCCESS;
    }

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsPoePdMacRowStatus
 Input       :  The Indices
                FsPoePdMacAddress
                

                The Object 
                setValFsPoePdMacRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPoePdMacRowStatus (tMacAddr FsPoePdMacAddress,
                           INT4 i4SetValFsPoePdMacRowStatus)
{
    UINT4               u4HashIndex = POE_INIT_VAL;
    tPoeMacEntry       *pPdMacNode = NULL;
    tPoePsePortEntry   *pPortEntry;

    /* The Mac learnt provides a way by which the ports can be 
     * prioritised so that when there is a need to remove power
     * from  low priority ports to provide for high priority ports, 
     * the ports through which valid PD mac address are learnt and 
     * are configured in the PD Mac table will not be not disturbed
     * for this purpose and power will not be removed from them */

    if (POE_IS_SHUTDOWN ())
    {
        POE_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                 "POE should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }

    switch (i4SetValFsPoePdMacRowStatus)
    {
        case CREATE_AND_GO:
        case CREATE_AND_WAIT:
            if (PoeSnmpLowCreatePdMacNode (FsPoePdMacAddress,
                                           &pPdMacNode) != POE_SUCCESS)
            {
                POE_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                         " SNMPPROP: PD Mac node creation Failed ...\n");
                return SNMP_FAILURE;
            }
            break;

        case ACTIVE:
            if (PoeSnmpLowGetPdMacNode (FsPoePdMacAddress,
                                        &pPdMacNode) != POE_SUCCESS)
            {

                POE_TRC (ALL_FAILURE_TRC,
                         " SNMPPROP: PD Mac node could not be obtained! \n");
                return SNMP_FAILURE;
            }
            /* Assuming Mac entry is learnt getting port entry of the port 
             * through which the Mac was learnt*/
            PoeGetPortEntry ((UINT2) pPdMacNode->u4PsePortIndex, &pPortEntry);
            if (pPortEntry == NULL)
            {
                POE_TRC (ALL_FAILURE_TRC,
                         "PD port entry for learnt Mac could not be obtained! \n");
                return SNMP_FAILURE;
            }
            /* Setting the port power priority to high */
            pPortEntry->PsePortPowerPriority = HIGH;
            break;

        case NOT_READY:
        case NOT_IN_SERVICE:
            if (PoeSnmpLowGetPdMacNode (FsPoePdMacAddress,
                                        &pPdMacNode) != POE_SUCCESS)
            {

                POE_TRC (ALL_FAILURE_TRC,
                         " SNMPPROP: PD Mac node could not be obtained! \n");
                return SNMP_FAILURE;
            }
            break;

        case DESTROY:
            if (PoeSnmpLowGetPdMacNode (FsPoePdMacAddress,
                                        &pPdMacNode) != POE_SUCCESS)
            {
                POE_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                         " SNMPPROP: No such Mac PD ...\n");
                return SNMP_FAILURE;
            }
            /* Ensuring the mac is learnt before getting the corresponding
             * port entry */
            if (pPdMacNode->u4PsePortIndex != 0)
            {
                /* Getting the corresponding port entry */
                PoeGetPortEntry ((UINT2) pPdMacNode->u4PsePortIndex,
                                 &pPortEntry);
                if (pPortEntry == NULL)
                {
                    POE_TRC (ALL_FAILURE_TRC,
                             "PD port entry for Mac could not be obtained! \n");
                    return SNMP_FAILURE;
                }
                /* Setting the port power priority to low */
                pPortEntry->PsePortPowerPriority = LOW;
            }
            /*Deleting the hash node corresponding to mac entry */
            PoeCalcPdMacTblIndex (FsPoePdMacAddress, &u4HashIndex);
            POE_HASH_DEL_NODE (POE_MACENTRY_TABLE, pPdMacNode, u4HashIndex);

            if (POE_MACENTRY_FREE_MEM_BLOCK (pPdMacNode) != POE_MEM_SUCCESS)
            {
                POE_TRC (DATA_PATH_TRC | ALL_FAILURE_TRC,
                         "POE: PD Mac Entry Memory Block Release FAILED\n");
                return POE_FAILURE;
            }

            POE_TRC (CONTROL_PLANE_TRC, "Mac PD entry Destroyed...");
            break;

        default:
            POE_TRC (CONTROL_PLANE_TRC, "unknown Row Status Value ...");
            return SNMP_FAILURE;
            break;
    }
    pPdMacNode->u1RowStatus = (UINT1) i4SetValFsPoePdMacRowStatus;
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsPoePdMacRowStatus
 Input       :  The Indices
                FsPoePdMacAddress
                

                The Object 
                testValFsPoePdMacRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPoePdMacRowStatus (UINT4 *pu4ErrorCode,
                              tMacAddr FsPoePdMacAddress,
                              INT4 i4TestValFsPoePdMacRowStatus)
{
    tPoeMacEntry       *pPdMacNode = NULL;

    if (POE_IS_SHUTDOWN ())
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
        POE_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                 "POE should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    switch (i4TestValFsPoePdMacRowStatus)
    {

        case ACTIVE:
            if (PoeSnmpLowGetPdMacNode (FsPoePdMacAddress,
                                        &pPdMacNode) != POE_SUCCESS)
            {

                *pu4ErrorCode = (UINT4) SNMP_ERR_NO_CREATION;
                POE_TRC (ALL_FAILURE_TRC,
                         " SNMPPROP: PD Mac node could not be obtained! \n");
                return SNMP_FAILURE;
            }

            /*Checking if Mac entry is learnt */
            if (pPdMacNode->u4PsePortIndex == 0)
            {
                POE_TRC (MGMT_TRC | ALL_FAILURE_TRC, "PD Mac not yet learnt\n");
                *pu4ErrorCode = (UINT4) SNMP_ERR_GEN_ERR;
                CLI_SET_ERR (CLI_POE_PD_MAC_LEARNT_ERR);
                return SNMP_FAILURE;
            }
            break;
        case NOT_IN_SERVICE:
        case NOT_READY:
        case DESTROY:
            if (PoeSnmpLowGetPdMacNode (FsPoePdMacAddress,
                                        &pPdMacNode) != POE_SUCCESS)
            {

                *pu4ErrorCode = (UINT4) SNMP_ERR_NO_CREATION;
                POE_TRC (ALL_FAILURE_TRC,
                         " SNMPPROP: PD Mac node could not be obtained! \n");
                CLI_SET_ERR (CLI_POE_NO_PD_MAC_ERR);
                return SNMP_FAILURE;
            }
            break;
        case CREATE_AND_GO:
        case CREATE_AND_WAIT:
            if (PoeSnmpLowGetPdMacNode (FsPoePdMacAddress,
                                        &pPdMacNode) == POE_SUCCESS)
            {
                POE_TRC (ALL_FAILURE_TRC,
                         " SNMPPROP: Entry already existing with this mac! \n");
                CLI_SET_ERR (CLI_POE_MAC_ADDR_EXIST_ERR);
                return SNMP_FAILURE;
            }
            break;

        default:

            *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
            POE_TRC (ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                     " SNMPPROP: Improper RowStatus value !!\n");
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsPoeMacTable
 Input       :  The Indices
                FsPoePdMacAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPoeMacTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  PoeNotifyProtocolShutdownStatus
 Input       :  None
 Output      :  None
 Returns     :  None
****************************************************************************/

VOID
PoeNotifyProtocolShutdownStatus (VOID)
{
#ifdef ISS_WANTED
    UINT1               au1ObjectOid[3][SNMP_MAX_OID_LENGTH];

    /* The oid list contains the list of Oids that has been registered 
     * for the protocol + the oid of the SystemControl object. */
    SNMPGetOidString (fspoe, (sizeof (fspoe) / sizeof (UINT4)),
                      au1ObjectOid[0]);
    SNMPGetOidString (stdpoe, (sizeof (stdpoe) / sizeof (UINT4)),
                      au1ObjectOid[1]);
    SNMPGetOidString (FsPoeGlobalAdminStatus,
                      (sizeof (FsPoeGlobalAdminStatus) / sizeof (UINT4)),
                      au1ObjectOid[2]);

    /* Send a notification to MSR to process the poe shutdown, with 
     * poe oids and its system control object 
     * As the protocol does not have MI support, invalid context id is sent */
    MSR_NOTIFY_PROTOCOL_SHUT (au1ObjectOid, 3, MSR_INVALID_CNTXT);
#endif
    return;
}
