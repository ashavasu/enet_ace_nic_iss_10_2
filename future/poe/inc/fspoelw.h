/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fspoelw.h,v 1.4 2008/08/20 15:23:39 iss Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPoeGlobalAdminStatus ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPoeGlobalAdminStatus ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPoeGlobalAdminStatus ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsPoeGlobalAdminStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsPoeMacTable. */
INT1
nmhValidateIndexInstanceFsPoeMacTable ARG_LIST((tMacAddr ));

/* Proto Type for Low Level GET FIRST fn for FsPoeMacTable  */

INT1
nmhGetFirstIndexFsPoeMacTable ARG_LIST((tMacAddr *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPoeMacTable ARG_LIST((tMacAddr , tMacAddr * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPoePdMacPort ARG_LIST((tMacAddr  , INT4 *));

INT1
nmhGetFsPoePdMacRowStatus ARG_LIST((tMacAddr  , INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPoePdMacRowStatus ARG_LIST((tMacAddr  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPoePdMacRowStatus ARG_LIST((UINT4 *  ,tMacAddr  ,  INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsPoeMacTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
