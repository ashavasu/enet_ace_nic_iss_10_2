#ifndef _POEMACRO_H
#define _POEMACRO_H

/* $Id: poemacro.h,v 1.5 2011/02/15 04:47:16 siva Exp $ */
/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2001-2002                                          */
/*****************************************************************************/
/*    FILE  NAME            : poemacro.h                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : POWER OVER ETHERNET                            */
/*    MODULE NAME           : All modules in PoE                             */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 27 Jan 2005                                    */
/*    AUTHOR                : Fcsdi Team                                     */
/*    DESCRIPTION           : This file contains all macros used in          */
/*                            PoE module                                     */
/*---------------------------------------------------------------------------*/

/* Defining all global structure used in the POE module*/

extern tPoeInfo gPoeInfo;
extern tMacAddr gPoeBCastAddr;

 /* Event Definitions */
#define   POE_NO_WAIT          OSIX_NO_WAIT

#define   POE_CREATE_SEMAPHORE(au1Name, count, flags, psemid) \
             OsixCreateSem((au1Name), (count), (flags), (psemid))

#define   POE_DELETE_SEMAPHORE(au1Name, psemid) \
             OsixDeleteSem(SELF, (au1Name))

#define   POE_TAKE_PROTOCOL_SEMAPHORE()      \
          {                         \
           if (OsixTakeSem(SELF, POE_PROTOCOL_SEMAPHORE, OSIX_WAIT, 0) == OSIX_SUCCESS) \
           {\
                gPoeInfo.bProtocolLocked = POE_TRUE;  \
           }\
          }

#define   POE_RELEASE_PROTOCOL_SEMAPHORE()      \
          {  \
             if (OsixGiveSem(SELF, POE_PROTOCOL_SEMAPHORE) == OSIX_SUCCESS)  \
             {\
                gPoeInfo.bProtocolLocked = POE_FALSE;  \
             }\
          }

/* POE Semaphore related constants */
#define POE_PROTOCOL_SEMAPHORE                   ((const UINT1*)"POEPS")
#define POE_BINARY_SEM                           1
#define POE_SEM_FLAGS                            0 /* unused */

#define    POE_MIN_PS                  1
#define    POE_MIN_PORTS               1
#define    POE_MAX_MAC                 POE_MAX_PORTS
#define    POE_MAC_TABLE_SIZE          POE_MAX_PORTS
#define    POE_SUCCESS                 0
#define    POE_FAILURE                 -1
#define    POE_OSIX_SUCCESS            OSIX_SUCCESS
#define    POE_OSIX_FAILURE            OSIX_FAILURE 
#define    POE_TASK_NAME               "PoeT"
#define    POE_QUE                     "POEQ"
#define    POE_Q_NAME_SIZE             5
#define    POE_Q_DEPTH                 50
#define    POE_Q_MODE                  OSIX_LOCAL
#define    POE_SYSTEM_CONTROL          gPoeInfo.poeGlobalAdminStatus
#define    POE_PSEENTRY_MEMBLK_SIZE    sizeof(tPoeMainPseEntry)
#define    POE_PSEENTRY_MEMBLK_COUNT   POE_MAX_PS 
#define    POE_PSEENTRY_POOL_ID        gPoeInfo.PoePsePoolId
#define    POE_PORTENTRY_MEMBLK_SIZE   sizeof(tPoePsePortEntry)
#define    POE_PORTENTRY_MEMBLK_COUNT  POE_MAX_PORTS
#define    POE_PORTENTRY_POOL_ID       gPoeInfo.PoePortPoolId
#define    POE_MACENTRY_MEMBLK_SIZE    sizeof(tPoeMacEntry)
#define    POE_MACENTRY_MEMBLK_COUNT   POE_MAX_MAC
#define    POE_MACENTRY_POOL_ID        gPoeInfo.PoeMacPoolId
#define    POE_INPUTQ_MEMBLK_SIZE      sizeof(tPoeQMsg)
#define    POE_INPUTQ_MEMBLK_COUNT     POE_Q_DEPTH 
#define    POE_INPUTQ_POOL_ID          gPoeInfo.PoeInputQPoolId
#define    POE_SEMAPHORE_ID            gPoeInfo.PoeProtocolSemId
#define    POE_MEM_SUCCESS             MEM_SUCCESS
#define    POE_TRC_FLAG                gPoeInfo.u4TraceOption
#define    POE_MOD_NAME                ((const char *)"POE")
#define    POE_PORT_SLL                &(gPoeInfo.poePsePortList)
#define    POE_MAC_ADDR_SIZE           6 /* Bytes */
#define    POE_PD_MAC_ADDRS_TABLE      gPoeInfo.pPoeMacTable
#define    POE_INIT_VAL                0
#define    POE_NTOHS                   OSIX_NTOHS
#define    POE_MEMORY_TYPE             MEM_DEFAULT_MEMORY_TYPE
#define    POE_MEM_ERROR               999 
#define    POE_RECEIVE_EVENT           OsixReceiveEvent
#define    POE_INPUTQ_MSG_CNT          (gPoeInfo.u4InputQMsgCnt)
#define    POE_MACENTRY_TABLE          gPoeInfo.pPoeMacTable
#define    POE_ADD                     1
#define    POE_SUBTRACT                2

/*Added for CLI*/
#define    POE_PD_MAC_ADD              1
#define    POE_PD_MAC_DEL              2

/* Defining the power class of the PSE */
#define    CLASS_0_POWER               15.4
#define    CLASS_1_POWER               4
#define    CLASS_2_POWER               7
#define    CLASS_3_POWER               15.4
#define    CLASS_4_POWER               15.4

/* Power consumption (in watts) for various module types in the chassis */
#define    PWR_CONSUM_DOWN_LINK_MODULE 285
#define    PWR_CONSUM_UP_LINK_MODULE   180
#define    PWR_CONSUM_SUPRVISR_MODULE  120
#define    PWR_CONSUM_FAN_MODULE       16

/* Power available in a power supply module (25A *48V) */
#define    PWR_AVAIL_PWR_SUPPLY_MODULE 1200

/* Poe return values for the 4 types of event*/
#define    POE_PSE_STATUS_EVENT        0x0001
#define    POE_HW_ERROR_EVENT          0x0002
#define    POE_LINK_STATUS_EVENT       0x0004
#define    POE_Q_EVENT                 0x0008

/* Poe errors for Mac Learning/Aging Q  events */
#define    POE_ERR_MAC_LEARNING_PROCESS  410
#define    POE_ERR_MAC_AGING_PROCESS     420

/* Poe Static Mac entry*/
#define    POE_STATIC_MAC_ENTRY           5

/* Poe Return Values*/
#define    POE_ERR_MEM_FAILURE            1
#define    POE_ERR_INVALID_VALUE          2

/* Poe Invalid Values */
#define    POE_INVALID_VAL                0

/* Ethernet Type */
#define    POE_ETHERNET_TYPE              CFA_ENET

/* Defining all macros used in the POE module */
#define    POE_INIT_COMPLETE(u4Status)    lrInitComplete(u4Status)

/* Macros for creating memory pools */
#define    POE_CREATE_MEM_POOL(x,y,pPoePoolId)\
           MemCreateMemPool(x,y,POE_MEMORY_TYPE,(tPoeMemPoolId*)(pPoePoolId))
           
#define    POE_PSEENTRY_ALLOC_MEM_BLOCK(pu1Msg , type)\
           pu1Msg = (type *)MemAllocMemBlk(POE_PSEENTRY_POOL_ID)
           
#define    POE_PORTENTRY_ALLOC_MEM_BLOCK(pu1Msg , type )\
           pu1Msg = (type *)MemAllocMemBlk(POE_PORTENTRY_POOL_ID)
           
#define    POE_MACENTRY_ALLOC_MEM_BLOCK(pu1Msg, type)\
           pu1Msg = (type *)MemAllocMemBlk(POE_MACENTRY_POOL_ID)

/* Macros for deleting Memory Pools */
#define    POE_DELETE_MEM_POOL(PoePoolId)\
           MemDeleteMemPool((tPoeMemPoolId) (PoePoolId))
           
#define    POE_PSEENTRY_FREE_MEM_BLOCK(pu1Msg)\
           MemReleaseMemBlock(POE_PSEENTRY_POOL_ID, (UINT1 *)pu1Msg)
           
#define    POE_PORTENTRY_FREE_MEM_BLOCK(pu1Msg)\
           MemReleaseMemBlock(POE_PORTENTRY_POOL_ID, (UINT1 *)pu1Msg)
           
#define    POE_MACENTRY_FREE_MEM_BLOCK(pu1Msg)\
           MemReleaseMemBlock(POE_MACENTRY_POOL_ID, (UINT1 *)pu1Msg)


/* Hash Table related macros */
#define   POE_HASH_CREATE_TABLE              TMO_HASH_Create_Table

#define   POE_HASH_GET_FIRST_BUCKET_NODE     TMO_HASH_Get_First_Bucket_Node

#define   POE_HASH_GET_NEXT_BUCKET_NODE      TMO_HASH_Get_Next_Bucket_Node

#define   POE_HASH_DELETE_TABLE              TMO_HASH_Delete_Table

#define   POE_HASH_INIT_NODE(pNode)\
             TMO_HASH_INIT_NODE((tPoeHashNode *)pNode)

#define   POE_HASH_ADD_NODE(pHashTab, pNode, u4Index,FuncParam)\
             TMO_HASH_Add_Node((tPoeHashTable *)pHashTab,\
             (tPoeHashNode *)pNode,(UINT4)u4Index, (UINT1 *)FuncParam)

#define   POE_HASH_DEL_NODE(pHashTab, pNode, u4Index)\
             TMO_HASH_Delete_Node((tPoeHashTable *)pHashTab, \
             (tPoeHashNode *)pNode, u4Index)

#define   POE_HASH_SCAN_BKT(pHashTab, u4Index, pNode, Typecast)\
             TMO_HASH_Scan_Bucket((tPoeHashTable *)pHashTab, u4Index,\
             (tPoeHashNode *)pNode, Typecast)

#define   POE_HASH_SCAN_TBL(pHashTab, u4Index)\
             TMO_HASH_Scan_Table((tPoeHashTable *)pHashTab, u4Index)

#define   POE_GET_TWOBYTE(u2Val, MacAddr)            \
          do {                                       \
               MEMCPY (&u2Val, MacAddr, 2);          \
               u2Val = (UINT2) POE_NTOHS(u2Val);     \
             } while(0)

/* SLL related macros */
#define   POE_SLL_FIRST(pList)\
          TMO_SLL_First((tPoeSll *)pList)

#define   POE_SLL_NEXT(pList,pNode)\
          TMO_SLL_Next((tPoeSll *)pList, (tPoeSllNode*)pNode)

#define   POE_SLL_LAST(pList)\
          TMO_SLL_Last((tPoeSll *)pList)

#define   POE_SLL_ADD(pList, pNode)\
          TMO_SLL_Add((tPoeSll *)pList, (tPoeSllNode *)pNode)

#define   POE_SLL_FIND(pList, pNode)\
          TMO_SLL_Find((tPoeSll *)pList, (tPoeSllNode *)pNode)

#define   POE_SLL_COUNT(pList)\
          TMO_SLL_Count((tPoeSll *)pList)

#define   POE_SLL_SCAN(pList, pNode, Typecast)\
          TMO_SLL_Scan((tPoeSll *)pList, pNode, Typecast)

#define   POE_SLL_INIT(pList)\
          TMO_SLL_Init((tPoeSll *)pList)

#define   POE_SLL_INIT_NODE(pNode)\
          TMO_SLL_Init_Node((tPoeSllNode*)pNode)

#define   POE_SLL_INSERT_IN_END(pList, pNode)\
          TMO_SLL_Insert((tPoeSll*)pList, POE_SLL_LAST((tPoeSll *)pList), \
                                                       (tPoeSllNode*)pNode)
#define   POE_SLL_INSERT(pList, pPrev, pNode)\
          TMO_SLL_Insert((tPoeSll*)pList, (tPoeSllNode*)pPrev, (tPoeSllNode*)pNode)

#define   POE_SLL_DELETE(pList, pNode)\
          TMO_SLL_Delete((tPoeSll *)pList, (tPoeSllNode *)pNode)

/* Defining Macros for POE trace messages */
#define POE_TRC(TraceType, Str)                                              \
        MOD_TRC(POE_TRC_FLAG, TraceType, POE_MOD_NAME, (const char *)Str)

#define POE_TRC_ARG1(TraceType, Str, Arg1)                                   \
        MOD_TRC_ARG1(POE_TRC_FLAG, TraceType, POE_MOD_NAME, (const char *)Str, Arg1)

/*for debug*/
#define   PNAC_DEBUG(x) {x}

/* POE Module related definitions */
#define   POE_IS_SHUTDOWN() \
   (POE_SYSTEM_CONTROL == POE_SHUTDOWN)

/* Macro for validating Mac address*/
#define   POE_IS_BROADCAST_ADDR(MacAddr) \
               ((MEMCMP(MacAddr,gPoeBCastAddr,POE_MAC_ADDR_SIZE) == 0) ? \
                 POE_TRUE : POE_FALSE)

#define   POE_COMPARE_MAC_ADDR(MacAddr1, MacAddr2) \
          ((MEMCMP(MacAddr1, MacAddr2, POE_MAC_ADDR_SIZE) == 0) ? \
            POE_TRUE : POE_FALSE)
/*Macro for POE Queue message*/

#define POE_QMSG_ALLOC_MEM_BLOCK(pQmsg , type)\
        POE_INPUTQ_MSG_CNT++ ;\
        pQmsg =  (type *)MemAllocMemBlk(POE_INPUTQ_POOL_ID)

#define POE_QMSG_FREE(pQmsg)\
        POE_INPUTQ_MSG_CNT-- ;\
        MemReleaseMemBlock(POE_INPUTQ_POOL_ID,(UINT1 *)pQmsg)
#endif /* _POEMACRO_H */

