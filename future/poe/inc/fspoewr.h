#ifndef _FSPOEWR_H
#define _FSPOEWR_H

VOID RegisterFSPOE(VOID);
INT4 FsPoeGlobalAdminStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsPoeGlobalAdminStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsPoeGlobalAdminStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPoeGlobalAdminStatusDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);

INT4 GetNextIndexFsPoeMacTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsPoePdMacAddressGet(tSnmpIndex *, tRetVal *);
INT4 FsPoePdMacPortGet(tSnmpIndex *, tRetVal *);
INT4 FsPoePdMacRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsPoePdMacRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsPoePdMacRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPoeMacTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

#endif /* _FSPOEWR_H */
