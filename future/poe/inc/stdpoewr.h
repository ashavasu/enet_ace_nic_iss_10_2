/* $Id: stdpoewr.h,v 1.4 2011/10/13 10:33:33 siva Exp $ */
#ifndef _STDPOEWR_H
#define _STDPOEWR_H
INT4 GetNextIndexPethPsePortTable(tSnmpIndex *, tSnmpIndex *);

VOID RegisterSTDPOE(VOID);
INT4 PethPsePortGroupIndexGet(tSnmpIndex *, tRetVal *);
INT4 PethPsePortIndexGet(tSnmpIndex *, tRetVal *);
INT4 PethPsePortAdminEnableGet(tSnmpIndex *, tRetVal *);
INT4 PethPsePortPowerPairsControlAbilityGet(tSnmpIndex *, tRetVal *);
INT4 PethPsePortPowerPairsGet(tSnmpIndex *, tRetVal *);
INT4 PethPsePortDetectionStatusGet(tSnmpIndex *, tRetVal *);
INT4 PethPsePortPowerPriorityGet(tSnmpIndex *, tRetVal *);
INT4 PethPsePortMPSAbsentCounterGet(tSnmpIndex *, tRetVal *);
INT4 PethPsePortTypeGet(tSnmpIndex *, tRetVal *);
INT4 PethPsePortPowerClassificationsGet(tSnmpIndex *, tRetVal *);
INT4 PethPsePortInvalidSignatureCounterGet(tSnmpIndex *, tRetVal *);
INT4 PethPsePortPowerDeniedCounterGet(tSnmpIndex *, tRetVal *);
INT4 PethPsePortOverLoadCounterGet(tSnmpIndex *, tRetVal *);
INT4 PethPsePortShortCounterGet(tSnmpIndex *, tRetVal *);
INT4 PethPsePortAdminEnableSet(tSnmpIndex *, tRetVal *);
INT4 PethPsePortPowerPairsSet(tSnmpIndex *, tRetVal *);
INT4 PethPsePortPowerPrioritySet(tSnmpIndex *, tRetVal *);
INT4 PethPsePortTypeSet(tSnmpIndex *, tRetVal *);
INT4 PethPsePortAdminEnableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PethPsePortPowerPairsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PethPsePortPowerPriorityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PethPsePortTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PethPsePortTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);




INT4 GetNextIndexPethMainPseTable(tSnmpIndex *, tSnmpIndex *);
INT4 PethMainPseGroupIndexGet(tSnmpIndex *, tRetVal *);
INT4 PethMainPsePowerGet(tSnmpIndex *, tRetVal *);
INT4 PethMainPseOperStatusGet(tSnmpIndex *, tRetVal *);
INT4 PethMainPseConsumptionPowerGet(tSnmpIndex *, tRetVal *);
INT4 PethMainPseUsageThresholdGet(tSnmpIndex *, tRetVal *);
INT4 PethMainPseUsageThresholdSet(tSnmpIndex *, tRetVal *);
INT4 PethMainPseUsageThresholdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PethMainPseTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

INT4 GetNextIndexPethNotificationControlTable(tSnmpIndex *, tSnmpIndex *);
INT4 PethNotificationControlGroupIndexGet(tSnmpIndex *, tRetVal *);
INT4 PethNotificationControlEnableGet(tSnmpIndex *, tRetVal *);
INT4 PethNotificationControlEnableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PethNotificationControlTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

#endif /* _STDPOEWR_H */
