#ifndef _POEHDRS_H
#define _POEHDRS_H

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* Licensee Aricent Inc., 2001-2002                         */
/*****************************************************************************/
/*    FILE  NAME            : poehdrs.h                                       */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : POWER OVER ETHERNET                            */
/*    MODULE NAME           : All modules in Power Over Ethernet             */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 27 Jan 2005                                    */
/*    AUTHOR                : FCSDI                                          */
/*    DESCRIPTION           : This file contains all include files in the    */
/*                            PoE module.                                    */
/*---------------------------------------------------------------------------*/


/*Common include files*/
#include "lr.h"
#include "cli.h"
#include "cfa.h"
#include "msr.h"
#include "iss.h"
#include "size.h"
#include "snmccons.h"
#include "trace.h"
#include "fssnmp.h"
#include "bridge.h"
#include "fsvlan.h"

#ifdef NPAPI_WANTED
#include "npapi.h"
#include "vlannp.h"
#endif

/*PoE Specific include files*/
#include "poe.h"
#include "poedfs.h"
#include "poemacro.h"
#include "fspoelw.h"
#include "fspoewr.h"
#include "poeproto.h"
#include "stdpoelw.h"
#include "stdpoewr.h"

#ifdef NPAPI_WANTED 
#include "poenp.h"
#endif

#endif   /* _POEHDRS_H  */
