/* $Id: poeproto.h,v 1.5 2013/10/28 09:49:59 siva Exp $ */
#ifndef _POEPROTO_H
#define _POEPROTO_H
/*****************************************************************************/
/*    FILE  NAME            : poeproto.h                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : POE                                            */
/*    MODULE NAME           : All modules in mib browser.                    */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                :                                                */
/*    DESCRIPTION           : This file contains prototypes of all functions */
/*                            in poe module                                  */
/*---------------------------------------------------------------------------*/


/*********************/
/* fspoelw.c */
/*********************/

INT4 PoeSnmpLowValidateSuppAddress (tMacAddr FsPoePdMacAddress);
INT4 PoeSnmpLowGetFirstValidPdMacAddress (tMacAddr FsPoePdMacAddress);
INT4 PoeSnmpLowGetNextValidPdMacAddress (tMacAddr FsPoePdMacAddress,tMacAddr * pNextFsPoePdMacAddress);
INT4 PoeSnmpLowGetPdMacNode (tMacAddr FsPoePdMacAddress,tPoeMacEntry ** ppPdMacNode);

/*********************/
/* stdpoelw.c */
/*********************/

INT4 PoeSnmpLowValidatePortIndex (INT4 i4Dot3afGroupIndex , INT4 i4Dot3afPortIndex);
INT4 PoeSnmpLowGetFirstValidPortIndex (INT4 *pi4Dot3afFirstIndex);
INT4 PoeSnmpLowGetNextValidPortIndex (INT4 i4Dot3afIndex, INT4 *pi4Dot3afNextIndex);

INT4 PoeSnmpLowValidateMainPseGroupIndex (INT4 i4Dot3afMainPseIndex);
INT4 PoeSnmpLowGetFirstValidPseGroupIndex (INT4 *pi4Dot3afFirstIndex);
INT4 PoeSnmpLowGetNextValidPseGroupIndex (INT4 i4Dot3afIndex, INT4 *pi4Dot3afNextIndex);


/*********************/
/* poemain.c */
/*********************/

/*For Mac Table*/
INT4 PoeSnmpLowCreatePdMacNode (tMacAddr macAddr , tPoeMacEntry ** ppPdMacNode);
INT4 PoeGetFirstValidPdMacAddress (tMacAddr macAddr);
VOID PoeCalcPdMacTblIndex        (tMacAddr macAddr, UINT4 *pu4HashIdx);
INT4 PoeGetNextValidPdMacAddress (tMacAddr refMacAddr, tMacAddr nextMacAddr);
INT4 PoeGetPdMacTblEntryByMac  (tMacAddr macAddr, tPoeMacEntry ** ppPdMacNode);
VOID PoeReleaseMacMemBlock (tPoeHashNode * pPoeHashEntry);

/*For Pse Table*/
INT4 PoeGetPowerFromClass (UINT2 u2PowerClass , UINT2 * u2PowerReq);
INT4 PoeGetPowerAvailableStatus (UINT2 u2PowerClass);
INT4 PoeGetTotalPowerAvailable PROTO((VOID));
INT4 PoeUpdatePowerAvailable (UINT2 u2PowerClass, UINT2 u2PowerUpdate);
INT4 PoeGetMainPseEntry (UINT2 u2PseIndex, tPoeMainPseEntry ** pPseEntry);
INT4 PoeGetNextMainPseEntry (tPoeMainPseEntry  * pPseEntry, tPoeMainPseEntry ** pNextPseEntry);
VOID PoeInitialisePseEntry (tPoeMainPseEntry * pPseEntry );
INT4 PoeAddEntryToPseTable (tPoeMainPseEntry * pPseEntry);
INT4 PoeDeleteEntryFromPseTable (tPoeMainPseEntry * pPseEntry);
INT4 PoeCreatePseTable (UINT2 u2PseIndex);

/*For Poe Port Table*/
INT4 PoeGetPortEntry (UINT2 u2PortIndex, tPoePsePortEntry ** ppPortEntry);
INT4 PoeGetNextPortEntry (tPoePsePortEntry * pPortEntry,tPoePsePortEntry ** ppNextPortEntry);
VOID PoeInitialisePortEntry (tPoePsePortEntry * pPortEntry );
INT4 PoeAddEntryToPortTable (tPoePsePortEntry * pPortEntry);
INT4 PoeDeleteEntryFromPortTable (tPoePsePortEntry * pPortEntry);

/*For Processing Poe Q events */
INT4 PoeProcessQMsg (const UINT1 *pu1Qname);
INT4 PoeProcessAgedOutMac   (tPoeMacPortMsg PoeMacPortPair);
INT4 PoeProcessNewMacLearnt (tPoeMacPortMsg  PoeMacPortPair);

/*For handling Poe Failure*/
VOID PoeHandleInitFailure PROTO((VOID));

/*Poe module init and shut */
INT4 PoeModuleInit PROTO((VOID));
INT4 PoeShutDown PROTO((VOID)); 


/* Poe Lock/Unlock */
INT4 PoeLockDbg (UINT1 *pu1File, UINT4 u4Line);
INT4 PoeLock PROTO((VOID));
INT4 PoeUnLock PROTO((VOID));

/*********************/
/* poecli.c */
/*********************/
INT4 PoeGlobalAdminState (tCliHandle CliHandle, INT4 i4PoeStatus);
INT4 PoeConfigMacList (tCliHandle CliHandle, UINT4 macAction, tMacAddr PdMac);
INT4 PoePortAdminState (tCliHandle CliHandle, INT4 PortIndex ,INT4 PortAdminStatus);
INT4 PoeSetPortPriority (tCliHandle CliHandle, INT4 PortIndex ,INT4 PortPriority);
INT4 ShowPoePseStatus (tCliHandle CliHandle); 
INT4 ShowAllPoeInterface (tCliHandle CliHandle);
INT4 ShowPoeInterfaceInfo (tCliHandle CliHandle, INT4 i4Port);
INT4 ShowPdMacAddrsListInfo (tCliHandle CliHandle);
INT4 PoeShowRunningConfig (tCliHandle CliHandle, UINT4 u4Module);
INT4 PoeShowRunningConfigScalar (tCliHandle CliHandle);
INT4 PoeShowRunningConfigTable (tCliHandle CliHandle);
INT4 PoeShowRunningConfigInterface (tCliHandle CliHandle);
INT4 PoeShowRunningConfigInterfaceDetails (tCliHandle CliHandle, INT4 i4IfIndex);


#endif  /* _POEPROTO_H */  
