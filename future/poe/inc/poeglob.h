#ifndef _POEGLOB_H
#define _POEGLOB_H

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* Licensee Aricent Inc., 2001-2002                         */
/*****************************************************************************/
/*    FILE  NAME            : poeglob.h                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : POE Module                                     */
/*    MODULE NAME           : All modules in Link Aggregation.               */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 27 Jan 2005                                    */
/*    AUTHOR                : Fcsdi Team                                     */
/*    DESCRIPTION           : This file contains the global structure used   */
/*                            in PoE module.                                 */
/*---------------------------------------------------------------------------*/

/*For broadcast Mac */
tMacAddr gPoeBCastAddr = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
/*Defining global Info for Poe */
tPoeInfo gPoeInfo ;

#endif
