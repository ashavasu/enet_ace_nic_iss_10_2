/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdpoelw.h,v 1.5 2011/10/13 10:33:33 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for PethPsePortTable. */
INT1
nmhValidateIndexInstancePethPsePortTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for PethPsePortTable  */

INT1
nmhGetFirstIndexPethPsePortTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPethPsePortTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPethPsePortAdminEnable ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetPethPsePortPowerPairsControlAbility ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetPethPsePortPowerPairs ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetPethPsePortDetectionStatus ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetPethPsePortPowerPriority ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetPethPsePortMPSAbsentCounter ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetPethPsePortType ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetPethPsePortPowerClassifications ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetPethPsePortInvalidSignatureCounter ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetPethPsePortPowerDeniedCounter ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetPethPsePortOverLoadCounter ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetPethPsePortShortCounter ARG_LIST((INT4  , INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPethPsePortAdminEnable ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetPethPsePortPowerPairs ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetPethPsePortPowerPriority ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetPethPsePortType ARG_LIST((INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PethPsePortAdminEnable ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2PethPsePortPowerPairs ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2PethPsePortPowerPriority ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2PethPsePortType ARG_LIST((UINT4 *  ,INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2PethPsePortTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for PethMainPseTable. */
INT1
nmhValidateIndexInstancePethMainPseTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PethMainPseTable  */

INT1
nmhGetFirstIndexPethMainPseTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPethMainPseTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPethMainPsePower ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPethMainPseOperStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPethMainPseConsumptionPower ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPethMainPseUsageThreshold ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPethMainPseUsageThreshold ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PethMainPseUsageThreshold ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2PethMainPseTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for PethNotificationControlTable. */
INT1
nmhValidateIndexInstancePethNotificationControlTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PethNotificationControlTable  */

INT1
nmhGetFirstIndexPethNotificationControlTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPethNotificationControlTable ARG_LIST((INT4 , INT4 *));





/* Low Level DEP Routines for.  */

INT1
nmhDepv2PethNotificationControlTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
