/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: poedfs.h,v 1.4 2012/01/09 12:44:21 siva Exp $
 *
 * Description: Contains definitions, macros and functions to be used
 *              by  external modules.
 *
 *******************************************************************/


#ifndef _POEDFS_H
#define _POEDFS_H
/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* Licensee Aricent Inc., 2001-2002                         */
/*****************************************************************************/
/*    FILE  NAME            : poedfs.h                                       */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : POWER OVER ETHERNET                            */
/*    MODULE NAME           : All modules in PoE                             */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 27 Jan 2005                                    */
/*    AUTHOR                : Fcsdi Team                                     */
/*    DESCRIPTION           : This file contains all typedefs and enums      */
/*                            used in PoE module                             */
/*---------------------------------------------------------------------------*/


typedef enum {
   ON = 1,
   OFF = 2,
   FAULTY = 3
}tPoePseOperStatus;

typedef enum {
   POE_START = 1,
   POE_SHUTDOWN = 2
}tPoeSysControl;


/*The # of modules for each of the module type for 4U
 *For 8U its twice of whats available for 4U */

#define    MAX_PWR_SUPPLY_MODULE        2
#define    MAX_DOWNLNK_MODULE           2
#define    MAX_UPLNK_MODULE             1
#define    MAX_SUPRVISR_MODULE          1
#define    MAX_FAN_MODULE               3

#define    POE_MAX_PS                  MAX_PWR_SUPPLY_MODULE 
#define    POE_MAX_PORTS               SYS_DEF_MAX_PHYSICAL_INTERFACES


/* Defining Hash table for PoE */
typedef tTMO_HASH_TABLE     tPoeHashTable;
typedef tTMO_HASH_NODE      tPoeHashNode;


/* For POE Task */
typedef tOsixTaskId         tPoeTaskId;

/*For creating memory pools for POE */
typedef tMemPoolId          tPoeMemPoolId;

typedef tOsixSemId          tPoeSemId;

/*For PoE Power Supply */
typedef struct PoeMainPseEntry {
   tPoeSllNode             NextPseEntry;   /* Next entry in the list */
   UINT4                   u4MainPsePower;
   UINT4                   u4MainPseConsumptionPower;
   tPoePseOperStatus       MainPseOperStatus; 
   UINT2                   u2MainPseGroupIndex;
   UINT2                   u2Reserved;
} tPoeMainPseEntry;

/*For PoE MAC Entry Table */
typedef struct PoeMacEntry {
   tPoeHashNode        NextMacEntry;
   UINT4               u4PsePortIndex;
   tMacAddr            pdMacAddress;
   UINT1               u1RowStatus;
   UINT1               u1Reserved;
} tPoeMacEntry;

/* Defining all global structure used in the POE module*/
typedef struct 
{
   tPoeMainPseEntry      *apPoeMainPseEntry[POE_MAX_PS];
                                  /* This array contains the pointers to 
                                   * Power Supply Entries. The PS Index 
                                   * is same as the array index */ 
   tPoePsePortEntry      *apPoePortEntry[POE_MAX_PORTS];
                                  /* This array contains the pointers to Port
                                   * Entries corresponding to their Port
                                   * Indices. The Port Index is same as the
                                   * array index */
   tPoeHashTable         *pPoeMacTable;   /*For Mac table entries*/
   tPoeMemPoolId         PoePsePoolId;    /*For Pse entry table*/
   tPoeMemPoolId         PoeMacPoolId;    /*For Port entry table*/
   tPoeMemPoolId         PoePortPoolId;   /*For Mac entry hash table;*/
   tPoeMemPoolId         PoeInputQPoolId; /*For Poe Input Queue */
   tPoeSemId   PoeProtocolSemId; /*For Poe Semaphore */
   UINT4                 u4InputQMsgCnt;  /*Count for number of messages in the Q*/
   tOsixQId              PoeInputQId;
   tPoeTaskId            poeTaskId;
   UINT4                 u4TraceOption;
   tPoeSysControl        poeGlobalAdminStatus;
   UINT2                 u2CurrPsePower;
   UINT2                 u2MaxPwrSupplyMod;
   UINT2                 u2MaxDnLnkMod;
   UINT2                 u2MaxUpLnkMod;
   UINT2                 u2MaxSuprvisrMod;
   UINT2                 u2MaxFanMod;
   UINT2                 u2PoeModInit;
   UINT2                 u2PoeMacAddrsProcess;
   tPoeBoolean   bProtocolLocked;
}tPoeInfo;


/*Poe Q Msg Type*/
typedef struct
{
    tMacAddr          MacAddr;
    UINT2             u2PortIndex;
    UINT2             u2Reserved;
}tPoeMacPortMsg;

typedef struct
{
    UINT4                u4MsgType;
    tPoeMacPortMsg       PoeMacPortPair;
    UINT1                au1Pad[2];
}tPoeQMsg;

#endif /*_POEDFS_H */
