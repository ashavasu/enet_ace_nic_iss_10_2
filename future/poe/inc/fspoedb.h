/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fspoedb.h,v 1.4 2008/08/20 15:23:39 iss Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSPOEDB_H
#define _FSPOEDB_H

UINT1 FsPoeMacTableINDEX [] = {SNMP_FIXED_LENGTH_OCTET_STRING ,6};

UINT4 fspoe [] ={1,3,6,1,4,1,2076,103};
tSNMP_OID_TYPE fspoeOID = {8, fspoe};


UINT4 FsPoeGlobalAdminStatus [ ] ={1,3,6,1,4,1,2076,103,1,1};
UINT4 FsPoePdMacAddress [ ] ={1,3,6,1,4,1,2076,103,1,2,1,1};
UINT4 FsPoePdMacPort [ ] ={1,3,6,1,4,1,2076,103,1,2,1,2};
UINT4 FsPoePdMacRowStatus [ ] ={1,3,6,1,4,1,2076,103,1,2,1,3};


tMbDbEntry fspoeMibEntry[]= {

{{10,FsPoeGlobalAdminStatus}, NULL, FsPoeGlobalAdminStatusGet, FsPoeGlobalAdminStatusSet, FsPoeGlobalAdminStatusTest, FsPoeGlobalAdminStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{12,FsPoePdMacAddress}, GetNextIndexFsPoeMacTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_NOACCESS, FsPoeMacTableINDEX, 1, 0, 0, NULL},

{{12,FsPoePdMacPort}, GetNextIndexFsPoeMacTable, FsPoePdMacPortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPoeMacTableINDEX, 1, 0, 0, NULL},

{{12,FsPoePdMacRowStatus}, GetNextIndexFsPoeMacTable, FsPoePdMacRowStatusGet, FsPoePdMacRowStatusSet, FsPoePdMacRowStatusTest, FsPoeMacTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPoeMacTableINDEX, 1, 0, 1, NULL},
};
tMibData fspoeEntry = { 4, fspoeMibEntry };
#endif /* _FSPOEDB_H */

