/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdpoedb.h,v 1.4 2008/08/20 15:23:39 iss Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _STDPOEDB_H
#define _STDPOEDB_H

UINT1 PethPsePortTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 PethMainPseTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 PethNotificationControlTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};

UINT4 stdpoe [] ={1,3,6,1,2,1,105};
tSNMP_OID_TYPE stdpoeOID = {7, stdpoe};


UINT4 PethPsePortGroupIndex [ ] ={1,3,6,1,2,1,105,1,1,1,1};
UINT4 PethPsePortIndex [ ] ={1,3,6,1,2,1,105,1,1,1,2};
UINT4 PethPsePortAdminEnable [ ] ={1,3,6,1,2,1,105,1,1,1,3};
UINT4 PethPsePortPowerPairsControlAbility [ ] ={1,3,6,1,2,1,105,1,1,1,4};
UINT4 PethPsePortPowerPairs [ ] ={1,3,6,1,2,1,105,1,1,1,5};
UINT4 PethPsePortDetectionStatus [ ] ={1,3,6,1,2,1,105,1,1,1,6};
UINT4 PethPsePortPowerPriority [ ] ={1,3,6,1,2,1,105,1,1,1,7};
UINT4 PethPsePortMPSAbsentCounter [ ] ={1,3,6,1,2,1,105,1,1,1,8};
UINT4 PethPsePortType [ ] ={1,3,6,1,2,1,105,1,1,1,9};
UINT4 PethPsePortPowerClassifications [ ] ={1,3,6,1,2,1,105,1,1,1,10};
UINT4 PethPsePortInvalidSignatureCounter [ ] ={1,3,6,1,2,1,105,1,1,1,11};
UINT4 PethPsePortPowerDeniedCounter [ ] ={1,3,6,1,2,1,105,1,1,1,12};
UINT4 PethPsePortOverLoadCounter [ ] ={1,3,6,1,2,1,105,1,1,1,13};
UINT4 PethPsePortShortCounter [ ] ={1,3,6,1,2,1,105,1,1,1,14};
UINT4 PethMainPseGroupIndex [ ] ={1,3,6,1,2,1,105,1,3,1,1,1};
UINT4 PethMainPsePower [ ] ={1,3,6,1,2,1,105,1,3,1,1,2};
UINT4 PethMainPseOperStatus [ ] ={1,3,6,1,2,1,105,1,3,1,1,3};
UINT4 PethMainPseConsumptionPower [ ] ={1,3,6,1,2,1,105,1,3,1,1,4};
UINT4 PethMainPseUsageThreshold [ ] ={1,3,6,1,2,1,105,1,3,1,1,5};
UINT4 PethNotificationControlGroupIndex [ ] ={1,3,6,1,2,1,105,1,4,1,1,1};
UINT4 PethNotificationControlEnable [ ] ={1,3,6,1,2,1,105,1,4,1,1,2};


tMbDbEntry stdpoeMibEntry[]= {

{{11,PethPsePortGroupIndex}, GetNextIndexPethPsePortTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, PethPsePortTableINDEX, 2, 0, 0, NULL},

{{11,PethPsePortIndex}, GetNextIndexPethPsePortTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, PethPsePortTableINDEX, 2, 0, 0, NULL},

{{11,PethPsePortAdminEnable}, GetNextIndexPethPsePortTable, PethPsePortAdminEnableGet, PethPsePortAdminEnableSet, PethPsePortAdminEnableTest, PethPsePortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, PethPsePortTableINDEX, 2, 0, 0, NULL},

{{11,PethPsePortPowerPairsControlAbility}, GetNextIndexPethPsePortTable, PethPsePortPowerPairsControlAbilityGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, PethPsePortTableINDEX, 2, 0, 0, NULL},

{{11,PethPsePortPowerPairs}, GetNextIndexPethPsePortTable, PethPsePortPowerPairsGet, PethPsePortPowerPairsSet, PethPsePortPowerPairsTest, PethPsePortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, PethPsePortTableINDEX, 2, 0, 0, NULL},

{{11,PethPsePortDetectionStatus}, GetNextIndexPethPsePortTable, PethPsePortDetectionStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, PethPsePortTableINDEX, 2, 0, 0, NULL},

{{11,PethPsePortPowerPriority}, GetNextIndexPethPsePortTable, PethPsePortPowerPriorityGet, PethPsePortPowerPrioritySet, PethPsePortPowerPriorityTest, PethPsePortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, PethPsePortTableINDEX, 2, 0, 0, NULL},

{{11,PethPsePortMPSAbsentCounter}, GetNextIndexPethPsePortTable, PethPsePortMPSAbsentCounterGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, PethPsePortTableINDEX, 2, 0, 0, NULL},

{{11,PethPsePortType}, GetNextIndexPethPsePortTable, PethPsePortTypeGet, PethPsePortTypeSet, PethPsePortTypeTest, PethPsePortTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, PethPsePortTableINDEX, 2, 0, 0, NULL},

{{11,PethPsePortPowerClassifications}, GetNextIndexPethPsePortTable, PethPsePortPowerClassificationsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, PethPsePortTableINDEX, 2, 0, 0, NULL},

{{11,PethPsePortInvalidSignatureCounter}, GetNextIndexPethPsePortTable, PethPsePortInvalidSignatureCounterGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, PethPsePortTableINDEX, 2, 0, 0, NULL},

{{11,PethPsePortPowerDeniedCounter}, GetNextIndexPethPsePortTable, PethPsePortPowerDeniedCounterGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, PethPsePortTableINDEX, 2, 0, 0, NULL},

{{11,PethPsePortOverLoadCounter}, GetNextIndexPethPsePortTable, PethPsePortOverLoadCounterGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, PethPsePortTableINDEX, 2, 0, 0, NULL},

{{11,PethPsePortShortCounter}, GetNextIndexPethPsePortTable, PethPsePortShortCounterGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, PethPsePortTableINDEX, 2, 0, 0, NULL},

{{12,PethMainPseGroupIndex}, GetNextIndexPethMainPseTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, PethMainPseTableINDEX, 1, 0, 0, NULL},

{{12,PethMainPsePower}, GetNextIndexPethMainPseTable, PethMainPsePowerGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, PethMainPseTableINDEX, 1, 0, 0, NULL},

{{12,PethMainPseOperStatus}, GetNextIndexPethMainPseTable, PethMainPseOperStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, PethMainPseTableINDEX, 1, 0, 0, NULL},

{{12,PethMainPseConsumptionPower}, GetNextIndexPethMainPseTable, PethMainPseConsumptionPowerGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, PethMainPseTableINDEX, 1, 0, 0, NULL},

{{12,PethMainPseUsageThreshold}, GetNextIndexPethMainPseTable, PethMainPseUsageThresholdGet, PethMainPseUsageThresholdSet, PethMainPseUsageThresholdTest, PethMainPseTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, PethMainPseTableINDEX, 1, 0, 0, NULL},

{{12,PethNotificationControlGroupIndex}, GetNextIndexPethNotificationControlTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, PethNotificationControlTableINDEX, 1, 0, 0, NULL},

{{12,PethNotificationControlEnable}, GetNextIndexPethNotificationControlTable, PethNotificationControlEnableGet, PethNotificationControlEnableSet, PethNotificationControlEnableTest, PethNotificationControlTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, PethNotificationControlTableINDEX, 1, 0, 0, NULL},
};
tMibData stdpoeEntry = { 21, stdpoeMibEntry };
#endif /* _STDPOEDB_H */

