#!/bin/csh
# Copyright (C) 2006 Aricent Inc . All Rights Reserved
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : make.h                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       : Aricent Inc.                     |
# |                                                                          |
# |   MAKE TOOL(S) USED      : Eg: GNU MAKE                                  |
# |                                                                          |
# |   TARGET ENVIRONMENT     : LINUX ( Slackware 1.2.1 )                     |
# |                                                                          |
# |   DATE                   : 07 March 2002                                 |
# |                                                                          |
# |   DESCRIPTION            : Provide the following information in order -  |
# |                            1. Number of Submodules present if Main       |
# |                               makefile.                                  |
# |                            2. Clean option                               |
# +--------------------------------------------------------------------------+

###########################################################################
#               COMPILATION SWITCHES                                      #
###########################################################################


POE_SWITCHES = -UDEBUG_WANTED 

ifeq (${NPAPI}, YES)
POE_SWITCHES += -DNP_API
endif

TOTAL_OPNS = ${POE_SWITCHES} $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES)

############################################################################
#                         Directories                                      #
############################################################################

POE_BASE_DIR = ${BASE_DIR}/poe
POE_SRC_DIR  = ${POE_BASE_DIR}/src
POE_INC_DIR  = ${POE_BASE_DIR}/inc
POE_OBJ_DIR  = ${POE_BASE_DIR}/obj
COMMON_INC_DIR  = ${BASE_DIR}/inc
SNMP_INC_DIR  = ${BASE_DIR}/inc/snmp
# FS_DELTA - Define CFA_BASE_DIR
CFA_BASE_DIR = ${BASE_DIR}/cfa2
CFA_INCD    = ${CFA_BASE_DIR}/inc
CLI_INCD    = ${BASE_DIR}/inc/cli

############################################################################
##                     INCLUDE OPTIONS                                    ##
############################################################################

GLOBAL_INCLUDES  =  -I${POE_INC_DIR} -I${CFA_INCD} -I${COMMON_INC_DIR} -I${CLI_INCD}-I${SNMP_INC_DIR}
INCLUDES         = ${GLOBAL_INCLUDES} ${COMMON_INCLUDE_DIRS}

#############################################################################
