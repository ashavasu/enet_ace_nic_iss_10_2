#!/bin/csh
# (C) 2002 Future Software Pvt. Ltd.
#/*$Id: make.h,v 1.1.1.1 2016/09/20 10:42:41 siva Exp $*/
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : MAKE.H                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       : Future Software Pvt. Ltd.                     |
# |                                                                          |
# |   MAKE TOOL(S) USED      : Eg: GNU MAKE                                  |
# |                                                                          |
# |   TARGET ENVIRONMENT     : LINUX ( Slackware 1.2.1 )                     |
# |                                                                          |
# |   DATE                   :                                               |
# |                                                                          |
# |   DESCRIPTION            : Provide the following information in order -  |
# |                            1. Number of Submodules present if Main       |
# |                               makefile.                                  |
# |                            2. Clean option                               |
# +--------------------------------------------------------------------------+

###########################################################################
#               COMPILATION SWITCHES                                      #
###########################################################################


TOTAL_OPNS = $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES)

############################################################################
#                         Directories                                      #
############################################################################

CFA_BASE_DIR   = ${BASE_DIR}/cfa2

WSSUSER_BASE_DIR      = ${BASE_DIR}/wss/wssuser
PNAC_BASE_DIR      = ${BASE_DIR}/pnac
WLAN_BASE_DIR      = ${BASE_DIR}/wss
WSSUSER_SRC_DIR       = ${WSSUSER_BASE_DIR}/src
WSSUSER_INC_DIR       = ${WSSUSER_BASE_DIR}/inc
WSSUSER_OBJ_DIR       = ${WSSUSER_BASE_DIR}/obj
CFA_INCD           = ${CFA_BASE_DIR}/inc
CMN_INC_DIR        = ${BASE_DIR}/inc
PNAC_INC_DIR       = ${PNAC_BASE_DIR}/inc
WLAN_INC_DIR       = ${WLAN_BASE_DIR}/wsswlan/inc
WSSSTA_INC_DIR     = ${WSSSTA_BASE_DIR}/inc

############################################################################
##                     INCLUDE OPTIONS                                    ##
############################################################################

GLOBAL_INCLUDES  =  -I${WSSUSER_INC_DIR} -I${CFA_INCD} -I${CMN_INC_DIR} -I${PNAC_INC_DIR} -I${WSSSTA_INC_DIR} -I${WLAN_INC_DIR}
INCLUDES         = ${GLOBAL_INCLUDES} ${COMMON_INCLUDE_DIRS} -I$(BASE_DIR)/util/aes

#############################################################################
