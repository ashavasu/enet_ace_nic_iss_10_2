/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: userutil.c,v 1.2 2017/05/23 14:16:59 siva Exp $
*
* Description: WSSUSER File.
*********************************************************************/

#include "userinc.h"
#include "snmputil.h"

/*********************************************************************************/
/* Function Name      : WssUserCompareGroupId                                    */
/*                                                                               */
/* Description        : This function compares the two GroupEntryNode            */
/*                      based on the Group Id                                    */
/*                                                                               */
/* Input(s)           : e1        Pointer to First Group                         */
/* Input(s)           : e2        Pointer to Second Group                        */
/*                                                                               */
/* Output(s)          : None                                                     */
/*                                                                               */
/* Return Value(s)    : 1. WSSUSER_GREATER - If key of first element is greater  */
/*                                           than key of second element          */
/*                      2. WSSUSER_EQUAL   - If key of first element is equal    */
/*                                           to key of second element            */
/*                      3. WSSUSER_LESSER  - If key of first element is lesser   */
/*                                           than key of second element          */
/*********************************************************************************/
INT4
WssUserCompareGroupId (tRBElem * e1, tRBElem * e2)
{
    tUserGroupEntry    *pRBNode1 = (tUserGroupEntry *) e1;
    tUserGroupEntry    *pRBNode2 = (tUserGroupEntry *) e2;

    if (pRBNode1->u4GroupId < pRBNode2->u4GroupId)
    {
        return WSSUSER_LESSER;
    }
    else if (pRBNode1->u4GroupId > pRBNode2->u4GroupId)
    {
        return WSSUSER_GREATER;
    }
    return WSSUSER_EQUAL;
}

/*********************************************************************************/
/* Function Name      : WssUserCompareUserNameAndWlanId                          */
/*                                                                               */
/* Description        : This function compares the two RoleEntryNode             */
/*                      based on the Wlan Id and User Name                       */
/*                                                                               */
/* Input(s)           : e1        Pointer to First Entry                         */
/* Input(s)           : e2        Pointer to Second Entry                        */
/*                                                                               */
/* Output(s)          : None                                                     */
/*                                                                               */
/* Return Value(s)    : 1. WSSUSER_GREATER - If key of first element is greater  */
/*                                           than key of second element          */
/*                      2. WSSUSER_EQUAL   - If key of first element is equal    */
/*                                           to key of second element            */
/*                      3. WSSUSER_LESSER  - If key of first element is lesser   */
/*                                           than key of second element          */
/*********************************************************************************/
INT4
WssUserCompareUserNameAndWlanId (tRBElem * e1, tRBElem * e2)
{

    tUserRoleEntry     *pRBNode1 = (tUserRoleEntry *) e1;
    tUserRoleEntry     *pRBNode2 = (tUserRoleEntry *) e2;

    if (pRBNode1->u4WlanIndex < pRBNode2->u4WlanIndex)
    {
        return WSSUSER_LESSER;
    }
    else if (pRBNode1->u4WlanIndex > pRBNode2->u4WlanIndex)
    {
        return WSSUSER_GREATER;
    }

    if (pRBNode1->u4UserNameLen > pRBNode2->u4UserNameLen)
    {
        return WSSUSER_GREATER;
    }
    else if (pRBNode1->u4UserNameLen < pRBNode2->u4UserNameLen)
    {
        return WSSUSER_LESSER;
    }

    /* Both length's are equal. checking the contents now */
    if (MEMCMP (pRBNode1->au1UserName,
                pRBNode2->au1UserName, pRBNode1->u4UserNameLen) > 0)
    {
        return WSSUSER_GREATER;
    }
    else if (MEMCMP (pRBNode1->au1UserName,
                     pRBNode2->au1UserName, pRBNode1->u4UserNameLen) < 0)
    {
        return WSSUSER_LESSER;
    }
    return WSSUSER_EQUAL;

}

/*********************************************************************************/
/* Function Name      : WssUserRestrictUserNameCmp                               */
/*                                                                               */
/* Description        : This function compares the two User Names based on       */
/*                      length and content                                       */
/*                                                                               */
/* Input(s)           : e1        Pointer to First User Name                     */
/* Input(s)           : e2        Pointer to Second User Name                    */
/*                                                                               */
/* Output(s)          : None                                                     */
/*                                                                               */
/* Return Value(s)    : 1. WSSUSER_GREATER - If key of first element is greater  */
/*                                           than key of second element          */
/*                      2. WSSUSER_EQUAL   - If key of first element is equal    */
/*                                           to key of second element            */
/*                      3. WSSUSER_LESSER  - If key of first element is lesser   */
/*                                           than key of second element          */
/*********************************************************************************/
INT4
WssUserRestrictUserNameCmp (tRBElem * e1, tRBElem * e2)
{
    tUserNameAccessListEntry *pUserNameAccessList1 =
        (tUserNameAccessListEntry *) e1;
    tUserNameAccessListEntry *pUserNameAccessList2 =
        (tUserNameAccessListEntry *) e2;

    if (pUserNameAccessList1->u4UserNameLen >
        pUserNameAccessList2->u4UserNameLen)
    {
        return WSSUSER_GREATER;
    }
    else if (pUserNameAccessList1->u4UserNameLen <
             pUserNameAccessList2->u4UserNameLen)
    {
        return WSSUSER_LESSER;
    }

    /* Both length's are equal. checking the contents now */
    if (MEMCMP (pUserNameAccessList1->au1UserName,
                pUserNameAccessList2->au1UserName,
                pUserNameAccessList1->u4UserNameLen) > 0)
    {
        return WSSUSER_GREATER;
    }
    else if (MEMCMP (pUserNameAccessList1->au1UserName,
                     pUserNameAccessList2->au1UserName,
                     pUserNameAccessList1->u4UserNameLen) < 0)
    {
        return WSSUSER_LESSER;
    }
    return WSSUSER_EQUAL;
}

/*********************************************************************************/
/* Function Name      : WssUserRestrictUserMacCmp                                */
/*                                                                               */
/* Description        : This function compares the two User Mac Address based    */
/*                      on string compare                                        */
/*                                                                               */
/* Input(s)           : e1        Pointer to First User Mac                      */
/* Input(s)           : e2        Pointer to Second User Mac                     */
/*                                                                               */
/* Output(s)          : None                                                     */
/*                                                                               */
/* Return Value(s)    : 1. WSSUSER_GREATER - If key of first element is greater  */
/*                                           than key of second element          */
/*                      2. WSSUSER_EQUAL   - If key of first element is equal    */
/*                                           to key of second element            */
/*                      3. WSSUSER_LESSER  - If key of first element is lesser   */
/*                                           than key of second element          */
/*********************************************************************************/
INT4
WssUserRestrictUserMacCmp (tRBElem * e1, tRBElem * e2)
{
    tUserMacAccessListEntry *pUserMacAccessList1 =
        (tUserMacAccessListEntry *) e1;
    tUserMacAccessListEntry *pUserMacAccessList2 =
        (tUserMacAccessListEntry *) e2;

    if ((MEMCMP (pUserMacAccessList1->staMac,
                 pUserMacAccessList2->staMac, sizeof (tMacAddr))) > 0)
    {
        return WSSUSER_GREATER;
    }
    else if ((MEMCMP (pUserMacAccessList1->staMac,
                      pUserMacAccessList2->staMac, sizeof (tMacAddr))) < 0)
    {
        return WSSUSER_LESSER;
    }
    return WSSUSER_EQUAL;
}

/*********************************************************************************/
/* Function Name      : WssUserCompareUserNameAndMac                             */
/*                                                                               */
/* Description        : This function compares the two RoleEntryNode             */
/*                      based on the Mac and User Name                           */
/*                                                                               */
/* Input(s)           : e1        Pointer to First Entry                         */
/* Input(s)           : e2        Pointer to Second Entry                        */
/*                                                                               */
/* Output(s)          : None                                                     */
/*                                                                               */
/* Return Value(s)    : 1. WSSUSER_GREATER - If key of first element is greater  */
/*                                           than key of second element          */
/*                      2. WSSUSER_EQUAL   - If key of first element is equal    */
/*                                           to key of second element            */
/*                      3. WSSUSER_LESSER  - If key of first element is lesser   */
/*                                           than key of second element          */
/*********************************************************************************/
INT4
WssUserCompareUserNameAndMac (tRBElem * e1, tRBElem * e2)
{
    tUserMappingListEntry *pRBNode1 = (tUserMappingListEntry *) e1;
    tUserMappingListEntry *pRBNode2 = (tUserMappingListEntry *) e2;

    if (pRBNode1->u4UserNameLen > pRBNode2->u4UserNameLen)
    {
        return WSSUSER_GREATER;
    }
    else if (pRBNode1->u4UserNameLen < pRBNode2->u4UserNameLen)
    {
        return WSSUSER_LESSER;
    }

    /* Both length's are equal. checking the contents now */
    if (MEMCMP (pRBNode1->au1UserName,
                pRBNode2->au1UserName, pRBNode1->u4UserNameLen) > 0)
    {
        return WSSUSER_GREATER;
    }
    else if (MEMCMP (pRBNode1->au1UserName,
                     pRBNode2->au1UserName, pRBNode1->u4UserNameLen) < 0)
    {
        return WSSUSER_LESSER;
    }

    if ((MEMCMP (pRBNode1->staMac, pRBNode2->staMac, sizeof (tMacAddr))) > 0)
    {
        return WSSUSER_GREATER;
    }
    else if ((MEMCMP (pRBNode1->staMac,
                      pRBNode2->staMac, sizeof (tMacAddr))) < 0)
    {
        return WSSUSER_LESSER;
    }
    return WSSUSER_EQUAL;
}

/*********************************************************************************/
/* Function Name      : WssUserSessNameCmp                                       */
/*                                                                               */
/* Description        : This function compares the two User Names based on       */
/*                      length and content                                       */
/*                                                                               */
/* Input(s)           : e1        Pointer to First User Name                     */
/* Input(s)           : e2        Pointer to Second User Name                    */
/*                                                                               */
/* Output(s)          : None                                                     */
/*                                                                               */
/* Return Value(s)    : 1. WSSUSER_GREATER - If key of first element is greater  */
/*                                           than key of second element          */
/*                      2. WSSUSER_EQUAL   - If key of first element is equal    */
/*                                           to key of second element            */
/*                      3. WSSUSER_LESSER  - If key of first element is lesser   */
/*                                           than key of second element          */
/*********************************************************************************/
INT4
WssUserSessNameCmp (tRBElem * e1, tRBElem * e2)
{
    tUserSessionEntry  *pUserSessionName1 = (tUserSessionEntry *) e1;
    tUserSessionEntry  *pUserSessionName2 = (tUserSessionEntry *) e2;

    if (pUserSessionName1->u4UserNameLen > pUserSessionName2->u4UserNameLen)
    {
        return WSSUSER_GREATER;
    }
    else if (pUserSessionName1->u4UserNameLen <
             pUserSessionName2->u4UserNameLen)
    {
        return WSSUSER_LESSER;
    }
    /* Both length's are equal. checking the contents now */
    if (MEMCMP (pUserSessionName1->au1UserName,
                pUserSessionName2->au1UserName,
                pUserSessionName1->u4UserNameLen) > 0)
    {
        return WSSUSER_GREATER;
    }
    else if (MEMCMP (pUserSessionName1->au1UserName,
                     pUserSessionName2->au1UserName,
                     pUserSessionName1->u4UserNameLen) < 0)
    {
        return WSSUSER_LESSER;
    }
    if ((MEMCMP (pUserSessionName1->staMac,
                 pUserSessionName2->staMac, sizeof (tMacAddr))) > 0)
    {
        return WSSUSER_GREATER;
    }
    else if ((MEMCMP (pUserSessionName1->staMac,
                      pUserSessionName2->staMac, sizeof (tMacAddr))) < 0)
    {
        return WSSUSER_LESSER;
    }
    return WSSUSER_EQUAL;

}

/*********************************************************************************/
/* Function Name      : WssUserNameCmp                                           */
/*                                                                               */
/* Description        : This function compares the two User Names based on       */
/*                      length and content                                       */
/*                                                                               */
/* Input(s)           : e1        Pointer to First User Name                     */
/* Input(s)           : e2        Pointer to Second User Name                    */
/*                                                                               */
/* Output(s)          : None                                                     */
/*                                                                               */
/* Return Value(s)    : 1. WSSUSER_GREATER - If key of first element is greater  */
/*                                           than key of second element          */
/*                      2. WSSUSER_EQUAL   - If key of first element is equal    */
/*                                           to key of second element            */
/*                      3. WSSUSER_LESSER  - If key of first element is lesser   */
/*                                           than key of second element          */
/*********************************************************************************/
INT4
WssUserNameCmp (tRBElem * e1, tRBElem * e2)
{
    tUserNameEntry     *pRBNode1 = (tUserNameEntry *) e1;
    tUserNameEntry     *pRBNode2 = (tUserNameEntry *) e2;

    if (pRBNode1->u4WlanIndex < pRBNode2->u4WlanIndex)
    {
        return WSSUSER_LESSER;
    }
    else if (pRBNode1->u4WlanIndex > pRBNode2->u4WlanIndex)
    {
        return WSSUSER_GREATER;
    }

    if (pRBNode1->u4UserNameLen > pRBNode2->u4UserNameLen)
    {
        return WSSUSER_GREATER;
    }
    else if (pRBNode1->u4UserNameLen < pRBNode2->u4UserNameLen)
    {
        return WSSUSER_LESSER;
    }

    /* Both length's are equal. checking the contents now */
    if (MEMCMP (pRBNode1->au1UserName,
                pRBNode2->au1UserName, pRBNode1->u4UserNameLen) > 0)
    {
        return WSSUSER_GREATER;
    }
    else if (MEMCMP (pRBNode1->au1UserName,
                     pRBNode2->au1UserName, pRBNode1->u4UserNameLen) < 0)
    {
        return WSSUSER_LESSER;
    }
    return WSSUSER_EQUAL;
}

/*****************************************************************************/
/* Function Name      : UserLock                                             */
/*                                                                           */
/* Description        : This function is called to Lock the semaphore for    */
/*                      proper processing of multiple events at a time       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
UserLock (VOID)
{
    if (gWssUserGlobals.UserProtocolSemId != 0)
    {
        USER_TAKE_PROTOCOL_SEMAPHORE (gWssUserGlobals.UserProtocolSemId);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/*****************************************************************************/
/* Function Name      : UserUnLock                                           */
/*                                                                           */
/* Description        : This function is called to Unlock the semaphore for  */
/*                      proper processing of multiple events at a time       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
INT4
UserUnLock (VOID)
{
    if (gWssUserGlobals.UserProtocolSemId != 0)
    {
        USER_RELEASE_PROTOCOL_SEMAPHORE (gWssUserGlobals.UserProtocolSemId);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/*****************************************************************************
 *                                                                           *
 * Function     : WssUserGetRestrictStaMac                         *
 *                                                                           *
 * Description  : This function is called to verify whether Correponding     *
 *                Station MAC is configured in Restrict Mac Address table    *
 *                                                                           *
 * Input        : staMacAddr :- Station Mac Address                          *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : WSSUSER_SUCCESS/WSSUSER_FAILURE                            *
 *                                                                           *
 *****************************************************************************/
INT4
WssUserGetRestrictStaMac (tMacAddr staMacAddr)
{
    tUserMacAccessListEntry UserMacAccessList;
    tUserMacAccessListEntry *pUserMacAccessList = NULL;

    MEMSET (&UserMacAccessList, 0, sizeof (tUserMacAccessListEntry));

    MEMCPY (UserMacAccessList.staMac, staMacAddr, MAC_ADDR_LEN);

    /* Retrive the entry based on the index MAC Address */
    pUserMacAccessList = (tUserMacAccessListEntry *) RBTreeGet (gWssUserGlobals.
                                                                WssUserMacAccessListTable,
                                                                (tRBElem *) &
                                                                UserMacAccessList);

    if (pUserMacAccessList == NULL)
    {
        return WSSUSER_FAILURE;
    }

    if (pUserMacAccessList->i4RowStatus != ACTIVE)
    {
        return WSSUSER_FAILURE;
    }
    return WSSUSER_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : WssUserGetRestrictUserName                           *
 *                                                                           *
 * Description  : This function is called to verify whether Correponding     *
 *                UserName is configured in Restrict User table              *
 *                                                                           *
 * Input        : pu1Username :- Pointer to the User Name                    *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : WSSUSER_SUCCESS/WSSUSER_FAILURE                            *
 *                                                                           *
 *****************************************************************************/
INT4
WssUserGetRestrictUserName (UINT1 *pu1Username)
{
    tUserNameAccessListEntry UserNameAccessList;
    tUserNameAccessListEntry *pUserNameList = NULL;

    MEMSET (&UserNameAccessList, 0, sizeof (tUserNameAccessListEntry));

    UserNameAccessList.u4UserNameLen = STRLEN (pu1Username);
    MEMCPY (UserNameAccessList.au1UserName, pu1Username,
            UserNameAccessList.u4UserNameLen);

    /* Retrive the entry based on the index UserName */

    pUserNameList = (tUserNameAccessListEntry *) RBTreeGet (gWssUserGlobals.
                                                            WssUserNameAccessListTable,
                                                            (tRBElem *) &
                                                            UserNameAccessList);

    if (pUserNameList == NULL)
    {
        return WSSUSER_FAILURE;
    }
    if (pUserNameList->i4RowStatus != ACTIVE)
    {
        return WSSUSER_FAILURE;
    }
    return WSSUSER_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : WssUserGetMacMappingEntry                                  *
 *                                                                           *
 * Description  : This function is called to verify whether Correponding     *
 *                Mac Mapping Entry is configured in the DB or not           *
 *                                                                           *
 * Input        : pu1Username :- Pointer to the User Name                    *
 *                staMacAddr  :- Station MAC Address                         *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : WSSUSER_SUCCESS/WSSUSER_FAILURE                            *
 *                                                                           *
 *****************************************************************************/
INT4
WssUserGetMacMappingEntry (UINT1 *pu1Username, tMacAddr staMacAddr)
{
    tUserMappingListEntry *pUserMappingListEntry = NULL;
    tUserMappingListEntry UserMappingListEntry;
    UINT1               au1UserName[MAX_USER_NAME_LENGTH + 1];
    BOOL1               bUserNameFound = OSIX_FALSE;

    MEMSET (&UserMappingListEntry, 0, sizeof (tUserMappingListEntry));

    UserMappingListEntry.u4UserNameLen = STRLEN (pu1Username);
    MEMCPY (UserMappingListEntry.au1UserName, pu1Username,
            STRLEN (pu1Username));

    /* Index for User Mapping Table is User Name and MAC.
     * Do a GetNext Operation with UserName alone (with MAC as zeros), 
     * to get the first entry of the corresponding User Name 
     */
    pUserMappingListEntry =
        (tUserMappingListEntry *) RBTreeGetNext (gWssUserGlobals.
                                                 WssUserMappingListTable,
                                                 (tRBElem *) &
                                                 UserMappingListEntry, NULL);

    while (pUserMappingListEntry != NULL)
    {
        MEMSET (au1UserName, 0, MAX_USER_NAME_LENGTH + 1);
        STRNCPY (au1UserName, pUserMappingListEntry->au1UserName,
                 pUserMappingListEntry->u4UserNameLen);
        au1UserName[pUserMappingListEntry->u4UserNameLen] = '\0';

        /* Check if the fetched entry contains User Name, same as the input */
        if (STRCMP (au1UserName, pu1Username) == 0)
        {
            /* Set a flag to mark that we are getting atleast one entry in the table,
             * with the user name as input, so that it is used to return a allowed or denied result.
             */
            bUserNameFound = OSIX_TRUE;
        }
        else
        {
            break;
        }

        if (MEMCMP (pUserMappingListEntry->staMac, staMacAddr, MAC_ADDR_LEN) ==
            0)
        {
            /* Since the matching entry is found, return SUCCESS, that 
             * 1) MAC matching as per the above condition.
             * 2) User Name matching as per the previous condition
             */
            return WSSUSER_SUCCESS;
        }

        /* Fetch the next entry with this User Name and Station MAC as input */

        MEMSET (&UserMappingListEntry, 0, sizeof (tUserMappingListEntry));

        MEMCPY (UserMappingListEntry.au1UserName, pu1Username,
                STRLEN (pu1Username));
        UserMappingListEntry.u4UserNameLen = STRLEN (pu1Username);
        MEMCPY (UserMappingListEntry.staMac, pUserMappingListEntry->staMac,
                MAC_ADDR_LEN);

        pUserMappingListEntry =
            (tUserMappingListEntry *) RBTreeGetNext (gWssUserGlobals.
                                                     WssUserMappingListTable,
                                                     (tRBElem *) &
                                                     UserMappingListEntry,
                                                     NULL);

    }

    if (bUserNameFound == OSIX_TRUE)
    {
        /* As we have atleast one entry with the User Name, but not corresponding Station MAC found,
         * return failure/denied */
        return WSSUSER_FAILURE;
    }
    return WSSUSER_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : WssUserRestrictMultipleUserLogin                           *
 *                                                                           *
 * Description  : This function is used to restrict multiple user login      *
 *                through same station at the same time                      *
 *                                                                           *
 *                                                                           *
 * Input        : pu1Username :- Pointer to the User Name                    *
 *                staMacAddr  :- Station MAC Address                         *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : WSSUSER_SUCCESS/WSSUSER_FAILURE                            *
 *                                                                           *
 *****************************************************************************/

INT4
WssUserRestrictMultipleUserLogin (UINT1 *pu1Username, tMacAddr staMacAddr)
{
    tUserSessionEntry  *pUserSessionEntry = NULL;

    pUserSessionEntry =
        (tUserSessionEntry *) RBTreeGetFirst (gWssUserGlobals.
                                              WssUserSessionTable);

    while (pUserSessionEntry != NULL)
    {
        if (MEMCMP (pUserSessionEntry->staMac, staMacAddr, MAC_ADDR_LEN) == 0)
        {
            if (MEMCMP (pUserSessionEntry->au1UserName, pu1Username,
                        MEM_MAX_LEN (pUserSessionEntry->u4UserNameLen,
                                     STRLEN (pu1Username))) != 0)
            {
                return WSSUSER_FAILURE;
            }
        }
        pUserSessionEntry =
            (tUserSessionEntry *) RBTreeGetNext (gWssUserGlobals.
                                                 WssUserSessionTable,
                                                 (tRBElem *) pUserSessionEntry,
                                                 NULL);
    }
    return WSSUSER_SUCCESS;

}

/*****************************************************************************
 *                                                                           *
 * Function     : WssUserDeAuthenticateStation                               *
 *                                                                           *
 * Description  : This function is invoked to Send Deauthentication message  *
 *                to station if the station MAC is configured in Restrict    *
 *                MAC Address DB                                             *
 *                                                                           *
 * Input        : staMacAddr :- Station Mac Address                          *
 *                pbDeleteUser :- Boolean variable TRUE/FALSE to give
 *                notification to  WssUserProcessNotification ()
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : WSSUSER_SUCCESS/WSSUSER_FAILURE                            *
 *                                                                           *
 *****************************************************************************/
INT4
WssUserDeAuthenticateStation (tMacAddr staMacAddr, BOOL1 bDeleteUser,
                              UINT4 u4TerminateCause)
{
    tWssStaWepProcessDB *pWssStaWepProcessDB = NULL;
    tWssifauthDBMsgStruct *pwssStaDB = NULL;

#ifdef RSNA_WANTED
    tWssRSNANotifyParams WssRSNANotifyParams;
#endif

#ifdef RSNA_WANTED
    MEMSET (&WssRSNANotifyParams, 0, sizeof (tWssRSNANotifyParams));
    MEMSET (WssRSNANotifyParams.MLMEDEAUTHIND.au1StaMac, 0, MAC_ADDR_LEN);
#endif

    pWssStaWepProcessDB = WssStaProcessEntryGet (staMacAddr);

    if (pWssStaWepProcessDB == NULL)
    {
        return WSSUSER_FAILURE;
    }
    pwssStaDB =
        (tWssifauthDBMsgStruct *) (VOID *) UtlShMemAllocWssIfAuthDbBuf ();
    if (pwssStaDB == NULL)
    {
        return OSIX_FAILURE;
    }
    MEMSET (pwssStaDB, 0, sizeof (tWssifauthDBMsgStruct));

    if (bDeleteUser == OSIX_TRUE)
    {
        WssUserDeleteSessionEntry (staMacAddr, u4TerminateCause);
    }
    /*  Send the deauthentication message */
    WssRsnaSendDeAuthMsg (pWssStaWepProcessDB->BssIdMacAddr, staMacAddr);

#ifdef RSNA_WANTED
    /* MLME deauth indication */
    MEMCPY (WssRSNANotifyParams.MLMEDEAUTHIND.au1StaMac, staMacAddr,
            MAC_ADDR_LEN);
    WssRSNANotifyParams.u4IfIndex = pWssStaWepProcessDB->u4BssIfIndex;
    WssRSNANotifyParams.eWssRsnaNotifyType = WSSRSNA_DEAUTH_IND;

    WSSUSER_TRC (WSSUSER_MGMT_TRC, "DeAuthentication triggered by WLC");

    /* Send Notification msg to Rsna */
    if (RsnaProcessWssNotification (&WssRSNANotifyParams) == OSIX_FAILURE)
    {
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
        return WSSUSER_FAILURE;
    }
    /* Deleting rsna session key */
    WssStaDeleteRsnaSessionKey (staMacAddr, pWssStaWepProcessDB->u4BssIfIndex);

#endif

#ifdef RFMGMT_WANTED
    /* Deleting RF Client session key */
    if (WssStaRFClientDBDelete (pWssStaWepProcessDB->u4BssIfIndex,
                                staMacAddr) != OSIX_SUCCESS)
    {

    }
#endif

    if (WssIfProcessWssAuthDBMsg (WSS_AUTH_UPDATE_DB, pwssStaDB) !=
        OSIX_SUCCESS)
    {
        WSSUSER_TRC (WSSUSER_ALL_FAILURE_TRC,
                     "WssIfProcessWssAuthDBMsg failed\r\n");
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
        return OSIX_FAILURE;
    }
    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
    return WSSUSER_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : WssUserDeAuthenticateUser                                  *
 *                                                                           *
 * Description  : This function is invoked to Send Deauthentication message  *
 *                to station if the UserName is configured in Restrict       *
 *                User Name DB                                               *
 *                                                                           *
 * Input        : pu1Username :- pointer to the User Name                    *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : WSSUSER_SUCCESS/WSSUSER_FAILURE                            *
 *                                                                           *
 *****************************************************************************/
INT4
WssUserDeAuthenticateUser (UINT1 *pu1Username, UINT4 u4WlanId,
                           UINT4 u4TerminateCause)
{
    tUserSessionEntry  *pUserSessionEntry = NULL;
    tWssStaWepProcessDB *pWssStaWepProcessDB = NULL;
    UINT4               u4TermCause = u4TerminateCause;

    pUserSessionEntry =
        (tUserSessionEntry *) RBTreeGetFirst (gWssUserGlobals.
                                              WssUserSessionTable);

    /* Searching the User Session entry corresponding to the UserName */
    while (pUserSessionEntry != NULL)
    {
        if (u4WlanId != 0)
        {
            if ((MEMCMP (pUserSessionEntry->au1UserName, pu1Username,
                         MAX_USER_NAME_LENGTH) == 0) &&
                (pUserSessionEntry->u4WlanIndex == u4WlanId))
            {
                pWssStaWepProcessDB =
                    WssStaProcessEntryGet (pUserSessionEntry->staMac);

                if (pWssStaWepProcessDB != NULL)
                {
                    WssUserDeAuthenticateStation (pUserSessionEntry->staMac,
                                                  OSIX_TRUE, u4TermCause);
                }
            }
        }
        else
        {
            if (MEMCMP (pUserSessionEntry->au1UserName, pu1Username,
                        MAX_USER_NAME_LENGTH) == 0)
            {
                pWssStaWepProcessDB =
                    WssStaProcessEntryGet (pUserSessionEntry->staMac);

                if (pWssStaWepProcessDB != NULL)
                {
                    WssUserDeAuthenticateStation (pUserSessionEntry->staMac,
                                                  OSIX_TRUE, u4TermCause);
                }
            }

        }
        pUserSessionEntry =
            (tUserSessionEntry *) RBTreeGetNext (gWssUserGlobals.
                                                 WssUserSessionTable,
                                                 (tRBElem *) pUserSessionEntry,
                                                 NULL);
    }
    return WSSUSER_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : WssUserAddSessionEntry                               */
/*                                                                           */
/* Description        : This function is used to add the Usersession entry   */
/*                                                                           */
/* Input(s)           : Pointer to WssUserSessionEntry                       */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : WSSUSER_SUCCESS/WSSUSER_FAILURE                      */
/*****************************************************************************/
INT4
WssUserAddSessionEntry (tUserSessionEntry * pWssUserSessionEntry)
{
    tUserRoleEntry      UserRoleEntry;
    tUserGroupEntry     UserGroupEntry;
    tUserSessionEntry  *pUserSessionEntry = NULL;
    tUserNameEntry      UserNameEntry;
    tUserNameEntry     *pUserNameEntry = NULL;
    tUserSessionEntry  *pUserSessionEntryExist = NULL;
    tWssStaWepProcessDB *pWssStaWepProcessDB = NULL;
    UINT4               u4ProfileIndex = 0;
    UINT4               u4WlanId = 0;
    UINT4               u4TerminateCause = 0;

    MEMSET (&UserRoleEntry, 0, sizeof (tUserRoleEntry));
    MEMSET (&UserGroupEntry, 0, sizeof (tUserGroupEntry));
    MEMSET (&UserNameEntry, 0, sizeof (tUserNameEntry));
    /* Retrive the Station Mac from the local database */
    pWssStaWepProcessDB = WssStaProcessEntryGet (pWssUserSessionEntry->staMac);

    if (pWssStaWepProcessDB == NULL)
    {
        return WSSUSER_FAILURE;
    }

    /* Retrivie the profile Index from the local database */
    if (WssIfGetProfileIfIndex (pWssStaWepProcessDB->u4BssIfIndex,
                                &u4ProfileIndex) == OSIX_FAILURE)
    {
        return WSSUSER_FAILURE;
    }

    /* Retrivie the Wlan Id from the local database */
    if (WssGetCapwapWlanId (u4ProfileIndex, &u4WlanId) == OSIX_FAILURE)
    {
        return WSSUSER_FAILURE;
    }

    /* Allocate Memory for User Session Entry */
    pUserSessionEntry =
        WSSUSER_MEM_ALLOCATE_MEM_BLK (WSSUSER_SESSION_INFO_MEMPOOL_ID);

    if (pUserSessionEntry == NULL)
    {
        return WSSUSER_FAILURE;
    }

    MEMSET (pUserSessionEntry, 0, sizeof (tUserSessionEntry));
    pUserSessionEntry->u4WlanIndex = u4WlanId;

    pUserSessionEntry->u4AllotedBandwidth =
        pWssUserSessionEntry->u4AllotedBandwidth;
    pUserSessionEntry->u4AllotedDLBandwidth =
        pWssUserSessionEntry->u4AllotedDLBandwidth;
    pUserSessionEntry->u4AllotedULBandwidth =
        pWssUserSessionEntry->u4AllotedULBandwidth;
    pUserSessionEntry->u4AllotedVolume = pWssUserSessionEntry->u4AllotedVolume;
    pUserSessionEntry->u4AllotedTime = pWssUserSessionEntry->u4AllotedTime;

    MEMCPY (pUserSessionEntry->staMac, pWssUserSessionEntry->staMac,
            MAC_ADDR_LEN);

    MEMCPY (pUserSessionEntry->au1UserName, pWssUserSessionEntry->au1UserName,
            pWssUserSessionEntry->u4UserNameLen);
    pUserSessionEntry->u4UserNameLen = pWssUserSessionEntry->u4UserNameLen;

    WSSUSER_TRC_ARG6 (WSSUSER_MGMT_TRC,
                      "Creating Session for Station MAC:%x:%x:%x:%x:%x:%x\n",
                      pWssUserSessionEntry->staMac[0],
                      pWssUserSessionEntry->staMac[1],
                      pWssUserSessionEntry->staMac[2],
                      pWssUserSessionEntry->staMac[3],
                      pWssUserSessionEntry->staMac[4],
                      pWssUserSessionEntry->staMac[5]);

    pUserSessionEntry->i4NasPort = pWssUserSessionEntry->i4NasPort;

    pUserSessionEntry->u4StartTime = OsixGetSysUpTime ();    /* This is required to compute the duration */

    pUserSessionEntryExist = (tUserSessionEntry *) RBTreeGet (gWssUserGlobals.
                                                              WssUserSessionTable,
                                                              (tRBElem *)
                                                              pUserSessionEntry);

    if (pUserSessionEntryExist != NULL)
    {
        u4TerminateCause = CALLBACK;
        if (WssUserDeleteSessionEntry
            (pUserSessionEntryExist->staMac,
             u4TerminateCause) != WSSUSER_SUCCESS)
        {
            WSSUSER_TRC (WSSUSER_MGMT_TRC, "WssUserAddSessionEntry:\
                         Deleting the existing session entry failed\n");

            WSSUSER_MEM_RELEASE_MEM_BLK (WSSUSER_SESSION_INFO_MEMPOOL_ID,
                                         pUserSessionEntry);
            return WSSUSER_FAILURE;
        }
    }

    /* Adding the User session in the DB */
    if (RBTreeAdd (gWssUserGlobals.WssUserSessionTable,
                   (tRBElem *) pUserSessionEntry) == RB_FAILURE)
    {
        WSSUSER_MEM_RELEASE_MEM_BLK (WSSUSER_SESSION_INFO_MEMPOOL_ID,
                                     pUserSessionEntry);
        return WSSUSER_FAILURE;
    }

    UserNameEntry.u4WlanIndex = pUserSessionEntry->u4WlanIndex;

    MEMCPY (UserNameEntry.au1UserName, pWssUserSessionEntry->au1UserName,
            pUserSessionEntry->u4UserNameLen);

    UserNameEntry.u4UserNameLen = pUserSessionEntry->u4UserNameLen;

    pUserNameEntry = (tUserNameEntry *) RBTreeGet (gWssUserGlobals.
                                                   WssUserNameTable,
                                                   (tRBElem *) & UserNameEntry);

    if (pUserNameEntry == NULL)
    {
        WssUserAddUserNameEntry (pUserSessionEntry);
    }
    else
    {
        pUserNameEntry->u4SessionCount++;
    }

    gWssUserGlobals.u4LoggedCount++;

    return WSSUSER_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : WssUserDeleteSessionEntry                                  *
 *                                                                           *
 * Description  : This function is used to delete the existing session       *
 *                entry                                                  *
 *                                                                           *
 * Input        : Station Mac Address                                        *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : WSSUSER_SUCCESS/WSSUSER_FAILURE                            *
 *                                                                           *
 *****************************************************************************/
INT4
WssUserDeleteSessionEntry (tMacAddr staMacAddr, UINT4 u4TerminateCause)
{
    tUserSessionEntry  *pUserSessionEntry = NULL;
    UINT1               u1EntryFound = OSIX_FALSE;
    UINT4               u4TermCause = 0;

    pUserSessionEntry =
        (tUserSessionEntry *) RBTreeGetFirst (gWssUserGlobals.
                                              WssUserSessionTable);

    /* Searching the User Session entry corresponding to the station */
    while (pUserSessionEntry != NULL)
    {
        if (MEMCMP (pUserSessionEntry->staMac, staMacAddr, MAC_ADDR_LEN) == 0)
        {
            u1EntryFound = OSIX_TRUE;
            break;
        }
        pUserSessionEntry =
            (tUserSessionEntry *) RBTreeGetNext (gWssUserGlobals.
                                                 WssUserSessionTable,
                                                 (tRBElem *) pUserSessionEntry,
                                                 NULL);
    }

    /* Once the entry found Stop the timer and delete the entry */
    if (u1EntryFound == OSIX_TRUE)
    {
        WSSUSER_TRC_ARG6 (WSSUSER_MGMT_TRC,
                          "Deleting Session for Station MAC:%x:%x:%x:%x:%x:%x\n",
                          pUserSessionEntry->staMac[0],
                          pUserSessionEntry->staMac[1],
                          pUserSessionEntry->staMac[2],
                          pUserSessionEntry->staMac[3],
                          pUserSessionEntry->staMac[4],
                          pUserSessionEntry->staMac[5]);
        u4TermCause = u4TerminateCause;
        WssUserDeleteUserNameEntry (pUserSessionEntry, u4TermCause);
#ifdef RSNA_WANTED
        RsnaDeletePmkCache (staMacAddr);
#endif
        /* Deleting the Entry from the RBTree */
        if (RBTreeRemove (gWssUserGlobals.WssUserSessionTable,
                          (tRBElem *) pUserSessionEntry) == RB_FAILURE)
        {
            return WSSUSER_FAILURE;

        }
        WSSUSER_MEM_RELEASE_MEM_BLK (WSSUSER_SESSION_INFO_MEMPOOL_ID,
                                     pUserSessionEntry);

        gWssUserGlobals.u4LoggedCount--;

    }

    return WSSUSER_SUCCESS;
}

/****************************************************************************
*                                                                           *
* Function     : WssUserUtilGetSSIDName                                     *
*                                                                           *
* Description  : This function retirves SSID Name for the given WLAN Id     *
*                                                                           *
* Input        : u4WlanId - Wlan Id                                         *
*                                                                           *
* Output       : pSSIDName     - SSID Name                                  *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*****************************************************************************/
VOID
WssUserUtilGetSSIDName (UINT4 u4WlanId, UINT1 *pSSIDName)
{
    tWssWlanDB         *pWssWlanDB = NULL;

    pWssWlanDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
    if (pWssWlanDB == NULL)
    {
        return;
    }

    MEMSET (pWssWlanDB, 0, sizeof (tWssWlanDB));
    pWssWlanDB->WssWlanAttributeDB.u2WlanProfileId = (UINT2) u4WlanId;

    pWssWlanDB->WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_PROFILE_ENTRY,
                                  pWssWlanDB) != OSIX_SUCCESS)
    {
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        return;
    }

    WssCfGetDot11DesiredSSID (pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex,
                              pSSIDName);
    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
}

/****************************************************************************
*                                                                           *
* Function     : WssUserGetSessionAttributes                                *
*                                                                           *
* Description  : This function retirves the session attributes (User Name   *
*                and Volume) by acccepting station mac address as input     *
*                                                                           *
* Input        : staMacAddr - Station Mac Address                           *
*                                                                           *
* Output       : pu1UserName - User Name                                    *
*                pu4UsedVolume - Used Volume                                *
*                                                                           *
* Returns      : WSSUSER_SUCCESS/WSSUSER_FAILURE                            *
*                                                                           *
*****************************************************************************/

UINT4
WssUserUtilGetAttributes (tMacAddr staMacAddr, UINT1 *pu1UserName,
                          UINT4 *pu4UsedVolume, UINT4 *pu4BandWidth,
                          UINT4 *pu4DLBandWidth, UINT4 *pu4ULBandWidth)
{
    tUserSessionEntry  *pUserSessionEntry = NULL;

    pUserSessionEntry =
        (tUserSessionEntry *) RBTreeGetFirst (gWssUserGlobals.
                                              WssUserSessionTable);

    while (pUserSessionEntry != NULL)
    {
        if (MEMCMP (pUserSessionEntry->staMac, staMacAddr, MAC_ADDR_LEN) == 0)
        {
            MEMCPY (pu1UserName, pUserSessionEntry->au1UserName,
                    pUserSessionEntry->u4UserNameLen);
            *pu4UsedVolume = pUserSessionEntry->u4UsedVolume;
            *pu4BandWidth = pUserSessionEntry->u4AllotedBandwidth;
            *pu4DLBandWidth = pUserSessionEntry->u4AllotedDLBandwidth;
            *pu4ULBandWidth = pUserSessionEntry->u4AllotedULBandwidth;
            return WSSUSER_SUCCESS;
        }
        pUserSessionEntry =
            (tUserSessionEntry *) RBTreeGetNext (gWssUserGlobals.
                                                 WssUserSessionTable,
                                                 (tRBElem *) pUserSessionEntry,
                                                 NULL);
    }
    return WSSUSER_FAILURE;
}

/****************************************************************************
*                                                                           *
* Function     : WssUserUtilRadAccounting                                   *
*                                                                           *
* Description  : Function to generate random number                         *
*                                                                           *
* Input        : pu1Ptr :- Pointer to the generated random number           *
*                u1Len  :- Length of the random number                      *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*****************************************************************************/
INT4
WssUserUtilRadAccounting (tUserNameEntry * pUserNameEntry, UINT1 u1AcctType)
{
    tRADIUS_INPUT_ACC   RadInputAcct;
    tACCOUNT_STAT       AccountStat;
    INT4                i4RadRetVal = NOT_OK;
    UINT4               u4TxOctets = 0;
    UINT4               u4RxOctets = 0;
    UINT4               u4TxPackets = 0;
    UINT4               u4RxPackets = 0;
    UINT4               u4TxGigaWords = 0;
    UINT4               u4RxGigaWords = 0;

    MEMSET (&RadInputAcct, 0, sizeof (tRADIUS_INPUT_ACC));
    /* Fill the Calling station Id and Called Station Id */
    if (WssUserUtilGetStationIDs (pUserNameEntry, &RadInputAcct) ==
        WSSUSER_FAILURE)
    {
        WSSUSER_TRC (WSSUSER_ALL_FAILURE_TRC, "WssUserUtilRadAccounting:\
                      Failed to fill Called-station-Id and Calling-Station-Id\n");

        return (NOT_OK);
    }

    /* Fill the User Name */
    STRNCPY (RadInputAcct.a_u1UserName,
             pUserNameEntry->au1UserName,
             MEM_MAX_BYTES (MAX_USER_NAME_LENGTH,
                            STRLEN (pUserNameEntry->au1UserName)));

    /* Fill the Session Id */
    MEMCPY (RadInputAcct.a_u1SessionId,
            pUserNameEntry->au1RadAccSessionID,
            MEM_MAX_BYTES (LEN_SESSION_ID,
                           STRLEN (pUserNameEntry->au1RadAccSessionID)));

    /* Fill Auth Type as RADIUS */
    RadInputAcct.u4_AuthType = AUTH_RADIUS;

    /* Fill the Other attributes */
    STRCPY (RadInputAcct.a_u1NasId, WSSUSER_NAS_IDENTIFIER_STRING);
    RadInputAcct.u4_NasPort = pUserNameEntry->i4NasPort;
    RadInputAcct.u4_NasPortType = NO_ATTRIBUTE;

    /* Assign the Acct_status_Type as Start, Stop and Interim-Update */
    RadInputAcct.u4_AccStatusType = u1AcctType;

    MEMSET (&AccountStat, 0, sizeof (tACCOUNT_STAT));

    switch (u1AcctType)
    {
        case ACCT_START:

            RadInputAcct.p_AccountStat = NULL;    /* No Accounting Info for Start Message */
            break;

        case ACCT_STOP:
            if (pUserNameEntry->TerminateCause == 0)
            {
                pUserNameEntry->TerminateCause = USER_REQUEST;
            }
            AccountStat.u4_TerminateCause = pUserNameEntry->TerminateCause;    /* The cause of termination of the session */
            /* Intentional fall through */

        case ACCT_INTERIM_UPDATE:

            if (pUserNameEntry->f4TxBytes > (FLT4) WSSUSER_ACCT_MAX_OCTETS)
            {
                u4TxGigaWords =
                    (UINT4) (pUserNameEntry->f4TxBytes /
                             (FLT4) WSSUSER_ACCT_MAX_OCTETS);
                u4TxOctets =
                    (UINT4) ((pUserNameEntry->f4TxBytes -
                              (FLT4) u4TxGigaWords) * 1024);
                u4TxPackets = (UINT4) ((pUserNameEntry->f4TxBytes / 8) * 1024);

            }
            else
            {
                u4TxOctets = (UINT4) ((pUserNameEntry->f4TxBytes) * 1024);
                u4TxPackets = (u4TxOctets / 8);
            }

            if (pUserNameEntry->f4RxBytes > (FLT4) WSSUSER_ACCT_MAX_OCTETS)
            {
                u4RxGigaWords =
                    (UINT4) (pUserNameEntry->f4RxBytes /
                             (FLT4) WSSUSER_ACCT_MAX_OCTETS);
                u4RxOctets =
                    (UINT4) ((pUserNameEntry->f4RxBytes -
                              (FLT4) u4RxGigaWords) * 1024);
                u4RxPackets = (UINT4) ((pUserNameEntry->f4RxBytes / 8) * 1024);
            }
            else
            {
                u4RxOctets = (UINT4) ((pUserNameEntry->f4RxBytes) * 1024);
                u4RxPackets = (u4RxOctets / 8);
            }

            /* Fill the accounting statistics */
            AccountStat.u4_InputGigaWords = u4RxGigaWords;    /* Total no of bytes received from the user */
            AccountStat.u4_OutputGigaWords = u4TxGigaWords;    /* Total no of bytes sent to the user */
            AccountStat.u4_InputOctets = u4RxOctets;    /* Total no of bytes received from the user */
            AccountStat.u4_OutputOctets = u4TxOctets;    /* Total no of bytes sent to the user */
            AccountStat.u4_InputPackets = u4RxPackets;    /* Total no of packets received from the user */
            AccountStat.u4_OutputPackets = u4TxPackets;    /* Total no of packets sent to user */

            AccountStat.u4_SessionTime = pUserNameEntry->u4UsedTime;    /* Duration of the session */
            RadInputAcct.p_AccountStat = &AccountStat;

            break;

        default:
            WSSUSER_TRC (WSSUSER_ALL_FAILURE_TRC, "WssUserUtilRadAccounting:\
                            Invalid Accounting Type\n");
            return (NOT_OK);

    }

#ifdef  RADIUS_WANTED
    /* Call the RADIUS Client API  */
    i4RadRetVal = radiusAccounting (&RadInputAcct, NULL, WssUserRadResponse, 0);    /* TBD - Need to verify the fourth parameter */
#endif
    if (i4RadRetVal != OK)
    {
        WSSUSER_TRC (WSSUSER_ALL_FAILURE_TRC, "WssUserUtilRadAccounting:\
                     Failed to transmit the packet\n");
        return (NOT_OK);
    }
    WssUserAcctTraceMessages (u1AcctType);

    return (OK);
}

/****************************************************************************
*                                                                           *
* Function     : WssUserAcctTraceMessages                                   *
*                                                                           *
* Description  : Function to Print the Trace messages when Accounting       *
                 packets has been sent                                      *
*                                                                           *
* Input        : u1AcctType - Type of Accounting Message has been sent      *
*                                                                           *
* Output       : NONE                                                       *
*                                                                           *
* Returns      : NONE                                                       *
*                                                                           *
*****************************************************************************/
VOID
WssUserAcctTraceMessages (UINT1 u1AcctType)
{

    switch (u1AcctType)
    {
        case ACCT_START:
            WSSUSER_TRC (WSSUSER_MGMT_TRC, "WssUserUtilRadAccounting:\
                         Accounting Start packet transmitted successfully..\n");
            break;
        case ACCT_INTERIM_UPDATE:

            WSSUSER_TRC (WSSUSER_MGMT_TRC, "WssUserUtilRadAccounting:\
                         Accounting Interim Update packet transmitted Successfully..\n");
            break;
        case ACCT_STOP:

            WSSUSER_TRC (WSSUSER_MGMT_TRC, "WssUserUtilRadAccounting:\
                         Accounting Stop packet transmitted successfully..\n");
            break;
        default:

            WSSUSER_TRC (WSSUSER_ALL_FAILURE_TRC, "WssUserUtilRadAccounting:\
                            Invalid Accounting Type\n");
            return;

    }

    return;

}

/****************************************************************************
*                                                                           *
* Function     : WssUserUtilGetStationIDs                                   *
*                                                                           *
* Description  : Function to get the station id                             *
*                                                                           *
* Input        : pUserNameEntry :- Pointer to the User Name Entry           *
*                pRadInputAcct :- Pointer to the Radius input Acct          *
*                                                                           *
* Output       : au1CalledStationId and au1CallingStationId                 *
*                                                                           *
* Returns      : WSSUSER_FAILURE/ WSSUSER_SUCCESS                           *
*                                                                           *
*****************************************************************************/
UINT4
WssUserUtilGetStationIDs (tUserNameEntry * pUserNameEntry,
                          tRADIUS_INPUT_ACC * pRadInputAcct)
{

    tWssStaWepProcessDB *pWssStaWepProcessDB = NULL;
    tUserSessionEntry  *pUserSessionEntry = NULL;
    UINT1               au1CallingStationId[WSSWLAN_STATION_ID_LEN];
    UINT1               au1CalledStationId[WSSWLAN_STATION_ID_LEN];
    UINT1               au1Dot11DesiredSSID[WSSWLAN_SSID_NAME_LEN];
    INT4                i4CalledStationIdLen = 0;
    INT4                i4CallingStationIdLen = 0;
    UINT1               u1EntryFound = 0;

    MEMSET (au1Dot11DesiredSSID, 0, WSSWLAN_SSID_NAME_LEN);
    MEMSET (au1CallingStationId, 0, WSSWLAN_STATION_ID_LEN);
    MEMSET (au1CalledStationId, 0, WSSWLAN_STATION_ID_LEN);

    pUserSessionEntry =
        (tUserSessionEntry *) RBTreeGetFirst (gWssUserGlobals.
                                              WssUserSessionTable);
    while (pUserSessionEntry != NULL)
    {
        if ((MEMCMP
             (pUserSessionEntry->au1UserName, pUserNameEntry->au1UserName,
              MAX_USER_NAME_LENGTH) == 0)
            && (pUserSessionEntry->u4WlanIndex == pUserNameEntry->u4WlanIndex))
        {
            u1EntryFound = OSIX_TRUE;
            break;
        }
        pUserSessionEntry =
            (tUserSessionEntry *) RBTreeGetNext (gWssUserGlobals.
                                                 WssUserSessionTable,
                                                 (tRBElem *) pUserSessionEntry,
                                                 NULL);
    }
    if (u1EntryFound == OSIX_TRUE)
    {
        pWssStaWepProcessDB = WssStaProcessEntryGet (pUserSessionEntry->staMac);

        if (pWssStaWepProcessDB == NULL)
        {

            WSSUSER_TRC (WSSUSER_ALL_FAILURE_TRC, "WssUserUtilGetStationIDs:\
                    Unable to get the Bssid\n");
            return WSSUSER_FAILURE;
        }

        WssUserUtilGetSSIDName (pUserNameEntry->u4WlanIndex,
                                au1Dot11DesiredSSID);

        SPRINTF ((CHR1 *) au1CalledStationId, "%02x-%02x-%02x-%02x-%02x-%02x:",
                 pWssStaWepProcessDB->BssIdMacAddr[0],
                 pWssStaWepProcessDB->BssIdMacAddr[1],
                 pWssStaWepProcessDB->BssIdMacAddr[2],
                 pWssStaWepProcessDB->BssIdMacAddr[3],
                 pWssStaWepProcessDB->BssIdMacAddr[4],
                 pWssStaWepProcessDB->BssIdMacAddr[5]);

        /*au1CalledStationId[18]  = '\0'; */
        STRCAT (au1CalledStationId, au1Dot11DesiredSSID);
        i4CalledStationIdLen = (INT4) STRLEN (au1CalledStationId);
        au1CalledStationId[i4CalledStationIdLen] = '\0';

        SPRINTF ((CHR1 *) au1CallingStationId, "%02x-%02x-%02x-%02x-%02x-%02x",
                 pUserSessionEntry->staMac[0], pUserSessionEntry->staMac[1],
                 pUserSessionEntry->staMac[2], pUserSessionEntry->staMac[3],
                 pUserSessionEntry->staMac[4], pUserSessionEntry->staMac[5]);

        i4CallingStationIdLen = (INT4) STRLEN (au1CallingStationId);
        au1CallingStationId[i4CallingStationIdLen] = '\0';

        MEMCPY (pRadInputAcct->au1CalledStationId, au1CalledStationId,
                i4CalledStationIdLen);

        MEMCPY (pRadInputAcct->au1CallingStationId, au1CallingStationId,
                i4CallingStationIdLen);
        pRadInputAcct->bCalledStationFlag = OSIX_TRUE;
        pRadInputAcct->bCallingStationFlag = OSIX_TRUE;
    }
    else
    {

        WSSUSER_TRC (WSSUSER_ALL_FAILURE_TRC, "WssUserUtilGetStationIDs:\
                     Entry is not found in user session table\n");
        return WSSUSER_FAILURE;

    }

    return WSSUSER_SUCCESS;

}

/****************************************************************************
*                                                                           *
* Function     : WssUserUtilGetAcctSessionID                                *
*                                                                           *
* Description  : Function to generate random number                         *
*                                                                           *
* Input        : pu1Ptr :- Pointer to the generated random number           *
*                u1Len  :- Length of the random number                      *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*****************************************************************************/
VOID
WssUserUtilGetAcctSessionID (tUserNameEntry * pUserNameEntry)
{
    UINT1              *pu1SessId = NULL;
    tUtlSysPreciseTime  SysPreciseTime;

    pu1SessId = pUserNameEntry->au1RadAccSessionID;
    MEMSET (&SysPreciseTime, 0, sizeof (tUtlSysPreciseTime));
    /* Function used to return the system time in seconds and nanoseconds */
    TmrGetPreciseSysTime (&SysPreciseTime);

    /* Generating the unique session Id with the combination of Username and 
     * current year, month and day and hour, min and secs and micro secs  */

    SPRINTF ((CHR1 *) pu1SessId, "%s-%d%d%d-%d%d%d-%d",
             pUserNameEntry->au1UserName, pUserNameEntry->utlStartTm.tm_year,
             ((pUserNameEntry->utlStartTm.tm_mon) + 1),
             pUserNameEntry->utlStartTm.tm_mday,
             pUserNameEntry->utlStartTm.tm_hour,
             pUserNameEntry->utlStartTm.tm_min,
             pUserNameEntry->utlStartTm.tm_sec,
             (SysPreciseTime.u4NanoSec / 1000));

    return;
}

VOID
WssUserRadResponse (VOID *pInterface)
{
    UNUSED_PARAM (pInterface);
    return;
}

/*****************************************************************************
 *                                                                           *
 * Function     : WssUserProcessVolConsumedInd                               *
 *                                                                           *
 * Description  : This function is used to process the Volume Consumed       *
 *                indication                                                   *
 *                                                                           *
 * Input        : Station Mac Address                                        *
 *                u8BytesConsumed - Bytes Consumed                           *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : WSSUSER_SUCCESS/WSSUSER_FAILURE                            *
 *                                                                           *
 *****************************************************************************/
INT4
WssUserProcessVolConsumedInd (tMacAddr staMacAddr, FLT4 f4TxBytes,
                              FLT4 f4RxBytes)
{
    FLT4                f4UsedBytes = 0;
    FLT4                f4AllotedBytes = 0;
    FLT4                f4UsedTxBytes = 0;
    FLT4                f4UsedRxBytes = 0;
    tUserSessionEntry  *pUserSessionEntry = NULL;
    tUserNameEntry      UserNameEntry;
    tUserNameEntry     *pUserNameEntry = NULL;
    UINT1               u1EntryFound = OSIX_FALSE;
    UINT4               u4TerminateCause = 0;
    tWlanUserTrapInfo   UserTrapInfo;

    MEMSET (&UserNameEntry, 0, sizeof (tUserNameEntry));
    MEMSET (&UserTrapInfo, 0, sizeof (tWlanUserTrapInfo));
    pUserSessionEntry =
        (tUserSessionEntry *) RBTreeGetFirst (gWssUserGlobals.
                                              WssUserSessionTable);

    while (pUserSessionEntry != NULL)
    {
        if (MEMCMP (pUserSessionEntry->staMac, staMacAddr, MAC_ADDR_LEN) == 0)
        {
            u1EntryFound = OSIX_TRUE;
            break;
        }
        pUserSessionEntry =
            (tUserSessionEntry *) RBTreeGetNext (gWssUserGlobals.
                                                 WssUserSessionTable,
                                                 (tRBElem *) pUserSessionEntry,
                                                 NULL);
    }

    if (u1EntryFound == OSIX_TRUE)
    {
        UserNameEntry.u4WlanIndex = pUserSessionEntry->u4WlanIndex;
        MEMCPY (UserNameEntry.au1UserName, pUserSessionEntry->au1UserName,
                pUserSessionEntry->u4UserNameLen);

        UserNameEntry.u4UserNameLen = pUserSessionEntry->u4UserNameLen;

        pUserNameEntry = (tUserNameEntry *) RBTreeGet (gWssUserGlobals.
                                                       WssUserNameTable,
                                                       (tRBElem *) &
                                                       UserNameEntry);

        if (pUserNameEntry == NULL)
        {
            WSSUSER_TRC (WSSUSER_ALL_FAILURE_TRC,
                         "User Name entry is NULL.....\n");
            return WSSUSER_FAILURE;
        }
        f4UsedBytes =
            (FLT4) pUserNameEntry->u4UsedVolume + ((f4TxBytes + f4RxBytes) -
                                                   (FLT4) pUserSessionEntry->
                                                   u4UsedVolume);
        f4UsedTxBytes =
            pUserNameEntry->f4TxBytes +
            ((f4TxBytes - pUserSessionEntry->f4TxBytes));
        f4UsedRxBytes =
            pUserNameEntry->f4RxBytes +
            ((f4RxBytes - pUserSessionEntry->f4RxBytes));

        f4AllotedBytes = (FLT4) pUserNameEntry->u4AllotedVolume;
        if (f4UsedBytes >= (FLT4) f4AllotedBytes)
        {
            pUserNameEntry->f4TxBytes = f4UsedTxBytes;
            pUserNameEntry->f4RxBytes = f4UsedRxBytes;
            u4TerminateCause = SERVICE_UNAVAILABLE;
            pUserNameEntry->u4UsedVolume = (UINT4) f4UsedBytes;
            /*Calling User-Role Volume Exceeded Trap */
            MEMCPY (UserTrapInfo.au1UserName, UserNameEntry.au1UserName,
                    UserNameEntry.u4UserNameLen);
            UserTrapInfo.u4UsedVol = pUserNameEntry->u4UsedVolume;
            UserTrapInfo.u4WlanId = UserNameEntry.u4WlanIndex;
            WssUserSnmpSendTrap (WSSUSER_VOLUME_EXCEEDED, &UserTrapInfo);
            WssUserDeAuthenticateUser (pUserNameEntry->au1UserName,
                                       pUserNameEntry->u4WlanIndex,
                                       u4TerminateCause);
        }
        else
        {
            pUserNameEntry->u4UsedVolume = (UINT4) f4UsedBytes;
            pUserSessionEntry->u4UsedVolume = (UINT4) (f4TxBytes + f4RxBytes);
            pUserSessionEntry->f4TxBytes = f4TxBytes;
            pUserSessionEntry->f4RxBytes = f4RxBytes;
            pUserNameEntry->f4TxBytes = f4UsedTxBytes;
            pUserNameEntry->f4RxBytes = f4UsedRxBytes;
        }
    }
    return WSSUSER_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : WssUserAddUserNameEntry                              */
/*                                                                           */
/* Description        : This function is used to add the User entry          */
/*                                                                           */
/* Input(s)           : Pointer to WssUserSessionEntry                       */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : WSSUSER_SUCCESS/WSSUSER_FAILURE                      */
/*****************************************************************************/
INT4
WssUserAddUserNameEntry (tUserSessionEntry * pUserSessionEntry)
{
    tUserNameEntry     *pUserNameEntry = NULL;

    /* Allocate Memory for User Name Entry */
    pUserNameEntry =
        WSSUSER_MEM_ALLOCATE_MEM_BLK (WSSUSER_NAME_INFO_MEMPOOL_ID);

    if (pUserNameEntry == NULL)
    {
        return WSSUSER_FAILURE;
    }

    MEMSET (pUserNameEntry, 0, sizeof (tUserNameEntry));

    pUserNameEntry->u4WlanIndex = pUserSessionEntry->u4WlanIndex;

    /* Filling the Attributes Value given from the Radius server */
    pUserNameEntry->u4AllotedBandwidth = pUserSessionEntry->u4AllotedBandwidth;
    pUserNameEntry->u4AllotedDLBandwidth =
        pUserSessionEntry->u4AllotedDLBandwidth;
    pUserNameEntry->u4AllotedULBandwidth =
        pUserSessionEntry->u4AllotedULBandwidth;
    pUserNameEntry->u4AllotedVolume = pUserSessionEntry->u4AllotedVolume;
    pUserNameEntry->u4AllotedTime = pUserSessionEntry->u4AllotedTime;

    MEMCPY (pUserNameEntry->au1UserName, pUserSessionEntry->au1UserName,
            pUserSessionEntry->u4UserNameLen);
    pUserNameEntry->u4UserNameLen = pUserSessionEntry->u4UserNameLen;

    pUserNameEntry->i4NasPort = pUserSessionEntry->i4NasPort;

    pUserNameEntry->u4StartTime = pUserSessionEntry->u4StartTime;

    /* Get the System Date and Start Time, which shall be used to store for 
     * syslog display purpose */
    MEMSET (&(pUserNameEntry->utlStartTm), 0, sizeof (tUtlTm));
    UtlGetTime (&(pUserNameEntry->utlStartTm));

    /* Create Radius Accounting Session ID for this user */
    WssUserUtilGetAcctSessionID (pUserNameEntry);

    /* Adding the User session in the DB */
    if (RBTreeAdd (gWssUserGlobals.WssUserNameTable,
                   (tRBElem *) pUserNameEntry) == RB_FAILURE)
    {
        WSSUSER_MEM_RELEASE_MEM_BLK (WSSUSER_NAME_INFO_MEMPOOL_ID,
                                     pUserNameEntry);
        return WSSUSER_FAILURE;
    }
    pUserNameEntry->u4SessionCount++;
    if (pUserNameEntry->u4AllotedTime != 0)
    {
        WSSUSER_TRC_ARG1 (WSSUSER_MGMT_TRC,
                          "Starting the timer for the duration:%d\n",
                          pUserNameEntry->u4AllotedTime);

        /* This check is added to over come the limitation of FSAP start timer function  for the 
         * maximum values ( > 604800). If the the Alloted Time is greater than maximum Timer Limit, 
         * then starting the timer for WSSUSER_MAX_SESS_TIMER_VALUE (604800)and copy the value to 
         * the Consumed Time  */

        /* The Consumed Time varible will be used in timer expiry function to calculate the remaining timer  
         * value.*/
        if (pUserNameEntry->u4AllotedTime > WSSUSER_MAX_SESS_TIMER_VALUE)
        {
            pUserNameEntry->u4ConsumedTime = WSSUSER_MAX_SESS_TIMER_VALUE;
            /* Starting the session timer for the Maximum Value */
            if (WssUserTmrStartTimer (&(pUserNameEntry->UserSessTmr),
                                      USER_SESS_TIMER_ID,
                                      WSSUSER_MAX_SESS_TIMER_VALUE,
                                      pUserNameEntry) != WSSUSER_SUCCESS)
            {
                WSSUSER_TRC (WSSUSER_ALL_FAILURE_TRC, "WssUserAddNameEntry:\
                                 Start timer Failed\n");
                return WSSUSER_FAILURE;
            }
        }
        else
        {
            /* The Alloted Time is less than WSSUSER_MAX_SESS_TIMER_VALUE,
             * then start the timer as usually for the received Time  and copy
             * the value to the Consumed Time*/
            pUserNameEntry->u4ConsumedTime = pUserSessionEntry->u4AllotedTime;
            if (WssUserTmrStartTimer (&(pUserNameEntry->UserSessTmr),
                                      USER_SESS_TIMER_ID,
                                      pUserNameEntry->u4AllotedTime,
                                      pUserNameEntry) != WSSUSER_SUCCESS)
            {
                WSSUSER_TRC (WSSUSER_ALL_FAILURE_TRC, "WssUserAddNameEntry:\
                                     Start timer Failed\n");
                return WSSUSER_FAILURE;
            }
        }
        if (WssUserTmrStartTimer ((&(pUserNameEntry->UserInterimUpdateTmr)),
                                  USER_INTERIM_UPDATE_TIMER_ID,
                                  USER_INTERIM_UPDATE_TIMOUT_VAL,
                                  pUserNameEntry) != WSSUSER_SUCCESS)
        {
            WSSUSER_TRC (WSSUSER_ALL_FAILURE_TRC, "WssUserAddSessionEntry:\
                         Interim Update timer Failed\n");
            return WSSUSER_FAILURE;
        }

    }
    WssUserUtilRadAccounting (pUserNameEntry, ACCT_START);
    return WSSUSER_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : WssUserDeleteSessionEntry                                  *
 *                                                                           *
 * Description  : This function is used to delete the existing session       *
 *                entry                                                  *
 *                                                                           *
 * Input        : Station Mac Address                                        *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : WSSUSER_SUCCESS/WSSUSER_FAILURE                            *
 *                                                                           *
 *****************************************************************************/
INT4
WssUserDeleteUserNameEntry (tUserSessionEntry * pUserSessionEntry,
                            UINT4 u4TermCause)
{
    tUserNameEntry     *pUserNameEntry = NULL;
    tUserNameEntry      UserNameEntry;
    tUtlTm              utlEndTm;
    tUtlTm             *putlStartTm = NULL;
    UINT1               au1Dot11DesiredSSID[WSSWLAN_SSID_NAME_LEN];
    UINT4               u4CurrentTime = 0;
    UINT4               u4ElapsedTime = 0;
    UINT4               u4ElapsedSec = 0;
    UINT4               u4ElapsedMin = 0;
    UINT4               u4ElapsedHrs = 0;
    UINT1               au1Month[12][4] =
        { "Jan", "Feb", "Mar", "Apr", "May", "Jun",
        "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
    };

    MEMSET (&UserNameEntry, 0, sizeof (tUserNameEntry));

    UserNameEntry.u4WlanIndex = pUserSessionEntry->u4WlanIndex;

    MEMCPY (UserNameEntry.au1UserName, pUserSessionEntry->au1UserName,
            pUserSessionEntry->u4UserNameLen);

    UserNameEntry.u4UserNameLen = pUserSessionEntry->u4UserNameLen;

    pUserNameEntry = (tUserNameEntry *) RBTreeGet (gWssUserGlobals.
                                                   WssUserNameTable,
                                                   (tRBElem *) & UserNameEntry);

    if (pUserNameEntry != NULL)
    {
        /* Once the entry found Stop the timer and delete the entry */
        if (pUserNameEntry->u4SessionCount == 1)
        {
            if (pUserNameEntry->u4AllotedTime != 0)
            {
                WSSUSER_TRC (WSSUSER_MGMT_TRC,
                             "Stopping the session timer.....\n");
                /* Stop the timer */
                if (WssUserTmrStopTimer (&(pUserNameEntry->UserSessTmr)) !=
                    WSSUSER_SUCCESS)
                {
                    WSSUSER_TRC (WSSUSER_ALL_FAILURE_TRC,
                                 "WssUserDeleteSessionEntry:\
                Stop Tmr Failed\n");
                }
                if (WssUserTmrStopTimer
                    (&(pUserNameEntry->UserInterimUpdateTmr)) !=
                    WSSUSER_SUCCESS)
                {
                    WSSUSER_TRC (WSSUSER_ALL_FAILURE_TRC,
                                 "WssUserDeleteSessionEntry:\
                                 Stop Tmr Failed\n");
                }

            }

            /* Get the Time stamp to display the session's end time in the syslog */
            MEMSET (&utlEndTm, 0, sizeof (tUtlTm));
            UtlGetTime (&utlEndTm);

            /* Get the SysUpTime to compute the duration of the session */
            u4CurrentTime = OsixGetSysUpTime ();
            u4ElapsedTime = (u4CurrentTime - (pUserNameEntry->u4StartTime));
            pUserNameEntry->u4UsedTime = u4ElapsedTime;
            pUserNameEntry->TerminateCause = u4TermCause;

            /* Send a RADIUS Accounting - STOP message */
            WssUserUtilRadAccounting (pUserNameEntry, ACCT_STOP);

#ifdef SYSLOG_WANTED

            /* Convert the ElapsedTime in Seconds - to hours, minutes and seconds, to 
             * to be displayed in syslog */

            u4ElapsedSec = (UINT4) (u4ElapsedTime % SECS_IN_MINUTE);
            u4ElapsedMin =
                (UINT4) ((u4ElapsedTime / SECS_IN_MINUTE) % MINUTES_IN_HOUR);
            u4ElapsedHrs =
                (UINT4) ((u4ElapsedTime / SECS_IN_HOUR) % HOURS_IN_DAY);

            MEMSET (au1Dot11DesiredSSID, 0, WSSWLAN_SSID_NAME_LEN);

            WssUserUtilGetSSIDName (pUserNameEntry->u4WlanIndex,
                                    au1Dot11DesiredSSID);

            putlStartTm = &(pUserNameEntry->utlStartTm);

            SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gWssUserGlobals.u4CDRSysLogId,
                          ", %s-%02d-%04d, %s, %x:%x:%x:%x:%x:%x, %s, %s-%02d-%04d %02d:%02d:%02d,"
                          " %s-%02d-%04d %02d:%02d:%02d,%d hrs %d mins %d secs ,Used Volume : %d ",
                          au1Month[(utlEndTm.tm_mon)], utlEndTm.tm_mday,
                          utlEndTm.tm_year, pUserNameEntry->au1UserName,
                          pUserSessionEntry->staMac[0],
                          pUserSessionEntry->staMac[1],
                          pUserSessionEntry->staMac[2],
                          pUserSessionEntry->staMac[3],
                          pUserSessionEntry->staMac[4],
                          pUserSessionEntry->staMac[5], au1Dot11DesiredSSID,
                          au1Month[(putlStartTm->tm_mon)], putlStartTm->tm_mday,
                          putlStartTm->tm_year, putlStartTm->tm_hour,
                          putlStartTm->tm_min, putlStartTm->tm_sec,
                          au1Month[(utlEndTm.tm_mon)], utlEndTm.tm_mday,
                          utlEndTm.tm_year, utlEndTm.tm_hour, utlEndTm.tm_min,
                          utlEndTm.tm_sec, u4ElapsedHrs, u4ElapsedMin,
                          u4ElapsedSec, pUserNameEntry->u4UsedVolume));

#endif

            /* Deleting the Entry from the RBTree */
            if (RBTreeRemove (gWssUserGlobals.WssUserNameTable,
                              (tRBElem *) pUserNameEntry) == RB_FAILURE)
            {
                return WSSUSER_FAILURE;

            }
            WSSUSER_MEM_RELEASE_MEM_BLK (WSSUSER_NAME_INFO_MEMPOOL_ID,
                                         pUserNameEntry);
        }
        else
        {
            pUserNameEntry->u4SessionCount--;
        }
    }

    return WSSUSER_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : WssUserSnmpSendTrap                                        *
 *                                                                           *
 * Description  : This function is used to send traps of User Role Management*
 *                module.                                                    *
 *                                                                           *
 * Input        : u1TrapId  -  Trap ID                                       *
 *                pTrapInfo -  Pointer to a structure containing the details *
 *                             of trap to be generated                       *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
PUBLIC VOID
WssUserSnmpSendTrap (UINT1 u1TrapId, VOID *pTrapInfo)
{
    tSNMP_VAR_BIND     *pVbList = NULL, *pStartVb = NULL;
    tSNMP_OID_TYPE     *pEnterpriseOid = NULL;
    tSNMP_COUNTER64_TYPE u8CounterVal = { 0, 0 };
    UINT1               au1UserNameBuf[USER_NAME_LENGTH];
    UINT4               au4snmpTrapOid[] = { 1, 3, 6, 1, 6, 3, 1, 1, 4, 1, 0 };
    UINT4               au4UserTrapOid[] =
        { 1, 3, 6, 1, 4, 1, 29601, 2, 90, 5, 0 };
    UINT4               au4UserWlanIndexOid[] =
        { 1, 3, 6, 1, 4, 1, 29601, 2, 90, 3, 1, 1, 3 };
    UINT4               au4NtfUserNameOid[] =
        { 1, 3, 6, 1, 4, 1, 29601, 2, 90, 4, 1, 2 };
    UINT4               au4UserStationMacOid[] =
        { 1, 3, 6, 1, 4, 1, 29601, 2, 90, 4, 1, 1 };
    UINT4               au4UserUsedTimeOid[] =
        { 1, 3, 6, 1, 4, 1, 29601, 2, 90, 3, 1, 1, 8 };
    UINT4               au4UserUsedVolumeOid[] =
        { 1, 3, 6, 1, 4, 1, 29601, 2, 90, 3, 1, 1, 7 };
    tSNMP_OID_TYPE     *pSnmpTrapOid = NULL;
    tWlanUserTrapInfo  *pUserTrapInfo = NULL;
    tSNMP_OCTET_STRING_TYPE *pOString = NULL;

    /* Checking whether the User Role Trap is enabled or not.
       It can be enabled using the CLI "User-role traps enable" */

    if (gWssUserGlobals.u4UserTrapStatus != WSSUSER_TRAP_ENABLE)
    {
        return;
    }

    pUserTrapInfo = (tWlanUserTrapInfo *) pTrapInfo;

    /*The structure "tUserSessionEntry" is used to retrive the 
       Station Mac Address from the RB Tree using WLANIndex and UserName */
    tUserSessionEntry  *pUserSessionEntry = NULL;

    /* The format of SNMP Trap varbinding which is being send using
     * SNMP_AGT_RIF_Notify_v2Trap() is as given below:
     *
     *
     ********************************************************************************
     *                   *                   *    Varbind     *     Varbind    *
     * sysUpTime = Time  * snmpTrapOID = OID * OID# 1 = Value * OID# 2 = Value *
     *                   *                   *                *                *
     ********************************************************************************
     *
     * First Var binding sysUpTime = Time - is filled by the SNMP_AGT_RIF_Notify_v2Trap().
     * Second Var binding snmpTrapOID - OID should be first Varbind in our list to the api 
     SNMP_AGT_RIF_Notify_v2Trap().
     * The remaining Varbind are updated based on the type of the trap, which
     * are going to be generated.
     *
     */

    /* Allocate the memory for the Varbind for snmpTrapOID MIB object OID and
     * the OID of the MIB object for those trap is going to be generated.
     */

    /* Allocate a memory for storing snmpTrapOID OID and assign it */
    pSnmpTrapOid = alloc_oid (WSSSTA_TRAP_OID_LEN);
    if (pSnmpTrapOid == NULL)
    {
        return;
    }

    /* Assigning TrapOid to EnterpriseOid and binding it to packet. */
    MEMCPY (pSnmpTrapOid->pu4_OidList, au4snmpTrapOid, sizeof (au4snmpTrapOid));
    pSnmpTrapOid->u4_Length = sizeof (au4snmpTrapOid) / sizeof (UINT4);

    /* Allocate a memory for storing OID Value which is the OID of the
     * Notification MIB object and assign it */
    pEnterpriseOid = alloc_oid (WSSSTA_TRAP_OID_LEN);
    if (pEnterpriseOid == NULL)
    {
        SNMP_FreeOid (pSnmpTrapOid);
        return;
    }

    /* Assigning TrapOid to EnterpriseOid and binding it to packet. */
    MEMCPY (pEnterpriseOid->pu4_OidList, au4UserTrapOid,
            sizeof (au4UserTrapOid));
    pEnterpriseOid->u4_Length = sizeof (au4UserTrapOid) / sizeof (UINT4);

    pEnterpriseOid->pu4_OidList[pEnterpriseOid->u4_Length++] = u1TrapId;

    /* Form a VarBind for snmpTrapOID OID and its value */
    pVbList = (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pSnmpTrapOid,
                                                       SNMP_DATA_TYPE_OBJECT_ID,
                                                       0L, 0, NULL,
                                                       pEnterpriseOid,
                                                       u8CounterVal);
    /* If the pVbList is empty or values are binded then free the memory
       allocated to pEnterpriseOid and pSnmpTrapOid. */
    if (pVbList == NULL)
    {
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_FreeOid (pEnterpriseOid);
        return;
    }

    /* Assigning pVbList to pStartVb */
    pStartVb = pVbList;

    /* Checking if the The Trap ID Is valid and preforming the 
       var bind which is common to both the SNMP Packets */
    if (u1TrapId == WSSUSER_TIME_EXCEEDED
        || u1TrapId == WSSUSER_VOLUME_EXCEEDED)
    {

        /* Retriving the Station MAC Address using au1UserName and u4WlanId
           by comparing it to the correspoding values present in the RB Tree */
        pUserSessionEntry =
            (tUserSessionEntry *) RBTreeGetFirst (gWssUserGlobals.
                                                  WssUserSessionTable);
        while (pUserSessionEntry != NULL)
        {
            if ((MEMCMP
                 (pUserSessionEntry->au1UserName, pUserTrapInfo->au1UserName,
                  MAX_USER_NAME_LENGTH) == 0)
                && (pUserSessionEntry->u4WlanIndex == pUserTrapInfo->u4WlanId))
            {
                WssUserMacToStr (pUserSessionEntry->staMac,
                                 pUserTrapInfo->au1StaMac);
                break;
            }
            pUserSessionEntry =
                (tUserSessionEntry *) RBTreeGetNext (gWssUserGlobals.
                                                     WssUserSessionTable,
                                                     (tRBElem *)
                                                     pUserSessionEntry, NULL);

        }
        /* Allocating memory for storing OID of fsWssUserStationMacAddress
         *  object and its length*/

        pSnmpTrapOid = alloc_oid (WSSSTA_TRAP_OID_LEN);

        /* Checking for error in memory allocation for pSnmpTrapOid and
         * freeing the memory allocated to pEnterpriseOid if there is an
         * error */

        if (pSnmpTrapOid == NULL)
        {
            SNMP_FreeOid (pEnterpriseOid);
            return;
        }

        /* Assigning the oid and oid length of fsWssUserStationMacAddress to pSnmpTrapOid */
        MEMCPY (pSnmpTrapOid->pu4_OidList, au4UserStationMacOid,
                sizeof (au4UserStationMacOid));
        pSnmpTrapOid->u4_Length =
            sizeof (au4UserStationMacOid) / sizeof (UINT4);

        /* Converting the normal string to OctetString using SNMP_AGT_FormOctetString function */
        pOString = SNMP_AGT_FormOctetString (pUserTrapInfo->au1StaMac,
                                             (INT4) STRLEN (pUserTrapInfo->
                                                            au1StaMac));
        /* Forming a VarBind for Station MAC Address, its value and storing it in pVbList */
        pVbList->pNextVarBind = SNMP_AGT_FormVarBind (pSnmpTrapOid,
                                                      SNMP_DATA_TYPE_OCTET_PRIM,
                                                      0, 0, pOString,
                                                      NULL, u8CounterVal);
        /* Checking if the pVbList is empty and freeing the memory allocated
           to pSnmpTrapOid and pEnterpriseOid if it is NULL */

        if (pVbList->pNextVarBind == NULL)
        {
            SNMP_FreeOid (pSnmpTrapOid);
            SNMP_FreeOid (pEnterpriseOid);
            return;
        }
        pVbList = pVbList->pNextVarBind;
        pVbList->pNextVarBind = NULL;

        /* Allocating the memory for pSnmpTrapOid to store OID of
         * fsWssUserWlanIndex object and length of the OID. */

        pSnmpTrapOid = alloc_oid (WSSSTA_TRAP_OID_LEN);

        /* Checking for error in memory allocation for pSnmpTrapOid and
         * freeing the memory allocated to pEnterpriseOid if there is an
         * error */
        if (pSnmpTrapOid == NULL)
        {
            SNMP_FreeOid (pEnterpriseOid);
            return;
        }
        /* Assigning the OID value and its length of fsWssUserWlanIndex to pSnmpTrapOid */
        MEMCPY (pSnmpTrapOid->pu4_OidList, au4UserWlanIndexOid,
                sizeof (au4UserWlanIndexOid));
        pSnmpTrapOid->u4_Length = sizeof (au4UserWlanIndexOid) / sizeof (UINT4);

        /* Forming a VarBind for OID of fsWssUserWlanIndex and its value */
        pVbList->pNextVarBind =
            (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pSnmpTrapOid,
                                                     SNMP_DATA_TYPE_INTEGER,
                                                     0L,
                                                     (INT4) pUserTrapInfo->
                                                     u4WlanId, NULL, NULL,
                                                     u8CounterVal);
        /* Checking if the pVbList is empty and freeing the memory allocated
           to pSnmpTrapOid and pEnterpriseOid if it is NULL */

        if (pVbList->pNextVarBind == NULL)
        {
            SNMP_FreeOid (pSnmpTrapOid);
            SNMP_FreeOid (pEnterpriseOid);
            return;
        }
        pVbList = pVbList->pNextVarBind;
        pVbList->pNextVarBind = NULL;

        /* Allocating the memory for pSnmpTrapOid to store OID of
         * fsWssNtfUserName object and length of the OID. */

        pSnmpTrapOid = alloc_oid (WSSSTA_TRAP_OID_LEN);

        /* Checking for error in memory allocation for pSnmpTrapOid and
         * freeing the memory allocated to pEnterpriseOid if there is an
         * error */

        if (pSnmpTrapOid == NULL)
        {
            SNMP_FreeOid (pEnterpriseOid);
            return;
        }
        /* Assigning the OID value and its length of fsWssNtfUserName to pSnmpTrapOid */
        MEMCPY (pSnmpTrapOid->pu4_OidList, au4NtfUserNameOid,
                sizeof (au4NtfUserNameOid));
        pSnmpTrapOid->u4_Length = sizeof (au4NtfUserNameOid) / sizeof (UINT4);

        MEMSET (au1UserNameBuf, 0, USER_NAME_LENGTH);
        /* Assigning UserName to UserNameBuf */
        SPRINTF ((CHR1 *) au1UserNameBuf, "%s", pUserTrapInfo->au1UserName);
        /* Converting the normal string to OctetString using SNMP_AGT_FormOctetString function */
        pOString =
            SNMP_AGT_FormOctetString ((UINT1 *) au1UserNameBuf,
                                      (INT4) STRLEN (au1UserNameBuf));
        /* Forming a VarBind for User Name, its value and storing it in pVbList */
        pVbList->pNextVarBind = SNMP_AGT_FormVarBind (pSnmpTrapOid,
                                                      SNMP_DATA_TYPE_OCTET_PRIM,
                                                      0, 0, pOString,
                                                      NULL, u8CounterVal);
        /* Checking if the pVbList is empty and freeing the memory allocated
           to pSnmpTrapOid and pEnterpriseOid if it is NULL */

        if (pVbList->pNextVarBind == NULL)
        {
            SNMP_FreeOid (pSnmpTrapOid);
            SNMP_FreeOid (pEnterpriseOid);
            return;
        }
        pVbList = pVbList->pNextVarBind;
        pVbList->pNextVarBind = NULL;

        /* Allocating the memory for pSnmpTrapOid to store OID of
         * fsWssUserUsedTime object and length of the OID. */

        pSnmpTrapOid = alloc_oid (WSSSTA_TRAP_OID_LEN);

        /* Checking for error in memory allocation for pSnmpTrapOid and
         * freeing the memory allocated to pEnterpriseOid if there is an
         * error */

        if (pSnmpTrapOid == NULL)
        {
            SNMP_FreeOid (pEnterpriseOid);
            return;
        }

        switch (u1TrapId)
        {
            case WSSUSER_TIME_EXCEEDED:
                /* Assigning the OID value and its length of fsWssUserUsedTime to
                 * pSnmpTrapOid */

                MEMCPY (pSnmpTrapOid->pu4_OidList, au4UserUsedTimeOid,
                        sizeof (au4UserUsedTimeOid));
                pSnmpTrapOid->u4_Length =
                    sizeof (au4UserUsedTimeOid) / sizeof (UINT4);

                pVbList->pNextVarBind =
                    (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pSnmpTrapOid,
                                                             SNMP_DATA_TYPE_INTEGER,
                                                             0L,
                                                             (INT4)
                                                             pUserTrapInfo->
                                                             u4UsedTime, NULL,
                                                             NULL,
                                                             u8CounterVal);
                /* Checking if the pVbList is empty and freeing the memory allocated
                   to pSnmpTrapOid and pEnterpriseOid if it is NULL */

                if (pVbList->pNextVarBind == NULL)
                {
                    SNMP_FreeOid (pSnmpTrapOid);
                    SNMP_FreeOid (pEnterpriseOid);
                    return;
                }
                pVbList = pVbList->pNextVarBind;
                pVbList->pNextVarBind = NULL;

                break;

            case WSSUSER_VOLUME_EXCEEDED:
                /* Assigning the OID value and its length of fsWssUserUsedVolume to
                 * pSnmpTrapOid */

                MEMCPY (pSnmpTrapOid->pu4_OidList, au4UserUsedVolumeOid,
                        sizeof (au4UserUsedVolumeOid));
                pSnmpTrapOid->u4_Length =
                    sizeof (au4UserUsedVolumeOid) / sizeof (UINT4);

                pVbList->pNextVarBind =
                    (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pSnmpTrapOid,
                                                             SNMP_DATA_TYPE_INTEGER,
                                                             0L,
                                                             (INT4)
                                                             pUserTrapInfo->
                                                             u4UsedVol, NULL,
                                                             NULL,
                                                             u8CounterVal);
                /* Checking if the pVbList is empty and freeing the memory allocated
                   to pSnmpTrapOid and pEnterpriseOid if it is NULL */

                if (pVbList->pNextVarBind == NULL)
                {
                    SNMP_FreeOid (pSnmpTrapOid);
                    SNMP_FreeOid (pEnterpriseOid);
                    return;
                }
                pVbList = pVbList->pNextVarBind;
                pVbList->pNextVarBind = NULL;

                break;
        }

#ifdef SNMP_2_WANTED
        /* The following API sends the Trap info to the FutureSNMP Agent. */
        SNMP_AGT_RIF_Notify_v2Trap (pStartVb);

        /* Replace this function with the Corresponding Function provided by
         * SNMP Agent for Notifying Traps.
         */
#endif

#ifdef DEBUG_WANTED
        DbgNotifyTrap (u1TrapId, pTrapInfo);
#endif
    }
    else
    {
        /*If Invalid Trap Id is passed */
        return;
    }

}

/***************************************************************************
 *                                                                         *
 *     Function Name : WssUserMacToStr                                     *
 *                                                                         *
 *     Description   : This function converts the given mac address        *
 *                     in to a string of form aa:aa:aa:aa:aa:aa            *
 *                                                                         *
 *     Input(s)      : pMacAdrr   : Pointer to the Mac address value array *
 *                     pu1Temp    : Pointer to the converted mac address   *
 *                                  string.(The string must be of length   *
 *                                  21 bytes minimum)                      *
 *                                                                         *
 *     Output(s)     : NULL                                                *
 *                                                                         *
 *     Returns       : NULL                                                *
 *                                                                         *
 ***************************************************************************/

VOID
WssUserMacToStr (UINT1 *pMacAddr, UINT1 *pu1Temp)
{

    UINT1               u1Byte;

    if (!(pMacAddr) || !(pu1Temp))
        return;

    for (u1Byte = 0; u1Byte < MAC_LEN; u1Byte++)
    {
        pu1Temp += SPRINTF ((CHR1 *) pu1Temp, "%02x:", *(pMacAddr + u1Byte));
    }
    SPRINTF ((CHR1 *) (pu1Temp - 1), "   ");

}

/************ End of USERUTIL.C ************/
