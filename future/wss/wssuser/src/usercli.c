/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: usercli.c,v 1.2 2017/05/23 14:16:59 siva Exp $
*
* Description: This file contains the WSSSUER CLI related routines
*********************************************************************/

/* SOURCE FILE HEADER :
 *
 *  ---------------------------------------------------------------------------
 * |  FILE NAME             : usercli.c                                        |
 * |                                                                           |
 * |  PRINCIPAL AUTHOR      :                                                  |
 * |                                                                           |
 * |  SUBSYSTEM NAME        : WSSUSER                                          |
 * |                                                                           |
 * |  MODULE NAME           : WSSUSER CLI configurations                       |
 * |                                                                           |
 * |  LANGUAGE              : C                                                |
 * |                                                                           |
 * |  TARGET ENVIRONMENT    :                                                  |
 * |                                                                           |
 * |  DATE OF FIRST RELEASE :                                                  |
 * |                                                                           |
 * |  DESCRIPTION           : CLI interfaces for WSSUSER ROLE MIB.             |
 *  ---------------------------------------------------------------------------
 *
 */
#ifndef WSSUSERCLI_C
#define WSSUSERCLI_C

#include "userinc.h"
#include "fsuserlw.h"

/*****************************************************************************/
/*                                                                           */
/* FUNCTION NAME    : cli_process_wssuser_cmd                                */
/*                                                                           */
/* DESCRIPTION      : This function takes in variable no. of arguments       */
/*                    and process the commands for the WSSUSER module.       */
/*                    defined in usercli.h                                   */
/*                                                                           */
/* INPUT            : CliHandle -  CLIHandler                                */
/*                    u4Command -  Command Identifier                        */
/*                                                                           */
/* OUTPUT           : NONE                                                   */
/*                                                                           */
/* RETURNS          : NONE                                                   */
/*                                                                           */
/*****************************************************************************/
INT4
cli_process_wssuser_command (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    tSNMP_OCTET_STRING_TYPE GroupName;
    tSNMP_OCTET_STRING_TYPE UserName;
    tMacAddr            MacAddress;
    va_list             ap;
    UINT4               u4ProfileId = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4GroupId = 0;
    UINT4               u4WlanId = 0;
    UINT4               u4Time = 0;
    INT4                i4RetStatus = CLI_FAILURE;
    UINT1              *args[CLI_MAX_ARGS] = { NULL };
    UINT1               u1Argno = 0;

    MEMSET (&GroupName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&UserName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (MacAddress, 0, MAC_ADDR_LEN);

    va_start (ap, u4Command);

    while (u1Argno < CLI_MAX_ARGS)
    {
        args[u1Argno++] = va_arg (ap, UINT1 *);
    }

    CLI_SET_ERR (0);
    va_end (ap);

    CliRegisterLock (CliHandle, UserLock, UserUnLock);

    /* Take the User Module Lock */
    UserLock ();

    switch (u4Command)
    {

/**************************/
/* CONFIGURATION COMMANDS */
/*************************/

            /* Create a User Group with Group Id as index */
        case CLI_WSSUSER_GROUP_CREATE:

            MEMCPY (&u4GroupId, args[WSSUSER_CLI_FIRST_ARG], sizeof (UINT4));
            i4RetStatus = WssUserCliCreateGroup (CliHandle, u4GroupId);
            break;

            /* Delete a User Group with Group Id as index */
        case CLI_WSSUSER_GROUP_DELETE:

            MEMCPY (&u4GroupId, args[WSSUSER_CLI_FIRST_ARG], sizeof (UINT4));
            i4RetStatus = WssUserCliDeleteGroup (CliHandle, u4GroupId);
            break;

            /* Update the Group Name in User Group Attribute */
        case CLI_WSSUSER_GROUP_NAME:

            u4GroupId = (UINT4) CLI_GET_GROUP_ID ();
            GroupName.pu1_OctetList = (UINT1 *) (args[WSSUSER_CLI_FIRST_ARG]);
            GroupName.i4_Length = (INT4) (STRLEN (GroupName.pu1_OctetList));
            i4RetStatus =
                WssUserCliSetGroupName (CliHandle, u4GroupId, &GroupName);
            break;

            /* Update Bandwidth value in User Group Attribute */
        case CLI_WSSUSER_GROUP_BANDWIDTH:

            u4GroupId = (UINT4) CLI_GET_GROUP_ID ();
            i4RetStatus = WssUserCliSetGroupBandWidth (CliHandle, u4GroupId,
                                                       (UINT4) (CLI_PTR_TO_I4
                                                                (args
                                                                 [WSSUSER_CLI_FIRST_ARG])));
            break;

        case CLI_WSSUSER_GROUP_DLBANDWIDTH:

            u4GroupId = (UINT4) CLI_GET_GROUP_ID ();
            i4RetStatus = WssUserCliSetGroupDLBandWidth (CliHandle, u4GroupId,
                                                         (UINT4) (CLI_PTR_TO_I4
                                                                  (args
                                                                   [WSSUSER_CLI_FIRST_ARG])));
            break;

        case CLI_WSSUSER_GROUP_ULBANDWIDTH:

            u4GroupId = (UINT4) CLI_GET_GROUP_ID ();
            i4RetStatus = WssUserCliSetGroupULBandWidth (CliHandle, u4GroupId,
                                                         (UINT4) (CLI_PTR_TO_I4
                                                                  (args
                                                                   [WSSUSER_CLI_FIRST_ARG])));
            break;

            /* Update the Volume in the User Group Attribute */
        case CLI_WSSUSER_GROUP_VOLUME:

            u4GroupId = (UINT4) CLI_GET_GROUP_ID ();
            i4RetStatus = WssUserCliSetGroupVolume (CliHandle, u4GroupId,
                                                    (UINT4) (CLI_PTR_TO_I4
                                                             (args
                                                              [WSSUSER_CLI_FIRST_ARG])));
            break;

            /* Update the time Value in the User Group Attribute */
        case CLI_WSSUSER_GROUP_TIME:

            u4GroupId = (UINT4) CLI_GET_GROUP_ID ();
            MEMCPY (&u4Time, args[WSSUSER_CLI_FIRST_ARG], sizeof (UINT4));
            i4RetStatus = WssUserCliSetGroupTime (CliHandle, u4GroupId, u4Time);
            break;

            /* Map the users to the corresponding user group */
        case CLI_WSSUSER_ROLE_ADD:

            UserName.pu1_OctetList = (UINT1 *) (args[WSSUSER_CLI_FIRST_ARG]);
            UserName.i4_Length = (INT4) (STRLEN (UserName.pu1_OctetList));
            MEMCPY (&u4WlanId, args[WSSUSER_CLI_SECOND_ARG], sizeof (UINT4));

            if (nmhGetCapwapDot11WlanProfileIfIndex
                (u4WlanId, (INT4 *) (&u4ProfileId)) != SNMP_SUCCESS)
            {
                CLI_SET_ERR (CLI_WSSUSER_WLAN_NOT_CREATED);
                i4RetStatus = WSSUSER_FAILURE;
                break;
            }

            i4RetStatus = WssUserCliAddRole (CliHandle, &UserName, u4ProfileId,
                                             (UINT4) (CLI_PTR_TO_I4
                                                      (args
                                                       [WSSUSER_CLI_THIRD_ARG])));
            break;

            /* Delete the users and its user group mapping */
        case CLI_WSSUSER_ROLE_DELETE:

            UserName.pu1_OctetList = (UINT1 *) (args[WSSUSER_CLI_FIRST_ARG]);
            UserName.i4_Length = (INT4) (STRLEN (UserName.pu1_OctetList));
            MEMCPY (&u4WlanId, args[WSSUSER_CLI_SECOND_ARG], sizeof (UINT4));

            if (nmhGetCapwapDot11WlanProfileIfIndex
                (u4WlanId, (INT4 *) (&u4ProfileId)) != SNMP_SUCCESS)
            {
                CLI_SET_ERR (CLI_WSSUSER_WLAN_NOT_CREATED);
                i4RetStatus = WSSUSER_FAILURE;
                break;
            }
            i4RetStatus =
                WssUserCliDeleteRole (CliHandle, &UserName, u4ProfileId);
            break;

            /* Restrict the user */
        case CLI_WSSUSER_NAME_RESTRICT:

            UserName.pu1_OctetList = (UINT1 *) (args[WSSUSER_CLI_FIRST_ARG]);
            UserName.i4_Length = (INT4) (STRLEN (UserName.pu1_OctetList));
            i4RetStatus = WssUserCliRestrictUserName (CliHandle, &UserName);

            break;

            /* No Restrict the user */
        case CLI_WSSUSER_NAME_NO_RESTRICT:

            UserName.pu1_OctetList = (UINT1 *) (args[WSSUSER_CLI_FIRST_ARG]);
            UserName.i4_Length = (INT4) (STRLEN (UserName.pu1_OctetList));
            i4RetStatus = WssUserCliNoRestrictUserName (CliHandle, &UserName);
            break;

            /* Restrict the station */
        case CLI_WSSUSER_MAC_RESTRICT:

            CliDotStrToMac ((UINT1 *) args[WSSUSER_CLI_FIRST_ARG], MacAddress);
            i4RetStatus = WssUserCliRestrictUserMac (CliHandle, MacAddress);
            break;

            /* No Restrict the station */
        case CLI_WSSUSER_MAC_NO_RESTRICT:

            CliDotStrToMac ((UINT1 *) args[WSSUSER_CLI_FIRST_ARG], MacAddress);
            i4RetStatus = WssUserCliNoRestrictUserMac (CliHandle, MacAddress);
            break;

            /* Mapping the station's mac-address and user name */
        case CLI_WSSUSER_NAME_MAC_MAP_ADD:

            CliDotStrToMac ((UINT1 *) args[WSSUSER_CLI_FIRST_ARG], MacAddress);
            UserName.pu1_OctetList = (UINT1 *) (args[WSSUSER_CLI_SECOND_ARG]);
            UserName.i4_Length = (INT4) (STRLEN (UserName.pu1_OctetList));
            i4RetStatus =
                WssUserCliAddMacMap (CliHandle, MacAddress, &UserName);
            break;

            /* Delete the mapping */
        case CLI_WSSUSER_NAME_MAC_MAP_DELETE:

            CliDotStrToMac ((UINT1 *) args[WSSUSER_CLI_FIRST_ARG], MacAddress);
            UserName.pu1_OctetList = (UINT1 *) (args[WSSUSER_CLI_SECOND_ARG]);
            UserName.i4_Length = (INT4) (STRLEN (UserName.pu1_OctetList));
            i4RetStatus =
                WssUserCliDelMacMap (CliHandle, MacAddress, &UserName);
            break;

            /* Enables the User Role */
        case CLI_WSSUSER_ENABLED:

            i4RetStatus =
                WssUserCliRoleStatus (CliHandle, WSSUSER_MODULE_ENABLED);
            break;

            /* Disables the User Role */
        case CLI_WSSUSER_DISABLED:

            i4RetStatus =
                WssUserCliRoleStatus (CliHandle, WSSUSER_MODULE_DISABLED);
            break;

/*****************/
/* SHOW COMMANDS */
/*****************/

            /* Display User Group details */
        case CLI_WSSUSER_SHOW_USER_GROUP:

            if (args[WSSUSER_CLI_FIRST_ARG] != NULL)
            {
                MEMCPY (&u4GroupId, args[WSSUSER_CLI_FIRST_ARG],
                        sizeof (UINT4));
                i4RetStatus = WssUserCliShowGroupDetails (CliHandle, u4GroupId);
            }

            else
            {
                i4RetStatus = WssUserCliShowGroupDetailsAll (CliHandle);
            }
            break;

            /* Display group details of the user */
        case CLI_WSSUSER_SHOW_USER_ID:

            if (args[WSSUSER_CLI_FIRST_ARG] != NULL)
            {
                UserName.pu1_OctetList =
                    (UINT1 *) (args[WSSUSER_CLI_FIRST_ARG]);
                UserName.i4_Length = (INT4) (STRLEN (UserName.pu1_OctetList));
                MEMCPY (&u4WlanId, args[WSSUSER_CLI_SECOND_ARG],
                        sizeof (UINT4));
                if (u4WlanId != 0)
                {
                    if (nmhGetCapwapDot11WlanProfileIfIndex
                        (u4WlanId, ((INT4 *) &u4ProfileId)) != SNMP_SUCCESS)
                    {
                        CLI_SET_ERR (CLI_WSSUSER_WLAN_NOT_CREATED);
                        i4RetStatus = WSSUSER_FAILURE;
                        break;
                    }

                    if (u4ProfileId == 0)
                    {
                        CLI_SET_ERR (CLI_WSSUSER_WLAN_NOT_CREATED);
                        i4RetStatus = WSSUSER_FAILURE;
                        break;
                    }
                }

                i4RetStatus =
                    WssUserCliShowUserDetails (CliHandle, &UserName,
                                               u4ProfileId);
            }

            else
            {
                i4RetStatus = WssUserCliShowUserDetailsAll (CliHandle);
            }
            break;

            /* Display the Session details */
        case CLI_WSSUSER_SHOW_USER_SESSION:

            if (args[WSSUSER_CLI_FIRST_ARG] != NULL)
            {
                UserName.pu1_OctetList =
                    (UINT1 *) (args[WSSUSER_CLI_FIRST_ARG]);
                UserName.i4_Length = (INT4) (STRLEN (UserName.pu1_OctetList));

                CliDotStrToMac ((UINT1 *) args[WSSUSER_CLI_SECOND_ARG],
                                MacAddress);
                i4RetStatus =
                    WssUserCliShowSessionDetails (CliHandle, &UserName,
                                                  MacAddress);
            }

            else
            {
                i4RetStatus = WssUserCliShowSessionAll (CliHandle);
            }
            break;

            /* Display the mapping details */
        case CLI_WSSUSER_SHOW_MAC_MAP:

            i4RetStatus = WssUserCliShowMacMapAll (CliHandle);
            break;

            /* Display all restricted user name */
        case CLI_WSSUSER_SHOW_RESTRICTED_USER:

            i4RetStatus = WssUserCliShowRestrictedUser (CliHandle);
            break;

            /* Display all restricted station */
        case CLI_WSSUSER_SHOW_RESTRICTED_STATION:

            i4RetStatus = WssUserCliShowRestrictedStation (CliHandle);
            break;

            /* Display User Role Module Status */
        case CLI_WSSUSER_SHOW_MODULE_STATUS:

            i4RetStatus = WssUserCliShowModuleStatus (CliHandle);
            break;

            /* Clear Logged and Blocked User counters */
        case CLI_WSSUSER_CLEAR_COUNTERS:

            i4RetStatus = WssUserCliClearCounters (CliHandle);
            break;

/*******************/
/* DEBUG COMMANDS  */
/*******************/

            /* Enable Debug trace */
        case CLI_WSSUSER_DEBUG:

            i4RetStatus = WssUserCliSetDebugs (CliHandle,
                                               CLI_PTR_TO_I4 (args
                                                              [WSSUSER_CLI_FIRST_ARG]),
                                               CLI_ENABLE);

            break;

            /* Disable Debug trace */
        case CLI_WSSUSER_NO_DEBUG:

            i4RetStatus = WssUserCliSetDebugs (CliHandle,
                                               CLI_PTR_TO_I4 (args
                                                              [WSSUSER_CLI_FIRST_ARG]),
                                               CLI_DISABLE);
            break;
        case CLI_WSSUSER_TRAP_ENABLE:

            i4RetStatus = WssUserCliTrapEnable (CliHandle);
            break;

        case CLI_WSSUSER_TRAP_DISABLE:

            i4RetStatus = WssUserCliTrapDisable (CliHandle);
            break;

            /* Default Case */
        default:

            CliPrintf (CliHandle, "\rERROR: Unknown Command !\r\n");

            /* Release the User Module Lock */
            UserUnLock ();

            return CLI_ERROR;
    }

    /* Display the error string */
    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_WSSUSER_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%s", WssUserCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }

    /* Release the User Module Lock */
    UserUnLock ();

    return i4RetStatus;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : WssUserCliCreateGroup                                   */
/*                                                                           */
/* Description     : This function is used to create User Group              */
/*                                                                           */
/* Input(s)        : CliHandle    - CLI Handler                              */
/*                   u4GroupId    - User Group Id                            */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/

INT4
WssUserCliCreateGroup (tCliHandle CliHandle, UINT4 u4GroupId)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4UserGroupRowStatus = -1;
    UINT1               au1Cmd[MAX_PROMPT_LEN];

    UNUSED_PARAM (CliHandle);

    MEMSET (au1Cmd, 0, sizeof (au1Cmd));

    /* Verify if the received User Group ID is already created.
     * If it is already created, enter into User Group CLI mode and should be 
     * able to edit the attributes.
     * If not already created, create a new entry for this User Group
     * and enter into User Group CLI mode, for editing the attribute values.
     */

    if (nmhGetFsWssUserGroupRowStatus (u4GroupId, &i4UserGroupRowStatus)
        != SNMP_SUCCESS)
    {
        /* Check whether the User Group Id is valid input */

        if (nmhTestv2FsWssUserGroupRowStatus (&u4ErrorCode, u4GroupId,
                                              CREATE_AND_GO) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        /* Create User Group */

        if (nmhSetFsWssUserGroupRowStatus (u4GroupId,
                                           CREATE_AND_GO) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

    }

    /* Change the CLI path to user-group mode */

    SPRINTF ((CHR1 *) au1Cmd, "%s%u", CLI_WSSUSER_GROUP_MODE, u4GroupId);

    if (CliChangePath ((CHR1 *) au1Cmd) == CLI_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to enter into Group mode\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : WssUserCliDeleteGroup                                   */
/*                                                                           */
/* Description     : This function is used to delete User Group              */
/*                                                                           */
/* Input(s)        : CliHandle    - CLI Handler                              */
/*                   u4GroupId    - User Group Id                            */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/

INT4
WssUserCliDeleteGroup (tCliHandle CliHandle, UINT4 u4GroupId)
{
    UINT4               u4ErrorCode = 0;

    UNUSED_PARAM (CliHandle);

    /* Check whether 
     * 1.User Group Id for deletion is default User Group.
     *   If yes, display error message as "Cannot delete Default Group".
     * 2.User Group Id for deletion contain atleast one User associated with it.
     *   If yes, display error message as "Cannot delete, 
     *   User is associated with this group".
     */
    if (nmhTestv2FsWssUserGroupRowStatus (&u4ErrorCode, u4GroupId,
                                          DESTROY) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    /* Delete User Group and Release corresponding MEMPOOL */

    if (nmhSetFsWssUserGroupRowStatus (u4GroupId, DESTROY) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : WssUserCliSetGroupName                                  */
/*                                                                           */
/* Description     : This function is used to set User Group Name            */
/*                                                                           */
/* Input(s)        : CliHandle         - CLI Handler                         */
/*                   u4GroupId         - User Group Id                       */
/*                   pUserGroupName    - User Group Name                     */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/

INT4
WssUserCliSetGroupName (tCliHandle CliHandle, UINT4 u4GroupId,
                        tSNMP_OCTET_STRING_TYPE * pUserGroupName)
{
    UINT4               u4ErrorCode = 0;

    UNUSED_PARAM (CliHandle);

    /* Set the Row Status of User Group Table as NOT_IN_SERVICE -
     * To update its field*/

    if (nmhSetFsWssUserGroupRowStatus (u4GroupId,
                                       NOT_IN_SERVICE) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    /* Check whether the User Group Name is valid input */

    if (nmhTestv2FsWssUserGroupName (&u4ErrorCode, u4GroupId,
                                     pUserGroupName) != SNMP_SUCCESS)
    {
        if (nmhSetFsWssUserGroupRowStatus (u4GroupId, ACTIVE) != SNMP_SUCCESS)
        {
        }
        return CLI_FAILURE;
    }

    /* Update User Group's Name */

    if (nmhSetFsWssUserGroupName (u4GroupId, pUserGroupName) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    /* Change back the Row Status of User Group table as ACTIVE  */

    if (nmhSetFsWssUserGroupRowStatus (u4GroupId, ACTIVE) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : WssUserCliSetGroupBandWidth                             */
/*                                                                           */
/* Description     : This function is used to set User Group BandWidth       */
/*                                                                           */
/* Input(s)        : CliHandle      - CLI Handler                            */
/*                   u4GroupId      - User Group Id                          */
/*                   u4BandWidth    - User Group BandWidth                   */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/
INT4
WssUserCliSetGroupBandWidth (tCliHandle CliHandle, UINT4 u4GroupId,
                             UINT4 u4BandWidth)
{
    UINT4               u4ErrorCode = 0;

    UNUSED_PARAM (CliHandle);

    /* Set the Row Status of User Group table as NOT_IN_SERVICE - 
     * To update its field */

    if (nmhSetFsWssUserGroupRowStatus (u4GroupId,
                                       NOT_IN_SERVICE) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    /* Check whether the Bandwidth value is valid input */

    if (nmhTestv2FsWssUserGroupBandWidth (&u4ErrorCode, u4GroupId,
                                          u4BandWidth) != SNMP_SUCCESS)
    {
        if (nmhSetFsWssUserGroupRowStatus (u4GroupId, ACTIVE) != SNMP_SUCCESS)
        {
        }
        return CLI_FAILURE;
    }

    /* Update the User Group's Bandwidth value */

    if (nmhSetFsWssUserGroupBandWidth (u4GroupId, u4BandWidth) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    /* Change back the Row Status of User Group table as ACTIVE  */

    if (nmhSetFsWssUserGroupRowStatus (u4GroupId, ACTIVE) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : WssUserCliSetGroupDLBandWidth                           */
/*                                                                           */
/* Description     : This function is used to set User Group                 */
/*                     Download BandWidth                                    */
/*                                                                           */
/* Input(s)        : CliHandle      - CLI Handler                            */
/*                   u4GroupId      - User Group Id                          */
/*                   u4DLBandWidth    - User Group Download BandWidth        */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/
INT4
WssUserCliSetGroupDLBandWidth (tCliHandle CliHandle, UINT4 u4GroupId,
                               UINT4 u4DLBandWidth)
{
    UINT4               u4ErrorCode = 0;

    UNUSED_PARAM (CliHandle);

    /* Set the Row Status of User Group table as NOT_IN_SERVICE - 
     * To update its field */

    if (nmhSetFsWssUserGroupRowStatus (u4GroupId,
                                       NOT_IN_SERVICE) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    /* Check whether the Bandwidth value is valid input */

    if (nmhTestv2FsWssUserGroupDLBandWidth (&u4ErrorCode, u4GroupId,
                                            u4DLBandWidth) != SNMP_SUCCESS)
    {
        if (nmhSetFsWssUserGroupRowStatus (u4GroupId, ACTIVE) != SNMP_SUCCESS)
        {
        }
        return CLI_FAILURE;
    }

    /* Update the User Group's download Bandwidth value */

    if (nmhSetFsWssUserGroupDLBandWidth (u4GroupId, u4DLBandWidth) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    /* Change back the Row Status of User Group table as ACTIVE  */

    if (nmhSetFsWssUserGroupRowStatus (u4GroupId, ACTIVE) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : WssUserCliSetGroupULBandWidth                           */
/*                                                                           */
/* Description     : This function is used to set User Group                 */
/*                     Upload  BandWidth                                     */
/*                                                                           */
/* Input(s)        : CliHandle      - CLI Handler                            */
/*                   u4GroupId      - User Group Id                          */
/*                   u4ULBandWidth  - User Group Upload BandWidth            */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/

INT4
WssUserCliSetGroupULBandWidth (tCliHandle CliHandle, UINT4 u4GroupId,
                               UINT4 u4ULBandWidth)
{
    UINT4               u4ErrorCode = 0;

    UNUSED_PARAM (CliHandle);

    /* Set the Row Status of User Group table as NOT_IN_SERVICE - 
     * To update its field */

    if (nmhSetFsWssUserGroupRowStatus (u4GroupId,
                                       NOT_IN_SERVICE) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    /* Check whether the Bandwidth value is valid input */

    if (nmhTestv2FsWssUserGroupULBandWidth (&u4ErrorCode, u4GroupId,
                                            u4ULBandWidth) != SNMP_SUCCESS)
    {
        if (nmhSetFsWssUserGroupRowStatus (u4GroupId, ACTIVE) != SNMP_SUCCESS)
        {
        }
        return CLI_FAILURE;
    }

    /* Update the User Group's upload Bandwidth value */

    if (nmhSetFsWssUserGroupULBandWidth (u4GroupId, u4ULBandWidth) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    /* Change back the Row Status of User Group table as ACTIVE  */

    if (nmhSetFsWssUserGroupRowStatus (u4GroupId, ACTIVE) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : WssUserCliSetGroupVolume                                */
/*                                                                           */
/* Description     : This function is used to set User Group Volume          */
/*                                                                           */
/* Input(s)        : CliHandle      - CLI Handler                            */
/*                   u4GroupId      - User Group Id                          */
/*                   u4Volume       - User Group Volume                      */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/

INT4
WssUserCliSetGroupVolume (tCliHandle CliHandle, UINT4 u4GroupId, UINT4 u4Volume)
{
    UINT4               u4ErrorCode = 0;

    UNUSED_PARAM (CliHandle);

    /* Set the Row Status of User Group table as NOT_IN_SERVICE - 
     * To update its field */

    if (nmhSetFsWssUserGroupRowStatus (u4GroupId,
                                       NOT_IN_SERVICE) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    /* Check whether the Volume value is valid input */

    if (nmhTestv2FsWssUserGroupVolume (&u4ErrorCode, u4GroupId,
                                       u4Volume) != SNMP_SUCCESS)
    {
        if (nmhSetFsWssUserGroupRowStatus (u4GroupId, ACTIVE) != SNMP_SUCCESS)
        {
        }
        return CLI_FAILURE;
    }

    /* Update the User Group's Volume value */

    if (nmhSetFsWssUserGroupVolume (u4GroupId, u4Volume) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    /* Change back the Row Status of User Group table as ACTIVE  */

    if (nmhSetFsWssUserGroupRowStatus (u4GroupId, ACTIVE) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : WssUserCliSetGroupTime                                  */
/*                                                                           */
/* Description     : This function is used to set User Group Time            */
/*                                                                           */
/* Input(s)        : CliHandle      - CLI Handler                            */
/*                   u4GroupId      - User Group Id                          */
/*                   u4Time         - User Group Time                        */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/

INT4
WssUserCliSetGroupTime (tCliHandle CliHandle, UINT4 u4GroupId, UINT4 u4Time)
{
    UINT4               u4ErrorCode = 0;

    UNUSED_PARAM (CliHandle);

    /* Set the Row Status of User Group table as NOT_IN_SERVICE -
     * To update its field */

    if (nmhSetFsWssUserGroupRowStatus (u4GroupId,
                                       NOT_IN_SERVICE) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    /* Check whether the Time value is valid input */

    if (nmhTestv2FsWssUserGroupTime (&u4ErrorCode, u4GroupId,
                                     u4Time) != SNMP_SUCCESS)
    {
        if (nmhSetFsWssUserGroupRowStatus (u4GroupId, ACTIVE) != SNMP_SUCCESS)
        {
        }
        return CLI_FAILURE;
    }

    /* Update the User Group's Time value */

    if (nmhSetFsWssUserGroupTime (u4GroupId, u4Time) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    /* Change back the Row Status of User Group table as ACTIVE  */

    if (nmhSetFsWssUserGroupRowStatus (u4GroupId, ACTIVE) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : WssUserCliAddRole                                       */
/*                                                                           */
/* Description     : This function is used to set User Role                  */
/*                                                                           */
/* Input(s)        : CliHandle      - CLI Handler                            */
/*                   UserName       - User Name                              */
/*                   u4ProfileId    - Profile Id                             */
/*                   u4GroupId      - User Group Id                          */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/

INT4
WssUserCliAddRole (tCliHandle CliHandle, tSNMP_OCTET_STRING_TYPE * pUserName,
                   UINT4 u4ProfileId, UINT4 u4GroupId)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4UserGroupRowStatus = -1;
    INT4                i4UserRoleRowStatus = -1;

    UNUSED_PARAM (CliHandle);

    if (u4GroupId != 0)
    {
        /* Check whether User Group of the corresponding GroupId is already created.
         * If not created, display error message saying "User Group is not created" */
        if (nmhGetFsWssUserGroupRowStatus (u4GroupId, &i4UserGroupRowStatus) !=
            SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhGetFsWssUserRoleRowStatus
            (pUserName, u4ProfileId, &i4UserRoleRowStatus) != SNMP_SUCCESS)
        {
            /* Check whether the UserName and WlanId value is valid input */
            if (nmhTestv2FsWssUserRoleRowStatus (&u4ErrorCode, pUserName,
                                                 u4ProfileId,
                                                 CREATE_AND_WAIT) !=
                SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            /* Set the Row Status of User Role Table to CREATE_AND_WAIT -
             * To add entry to User Role Table*/
            if (nmhSetFsWssUserRoleRowStatus (pUserName, u4ProfileId,
                                              CREATE_AND_WAIT) != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            /* Check whether 
             * 1. User Name and WlanId is valid input,
             * 2. User Group of the corresponding GroupId is already created. 
             *    If not created, display error message saying "User Group is not created"
             * 3. User is already associated with another User Group.
             *    If yes, display error message saying "User Role Entry is already created."*/
            if (nmhTestv2FsWssUserRoleGroupId
                (&u4ErrorCode, pUserName, u4ProfileId,
                 u4GroupId) != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            /* Set the Row Status of User Role Table as NOT_IN_SERVICE -
             * To update its field*/
            if (nmhSetFsWssUserRoleRowStatus (pUserName, u4ProfileId,
                                              NOT_IN_SERVICE) != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            /* Update Group Id for the given User Name and WlanId */
            if (nmhSetFsWssUserRoleGroupId (pUserName, u4ProfileId,
                                            u4GroupId) != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            if (nmhTestv2FsWssUserRoleRowStatus (&u4ErrorCode, pUserName,
                                                 u4ProfileId,
                                                 ACTIVE) != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            /* Change back the Row Status of User Role table as ACTIVE  */
            if (nmhSetFsWssUserRoleRowStatus (pUserName, u4ProfileId,
                                              ACTIVE) != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }
        }

        else
        {
            if (nmhTestv2FsWssUserRoleRowStatus
                (&u4ErrorCode, pUserName, u4ProfileId,
                 NOT_IN_SERVICE) != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            /* Set the Row Status of User Role Table as NOT_IN_SERVICE -
             * To update its field*/
            if (nmhSetFsWssUserRoleRowStatus (pUserName, u4ProfileId,
                                              NOT_IN_SERVICE) != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            if (nmhTestv2FsWssUserRoleGroupId
                (&u4ErrorCode, pUserName, u4ProfileId,
                 u4GroupId) != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            /* Update Group Id for the given User Name and WlanId */
            if (nmhSetFsWssUserRoleGroupId (pUserName, u4ProfileId,
                                            u4GroupId) != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            if (nmhTestv2FsWssUserRoleRowStatus (&u4ErrorCode, pUserName,
                                                 u4ProfileId,
                                                 ACTIVE) != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            /* Change back the Row Status of User Role table as ACTIVE  */
            if (nmhSetFsWssUserRoleRowStatus (pUserName, u4ProfileId,
                                              ACTIVE) != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }
        }
    }
    else
    {
        if (nmhGetFsWssUserRoleRowStatus
            (pUserName, u4ProfileId, &i4UserRoleRowStatus) != SNMP_SUCCESS)
        {

            /* User is created without specifying User Group.  
             * Let it be set to default User Group.
             */

            if (nmhTestv2FsWssUserRoleRowStatus (&u4ErrorCode, pUserName,
                                                 u4ProfileId,
                                                 CREATE_AND_GO) != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            if (nmhSetFsWssUserRoleRowStatus (pUserName, u4ProfileId,
                                              CREATE_AND_GO) != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }
        }

        else
        {
            if (nmhTestv2FsWssUserRoleRowStatus
                (&u4ErrorCode, pUserName, u4ProfileId,
                 NOT_IN_SERVICE) != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            /* Set the Row Status of User Role Table as NOT_IN_SERVICE -
             * To update its field*/
            if (nmhSetFsWssUserRoleRowStatus (pUserName, u4ProfileId,
                                              NOT_IN_SERVICE) != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            if (nmhTestv2FsWssUserRoleGroupId
                (&u4ErrorCode, pUserName, u4ProfileId,
                 u4GroupId) != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            /* Update Group Id for the given User Name and WlanId */
            if (nmhSetFsWssUserRoleGroupId (pUserName, u4ProfileId,
                                            u4GroupId) != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            if (nmhTestv2FsWssUserRoleRowStatus (&u4ErrorCode, pUserName,
                                                 u4ProfileId,
                                                 ACTIVE) != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            /* Change back the Row Status of User Role table as ACTIVE  */
            if (nmhSetFsWssUserRoleRowStatus (pUserName, u4ProfileId,
                                              ACTIVE) != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }
        }

    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : WssUserCliDeleteRole                                    */
/*                                                                           */
/* Description     : This function is used to delete User Role               */
/*                                                                           */
/* Input(s)        : CliHandle    - CLI Handler                              */
/*                   u4ProfileId  - Profile Id                               */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/

INT4
WssUserCliDeleteRole (tCliHandle CliHandle, tSNMP_OCTET_STRING_TYPE * pUserName,
                      UINT4 u4ProfileId)
{
    UINT4               u4ErrorCode = 0;

    UNUSED_PARAM (CliHandle);

    /* Check whether User Name and WlanId value is valid input */

    if (nmhTestv2FsWssUserRoleRowStatus (&u4ErrorCode, pUserName,
                                         u4ProfileId, DESTROY) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    /* Delete User Role and Release the corresponding MEMPOOL */

    if (nmhSetFsWssUserRoleRowStatus (pUserName, u4ProfileId,
                                      DESTROY) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : WssUserCliRestrictUserName                              */
/*                                                                           */
/* Description     : This function is used to restrict particular            */
/*                   User Name                                               */
/*                                                                           */
/* Input(s)        : CliHandle      - CLI Handler                            */
/*                   pUserName      - User Name                              */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/

INT4
WssUserCliRestrictUserName (tCliHandle CliHandle,
                            tSNMP_OCTET_STRING_TYPE * pUserName)
{
    UINT4               u4ErrorCode = 0;

    UNUSED_PARAM (CliHandle);

    /* Check whether 
     * 1. User Name value is valid input.
     * 2. No of User Name that can be restricted exceeds the Max Limit.
     * 3. User Name is already restricted. */

    if (nmhTestv2FsWssUserNameAccessListRowStatus (&u4ErrorCode, pUserName,
                                                   CREATE_AND_GO) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    /* Update User Name List Entry */

    if (nmhSetFsWssUserNameAccessListRowStatus (pUserName, CREATE_AND_GO) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : WssUserCliNoRestrictUserName                            */
/*                                                                           */
/* Description     : This function is used to grant access                   */
/*                   to the restricted user                                  */
/*                                                                           */
/* Input(s)        : CliHandle      - CLI Handler                            */
/*                   pUserName      - User Name                              */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/

INT4
WssUserCliNoRestrictUserName (tCliHandle CliHandle,
                              tSNMP_OCTET_STRING_TYPE * pUserName)
{
    UINT4               u4ErrorCode = 0;

    UNUSED_PARAM (CliHandle);

    /* Check whether User Name value is valid input. */

    if (nmhTestv2FsWssUserNameAccessListRowStatus (&u4ErrorCode, pUserName,
                                                   DESTROY) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    /* Delete the User Name from the restricted list and 
     * Release the corresponding MEMPOOL */

    if (nmhSetFsWssUserNameAccessListRowStatus (pUserName, DESTROY) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : WssUserCliRestrictUserMac                               */
/*                                                                           */
/* Description     : This function is used to restrict                       */
/*                   particular mac address                                  */
/*                                                                           */
/* Input(s)        : CliHandle      - CLI Handler                            */
/*                   MacAddress     - User Mac Address                       */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/

INT4
WssUserCliRestrictUserMac (tCliHandle CliHandle, tMacAddr MacAddress)
{
    UINT4               u4ErrorCode = 0;

    UNUSED_PARAM (CliHandle);

    /* Check whether 
     * 1. MAC Address value is valid input.
     * 2. No of MAC Address that can be restricted exceeds the Max Limit.
     * 3. MAC Address is already restricted. */

    if (nmhTestv2FsWssUserMacAccessListRowStatus (&u4ErrorCode, MacAddress,
                                                  CREATE_AND_GO) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    /* Update User MAC Address List Entry */

    if (nmhSetFsWssUserMacAccessListRowStatus (MacAddress, CREATE_AND_GO) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : WssUserCliNoRestrictUserMac                             */
/*                                                                           */
/* Description     : This function is used to grant access                   */
/*                   to the restricted  mac address                          */
/*                                                                           */
/* Input(s)        : CliHandle      - CLI Handler                            */
/*                   MacAddress     - User Mac Address                       */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/

INT4
WssUserCliNoRestrictUserMac (tCliHandle CliHandle, tMacAddr MacAddress)
{
    UINT4               u4ErrorCode = 0;

    UNUSED_PARAM (CliHandle);

    /* Check whether MAC Address value is valid input. */

    if (nmhTestv2FsWssUserMacAccessListRowStatus (&u4ErrorCode, MacAddress,
                                                  DESTROY) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    /* Delete the MAC Address from the restricted list and 
     * Release the corresponding MEMPOOL */

    if (nmhSetFsWssUserMacAccessListRowStatus (MacAddress, DESTROY) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : WssUserCliAddMacMap                                     */
/*                                                                           */
/* Description     : This function is used to map User Name                  */
/*                   and Mac Address                                         */
/*                                                                           */
/* Input(s)        : CliHandle      - CLI Handler                            */
/*                   MacAddress     - User Mac Address                       */
/*                   UserName       - User Name                              */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/

INT4
WssUserCliAddMacMap (tCliHandle CliHandle, tMacAddr MacAddress,
                     tSNMP_OCTET_STRING_TYPE * pUserName)
{
    UINT4               u4ErrorCode = 0;

    UNUSED_PARAM (CliHandle);

    /* Check whether 
     * 1. User Name and MAC Address value is valid input.
     * 2. User-MAC mapping count exceeds the Max Limit.
     * 3. User Name and MAC Address is already mapped. */

    if (nmhTestv2FsWssUserMappingRowStatus (&u4ErrorCode, pUserName,
                                            MacAddress,
                                            CREATE_AND_GO) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    /* Update User-MAC Mapping */

    if (nmhSetFsWssUserMappingRowStatus (pUserName, MacAddress,
                                         CREATE_AND_GO) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : WssUserCliDelMacMap                                     */
/*                                                                           */
/* Description     : This function is used to delete the mapping             */
/*                   between  User Name and Mac Address                      */
/*                                                                           */
/* Input(s)        : CliHandle      - CLI Handler                            */
/*                   MacAddress     - User Mac Address                       */
/*                   UserName       - User Name                              */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/

INT4
WssUserCliDelMacMap (tCliHandle CliHandle, tMacAddr MacAddress,
                     tSNMP_OCTET_STRING_TYPE * UserName)
{
    UINT4               u4ErrorCode = 0;

    UNUSED_PARAM (CliHandle);

    /* Check whether MAC Address value is valid input. */

    if (nmhTestv2FsWssUserMappingRowStatus (&u4ErrorCode, UserName, MacAddress,
                                            DESTROY) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    /* Delete the User-MAC mapping and
     * Release the corresponding MEMPOOL */

    if (nmhSetFsWssUserMappingRowStatus (UserName, MacAddress,
                                         DESTROY) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : WssUserCliRoleStatus                                    */
/*                                                                           */
/* Description     : This function is used to enable/disable the             */
/*                   User Role Module                                        */
/*                                                                           */
/* Input(s)        : CliHandle      - CLI Handler                            */
/*                   i4RoleStatus   - User Role Module Status                */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/

INT4
WssUserCliRoleStatus (tCliHandle CliHandle, INT4 i4RoleStatus)
{
    UINT4               u4ErrorCode = 0;

    UNUSED_PARAM (CliHandle);

    /* Check whether User Role Status value is valid input */

    if (nmhTestv2FsWssUserRoleStatus (&u4ErrorCode, i4RoleStatus) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    /* To disconnect the station and delete user-role table entries */

    if (i4RoleStatus == WSSUSER_MODULE_DISABLED)
    {
        /* Delete all users from Restricted list */

        WssUserUtilClearRestrictedUserTable (CliHandle);

        /* Delete all stations from Restricted list */

        WssUserUtilClearRestrictedStationTable (CliHandle);

        /* Disconnect the ongoing session */

        WssUserUtilDisconnectStation ();
        /* Delete all user details */

        WssUserUtilClearUserRoleTable (CliHandle);

        /* Delete all User-Group except DEFAULT User-Group */

        WssUserUtilClearUserGroupTable (CliHandle);
    }

    /* Update the User Role Status */
    if (nmhSetFsWssUserRoleStatus (i4RoleStatus) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : WssUserCliShowGroupDetails                              */
/*                                                                           */
/* Description     : This function is used to list  user group details       */
/*                                                                           */
/* Input(s)        : CliHandle      - CLI Handler                            */
/*                   u4GroupId      - User Group Id                          */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/

INT4
WssUserCliShowGroupDetails (tCliHandle CliHandle, UINT4 u4GroupId)
{
    tSNMP_OCTET_STRING_TYPE GroupName;
    UINT4               u4BandWidth = 0;
    UINT4               u4DLBandWidth = 0;
    UINT4               u4ULBandWidth = 0;
    UINT4               u4Volume = 0;
    UINT4               u4Time = 0;
    INT4                i4RowStatus = 0;
    UINT1               au1GroupName[MAX_GROUP_NAME_LENGTH];

    if (gWssUserGlobals.i4UserRoleStatus == WSSUSER_MODULE_DISABLED)
    {
        CliPrintf (CliHandle, "\rUSER-ROLE STATUS : DISABLED\n");
        return CLI_SUCCESS;
    }
    MEMSET (&GroupName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1GroupName, 0, MAX_GROUP_NAME_LENGTH);

    GroupName.pu1_OctetList = (UINT1 *) au1GroupName;
    GroupName.i4_Length = MAX_GROUP_NAME_LENGTH;

    /* Display the User Group details for the corresponding User Group Id */

    if (nmhGetFsWssUserGroupName (u4GroupId, &GroupName) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhGetFsWssUserGroupBandWidth (u4GroupId, &u4BandWidth) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhGetFsWssUserGroupDLBandWidth (u4GroupId, &u4DLBandWidth) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhGetFsWssUserGroupULBandWidth (u4GroupId, &u4ULBandWidth) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhGetFsWssUserGroupVolume (u4GroupId, &u4Volume) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhGetFsWssUserGroupTime (u4GroupId, &u4Time) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhGetFsWssUserGroupRowStatus (u4GroupId, &i4RowStatus) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    CliPrintf (CliHandle, "\rGroup Id             : %d\r\n", u4GroupId);
    CliPrintf (CliHandle, "\rGroup Name           : %s\r\n",
               GroupName.pu1_OctetList);

    WssShowUtilBandwidthDisplay (CliHandle, u4BandWidth,
                                 u4DLBandWidth, u4ULBandWidth);
    WssShowUtilVolumeDisplay (CliHandle, u4Volume);

    if (u4Time == 0)
    {
        CliPrintf (CliHandle, "\rTime                 : UnLimited\r\n\n");
    }

    else
    {
        CliPrintf (CliHandle, "\rTime                 : %dseconds\r\n\n",
                   u4Time);
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : WssUserCliShowGroupDetailsAll                           */
/*                                                                           */
/* Description     : This function is used to list  all                      */
/*                   user group details                                      */
/*                                                                           */
/* Input(s)        : CliHandle      - CLI Handler                            */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/

INT4
WssUserCliShowGroupDetailsAll (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE GroupName;
    UINT4               u4NextGroupId = 0;
    UINT4               u4GroupId = 0;
    UINT4               u4BandWidth;
    UINT4               u4DLBandWidth;
    UINT4               u4ULBandWidth;
    UINT4               u4Volume;
    UINT4               u4Time;
    INT4                i4RowStatus;
    UINT1               au1GroupName[MAX_GROUP_NAME_LENGTH];
    INT1                i1RetValue = SNMP_FAILURE;

    if (gWssUserGlobals.i4UserRoleStatus == WSSUSER_MODULE_DISABLED)
    {
        CliPrintf (CliHandle, "\rUSER-ROLE STATUS : DISABLED\n");
        return CLI_SUCCESS;
    }

    /* Display all User Group details */

    i1RetValue = nmhGetFirstIndexFsWssUserGroupTable (&u4NextGroupId);

    if (i1RetValue == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\rEntry not found\r\n");
        return CLI_FAILURE;
    }

    do
    {
        u4GroupId = u4NextGroupId;
        u4DLBandWidth = 0;
        u4ULBandWidth = 0;
        u4Volume = 0;
        u4Time = 0;
        i4RowStatus = 0;

        MEMSET (&GroupName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        MEMSET (&au1GroupName, 0, MAX_GROUP_NAME_LENGTH);
        GroupName.pu1_OctetList = (UINT1 *) au1GroupName;
        GroupName.i4_Length = MAX_GROUP_NAME_LENGTH;

        if (nmhGetFsWssUserGroupName (u4GroupId, &GroupName) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
        if (nmhGetFsWssUserGroupBandWidth (u4GroupId, &u4BandWidth) !=
            SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhGetFsWssUserGroupDLBandWidth (u4GroupId, &u4DLBandWidth) !=
            SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhGetFsWssUserGroupULBandWidth (u4GroupId, &u4ULBandWidth) !=
            SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhGetFsWssUserGroupVolume (u4GroupId, &u4Volume) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhGetFsWssUserGroupTime (u4GroupId, &u4Time) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhGetFsWssUserGroupRowStatus (u4GroupId, &i4RowStatus) !=
            SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        CliPrintf (CliHandle, "\r\nGroup Id           :   %d\r\n", u4GroupId);
        CliPrintf (CliHandle, "\rGroup Name         :   %s\r\n",
                   GroupName.pu1_OctetList);

        WssShowUtilBandwidthDisplay (CliHandle, u4BandWidth,
                                     u4DLBandWidth, u4ULBandWidth);
        WssShowUtilVolumeDisplay (CliHandle, u4Volume);

        if (u4Time == 0)
        {
            CliPrintf (CliHandle, "\rTime               :   UnLimited\r\n");
        }

        else
        {
            CliPrintf (CliHandle, "\rTime               :   %dseconds\r\n",
                       u4Time);
        }
    }
    while (nmhGetNextIndexFsWssUserGroupTable (u4GroupId,
                                               &u4NextGroupId) == SNMP_SUCCESS);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : WssUserCliShowSessionDetails                            */
/*                                                                           */
/* Description     : This function is used to show user session details      */
/*                                                                           */
/* Input(s)        : CliHandle      - CLI Handler                            */
/*                   u4GroupId      - User Group Id                          */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/

INT4
WssUserCliShowSessionDetails (tCliHandle CliHandle,
                              tSNMP_OCTET_STRING_TYPE * pUserName,
                              tMacAddr MacAddr)
{
    UINT4               u4AllotedBandWidth = 0;
    UINT4               u4AllotedDLBandWidth = 0;
    UINT4               u4AllotedULBandWidth = 0;
    UINT4               u4AllotedVolume = 0;
    UINT4               u4AllotedTime = 0;
    UINT4               u4UsedVolume = 0;
    UINT4               u4WlanIndex = 0;
    UINT4               u4UsedTime = 0;
    UINT4               u4Sec = 0;
    UINT4               u4Hrs = 0;
    UINT4               u4Min = 0;
    UINT4               u4Days = 0;
    UINT1               au1String[MAC_TO_STRING_LEN];
    UINT1               au1DateString[100];

    MEMSET (au1DateString, 0, 100);

    if (gWssUserGlobals.i4UserRoleStatus == WSSUSER_MODULE_DISABLED)
    {
        CliPrintf (CliHandle, "\rUSER-ROLE STATUS : DISABLED\n");
        return CLI_SUCCESS;
    }

    /* Display the Session Details for the given User Name and MAC Address */

    if (nmhGetFsWssUserWlanIndex (pUserName, MacAddr, &u4WlanIndex) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhGetFsWssUserAllotedBandWidth
        (pUserName, MacAddr, &u4AllotedBandWidth) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhGetFsWssUserAllotedDLBandWidth
        (pUserName, MacAddr, &u4AllotedDLBandWidth) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhGetFsWssUserAllotedULBandWidth
        (pUserName, MacAddr, &u4AllotedULBandWidth) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhGetFsWssUserAllotedVolume (pUserName, MacAddr, &u4AllotedVolume) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhGetFsWssUserAllotedTime (pUserName, MacAddr, &u4AllotedTime) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhGetFsWssUserUsedVolume (pUserName, MacAddr, &u4UsedVolume) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhGetFsWssUserUsedTime (pUserName, MacAddr, &u4UsedTime) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    MEMSET (au1String, 0, MAC_TO_STRING_LEN);
    PrintMacAddress (MacAddr, au1String);

    CliPrintf (CliHandle, "\rUser Name                  :   %s\n",
               pUserName->pu1_OctetList);
    CliPrintf (CliHandle, "\rMAC Address                :   %s\n", au1String);
    CliPrintf (CliHandle, "\rWlan ID                    :   %d\n", u4WlanIndex);

    CliPrintf (CliHandle, "\rAlloted ");
    WssShowUtilBandwidthDisplay (CliHandle, u4AllotedBandWidth,
                                 u4AllotedDLBandWidth, u4AllotedULBandWidth);

    CliPrintf (CliHandle, "\rAlloted ");
    WssShowUtilVolumeDisplay (CliHandle, u4AllotedVolume);

    u4Sec = (UINT4) (u4AllotedTime % SECS_IN_MINUTE);
    u4Min = (UINT4) ((u4AllotedTime / SECS_IN_MINUTE) % MINUTES_IN_HOUR);
    u4Hrs = (UINT4) ((u4AllotedTime / SECS_IN_HOUR) % HOURS_IN_DAY);
    u4Days = (UINT4) (u4AllotedTime / SECS_IN_DAY);
    SPRINTF ((CHR1 *) au1DateString, "%d Days %d Hrs, %d Mins, %d Secs",
             u4Days, u4Hrs, u4Min, u4Sec);

    CliPrintf (CliHandle, "\rAlloted Time               :   %s\n",
               au1DateString);
    CliPrintf (CliHandle, "\rUsed    ");
    WssShowUtilVolumeDisplay (CliHandle, u4UsedVolume);

    MEMSET (au1DateString, 0, 100);
    u4Sec = (UINT4) (u4UsedTime % SECS_IN_MINUTE);
    u4Min = (UINT4) ((u4UsedTime / SECS_IN_MINUTE) % MINUTES_IN_HOUR);
    u4Hrs = (UINT4) ((u4UsedTime / SECS_IN_HOUR) % HOURS_IN_DAY);
    u4Days = (UINT4) (u4UsedTime / SECS_IN_DAY);
    SPRINTF ((CHR1 *) au1DateString, "%d Days %d Hrs, %d Mins, %d Secs",
             u4Days, u4Hrs, u4Min, u4Sec);

    CliPrintf (CliHandle, "\rUsed Time                  :   %s\n",
               au1DateString);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : WssUserCliShowSessionAll                                */
/*                                                                           */
/* Description     : This function is used to display session table          */
/*                   of all user                                             */
/*                                                                           */
/* Input(s)        : CliHandle      - CLI Handler                            */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/

INT4
WssUserCliShowSessionAll (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE UserName;
    tSNMP_OCTET_STRING_TYPE NextUserName;
    tMacAddr            MacAddr;
    tMacAddr            NextMacAddr;
    UINT4               u4AllotedBandWidth = 0;
    UINT4               u4AllotedDLBandWidth = 0;
    UINT4               u4AllotedULBandWidth = 0;
    UINT4               u4AllotedVolume = 0;
    UINT4               u4AllotedTime = 0;
    UINT4               u4UsedVolume = 0;
    UINT4               u4WlanIndex = 0;
    UINT4               u4UsedTime = 0;
    UINT4               u4Sec = 0;
    UINT4               u4Hrs = 0;
    UINT4               u4Min = 0;
    UINT4               u4Days = 0;
    UINT1               au1NextUserName[MAX_USER_NAME_LENGTH];
    UINT1               au1UserName[MAX_USER_NAME_LENGTH];
    UINT1               au1String[MAC_TO_STRING_LEN];
    INT1                i1RetValue = SNMP_FAILURE;
    UINT1               au1DateString[100];

    MEMSET (au1DateString, 0, 100);

    if (gWssUserGlobals.i4UserRoleStatus == WSSUSER_MODULE_DISABLED)
    {
        CliPrintf (CliHandle, "\rUSER-ROLE STATUS : DISABLED\n");
        return CLI_SUCCESS;
    }

    MEMSET (&NextUserName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&UserName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1NextUserName, 0, MAX_USER_NAME_LENGTH);
    MEMSET (&au1UserName, 0, MAX_USER_NAME_LENGTH);
    MEMSET (&NextMacAddr, 0, MAC_ADDR_LEN);
    MEMSET (&MacAddr, 0, MAC_ADDR_LEN);

    UserName.pu1_OctetList = (UINT1 *) au1UserName;
    NextUserName.pu1_OctetList = (UINT1 *) au1NextUserName;

    /* Get User Name (Index) of the first entry in User Session Table */
    i1RetValue =
        nmhGetFirstIndexFsWssUserSessionTable (&NextUserName, &NextMacAddr);

    if (i1RetValue == SNMP_FAILURE)
    {
        /* Since no entry is available in the User session table, return */
        return CLI_FAILURE;
    }

    do
    {
        /* Since fetching entry from the User session table printing the elements */
        MEMCPY (UserName.pu1_OctetList, NextUserName.pu1_OctetList,
                NextUserName.i4_Length);
        UserName.i4_Length = NextUserName.i4_Length;
        MEMCPY (MacAddr, NextMacAddr, MAC_ADDR_LEN);

        MEMSET (&NextMacAddr, 0, MAC_ADDR_LEN);
        MEMSET (&NextUserName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        MEMSET (&au1NextUserName, 0, MAX_USER_NAME_LENGTH);
        NextUserName.pu1_OctetList = (UINT1 *) au1NextUserName;

        if (nmhGetFsWssUserWlanIndex (&UserName, MacAddr, &u4WlanIndex) !=
            SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhGetFsWssUserAllotedBandWidth
            (&UserName, MacAddr, &u4AllotedBandWidth) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
        if (nmhGetFsWssUserAllotedDLBandWidth
            (&UserName, MacAddr, &u4AllotedDLBandWidth) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhGetFsWssUserAllotedULBandWidth
            (&UserName, MacAddr, &u4AllotedULBandWidth) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhGetFsWssUserAllotedVolume (&UserName, MacAddr, &u4AllotedVolume)
            != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhGetFsWssUserAllotedTime (&UserName, MacAddr, &u4AllotedTime) !=
            SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhGetFsWssUserUsedVolume (&UserName, MacAddr, &u4UsedVolume) !=
            SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhGetFsWssUserUsedTime (&UserName, MacAddr, &u4UsedTime) !=
            SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        MEMSET (au1String, 0, MAC_TO_STRING_LEN);
        PrintMacAddress (MacAddr, au1String);

        CliPrintf (CliHandle, "\rUser Name                  :   %s\n",
                   UserName.pu1_OctetList);

        CliPrintf (CliHandle, "\rMAC Address                :   %s\n",
                   au1String);
        CliPrintf (CliHandle, "\rWlan ID                    :   %d\n",
                   u4WlanIndex);
        CliPrintf (CliHandle, "\rAlloted ");
        WssShowUtilBandwidthDisplay (CliHandle, u4AllotedBandWidth,
                                     u4AllotedDLBandWidth,
                                     u4AllotedULBandWidth);

        CliPrintf (CliHandle, "\rAlloted ");
        WssShowUtilVolumeDisplay (CliHandle, u4AllotedVolume);
        u4Sec = (UINT4) (u4AllotedTime % SECS_IN_MINUTE);
        u4Min = (UINT4) ((u4AllotedTime / SECS_IN_MINUTE) % MINUTES_IN_HOUR);
        u4Hrs = (UINT4) ((u4AllotedTime / SECS_IN_HOUR) % HOURS_IN_DAY);
        u4Days = (UINT4) (u4AllotedTime / SECS_IN_DAY);
        SPRINTF ((CHR1 *) au1DateString, "%d Days %d Hrs, %d Mins, %d Secs",
                 u4Days, u4Hrs, u4Min, u4Sec);

        CliPrintf (CliHandle, "\rAlloted Time               :   %s\n",
                   au1DateString);

        CliPrintf (CliHandle, "\rUsed    ");
        WssShowUtilVolumeDisplay (CliHandle, u4UsedVolume);
        MEMSET (au1DateString, 0, 100);
        u4Sec = (UINT4) (u4UsedTime % SECS_IN_MINUTE);
        u4Min = (UINT4) ((u4UsedTime / SECS_IN_MINUTE) % MINUTES_IN_HOUR);
        u4Hrs = (UINT4) ((u4UsedTime / SECS_IN_HOUR) % HOURS_IN_DAY);
        u4Days = (UINT4) (u4UsedTime / SECS_IN_DAY);
        SPRINTF ((CHR1 *) au1DateString, "%d Days %d Hrs, %d Mins, %d Secs",
                 u4Days, u4Hrs, u4Min, u4Sec);

        CliPrintf (CliHandle, "\rUsed Time                  :   %s\n",
                   au1DateString);
    }
    /* Getting the next entry from the User session table */
    while (nmhGetNextIndexFsWssUserSessionTable
           (&UserName, &NextUserName, MacAddr, &NextMacAddr) == SNMP_SUCCESS);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : WssUserCliShowUserDetails                               */
/*                                                                           */
/* Description     : This function is used to display group id               */
/*                   of the particulaR user                                  */
/*                                                                           */
/* Input(s)        : CliHandle      - CLI Handler                            */
/*                   UserName       - User Name                              */
/*                   u4ProfileId    - Wlan Id                                */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/

INT4
WssUserCliShowUserDetails (tCliHandle CliHandle,
                           tSNMP_OCTET_STRING_TYPE * pUserName,
                           UINT4 u4ProfileId)
{
    UINT4               u4GroupId = 0;
    UINT4               u4WlanId = 0;

    if (gWssUserGlobals.i4UserRoleStatus == WSSUSER_MODULE_DISABLED)
    {
        CliPrintf (CliHandle, "\rUSER-ROLE STATUS : DISABLED\n");
        return CLI_SUCCESS;
    }
    /* Display User Role Details of the given User Name and Wlan Id */

    if (nmhGetFsWssUserRoleGroupId (pUserName, u4ProfileId,
                                    &u4GroupId) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    WssGetCapwapWlanId (u4ProfileId, &u4WlanId);

    CliPrintf (CliHandle, "\r\nUser Name\r\t\t");
    CliPrintf (CliHandle, "Wlan Id\r\t\t\t\t");
    CliPrintf (CliHandle, "Group Id");
    CliPrintf (CliHandle, "\r\n---------\r\t\t");
    CliPrintf (CliHandle, "-------\r\t\t\t\t");
    CliPrintf (CliHandle, "--------\r\n");
    CliPrintf (CliHandle, "%s\r\t\t", pUserName->pu1_OctetList);
    CliPrintf (CliHandle, "%d\r\t\t\t\t", u4WlanId);
    CliPrintf (CliHandle, "%d\r\n", u4GroupId);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : WssUserCliShowUserDetailsAll                            */
/*                                                                           */
/* Description     : This function is used to display group id               */
/*                   of all user                                             */
/*                                                                           */
/* Input(s)        : CliHandle      - CLI Handler                            */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/

INT4
WssUserCliShowUserDetailsAll (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE UserName;
    tSNMP_OCTET_STRING_TYPE NextUserName;
    UINT4               u4NextProfileId = 0;
    UINT4               u4ProfileId = 0;
    UINT4               u4GroupId = 0;
    UINT4               u4WlanId = 0;
    UINT1               au1NextUserName[MAX_USER_NAME_LENGTH + 1];
    UINT1               au1UserName[MAX_USER_NAME_LENGTH + 1];
    INT1                i1RetValue = SNMP_FAILURE;

    if (gWssUserGlobals.i4UserRoleStatus == WSSUSER_MODULE_DISABLED)
    {
        CliPrintf (CliHandle, "\rUSER-ROLE STATUS : DISABLED\n");
        return CLI_SUCCESS;
    }

    MEMSET (&NextUserName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1NextUserName, 0, MAX_USER_NAME_LENGTH + 1);

    NextUserName.pu1_OctetList = (UINT1 *) au1NextUserName;

    /* Display all User Role details */

    i1RetValue =
        nmhGetFirstIndexFsWssUserRoleTable (&NextUserName, &u4NextProfileId);

    if (i1RetValue == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\rEntry not found\r\n");
        return CLI_FAILURE;
    }

    CliPrintf (CliHandle, "\r\nUser Name\r\t\t\t\t Wlan ID\tGroup Id\r\n");
    CliPrintf (CliHandle, "---------\r\t\t\t\t -------\t--------\r\n");

    do
    {
        u4ProfileId = u4NextProfileId;

        MEMSET (&UserName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        MEMSET (&au1UserName, 0, MAX_USER_NAME_LENGTH + 1);

        UserName.pu1_OctetList = (UINT1 *) au1UserName;

        MEMCPY (UserName.pu1_OctetList, NextUserName.pu1_OctetList,
                NextUserName.i4_Length);
        UserName.i4_Length = NextUserName.i4_Length;

        u4GroupId = 0;

        if (nmhGetFsWssUserRoleGroupId (&UserName, u4ProfileId, &u4GroupId) !=
            SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        WssGetCapwapWlanId (u4ProfileId, &u4WlanId);

        CliPrintf (CliHandle, "%s\r\t\t\t\t %d\t\t%d\r\n",
                   UserName.pu1_OctetList, u4WlanId, u4GroupId);

    }
    while (nmhGetNextIndexFsWssUserRoleTable (&UserName, &NextUserName,
                                              u4ProfileId,
                                              &u4NextProfileId) ==
           SNMP_SUCCESS);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : WssUserCliShowMacMapAll                                 */
/*                                                                           */
/* Description     : This function is used to display Mac Map                */
/*                   of all user                                             */
/*                                                                           */
/* Input(s)        : CliHandle      - CLI Handler                            */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/

INT4
WssUserCliShowMacMapAll (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE UserName;
    tSNMP_OCTET_STRING_TYPE NextUserName;
    tMacAddr            NextMacAddr;
    tMacAddr            MacAddr;
    UINT1               au1NextUserName[MAX_USER_NAME_LENGTH];
    UINT1               au1UserName[MAX_USER_NAME_LENGTH];
    UINT1               au1String[MAC_TO_STRING_LEN];
    INT1                i1RetValue = SNMP_FAILURE;

    if (gWssUserGlobals.i4UserRoleStatus == WSSUSER_MODULE_DISABLED)
    {
        CliPrintf (CliHandle, "\rUSER-ROLE STATUS : DISABLED\n");
        return CLI_SUCCESS;
    }

    MEMSET (&NextUserName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1NextUserName, 0, MAX_USER_NAME_LENGTH);
    MEMSET (&NextMacAddr, 0, MAC_ADDR_LEN);
    MEMSET (&MacAddr, 0, MAC_ADDR_LEN);

    NextUserName.pu1_OctetList = (UINT1 *) au1NextUserName;

    /* Display all User-MAC Mapping details */

    i1RetValue =
        nmhGetFirstIndexFsWssUserMappingTable (&NextUserName, &NextMacAddr);

    if (i1RetValue == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\rEntry not found\r\n");
        return CLI_FAILURE;
    }

    CliPrintf (CliHandle, "\r\nUser Name");
    CliPrintf (CliHandle, "\r\t\t\t\t\tMAC Address");
    CliPrintf (CliHandle, "\r\n---------");
    CliPrintf (CliHandle, "\r\t\t\t\t\t-----------\r\n");

    do
    {
        MEMSET (au1String, 0, MAC_TO_STRING_LEN);

        MEMCPY (MacAddr, NextMacAddr, MAC_ADDR_LEN);
        PrintMacAddress (MacAddr, au1String);

        CliPrintf (CliHandle, "%s", NextUserName.pu1_OctetList);
        CliPrintf (CliHandle, "\r\t\t\t\t\t%s\r\n", au1String);

        MEMSET (&UserName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        MEMSET (au1UserName, 0, MAX_USER_NAME_LENGTH);
        MEMSET (au1String, 0, MAC_TO_STRING_LEN);

        UserName.pu1_OctetList = (UINT1 *) au1UserName;

        MEMCPY (UserName.pu1_OctetList, NextUserName.pu1_OctetList,
                NextUserName.i4_Length);
        UserName.i4_Length = NextUserName.i4_Length;

        MEMSET (&NextUserName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

        MEMSET (au1NextUserName, 0, MAX_USER_NAME_LENGTH);
        NextUserName.pu1_OctetList = (UINT1 *) au1NextUserName;

    }
    while (nmhGetNextIndexFsWssUserMappingTable (&UserName, &NextUserName,
                                                 MacAddr,
                                                 &NextMacAddr) == SNMP_SUCCESS);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : WssUserCliShowRestrictedUser                            */
/*                                                                           */
/* Description     : This function is used to display blolcked               */
/*                   user name                                               */
/*                                                                           */
/* Input(s)        : CliHandle      - CLI Handler                            */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/

INT4
WssUserCliShowRestrictedUser (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE NextUserName;
    tSNMP_OCTET_STRING_TYPE UserName;
    UINT1               au1NextUserName[MAX_USER_NAME_LENGTH + 1];
    UINT1               au1UserName[MAX_USER_NAME_LENGTH + 1];
    UINT2               u2Count = 1;
    INT1                i1RetValue = SNMP_FAILURE;

    if (gWssUserGlobals.i4UserRoleStatus == WSSUSER_MODULE_DISABLED)
    {
        CliPrintf (CliHandle, "\rUSER-ROLE STATUS : DISABLED\n");
        return CLI_SUCCESS;
    }

    MEMSET (&NextUserName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1NextUserName, 0, MAX_USER_NAME_LENGTH + 1);

    NextUserName.pu1_OctetList = (UINT1 *) au1NextUserName;

    /* Display the list of Restricted User */

    i1RetValue = nmhGetFirstIndexFsWssUserNameAccessListTable (&NextUserName);

    if (i1RetValue == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r Entry not found\r\n");
        return CLI_FAILURE;
    }

    CliPrintf (CliHandle, "\r\n S.No");
    CliPrintf (CliHandle, "\r\t User Name");
    CliPrintf (CliHandle, "\r\n ----");
    CliPrintf (CliHandle, "\r\t --------");

    do
    {
        CliPrintf (CliHandle, "\r\n %d", u2Count++);
        CliPrintf (CliHandle, "\r\t %s", NextUserName.pu1_OctetList);
        CliPrintf (CliHandle, "\r\n");

        MEMSET (&UserName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        MEMSET (au1UserName, 0, MAX_USER_NAME_LENGTH + 1);

        UserName.pu1_OctetList = (UINT1 *) au1UserName;

        MEMCPY (UserName.pu1_OctetList, NextUserName.pu1_OctetList,
                NextUserName.i4_Length);
        UserName.i4_Length = NextUserName.i4_Length;

        MEMSET (&NextUserName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        MEMSET (au1NextUserName, 0, MAX_USER_NAME_LENGTH);

        NextUserName.pu1_OctetList = (UINT1 *) au1NextUserName;

    }
    while (nmhGetNextIndexFsWssUserNameAccessListTable
           (&UserName, &NextUserName) == SNMP_SUCCESS);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : WssUserCliShowRestrictedStation                         */
/*                                                                           */
/* Description     : This function is used to display blolcked               */
/*                   station                                                 */
/*                                                                           */
/* Input(s)        : CliHandle      - CLI Handler                            */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/

INT4
WssUserCliShowRestrictedStation (tCliHandle CliHandle)
{
    tMacAddr            NextMacAddr;
    tMacAddr            MacAddr;
    UINT2               u2Count = 1;
    UINT1               au1String[MAC_TO_STRING_LEN];
    INT1                i1RetValue = SNMP_FAILURE;

    if (gWssUserGlobals.i4UserRoleStatus == WSSUSER_MODULE_DISABLED)
    {
        CliPrintf (CliHandle, "\rUSER-ROLE STATUS : DISABLED\n");
        return CLI_SUCCESS;
    }

    MEMSET (&NextMacAddr, 0, sizeof (tMacAddr));

    /* Display the list of Restricted Station */

    i1RetValue = nmhGetFirstIndexFsWssUserMacAccessListTable (&NextMacAddr);

    if (i1RetValue == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r Entry not found\r\n");
        return CLI_FAILURE;
    }

    CliPrintf (CliHandle, "\r\n S.No");
    CliPrintf (CliHandle, "\r\t MAC Address");
    CliPrintf (CliHandle, "\r\n ----");
    CliPrintf (CliHandle, "\r\t ---------");

    do
    {
        MEMSET (au1String, 0, MAC_TO_STRING_LEN);
        MEMSET (&MacAddr, 0, sizeof (tMacAddr));

        MEMCPY (MacAddr, NextMacAddr, MAC_ADDR_LEN);

        CliPrintf (CliHandle, "\r\n %d", u2Count++);
        PrintMacAddress (MacAddr, au1String);
        CliPrintf (CliHandle, "\r\t %s", au1String);
        CliPrintf (CliHandle, "\r\n");

        MEMSET (&NextMacAddr, 0, sizeof (tMacAddr));
    }
    while (nmhGetNextIndexFsWssUserMacAccessListTable
           (MacAddr, &NextMacAddr) == SNMP_SUCCESS);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : WssUserCliShowModuleStatus                              */
/*                                                                           */
/* Description     : This function is used to display User Role              */
/*                   Module Status                                           */
/*                                                                           */
/* Input(s)        : CliHandle      - CLI Handler                            */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/

INT4
WssUserCliShowModuleStatus (tCliHandle CliHandle)
{
    UINT4               u4LoggedUserCount = 0;
    UINT4               u4BlockedUserCount = 0;
    INT4                i4UserTrapStatus = 0;
    INT4                i4UserRoleStatus = WSSUSER_MODULE_DISABLED;

    /* Display User Role Module Status and Blocked and Logged User Count */

    if (nmhGetFsWssUserRoleStatus (&i4UserRoleStatus) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhGetFsWssUserBlockedCount (&u4BlockedUserCount) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhGetFsWssUserLoggedCount (&u4LoggedUserCount) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhGetFsWssUserRoleTrapStatus (&i4UserTrapStatus) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (i4UserRoleStatus == WSSUSER_MODULE_ENABLED)
    {
        CliPrintf (CliHandle, "\r\nUser Role Module Status     : Enabled\r\n");
    }

    else if (i4UserRoleStatus == WSSUSER_MODULE_DISABLED)
    {
        CliPrintf (CliHandle, "\r\nUser Role Module Status     : Disabled\r\n");
    }

    CliPrintf (CliHandle, "\rNumber of Logged User       : %d\r\n",
               u4LoggedUserCount);
    CliPrintf (CliHandle, "\rNumber of Restricted Session: %d\r\n",
               u4BlockedUserCount);
    if (i4UserTrapStatus == WSSUSER_TRAP_ENABLE)
    {
        CliPrintf (CliHandle, "\rUser Role Trap Status       : Enabled\r\n");

    }

    else
    {
        CliPrintf (CliHandle, "\rUser Role Trap Status       : Disabled\r\n");

    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : WssUserCliClearCounters                                 */
/*                                                                           */
/* Description     : This function is used to clear Logged and               */
/*                   Blocked User Count                                      */
/*                                                                           */
/* Input(s)        : CliHandle      - CLI Handler                            */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/

INT4
WssUserCliClearCounters (tCliHandle CliHandle)
{
    UINT4               u4LoggedUserCount = 0;
    UINT4               u4BlockedUserCount = 0;

    UNUSED_PARAM (CliHandle);

    /* Clear Logged and Blocked User Count */

    if (nmhSetFsWssUserBlockedCount (u4BlockedUserCount) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsWssUserLoggedCount (u4LoggedUserCount) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : WssUserCliSetDebugs                                     */
/*                                                                           */
/* Description     : Set trace level                                         */
/* Input(s)        : CliHandle - Handler                                     */
/*                 : i4CliTraceVal - trace level                             */
/*                 : u1TraceFlag   - Enable / Disable                        */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/

INT4
WssUserCliSetDebugs (tCliHandle CliHandle, INT4 i4CliTraceVal, INT4 u1TraceFlag)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4TraceVal = 0;

    /* Get WssUser Trace Option */

    nmhGetFsWssUserTraceOption (&i4TraceVal);

    if (u1TraceFlag == CLI_ENABLE)
    {
        i4TraceVal |= i4CliTraceVal;
    }

    else
    {
        i4TraceVal &= (~(i4CliTraceVal));
    }

    if (nmhTestv2FsWssUserTraceOption (&u4ErrorCode, i4TraceVal) !=
        SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r%%Unable to set trace messages\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsWssUserTraceOption (i4TraceVal) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r%% Unable to set trace messages\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************/
/* Function    :  WssUserShowRunningConfig                                  */
/* Input       :  CliHandle                                                 */
/* Descritpion :  This Routine shows the configured CLI commands of the     */
/*                WSS module, by comapring with the default values.         */
/* Output      :  CLI commands                                              */
/* Returns     :  CLI_SUCCESS or CLI_FAILURE                                */
/****************************************************************************/

INT4
WssUserShowRunningConfig (tCliHandle CliHandle)
{
    WssUserShowRunningUserRoleModuleStatusConfig (CliHandle);

    WssUserShowRunningUserGroupConfig (CliHandle);

    WssUserShowRunningUserRoleConfig (CliHandle);

    WssUserShowRunningUserNameAccessListConfig (CliHandle);

    WssUserShowRunningUserMacAccessListConfig (CliHandle);

    WssUserShowRunningUserMacMappingConfig (CliHandle);

    WssUserShowRunningTrapStatus (CliHandle);

    return CLI_SUCCESS;
}

/****************************************************************************/
/* Function    :  WssUserShowRunningUserGroupConfig                         */
/* Input       :  CliHandle                                                 */
/* Descritpion :  This Routine displays current User Group Configuration    */
/* Output      :  CLI commands                                              */
/* Returns     :  NONE                                                      */
/****************************************************************************/

VOID
WssUserShowRunningUserGroupConfig (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE GroupName;
    UINT4               u4NextGroupId = 0;
    UINT4               u4GroupId = 0;
    UINT4               u4BandWidth;
    UINT4               u4DLBandWidth;
    UINT4               u4ULBandWidth;
    UINT4               u4Volume;
    UINT4               u4Time;
    INT4                i4RowStatus;
    UINT1               au1GroupName[MAX_GROUP_NAME_LENGTH];
    UINT1               au1Name[MAX_GROUP_NAME_LENGTH];
    INT1                i1RetValue = SNMP_FAILURE;

    /* Get all User Group details */

    i1RetValue = nmhGetFirstIndexFsWssUserGroupTable (&u4NextGroupId);

    if (i1RetValue == SNMP_FAILURE)
    {
        return;
    }

    do
    {
        u4GroupId = u4NextGroupId;

        if (u4GroupId != WSSUSER_DEFAULT_GROUP_ID)
        {
            u4DLBandWidth = 0;
            u4ULBandWidth = 0;
            u4Volume = 0;
            u4Time = 0;
            i4RowStatus = 0;

            MEMSET (&GroupName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
            MEMSET (&au1GroupName, 0, MAX_GROUP_NAME_LENGTH);

            GroupName.pu1_OctetList = (UINT1 *) au1GroupName;
            GroupName.i4_Length = MAX_GROUP_NAME_LENGTH;

            if (nmhGetFsWssUserGroupName (u4GroupId, &GroupName) !=
                SNMP_SUCCESS)
            {
                return;
            }

            if (nmhGetFsWssUserGroupBandWidth (u4GroupId, &u4BandWidth) !=
                SNMP_SUCCESS)
            {
                return;
            }

            if (nmhGetFsWssUserGroupDLBandWidth (u4GroupId, &u4DLBandWidth) !=
                SNMP_SUCCESS)
            {
                return;
            }

            if (nmhGetFsWssUserGroupULBandWidth (u4GroupId, &u4ULBandWidth) !=
                SNMP_SUCCESS)
            {
                return;
            }

            if (nmhGetFsWssUserGroupVolume (u4GroupId, &u4Volume) !=
                SNMP_SUCCESS)
            {
                return;
            }

            if (nmhGetFsWssUserGroupTime (u4GroupId, &u4Time) != SNMP_SUCCESS)
            {
                return;
            }

            if (nmhGetFsWssUserGroupRowStatus (u4GroupId, &i4RowStatus) !=
                SNMP_SUCCESS)
            {
                return;
            }

            MEMSET (&au1Name, 0, MAX_GROUP_NAME_LENGTH);
            SPRINTF ((CHR1 *) (au1Name), "%s%d",
                     WSSUSER_USER_GROUP_NAME, u4GroupId);

            CliPrintf (CliHandle, "!\r\n");
            CliPrintf (CliHandle, "\ruser-group %d\r\n", u4GroupId);

            if (MEMCMP (au1Name, GroupName.pu1_OctetList, GroupName.i4_Length)
                != 0)
            {
                CliPrintf (CliHandle, "\r group-name %s\r\n",
                           GroupName.pu1_OctetList);
            }
            if (u4BandWidth != WSSUSER_DEFAULT_BANDWIDTH)
            {
                CliPrintf (CliHandle, "\r bandwidth %dKpbs\r\n", u4BandWidth);
            }
            if (u4DLBandWidth != WSSUSER_DEFAULT_BANDWIDTH)
            {
                CliPrintf (CliHandle, "\r dl-bandwidth %dKpbs\r\n",
                           u4DLBandWidth);
            }

            if (u4ULBandWidth != WSSUSER_DEFAULT_BANDWIDTH)
            {
                CliPrintf (CliHandle, "\r ul-bandwidth %dKpbs\r\n",
                           u4ULBandWidth);
            }

            if (u4Volume != WSSUSER_DEFAULT_VOLUME)
            {
                CliPrintf (CliHandle, "\r volume %dMB\r\n", u4Volume);
            }

            if (u4Time != WSSUSER_DEFAULT_TIME)
            {
                CliPrintf (CliHandle, "\r time %d\r\n", u4Time);
            }

            CliPrintf (CliHandle, "!\r\n");
        }
    }
    while (nmhGetNextIndexFsWssUserGroupTable (u4GroupId,
                                               &u4NextGroupId) == SNMP_SUCCESS);

    return;
}

/****************************************************************************/
/* Function    :  WssUserShowRunningUserRoleConfig                          */
/* Input       :  CliHandle                                                 */
/* Descritpion :  This Routine displays current User Role Configuration     */
/* Output      :  CLI commands                                              */
/* Returns     :  NONE                                                      */
/****************************************************************************/

VOID
WssUserShowRunningUserRoleConfig (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE UserName;
    tSNMP_OCTET_STRING_TYPE NextUserName;
    UINT4               u4NextProfileId = 0;
    UINT4               u4ProfileId = 0;
    UINT4               u4GroupId = 0;
    UINT4               u4WlanId = 0;
    UINT1               au1NextUserName[MAX_USER_NAME_LENGTH + 1];
    UINT1               au1UserName[MAX_USER_NAME_LENGTH + 1];
    INT1                i1RetValue = SNMP_FAILURE;

    MEMSET (&NextUserName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1NextUserName, 0, MAX_USER_NAME_LENGTH + 1);

    NextUserName.pu1_OctetList = (UINT1 *) au1NextUserName;

    /* Get all User Role details */

    i1RetValue =
        nmhGetFirstIndexFsWssUserRoleTable (&NextUserName, &u4NextProfileId);

    if (i1RetValue == SNMP_FAILURE)
    {
        return;
    }

    CliPrintf (CliHandle, "!\r\n");
    do
    {
        u4ProfileId = u4NextProfileId;

        MEMSET (&UserName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        MEMSET (au1UserName, 0, MAX_USER_NAME_LENGTH + 1);

        UserName.pu1_OctetList = (UINT1 *) au1UserName;

        MEMCPY (UserName.pu1_OctetList, NextUserName.pu1_OctetList,
                NextUserName.i4_Length);
        UserName.i4_Length = NextUserName.i4_Length;

        u4GroupId = 0;

        if (nmhGetFsWssUserRoleGroupId (&UserName, u4ProfileId, &u4GroupId) !=
            SNMP_SUCCESS)
        {
            return;
        }

        WssGetCapwapWlanId (u4ProfileId, &u4WlanId);

        CliPrintf (CliHandle, "\ruser-role %s wlan-id %d group-id %d\r\n",
                   UserName.pu1_OctetList, u4WlanId, u4GroupId);

    }
    while (nmhGetNextIndexFsWssUserRoleTable (&UserName, &NextUserName,
                                              u4ProfileId,
                                              &u4NextProfileId) ==
           SNMP_SUCCESS);

    CliPrintf (CliHandle, "!\r\n");

    return;
}

/****************************************************************************/
/* Function    :  WssUserShowRunningUserNameAccessListConfig                */
/* Input       :  CliHandle                                                 */
/* Descritpion :  This Routine shows the configured CLI commands of the     */
/*                WSS module, by comapring with the default values.         */
/* Output      :  CLI commands                                              */
/* Returns     :  NONE                                                      */
/****************************************************************************/

VOID
WssUserShowRunningUserNameAccessListConfig (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE UserName;
    tSNMP_OCTET_STRING_TYPE NextUserName;
    UINT1               au1NextUserName[MAX_USER_NAME_LENGTH + 1];
    UINT1               au1UserName[MAX_USER_NAME_LENGTH + 1];
    INT1                i1RetValue = SNMP_FAILURE;

    MEMSET (&NextUserName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1NextUserName, 0, MAX_USER_NAME_LENGTH + 1);

    NextUserName.pu1_OctetList = (UINT1 *) au1NextUserName;

    /* Get the list of Restricted User */

    i1RetValue = nmhGetFirstIndexFsWssUserNameAccessListTable (&NextUserName);

    if (i1RetValue == SNMP_FAILURE)
    {
        return;
    }

    CliPrintf (CliHandle, "!\r\n");
    do
    {
        MEMSET (&UserName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        MEMSET (au1UserName, 0, MAX_USER_NAME_LENGTH + 1);

        UserName.pu1_OctetList = (UINT1 *) au1UserName;

        MEMCPY (UserName.pu1_OctetList, NextUserName.pu1_OctetList,
                NextUserName.i4_Length);
        UserName.i4_Length = NextUserName.i4_Length;

        CliPrintf (CliHandle, "\ruser-role restrict user-name %s\r\n",
                   UserName.pu1_OctetList);
    }
    while (nmhGetNextIndexFsWssUserNameAccessListTable
           (&UserName, &NextUserName) == SNMP_SUCCESS);

    CliPrintf (CliHandle, "!\r\n");

    return;
}

/****************************************************************************/
/* Function    :  WssUserShowRunningUserMacAccessListConfig                 */
/* Input       :  CliHandle                                                 */
/* Descritpion :  This Routine shows the configured CLI commands of the     */
/*                WSS module, by comapring with the default values.         */
/* Output      :  CLI commands                                              */
/* Returns     :  NONE                                                      */
/****************************************************************************/

VOID
WssUserShowRunningUserMacAccessListConfig (tCliHandle CliHandle)
{
    tMacAddr            NextMacAddr;
    tMacAddr            MacAddr;
    UINT1               au1String[MAC_TO_STRING_LEN];
    INT1                i1RetValue = SNMP_FAILURE;

    MEMSET (&NextMacAddr, 0, MAC_ADDR_LEN);
    MEMSET (&MacAddr, 0, MAC_ADDR_LEN);

    /* Get the list of Restricted Station */

    i1RetValue = nmhGetFirstIndexFsWssUserMacAccessListTable (&NextMacAddr);

    if (i1RetValue == SNMP_FAILURE)
    {
        return;
    }

    CliPrintf (CliHandle, "!\r\n");
    do
    {
        MEMSET (au1String, 0, MAC_TO_STRING_LEN);

        MEMCPY (MacAddr, NextMacAddr, MAC_ADDR_LEN);

        PrintMacAddress (MacAddr, au1String);

        CliPrintf (CliHandle, "\ruser-role restrict station %s\r\n", au1String);

    }
    while (nmhGetNextIndexFsWssUserMacAccessListTable
           (MacAddr, &NextMacAddr) == SNMP_SUCCESS);
    CliPrintf (CliHandle, "!\r\n");

    return;
}

/****************************************************************************/
/* Function    :  WssUserShowRunningUserMacMappingConfig                    */
/* Input       :  CliHandle                                                 */
/* Descritpion :  This Routine shows the configured CLI commands of the     */
/*                WSS module, by comapring with the default values.         */
/* Output      :  CLI commands                                              */
/* Returns     :  NONE                                                      */
/****************************************************************************/

VOID
WssUserShowRunningUserMacMappingConfig (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE NextUserName;
    tSNMP_OCTET_STRING_TYPE UserName;
    tMacAddr            NextMacAddr;
    tMacAddr            MacAddr;
    UINT1               au1NextUserName[MAX_USER_NAME_LENGTH + 1];
    UINT1               au1UserName[MAX_USER_NAME_LENGTH + 1];
    UINT1               au1String[MAC_TO_STRING_LEN];
    INT1                i1RetValue = SNMP_FAILURE;

    MEMSET (&NextUserName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&UserName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1NextUserName, 0, MAX_USER_NAME_LENGTH + 1);
    MEMSET (&NextMacAddr, 0, MAC_ADDR_LEN);
    MEMSET (&MacAddr, 0, MAC_ADDR_LEN);

    NextUserName.pu1_OctetList = (UINT1 *) au1NextUserName;

    /* Get User-MAC mapping details */

    i1RetValue =
        nmhGetFirstIndexFsWssUserMappingTable (&NextUserName, &NextMacAddr);

    if (i1RetValue == SNMP_FAILURE)
    {
        return;
    }

    CliPrintf (CliHandle, "!\r\n");
    do
    {
        MEMSET (&UserName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        MEMSET (&au1UserName, 0, MAX_USER_NAME_LENGTH + 1);
        MEMSET (au1String, 0, MAC_TO_STRING_LEN);

        UserName.pu1_OctetList = (UINT1 *) au1UserName;

        MEMCPY (UserName.pu1_OctetList, NextUserName.pu1_OctetList,
                NextUserName.i4_Length);
        UserName.i4_Length = NextUserName.i4_Length;

        MEMCPY (MacAddr, NextMacAddr, MAC_ADDR_LEN);

        PrintMacAddress (MacAddr, au1String);
        CliPrintf (CliHandle, "\ruser-mac-map %s user-name %s\r\n",
                   au1String, UserName.pu1_OctetList);

    }
    while (nmhGetNextIndexFsWssUserMappingTable (&UserName, &NextUserName,
                                                 MacAddr,
                                                 &NextMacAddr) == SNMP_SUCCESS);
    CliPrintf (CliHandle, "!\r\n");

    return;
}

/****************************************************************************/
/* Function    :  WssUserShowRunningUserRoleModuleStatusConfig              */
/* Input       :  CliHandle                                                 */
/* Descritpion :  This Routine shows the configured CLI commands of the     */
/*                WSS module, by comapring with the default values.         */
/* Output      :  CLI commands                                              */
/* Returns     :  NONE                                                      */
/****************************************************************************/

VOID
WssUserShowRunningUserRoleModuleStatusConfig (tCliHandle CliHandle)
{
    INT4                i4UserRoleStatus = WSSUSER_DEFAULT_MODULE_STATUS;

    /* Get User Role Module Status */

    if (nmhGetFsWssUserRoleStatus (&i4UserRoleStatus) != SNMP_SUCCESS)
    {
        return;
    }

    CliPrintf (CliHandle, "!\r\n");

    if (i4UserRoleStatus != WSSUSER_DEFAULT_MODULE_STATUS)
    {
        if (i4UserRoleStatus == WSSUSER_MODULE_ENABLED)
        {
            CliPrintf (CliHandle, "\ruser-role enable\r\n");
        }

        else
        {
            CliPrintf (CliHandle, "\ruser-role disable\r\n");
        }
    }
    CliPrintf (CliHandle, "!\r\n");

    return;
}

/****************************************************************************/
/* Function    :  WssUserShowRunningTrapStatus                              */
/* Input       :  CliHandle                                                 */
/* Descritpion :  This Routine shows the configured CLI commands of the     */
/*                WSS module, by comapring with the default values.         */
/* Output      :  CLI commands                                              */
/* Returns     :  NONE                                                      */
/****************************************************************************/

VOID
WssUserShowRunningTrapStatus (tCliHandle CliHandle)
{
    INT4                i4TrapStatus = 0;

    nmhGetFsWssUserRoleTrapStatus (&i4TrapStatus);

    if (WSSUSER_DEFAULT_TRAP_STATUS == WSSUSER_TRAP_DISABLE)
    {
        if (i4TrapStatus != WSSUSER_DEFAULT_TRAP_STATUS)
        {
            CliPrintf (CliHandle, "user-role traps enable \r\n\r\n");
        }
    }
    else if (WSSUSER_DEFAULT_TRAP_STATUS == WSSUSER_TRAP_ENABLE)
    {
        if (i4TrapStatus != WSSUSER_DEFAULT_TRAP_STATUS)
        {
            CliPrintf (CliHandle, "no user-role traps enable \r\n\r\n");
        }
    }
    CliPrintf (CliHandle, "!\r\n\r\n");

    return;
}

/***************************************************************************
 * FUNCTION NAME    : WssUserGetGroupCfgPrompt                              
 *
 * DESCRIPTION      : This function returns the prompt to be displayed
 *                    for WssUser Group. It is exported to CLI module.
 *
 * INPUT            : None
 *
 *
 * OUTPUT           : pi1ModeName - Mode String
 *                    pi1DispStr - Display string
 *
 * RETURNS          : OSIX_TRUE/OSIX_FALSE
 *
 **************************************************************************/

INT1
WssUserGetGroupCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len = STRLEN (CLI_WSSUSER_GROUP_MODE);
    UINT4               u4GroupId = 0;

    if ((!pi1DispStr) || (!pi1ModeName))
    {
        return OSIX_FALSE;
    }

    if (STRNCMP (pi1ModeName, CLI_WSSUSER_GROUP_MODE, u4Len) != 0)
    {
        return OSIX_FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    u4GroupId = (UINT4) CLI_ATOI (pi1ModeName);

    /*
     * No need to take lock here, since it is taken by
     * Cli in cli_process_wssuser_cmd.
     */
    CLI_SET_GROUP_ID ((INT4) u4GroupId);

    STRCPY (pi1DispStr, "(config-user_group)#");

    return OSIX_TRUE;
}

/***************************************************************/
/* Function    :  WssShowUtilBandwidthDisplay                  */
/* Input       :  CliHandle                                    */
/* Descritpion :  This Routine is used to display Bandwidth    */
/*                with the appropriate units.                  */
/* Output      :  CLI commands                                 */
/* Returns     :  NONE                                         */
/***************************************************************/

VOID
WssShowUtilBandwidthDisplay (tCliHandle CliHandle, UINT4 u4BandWidth,
                             UINT4 u4DLBandWidth, UINT4 u4ULBandWidth)
{
    if (u4BandWidth >= WSSUSER_1024Kbps)
    {
        CliPrintf (CliHandle, "Bandwidth            :   %d Mbps\r\n",
                   (u4BandWidth / WSSUSER_1024Kbps));
    }
    else
    {
        CliPrintf (CliHandle, "Bandwidth            :   %d Kbps\r\n",
                   u4BandWidth);
    }

    if (u4DLBandWidth >= WSSUSER_1024Kbps)
    {
        CliPrintf (CliHandle, "Download Bandwidth    :   %d Mbps\r\n",
                   (u4DLBandWidth / WSSUSER_1024Kbps));
    }
    else
    {
        CliPrintf (CliHandle, "Download Bandwidth     :   %d Kbps\r\n",
                   u4DLBandWidth);
    }

    if (u4ULBandWidth >= WSSUSER_1024Kbps)
    {
        CliPrintf (CliHandle, "Upload Bandwidth        :   %d Mbps\r\n",
                   (u4ULBandWidth / WSSUSER_1024Kbps));
    }
    else
    {
        CliPrintf (CliHandle, "Upload Bandwidth        :   %d Kbps\r\n",
                   u4ULBandWidth);
    }
}

/***************************************************************/
/* Function    :  WssShowUtilVolumeDisplay                     */
/* Input       :  CliHandle                                    */
/* Descritpion :  This Routine is used to display Volume       */
/*                with the appropriate units.                  */
/* Output      :  CLI commands                                 */
/* Returns     :  NONE                                         */
/***************************************************************/

VOID
WssShowUtilVolumeDisplay (tCliHandle CliHandle, UINT4 u4Volume)
{
    CliPrintf (CliHandle, "Volume             :   %d KB\r\n", u4Volume);
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : WssUserUtilClearRestrictedUserTable                     */
/*                                                                           */
/* Description     : This function is used to clear Restricted user entries  */
/*                                                                           */
/* Input(s)        : CliHandle - CLI HANDLER                                 */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : NONE                                                    */
/*                                                                           */
/*****************************************************************************/

VOID
WssUserUtilClearRestrictedUserTable (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE NextUserName;
    tSNMP_OCTET_STRING_TYPE UserName;
    UINT1               au1NextUserName[MAX_USER_NAME_LENGTH + 1];
    UINT1               au1UserName[MAX_USER_NAME_LENGTH + 1];

    MEMSET (&NextUserName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&UserName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1NextUserName, 0, MAX_USER_NAME_LENGTH + 1);
    MEMSET (&au1UserName, 0, MAX_USER_NAME_LENGTH + 1);

    NextUserName.pu1_OctetList = (UINT1 *) au1NextUserName;
    UserName.pu1_OctetList = (UINT1 *) au1UserName;
    MEMSET (UserName.pu1_OctetList, 0, MAX_USER_NAME_LENGTH + 1);

    while (nmhGetNextIndexFsWssUserNameAccessListTable
           (&UserName, &NextUserName) == SNMP_SUCCESS)
    {
        if (WssUserCliNoRestrictUserName (CliHandle, &NextUserName) ==
            CLI_FAILURE)
        {
            return;
        }

        MEMSET (UserName.pu1_OctetList, 0, MAX_USER_NAME_LENGTH + 1);

        MEMCPY (UserName.pu1_OctetList, NextUserName.pu1_OctetList,
                NextUserName.i4_Length);
        UserName.i4_Length = NextUserName.i4_Length;

        MEMSET (NextUserName.pu1_OctetList, 0, MAX_USER_NAME_LENGTH + 1);
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : WssUserUtilClearRestrictedStationTable                  */
/*                                                                           */
/* Description     : This function is used to clear Restricted               */
/*                   station entries                                         */
/*                                                                           */
/* Input(s)        : CliHandle - CLI HANDLER                                 */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : NONE                                                    */
/*                                                                           */
/*****************************************************************************/

VOID
WssUserUtilClearRestrictedStationTable (tCliHandle CliHandle)
{
    tMacAddr            NextMacAddr;
    tMacAddr            MacAddr;

    MEMSET (&NextMacAddr, 0, sizeof (tMacAddr));
    MEMSET (&MacAddr, 0, sizeof (tMacAddr));

    while (nmhGetNextIndexFsWssUserMacAccessListTable
           (MacAddr, &NextMacAddr) == SNMP_SUCCESS)
    {
        if (WssUserCliNoRestrictUserMac (CliHandle, NextMacAddr) != CLI_SUCCESS)
        {
            return;
        }

        MEMCPY (MacAddr, NextMacAddr, MAC_ADDR_LEN);

        MEMSET (&NextMacAddr, 0, sizeof (tMacAddr));
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : WssUserUtilDisconnectStation                            */
/* Description     : This function is used to disconnect Ongoing session     */
/*                                                                           */
/* Input(s)        : NONE                                                    */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/* Returns         : NONE                                                    */
/*                                                                           */
/*****************************************************************************/
VOID
WssUserUtilDisconnectStation ()
{
    tSNMP_OCTET_STRING_TYPE UserName;
    tSNMP_OCTET_STRING_TYPE NextUserName;
    tMacAddr            MacAddr;
    tMacAddr            NextMacAddr;
    UINT1               au1NextUserName[MAX_USER_NAME_LENGTH + 1];
    UINT1               au1UserName[MAX_USER_NAME_LENGTH + 1];
    UINT4               u4TerminateCause = 0;

    MEMSET (&NextUserName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&UserName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1NextUserName, 0, MAX_USER_NAME_LENGTH + 1);
    MEMSET (&au1UserName, 0, MAX_USER_NAME_LENGTH + 1);
    MEMSET (&NextMacAddr, 0, MAC_ADDR_LEN);
    MEMSET (&MacAddr, 0, MAC_ADDR_LEN);

    NextUserName.pu1_OctetList = (UINT1 *) au1NextUserName;
    UserName.pu1_OctetList = (UINT1 *) au1UserName;
    u4TerminateCause = ADMIN_RESET;
    while ((nmhGetNextIndexFsWssUserSessionTable (&UserName, &NextUserName,
                                                  MacAddr,
                                                  &NextMacAddr)) ==
           SNMP_SUCCESS)
    {

        WssUserDeAuthenticateStation (NextMacAddr, OSIX_TRUE, u4TerminateCause);

        MEMCPY (UserName.pu1_OctetList, NextUserName.pu1_OctetList,
                NextUserName.i4_Length);
        UserName.i4_Length = NextUserName.i4_Length;
        MEMCPY (MacAddr, NextMacAddr, MAC_ADDR_LEN);

        MEMSET (NextUserName.pu1_OctetList, 0, MAX_USER_NAME_LENGTH + 1);
        MEMSET (&NextMacAddr, 0, MAC_ADDR_LEN);
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : WssUserUtilClearUserRoleTable                           */
/*                                                                           */
/* Description     : This function is used to clear User Role Table entries  */
/*                                                                           */
/* Input(s)        : CliHandle - CLI HANDLER                                 */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : NONE                                                    */
/*                                                                           */
/*****************************************************************************/
VOID
WssUserUtilClearUserRoleTable (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE UserName;
    tSNMP_OCTET_STRING_TYPE NextUserName;
    UINT4               u4NextProfileId = 0;
    UINT4               u4ProfileId = 0;
    UINT1               au1NextUserName[MAX_USER_NAME_LENGTH + 1];
    UINT1               au1UserName[MAX_USER_NAME_LENGTH + 1];

    MEMSET (&UserName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1UserName, 0, MAX_USER_NAME_LENGTH + 1);
    MEMSET (&NextUserName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1NextUserName, 0, MAX_USER_NAME_LENGTH + 1);

    NextUserName.pu1_OctetList = (UINT1 *) au1NextUserName;
    UserName.pu1_OctetList = (UINT1 *) au1UserName;
    MEMSET (UserName.pu1_OctetList, 0, MAX_USER_NAME_LENGTH + 1);

    while (nmhGetNextIndexFsWssUserRoleTable (&UserName, &NextUserName,
                                              u4ProfileId,
                                              &u4NextProfileId) == SNMP_SUCCESS)
    {
        if (WssUserCliDeleteRole (CliHandle, &NextUserName, u4NextProfileId) !=
            CLI_SUCCESS)
        {
            return;
        }

        u4ProfileId = u4NextProfileId;

        MEMCPY (UserName.pu1_OctetList, NextUserName.pu1_OctetList,
                NextUserName.i4_Length);
        UserName.i4_Length = NextUserName.i4_Length;

        MEMSET (NextUserName.pu1_OctetList, 0, MAX_USER_NAME_LENGTH + 1);
        u4NextProfileId = 0;
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : WssUserUtilClearUserGroupTable                          */
/*                                                                           */
/* Description     : This function is used to clear User Group Table entries */
/*                                                                           */
/* Input(s)        : CliHandle - CLI HANDLER                                 */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : NONE                                                    */
/*                                                                           */
/*****************************************************************************/
VOID
WssUserUtilClearUserGroupTable (tCliHandle CliHandle)
{
    UINT4               u4NextGroupId = 0;
    UINT4               u4GroupId = 0;

    while (nmhGetNextIndexFsWssUserGroupTable (u4GroupId,
                                               &u4NextGroupId) == SNMP_SUCCESS)
    {
        if (u4NextGroupId != WSSUSER_DEFAULT_GROUP_ID)
        {
            if (WssUserCliDeleteGroup (CliHandle, u4NextGroupId) != CLI_SUCCESS)
            {
                return;
            }
        }
        u4GroupId = u4NextGroupId;
        u4NextGroupId = 0;
    }

    return;
}

INT1
WssUserCliTrapEnable (tCliHandle CliHandle)
{
    UINT4               u4ErrorCode = 0;
    UNUSED_PARAM (CliHandle);
    if (nmhTestv2FsWssUserRoleStatus (&u4ErrorCode,
                                      WSSUSER_TRAP_ENABLE) == SNMP_SUCCESS)
    {
        nmhSetFsWssUserRoleTrapStatus (WSSUSER_TRAP_ENABLE);
        return CLI_SUCCESS;
    }
    else
    {
        return CLI_FAILURE;
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : WssUserCliTrapDisable                                   */
/*                                                                           */
/* Description     : This function is used to Disable User Role Trap         */
/*                                                                           */
/* Input(s)        : CliHandle    - CLI Handler                              */
/*                                                                           */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : NONE                                                    */
/*                                                                           */
/*****************************************************************************/

INT1
WssUserCliTrapDisable (tCliHandle CliHandle)
{
    UINT4               u4ErrorCode = 0;
    UNUSED_PARAM (CliHandle);
    if (nmhTestv2FsWssUserRoleStatus (&u4ErrorCode,
                                      WSSUSER_TRAP_DISABLE) == SNMP_SUCCESS)
    {
        nmhSetFsWssUserRoleTrapStatus (WSSUSER_TRAP_DISABLE);
        return CLI_SUCCESS;
    }
    else
    {
        return CLI_FAILURE;
    }
}

#endif /* WSSUSERCLI_C */
