/* $Id: fsuserwr.c,v 1.2 2017/11/24 10:37:12 siva Exp $ */
# include  "lr.h"
# include  "fssnmp.h"
# include  "userrole.h"
# include  "fsuserlw.h"
# include  "fsuserwr.h"
# include  "fsuserdb.h"

VOID
RegisterFSWSSU ()
{
    SNMPRegisterMibWithLock (&fswssuOID, &fswssuEntry, UserLock, UserUnLock,
                             SNMP_MSR_TGR_FALSE);
    SNMPAddSysorEntry (&fswssuOID, (const UINT1 *) "fswssuser");
}

VOID
UnRegisterFSWSSU ()
{
    SNMPUnRegisterMib (&fswssuOID, &fswssuEntry);
    SNMPDelSysorEntry (&fswssuOID, (const UINT1 *) "fswssuser");
}

INT4
FsWssUserRoleStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsWssUserRoleStatus (&(pMultiData->i4_SLongValue)));
}

INT4
FsWssUserBlockedCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsWssUserBlockedCount (&(pMultiData->u4_ULongValue)));
}

INT4
FsWssUserLoggedCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsWssUserLoggedCount (&(pMultiData->u4_ULongValue)));
}

INT4
FsWssUserTraceOptionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsWssUserTraceOption (&(pMultiData->i4_SLongValue)));
}

INT4
FsWssUserRoleTrapStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsWssUserRoleTrapStatus (&(pMultiData->i4_SLongValue)));
}

INT4
FsWssUserRoleStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsWssUserRoleStatus (pMultiData->i4_SLongValue));
}

INT4
FsWssUserBlockedCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsWssUserBlockedCount (pMultiData->u4_ULongValue));
}

INT4
FsWssUserLoggedCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsWssUserLoggedCount (pMultiData->u4_ULongValue));
}

INT4
FsWssUserTraceOptionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsWssUserTraceOption (pMultiData->i4_SLongValue));
}

INT4
FsWssUserRoleTrapStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsWssUserRoleTrapStatus (pMultiData->i4_SLongValue));
}

INT4
FsWssUserRoleStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsWssUserRoleStatus (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsWssUserBlockedCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsWssUserBlockedCount
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
FsWssUserLoggedCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsWssUserLoggedCount
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
FsWssUserTraceOptionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsWssUserTraceOption
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsWssUserRoleTrapStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsWssUserRoleTrapStatus
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsWssUserRoleStatusDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsWssUserRoleStatus
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsWssUserBlockedCountDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsWssUserBlockedCount
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsWssUserLoggedCountDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsWssUserLoggedCount
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsWssUserTraceOptionDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsWssUserTraceOption
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsWssUserRoleTrapStatusDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsWssUserRoleTrapStatus
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsWssUserGroupTable (tSnmpIndex * pFirstMultiIndex,
                                 tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsWssUserGroupTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsWssUserGroupTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsWssUserGroupNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWssUserGroupTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWssUserGroupName (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiData->pOctetStrValue));

}

INT4
FsWssUserGroupBandWidthGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWssUserGroupTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWssUserGroupBandWidth (pMultiIndex->pIndex[0].u4_ULongValue,
                                           &(pMultiData->u4_ULongValue)));

}

INT4
FsWssUserGroupDLBandWidthGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWssUserGroupTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWssUserGroupDLBandWidth
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsWssUserGroupULBandWidthGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWssUserGroupTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWssUserGroupULBandWidth
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsWssUserGroupVolumeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWssUserGroupTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWssUserGroupVolume (pMultiIndex->pIndex[0].u4_ULongValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
FsWssUserGroupTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWssUserGroupTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWssUserGroupTime (pMultiIndex->pIndex[0].u4_ULongValue,
                                      &(pMultiData->u4_ULongValue)));

}

INT4
FsWssUserGroupRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWssUserGroupTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWssUserGroupRowStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                           &(pMultiData->i4_SLongValue)));

}

INT4
FsWssUserGroupNameSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsWssUserGroupName (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiData->pOctetStrValue));

}

INT4
FsWssUserGroupBandWidthSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsWssUserGroupBandWidth (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiData->u4_ULongValue));

}

INT4
FsWssUserGroupDLBandWidthSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsWssUserGroupDLBandWidth
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsWssUserGroupULBandWidthSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsWssUserGroupULBandWidth
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsWssUserGroupVolumeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsWssUserGroupVolume (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiData->u4_ULongValue));

}

INT4
FsWssUserGroupTimeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsWssUserGroupTime (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiData->u4_ULongValue));

}

INT4
FsWssUserGroupRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsWssUserGroupRowStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
FsWssUserGroupNameTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2FsWssUserGroupName (pu4Error,
                                         pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiData->pOctetStrValue));

}

INT4
FsWssUserGroupBandWidthTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2FsWssUserGroupBandWidth (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              u4_ULongValue,
                                              pMultiData->u4_ULongValue));

}

INT4
FsWssUserGroupDLBandWidthTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2FsWssUserGroupDLBandWidth (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                u4_ULongValue,
                                                pMultiData->u4_ULongValue));

}

INT4
FsWssUserGroupULBandWidthTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2FsWssUserGroupULBandWidth (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                u4_ULongValue,
                                                pMultiData->u4_ULongValue));

}

INT4
FsWssUserGroupVolumeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2FsWssUserGroupVolume (pu4Error,
                                           pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiData->u4_ULongValue));

}

INT4
FsWssUserGroupTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2FsWssUserGroupTime (pu4Error,
                                         pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiData->u4_ULongValue));

}

INT4
FsWssUserGroupRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2FsWssUserGroupRowStatus (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              u4_ULongValue,
                                              pMultiData->i4_SLongValue));

}

INT4
FsWssUserGroupTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsWssUserGroupTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsWssUserRoleTable (tSnmpIndex * pFirstMultiIndex,
                                tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsWssUserRoleTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsWssUserRoleTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue,
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsWssUserRoleGroupIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWssUserRoleTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWssUserRoleGroupId (pMultiIndex->pIndex[0].pOctetStrValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
FsWssUserRoleRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWssUserRoleTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWssUserRoleRowStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
FsWssUserRoleGroupIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsWssUserRoleGroupId (pMultiIndex->pIndex[0].pOctetStrValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiData->u4_ULongValue));

}

INT4
FsWssUserRoleRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsWssUserRoleRowStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsWssUserRoleGroupIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2FsWssUserRoleGroupId (pu4Error,
                                           pMultiIndex->pIndex[0].
                                           pOctetStrValue,
                                           pMultiIndex->pIndex[1].u4_ULongValue,
                                           pMultiData->u4_ULongValue));

}

INT4
FsWssUserRoleRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2FsWssUserRoleRowStatus (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             pOctetStrValue,
                                             pMultiIndex->pIndex[1].
                                             u4_ULongValue,
                                             pMultiData->i4_SLongValue));

}

INT4
FsWssUserRoleTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsWssUserRoleTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsWssUserNameAccessListTable (tSnmpIndex * pFirstMultiIndex,
                                          tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsWssUserNameAccessListTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsWssUserNameAccessListTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsWssUserNameAccessListRowStatusGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWssUserNameAccessListTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWssUserNameAccessListRowStatus
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsWssUserNameAccessListRowStatusSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhSetFsWssUserNameAccessListRowStatus
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsWssUserNameAccessListRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhTestv2FsWssUserNameAccessListRowStatus (pu4Error,
                                                       pMultiIndex->pIndex[0].
                                                       pOctetStrValue,
                                                       pMultiData->
                                                       i4_SLongValue));

}

INT4
FsWssUserNameAccessListTableDep (UINT4 *pu4Error,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsWssUserNameAccessListTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsWssUserMacAccessListTable (tSnmpIndex * pFirstMultiIndex,
                                         tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsWssUserMacAccessListTable ((tMacAddr *)
                                                         pNextMultiIndex->
                                                         pIndex[0].
                                                         pOctetStrValue->
                                                         pu1_OctetList) ==
            SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsWssUserMacAccessListTable
            (*(tMacAddr *) pFirstMultiIndex->pIndex[0].pOctetStrValue->
             pu1_OctetList,
             (tMacAddr *) pNextMultiIndex->pIndex[0].pOctetStrValue->
             pu1_OctetList) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[0].pOctetStrValue->i4_Length = 6;
    return SNMP_SUCCESS;
}

INT4
FsWssUserMacAccessListRowStatusGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWssUserMacAccessListTable ((*(tMacAddr *)
                                                              pMultiIndex->
                                                              pIndex[0].
                                                              pOctetStrValue->
                                                              pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWssUserMacAccessListRowStatus ((*(tMacAddr *) pMultiIndex->
                                                    pIndex[0].pOctetStrValue->
                                                    pu1_OctetList),
                                                   &(pMultiData->
                                                     i4_SLongValue)));

}

INT4
FsWssUserMacAccessListRowStatusSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhSetFsWssUserMacAccessListRowStatus ((*(tMacAddr *) pMultiIndex->
                                                    pIndex[0].pOctetStrValue->
                                                    pu1_OctetList),
                                                   pMultiData->i4_SLongValue));

}

INT4
FsWssUserMacAccessListRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhTestv2FsWssUserMacAccessListRowStatus (pu4Error,
                                                      (*(tMacAddr *)
                                                       pMultiIndex->pIndex[0].
                                                       pOctetStrValue->
                                                       pu1_OctetList),
                                                      pMultiData->
                                                      i4_SLongValue));

}

INT4
FsWssUserMacAccessListTableDep (UINT4 *pu4Error,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsWssUserMacAccessListTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsWssUserMappingTable (tSnmpIndex * pFirstMultiIndex,
                                   tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsWssUserMappingTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue,
             (tMacAddr *) pNextMultiIndex->pIndex[1].pOctetStrValue->
             pu1_OctetList) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsWssUserMappingTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue,
             *(tMacAddr *) pFirstMultiIndex->pIndex[1].pOctetStrValue->
             pu1_OctetList,
             (tMacAddr *) pNextMultiIndex->pIndex[1].pOctetStrValue->
             pu1_OctetList) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[1].pOctetStrValue->i4_Length = 6;
    return SNMP_SUCCESS;
}

INT4
FsWssUserMappingRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWssUserMappingTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         (*(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->
          pu1_OctetList)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWssUserMappingRowStatus
            (pMultiIndex->pIndex[0].pOctetStrValue,
             (*(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->
              pu1_OctetList), &(pMultiData->i4_SLongValue)));

}

INT4
FsWssUserMappingRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsWssUserMappingRowStatus
            (pMultiIndex->pIndex[0].pOctetStrValue,
             (*(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->
              pu1_OctetList), pMultiData->i4_SLongValue));

}

INT4
FsWssUserMappingRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2FsWssUserMappingRowStatus (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                pOctetStrValue,
                                                (*(tMacAddr *) pMultiIndex->
                                                 pIndex[1].pOctetStrValue->
                                                 pu1_OctetList),
                                                pMultiData->i4_SLongValue));

}

INT4
FsWssUserMappingTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsWssUserMappingTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsWssUserSessionTable (tSnmpIndex * pFirstMultiIndex,
                                   tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsWssUserSessionTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue,
             (tMacAddr *) pNextMultiIndex->pIndex[1].pOctetStrValue->
             pu1_OctetList) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsWssUserSessionTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue,
             *(tMacAddr *) pFirstMultiIndex->pIndex[1].pOctetStrValue->
             pu1_OctetList,
             (tMacAddr *) pNextMultiIndex->pIndex[1].pOctetStrValue->
             pu1_OctetList) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[1].pOctetStrValue->i4_Length = 6;
    return SNMP_SUCCESS;
}

INT4
FsWssUserWlanIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWssUserSessionTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         (*(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->
          pu1_OctetList)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWssUserWlanIndex (pMultiIndex->pIndex[0].pOctetStrValue,
                                      (*(tMacAddr *) pMultiIndex->pIndex[1].
                                       pOctetStrValue->pu1_OctetList),
                                      &(pMultiData->u4_ULongValue)));

}

INT4
FsWssUserAllotedBandWidthGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWssUserSessionTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         (*(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->
          pu1_OctetList)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWssUserAllotedDLBandWidth
            (pMultiIndex->pIndex[0].pOctetStrValue,
             (*(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->
              pu1_OctetList), &(pMultiData->u4_ULongValue)));
}

INT4
FsWssUserAllotedDLBandWidthGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWssUserSessionTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         (*(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->
          pu1_OctetList)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWssUserAllotedDLBandWidth
            (pMultiIndex->pIndex[0].pOctetStrValue,
             (*(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->
              pu1_OctetList), &(pMultiData->u4_ULongValue)));

}

INT4
FsWssUserAllotedULBandWidthGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWssUserSessionTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         (*(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->
          pu1_OctetList)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWssUserAllotedULBandWidth
            (pMultiIndex->pIndex[0].pOctetStrValue,
             (*(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->
              pu1_OctetList), &(pMultiData->u4_ULongValue)));

}

INT4
FsWssUserAllotedVolumeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWssUserSessionTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         (*(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->
          pu1_OctetList)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWssUserAllotedVolume (pMultiIndex->pIndex[0].pOctetStrValue,
                                          (*(tMacAddr *) pMultiIndex->pIndex[1].
                                           pOctetStrValue->pu1_OctetList),
                                          &(pMultiData->u4_ULongValue)));

}

INT4
FsWssUserAllotedTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWssUserSessionTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         (*(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->
          pu1_OctetList)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWssUserAllotedTime (pMultiIndex->pIndex[0].pOctetStrValue,
                                        (*(tMacAddr *) pMultiIndex->pIndex[1].
                                         pOctetStrValue->pu1_OctetList),
                                        &(pMultiData->u4_ULongValue)));

}

INT4
FsWssUserUsedVolumeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWssUserSessionTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         (*(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->
          pu1_OctetList)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWssUserUsedVolume (pMultiIndex->pIndex[0].pOctetStrValue,
                                       (*(tMacAddr *) pMultiIndex->pIndex[1].
                                        pOctetStrValue->pu1_OctetList),
                                       &(pMultiData->u4_ULongValue)));

}

INT4
FsWssUserUsedTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWssUserSessionTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         (*(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->
          pu1_OctetList)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWssUserUsedTime (pMultiIndex->pIndex[0].pOctetStrValue,
                                     (*(tMacAddr *) pMultiIndex->pIndex[1].
                                      pOctetStrValue->pu1_OctetList),
                                     &(pMultiData->u4_ULongValue)));

}
