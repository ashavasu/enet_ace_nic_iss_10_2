/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsuserlw.c,v 1.1.1.1 2016/09/20 10:42:41 siva Exp $
*
* Description: Protocol Low Level Routines for WSSUSER
*********************************************************************/
# include  "lr.h" 
# include  "fssnmp.h"
# include  "fsuserlw.h"
# include  "userinc.h"

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsWssUserRoleStatus
 Input       :  The Indices

                The Object 
                retValFsWssUserRoleStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1 nmhGetFsWssUserRoleStatus(INT4 *pi4RetValFsWssUserRoleStatus)
{
	*pi4RetValFsWssUserRoleStatus = gWssUserGlobals.i4UserRoleStatus;
	return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsWssUserBlockedCount
 Input       :  The Indices

                The Object 
                retValFsWssUserBlockedCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1 nmhGetFsWssUserBlockedCount(UINT4 *pu4RetValFsWssUserBlockedCount)
{
	*pu4RetValFsWssUserBlockedCount = gWssUserGlobals.u4BlockedCount ;    
	return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsWssUserLoggedCount
 Input       :  The Indices

                The Object 
                retValFsWssUserLoggedCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1 nmhGetFsWssUserLoggedCount(UINT4 *pu4RetValFsWssUserLoggedCount)
{
    *pu4RetValFsWssUserLoggedCount = gWssUserGlobals.u4LoggedCount;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsWssUserTraceOption
 Input       :  The Indices

                The Object 
                retValFsWssUserTraceOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1 nmhGetFsWssUserTraceOption(INT4 *pi4RetValFsWssUserTraceOption)
{
    *pi4RetValFsWssUserTraceOption = (INT4)(gWssUserGlobals.u4WssUserTrcOpt);
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsWssUserRoleTrapStatus
 Input       :  The Indices

                The Object 
                retValFsWssUserRoleTrapStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsWssUserRoleTrapStatus(INT4 *pi4RetValFsWssUserRoleTrapStatus)
{
    *pi4RetValFsWssUserRoleTrapStatus = (INT4)gWssUserGlobals.u4UserTrapStatus;
     return SNMP_SUCCESS;

}
/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsWssUserRoleStatus
 Input       :  The Indices

                The Object 
                setValFsWssUserRoleStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1 nmhSetFsWssUserRoleStatus(INT4 i4SetValFsWssUserRoleStatus)
{
    gWssUserGlobals.i4UserRoleStatus = i4SetValFsWssUserRoleStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsWssUserBlockedCount
 Input       :  The Indices

                The Object 
                setValFsWssUserBlockedCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1 nmhSetFsWssUserBlockedCount(UINT4 u4SetValFsWssUserBlockedCount)
{
    gWssUserGlobals.u4BlockedCount = u4SetValFsWssUserBlockedCount;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsWssUserLoggedCount
 Input       :  The Indices

                The Object 
                setValFsWssUserLoggedCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1 nmhSetFsWssUserLoggedCount(UINT4 u4SetValFsWssUserLoggedCount)
{
    gWssUserGlobals.u4LoggedCount = u4SetValFsWssUserLoggedCount;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsWssUserTraceOption
 Input       :  The Indices

                The Object 
                setValFsWssUserTraceOption
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1 nmhSetFsWssUserTraceOption(INT4 i4SetValFsWssUserTraceOption)
{
    gWssUserGlobals.u4WssUserTrcOpt = (UINT4)(i4SetValFsWssUserTraceOption);
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFsWssUserRoleTrapStatus
 Input       :  The Indices

                The Object 
                setValFsWssUserRoleTrapStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsWssUserRoleTrapStatus(INT4 i4SetValFsWssUserRoleTrapStatus)
{
    gWssUserGlobals.u4UserTrapStatus = (UINT4) i4SetValFsWssUserRoleTrapStatus;
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsWssUserRoleStatus
 Input       :  The Indices

                The Object 
                testValFsWssUserRoleStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1 nmhTestv2FsWssUserRoleStatus(UINT4 *pu4ErrorCode , INT4 i4TestValFsWssUserRoleStatus)
{
	if ((i4TestValFsWssUserRoleStatus != WSSUSER_MODULE_ENABLED) &&
			(i4TestValFsWssUserRoleStatus != WSSUSER_MODULE_DISABLED))
	{
		WSSUSER_TRC (WSSUSER_CONTROL_PATH_TRC | WSSUSER_ALL_FAILURE_TRC,
				"nmhTestv2FsWssUserRoleStatus: Invalid value\n");
		*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
		return SNMP_FAILURE;
	}

	return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsWssUserBlockedCount
 Input       :  The Indices

                The Object 
                testValFsWssUserBlockedCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1 nmhTestv2FsWssUserBlockedCount(UINT4 *pu4ErrorCode , UINT4 u4TestValFsWssUserBlockedCount)
{
	if (u4TestValFsWssUserBlockedCount != 0)

	{
		WSSUSER_TRC (WSSUSER_CONTROL_PATH_TRC | WSSUSER_ALL_FAILURE_TRC,
				"nmhTestv2FsWssUserBlockedCount: Invalid value\r\n");
		*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
		return SNMP_FAILURE;
	}

	return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsWssUserLoggedCount
 Input       :  The Indices

                The Object 
                testValFsWssUserLoggedCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1 nmhTestv2FsWssUserLoggedCount(UINT4 *pu4ErrorCode , UINT4 u4TestValFsWssUserLoggedCount)
{
	if (u4TestValFsWssUserLoggedCount != 0)

	{
		WSSUSER_TRC (WSSUSER_CONTROL_PATH_TRC | WSSUSER_ALL_FAILURE_TRC,
				"nmhTestv2FsWssUserLoggedCount: Invalid value\r\n");
		*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
		return SNMP_FAILURE;
	}

	return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsWssUserTraceOption
 Input       :  The Indices

                The Object 
                testValFsWssUserTraceOption
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1 nmhTestv2FsWssUserTraceOption(UINT4 *pu4ErrorCode , INT4 i4TestValFsWssUserTraceOption)
{
	if (i4TestValFsWssUserTraceOption <  0)
	{
		*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
		return SNMP_FAILURE;
	}

	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FsWssUserRoleTrapStatus
 Input       :  The Indices

                The Object 
                testValFsWssUserRoleTrapStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsWssUserRoleTrapStatus(UINT4 *pu4ErrorCode , INT4 i4TestValFsWssUserRoleTrapStatus)
{
    if ((i4TestValFsWssUserRoleTrapStatus != WSSUSER_TRAP_ENABLE ) &&
        (i4TestValFsWssUserRoleTrapStatus != WSSUSER_TRAP_DISABLE ))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_WSSUSER_INVALID_TRAP_STATUS);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

  
}
/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsWssUserRoleStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1 nmhDepv2FsWssUserRoleStatus(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsWssUserBlockedCount
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1 nmhDepv2FsWssUserBlockedCount(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
    UNUSED_PARAM(pu4ErrorCode);
    UNUSED_PARAM(pSnmpIndexList);
    UNUSED_PARAM(pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsWssUserLoggedCount
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1 nmhDepv2FsWssUserLoggedCount(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
    UNUSED_PARAM(pu4ErrorCode);
    UNUSED_PARAM(pSnmpIndexList);
    UNUSED_PARAM(pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsWssUserTraceOption
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1 nmhDepv2FsWssUserTraceOption(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhDepv2FsWssUserRoleTrapStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FsWssUserRoleTrapStatus(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsWssUserGroupTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsWssUserGroupTable
 Input       :  The Indices
                FsWssUserGroupId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 nmhValidateIndexInstanceFsWssUserGroupTable(UINT4 u4FsWssUserGroupId)
{
	if (u4FsWssUserGroupId < WSSUSER_MIN_VALUE ||
			u4FsWssUserGroupId > WSSUSER_MAX_VALUE)
	{
		CLI_SET_ERR (CLI_WSSUSER_INVALID_GROUP_ID);
		WSSUSER_TRC_ARG1 (WSSUSER_CONTROL_PATH_TRC | WSSUSER_ALL_FAILURE_TRC,
				"Group Id - Not in Range\n", u4FsWssUserGroupId);
		return SNMP_FAILURE;
	}

	return SNMP_SUCCESS; 
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsWssUserGroupTable
 Input       :  The Indices
                FsWssUserGroupId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 nmhGetFirstIndexFsWssUserGroupTable(UINT4 *pu4FsWssUserGroupId)
{
	tUserGroupEntry    *pUserGroupEntry = NULL;

	pUserGroupEntry = (tUserGroupEntry *)RBTreeGetFirst (gWssUserGlobals.
			                                      WssUserGroupEntryTable);

	if (pUserGroupEntry == NULL)
	{
		CLI_SET_ERR (CLI_WSSUSER_GROUP_NOT_CREATED);
		return SNMP_FAILURE;
	}
	*pu4FsWssUserGroupId = pUserGroupEntry->u4GroupId;

	return SNMP_SUCCESS; 
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsWssUserGroupTable
 Input       :  The Indices
                FsWssUserGroupId
                nextFsWssUserGroupId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */

INT1 nmhGetNextIndexFsWssUserGroupTable(UINT4 u4FsWssUserGroupId ,UINT4 *pu4NextFsWssUserGroupId )
{
	tUserGroupEntry    *pUserGroupEntry = NULL;
	tUserGroupEntry     UserGroupEntry;

	MEMSET (&UserGroupEntry, 0, sizeof(tUserGroupEntry));

	UserGroupEntry.u4GroupId = u4FsWssUserGroupId;

	pUserGroupEntry = (tUserGroupEntry*) RBTreeGetNext(gWssUserGlobals.
			WssUserGroupEntryTable,(tRBElem *)&UserGroupEntry,NULL);
	if (pUserGroupEntry == NULL)
	{
		return SNMP_FAILURE;
	}
	*pu4NextFsWssUserGroupId = pUserGroupEntry->u4GroupId;

	return SNMP_SUCCESS; 
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsWssUserGroupName
 Input       :  The Indices
                FsWssUserGroupId

                The Object 
                retValFsWssUserGroupName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1 nmhGetFsWssUserGroupName(UINT4 u4FsWssUserGroupId , tSNMP_OCTET_STRING_TYPE * pRetValFsWssUserGroupName)
{
	tUserGroupEntry    *pUserGroupEntry = NULL;
	tUserGroupEntry     UserGroupEntry;

	MEMSET (&UserGroupEntry, 0, sizeof (tUserGroupEntry));

	UserGroupEntry.u4GroupId = u4FsWssUserGroupId;

	pUserGroupEntry = (tUserGroupEntry *) RBTreeGet (gWssUserGlobals.
			WssUserGroupEntryTable,(tRBElem *) &UserGroupEntry);

	if (pUserGroupEntry == NULL)
	{
		WSSUSER_TRC_ARG1 (WSSUSER_CONTROL_PATH_TRC | WSSUSER_ALL_FAILURE_TRC,
		"nmhGetFsWssUserGroupName: User Group %d  Not Created\n", u4FsWssUserGroupId);

		CLI_SET_ERR (CLI_WSSUSER_GROUP_NOT_CREATED);
		return SNMP_FAILURE;
	}

	MEMCPY (pRetValFsWssUserGroupName->pu1_OctetList, 
			pUserGroupEntry->au1GroupName,  
			pUserGroupEntry->u4UserGroupLen);

	pRetValFsWssUserGroupName->i4_Length = (INT4)(pUserGroupEntry->u4UserGroupLen);

	return SNMP_SUCCESS; 
}

/****************************************************************************
 Function    :  nmhGetFsWssUserGroupBandWidth
 Input       :  The Indices
                FsWssUserGroupId

                The Object 
                retValFsWssUserGroupBandWidth
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsWssUserGroupBandWidth(UINT4 u4FsWssUserGroupId ,
                        UINT4 *pu4RetValFsWssUserGroupBandWidth)
{
    tUserGroupEntry    *pUserGroupEntry = NULL;
    tUserGroupEntry     UserGroupEntry;

    MEMSET (&UserGroupEntry, 0, sizeof (tUserGroupEntry));

    UserGroupEntry.u4GroupId = u4FsWssUserGroupId;

    pUserGroupEntry = (tUserGroupEntry *) RBTreeGet (gWssUserGlobals.
	    WssUserGroupEntryTable,(tRBElem *) &UserGroupEntry);

    if (pUserGroupEntry == NULL)
    {
	WSSUSER_TRC_ARG1 (WSSUSER_CONTROL_PATH_TRC | WSSUSER_ALL_FAILURE_TRC,
		"nmhGetFsWssUserGroupBandWidth: User Group %d  Not Created\n", u4FsWssUserGroupId);

	CLI_SET_ERR (CLI_WSSUSER_GROUP_NOT_CREATED);
	return SNMP_FAILURE;
    }
    *pu4RetValFsWssUserGroupBandWidth = pUserGroupEntry->u4Bandwidth;

    return SNMP_SUCCESS; 
}

/****************************************************************************
 Function    :  nmhGetFsWssUserGroupDLBandWidth
 Input       :  The Indices
                FsWssUserGroupId

                The Object 
                retValFsWssUserGroupDLBandWidth
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsWssUserGroupDLBandWidth(UINT4 u4FsWssUserGroupId ,
                        UINT4 *pu4RetValFsWssUserGroupDLBandWidth)
{
    tUserGroupEntry    *pUserGroupEntry = NULL;
    tUserGroupEntry     UserGroupEntry;

    MEMSET (&UserGroupEntry, 0, sizeof (tUserGroupEntry));

    UserGroupEntry.u4GroupId = u4FsWssUserGroupId;

    pUserGroupEntry = (tUserGroupEntry *) RBTreeGet (gWssUserGlobals.
	    WssUserGroupEntryTable,(tRBElem *) &UserGroupEntry);

    if (pUserGroupEntry == NULL)
    {
	WSSUSER_TRC_ARG1 (WSSUSER_CONTROL_PATH_TRC | WSSUSER_ALL_FAILURE_TRC,
		"nmhGetFsWssUserGroupBandWidth: User Group %d  Not Created\n", u4FsWssUserGroupId);

	CLI_SET_ERR (CLI_WSSUSER_GROUP_NOT_CREATED);
	return SNMP_FAILURE;
    }
    *pu4RetValFsWssUserGroupDLBandWidth = pUserGroupEntry->u4DLBandwidth;

    return SNMP_SUCCESS; 
}

/****************************************************************************
 Function    :  nmhGetFsWssUserGroupULBandWidth
 Input       :  The Indices
                FsWssUserGroupId

                The Object
                retValFsWssUserGroupULBandWidth
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhGetFsWssUserGroupULBandWidth(UINT4 u4FsWssUserGroupId, 
			UINT4 *pu4RetValFsWssUserGroupULBandWidth)
{
    tUserGroupEntry    *pUserGroupEntry = NULL;
    tUserGroupEntry     UserGroupEntry;

    MEMSET (&UserGroupEntry, 0, sizeof (tUserGroupEntry));

    UserGroupEntry.u4GroupId = u4FsWssUserGroupId;

    pUserGroupEntry = (tUserGroupEntry *) RBTreeGet (gWssUserGlobals.
            WssUserGroupEntryTable,(tRBElem *) &UserGroupEntry);

    if (pUserGroupEntry == NULL)
    {
        WSSUSER_TRC_ARG1 (WSSUSER_CONTROL_PATH_TRC | WSSUSER_ALL_FAILURE_TRC,
                "nmhGetFsWssUserGroupULBandWidth: User Group %d  Not Created\n", u4FsWssUserGroupId);

        CLI_SET_ERR (CLI_WSSUSER_GROUP_NOT_CREATED);
        return SNMP_FAILURE;
    }
    *pu4RetValFsWssUserGroupULBandWidth = pUserGroupEntry->u4ULBandwidth;

    return SNMP_SUCCESS; 
}

/****************************************************************************
 Function    :  nmhGetFsWssUserGroupVolume
 Input       :  The Indices
                FsWssUserGroupId

                The Object 
                retValFsWssUserGroupVolume
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1 nmhGetFsWssUserGroupVolume(UINT4 u4FsWssUserGroupId , UINT4 *pu4RetValFsWssUserGroupVolume)
{
	tUserGroupEntry    *pUserGroupEntry = NULL;
	tUserGroupEntry     UserGroupEntry;

	MEMSET (&UserGroupEntry, 0, sizeof (tUserGroupEntry));

	UserGroupEntry.u4GroupId = u4FsWssUserGroupId;

	pUserGroupEntry = (tUserGroupEntry *) RBTreeGet (gWssUserGlobals.
			WssUserGroupEntryTable,(tRBElem *) &UserGroupEntry);

	if (pUserGroupEntry == NULL)
	{
		WSSUSER_TRC_ARG1 (WSSUSER_CONTROL_PATH_TRC | WSSUSER_ALL_FAILURE_TRC,
		"nmhGetFsWssUserGroupVolume: User Group %d  Not Created\n", u4FsWssUserGroupId);

		CLI_SET_ERR (CLI_WSSUSER_GROUP_NOT_CREATED);
		return SNMP_FAILURE;
	}
	*pu4RetValFsWssUserGroupVolume = pUserGroupEntry->u4Volume;

	return SNMP_SUCCESS; 
}

/****************************************************************************
 Function    :  nmhGetFsWssUserGroupTime
 Input       :  The Indices
                FsWssUserGroupId

                The Object 
                retValFsWssUserGroupTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1 nmhGetFsWssUserGroupTime(UINT4 u4FsWssUserGroupId , UINT4 *pu4RetValFsWssUserGroupTime)
{
	tUserGroupEntry    *pUserGroupEntry = NULL;
	tUserGroupEntry     UserGroupEntry;

	MEMSET (&UserGroupEntry, 0, sizeof (tUserGroupEntry));

	UserGroupEntry.u4GroupId = u4FsWssUserGroupId;

	pUserGroupEntry = (tUserGroupEntry *) RBTreeGet (gWssUserGlobals.
			WssUserGroupEntryTable,(tRBElem *) &UserGroupEntry);

	if (pUserGroupEntry == NULL)
	{
		WSSUSER_TRC_ARG1 (WSSUSER_CONTROL_PATH_TRC | WSSUSER_ALL_FAILURE_TRC,
		"nmhGetFsWssUserGroupTime: User Group %d  Not Created\n", u4FsWssUserGroupId);

		CLI_SET_ERR (CLI_WSSUSER_GROUP_NOT_CREATED);
		return SNMP_FAILURE;
	}
	*pu4RetValFsWssUserGroupTime = pUserGroupEntry->u4Time;

	return SNMP_SUCCESS; 

}

/****************************************************************************
 Function    :  nmhGetFsWssUserGroupRowStatus
 Input       :  The Indices
                FsWssUserGroupId

                The Object 
                retValFsWssUserGroupRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1 nmhGetFsWssUserGroupRowStatus(UINT4 u4FsWssUserGroupId , INT4 *pi4RetValFsWssUserGroupRowStatus)
{
	tUserGroupEntry   *pUserGroupEntry = NULL;
	tUserGroupEntry    UserGroupEntry;

	MEMSET (&UserGroupEntry, 0, sizeof(tUserGroupEntry));

	UserGroupEntry.u4GroupId = u4FsWssUserGroupId;

	pUserGroupEntry = (tUserGroupEntry *) RBTreeGet (gWssUserGlobals.
			WssUserGroupEntryTable,(tRBElem *) &UserGroupEntry);

	if (pUserGroupEntry == NULL)
	{
		WSSUSER_TRC_ARG1 (WSSUSER_CONTROL_PATH_TRC | WSSUSER_ALL_FAILURE_TRC,
				"nmhGetFsWssUserGroupRowStatus: User Group %d  Not Created\n", u4FsWssUserGroupId);

		CLI_SET_ERR (CLI_WSSUSER_GROUP_NOT_CREATED);
		return SNMP_FAILURE;
	}
	*pi4RetValFsWssUserGroupRowStatus = pUserGroupEntry->i4RowStatus;   

	return SNMP_SUCCESS; 
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsWssUserGroupName
 Input       :  The Indices
                FsWssUserGroupId

                The Object 
                setValFsWssUserGroupName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1 nmhSetFsWssUserGroupName(UINT4 u4FsWssUserGroupId , tSNMP_OCTET_STRING_TYPE *pSetValFsWssUserGroupName)
{
	tUserGroupEntry    *pUserGroupEntry = NULL;
	tUserGroupEntry     UserGroupEntry;

	MEMSET (&UserGroupEntry, 0, sizeof (tUserGroupEntry));

	UserGroupEntry.u4GroupId = u4FsWssUserGroupId;

	pUserGroupEntry = (tUserGroupEntry *) RBTreeGet (gWssUserGlobals.
			WssUserGroupEntryTable, (tRBElem *) &UserGroupEntry);

	if (pUserGroupEntry == NULL)
	{
		WSSUSER_TRC_ARG1 (WSSUSER_CONTROL_PATH_TRC | WSSUSER_ALL_FAILURE_TRC,
				"nmhSetFsWssUserGroupName: User Group %d  Not Created\n", u4FsWssUserGroupId);

		CLI_SET_ERR (CLI_WSSUSER_GROUP_NOT_CREATED);
		return SNMP_FAILURE;
	}

	pUserGroupEntry->u4UserGroupLen = (UINT4)(pSetValFsWssUserGroupName->i4_Length);

	MEMSET (pUserGroupEntry->au1GroupName, 0, MAX_GROUP_NAME_LENGTH);

	MEMCPY (pUserGroupEntry->au1GroupName, 
			pSetValFsWssUserGroupName->pu1_OctetList,pUserGroupEntry->u4UserGroupLen);

	return SNMP_SUCCESS; 
}

/****************************************************************************
 Function    :  nmhSetFsWssUserGroupBandWidth
 Input       :  The Indices
                FsWssUserGroupId

                The Object 
                setValFsWssUserGroupBandWidth
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsWssUserGroupBandWidth(UINT4 u4FsWssUserGroupId , 
				UINT4 u4SetValFsWssUserGroupBandWidth)
{
    tUserGroupEntry    *pUserGroupEntry = NULL;
    tUserGroupEntry     UserGroupEntry;

    MEMSET (&UserGroupEntry, 0, sizeof (tUserGroupEntry));

    UserGroupEntry.u4GroupId = u4FsWssUserGroupId;

    pUserGroupEntry = (tUserGroupEntry *) RBTreeGet (gWssUserGlobals.
	    WssUserGroupEntryTable, (tRBElem *) &UserGroupEntry);

    if (pUserGroupEntry == NULL)
    {
	WSSUSER_TRC (WSSUSER_CONTROL_PATH_TRC | WSSUSER_ALL_FAILURE_TRC,
		"Invalid Group BandWidth\n");
	CLI_SET_ERR (CLI_WSSUSER_INVALID_BANDWIDTH);
	return SNMP_FAILURE;
    }

    pUserGroupEntry->u4Bandwidth = u4SetValFsWssUserGroupBandWidth;

    return SNMP_SUCCESS; 
}

/****************************************************************************
 Function    :  nmhSetFsWssUserGroupDLBandWidth
 Input       :  The Indices
                FsWssUserGroupId

                The Object 
                setValFsWssUserGroupDLBandWidth
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsWssUserGroupDLBandWidth(UINT4 u4FsWssUserGroupId , 
				UINT4 u4SetValFsWssUserGroupDLBandWidth)
{
    tUserGroupEntry    *pUserGroupEntry = NULL;
    tUserGroupEntry     UserGroupEntry;

    MEMSET (&UserGroupEntry, 0, sizeof (tUserGroupEntry));

    UserGroupEntry.u4GroupId = u4FsWssUserGroupId;

    pUserGroupEntry = (tUserGroupEntry *) RBTreeGet (gWssUserGlobals.
	    WssUserGroupEntryTable, (tRBElem *) &UserGroupEntry);

    if (pUserGroupEntry == NULL)
    {
	WSSUSER_TRC (WSSUSER_CONTROL_PATH_TRC | WSSUSER_ALL_FAILURE_TRC,
		"Invalid Group BandWidth\n");
	CLI_SET_ERR (CLI_WSSUSER_INVALID_BANDWIDTH);
	return SNMP_FAILURE;
    }

    pUserGroupEntry->u4DLBandwidth = u4SetValFsWssUserGroupDLBandWidth;

    return SNMP_SUCCESS; 
}

/****************************************************************************
 Function    :  nmhSetFsWssUserGroupULBandWidth
 Input       :  The Indices
                FsWssUserGroupId

                The Object
                setValFsWssUserGroupULBandWidth
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsWssUserGroupULBandWidth(UINT4 u4FsWssUserGroupId , 
			UINT4 u4SetValFsWssUserGroupULBandWidth)
{
    tUserGroupEntry    *pUserGroupEntry = NULL;
    tUserGroupEntry     UserGroupEntry;

    MEMSET (&UserGroupEntry, 0, sizeof (tUserGroupEntry));

    UserGroupEntry.u4GroupId = u4FsWssUserGroupId;

    pUserGroupEntry = (tUserGroupEntry *) RBTreeGet (gWssUserGlobals.
	    WssUserGroupEntryTable, (tRBElem *) &UserGroupEntry);

    if (pUserGroupEntry == NULL)
    {
        WSSUSER_TRC (WSSUSER_CONTROL_PATH_TRC | WSSUSER_ALL_FAILURE_TRC,
                "Invalid Group BandWidth\n");
        CLI_SET_ERR (CLI_WSSUSER_INVALID_BANDWIDTH);
        return SNMP_FAILURE;
    }

    pUserGroupEntry->u4ULBandwidth = u4SetValFsWssUserGroupULBandWidth;

    return SNMP_SUCCESS; 
}

/****************************************************************************
 Function    :  nmhSetFsWssUserGroupVolume
 Input       :  The Indices
                FsWssUserGroupId

                The Object 
                setValFsWssUserGroupVolume
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1 nmhSetFsWssUserGroupVolume(UINT4 u4FsWssUserGroupId , UINT4 u4SetValFsWssUserGroupVolume)
{
	tUserGroupEntry    *pUserGroupEntry = NULL;
	tUserGroupEntry     UserGroupEntry;

	MEMSET (&UserGroupEntry, 0, sizeof (tUserGroupEntry));

	UserGroupEntry.u4GroupId = u4FsWssUserGroupId;

	pUserGroupEntry = (tUserGroupEntry *) RBTreeGet (gWssUserGlobals.
			WssUserGroupEntryTable, (tRBElem *) &UserGroupEntry);

	if (pUserGroupEntry == NULL)
	{
		WSSUSER_TRC (WSSUSER_CONTROL_PATH_TRC | WSSUSER_ALL_FAILURE_TRC,
				"Invalid Group Volume\n");
		CLI_SET_ERR (CLI_WSSUSER_INVALID_VOLUME);
		return SNMP_FAILURE;
	}
	pUserGroupEntry->u4Volume = u4SetValFsWssUserGroupVolume;

	return SNMP_SUCCESS; 
}

/****************************************************************************
 Function    :  nmhSetFsWssUserGroupTime
 Input       :  The Indices
                FsWssUserGroupId

                The Object 
                setValFsWssUserGroupTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1 nmhSetFsWssUserGroupTime(UINT4 u4FsWssUserGroupId , UINT4 u4SetValFsWssUserGroupTime)
{
	tUserGroupEntry    *pUserGroupEntry = NULL;
	tUserGroupEntry     UserGroupEntry;

	MEMSET (&UserGroupEntry, 0, sizeof (tUserGroupEntry));

	UserGroupEntry.u4GroupId = u4FsWssUserGroupId;

	pUserGroupEntry = (tUserGroupEntry *) RBTreeGet (gWssUserGlobals.
			WssUserGroupEntryTable, (tRBElem *) &UserGroupEntry);

	if (pUserGroupEntry == NULL)
	{
		WSSUSER_TRC (WSSUSER_CONTROL_PATH_TRC | WSSUSER_ALL_FAILURE_TRC,
				"Invalid Group Time\n");
		CLI_SET_ERR (CLI_WSSUSER_INVALID_TIME);
		return SNMP_FAILURE;
	}

	pUserGroupEntry->u4Time = u4SetValFsWssUserGroupTime;

	return SNMP_SUCCESS; 
}

/****************************************************************************
 Function    :  nmhSetFsWssUserGroupRowStatus
 Input       :  The Indices
                FsWssUserGroupId

                The Object 
                setValFsWssUserGroupRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1 nmhSetFsWssUserGroupRowStatus(UINT4 u4FsWssUserGroupId , INT4 i4SetValFsWssUserGroupRowStatus)
{
	tUserGroupEntry    *pUserGroupEntry = NULL;
	tUserGroupEntry     UserGroupEntry;

	MEMSET (&UserGroupEntry, 0, sizeof (tUserGroupEntry));

	switch (i4SetValFsWssUserGroupRowStatus)
	{
		case CREATE_AND_WAIT:

			pUserGroupEntry = WSSUSER_MEM_ALLOCATE_MEM_BLK (WSSUSER_GROUP_INFO_MEMPOOL_ID);
			if (pUserGroupEntry == NULL)
			{
				WSSUSER_TRC (WSSUSER_CONTROL_PATH_TRC | 
						WSSUSER_ALL_FAILURE_TRC,
						"Memory Allocation failed for User Group");

				CLI_SET_ERR (CLI_WSSUSER_GROUP_MEM_ALLOCATE_FAIL);
				return SNMP_FAILURE;
			}

			/* Initialize all default values */ 
			SPRINTF ((CHR1 *)(pUserGroupEntry->au1GroupName), "%s%d" ,
					WSSUSER_USER_GROUP_NAME, u4FsWssUserGroupId);
			pUserGroupEntry->u4UserGroupLen = STRLEN(pUserGroupEntry->au1GroupName);
			pUserGroupEntry->u4GroupId = u4FsWssUserGroupId;
			pUserGroupEntry->u4Bandwidth = WSSUSER_DEFAULT_BANDWIDTH;
			pUserGroupEntry->u4DLBandwidth = WSSUSER_DEFAULT_BANDWIDTH;
			pUserGroupEntry->u4ULBandwidth = WSSUSER_DEFAULT_BANDWIDTH;
			pUserGroupEntry->u4Volume = WSSUSER_DEFAULT_VOLUME;
			pUserGroupEntry->u4Time = WSSUSER_DEFAULT_TIME;

			/* Add the User Group node to RB Tree */
			if( RBTreeAdd (gWssUserGlobals.WssUserGroupEntryTable, 
						(tRBElem *) pUserGroupEntry) == RB_FAILURE)
			{
				WSSUSER_MEM_RELEASE_MEM_BLK (WSSUSER_GROUP_INFO_MEMPOOL_ID, 
						pUserGroupEntry);
				return SNMP_FAILURE;
			}
			pUserGroupEntry->i4RowStatus = NOT_READY;
			break;

		case NOT_IN_SERVICE:

			UserGroupEntry.u4GroupId = u4FsWssUserGroupId;
			pUserGroupEntry = (tUserGroupEntry *) RBTreeGet (gWssUserGlobals.
					WssUserGroupEntryTable,(tRBElem *) &UserGroupEntry);
			if (pUserGroupEntry == NULL) 
			{
				WSSUSER_TRC_ARG1 (WSSUSER_CONTROL_PATH_TRC | 
						WSSUSER_ALL_FAILURE_TRC,
						"nmhSetFsWssUserGroupRowStatus :User Group %d  Not Created\n", u4FsWssUserGroupId);

				CLI_SET_ERR (CLI_WSSUSER_GROUP_NOT_CREATED);
				return SNMP_FAILURE;
			}
			pUserGroupEntry->i4RowStatus = NOT_IN_SERVICE;
			break;

		case CREATE_AND_GO:

			pUserGroupEntry = WSSUSER_MEM_ALLOCATE_MEM_BLK (WSSUSER_GROUP_INFO_MEMPOOL_ID);
			if (pUserGroupEntry == NULL)
			{
				WSSUSER_TRC (WSSUSER_CONTROL_PATH_TRC | 
						WSSUSER_ALL_FAILURE_TRC,
						"Memory Allocation failed for User Group");

				CLI_SET_ERR (CLI_WSSUSER_GROUP_MEM_ALLOCATE_FAIL);
				return SNMP_FAILURE;
			}

			/* Initialize all default values */ 
			pUserGroupEntry->u4GroupId = u4FsWssUserGroupId;
			SPRINTF ((CHR1 *)(pUserGroupEntry->au1GroupName), "%s%d" ,
					WSSUSER_USER_GROUP_NAME, u4FsWssUserGroupId);
			pUserGroupEntry->u4UserGroupLen = STRLEN(pUserGroupEntry->au1GroupName);
			pUserGroupEntry->u4Bandwidth = WSSUSER_DEFAULT_BANDWIDTH;
			pUserGroupEntry->u4DLBandwidth = WSSUSER_DEFAULT_BANDWIDTH;
			pUserGroupEntry->u4ULBandwidth = WSSUSER_DEFAULT_BANDWIDTH;
			pUserGroupEntry->u4Volume = WSSUSER_DEFAULT_VOLUME;
			pUserGroupEntry->u4Time = WSSUSER_DEFAULT_TIME;

			/* Add the User Group node to RB Tree */
			if( RBTreeAdd (gWssUserGlobals.WssUserGroupEntryTable, 
						(tRBElem *) pUserGroupEntry) == RB_FAILURE)
			{
				WSSUSER_MEM_RELEASE_MEM_BLK (WSSUSER_GROUP_INFO_MEMPOOL_ID, 
						pUserGroupEntry);
				return SNMP_FAILURE;
			}
			pUserGroupEntry->i4RowStatus = ACTIVE;
			break;

		case ACTIVE:

			UserGroupEntry.u4GroupId = u4FsWssUserGroupId;
			pUserGroupEntry = (tUserGroupEntry *) RBTreeGet (gWssUserGlobals.
					WssUserGroupEntryTable,(tRBElem *) &UserGroupEntry);
			if (pUserGroupEntry == NULL) 
			{
				WSSUSER_TRC_ARG1 (WSSUSER_CONTROL_PATH_TRC | 
						WSSUSER_ALL_FAILURE_TRC,
						"nmhSetFsWssUserGroupRowStatus: User Group %d  Not Created\n", u4FsWssUserGroupId);

				CLI_SET_ERR (CLI_WSSUSER_GROUP_NOT_CREATED);
				return SNMP_FAILURE;
			}
			pUserGroupEntry->i4RowStatus = ACTIVE;
			break;

		case DESTROY:

			UserGroupEntry.u4GroupId = u4FsWssUserGroupId;
			pUserGroupEntry = (tUserGroupEntry *) RBTreeGet (gWssUserGlobals.
					WssUserGroupEntryTable,(tRBElem *) &UserGroupEntry);

			if (pUserGroupEntry == NULL) 
			{
				WSSUSER_TRC_ARG1 (WSSUSER_CONTROL_PATH_TRC | 
						WSSUSER_ALL_FAILURE_TRC,
						"nmhSetFsWssUserGroupRowStatus: User Group %d  Not Created\n", u4FsWssUserGroupId);

				CLI_SET_ERR (CLI_WSSUSER_GROUP_NOT_CREATED);
				return SNMP_FAILURE;
			}

			if (RBTreeRemove (gWssUserGlobals.WssUserGroupEntryTable, 
						(tRBElem *)pUserGroupEntry) == RB_FAILURE)
			{
				WSSUSER_TRC_ARG1 (WSSUSER_CONTROL_PATH_TRC | 
						WSSUSER_ALL_FAILURE_TRC,
						"Cannot delete User Group: %d\n", u4FsWssUserGroupId);

				CLI_SET_ERR (CLI_WSSUSER_GROUP_CANNOT_DELETE);
				return SNMP_FAILURE;

			}
			WSSUSER_MEM_RELEASE_MEM_BLK (WSSUSER_GROUP_INFO_MEMPOOL_ID, 
					pUserGroupEntry);
			break;

		default:

			WSSUSER_TRC (WSSUSER_CONTROL_PATH_TRC |
					WSSUSER_ALL_FAILURE_TRC,
					"Invalid Row Status\n");
			return SNMP_FAILURE;
	}
	return SNMP_SUCCESS; 
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsWssUserGroupName
 Input       :  The Indices
                FsWssUserGroupId

                The Object 
                testValFsWssUserGroupName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1 nmhTestv2FsWssUserGroupName(UINT4 *pu4ErrorCode , UINT4 u4FsWssUserGroupId , tSNMP_OCTET_STRING_TYPE *pTestValFsWssUserGroupName)
{
	tUserGroupEntry    *pUserGroupEntry = NULL;
	tUserGroupEntry     UserGroupEntry;
    UINT1               u1Index = 0;

	if (pTestValFsWssUserGroupName->i4_Length > MAX_GROUP_NAME_LENGTH)
	{
		*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
		WSSUSER_TRC (WSSUSER_CONTROL_PATH_TRC | WSSUSER_ALL_FAILURE_TRC,
				"Invalid Group Name\n");
		CLI_SET_ERR (CLI_WSSUSER_GROUPNAME_LEN_EXCEEDS);
		return SNMP_FAILURE;
	}

    /* Verify whether Group Name starts with alphabets. 
     * If not display error message as "Invalid Group Name" */

    if (!ISALPHA (pTestValFsWssUserGroupName->pu1_OctetList[0]))
    {
        CLI_SET_ERR (CLI_WSSUSER_INVALID_GROUP_NAME);
        return SNMP_FAILURE;
    }
    
    /* Group Name is valid if and only if it contains  
     * AlphaNumeric characters, Hiphens ("-") and Underscore ("_") */

    for (u1Index = 1; u1Index < pTestValFsWssUserGroupName->i4_Length; u1Index++)
    {
        if (!isalnum(pTestValFsWssUserGroupName->pu1_OctetList[u1Index]) &&
           pTestValFsWssUserGroupName->pu1_OctetList[u1Index] != WSSUSER_HIPHEN &&
           pTestValFsWssUserGroupName->pu1_OctetList[u1Index] != WSSUSER_UNDERSCORE)

        {
            CLI_SET_ERR (CLI_WSSUSER_INVALID_GROUP_NAME);
            return SNMP_FAILURE;
        }
    }

	MEMSET (&UserGroupEntry, 0, sizeof (tUserGroupEntry));

	UserGroupEntry.u4GroupId = u4FsWssUserGroupId;

	pUserGroupEntry = (tUserGroupEntry *) RBTreeGet (gWssUserGlobals.
			WssUserGroupEntryTable,(tRBElem *) &UserGroupEntry);

	if (pUserGroupEntry == NULL) 
	{
		WSSUSER_TRC_ARG1 (WSSUSER_CONTROL_PATH_TRC | WSSUSER_ALL_FAILURE_TRC,
				"nmhTestv2FsWssUserGroupName: User Group %d  Not Created\n", u4FsWssUserGroupId);

		CLI_SET_ERR (CLI_WSSUSER_GROUP_NOT_CREATED);
		return SNMP_FAILURE;
	}

	/* Allow to edit the Group Name if and only if 
	 * Row Status is NOT_IN_SERVICE*/

	if (pUserGroupEntry->i4RowStatus == ACTIVE)
	{
		*pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
		return SNMP_FAILURE;
	}

	return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsWssUserGroupBandWidth
 Input       :  The Indices
                FsWssUserGroupId

                The Object 
                testValFsWssUserGroupBandWidth
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhTestv2FsWssUserGroupBandWidth(UINT4 *pu4ErrorCode , 
			UINT4 u4FsWssUserGroupId , 
			UINT4 u4TestValFsWssUserGroupBandWidth)
{
    tUserGroupEntry    *pUserGroupEntry = NULL;
    tUserGroupEntry     UserGroupEntry;

    UNUSED_PARAM (u4TestValFsWssUserGroupBandWidth);

    MEMSET (&UserGroupEntry, 0, sizeof (tUserGroupEntry));

    UserGroupEntry.u4GroupId = u4FsWssUserGroupId;

    pUserGroupEntry = (tUserGroupEntry *) RBTreeGet (gWssUserGlobals.
	    WssUserGroupEntryTable,(tRBElem *) &UserGroupEntry);

    if (pUserGroupEntry == NULL) 
    {
	WSSUSER_TRC_ARG1 (WSSUSER_CONTROL_PATH_TRC | WSSUSER_ALL_FAILURE_TRC,
		"nmhTestv2FsWssUserGroupBandWidth: User Group %d  Not Created\n", 
		u4FsWssUserGroupId);

	CLI_SET_ERR (CLI_WSSUSER_GROUP_NOT_CREATED);
	return SNMP_FAILURE;
    }

    /* Allow to edit the Group BandWidth if and only if 
     * Row Status is NOT_IN_SERVICE*/

    if (pUserGroupEntry->i4RowStatus == ACTIVE)
    {
	*pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
	return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}


/****************************************************************************
 Function    :  nmhTestv2FsWssUserGroupDLBandWidth
 Input       :  The Indices
                FsWssUserGroupId

                The Object 
                testValFsWssUserGroupDLBandWidth
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsWssUserGroupDLBandWidth(UINT4 *pu4ErrorCode , 
			UINT4 u4FsWssUserGroupId , 
			UINT4 u4TestValFsWssUserGroupDLBandWidth)
{
    tUserGroupEntry    *pUserGroupEntry = NULL;
    tUserGroupEntry     UserGroupEntry;

    UNUSED_PARAM (u4TestValFsWssUserGroupDLBandWidth);

    MEMSET (&UserGroupEntry, 0, sizeof (tUserGroupEntry));

    UserGroupEntry.u4GroupId = u4FsWssUserGroupId;

    pUserGroupEntry = (tUserGroupEntry *) RBTreeGet (gWssUserGlobals.
	    WssUserGroupEntryTable,(tRBElem *) &UserGroupEntry);

    if (pUserGroupEntry == NULL) 
    {
	WSSUSER_TRC_ARG1 (WSSUSER_CONTROL_PATH_TRC | WSSUSER_ALL_FAILURE_TRC,
		"nmhTestv2FsWssUserGroupDLBandWidth: User Group %d  Not Created\n", 
		u4FsWssUserGroupId);

	CLI_SET_ERR (CLI_WSSUSER_GROUP_NOT_CREATED);
	return SNMP_FAILURE;
    }

    /* Allow to edit the Group BandWidth if and only if 
     * Row Status is NOT_IN_SERVICE*/

    if (pUserGroupEntry->i4RowStatus == ACTIVE)
    {
	*pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
	return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsWssUserGroupULBandWidth
 Input       :  The Indices
                FsWssUserGroupId

                The Object
                testValFsWssUserGroupULBandWidth
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsWssUserGroupULBandWidth(UINT4 *pu4ErrorCode, 
		UINT4 u4FsWssUserGroupId, 
		UINT4 u4TestValFsWssUserGroupULBandWidth)
{
    tUserGroupEntry    *pUserGroupEntry = NULL;
    tUserGroupEntry     UserGroupEntry;

    UNUSED_PARAM (u4TestValFsWssUserGroupULBandWidth);

    MEMSET (&UserGroupEntry, 0, sizeof (tUserGroupEntry));

    UserGroupEntry.u4GroupId = u4FsWssUserGroupId;

    pUserGroupEntry = (tUserGroupEntry *) RBTreeGet (gWssUserGlobals.
            WssUserGroupEntryTable,(tRBElem *) &UserGroupEntry);

    if (pUserGroupEntry == NULL) 
    {
        WSSUSER_TRC_ARG1 (WSSUSER_CONTROL_PATH_TRC | WSSUSER_ALL_FAILURE_TRC,
                "nmhTestv2FsWssUserGroupULBandWidth: User Group %d  Not Created\n", 
                u4FsWssUserGroupId);

        CLI_SET_ERR (CLI_WSSUSER_GROUP_NOT_CREATED);
        return SNMP_FAILURE;
    }

    /* Allow to edit the Group BandWidth if and only if 
     * Row Status is NOT_IN_SERVICE*/

    if (pUserGroupEntry->i4RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsWssUserGroupVolume
 Input       :  The Indices
                FsWssUserGroupId

                The Object 
                testValFsWssUserGroupVolume
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1 nmhTestv2FsWssUserGroupVolume(UINT4 *pu4ErrorCode , UINT4 u4FsWssUserGroupId , UINT4 u4TestValFsWssUserGroupVolume)
{
	tUserGroupEntry    *pUserGroupEntry = NULL;
	tUserGroupEntry     UserGroupEntry;

	UNUSED_PARAM (u4TestValFsWssUserGroupVolume);

	MEMSET (&UserGroupEntry, 0, sizeof (tUserGroupEntry));

	UserGroupEntry.u4GroupId = u4FsWssUserGroupId;

	pUserGroupEntry = (tUserGroupEntry *) RBTreeGet (gWssUserGlobals.
			WssUserGroupEntryTable,(tRBElem *) &UserGroupEntry);

	if (pUserGroupEntry == NULL) 
	{
		WSSUSER_TRC_ARG1 (WSSUSER_CONTROL_PATH_TRC | WSSUSER_ALL_FAILURE_TRC,
		"nmhTestv2FsWssUserGroupVolume: User Group %d  Not Created\n", u4FsWssUserGroupId);

		CLI_SET_ERR (CLI_WSSUSER_GROUP_NOT_CREATED);
		return SNMP_FAILURE;
	}

	/* Allow to edit the Group Volume if and only if 
	 * Row Status is NOT_IN_SERVICE*/

	if (pUserGroupEntry->i4RowStatus == ACTIVE)
	{
		*pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
		return SNMP_FAILURE;
	}

	return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsWssUserGroupTime
 Input       :  The Indices
                FsWssUserGroupId

                The Object 
                testValFsWssUserGroupTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1 nmhTestv2FsWssUserGroupTime(UINT4 *pu4ErrorCode , UINT4 u4FsWssUserGroupId , UINT4 u4TestValFsWssUserGroupTime)
{
	tUserGroupEntry    *pUserGroupEntry = NULL;
	tUserGroupEntry     UserGroupEntry;

	if (u4TestValFsWssUserGroupTime > WSSUSER_MAX_TIME_VALUE)
	{
		*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
		WSSUSER_TRC (WSSUSER_CONTROL_PATH_TRC | WSSUSER_ALL_FAILURE_TRC,
				"Invalid Group Time\n");
		CLI_SET_ERR (CLI_WSSUSER_INVALID_TIME);
		return SNMP_FAILURE;

	}

	MEMSET (&UserGroupEntry, 0, sizeof (tUserGroupEntry));

	UserGroupEntry.u4GroupId = u4FsWssUserGroupId;

	pUserGroupEntry = (tUserGroupEntry *) RBTreeGet (gWssUserGlobals.
			WssUserGroupEntryTable,(tRBElem *) &UserGroupEntry);

	if (pUserGroupEntry == NULL) 
	{
		WSSUSER_TRC_ARG1 (WSSUSER_CONTROL_PATH_TRC | WSSUSER_ALL_FAILURE_TRC,
				"nmhTestv2FsWssUserGroupTime: User Group %d  Not Created\n", 
				u4FsWssUserGroupId);

		CLI_SET_ERR (CLI_WSSUSER_GROUP_NOT_CREATED);
		return SNMP_FAILURE;
	}

	/* Allow to edit the Group Time if and only if 
	 * Row Status is NOT_IN_SERVICE*/

	if (pUserGroupEntry->i4RowStatus == ACTIVE)
	{
		*pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
		return SNMP_FAILURE;
	}

	return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsWssUserGroupRowStatus
 Input       :  The Indices
                FsWssUserGroupId

                The Object 
                testValFsWssUserGroupRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1 nmhTestv2FsWssUserGroupRowStatus(UINT4 *pu4ErrorCode , UINT4 u4FsWssUserGroupId , INT4 i4TestValFsWssUserGroupRowStatus)
{
	tSNMP_OCTET_STRING_TYPE    NextUserName;
	tSNMP_OCTET_STRING_TYPE    UserName;
	tUserGroupEntry           *pUserGroupEntry = NULL;
	tUserGroupEntry            UserGroupEntry;
	UINT4                      u4RoleGroupId = 0;
	UINT4                      u4NextWlanId = 0;
	UINT4                      u4WlanId = 0;
	UINT4                      u4Count = 0;
	UINT1                      au1NextUserName [MAX_USER_NAME_LENGTH];
	UINT1                      au1UserName [MAX_USER_NAME_LENGTH];
	INT1                       i1RetValue = SNMP_FAILURE;

	if (gWssUserGlobals.i4UserRoleStatus == WSSUSER_MODULE_DISABLED)
	{
		*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
		CLI_SET_ERR (CLI_WSSUSER_NOT_ENABLED);
		return SNMP_FAILURE;
	}

	if (u4FsWssUserGroupId < WSSUSER_MIN_VALUE ||
			u4FsWssUserGroupId > WSSUSER_MAX_VALUE)
	{
		*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
		CLI_SET_ERR (CLI_WSSUSER_INVALID_GROUP_ID);
		WSSUSER_TRC_ARG1 (WSSUSER_CONTROL_PATH_TRC | WSSUSER_ALL_FAILURE_TRC,
				"Group Id - Not in Range\n", u4FsWssUserGroupId);
		return SNMP_FAILURE;
	}

	MEMSET (&NextUserName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
	MEMSET (&UserGroupEntry, 0, sizeof (tUserGroupEntry));
	MEMSET (&au1NextUserName, 0, MAX_USER_NAME_LENGTH);
	MEMSET (&au1UserName, 0, MAX_USER_NAME_LENGTH);

	NextUserName.pu1_OctetList = (UINT1 *) au1NextUserName;
	NextUserName.i4_Length = MAX_USER_NAME_LENGTH;

	UserGroupEntry.u4GroupId = u4FsWssUserGroupId;

	pUserGroupEntry = (tUserGroupEntry *) RBTreeGet (gWssUserGlobals.
			WssUserGroupEntryTable,(tRBElem *) &UserGroupEntry);

	if ((pUserGroupEntry == NULL) && 
			(i4TestValFsWssUserGroupRowStatus != CREATE_AND_WAIT) &&
			(i4TestValFsWssUserGroupRowStatus != CREATE_AND_GO))
	{
		*pu4ErrorCode = SNMP_ERR_WRONG_VALUE; 
		WSSUSER_TRC_ARG1 (WSSUSER_CONTROL_PATH_TRC | WSSUSER_ALL_FAILURE_TRC,
				"nmhTestv2FsWssUserGroupRowStatus: User Group %d  Not Created\n", u4FsWssUserGroupId);

		CLI_SET_ERR (CLI_WSSUSER_GROUP_NOT_CREATED);
		return SNMP_FAILURE;
	}

	switch (i4TestValFsWssUserGroupRowStatus)
	{

		case CREATE_AND_WAIT:
		case CREATE_AND_GO:

			if (RBTreeCount (gWssUserGlobals.WssUserGroupEntryTable, 
						&u4Count) != RB_SUCCESS)
			{
				return SNMP_FAILURE;   
			}

            /* Verify whether the User Group Entry count exceed the MAX limits */

			if (u4Count == MAX_WSS_USERGROUP_INFO)
			{
				*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
				WSSUSER_TRC_ARG1 (WSSUSER_CONTROL_PATH_TRC | WSSUSER_ALL_FAILURE_TRC,
						"Only %d User Groups can be created", MAX_WSS_USERGROUP_INFO);

				CLI_SET_ERR (CLI_WSSUSER_GROUP_MAX_LIMIT);
				return SNMP_FAILURE;
			}
			break;

		case NOT_READY:
		case ACTIVE:
		case NOT_IN_SERVICE:
			break;

		case DESTROY:

			/* Verify whether User Group Id for deletion is Default User Group.
             * If yes, display errro message as "Cannot delete Default Group" */

			if (u4FsWssUserGroupId == WSSUSER_DEFAULT_GROUP_ID)
			{
				*pu4ErrorCode = SNMP_ERR_WRONG_VALUE; 
				CLI_SET_ERR (CLI_WSSUSER_GROUP_CANNOT_DELETE_DEFAULT);
				WSSUSER_TRC_ARG1 (WSSUSER_CONTROL_PATH_TRC | 
						WSSUSER_ALL_FAILURE_TRC,
						"Cannot delete Default Group  %d\n", 
						u4FsWssUserGroupId);
				return SNMP_FAILURE;
			}

			/* Get all User Role Details */

			i1RetValue = nmhGetFirstIndexFsWssUserRoleTable (&NextUserName, 
					&u4NextWlanId);
			if( i1RetValue == SNMP_FAILURE)
			{
				return CLI_FAILURE;
			}

			/* Don't allow to delete the user-group,
			 * if it contains atleast one user */

			do
			{
				u4WlanId = u4NextWlanId;

				MEMSET (&UserName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

				UserName.pu1_OctetList = (UINT1 *) au1UserName;
				UserName.i4_Length = MAX_USER_NAME_LENGTH;

				MEMCPY (UserName.pu1_OctetList, NextUserName.pu1_OctetList,
						NextUserName.i4_Length);
				UserName.i4_Length = NextUserName.i4_Length;

				u4RoleGroupId = 0;

				if(nmhGetFsWssUserRoleGroupId (&UserName, 
							u4WlanId, &u4RoleGroupId)!= SNMP_SUCCESS)
				{
					return SNMP_FAILURE;
				}

				/* Cannot delete User-Group if atleast one user is associated
				 * with that group */
				if ( u4RoleGroupId == u4FsWssUserGroupId )
				{
					*pu4ErrorCode = SNMP_ERR_WRONG_VALUE; 
					CLI_SET_ERR (CLI_WSSUSER_USER_PRESENT);
					WSSUSER_TRC_ARG1 (WSSUSER_CONTROL_PATH_TRC | 
							WSSUSER_ALL_FAILURE_TRC,
							"Cannot delete, User is associated with  group %d\n", u4FsWssUserGroupId);
					return SNMP_FAILURE;
				}

			}
			while (nmhGetNextIndexFsWssUserRoleTable (&UserName, &NextUserName,
						u4WlanId, &u4NextWlanId) == SNMP_SUCCESS);

			break;

		default:
			*pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
			WSSUSER_TRC (WSSUSER_CONTROL_PATH_TRC |
					WSSUSER_ALL_FAILURE_TRC,
					"Invalid Row Status\n");

			return SNMP_FAILURE;    
	}
	return SNMP_SUCCESS;
}
/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsWssUserGroupTable
 Input       :  The Indices
                FsWssUserGroupId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FsWssUserGroupTable(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
    UNUSED_PARAM(pu4ErrorCode);
    UNUSED_PARAM(pSnmpIndexList);
    UNUSED_PARAM(pSnmpVarBind);
    return SNMP_SUCCESS;
}
/* LOW LEVEL Routines for Table : FsWssUserRoleTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsWssUserRoleTable
 Input       :  The Indices
                FsWssUserRoleName
                FsWssUserRoleWlanIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 nmhValidateIndexInstanceFsWssUserRoleTable(tSNMP_OCTET_STRING_TYPE *pFsWssUserRoleName , UINT4 u4FsWssUserRoleWlanIndex)
{
    UINT1    u1Index = 0;

	if ((u4FsWssUserRoleWlanIndex < CFA_MIN_WSS_IF_INDEX) ||
			(u4FsWssUserRoleWlanIndex > CFA_MAX_WSS_IF_INDEX) ||
			(pFsWssUserRoleName->i4_Length > MAX_USER_NAME_LENGTH))
	{
		CLI_SET_ERR (CLI_WSSUSER_INVALID_ROLE_INDEX);
		WSSUSER_TRC (WSSUSER_CONTROL_PATH_TRC | WSSUSER_ALL_FAILURE_TRC,
				"nmhValidateIndexInstanceFsWssUserRoleTable: Invalid User Name and Wlan Index\n");

		return SNMP_FAILURE;
	}

   /* Verify whether User Name starts with alphabets.
    * If not display error message as "Invalid User Name" */

    if (!ISALPHA (pFsWssUserRoleName->pu1_OctetList[0]))
    {
        CLI_SET_ERR (CLI_WSSUSER_INVALID_USER_NAME);
        return SNMP_FAILURE;
    }

    /* User Name is valid if and only if it contains 
     * AlphaNumeric characters, Hiphens ("-"), Underscore ("_") */    

    for (u1Index = 1; u1Index < pFsWssUserRoleName->i4_Length; u1Index++)
    {
        if(!isalnum(pFsWssUserRoleName->pu1_OctetList[u1Index]) &&
            pFsWssUserRoleName->pu1_OctetList[u1Index] != WSSUSER_HIPHEN &&
            pFsWssUserRoleName->pu1_OctetList[u1Index] != WSSUSER_UNDERSCORE)
        {
            CLI_SET_ERR (CLI_WSSUSER_INVALID_USER_NAME);
            return SNMP_FAILURE;
        }
    }

	return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsWssUserRoleTable
 Input       :  The Indices
                FsWssUserRoleName
                FsWssUserRoleWlanIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 nmhGetFirstIndexFsWssUserRoleTable(tSNMP_OCTET_STRING_TYPE * pFsWssUserRoleName , UINT4 *pu4FsWssUserRoleWlanIndex)
{
	tUserRoleEntry    *pUserRoleEntry = NULL;

	pUserRoleEntry = (tUserRoleEntry *)RBTreeGetFirst (gWssUserGlobals.WssUserRoleEntryTable);

	if (pUserRoleEntry == NULL)
	{
		CLI_SET_ERR (CLI_WSSUSER_ROLE_NOT_CREATED);
		return SNMP_FAILURE;
	}

	MEMCPY ( pFsWssUserRoleName->pu1_OctetList, pUserRoleEntry->au1UserName, 
			pUserRoleEntry->u4UserNameLen);
	pFsWssUserRoleName->i4_Length = (INT4)(pUserRoleEntry->u4UserNameLen);

	*pu4FsWssUserRoleWlanIndex = pUserRoleEntry->u4WlanIndex;

	return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsWssUserRoleTable
 Input       :  The Indices
                FsWssUserRoleName
                nextFsWssUserRoleName
                FsWssUserRoleWlanIndex
                nextFsWssUserRoleWlanIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */

INT1 nmhGetNextIndexFsWssUserRoleTable(tSNMP_OCTET_STRING_TYPE *pFsWssUserRoleName ,tSNMP_OCTET_STRING_TYPE * pNextFsWssUserRoleName  , UINT4 u4FsWssUserRoleWlanIndex ,UINT4 *pu4NextFsWssUserRoleWlanIndex )
{
	tUserRoleEntry    *pUserRoleEntry = NULL;
	tUserRoleEntry     UserRoleEntry;  

	MEMSET (&UserRoleEntry, 0, sizeof(tUserRoleEntry));

	MEMCPY (UserRoleEntry.au1UserName, pFsWssUserRoleName->pu1_OctetList,
			            pFsWssUserRoleName->i4_Length);

	UserRoleEntry.u4UserNameLen = (UINT4)(pFsWssUserRoleName->i4_Length);

	UserRoleEntry.u4WlanIndex = u4FsWssUserRoleWlanIndex;

	pUserRoleEntry = (tUserRoleEntry*) RBTreeGetNext(gWssUserGlobals.WssUserRoleEntryTable,
			(tRBElem *)&UserRoleEntry,NULL);

	if (pUserRoleEntry == NULL)
	{
		return SNMP_FAILURE;
	}

	*pu4NextFsWssUserRoleWlanIndex = pUserRoleEntry->u4WlanIndex;

	MEMCPY (pNextFsWssUserRoleName->pu1_OctetList, pUserRoleEntry->au1UserName,
			pUserRoleEntry->u4UserNameLen);

	pNextFsWssUserRoleName->i4_Length = (INT4)(pUserRoleEntry->u4UserNameLen);

	return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsWssUserRoleGroupId
 Input       :  The Indices
                FsWssUserRoleName
                FsWssUserRoleWlanIndex

                The Object 
                retValFsWssUserRoleGroupId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1 nmhGetFsWssUserRoleGroupId(tSNMP_OCTET_STRING_TYPE *pFsWssUserRoleName , UINT4 u4FsWssUserRoleWlanIndex , UINT4 *pu4RetValFsWssUserRoleGroupId)
{
	tUserRoleEntry    *pUserRoleEntry = NULL;
	tUserRoleEntry     UserRoleEntry;

	MEMSET (&UserRoleEntry, 0, sizeof (tUserRoleEntry));

	MEMCPY (UserRoleEntry.au1UserName , pFsWssUserRoleName->pu1_OctetList, 
			             pFsWssUserRoleName->i4_Length);

	UserRoleEntry.u4UserNameLen = (UINT4)(pFsWssUserRoleName->i4_Length);

	UserRoleEntry.u4WlanIndex = u4FsWssUserRoleWlanIndex;

	pUserRoleEntry = (tUserRoleEntry *) RBTreeGet (gWssUserGlobals. 
			WssUserRoleEntryTable,(tRBElem *) &UserRoleEntry);

	if (pUserRoleEntry == NULL)
	{
		WSSUSER_TRC (WSSUSER_CONTROL_PATH_TRC | WSSUSER_ALL_FAILURE_TRC,
				"nmhGetFsWssUserRoleGroupId: User Role is not present\n");
		CLI_SET_ERR (CLI_WSSUSER_ROLE_NOT_CREATED);
		return SNMP_FAILURE;
	}
	*pu4RetValFsWssUserRoleGroupId = pUserRoleEntry->u4GroupId;

	return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsWssUserRoleRowStatus
 Input       :  The Indices
                FsWssUserRoleName
                FsWssUserRoleWlanIndex

                The Object 
                retValFsWssUserRoleRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1 nmhGetFsWssUserRoleRowStatus(tSNMP_OCTET_STRING_TYPE *pFsWssUserRoleName , UINT4 u4FsWssUserRoleWlanIndex , INT4 *pi4RetValFsWssUserRoleRowStatus)
{
	tUserRoleEntry    *pUserRoleEntry = NULL;
	tUserRoleEntry     UserRoleEntry;

	MEMSET (&UserRoleEntry, 0, sizeof(tUserRoleEntry));

	MEMCPY (UserRoleEntry.au1UserName , pFsWssUserRoleName->pu1_OctetList, 
			           pFsWssUserRoleName->i4_Length);

	UserRoleEntry.u4UserNameLen = (UINT4)(pFsWssUserRoleName->i4_Length);

	UserRoleEntry.u4WlanIndex = u4FsWssUserRoleWlanIndex;

	pUserRoleEntry = (tUserRoleEntry *) RBTreeGet (gWssUserGlobals.WssUserRoleEntryTable,
			(tRBElem *) &UserRoleEntry);

	if (pUserRoleEntry == NULL)
	{
		return SNMP_FAILURE;
	}
	*pi4RetValFsWssUserRoleRowStatus = pUserRoleEntry->i4RowStatus; 

	return SNMP_SUCCESS; 
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsWssUserRoleGroupId
 Input       :  The Indices
                FsWssUserRoleName
                FsWssUserRoleWlanIndex

                The Object 
                setValFsWssUserRoleGroupId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1 nmhSetFsWssUserRoleGroupId(tSNMP_OCTET_STRING_TYPE *pFsWssUserRoleName , UINT4 u4FsWssUserRoleWlanIndex , UINT4 u4SetValFsWssUserRoleGroupId)
{
	tUserRoleEntry    *pUserRoleEntry = NULL;
	tUserRoleEntry     UserRoleEntry;
    UINT4              u4TerminateCause = 0;

	MEMSET (&UserRoleEntry, 0, sizeof (tUserRoleEntry));

	MEMCPY (UserRoleEntry.au1UserName , pFsWssUserRoleName->pu1_OctetList, 
			        pFsWssUserRoleName->i4_Length);

	UserRoleEntry.u4UserNameLen = (UINT4)(pFsWssUserRoleName->i4_Length);

	UserRoleEntry.u4WlanIndex = u4FsWssUserRoleWlanIndex;

	pUserRoleEntry = (tUserRoleEntry *) RBTreeGet (gWssUserGlobals.WssUserRoleEntryTable,
			(tRBElem *) &UserRoleEntry);

	if (pUserRoleEntry == NULL)
	{
		WSSUSER_TRC (WSSUSER_CONTROL_PATH_TRC | WSSUSER_ALL_FAILURE_TRC,
				"nmhSetFsWssUserRoleGroupId: User Role is not present\n");
		CLI_SET_ERR (CLI_WSSUSER_ROLE_NOT_CREATED);
		return SNMP_FAILURE;
	}

    if ((pUserRoleEntry->u4GroupId != u4SetValFsWssUserRoleGroupId) &&
         (pUserRoleEntry->u4GroupId != 0))
    {
       /* Remapping the user to different group
        * which needs deletion of ongoing session
        */
        u4TerminateCause = ADMIN_RESET;
        WssUserDeAuthenticateUser (pUserRoleEntry->au1UserName, u4FsWssUserRoleWlanIndex,u4TerminateCause);
    }

    if (u4SetValFsWssUserRoleGroupId == 0)
    {
        u4SetValFsWssUserRoleGroupId = WSSUSER_DEFAULT_GROUP_ID;
    }

	pUserRoleEntry->u4GroupId = u4SetValFsWssUserRoleGroupId;

	return SNMP_SUCCESS; 
}

/****************************************************************************

 Function    :  nmhSetFsWssUserRoleRowStatus
 Input       :  The Indices
                FsWssUserRoleName
                FsWssUserRoleWlanIndex

                The Object 
                setValFsWssUserRoleRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1 nmhSetFsWssUserRoleRowStatus(tSNMP_OCTET_STRING_TYPE *pFsWssUserRoleName , UINT4 u4FsWssUserRoleWlanIndex , INT4 i4SetValFsWssUserRoleRowStatus)
{
	tUserRoleEntry    *pUserRoleEntry = NULL;
	tUserRoleEntry     UserRoleEntry;
    UINT4              u4TerminateCause = 0;

	MEMSET (&UserRoleEntry, 0, sizeof (tUserRoleEntry));

	switch (i4SetValFsWssUserRoleRowStatus)
	{
		case CREATE_AND_WAIT:

			pUserRoleEntry = WSSUSER_MEM_ALLOCATE_MEM_BLK (WSSUSER_ROLE_INFO_MEMPOOL_ID);

			if (pUserRoleEntry == NULL)
			{
				return SNMP_FAILURE;
			}
			pUserRoleEntry->u4UserNameLen = (UINT4)(pFsWssUserRoleName->i4_Length);
			pUserRoleEntry->u4WlanIndex = u4FsWssUserRoleWlanIndex;
			MEMCPY (pUserRoleEntry->au1UserName, 
					pFsWssUserRoleName->pu1_OctetList, pUserRoleEntry->u4UserNameLen);

			pUserRoleEntry->u4GroupId = WSSUSER_DEFAULT_GROUP_ID;

			/* Add the User Role node to RB Tree */
			if( RBTreeAdd (gWssUserGlobals.WssUserRoleEntryTable, 
						(tRBElem *) pUserRoleEntry) == RB_FAILURE)
			{
				WSSUSER_MEM_RELEASE_MEM_BLK (WSSUSER_ROLE_INFO_MEMPOOL_ID, 
						pUserRoleEntry);
				return SNMP_FAILURE;
			}
			pUserRoleEntry->i4RowStatus = NOT_READY;
			break;

		case NOT_IN_SERVICE:

			UserRoleEntry.u4UserNameLen = (UINT4)(pFsWssUserRoleName->i4_Length);
			MEMCPY (UserRoleEntry.au1UserName, 
					pFsWssUserRoleName->pu1_OctetList, UserRoleEntry.u4UserNameLen);

			UserRoleEntry.u4WlanIndex = u4FsWssUserRoleWlanIndex;

			pUserRoleEntry = (tUserRoleEntry *) RBTreeGet (gWssUserGlobals.
					WssUserRoleEntryTable,(tRBElem *) &UserRoleEntry);
			if (pUserRoleEntry == NULL) 
			{
				WSSUSER_TRC (WSSUSER_CONTROL_PATH_TRC | 
						WSSUSER_ALL_FAILURE_TRC,
						"nmhSetFsWssUserRoleGroupId: User Role is not present");

				CLI_SET_ERR (CLI_WSSUSER_ROLE_NOT_CREATED);
				return SNMP_FAILURE;
			}
			pUserRoleEntry->i4RowStatus = NOT_IN_SERVICE;
			break;

		case CREATE_AND_GO:

			pUserRoleEntry = WSSUSER_MEM_ALLOCATE_MEM_BLK (WSSUSER_ROLE_INFO_MEMPOOL_ID);
			if (pUserRoleEntry == NULL)
			{
				return SNMP_FAILURE;
			}

			pUserRoleEntry->u4WlanIndex = u4FsWssUserRoleWlanIndex;
			pUserRoleEntry->u4UserNameLen = (UINT4)(pFsWssUserRoleName->i4_Length);
			MEMCPY (pUserRoleEntry->au1UserName, pFsWssUserRoleName->pu1_OctetList,
					pUserRoleEntry->u4UserNameLen);

			pUserRoleEntry->u4GroupId = WSSUSER_DEFAULT_GROUP_ID;

			/* Add the User Role node to RB Tree */
			if( RBTreeAdd (gWssUserGlobals.WssUserRoleEntryTable, 
						(tRBElem *) pUserRoleEntry) == RB_FAILURE)
			{
				WSSUSER_MEM_RELEASE_MEM_BLK (WSSUSER_ROLE_INFO_MEMPOOL_ID, 
						pUserRoleEntry);
				return SNMP_FAILURE;
			}
			pUserRoleEntry->i4RowStatus = ACTIVE;
			break;

		case ACTIVE:

			UserRoleEntry.u4UserNameLen = (UINT4)(pFsWssUserRoleName->i4_Length);
			MEMCPY (UserRoleEntry.au1UserName, 
					pFsWssUserRoleName->pu1_OctetList, UserRoleEntry.u4UserNameLen);

			UserRoleEntry.u4WlanIndex = u4FsWssUserRoleWlanIndex;

			pUserRoleEntry = (tUserRoleEntry *) RBTreeGet (gWssUserGlobals.
					WssUserRoleEntryTable,(tRBElem *) &UserRoleEntry);

			if (pUserRoleEntry == NULL) 
			{
				WSSUSER_TRC (WSSUSER_CONTROL_PATH_TRC | 
						WSSUSER_ALL_FAILURE_TRC,
						"nmhSetFsWssUserRoleGroupId: User Role is not present");

				CLI_SET_ERR (CLI_WSSUSER_ROLE_NOT_CREATED);
				return SNMP_FAILURE;
			}

			pUserRoleEntry->i4RowStatus = ACTIVE;
			break;

		case DESTROY:

			UserRoleEntry.u4UserNameLen = (UINT4)(pFsWssUserRoleName->i4_Length);
			MEMCPY (UserRoleEntry.au1UserName, 
					pFsWssUserRoleName->pu1_OctetList, UserRoleEntry.u4UserNameLen);

			UserRoleEntry.u4WlanIndex = u4FsWssUserRoleWlanIndex;

			pUserRoleEntry = (tUserRoleEntry *) RBTreeGet (gWssUserGlobals.
					WssUserRoleEntryTable, (tRBElem *) &UserRoleEntry);

			if (pUserRoleEntry == NULL) 
			{
				WSSUSER_TRC (WSSUSER_CONTROL_PATH_TRC | 
						WSSUSER_ALL_FAILURE_TRC,
						"nmhSetFsWssUserRoleGroupId: User Role is not present");

				CLI_SET_ERR (CLI_WSSUSER_ROLE_NOT_CREATED);
				return SNMP_FAILURE;
			}
            u4TerminateCause = ADMIN_RESET;

            WssUserDeAuthenticateUser (pUserRoleEntry->au1UserName,u4FsWssUserRoleWlanIndex,u4TerminateCause);

			if (RBTreeRemove (gWssUserGlobals.WssUserRoleEntryTable, 
						(tRBElem *)pUserRoleEntry) == RB_FAILURE)
			{
				WSSUSER_TRC (WSSUSER_CONTROL_PATH_TRC | 
						WSSUSER_ALL_FAILURE_TRC,
						"Cannot delete User Role\n");

				CLI_SET_ERR (CLI_WSSUSER_ROLE_CANNOT_DELETE);
				return SNMP_FAILURE;

			}

			WSSUSER_MEM_RELEASE_MEM_BLK (WSSUSER_ROLE_INFO_MEMPOOL_ID, 
					pUserRoleEntry);
			break;

		default:

			WSSUSER_TRC (WSSUSER_CONTROL_PATH_TRC |
					WSSUSER_ALL_FAILURE_TRC,
					"Invalid Row Status\n");
			return SNMP_FAILURE;

	}
	return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsWssUserRoleGroupId
 Input       :  The Indices
                FsWssUserRoleName
                FsWssUserRoleWlanIndex

                The Object 
                testValFsWssUserRoleGroupId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1 nmhTestv2FsWssUserRoleGroupId(UINT4 *pu4ErrorCode , tSNMP_OCTET_STRING_TYPE *pFsWssUserRoleName , UINT4 u4FsWssUserRoleWlanIndex , UINT4 u4TestValFsWssUserRoleGroupId)
{
	tUserGroupEntry    *pUserGroupEntry = NULL;
	tUserRoleEntry     *pUserRoleEntry = NULL;
	tUserGroupEntry     UserGroupEntry;
	tUserRoleEntry      UserRoleEntry;
    UINT1               u1Index = 0;

	if ((u4FsWssUserRoleWlanIndex < CFA_MIN_WSS_IF_INDEX) ||
			(u4FsWssUserRoleWlanIndex > CFA_MAX_WSS_IF_INDEX) ||
			(pFsWssUserRoleName->i4_Length > MAX_USER_NAME_LENGTH))
	{
		*pu4ErrorCode = SNMP_ERR_WRONG_VALUE; 
		CLI_SET_ERR (CLI_WSSUSER_INVALID_ROLE_INDEX);
		WSSUSER_TRC (WSSUSER_CONTROL_PATH_TRC | WSSUSER_ALL_FAILURE_TRC,
				"nmhTestv2FsWssUserRoleGroupId: Invalid User Name and Wlan Index\n");

		return SNMP_FAILURE;
	}

    /* Verify whether User Name starts with alphabets.
     * If not display error message as "Invalid User Name" */

    if (!ISALPHA (pFsWssUserRoleName->pu1_OctetList[0]))
    {
        CLI_SET_ERR (CLI_WSSUSER_INVALID_USER_NAME);
        return SNMP_FAILURE;
    }

    /* User Name is valid if and only if it contains 
     * AlphaNumeric characters, Hiphens ("-"), Underscore ("_") */
    
    for (u1Index = 1; u1Index < pFsWssUserRoleName->i4_Length; u1Index++)
    {
        if(!isalnum(pFsWssUserRoleName->pu1_OctetList[u1Index]) &&
            pFsWssUserRoleName->pu1_OctetList[u1Index] != WSSUSER_HIPHEN &&
            pFsWssUserRoleName->pu1_OctetList[u1Index] != WSSUSER_UNDERSCORE)
        {
            CLI_SET_ERR (CLI_WSSUSER_INVALID_USER_NAME);
            return SNMP_FAILURE;
        }
    }

	MEMSET (&UserGroupEntry, 0, sizeof (tUserGroupEntry));
	MEMSET (&UserRoleEntry, 0, sizeof (tUserRoleEntry));

    if (u4TestValFsWssUserRoleGroupId == 0)
    {
        u4TestValFsWssUserRoleGroupId = WSSUSER_DEFAULT_GROUP_ID;
    }
    
	UserGroupEntry.u4GroupId = u4TestValFsWssUserRoleGroupId;

    pUserGroupEntry = (tUserGroupEntry *) RBTreeGet (gWssUserGlobals.WssUserGroupEntryTable,
            (tRBElem *) &UserGroupEntry);

    if (pUserGroupEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        WSSUSER_TRC_ARG1 (WSSUSER_CONTROL_PATH_TRC | WSSUSER_ALL_FAILURE_TRC,
                "nmhTestv2FsWssUserRoleGroupId: User Group %d  Not Created\n", u4TestValFsWssUserRoleGroupId);

        CLI_SET_ERR (CLI_WSSUSER_GROUP_NOT_CREATED);
        return SNMP_FAILURE;
    }
	UserRoleEntry.u4UserNameLen = (UINT4)(pFsWssUserRoleName->i4_Length);

	MEMCPY (UserRoleEntry.au1UserName, pFsWssUserRoleName->pu1_OctetList,
			UserRoleEntry.u4UserNameLen);

	UserRoleEntry.u4WlanIndex = u4FsWssUserRoleWlanIndex;

	pUserRoleEntry = (tUserRoleEntry *) RBTreeGet (gWssUserGlobals.WssUserRoleEntryTable,
			(tRBElem *) &UserRoleEntry);

	if (pUserRoleEntry == NULL) 
	{
		WSSUSER_TRC (WSSUSER_CONTROL_PATH_TRC | WSSUSER_ALL_FAILURE_TRC,
				"nmhTestv2FsWssUserRoleGroupId: User Role is not present");

		CLI_SET_ERR (CLI_WSSUSER_ROLE_NOT_CREATED);
		return SNMP_FAILURE;
	}

	if(pUserRoleEntry->i4RowStatus ==  ACTIVE)
	{
		*pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
		return SNMP_FAILURE;
	}

	return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsWssUserRoleRowStatus
 Input       :  The Indices
                FsWssUserRoleName
                FsWssUserRoleWlanIndex

                The Object 
                testValFsWssUserRoleRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1 nmhTestv2FsWssUserRoleRowStatus(UINT4 *pu4ErrorCode , tSNMP_OCTET_STRING_TYPE *pFsWssUserRoleName , UINT4 u4FsWssUserRoleWlanIndex , INT4 i4TestValFsWssUserRoleRowStatus)
{
	tUserRoleEntry    *pUserRoleEntry = NULL;
	tUserRoleEntry     UserRoleEntry;
	UINT4              u4Count = 0;
	UINT1              u1Index = 0;

	if (gWssUserGlobals.i4UserRoleStatus == WSSUSER_MODULE_DISABLED)
	{
		*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
		CLI_SET_ERR (CLI_WSSUSER_NOT_ENABLED);
		return SNMP_FAILURE;
	}

	if ((u4FsWssUserRoleWlanIndex < CFA_MIN_WSS_IF_INDEX) ||
			(u4FsWssUserRoleWlanIndex > CFA_MAX_WSS_IF_INDEX) ||
			(pFsWssUserRoleName->i4_Length > MAX_USER_NAME_LENGTH))
	{
		*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
		CLI_SET_ERR (CLI_WSSUSER_INVALID_ROLE_INDEX);
		WSSUSER_TRC (WSSUSER_CONTROL_PATH_TRC | WSSUSER_ALL_FAILURE_TRC,
				"nmhTestv2FsWssUserRoleRowStatus: Invalid User Name and Wlan Index\n");
		return SNMP_FAILURE;

	}

    /* Verify whether User Name starts with alphabets.
     * If not display error message as "Invalid User Name" */

    if (!ISALPHA (pFsWssUserRoleName->pu1_OctetList[0]))
    {
        CLI_SET_ERR (CLI_WSSUSER_INVALID_USER_NAME);
        return SNMP_FAILURE;
    }

    /* User Name is valid if and only if it contains 
     * AlphaNumeric characters, Hiphens ("-"), Underscore ("_") */
    
    for (u1Index = 1; u1Index < pFsWssUserRoleName->i4_Length; u1Index++)
    {
        if(!isalnum(pFsWssUserRoleName->pu1_OctetList[u1Index]) &&
            pFsWssUserRoleName->pu1_OctetList[u1Index] != WSSUSER_HIPHEN &&
            pFsWssUserRoleName->pu1_OctetList[u1Index] != WSSUSER_UNDERSCORE)
        {
            CLI_SET_ERR (CLI_WSSUSER_INVALID_USER_NAME);
            return SNMP_FAILURE;
        }
    }

	MEMSET (&UserRoleEntry, 0, sizeof (tUserRoleEntry));

	MEMCPY (UserRoleEntry.au1UserName , pFsWssUserRoleName->pu1_OctetList, 
			        pFsWssUserRoleName->i4_Length);

	UserRoleEntry.u4UserNameLen = (UINT4)(pFsWssUserRoleName->i4_Length);

	UserRoleEntry.u4WlanIndex = u4FsWssUserRoleWlanIndex;

	pUserRoleEntry = (tUserRoleEntry *) RBTreeGet (gWssUserGlobals.WssUserRoleEntryTable,
			(tRBElem *) &UserRoleEntry);

	if ((pUserRoleEntry == NULL) && 
			(i4TestValFsWssUserRoleRowStatus != CREATE_AND_WAIT) &&
			(i4TestValFsWssUserRoleRowStatus != CREATE_AND_GO))
	{
		*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
		WSSUSER_TRC (WSSUSER_CONTROL_PATH_TRC | WSSUSER_ALL_FAILURE_TRC,
				"nmhTestv2FsWssUserRoleRowStatus: User Role is not present\n");

		CLI_SET_ERR (CLI_WSSUSER_ROLE_NOT_CREATED);
		return SNMP_FAILURE;
	}

	switch (i4TestValFsWssUserRoleRowStatus)
	{
		case CREATE_AND_WAIT:
		case CREATE_AND_GO:

			if (RBTreeCount (gWssUserGlobals.WssUserRoleEntryTable, 
						&u4Count) != RB_SUCCESS)
			{
				return SNMP_FAILURE;   
			}

            /* Verify whether User Role count exceed the MAX limit.
             * If yes, display error message as " Only limited number 
             * of Users can be created - Exceeds the limit" */

			if (u4Count == MAX_WSS_USERROLE_INFO)
			{
				*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
				WSSUSER_TRC_ARG1 (WSSUSER_CONTROL_PATH_TRC | WSSUSER_ALL_FAILURE_TRC,
						"Only %d Users can be created", MAX_WSS_USERROLE_INFO);
				CLI_SET_ERR (CLI_WSSUSER_ROLE_MAX_LIMIT);
				return SNMP_FAILURE;
			}

			/* Here, the entry exist in UserRoleEntryTable, and we have received the 
			 * RowStatus as CREATE_AND_WAIT or CREATE_AND_GO, in this case
			 * Entry should not be allowed to be created */

			if (pUserRoleEntry != NULL)  
			{
				return SNMP_FAILURE;
			}
			break;

		case ACTIVE:
		case NOT_READY:
		case NOT_IN_SERVICE:
		case DESTROY:
			break;

		default:
			WSSUSER_TRC (WSSUSER_CONTROL_PATH_TRC | WSSUSER_ALL_FAILURE_TRC,
					"Invalid Row Status\n");
			return SNMP_FAILURE;
	}
	return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsWssUserRoleTable
 Input       :  The Indices
                FsWssUserRoleName
                FsWssUserRoleWlanIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FsWssUserRoleTable(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
    UNUSED_PARAM(pu4ErrorCode);
    UNUSED_PARAM(pSnmpIndexList);
    UNUSED_PARAM(pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsWssUserNameAccessListTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsWssUserNameAccessListTable
 Input       :  The Indices
                FsWssUserNameAccessListUserName
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 nmhValidateIndexInstanceFsWssUserNameAccessListTable(tSNMP_OCTET_STRING_TYPE *pFsWssUserNameAccessListUserName)
{
    UINT1    u1Index = 0;   
 
	if(pFsWssUserNameAccessListUserName->i4_Length > MAX_USER_NAME_LENGTH)
	{

		WSSUSER_TRC (WSSUSER_CONTROL_PATH_TRC | WSSUSER_ALL_FAILURE_TRC,
				"nmhValidateIndexInstanceFsWssUserNameAccessListTable: Length of Username is greater than 32\r \n");

		return SNMP_FAILURE;
	}
  
    /* Verify whether User Name starts with alphabets.
     * If not display error message as "Invalid User Name" */

    if (!ISALPHA (pFsWssUserNameAccessListUserName->pu1_OctetList[0]))
    {
        CLI_SET_ERR (CLI_WSSUSER_INVALID_USER_NAME);
        return SNMP_FAILURE;
    }
    
    /* User Name is valid if and only if it contains 
     * AlphaNumeric characters, Hiphens ("-"), Underscore ("_") */

    for (u1Index = 1; u1Index < pFsWssUserNameAccessListUserName->i4_Length; u1Index++)
    {
        if(!isalnum(pFsWssUserNameAccessListUserName->pu1_OctetList[u1Index]) &&
            pFsWssUserNameAccessListUserName->pu1_OctetList[u1Index] != WSSUSER_HIPHEN &&
            pFsWssUserNameAccessListUserName->pu1_OctetList[u1Index] != WSSUSER_UNDERSCORE)
        {
            CLI_SET_ERR (CLI_WSSUSER_INVALID_USER_NAME);
            return SNMP_FAILURE;
        }
    }

	return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsWssUserNameAccessListTable
 Input       :  The Indices
                FsWssUserNameAccessListUserName
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 nmhGetFirstIndexFsWssUserNameAccessListTable(tSNMP_OCTET_STRING_TYPE * pFsWssUserNameAccessListUserName)
{
	tUserNameAccessListEntry    *pUserNameAccessList = NULL;

	pUserNameAccessList = (tUserNameAccessListEntry *)RBTreeGetFirst (gWssUserGlobals.
			WssUserNameAccessListTable);

	if (pUserNameAccessList == NULL)
	{
		return SNMP_FAILURE;
	}

	MEMCPY (pFsWssUserNameAccessListUserName->pu1_OctetList,
			pUserNameAccessList->au1UserName,
			pUserNameAccessList->u4UserNameLen);

	pFsWssUserNameAccessListUserName->i4_Length = (INT4)(pUserNameAccessList->u4UserNameLen);

	return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsWssUserNameAccessListTable
 Input       :  The Indices
                FsWssUserNameAccessListUserName
                nextFsWssUserNameAccessListUserName
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */

INT1 nmhGetNextIndexFsWssUserNameAccessListTable(tSNMP_OCTET_STRING_TYPE *pFsWssUserNameAccessListUserName ,tSNMP_OCTET_STRING_TYPE * pNextFsWssUserNameAccessListUserName )
{
	tUserNameAccessListEntry    *pUserNameList = NULL;
	tUserNameAccessListEntry     UserNameAccessList;

	MEMSET (&UserNameAccessList, 0, sizeof(tUserNameAccessListEntry));

	MEMCPY (UserNameAccessList.au1UserName, pFsWssUserNameAccessListUserName->pu1_OctetList,
			pFsWssUserNameAccessListUserName->i4_Length);
	UserNameAccessList.u4UserNameLen  = (UINT4)(pFsWssUserNameAccessListUserName->i4_Length);

	pUserNameList = (tUserNameAccessListEntry*) RBTreeGetNext(gWssUserGlobals.
			WssUserNameAccessListTable,(tRBElem *)&UserNameAccessList,NULL);
	if (pUserNameList == NULL)
	{
		return SNMP_FAILURE;
	}

	MEMCPY ( pNextFsWssUserNameAccessListUserName->pu1_OctetList,
			(UINT1 *) (pUserNameList->au1UserName),
			pUserNameList->u4UserNameLen);

	pNextFsWssUserNameAccessListUserName->i4_Length  = (INT4)(pUserNameList->u4UserNameLen);

	return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsWssUserNameAccessListRowStatus
 Input       :  The Indices
                FsWssUserNameAccessListUserName

                The Object 
                retValFsWssUserNameAccessListRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1 nmhGetFsWssUserNameAccessListRowStatus(tSNMP_OCTET_STRING_TYPE *pFsWssUserNameAccessListUserName , INT4 *pi4RetValFsWssUserNameAccessListRowStatus)
{
	tUserNameAccessListEntry    *pUserNameList = NULL;
	tUserNameAccessListEntry     UserNameAccessList;

	MEMSET (&UserNameAccessList, 0, sizeof(tUserNameAccessListEntry));

	MEMCPY (UserNameAccessList.au1UserName, pFsWssUserNameAccessListUserName->pu1_OctetList,
			pFsWssUserNameAccessListUserName->i4_Length);
	UserNameAccessList.u4UserNameLen = (UINT4) pFsWssUserNameAccessListUserName->i4_Length;

	pUserNameList = (tUserNameAccessListEntry *) RBTreeGet (gWssUserGlobals.
			WssUserNameAccessListTable,(tRBElem *) &UserNameAccessList);

	if (pUserNameList == NULL)
	{
		WSSUSER_TRC (WSSUSER_CONTROL_PATH_TRC | WSSUSER_ALL_FAILURE_TRC,
				"nmhGetFsWssUserNameAccessListRowStatus: entry is not present\n");
		return SNMP_FAILURE;
	}
	*pi4RetValFsWssUserNameAccessListRowStatus = pUserNameList->i4RowStatus;

	return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsWssUserNameAccessListRowStatus
 Input       :  The Indices
                FsWssUserNameAccessListUserName

                The Object 
                setValFsWssUserNameAccessListRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1 nmhSetFsWssUserNameAccessListRowStatus(tSNMP_OCTET_STRING_TYPE *pFsWssUserNameAccessListUserName , INT4 i4SetValFsWssUserNameAccessListRowStatus)
{
	tUserNameAccessListEntry    *pUserNameAccessList = NULL;
	tUserNameAccessListEntry     UserNameAccessList;
        UINT4                        u4WlanId = 0;
        UINT4                        u4TerminateCause = 0;

	MEMSET (&UserNameAccessList, 0, sizeof(tUserNameAccessListEntry));

	switch (i4SetValFsWssUserNameAccessListRowStatus)
	{
		case CREATE_AND_WAIT:

			pUserNameAccessList = WSSUSER_MEM_ALLOCATE_MEM_BLK (WSSUSER_NAME_ACC_LIST_INFO_MEMPOOL_ID);

			if (pUserNameAccessList == NULL)
			{
				WSSUSER_TRC (WSSUSER_BUFFER_TRC | WSSUSER_ALL_FAILURE_TRC,
					"nmhSetFsWssUserNameAccessListRowStatus: Memory allocation failed for Username access list\n");
				return SNMP_FAILURE;
			}

			MEMSET (pUserNameAccessList, 0, sizeof (tUserNameAccessListEntry));
			MEMCPY (pUserNameAccessList->au1UserName, pFsWssUserNameAccessListUserName->pu1_OctetList,
					pFsWssUserNameAccessListUserName->i4_Length);
			pUserNameAccessList->u4UserNameLen = (UINT4)(pFsWssUserNameAccessListUserName->i4_Length); 

			if (RBTreeAdd (gWssUserGlobals.WssUserNameAccessListTable, 
						(tRBElem *) pUserNameAccessList) == RB_FAILURE)
			{
				WSSUSER_TRC (WSSUSER_MGMT_TRC | WSSUSER_ALL_FAILURE_TRC,
						"nmhSetFsWssUserNameAccessListRowStatus: Node addition to WssUserNameAccessListTable failed\n");

				WSSUSER_MEM_RELEASE_MEM_BLK (WSSUSER_NAME_ACC_LIST_INFO_MEMPOOL_ID, 
						pUserNameAccessList);
				return SNMP_FAILURE;
			}

			pUserNameAccessList->i4RowStatus = NOT_READY;
			break;

		case CREATE_AND_GO:

			pUserNameAccessList = WSSUSER_MEM_ALLOCATE_MEM_BLK (WSSUSER_NAME_ACC_LIST_INFO_MEMPOOL_ID);
			if (pUserNameAccessList == NULL)
			{
				WSSUSER_TRC (WSSUSER_BUFFER_TRC | WSSUSER_ALL_FAILURE_TRC,
						"nmhSetFsWssUserNameAccessListRowStatus:Memory allocation failed for Username access list\n");
				return SNMP_FAILURE;
			}
			MEMSET (pUserNameAccessList, 0, sizeof (tUserNameAccessListEntry));

			MEMCPY (pUserNameAccessList->au1UserName, pFsWssUserNameAccessListUserName->pu1_OctetList,
					pFsWssUserNameAccessListUserName->i4_Length);
			pUserNameAccessList->u4UserNameLen = (UINT4)(pFsWssUserNameAccessListUserName->i4_Length); 

			if (RBTreeAdd (gWssUserGlobals.WssUserNameAccessListTable, 
						(tRBElem *) pUserNameAccessList) == RB_FAILURE)
			{
				WSSUSER_TRC (WSSUSER_MGMT_TRC | WSSUSER_ALL_FAILURE_TRC,
						"nmhSetFsWssUserNameAccessListRowStatus:Node addition to WssUserNameAccessListTable failed\n");

				WSSUSER_MEM_RELEASE_MEM_BLK (WSSUSER_NAME_ACC_LIST_INFO_MEMPOOL_ID, 
						pUserNameAccessList);
				return SNMP_FAILURE;
			}

			pUserNameAccessList->i4RowStatus = ACTIVE;
            u4TerminateCause = ADMIN_RESET; 
			WssUserDeAuthenticateUser (pUserNameAccessList->au1UserName, u4WlanId, u4TerminateCause);
			break;

		case ACTIVE:

			MEMCPY (UserNameAccessList.au1UserName, pFsWssUserNameAccessListUserName->pu1_OctetList,
					pFsWssUserNameAccessListUserName->i4_Length);
			UserNameAccessList.u4UserNameLen = (UINT4)(pFsWssUserNameAccessListUserName->i4_Length);

			pUserNameAccessList = (tUserNameAccessListEntry *) RBTreeGet (gWssUserGlobals.
					WssUserNameAccessListTable,(tRBElem *) &UserNameAccessList);

			if (pUserNameAccessList == NULL)
			{
				WSSUSER_TRC (WSSUSER_CONTROL_PATH_TRC | WSSUSER_ALL_FAILURE_TRC,
						"nmhSetFsWssUserNameAccessListRowStatus: entry is not present\n");
				return SNMP_FAILURE;
			}

			pUserNameAccessList->i4RowStatus = ACTIVE;
            u4TerminateCause = ADMIN_RESET; 
			WssUserDeAuthenticateUser (pUserNameAccessList->au1UserName,u4WlanId,u4TerminateCause);
			break;

		case NOT_IN_SERVICE:

			MEMCPY (UserNameAccessList.au1UserName, pFsWssUserNameAccessListUserName->pu1_OctetList,
					pFsWssUserNameAccessListUserName->i4_Length);
			UserNameAccessList.u4UserNameLen = (UINT4) pFsWssUserNameAccessListUserName->i4_Length; 

			pUserNameAccessList = (tUserNameAccessListEntry *) RBTreeGet (gWssUserGlobals.
					WssUserNameAccessListTable,(tRBElem *) &UserNameAccessList);

			if (pUserNameAccessList == NULL)
			{
				WSSUSER_TRC (WSSUSER_CONTROL_PATH_TRC | WSSUSER_ALL_FAILURE_TRC,
						"nmhSetFsWssUserNameAccessListRowStatus: entry is not present\n");
				return SNMP_FAILURE;
			}

			pUserNameAccessList->i4RowStatus = NOT_IN_SERVICE;
			break;

		case DESTROY:

			MEMCPY (UserNameAccessList.au1UserName, pFsWssUserNameAccessListUserName->pu1_OctetList,
					pFsWssUserNameAccessListUserName->i4_Length);
			UserNameAccessList.u4UserNameLen = (UINT4) pFsWssUserNameAccessListUserName->i4_Length; 

			pUserNameAccessList = (tUserNameAccessListEntry *) RBTreeGet (gWssUserGlobals.
					WssUserNameAccessListTable,(tRBElem *) &UserNameAccessList);

			if (pUserNameAccessList == NULL)
			{
				WSSUSER_TRC (WSSUSER_MGMT_TRC | WSSUSER_ALL_FAILURE_TRC,
						"UserName Entry is Not present \r\n");

				return SNMP_FAILURE;
			}

			if (RBTreeRemove (gWssUserGlobals.WssUserNameAccessListTable, 
						(tRBElem *) pUserNameAccessList) == RB_FAILURE)
			{
				WSSUSER_TRC (WSSUSER_MGMT_TRC | WSSUSER_ALL_FAILURE_TRC,                                                                               "RBTreeRemove function failed !!!! \r\n");

				return SNMP_FAILURE;
			}

			WSSUSER_MEM_RELEASE_MEM_BLK (WSSUSER_NAME_ACC_LIST_INFO_MEMPOOL_ID, 
					pUserNameAccessList);
			break;   

		default:

			WSSUSER_TRC (WSSUSER_CONTROL_PATH_TRC |
					WSSUSER_ALL_FAILURE_TRC,
					"Invalid Row Status\n");
			return SNMP_FAILURE;
	}
	return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsWssUserNameAccessListRowStatus
 Input       :  The Indices
                FsWssUserNameAccessListUserName

                The Object 
                testValFsWssUserNameAccessListRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1 nmhTestv2FsWssUserNameAccessListRowStatus(UINT4 *pu4ErrorCode , 
                                               tSNMP_OCTET_STRING_TYPE *pFsWssUserNameAccessListUserName , 
                                               INT4 i4TestValFsWssUserNameAccessListRowStatus)
{
	tUserNameAccessListEntry   *pUserNameList = NULL;
	tUserNameAccessListEntry    UserNameAccessList;
	UINT4                       u4Count = 0;
	UINT1                       u1Index = 0;

	if (gWssUserGlobals.i4UserRoleStatus == WSSUSER_MODULE_DISABLED)
	{
		*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
		CLI_SET_ERR (CLI_WSSUSER_NOT_ENABLED);
		return SNMP_FAILURE;
	}

	if ((pFsWssUserNameAccessListUserName->i4_Length == 0) ||
			(pFsWssUserNameAccessListUserName->i4_Length > MAX_USER_NAME_LENGTH))
	{  
		WSSUSER_TRC (WSSUSER_CONTROL_PATH_TRC | WSSUSER_ALL_FAILURE_TRC,
				"nmhTestv2FsWssUserNameAccessListRowStatus: Invalid length user name.\r\n");
		CLI_SET_ERR (CLI_WSSUSER_INVALID_USER_NAME);
		return SNMP_FAILURE;
	}

    /* Verify whether User Name starts with alphabets.
     * If not display error message as "Invalid User Name" */

    if (!ISALPHA (pFsWssUserNameAccessListUserName->pu1_OctetList[0]))
    {
        CLI_SET_ERR (CLI_WSSUSER_INVALID_USER_NAME);
        return SNMP_FAILURE;
    }
 
   
    /* User Name is valid if and only if it contains 
     * AlphaNumeric characters, Hiphens ("-"), Underscore ("_") */
    
    for (u1Index = 1; u1Index < pFsWssUserNameAccessListUserName->i4_Length; u1Index++)
    {
        if(!isalnum(pFsWssUserNameAccessListUserName->pu1_OctetList[u1Index]) &&
            pFsWssUserNameAccessListUserName->pu1_OctetList[u1Index] != WSSUSER_HIPHEN &&
            pFsWssUserNameAccessListUserName->pu1_OctetList[u1Index] != WSSUSER_UNDERSCORE)
        {
            CLI_SET_ERR (CLI_WSSUSER_INVALID_USER_NAME);
            return SNMP_FAILURE;
        }
    }

	MEMSET (&UserNameAccessList, 0, sizeof (tUserNameAccessListEntry));

	MEMCPY (UserNameAccessList.au1UserName, pFsWssUserNameAccessListUserName->pu1_OctetList,
		  	        pFsWssUserNameAccessListUserName->i4_Length);

	UserNameAccessList.u4UserNameLen =  (UINT4)(pFsWssUserNameAccessListUserName->i4_Length);

	pUserNameList = (tUserNameAccessListEntry *) RBTreeGet (gWssUserGlobals.
			WssUserNameAccessListTable, (tRBElem *) &UserNameAccessList);

	if (pUserNameList == NULL &&
			(i4TestValFsWssUserNameAccessListRowStatus != CREATE_AND_WAIT) &&
			(i4TestValFsWssUserNameAccessListRowStatus != CREATE_AND_GO))

	{
		*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
		WSSUSER_TRC_ARG1 (WSSUSER_CONTROL_PATH_TRC | WSSUSER_ALL_FAILURE_TRC,
				"User Name %s is not present\n",
				pUserNameList->au1UserName);
		CLI_SET_ERR (CLI_WSSUSER_NO_USER_NAME);
		return SNMP_FAILURE;
	}

	switch (i4TestValFsWssUserNameAccessListRowStatus)   
	{
		case CREATE_AND_WAIT:
		case CREATE_AND_GO:

			if (RBTreeCount (gWssUserGlobals.WssUserNameAccessListTable, 
						&u4Count) != RB_SUCCESS)
			{
				return SNMP_FAILURE;   
			}

            /* Verify whether User Name restriction count exceeds the limit */

			if (u4Count == MAX_WSS_USERNAME_ACC_LIST_INFO)
			{
				*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
				WSSUSER_TRC_ARG1 (WSSUSER_CONTROL_PATH_TRC | WSSUSER_ALL_FAILURE_TRC,
						"Only %d Users can be restricted", MAX_WSS_USERNAME_ACC_LIST_INFO);
				CLI_SET_ERR (CLI_WSSUSER_RESTRICTED_USER_MAX_LIMIT);
				return SNMP_FAILURE;
			}

			if(pUserNameList != NULL)

			{
                /* Verify whether the User Name is already restricted.
                 * If yes, display error message as "User Name Address is already Restricted" */

				WSSUSER_TRC (WSSUSER_CONTROL_PATH_TRC | WSSUSER_ALL_FAILURE_TRC,
						"User Name Already Restricted\r\n");
				CLI_SET_ERR (CLI_WSSUSER_USER_NAME_RESTRICTED);

				return SNMP_FAILURE;
			}

			break;

		case NOT_IN_SERVICE:
		case NOT_READY:
		case DESTROY:
		case ACTIVE:
			break;

		default:

			*pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
			WSSUSER_TRC (WSSUSER_CONTROL_PATH_TRC |
					WSSUSER_ALL_FAILURE_TRC,
					"Invalid Row Status\n");
			return SNMP_FAILURE;

	}
	return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsWssUserNameAccessListTable
 Input       :  The Indices
                FsWssUserNameAccessListUserName
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1 nmhDepv2FsWssUserNameAccessListTable(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
    UNUSED_PARAM(pu4ErrorCode);
    UNUSED_PARAM(pSnmpIndexList);
    UNUSED_PARAM(pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsWssUserMacAccessListTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsWssUserMacAccessListTable
 Input       :  The Indices
                FsWssUserMacAccessListStaMac
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 nmhValidateIndexInstanceFsWssUserMacAccessListTable(tMacAddr FsWssUserMacAccessListStaMac)
{
	tMacAddr    BcastMacAddr;
	tMacAddr    zeroAddr;

	MEMSET (BcastMacAddr, 0xff, MAC_ADDR_LEN);
	MEMSET (zeroAddr, 0, MAC_ADDR_LEN);

	if  ((MEMCMP (FsWssUserMacAccessListStaMac, zeroAddr,
					MAC_ADDR_LEN) == 0) ||
			(MEMCMP (FsWssUserMacAccessListStaMac, BcastMacAddr,
					 MAC_ADDR_LEN) == 0))
	{
		CLI_SET_ERR (CLI_WSSUSER_INVALID_MAPPING);
		WSSUSER_TRC (WSSUSER_CONTROL_PATH_TRC | WSSUSER_ALL_FAILURE_TRC,
				"nmhValidateIndexInstanceFsWssUserMacAccessListTable: Invalid Mac Address\n");
		return SNMP_FAILURE;

	}
	return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsWssUserMacAccessListTable
 Input       :  The Indices
                FsWssUserMacAccessListStaMac
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 nmhGetFirstIndexFsWssUserMacAccessListTable(tMacAddr * pFsWssUserMacAccessListStaMac)
{
	tUserMacAccessListEntry    *pUserMacAccessList = NULL;

	pUserMacAccessList = (tUserMacAccessListEntry *)RBTreeGetFirst (gWssUserGlobals.
			WssUserMacAccessListTable);

	if (pUserMacAccessList == NULL)
	{
		return SNMP_FAILURE;
	}

	MEMCPY (pFsWssUserMacAccessListStaMac,
			(tMacAddr *)(pUserMacAccessList->staMac), MAC_ADDR_LEN);

	return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsWssUserMacAccessListTable
 Input       :  The Indices
                FsWssUserMacAccessListStaMac
                nextFsWssUserMacAccessListStaMac
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

/* GET_NEXT Routine.  */
INT1 nmhGetNextIndexFsWssUserMacAccessListTable(tMacAddr FsWssUserMacAccessListStaMac ,tMacAddr * pNextFsWssUserMacAccessListStaMac )
{
	tUserMacAccessListEntry    *pUserMacList = NULL;
	tUserMacAccessListEntry     UserMacAccessList;

	MEMSET (&UserMacAccessList, 0, sizeof(tUserMacAccessListEntry));

	MEMCPY (UserMacAccessList.staMac, FsWssUserMacAccessListStaMac,
			MAC_ADDR_LEN);

	pUserMacList = (tUserMacAccessListEntry*) RBTreeGetNext(gWssUserGlobals.
			WssUserMacAccessListTable, (tRBElem *)&UserMacAccessList,NULL);
	if (pUserMacList == NULL)
	{
		return SNMP_FAILURE;
	}

	MEMCPY (pNextFsWssUserMacAccessListStaMac,
			(tMacAddr *) (pUserMacList->staMac),MAC_ADDR_LEN);

	return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsWssUserMacAccessListRowStatus
 Input       :  The Indices
                FsWssUserMacAccessListStaMac

                The Object 
                retValFsWssUserMacAccessListRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1 nmhGetFsWssUserMacAccessListRowStatus(tMacAddr FsWssUserMacAccessListStaMac , INT4 *pi4RetValFsWssUserMacAccessListRowStatus)
{
	tUserMacAccessListEntry    *pUserMacList = NULL;
	tUserMacAccessListEntry     UserMacAccessList;

	MEMSET (&UserMacAccessList, 0, sizeof(tUserMacAccessListEntry));

	MEMCPY (UserMacAccessList.staMac, FsWssUserMacAccessListStaMac,
			MAC_ADDR_LEN);

	pUserMacList = (tUserMacAccessListEntry *) RBTreeGet (gWssUserGlobals.
			WssUserMacAccessListTable,(tRBElem *) &UserMacAccessList);

	if (pUserMacList == NULL)
	{
		return SNMP_FAILURE;
	}

	*pi4RetValFsWssUserMacAccessListRowStatus = pUserMacList->i4RowStatus;

	return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsWssUserMacAccessListRowStatus
 Input       :  The Indices
                FsWssUserMacAccessListStaMac

                The Object 
                setValFsWssUserMacAccessListRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1 nmhSetFsWssUserMacAccessListRowStatus(tMacAddr FsWssUserMacAccessListStaMac , INT4 i4SetValFsWssUserMacAccessListRowStatus)
{
	tUserMacAccessListEntry    *pUserMacAccessList = NULL;
	tWssStaWepProcessDB        *pWssStaWepProcessDB = NULL;
	tUserMacAccessListEntry     UserMacAccessList;
    UINT4                       u4TerminateCause = 0;

	switch (i4SetValFsWssUserMacAccessListRowStatus)
	{
		case CREATE_AND_WAIT:

			pUserMacAccessList = WSSUSER_MEM_ALLOCATE_MEM_BLK (WSSUSER_MAC_ACC_LIST_INFO_MEMPOOL_ID);
			if (pUserMacAccessList == NULL)
			{
				WSSUSER_TRC (WSSUSER_BUFFER_TRC | WSSUSER_ALL_FAILURE_TRC,
						"nmhSetFsWssUserMacAccessListRowStatus:Memory allocation failed for UserMac access list\n");
				return SNMP_FAILURE;
			}

			MEMSET (pUserMacAccessList, 0, sizeof (tUserMacAccessListEntry));

			MEMCPY (pUserMacAccessList->staMac, FsWssUserMacAccessListStaMac,
					MAC_ADDR_LEN);

			if (RBTreeAdd (gWssUserGlobals.WssUserMacAccessListTable, 
						(tRBElem *) pUserMacAccessList) == RB_FAILURE)
			{

				WSSUSER_TRC (WSSUSER_MGMT_TRC | WSSUSER_ALL_FAILURE_TRC,
						"nmhSetFsWssUserMacAccessListRowStatus: Node addition to WssUserMacAccessListTable failed\n");

				WSSUSER_MEM_RELEASE_MEM_BLK (WSSUSER_MAC_ACC_LIST_INFO_MEMPOOL_ID, 
						pUserMacAccessList);
				return SNMP_FAILURE;
			}

			pUserMacAccessList->i4RowStatus = NOT_READY;
			break;

		case CREATE_AND_GO:

			pUserMacAccessList = WSSUSER_MEM_ALLOCATE_MEM_BLK (WSSUSER_MAC_ACC_LIST_INFO_MEMPOOL_ID);
			if (pUserMacAccessList == NULL)
			{
				WSSUSER_TRC (WSSUSER_BUFFER_TRC | WSSUSER_ALL_FAILURE_TRC,
						"nmhSetFsWssUserMacAccessListRowStatus:Memory allocation failed for UserMac access list\n");
				return SNMP_FAILURE;
			}

			MEMSET (pUserMacAccessList, 0, sizeof (tUserMacAccessListEntry));

			MEMCPY (pUserMacAccessList->staMac, FsWssUserMacAccessListStaMac,
					sizeof (tMacAddr));

			if (RBTreeAdd (gWssUserGlobals.WssUserMacAccessListTable, 
						(tRBElem *) pUserMacAccessList)== RB_FAILURE)
			{

				WSSUSER_TRC (WSSUSER_MGMT_TRC | WSSUSER_ALL_FAILURE_TRC,
						"nmhSetFsWssUserMacAccessListRowStatus: Node addition to WssUserMacAccessListTable failed\n");

				WSSUSER_MEM_RELEASE_MEM_BLK (WSSUSER_MAC_ACC_LIST_INFO_MEMPOOL_ID, 
						pUserMacAccessList);
				return SNMP_FAILURE;
			}

			pUserMacAccessList->i4RowStatus = ACTIVE;
			pWssStaWepProcessDB = WssStaProcessEntryGet (FsWssUserMacAccessListStaMac);

			if (pWssStaWepProcessDB != NULL)
			{
                u4TerminateCause = ADMIN_RESET;
				WssUserDeAuthenticateStation (FsWssUserMacAccessListStaMac, OSIX_TRUE,u4TerminateCause);
			}

			break;
		case ACTIVE:

			MEMCPY (UserMacAccessList.staMac, FsWssUserMacAccessListStaMac, MAC_ADDR_LEN);

			pUserMacAccessList = (tUserMacAccessListEntry *) RBTreeGet (gWssUserGlobals.
					WssUserMacAccessListTable,(tRBElem *) &UserMacAccessList);

			if (pUserMacAccessList == NULL)
			{
				return SNMP_FAILURE;
			}

			pUserMacAccessList->i4RowStatus = ACTIVE;
			pWssStaWepProcessDB = WssStaProcessEntryGet (FsWssUserMacAccessListStaMac);

			if (pWssStaWepProcessDB != NULL)
			{
                u4TerminateCause = ADMIN_RESET;
				WssUserDeAuthenticateStation (FsWssUserMacAccessListStaMac, OSIX_TRUE,u4TerminateCause);
			}
			break;

		case NOT_IN_SERVICE:

			MEMCPY (UserMacAccessList.staMac, FsWssUserMacAccessListStaMac, MAC_ADDR_LEN);

			pUserMacAccessList = (tUserMacAccessListEntry *) RBTreeGet (gWssUserGlobals.
					WssUserMacAccessListTable,(tRBElem *) &UserMacAccessList);

			if (pUserMacAccessList == NULL)
			{
				return SNMP_FAILURE;
			}

			pUserMacAccessList->i4RowStatus = NOT_IN_SERVICE;
			break;

		case DESTROY:

			MEMCPY (UserMacAccessList.staMac, FsWssUserMacAccessListStaMac,
					MAC_ADDR_LEN);

			pUserMacAccessList = (tUserMacAccessListEntry *) RBTreeGet (gWssUserGlobals.
					WssUserMacAccessListTable,(tRBElem *) &UserMacAccessList);

			if(pUserMacAccessList == NULL)
			{
				WSSUSER_TRC (WSSUSER_MGMT_TRC | WSSUSER_ALL_FAILURE_TRC,
						"UserMac Address Entry is Not Present\r\n");

				return SNMP_FAILURE;

			}

			if (RBTreeRemove (gWssUserGlobals.WssUserMacAccessListTable, 
						(tRBElem *) pUserMacAccessList) == RB_FAILURE)
			{
				WSSUSER_TRC (WSSUSER_MGMT_TRC | WSSUSER_ALL_FAILURE_TRC,
						"RBTreeRemove function failed !!!! \r\n");

				return SNMP_FAILURE;
			}

			WSSUSER_MEM_RELEASE_MEM_BLK (WSSUSER_MAC_ACC_LIST_INFO_MEMPOOL_ID, pUserMacAccessList);
			break;

		default:

			WSSUSER_TRC (WSSUSER_CONTROL_PATH_TRC |
					WSSUSER_ALL_FAILURE_TRC,
					"Invalid Row Status\n");
			return  SNMP_FAILURE;
	}

	return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsWssUserMacAccessListRowStatus
 Input       :  The Indices
                FsWssUserMacAccessListStaMac

                The Object 
                testValFsWssUserMacAccessListRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1 nmhTestv2FsWssUserMacAccessListRowStatus(UINT4 *pu4ErrorCode , tMacAddr FsWssUserMacAccessListStaMac , INT4 i4TestValFsWssUserMacAccessListRowStatus)
{
	tUserMacAccessListEntry          *pUserMacList = NULL;
	tUserMacAccessListEntry           UserMacAccessList;
	tMacAddr                          BcastMacAddr;
	tMacAddr                          zeroAddr;
	UINT4                             u4Count = 0;

	if (gWssUserGlobals.i4UserRoleStatus == WSSUSER_MODULE_DISABLED)
	{
		*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
		CLI_SET_ERR (CLI_WSSUSER_NOT_ENABLED);
		return SNMP_FAILURE;
	}

	MEMSET (BcastMacAddr, 0xff, MAC_ADDR_LEN);
	MEMSET (zeroAddr, 0, MAC_ADDR_LEN);

	if  ((MEMCMP (FsWssUserMacAccessListStaMac, zeroAddr,
					MAC_ADDR_LEN) == 0) ||
			(MEMCMP (FsWssUserMacAccessListStaMac, BcastMacAddr,
					 MAC_ADDR_LEN) == 0))
	{
		*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
		CLI_SET_ERR (CLI_WSSUSER_INVALID_MAPPING);
		WSSUSER_TRC (WSSUSER_CONTROL_PATH_TRC | WSSUSER_ALL_FAILURE_TRC,
				"nmhTestv2FsWssUserMacAccessListRowStatus: Invalid Mac Address\n");
		return SNMP_FAILURE;

	}

	MEMSET (&UserMacAccessList, 0, sizeof (tUserMacAccessListEntry));

	MEMCPY (UserMacAccessList.staMac, FsWssUserMacAccessListStaMac,
			sizeof (tMacAddr));

	pUserMacList = (tUserMacAccessListEntry *) RBTreeGet (gWssUserGlobals.WssUserMacAccessListTable,
			(tRBElem *) &UserMacAccessList);

	if (pUserMacList == NULL &&
			(i4TestValFsWssUserMacAccessListRowStatus != CREATE_AND_WAIT) &&
			(i4TestValFsWssUserMacAccessListRowStatus != CREATE_AND_GO))

	{
		*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
		WSSUSER_TRC (WSSUSER_CONTROL_PATH_TRC | WSSUSER_ALL_FAILURE_TRC,
				"MAC Address is not present\n");
		CLI_SET_ERR (CLI_WSSUSER_NO_MAC_ADDR);
		return SNMP_FAILURE;
	}

	switch (i4TestValFsWssUserMacAccessListRowStatus)
	{
		case CREATE_AND_WAIT:
		case CREATE_AND_GO:

			if (RBTreeCount (gWssUserGlobals.WssUserMacAccessListTable, 
						&u4Count) != RB_SUCCESS)
			{
				return SNMP_FAILURE;   
			}

            /* Verify whether User MAC Address restriction count exceeds the limit */

			if (u4Count == MAX_WSS_USERMAC_ACC_LIST_INFO)
			{
				*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
				WSSUSER_TRC_ARG1 (WSSUSER_CONTROL_PATH_TRC | WSSUSER_ALL_FAILURE_TRC,
						"Only %d MAC can be restricted", MAX_WSS_USERMAC_ACC_LIST_INFO);
				CLI_SET_ERR (CLI_WSSUSER_RESTRICTED_MAC_MAX_LIMIT);
				return SNMP_FAILURE;
			}

			if(pUserMacList != NULL)
			{	
                /* Verify whether the MAC Address is already restricted.
                 * If yes, display error message as "MAC Address is already Restricted" */

				*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
				WSSUSER_TRC (WSSUSER_CONTROL_PATH_TRC | WSSUSER_ALL_FAILURE_TRC,
						"MAC Address is already Restricted\n");
				CLI_SET_ERR (CLI_WSSSUER_MAC_RESTRICTED);

				return SNMP_FAILURE;
			}
			break;

		case ACTIVE:
		case NOT_IN_SERVICE:
		case DESTROY:
			break;

		default:

			*pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
			WSSUSER_TRC (WSSUSER_CONTROL_PATH_TRC |
					WSSUSER_ALL_FAILURE_TRC,
					"Invalid Row Status\n");
			return  SNMP_FAILURE;
	}

	return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsWssUserMacAccessListTable
 Input       :  The Indices
                FsWssUserMacAccessListStaMac
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1 nmhDepv2FsWssUserMacAccessListTable(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
    UNUSED_PARAM(pu4ErrorCode);
    UNUSED_PARAM(pSnmpIndexList);
    UNUSED_PARAM(pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsWssUserMappingTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsWssUserMappingTable
 Input       :  The Indices

                FsWssUserMappingName
                FsWssUserMappingStaMac
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 nmhValidateIndexInstanceFsWssUserMappingTable(tSNMP_OCTET_STRING_TYPE *pFsWssUserMappingName , tMacAddr FsWssUserMappingStaMac)
{
	tMacAddr                  BcastMacAddr;
	tMacAddr                  zeroAddr;
	UINT2                     u2Len = 0;
    UINT1                     u1Index = 0;

	MEMSET (BcastMacAddr, 0xff, MAC_ADDR_LEN);
	MEMSET (zeroAddr, 0, MAC_ADDR_LEN);

	u2Len = (UINT2)(pFsWssUserMappingName->i4_Length);

	if  ((u2Len > MAX_USER_NAME_LENGTH) ||
			(MEMCMP (FsWssUserMappingStaMac, zeroAddr,
					 MAC_ADDR_LEN) == 0)   ||
			(MEMCMP (FsWssUserMappingStaMac, BcastMacAddr,
					 MAC_ADDR_LEN) == 0))
	{
		CLI_SET_ERR (CLI_WSSUSER_INVALID_MAPPING);
		WSSUSER_TRC (WSSUSER_CONTROL_PATH_TRC | WSSUSER_ALL_FAILURE_TRC,
				"nmhValidateIndexInstanceFsWssUserMappingTable: Invalid User Name and Mac Address\n");
		return SNMP_FAILURE;

	}

    /* Verify whether User Name starts with alphabets.
     * If not display error message as "Invalid User Name" */

    if (!ISALPHA (pFsWssUserMappingName->pu1_OctetList[0]))
    {
        CLI_SET_ERR (CLI_WSSUSER_INVALID_USER_NAME);
        return SNMP_FAILURE;
    }
    
    /* User Name is valid if and only if it contains 
     * AlphaNumeric characters, Hiphens ("-"), Underscore ("_") */

    for (u1Index = 1; u1Index < pFsWssUserMappingName->i4_Length; u1Index++)
    {
        if(!isalnum(pFsWssUserMappingName->pu1_OctetList[u1Index]) &&
            pFsWssUserMappingName->pu1_OctetList[u1Index] != WSSUSER_HIPHEN &&
            pFsWssUserMappingName->pu1_OctetList[u1Index] != WSSUSER_UNDERSCORE)
        {
            CLI_SET_ERR (CLI_WSSUSER_INVALID_USER_NAME);
            return SNMP_FAILURE;
        }
    }

	return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsWssUserMappingTable
 Input       :  The Indices

                FsWssUserMappingName
                FsWssUserMappingStaMac
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 nmhGetFirstIndexFsWssUserMappingTable(tSNMP_OCTET_STRING_TYPE * pFsWssUserMappingName , tMacAddr * pFsWssUserMappingStaMac)
{
	tUserMappingListEntry    *pUserMappingListEntry = NULL;

	pUserMappingListEntry = (tUserMappingListEntry *)RBTreeGetFirst (gWssUserGlobals.WssUserMappingListTable);

	if (pUserMappingListEntry == NULL)
	{
		CLI_SET_ERR (CLI_WSSUSER_MAPPING_NOT_CREATED);
		return SNMP_FAILURE;
	}

	MEMCPY (pFsWssUserMappingName->pu1_OctetList, pUserMappingListEntry->au1UserName, 
			pUserMappingListEntry->u4UserNameLen);
	pFsWssUserMappingName->i4_Length = (INT4)(pUserMappingListEntry->u4UserNameLen);

	MEMCPY (pFsWssUserMappingStaMac, pUserMappingListEntry->staMac, MAC_ADDR_LEN);

	return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsWssUserMappingTable
 Input       :  The Indices

                FsWssUserMappingName
                nextFsWssUserMappingName
                FsWssUserMappingStaMac
                nextFsWssUserMappingStaMac
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

/* GET_NEXT Routine.  */
INT1 nmhGetNextIndexFsWssUserMappingTable(tSNMP_OCTET_STRING_TYPE *pFsWssUserMappingName ,tSNMP_OCTET_STRING_TYPE * pNextFsWssUserMappingName  , tMacAddr FsWssUserMappingStaMac ,tMacAddr * pNextFsWssUserMappingStaMac )
{
	tUserMappingListEntry    *pUserMappingListEntry = NULL;
	tUserMappingListEntry     UserMappingListEntry;  

	MEMSET (&UserMappingListEntry, 0, sizeof(tUserMappingListEntry));

	UserMappingListEntry.u4UserNameLen = (UINT4)(pFsWssUserMappingName->i4_Length);

	MEMCPY (UserMappingListEntry.au1UserName, pFsWssUserMappingName->pu1_OctetList,
			UserMappingListEntry.u4UserNameLen);

	MEMCPY (UserMappingListEntry.staMac, FsWssUserMappingStaMac, MAC_ADDR_LEN);

	pUserMappingListEntry = (tUserMappingListEntry *) RBTreeGetNext(gWssUserGlobals.WssUserMappingListTable,
			(tRBElem *)&UserMappingListEntry,NULL);
	if (pUserMappingListEntry == NULL)
	{
		return SNMP_FAILURE;
	}

	MEMCPY (pNextFsWssUserMappingName->pu1_OctetList, pUserMappingListEntry->au1UserName,
			pUserMappingListEntry->u4UserNameLen);
	pNextFsWssUserMappingName->i4_Length =  (INT4)(pUserMappingListEntry->u4UserNameLen);

	MEMCPY (pNextFsWssUserMappingStaMac, pUserMappingListEntry->staMac, MAC_ADDR_LEN);

	return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsWssUserMappingRowStatus
 Input       :  The Indices

                FsWssUserMappingName
                FsWssUserMappingStaMac

                The Object 
                retValFsWssUserMappingRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1 nmhGetFsWssUserMappingRowStatus(tSNMP_OCTET_STRING_TYPE *pFsWssUserMappingName , tMacAddr FsWssUserMappingStaMac , INT4 *pi4RetValFsWssUserMappingRowStatus)
{
	tUserMappingListEntry    *pUserMappingListEntry = NULL;
	tUserMappingListEntry     UserMappingListEntry;

	MEMSET (&UserMappingListEntry, 0, sizeof(tUserMappingListEntry));

	UserMappingListEntry.u4UserNameLen = (UINT4)(pFsWssUserMappingName->i4_Length);

	MEMCPY (UserMappingListEntry.au1UserName , pFsWssUserMappingName->pu1_OctetList, 
			UserMappingListEntry.u4UserNameLen);

	MEMCPY (UserMappingListEntry.staMac, FsWssUserMappingStaMac, MAC_ADDR_LEN);

	pUserMappingListEntry = (tUserMappingListEntry *) RBTreeGet (gWssUserGlobals.WssUserMappingListTable,
			(tRBElem *) &UserMappingListEntry);

	if (pUserMappingListEntry == NULL)
	{
		WSSUSER_TRC (WSSUSER_CONTROL_PATH_TRC | WSSUSER_ALL_FAILURE_TRC,
				"nmhGetFsWssUserMappingRowStatus: User Mapping is not present\n");
		CLI_SET_ERR (CLI_WSSUSER_MAPPING_NOT_CREATED);
		return SNMP_FAILURE;
	}
	*pi4RetValFsWssUserMappingRowStatus = pUserMappingListEntry->i4RowStatus;   

	return SNMP_SUCCESS; 
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsWssUserMappingRowStatus
 Input       :  The Indices

                FsWssUserMappingName
                FsWssUserMappingStaMac

                The Object 
                setValFsWssUserMappingRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1 nmhSetFsWssUserMappingRowStatus(tSNMP_OCTET_STRING_TYPE *pFsWssUserMappingName , tMacAddr FsWssUserMappingStaMac , INT4 i4SetValFsWssUserMappingRowStatus)
{
	tUserMappingListEntry    *pUserMappingListEntry = NULL;
	tUserMappingListEntry     UserMappingListEntry;

	MEMSET (&UserMappingListEntry, 0, sizeof (tUserMappingListEntry));

	switch (i4SetValFsWssUserMappingRowStatus)
	{
		case CREATE_AND_WAIT:

			pUserMappingListEntry = WSSUSER_MEM_ALLOCATE_MEM_BLK (WSSUSER_MAPPING_LIST_INFO_MEMPOOL_ID);

			if (pUserMappingListEntry == NULL)
			{
				return SNMP_FAILURE;
			}

			pUserMappingListEntry->u4UserNameLen = (UINT4)(pFsWssUserMappingName->i4_Length);

			MEMCPY (pUserMappingListEntry->au1UserName, pFsWssUserMappingName->pu1_OctetList,
					pUserMappingListEntry->u4UserNameLen);

			MEMCPY (pUserMappingListEntry->staMac, FsWssUserMappingStaMac, MAC_ADDR_LEN);

			/* Add the User Mapping node to RB Tree */
			if( RBTreeAdd (gWssUserGlobals.WssUserMappingListTable, 
						(tRBElem *) pUserMappingListEntry) == RB_FAILURE)
			{
				WSSUSER_MEM_RELEASE_MEM_BLK (WSSUSER_MAPPING_LIST_INFO_MEMPOOL_ID, pUserMappingListEntry);
				return SNMP_FAILURE;
			}

			pUserMappingListEntry->i4RowStatus = NOT_READY;
			break;

		case NOT_IN_SERVICE:

			UserMappingListEntry.u4UserNameLen = (UINT4)(pFsWssUserMappingName->i4_Length);
			MEMCPY (UserMappingListEntry.au1UserName, pFsWssUserMappingName->pu1_OctetList,
					UserMappingListEntry.u4UserNameLen);

			MEMCPY (UserMappingListEntry.staMac, FsWssUserMappingStaMac, MAC_ADDR_LEN);

			pUserMappingListEntry = (tUserMappingListEntry *) RBTreeGet (gWssUserGlobals.
					WssUserMappingListTable,(tRBElem *) &UserMappingListEntry);
			if (pUserMappingListEntry == NULL) 
			{
				WSSUSER_TRC (WSSUSER_CONTROL_PATH_TRC | WSSUSER_ALL_FAILURE_TRC,
						"nmhSetFsWssUserMappingRowStatus: User Mapping is not present");

				CLI_SET_ERR (CLI_WSSUSER_MAPPING_NOT_CREATED);
				return SNMP_FAILURE;
			}

			pUserMappingListEntry->i4RowStatus = NOT_IN_SERVICE;
			break;

		case CREATE_AND_GO:

			pUserMappingListEntry = WSSUSER_MEM_ALLOCATE_MEM_BLK (WSSUSER_MAPPING_LIST_INFO_MEMPOOL_ID);

			if (pUserMappingListEntry == NULL)
			{
				return SNMP_FAILURE;
			}

			pUserMappingListEntry->u4UserNameLen = (UINT4)(pFsWssUserMappingName->i4_Length);

			MEMCPY (pUserMappingListEntry->au1UserName, pFsWssUserMappingName->pu1_OctetList,
					pUserMappingListEntry->u4UserNameLen);

			MEMCPY (pUserMappingListEntry->staMac, FsWssUserMappingStaMac, MAC_ADDR_LEN);

			/* Add the User Role node to RB Tree */
			if( RBTreeAdd (gWssUserGlobals.WssUserMappingListTable, 
						(tRBElem *) pUserMappingListEntry) == RB_FAILURE)
			{
				WSSUSER_MEM_RELEASE_MEM_BLK (WSSUSER_MAPPING_LIST_INFO_MEMPOOL_ID, pUserMappingListEntry);
				return SNMP_FAILURE;
			}

			pUserMappingListEntry->i4RowStatus = ACTIVE;
			break;

		case ACTIVE:

			UserMappingListEntry.u4UserNameLen = (UINT4)(pFsWssUserMappingName->i4_Length);
			MEMCPY (UserMappingListEntry.au1UserName, pFsWssUserMappingName->pu1_OctetList,
					UserMappingListEntry.u4UserNameLen);

			MEMCPY (UserMappingListEntry.staMac, FsWssUserMappingStaMac, MAC_ADDR_LEN);

			pUserMappingListEntry = (tUserMappingListEntry *) RBTreeGet (gWssUserGlobals.
					WssUserMappingListTable,(tRBElem *) &UserMappingListEntry);

			if (pUserMappingListEntry == NULL) 
			{
				WSSUSER_TRC (WSSUSER_CONTROL_PATH_TRC | WSSUSER_ALL_FAILURE_TRC,
						"nmhSetFsWssUserMappingRowStatus: User Mapping is not present");

				CLI_SET_ERR (CLI_WSSUSER_MAPPING_NOT_CREATED);
				return SNMP_FAILURE;
			}

			pUserMappingListEntry->i4RowStatus = ACTIVE;
			break;

		case DESTROY:

			UserMappingListEntry.u4UserNameLen = (UINT4)(pFsWssUserMappingName->i4_Length);
			MEMCPY (UserMappingListEntry.au1UserName, pFsWssUserMappingName->pu1_OctetList,
					UserMappingListEntry.u4UserNameLen);

			MEMCPY (UserMappingListEntry.staMac, FsWssUserMappingStaMac, MAC_ADDR_LEN);

			pUserMappingListEntry = (tUserMappingListEntry *) RBTreeGet (gWssUserGlobals.
					WssUserMappingListTable,(tRBElem *) &UserMappingListEntry);

			if (pUserMappingListEntry == NULL) 
			{
				WSSUSER_TRC (WSSUSER_CONTROL_PATH_TRC | WSSUSER_ALL_FAILURE_TRC,
						"nmhSetFsWssUserMappingRowStatus: User Mapping is not present");

				CLI_SET_ERR (CLI_WSSUSER_MAPPING_NOT_CREATED);
				return SNMP_FAILURE;
			}

			if (RBTreeRemove (gWssUserGlobals.WssUserMappingListTable, 
						(tRBElem *)pUserMappingListEntry)== RB_FAILURE)
			{
				WSSUSER_TRC (WSSUSER_CONTROL_PATH_TRC | WSSUSER_ALL_FAILURE_TRC,
						"Cannot delete User Mapping\n");

				CLI_SET_ERR (CLI_WSSUSER_MAPPING_CANNOT_DELETE);
				return SNMP_FAILURE;

			}

			WSSUSER_MEM_RELEASE_MEM_BLK (WSSUSER_MAPPING_LIST_INFO_MEMPOOL_ID, 
					pUserMappingListEntry);
			break;

		default:

			WSSUSER_TRC (WSSUSER_CONTROL_PATH_TRC |
					WSSUSER_ALL_FAILURE_TRC,
					"Invalid Row Status\n");
			return  SNMP_FAILURE;
	}
	return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsWssUserMappingRowStatus
 Input       :  The Indices

                FsWssUserMappingName
                FsWssUserMappingStaMac

                The Object 
                testValFsWssUserMappingRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1 nmhTestv2FsWssUserMappingRowStatus(UINT4 *pu4ErrorCode , tSNMP_OCTET_STRING_TYPE *pFsWssUserMappingName , tMacAddr FsWssUserMappingStaMac , INT4 i4TestValFsWssUserMappingRowStatus)
{
	tUserMappingListEntry    *pUserMappingListEntry = NULL;
	tUserMappingListEntry     UserMappingListEntry;
	tMacAddr                  BcastMacAddr;
	tMacAddr                  zeroAddr;
	UINT4                     u4Count = 0;
	UINT1                     u1Index = 0;

	if (gWssUserGlobals.i4UserRoleStatus == WSSUSER_MODULE_DISABLED)
	{
		*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
		CLI_SET_ERR (CLI_WSSUSER_NOT_ENABLED);
		return SNMP_FAILURE;
	}

	MEMSET (BcastMacAddr, 0xff, MAC_ADDR_LEN);
	MEMSET (zeroAddr, 0, MAC_ADDR_LEN);

	if  ((pFsWssUserMappingName->i4_Length > MAX_USER_NAME_LENGTH) ||
			(MEMCMP (FsWssUserMappingStaMac, zeroAddr,
					 MAC_ADDR_LEN) == 0)   ||
			(MEMCMP (FsWssUserMappingStaMac, BcastMacAddr,
					 MAC_ADDR_LEN) == 0))
	{
		*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
		CLI_SET_ERR (CLI_WSSUSER_INVALID_MAPPING);
		WSSUSER_TRC (WSSUSER_CONTROL_PATH_TRC | WSSUSER_ALL_FAILURE_TRC,
				"nmhTestv2FsWssUserMappingRowStatus: Invalid User Name and Mac Address\n");
		return SNMP_FAILURE;

	}

    /* Verify whether User Name starts with alphabets.
     * If not display error message as "Invalid User Name" */

    if (!ISALPHA (pFsWssUserMappingName->pu1_OctetList[0]))
    {
        CLI_SET_ERR (CLI_WSSUSER_INVALID_USER_NAME);
        return SNMP_FAILURE;
    }

    /* User Name is valid if and only if it contains 
     * AlphaNumeric characters, Hiphens ("-"), Underscore ("_") */
    
    for (u1Index = 1; u1Index < pFsWssUserMappingName->i4_Length; u1Index++)
    {
        if(!isalnum(pFsWssUserMappingName->pu1_OctetList[u1Index]) &&
            pFsWssUserMappingName->pu1_OctetList[u1Index] != WSSUSER_HIPHEN &&
            pFsWssUserMappingName->pu1_OctetList[u1Index] != WSSUSER_UNDERSCORE)
        {
            CLI_SET_ERR (CLI_WSSUSER_INVALID_USER_NAME);
            return SNMP_FAILURE;
        }
    }

	MEMSET (&UserMappingListEntry, 0, sizeof (tUserMappingListEntry));

	MEMCPY (UserMappingListEntry.au1UserName , pFsWssUserMappingName->pu1_OctetList, 
			       pFsWssUserMappingName->i4_Length);

	UserMappingListEntry.u4UserNameLen = (UINT4)(pFsWssUserMappingName->i4_Length);

	MEMCPY (UserMappingListEntry.staMac, FsWssUserMappingStaMac, MAC_ADDR_LEN);

	pUserMappingListEntry = (tUserMappingListEntry *) RBTreeGet (gWssUserGlobals.
			WssUserMappingListTable,(tRBElem *) &UserMappingListEntry);

	if ((pUserMappingListEntry == NULL) && 
			(i4TestValFsWssUserMappingRowStatus != CREATE_AND_WAIT) &&
			(i4TestValFsWssUserMappingRowStatus != CREATE_AND_GO))
	{
		*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
		WSSUSER_TRC (WSSUSER_CONTROL_PATH_TRC | WSSUSER_ALL_FAILURE_TRC,
				"nmhTestv2FsWssUserMappingRowStatus: User Mapping is not present\n");

		CLI_SET_ERR (CLI_WSSUSER_MAPPING_NOT_CREATED);
		return SNMP_FAILURE;
	}

	switch (i4TestValFsWssUserMappingRowStatus)
	{
		case CREATE_AND_WAIT:
		case CREATE_AND_GO:

			if (RBTreeCount (gWssUserGlobals.WssUserMappingListTable, 
						&u4Count) != RB_SUCCESS)
			{
				return SNMP_FAILURE;   
			}

            /* Verify whether User-MAC Mapping count exceeds the MAX limit.
             * If yes, display error message as " Only limited number of 
             * User-MAC Mapping can be done - Exceeds the limit"*/

			if (u4Count == MAX_WSS_USERMAPPING_LIST_INFO)
			{
				*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
				WSSUSER_TRC_ARG1 (WSSUSER_CONTROL_PATH_TRC | WSSUSER_ALL_FAILURE_TRC,
						"Only %d User-MAC mapping can be done", MAX_WSS_USERMAPPING_LIST_INFO);
				CLI_SET_ERR (CLI_WSSUSER_MACMAP_MAX_LIMIT);
				return SNMP_FAILURE;
			}

			if (pUserMappingListEntry != NULL)
			{
				/* Here pUserMappingListEntry already exist for this User and MAC,
				 * hence new entry should not be allowed to created */
				*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
				WSSUSER_TRC (WSSUSER_CONTROL_PATH_TRC | WSSUSER_ALL_FAILURE_TRC, 
						"nmhTestv2FsWssUserMappingRowStatus: User Mapping is already present\n");
				CLI_SET_ERR (CLI_WSSUSER_MAPPING_ALREADY_CREATED);
				return SNMP_FAILURE;
			}
			break;

		case NOT_READY:
		case ACTIVE:
		case NOT_IN_SERVICE:
		case DESTROY:
			break;

		default:

			WSSUSER_TRC (WSSUSER_CONTROL_PATH_TRC |
					WSSUSER_ALL_FAILURE_TRC,
					"Invalid Row Status\n");
			return  SNMP_FAILURE;
	}

	return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsWssUserMappingTable
 Input       :  The Indices

                FsWssUserMappingName
                FsWssUserMappingStaMac
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FsWssUserMappingTable(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
    UNUSED_PARAM(pu4ErrorCode);
    UNUSED_PARAM(pSnmpIndexList);
    UNUSED_PARAM(pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsWssUserSessionTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsWssUserSessionTable
 Input       :  The Indices
                FsWssUserName
                FsWssUserStaMac
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 nmhValidateIndexInstanceFsWssUserSessionTable(tSNMP_OCTET_STRING_TYPE *pFsWssUserName , tMacAddr FsWssUserStaMac)
{
    UNUSED_PARAM(pFsWssUserName);
    UNUSED_PARAM(FsWssUserStaMac);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsWssUserSessionTable
 Input       :  The Indices
                FsWssUserName
                FsWssUserStaMac
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 nmhGetFirstIndexFsWssUserSessionTable(tSNMP_OCTET_STRING_TYPE * pFsWssUserName , tMacAddr * pFsWssUserStaMac)
{
	tUserSessionEntry    *pUserSessionEntry = NULL;

	pUserSessionEntry = (tUserSessionEntry *)RBTreeGetFirst (gWssUserGlobals.
			WssUserSessionTable);

	if (pUserSessionEntry== NULL)
	{
		CLI_SET_ERR (CLI_WSSUSER_SESSION_NOT_CREATED);
		return SNMP_FAILURE;
	}
	MEMCPY ( pFsWssUserName->pu1_OctetList, pUserSessionEntry->au1UserName,
			pUserSessionEntry->u4UserNameLen);
	pFsWssUserName->i4_Length = (INT4)(pUserSessionEntry->u4UserNameLen);

	MEMCPY(pFsWssUserStaMac,pUserSessionEntry->staMac, sizeof(tMacAddr));

	return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsWssUserSessionTable
 Input       :  The Indices
                FsWssUserName
                nextFsWssUserName
                FsWssUserStaMac
                nextFsWssUserStaMac
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

/* GET_NEXT Routine.  */
INT1 nmhGetNextIndexFsWssUserSessionTable(tSNMP_OCTET_STRING_TYPE *pFsWssUserName ,tSNMP_OCTET_STRING_TYPE * pNextFsWssUserName  , tMacAddr FsWssUserStaMac ,tMacAddr * pNextFsWssUserStaMac )
{
	tUserSessionEntry   *pUserSessionEntry = NULL;
	tUserSessionEntry    UserSessionEntry;

	MEMSET (&UserSessionEntry, 0, sizeof(tUserSessionEntry));

	UserSessionEntry.u4UserNameLen = (UINT4)(pFsWssUserName->i4_Length);

	MEMCPY (UserSessionEntry.au1UserName, pFsWssUserName->pu1_OctetList,
			UserSessionEntry.u4UserNameLen);
	MEMCPY(UserSessionEntry.staMac,FsWssUserStaMac, sizeof(tMacAddr));

	pUserSessionEntry = (tUserSessionEntry*) RBTreeGetNext(gWssUserGlobals.WssUserSessionTable,
			(tRBElem *)&UserSessionEntry,NULL);

	if (pUserSessionEntry == NULL)
	{
		return SNMP_FAILURE;
	}

	MEMCPY (pNextFsWssUserName->pu1_OctetList, pUserSessionEntry->au1UserName,
			pUserSessionEntry->u4UserNameLen);
	pNextFsWssUserName->i4_Length = (INT4)(pUserSessionEntry->u4UserNameLen);

	MEMCPY(pNextFsWssUserStaMac,pUserSessionEntry->staMac, sizeof(tMacAddr));

	return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsWssUserWlanIndex
 Input       :  The Indices
                FsWssUserName
                FsWssUserStaMac

                The Object 
                retValFsWssUserWlanIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1 nmhGetFsWssUserWlanIndex(tSNMP_OCTET_STRING_TYPE *pFsWssUserName , tMacAddr FsWssUserStaMac , UINT4 *pu4RetValFsWssUserWlanIndex)
{
	tUserSessionEntry    *pUserSessionEntry = NULL;
	tUserSessionEntry     UserSessionEntry;

	MEMSET (&UserSessionEntry, 0, sizeof (tUserSessionEntry));

	MEMCPY (UserSessionEntry.au1UserName , pFsWssUserName->pu1_OctetList,
			  pFsWssUserName->i4_Length);

	UserSessionEntry.u4UserNameLen  = (UINT4)(pFsWssUserName->i4_Length);

	MEMCPY(UserSessionEntry.staMac,FsWssUserStaMac, sizeof(tMacAddr));

	pUserSessionEntry = (tUserSessionEntry *) RBTreeGet (gWssUserGlobals.
			WssUserSessionTable,(tRBElem *) &UserSessionEntry);

	if (pUserSessionEntry == NULL)
	{
		WSSUSER_TRC_ARG1 (WSSUSER_CONTROL_PATH_TRC | WSSUSER_ALL_FAILURE_TRC,
				"nmhGetFsWssUserWlanIndex: User Session %s  Not Created\n", pFsWssUserName->pu1_OctetList);

		CLI_SET_ERR (CLI_WSSUSER_SESSION_NOT_CREATED);
		return SNMP_FAILURE;
	}
	*pu4RetValFsWssUserWlanIndex = pUserSessionEntry->u4WlanIndex;

	return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsWssUserAllotedBandWidth
 Input       :  The Indices
                FsWssUserName
                FsWssUserStaMac

                The Object 
                retValFsWssUserAllotedBandWidth
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsWssUserAllotedBandWidth(tSNMP_OCTET_STRING_TYPE *pFsWssUserName, 
			tMacAddr FsWssUserStaMac, 
			UINT4 *pu4RetValFsWssUserAllotedBandWidth)
{
    tUserSessionEntry    *pUserSessionEntry = NULL;
    tUserSessionEntry     UserSessionEntry;

    MEMSET (&UserSessionEntry, 0, sizeof (tUserSessionEntry));

    MEMCPY (UserSessionEntry.au1UserName , pFsWssUserName->pu1_OctetList,
	    pFsWssUserName->i4_Length);

    UserSessionEntry.u4UserNameLen  = (UINT4)(pFsWssUserName->i4_Length);

    MEMCPY(UserSessionEntry.staMac,FsWssUserStaMac, sizeof(tMacAddr));

    pUserSessionEntry = (tUserSessionEntry *) RBTreeGet (gWssUserGlobals.
	    WssUserSessionTable,(tRBElem *) &UserSessionEntry);

    if (pUserSessionEntry == NULL)
    {
	WSSUSER_TRC_ARG1 (WSSUSER_CONTROL_PATH_TRC | WSSUSER_ALL_FAILURE_TRC,
		"nmhGetFsWssUserAllotedBandWidth: User Session %s  Not Created\n", pFsWssUserName->pu1_OctetList);

	CLI_SET_ERR (CLI_WSSUSER_SESSION_NOT_CREATED);
	return SNMP_FAILURE;
    }
    *pu4RetValFsWssUserAllotedBandWidth = pUserSessionEntry->u4AllotedBandwidth;
    return SNMP_SUCCESS;
}


/****************************************************************************
 Function    :  nmhGetFsWssUserAllotedDLBandWidth
 Input       :  The Indices
                FsWssUserName
                FsWssUserStaMac

                The Object 
                retValFsWssUserAllotedDLBandWidth
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsWssUserAllotedDLBandWidth(tSNMP_OCTET_STRING_TYPE *pFsWssUserName, 
			tMacAddr FsWssUserStaMac, 
			UINT4 *pu4RetValFsWssUserAllotedDLBandWidth)
{
    tUserSessionEntry    *pUserSessionEntry = NULL;
    tUserSessionEntry     UserSessionEntry;

    MEMSET (&UserSessionEntry, 0, sizeof (tUserSessionEntry));

    MEMCPY (UserSessionEntry.au1UserName , pFsWssUserName->pu1_OctetList,
	    pFsWssUserName->i4_Length);

    UserSessionEntry.u4UserNameLen  = (UINT4)(pFsWssUserName->i4_Length);

    MEMCPY(UserSessionEntry.staMac,FsWssUserStaMac, sizeof(tMacAddr));

    pUserSessionEntry = (tUserSessionEntry *) RBTreeGet (gWssUserGlobals.
	    WssUserSessionTable,(tRBElem *) &UserSessionEntry);

    if (pUserSessionEntry == NULL)
    {
	WSSUSER_TRC_ARG1 (WSSUSER_CONTROL_PATH_TRC | WSSUSER_ALL_FAILURE_TRC,
		"nmhGetFsWssUserAllotedDLBandWidth: User Session %s  Not Created\n", pFsWssUserName->pu1_OctetList);

	CLI_SET_ERR (CLI_WSSUSER_SESSION_NOT_CREATED);
	return SNMP_FAILURE;
    }
    *pu4RetValFsWssUserAllotedDLBandWidth = pUserSessionEntry->u4AllotedDLBandwidth;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsWssUserAllotedULBandWidth
 Input       :  The Indices
                FsWssUserName
                FsWssUserStaMac

                The Object
                retValFsWssUserAllotedULBandWidth
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhGetFsWssUserAllotedULBandWidth(tSNMP_OCTET_STRING_TYPE *pFsWssUserName, 
			tMacAddr FsWssUserStaMac, 
			UINT4 *pu4RetValFsWssUserAllotedULBandWidth)
{
    tUserSessionEntry    *pUserSessionEntry = NULL;
    tUserSessionEntry     UserSessionEntry;

    MEMSET (&UserSessionEntry, 0, sizeof (tUserSessionEntry));

    MEMCPY (UserSessionEntry.au1UserName , pFsWssUserName->pu1_OctetList,
	    pFsWssUserName->i4_Length);

    UserSessionEntry.u4UserNameLen  = (UINT4)(pFsWssUserName->i4_Length);

    MEMCPY(UserSessionEntry.staMac,FsWssUserStaMac, sizeof(tMacAddr));

    pUserSessionEntry = (tUserSessionEntry *) RBTreeGet (gWssUserGlobals.
	    WssUserSessionTable,(tRBElem *) &UserSessionEntry);

    if (pUserSessionEntry == NULL)
    {
        WSSUSER_TRC_ARG1 (WSSUSER_CONTROL_PATH_TRC | WSSUSER_ALL_FAILURE_TRC,
                "nmhGetFsWssUserAllotedULBandWidth: User Session %s  Not Created\n", pFsWssUserName->pu1_OctetList);

        CLI_SET_ERR (CLI_WSSUSER_SESSION_NOT_CREATED);
        return SNMP_FAILURE;
    }

    *pu4RetValFsWssUserAllotedULBandWidth = pUserSessionEntry->u4AllotedULBandwidth;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsWssUserAllotedVolume
 Input       :  The Indices
                FsWssUserName
                FsWssUserStaMac

                The Object 
                retValFsWssUserAllotedVolume
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsWssUserAllotedVolume(tSNMP_OCTET_STRING_TYPE *pFsWssUserName , tMacAddr FsWssUserStaMac , UINT4 *pu4RetValFsWssUserAllotedVolume)
{
	tUserSessionEntry    *pUserSessionEntry = NULL;
	tUserSessionEntry     UserSessionEntry;

	MEMSET (&UserSessionEntry, 0, sizeof (tUserSessionEntry));

	MEMCPY (UserSessionEntry.au1UserName , pFsWssUserName->pu1_OctetList,
			    pFsWssUserName->i4_Length);

	UserSessionEntry.u4UserNameLen  = (UINT4)(pFsWssUserName->i4_Length);

	MEMCPY(UserSessionEntry.staMac,FsWssUserStaMac, sizeof(tMacAddr));

	pUserSessionEntry = (tUserSessionEntry *) RBTreeGet (gWssUserGlobals.
			WssUserSessionTable,(tRBElem *) &UserSessionEntry);

	if (pUserSessionEntry == NULL)
	{
		WSSUSER_TRC_ARG1 (WSSUSER_CONTROL_PATH_TRC | WSSUSER_ALL_FAILURE_TRC,
				"nmhGetFsWssUserAllotedVolume: User Session %s  Not Created\n", pFsWssUserName->pu1_OctetList);

		CLI_SET_ERR (CLI_WSSUSER_SESSION_NOT_CREATED);
		return SNMP_FAILURE;
	}
	*pu4RetValFsWssUserAllotedVolume = pUserSessionEntry->u4AllotedVolume;

	return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsWssUserAllotedTime
 Input       :  The Indices
                FsWssUserName
                FsWssUserStaMac

                The Object 
                retValFsWssUserAllotedTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1 nmhGetFsWssUserAllotedTime(tSNMP_OCTET_STRING_TYPE *pFsWssUserName , tMacAddr FsWssUserStaMac , UINT4 *pu4RetValFsWssUserAllotedTime)
{
	tUserSessionEntry    *pUserSessionEntry = NULL;
	tUserSessionEntry     UserSessionEntry;

	MEMSET (&UserSessionEntry, 0, sizeof (tUserSessionEntry));

	MEMCPY (UserSessionEntry.au1UserName , pFsWssUserName->pu1_OctetList,
			        pFsWssUserName->i4_Length);

	UserSessionEntry.u4UserNameLen  = (UINT4)(pFsWssUserName->i4_Length);

	MEMCPY(UserSessionEntry.staMac,FsWssUserStaMac, sizeof(tMacAddr));

	pUserSessionEntry = (tUserSessionEntry *) RBTreeGet (gWssUserGlobals.
			WssUserSessionTable,(tRBElem *) &UserSessionEntry);

	if (pUserSessionEntry == NULL)
	{
		WSSUSER_TRC_ARG1 (WSSUSER_CONTROL_PATH_TRC | WSSUSER_ALL_FAILURE_TRC,
				"nmhGetFsWssUserAllotedTime: User Session %s  Not Created\n", pFsWssUserName->pu1_OctetList);

		CLI_SET_ERR (CLI_WSSUSER_SESSION_NOT_CREATED);
		return SNMP_FAILURE;
	}

	*pu4RetValFsWssUserAllotedTime = pUserSessionEntry->u4AllotedTime;

	return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsWssUserUsedVolume
 Input       :  The Indices
                FsWssUserName
                FsWssUserStaMac

                The Object 
                retValFsWssUserUsedVolume
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1 
nmhGetFsWssUserUsedVolume(tSNMP_OCTET_STRING_TYPE *pFsWssUserName, 
			tMacAddr FsWssUserStaMac, 
			UINT4 *pu4RetValFsWssUserUsedVolume)
{
	tUserSessionEntry    *pUserSessionEntry = NULL;
	tUserSessionEntry     UserSessionEntry;

	MEMSET (&UserSessionEntry, 0, sizeof (tUserSessionEntry));

	MEMCPY (UserSessionEntry.au1UserName , pFsWssUserName->pu1_OctetList,
			           pFsWssUserName->i4_Length);

	UserSessionEntry.u4UserNameLen  = (UINT4)(pFsWssUserName->i4_Length);

	MEMCPY(UserSessionEntry.staMac,FsWssUserStaMac, sizeof(tMacAddr));

	pUserSessionEntry = (tUserSessionEntry *) RBTreeGet (gWssUserGlobals.
			WssUserSessionTable,(tRBElem *) &UserSessionEntry);

	if (pUserSessionEntry == NULL)
	{
		WSSUSER_TRC_ARG1 (WSSUSER_CONTROL_PATH_TRC | WSSUSER_ALL_FAILURE_TRC,
				"nmhGetFsWssUserUsedVolume: User Session %s  Not Created\n", pFsWssUserName->pu1_OctetList);

		CLI_SET_ERR (CLI_WSSUSER_SESSION_NOT_CREATED);
		return SNMP_FAILURE;
	}

	*pu4RetValFsWssUserUsedVolume = pUserSessionEntry->u4UsedVolume;

	return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsWssUserUsedTime
 Input       :  The Indices
                FsWssUserName
                FsWssUserStaMac

                The Object 
                retValFsWssUserUsedTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1 
nmhGetFsWssUserUsedTime(tSNMP_OCTET_STRING_TYPE *pFsWssUserName, 
		tMacAddr FsWssUserStaMac, 
		UINT4 *pu4RetValFsWssUserUsedTime)
{
	tUserSessionEntry   *pUserSessionEntry = NULL;
	tUserSessionEntry    UserSessionEntry;
	UINT4                u4CurrentTime = 0; 
	UINT4                u4ElapsedTime = 0; 

	MEMSET (&UserSessionEntry, 0, sizeof (tUserSessionEntry));

	MEMCPY (UserSessionEntry.au1UserName , pFsWssUserName->pu1_OctetList,
			          pFsWssUserName->i4_Length);

	UserSessionEntry.u4UserNameLen  = (UINT4)(pFsWssUserName->i4_Length);


	MEMCPY(UserSessionEntry.staMac,FsWssUserStaMac, sizeof(tMacAddr));

	pUserSessionEntry = (tUserSessionEntry *) RBTreeGet (gWssUserGlobals.
			WssUserSessionTable,(tRBElem *) &UserSessionEntry);

	if (pUserSessionEntry == NULL)
	{
		WSSUSER_TRC_ARG1 (WSSUSER_CONTROL_PATH_TRC | WSSUSER_ALL_FAILURE_TRC,
				"nmhGetFsWssUserUsedTime: User Session %s  Not Created\n", 
				pFsWssUserName->pu1_OctetList);

		CLI_SET_ERR (CLI_WSSUSER_SESSION_NOT_CREATED);
		return SNMP_FAILURE;
	}

	u4CurrentTime = OsixGetSysUpTime ();
	u4ElapsedTime = (u4CurrentTime - (pUserSessionEntry->u4StartTime));
	pUserSessionEntry->u4UsedTime = u4ElapsedTime;   
	*pu4RetValFsWssUserUsedTime = pUserSessionEntry->u4UsedTime;

	return SNMP_SUCCESS;
}
