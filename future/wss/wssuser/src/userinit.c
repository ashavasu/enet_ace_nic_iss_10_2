/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: userinit.c,v 1.1.1.1 2016/09/20 10:42:41 siva Exp $
*
* Description : This file contains init functions for WSSUSER Module 
*********************************************************************/

#ifndef _USERINIT_C
#define _USERINIT_C

#include "userinc.h"

/*****************************************************************************/
/*                                                                           */
/* Function     : WssUserInitModuleStart                                     */
/*                                                                           */
/* Description  : This function creates & initializes the system resources   */
/*                like memory pool, semaphore, message queue and SNMP MIB    */
/*                registration for the WSS User Role Module                  */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : WSSUSER_SUCCESS/WSSUSER_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
UINT4 WssUserInitModuleStart (VOID)
{
    UINT4             u4Events = 0;

    MEMSET (&gWssUserGlobals, 0, sizeof (tWssUserGlobals));

    gWssUserGlobals.u4WssUserTrcOpt = 0;
    gWssUserGlobals.u4LoggedCount = 0;
    gWssUserGlobals.u4BlockedCount = 0;
 
 
    WSSUSER_TRC (WSSUSER_INIT_SHUT_TRC | WSSUSER_CONTROL_PATH_TRC,
                 "Initializing WSSUSER.....\n");

    /* Get Task Id of the created User Role Managment Task and store it.
       This is required to receive the events posted to this TASK  */

    if ( OsixTskIdSelf (&gWssUserGlobals.UserTaskId) != OSIX_SUCCESS)
    {
        WSSUSER_INIT_COMPLETE (OSIX_FAILURE);
        return WSSUSER_FAILURE;
    }


    /* Create Memory Pools Required for User Role Module */

    if (WssuserSizingMemCreateMemPools () != WSSUSER_SUCCESS)
    {
        WSSUSER_TRC (WSSUSER_INIT_SHUT_TRC | WSSUSER_CONTROL_PATH_TRC |
                     WSSUSER_ALL_FAILURE_TRC, "Create mem pool for WSSUSER failed\n");

        WSSUSER_INIT_COMPLETE (OSIX_FAILURE);

        return WSSUSER_FAILURE;
    }

    /* Assign MempoolIds created in sz.c to global MempoolIds */
    WssUserAssignMempoolIds ();

    /* Initialise the timer module */
    if (WssUserTmrInit () != WSSUSER_SUCCESS)
    {
        WSSUSER_TRC (WSSUSER_INIT_SHUT_TRC | WSSUSER_CONTROL_PATH_TRC |
                     WSSUSER_ALL_FAILURE_TRC, "Timer Init Failed\n");
        WssuserSizingMemDeleteMemPools ();
        return WSSUSER_FAILURE;
    }

    /* Create the RBTree to store the User Group Entry */
    gWssUserGlobals.WssUserGroupEntryTable =
        RBTreeCreateEmbedded ((FSAP_OFFSETOF (tUserGroupEntry,
                                              UserGroupEntryNode)),
                               WssUserCompareGroupId);

    if (gWssUserGlobals.WssUserGroupEntryTable == NULL)
    {
        WSSUSER_TRC (WSSUSER_INIT_SHUT_TRC | WSSUSER_CONTROL_PATH_TRC|
                     WSSUSER_ALL_FAILURE_TRC, "RB Tree Creation for "
                     "User Group Table failed\n");
 
        WssuserSizingMemDeleteMemPools ();
        WSSUSER_INIT_COMPLETE (OSIX_FAILURE);
        return WSSUSER_FAILURE;
    }


    /* Create the RBTree to store the User Role Entry */
    gWssUserGlobals.WssUserRoleEntryTable =
        RBTreeCreateEmbedded ((FSAP_OFFSETOF (tUserRoleEntry,
                                              UserRoleEntryNode)),
                              WssUserCompareUserNameAndWlanId);

    if (gWssUserGlobals.WssUserRoleEntryTable == NULL)
    {
        WSSUSER_TRC (WSSUSER_INIT_SHUT_TRC | WSSUSER_CONTROL_PATH_TRC|
                     WSSUSER_ALL_FAILURE_TRC, "RB Tree Creation for "
                     "User Role Table failed\n");

        WssuserSizingMemDeleteMemPools ();
        RBTreeDelete (gWssUserGlobals.WssUserGroupEntryTable);
        WSSUSER_INIT_COMPLETE (OSIX_FAILURE);
        return WSSUSER_FAILURE;
    }


    /* Create the RBTree to store User Name access list Entry - 
       to restrict the user's access based User Name*/
    gWssUserGlobals.WssUserNameAccessListTable =
        RBTreeCreateEmbedded ((FSAP_OFFSETOF (tUserNameAccessListEntry,
                                              UserNameAccessListNode)),
                              WssUserRestrictUserNameCmp);

    if (gWssUserGlobals.WssUserNameAccessListTable == NULL)
    {
        WSSUSER_TRC (WSSUSER_INIT_SHUT_TRC | WSSUSER_CONTROL_PATH_TRC|
                     WSSUSER_ALL_FAILURE_TRC, "RB Tree Creation for "
                     "Restrict User Name Table failed\n");
        WssuserSizingMemDeleteMemPools ();
        RBTreeDelete (gWssUserGlobals.WssUserGroupEntryTable);
        RBTreeDelete (gWssUserGlobals.WssUserRoleEntryTable);
        WSSUSER_INIT_COMPLETE (OSIX_FAILURE);
        return WSSUSER_FAILURE;
    }


    /* Create the RBTree to store User MAC access list Entry - 
       to restrict the user's access based on User MAC */
    gWssUserGlobals.WssUserMacAccessListTable =
        RBTreeCreateEmbedded ((FSAP_OFFSETOF (tUserMacAccessListEntry,
                                              UserMacAccessListNode)),
                              WssUserRestrictUserMacCmp);

    if (gWssUserGlobals.WssUserMacAccessListTable == NULL)
    {
        WSSUSER_TRC (WSSUSER_INIT_SHUT_TRC | WSSUSER_CONTROL_PATH_TRC|
                     WSSUSER_ALL_FAILURE_TRC, "RB Tree Creation for "
                     "Restrict User Mac Table failed\n");

        WssuserSizingMemDeleteMemPools ();
        RBTreeDelete (gWssUserGlobals.WssUserGroupEntryTable);
        RBTreeDelete (gWssUserGlobals.WssUserRoleEntryTable);
        RBTreeDelete (gWssUserGlobals.WssUserNameAccessListTable);
        WSSUSER_INIT_COMPLETE (OSIX_FAILURE);
        return WSSUSER_FAILURE;
    }

    /* Create the RBTree to store the User - MAC Mapping List Entry - 
       to association User only from particular MAC address */
    gWssUserGlobals.WssUserMappingListTable =
        RBTreeCreateEmbedded ((FSAP_OFFSETOF (tUserMappingListEntry,
                                              UserMappingNode)),
                              WssUserCompareUserNameAndMac);

    if (gWssUserGlobals.WssUserMappingListTable == NULL)
    {
        WSSUSER_TRC (WSSUSER_INIT_SHUT_TRC | WSSUSER_CONTROL_PATH_TRC|
                     WSSUSER_ALL_FAILURE_TRC, "RB Tree Creation for "
                     "User Mapping List Table failed\n");

        WssuserSizingMemDeleteMemPools ();
        RBTreeDelete (gWssUserGlobals.WssUserGroupEntryTable);
        RBTreeDelete (gWssUserGlobals.WssUserRoleEntryTable);
        RBTreeDelete (gWssUserGlobals.WssUserNameAccessListTable);
        RBTreeDelete (gWssUserGlobals.WssUserMacAccessListTable);
        WSSUSER_INIT_COMPLETE (OSIX_FAILURE);
        return WSSUSER_FAILURE;
    }

    /* Create the RBTree to store the User - Session Table */ 
    gWssUserGlobals.WssUserSessionTable =
        RBTreeCreateEmbedded ((FSAP_OFFSETOF (tUserSessionEntry,
                                              UserSessionNode)),
                               WssUserSessNameCmp);

    if (gWssUserGlobals.WssUserMappingListTable == NULL)
    {
        WSSUSER_TRC (WSSUSER_INIT_SHUT_TRC | WSSUSER_CONTROL_PATH_TRC|
                     WSSUSER_ALL_FAILURE_TRC, "RB Tree Creation for "
                     "User Mapping List Table failed\n");
        WssuserSizingMemDeleteMemPools ();
        RBTreeDelete (gWssUserGlobals.WssUserGroupEntryTable);
        RBTreeDelete (gWssUserGlobals.WssUserRoleEntryTable);
        RBTreeDelete (gWssUserGlobals.WssUserNameAccessListTable);
        RBTreeDelete (gWssUserGlobals.WssUserMacAccessListTable);
        RBTreeDelete (gWssUserGlobals.WssUserMappingListTable);
        WSSUSER_INIT_COMPLETE (OSIX_FAILURE);
        return WSSUSER_FAILURE;
    }

    /* Create the RBTree to store the User - Session Table */ 
    gWssUserGlobals.WssUserNameTable =
        RBTreeCreateEmbedded ((FSAP_OFFSETOF (tUserNameEntry,
                                              UserNameNode)),
                               WssUserNameCmp);

    if (gWssUserGlobals.WssUserNameTable  == NULL)
    {
        WSSUSER_TRC (WSSUSER_INIT_SHUT_TRC | WSSUSER_CONTROL_PATH_TRC|
                     WSSUSER_ALL_FAILURE_TRC, "RB Tree Creation for "
                     "User Name Table failed\n");
        WssuserSizingMemDeleteMemPools ();
        RBTreeDelete (gWssUserGlobals.WssUserGroupEntryTable);
        RBTreeDelete (gWssUserGlobals.WssUserRoleEntryTable);
        RBTreeDelete (gWssUserGlobals.WssUserNameAccessListTable);
        RBTreeDelete (gWssUserGlobals.WssUserMacAccessListTable);
        RBTreeDelete (gWssUserGlobals.WssUserMappingListTable);
        RBTreeDelete (gWssUserGlobals.WssUserSessionTable);
        WSSUSER_INIT_COMPLETE (OSIX_FAILURE);
        return WSSUSER_FAILURE;
    }

    /* Create a Semphore for Protocol Lock */

    if (USER_CREATE_SEMAPHORE (USER_PROTOCOL_SEMAPHORE, USER_BINARY_SEM, USER_SEM_FLAGS,
                             &(gWssUserGlobals.UserProtocolSemId)) != OSIX_SUCCESS)
    {

        WSSUSER_TRC (WSSUSER_INIT_SHUT_TRC | WSSUSER_CONTROL_PATH_TRC|
                     WSSUSER_ALL_FAILURE_TRC, "User Task Failed :- Semaphore Creation Failed \n");

        WssuserSizingMemDeleteMemPools ();
        RBTreeDelete (gWssUserGlobals.WssUserGroupEntryTable);
        RBTreeDelete (gWssUserGlobals.WssUserRoleEntryTable);
        RBTreeDelete (gWssUserGlobals.WssUserNameAccessListTable);
        RBTreeDelete (gWssUserGlobals.WssUserMacAccessListTable);
        RBTreeDelete (gWssUserGlobals.WssUserMappingListTable);
        RBTreeDelete (gWssUserGlobals.WssUserSessionTable);
        RBTreeDelete (gWssUserGlobals.WssUserNameTable);
        WSSUSER_INIT_COMPLETE (OSIX_FAILURE);
        return WSSUSER_FAILURE;
    }

    /* Register User Role Management MIB with SNMP Agent */
    RegisterFSWSSU ();


    if (OsixQueCrt ((UINT1 *) WSSUSER_TASK_QUEUE_NAME, OSIX_MAX_Q_MSG_LEN,
                WSSUSER_TASK_QUEUE_DEPTH,
                &(WSSUSER_TASK_QUEUE_ID)) == OSIX_FAILURE)
    {
        WSSUSER_TRC (WSSUSER_ALL_FAILURE_TRC, "WssUserInit: Osix Queue Crt Failed!\n");
        WssuserSizingMemDeleteMemPools ();
        RBTreeDelete (gWssUserGlobals.WssUserGroupEntryTable);
        RBTreeDelete (gWssUserGlobals.WssUserRoleEntryTable);
        RBTreeDelete (gWssUserGlobals.WssUserNameAccessListTable);
        RBTreeDelete (gWssUserGlobals.WssUserMacAccessListTable);
        RBTreeDelete (gWssUserGlobals.WssUserMappingListTable);
        RBTreeDelete (gWssUserGlobals.WssUserSessionTable);
        RBTreeDelete (gWssUserGlobals.WssUserNameTable);
        WSSUSER_INIT_COMPLETE (OSIX_FAILURE);
        UnRegisterFSWSSU();
        return WSSUSER_FAILURE;
    }

 
    /* Set the default module status of User Role Module */
    gWssUserGlobals.i4UserRoleStatus = WSSUSER_DEFAULT_MODULE_STATUS;
    gWssUserGlobals.u4UserTrapStatus= WSSUSER_DEFAULT_TRAP_STATUS;
    /* Create & Define default User Group and its attribute */
    WssUserCreateDefaultUserGroup (); 



#ifdef SYSLOG_WANTED
    /* Create SYS_LOG Handler for logging CDR Records */
    gWssUserGlobals.u4CDRSysLogId = (UINT4) (SYS_LOG_REGISTER ((CONST UINT1 *) "CDR", SYSLOG_INFO_LEVEL));
#endif



    WSSUSER_TRC (WSSUSER_INIT_SHUT_TRC | WSSUSER_CONTROL_PATH_TRC,
                "WSSUSER Module Initialization completed ...\n");

    /* Inform the sucessful Task initialization to LR Module */
    WSSUSER_INIT_COMPLETE (OSIX_SUCCESS);


    /* Waiting in a blocking, continuous call in order to receive a events to 
     * User Role Module task.
     */

    while (1)
    {
        u4Events = 0;

        if (OsixEvtRecv (gWssUserGlobals.UserTaskId, 
                         WSSUSER_TIMER_EXP_EVENT | WSSUSER_QUEUE_EVENT, 
                         OSIX_WAIT, &u4Events)  == OSIX_SUCCESS)
        {
            UserLock();
            if (u4Events & WSSUSER_TIMER_EXP_EVENT)
            {
               UserTmrProcessTimer();
            }
            if (u4Events & WSSUSER_QUEUE_EVENT)
            {
                WssUserInterfaceHandleQueueEvent ();
            }
            UserUnLock();
        }
    }                            /* End of While */

    return WSSUSER_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : WssUserCreateDefaultUserGroup                        */
/*                                                                           */
/* Description        : This function creates a default User Group to the    */
/*                      User Group Table with the default Group id           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID WssUserCreateDefaultUserGroup (VOID)
{
    tUserGroupEntry         *pUserGroupEntry = NULL;

    /* Create Memory for User Group Node */
    pUserGroupEntry = WSSUSER_MEM_ALLOCATE_MEM_BLK (WSSUSER_GROUP_INFO_MEMPOOL_ID);

    if (pUserGroupEntry == NULL)
    {
        WSSUSER_TRC (WSSUSER_INIT_SHUT_TRC | WSSUSER_CONTROL_PATH_TRC,
                     "Memory Alloc failure in WssUserCreateDefaultUserGroup () ... \n");
        return;
    }


    /* Assign Default Values of User Group Attributes */
    MEMSET (pUserGroupEntry, 0, sizeof (tUserGroupEntry));

    /* Assign Group Id */
    pUserGroupEntry->u4GroupId     =  WSSUSER_DEFAULT_GROUP_ID;

   
    /* Assign Group Name */
    MEMCPY (pUserGroupEntry->au1GroupName, WSSUSER_DEFAULT_GROUP_NAME, STRLEN(WSSUSER_DEFAULT_GROUP_NAME));
    pUserGroupEntry->u4UserGroupLen = STRLEN(WSSUSER_DEFAULT_GROUP_NAME);

    /* Assign default values to the User Group attributes */
    pUserGroupEntry->u4Bandwidth   = WSSUSER_DEFAULT_BANDWIDTH;
    pUserGroupEntry->u4DLBandwidth   = WSSUSER_DEFAULT_BANDWIDTH;
    pUserGroupEntry->u4ULBandwidth   = WSSUSER_DEFAULT_BANDWIDTH;
    pUserGroupEntry->u4Volume      = WSSUSER_DEFAULT_VOLUME;
    pUserGroupEntry->u4Time        = WSSUSER_DEFAULT_TIME;
    pUserGroupEntry->i4RowStatus   = ACTIVE;


    /* Add/Update into the User Group Datastructure */
    if (RBTreeAdd (gWssUserGlobals.WssUserGroupEntryTable, 
                  (tRBElem *) pUserGroupEntry) == RB_FAILURE)
    {
        WSSUSER_TRC (WSSUSER_INIT_SHUT_TRC | WSSUSER_CONTROL_PATH_TRC,
                     "RBTreeAdd  failure in WssUserCreateDefaultUserGroup () ... \n");
        return;
    }

    return;
}


/*****************************************************************************/
/* Function Name      : WssUserEnQFrameToWssUserTask                         */
/*                                                                           */
/* Description        : This enqueues a received frame to User Interface     */
/*                      Queue and sends an event WSSUSER_QUEUE_EVENT to the  */
/*                      Interface task.                                      */
/*                                                                           */
/* Input(s)           : pMsg :-  points to WssUserNotifyParams               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
WssUserEnQFrameToWssUserTask (tWssUserNotifyParams * pMsg)
{
    /* Sending/put the message to the User Role Module into the Queue */
    if (OsixQueSend (WSSUSER_TASK_QUEUE_ID, (UINT1 *) &pMsg,
                     OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        WSSUSER_TRC (WSSUSER_ALL_FAILURE_TRC, 
                     "WssUserEnQFrameToWssUserTask: Osix Queue Send Failed!\n");
        return;
    }

    /* Trigger a WSSUSER_QUEUE_EVENT, event to the User Role Task, to process
     * message placed in the queue */
    if (OsixEvtSend (WSSUSER_TASK_ID, WSSUSER_QUEUE_EVENT) == OSIX_FAILURE)
    {
        WSSUSER_TRC (WSSUSER_ALL_FAILURE_TRC, 
                     "WssUserEnQFrameToWssUserTask: Osix Event Send Failed!\n");
        return;
    }

    return;
}


/*****************************************************************************/
/* Function Name      : WssUserInterfaceHandleQueueEvent                     */
/*                                                                           */
/* Description        : This Initiates the Action for the Queue Event. It    */
/*                      receives the information from the buffer received    */
/*                      from the Queue and processes it                      */ 
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
WssUserInterfaceHandleQueueEvent (VOID)
{
    tWssUserNotifyParams *pMsg = NULL;
    tUserSessionEntry UserSessionEntry;

    /* Receiving the packets received in queue */
    while (OsixQueRecv (WSSUSER_TASK_QUEUE_ID,
                        (UINT1 *) &pMsg,
                        OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
    {

        if (pMsg == NULL)
        {
            continue;
        }
	switch (pMsg->eWssUserNotifyType)
	{
            /* Copying the incoming attributes in User Session DB and 
             * adding the same table in RBTree*/
	    case WSSUSER_SESSION_ADD:
                MEMSET (&UserSessionEntry, 0, sizeof (tUserSessionEntry));

		UserSessionEntry.u4AllotedBandwidth = pMsg->u4Bandwidth;
		UserSessionEntry.u4AllotedULBandwidth = pMsg->u4ULBandwidth;
		UserSessionEntry.u4AllotedDLBandwidth = pMsg->u4DLBandwidth;
		UserSessionEntry.u4AllotedVolume = pMsg->u4Volume;
		UserSessionEntry.u4AllotedTime= pMsg->u4Time;
		MEMCPY(UserSessionEntry.staMac,pMsg->staMac,MAC_ADDR_LEN);
                UserSessionEntry.u4UserNameLen = STRLEN (pMsg->au1UserName);
		MEMCPY(UserSessionEntry.au1UserName,pMsg->au1UserName, UserSessionEntry.u4UserNameLen);
		UserSessionEntry.i4NasPort = pMsg->i4NasPort;
		if(WssUserAddSessionEntry(&UserSessionEntry) != WSSUSER_FAILURE)
		{
		    if(pMsg->u1EAPType == PRO_PAP)
		    {
			if(WssStaSendBandWidth(pMsg->staMac,pMsg->u4Bandwidth,
                        pMsg->u4DLBandwidth, pMsg->u4ULBandwidth) 
                                        == OSIX_FAILURE)
                       {
                           WSSUSER_TRC (WSSUSER_ALL_FAILURE_TRC,
                                        "WssUserInterfaceHandleQueueEvent:" 
                                         "WssStaSendBandWidth returning faillure!\n");
                       }  
                    }
                }
		break;
            /* Deleting User SessionEntryTable from RBTree*/
	    case WSSUSER_SESSION_DELETE:
		WssUserDeleteSessionEntry (pMsg->staMac,pMsg->u4TerminateCause);
		break;
	    case WSSUSER_VOLUME_CONSUMED_IND:
		WssUserProcessVolConsumedInd (pMsg->staMac, pMsg->f4TxBytes, pMsg->f4RxBytes);
		break;
	    default:
		WSSUSER_TRC_ARG1 (WSSUSER_ALL_FAILURE_TRC,
			"WssUserInterfaceHandleQueueEvent:- "
			"Invalid Message Type %d\n",
			pMsg->eWssUserNotifyType);
		break;
	}
	MemReleaseMemBlock (WSSUSER_NOTIFY_INFO_MEMPOOL_ID, (UINT1 *) pMsg);
    }
}

/*****************************************************************************/
/* Function Name      : WssUserAssignMempoolIds                              */
/*                                                                           */
/* Description        : This function assign mempools from sz.c ie.          */
/*                      WssUserMemPoolIds to global mempoolIds.              */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID WssUserAssignMempoolIds (VOID)
{

    /* tUserGroupEntry */
    WSSUSER_GROUP_INFO_MEMPOOL_ID = WSSUSERMemPoolIds[MAX_WSS_USERGROUP_INFO_SIZING_ID];

    /* tUserRoleEntry */
    WSSUSER_ROLE_INFO_MEMPOOL_ID = WSSUSERMemPoolIds[MAX_WSS_USERROLE_INFO_SIZING_ID];

    /* tUserNameAccessListEntry */
    WSSUSER_NAME_ACC_LIST_INFO_MEMPOOL_ID = WSSUSERMemPoolIds[MAX_WSS_USERNAME_ACC_LIST_INFO_SIZING_ID];

    /* tUserMacAccessListEntry */
    WSSUSER_MAC_ACC_LIST_INFO_MEMPOOL_ID = WSSUSERMemPoolIds[MAX_WSS_USERMAC_ACC_LIST_INFO_SIZING_ID];

    /* tUserMappingListEntry */
    WSSUSER_MAPPING_LIST_INFO_MEMPOOL_ID = WSSUSERMemPoolIds[MAX_WSS_USERMAPPING_LIST_INFO_SIZING_ID];

    /* tUserSessionEntry */
    WSSUSER_SESSION_INFO_MEMPOOL_ID = WSSUSERMemPoolIds[MAX_WSS_USERSESSION_INFO_SIZING_ID];

    /* tWssUserNotifyParams */
    WSSUSER_NOTIFY_INFO_MEMPOOL_ID = WSSUSERMemPoolIds[MAX_WSS_USER_NOTIFY_INFO_SIZING_ID];

    /* tUserNameEntry */
    WSSUSER_NAME_INFO_MEMPOOL_ID = WSSUSERMemPoolIds[MAX_WSS_USERNAME_INFO_SIZING_ID];
}

#endif /* _USERINIT_C */
