/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: usertmr.c,v 1.1.1.1 2016/09/20 10:42:41 siva Exp $
*
* Description: USER File.
*********************************************************************/
#include "userinc.h"


extern UINT4        gu4Stups;

PRIVATE VOID        (*gaUserTimerRoutines[]) (VOID *) =
{
NULL,
WssUserTmrProcSessTimer,
WssUserProcInterimUpdateTmr};

/****************************************************************************
*                                                                           *
* Function     : WssUserTmrInit                                                *
*                                                                           *
* Description  : Function to create user timer list                         *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : WSSUSER_SUCCESS/WSSUSER_FAILURE                                  *
*                                                                           *
*****************************************************************************/
UINT4
WssUserTmrInit (VOID)
{

    if (TmrCreateTimerList (WSSUSER_TASK_NAME, WSSUSER_TIMER_EXP_EVENT,
                            NULL,
                            (tTimerListId) & gWssUserGlobals.u4UserTimerId) ==
        TMR_FAILURE)
    {
        WSSUSER_TRC (WSSUSER_INIT_SHUT_TRC | WSSUSER_ALL_FAILURE_TRC |
                  WSSUSER_OS_RESOURCE_TRC | WSSUSER_CONTROL_PATH_TRC,
                  " TMR: Timer list creation failed\n");
        return (WSSUSER_FAILURE);
    }

    return (WSSUSER_SUCCESS);
}


/****************************************************************************
*                                                                           *
* Function     : WssUserTmrStartTimer                                          *
*                                                                           *
* Description  : Function to start the timer                                *
*                                                                           *
* Input        : pUserTimer :- Points to the user timer                     *
*                TimerId    :- Timer Id                                     *
*                u4Timeout  :- The valu for which the timer to be started   *
*                pTmrContext :- Refers to the context that will be used     *
*                               for processing the timer                    *
* Output       : None                                                       *
*                                                                           *
* Returns      : USER_SUCCESS/USER_FAILURE                                  *
*                                                                           *
*****************************************************************************/
UINT4
WssUserTmrStartTimer (tUserTimer * pUserTimer, eUserTimerId TimerId,
                   UINT4 u4TimeOut, VOID *pTmrContext)
{

    if (USER_IS_TIMER_ACTIVE (pUserTimer) == TRUE)
    {
        WSSUSER_TRC (DATA_PATH_TRC, "UserTmrStartTimer:Attempt to start"
                  "already running timer\n");
        return WSSUSER_FAILURE;
    }

    USER_ACTIVATE_TIMER (pUserTimer);
    pUserTimer->u1TimerId = (UINT1) TimerId;
    pUserTimer->pContext = pTmrContext;
    pUserTimer->Node.u4Data = (FS_ULONG) pUserTimer;

    switch (pUserTimer->u1TimerId)
    {

        case  USER_SESS_TIMER_ID:
              break;
        case  USER_INTERIM_UPDATE_TIMER_ID:
              break;
        default:
        WSSUSER_TRC (WSSUSER_ALL_FAILURE_TRC, "WssUserTmrStartTimer: Timer id invalid\n");
              return (WSSUSER_FAILURE);
   
    } 


    if (TmrStartTimer
        ((tTimerListId) gWssUserGlobals.u4UserTimerId, &(pUserTimer->Node),
         (u4TimeOut * SYS_NUM_OF_TIME_UNITS_IN_A_SEC)) == TMR_SUCCESS)
    {
        return (WSSUSER_SUCCESS);
    }
    else
    {
        return (WSSUSER_FAILURE);
    }
}

/****************************************************************************
*                                                                           *
* Function     : WssUserTmrStopTimer                                           *
*                                                                           *
* Description  : Function invoked to stop timer                             *
*                                                                           *
* Input        : pUserTimer :- Points to the timer to be stopped            *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : USER_SUCCESS/USER_FAILURE                                  *
*                                                                           *
*****************************************************************************/
UINT4
WssUserTmrStopTimer (tUserTimer * pUserTimer)
{
    if (USER_IS_TIMER_ACTIVE (pUserTimer) == TRUE)
    {
        if (TmrStopTimer
            ((tTimerListId) gWssUserGlobals.u4UserTimerId,
             &(pUserTimer->Node)) == TMR_FAILURE)
        {
            WSSUSER_TRC (DATA_PATH_TRC, "UserTmrStopTimer:TmrStopTimer failed!\n");
            return WSSUSER_FAILURE;
        }
        USER_INIT_TIMER (pUserTimer);
    }
    return WSSUSER_SUCCESS;
}


/****************************************************************************
*                                                                           *
* Function     : UserTmrProcessTimer *
*                                                                           *
* Description  : Function invoked to process the expired timers             *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*****************************************************************************/
VOID
UserTmrProcessTimer (VOID)
{
    tUserTimer         *pUserTimer = NULL;
    tTmrAppTimer       *pExpTimer = NULL;

    pExpTimer =
        TmrGetNextExpiredTimer (gWssUserGlobals.u4UserTimerId);
    while (pExpTimer != NULL)
    {
        pUserTimer = (tUserTimer*) pExpTimer->u4Data;

        if (pUserTimer != NULL)
        {
            USER_INIT_TIMER (pUserTimer);
            if ((pUserTimer->u1TimerId > USER_MIN_TIMER_ID) &&
                (pUserTimer->u1TimerId < USER_MAX_TIMER_ID))
            {
                (*gaUserTimerRoutines[pUserTimer->u1TimerId])
                    (pUserTimer->pContext);
            }
        }
        pExpTimer =
            TmrGetNextExpiredTimer (gWssUserGlobals.u4UserTimerId);
    }
}


/****************************************************************************
*                                                                           *
* Function     : UserTmrProcRetransTimer                                    *
*                                                                           *
* Description  : Function invoked when user session timer expires           *
*                                                                           *
* Input        : pTmrContext :- Context that will be used for processing    *
*                the session timer                                          *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : USER_SUCCESS/USER_FAILURE                                  *
*                                                                           *
*****************************************************************************/
VOID
WssUserTmrProcSessTimer (VOID *pTmrContext)
{

    tUserNameEntry *pUserNameEntry= NULL;
    tWlanUserTrapInfo     UserTrapInfo;
    UINT4           u4TerminateCause = 0;
    UINT4           u4RemainTime = 0;

    MEMSET (&UserTrapInfo, 0,  sizeof(tWlanUserTrapInfo));

    pUserNameEntry = (tUserNameEntry *) pTmrContext;

   /* Check to verify whether the timer is started for Total Alloted Time or for Maximum Timer Value */
   if (pUserNameEntry->u4AllotedTime > pUserNameEntry->u4ConsumedTime)
   {
        /* Timer is started for the Maximum Timer Limit, Check for the remaing Timer Value*/

 	u4RemainTime = pUserNameEntry->u4AllotedTime - pUserNameEntry->u4ConsumedTime;	

        /* If again the remaining value is greater than Maximum Timer Limit, then start the timer 
         * for WSSUSER_MAX_SESS_TIMER_VALUE else start the timer for the remaining value*/

        if (u4RemainTime > WSSUSER_MAX_SESS_TIMER_VALUE)
        {
                 pUserNameEntry->u4ConsumedTime += WSSUSER_MAX_SESS_TIMER_VALUE;
                 /* Restarting the session timer for the Maximum Value*/
                if (WssUserTmrStartTimer (&(pUserNameEntry->UserSessTmr),
                                            USER_SESS_TIMER_ID,
                                            WSSUSER_MAX_SESS_TIMER_VALUE,
                                            pUserNameEntry) != WSSUSER_SUCCESS)
               {
                        WSSUSER_TRC (WSSUSER_ALL_FAILURE_TRC, "WssUserTmrProcSessTimer:\
                                     Start timer Failed\n");
                return;
               }

        }
        else 
        {
                pUserNameEntry->u4ConsumedTime += u4RemainTime;
                /* Re-Starting the session timer for the remaining Time value*/
                if (WssUserTmrStartTimer (&(pUserNameEntry->UserSessTmr),
                                            USER_SESS_TIMER_ID,
                                            u4RemainTime,
                                            pUserNameEntry) != WSSUSER_SUCCESS)
               {
                        WSSUSER_TRC (WSSUSER_ALL_FAILURE_TRC, "WssUserTmrProcSessTimer:\
                                     Start timer Failed\n");
                return;
               }

        }
   }
   else
   {
      /*USER ROLE TIME EXCEEDED TRAP*/
        MEMCPY (UserTrapInfo.au1UserName, pUserNameEntry->au1UserName,
                pUserNameEntry->u4UserNameLen);
        UserTrapInfo.u4UsedTime = pUserNameEntry->u4AllotedTime;
        UserTrapInfo.u4WlanId = pUserNameEntry->u4WlanIndex;
        WssUserSnmpSendTrap(WSSUSER_TIME_EXCEEDED, &UserTrapInfo);
 
    /* If the Alloted Time and Consumed Time is equal, then send the Deauth message to the user*/
    u4TerminateCause = SESSION_TIMEOUT;
    WssUserDeAuthenticateUser (pUserNameEntry->au1UserName,pUserNameEntry->u4WlanIndex,u4TerminateCause);
   }

    return;
}

/****************************************************************************
*                                                                           *
* Function     : WssUserProcInterimUpdateTmr                                *
*                                                                           *
* Description  : Function invoked when user interim timer expires           *
*                                                                           *
* Input        : pTmrContext :- Context that will be used for processing    *
*                the session timer                                          *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : USER_SUCCESS/USER_FAILURE                                  *
*                                                                           *
*****************************************************************************/
VOID
WssUserProcInterimUpdateTmr (VOID *pTmrContext)
{

    tUserNameEntry       *pUserNameEntry = NULL;
    UINT4                u4CurrentTime = 0;
    UINT4                u4ElapsedTime = 0;


    pUserNameEntry = (tUserNameEntry *) pTmrContext;


     /* Get the SysUpTime to compute the duration of the session */
     u4CurrentTime = OsixGetSysUpTime ();
     u4ElapsedTime = (u4CurrentTime - (pUserNameEntry->u4StartTime));
     pUserNameEntry->u4UsedTime = u4ElapsedTime;


     /* Send a RADIUS Accounting - STOP message */
     WssUserUtilRadAccounting (pUserNameEntry, ACCT_INTERIM_UPDATE);
     MEMSET (&pUserNameEntry->UserInterimUpdateTmr, 0, sizeof(tUserTimer));
     if (WssUserTmrStartTimer ((&(pUserNameEntry->UserInterimUpdateTmr)),
                               USER_INTERIM_UPDATE_TIMER_ID,
                               USER_INTERIM_UPDATE_TIMOUT_VAL,
                                pUserNameEntry) != WSSUSER_SUCCESS)
  {
        WSSUSER_TRC (WSSUSER_ALL_FAILURE_TRC, "WssUserProcInterimUpdateTmr:\
                     Interim Update timer Failed\n");
        return;
   }

    return;
}
