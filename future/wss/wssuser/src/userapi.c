/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: userapi.c,v 1.1.1.1 2016/09/20 10:42:41 siva Exp $
*
* Description: RSNA File.
*********************************************************************/

#include "userinc.h"

#if defined (WLC_WANTED) && defined (LNXIP4_WANTED)
extern UINT1       *CfaGddGetLnxIntfnameForPort (UINT2 u2Index);
#endif


/*****************************************************************************
 *                                                                           *
 * Function     : WssUserRoleModuleStatus                                    *
 *                                                                           *
 * Description  : This function is returns User-Role Module Status to        *
 *                External modules                                           *
 *                                                                           *
 * Input        : NONE                                                       *
 *                                                                           *
 * Output       : NONE                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 *                                                                           *
 *****************************************************************************/
UINT1 WssUserRoleModuleStatus(VOID)
{
    UserLock();
    if (gWssUserGlobals.i4UserRoleStatus == WSSUSER_MODULE_ENABLED)
    {
        UserUnLock();
        return OSIX_SUCCESS;
    }
    else if (gWssUserGlobals.i4UserRoleStatus == WSSUSER_MODULE_DISABLED)
    {
        UserUnLock();
        return OSIX_FAILURE;
    }
    UserUnLock();
    return OSIX_FAILURE;

}

/*****************************************************************************
 *                                                                           *
 * Function     : WssUserProcessRestrictedUser                               *
 *                                                                           *
 * Description  : This function is invoked before sending Access Request to  *
 *                Radius server, to restrict the user based on the UserName/ *
 *                MacAddress if the UserName/MacAddress is already           *
 *                configured in restricted list                              *
 *                                                                           *
 * Input        : staMacAddr   - Station Mac Address                         *
 *                pu1Username  - User Name                                   *
 * Output       : pbRestricted - OSIX_TRUE - if the user is restricted       *
 *                             - OSIX_FALSE - if the user is not restricted  *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
VOID
WssUserProcessRestrictedUser (tMacAddr staMacAddr, UINT1 *pu1Username, BOOL1 *pbRestricted)
{
    UINT4    u4TerminateCause = 0; 
    UserLock();
    u4TerminateCause = SERVICE_UNAVAILABLE;
    if (gWssUserGlobals.i4UserRoleStatus == WSSUSER_MODULE_ENABLED)
    {
        /* Check whether we can allow the user based on the Mac Address*/
        if (WssUserGetRestrictStaMac (staMacAddr) == WSSUSER_SUCCESS)
        {
            /* MAC entry present, so returning failure*/
            WSSUSER_TRC (WSSUSER_ALL_FAILURE_TRC, 
                         "WssUserIsRestrictedUser: Mac address is restricted!\n");
            WssUserDeAuthenticateStation (staMacAddr,OSIX_FALSE,u4TerminateCause);
            gWssUserGlobals.u4BlockedCount++;
            *pbRestricted = OSIX_TRUE;   /* User is restricted */
            UserUnLock();
            return;
        }

        /* Check whether we can allow the user Based on the User Name*/
        if (WssUserGetRestrictUserName (pu1Username) == WSSUSER_SUCCESS)
        {
            /* MAC entry present, so returning failure*/
            WSSUSER_TRC (WSSUSER_ALL_FAILURE_TRC,
                         "WssUserIsRestrictedUser: User Name is restricted!\n");
            WssUserDeAuthenticateStation (staMacAddr,OSIX_FALSE,u4TerminateCause);
            gWssUserGlobals.u4BlockedCount++;
            *pbRestricted = OSIX_TRUE;   /* User is restricted */
            UserUnLock();
            return;
        }
    
        /* Check whether we can allow the user Based on the Mac Mapping entry*/
        if (WssUserGetMacMappingEntry (pu1Username,
                              staMacAddr) != WSSUSER_SUCCESS)
        {
             /* MAC entry present, so returning failure*/
             WSSUSER_TRC (WSSUSER_ALL_FAILURE_TRC, 
                          "WssUserIsRestrictedUser: User Name/Mac mapping is not present!\n");
             WssUserDeAuthenticateStation (staMacAddr, OSIX_FALSE,u4TerminateCause);
             gWssUserGlobals.u4BlockedCount++;
            *pbRestricted = OSIX_TRUE;   /* User is restricted */
             UserUnLock();
             return;
        }

        /* This function is used in the case of Web-Auth.
         * If multiple users is trying to log-in from the same station at the same time,
         * connection should not be allowed. If so, this funtoin will return failure
         */
        if (WssUserRestrictMultipleUserLogin (pu1Username,
                              staMacAddr) != WSSUSER_SUCCESS)
        {
             WSSUSER_TRC (WSSUSER_ALL_FAILURE_TRC, 
                          "WssUserProcessRestrictedUser: Already an user is connected through this station!\n");
             *pbRestricted = OSIX_TRUE;
             UserUnLock();
             return;
        }
    }

    UserUnLock();

    return;
}
/****************************************************************************
*                                                                           *
* Function     : WssUserProcessNotification                                 *
*                                                                           *
* Description  : Function to process on the notification                    *
*                received from WSSIF/WSSUSER modules                        *
*                                                                           *
* Input        : pWssUserNotifyParams:- Pointer to tWssUserNotifyParams     *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*****************************************************************************/
UINT4
WssUserProcessNotification (tWssUserNotifyParams *pWssUserNotifyParams)
{
    tWssUserNotifyParams *pMsg = NULL;
    
    if (gWssUserGlobals.i4UserRoleStatus == WSSUSER_MODULE_DISABLED)
    {
	
       return WSSUSER_SUCCESS;
    }

    if ((pMsg = (tWssUserNotifyParams *) WSSUSER_MEM_ALLOCATE_MEM_BLK 
		(WSSUSER_NOTIFY_INFO_MEMPOOL_ID)) == NULL)

    {
	WSSUSER_TRC (WSSUSER_ALL_FAILURE_TRC, "WssUserProcessNotification: "
                     "WSSUSER_MEM_ALLOCATE_MEM_BLK Failed \n");
	return WSSUSER_FAILURE;
    }

    MEMCPY (pMsg,pWssUserNotifyParams, sizeof (tWssUserNotifyParams));

    switch (pMsg->eWssUserNotifyType)
    {

        /*******************************************************
         *              Indication from WSS --> WSSUSER 
         *****************************************************/
        
        case WSSUSER_SESSION_ADD:
        case WSSUSER_SESSION_DELETE:
        case WSSUSER_VOLUME_CONSUMED_IND:
            /* The above cases falls under the category of WSS --> 
             * WSSUSER  messages Since it is called by an external WSS  
             * module,posting to the
             * WSS user  queue is required */
            WssUserEnQFrameToWssUserTask (pMsg);
            break;
	    default:
		    break;
      }
      /********************************************************
       *              Indication from WSSUSER --> WSS
       ********************************************************/

    return WSSUSER_SUCCESS;
}
/****************************************************************************
*                                                                           *
* Function     : WssUserGetSessionAttributes                                *
*                                                                           *
* Description  : This function retirves the session attributes (User Name   *
*                and Volume) by acccepting station mac address as input     *
*                                                                           *
* Input        : staMacAddr - Station Mac Address                           *
*                                                                           *
* Output       : pu1UserName - User Name                                    *
*                pu4UsedVolume - Used Volume                                *
*                                                                           *
* Returns      : WSSUSER_SUCCESS/WSSUSER_FAILURE                            *
*                                                                           *
*****************************************************************************/

UINT4
WssUserGetSessionAttributes (tMacAddr staMacAddr, UINT1 *pu1UserName, 
		UINT4 *pu4UsedVolume, UINT4 *pu4BandWidth, UINT4 *pu4DLBandWidth,
		UINT4 *pu4ULBandWidth)
{
    UserLock();

    if (gWssUserGlobals.i4UserRoleStatus == WSSUSER_MODULE_DISABLED)
    {
       /* Since the User Role Module is DISABLED, set the consumed volume as 0 
        * and return success
        */
        WSSUSER_TRC (WSSUSER_MGMT_TRC, "WssUserGetSessionAttributes: "
                     "USER-ROLE was not enabled \n");
       
       *pu4UsedVolume = 0;

       UserUnLock ();
       return WSSUSER_FAILURE;
    }

  
    if (WssUserUtilGetAttributes (staMacAddr, pu1UserName, 
		pu4UsedVolume, pu4BandWidth, pu4DLBandWidth, pu4ULBandWidth) 
	    != WSSUSER_SUCCESS)
   {
        WSSUSER_TRC (WSSUSER_ALL_FAILURE_TRC, "WssUserGetSessionAttributes: "
                     "User sessoin was not present \n");
        UserUnLock();
        return WSSUSER_FAILURE;
    } 

    UserUnLock();

    return WSSUSER_SUCCESS;
}
/****************************************************************************
*                                                                           *
* Function     : WssUserGetSessionEntry                                *
*                                                                           *
* Description  : This function retirves the session attributes (User Name   *
*                and Volume) by acccepting station mac address as input     *
*                                                                           *
* Input        : staMacAddr - Station Mac Address                           *
*                                                                           *
* Output       : pu1UserName - User Name                                    *
*                pu4UsedVolume - Used Volume                                *
*                                                                           *
* Returns      : WSSUSER_SUCCESS/WSSUSER_FAILURE                            *
*                                                                           *
*****************************************************************************/

UINT4
WssUserGetSessionEntry (tMacAddr staMacAddr)
{
    tUserSessionEntry   *pUserSessionEntryExist = NULL;
    UINT1                u1EntryFound = OSIX_FALSE;

    UserLock();

    if (gWssUserGlobals.i4UserRoleStatus == WSSUSER_MODULE_ENABLED)
    {
       /* Since the User Role Module is DISABLED, set the consumed volume as 0 
        * and return success
        */

    pUserSessionEntryExist = (tUserSessionEntry *)RBTreeGetFirst (gWssUserGlobals.WssUserSessionTable);

    /* Searching the User Session entry corresponding to the station */
    while (pUserSessionEntryExist != NULL)
    {
    if (MEMCMP (pUserSessionEntryExist->staMac, staMacAddr, MAC_ADDR_LEN) ==  0)
    {
        u1EntryFound = OSIX_TRUE;
        break;
    }
    pUserSessionEntryExist = (tUserSessionEntry *)RBTreeGetNext (gWssUserGlobals.
        WssUserSessionTable ,(tRBElem *)pUserSessionEntryExist, NULL);
    }

    /* Once the entry found Stop the timer and delete the entry */
    if (u1EntryFound == OSIX_TRUE)
    {
         
       UserUnLock ();
       return WSSUSER_SUCCESS;
    }


    }
    UserUnLock();

    return WSSUSER_FAILURE;
}

#if defined (WLC_WANTED) && defined (LNXIP4_WANTED)
/****************************************************************************
*                                                                           *
* Function     : WssUserSetBandWidth                                        *
*                                                                           *
* Description  : This function invokes to configure the Bandwidth in WLC    *
*                                                                           *
* Input        : staMacAddr - Station Mac Address                           *
*                u4BandWidth - Bandwidth for the station                    *
*                u1DeleteFlag - Delete Flag                                 *
*                                                                           *
* Output       : -                                                          *
*                                                                           *
* Returns      : WSSUSER_SUCCESS/WSSUSER_FAILURE                            *
*                                                                           *
*****************************************************************************/

UINT1
WssUserSetBandWidth(tMacAddr StationMacAddr, UINT4 u4BandWidth,
                UINT4 u4DLBandWidth, UINT4 u4ULBandWidth, UINT1 u1DeleteFlag)
{
    tWssIfCapDB      *pWssIfCapwapDB = NULL;
    UINT2             u2Port = 0;
    UINT1             u2VlanId = 1;
    UINT1             u1Status = 0;
    UINT1            *pu1PortName = NULL;

    if(u4BandWidth != 0)
    {
        WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE);
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpMacAddress = OSIX_TRUE;
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpLocalRouting = OSIX_TRUE;

        if( WssIfGetCapwapDB(StationMacAddr,pWssIfCapwapDB) == OSIX_FAILURE)
        {
            WSSUSER_TRC (WSSUSER_ALL_FAILURE_TRC,"WssUserSetBandWidth:"
                    "Failed to get Capwap DB \r\n");
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
        if(pWssIfCapwapDB->CapwapGetDB.u1WtpLocalRouting == LOCAL_ROUTING_ENABLED && u1DeleteFlag == OSIX_FALSE)
        {
            WSSUSER_TRC (WSSUSER_MGMT_TRC,"TC Filter has not installed in Local Routing Case \n");
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return WSSUSER_SUCCESS;
        }
        if (VlanGetFdbEntryDetails (u2VlanId,pWssIfCapwapDB->CapwapGetDB.WtpMacAddress,
                    &u2Port, &u1Status) != SUCCESS)
        {
            WSSUSER_TRC (WSSUSER_ALL_FAILURE_TRC, "Mac Entry does not learnt in the given port\r\n");
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
        pu1PortName = CfaGddGetLnxIntfnameForPort (u2Port);

        if(pu1PortName != NULL)
        {
            if(WSSUserRoleConfigBandwidth(StationMacAddr,
                        u4BandWidth, u4DLBandWidth, u4ULBandWidth,
                        pu1PortName,u1DeleteFlag) != OSIX_SUCCESS)
            {

            }
        }
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    }
    return WSSUSER_SUCCESS;
}
#endif
/************ End of USERAPI.C ************/
