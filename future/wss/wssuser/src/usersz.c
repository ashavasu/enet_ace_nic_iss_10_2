/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: usersz.c,v 1.1.1.1 2016/09/20 10:42:41 siva Exp $
 *
 * Description: This file contains the WSSUSER MemPool Init routines.
 *
 *****************************************************************************/

#define _WSSUSERSZ_C
#include "userinc.h"
extern INT4  IssSzRegisterModuleSizingParams( CHR1 *pu1ModName, tFsModSizingParams *pModSizingParams); 
extern INT4  IssSzRegisterModulePoolId( CHR1 *pu1ModName, tMemPoolId *pModPoolId); 
INT4  WssuserSizingMemCreateMemPools()
{
    UINT4 u4RetVal;
    INT4 i4SizingId;

    for( i4SizingId=0; i4SizingId < WSSUSER_MAX_SIZING_ID; i4SizingId++) {
        u4RetVal = MemCreateMemPool( 
                          FsWSSUSERSizingParams[i4SizingId].u4StructSize,
                          FsWSSUSERSizingParams[i4SizingId].u4PreAllocatedUnits,
                          MEM_DEFAULT_MEMORY_TYPE,
                          &(WSSUSERMemPoolIds[ i4SizingId]));
        if( u4RetVal == MEM_FAILURE) {
            WssuserSizingMemDeleteMemPools();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}


INT4   WssuserSzRegisterModuleSizingParams( CHR1 *pu1ModName)
{
      /* Copy the Module Name */ 
       IssSzRegisterModuleSizingParams( pu1ModName, FsWSSUSERSizingParams); 
      IssSzRegisterModulePoolId( pu1ModName, WSSUSERMemPoolIds); 
      return OSIX_SUCCESS; 
}


VOID  WssuserSizingMemDeleteMemPools()
{
    INT4 i4SizingId;

    for( i4SizingId=0; i4SizingId < WSSUSER_MAX_SIZING_ID; i4SizingId++) {
        if(WSSUSERMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool( WSSUSERMemPoolIds[ i4SizingId] );
            WSSUSERMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
