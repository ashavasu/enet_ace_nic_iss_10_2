/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: usermacr.h,v 1.1.1.1 2016/09/20 10:42:41 siva Exp $
*
* Description: WSSUSER File.
*********************************************************************/


#ifndef _USERMACR_H
#define _USERMACR_H

#define MEM_MAX_LEN(len1, len2)       (((len1) >= (len2)) ? (len1) : (len2))

/* WSSUSER task related macros */
#define   WSSUSER_TASK_NAME             ((const UINT1 *)"WUser")
#define   WSSUSER_TASK_QUEUE_NAME        ((UINT1 *)"WUserQ")
#define   WSSUSER_TASK_ID                gWssUserGlobals.UserTaskId
#define   WSSUSER_TASK_QUEUE_ID          gWssUserGlobals.UserTaskQId
#define   WSSUSER_MODULE_STATUS          gWssUserGlobals.i4UserRoleStatus
#define   WSSUSER_TASK_QUEUE_DEPTH       50

/*FSAP related calls*/
#define  WSSUSER_GET_TASK_ID            OsixGetTaskId


#define  WSSUSER_INIT_COMPLETE(u4Status) lrInitComplete(u4Status)

#define  WSSUSER_QUEUE_EVENT           0x01
#define  WSSUSER_TIMER_EXP_EVENT       0x02


#define  MAX_GROUP_NAME_LENGTH          32

#define LEN_SESSION_ID 52
#define WSSUSER_ACCT_START              ACCT_START
#define WSSUSER_ACCT_STOP               ACCT_STOP
#define WSSUSER_ACCT_INTERIM_UPDATE     ACCT_INTERIM_UPDATE
#define WSSUSER_ACCT_MAX_OCTETS     4194304
#define WSSUSER_NAS_IDENTIFIER_STRING             "fsNas1"
#define SECS_IN_MINUTE    60
#define DEFAULT_NAS_IPADDRESS       0x0a000001

#define  WSSUSER_DEFAULT_GROUP_NAME     "Default"
#define  WSSUSER_USER_GROUP_NAME        "User-Group"


#define  WSSUSER_DEFAULT_BANDWIDTH      256
#define  WSSUSER_DEFAULT_VOLUME         0
#define  WSSUSER_DEFAULT_TIME           0

#define  WSSUSER_MIN_VALUE              1
#define  WSSUSER_MAX_VALUE              1024

#define  WSSUSER_MIN_TIME_VALUE         0
#define  WSSUSER_MAX_TIME_VALUE         31536000  /* 365 days */
#define  WSSUSER_MAX_SESS_TIMER_VALUE   604800  /* 1 week as per FSAP max timer value - TODO Need to map with FSAP Macro*/

#define  WSSUSER_MIN_BANDWIDTH_VALUE    0
#define  WSSUSER_MAX_BANDWIDTH_VALUE    4294967295

#define  WSSUSER_MIN_VOLUME_VALUE       0
#define  WSSUSER_MAX_VOLUME_VALUE       4294967295

#define  WSSUSER_1024Kbps 1024
#define  WSSUSER_1024MB 1024

#define  WSSUSER_HIPHEN '-'
#define  WSSUSER_UNDERSCORE '_'

#define WSSUSER_GROUP_INFO_MEMPOOL_ID         gWssUserGlobals.WssUserGroupPoolId
#define WSSUSER_ROLE_INFO_MEMPOOL_ID          gWssUserGlobals.WssUserRolePoolId
#define WSSUSER_NAME_ACC_LIST_INFO_MEMPOOL_ID gWssUserGlobals.WssUserNameAccessListPoolId
#define WSSUSER_MAC_ACC_LIST_INFO_MEMPOOL_ID  gWssUserGlobals.WssUserMacAccessListPoolId
#define WSSUSER_MAPPING_LIST_INFO_MEMPOOL_ID  gWssUserGlobals.WssUserMappingListPoolId
#define WSSUSER_SESSION_INFO_MEMPOOL_ID       gWssUserGlobals.WssUserSessionPoolId
#define WSSUSER_NOTIFY_INFO_MEMPOOL_ID        gWssUserGlobals.WssUserNotifyPoolId
#define WSSUSER_NAME_INFO_MEMPOOL_ID          gWssUserGlobals.WssUserNamePoolId


/* macros for allocating memory block */

#define WSSUSER_MEM_ALLOCATE_MEM_BLK(WSSUSER_MEMPOOL_ID) \
           MemAllocMemBlk(WSSUSER_MEMPOOL_ID)

/*Macros for releasing memory block*/

#define WSSUSER_MEM_RELEASE_MEM_BLK(WSSUSER_MEMPOOL_ID,pNode) \
          MemReleaseMemBlock(WSSUSER_MEMPOOL_ID, \
                             (UINT1 *)pNode)




/* User Role Semaphore related constants */
#define USER_PROTOCOL_SEMAPHORE                   ((const UINT1*)"USEM")
#define USER_BINARY_SEM                           1
#define USER_SEM_FLAGS                            0 /* unused */


#define   USER_CREATE_SEMAPHORE(au1Name, count, flags, psemid) \
             OsixCreateSem((au1Name), (count), (flags), (psemid))

#define   USER_DELETE_SEMAPHORE(semid) OsixSemDel(semid)

#define   USER_TAKE_PROTOCOL_SEMAPHORE(SemId)      \
          {                         \
           if (OsixSemTake(SemId) == OSIX_SUCCESS) \
           {\
           }\
          }

#define   USER_RELEASE_PROTOCOL_SEMAPHORE(SemId)      \
          {  \
                OsixSemGive(SemId);  \
          }

#define   WSSUSER_GREATER  1
#define   WSSUSER_EQUAL    0
#define   WSSUSER_LESSER   -1

#define   WSSUSER_SUCCESS 0
#define   WSSUSER_FAILURE 1
#define   MAC_TO_STRING_LEN 21


#define   WSSUSER_VOLUME_EXCEEDED 1
#define   WSSUSER_TIME_EXCEEDED   2

#define   WSSUSER_TRAP_ENABLE  1
#define   WSSUSER_TRAP_DISABLE 2

#define   WSSUSER_DEFAULT_TRAP_STATUS   WSSUSER_TRAP_DISABLE

#define   MAC_ADDR_LENGTH           6
#define   USER_NAME_LENGTH          32
#define WSSSTA_TRAP_OID_LEN   11


#endif /* _USERMACR_H */
