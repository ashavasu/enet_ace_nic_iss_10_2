/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: userprot.h,v 1.2 2017/05/23 14:16:59 siva Exp $
*
* Description: WSSUSER File.
*********************************************************************/

#ifndef _USERPROT_H
#define _USERPROT_H


INT4 WssUserCompareGroupId (tRBElem*, tRBElem*);
INT4 WssUserRestrictUserNameCmp (tRBElem*, tRBElem*);
INT4 WssUserRestrictUserMacCmp (tRBElem*, tRBElem*);
INT4 WssUserCompareUserNameAndWlanId (tRBElem *, tRBElem*);
INT4 WssUserCompareUserNameAndMac (tRBElem *, tRBElem*);
INT4 WssUserSessNameCmp (tRBElem * e1, tRBElem * e2);
INT4 WssUserNameCmp (tRBElem*, tRBElem*);
VOID WssUserAssignMempoolIds (VOID);
VOID WssUserCreateDefaultUserGroup (VOID);

INT4 WssUserGetRestrictStaMac (tMacAddr staMacAddr);
INT4 WssUserGetRestrictUserName (UINT1 *pu1Username);
INT4 WssUserGetMacMappingEntry (UINT1 *pu1Username, tMacAddr staMacAddr);
INT4 WssUserRestrictMultipleUserLogin (UINT1 *pu1Username,tMacAddr staMacAddr);
INT4 WssUserDeAuthenticateStation  (tMacAddr staMacAddr,BOOL1 bDeleteUser,UINT4 u4TerminateCause);

INT4 WssUserDeleteSessionEntry (tMacAddr staMacAddr, UINT4 u4TerminateCause);
INT4 WssUserAddSessionEntry (tUserSessionEntry *pUserSessionEntry);
INT4 WssUserAddUserNameEntry (tUserSessionEntry *pUserSessionEntry);
INT4 WssUserDeleteUserNameEntry (tUserSessionEntry *pUserSessionEntry,UINT4 u4TermCause);
INT4 WssUserDeAuthenticateUser  (UINT1 *pu1Username, UINT4 u4WlanId, UINT4 u4TerminateCause);

VOID WssUserInterfaceHandleQueueEvent (VOID);

VOID
WssUserEnQFrameToWssUserTask (tWssUserNotifyParams * pMsg);

UINT4
WssUserUtilGetAttributes (tMacAddr staMacAddr, UINT1 *pu1UserName, 
       UINT4 *pu4UsedVolume, UINT4 *pu4BandWidth, 
    UINT4 *pu4DLBandWidth, UINT4 *pu4ULBandWidth);

INT4
WssUserProcessVolConsumedInd (tMacAddr staMacAddr,  FLT4 f4TxBytes, FLT4 f4RxBytes);

VOID
WssUserUtilGetSSIDName (UINT4 u4WlanId, UINT1 *pSSIDName);

VOID WssUserUtilGetAcctSessionID PROTO ((tUserNameEntry *pUserNameEntry));
INT4 WssUserUtilRadAccounting PROTO ((tUserNameEntry *pUserNameEntry, UINT1 u1AcctType ));
UINT4 WssUserUtilGetStationIDs PROTO ((tUserNameEntry  *pUserNameEntry, tRADIUS_INPUT_ACC *pRadInputAcct));
VOID WssUserAcctTraceMessages PROTO ((UINT1 u1AcctType));
VOID WssUserRadResponse(VOID   *);
VOID WssUserSnmpSendTrap PROTO((UINT1 ,VOID *));
VOID WssShowUtilBandwidthDisplay (tCliHandle, UINT4, UINT4, UINT4);
VOID WssShowUtilVolumeDisplay (tCliHandle, UINT4);

VOID WssUserUtilClearRestrictedUserTable (tCliHandle);
VOID WssUserUtilClearRestrictedStationTable (tCliHandle);
VOID WssUserUtilClearUserRoleTable (tCliHandle);
VOID WssUserUtilClearUserGroupTable (tCliHandle);
VOID WssUserUtilDisconnectStation (VOID);
INT1 WssUserCliTrapEnable (tCliHandle);
INT1 WssUserCliTrapDisable (tCliHandle);
VOID WssUserMacToStr (UINT1 *pMacAddr, UINT1 *pu1Temp);

#endif /* _USERPROT_H */
/********** END OF USERPROTO.H *****************/
