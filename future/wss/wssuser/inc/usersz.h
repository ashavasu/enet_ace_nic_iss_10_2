/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 * $Id: usersz.h,v 1.1.1.1 2016/09/20 10:42:41 siva Exp $
 *
 * Description: This file contains WSSUSER Sizing parameters
 *******************************************************************/

enum {
    MAX_WSS_USERGROUP_INFO_SIZING_ID,
    MAX_WSS_USERMAC_ACC_LIST_INFO_SIZING_ID,
    MAX_WSS_USERMAPPING_LIST_INFO_SIZING_ID,
    MAX_WSS_USERNAME_ACC_LIST_INFO_SIZING_ID,
    MAX_WSS_USERNAME_INFO_SIZING_ID,
    MAX_WSS_USER_NOTIFY_INFO_SIZING_ID,
    MAX_WSS_USERROLE_INFO_SIZING_ID,
    MAX_WSS_USERSESSION_INFO_SIZING_ID,
    WSSUSER_MAX_SIZING_ID
};


#ifdef  _WSSUSERSZ_C
tMemPoolId WSSUSERMemPoolIds[ WSSUSER_MAX_SIZING_ID];
INT4  WssuserSizingMemCreateMemPools(VOID);
VOID  WssuserSizingMemDeleteMemPools(VOID);
INT4  WssuserSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _WSSUSERSZ_C  */
extern tMemPoolId WSSUSERMemPoolIds[ ];
extern INT4  WssuserSizingMemCreateMemPools(VOID);
extern VOID  WssuserSizingMemDeleteMemPools(VOID);
#endif /*  _WSSUSERSZ_C  */


#ifdef  _WSSUSERSZ_C
tFsModSizingParams FsWSSUSERSizingParams [] = {
{ "tUserGroupEntry", "MAX_WSS_USERGROUP_INFO", sizeof(tUserGroupEntry),MAX_WSS_USERGROUP_INFO, MAX_WSS_USERGROUP_INFO,0 },
{ "tUserMacAccessListEntry", "MAX_WSS_USERMAC_ACC_LIST_INFO", sizeof(tUserMacAccessListEntry),MAX_WSS_USERMAC_ACC_LIST_INFO, MAX_WSS_USERMAC_ACC_LIST_INFO,0 },
{ "tUserMappingListEntry", "MAX_WSS_USERMAPPING_LIST_INFO", sizeof(tUserMappingListEntry),MAX_WSS_USERMAPPING_LIST_INFO, MAX_WSS_USERMAPPING_LIST_INFO,0 },
{ "tUserNameAccessListEntry", "MAX_WSS_USERNAME_ACC_LIST_INFO", sizeof(tUserNameAccessListEntry),MAX_WSS_USERNAME_ACC_LIST_INFO, MAX_WSS_USERNAME_ACC_LIST_INFO,0 },
{ "tUserNameEntry", "MAX_WSS_USERNAME_INFO", sizeof(tUserNameEntry),MAX_WSS_USERNAME_INFO, MAX_WSS_USERNAME_INFO,0 },
{ "tWssUserNotifyParams", "MAX_WSS_USER_NOTIFY_INFO", sizeof(tWssUserNotifyParams),MAX_WSS_USER_NOTIFY_INFO, MAX_WSS_USER_NOTIFY_INFO,0 },
{ "tUserRoleEntry", "MAX_WSS_USERROLE_INFO", sizeof(tUserRoleEntry),MAX_WSS_USERROLE_INFO, MAX_WSS_USERROLE_INFO,0 },
{ "tUserSessionEntry", "MAX_WSS_USERSESSION_INFO", sizeof(tUserSessionEntry),MAX_WSS_USERSESSION_INFO, MAX_WSS_USERSESSION_INFO,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _WSSUSERSZ_C  */
extern tFsModSizingParams FsWSSUSERSizingParams [];
#endif /*  _WSSUSERSZ_C  */


