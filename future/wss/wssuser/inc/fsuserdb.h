/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsuserdb.h,v 1.1.1.1 2016/09/20 10:42:41 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSWSSUDB_H
#define _FSWSSUDB_H

UINT1 FsWssUserGroupTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsWssUserRoleTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsWssUserNameAccessListTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsWssUserMacAccessListTableINDEX [] = {SNMP_FIXED_LENGTH_OCTET_STRING ,6};
UINT1 FsWssUserMappingTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_FIXED_LENGTH_OCTET_STRING ,6};
UINT1 FsWssUserSessionTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_FIXED_LENGTH_OCTET_STRING ,6};

UINT4 fswssu [] ={1,3,6,1,4,1,29601,2,90};
tSNMP_OID_TYPE fswssuOID = {9, fswssu};


UINT4 FsWssUserRoleStatus [ ] ={1,3,6,1,4,1,29601,2,90,1,1};
UINT4 FsWssUserBlockedCount [ ] ={1,3,6,1,4,1,29601,2,90,1,2};
UINT4 FsWssUserLoggedCount [ ] ={1,3,6,1,4,1,29601,2,90,1,3};
UINT4 FsWssUserTraceOption [ ] ={1,3,6,1,4,1,29601,2,90,1,4};
UINT4 FsWssUserRoleTrapStatus [ ] ={1,3,6,1,4,1,29601,2,90,1,5};
UINT4 FsWssUserGroupId [ ] ={1,3,6,1,4,1,29601,2,90,2,1,1,1};
UINT4 FsWssUserGroupName [ ] ={1,3,6,1,4,1,29601,2,90,2,1,1,2};
UINT4 FsWssUserGroupBandWidth [ ] ={1,3,6,1,4,1,29601,2,90,2,1,1,3};
UINT4 FsWssUserGroupVolume [ ] ={1,3,6,1,4,1,29601,2,90,2,1,1,4};
UINT4 FsWssUserGroupTime [ ] ={1,3,6,1,4,1,29601,2,90,2,1,1,5};
UINT4 FsWssUserGroupRowStatus [ ] ={1,3,6,1,4,1,29601,2,90,2,1,1,6};
UINT4 FsWssUserGroupDLBandWidth [ ] ={1,3,6,1,4,1,29601,2,90,2,1,1,7};
UINT4 FsWssUserGroupULBandWidth [ ] ={1,3,6,1,4,1,29601,2,90,2,1,1,8};
UINT4 FsWssUserRoleName [ ] ={1,3,6,1,4,1,29601,2,90,2,2,1,1};
UINT4 FsWssUserRoleWlanIndex [ ] ={1,3,6,1,4,1,29601,2,90,2,2,1,2};
UINT4 FsWssUserRoleGroupId [ ] ={1,3,6,1,4,1,29601,2,90,2,2,1,3};
UINT4 FsWssUserRoleRowStatus [ ] ={1,3,6,1,4,1,29601,2,90,2,2,1,4};
UINT4 FsWssUserNameAccessListUserName [ ] ={1,3,6,1,4,1,29601,2,90,2,3,1,1};
UINT4 FsWssUserNameAccessListRowStatus [ ] ={1,3,6,1,4,1,29601,2,90,2,3,1,2};
UINT4 FsWssUserMacAccessListStaMac [ ] ={1,3,6,1,4,1,29601,2,90,2,4,1,1};
UINT4 FsWssUserMacAccessListRowStatus [ ] ={1,3,6,1,4,1,29601,2,90,2,4,1,2};
UINT4 FsWssUserMappingName [ ] ={1,3,6,1,4,1,29601,2,90,2,5,1,1};
UINT4 FsWssUserMappingStaMac [ ] ={1,3,6,1,4,1,29601,2,90,2,5,1,2};
UINT4 FsWssUserMappingRowStatus [ ] ={1,3,6,1,4,1,29601,2,90,2,5,1,3};
UINT4 FsWssUserName [ ] ={1,3,6,1,4,1,29601,2,90,3,1,1,1};
UINT4 FsWssUserStaMac [ ] ={1,3,6,1,4,1,29601,2,90,3,1,1,2};
UINT4 FsWssUserWlanIndex [ ] ={1,3,6,1,4,1,29601,2,90,3,1,1,3};
UINT4 FsWssUserAllotedBandWidth [ ] ={1,3,6,1,4,1,29601,2,90,3,1,1,4};
UINT4 FsWssUserAllotedVolume [ ] ={1,3,6,1,4,1,29601,2,90,3,1,1,5};
UINT4 FsWssUserAllotedTime [ ] ={1,3,6,1,4,1,29601,2,90,3,1,1,6};
UINT4 FsWssUserUsedVolume [ ] ={1,3,6,1,4,1,29601,2,90,3,1,1,7};
UINT4 FsWssUserUsedTime [ ] ={1,3,6,1,4,1,29601,2,90,3,1,1,8};
UINT4 FsWssUserAllotedDLBandWidth [ ] ={1,3,6,1,4,1,29601,2,90,3,1,1,9};
UINT4 FsWssUserAllotedULBandWidth [ ] ={1,3,6,1,4,1,29601,2,90,3,1,1,10};
UINT4 FsWssUserStationMacAddress [ ] ={1,3,6,1,4,1,29601,2,90,4,1,1};
UINT4 FsWssNtfUserName [ ] ={1,3,6,1,4,1,29601,2,90,4,1,2};




tMbDbEntry fswssuMibEntry[]= {

{{11,FsWssUserRoleStatus}, NULL, FsWssUserRoleStatusGet, FsWssUserRoleStatusSet, FsWssUserRoleStatusTest, FsWssUserRoleStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{11,FsWssUserBlockedCount}, NULL, FsWssUserBlockedCountGet, FsWssUserBlockedCountSet, FsWssUserBlockedCountTest, FsWssUserBlockedCountDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsWssUserLoggedCount}, NULL, FsWssUserLoggedCountGet, FsWssUserLoggedCountSet, FsWssUserLoggedCountTest, FsWssUserLoggedCountDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsWssUserTraceOption}, NULL, FsWssUserTraceOptionGet, FsWssUserTraceOptionSet, FsWssUserTraceOptionTest, FsWssUserTraceOptionDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{11,FsWssUserRoleTrapStatus}, NULL, FsWssUserRoleTrapStatusGet, FsWssUserRoleTrapStatusSet, FsWssUserRoleTrapStatusTest, FsWssUserRoleTrapStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{13,FsWssUserGroupId}, GetNextIndexFsWssUserGroupTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsWssUserGroupTableINDEX, 1, 0, 0, NULL},

{{13,FsWssUserGroupName}, GetNextIndexFsWssUserGroupTable, FsWssUserGroupNameGet, FsWssUserGroupNameSet, FsWssUserGroupNameTest, FsWssUserGroupTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsWssUserGroupTableINDEX, 1, 0, 0, NULL},

{{13,FsWssUserGroupBandWidth}, GetNextIndexFsWssUserGroupTable, FsWssUserGroupBandWidthGet, FsWssUserGroupBandWidthSet, FsWssUserGroupBandWidthTest, FsWssUserGroupTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsWssUserGroupTableINDEX, 1, 0, 0, "256"},

{{13,FsWssUserGroupVolume}, GetNextIndexFsWssUserGroupTable, FsWssUserGroupVolumeGet, FsWssUserGroupVolumeSet, FsWssUserGroupVolumeTest, FsWssUserGroupTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsWssUserGroupTableINDEX, 1, 0, 0, "0"},

{{13,FsWssUserGroupTime}, GetNextIndexFsWssUserGroupTable, FsWssUserGroupTimeGet, FsWssUserGroupTimeSet, FsWssUserGroupTimeTest, FsWssUserGroupTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsWssUserGroupTableINDEX, 1, 0, 0, "0"},

{{13,FsWssUserGroupRowStatus}, GetNextIndexFsWssUserGroupTable, FsWssUserGroupRowStatusGet, FsWssUserGroupRowStatusSet, FsWssUserGroupRowStatusTest, FsWssUserGroupTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsWssUserGroupTableINDEX, 1, 0, 1, NULL},

{{13,FsWssUserGroupDLBandWidth}, GetNextIndexFsWssUserGroupTable, FsWssUserGroupDLBandWidthGet, FsWssUserGroupDLBandWidthSet, FsWssUserGroupDLBandWidthTest, FsWssUserGroupTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsWssUserGroupTableINDEX, 1, 0, 0, "256"},

{{13,FsWssUserGroupULBandWidth}, GetNextIndexFsWssUserGroupTable, FsWssUserGroupULBandWidthGet, FsWssUserGroupULBandWidthSet, FsWssUserGroupULBandWidthTest, FsWssUserGroupTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsWssUserGroupTableINDEX, 1, 0, 0, "256"},

{{13,FsWssUserRoleName}, GetNextIndexFsWssUserRoleTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsWssUserRoleTableINDEX, 2, 0, 0, NULL},

{{13,FsWssUserRoleWlanIndex}, GetNextIndexFsWssUserRoleTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsWssUserRoleTableINDEX, 2, 0, 0, NULL},

{{13,FsWssUserRoleGroupId}, GetNextIndexFsWssUserRoleTable, FsWssUserRoleGroupIdGet, FsWssUserRoleGroupIdSet, FsWssUserRoleGroupIdTest, FsWssUserRoleTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsWssUserRoleTableINDEX, 2, 0, 0, NULL},

{{13,FsWssUserRoleRowStatus}, GetNextIndexFsWssUserRoleTable, FsWssUserRoleRowStatusGet, FsWssUserRoleRowStatusSet, FsWssUserRoleRowStatusTest, FsWssUserRoleTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsWssUserRoleTableINDEX, 2, 0, 1, NULL},

{{13,FsWssUserNameAccessListUserName}, GetNextIndexFsWssUserNameAccessListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsWssUserNameAccessListTableINDEX, 1, 0, 0, NULL},

{{13,FsWssUserNameAccessListRowStatus}, GetNextIndexFsWssUserNameAccessListTable, FsWssUserNameAccessListRowStatusGet, FsWssUserNameAccessListRowStatusSet, FsWssUserNameAccessListRowStatusTest, FsWssUserNameAccessListTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsWssUserNameAccessListTableINDEX, 1, 0, 1, NULL},

{{13,FsWssUserMacAccessListStaMac}, GetNextIndexFsWssUserMacAccessListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_NOACCESS, FsWssUserMacAccessListTableINDEX, 1, 0, 0, NULL},

{{13,FsWssUserMacAccessListRowStatus}, GetNextIndexFsWssUserMacAccessListTable, FsWssUserMacAccessListRowStatusGet, FsWssUserMacAccessListRowStatusSet, FsWssUserMacAccessListRowStatusTest, FsWssUserMacAccessListTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsWssUserMacAccessListTableINDEX, 1, 0, 1, NULL},

{{13,FsWssUserMappingName}, GetNextIndexFsWssUserMappingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsWssUserMappingTableINDEX, 2, 0, 0, NULL},

{{13,FsWssUserMappingStaMac}, GetNextIndexFsWssUserMappingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_NOACCESS, FsWssUserMappingTableINDEX, 2, 0, 0, NULL},

{{13,FsWssUserMappingRowStatus}, GetNextIndexFsWssUserMappingTable, FsWssUserMappingRowStatusGet, FsWssUserMappingRowStatusSet, FsWssUserMappingRowStatusTest, FsWssUserMappingTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsWssUserMappingTableINDEX, 2, 0, 1, NULL},

{{13,FsWssUserName}, GetNextIndexFsWssUserSessionTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsWssUserSessionTableINDEX, 2, 0, 0, NULL},

{{13,FsWssUserStaMac}, GetNextIndexFsWssUserSessionTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_NOACCESS, FsWssUserSessionTableINDEX, 2, 0, 0, NULL},

{{13,FsWssUserWlanIndex}, GetNextIndexFsWssUserSessionTable, FsWssUserWlanIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsWssUserSessionTableINDEX, 2, 0, 0, NULL},

{{13,FsWssUserAllotedBandWidth}, GetNextIndexFsWssUserSessionTable, FsWssUserAllotedBandWidthGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsWssUserSessionTableINDEX, 2, 0, 0, NULL},

{{13,FsWssUserAllotedVolume}, GetNextIndexFsWssUserSessionTable, FsWssUserAllotedVolumeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsWssUserSessionTableINDEX, 2, 0, 0, NULL},

{{13,FsWssUserAllotedTime}, GetNextIndexFsWssUserSessionTable, FsWssUserAllotedTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsWssUserSessionTableINDEX, 2, 0, 0, NULL},

{{13,FsWssUserUsedVolume}, GetNextIndexFsWssUserSessionTable, FsWssUserUsedVolumeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsWssUserSessionTableINDEX, 2, 0, 0, NULL},

{{13,FsWssUserUsedTime}, GetNextIndexFsWssUserSessionTable, FsWssUserUsedTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsWssUserSessionTableINDEX, 2, 0, 0, NULL},

{{13,FsWssUserAllotedDLBandWidth}, GetNextIndexFsWssUserSessionTable, FsWssUserAllotedDLBandWidthGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsWssUserSessionTableINDEX, 2, 0, 0, NULL},

{{13,FsWssUserAllotedULBandWidth}, GetNextIndexFsWssUserSessionTable, FsWssUserAllotedULBandWidthGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsWssUserSessionTableINDEX, 2, 0, 0, NULL},

{{12,FsWssUserStationMacAddress}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},

{{12,FsWssNtfUserName}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},
};
tMibData fswssuEntry = { 36, fswssuMibEntry };

#endif /* _FSWSSUDB_H */

