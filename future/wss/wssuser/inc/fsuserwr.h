/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: fsuserwr.h,v 1.1.1.1 2016/09/20 10:42:41 siva Exp $
*
* Description: This file contains the prototype(wr) for WSSUSER
*********************************************************************/

#ifndef _FSWSSUWR_H
#define _FSWSSUWR_H

VOID RegisterFSWSSU(VOID);

VOID UnRegisterFSWSSU(VOID);
INT4 FsWssUserRoleStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsWssUserBlockedCountGet(tSnmpIndex *, tRetVal *);
INT4 FsWssUserLoggedCountGet(tSnmpIndex *, tRetVal *);
INT4 FsWssUserTraceOptionGet(tSnmpIndex *, tRetVal *);
INT4 FsWssUserRoleTrapStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsWssUserRoleStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsWssUserBlockedCountSet(tSnmpIndex *, tRetVal *);
INT4 FsWssUserLoggedCountSet(tSnmpIndex *, tRetVal *);
INT4 FsWssUserTraceOptionSet(tSnmpIndex *, tRetVal *);
INT4 FsWssUserRoleTrapStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsWssUserRoleStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsWssUserBlockedCountTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsWssUserLoggedCountTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsWssUserTraceOptionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsWssUserRoleTrapStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsWssUserRoleStatusDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsWssUserBlockedCountDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsWssUserLoggedCountDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsWssUserTraceOptionDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsWssUserRoleTrapStatusDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 GetNextIndexFsWssUserGroupTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsWssUserGroupNameGet(tSnmpIndex *, tRetVal *);
INT4 FsWssUserGroupBandWidthGet(tSnmpIndex *, tRetVal *);
INT4 FsWssUserGroupDLBandWidthGet(tSnmpIndex *, tRetVal *);
INT4 FsWssUserGroupULBandWidthGet(tSnmpIndex *, tRetVal *);
INT4 FsWssUserGroupVolumeGet(tSnmpIndex *, tRetVal *);
INT4 FsWssUserGroupTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsWssUserGroupRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsWssUserGroupNameSet(tSnmpIndex *, tRetVal *);
INT4 FsWssUserGroupBandWidthSet(tSnmpIndex *, tRetVal *);
INT4 FsWssUserGroupDLBandWidthSet(tSnmpIndex *, tRetVal *);
INT4 FsWssUserGroupULBandWidthSet(tSnmpIndex *, tRetVal *);
INT4 FsWssUserGroupVolumeSet(tSnmpIndex *, tRetVal *);
INT4 FsWssUserGroupTimeSet(tSnmpIndex *, tRetVal *);
INT4 FsWssUserGroupRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsWssUserGroupNameTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsWssUserGroupBandWidthTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsWssUserGroupDLBandWidthTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsWssUserGroupULBandWidthTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsWssUserGroupVolumeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsWssUserGroupTimeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsWssUserGroupRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsWssUserGroupTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsWssUserRoleTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsWssUserRoleGroupIdGet(tSnmpIndex *, tRetVal *);
INT4 FsWssUserRoleRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsWssUserRoleGroupIdSet(tSnmpIndex *, tRetVal *);
INT4 FsWssUserRoleRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsWssUserRoleGroupIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsWssUserRoleRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsWssUserRoleTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsWssUserNameAccessListTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsWssUserNameAccessListRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsWssUserNameAccessListRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsWssUserNameAccessListRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsWssUserNameAccessListTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsWssUserMacAccessListTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsWssUserMacAccessListRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsWssUserMacAccessListRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsWssUserMacAccessListRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsWssUserMacAccessListTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsWssUserMappingTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsWssUserMappingRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsWssUserMappingRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsWssUserMappingRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsWssUserMappingTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsWssUserSessionTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsWssUserWlanIndexGet(tSnmpIndex *, tRetVal *);
INT4 FsWssUserAllotedBandWidthGet(tSnmpIndex *, tRetVal *);
INT4 FsWssUserAllotedDLBandWidthGet(tSnmpIndex *, tRetVal *);
INT4 FsWssUserAllotedULBandWidthGet(tSnmpIndex *, tRetVal *);
INT4 FsWssUserAllotedVolumeGet(tSnmpIndex *, tRetVal *);
INT4 FsWssUserAllotedTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsWssUserUsedVolumeGet(tSnmpIndex *, tRetVal *);
INT4 FsWssUserUsedTimeGet(tSnmpIndex *, tRetVal *);
#endif /* _FSWSSUWR_H */
