/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: userextn.h,v 1.1.1.1 2016/09/20 10:42:41 siva Exp $
*
* Description: WSSUSER File.
*********************************************************************/

#ifndef _WSSUSEREXTN_H_
#define _WSSUSEREXTN_H_

PUBLIC tWssUserGlobals gWssUserGlobals;

#endif/*_USEREXTN_H_*/
   
/*-----------------------------------------------------------------------*/
/*                       End of the file userextn.h                      */
/*-----------------------------------------------------------------------*/

