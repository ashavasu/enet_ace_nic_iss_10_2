/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: usertmr.h,v 1.1.1.1 2016/09/20 10:42:41 siva Exp $
*
* Description: USER File.
*********************************************************************/

#ifndef _USERTMR_H
#define _USERTMR_H

typedef enum { 
    USER_MIN_TIMER_ID           =   0,
    USER_SESS_TIMER_ID          =   1,
    USER_INTERIM_UPDATE_TIMER_ID          =   2,
    USER_MAX_TIMER_ID           =   3
} eUserTimerId;

typedef struct  USERTIMER {
     tTmrAppTimer Node;
     VOID *pContext;              /* Refers to the context to which 
                                       timer is attached */
     UINT1  u1TimerId;              /* Identification of Timer */
     UINT1 u1Status;                /* Validity of the structure 
                                       stored */
     UINT2 u2Pad;                   /* Padding for 4 Byte allignment */
}tUserTimer;


#define USER_TIMER_ACTIVE     1
#define USER_TIMER_INACTIVE   0
#define USER_INTERIM_UPDATE_TIMOUT_VAL 120

#define USER_INIT_TIMER(pUserTimer)      \
    ((pUserTimer)->u1Status = USER_TIMER_INACTIVE)
#define USER_ACTIVATE_TIMER(pUserTimer)      \
    ((pUserTimer)->u1Status = USER_TIMER_ACTIVE)
#define USER_IS_TIMER_ACTIVE(pUserTimer) \
    (((pUserTimer)->u1Status == USER_TIMER_INACTIVE)?FALSE:TRUE)


VOID UserTmrProcessTimer  PROTO ((VOID));


UINT4 WssUserTmrInit                     PROTO ((VOID));

UINT4 WssUserTmrStartTimer                  PROTO ((tUserTimer *pUserTmr,
                                             eUserTimerId UserTmrId,
                                             UINT4 u4TimeOut,
                                             VOID *pTmrContext));
UINT4 WssUserInterimUpdateTimer              PROTO ((tUserTimer *pUserTmr,
                                             eUserTimerId UserTmrId,
                                             UINT4 u4TimeOut,
                                             VOID *pTmrContext));
UINT4 WssUserTmrStopTimer                   PROTO ((tUserTimer *pUserTmr));
VOID  WssUserTmrProcSessTimer         PROTO ((VOID *pTmrContext));
VOID  WssUserProcInterimUpdateTmr     PROTO ((VOID *pTmrContext));

#endif /* _USERTMR_H */
