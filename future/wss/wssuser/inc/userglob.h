/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: userglob.h,v 1.1.1.1 2016/09/20 10:42:41 siva Exp $
*
* Description: WSSUSER File.
*********************************************************************/

#ifndef __WSSUSERGLOB_H__
#define __WSSUSERGLOB_H__

tWssUserGlobals gWssUserGlobals;

#endif  /*__USERGLOB_H__*/

