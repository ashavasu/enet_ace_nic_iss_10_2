/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: userinc.h,v 1.1.1.1 2016/09/20 10:42:41 siva Exp $
*
* Description: WSSUSER File.
*********************************************************************/

#ifndef _USERINC_H
#define _USERINC_H

#include "lr.h"
#include "ip.h"
#include "cfa.h"
#include "usercli.h"
#include "userrole.h"
#include "usermacr.h"
#include "wsswlan.h"
#include "usertdfs.h"
#include "userprot.h"
#include "usersz.h"
#include "fsuserwr.h"
#include "usertrc.h"
#include "usertmr.h"
#include  "wssstawlcprot.h"

#ifdef WLC_WANTED
#include "wssifwlcwlandb.h"
#endif


#ifdef _USERINIT_C
#include "userglob.h"
#else
#include "userextn.h"
#endif

#include "wsscfglwg.h"
#endif /* _USERINC_H */
