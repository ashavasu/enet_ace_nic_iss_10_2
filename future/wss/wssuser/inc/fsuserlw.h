/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsuserlw.h,v 1.1.1.1 2016/09/20 10:42:41 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsWssUserRoleStatus ARG_LIST((INT4 *));

INT1
nmhGetFsWssUserBlockedCount ARG_LIST((UINT4 *));

INT1
nmhGetFsWssUserLoggedCount ARG_LIST((UINT4 *));

INT1
nmhGetFsWssUserTraceOption ARG_LIST((INT4 *));

INT1
nmhGetFsWssUserRoleTrapStatus ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsWssUserRoleStatus ARG_LIST((INT4 ));

INT1
nmhSetFsWssUserBlockedCount ARG_LIST((UINT4 ));

INT1
nmhSetFsWssUserLoggedCount ARG_LIST((UINT4 ));

INT1
nmhSetFsWssUserTraceOption ARG_LIST((INT4 ));

INT1
nmhSetFsWssUserRoleTrapStatus ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsWssUserRoleStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsWssUserBlockedCount ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsWssUserLoggedCount ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsWssUserTraceOption ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsWssUserRoleTrapStatus ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsWssUserRoleStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsWssUserBlockedCount ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsWssUserLoggedCount ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsWssUserTraceOption ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsWssUserRoleTrapStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsWssUserGroupTable. */
INT1
nmhValidateIndexInstanceFsWssUserGroupTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsWssUserGroupTable  */

INT1
nmhGetFirstIndexFsWssUserGroupTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsWssUserGroupTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsWssUserGroupName ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsWssUserGroupBandWidth ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsWssUserGroupDLBandWidth ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsWssUserGroupULBandWidth ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsWssUserGroupVolume ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsWssUserGroupTime ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsWssUserGroupRowStatus ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsWssUserGroupName ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsWssUserGroupBandWidth ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsWssUserGroupDLBandWidth ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsWssUserGroupULBandWidth ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsWssUserGroupVolume ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsWssUserGroupTime ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsWssUserGroupRowStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsWssUserGroupName ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsWssUserGroupBandWidth ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsWssUserGroupDLBandWidth ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsWssUserGroupULBandWidth ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsWssUserGroupVolume ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsWssUserGroupTime ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsWssUserGroupRowStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsWssUserGroupTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsWssUserRoleTable. */
INT1
nmhValidateIndexInstanceFsWssUserRoleTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsWssUserRoleTable  */

INT1
nmhGetFirstIndexFsWssUserRoleTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *  , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsWssUserRoleTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsWssUserRoleGroupId ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,UINT4 *));

INT1
nmhGetFsWssUserRoleRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsWssUserRoleGroupId ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  ,UINT4 ));

INT1
nmhSetFsWssUserRoleRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsWssUserRoleGroupId ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  ,UINT4 ));

INT1
nmhTestv2FsWssUserRoleRowStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsWssUserRoleTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsWssUserNameAccessListTable. */
INT1
nmhValidateIndexInstanceFsWssUserNameAccessListTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsWssUserNameAccessListTable  */

INT1
nmhGetFirstIndexFsWssUserNameAccessListTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsWssUserNameAccessListTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsWssUserNameAccessListRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsWssUserNameAccessListRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsWssUserNameAccessListRowStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsWssUserNameAccessListTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsWssUserMacAccessListTable. */
INT1
nmhValidateIndexInstanceFsWssUserMacAccessListTable ARG_LIST((tMacAddr ));

/* Proto Type for Low Level GET FIRST fn for FsWssUserMacAccessListTable  */

INT1
nmhGetFirstIndexFsWssUserMacAccessListTable ARG_LIST((tMacAddr * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsWssUserMacAccessListTable ARG_LIST((tMacAddr , tMacAddr * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsWssUserMacAccessListRowStatus ARG_LIST((tMacAddr ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsWssUserMacAccessListRowStatus ARG_LIST((tMacAddr  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsWssUserMacAccessListRowStatus ARG_LIST((UINT4 *  ,tMacAddr  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsWssUserMacAccessListTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsWssUserMappingTable. */
INT1
nmhValidateIndexInstanceFsWssUserMappingTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tMacAddr ));

/* Proto Type for Low Level GET FIRST fn for FsWssUserMappingTable  */

INT1
nmhGetFirstIndexFsWssUserMappingTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *  , tMacAddr * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsWssUserMappingTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , tMacAddr , tMacAddr * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsWssUserMappingRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tMacAddr ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsWssUserMappingRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tMacAddr  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsWssUserMappingRowStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , tMacAddr  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsWssUserMappingTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsWssUserSessionTable. */
INT1
nmhValidateIndexInstanceFsWssUserSessionTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tMacAddr ));

/* Proto Type for Low Level GET FIRST fn for FsWssUserSessionTable  */

INT1
nmhGetFirstIndexFsWssUserSessionTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *  , tMacAddr * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsWssUserSessionTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , tMacAddr , tMacAddr * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsWssUserWlanIndex ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tMacAddr ,UINT4 *));

INT1
nmhGetFsWssUserAllotedBandWidth ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tMacAddr ,UINT4 *));
INT1
nmhGetFsWssUserAllotedDLBandWidth ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tMacAddr ,UINT4 *));

INT1
nmhGetFsWssUserAllotedULBandWidth ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tMacAddr ,UINT4 *));

INT1
nmhGetFsWssUserAllotedVolume ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tMacAddr ,UINT4 *));

INT1
nmhGetFsWssUserAllotedTime ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tMacAddr ,UINT4 *));

INT1
nmhGetFsWssUserUsedVolume ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tMacAddr ,UINT4 *));

INT1
nmhGetFsWssUserUsedTime ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tMacAddr ,UINT4 *));
