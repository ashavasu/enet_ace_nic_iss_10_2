/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: usertrc.h,v 1.1.1.1 2016/09/20 10:42:41 siva Exp $
*
* Description: WSSUSER File.
*********************************************************************/
#ifndef _USERTRC_H_
#define _USERTRC_H_

/* Trace and debug flags */
#define   WSSUSER_TRC_FLAG     gWssUserGlobals.u4WssUserTrcOpt     

/* Module names */
#define   WSSUSER_MOD_NAME           ((const char *)"WSSUSER")

#define   WSSUSER_INIT_SHUT_TRC      INIT_SHUT_TRC     
#define   WSSUSER_MGMT_TRC           MGMT_TRC          
#define   WSSUSER_DATA_PATH_TRC      DATA_PATH_TRC     
#define   WSSUSER_CONTROL_PATH_TRC   CONTROL_PLANE_TRC 
#define   WSSUSER_DUMP_TRC           DUMP_TRC          
#define   WSSUSER_OS_RESOURCE_TRC    OS_RESOURCE_TRC   
#define   WSSUSER_ALL_FAILURE_TRC    ALL_FAILURE_TRC   
#define   WSSUSER_BUFFER_TRC         BUFFER_TRC        

/* Trace definitions */
#ifdef  TRACE_WANTED

#define WSSUSER_TRC(TraceType, Str)                                              \
        MOD_TRC(WSSUSER_TRC_FLAG, TraceType, WSSUSER_MOD_NAME, (const char *)Str)

#define WSSUSER_TRC_ARG1(TraceType, Str, Arg1)                                   \
        MOD_TRC_ARG1(WSSUSER_TRC_FLAG, TraceType, WSSUSER_MOD_NAME, (const char *)Str, Arg1)

#define WSSUSER_TRC_ARG2(TraceType, Str, Arg1, Arg2)                             \
        MOD_TRC_ARG2(WSSUSER_TRC_FLAG, TraceType, WSSUSER_MOD_NAME, (const char *)Str, Arg1, Arg2)

#define WSSUSER_TRC_ARG3(TraceType, Str, Arg1, Arg2, Arg3)                       \
        MOD_TRC_ARG3(WSSUSER_TRC_FLAG, TraceType, WSSUSER_MOD_NAME, (const char *)Str, Arg1, Arg2, Arg3)

#define WSSUSER_TRC_ARG4(TraceType, Str, Arg1, Arg2, Arg3,Arg4)                       \
        MOD_TRC_ARG4(WSSUSER_TRC_FLAG, TraceType, WSSUSER_MOD_NAME, (const char *)Str, Arg1, Arg2, Arg3,Arg4)

#define WSSUSER_TRC_ARG5(TraceType, Str, Arg1, Arg2, Arg3,Arg4,Arg5)                       \
        MOD_TRC_ARG5(WSSUSER_TRC_FLAG, TraceType, WSSUSER_MOD_NAME, (const char *)Str, Arg1, Arg2, Arg3,Arg4,Arg5)

#define WSSUSER_TRC_ARG6(TraceType, Str, Arg1, Arg2, Arg3,Arg4,Arg5,Arg6)                       \
        MOD_TRC_ARG6(WSSUSER_TRC_FLAG, TraceType, WSSUSER_MOD_NAME, (const char *)Str, Arg1, Arg2, Arg3,Arg4,Arg5,Arg6)


#define   WSSUSER_DEBUG(x) {x}

#else  /* TRACE_WANTED */

#define WSSUSER_TRC(TraceType, Str)
#define WSSUSER_TRC_ARG1(TraceType, Str, Arg1)
#define WSSUSER_TRC_ARG2(TraceType, Str, Arg1, Arg2)
#define WSSUSER_TRC_ARG3(TraceType, Str, Arg1, Arg2, Arg3)
#define WSSUSER_TRC_ARG4(TraceType, Str, Arg1, Arg2, Arg3,Arg4)
#define WSSUSER_TRC_ARG5(TraceType, Str, Arg1, Arg2, Arg3,Arg4,Arg5)
#define WSSUSER_TRC_ARG6(TraceType, Str, Arg1, Arg2, Arg3,Arg4,Arg5,Arg6)

#define   WSSUSER_DEBUG(x)

#endif /* TRACE_WANTED */

#endif/* _USERTRC_H_ */

