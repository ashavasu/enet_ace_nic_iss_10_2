/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: usertdfs.h,v 1.2 2017/05/23 14:16:59 siva Exp $
*
* Description: WSSUSER File.
*********************************************************************/

#ifndef _USERTDFS_H
#define _USERTDFS_H


#include "usermacr.h"
#include "userrole.h"
#include "usertmr.h"


/* Global Declaration tWssUserGlobals - contains globals used for various handlers
   and variables.*/

typedef struct   WssUserGlobals {
   tMemPoolId    WssUserGroupPoolId;                    /* PoolId for User Group Table */      
   tMemPoolId    WssUserRolePoolId;                     /* PoolId for User Role Table */
   tMemPoolId    WssUserNameAccessListPoolId;           /* PoolId for User Name Access List Table */
   tMemPoolId    WssUserMacAccessListPoolId;            /* PoolId for User MAC Access List Table */
   tMemPoolId    WssUserMappingListPoolId;              /* PoolId for User Mapping List Table */
   tMemPoolId    WssUserSessionPoolId;                  /* PoolId for User Session table */
   tMemPoolId    WssUserNotifyPoolId;                   /* PoolId for tWssUserNotifyParams structure*/
   tMemPoolId    WssUserNamePoolId;                     /* PoolId for User Name table */
   tRBTree       WssUserGroupEntryTable;                /* User Group List */
   tRBTree       WssUserRoleEntryTable;                 /* User Role List */
   tRBTree       WssUserNameAccessListTable;            /* User Name Access List */
   tRBTree       WssUserMacAccessListTable;             /* User MAC Access List */
   tRBTree       WssUserMappingListTable;               /* User Mapping List */
   tRBTree       WssUserSessionTable;                   /* User Session List */
   tRBTree       WssUserNameTable;                      /* User Name Table */
   tOsixSemId    UserProtocolSemId ;                    /* Semaphore ID for User Protocol Module */
   tOsixTaskId   UserTaskId;                            /* Task Id of User Task created. */
   tOsixQId      UserTaskQId;                           /* Queue id of the task created */
   INT4          i4UserRoleStatus;                      /* User Role Status */
   UINT4         u4LoggedCount;                         /* Logged User Count */
   UINT4         u4BlockedCount;                        /* Blocked User Count */
   UINT4         u4WssUserTrcOpt;                       /* Trace Variable */ 
   tTimerListId  u4UserTimerId;                         /* Timer Id*/
   UINT4         u4CDRSysLogId;                         /* Id CDR Syslog Handler */
   UINT4         u4UserTrapStatus;
}tWssUserGlobals;


/* User Group Table - Contains the User Group details with its attributes.
   This datastructure corresponds to fsWssUserGroupTable MIB Table */

typedef struct   UserGroupEntry {
    tRBNodeEmbd  UserGroupEntryNode;                    /* UserGroupEntyNode */
    UINT4        u4GroupId;                             /* UserGroup Identifier */
    UINT1        au1GroupName[MAX_GROUP_NAME_LENGTH];   /* Name of the user group */
    UINT4        u4Bandwidth;                           /* Bandwidth for the users in the user group */
    UINT4        u4DLBandwidth;                         /* Download Bandwidth for the users in the user group */
    UINT4        u4ULBandwidth;                         /* Upload Bandwidth for the users in the user group */
    UINT4        u4Volume;                              /* Volume for the users in the user group */
    UINT4        u4Time;                                /* Time for the users in the user group */
    UINT4        u4UserGroupLen;                        /* User Group Len*/
    INT4         i4RowStatus;                           /* RowStatus */
}tUserGroupEntry;



/* User Role Table - Contains the User and their User Group Mapping details.
   This datastructure corresponds to fsWssUserRoleTable MIB Table */

typedef struct   UserRoleEntry {
    tRBNodeEmbd  UserRoleEntryNode;                     /* UserRole Entry Node */
    UINT1        au1UserName[MAX_USER_NAME_LENGTH];     /* User Name */
    UINT4        u4GroupId;                             /* UserGroup Identifier */
    UINT4        u4WlanIndex;                           /* Wlan If Index */
    UINT4        u4UserNameLen;                         /* User Name Len*/
    INT4         i4RowStatus;                           /* RowStatus */
}tUserRoleEntry;



/* User Restrict Table - Contains the list of restricted users.
   This datastructure corresponds to fsWssUserNameAccessListTable MIB Table */

typedef struct   UserNameAccessListEntry {
    tRBNodeEmbd  UserNameAccessListNode;                /* UserNameAccessList Node */
    UINT1        au1UserName[MAX_USER_NAME_LENGTH];     /* User Name */
    UINT4        u4UserNameLen;
    INT4         i4RowStatus;                           /* RowStatus */
}tUserNameAccessListEntry;



/* Station Restrict Table - Contains the list of station MAC address, 
   those access needs to be restricted.  This datastructure corresponds to 
   fsWssUserMacAccessListTable MIB Table */

typedef struct   UserMacAccessListEntry {
    tRBNodeEmbd  UserMacAccessListNode;                 /* UserNameAccessList Node */
    tMacAddr     staMac;                                /* Station Mac Address*/
 UINT1        au1Pad[2];                             /* Structure Padding variable */
    INT4         i4RowStatus;                           /* RowStatus */
}tUserMacAccessListEntry;



/* User - MAC Mapping Table - Contains the details of access to the User 
   only from the specified station MAC addresses.  This datastructure corresponds to 
   fsWssUserMacAccessListTable MIB Table */

typedef struct   UserMappingListEntry  {
    tRBNodeEmbd  UserMappingNode;                       /* UserMapping Node*/
    UINT1        au1UserName[MAX_USER_NAME_LENGTH];     /* User Name */
    tMacAddr     staMac;                                /* Station Mac Address*/
    UINT1        au1Pad[2];                             /* Structure Padding variable */
    UINT4        u4UserNameLen;                         /* User Name Len*/
    INT4         i4RowStatus;                           /* RowStatus */
}tUserMappingListEntry;



/* User Session Table - Contains the details of user's session usage details.
   This datastructure corresponds to fsWssUserMappingTable MIB Table */

typedef struct   UserSessionEntry {
    tRBNodeEmbd  UserSessionNode;                       /* UserSessionNode */
    tMacAddr     staMac;                                /* Station Mac */
    UINT1        au1Pad[2];                             /* Structure Padding variable */
    INT4         i4NasPort;                              /* Nas Port*/
    UINT4        u4WlanIndex;                           /* Wlan If Index */
    UINT1        au1UserName[MAX_USER_NAME_LENGTH];     /* User Name */
    UINT4        u4UserNameLen;                         /* User Name Len*/
    UINT4        u4AllotedBandwidth;                    /* Alloted Bandwidth */
    UINT4        u4AllotedDLBandwidth;                  /*Allocated download Bandwidth*/
    UINT4        u4AllotedULBandwidth;                  /*Allocated upload Bandwidth*/
    UINT4        u4AllotedVolume;                       /* Alloted Volume */
    UINT4        u4AllotedTime;                         /* Alloted Time */
    UINT4        u4UsedVolume;                          /* Used Volume */
    UINT4        u4StartTime;                           /* Start Time with reference to SysUpTime, to compute duration */
    tUtlTm       utlStartTm;                            /* To store Start Date & Time for display */
    UINT4        u4UsedTime;                            /* Used Time */
    FLT4         f4TxBytes;                             /* Tx bytes */
    FLT4         f4RxBytes;                              /* RX bytes */
    UINT1        au1RadAccSessionID[LEN_SESSION_ID];    /* Radius Accounting Session ID */
     
}tUserSessionEntry;

typedef struct   UserNameEntry {
    tUserTimer   UserSessTmr;                           /* User Timer*/
    tUserTimer   UserInterimUpdateTmr;                  /* Interim Update Timer*/
    tRBNodeEmbd  UserNameNode;                          /* UserNameNode */
    eUserTerminateCause TerminateCause;                 /*Terminate Cause*/
    UINT1        au1UserName[MAX_USER_NAME_LENGTH];     /* User Name */
    UINT1        au1RadAccSessionID[LEN_SESSION_ID];    /* Radius Accounting Session ID */
    tUtlTm       utlStartTm;                            /* To store Start Date & Time for display */
    UINT4        u4WlanIndex;                           /* Wlan If Index */
    UINT4        u4UserNameLen;                         /* User Name Len*/
    UINT4        u4AllotedBandwidth;                    /*Allocated Bandwidth*/
    UINT4        u4AllotedDLBandwidth;                  /* Alloted download Bandwidth */
    UINT4        u4AllotedULBandwidth;                  /* Alloted upload Bandwidth */
    UINT4        u4AllotedVolume;                       /* Alloted Volume */
    UINT4        u4AllotedTime;                         /* Alloted Time */
    UINT4        u4StartTime;                           /* Start Time with reference to SysUpTime, to compute duration */
    UINT4        u4SessionCount;                        /* Session Counter */
    UINT4        u4UsedTime;                            /* Used Time */
    UINT4        u4UsedVolume;                          /* Used Volume */
    UINT4        u4ConsumedTime;                        /* To store the session duration value */
    FLT4         f4TxBytes;                             /* Tx bytes */
    FLT4         f4RxBytes;                              /* RX bytes */
    INT4         i4NasPort;                              /* Nas Port*/
     
}tUserNameEntry;


typedef struct{
    UINT4 u4WlanId;
    UINT4 u4UsedVol;
    UINT4 u4UsedTime;
    UINT1 au1UserName[32];
    UINT1 au1StaMac[21];
    UINT1 au1pad[3];
}tWlanUserTrapInfo;


#endif 
