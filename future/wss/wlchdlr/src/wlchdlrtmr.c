/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: wlchdlrtmr.c,v 1.5 2017/12/08 10:16:31 siva Exp $
 *
 * Description: This file contains the WLCHDLR Timer related routines.
 *
 *****************************************************************************/

#ifndef __WLCHDLRTMR_C__
#define __WLCHDLRTMR_C__

#include "wlchdlrinc.h"
#include "radioifextn.h"
#ifdef BAND_SELECT_WANTED
#include "wssstawlcprot.h"
#endif
#include "wlchdlr.h"
#ifdef RFMGMT_WANTED
#include "rfmextn.h"
#endif

static tWlcHdlrTimerList gWlcHdlrTmrList;
extern tRBTree      gWlcHdlrStaConfigDB;
/* timer block */
static tTmrBlk      tmrWlchdlrTmrBlk;
static tTmrBlk      tmrWlcWebAuthTmrBlk;
static tTmrBlk      tmrWlcStaConfTmrBlk;
static UINT4        gu4AgeOutTime;

/*****************************************************************************
 *                                                                           *
 * Function     : WlcHdlrpTmrInit                                            *
 *                                                                           *
 * Description  :  This function creates a timer list for all the timers     *
 *                          in WLC HDLR module.                              *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
WlcHdlrTmrInit (VOID)
{
    WLCHDLR_FN_ENTRY ();
    /* Timer List for State Machine Timers */
    if (TmrCreateTimerList ((CONST UINT1 *) WLCHDLR_TASK_NAME,
                            WLCHDLR_TMR_EXP_EVENT,
                            NULL,
                            (tTimerListId *) & (gWlcHdlrTmrList.
                                                WlcHdlrTmrListId)) ==
        TMR_FAILURE)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "WlchdlrTmrInit: Failed to "
                     "create the Application Timer List \r\n");
        return OSIX_FAILURE;
    }

    WlcHdlrTmrInitTmrDesc ();

    WLCHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : WlcHdlrTmrInitTmrDesc                                       *
 *                                                                           *
 * Description  : This function intializes the timer desc for all            *
 *                the timers in WLCHDLR module.                               *
 *                                                                           *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
VOID
WlcHdlrTmrInitTmrDesc (VOID)
{
#ifdef BAND_SELECT_WANTED
    tWssStaBandSteerDB  WssStaBandSteerDB;
    MEMSET (&WssStaBandSteerDB, 0, sizeof (tWssStaBandSteerDB));
#endif
    WLCHDLR_FN_ENTRY ();
    gWlcHdlrTmrList.aWlcHdlrTmrDesc[WLCHDLR_RETRANSMIT_INTERVAL_TMR].TmrExpFn
        = WlcHdlrRetransmitIntervalTmrExp;
    gWlcHdlrTmrList.aWlcHdlrTmrDesc[WLCHDLR_RETRANSMIT_INTERVAL_TMR].i2Offset =
        (INT2) FSAP_OFFSETOF (tRemoteSessionManager, RetransIntervalTmr);

    gWlcHdlrTmrList.aWlcHdlrTmrDesc[WLCHDLR_CPU_RELINQUISH_TMR].TmrExpFn
        = WlcHdlrCPURelinquishTmrExp;
    gWlcHdlrTmrList.aWlcHdlrTmrDesc[WLCHDLR_CPU_RELINQUISH_TMR].i2Offset = -1;

    gWlcHdlrTmrList.aWlcHdlrTmrDesc[WLCHDLR_DATA_TRANSFER_TMR].TmrExpFn
        = WlcHdlrDataTransferTmrExp;
    gWlcHdlrTmrList.aWlcHdlrTmrDesc[WLCHDLR_DATA_TRANSFER_TMR].i2Offset =
        (INT2) FSAP_OFFSETOF (tRemoteSessionManager, DataTransferStartTmr);
    gWlcHdlrTmrList.aWlcHdlrTmrDesc[WLCHDLR_WEBAUTH_LIFETIME_EXP_TMR].TmrExpFn
        = WlcHdlrWebAuthLifetimeExp;
    gWlcHdlrTmrList.aWlcHdlrTmrDesc[WLCHDLR_WEBAUTH_LIFETIME_EXP_TMR].i2Offset =
        -1;

    gWlcHdlrTmrList.aWlcHdlrTmrDesc[WLCHDLR_STA_CONF_PKT_TMR].TmrExpFn
        = WlcHdlrStaConfClearTmrExp;
    gWlcHdlrTmrList.aWlcHdlrTmrDesc[WLCHDLR_STA_CONF_PKT_TMR].i2Offset = -1;

#ifdef BAND_SELECT_WANTED
    gWlcHdlrTmrList.aWlcHdlrTmrDesc[WLCHDLR_PROBE_ENTRY_TMR].TmrExpFn
        = WlcHdlrStaProbeExp;
    gWlcHdlrTmrList.aWlcHdlrTmrDesc[WLCHDLR_PROBE_ENTRY_TMR].i2Offset =
        (INT2) FSAP_OFFSETOF (tWssStaBandSteerDB, ProbeEntryTmr);

    gWlcHdlrTmrList.aWlcHdlrTmrDesc[WLCHDLR_ASSOC_COUNT_TMR].TmrExpFn
        = WlcHdlrStaAssocCountExp;
    gWlcHdlrTmrList.aWlcHdlrTmrDesc[WLCHDLR_ASSOC_COUNT_TMR].i2Offset =
        (INT2) FSAP_OFFSETOF (tWssStaBandSteerDB, StaAssocCountTmr);
#endif

    WLCHDLR_FN_EXIT ();
}

/*****************************************************************************
 *                                                                           *
 * Function     : WlcHdlrTmrExpHandler                                       *
 *                                                                           *
 * Description  :  This function is called whenever a timer expiry           *
 *                 message is received by Service task. Different timer      *
 *                 expiry handlers are called based on the timer type.       *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
VOID
WlcHdlrTmrExpHandler (VOID)
{
    tTmrAppTimer       *pExpiredTimers = NULL;
    UINT1               u1TimerId = 0;
    INT2                i2Offset = 0;

    WLCHDLR_FN_ENTRY ();

    while ((pExpiredTimers =
            TmrGetNextExpiredTimer (gWlcHdlrTmrList.WlcHdlrTmrListId)) != NULL)
    {

        u1TimerId = ((tTmrBlk *) pExpiredTimers)->u1TimerId;

        WLCHDLR_TRC1 (WLCHDLR_MGMT_TRC, "Timer to be processed %d\r\n",
                      u1TimerId);

        if (u1TimerId < WLCHDLR_MAX_TMR)

        {
            i2Offset = gWlcHdlrTmrList.aWlcHdlrTmrDesc[u1TimerId].i2Offset;

            if (i2Offset == -1)
            {
                /* The timer function does not take any parameter. */
                (*(gWlcHdlrTmrList.aWlcHdlrTmrDesc[u1TimerId].TmrExpFn)) (NULL);
            }
            else
            {
                (*(gWlcHdlrTmrList.aWlcHdlrTmrDesc[u1TimerId].TmrExpFn))
                    ((UINT1 *) pExpiredTimers - i2Offset);
            }
        }
    }
    WLCHDLR_FN_EXIT ();
    return;
}

/*****************************************************************************
 *                                                                           *
 * Function     : WlcHdlrTmrStart                                             *
 *                                                                           *
 * Description  : This function used to start the wlchdlr state machiune      *
 *                specified timers.                                          *
 *                                                                           *
 * Input        : pSessEntry - pointer to session entry table                *
 *                u4TmrInterval - Time interval for which timer must run     *
 *               (in seconds)                                                *
 *                u1TmrType - Indicates which timer to start.                *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
WlcHdlrTmrStart (tRemoteSessionManager * pSessEntry, UINT1 u1TmrType,
                 UINT4 u4TmrInterval)
{

    switch (u1TmrType)
    {
        case WLCHDLR_RETRANSMIT_INTERVAL_TMR:
            if (TmrStart (gWlcHdlrTmrList.WlcHdlrTmrListId,
                          &(pSessEntry->RetransIntervalTmr),
                          u1TmrType, u4TmrInterval, 0) == TMR_FAILURE)
            {
                WLCHDLR_TRC1 (WLCHDLR_FAILURE_TRC, "WlcHdlrTmrStart: "
                              "Failed to start the Timer Type = %d \r\n",
                              u1TmrType);
                return OSIX_FAILURE;
            }
            break;
        case WLCHDLR_CPU_RELINQUISH_TMR:
            if (TmrStart (gWlcHdlrTmrList.WlcHdlrTmrListId, &tmrWlchdlrTmrBlk,
                          u1TmrType, u4TmrInterval, 0) == TMR_FAILURE)
            {
                WLCHDLR_TRC1 (WLCHDLR_FAILURE_TRC, "WlcHdlrTmrStart: "
                              "Failed to start the Timer Type = %d \r\n",
                              u1TmrType);
                return OSIX_FAILURE;
            }
            break;
        case WLCHDLR_DATA_TRANSFER_TMR:
            if (TmrStart (gWlcHdlrTmrList.WlcHdlrTmrListId,
                          &(pSessEntry->DataTransferStartTmr),
                          u1TmrType, u4TmrInterval, 0) == TMR_FAILURE)
            {
                WLCHDLR_TRC1 (WLCHDLR_FAILURE_TRC, "WlcHdlrTmrStart: "
                              "Failed to start the Timer Type = %d \r\n",
                              u1TmrType);
                return OSIX_FAILURE;
            }
            break;
        case WLCHDLR_WEBAUTH_LIFETIME_EXP_TMR:
            if (TmrStart
                (gWlcHdlrTmrList.WlcHdlrTmrListId, &tmrWlcWebAuthTmrBlk,
                 u1TmrType, u4TmrInterval, 0) == TMR_FAILURE)
            {
                WLCHDLR_TRC1 (WLCHDLR_FAILURE_TRC, "WlcHdlrTmrStart: "
                              "Failed to start the Timer Type = %d \r\n",
                              u1TmrType);
                return OSIX_FAILURE;
            }
            break;
        default:
            WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                         "WlcHdlrTmrStart:Invalid Timer type FAILED !!!\r\n");
            return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function                  : WlcHdlrTmrStop                                *
 *                                                                           *
 * Description               : This routine stops the given discovery timer  *
 *                                                                           *
 * Input                     : u1TmrType - Indicates which timer to stop     *
 *                             pSessEntry - pointer to session entry table   *
 *                                                                           *
 * Output                    : None                                          *
 *                                                                           *
 * Returns                   : None.                                         *
 *                                                                           *
 *****************************************************************************/
INT4
WlcHdlrTmrStop (tRemoteSessionManager * pSessEntry, UINT1 u1TmrType)
{
    UINT4               u4RemainingTime = 0;
    UINT4               u4TmrRetVal = TMR_SUCCESS;
    INT4                i4RetVal = OSIX_FAILURE;

    WLCHDLR_FN_ENTRY ();

    switch (u1TmrType)
    {
        case WLCHDLR_RETRANSMIT_INTERVAL_TMR:
            u4TmrRetVal = TmrGetRemainingTime (gWlcHdlrTmrList.WlcHdlrTmrListId,
                                               &(pSessEntry->RetransIntervalTmr.
                                                 TimerNode), &u4RemainingTime);
            if (u4TmrRetVal != TMR_FAILURE)
            {
                if (TmrStop (gWlcHdlrTmrList.WlcHdlrTmrListId,
                             &(pSessEntry->RetransIntervalTmr)) != TMR_SUCCESS)
                {
                    WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "WlchdlrTmrStop: "
                                 "Failure to stop MaxRetransmitIntervalr\r\n");
                }
                u4RemainingTime = 0;
                i4RetVal = OSIX_SUCCESS;
            }
            break;
        case WLCHDLR_CPU_RELINQUISH_TMR:
            u4TmrRetVal = TmrGetRemainingTime (gWlcHdlrTmrList.WlcHdlrTmrListId,
                                               &(tmrWlchdlrTmrBlk.TimerNode),
                                               &u4RemainingTime);
            if (u4TmrRetVal != TMR_FAILURE)
            {
                if (TmrStop (gWlcHdlrTmrList.WlcHdlrTmrListId,
                             &tmrWlchdlrTmrBlk) != TMR_SUCCESS)
                {
                    WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "WlchdlrTmrStop: "
                                 "Failure to stop wlchdlrCPURelinquishTmr \r\n");
                }
                u4RemainingTime = 0;
                i4RetVal = OSIX_SUCCESS;
            }
            break;
        case WLCHDLR_DATA_TRANSFER_TMR:
            u4TmrRetVal = TmrGetRemainingTime (gWlcHdlrTmrList.WlcHdlrTmrListId,
                                               &(pSessEntry->
                                                 DataTransferStartTmr.
                                                 TimerNode), &u4RemainingTime);
            if (u4TmrRetVal != TMR_FAILURE)
            {
                if (TmrStop (gWlcHdlrTmrList.WlcHdlrTmrListId,
                             &(pSessEntry->DataTransferStartTmr))
                    != TMR_SUCCESS)
                {
                    WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "WlchdlrTmrStop: "
                                 "Failure to stop DataTransfer Timer\r\n");
                }
                u4RemainingTime = 0;
                i4RetVal = OSIX_SUCCESS;
            }
            break;
        case WLCHDLR_WEBAUTH_LIFETIME_EXP_TMR:
            u4TmrRetVal = TmrGetRemainingTime (gWlcHdlrTmrList.WlcHdlrTmrListId,
                                               &(tmrWlcWebAuthTmrBlk.TimerNode),
                                               &u4RemainingTime);
            if (u4TmrRetVal != TMR_FAILURE)
            {
                if (TmrStop (gWlcHdlrTmrList.WlcHdlrTmrListId,
                             &tmrWlchdlrTmrBlk) != TMR_SUCCESS)
                {
                    WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "WlchdlrTmrStop: "
                                 "Failure to stop wlchdlrCPURelinquishTmr \r\n");
                }
                u4RemainingTime = 0;
                i4RetVal = OSIX_SUCCESS;
            }
            break;
        default:
            WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                         "WlcHdlrTmrStop:Invalid Timer type FAILED !!!\r\n");
            return OSIX_FAILURE;
    }
    WLCHDLR_FN_EXIT ();
    return i4RetVal;
}

/*****************************************************************************
 * Function                  : WlcHdlrRetransmitIntervalTmrExp               *
 *                                                                           *
 * Description               : This routine handles the Retransmit Timer     *
 *                             Expiry.                                       *
 *                                                                           *
 * Input                     : pSessEntry - pointer to session entry table   *
 *                                                                           *
 * Output                    : None                                          *
 *                                                                           *
 * Returns                   : None.                                         *
 *                                                                           *
 *****************************************************************************/
VOID
WlcHdlrRetransmitIntervalTmrExp (VOID *pArg)
{
    tRemoteSessionManager *pSessEntry = NULL;
    unCapwapMsgStruct   CapwapMsgStruct;
    UINT4               u4MaxRetransmitCount = 0;
    UINT4               u4MaxRetransmitInterval = 0;
    tWlcHdlrStaConfigDB *pWlcHdlrStaConfigDB = NULL;
    tWlcHdlrStaConfigDB *pWlcHdlrTmpStaConfigDB = NULL;
    UINT1              *pRcvBuf = NULL;
    UINT1               u1IsNotLinear = OSIX_FALSE;

    WLCHDLR_FN_ENTRY ();

    MEMSET (&CapwapMsgStruct, 0, sizeof (unCapwapMsgStruct));
    pSessEntry = (tRemoteSessionManager *) pArg;
    if (pSessEntry == NULL)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "Session Disconnected before "
                     "Retransmit Interval Timer Expiry \r\n");
        return;
    }

    if (pSessEntry->lastTransmittedPkt == NULL)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "Last Transmitted Pkt"
                     "is NULL. Cannot Retransmit\r\n");
        return;
    }

    pRcvBuf = WLCHDLR_BUF_IF_LINEAR (pSessEntry->lastTransmittedPkt, 0,
                                     pSessEntry->lastTransmittedPktLen);
    if (pRcvBuf == NULL)
    {
        WLCHDLR_STA_MEM_ALLOC (pWlcHdlrTmpStaConfigDB);
        if (pWlcHdlrTmpStaConfigDB == NULL)
        {
            WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "STA BUFFER is empty \r\n");
            return;
        }
        pRcvBuf = pWlcHdlrTmpStaConfigDB->au1StaBuf;
        WLCHDLR_COPY_FROM_BUF (pSessEntry->lastTransmittedPkt, pRcvBuf, 0,
                               pSessEntry->lastTransmittedPktLen);
        u1IsNotLinear = OSIX_TRUE;
    }

    if (CapwapGetCapwapBaseAcMaxRetransmit (&u4MaxRetransmitCount) !=
        OSIX_SUCCESS)
    {
    }

    if (CapwapGetCapwapBaseAcRetransmitInterval (&u4MaxRetransmitInterval) !=
        OSIX_SUCCESS)
    {
    }
    if (pSessEntry->u1RetransmitCount < u4MaxRetransmitCount)
    {
        CapwapMsgStruct.CapwapTxPkt.pData = pSessEntry->lastTransmittedPkt;
        CapwapMsgStruct.CapwapTxPkt.u4pktLen =
            pSessEntry->lastTransmittedPktLen;
        CapwapMsgStruct.CapwapTxPkt.u4IpAddr =
            pSessEntry->remoteIpAddr.u4_addr[0];
        CapwapMsgStruct.CapwapTxPkt.u4DestPort = pSessEntry->u4RemoteCtrlPort;
        if (WssIfProcessCapwapMsg (WSS_CAPWAP_TX_CTRL_PKT, &CapwapMsgStruct)
            == OSIX_FAILURE)
        {
            WlchdlrFreeTransmittedPacket (pSessEntry);
            WLCHDLR_TRC (ALL_FAILURE_TRC, "WlcHdlrRetransmitIntervalTmrExp: "
                         "Failed to Transmit the packet \r\n");
            if (u1IsNotLinear == OSIX_TRUE)
            {
                WLCHDLR_STA_MEM_RELEASE (pWlcHdlrTmpStaConfigDB);
            }
            return;
        }
        else
        {
            WlcHdlrTmrStart (pSessEntry, WLCHDLR_RETRANSMIT_INTERVAL_TMR,
                             (UINT4) (u4MaxRetransmitInterval * (UINT4)
                                      (1 << (pSessEntry->u1RetransmitCount))));
            pSessEntry->u1RetransmitCount++;
        }
    }
    else
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                     "WLCHDLR Retransmit count reached maximuxm value\r\n");
        gu1IsConfigResponseReceived = CAPWAP_RESPONSE_TIMEOUT;

#ifdef RFMGMT_WANTED
        gu1IsRfMgmtResponseReceived = CAPWAP_RESPONSE_TIMEOUT;
#endif

        if (pRcvBuf[WLCHDLR_GET_MSG_TYPE_BIT] == CAPWAP_STATION_CONF_REQ)
        {
            /*Delete the station Entry from the DB */
            pWlcHdlrStaConfigDB =
                WlcHdlrStationDetailsGet (pSessEntry->u2IntProfileId,
                                          pSessEntry->lastTransmitedSeqNum);

            if (pWlcHdlrStaConfigDB != NULL)
            {
                RBTreeRem (gWlcHdlrStaConfigDB,
                           (tRBElem *) pWlcHdlrStaConfigDB);
                WLCHDLR_STA_MEM_RELEASE (pWlcHdlrStaConfigDB);
            }
            else
            {
                WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                             "WlcHdlrRetransmitIntervalTmrExp:"
                             "No Entry is present in DB\r\n");
            }
        }
        WlchdlrFreeTransmittedPacket (pSessEntry);
    }
    if (u1IsNotLinear == OSIX_TRUE)
    {
        WLCHDLR_STA_MEM_RELEASE (pWlcHdlrTmpStaConfigDB);
    }
    WLCHDLR_FN_EXIT ();
}

/*****************************************************************************
 * Function                  : WlcHdlrDataTransferTmrExp                     *
 *                                                                           *
 * Description               : This routine handles the Data Transfer Timer  *
 *                             Expiry.                                       *
 *                                                                           *
 * Input                     : pSessEntry - pointer to session entry table   *
 *                                                                           *
 * Output                    : None                                          *
 *                                                                           *
 * Returns                   : None.                                         *
 *                                                                           *
 *****************************************************************************/
VOID
WlcHdlrDataTransferTmrExp (VOID *pArg)
{
    tRemoteSessionManager *pSessEntry = NULL;
    unCapwapMsgStruct   CapwapMsgStruct;

    WLCHDLR_FN_ENTRY ();

    MEMSET (&CapwapMsgStruct, 0, sizeof (unCapwapMsgStruct));
    pSessEntry = (tRemoteSessionManager *) pArg;
    if (pSessEntry == NULL)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "Session Disconnected before "
                     "Data Transfer Interval Timer Expiry \r\n");
        return;
    }

    pSessEntry->u1DataTransferTimerRunning = OSIX_FALSE;

    /*Reset the Data Transfer Info fore the WTP(read) side */
    if (pSessEntry->u1CrashReadFileOpened == OSIX_TRUE &&
        pSessEntry->readCrashFd != NULL)
    {
        fclose (pSessEntry->readCrashFd);
        pSessEntry->readCrashFd = NULL;
    }
    pSessEntry->i4CrashFileSize = 0;
    pSessEntry->u1CrashEndofFileReached = OSIX_FALSE;
    pSessEntry->u1CrashReadFileOpened = OSIX_FALSE;
    pSessEntry->u4CrashFileReadOffset = 0;
    pSessEntry->u1CrashFileNameSet = OSIX_FALSE;

    /*Reset the Data Transfer Info fore the WLC(write) side */
    if (pSessEntry->writeCrashFd != NULL)
    {
        fclose (pSessEntry->writeCrashFd);
        pSessEntry->writeCrashFd = NULL;
    }
    pSessEntry->u1CrashWriteFileOpened = OSIX_FALSE;

    WLCHDLR_FN_EXIT ();
}

/*****************************************************************************
 * Function                  : WlcHdlrWebAuthLifetimeExp                     *
 *                                                                           *
 * Description               : This routine handles the webauth lifetime     *
 *                             Expiry.                                       *
 *                                                                           *
 * Input                     : None                                          *
 *                                                                           *
 * Output                    : None                                          *
 *                                                                           *
 * Returns                   : None.                                         *
 *                                                                           *
 *****************************************************************************/
VOID
WlcHdlrWebAuthLifetimeExp (VOID *pArg)
{

    UNUSED_PARAM (pArg);
    WLCHDLR_FN_ENTRY ();
    if (WssIfProcessWssAuthDBMsg (WSS_STA_WEBAUTH_UPDATE_LIFESPAN,
                                  NULL) == OSIX_SUCCESS)
    {
        /* Start the timer */
        WlcHdlrTmrStart (NULL,
                         WLCHDLR_WEBAUTH_LIFETIME_EXP_TMR,
                         MAX_WEBAUTH_LIFETIME_EXP_TIMEOUT);
    }
    WLCHDLR_FN_EXIT ();
}

#ifdef BAND_SELECT_WANTED
/*****************************************************************************
 * Function                  : WlcHdlrProbeTmrStart                          *
 *                                                                           *
 * Description               : This routine starts  the probe timer          *
 *                                                                           *
 * Input                     : None                                          *
 *                                                                           *
 * Output                    : None                                          *
 *                                                                           *
 * Returns                   : None.                                         *
 *                                                                           *
 *****************************************************************************/
INT4
WlcHdlrProbeTmrStart (tWssStaBandSteerDB * pWssStaBandSteerDB,
                      UINT1 u1AgeOutTime)
{
    if (TmrStart
        (gWlcHdlrTmrList.WlcHdlrTmrListId, &(pWssStaBandSteerDB->ProbeEntryTmr),
         WLCHDLR_PROBE_ENTRY_TMR, u1AgeOutTime, 0) == TMR_FAILURE)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "WlcHdlrProbeTmrStart: "
                     "Failed to start the Timer Type \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function                  : WlcHdlrStaProbeExp                            *
 *                                                                           *
 * Description               : This routine handles the probe timer          *
 *                             Expiry.                                       *
 *                                                                           *
 * Input                     : None                                          *
 *                                                                           *
 * Output                    : None                                          *
 *                                                                           *
 * Returns                   : None.                                         *
 *                                                                           *
 *****************************************************************************/
VOID
WlcHdlrStaProbeExp (VOID *pArg)
{
    tWssStaBandSteerDB *pWssStaBandSteerDB = NULL;
    WLCHDLR_FN_ENTRY ();
    pWssStaBandSteerDB = (tWssStaBandSteerDB *) pArg;
    if (pWssStaBandSteerDB == NULL)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "WlcHdlrStaProbeExp: "
                     "Failed BandSteerDB is null\r\n");
        return;
    }
    if (WssStaUpdateBandSteerProcessDB (WSSSTA_GET_BAND_STEER_DB,
                                        pWssStaBandSteerDB) == OSIX_SUCCESS)
    {
        if (WssStaUpdateBandSteerProcessDB (WSSSTA_DESTROY_BAND_STEER_DB,
                                            pWssStaBandSteerDB) == OSIX_FAILURE)
        {
            WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "WlcHdlrStaProbeExp: "
                         "Failed to delete entry in BandSteerBD \r\n");
            return;
        }
    }
    WLCHDLR_FN_EXIT ();
    return;
}

/*****************************************************************************
 * Function                  : WlcHdlrStaAssocCountTmrStart                  *
 *                                                                           *
 * Description               : This routine starts the assoc timer           *
 *                                                                           *
 * Input                     : None                                          *
 *                                                                           *
 * Output                    : None                                          *
 *                                                                           *
 * Returns                   : None.                                         *
 *                                                                           *
 *****************************************************************************/
INT4
WlcHdlrStaAssocCountTmrStart (tWssStaBandSteerDB * pWssStaBandSteerDB,
                              UINT1 u1AssocResetTime)
{
    if (TmrStart
        (gWlcHdlrTmrList.WlcHdlrTmrListId,
         &(pWssStaBandSteerDB->StaAssocCountTmr), WLCHDLR_ASSOC_COUNT_TMR,
         u1AssocResetTime, 0) == TMR_FAILURE)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "WlcHdlrStaAssocCountTmrStart: "
                     "Failed to start the Timer Type \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function                  : WlcHdlrProbeTmrStop                           *
 *                                                                           *
 * Description               : This routine stops   the probe timer          *
 *                                                                           *
 * Input                     : None                                          *
 *                                                                           *
 * Output                    : None                                          *
 *                                                                           *
 * Returns                   : None.                                         *
 *                                                                           *
 *****************************************************************************/
INT4
WlcHdlrProbeTmrStop (tWssStaBandSteerDB * pWssStaBandSteerDB)
{
    UINT4               u4RemainingTime = 0;
    UINT4               u4TmrRetVal = TMR_SUCCESS;
    INT4                i4RetVal = OSIX_FAILURE;
    u4TmrRetVal = TmrGetRemainingTime (gWlcHdlrTmrList.WlcHdlrTmrListId,
                                       &(pWssStaBandSteerDB->ProbeEntryTmr.
                                         TimerNode), &u4RemainingTime);
    if (u4TmrRetVal != TMR_FAILURE)
    {
        if (TmrStop (gWlcHdlrTmrList.WlcHdlrTmrListId,
                     &(pWssStaBandSteerDB->ProbeEntryTmr)) != TMR_SUCCESS)
        {
            WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "WlchdlrTmrStop: "
                         "Failure to stop Probe request entry Timer\r\n");
            return i4RetVal;
        }
        u4RemainingTime = 0;
        i4RetVal = OSIX_SUCCESS;
    }
    return i4RetVal;
}

/*****************************************************************************
 * Function                  : WlcHdlrStaAssocTmrStop                        *
 *                                                                           *
 * Description               : This routine stops   the assoc timer          *
 *                                                                           *
 * Input                     : None                                          *
 *                                                                           *
 * Output                    : None                                          *
 *                                                                           *
 * Returns                   : None.                                         *
 *                                                                           *
 *****************************************************************************/
INT4
WlcHdlrStaAssocTmrStop (tWssStaBandSteerDB * pWssStaBandSteerDB)
{
    UINT4               u4RemainingTime = 0;
    UINT4               u4TmrRetVal = TMR_SUCCESS;
    INT4                i4RetVal = OSIX_FAILURE;
    u4TmrRetVal = TmrGetRemainingTime (gWlcHdlrTmrList.WlcHdlrTmrListId,
                                       &(pWssStaBandSteerDB->StaAssocCountTmr.
                                         TimerNode), &u4RemainingTime);
    if (u4TmrRetVal != TMR_FAILURE)
    {
        if (TmrStop (gWlcHdlrTmrList.WlcHdlrTmrListId,
                     &(pWssStaBandSteerDB->StaAssocCountTmr)) != TMR_SUCCESS)
        {
            WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "WlchdlrTmrStop: "
                         "Failure to stop station Assoc Timer\r\n");
            return i4RetVal;
        }
        u4RemainingTime = 0;
        i4RetVal = OSIX_SUCCESS;
    }
    return i4RetVal;
}

/*****************************************************************************
 * Function                  : WlcHdlrStaAssocCountExp                       *
 *                                                                           *
 * Description               : This routine handles the assoc timer          *
 *                             Expiry.                                       *
 *                                                                           *
 * Input                     : None                                          *
 *                                                                           *
 * Output                    : None                                          *
 *                                                                           *
 * Returns                   : None.                                         *
 *                                                                           *
 *****************************************************************************/
VOID
WlcHdlrStaAssocCountExp (VOID *pArg)
{
    tWssStaBandSteerDB *pWssStaBandSteerDB = NULL;
    WLCHDLR_FN_ENTRY ();
    pWssStaBandSteerDB = (tWssStaBandSteerDB *) pArg;
    if (pWssStaBandSteerDB == NULL)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "WlcHdlrStaAssocCountExp: "
                     "Failed BandSteerDB is null\r\n");
        return;
    }
    pWssStaBandSteerDB->u1StaAssocCount = 0;
    if (WssStaUpdateBandSteerProcessDB (WSSSTA_SET_BAND_STEER_DB,
                                        pWssStaBandSteerDB) == OSIX_FAILURE)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "WlcHdlrStaAssocCountExp: "
                     "Failed to Set details in BANDSTEERDB \r\n");
        return;
    }
    /*Stop Sta Assoc timer */
    if (WlcHdlrStaAssocTmrStop (pWssStaBandSteerDB) == OSIX_FAILURE)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "WlcHdlrStaAssocCountExp: "
                     "Failed to Station Assoc  Timer\r\n");
        return;
    }
    WLCHDLR_FN_EXIT ();
    return;
}
#endif
/*****************************************************************************
 * Function                  : WlcHdlrStaConfClearTmrExp                     *
 *                                                                           *
 * Description               : This routine handles the station config Timer *
 *                             Expiry.                                       *
 *                                                                           *
 * Output                    : None                                          *
 *                                                                           *
 * Returns                   : None.                                         *
 *                                                                           *
 *****************************************************************************/
VOID
WlcHdlrStaConfClearTmrExp (VOID *pArg)
{
    WLCHDLR_FN_ENTRY ();

    UNUSED_PARAM (pArg);
    WlcHdlrClearAllStationEntries ();
    WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "WlcHdlrStaConfClearTmrExp: "
                 "WLCHDLR_STA_CONF_PKT_TMR is running \r\n");
    if (TmrStart (gWlcHdlrTmrList.WlcHdlrTmrListId, &tmrWlcStaConfTmrBlk,
                  WLCHDLR_STA_CONF_PKT_TMR, gu4AgeOutTime, 0) == TMR_FAILURE)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "WlcHdlrStaConfClearTmrExp: "
                     "Failed to start the Timer Type \r\n");
    }
    WLCHDLR_FN_EXIT ();
    return;
}

/*****************************************************************************
 * Function                  : WlcHdlrStaConfClearTmrControl                 *
 *                                                                           *
 * Description               : This routine handles the station config Timer *
 *                             Expiry.                                       *
 *                                                                           *
 * Output                    : None                                          *
 *                                                                           *
 * Returns                   : None.                                         *
 *                                                                           *
 *****************************************************************************/
VOID
WlcHdlrStaConfClearTmrControl (UINT4 u4Status, UINT4 u4TimerValue)
{
    if (u4Status == ENABLE)
    {
        gu4AgeOutTime = u4TimerValue;
        if (TmrStart
            (gWlcHdlrTmrList.WlcHdlrTmrListId, &tmrWlcStaConfTmrBlk,
             WLCHDLR_STA_CONF_PKT_TMR, gu4AgeOutTime, 0) == TMR_FAILURE)
        {
            WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "WlcHdlrProbeTmrStart: "
                         "Failed to start the Timer Type \r\n");
        }
    }
    else
    {
        if (TmrStop (gWlcHdlrTmrList.WlcHdlrTmrListId, &tmrWlcStaConfTmrBlk) ==
            TMR_FAILURE)
        {
            WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "WlcHdlrProbeTmrStart: "
                         "Failed to stop the Timer Type \r\n");
        }
    }
}

#endif
