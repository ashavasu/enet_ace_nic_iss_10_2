/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 * $Id: wlchdlrdata.c,v 1.3 2018/01/04 09:54:33 siva Exp $
 * Description: This file contains the WLCHDLR Data Transfer Request
 *
 *******************************************************************/
#ifndef __WLCHDLR_DATA_C__
#define __WLCHDLR_DATA_C__
#include "wlchdlrinc.h"

/************************************************************************/
/*  Function Name   : CapwapProcessDataTransferRequest                  */
/*  Description     : The function processes the received CAPWAP        */
/*                    Data Transfer request                             */
/*  Input(s)        : DataReqMsg - CAPWAP Data request packet received  */
/*                    pSessEntry - Pointer to the session data base     */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/

INT4
CapwapProcessDataTransferRequest (tSegment * pBuf,
                                  tCapwapControlPacket * pDataReqMsg,
                                  tRemoteSessionManager * pSessEntry)
{
    tDataRsp            DataRsp;
    tDataReq           *pDataReq = NULL;
    tSegment           *pTxBuf = NULL;
    UINT1              *pu1TxBuf = NULL;
    UINT1              *pRcvdBuf = NULL;
    UINT1               u1SeqNum = 0;
    UINT2               u2PacketLen;
    UINT1               u1IsNotLinear = OSIX_FALSE;
    UINT1               u1IsNotLinear_Tx = OSIX_FALSE;
    UINT4               u4ResponseCode = CAPWAP_SUCCESS;
    UINT4               u4MsgLen = 0;
    INT4                i4Status = OSIX_SUCCESS;

    WLCHDLR_FN_ENTRY ();
    pDataReq = (tDataReq *) (VOID *) UtlShMemAllocDataReqBuf ();
    if (pDataReq == NULL)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                     "CapwapProcessDataTransferRequest:- "
                     "UtlShMemAllocDataReqBuf returned failure\n");
        return OSIX_FAILURE;
    }

    MEMSET (&DataRsp, 0, sizeof (tDataRsp));
    MEMSET (pDataReq, 0, sizeof (tDataReq));
    if (pSessEntry == NULL)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "CapwapProcessDataTransferRequest ::"
                     "CAPWAP Session Entry is NULL, retrun FAILURE \r\n");
        UtlShMemFreeDataReqBuf ((UINT1 *) pDataReq);
        return OSIX_FAILURE;
    }
    if (pSessEntry->eCurrentState != CAPWAP_RUN)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "CapwapProcessDataTransferRequest ::"
                     "Session not in RUN state \r\n");
        UtlShMemFreeDataReqBuf ((UINT1 *) pDataReq);
        return OSIX_FAILURE;
    }
#if IMPLEMENTATION_ONGOING
    if (pSessEntry->u1DataTransferTimerRunning != OSIX_TRUE)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "CapwapProcessDataTransferRequest ::"
                     "Data Transfer Timer Not Running\r\n");
        CapwapClearDataTransferSessionEntries (pSessEntry);
        UtlShMemFreeDataReqBuf ((UINT1 *) pDataReq);
        return OSIX_FAILURE;
    }
#endif
    u2PacketLen = (UINT2) WLCHDLR_GET_BUF_LEN (pBuf);
    pRcvdBuf = WLCHDLR_BUF_IF_LINEAR (pBuf, 0, u2PacketLen);

    if (pRcvdBuf == NULL)
    {
        pRcvdBuf = (UINT1 *) MemAllocMemBlk (WLCHDLR_DATA_TRANSFER_POOLID);
        if (pRcvdBuf == NULL)
        {
            WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "CapwapProcessDataTransferRequest"
                         ":: Memory Allocation Failed\r\n");
            UtlShMemFreeDataReqBuf ((UINT1 *) pDataReq);
            return OSIX_FAILURE;
        }
        CAPWAP_COPY_FROM_BUF (pBuf, pRcvdBuf, 0, u2PacketLen);
        u1IsNotLinear = OSIX_TRUE;
    }

    /* Recieved sequence number */
    u1SeqNum = pDataReqMsg->capwapCtrlHdr.u1SeqNum;

    if (CapwapValidateDataTransferRequestMsgElements (pRcvdBuf, pDataReqMsg,
                                                      pDataReq) != OSIX_SUCCESS)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "CapwapProcessDataTransferRequest ::"
                     "Data Transfer Request Validation Failed\r\n");
        if (u1IsNotLinear == OSIX_TRUE)
        {
            MemReleaseMemBlock (WLCHDLR_DATA_TRANSFER_POOLID, pRcvdBuf);
        }
        UtlShMemFreeDataReqBuf ((UINT1 *) pDataReq);
        return OSIX_FAILURE;
    }

    if (u1IsNotLinear == OSIX_TRUE)
    {
        MemReleaseMemBlock (WLCHDLR_DATA_TRANSFER_POOLID, pRcvdBuf);
    }

    if (pDataReq->datatransferdata.DataType == DATA_TRANS_DATA)
    {
        if (pSessEntry->u1CrashWriteFileOpened == OSIX_TRUE)
        {
            i4Status = CapwapWriteCrashData (pDataReq->datatransferdata.Data,
                                             pSessEntry,
                                             pDataReq->datatransferdata.
                                             DataLength);
            if (i4Status != OSIX_SUCCESS)
            {
                WLCHDLR_TRC (CAPWAP_FAILURE_TRC,
                             "CapwapProcessDataTransferRequest : Crash File Write"
                             " Failed \r\n");
                u4ResponseCode = CAPWAP_FAILURE;
            }
        }
        else
        {
            i4Status =
                CapwapOpenAndWriteCrashData (pDataReq->datatransferdata.Data,
                                             pSessEntry,
                                             pDataReq->datatransferdata.
                                             DataLength);
            if (i4Status != OSIX_SUCCESS)
            {
                WLCHDLR_TRC (CAPWAP_FAILURE_TRC,
                             "CapwapProcessDataTransferRequest : Crash File Write"
                             " Failed \r\n");
                u4ResponseCode = CAPWAP_FAILURE;
            }
        }
    }
    else
    {
        pSessEntry->u1CrashWriteEndofFileReached = OSIX_TRUE;

        if (pSessEntry->u1CrashWriteFileOpened == OSIX_TRUE)
        {
            i4Status = CapwapWriteCrashData (pDataReq->datatransferdata.Data,
                                             pSessEntry,
                                             pDataReq->datatransferdata.
                                             DataLength);
            if (i4Status != OSIX_SUCCESS)
            {
                WLCHDLR_TRC (CAPWAP_FAILURE_TRC,
                             "CapwapProcessDataTransferRequest : Crash File Write"
                             " Failed \r\n");
                u4ResponseCode = CAPWAP_FAILURE;
            }
        }
        else
        {
            i4Status =
                CapwapOpenAndWriteCrashData (pDataReq->datatransferdata.Data,
                                             pSessEntry,
                                             pDataReq->datatransferdata.
                                             DataLength);
            if (i4Status != OSIX_SUCCESS)
            {
                WLCHDLR_TRC (CAPWAP_FAILURE_TRC,
                             "CapwapProcessDataTransferRequest : Crash File Write "
                             " Failed \r\n");
                u4ResponseCode = CAPWAP_FAILURE;
            }
        }
        pSessEntry->u1CrashWriteEndofFileReached = OSIX_FALSE;
    }

    /* Update the Return Response Code */
    CapwapUpdateResultCode (&DataRsp.resultCode, u4ResponseCode, &u4MsgLen);

    /* Get the values to send out the Data Transfer Response */
    if (CapwapGetWTPDataTransferRspMsgElements (&DataRsp, &u4MsgLen)
        == OSIX_SUCCESS)
    {
        /* update the seq number */
        DataRsp.u1SeqNum = u1SeqNum;
        DataRsp.u2CapwapMsgElemenLen = (UINT2) (u4MsgLen + 3);
        pTxBuf = CAPWAP_ALLOCATE_CRU_BUF (CAPWAP_MAX_PKT_LEN, 0);
        if (pTxBuf == NULL)
        {
            WLCHDLR_TRC (CAPWAP_FAILURE_TRC, "CapwapProcessDataTransferRequest"
                         ": Memory Allocation Failed \r\n");
            UtlShMemFreeDataReqBuf ((UINT1 *) pDataReq);
            return OSIX_FAILURE;
        }
        pu1TxBuf = CAPWAP_BUF_IF_LINEAR (pTxBuf, 0, CAPWAP_MAX_PKT_LEN);
        if (pu1TxBuf == NULL)
        {
            pu1TxBuf = (UINT1 *) MemAllocMemBlk (CAPWAP_PKTBUF_POOLID);
            if (pu1TxBuf == NULL)
            {
                WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                             "CapwapProcessDataTransferRequest :: Memory Allocation"
                             " Failed\r\n");
                CAPWAP_RELEASE_CRU_BUF (pTxBuf);
                UtlShMemFreeDataReqBuf ((UINT1 *) pDataReq);
                return OSIX_FAILURE;
            }
            CAPWAP_COPY_FROM_BUF (pTxBuf, pu1TxBuf, 0, CAPWAP_MAX_PKT_LEN);
            u1IsNotLinear_Tx = OSIX_TRUE;
        }
        if ((CapwapAssembleDataTransferResp (DataRsp.u2CapwapMsgElemenLen,
                                             &DataRsp,
                                             pu1TxBuf)) == OSIX_FAILURE)
        {
            if (u1IsNotLinear_Tx == OSIX_TRUE)
            {
                MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pu1TxBuf);
            }
            CAPWAP_RELEASE_CRU_BUF (pTxBuf);
            UtlShMemFreeDataReqBuf ((UINT1 *) pDataReq);
            return OSIX_FAILURE;
        }
        if (u1IsNotLinear_Tx == OSIX_TRUE)
        {
            /* If the CRU buffer memory is not linear */
            WLCHDLR_COPY_TO_BUF (pTxBuf, pu1TxBuf, 0, CAPWAP_MAX_PKT_LEN);
            MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pu1TxBuf);
        }

        i4Status = CapwapTxMessage (pTxBuf, pSessEntry,
                                    (UINT4) (DataRsp.u2CapwapMsgElemenLen +
                                             CAPWAP_MSG_ELE_LEN),
                                    WSS_CAPWAP_802_11_CTRL_PKT);

        if (i4Status == OSIX_SUCCESS)
        {
            pSessEntry->lastTransmitedSeqNum++;
        }
        else
        {
            WLCHDLR_TRC (CAPWAP_FAILURE_TRC, "Failed to Transmit the "
                         "packet \r\n");
            CAPWAP_RELEASE_CRU_BUF (pTxBuf);
            UtlShMemFreeDataReqBuf ((UINT1 *) pDataReq);
            return OSIX_FAILURE;
        }
        CAPWAP_RELEASE_CRU_BUF (pTxBuf);
    }
    if (u4ResponseCode == CAPWAP_FAILURE)
    {
        /* Clear entries in the pSessEntry */
        CapwapClearDataTransferSessionEntries (pSessEntry);
#if IMPLEMENTATION_ONGOING
        /* Data Transfer Cannot proceed as there is error */
        if (WlcHdlrTmrStop (pSessEntry,
                            WLCHDLR_DATA_TRANSFER_TMR) == OSIX_FAILURE)
        {
            WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "CapwapProcessDataTransferRequest"
                         ":: Data Transfer Timer Stop Failed\r\n");
            UtlShMemFreeDataReqBuf ((UINT1 *) pDataReq);
            return OSIX_FAILURE;
        }
        pSessEntry->u1DataTransferTimerRunning = OSIX_FALSE;
#endif
    }
    UtlShMemFreeDataReqBuf ((UINT1 *) pDataReq);
    WLCHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapProcessDataTransferResp                     */
/*  Description     : The function processes the received CAPWAP        */
/*                    Data response.                                    */
/*  Input(s)        : DataRespMsg- Data response packet received        */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
CapwapProcessDataTransferResp (tSegment * pBuf,
                               tCapwapControlPacket * pDataRespMsg,
                               tRemoteSessionManager * pSessEntry)
{
    tWssIfPMDB          WssIfPMDB;
    UINT1              *pRcvdBuf = NULL;
    UINT2               u2PacketLen;
    UINT4               u4Offset;
    UINT1               u1IsNotLinear = OSIX_FALSE;

    tDataRsp            DataResp;

    WLCHDLR_FN_ENTRY ();
    MEMSET (&DataResp, 0, sizeof (tDataRsp));

    if (pSessEntry == NULL)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "CapwapProcessDataTransferResp :: "
                     "CAPWAP Session Entry is NULL, retrun FAILURE \r\n");
        return OSIX_FAILURE;
    }
    if (pSessEntry->eCurrentState != CAPWAP_RUN)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "CapwapProcessDataTransferResp :: "
                     "Session not in RUN state \r\n");
        if (WlcHdlrTmrStop (pSessEntry, WLCHDLR_RETRANSMIT_INTERVAL_TMR)
            == OSIX_FAILURE)
        {
            WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "CapwapProcessDataTransferRequest"
                         ":: Failed to stop the Retransmit Timer \r\n");
            WlchdlrFreeTransmittedPacket (pSessEntry);
            return OSIX_FAILURE;
        }
        return OSIX_FAILURE;
    }

    if (WlcHdlrTmrStop (pSessEntry, WLCHDLR_RETRANSMIT_INTERVAL_TMR)
        == OSIX_FAILURE)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "CapwapProcessDataTransferRequest ::"
                     "Failed to stop the Retransmit Timer \r\n");
        WlchdlrFreeTransmittedPacket (pSessEntry);
        return OSIX_FAILURE;
    }

    /* Release the Buffer */
    WlchdlrFreeTransmittedPacket (pSessEntry);

    u2PacketLen = (UINT2) WLCHDLR_GET_BUF_LEN (pBuf);
    pRcvdBuf = WLCHDLR_BUF_IF_LINEAR (pBuf, 0, u2PacketLen);

    if (pRcvdBuf == NULL)
    {
        pRcvdBuf = (UINT1 *) MemAllocMemBlk (CAPWAP_PKTBUF_POOLID);
        if (pRcvdBuf == NULL)
        {
            WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "CapwapProcessDataTransferRequest"
                         ":: Memory Allocation Failed\r\n");
            return OSIX_FAILURE;
        }
        CAPWAP_COPY_FROM_BUF (pBuf, pRcvdBuf, 0, u2PacketLen);
        u1IsNotLinear = OSIX_TRUE;
    }

    u4Offset =
        pDataRespMsg->capwapMsgElm[RESULT_CODE_DATA_RESP_INDEX].pu2Offset[0];
    pRcvdBuf += u4Offset + 4;

    CAPWAP_GET_4BYTE (DataResp.resultCode.u4Value, pRcvdBuf);

    if (u1IsNotLinear == OSIX_TRUE)
    {
        MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pRcvdBuf);
    }

    if (DataResp.resultCode.u4Value != CAPWAP_SUCCESS &&
        DataResp.resultCode.u4Value != DATATRANSFER_NO_INFO_TO_TRANSFER)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "CapwapProcessDataTransferRequest ::"
                     "Invalid Result Code Received\r\n");
        MEMSET (&WssIfPMDB, 0, sizeof (tWssIfPMDB));
        WssIfPMDB.u2ProfileId = pSessEntry->u2IntProfileId;
        WssIfPMDB.WssPmWLCCapwapStatsSetFlag.bDataTransferStats = OSIX_TRUE;
        WssIfPMDB.WssPmWLCCapwapStatsSetFlag.bRunUpdateunsuccessfulProcessed =
            OSIX_TRUE;

        if (WssIfProcessPMDBMsg (WSS_PM_WLC_CAPWAP_RUN_STATS_SET,
                                 &WssIfPMDB) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to update PM db\r\n");
        }

        return OSIX_FAILURE;
    }

    if (DataResp.resultCode.u4Value != DATATRANSFER_NO_INFO_TO_TRANSFER)
    {
        /* Clear entries in the pSessEntry */
        CapwapClearDataTransferSessionEntries (pSessEntry);
#if IMPLEMENTATION_ONGOING
        /* Data Transfer Cannot proceed as there is error */
        if (WlcHdlrTmrStop (pSessEntry, WLCHDLR_DATA_TRANSFER_TMR)
            == OSIX_FAILURE)
        {
            WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "CapwapProcessDataTransferRequest"
                         ":: Data Transfer Timer Stop Failed\r\n");
            return OSIX_FAILURE;
        }
        pSessEntry->u1DataTransferTimerRunning = OSIX_FALSE;
#endif
    }

    MEMSET (&WssIfPMDB, 0, sizeof (tWssIfPMDB));
    WssIfPMDB.u2ProfileId = pSessEntry->u2IntProfileId;
    WssIfPMDB.WssPmWLCCapwapStatsSetFlag.bResetStats = OSIX_TRUE;
    WssIfPMDB.WssPmWLCCapwapStatsSetFlag.bRunUpdateRspReceived = OSIX_TRUE;

    if (WssIfProcessPMDBMsg (WSS_PM_WLC_CAPWAP_RUN_STATS_SET,
                             &WssIfPMDB) != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to update PM db\r\n");
    }

    WLCHDLR_FN_EXIT ();

    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapValidateDataTransferRequestMsgElements      */
/*  Description     : The function validated the Data Transfer Request  */
/*                    message elements                                  */
/*                    Data response.                                    */
/*  Input(s)        :                                                   */
/*  Output(s)       :                                                   */
/*  Returns         :                                                   */
/************************************************************************/
INT4
CapwapValidateDataTransferRequestMsgElements (UINT1 *pRcvBuf,
                                              tCapwapControlPacket *
                                              pDataReqMsg, tDataReq * pDataReq)
{
    UINT4               u4Offset;
    WLCHDLR_FN_ENTRY ();
    u4Offset =
        pDataReqMsg->capwapMsgElm[DATA_TRANSFER_DATA_REQ_INDEX].pu2Offset[0];
    pRcvBuf += u4Offset + 4;

    /* read the data type */
    CAPWAP_GET_1BYTE (pDataReq->datatransferdata.DataType, pRcvBuf);
    if (pDataReq->datatransferdata.DataType != DATA_TRANS_DATA
        && pDataReq->datatransferdata.DataType != DATA_TRANS_EOF)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                     "CapwapValidateDataTransferRequestMsgElements :"
                     "Invalid Data Type Received\r\n");
        return OSIX_FAILURE;
    }

    /* read the data mode */
    CAPWAP_GET_1BYTE (pDataReq->datatransferdata.DataMode, pRcvBuf);
    if (pDataReq->datatransferdata.DataMode != DATA_TRANS_MODE_WTP_CRASH)
    {
        /* As of now we do not support MEMORY DUMP DATA */
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                     "CapwapValidateDataTransferRequestMsgElements : "
                     "Invalid Data Mode Received\r\n");
        return OSIX_FAILURE;
    }

    /* read the crash data length */
    CAPWAP_GET_2BYTE (pDataReq->datatransferdata.DataLength, pRcvBuf);

    /* read the crash data */
    CAPWAP_GET_NBYTE (pDataReq->datatransferdata.Data,
                      pRcvBuf, pDataReq->datatransferdata.DataLength);

    /* After reading Data Transfer Data, read the Vendor Specific Info.
       There is no vendor specific message currently sent. */
    WLCHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapOpenAndWriteCrashData                       */
/*  Description     : The function Writes the crash data to the file    */
/*                    first time.                                       */
/*  Input(s)        :                                                   */
/*  Output(s)       :                                                   */
/*  Returns         :                                                   */
/************************************************************************/
INT4
CapwapOpenAndWriteCrashData (UINT1 *pBuf,
                             tRemoteSessionManager * pSess, UINT2 u2WriteLen)
{
    FILE               *fp = NULL;

    WLCHDLR_FN_ENTRY ();

    if (pSess == NULL)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "pSess is NULL\r\n");
        return OSIX_FAILURE;
    }

    if (pSess->u1CrashFileNameSet != OSIX_TRUE)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "CapwapOpenAndWriteCrashData : "
                     "Crash File Name Not Set\r\n");
        return OSIX_FAILURE;
    }

    fp = fopen ((char *) pSess->au1CrashFileName, "wb");
    if (NULL == fp)
    {
        WLCHDLR_TRC (ALL_FAILURE_TRC, "Failed to open the file \n");
        return OSIX_FAILURE;
    }

    if (fseek (fp, 0, SEEK_END) > 0)
    {
        WLCHDLR_TRC (ALL_FAILURE_TRC, "Failed to seek end of file \n");
        fclose (fp);
        return OSIX_FAILURE;
    }
    if (fseek (fp, 0, SEEK_SET) > 0)
    {
        WLCHDLR_TRC (ALL_FAILURE_TRC, "Failed to seek to start of file \n");
        fclose (fp);
        return OSIX_FAILURE;
    }

    pSess->u1CrashWriteFileOpened = OSIX_TRUE;
    pSess->writeCrashFd = fp;

    if (fwrite (pBuf, 1, u2WriteLen, fp) != u2WriteLen)
    {
        WLCHDLR_TRC (ALL_FAILURE_TRC, "Crash File Write Failed\n");
        pSess->u1CrashWriteFileOpened = OSIX_FALSE;
        fclose (pSess->writeCrashFd);
        pSess->writeCrashFd = NULL;
    }
    if (pSess->u1CrashWriteEndofFileReached == OSIX_TRUE)
    {
        pSess->u1CrashWriteFileOpened = OSIX_FALSE;
        if (pSess->writeCrashFd != NULL)
            fclose (pSess->writeCrashFd);
        pSess->writeCrashFd = NULL;
    }

    WLCHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapWriteCrashData                              */
/*  Description     : The function Writes the crash data to the file    */
/*  Input(s)        :                                                   */
/*  Output(s)       :                                                   */
/*  Returns         :                                                   */
/************************************************************************/
INT4
CapwapWriteCrashData (UINT1 *pBuf,
                      tRemoteSessionManager * pSess, UINT2 u2WriteLen)
{
    WLCHDLR_FN_ENTRY ();
    if (pSess == NULL)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "pSess is NULL\r\n");
        return OSIX_FAILURE;
    }
    if (pBuf == NULL)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "pBuf is NULL\r\n");
        return OSIX_FAILURE;
    }

    if (pSess->u1CrashWriteEndofFileReached == OSIX_TRUE)
    {
        if (pSess->writeCrashFd == NULL)
        {
            WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "Write Crash Fd is NULL\r\n");
            return OSIX_FAILURE;
        }

        if (fwrite (pBuf, 1, u2WriteLen, pSess->writeCrashFd) != u2WriteLen)
        {
            WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "CapwapWriteCrashData : Crash "
                         "File Write Failed\r\n");
            return OSIX_FAILURE;
        }
        if (pSess->writeCrashFd != NULL)
        {
            fclose (pSess->writeCrashFd);
            pSess->writeCrashFd = NULL;
        }
        pSess->u1CrashWriteFileOpened = OSIX_FALSE;
    }
    else
    {
        if (pSess->writeCrashFd == NULL)
        {
            WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "Write Crash Fd is NULL\r\n");
            return OSIX_FAILURE;
        }

        if (fwrite (pBuf, 1, u2WriteLen, pSess->writeCrashFd) != u2WriteLen)
        {
            WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "CapwapWriteCrashData : "
                         "Crash File Write Failed\r\n");
            return OSIX_FAILURE;
        }
    }
    WLCHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapGetWTPDataTransferRspMsgElements            */
/*  Description     : The function gets the DTRs message elements       */
/*  Input(s)        :                                                   */
/*  Output(s)       :                                                   */
/*  Returns         :                                                   */
/************************************************************************/
INT4
CapwapGetWTPDataTransferRspMsgElements (tDataRsp * pDataRsp, UINT4 *u4MsgLen)
{
    UNUSED_PARAM (u4MsgLen);

    CAPWAP_FN_ENTRY ();

    if (CapwapConstructCpHeader (&pDataRsp->capwapHdr) == OSIX_FAILURE)
    {
        WLCHDLR_TRC (CAPWAP_FAILURE_TRC,
                     "CapwapGetWTPDataTransferRspMsgElements :Failed to construct "
                     "the capwap Header\n");
        return OSIX_FAILURE;
    }

    /* update Control Header */
    pDataRsp->u1NumMsgBlocks = 0;    /* flags */

    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapClearDataTransferSessionEntries             */
/*  Description     : The function clears the DTR session entries       */
/*  Input(s)        :                                                   */
/*  Output(s)       :                                                   */
/*  Returns         :                                                   */
/************************************************************************/
VOID
CapwapClearDataTransferSessionEntries (tRemoteSessionManager * pSessEntry)
{
    if (pSessEntry == NULL)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "Session in Disconnected State \r\n");
        return;
    }

    /*Reset the Data Transfer Info fore the WLC(write) side */
    if (pSessEntry->writeCrashFd != NULL)
    {
        fclose (pSessEntry->writeCrashFd);
        pSessEntry->writeCrashFd = NULL;
    }
    pSessEntry->u1CrashWriteFileOpened = OSIX_FALSE;
}
#endif
