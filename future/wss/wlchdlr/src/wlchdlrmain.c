/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: wlchdlrmain.c,v 1.5.2.1 2018/03/07 12:52:25 siva Exp $
 *
 * Description: This file contains the WLCHDLR Main and Init routines.
 *
 *****************************************************************************/

#ifndef __WLCHDLRMAIN_C__
#define __WLCHDLRMAIN_C__

#include "wlchdlrinc.h"
#include "wssifpmdb.h"
#include "wsscfgtmr.h"
#include "wsscfgprot.h"
#include "fswebnm.h"
#include "wlchdlr.h"
#ifdef KERNEL_CAPWAP_WANTED
#include "fcusprot.h"
#endif
#include "wssstawlcglob.h"
extern UINT4        gu4WssStaWebAuthDebugMask;
static tWlcHdlrTaskGlobals gWlcHdlrTaskGlobals;
extern INT1         WebnmWebAuthRadiusAuthentication (INT1 *, INT1 *, tMacAddr);
tRBTree             gWlcHdlrStaConfigDB;

/* CPU relinquish counter */
static UINT4        gu4CPUWlchdlrRelinquishCounter = 0;

VOID                WlcHdlrCheckRelinquishCounters (VOID);
extern eState       gCapwapState;
UINT1               GET_SKIP_STRING[] = "HTTP/1.1\r\n";
UINT4               gu4WlcHdlrSysLogId = 0;
extern INT1         nmhGetFsDot11WlanLoginAuthentication (INT4, UINT4 *);

/*****************************************************************************/
/* Function Name      : WlchdlrGetStaRedirectionUrl                          */
/*                                                                           */
/* Description        : This function is to return the RedirectionURL        */
/*                      for a particular station.                            */
/*                                                                           */
/* Input(s)           : Station Mac Address , pointer to URL                 */
/*                                                                           */
/* Output(s)          : URL to which the particular station is to be         */
/*                      redirected                                           */
/*                                                                           */
/* Return Value(s)    : SUCCESS on returning correct URL                     */
/*****************************************************************************/

INT4
WlchdlrGetStaRedirectionUrl (tMacAddr StaMacAddr, UINT1 *pu1StaRedirectionURL)
{
    tWssifauthDBMsgStruct WssifauthDBMsgStruct;
    MEMSET (&WssifauthDBMsgStruct, 0, sizeof (tWssifauthDBMsgStruct));

    MEMCPY (WssifauthDBMsgStruct.WssIfAuthStateDB.
            stationMacAddress, StaMacAddr, sizeof (tMacAddr));

    if (WssIfProcessWssAuthDBMsg (WSS_AUTH_GET_STATION_DB,
                                  &WssifauthDBMsgStruct) != OSIX_SUCCESS)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "WlchdlrGetStaRedirectionUrl:"
                     "WssAuth DB Failed to get Auth Status for "
                     "Station Mac Address\r\n");
        return OSIX_FAILURE;
    }
    MEMCPY (pu1StaRedirectionURL,
            WssifauthDBMsgStruct.WssIfAuthStateDB.au1StaRedirectionURL,
            STRLEN (WssifauthDBMsgStruct.WssIfAuthStateDB.
                    au1StaRedirectionURL));
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : WlcHdlrEnqueCtrlPkts                                 */
/*                                                                           */
/* Description        : This function is to enque received packets           */
/*                      to the WLC HDLR task                                 */
/*                                                                           */
/* Input(s)           : pQMsg - Received queue message                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
WlcHdlrEnqueCtrlPkts (unWlcHdlrMsgStruct * pMsg)
{
    INT1                i4Status = OSIX_SUCCESS;
    tWlcHdlrQueueReq   *pWlcHdlrQueueReq = NULL;

    WLCHDLR_FN_ENTRY ();

    pWlcHdlrQueueReq = (tWlcHdlrQueueReq *)
        MemAllocMemBlk (WLCHDLR_QUEUE_POOLID);
    if (pWlcHdlrQueueReq == NULL)
    {
        WLCHDLR_TRC2 (WLCHDLR_FAILURE_TRC, "Memory Allocation \
                Failed %s Line %d\r\n", __FUNCTION__, __LINE__);
        if (pMsg->WlcHdlrQueueReq.pRcvBuf != NULL)
        {
            WLCHDLR_RELEASE_CRU_BUF (pMsg->WlcHdlrQueueReq.pRcvBuf);
        }
        return OSIX_FAILURE;
    }

    if (pMsg->WlcHdlrQueueReq.pRcvBuf == NULL)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "Received buffer in the WLCHDLR "
                     "Queue is NULL\r\n");
        MemReleaseMemBlock (WLCHDLR_QUEUE_POOLID, (UINT1 *) pWlcHdlrQueueReq);
        return OSIX_FAILURE;
    }

    pWlcHdlrQueueReq->u4MsgType = pMsg->WlcHdlrQueueReq.u4MsgType;
    pWlcHdlrQueueReq->pRcvBuf = pMsg->WlcHdlrQueueReq.pRcvBuf;

    pWlcHdlrQueueReq->u4DestIp =
        CRU_BUF_Get_U4Reserved2 (pMsg->WlcHdlrQueueReq.pRcvBuf);
    pWlcHdlrQueueReq->u4DestPort =
        CRU_BUF_Get_U4Reserved1 (pMsg->WlcHdlrQueueReq.pRcvBuf);

    pWlcHdlrQueueReq->u2SessId = pMsg->WlcHdlrQueueReq.u2SessId;

    switch (pWlcHdlrQueueReq->u4MsgType)
    {
        case WLCHDLR_RELEASE_LAST_TRANSMIT_MSG:
        case WLCHDLR_CTRL_MSG:
            if (OsixQueSend (gWlcHdlrTaskGlobals.ctrlRxMsgQId,
                             (UINT1 *) &pWlcHdlrQueueReq,
                             OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
            {
                WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                             "Failed to send the message to CTRL Rx queue \r\n");
                WLCHDLR_RELEASE_CRU_BUF (pMsg->WlcHdlrQueueReq.pRcvBuf);
                MemReleaseMemBlock (WLCHDLR_QUEUE_POOLID,
                                    (UINT1 *) pWlcHdlrQueueReq);
                return OSIX_FAILURE;
            }
            /*post event to Service Task */
            if (OsixEvtSend (gWlcHdlrTaskGlobals.wlcHdlrTaskId,
                             WLCHDLR_CTRL_RX_MSGQ_EVENT) == OSIX_FAILURE)
            {
                WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                             "WlcHdlrEnqueCtrlPkts: Failed to send Event \r\n");
                WLCHDLR_RELEASE_CRU_BUF (pMsg->WlcHdlrQueueReq.pRcvBuf);
                MemReleaseMemBlock (WLCHDLR_QUEUE_POOLID,
                                    (UINT1 *) pWlcHdlrQueueReq);
                i4Status = OSIX_FAILURE;
            }
            break;
        case WLCHDLR_DATA_RX_MSG:
            MEMCPY (pWlcHdlrQueueReq->BssId, pMsg->WlcHdlrQueueReq.BssId,
                    sizeof (tMacAddr));
            pWlcHdlrQueueReq->u1MacType = pMsg->WlcHdlrQueueReq.u1MacType;
            pWlcHdlrQueueReq->u2VlanId = pMsg->WlcHdlrQueueReq.u2VlanId;
            if (OsixQueSend (gWlcHdlrTaskGlobals.dataRxMsgQId,
                             (UINT1 *) &pWlcHdlrQueueReq,
                             OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
            {
                WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                             "Failed to send the message to DATA Rx queue \r\n");
                WLCHDLR_RELEASE_CRU_BUF (pMsg->WlcHdlrQueueReq.pRcvBuf);
                MemReleaseMemBlock (WLCHDLR_QUEUE_POOLID,
                                    (UINT1 *) pWlcHdlrQueueReq);
                return OSIX_FAILURE;
            }
            /*post event to Service Task */
            if (OsixEvtSend (gWlcHdlrTaskGlobals.wlcHdlrTaskId,
                             WLCHDLR_DATA_RX_MSGQ_EVENT) == OSIX_FAILURE)
            {
                WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                             "WlcHdlrEnqueCtrlPkts: Failed to send Event \r\n");
                WLCHDLR_RELEASE_CRU_BUF (pMsg->WlcHdlrQueueReq.pRcvBuf);
                MemReleaseMemBlock (WLCHDLR_QUEUE_POOLID,
                                    (UINT1 *) pWlcHdlrQueueReq);
                i4Status = OSIX_FAILURE;
            }
            break;
        case WLCHDLR_DATA_RX_PROBE_MSG:
            pWlcHdlrQueueReq->u1MacType = pMsg->WlcHdlrQueueReq.u1MacType;
            if (OsixQueSend (gWlcHdlrTaskGlobals.dataRxMsgQId,
                             (UINT1 *) &pWlcHdlrQueueReq,
                             OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
            {
                WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                             "Failed to send the message to DATA Rx queue \r\n");
                WLCHDLR_RELEASE_CRU_BUF (pMsg->WlcHdlrQueueReq.pRcvBuf);
                MemReleaseMemBlock (WLCHDLR_QUEUE_POOLID,
                                    (UINT1 *) pWlcHdlrQueueReq);
                return OSIX_FAILURE;
            }
            /*post event to Service Task */
            if (OsixEvtSend (gWlcHdlrTaskGlobals.wlcHdlrTaskId,
                             WLCHDLR_DATA_RX_MSGQ_EVENT) == OSIX_FAILURE)
            {
                WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                             "WlcHdlrEnqueCtrlPkts: Failed to send Event \r\n");
                WLCHDLR_RELEASE_CRU_BUF (pMsg->WlcHdlrQueueReq.pRcvBuf);
                MemReleaseMemBlock (WLCHDLR_QUEUE_POOLID,
                                    (UINT1 *) pWlcHdlrQueueReq);
                i4Status = OSIX_FAILURE;
            }
            break;
        case WLCHDLR_DATA_TX_MSG:
            if (OsixQueSend (gWlcHdlrTaskGlobals.dataTxMsgQId,
                             (UINT1 *) &pWlcHdlrQueueReq,
                             OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
            {
                WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                             "Failed to send the message to DATA Tx queue \r\n");
                WLCHDLR_RELEASE_CRU_BUF (pMsg->WlcHdlrQueueReq.pRcvBuf);
                MemReleaseMemBlock (WLCHDLR_QUEUE_POOLID,
                                    (UINT1 *) pWlcHdlrQueueReq);
                return OSIX_FAILURE;
            }
            /*post event to Service Task */
            if (OsixEvtSend (gWlcHdlrTaskGlobals.wlcHdlrTaskId,
                             WLCHDLR_DATA_TX_MSGQ_EVENT) == OSIX_FAILURE)
            {
                WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                             "WlcHdlrEnqueCtrlPkts: Failed to send Event \r\n");
                WLCHDLR_RELEASE_CRU_BUF (pMsg->WlcHdlrQueueReq.pRcvBuf);
                MemReleaseMemBlock (WLCHDLR_QUEUE_POOLID,
                                    (UINT1 *) pWlcHdlrQueueReq);
                i4Status = OSIX_FAILURE;
            }
            break;
        case WLCHDLR_CFA_RX_MSG:
            if (OsixQueSend (gWlcHdlrTaskGlobals.dataRxMsgQId,
                             (UINT1 *) &pWlcHdlrQueueReq,
                             OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
            {
                WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                             "Failed to send the message to DATA Rx queue \r\n");
                WLCHDLR_RELEASE_CRU_BUF (pMsg->WlcHdlrQueueReq.pRcvBuf);
                MemReleaseMemBlock (WLCHDLR_QUEUE_POOLID,
                                    (UINT1 *) pWlcHdlrQueueReq);
                return OSIX_FAILURE;
            }
            /*post event to Service Task */
            if (OsixEvtSend (gWlcHdlrTaskGlobals.wlcHdlrTaskId,
                             WLCHDLR_DATA_RX_MSGQ_EVENT) == OSIX_FAILURE)
            {
                WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                             "WlcHdlrEnqueCtrlPkts: Failed to send Event \r\n");
                WLCHDLR_RELEASE_CRU_BUF (pMsg->WlcHdlrQueueReq.pRcvBuf);
                MemReleaseMemBlock (WLCHDLR_QUEUE_POOLID,
                                    (UINT1 *) pWlcHdlrQueueReq);
                i4Status = OSIX_FAILURE;
            }
            break;
        default:
            WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                         "WlcHdlrEnqueCtrlPkts: unknown msg queue \r\n");
            WLCHDLR_RELEASE_CRU_BUF (pMsg->WlcHdlrQueueReq.pRcvBuf);
            MemReleaseMemBlock (WLCHDLR_QUEUE_POOLID,
                                (UINT1 *) pWlcHdlrQueueReq);
            i4Status = OSIX_FAILURE;
    }
    WLCHDLR_FN_EXIT ();
    return i4Status;
}

INT4
WlcHdlrEnqueCfaTxPkts (UINT1 MsgType, unWlcHdlrMsgStruct * pMsg)
{
    tWssMacDot11PktBuf *pMacDot11Pkt = NULL;
    tCfaMsgStruct       CfaMsgStruct;

    MEMSET (&CfaMsgStruct, 0, sizeof (tCfaMsgStruct));

    pMacDot11Pkt = &(pMsg->WssMacMsgStruct.unMacMsg.MacDot11PktBuf);

    if (WSS_WLCHDLR_CFA_TX_BUF != MsgType)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                     "\r\nWlcHdlrEnqueCfaTxPkts:Invalid MsgType\n");
        return OSIX_FAILURE;
    }

    CfaMsgStruct.unCfaMsg.CfaDot11PktBuf.u4IfIndex = pMacDot11Pkt->u4IfIndex;
    CfaMsgStruct.unCfaMsg.CfaDot11PktBuf.pTxPktBuf = pMacDot11Pkt->pDot11MacPdu;
    CfaMsgStruct.unCfaMsg.CfaDot11PktBuf.u2VlanId = pMacDot11Pkt->u2VlanId;
    if (WssIfProcessCfaMsg (CFA_WSS_TX_DOT3_PKT, &CfaMsgStruct) != OSIX_SUCCESS)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                     "\r\nWlcHdlrEnqueCfaTxPkts:WssIfProcessCfaMsg failed\n");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : WlcHdlrVerifyStaPkts                                       *
 *                                                                           *
 * Description  : Function to decide whether packet need to go to STA or CFA *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS, if initialization succeeds                   *
 *                OSIX_FAILURE, otherwise                                    *
 *                                                                           *
 *****************************************************************************/

INT4
WlcHdlrVerifyStaPkts (unWlcHdlrMsgStruct * pWlcHdlrMsgStruct)
{
    tWssifauthDBMsgStruct *pWssifauthDBMsgStruct = NULL;
    tWssWlanDB         *pWssWlanDB = NULL;
    tIpAddr             IpAddr;
#ifdef RSNA_WANTED
    INT4                i4RetValDot11RSNAEnabled = RSNA_DISABLED;
#endif
#ifdef WPS_WANTED
    INT4                i4WPSEnabled = WPS_DISABLED;
#endif
    UINT1               REDIRECTION_URL_TEMP[STA_REDIRECTION_URL_LENGTH];
    UINT1               WEBAUTH_EXTERNAL_URL[120];
    UINT4               u4ArpIp = 0;
    UINT2               u2EthType = 0;
    UINT1               u1Proto = 0;
    UINT2               u2Dport = 0;
    UINT2               u2Sport = 0;
    UINT1              *pBuf = NULL;
    UINT1              *pTemp = NULL;
    UINT2               u2PacketLen = 0;
    UINT1               u1IsNotLinear = OSIX_FALSE;
    INT1                i1Char = 0;
    UINT2               i = 0;
    UINT2               u2ReadBytes = 0;
    UINT1               u1Char = 0;
    INT1                ai1Line[STA_REDIRECTION_URL_LENGTH];

    UINT1               u1Hlen = 0, u1TcpHdrLen = 0;
    UINT2               u2TotLen = 0;
    UINT1               au1StaMacAddr[MAC_ADDR_LEN] = { 0 };
    UINT1               u1ReqMethod = 0;
    UINT4               u4SrcIp = 0;
    INT4                i4LoginAuth = 0;
    INT1                ai1UserName[64] = { 0 };
    INT1                ai1Password[64] = { 0 };
    UINT1               u1ParsingDone = 0;
    UINT1               au1DstMacAddr[MAC_ADDR_LEN] = { 0 };
    UINT1               au1SysMac[MAC_ADDR_LEN] = { 0 };
    INT4                i4HttpPort = 0;
    INT4                i4RetVal = 0;

    MEMSET (ai1Line, 0, sizeof (ai1Line));
    MEMSET (REDIRECTION_URL_TEMP, 0, STA_REDIRECTION_URL_LENGTH);

    pWssWlanDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
    if (pWssWlanDB == NULL)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "WlcHdlrVerifyStaPkts:"
                     "WlanDbBuf Failed to allocate memory\r\n");
        return OSIX_FAILURE;

    }
    MEMSET (pWssWlanDB, 0, sizeof (tWssWlanDB));

    pWssifauthDBMsgStruct =
        (tWssifauthDBMsgStruct *) (VOID *) UtlShMemAllocWssIfAuthDbBuf ();
    if (pWssifauthDBMsgStruct == NULL)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "WlcHdlrVerifyStaPkts:"
                     "WssIfAuthDbBuf Failed to allocate memory\r\n");

        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        return OSIX_FAILURE;
    }
    MEMSET (pWssifauthDBMsgStruct, 0, sizeof (tWssifauthDBMsgStruct));
    /* Take a copy of MAC pdu */
    u2PacketLen =
        (UINT2) WLCHDLR_GET_BUF_LEN (pWlcHdlrMsgStruct->WssMacMsgStruct.
                                     unMacMsg.MacDot11PktBuf.pDot11MacPdu);
    pBuf =
        WLCHDLR_BUF_IF_LINEAR (pWlcHdlrMsgStruct->WssMacMsgStruct.unMacMsg.
                               MacDot11PktBuf.pDot11MacPdu, 0, u2PacketLen);
    if (pBuf == NULL)
    {
        WLCHDLR_PKTBUF_ALLOC_MEM_BLOCK (pBuf);
        /* Kloc Fix Start */
        if (pBuf == NULL)
        {
            WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "Memory failed to "
                         "allocate from WLCHDLR_PKTBUF_POOLID \r\n");
            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssifauthDBMsgStruct);
            return OSIX_FAILURE;
        }
        pTemp = pBuf;
        /* Kloc Fix Ends */
        WLCHDLR_COPY_FROM_BUF (pWlcHdlrMsgStruct->WssMacMsgStruct.unMacMsg.
                               MacDot11PktBuf.pDot11MacPdu, pBuf, 0,
                               u2PacketLen);
        u1IsNotLinear = OSIX_TRUE;
    }

    MEMCPY (pWssWlanDB->WssWlanAttributeDB.BssId,
            pWlcHdlrMsgStruct->WssMacMsgStruct.unMacMsg.MacDot11PktBuf.BssIdMac,
            MAC_ADDR_LEN);
    pWssWlanDB->WssWlanIsPresentDB.bWebAuthStatus = OSIX_TRUE;
    pWssWlanDB->WssWlanIsPresentDB.bBssIfIndex = OSIX_TRUE;
    pWssWlanDB->WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSSID_MAPPING_ENTRY,
                                  pWssWlanDB) != OSIX_SUCCESS)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "WlcHdlrVerifyStaPkts:"
                     "Wlan DB Failed to return WebAuthStatus\r\n");
        if (u1IsNotLinear == OSIX_TRUE)
        {
            WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pTemp);
        }
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssifauthDBMsgStruct);
        return OSIX_FAILURE;
    }

    pWssWlanDB->WssWlanIsPresentDB.bExternalWebAuthMethod = OSIX_TRUE;
    pWssWlanDB->WssWlanIsPresentDB.bExternalWebAuthUrl = OSIX_TRUE;

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY,
                                  pWssWlanDB) != OSIX_SUCCESS)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "WlcHdlrVerifyStaPkts:"
                     "Wlan DB Failed to return WebAuth info\r\n");
        if (u1IsNotLinear == OSIX_TRUE)
        {
            WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pTemp);
        }
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssifauthDBMsgStruct);
        return OSIX_FAILURE;
    }

    /* To avoid the situation of the PNAC starting the state
     * machine for the first arrival packet, when the race condition of
     * first data packet is seen by the PNAC module before the 
     * RSNA association processing is completed, let us check the
     * received station's data packet is WLAN is RSNA enabled and
     * the RSNA association is completed for that station.
     * Based on this let proceed processing this packet or drop it
     */

    /* Verify if the RSNA is Enabled on the received packets WLAN */
    CAPWAP_GET_NBYTE (au1DstMacAddr, pBuf, sizeof (tMacAddr));
    CAPWAP_GET_NBYTE (pWssifauthDBMsgStruct->WssIfAuthStateDB.
                      stationMacAddress, pBuf, sizeof (tMacAddr));
    MEMCPY (au1StaMacAddr,
            pWssifauthDBMsgStruct->WssIfAuthStateDB.stationMacAddress,
            MAC_ADDR_LEN);
    CAPWAP_GET_2BYTE (u2EthType, pBuf);

#ifdef RSNA_WANTED

    if (RsnaGetDot11RSNAEnabled
        ((INT4) pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex,
         &i4RetValDot11RSNAEnabled) != SNMP_FAILURE)
    {
        if (i4RetValDot11RSNAEnabled == RSNA_ENABLED)
        {
            if ((u2EthType != 0x888E) &&
                (RsnaGetIsPtkInDoneState
                 (pWssifauthDBMsgStruct->WssIfAuthStateDB.stationMacAddress) !=
                 RSNA_SUCCESS))
            {
#ifdef WPS_WANTED
                if (WpsGetWPSEnabled
                    ((INT4) pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex,
                     pWssWlanDB->WssWlanAttributeDB.u2WtpInternalId,
                     &i4WPSEnabled) != SNMP_FAILURE)
                {
                    if (i4WPSEnabled != WPS_ENABLED)
                    {
#endif
                        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                                     "WlcHdlrVerifyStaPkts:"
                                     "First Data Packet of incompleted RSNA association dropped \r\n");

                        if (u1IsNotLinear == OSIX_TRUE)
                        {
                            WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pTemp);
                        }

                        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *)
                                                    pWssifauthDBMsgStruct);
                        return OSIX_FAILURE;
#ifdef WPS_WANTED
                    }
                }
#endif
            }
        }
    }

#endif

    if (pWssWlanDB->WssWlanAttributeDB.u1WebAuthStatus == WSSWLAN_ENABLE)
    {
        if (WssIfProcessWssAuthDBMsg (WSS_AUTH_GET_STATION_DB,
                                      pWssifauthDBMsgStruct) != OSIX_SUCCESS)
        {
            WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "WlcHdlrVerifyStaPkts:"
                         "WssAuth DB Failed to get Auth Status for"
                         "Station Mac Address\r\n");
            if (u1IsNotLinear == OSIX_TRUE)
            {
                WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pTemp);
            }
            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssifauthDBMsgStruct);
            return OSIX_FAILURE;
        }
        if (pWssifauthDBMsgStruct->WssIfAuthStateDB.u1StaAuthFinished !=
            OSIX_TRUE)
        {
            CAPWAP_SKIP_N_BYTES (SHIFT_2, pBuf);
            CAPWAP_GET_2BYTE (u2TotLen, pBuf);
            if (u2EthType == ARP_ETHTYPE)
            {
                CAPWAP_SKIP_N_BYTES (SHIFT_20, pBuf);
                CAPWAP_GET_4BYTE (u4ArpIp, pBuf);

                IpAddr.u4_addr[0] = IssGetIpAddrFromNvRam ();
                if (u1IsNotLinear == OSIX_TRUE)
                {
                    WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pTemp);
                }
                UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssifauthDBMsgStruct);
                /* ToDo: Return SUCCESS for now */
                return OSIX_SUCCESS;
                /*  return NO_NEED_TO_PROCESS; */
            }
            /* Check whether the packet is IP */
            if (u2EthType == IP_ETHTYPE)
            {
                /* CAPWAP_SKIP_N_BYTES(9 , pBuf); */
                CAPWAP_SKIP_N_BYTES (SHIFT_5, pBuf);
                CAPWAP_GET_1BYTE (u1Proto, pBuf);

                /* When router present between WLC and WTP below changes required */
                CAPWAP_SKIP_N_BYTES (2, pBuf);
                CAPWAP_GET_4BYTE (u4SrcIp, pBuf);

#ifdef KERNEL_CAPWAP_WANTED
                CapwapUpdateArpTable (u4SrcIp,
                                      pWssifauthDBMsgStruct->WssIfAuthStateDB.
                                      stationMacAddress);
#endif
                if (u1Proto == ICMP_PROTOCOL_TYPE)
                    /* discard ping packets before web authentication */
                {
                    if (u1IsNotLinear == OSIX_TRUE)
                    {
                        WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pTemp);
                    }
                    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *)
                                                pWssifauthDBMsgStruct);
                    return NO_NEED_TO_PROCESS;
                }
                /* Check whether the packet protocol is TCP */
                if (u1Proto == TCP_PROTOCOL_TYPE)
                {
                    CAPWAP_SKIP_N_BYTES (6, pBuf);
                    CAPWAP_GET_2BYTE (u2Dport, pBuf);

                    /* Check whether the packet is destined to HTTP port */
                    i4HttpPort = HttpGetSourcePort ();
                    if ((u2Dport == (UINT2) i4HttpPort) ||
                        (u2Dport == HTTP_DESTINATION_PORT))
                    {

                        WSSSTA_WEBAUTH_TRC6 (WSSSTAWEBAUTH_MASK,
                                             "Recv Req from STA %02x:%02x:%02x:%02x:%02x:%02x ",
                                             pWssifauthDBMsgStruct->
                                             WssIfAuthStateDB.
                                             stationMacAddress[0],
                                             pWssifauthDBMsgStruct->
                                             WssIfAuthStateDB.
                                             stationMacAddress[1],
                                             pWssifauthDBMsgStruct->
                                             WssIfAuthStateDB.
                                             stationMacAddress[2],
                                             pWssifauthDBMsgStruct->
                                             WssIfAuthStateDB.
                                             stationMacAddress[3],
                                             pWssifauthDBMsgStruct->
                                             WssIfAuthStateDB.
                                             stationMacAddress[4],
                                             pWssifauthDBMsgStruct->
                                             WssIfAuthStateDB.
                                             stationMacAddress[5]);

                        WSSSTA_WEBAUTH_TRC1 (WSSSTAWEBAUTH_MASK, "on AP %d\n",
                                             pWlcHdlrMsgStruct->WssMacMsgStruct.
                                             unMacMsg.MacDot11PktBuf.u2SessId);

                        CAPWAP_SKIP_N_BYTES (SHIFT_8, pBuf);
                        CAPWAP_GET_1BYTE (u1Hlen, pBuf);

                        u1TcpHdrLen =
                            (UINT1) ((((u1Hlen) & TCP_IPVER_MASK) >> 4) * 4);

                        if ((u2TotLen - (u1TcpHdrLen + IP_HDR_LEN)) > 0)
                        {
                            CAPWAP_SKIP_N_BYTES ((u1TcpHdrLen + SKIP_TO_HTTP),
                                                 pBuf);
                            i = 0;
                            while (1)
                                /* for(i=0; i<8; i++) */
                            {
                                CAPWAP_GET_1BYTE (u1Char, pBuf);
                                i1Char = (INT1) u1Char;
                                u2ReadBytes++;

                                if (i1Char == -1)
                                {
                                    WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                                                 "WLCHDLR:Failed to Parse the HTTP packet \n");
                                }
                                if ((i1Char == ' ') ||
                                    (STRLEN ("GET") < STRLEN (ai1Line)))
                                {
                                    ai1Line[i] = '\0';
                                    break;
                                }
                                ai1Line[i] = i1Char;
                                i++;

                                if ((u2ReadBytes >= u2PacketLen) ||
                                    (u2ReadBytes > STA_REDIRECTION_URL_LENGTH))
                                {
                                    if (u2ReadBytes >
                                        STA_REDIRECTION_URL_LENGTH)
                                    {
                                        WSSSTA_WEBAUTH_TRC1 (WSSSTAWEBAUTH_MASK,
                                                             "WlcHdlrVerifyStaPkts() ==> %d ReadBytes Exceeds configured length\r\n",
                                                             __LINE__);
                                    }
                                    if (u1IsNotLinear == OSIX_TRUE)
                                    {
                                        WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK
                                            (pTemp);
                                    }
                                    UtlShMemFreeWlanDbBuf ((UINT1 *)
                                                           pWssWlanDB);
                                    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *)
                                                                pWssifauthDBMsgStruct);
                                    /* Packet Completely parsed. */
                                    return OSIX_FAILURE;
                                }
                            }

                            u2PacketLen = u2PacketLen - u2ReadBytes;

                            u2ReadBytes = 0;

                            if ((STRCMP (ai1Line, "GET") == 0) ||
                                (STRCMP (ai1Line, "POST") == 0))
                            {
                                if (STRCMP (ai1Line, "GET") == 0)
                                {
                                    u1ReqMethod = WEBAUTH_HTTP_GET;
                                }
                                else
                                {
                                    u1ReqMethod = WEBAUTH_HTTP_POST;
                                }

                                i = 0;
                                while (1)
                                {
                                    CAPWAP_GET_1BYTE (u1Char, pBuf);
                                    i1Char = (INT1) u1Char;

                                    u2ReadBytes++;

                                    if (i1Char == -1)
                                    {
                                        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                                                     "WLCHDLR:Failed to Parse the HTTP packet \n");
                                    }
                                    if ((i1Char == ' ') ||
                                        (STRLEN (ai1Line) ==
                                         (sizeof (ai1Line) - 1)))
                                    {
                                        ai1Line[i] = '\0';
                                        break;
                                    }
                                    ai1Line[i] = i1Char;
                                    i++;

                                    if ((u2ReadBytes >= u2PacketLen) ||
                                        (u2ReadBytes >
                                         STA_REDIRECTION_URL_LENGTH))
                                    {
                                        if (u2ReadBytes >
                                            STA_REDIRECTION_URL_LENGTH)
                                        {
                                            WSSSTA_WEBAUTH_TRC1
                                                (WSSSTAWEBAUTH_MASK,
                                                 "WlcHdlrVerifyStaPkts() ==> %d ReadBytes Exceeds configured length\r\n",
                                                 __LINE__);
                                        }
                                        if (u1IsNotLinear == OSIX_TRUE)
                                        {
                                            WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK
                                                (pTemp);
                                        }
                                        UtlShMemFreeWlanDbBuf ((UINT1 *)
                                                               pWssWlanDB);
                                        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *)
                                                                    pWssifauthDBMsgStruct);
                                        /* Packet Completely parsed. */
                                        return OSIX_FAILURE;
                                    }

                                }
                                u2PacketLen = u2PacketLen - u2ReadBytes;

                                u2ReadBytes = 0;

                                if (MEMCMP
                                    (ai1Line, "/iss/specific/",
                                     STRLEN ("/iss/specific/")) == 0 ||
                                    MEMCMP
                                    (ai1Line, "/favicon.ico",
                                     STRLEN ("/favicon.ico")) == 0)
                                {
                                    if (u1IsNotLinear == OSIX_TRUE)
                                    {
                                        WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK
                                            (pTemp);
                                    }
                                    UtlShMemFreeWlanDbBuf ((UINT1 *)
                                                           pWssWlanDB);
                                    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *)
                                                                pWssifauthDBMsgStruct);
                                    return OSIX_SUCCESS;
                                }

                                CAPWAP_SKIP_N_BYTES (sizeof (GET_SKIP_STRING) -
                                                     1, pBuf);

                                /* To go to the location of Host in the packet */
                                i = 0;
                                while (1)
                                {

                                    CAPWAP_GET_1BYTE (u1Char, pBuf);
                                    i1Char = (INT1) u1Char;
                                    u2ReadBytes++;

                                    if (i1Char == -1)
                                    {
                                        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                                                     "WLCHDLR:Failed to Parse the HTTP packet \n");
                                    }
                                    if (i1Char == ':')
                                    {
                                        if (MEMCMP
                                            (REDIRECTION_URL_TEMP, "Host",
                                             STRLEN ("Host")) == 0)
                                        {
                                            CAPWAP_GET_1BYTE (u1Char, pBuf);
                                            break;
                                        }
                                        MEMSET (REDIRECTION_URL_TEMP, 0,
                                                sizeof (REDIRECTION_URL_TEMP));
                                        i = 0;

                                        while (1)
                                        {
                                            CAPWAP_GET_1BYTE (u1Char, pBuf);
                                            i1Char = (INT1) u1Char;
                                            if (i1Char == -1)
                                            {
                                                WLCHDLR_TRC
                                                    (WLCHDLR_FAILURE_TRC,
                                                     "WLCHDLR:Failed to Parse the HTTP packet skip_until\n");
                                            }
                                            if (i1Char == '\n')
                                            {
                                                break;
                                            }

                                        }

                                    }
                                    else
                                    {
                                        REDIRECTION_URL_TEMP[i] =
                                            (UINT1) i1Char;
                                        i++;
                                        if (STRLEN (REDIRECTION_URL_TEMP) ==
                                            (sizeof (REDIRECTION_URL_TEMP) - 1))
                                        {
                                            break;
                                        }
                                    }

                                    if ((u2ReadBytes >= u2PacketLen) ||
                                        (u2ReadBytes >
                                         STA_REDIRECTION_URL_LENGTH))
                                    {
                                        if (u2ReadBytes >
                                            STA_REDIRECTION_URL_LENGTH)
                                        {
                                            WSSSTA_WEBAUTH_TRC1
                                                (WSSSTAWEBAUTH_MASK,
                                                 "WlcHdlrVerifyStaPkts() ==> %d ReadBytes Exceeds configured length\r\n",
                                                 __LINE__);
                                        }
                                        if (u1IsNotLinear == OSIX_TRUE)
                                        {
                                            WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK
                                                (pTemp);
                                        }
                                        UtlShMemFreeWlanDbBuf ((UINT1 *)
                                                               pWssWlanDB);
                                        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *)
                                                                    pWssifauthDBMsgStruct);
                                        /* Packet Completely parsed. */
                                        return OSIX_FAILURE;
                                    }

                                }
                                u2PacketLen = u2PacketLen - u2ReadBytes;

                                u2ReadBytes = 0;

                                /* To get the host address to which the station wants to be redirected */
                                i = 0;
                                while (1)
                                {
                                    CAPWAP_GET_1BYTE (u1Char, pBuf);
                                    i1Char = (INT1) u1Char;
                                    u2ReadBytes++;

                                    if (i1Char == -1)
                                    {
                                        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                                                     "WLCHDLR:Failed to Parse the HTTP packet \n");
                                    }
                                    if ((i1Char == '\r') ||
                                        (STRLEN (REDIRECTION_URL_TEMP) ==
                                         (sizeof (REDIRECTION_URL_TEMP) - 1)))
                                    {
                                        REDIRECTION_URL_TEMP[i] = '\0';
                                        break;
                                    }
                                    REDIRECTION_URL_TEMP[i] = (UINT1) i1Char;
                                    i++;

                                    if ((u2ReadBytes >= u2PacketLen) ||
                                        (u2ReadBytes >
                                         STA_REDIRECTION_URL_LENGTH))
                                    {
                                        if (u2ReadBytes >
                                            STA_REDIRECTION_URL_LENGTH)
                                        {
                                            WSSSTA_WEBAUTH_TRC1
                                                (WSSSTAWEBAUTH_MASK,
                                                 "WlcHdlrVerifyStaPkts() ==> %d ReadBytes Exceeds configured length\r\n",
                                                 __LINE__);
                                        }
                                        if (u1IsNotLinear == OSIX_TRUE)
                                        {
                                            WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK
                                                (pTemp);
                                        }
                                        UtlShMemFreeWlanDbBuf ((UINT1 *)
                                                               pWssWlanDB);
                                        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *)
                                                                    pWssifauthDBMsgStruct);
                                        /* Packet Completely parsed. */
                                        return OSIX_FAILURE;
                                    }

                                }

                                /*  SPRINTF ((CHR1 *)REDIRECTION_URL_TEMP, "%s%s",
                                   REDIRECTION_URL_TEMP, ai1Line); */

                                u2PacketLen = u2PacketLen - u2ReadBytes;
                                u2ReadBytes = 0;

                                if (u1ReqMethod == WEBAUTH_HTTP_GET)
                                {
                                    SPRINTF ((CHR1 *) REDIRECTION_URL_TEMP,
                                             "%s%s", REDIRECTION_URL_TEMP,
                                             ai1Line);
                                    if (pWssWlanDB->WssWlanAttributeDB.
                                        u1ExternalWebAuthMethod ==
                                        WEBAUTH_EXTERNAL)
                                    {
                                        SPRINTF ((CHR1 *) WEBAUTH_EXTERNAL_URL,
                                                 "%s%s", "http://",
                                                 REDIRECTION_URL_TEMP);
                                        if (MEMCMP
                                            (WEBAUTH_EXTERNAL_URL,
                                             pWssWlanDB->WssWlanAttributeDB.
                                             au1ExternalWebAuthUrl,
                                             STRLEN (pWssWlanDB->
                                                     WssWlanAttributeDB.
                                                     au1ExternalWebAuthUrl)) ==
                                            0)
                                        {
                                            /* If GET is external redirect link, give it to CFA to
                                             * forward it to external server */

                                            /* For routing if the destination mac should
                                             * be local mac, this case can happen when the
                                             * packet is CAPWAP encapsulated and router
                                             * is present in between WLC and WTP, in
                                             * that case change the destination mac to switch mac */
                                            CfaGetSysMacAddress (au1SysMac);
                                            if (MEMCMP
                                                (au1DstMacAddr, au1SysMac,
                                                 sizeof (tMacAddr)) != 0)
                                            {
                                                /* change the destination mac to sys mac
                                                 * */
                                                WLCHDLR_COPY_TO_BUF
                                                    (pWlcHdlrMsgStruct->
                                                     WssMacMsgStruct.unMacMsg.
                                                     MacDot11PktBuf.
                                                     pDot11MacPdu, au1SysMac, 0,
                                                     sizeof (tMacAddr));
                                            }
                                            if (u1IsNotLinear == OSIX_TRUE)
                                            {
                                                WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK
                                                    (pTemp);
                                            }
                                            UtlShMemFreeWlanDbBuf ((UINT1 *)
                                                                   pWssWlanDB);
                                            UtlShMemFreeWssIfAuthDbBuf ((UINT1
                                                                         *)
                                                                        pWssifauthDBMsgStruct);
                                            return OSIX_SUCCESS;
                                        }
                                    }

                                    MEMSET (pWssifauthDBMsgStruct->
                                            WssIfAuthStateDB.
                                            au1StaRedirectionURL, 0,
                                            STA_REDIRECTION_URL_LENGTH);
                                    MEMCPY (pWssifauthDBMsgStruct->
                                            WssIfAuthStateDB.
                                            au1StaRedirectionURL,
                                            REDIRECTION_URL_TEMP,
                                            sizeof (REDIRECTION_URL_TEMP));
                                    pWssifauthDBMsgStruct->WssIfAuthStateDB.
                                        bStaRedirectionURL = OSIX_TRUE;
                                    if (WssIfProcessWssAuthDBMsg
                                        (WSS_AUTH_DB_UPDATE_RULENUM,
                                         pWssifauthDBMsgStruct) != OSIX_SUCCESS)
                                    {
                                        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                                                     "WlcHdlrVerifyStaPkts:"
                                                     "WssAuth DB Failed to set Redirection URL for"
                                                     "Station Mac Address\r\n");
                                        if (u1IsNotLinear == OSIX_TRUE)
                                        {
                                            WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK
                                                (pTemp);
                                        }
                                        UtlShMemFreeWlanDbBuf ((UINT1 *)
                                                               pWssWlanDB);
                                        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *)
                                                                    pWssifauthDBMsgStruct);
                                        return OSIX_FAILURE;
                                    }

                                    if (WssStaProcessTCPPacket
                                        (pWlcHdlrMsgStruct->WssMacMsgStruct.
                                         unMacMsg.MacDot11PktBuf.
                                         pDot11MacPdu) != OSIX_SUCCESS)
                                    {
                                        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                                                     "WssWlanProcessConfigResponse:"
                                                     "WssStaProcessTCPPacket failed\r\n");
                                        if (u1IsNotLinear == OSIX_TRUE)
                                        {
                                            WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK
                                                (pTemp);
                                        }
                                        UtlShMemFreeWlanDbBuf ((UINT1 *)
                                                               pWssWlanDB);
                                        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *)
                                                                    pWssifauthDBMsgStruct);
                                        return OSIX_FAILURE;
                                    }
                                }
                                else
                                {
                                    /* If the packet is POST, 
                                     * check for internal or external authorization,
                                     * destination IP/Hostname is redirect link */
                                    i4RetVal =
                                        nmhGetFsDot11WlanLoginAuthentication ((INT4) pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex, (UINT4 *) &i4LoginAuth);
                                    if (i4RetVal != SNMP_SUCCESS)
                                    {
                                        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                                                     "WLCHDLR:Failed to get the LoginAuthentication for the given WlanInded\n");

                                        if (u1IsNotLinear == OSIX_TRUE)
                                        {
                                            WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK
                                                (pTemp);
                                        }
                                        UtlShMemFreeWlanDbBuf ((UINT1 *)
                                                               pWssWlanDB);
                                        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *)
                                                                    pWssifauthDBMsgStruct);
                                        /* Packet Completely parsed. */
                                        return OSIX_FAILURE;
                                    }
                                    if ((pWssWlanDB->WssWlanAttributeDB.
                                         u1ExternalWebAuthMethod ==
                                         WEBAUTH_EXTERNAL)
                                        || (i4LoginAuth == REMOTE_LOGIN_RADIUS))

                                    {
                                        /* Parse the username and password, and
                                         * send it for RADIUS authentication */
                                        MEMSET (ai1Line, 0, sizeof (ai1Line));
                                        i = 0;
                                        while (1)
                                        {
                                            CAPWAP_GET_1BYTE (u1Char, pBuf);
                                            i1Char = (INT1) u1Char;
                                            u2ReadBytes++;

                                            if (i1Char == -1)
                                            {
                                                WLCHDLR_TRC
                                                    (WLCHDLR_FAILURE_TRC,
                                                     "WLCHDLR:Failed to Parse the HTTP packet \n");
                                            }

                                            /* Username and Password in the HTTP
                                             * packet will start after the end
                                             * of content */
                                            /* End of content will be "\r\n\r\n" */
                                            if (MEMCMP
                                                (&i1Char, "\r",
                                                 strlen ("\r")) == 0)
                                            {
                                                CAPWAP_GET_1BYTE (u1Char, pBuf);
                                                i1Char = (INT1) u1Char;
                                                u2ReadBytes++;
                                                if (MEMCMP
                                                    (&i1Char, "\n",
                                                     strlen ("\n")) == 0)
                                                {
                                                    CAPWAP_GET_1BYTE (u1Char,
                                                                      pBuf);
                                                    i1Char = (INT1) u1Char;
                                                    u2ReadBytes++;
                                                    if (MEMCMP
                                                        (&i1Char, "\r",
                                                         strlen ("\r")) == 0)
                                                    {
                                                        CAPWAP_GET_1BYTE
                                                            (u1Char, pBuf);
                                                        i1Char = (INT1) u1Char;
                                                        u2ReadBytes++;
                                                        if (MEMCMP
                                                            (&i1Char, "\n",
                                                             strlen ("\n")) ==
                                                            0)
                                                        {
                                                            break;
                                                        }
                                                    }
                                                }
                                            }

                                            if (u2ReadBytes >= u2PacketLen)
                                            {
                                                if (u1IsNotLinear == OSIX_TRUE)
                                                {
                                                    WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK
                                                        (pTemp);
                                                }
                                                UtlShMemFreeWlanDbBuf ((UINT1 *)
                                                                       pWssWlanDB);
                                                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssifauthDBMsgStruct);
                                                /* Packet Completely parsed. */
                                                return OSIX_FAILURE;
                                            }

                                        }

                                        u2PacketLen = u2PacketLen - u2ReadBytes;
                                        u2ReadBytes = 0;

                                        /* Parse the Username and password */
                                        MEMSET (ai1Line, 0, sizeof (ai1Line));
                                        i = 0;
                                        while (1)
                                        {
                                            CAPWAP_GET_1BYTE (u1Char, pBuf);
                                            i1Char = (INT1) u1Char;
                                            u2ReadBytes++;

                                            if (i1Char == -1)
                                            {
                                                WLCHDLR_TRC
                                                    (WLCHDLR_FAILURE_TRC,
                                                     "WLCHDLR:Failed to Parse the HTTP packet \n");
                                            }

                                            if (MEMCMP
                                                (ai1Line, "LOGIN_NAME",
                                                 STRLEN ("LOGIN_NAME")) == 0)
                                            {
                                                i = 0;
                                                while (1)
                                                {
                                                    CAPWAP_GET_1BYTE (u1Char,
                                                                      pBuf);
                                                    i1Char = (INT1) u1Char;
                                                    u2ReadBytes++;
                                                    if (i1Char == -1)
                                                    {
                                                        WLCHDLR_TRC
                                                            (WLCHDLR_FAILURE_TRC,
                                                             "WLCHDLR:Failed to Parse the HTTP packet \n");
                                                    }

                                                    if (MEMCMP
                                                        (&i1Char, "&",
                                                         STRLEN ("&")) == 0)
                                                    {
                                                        ai1UserName[i] = '\0';
                                                        i = 0;
                                                        MEMSET (ai1Line, 0,
                                                                sizeof
                                                                (ai1Line));
                                                        CAPWAP_GET_1BYTE
                                                            (u1Char, pBuf);
                                                        i1Char = (INT1) u1Char;
                                                        u2ReadBytes++;

                                                        break;
                                                    }

                                                    ai1UserName[i] = i1Char;
                                                    i++;
                                                }
                                                MEMSET (ai1Line, 0,
                                                        sizeof (ai1Line));
                                            }
                                            else if (MEMCMP
                                                     (ai1Line, "PASSWORD",
                                                      STRLEN ("PASSWORD")) == 0)
                                            {
                                                i = 0;
                                                while (1)
                                                {
                                                    CAPWAP_GET_1BYTE (u1Char,
                                                                      pBuf);
                                                    i1Char = (INT1) u1Char;
                                                    u2ReadBytes++;

                                                    if (i1Char == -1)
                                                    {
                                                        WLCHDLR_TRC
                                                            (WLCHDLR_FAILURE_TRC,
                                                             "WLCHDLR:Failed to Parse the HTTP packet \n");
                                                    }

                                                    if (MEMCMP
                                                        (&i1Char, "&",
                                                         STRLEN ("&")) == 0)
                                                    {
                                                        ai1Password[i] = '\0';
                                                        u1ParsingDone = 1;
                                                        break;
                                                    }

                                                    ai1Password[i] = i1Char;
                                                    i++;
                                                }
                                            }

                                            if (u1ParsingDone == 1)
                                            {
                                                break;
                                            }

                                            ai1Line[i] = i1Char;
                                            i++;

                                            if (u2ReadBytes >= u2PacketLen)
                                            {
                                                if (u1IsNotLinear == OSIX_TRUE)
                                                {
                                                    WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK
                                                        (pTemp);
                                                }
                                                UtlShMemFreeWlanDbBuf ((UINT1 *)
                                                                       pWssWlanDB);
                                                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssifauthDBMsgStruct);
                                                /* Packet Completely parsed. */
                                                return OSIX_FAILURE;
                                            }
                                        }

                                        /* Do Radius Authentication */
#ifdef RADIUS_WANTED
                                        if ((STRLEN (ai1UserName) != 0)
                                            && (STRLEN (ai1Password) != 0))
                                        {
                                            WebnmWebAuthRadiusAuthentication
                                                (ai1UserName, ai1Password,
                                                 au1StaMacAddr);
                                        }

                                        if (WssStaProcessTCPPacket
                                            (pWlcHdlrMsgStruct->WssMacMsgStruct.
                                             unMacMsg.MacDot11PktBuf.
                                             pDot11MacPdu) != OSIX_SUCCESS)
                                        {
                                            WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                                                         "WssWlanProcessConfigResponse:"
                                                         "WssStaProcessTCPPacket failed\r\n");
                                            if (u1IsNotLinear == OSIX_TRUE)
                                            {
                                                WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK
                                                    (pTemp);
                                            }
                                            UtlShMemFreeWlanDbBuf ((UINT1 *)
                                                                   pWssWlanDB);
                                            UtlShMemFreeWssIfAuthDbBuf ((UINT1
                                                                         *)
                                                                        pWssifauthDBMsgStruct);
                                            return OSIX_FAILURE;
                                        }
#endif
                                    }
                                    else
                                    {
                                        /* If the Authentication method is not
                                         * radius, return the packet to CFA*/
                                        if (u1IsNotLinear == OSIX_TRUE)
                                        {
                                            WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK
                                                (pTemp);
                                        }
                                        UtlShMemFreeWlanDbBuf ((UINT1 *)
                                                               pWssWlanDB);
                                        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *)
                                                                    pWssifauthDBMsgStruct);
                                        return OSIX_SUCCESS;
                                    }
                                }
                                if (u1IsNotLinear == TRUE)
                                {
                                    WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pTemp);
                                }
                                UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *)
                                                            pWssifauthDBMsgStruct);
                                return PROCESSED_BY_WSSSTA;
                            }
                        }

#ifdef DEBUG_WANTED
                        /* For Centralized routing, the internal web auth http request 
                         * packets are not processed successfully with the below code.
                         * Hence, commenting the below code as of now */
                        /* TCP connection establishment packets */
                        if (WssStaProcessTCPPacket
                            (pWlcHdlrMsgStruct->WssMacMsgStruct.
                             unMacMsg.MacDot11PktBuf.pDot11MacPdu) !=
                            OSIX_SUCCESS)
                        {
                            WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                                         "WssWlanProcessConfigResponse:"
                                         "WssStaProcessTCPPacket failed\r\n");
                            if (u1IsNotLinear == OSIX_TRUE)
                            {
                                WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pTemp);
                            }
                            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *)
                                                        pWssifauthDBMsgStruct);
                            return OSIX_FAILURE;
                        }
                        if (u1IsNotLinear == TRUE)
                        {
                            WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pTemp);
                            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *)
                                                        pWssifauthDBMsgStruct);
                            return PROCESSED_BY_WSSSTA;
                        }
#endif
                    }
                    /* discard FTP  packets before web authentication */
                    else
                    {
                        if (u1IsNotLinear == OSIX_TRUE)
                        {
                            WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pTemp);
                        }
                        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *)
                                                    pWssifauthDBMsgStruct);
                        return NO_NEED_TO_PROCESS;
                    }
                }
                else if (u1Proto == UDP_PROTOCOL_TYPE)
                {
                    CAPWAP_SKIP_N_BYTES (4, pBuf);
                    CAPWAP_GET_2BYTE (u2Sport, pBuf);
                    CAPWAP_GET_2BYTE (u2Dport, pBuf);
                    /*Only DNS ans BOOTP packets are to be processed before authentication */
                    if ((u2Dport == BOOTP_SRV_PORT)
                        || (u2Dport == BOOTP_CLIENT_PORT))
                    {
                        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                                     "WlcHdlrVerifyStaPkts :"
                                     "BOOTP packets received\r\n");
                    }
                    else if ((u2Sport == DNS_PORT) || (u2Dport == DNS_PORT))
                    {
                        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                                     "WlcHdlrVerifyStaPkts :"
                                     "DNS packets received\r\n");
                    }
                    else
                    {
                        if (u1IsNotLinear == OSIX_TRUE)
                        {
                            WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pTemp);
                        }
                        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *)
                                                    pWssifauthDBMsgStruct);
                        return NO_NEED_TO_PROCESS;
                    }
                }

            }
        }
    }

    if (u1IsNotLinear == OSIX_TRUE)
    {
        WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pTemp);
    }
    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssifauthDBMsgStruct);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : WlcHandlerMainTaskInit                                     *
 *                                                                           *
 * Description  : WLC Handler task initialization routine.                   *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS, if initialization succeeds                   *
 *                OSIX_FAILURE, otherwise                                    *
 *                                                                           *
 *****************************************************************************/
UINT4
WlcHandlerMainTaskInit (VOID)
{
    UINT4               u4Event = 0;
    tRadioIfMsgStruct   RadioIfMsgStruct;
    tWssWlanMsgStruct  *pWssWlanMsgStruct = NULL;
    tCfaMsgStruct       CfaMsgStruct;
    tWssStaMsgStruct    WssStaMsgStruct;

    WLCHDLR_FN_ENTRY ();
    pWssWlanMsgStruct =
        (tWssWlanMsgStruct *) (VOID *) UtlShMemAllocWssWlanBuf ();
    if (pWssWlanMsgStruct == NULL)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                     "WlcHandlerMainTaskInit:- "
                     "UtlShMemAllocWssWlanBuf returned failure\n");
        return OSIX_FAILURE;
    }

    MEMSET (&RadioIfMsgStruct, 0, sizeof (tRadioIfMsgStruct));
    MEMSET (pWssWlanMsgStruct, 0, sizeof (tWssWlanMsgStruct));
    MEMSET (&CfaMsgStruct, 0, sizeof (tCfaMsgStruct));
    MEMSET (&WssStaMsgStruct, 0, sizeof (tWssStaMsgStruct));

    /* Create buffer pools for data structures */
    if (WlcHdlrMemInit () == OSIX_FAILURE)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "Memory Pool Creation Failed\n");
        lrInitComplete (OSIX_FAILURE);
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        return OSIX_FAILURE;
    }

    /*Wlchdlr Semaphore */
    if (OsixCreateSem (WLCHDLR_SEM_NAME, WLCHDLR_SEM_CREATE_INIT_CNT, 0,
                       &gWlcHdlrTaskGlobals.wlcHdlrSemId) == OSIX_FAILURE)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                     "WlcHandlerMainTaskInit: WLCHDLR Semaphore creation FAILED!!!\r\n");
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        return OSIX_FAILURE;
    }

    /*Wlchdlr Semaphore for CRU Buf */
    if (OsixCreateSem (WLCHDLR_BUF_SEM_NAME, WLCHDLR_SEM_CREATE_INIT_CNT, 0,
                       &gWlcHdlrTaskGlobals.wlcHdlrBufSemId) == OSIX_FAILURE)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                     "WlcHandlerMainTaskInit: WLCHDLR BUF Semaphore creation FAILED!!!\r\n");
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        return OSIX_FAILURE;
    }

    gu4WlcHdlrSysLogId =
        (UINT4) SYS_LOG_REGISTER ((CONST UINT1 *) WLCHDLR_SEM_NAME,
                                  SYSLOG_DEBUG_LEVEL);

    if (OsixTskIdSelf (&gWlcHdlrTaskGlobals.wlcHdlrTaskId) == OSIX_FAILURE)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                     " !!!!! WLC HANDLER TASK INIT FAILURE  !!!!! \r\n");
        lrInitComplete (OSIX_FAILURE);
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        return OSIX_FAILURE;
    }
    /* Create 'CTRL RX Q' queue */
    if (OsixQueCrt (WLCHDLR_CTRL_Q_NAME, OSIX_MAX_Q_MSG_LEN,
                    WLCHDLR_Q_DEPTH,
                    &(gWlcHdlrTaskGlobals.ctrlRxMsgQId)) != OSIX_SUCCESS)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                     "WlcHandlerMainTaskInit:Ctrl Rx Queue Creation failed \r\n");
        lrInitComplete (OSIX_FAILURE);
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        return OSIX_FAILURE;
    }

    /* Create Data Rx queue */
    if (OsixQueCrt (WLCHDLR_DATARX_Q_NAME, OSIX_MAX_Q_MSG_LEN,
                    MAX_WLCHDLR_PKT_QUE,
                    &(gWlcHdlrTaskGlobals.dataRxMsgQId)) != OSIX_SUCCESS)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                     "WlcHandlerMainTaskInit:Data Rx Queue Creation failed \r\n");
        lrInitComplete (OSIX_FAILURE);
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        return OSIX_FAILURE;
    }

    /* Create Data Tx Queue */
    if (OsixQueCrt (WLCHDLR_DATATX_Q_NAME, OSIX_MAX_Q_MSG_LEN,
                    WLCHDLR_Q_DEPTH,
                    &(gWlcHdlrTaskGlobals.dataTxMsgQId)) != OSIX_SUCCESS)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                     "WlcHandlerMainTaskInit:Data Tx Queue Creation failed \r\n");
        lrInitComplete (OSIX_FAILURE);
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        return OSIX_FAILURE;
    }

    /* Create the Timer List */
    if (WlcHdlrTmrInit () == OSIX_FAILURE)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                     "WlcHandlerMainTaskInit:WlcHdlr Timer creation Failed \r\n");
        lrInitComplete (OSIX_FAILURE);
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        return OSIX_FAILURE;
    }

#ifdef WSSSTA_WANTED
    if (WssIfProcessWssStaMsg (WSS_STA_INIT, &WssStaMsgStruct) != OSIX_SUCCESS)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                     "WlcHandlerMainTaskInit: Station init failed \r\n");
        lrInitComplete (OSIX_FAILURE);
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        return OSIX_FAILURE;
    }
#endif

    /* Initialize the WSS modules */
    WssIfInit ();
    if (WsscfgMainTask () != OSIX_SUCCESS)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                     "WlcHandlerMainTaskInit: Memory init failed \r\n");
        lrInitComplete (OSIX_FAILURE);
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        return OSIX_FAILURE;
    }
    wlchdlrDBInit ();

    if (WssIfProcessRadioIfMsg (WSS_RADIOIF_INIT_MSG, &RadioIfMsgStruct)
        != OSIX_SUCCESS)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "WlcHandlerMainTaskInit:"
                     "WLAN module initialization failed \r\n");
        lrInitComplete (OSIX_FAILURE);
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        return OSIX_FAILURE;
    }
    if (WssIfProcessWssWlanMsg (WSS_WLAN_INIT_MSG, pWssWlanMsgStruct)
        != OSIX_SUCCESS)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "WlcHandlerMainTaskInit:WLAN module"
                     " initialization failed \r\n");
        lrInitComplete (OSIX_FAILURE);
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        return OSIX_FAILURE;
    }

#if WLCHDLR_UNUSED_CODE
    /* create a timer */
    if (TMR_FAILURE == TmrCreateTimerList ((CONST UINT1 *)
                                           "CPU_RELINQUISH_WLCHDLR_TIMER", 0,
                                           tmrWlchdlrCallback,
                                           &pWlchdlrTimerListId))
    {
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        return OSIX_FAILURE;
    }

    /* start the timer */
    if (TmrStart (pWlchdlrTimerListId, &tmrWlchdlrTmrBlk,
                  CAPWAP_CPU_RELINQUISH_WLCHDLR_TMR, 1, 0) == TMR_FAILURE)
    {
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        return OSIX_FAILURE;
    }
#endif

    if (WlcHdlrTmrStart (NULL, WLCHDLR_CPU_RELINQUISH_TMR,
                         CAPWAP_RELINQUISH_TIMER_VAL) != OSIX_SUCCESS)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                     "Relinquish Timer event send failure\r\n");
    }
    if (WlcHdlrTmrStart (NULL, WLCHDLR_WEBAUTH_LIFETIME_EXP_TMR,
                         MAX_WEBAUTH_LIFETIME_EXP_TIMEOUT) != OSIX_SUCCESS)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                     "Webauth lifetime expiry timer init failure\r\n");
    }
    /*WLCHDLR_STA_CONF_PKT_TMR is started by default */
    WlcHdlrStaConfClearTmrControl (ENABLE, DEFAULT_STA_CONF_TMR_VALUE);

    /* initialize counter to zero */
    gu4CPUWlchdlrRelinquishCounter = 0;

    gWlcHdlrStaConfigDB =
        RBTreeCreateEmbedded ((FSAP_OFFSETOF
                               (tWlcHdlrStaConfigDB, nextWlcHdlrStaConfigDB)),
                              WssStaCompareSeqProfIdDBRBTree);

    if (gWlcHdlrStaConfigDB == NULL)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                     "WlcHandlerMainTaskInit : gWlcHdlrStaConfigDB RB Tree Creation Failed \r\n");
        lrInitComplete (OSIX_FAILURE);
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        return OSIX_FAILURE;
    }

    lrInitComplete (OSIX_SUCCESS);

    while (1)
    {
        if (OsixReceiveEvent (WLCHDLR_CTRL_RX_MSGQ_EVENT |
                              WLCHDLR_DATA_RX_MSGQ_EVENT |
                              WLCHDLR_TMR_EXP_EVENT |
                              WLCHDLR_DATA_TX_MSGQ_EVENT, OSIX_WAIT, (UINT4) 0,
                              (UINT4 *) &(u4Event)) == OSIX_SUCCESS)
        {
            WLCHDLR_LOCK;
            if ((u4Event & WLCHDLR_CTRL_RX_MSGQ_EVENT) ==
                WLCHDLR_CTRL_RX_MSGQ_EVENT)
            {
                WlcHdlrProcessCtrlRxMsg ();
                WlcHdlrCheckRelinquishCounters ();
            }
            if ((u4Event & WLCHDLR_DATA_RX_MSGQ_EVENT) ==
                WLCHDLR_DATA_RX_MSGQ_EVENT)
            {
                WlcHdlrProcessDataRxMsg ();
                WlcHdlrCheckRelinquishCounters ();
            }
            if ((u4Event & WLCHDLR_TMR_EXP_EVENT) == WLCHDLR_TMR_EXP_EVENT)
            {
                WlcHdlrTmrExpHandler ();
                WlcHdlrCheckRelinquishCounters ();
            }
            if ((u4Event & WLCHDLR_DATA_TX_MSGQ_EVENT) ==
                WLCHDLR_DATA_TX_MSGQ_EVENT)
            {
                WlcHdlrProcessDataTxMsg ();
                WlcHdlrCheckRelinquishCounters ();
            }
            if ((u4Event & WLCHDLR_STATMR_EXP_EVENT) ==
                WLCHDLR_STATMR_EXP_EVENT)
            {
                WssStaTmrExpHandler ();
                WlcHdlrCheckRelinquishCounters ();
            }

#if WLCHDLR_UNUSED_CODE
            else if ((u4Event & WLCHDLR_RELINQUISH_EVENT) ==
                     WLCHDLR_RELINQUISH_EVENT)
            {
                gu4CPUWlchdlrRelinquishCounter = 0;
            }

            if (gu4CPUWlchdlrRelinquishCounter == CAPWAP_RELINQUISH_MSG_COUNT)
            {
                if (OsixReceiveEvent (WLCHDLR_RELINQUISH_EVENT,
                                      OSIX_WAIT, (UINT4) 0,
                                      (UINT4 *) &(u4Event)) == OSIX_SUCCESS)
                {
                    gu4CPUWlchdlrRelinquishCounter = 0;
                }
            }
#endif
            WLCHDLR_UNLOCK;
        }
    }
    /* Register with SYSLOG */
    gu4StaSysLogId = (UINT4) SYS_LOG_REGISTER ((CONST UINT1 *) WLCHDLR_SEM_NAME,
                                               SYSLOG_CRITICAL_LEVEL);
    UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
    WLCHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : WssStaCompareSeqProfIdDBRBTree                             *
 *                                                                           *
 * Description  : This function is used during the adding of a new station to *
 *                the  Rb tree                                               *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
WssStaCompareSeqProfIdDBRBTree (tRBElem * e1, tRBElem * e2)
{

    tWlcHdlrStaConfigDB *pNode1 = (tWlcHdlrStaConfigDB *) e1;
    tWlcHdlrStaConfigDB *pNode2 = (tWlcHdlrStaConfigDB *) e2;

    if (pNode1->u2WtpProfileId > pNode2->u2WtpProfileId)
    {
        return 1;
    }
    else if (pNode1->u2WtpProfileId < pNode2->u2WtpProfileId)
    {
        return -1;
    }

    if (pNode1->u1SeqNum > pNode2->u1SeqNum)
    {
        return 1;
    }
    else if (pNode1->u1SeqNum < pNode2->u1SeqNum)
    {
        return -1;
    }
    return 0;
}

/*****************************************************************************
 * Function     : WlcHdlrStationDetailsGet                                   *
 *                                                                           *
 * Description  : This function is used to get the value from Db based on    *
 *                                 seqNum & ProfId.                          *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : tWlcHdlrStaConfigDB *                                      *
 *                                                                           *
 *****************************************************************************/
tWlcHdlrStaConfigDB *
WlcHdlrStationDetailsGet (UINT2 u2ProfId, UINT1 u1SeqNum)
{
    tWlcHdlrStaConfigDB wlcHdlrStaConfigDB;
    tWlcHdlrStaConfigDB *pWlcHdlrStaConfigDB = NULL;

    MEMSET (&wlcHdlrStaConfigDB, 0, sizeof (tWlcHdlrStaConfigDB));

    wlcHdlrStaConfigDB.u2WtpProfileId = u2ProfId;
    wlcHdlrStaConfigDB.u1SeqNum = u1SeqNum;

    pWlcHdlrStaConfigDB =
        ((tWlcHdlrStaConfigDB *)
         RBTreeGet (gWlcHdlrStaConfigDB, (tRBElem *) & wlcHdlrStaConfigDB));
    return pWlcHdlrStaConfigDB;
}

/*****************************************************************************
 *                                                                           *
 * Function     : WlcHdlrMemInit                                             *
 *                                                                           *
 * Description  : Memory creation and initialization required for            *
 *                WLC HDLR  module                                           *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
UINT4
WlcHdlrMemInit (VOID)
{
    WLCHDLR_FN_ENTRY ();
    if (WlchdlrSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    WLCHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : WlcHdlrMemClear                                            *
 *                                                                           *
 * Description  : Deletes all the memory pools in Capwap                     *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 *                                                                           *
 *****************************************************************************/
VOID
WlcHdlrMemClear (VOID)
{
    WLCHDLR_FN_ENTRY ();
    WlchdlrSizingMemDeleteMemPools ();
    WLCHDLR_FN_EXIT ();
    return;
}

/*****************************************************************************
 *                                                                           *
 * Function     : WlcHdlrProcessCtrlRxMsg                                    *
 *                                                                           *
 * Description  : This function process the received control message         *
 *                The expected control message are,                          *
 *                1. Configuration Update Response,                          *
 *                2. WTP Event Request,                                      *
 *                3. Image Data Request,                                     *
 *                4. Image Data Response,                                    * 
 *                5. Reset Response                                          *
 *                6. Data Transfer Request                                   *
 *                7. Data Transfer Response                                  *
 *                8. Clear Configuration Response                            *
 *                9. Station Configuration Response                          *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
WlcHdlrProcessCtrlRxMsg ()
{
    tWlcHdlrQueueReq   *pRxQMsg = NULL;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    UINT4               u4MsgType = 0, u4DestIpAddr = 0, u4DestPort = 0;
    tCapwapControlPacket ParseCtrlMsg;
    tRemoteSessionManager *pSessEntry = NULL;
    unCapwapMsgStruct   CapwapMsgStruct;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tWssIfPMDB          WssIfPMDB;

    WLCHDLR_FN_ENTRY ();
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (&ParseCtrlMsg, 0, sizeof (tCapwapControlPacket));
    MEMSET (&CapwapMsgStruct, 0, sizeof (unCapwapMsgStruct));
    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&WssIfPMDB, 0, sizeof (tWssIfPMDB));

    while (OsixQueRecv (gWlcHdlrTaskGlobals.ctrlRxMsgQId, (UINT1 *) (&pRxQMsg),
                        OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        if (pRxQMsg == NULL)
        {
            WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "WlcHdlrProcessCtrlRxMsg: "
                         "Received NULL Pointer\r\n");
            continue;
        }

        if (pRxQMsg->u4MsgType == WLCHDLR_CTRL_MSG)
        {
            if (pRxQMsg->pRcvBuf == NULL)
            {
                WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "WlcHdlrProcessCtrlRxMsg: "
                             "Received NULL Pointer for the Message\r\n");
                MemReleaseMemBlock (WLCHDLR_QUEUE_POOLID, (UINT1 *) pRxQMsg);
                continue;
            }

            pBuf = pRxQMsg->pRcvBuf;
            u4DestIpAddr = pRxQMsg->u4DestIp;
            u4DestPort = pRxQMsg->u4DestPort;

            CapwapMsgStruct.CapwapParseReq.pData = pBuf;
            CapwapMsgStruct.CapwapParseReq.pCapwapCtrlMsg = &ParseCtrlMsg;

            if (WssIfProcessCapwapMsg (WSS_CAPWAP_PARSE_REQ, &CapwapMsgStruct)
                != OSIX_SUCCESS)
            {
                WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "capwapProcessCpCtrlRxMsg:"
                             " Failed to parse the recvd WLCHDLR ctrl packet\r\n");
                WLCHDLR_RELEASE_CRU_BUF (pBuf);
                MemReleaseMemBlock (WLCHDLR_QUEUE_POOLID, (UINT1 *) pRxQMsg);
                continue;
            }
            /* Get the Remote session if the message type is 
             * other than Join request */
            pWssIfCapwapDB->u4DestIp = u4DestIpAddr;
            pWssIfCapwapDB->u4DestPort = u4DestPort;
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_RSM, pWssIfCapwapDB)
                != OSIX_SUCCESS)
            {
                WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                             "Failed to Retrive CAPWAP DB \r\n");
                WLCHDLR_RELEASE_CRU_BUF (pBuf);
                MemReleaseMemBlock (WLCHDLR_QUEUE_POOLID, (UINT1 *) pRxQMsg);
                WssIfProcessCapwapMsg (WSS_CAPWAP_PARSE_MEMREL_REQ,
                                       &CapwapMsgStruct);
                continue;
            }
            pSessEntry = pWssIfCapwapDB->pSessEntry;
            if (pSessEntry == NULL)
            {
                WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "Failed to Get the "
                             "entry in Remote Session Module \r\n");
                WLCHDLR_RELEASE_CRU_BUF (pBuf);
                MemReleaseMemBlock (WLCHDLR_QUEUE_POOLID, (UINT1 *) pRxQMsg);
                WssIfProcessCapwapMsg (WSS_CAPWAP_PARSE_MEMREL_REQ,
                                       &CapwapMsgStruct);
                continue;
            }
            u4MsgType = ParseCtrlMsg.capwapCtrlHdr.u4MsgType;
            ParseCtrlMsg.u2IntProfileId = pSessEntry->u2IntProfileId;
            if ((u4MsgType >= WLAN_CONF_REQ) && (u4MsgType <= WLAN_CONF_RSP))
            {
                /* To make the received IEEE request message compatable 
                 * with the 2-D array */
                u4MsgType = u4MsgType - CAPWAP_IEEE_MSG_TYPE_BASE;
            }
            if (gCapwapStateMachine[pSessEntry->eCurrentState][u4MsgType]
                (pBuf, &ParseCtrlMsg, pSessEntry) != OSIX_SUCCESS)
            {
                WLCHDLR_RELEASE_CRU_BUF (pBuf);
                MemReleaseMemBlock (WLCHDLR_QUEUE_POOLID, (UINT1 *) pRxQMsg);
                WssIfProcessCapwapMsg (WSS_CAPWAP_PARSE_MEMREL_REQ,
                                       &CapwapMsgStruct);
                WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                             "WLCHDLR Failed to Process the Received Packet \r\n");
                if (u4MsgType == CAPWAP_CONF_UPD_RSP)
                {
                    MEMSET (&WssIfPMDB, 0, sizeof (WssIfPMDB));
                    WssIfPMDB.u2ProfileId = pSessEntry->u2IntProfileId;
                    WssIfPMDB.tWtpPmAttState.wssPmWLCStatsSetDB.
                        bWssPmWLCCapwapCfgUnsuccRespProcessed = OSIX_TRUE;
                    WssIfPMDB.tWtpPmAttState.wssPmWLCStatsSetDB.
                        bWssPmWLCCapwapCfgLastUnsuccAttTime = OSIX_TRUE;
                    if (WssIfProcessPMDBMsg (WSS_PM_WLC_CAPWAP_STATS_SET,
                                             &WssIfPMDB) != OSIX_SUCCESS)
                    {
                        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                    "Failed to update Join Req Stats to PM \n");
                    }
                }
                continue;
            }
        }
        else if (pRxQMsg->u4MsgType == WLCHDLR_RELEASE_LAST_TRANSMIT_MSG)
        {
            if (pRxQMsg->pRcvBuf == NULL)
            {
                WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "WlcHdlrProcessCtrlRxMsg: "
                             "Received NULL Pointer for the Message\r\n");
                MemReleaseMemBlock (WLCHDLR_QUEUE_POOLID, (UINT1 *) pRxQMsg);
                continue;
            }

            pBuf = pRxQMsg->pRcvBuf;
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            MemReleaseMemBlock (WLCHDLR_QUEUE_POOLID, (UINT1 *) pRxQMsg);
            continue;
        }
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        MemReleaseMemBlock (WLCHDLR_QUEUE_POOLID, (UINT1 *) pRxQMsg);
        WssIfProcessCapwapMsg (WSS_CAPWAP_PARSE_MEMREL_REQ, &CapwapMsgStruct);
    }

    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    WLCHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : WlcHdlrProcessDataRxMsg                              */
/*                                                                           */
/* Description        : This function receives the Data packets from CAPWAP  */
/*                      posted to the WlcHdlr Data Queue.                    */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS | OSIX_FAILURE                          */
/*****************************************************************************/
INT4
WlcHdlrProcessDataRxMsg (VOID)
{
    tWlcHdlrQueueReq   *pRxQMsg = NULL;
    tWssMacMsgStruct   *pWssMacMsgStruct = NULL;
    tWssWlanDB          wssWlanDB;
    tMacAddr            McastBssId = { 0xff, 0xff, 0xff, 0xff, 0xff, 0xff };
    WLCHDLR_FN_ENTRY ();
    pWssMacMsgStruct =
        (tWssMacMsgStruct *) (VOID *) UtlShMemAllocMacMsgStructBuf ();
    if (pWssMacMsgStruct == NULL)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                     "WlcHdlrProcessDataRxMsg:-"
                     "UtlShMemAllocMacMsgStructBuf returned failure\n");
        return OSIX_FAILURE;
    }

    MEMSET (&wssWlanDB, 0, sizeof (tWssWlanDB));

    while (OsixQueRecv (gWlcHdlrTaskGlobals.dataRxMsgQId, (UINT1 *) (&pRxQMsg),
                        OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        if (pWssMacMsgStruct == NULL)
        {
            WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                         "WlcHdlrProcessDataRxMsg:-"
                         "UtlShMemAllocMacMsgStructBuf returned failure\n");
            WLCHDLR_RELEASE_CRU_BUF (pRxQMsg->pRcvBuf);
            MemReleaseMemBlock (WLCHDLR_QUEUE_POOLID, (UINT1 *) pRxQMsg);
            continue;
        }

        MEMSET (pWssMacMsgStruct, 0, sizeof (tWssMacMsgStruct));

        if (pRxQMsg->u4MsgType == WLCHDLR_DATA_RX_MSG)
        {
#ifdef WSSMAC_WANTED

            pWssMacMsgStruct->unMacMsg.MacDot11PktBuf.pDot11MacPdu =
                pRxQMsg->pRcvBuf;

            pWssMacMsgStruct->unMacMsg.MacDot11PktBuf.u2SessId =
                pRxQMsg->u2SessId;
            pWssMacMsgStruct->unMacMsg.MacDot11PktBuf.u1MacType =
                pRxQMsg->u1MacType;
            pWssMacMsgStruct->unMacMsg.MacDot11PktBuf.u2VlanId =
                pRxQMsg->u2VlanId;
            MEMCPY (pWssMacMsgStruct->unMacMsg.MacDot11PktBuf.BssIdMac,
                    pRxQMsg->BssId, sizeof (tMacAddr));

            MEMCPY (wssWlanDB.WssWlanAttributeDB.BssId, pRxQMsg->BssId,
                    sizeof (tMacAddr));
            wssWlanDB.WssWlanIsPresentDB.bBssIfIndex = OSIX_TRUE;
            if (MEMCMP (McastBssId, pRxQMsg->BssId, sizeof (tMacAddr)) != 0)
            {

                if (WssIfProcessWssWlanDBMsg
                    (WSS_WLAN_GET_BSSID_MAPPING_ENTRY, &wssWlanDB))
                {
                    WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                                 "WlcHdlrProcessDataRxMsg: "
                                 "Failed to retrieve WssWlanDB\r\n");
                    WLCHDLR_RELEASE_CRU_BUF (pRxQMsg->pRcvBuf);
                    MemReleaseMemBlock (WLCHDLR_QUEUE_POOLID,
                                        (UINT1 *) pRxQMsg);
                    continue;
                }
                pWssMacMsgStruct->unMacMsg.MacDot11PktBuf.u4IfIndex =
                    wssWlanDB.WssWlanAttributeDB.u4BssIfIndex;
            }
            if (WssIfProcessWssMacMsg (WSS_MAC_CAPWAP_MSG, pWssMacMsgStruct)
                == OSIX_FAILURE)
            {
                WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "Failed to parse the "
                             "received CAPWAP Data Pkt \r\n");
                MemReleaseMemBlock (WLCHDLR_QUEUE_POOLID, (UINT1 *) pRxQMsg);
                continue;
            }
#endif
            MemReleaseMemBlock (WLCHDLR_QUEUE_POOLID, (UINT1 *) pRxQMsg);
        }
#ifdef WPS_WANTED
        else if ((pRxQMsg->u4MsgType == WLCHDLR_DATA_RX_PROBE_MSG))
        {
#ifdef WSSMAC_WANTED

            pWssMacMsgStruct->unMacMsg.MacDot11PktBuf.pDot11MacPdu =
                pRxQMsg->pRcvBuf;
            pWssMacMsgStruct->unMacMsg.MacDot11PktBuf.u2SessId =
                pRxQMsg->u2SessId;
            pWssMacMsgStruct->unMacMsg.MacDot11PktBuf.u1MacType =
                pRxQMsg->u1MacType;
            if (WssIfProcessWssMacMsg (WSS_MAC_CAPWAP_MSG, pWssMacMsgStruct)
                == OSIX_FAILURE)
            {
                WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "Failed to parse the "
                             "received CAPWAP Data Pkt \r\n");
                MemReleaseMemBlock (WLCHDLR_QUEUE_POOLID, (UINT1 *) pRxQMsg);
                continue;
            }
#endif
            MemReleaseMemBlock (WLCHDLR_QUEUE_POOLID, (UINT1 *) pRxQMsg);
        }
#endif
        else if (pRxQMsg->u4MsgType == WLCHDLR_CFA_RX_MSG)
        {
#ifdef WSSMAC_WANTED

            if (pRxQMsg->pRcvBuf == NULL)
            {
                MemReleaseMemBlock (WLCHDLR_QUEUE_POOLID, (UINT1 *) pRxQMsg);
                continue;
            }

            pWssMacMsgStruct->unMacMsg.MacDot11PktBuf.pDot11MacPdu =
                pRxQMsg->pRcvBuf;
            /* Data Packet Comes with VLAN ID as destination information */
            pWssMacMsgStruct->unMacMsg.MacDot11PktBuf.u2VlanId =
                pRxQMsg->u2SessId;

            if (WssIfProcessWssMacMsg (WSS_MAC_CFA_MSG, pWssMacMsgStruct) ==
                OSIX_FAILURE)
            {
                WLCHDLR_RELEASE_CRU_BUF (pRxQMsg->pRcvBuf);
                MemReleaseMemBlock (WLCHDLR_QUEUE_POOLID, (UINT1 *) pRxQMsg);
                continue;
            }
#endif

            WLCHDLR_RELEASE_CRU_BUF (pRxQMsg->pRcvBuf);
            MemReleaseMemBlock (WLCHDLR_QUEUE_POOLID, (UINT1 *) pRxQMsg);
        }
        else
        {
            WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "Recvd UNKNOWN Queue Type\r\n");
            if (NULL != pRxQMsg->pRcvBuf)
            {
                WLCHDLR_RELEASE_CRU_BUF (pRxQMsg->pRcvBuf);
            }
            MemReleaseMemBlock (WLCHDLR_QUEUE_POOLID, (UINT1 *) pRxQMsg);
            WLCHDLR_FN_EXIT ();
            continue;
        }
    }
    if (pWssMacMsgStruct != NULL)
    {
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssMacMsgStruct);
    }

    WLCHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : WlcHdlrProcessDataTxMsg                              */
/*                                                                           */
/* Description        : This function is to transmit the data packets -      */
/*                      This is unused now                                   */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS                                         */
/*****************************************************************************/
INT4
WlcHdlrProcessDataTxMsg ()
{
    tWlcHdlrQueueReq   *pRxQMsg = NULL;

    WLCHDLR_FN_ENTRY ();

    while (OsixQueRecv (gWlcHdlrTaskGlobals.ctrlRxMsgQId, (UINT1 *) (&pRxQMsg),
                        OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        if (pRxQMsg->u4MsgType == WLCHDLR_DATA_TX_MSG)
        {
            return OSIX_SUCCESS;
        }
    }

    WLCHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : WlcHdlrCheckRelinquishCounters                              *
 *                                                                           *
 * Description  : Check relinquish counters and take action                  *
 *                                                                           *
 * Input        : NONE                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
VOID
WlcHdlrCheckRelinquishCounters (VOID)
{
#ifdef WTP_WANTED
    UINT4               u4Event = 0;

    WLCHDLR_FN_ENTRY ();

    gu4CPUWlchdlrRelinquishCounter++;
    {
        if (gCapwapState != CAPWAP_IMAGEDATA)
        {
            if (gu4CPUWlchdlrRelinquishCounter == CAPWAP_RELINQUISH_MSG_COUNT)
            {
                if (OsixReceiveEvent (WLCHDLR_RELINQUISH_EVENT, OSIX_WAIT,
                                      (UINT4) 0,
                                      (UINT4 *) &(u4Event)) == OSIX_SUCCESS)
                {
                    gu4CPUWlchdlrRelinquishCounter = 0;
                }
            }
        }
    }

    WLCHDLR_FN_EXIT ();
#endif
    return;
}

/*****************************************************************************
 * Function     : WlcHdlrCheckServRelinquishCounters                         *
 *                                                                           *
 * Description  : Check relinquish counters and take action                  *
 *                                                                           *
 * Input        : NONE                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
VOID
WlcHdlrCPURelinquishTmrExp (VOID *param)
{
    UNUSED_PARAM (param);
    WLCHDLR_FN_ENTRY ();
    if (OsixEvtSend (gWlcHdlrTaskGlobals.wlcHdlrTaskId,
                     WLCHDLR_RELINQUISH_EVENT) != OSIX_SUCCESS)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                     "WLCHDLR_RELINQUISH_EVENT Send Failed \r\n");
    }

    if (WlcHdlrTmrStart (NULL, WLCHDLR_CPU_RELINQUISH_TMR,
                         CAPWAP_RELINQUISH_TIMER_VAL) != OSIX_SUCCESS)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                     "WLCHDLR_CPU_RELINQUISH_TMR Start Failed \r\n");
    }

    WLCHDLR_FN_EXIT ();
    return;
}

/****************************************************************************
 *                                                                          *
 * Function     : WlchdlrLock                                                *
 *                                                                          *
 * Description  : This function is to take Wlchdlr mutex semaphore.          *
 *                                                                          *
 * Input        : None                                                      *
 *                                                                          *
 * Output       : None                                                      *
 *                                                                          *
 * Returns      : None                                                      *
 *                                                                          *
 ****************************************************************************/
INT4
WlchdlrLock (VOID)
{
    WLCHDLR_FN_ENTRY ();
    if (OsixSemTake (gWlcHdlrTaskGlobals.wlcHdlrSemId) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    WLCHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          *
 * Function     : WlchdlrUnlock                                              *
 *                                                                          *
 * Description  : This function is to release Wlchdlr mutex semaphore.       *
 *                                                                          *
 * Input        : None                                                      *
 *                                                                          *
 * Output       : None                                                      *
 *                                                                          *
 * Returns      : None                                                      *
 *                                                                          *
 ****************************************************************************/
INT4
WlchdlrUnLock (VOID)
{
    WLCHDLR_FN_ENTRY ();

    if (OsixSemGive (gWlcHdlrTaskGlobals.wlcHdlrSemId) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    WLCHDLR_FN_EXIT ();

    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          *
 * Function     : WlchdlrSendEvent                                          *
 *                                                                          *
 * Description  : This function shall send event to WLCHDLR in case of ECP  *
 *                                                                          *
 * Input        : u4Event                                                   *
 *                                                                          *
 * Output       : None                                                      *
 *                                                                          *
 * Returns      : None                                                      *
 *                                                                          *
 ****************************************************************************/
INT4
WlchdlrSendEvent (UINT4 u4Event)
{
    if (OsixEvtSend (gWlcHdlrTaskGlobals.wlcHdlrTaskId,
                     WLCHDLR_CMN_ACCEPT_EVENT) == OSIX_FAILURE)
    {
        WLCHDLR_TRC1 (WLCHDLR_FAILURE_TRC,
                      "WlchdlrSendEvent: Failed to send Event %d\n", u4Event);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          *
 * Function     : WlchdlrBufLock                                            *
 *                                                                          *
 * Description  : This function is to take Wlchdlr mutex semaphore.          *
 *                                                                          *
 * Input        : None                                                      *
 *                                                                          *
 * Output       : None                                                      *
 *                                                                          *
 * Returns      : None                                                      *
 *                                                                          *
 ****************************************************************************/
INT4
WlchdlrBufLock (VOID)
{
    WLCHDLR_FN_ENTRY ();
    if (OsixSemTake (gWlcHdlrTaskGlobals.wlcHdlrBufSemId) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    WLCHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          *
 * Function     : WlchdlrBufUnlock                                          *
 *                                                                          *
 * Description  : This function is to release Wlchdlr mutex semaphore.       *
 *                                                                          *
 * Input        : None                                                      *
 *                                                                          *
 * Output       : None                                                      *
 *                                                                          *
 * Returns      : None                                                      *
 *                                                                          *
 ****************************************************************************/
INT4
WlchdlrBufUnLock (VOID)
{
    WLCHDLR_FN_ENTRY ();

    if (OsixSemGive (gWlcHdlrTaskGlobals.wlcHdlrBufSemId) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    WLCHDLR_FN_EXIT ();

    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          *
 * Function     : WlchdlrFreeTransmittedPacket                              *
 *                                                                          *
 * Description  : This function is to release the last transmitted CRU Buff *
 *                                                                          *
 * Input        : None                                                      *
 *                                                                          *
 * Output       : None                                                      *
 *                                                                          *
 * Returns      : None                                                      *
 *                                                                          *
 ****************************************************************************/
INT4
WlchdlrFreeTransmittedPacket (tRemoteSessionManager * pSessEntry)
{
    WLCHDLR_FN_ENTRY ();
    WLCHDLR_BUF_LOCK;

    if (pSessEntry->lastTransmittedPkt != NULL)
    {
        WLCHDLR_RELEASE_CRU_BUF (pSessEntry->lastTransmittedPkt);
        pSessEntry->lastTransmittedPkt = NULL;
        pSessEntry->lastTransmittedPktLen = 0;
        pSessEntry->u1RetransmitCount = 0;
    }
    WLCHDLR_BUF_UNLOCK;

    WLCHDLR_FN_EXIT ();

    return OSIX_SUCCESS;
}
#endif /* _WLCHDLRMAIN_C__ */
