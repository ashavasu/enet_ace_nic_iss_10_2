/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: wlchdlrstation.c,v 1.4 2018/01/22 09:39:53 siva Exp $
 *
 * Description: This file contains the WLCHDLR Station config routines.
 *
 *****************************************************************************/
#ifndef __CAPWAP_STATION_C__
#define __CAPWAP_STATION_C__

#include "wlchdlrinc.h"

extern tRBTree      gWlcHdlrStaConfigDB;
#ifdef KERNEL_CAPWAP_WANTED
#include "fcusprot.h"
#include "fcusglob.h"
#include "wssstawlcprot.h"
#endif
#include "wssifstatrc.h"
extern UINT4        gu4WssStaWebAuthDebugMask;

/************************************************************************/
/*  Function Name   : CapwapProcessConfigStationResp                    */
/*  Description     : The function processes the received CAPWAP        */
/*                    Config Station response.                          */
/*  Input(s)        : ConfigStationRespMsg- Station Config response     */
/*                      packet received                                 */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
CapwapProcessConfigStationResp (tSegment * pBuf,
                                tCapwapControlPacket * pConfigStationRespMsg,
                                tRemoteSessionManager * pSessEntry)
{
    tWssIfPMDB          WssIfPMDB;
    UINT1              *pRcvBuf = NULL;
    UINT2               u2PacketLen = 0;
    UINT4               u4Offset = 0;
    tCRU_BUF_CHAIN_HEADER *pTxBuf = NULL;
    UINT2               u2NumOptionalMsg = 0, u2MsgType = 0;
    UINT1               u1Index = 0;
    UINT2               u2MsgIndex = 0;
#ifdef PMF_DEBUG_WANTED
    UINT2               u2MsgSubType = 0;
#endif
    UINT1              *pu1TxBuf = NULL;
    UINT1              *pTempBuf = NULL;
    UINT1               u1IsNotLinear = OSIX_FALSE;
    tWssStaMsgStruct    WssStaMsgStruct;
    tCapwapControlPacket pCapwapCtrlmsg;
    tStationConfRsp     StationConfRsp;
    unCapwapMsgStruct   CapwapMsgStruct;
    tWlcHdlrStaConfigDB *pWlcHdlrStaConfigDB = NULL;

#ifdef KERNEL_CAPWAP_WANTED
    tWssStaWepProcessDB *pWssStaWepProcessDB = NULL;
#endif

    WLCHDLR_FN_ENTRY ();

    MEMSET (&WssStaMsgStruct, 0, sizeof (tWssStaMsgStruct));
    MEMSET (&pCapwapCtrlmsg, 0, sizeof (tCapwapControlPacket));
    MEMSET (&StationConfRsp, 0, sizeof (tStationConfRsp));
    MEMSET (&CapwapMsgStruct, 0, sizeof (unCapwapMsgStruct));

    if (pSessEntry == NULL)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "WLCHDLR Session Entry is NULL, "
                     "return FAILURE \r\n");
        return NO_AP_PRESENT;
    }
    if (pSessEntry->eCurrentState != CAPWAP_RUN)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "Session not in RUN state \r\n");
        return NO_AP_PRESENT;
    }
    if (WlcHdlrTmrStop (pSessEntry, WLCHDLR_RETRANSMIT_INTERVAL_TMR)
        == OSIX_FAILURE)
    {
        WLCHDLR_TRC (CAPWAP_FAILURE, "Failed to stop the wlchdlr timer \r\n");
        WlchdlrFreeTransmittedPacket (pSessEntry);
        return OSIX_FAILURE;
    }
    WlchdlrFreeTransmittedPacket (pSessEntry);

    u2PacketLen = (UINT2) CAPWAP_GET_BUF_LEN (pBuf);
    pRcvBuf = WLCHDLR_BUF_IF_LINEAR (pBuf, 0, u2PacketLen);
    if (pRcvBuf == NULL)
    {
        WLCHDLR_PKTBUF_ALLOC_MEM_BLOCK (pRcvBuf);
        WLCHDLR_COPY_FROM_BUF (pBuf, pRcvBuf, 0, u2PacketLen);
        u1IsNotLinear = OSIX_TRUE;
    }
    /*Get the details of station from the DB having the specified profile ID and Sequence
     * Number*/
    if (pRcvBuf != NULL)
    {
        pWlcHdlrStaConfigDB =
            WlcHdlrStationDetailsGet (pSessEntry->u2IntProfileId,
                                      (UINT1) pRcvBuf[WLCHDLR_GET_SEQ_NUM_BIT]);
    }
    if (pWlcHdlrStaConfigDB == NULL)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "WlcHdlrStationDetailsGet:"
                     "FAILURE :No Entry is present in DB\r\n");
        if (u1IsNotLinear == OSIX_TRUE)
        {
            WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pRcvBuf);
        }
        return OSIX_FAILURE;
    }

    pTxBuf = WLCHDLR_ALLOCATE_CRU_BUF (pWlcHdlrStaConfigDB->u4StaBufLen, 0);

    if (pTxBuf == NULL)
    {
        WLCHDLR_TRC1 (CAPWAP_FAILURE, "CapwapProcessConfigStationResp:: "
                      "Failed to allocate memory for pTxBuf of Len %d\r\n",
                      pWlcHdlrStaConfigDB->u4StaBufLen);
        if (u1IsNotLinear == OSIX_TRUE)
        {
            WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pRcvBuf);
        }
        return OSIX_FAILURE;
    }

    WLCHDLR_COPY_TO_BUF (pTxBuf, pWlcHdlrStaConfigDB->au1StaBuf, 0,
                         pWlcHdlrStaConfigDB->u4StaBufLen);

    RBTreeRem (gWlcHdlrStaConfigDB, (tRBElem *) pWlcHdlrStaConfigDB);
    WLCHDLR_STA_MEM_RELEASE (pWlcHdlrStaConfigDB);

    CapwapMsgStruct.CapwapParseReq.pData = pTxBuf;
    CapwapMsgStruct.CapwapParseReq.pCapwapCtrlMsg = &pCapwapCtrlmsg;

    if (WssIfProcessCapwapMsg (WSS_CAPWAP_PARSE_REQ, &CapwapMsgStruct)
        != OSIX_SUCCESS)
    {
        WLCHDLR_TRC (CAPWAP_FAILURE, "WSS_CAPWAP_PARSE_REQ FAILED \r\n");
        WLCHDLR_RELEASE_CRU_BUF (pTxBuf);
        if (u1IsNotLinear == OSIX_TRUE)
        {
            WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pRcvBuf);
        }
        return OSIX_FAILURE;
    }
    else
    {
        pu1TxBuf = WLCHDLR_BUF_IF_LINEAR (pTxBuf, 0, CAPWAP_MAX_PKT_LEN);
        if (pu1TxBuf == NULL)
        {
            WLCHDLR_PKTBUF_ALLOC_MEM_BLOCK (pu1TxBuf);
            if (pu1TxBuf == NULL)
            {
                WLCHDLR_TRC (CAPWAP_FAILURE,
                             "WSS_CAPWAP_PARSE_REQ FAILED \r\n");
                WLCHDLR_RELEASE_CRU_BUF (pTxBuf);
                if (u1IsNotLinear == OSIX_TRUE)
                {
                    WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pRcvBuf);
                }
                return OSIX_FAILURE;
            }
            CAPWAP_COPY_FROM_BUF (pTxBuf, pu1TxBuf, 0, CAPWAP_MAX_PKT_LEN);
            u1IsNotLinear = OSIX_TRUE;
        }
        pTempBuf = pu1TxBuf;
        u2NumOptionalMsg = pCapwapCtrlmsg.numOptionalElements;
        for (u1Index = 0; u1Index < u2NumOptionalMsg; u1Index++)
        {
            pu1TxBuf = pTempBuf;
            u2MsgType = pCapwapCtrlmsg.capwapMsgElm
                [MAX_CONF_STATION_REQ_MAND_MSG_ELEMENTS + u1Index].u2MsgType;
            u4Offset = pCapwapCtrlmsg.capwapMsgElm
                [MAX_CONF_STATION_REQ_MAND_MSG_ELEMENTS + u1Index].pu2Offset[0];

            pu1TxBuf += u4Offset;
            switch (u2MsgType)
            {
                case ADD_STATION:
                {
                    CAPWAP_GET_2BYTE (WssStaMsgStruct.unAuthMsg.
                                      WssStaConfigReqInfo.AddSta.u2MessageType,
                                      pu1TxBuf);
                    CAPWAP_GET_2BYTE (WssStaMsgStruct.unAuthMsg.
                                      WssStaConfigReqInfo.AddSta.
                                      u2MessageLength, pu1TxBuf);
                    CAPWAP_GET_1BYTE (WssStaMsgStruct.unAuthMsg.
                                      WssStaConfigReqInfo.AddSta.u1RadioId,
                                      pu1TxBuf);
                    CAPWAP_GET_1BYTE (WssStaMsgStruct.unAuthMsg.
                                      WssStaConfigReqInfo.AddSta.u1MacAddrLen,
                                      pu1TxBuf);
                    CAPWAP_GET_NBYTE (WssStaMsgStruct.unAuthMsg.
                                      WssStaConfigReqInfo.AddSta.StaMacAddress,
                                      pu1TxBuf, sizeof (tMacAddr));
                    WssStaMsgStruct.unAuthMsg.WssStaConfigReqInfo.AddSta.
                        isPresent = OSIX_TRUE;
                }
                    break;
                case DELETE_STATION:
                {
                    CAPWAP_GET_2BYTE (WssStaMsgStruct.unAuthMsg.
                                      WssStaConfigReqInfo.DelSta.u2MessageType,
                                      pu1TxBuf);
                    CAPWAP_GET_2BYTE (WssStaMsgStruct.unAuthMsg.
                                      WssStaConfigReqInfo.DelSta.
                                      u2MessageLength, pu1TxBuf);
                    CAPWAP_GET_1BYTE (WssStaMsgStruct.unAuthMsg.
                                      WssStaConfigReqInfo.DelSta.u1RadioId,
                                      pu1TxBuf);
                    CAPWAP_GET_1BYTE (WssStaMsgStruct.unAuthMsg.
                                      WssStaConfigReqInfo.DelSta.u1MacAddrLen,
                                      pu1TxBuf);
                    CAPWAP_GET_NBYTE (WssStaMsgStruct.unAuthMsg.
                                      WssStaConfigReqInfo.DelSta.StaMacAddr,
                                      pu1TxBuf, sizeof (tMacAddr));
                    WssStaMsgStruct.unAuthMsg.WssStaConfigReqInfo.DelSta.
                        isPresent = OSIX_TRUE;
                }
                    break;
                case VENDOR_SPECIFIC_PAYLOAD:
                {
                    CAPWAP_SKIP_N_BYTES (WLCHDLR_VENDOR_SPEC_USERROLE_OFFSET,
                                         pu1TxBuf);
                    CAPWAP_GET_NBYTE (WssStaMsgStruct.unAuthMsg.
                                      WssStaConfigReqInfo.VendSpec.StaMacAddr,
                                      pu1TxBuf, sizeof (tMacAddr));
                    CAPWAP_GET_4BYTE (WssStaMsgStruct.unAuthMsg.
                                      WssStaConfigReqInfo.VendSpec.u4BandWidth,
                                      pu1TxBuf);
                    CAPWAP_GET_4BYTE (WssStaMsgStruct.unAuthMsg.
                                      WssStaConfigReqInfo.VendSpec.
                                      u4DLBandWidth, pu1TxBuf);
                    CAPWAP_GET_4BYTE (WssStaMsgStruct.unAuthMsg.
                                      WssStaConfigReqInfo.VendSpec.
                                      u4ULBandWidth, pu1TxBuf);

                    WssStaMsgStruct.unAuthMsg.WssStaConfigReqInfo.VendSpec.
                        isOptional = OSIX_TRUE;
                }
                    break;
                case IEEE_STATION:
                case IEEE_STATION_SESSION_KEY:
                    break;
                default:
                    break;
            }
        }

        WssIfProcessCapwapMsg (WSS_CAPWAP_PARSE_MEMREL_REQ, &CapwapMsgStruct);

        if (u1IsNotLinear == OSIX_TRUE)
        {
            WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pTempBuf);
        }
    }
    WLCHDLR_RELEASE_CRU_BUF (pTxBuf);
    u1IsNotLinear = OSIX_FALSE;

#ifdef PMF_DEBUG_WANTED

    /* Changes done for Processing Station Config Response */

    u2PacketLen = (UINT2) CAPWAP_GET_BUF_LEN (pBuf);
    pRcvBuf = WLCHDLR_BUF_IF_LINEAR (pBuf, 0, u2PacketLen);
    if (pRcvBuf == NULL)
    {
        WLCHDLR_PKTBUF_ALLOC_MEM_BLOCK (pRcvBuf);
        WLCHDLR_COPY_FROM_BUF (pBuf, pRcvBuf, 0, u2PacketLen);
        u1IsNotLinear = OSIX_TRUE;
    }

    pTempBuf = pRcvBuf;
#endif
    u4Offset = pConfigStationRespMsg->capwapMsgElm[u2MsgIndex].pu2Offset[0];
    pRcvBuf += u4Offset;

    CAPWAP_GET_2BYTE (StationConfRsp.resultCode.u2MsgEleType, pRcvBuf);
    CAPWAP_GET_2BYTE (StationConfRsp.resultCode.u2MsgEleLen, pRcvBuf);
    CAPWAP_GET_4BYTE (StationConfRsp.resultCode.u4Value, pRcvBuf);
#ifdef PMF_DEBUG_WANTED
    u2NumOptionalMsg = pConfigStationRespMsg->numOptionalElements;
    for (u1Index = 0; u1Index < u2NumOptionalMsg; u1Index++)
    {
        pRcvBuf = pTempBuf;
        u2MsgType = pConfigStationRespMsg->capwapMsgElm
            [MAX_CONF_STATION_RESP_MAND_MSG_ELEMENTS + u1Index].u2MsgType;
        u4Offset = pConfigStationRespMsg->capwapMsgElm
            [MAX_CONF_STATION_RESP_MAND_MSG_ELEMENTS + u1Index].pu2Offset[0];
        pRcvBuf += u4Offset;

        switch (u2MsgType)
        {

            case VENDOR_SPECIFIC_PAYLOAD:
            {
                MEMCPY (&u2MsgSubType, pRcvBuf + 4, 2);
                u2MsgSubType = OSIX_NTOHS (u2MsgSubType);

                CAPWAP_SKIP_N_BYTES (4, pRcvBuf);

                switch (u2MsgSubType)
                {
                    case VENDOR_PMF_MSG:
                    {
                        CAPWAP_GET_2BYTE (WssStaMsgStruct.unAuthMsg.
                                          WssStaConfigReqInfo.VendSpec.
                                          PMFStaVendor.u2MsgEleType, pRcvBuf);

                        CAPWAP_GET_2BYTE (WssStaMsgStruct.unAuthMsg.
                                          WssStaConfigReqInfo.VendSpec.
                                          PMFStaVendor.u2MsgEleLen, pRcvBuf);

                        CAPWAP_GET_4BYTE (WssStaMsgStruct.unAuthMsg.
                                          WssStaConfigReqInfo.VendSpec.
                                          PMFStaVendor.u4VendorId, pRcvBuf);

                        CAPWAP_GET_1BYTE (WssStaMsgStruct.unAuthMsg.
                                          WssStaConfigReqInfo.VendSpec.
                                          PMFStaVendor.RsnaIGTKSeqInfo[0].
                                          u1KeyIndex, pRcvBuf);

                        CAPWAP_GET_NBYTE (WssStaMsgStruct.unAuthMsg.
                                          WssStaConfigReqInfo.VendSpec.
                                          PMFStaVendor.RsnaIGTKSeqInfo[0].
                                          au1IGTKPktNo, pRcvBuf, 6);

                        CAPWAP_GET_1BYTE (WssStaMsgStruct.unAuthMsg.
                                          WssStaConfigReqInfo.VendSpec.
                                          PMFStaVendor.RsnaIGTKSeqInfo[1].
                                          u1KeyIndex, pRcvBuf);

                        CAPWAP_GET_NBYTE (WssStaMsgStruct.unAuthMsg.
                                          WssStaConfigReqInfo.VendSpec.
                                          PMFStaVendor.RsnaIGTKSeqInfo[1].
                                          au1IGTKPktNo, pRcvBuf, 6);

                        WssStaMsgStruct.unAuthMsg.WssStaConfigReqInfo.VendSpec.
                            PMFStaVendor.isOptional = TRUE;

                    }
                        break;
                    default:
                        break;

                }

            }
                break;
            default:
                break;
        }
    }
#endif

    /* Validate the Conf Station response message */
    if ((CapValidateConfStatRespMsgElems (pRcvBuf, pConfigStationRespMsg)
         != OSIX_SUCCESS))
    {
        WLCHDLR_TRC (CAPWAP_FAILURE,
                     "CapValidateConfStatRespMsgElems FAILED \r\n");
        MEMSET (&WssIfPMDB, 0, sizeof (tWssIfPMDB));
        WssIfPMDB.u2ProfileId = pSessEntry->u2IntProfileId;
        WssIfPMDB.WssPmWLCCapwapStatsSetFlag.bStaConfigStats = OSIX_TRUE;
        WssIfPMDB.WssPmWLCCapwapStatsSetFlag.bRunUpdateunsuccessfulProcessed =
            OSIX_TRUE;

        if (WssIfProcessPMDBMsg (WSS_PM_WLC_CAPWAP_RUN_STATS_SET,
                                 &WssIfPMDB) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to update PM db\r\n");
        }

        return OSIX_FAILURE;
    }
    MEMSET (&WssIfPMDB, 0, sizeof (tWssIfPMDB));
    WssIfPMDB.u2ProfileId = pSessEntry->u2IntProfileId;
    WssIfPMDB.WssPmWLCCapwapStatsSetFlag.bStaConfigStats = OSIX_TRUE;
    WssIfPMDB.WssPmWLCCapwapStatsSetFlag.bRunUpdateRspReceived = OSIX_TRUE;

    if (WssIfProcessPMDBMsg (WSS_PM_WLC_CAPWAP_RUN_STATS_SET,
                             &WssIfPMDB) != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to update PM db\r\n");
    }

    WssStaMsgStruct.unAuthMsg.WssStaConfigReqInfo.Resultcode =
        StationConfRsp.resultCode.u4Value;
    if ((WssStaProcessWssIfMsg (WSS_STA_STACONFIG_RESP, &WssStaMsgStruct)
         != OSIX_SUCCESS))
    {
        WLCHDLR_TRC (CAPWAP_FAILURE, "WSS_STA_STACONFIG_RESP FAILED \r\n");
        return OSIX_FAILURE;
    }
#ifdef KERNEL_CAPWAP_WANTED
#ifdef WLC_WANTED
    if (WssStaMsgStruct.unAuthMsg.WssStaConfigReqInfo.AddSta.isPresent ==
        OSIX_TRUE)
    {
        pWssStaWepProcessDB =
            WssStaProcessEntryGet (WssStaMsgStruct.unAuthMsg.
                                   WssStaConfigReqInfo.AddSta.StaMacAddress);
        pWssStaWepProcessDB->u4ApIpAddress =
            pSessEntry->remoteIpAddr.u4_addr[0];

        if (CapwapUpdateKernelStaTable
            (WssStaMsgStruct.unAuthMsg.WssStaConfigReqInfo.AddSta.
             StaMacAddress) == OSIX_FAILURE)
        {
            WLCHDLR_TRC (CAPWAP_FAILURE, "KERNEL DB FAILED \r\n");
            return OSIX_FAILURE;
        }
    }
    else if (WssStaMsgStruct.unAuthMsg.WssStaConfigReqInfo.DelSta.isPresent ==
             OSIX_TRUE)
    {
        if (CapwapRemoveKernelStaTable
            (WssStaMsgStruct.unAuthMsg.WssStaConfigReqInfo.DelSta.StaMacAddr) ==
            OSIX_FAILURE)
        {
            WLCHDLR_TRC (CAPWAP_FAILURE, "KERNEL DB FAILED \r\n");
            return OSIX_FAILURE;
        }
    }
#endif
#endif
    UNUSED_PARAM (u1IsNotLinear);
    WLCHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapValidateConfStatRespMsgElems                   */
/*  Description     : The function validates the functional             */
/*                    characteristics of the received CAPWAP            */
/*                    station Response.                                 */
/*  Input(s)        : ConfigStationReqMsg - parsed CAPWAP StationConfig */
/*                                            request                   */
/*                    pRcvBuf - Received Config Station packet          */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
CapValidateConfStatRespMsgElems (UINT1 *pRcvBuf,
                                 tCapwapControlPacket * ConfigStationRespMsg)
{
    UNUSED_PARAM (pRcvBuf);
    UNUSED_PARAM (ConfigStationRespMsg);
    tVendorSpecPayload  vendSpec;
    tResCode            resultcode;

    WLCHDLR_FN_ENTRY ();
    MEMSET (&resultcode, 0, sizeof (tResCode));
    MEMSET (&vendSpec, 0, sizeof (tVendorSpecPayload));

#if WLCHDLR_UNUSED_CODE
    /* Validate the Mandatory message elements */
    if ((CapwapValidateResultCode (pRcvBuf, ConfigStationRespMsg,
                                   &resultcode,
                                   RESULT_CODE_CONF_STATION_RESP_INDEX) ==
         OSIX_SUCCESS))
    {
        return OSIX_SUCCESS;
    }

    /* Validate the Optional message elements */
    if (CapwapValidateVendSpecPld (pRcvBuf, ConfigStationRespMsg, &vendSpec,
                                   VENDOR_SPECIFIC_CONF_STATION_RESP_INDEX)
        == OSIX_SUCCESS)
    {
        return OSIX_SUCCESS;
    }
#endif

    WLCHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapProcessConfigStationReq                     */
/*  Description     : The function is unused in WLC                     */
/*  Input(s)        : None                                              */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/
INT4
CapwapProcessConfigStationReq (tSegment * pBuf,
                               tCapwapControlPacket * pconfigStationReqMsg,
                               tRemoteSessionManager * pSessEntry)
{
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pconfigStationReqMsg);
    UNUSED_PARAM (pSessEntry);
    return OSIX_SUCCESS;
}

#ifdef KERNEL_CAPWAP_WANTED
/************************************************************************/
/*  Function Name   : CapwapUpdateKernelStaTable                        */
/*  Description     : The function is used to update kernel DB          */
/*  Input(s)        : staMac - Station Mac Address                      */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS / OSIX_FAILURE                       */
/************************************************************************/
INT4
CapwapUpdateKernelStaTable (tMacAddr staMac)
{
    UINT1               u1Module = 0, u2Field = 0;
    tHandleRbWlc        WssKernelDB;
    tWssStaWepProcessDB *pWssStaWepProcessDB = NULL;
    tWssWlanDB          wssWlanDB;
    tRadioIfGetDB       RadioIfGetDB;
    tWssifauthDBMsgStruct *pWssifauthDBMsgStruct = NULL;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    pWssifauthDBMsgStruct =
        (tWssifauthDBMsgStruct *) UtlShMemAllocWssIfAuthDbBuf ();
    if (pWssifauthDBMsgStruct == NULL)
    {
        WSSSTA_TRC (WSSSTA_MGMT_TRC,
                    "WssStaConfigStaFwdRule:- "
                    "UtlShMemAllocWssIfAuthDbBuf returned failure\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    MEMSET (pWssifauthDBMsgStruct, 0, sizeof (tWssifauthDBMsgStruct));
    MEMSET (&WssKernelDB, 0, sizeof (tHandleRbWlc));
    MEMSET (&wssWlanDB, 0, sizeof (tWssWlanDB));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    /* Bssid is got from the station DB */
    pWssStaWepProcessDB = WssStaProcessEntryGet (staMac);

    if (pWssStaWepProcessDB == NULL)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "Failed to retrieve data from WssStationDB\r\n");
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssifauthDBMsgStruct);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    MEMCPY (wssWlanDB.WssWlanAttributeDB.BssId,
            pWssStaWepProcessDB->BssIdMacAddr, sizeof (tMacAddr));
    wssWlanDB.WssWlanIsPresentDB.bBssId = OSIX_TRUE;
    wssWlanDB.WssWlanIsPresentDB.bBssIfIndex = OSIX_TRUE;
    wssWlanDB.WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
    wssWlanDB.WssWlanIsPresentDB.bWebAuthStatus = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSSID_MAPPING_ENTRY, &wssWlanDB)
        == OSIX_FAILURE)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "Failed to retrieve data from WssWlanDB\r\n");
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssifauthDBMsgStruct);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    wssWlanDB.WssWlanIsPresentDB.bRadioIfIndex = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_IFINDEX_ENTRY, &wssWlanDB)
        == OSIX_FAILURE)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "Failed to retrieve data from WssWlanDB\r\n");
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssifauthDBMsgStruct);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    wssWlanDB.WssWlanIsPresentDB.bQosOptionImplemented = OSIX_TRUE;
    wssWlanDB.WssWlanIsPresentDB.bPassengerTrustMode = OSIX_TRUE;
    wssWlanDB.WssWlanIsPresentDB.bQosTraffic = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY, &wssWlanDB)
        == OSIX_FAILURE)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "Failed to retrieve data from WssWlanDB\r\n");
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssifauthDBMsgStruct);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    RadioIfGetDB.RadioIfIsGetAllDB.bQosIndex = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.u1QosIndex = 1;
    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
        wssWlanDB.WssWlanAttributeDB.u4RadioIfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bTaggingPolicy = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_QOS_CONFIG_DB, &RadioIfGetDB)
        == OSIX_FAILURE)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "Failed to retrieve data from RadioIfDB\r\n");
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssifauthDBMsgStruct);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, &RadioIfGetDB)
        == OSIX_FAILURE)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "Failed to retrieve data from RadioIfDB\r\n");
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssifauthDBMsgStruct);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
        RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpMacAddress = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpLocalRouting = OSIX_TRUE;

    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
        != OSIX_SUCCESS)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "Getting WTP MAC failed\n");
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssifauthDBMsgStruct);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    /* The authentication status of the station is got from the Auth DB */
    MEMCPY (pWssifauthDBMsgStruct->WssIfAuthStateDB.stationMacAddress,
            staMac, sizeof (tMacAddr));

    if (WssIfProcessWssAuthDBMsg (WSS_AUTH_GET_STATION_DB,
                                  pWssifauthDBMsgStruct) != OSIX_SUCCESS)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssIfProcessWssAuthDBMsg: Failed to get station auth status\r\n");
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssifauthDBMsgStruct);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    MEMCPY (WssKernelDB.rbData.unTable.apStaData.staMac, staMac,
            sizeof (tMacAddr));

    MEMCPY (WssKernelDB.rbData.unTable.apStaData.wtpMac,
            pWssIfCapwapDB->CapwapGetDB.WtpMacAddress, sizeof (tMacAddr));
    MEMCPY (WssKernelDB.rbData.unTable.apStaData.bssid,
            pWssStaWepProcessDB->BssIdMacAddr, sizeof (tMacAddr));
    WssKernelDB.rbData.unTable.apStaData.u4ipAddr =
        pWssStaWepProcessDB->u4ApIpAddress;
    WssKernelDB.rbData.unTable.apStaData.u4StaIpAddr =
        pWssStaWepProcessDB->u4StationIpAddress;

    WssKernelDB.rbData.unTable.apStaData.u1WebAuthEnable =
        wssWlanDB.WssWlanAttributeDB.u1WebAuthStatus;
    WssKernelDB.rbData.unTable.apStaData.u1WebAuthStatus =
        pWssifauthDBMsgStruct->WssIfAuthStateDB.u1StaAuthFinished;
/*WMM_ENABLE/DISABLE respective to STA*/
    WssKernelDB.rbData.unTable.apStaData.u1WMMEnable =
        pWssStaWepProcessDB->u1StationWmmEnabled;
    WssKernelDB.rbData.unTable.apStaData.u1TaggingPolicy =
        RadioIfGetDB.RadioIfGetAllDB.u1TaggingPolicy;
    WssKernelDB.rbData.unTable.apStaData.u1TrustMode =
        wssWlanDB.WssWlanAttributeDB.u1PassengerTrustMode;
    WssKernelDB.rbData.unTable.apStaData.u1QosProfilePriority =
        wssWlanDB.WssWlanAttributeDB.u1QosTraffic;
    WssKernelDB.rbData.unTable.apStaData.u1LocalRouting =
        pWssIfCapwapDB->CapwapGetDB.u1WtpLocalRouting;

    u1Module = RX_MODULE;
    u2Field = AP_STA_TABLE;

    if (capwapNpUpdateKernelDBWlc (u1Module, u2Field, &WssKernelDB) ==
        OSIX_FAILURE)
    {
        WLCHDLR_TRC (CAPWAP_FAILURE, "Kernel DB Failed \r\n");
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssifauthDBMsgStruct);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    u1Module = TX_MODULE;
    u2Field = AP_STA_TABLE;

    if (capwapNpUpdateKernelDBWlc (u1Module, u2Field, &WssKernelDB) ==
        OSIX_FAILURE)
    {
        WLCHDLR_TRC (CAPWAP_FAILURE, "Kernel DB Failed \r\n");
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssifauthDBMsgStruct);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    WSSSTA_WEBAUTH_TRC2 (WSSSTAWEBAUTH_MASK,
                         "Update kernel module web auth status as %d @ %d\n",
                         WssKernelDB.rbData.unTable.apStaData.u1WebAuthStatus,
                         __LINE__);

    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssifauthDBMsgStruct);
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapKernelStaTableUpdate                        */
/*  Description     : The function is used to update kernel DB          */
/*  Input(s)        : staMac - Station Mac Address                      */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS / OSIX_FAILURE                       */
/************************************************************************/
INT4
CapwapKernelStaTableUpdate (tMacAddr staMac, tWssIfAuthStateDB * pStaStateDB)
{
    UINT1               u1Module = 0, u2Field = 0;
    tHandleRbWlc        WssKernelDB;
    tWssStaWepProcessDB *pWssStaWepProcessDB = NULL;
    tWssWlanDB          wssWlanDB;
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&WssKernelDB, 0, sizeof (tHandleRbWlc));
    MEMSET (&wssWlanDB, 0, sizeof (tWssWlanDB));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    /* Bssid is got from the station DB */
    pWssStaWepProcessDB = WssStaProcessEntryGet (staMac);

    MEMCPY (wssWlanDB.WssWlanAttributeDB.BssId,
            pWssStaWepProcessDB->BssIdMacAddr, sizeof (tMacAddr));
    wssWlanDB.WssWlanIsPresentDB.bBssId = OSIX_TRUE;
    wssWlanDB.WssWlanIsPresentDB.bBssIfIndex = OSIX_TRUE;
    wssWlanDB.WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
    wssWlanDB.WssWlanIsPresentDB.bWebAuthStatus = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSSID_MAPPING_ENTRY, &wssWlanDB)
        == OSIX_FAILURE)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "Failed to retrieve data from WssWlanDB\r\n");
        return OSIX_FAILURE;
    }

    wssWlanDB.WssWlanIsPresentDB.bRadioIfIndex = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_IFINDEX_ENTRY, &wssWlanDB)
        == OSIX_FAILURE)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "Failed to retrieve data from WssWlanDB\r\n");
        return OSIX_FAILURE;
    }

    wssWlanDB.WssWlanIsPresentDB.bQosOptionImplemented = OSIX_TRUE;
    wssWlanDB.WssWlanIsPresentDB.bPassengerTrustMode = OSIX_TRUE;
    wssWlanDB.WssWlanIsPresentDB.bQosTraffic = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY, &wssWlanDB)
        == OSIX_FAILURE)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "Failed to retrieve data from WssWlanDB\r\n");
        return OSIX_FAILURE;
    }

    RadioIfGetDB.RadioIfIsGetAllDB.bQosIndex = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.u1QosIndex = 1;
    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
        wssWlanDB.WssWlanAttributeDB.u4RadioIfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bTaggingPolicy = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_QOS_CONFIG_DB, &RadioIfGetDB)
        == OSIX_FAILURE)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "Failed to retrieve data from RadioIfDB\r\n");
        return OSIX_FAILURE;
    }

    MEMCPY (WssKernelDB.rbData.unTable.apStaData.staMac, staMac,
            sizeof (tMacAddr));
    MEMCPY (WssKernelDB.rbData.unTable.apStaData.bssid,
            pWssStaWepProcessDB->BssIdMacAddr, sizeof (tMacAddr));
    WssKernelDB.rbData.unTable.apStaData.u4ipAddr =
        pWssStaWepProcessDB->u4ApIpAddress;
    WssKernelDB.rbData.unTable.apStaData.u4StaIpAddr =
        pWssStaWepProcessDB->u4StationIpAddress;

    WssKernelDB.rbData.unTable.apStaData.u1WebAuthEnable =
        wssWlanDB.WssWlanAttributeDB.u1WebAuthStatus;
    WssKernelDB.rbData.unTable.apStaData.u1WebAuthStatus =
        pStaStateDB->u1StaAuthFinished;
    WssKernelDB.rbData.unTable.apStaData.u1WMMEnable =
        wssWlanDB.WssWlanAttributeDB.u1QosOptionImplemented;
    WssKernelDB.rbData.unTable.apStaData.u1TaggingPolicy =
        RadioIfGetDB.RadioIfGetAllDB.u1TaggingPolicy;
    WssKernelDB.rbData.unTable.apStaData.u1TrustMode =
        wssWlanDB.WssWlanAttributeDB.u1PassengerTrustMode;
    WssKernelDB.rbData.unTable.apStaData.u1QosProfilePriority =
        wssWlanDB.WssWlanAttributeDB.u1QosTraffic;

    u1Module = RX_MODULE;
    u2Field = AP_STA_TABLE;

    if (capwapNpUpdateKernelDBWlc (u1Module, u2Field, &WssKernelDB) ==
        OSIX_FAILURE)
    {
        WLCHDLR_TRC (CAPWAP_FAILURE, "Kernel DB Failed \r\n");
        return OSIX_FAILURE;
    }
    u1Module = TX_MODULE;
    u2Field = AP_STA_TABLE;

    if (capwapNpUpdateKernelDBWlc (u1Module, u2Field, &WssKernelDB) ==
        OSIX_FAILURE)
    {
        WLCHDLR_TRC (CAPWAP_FAILURE, "Kernel DB Failed \r\n");
        return OSIX_FAILURE;
    }
    WSSSTA_WEBAUTH_TRC2 (WSSSTAWEBAUTH_MASK,
                         "Update kernel module web auth status as %d @ %d\n",
                         WssKernelDB.rbData.unTable.apStaData.u1WebAuthStatus,
                         __LINE__);

    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapReplaceKernelStaTable                       */
/*  Description     : The function is used to replace kernel DB         */
/*  Input(s)        : staMac - Station Mac Address                      */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS / OSIX_FAILURE                       */
/************************************************************************/
INT4
CapwapReplaceKernelStaTable (tMacAddr staMac)
{
    UINT1               u1Module = 0, u2Field = 0;
    tHandleRbWlc        WssKernelDB;
    tWssStaWepProcessDB *pWssStaWepProcessDB = NULL;
    tWssWlanDB         *pwssWlanDB = NULL;
    tRadioIfGetDB       RadioIfGetDB;
    tWssifauthDBMsgStruct *pWssifauthDBMsgStruct = NULL;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    pWssifauthDBMsgStruct =
        (tWssifauthDBMsgStruct *) UtlShMemAllocWssIfAuthDbBuf ();
    if (pWssifauthDBMsgStruct == NULL)
    {
        WSSSTA_TRC (WSSSTA_MGMT_TRC,
                    "WssStaConfigStaFwdRule:- "
                    "UtlShMemAllocWssIfAuthDbBuf returned failure\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    pwssWlanDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
    if (pwssWlanDB == NULL)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "CapwapReplaceKernelStaTable "
                     "WlanDbBuf Failed to allocate memory\r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssifauthDBMsgStruct);
        return OSIX_FAILURE;
    }

    MEMSET (pWssifauthDBMsgStruct, 0, sizeof (tWssifauthDBMsgStruct));
    MEMSET (&WssKernelDB, 0, sizeof (tHandleRbWlc));
    MEMSET (pwssWlanDB, 0, sizeof (tWssWlanDB));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    /* Bssid is got from the station DB */
    pWssStaWepProcessDB = WssStaProcessEntryGet (staMac);

    if (pWssStaWepProcessDB == NULL)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "Failed to retrieve data from WssStationDB\r\n");
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssifauthDBMsgStruct);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        return OSIX_FAILURE;
    }
    MEMCPY (pwssWlanDB->WssWlanAttributeDB.BssId,
            pWssStaWepProcessDB->BssIdMacAddr, sizeof (tMacAddr));
    pwssWlanDB->WssWlanIsPresentDB.bBssId = OSIX_TRUE;
    pwssWlanDB->WssWlanIsPresentDB.bBssIfIndex = OSIX_TRUE;
    pwssWlanDB->WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
    pwssWlanDB->WssWlanIsPresentDB.bWebAuthStatus = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSSID_MAPPING_ENTRY, pwssWlanDB)
        == OSIX_FAILURE)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "Failed to retrieve data from WssWlanDB\r\n");
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssifauthDBMsgStruct);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        return OSIX_FAILURE;
    }

    pwssWlanDB->WssWlanIsPresentDB.bRadioIfIndex = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_IFINDEX_ENTRY, pwssWlanDB)
        == OSIX_FAILURE)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "Failed to retrieve data from WssWlanDB\r\n");
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssifauthDBMsgStruct);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        return OSIX_FAILURE;
    }

    pwssWlanDB->WssWlanIsPresentDB.bQosOptionImplemented = OSIX_TRUE;
    pwssWlanDB->WssWlanIsPresentDB.bPassengerTrustMode = OSIX_TRUE;
    pwssWlanDB->WssWlanIsPresentDB.bQosTraffic = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY, pwssWlanDB)
        == OSIX_FAILURE)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "Failed to retrieve data from WssWlanDB\r\n");
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssifauthDBMsgStruct);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        return OSIX_FAILURE;
    }

    RadioIfGetDB.RadioIfIsGetAllDB.bQosIndex = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.u1QosIndex = 1;
    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
        pwssWlanDB->WssWlanAttributeDB.u4RadioIfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bTaggingPolicy = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_QOS_CONFIG_DB, &RadioIfGetDB)
        == OSIX_FAILURE)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "Failed to retrieve data from RadioIfDB\r\n");
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssifauthDBMsgStruct);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        return OSIX_FAILURE;
    }

    RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, &RadioIfGetDB)
        == OSIX_FAILURE)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "Failed to retrieve data from RadioIfDB\r\n");
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssifauthDBMsgStruct);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        return OSIX_FAILURE;
    }

    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
        RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpMacAddress = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpLocalRouting = OSIX_TRUE;

    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
        != OSIX_SUCCESS)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "Getting WTP MAC failed\n");
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssifauthDBMsgStruct);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        return OSIX_FAILURE;
    }

    /* The authentication status of the station is got from the Auth DB */
    MEMCPY (pWssifauthDBMsgStruct->WssIfAuthStateDB.stationMacAddress,
            staMac, sizeof (tMacAddr));

    if (WssIfProcessWssAuthDBMsg (WSS_AUTH_GET_STATION_DB,
                                  pWssifauthDBMsgStruct) != OSIX_SUCCESS)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssIfProcessWssAuthDBMsg: Failed to get station auth status\r\n");
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssifauthDBMsgStruct);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        return OSIX_FAILURE;
    }
    MEMCPY (WssKernelDB.rbData.unTable.apStaData.staMac, staMac,
            sizeof (tMacAddr));

    MEMCPY (WssKernelDB.rbData.unTable.apStaData.wtpMac,
            pWssIfCapwapDB->CapwapGetDB.WtpMacAddress, sizeof (tMacAddr));
    MEMCPY (WssKernelDB.rbData.unTable.apStaData.bssid,
            pWssStaWepProcessDB->BssIdMacAddr, sizeof (tMacAddr));
    WssKernelDB.rbData.unTable.apStaData.u4ipAddr =
        pWssStaWepProcessDB->u4ApIpAddress;
    WssKernelDB.rbData.unTable.apStaData.u4StaIpAddr =
        pWssStaWepProcessDB->u4StationIpAddress;

    WssKernelDB.rbData.unTable.apStaData.u1WebAuthEnable =
        pwssWlanDB->WssWlanAttributeDB.u1WebAuthStatus;
    WssKernelDB.rbData.unTable.apStaData.u1WebAuthStatus =
        pWssifauthDBMsgStruct->WssIfAuthStateDB.u1StaAuthFinished;
/*WMM_ENABLE/DISABLE respective to STA*/
    WssKernelDB.rbData.unTable.apStaData.u1WMMEnable =
        pWssStaWepProcessDB->u1StationWmmEnabled;
    WssKernelDB.rbData.unTable.apStaData.u1TaggingPolicy =
        RadioIfGetDB.RadioIfGetAllDB.u1TaggingPolicy;
    WssKernelDB.rbData.unTable.apStaData.u1TrustMode =
        pwssWlanDB->WssWlanAttributeDB.u1PassengerTrustMode;
    WssKernelDB.rbData.unTable.apStaData.u1QosProfilePriority =
        pwssWlanDB->WssWlanAttributeDB.u1QosTraffic;
    WssKernelDB.rbData.unTable.apStaData.u1LocalRouting =
        pWssIfCapwapDB->CapwapGetDB.u1WtpLocalRouting;

    u1Module = RX_MODULE;
    u2Field = AP_STA_TABLE;

    if (capwapNpReplaceKernelDBWlc (u1Module, u2Field, &WssKernelDB) ==
        OSIX_FAILURE)
    {
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssifauthDBMsgStruct);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        WLCHDLR_TRC (CAPWAP_FAILURE, "Kernel DB Failed \r\n");
        return OSIX_FAILURE;
    }
    u1Module = TX_MODULE;
    u2Field = AP_STA_TABLE;

    if (capwapNpReplaceKernelDBWlc (u1Module, u2Field, &WssKernelDB) ==
        OSIX_FAILURE)
    {
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssifauthDBMsgStruct);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        WLCHDLR_TRC (CAPWAP_FAILURE, "Kernel DB Failed \r\n");
        return OSIX_FAILURE;
    }
    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssifauthDBMsgStruct);
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapRemoveKernelStaTable                        */
/*  Description     : The function is used to remove kernel DB          */
/*  Input(s)        : staMac - Station Mac Address                      */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS / OSIX_FAILURE                       */
/************************************************************************/
INT4
CapwapRemoveKernelStaTable (tMacAddr staMac)
{
    UINT1               u1Module = 0, u2Field = 0;
    tHandleRbWlc        WssKernelDB;

    MEMSET (&WssKernelDB, 0, sizeof (tHandleRbWlc));

    MEMCPY (WssKernelDB.rbData.unTable.apStaData.staMac, staMac,
            sizeof (tMacAddr));

    u1Module = RX_MODULE;
    u2Field = AP_STA_TABLE;

    if (capwapNpRemoveKernelDBWlc (u1Module, u2Field, &WssKernelDB) ==
        OSIX_FAILURE)
    {
        WLCHDLR_TRC (CAPWAP_FAILURE, "Kernel DB Failed \r\n");
        return OSIX_FAILURE;
    }
    u1Module = TX_MODULE;
    u2Field = AP_STA_TABLE;

    if (capwapNpRemoveKernelDBWlc (u1Module, u2Field, &WssKernelDB) ==
        OSIX_FAILURE)
    {
        WLCHDLR_TRC (CAPWAP_FAILURE, "Kernel DB Failed \r\n");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapUpdateKernelStationIPTable                  */
/*  Description     : The function is used to update kernel DB          */
/*  Input(s)        : staMac - Station Mac Address                      */
/*                    u4StaIPAddr - Station IP Address                  */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS / OSIX_FAILURE                       */
/************************************************************************/
INT4
CapwapUpdateKernelStationIPTable (tMacAddr staMac, UINT4 u4StaIPAddr)
{
    UINT1               u1Module = 0, u2Field = 0;
    tHandleRbWlc        WssKernelDB;
    tWssStaWepProcessDB *pWssStaWepProcessDB = NULL;
    tWssWlanDB          wssWlanDB;
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&WssKernelDB, 0, sizeof (tHandleRbWlc));

    /* Bssid is got from the station DB */
    pWssStaWepProcessDB = WssStaProcessEntryGet (staMac);

    if (pWssStaWepProcessDB == NULL)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC, "Station entry not found\r\n");
        return OSIX_FAILURE;
    }

    MEMCPY (wssWlanDB.WssWlanAttributeDB.BssId,
            pWssStaWepProcessDB->BssIdMacAddr, sizeof (tMacAddr));
    wssWlanDB.WssWlanIsPresentDB.bBssId = OSIX_TRUE;
    wssWlanDB.WssWlanIsPresentDB.bBssIfIndex = OSIX_TRUE;
    wssWlanDB.WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSSID_MAPPING_ENTRY, &wssWlanDB)
        == OSIX_FAILURE)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "Failed to retrieve data from WssWlanDB\r\n");
        return OSIX_FAILURE;
    }

    wssWlanDB.WssWlanIsPresentDB.bRadioIfIndex = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_IFINDEX_ENTRY, &wssWlanDB)
        == OSIX_FAILURE)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "Failed to retrieve data from WssWlanDB\r\n");
        return OSIX_FAILURE;
    }

    wssWlanDB.WssWlanIsPresentDB.bQosOptionImplemented = OSIX_TRUE;
    wssWlanDB.WssWlanIsPresentDB.bPassengerTrustMode = OSIX_TRUE;
    wssWlanDB.WssWlanIsPresentDB.bQosTraffic = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY, &wssWlanDB)
        == OSIX_FAILURE)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "Failed to retrieve data from WssWlanDB\r\n");
        return OSIX_FAILURE;
    }
    RadioIfGetDB.RadioIfIsGetAllDB.bQosIndex = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.u1QosIndex = 1;
    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
        wssWlanDB.WssWlanAttributeDB.u4RadioIfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bTaggingPolicy = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_QOS_CONFIG_DB, &RadioIfGetDB)
        == OSIX_FAILURE)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "Failed to retrieve data from RadioIfDB\r\n");
        return OSIX_FAILURE;
    }

    MEMCPY (WssKernelDB.rbData.unTable.StaIpData.staMac, staMac,
            sizeof (tMacAddr));
    MEMCPY (WssKernelDB.rbData.unTable.StaIpData.bssid,
            pWssStaWepProcessDB->BssIdMacAddr, sizeof (tMacAddr));
    WssKernelDB.rbData.unTable.StaIpData.u4StaIp = u4StaIPAddr;
/*WMM_ENABLE/DISABLE respective to STA*/
    WssKernelDB.rbData.unTable.StaIpData.u1WMMEnable =
        pWssStaWepProcessDB->u1StationWmmEnabled;
    WssKernelDB.rbData.unTable.StaIpData.u1TaggingPolicy =
        RadioIfGetDB.RadioIfGetAllDB.u1TaggingPolicy;
    WssKernelDB.rbData.unTable.StaIpData.u1TrustMode =
        wssWlanDB.WssWlanAttributeDB.u1PassengerTrustMode;
    WssKernelDB.rbData.unTable.StaIpData.u1QosProfilePriority =
        wssWlanDB.WssWlanAttributeDB.u1QosTraffic;

    u1Module = TX_MODULE;
    u2Field = STA_IP_TABLE;

    if (capwapNpUpdateKernelDBWlc (u1Module, u2Field, &WssKernelDB) ==
        OSIX_FAILURE)
    {
        WLCHDLR_TRC (CAPWAP_FAILURE, "Kernel DB Failed \r\n");
        return OSIX_FAILURE;
    }

    u1Module = RX_MODULE;
    u2Field = STA_IP_TABLE;

    if (capwapNpUpdateKernelDBWlc (u1Module, u2Field, &WssKernelDB) ==
        OSIX_FAILURE)
    {
        WLCHDLR_TRC (CAPWAP_FAILURE, "Kernel DB Failed \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;

}

/************************************************************************/
/*  Function Name   : CapwapRemoveKernelStationIPTable                  */
/*  Description     : The function is used to remove kernel DB          */
/*  Input(s)        : u4staIp - Station IP Address                      */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS / OSIX_FAILURE                       */
/************************************************************************/
INT4
CapwapRemoveKernelStationIPTable (UINT4 u4staIp)
{
    UINT1               u1Module = 0, u2Field = 0;
    tHandleRbWlc        WssKernelDB;

    MEMSET (&WssKernelDB, 0, sizeof (tHandleRbWlc));

    WssKernelDB.rbData.unTable.StaIpData.u4StaIp = u4staIp;

    u1Module = TX_MODULE;
    u2Field = STA_IP_TABLE;

    if (capwapNpRemoveKernelDBWlc (u1Module, u2Field, &WssKernelDB) ==
        OSIX_FAILURE)
    {
        WLCHDLR_TRC (CAPWAP_FAILURE, "Kernel DB Failed \r\n");
        return OSIX_FAILURE;
    }

    u1Module = RX_MODULE;
    u2Field = STA_IP_TABLE;

    if (capwapNpRemoveKernelDBWlc (u1Module, u2Field, &WssKernelDB) ==
        OSIX_FAILURE)
    {
        WLCHDLR_TRC (CAPWAP_FAILURE, "Kernel DB Failed \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapUpdateKernelStaTable                        */
/*  Description     : The function is used to update kernel DB during   */
/*                    webauth user deletion                             */
/*  Input(s)        : staMac - Station Mac Address                      */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS / OSIX_FAILURE                       */
/************************************************************************/
INT4
CapwapUpdateKernelUsrDeletionStaTable (tMacAddr staMac)
{
    UINT1               u1Module = 0, u2Field = 0;
    tHandleRbWlc        WssKernelDB;
    tWssStaWepProcessDB *pWssStaWepProcessDB = NULL;
    tWssWlanDB          wssWlanDB;

    MEMSET (&WssKernelDB, 0, sizeof (tHandleRbWlc));
    MEMSET (&wssWlanDB, 0, sizeof (tWssWlanDB));

    /* Bssid is got from the station DB */
    pWssStaWepProcessDB = WssStaProcessEntryGet (staMac);

    if (pWssStaWepProcessDB == NULL)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC, "Station entry not found\r\n");
        return OSIX_FAILURE;
    }

    MEMCPY (wssWlanDB.WssWlanAttributeDB.BssId,
            pWssStaWepProcessDB->BssIdMacAddr, sizeof (tMacAddr));
    wssWlanDB.WssWlanIsPresentDB.bBssId = OSIX_TRUE;
    wssWlanDB.WssWlanIsPresentDB.bBssIfIndex = OSIX_TRUE;
    wssWlanDB.WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
    wssWlanDB.WssWlanIsPresentDB.bWebAuthStatus = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSSID_MAPPING_ENTRY, &wssWlanDB)
        == OSIX_FAILURE)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "Failed to retrieve data from WssWlanDB\r\n");
        return OSIX_FAILURE;
    }

    MEMCPY (WssKernelDB.rbData.unTable.apStaData.staMac, staMac,
            sizeof (tMacAddr));
    MEMCPY (WssKernelDB.rbData.unTable.apStaData.bssid,
            pWssStaWepProcessDB->BssIdMacAddr, sizeof (tMacAddr));
    WssKernelDB.rbData.unTable.apStaData.u4ipAddr =
        pWssStaWepProcessDB->u4ApIpAddress;
    WssKernelDB.rbData.unTable.apStaData.u4StaIpAddr =
        pWssStaWepProcessDB->u4StationIpAddress;

    WssKernelDB.rbData.unTable.apStaData.u1WebAuthEnable =
        wssWlanDB.WssWlanAttributeDB.u1WebAuthStatus;
    WssKernelDB.rbData.unTable.apStaData.u1WebAuthStatus = OSIX_FALSE;

    u1Module = RX_MODULE;
    u2Field = AP_STA_TABLE;

    if (capwapNpUpdateKernelDBWlc (u1Module, u2Field, &WssKernelDB) ==
        OSIX_FAILURE)
    {
        WLCHDLR_TRC (CAPWAP_FAILURE, "Kernel DB Failed \r\n");
        return OSIX_FAILURE;
    }
    u1Module = TX_MODULE;
    u2Field = AP_STA_TABLE;

    if (capwapNpUpdateKernelDBWlc (u1Module, u2Field, &WssKernelDB) ==
        OSIX_FAILURE)
    {
        WLCHDLR_TRC (CAPWAP_FAILURE, "Kernel DB Failed \r\n");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

#endif
/*****************************************************************************
 * Function                  : WlcHdlrClearAllStationEntries                 *
 *                                                                           *
 * Description               : This routine shall remove the station config  *
 *                             request buffer stored in DB                   *
 *                                                                           *
 * Input                     : None                                          *
 *                                                                           *
 * Output                    : None                                          *
 *                                                                           *
 * Returns                   : None.                                         *
 *                                                                           *
 *****************************************************************************/
INT4
WlcHdlrClearAllStationEntries ()
{
    tWlcHdlrStaConfigDB wlcHdlrStaConfigDB;
    tWlcHdlrStaConfigDB *pWlcHdlrStaConfigDB = NULL;

    MEMSET (&wlcHdlrStaConfigDB, 0, sizeof (tWlcHdlrStaConfigDB));
    pWlcHdlrStaConfigDB =
        RBTreeGetNext (gWlcHdlrStaConfigDB, (tRBElem *) & wlcHdlrStaConfigDB,
                       NULL);

    while (pWlcHdlrStaConfigDB != NULL)
    {
        RBTreeRem (gWlcHdlrStaConfigDB, (tRBElem *) pWlcHdlrStaConfigDB);
        WLCHDLR_STA_MEM_RELEASE (pWlcHdlrStaConfigDB);
        pWlcHdlrStaConfigDB = NULL;
        pWlcHdlrStaConfigDB =
            RBTreeGetNext (gWlcHdlrStaConfigDB,
                           (tRBElem *) & wlcHdlrStaConfigDB, NULL);
    }
    return OSIX_SUCCESS;
}
#endif
