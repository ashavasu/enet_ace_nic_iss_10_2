/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: wlchdlrpridisc.c,v 1.2 2017/05/23 14:16:54 siva Exp $
 *
 * Description: This file contains the WLCHDLR Primary Discovery routines.
 *
 *****************************************************************************/
#ifndef __CAPWAP_PRIDISC_C__
#define __CAPWAP_PRIDISC_C__

#include "wlchdlrinc.h"
/************************************************************************/
/*  Function Name   : CapwapProcessPrimaryDiscoveryRequest              */
/*  Description     : The function processes the received CAPWAP        */
/*                    discovery request                                 */
/*  Input(s)        : discReqMsg - CAPWAP Discovery packet received     */
/*                    u4DestPort - Received UDP port                    */
/*                    u4DestIp - Dest IP Address                        */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/

INT4
CapwapProcessPrimaryDiscoveryRequest (tSegment * pBuf,
                                      tCapwapControlPacket * pDiscReqMsg,
                                      tRemoteSessionManager * pSessEntry)
{
    tWssIfPMDB          WssIfPMDB;
    UINT4               u4MsgLen = 0;
    INT4                i4Status = OSIX_SUCCESS;
    tPriDiscRsp        *pDiscResponse = NULL;
    UINT1              *pu1TxBuf = NULL;
    UINT1              *pRcvdBuf = NULL;
    tIp6Addr            destIpAddr;
    UINT4               u4DestPort = 0;
    UINT1               u1IsNotLinear = OSIX_FALSE;
    UINT2               u2PacketLen = 0;
    UINT2               u2WtpInternalId = 0;
    tSegment           *pTxBuf = NULL;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT2               u2TempLength = 0;
    UINT1              *pu1TempBuf = NULL;

    UNUSED_PARAM (pSessEntry);

    CAPWAP_FN_ENTRY ();

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        pDiscResponse = (tPriDiscRsp *) (VOID *) UtlShMemAllocDiscRspBuf ();
    if (pDiscResponse == NULL)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapProcessDiscoveryRequest:- "
                    "UtlShMemAllocDiscRspBuf returned failure\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    MEMSET (pDiscResponse, 0, sizeof (tPriDiscRsp));
    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    u4DestPort = CRU_BUF_Get_U4Reserved1 (pBuf);
    destIpAddr.u4_addr[0] = CRU_BUF_Get_U4Reserved2 (pBuf);

    u2PacketLen = (UINT2) CAPWAP_GET_BUF_LEN (pBuf);
    pRcvdBuf = CAPWAP_BUF_IF_LINEAR (pBuf, 0, u2PacketLen);

    if (NULL == pRcvdBuf)
    {
        pRcvdBuf = (UINT1 *) MemAllocMemBlk (CAPWAP_PKTBUF_POOLID);
        CAPWAP_COPY_FROM_BUF (pBuf, pRcvdBuf, 0, u2PacketLen);
        u1IsNotLinear = OSIX_TRUE;
    }
    pWssIfCapwapDB->CapwapIsGetAllDB.bDiscMode = OSIX_TRUE;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                 pWssIfCapwapDB) == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to Discovery Mode from CAPWAP DB \r\n");
        MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pRcvdBuf);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        UtlShMemFreeDiscRspBuf ((UINT1 *) pDiscResponse);
        return OSIX_FAILURE;
    }

    /* In Auto discovery mode WLC send the
     *discovery response with out any validation */
    if (((pWssIfCapwapDB->u1DiscMode == MAC_DISCOVERY_MODE) &&
         (CapwapValidateDiscReqMsgElems (pRcvdBuf, pDiscReqMsg) ==
          OSIX_SUCCESS)) || (pWssIfCapwapDB->u1DiscMode == AUTO_DISCOVERY_MODE))
    {
        if (u1IsNotLinear == OSIX_TRUE)
        {
            MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pRcvdBuf);
        }

        /* Start assembling the discovery response */
        /* update CAPWAP Header */
        if (CapwapConstructCpHeader (&(pDiscResponse->capwapHdr)) ==
            OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "capwapProcessDiscoveryRequest:"
                        "Failed to construct the capwap Header\n");
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            UtlShMemFreeDiscRspBuf ((UINT1 *) pDiscResponse);
            return OSIX_FAILURE;
        }

        /* Get the internal Id from the CAPWAP get DB */
        if (CapwapGetInternalId (pRcvdBuf, pDiscReqMsg, &u2WtpInternalId)
            != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "capwapProcessDiscoveryRequest:Failed to get the internal id\n");
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            UtlShMemFreeDiscRspBuf ((UINT1 *) pDiscResponse);
            return OSIX_FAILURE;
        }

        /* update Mandatory Message Elements */
        if ((CapwapGetACDescriptor (&(pDiscResponse->acDesc),
                                    &u4MsgLen) == OSIX_FAILURE) ||
            (CapwapGetACName (&(pDiscResponse->acName),
                              &u4MsgLen) == OSIX_FAILURE) ||
            (CapwapGetControlIpAddr (&(pDiscResponse->ctrlAddr),
                                     &u4MsgLen) == OSIX_FAILURE))
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to get Mandatory Discovery Response Message Elements \r\n");
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            UtlShMemFreeDiscRspBuf ((UINT1 *) pDiscResponse);
            return OSIX_FAILURE;
        }

        /* update Optional Message Elements */
        if (CapwapGetVendorPayload (&(pDiscResponse->vendSpec), &u4MsgLen)
            == OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to get Optional Discovery Response Message Elements \r\n");
            /* with out optional message
             * elements session should
             * establish continue */
            /* return OSIX_FAILURE; */
        }

        /* Update the Option Ac Ip List */
        if (CapwapGetAcIpList (&(pDiscResponse->ipv4List), &u4MsgLen)
            == OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to get Optional Discovery Response Message Elements \r\n");
        }
        /* update Control Header */
        pDiscResponse->u1SeqNum = pDiscReqMsg->capwapCtrlHdr.u1SeqNum;
        pDiscResponse->u2CapwapMsgElemenLen =
            (UINT2) (u4MsgLen + CAPWAP_CMN_MSG_ELM_HEAD_LEN);
        pDiscResponse->u1NumMsgBlocks = 0;    /* flags always zero */

        pTxBuf = CAPWAP_ALLOCATE_CRU_BUF (CAPWAP_MAX_PKT_LEN, 0);
        if (NULL == pTxBuf)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "CapwapProcessDiscoveryRequest:Memory Allocation Failed\n");
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            UtlShMemFreeDiscRspBuf ((UINT1 *) pDiscResponse);
            return OSIX_FAILURE;
        }
        pu1TxBuf = CAPWAP_BUF_IF_LINEAR (pTxBuf, 0, CAPWAP_MAX_PKT_LEN);
        if (NULL == pu1TxBuf)
        {
            pu1TxBuf = (UINT1 *) MemAllocMemBlk (CAPWAP_PKTBUF_POOLID);
            CAPWAP_COPY_FROM_BUF (pBuf, pu1TxBuf, 0, CAPWAP_MAX_PKT_LEN);
            u1IsNotLinear = OSIX_TRUE;
        }
        if ((CapwapAssemblePrimaryDiscoveryRsp ((UINT2) u4MsgLen,
                                                pDiscResponse,
                                                pu1TxBuf)) == OSIX_FAILURE)
        {
            CAPWAP_RELEASE_CRU_BUF (pTxBuf);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            UtlShMemFreeDiscRspBuf ((UINT1 *) pDiscResponse);
            return OSIX_FAILURE;
        }

        pu1TempBuf = pu1TxBuf + CAPWAP_MSG_OFFSET;
        CAPWAP_GET_2BYTE (u2TempLength, pu1TempBuf);

        if (u1IsNotLinear == OSIX_TRUE)
        {
            /* If the CRU buffer memory is not linear */
            CAPWAP_COPY_TO_BUF (pTxBuf, pu1TxBuf, 0, CAPWAP_MAX_PKT_LEN);
            MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pu1TxBuf);
        }
        pDiscResponse->u2CapwapMsgElemenLen = u2TempLength;
        i4Status = CapwapCtrlUdpTxPacket (pTxBuf,
                                          destIpAddr.u4_addr[0],
                                          u4DestPort,
                                          (UINT4) (pDiscResponse->
                                                   u2CapwapMsgElemenLen +
                                                   CAPWAP_CMN_HDR_LEN));
        if (OSIX_FAILURE == i4Status)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to Transmit the packet \r\n");
        }
        MEMSET (&WssIfPMDB, 0, sizeof (tWssIfPMDB));
        WssIfPMDB.u2ProfileId = u2WtpInternalId;
        WssIfPMDB.WssPmWLCCapwapStatsSetFlag.bPrimaryDiscStats = OSIX_TRUE;
        WssIfPMDB.WssPmWLCCapwapStatsSetFlag.bRunUpdateReqReceived = OSIX_TRUE;
        if (OSIX_FAILURE == i4Status)
        {
            WssIfPMDB.WssPmWLCCapwapStatsSetFlag.
                bRunUpdateunsuccessfulProcessed = OSIX_TRUE;
        }
        else
        {
            WssIfPMDB.WssPmWLCCapwapStatsSetFlag.bRunUpdateRspTransmitted =
                OSIX_TRUE;
        }
        if (WssIfProcessPMDBMsg (WSS_PM_WLC_CAPWAP_RUN_STATS_SET,
                                 &WssIfPMDB) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to update PM db\r\n");
        }

        CAPWAP_RELEASE_CRU_BUF (pTxBuf);
        UtlShMemFreeDiscRspBuf ((UINT1 *) pDiscResponse);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    }
    else
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to validate the received Disc Req\n");
        if (u1IsNotLinear == OSIX_TRUE)
        {
            MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pRcvdBuf);
        }
        UtlShMemFreeDiscRspBuf ((UINT1 *) pDiscResponse);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapProcessPrimaryDiscoveryResponse             */
/*  Description     : The function is unused in WLC.                    */
/*  Input(s)        : None                                              */
/*  Output(s)       : None                                              */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/
INT4
CapwapProcessPrimaryDiscoveryResponse (tSegment * pBuf,
                                       tCapwapControlPacket * pDiscReqMsg,
                                       tRemoteSessionManager * pSessEntry)
{
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pDiscReqMsg);
    UNUSED_PARAM (pSessEntry);
    return OSIX_SUCCESS;
}

#endif
