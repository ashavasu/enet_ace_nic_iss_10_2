/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: wlchdlrsz.c,v 1.1.1.1 2016/09/20 10:42:41 siva Exp $
 *
 * Description: This file contains the WLCHDLR Sizing Mem Pool Routines.
 *
 *****************************************************************************/

#define _WLCHDLRSZ_C
#include "wlchdlrinc.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
WlchdlrSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < WLCHDLR_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            (INT4)MemCreateMemPool (FsWLCHDLRSizingParams[i4SizingId].u4StructSize,
                              FsWLCHDLRSizingParams[i4SizingId].
                              u4PreAllocatedUnits, MEM_DEFAULT_MEMORY_TYPE,
                              &(WLCHDLRMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            WlchdlrSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
WlchdlrSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsWLCHDLRSizingParams);
    IssSzRegisterModulePoolId (pu1ModName, WLCHDLRMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
WlchdlrSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < WLCHDLR_MAX_SIZING_ID; i4SizingId++)
    {
        if (WLCHDLRMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (WLCHDLRMemPoolIds[i4SizingId]);
            WLCHDLRMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
