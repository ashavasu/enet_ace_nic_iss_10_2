/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: wlchdlrconfigupdate.c,v 1.3 2018/01/22 09:39:53 siva Exp $
 *
 * Description: This file contains the WLCHDLR Config Update routines.
 *
 *****************************************************************************/
#ifndef __CAPWAP_UPDATE_C__
#define __CAPWAP_UPDATE_C__

#include "wlchdlrinc.h"
#include "wssifpmdb.h"

extern char         pConfBuf[WLCHDLR_PKT_SIZE];
extern UINT4        u4ConfBufLen;
extern UINT4        gu1IsConfigResponseReceived;

/************************************************************************/
/*  Function Name   : CapwapProcessConfigUpdateResp                     */
/*  Description     : The function processes the received CAPWAP        */
/*                    Config Update response.                           */
/*  Input(s)        : ConfigUpdateRespMsg- ConfigUpdate response packet */
/*                                           received                   */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
CapwapProcessConfigUpdateResp (tSegment * pBuf,
                               tCapwapControlPacket * pConfigUpdateRespMsg,
                               tRemoteSessionManager * pSessEntry)
{
    UINT1              *pRcvBuf = NULL;
    UINT2               u2PacketLen = 0;
    UINT4               u4Offset = 0;
    UINT2               u2MsgIndex = RESULT_CODE_CONF_UPDATE_RESP_INDEX;
    UINT1              *pu1TxBuf = NULL;
    tCRU_BUF_CHAIN_HEADER *pTxBuf = NULL;
    UINT2               u2NumOptionalMsg = 0, u2MsgType = 0;
    UINT1               u1Index = 0;
    UINT1               u1IsNotLinear = OSIX_FALSE;
    UINT2               u2IntId = 0, u2Type = 0, u2Len = 0;
    UINT4               uOffset = 0;
    UINT1               array[CAPWAP_WLC_NAME_SIZE] = { 0 };
    UINT1               uIndex = 0;
    tCapwapControlPacket pCapwapCtrlmsg;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tResCode            ResCode;
    tRadioIfOperStatus  RadioOper;
    tRadioIfMsgStruct   WssMsgStruct;
    tWssIfCapDB        *ptmpWssIfCapwapDB = NULL;
    unCapwapMsgStruct   CapwapMsgStruct;
    tWssIfPMDB          WssIfPMDB;
    UINT1               u1IsRadioConfUpdReq = OSIX_FALSE;
#ifdef RFMGMT_WANTED
    UINT1               u1IsRfMgmtConfUpdReq = OSIX_FALSE;
    tRfMgmtMsgStruct    RfMgmtMsgStruct;
#endif

    WLCHDLR_FN_ENTRY ();
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MemAllocateMemBlock (WSS_IF_DB_TEMP_MEM_POOL_ID,
                             (UINT1 **) (VOID *) &ptmpWssIfCapwapDB);
    if (ptmpWssIfCapwapDB == NULL)
    {
        PRINTF
            ("!!!Function = %s ; Line No = %d Alloc Failed - WSS_IF_DB_TEMP_MEM_POOL_ID\n",
             __FUNCTION__, __LINE__);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return NO_AP_PRESENT;
    }

    MEMSET (&pCapwapCtrlmsg, 0, sizeof (tCapwapControlPacket));
    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&ResCode, 0, sizeof (tResCode));
    MEMSET (&RadioOper, 0, sizeof (tRadioIfOperStatus));
    MEMSET (&WssMsgStruct, 0, sizeof (tRadioIfMsgStruct));
    MEMSET (ptmpWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&CapwapMsgStruct, 0, sizeof (unCapwapMsgStruct));
#ifdef RFMGMT_WANTED
    MEMSET (&RfMgmtMsgStruct, 0, sizeof (tRfMgmtMsgStruct));
#endif

    if (pSessEntry == NULL)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                     "WLCHDLR Session Entry is NULL, return FAILURE \r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (ptmpWssIfCapwapDB);
        return NO_AP_PRESENT;
    }
    if (pSessEntry->eCurrentState != CAPWAP_RUN)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "Session not in RUN state \r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (ptmpWssIfCapwapDB);
        return NO_AP_PRESENT;
    }
    if (WlcHdlrTmrStop (pSessEntry, WLCHDLR_RETRANSMIT_INTERVAL_TMR)
        == OSIX_FAILURE)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                     "Failed to stop the Retransmit Timer \r\n");
        WlchdlrFreeTransmittedPacket (pSessEntry);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (ptmpWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    WlchdlrFreeTransmittedPacket (pSessEntry);

    pTxBuf = WLCHDLR_ALLOCATE_CRU_BUF (u4ConfBufLen, 0);

    if (pTxBuf == NULL)
    {
        WLCHDLR_TRC1 (CAPWAP_FAILURE,
                      "Failed to allocate memory for pTxBuf of Len %d\r\n",
                      u4ConfBufLen);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (ptmpWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    WLCHDLR_COPY_TO_BUF (pTxBuf, pConfBuf, 0, u4ConfBufLen);

    CapwapMsgStruct.CapwapParseReq.pData = pTxBuf;
    CapwapMsgStruct.CapwapParseReq.pCapwapCtrlMsg = &pCapwapCtrlmsg;

    if (WssIfProcessCapwapMsg (WSS_CAPWAP_PARSE_REQ, &CapwapMsgStruct)
        != OSIX_SUCCESS)
    {
        WLCHDLR_TRC (CAPWAP_FAILURE, "WSS_CAPWAP_PARSE_REQ FAILED \r\n");
        WLCHDLR_RELEASE_CRU_BUF (pTxBuf);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (ptmpWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    else
    {
        pu1TxBuf = WLCHDLR_BUF_IF_LINEAR (pTxBuf, 0, u4ConfBufLen);
        if (pu1TxBuf == NULL)
        {
            WLCHDLR_PKTBUF_ALLOC_MEM_BLOCK (pu1TxBuf);
            CAPWAP_COPY_FROM_BUF (pTxBuf, pu1TxBuf, 0, u4ConfBufLen);
            u1IsNotLinear = OSIX_TRUE;
        }

        u2NumOptionalMsg = pCapwapCtrlmsg.numOptionalElements;
        for (u1Index = 0; u1Index < u2NumOptionalMsg; u1Index++)
        {
            u2MsgType = pCapwapCtrlmsg.
                capwapMsgElm[MAX_CONF_UPDATE_REQ_MAND_MSG_ELEMENTS +
                             u1Index].u2MsgType;
            uOffset = pCapwapCtrlmsg.
                capwapMsgElm[MAX_CONF_UPDATE_REQ_MAND_MSG_ELEMENTS + u1Index].
                pu2Offset[u1Index];
            pu1TxBuf += uOffset;
            switch (u2MsgType)
            {
                case RADIO_ADMIN_STATE:
                case IEEE_ANTENNA:
                case IEEE_DIRECT_SEQUENCE_CONTROL:
                case IEEE_INFORMATION_ELEMENT:
                case IEEE_MAC_OPERATION:
                case IEEE_MULTIDOMAIN_CAPABILITY:
                case IEEE_OFDM_CONTROL:
                case IEEE_RATE_SET:
                case IEEE_TX_POWER:
                case IEEE_STATION_QOS_PROFILE:
                case IEEE_WTP_RADIO_CONFIGURATION:
                {
                    u1IsRadioConfUpdReq = OSIX_TRUE;
                }
                    break;

                case LOCATION_DATA:
                    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpLocation = OSIX_TRUE;
                    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2IntId;
                    CAPWAP_GET_NBYTE (pWssIfCapwapDB->CapwapGetDB.
                                      au1WtpLocation, pu1TxBuf, u2Len);
                    break;

                case WTP_NAME:
                    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpName = OSIX_TRUE;
                    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2IntId;
                    break;

                case IMAGE_IDENTIFIER:
                    break;

                case CAPWAP_TIMERS:
                    CAPWAP_GET_1BYTE (pWssIfCapwapDB->CapwapGetDB.
                                      u2WtpMaxDiscoveryInterval, pu1TxBuf);
                    CAPWAP_GET_1BYTE (pWssIfCapwapDB->CapwapGetDB.
                                      u2WtpEchoInterval, pu1TxBuf);
                    if (pWssIfCapwapDB->CapwapGetDB.u2WtpMaxDiscoveryInterval
                        != 0)
                    {
                        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId =
                            OSIX_TRUE;
                        pWssIfCapwapDB->CapwapIsGetAllDB.
                            bWtpMaxDiscoveryInterval = OSIX_TRUE;
                    }
                    else if (pWssIfCapwapDB->CapwapGetDB.u2WtpEchoInterval != 0)
                    {
                        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId =
                            OSIX_TRUE;
                        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpEchoInterval =
                            OSIX_TRUE;
                    }
                    break;

                case DECRYPTION_ERROR_REPORT_PERIOD:
                    break;

                case IDLE_TIMEOUT:
                    CAPWAP_GET_4BYTE (pWssIfCapwapDB->CapwapGetDB.
                                      u4WtpIdleTimeout, pu1TxBuf);
                    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpIdleTimeout =
                        OSIX_TRUE;
                    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                    break;

                case WTP_FALLBACK:
                    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpFallbackEnable =
                        OSIX_TRUE;
                    CAPWAP_GET_1BYTE (pWssIfCapwapDB->CapwapGetDB.
                                      u1WtpFallbackEnable, pu1TxBuf);
                    break;

                case STATS_TIMER:
                    CAPWAP_GET_2BYTE (pWssIfCapwapDB->CapwapGetDB.
                                      u2WtpStatisticsTimer, pu1TxBuf);
                    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpStatisticsTimer =
                        OSIX_TRUE;
                    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                    break;

                case AC_TIMESTAMP:
                    break;

                case ADD_MAC_ACL_ENTRY:
                    break;

                case DELETE_MAC_ACL_ENTRY:
                    break;

                case WTP_STATIC_IP_ADDR_INFO:
                    break;

                case AC_NAME_WITH_PRIO:
                    CAPWAP_GET_2BYTE (u2Type, pu1TxBuf);
                    CAPWAP_GET_2BYTE (u2Len, pu1TxBuf);
                    CAPWAP_GET_1BYTE (pWssIfCapwapDB->CapwapGetDB.
                                      AcNameWithPri[0].u1Priority, pu1TxBuf);
                    CAPWAP_GET_NBYTE (pWssIfCapwapDB->CapwapGetDB.
                                      AcNameWithPri[0].wlcName, pu1TxBuf,
                                      u2Len);
                    pWssIfCapwapDB->CapwapIsGetAllDB.bAcNamewithPriority =
                        OSIX_TRUE;
                    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2IntId;
                    ptmpWssIfCapwapDB->CapwapIsGetAllDB.bAcNamewithPriority =
                        OSIX_TRUE;
                    ptmpWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId =
                        OSIX_TRUE;
                    ptmpWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2IntId;
                    break;
                case VENDOR_SPECIFIC_PAYLOAD:
                    u2Len = CAPWAP_SKIP_VEND_INFO;
                    CAPWAP_SKIP_N_BYTES (u2Len, pu1TxBuf);
                    CAPWAP_GET_2BYTE (u2Type, pu1TxBuf);
                    switch (u2Type)
                    {
#ifdef RFMGMT_WANTED
                        case NEIGH_CONFIG_VENDOR_MSG:
                        case CLIENT_CONFIG_VENDOR_MSG:
                        case CH_SWITCH_STATUS_VENDOR_MSG:
                        case VENDOR_SPECT_MGMT_TPC_MSG:
                        case VENDOR_SPECTRUM_MGMT_DFS_MSG:
                            u1IsRfMgmtConfUpdReq = OSIX_TRUE;
                            break;
#endif
                        default:
                            break;
                    }
                default:
                    break;
            }
        }

        WssIfProcessCapwapMsg (WSS_CAPWAP_PARSE_MEMREL_REQ, &CapwapMsgStruct);

        if (u1IsNotLinear == OSIX_TRUE)
        {
            WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pu1TxBuf);
        }
    }
    WLCHDLR_RELEASE_CRU_BUF (pTxBuf);
    u1IsNotLinear = OSIX_FALSE;

    u2PacketLen = (UINT2) CAPWAP_GET_BUF_LEN (pBuf);
    pRcvBuf = WLCHDLR_BUF_IF_LINEAR (pBuf, 0, u2PacketLen);
    if (pRcvBuf == NULL)
    {
        WLCHDLR_PKTBUF_ALLOC_MEM_BLOCK (pRcvBuf);
        WLCHDLR_COPY_FROM_BUF (pBuf, pRcvBuf, 0, u2PacketLen);
        u1IsNotLinear = OSIX_TRUE;
    }

    u4Offset = pConfigUpdateRespMsg->capwapMsgElm[u2MsgIndex].pu2Offset[0];
    pRcvBuf += u4Offset;

    CAPWAP_GET_2BYTE (ResCode.u2MsgEleType, pRcvBuf);
    CAPWAP_GET_2BYTE (ResCode.u2MsgEleLen, pRcvBuf);
    CAPWAP_GET_4BYTE (ResCode.u4Value, pRcvBuf);

    CAPWAP_GET_2BYTE (RadioOper.u2MessageType, pRcvBuf);
    CAPWAP_GET_2BYTE (RadioOper.u2MessageLength, pRcvBuf);
    CAPWAP_GET_1BYTE (RadioOper.u1RadioId, pRcvBuf);
    CAPWAP_GET_1BYTE (RadioOper.u1OperStatus, pRcvBuf);
    CAPWAP_GET_1BYTE (RadioOper.u1FailureCause, pRcvBuf);

    if (u1IsRadioConfUpdReq == OSIX_TRUE)
    {
        WssMsgStruct.unRadioIfMsg.RadioIfConfigUpdateRsp.u2WtpInternalId = 1;
        WssMsgStruct.unRadioIfMsg.RadioIfConfigUpdateRsp.u4ResultCode =
            ResCode.u4Value;

        if (WssIfProcessRadioIfMsg
            (WSS_RADIOIF_CONFIG_UPDATE_RSP, &WssMsgStruct) != OSIX_SUCCESS)
        {
            WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "Failed to Set Config update "
                         "Response message elements \r\n");
            if (u1IsNotLinear == OSIX_TRUE)
            {
                WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pRcvBuf);
            }
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (ptmpWssIfCapwapDB);
            return OSIX_FAILURE;
        }
    }

#ifdef RFMGMT_WANTED
    if (u1IsRfMgmtConfUpdReq == OSIX_TRUE)
    {
        RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtConfigUpdateRsp.u2WtpInternalId =
            pConfigUpdateRespMsg->u2IntProfileId;
        RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtConfigUpdateRsp.u4ResultCode =
            ResCode.u4Value;
        RfMgmtMsgStruct.u1Opcode = (UINT1) u2Type;

        if (WssIfProcessRfMgmtMsg (RFMGMT_CONFIG_UPDATE_RSP, &RfMgmtMsgStruct)
            != OSIX_SUCCESS)
        {
            WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "Failed to Set Config update "
                         "Response message elements \r\n");
            if (u1IsNotLinear == OSIX_TRUE)
            {
                WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pRcvBuf);
            }
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (ptmpWssIfCapwapDB);
            return OSIX_FAILURE;
        }
    }
#endif

    if (ResCode.u4Value == OSIX_SUCCESS)
    {
        MEMSET (&WssIfPMDB, 0, sizeof (tWssIfPMDB));
        WssIfPMDB.u2ProfileId = pSessEntry->u2IntProfileId;
        WssIfPMDB.WssPmWLCCapwapStatsSetFlag.bConfigUpdateStats = OSIX_TRUE;
        WssIfPMDB.WssPmWLCCapwapStatsSetFlag.bRunUpdateRspReceived = OSIX_TRUE;

        if (WssIfProcessPMDBMsg (WSS_PM_WLC_CAPWAP_RUN_STATS_SET,
                                 &WssIfPMDB) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to update PM db\r\n");
        }

        gu1IsConfigResponseReceived = OSIX_SUCCESS;

        if (u2MsgType == AC_NAME_WITH_PRIO)
        {
            if (pWssIfCapwapDB->CapwapGetDB.AcNameWithPri[0].u1Priority
                != OSIX_FALSE)
            {
                if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                             ptmpWssIfCapwapDB) == OSIX_FAILURE)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "Failed to Validate the AC Name List \r\n");
                    if (u1IsNotLinear == OSIX_TRUE)
                    {
                        WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pRcvBuf);
                    }
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    WSS_IF_DB_TEMP_MEM_RELEASE (ptmpWssIfCapwapDB);
                    return OSIX_FAILURE;
                }
                uIndex =
                    (UINT1) ((pWssIfCapwapDB->CapwapGetDB.AcNameWithPri[0].
                              u1Priority) - 1);
                ptmpWssIfCapwapDB->CapwapGetDB.AcNameWithPri[uIndex].
                    u1Priority =
                    pWssIfCapwapDB->CapwapGetDB.AcNameWithPri[0].u1Priority;
                MEMCPY (ptmpWssIfCapwapDB->CapwapGetDB.AcNameWithPri[uIndex].
                        wlcName,
                        pWssIfCapwapDB->CapwapGetDB.AcNameWithPri[0].wlcName,
                        STRLEN (pWssIfCapwapDB->CapwapGetDB.AcNameWithPri[0].
                                wlcName));

                if (MEMCMP
                    (pWssIfCapwapDB->CapwapGetDB.AcNameWithPri[0].wlcName,
                     array, CAPWAP_WLC_NAME_SIZE) == 0)
                {
                    MEMSET (ptmpWssIfCapwapDB->CapwapGetDB.
                            AcNameWithPri[uIndex].wlcName, 0,
                            CAPWAP_WLC_NAME_SIZE);
                }
            }
        }

        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_SET_DB, ptmpWssIfCapwapDB)
            == OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to Validate the AC Name List \r\n");
            if (u1IsNotLinear == OSIX_TRUE)
            {
                WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pRcvBuf);
            }
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (ptmpWssIfCapwapDB);
            return OSIX_FAILURE;
        }
    }
    else
    {
        MEMSET (&WssIfPMDB, 0, sizeof (tWssIfPMDB));
        WssIfPMDB.u2ProfileId = pSessEntry->u2IntProfileId;
        WssIfPMDB.WssPmWLCCapwapStatsSetFlag.bConfigUpdateStats = OSIX_TRUE;
        WssIfPMDB.WssPmWLCCapwapStatsSetFlag.bRunUpdateunsuccessfulProcessed
            = OSIX_TRUE;

        if (WssIfProcessPMDBMsg (WSS_PM_WLC_CAPWAP_RUN_STATS_SET,
                                 &WssIfPMDB) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to update PM db\r\n");
        }

        gu1IsConfigResponseReceived = OSIX_FAILURE;
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to Update the DB At WLC Side \r\n");
        if (u1IsNotLinear == OSIX_TRUE)
        {
            WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pRcvBuf);
        }
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (ptmpWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    if (u1IsNotLinear == OSIX_TRUE)
    {
        WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pRcvBuf);
    }

    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    WSS_IF_DB_TEMP_MEM_RELEASE (ptmpWssIfCapwapDB);
    WLCHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapValidateConfUpdateRespMsgElem                  */
/*  Description     : The function validates the functional             */
/*                    characteristics of the received CAPWAP            */
/*                    Config Update request agains the configured       */
/*                    WTP profile                                       */
/*  Input(s)        : ConfigUpdateRespMsg - parsed CAPWAP Config Update */
/*                                            response                  */
/*                    pRcvBuf - Received Config Update packet           */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
CapValidateConfUpdateRespMsgElem (UINT1 *pRcvBuf,
                                  tCapwapControlPacket * ConfigUpdateRespMsg)
{

    tResCode            resultCode;
    tVendorSpecPayload  vendSpec;
    tRadioIfOperStatus  radioOper;

    WLCHDLR_FN_ENTRY ();

    MEMSET (&resultCode, 0, sizeof (tResCode));
    MEMSET (&vendSpec, 0, sizeof (tVendorSpecPayload));
    MEMSET (&radioOper, 0, sizeof (tRadioIfOperStatus));

#if WLCHDLR_UNUSED_CODE
    /* Validate the mandatory message elements */
    if ((CapwapValidateResultCode (pRcvBuf, ConfigUpdateRespMsg,
                                   &resultCode,
                                   RESULT_CODE_CONF_UPDATE_RESP_INDEX) ==
         OSIX_SUCCESS))
    {
        return OSIX_SUCCESS;
    }

    if ((CapwapValidateVendSpecPld (pRcvBuf, ConfigUpdateRespMsg,
                                    &vendSpec,
                                    VENDOR_SPECIFIC_PAYLOAD_CONF_UPDATE_RESP_INDEX)
         == OSIX_SUCCESS))

#endif
        if (CapwapValidateRadioOperState (pRcvBuf, ConfigUpdateRespMsg,
                                          &radioOper,
                                          RADIO_OPER_STATE_CONF_UPDATE_RESP_INDEX)
            == OSIX_SUCCESS)
        {
            return OSIX_SUCCESS;
        }

    WLCHDLR_FN_EXIT ();

    return OSIX_FAILURE;
}

/************************************************************************/
/*  Function Name   : CapwapProcessConfigUpdateRequest                  */
/*  Description     : The function is unused in WLC.                    */
/*  Input(s)        : None                                              */
/*  Output(s)       : None                                              */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/
INT4
CapwapProcessConfigUpdateRequest (tSegment * pBuf,
                                  tCapwapControlPacket * pConfigUpdateReqMsg,
                                  tRemoteSessionManager * pSessEntry)
{
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pConfigUpdateReqMsg);
    UNUSED_PARAM (pSessEntry);
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapProcessWlanConfigReq                        */
/*  Description     : The function is unused in WLC.                    */
/*  Input(s)        : None                                              */
/*  Output(s)       : None                                              */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/
INT4
CapwapProcessWlanConfigReq (tCRU_BUF_CHAIN_HEADER * pBuf,
                            tCapwapControlPacket * pWlanConfReq,
                            tRemoteSessionManager * pSessEntry)
{
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pWlanConfReq);
    UNUSED_PARAM (pSessEntry);
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : WlcHdlrValidateWlanConfigRsp                      */
/*  Description     : The function validates the Wlan Config Response   */
/*  Input(s)        : pRcvdBuf - Buffer Pointer                         */
/*                    pWlanConfRsp - Wlan Config Rsp                    */
/*                    pSessEntry - Session Entry                        */
/*  Output(s)       : None                                              */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/
INT4
WlcHdlrValidateWlanConfigRsp (UINT1 *pRcvdBuf,
                              tCapwapControlPacket * pWlanConfRsp,
                              tRemoteSessionManager * pSessEntry)
{
    UINT4               u4Offset = 0;
    tWssWlanMsgStruct  *pWssWlanMsgStruct = NULL;
    UINT1               u1Index = 0;
    UINT2               u2NumOptionalMsg = 0, u2MsgType = 0;
    WLCHDLR_FN_ENTRY ();
    pWssWlanMsgStruct =
        (tWssWlanMsgStruct *) (VOID *) UtlShMemAllocWssWlanBuf ();
    if (pWssWlanMsgStruct == NULL)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                     "WlcHdlrValidateWlanConfigRsp:- "
                     "UtlShMemAllocWssWlanBuf returned failure\n");
        return OSIX_FAILURE;
    }

    MEMSET (pWssWlanMsgStruct, 0, sizeof (tWssWlanMsgStruct));

    u2NumOptionalMsg = pWlanConfRsp->numOptionalElements;
    u4Offset = pWlanConfRsp->capwapMsgElm[0].pu2Offset[0];
    pRcvdBuf += u4Offset;

    pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigRsp.u2WtpInternalId =
        pSessEntry->u2IntProfileId;
    /* Get the Result code. Its a Mandatory Parameter */
    CAPWAP_GET_2BYTE (pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigRsp.
                      WssWlanResultCode.u2MessageType, pRcvdBuf);
    CAPWAP_GET_2BYTE (pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigRsp.
                      WssWlanResultCode.u2Length, pRcvdBuf);
    CAPWAP_GET_4BYTE (pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigRsp.
                      WssWlanResultCode.u4ResultCode, pRcvdBuf);
    pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigRsp.WssWlanResultCode.
        u1IsPresent = OSIX_TRUE;
    /* Validate IEEE radio optional elements */
    for (u1Index = 0; u1Index < u2NumOptionalMsg; u1Index++)
    {
        u2MsgType = pWlanConfRsp->capwapMsgElm[u1Index + 1].u2MsgType;
        switch (u2MsgType)
        {
            case IEEE_ASSIGNED_WTPBSSID:
            {
                CAPWAP_GET_2BYTE (pWssWlanMsgStruct->unWssWlanMsg.
                                  WssWlanConfigRsp.WssWlanRsp.u2MessageType,
                                  pRcvdBuf);
                CAPWAP_GET_2BYTE (pWssWlanMsgStruct->unWssWlanMsg.
                                  WssWlanConfigRsp.WssWlanRsp.u2Length,
                                  pRcvdBuf);
                CAPWAP_GET_1BYTE (pWssWlanMsgStruct->unWssWlanMsg.
                                  WssWlanConfigRsp.WssWlanRsp.u1RadioId,
                                  pRcvdBuf);
                CAPWAP_GET_1BYTE (pWssWlanMsgStruct->unWssWlanMsg.
                                  WssWlanConfigRsp.WssWlanRsp.u1WlanId,
                                  pRcvdBuf);
                CAPWAP_GET_NBYTE (pWssWlanMsgStruct->unWssWlanMsg.
                                  WssWlanConfigRsp.WssWlanRsp.BssId, pRcvdBuf,
                                  sizeof (tMacAddr));
                pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigRsp.bIsSetAddRsp =
                    OSIX_TRUE;
            }
                break;
            case VENDOR_SPECIFIC_PAYLOAD:
                break;
            default:
                break;
        }
    }
    /* Call WLAN  Module to Validte received response and set the response */
    if (WssIfProcessWssWlanMsg (WSS_WLAN_CONFIG_RSP, pWssWlanMsgStruct)
        != OSIX_SUCCESS)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                     "Failed to Validate the Add WLAN Message \r\n");
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        return OSIX_FAILURE;
    }

    UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
    WLCHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapProcessWlanConfigRsp                        */
/*  Description     : The function processes the received CAPWAP        */
/*                    WLAN Config response.                             */
/*  Input(s)        : pRcvdBuf - Buffer Pointer                         */
/*                    pWlanConfRsp - Wlan Config Rsp                    */
/*                    pSessEntry - Session Entry                        */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
CapwapProcessWlanConfigRsp (tCRU_BUF_CHAIN_HEADER * pBuf,
                            tCapwapControlPacket * pWlanConfRsp,
                            tRemoteSessionManager * pSessEntry)
{
    UINT2               u2PacketLen = 0;
    UINT1               u1IsNotLinear = OSIX_FALSE;
    UINT1              *pRcvdBuf = NULL;
    tWssIfPMDB          WssIfPMDB;

    WLCHDLR_FN_ENTRY ();

    if (pSessEntry == NULL)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "WLCHDLR Session Entry is NULL, "
                     "return FAILURE \r\n");
        return OSIX_FAILURE;
    }
    if (WlcHdlrTmrStop (pSessEntry, WLCHDLR_RETRANSMIT_INTERVAL_TMR)
        == OSIX_FAILURE)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                     "Failed to stop the Retransmit Timer \r\n");
        WlchdlrFreeTransmittedPacket (pSessEntry);
        return OSIX_FAILURE;
    }
    WlchdlrFreeTransmittedPacket (pSessEntry);

    /* pWlanConfRsp->u2IntProfileId = pSessEntry->u2IntProfileId;  */
    u2PacketLen = (UINT2) WLCHDLR_GET_BUF_LEN (pBuf);
    pRcvdBuf = WLCHDLR_BUF_IF_LINEAR (pBuf, 0, u2PacketLen);
    if (pRcvdBuf == NULL)
    {
        WLCHDLR_PKTBUF_ALLOC_MEM_BLOCK (pRcvdBuf);
        WLCHDLR_COPY_FROM_BUF (pBuf, pRcvdBuf, 0, u2PacketLen);
        u1IsNotLinear = OSIX_TRUE;
    }
    if (WlcHdlrValidateWlanConfigRsp (pRcvdBuf, pWlanConfRsp, pSessEntry)
        == OSIX_FAILURE)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "Failed to Validate/Set the "
                     "Wlan Configuration Response \r\n");
        if (u1IsNotLinear == OSIX_TRUE)
        {
            WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pRcvdBuf);
        }
        MEMSET (&WssIfPMDB, 0, sizeof (tWssIfPMDB));
        WssIfPMDB.u2ProfileId = pSessEntry->u2IntProfileId;
        WssIfPMDB.WssPmWLCCapwapStatsSetFlag.bConfigUpdateStats = OSIX_TRUE;
        WssIfPMDB.WssPmWLCCapwapStatsSetFlag.bRunUpdateunsuccessfulProcessed
            = OSIX_TRUE;

        if (WssIfProcessPMDBMsg (WSS_PM_WLC_CAPWAP_RUN_STATS_SET,
                                 &WssIfPMDB) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to update PM db\r\n");
        }

        return OSIX_FAILURE;
    }
    if (u1IsNotLinear == OSIX_TRUE)
    {
        WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pRcvdBuf);
    }
    MEMSET (&WssIfPMDB, 0, sizeof (tWssIfPMDB));
    WssIfPMDB.u2ProfileId = pSessEntry->u2IntProfileId;
    WssIfPMDB.WssPmWLCCapwapStatsSetFlag.bConfigUpdateStats = OSIX_TRUE;
    WssIfPMDB.WssPmWLCCapwapStatsSetFlag.bRunUpdateRspReceived = OSIX_TRUE;

    if (WssIfProcessPMDBMsg (WSS_PM_WLC_CAPWAP_RUN_STATS_SET,
                             &WssIfPMDB) != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to update PM db\r\n");
    }

    WLCHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

#endif
