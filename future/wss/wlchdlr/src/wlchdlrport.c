/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: wlchdlrport.c,v 1.5 2018/02/15 10:22:24 siva Exp $
 *
 * Description: This file contains the WLCHDLR Interface related routines.
 *
 *****************************************************************************/

#ifndef __WLCHDLRPORT_C__
#define __WLCHDLRPORT_C__

#include "wlchdlrinc.h"
#include "wssifpmdb.h"

PUBLIC UINT4        gu4WlcHdlrSysLogId;
#ifdef SNTP_WANTED
extern UINT4        SntpTmToSec (tUtlTm *);
#endif

char                pStaBuf[WLCHDLR_PKT_SIZE] = { 0 };
char                pConfBuf[WLCHDLR_PKT_SIZE] = { 0 };
UINT4               u4StaBufLen = 0;
UINT4               u4ConfBufLen = 0;
extern tRBTree      gWlcHdlrStaConfigDB;

/************************************************************************/
/*  Function Name   : WlcHdlrProcessWssIfMsg                            */
/*  Description     : This function is the WlcHdlr Interface for other  */
/*                    WSS modules.                                      */
/*  Input(s)        : MsgType - Message Type                            */
/*                    pWlcHdlrMsgStruct - Message Structure             */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS | OSIX_FAILURE                       */
/************************************************************************/
INT4
WlcHdlrProcessWssIfMsg (UINT1 MsgType, unWlcHdlrMsgStruct * pWlcHdlrMsgStruct)
{
    tWssWlanMsgStruct  *pWssWlanMsgStruct = NULL;
    tWssMacMsgStruct   *pWssMacMsgStruct = NULL;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    INT4                i4RetVal = 0;
#ifdef WSSSTA_WANTED
    tWssStaMsgStruct   *pWssStaMsgStruct = NULL;
#endif
    unCapwapMsgStruct   CapwapMsgStruct;

    WLCHDLR_FN_ENTRY ();
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    pWssMacMsgStruct =
        (tWssMacMsgStruct *) (VOID *) UtlShMemAllocMacMsgStructBuf ();
    if (pWssMacMsgStruct == NULL)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                     "WlcHdlrProcessWssIfMsg:-"
                     "UtlShMemAllocMacMsgStructBuf returned failure\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    pWssWlanMsgStruct =
        (tWssWlanMsgStruct *) (VOID *) UtlShMemAllocWssWlanBuf ();
    if (pWssWlanMsgStruct == NULL)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                     "WlcHdlrProcessWssIfMsg:- "
                     "UtlShMemAllocWssWlanBuf returned failure\n");
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssMacMsgStruct);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    pWssStaMsgStruct = (tWssStaMsgStruct *)
        MemAllocMemBlk (WLCHDLR_STA_MSG_POOLID);
    if (pWssStaMsgStruct == NULL)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "pWssStaMsgStruct Memory Allocation"
                     "{Failed\n");
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssMacMsgStruct);
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    MEMSET (pWssWlanMsgStruct, 0, sizeof (tWssWlanMsgStruct));
    MEMSET (pWssMacMsgStruct, 0, sizeof (tWssMacMsgStruct));
#ifdef WSSSTA_WANTED
    MEMSET (pWssStaMsgStruct, 0, sizeof (tWssStaMsgStruct));
#endif
    MEMSET (&CapwapMsgStruct, 0, sizeof (unCapwapMsgStruct));

    switch (MsgType)
    {
        case WSS_WLCHDLR_CAPWAP_CONF_UPDATE_REQ:
        case WSS_WLCHDLR_RADIO_CONF_UPDATE_REQ:
        case WSS_WLCHDLR_PM_CONF_UPDATE_REQ:
#ifdef RFMGMT_WANTED
        case WSS_WLCHDLR_RFMGMT_CONF_UPDATE_REQ:
#endif
            i4RetVal = WlcHdlrProcessConfigUpdateReq (MsgType,
                                                      pWlcHdlrMsgStruct);
            if (OSIX_FAILURE == i4RetVal)
            {
                WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "Failed to process "
                             "Configuration Update Request \r\n");
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssMacMsgStruct);
                MemReleaseMemBlock (WLCHDLR_STA_MSG_POOLID,
                                    (UINT1 *) pWssStaMsgStruct);
                return OSIX_FAILURE;
            }
            else if (NO_AP_PRESENT == i4RetVal)
            {
                WLCHDLR_TRC (WLCHDLR_INFO_TRC,
                             "Conf Req - AP is not Present \r\n");
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssMacMsgStruct);
                MemReleaseMemBlock (WLCHDLR_STA_MSG_POOLID,
                                    (UINT1 *) pWssStaMsgStruct);
                return NO_AP_PRESENT;
            }
            break;
        case WSS_WLCHDLR_CLKIWF_CONF_UPDATE_REQ:
        {
            i4RetVal = WlcHdlrProcessConfigUpdateReq (MsgType,
                                                      pWlcHdlrMsgStruct);
            if (OSIX_FAILURE == i4RetVal)
            {
                WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "Failed to process "
                             "ClkIwf Configuration Update Request \r\n");
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssMacMsgStruct);
                MemReleaseMemBlock (WLCHDLR_STA_MSG_POOLID,
                                    (UINT1 *) pWssStaMsgStruct);
                return OSIX_FAILURE;
            }
            else if (NO_AP_PRESENT == i4RetVal)
            {
                WLCHDLR_TRC (WLCHDLR_INFO_TRC, "ClkIwf Conf Update "
                             "Req - AP is not Present \r\n");
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssMacMsgStruct);
                MemReleaseMemBlock (WLCHDLR_STA_MSG_POOLID,
                                    (UINT1 *) pWssStaMsgStruct);
                return NO_AP_PRESENT;
            }
        }
            break;
        case WSS_WLCHDLR_RESET_REQ:
            if (WlcHdlrProcessResetReq (&pWlcHdlrMsgStruct->ResetReq) ==
                OSIX_FAILURE)
            {
                WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "Failed to process Reset "
                             "Request \r\n");
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssMacMsgStruct);
                MemReleaseMemBlock (WLCHDLR_STA_MSG_POOLID,
                                    (UINT1 *) pWssStaMsgStruct);
                return OSIX_FAILURE;
            }
            else if (NO_AP_PRESENT == i4RetVal)
            {
                WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                             "Reset Req - AP is not Present \r\n");
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssMacMsgStruct);
                MemReleaseMemBlock (WLCHDLR_STA_MSG_POOLID,
                                    (UINT1 *) pWssStaMsgStruct);
                return NO_AP_PRESENT;
            }
            break;
        case WSS_WLCHDLR_CLEAR_CONF_REQ:

            if (WlcHdlrProcessClearConfigReq
                (&pWlcHdlrMsgStruct->ClearConfigReq) == OSIX_FAILURE)
            {
                WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "Failed to process Clear "
                             "Request \r\n");
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssMacMsgStruct);
                MemReleaseMemBlock (WLCHDLR_STA_MSG_POOLID,
                                    (UINT1 *) pWssStaMsgStruct);
                return OSIX_FAILURE;
            }
            else if (NO_AP_PRESENT == i4RetVal)
            {
                WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                             "Clear Req - AP is not Present \r\n");
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssMacMsgStruct);
                MemReleaseMemBlock (WLCHDLR_STA_MSG_POOLID,
                                    (UINT1 *) pWssStaMsgStruct);
                return NO_AP_PRESENT;
            }
            break;
        case WSS_WLCHDLR_WLAN_CONF_REQ:
            i4RetVal =
                WlcHdlrProcessWlanConfigRequest (&pWlcHdlrMsgStruct->
                                                 WssWlanConfigReq);

            if (i4RetVal == OSIX_FAILURE)
            {
                WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                             "Failed to process WLAN Config Request \r\n");
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssMacMsgStruct);
                MemReleaseMemBlock (WLCHDLR_STA_MSG_POOLID,
                                    (UINT1 *) pWssStaMsgStruct);
                return OSIX_FAILURE;
            }
            else if (i4RetVal == NO_AP_PRESENT)
            {
                WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                             "WLAN - AP is not Present\r\n");
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssMacMsgStruct);
                MemReleaseMemBlock (WLCHDLR_STA_MSG_POOLID,
                                    (UINT1 *) pWssStaMsgStruct);
                return NO_AP_PRESENT;
            }

            break;

        case WSS_WLCHDLR_STATION_CONF_REQ:
            if (WlcHdlrProcessStationConfigReq
                (&pWlcHdlrMsgStruct->StationConfReq) == OSIX_FAILURE)
            {
                WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "Failed to process Station "
                             "Configuration Request \r\n");
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssMacMsgStruct);
                MemReleaseMemBlock (WLCHDLR_STA_MSG_POOLID,
                                    (UINT1 *) pWssStaMsgStruct);
                return OSIX_FAILURE;
            }
            break;
        case WSS_WLCHDLR_CAPWAP_QUEUE_REQ:
            if (WlcHdlrEnqueCtrlPkts (pWlcHdlrMsgStruct) == OSIX_FAILURE)
            {
                WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "Failed to process "
                             "CAPWAP_QUEUE_REQ Request msgs\r\n");
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssMacMsgStruct);
                MemReleaseMemBlock (WLCHDLR_STA_MSG_POOLID,
                                    (UINT1 *) pWssStaMsgStruct);
                return OSIX_FAILURE;
            }
            break;
        case WSS_WLCHDLR_DATA_TRANSFER_REQ:
            if (WlcHdlrProcessDataTransferReq (&pWlcHdlrMsgStruct->DataReq)
                == OSIX_FAILURE)
            {
                WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "Failed to process Data "
                             "Transfer Request \r\n");
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssMacMsgStruct);
                MemReleaseMemBlock (WLCHDLR_STA_MSG_POOLID,
                                    (UINT1 *) pWssStaMsgStruct);
                return OSIX_FAILURE;
            }
            break;
        case WSS_WLCHDLR_WLAN_PROBE_REQ:
#ifdef WSSWLAN_WANTED
            MEMCPY (&(pWssWlanMsgStruct->unWssWlanMsg.ProbReqMacFrame),
                    &(pWlcHdlrMsgStruct->WssMacMsgStruct.
                      unMacMsg.ProbReqMacFrame),
                    sizeof (tDot11ProbReqMacFrame));
            if (WssIfProcessWssWlanMsg (MsgType, pWssWlanMsgStruct)
                != OSIX_SUCCESS)
            {
                WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "Failed to Process the Probe "
                             "Req Msg Received from WSSMAC \r\n");
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssMacMsgStruct);
                MemReleaseMemBlock (WLCHDLR_STA_MSG_POOLID,
                                    (UINT1 *) pWssStaMsgStruct);
                return OSIX_FAILURE;
            }
#endif
            break;

        case WSS_WLCHDLR_STA_AUTH_MSG:
#ifdef WSSSTA_WANTED
            MEMCPY (&(pWssStaMsgStruct->unAuthMsg.WssMacAuthMacFrame),
                    &(pWlcHdlrMsgStruct->WssMacMsgStruct.unMacMsg.AuthMacFrame),
                    sizeof (tDot11AuthMacFrame));
            if (WssIfProcessWssStaMsg (WSS_MAC_MGMT_AUTH_MSG, pWssStaMsgStruct)
                != OSIX_SUCCESS)
            {
                WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "Failed to Process the AUTH "
                             "Msg Received from WSSMAC \r\n");
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssMacMsgStruct);
                MemReleaseMemBlock (WLCHDLR_STA_MSG_POOLID,
                                    (UINT1 *) pWssStaMsgStruct);
                return OSIX_FAILURE;
            }
#endif
            break;

        case WSS_WLCHDLR_STA_DEAUTH_MSG:
#ifdef WSSSTA_WANTED
            MEMCPY (&(pWssStaMsgStruct->unAuthMsg.WssMacDeauthMacFrame),
                    &(pWlcHdlrMsgStruct->WssMacMsgStruct.
                      unMacMsg.DeauthMacFrame), sizeof (tDot11DeauthMacFrame));
            if (WssIfProcessWssStaMsg (WSS_MAC_MGMT_DEAUTH_MSG,
                                       pWssStaMsgStruct) != OSIX_SUCCESS)
            {
                WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "Failed to Process the "
                             "DEAUTH Msg Received from WSSMAC \r\n");
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssMacMsgStruct);
                MemReleaseMemBlock (WLCHDLR_STA_MSG_POOLID,
                                    (UINT1 *) pWssStaMsgStruct);
                return OSIX_FAILURE;
            }
#endif
            break;

        case WSS_WLCHDLR_STA_ASSOC_REQ:
#ifdef WSSSTA_WANTED
            MEMCPY (&(pWssStaMsgStruct->unAuthMsg.WssMacAssocReqMacFrame),
                    &(pWlcHdlrMsgStruct->WssMacMsgStruct.
                      unMacMsg.AssocReqMacFrame),
                    sizeof (tDot11AssocReqMacFrame));
            if (WssIfProcessWssStaMsg (WSS_MAC_MGMT_ASSOC_REQ,
                                       pWssStaMsgStruct) != OSIX_SUCCESS)
            {
                WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "Failed to Process the "
                             "ASSOC REQ Msg Received from WSSMAC \r\n");
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
                MemReleaseMemBlock (WLCHDLR_STA_MSG_POOLID,
                                    (UINT1 *) pWssStaMsgStruct);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssMacMsgStruct);
                return OSIX_FAILURE;
            }
#endif
            break;

        case WSS_WLCHDLR_STA_REASSOC_REQ:
#ifdef WSSSTA_WANTED
            MEMCPY (&(pWssStaMsgStruct->unAuthMsg.WssMacReassocReqMacFrame),
                    &(pWlcHdlrMsgStruct->WssMacMsgStruct.
                      unMacMsg.ReassocReqMacFrame),
                    sizeof (tDot11ReassocReqMacFrame));
            if (WssIfProcessWssStaMsg (WSS_MAC_MGMT_REASSOC_REQ,
                                       pWssStaMsgStruct) != OSIX_SUCCESS)
            {
                WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "Failed to Process the "
                             "REASSOC REQ Msg Received from WSSMAC \r\n");
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssMacMsgStruct);
                MemReleaseMemBlock (WLCHDLR_STA_MSG_POOLID,
                                    (UINT1 *) pWssStaMsgStruct);
                return OSIX_FAILURE;
            }
#endif
            break;

        case WSS_WLCHDLR_STA_DISASSOC_MSG:
#ifdef WSSSTA_WANTED
            MEMCPY (&(pWssStaMsgStruct->unAuthMsg.WssMacDisassocMacFrame),
                    &(pWlcHdlrMsgStruct->WssMacMsgStruct.
                      unMacMsg.DisassocMacFrame),
                    sizeof (tDot11DisassocMacFrame));
            if (WssIfProcessWssStaMsg (WSS_MAC_MGMT_DISASSOC_MSG,
                                       pWssStaMsgStruct) != OSIX_SUCCESS)
            {
                WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "Failed to Process the "
                             "DISASSOC Msg Received from WSSMAC \r\n");
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssMacMsgStruct);
                MemReleaseMemBlock (WLCHDLR_STA_MSG_POOLID,
                                    (UINT1 *) pWssStaMsgStruct);
                return OSIX_FAILURE;
            }
#endif
            break;

        case WSS_WLCHDLR_STA_ACTION_REQ:
            break;

        case WSS_WLCHDLR_MAC_AUTH_MSG:
#ifdef WSSMAC_WANTED
            MEMCPY (&(pWssMacMsgStruct->unMacMsg.AuthMacFrame),
                    &(pWlcHdlrMsgStruct->WssMacMsgStruct.unMacMsg.AuthMacFrame),
                    sizeof (tDot11AuthMacFrame));
            pWssMacMsgStruct->msgType =
                pWlcHdlrMsgStruct->WssMacMsgStruct.msgType;
            if (WssIfProcessWssMacMsg (WSS_DOT11_MGMT_AUTH_MSG,
                                       pWssMacMsgStruct) != OSIX_SUCCESS)
            {
                WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "Failed to Process the "
                             "AUTH Msg Received from WSSSTA \r\n");
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssMacMsgStruct);
                MemReleaseMemBlock (WLCHDLR_STA_MSG_POOLID,
                                    (UINT1 *) pWssStaMsgStruct);
                return OSIX_FAILURE;
            }
#endif
            break;

        case WSS_WLCHDLR_MAC_DEAUTH_MSG:
#ifdef WSSMAC_WANTED
            MEMCPY (&(pWssMacMsgStruct->unMacMsg.DeauthMacFrame),
                    &(pWlcHdlrMsgStruct->WssMacMsgStruct.
                      unMacMsg.DeauthMacFrame), sizeof (tDot11DeauthMacFrame));
            pWssMacMsgStruct->msgType =
                pWlcHdlrMsgStruct->WssMacMsgStruct.msgType;
            if (WssIfProcessWssMacMsg (WSS_DOT11_MGMT_DEAUTH_MSG,
                                       pWssMacMsgStruct) != OSIX_SUCCESS)
            {
                WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "Failed to Process the "
                             "DEAUTH Msg Received from WSSSTA \r\n");
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssMacMsgStruct);
                MemReleaseMemBlock (WLCHDLR_STA_MSG_POOLID,
                                    (UINT1 *) pWssStaMsgStruct);
                return OSIX_FAILURE;
            }
#endif
            break;

        case WSS_WLCHDLR_MAC_ASSOC_RSP:
#ifdef WSSMAC_WANTED
            MEMCPY (&(pWssMacMsgStruct->unMacMsg.AssocRspMacFrame),
                    &(pWlcHdlrMsgStruct->WssMacMsgStruct.
                      unMacMsg.AssocRspMacFrame),
                    sizeof (tDot11AssocRspMacFrame));
            if (WssIfProcessWssMacMsg (WSS_DOT11_MGMT_ASSOC_RSP,
                                       pWssMacMsgStruct) != OSIX_SUCCESS)
            {
                WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "Failed to Process the "
                             "ASSOC RSP Msg Received from WSSSTA \r\n");
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssMacMsgStruct);
                MemReleaseMemBlock (WLCHDLR_STA_MSG_POOLID,
                                    (UINT1 *) pWssStaMsgStruct);
                return OSIX_FAILURE;
            }
#endif
            break;

        case WSS_WLCHDLR_MAC_REASSOC_RSP:
#ifdef WSSMAC_WANTED
            MEMCPY (&(pWssMacMsgStruct->unMacMsg.ReassocRspMacFrame),
                    &(pWlcHdlrMsgStruct->WssMacMsgStruct.
                      unMacMsg.ReassocRspMacFrame),
                    sizeof (tDot11ReassocRspMacFrame));
            if (WssIfProcessWssMacMsg (WSS_DOT11_MGMT_REASSOC_RSP,
                                       pWssMacMsgStruct) != OSIX_SUCCESS)
            {
                WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "Failed to Process the "
                             "REASSOC RSP Msg Received from WSSSTA \r\n");
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssMacMsgStruct);
                MemReleaseMemBlock (WLCHDLR_STA_MSG_POOLID,
                                    (UINT1 *) pWssStaMsgStruct);
                return OSIX_FAILURE;
            }
#endif
            break;

        case WSS_WLCHDLR_MAC_DISASSOC_MSG:
#ifdef WSSMAC_WANTED
            MEMCPY (&(pWssMacMsgStruct->unMacMsg.DisassocMacFrame),
                    &(pWlcHdlrMsgStruct->WssMacMsgStruct.
                      unMacMsg.DisassocMacFrame),
                    sizeof (tDot11DisassocMacFrame));
            pWssMacMsgStruct->msgType =
                pWlcHdlrMsgStruct->WssMacMsgStruct.msgType;
            if (WssIfProcessWssMacMsg (WSS_DOT11_MGMT_DISASSOC_MSG,
                                       pWssMacMsgStruct) != OSIX_SUCCESS)
            {
                WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "Failed to Process the "
                             "DISASSOC Msg Received from WSSSTA \r\n");
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssMacMsgStruct);
                MemReleaseMemBlock (WLCHDLR_STA_MSG_POOLID,
                                    (UINT1 *) pWssStaMsgStruct);
                return OSIX_FAILURE;
            }
#endif
            break;

        case WSS_WLCHDLR_MAC_ACTION_RSP:
#ifdef WSSMAC_WANTED
            MEMCPY (&(pWssMacMsgStruct->unMacMsg.ActionRspFrame),
                    &(pWlcHdlrMsgStruct->WssMacMsgStruct.
                      unMacMsg.ActionRspFrame), sizeof (tDot11ActionRspFrame));
            if (WssIfProcessWssMacMsg (WSS_DOT11_MGMT_ACTION_MSG,
                                       pWssMacMsgStruct) != OSIX_SUCCESS)
            {
                WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "Failed to Process the "
                             "ACTION RSP Msg Received from WSSSTA \r\n");
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssMacMsgStruct);
                MemReleaseMemBlock (WLCHDLR_STA_MSG_POOLID,
                                    (UINT1 *) pWssStaMsgStruct);
                return OSIX_FAILURE;
            }
#endif
            break;

        case WSS_WLCHDLR_CAPWAP_MAC_MSG:
            MEMSET (&CapwapMsgStruct, 0, sizeof (unCapwapMsgStruct));
            CapwapMsgStruct.CapwapTxPkt.pData =
                pWlcHdlrMsgStruct->WlcHdlrQueueReq.pRcvBuf;
            CapwapMsgStruct.CapwapTxPkt.u2SessId =
                pWlcHdlrMsgStruct->WlcHdlrQueueReq.u2SessId;

            MEMCPY (&CapwapMsgStruct.CapwapTxPkt.BssIdMac,
                    &pWlcHdlrMsgStruct->WlcHdlrQueueReq.BssId,
                    sizeof (tMacAddr));
            CapwapMsgStruct.CapwapTxPkt.u1MacType =
                pWlcHdlrMsgStruct->WlcHdlrQueueReq.u1MacType;

            CapwapMsgStruct.CapwapTxPkt.u4MsgType =
                pWlcHdlrMsgStruct->WlcHdlrQueueReq.u4MsgType;

            if (WssIfProcessCapwapMsg (WSS_CAPWAP_WLC_TX_DATA_PKT,
                                       &CapwapMsgStruct) != OSIX_SUCCESS)
            {
                WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "Failed to Process CAPWAP "
                             "MAC Msg Received from WSSMAC \r\n");
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssMacMsgStruct);
                MemReleaseMemBlock (WLCHDLR_STA_MSG_POOLID,
                                    (UINT1 *) pWssStaMsgStruct);
                return OSIX_FAILURE;
            }
            break;

        case WSS_WLCHDLR_CFA_QUEUE_REQ:
            if (WlcHdlrEnqueCtrlPkts (pWlcHdlrMsgStruct) != OSIX_SUCCESS)
            {
                WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "Failed to Process the "
                             "Queue Message Received from CFA \r\n");
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssMacMsgStruct);
                MemReleaseMemBlock (WLCHDLR_STA_MSG_POOLID,
                                    (UINT1 *) pWssStaMsgStruct);
                return OSIX_FAILURE;
            }
            break;

        case WSS_WLCHDLR_CFA_TX_BUF:
            i4RetVal = WlcHdlrVerifyStaPkts (pWlcHdlrMsgStruct);
            if (i4RetVal == OSIX_FAILURE)
            {
                WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "Failed to Verify the "
                             "packet using WlcHdlrVerifyStaPkts\r\n");
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssMacMsgStruct);
                MemReleaseMemBlock (WLCHDLR_STA_MSG_POOLID,
                                    (UINT1 *) pWssStaMsgStruct);
                WLCHDLR_RELEASE_CRU_BUF (pWlcHdlrMsgStruct->WssMacMsgStruct.
                                         unMacMsg.MacDot11PktBuf.pDot11MacPdu);
                return OSIX_FAILURE;
            }
            else if (i4RetVal == OSIX_SUCCESS)
            {
                if (WlcHdlrEnqueCfaTxPkts (MsgType, pWlcHdlrMsgStruct)
                    != OSIX_SUCCESS)
                {
                    WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                                 "Failed to Process CFA Tx "
                                 "Msg Received from WSSMAC \r\n");
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
                    UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssMacMsgStruct);
                    MemReleaseMemBlock (WLCHDLR_STA_MSG_POOLID,
                                        (UINT1 *) pWssStaMsgStruct);

                    WLCHDLR_RELEASE_CRU_BUF (pWlcHdlrMsgStruct->WssMacMsgStruct.
                                             unMacMsg.MacDot11PktBuf.
                                             pDot11MacPdu);
                    return OSIX_FAILURE;
                }
            }
            else if (i4RetVal == NO_NEED_TO_PROCESS)
            {
                WLCHDLR_RELEASE_CRU_BUF (pWlcHdlrMsgStruct->WssMacMsgStruct.
                                         unMacMsg.MacDot11PktBuf.pDot11MacPdu);

            }
            break;
#ifdef PMF_WANTED
        case WSS_WLCHDLR_MAC_SAQUERY_ACTION_RSP:
#ifdef WSSMAC_WANTED
            MEMCPY (&(pWssMacMsgStruct->unMacMsg.StationQueryActionFrame),
                    &(pWlcHdlrMsgStruct->WssMacMsgStruct.
                      unMacMsg.StationQueryActionFrame),
                    sizeof (tDot11ActionStationQueryFrame));
            if (WssIfProcessWssMacMsg
                (WSS_DOT11_MGMT_SAQUERY_ACTION_RSP,
                 pWssMacMsgStruct) != OSIX_SUCCESS)
            {
                WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "Failed to Process the "
                             "ACTION RSP Msg Received from WSSSTA \r\n");
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssMacMsgStruct);
                MemReleaseMemBlock (WLCHDLR_STA_MSG_POOLID,
                                    (UINT1 *) pWssStaMsgStruct);
                return OSIX_FAILURE;
            }
#endif
            break;
        case WSS_WLCHDLR_MAC_SAQUERY_ACTION_REQ:
#ifdef WSSMAC_WANTED
            MEMCPY (&(pWssMacMsgStruct->unMacMsg.StationQueryActionFrame),
                    &(pWlcHdlrMsgStruct->WssMacMsgStruct.
                      unMacMsg.StationQueryActionFrame),
                    sizeof (tDot11ActionStationQueryFrame));
            if (WssIfProcessWssMacMsg
                (WSS_DOT11_MGMT_SAQUERY_ACTION_REQ,
                 pWssMacMsgStruct) != OSIX_SUCCESS)
            {
                WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "Failed to Process the "
                             "ACTION RSP Msg Received from WSSSTA \r\n");
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssMacMsgStruct);
                MemReleaseMemBlock (WLCHDLR_STA_MSG_POOLID,
                                    (UINT1 *) pWssStaMsgStruct);
                return OSIX_FAILURE;
            }
#endif
            break;
#endif
        default:
            WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "Received unknown op-code \r\n");
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
            UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssMacMsgStruct);
            MemReleaseMemBlock (WLCHDLR_STA_MSG_POOLID,
                                (UINT1 *) pWssStaMsgStruct);
            return OSIX_FAILURE;
    }
    UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssMacMsgStruct);
    MemReleaseMemBlock (WLCHDLR_STA_MSG_POOLID, (UINT1 *) pWssStaMsgStruct);
    WLCHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : WlcHdlrProcessConfigUpdateReq                     */
/*  Description     : This function is handles the Config Update Req.   */
/*  Input(s)        : MsgType - Message Type                            */
/*                    pWlcHdlrMsgStruct - Message Structure             */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS | OSIX_FAILURE                       */
/************************************************************************/
INT4
WlcHdlrProcessConfigUpdateReq (UINT1 MsgType,
                               unWlcHdlrMsgStruct * pWlcHdlrMsgStruct)
{
    UINT1              *pu1TxBuf = NULL;
    UINT4               u4MsgLen = 0;
    UINT1               u1IsNotLinear = OSIX_FALSE;
    unCapwapMsgStruct   CapwapMsgStruct;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tRemoteSessionManager *pSessEntry = NULL;
    tCapwapConfigUpdateReq *pCapUpdteReq = NULL;
    tRadioIfConfigUpdateReq *pRadUpdateReq = NULL;
    tPmConfigUpdateReq *pPmUpdateReq = NULL;
    tClkiwfConfigUpdateReq *pClkUpdateReq = NULL;
#ifdef RFMGMT_WANTED
    tRfMgmtConfigUpdateReq *pRfmConfUpdateReq = NULL;
#endif
    tConfigUpdateReq    ConfigUpdateReq;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT2               wtpInternalId = 0;
    tWssIfPMDB          WssIfPMDB;

    WLCHDLR_FN_ENTRY ();
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&ConfigUpdateReq, 0, sizeof (tConfigUpdateReq));
    MEMSET (&CapwapMsgStruct, 0, sizeof (unCapwapMsgStruct));
    MEMSET (&WssIfPMDB, 0, sizeof (tWssIfPMDB));

    switch (MsgType)
    {
        case WSS_WLCHDLR_CAPWAP_CONF_UPDATE_REQ:
            pCapUpdteReq = &pWlcHdlrMsgStruct->CapwapConfigUpdateReq;
            pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
                pCapUpdteReq->u2InternalId;
            wtpInternalId = pCapUpdteReq->u2InternalId;
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_RSM_FROM_INDEX,
                                         pWssIfCapwapDB) != OSIX_SUCCESS)
            {
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
            pSessEntry = pWssIfCapwapDB->pSessEntry;
            if (pSessEntry == NULL)
            {
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return NO_AP_PRESENT;
            }
            pSessEntry->u1ModuleId = WSS_CAPWAP_MODULE;

            MEMCPY (&(ConfigUpdateReq.CapwapConfigUpdateReq),
                    &(pWlcHdlrMsgStruct->CapwapConfigUpdateReq),
                    sizeof (tCapwapConfigUpdateReq));

            if (pWlcHdlrMsgStruct->CapwapConfigUpdateReq.acNameWithPrio[0].
                isOptional == OSIX_TRUE)
            {
                u4MsgLen += (UINT4) (pWlcHdlrMsgStruct->CapwapConfigUpdateReq.
                                     acNameWithPrio[0].u2MsgEleLen +
                                     CAPWAP_MSG_ELEM_TYPE_LEN);
            }
            if (pWlcHdlrMsgStruct->CapwapConfigUpdateReq.acNameWithPrio[1].
                isOptional == OSIX_TRUE)
            {
                u4MsgLen += (UINT4) (pWlcHdlrMsgStruct->CapwapConfigUpdateReq.
                                     acNameWithPrio[1].u2MsgEleLen +
                                     CAPWAP_MSG_ELEM_TYPE_LEN);
            }
            if (pWlcHdlrMsgStruct->CapwapConfigUpdateReq.acNameWithPrio[2].
                isOptional == OSIX_TRUE)
            {
                u4MsgLen += (UINT4) (pWlcHdlrMsgStruct->CapwapConfigUpdateReq.
                                     acNameWithPrio[2].u2MsgEleLen +
                                     CAPWAP_MSG_ELEM_TYPE_LEN);
            }
            if (pWlcHdlrMsgStruct->CapwapConfigUpdateReq.addMacEntry.isOptional
                == OSIX_TRUE)
            {
                u4MsgLen += (UINT4) (pWlcHdlrMsgStruct->CapwapConfigUpdateReq.
                                     addMacEntry.u2MsgEleLen +
                                     CAPWAP_MSG_ELEM_TYPE_LEN);
            }
            if (pWlcHdlrMsgStruct->CapwapConfigUpdateReq.capwapTimer.isOptional
                == OSIX_TRUE)
            {
                u4MsgLen += (UINT4) (pWlcHdlrMsgStruct->CapwapConfigUpdateReq.
                                     capwapTimer.u2MsgEleLen +
                                     CAPWAP_MSG_ELEM_TYPE_LEN);
            }
            if (pWlcHdlrMsgStruct->CapwapConfigUpdateReq.decryErrPeriod.
                isOptional == OSIX_TRUE)
            {
                u4MsgLen += (UINT4) (pWlcHdlrMsgStruct->CapwapConfigUpdateReq.
                                     decryErrPeriod.u2MsgEleLen +
                                     CAPWAP_MSG_ELEM_TYPE_LEN);
            }
            if (pWlcHdlrMsgStruct->CapwapConfigUpdateReq.delMacEntry.isOptional
                == OSIX_TRUE)
            {
                u4MsgLen += (UINT4) (pWlcHdlrMsgStruct->CapwapConfigUpdateReq.
                                     delMacEntry.u2MsgEleLen +
                                     CAPWAP_MSG_ELEM_TYPE_LEN);
            }
            if (pWlcHdlrMsgStruct->CapwapConfigUpdateReq.idleTimeout.isOptional
                == OSIX_TRUE)
            {
                u4MsgLen += (UINT4) (pWlcHdlrMsgStruct->CapwapConfigUpdateReq.
                                     idleTimeout.u2MsgEleLen +
                                     CAPWAP_MSG_ELEM_TYPE_LEN);
            }
            if (pWlcHdlrMsgStruct->CapwapConfigUpdateReq.wtpLocation.isOptional
                == OSIX_TRUE)
            {
                u4MsgLen += (UINT4) (pWlcHdlrMsgStruct->CapwapConfigUpdateReq.
                                     wtpLocation.u2MsgEleLen +
                                     CAPWAP_MSG_ELEM_TYPE_LEN);
            }
            if (pWlcHdlrMsgStruct->CapwapConfigUpdateReq.statsTimer.isOptional
                == OSIX_TRUE)
            {
                u4MsgLen +=
                    (UINT4) (pWlcHdlrMsgStruct->CapwapConfigUpdateReq.
                             statsTimer.u2MsgEleLen + CAPWAP_MSG_ELEM_TYPE_LEN);
            }
            if (pWlcHdlrMsgStruct->CapwapConfigUpdateReq.wtpFallback.isOptional
                == OSIX_TRUE)
            {
                u4MsgLen += (UINT4) (pWlcHdlrMsgStruct->CapwapConfigUpdateReq.
                                     wtpFallback.u2MsgEleLen +
                                     CAPWAP_MSG_ELEM_TYPE_LEN);
            }
            if (pWlcHdlrMsgStruct->CapwapConfigUpdateReq.wtpName.isOptional
                == OSIX_TRUE)
            {
                u4MsgLen +=
                    (UINT4) (pWlcHdlrMsgStruct->CapwapConfigUpdateReq.wtpName.
                             u2MsgEleLen + CAPWAP_MSG_ELEM_TYPE_LEN);
            }
            if (pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.isOptional
                == OSIX_TRUE)
            {
                u4MsgLen +=
                    (UINT4) (pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.
                             u2MsgEleLen + CAPWAP_MSG_ELEM_TYPE_LEN);
            }
            if (pWlcHdlrMsgStruct->CapwapConfigUpdateReq.wtpStaticIpAddr.
                isOptional == OSIX_TRUE)
            {
                u4MsgLen += (UINT4) (pWlcHdlrMsgStruct->CapwapConfigUpdateReq.
                                     wtpStaticIpAddr.u2MsgEleLen +
                                     CAPWAP_MSG_ELEM_TYPE_LEN);
            }
            break;
        case WSS_WLCHDLR_CLKIWF_CONF_UPDATE_REQ:
        {
            pClkUpdateReq = &(pWlcHdlrMsgStruct->ClkiwfConfigUpdateReq);
            pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
                (UINT2) pClkUpdateReq->u4SessId;
            wtpInternalId = (UINT2) pClkUpdateReq->u4SessId;
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_RSM_FROM_INDEX,
                                         pWssIfCapwapDB) != OSIX_SUCCESS)
            {
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
            pSessEntry = pWssIfCapwapDB->pSessEntry;
            if (pSessEntry == NULL)
            {
                WLCHDLR_TRC1 (WLCHDLR_FAILURE_TRC,
                              "WSS_WLCHDLR_CLKIWF_CONF_UPDATE_REQ:: pSessEntry "
                              "is NULL for WTP Index %d\r\n", wtpInternalId);
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
            MEMCPY (&ConfigUpdateReq.ClkiwfConfigUpdateReq, pClkUpdateReq,
                    sizeof (tClkiwfConfigUpdateReq));

            if (pWlcHdlrMsgStruct->ClkiwfConfigUpdateReq.acTimestamp.
                isOptional == OSIX_TRUE)
            {
                u4MsgLen += (UINT4) (pWlcHdlrMsgStruct->ClkiwfConfigUpdateReq.
                                     acTimestamp.u2MsgEleLen +
                                     CAPWAP_MSG_ELEM_TYPE_LEN);
            }
        }
            break;
        case WSS_WLCHDLR_RADIO_CONF_UPDATE_REQ:
        {
            pRadUpdateReq = &(pWlcHdlrMsgStruct->RadioIfConfigUpdateReq);
            pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
                pRadUpdateReq->u2WtpInternalId;
            wtpInternalId = pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId;
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_RSM_FROM_INDEX,
                                         pWssIfCapwapDB) == OSIX_FAILURE)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "Failed to Get Rsm from CAPWAP DB \r\n");
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
            pSessEntry = pWssIfCapwapDB->pSessEntry;
            if (pSessEntry == NULL)
            {
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return NO_AP_PRESENT;
            }
            MEMCPY (&ConfigUpdateReq.RadioIfConfigUpdateReq, pRadUpdateReq,
                    sizeof (tRadioIfConfigUpdateReq));
            if (pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
                RadioIfMacOperation.isPresent == OSIX_TRUE)
            {
                u4MsgLen += (UINT4) (pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
                                     RadioIfMacOperation.u2MessageLength +
                                     CAPWAP_MSG_ELEM_TYPE_LEN);
            }
            if (pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
                RadioIfAdminStatus.isPresent == OSIX_TRUE)
            {
                u4MsgLen += (UINT4) (pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
                                     RadioIfAdminStatus.u2MessageLength +
                                     CAPWAP_MSG_ELEM_TYPE_LEN);
            }
            if (pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfOFDMPhy.
                isPresent == OSIX_TRUE)
            {
                u4MsgLen += (UINT4) (pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
                                     RadioIfOFDMPhy.u2MessageLength +
                                     CAPWAP_MSG_ELEM_TYPE_LEN);
            }
            if (pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfDSSSPhy.
                isPresent == OSIX_TRUE)
            {
                u4MsgLen += (UINT4) (pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
                                     RadioIfDSSSPhy.u2MessageLength +
                                     CAPWAP_MSG_ELEM_TYPE_LEN);
            }
            if (pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfTxPower.
                isPresent == OSIX_TRUE)
            {
                u4MsgLen += (UINT4) (pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
                                     RadioIfTxPower.u2MessageLength +
                                     CAPWAP_MSG_ELEM_TYPE_LEN);
            }
            if (pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfQos.
                isPresent == OSIX_TRUE)
            {
                u4MsgLen += (UINT4) (pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
                                     RadioIfQos.u2MessageLength +
                                     CAPWAP_MSG_ELEM_TYPE_LEN);
            }
            if (pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfInfo.
                isPresent == OSIX_TRUE)
            {
                u4MsgLen += (UINT4) (pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
                                     RadioIfInfo.u2MessageLength +
                                     CAPWAP_MSG_ELEM_TYPE_LEN);
            }
            if (pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfConfig.
                isPresent == OSIX_TRUE)
            {
                u4MsgLen += (UINT4) (pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
                                     RadioIfConfig.u2MessageLength +
                                     CAPWAP_MSG_ELEM_TYPE_LEN);
            }
            if (pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfAntenna.
                isPresent == OSIX_TRUE)
            {
                u4MsgLen += (UINT4) (pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
                                     RadioIfAntenna.u2MessageLength +
                                     CAPWAP_MSG_ELEM_TYPE_LEN);
            }
            if (pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfRateSet.
                isPresent == OSIX_TRUE)
            {
                u4MsgLen += (UINT4) (pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
                                     RadioIfRateSet.u2MessageLength +
                                     CAPWAP_MSG_ELEM_TYPE_LEN);
            }
            if (pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
                RadioIfInfoElem.isPresent == OSIX_TRUE)
            {
                u4MsgLen += (UINT4) (pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
                                     RadioIfInfoElem.u2MessageLength +
                                     CAPWAP_MSG_ELEM_TYPE_LEN);
            }
            if (pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfDot11nParam.
                isPresent == OSIX_TRUE)
            {
                u4MsgLen += (UINT4) (pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
                                     RadioIfDot11nParam.u2MessageLength +
                                     CAPWAP_MSG_ELEM_TYPE_LEN +
                                     CAPWAP_MSG_ELEM_TYPE_LEN);
            }
            if (pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
                RadioIfMultiDomainCap.isPresent == OSIX_TRUE)
            {
                u4MsgLen += (UINT4) (pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
                                     RadioIfMultiDomainCap.u2MessageLength +
                                     CAPWAP_MSG_ELEM_TYPE_LEN);
            }
            if (pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfDot11nCfg.
                isPresent == OSIX_TRUE)
            {
                u4MsgLen += (UINT4) (pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
                                     RadioIfDot11nCfg.u2MessageLength +
                                     CAPWAP_MSG_ELEM_TYPE_LEN +
                                     CAPWAP_MSG_ELEM_TYPE_LEN);
            }

            break;
        }
        case WSS_WLCHDLR_PM_CONF_UPDATE_REQ:
        {
            MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
            pPmUpdateReq = &pWlcHdlrMsgStruct->PmConfigUpdateReq;

            pWssIfCapwapDB->CapwapIsGetAllDB.bWtpProfileId = OSIX_TRUE;
            wtpInternalId = (UINT2) pPmUpdateReq->u4SessId;
            pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = wtpInternalId;
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_RSM_FROM_INDEX,
                                         pWssIfCapwapDB) != OSIX_SUCCESS)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           " WssIfProcessCapwapDBMsg : Failed \r\n");
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
            pSessEntry = pWssIfCapwapDB->pSessEntry;
            MEMCPY (&ConfigUpdateReq.PmConfigUpdateReq,
                    pPmUpdateReq, sizeof (tPmConfigUpdateReq));
            if (pSessEntry == NULL)
            {
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return NO_AP_PRESENT;
            }
        }
            break;
#ifdef RFMGMT_WANTED
        case WSS_WLCHDLR_RFMGMT_CONF_UPDATE_REQ:
        {
            pRfmConfUpdateReq = &(pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq);
            pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
                pRfmConfUpdateReq->u2WtpInternalId;
            wtpInternalId = pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId;
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_RSM_FROM_INDEX,
                                         pWssIfCapwapDB) == OSIX_FAILURE)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "Failed to Get Rsm from CAPWAP DB \r\n");
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
            pSessEntry = pWssIfCapwapDB->pSessEntry;
            if (pSessEntry == NULL)
            {
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return NO_AP_PRESENT;
            }
            MEMCPY (&ConfigUpdateReq.RfMgmtConfigUpdateReq,
                    pRfmConfUpdateReq, sizeof (tRfMgmtConfigUpdateReq));

            if (pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.
                NeighApTableConfig.isOptional == OSIX_TRUE)
            {
                u4MsgLen += (UINT4) (pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.
                                     NeighApTableConfig.u2MsgEleLen +
                                     CAPWAP_MSG_ELEM_TYPE_LEN +
                                     CAPWAP_MSG_ELEM_TYPE_LEN);
            }
            if (pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.
                ClientTableConfig.isOptional == OSIX_TRUE)
            {
                u4MsgLen += (UINT4) (pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.
                                     ClientTableConfig.u2MsgEleLen +
                                     CAPWAP_MSG_ELEM_TYPE_LEN +
                                     CAPWAP_MSG_ELEM_TYPE_LEN);
            }
            if (pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.
                ChSwitchStatusTable.isOptional == OSIX_TRUE)
            {
                u4MsgLen += (UINT4) (pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.
                                     ChSwitchStatusTable.u2MsgEleLen +
                                     CAPWAP_MSG_ELEM_TYPE_LEN +
                                     CAPWAP_MSG_ELEM_TYPE_LEN);
            }
            if (pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.
                TpcSpectMgmtTable.isOptional == OSIX_TRUE)
            {
                u4MsgLen += (UINT4) (pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.
                                     TpcSpectMgmtTable.u2MsgEleLen +
                                     CAPWAP_MSG_ELEM_TYPE_LEN +
                                     CAPWAP_MSG_ELEM_TYPE_LEN);
            }
            if (pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.
                DfsParamsTable.isOptional == OSIX_TRUE)
            {
                u4MsgLen += (UINT4) (pWlcHdlrMsgStruct->
                                     RfMgmtConfigUpdateReq.DfsParamsTable.
                                     u2MsgEleLen + CAPWAP_MSG_ELEM_TYPE_LEN +
                                     CAPWAP_MSG_ELEM_TYPE_LEN);
            }

#ifdef ROGUEAP_WANTED
            if (pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.RogueMgmt.isOptional ==
                OSIX_TRUE)
            {
                u4MsgLen += (UINT4) (pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.
                                     RogueMgmt.u2MsgEleLen +
                                     CAPWAP_MSG_ELEM_TYPE_LEN +
                                     CAPWAP_MSG_ELEM_TYPE_LEN);
            }
#endif

            break;
        }
#endif
        default:
            WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "Received unknown event \r\n");
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
    }

    if (pSessEntry->eCurrentState != CAPWAP_RUN)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "CAPWAP Session Entry is Not in "
                     "Run State, return FAILURE \r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return NO_AP_PRESENT;
    }

    /* CAPWAP Header updated by CAPWAP
       module before assembling the packet */

    /* update Control Header */
    ConfigUpdateReq.u2CapwapMsgElemenLen =
        (UINT2) (u4MsgLen + CAPWAP_MSG_ELEM_PLUS_SEQ_LEN);
    ConfigUpdateReq.u1NumMsgBlocks = 0;    /* flags */
    ConfigUpdateReq.u1SeqNum = (UINT1) (pSessEntry->lastTransmitedSeqNum + 1);

    /* Allocate memory for packet linear buffer */
    pBuf = WLCHDLR_ALLOCATE_CRU_BUF (CAPWAP_MAX_PKT_LEN, 0);
    if (pBuf == NULL)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "Memory Allocation Failed \r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    pu1TxBuf = WLCHDLR_BUF_IF_LINEAR (pBuf, 0, CAPWAP_MAX_PKT_LEN);
    if (pu1TxBuf == NULL)
    {
        WLCHDLR_PKTBUF_ALLOC_MEM_BLOCK (pu1TxBuf);
        u1IsNotLinear = OSIX_TRUE;
    }

    if (pWlcHdlrMsgStruct->CapwapConfigUpdateReq.imageId.isOptional ==
        OSIX_TRUE)
    {
        MEMCPY (&ConfigUpdateReq.CapwapConfigUpdateReq.imageId.data,
                &(pWlcHdlrMsgStruct->CapwapConfigUpdateReq.imageId.data),
                pWlcHdlrMsgStruct->CapwapConfigUpdateReq.imageId.u2MsgEleLen);
        ConfigUpdateReq.CapwapConfigUpdateReq.imageId.u2MsgEleLen
            = pWlcHdlrMsgStruct->CapwapConfigUpdateReq.imageId.u2MsgEleLen;

        if (MsgType == WSS_WLCHDLR_CAPWAP_CONF_UPDATE_REQ)
        {
            u4MsgLen = 0;
            /* update Manadatory Messege elements */
            if (CapwapGetWLCImageId
                (&ConfigUpdateReq.CapwapConfigUpdateReq.imageId,
                 &u4MsgLen) == OSIX_FAILURE)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Failed to get Mandatory Message Elements \n");
                WLCHDLR_RELEASE_CRU_BUF (pBuf);
                if (u1IsNotLinear == OSIX_TRUE)
                {
                    WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pu1TxBuf);
                }
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }

            ConfigUpdateReq.u2CapwapMsgElemenLen =
                (UINT2) (ConfigUpdateReq.u2CapwapMsgElemenLen + u4MsgLen);
            MEMSET (&pSessEntry->ImageId, 0, sizeof (tImageId));
            MEMCPY (&pSessEntry->ImageId,
                    &ConfigUpdateReq.CapwapConfigUpdateReq.imageId,
                    sizeof (tImageId));
        }
    }

    if (MsgType == WSS_WLCHDLR_PM_CONF_UPDATE_REQ)
    {
        /* update Manadatory Messege elements */
        if (CapwapGetStatisticsTimer
            (&ConfigUpdateReq.PmConfigUpdateReq.statsTimer,
             &u4MsgLen) == OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to get Mandatory Message Elements \n");
            WLCHDLR_RELEASE_CRU_BUF (pBuf);
            if (u1IsNotLinear == OSIX_TRUE)
            {
                WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pu1TxBuf);
            }
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }

        ConfigUpdateReq.u2CapwapMsgElemenLen =
            (UINT2) (ConfigUpdateReq.u2CapwapMsgElemenLen + u4MsgLen);
    }

    CapwapMsgStruct.CapAssembleConfUpdateReq.pData = pu1TxBuf;
    CapwapMsgStruct.CapAssembleConfUpdateReq.pCapwapCtrlMsg = &ConfigUpdateReq;
    CapwapMsgStruct.CapAssembleConfUpdateReq.u2MsgLen =
        (UINT2) (ConfigUpdateReq.u2CapwapMsgElemenLen + CAPWAP_CMN_HDR_LEN);
    if (WssIfProcessCapwapMsg (WSS_CAPWAP_ASSEMBLE_CONF_UPDATE_REQ,
                               &CapwapMsgStruct) == OSIX_FAILURE)
    {
        WLCHDLR_RELEASE_CRU_BUF (pBuf);
        if (u1IsNotLinear == OSIX_TRUE)
        {
            WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pu1TxBuf);
        }
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "Failed to Assemble the packet \r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    if (u1IsNotLinear == OSIX_TRUE)
    {
        /* If the CRU buffer memory is not linear */
        WLCHDLR_COPY_TO_BUF (pBuf, pu1TxBuf, 0, CAPWAP_MAX_PKT_LEN);
        WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pu1TxBuf);
    }

    CapwapMsgStruct.CapwapTxPkt.pData = pBuf;
    CapwapMsgStruct.CapwapTxPkt.u4pktLen =
        (UINT4) (ConfigUpdateReq.u2CapwapMsgElemenLen + CAPWAP_CMN_HDR_LEN);
    CapwapMsgStruct.CapwapTxPkt.u4IpAddr = pSessEntry->remoteIpAddr.u4_addr[0];
    CapwapMsgStruct.CapwapTxPkt.u4DestPort = pSessEntry->u4RemoteCtrlPort;
    if (WssIfProcessCapwapMsg (WSS_CAPWAP_TX_CTRL_PKT, &CapwapMsgStruct)
        == OSIX_FAILURE)
    {
        WLCHDLR_RELEASE_CRU_BUF (pBuf);
        WLCHDLR_TRC (ALL_FAILURE_TRC, "Failed to Transmit the packet \r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    MEMSET (&WssIfPMDB, 0, sizeof (tWssIfPMDB));
    WssIfPMDB.u2ProfileId = pSessEntry->u2IntProfileId;
    WssIfPMDB.WssPmWLCCapwapStatsSetFlag.bConfigUpdateStats = OSIX_TRUE;
    WssIfPMDB.WssPmWLCCapwapStatsSetFlag.bRunUpdateReqTransmitted = OSIX_TRUE;

    if (WssIfProcessPMDBMsg (WSS_PM_WLC_CAPWAP_RUN_STATS_SET,
                             &WssIfPMDB) != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to update PM db\r\n");
    }

    /* Before storing the packet check any packet is already stored in the
     * buffer. If yes then release the buffer and stop the timer. */
    if (pSessEntry->lastTransmittedPkt != NULL)
    {
        if (WlcHdlrTmrStop (pSessEntry, WLCHDLR_RETRANSMIT_INTERVAL_TMR)
            == OSIX_FAILURE)
        {
            WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                         "Failed to stop the retransmit timer \r\n");
        }
        WlchdlrFreeTransmittedPacket (pSessEntry);
    }

    /* store the packet buffer pointer in
     * the remote session DB copy the pointer */
    pSessEntry->lastTransmittedPkt = pBuf;
    pSessEntry->lastTransmittedPktLen =
        (UINT4) (ConfigUpdateReq.u2CapwapMsgElemenLen + CAPWAP_CMN_HDR_LEN);

    /* Store the tx STA pkt in a buffer */
    WLCHDLR_COPY_FROM_BUF (pBuf, pConfBuf, 0,
                           (UINT4) (ConfigUpdateReq.u2CapwapMsgElemenLen +
                                    CAPWAP_CMN_HDR_LEN));
    u4ConfBufLen = pSessEntry->lastTransmittedPktLen;

    /*Start the Retransmit timer */
    WlcHdlrTmrStart (pSessEntry, WLCHDLR_RETRANSMIT_INTERVAL_TMR,
                     MAX_RETRANSMIT_INTERVAL_TIMEOUT);

    /* update the seq number */
    pSessEntry->lastTransmitedSeqNum++;

    /* update the retransmit count */
    pSessEntry->u1RetransmitCount = 0;

    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    WLCHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : WlcHdlrProcessWlanConfigRequest                   */
/*  Description     : This function is handles the WLAN Config Req.     */
/*  Input(s)        : pWlanConfReq - WLAN Conf Req Struct               */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS | OSIX_FAILURE                       */
/************************************************************************/
INT4
WlcHdlrProcessWlanConfigRequest (tWssWlanConfigReq * pWlanConfReq)
{
    UINT1              *pu1TxBuf = NULL;
    UINT4               u4MsgLen = 0;
    UINT1               u1IsNotLinear = OSIX_FALSE;
    UINT1               u1Index = 0;
    tRemoteSessionManager *pSessEntry = NULL;
    unCapwapMsgStruct   CapwapMsgStruct;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tWssIfPMDB          WssIfPMDB;
    WLCHDLR_FN_ENTRY ();
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&CapwapMsgStruct, 0, sizeof (unCapwapMsgStruct));
    MEMSET (&WssIfPMDB, 0, sizeof (tWssIfPMDB));

    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = pWlanConfReq->u2SessId;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_RSM_FROM_INDEX,
                                 pWssIfCapwapDB) == OSIX_FAILURE)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                     "Failed to get RSM from WTP Internal Id \r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = pWlanConfReq->u2SessId;
    pWssIfCapwapDB->CapwapIsGetAllDB.bProfileName = OSIX_TRUE;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                 pWssIfCapwapDB) == OSIX_FAILURE)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "Failed to Get RSM from "
                     "CAPWAP DB, return Failure \r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    pSessEntry = pWssIfCapwapDB->pSessEntry;
    if (pSessEntry == NULL)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return NO_AP_PRESENT;
    }
    if (pSessEntry->eCurrentState != CAPWAP_RUN)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "CAPWAP Session Entry is Not in "
                     "Run State \r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return NO_AP_PRESENT;
    }

    /* CAPWAP Header updated by CAPWAP 
       module before assembling the packet */

    /* update Control Header */
    if (pWlanConfReq->u1WlanOption == WSSWLAN_ADD_REQ)
    {
        u4MsgLen +=
            (UINT4) (pWlanConfReq->unWlanConfReq.WssWlanAddReq.u2MessageLength +
                     CAPWAP_MSG_ELEM_TYPE_LEN);
    }
    if (pWlanConfReq->u1WlanOption == WSSWLAN_DEL_REQ)
    {
        u4MsgLen += (UINT4) (pWlanConfReq->unWlanConfReq.WssWlanDeleteReq.
                             u2MessageLength + CAPWAP_MSG_ELEM_TYPE_LEN);
    }
    if (pWlanConfReq->u1WlanOption == WSSWLAN_UPDATE_REQ)
    {
        u4MsgLen += (UINT4) (pWlanConfReq->unWlanConfReq.WssWlanUpdateReq.
                             u2MessageLength + CAPWAP_MSG_ELEM_TYPE_LEN);
        CAPWAP_TRC2 (CAPWAP_STATION_TRC, "Send Update WLAN to %s Radio %d\n",
                     pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                     pWlanConfReq->unWlanConfReq.WssWlanUpdateReq.u1RadioId);
        SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4WlcHdlrSysLogId,
                      "Send Update WLANto %s Radio %d !!!",
                      pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                      pWlanConfReq->unWlanConfReq.WssWlanUpdateReq.u1RadioId));
    }
    for (u1Index = 0; u1Index < VEND_CONF_MAX; u1Index++)
    {
        if (pWlanConfReq->vendSpec[u1Index].isOptional == OSIX_TRUE)
        {
            u4MsgLen += (UINT4) (pWlanConfReq->vendSpec[u1Index].u2MsgEleLen +
                                 CAPWAP_MSG_ELEM_TYPE_LEN);
        }
    }
#ifdef WPS_WANTED
    if (pWlanConfReq->RadioIfInfoElement.isPresent == OSIX_TRUE)
    {
        u4MsgLen +=
            (UINT4) (pWlanConfReq->RadioIfInfoElement.u2MessageLength +
                     CAPWAP_MSG_ELEM_TYPE_LEN);
    }
#endif
    pWlanConfReq->u2CapwapMsgElemenLen = (UINT2) (u4MsgLen
                                                  +
                                                  CAPWAP_MSG_ELEM_PLUS_SEQ_LEN);
    pWlanConfReq->u1NumMsgBlocks = 0;    /* flags */
    pWlanConfReq->u1SeqNum = (UINT1) (pSessEntry->lastTransmitedSeqNum + 1);

    pBuf = WLCHDLR_ALLOCATE_CRU_BUF (WLCHDLR_MAX_PKT_LEN, 0);
    if (pBuf == NULL)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "Memory Allocation Failed \r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    pu1TxBuf = WLCHDLR_BUF_IF_LINEAR (pBuf, 0, WLCHDLR_MAX_PKT_LEN);
    if (pu1TxBuf == NULL)
    {
        WLCHDLR_PKTBUF_ALLOC_MEM_BLOCK (pu1TxBuf);
        u1IsNotLinear = OSIX_TRUE;
    }

    CapwapMsgStruct.CapAssembleWlanUpdateReq.pData = pu1TxBuf;
    CapwapMsgStruct.CapAssembleWlanUpdateReq.pCapwapCtrlMsg = pWlanConfReq;
    CapwapMsgStruct.CapAssembleWlanUpdateReq.u2MsgLen =
        (UINT2) (pWlanConfReq->u2CapwapMsgElemenLen + CAPWAP_CMN_HDR_LEN);
    if (WssIfProcessCapwapMsg (WSS_CAPWAP_ASSEMBLE_WLAN_CONF_REQ,
                               &CapwapMsgStruct) == OSIX_FAILURE)
    {
        WLCHDLR_RELEASE_CRU_BUF (pBuf);
        if (u1IsNotLinear == OSIX_TRUE)
        {
            WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pu1TxBuf);
        }
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "Failed to Assemble the packet \r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    if (u1IsNotLinear == OSIX_TRUE)
    {
        /* If the CRU buffer memory is not linear */
        WLCHDLR_COPY_TO_BUF (pBuf, pu1TxBuf, 0,
                             (UINT4) (pWlanConfReq->u2CapwapMsgElemenLen +
                                      CAPWAP_CMN_HDR_LEN));
        WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pu1TxBuf);
    }

    CapwapMsgStruct.CapwapTxPkt.pData = pBuf;
    CapwapMsgStruct.CapwapTxPkt.u4pktLen =
        (UINT4) (pWlanConfReq->u2CapwapMsgElemenLen + CAPWAP_CMN_HDR_LEN);
    CapwapMsgStruct.CapwapTxPkt.u4IpAddr = pSessEntry->remoteIpAddr.u4_addr[0];
    CapwapMsgStruct.CapwapTxPkt.u4DestPort = pSessEntry->u4RemoteCtrlPort;
    if (WssIfProcessCapwapMsg (WSS_CAPWAP_TX_CTRL_PKT, &CapwapMsgStruct)
        == OSIX_FAILURE)
    {
        WLCHDLR_RELEASE_CRU_BUF (pBuf);
        WLCHDLR_TRC (ALL_FAILURE_TRC, "Failed to Transmit the packet \r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    WssIfPMDB.u2ProfileId = pSessEntry->u2IntProfileId;
    WssIfPMDB.WssPmWLCCapwapStatsSetFlag.bConfigUpdateStats = OSIX_TRUE;
    WssIfPMDB.WssPmWLCCapwapStatsSetFlag.bRunUpdateReqTransmitted = OSIX_TRUE;

    if (WssIfProcessPMDBMsg (WSS_PM_WLC_CAPWAP_RUN_STATS_SET,
                             &WssIfPMDB) != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to update PM db\r\n");
    }

    /* Before storing the packet check any packet is already stored in the
     * buffer. If yes then release the buffer and stop the timer. */
    if (pSessEntry->lastTransmittedPkt != NULL)
    {
        if (WlcHdlrTmrStop (pSessEntry, WLCHDLR_RETRANSMIT_INTERVAL_TMR)
            == OSIX_FAILURE)
        {
            WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                         "Failed to stop the retransmit timer \r\n");
        }
        WlchdlrFreeTransmittedPacket (pSessEntry);
    }

    /* store the packet buffer pointer in
     * the remote session DB copy the pointer */
    pSessEntry->lastTransmittedPkt = pBuf;
    pSessEntry->lastTransmittedPktLen =
        (UINT4) (pWlanConfReq->u2CapwapMsgElemenLen + CAPWAP_CMN_HDR_LEN);
    /*Start the Retransmit timer */
    WlcHdlrTmrStart (pSessEntry, WLCHDLR_RETRANSMIT_INTERVAL_TMR,
                     MAX_RETRANSMIT_INTERVAL_TIMEOUT);

    /* update the seq number */
    pSessEntry->lastTransmitedSeqNum++;

    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    WLCHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : WlcHdlrProcessStationConfigReq                    */
/*  Description     : This function is handles the Station Config Req.  */
/*  Input(s)        : pStationConfReq - Pointer to StationConfReq Struct*/
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS | OSIX_FAILURE                       */
/************************************************************************/
INT4
WlcHdlrProcessStationConfigReq (tStationConfReq * pStationConfReq)
{
    UINT1              *pu1TxBuf = NULL;
    UINT4               u4MsgLen = 0;
    tRemoteSessionManager *pSessEntry = NULL;
    unCapwapMsgStruct   CapwapMsgStruct;
    UINT1               u1IsNotLinear = OSIX_FALSE;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tWssIfPMDB          WssIfPMDB;
    tWlcHdlrStaConfigDB *pWlcHdlrStaConfigDB = NULL;

    WLCHDLR_FN_ENTRY ();
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (&CapwapMsgStruct, 0, sizeof (unCapwapMsgStruct));
    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    MEMSET (&WssIfPMDB, 0, sizeof (tWssIfPMDB));
    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
        pStationConfReq->wssStaConfig.u2SessId;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_RSM_FROM_INDEX,
                                 pWssIfCapwapDB) == OSIX_FAILURE)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "Failed to Get RSM from "
                     "CAPWAP DB, return Failure \r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
        pStationConfReq->wssStaConfig.u2SessId;
    pWssIfCapwapDB->CapwapIsGetAllDB.bProfileName = OSIX_TRUE;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                 pWssIfCapwapDB) == OSIX_FAILURE)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "Failed to Get RSM from "
                     "CAPWAP DB, return Failure \r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    pSessEntry = pWssIfCapwapDB->pSessEntry;
    if (pSessEntry == NULL)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "CAPWAP Session Entry is NULL,"
                     " return FAILURE \r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    if (pSessEntry->eCurrentState != CAPWAP_RUN)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "CAPWAP Session Entry is Not in Run"
                     " State, retrun FAILURE \r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    /* CAPWAP Header updated by CAPWAP
       module before assembling the packet */

    if (pStationConfReq->wssStaConfig.AddSta.isPresent == OSIX_TRUE)
    {
        u4MsgLen +=
            (UINT4) (pStationConfReq->wssStaConfig.AddSta.u2MessageLength +
                     CAPWAP_MSG_ELEM_TYPE_LEN);
        CAPWAP_TRC2 (CAPWAP_STATION_TRC, "ADD STA: %s Radio %d\n",
                     pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                     pStationConfReq->wssStaConfig.AddSta.u1RadioId);
        SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4WlcHdlrSysLogId,
                      "ADD STA: %s Radio %d !!!",
                      pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                      pStationConfReq->wssStaConfig.AddSta.u1RadioId));
    }
    if (pStationConfReq->wssStaConfig.DelSta.isPresent == OSIX_TRUE)
    {
        u4MsgLen +=
            (UINT4) (pStationConfReq->wssStaConfig.DelSta.u2MessageLength +
                     CAPWAP_MSG_ELEM_TYPE_LEN);
        CAPWAP_TRC2 (CAPWAP_STATION_TRC, "DELETE STA: %s Radio %d\n",
                     pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                     pStationConfReq->wssStaConfig.DelSta.u1RadioId);
        SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4WlcHdlrSysLogId,
                      "DELETE STA: %s Radio %d !!!",
                      pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                      pStationConfReq->wssStaConfig.DelSta.u1RadioId));
    }
    if (pStationConfReq->wssStaConfig.StaMsg.isPresent == OSIX_TRUE)
    {
        u4MsgLen +=
            (UINT4) (pStationConfReq->wssStaConfig.StaMsg.u2MessageLength +
                     CAPWAP_MSG_ELEM_TYPE_LEN);
    }
    if (pStationConfReq->wssStaConfig.StaSessKey.isPresent == OSIX_TRUE)
    {
        u4MsgLen +=
            (UINT4) (pStationConfReq->wssStaConfig.StaSessKey.u2MessageLength +
                     CAPWAP_MSG_ELEM_TYPE_LEN);
    }
    if (pStationConfReq->vendSpec.isOptional == OSIX_TRUE)
    {
        u4MsgLen += (UINT4) (pStationConfReq->vendSpec.u2MsgEleLen +
                             CAPWAP_MSG_ELEM_TYPE_LEN);
    }
    /* update Control Header */
    pStationConfReq->u2CapwapMsgElemenLen =
        (UINT2) (u4MsgLen + CAPWAP_MSG_ELEM_PLUS_SEQ_LEN);
    pStationConfReq->u1NumMsgBlocks = 0;    /* flags */
    pStationConfReq->u1SeqNum = (UINT1) (pSessEntry->lastTransmitedSeqNum + 1);
    pBuf = WLCHDLR_ALLOCATE_CRU_BUF (CAPWAP_MAX_PKT_LEN, 0);
    if (pBuf == NULL)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "Memory Allocation Failed \r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    pu1TxBuf = WLCHDLR_BUF_IF_LINEAR (pBuf, 0, CAPWAP_MAX_PKT_LEN);
    if (pu1TxBuf == NULL)
    {
        WLCHDLR_PKTBUF_ALLOC_MEM_BLOCK (pu1TxBuf);

        if (pu1TxBuf == NULL)
        {
            WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "Memory Allocation Failed \r\n");
            WLCHDLR_RELEASE_CRU_BUF (pBuf);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }

        u1IsNotLinear = OSIX_TRUE;
    }
    CapwapMsgStruct.CapAssembleStaConfReq.u2MsgLen =
        pStationConfReq->u2CapwapMsgElemenLen;
    CapwapMsgStruct.CapAssembleStaConfReq.pData = pu1TxBuf;
    CapwapMsgStruct.CapAssembleStaConfReq.pCapwapCtrlMsg = pStationConfReq;

    if (WssIfProcessCapwapMsg (WSS_CAPWAP_ASSEMBLE_STATION_CONF_REQ,
                               &CapwapMsgStruct) == OSIX_FAILURE)
    {
        WLCHDLR_RELEASE_CRU_BUF (pBuf);
        if (u1IsNotLinear == OSIX_TRUE)
        {
            WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pu1TxBuf);
        }
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "Failed to Assemble the packet \r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    if (u1IsNotLinear == OSIX_TRUE)
    {
        /* If the CRU buffer memory is not linear */
        WLCHDLR_COPY_TO_BUF (pBuf, pu1TxBuf, 0, CAPWAP_MAX_PKT_LEN);
        WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pu1TxBuf);
    }
    CapwapMsgStruct.CapwapTxPkt.pData = pBuf;
    CapwapMsgStruct.CapwapTxPkt.u4pktLen =
        (UINT4) (pStationConfReq->u2CapwapMsgElemenLen + CAPWAP_CMN_HDR_LEN);
    CapwapMsgStruct.CapwapTxPkt.u4IpAddr = pSessEntry->remoteIpAddr.u4_addr[0];
    CapwapMsgStruct.CapwapTxPkt.u4DestPort = pSessEntry->u4RemoteCtrlPort;
    if (WssIfProcessCapwapMsg (WSS_CAPWAP_TX_CTRL_PKT, &CapwapMsgStruct)
        == OSIX_FAILURE)
    {
        WLCHDLR_RELEASE_CRU_BUF (pBuf);
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "Failed to Transmit the packet \r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    WssIfPMDB.u2ProfileId = pSessEntry->u2IntProfileId;
    WssIfPMDB.WssPmWLCCapwapStatsSetFlag.bStaConfigStats = OSIX_TRUE;
    WssIfPMDB.WssPmWLCCapwapStatsSetFlag.bRunUpdateReqTransmitted = OSIX_TRUE;

    if (WssIfProcessPMDBMsg (WSS_PM_WLC_CAPWAP_RUN_STATS_SET,
                             &WssIfPMDB) != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to update PM db\r\n");
    }

    /* Before storing the packet check any packet is already stored in the
     * buffer. If yes then release the buffer and stop the timer. */
    if (pSessEntry->lastTransmittedPkt != NULL)
    {
        if (WlcHdlrTmrStop (pSessEntry, WLCHDLR_RETRANSMIT_INTERVAL_TMR)
            == OSIX_FAILURE)
        {
            WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                         "Failed to stop the retransmit timer \r\n");
        }
        WlchdlrFreeTransmittedPacket (pSessEntry);
    }
    /* store the packet buffer pointer in
     * the remote session DB copy the pointer */
    pSessEntry->lastTransmittedPkt = pBuf;
    pSessEntry->lastTransmittedPktLen =
        (UINT4) (pStationConfReq->u2CapwapMsgElemenLen + CAPWAP_CMN_HDR_LEN);

    WLCHDLR_STA_MEM_ALLOC (pWlcHdlrStaConfigDB);
    if (pWlcHdlrStaConfigDB == NULL)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                     "WlcHdlrProcessStationConfigReq:- "
                     "Failed : Mem Alloc for MemAllocMemBlk \n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    MEMSET (pWlcHdlrStaConfigDB, 0, sizeof (tWlcHdlrStaConfigDB));

    pWlcHdlrStaConfigDB->u4StaBufLen = pSessEntry->lastTransmittedPktLen;
    pWlcHdlrStaConfigDB->u1SeqNum = pStationConfReq->u1SeqNum;
    pWlcHdlrStaConfigDB->u2WtpProfileId = pSessEntry->u2IntProfileId;
    WLCHDLR_COPY_FROM_BUF (pBuf, pWlcHdlrStaConfigDB->au1StaBuf, 0,
                           pWlcHdlrStaConfigDB->u4StaBufLen);
    if (RBTreeAdd (gWlcHdlrStaConfigDB, pWlcHdlrStaConfigDB) != RB_SUCCESS)
    {
        WLCHDLR_STA_MEM_RELEASE (pWlcHdlrStaConfigDB);
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                     "Failed : Failed to add entry in RB Tree\r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    /*Start the Retransmit timer */
    WlcHdlrTmrStart (pSessEntry, WLCHDLR_RETRANSMIT_INTERVAL_TMR,
                     MAX_RETRANSMIT_INTERVAL_TIMEOUT);

    /* update the seq number */
    pSessEntry->lastTransmitedSeqNum++;

    /* update the retransmit count */
    pSessEntry->u1RetransmitCount = 0;

    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    WLCHDLR_FN_EXIT ();
    return OSIX_SUCCESS;

}

/************************************************************************/
/*  Function Name   : WlcHdlrProcessClearConfigReq                      */
/*  Description     : This function is handles the Clear Config Req.    */
/*  Input(s)        : pClearconfigReq - Pointer to Clear Conf Req Struct*/
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS | OSIX_FAILURE                       */
/************************************************************************/
INT4
WlcHdlrProcessClearConfigReq (tClearconfigReq * pClearconfigReq)
{
    UINT1              *pu1TxBuf = NULL;
    UINT4               u4MsgLen = 0;
    tRemoteSessionManager *pSessEntry = NULL;
    unCapwapMsgStruct   CapwapMsgStruct;
    UINT1               u1IsNotLinear = OSIX_FALSE;
    UINT1               u1NumRadios = 0;
    UINT1               u1RadioId = 0;
    UINT1               u1BssIdCount = 0;
    UINT1               u1IdCountIndex = 0;
    INT4                i4Result = 0;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tWssIfCapDB        *pWssIfCapwapDB1 = NULL;
    tRadioIfGetDB       RadioIfGetDB;
    tWssWlanMsgStruct  *pWssWlanMsg = NULL;
    tWssIfPMDB          WssIfPMDB;

    WLCHDLR_FN_ENTRY ();
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB1, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&WssIfPMDB, 0, sizeof (tWssIfPMDB));
    MEMSET (pWssIfCapwapDB1, 0, sizeof (tWssIfCapDB));
    pWssWlanMsg = (tWssWlanMsgStruct *) (VOID *) UtlShMemAllocWssWlanBuf ();
    if (pWssWlanMsg == NULL)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                     "WlcHdlrProcessClearConfigReq:- "
                     "UtlShMemAllocWssWlanBuf returned failure\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB1);
        return OSIX_FAILURE;
    }

    MEMSET (&CapwapMsgStruct, 0, sizeof (unCapwapMsgStruct));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    MEMSET (pWssWlanMsg, 0, sizeof (tWssWlanMsgStruct));

    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = pClearconfigReq->u2SessId;

    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_RSM_FROM_INDEX, pWssIfCapwapDB)
        != OSIX_SUCCESS)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB1);
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsg);
        return OSIX_FAILURE;
    }
    pSessEntry = pWssIfCapwapDB->pSessEntry;

    if (pSessEntry == NULL)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "CAPWAP Session Entry is NULL,"
                     " return FAILURE \r\n");
        i4Result = NO_AP_PRESENT;
    }
    else
    {
        if (pSessEntry->eCurrentState != CAPWAP_RUN)
        {
            WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "CAPWAP Session Entry is Not "
                         "in Run State, return FAILURE \r\n");
            i4Result = OSIX_SUCCESS;
        }
    }

    if ((pSessEntry == NULL) || pSessEntry->eCurrentState != CAPWAP_RUN)
    {
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpProfileId = OSIX_TRUE;
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
        pWssIfCapwapDB->CapwapIsGetAllDB.bNoOfRadio = OSIX_TRUE;
        pWssIfCapwapDB->CapwapIsGetAllDB.bModelCheck = OSIX_TRUE;
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpModelNumber = OSIX_TRUE;
        pWssIfCapwapDB->CapwapIsGetAllDB.bRadioAdminStatus = OSIX_TRUE;
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
            == OSIX_SUCCESS)
        {
            u1NumRadios = pWssIfCapwapDB->CapwapGetDB.u1WtpNoofRadio;
            for (u1RadioId = 1; u1RadioId <= u1NumRadios; u1RadioId++)
            {
                /* If Radio Admin status is up */
                if (pWssIfCapwapDB->CapwapGetDB.
                    au1WtpRadioAdminStatus[u1RadioId - 1] == 1)
                {
                    /* Radio Virtual If Index */
                    pWssIfCapwapDB1->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;
                    pWssIfCapwapDB1->CapwapGetDB.u1RadioId = u1RadioId;
                    pWssIfCapwapDB1->CapwapGetDB.u2WtpInternalId =
                        pClearconfigReq->u2SessId;
                    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                                 pWssIfCapwapDB1) ==
                        OSIX_SUCCESS)
                    {
                        RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
                            UtlShMemAllocAntennaSelectionBuf ();

                        if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection ==
                            NULL)
                        {
                            WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                                         "\r%% Memory allocation for Antenna"
                                         " selection failed.\r\n");
                            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB1);
                            UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsg);
                            return OSIX_FAILURE;
                        }
                        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
                            pWssIfCapwapDB1->CapwapGetDB.u4IfIndex;
                        RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
                        RadioIfGetDB.RadioIfIsGetAllDB.bBssIdCount = OSIX_TRUE;

                        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                                      (&RadioIfGetDB)) ==
                            OSIX_SUCCESS)
                        {
                            u1BssIdCount =
                                RadioIfGetDB.RadioIfGetAllDB.u1BssIdCount;
                            /* For every BSS ID count */
                            for (u1IdCountIndex = 1;
                                 u1IdCountIndex <= u1BssIdCount;
                                 u1IdCountIndex++)
                            {
                                RadioIfGetDB.RadioIfGetAllDB.u1WlanId =
                                    u1IdCountIndex;
                                RadioIfGetDB.RadioIfIsGetAllDB.bBssIfIndex =
                                    OSIX_TRUE;

                                if (WssIfProcessRadioIfDBMsg
                                    (WSS_GET_WLAN_BSS_IFINDEX_DB,
                                     (&RadioIfGetDB)) != OSIX_SUCCESS)
                                {
                                    WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                                                 "RadioIfConfigUpdateReq: Retreival "
                                                 "of BSSIF Index Failed \r\n");
                                    continue;
                                }

                                /* To Delete the Wlans */
                                pWssWlanMsg->unWssWlanMsg.
                                    WssWlanSetAdminOperStatus.u4IfIndex =
                                    RadioIfGetDB.RadioIfGetAllDB.u4BssIfIndex;
                                if (WssIfProcessWssWlanMsg
                                    (WSS_WLAN_CLEAR_CONFIG,
                                     pWssWlanMsg) != OSIX_SUCCESS)
                                {
                                    WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                                                 "RadioIfDeleteInterface: Deletion "
                                                 "of Wlan Failed \r\n");
                                    continue;
                                }
                            }
                        }
                        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                         RadioIfGetAllDB.
                                                         pu1AntennaSelection);
                    }
                }
            }
        }
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsg);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB1);
        return i4Result;
    }
    /* CAPWAP Header updated by CAPWAP
       module before assembling the packet */

    /* update Control Header */
    pClearconfigReq->u2CapwapMsgElemenLen =
        (UINT2) (u4MsgLen + CAPWAP_MSG_ELEM_PLUS_SEQ_LEN);
    pClearconfigReq->u1NumMsgBlocks = 0;
    pClearconfigReq->u1SeqNum = (UINT1) (pSessEntry->lastTransmitedSeqNum + 1);

    pBuf = WLCHDLR_ALLOCATE_CRU_BUF (CAPWAP_MAX_PKT_LEN, 0);
    if (pBuf == NULL)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "Memory Allocation Failed \r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB1);
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsg);
        return OSIX_FAILURE;
    }
    pu1TxBuf = WLCHDLR_BUF_IF_LINEAR (pBuf, 0, CAPWAP_MAX_PKT_LEN);
    if (pu1TxBuf == NULL)
    {
        WLCHDLR_PKTBUF_ALLOC_MEM_BLOCK (pu1TxBuf);
        WLCHDLR_COPY_FROM_BUF (pBuf, pu1TxBuf, 0, WLCHDLR_MAX_PKT_LEN);
        u1IsNotLinear = OSIX_TRUE;
    }

    CapwapMsgStruct.CapAssembleClearConfReq.pData = pu1TxBuf;
    CapwapMsgStruct.CapAssembleClearConfReq.pCapwapCtrlMsg = pClearconfigReq;

    if (WssIfProcessCapwapMsg (WSS_CAPWAP_ASSEMBLE_CLEARCONF_REQ,
                               &CapwapMsgStruct) == OSIX_FAILURE)
    {
        WLCHDLR_RELEASE_CRU_BUF (pBuf);
        if (u1IsNotLinear == OSIX_TRUE)
        {
            WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pu1TxBuf);
        }
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "wtpEnterClearConfigState:Failed "
                     "to Assemble the packet \r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB1);
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsg);
        return OSIX_FAILURE;
    }

    if (u1IsNotLinear == OSIX_TRUE)
    {
        /* If the CRU buffer memory is not linear */
        WLCHDLR_COPY_TO_BUF (pBuf, pu1TxBuf, 0, WLCHDLR_MAX_PKT_LEN);
        WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pu1TxBuf);
    }

    CapwapMsgStruct.CapwapTxPkt.pData = pBuf;
    CapwapMsgStruct.CapwapTxPkt.u4pktLen =
        (UINT4) (pClearconfigReq->u2CapwapMsgElemenLen + CAPWAP_CMN_HDR_LEN);
    CapwapMsgStruct.CapwapTxPkt.u4IpAddr = pSessEntry->remoteIpAddr.u4_addr[0];
    CapwapMsgStruct.CapwapTxPkt.u4DestPort = pSessEntry->u4RemoteCtrlPort;
    if (WssIfProcessCapwapMsg (WSS_CAPWAP_TX_CTRL_PKT, &CapwapMsgStruct)
        == OSIX_FAILURE)
    {
        WLCHDLR_RELEASE_CRU_BUF (pBuf);
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "wtpEnterClearConfigState:"
                     "Failed to Transmit the packet \r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB1);
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsg);
        return OSIX_FAILURE;
    }
    WssIfPMDB.u2ProfileId = pSessEntry->u2IntProfileId;
    WssIfPMDB.WssPmWLCCapwapStatsSetFlag.bClearConfigStats = OSIX_TRUE;
    WssIfPMDB.WssPmWLCCapwapStatsSetFlag.bRunUpdateReqTransmitted = OSIX_TRUE;

    if (WssIfProcessPMDBMsg (WSS_PM_WLC_CAPWAP_RUN_STATS_SET,
                             &WssIfPMDB) != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to update PM db\r\n");
    }

    /* Before storing the packet check any packet is already stored in the
     * buffer. If yes then release the buffer and stop the timer. */
    if (pSessEntry->lastTransmittedPkt != NULL)
    {
        if (WlcHdlrTmrStop (pSessEntry, WLCHDLR_RETRANSMIT_INTERVAL_TMR)
            == OSIX_FAILURE)
        {
            WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                         "Failed to stop the retransmit timer \r\n");
        }
        WlchdlrFreeTransmittedPacket (pSessEntry);
    }
    /* store the packet buffer pointer in
     * the remote session DB copy the pointer */
    pSessEntry->lastTransmittedPkt = pBuf;
    pSessEntry->lastTransmittedPktLen =
        (UINT4) (pClearconfigReq->u2CapwapMsgElemenLen + CAPWAP_CMN_HDR_LEN);

    /*Start the Retransmit timer */
    WlcHdlrTmrStart (pSessEntry, WLCHDLR_RETRANSMIT_INTERVAL_TMR,
                     MAX_RETRANSMIT_INTERVAL_TIMEOUT);

    /* update the seq number */
    pSessEntry->lastTransmitedSeqNum++;
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB1);
    UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsg);
    WLCHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : WlcHdlrProcessResetReq                            */
/*  Description     : This function is handles the Reset Request.       */
/*  Input(s)        : pResetReq - Pointer to Reset Req Struct           */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS | OSIX_FAILURE                       */
/************************************************************************/
INT4
WlcHdlrProcessResetReq (tResetReq * pResetReq)
{
    UINT1              *pu1TxBuf = NULL;
    UINT4               u4MsgLen = 0;
    tRemoteSessionManager *pSessEntry = NULL;
    unCapwapMsgStruct   CapwapMsgStruct;
    UINT1               u1IsNotLinear = OSIX_FALSE;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tWssIfPMDB          WssIfPMDB;

    WLCHDLR_FN_ENTRY ();
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (&CapwapMsgStruct, 0, sizeof (unCapwapMsgStruct));
    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&WssIfPMDB, 0, sizeof (tWssIfPMDB));

    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = pResetReq->u2SessId;

    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_RSM_FROM_INDEX, pWssIfCapwapDB)
        != OSIX_SUCCESS)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    pSessEntry = pWssIfCapwapDB->pSessEntry;

    if (pSessEntry == NULL)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return NO_AP_PRESENT;
    }

    /* update Control Header */
    pResetReq->u2CapwapMsgElemenLen =
        (UINT2) (u4MsgLen + CAPWAP_MSG_ELEM_PLUS_SEQ_LEN);
    pResetReq->u1NumMsgBlocks = 0;    /* flags */
    pResetReq->u1SeqNum = (UINT1) (pSessEntry->lastTransmitedSeqNum + 1);

    pBuf = WLCHDLR_ALLOCATE_CRU_BUF (CAPWAP_MAX_PKT_LEN, 0);
    if (pBuf == NULL)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "Memory Allocation Failed \r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    pu1TxBuf = WLCHDLR_BUF_IF_LINEAR (pBuf, 0, CAPWAP_MAX_PKT_LEN);
    if (pu1TxBuf == NULL)
    {
        WLCHDLR_PKTBUF_ALLOC_MEM_BLOCK (pu1TxBuf);
        WLCHDLR_COPY_FROM_BUF (pBuf, pu1TxBuf, 0, WLCHDLR_MAX_PKT_LEN);
        u1IsNotLinear = OSIX_TRUE;
    }

    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    pWssIfCapwapDB->CapwapIsGetAllDB.bImageName = OSIX_TRUE;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
        != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "WlcHdlrProcessResetReq: "
                    "Getting IMAGE ID for WLC failed \r\n");
        WLCHDLR_RELEASE_CRU_BUF (pBuf);
        if (u1IsNotLinear == OSIX_TRUE)
        {
            WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pu1TxBuf);
        }
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    MEMCPY (pResetReq->imageId.data,
            pWssIfCapwapDB->CapwapGetDB.au1WtpImageIdentifier,
            pWssIfCapwapDB->CapwapGetDB.au1WtpImageIdLen);
    pResetReq->imageId.u2MsgEleLen =
        pWssIfCapwapDB->CapwapGetDB.au1WtpImageIdLen;

    MEMCPY (pResetReq->imageId.data,
            pSessEntry->ImageId.data, pSessEntry->ImageId.u2MsgEleLen);
    pResetReq->imageId.u2MsgEleLen = pSessEntry->ImageId.u2MsgEleLen;

    /* CAPWAP Header updated by CAPWAP
       module before assembling the packet */
    if (CapwapGetWLCImageId (&pResetReq->imageId, &u4MsgLen) == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to get Mandatory Message Elements \n");
        WLCHDLR_RELEASE_CRU_BUF (pBuf);
        if (u1IsNotLinear == OSIX_TRUE)
        {
            WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pu1TxBuf);
        }
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    pResetReq->u2CapwapMsgElemenLen =
        (UINT2) (pResetReq->u2CapwapMsgElemenLen + u4MsgLen);

    CapwapMsgStruct.CapAssembleResetReq.pData = pu1TxBuf;
    CapwapMsgStruct.CapAssembleResetReq.pCapwapCtrlMsg = pResetReq;

    if (WssIfProcessCapwapMsg (WSS_CAPWAP_ASSEMBLE_RESET_REQ,
                               &CapwapMsgStruct) == OSIX_FAILURE)
    {
        WLCHDLR_RELEASE_CRU_BUF (pBuf);
        if (u1IsNotLinear == OSIX_TRUE)
        {
            WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pu1TxBuf);
        }
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "Failed to Assemble the packet \r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    if (u1IsNotLinear == OSIX_TRUE)
    {
        /* If the CRU buffer memory is not linear */
        WLCHDLR_COPY_TO_BUF (pBuf, pu1TxBuf, 0, WLCHDLR_MAX_PKT_LEN);
        WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pu1TxBuf);
    }

    CapwapMsgStruct.CapwapTxPkt.pData = pBuf;
    CapwapMsgStruct.CapwapTxPkt.u4pktLen =
        (UINT4) (pResetReq->u2CapwapMsgElemenLen + CAPWAP_CMN_HDR_LEN);
    CapwapMsgStruct.CapwapTxPkt.u4IpAddr = pSessEntry->remoteIpAddr.u4_addr[0];
    CapwapMsgStruct.CapwapTxPkt.u4DestPort = pSessEntry->u4RemoteCtrlPort;
    if (WssIfProcessCapwapMsg (WSS_CAPWAP_TX_CTRL_PKT, &CapwapMsgStruct)
        == OSIX_FAILURE)
    {
        WLCHDLR_RELEASE_CRU_BUF (pBuf);
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "Failed to Transmit the packet \r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    WssIfPMDB.u2ProfileId = pSessEntry->u2IntProfileId;
    WssIfPMDB.WssPmWLCCapwapStatsSetFlag.bResetStats = OSIX_TRUE;
    WssIfPMDB.WssPmWLCCapwapStatsSetFlag.bRunUpdateReqTransmitted = OSIX_TRUE;

    if (WssIfProcessPMDBMsg (WSS_PM_WLC_CAPWAP_RUN_STATS_SET,
                             &WssIfPMDB) != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to update PM db\r\n");
    }

    /* Before storing the packet check any packet is already stored in the
     * buffer. If yes then release the buffer and stop the timer. */
    if (pSessEntry->lastTransmittedPkt != NULL)
    {
        if (WlcHdlrTmrStop (pSessEntry, WLCHDLR_RETRANSMIT_INTERVAL_TMR)
            == OSIX_FAILURE)
        {
            WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                         "Failed to stop the retransmit timer \r\n");
        }
        WlchdlrFreeTransmittedPacket (pSessEntry);

    }
    /* store the packet buffer pointer in
     * the remote session DB copy the pointer */
    pSessEntry->lastTransmittedPkt = pBuf;
    pSessEntry->lastTransmittedPktLen =
        (UINT4) (pResetReq->u2CapwapMsgElemenLen + CAPWAP_CMN_HDR_LEN);
    /*Start the Retransmit timer */
    WlcHdlrTmrStart (pSessEntry, WLCHDLR_RETRANSMIT_INTERVAL_TMR,
                     MAX_RETRANSMIT_INTERVAL_TIMEOUT);

    /* update the seq number */
    pSessEntry->lastTransmitedSeqNum++;

    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    WLCHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : WlcHdlrProcessDataTransferReq                     */
/*  Description     : This function is handles the Dat Transfer Req.    */
/*  Input(s)        : pDataReq - Pointer to Data Req Struct             */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS | OSIX_FAILURE                       */
/************************************************************************/
INT4
WlcHdlrProcessDataTransferReq (tDataReq * pDataReq)
{
    UINT1              *pu1TxBuf = NULL;
    UINT4               u4MsgLen = 0;
    tRemoteSessionManager *pSessEntry = NULL;
    unCapwapMsgStruct   CapwapMsgStruct;
    UINT1               u1IsNotLinear = OSIX_FALSE;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tWssIfPMDB          WssIfPMDB;

    WLCHDLR_FN_ENTRY ();
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (&CapwapMsgStruct, 0, sizeof (unCapwapMsgStruct));
    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    MEMSET (&WssIfPMDB, 0, sizeof (tWssIfPMDB));
    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = pDataReq->u2SessId;

    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_RSM_FROM_INDEX, pWssIfCapwapDB)
        != OSIX_SUCCESS)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, " WssIfprocessDBMsg : Failed \r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    pSessEntry = pWssIfCapwapDB->pSessEntry;

    if (pSessEntry == NULL)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                     "pSession Entry is NULL, return FAILURE \r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    if (pSessEntry->eCurrentState != CAPWAP_RUN)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "CAPWAP Session Entry is Not "
                     "in RunState, return FAILURE \r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    if (pDataReq->datatransfermode.isPresent == OSIX_TRUE)
    {
        u4MsgLen += (UINT4) (pDataReq->datatransfermode.u2MsgEleLen +
                             CAPWAP_MSG_ELEM_TYPE_LEN);
    }

    pDataReq->u2CapwapMsgElemenLen =
        (UINT2) (u4MsgLen + CAPWAP_MSG_ELEM_PLUS_SEQ_LEN);
    pDataReq->u1NumMsgBlocks = 0;    /* flags */
    pDataReq->u1SeqNum = (UINT1) (pSessEntry->lastTransmitedSeqNum + 1);

    pBuf = WLCHDLR_ALLOCATE_CRU_BUF (CAPWAP_MAX_PKT_LEN, 0);
    if (pBuf == NULL)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "Memory Allocation Failed \r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    pu1TxBuf = WLCHDLR_BUF_IF_LINEAR (pBuf, 0, CAPWAP_MAX_PKT_LEN);
    if (pu1TxBuf == NULL)
    {
        WLCHDLR_PKTBUF_ALLOC_MEM_BLOCK (pu1TxBuf);
        if (pu1TxBuf == NULL)
        {
            WLCHDLR_RELEASE_CRU_BUF (pBuf);
            WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "Memory Allocation Failed \r\n");
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
        u1IsNotLinear = OSIX_TRUE;
    }

    CapwapMsgStruct.CapAssembleDataTransferReq.pData = pu1TxBuf;
    CapwapMsgStruct.CapAssembleDataTransferReq.pCapwapCtrlMsg = pDataReq;
    if (WssIfProcessCapwapMsg (WSS_CAPWAP_ASSEMBLE_DATATRANSFER_REQ,
                               &CapwapMsgStruct) == OSIX_FAILURE)
    {
        WLCHDLR_RELEASE_CRU_BUF (pBuf);
        if (u1IsNotLinear == OSIX_TRUE)
        {
            WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pu1TxBuf);
        }
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "WlcHdlrProcessDataTransferReq: "
                     "Failed to Assemble thepacket\r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    if (u1IsNotLinear == OSIX_TRUE)
    {
        /* If the CRU buffer memory is not linear */
        WLCHDLR_COPY_TO_BUF (pBuf, pu1TxBuf, 0, CAPWAP_MAX_PKT_LEN);
        WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pu1TxBuf);
    }

#if IMPLEMENTATION_ONGOING
    /* Start the DataTransfer Timer */
    if (WlcHdlrTmrStart (pSessEntry, WLCHDLR_DATA_TRANSFER_TMR,
                         CAPWAP_DATA_TRANSFER_TIMER_VAL) != OSIX_SUCCESS)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "WlcHdlrProcessDataTransferReq: "
                     "Data Transfer timer start failed\r\n");
    }
    printf ("%s::%d Set the Falg to true\n", __FUNCTION__, __LINE__);
    /* Set the Data Transfer Timer Running Falg to True */
    pSessEntry->u1DataTransferTimerRunning = OSIX_TRUE;
#endif

    CapwapMsgStruct.CapwapTxPkt.pData = pBuf;
    CapwapMsgStruct.CapwapTxPkt.u4pktLen =
        (UINT4) (pDataReq->u2CapwapMsgElemenLen + CAPWAP_CMN_HDR_LEN);
    CapwapMsgStruct.CapwapTxPkt.u4IpAddr = pSessEntry->remoteIpAddr.u4_addr[0];
    CapwapMsgStruct.CapwapTxPkt.u4DestPort = pSessEntry->u4RemoteCtrlPort;
    if (WssIfProcessCapwapMsg (WSS_CAPWAP_TX_CTRL_PKT, &CapwapMsgStruct) ==
        OSIX_FAILURE)
    {
        WLCHDLR_RELEASE_CRU_BUF (pBuf);
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "WlcHdlrProcessDataTransferReq: "
                     "Failed to Transmit thepacket\r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    WssIfPMDB.u2ProfileId = pSessEntry->u2IntProfileId;
    WssIfPMDB.WssPmWLCCapwapStatsSetFlag.bDataTransferStats = OSIX_TRUE;
    WssIfPMDB.WssPmWLCCapwapStatsSetFlag.bRunUpdateReqTransmitted = OSIX_TRUE;

    if (WssIfProcessPMDBMsg (WSS_PM_WLC_CAPWAP_RUN_STATS_SET,
                             &WssIfPMDB) != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to update PM db\r\n");
    }

    /* Before storing the packet check any packet is already stored in the
     * buffer. If yes then release the buffer and stop the timer. */
    if (pSessEntry->lastTransmittedPkt != NULL)
    {
        if (WlcHdlrTmrStop (pSessEntry, WLCHDLR_RETRANSMIT_INTERVAL_TMR)
            == OSIX_FAILURE)
        {
            WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                         "Failed to stop the retransmit timer \r\n");
        }
        WlchdlrFreeTransmittedPacket (pSessEntry);
    }
    /* store the packet buffer pointer in
     * the remote session DB copy the pointer */
    pSessEntry->lastTransmittedPkt = pBuf;
    pSessEntry->lastTransmittedPktLen =
        (UINT4) (pDataReq->u2CapwapMsgElemenLen + 13);

    /*Start the Retransmit timer */
    if (WlcHdlrTmrStart (pSessEntry, WLCHDLR_RETRANSMIT_INTERVAL_TMR,
                         MAX_RETRANSMIT_INTERVAL_TIMEOUT) != OSIX_SUCCESS)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "WlcHdlrProcessDataTransferReq: "
                     "Retransmit timer start failed\r\n");
    }
    /* update the seq number */
    pSessEntry->lastTransmitedSeqNum++;

    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    WLCHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : WlcHdlrProcessWssCFAMsg                           */
/*  Description     : This function is the WlcHdlr Interface for CFA    */
/*  Input(s)        : MsgType - Message Type                            */
/*                    pWlcHdlrMsgStruct - Message Structure             */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS | OSIX_FAILURE                       */
/************************************************************************/
INT4
WlcHdlrProcessWssCFAMsg (UINT1 MsgType, unWlcHdlrMsgStruct * pWlcHdlrMsgStruct)
{
    UNUSED_PARAM (MsgType);

    if (WlcHdlrEnqueCtrlPkts (pWlcHdlrMsgStruct) != OSIX_SUCCESS)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "Failed to Process the "
                     "Queue Message Received from CFA \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

#ifdef CLKIWF_WANTED
/************************************************************************/
/*  Function Name   : WlcHdlrProcessWssClkUpdateMsg                     */
/*  Description     : This function is the WlcHdlr Interface for CFA    */
/*  Input(s)        : MsgType - Message Type                            */
/*                    pWlcHdlrMsgStruct - Message Structure             */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS | OSIX_FAILURE                       */
/************************************************************************/
INT4
WlcHdlrProcessWssClkUpdateMsg (INT4 i4TimeSource,
                               tUtlSysPreciseTime * pSysPreciseTime,
                               tUtlTm * pUtlTm)
{
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;

    UINT1               u1Index = 0;
    UINT1               u1MaxWtps = 0;
    UNUSED_PARAM (i4TimeSource);
    UNUSED_PARAM (pSysPreciseTime);
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        pWlcHdlrMsgStruct =
        (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pWlcHdlrMsgStruct == NULL)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));
    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    pWssIfCapwapDB->CapwapIsGetAllDB.bActiveWtps = OSIX_TRUE;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB) !=
        OSIX_SUCCESS)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;
    }

    u1MaxWtps = (UINT1) pWssIfCapwapDB->u2ActiveWtps;

    pWlcHdlrMsgStruct->ClkiwfConfigUpdateReq.acTimestamp.isOptional = OSIX_TRUE;
    pWlcHdlrMsgStruct->ClkiwfConfigUpdateReq.acTimestamp.u2MsgEleType
        = CAPWAP_AC_TIMESTAMP_MSG_TYPE;
    pWlcHdlrMsgStruct->ClkiwfConfigUpdateReq.acTimestamp.u2MsgEleLen = 4;

    pUtlTm->tm_yday = pUtlTm->tm_yday + 1;

#ifdef SNTP_WANTED
    pWlcHdlrMsgStruct->ClkiwfConfigUpdateReq.acTimestamp.u4Timestamp
        = SntpTmToSec (pUtlTm);
#endif
    for (u1Index = 0; u1Index < u1MaxWtps; u1Index++)
    {
        pWlcHdlrMsgStruct->ClkiwfConfigUpdateReq.u4SessId = u1Index;

        if (WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_CLKIWF_CONF_UPDATE_REQ,
                                    pWlcHdlrMsgStruct) == NO_AP_PRESENT)
        {
            WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                         "No AP not resent during WTP timestamp Updation\r\n");

        }

    }

    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);

    return OSIX_SUCCESS;
}
#endif

#endif /* __WLCHDLRPORT_C__ */
