/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: wlchdlrwtpevent.c,v 1.4 2018/01/22 09:39:53 siva Exp $
 *
 * Description: This file contains the WLCHDLR WTP Event message routines.
 *
 *****************************************************************************/
#ifndef _WLCHDLR_WTP_EVENT_C_
#define _WLCHDLR_WTP_EVENT_C_

#include "wlchdlrinc.h"
#include "capwap.h"
#include "wssifpmdb.h"
#include "rfmconst.h"
#include "wssstawlcmacr.h"
#include "wssstawlcprot.h"

extern tWssClientSummary gaWssClientSummary[MAX_STA_SUPP_PER_WLC];
extern UINT4        gu4ClientWalkIndex;
UINT4               gu4WlcIpAddr = 0;

/************************************************************************/
/*  Function Name   : CapwapProcessWtpEventRequest                      */
/*  Description     : The function processes the received WLCHDLR       */
/*                    Wtp Event request                                 */
/*  Input(s)        : pWtpEveReqMsg - WLCHDLR Wtp Event request packet  */
/*                                   received                           */
/*                    pSessEntry - Pointer to the session data base     */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
CapwapProcessWtpEventRequest (tCRU_BUF_CHAIN_HEADER * pBuf,
                              tCapwapControlPacket * pWtpEveReqMsg,
                              tRemoteSessionManager * pSessEntry)
{
    INT4                i4Status = OSIX_SUCCESS;
    tWtpEveRsp          WtpEveResp;
    UINT1              *pu1TxBuf = NULL, *pRcvdBuf = NULL;
    UINT4               u4MsgLen = 0;
    tCRU_BUF_CHAIN_HEADER *pTxBuf = NULL;
    UINT1               u1IsNotLinear = OSIX_FALSE;
    UINT2               u2PacketLen = 0;
    UINT1               u1IsInputNotLinear = OSIX_FALSE;

    WLCHDLR_FN_ENTRY ();

    if (pSessEntry == NULL)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "WLCHDLR Session Entry is NULL, "
                     "return FAILURE \r\n");
        return OSIX_FAILURE;
    }
    MEMSET (&WtpEveResp, 0, sizeof (tWtpEveRsp));

    u2PacketLen = CAPWAP_GET_BUF_LEN (pBuf);
    pRcvdBuf = CAPWAP_BUF_IF_LINEAR (pBuf, 0, CAPWAP_MAX_PKT_LEN);

    if (pRcvdBuf == NULL)
    {
        WLCHDLR_PKTBUF_ALLOC_MEM_BLOCK (pRcvdBuf);
        /* Kloc Fix Start */
        if (pRcvdBuf == NULL)
        {
            WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "Memory Allocation Failed\n");
            return OSIX_FAILURE;
        }
        /* Kloc Fix Ends */
        CAPWAP_COPY_FROM_BUF (pBuf, pRcvdBuf, 0, u2PacketLen);
        u1IsInputNotLinear = OSIX_TRUE;
    }

    /* Validate the reset message. if it is success construct the Config 
       Status Response */
    if ((CapValidateWtpEventReqMsgElems (pRcvdBuf, pWtpEveReqMsg)
         == OSIX_SUCCESS))
    {
        if (u1IsInputNotLinear == OSIX_TRUE)
        {
            WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pRcvdBuf);
        }

        if (CapwapConstructCpHeader (&WtpEveResp.capwapHdr) == OSIX_FAILURE)
        {
            WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                         "capwapProcessWtpEventRequest:Failed to construct "
                         "the capwap Header\n");
            return OSIX_FAILURE;
        }
        /* get message elements to construct the Wtp Event response */
        /* update Control Header */
        WtpEveResp.u1SeqNum = pWtpEveReqMsg->capwapCtrlHdr.u1SeqNum;
        WtpEveResp.u2CapwapMsgElemenLen =
            (UINT2) (WtpEveResp.u2CapwapMsgElemenLen + u4MsgLen +
                     CAPWAP_MSG_ELEM_PLUS_SEQ_LEN);
        WtpEveResp.u1NumMsgBlocks = 0;    /* flags always zero */

        /* Allocate memory for packet linear buffer */
        pTxBuf = CAPWAP_ALLOCATE_CRU_BUF (CAPWAP_MAX_PKT_LEN, 0);
        if (pTxBuf == NULL)
        {
            WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "Memory Allocation Failed\n");
            return OSIX_FAILURE;
        }
        pu1TxBuf = CAPWAP_BUF_IF_LINEAR (pTxBuf, 0, CAPWAP_MAX_PKT_LEN);
        if (pu1TxBuf == NULL)
        {
            WLCHDLR_PKTBUF_ALLOC_MEM_BLOCK (pu1TxBuf);
            /* Kloc Fix Start */
            if (pu1TxBuf == NULL)
            {
                WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "Memory Allocation Failed\n");
                CAPWAP_RELEASE_CRU_BUF (pTxBuf);
                return OSIX_FAILURE;
            }
            /* Kloc Fix Ends */
            CAPWAP_COPY_FROM_BUF (pTxBuf, pu1TxBuf, 0, CAPWAP_MAX_PKT_LEN);
            u1IsNotLinear = OSIX_TRUE;
        }
        if ((CapwapAssembleWtpEventResponse (WtpEveResp.u2CapwapMsgElemenLen,
                                             &WtpEveResp,
                                             pu1TxBuf)) == OSIX_FAILURE)
        {

            WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                         "Failed to assemble Config Station Response Message \r\n");
            if (u1IsNotLinear == OSIX_TRUE)
            {
                WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pu1TxBuf);
            }
            CAPWAP_RELEASE_CRU_BUF (pTxBuf);
            return OSIX_FAILURE;
        }
        if (u1IsNotLinear == OSIX_TRUE)
        {
            /* If the CRU buffer memory is not linear */
            CAPWAP_COPY_TO_BUF (pTxBuf, pu1TxBuf, 0, CAPWAP_MAX_PKT_LEN);
            WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pu1TxBuf);
        }
        /* Transmit Configuration Status Response Message */
        i4Status = CapwapTxMessage (pTxBuf,
                                    pSessEntry, (UINT4)
                                    (WtpEveResp.u2CapwapMsgElemenLen
                                     + CAPWAP_CMN_HDR_LEN),
                                    WSS_CAPWAP_802_11_CTRL_PKT);
        if (i4Status == OSIX_FAILURE)
        {
            WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                         "Failed to Transmit the packet\n");
        }
        else
        {
            pSessEntry->lastTransmitedSeqNum++;
        }
        CAPWAP_RELEASE_CRU_BUF (pTxBuf);
        return (i4Status);
    }
    else
    {
        if (u1IsInputNotLinear == OSIX_TRUE)
        {
            WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pRcvdBuf);
        }
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                     "Failed to validate WTP Event Req \r\n");

        return OSIX_FAILURE;
    }

    WLCHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapValidateWTPEventVendSpecPld                 */
/*  Description     : The function validates the WTP Event for          */
/*                    Vendor Spec Id.                                   */
/*  Input(s)        : pRcvBuf - Received Buffer                         */
/*                    cwMsg - Capwap Ctrl Msg                           */
/*                    u2MsgIndex - Message Index                        */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/
INT4
CapwapValidateWTPEventVendSpecPld (UINT1 *pRcvBuf,
                                   tCapwapControlPacket * cwMsg,
                                   UINT2 u2MsgIndex)
{
    UINT4               u4Offset = 0;

#ifndef RFMGMT_WANTED
    UINT2               u2NumMsgElems = 0;
#endif
#ifdef WPS_WANTED
    tWssWpsNotifyParams WssWpsNotifyParams;
    MEMSET (&WssWpsNotifyParams, 0, sizeof (tWssWpsNotifyParams));
#endif
    tWssIfPMDB          wssIfPMDB;
    tWssIfPMDB          wssIfPMBSSIDDB;
    tWssIfPMDB          wssIfPMClientDB;
    tRadioIfMsgStruct   RadioIfMsgStruct;
    tRadioIfGetDB       RadioIfGetDB;
    UINT1               numBssCount = 0;
    UINT1               numRadioCount = 0;
    UINT1               numStaCount = 0;
    tMacAddr            BssId;
    UINT1               u1index = 0;
    UINT1               u1index1 = 0;
    UINT1               u1index2 = 0;
    UINT1               u1Radioid = 0;
    UINT1               u1MultiDomainIndex = 0;
    UINT2               elementType = 0;
    UINT2               u2Length = 0;
    UINT2               u2MsgLength = 0;
    UINT2               u2NumRadioElems = 0;
    tVendorSpecPayload  VendorSpecPayload;
    UINT1              *pTempBuf = NULL;
    UINT4               u4PrevSentBytes = 0;
    UINT4               u4PrevRcvdBytes = 0;
    UINT4               u4StaIpAddr = 0;
    UINT4               u4Index = 0;
    UINT4               u4ClientStatsBytesRecvdCount = 0;
    UINT4               u4ClientStatsBytesSentCount = 0;
    tWssWlanDB          WssWlanDB;
    tWssWlanDB          wssWlanDB;
#ifdef RFMGMT_WANTED
    UINT2               u2MsgLen = 0;
    UINT1               u1NeighborApCount = 0, u1Index = 0;
    UINT2               u1ClientCount = 0;
    tRfMgmtMsgStruct    RfMgmtMsgStruct;
    UINT4               u4MaxChannel;
    tRfMgmtScanChannel *pTempScanChannel;

    MEMSET (&RfMgmtMsgStruct, 0, sizeof (tRfMgmtMsgStruct));
#endif

    WLCHDLR_FN_ENTRY ();

    MEMSET (&VendorSpecPayload, 0, sizeof (tVendorSpecPayload));
    MEMSET (&wssIfPMDB, 0, sizeof (tWssIfPMDB));
    MEMSET (&wssIfPMBSSIDDB, 0, sizeof (tWssIfPMDB));
    MEMSET (&wssIfPMClientDB, 0, sizeof (tWssIfPMDB));
    MEMSET (&BssId, 0, sizeof (tMacAddr));
    MEMSET (&wssWlanDB, 0, sizeof (tWssWlanDB));
    MEMSET (&WssWlanDB, 0, sizeof (tWssWlanDB));
    MEMSET (&RadioIfMsgStruct, 0, sizeof (tRadioIfMsgStruct));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

#ifndef RFMGMT_WANTED
    u2NumMsgElems = cwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;
    if (u2NumMsgElems > 1)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "The Message element Vendor "
                    "specific payload Received More than once \r\n");
    }
    else
#endif
    {
        u4Offset = cwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[0];
        pTempBuf = pRcvBuf;
        pRcvBuf += u4Offset;
        CAPWAP_GET_2BYTE (VendorSpecPayload.u2MsgEleType, pRcvBuf);
        CAPWAP_GET_2BYTE (VendorSpecPayload.u2MsgEleLen, pRcvBuf);
        CAPWAP_GET_2BYTE (elementType, pRcvBuf);
        VendorSpecPayload.elementId =
            (UINT2) (elementType - VENDOR_SPECIFIC_MSG);
        switch (VendorSpecPayload.elementId)
        {
            case VENDOR_BSSID_STATS_TYPE:

                u4Offset = cwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[0];
                pRcvBuf = pTempBuf;
                pRcvBuf += u4Offset;
                /* Copy the message type and length */
                CAPWAP_SKIP_N_BYTES (CAPWAP_MSG_ELEM_TYPE_LEN, pRcvBuf);
                /* Skip 4 bytes vendor id */
                CAPWAP_SKIP_N_BYTES (CAPWAP_VENDOR_ID_LEN, pRcvBuf);
                /* Skip 2 bytes element id */
                CAPWAP_SKIP_N_BYTES (CAPWAP_VENDOR_ELEM_LEN, pRcvBuf);

                MEMSET (&wssIfPMDB, 0, sizeof (tWssIfPMDB));
                CAPWAP_GET_NBYTE (wssIfPMDB.wtpVSPCPWPWtpStats, pRcvBuf,
                                  sizeof (tWtpCPWPWtpStats));

                CAPWAP_TRC (CAPWAP_STATS_TRC,
                            "---------- Capwap wtp stats--------- \n");
                CAPWAP_TRC1 (CAPWAP_STATS_TRC,
                             "VSPCPWPWtpStats:: discReqSent %d\n",
                             wssIfPMDB.wtpVSPCPWPWtpStats.discReqSent);
                CAPWAP_TRC1 (CAPWAP_STATS_TRC,
                             "VSPCPWPWtpStats:: discRespRxd %d\n",
                             wssIfPMDB.wtpVSPCPWPWtpStats.discRespRxd);
                CAPWAP_TRC1 (CAPWAP_STATS_TRC,
                             "VSPCPWPWtpStats:: joinReqSent %d\n ",
                             wssIfPMDB.wtpVSPCPWPWtpStats.joinReqSent);
                CAPWAP_TRC1 (CAPWAP_STATS_TRC,
                             "VSPCPWPWtpStats:: joinRespRxd %d\n ",
                             wssIfPMDB.wtpVSPCPWPWtpStats.joinRespRxd);
                CAPWAP_TRC1 (CAPWAP_STATS_TRC,
                             "VSPCPWPWtpStats:: cfgReqSent %d\n",
                             wssIfPMDB.wtpVSPCPWPWtpStats.cfgReqSent);
                CAPWAP_TRC1 (CAPWAP_STATS_TRC,
                             "VSPCPWPWtpStats:: cfgRespRxd %d\n",
                             wssIfPMDB.wtpVSPCPWPWtpStats.cfgRespRxd);
                CAPWAP_TRC1 (CAPWAP_STATS_TRC,
                             "VSPCPWPWtpStats:: keepAliveMsgRcvd %d\n",
                             wssIfPMDB.wtpVSPCPWPWtpStats.keepAliveMsgRcvd);

                wssIfPMDB.tWtpPmAttState.bWssPmWtpEvtWtpCWStatsSet = OSIX_TRUE;
                wssIfPMDB.u2ProfileId = cwMsg->u2IntProfileId;
                if (WssIfProcessPMDBMsg
                    (WSS_PM_WTP_EVT_WTP_CAPWAP_SESS_STATS_SET,
                     &wssIfPMDB) != OSIX_SUCCESS)
                {
                    return OSIX_FAILURE;
                }

                CAPWAP_GET_1BYTE (numBssCount, pRcvBuf);
                for (u1index = 0; u1index < numBssCount; u1index++)
                {
                    MEMSET (&wssIfPMDB, 0, sizeof (tWssIfPMDB));

                    WssWlanDB.WssWlanIsPresentDB.bBssIfIndex = OSIX_TRUE;
                    WssWlanDB.WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
                    CAPWAP_GET_NBYTE (WssWlanDB.WssWlanAttributeDB.BssId,
                                      pRcvBuf, MAC_ADDR_LEN);
                    if (WssIfProcessWssWlanDBMsg
                        (WSS_WLAN_GET_BSSID_MAPPING_ENTRY,
                         &WssWlanDB) != OSIX_SUCCESS)
                    {
                        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                                     "Validate WTP event VSP :\
                                Failed to return BssIfIndex \r\n");
                        return OSIX_FAILURE;
                    }

                    MEMCPY (&BssId, WssWlanDB.WssWlanAttributeDB.BssId,
                            MAC_ADDR_LEN);
                    MEMSET (&wssIfPMDB.tWtpVSPBSSIDStats,
                            0, sizeof (tBSSIDMacHndlrStats));

                    CAPWAP_GET_4BYTE (wssIfPMDB.tWtpVSPBSSIDStats.
                                      wlanBSSIDBeaconsSentCount, pRcvBuf);
                    CAPWAP_GET_4BYTE (wssIfPMDB.tWtpVSPBSSIDStats.
                                      wlanBSSIDProbeReqRcvdCount, pRcvBuf);
                    CAPWAP_GET_4BYTE (wssIfPMDB.tWtpVSPBSSIDStats.
                                      wlanBSSIDProbeRespSentCount, pRcvBuf);
                    CAPWAP_GET_4BYTE (wssIfPMDB.tWtpVSPBSSIDStats.
                                      wlanBSSIDDataPktRcvdCount, pRcvBuf);
                    CAPWAP_GET_4BYTE (wssIfPMDB.tWtpVSPBSSIDStats.
                                      wlanBSSIDDataPktSentCount, pRcvBuf);

                    wssIfPMBSSIDDB.u2ProfileBSSID =
                        (UINT2) WssWlanDB.WssWlanAttributeDB.u4BssIfIndex;

                    if (WssIfProcessPMDBMsg (WSS_PM_WTP_EVT_VSP_BSSID_STATS_GET,
                                             &wssIfPMBSSIDDB) != OSIX_SUCCESS)
                    {
                        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                                     "WSS_PM_WTP_EVT_VSP_BSSID_STATS_GET Failed\r\n");
                    }

                    MEMSET (&wssIfPMDB.tWtpPmAttState, 0,
                            sizeof (tWssPmAttributeDB));
                    wssIfPMDB.tWtpPmAttState.bWssPmVSPBSSIDStatsSet = OSIX_TRUE;
                    wssIfPMDB.tWtpVSPBSSIDStats.isOptional = OSIX_FALSE;
                    wssIfPMDB.u2ProfileBSSID =
                        (UINT2) WssWlanDB.WssWlanAttributeDB.u4BssIfIndex;
                    wssIfPMDB.tWtpVSPBSSIDStats.wlanBSSIDDataPktRcvdCount =
                        wssIfPMBSSIDDB.tWtpVSPBSSIDStats.
                        wlanBSSIDDataPktRcvdCount;
                    wssIfPMDB.tWtpVSPBSSIDStats.wlanBSSIDDataPktSentCount =
                        wssIfPMBSSIDDB.tWtpVSPBSSIDStats.
                        wlanBSSIDDataPktSentCount;

                    if (WssIfProcessPMDBMsg (WSS_PM_WTP_EVT_VSP_BSSID_STATS_SET,
                                             &wssIfPMDB) != OSIX_SUCCESS)
                    {
                        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                                     "WSS_PM_WTP_EVT_VSP_BSSID_STATS_SET Failed\r\n");
                    }

                    wssIfPMDB.tWtpVSPBSSIDStats.wlanBSSIDBeaconsSentCount =
                        (wssIfPMDB.tWtpVSPBSSIDStats.wlanBSSIDBeaconsSentCount -
                         wssIfPMBSSIDDB.tWtpVSPBSSIDStats.
                         wlanBSSIDBeaconsSentCount);
                    wssIfPMDB.tWtpVSPBSSIDStats.wlanBSSIDProbeReqRcvdCount =
                        (wssIfPMDB.tWtpVSPBSSIDStats.
                         wlanBSSIDProbeReqRcvdCount -
                         wssIfPMBSSIDDB.tWtpVSPBSSIDStats.
                         wlanBSSIDProbeReqRcvdCount);
                    wssIfPMDB.tWtpVSPBSSIDStats.wlanBSSIDProbeRespSentCount =
                        (wssIfPMDB.tWtpVSPBSSIDStats.
                         wlanBSSIDProbeRespSentCount -
                         wssIfPMBSSIDDB.tWtpVSPBSSIDStats.
                         wlanBSSIDProbeRespSentCount);

                    wssWlanDB.WssWlanAttributeDB.u4WlanIfIndex =
                        WssWlanDB.WssWlanAttributeDB.u4WlanIfIndex;
                    wssWlanDB.WssWlanIsPresentDB.bWlanProfileId = OSIX_TRUE;

                    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY,
                                                  &wssWlanDB) != OSIX_SUCCESS)
                    {
                        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                                     "WSS_WLAN_GET_IFINDEX_ENTRY Failed\r\n");
                    }
                    wssIfPMDB.tWtpPmAttState.bWssPmVSPSSIDStatsSet = OSIX_TRUE;
                    wssIfPMDB.tWtpVSPBSSIDStats.isOptional = OSIX_FALSE;
                    wssIfPMDB.u4FsDot11WlanProfileId =
                        (UINT4) wssWlanDB.WssWlanAttributeDB.u2WlanProfileId;

                    if (WssIfProcessPMDBMsg (WSS_PM_WTP_EVT_VSP_SSID_STATS_SET,
                                             &wssIfPMDB) == OSIX_SUCCESS)
                    {
                        continue;
                    }
                }

                if (WssIfProcessPMDBMsg
                    (WSS_PM_WTP_EVT_VSP_AP_STATS_GET,
                     &wssIfPMDB) == OSIX_SUCCESS)
                {
                    u4PrevSentBytes = wssIfPMDB.ApElement.u4SentBytes;
                    u4PrevRcvdBytes = wssIfPMDB.ApElement.u4RcvdBytes;
                }

                MEMSET (&wssIfPMDB.ApElement, 0, sizeof (tApParams));
                CAPWAP_GET_4BYTE (wssIfPMDB.ApElement.u4GatewayIp, pRcvBuf);
                CAPWAP_GET_4BYTE (wssIfPMDB.ApElement.u4Subnetmask, pRcvBuf);
                CAPWAP_GET_4BYTE (wssIfPMDB.ApElement.u4SentBytes, pRcvBuf);
                CAPWAP_GET_4BYTE (wssIfPMDB.ApElement.u4RcvdBytes, pRcvBuf);
                CAPWAP_GET_4BYTE (wssIfPMDB.ApElement.u4SentTrafficRate,
                                  pRcvBuf);
                CAPWAP_GET_4BYTE (wssIfPMDB.ApElement.u4RcvdTrafficRate,
                                  pRcvBuf);
                CAPWAP_GET_1BYTE (wssIfPMDB.ApElement.u1TotalInterfaces,
                                  pRcvBuf);
                if (u4PrevSentBytes > 0)
                {
                    wssIfPMDB.ApElement.u4SentTrafficRate =
                        ((wssIfPMDB.ApElement.u4SentBytes -
                          u4PrevSentBytes) * 8) / 120;
                }
                if (u4PrevRcvdBytes > 0)
                {
                    wssIfPMDB.ApElement.u4RcvdTrafficRate =
                        ((wssIfPMDB.ApElement.u4RcvdBytes -
                          u4PrevRcvdBytes) * 8) / 120;
                }

                MEMSET (&wssIfPMDB.tWtpPmAttState, 0,
                        sizeof (tWssPmAttributeDB));
                wssIfPMDB.tWtpPmAttState.bWssPmVSPApStatsSet = OSIX_TRUE;
                wssIfPMDB.u2ProfileId = cwMsg->u2IntProfileId;
                if (gu4WlcIpAddr == 0)
                {
                    gu4WlcIpAddr = wssIfPMDB.ApElement.u4GatewayIp;
#ifdef KERNEL_CAPWAP_WANTED
                    CapwapUpdateKernelWlcIpaddr (gu4WlcIpAddr);
#endif
                }
                if (WssIfProcessPMDBMsg
                    (WSS_PM_WTP_EVT_VSP_AP_STATS_SET,
                     &wssIfPMDB) != OSIX_SUCCESS)
                {
                }

                CAPWAP_GET_1BYTE (numRadioCount, pRcvBuf);
                for (u1index1 = 0; u1index1 < numRadioCount; u1index1++)
                {
                    MEMSET (&wssIfPMDB, 0, sizeof (tWssIfPMDB));
                    CAPWAP_GET_4BYTE (wssIfPMDB.radioClientStats.u4IfInOctets,
                                      pRcvBuf);
                    CAPWAP_GET_4BYTE (wssIfPMDB.radioClientStats.u4IfOutOctets,
                                      pRcvBuf);

                    tWssIfCapDB        *pWssIfCapwapDB = NULL;
                    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
                        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
                    pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;
                    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
                        cwMsg->u2IntProfileId;
                    pWssIfCapwapDB->CapwapGetDB.u1RadioId =
                        (UINT1) (u1index1 + 1);
                    if (WssIfProcessCapwapDBMsg
                        (WSS_CAPWAP_GET_DB, pWssIfCapwapDB) != OSIX_SUCCESS)
                    {
                        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                                     "WLCHDLR : Getting Radio index failed\r\n");
                    }

                    MEMSET (&wssIfPMDB.tWtpPmAttState, 0,
                            sizeof (tWssPmAttributeDB));
                    wssIfPMDB.tWtpPmAttState.bWssPmVSPRadioStatsSet = OSIX_TRUE;
                    wssIfPMDB.u2ProfileId = cwMsg->u2IntProfileId;
                    wssIfPMDB.u4RadioIfIndex =
                        pWssIfCapwapDB->CapwapGetDB.u4IfIndex;
                    if (WssIfProcessPMDBMsg
                        (WSS_PM_WTP_EVT_VSP_RADIO_STATS_SET,
                         &wssIfPMDB) != OSIX_SUCCESS)
                    {
                    }
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                }

                CAPWAP_GET_1BYTE (numStaCount, pRcvBuf);
                for (u1index2 = 0; u1index2 < numStaCount; u1index2++)
                {
                    MEMSET (&wssIfPMDB, 0, sizeof (tWssIfPMDB));
                    CAPWAP_GET_NBYTE (wssIfPMDB.clientStats.StaMacAddr,
                                      pRcvBuf, MAC_ADDR_LEN);
                    MEMCPY (wssIfPMDB.FsWlanClientStatsMACAddress,
                            wssIfPMDB.clientStats.StaMacAddr, MAC_ADDR_LEN);
                    if (WssIfProcessPMDBMsg
                        (WSS_PM_WTP_EVT_VSP_CLIENT_STATS_GET,
                         &wssIfPMDB) == OSIX_SUCCESS)
                    {
                        u4ClientStatsBytesSentCount = wssIfPMDB.clientStats.
                            u4ClientStatsBytesSentCount;
                        u4ClientStatsBytesRecvdCount = wssIfPMDB.clientStats.
                            u4ClientStatsBytesRecvdCount;
                        u4StaIpAddr = wssIfPMDB.clientStats.u4StaIpAddr;
                    }

                    CAPWAP_GET_4BYTE (wssIfPMDB.clientStats.
                                      u4ClientStatsBytesSentCount, pRcvBuf);
                    CAPWAP_GET_4BYTE (wssIfPMDB.clientStats.
                                      u4ClientStatsBytesRecvdCount, pRcvBuf);

                    MEMSET (&wssIfPMDB.tWtpPmAttState, 0,
                            sizeof (tWssPmAttributeDB));
                    wssIfPMDB.tWtpPmAttState.bWssPmVSPClientStatsSet =
                        OSIX_TRUE;
                    wssIfPMDB.u2ProfileId = cwMsg->u2IntProfileId;

                    if (u4StaIpAddr == 0)
                    {
                        CAPWAP_GET_4BYTE (wssIfPMDB.clientStats.
                                          u4StaIpAddr, pRcvBuf);
                    }
                    else
                    {
                        CAPWAP_SKIP_N_BYTES (CAPWAP_MSG_ELEM_TYPE_LEN, pRcvBuf);
                        wssIfPMDB.clientStats.u4StaIpAddr = u4StaIpAddr;

                    }

                    if (WssIfProcessPMDBMsg
                        (WSS_PM_WTP_EVT_VSP_CLIENT_STATS_SET,
                         &wssIfPMDB) != OSIX_SUCCESS)
                    {
                    }

                    if (wssIfPMDB.clientStats.u4ClientStatsBytesSentCount <=
                        u4ClientStatsBytesSentCount)
                    {
                        u4ClientStatsBytesSentCount = wssIfPMDB.clientStats.
                            u4ClientStatsBytesSentCount;
                    }
                    else
                    {
                        u4ClientStatsBytesSentCount = (wssIfPMDB.clientStats.
                                                       u4ClientStatsBytesSentCount
                                                       -
                                                       u4ClientStatsBytesSentCount);
                    }
                    if (wssIfPMDB.clientStats.u4ClientStatsBytesRecvdCount <=
                        u4ClientStatsBytesRecvdCount)
                    {
                        u4ClientStatsBytesRecvdCount = wssIfPMDB.clientStats.
                            u4ClientStatsBytesRecvdCount;
                    }
                    else
                    {
                        u4ClientStatsBytesRecvdCount = (wssIfPMDB.clientStats.
                                                        u4ClientStatsBytesRecvdCount
                                                        -
                                                        u4ClientStatsBytesRecvdCount);
                    }
                    WssStaShowClient ();

                    for (u4Index = 0; u4Index < gu4ClientWalkIndex; u4Index++)
                    {
                        wssWlanDB.WssWlanAttributeDB.u4BssIfIndex =
                            gaWssClientSummary[u4Index].u4BssIfIndex;
                        wssWlanDB.WssWlanIsPresentDB.bWlanInternalId =
                            OSIX_TRUE;
                        wssWlanDB.WssWlanIsPresentDB.bWlanProfileId = OSIX_TRUE;
                        wssWlanDB.WssWlanIsPresentDB.bRadioIfIndex = OSIX_TRUE;

                        if (WssIfProcessWssWlanDBMsg
                            (WSS_WLAN_GET_BSS_IFINDEX_ENTRY,
                             &wssWlanDB) != OSIX_SUCCESS)
                        {
                            WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                                         "WSS_WLAN_GET_BSS_IFINDEX_ENTRY Failed\r\n");
                        }

                        if ((MEMCMP (wssIfPMDB.FsWlanClientStatsMACAddress,
                                     gaWssClientSummary[u4Index].staMacAddr,
                                     MAC_ADDR_LEN)) == 0)
                        {
                            wssIfPMClientDB.u2ProfileBSSID =
                                (UINT2) wssWlanDB.WssWlanAttributeDB.
                                u4BssIfIndex;

                            wssIfPMClientDB.tWtpVSPBSSIDStats.
                                wlanBSSIDDataPktRcvdCount =
                                u4ClientStatsBytesRecvdCount;
                            wssIfPMClientDB.tWtpVSPBSSIDStats.
                                wlanBSSIDDataPktSentCount =
                                u4ClientStatsBytesSentCount;

                            MEMSET (&wssIfPMClientDB.tWtpPmAttState, 0,
                                    sizeof (tWssPmAttributeDB));
                            wssIfPMClientDB.tWtpPmAttState.
                                bWssPmVSPBSSIDStatsSet = OSIX_TRUE;
                            wssIfPMClientDB.tWtpVSPBSSIDStats.isOptional =
                                OSIX_TRUE;
                            if (WssIfProcessPMDBMsg
                                (WSS_PM_WTP_EVT_VSP_BSSID_STATS_SET,
                                 &wssIfPMClientDB) != OSIX_SUCCESS)
                            {
                                WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                                             "WSS_PM_WTP_EVT_VSP_BSSID_STATS_SET Failed\r\n");
                            }

                            wssIfPMClientDB.tWtpPmAttState.
                                bWssPmVSPSSIDStatsSet = OSIX_TRUE;
                            wssIfPMClientDB.tWtpVSPBSSIDStats.isOptional =
                                OSIX_TRUE;
                            wssIfPMClientDB.u4FsDot11WlanProfileId =
                                (UINT4) wssWlanDB.WssWlanAttributeDB.
                                u2WlanProfileId;

                            if (WssIfProcessPMDBMsg
                                (WSS_PM_WTP_EVT_VSP_SSID_STATS_SET,
                                 &wssIfPMClientDB) != OSIX_SUCCESS)
                            {
                                WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                                             "WSS_PM_WTP_EVT_VSP_SSID_STATS_SET Failed\r\n");
                            }
                        }
                    }
                }
                break;

#ifdef RFMGMT_WANTED
            case VENDOR_NEIGHBOR_AP_TYPE:
            {
                /* Parse the received neighbor message */
                WSS_IF_DB_NEIGHBOR_AP_SCAN_ALLOC (VendorSpecPayload.
                                                  unVendorSpec.
                                                  VendNeighAP.ScanChannel);

                if (VendorSpecPayload.unVendorSpec.
                    VendNeighAP.ScanChannel == NULL)
                {
                    return OSIX_FAILURE;
                }

                VendorSpecPayload.unVendorSpec.VendNeighAP.
                    u2MsgEleType = elementType;

                CAPWAP_GET_2BYTE (VendorSpecPayload.unVendorSpec.
                                  VendNeighAP.u2MsgEleLen, pRcvBuf);
                CAPWAP_GET_4BYTE (VendorSpecPayload.unVendorSpec.
                                  VendNeighAP.u4VendorId, pRcvBuf);
                CAPWAP_GET_1BYTE (VendorSpecPayload.unVendorSpec.
                                  VendNeighAP.u1RadioId, pRcvBuf);
                CAPWAP_GET_4BYTE (VendorSpecPayload.unVendorSpec.
                                  VendNeighAP.u4Dot11RadioType, pRcvBuf);

                if ((VendorSpecPayload.unVendorSpec.VendNeighAP.
                     u4Dot11RadioType == RFMGMT_RADIO_TYPEA)
                    || (VendorSpecPayload.unVendorSpec.VendNeighAP.
                        u4Dot11RadioType == RFMGMT_RADIO_TYPEAN)
                    || (VendorSpecPayload.unVendorSpec.VendNeighAP.
                        u4Dot11RadioType == RFMGMT_RADIO_TYPEAC))
                {
                    u4MaxChannel = RFMGMT_MAX_CHANNELA;
                }
                else
                {
                    u4MaxChannel = RFMGMT_MAX_CHANNELB;
                }
                u1Index = 0;

                /* Parse the received packet till the max length is 
                 * reached */
                for (u2MsgLen = RFMGMT_NEIGHBOR_MSG_LEN;
                     u2MsgLen < VendorSpecPayload.
                     unVendorSpec.VendNeighAP.u2MsgEleLen; u1Index++)
                {
                    pTempScanChannel = &VendorSpecPayload.unVendorSpec.
                        VendNeighAP.ScanChannel[u1Index];

                    /* check for ScanChannel array out of bounds */
                    /*Validation for radio type a to be added. */
                    if (u1Index >= u4MaxChannel)
                    {
                        break;
                    }
                    /* Get the scanned channel number. If the value is
                     * non-zero then copy the value to the vendor payload
                     * structure  */

                    /* CAPWAP_GET_2BYTE (VendorSpecPayload.unVendorSpec.
                       VendNeighAP.ScanChannel[u1Index]. */

                    CAPWAP_GET_2BYTE (pTempScanChannel->
                                      u2ScannedChannel, pRcvBuf);

                    u2MsgLen = (UINT2) (u2MsgLen + sizeof (UINT2));
                    if (VendorSpecPayload.unVendorSpec.VendNeighAP.
                        ScanChannel[u1Index].u2ScannedChannel != 0)
                    {
                        CAPWAP_GET_1BYTE (pTempScanChannel->u1NeighborApCount,
                                          pRcvBuf);

                        u2MsgLen = (UINT2) (u2MsgLen + sizeof (UINT1));

                        for (u1NeighborApCount = 0; u1NeighborApCount <
                             pTempScanChannel->u1NeighborApCount;
                             u1NeighborApCount++)
                        {
                            /* For each entry, scanned channel, neighbor
                             * count and entry status will be present. Hence
                             * when more than a neighbor is present, skip
                             * scanned channel and neighbor AP count. */
                            if (u1NeighborApCount != 0)
                            {

                                CAPWAP_SKIP_N_BYTES (CAPWAP_MSG_ELEM_TYPE_LEN -
                                                     1, pRcvBuf);
                                u2MsgLen =
                                    (UINT2) (u2MsgLen +
                                             CAPWAP_MSG_ELEM_TYPE_LEN - 1);
                            }
                            if (u1NeighborApCount >= RFMGMT_MAX_NEIGHBOR_AP)
                            {
                                continue;
                            }
                            CAPWAP_GET_1BYTE (pTempScanChannel->
                                              u1EntryStatus[u1NeighborApCount],
                                              pRcvBuf);
                            CAPWAP_GET_NBYTE (pTempScanChannel->
                                              NeighborMacAddr
                                              [u1NeighborApCount], pRcvBuf,
                                              MAC_ADDR_LEN);
                            CAPWAP_GET_INT_2BYTE (pTempScanChannel->
                                                  i2Rssi[u1NeighborApCount],
                                                  pRcvBuf);
                            CAPWAP_GET_1BYTE (pTempScanChannel->u1NonGFPresent,
                                              pRcvBuf);
                            CAPWAP_GET_1BYTE (pTempScanChannel->
                                              u1OBSSNonGFPresent, pRcvBuf);
#ifdef ROGUEAP_WANTED
                            CAPWAP_GET_1BYTE (pTempScanChannel->
                                              isRogueApProcetionType
                                              [u1NeighborApCount], pRcvBuf);
                            CAPWAP_GET_1BYTE (pTempScanChannel->
                                              isRogueApStatus
                                              [u1NeighborApCount], pRcvBuf);
                            CAPWAP_GET_4BYTE (pTempScanChannel->
                                              u4RogueApLastReportedTime
                                              [u1NeighborApCount], pRcvBuf);
                            CAPWAP_GET_NBYTE (pTempScanChannel->
                                              au1ssid[u1NeighborApCount],
                                              pRcvBuf, WLAN_MAX_SSID_LEN);
                            CAPWAP_GET_NBYTE (pTempScanChannel->
                                              BSSIDRogueApLearntFrom
                                              [u1NeighborApCount], pRcvBuf,
                                              MAC_ADDR_LEN);
                            CAPWAP_GET_1BYTE (pTempScanChannel->
                                              u1StationCount[u1NeighborApCount],
                                              pRcvBuf);

                            CAPWAP_GET_NBYTE (pTempScanChannel->
                                              au1RfGroupID
                                              [u1NeighborApCount], pRcvBuf,
                                              RF_GROUP_ID_MAX_LEN);
#endif
                            u2MsgLen =
                                (UINT2) (u2MsgLen +
                                         RFMGMT_NEIGHBOR_COUNT_FIXED_LEN);

                        }
                    }
                }
                MEMCPY (&RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtPmEvtStatsReq.
                        VendorNeighborAp, &VendorSpecPayload.unVendorSpec.
                        VendNeighAP, sizeof (tVendorNeighborAp));

                RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtPmEvtStatsReq.
                    u2WtpInternalId = cwMsg->u2IntProfileId;
                RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtPmEvtStatsReq.
                    VendorNeighborAp.isOptional = OSIX_TRUE;

                if (WssIfProcessRfMgmtMsg (RFMGMT_WLC_RECV_NEIGHBOR_MSG,
                                           &RfMgmtMsgStruct) != OSIX_SUCCESS)
                {
                    WSS_IF_DB_NEIGHBOR_AP_SCAN_RELEASE (VendorSpecPayload.
                                                        unVendorSpec.
                                                        VendNeighAP.
                                                        ScanChannel);

                    WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                                 "Validate WTP event VSP : "
                                 "Failed to return BssIfIndex \r\n");
                    return OSIX_FAILURE;
                }
            }
                WSS_IF_DB_NEIGHBOR_AP_SCAN_RELEASE (VendorSpecPayload.
                                                    unVendorSpec.
                                                    VendNeighAP.ScanChannel);

                break;

            case VENDOR_CLIENT_SCAN_TYPE:
            {
                VendorSpecPayload.unVendorSpec.VendClientScan.u2MsgEleType =
                    elementType;
                /* Parse the received client scan message */
                CAPWAP_GET_2BYTE (VendorSpecPayload.unVendorSpec.
                                  VendClientScan.u2MsgEleLen, pRcvBuf);
                CAPWAP_GET_4BYTE (VendorSpecPayload.unVendorSpec.
                                  VendClientScan.u4VendorId, pRcvBuf);
                CAPWAP_GET_1BYTE (VendorSpecPayload.unVendorSpec.
                                  VendClientScan.u1RadioId, pRcvBuf);
                CAPWAP_GET_1BYTE (VendorSpecPayload.unVendorSpec.VendClientScan.
                                  u1ClientCount, pRcvBuf);

                /* Parse the received packet till the max length is 
                 * reached */
                for (u2MsgLen = RFMGMT_CLIENT_SCAN_MSG_LEN;
                     u2MsgLen < VendorSpecPayload.
                     unVendorSpec.VendClientScan.u2MsgEleLen;)
                {
                    /* Get the scanned channel number. If the value is
                     * non-zero then copy the value to the vendor payload
                     * structure  */

                    for (u1ClientCount = 0; u1ClientCount <
                         VendorSpecPayload.unVendorSpec.
                         VendClientScan.u1ClientCount; u1ClientCount++)
                    {
                        CAPWAP_GET_1BYTE (VendorSpecPayload.unVendorSpec.
                                          VendClientScan.
                                          u1EntryStatus[u1ClientCount],
                                          pRcvBuf);
                        CAPWAP_GET_NBYTE (VendorSpecPayload.unVendorSpec.
                                          VendClientScan.
                                          ClientMacAddress[u1ClientCount],
                                          pRcvBuf, MAC_ADDR_LEN);

                        CAPWAP_GET_INT_2BYTE (VendorSpecPayload.unVendorSpec.
                                              VendClientScan.
                                              i2ClientSNR[u1ClientCount],
                                              pRcvBuf);

                        u2MsgLen =
                            (UINT2) (u2MsgLen + RFMGMT_CLIENT_SCAN_FIXED_LEN);

                    }
                }
                MEMCPY (&RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtPmEvtStatsReq.
                        VendorClientScan, &VendorSpecPayload.unVendorSpec.
                        VendClientScan, sizeof (tVendorClientScan));

                RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtPmEvtStatsReq.
                    u2WtpInternalId = cwMsg->u2IntProfileId;
                RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtPmEvtStatsReq.
                    VendorClientScan.isOptional = OSIX_TRUE;

                if (WssIfProcessRfMgmtMsg (RFMGMT_WLC_RECV_CLIENT_MSG,
                                           &RfMgmtMsgStruct) != OSIX_SUCCESS)
                {
                    WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                                 "Validate WTP event VSP : "
                                 "Failed to return BssIfIndex \r\n");
                    return OSIX_FAILURE;
                }
            }

                break;
#endif
            case VENDOR_MULTIDOMAIN_INFO_TYPE:
                RadioIfMsgStruct.unRadioIfMsg.RadioIfPmWtpEventReq.
                    VendorMultiDomainCap.u2MessageType = elementType;

                /* Parse the received multi domain info message */
                CAPWAP_GET_2BYTE (RadioIfMsgStruct.unRadioIfMsg.
                                  RadioIfPmWtpEventReq.VendorMultiDomainCap.
                                  u2MessageLength, pRcvBuf);
                CAPWAP_GET_4BYTE (RadioIfMsgStruct.unRadioIfMsg.
                                  RadioIfPmWtpEventReq.VendorMultiDomainCap.
                                  u4VendorId, pRcvBuf);
                CapwapConfigNoOfRadio (cwMsg->u2IntProfileId, &u2NumRadioElems);
                for (u1Radioid = 1; u1Radioid <= u2NumRadioElems; u1Radioid++)
                {
                    CAPWAP_GET_1BYTE (RadioIfMsgStruct.unRadioIfMsg.
                                      RadioIfPmWtpEventReq.VendorMultiDomainCap.
                                      u1RadioId, pRcvBuf);
                    CAPWAP_GET_2BYTE (RadioIfMsgStruct.unRadioIfMsg.
                                      RadioIfPmWtpEventReq.VendorMultiDomainCap.
                                      u2MsgLength, pRcvBuf);
                    u2MsgLength =
                        RadioIfMsgStruct.unRadioIfMsg.RadioIfPmWtpEventReq.
                        VendorMultiDomainCap.u2MsgLength;
                    u2Length = 0;
                    for (u1MultiDomainIndex = 1; u1MultiDomainIndex <=
                         MAX_MULTIDOMAIN_INFO_ELEMENT; u1MultiDomainIndex++)
                    {
                        if (u2Length >= u2MsgLength)
                        {
                            break;
                        }
                        else
                        {
                            CAPWAP_GET_2BYTE (RadioIfMsgStruct.
                                              unRadioIfMsg.RadioIfPmWtpEventReq.
                                              VendorMultiDomainCap.
                                              u2FirstChannel, pRcvBuf);
                            CAPWAP_GET_2BYTE (RadioIfMsgStruct.unRadioIfMsg.
                                              RadioIfPmWtpEventReq.
                                              VendorMultiDomainCap.
                                              u2NumOfChannels, pRcvBuf);
                            CAPWAP_GET_2BYTE (RadioIfMsgStruct.unRadioIfMsg.
                                              RadioIfPmWtpEventReq.
                                              VendorMultiDomainCap.
                                              u2MaxTxPowerLevel, pRcvBuf);
                            u2Length =
                                (UINT2) (u2Length +
                                         RADIOIF_MULTI_DOMAIN_BASE_INDEX);

                            RadioIfMsgStruct.unRadioIfMsg.RadioIfPmWtpEventReq.
                                VendorMultiDomainCap.u1MultiDomainIndex =
                                u1MultiDomainIndex;
                            RadioIfMsgStruct.unRadioIfMsg.RadioIfPmWtpEventReq.
                                u2WtpInternalId = cwMsg->u2IntProfileId;
                            RadioIfMsgStruct.unRadioIfMsg.RadioIfPmWtpEventReq.
                                VendorMultiDomainCap.isPresent = OSIX_TRUE;
                            if (WssIfProcessRadioIfMsg
                                (WSS_RADIOIF_PM_WTP_EVENT_REQ,
                                 &RadioIfMsgStruct) != OSIX_SUCCESS)
                            {
                                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                            "Failed to Set Multi"
                                            "Domain Capability in Radio IF \r\n");
                                continue;
                            }

                        }
                    }
                }
                break;
#ifdef WPS_WANTED
            case VENDOR_WPS_TYPE:
                WssWpsNotifyParams.eWssWpsNotifyType = WSSWPS_PBC_IND;
                CAPWAP_GET_1BYTE (WssWpsNotifyParams.WPSPBCIND.WlanId, pRcvBuf);
                CAPWAP_GET_2BYTE (WssWpsNotifyParams.WPSPBCIND.u2WtpInternalId,
                                  pRcvBuf);
                if (WssIfProcessWpsMsg (WSSWPS_PBC_IND, &WssWpsNotifyParams) !=
                    OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "Configuration Status Request "
                                "failure: CH_SWITCH_MSG Vendor Type\r\n");
                    return OSIX_FAILURE;
                }
                break;
#endif
            case VENDOR_DFS_RADAR_STATS:
            {
                VendorSpecPayload.unVendorSpec.DFSRadarEventInfo.u2MsgEleType =
                    elementType;
                CAPWAP_GET_2BYTE (VendorSpecPayload.unVendorSpec.
                                  DFSRadarEventInfo.u2MsgEleLen, pRcvBuf);
                CAPWAP_GET_2BYTE (VendorSpecPayload.unVendorSpec.
                                  DFSRadarEventInfo.u2ChannelNum, pRcvBuf);
                CAPWAP_GET_1BYTE (VendorSpecPayload.unVendorSpec.
                                  DFSRadarEventInfo.u1RadioId, pRcvBuf);
                CAPWAP_GET_1BYTE (VendorSpecPayload.unVendorSpec.
                                  DFSRadarEventInfo.isRadarFound, pRcvBuf);
                MEMCPY (&RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtPmEvtStatsReq.
                        DFSRadarEventInfo,
                        &VendorSpecPayload.unVendorSpec.DFSRadarEventInfo,
                        sizeof (tVendorInfoDFSRadarEventInfo));

                RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtPmEvtStatsReq.
                    u2WtpInternalId = cwMsg->u2IntProfileId;
                RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtPmEvtStatsReq.
                    DFSRadarEventInfo.isOptional = OSIX_TRUE;

                if (WssIfProcessRfMgmtMsg (RFMGMT_WLC_RECV_RADAREVENT_MSG,
                                           &RfMgmtMsgStruct) != OSIX_SUCCESS)
                {
                    WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                                 "Failed to send radarevent \r\n");
                    return OSIX_FAILURE;
                }
            }
                break;
            default:
                break;
        }
    }

    WLCHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapValidateWtpEventReqMsgElems                    */
/*  Description     : The function validates the functional             */
/*                    characteristics of the received WLCHDLR            */
/*                    Wtp Event request agains the configured           */
/*                    WTP profile                                       */
/*  Input(s)        : WtpEveReqMsg - parsed WLCHDLR Wtp Event request    */
/*                    pRcvBuf - Received Wtp Event packet               */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
CapValidateWtpEventReqMsgElems (UINT1 *pRcvBuf,
                                tCapwapControlPacket * pWtpEveReqMsg)
{
    tVendorSpecPayload  vendSpec;
    tWtpRebootStats     rebootStats;
    tDecryptErrReport   decryptErrReport;
    tWtpRadioStatsElement radioStats;
    tDupIPV4Addr        IPV4Addr;
    tDupIPV6Addr        IPV6Addr;
    UINT2               u2NumOptionalMsg = 0, u2MsgType = 0;
    UINT1               u1Index = 0;

    WLCHDLR_FN_ENTRY ();
    MEMSET (&vendSpec, 0, sizeof (tVendorSpecPayload));
    MEMSET (&rebootStats, 0, sizeof (tWtpRebootStats));
    MEMSET (&decryptErrReport, 0, sizeof (tDecryptErrReport));
    MEMSET (&radioStats, 0, sizeof (tWtpRadioStatsElement));
    MEMSET (&IPV4Addr, 0, sizeof (tDupIPV4Addr));
    MEMSET (&IPV6Addr, 0, sizeof (tDupIPV4Addr));

    /* Validate the Optional message elements */

    u2NumOptionalMsg = pWtpEveReqMsg->numOptionalElements;
    for (u1Index = 0; u1Index <= u2NumOptionalMsg; u1Index++)
    {
        /* Get the message element type */
        u2MsgType = pWtpEveReqMsg->capwapMsgElm
            [MAX_WTP_EVENT_REQ_MAND_MSG_ELEMENTS + u1Index].u2MsgType;
        switch (u2MsgType)
        {
            case DELETE_STATION:
                if (CapwapValidateWtpDeleteStation (pRcvBuf, pWtpEveReqMsg,
                                                    u1Index) == OSIX_FAILURE)
                {
                    return OSIX_FAILURE;
                }
                break;
            case VENDOR_SPECIFIC_PAYLOAD:
                if (CapwapValidateWTPEventVendSpecPld (pRcvBuf, pWtpEveReqMsg,
                                                       u1Index) == OSIX_FAILURE)
                {
                    return OSIX_FAILURE;
                }
                break;
            case WTP_REBOOT_STATISTICS:
                if (CapwapValidateWTPEventRebootStats (pRcvBuf, pWtpEveReqMsg,
                                                       u1Index) == OSIX_FAILURE)
                {
                    return OSIX_FAILURE;
                }
                break;
            case WTP_RADIO_STATISTICS:
                if (CapwapValidateRadioStats (pRcvBuf, pWtpEveReqMsg,
                                              u1Index) == OSIX_FAILURE)
                {
                    return OSIX_FAILURE;
                }
                break;
#if WLCHDLR_UNUSED_CODE
            case DECRYPTION_ERROR_REPORT_PERIOD:
                if (CapwapValidateDecryptErrReport (pRcvBuf, pWtpEveReqMsg,
                                                    u1Index) == OSIX_FAILURE)
                {
                    return OSIX_FAILURE;
                }
                break;
            case WTP_RADIO_STATISTICS:
                if (CapwapValidateRadioStats (pRcvBuf, pWtpEveReqMsg,
                                              u1Index) == OSIX_FAILURE)
                {
                    return OSIX_FAILURE;
                }
                break;
            case DUPLICATE_IPV4_ADDR:
                if (CapwapValidateDupIPV4Addr (pRcvBuf, pWtpEveReqMsg,
                                               u1Index) == OSIX_FAILURE)
                {
                    return OSIX_FAILURE;
                }
                break;
            case DUPLICATE_IPV6_ADDR:
                if (CapwapValidateDupIPV6Addr (pRcvBuf, pWtpEveReqMsg,
                                               u1Index) == OSIX_FAILURE)
                {
                    return OSIX_FAILURE;
                }
                break;
            case IEEE_MIC_COUNTERMEASURES:
                if (CapwapValidateDot11MicCountermeasures (pRcvBuf,
                                                           pWtpEveReqMsg,
                                                           u1Index)
                    == OSIX_FAILURE)
                {
                    return OSIX_FAILURE;
                }
                break;
            case IEEE_RSNA_ERROR_REPORT:
                if (CapwapValidateDot11RSNAErrReport (pRcvBuf, pWtpEveReqMsg,
                                                      u1Index) == OSIX_FAILURE)
                {
                    return OSIX_FAILURE;
                }
                break;
#endif
            case IEEE_STATISTICS:
                if (CapwapValidatetdot11Statistics (pRcvBuf, pWtpEveReqMsg,
                                                    u1Index) == OSIX_FAILURE)
                {
                    return OSIX_FAILURE;
                }
                break;
            default:
                break;
        }
    }

    /* Profile not configured by the operator
     * ncrement the error count*/
    WLCHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapValidateWtpDeleteStation                    */
/*  Description     : The function validates the WTP Event for          */
/*                    Delete Station.                                   */
/*  Input(s)        : pRcvBuf - Received Buffer                         */
/*                    cwMsg - Capwap Ctrl Msg                           */
/*                    u2MsgIndex - Message Index                        */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/
INT4
CapwapValidateWtpDeleteStation (UINT1 *pRcvBuf,
                                tCapwapControlPacket * cwMsg, UINT2 u2MsgIndex)
{
    UINT4               u4Offset = 0;
    UINT2               u2NumMsgElems = 0;
    tWtpProfile         WtpProfile;
    tWssStaMsgStruct    WssStaMsgStruct;
    tWlanSTATrapInfo    stationTrap;
    tWssStaWepProcessDB *pWssStaWepProcessDB = NULL;
    UINT4               u4ProfileIndex = 0;

    WLCHDLR_FN_ENTRY ();
    MEMSET (&WssStaMsgStruct, 0, sizeof (tWssStaMsgStruct));
    MEMSET (&WtpProfile, 0, sizeof (tWtpProfile));
    MEMSET (&stationTrap, 0, sizeof (tWlanSTATrapInfo));

    u2NumMsgElems = cwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;

    if (u2NumMsgElems > (UINT2) 1)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "The Message element Delete "
                    "Station Received More than once \r\n");
        return OSIX_FAILURE;
    }
    else
    {
        u4Offset = cwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[0];
        pRcvBuf += u4Offset;
        CAPWAP_GET_2BYTE (WssStaMsgStruct.unAuthMsg.AuthDelStation.
                          u2MessageType, pRcvBuf);
        CAPWAP_GET_2BYTE (WssStaMsgStruct.unAuthMsg.AuthDelStation.
                          u2MessageLength, pRcvBuf);
        CAPWAP_GET_1BYTE (WssStaMsgStruct.unAuthMsg.AuthDelStation.
                          u1RadioId, pRcvBuf);
        CAPWAP_GET_1BYTE (WssStaMsgStruct.unAuthMsg.AuthDelStation.
                          u1MacAddrLen, pRcvBuf);
        CAPWAP_GET_NBYTE (WssStaMsgStruct.unAuthMsg.AuthDelStation.
                          StaMacAddr, pRcvBuf, sizeof (tMacAddr));
        WssStaMsgStruct.unAuthMsg.AuthDelStation.isPresent = TRUE;

        /* STATION IDLE TIMEOUT TRAP */
        MEMCPY (stationTrap.au1CliMac,
                WssStaMsgStruct.unAuthMsg.AuthDelStation.StaMacAddr,
                MAC_ADDR_LEN);
        stationTrap.u4AssocStatus = WSS_WLAN_STA_DISCONNECTED;
        stationTrap.u4DisconnReason = WSS_STA_IDLE_TIMEOUT_TRAP;

        pWssStaWepProcessDB =
            WssStaProcessEntryGet (WssStaMsgStruct.unAuthMsg.AuthDelStation.
                                   StaMacAddr);

        if (pWssStaWepProcessDB == NULL)
        {
            return OSIX_FAILURE;
        }

        /* Retrivie the profile Index from the local database */
        if (WssIfGetProfileIfIndex (pWssStaWepProcessDB->u4BssIfIndex,
                                    &u4ProfileIndex) == OSIX_FAILURE)
        {
            return OSIX_FAILURE;
        }

        stationTrap.u4ifIndex = u4ProfileIndex;

        WlanSnmpifSendTrap (WSS_WLAN_DISASSOCIATE, &stationTrap);

        if (WssIfProcessWssStaMsg (WSS_STA_IDLE_TIMEOUT, &WssStaMsgStruct)
            == OSIX_FAILURE)
        {
            WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "CapwapValidateWtpDeleteStation"
                         ":: WSS_STA_IDLE_TIMEOUT Failed");
            return OSIX_FAILURE;
        }

    }
    WLCHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapValidateRadioStats                          */
/*  Description     : The function validates the WTP Event for          */
/*                    Radio Statistics.                                 */
/*  Input(s)        : pRcvBuf - Received Buffer                         */
/*                    cwMsg - Capwap Ctrl Msg                           */
/*                    u2MsgIndex - Message Index                        */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/
INT4
CapwapValidateRadioStats (UINT1 *pRcvBuf,
                          tCapwapControlPacket * cwMsg, UINT2 u2MsgIndex)
{
    UINT4               u4Offset = 0;
    UINT2               u2NumMsgElems = 0;
    tWssIfPMDB          wssIfPMDB;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;

    WLCHDLR_FN_ENTRY ();
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (&wssIfPMDB, 0, sizeof (tWssIfPMDB));

    u2NumMsgElems = cwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;

    if (u2NumMsgElems > 1)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "The Message element Radio "
                    "Statistics Received More than once \r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    else
    {
        u4Offset = cwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[0];
        pRcvBuf += u4Offset;
        CAPWAP_SKIP_N_BYTES (CAPWAP_MSG_ELEM_TYPE_LEN, pRcvBuf);
        CAPWAP_GET_1BYTE (wssIfPMDB.tRadioStatistics.u1RadioId, pRcvBuf);
        CAPWAP_GET_1BYTE (wssIfPMDB.tRadioStatistics.u1LastFailureType,
                          pRcvBuf);
        CAPWAP_GET_2BYTE (wssIfPMDB.tRadioStatistics.u2ResetCount, pRcvBuf);
        CAPWAP_GET_2BYTE (wssIfPMDB.tRadioStatistics.u2SwFailCount, pRcvBuf);
        CAPWAP_GET_2BYTE (wssIfPMDB.tRadioStatistics.u2HwFailCount, pRcvBuf);
        CAPWAP_GET_2BYTE (wssIfPMDB.tRadioStatistics.u2OtherFailureCount,
                          pRcvBuf);
        CAPWAP_GET_2BYTE (wssIfPMDB.tRadioStatistics.u2UnknownFailureCount,
                          pRcvBuf);
        CAPWAP_GET_2BYTE (wssIfPMDB.tRadioStatistics.u2ConfigUpdateCount,
                          pRcvBuf);
        CAPWAP_GET_2BYTE (wssIfPMDB.tRadioStatistics.u2ChannelChangeCount,
                          pRcvBuf);
        CAPWAP_GET_2BYTE (wssIfPMDB.tRadioStatistics.u2BandChangeCount,
                          pRcvBuf);
        CAPWAP_GET_2BYTE (wssIfPMDB.tRadioStatistics.u2CurrentNoiseFloor,
                          pRcvBuf);

        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

        pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;
        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = cwMsg->u2IntProfileId;
        pWssIfCapwapDB->CapwapGetDB.u1RadioId = wssIfPMDB.tRadioStatistics.
            u1RadioId;
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
            != OSIX_SUCCESS)
        {
            WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                         "WLCHDLR : Getting Radio index failed\r\n");
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }

        wssIfPMDB.tWtpPmAttState.bWssPmRadStatsSet = TRUE;
        wssIfPMDB.u2ProfileId = cwMsg->u2IntProfileId;
        wssIfPMDB.u4RadioIfIndex = pWssIfCapwapDB->CapwapGetDB.u4IfIndex;

        if (WssIfProcessPMDBMsg (WSS_PM_WTP_EVT_RADIO_STATS_SET, &wssIfPMDB)
            == OSIX_SUCCESS)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_SUCCESS;
        }
        else
        {
            WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "Failed to send the message "
                         "to PmTask queue\n");
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    WLCHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapValidatetdot11Statistics                    */
/*  Description     : The function validates the WTP Event for          */
/*                    Dot11 Statistics.                                 */
/*  Input(s)        : pRcvBuf - Received Buffer                         */
/*                    cwMsg - Capwap Ctrl Msg                           */
/*                    u2MsgIndex - Message Index                        */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/
INT4
CapwapValidatetdot11Statistics (UINT1 *pRcvBuf, tCapwapControlPacket * cwMsg,
                                UINT2 u2MsgIndex)
{
    UINT4               u4Offset = 0;
    UINT2               u2NumMsgElems = 0;
    tWssIfPMDB          wssIfPMDB;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;

    WLCHDLR_FN_ENTRY ();
    MEMSET (&wssIfPMDB, 0, sizeof (tWssIfPMDB));

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        u2NumMsgElems = cwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;
    if (u2NumMsgElems > 1)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "The Message element Reboot Statistics Received "
                    "More than once \r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    else
    {
        u4Offset = cwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[0];

        pRcvBuf += u4Offset;
        CAPWAP_SKIP_N_BYTES (CAPWAP_MSG_ELEM_TYPE_LEN, pRcvBuf);
        CAPWAP_GET_1BYTE (wssIfPMDB.tdot11Statistics.u1RadioId, pRcvBuf);
        CAPWAP_GET_4BYTE (wssIfPMDB.tdot11Statistics.u4TxFragmentCnt, pRcvBuf);
        CAPWAP_GET_4BYTE (wssIfPMDB.tdot11Statistics.u4MulticastTxCnt, pRcvBuf);
        CAPWAP_GET_4BYTE (wssIfPMDB.tdot11Statistics.u4FailedCount, pRcvBuf);
        CAPWAP_GET_4BYTE (wssIfPMDB.tdot11Statistics.u4RetryCount, pRcvBuf);
        CAPWAP_GET_4BYTE (wssIfPMDB.tdot11Statistics.u4MultipleRetryCount,
                          pRcvBuf);
        CAPWAP_GET_4BYTE (wssIfPMDB.tdot11Statistics.u4FrameDupCount, pRcvBuf);
        CAPWAP_GET_4BYTE (wssIfPMDB.tdot11Statistics.u4RTSSuccessCount,
                          pRcvBuf);
        CAPWAP_GET_4BYTE (wssIfPMDB.tdot11Statistics.u4RTSFailCount, pRcvBuf);
        CAPWAP_GET_4BYTE (wssIfPMDB.tdot11Statistics.u4ACKFailCount, pRcvBuf);
        CAPWAP_GET_4BYTE (wssIfPMDB.tdot11Statistics.u4RxFragmentCount,
                          pRcvBuf);
        CAPWAP_GET_4BYTE (wssIfPMDB.tdot11Statistics.u4MulticastRxCount,
                          pRcvBuf);
        CAPWAP_GET_4BYTE (wssIfPMDB.tdot11Statistics.u4FCSErrCount, pRcvBuf);
        CAPWAP_GET_4BYTE (wssIfPMDB.tdot11Statistics.u4TxFrameCount, pRcvBuf);
        CAPWAP_GET_4BYTE (wssIfPMDB.tdot11Statistics.u4DecryptionErr, pRcvBuf);
        CAPWAP_GET_4BYTE (wssIfPMDB.tdot11Statistics.u4DiscardQosFragmentCnt,
                          pRcvBuf);
        CAPWAP_GET_4BYTE (wssIfPMDB.tdot11Statistics.u4AssociatedStaCount,
                          pRcvBuf);
        CAPWAP_GET_4BYTE (wssIfPMDB.tdot11Statistics.u4QosCFPollsRecvdCount,
                          pRcvBuf);
        CAPWAP_GET_4BYTE (wssIfPMDB.tdot11Statistics.u4QosCFPollsUnusedCount,
                          pRcvBuf);
        CAPWAP_GET_4BYTE (wssIfPMDB.tdot11Statistics.u4QosCFPollsUnusableCount,
                          pRcvBuf);

        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

        pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;
        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = cwMsg->u2IntProfileId;
        pWssIfCapwapDB->CapwapGetDB.u1RadioId =
            wssIfPMDB.tdot11Statistics.u1RadioId;
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
            != OSIX_SUCCESS)
        {
            WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                         "WLCHDLR : Getting Radio index failed\r\n");
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }

        wssIfPMDB.u2ProfileId = cwMsg->u2IntProfileId;
        wssIfPMDB.u4RadioIfIndex = pWssIfCapwapDB->CapwapGetDB.u4IfIndex;
        wssIfPMDB.tWtpPmAttState.bWssPmIEEE80211StatsSet = TRUE;
        if (WssIfProcessPMDBMsg (WSS_PM_WTP_EVT_WTP_802_11_STATS_SET,
                                 &wssIfPMDB) == OSIX_SUCCESS)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_SUCCESS;
        }
        else
        {
            WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                         "CapwapValidateRebootStats: Failed to send Event \r\n");
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
    }
    WLCHDLR_FN_EXIT ();
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapValidateWTPEventRebootStats                 */
/*  Description     : The function validates the WTP Event for          */
/*                    Reboot Statistics.                                */
/*  Input(s)        : pRcvBuf - Received Buffer                         */
/*                    cwMsg - Capwap Ctrl Msg                           */
/*                    u2MsgIndex - Message Index                        */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/
INT4
CapwapValidateWTPEventRebootStats (UINT1 *pRcvBuf,
                                   tCapwapControlPacket * cwMsg,
                                   UINT2 u2MsgIndex)
{
    UINT4               u4Offset = 0;
    UINT2               u2NumMsgElems = 0;
    tWssIfPMDB          wssIfPMDB;

    WLCHDLR_FN_ENTRY ();
    MEMSET (&wssIfPMDB, 0, sizeof (tWssIfPMDB));

    u2NumMsgElems = cwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;
    if (u2NumMsgElems > 1)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "The Message element Reboot Statistics Received "
                    "More than once \r\n");
        return OSIX_FAILURE;
    }
    else
    {
        u4Offset = cwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[0];
        pRcvBuf += u4Offset;
        /* Copy the message type and length */
        CAPWAP_SKIP_N_BYTES (CAPWAP_MSG_ELEM_TYPE_LEN, pRcvBuf);
        CAPWAP_GET_2BYTE (wssIfPMDB.tWtpRebootStatistics.u2RebootCount,
                          pRcvBuf);
        CAPWAP_GET_2BYTE (wssIfPMDB.tWtpRebootStatistics.u2AcInitiatedCount,
                          pRcvBuf);
        CAPWAP_GET_2BYTE (wssIfPMDB.tWtpRebootStatistics.u2LinkFailCount,
                          pRcvBuf);
        CAPWAP_GET_2BYTE (wssIfPMDB.tWtpRebootStatistics.u2SwFailCount,
                          pRcvBuf);
        CAPWAP_GET_2BYTE (wssIfPMDB.tWtpRebootStatistics.u2HwFailCount,
                          pRcvBuf);
        CAPWAP_GET_2BYTE (wssIfPMDB.tWtpRebootStatistics.u2OtherFailCount,
                          pRcvBuf);
        CAPWAP_GET_2BYTE (wssIfPMDB.tWtpRebootStatistics.u2UnknownFailCount,
                          pRcvBuf);
        CAPWAP_GET_1BYTE (wssIfPMDB.tWtpRebootStatistics.u1LastFailureType,
                          pRcvBuf);
        wssIfPMDB.tWtpPmAttState.bWssPmRbtStatsSet = TRUE;
        wssIfPMDB.u2ProfileId = cwMsg->u2IntProfileId;
        if (WssIfProcessPMDBMsg (WSS_PM_WTP_EVT_REBOOT_STATS_SET, &wssIfPMDB)
            == OSIX_SUCCESS)
        {
            return OSIX_SUCCESS;
        }
        else
        {
            return OSIX_FAILURE;
        }
    }
    WLCHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapGetDataTransferMode                         */
/*  Description     : The function is unused in WLC                     */
/*  Input(s)        : None                                              */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/
INT4
CapwapGetDataTransferMode (tDataTransferMode * datatransfermode, UINT4 *pMsgLen)
{
    UNUSED_PARAM (datatransfermode);
    UNUSED_PARAM (pMsgLen);
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapGetDataTransferData                         */
/*  Description     : The function is unused in WLC                     */
/*  Input(s)        : None                                              */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/
INT4
CapwapGetDataTransferData (tDataTransferData * datatransferdata, UINT4 *pMsgLen)
{
    UNUSED_PARAM (datatransferdata);
    UNUSED_PARAM (pMsgLen);
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapGetDecryptErrReport                         */
/*  Description     : The function is unused in WLC                     */
/*  Input(s)        : None                                              */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/
INT4
CapwapGetDecryptErrReport (tDecryptErrReport * pDecryErr, UINT4 *pMsgLen)
{
    UNUSED_PARAM (pDecryErr);
    UNUSED_PARAM (pMsgLen);
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapProcessWtpEventResponse                     */
/*  Description     : The function is unused in WLC                     */
/*  Input(s)        : None                                              */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/
INT4
CapwapProcessWtpEventResponse (tSegment * pBuf,
                               tCapwapControlPacket * pWtpEveRespMsg,
                               tRemoteSessionManager * pSessEntry)
{
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pWtpEveRespMsg);
    UNUSED_PARAM (pSessEntry);
    return OSIX_FAILURE;
}

#endif
