/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: wlchdlrreset.c,v 1.4 2018/01/22 09:39:53 siva Exp $
 *
 * Description: This file contains the WLCHDLR Reset Related routines.
 *
 *****************************************************************************/
#ifndef __WLCHDLR_RESET_C__
#define __WLCHDLR_RESET_C__

#include "wlchdlrinc.h"

/*****************************************************************************
 * Function     : CapwapConstructResetRequest                                *
 *                                                                           *
 * Description  : This function constructs the Reset request packet          *
 *                including capwap header,control header and message element.*
 *                                                                           *
 * Input        : presetReq - Reset Req Structure Pointer                    *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
CapwapConstructResetRequest (tResetReq * presetReq)
{
    UINT4               u4MsgLen = 0;

    WLCHDLR_FN_ENTRY ();

    /* update WLCHDLR Header */
    if (CapwapConstructCpHeader (&presetReq->capwapHdr) == OSIX_FAILURE)
    {
        WLCHDLR_TRC (ALL_FAILURE_TRC, "capwapResetReq:Failed to construct "
                     "the capwap Header\n");
        return OSIX_FAILURE;
    }

    /* update Manadatory Messege elements */
    if (CapwapGetWLCImageId (&presetReq->imageId, &u4MsgLen) == OSIX_FAILURE)

    {
        WLCHDLR_TRC (ALL_FAILURE_TRC,
                     "Failed to get Mandatory Message Elements \n");
        return OSIX_FAILURE;
    }

    /* update Optional Messege elements */

    if (CapwapGetVendorPayload (&presetReq->vendSpec, &u4MsgLen)
        == OSIX_FAILURE)

    {
        WLCHDLR_TRC (ALL_FAILURE_TRC,
                     "Failed to get Optional Message Elements \n");
        return OSIX_FAILURE;
    }

    /* update Control Header */
    presetReq->u2CapwapMsgElemenLen = (UINT2) (u4MsgLen +
                                               CAPWAP_MSG_ELEM_PLUS_SEQ_LEN);
    presetReq->u1NumMsgBlocks = 0;    /* flags */

    WLCHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapProcessResetResponse                        */
/*  Description     : The function processes the received WLCHDLR        */
/*                    reset response.                                   */
/*  Input(s)        : resetRespMsg- reset response packet received      */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
CapwapProcessResetResponse (tSegment * pBuf,
                            tCapwapControlPacket * presetRespMsg,
                            tRemoteSessionManager * pSessEntry)
{
    tWssIfPMDB          WssIfPMDB;

    UINT1              *pRcvBuf = NULL;
    UINT2               u2PacketLen = 0;
    UINT1               u1IsNotLinear = OSIX_FALSE;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;

    WLCHDLR_FN_ENTRY ();
    MEMSET (&WssIfPMDB, 0, sizeof (tWssIfPMDB));
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    if (pSessEntry == NULL)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                     "WLCHDLR Session Entry is NULL, return FAILURE \r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    if (WlcHdlrTmrStop (pSessEntry, WLCHDLR_RETRANSMIT_INTERVAL_TMR)
        == OSIX_FAILURE)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                     "Failed to stop the Retransmit Timer \r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    u2PacketLen = (UINT2) WLCHDLR_GET_BUF_LEN (pBuf);
    pRcvBuf = WLCHDLR_BUF_IF_LINEAR (pBuf, 0, u2PacketLen);
    if (pRcvBuf == NULL)
    {
        WLCHDLR_PKTBUF_ALLOC_MEM_BLOCK (pRcvBuf);
        /* Kloc Fix Start */
        if (pRcvBuf == NULL)
        {
            WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "Memory FAILURE \r\n");
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
        /* Kloc Fix Ends */
        WLCHDLR_COPY_FROM_BUF (pBuf, pRcvBuf, 0, u2PacketLen);
        u1IsNotLinear = OSIX_TRUE;
    }

    /* Validate the Reset  response message */
    if ((CapValidateResetRespMsgElems (pRcvBuf, presetRespMsg) == OSIX_FAILURE))
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "Error in validate msg elements\r\n");
        /* Kloc Fix Start */
        if (u1IsNotLinear == OSIX_TRUE)
        {
            WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pRcvBuf);
        }
        /* Kloc Fix Ends */
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        WssIfPMDB.u2ProfileId = pSessEntry->u2IntProfileId;
        WssIfPMDB.WssPmWLCCapwapStatsSetFlag.bResetStats = OSIX_TRUE;
        WssIfPMDB.WssPmWLCCapwapStatsSetFlag.bRunUpdateunsuccessfulProcessed =
            OSIX_TRUE;

        if (WssIfProcessPMDBMsg (WSS_PM_WLC_CAPWAP_RUN_STATS_SET,
                                 &WssIfPMDB) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to update PM db\r\n");
        }

        return OSIX_FAILURE;
    }

    WLCHDLR_TRC (WLCHDLR_MGMT_TRC, "Enter the CAPWAP DTLS TD State \r\n");
    pSessEntry->eCurrentState = CAPWAP_DTLSTD;
    if (CapwapShutDownDTLS (pSessEntry) == OSIX_FAILURE)
    {
        WLCHDLR_TRC (WLCHDLR_MGMT_TRC, "Failed to Disconnect the Session \r\n");
    }
    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = pSessEntry->u2IntProfileId;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpProfileId = OSIX_TRUE;
    if (WssIfProcessCapwapDBMsg
        (WSS_CAPWAP_GET_SESSPROFID_ENTRY, pWssIfCapwapDB) == OSIX_FAILURE)
    {
        CAPWAP_TRC1 (CAPWAP_FSM_TRC,
                     "Failed to Retrieve SESS Prof for WTP Prof: %d\r\n",
                     pSessEntry->u2IntProfileId);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    if (pWssIfCapwapDB->pSessProfileIdEntry != NULL)
    {
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_DELETE_SESSPROFID_ENTRY,
                                     pWssIfCapwapDB) == OSIX_FAILURE)
        {
            CAPWAP_TRC1 (CAPWAP_FSM_TRC,
                         "Failed to Delete SESS Prof for WTP Prof: %d\r\n",
                         pSessEntry->u2IntProfileId);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
    }

    pWssIfCapwapDB->u4DestIp = pSessEntry->remoteIpAddr.u4_addr[0];
    pWssIfCapwapDB->u4DestPort = pSessEntry->u4RemoteCtrlPort;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_DELETE_RSM, pWssIfCapwapDB)
        != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "WssIfProcessCapwapDBMsg failed \r\n");
        /* Kloc Fix Start */
        if (u1IsNotLinear == OSIX_TRUE)
        {
            WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pRcvBuf);
        }
        /* Kloc Fix Ends */
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    /* Kloc Fix Start */
    if (u1IsNotLinear == OSIX_TRUE)
    {
        WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pRcvBuf);
    }
    /* Kloc Fix Ends */
    MEMSET (&WssIfPMDB, 0, sizeof (tWssIfPMDB));
    WssIfPMDB.u2ProfileId = pSessEntry->u2IntProfileId;
    WssIfPMDB.WssPmWLCCapwapStatsSetFlag.bResetStats = OSIX_TRUE;
    WssIfPMDB.WssPmWLCCapwapStatsSetFlag.bRunUpdateRspReceived = OSIX_TRUE;

    if (WssIfProcessPMDBMsg (WSS_PM_WLC_CAPWAP_RUN_STATS_SET,
                             &WssIfPMDB) != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to update PM db\r\n");
    }

    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    WLCHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapValidateResetRespMsgElems                      */
/*  Description     : The function validates the functional             */
/*                    characteristics of the received WLCHDLR           */
/*                    reset request agains the configured               */
/*                    WTP profile                                       */
/*  Input(s)        : resetRespMsg - parsed WLCHDLR reset response      */
/*                    pRcvBuf - Received reset packet                   */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
CapValidateResetRespMsgElems (UINT1 *pRcvBuf,
                              tCapwapControlPacket * resetRespMsg)
{
    UNUSED_PARAM (pRcvBuf);
    UNUSED_PARAM (resetRespMsg);
    tResCode            resultCode;
    tVendorSpecPayload  vendSpec;

    WLCHDLR_FN_ENTRY ();

    MEMSET (&resultCode, 0, sizeof (tResCode));
    MEMSET (&vendSpec, 0, sizeof (tVendorSpecPayload));

    if (CapwapValidateResultCode (pRcvBuf, resetRespMsg, &resultCode,
                                  RESULT_CODE_RESET_RESP_INDEX) == OSIX_FAILURE)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, " Invalid Result code \r\n");
        return OSIX_FAILURE;
    }

    WLCHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapProcessResetRequest                         */
/*  Description     : The function is unused in WLC                     */
/*  Input(s)        : None                                              */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/
INT4
CapwapProcessResetRequest (tSegment * pBuf,
                           tCapwapControlPacket * presetRespMsg,
                           tRemoteSessionManager * pSessEntry)
{
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (presetRespMsg);
    UNUSED_PARAM (pSessEntry);
    return OSIX_SUCCESS;
}

#endif
