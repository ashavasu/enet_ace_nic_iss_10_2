/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: wlchdlrclearconfig.c,v 1.2 2018/01/22 09:39:53 siva Exp $
 *
 * Description: This file contains the WLCHDLR Clear config routines.
 *
 *****************************************************************************/
#ifndef _WLCHDLR_CLEAR_C_
#define _WLCHDLR_CLEAR_C_

#include "wlchdlrinc.h"

/*****************************************************************************
 * Function     : CapwapConstructClearConfigReq                              *
 *                                                                           *
 * Description  : This function constructs the Clear Config request packet   *
 *                including capwap header,control header and message element *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
CapwapConstructClearConfigReq (tClearconfigReq * pClearConfigReq)
{
    UINT4               u4MsgLen = 0;

    WLCHDLR_FN_ENTRY ();

    /* update CAPWAP Header */
    if (CapwapConstructCpHeader (&pClearConfigReq->capwapHdr) == OSIX_FAILURE)
    {
        WLCHDLR_TRC (ALL_FAILURE_TRC, "CapwapConstructClearConfigReq:"
                     "Failed to construct the capwap Header\n");
        return OSIX_FAILURE;
    }

    /* update Optional Messege elements */
#if WLCHDLR_UNUSED_CODE
    if (CapwapGetVendorPayload (&pClearConfigReq->vendSpec, &u4MsgLen)
        == OSIX_FAILURE)

    {
        WLCHDLR_TRC (ALL_FAILURE_TRC, "Failed to get Optional "
                     "Clear Config Request Message Elements \n");
        return OSIX_FAILURE;
    }
#endif

    /* update Control Header */
    pClearConfigReq->u2CapwapMsgElemenLen = (UINT2)
        (u4MsgLen + CAPWAP_MSG_ELEM_PLUS_SEQ_LEN);
    pClearConfigReq->u1NumMsgBlocks = 0;    /* flags */

    WLCHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapProcessClearConfigResp                      */
/*  Description     : The function processes the received CAPWAP        */
/*                    Clearconfig response.                             */
/*  Input(s)        : clearconfigRespMsg- Join response packet received */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
CapwapProcessClearConfigResp (tSegment * pBuf,
                              tCapwapControlPacket * pclearconfigRespMsg,
                              tRemoteSessionManager * pSessEntry)
{
    tWssIfPMDB          WssIfPMDB;
    tRadioIfGetDB       RadioIfGetDB;
    tWssWlanMsgStruct  *pWssWlanMsg = NULL;
    UINT1              *pRcvBuf = NULL;
    UINT2               u2PacketLen = 0;
    UINT1               u1IsNotLinear = OSIX_FALSE;
    UINT1               u1NumRadios = 0, u1index = 0;
    UINT1               u1BssIdCount = 0, u1IdCountIndex = 0;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tWssIfCapDB        *pWssIfCapwapDB1 = NULL;

    WLCHDLR_FN_ENTRY ();
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB1, OSIX_FAILURE)
        pWssWlanMsg = (tWssWlanMsgStruct *) (VOID *) UtlShMemAllocWssWlanBuf ();
    if (pWssWlanMsg == NULL)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                     "CapwapProcessClearConfigResp:- "
                     "UtlShMemAllocWssWlanBuf returned failure\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB1);
        return OSIX_FAILURE;
    }
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (pWssIfCapwapDB1, 0, sizeof (tWssIfCapDB));
    MEMSET (pWssWlanMsg, 0, sizeof (tWssWlanMsgStruct));

    if (pSessEntry == NULL)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "WLCHDLR Session Entry is NULL,"
                     " return FAILURE \r\n");
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsg);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB1);
        return OSIX_FAILURE;
    }
    if (pSessEntry->eCurrentState != CAPWAP_RUN)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "Session not in RUN state \r\n");
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsg);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB1);
        return OSIX_FAILURE;
    }
    /* Stop the retransmit interval timer */
    if (WlcHdlrTmrStop (pSessEntry, WLCHDLR_RETRANSMIT_INTERVAL_TMR)
        == OSIX_FAILURE)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                     "Failed to stop the Retransmit Timer \r\n");
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsg);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB1);
        return OSIX_FAILURE;
    }

    u2PacketLen = (UINT2) WLCHDLR_GET_BUF_LEN (pBuf);
    pRcvBuf = WLCHDLR_BUF_IF_LINEAR (pBuf, 0, u2PacketLen);
    if (pRcvBuf == NULL)
    {
        WLCHDLR_PKTBUF_ALLOC_MEM_BLOCK (pRcvBuf);
        WLCHDLR_COPY_FROM_BUF (pBuf, pRcvBuf, 0, u2PacketLen);
        u1IsNotLinear = OSIX_TRUE;
    }

    /* Validate the Clear Config response message */
    if ((CapValidateClearConfRespMsgElems (pRcvBuf, pclearconfigRespMsg)
         != OSIX_SUCCESS))
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC, "Validation of Clear Config "
                     "Response Failed \r\n");
        MEMSET (&WssIfPMDB, 0, sizeof (tWssIfPMDB));
        WssIfPMDB.u2ProfileId = pSessEntry->u2IntProfileId;
        WssIfPMDB.WssPmWLCCapwapStatsSetFlag.bClearConfigStats = OSIX_TRUE;
        WssIfPMDB.WssPmWLCCapwapStatsSetFlag.bRunUpdateunsuccessfulProcessed =
            OSIX_TRUE;

        if (WssIfProcessPMDBMsg (WSS_PM_WLC_CAPWAP_RUN_STATS_SET,
                                 &WssIfPMDB) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to update PM db\r\n");
        }

        /* Kloc Fix Start */
        if (u1IsNotLinear == OSIX_TRUE)
        {
            WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pRcvBuf);
        }
        /* Kloc Fix Ends */
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsg);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB1);
        return OSIX_FAILURE;
    }
    MEMSET (&WssIfPMDB, 0, sizeof (tWssIfPMDB));
    WssIfPMDB.u2ProfileId = pSessEntry->u2IntProfileId;
    WssIfPMDB.WssPmWLCCapwapStatsSetFlag.bClearConfigStats = OSIX_TRUE;
    WssIfPMDB.WssPmWLCCapwapStatsSetFlag.bRunUpdateRspReceived = OSIX_TRUE;

    if (WssIfProcessPMDBMsg (WSS_PM_WLC_CAPWAP_RUN_STATS_SET,
                             &WssIfPMDB) != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to update PM db\r\n");
    }

    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpProfileId = OSIX_TRUE;
    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
        pclearconfigRespMsg->u2IntProfileId;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bNoOfRadio = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bModelCheck = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpModelNumber = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bRadioAdminStatus = OSIX_TRUE;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
        == OSIX_SUCCESS)
    {
        u1NumRadios = pWssIfCapwapDB->CapwapGetDB.u1WtpNoofRadio;
        for (u1index = 0; u1index < u1NumRadios; u1index++)
        {
            /* If Radio Admin status is up */
            if (pWssIfCapwapDB->CapwapGetDB.au1WtpRadioAdminStatus[u1index] ==
                1)
            {
                /* Radio Virtual If Index */
                pWssIfCapwapDB1->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;
                pWssIfCapwapDB1->CapwapGetDB.u1RadioId = (UINT1) (u1index + 1);
                pWssIfCapwapDB1->CapwapGetDB.u2WtpInternalId =
                    pclearconfigRespMsg->u2IntProfileId;
                if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB1)
                    == OSIX_SUCCESS)
                {
                    RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
                        UtlShMemAllocAntennaSelectionBuf ();

                    if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection ==
                        NULL)
                    {
                        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                                     "\r%% Memory allocation for Antenna"
                                     " selection failed.\r\n");
                        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsg);
                        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB1);
                        return OSIX_FAILURE;
                    }
                    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
                        pWssIfCapwapDB1->CapwapGetDB.u4IfIndex;
                    RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
                    RadioIfGetDB.RadioIfIsGetAllDB.bBssIdCount = OSIX_TRUE;

                    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                                  (&RadioIfGetDB)) ==
                        OSIX_SUCCESS)
                    {
                        u1BssIdCount =
                            RadioIfGetDB.RadioIfGetAllDB.u1BssIdCount;
                        /* For every BSS ID count */
                        for (u1IdCountIndex = 1;
                             u1IdCountIndex <= u1BssIdCount; u1IdCountIndex++)
                        {
                            RadioIfGetDB.RadioIfGetAllDB.u1WlanId =
                                u1IdCountIndex;
                            RadioIfGetDB.RadioIfIsGetAllDB.bBssIfIndex =
                                OSIX_TRUE;

                            if (WssIfProcessRadioIfDBMsg
                                (WSS_GET_WLAN_BSS_IFINDEX_DB,
                                 (&RadioIfGetDB)) != OSIX_SUCCESS)
                            {
                                WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                                             "RadioIfConfigUpdateReq : Retreival of"
                                             " BSSIF Index Failed \r\n");
                                /* Kloc Fix Start */
                                if (u1IsNotLinear == OSIX_TRUE)
                                {
                                    WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pRcvBuf);
                                }
                                /* Kloc Fix Ends */
                                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                                 RadioIfGetAllDB.
                                                                 pu1AntennaSelection);
                                UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsg);
                                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB1);
                                return OSIX_FAILURE;
                            }

                            /* To Delete the Wlans */
                            pWssWlanMsg->unWssWlanMsg.WssWlanSetAdminOperStatus.
                                u4IfIndex = RadioIfGetDB.RadioIfGetAllDB.
                                u4BssIfIndex;
                            if (WssIfProcessWssWlanMsg (WSS_WLAN_CLEAR_CONFIG,
                                                        pWssWlanMsg) !=
                                OSIX_SUCCESS)
                            {
                                WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                                             "RadioIfDeleteInterface :DELETION of "
                                             "Wlan Failed \r\n");
                                /* Kloc Fix Start */
                                if (u1IsNotLinear == OSIX_TRUE)
                                {
                                    WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pRcvBuf);
                                }
                                /* Kloc Fix Ends */
                                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                                 RadioIfGetAllDB.
                                                                 pu1AntennaSelection);
                                UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsg);
                                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB1);
                                return OSIX_FAILURE;
                            }
                        }
                    }
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                }
            }
        }
    }
    /* Kloc Fix Start */
    if (u1IsNotLinear == OSIX_TRUE)
    {
        WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pRcvBuf);
    }
    /* Kloc Fix Ends */

    UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsg);
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB1);
    WLCHDLR_FN_EXIT ();

    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapValidateClearConfRespMsgElems                  */
/*  Description     : The function validates the functional             */
/*                    characteristics of the received CAPWAP            */
/*                    Clearconfig request agains the configured         */
/*                    WTP profile                                       */
/*  Input(s)        : discReqMsg - parsed CAPWAP Clearconfig request    */
/*                    pRcvBuf - Received clearconfig packet             */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
CapValidateClearConfRespMsgElems (UINT1 *pRcvBuf,
                                  tCapwapControlPacket * pclearconfigRespMsg)
{
    UNUSED_PARAM (pRcvBuf);
    UNUSED_PARAM (pclearconfigRespMsg);
    tResCode            resultCode;
    tVendorSpecPayload  vendSpec;

    WLCHDLR_FN_ENTRY ();
    MEMSET (&resultCode, 0, sizeof (tResCode));
    MEMSET (&vendSpec, 0, sizeof (tVendorSpecPayload));

    /* Validate the mandatory message elements */

    if (CapwapValidateResultCode (pRcvBuf, pclearconfigRespMsg, &resultCode,
                                  RESULT_CODE_CLEAR_CONFIG_RESP_INDEX) !=
        OSIX_SUCCESS)
    {
        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                     "CapValidateClearConfRespMsgElems :Validation Failed \r\n");
        return OSIX_FAILURE;
    }

#if WLCHDLR_UNUSED_CODE
    /* Validate the Optional message elements */

    if (CapwapValidateVendSpecPld (pRcvBuf, pclearconfigRespMsg, &vendSpec,
                                   VENDOR_SPECIFIC_PAYLOAD_CLEAR_CONFIG_RESP_INDEX)
        == OSIX_SUCCESS)
    {
        return OSIX_SUCCESS;
    }
#endif

    WLCHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapProcessClearConfigRequest                   */
/*  Description     : The function is unused in WLC.                    */
/*  Input(s)        : pBuf, pclearconfigRespMsg, pSessEntry             */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
CapwapProcessClearConfigRequest (tSegment * pBuf,
                                 tCapwapControlPacket * pclearconfigRespMsg,
                                 tRemoteSessionManager * pSessEntry)
{
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pclearconfigRespMsg);
    UNUSED_PARAM (pSessEntry);
    return OSIX_SUCCESS;
}

#endif
