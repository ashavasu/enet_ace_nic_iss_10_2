/************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved                 *
 * $Id: wlchdlrmacr.h,v 1.5 2018/01/22 09:39:52 siva Exp $                                                                     *
 * DESCRIPTION    : This file contains the Macros  used in              *
 *                  WLCHDLR module                                       *
 ************************************************************************/

#ifndef  __WLCHDLRMACR_H__
#define  __WLCHDLRMACR_H__

#define WLCHDLR_PKTBUF_POOLID\
    WLCHDLRMemPoolIds[MAX_WLCHDLR_PKTBUF_SIZING_ID]
#define WLCHDLR_DATA_TRANSFER_POOLID\
    WLCHDLRMemPoolIds[MAX_WLCHDLR_DATA_TRANSFER_PKTBUF_SIZING_ID]
#define WLCHDLR_QUEUE_POOLID\
    WLCHDLRMemPoolIds[MAX_WLCHDLR_PKT_QUE_SIZING_ID]
#define WLCHDLR_STA_MSG_POOLID\
    WLCHDLRMemPoolIds[MAX_WLCHDLR_STA_MSG_SIZING_ID]
#define WLCHDLR_STA_NUM_POOLID\
    WLCHDLRMemPoolIds[MAX_WLCHDLR_STA_NUM_SIZING_ID]

#define  WLCHDLR_PKTBUF_ALLOC_MEM_BLOCK(pu1Block)\
/*printf("+++%s===%d\n",__func__,__LINE__);*/\
         (pu1Block = \
          (UINT1 *)(MemAllocMemBlk (WLCHDLR_PKTBUF_POOLID)))

#define  WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK(pu1Block)\
/* printf("---%s===%d\n",__func__,__LINE__);*/\
          MemReleaseMemBlock (WLCHDLR_PKTBUF_POOLID, pu1Block)\

#define  WLCHDLR_DATA_TRANSFER_ALLOC_MEM_BLOCK(pu1Block)\
         (pu1Block = \
          (UINT1 *)(MemAllocMemBlk (WLCHDLR_DATA_TRANSFER_POOLID)))

#define WLCHDLR_GET_BUF_LEN(pBuf) \
        CRU_BUF_Get_ChainValidByteCount(pBuf)

#define WLCHDLR_BUF_IF_LINEAR(pBuf, u4OffSet, u4Size) \
        CRU_BUF_Get_DataPtr_IfLinear ((tCRU_BUF_CHAIN_HEADER *)(pBuf), \
                                      (u4OffSet), (u4Size))

#define WLCHDLR_COPY_FROM_BUF(pBuf, pu1Dst, u4Offset, u4Size) \
        CRU_BUF_Copy_FromBufChain ((pBuf), (UINT1 *)(pu1Dst), u4Offset, u4Size)

#define WLCHDLR_ALLOCATE_CRU_BUF(u4Size, u4Offset) \
        CRU_BUF_Allocate_MsgBufChain (u4Size, u4Offset)/*;\
 printf("+++%s===%d\n",__func__,__LINE__)*/

#define WLCHDLR_RELEASE_CRU_BUF(pBuf) \
/* printf("---%s===%d\n",__func__,__LINE__);*/\
        CRU_BUF_Release_MsgBufChain ((pBuf), 0)

#define WLCHDLR_COPY_TO_BUF(pBuf, pu1Src, u4Offset, u4Size) \
        CRU_BUF_Copy_OverBufChain ((pBuf), (UINT1 *)(pu1Src), u4Offset, u4Size)

#define WLCHDLR_UNUSED_CODE 0

#define WLCHDLR_QUEUE_DEPTH 30

#define WLCHDLR_MAX_PKT_LEN                   5000

#define MAX_WLCHDLR_PKTBUF                    50
#define WLCHDLR_Q_DEPTH                       100
#define MAX_WLCHDLR_PKT_QUE                   1000
#define MAX_WLCHDLR_STA_MSG        10
#define MAX_WLCHDLR_STA_NUM                   MAX_NUM_OF_STA_PER_WLC
#define WLCHDLR_MAX_DATA_TRANSFER_LEN         6000
#define MAX_WLCHDLR_DATA_TRANSFER_PKTBUF      10
#define WLCHDLR_VENDOR_SPEC_USERROLE_OFFSET   12

#define CRASHDUMP "/tmp/crashfiles"
#define MEMORYDUMP "/tmp/dumpfiles"

#define NO_AP_PRESENT               2
#define NO_NEED_TO_PROCESS          2
#define PROCESSED_BY_WSSSTA         3

#define SKIP_TO_HTTP               ( 34 - 47 )
/* This variable is added to the TCP header length having a HTTP packet 
 * added to it. By doing so we are moving it to the location of HTTP type*/

#define TCP_IPVER_MASK             0xf0
#define HTTP_DESTINATION_PORT      80
#define TCP_PROTOCOL_TYPE         0x06
#define UDP_PROTOCOL_TYPE         0x11
#define ICMP_PROTOCOL_TYPE         0x01
#define IP_ETHTYPE                 0x0800
#define ARP_ETHTYPE                 0x0806
#define BOOTP_SRV_PORT              0x0043 
#define BOOTP_CLIENT_PORT           0x0044
#define DNS_PORT                    0x0035 

#define SHIFT_2                      2
#define SHIFT_12                     12
#define SHIFT_20                     20
#define SHIFT_5                      5
#define SHIFT_8                     8
#define DEFAULT_STA_CONF_TMR_VALUE  1800

#define MAX_WEBAUTH_LIFETIME_EXP_TIMEOUT 60
#endif
