/******************************************************************* 
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 * $Id: wlchdlrsz.h,v 1.2 2017/05/23 14:16:54 siva Exp $
 * Description: This file contains WLCHDLR Sizing parameters
 *******************************************************************/


enum {
    MAX_WLCHDLR_DATA_TRANSFER_PKTBUF_SIZING_ID,
    MAX_WLCHDLR_PKTBUF_SIZING_ID,
    MAX_WLCHDLR_PKT_QUE_SIZING_ID,
    MAX_WLCHDLR_STA_MSG_SIZING_ID,
    MAX_WLCHDLR_STA_NUM_SIZING_ID,
    WLCHDLR_MAX_SIZING_ID
};


#ifdef  _WLCHDLRSZ_C
tMemPoolId WLCHDLRMemPoolIds[ WLCHDLR_MAX_SIZING_ID];
INT4  WlchdlrSizingMemCreateMemPools(VOID);
VOID  WlchdlrSizingMemDeleteMemPools(VOID);
INT4  WlchdlrSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _WLCHDLRSZ_C  */
extern tMemPoolId WLCHDLRMemPoolIds[ ];
extern INT4  WlchdlrSizingMemCreateMemPools(VOID);
extern VOID  WlchdlrSizingMemDeleteMemPools(VOID);
#endif /*  _WLCHDLRSZ_C  */


#ifdef  _WLCHDLRSZ_C
tFsModSizingParams FsWLCHDLRSizingParams [] = {
{ "UINT1[WLCHDLR_MAX_DATA_TRANSFER_LEN]", "MAX_WLCHDLR_DATA_TRANSFER_PKTBUF", sizeof(UINT1[WLCHDLR_MAX_DATA_TRANSFER_LEN]),MAX_WLCHDLR_DATA_TRANSFER_PKTBUF, MAX_WLCHDLR_DATA_TRANSFER_PKTBUF,0 },
{ "UINT1[WLCHDLR_MAX_PKT_LEN]", "MAX_WLCHDLR_PKTBUF", sizeof(UINT1[WLCHDLR_MAX_PKT_LEN]),MAX_WLCHDLR_PKTBUF, MAX_WLCHDLR_PKTBUF,0 },
{ "tWlcHdlrQueueReq", "MAX_WLCHDLR_PKT_QUE", sizeof(tWlcHdlrQueueReq),MAX_WLCHDLR_PKT_QUE, MAX_WLCHDLR_PKT_QUE,0 },
{ "tWssStaMsgStruct", "MAX_WLCHDLR_STA_MSG", sizeof(tWssStaMsgStruct),MAX_WLCHDLR_STA_MSG, MAX_WLCHDLR_STA_MSG,0 },
{ "tWlcHdlrStaConfigDB", "MAX_WLCHDLR_STA_NUM", sizeof(tWlcHdlrStaConfigDB),MAX_WLCHDLR_STA_NUM, MAX_WLCHDLR_STA_NUM,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _WLCHDLRSZ_C  */
extern tFsModSizingParams FsWLCHDLRSizingParams [];
#endif /*  _WLCHDLRSZ_C  */


