/************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved                 * 
 * $Id: wlchdlrport.h,v 1.1.1.1 2016/09/20 10:42:41 siva Exp $                                                                     *
 * DESCRIPTION    : This file contains the  definitions used for WLC    *
 *                  Handler  module                                     *
 ************************************************************************/

#ifndef  __WLCHDLR_PORT_H__
#define  __WLCHDLR_PORT_H__

enum { 
    /* Event processed by WLC handler */
    /* Name with Priority,Add MAC ACL Entry,Decryption Error Report Period */
    /* update the WTP profil, which sits in CPWAP module.  So it will initiate */
    /* configuraion updted rwequest */
    WSS_WLCHDLR_CAPWAP_CONF_UPDATE_REQ,
    /* AC Timestamp */
    WSS_WLCHDLR_CLKIWF_CONF_UPDATE_REQ,
    /* Radio Administrative State */
    WSS_WLCHDLR_RADIO_CONF_UPDATE_REQ,
    /* Statistics Timer */
    WSS_WLCHDLR_PM_CONF_UPDATE_REQ,
    /*  WTP Static IP Address Information */
    WSS_WLCHDLR_CFA_CONF_UPDATE_REQ,
    /* WLAN configuration request */
    WSS_WLCHDLR_WLAN_UPDATE_CONF_REQ,
    /* Add station or Delete Station */
    WSS_WLCHDLR_STATION_CONF_REQ,

    /* Event processed by AP HDLR */
    /* Change State Event Req */
    WSS_APHDLR_CHANGE_STATE_EVENT_REQ,
    /* Decrytion error report */
    WSS_APHDLR_MACHDLR_WTP_EVENT_REQ,
    /* Duplicate Ipv4 or Ipv6 Address*/
    WSS_APHDLR_ARP_WTP_EVENT_REQ,
    /* WTP reboot statistic and WTP radio statistics */
    WSS_APHDLR_PM_WTP_EVENT_REQ
        /* Delete Station */
        WSS_APHDLR_STAION_WTP_EVENT_REQ,

    /* Event processed by CAPWAP */
    /* WSS_CAPWAP_TX_CTRL_PKT, */

    CAPWAP_DTLS_PEER_AUTHORIZE,
    CAPWAP_DTLS_ESTABLISHED,
    CAPWAP_DTLS_ESTABLISHFAIL,
    CAPWAP_DTLS_AUTHENTICATE_FAIL,
    CAPWAP_DTLS_ABORTED,
    CAPWAP_DTLS_REASSEMBLY_FAIL,
    CAPWAP_DTLS_DECAP_FAIL,
    CAPWAP_DTLS_PEER_DISCONNECT,

};

typedef struct {
    /* defined in wlchdlrtdfs.h */
    tRadioIfConfigUpdateReq   RadioIfConfigUpdateReq;
    tCapwapConfigUpdateReq    CapwapConfigUpdateReq;
    tClkiwfConfigUpdateReq    ClkiwfConfigUpdateReq;
    tPmConfigUpdateReq        PmConfigUpdateReq;
    tCfaConfigUpdateReq       CfaConfigUpdateReq
        tStationConfReq           StationConfReq;
    tWssWlanConfigReq         WssWlanConfigReq;
    /* define in aphdlrtdfs.h */
    tRadioChangeStateEvtReq   RadioChangeStateEvtReq;
    tMacHdlrWtpEventReq       MacHdlrWtpEventReq;
    tArpWtpEventReq           ArpWtpEventReq;
    tPmWtpEventReq            PmWtpEventReq;
    tStationWtpEventReq       StationWtpEventReq;
    /* define in capwaptdfs.h */
    tCapwapDtlsParams         CapwapDtlsParams;
};

UINT1 WlcHdlrProcessWssIfMsg  PROTO ((UINT1 , tWlcHdlrProcessReq *));
UNIT4 WssIfProcessStaticNameinWlcHdlrMsg PROTO (tWssIfCapDB *);
UINT1 WssIfProcessWlcHdlrDBMsg  PROTO ((UINT1 , tWlcHdlrProcessDBReq *));

#endif

