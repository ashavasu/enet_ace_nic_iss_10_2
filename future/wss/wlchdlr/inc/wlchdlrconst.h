/************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved                 * 
 * $Id: wlchdlrconst.h,v 1.2 2017/05/23 14:16:54 siva Exp $
 *                                                                      *
 * DESCRIPTION    : This file contains the constants used for WLC       *
 *                  Handler  module                                     *
 ************************************************************************/
#if 0 /* Macros in this file is no more valid.  This shall be obsolete later */
#ifndef  __WLCHDLR_CONST_H__
#define  __WLCHDLR_CONST_H__

#define WLCHDLR_UNUSED_CODE 0

#define WLCHDLR_PKT_SIZE    1500
#define WLCHDLR_QUEUE_DEPTH 30

/* Reset response - Mandatory */
#define  RESULT_CODE_RESET_RESP_INDEX 0
#define  IMAGE_IDENTIFIER_RESET_RESP_INDEX 1

#define WLCHDLR_MAX_PKT_LEN                   1272
#define MAX_WLCHDLR_PKTBUF                    30
#define MAX_WLCHDLR_PKT_QUE                   30
#define MAX_WLCHDLR_STA_MSG        10

#define CRASHDUMP "/tmp/crashfiles"
#define MEMORYDUMP "/tmp/dumpfiles"

#define NO_AP_PRESENT               2

/* These Needs to put in capwapconst.h */
/* Config Update request - optional */
#define MAX_CONF_STATION_REQ_MAND_MSG_ELEMENTS 0
#define MAX_CONF_STATION_RESP_MAND_MSG_ELEMENTS 1
#define MAX_CLEAR_CONFIG_REQ_MAND_MSG_ELEMENTS 0
#define MAX_RESET_REQ_MAND_MSG_ELEMENTS 1
#define MAX_RESET_RESP_MAND_MSG_ELEMENTS 0
#define MAX_WTP_EVENT_REQ_MAND_MSG_ELEMENTS 0
#define MAX_WTP_EVENT_RESP_MAND_MSG_ELEMENTS 0
#define MAX_DATA_REQ_MAND_MSG_ELEMENTS 1
#define MAX_DATA_RESP_MAND_MSG_ELEMENTS 1
#define MAX_CONF_UPDATE_REQ_MAND_MSG_ELEMENTS 0
#define MAX_CONF_UPDATE_RESP_MAND_MSG_ELEMENTS 1 
#define MAX_CLEAR_CONFIG_RESP_MAND_MSG_ELEMENTS 1 
#define  VENDOR_SPECIFIC_PAYLOAD_CONFIG_UPDATE_REQ_INDEX 0
#define  LOCATION_DATA_CONFIG_UPDATE_REQ_INDEX 1
#define  WTP_NAME_CONFIG_UPDATE_REQ_INDEX 2
#define  IMAGE_ID_CONFIG_UPDATE_REQ_INDEX 3
#define  RADIO_ADMIN_STATE_CONFIG_UPDATE_REQ_INDEX 4
#define  CAPWAP_TIMER_CONFIG_UPDATE_REQ_INDEX 5
#define  DECRYPTION_REPORT_PERIOD_CONFIG_UPDATE_REQ_INDEX 6
#define  IDLE_TIMEOUT_CONFIG_UPDATE_REQ_INDEX 7
#define  WTP_FALLOUT_CONFIG_UPDATE_REQ_INDEX 8
#define  STATS_TIMER_CONFIG_UPDATE_REQ_INDEX 9
#define  ACNAME_PRIORITY_CONFIG_UPDATE_REQ_INDEX 10
#define  DELETE_MACENTRY_CONFIG_UPDATE_REQ_INDEX 11
#define  ADD_MACENTRY_CONFIG_UPDATE_REQ_INDEX 12
#define  WTP_STATIC_IPADDRESS_CONFIG_UPDATE_REQ_INDEX 13
#define  ACTIME_STAMP_CONFIG_UPDATE_REQ_INDEX 14

/* Config Update response - Mandatory */
#define  RESULT_CODE_CONF_UPDATE_RESP_INDEX 0

/* Config Update response - Optional */

/* Echo request - Mandatory */
#define  VENDOR_SPECIFIC_PAYLOAD_ECHO_REQ_INDEX 0

/* Echo response - Mandatory */
#define  VENDOR_SPECIFIC_PAYLOAD_DATA_RESP_INDEX 0

/* Reset request - Mandatory */
#define  IMAGE_IDENTIFIER_RESET_REQ_INDEX 0 
#define VENDOR_SPECIFIC_PAYLOAD_RESET_REQ_INDEX 1

/* Reset response - Mandatory */
#define  RESULT_CODE_RESET_RESP_INDEX 0  
#define  IMAGE_IDENTIFIER_RESET_RESP_INDEX 1

/* Config Station request - Mandatory */
#define  ADD_STATION_CONF_STATION_REQ_INDEX 0
#define  DELETE_STATION_CONF_STATION_REQ_INDEX 1
#define  VENDOR_SPECIFIC_PAYLOAD_CONF_STATION_REQ_INDEX 2

/* Config Station response - Mandatory */
#define  RESULT_CODE_CONF_STATION_RESP_INDEX 0
#define  VENDOR_SPECIFIC_CONF_STATION_RESP_INDEX 1

/* Data request - Mandatory */
#define  VENDOR_SPECIFIC_PAYLOAD_DATA_REQ_INDEX 1
#define  DATA_TRANSFER_MODE_REQ_INDEX 0
#define  DATA_TRANSFER_DATA_REQ_INDEX 0

/* Data response - Mandatory */
#define  VENDOR_SPECIFIC_PAYLOAD_DATA_RESP_INDEX 1
#define  RESULT_CODE_DATA_RESP_INDEX 0

/* Clear Config request - Mandatory */
#define  VENDOR_SPECIFIC_CLEAR_CONFIG_REQ_INDEX 0

/* Clear Config response - Mandatory */
#define  RESULT_CODE_CLEAR_CONFIG_RESP_INDEX 0
#define  VENDOR_SPECIFIC_PAYLOAD_CLEAR_CONFIG_RESP_INDEX 1

/* WTP Event request - Mandatory */
#define VENDOR_SPECIFIC_PAYLOAD_WTP_EVENT_REQ_INDEX 3
/* #define DELETE_STATION_WTP_EVENT_REQ_INDEX 1 */
#define WTP_REBOOT_STATS_WTP_EVENT_REQ_INDEX 1
#define DESC_ERR_REPORT_WTP_EVENT_REQ_INDEX 3
#define RADIO_STATS_WTP_EVENT_REQ_INDEX 0
#define DUP_IPV4_ADDR_WTP_EVENT_REQ_INDEX 5
#define DUP_IPV6_ADDR_WTP_EVENT_REQ_INDEX 3

/* WTP Event response - Mandatory */
#define VENDOR_SPECIFIC_WTP_EVENT_RESP_INDEX 0

#define WTP_RADIO_STATISTICS 50


/* THese Needs to go to capwapprot.h */


/**********Reset Response **********************/
/* Functions to parse clear Config status response and its message elements */
INT4
CapwapParseResetResponse(UINT1 *,  tCapwapControlPacket *, UINT2);

/**********Wtp Event Request **********************/
/* Functions to parse Wtp Event request and its message elements */
INT4
CapwapParseWtpEventRequest(UINT1 *,  tCapwapControlPacket *, UINT2);

/**********Wtp Event Response **********************/
/* Functions to parse Wtp Event request and its message elements */
INT4
capwapParseWtpEventResponse(UINT1 *,  tCapwapControlPacket *, UINT2);

/**********Data Transfer Request **********************/
/* Functions to parse Data Transfer request and its message elements */
INT4
CapwapParseDataTransferRequest(UINT1 *,  tCapwapControlPacket *, UINT2);

/**********Data Transfer Response **********************/
/* Functions to parse Data Transfer response and its message elements */
INT4
CapwapParseDataTransferResponse(UINT1 *,  tCapwapControlPacket *, UINT2);

/**********State Event Request **********************/
/* Functions to parse State Event request and its message elements */
INT4
capwapParseStateEventRequest(UINT1 *,  tCapwapControlPacket *, UINT2);

/**********State Event Response **********************/
/* Functions to parse State Event response and its message elements */
INT4
capwapParseStateEventResponse(UINT1 *,  tCapwapControlPacket *, UINT2);

/**********Configuaration Station request **********************/
/* Functions to parse Configuaration station request and its message elements */
INT4
CapwapParseConfStationReq(UINT1 *,  tCapwapControlPacket *, UINT2);

/**********Configuaration Station response **********************/
/* Functions to parse Configuaration station response and message elements */
INT4
CapwapParseConfStationResp(UINT1 *,  tCapwapControlPacket *, UINT2);

/**********Echo request **********************/
/* Functions to parse Echo Request and its message elements */
INT4
CapwapParseEchoRequest(UINT1 *,  tCapwapControlPacket *, UINT2);

/**********Echo response **********************/
/* Functions to parse Echo Response and its message elements */
INT4
CapwapParseEchoResponse(UINT1 *,  tCapwapControlPacket *, UINT2);

/**********Clear Config request **********************/
/* Functions to Clear Config Request and its message elements */
INT4
CapwapParseClearConfigRequest(UINT1 *,  tCapwapControlPacket *, UINT2);

/**********Reset Request **********************/
/* Functions to parse clear Config status response and its message elements */
INT4
CapwapParseResetRequest(UINT1 *,  tCapwapControlPacket *, UINT2);


/**********Clear Config Status response **********************/
/* Functions to parse clear Config status response and its message elements */
INT4
CapwapParseClearConfigResponse(UINT1 *,  tCapwapControlPacket *, UINT2);

/**********Configuration Update request **********************/
/* Functions to parse Config update request and its message elements */
INT4
CapwapParseConfUpdateReq(UINT1 *,  tCapwapControlPacket *, UINT2);

/**********Configuration Update response ***************/
/* Functions to parse Config update response and its message elements */
INT4
CapwapParseConfUpdateResp(UINT1 *,  tCapwapControlPacket *, UINT2);


INT4
capwapValidateImageId(UINT1 *,
                       tCapwapControlPacket *,
                       UINT2);
INT4
capwapValidateAcNamePriority(UINT1 *,
                       tCapwapControlPacket *,
                       UINT2);
INT4
capwapValidateAddMacEntry(UINT1 *,
                       tCapwapControlPacket *,
                       UINT2);
INT4
capwapValidateDeleteMacEntry(UINT1 *,
                       tCapwapControlPacket *,
                       UINT2);
INT4
capwapValidateWtpStaticIpAddress(UINT1 *,
                       tCapwapControlPacket *,
                       UINT2);
INT4
capwapValidateAcTimeStamp(UINT1 *,
                       tCapwapControlPacket *,
                       UINT2);
INT4
capwapValidateDataTransferMode(UINT1 *,
                       tCapwapControlPacket *,
                       UINT2);
INT4
capwapValidateDataTransferData(UINT1 *,
                       tCapwapControlPacket *,
                       UINT2);
INT4
capwapValidateDecryptErrReport(UINT1 *,
                       tCapwapControlPacket *,
                       UINT2);
INT4
capwapValidateRadioStats(UINT1 *,
                       tCapwapControlPacket *,
                       UINT2);
INT4
capwapValidateDupIPV4Addr(UINT1 *,
                       tCapwapControlPacket *,
                       UINT2);
INT4
capwapValidateDupIPV6Addr(UINT1 *,
                       tCapwapControlPacket *,
                       UINT2);

INT4 CapwapProcessConfigStationResp (tSegment *,tCapwapControlPacket *,
        tRemoteSessionManager  *);
INT4 CapwapProcessConfigUpdateRequest (tSegment *,tCapwapControlPacket *, 
        tRemoteSessionManager  *);
INT4 CapwapProcessConfigUpdateResp (tSegment *,tCapwapControlPacket *,
        tRemoteSessionManager  *);
INT4 CapValidateConfStatReqMsgElems(UINT1 *,tCapwapControlPacket *);
INT4 CapValidateConfStatRespMsgElems(UINT1 *,tCapwapControlPacket *);
INT4 CapValidateConfUpdateReqMsgElem(UINT1 *,tCapwapControlPacket *);
INT4 CapValidateConfUpdateRespMsgElem(UINT1 *,tCapwapControlPacket *);
INT4 CapValidateDataTransReqMsgElems (UINT1 *,tCapwapControlPacket *);
INT4 CapValidateDataTransRespMsgElems(UINT1 *,tCapwapControlPacket *);
INT4 CapwapProcessClearConfigRequest (tSegment *,tCapwapControlPacket *, 
        tRemoteSessionManager  *);
INT4 CapValidateClearConfReqMsgElems(UINT1 *,tCapwapControlPacket *);
INT4 CapValidateClearConfRespMsgElems(UINT1 *,tCapwapControlPacket *);
INT4 capwapProcessWtpEventRequest (tSegment *,tCapwapControlPacket *, 
        tRemoteSessionManager  *);
INT4 capwapProcessEchoRequest (tSegment *,tCapwapControlPacket *,UINT1);
INT4 CapwapProcessEchoResponse (tSegment *,tCapwapControlPacket *,UINT1);
INT4 CapwapValidateEchoReqMsgElems(UINT1 *,tCapwapControlPacket *, UINT2 *);
INT4 CapwapValidateEchoRespMsgElems(UINT1 *,tCapwapControlPacket *, UINT2 *);
INT4 CapValidateResetReqMsgElems (UINT1 *, tCapwapControlPacket *);
INT4 CapValidateResetRespMsgElems (UINT1 *, tCapwapControlPacket *);
INT4 CapValidateWtpEventReqMsgElems(UINT1 *,tCapwapControlPacket *);
INT4 CapValidateWtpEventRespMsgElems(UINT1 *,tCapwapControlPacket *);
VOID capwapAssembleAddstation(UINT1 *,tAddstation *);
VOID capwapAssembleDeletestation(UINT1 *,tDeletestation *);
VOID capwapAssembleDecryptionErrReport(UINT1 *, tDecryptErrReport *);
VOID capwapAssembleDuplicateIpv4Address(UINT1 *, tDupIPV4Addr *);
VOID capwapAssembleDuplicateIpv6Address(UINT1 *, tDupIPV6Addr *);
VOID capwapAssembleDataTransferMode(UINT1 *, tDataTransferMode *);
VOID capwapAssembleDataTransferData(UINT1 *, tDataTransferData *);
VOID capwapAssembleWtpRadioStatistics(UINT1 *, tWtpRadioStats *);
VOID capwapAssembleRadioOperationalState(UINT1 *, tRadioOperState *);

INT4 CapwapAssembleConfigStationReq PROTO ((UINT2, tStationConfReq *, UINT1 *));
INT4 CapwapAssembleConfigStationResp PROTO ((UINT2, tConfigStationResp *, 
            UINT1 *));
INT4 CapwapAssembleEchoRequest PROTO ((UINT2, tEchoReq *, UINT1 *));
INT4 CapwapAssembleEchoResponse PROTO ((UINT2, tEchoResp *, UINT1 *));
INT4 CapwapAssembleClearConfigReq PROTO ((UINT2, tClearconfigReq *, UINT1 *));
INT4 CapwapAssembleClearConfigResp PROTO ((UINT2, tClearconfigResp *, UINT1 *));
INT4 CapwapAssembleResetRequest PROTO ((UINT2, tResetReq *, UINT1 *));
INT4 CapwapAssembleResetResponse PROTO ((UINT2, tResetResp *, UINT1 *));
INT4 CapwapAssembleWtpEventRequest PROTO ((UINT2, tWtpEveReq *, UINT1 *));
INT4 CapwapAssembleWtpEventResponse PROTO ((UINT2, tWtpEveResp *, UINT1 *));
INT4 CapwapAssembleDataTransferReq PROTO ((UINT2, tDataReq *, UINT1 *));
INT4 CapwapAssembleDataTransferResp PROTO ((UINT2, tDataResp *, UINT1 *));
INT4 CapwapAssembleConfigUpdateReq PROTO ((UINT2, tConfigUpdateReq *, UINT1 *));
INT4 CapwapAssembleConfigUpdateResp PROTO ((UINT2, tConfigUpdateResp *, 
            UINT1 *));
INT4 test_capwapAssembleConfigStationRequest (VOID);
INT4 WtpEnterConfigStationState (VOID);
INT4 WtpEnterConfigUpdateState (VOID);
INT4 WtpEnterResetState (VOID);
INT4 WtpEnterDataTransferState (VOID);
INT4 WtpEnterClearConfigState (VOID);
INT4 WtpEnterWtpEventRequestState (VOID);
INT4 WtpEnterEchoState (VOID);
INT4
capwapValidateAddStation(UINT1 *,
                       tCapwapControlPacket *,
                       UINT2);
INT4
capwapValidateDeleteStation(UINT1 *,
                       tCapwapControlPacket *,
                       UINT2);

INT4 capwapGetAddStation (tAddstation *, UINT4 *);
INT4 capwapGetDeleteStation (tDeletestation *, UINT4 *);
INT4 capwapGetDataTransferMode (tDataTransferMode *, UINT4 *);
INT4 capwapGetDataTransferData (tDataTransferData *, UINT4 *);
INT4 capwapGetDecryptErrReport(tDecryptErrReport  *,UINT4 *); 
INT4 CapwapConstructConfigStationReq (tStationConfReq *);
INT4 CapwapConstructClearConfigReq (tClearconfigReq *);
INT4 CapwapConstructResetRequest (tResetReq *);
INT4 CapwapConstructWtpEventRequest(tWtpEveReq *);
INT4 CapwapConstructConfigUpdateReq (tConfigUpdateReq *);
INT4 CapwapConstructDataTransRequest (tDataReq *);
INT4 CapwapConstructEchoReq (tEchoReq *);
INT4 CapwapProcessResetRequest (tSegment *,tCapwapControlPacket *, 
        tRemoteSessionManager  *);

#endif
#endif
