/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 * $Id: wlchdlrtrc.h,v 1.1.1.1 2016/09/20 10:42:41 siva Exp $
 * Description:This file contains procedures and definitions
 *             used for debugging.
 *******************************************************************/

#ifndef _WLCHDLRTRC_H_
#define _WLCHDLRTRC_H_

#define  WLCHDLR_MOD                ((const char *)"WLCHDLR")

#define WLCHDLR_MASK                WLCHDLR_FAILURE_TRC

/* Trace and debug flags */
/* #define  WLCHDLR_MASK gu4CapwapDebugMask */

#define WLCHDLR_PKT_FLOW_IN              1
#define WLCHDLR_PKT_FLOW_OUT             2
/*Trace Level*/
#define WLCHDLR_MGMT_TRC                    0x00000001
#define WLCHDLR_INIT_TRC                    0x00000002
#define WLCHDLR_ENTRY_TRC                   0x00000004
#define WLCHDLR_EXIT_TRC                    0x00000008
#define WLCHDLR_FAILURE_TRC                 0x00000010
#define WLCHDLR_INFO_TRC                    0x00000020


#define WLCHDLR_FN_ENTRY() \
    MOD_FN_ENTRY (WLCHDLR_MASK, WLCHDLR_ENTRY_TRC,WLCHDLR_MOD)

#define WLCHDLR_FN_EXIT() \
    MOD_FN_EXIT (WLCHDLR_MASK, WLCHDLR_EXIT_TRC,WLCHDLR_MOD)

#define WLCHDLR_PKT_DUMP(mask, pBuf, Length, fmt)                           \
    MOD_PKT_DUMP(WLCHDLR_PKT_DUMP_TRC,mask, WLCHDLR_MOD, pBuf, Length, fmt)
#define WLCHDLR_TRC(mask, fmt)\
    MOD_TRC(WLCHDLR_MASK, mask, WLCHDLR_MOD, fmt)
#define WLCHDLR_TRC1(mask,fmt,arg1)\
    MOD_TRC_ARG1(WLCHDLR_MASK,mask,WLCHDLR_MOD,fmt,arg1)
#define WLCHDLR_TRC2(mask,fmt,arg1,arg2)\
    MOD_TRC_ARG2(WLCHDLR_MASK,mask,WLCHDLR_MOD,fmt,arg1,arg2)
#define WLCHDLR_TRC3(mask,fmt,arg1,arg2,arg3)\
    MOD_TRC_ARG3(WLCHDLR_MASK,mask,WLCHDLR_MOD,fmt,arg1,arg2,arg3)
#define WLCHDLR_TRC4(mask,fmt,arg1,arg2,arg3,arg4)\
    MOD_TRC_ARG4(WLCHDLR_MASK,mask,WLCHDLR_MOD,fmt,arg1,arg2,arg3,arg4)


#endif

