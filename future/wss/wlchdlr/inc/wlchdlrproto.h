/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 * $Id: wlchdlrproto.h,v 1.4 2017/11/24 10:37:06 siva Exp $
 * Description: This file contains all the proto types used by the
 *              WLC HDLR module
 *******************************************************************/
#ifndef __WLCHDLRPROTO_H__
#define __WLCHDLRPROTO_H__

INT4 WlcHdlrEnqueCtrlPkts (unWlcHdlrMsgStruct *);
INT4 WlcHdlrEnqueCfaTxPkts (UINT1 MsgType, unWlcHdlrMsgStruct *pWssIfMsg);
INT4 WlcHdlrVerifyStaPkts (unWlcHdlrMsgStruct *pWssIfMsg);

UINT4 WlcHdlrMemInit (VOID);
VOID WlcHdlrMemClear (VOID);
INT4 WlcHdlrProcessCtrlRxMsg (VOID);
INT4 WlcHdlrProcessDataRxMsg (VOID);
INT4 WlcHdlrProcessDataTxMsg (VOID);

INT4 wlchdlrDBInit (VOID);
INT4 WlcHdlrTmrInit(VOID);
VOID WlcHdlrTmrInitTmrDesc (VOID);
VOID WlcHdlrTmrExpHandler (VOID);
VOID WlcHdlrCPURelinquishTmrExp (VOID*);
INT4 WlcHdlrTmrStart (tRemoteSessionManager *, UINT1, UINT4);
INT4 WlcHdlrTmrStop (tRemoteSessionManager *, UINT1);
VOID WlcHdlrRetransmitIntervalTmrExp(VOID *);
VOID WlcHdlrDataTransferTmrExp(VOID *);
VOID WlcHdlrWebAuthLifetimeExp (VOID*);
VOID WlcHdlrStaConfClearTmrExp (VOID*);
VOID tmrWlchdlrCallback (tTimerListId);

INT4 WlchdlrLock PROTO ((VOID));
INT4 WlchdlrUnLock PROTO ((VOID));
INT4 WlchdlrBufLock PROTO ((VOID));
INT4 WlchdlrBufUnLock PROTO ((VOID));

INT4
WlchdlrFreeTransmittedPacket PROTO ((tRemoteSessionManager *pSessEntry));
INT4
WlcHdlrProcessWssIfMsg (UINT1 ,unWlcHdlrMsgStruct *);
INT4
WlcHdlrProcessConfigUpdateReq (UINT1 ,unWlcHdlrMsgStruct *);
INT4
WlcHdlrProcessWlanConfigRequest (tWssWlanConfigReq *);
INT4
WlcHdlrProcessStationConfigReq (tStationConfReq *);

INT4 WlcHdlrProcessClearConfigReq (tClearconfigReq *);
INT4 WlcHdlrProcessResetReq (tResetReq *);
INT4 WlcHdlrProcessDataTransferReq (tDataReq  *);

INT4 WlcHdlrValidateWlanConfigRsp (UINT1 *, tCapwapControlPacket *,
tRemoteSessionManager *);
INT4 CapValidateDataTransReqMsgElems (UINT1 *,tCapwapControlPacket *);
INT4 CapValidateDataTransRespMsgElems(UINT1 *,tCapwapControlPacket *);
INT4 CapValidateConfStatRespMsgElems(UINT1 *,tCapwapControlPacket *);
INT4 CapwapConstructResetRequest (tResetReq *);
INT4 CapValidateResetRespMsgElems (UINT1 *, tCapwapControlPacket *);
INT4 CapwapConstructClearConfigReq (tClearconfigReq *);
INT4 CapValidateClearConfRespMsgElems(UINT1 *,tCapwapControlPacket *);
INT4 CapValidateConfUpdateRespMsgElem(UINT1 *,tCapwapControlPacket *);
INT4 CapValidateWtpEventRespMsgElems(UINT1 *,tCapwapControlPacket *);
INT4 CapwapConstructDataTransRequest (tDataReq *);
INT4 CapwapValidateDataTransferData(UINT1 *, tCapwapControlPacket *,
        tDataTransferData *, UINT2);
INT4 CapwapGetDataTransferData(tDataTransferData *, UINT4 *);
INT4 CapwapGetDecryptErrReport(tDecryptErrReport *, UINT4 *);
INT4 CapwapValidatetdot11Statistics(UINT1 *, tCapwapControlPacket *, UINT2);
INT4 CapwapValidateWTPEventVendSpecPld (UINT1 *, tCapwapControlPacket *, 
        UINT2 );
INT4 CapwapValidateWTPEventRebootStats(UINT1 *, tCapwapControlPacket *, UINT2 );
INT4 CapValidateWtpEventReqMsgElems(UINT1 *, tCapwapControlPacket *);
INT4 CapwapGetDataTransferMode(tDataTransferMode *, UINT4 *);

INT4 WtpEnterResetState (void);
INT4 WtpEnterClearConfigState (void);

INT4 CapwapValidateDataTransferRequestMsgElements (UINT1 *,tCapwapControlPacket *, tDataReq *);
INT4 CapwapOpenAndWriteCrashData (UINT1  *,tRemoteSessionManager *, UINT2);
INT4 CapwapWriteCrashData(UINT1 *, tRemoteSessionManager *, UINT2);
INT4 CapwapGetWTPDataTransferRspMsgElements(tDataRsp *, UINT4 *);
VOID CapwapClearDataTransferSessionEntries(tRemoteSessionManager *);

INT4
CapwapKernelStaTableUpdate (tMacAddr , tWssIfAuthStateDB  *);
extern INT1 nmhGetIssLoginAuthentication ARG_LIST((INT4 *));

INT4 WlcHdlrClearStationEntries (UINT4 );
INT4 WlcHdlrClearAllStationEntries(VOID);
VOID WlcHdlrStaConfClearTmrControl(UINT4 u4Status,UINT4 u4TimerValue);
#ifdef BAND_SELECT_WANTED
VOID
WlcHdlrStaProbeExp (VOID *pArg);
VOID
WlcHdlrStaAssocCountExp (VOID *pArg);

INT4
WlcHdlrProbeTmrStart (tWssStaBandSteerDB * pWssStaBandSteerDB, UINT1 u1AgeOutTime);
INT4
WlcHdlrStaAssocCountTmrStart (tWssStaBandSteerDB * pWssStaBandSteerDB, UINT1 u1AssocResetTime);

INT4
WlcHdlrProbeTmrStop (tWssStaBandSteerDB * pWssStaBandSteerDB);

INT4
WlcHdlrStaAssocTmrStop (tWssStaBandSteerDB * pWssStaBandSteerDB);

#endif
INT4 WlchdlrSendEvent (UINT4);

#endif


