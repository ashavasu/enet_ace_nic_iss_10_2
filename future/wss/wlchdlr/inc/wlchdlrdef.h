/************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved                 * 
 * $Id: wlchdlrdef.h,v 1.2 2017/11/24 10:37:06 siva Exp $                                                                      *
 * DESCRIPTION    : This file contains the  definitions used for WLC    *
 *                  Handler  module                                     *
 ************************************************************************/

#ifndef  __WLCHDLR_DEF_H__
#define  __WLCHDLR_DEF_H__

#define WLCHDLR_TASK_NAME                              "WLCH"                  
#define WLCHDLR_RECV_TASK_PRIORITY                       100
#define WLCHDLR_STACK_SIZE                  OSIX_DEFAULT_STACK_SIZE

#define WLCHDLR_SEM_NAME                    (const UINT1 *) "WCCS"
#define WLCHDLR_BUF_SEM_NAME                (const UINT1 *) "WCBS"
#define WLCHDLR_CTRL_Q_NAME                 (UINT1 *) "WCCQ"
#define WLCHDLR_DATARX_Q_NAME               (UINT1 *) "WCDR"
#define WLCHDLR_DATATX_Q_NAME               (UINT1 *) "WCDT"

#define WLCHDLR_CTRL_RX_MSGQ_EVENT          0x00000001
#define WLCHDLR_DATA_RX_MSGQ_EVENT          0x00000002
#define WLCHDLR_DATA_TX_MSGQ_EVENT          0x00000004
#define WLCHDLR_TMR_EXP_EVENT               0x00000008 
#define WLCHDLR_STATMR_EXP_EVENT            0x00000010
#define WLCHDLR_RELINQUISH_EVENT            0x00000020
#define WLCHDLR_ACCEPT_EVENT                WLCHDLR_CMN_ACCEPT_EVENT
#define WLCHDLR_REJECT_EVENT                WLCHDLR_CMN_REJECT_EVENT

#define IMPLEMENTATION_ONGOING              0

#define WLCHDLR_SEM_CREATE_INIT_CNT         1
#define WLCHDLR_LOCK                   WlchdlrLock ()
#define WLCHDLR_UNLOCK                 WlchdlrUnLock ()
#define WLCHDLR_BUF_LOCK               WlchdlrBufLock ()
#define WLCHDLR_BUF_UNLOCK             WlchdlrBufUnLock ()

#endif

