/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved          
 * $Id: wlchdlrtdfs.h,v 1.2 2017/11/24 10:37:06 siva Exp $
 * Description: This file contains all the typedefs used by the WLC 
 *              Handler module 
 *******************************************************************/
#ifndef __WLCHDLR_DEFN_H__
#define __WLCHDLR_DEFN_H__

typedef enum {
    WLCHDLR_RETRANSMIT_INTERVAL_TMR = 0,
    WLCHDLR_CPU_RELINQUISH_TMR = 1,
    WLCHDLR_DATA_TRANSFER_TMR = 2,
    WLCHDLR_WEBAUTH_LIFETIME_EXP_TMR,
    WLCHDLR_PROBE_ENTRY_TMR,
    WLCHDLR_ASSOC_COUNT_TMR,
    WLCHDLR_STA_CONF_PKT_TMR,
    WLCHDLR_MAX_TMR
}tWlcHdlrTmrId;

/* ------------------------------------------------------------------
 *                Global Information
 * This structure contains all the Global Data required for WLC Handler
 * task Operation .
 * ----------------------------------------------------------------- */
typedef struct{
    tOsixTaskId         wlcHdlrTaskId;
    tOsixQId            ctrlRxMsgQId;    /*Ctrl Rx Queue */
    tOsixQId            ctrlRxFragMsgQId;       /*Ctrl Fragment RX Queue */
    tOsixQId            dataRxMsgQId;  /* Data Rx Queue */
    tOsixQId            dataTxMsgQId;           /* Data Tx Queue */
    tOsixSemId          wlcHdlrSemId;
    tOsixSemId          wlcHdlrBufSemId;
}tWlcHdlrTaskGlobals;


typedef struct{
    /*CAPWAP Timer List ID */
    tTimerListId        WlcHdlrTmrListId;
    tTmrDesc            aWlcHdlrTmrDesc[WLCHDLR_MAX_TMR];

    /* Timer data struct that contains
     * func ptrs for timer handling and

     * offsets to identify the data
     * struct containing timer block.
     * Timer ID is the index to this
     * data structure */
}tWlcHdlrTimerList;


#endif
