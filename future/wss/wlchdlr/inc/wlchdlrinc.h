/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 * $Id: wlchdlrinc.h,v 1.1.1.1 2016/09/20 10:42:41 siva Exp $
 * Description: This file contains header files included in
 *              WLC HDLR module.
 *********************************************************************/
#ifndef _WLCHDLRINC_H__
#define _WLCHDLRINC_H__

#include "lr.h"
#include "cfa.h"
#include "ipv6.h"
#include "tcp.h"
#include "cli.h"
#include "trace.h"
#include "fssocket.h"
#include "fssyslog.h"
#include "utilrand.h"
#include "wssifinc.h"
#include "capwap.h"
#include "wlchdlr.h"
#include "capwapinc.h"
#include "wlchdlrmacr.h"
#include "wlchdlrtdfs.h"
#include "wlchdlrtrc.h"
#include "wlchdlrdef.h"
#include "wlchdlrsz.h"
#include "wlchdlrproto.h"

#endif
