#!/bin/csh
# $Id: make.h,v 1.1.1.1 2016/09/20 10:42:41 siva Exp $
# Copyright (C) 2006 Aricent Inc . All Rights Reserved
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : MAKE.H                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       : Aricent Inc.                     |
# |                                                                          |
# |   MAKE TOOL(S) USED      : Eg: GNU MAKE                                  |
# |                                                                          |
# |   TARGET ENVIRONMENT     : LINUX ( Slackware 1.2.1 )                     |
# |                                                                          |
# |   DATE                   : 07th Mar 2002                                 |
# |                                                                          |
# |   DESCRIPTION            : Provide the following information in order -  |
# |                            1. Number of Submodules present if Main       |
# |                               makefile.                                  |
# |                            2. Clean option                               |
# +--------------------------------------------------------------------------+

###########################################################################
#               COMPILATION SWITCHES                                      #
###########################################################################

WLCHDLR_SWITCHES = -DRAD_TEST

TOTAL_OPNS = ${WLCHDLR_SWITCHES} $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES)

############################################################################
#                         Directories                                      #
############################################################################

CFA_BASE_DIR   = ${BASE_DIR}/cfa2

FCAP_INC              = ${BASE_DIR}/wss/fcapwap/inc
WLCHDLR_BASE_DIR      = ${BASE_DIR}/wss/wlchdlr
WLCHDLR_SRC_DIR       = ${WLCHDLR_BASE_DIR}/src
WLCHDLR_INC_DIR       = ${WLCHDLR_BASE_DIR}/inc
WLCHDLR_OBJ_DIR       = ${WLCHDLR_BASE_DIR}/obj
WSSIF_INC_DIR         = ${BASE_DIR}/wss/wssif/inc
CAPWAP_INC_DIR        = ${BASE_DIR}/wss/capwap/inc
SNMP_INCL_DIR         = ${BASE_DIR}/inc/snmp
CFA_INCD              = ${CFA_BASE_DIR}/inc
DTLS_INC_DIR          = ${BASE_DIR}/wss/dtls/inc
CMN_INC_DIR           = ${BASE_DIR}/inc
WSSPM_INC_DIR         = ${BASE_DIR}/wss/wsspm/inc
WSSCFG_INC_DIR        = ${BASE_DIR}/wss/wsscfg/inc
WSSSTAWLC_INC_DIR     = ${BASE_DIR}/wss/wsssta/inc
RADIOIF_INC_DIR    = ${BASE_DIR}/wss/radioif/inc
RFMGMT_INC_DIR        = ${BASE_DIR}/wss/rfmgmt/inc

############################################################################
##                     INCLUDE OPTIONS                                    ##
############################################################################

GLOBAL_INCLUDES  = -I${DTLS_INC_DIR} -I${WSSIF_INC_DIR} -I${CAPWAP_INC_DIR} -I${WLCHDLR_INC_DIR} -I${CFA_INCD} -I${CMN_INC_DIR} -I${RFMGMT_INC_DIR} -I${RADIOIF_INC_DIR} -I${FCAP_INC}
INCLUDES         = ${GLOBAL_INCLUDES} ${COMMON_INCLUDE_DIRS} -I${WSSCFG_INC_DIR} -I${WSSSTAWLC_INC_DIR} -I${WSSPM_INC_DIR}

#############################################################################
