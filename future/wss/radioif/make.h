# $Id: make.h,v 1.1.1.1 2016/09/20 10:42:38 siva Exp $
#!/bin/csh
# Copyright (C) 2006 Aricent Inc . All Rights Reserved
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : make.h                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       : Aricent Inc.                                  |
# |                                                                          |
# |   MAKE TOOL(S) USED      : Eg: GNU MAKE                                  |
# |                                                                          |
# |   TARGET ENVIRONMENT     : ANY                                           |
# |                                                                          |   
# |   DATE                   : 04 Mar 2013                                   |
# |                                                                          |  
# |   DESCRIPTION            : Provide the following information in order -  |
# |                            1. Number of Submodules present if Main       |
# |                               makefile.                                  |
# |                            2. Clean option                               |
# +--------------------------------------------------------------------------+

###########################################################################
#               COMPILATION SWITCHES                                      #
###########################################################################


TOTAL_OPNS =  $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES)

############################################################################
#                         Directories                                      #
############################################################################
RADIOIF_BASE_DIR = ${BASE_DIR}/wss/radioif
RADIOIF_INC_DIR  = ${RADIOIF_BASE_DIR}/inc
RADIOIF_SRC_DIR  = ${RADIOIF_BASE_DIR}/src
RADIOIF_OBJ_DIR  = ${RADIOIF_BASE_DIR}/obj
WSSWLAN_INC_DIR  = ${BASE_DIR}/wss/wsswlan/inc
WSSIF_INC_DIR  = ${BASE_DIR}/wss/wssif/inc
WSSMAC_INC_DIR  = ${BASE_DIR}/wss/wssmac/inc
BCNMGR_INC_DIR  = ${BASE_DIR}/wss/bcnmgr/inc
APHDLR_INC_DIR = ${BASE_DIR}/wss/aphdlr/inc
WLCHDLR_INC_DIR = ${BASE_DIR}/wss/wlchdlr/inc
CAPWAP_INC_DIR = ${BASE_DIR}/wss/capwap/inc
WSSAUTH_INC_DIR = ${BASE_DIR}/wss/wssauth/inc
WSSSTA_INC_DIR = ${BASE_DIR}/wss/wsssta/inc
WSSCFG_INC_DIR = ${BASE_DIR}/wss/wsscfg/inc
CFA_INC_DIR = ${BASE_DIR}/cfa2/inc
WSSPM_INC_DIR = ${BASE_DIR}/wss/wsspm/inc
RFMGMT_INC_DIR = ${BASE_DIR}/wss/rfmgmt/inc
ISS_INC_DIR = ${BASE_DIR}/ISS/common/system/inc

############################################################################
##                     INCLUDE OPTIONS                                    ##
############################################################################

GLOBAL_INCLUDES  =  -I${CFA_INC_DIR}  -I${RADIOIF_INC_DIR} -I${WSSWLAN_INC_DIR} -I${WSSIF_INC_DIR} -I${WSSMAC_INC_DIR} -I${BCNMGR_INC_DIR} -I${APHDLR_INC_DIR} -I${WLCHDLR_INC_DIR} -I${CAPWAP_INC_DIR} -I${WSSAUTH_INC_DIR} -I${WSSSTA_INC_DIR}  -I${WSSCFG_INC_DIR} -I${WSSPM_INC_DIR} -I${ISS_INC_DIR} -I${RFMGMT_INC_DIR}
INCLUDES         = ${GLOBAL_INCLUDES} ${COMMON_INCLUDE_DIRS}

#############################################################################


