/********************************************************************
 *  Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *  
 *  $Id: radioifconst.h,v 1.4 2017/11/24 10:37:03 siva Exp $
 *  
 *  Description: This file contains the macros defined for Radio module.
 **********************************************************************/

#ifndef _RADIOIFCONST_H
#define _RADIOIFCONST_H

#define RADIO_MAX_COUNT 31
#define RADIO_MIN_COUNT 1

#define RADIOIF_CHANGE_STATE_EVENT_REQ_MSG_TYPE       32 
#define RADIOIF_CHANGE_STATE_EVENT_REQ_MSG_LEN        3

#define RADIOIF_FAIL_ALARM_MSG_TYPE                   1047  
#define RADIOIF_FAIL_ALARM_MSG_LEN                    4

#define RADIO_ANTENNA_MIN_COUNT                       1
#define RADIO_ANTENNA_MAX_COUNT                       255 

#define WSS_SET_RADIOIF_ADMIN_OPER_STATUS            11
#define RADIOIF_GLOBAL_INVALID_VALUE                  0xFF
#define RADIOIF_NO_AP_PRESENT                         2
#define RADIOIF_RESPONSE_TIMEOUT                      3
#define RADIOIF_CONFIG_UPDATE_REQ_MSG_TYPE            31
#define RADIOIF_CONFIG_UPDATE_REQ_MSG_LEN             2
#define RADIO_CONF_UPDATE_REQ                         2
#define RADIO_DEF_DTIM_PERIOD                         5 
#define RADIO_MAX_DTIM_PERIOD                         255 

#define RADIO_CONF_UPDATE_STA_MSG_TYPE                1046 
#define RADIO_CONF_UPDATE_STA_MSG_LENGTH              16 

#define RADIO_QOS_MSG_TYPE                            1045
#define RADIO_QOS_MSG_LEN                             34
#define RADIO_QOS_CWMIN_MAX_VAL                       255
#define RADIO_QOS_CWMAX_MAX_VAL                       65535
#define RADIO_QOS_AIFSN_MIN_VAL                       0
#define RADIO_QOS_AIFSN_MAX_VAL                       15
#define RADIO_QOS_PROFILE                             4
#define RADIO_QOS_PRIO_MAX                            7
#define RADIO_DSCP_TAG_MAX                            63

#define RADIO_CONF_UPDATE_RATE_SET_MSG_TYPE           1034 
#define RADIO_SUPPORTED_BIT_RATE                      1000 

#define RADIO_CONF_UPDATE_DSSS_MSG_TYPE               1028 
#define RADIO_CONF_UPDATE_DSSS_MSG_LENGTH             8 

#define RADIO_CONF_UPDATE_OFDM_MSG_TYPE               1033 
#define RADIO_CONF_UPDATE_OFDM_MSG_LENGTH             8 

#define RADIO_CONF_UPDATE_TX_POWER_MSG_TYPE           1041 
#define RADIO_CONF_UPDATE_TX_POWER_LENGTH             4

#define RADIO_ADMIN_STATE_MSG_TYPE                    31 
#define RADIO_ADMIN_STATE_MSG_LEN                     2

#define CLI_MAX_INDEX                                 4
#define RADIO_ANTENNA_MSG_TYPE                        1025
#define RADIO_ANTENNA_MSG_MIN_LEN                     5
#define RADIO_ANTENNA_MSG_MAX_LEN                     5
#define RADIO_ANTENNA_MSG_LEN_CONS                    4
#define RADIO_ANTENNA_DIVERSITY_ENABLED               1
#define RADIO_ANTENNA_DIVERSITY_DISABLED              0
#define RADIO_ANTENNA_MODE_MAX                        4
#define RADIO_ANTENNA_INTERNAL                        1
#define RADIO_ANTENNA_EXTERNAL                        2

#define RADIO_DSSS_MSG_TYPE                           1028
#define RADIO_DSSS_MSG_LEN                            8

#define RADIO_MAX_DSSS_CHANNEL                        14
#define RADIO_MIN_DSSS_CHANNEL                        1

#define RADIO_MAX_DSSS_ED_THRESHOLD                   -35 
#define RADIO_MIN_DSSS_ED_THRESHOLD                   -95

#define RADIO_DSSS_CCA_ED                             1
#define RADIO_DSSS_CCA_CS                             2
#define RADIO_DSSS_CCA_CS_ED                          4
#define RADIO_DSSS_CCA_CS_TIMER                       8
#define RADIO_DSSS_CCA_CS_ED_HR                       16

#define RADIO_INFO_ELEM_MSG_TYPE                     1029
#define RADIO_INFO_ELEM_FIXED_MSG_LEN                5
#define RADIO_INFO_HT_CAP_ELEM_ID                    45
#define RADIO_INFO_HT_CAP_MSG_LEN                    26
#define RADIO_INFO_HT_INFO_ELEM_ID                    61
#define RADIO_INFO_HT_INFO_MSG_LEN                    22

#define RADIO_MAC_OPER_MSG_TYPE                       1030 
#define RADIO_MAC_OPER_MSG_LEN                        16
#define RADIO_MAX_RTS_THRESHOLD                       3000
#define RADIO_MIN_RTS_THRESHOLD                       0

#define RADIO_MAX_SHORT_RETRY                         255
#define RADIO_MIN_SHORT_RETRY                         1

#define RADIO_MAX_LONG_RETRY                          255
#define RADIO_MIN_LONG_RETRY                          1


#define RADIO_MIN_FRAGMENT_THRESHOLD                  256
#define RADIO_MAX_FRAGMENT_THRESHOLD                  3000

#define RADIO_MIN_TX_MSDU_LIFETIME                    1 
#define RADIO_MAX_TX_MSDU_LIFETIME                    65535

#define RADIO_MIN_RX_MSDU_LIFETIME                    1
#define RADIO_MAX_RX_MSDU_LIFETIME                    65535


#define RADIO_MULTI_DOMAIN_MSG_TYPE                   1032
#define RADIO_MULTI_DOMAIN_MSG_LEN                    8
#define RADIO_MULTI_DOMAIN_MAX_TX_POWER_LEVEL         8

#define RADIO_OFDM_MSG_TYPE                           1033
#define RADIO_OFDM_MSG_LEN                            8
#define RADIO_MAX_OFDM_CHANNEL                        196

#define RADIO_OFDM_CHANNEL_VALUE_MIN                  1
#define RADIO_OFDM_CHANNEL_VALUE_MAX                  200

#define RADIO_OFDM_SUPPORTED_BAND_VALUE_MAX           127 
#define RADIO_OFDM_SUPPORTED_BAND_VALUE_MIN           1

#define RADIO_OFDM_TI_THRESHOLD_VALUE_MAX             400 
#define RADIO_OFDM_TI_THRESHOLD_VALUE_MIN             1

#define RADIO_SUPPORTED_RATE_MSG_TYPE                 1040
#define RADIO_SUPPORTED_RATE_MSG_LEN                  3

#define RADIO_RATE_SET_MSG_TYPE                       1034
#define RADIO_RATE_SET_MSG_LEN                        9

#define RADIO_TXPOWER_MSG_TYPE                        1040
#define RADIO_TXPOWER_MSG_LEN                         4

#define RADIO_TXPOWER_LEVEL_MSG_TYPE                  1040
#define RADIO_TXPOWER_LEVEL_MSG_LEN                   4

#define RADIOIF_MAX_TX_POWER                          19
#define RADIOIF_POWER_MULTIPLIER                      3
#define RADIOIF_POWER_REF1                            30
#define RADIOIF_POWER_REF2                            10
#define RADIOIF_POWER_REF3                            1000
#define RADIOIF_POWER_REF4                            10.0

#define RADIO_CONFIG_MSG_TYPE                         1046
#define RADIO_CONFIG_MSG_LEN                          16

#define RADIO_QOS_CWMIN_MAX_VAL                       255
#define RADIO_QOS_CWMAX_MAX_VAL                       65535
#define RADIO_QOS_AIFSN_MIN_VAL                       0
#define RADIO_QOS_AIFSN_MAX_VAL                       15
#define RADIO_QOS_TXOP_MAX_VAL                        65535

#define RADIO_INFO_MSG_TYPE                           1048
#define RADIO_INFO_MSG_LEN                            5
#define RADIO_TYPE_B                                  1
#define RADIO_TYPE_A                                  2
#define RADIO_TYPE_G                                  4
#define RADIO_TYPE_N                                  8

#define RADIO_TYPE_BGN                                13
#define RADIO_TYPE_BG                                 5
#define RADIO_TYPE_AN                                 10
#define RADIO_TYPE_AC                                 0x80000000


/* DOT11N Vendor Specific macros */
#define   VENDOR_ID                                   1111
#define   RADIO_DOT11N_MSG_TYPE                       101
#define   RADIO_DOT11N_MSG_LEN                        16

#define RADIO_OPER_STATE_MSG_TYPE                     32
#define RADIO_OPER_STATE_MSG_LENGTH                   3
#define RADIO_MAX_FAILURE_CAUSE                       3    
#define RADIO_FAIL_ALARM_MSG_TYPE                     1047
#define RADIO_FAIL_ALARM_MSG_LENGTH                   4
#define RADIO_TRANSMIT_TYPE                           2
#define RADIO_RECEIVE_TYPE                            1
#define RADIO_FAILURE_OCCURED                         1
#define RADIO_FAILURE_CLEARED                         0

#define RADIO_SHORT_PREAMBLE_ENABLE                   1
#define RADIO_BAND_SUPPORTED                          15
#define RADIO_CCA_MODE_SUPPORTED                      1

#define RADIO_TYPEA_FIRST_CHANNEL                     36  
#define RADIO_TYPEB_FIRST_CHANNEL                     1 

#define RADIO_TYPEA_NO_OF_CHANNELS                    25                        
#define RADIO_TYPEB_NO_OF_CHANNELS                    11      

#define RADIO_MAX_BSSID_SUPPORTED                     16      

#ifndef NPAPI_WANTED
#define RADIO_INTF_NAME                               "wifi"
#define RADIO_INTF_NAME_LEN                              5
#define RADIO_ATH_INTF_NAME                           "ath"
#endif

#define RADIO_SUPPORTED_RATE1                         0x82
#define RADIO_SUPPORTED_RATE2                         0x84

#define SOFTWARE_FAILURE_TYPE                         1

#define RADIO_DOT11N_EXT_MSG_LEN                      12

#define NP_UTIL_FILL_PARAMS(FsHwNp, ModuleId, Opcode, \
        IfIndex, NumPortsParams, NumPortlistParams) \
      { \
           MEMSET (&FsHwNp, 0, sizeof(tFsHwNp)); \
               FsHwNp.NpModuleId = ModuleId; \
               FsHwNp.u4Opcode = Opcode; \
               FsHwNp.u4IfIndex = IfIndex; \
               FsHwNp.u4NumPortsParams = NumPortsParams; \
               FsHwNp.u4NumPortlistParams = NumPortlistParams; \
              }


enum {
RADIOIF_ADMIN_STATUS_DISABLED = 2,
RADIOIF_ADMIN_STATUS_ENABLED  = 1,
RADIOIF_ADMIN_STATUS_RESERVED = 0
};

#define RADIO_CHAN_RADAR                            0x00000500
#define RADIO_CHAN_DFS_AVAILABLE                        0x00000200 
#define RADIO_CHAN_DFS_UNAVAILABLE                      0x00000300
#define RADIO_CHAN_DISABLED                         0x00000400
#define RADIO_CHAN_DFS_MASK                         0x00000300 
#endif
