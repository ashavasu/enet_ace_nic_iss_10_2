/********************************************************************
 *  Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *  
 *  $Id: radioiftrc.h,v 1.2 2017/11/24 10:37:04 siva Exp $
 *  
 *  Description: This file contains the trace related info for Radio module.
 **********************************************************************/
#ifndef _RADIOIFTRC_H_
#define _RADIOIFTRC_H_

#define RADIOIF_MGMT_TRC                    0x00000001
#define RADIOIF_INIT_TRC                    0x00000002
#define RADIOIF_ENTRY_TRC                   0x00000004
#define RADIOIF_EXIT_TRC                    0x00000008
#define RADIOIF_FAILURE_TRC                 0x00000010
#define RADIOIF_BUF_TRC                     0x00000020
#define RADIOIF_SESS_TRC                    0x00000040
#define RADIOIF_CRITICAL_TRC                0x00000080
#define RADIOIF_INFO_TRC                    0x00000100


#define  RADIOIF_MOD                ((const char *)"RADIOIF")

#define RADIOIF_MASK                (RADIOIF_MGMT_TRC | RADIOIF_FAILURE_TRC | RADIOIF_CRITICAL_TRC)

/* Trace and debug flags */
#define RADIOIF_FN_ENTRY() MOD_FN_ENTRY (RADIOIF_MASK, \
        RADIOIF_ENTRY_TRC,RADIOIF_MOD)

#define RADIOIF_FN_EXIT() MOD_FN_EXIT (RADIOIF_MASK, \
        RADIOIF_EXIT_TRC,RADIOIF_MOD)

#define RADIOIF_TRC(mask,fmt)\
      MOD_TRC(RADIOIF_MASK,mask,RADIOIF_MOD,fmt)
#define RADIOIF_TRC1(mask,fmt,arg1)\
        MOD_TRC_ARG1(RADIOIF_MASK,mask,RADIOIF_MOD,fmt,arg1)
#define RADIOIF_TRC2(mask,fmt,arg1,arg2)\
        MOD_TRC_ARG2(RADIOIF_MASK,mask,RADIOIF_MOD,fmt,arg1,arg2)
#define RADIOIF_TRC3(mask,fmt,arg1,arg2,arg3)\
        MOD_TRC_ARG3(RADIOIF_MASK,mask,RADIOIF_MOD,fmt,arg1,arg2,arg3)
#define RADIOIF_TRC4(mask,fmt,arg1,arg2,arg3,arg4)\
        MOD_TRC_ARG4(RADIOIF_MASK,mask,RADIOIF_MOD,fmt,arg1,arg2,arg3,arg4)
#define RADIOIF_TRC5(mask,fmt,arg1,arg2,arg3,arg4,arg5)\
        MOD_TRC_ARG5(RADIOIF_MASK,mask,RADIOIF_MOD,fmt,arg1,arg2,arg3,arg4,arg5)
#define RADIOIF_TRC6(mask,fmt,arg1,arg2,arg3,arg4,arg5,arg6)\
        MOD_TRC_ARG6(RADIOIF_MASK,mask,RADIOIF_MOD,fmt,arg1,arg2,arg3,arg4,\
                arg5,arg6)
#endif
