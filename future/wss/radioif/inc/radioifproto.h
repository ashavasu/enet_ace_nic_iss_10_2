/********************************************************************
 *  Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *  
 *  $Id: radioifproto.h,v 1.4 2017/11/24 10:37:04 siva Exp $
 *  
 *  Description: This file contains the prototypes for Radio module.
 **********************************************************************/

#ifndef _RADIOIFPROTO_H
#define _RADIOIFPROTO_H
#ifdef NPAPI_WANTED
#include "nputil.h"
#endif

UINT1 RadioIfProcessWssIfMsg  PROTO ((UINT1 , tRadioIfMsgStruct *));

UINT1 RadioIfCreateVirtualIntf PROTO ((UINT2 , UINT1 , UINT4 *));

UINT1 RadioIfProcessDeleteWtpProfile PROTO ((UINT4 ));

UINT1 RadioIfGetWtpInternalId PROTO ((UINT4 , UINT2 *, UINT4 *));

UINT1 RadioIfGetDSSSTable PROTO ((tRadioIfGetDB *));

UINT1 RadioIfGetOFDMTable PROTO ((tRadioIfGetDB *));

UINT1 RadioIfSetDot11RadioType PROTO ((tRadioIfGetDB *));

UINT1 RadioIfSetAntennasList PROTO ((tRadioIfGetDB *));

UINT1 RadioIfSetOperationalRate PROTO ((tRadioIfGetDB *, UINT1 *));

UINT1 RadioIfGetEDCAParams PROTO ((tRadioIfGetDB *));    

UINT1 RadioIfCreateProfile PROTO ((tRadioIfMsgStruct*));

UINT1 RadioIfDeleteProfile PROTO ((tRadioIfMsgStruct*));

UINT1 RadioIfTearDownProcess PROTO ((tRadioIfMsgStruct*));

UINT1 RadioIfChangeStateEventReq PROTO ((tRadioIfMsgStruct*));

UINT1 RadioIfConfigUpdateResponse PROTO ((tRadioIfMsgStruct*));

UINT1 RadioIfConfigUpdateReq PROTO ((tRadioIfMsgStruct *));

UINT1 RadioIfCreateInterface PROTO ((tRadioIfMsgStruct *));

UINT1 RadioIfConfigUpdateReqValidate PROTO ((tRadioIfMsgStruct *));

UINT1 RadioIfConstructAdminStatus PROTO ((UINT4, UINT1, tRadioIfMsgStruct *));

UINT1 RadioIfSetStationConfig PROTO ((tRadioIfGetDB *));

UINT1 RadioIfSetTxPowerTable PROTO ((tRadioIfGetDB *));

UINT1 RadioIfSetDSSSTable PROTO ((tRadioIfGetDB  *));

UINT1 RadioIfSetOFDMTable PROTO ((tRadioIfGetDB *));

UINT1 RadioIfValidateChangeStateEvtReq PROTO ((tRadioIfMsgStruct *));

UINT1 RadioIfConfigStatusValidateReq PROTO ((tRadioIfMsgStruct *));

UINT1 RadioIfConfigStatusReq PROTO ((tRadioIfMsgStruct *));

UINT1 RadioIfPmWtpEventReq PROTO ((tRadioIfMsgStruct *));

UINT1 RadioIfRadioDbReset PROTO ((tRadioIfMsgStruct *));

UINT1 RadioIfConfigStatusRsp PROTO ((tRadioIfMsgStruct *));

UINT1 RadioIfProcessConfigUpdateReq PROTO ((tRadioIfMsgStruct *));

UINT1 RadioIfSetOperationTable PROTO ((tRadioIfGetDB  *));

UINT1 RadioIfSetFsDot11nParams PROTO ((tRadioIfGetDB  *));

UINT1 RadioIfSetEDCATable PROTO ((tRadioIfGetDB *));

UINT1 RadioIfGetMaxTxPowerLevel2G PROTO ((tRadioIfGetDB *, UINT1 *));

UINT1 RadioIfGetMaxTxPowerLevel5G PROTO ((tRadioIfGetDB *, UINT1 *));

UINT1 RadioIfGetCountryString PROTO ((tRadioIfGetDB *, UINT1 *));

UINT1 RadioIfGetMultiDomainInfo PROTO ((tRadioIfGetDB *, UINT1 *));

UINT1 RadioIfGetMacAddr PROTO ((tRadioIfGetDB *, UINT1 *));

UINT1 RadioIfGetBeaconPeriod PROTO ((tRadioIfGetDB *, UINT1 *));

UINT1 RadioIfGetDTIMPeriod PROTO ((tRadioIfGetDB *, UINT1 *));

UINT1 RadioIfGetCurrentChannel PROTO ((tRadioIfGetDB *, UINT1 *));

UINT1 RadioIfGetCurrentTxPower PROTO ((tRadioIfGetDB *, UINT1 *));

UINT1 RadioIfGetRadioDefault PROTO ((tRadioIfGetDB *, UINT1 *));

UINT1 RadioIfGetFragmentationThreshold PROTO ((tRadioIfGetDB *, UINT1 *));

UINT1 RadioIfGetRTSThreshold PROTO ((tRadioIfGetDB *, UINT1 *));

UINT1 RadioIfGetSupportedRate PROTO ((tRadioIfGetDB *, UINT1 *));

UINT1 RadioIfGetAntennaCount PROTO ((tRadioIfGetDB *, UINT1 *));

UINT1 RadioIfGetAntennaDiversity PROTO ((tRadioIfGetDB *, UINT1 *));

UINT1 RadioIfInitDefaultValues PROTO ((tRadioIfGetDB *));

UINT1 RadioIfConfigStatusValidateRsp PROTO ((tRadioIfMsgStruct *));

UINT1 RadioIfSetShortRetry PROTO ((tRadioIfGetDB *, UINT1 *));

UINT1 RadioIfSetLongRetry  PROTO ((tRadioIfGetDB *, UINT1 *));

UINT1 RadioIfSetAntennaCount PROTO ((tRadioIfGetDB *, UINT1 *));

UINT1 RadioIfSetAntennaCombiner PROTO ((tRadioIfGetDB *, UINT1 *));

UINT1 RadioIfSetAntennaSelection PROTO ((tRadioIfGetDB *, UINT1 *));

UINT1 RadioIfSetDSCEDThreshold PROTO ((tRadioIfGetDB *, UINT1 *));

UINT1 RadioIfSetRTSThreshold PROTO ((tRadioIfGetDB *, UINT1 *));

UINT1 RadioIfSetFragmentationThreshold PROTO ((tRadioIfGetDB *, UINT1 *));

UINT1 RadioIfSetTxMsduLifeTime PROTO ((tRadioIfGetDB *, UINT1 *));

UINT1 RadioIfSetRxMsduLifeTime PROTO ((tRadioIfGetDB *, UINT1 *));

UINT1 RadioIfAddChanlSwitIE PROTO ((tRadioIfGetDB *, UINT1 *));

UINT1 RadioIfRemoveChanlSwitIE PROTO ((tRadioIfGetDB *));

UINT1 RadioIfSetOFDMCurrentChannel PROTO ((tRadioIfGetDB *, UINT1 *));

UINT1 RadioIfSetOFDMCurrentChannelForWlan PROTO ((tRadioIfGetDB *, UINT1 *));

UINT1 RadioIfSetOFDMTIThreshold PROTO ((tRadioIfGetDB *, UINT1 *));

UINT1 RadioIfSetTxPowerLevel PROTO ((tRadioIfGetDB *, UINT1 *));

UINT1 RadioIfSetQosQueueDepth PROTO ((tRadioIfGetDB *, UINT1 *));

UINT1 RadioIfSetEdcaParams PROTO ((tRadioIfGetDB *, UINT1 *));

UINT1 RadioIfSetQosCwMin PROTO ((tRadioIfGetDB *, UINT1 *));

UINT1 RadioIfSetQosCwMax PROTO ((tRadioIfGetDB *, UINT1 *));

UINT1 RadioIfSetQosAifsn PROTO ((tRadioIfGetDB *, UINT1 *));

UINT1 RadioIfSetQosTxop PROTO ((tRadioIfGetDB *, UINT1 *));

UINT1 RadioIfSetQosAdmsnControl PROTO ((tRadioIfGetDB *, UINT1 *));

UINT1 RadioIfSetQosPrio PROTO ((tRadioIfGetDB *, UINT1 *));

UINT1 RadioIfSetQosDscp PROTO ((tRadioIfGetDB *, UINT1 *));

UINT1 RadioIfSetAdminStatus PROTO ((tRadioIfGetDB *, UINT1 *));

UINT1 RadioIfSetMaxTxPowerLevel2G PROTO ((tRadioIfGetDB *, UINT1 *));

UINT1 RadioIfSetMaxTxPowerLevel5G PROTO ((tRadioIfGetDB *, UINT1 *));

UINT1 RadioIfSetBeaconPeriod PROTO ((tRadioIfGetDB *, UINT1 *));

UINT1 RadioIfSetFsDot11nExtParams PROTO ((tRadioIfGetDB  *));
#ifdef HOSTAPD_WANTED
UINT1 RadioIfHostApdReset PROTO ((void));
#endif
UINT1 RadioIfSetDTIMPeriod PROTO ((tRadioIfGetDB *, UINT1 *));

UINT1 RadioIfSetCountryString PROTO ((tRadioIfGetDB *, UINT1 *));

UINT1 RadioIfGetMultiDomainAndAssembleBeacon PROTO ((tRadioIfGetDB *, UINT1 *));

UINT1 RadioIfSetLocalRoutingStatus PROTO ((UINT1 u1WtpLocalRouting));
UINT1 RadioIfUpdateLocalRoutingStatus PROTO ((UINT1 u1WtpLocalRouting));


UINT1 RadioIfSetAntennaDiversity PROTO ((tRadioIfGetDB *, UINT1 *));

UINT1 RadioIfSetDSCCurrentChannel PROTO ((tRadioIfGetDB *, UINT1 *));

UINT1 RadioIfWlanUpdate PROTO ((tRadioIfGetDB *));

UINT1 RadioIfDecryptReport PROTO ((tRadioIfMsgStruct *));

UINT1 RadioIfUpdateDecryptReport PROTO ((tRadioIfMsgStruct *));

UINT1 RadioIfUpdatePower PROTO ((tRadioIfMsgStruct *));

UINT1 RadioIfUpdateChannel PROTO ((tRadioIfMsgStruct *));

UINT1 RadioIfUpdateCenterFrequency PROTO ((UINT4 , UINT1));

UINT1 RadioIfSetDot11N PROTO ((tRadioIfGetDB *, UINT1 *));

UINT1
RadioIfProcess11NStatusReq PROTO ((tRadioIfGetDB * ,
                             tRadioIfMsgStruct * ));

UINT1
RadioIfProcess11nStatusReq PROTO ((tRadioIfGetDB * ,
       tRadioIfMsgStruct *));
UINT1
RadioIfSetDot11nOperParams PROTO ((tRadioIfGetDB * pRadioIfGetDB));

UINT1 RadioIfSetDot11AC PROTO ((tRadioIfGetDB *, UINT1 *));

UINT1 RadioIfProcess11AcStatusReq PROTO ((tRadioIfGetDB *, tRadioIfMsgStruct *));

UINT1 RadioIfSetDot11AcCapaParams PROTO ((tRadioIfGetDB *));

UINT1 RadioIfSetDot11AcOperParams PROTO ((tRadioIfGetDB *));

UINT1 RadioIfDeriveTxPower PROTO ((UINT4));

UINT1 RadioIfValidateAllowedChannels PROTO ((UINT4, UINT1, UINT1 *));

UINT1 RadioIfSetAllowedChannel PROTO ((UINT4, UINT1 *));

UINT1 RadioHwGetDriverMaxMPDULen PROTO((tRadioIfGetDB *, UINT1 *));
UINT1 RadioHwGetDriverRxLdpc PROTO((tRadioIfGetDB *, UINT1 *));
UINT1 RadioHwGetDriverShortGi80 PROTO((tRadioIfGetDB *, UINT1 *));
UINT1 RadioHwGetDriverShortGi160 PROTO((tRadioIfGetDB *, UINT1 *));
UINT1 RadioHwGetDriverTxStbc PROTO((tRadioIfGetDB *, UINT1 *));
UINT1 RadioHwGetDriverRxStbc PROTO((tRadioIfGetDB *, UINT1 *));
UINT1 RadioHwGetDriverSuBeamFormer PROTO((tRadioIfGetDB *, UINT1 *));
UINT1 RadioHwGetDriverSuBeamFormee PROTO((tRadioIfGetDB *, UINT1 *));
UINT1 RadioHwGetDriverSuppBeamFormAnt PROTO((tRadioIfGetDB *, UINT1 *));
UINT1 RadioHwGetDriverSoundDimension PROTO((tRadioIfGetDB *, UINT1 *));
UINT1 RadioHwGetDriverMuBeamFormer PROTO((tRadioIfGetDB *, UINT1 *));
UINT1 RadioHwGetDriverMuBeamFormee PROTO((tRadioIfGetDB *, UINT1 *));
UINT1 RadioHwGetDriverVhtTxOpPs PROTO((tRadioIfGetDB *, UINT1 *));
UINT1 RadioHwGetDriverHtcVhtCapable PROTO((tRadioIfGetDB *, UINT1 *));
UINT1 RadioHwGetDriverVhtMaxAMPDU PROTO((tRadioIfGetDB *, UINT1 *));
UINT1 RadioHwGetDriverVhtLinkAdaption PROTO((tRadioIfGetDB *, UINT1 *));
UINT1 RadioHwGetDriverVhtRxAntenna PROTO((tRadioIfGetDB *, UINT1 *));
UINT1 RadioHwGetDriverVhtTxAntenna PROTO((tRadioIfGetDB *, UINT1 *));
UINT1
RadioHwGetDriverSuppChannelWidth PROTO((tRadioIfGetDB * , UINT1 *));
UINT1
RadioHwGetDriverVhtSuppMcs PROTO((tRadioIfGetDB * , UINT1 *));
UINT1
RadioHwGetDriverVhtCapablities PROTO((tRadioIfGetDB * , UINT1 *));
UINT1
RadioHwGetSuppRadioType PROTO((tRadioIfGetDB * , UINT1 *));
UINT1
RadioIfGetScanDetails PROTO((tRadioIfGetDB * , UINT1 *));
UINT1
RadioHwGetDriverHtCapablities PROTO ((tRadioIfGetDB * pRadioIfGetDB,
                                UINT1 *pu1RadioName));
UINT1
RadioHwGetDriverHtSuppMcs PROTO ((tRadioIfGetDB * pRadioIfGetDB, 
    UINT1 *pu1RadioName));
UINT1
RadioHwGetDriverExtHtCapabilities PROTO ((tRadioIfGetDB * pRadioIfGetDB, 
   UINT1  *pu1RadioName));

UINT1
RadioHwGetDriverBeamformCapability PROTO ((tRadioIfGetDB * pRadioIfGetDB, 
   UINT1  *pu1RadioName));

VOID
RadioIfSetFsDot11nSuppMsc PROTO ((tRadioIfGetDB *pRadioIfGetDB, 
    tRadioIfGetDB *pSuppRadioIfGetDB, UINT1 *u1IsValChanged));

VOID
RadioIfSetFsDot11nAmpdu PROTO ((tRadioIfGetDB *pRadioIfGetDB, 
   tRadioIfGetDB *pSuppRadioIfGetDB, UINT1 *u1IsValChanged));

VOID
RadioIfSetFsDot11nHTExtCapability PROTO ((tRadioIfGetDB *pRadioIfGetDB, 
    tRadioIfGetDB *pSuppRadioIfGetDB, UINT1 *u1IsValChanged));

VOID
RadioIfSetFsDot11nBeamForming PROTO ((tRadioIfGetDB *pRadioIfGetDB, 
   tRadioIfGetDB *pSuppRadioIfGetDB, UINT1 *u1IsValChanged));

UINT1
RadioHwGetDriverHtAMPDU PROTO ((tRadioIfGetDB * pRadioIfGetDB, UINT1 *pu1RadioName));
UINT1 RadioHwGetDriverChannelCapablities PROTO ((tRadioIfGetDB * pRadioIfGetDB,
 UINT1 *pu1RadioName));
UINT4 RadioIfDFSChannelUpdate PROTO((tRadioIfMsgStruct *pWssMsgStruct));
UINT4 RadioIfDFSRadarStatusUpdate PROTO((tRadioIfMsgStruct *pWssMsgStruct));

UINT1
RadioIfGetSeqNum  PROTO ((tRadioIfGetDB * pRadioIfGetDB,INT4 i4IGTKKeyIndex, UINT1 *pu1IGTKPktNum,
                          UINT1 *pu1RadioName));

INT4
FsRadioHwGetWlanIfName PROTO ((UINT1 u1RadioId, UINT1 u1WlanId,
          UINT1 *pu1WlanIntfName, UINT1 *pu1WlanId));

#ifdef NPAPI_WANTED
VOID
RadioIfAddChanlWrapIE PROTO ((tRadioIfGetDB *, tFsHwNp *));
#endif 

UINT1
RadioIfDisassocStation PROTO ((tRadioIfGetDB * pRadioIfGetDB));

#ifdef BAND_SELECT_WANTED
UINT1
RadioIfDelStaBandSteerDB  PROTO ((UINT1 * pu1StaMacAddr,UINT1 *pu1RadioName,UINT1 *pu1WlanId ));
#endif
UINT1
RadioIfGetChannelWidth PROTO ((tRadioIfGetDB * pRadioIfGetDB, UINT1 *pu1RadioName));
UINT1
RadioIfGetShortGi PROTO ((tRadioIfGetDB * pRadioIfGetDB, UINT1 *pu1RadioName));
UINT1
RadioIfSetDot11nConfig PROTO((tRadioIfGetDB * pRadioIfGetDB, UINT1 *pu1RadioName));
UINT1
RadioIfSetQosInfoParams PROTO ((tRadioIfGetDB * pRadioIfGetDB, UINT1 *pu1RadioName));

#endif /* _RADIOIFPROTO_H */
