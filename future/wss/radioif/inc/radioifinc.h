/********************************************************************
 *  Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *  
 *  $Id: radioifinc.h,v 1.1.1.1 2016/09/20 10:42:38 siva Exp $
 *  
 *  Description: This file contains the common includes of Radio module.
 **********************************************************************/
#ifndef __RADIOIFINC_H_
#define __RADIOIFINC_H_

#include "lr.h"
#include "cfa.h"
#include "ip.h"

#include "trace.h"
#include "fssocket.h"
#include "fssyslog.h"
#include "utilrand.h"

#include "wsssta.h"
#include "wsswlan.h"

#include "wlchdlr.h"
#include "aphdlr.h"  

#ifdef RFMGMT_WANTED
#include "rfmgmt.h"  
#endif

#include "radioifconst.h"
#include "radioif.h"
#include "radioiftrc.h"

#ifdef NPAPI_WANTED
#include "radioifhwnpwr.h"
#endif

#ifdef __RADIOIFMAIN_C__
#include "radioifglob.h"
#else
#include "radioifextn.h"
#endif

#include "wssifinc.h" 
#include "wssifproto.h"
#include "radioifproto.h"
#include "capwapclilwg.h" 

#ifdef RFMGMT_WANTED
#include "rfminc.h" 
#endif

#ifdef WTP_WANTED
#include "isslow.h"
#include "fsisswr.h"
#include "fsisscli.h" 
#include "wsscfgcli.h" 
#endif

#endif
