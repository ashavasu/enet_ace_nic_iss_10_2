/********************************************************************
 *  Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *  
 *  $Id: radioifextn.h,v 1.1.1.1 2016/09/20 10:42:38 siva Exp $
 *  
 *  Description: This file contains the extern variables to be used by other
 *  modules
 **********************************************************************/
#ifndef  __RADIOIF_EXTN_H
#define  __RADIOIF_EXTN_H


PUBLIC UINT1 gu1IsConfigResponseReceived;

PUBLIC UINT1 gau1OperationalRate_RadioA[RADIOIF_OPER_RATE];

PUBLIC UINT1 gau1OperationalRate_RadioB[RADIOIF_OPER_RATE];

#endif

