/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: radioifwtpmain.c,v 1.1.1.1 2016/09/20 10:42:38 siva Exp $
 *
 * Description: This file handles all the functional interface invoked from
 * other modules
 ***********************************************************************/
#ifndef __RADIOIFMAIN_C__
#define __RADIOIFMAIN_C__

#include "radioifinc.h"

/*****************************************************************************
 * Function Name      : RadioIfProcessWssIfMsg                               *
 *                                                                           *
 * Description        : To call the appropriate Func calls(in RadioIf) based *
 *                      on the opcode received.                              *
 *                                                                           *
 * Input(s)           : u1OpCode - Received OP code                          *
 *                      pWssMsgStruct - Pointer to the input structure       *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1 
RadioIfProcessWssIfMsg(UINT1 u1OpCode, tRadioIfMsgStruct *pWssMsgStruct)
{
    tRadioIfGetDB   RadioIfGetDB;

    UINT4   u4FsCapwapWtpRadioSwFailureCount = 0;
    INT4   i4FsCapwapWtpRadioLastFailureType = 0;
    INT4    i4RadioIfIndex = 1;


    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    if (pWssMsgStruct ==  NULL)
    {
	RADIOIF_TRC (RADIOIF_FAILURE_TRC, 
		"RadioIfProcessWssIfMsg : Null input received\n");
	return OSIX_FAILURE;
    }

    switch(u1OpCode)
    {
	case WSS_RADIOIF_INIT_MSG:
	    if(RadioIfInit() != OSIX_SUCCESS)
	    {
		RADIOIF_TRC (RADIOIF_FAILURE_TRC,
			"RadioIfProcessWssIfMsg: Radio IF Init Failed \n");
		return OSIX_FAILURE;
	    }
	    break;

	case WSS_RADIOIF_CREATE_PHY_RADIO_INTF_MSG:
	    if(RadioIfCreateInterface(pWssMsgStruct) != OSIX_SUCCESS) 
	    {
		RADIOIF_TRC (RADIOIF_FAILURE_TRC,
			"RadioIfProcessWssIfMsg: Intf Creation Failed\n");
		return OSIX_FAILURE;
	    }
	    break;

	case WSS_RADIOIF_CONFIG_UPDATE_REQ:
	    if(RadioIfProcessConfigUpdateReq(pWssMsgStruct) != OSIX_SUCCESS) 
	    {
		if (nmhGetFsCapwapWtpRadioSwFailureCount(i4RadioIfIndex,
			    &u4FsCapwapWtpRadioSwFailureCount) != SNMP_SUCCESS)
		{
		    RADIOIF_TRC (RADIOIF_FAILURE_TRC,
			    "nmhGetFsCapwapWtpRadioSwFailureCount, returns failure\r\n");
		}

		u4FsCapwapWtpRadioSwFailureCount++;

		if (nmhSetFsCapwapWtpRadioSwFailureCount(i4RadioIfIndex,
			    u4FsCapwapWtpRadioSwFailureCount) != SNMP_SUCCESS)
		{
		    RADIOIF_TRC (RADIOIF_FAILURE_TRC,
			    "nmhSetFsCapwapWtpRadioSwFailureCount, returns failure \r\n");
		}

		if (nmhGetFsCapwapWtpRadioLastFailType(i4RadioIfIndex, 
			    &i4FsCapwapWtpRadioLastFailureType) != SNMP_SUCCESS)
		{
		    RADIOIF_TRC (RADIOIF_FAILURE_TRC,
			    "nmhGetFsCapwapWtpRadioLastFailType, returns FAILURE \r\n");
		}
		i4FsCapwapWtpRadioLastFailureType = SOFTWARE_FAILURE_TYPE;
		if (nmhSetFsCapwapWtpRadioLastFailType(
			    i4RadioIfIndex, 
			    i4FsCapwapWtpRadioLastFailureType) != SNMP_SUCCESS)
		{
		    RADIOIF_TRC (RADIOIF_FAILURE_TRC,
			    "nmhSetFsCapwapWtpRadioLastFailType, returns FAILURE \r\n");
		}

		RADIOIF_TRC (RADIOIF_FAILURE_TRC,
			"RadioIfProcessWssIfMsg: Conf Update Req Failed\n");
		return OSIX_FAILURE;
	    }
	    break;

	case WSS_RADIOIF_CONFIG_UPDATE_REQ_VALIDATE:
	    if(RadioIfConfigUpdateReqValidate(pWssMsgStruct) != OSIX_SUCCESS) 
	    {
		RADIOIF_TRC (RADIOIF_FAILURE_TRC,
			"RadioIfProcessMsg: Conf Update Req Validate failed\n");
		return OSIX_FAILURE;
	    }
	    break;

	case WSS_RADIOIF_CONFIG_STATUS_RSP:
	    if(RadioIfConfigStatusRsp(pWssMsgStruct) != OSIX_SUCCESS) 
	    {
		RADIOIF_TRC (RADIOIF_FAILURE_TRC,
			"RadioIfProcessMsg:: Config Status Resp Failed \n");
		return OSIX_FAILURE;
	    }
	    break;

	case WSS_RADIOIF_OPER_STATUS_CHG_MSG:
	    if(RadioIfChangeStateEventReq(pWssMsgStruct) != OSIX_SUCCESS) 
	    {
		RADIOIF_TRC (RADIOIF_FAILURE_TRC,
			"RadioIfProcessMsg:Change State Event Failed \n");
		return OSIX_FAILURE;
	    }
	    break;

	case WSS_RADIOIF_CONFIG_STATUS_RSP_VALIDATE:

	    if(RadioIfConfigStatusValidateRsp(pWssMsgStruct) != OSIX_SUCCESS) 
	    {
		RADIOIF_TRC (RADIOIF_FAILURE_TRC,
			"RadioIfProcessMsg: Status Resp validate failed \n");
		return OSIX_FAILURE;
	    }
	    break;

	case WSS_WLAN_UPDATE_RADIO_IF_DB:
	    RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection = UtlShMemAllocAntennaSelectionBuf ();

	    if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
	    {
		RADIOIF_TRC (RADIOIF_FAILURE_TRC, "\r%% Memory allocation for Antenna"
			" selection failed.\r\n");
		return OSIX_FAILURE;
	    } 

	    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = 
		pWssMsgStruct->unRadioIfMsg.RadioIfWlanUpdate.u4IfIndex;
	    RadioIfGetDB.RadioIfGetAllDB.u1WlanId = 
		pWssMsgStruct->unRadioIfMsg.RadioIfWlanUpdate.u1WlanId;

	    if(RadioIfWlanUpdate(&RadioIfGetDB) != OSIX_SUCCESS)
	    {
		RADIOIF_TRC (RADIOIF_FAILURE_TRC,
			"RadioIfProcessMsg: Update hardware call failed \n"); 
		UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
		return OSIX_FAILURE;
	    }
	    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
	    break;
	case WSS_RADIOIF_DECRYPT_REPORT:
	    if(RadioIfUpdateDecryptReport (pWssMsgStruct) != OSIX_SUCCESS)
	    {
		if (nmhGetFsCapwapWtpRadioSwFailureCount(i4RadioIfIndex,
			    &u4FsCapwapWtpRadioSwFailureCount) != SNMP_SUCCESS)
		{
		    RADIOIF_TRC (RADIOIF_FAILURE_TRC,
			    "nmhGetFsCapwapWtpRadioSwFailureCount, returns failure\r\n");
		}

		u4FsCapwapWtpRadioSwFailureCount++;

		if (nmhSetFsCapwapWtpRadioSwFailureCount(i4RadioIfIndex,
			    u4FsCapwapWtpRadioSwFailureCount) != SNMP_SUCCESS)
		{
		    RADIOIF_TRC (RADIOIF_FAILURE_TRC,
			    "nmhSetFsCapwapWtpRadioSwFailureCount, returns failure \r\n");
		}

		if (nmhGetFsCapwapWtpRadioLastFailType(i4RadioIfIndex, 
			    &i4FsCapwapWtpRadioLastFailureType) != SNMP_SUCCESS)
		{
		    RADIOIF_TRC (RADIOIF_FAILURE_TRC,
			    "nmhGetFsCapwapWtpRadioLastFailType, returns FAILURE \r\n");
		}
		i4FsCapwapWtpRadioLastFailureType = SOFTWARE_FAILURE_TYPE;
		if (nmhSetFsCapwapWtpRadioLastFailType(
			    i4RadioIfIndex, 
			    i4FsCapwapWtpRadioLastFailureType) != SNMP_SUCCESS)
		{
		    RADIOIF_TRC (RADIOIF_FAILURE_TRC,
			    "nmhSetFsCapwapWtpRadioLastFailType, returns FAILURE \r\n");
		}

		RADIOIF_TRC (RADIOIF_FAILURE_TRC,
			"RadioIfProcessMsg: Decrypt Report Period Failed \r\n");
		return OSIX_FAILURE;
	    }
	    break;
	case WSS_RADIOIF_DFS_CHANNEL_UPDATE:
	    {
		if(RadioIfDFSChannelUpdate(pWssMsgStruct) != OSIX_SUCCESS)
		{
		    RADIOIF_TRC (RADIOIF_FAILURE_TRC,
			    "RadioIfProcessMsg: DFS Channel Update failed \n");
		    return OSIX_FAILURE;
		}


	    }
	    break;
	case WSS_RADIOIF_DFS_RADAR_STAT_UPDATE:
	    {
		if(RadioIfDFSRadarStatusUpdate(pWssMsgStruct) != OSIX_SUCCESS)
		{
		    RADIOIF_TRC (RADIOIF_FAILURE_TRC,
			    "RadioIfProcessMsg: Status Resp validate failed \n");
		    return OSIX_FAILURE;
		}


	    }
	    break;

	default:
	    RADIOIF_TRC(RADIOIF_FAILURE_TRC,
		    "RadioIfProcessWssIfMsg: Invalid OpCode, return failure");
	    break;
    }
    return OSIX_SUCCESS;
}

#endif /* __RADIOIFMAIN_C__ */
