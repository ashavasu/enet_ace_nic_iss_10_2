/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: radioifwlcmain.c,v 1.1.1.1 2016/09/20 10:42:38 siva Exp $
 *
 * Description: This file handles all the functional interface invoked from
 * other modules
 ***********************************************************************/
#ifndef __RADIOIFMAIN_C__
#define __RADIOIFMAIN_C__

#include "radioifinc.h"
/*****************************************************************************
 * Function Name      : RadioIfProcessWssIfMsg                               *
 *                                                                           *
 * Description        : To call the appropriate Func calls(in RadioIf) based *
 *                      on the opcode received.                              *
 *                                                                           *
 * Input(s)           : u1OpCode - Received OP code                          *
 *                      pWssMsgStruct - Pointer to the input structure       *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfProcessWssIfMsg (UINT1 u1OpCode, tRadioIfMsgStruct * pWssMsgStruct)
{
    if (pWssMsgStruct == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfProcessWssIfMsg : Null input received\n");
        return OSIX_FAILURE;
    }

    switch (u1OpCode)
    {
        case WSS_RADIOIF_INIT_MSG:
            if (RadioIfInit () != OSIX_SUCCESS)
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                             "RadioIfProcessWssIfMsg: RadioIf Init failed \n");
                return OSIX_FAILURE;
            }
            break;

        case WSS_RADIOIF_CREATE_PHY_RADIO_INTF_MSG:
            if (RadioIfCreateInterface (pWssMsgStruct) != OSIX_SUCCESS)
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                             "RadioIfProcessWssIfMsg: Interface Creation Failed\n");
                return OSIX_FAILURE;
            }
            break;

        case WSS_RADIOIF_CREATE_VIRTUAL_RADIO_MSG:
            if (RadioIfCreateProfile (pWssMsgStruct) != OSIX_SUCCESS)
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                             "RadioIfProcessWssIfMsg: Profile Creation Failed \n");
                return OSIX_FAILURE;
            }
            break;

        case WSS_RADIOIF_DELETE_VIRTUAL_RADIO_MSG:
            if (RadioIfDeleteProfile (pWssMsgStruct) != OSIX_SUCCESS)
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                             "RadioIfProcessWssIfMsg:Profile Deletion Failed \r\n");
                return OSIX_FAILURE;
            }
            break;

        case WSS_RADIOIF_CHANGE_STATE_EVENT_REQ:
            if (RadioIfChangeStateEventReq (pWssMsgStruct) != OSIX_SUCCESS)
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                             "RadioIfProcessWssIfMsg: Change State Evt Failed \n");
                return OSIX_FAILURE;
            }
            break;

        case WSS_RADIOIF_CONFIG_UPDATE_RSP_VALIDATE:
            break;

        case WSS_RADIOIF_ADMIN_STATUS_CHG_MSG:
            if (RadioIfConfigUpdateReq (pWssMsgStruct) != OSIX_SUCCESS)
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                             "RadioIfProcessWssIfMsg: Config Update Req Failed \n");
                return OSIX_FAILURE;
            }
            break;

        case WSS_RADIOIF_CONFIG_UPDATE_RSP:

            if (RadioIfConfigUpdateResponse (pWssMsgStruct) != OSIX_SUCCESS)
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                             "RadioIfProcessWssIfMsg: Config State Resp Failed \n");
                return OSIX_FAILURE;
            }
            break;

        case WSS_RADIOIF_CONFIG_STATUS_REQ_VALIDATE:

            if (RadioIfConfigStatusValidateReq (pWssMsgStruct) != OSIX_SUCCESS)
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                             "RadioIfProcessMsg: Config Status Validate Failed\n");
                return OSIX_FAILURE;
            }
            break;

        case WSS_RADIOIF_CONFIG_STATUS_REQ:

            if (RadioIfConfigStatusReq (pWssMsgStruct) != OSIX_SUCCESS)
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                             "RadioIfProcessMsg: Config Status Req Failed r\n");
                return OSIX_FAILURE;
            }
            break;
        case WSS_RADIOIF_DECRYPT_REPORT:
            if (RadioIfDecryptReport (pWssMsgStruct) != OSIX_SUCCESS)
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                             "RadioIfProcessMsg: Decrypt Report Period Failed \r\n");
                return OSIX_FAILURE;
            }
            break;

        case WSS_RADIOIF_POWER_UPDATE:
            if (RadioIfUpdatePower (pWssMsgStruct) != OSIX_SUCCESS)
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                             "RadioIfProcessMsg: TxPower update Failed \r\n");
                return OSIX_FAILURE;
            }
            break;

        case WSS_RADIOIF_CHANNEL_UPDATE:
            if (RadioIfUpdateChannel (pWssMsgStruct) != OSIX_SUCCESS)
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                             "RadioIfProcessMsg: Channel updated Failed \r\n");
                return OSIX_FAILURE;
            }
            break;

        case WSS_RADIOIF_CHANGE_STATE_EVT_REQ_VALIDATE:
            break;


        case WSS_RADIOIF_TEARDOWN_NOTIFICATION_MSG:
            if (RadioIfTearDownProcess (pWssMsgStruct) != OSIX_SUCCESS)
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                             "RadioIfProcessMsg: Teardown notification process failed \r\n");
                return OSIX_FAILURE;
            }

            break;

        case WSS_RADIOIF_PM_WTP_EVENT_REQ:

            if (RadioIfPmWtpEventReq (pWssMsgStruct) != OSIX_SUCCESS)
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                             "RadioIfProcessMsg: Wtp Event Req Failed r\n");
                return OSIX_FAILURE;
            }
            break;

        case WSS_RADIOIF_RADIODB_RESET:

            if (RadioIfRadioDbReset (pWssMsgStruct) != OSIX_SUCCESS)
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                             "RadioIfProcessMsg: Wtp Event Req Failed r\n");
                return OSIX_FAILURE;
            }
            break;

        default:
            RADIOIF_TRC1 (RADIOIF_FAILURE_TRC,
                          "RadioIfProcessWssIfMsg: Invalid OpCode %d,\
                    return failure", u1OpCode);
            break;
    }
    return OSIX_SUCCESS;
}
#endif /* __RADIOIFMAIN_C__ */
