/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: radioifwlcproc.c,v 1.5 2018/01/04 09:54:33 siva Exp $
 *
 * Description: This file handles all the configurations to be sent to WTP
 ***********************************************************************/
#ifndef __RADIOIFAC_C__
#define __RADIOIFAC_C__

#include "radioifinc.h"
#include "wssifproto.h"
#include "wsscfgcli.h"
#include "wsspmdb.h"
#include "wsspmprot.h"
#include "wsscfgwlanproto.h"

PRIVATE UINT1       UtilIsDFSChannels (UINT1 u1Channel);

/*****************************************************************************
 * Function Name      : RadioIfCreateProfile                                 *
 *                                                                           *
 * Description        : This functions creates an entry for each of the      *
 *                      virtual radio interface                              *
 *                                                                           *
 * Input(s)           : pWssMsgStruct - Pointer to the input structure       *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/

UINT1
RadioIfCreateProfile (tRadioIfMsgStruct * pWssMsgStruct)
{
    tCfaMsgStruct       CfaMsgStruct;
    tRadioIfGetDB       RadioIfGetDB;
    tWssPMInfo          WssPmInfo;
#ifdef RFMGMT_WANTED
    tRfMgmtMsgStruct    RfMgmtMsgStruct;
#endif
    UINT2               u2CapwapBaseWtpProfileId = 0;
    UINT4               u4CapwapBaseWirelessBindingRadioId = 0;
    INT4                i4FsCapwapWirelessBindingRowStatus = 0;
    INT4                i4FsCapwapWirelessBindingVirtualRadioIfIndex = 0;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    MEMSET (&CfaMsgStruct, 0, sizeof (tCfaMsgStruct));
    MEMSET (&WssPmInfo, 0, sizeof (tWssPMInfo));
#ifdef RFMGMT_WANTED
    MEMSET (&RfMgmtMsgStruct, 0, sizeof (tRfMgmtMsgStruct));
#endif

    /*Input Validation */
    if (pWssMsgStruct == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfCreateProfile: Null input received\n");
        return OSIX_FAILURE;
    }

    /*To check if the Structure is filled (Ispresent is set True) */
    if (pWssMsgStruct->unRadioIfMsg.RadioIfVirtualIntfCreate.isPresent
        == OSIX_FALSE)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfCreateProfile : Inconsistent input received\n");
        return OSIX_FAILURE;
    }

    CapwapGetProfileIdFromInternalProfId
        (pWssMsgStruct->unRadioIfMsg.RadioIfVirtualIntfCreate.u2WtpInternalId,
         &u2CapwapBaseWtpProfileId);

    u4CapwapBaseWirelessBindingRadioId =
        pWssMsgStruct->unRadioIfMsg.RadioIfVirtualIntfCreate.u1RadioId;

    /*TO CFA : Iftype , gets IfIndex */
    CfaMsgStruct.unCfaMsg.CfaCreateIfIndex.u1IfType =
        pWssMsgStruct->unRadioIfMsg.RadioIfVirtualIntfCreate.u1IfType;

    if (nmhGetFsCapwapWirelessBindingRowStatus (u2CapwapBaseWtpProfileId,
                                                u4CapwapBaseWirelessBindingRadioId,
                                                &i4FsCapwapWirelessBindingRowStatus)
        == SNMP_SUCCESS)
    {
        if (nmhGetFsCapwapWirelessBindingVirtualRadioIfIndex
            (u2CapwapBaseWtpProfileId, u4CapwapBaseWirelessBindingRadioId,
             &i4FsCapwapWirelessBindingVirtualRadioIfIndex) == SNMP_SUCCESS)
        {
            /* get the IfIndex from the stored local database itself */
            CfaMsgStruct.unCfaMsg.CfaCreateIfIndex.u4IfIndex = (UINT4)
                i4FsCapwapWirelessBindingVirtualRadioIfIndex;
        }
    }
    else
    {
        /*Call to the Cfa function */
        if (WssIfProcessCfaMsg (CFA_WSS_CREATE_IFINDEX_MSG, &CfaMsgStruct)
            != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfCreateProfile : Call to the Cfa failed \n");
            return OSIX_FAILURE;
        }
    }

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
        CfaMsgStruct.unCfaMsg.CfaCreateIfIndex.u4IfIndex;
    RadioIfGetDB.RadioIfGetAllDB.u1IfType =
        CfaMsgStruct.unCfaMsg.CfaCreateIfIndex.u1IfType;

    if (pWssMsgStruct->unRadioIfMsg.RadioIfVirtualIntfCreate.u1AdminStatus == 0)
    {
        pWssMsgStruct->unRadioIfMsg.RadioIfVirtualIntfCreate.u1AdminStatus =
            CFA_IF_UP;
    }

    RadioIfGetDB.RadioIfGetAllDB.u1AdminStatus =
        pWssMsgStruct->unRadioIfMsg.RadioIfVirtualIntfCreate.u1AdminStatus;
    RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId =
        pWssMsgStruct->unRadioIfMsg.RadioIfVirtualIntfCreate.u2WtpInternalId;
    RadioIfGetDB.RadioIfGetAllDB.u1RadioId =
        pWssMsgStruct->unRadioIfMsg.RadioIfVirtualIntfCreate.u1RadioId;
    MEMCPY (RadioIfGetDB.RadioIfGetAllDB.MacAddr,
            pWssMsgStruct->unRadioIfMsg.RadioIfVirtualIntfCreate.MacAddr,
            sizeof (tMacAddr));
    RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType =
        pWssMsgStruct->unRadioIfMsg.RadioIfVirtualIntfCreate.u4Dot11RadioType;
    RadioIfGetDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_TRUE;

    /*DB Updation */
    if (WssIfProcessRadioIfDBMsg (WSS_ADD_RADIO_IF_DB, &RadioIfGetDB)
        != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfCreateProfile : Value to be set in DB Failed \r\n");
        return OSIX_FAILURE;
    }

    RadioIfGetDB.RadioIfGetAllDB.u4RowStatus = CREATE_AND_GO;
    RadioIfGetDB.RadioIfIsGetAllDB.bRowStatus = OSIX_TRUE;

    if (WssCfgSetDot11Radiotype (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "CreateProfile : CliDBProcessWssIfMsg failed\n");
        return OSIX_FAILURE;
    }

    CfaMsgStruct.unCfaMsg.CfaCreateIfIndex.u1AdminStatus =
        pWssMsgStruct->unRadioIfMsg.RadioIfVirtualIntfCreate.u1AdminStatus;

    if (CfaProcessWssIfMsg (CFA_WSS_ADMIN_STATUS_CHG_MSG, &CfaMsgStruct)
        != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "CreateProfile : CfaProcessWssIfMsg failed\r\n");
        return OSIX_FAILURE;
    }

    pWssMsgStruct->unRadioIfMsg.RadioIfVirtualIntfCreate.u4IfIndex
        = RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex;

    WssPmInfo.u2ProfileId = RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;
    WssPmInfo.u4RadioIfIndex = RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex;
    WssPmInfo.stats_timer = WSS_PM_DEF_STATS_TIMER_PERIOD;

    if (pmWTPInfoNotify (WTPPRF_ADD, &WssPmInfo) != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "CreateProfile : Update PM Stats DB Failed\n");
        return OSIX_FAILURE;
    }
#ifdef  RFMGMT_WANTED
    RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtIntfConfigReq.u4RadioIfIndex =
        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex;

    RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtIntfConfigReq.u4Dot11RadioType =
        RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType;

    RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtIntfConfigReq.isPresent = OSIX_TRUE;

    MEMCPY (RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtIntfConfigReq.RadioMacAddr,
            RadioIfGetDB.RadioIfGetAllDB.MacAddr, sizeof (tMacAddr));
    if (WssIfProcessRfMgmtMsg (RFMGMT_CREATE_RADIO_ENTRY,
                               &RfMgmtMsgStruct) != RFMGMT_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "CreateProfile : Update RM Mgmt DB Failed\n");
        return OSIX_FAILURE;
    }
#endif
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfDeleteProfile                                 *
 *                                                                           *
 * Description        : This functions deletes an entry for each of the      *
 *                      virtual radio interface                              *
 *                                                                           *
 * Input(s)           : pWssMsgStruct - Pointer to the input structure       *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfDeleteProfile (tRadioIfMsgStruct * pWssMsgStruct)
{
    tWssPMInfo          WssPmInfo;
    tRadioIfGetDB       RadioIfGetDB;
    tCfaMsgStruct       CfaMsgStruct;
    UINT1               u1IdCountIndex = 0;
    UINT1               u1BssIdCount = 0;
#ifdef RFMGMT_WANTED
    tRfMgmtMsgStruct    RfMgmtMsgStruct;
#endif

#ifdef RFMGMT_WANTED
    MEMSET (&RfMgmtMsgStruct, 0, sizeof (tRfMgmtMsgStruct));
#endif
    MEMSET (&WssPmInfo, 0, sizeof (tWssPMInfo));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    MEMSET (&CfaMsgStruct, 0, sizeof (tCfaMsgStruct));

    /*Input Validation */
    if (pWssMsgStruct == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfDeleteProfile: Null input received\n");
        return OSIX_FAILURE;
    }

    /*To check if the Structure is filled (Ispresent is set True) */
    if (pWssMsgStruct->unRadioIfMsg.RadioIfVirtualIntfCreate.isPresent
        == OSIX_FALSE)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfDeleteProfile: Input Validation Failed \r\n");
        return OSIX_FAILURE;
    }

    RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
        UtlShMemAllocAntennaSelectionBuf ();

    if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC, "\r%% Memory allocation for Antenna"
                     " selection failed.\r\n");
        return OSIX_FAILURE;
    }
    /*To get the RadioIfDB Value for BSSID count */
    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
        pWssMsgStruct->unRadioIfMsg.RadioIfVirtualIntfCreate.u4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bBssIdCount = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
#ifdef RFMGMT_WANTED
    RadioIfGetDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_TRUE;
#endif

    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, (&RadioIfGetDB))
        != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfDeleteProfile : DB Access failed\r\n");
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                         RadioIfGetAllDB.pu1AntennaSelection);
        return OSIX_FAILURE;
    }
    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                     RadioIfGetAllDB.pu1AntennaSelection);

    u1BssIdCount = RadioIfGetDB.RadioIfGetAllDB.u1BssIdCount;    /*BSS Count */

    /*For All the Bss present */
    for (u1IdCountIndex = 1; u1IdCountIndex <= u1BssIdCount; u1IdCountIndex++)
    {
        /*To Get BssIfIndex from RadioIf Module , filling IfIndex */

        RadioIfGetDB.RadioIfGetAllDB.u1WlanId = u1IdCountIndex;
        RadioIfGetDB.RadioIfIsGetAllDB.bBssIfIndex = OSIX_TRUE;

        if (WssIfProcessRadioIfDBMsg (WSS_GET_WLAN_BSS_IFINDEX_DB,
                                      (&RadioIfGetDB)) != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfDeleteProfile: Retreival of BSSIF Index Failed\n");
            return OSIX_FAILURE;
        }

        if (WssCfgDeleteWlanBinding (RadioIfGetDB.RadioIfGetAllDB.u4BssIfIndex)
            != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfDeleteProfile: BSS Interface deletion"
                         "failed\r\n");
            return OSIX_FAILURE;
        }

    }

    MEMSET (&CfaMsgStruct, 0, sizeof (tCfaMsgStruct));
    /*  CFA module , To delete the radio Interface  */
    CfaMsgStruct.unCfaMsg.CfaCreateIfIndex.u4IfIndex =
        pWssMsgStruct->unRadioIfMsg.RadioIfVirtualIntfCreate.u4IfIndex;

    if (WssIfProcessCfaMsg (CFA_WSS_DELETE_IFINDEX_MSG, &CfaMsgStruct)
        != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfDeleteProfile: Deletion of Interface Failed\n");
        return OSIX_FAILURE;
    }

    /* To Delete node(from Tree), with IfIndex(Obtained from WLC HDLR) */
    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
        pWssMsgStruct->unRadioIfMsg.RadioIfVirtualIntfCreate.u4IfIndex;
    RadioIfGetDB.RadioIfGetAllDB.u4RowStatus = DESTROY;
    RadioIfGetDB.RadioIfIsGetAllDB.bRowStatus = OSIX_TRUE;

    if (WssCfgSetDot11Radiotype (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "DeleteProfile: CliDBProcessWssIfMsg failed\r\n");
        return OSIX_FAILURE;
    }

    WssPmInfo.u2ProfileId = RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;
    WssPmInfo.u4RadioIfIndex = RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex;
    WssPmInfo.u1RadioId = RadioIfGetDB.RadioIfGetAllDB.u1RadioId;

    if (pmWTPInfoNotify (WTPPRF_DEL, &WssPmInfo) != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "DeleteProfile : Update PM Stats DB Failed \r\n");
        return OSIX_FAILURE;
    }

#ifdef  RFMGMT_WANTED
    RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtIntfConfigReq.u4RadioIfIndex =
        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex;
    RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtIntfConfigReq.u4Dot11RadioType =
        RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType;
    RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtIntfConfigReq.u2WtpInternalId =
        RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;

    RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtIntfConfigReq.isPresent = OSIX_TRUE;

    if (WssIfProcessRfMgmtMsg (RFMGMT_DELETE_RADIO_ENTRY,
                               &RfMgmtMsgStruct) != RFMGMT_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "CreateProfile : Update RM Mgmt DB Failed\n");
        return OSIX_FAILURE;
    }
#endif
    /* To Delete the corresponding node from the tree */
    if (WssIfProcessRadioIfDBMsg (WSS_DEL_RADIO_IF_DB, (&RadioIfGetDB))
        != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfDeleteInterface: DB Get failed \r\n");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfTearDownProcess                               *
 *                                                                           *
 * Description        : This functions process the AP Teardown notification  *
 *                      to remove the station entries associated with it     *
 *                                                                           *
 * Input(s)           : pWssMsgStruct - Pointer to the input structure       *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfTearDownProcess (tRadioIfMsgStruct * pWssMsgStruct)
{
    tRadioIfGetDB       RadioIfGetDB;
    tCfaMsgStruct       CfaMsgStruct;
    UINT1               u1IdCountIndex = 0;
    UINT1               u1BssIdCount = 0;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    MEMSET (&CfaMsgStruct, 0, sizeof (tCfaMsgStruct));

    /*Input Validation */
    if (pWssMsgStruct == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfTearDownProcess: Null input received\n");
        return OSIX_FAILURE;
    }

    /*To check if the Structure is filled (Ispresent is set True) */
    if (pWssMsgStruct->unRadioIfMsg.RadioIfVirtualIntfCreate.isPresent
        == OSIX_FALSE)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfTearDownProcess: Input Validation Failed \r\n");
        return OSIX_FAILURE;
    }

    /*To get the RadioIfDB Value for BSSID count */
    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
        pWssMsgStruct->unRadioIfMsg.RadioIfVirtualIntfCreate.u4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bBssIdCount = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;

    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, (&RadioIfGetDB))
        != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfTearDownProcess : DB Access failed\r\n");
        return OSIX_FAILURE;
    }

    u1BssIdCount = RadioIfGetDB.RadioIfGetAllDB.u1BssIdCount;    /*BSS Count */

    /*For All the Bss present */
    for (u1IdCountIndex = 1; u1IdCountIndex <= u1BssIdCount; u1IdCountIndex++)
    {
        /*To Get BssIfIndex from RadioIf Module , filling IfIndex */

        RadioIfGetDB.RadioIfGetAllDB.u1WlanId = u1IdCountIndex;
        RadioIfGetDB.RadioIfIsGetAllDB.bBssIfIndex = OSIX_TRUE;

        if (WssIfProcessRadioIfDBMsg (WSS_GET_WLAN_BSS_IFINDEX_DB,
                                      (&RadioIfGetDB)) != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfTearDownProcess: Retreival of BSSIF Index Failed\n");
            return OSIX_FAILURE;
        }
        if (WssStaDeleteStation (RadioIfGetDB.RadioIfGetAllDB.u4BssIfIndex)
            != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfTearDownProcess: Station entry failed\r\n");
            return OSIX_FAILURE;
        }
    }
    if (RadioIfRadioDbReset (pWssMsgStruct) != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfRadioDbReset: Radio DB reset failed\r\n");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfConfigUpdateReq                               *
 *                                                                           *
 * Description        : This functions constructs and send config update     *
 *                      request message for Radio Admin Status               *
 *                                                                           *
 * Input(s)           : pWssMsgStruct - Pointer to the input structure       *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfConfigUpdateReq (tRadioIfMsgStruct * pWssMsgStruct)
{
    tRadioIfGetDB       RadioIfGetDB;
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;
    tWssWlanMsgStruct  *pWssWlanMsgStruct = NULL;
#ifdef RFMGMT_WANTED
    tRfMgmtMsgStruct    RfMgmtMsgStruct;
#endif
    UINT1               u1IdCountIndex = 0;
    UINT1               u1BssIdCount = 0;
    INT4                i4RetVal = 0;

    pWssWlanMsgStruct =
        (tWssWlanMsgStruct *) (VOID *) UtlShMemAllocWssWlanBuf ();
    if (pWssWlanMsgStruct == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfConfigUpdateReq:- "
                     "UtlShMemAllocWssWlanBuf returned failure\n");
        return OSIX_FAILURE;
    }
    pWlcHdlrMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pWlcHdlrMsgStruct == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfConfigUpdateReq:- "
                     "UtlShMemAllocWlcBuf returned failure\n");
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        return OSIX_FAILURE;
    }

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));
    MEMSET (pWssWlanMsgStruct, 0, sizeof (tWssWlanMsgStruct));
#ifdef RFMGMT_WANTED
    MEMSET (&RfMgmtMsgStruct, 0, sizeof (tRfMgmtMsgStruct));
#endif

    /* Input Parameter Check */
    if (pWssMsgStruct == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfConfigUpdateReq: Null input received\n");
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;
    }

    /*Not Required, it can be deleted */
    /*To get IfIndex value from the input structure */
    if (pWssMsgStruct->unRadioIfMsg.RadioIfSetAdminOperStatus.isPresent
        == OSIX_FALSE)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfConfigiUpdateReq : Invalid input received\n");
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;
    }

    /* To get the RadioIfDB Value (Parameters needed here : BSSID COUNT 
     * and Admin Status )*/
    RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
        UtlShMemAllocAntennaSelectionBuf ();

    if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC, "\r%% Memory allocation for Antenna"
                     " selection failed.\r\n");
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;
    }
    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
        pWssMsgStruct->unRadioIfMsg.RadioIfSetAdminOperStatus.u4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bBssIdCount = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bAdminStatus = OSIX_TRUE;

    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, (&RadioIfGetDB))
        != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfConfigStateReq : RBTREE get of RadioIf DB failed \r\n");
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                         RadioIfGetAllDB.pu1AntennaSelection);
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;
    }

    /* If Admin Status is same , No Changes required, Return Success */
    if (RadioIfGetDB.RadioIfGetAllDB.u1AdminStatus ==
        pWssMsgStruct->unRadioIfMsg.RadioIfSetAdminOperStatus.u1AdminStatus)
    {
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                         RadioIfGetAllDB.pu1AntennaSelection);
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_SUCCESS;
    }
    else
    {
        RadioIfGetDB.RadioIfGetAllDB.u1AdminStatus =
            pWssMsgStruct->unRadioIfMsg.RadioIfSetAdminOperStatus.u1AdminStatus;
    }

    /*If Admin Status to be set is Down, then */
    if (pWssMsgStruct->unRadioIfMsg.RadioIfSetAdminOperStatus.u1AdminStatus
        == CFA_IF_DOWN)
    {
        /* For All the Bss present */
        u1BssIdCount = RadioIfGetDB.RadioIfGetAllDB.u1BssIdCount;

        for (u1IdCountIndex = 1; u1IdCountIndex <= u1BssIdCount;
             u1IdCountIndex++)
        {
            RadioIfGetDB.RadioIfGetAllDB.u1WlanId = u1IdCountIndex;
            RadioIfGetDB.RadioIfIsGetAllDB.bBssIfIndex = OSIX_TRUE;

            if (WssIfProcessRadioIfDBMsg (WSS_GET_WLAN_BSS_IFINDEX_DB,
                                          (&RadioIfGetDB)) != OSIX_SUCCESS)
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                             "ConfigUpdateReq: Retrieval BSSIF Index Failed\n");
                continue;
            }

            /*TO Delete the Wlans */
            pWssWlanMsgStruct->unWssWlanMsg.WssStaMsgStruct.
                unAuthMsg.AuthDelProfile.u4BssIfIndex =
                RadioIfGetDB.RadioIfGetAllDB.u4BssIfIndex;

#ifdef RFMGMT_WANTED
            RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtIntfConfigReq.u4RadioIfIndex =
                pWssMsgStruct->unRadioIfMsg.RadioIfSetAdminOperStatus.u4IfIndex;

            if (WssIfProcessRfMgmtMsg (RFMGMT_RADIO_DOWN_MSG,
                                       &RfMgmtMsgStruct) != OSIX_TRUE)
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                             "Update to RF module failed \r\n");
            }
#endif
            if (WssIfProcessWssStaMsg (WSS_STA_WLAN_DELETE,
                                       &(pWssWlanMsgStruct->
                                         unWssWlanMsg.WssStaMsgStruct)) !=
                OSIX_SUCCESS)
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                             "ConfigUpdateReq: Delete Station failed\n");
                continue;
            }
        }
    }

    /* Construct Msg to be sent to AP */
    if (RadioIfConstructAdminStatus
        (RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex,
         RadioIfGetDB.RadioIfGetAllDB.u1AdminStatus,
         pWssMsgStruct) != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "WssIfProcessRadioIfConstructMsg : Construct Msg Failed \r\n");
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                         RadioIfGetAllDB.pu1AntennaSelection);
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;
    }

    MEMCPY (&(pWlcHdlrMsgStruct->RadioIfConfigUpdateReq),
            &pWssMsgStruct->unRadioIfMsg.RadioIfConfigUpdateReq,
            sizeof (tRadioIfConfigUpdateReq));

    i4RetVal = WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_RADIO_CONF_UPDATE_REQ,
                                       pWlcHdlrMsgStruct);

    if (i4RetVal == RADIOIF_NO_AP_PRESENT)
    {
        gu1IsConfigResponseReceived = RADIOIF_NO_AP_PRESENT;
    }
    else if (i4RetVal != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "ConfigUpdateReq : To change Admin Status in AP failed \n");
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                         RadioIfGetAllDB.pu1AntennaSelection);
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;
    }

    /* Set Admin status in DB */
    RadioIfGetDB.RadioIfIsGetAllDB.bAdminStatus = OSIX_TRUE;

    if (WssIfProcessRadioIfDBMsg (WSS_SET_RADIO_IF_DB, &RadioIfGetDB)
        != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "ConfigUpdateReq : Value to be set in DB Failed \n");
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                         RadioIfGetAllDB.pu1AntennaSelection);
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;
    }

    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                     RadioIfGetAllDB.pu1AntennaSelection);
    UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfConfigUpdateResponse                          *
 *                                                                           *
 * Description        : This functions processes the response message recvd  *
 *                      from WTP                                             *
 *                                                                           *
 * Input(s)           : pWssMsgStruct - Pointer to the input structure       *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfConfigUpdateResponse (tRadioIfMsgStruct * pWssMsgStruct)
{
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tRadioIfGetDB       RadioIfGetDB;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    /* Input Parameter Check */
    if (pWssMsgStruct == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "ConfigUpdateRsp: Null input received\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    gu1IsConfigResponseReceived = (UINT1)
        pWssMsgStruct->unRadioIfMsg.RadioIfConfigUpdateRsp.u4ResultCode;
    if (pWssMsgStruct->unRadioIfMsg.RadioIfConfigUpdateRsp.u4ResultCode
        == OSIX_SUCCESS)
    {
        /* Reset the flag received. This flag will be set by each attibute if
         * there is a conflict between the values stored in DB and input
         * received */
        if (pWssMsgStruct->unRadioIfMsg.
            RadioIfConfigUpdateRsp.RadioIfOperStatus.isPresent != OSIX_FALSE)
        {
            pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;
            pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
                pWssMsgStruct->unRadioIfMsg.
                RadioIfConfigUpdateRsp.u2WtpInternalId;
            pWssIfCapwapDB->CapwapGetDB.u1RadioId =
                pWssMsgStruct->unRadioIfMsg.
                RadioIfConfigUpdateRsp.RadioIfOperStatus.u1RadioId;

            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
                != OSIX_SUCCESS)
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                             "ConfigUpdateRsp : Getting Radio index failed\n");
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }

            RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
                UtlShMemAllocAntennaSelectionBuf ();

            if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                             "\r%% Memory allocation for Antenna"
                             " selection failed.\r\n");
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
            RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
                pWssIfCapwapDB->CapwapGetDB.u4IfIndex;
            RadioIfGetDB.RadioIfIsGetAllDB.bOperStatus = OSIX_TRUE;
            RadioIfGetDB.RadioIfIsGetAllDB.bFailureStatus = OSIX_TRUE;

            RadioIfGetDB.RadioIfGetAllDB.u1OperStatus =
                pWssMsgStruct->unRadioIfMsg.
                RadioIfConfigUpdateRsp.RadioIfOperStatus.u1OperStatus;
            RadioIfGetDB.RadioIfGetAllDB.u1FailureStatus =
                pWssMsgStruct->unRadioIfMsg.
                RadioIfConfigUpdateRsp.RadioIfOperStatus.u1FailureCause;

            if (WssIfProcessRadioIfDBMsg (WSS_SET_RADIO_IF_DB, (&RadioIfGetDB))
                != OSIX_SUCCESS)
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                             "ConfigUpdateRsp: DB get of RadioIf DB failed \n");
                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                 RadioIfGetAllDB.
                                                 pu1AntennaSelection);
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
        }
    }
    else
    {
        if (pWssMsgStruct->unRadioIfMsg.RadioIfConfigUpdateRsp.u4ResultCode <
            MAX_CAPWAP_RETURN_CODE)
        {
            RADIOIF_TRC2 (RADIOIF_CRITICAL_TRC,
                          "\n ConfigUpdateResponse failed: Result Code %d, Error message %s\n",
                          pWssMsgStruct->unRadioIfMsg.
                          RadioIfConfigUpdateRsp.u4ResultCode,
                          gcp1CapwapReturnCode[pWssMsgStruct->
                                               unRadioIfMsg.
                                               RadioIfConfigUpdateRsp.
                                               u4ResultCode]);
        }
    }

    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfConfigStatusValidateReq                       *
 *                                                                           *
 * Description        : This functions validates the status request message  *
 *                      from WTP                                             *
 *                                                                           *
 * Input(s)           : pWssMsgStruct - Pointer to the input structure       *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfConfigStatusValidateReq (tRadioIfMsgStruct * pWssMsgStruct)
{
    UINT1               u1AntennaCount = 0;

    /* Input Parameter Check */
    if (pWssMsgStruct == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "ConfigStatusValidateReq: Null input received\n");
        return OSIX_FAILURE;
    }

    if (pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.RadioIfAdminStatus.
        isPresent != OSIX_FALSE)
    {
        if (pWssMsgStruct->unRadioIfMsg.
            RadioIfConfigStatusReq.RadioIfAdminStatus.u2MessageType !=
            RADIO_ADMIN_STATE_MSG_TYPE)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "ConfigStatusValidateReq: Invalid msg type recvd for Radio "
                         "Admin status, Validation failed\n");
            return OSIX_FAILURE;

        }
        if (pWssMsgStruct->unRadioIfMsg.
            RadioIfConfigStatusReq.RadioIfAdminStatus.u2MessageLength !=
            RADIO_ADMIN_STATE_MSG_LEN)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "ConfigStatusValidateReq: Invalid msg length recvd "
                         "for Radio Admin status, Validation failed\n");
            return OSIX_FAILURE;

        }
        if ((pWssMsgStruct->unRadioIfMsg.
             RadioIfConfigStatusReq.RadioIfAdminStatus.u1RadioId >
             MAX_NUM_OF_RADIO_PER_AP)
            || (pWssMsgStruct->unRadioIfMsg.
                RadioIfConfigStatusReq.RadioIfAdminStatus.u1RadioId == 0))
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "ConfigStatusValidateReq: Invalid Radio Id received, "
                         "Validation failed \r\n");
            return OSIX_FAILURE;
        }

        if ((pWssMsgStruct->unRadioIfMsg.
             RadioIfConfigStatusReq.RadioIfAdminStatus.u1AdminStatus !=
             CFA_IF_UP)
            && (pWssMsgStruct->unRadioIfMsg.
                RadioIfConfigStatusReq.RadioIfAdminStatus.u1AdminStatus !=
                CFA_IF_DOWN))
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "ConfigStatusValidateReq: Invalid value for admin status, "
                         "Validation failed \n");
            return OSIX_FAILURE;

        }
    }

    if (pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.
        RadioIfAntenna.isPresent != OSIX_FALSE)
    {
        if (pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.
            RadioIfAntenna.u2MessageType != RADIO_ANTENNA_MSG_TYPE)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "ConfigStatusValidateReq: Invalid msg type recvd for Radio "
                         "Antenna, Validation failed\n");
            return OSIX_FAILURE;

        }
        if (pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.
            RadioIfAntenna.u2MessageLength < RADIO_ANTENNA_MSG_MIN_LEN)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "ConfigStatusValidateReq: Invalid msg length received for "
                         "Radio Antenna, Validation failed\n");
            return OSIX_FAILURE;

        }
        if ((pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.RadioIfAntenna.
             u1RadioId > MAX_NUM_OF_RADIO_PER_AP)
            || (pWssMsgStruct->unRadioIfMsg.
                RadioIfConfigStatusReq.RadioIfAntenna.u1RadioId == 0))
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "ConfigStatusValidateReq: Invalid Radio Id received, "
                         "Validation failed \r\n");
            return OSIX_FAILURE;
        }

        if ((pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.
             RadioIfAntenna.u1ReceiveDiversity !=
             RADIO_ANTENNA_DIVERSITY_ENABLED)
            && (pWssMsgStruct->unRadioIfMsg.
                RadioIfConfigStatusReq.RadioIfAntenna.u1ReceiveDiversity !=
                RADIO_ANTENNA_DIVERSITY_DISABLED))
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "ConfigStatusValidateReq: Invalid diversity values for "
                         "antenna, Validation failed \r\n");
            return OSIX_FAILURE;
        }

        if (pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.RadioIfAntenna.
            u1AntennaMode > RADIO_ANTENNA_MODE_MAX)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "ConfigStatusValidateReq: Invalid diversity values for "
                         "antenna, Validation failed \n");
            return OSIX_FAILURE;
        }

        for (u1AntennaCount = 0;
             u1AntennaCount <
             pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.RadioIfAntenna.
             u1CurrentTxAntenna; u1AntennaCount++)
        {
            if ((pWssMsgStruct->unRadioIfMsg.
                 RadioIfConfigStatusReq.RadioIfAntenna.
                 au1AntennaSelection[u1AntennaCount] != RADIO_ANTENNA_INTERNAL)
                && (pWssMsgStruct->unRadioIfMsg.
                    RadioIfConfigStatusReq.RadioIfAntenna.
                    au1AntennaSelection[u1AntennaCount] !=
                    RADIO_ANTENNA_EXTERNAL))
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                             "ConfigStatusValidateReq: Invalid antenna type recvd, "
                             "Validation failed \n");
                return OSIX_FAILURE;
            }
        }
    }

    if (pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.
        RadioIfDSSPhy.isPresent != OSIX_FALSE)
    {
        if (pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.
            RadioIfDSSPhy.u2MessageType != RADIO_DSSS_MSG_TYPE)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "ConfigStatusValidateReq: Invalid message type recvd for "
                         "DSSS, Validation failed\n");
            return OSIX_FAILURE;

        }
        if (pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.
            RadioIfDSSPhy.u2MessageLength != RADIO_DSSS_MSG_LEN)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "ConfigStatusValidateReq: Invalid message length received "
                         "for DSSS, Validation failed\r\n");
            return OSIX_FAILURE;

        }
        if ((pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.
             RadioIfDSSPhy.u1RadioId > MAX_NUM_OF_RADIO_PER_AP)
            || (pWssMsgStruct->unRadioIfMsg.
                RadioIfConfigStatusReq.RadioIfDSSPhy.u1RadioId == 0))
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "ConfigStatusValidateReq: Invalid Radio Id recvd, "
                         "Validation failed \n");
            return OSIX_FAILURE;
        }
        if (pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.RadioIfDSSPhy.
            i1CurrentChannel > RADIO_MAX_DSSS_CHANNEL)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "ConfigStatusValidateReq: Invalid DSSS Channel received, "
                         "Validation failed \r\n");
            return OSIX_FAILURE;
        }
    }

    if (pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.RadioIfMacOperation.
        isPresent != OSIX_FALSE)
    {
        if (pWssMsgStruct->unRadioIfMsg.
            RadioIfConfigStatusReq.RadioIfMacOperation.u2MessageType !=
            RADIO_MAC_OPER_MSG_TYPE)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "ConfigStatusValidateReq: Invalid message type recvd for "
                         "MAC Table, Validation failed\r\n");
            return OSIX_FAILURE;
        }

        if (pWssMsgStruct->unRadioIfMsg.
            RadioIfConfigStatusReq.RadioIfMacOperation.u2MessageLength !=
            RADIO_MAC_OPER_MSG_LEN)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "ConfigStatusValidateReq: Invalid message length recvd "
                         "for MAC Table, Validation failed\r\n");
            return OSIX_FAILURE;

        }
        if ((pWssMsgStruct->unRadioIfMsg.
             RadioIfConfigStatusReq.RadioIfMacOperation.u1RadioId >
             MAX_NUM_OF_RADIO_PER_AP)
            || (pWssMsgStruct->unRadioIfMsg.
                RadioIfConfigStatusReq.RadioIfMacOperation.u1RadioId == 0))
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "ConfigStatusValidateReq: Invalid Radio Id received, "
                         "Validation failed \r\n");
            return OSIX_FAILURE;
        }
        if (pWssMsgStruct->unRadioIfMsg.
            RadioIfConfigStatusReq.RadioIfMacOperation.u2RTSThreshold >
            RADIO_MAX_RTS_THRESHOLD)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "ConfigStatusValidateReq: Invalid RTS Threshold received, "
                         "Validation failed \r\n");
            return OSIX_FAILURE;
        }
        if ((pWssMsgStruct->unRadioIfMsg.
             RadioIfConfigStatusReq.RadioIfMacOperation.u2FragmentThreshold <
             RADIO_MIN_FRAGMENT_THRESHOLD)
            || (pWssMsgStruct->unRadioIfMsg.
                RadioIfConfigStatusReq.RadioIfMacOperation.u2FragmentThreshold >
                RADIO_MAX_FRAGMENT_THRESHOLD))
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "ConfigStatusValidateReq: Invalid Fragment Threshold "
                         "received, Validation failed \r\n");
            return OSIX_FAILURE;
        }
    }
    if (pWssMsgStruct->unRadioIfMsg.
        RadioIfConfigStatusReq.RadioIfMultiDomainCap.isPresent != OSIX_FALSE)
    {
        if (pWssMsgStruct->unRadioIfMsg.
            RadioIfConfigStatusReq.RadioIfMultiDomainCap.u2MessageType !=
            RADIO_MULTI_DOMAIN_MSG_TYPE)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "ConfigStatusValidateReq: Invalid msg type recvd for "
                         "Multidomain Table, Validation failed\r\n");
            return OSIX_FAILURE;

        }
        if (pWssMsgStruct->unRadioIfMsg.
            RadioIfConfigStatusReq.RadioIfMultiDomainCap.u2MessageLength !=
            RADIO_MULTI_DOMAIN_MSG_LEN)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "ConfigStatusValidateReq: Invalid msg length received "
                         "for Multidomain Table, Validation failed\r\n");
            return OSIX_FAILURE;

        }
        if ((pWssMsgStruct->unRadioIfMsg.
             RadioIfConfigStatusReq.RadioIfMultiDomainCap.u1RadioId >
             MAX_NUM_OF_RADIO_PER_AP)
            || (pWssMsgStruct->
                unRadioIfMsg.RadioIfConfigStatusReq.RadioIfMultiDomainCap.
                u1RadioId == 0))
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "ConfigStatusValidateReq: Invalid Radio Id received, "
                         "Validation failed \r\n");
            return OSIX_FAILURE;
        }
    }

    if (pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.
        RadioIfOFDMPhy.isPresent != OSIX_FALSE)
    {
        if (pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.
            RadioIfOFDMPhy.u2MessageType != RADIO_OFDM_MSG_TYPE)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "ConfigStatusValidateReq: Invalid msg type recvd for OFDM, "
                         "Validation failed\r\n");
            return OSIX_FAILURE;

        }
        if (pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.
            RadioIfOFDMPhy.u2MessageLength != RADIO_OFDM_MSG_LEN)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "ConfigStatusValidateReq: Invalid msg length received for "
                         "OFDM, Validation failed\r\n");
            return OSIX_FAILURE;

        }
        if ((pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.
             RadioIfOFDMPhy.u1RadioId > MAX_NUM_OF_RADIO_PER_AP)
            || (pWssMsgStruct->unRadioIfMsg.
                RadioIfConfigStatusReq.RadioIfOFDMPhy.u1RadioId == 0))
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "ConfigStatusValidateReq : Invalid Radio Id received, "
                         "Validation failed \r\n");
            return OSIX_FAILURE;
        }
        if (pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.
            RadioIfOFDMPhy.u1CurrentChannel > RADIO_MAX_OFDM_CHANNEL)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "ConfigStatusValidateReq: Invalid OFDM Channel received, "
                         "Validation failed \r\n");
            return OSIX_FAILURE;
        }
    }
    if (pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.RadioIfSupportedRate.
        isPresent != OSIX_FALSE)
    {
        if (pWssMsgStruct->unRadioIfMsg.
            RadioIfConfigStatusReq.RadioIfSupportedRate.u2MessageType !=
            RADIO_SUPPORTED_RATE_MSG_TYPE)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "ConfigStatusValidateReq: Invalid message type recvd for "
                         "Supp Rate, Validation failed\r\n");
            return OSIX_FAILURE;

        }
        if (pWssMsgStruct->unRadioIfMsg.
            RadioIfConfigStatusReq.RadioIfSupportedRate.u2MessageLength <
            RADIO_SUPPORTED_RATE_MSG_LEN)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "ConfigStatusValidateReq: Invalid message length recvd for "
                         "Supp Rate, Validation failed\r\n");
            return OSIX_FAILURE;

        }
        if ((pWssMsgStruct->unRadioIfMsg.
             RadioIfConfigStatusReq.RadioIfSupportedRate.u1RadioId >
             MAX_NUM_OF_RADIO_PER_AP)
            || (pWssMsgStruct->unRadioIfMsg.
                RadioIfConfigStatusReq.RadioIfSupportedRate.u1RadioId == 0))
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "ConfigStatusValidateReq: Invalid Radio Id received, "
                         "Validation failed \r\n");
            return OSIX_FAILURE;
        }
    }
    if (pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.
        RadioIfTxPower.isPresent != OSIX_FALSE)
    {
        if (pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.RadioIfTxPower.
            u2MessageType != RADIO_TXPOWER_MSG_TYPE)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "ConfigStatusValidateReq: Invalid message type recvd for "
                         "Tx Power, Validation failed\r\n");
            return OSIX_FAILURE;

        }
        if (pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.
            RadioIfTxPower.u2MessageLength != RADIO_TXPOWER_MSG_LEN)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "ConfigStatusValidateReq: Invalid msg length received for "
                         "Tx Power, Validation failed\r\n");
            return OSIX_FAILURE;
        }

        if ((pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.RadioIfTxPower.
             u1RadioId > MAX_NUM_OF_RADIO_PER_AP)
            || (pWssMsgStruct->unRadioIfMsg.
                RadioIfConfigStatusReq.RadioIfTxPower.u1RadioId == 0))
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "ConfigStatusValidateReq: Invalid Radio Id received, "
                         "Validation failed \r\n");
            return OSIX_FAILURE;
        }
    }
    if (pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.RadioIfTxPowerLevel.
        isPresent != OSIX_FALSE)
    {
        if (pWssMsgStruct->unRadioIfMsg.
            RadioIfConfigStatusReq.RadioIfTxPowerLevel.u2MessageType !=
            RADIO_TXPOWER_LEVEL_MSG_TYPE)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "ConfigStatusValidateReq: Invalid msg type received for "
                         "Tx Power Level, Validation failed\r\n");
            return OSIX_FAILURE;

        }
        if (pWssMsgStruct->unRadioIfMsg.
            RadioIfConfigStatusReq.RadioIfTxPowerLevel.u2MessageLength <
            RADIO_TXPOWER_LEVEL_MSG_LEN)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "ConfigStatusValidateReq : Invalid msg length recvd for Tx "
                         "Power, Validation failed\r\n");
            return OSIX_FAILURE;
        }
        if ((pWssMsgStruct->unRadioIfMsg.
             RadioIfConfigStatusReq.RadioIfTxPowerLevel.u1RadioId >
             MAX_NUM_OF_RADIO_PER_AP)
            || (pWssMsgStruct->unRadioIfMsg.
                RadioIfConfigStatusReq.RadioIfTxPowerLevel.u1RadioId == 0))
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "ConfigStatusValidateReq: Invalid Radio Id received, "
                         "Validation failed \r\n");
            return OSIX_FAILURE;
        }
    }
    if (pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.
        RadioIfConfig.isPresent != OSIX_FALSE)
    {
        if (pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.RadioIfConfig.
            u2MessageType != RADIO_CONFIG_MSG_TYPE)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "ConfigStatusValidateReq: Invalid message type recvd for "
                         "Radio Config, Validation failed\r\n");
            return OSIX_FAILURE;
        }
        if (pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.
            RadioIfConfig.u2MessageLength != RADIO_CONFIG_MSG_LEN)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "ConfigStatusValidateReq : Invalid msg length recvd for "
                         "Radio Config, Validation failed\r\n");
            return OSIX_FAILURE;
        }
        if ((pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.
             RadioIfConfig.u1RadioId > MAX_NUM_OF_RADIO_PER_AP)
            || (pWssMsgStruct->unRadioIfMsg.
                RadioIfConfigStatusReq.RadioIfConfig.u1RadioId == 0))
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "ConfigStatusValidateReq: Invalid Radio Id received, "
                         "Validation failed \r\n");
            return OSIX_FAILURE;
        }
    }
    if (pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.
        RadioIfInfo.isPresent != OSIX_FALSE)
    {
        if (pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.
            RadioIfInfo.u2MessageType != RADIO_INFO_MSG_TYPE)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "ConfigStatusValidateReq: Invalid message type recvd for "
                         "Radio Info, Validation failed\r\n");
            return OSIX_FAILURE;
        }
        if (pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.
            RadioIfInfo.u2MessageLength != RADIO_INFO_MSG_LEN)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "ConfigStatusValidateReq: Invalid message length recvd for "
                         "Radio Info, Validation failed\r\n");
            return OSIX_FAILURE;
        }
        if ((pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.
             RadioIfInfo.u1RadioId > MAX_NUM_OF_RADIO_PER_AP)
            || (pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.RadioIfInfo.
                u1RadioId == 0))
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "ConfigStatusValidateReq: Invalid Radio Id received, "
                         "Validation failed \r\n");
            return OSIX_FAILURE;
        }
        if ((pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.
             RadioIfInfo.u4RadioType != RADIO_TYPE_A)
            && (pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.RadioIfInfo.
                u4RadioType != RADIO_TYPE_B)
            && (pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.RadioIfInfo.
                u4RadioType != RADIO_TYPE_G)
            && (pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.RadioIfInfo.
                u4RadioType != RADIO_TYPE_AN)
            && (pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.RadioIfInfo.
                u4RadioType != RADIO_TYPE_BG)
            && (pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.RadioIfInfo.
                u4RadioType != RADIO_TYPE_BGN)
            && (pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.RadioIfInfo.
                u4RadioType != RADIO_TYPE_AC))
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "ConfigStatusValidateReq : Invalid Radio Type recevied, "
                         "Validation failed \r\n");
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfConfigStatusReq                               *
 *                                                                           *
 * Description        : This functions processes Config Status Request from  *
 *                      from WTP                                             *
 *                                                                           *
 * Input(s)           : pWssMsgStruct - Pointer to the input structure       *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfConfigStatusReq (tRadioIfMsgStruct * pWssMsgStruct)
{
    UINT1               u1AntennaCount = 0;
    tRadioIfGetDB       RadioIfGetDB;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
#ifdef RFMGMT_WANTED
    tRfMgmtMsgStruct    RfMgmtMsgStruct;
#endif
    UINT1               au1MCSRateSet[RADIOIF_11N_MCS_RATE_LEN];
    INT4                i4SupportedRate = 0;
    UINT2               u2PowerLevel = 0;
    UINT2               u2MultiDomainLoopIndex = 0;
    UINT1               u1Antennaindex = 0;
    UINT1               u1MultiDomainIndex = 0;
    UINT1               u1MultiDomainCapIndex = 1;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    MEMSET (au1MCSRateSet, 0, RADIOIF_11N_MCS_RATE_LEN);
#ifdef RFMGMT_WANTED
    MEMSET (&RfMgmtMsgStruct, 0, sizeof (tRfMgmtMsgStruct));
#endif
    /*MEMSET (&au1OperRate, 0, RADIOIF_OPER_RATE); */

    /* Input Parameter Check */
    if (pWssMsgStruct == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfConfigStatusReq : Null input received\r\n");
        return OSIX_FAILURE;
    }
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
        UtlShMemAllocAntennaSelectionBuf ();

    if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC, "\r%% Memory allocation for Antenna"
                     " selection failed.\r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    if (pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.RadioIfAdminStatus.
        isPresent != OSIX_FALSE)
    {
        /* Reset the flag received. This flag will be set by each attibute if
         * there is a conflict between the values stored in DB and input
         * received */
        pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.RadioIfAdminStatus.
            isPresent = OSIX_FALSE;

        pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;
        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
            pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.u2WtpInternalId;
        pWssIfCapwapDB->CapwapGetDB.u1RadioId =
            pWssMsgStruct->unRadioIfMsg.
            RadioIfConfigStatusReq.RadioIfAdminStatus.u1RadioId;

        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
            != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigStatusReq : Getting Radio index failed\r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }

        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
            pWssIfCapwapDB->CapwapGetDB.u4IfIndex;
        RadioIfGetDB.RadioIfIsGetAllDB.bAdminStatus = OSIX_TRUE;

        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, (&RadioIfGetDB))
            != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigStatusReq : Radio DB get failed \r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }

        /* If the existing value in DB and the request are same value then
         * clear the boolean value */
        if (pWssMsgStruct->unRadioIfMsg.
            RadioIfConfigStatusReq.RadioIfAdminStatus.u1AdminStatus !=
            RadioIfGetDB.RadioIfGetAllDB.u1AdminStatus)
        {
            pWssMsgStruct->unRadioIfMsg.
                RadioIfConfigStatusReq.RadioIfAdminStatus.isPresent |=
                OSIX_TRUE;
        }
        RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
        RadioIfGetDB.RadioIfGetAllDB.u1RadioId =
            pWssMsgStruct->unRadioIfMsg.
            RadioIfConfigStatusReq.RadioIfAdminStatus.u1RadioId;
        if (WssIfProcessRadioIfDBMsg (WSS_SET_RADIO_IF_DB, (&RadioIfGetDB)) !=
            OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigStatusReq : Set of Radio DB failed \r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;

        }
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                         RadioIfGetAllDB.pu1AntennaSelection);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_SUCCESS;
    }

    if (pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.
        RadioIfAntenna.isPresent != OSIX_FALSE)
    {
        /* Reset the flag received. This flag will be set by each attibute if
         * there is a conflict between the values stored in DB and input
         * received */
        pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.
            RadioIfAntenna.isPresent = OSIX_FALSE;

        pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;
        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
            pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.u2WtpInternalId;
        pWssIfCapwapDB->CapwapGetDB.u1RadioId =
            pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.RadioIfAntenna.
            u1RadioId;

        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
            != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigStatusReq : Getting Radio index failed\r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }

        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
            pWssIfCapwapDB->CapwapGetDB.u4IfIndex;

        RadioIfGetDB.RadioIfIsGetAllDB.bDiversitySupport = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bAntennaMode = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bCurrentTxAntenna = OSIX_TRUE;

        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, (&RadioIfGetDB))
            != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigStatusReq : Get of Radio DB failed \r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }

        if (pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.
            RadioIfAntenna.u1ReceiveDiversity !=
            RadioIfGetDB.RadioIfGetAllDB.u1DiversitySupport)
        {
            pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.
                RadioIfAntenna.isPresent |= OSIX_TRUE;
        }

        if (pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.
            RadioIfAntenna.u1AntennaMode !=
            RadioIfGetDB.RadioIfGetAllDB.u1AntennaMode)
        {
            pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.
                RadioIfAntenna.isPresent |= OSIX_TRUE;
        }

        if (pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.RadioIfAntenna.
            u1CurrentTxAntenna != 0)
        {
            RadioIfGetDB.RadioIfGetAllDB.u1CurrentTxAntenna =
                pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.
                RadioIfAntenna.u1CurrentTxAntenna;

            for (u1Antennaindex = 0;
                 u1Antennaindex <
                 RadioIfGetDB.RadioIfGetAllDB.u1CurrentTxAntenna;
                 u1Antennaindex++)
            {
                RadioIfGetDB.RadioIfIsGetAllDB.bAntennaSelection[u1Antennaindex]
                    = OSIX_TRUE;
            }
        }

        RadioIfGetDB.RadioIfIsGetAllDB.bCurrentTxAntenna = OSIX_FALSE;

        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, (&RadioIfGetDB))
            != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigStatusReq: Get of Radio DB failed \r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }

        for (u1AntennaCount = 0;
             u1AntennaCount <
             pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.RadioIfAntenna.
             u1CurrentTxAntenna; u1AntennaCount++)
        {
            if (pWssMsgStruct->unRadioIfMsg.
                RadioIfConfigStatusReq.RadioIfAntenna.
                au1AntennaSelection[u1AntennaCount] !=
                RadioIfGetDB.RadioIfGetAllDB.
                pu1AntennaSelection[u1AntennaCount])
            {
                pWssMsgStruct->unRadioIfMsg.
                    RadioIfConfigStatusReq.RadioIfAntenna.isPresent |=
                    OSIX_TRUE;
            }
        }
        RadioIfGetDB.RadioIfGetAllDB.u1DiversitySupport =
            pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.RadioIfAntenna.
            u1ReceiveDiversity;
        RadioIfGetDB.RadioIfIsGetAllDB.bDiversitySupport = OSIX_TRUE;
        RadioIfGetDB.RadioIfGetAllDB.u1AntennaMode =
            pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.RadioIfAntenna.
            u1AntennaMode;
        RadioIfGetDB.RadioIfIsGetAllDB.bAntennaMode = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bCurrentTxAntenna = OSIX_TRUE;

        if (WssIfProcessRadioIfDBMsg (WSS_SET_RADIO_IF_DB, (&RadioIfGetDB))
            != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigStatusReq : Set of RadioIf DB failed \r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;

        }
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                         RadioIfGetAllDB.pu1AntennaSelection);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_SUCCESS;
    }

    if (pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.
        RadioIfDSSPhy.isPresent != OSIX_FALSE)
    {
        /* Reset the flag received. This flag will be set by each attibute if
         * there is a conflict between the values stored in DB and input
         * received */
        pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.
            RadioIfDSSPhy.isPresent = OSIX_FALSE;

        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
        pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;
        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
            pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.u2WtpInternalId;
        pWssIfCapwapDB->CapwapGetDB.u1RadioId =
            pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.RadioIfDSSPhy.
            u1RadioId;

        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
            != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigStatusReq : Getting Radio index failed\r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }

        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
            pWssIfCapwapDB->CapwapGetDB.u4IfIndex;

        RadioIfGetDB.RadioIfIsGetAllDB.bCurrentChannel = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bEDThreshold = OSIX_TRUE;

        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, (&RadioIfGetDB))
            != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigStatusReq: Get of Radio DB failed \r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }

        if (pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.
            RadioIfDSSPhy.i1CurrentChannel !=
            RadioIfGetDB.RadioIfGetAllDB.u1CurrentChannel)
        {
            pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.
                RadioIfDSSPhy.isPresent |= OSIX_TRUE;
        }

        /* Copy the Current CCA MOde from the request message */
        RadioIfGetDB.RadioIfIsGetAllDB.bCurrentCCAMode = OSIX_TRUE;
        RadioIfGetDB.RadioIfGetAllDB.u1CurrentCCAMode = (UINT1)
            pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.RadioIfDSSPhy.
            i1CurrentCCAMode;

        if (pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.
            RadioIfDSSPhy.i4EDThreshold !=
            RadioIfGetDB.RadioIfGetAllDB.i4EDThreshold)
        {
            pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.
                RadioIfDSSPhy.isPresent |= OSIX_TRUE;
        }
        if (WssIfProcessRadioIfDBMsg (WSS_SET_RADIO_IF_DB, (&RadioIfGetDB))
            != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigStatusReq: Set of Radio DB failed \r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                         RadioIfGetAllDB.pu1AntennaSelection);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_SUCCESS;
    }

    if (pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.RadioIfInfoElem.
        isPresent != OSIX_FALSE)
    {
        /* Reset the flag received. This flag will be set by each attibute if
         * there is a conflict between the values stored in DB and input
         * received */
        pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.
            RadioIfInfoElem.isPresent = OSIX_FALSE;

        pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;
        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
            pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.u2WtpInternalId;
        pWssIfCapwapDB->CapwapGetDB.u1RadioId =
            pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.RadioIfInfoElem.
            u1RadioId;

        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
            != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigStatusReq: Getting Radio index failed\r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }

        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
            pWssIfCapwapDB->CapwapGetDB.u4IfIndex;

        switch (pWssMsgStruct->unRadioIfMsg.
                RadioIfConfigStatusReq.RadioIfInfoElem.u1EleId)
        {
            case DOT11_INFO_ELEM_HTCAPABILITY:
                /*11HT params Config status update processing */
                if (RadioIfProcess11nStatusReq ((&RadioIfGetDB),
                                                pWssMsgStruct) != OSIX_SUCCESS)
                {
                    RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                                 "RadioIfConfigStatusReq: Failed to process"
                                 " the received 11N params \r\n");
                    UtlShMemFreeAntennaSelectionBuf
                        (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    return OSIX_FAILURE;
                }
                break;
            case DOT11_INFO_ELEM_HTOPERATION:
                pWssMsgStruct->unRadioIfMsg.
                    RadioIfConfigStatusReq.RadioIfInfoElem.isPresent |=
                    OSIX_TRUE;
                pWssMsgStruct->unRadioIfMsg.
                    RadioIfConfigStatusReq.RadioIfInfoElem.unInfoElem.
                    HTOperation.isOptional |= OSIX_TRUE;
                break;

            case DOT11AC_INFO_ELEM_VHTCAPABILITY:
                /*11AC params Config status update processing */
                if (RadioIfProcess11AcStatusReq ((&RadioIfGetDB),
                                                 pWssMsgStruct) != OSIX_SUCCESS)
                {
                    RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                                 "RadioIfConfigStatusReq: Failed to process"
                                 " the received 11AC params \r\n");
                    UtlShMemFreeAntennaSelectionBuf
                        (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    return OSIX_FAILURE;

                }
                break;

            case DOT11AC_INFO_ELEM_VHTOPERATION:
                /*Configured Vht oper info has to be sent from WLC, 
                 *in staus response without any comparison
                 * with status request message elements.*/
                pWssMsgStruct->unRadioIfMsg.
                    RadioIfConfigStatusReq.RadioIfInfoElem.isPresent |=
                    OSIX_TRUE;
                pWssMsgStruct->unRadioIfMsg.
                    RadioIfConfigStatusReq.RadioIfInfoElem.unInfoElem.
                    VHTOperation.isOptional |= OSIX_TRUE;

                break;

            default:
                break;

        }

        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                         RadioIfGetAllDB.pu1AntennaSelection);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_SUCCESS;
    }

    if (pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.RadioIfMacOperation.
        isPresent != OSIX_FALSE)
    {
        /* Reset the flag received. This flag will be set by each attibute if
         * there is a conflict between the values stored in DB and input
         * received */
        pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.
            RadioIfMacOperation.isPresent = OSIX_FALSE;

        pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;
        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
            pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.u2WtpInternalId;
        pWssIfCapwapDB->CapwapGetDB.u1RadioId =
            pWssMsgStruct->unRadioIfMsg.
            RadioIfConfigStatusReq.RadioIfMacOperation.u1RadioId;

        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
            != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigStatusReq: Getting Radio index failed\r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }

        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
            pWssIfCapwapDB->CapwapGetDB.u4IfIndex;

        RadioIfGetDB.RadioIfIsGetAllDB.bRTSThreshold = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bShortRetry = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bLongRetry = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bFragmentationThreshold = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bTxMsduLifetime = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bRxMsduLifetime = OSIX_TRUE;

        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, (&RadioIfGetDB))
            != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigStatusReq: Get of Radio DB failed \n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
        if (pWssMsgStruct->unRadioIfMsg.
            RadioIfConfigStatusReq.RadioIfMacOperation.u2RTSThreshold !=
            RadioIfGetDB.RadioIfGetAllDB.u2RTSThreshold)
        {
            pWssMsgStruct->unRadioIfMsg.
                RadioIfConfigStatusReq.RadioIfMacOperation.isPresent |=
                OSIX_TRUE;
        }

        if (pWssMsgStruct->unRadioIfMsg.
            RadioIfConfigStatusReq.RadioIfMacOperation.u1ShortRetry !=
            RadioIfGetDB.RadioIfGetAllDB.u1ShortRetry)
        {
            pWssMsgStruct->unRadioIfMsg.
                RadioIfConfigStatusReq.RadioIfMacOperation.isPresent |=
                OSIX_TRUE;
        }

        if (pWssMsgStruct->unRadioIfMsg.
            RadioIfConfigStatusReq.RadioIfMacOperation.u1LongRetry !=
            RadioIfGetDB.RadioIfGetAllDB.u1LongRetry)
        {
            pWssMsgStruct->unRadioIfMsg.
                RadioIfConfigStatusReq.RadioIfMacOperation.isPresent |=
                OSIX_TRUE;
        }

        if (pWssMsgStruct->unRadioIfMsg.
            RadioIfConfigStatusReq.RadioIfMacOperation.u2FragmentThreshold !=
            RadioIfGetDB.RadioIfGetAllDB.u2FragmentationThreshold)
        {
            pWssMsgStruct->unRadioIfMsg.
                RadioIfConfigStatusReq.RadioIfMacOperation.isPresent |=
                OSIX_TRUE;
        }

        if (pWssMsgStruct->unRadioIfMsg.
            RadioIfConfigStatusReq.RadioIfMacOperation.u4RxMsduLifetime !=
            RadioIfGetDB.RadioIfGetAllDB.u4RxMsduLifetime)
        {
            pWssMsgStruct->unRadioIfMsg.
                RadioIfConfigStatusReq.RadioIfMacOperation.isPresent |=
                OSIX_TRUE;
        }

        if (pWssMsgStruct->unRadioIfMsg.
            RadioIfConfigStatusReq.RadioIfMacOperation.u4TxMsduLifetime !=
            RadioIfGetDB.RadioIfGetAllDB.u4TxMsduLifetime)
        {
            pWssMsgStruct->unRadioIfMsg.
                RadioIfConfigStatusReq.RadioIfMacOperation.isPresent |=
                OSIX_TRUE;
        }
        if (WssIfProcessRadioIfDBMsg (WSS_SET_RADIO_IF_DB, (&RadioIfGetDB))
            != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigStatusReq: Get of Radio DB failed \r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;

        }
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                         RadioIfGetAllDB.pu1AntennaSelection);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_SUCCESS;
    }

    if (pWssMsgStruct->unRadioIfMsg.
        RadioIfConfigStatusReq.RadioIfMultiDomainCap.isPresent != OSIX_FALSE)
    {
        /* Reset the flag received. This flag will be set by each attibute if
         * there is a conflict between the values stored in DB and input
         * received */
        pWssMsgStruct->unRadioIfMsg.
            RadioIfConfigStatusReq.RadioIfMultiDomainCap.isPresent = OSIX_FALSE;

        pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;
        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
            pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.u2WtpInternalId;
        pWssIfCapwapDB->CapwapGetDB.u1RadioId =
            pWssMsgStruct->unRadioIfMsg.
            RadioIfConfigStatusReq.RadioIfMultiDomainCap.u1RadioId;

        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
            != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigStatusReq : Getting Radio index failed\r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }

        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
            pWssIfCapwapDB->CapwapGetDB.u4IfIndex;

        RadioIfGetDB.RadioIfIsGetAllDB.bMultiDomainInfo = OSIX_TRUE;

        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, (&RadioIfGetDB))
            != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigStatusReq: Get of Radio DB failed \r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }

        for (u1MultiDomainIndex = 1; u1MultiDomainIndex <=
             MAX_MULTIDOMAIN_INFO_ELEMENT; u1MultiDomainIndex++)
        {
            if (RadioIfGetDB.RadioIfGetAllDB.
                MultiDomainInfo[u1MultiDomainIndex - 1].u2FirstChannelNo == 0)
            {
                u1MultiDomainCapIndex = u1MultiDomainIndex;
                break;
            }
        }
        u1MultiDomainIndex = u1MultiDomainCapIndex;
        RadioIfGetDB.RadioIfGetAllDB.
            MultiDomainInfo[u1MultiDomainIndex - 1].u2FirstChannelNo =
            pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.
            RadioIfMultiDomainCap.u2FirstChannel;

        RadioIfGetDB.RadioIfGetAllDB.
            MultiDomainInfo[u1MultiDomainIndex - 1].u2NoOfChannels =
            pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.
            RadioIfMultiDomainCap.u2NumOfChannels;

        RadioIfGetDB.RadioIfGetAllDB.
            MultiDomainInfo[u1MultiDomainIndex - 1].u2MaxTxPowerLevel =
            pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.
            RadioIfMultiDomainCap.u2MaxTxPowerLevel;

        RadioIfGetDB.RadioIfIsGetAllDB.bMultiDomainInfo = OSIX_TRUE;
        if (WssIfProcessRadioIfDBMsg (WSS_SET_RADIO_IF_DB,
                                      (&RadioIfGetDB)) != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigStatusReq: Set of Radio DB failed \n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;

        }
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                         RadioIfGetAllDB.pu1AntennaSelection);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_SUCCESS;
    }

    if (pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.RadioIfOFDMPhy.
        isPresent != OSIX_FALSE)
    {
        /* Reset the flag received. This flag will be set by each attibute if
         * there is a conflict between the values stored in DB and input
         * received */
        pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.RadioIfOFDMPhy.
            isPresent = OSIX_FALSE;

        pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;
        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
            pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.u2WtpInternalId;
        pWssIfCapwapDB->CapwapGetDB.u1RadioId =
            pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.RadioIfOFDMPhy.
            u1RadioId;

        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
            != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigStatusReq : Getting Radio index failed\r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }

        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
            pWssIfCapwapDB->CapwapGetDB.u4IfIndex;

        RadioIfGetDB.RadioIfIsGetAllDB.bCurrentChannel = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bT1Threshold = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bSupportedBand = OSIX_TRUE;

        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                      (&RadioIfGetDB)) != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigStatusReq: Get of RadioIf DB failed \r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }

        if (pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.RadioIfOFDMPhy.
            u1CurrentChannel != RadioIfGetDB.RadioIfGetAllDB.u1CurrentChannel)
        {
            pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.RadioIfOFDMPhy.
                isPresent |= OSIX_TRUE;
        }

        if (pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.RadioIfOFDMPhy.
            u4T1Threshold != RadioIfGetDB.RadioIfGetAllDB.u4T1Threshold)
        {
            pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.RadioIfOFDMPhy.
                isPresent |= OSIX_TRUE;
        }

        RadioIfGetDB.RadioIfGetAllDB.u1SupportedBand =
            pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.RadioIfOFDMPhy.
            u1SupportedBand;

        if (pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.RadioIfOFDMPhy.
            u1SupportedBand != RadioIfGetDB.RadioIfGetAllDB.u1SupportedBand)
        {
            pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.RadioIfOFDMPhy.
                isPresent |= OSIX_TRUE;
        }

        if (WssIfProcessRadioIfDBMsg (WSS_SET_RADIO_IF_DB, (&RadioIfGetDB))
            != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigStatusReq: Set of Radio DB failed \r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;

        }
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                         RadioIfGetAllDB.pu1AntennaSelection);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_SUCCESS;

    }
    if (pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.RadioIfSupportedRate.
        isPresent != OSIX_FALSE)
    {
        /* Reset the flag received. This flag will be set by each attibute if
         * there is a conflict between the values stored in DB and input
         * received */
        pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.RadioIfSupportedRate.
            isPresent = OSIX_FALSE;

        pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;
        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
            pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.u2WtpInternalId;
        pWssIfCapwapDB->CapwapGetDB.u1RadioId =
            pWssMsgStruct->unRadioIfMsg.
            RadioIfConfigStatusReq.RadioIfSupportedRate.u1RadioId;

        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
            != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigStatusReq : Getting Radio index failed\r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }

        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
            pWssIfCapwapDB->CapwapGetDB.u4IfIndex;
        RadioIfGetDB.RadioIfIsGetAllDB.bSupportedRate = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bOperationalRate = OSIX_TRUE;

        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, (&RadioIfGetDB))
            != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigStatusReq : Radio DB get failed \r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }

        for (i4SupportedRate = 0; i4SupportedRate < RADIOIF_OPER_RATE;
             i4SupportedRate++)
        {
            RadioIfGetDB.RadioIfGetAllDB.au1SupportedRate[i4SupportedRate] =
                pWssMsgStruct->unRadioIfMsg.
                RadioIfConfigStatusReq.RadioIfSupportedRate.
                au1SupportedRate[i4SupportedRate];
        }
        if (MEMCMP
            (RadioIfGetDB.RadioIfGetAllDB.au1OperationalRate,
             "\0\0\0\0\0\0\0\0",
             sizeof (RadioIfGetDB.RadioIfGetAllDB.au1OperationalRate)) != 0)
        {
            pWssMsgStruct->unRadioIfMsg.
                RadioIfConfigStatusReq.RadioIfSupportedRate.isPresent |=
                OSIX_TRUE;
        }
        if (WssIfProcessRadioIfDBMsg (WSS_SET_RADIO_IF_DB, (&RadioIfGetDB))
            != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigStatusReq: Set of Radio DB failed \r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;

        }
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                         RadioIfGetAllDB.pu1AntennaSelection);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_SUCCESS;

    }
    if (pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.RadioIfTxPower.
        isPresent != OSIX_FALSE)
    {
        /* Reset the flag received. This flag will be set by each attibute if
         * there is a conflict between the values stored in DB and input
         * received */
        pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.
            RadioIfTxPower.isPresent = OSIX_FALSE;

        pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;
        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
            pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.u2WtpInternalId;
        pWssIfCapwapDB->CapwapGetDB.u1RadioId =
            pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.RadioIfTxPower.
            u1RadioId;

        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
            != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigStatusReq : Getting Radio index failed\r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }

        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
            pWssIfCapwapDB->CapwapGetDB.u4IfIndex;

        RadioIfGetDB.RadioIfIsGetAllDB.bCurrentTxPower = OSIX_TRUE;

        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, (&RadioIfGetDB))
            != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigStatusReq: Get of RadioIf DB failed \r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }

        if ((RadioIfGetDB.RadioIfGetAllDB.i2CurrentTxPower != pWssMsgStruct->
             unRadioIfMsg.RadioIfConfigStatusReq.RadioIfTxPower.u2TxPowerLevel)
            && (RadioIfGetDB.RadioIfGetAllDB.i2CurrentTxPower != 0))
        {
            pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.
                RadioIfTxPower.isPresent = OSIX_TRUE;
        }
        else
        {
            RadioIfGetDB.RadioIfIsGetAllDB.bCurrentTxPower = OSIX_TRUE;
            RadioIfGetDB.RadioIfIsGetAllDB.bMaxTxPowerLevel = OSIX_TRUE;
            RadioIfGetDB.RadioIfGetAllDB.u2MaxTxPowerLevel =
                pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.
                RadioIfTxPower.u2TxPowerLevel;

            RadioIfGetDB.RadioIfGetAllDB.i2CurrentTxPower =
                pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.
                RadioIfTxPower.u2TxPowerLevel;
            if (WssIfProcessRadioIfDBMsg (WSS_SET_RADIO_IF_DB,
                                          (&RadioIfGetDB)) != OSIX_SUCCESS)
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                             "RadioIfConfigStatusReq: Set of Radio DB failed \n");
                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                 RadioIfGetAllDB.
                                                 pu1AntennaSelection);
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;

            }
        }

        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                         RadioIfGetAllDB.pu1AntennaSelection);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_SUCCESS;
    }

    if (pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.RadioIfTxPowerLevel.
        isPresent != OSIX_FALSE)
    {
        /* Reset the flag received. This flag will be set by each attibute if
         * there is a conflict between the values stored in DB and input
         * received */
        pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.RadioIfTxPowerLevel.
            isPresent = OSIX_FALSE;

        pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;
        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
            pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.u2WtpInternalId;
        pWssIfCapwapDB->CapwapGetDB.u1RadioId =
            pWssMsgStruct->unRadioIfMsg.
            RadioIfConfigStatusReq.RadioIfTxPowerLevel.u1RadioId;

        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
            != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigStatusReq: Getting Radio index failed\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }

        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
            pWssIfCapwapDB->CapwapGetDB.u4IfIndex;

        RadioIfGetDB.RadioIfIsGetAllDB.bTxPowerLevel = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bNoOfSupportedPowerLevels = OSIX_TRUE;

        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, (&RadioIfGetDB))
            != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigStatusReq: Get of RadioIf DB failed \r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }

        for (u2PowerLevel = 0;
             u2PowerLevel <
             pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.
             RadioIfTxPowerLevel.u1NumOfLevels; u2PowerLevel++)
        {
            RadioIfGetDB.RadioIfGetAllDB.au2TxPowerLevel[u2PowerLevel] =
                pWssMsgStruct->unRadioIfMsg.
                RadioIfConfigStatusReq.RadioIfTxPowerLevel.
                au2PowerLevels[u2PowerLevel];
        }
        RadioIfGetDB.RadioIfGetAllDB.u1NoOfSupportedPowerLevels =
            pWssMsgStruct->unRadioIfMsg.
            RadioIfConfigStatusReq.RadioIfTxPowerLevel.u1NumOfLevels;
        if (WssIfProcessRadioIfDBMsg (WSS_SET_RADIO_IF_DB, (&RadioIfGetDB)) !=
            OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigStatusReq: Set of RadioIf DB failed \r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;

        }
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                         RadioIfGetAllDB.pu1AntennaSelection);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_SUCCESS;
    }

    if (pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.RadioIfConfig.
        isPresent != OSIX_FALSE)
    {
        /* Reset the flag received. This flag will be set by each attibute if
         * there is a conflict between the values stored in DB and input
         * received */
        pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.RadioIfConfig.
            isPresent = OSIX_FALSE;

        pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;
        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
            pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.u2WtpInternalId;
        pWssIfCapwapDB->CapwapGetDB.u1RadioId =
            pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.RadioIfConfig.
            u1RadioId;

        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
            != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigStatusReq: Getting Radio index failed\r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }

        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
            pWssIfCapwapDB->CapwapGetDB.u4IfIndex;

        RadioIfGetDB.RadioIfIsGetAllDB.bShortPreamble = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bDTIMPeriod = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bBeaconPeriod = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bCountryString = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bMacAddr = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bMultiDomainInfo = OSIX_TRUE;

        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, (&RadioIfGetDB))
            != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigStatusReq: Get of RadioIf DB failed \r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }

        if (pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.RadioIfConfig.
            u1ShortPreamble != RadioIfGetDB.RadioIfGetAllDB.u1ShortPreamble)
        {
            pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.RadioIfConfig.
                isPresent |= OSIX_TRUE;
        }

        if (pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.RadioIfConfig.
            u1DtimPeriod != RadioIfGetDB.RadioIfGetAllDB.u1DTIMPeriod)
        {
            pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.
                RadioIfConfig.isPresent |= OSIX_TRUE;
        }

        if (pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.RadioIfConfig.
            u2BeaconPeriod != RadioIfGetDB.RadioIfGetAllDB.u2BeaconPeriod)
        {
            pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.RadioIfConfig.
                isPresent |= OSIX_TRUE;
        }

        if (MEMCMP
            (pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.RadioIfConfig.
             au1CountryString, RadioIfGetDB.RadioIfGetAllDB.au1CountryString,
             STRLEN (pWssMsgStruct->unRadioIfMsg.
                     RadioIfConfigStatusReq.RadioIfConfig.au1CountryString)) !=
            0)
        {
            pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.RadioIfConfig.
                isPresent |= OSIX_TRUE;
            for (u2MultiDomainLoopIndex = 0;
                 u2MultiDomainLoopIndex < MAX_MULTIDOMAIN_INFO_ELEMENT;
                 u2MultiDomainLoopIndex++)
            {
                RadioIfGetDB.RadioIfGetAllDB.
                    MultiDomainInfo[u2MultiDomainLoopIndex].u2FirstChannelNo =
                    0;
                RadioIfGetDB.RadioIfGetAllDB.
                    MultiDomainInfo[u2MultiDomainLoopIndex].u2NoOfChannels = 0;
                RadioIfGetDB.RadioIfGetAllDB.
                    MultiDomainInfo[u2MultiDomainLoopIndex].u2MaxTxPowerLevel =
                    0;
            }
        }

        MEMCPY (RadioIfGetDB.RadioIfGetAllDB.MacAddr,
                pWssMsgStruct->unRadioIfMsg.
                RadioIfConfigStatusReq.RadioIfConfig.MacAddr, MAC_ADDR_LEN);
        RadioIfGetDB.RadioIfIsGetAllDB.bShortPreamble = OSIX_FALSE;
        RadioIfGetDB.RadioIfIsGetAllDB.bDTIMPeriod = OSIX_FALSE;
        RadioIfGetDB.RadioIfIsGetAllDB.bBeaconPeriod = OSIX_FALSE;
        RadioIfGetDB.RadioIfIsGetAllDB.bCountryString = OSIX_FALSE;
        RadioIfGetDB.RadioIfIsGetAllDB.bMacAddr = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bNoOfBssId = OSIX_TRUE;

        RadioIfGetDB.RadioIfGetAllDB.u1NoOfBssId =
            pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.RadioIfConfig.
            u1NoOfBssId;
        if (WssIfProcessRadioIfDBMsg (WSS_SET_RADIO_IF_DB, (&RadioIfGetDB)) !=
            OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigStatusReq:Set of RadioIf DB failed \r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;

        }
#ifdef  RFMGMT_WANTED
        RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtIntfConfigReq.u4RadioIfIndex =
            RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex;

        RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtIntfConfigReq.isPresent = OSIX_TRUE;

        MEMCPY (RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtIntfConfigReq.RadioMacAddr,
                RadioIfGetDB.RadioIfGetAllDB.MacAddr, sizeof (tMacAddr));

        if (WssIfProcessRfMgmtMsg (RFMGMT_UPDATE_RADIO_MAC_ADDR,
                                   &RfMgmtMsgStruct) != RFMGMT_SUCCESS)
        {
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);

            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "Config Status Req : Update RM Mgmt DB Failed\n");
            return OSIX_FAILURE;
        }
#endif
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                         RadioIfGetAllDB.pu1AntennaSelection);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_SUCCESS;
    }

    if (pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.RadioIfInfo.
        isPresent != OSIX_FALSE)
    {
        /* Reset the flag received. This flag will be set by each attibute if
         * there is a conflict between the values stored in DB and input
         * received */
        pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.RadioIfInfo.
            isPresent = OSIX_FALSE;

        pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;
        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
            pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.u2WtpInternalId;
        pWssIfCapwapDB->CapwapGetDB.u1RadioId =
            pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.RadioIfInfo.
            u1RadioId;

        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
            != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigStatusReq : Getting Radio index failed\r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }

        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
            pWssIfCapwapDB->CapwapGetDB.u4IfIndex;

        RadioIfGetDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_TRUE;

        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, (&RadioIfGetDB))
            != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigStatusReq: Get of RadioIf DB failed \r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }

        if (pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.RadioIfInfo.
            u4RadioType != RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType)
        {
            pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.RadioIfInfo.
                isPresent |= OSIX_TRUE;
        }
        else
        {
            RadioIfGetDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_FALSE;
        }

        if (WssIfProcessRadioIfDBMsg (WSS_SET_RADIO_IF_DB, (&RadioIfGetDB))
            != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigStatusReq: Set of Radio DB failed\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;

        }
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                         RadioIfGetAllDB.pu1AntennaSelection);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_SUCCESS;
    }

    if (WssIfProcessRadioIfDBMsg (WSS_SET_RADIO_IF_DB, (&RadioIfGetDB))
        != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfConfigStatusReq: Set of RadioIf DB failed \r\n");
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                         RadioIfGetAllDB.pu1AntennaSelection);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;

    }
    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                     RadioIfGetAllDB.pu1AntennaSelection);
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfChangeStateEventReq                           *
 *                                                                           *
 * Description        : This function constructs the change state event req  *
 *                                                                           *
 * Input(s)           : pWssMsgStruct - Pointer to input strucutre           *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfChangeStateEventReq (tRadioIfMsgStruct * pWssMsgStruct)
{
    tRadioIfGetDB       RadioIfGetDB;
    tCfaMsgStruct       CfaMsgStruct;
    UINT1               u1IdCountIndex = 0;
    UINT1               u1BssIdCount = 0;

    MEMSET (&CfaMsgStruct, 0, sizeof (tCfaMsgStruct));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    if (pWssMsgStruct == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC, "Null input received\r\n");
        return OSIX_FAILURE;
    }

    /*To get IfIndex value from the input structure */
    if (pWssMsgStruct->unRadioIfMsg.RadioIfSetAdminOperStatus.isPresent
        != OSIX_FALSE)
    {
        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
            pWssMsgStruct->unRadioIfMsg.RadioIfSetAdminOperStatus.u4IfIndex;
    }

    RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
        UtlShMemAllocAntennaSelectionBuf ();

    if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC, "\r%% Memory allocation for Antenna"
                     " selection failed.\r\n");
        return OSIX_FAILURE;
    }
    RadioIfGetDB.RadioIfIsGetAllDB.bAdminStatus = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bNoOfBssId = OSIX_TRUE;

    /* To get the RadioIfDB Value (Parameters needed here : BSSID 
     * COUNT and Admin Status )*/
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                  (&RadioIfGetDB)) != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfChangeStateEventReq: Get of RadioIf DB failed \r\n");
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                         RadioIfGetAllDB.pu1AntennaSelection);
        return OSIX_FAILURE;
    }
    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                     RadioIfGetAllDB.pu1AntennaSelection);

    /* If Operational Status is same , No Changes required, Return Success */
    if (RadioIfGetDB.RadioIfGetAllDB.u1OperStatus ==
        pWssMsgStruct->unRadioIfMsg.RadioIfSetAdminOperStatus.u1OperStatus)
    {
        return OSIX_SUCCESS;
    }

    if (pWssMsgStruct->unRadioIfMsg.RadioIfSetAdminOperStatus.u1OperStatus == CFA_IF_DOWN)    /*If Oper status is Down */
    {
        u1BssIdCount = RadioIfGetDB.RadioIfGetAllDB.u1NoOfBssId;    /*BSS Count */

        /*For All the Bss present */
        for (u1IdCountIndex = 1; u1IdCountIndex <= u1BssIdCount;
             u1IdCountIndex++)
        {
            /*To Get BssIfIndex from RadioIf Module , filling IfIndex */

            RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
                pWssMsgStruct->unRadioIfMsg.RadioIfSetAdminOperStatus.u4IfIndex;
            RadioIfGetDB.RadioIfGetAllDB.u1WlanId = u1IdCountIndex;
            RadioIfGetDB.RadioIfIsGetAllDB.bBssIfIndex = OSIX_TRUE;

            if (WssIfProcessRadioIfDBMsg (WSS_GET_WLAN_BSS_IFINDEX_DB,
                                          (&RadioIfGetDB)) != OSIX_SUCCESS)
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                             "RadioIfChangeStateEventReq : Retreival of BSSIF "
                             "Index Failed \r\n");
                return OSIX_FAILURE;
            }
        }
    }

    /* To Change the Operational Status */
    CfaMsgStruct.unCfaMsg.CfaCreateIfIndex.u4IfIndex =
        pWssMsgStruct->unRadioIfMsg.RadioIfSetAdminOperStatus.u4IfIndex;
    CfaMsgStruct.unCfaMsg.CfaCreateIfIndex.u1OperStatus =
        pWssMsgStruct->unRadioIfMsg.RadioIfSetAdminOperStatus.u1OperStatus;

    if (WssIfProcessCfaMsg (CFA_WSS_OPER_STATUS_CHG_MSG, &CfaMsgStruct)
        != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfConfigUpdateReq : Oper status change failed (CFA)\n");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfPmWtpEventReq                                 *
 *                                                                           *
 * Description        : This function sets the multi doamin info from Wtp    *
 *                      event req                                            *
 *                                                                           *
 * Input(s)           : pWssMsgStruct - Pointer to input strucutre           *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfPmWtpEventReq (tRadioIfMsgStruct * pWssMsgStruct)
{
    tRadioIfGetDB       RadioIfGetDB;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT1               au1AllowedChannelList[RADIOIF_MAX_CHANNEL_A];
    UINT1               u1MultiDomainIndex = 0;
    UINT1               u1Count = 0;
    UINT2               u2MsgLength = 0;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    MEMSET (&au1AllowedChannelList, 0, RADIOIF_MAX_CHANNEL_A);
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    if (pWssMsgStruct->unRadioIfMsg.
        RadioIfPmWtpEventReq.VendorMultiDomainCap.isPresent != OSIX_FALSE)
    {
        /* Reset the flag received. This flag will be set by each attibute if
         * there is a conflict between the values stored in DB and input
         * received */
        pWssMsgStruct->unRadioIfMsg.
            RadioIfPmWtpEventReq.VendorMultiDomainCap.isPresent = OSIX_FALSE;

        pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;
        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
            pWssMsgStruct->unRadioIfMsg.RadioIfPmWtpEventReq.u2WtpInternalId;
        pWssIfCapwapDB->CapwapGetDB.u1RadioId =
            pWssMsgStruct->unRadioIfMsg.
            RadioIfPmWtpEventReq.VendorMultiDomainCap.u1RadioId;

        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
            != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigStatusReq : Getting Radio index failed\r\n");
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }

        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
            pWssIfCapwapDB->CapwapGetDB.u4IfIndex;

        RadioIfGetDB.RadioIfIsGetAllDB.bMultiDomainInfo = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bCurrentChannel = OSIX_TRUE;

        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, (&RadioIfGetDB))
            != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigStatusReq: Get of Radio DB failed \r\n");
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }

        RadioIfGetDB.RadioIfIsGetAllDB.bCurrentChannel = OSIX_FALSE;
        u1MultiDomainIndex = pWssMsgStruct->unRadioIfMsg.RadioIfPmWtpEventReq.
            VendorMultiDomainCap.u1MultiDomainIndex;
        u2MsgLength =
            pWssMsgStruct->unRadioIfMsg.RadioIfPmWtpEventReq.
            VendorMultiDomainCap.u2MsgLength;

        RadioIfGetDB.RadioIfGetAllDB.
            MultiDomainInfo[u1MultiDomainIndex - 1].u2FirstChannelNo =
            pWssMsgStruct->unRadioIfMsg.RadioIfPmWtpEventReq.
            VendorMultiDomainCap.u2FirstChannel;

        RadioIfGetDB.RadioIfGetAllDB.
            MultiDomainInfo[u1MultiDomainIndex - 1].u2NoOfChannels =
            pWssMsgStruct->unRadioIfMsg.RadioIfPmWtpEventReq.
            VendorMultiDomainCap.u2NumOfChannels;

        RadioIfGetDB.RadioIfGetAllDB.
            MultiDomainInfo[u1MultiDomainIndex - 1].u2MaxTxPowerLevel =
            pWssMsgStruct->unRadioIfMsg.RadioIfPmWtpEventReq.
            VendorMultiDomainCap.u2MaxTxPowerLevel;

        RadioIfGetDB.RadioIfIsGetAllDB.bMultiDomainInfo = OSIX_TRUE;
        if (WssIfProcessRadioIfDBMsg (WSS_SET_RADIO_IF_DB,
                                      (&RadioIfGetDB)) != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigStatusReq: Set of Radio DB failed \n");
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;

        }

        u1Count = (UINT1) (u2MsgLength / RADIOIF_MULTI_DOMAIN_BASE_INDEX);
        if (u1Count == u1MultiDomainIndex)
        {
#if 0
            if (RadioIfValidateAllowedChannels
                (RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex,
                 RadioIfGetDB.RadioIfGetAllDB.u1CurrentChannel,
                 au1AllowedChannelList) == OSIX_FAILURE)
#endif
            {
                RadioIfSetAllowedChannel (RadioIfGetDB.RadioIfGetAllDB.
                                          u4RadioIfIndex,
                                          au1AllowedChannelList);
            }
            if (RadioIfDeriveTxPower
                (RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex) != OSIX_SUCCESS)
            {
            }
        }
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_SUCCESS;
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfRadioDbReset                                  *
 *                                                                           *
 * Description        : This function resets the multi doamin info in radio  *
 *                      DB                                                   *
 *                                                                           *
 * Input(s)           : pWssMsgStruct - Pointer to input strucutre           *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfRadioDbReset (tRadioIfMsgStruct * pWssMsgStruct)
{
    tRadioIfGetDB       RadioIfGetDB;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT1               u1Radioid = 0;
    UINT2               u2NumRadioElems = 0;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
        pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.u2WtpInternalId;

    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bNoOfRadio = OSIX_TRUE;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
        != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfRadioDbReset: Getting No of Radio failed\r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    u2NumRadioElems = pWssIfCapwapDB->CapwapGetDB.u1WtpNoofRadio;
    for (u1Radioid = 1; u1Radioid <= u2NumRadioElems; u1Radioid++)
    {
        pWssIfCapwapDB->CapwapGetDB.u1RadioId = u1Radioid;
        pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;

        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
            != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfRadioDbReset : Getting Radio index failed\r\n");
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }

        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
            pWssIfCapwapDB->CapwapGetDB.u4IfIndex;

        RadioIfGetDB.RadioIfIsGetAllDB.bMultiDomainInfo = OSIX_TRUE;
        if (WssIfProcessRadioIfDBMsg (WSS_SET_RADIO_IF_DB,
                                      (&RadioIfGetDB)) != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfRadioDbReset: Set of Radio DB failed \n");
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;

        }
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;

}

/*****************************************************************************
 * Function Name      : RadioIfSetStationConfig                              *
 *                                                                           *
 * Description        : This function constructs WTP Radio Config structure  *
 *                      and send it to WLC Handler and update the WSSIF DB   *
 *                      in case the configuration is succesfully applied in  *
 *                      WTP                                                  *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to Radio IF DB with ifIndex  *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfSetStationConfig (tRadioIfGetDB * pRadioIfGetDB)
{
    tRadioIfDB          RadioIfDB;
    tRadioIfGetDB       RadioIfGetDB;
    tRadioIfMsgStruct   WssMsgStruct;
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tRemoteSessionManager *pSessEntry = NULL;
    INT4                i4RetVal = 0;
    UINT1               u1IsValChanged = 0;
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        pWlcHdlrMsgStruct =
        (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pWlcHdlrMsgStruct == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetStationConfig:- "
                     "UtlShMemAllocWlcBuf returned failure\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));
    MEMSET (&WssMsgStruct, 0, sizeof (tRadioIfMsgStruct));
    MEMSET (&RadioIfDB, 0, sizeof (tRadioIfDB));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    /* reset the global variable so that it is not affected by earlier cases */
    gu1IsConfigResponseReceived = RADIOIF_GLOBAL_INVALID_VALUE;

    if (pRadioIfGetDB == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetStationConfig : Null Input received \r\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
        UtlShMemAllocAntennaSelectionBuf ();

    if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC, "\r%% Memory allocation for Antenna"
                     " selection failed.\r\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    if (pRadioIfGetDB->RadioIfIsGetAllDB.bOperationalRate != OSIX_FALSE)
    {
        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex
            = pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
        RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bRadioIfIndex = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bOperationalRate = OSIX_TRUE;

        /* Fill boolean Radio Id     */
        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                      &RadioIfGetDB) != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfSetStationConfig: RadioIf DB access failed. \n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
        /* nmhRoutines already validated whether the input received and old
         * values are different. Hence fill config update request structure */

        MEMCPY (pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
                RadioIfRateSet.au1OperationalRate,
                pRadioIfGetDB->RadioIfGetAllDB.au1OperationalRate,
                sizeof (pRadioIfGetDB->RadioIfGetAllDB.au1OperationalRate));

        if (MEMCMP (pRadioIfGetDB->RadioIfGetAllDB.au1OperationalRate,
                    RadioIfGetDB.RadioIfGetAllDB.au1OperationalRate,
                    RADIOIF_OPER_RATE) != 0)
        {
            u1IsValChanged = OSIX_TRUE;
        }

        if (u1IsValChanged == OSIX_FALSE)
        {
            RADIOIF_TRC (RADIOIF_INFO_TRC,
                         "RadioIfSetStationConfig: No Value Change, "
                         "Not triggering Config Update \r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_SUCCESS;
        }

        pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfRateSet.u2MessageType
            = RADIO_CONF_UPDATE_RATE_SET_MSG_TYPE;

        pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfRateSet.u2MessageLength
            = sizeof (pRadioIfGetDB->RadioIfGetAllDB.au1OperationalRate) + 1;

        pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfRateSet.u1RadioId
            = RadioIfGetDB.RadioIfGetAllDB.u1RadioId;

        pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.u2WtpInternalId
            = RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;

        pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfRateSet.isPresent
            = OSIX_TRUE;

        i4RetVal = WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_RADIO_CONF_UPDATE_REQ,
                                           pWlcHdlrMsgStruct);

        /* Invoke WLC Handler to send the config update request */
        if (i4RetVal != OSIX_SUCCESS)
        {
            if (i4RetVal == RADIOIF_NO_AP_PRESENT)
            {
                gu1IsConfigResponseReceived = RADIOIF_NO_AP_PRESENT;
            }
            else
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                             "RadioIfSetStationConfig: To change the operation "
                             "rate of radio failed \r\n");
                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                 RadioIfGetAllDB.
                                                 pu1AntennaSelection);
                UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
        }

        /* If the configuration is success then update the DB */

        pRadioIfGetDB->RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
        pRadioIfGetDB->RadioIfGetAllDB.u1RadioId =
            RadioIfGetDB.RadioIfGetAllDB.u1RadioId;
        pRadioIfGetDB->RadioIfIsGetAllDB.bOperationalRate = OSIX_TRUE;

        /* Update DB */
        if (WssIfProcessRadioIfDBMsg (WSS_SET_RADIO_IF_DB,
                                      pRadioIfGetDB) != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfSetStationConfig:Radio DB Update failed\n");
            UtlShMemFreeAntennaSelectionBuf
                (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
    }

    u1IsValChanged = OSIX_FALSE;

    if ((pRadioIfGetDB->RadioIfIsGetAllDB.bBeaconPeriod != OSIX_FALSE) ||
        (pRadioIfGetDB->RadioIfIsGetAllDB.bCountryString != OSIX_FALSE))
    {

        /*WlcHdlrMsgStruct.RadioIfConfigUpdateReq.RadioIfConfig.u2BeaconPeriod
           = pRadioIfGetDB->RadioIfGetAllDB.u2BeaconPeriod;

           RadioIfGetDB.RadioIfGetAllDB.u2BeaconPeriod 
           = pRadioIfGetDB->RadioIfGetAllDB.u2BeaconPeriod; */
        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
            pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
        RadioIfGetDB.RadioIfIsGetAllDB.bRadioIfIndex = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bShortPreamble = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bNoOfBssId = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bMacAddr = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bCountryString = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bBeaconPeriod = OSIX_TRUE;

        /* Get the Radio IF DB info to fill the structure  */
        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, &RadioIfGetDB)
            != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfSetStationConfig: RadioIf DB access failed. \n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }

        if (RadioIfGetDB.RadioIfGetAllDB.u2BeaconPeriod !=
            pRadioIfGetDB->RadioIfGetAllDB.u2BeaconPeriod)
        {
            u1IsValChanged = OSIX_TRUE;
        }

        /* nmhRoutines already validated whether the input received and old
         * values are different. Hence fill config update request structure */
        pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfConfig.isPresent
            = OSIX_TRUE;
        if (pRadioIfGetDB->RadioIfIsGetAllDB.bMessageType != OSIX_TRUE)
        {
            pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
                RadioIfConfig.u2MessageType = RADIO_CONF_UPDATE_STA_MSG_TYPE;
        }
        else
        {
            pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
                RadioIfConfig.u2MessageType =
                pRadioIfGetDB->RadioIfGetAllDB.u2MessageType;
        }
        if (pRadioIfGetDB->RadioIfIsGetAllDB.bMessageLength != OSIX_TRUE)
        {
            pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
                RadioIfConfig.u2MessageLength =
                RADIO_CONF_UPDATE_STA_MSG_LENGTH;
        }
        else
        {
            pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
                RadioIfConfig.u2MessageLength =
                pRadioIfGetDB->RadioIfGetAllDB.u2MessageLength;
        }

        if (pRadioIfGetDB->RadioIfIsGetAllDB.bRadioId != OSIX_TRUE)
        {
            pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfConfig.u1RadioId
                = RadioIfGetDB.RadioIfGetAllDB.u1RadioId;
        }
        else
        {
            pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfConfig.u1RadioId =
                pRadioIfGetDB->RadioIfGetAllDB.u1RadioId;
        }

        if (pRadioIfGetDB->RadioIfIsGetAllDB.bWtpInternalId != OSIX_TRUE)
        {
            pRadioIfGetDB->RadioIfGetAllDB.u2WtpInternalId =
                RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;
            pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.u2WtpInternalId
                = RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;
        }
        else
        {
            pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
                RadioIfConfig.u2WtpInternalId =
                pRadioIfGetDB->RadioIfGetAllDB.u2WtpInternalId;
        }

        if (pRadioIfGetDB->RadioIfIsGetAllDB.bBeaconPeriod != OSIX_TRUE)
        {
            pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
                RadioIfConfig.u2BeaconPeriod =
                RadioIfGetDB.RadioIfGetAllDB.u2BeaconPeriod;
        }
        else
        {
            pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
                RadioIfConfig.u2BeaconPeriod =
                pRadioIfGetDB->RadioIfGetAllDB.u2BeaconPeriod;
        }

        if (pRadioIfGetDB->RadioIfIsGetAllDB.bShortPreamble != OSIX_TRUE)
        {
            pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
                RadioIfConfig.u1ShortPreamble =
                RadioIfGetDB.RadioIfGetAllDB.u1ShortPreamble;
        }
        else
        {
            pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
                RadioIfConfig.u1ShortPreamble =
                pRadioIfGetDB->RadioIfGetAllDB.u1ShortPreamble;

            if ((pRadioIfGetDB->RadioIfGetAllDB.u1ShortPreamble) !=
                (RadioIfGetDB.RadioIfGetAllDB.u1ShortPreamble))
            {
                u1IsValChanged = OSIX_TRUE;
            }

        }

        if (pRadioIfGetDB->RadioIfIsGetAllDB.bNoOfBssId != OSIX_TRUE)
        {
            pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfConfig.u1NoOfBssId
                = RadioIfGetDB.RadioIfGetAllDB.u1NoOfBssId;
        }
        else
        {
            pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
                RadioIfConfig.u1NoOfBssId =
                pRadioIfGetDB->RadioIfGetAllDB.u1NoOfBssId;
        }

        if (pRadioIfGetDB->RadioIfIsGetAllDB.bDTIMPeriod != OSIX_TRUE)
        {
            pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfConfig.u1DtimPeriod
                = RADIO_DEF_DTIM_PERIOD;
        }
        else
        {
            pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
                RadioIfConfig.u1DtimPeriod =
                pRadioIfGetDB->RadioIfGetAllDB.u1DTIMPeriod;

            if ((pRadioIfGetDB->RadioIfGetAllDB.u1DTIMPeriod) !=
                (RadioIfGetDB.RadioIfGetAllDB.u1DTIMPeriod))
            {
                u1IsValChanged = OSIX_TRUE;
            }
        }

        if (pRadioIfGetDB->RadioIfIsGetAllDB.bCountryString != OSIX_FALSE)
        {
            pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
                RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_RSM_FROM_INDEX,
                                         pWssIfCapwapDB) == OSIX_SUCCESS)
            {
                pSessEntry = pWssIfCapwapDB->pSessEntry;
                if ((pSessEntry != NULL)
                    && (pSessEntry->eCurrentState == CAPWAP_RUN))
                {
                    RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                                 "RadioIfSetStationConfig: AP in run state."
                                 "Cannot change country.\r\n");
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    return OSIX_FAILURE;
                }
            }
            if (MEMCMP (pRadioIfGetDB->RadioIfGetAllDB.au1CountryString,
                        RadioIfGetDB.RadioIfGetAllDB.au1CountryString,
                        STRLEN (pRadioIfGetDB->
                                RadioIfGetAllDB.au1CountryString)) != 0)
            {
                u1IsValChanged = OSIX_TRUE;
            }
            MEMCPY (pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
                    RadioIfConfig.au1CountryString,
                    pRadioIfGetDB->RadioIfGetAllDB.au1CountryString,
                    sizeof (pRadioIfGetDB->RadioIfGetAllDB.au1CountryString));
        }
        else
        {
            MEMCPY (pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
                    RadioIfConfig.au1CountryString,
                    RadioIfGetDB.RadioIfGetAllDB.au1CountryString,
                    sizeof (RadioIfGetDB.RadioIfGetAllDB.au1CountryString));
        }

        if (u1IsValChanged == OSIX_FALSE)
        {
            RADIOIF_TRC (RADIOIF_INFO_TRC,
                         "RadioIfSetStationConfig(2) : No Value Change, "
                         "Not triggering Config Update \r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_SUCCESS;
        }

        MEMCPY (pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfConfig.MacAddr,
                RadioIfGetDB.RadioIfGetAllDB.MacAddr, MAC_ADDR_LEN);

        i4RetVal = WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_RADIO_CONF_UPDATE_REQ,
                                           pWlcHdlrMsgStruct);

        /* Invoke WLC Handler to send the config update request */
        if (i4RetVal != OSIX_SUCCESS)
        {
            if (i4RetVal == RADIOIF_NO_AP_PRESENT)
            {
                gu1IsConfigResponseReceived = RADIOIF_NO_AP_PRESENT;
            }
            else
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                             "RadioIfSetStationConfig : To change the beacon "
                             "period of radio failed \r\n");
                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                 RadioIfGetAllDB.
                                                 pu1AntennaSelection);
                UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
        }
        if (WssIfProcessRadioIfDBMsg (WSS_SET_RADIO_IF_DB,
                                      pRadioIfGetDB) != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfSetStationConfig:Radio DB Update failed\n");
            UtlShMemFreeAntennaSelectionBuf
                (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }

    }
    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                     RadioIfGetAllDB.pu1AntennaSelection);
    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfSetOperationTable                             *
 *                                                                           *
 * Description        : This function constructsMAC Parameters and send it to*
 *                      WLC Handler and update the WSSIF DB in case the      *
 *                      configuration is succesfully applied in WTP          *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to Radio IF DB with ifIndex  *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfSetOperationTable (tRadioIfGetDB * pRadioIfGetDB)
{
    tRadioIfGetDB       RadioIfGetDB;
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;
    INT4                i4RetVal = 0;
    UINT1               u1IsValChanged = 0;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    if (pRadioIfGetDB == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetOperationTable : Null Input received \r\n");
        return OSIX_FAILURE;
    }
    RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
        UtlShMemAllocAntennaSelectionBuf ();
    if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC, "\r%% Memory allocation for Antenna"
                     " selection failed.\r\n");
        return OSIX_FAILURE;
    }
    pWlcHdlrMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pWlcHdlrMsgStruct == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetOperationTable:- "
                     "UtlShMemAllocWlcBuf returned failure\n");
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                         RadioIfGetAllDB.pu1AntennaSelection);
        return OSIX_FAILURE;
    }

    MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));
    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
        pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioIfIndex = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bRTSThreshold = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bShortRetry = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bLongRetry = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bFragmentationThreshold = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bTxMsduLifetime = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bRxMsduLifetime = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;

    /* Get the Radio IF DB info to fill the structure  */
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                  &RadioIfGetDB) != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetOperationTable : RadioIf DB access failed. \r\n");
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                         RadioIfGetAllDB.pu1AntennaSelection);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;

    }
    /* nmhRoutines already validated whether the input received and old
     * values are different. Hence fill config update request structure */
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfMacOperation.u2MessageType
        = RADIO_MAC_OPER_MSG_TYPE;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
        RadioIfMacOperation.u2MessageLength = RADIO_MAC_OPER_MSG_LEN;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfMacOperation.u1RadioId =
        RadioIfGetDB.RadioIfGetAllDB.u1RadioId;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.u2WtpInternalId =
        RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfMacOperation.isPresent =
        OSIX_TRUE;

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bRTSThreshold != OSIX_TRUE)
    {
        pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
            RadioIfMacOperation.u2RTSThreshold =
            RadioIfGetDB.RadioIfGetAllDB.u2RTSThreshold;
    }
    else
    {
        pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
            RadioIfMacOperation.u2RTSThreshold =
            pRadioIfGetDB->RadioIfGetAllDB.u2RTSThreshold;

        if ((pRadioIfGetDB->RadioIfGetAllDB.u2RTSThreshold) !=
            (RadioIfGetDB.RadioIfGetAllDB.u2RTSThreshold))
        {
            u1IsValChanged = OSIX_TRUE;
        }
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bShortRetry != OSIX_TRUE)
    {
        pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
            RadioIfMacOperation.u1ShortRetry =
            RadioIfGetDB.RadioIfGetAllDB.u1ShortRetry;
    }
    else
    {
        pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
            RadioIfMacOperation.u1ShortRetry =
            pRadioIfGetDB->RadioIfGetAllDB.u1ShortRetry;

        if ((pRadioIfGetDB->RadioIfGetAllDB.u1ShortRetry) !=
            (RadioIfGetDB.RadioIfGetAllDB.u1ShortRetry))
        {
            u1IsValChanged = OSIX_TRUE;
        }
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bLongRetry != OSIX_TRUE)
    {
        pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
            RadioIfMacOperation.u1LongRetry =
            RadioIfGetDB.RadioIfGetAllDB.u1LongRetry;
    }
    else
    {
        pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
            RadioIfMacOperation.u1LongRetry =
            pRadioIfGetDB->RadioIfGetAllDB.u1LongRetry;

        if ((pRadioIfGetDB->RadioIfGetAllDB.u1LongRetry) !=
            (RadioIfGetDB.RadioIfGetAllDB.u1LongRetry))
        {
            u1IsValChanged = OSIX_TRUE;
        }
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bFragmentationThreshold != OSIX_TRUE)
    {
        pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
            RadioIfMacOperation.u2FragmentThreshold =
            RadioIfGetDB.RadioIfGetAllDB.u2FragmentationThreshold;
    }
    else
    {
        pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
            RadioIfMacOperation.u2FragmentThreshold =
            pRadioIfGetDB->RadioIfGetAllDB.u2FragmentationThreshold;

        if ((pRadioIfGetDB->RadioIfGetAllDB.u2FragmentationThreshold) !=
            (RadioIfGetDB.RadioIfGetAllDB.u2FragmentationThreshold))
        {
            u1IsValChanged = OSIX_TRUE;
        }
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bTxMsduLifetime != OSIX_TRUE)
    {
        pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
            RadioIfMacOperation.u4TxMsduLifetime =
            RadioIfGetDB.RadioIfGetAllDB.u4TxMsduLifetime;
    }
    else
    {
        pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
            RadioIfMacOperation.u4TxMsduLifetime =
            pRadioIfGetDB->RadioIfGetAllDB.u4TxMsduLifetime;

        if ((pRadioIfGetDB->RadioIfGetAllDB.u4TxMsduLifetime) !=
            (RadioIfGetDB.RadioIfGetAllDB.u4TxMsduLifetime))
        {
            u1IsValChanged = OSIX_TRUE;
        }
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bRxMsduLifetime != OSIX_TRUE)
    {
        pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
            RadioIfMacOperation.u4RxMsduLifetime =
            RadioIfGetDB.RadioIfGetAllDB.u4RxMsduLifetime;
    }
    else
    {
        pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
            RadioIfMacOperation.u4RxMsduLifetime =
            pRadioIfGetDB->RadioIfGetAllDB.u4RxMsduLifetime;

        if ((pRadioIfGetDB->RadioIfGetAllDB.u4RxMsduLifetime) !=
            (RadioIfGetDB.RadioIfGetAllDB.u4RxMsduLifetime))
        {
            u1IsValChanged = OSIX_TRUE;
        }
    }

    if (u1IsValChanged == OSIX_FALSE)
    {
        RADIOIF_TRC (RADIOIF_INFO_TRC,
                     "RadioIfSetOperationTable : No Value Change, Not "
                     "triggering Config Update \r\n");
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                         RadioIfGetAllDB.pu1AntennaSelection);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_SUCCESS;
    }

    /* Invoke WLC Handler to send the config update request */
    i4RetVal = WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_RADIO_CONF_UPDATE_REQ,
                                       pWlcHdlrMsgStruct);

    /* Invoke WLC Handler to send the config update request */
    if (i4RetVal != OSIX_SUCCESS)
    {
        if (i4RetVal == RADIOIF_NO_AP_PRESENT)
        {
            gu1IsConfigResponseReceived = RADIOIF_NO_AP_PRESENT;
        }
        else
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfSetOperationTable : Mac Operation failed \r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            return OSIX_FAILURE;
        }
    }
    if (WssIfProcessRadioIfDBMsg (WSS_SET_RADIO_IF_DB,
                                  pRadioIfGetDB) != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetOperationTable : Radio DB Update failure\n");
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                         RadioIfGetAllDB.pu1AntennaSelection);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;
    }

    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                     RadioIfGetAllDB.pu1AntennaSelection);
    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfSetFsDot11nBeamForming                        *
 *                                                                           *
 * Description        : This function constructs the 802.11n Parameters and  *
 *                      sends it to WLC Handler and update the WSSIF DB in   *
 *                      case the configuration is succesfully applied in WTP *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to Radio IF DB with ifIndex  *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
VOID
RadioIfSetFsDot11nBeamForming (tRadioIfGetDB * pRadioIfGetDB,
                               tRadioIfGetDB * pFromDbRadioIfGetDB,
                               UINT1 *u1IsValChanged)
{
    if (pRadioIfGetDB->RadioIfIsGetAllDB.bImpTranBeamRecCapable != OSIX_FALSE)
    {
        if ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
             u1ImpTranBeamRecCapable) !=
            (pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
             u1ImpTranBeamRecCapable))
        {
            *u1IsValChanged = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bImpTranBeamRecCapable = OSIX_TRUE;
            pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1SuppImpTranBeamRecCapable =
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1ImpTranBeamRecCapable;
        }
    }
    else
    {
        pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
            u1SuppImpTranBeamRecCapable =
            pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
            u1ImpTranBeamRecCapable;
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bRecStagSoundCapable != OSIX_FALSE)
    {
        if ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
             u1RecStagSoundCapable) !=
            (pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
             u1RecStagSoundCapable))
        {
            *u1IsValChanged = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bRecStagSoundCapable = OSIX_TRUE;
            pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1SuppRecStagSoundCapable =
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1RecStagSoundCapable;
        }
    }
    else
    {
        pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
            u1SuppRecStagSoundCapable =
            pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
            u1RecStagSoundCapable;
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bTransStagSoundCapable != OSIX_FALSE)
    {
        if ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
             u1TransStagSoundCapable) !=
            (pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
             u1TransStagSoundCapable))
        {
            *u1IsValChanged = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bTransStagSoundCapable = OSIX_TRUE;
            pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1SuppTransStagSoundCapable =
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1TransStagSoundCapable;
        }
    }
    else
    {
        pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
            u1SuppTransStagSoundCapable =
            pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
            u1TransStagSoundCapable;
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bRecNdpCapable != OSIX_FALSE)
    {
        if ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
             u1RecNdpCapable) !=
            (pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
             u1RecNdpCapable))
        {
            *u1IsValChanged = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bRecNdpCapable = OSIX_TRUE;
            pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1SuppRecNdpCapable =
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1RecNdpCapable;
        }
    }
    else
    {
        pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
            u1SuppRecNdpCapable =
            pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
            u1RecNdpCapable;
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bTransNdpCapable != OSIX_FALSE)
    {
        if ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
             u1TransNdpCapable) !=
            (pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
             u1TransNdpCapable))
        {
            *u1IsValChanged = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bTransNdpCapable = OSIX_TRUE;
            pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1SuppTransNdpCapable =
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1TransNdpCapable;
        }
    }
    else
    {
        pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
            u1SuppTransNdpCapable =
            pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
            u1TransNdpCapable;
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bImpTranBeamCapable != OSIX_FALSE)
    {
        if ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
             u1ImpTranBeamCapable) !=
            (pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
             u1ImpTranBeamCapable))
        {
            *u1IsValChanged = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bImpTranBeamCapable = OSIX_TRUE;
            pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1SuppImpTranBeamCapable =
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1ImpTranBeamCapable;
        }
    }
    else
    {
        pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
            u1SuppImpTranBeamCapable =
            pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
            u1ImpTranBeamCapable;
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bCalibration != OSIX_FALSE)
    {
        if ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
             u1Calibration) !=
            (pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
             u1Calibration))
        {
            *u1IsValChanged = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bCalibration = OSIX_TRUE;
            pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1SuppCalibration =
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1Calibration;
        }
    }
    else
    {
        pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
            u1SuppCalibration =
            pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
            u1Calibration;
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bExplCsiTranBeamCapable != OSIX_FALSE)
    {
        if ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
             u1ExplCsiTranBeamCapable) !=
            (pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
             u1ExplCsiTranBeamCapable))
        {
            *u1IsValChanged = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bExplCsiTranBeamCapable =
                OSIX_TRUE;
            pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1SuppExplCsiTranBeamCapable =
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1ExplCsiTranBeamCapable;
        }
    }
    else
    {
        pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
            u1SuppExplCsiTranBeamCapable =
            pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
            u1ExplCsiTranBeamCapable;
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bExplNoncompSteCapable != OSIX_FALSE)
    {
        if ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
             u1ExplNoncompSteCapable) !=
            (pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
             u1ExplNoncompSteCapable))
        {
            *u1IsValChanged = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bExplNoncompSteCapable = OSIX_TRUE;
            pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1SuppExplNoncompSteCapable =
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1ExplNoncompSteCapable;
        }
    }
    else
    {
        pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
            u1SuppExplNoncompSteCapable =
            pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
            u1ExplNoncompSteCapable;
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bExplCompSteCapable != OSIX_FALSE)
    {
        if ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
             u1ExplCompSteCapable) !=
            (pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
             u1ExplCompSteCapable))
        {
            *u1IsValChanged = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bExplCompSteCapable = OSIX_TRUE;
            pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1SuppExplCompSteCapable =
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1ExplCompSteCapable;
        }
    }
    else
    {
        pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
            u1SuppExplCompSteCapable =
            pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
            u1ExplCompSteCapable;
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bExplTranBeamCsiFeedback != OSIX_FALSE)
    {
        if ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
             u1ExplTranBeamCsiFeedback) !=
            (pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
             u1ExplTranBeamCsiFeedback))
        {
            *u1IsValChanged = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bExplTranBeamCsiFeedback =
                OSIX_TRUE;
            pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1SuppExplTranBeamCsiFeedback =
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1ExplTranBeamCsiFeedback;
        }
    }
    else
    {
        pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
            u1SuppExplTranBeamCsiFeedback =
            pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
            u1ExplTranBeamCsiFeedback;
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bExplNoncompBeamFbCapable !=
        OSIX_FALSE)
    {
        if ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
             u1ExplNoncompBeamFbCapable) !=
            (pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
             u1ExplNoncompBeamFbCapable))
        {
            *u1IsValChanged = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bExplNoncompBeamFbCapable =
                OSIX_TRUE;
            pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1SuppExplNoncompBeamFbCapable =
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1ExplNoncompBeamFbCapable;
        }
    }
    else
    {
        pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
            u1SuppExplNoncompBeamFbCapable =
            pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
            u1ExplNoncompBeamFbCapable;
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bExplCompBeamFbCapable != OSIX_FALSE)
    {
        if ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
             u1ExplCompBeamFbCapable) !=
            (pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
             u1ExplCompBeamFbCapable))
        {
            *u1IsValChanged = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bExplCompBeamFbCapable = OSIX_TRUE;
            pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1SuppExplCompBeamFbCapable =
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1ExplCompBeamFbCapable;
        }
    }
    else
    {
        pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
            u1SuppExplCompBeamFbCapable =
            pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
            u1ExplCompBeamFbCapable;
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bMinGrouping != OSIX_FALSE)
    {
        if ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
             u1MinGrouping) !=
            (pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
             u1MinGrouping))
        {
            *u1IsValChanged = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bMinGrouping = OSIX_TRUE;
            pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1SuppMinGrouping =
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1MinGrouping;
        }
    }
    else
    {
        pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
            u1SuppMinGrouping =
            pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
            u1MinGrouping;
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bCsiNoofBeamAntSupported != OSIX_FALSE)
    {
        if ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
             u1CsiNoofBeamAntSupported) !=
            (pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
             u1CsiNoofBeamAntSupported))
        {
            *u1IsValChanged = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bCsiNoofBeamAntSupported =
                OSIX_TRUE;
            pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1CsiNoofBeamAntSupported =
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1CsiNoofBeamAntSupported;
        }
    }
    else
    {
        pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
            u1SuppCsiNoofBeamAntSupported =
            pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
            u1CsiNoofBeamAntSupported;
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bNoncompSteNoBeamformer != OSIX_FALSE)
    {
        if ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
             u1NoncompSteNoBeamformerAntSupp) !=
            (pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
             u1NoncompSteNoBeamformerAntSupp))
        {
            *u1IsValChanged = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bNoncompSteNoBeamformer =
                OSIX_TRUE;
            pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1SuppNoncompSteNoBeamformer =
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1NoncompSteNoBeamformerAntSupp;
        }
    }
    else
    {
        pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
            u1SuppNoncompSteNoBeamformer =
            pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
            u1NoncompSteNoBeamformerAntSupp;
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bCompSteNoBeamformer != OSIX_FALSE)
    {
        if ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
             u1CompSteNoBeamformerAntSupp) !=
            (pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
             u1CompSteNoBeamformerAntSupp))
        {
            *u1IsValChanged = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bCompSteNoBeamformer = OSIX_TRUE;
            pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1SuppCompSteNoBeamformerAntSupp =
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1CompSteNoBeamformerAntSupp;
        }
    }
    else
    {
        pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
            u1SuppCompSteNoBeamformerAntSupp =
            pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
            u1CompSteNoBeamformerAntSupp;
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bCSIMaxNoRowsBeamformer != OSIX_FALSE)
    {
        if ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
             u1CSIMaxNoRowsBeamformerSupp) !=
            (pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
             u1CSIMaxNoRowsBeamformerSupp))
        {
            *u1IsValChanged = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bCSIMaxNoRowsBeamformer =
                OSIX_TRUE;
            pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1SuppCSIMaxNoRowsBeamformerSupp =
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1CSIMaxNoRowsBeamformerSupp;
        }
    }
    else
    {
        pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
            u1SuppCSIMaxNoRowsBeamformerSupp =
            pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
            u1CSIMaxNoRowsBeamformerSupp;
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bChannelEstCapability != OSIX_FALSE)
    {
        if ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
             u1ChannelEstCapability) !=
            (pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
             u1ChannelEstCapability))
        {
            *u1IsValChanged = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bChannelEstCapability = OSIX_TRUE;
            pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1SuppChannelEstCapability =
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1ChannelEstCapability;
        }
    }
    else
    {
        pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
            u1SuppChannelEstCapability =
            pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
            u1ChannelEstCapability;
    }

}

/*****************************************************************************
 * Function Name      : RadioIfSetFsDot11nHTExtCapability                    *
 *                                                                           *
 * Description        : This function constructs the 802.11n Parameters and  *
 *                      sends it to WLC Handler and update the WSSIF DB in   *
 *                      case the configuration is succesfully applied in WTP *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to Radio IF DB with ifIndex  *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
VOID
RadioIfSetFsDot11nHTExtCapability (tRadioIfGetDB * pRadioIfGetDB,
                                   tRadioIfGetDB * pFromDbRadioIfGetDB,
                                   UINT1 *u1IsValChanged)
{
    if (pRadioIfGetDB->RadioIfIsGetAllDB.bPco != OSIX_FALSE)
    {
        if ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.u1Pco) !=
            (pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.u1Pco))
        {
            *u1IsValChanged = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bPco = OSIX_TRUE;
            /*Using Supp variables only HT Ext Cap will be assembled */
            pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.u1SuppPco
                = pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.u1Pco;
        }
    }
    else
    {
        /*Using Supp variables only HT Ext Cap will be assembled */
        pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.u1SuppPco =
            pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.u1Pco;
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bPcoTransTime != OSIX_FALSE)
    {
        if ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.
             u1PcoTransTime) !=
            (pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.
             u1PcoTransTime))
        {
            *u1IsValChanged = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bPcoTransTime = OSIX_TRUE;
            /*Using Supp variables only HT Ext Cap will be assembled */
            pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.
                u1SuppPcoTransTime =
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.
                u1PcoTransTime;
        }
    }
    else
    {
        /*Using Supp variables only HT Ext Cap will be assembled */
        pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.
            u1SuppPcoTransTime =
            pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.
            u1PcoTransTime;
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bMcsFeedback != OSIX_FALSE)
    {
        if ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.
             u1McsFeedback) !=
            (pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.
             u1McsFeedback))
        {
            *u1IsValChanged = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bMcsFeedback = OSIX_TRUE;
            /*Using Supp variables only HT Ext Cap will be assembled */
            pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.
                u1SuppMcsFeedback =
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.
                u1McsFeedback;
        }
    }
    else
    {
        /*Using Supp variables only HT Ext Cap will be assembled */
        pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.
            u1SuppMcsFeedback =
            pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.
            u1McsFeedback;
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bHtcSupp != OSIX_FALSE)
    {
        if ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.u1HtcSupp) !=
            (pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.
             u1HtcSupp))
        {
            *u1IsValChanged = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bHtcSupp = OSIX_TRUE;
            /*Using Supp variables only HT Ext Cap will be assembled */
            pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.
                u1SuppHtcSupp =
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.u1HtcSupp;
        }
    }
    else
    {
        /*Using Supp variables only HT Ext Cap will be assembled */
        pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.
            u1SuppHtcSupp =
            pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.u1HtcSupp;
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bRdResponder != OSIX_FALSE)
    {
        if ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.
             u1RdResponder) !=
            (pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.
             u1RdResponder))
        {
            *u1IsValChanged = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bRdResponder = OSIX_TRUE;
            /*Using Supp variables only HT Ext Cap will be assembled */
            pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.
                u1SuppRdResponder =
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.
                u1RdResponder;
        }
    }
    else
    {
        /*Using Supp variables only HT Ext Cap will be assembled */
        pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.
            u1SuppRdResponder =
            pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.
            u1RdResponder;
    }

}

/*****************************************************************************
 * Function Name      : RadioIfSetFsDot11nAmpdu                              *
 *                                                                           *
 * Description        : This function constructs the 802.11n Parameters and  *
 *                      sends it to WLC Handler and update the WSSIF DB in   *
 *                      case the configuration is succesfully applied in WTP *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to Radio IF DB with ifIndex  *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
VOID
RadioIfSetFsDot11nAmpdu (tRadioIfGetDB * pRadioIfGetDB,
                         tRadioIfGetDB * pFromDbRadioIfGetDB,
                         UINT1 *u1IsValChanged)
{
    if (pRadioIfGetDB->RadioIfIsGetAllDB.bMaxAmpduLen != OSIX_FALSE)
    {
        if ((pRadioIfGetDB->RadioIfGetAllDB.
             Dot11NampduParams.u1MaxAmpduLen) !=
            (pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NampduParams.
             u1MaxAmpduLen))
        {
            *u1IsValChanged = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bMaxAmpduLen = OSIX_TRUE;
            /*Using Supp variables only Ampdu will be assembled */
            pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NampduParams.
                u1SuppMaxAmpduLen =
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NampduParams.u1MaxAmpduLen;
        }
    }
    else
    {
        /*Using Supp variables only ampdu will be assembled */
        pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NampduParams.
            u1SuppMaxAmpduLen =
            pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NampduParams.
            u1MaxAmpduLen;
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bMinAmpduStart != OSIX_FALSE)
    {
        if ((pRadioIfGetDB->RadioIfGetAllDB.
             Dot11NampduParams.u1MinAmpduStart) !=
            (pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NampduParams.
             u1MinAmpduStart))
        {
            *u1IsValChanged = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bMinAmpduStart = OSIX_TRUE;
            /*Using Supp variables only Ampdu will be assembled */
            pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NampduParams.
                u1SuppMinAmpduStart =
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NampduParams.
                u1MinAmpduStart;
        }
    }
    else
    {
        /*Using Supp variables only ampdu will be assembled */
        pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NampduParams.
            u1SuppMinAmpduStart =
            pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NampduParams.
            u1MinAmpduStart;
    }
}

/*****************************************************************************
 * Function Name      : RadioIfSetFsDot11nSuppMsc                                *
 *                                                                           *
 * Description        : This function constructs the 802.11n Parameters and  *
 *                      sends it to WLC Handler and update the WSSIF DB in   *
 *                      case the configuration is succesfully applied in WTP *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to Radio IF DB with ifIndex  *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
VOID
RadioIfSetFsDot11nSuppMsc (tRadioIfGetDB * pRadioIfGetDB,
                           tRadioIfGetDB * pFromDbRadioIfGetDB,
                           UINT1 *u1IsValChanged)
{

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bRxMcsBitmask != OSIX_FALSE)
    {
        if ((pRadioIfGetDB->RadioIfGetAllDB.
             Dot11NsuppMcsParams.au1RxMcsBitmask[0]) !=
            (pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
             au1RxMcsBitmask[0]))
        {
            *u1IsValChanged = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bRxMcsBitmask = OSIX_TRUE;
            /*Using Supp variables only mcs will be assembled */
            pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                au1SuppRxMcsBitmask[0] =
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                au1RxMcsBitmask[0];
        }
        else
        {
            /*Using Supp variables only mcs will be assembled */
            pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                au1SuppRxMcsBitmask[0] =
                pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                au1RxMcsBitmask[0];
        }

        if ((pRadioIfGetDB->RadioIfGetAllDB.
             Dot11NsuppMcsParams.au1RxMcsBitmask[1]) !=
            (pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
             au1RxMcsBitmask[1]))
        {
            *u1IsValChanged = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bRxMcsBitmask = OSIX_TRUE;
            /*Using Supp variables only mcs will be assembled */
            pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                au1SuppRxMcsBitmask[1] =
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                au1RxMcsBitmask[1];
        }
        else
        {
            /*Using Supp variables only mcs will be assembled */
            pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                au1SuppRxMcsBitmask[1] =
                pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                au1RxMcsBitmask[1];
        }

        if ((pRadioIfGetDB->RadioIfGetAllDB.
             Dot11NsuppMcsParams.au1RxMcsBitmask[2]) !=
            (pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
             au1RxMcsBitmask[2]))
        {
            *u1IsValChanged = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bRxMcsBitmask = OSIX_TRUE;
            /*Using Supp variables only mcs will be assembled */
            pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                au1SuppRxMcsBitmask[2] =
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                au1RxMcsBitmask[2];
        }
        else
        {
            /*Using Supp variables only mcs will be assembled */
            pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                au1SuppRxMcsBitmask[2] =
                pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                au1RxMcsBitmask[2];
        }
    }
    else
    {
        MEMCPY (pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                au1SuppRxMcsBitmask,
                pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                au1RxMcsBitmask, 3);
    }

    if (pFromDbRadioIfGetDB->RadioIfGetAllDB.
        Dot11NsuppMcsParams.au1SuppRxMcsBitmask[0] != 0)
    {
        WssIfDot11nHighestSuppDataRate (pFromDbRadioIfGetDB);
    }
    if (pRadioIfGetDB->RadioIfIsGetAllDB.bTxMcsSetDefined != OSIX_FALSE)
    {
        if ((pRadioIfGetDB->RadioIfGetAllDB.
             Dot11NsuppMcsParams.u1TxMcsSetDefined) !=
            (pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
             u1TxMcsSetDefined))
        {
            *u1IsValChanged = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bTxMcsSetDefined = OSIX_TRUE;
            /*Using Supp variables only mcs will be assembled */
            pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                u1SuppTxMcsSetDefined =
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                u1TxMcsSetDefined;
        }
    }
    else
    {
        /*Using Supp variables only mcs will be assembled */
        pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
            u1SuppTxMcsSetDefined =
            pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
            u1TxMcsSetDefined;
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bTxRxMcsSetNotEqual != OSIX_FALSE)
    {
        if ((pRadioIfGetDB->RadioIfGetAllDB.
             Dot11NsuppMcsParams.u1TxRxMcsSetNotEqual) !=
            (pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
             u1TxRxMcsSetNotEqual))
        {
            *u1IsValChanged = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bTxRxMcsSetNotEqual = OSIX_TRUE;
            /*Using Supp variables only mcs will be assembled */
            pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                u1SuppTxRxMcsSetNotEqual =
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                u1TxRxMcsSetNotEqual;
        }
    }
    else
    {
        /*Using Supp variables only mcs will be assembled */
        pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
            u1SuppTxRxMcsSetNotEqual =
            pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
            u1TxRxMcsSetNotEqual;
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bTxMaxNoSpatStrm != OSIX_FALSE)
    {
        if ((pRadioIfGetDB->RadioIfGetAllDB.
             Dot11NsuppMcsParams.u1TxMaxNoSpatStrm) !=
            (pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
             u1TxMaxNoSpatStrm))
        {
            *u1IsValChanged = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bTxMaxNoSpatStrm = OSIX_TRUE;
            /*Using Supp variables only mcs will be assembled */
            pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                u1SuppTxMaxNoSpatStrm =
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                u1TxMaxNoSpatStrm;
        }
    }
    else
    {
        /*Using Supp variables only mcs will be assembled */
        pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
            u1SuppTxMaxNoSpatStrm =
            pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
            u1TxMaxNoSpatStrm;
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bTxUnequalModSupp != OSIX_FALSE)
    {
        if ((pRadioIfGetDB->RadioIfGetAllDB.
             Dot11NsuppMcsParams.u1TxUnequalModSupp) !=
            (pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
             u1TxUnequalModSupp))
        {
            *u1IsValChanged = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bTxUnequalModSupp = OSIX_TRUE;
            /*Using Supp variables only mcs will be assembled */
            pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                u1SuppTxUnequalModSupp =
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                u1TxUnequalModSupp;
        }
    }
    else
    {
        /*Using Supp variables only mcs will be assembled */
        pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
            u1SuppTxUnequalModSupp =
            pFromDbRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
            u1TxUnequalModSupp;
    }

}

/*****************************************************************************
 * Function Name      : RadioIfSetFsDot11nParams                             *
 *                                                                           *
 * Description        : This function constructs the 802.11n Parameters and  *
 *                      sends it to WLC Handler and update the WSSIF DB in   *
 *                      case the configuration is succesfully applied in WTP *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to Radio IF DB with ifIndex  *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfSetFsDot11nParams (tRadioIfGetDB * pRadioIfGetDB)
{
    tRadioIfGetDB       RadioIfGetDB;
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;
    INT4                i4RetVal = 0;
    UINT1               u1IsValChanged = 0;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    if (pRadioIfGetDB == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetFsDot11nParams : Null Input received \r\n");
        return OSIX_FAILURE;
    }
    pWlcHdlrMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pWlcHdlrMsgStruct == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetFsDot11nParams:- "
                     "UtlShMemAllocWlcBuf returned failure\n");
        return OSIX_FAILURE;
    }

    MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
        pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;

    /*Filling the booleans to get 11N params from DB */
    MEMSET (&(RadioIfGetDB.RadioIfIsGetAllDB), 1,
            sizeof (RadioIfGetDB.RadioIfIsGetAllDB));
    /* Get the Radio IF DB info to fill the structure  */
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N,
                                  &RadioIfGetDB) != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetFsDot11nParams :"
                     " RadioIf DB access failed. \r\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;

    }
    /*Resetting the booleans */
    MEMSET (&(RadioIfGetDB.RadioIfIsGetAllDB), 0,
            sizeof (RadioIfGetDB.RadioIfIsGetAllDB));

    /***************************************
     * HT Capabilities Info Updation       *
     **************************************/
    if (pRadioIfGetDB->RadioIfIsGetAllDB.bLdpcCoding != OSIX_FALSE)
    {
        if ((pRadioIfGetDB->RadioIfGetAllDB.
             Dot11NCapaParams.u1LdpcCoding) !=
            (RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1LdpcCoding))
        {
            u1IsValChanged = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bLdpcCoding = OSIX_TRUE;
            /*Using Supp variables only HtCapInfo will be assembled */
            RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.
                u1SuppLdpcCoding =
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u1LdpcCoding;
        }
    }
    else
    {
        /*Using Supp variables only HtCapInfo will be assembled */
        RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1SuppLdpcCoding =
            RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1LdpcCoding;
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bChannelWidth != OSIX_FALSE)
    {
        if ((pRadioIfGetDB->RadioIfGetAllDB.
             Dot11NCapaParams.u1ChannelWidth) !=
            (RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1ChannelWidth))
        {
            u1IsValChanged = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bChannelWidth = OSIX_TRUE;
            /*Using Supp variables only HtCapInfo will be assembled */
            RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.
                u1SuppChannelWidth =
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u1ChannelWidth;
        }
    }
    else
    {
        /*Using Supp variables only HtCapInfo will be assembled */
        RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1SuppChannelWidth =
            RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1ChannelWidth;
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bSmPowerSave != OSIX_FALSE)
    {
        if ((pRadioIfGetDB->RadioIfGetAllDB.
             Dot11NCapaParams.u1SmPowerSave) !=
            (RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1SmPowerSave))
        {
            u1IsValChanged = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bSmPowerSave = OSIX_TRUE;
            /*Using Supp variables only HtCapInfo will be assembled */
            RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.
                u1SuppSmPowerSave =
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u1SmPowerSave;
        }
    }
    else
    {
        /*Using Supp variables only HtCapInfo will be assembled */
        RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1SuppSmPowerSave =
            RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1SmPowerSave;
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bHtGreenField != OSIX_FALSE)
    {
        if ((pRadioIfGetDB->RadioIfGetAllDB.
             Dot11NCapaParams.u1HtGreenField) !=
            (RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1HtGreenField))
        {
            u1IsValChanged = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bHtGreenField = OSIX_TRUE;
            /*Using Supp variables only HtCapInfo will be assembled */
            RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.
                u1SuppHtGreenField =
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u1HtGreenField;
        }
    }
    else
    {
        /*Using Supp variables only HtCapInfo will be assembled */
        RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1SuppHtGreenField =
            RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1HtGreenField;
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bShortGi20 != OSIX_FALSE)
    {
        if ((pRadioIfGetDB->RadioIfGetAllDB.
             Dot11NCapaParams.u1ShortGi20) !=
            (RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1ShortGi20))
        {
            u1IsValChanged = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bShortGi20 = OSIX_TRUE;
            /*Using Supp variables only HtCapInfo will be assembled */
            RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1SuppShortGi20
                = pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u1ShortGi20;
        }
    }
    else
    {
        /*Using Supp variables only HtCapInfo will be assembled */
        RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1SuppShortGi20 =
            RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1ShortGi20;
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bShortGi40 != OSIX_FALSE)
    {
        if ((pRadioIfGetDB->RadioIfGetAllDB.
             Dot11NCapaParams.u1ShortGi40) !=
            (RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1ShortGi40))
        {
            u1IsValChanged = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bShortGi40 = OSIX_TRUE;
            /*Using Supp variables only HtCapInfo will be assembled */
            RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1SuppShortGi40
                = pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u1ShortGi40;
        }
    }
    else
    {
        /*Using Supp variables only HtCapInfo will be assembled */
        RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1SuppShortGi40 =
            RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1ShortGi40;
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bTxStbc != OSIX_FALSE)
    {
        if ((pRadioIfGetDB->RadioIfGetAllDB.
             Dot11NCapaParams.u1TxStbc) !=
            (RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1TxStbc))
        {
            u1IsValChanged = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bTxStbc = OSIX_TRUE;
            /*Using Supp variables only HtCapInfo will be assembled */
            RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1SuppTxStbc
                = pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u1TxStbc;
        }
    }
    else
    {
        /*Using Supp variables only HtCapInfo will be assembled */
        RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1SuppTxStbc =
            RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1TxStbc;
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bRxStbc != OSIX_FALSE)
    {
        if ((pRadioIfGetDB->RadioIfGetAllDB.
             Dot11NCapaParams.u1RxStbc) !=
            (RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1RxStbc))
        {
            u1IsValChanged = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bRxStbc = OSIX_TRUE;
            /*Using Supp variables only HtCapInfo will be assembled */
            RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1SuppRxStbc
                = pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u1RxStbc;
        }
    }
    else
    {
        /*Using Supp variables only HtCapInfo will be assembled */
        RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1SuppRxStbc =
            RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1RxStbc;
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bDelayedBlockack != OSIX_FALSE)
    {
        if ((pRadioIfGetDB->RadioIfGetAllDB.
             Dot11NCapaParams.u1DelayedBlockack) !=
            (RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1DelayedBlockack))
        {
            u1IsValChanged = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bDelayedBlockack = OSIX_TRUE;
            /*Using Supp variables only HtCapInfo will be assembled */
            RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1SuppDelayedBlockack
                =
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.
                u1DelayedBlockack;
        }
    }
    else
    {
        /*Using Supp variables only HtCapInfo will be assembled */
        RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1SuppDelayedBlockack =
            RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1DelayedBlockack;
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bMaxAmsduLen != OSIX_FALSE)
    {
        if ((pRadioIfGetDB->RadioIfGetAllDB.
             Dot11NCapaParams.u1MaxAmsduLen) !=
            (RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1MaxAmsduLen))
        {
            u1IsValChanged = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bMaxAmsduLen = OSIX_TRUE;
            /*Using Supp variables only HtCapInfo will be assembled */
            RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1SuppMaxAmsduLen
                = pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u1MaxAmsduLen;
        }
    }
    else
    {
        /*Using Supp variables only HtCapInfo will be assembled */
        RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1SuppMaxAmsduLen =
            RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1MaxAmsduLen;
    }
    if (pRadioIfGetDB->RadioIfIsGetAllDB.bDsssCck40 != OSIX_FALSE)
    {
        if ((pRadioIfGetDB->RadioIfGetAllDB.
             Dot11NCapaParams.u1DsssCck40) !=
            (RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1DsssCck40))
        {
            u1IsValChanged = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bDsssCck40 = OSIX_TRUE;
            /*Using Supp variables only HtCapInfo will be assembled */
            RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1SuppDsssCck40
                = pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u1DsssCck40;
        }
    }
    else
    {
        /*Using Supp variables only HtCapInfo will be assembled */
        RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1SuppDsssCck40 =
            RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1DsssCck40;
    }
    if (pRadioIfGetDB->RadioIfIsGetAllDB.bFortyInto != OSIX_FALSE)
    {
        if ((pRadioIfGetDB->RadioIfGetAllDB.
             Dot11NCapaParams.u1FortyInto) !=
            (RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1FortyInto))
        {
            u1IsValChanged = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bFortyInto = OSIX_TRUE;
            /*Using Supp variables only HtCapInfo will be assembled */
            RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1SuppFortyInto
                = pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u1FortyInto;
        }
    }
    else
    {
        /*Using Supp variables only HtCapInfo will be assembled */
        RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1SuppFortyInto =
            RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1FortyInto;
    }
    if (pRadioIfGetDB->RadioIfIsGetAllDB.bLsigTxopProtFulSup != OSIX_FALSE)
    {
        if ((pRadioIfGetDB->RadioIfGetAllDB.
             Dot11NCapaParams.u1LsigTxopProtFulSup) !=
            (RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.
             u1LsigTxopProtFulSup))
        {
            u1IsValChanged = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bLsigTxopProtFulSup = OSIX_TRUE;
            /*Using Supp variables only HtCapInfo will be assembled */
            RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.
                u1SuppLsigTxopProtFulSup =
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.
                u1LsigTxopProtFulSup;
        }
    }
    else
    {
        /*Using Supp variables only HtCapInfo will be assembled */
        RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1SuppLsigTxopProtFulSup =
            RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1LsigTxopProtFulSup;
    }

    /***********************************
     * Supported MCS Updation          *
     * *********************************/
    RadioIfSetFsDot11nSuppMsc (pRadioIfGetDB, &RadioIfGetDB, &u1IsValChanged);

    /****************************************************
     * Set Rx-Stbc value according to standard          *
     * *************************************************/

    if (RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1RxStbc !=
        RX_STBC_VALUE_DISABLE
        || pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u1RxStbc !=
        RX_STBC_VALUE_DISABLE)
    {
        if (pRadioIfGetDB->RadioIfIsGetAllDB.bRxMcsBitmask == OSIX_TRUE)
        {
            Dot11nCalculateRxStbc (pRadioIfGetDB);
            pRadioIfGetDB->RadioIfIsGetAllDB.bRxStbc = OSIX_TRUE;
            RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1SuppRxStbc =
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u1RxStbc;
        }
        else
        {
            Dot11nCalculateRxStbc (&RadioIfGetDB);
            pRadioIfGetDB->RadioIfIsGetAllDB.bRxStbc = OSIX_TRUE;
            RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1SuppRxStbc =
                RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1RxStbc;
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u1RxStbc =
                RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1RxStbc;
        }
    }

    /***************************************
     * A-MPDU Param Updation               *
     **************************************/
    RadioIfSetFsDot11nAmpdu (pRadioIfGetDB, &RadioIfGetDB, &u1IsValChanged);

    /***************************************
     * HT Ext Capabilities Updation        *
     **************************************/
    RadioIfSetFsDot11nHTExtCapability (pRadioIfGetDB, &RadioIfGetDB,
                                       &u1IsValChanged);

    /***************************************
     *Beam Forming Updation               *
     **************************************/
    RadioIfSetFsDot11nBeamForming (pRadioIfGetDB, &RadioIfGetDB,
                                   &u1IsValChanged);

    if (WssIfProcessRadioIfDBMsg (WSS_ASSEMBLE_11N_CAPABILITY_INFO,
                                  &RadioIfGetDB) != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetDot11nCapaParams:"
                     "Failed to assemble capability params\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;
    }

    if (WssIfProcessRadioIfDBMsg (WSS_ASSEMBLE_11N_AMPDU_INFO,
                                  &RadioIfGetDB) != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetDot11nCapaParams:"
                     "Failed to assemble capability params\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;
    }
    if (WssIfProcessRadioIfDBMsg (WSS_ASSEMBLE_11N_MCS_INFO,
                                  &RadioIfGetDB) != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetDot11nCapaParams:"
                     "Failed to assemble capability params\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;
    }
    if (WssIfProcessRadioIfDBMsg (WSS_ASSEMBLE_11N_EXTCAP_INFO,
                                  &RadioIfGetDB) != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetDot11nCapaParams:"
                     "Failed to assemble capability params\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;
    }
    if (WssIfProcessRadioIfDBMsg (WSS_ASSEMBLE_11N_BEAMFORMING_INFO,
                                  &RadioIfGetDB) != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetDot11nCapaParams:"
                     "Failed to assemble capability params\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;
    }

    pRadioIfGetDB->RadioIfIsGetAllDB.bHtCapInfo = OSIX_TRUE;
    pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u2HtCapInfo =
        RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u2HtCapInfo;

    pRadioIfGetDB->RadioIfIsGetAllDB.bAmpduParam = OSIX_TRUE;
    pRadioIfGetDB->RadioIfGetAllDB.Dot11NampduParams.u1AmpduParam =
        RadioIfGetDB.RadioIfGetAllDB.Dot11NampduParams.u1AmpduParam;

    pRadioIfGetDB->RadioIfIsGetAllDB.bHtCapaMcs = OSIX_TRUE;
    MEMCPY (pRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.au1HtCapaMcs,
            RadioIfGetDB.RadioIfGetAllDB.Dot11NsuppMcsParams.au1HtCapaMcs,
            DOT11N_HT_CAP_MCS_LEN);

    pRadioIfGetDB->RadioIfIsGetAllDB.bHtExtCap = OSIX_TRUE;
    pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.u2HtExtCap =
        RadioIfGetDB.RadioIfGetAllDB.Dot11NhtExtCapParams.u2HtExtCap;

    pRadioIfGetDB->RadioIfIsGetAllDB.bTxBeamCapParam = OSIX_TRUE;
    pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.u4TxBeamCapParam =
        RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.u4TxBeamCapParam;

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bTxAntenna != OSIX_TRUE)
    {
        pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
            RadioIfDot11nParam.u1TxAntenna =
            RadioIfGetDB.RadioIfGetAllDB.u1TxAntenna;
    }
    else
    {
        pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
            RadioIfDot11nParam.u1TxAntenna =
            pRadioIfGetDB->RadioIfGetAllDB.u1TxAntenna;

        if ((pRadioIfGetDB->RadioIfGetAllDB.u1TxAntenna) !=
            (RadioIfGetDB.RadioIfGetAllDB.u1TxAntenna))
        {
            u1IsValChanged = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bTxAntenna = OSIX_TRUE;
        }
    }
    if (pRadioIfGetDB->RadioIfIsGetAllDB.bRxAntenna != OSIX_TRUE)
    {
        pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
            RadioIfDot11nParam.u1RxAntenna =
            RadioIfGetDB.RadioIfGetAllDB.u1RxAntenna;
    }
    else
    {
        pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
            RadioIfDot11nParam.u1RxAntenna =
            pRadioIfGetDB->RadioIfGetAllDB.u1RxAntenna;

        if ((pRadioIfGetDB->RadioIfGetAllDB.u1RxAntenna) !=
            (RadioIfGetDB.RadioIfGetAllDB.u1RxAntenna))
        {
            u1IsValChanged = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bRxAntenna = OSIX_TRUE;
        }
    }

    /*If the configured value differ from the DB value, then WssifRadioIfDB updation
     * takes place*/
    if (u1IsValChanged == OSIX_FALSE)
    {
        RADIOIF_TRC (RADIOIF_INFO_TRC,
                     "RadioIfSetFsDot11nParams : No Value Change, "
                     "Not triggering Config Update \r\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_SUCCESS;
    }

    /* Get the Radio IF DB info to fill RadioId and BPFlag structure  */
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bBPFlag = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;

    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                  &RadioIfGetDB) != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetDot11nOperParams : RadioIf DB access failed. \r\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;

    }

    /* HT Capability Information Element in Config Update Request */
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfInfoElem.u1RadioId =
        RadioIfGetDB.RadioIfGetAllDB.u1RadioId;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfInfoElem.u1WlanId =
        RADIO_VALUE_1;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfInfoElem.u1BPFlag =
        RadioIfGetDB.RadioIfGetAllDB.u1BPFlag;

    /* nmhRoutines already validated whether the input received and old
     * values are different. Hence fill config update request structure */
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfDot11nParam.u2MessageType =
        RADIO_DOT11N_MSG_TYPE;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfDot11nParam.u2MessageLength
        = RADIO_DOT11N_MSG_LEN;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfDot11nParam.u4VendorId
        = VENDOR_ID;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.u2WtpInternalId
        = RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;

    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfDot11nParam.u1RadioId
        = RadioIfGetDB.RadioIfGetAllDB.u1RadioId;

    /* HT Capability Information Element in Config Update Request */
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfInfoElem.u2MessageType
        = RADIO_INFO_ELEM_MSG_TYPE;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfInfoElem.u2MessageLength
        = RADIO_INFO_ELEM_FIXED_MSG_LEN + RADIO_INFO_HT_CAP_MSG_LEN;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfInfoElem.isPresent
        = OSIX_TRUE;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfInfoElem.u1EleId
        = RADIO_INFO_HT_CAP_ELEM_ID;

    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfInfoElem.unInfoElem.
        HTCapability.u1ElemLen = RADIO_INFO_HT_CAP_MSG_LEN;

    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfInfoElem.unInfoElem.
        HTCapability.u1ElemId = RADIO_INFO_HT_CAP_ELEM_ID;

    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfInfoElem.unInfoElem.
        HTCapability.isOptional = OSIX_TRUE;

    RadioIfGetDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_FALSE;

    /* Construct configupdate req for 802.11n HT Capabilities element */
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
        RadioIfInfoElem.unInfoElem.HTCapability.u2HTCapInfo =
        pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u2HtCapInfo;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
        RadioIfInfoElem.unInfoElem.HTCapability.u1AMPDUParam =
        pRadioIfGetDB->RadioIfGetAllDB.Dot11NampduParams.u1AmpduParam;
    MEMCPY (pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
            RadioIfInfoElem.unInfoElem.HTCapability.au1SuppMCSSet,
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.au1HtCapaMcs,
            RADIOIF_11N_MCS_RATE_LEN_PKT);
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
        RadioIfInfoElem.unInfoElem.HTCapability.u2HTExtCap =
        pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.u2HtExtCap;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
        RadioIfInfoElem.unInfoElem.HTCapability.u4TranBeamformCap =
        pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.u4TxBeamCapParam;
    /* Invoke WLC Handler to send the config update request for Information
     * Element*/
    i4RetVal = WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_RADIO_CONF_UPDATE_REQ,
                                       pWlcHdlrMsgStruct);

    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfDot11nParam.isPresent
        = OSIX_TRUE;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfInfoElem.isPresent
        = OSIX_FALSE;
    /* Invoke WLC Handler to send the config update request for 802.11n Radio
     * Configuration Message Element */
    i4RetVal = WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_RADIO_CONF_UPDATE_REQ,
                                       pWlcHdlrMsgStruct);
    /* Invoke WLC Handler to send the config update request */
    if (i4RetVal != OSIX_SUCCESS)
    {
        if (i4RetVal == RADIOIF_NO_AP_PRESENT)
        {
            gu1IsConfigResponseReceived = RADIOIF_NO_AP_PRESENT;
        }
        else
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfSetFsDot11nParams : Mac Operation failed \r\n");
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            return OSIX_FAILURE;
        }
    }
    if (WssIfProcessRadioIfDBMsg (WSS_SET_RADIO_IF_DB_11N,
                                  pRadioIfGetDB) != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetFsDot11nParams: Radio DB Update failure\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;
    }

    /*During run state all the station should be disassociated, 
     * when any radio paramter is changed*/
    if (i4RetVal != RADIOIF_NO_AP_PRESENT)
    {
        if (RadioIfDisassocStation (&RadioIfGetDB) != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfSetFsDot11nParams: cannot disassoc the stations\n");
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            return OSIX_FAILURE;

        }
    }
    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfSetFsDot11nOperParams                         *
 *                                                                           *
 * Description        : This function constructs the 802.11n Operation       *
 *                      parameters and sends it to WLC Handler and update    * 
 *                      the WSSIF DB in case the configuration is            * 
 *                      succesfully applied in WTP                           *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to Radio IF DB with ifIndex  *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfSetDot11nOperParams (tRadioIfGetDB * pRadioIfGetDB)
{
    tRadioIfGetDB       RadioIfGetDB;
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;
    INT4                i4RetVal = 0;
    UINT1               u1IsValChanged = 0;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    if (pRadioIfGetDB == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetDot11nOperParams: Null Input received \r\n");
        return OSIX_FAILURE;
    }

    pWlcHdlrMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pWlcHdlrMsgStruct == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetDot11nOperParams:- "
                     "UtlShMemAllocWlcBuf returned failure\n");
        return OSIX_FAILURE;
    }

    MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
        pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bPrimaryChannel = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bBasicMCSSet = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bSecondaryChannel = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bStaChanWidth = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bRifsMode = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bHtProtection = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bNongfHtStasPresent = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bObssNonHtStasPresent = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bDualBeacon = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bDualCtsProtection = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bStbcBeacon = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bLsigTxopProtFulSup = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bPcoActive = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bPcoPhase = OSIX_TRUE;

    /* Get the Radio IF DB info to fill the structure  */
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N,
                                  &RadioIfGetDB) != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetDot11nCapaParams:"
                     "RadioIf DB access failed. \r\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;

    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bPrimaryChannel != OSIX_FALSE)
    {
        if ((pRadioIfGetDB->RadioIfGetAllDB.
             Dot11NhtOperation.u1PrimaryChannel) !=
            (RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.u1PrimaryChannel))
        {
            u1IsValChanged = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bPrimaryChannel = OSIX_TRUE;
            /*Using Supp variables only HtOperInfo will be assembled */
            RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.u1SuppPrimaryChannel
                =
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.
                u1PrimaryChannel;
        }
    }
    else
    {
        /*Using Supp variables only HtCapInfo will be assembled */
        RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.u1SuppPrimaryChannel =
            RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.u1PrimaryChannel;
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bSecondaryChannel != OSIX_FALSE)
    {
        if ((pRadioIfGetDB->RadioIfGetAllDB.
             Dot11NhtOperation.u1SecondaryChannel) !=
            (RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.u1SecondaryChannel))
        {
            u1IsValChanged = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bSecondaryChannel = OSIX_TRUE;
            /*Using Supp variables only HtOperInfo will be assembled */
            RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.
                u1SuppSecondaryChannel =
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.
                u1SecondaryChannel;
        }
    }
    else
    {
        /*Using Supp variables only HtCapInfo will be assembled */
        RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.u1SuppSecondaryChannel =
            RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.u1SecondaryChannel;
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bStaChanWidth != OSIX_FALSE)
    {
        if ((pRadioIfGetDB->RadioIfGetAllDB.
             Dot11NhtOperation.u1StaChanWidth) !=
            (RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.u1StaChanWidth))
        {
            u1IsValChanged = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bStaChanWidth = OSIX_TRUE;
            /*Using Supp variables only HtOperInfo will be assembled */
            RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.u1SuppStaChanWidth
                =
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.u1StaChanWidth;
        }
    }
    else
    {
        /*Using Supp variables only HtCapInfo will be assembled */
        RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.u1SuppStaChanWidth =
            RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.u1StaChanWidth;
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bRifsMode != OSIX_FALSE)
    {
        if ((pRadioIfGetDB->RadioIfGetAllDB.
             Dot11NhtOperation.u1RifsMode) !=
            (RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.u1RifsMode))
        {
            u1IsValChanged = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bRifsMode = OSIX_TRUE;
            /*Using Supp variables only HtOperInfo will be assembled */
            RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.u1SuppRifsMode
                = pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.u1RifsMode;
        }
    }
    else
    {
        /*Using Supp variables only HtCapInfo will be assembled */
        RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.u1SuppRifsMode =
            RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.u1RifsMode;
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bHtProtection != OSIX_FALSE)
    {
        if ((pRadioIfGetDB->RadioIfGetAllDB.
             Dot11NhtOperation.u1HtProtection) !=
            (RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.u1HtProtection))
        {
            u1IsValChanged = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bHtProtection = OSIX_TRUE;
            /*Using Supp variables only HtOperInfo will be assembled */
            RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.u1SuppHtProtection
                =
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.u1HtProtection;
        }
    }
    else
    {
        /*Using Supp variables only HtCapInfo will be assembled */
        RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.u1SuppHtProtection =
            RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.u1HtProtection;
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bNongfHtStasPresent != OSIX_FALSE)
    {
        if ((pRadioIfGetDB->RadioIfGetAllDB.
             Dot11NhtOperation.u1NongfHtStasPresent) !=
            (RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.
             u1NongfHtStasPresent))
        {
            u1IsValChanged = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bNongfHtStasPresent = OSIX_TRUE;
            /*Using Supp variables only HtOperInfo will be assembled */
            RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.
                u1SuppNongfHtStasPresent =
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.
                u1NongfHtStasPresent;
        }
    }
    else
    {
        /*Using Supp variables only HtCapInfo will be assembled */
        RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.
            u1SuppNongfHtStasPresent =
            RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.u1NongfHtStasPresent;
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bObssNonHtStasPresent != OSIX_FALSE)
    {
        if ((pRadioIfGetDB->RadioIfGetAllDB.
             Dot11NhtOperation.u1ObssNonHtStasPresent) !=
            (RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.
             u1ObssNonHtStasPresent))
        {
            u1IsValChanged = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bObssNonHtStasPresent = OSIX_TRUE;
            /*Using Supp variables only HtOperInfo will be assembled */
            RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.
                u1SuppObssNonHtStasPresent =
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.
                u1ObssNonHtStasPresent;
        }
    }
    else
    {
        /*Using Supp variables only HtCapInfo will be assembled */
        RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.
            u1SuppObssNonHtStasPresent =
            RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.
            u1ObssNonHtStasPresent;
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bDualBeacon != OSIX_FALSE)
    {
        if ((pRadioIfGetDB->RadioIfGetAllDB.
             Dot11NhtOperation.u1DualBeacon) !=
            (RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.u1DualBeacon))
        {
            u1IsValChanged = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bDualBeacon = OSIX_TRUE;
            /*Using Supp variables only HtOperInfo will be assembled */
            RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.u1SuppDualBeacon
                = pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.u1DualBeacon;
        }
    }
    else
    {
        /*Using Supp variables only HtCapInfo will be assembled */
        RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.u1SuppDualBeacon =
            RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.u1DualBeacon;
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bDualCtsProtection != OSIX_FALSE)
    {
        if ((pRadioIfGetDB->RadioIfGetAllDB.
             Dot11NhtOperation.u1DualCtsProtection) !=
            (RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.
             u1DualCtsProtection))
        {
            u1IsValChanged = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bDualCtsProtection = OSIX_TRUE;
            /*Using Supp variables only HtOperInfo will be assembled */
            RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.
                u1SuppDualCtsProtection =
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.
                u1DualCtsProtection;
        }
    }
    else
    {
        /*Using Supp variables only HtCapInfo will be assembled */
        RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.u1SuppDualCtsProtection =
            RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.u1DualCtsProtection;
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bStbcBeacon != OSIX_FALSE)
    {
        if ((pRadioIfGetDB->RadioIfGetAllDB.
             Dot11NhtOperation.u1StbcBeacon) !=
            (RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.u1StbcBeacon))
        {
            u1IsValChanged = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bStbcBeacon = OSIX_TRUE;
            /*Using Supp variables only HtOperInfo will be assembled */
            RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.u1SuppStbcBeacon
                = pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.u1StbcBeacon;
        }
    }
    else
    {
        /*Using Supp variables only HtCapInfo will be assembled */
        RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.u1SuppStbcBeacon =
            RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.u1StbcBeacon;
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bLsigTxopProtFulSup != OSIX_FALSE)
    {
        if ((pRadioIfGetDB->RadioIfGetAllDB.
             Dot11NhtOperation.u1LsigTxopProtFulSup) !=
            (RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.
             u1LsigTxopProtFulSup))
        {
            u1IsValChanged = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bLsigTxopProtFulSup = OSIX_TRUE;
            /*Using Supp variables only HtOperInfo will be assembled */
            RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.
                u1SuppLsigTxopProtFulSup =
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.
                u1LsigTxopProtFulSup;
        }
    }
    else
    {
        /*Using Supp variables only HtCapInfo will be assembled */
        RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.
            u1SuppLsigTxopProtFulSup =
            RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.u1LsigTxopProtFulSup;
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bPcoActive != OSIX_FALSE)
    {
        if ((pRadioIfGetDB->RadioIfGetAllDB.
             Dot11NhtOperation.u1PcoActive) !=
            (RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.u1PcoActive))
        {
            u1IsValChanged = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bPcoActive = OSIX_TRUE;
            /*Using Supp variables only HtOperInfo will be assembled */
            RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.u1SuppPcoActive
                = pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.u1PcoActive;
        }
    }
    else
    {
        /*Using Supp variables only HtCapInfo will be assembled */
        RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.u1SuppPcoActive =
            RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.u1PcoActive;
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bPcoPhase != OSIX_FALSE)
    {
        if ((pRadioIfGetDB->RadioIfGetAllDB.
             Dot11NhtOperation.u1PcoPhase) !=
            (RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.u1PcoPhase))
        {
            u1IsValChanged = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bPcoPhase = OSIX_TRUE;
            /*Using Supp variables only HtOperInfo will be assembled */
            RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.u1SuppPcoPhase
                = pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.u1PcoPhase;
        }
    }
    else
    {
        /*Using Supp variables only HtCapInfo will be assembled */
        RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.u1SuppPcoPhase =
            RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.u1PcoPhase;
    }

    MEMCPY (RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.au1SuppBasicMCSSet,
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.au1BasicMCSSet,
            WSSMAC_BASIC_MCS_SET);

    if (u1IsValChanged == OSIX_FALSE)
    {
        RADIOIF_TRC (RADIOIF_INFO_TRC,
                     "RadioIfSetDot11nOperParams : No Value Change, "
                     "Not triggering Config Update \r\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_SUCCESS;
    }

    if (WssIfProcessRadioIfDBMsg (WSS_ASSEMBLE_11N_OPER_INFO,
                                  &RadioIfGetDB) != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetDot11nCapaParams:"
                     "Failed to assemble capability params\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;
    }

    pRadioIfGetDB->RadioIfIsGetAllDB.bPrimaryChannel = OSIX_TRUE;
    pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.u1PrimaryChannel =
        RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.u1SuppPrimaryChannel;

    pRadioIfGetDB->RadioIfIsGetAllDB.bHTOpeInfo = OSIX_TRUE;
    MEMCPY (pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.au1HTOpeInfo,
            RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.au1HTOpeInfo,
            WSSMAC_HTOPE_INFO);

    pRadioIfGetDB->RadioIfIsGetAllDB.bBasicMCSSet = OSIX_TRUE;
    MEMCPY (pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.au1BasicMCSSet,
            RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.au1SuppBasicMCSSet,
            WSSMAC_BASIC_MCS_SET);

    /* Get the Radio IF DB info to fill RadioId and BPFlag structure  */
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bBPFlag = OSIX_TRUE;

    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                  &RadioIfGetDB) != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetDot11nOperParams : RadioIf DB access failed. \r\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;

    }

    /* nmhRoutines already validated whether the input received and old
     * values are different. Hence fill config update request structure */
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfDot11nParam.u2MessageType =
        RADIO_DOT11N_MSG_TYPE;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfDot11nParam.u2MessageLength
        = RADIO_DOT11N_MSG_LEN;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfDot11nParam.u4VendorId
        = VENDOR_ID;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.u2WtpInternalId
        = RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfDot11nParam.u1RadioId
        = RadioIfGetDB.RadioIfGetAllDB.u1RadioId;

    /* HT Opearion Information Element in Config Update Request */
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfInfoElem.u1RadioId =
        RadioIfGetDB.RadioIfGetAllDB.u1RadioId;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfInfoElem.u1WlanId =
        RADIO_VALUE_1;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfInfoElem.u1BPFlag =
        RadioIfGetDB.RadioIfGetAllDB.u1BPFlag;

    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfInfoElem.u2MessageType =
        RADIO_INFO_ELEM_MSG_TYPE;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfInfoElem.u2MessageLength =
        RADIO_INFO_ELEM_FIXED_MSG_LEN + RADIO_INFO_HT_INFO_MSG_LEN;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfInfoElem.isPresent =
        OSIX_TRUE;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfInfoElem.u1EleId =
        RADIO_INFO_HT_INFO_ELEM_ID;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfInfoElem.unInfoElem.
        HTOperation.u1ElemLen = RADIO_INFO_HT_INFO_MSG_LEN;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfInfoElem.unInfoElem.
        HTOperation.u1ElemId = RADIO_INFO_HT_INFO_ELEM_ID;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfInfoElem.unInfoElem.
        HTOperation.isOptional = OSIX_TRUE;

    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
        RadioIfInfoElem.unInfoElem.HTOperation.u1PrimaryCh =
        pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.u1PrimaryChannel;
    MEMCPY (pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
            RadioIfInfoElem.unInfoElem.HTOperation.au1HTOpeInfo,
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.au1HTOpeInfo,
            WSSMAC_HTOPE_INFO);
    MEMCPY (pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfInfoElem.
            unInfoElem.HTOperation.au1BasicMCSSet,
            RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.au1BasicMCSSet,
            WSSMAC_BASIC_MCS_SET);

    /* Invoke WLC Handler to send the 
     * config update request for HT Operation Information Element*/
    i4RetVal = WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_RADIO_CONF_UPDATE_REQ,
                                       pWlcHdlrMsgStruct);

    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfDot11nParam.isPresent
        = OSIX_TRUE;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfInfoElem.isPresent
        = OSIX_FALSE;

    /* Invoke WLC Handler to send the config update request */
    if (i4RetVal != OSIX_SUCCESS)
    {
        if (i4RetVal == RADIOIF_NO_AP_PRESENT)
        {
            gu1IsConfigResponseReceived = RADIOIF_NO_AP_PRESENT;
        }
        else
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfSetDot11nOperParams:"
                         "Mac Operation failed \r\n");
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            return OSIX_FAILURE;
        }
    }
    if (WssIfProcessRadioIfDBMsg (WSS_SET_RADIO_IF_DB_11N,
                                  pRadioIfGetDB) != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetDot11nOperParams:" "Radio DB Update failure\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;
    }
    /*During run state all the station should be disassociated, 
     * when any radio paramter is changed*/
    if (i4RetVal != RADIOIF_NO_AP_PRESENT)
    {
        if (RadioIfDisassocStation (&RadioIfGetDB) != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfSetFsDot11nParams: cannot disassoc the stations\n");
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            return OSIX_FAILURE;

        }
    }

    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfSetEDCATable                                  *
 *                                                                           *
 * Description        : This function constructs EDCA related message element*
 *                      and send config request to WTP                       *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to Radio IF DB with ifIndex  *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfSetEDCATable (tRadioIfGetDB * pRadioIfGetDB)
{
    tRadioIfGetDB       RadioIfGetDB;
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;
    tWssifauthDBMsgStruct *pWssStaMsg = NULL;
    INT4                i4RetVal = 0;
    UINT1               u1QosIndex = 0;
    UINT4               u4Index = 4;
    UINT1               u1Count = 0;
    UINT1               u1IsValChanged = 0;
    UINT1               u1GetTaggingPolicy = 0;
    UINT1               u1WlanId = 0;
    pWlcHdlrMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pWlcHdlrMsgStruct == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetEDCATable:- "
                     "UtlShMemAllocWlcBuf returned failure\n");
        return OSIX_FAILURE;
    }
    pWssStaMsg =
        (tWssifauthDBMsgStruct *) (VOID *) UtlShMemAllocWssIfAuthDbBuf ();
    if (pWssStaMsg == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "WssWlanSetQosProfile:- "
                     "UtlShMemAllocWssIfAuthDbBuf returned failure\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;
    }
    MEMSET (pWssStaMsg, 0, sizeof (tWssifauthDBMsgStruct));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
        UtlShMemAllocAntennaSelectionBuf ();

    if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC, "\r%% Memory allocation for Antenna"
                     " selection failed.\r\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssStaMsg);
        return OSIX_FAILURE;
    }

    MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));

    if (pRadioIfGetDB == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetEDCATable : Null Input received \r\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                         RadioIfGetAllDB.pu1AntennaSelection);
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssStaMsg);
        return OSIX_FAILURE;
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bRadioIfIndex != OSIX_FALSE)
    {
        /*TO get the RadioIfDb , using RadioIfIndex */
        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
            pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
        RadioIfGetDB.RadioIfIsGetAllDB.bRadioIfIndex = OSIX_TRUE;
    }
    for (u1Count = 0; u1Count < u4Index; u1Count++)
    {
        RadioIfGetDB.RadioIfGetAllDB.u1QosIndex = (UINT1) (u1Count + 1);
        RadioIfGetDB.RadioIfIsGetAllDB.bQosIndex = OSIX_TRUE;

        /*To get the particular values */
        RadioIfGetDB.RadioIfIsGetAllDB.bCwMin = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bCwMax = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bAifsn = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bTxOpLimit = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bTaggingPolicy = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bQueueDepth = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bQosPrio = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bDscp = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bAdmissionControl = OSIX_TRUE;

        /* Get the Radio IF DB info to fill the structure  */
        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_QOS_CONFIG_DB,
                                      &RadioIfGetDB) != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfSetEDCATable : RadioIf DB access failed. \r\n");
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssStaMsg);
            return OSIX_FAILURE;
        }
        if (RadioIfGetDB.RadioIfGetAllDB.u1QosIndex == OSIX_TRUE)
        {
            u1GetTaggingPolicy = RadioIfGetDB.RadioIfGetAllDB.u1TaggingPolicy;
        }
        pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfQos.u2CwMin[u1Count]
            = RadioIfGetDB.RadioIfGetAllDB.au2CwMin[u1Count];
        pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfQos.u2CwMax[u1Count]
            = RadioIfGetDB.RadioIfGetAllDB.au2CwMax[u1Count];
        pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfQos.u1Aifsn[u1Count]
            = RadioIfGetDB.RadioIfGetAllDB.au1Aifsn[u1Count];
        pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfQos.u1Prio[u1Count]
            = RadioIfGetDB.RadioIfGetAllDB.au1QosPrio[u1Count];
        pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfQos.u1Dscp[u1Count]
            = RadioIfGetDB.RadioIfGetAllDB.au1Dscp[u1Count];
        pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfQos.u1Index = u1Count;
    }

    RadioIfGetDB.RadioIfGetAllDB.u1QosIndex =
        pRadioIfGetDB->RadioIfGetAllDB.u1QosIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bQosIndex = OSIX_TRUE;

    RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;

    /* Get the Radio IF DB info to fill the structure  */
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                  &RadioIfGetDB) != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetEDCATable : RadioIf DB access failed. \r\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                         RadioIfGetAllDB.pu1AntennaSelection);
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssStaMsg);
        return OSIX_FAILURE;
    }

    /* nmhRoutines already validated whether the input received and old
     * values are different. Hence fill config update request structure */
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfQos.u2MessageType
        = RADIO_QOS_MSG_TYPE;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfQos.u2MessageLength
        = RADIO_QOS_MSG_LEN;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfQos.u1RadioId
        = RadioIfGetDB.RadioIfGetAllDB.u1RadioId;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.u2WtpInternalId
        = RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfQos.u1Index
        = pRadioIfGetDB->RadioIfGetAllDB.u1QosIndex;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfQos.isPresent = OSIX_TRUE;

    u1QosIndex = (pRadioIfGetDB->RadioIfGetAllDB.u1QosIndex);
    if (pRadioIfGetDB->RadioIfIsGetAllDB.bCwMin == OSIX_TRUE)
    {
        pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
            RadioIfQos.u2CwMin[u1QosIndex - 1] =
            pRadioIfGetDB->RadioIfGetAllDB.au2CwMin[u1QosIndex];

        if ((pRadioIfGetDB->RadioIfGetAllDB.au2CwMin[u1QosIndex]) !=
            (RadioIfGetDB.RadioIfGetAllDB.au2CwMin[u1QosIndex - 1]))
        {
            u1IsValChanged = OSIX_TRUE;
        }
    }
    if (pRadioIfGetDB->RadioIfIsGetAllDB.bCwMax == OSIX_TRUE)
    {
        pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
            RadioIfQos.u2CwMax[u1QosIndex - 1] =
            pRadioIfGetDB->RadioIfGetAllDB.au2CwMax[u1QosIndex];

        if ((pRadioIfGetDB->RadioIfGetAllDB.au2CwMax[u1QosIndex]) !=
            (RadioIfGetDB.RadioIfGetAllDB.au2CwMax[u1QosIndex - 1]))
        {
            u1IsValChanged = OSIX_TRUE;
        }
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bTaggingPolicy != OSIX_TRUE)
    {
        pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfQos.u1TaggingPolicy
            = u1GetTaggingPolicy;
    }
    else
    {
        pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfQos.u1TaggingPolicy
            = pRadioIfGetDB->RadioIfGetAllDB.u1TaggingPolicy;

        if ((pRadioIfGetDB->RadioIfGetAllDB.u1TaggingPolicy) !=
            (u1GetTaggingPolicy))
        {
            u1IsValChanged = OSIX_TRUE;
        }
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bAdmissionControl == OSIX_TRUE)
    {
        pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
            RadioIfQos.u1AdmissionControl[u1QosIndex - 1] =
            pRadioIfGetDB->RadioIfGetAllDB.au1AdmissionControl[u1QosIndex];

    }
    if (pRadioIfGetDB->RadioIfIsGetAllDB.bAifsn == OSIX_TRUE)
    {
        pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
            RadioIfQos.u1Aifsn[u1QosIndex - 1] =
            pRadioIfGetDB->RadioIfGetAllDB.au1Aifsn[u1QosIndex];

        if ((pRadioIfGetDB->RadioIfGetAllDB.au1Aifsn[u1QosIndex]) !=
            (RadioIfGetDB.RadioIfGetAllDB.au1Aifsn[u1QosIndex - 1]))
        {
            u1IsValChanged = OSIX_TRUE;
        }
    }
    if ((pRadioIfGetDB->RadioIfIsGetAllDB.bQosPrio == OSIX_TRUE) &&
        ((u1GetTaggingPolicy
          == CLI_TAG_POLICY_PBIT) ||
         (u1GetTaggingPolicy == CLI_TAG_POLICY_BOTH)))
    {
        pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfQos.u1Prio[u1QosIndex -
                                                                    1] =
            pRadioIfGetDB->RadioIfGetAllDB.au1QosPrio[u1QosIndex];

        if ((pRadioIfGetDB->RadioIfGetAllDB.au1QosPrio[u1QosIndex]) !=
            (RadioIfGetDB.RadioIfGetAllDB.au1QosPrio[u1QosIndex - 1]))
        {
            u1IsValChanged = OSIX_TRUE;
        }
    }
    if ((pRadioIfGetDB->RadioIfIsGetAllDB.bDscp == OSIX_TRUE) &&
        ((u1GetTaggingPolicy
          == CLI_TAG_POLICY_DSCP) ||
         (u1GetTaggingPolicy == CLI_TAG_POLICY_BOTH)))
    {
        pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfQos.u1Dscp[u1QosIndex -
                                                                    1] =
            pRadioIfGetDB->RadioIfGetAllDB.au1Dscp[u1QosIndex];

        if ((pRadioIfGetDB->RadioIfGetAllDB.au1Dscp[u1QosIndex]) !=
            (RadioIfGetDB.RadioIfGetAllDB.au1Dscp[u1QosIndex - 1]))
        {
            u1IsValChanged = OSIX_TRUE;
        }
    }

    /* Update DB */
    pRadioIfGetDB->RadioIfIsGetAllDB.bQosIndex = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_SET_RADIO_QOS_CONFIG_DB_WLC,
                                  pRadioIfGetDB) != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetEDCATable : Radio DB Update failure\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                         RadioIfGetAllDB.pu1AntennaSelection);
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssStaMsg);
        return OSIX_FAILURE;
    }

    if (u1IsValChanged == OSIX_FALSE)
    {
        RADIOIF_TRC (RADIOIF_INFO_TRC,
                     "RadioIfSetEDCATable : No Value Change, Not triggering "
                     "Config Update \r\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                         RadioIfGetAllDB.pu1AntennaSelection);
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssStaMsg);
        return OSIX_SUCCESS;
    }

    i4RetVal = WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_RADIO_CONF_UPDATE_REQ,
                                       pWlcHdlrMsgStruct);

    /* Invoke WLC Handler to send the config update request */
    if (i4RetVal != OSIX_SUCCESS)
    {
        if (i4RetVal == RADIOIF_NO_AP_PRESENT)
        {
            gu1IsConfigResponseReceived = RADIOIF_NO_AP_PRESENT;
        }
        else
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfSetEDCATable : Radio Config Table failed \r\n");
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssStaMsg);
            return OSIX_FAILURE;
        }
    }

    /* WMM changes */

    if (RadioIfGetDB.RadioIfIsGetAllDB.bTaggingPolicy == OSIX_TRUE)
    {
        for (u1WlanId = 1; u1WlanId <= 16; u1WlanId++)
        {
            RadioIfGetDB.RadioIfGetAllDB.u1WlanId = u1WlanId;
            if (WssIfProcessRadioIfDBMsg
                (WSS_GET_WLAN_BSS_IFINDEX_DB, &RadioIfGetDB) == OSIX_SUCCESS)
            {
                pWssStaMsg->WssStaDB.u4BssIfIndex =
                    RadioIfGetDB.RadioIfGetAllDB.u4BssIfIndex;
                if (WssIfProcessWssAuthDBMsg (WSS_STA_GET_CLIENT_MAC_PER_BSSID,
                                              pWssStaMsg) != OSIX_SUCCESS)
                {
                }
            }
        }
    }

    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                     RadioIfGetAllDB.pu1AntennaSelection);
    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssStaMsg);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfSetTxPowerTable                               *
 *                                                                           *
 * Description        : This function constructs Tx Power message element    *
 *                      and send config request to WTP                       *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to Radio IF DB with ifIndex  *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfSetTxPowerTable (tRadioIfGetDB * pRadioIfGetDB)
{
    tRadioIfGetDB       RadioIfGetDB;
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;
    INT4                i4RetVal = 0;
    UINT2               u2MaxTxPowerLevel = 0;
    UINT2               u2Level = 0;
    UINT2               u2PowerLevel = 0;
    UINT1               u1IsValChanged = OSIX_FALSE;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    if (pRadioIfGetDB == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetTxPowerTable : Null Input received \r\n");
        return OSIX_FAILURE;
    }
    RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
        UtlShMemAllocAntennaSelectionBuf ();

    if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC, "\r%% Memory allocation for Antenna"
                     " selection failed.\r\n");
        return OSIX_FAILURE;
    }
    pWlcHdlrMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pWlcHdlrMsgStruct == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetTxPowerTable:- "
                     "UtlShMemAllocWlcBuf returned failure\n");
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                         RadioIfGetAllDB.pu1AntennaSelection);
        return OSIX_FAILURE;
    }
    MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
        pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioIfIndex = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bCurrentTxPowerLevel = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bCountryString = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bMaxTxPowerLevel = OSIX_TRUE;

    /* Get the Radio IF DB info to fill the structure  */
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                  &RadioIfGetDB) != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetTxpowerTable : RadioIf DB access failed. \r\n");
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                         RadioIfGetAllDB.pu1AntennaSelection);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;

    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bCurrentTxPowerLevel != OSIX_FALSE)
    {
        if ((pRadioIfGetDB->RadioIfGetAllDB.u2CurrentTxPowerLevel) !=
            (RadioIfGetDB.RadioIfGetAllDB.u2CurrentTxPowerLevel))
        {
            u1IsValChanged = OSIX_TRUE;
        }
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bMaxTxPowerLevel != OSIX_FALSE)
    {
        if ((pRadioIfGetDB->RadioIfGetAllDB.u2MaxTxPowerLevel) !=
            (RadioIfGetDB.RadioIfGetAllDB.u2MaxTxPowerLevel))
        {
            u1IsValChanged = OSIX_TRUE;
        }
    }

    if (u1IsValChanged == OSIX_FALSE)
    {
        RADIOIF_TRC (RADIOIF_INFO_TRC,
                     "RadioIfSetTxPowerTable : No Value Change, Not triggering "
                     "Config Update \r\n");
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                         RadioIfGetAllDB.pu1AntennaSelection);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_SUCCESS;
    }

    u2MaxTxPowerLevel = pRadioIfGetDB->RadioIfGetAllDB.u2MaxTxPowerLevel;

    if (u2MaxTxPowerLevel != 0)
    {
        if (pRadioIfGetDB->RadioIfIsGetAllDB.bCurrentTxPowerLevel != OSIX_FALSE)
        {
            u2Level = pRadioIfGetDB->RadioIfGetAllDB.u2CurrentTxPowerLevel;
            u2PowerLevel = (UINT2) (((u2MaxTxPowerLevel -
                                      (RADIOIF_POWER_MULTIPLIER * u2Level)) +
                                     RADIOIF_POWER_MULTIPLIER));
            pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfTxPower.
                u2TxPowerLevel = u2PowerLevel;
            pRadioIfGetDB->RadioIfGetAllDB.i2CurrentTxPower =
                (INT2) u2PowerLevel;

        }
        else
        {
            u2Level = RadioIfGetDB.RadioIfGetAllDB.u2CurrentTxPowerLevel;
            pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfTxPower.
                u2TxPowerLevel =
                (UINT2) ((u2MaxTxPowerLevel -
                          (RADIOIF_POWER_MULTIPLIER * u2Level)) +
                         RADIOIF_POWER_MULTIPLIER);
        }
    }

    /* nmhRoutines already validated whether the input received and old
     * values are different. Hence fill config update request structure */
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfTxPower.isPresent
        = OSIX_TRUE;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfTxPower.u2MessageType
        = RADIO_CONF_UPDATE_TX_POWER_MSG_TYPE;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfTxPower.u2MessageLength
        = RADIO_CONF_UPDATE_TX_POWER_LENGTH;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfTxPower.u1RadioId
        = RadioIfGetDB.RadioIfGetAllDB.u1RadioId;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.u2WtpInternalId
        = RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;

    i4RetVal = WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_RADIO_CONF_UPDATE_REQ,
                                       pWlcHdlrMsgStruct);
    /* Invoke WLC Handler to send the config update request */
    if (i4RetVal != OSIX_SUCCESS)
    {
        if (i4RetVal == RADIOIF_NO_AP_PRESENT)
        {
            gu1IsConfigResponseReceived = RADIOIF_NO_AP_PRESENT;
        }
        else
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfSetTxPowerTable : To change the Current Channel "
                         "of radio failed \r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            return OSIX_FAILURE;
        }
    }

    /* If the configuration is success then update the DB */

    pRadioIfGetDB->RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
    pRadioIfGetDB->RadioIfGetAllDB.u1RadioId =
        RadioIfGetDB.RadioIfGetAllDB.u1RadioId;
    pRadioIfGetDB->RadioIfIsGetAllDB.bCurrentTxPower = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bMaxTxPowerLevel = OSIX_TRUE;

    /* Update DB */
    if (WssIfProcessRadioIfDBMsg (WSS_SET_RADIO_IF_DB,
                                  pRadioIfGetDB) != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetTxPowerLevel: Radio IF DB Update failure\n");
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                         RadioIfGetAllDB.pu1AntennaSelection);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;
    }

    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                     RadioIfGetAllDB.pu1AntennaSelection);
    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfSetAntennasList                               *
 *                                                                           *
 * Description        : This function constructs Antenna related config      *
 *                      and send request to WTP                              *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to Radio IF DB with ifIndex  *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfSetAntennasList (tRadioIfGetDB * pRadioIfGetDB)
{
    tRadioIfGetDB       RadioIfGetDB;
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;
    INT4                i4RetVal = 0;
    UINT1               u1Index = 0;
    UINT1               u1AntennaCount = 0;
    UINT1               u1IsValChanged = 0;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    if (pRadioIfGetDB == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetAntennasList : Null Input received \r\n");
        return OSIX_FAILURE;
    }
    RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
        UtlShMemAllocAntennaSelectionBuf ();

    if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC, "\r%% Memory allocation for Antenna"
                     " selection failed.\r\n");
        return OSIX_FAILURE;
    }

    pWlcHdlrMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pWlcHdlrMsgStruct == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetAntennasList:- "
                     "UtlShMemAllocWlcBuf returned failure\n");
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                         RadioIfGetAllDB.pu1AntennaSelection);
        return OSIX_FAILURE;
    }
    MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bRadioIfIndex != OSIX_FALSE)
    {
        /*To get the RadioIfDb , using RadioIfIndex */
        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
            pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
        RadioIfGetDB.RadioIfIsGetAllDB.bRadioIfIndex = OSIX_TRUE;
    }
    /*To get the particular values */
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bDiversitySupport = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bAntennaMode = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bCurrentTxAntenna = OSIX_TRUE;

    /* Get the Radio IF DB info to fill the structure  */
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                  &RadioIfGetDB) != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetAntennasList : RadioIf DB access failed. \r\n");
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                         RadioIfGetAllDB.pu1AntennaSelection);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;
    }
    for (u1AntennaCount = 0; u1AntennaCount <
         RadioIfGetDB.RadioIfGetAllDB.u1CurrentTxAntenna; u1AntennaCount++)
    {
        RadioIfGetDB.RadioIfIsGetAllDB.bAntennaSelection[u1AntennaCount]
            = OSIX_TRUE;
    }
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                  &RadioIfGetDB) != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetAntennasList : RadioIf DB access failed. \r\n");
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                         RadioIfGetAllDB.pu1AntennaSelection);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;
    }
    for (u1AntennaCount = 0; u1AntennaCount <
         RadioIfGetDB.RadioIfGetAllDB.u1CurrentTxAntenna; u1AntennaCount++)
    {
        pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
            RadioIfAntenna.au1AntennaSelection[u1AntennaCount] =
            RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection[u1AntennaCount];
    }
    /* nmhRoutines already validated whether the input received and 
     * old values are different. Hence fill config update request structure */
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfAntenna.isPresent
        = OSIX_TRUE;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfAntenna.u2MessageType
        = RADIO_ANTENNA_MSG_TYPE;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfAntenna.u2MessageLength
        = (UINT2) (RadioIfGetDB.RadioIfGetAllDB.u1CurrentTxAntenna +
                   RADIO_ANTENNA_MSG_LEN_CONS);
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfAntenna.u1RadioId
        = RadioIfGetDB.RadioIfGetAllDB.u1RadioId;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.u2WtpInternalId =
        RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bDiversitySupport != OSIX_TRUE)
    {
        pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
            RadioIfAntenna.u1ReceiveDiversity =
            RadioIfGetDB.RadioIfGetAllDB.u1DiversitySupport;
    }
    else
    {
        pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
            RadioIfAntenna.u1ReceiveDiversity =
            pRadioIfGetDB->RadioIfGetAllDB.u1DiversitySupport;

        if ((pRadioIfGetDB->RadioIfGetAllDB.u1DiversitySupport) !=
            (RadioIfGetDB.RadioIfGetAllDB.u1DiversitySupport))
        {
            u1IsValChanged = OSIX_TRUE;
        }
    }
    if (pRadioIfGetDB->RadioIfIsGetAllDB.bAntennaMode != OSIX_TRUE)
    {
        pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfAntenna.u1AntennaMode
            = RadioIfGetDB.RadioIfGetAllDB.u1AntennaMode;
    }
    else
    {
        pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfAntenna.u1AntennaMode
            = pRadioIfGetDB->RadioIfGetAllDB.u1AntennaMode;

        if ((pRadioIfGetDB->RadioIfGetAllDB.u1AntennaMode) !=
            (RadioIfGetDB.RadioIfGetAllDB.u1AntennaMode))
        {
            u1IsValChanged = OSIX_TRUE;
        }
    }

    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
        RadioIfAntenna.u1CurrentTxAntenna =
        RadioIfGetDB.RadioIfGetAllDB.u1CurrentTxAntenna;

    for (u1Index = 0;
         u1Index <
         pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfAntenna.
         u1CurrentTxAntenna; u1Index++)
    {
        if (pRadioIfGetDB->RadioIfIsGetAllDB.bAntennaSelection[u1Index]
            == OSIX_TRUE)
        {
            if ((pRadioIfGetDB->RadioIfGetAllDB.pu1AntennaSelection[u1Index]
                 == CLI_ANTENNA_INTERNAL) ||
                (pRadioIfGetDB->RadioIfGetAllDB.pu1AntennaSelection[u1Index]
                 == CLI_ANTENNA_EXTERNAL))
            {
                pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
                    RadioIfAntenna.au1AntennaSelection[u1Index] =
                    pRadioIfGetDB->RadioIfGetAllDB.pu1AntennaSelection[u1Index];

                if ((pRadioIfGetDB->
                     RadioIfGetAllDB.pu1AntennaSelection[u1Index]) !=
                    (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection[u1Index]))
                {
                    u1IsValChanged = OSIX_TRUE;
                }

                break;
            }
        }
    }
    if (pRadioIfGetDB->RadioIfIsGetAllDB.bCurrentTxAntenna != OSIX_TRUE)
    {
        pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
            RadioIfAntenna.u1CurrentTxAntenna =
            RadioIfGetDB.RadioIfGetAllDB.u1CurrentTxAntenna;
    }
    else
    {
        pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
            RadioIfAntenna.u1CurrentTxAntenna =
            pRadioIfGetDB->RadioIfGetAllDB.u1CurrentTxAntenna;

        if ((pRadioIfGetDB->RadioIfGetAllDB.u1CurrentTxAntenna) !=
            (RadioIfGetDB.RadioIfGetAllDB.u1CurrentTxAntenna))
        {
            u1IsValChanged = OSIX_TRUE;
        }
    }

    if (u1IsValChanged == OSIX_FALSE)
    {
        RADIOIF_TRC (RADIOIF_INFO_TRC,
                     "RadioIfSetAntennasList: No Value Change, "
                     "Not triggering Config Update \r\n");
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                         RadioIfGetAllDB.pu1AntennaSelection);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_SUCCESS;
    }

    i4RetVal = WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_RADIO_CONF_UPDATE_REQ,
                                       pWlcHdlrMsgStruct);

    /* Invoke WLC Handler to send the config update request */
    if (i4RetVal != OSIX_SUCCESS)
    {
        if (i4RetVal == RADIOIF_NO_AP_PRESENT)
        {
            gu1IsConfigResponseReceived = RADIOIF_NO_AP_PRESENT;
        }
        else
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfSetAntennasList : Mac Operation failed \r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            return OSIX_FAILURE;
        }
    }
    /* Update DB */
    if (WssIfProcessRadioIfDBMsg (WSS_SET_RADIO_IF_DB, pRadioIfGetDB)
        != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetAntennasList: Radio IF DB Update failure\n");
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                         RadioIfGetAllDB.pu1AntennaSelection);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;
    }
    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                     RadioIfGetAllDB.pu1AntennaSelection);
    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfSetDSSSTable                                  *
 *                                                                           *
 * Description        : This function constructs Radio DSSS related config   *
 *                      and send request to WTP                              *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to Radio IF DB with ifIndex  *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfSetDSSSTable (tRadioIfGetDB * pRadioIfGetDB)
{
    tRadioIfGetDB       RadioIfGetDB;
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;
    INT4                i4RetVal = 0;
    UINT1               u1IsValChanged = 0;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    if (pRadioIfGetDB == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetDSSSTable : Null Input received \r\n");
        return OSIX_FAILURE;
    }
    RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
        UtlShMemAllocAntennaSelectionBuf ();

    if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC, "\r%% Memory allocation for Antenna"
                     " selection failed.\r\n");
        return OSIX_FAILURE;
    }

    pWlcHdlrMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pWlcHdlrMsgStruct == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetDSSSTable:- "
                     "UtlShMemAllocWlcBuf returned failure\n");
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                         RadioIfGetAllDB.pu1AntennaSelection);
        return OSIX_FAILURE;
    }
    MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
        pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioIfIndex = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bCurrentChannel = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bEDThreshold = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bCurrentCCAMode = OSIX_TRUE;

    /* Get the Radio IF DB info to fill the structure  */
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, &RadioIfGetDB)
        != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetDSSSTable : RadioIf DB access failed. \r\n");
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                         RadioIfGetAllDB.pu1AntennaSelection);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;

    }
    if (pRadioIfGetDB->RadioIfIsGetAllDB.bCurrentChannel != OSIX_FALSE)
    {

        pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
            RadioIfDSSSPhy.i1CurrentChannel =
            (INT1) pRadioIfGetDB->RadioIfGetAllDB.u1CurrentChannel;

        if ((pRadioIfGetDB->RadioIfGetAllDB.u1CurrentChannel) !=
            (RadioIfGetDB.RadioIfGetAllDB.u1CurrentChannel))
        {
            u1IsValChanged = OSIX_TRUE;
        }
    }
    else
    {
        pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
            RadioIfDSSSPhy.i1CurrentChannel =
            (INT1) RadioIfGetDB.RadioIfGetAllDB.u1CurrentChannel;

    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bEDThreshold != OSIX_FALSE)
    {
        pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfDSSSPhy.i4EDThreshold =
            pRadioIfGetDB->RadioIfGetAllDB.i4EDThreshold;

        if ((pRadioIfGetDB->RadioIfGetAllDB.i4EDThreshold) !=
            (RadioIfGetDB.RadioIfGetAllDB.i4EDThreshold))
        {
            u1IsValChanged = OSIX_TRUE;
        }
    }
    else
    {
        pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfDSSSPhy.i4EDThreshold =
            RadioIfGetDB.RadioIfGetAllDB.i4EDThreshold;
    }
    /* nmhRoutines already validated whether the input received and old
     * values are different. Hence fill config update request structure */

    if (u1IsValChanged == OSIX_FALSE)
    {
        RADIOIF_TRC (RADIOIF_INFO_TRC,
                     "RadioIfSetDSSSTable : No Value Change, Not triggering "
                     "Config Update \r\n");
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                         RadioIfGetAllDB.pu1AntennaSelection);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_SUCCESS;
    }

    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfDSSSPhy.isPresent
        = OSIX_TRUE;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfDSSSPhy.u2MessageType
        = RADIO_CONF_UPDATE_DSSS_MSG_TYPE;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfDSSSPhy.u2MessageLength
        = RADIO_CONF_UPDATE_DSSS_MSG_LENGTH;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfDSSSPhy.u1RadioId
        = RadioIfGetDB.RadioIfGetAllDB.u1RadioId;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfDSSSPhy.i1CurrentCCAMode
        = (INT1) RadioIfGetDB.RadioIfGetAllDB.u1CurrentCCAMode;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.u2WtpInternalId
        = RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;

    i4RetVal = WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_RADIO_CONF_UPDATE_REQ,
                                       pWlcHdlrMsgStruct);

    /* Invoke WLC Handler to send the config update request */
    if (i4RetVal != OSIX_SUCCESS)
    {
        if (i4RetVal == RADIOIF_NO_AP_PRESENT)
        {
            gu1IsConfigResponseReceived = RADIOIF_NO_AP_PRESENT;
        }
        else
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfSetDSSSTable : To change the Current Channel of "
                         "radio failed \r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            return OSIX_FAILURE;
        }
    }

    /* If the configuration is success then update the DB */

    pRadioIfGetDB->RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
    pRadioIfGetDB->RadioIfGetAllDB.u1RadioId =
        RadioIfGetDB.RadioIfGetAllDB.u1RadioId;
    pRadioIfGetDB->RadioIfIsGetAllDB.bCurrentCCAMode = OSIX_TRUE;

    /* Update DB */
    if (WssIfProcessRadioIfDBMsg (WSS_SET_RADIO_IF_DB,
                                  pRadioIfGetDB) != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetDSSSTable: RadioIF DB Update failure \r\n");
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                         RadioIfGetAllDB.pu1AntennaSelection);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;
    }
    if (RadioIfDeriveTxPower (pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex) !=
        OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfDeriveTxPower : updation failed \r\n");
    }

    /*During run state all the station should be disassociated, 
     * when any radio paramter is changed*/
    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                     RadioIfGetAllDB.pu1AntennaSelection);
    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfSetOFDMTable                                  *
 *                                                                           *
 * Description        : This function constructs Radio OFDM related config   *
 *                      and send request to WTP                              *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to Radio IF DB with ifIndex  *
 *                                                                           *
 * Output(s)          : NONE                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfSetOFDMTable (tRadioIfGetDB * pRadioIfGetDB)
{
    tRadioIfGetDB       RadioIfGetDB;
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;
    INT4                i4RetVal = 0;
    UINT1               u1IsValChanged = 0;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    if (pRadioIfGetDB == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetOFDMTable : Null Input received \r\n");
        return OSIX_FAILURE;
    }

    pWlcHdlrMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pWlcHdlrMsgStruct == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetOFDMTable:- "
                     "UtlShMemAllocWlcBuf returned failure\n");
        return OSIX_FAILURE;
    }
    MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
        pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioIfIndex = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bSupportedBand = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bCurrentChannel = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bT1Threshold = OSIX_TRUE;

    /* Get the Radio IF DB info to fill the structure  */
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, &RadioIfGetDB)
        != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetOFDMTable : RadioIf DB access failed. \r\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;

    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bCurrentChannel != OSIX_FALSE)
    {
        pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
            RadioIfOFDMPhy.u1CurrentChannel =
            pRadioIfGetDB->RadioIfGetAllDB.u1CurrentChannel;

        if ((pRadioIfGetDB->RadioIfGetAllDB.u1CurrentChannel) !=
            (RadioIfGetDB.RadioIfGetAllDB.u1CurrentChannel))
        {
            u1IsValChanged = OSIX_TRUE;
        }
    }
    else
    {
        pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
            RadioIfOFDMPhy.u1CurrentChannel =
            RadioIfGetDB.RadioIfGetAllDB.u1CurrentChannel;
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bT1Threshold != OSIX_FALSE)
    {
        pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfOFDMPhy.u4T1Threshold =
            pRadioIfGetDB->RadioIfGetAllDB.u4T1Threshold;

        if ((pRadioIfGetDB->RadioIfGetAllDB.u4T1Threshold) !=
            (RadioIfGetDB.RadioIfGetAllDB.u4T1Threshold))
        {
            u1IsValChanged = OSIX_TRUE;
        }
    }
    else
    {
        pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfOFDMPhy.u4T1Threshold =
            RadioIfGetDB.RadioIfGetAllDB.u4T1Threshold;
    }

    if (u1IsValChanged == OSIX_FALSE)
    {
        RADIOIF_TRC (RADIOIF_INFO_TRC,
                     "RadioIfSetOFDMTable: No Value Change, "
                     "Not triggering Config Update \r\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_SUCCESS;
    }

    /* nmhRoutines already validated whether the input received and old
     * values are different. Hence fill config update request structure */
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfOFDMPhy.isPresent
        = OSIX_TRUE;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfOFDMPhy.u2MessageType
        = RADIO_CONF_UPDATE_OFDM_MSG_TYPE;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfOFDMPhy.u2MessageLength
        = RADIO_CONF_UPDATE_OFDM_MSG_LENGTH;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfOFDMPhy.u1RadioId
        = RadioIfGetDB.RadioIfGetAllDB.u1RadioId;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfOFDMPhy.u1SupportedBand
        = RadioIfGetDB.RadioIfGetAllDB.u1SupportedBand;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.u2WtpInternalId
        = RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;

    i4RetVal = WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_RADIO_CONF_UPDATE_REQ,
                                       pWlcHdlrMsgStruct);

    /* Invoke WLC Handler to send the config update request */
    if (i4RetVal != OSIX_SUCCESS)
    {
        if (i4RetVal == RADIOIF_NO_AP_PRESENT)
        {
            gu1IsConfigResponseReceived = RADIOIF_NO_AP_PRESENT;
        }
        else
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfSetOFDMTable : To change the Current Channel of "
                         "radio failed \r\n");
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            return OSIX_FAILURE;
        }
    }

    /* If the configuration is success then update the DB */

    pRadioIfGetDB->RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
    pRadioIfGetDB->RadioIfGetAllDB.u1RadioId =
        RadioIfGetDB.RadioIfGetAllDB.u1RadioId;
    pRadioIfGetDB->RadioIfIsGetAllDB.bSupportedBand = OSIX_TRUE;

    /* Update DB */
    if (WssIfProcessRadioIfDBMsg (WSS_SET_RADIO_IF_DB,
                                  pRadioIfGetDB) != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetOFDMTable: Radio IF DB Update failure\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;
    }

    if (RadioIfDeriveTxPower (pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex) !=
        OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfDeriveTxPower : updation failed \r\n");
    }

    if (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType == RADIO_TYPE_AC)
    {
        if (RadioIfUpdateCenterFrequency
            (pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex,
             pRadioIfGetDB->RadioIfGetAllDB.u1CurrentChannel) != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfSetOFDMTable : Update Center Frequency failed\r\n");
        }
    }
    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfValidateChangeStateEvtReq                     *
 *                                                                           *
 * Description        : Validate the change state event request message      *
 *                                                                           *
 * Input(s)           : pWssMsgStruct - Pointer to input structure           *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfValidateChangeStateEvtReq (tRadioIfMsgStruct * pWssMsgStruct)
{
    /* Input Parameter Check */
    if (pWssMsgStruct == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfValidateChangeStateEvtReq: NULL input received\r\n");
        return OSIX_FAILURE;
    }

    /*To get IfIndex value from the input structure */
    if (pWssMsgStruct->unRadioIfMsg.
        RadioIfChangeStateEventReq.RadioIfOperStatus.isPresent == OSIX_FALSE)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfValidateChangeStateEvtReq: Mandatory argument missing,"
                     " Validation failed \r\n");
        return OSIX_FAILURE;
    }

    if (pWssMsgStruct->unRadioIfMsg.
        RadioIfChangeStateEventReq.RadioIfOperStatus.u2MessageType !=
        RADIO_OPER_STATE_MSG_TYPE)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfValidateChangeStateEvtReq: Invalid message type for "
                     "oper status, Validation failed \r\n");
        return OSIX_FAILURE;
    }
    if (pWssMsgStruct->unRadioIfMsg.
        RadioIfChangeStateEventReq.RadioIfOperStatus.u2MessageLength !=
        RADIO_OPER_STATE_MSG_LENGTH)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfValidateChangeStateEvtReq: Invalid message length for "
                     "oper status, Validation failed \r\n");
        return OSIX_FAILURE;
    }
    if ((pWssMsgStruct->unRadioIfMsg.
         RadioIfChangeStateEventReq.RadioIfOperStatus.u1RadioId >
         MAX_NUM_OF_RADIO_PER_AP)
        || (pWssMsgStruct->unRadioIfMsg.
            RadioIfChangeStateEventReq.RadioIfOperStatus.u1RadioId == 0))
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfValidateChangeStateEvtReq: Invalid Radio Id received, "
                     "Validation failed \r\n");
        return OSIX_FAILURE;
    }
    if ((pWssMsgStruct->unRadioIfMsg.
         RadioIfChangeStateEventReq.RadioIfOperStatus.u1OperStatus != CFA_IF_UP)
        && (pWssMsgStruct->unRadioIfMsg.
            RadioIfChangeStateEventReq.RadioIfOperStatus.u1OperStatus !=
            CFA_IF_DOWN))
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfValidateChangeStateEvtReq: Invalid value for oper "
                     "status, Validation failed \r\n");
        return OSIX_FAILURE;
    }
    if (pWssMsgStruct->unRadioIfMsg.
        RadioIfChangeStateEventReq.RadioIfOperStatus.u1FailureCause >
        RADIO_MAX_FAILURE_CAUSE)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfValidateChangeStateEvtReq: Invalid value for reason "
                     "of failure, Validation failed \r\n");
        return OSIX_FAILURE;
    }

    if (pWssMsgStruct->unRadioIfMsg.RadioIfChangeStateEventReq.RadioIfFailAlarm.
        isPresent == OSIX_TRUE)
    {
        if (pWssMsgStruct->unRadioIfMsg.
            RadioIfChangeStateEventReq.RadioIfFailAlarm.u2MessageType !=
            RADIO_FAIL_ALARM_MSG_TYPE)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfValidateChangeStateEvtReq: Invalid message type for"
                         " radio failure, Validation failed \r\n");
            return OSIX_FAILURE;
        }
        if (pWssMsgStruct->unRadioIfMsg.
            RadioIfChangeStateEventReq.RadioIfFailAlarm.u2MessageLength !=
            RADIO_FAIL_ALARM_MSG_LENGTH)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfValidateChangeStateEvtReq: Invalid message length "
                         " for radio failure, Validation failed \r\n");
            return OSIX_FAILURE;
        }
        if ((pWssMsgStruct->unRadioIfMsg.
             RadioIfChangeStateEventReq.RadioIfFailAlarm.u1RadioId >
             MAX_NUM_OF_RADIO_PER_AP)
            || (pWssMsgStruct->unRadioIfMsg.
                RadioIfChangeStateEventReq.RadioIfFailAlarm.u1RadioId == 0))
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfValidateChangeStateEvtReq: Invalid Radio Id "
                         "received, Validation failed \r\n");
            return OSIX_FAILURE;
        }
        if ((pWssMsgStruct->unRadioIfMsg.
             RadioIfChangeStateEventReq.RadioIfFailAlarm.u1Type !=
             RADIO_TRANSMIT_TYPE)
            && (pWssMsgStruct->unRadioIfMsg.
                RadioIfChangeStateEventReq.RadioIfFailAlarm.u1Type !=
                RADIO_RECEIVE_TYPE))
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfValidateChangeStateEvtReq: Invalid failure type "
                         "for radio, Validation failed \r\n");
            return OSIX_FAILURE;
        }
        if ((pWssMsgStruct->unRadioIfMsg.
             RadioIfChangeStateEventReq.RadioIfFailAlarm.u1Status !=
             RADIO_FAILURE_OCCURED)
            && (pWssMsgStruct->unRadioIfMsg.
                RadioIfChangeStateEventReq.RadioIfFailAlarm.u1Status !=
                RADIO_FAILURE_CLEARED))
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfValidateChangeStateEvtReq : Invalid failure status "
                         "recevied, Validation failed \r\n");
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfSetDot11RadioType                             *
 *                                                                           *
 * Description        : Function to get the configured Radio Type from the   *
 *                      Radio DB                                             *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to Radio IF DB with ifIndex  *
 *                                                                           *
 * Output(s)          : pRadioIfGetDB                                        *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfSetDot11RadioType (tRadioIfGetDB * pRadioIfGetDB)
{
    tRadioIfGetDB       RadioIfGetDB;
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tRemoteSessionManager *pSessEntry = NULL;
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        pWlcHdlrMsgStruct =
        (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pWlcHdlrMsgStruct == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetDot11RadioType:- "
                     "UtlShMemAllocWlcBuf returned failure\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));

    /* Input Parameter Check */
    if (pRadioIfGetDB == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetDot11RadioType : NULL input received\r\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
        UtlShMemAllocAntennaSelectionBuf ();

    if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC, "\r%% Memory allocation for Antenna"
                     " selection failed.\r\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
        pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;

    /* Get the Radio IF DB info to fill the structure  */
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                  &RadioIfGetDB) != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetDot11RadioType : RadioIf DB access failed. \r\n");
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                         RadioIfGetAllDB.pu1AntennaSelection);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    if (pRadioIfGetDB->RadioIfGetAllDB.u4Dot11RadioType ==
        RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType)
    {
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                         RadioIfGetAllDB.pu1AntennaSelection);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_SUCCESS;
    }

    /* Get the CAPWAP current state. If the state machine is in run state then
     * do not allow to change the Radio Type */
    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
        RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;

    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_RSM_FROM_INDEX,
                                 pWssIfCapwapDB) == OSIX_SUCCESS)
    {
        pSessEntry = pWssIfCapwapDB->pSessEntry;
        if ((pSessEntry != NULL) && (pSessEntry->eCurrentState == CAPWAP_RUN))
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfSetDot11RadioType: AP in run state. "
                         "Cannot change radio type.\r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
    }
    /* Update DB */
    if (WssIfProcessRadioIfDBMsg (WSS_SET_RADIO_IF_DB, pRadioIfGetDB)
        != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetDot11RadioType : Radio IF DB Update failure \r\n");
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                         RadioIfGetAllDB.pu1AntennaSelection);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                     RadioIfGetAllDB.pu1AntennaSelection);
    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfGetTxPowerTable                               *
 *                                                                           *
 * Description        : Function to get the Radio Qos related info from the  *
 *                      Radio DB                                             *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to Radio IF DB with ifIndex  *
 *                                                                           *
 * Output(s)          : pRadioIfGetDB - TxPower filled with Radio DB         *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfGetTxPowerTable (tRadioIfGetDB * pRadioIfGetDB)
{
    if (pRadioIfGetDB == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfGetTxPowerTable : Null Input received \r\n");
        return OSIX_FAILURE;
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bRadioIfIndex != OSIX_FALSE)
    {
        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                      pRadioIfGetDB) != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfGetTxPowerLevel : Radio IF DB Access failure \r\n");
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfGetEDCAParams                                 *
 *                                                                           *
 * Description        : Function to get the Radio Qos related info from the  *
 *                      Radio DB                                             *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to Radio IF DB with ifIndex  *
 *                                                                           *
 * Output(s)          : pRadioIfGetDB - QoS filled with Radio DB             *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfGetEDCAParams (tRadioIfGetDB * pRadioIfGetDB)
{
    if (pRadioIfGetDB == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfGetTxPowerTable : Null Input received \r\n");
        return OSIX_FAILURE;
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bRadioIfIndex != OSIX_FALSE)
    {
        pRadioIfGetDB->RadioIfIsGetAllDB.bTaggingPolicy = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bQosIndex = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bCwMin = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bCwMax = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bAifsn = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bTxOpLimit = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bQosPrio = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bDscp = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bAdmissionControl = OSIX_TRUE;

        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_QOS_CONFIG_DB,
                                      pRadioIfGetDB) != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfGetTxPowerLevel : Radio IF DB Access failure \r\n");
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfGetAllParams                                  *
 *                                                                           *
 * Description        : Function to get the Radio related info from the      *
 *                      Radio DB                                             *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to Radio IF DB with ifIndex  *
 *                                                                           *
 * Output(s)          : pRadioIfGetDB - Channel filled with Radio DB         *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfGetAllParams (tRadioIfGetDB * pRadioIfGetDB)
{
    if (pRadioIfGetDB == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfGetTxPowerTable : Null Input received \r\n");
        return OSIX_FAILURE;
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bRadioIfIndex != OSIX_FALSE)
    {
        pRadioIfGetDB->RadioIfIsGetAllDB.bDTIMPeriod = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bBeaconPeriod = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bCountryString = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bRTSThreshold = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bShortRetry = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bLongRetry = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bFragmentationThreshold = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bTxMsduLifetime = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bRxMsduLifetime = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bShortPreamble = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bDiversitySupport = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bBssIdCount = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bCurrentTxPowerLevel = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bRadioAntennaType = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bAntennaMode = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bCurrentTxAntenna = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bOperationalRate = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bSupportedBand = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bT1Threshold = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bCurrentFrequency = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bCurrentChannel = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bCurrentTxPower = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bNoOfSupportedPowerLevels = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bTaggingPolicy = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bQosIndex = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bQueueDepth = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bCwMin = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bCwMax = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bAifsn = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bTxOpLimit = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bQosPrio = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bDscp = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bDot11RadioType = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bChannelWidth = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bShortGi20 = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bShortGi40 = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bMCSRateSet = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bHtFlag = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bMaxSuppMCS = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bMaxManMCS = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bTxAntenna = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bRxAntenna = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bMultiDomainInfo = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bRxMcsBitmask = OSIX_TRUE;

        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, pRadioIfGetDB)
            != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfGetTxPowerLevel : Radio IF DB Access failure \r\n");
            return OSIX_FAILURE;
        }
        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, pRadioIfGetDB)
            != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfGetTxPowerLevel : Radio IF DB Access failure \r\n");
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfGetOFDMTable                                  *
 *                                                                           *
 * Description        : Function to get the Radio A channel info from the    *
 *                      Radio DB                                             *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to Radio IF DB with ifIndex  *
 *                                                                           *
 * Output(s)          : pRadioIfGetDB - Channel filled with Radio DB         *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfGetOFDMTable (tRadioIfGetDB * pRadioIfGetDB)
{
    if (pRadioIfGetDB == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfGetOFDMTable : Null Input received \r\n");
        return OSIX_FAILURE;
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bRadioIfIndex != OSIX_FALSE)
    {
        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, pRadioIfGetDB)
            != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfGetOFDMLevel : Radio IF DB Access failure \r\n");
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfGetDSSSTable                                  *
 *                                                                           *
 * Description        : Function to get the Radio B channel info from the    *
 *                      Radio DB                                             *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to Radio IF DB with ifIndex  *
 *                                                                           *
 * Output(s)          : pRadioIfGetDB - Channel filled with Radio DB         *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfGetDSSSTable (tRadioIfGetDB * pRadioIfGetDB)
{
    if (pRadioIfGetDB == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfGetDSSSTable : Null Input received \r\n");
        return OSIX_FAILURE;
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bRadioIfIndex != OSIX_FALSE)
    {
        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, pRadioIfGetDB)
            != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfGetDSSS : Radio IF DB Access failure \r\n");
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfGetWtpInternalId                              *
 *                                                                           *
 * Description        : Function to get the WTP Internal Id from radio ifidx *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to Radio IF DB with ifIndex  *
 *                                                                           *
 * Output(s)          : pRadioIfGetDB - Channel filled with Radio DB         *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfGetWtpInternalId (UINT4 u4RadioIfIndex,
                         UINT2 *pu2WtpInternalId, UINT4 *pu4RadioId)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
        UtlShMemAllocAntennaSelectionBuf ();

    if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC, "\r%% Memory allocation for Antenna"
                     " selection failed.\r\n");
        return OSIX_FAILURE;
    }

    RadioIfGetDB.RadioIfIsGetAllDB.bRadioIfIndex = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = u4RadioIfIndex;

    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, &RadioIfGetDB)
        != OSIX_SUCCESS)
    {
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                         RadioIfGetAllDB.pu1AntennaSelection);
        return OSIX_FAILURE;
    }

    *pu2WtpInternalId = RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;
    *pu4RadioId = RadioIfGetDB.RadioIfGetAllDB.u1RadioId;

    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                     RadioIfGetAllDB.pu1AntennaSelection);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfConstructAdminStatus                          *
 *                                                                           *
 * Description        : To construct the admin status message to send to WTP *
 *                                                                           *
 * Input(s)           : u4IfIndex - Virtual Radio Interface Index            *
 *                      u1AdminStatus - Enable/Disable Status                *
 *                                                                           *
 * Output(s)          : pWssMsgStruct - Pointer to output structure          *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *****************************************************************************/
UINT1
RadioIfConstructAdminStatus (UINT4 u4IfIndex, UINT1 u1AdminStatus,
                             tRadioIfMsgStruct * pWssMsgStruct)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    /* Validate the input */
    if (pWssMsgStruct == NULL)
    {
        return OSIX_FAILURE;
    }
    RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
        UtlShMemAllocAntennaSelectionBuf ();

    if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC, "\r%% Memory allocation for Antenna"
                     " selection failed.\r\n");
        return OSIX_FAILURE;
    }
    /*To get Radio Id */
    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = u4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;

    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                  &RadioIfGetDB) != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "WssIfProcessRadioIfConstructMsg : Access to DB failed\n");
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                         RadioIfGetAllDB.pu1AntennaSelection);
        return OSIX_FAILURE;
    }

    /* Constructing the message to be sent to HDLR */
    pWssMsgStruct->unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfAdminStatus.
        isPresent = OSIX_TRUE;

    pWssMsgStruct->unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfAdminStatus.
        u2MessageType = RADIOIF_CONFIG_UPDATE_REQ_MSG_TYPE;

    pWssMsgStruct->unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfAdminStatus.
        u2MessageLength = RADIOIF_CONFIG_UPDATE_REQ_MSG_LEN;

    pWssMsgStruct->unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfAdminStatus.
        u1RadioId = RadioIfGetDB.RadioIfGetAllDB.u1RadioId;

    pWssMsgStruct->unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfAdminStatus.
        u1AdminStatus = u1AdminStatus;

    pWssMsgStruct->unRadioIfMsg.RadioIfConfigUpdateReq.u2WtpInternalId =
        RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;
    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                     RadioIfGetAllDB.pu1AntennaSelection);
    return OSIX_SUCCESS;
}

UINT1
RadioIfDecryptReport (tRadioIfMsgStruct * pWssMsgStruct)
{
    tRadioIfGetDB       RadioIfGetDB;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    /* Input Parameter Check */
    if (pWssMsgStruct == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfConfigUpdateReq: Null input received\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
        UtlShMemAllocAntennaSelectionBuf ();

    if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC, "\r%% Memory allocation for Antenna"
                     " selection failed.\r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;
    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
        pWssMsgStruct->unRadioIfMsg.DecryptReportTimer.u2WtpInternalId;
    pWssIfCapwapDB->CapwapGetDB.u1RadioId =
        pWssMsgStruct->unRadioIfMsg.DecryptReportTimer.u1RadioId;

    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
        != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfDecryptReport : Getting Radio index failed\n");
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                         RadioIfGetAllDB.pu1AntennaSelection);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
        pWssIfCapwapDB->CapwapGetDB.u4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bReportInterval = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.u2ReportInterval =
        pWssMsgStruct->unRadioIfMsg.DecryptReportTimer.u2ReportInterval;

    if (WssIfProcessRadioIfDBMsg (WSS_SET_RADIO_IF_DB, (&RadioIfGetDB))
        != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfDecryptReport: DB get of RadioIf DB failed \n");
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                         RadioIfGetAllDB.pu1AntennaSelection);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                     RadioIfGetAllDB.pu1AntennaSelection);
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function     : RadioIfUpdateChannel                                       */
/*                                                                           */
/* Description  : This functions fills the radio if DB and invoke the func   */
/*                to send message to WTP for Channel                         */
/*                                                                           */
/* Input        : pWssMsgStruct - Pointer to the input structure             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS, is processing succeeds                       */
/*                OSIX_FAILURE, otherwise                                    */
/*****************************************************************************/
UINT1
RadioIfUpdateChannel (tRadioIfMsgStruct * pWssMsgStruct)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    /* Copy the radio information to the DB strcture and call the 
     * corresponding function to send the message to WTP */
    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
        pWssMsgStruct->unRadioIfMsg.RadioIfDcaTpcUpdate.u4IfIndex;

    /*Get the radio type with ifindex from radio DB */

    RadioIfGetDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, &RadioIfGetDB) !=
        OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC, "\rRadio Type fetch failed");
        return OSIX_FAILURE;
    }

    RadioIfGetDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_FALSE;
    RadioIfGetDB.RadioIfIsGetAllDB.bCurrentChannel = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.u1CurrentChannel =
        (UINT1) pWssMsgStruct->unRadioIfMsg.
        RadioIfDcaTpcUpdate.u2CurrentChannel;
    if ((RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType == RADIO_TYPE_A)
        || (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType == RADIO_TYPE_AN)
        || (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType == RADIO_TYPE_AC))
    {
        if (RadioIfSetOFDMTable (&RadioIfGetDB) != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfUpdateChannel : updation failed \r\n");
            return OSIX_FAILURE;
        }
    }
    else
    {
        if (RadioIfSetDSSSTable (&RadioIfGetDB) != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfUpdateChannel : updation failed \r\n");
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function     : RadioIfUpdatePower                                         */
/*                                                                           */
/* Description  : This functions fills the radio if DB and invoke the func   */
/*                to send message to WTP for Tx Power                        */
/*                                                                           */
/* Input        : pWssMsgStruct - Pointer to the input structure             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS, is processing succeeds                       */
/*                OSIX_FAILURE, otherwise                                    */
/*****************************************************************************/
UINT1
RadioIfUpdatePower (tRadioIfMsgStruct * pWssMsgStruct)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
        pWssMsgStruct->unRadioIfMsg.RadioIfDcaTpcUpdate.u4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bMaxTxPowerLevel = OSIX_TRUE;

    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                  &RadioIfGetDB) != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfUpdatePower: RadioIf DB access failed. \r\n");
        return OSIX_FAILURE;
    }

    RadioIfGetDB.RadioIfIsGetAllDB.bCurrentTxPowerLevel = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.u2CurrentTxPowerLevel =
        pWssMsgStruct->unRadioIfMsg.RadioIfDcaTpcUpdate.u2TxPowerLevel;

    if (RadioIfSetTxPowerTable (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfUpdatePower : updation failed \r\n");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfProcess11nStatusReq                           *
 *                                                                           *
 * Description        : This functions processes 11N  Config Status Request  *
 *                      params from WTP                                      *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to access the radio          *
 *                      DB elements                                          * 
 *                        pWssMsgStruct - Pointer to the input structure     *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfProcess11nStatusReq (tRadioIfGetDB * pRadioIfGetDB,
                            tRadioIfMsgStruct * pWssMsgStruct)
{
    tRadioIfGetDB       RadioIfGetDB;
    UINT1               u1MsgFlag = OSIX_FALSE;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    /*VHT Capability */
    switch (pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.RadioIfInfoElem.
            u1EleId)
    {
        case DOT11_INFO_ELEM_HTCAPABILITY:

            /***************************************************************
             * Validation for HT Capabilities Element : HT Capabilities Info 
             **************************************************************/

            RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u2SuppHtCapInfo =
                pWssMsgStruct->unRadioIfMsg.
                RadioIfConfigStatusReq.RadioIfInfoElem.unInfoElem.HTCapability.
                u2HTCapInfo;

            if (WssIfProcessRadioIfDBMsg (WSS_PARSE_11N_CAPABILITY_INFO,
                                          (&RadioIfGetDB)) != OSIX_SUCCESS)
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                             "RadioIfProcess11nStatusReq: Parsing"
                             "u2HtCapInfo to individual elements failed\n");
                return OSIX_FAILURE;
            }

            /* HT Capabilities bool varibales */
            pRadioIfGetDB->RadioIfIsGetAllDB.bLdpcCoding = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bChannelWidth = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bSmPowerSave = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bHtGreenField = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bShortGi20 = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bShortGi40 = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bTxStbc = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bRxStbc = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bDelayedBlockack = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bMaxAmsduLen = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bDsssCck40 = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bLsigTxopProtFulSup = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bFortyInto = OSIX_TRUE;

            pRadioIfGetDB->RadioIfIsGetAllDB.bMaxAmpduLen = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bMinAmpduStart = OSIX_TRUE;

            pRadioIfGetDB->RadioIfIsGetAllDB.bRxMcsBitmask = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bHighSuppDataRate = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bTxMcsSetDefined = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bTxRxMcsSetNotEqual = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bTxMaxNoSpatStrm = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bTxUnequalModSupp = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bPco = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bPcoTransTime = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bMcsFeedback = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bHtcSupp = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bRdResponder = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bSuppHtExtCap = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bSuppTxBeamCapParam = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bSuppPco = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bSuppPcoTransTime = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bSuppMcsFeedback = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bSuppHtcSupp = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bSuppRdResponder = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bSuppImpTranBeamRecCapable =
                OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bSuppRecStagSoundCapable =
                OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bSuppTransStagSoundCapable =
                OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bSuppRecNdpCapable = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bSuppTransNdpCapable = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bSuppImpTranBeamCapable =
                OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bSuppCalibration = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bSuppExplCsiTranBeamCapable =
                OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bSuppExplNoncompSteCapable =
                OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bSuppExplCompSteCapable =
                OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bSuppExplTranBeamCsiFeedback =
                OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bSuppExplNoncompBeamFbCapable =
                OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bSuppExplCompBeamFbCapable =
                OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bSuppMinGrouping = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bSuppCsiNoofBeamAntSupported =
                OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bSuppNoncompSteNoBeamformer =
                OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bSuppCompSteNoBeamformer =
                OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bSuppCSIMaxNoRowsBeamformer =
                OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bSuppChannelEstCapability =
                OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bHtExtCap = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bTxBeamCapParam = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bImpTranBeamRecCapable = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bRecStagSoundCapable = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bTransStagSoundCapable = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bRecNdpCapable = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bTransNdpCapable = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bImpTranBeamCapable = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bCalibration = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bExplCsiTranBeamCapable =
                OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bExplNoncompSteCapable = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bExplCompSteCapable = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bExplTranBeamCsiFeedback =
                OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bExplNoncompBeamFbCapable =
                OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bExplCompBeamFbCapable = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bMinGrouping = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bCsiNoofBeamAntSupported =
                OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bNoncompSteNoBeamformer =
                OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bCompSteNoBeamformer = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bCSIMaxNoRowsBeamformer =
                OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bChannelEstCapability = OSIX_TRUE;

            if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N,
                                          (pRadioIfGetDB)) != OSIX_SUCCESS)
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                             "RadioIfProcess11nStatusReq:"
                             " Get of Radio DB for 11N failed \n");
                return OSIX_FAILURE;
            }
            /*
             * Updation for Driver HT Capability value into WLC RadioDB
             */
            pRadioIfGetDB->RadioIfIsGetAllDB.bSuppHtCapInfo = OSIX_TRUE;
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u2SuppHtCapInfo =
                RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u2SuppHtCapInfo;

            pRadioIfGetDB->RadioIfIsGetAllDB.bSuppLdpcCoding = OSIX_TRUE;
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u1SuppLdpcCoding =
                RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1SuppLdpcCoding;

            if (pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u1LdpcCoding
                <=
                RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1SuppLdpcCoding)
            {
                RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1SuppLdpcCoding =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.
                    u1LdpcCoding;
                pRadioIfGetDB->RadioIfIsGetAllDB.bLdpcCoding = OSIX_FALSE;
                u1MsgFlag = OSIX_TRUE;
            }
            else
            {
                /*Only Db updation */
                pRadioIfGetDB->RadioIfIsGetAllDB.bLdpcCoding = OSIX_TRUE;
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u1LdpcCoding =
                    RadioIfGetDB.RadioIfGetAllDB.
                    Dot11NCapaParams.u1SuppLdpcCoding;
            }

            pRadioIfGetDB->RadioIfIsGetAllDB.bSuppChannelWidth = OSIX_TRUE;
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u1SuppChannelWidth =
                RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.
                u1SuppChannelWidth;

            if (RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.
                u1SuppChannelWidth == 0)
            {
                if (pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.
                    u1ChannelWidth <=
                    RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.
                    u1SuppChannelWidth)
                {
                    RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.
                        u1SuppChannelWidth =
                        pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.
                        u1ChannelWidth;
                    pRadioIfGetDB->RadioIfIsGetAllDB.bChannelWidth = OSIX_FALSE;
                    u1MsgFlag = OSIX_TRUE;
                }
                else
                {
                    /*Only Db updation */
                    pRadioIfGetDB->RadioIfIsGetAllDB.bChannelWidth = OSIX_TRUE;
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.
                        u1ChannelWidth =
                        RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.
                        u1SuppChannelWidth;
                }
            }
            else
            {
                if (pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.
                    u1ChannelWidth <=
                    RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.
                    u1SuppChannelWidth)
                {
                    RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.
                        u1SuppChannelWidth =
                        pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.
                        u1ChannelWidth;
                    pRadioIfGetDB->RadioIfIsGetAllDB.bChannelWidth = OSIX_FALSE;
                    u1MsgFlag = OSIX_TRUE;
                }

            }

            pRadioIfGetDB->RadioIfIsGetAllDB.bSuppSmPowerSave = OSIX_TRUE;
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u1SuppSmPowerSave =
                RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1SuppSmPowerSave;

            if (pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u1SmPowerSave
                <=
                RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1SuppSmPowerSave)
            {
                RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.
                    u1SuppSmPowerSave =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.
                    u1SmPowerSave;
                pRadioIfGetDB->RadioIfIsGetAllDB.bSmPowerSave = OSIX_FALSE;
                u1MsgFlag = OSIX_TRUE;
            }
            else
            {
                /*Only Db updation */
                pRadioIfGetDB->RadioIfIsGetAllDB.bSmPowerSave = OSIX_TRUE;
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u1SmPowerSave =
                    RadioIfGetDB.RadioIfGetAllDB.
                    Dot11NCapaParams.u1SuppSmPowerSave;
            }

            pRadioIfGetDB->RadioIfIsGetAllDB.bSuppHtGreenField = OSIX_TRUE;
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u1SuppHtGreenField =
                RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.
                u1SuppHtGreenField;

            if (pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u1HtGreenField
                <=
                RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.
                u1SuppHtGreenField)
            {
                RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.
                    u1SuppHtGreenField =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.
                    u1HtGreenField;
                pRadioIfGetDB->RadioIfIsGetAllDB.bHtGreenField = OSIX_FALSE;
                u1MsgFlag = OSIX_TRUE;
            }
            else
            {
                /*Only Db updation */
                pRadioIfGetDB->RadioIfIsGetAllDB.bHtGreenField = OSIX_TRUE;
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u1HtGreenField =
                    RadioIfGetDB.RadioIfGetAllDB.
                    Dot11NCapaParams.u1SuppHtGreenField;
            }

            pRadioIfGetDB->RadioIfIsGetAllDB.bSuppShortGi20 = OSIX_TRUE;
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u1SuppShortGi20 =
                RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1SuppShortGi20;

            if (pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u1ShortGi20
                <=
                RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1SuppShortGi20)
            {
                RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1SuppShortGi20 =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u1ShortGi20;
                pRadioIfGetDB->RadioIfIsGetAllDB.bShortGi20 = OSIX_FALSE;
                u1MsgFlag = OSIX_TRUE;
            }
            else
            {
                /*Only Db updation */
                pRadioIfGetDB->RadioIfIsGetAllDB.bShortGi20 = OSIX_TRUE;
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u1ShortGi20 =
                    RadioIfGetDB.RadioIfGetAllDB.
                    Dot11NCapaParams.u1SuppShortGi20;
            }

            pRadioIfGetDB->RadioIfIsGetAllDB.bSuppShortGi40 = OSIX_TRUE;
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u1SuppShortGi40 =
                RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1SuppShortGi40;

            if (pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u1ShortGi40
                <=
                RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1SuppShortGi40)
            {
                RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1SuppShortGi40 =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u1ShortGi40;
                pRadioIfGetDB->RadioIfIsGetAllDB.bShortGi40 = OSIX_FALSE;
                u1MsgFlag = OSIX_TRUE;
            }
            else
            {
                /*Only Db updation */
                pRadioIfGetDB->RadioIfIsGetAllDB.bShortGi40 = OSIX_TRUE;
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u1ShortGi40 =
                    RadioIfGetDB.RadioIfGetAllDB.
                    Dot11NCapaParams.u1SuppShortGi40;
            }

            pRadioIfGetDB->RadioIfIsGetAllDB.bSuppTxStbc = OSIX_TRUE;
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u1SuppTxStbc =
                RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1SuppTxStbc;

            if (pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u1TxStbc
                <= RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1SuppTxStbc)
            {
                RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1SuppTxStbc =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u1TxStbc;
                pRadioIfGetDB->RadioIfIsGetAllDB.bTxStbc = OSIX_FALSE;
                u1MsgFlag = OSIX_TRUE;
            }
            else
            {
                /*Only Db updation */
                pRadioIfGetDB->RadioIfIsGetAllDB.bTxStbc = OSIX_TRUE;
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u1TxStbc =
                    RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1SuppTxStbc;
            }
            /* Need ito update Rxstbc */

            pRadioIfGetDB->RadioIfIsGetAllDB.bSuppRxStbc = OSIX_TRUE;
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u1SuppRxStbc =
                RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1SuppRxStbc;

            if (pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u1RxStbc
                <= RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1SuppRxStbc)
            {
                RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1SuppRxStbc =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u1RxStbc;
                pRadioIfGetDB->RadioIfIsGetAllDB.bRxStbc = OSIX_FALSE;
                u1MsgFlag = OSIX_TRUE;
            }
            else
            {
                /*Only Db updation */
                pRadioIfGetDB->RadioIfIsGetAllDB.bRxStbc = OSIX_TRUE;
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u1RxStbc =
                    RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1SuppRxStbc;
            }

            pRadioIfGetDB->RadioIfIsGetAllDB.bSuppDelayedBlockack = OSIX_TRUE;
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.
                u1SuppDelayedBlockack =
                RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.
                u1SuppDelayedBlockack;

            if (pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.
                u1DelayedBlockack <=
                RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.
                u1SuppDelayedBlockack)
            {
                RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.
                    u1SuppDelayedBlockack =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.
                    u1DelayedBlockack;
                pRadioIfGetDB->RadioIfIsGetAllDB.bDelayedBlockack = OSIX_FALSE;
                u1MsgFlag = OSIX_TRUE;
            }
            else
            {
                /*Only Db updation */
                pRadioIfGetDB->RadioIfIsGetAllDB.bDelayedBlockack = OSIX_TRUE;
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.
                    u1DelayedBlockack =
                    RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.
                    u1SuppDelayedBlockack;
            }

            pRadioIfGetDB->RadioIfIsGetAllDB.bSuppMaxAmsduLen = OSIX_TRUE;
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u1SuppMaxAmsduLen =
                RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1SuppMaxAmsduLen;

            if (pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u1MaxAmsduLen
                <=
                RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1SuppMaxAmsduLen)
            {
                RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.
                    u1SuppMaxAmsduLen =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.
                    u1MaxAmsduLen;
                pRadioIfGetDB->RadioIfIsGetAllDB.bMaxAmsduLen = OSIX_FALSE;
                u1MsgFlag = OSIX_TRUE;
            }
            else
            {
                /*Only Db updation */
                pRadioIfGetDB->RadioIfIsGetAllDB.bMaxAmsduLen = OSIX_TRUE;
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u1MaxAmsduLen =
                    RadioIfGetDB.RadioIfGetAllDB.
                    Dot11NCapaParams.u1SuppMaxAmsduLen;
            }

            pRadioIfGetDB->RadioIfIsGetAllDB.bSuppDsssCck40 = OSIX_TRUE;
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u1SuppDsssCck40 =
                RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1SuppDsssCck40;

            if (pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u1DsssCck40
                <=
                RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1SuppDsssCck40)
            {
                RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1SuppDsssCck40 =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u1DsssCck40;
                pRadioIfGetDB->RadioIfIsGetAllDB.bDsssCck40 = OSIX_FALSE;
                u1MsgFlag = OSIX_TRUE;
            }
            else
            {
                /*Only Db updation */
                pRadioIfGetDB->RadioIfIsGetAllDB.bDsssCck40 = OSIX_TRUE;
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u1DsssCck40 =
                    RadioIfGetDB.RadioIfGetAllDB.
                    Dot11NCapaParams.u1SuppDsssCck40;
            }

            pRadioIfGetDB->RadioIfIsGetAllDB.bSuppFortyInto = OSIX_TRUE;
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u1SuppFortyInto =
                RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1SuppFortyInto;

            if (pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u1FortyInto
                <=
                RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1SuppFortyInto)
            {
                RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1SuppFortyInto =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u1FortyInto;
                pRadioIfGetDB->RadioIfIsGetAllDB.bFortyInto = OSIX_FALSE;
                u1MsgFlag = OSIX_TRUE;
            }
            else
            {
                /*Only Db updation */
                pRadioIfGetDB->RadioIfIsGetAllDB.bFortyInto = OSIX_TRUE;
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u1FortyInto =
                    RadioIfGetDB.RadioIfGetAllDB.
                    Dot11NCapaParams.u1SuppFortyInto;
            }

            pRadioIfGetDB->RadioIfIsGetAllDB.bSuppLsigTxopProtFulSup =
                OSIX_TRUE;
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.
                u1SuppLsigTxopProtFulSup =
                RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.
                u1SuppLsigTxopProtFulSup;

            if (pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.
                u1LsigTxopProtFulSup <=
                RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.
                u1SuppLsigTxopProtFulSup)
            {
                RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.
                    u1SuppLsigTxopProtFulSup =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.
                    u1LsigTxopProtFulSup;
                pRadioIfGetDB->RadioIfIsGetAllDB.bLsigTxopProtFulSup =
                    OSIX_FALSE;
                u1MsgFlag = OSIX_TRUE;
            }
            else
            {
                /*Only Db updation */
                pRadioIfGetDB->RadioIfIsGetAllDB.bLsigTxopProtFulSup =
                    OSIX_TRUE;
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.
                    u1LsigTxopProtFulSup =
                    RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.
                    u1SuppLsigTxopProtFulSup;
            }

            /**************************************************************
             * Validation for HT Capabilities Element : A - MPDU Parameters 
             **************************************************************/

            RadioIfGetDB.RadioIfGetAllDB.Dot11NampduParams.u1SuppAmpduParam =
                pWssMsgStruct->unRadioIfMsg.
                RadioIfConfigStatusReq.RadioIfInfoElem.unInfoElem.HTCapability.
                u1AMPDUParam;

            if (WssIfProcessRadioIfDBMsg
                (WSS_PARSE_11N_AMPDU_INFO, (&RadioIfGetDB)) != OSIX_SUCCESS)
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                             "RadioIfProcess11nStatusReq: Parsing"
                             "u1SuppAmpduParam to individual elements failed\n");
                return OSIX_FAILURE;
            }
            /* HT Capabilities bool varibales */

            pRadioIfGetDB->RadioIfIsGetAllDB.bSuppMaxAmpduLen = OSIX_TRUE;
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NampduParams.u1SuppMaxAmpduLen =
                RadioIfGetDB.RadioIfGetAllDB.Dot11NampduParams.
                u1SuppMaxAmpduLen;

            if (pRadioIfGetDB->RadioIfGetAllDB.Dot11NampduParams.u1MaxAmpduLen
                <=
                RadioIfGetDB.RadioIfGetAllDB.Dot11NampduParams.
                u1SuppMaxAmpduLen)
            {
                RadioIfGetDB.RadioIfGetAllDB.Dot11NampduParams.
                    u1SuppMaxAmpduLen =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NampduParams.
                    u1MaxAmpduLen;
                pRadioIfGetDB->RadioIfIsGetAllDB.bMaxAmpduLen = OSIX_FALSE;
                u1MsgFlag = OSIX_TRUE;
            }
            else
            {
                /*Only Db updation */
                pRadioIfGetDB->RadioIfIsGetAllDB.bMaxAmpduLen = OSIX_TRUE;
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NampduParams.u1MaxAmpduLen =
                    RadioIfGetDB.RadioIfGetAllDB.
                    Dot11NampduParams.u1SuppMaxAmpduLen;
            }

            pRadioIfGetDB->RadioIfIsGetAllDB.bSuppMinAmpduStart = OSIX_TRUE;
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NampduParams.
                u1SuppMinAmpduStart =
                RadioIfGetDB.RadioIfGetAllDB.Dot11NampduParams.
                u1SuppMinAmpduStart;

            if (pRadioIfGetDB->RadioIfGetAllDB.Dot11NampduParams.u1MinAmpduStart
                <=
                RadioIfGetDB.RadioIfGetAllDB.Dot11NampduParams.
                u1SuppMinAmpduStart)
            {
                RadioIfGetDB.RadioIfGetAllDB.Dot11NampduParams.
                    u1SuppMinAmpduStart =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NampduParams.
                    u1MinAmpduStart;
                pRadioIfGetDB->RadioIfIsGetAllDB.bMinAmpduStart = OSIX_FALSE;
                u1MsgFlag = OSIX_TRUE;
            }
            else
            {
                /*Only Db updation */
                pRadioIfGetDB->RadioIfIsGetAllDB.bMinAmpduStart = OSIX_TRUE;
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NampduParams.
                    u1MinAmpduStart =
                    RadioIfGetDB.RadioIfGetAllDB.Dot11NampduParams.
                    u1SuppMinAmpduStart;
            }

            /**************************************************************
             * Validation for HT Capabilities Element : Supp MCS Parameters 
             **************************************************************/

            MEMCPY (RadioIfGetDB.RadioIfGetAllDB.Dot11NsuppMcsParams.
                    au1SuppHtCapaMcs,
                    pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.
                    RadioIfInfoElem.unInfoElem.HTCapability.au1SuppMCSSet,
                    sizeof (RadioIfGetDB.RadioIfGetAllDB.Dot11NsuppMcsParams.
                            au1SuppHtCapaMcs));

            if (WssIfProcessRadioIfDBMsg
                (WSS_PARSE_11N_MCS_INFO, (&RadioIfGetDB)) != OSIX_SUCCESS)
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                             "RadioIfProcess11nStatusReq: Parsing"
                             "u1SuppAmpduParam to individual elements failed\n");
                return OSIX_FAILURE;
            }
            /* HT Capabilities bool varibales */

            pRadioIfGetDB->RadioIfIsGetAllDB.bSuppRxMcsBitmask = OSIX_TRUE;
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                au1SuppRxMcsBitmask[0] =
                RadioIfGetDB.RadioIfGetAllDB.Dot11NsuppMcsParams.
                au1SuppRxMcsBitmask[0];

            if (pRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                au1RxMcsBitmask[0] <=
                RadioIfGetDB.RadioIfGetAllDB.Dot11NsuppMcsParams.
                au1SuppRxMcsBitmask[0])
            {
                RadioIfGetDB.RadioIfGetAllDB.Dot11NsuppMcsParams.
                    au1SuppRxMcsBitmask[0] =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                    au1RxMcsBitmask[0];
                pRadioIfGetDB->RadioIfIsGetAllDB.bRxMcsBitmask = OSIX_FALSE;
                u1MsgFlag = OSIX_TRUE;
            }
            else
            {
                /*Only Db updation */
                pRadioIfGetDB->RadioIfIsGetAllDB.bRxMcsBitmask = OSIX_TRUE;
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                    au1RxMcsBitmask[0] =
                    RadioIfGetDB.RadioIfGetAllDB.Dot11NsuppMcsParams.
                    au1SuppRxMcsBitmask[0];
            }

            pRadioIfGetDB->RadioIfIsGetAllDB.bSuppRxMcsBitmask = OSIX_TRUE;
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                au1SuppRxMcsBitmask[1] =
                RadioIfGetDB.RadioIfGetAllDB.Dot11NsuppMcsParams.
                au1SuppRxMcsBitmask[1];

            if (pRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                au1RxMcsBitmask[1] <=
                RadioIfGetDB.RadioIfGetAllDB.Dot11NsuppMcsParams.
                au1SuppRxMcsBitmask[1])
            {
                RadioIfGetDB.RadioIfGetAllDB.Dot11NsuppMcsParams.
                    au1SuppRxMcsBitmask[1] =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                    au1RxMcsBitmask[1];
                pRadioIfGetDB->RadioIfIsGetAllDB.bRxMcsBitmask = OSIX_FALSE;
                u1MsgFlag = OSIX_TRUE;
            }
            else
            {
                /*Only Db updation */
                pRadioIfGetDB->RadioIfIsGetAllDB.bRxMcsBitmask = OSIX_TRUE;
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                    au1RxMcsBitmask[1] =
                    RadioIfGetDB.RadioIfGetAllDB.Dot11NsuppMcsParams.
                    au1SuppRxMcsBitmask[1];
            }

            pRadioIfGetDB->RadioIfIsGetAllDB.bSuppRxMcsBitmask = OSIX_TRUE;
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                au1SuppRxMcsBitmask[2] =
                RadioIfGetDB.RadioIfGetAllDB.Dot11NsuppMcsParams.
                au1SuppRxMcsBitmask[2];

            if (pRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                au1RxMcsBitmask[2] <=
                RadioIfGetDB.RadioIfGetAllDB.Dot11NsuppMcsParams.
                au1SuppRxMcsBitmask[2])
            {
                RadioIfGetDB.RadioIfGetAllDB.Dot11NsuppMcsParams.
                    au1SuppRxMcsBitmask[2] =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                    au1RxMcsBitmask[2];
                pRadioIfGetDB->RadioIfIsGetAllDB.bRxMcsBitmask = OSIX_FALSE;
                u1MsgFlag = OSIX_TRUE;
            }
            else
            {
                /*Only Db updation */
                pRadioIfGetDB->RadioIfIsGetAllDB.bRxMcsBitmask = OSIX_TRUE;
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                    au1RxMcsBitmask[2] =
                    RadioIfGetDB.RadioIfGetAllDB.Dot11NsuppMcsParams.
                    au1SuppRxMcsBitmask[2];
            }

            /*Set the value of rx-stbc according to 80211n standard */
            if (pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u1RxStbc !=
                RX_STBC_VALUE_DISABLE)
            {
                Dot11nCalculateRxStbc (pRadioIfGetDB);
                RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1SuppRxStbc =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u1RxStbc;
            }

            if (RadioIfGetDB.RadioIfGetAllDB.
                Dot11NsuppMcsParams.au1SuppRxMcsBitmask[0] != 0)
            {
                WssIfDot11nHighestSuppDataRate (&RadioIfGetDB);
            }

            pRadioIfGetDB->RadioIfIsGetAllDB.bSuppTxMcsSetDefined = OSIX_TRUE;
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                u1SuppTxMcsSetDefined =
                RadioIfGetDB.RadioIfGetAllDB.Dot11NsuppMcsParams.
                u1SuppTxMcsSetDefined;

            if (pRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                u1TxMcsSetDefined <=
                RadioIfGetDB.RadioIfGetAllDB.Dot11NsuppMcsParams.
                u1SuppTxMcsSetDefined)
            {
                RadioIfGetDB.RadioIfGetAllDB.Dot11NsuppMcsParams.
                    u1SuppTxMcsSetDefined =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                    u1TxMcsSetDefined;
                pRadioIfGetDB->RadioIfIsGetAllDB.bTxMcsSetDefined = OSIX_FALSE;
                u1MsgFlag = OSIX_TRUE;
            }
            else
            {
                /*Only Db updation */
                pRadioIfGetDB->RadioIfIsGetAllDB.bTxMcsSetDefined = OSIX_TRUE;
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                    u1TxMcsSetDefined =
                    RadioIfGetDB.RadioIfGetAllDB.Dot11NsuppMcsParams.
                    u1SuppTxMcsSetDefined;
            }

            pRadioIfGetDB->RadioIfIsGetAllDB.bSuppTxRxMcsSetNotEqual =
                OSIX_TRUE;
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                u1SuppTxMcsSetDefined =
                RadioIfGetDB.RadioIfGetAllDB.Dot11NsuppMcsParams.
                u1SuppTxMcsSetDefined;

            if (pRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                u1TxRxMcsSetNotEqual <=
                RadioIfGetDB.RadioIfGetAllDB.Dot11NsuppMcsParams.
                u1SuppTxRxMcsSetNotEqual)
            {
                RadioIfGetDB.RadioIfGetAllDB.Dot11NsuppMcsParams.
                    u1SuppTxRxMcsSetNotEqual =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                    u1TxRxMcsSetNotEqual;
                pRadioIfGetDB->RadioIfIsGetAllDB.bTxRxMcsSetNotEqual =
                    OSIX_FALSE;
                u1MsgFlag = OSIX_TRUE;
            }
            else
            {
                /*Only Db updation */
                pRadioIfGetDB->RadioIfIsGetAllDB.bTxRxMcsSetNotEqual =
                    OSIX_TRUE;
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                    u1TxRxMcsSetNotEqual =
                    RadioIfGetDB.RadioIfGetAllDB.Dot11NsuppMcsParams.
                    u1SuppTxRxMcsSetNotEqual;
            }

            if ((RadioIfGetDB.RadioIfGetAllDB.Dot11NsuppMcsParams.
                 u1SuppTxRxMcsSetNotEqual !=
                 RADIO_DOT11N_TXRXMCSSSETNOTEQUAL_DISABLE)
                && (RadioIfGetDB.RadioIfGetAllDB.Dot11NsuppMcsParams.
                    u1SuppTxMcsSetDefined !=
                    RADIO_DOT11N_TXMCSSETDEFINED_DISABLE))
            {
                WssIfDot11nSplStreamAndUnequal (&RadioIfGetDB);
            }
            /*Updation of DB based on Extended capabilities */
            RadioIfGetDB.RadioIfGetAllDB.Dot11NhtExtCapParams.u2SuppHtExtCap
                = pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.
                RadioIfInfoElem.unInfoElem.HTCapability.u2HTExtCap;

            if (WssIfProcessRadioIfDBMsg (WSS_PARSE_11N_EXTCAP_INFO,
                                          (&RadioIfGetDB)) != OSIX_SUCCESS)
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                             "RadioIfProcess11nStatusReq: Parsing"
                             "ExtHtCapInfo to individual elements failed\n");
                return OSIX_FAILURE;
            }
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.u2SuppHtExtCap =
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtExtCapParams.
                u2SuppHtExtCap;

            if (pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.u1Pco
                <= RadioIfGetDB.RadioIfGetAllDB.Dot11NhtExtCapParams.u1Pco)
            {
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtExtCapParams.u1Pco =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.u1Pco;
                pRadioIfGetDB->RadioIfIsGetAllDB.bPco = OSIX_FALSE;
                u1MsgFlag = OSIX_TRUE;
            }
            else
            {
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.u1Pco =
                    RadioIfGetDB.RadioIfGetAllDB.Dot11NhtExtCapParams.u1Pco;
                pRadioIfGetDB->RadioIfIsGetAllDB.bPco = OSIX_TRUE;
            }
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.u1SuppPco =
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtExtCapParams.u1SuppPco;

            if (pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.
                u1PcoTransTime <=
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtExtCapParams.
                u1PcoTransTime)
            {
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtExtCapParams.
                    u1PcoTransTime =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.
                    u1PcoTransTime;
                pRadioIfGetDB->RadioIfIsGetAllDB.bPcoTransTime = OSIX_FALSE;
                u1MsgFlag = OSIX_TRUE;
            }
            else
            {
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.
                    u1PcoTransTime =
                    RadioIfGetDB.RadioIfGetAllDB.Dot11NhtExtCapParams.
                    u1PcoTransTime;
                pRadioIfGetDB->RadioIfIsGetAllDB.bPcoTransTime = OSIX_TRUE;
            }
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.
                u1SuppPcoTransTime =
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtExtCapParams.
                u1SuppPcoTransTime;

            if (pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.
                u1McsFeedback <=
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtExtCapParams.u1McsFeedback)
            {
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtExtCapParams.
                    u1McsFeedback =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.
                    u1McsFeedback;
                pRadioIfGetDB->RadioIfIsGetAllDB.bMcsFeedback = OSIX_FALSE;
                u1MsgFlag = OSIX_TRUE;
            }
            else
            {
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.
                    u1McsFeedback =
                    RadioIfGetDB.RadioIfGetAllDB.Dot11NhtExtCapParams.
                    u1McsFeedback;
                pRadioIfGetDB->RadioIfIsGetAllDB.bMcsFeedback = OSIX_TRUE;
            }
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.
                u1SuppMcsFeedback =
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtExtCapParams.
                u1SuppMcsFeedback;

            if (pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.u1HtcSupp
                <= RadioIfGetDB.RadioIfGetAllDB.Dot11NhtExtCapParams.u1HtcSupp)
            {
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtExtCapParams.u1HtcSupp =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.
                    u1HtcSupp;
                pRadioIfGetDB->RadioIfIsGetAllDB.bHtcSupp = OSIX_FALSE;
                u1MsgFlag = OSIX_TRUE;
            }
            else
            {
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.u1HtcSupp =
                    RadioIfGetDB.RadioIfGetAllDB.Dot11NhtExtCapParams.u1HtcSupp;
                pRadioIfGetDB->RadioIfIsGetAllDB.bHtcSupp = OSIX_TRUE;
            }
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.u1SuppHtcSupp =
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtExtCapParams.u1SuppHtcSupp;

            if (pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.
                u1RdResponder <=
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtExtCapParams.u1RdResponder)
            {
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtExtCapParams.
                    u1RdResponder =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.
                    u1RdResponder;
                pRadioIfGetDB->RadioIfIsGetAllDB.bRdResponder = OSIX_FALSE;
                u1MsgFlag = OSIX_TRUE;
            }
            else
            {
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.
                    u1RdResponder =
                    RadioIfGetDB.RadioIfGetAllDB.Dot11NhtExtCapParams.
                    u1RdResponder;
                pRadioIfGetDB->RadioIfIsGetAllDB.bRdResponder = OSIX_TRUE;
            }
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.
                u1SuppRdResponder =
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtExtCapParams.
                u1SuppRdResponder;

            /*Updation of DB based on Beamform  capabilities */
            RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u4SuppTxBeamCapParam =
                pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.
                RadioIfInfoElem.unInfoElem.HTCapability.u4TranBeamformCap;

            if (WssIfProcessRadioIfDBMsg (WSS_PARSE_11N_BEAMFORMING_INFO,
                                          (&RadioIfGetDB)) != OSIX_SUCCESS)
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                             "RadioIfProcess11nStatusReq: Parsing"
                             "ExtHtCapInfo to individual elements failed\n");
                return OSIX_FAILURE;
            }
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u4SuppTxBeamCapParam =
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u4SuppTxBeamCapParam;

            if (pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1ImpTranBeamRecCapable <=
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1ImpTranBeamRecCapable)
            {
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1ImpTranBeamRecCapable =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1ImpTranBeamRecCapable;
                pRadioIfGetDB->RadioIfIsGetAllDB.bImpTranBeamRecCapable =
                    OSIX_FALSE;
                u1MsgFlag = OSIX_TRUE;
            }
            else
            {
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1ImpTranBeamRecCapable =
                    RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1ImpTranBeamRecCapable;
                pRadioIfGetDB->RadioIfIsGetAllDB.bImpTranBeamRecCapable =
                    OSIX_TRUE;
            }
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1SuppImpTranBeamRecCapable =
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1SuppImpTranBeamRecCapable;

            if (pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1RecStagSoundCapable <=
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1RecStagSoundCapable)
            {
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1RecStagSoundCapable =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1RecStagSoundCapable;
                pRadioIfGetDB->RadioIfIsGetAllDB.bRecStagSoundCapable =
                    OSIX_FALSE;
                u1MsgFlag = OSIX_TRUE;
            }
            else
            {
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1RecStagSoundCapable =
                    RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1RecStagSoundCapable;
                pRadioIfGetDB->RadioIfIsGetAllDB.bRecStagSoundCapable =
                    OSIX_TRUE;
            }
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1SuppRecStagSoundCapable =
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1SuppRecStagSoundCapable;

            if (pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1TransStagSoundCapable <=
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1TransStagSoundCapable)
            {
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1TransStagSoundCapable =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1TransStagSoundCapable;
                pRadioIfGetDB->RadioIfIsGetAllDB.bTransStagSoundCapable =
                    OSIX_FALSE;
                u1MsgFlag = OSIX_TRUE;
            }
            else
            {
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1TransStagSoundCapable =
                    RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1TransStagSoundCapable;
                pRadioIfGetDB->RadioIfIsGetAllDB.bTransStagSoundCapable =
                    OSIX_TRUE;
            }
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1SuppTransStagSoundCapable =
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1SuppTransStagSoundCapable;

            if (pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1RecNdpCapable <=
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1RecNdpCapable)
            {
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1RecNdpCapable =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1RecNdpCapable;
                pRadioIfGetDB->RadioIfIsGetAllDB.bRecNdpCapable = OSIX_FALSE;
                u1MsgFlag = OSIX_TRUE;
            }
            else
            {
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1RecNdpCapable =
                    RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1RecNdpCapable;
                pRadioIfGetDB->RadioIfIsGetAllDB.bRecNdpCapable = OSIX_TRUE;
            }
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1SuppRecNdpCapable =
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1SuppRecNdpCapable;

            if (pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1TransNdpCapable <=
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1TransNdpCapable)
            {
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1TransNdpCapable =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1TransNdpCapable;
                pRadioIfGetDB->RadioIfIsGetAllDB.bTransNdpCapable = OSIX_FALSE;
                u1MsgFlag = OSIX_TRUE;
            }
            else
            {
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1TransNdpCapable =
                    RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1TransNdpCapable;
                pRadioIfGetDB->RadioIfIsGetAllDB.bTransNdpCapable = OSIX_TRUE;
            }
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1SuppTransNdpCapable =
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1SuppTransNdpCapable;

            if (pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1ImpTranBeamCapable <=
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1ImpTranBeamCapable)
            {
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1ImpTranBeamCapable =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1ImpTranBeamCapable;
                pRadioIfGetDB->RadioIfIsGetAllDB.bImpTranBeamCapable =
                    OSIX_FALSE;
                u1MsgFlag = OSIX_TRUE;
            }
            else
            {
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1ImpTranBeamCapable =
                    RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1ImpTranBeamCapable;
                pRadioIfGetDB->RadioIfIsGetAllDB.bImpTranBeamCapable =
                    OSIX_TRUE;
            }
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1SuppImpTranBeamCapable =
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1SuppImpTranBeamCapable;

            if (pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1Calibration <=
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1Calibration)
            {
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1Calibration =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1Calibration;
                pRadioIfGetDB->RadioIfIsGetAllDB.bCalibration = OSIX_FALSE;
                u1MsgFlag = OSIX_TRUE;
            }
            else
            {
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1Calibration =
                    RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1Calibration;
                pRadioIfGetDB->RadioIfIsGetAllDB.bCalibration = OSIX_TRUE;
            }
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1SuppCalibration =
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1SuppCalibration;

            if (pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1ExplCsiTranBeamCapable <=
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1ExplCsiTranBeamCapable)
            {
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1ExplCsiTranBeamCapable =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1ExplCsiTranBeamCapable;
                pRadioIfGetDB->RadioIfIsGetAllDB.bExplCsiTranBeamCapable =
                    OSIX_FALSE;
                u1MsgFlag = OSIX_TRUE;
            }
            else
            {
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1ExplCsiTranBeamCapable =
                    RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1ExplCsiTranBeamCapable;
                pRadioIfGetDB->RadioIfIsGetAllDB.bExplCsiTranBeamCapable =
                    OSIX_TRUE;
            }
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1SuppExplCsiTranBeamCapable =
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1SuppExplCsiTranBeamCapable;

            if (pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1ExplNoncompSteCapable <=
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1ExplNoncompSteCapable)
            {
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1ExplNoncompSteCapable =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1ExplNoncompSteCapable;
                pRadioIfGetDB->RadioIfIsGetAllDB.bExplNoncompSteCapable =
                    OSIX_FALSE;
                u1MsgFlag = OSIX_TRUE;
            }
            else
            {
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1ExplNoncompSteCapable =
                    RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1ExplNoncompSteCapable;
                pRadioIfGetDB->RadioIfIsGetAllDB.bExplNoncompSteCapable =
                    OSIX_TRUE;
            }
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1SuppExplNoncompSteCapable =
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1SuppExplNoncompSteCapable;

            if (pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1ExplCompSteCapable <=
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1ExplCompSteCapable)
            {
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1ExplCompSteCapable =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1ExplCompSteCapable;
                pRadioIfGetDB->RadioIfIsGetAllDB.bExplCompSteCapable =
                    OSIX_FALSE;
                u1MsgFlag = OSIX_TRUE;
            }
            else
            {
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1ExplCompSteCapable =
                    RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1ExplCompSteCapable;
                pRadioIfGetDB->RadioIfIsGetAllDB.bExplCompSteCapable =
                    OSIX_TRUE;
            }
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1SuppExplCompSteCapable =
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1SuppExplCompSteCapable;

            if (pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1ExplTranBeamCsiFeedback <=
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1ExplTranBeamCsiFeedback)
            {
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1ExplTranBeamCsiFeedback =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1ExplTranBeamCsiFeedback;
                pRadioIfGetDB->RadioIfIsGetAllDB.bExplTranBeamCsiFeedback =
                    OSIX_FALSE;
                u1MsgFlag = OSIX_TRUE;
            }
            else
            {
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1ExplTranBeamCsiFeedback =
                    RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1ExplTranBeamCsiFeedback;
                pRadioIfGetDB->RadioIfIsGetAllDB.bExplTranBeamCsiFeedback =
                    OSIX_TRUE;
            }
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1SuppExplTranBeamCsiFeedback =
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1SuppExplTranBeamCsiFeedback;

            if (pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1ExplNoncompBeamFbCapable <=
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1ExplNoncompBeamFbCapable)
            {
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1ExplNoncompBeamFbCapable =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1ExplNoncompBeamFbCapable;
                pRadioIfGetDB->RadioIfIsGetAllDB.bExplNoncompBeamFbCapable =
                    OSIX_FALSE;
                u1MsgFlag = OSIX_TRUE;
            }
            else
            {
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1ExplNoncompBeamFbCapable =
                    RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1ExplNoncompBeamFbCapable;
                pRadioIfGetDB->RadioIfIsGetAllDB.bExplNoncompBeamFbCapable =
                    OSIX_TRUE;
            }
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1SuppExplNoncompBeamFbCapable =
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1SuppExplNoncompBeamFbCapable;

            if (pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1ExplCompBeamFbCapable <=
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1ExplCompBeamFbCapable)
            {
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1ExplCompBeamFbCapable =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1ExplCompBeamFbCapable;
                pRadioIfGetDB->RadioIfIsGetAllDB.bExplCompBeamFbCapable =
                    OSIX_FALSE;
                u1MsgFlag = OSIX_TRUE;
            }
            else
            {
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1ExplCompBeamFbCapable =
                    RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1ExplCompBeamFbCapable;
                pRadioIfGetDB->RadioIfIsGetAllDB.bExplCompBeamFbCapable =
                    OSIX_TRUE;
            }
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1SuppExplCompBeamFbCapable =
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1SuppExplCompBeamFbCapable;

            if (pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1MinGrouping <=
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1MinGrouping)
            {
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1MinGrouping =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1MinGrouping;
                pRadioIfGetDB->RadioIfIsGetAllDB.bMinGrouping = OSIX_FALSE;
                u1MsgFlag = OSIX_TRUE;
            }
            else
            {
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1MinGrouping =
                    RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1MinGrouping;
                pRadioIfGetDB->RadioIfIsGetAllDB.bMinGrouping = OSIX_TRUE;
            }
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1SuppMinGrouping =
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1SuppMinGrouping;

            if (pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1CsiNoofBeamAntSupported <=
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1CsiNoofBeamAntSupported)
            {
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1CsiNoofBeamAntSupported =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1CsiNoofBeamAntSupported;
                pRadioIfGetDB->RadioIfIsGetAllDB.bCsiNoofBeamAntSupported =
                    OSIX_FALSE;
                u1MsgFlag = OSIX_TRUE;
            }
            else
            {
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1CsiNoofBeamAntSupported =
                    RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1CsiNoofBeamAntSupported;
                pRadioIfGetDB->RadioIfIsGetAllDB.bCsiNoofBeamAntSupported =
                    OSIX_TRUE;
            }
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1SuppCsiNoofBeamAntSupported =
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1SuppCsiNoofBeamAntSupported;

            if (pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1NoncompSteNoBeamformerAntSupp <=
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1NoncompSteNoBeamformerAntSupp)
            {
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1NoncompSteNoBeamformerAntSupp =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1NoncompSteNoBeamformerAntSupp;
                pRadioIfGetDB->RadioIfIsGetAllDB.bNoncompSteNoBeamformer =
                    OSIX_FALSE;
                u1MsgFlag = OSIX_TRUE;
            }
            else
            {
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1NoncompSteNoBeamformerAntSupp =
                    RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1NoncompSteNoBeamformerAntSupp;
                pRadioIfGetDB->RadioIfIsGetAllDB.bNoncompSteNoBeamformer =
                    OSIX_TRUE;
            }
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1SuppNoncompSteNoBeamformer =
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1SuppNoncompSteNoBeamformer;

            if (pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1CompSteNoBeamformerAntSupp <=
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1CompSteNoBeamformerAntSupp)
            {
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1CompSteNoBeamformerAntSupp =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1CompSteNoBeamformerAntSupp;
                pRadioIfGetDB->RadioIfIsGetAllDB.bCompSteNoBeamformer =
                    OSIX_FALSE;
                u1MsgFlag = OSIX_TRUE;
            }
            else
            {
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1CompSteNoBeamformerAntSupp =
                    RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1CompSteNoBeamformerAntSupp;
                pRadioIfGetDB->RadioIfIsGetAllDB.bCompSteNoBeamformer =
                    OSIX_TRUE;
            }
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1SuppCompSteNoBeamformerAntSupp =
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1SuppCompSteNoBeamformerAntSupp;

            if (pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1CSIMaxNoRowsBeamformerSupp <=
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1CSIMaxNoRowsBeamformerSupp)
            {
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1CSIMaxNoRowsBeamformerSupp =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1CSIMaxNoRowsBeamformerSupp;
                pRadioIfGetDB->RadioIfIsGetAllDB.bCSIMaxNoRowsBeamformer =
                    OSIX_FALSE;
                u1MsgFlag = OSIX_TRUE;
            }
            else
            {
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1CSIMaxNoRowsBeamformerSupp =
                    RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1CSIMaxNoRowsBeamformerSupp;
                pRadioIfGetDB->RadioIfIsGetAllDB.bCSIMaxNoRowsBeamformer =
                    OSIX_TRUE;
            }
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1SuppCSIMaxNoRowsBeamformerSupp =
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1SuppCSIMaxNoRowsBeamformerSupp;

            if (pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1ChannelEstCapability <=
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1ChannelEstCapability)
            {
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1ChannelEstCapability =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1ChannelEstCapability;
                pRadioIfGetDB->RadioIfIsGetAllDB.bChannelEstCapability =
                    OSIX_FALSE;
                u1MsgFlag = OSIX_TRUE;
            }
            else
            {
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1ChannelEstCapability =
                    RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1ChannelEstCapability;
                pRadioIfGetDB->RadioIfIsGetAllDB.bChannelEstCapability =
                    OSIX_TRUE;
            }
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1SuppChannelEstCapability =
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1SuppChannelEstCapability;

            if (u1MsgFlag == OSIX_TRUE)
            {

                /*********************************************
                 * Message Assamble for HT Capabilities Info
                 *********************************************/
                /*Accomodation of 20 params in u2 variable */
                if (WssIfProcessRadioIfDBMsg
                    (WSS_ASSEMBLE_11N_CAPABILITY_INFO,
                     (&RadioIfGetDB)) != OSIX_SUCCESS)
                {
                    RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                                 "RadioIfProcess11nStatusReq: Parsing u2HtCapInfo"
                                 "to individual elements failed\n");
                    return OSIX_FAILURE;
                }
                pWssMsgStruct->unRadioIfMsg.
                    RadioIfConfigStatusReq.RadioIfInfoElem.isPresent |=
                    OSIX_TRUE;
                pWssMsgStruct->unRadioIfMsg.
                    RadioIfConfigStatusReq.RadioIfInfoElem.unInfoElem.
                    HTCapability.isOptional |= OSIX_TRUE;
                /* Supported value WLC value is less than the Supp WTP value.
                   So response message constructed
                 */

                pRadioIfGetDB->RadioIfIsGetAllDB.bHtCapInfo = OSIX_TRUE;
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u2HtCapInfo
                    = RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u2HtCapInfo;
            /************************************ 
             * Message Assamble for A-MPDU Param
             ************************************/

                /*Accomodation of 20 params in u2 variable */
                if (WssIfProcessRadioIfDBMsg
                    (WSS_ASSEMBLE_11N_AMPDU_INFO,
                     (&RadioIfGetDB)) != OSIX_SUCCESS)
                {
                    RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                                 "RadioIfProcess11nStatusReq: Parsing "
                                 "to individual elements failed\n");
                    return OSIX_FAILURE;
                }
                pWssMsgStruct->unRadioIfMsg.
                    RadioIfConfigStatusReq.RadioIfInfoElem.isPresent |=
                    OSIX_TRUE;
                pWssMsgStruct->unRadioIfMsg.
                    RadioIfConfigStatusReq.RadioIfInfoElem.unInfoElem.
                    HTCapability.isOptional |= OSIX_TRUE;
                /* Supported value WLC value is less than the Supp WTP value.
                   So response message constructed
                 */

                pRadioIfGetDB->RadioIfIsGetAllDB.bAmpduParam = OSIX_TRUE;
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NampduParams.u1AmpduParam
                    =
                    RadioIfGetDB.RadioIfGetAllDB.Dot11NampduParams.u1AmpduParam;

                /*********************************************
                 * Message Assamble for HT Supported MCS
                 *********************************************/
                /*Accomodation of 20 params in u2 variable */
                if (WssIfProcessRadioIfDBMsg
                    (WSS_ASSEMBLE_11N_MCS_INFO,
                     (&RadioIfGetDB)) != OSIX_SUCCESS)
                {
                    RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                                 "RadioIfProcess11nStatusReq: Parsing u2HtCapInfo"
                                 "to individual elements failed\n");
                    return OSIX_FAILURE;
                }
                pWssMsgStruct->unRadioIfMsg.
                    RadioIfConfigStatusReq.RadioIfInfoElem.isPresent |=
                    OSIX_TRUE;
                pWssMsgStruct->unRadioIfMsg.
                    RadioIfConfigStatusReq.RadioIfInfoElem.unInfoElem.
                    HTCapability.isOptional |= OSIX_TRUE;
                /* Supported value WLC value is less than the Supp WTP value.
                   So response message constructed
                 */

                pRadioIfGetDB->RadioIfIsGetAllDB.bHtCapaMcs = OSIX_TRUE;
                MEMCPY (pRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                        au1HtCapaMcs,
                        RadioIfGetDB.RadioIfGetAllDB.Dot11NsuppMcsParams.
                        au1HtCapaMcs,
                        sizeof (pRadioIfGetDB->RadioIfGetAllDB.
                                Dot11NsuppMcsParams.au1HtCapaMcs));
                if (WssIfProcessRadioIfDBMsg
                    (WSS_ASSEMBLE_11N_EXTCAP_INFO,
                     (&RadioIfGetDB)) != OSIX_SUCCESS)
                {
                    RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                                 "RadioIfProcess11nStatusReq: Assembling of"
                                 "capabilities from individual elements failed\n");
                    return OSIX_FAILURE;
                }
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.u2HtExtCap =
                    RadioIfGetDB.RadioIfGetAllDB.Dot11NhtExtCapParams.
                    u2SuppHtExtCap;
                if (WssIfProcessRadioIfDBMsg
                    (WSS_ASSEMBLE_11N_BEAMFORMING_INFO,
                     (&RadioIfGetDB)) != OSIX_SUCCESS)
                {
                    RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                                 "RadioIfProcess11nStatusReq: Assembling of"
                                 "capabilities from individual elements failed\n");
                    return OSIX_FAILURE;
                }
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u4TxBeamCapParam =
                    RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u4SuppTxBeamCapParam;

            }
            break;
        case DOT11_INFO_ELEM_HTOPERATION:
            pWssMsgStruct->unRadioIfMsg.
                RadioIfConfigStatusReq.RadioIfInfoElem.isPresent |= OSIX_TRUE;
            pWssMsgStruct->unRadioIfMsg.
                RadioIfConfigStatusReq.RadioIfInfoElem.unInfoElem.
                HTOperation.isOptional |= OSIX_TRUE;
            break;
        default:
            break;
    }

    if (WssIfProcessRadioIfDBMsg (WSS_SET_RADIO_IF_DB_11N, (pRadioIfGetDB))
        != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfConfigStatusReq: Set of Radio DB for 11AC failed \r\n");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;

}

/*****************************************************************************
 * Function Name      : Dot11nCalculateRxStbc                                 *
 *                                                                           *
 * Description        : This functions calculates the rx stbc value based on *
 *                      the mcs values                                       *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to access the radio          *
 *                      DB elements                                          * 
 *                                                       *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : NONE
 *                                                                        *
 *                                                                           *
******************************************************************************/
VOID
Dot11nCalculateRxStbc (tRadioIfGetDB * pRadioIfGetDB)
{
    UINT1               u1McsIndex;
    pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u1RxStbc = 0;
    for (u1McsIndex = 0; u1McsIndex < 3; u1McsIndex++)
    {
        if (pRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
            au1RxMcsBitmask[u1McsIndex] != 0)
        {
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u1RxStbc++;
        }
    }
}

/*****************************************************************************
 * Function Name      : RadioIfProcess11AcStatusReq                          *
 *                                                                           *
 * Description        : This functions processes 11AC Config Status Request  *
 *                      params from WTP                                      *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to access the radio          *
 *                      DB elements                                          * 
 *                        pWssMsgStruct - Pointer to the input structure       *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/

UINT1
RadioIfProcess11AcStatusReq (tRadioIfGetDB * pRadioIfGetDB,
                             tRadioIfMsgStruct * pWssMsgStruct)
{
    tRadioIfGetDB       RadioIfGetDB;
    UINT2               u2SuppRxMcs = 0;
    UINT2               u2SuppTxMcs = 0;
    UINT2               u2RxMcs = 0;
    UINT2               u2TxMcs = 0;
    UINT2               u2Shift = 0;
    UINT2               u2CtrlMcs = 0;
    UINT2               u2SuppMcs = 0;
    UINT2               u2Mcs = 0;
    UINT1               u1MsgFlag = OSIX_FALSE;
    UINT1               u1Loop = 0;
    UINT2               u2RxDataRate = 0;
    UINT2               u2TxDataRate = 0;
    UINT2               u2McsSts = 0;
    UINT1               u1Mcs = 0;
    UINT1               u1STS = 0;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    /*VHT Capability */
    switch (pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.RadioIfInfoElem.
            u1EleId)
    {
        case DOT11AC_INFO_ELEM_VHTCAPABILITY:

            RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u4VhtCapInfo =
                pWssMsgStruct->unRadioIfMsg.
                RadioIfConfigStatusReq.RadioIfInfoElem.unInfoElem.VHTCapability.
                u4VhtCapInfo;
            if (WssIfProcessRadioIfDBMsg
                (WSS_PARSE_11AC_CAPABILITY_INFO,
                 (&RadioIfGetDB)) != OSIX_SUCCESS)
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                             "RadioIfProcess11AcStatusReq: Parsing"
                             "u4VhtCapInfo to individual elements failed\n");
                return OSIX_FAILURE;
            }

            pRadioIfGetDB->RadioIfIsGetAllDB.bMaxMPDULen = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bRxLpdc = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bShortGi80 = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bTxStbc = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bRxStbc = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bVhtMaxAMPDU = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bVhtCapaMcs = OSIX_TRUE;

            /*VHT Operation */
            if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11AC,
                                          (pRadioIfGetDB)) != OSIX_SUCCESS)
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                             "RadioIfProcess11AcStatusReq:"
                             " Get of Radio DB for 11AC failed \n");
                return OSIX_FAILURE;
            }

            /*Updation of Capability value in db */
            pRadioIfGetDB->RadioIfIsGetAllDB.bSuppVhtCapInfo = OSIX_TRUE;
            pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u4SuppVhtCapInfo =
                RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u4VhtCapInfo;
            pRadioIfGetDB->RadioIfIsGetAllDB.bSuppMaxMPDULen = OSIX_TRUE;
            pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1SuppMaxMPDULen =
                RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1SuppMaxMPDULen;

            if ((pRadioIfGetDB->RadioIfGetAllDB.
                 Dot11AcCapaParams.u1MaxMPDULen) <
                (RadioIfGetDB.RadioIfGetAllDB.
                 Dot11AcCapaParams.u1SuppMaxMPDULen))
            {
                /* Mandatory value less the the supported value.
                 * No Db updation
                 * Response Message needs to be constructed*/

                RadioIfGetDB.RadioIfGetAllDB.
                    Dot11AcCapaParams.u1SuppMaxMPDULen =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11AcCapaParams.u1MaxMPDULen;
                pRadioIfGetDB->RadioIfIsGetAllDB.bMaxMPDULen = OSIX_FALSE;
                u1MsgFlag = OSIX_TRUE;
            }
            else
            {
                /*Only Db updation */
                pRadioIfGetDB->RadioIfIsGetAllDB.bMaxMPDULen = OSIX_TRUE;
                pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1MaxMPDULen =
                    RadioIfGetDB.RadioIfGetAllDB.
                    Dot11AcCapaParams.u1SuppMaxMPDULen;
            }

            /*Updation of Capability value in db */
            pRadioIfGetDB->RadioIfIsGetAllDB.bSuppChanWidth = OSIX_TRUE;
            pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1SuppChanWidth =
                RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1SuppChanWidth;

            /*Updation of Capability value in db */
            pRadioIfGetDB->RadioIfIsGetAllDB.bSuppRxLpdc = OSIX_TRUE;
            pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1SuppRxLpdc =
                RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1SuppRxLpdc;

            if ((pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1RxLpdc) <
                (RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1SuppRxLpdc))
            {
                /* Mandatory value less the the supported value.
                 * No Db updation
                 * Response Message needs to be constructed*/

                RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1SuppRxLpdc =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1RxLpdc;
                pRadioIfGetDB->RadioIfIsGetAllDB.bRxLpdc = OSIX_FALSE;
                u1MsgFlag = OSIX_TRUE;

            }
            else
            {
                /*Only Db updation */
                pRadioIfGetDB->RadioIfIsGetAllDB.bRxLpdc = OSIX_TRUE;
                pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1RxLpdc =
                    RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1SuppRxLpdc;
            }

            /*Updation of Capability value in db */
            pRadioIfGetDB->RadioIfIsGetAllDB.bSuppShortGi80 = OSIX_TRUE;
            pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1SuppShortGi80 =
                RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1SuppShortGi80;

            if ((pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1ShortGi80) <
                (RadioIfGetDB.RadioIfGetAllDB.
                 Dot11AcCapaParams.u1SuppShortGi80))
            {
                /* Mandatory value less the the supported value.
                 * No Db updation
                 * Response Message needs to be constructed*/

                RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1SuppShortGi80 =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11AcCapaParams.u1ShortGi80;
                pRadioIfGetDB->RadioIfIsGetAllDB.bShortGi80 = OSIX_FALSE;
                u1MsgFlag = OSIX_TRUE;

            }
            else
            {
                /*Only Db updation */
                pRadioIfGetDB->RadioIfIsGetAllDB.bShortGi80 = OSIX_TRUE;
                pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1ShortGi80 =
                    RadioIfGetDB.RadioIfGetAllDB.
                    Dot11AcCapaParams.u1SuppShortGi80;
            }

            /*Updation of Capability value in db */
            pRadioIfGetDB->RadioIfIsGetAllDB.bShortGi160 = OSIX_TRUE;
            pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1ShortGi160 =
                RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1ShortGi160;

            /*Updation of Capability value in db */
            pRadioIfGetDB->RadioIfIsGetAllDB.bSuppVhtTxStbc = OSIX_TRUE;
            pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1SuppTxStbc =
                RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1SuppTxStbc;

            if ((pRadioIfGetDB->RadioIfGetAllDB.
                 Dot11AcCapaParams.u1TxStbc) <
                (RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1SuppTxStbc))
            {
                /* Mandatory value less the the supported value.
                 * No Db updation
                 * Response Message needs to be constructed*/

                RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.
                    u1SuppTxStbc = pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11AcCapaParams.u1TxStbc;
                pRadioIfGetDB->RadioIfIsGetAllDB.bTxStbc = OSIX_FALSE;
                u1MsgFlag = OSIX_TRUE;

            }
            else
            {
                /*Only Db updation */
                pRadioIfGetDB->RadioIfIsGetAllDB.bTxStbc = OSIX_TRUE;
                pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1TxStbc =
                    RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1SuppTxStbc;
            }

            /*Updation of Capability value in db */
            pRadioIfGetDB->RadioIfIsGetAllDB.bSuppVhtRxStbc = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bSuppRxStbcStatus = OSIX_TRUE;
            pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1SuppRxStbc =
                RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1SuppRxStbc;
            pRadioIfGetDB->RadioIfIsGetAllDB.bSuppRxStbcStatus = OSIX_TRUE;
            if (pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.
                u1SuppRxStbc != 0)
            {
                pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.
                    u1SuppRxStbcStatus = OSIX_TRUE;
            }
            if ((pRadioIfGetDB->RadioIfGetAllDB.
                 Dot11AcCapaParams.u1RxStbc) <
                (RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1SuppRxStbc))
            {
                /* Mandatory value less the the supported value.
                 * No Db updation
                 * Response Message needs to be constructed*/

                RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.
                    u1SuppRxStbc = pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11AcCapaParams.u1RxStbc;
                pRadioIfGetDB->RadioIfIsGetAllDB.bRxStbc = OSIX_FALSE;
                u1MsgFlag = OSIX_TRUE;

            }
            else
            {
                /*Only Db updation */
                pRadioIfGetDB->RadioIfIsGetAllDB.bRxStbc = OSIX_TRUE;
                pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1RxStbc =
                    RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1SuppRxStbc;
                pRadioIfGetDB->RadioIfIsGetAllDB.bRxStbcStatus = OSIX_TRUE;
                if (pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.
                    u1RxStbc != 0)
                {
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.
                        u1RxStbcStatus = OSIX_TRUE;
                }
            }

            /*Updation of Capability value in db */
            pRadioIfGetDB->RadioIfIsGetAllDB.bSuBeamFormer = OSIX_TRUE;
            pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1SuBeamFormer =
                RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1SuBeamFormer;

            /*Updation of Capability value in db */
            pRadioIfGetDB->RadioIfIsGetAllDB.bSuBeamFormee = OSIX_TRUE;
            pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1SuBeamFormee =
                RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1SuBeamFormee;

            /*Updation of Capability value in db */
            pRadioIfGetDB->RadioIfIsGetAllDB.bSuppBeamFormAnt = OSIX_TRUE;
            pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1SuppBeamFormAnt =
                RadioIfGetDB.RadioIfGetAllDB.
                Dot11AcCapaParams.u1SuppBeamFormAnt;

            /*Updation of Capability value in db */
            pRadioIfGetDB->RadioIfIsGetAllDB.bSoundDimension = OSIX_TRUE;
            pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1SoundDimension =
                RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1SoundDimension;

            /*Updation of Capability value in db */
            pRadioIfGetDB->RadioIfIsGetAllDB.bMuBeamFormer = OSIX_TRUE;
            pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1MuBeamFormer =
                RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1MuBeamFormer;

            /*Updation of Capability value in db */
            pRadioIfGetDB->RadioIfIsGetAllDB.bMuBeamFormee = OSIX_TRUE;
            pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1MuBeamFormee =
                RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1MuBeamFormee;

            /*Updation of Capability value in db */
            pRadioIfGetDB->RadioIfIsGetAllDB.bVhtTxOpPs = OSIX_TRUE;
            pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1VhtTxOpPs =
                RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1VhtTxOpPs;

            /*Updation of Capability value in db */
            pRadioIfGetDB->RadioIfIsGetAllDB.bHtcVhtCapable = OSIX_TRUE;
            pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1HtcVhtCapable =
                RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1HtcVhtCapable;

            /*Updation of Capability value in db */
            pRadioIfGetDB->RadioIfIsGetAllDB.bSuppVhtMaxAMPDU = OSIX_TRUE;
            pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1SuppVhtMaxAMPDU =
                RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.
                u1SuppVhtMaxAMPDU;

            if ((pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1VhtMaxAMPDU)
                <
                (RadioIfGetDB.RadioIfGetAllDB.
                 Dot11AcCapaParams.u1SuppVhtMaxAMPDU))
            {
                /* Mandatory value less the the supported value.
                 * No Db updation
                 * Response Message needs to be constructed*/

                RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1SuppVhtMaxAMPDU
                    =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11AcCapaParams.u1VhtMaxAMPDU;
                pRadioIfGetDB->RadioIfIsGetAllDB.bVhtMaxAMPDU = OSIX_FALSE;
                u1MsgFlag = OSIX_TRUE;
            }
            else
            {
                /*Only Db updation */
                pRadioIfGetDB->RadioIfIsGetAllDB.bVhtMaxAMPDU = OSIX_TRUE;
                pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1VhtMaxAMPDU =
                    RadioIfGetDB.RadioIfGetAllDB.
                    Dot11AcCapaParams.u1SuppVhtMaxAMPDU;
            }

            /*Updation of Capability value in db */
            pRadioIfGetDB->RadioIfIsGetAllDB.bVhtLinkAdaption = OSIX_TRUE;
            pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1VhtLinkAdaption =
                RadioIfGetDB.RadioIfGetAllDB.
                Dot11AcCapaParams.u1VhtLinkAdaption;

            /*Updation of Capability value in db */
            pRadioIfGetDB->RadioIfIsGetAllDB.bVhtRxAntenna = OSIX_TRUE;
            pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1VhtRxAntenna =
                RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1VhtRxAntenna;

            /*Updation of Capability value in db */
            pRadioIfGetDB->RadioIfIsGetAllDB.bVhtTxAntenna = OSIX_TRUE;
            pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1VhtTxAntenna =
                RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1VhtTxAntenna;

            /*Updation of Capability value in db */
            pRadioIfGetDB->RadioIfIsGetAllDB.bSuppVhtCapaMcs = OSIX_TRUE;
            MEMCPY (pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11AcCapaParams.au1SuppVhtCapaMcs,
                    pWssMsgStruct->unRadioIfMsg.
                    RadioIfConfigStatusReq.RadioIfInfoElem.unInfoElem.
                    VHTCapability.au1SuppMCS, DOT11AC_VHT_CAP_MCS_LEN);

            /*Highest Supported Long Gi Data Rate */
            MEMCPY (&u2RxDataRate, &(pRadioIfGetDB->RadioIfGetAllDB.
                                     Dot11AcCapaParams.
                                     au1SuppVhtCapaMcs[RADIO_VALUE_2]),
                    RADIO_VALUE_2);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u2SuppRxDataRate =
                (OSIX_NTOHS ((UINT2) (u2RxDataRate >> RADIO_VALUE_3)));
            MEMCPY (&u2TxDataRate,
                    &(pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.
                      au1SuppVhtCapaMcs[RADIO_VALUE_6]), RADIO_VALUE_2);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u2SuppTxDataRate =
                (OSIX_NTOHS ((UINT2) (u2TxDataRate >> RADIO_VALUE_3)));

            if (MEMCMP
                (pWssMsgStruct->unRadioIfMsg.
                 RadioIfConfigStatusReq.RadioIfInfoElem.unInfoElem.
                 VHTCapability.au1SuppMCS,
                 pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.au1VhtCapaMcs,
                 DOT11AC_VHT_CAP_MCS_LEN) != 0)
            {
                pRadioIfGetDB->RadioIfIsGetAllDB.bVhtCapaMcs = OSIX_FALSE;
                MEMCPY (&u2SuppRxMcs,
                        (pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.
                         RadioIfInfoElem.unInfoElem.VHTCapability.au1SuppMCS),
                        DOT11AC_MCS_MAP);
                MEMCPY (&u2SuppTxMcs,
                        &(pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusReq.
                          RadioIfInfoElem.unInfoElem.VHTCapability.
                          au1SuppMCS[DOT11AC_MCS_MAP + DOT11AC_MCS_MAP]),
                        DOT11AC_MCS_MAP);
                /*Computation of STS */
                u2McsSts = (u2SuppRxMcs & u2SuppTxMcs);
                for (u1Loop = RADIO_VALUE_0; u1Loop <
                     RADIO_11AC_VHT_CAPA_MCS_LEN; u1Loop++)
                {
                    u1Mcs = (UINT2) (u2McsSts & RADIO_11AC_VHT_MCS_SHIFT);
                    if (u1Mcs == RADIO_VHT_MCS_DISABLE)
                    {
                        u1STS = u1Loop;
                        break;
                    }
                    u2McsSts = (u2McsSts >> RADIO_11AC_VHT_MCS_SHIFT_BITS);
                }
                pRadioIfGetDB->RadioIfIsGetAllDB.bSuppVhtSTS = OSIX_TRUE;
                pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.
                    u1SuppVhtSTS = u1STS;

                /*Compution of Rx MCS */
                MEMCPY (&u2RxMcs,
                        (pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.
                         au1VhtCapaMcs), DOT11AC_MCS_MAP);

                u2Shift = RADIO_11AC_VHT_MCS_SHIFT;

                for (u1Loop = 0; u1Loop < RADIO_11AC_VHT_CAPA_MCS_LEN; u1Loop++)
                {
                    u2CtrlMcs = u2RxMcs & u2Shift;
                    u2SuppMcs = u2SuppRxMcs & u2Shift;

                    /*If the any Mcs is disabled in AP then it's disabled in AC */
                    if (u2SuppMcs == RADIO_VHT_MCS_DISABLE)
                    {
                        u2CtrlMcs = u2SuppMcs;
                        pRadioIfGetDB->RadioIfIsGetAllDB.bVhtCapaMcs =
                            OSIX_TRUE;
                    }
                    else if (u2CtrlMcs == RADIO_VHT_MCS_DISABLE)
                    {
                        u1MsgFlag = OSIX_TRUE;
                    }
                    else
                    {
                        /*If any configured control variable is 
                         *            greator than the supported value, then 
                         *            configured value is reduced to supported 
                         *            value.
                         *            Else message is contructed */
                        if (u2SuppMcs < u2CtrlMcs)
                        {
                            u2CtrlMcs = u2SuppMcs;
                            pRadioIfGetDB->RadioIfIsGetAllDB.bVhtCapaMcs =
                                OSIX_TRUE;
                        }
                        else
                        {
                            u1MsgFlag = OSIX_TRUE;
                        }
                    }
                    if (u2Mcs != 0)
                    {
                        u2Mcs = ((u2CtrlMcs << 14) | (u2Mcs >> 2));
                    }
                    else
                    {
                        u2Mcs = (u2Mcs | u2CtrlMcs);
                        u2Mcs = u2Mcs << 14;
                    }

                    u2RxMcs =
                        (UINT2) (u2RxMcs >> RADIO_11AC_VHT_MCS_SHIFT_BITS);
                    u2SuppRxMcs =
                        (UINT2) (u2SuppRxMcs >> RADIO_11AC_VHT_MCS_SHIFT_BITS);

                }

                u2SuppRxMcs = OSIX_NTOHS (u2Mcs);
                MEMCPY ((pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.
                         au1VhtCapaMcs), &u2Mcs, DOT11AC_MCS_MAP);

                /*Compution of Tx MCS */
                MEMCPY (&u2TxMcs,
                        &(pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.
                          au1VhtCapaMcs[DOT11AC_MCS_MAP + DOT11AC_MCS_MAP]),
                        DOT11AC_MCS_MAP);

                u2Shift = RADIO_11AC_VHT_MCS_SHIFT;
                u2CtrlMcs = 0;    /*Re-initializing the variables */
                u2SuppMcs = 0;
                u2Mcs = 0;

                for (u1Loop = 0; u1Loop < RADIO_11AC_VHT_CAPA_MCS_LEN; u1Loop++)
                {
                    u2CtrlMcs = u2TxMcs & u2Shift;
                    u2SuppMcs = u2SuppTxMcs & u2Shift;

                    /*If the any Mcs is disabled in AP then it's
                     * disabled in AC*/
                    if (u2SuppMcs == RADIO_VHT_MCS_DISABLE)
                    {
                        u2CtrlMcs = u2SuppMcs;
                        pRadioIfGetDB->RadioIfIsGetAllDB.bVhtCapaMcs =
                            OSIX_TRUE;
                    }
                    else if (u2CtrlMcs == RADIO_VHT_MCS_DISABLE)
                    {
                        u1MsgFlag = OSIX_TRUE;
                    }
                    else
                    {
                        /*If any configured control variable is 
                         *            greater than the supported value, then 
                         *            configured value is reduced to supported
                         *            value.
                         *            Else message is contructed */
                        if (u2SuppMcs < u2CtrlMcs)
                        {
                            u2CtrlMcs = u2SuppMcs;
                            pRadioIfGetDB->RadioIfIsGetAllDB.bVhtCapaMcs =
                                OSIX_TRUE;
                        }
                        else
                        {
                            u1MsgFlag = OSIX_TRUE;
                        }
                    }

                    if (u2Mcs != 0)
                    {
                        u2Mcs = ((u2CtrlMcs << 14) | (u2Mcs >> 2));
                    }
                    else
                    {
                        u2Mcs = (u2Mcs | u2CtrlMcs);
                        u2Mcs = u2Mcs << 14;
                    }

                    u2TxMcs =
                        (UINT2) (u2TxMcs >> RADIO_11AC_VHT_MCS_SHIFT_BITS);
                    u2SuppTxMcs =
                        (UINT2) (u2SuppTxMcs >> RADIO_11AC_VHT_MCS_SHIFT_BITS);
                }
                u2SuppTxMcs = OSIX_NTOHS (u2Mcs);
                MEMCPY (&(pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.
                          au1VhtCapaMcs[DOT11AC_MCS_MAP + DOT11AC_MCS_MAP]),
                        &u2Mcs, DOT11AC_MCS_MAP);

                /* Configuring Basic MCS set in Vht Operation */
                pRadioIfGetDB->RadioIfIsGetAllDB.bVhtOperMcsSet = OSIX_TRUE;
                pRadioIfGetDB->RadioIfGetAllDB.Dot11AcOperParams.
                    u2VhtOperMcsSet = (u2SuppRxMcs & u2SuppTxMcs);

                /*Computation of Configured STS */
                u1Mcs = 0;
                u2McsSts =
                    OSIX_NTOHS (pRadioIfGetDB->RadioIfGetAllDB.
                                Dot11AcOperParams.u2VhtOperMcsSet);
                for (u1Loop = RADIO_VALUE_0;
                     u1Loop < RADIO_11AC_VHT_CAPA_MCS_LEN; u1Loop++)
                {
                    u1Mcs = (UINT2) (u2McsSts & RADIO_11AC_VHT_MCS_SHIFT);
                    if (u1Mcs == RADIO_VHT_MCS_DISABLE)
                    {
                        u1STS = u1Loop;
                        break;
                    }
                    u2McsSts = (u2McsSts >> RADIO_11AC_VHT_MCS_SHIFT_BITS);
                }
                pRadioIfGetDB->RadioIfIsGetAllDB.bVhtSTS = OSIX_TRUE;
                pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.
                    u1VhtSTS = u1STS;

            }

            if (u1MsgFlag == OSIX_TRUE)
            {
                /*Accomodation of 20 params in u4 variable */
                pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u4VhtCapInfo =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11AcCapaParams.u4SuppVhtCapInfo;
                if (WssIfProcessRadioIfDBMsg
                    (WSS_ASSEMBLE_11AC_CAPABILITY_INFO,
                     (&RadioIfGetDB)) != OSIX_SUCCESS)
                {
                    RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                                 "RadioIfProcess11AcStatusReq: Parsing u4VhtCapInfo"
                                 "to individual elements failed\n");
                    return OSIX_FAILURE;
                }
                pWssMsgStruct->unRadioIfMsg.
                    RadioIfConfigStatusReq.RadioIfInfoElem.isPresent |=
                    OSIX_TRUE;
                pWssMsgStruct->unRadioIfMsg.
                    RadioIfConfigStatusReq.RadioIfInfoElem.unInfoElem.
                    VHTCapability.isOptional |= OSIX_TRUE;
                /* Supported value WLC value is less than the Supp WTP value.
                   So response message constructed
                 */

                pRadioIfGetDB->RadioIfIsGetAllDB.bVhtCapInfo = OSIX_TRUE;
                pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u4VhtCapInfo
                    =
                    RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u4VhtCapInfo;
            }
            break;

        case DOT11AC_INFO_ELEM_VHTOPERATION:
            /*VHT Operation */

            MEMCPY (RadioIfGetDB.RadioIfGetAllDB.
                    Dot11AcOperParams.au1VhtOperInfo,
                    pWssMsgStruct->unRadioIfMsg.
                    RadioIfConfigStatusReq.RadioIfInfoElem.unInfoElem.
                    VHTOperation.au1VhtOperInfo, DOT11AC_VHT_OPER_INFO_LEN);

            /*Processing of VHT Operation elements */
            if (WssIfProcessRadioIfDBMsg (WSS_PARSE_11AC_OPER_INFO,
                                          (&RadioIfGetDB)) != OSIX_SUCCESS)
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                             "RadioIfProcess11AcStatusReq: Parsing au1VhtOperInfo"
                             "to individual elements failed\n");
                return OSIX_FAILURE;
            }

            pRadioIfGetDB->RadioIfIsGetAllDB.bVhtChannelWidth = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bCenterFcy0 = OSIX_TRUE;

            if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11AC,
                                          (pRadioIfGetDB)) != OSIX_SUCCESS)
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                             "RadioIfProcess11AcStatusReq: Get"
                             "of Radio DB for 11AC failed \n");
                return OSIX_FAILURE;
            }

            /*Duplication of pRadioIfGetDb for parsing the received Info */
            /*  MEMCPY(&RadioIfGetDB,pRadioIfGetDB,sizeof(tRadioIfGetDB)); */

            pRadioIfGetDB->RadioIfIsGetAllDB.bSuppVhtChannelWidth = OSIX_TRUE;
            pRadioIfGetDB->RadioIfGetAllDB.
                Dot11AcOperParams.u1SuppVhtChannelWidth =
                RadioIfGetDB.RadioIfGetAllDB.
                Dot11AcOperParams.u1SuppVhtChannelWidth;

            if ((pRadioIfGetDB->RadioIfGetAllDB.
                 Dot11AcOperParams.u1VhtChannelWidth) <
                (RadioIfGetDB.RadioIfGetAllDB.
                 Dot11AcOperParams.u1SuppVhtChannelWidth))
            {
                RadioIfGetDB.RadioIfGetAllDB.
                    Dot11AcOperParams.u1SuppVhtChannelWidth =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11AcOperParams.u1VhtChannelWidth;
                pRadioIfGetDB->RadioIfIsGetAllDB.bVhtChannelWidth = OSIX_FALSE;
                u1MsgFlag = OSIX_TRUE;
            }
            else
            {
                /*Only Db updation */
                pRadioIfGetDB->RadioIfIsGetAllDB.bVhtChannelWidth = OSIX_TRUE;
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11AcOperParams.u1VhtChannelWidth =
                    RadioIfGetDB.RadioIfGetAllDB.
                    Dot11AcOperParams.u1SuppVhtChannelWidth;
            }

            pRadioIfGetDB->RadioIfIsGetAllDB.bSuppCenterFcy0 = OSIX_TRUE;
            pRadioIfGetDB->RadioIfGetAllDB.Dot11AcOperParams.u1SuppCenterFcy0 =
                RadioIfGetDB.RadioIfGetAllDB.Dot11AcOperParams.u1SuppCenterFcy0;

            if ((pRadioIfGetDB->RadioIfGetAllDB.Dot11AcOperParams.u1CenterFcy0)
                <
                (RadioIfGetDB.RadioIfGetAllDB.
                 Dot11AcOperParams.u1SuppCenterFcy0))
            {
                RadioIfGetDB.RadioIfGetAllDB.
                    Dot11AcOperParams.u1SuppCenterFcy0 =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11AcOperParams.u1CenterFcy0;
                pRadioIfGetDB->RadioIfIsGetAllDB.bCenterFcy0 = OSIX_FALSE;
                u1MsgFlag = OSIX_TRUE;
            }
            else
            {
                /*Only Db updation */
                pRadioIfGetDB->RadioIfIsGetAllDB.bCenterFcy0 = OSIX_TRUE;
                pRadioIfGetDB->RadioIfGetAllDB.Dot11AcOperParams.u1CenterFcy0 =
                    RadioIfGetDB.RadioIfGetAllDB.Dot11AcOperParams.u1CenterFcy0;
            }

            pRadioIfGetDB->RadioIfIsGetAllDB.bCenterFcy1 = OSIX_TRUE;
            pRadioIfGetDB->RadioIfGetAllDB.Dot11AcOperParams.u1CenterFcy1 =
                RadioIfGetDB.RadioIfGetAllDB.Dot11AcOperParams.u1CenterFcy1;

            pRadioIfGetDB->RadioIfIsGetAllDB.bVhtOperMcsSet = OSIX_TRUE;
            pRadioIfGetDB->RadioIfGetAllDB.Dot11AcOperParams.u2VhtOperMcsSet =
                pWssMsgStruct->unRadioIfMsg.
                RadioIfConfigStatusReq.RadioIfInfoElem.unInfoElem.VHTOperation.
                u2BasicMCS;

            if (u1MsgFlag == OSIX_TRUE)
            {
                /*Accomodation of 20 params in u4 variable */

                if (WssIfProcessRadioIfDBMsg (WSS_ASSEMBLE_11AC_OPER_INFO,
                                              (&RadioIfGetDB)) != OSIX_SUCCESS)
                {
                    RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                                 "RadioIfProcess11AcStatusReq: Parsing u4VhtCapInfo"
                                 "to individual elements failed\n");
                    return OSIX_FAILURE;
                }
                pWssMsgStruct->unRadioIfMsg.
                    RadioIfConfigStatusReq.RadioIfInfoElem.isPresent |=
                    OSIX_TRUE;
                pWssMsgStruct->unRadioIfMsg.
                    RadioIfConfigStatusReq.RadioIfInfoElem.unInfoElem.
                    VHTOperation.isOptional |= OSIX_TRUE;

                /*Supported value WLC value is less than the Supp WTP value.
                 *so response message constructed
                 **/

                pRadioIfGetDB->RadioIfIsGetAllDB.bVhtOperInfo = OSIX_TRUE;
                MEMCPY ((pRadioIfGetDB->RadioIfGetAllDB.
                         Dot11AcOperParams.au1VhtOperInfo),
                        (RadioIfGetDB.RadioIfGetAllDB.
                         Dot11AcOperParams.au1VhtOperInfo),
                        DOT11AC_VHT_OPER_INFO_LEN);
            }
            break;

        default:
            break;
    }

    if (WssIfProcessRadioIfDBMsg (WSS_SET_RADIO_IF_DB_11AC, (pRadioIfGetDB))
        != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfConfigStatusReq: Set of Radio DB for 11AC failed \r\n");
        return OSIX_FAILURE;
    }
/*Computation of Highest Configured Data Rate*/
    pRadioIfGetDB->RadioIfIsGetAllDB.bVhtChannelWidth = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bShortGi80 = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bVhtCapaMcs = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11AC, (pRadioIfGetDB))
        != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfConfigStatusReq: Get of Radio DB"
                     " for 11AC failed \r\n");
        return OSIX_FAILURE;
    }
    pRadioIfGetDB->RadioIfIsGetAllDB.bChannelWidth = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, (pRadioIfGetDB))
        != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfConfigStatusReq: Get of Radio DB"
                     " for 11AC failed \r\n");
        return OSIX_FAILURE;
    }
    pRadioIfGetDB->RadioIfIsGetAllDB.bVhtChannelWidth = OSIX_FALSE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bShortGi80 = OSIX_FALSE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bVhtCapaMcs = OSIX_FALSE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bChannelWidth = OSIX_FALSE;

    WssIfVhtHighestSuppDataRate (pRadioIfGetDB);
    if (WssIfProcessRadioIfDBMsg (WSS_SET_RADIO_IF_DB_11AC, (pRadioIfGetDB))
        != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfConfigStatusReq: Set of Radio DB for 11AC failed \r\n");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfSetFsDot11AcCapaParams                        *
 *                                                                           *
 * Description        : This function constructs the 802.11ac capability     *
 *                         parameters and sends it to WLC Handler and update    * 
 *                         the WSSIF DB in case the configuration is            * 
 *                         succesfully applied in WTP                              *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to Radio IF DB with ifIndex  *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfSetDot11AcCapaParams (tRadioIfGetDB * pRadioIfGetDB)
{
    tRadioIfGetDB       RadioIfGetDB;
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;
    INT4                i4RetVal = 0;
    UINT1               u1IsValChanged = 0;
    UINT2               u2SuppRxMcs = 0;
    UINT2               u2SuppTxMcs = 0;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    if (pRadioIfGetDB == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetDot11AcCapaParams : Null Input received \r\n");
        return OSIX_FAILURE;
    }

    pWlcHdlrMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pWlcHdlrMsgStruct == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetDot11AcCapaParams:- "
                     "UtlShMemAllocWlcBuf returned failure\n");
        return OSIX_FAILURE;
    }

    MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
        pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;

    /*Filling the booleans to get 11Ac params from DB */
    MEMSET (&(RadioIfGetDB.RadioIfIsGetAllDB), 1,
            sizeof (RadioIfGetDB.RadioIfIsGetAllDB));

    /* Get the Radio IF DB info to fill the structure  */
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11AC,
                                  &RadioIfGetDB) != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetDot11AcCapaParams:"
                     "RadioIf DB access failed. \r\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;
    }
    /*Resetting the booleans */
    MEMSET (&(RadioIfGetDB.RadioIfIsGetAllDB), 0,
            sizeof (RadioIfGetDB.RadioIfIsGetAllDB));

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bMaxMPDULen != OSIX_FALSE)
    {
        if ((pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1MaxMPDULen) !=
            (RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1MaxMPDULen))
        {
            u1IsValChanged = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bMaxMPDULen = OSIX_TRUE;
            /*Using Supp variables only VhtCapInfo will be assembled */
            RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1SuppMaxMPDULen =
                pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1MaxMPDULen;
        }
    }
    else
    {
        /*Using Supp variables only VhtCapInfo will be assembled */
        RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1SuppMaxMPDULen =
            RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1MaxMPDULen;
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bRxLpdc != OSIX_FALSE)
    {
        if ((pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1RxLpdc) !=
            (RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1RxLpdc))
        {
            u1IsValChanged = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bRxLpdc = OSIX_TRUE;
            /*Using Supp variables only VhtCapInfo will be assembled */
            RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1SuppRxLpdc =
                pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1RxLpdc;
        }
    }
    else
    {
        /*Using Supp variables only VhtCapInfo will be assembled */
        RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1SuppRxLpdc =
            RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1RxLpdc;
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bShortGi80 != OSIX_FALSE)
    {
        if ((pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1ShortGi80) !=
            (RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1ShortGi80))
        {
            u1IsValChanged = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bShortGi80 = OSIX_TRUE;
            /*Using Supp variables only VhtCapInfo will be assembled */
            RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1SuppShortGi80 =
                pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1ShortGi80;
        }
    }
    else
    {
        /*Using Supp variables only VhtCapInfo will be assembled */
        RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1SuppShortGi80 =
            RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1ShortGi80;
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bTxStbc != OSIX_FALSE)
    {
        if ((pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1TxStbc) !=
            (RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1TxStbc))
        {
            u1IsValChanged = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bTxStbc = OSIX_TRUE;
            /*Using Supp variables only VhtCapInfo will be assembled */
            RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1SuppTxStbc =
                pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1TxStbc;
        }
    }
    else
    {
        /*Using Supp variables only VhtCapInfo will be assembled */
        RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1SuppTxStbc =
            RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1TxStbc;
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bRxStbc != OSIX_FALSE)
    {
        if ((pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1RxStbc) !=
            (RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1RxStbc))
        {
            u1IsValChanged = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bRxStbc = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bRxStbcStatus = OSIX_TRUE;
            /*Using Supp variables only VhtCapInfo will be assembled */
            RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1SuppRxStbc =
                pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1RxStbc;
        }
    }
    else
    {
        /*Using Supp variables only VhtCapInfo will be assembled */
        RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1SuppRxStbc =
            RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1RxStbc;
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bVhtMaxAMPDU != OSIX_FALSE)
    {
        if ((pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1VhtMaxAMPDU) !=
            (RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1VhtMaxAMPDU))
        {
            u1IsValChanged = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bVhtMaxAMPDU = OSIX_TRUE;
            /*Using Supp variables only VhtCapInfo will be assembled */
            RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1SuppVhtMaxAMPDU =
                pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1VhtMaxAMPDU;
        }
    }
    else
    {
        /*Using Supp variables only VhtCapInfo will be assembled */
        RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1SuppVhtMaxAMPDU =
            RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1VhtMaxAMPDU;
    }

    if (WssIfProcessRadioIfDBMsg (WSS_ASSEMBLE_11AC_CAPABILITY_INFO,
                                  &RadioIfGetDB) != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetDot11AcCapaParams:"
                     "Failed to assemble capability params\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;
    }

    if ((pRadioIfGetDB->RadioIfIsGetAllDB.bVhtCapaMcs != OSIX_FALSE) ||
        (pRadioIfGetDB->RadioIfIsGetAllDB.bVhtSTS != OSIX_FALSE))
    {
        if (MEMCMP
            ((pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.au1VhtCapaMcs),
             (RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.au1VhtCapaMcs),
             DOT11AC_VHT_CAP_MCS_LEN) != 0)
        {
            u1IsValChanged = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bVhtCapaMcs = OSIX_TRUE;
            MEMCPY ((RadioIfGetDB.RadioIfGetAllDB.
                     Dot11AcCapaParams.au1VhtCapaMcs),
                    (pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.
                     au1VhtCapaMcs), DOT11AC_VHT_CAP_MCS_LEN);

            MEMCPY (&u2SuppRxMcs,
                    (pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.
                     au1VhtCapaMcs), DOT11AC_MCS_MAP);
            MEMCPY (&u2SuppTxMcs,
                    &(pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.
                      au1VhtCapaMcs[DOT11AC_MCS_MAP + DOT11AC_MCS_MAP]),
                    DOT11AC_MCS_MAP);

            pRadioIfGetDB->RadioIfIsGetAllDB.bVhtOperMcsSet = OSIX_TRUE;
            pRadioIfGetDB->RadioIfGetAllDB.Dot11AcOperParams.
                u2VhtOperMcsSet = (u2SuppRxMcs & u2SuppTxMcs);
/*Computation of Highest Configured Data Rate*/
            WssIfVhtHighestSuppDataRate (&RadioIfGetDB);
        }
    }

    /*If the configured value differ from the DB value, then WssifRadioIfDB 
     * updation takes place*/
    if (u1IsValChanged == OSIX_FALSE)
    {
        RADIOIF_TRC (RADIOIF_INFO_TRC,
                     "RadioIfSetDot11AcCapaParams : No Value Change, "
                     "Not triggering Config Update \r\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_SUCCESS;
    }

    pRadioIfGetDB->RadioIfIsGetAllDB.bVhtCapInfo = OSIX_TRUE;
    pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u4VhtCapInfo =
        RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u4VhtCapInfo;

    /* Get the Radio IF DB info to fill RadioId and BPFlag structure  */
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bBPFlag = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;

    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                  &RadioIfGetDB) != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetDot11AcCapaParams : RadioIf DB access failed. \r\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;

    }
    /* VHT Capability Information Element in Config Update Request */
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfInfoElem.u1RadioId =
        RadioIfGetDB.RadioIfGetAllDB.u1RadioId;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfInfoElem.u1WlanId =
        RADIO_VALUE_1;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfInfoElem.u1BPFlag =
        RadioIfGetDB.RadioIfGetAllDB.u1BPFlag;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.u2WtpInternalId
        = RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfInfoElem.u2MessageType =
        RADIO_INFO_ELEM_MSG_TYPE;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfInfoElem.u2MessageLength =
        RADIO_INFO_ELEM_FIXED_MSG_LEN + DOT11AC_INFO_VHT_CAP_MSG_LEN;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfInfoElem.isPresent =
        OSIX_TRUE;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfInfoElem.u1EleId =
        DOT11AC_INFO_ELEM_VHTCAPABILITY;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfInfoElem.unInfoElem.
        VHTCapability.u1ElemLen = DOT11AC_INFO_VHT_CAP_MSG_LEN;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfInfoElem.unInfoElem.
        VHTCapability.u1ElemId = DOT11AC_INFO_ELEM_VHTCAPABILITY;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfInfoElem.unInfoElem.
        VHTCapability.isOptional = OSIX_TRUE;

    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfInfoElem.unInfoElem.
        VHTCapability.u4VhtCapInfo =
        pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u4VhtCapInfo;

    MEMCPY (pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
            RadioIfInfoElem.unInfoElem.VHTCapability.au1SuppMCS,
            RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.au1VhtCapaMcs,
            DOT11AC_VHT_CAP_MCS_LEN);

    /* Invoke WLC Handler to send the 
     * config update request for VHT Capability Information Element*/
    i4RetVal = WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_RADIO_CONF_UPDATE_REQ,
                                       pWlcHdlrMsgStruct);

    /* Invoke WLC Handler to send the config update request */
    if (i4RetVal != OSIX_SUCCESS)
    {
        if (i4RetVal == RADIOIF_NO_AP_PRESENT)
        {
            gu1IsConfigResponseReceived = RADIOIF_NO_AP_PRESENT;
        }
        else
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfSetDot11AcCapaParams : Mac Operation failed \r\n");
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            return OSIX_FAILURE;
        }
    }
    if (WssIfProcessRadioIfDBMsg (WSS_SET_RADIO_IF_DB_11AC,
                                  pRadioIfGetDB) != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetDot11AcCapaParams: Radio DB Update failure\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;
    }
    /*During run state all the station should be disassociated, 
     * when any radio paramter is changed*/
    if (i4RetVal != RADIOIF_NO_AP_PRESENT)
    {
        if (RadioIfDisassocStation (&RadioIfGetDB) != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfSetFsDot11nParams: cannot disassoc the stations\n");
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            return OSIX_FAILURE;

        }
    }

    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);

    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfSetFsDot11AcOperParams                        *
 *                                                                           *
 * Description        : This function constructs the 802.11ac capability     *
 *             parameters and sends it to WLC Handler and update                * 
 *             the WSSIF DB in case the configuration is                        * 
 *             succesfully applied in WTP                                          *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to Radio IF DB with ifIndex  *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfSetDot11AcOperParams (tRadioIfGetDB * pRadioIfGetDB)
{
    tRadioIfGetDB       RadioIfGetDB;
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;
    INT4                i4RetVal = 0;
    UINT1               u1IsValChanged = 0;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    if (pRadioIfGetDB == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetDot11AcOperParams: Null Input received \r\n");
        return OSIX_FAILURE;
    }

    pWlcHdlrMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pWlcHdlrMsgStruct == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetDot11AcOperParams:- "
                     "UtlShMemAllocWlcBuf returned failure\n");
        return OSIX_FAILURE;
    }

    MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
        pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;

    /*Filling the booleans to get 11Ac params from DB */
    MEMSET (&(RadioIfGetDB.RadioIfIsGetAllDB), 1,
            sizeof (RadioIfGetDB.RadioIfIsGetAllDB));

    /* Get the Radio IF DB info to fill the structure  */
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11AC,
                                  &RadioIfGetDB) != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetDot11AcOperParams:"
                     "RadioIf DB access failed. \r\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;

    }
    /*Resetting the booleans */
    MEMSET (&(RadioIfGetDB.RadioIfIsGetAllDB), 0,
            sizeof (RadioIfGetDB.RadioIfIsGetAllDB));

    RadioIfGetDB.RadioIfIsGetAllDB.bChannelWidth = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N,
                                  &RadioIfGetDB) != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetDot11AcOperParams: "
                     "RadioIf DB access failed for 11n " "Channel width \r\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;

    }
    RadioIfGetDB.RadioIfIsGetAllDB.bChannelWidth = OSIX_FALSE;

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bVhtChannelWidth != OSIX_FALSE)
    {
        if ((pRadioIfGetDB->RadioIfGetAllDB.
             Dot11AcOperParams.u1VhtChannelWidth) !=
            (RadioIfGetDB.RadioIfGetAllDB.Dot11AcOperParams.u1VhtChannelWidth))
        {
            u1IsValChanged = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bVhtChannelWidth = OSIX_TRUE;
            /*Using Supp variables only VhtOperInfo will be assembled */
            RadioIfGetDB.RadioIfGetAllDB.Dot11AcOperParams.
                u1SuppVhtChannelWidth =
                pRadioIfGetDB->RadioIfGetAllDB.Dot11AcOperParams.
                u1VhtChannelWidth;
            pRadioIfGetDB->RadioIfIsGetAllDB.bChannelWidth = OSIX_TRUE;
            if ((pRadioIfGetDB->RadioIfGetAllDB.Dot11AcOperParams.
                 u1VhtChannelWidth == RADIO_VALUE_0) &&
                (RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.
                 u1ChannelWidth == RADIO_VALUE_1))
            {
                RADIOIF_TRC (RADIOIF_MGMT_TRC,
                             "Moving to 40MHz bandwidth \r\n");
            }
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.
                u1ChannelWidth = RADIO_VALUE_1;
            pRadioIfGetDB->RadioIfIsGetAllDB.bChannelWidth = OSIX_TRUE;
            if ((RadioIfSetFsDot11nParams (pRadioIfGetDB)) != OSIX_SUCCESS)
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                             "RadioIfSetDot11AcCapaParams:"
                             "Failed to set channel width 40 in "
                             "RadioIfSetFsDot11nParams \r\n");
                UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                return OSIX_FAILURE;

            }

        }
    }
    else
    {
        /*Using Supp variables only VhtCapInfo will be assembled */
        RadioIfGetDB.RadioIfGetAllDB.Dot11AcOperParams.u1SuppVhtChannelWidth =
            RadioIfGetDB.RadioIfGetAllDB.Dot11AcOperParams.u1VhtChannelWidth;
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bCenterFcy0 != OSIX_FALSE)
    {
        if ((pRadioIfGetDB->RadioIfGetAllDB.Dot11AcOperParams.u1CenterFcy0) !=
            (RadioIfGetDB.RadioIfGetAllDB.Dot11AcOperParams.u1CenterFcy0))
        {
            u1IsValChanged = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bCenterFcy0 = OSIX_TRUE;
            /*Using Supp variables only VhtOperInfo will be assembled */
            RadioIfGetDB.RadioIfGetAllDB.Dot11AcOperParams.u1SuppCenterFcy0 =
                pRadioIfGetDB->RadioIfGetAllDB.Dot11AcOperParams.u1CenterFcy0;
        }
    }
    else
    {
        /*Using Supp variables only VhtCapInfo will be assembled */
        RadioIfGetDB.RadioIfGetAllDB.Dot11AcOperParams.u1SuppCenterFcy0 =
            RadioIfGetDB.RadioIfGetAllDB.Dot11AcOperParams.u1CenterFcy0;
    }

    /*If the configured value differ from the DB value, then WssifRadioIfDB 
     * updation
     *takes place*/
    if (u1IsValChanged == OSIX_FALSE)
    {
        RADIOIF_TRC (RADIOIF_INFO_TRC,
                     "RadioIfSetDot11AcOperParams : No Value Change, "
                     "Not triggering Config Update \r\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_SUCCESS;
    }

    if (WssIfProcessRadioIfDBMsg (WSS_ASSEMBLE_11AC_OPER_INFO,
                                  &RadioIfGetDB) != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetDot11AcCapaParams:"
                     "Failed to assemble capability params\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;
    }
    pRadioIfGetDB->RadioIfIsGetAllDB.bVhtOperInfo = OSIX_TRUE;
    MEMCPY (pRadioIfGetDB->RadioIfGetAllDB.Dot11AcOperParams.au1VhtOperInfo,
            RadioIfGetDB.RadioIfGetAllDB.Dot11AcOperParams.au1VhtOperInfo,
            DOT11AC_VHT_OPER_INFO_LEN);

    /* Get the Radio IF DB info to fill RadioId and BPFlag structure  */
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bBPFlag = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;

    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                  &RadioIfGetDB) != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetDot11AcOperParams : RadioIf DB access failed. \r\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;

    }

    /* VHT Opearion Information Element in Config Update Request */
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfInfoElem.u1RadioId =
        RadioIfGetDB.RadioIfGetAllDB.u1RadioId;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfInfoElem.u1WlanId =
        RADIO_VALUE_1;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfInfoElem.u1BPFlag =
        RadioIfGetDB.RadioIfGetAllDB.u1BPFlag;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.u2WtpInternalId
        = RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfInfoElem.u2MessageType =
        RADIO_INFO_ELEM_MSG_TYPE;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfInfoElem.u2MessageLength =
        RADIO_INFO_ELEM_FIXED_MSG_LEN + DOT11AC_INFO_VHT_OPER_MSG_LEN;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfInfoElem.isPresent =
        OSIX_TRUE;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfInfoElem.u1EleId =
        DOT11AC_INFO_ELEM_VHTOPERATION;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfInfoElem.unInfoElem.
        VHTOperation.u1ElemLen = DOT11AC_INFO_VHT_OPER_MSG_LEN;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfInfoElem.unInfoElem.
        VHTOperation.u1ElemId = DOT11AC_INFO_ELEM_VHTOPERATION;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfInfoElem.unInfoElem.
        VHTOperation.isOptional = OSIX_TRUE;

    MEMCPY (pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
            RadioIfInfoElem.unInfoElem.VHTOperation.au1VhtOperInfo,
            pRadioIfGetDB->RadioIfGetAllDB.Dot11AcOperParams.au1VhtOperInfo,
            DOT11AC_VHT_OPER_INFO_LEN);

    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfInfoElem.unInfoElem.
        VHTOperation.u2BasicMCS =
        RadioIfGetDB.RadioIfGetAllDB.Dot11AcOperParams.u2VhtOperMcsSet;

    /* Invoke WLC Handler to send the 
     * config update request for VHT Operation Information Element*/
    i4RetVal = WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_RADIO_CONF_UPDATE_REQ,
                                       pWlcHdlrMsgStruct);

    /* Invoke WLC Handler to send the config update request */
    if (i4RetVal != OSIX_SUCCESS)
    {
        if (i4RetVal == RADIOIF_NO_AP_PRESENT)
        {
            gu1IsConfigResponseReceived = RADIOIF_NO_AP_PRESENT;
        }
        else
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfSetDot11AcOperParams:"
                         "Mac Operation failed \r\n");
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            return OSIX_FAILURE;
        }
    }
    if (WssIfProcessRadioIfDBMsg (WSS_SET_RADIO_IF_DB_11AC,
                                  pRadioIfGetDB) != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetDot11AcOperParams:"
                     "Radio DB Update failure\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;
    }
    /*During run state all the station should be disassociated, 
     * when any radio paramter is changed*/
    if (i4RetVal != RADIOIF_NO_AP_PRESENT)
    {
        if (RadioIfDisassocStation (&RadioIfGetDB) != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfSetFsDot11nParams: cannot disassoc the stations\n");
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            return OSIX_FAILURE;

        }
    }
    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);

    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfDeriveTxPower                                 *
 *                                                                           *
 * Description        : This function setx the maximum transmit power and    *
 *                      current Tx power for the configured channel          * 
 *                                                                           *
 * Input(s)           : u4RadioIfIndex - Radio interface index               *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfDeriveTxPower (UINT4 u4RadioIfIndex)
{
    tRadioIfGetDB       RadioIfGetDB;
#ifdef RFMGMT_WANTED
    tRfMgmtMsgStruct    RfMgmtMsgStruct;
#endif
    UINT2               u2MaxTxPower = 0;
    UINT1               u1MultiIndex = 0;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
#ifdef RFMGMT_WANTED
    MEMSET (&RfMgmtMsgStruct, 0, sizeof (tRfMgmtMsgStruct));
#endif

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = u4RadioIfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bMultiDomainInfo = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bCurrentChannel = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bCurrentTxPowerLevel = OSIX_TRUE;

    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                  &RadioIfGetDB) != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfDeriveTxPower: RadioIf DB access failed. \r\n");
        return OSIX_FAILURE;
    }
    RadioIfGetDB.RadioIfIsGetAllDB.bCurrentChannel = OSIX_FALSE;
    RadioIfGetDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_FALSE;
    RadioIfGetDB.RadioIfIsGetAllDB.bMultiDomainInfo = OSIX_FALSE;
    RadioIfGetDB.RadioIfIsGetAllDB.bCurrentTxPowerLevel = OSIX_FALSE;

    /* Get the Max Tx power level for the current channel from the multi 
     * domain capability information */
    for (u1MultiIndex = 0; u1MultiIndex < MAX_MULTIDOMAIN_INFO_ELEMENT;
         u1MultiIndex++)
    {
        if ((RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType == RADIO_TYPE_B) ||
            (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType == RADIO_TYPE_G) ||
            (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType == RADIO_TYPE_BG) ||
            (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType == RADIO_TYPE_BGN))
        {
            if (RadioIfGetDB.RadioIfGetAllDB.u1CurrentChannel <
                RadioIfGetDB.RadioIfGetAllDB.MultiDomainInfo[u1MultiIndex].
                u2NoOfChannels)
            {
                RadioIfGetDB.RadioIfGetAllDB.u2MaxTxPowerLevel =
                    RadioIfGetDB.RadioIfGetAllDB.
                    MultiDomainInfo[u1MultiIndex].u2MaxTxPowerLevel;
                break;
            }

        }
        else
        {
            if ((RadioIfGetDB.RadioIfGetAllDB.u1CurrentChannel >=
                 RadioIfGetDB.RadioIfGetAllDB.MultiDomainInfo[u1MultiIndex].
                 u2FirstChannelNo))
            {
                if (((UINT1) (u1MultiIndex + 1)) < MAX_MULTIDOMAIN_INFO_ELEMENT)
                {
                    if ((RadioIfGetDB.RadioIfGetAllDB.u1CurrentChannel <
                         RadioIfGetDB.RadioIfGetAllDB.
                         MultiDomainInfo[u1MultiIndex + 1].u2FirstChannelNo)
                        || (RadioIfGetDB.RadioIfGetAllDB.
                            MultiDomainInfo[u1MultiIndex +
                                            1].u2FirstChannelNo == 0))
                    {
                        RadioIfGetDB.RadioIfGetAllDB.u2MaxTxPowerLevel =
                            RadioIfGetDB.RadioIfGetAllDB.
                            MultiDomainInfo[u1MultiIndex].u2MaxTxPowerLevel;
                        break;
                    }
                }
            }
        }

    }

    /* With Max Tx power and power level available, determine 
     * the current Tx Power */

    u2MaxTxPower = RadioIfGetDB.RadioIfGetAllDB.u2MaxTxPowerLevel;

    if (u2MaxTxPower != 0)
    {
        /*  The current Tx Power will be 0 until Max Tx power level is not 
         *  received from AP and user not configured the value in CLI/SNMP */
        if (RadioIfGetDB.RadioIfGetAllDB.u2CurrentTxPowerLevel == 0)
        {
            RadioIfGetDB.RadioIfGetAllDB.u2CurrentTxPowerLevel = 1;
        }

        RadioIfGetDB.RadioIfIsGetAllDB.bMaxTxPowerLevel = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bCurrentTxPower = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bCurrentTxPowerLevel = OSIX_TRUE;

        /* RF module should be called only if Current Tx Power is received from WTP
         * and no value is configured from CLI/SNMP */
#ifdef RFMGMT_WANTED
        RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtIntfConfigReq.u4RadioIfIndex =
            RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex;
        RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtIntfConfigReq.u2TxPower =
            RadioIfGetDB.RadioIfGetAllDB.u2CurrentTxPowerLevel;

        if (WssIfProcessRfMgmtMsg (RFMGMT_TX_POWER_UPDATE,
                                   &RfMgmtMsgStruct) != RFMGMT_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfDeriveTxPower: Update RM Mgmt DB Failed\n");
        }
        if (RadioIfSetTxPowerTable (&RadioIfGetDB) != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfDeriveTxPower: updation failed \r\n");
            return OSIX_FAILURE;
        }
#endif
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfValidateAllowedChannels                       *
 *                                                                           *
 * Description        : This function validates the configured channel       * 
 *                                                                           *
 * Input(s)           : u4RadioIfIndex - Radio interface index               *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfValidateAllowedChannels (UINT4 u4RadioIfIndex, UINT1 u1CurrentChannel,
                                UINT1 *pAllowedChannelList)
{
    tRadioIfGetDB       RadioIfGetDB;
    tRfMgmtDB           RfMgmtDB;
    UINT1               au1Dot11ChannelList[RADIOIF_MAX_CHANNEL_A];
    UINT1               au1Dot11ApChannelList[RADIOIF_MAX_CHANNEL_A];
    UINT1               u1Channel = 0;
    UINT1               u1MaxChannel = 0;
    UINT1               u1IsValPresent = OSIX_FALSE;
    UINT1               u1MultiIndex = 0;
    UINT2               u2NoOfChannels = 0;
    UINT4               u4Dot11RadioType = 0;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
    MEMSET (&au1Dot11ChannelList, 0, RADIOIF_MAX_CHANNEL_A);
    MEMSET (&au1Dot11ApChannelList, 0, RADIOIF_MAX_CHANNEL_A);

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = u4RadioIfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bMultiDomainInfo = OSIX_TRUE;

    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                  &RadioIfGetDB) != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfValidateAllowedChannels: RadioIf DB access failed. \r\n");
        return OSIX_FAILURE;
    }
    RadioIfGetDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_FALSE;
    RadioIfGetDB.RadioIfIsGetAllDB.bMultiDomainInfo = OSIX_FALSE;

    u4Dot11RadioType = RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType;

    if ((u4Dot11RadioType == RADIO_TYPE_A) ||
        (u4Dot11RadioType == RADIO_TYPE_AN) ||
        (u4Dot11RadioType == RADIO_TYPE_AC))
    {
        u1MaxChannel = RADIOIF_MAX_CHANNEL_A;
        u4Dot11RadioType = RADIO_TYPE_A;
    }
    else
    {
        u1MaxChannel = RADIOIF_MAX_CHANNEL_B;
        u4Dot11RadioType = RADIO_TYPE_B;
    }

    /* Get the allowed channel list for per AP from RF DB */

    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u4Dot11RadioType = u4Dot11RadioType;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfIsSetDB.bAllowedChannelList = OSIX_TRUE;

    if (RfMgmtProcessDBMsg (RFMGMT_GET_AUTO_RF_ENTRY, &RfMgmtDB) ==
        RFMGMT_FAILURE)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfValidateAllowedChannels: Access of Rfmgmt DB failed. \r\n");
        return OSIX_FAILURE;
    }

    MEMCPY (au1Dot11ChannelList,
            RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
            au1AllowedChannelList, u1MaxChannel);

    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigDB.u4RadioIfIndex = u4RadioIfIndex;
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigIsSetDB.bGetAllowedList = OSIX_TRUE;
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigIsSetDB.bDot11RadioType = OSIX_TRUE;

    if (RfMgmtProcessDBMsg (RFMGMT_GET_AP_CONFIG_ENTRY, &RfMgmtDB) ==
        RFMGMT_FAILURE)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfValidateAllowedChannels: Access of Rfmgmt DB failed. \r\n");
        return OSIX_FAILURE;
    }

    MEMCPY (au1Dot11ApChannelList,
            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
            au1AllowedChannelList, u1MaxChannel);

    if (RadioIfGetDB.RadioIfGetAllDB.MultiDomainInfo[0].u2FirstChannelNo != 0)
    {
        /* populating a local array with the allowed channel list of the configured country */

        for (u1MultiIndex = 0; u1MultiIndex < MAX_MULTIDOMAIN_INFO_ELEMENT;
             u1MultiIndex++)
        {
            if (RadioIfGetDB.RadioIfGetAllDB.MultiDomainInfo[u1MultiIndex].
                u2FirstChannelNo != 0)
            {
                for (u1Channel = 0; u1Channel < u1MaxChannel; u1Channel++)
                {
                    if (au1Dot11ChannelList[u1Channel] ==
                        RadioIfGetDB.RadioIfGetAllDB.
                        MultiDomainInfo[u1MultiIndex].u2FirstChannelNo)
                    {
                        u2NoOfChannels =
                            RadioIfGetDB.RadioIfGetAllDB.
                            MultiDomainInfo[u1MultiIndex].u2NoOfChannels;
                        MEMCPY (&(pAllowedChannelList[u1Channel]),
                                &au1Dot11ChannelList[u1Channel],
                                u2NoOfChannels);
                        break;
                    }
                }
            }
            else
            {
                break;
            }
        }

        /* Validating whether the configured channel is in the allowed channel list *
         * of configured country and allowed channel list of Per ap */

        for (u1Channel = 0; u1Channel < u1MaxChannel; u1Channel++)
        {
            if (u1CurrentChannel == pAllowedChannelList[u1Channel])
            {
                if (u1CurrentChannel == au1Dot11ApChannelList[u1Channel])
                {
                    u1IsValPresent = OSIX_TRUE;
                    break;
                }
            }
        }

        if (u1IsValPresent == OSIX_FALSE)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "Invalid Channel input : RadioIfValidateAllowedChannels failed. \r\n");
            return OSIX_FAILURE;
        }
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfSetAllowedChannel                            *
 *                                                                           *
 * Description        : This function sets the first available channel  if   *
 *                      RadioIfValidateAllowedChannels finction is failed    * 
 *                                                                           *
 * Input(s)           : u4RadioIfIndex - Radio interface index               *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfSetAllowedChannel (UINT4 u4RadioIfIndex, UINT1 *pAllowedChannelList)
{
    tRadioIfGetDB       RadioIfGetDB;
    tRfMgmtDB           RfMgmtDB;
    tRadioIfMsgStruct   WssMsgStruct;
    UINT1               au1Dot11ApChannelList[RADIOIF_MAX_CHANNEL_A];
    UINT1               u1Channel = 0;
    UINT1               u1CurrentChannel = 0;
    UINT1               u1MaxChannel = 0;
    UINT4               u4Dot11RadioType = 0;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    MEMSET (&WssMsgStruct, 0, sizeof (tRadioIfMsgStruct));
    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
    MEMSET (&au1Dot11ApChannelList, 0, RADIOIF_MAX_CHANNEL_A);

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = u4RadioIfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bMultiDomainInfo = OSIX_TRUE;

    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                  &RadioIfGetDB) != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetAllowedChannel: RadioIf DB access failed. \r\n");
        return OSIX_FAILURE;
    }
    RadioIfGetDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_FALSE;
    RadioIfGetDB.RadioIfIsGetAllDB.bMultiDomainInfo = OSIX_FALSE;

    u4Dot11RadioType = RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType;

    if ((u4Dot11RadioType == RADIO_TYPE_A) ||
        (u4Dot11RadioType == RADIO_TYPE_AN) ||
        (u4Dot11RadioType == RADIO_TYPE_AC))
    {
        u1MaxChannel = RADIOIF_MAX_CHANNEL_A;
    }
    else
    {
        u1MaxChannel = RADIOIF_MAX_CHANNEL_B;
    }

    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigDB.u4RadioIfIndex = u4RadioIfIndex;
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigIsSetDB.bDot11RadioType = OSIX_TRUE;
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigIsSetDB.bGetAllowedList = OSIX_TRUE;

    if (RfMgmtProcessDBMsg (RFMGMT_GET_AP_CONFIG_ENTRY, &RfMgmtDB) ==
        RFMGMT_FAILURE)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetAllowedChannel: Access of Rfmgmt DB failed. \r\n");
        return OSIX_FAILURE;
    }

    MEMCPY (au1Dot11ApChannelList,
            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
            au1AllowedChannelList, u1MaxChannel);

    for (u1Channel = 0; u1Channel < u1MaxChannel; u1Channel++)
    {
        if (UtilIsDFSChannels (pAllowedChannelList[u1Channel]) == OSIX_SUCCESS)
        {
            /*If the selected channel lies in DFS channel range,select a channel ,
             *skip this process until a assign a channel from available channels in UN11-1 and UN11-3*/
            continue;
        }

        if ((au1Dot11ApChannelList[u1Channel] != 0) &&
            (pAllowedChannelList[u1Channel] != 0))
        {
            if (au1Dot11ApChannelList[u1Channel] ==
                pAllowedChannelList[u1Channel])
            {
                RADIOIF_TRC (RADIOIF_INFO_TRC,
                             "configuring default channel of the country.\r\n");
                u1CurrentChannel = au1Dot11ApChannelList[u1Channel];

                break;
            }
        }
    }
    /*  code to be added, If DFS channels are need to assign */
    if (u1CurrentChannel != 0)
    {
        WssMsgStruct.unRadioIfMsg.RadioIfDcaTpcUpdate.u4IfIndex =
            u4RadioIfIndex;
        WssMsgStruct.unRadioIfMsg.RadioIfDcaTpcUpdate.u2CurrentChannel =
            (UINT2) u1CurrentChannel;

        if ((RadioIfUpdateChannel (&WssMsgStruct)) == OSIX_FAILURE)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfSetAllowedChannel: RadioIfUpdateChannel failed. \r\n");
            return OSIX_FAILURE;
        }

        RadioIfGetDB.RadioIfIsGetAllDB.bVhtChannelWidth = OSIX_TRUE;

        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11AC,
                                      (&RadioIfGetDB)) != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfProcess11AcStatusReq: Get"
                         "of Radio DB for 11AC failed \n");
            return OSIX_FAILURE;
        }
        RadioIfGetDB.RadioIfIsGetAllDB.bVhtChannelWidth = OSIX_FALSE;
        /*updating center frequency only for 80Mzh channel width */
        if (RadioIfGetDB.RadioIfGetAllDB.
            Dot11AcOperParams.u1VhtChannelWidth == RADIO_VALUE_1)
        {
            /*Getting center frequency */
            if ((RadioIfUpdateCenterFrequency
                 (u4RadioIfIndex, u1CurrentChannel)) == OSIX_FAILURE)
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                             "RadioIfSetAllowedChannel: RadioIfUpdateCenterFrequency failed. \r\n");
                return OSIX_FAILURE;
            }
        }
    }
    return OSIX_SUCCESS;
}

/********************************************************************************
 * Function Name      : RadioIfUpdateCenterFrequency                            *
 *                                                                              *
 * Description        : This function set the centre frequency for 11d in 11AC  *
 *                                                                              *
 * Input(s)           : u4RadioIfIndex - Radio interface index                  *
 *                      u1CurrentChannel - Current channel                      *
 * Output(s)          : None                                                    *
 *                                                                              *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds                *
 *                      OSIX_FAILURE, otherwise                                 *
 *                                                                              *
 ********************************************************************************/

UINT1
RadioIfUpdateCenterFrequency (UINT4 u4RadioIfIndex, UINT1 u1CurrentChannel)
{
    tRadioIfGetDB       RadioIfGetDB;
    UINT1               u1BaseChannel = 0;
    UINT1               u1MultiIndex = 0;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = u4RadioIfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bMultiDomainInfo = OSIX_TRUE;

    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                  &RadioIfGetDB) != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfValidateAllowedChannels: RadioIf DB access failed. \r\n");
        return OSIX_FAILURE;
    }

    RadioIfGetDB.RadioIfIsGetAllDB.bMultiDomainInfo = OSIX_FALSE;

    for (u1MultiIndex = 0; u1MultiIndex < MAX_MULTIDOMAIN_INFO_ELEMENT;
         u1MultiIndex++)
    {
        /*TO get the Base channel of the configured country*
         *checking the cutrrent channel's frequency band */
        if (((UINT1) (u1MultiIndex + 1)) < MAX_MULTIDOMAIN_INFO_ELEMENT)
        {

            if (RadioIfGetDB.RadioIfGetAllDB.MultiDomainInfo[u1MultiIndex + 1].
                u2FirstChannelNo != 0)
            {
                if (((u1CurrentChannel) >= (RadioIfGetDB.RadioIfGetAllDB.
                                            MultiDomainInfo[u1MultiIndex].
                                            u2FirstChannelNo)
                     && (u1CurrentChannel <
                         (RadioIfGetDB.RadioIfGetAllDB.
                          MultiDomainInfo[u1MultiIndex + 1].u2FirstChannelNo))))
                {
                    u1BaseChannel = RadioIfGetDB.RadioIfGetAllDB.
                        MultiDomainInfo[u1MultiIndex].u2FirstChannelNo;
                    break;
                }

            }
            else if ((u1CurrentChannel) >= (RadioIfGetDB.RadioIfGetAllDB.
                                            MultiDomainInfo[u1MultiIndex].
                                            u2FirstChannelNo))
            {
                u1BaseChannel = RadioIfGetDB.RadioIfGetAllDB.
                    MultiDomainInfo[u1MultiIndex].u2FirstChannelNo;
                break;
            }
        }
    }
    RadioIfGetDB.RadioIfIsGetAllDB.bCenterFcy0 = OSIX_TRUE;
    /*center frequency is base channel plus 6 */
    RadioIfGetDB.RadioIfGetAllDB.Dot11AcOperParams.u1CenterFcy0 =
        u1BaseChannel + RADIO_VALUE_6;
    /*updating center frequency in radio db */
    if (RadioIfSetDot11AcOperParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/*****************************************************************************
 * Function Name      : UtilIsDFSChannels                                    *
 *                                                                           *
 * Description        : This utility will check whether the channel is       *
 *                      DFS or NON-DFS                                       *
 *                                                                           *
 * Input(s)           : u1Channel                                            *
 *                                                                           *
 * Output(s)          : NIL                                                  *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/

UINT1
UtilIsDFSChannels (UINT1 u1Channel)
{
    if ((u1Channel >= DFS_CHANNEL_MIN) && (u1Channel <= DFS_CHANNEL_MAX))
    {
        return OSIX_SUCCESS;
    }
    else
    {
        return OSIX_FAILURE;
    }
}

/*****************************************************************************
 * Function Name      : RadioIfDisassocStation                               *
 *                                                                           *
 * Description        : This functions disassociates all the connected       *
 *                      stations when any radio parameter is changed         *
 *                                                                           *
 * Input(s)           : NONE                                                 *
 *                                                                           *
 * Output(s)          : NONE                                                 *
 *                                                                           *
 * Return Value(s)    : UINT1                                                *
 *                                                                           *
 *****************************************************************************/

UINT1
RadioIfDisassocStation (tRadioIfGetDB * pRadioIfGetDB)
{
    tWssifauthDBMsgStruct *pWssStaMsg = NULL;
    UINT1               u1IdCountIndex = 0;

    pWssStaMsg =
        (tWssifauthDBMsgStruct *) (VOID *) UtlShMemAllocWssIfAuthDbBuf ();
    if (pWssStaMsg == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "RadioIfDisassocStation:- "
                    "UtlShMemAllocWssIfAuthDbBuf returned failure\n");
        return OSIX_FAILURE;
    }
    MEMSET (pWssStaMsg, 0, sizeof (tWssifauthDBMsgStruct));

    for (u1IdCountIndex = WSSWLAN_START_WLANID_PER_RADIO;
         u1IdCountIndex <= WSSWLAN_END_WLANID_PER_RADIO; u1IdCountIndex++)
    {
        pRadioIfGetDB->RadioIfGetAllDB.u1WlanId = u1IdCountIndex;
        pRadioIfGetDB->RadioIfIsGetAllDB.bBssIfIndex = OSIX_TRUE;

        if (WssIfProcessRadioIfDBMsg (WSS_GET_WLAN_BSS_IFINDEX_DB,
                                      (pRadioIfGetDB)) != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_INFO_TRC,
                         "ConfigUpdateReq: No additional wlan exists\n");
            continue;
        }

        /*TO Delete the Wlans */
        pWssStaMsg->WssStaDB.u4BssIfIndex =
            pRadioIfGetDB->RadioIfGetAllDB.u4BssIfIndex;

        if (WssIfProcessWssAuthDBMsg (WSS_STA_RADIO_DEAUTH,
                                      pWssStaMsg) != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_INFO_TRC,
                         "ConfigUpdateReq: No station entry available "
                         "for this wlan\n");
            continue;
        }
    }
    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssStaMsg);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfSetFsDot11nExtParams                          *
 *                                                                           *
 * Description        : This function constructs the 802.11n Parameters and  *
 *                      sends it to WLC Handler and update the WSSIF DB in   *
 *                      case the configuration is succesfully applied in WTP *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to Radio IF DB with ifIndex  *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfSetFsDot11nExtParams (tRadioIfGetDB * pRadioIfGetDB)
{
    tRadioIfGetDB       RadioIfGetDB;
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;
    INT4                i4RetVal = 0;
    UINT1               u1IsValChanged = 0;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    if (pRadioIfGetDB == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetFsDot11nExtParams : Null Input received \r\n");
        return OSIX_FAILURE;
    }
    pWlcHdlrMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pWlcHdlrMsgStruct == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetFsDot11nExtParams:- "
                     "UtlShMemAllocWlcBuf returned failure\n");
        return OSIX_FAILURE;
    }

    MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
        pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bAMPDUStatus = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bAMPDUSubFrame = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bAMPDULimit = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bAMSDUStatus = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bAMSDULimit = OSIX_TRUE;

    /* Get the Radio IF DB info to fill the structure  */
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N,
                                  &RadioIfGetDB) != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetFsDot11nParams :"
                     " RadioIf DB access failed. \r\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;

    }
    /*Resetting the booleans */
    MEMSET (&(RadioIfGetDB.RadioIfIsGetAllDB), 0,
            sizeof (RadioIfGetDB.RadioIfIsGetAllDB));

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bAMPDUStatus != OSIX_FALSE)
    {
        if (pRadioIfGetDB->RadioIfGetAllDB.u1AMPDUStatus !=
            RadioIfGetDB.RadioIfGetAllDB.u1AMPDUStatus)
        {
            u1IsValChanged = OSIX_TRUE;
            /*Using Supp variables only HtCapInfo will be assembled */
            RadioIfGetDB.RadioIfGetAllDB.u1AMPDUStatus =
                pRadioIfGetDB->RadioIfGetAllDB.u1AMPDUStatus;
        }
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bAMPDUSubFrame != OSIX_FALSE)
    {
        if (pRadioIfGetDB->RadioIfGetAllDB.u1AMPDUSubFrame !=
            RadioIfGetDB.RadioIfGetAllDB.u1AMPDUSubFrame)
        {
            u1IsValChanged = OSIX_TRUE;
            /*Using Supp variables only HtCapInfo will be assembled */
            RadioIfGetDB.RadioIfGetAllDB.u1AMPDUSubFrame =
                pRadioIfGetDB->RadioIfGetAllDB.u1AMPDUSubFrame;
        }
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bAMPDULimit != OSIX_FALSE)
    {
        if (pRadioIfGetDB->RadioIfGetAllDB.u2AMPDULimit !=
            RadioIfGetDB.RadioIfGetAllDB.u2AMPDULimit)
        {
            u1IsValChanged = OSIX_TRUE;
            /*Using Supp variables only HtCapInfo will be assembled */
            RadioIfGetDB.RadioIfGetAllDB.u2AMPDULimit =
                pRadioIfGetDB->RadioIfGetAllDB.u2AMPDULimit;
        }
    }
    if (pRadioIfGetDB->RadioIfIsGetAllDB.bAMSDUStatus != OSIX_FALSE)
    {
        if (pRadioIfGetDB->RadioIfGetAllDB.u1AMSDUStatus !=
            RadioIfGetDB.RadioIfGetAllDB.u1AMSDUStatus)
        {
            u1IsValChanged = OSIX_TRUE;
            /*Using Supp variables only HtCapInfo will be assembled */
            RadioIfGetDB.RadioIfGetAllDB.u1AMSDUStatus =
                pRadioIfGetDB->RadioIfGetAllDB.u1AMSDUStatus;
        }
    }
    if (pRadioIfGetDB->RadioIfIsGetAllDB.bAMSDULimit != OSIX_FALSE)
    {
        if (pRadioIfGetDB->RadioIfGetAllDB.u2AMSDULimit !=
            RadioIfGetDB.RadioIfGetAllDB.u2AMSDULimit)
        {
            u1IsValChanged = OSIX_TRUE;
            /*Using Supp variables only HtCapInfo will be assembled */
            RadioIfGetDB.RadioIfGetAllDB.u2AMSDULimit =
                pRadioIfGetDB->RadioIfGetAllDB.u2AMSDULimit;
        }
    }
    /*If the configured value differ from the DB value, then WssifRadioIfDB updation
     * takes place*/
    if (u1IsValChanged == OSIX_FALSE)
    {
        RADIOIF_TRC (RADIOIF_INFO_TRC,
                     "RadioIfSetFsDot11nExtParams : No Value Change, "
                     "Not triggering Config Update \r\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_SUCCESS;
    }

    /* Get the Radio IF DB info to fill RadioId and BPFlag structure  */
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;

    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                  &RadioIfGetDB) != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetDot11nExtParams : RadioIf DB access failed. \r\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;

    }

    /* HT Capability Information Element in Config Update Request */
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfDot11nCfg.u1RadioId =
        RadioIfGetDB.RadioIfGetAllDB.u1RadioId;

    /* nmhRoutines already validated whether the input received and old
     * values are different. Hence fill config update request structure */
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfDot11nCfg.u2MessageType =
        VENDOR_DOT11N_CONFIG_MSG;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfDot11nCfg.u2MessageLength
        = RADIO_DOT11N_EXT_MSG_LEN;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfDot11nCfg.u4VendorId
        = VENDOR_ID;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.u2WtpInternalId
        = RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;

    /* HT Capability Information Element in Config Update Request */
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfDot11nCfg.isPresent
        = OSIX_TRUE;

    /* Construct configupdate req for 802.11n HT Capabilities element */
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfDot11nCfg.u1AMPDUStatus =
        RadioIfGetDB.RadioIfGetAllDB.u1AMPDUStatus;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfDot11nCfg.u1AMPDUSubFrame =
        RadioIfGetDB.RadioIfGetAllDB.u1AMPDUSubFrame;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfDot11nCfg.u2AMPDULimit =
        RadioIfGetDB.RadioIfGetAllDB.u2AMPDULimit;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfDot11nCfg.u1AMSDUStatus =
        RadioIfGetDB.RadioIfGetAllDB.u1AMSDUStatus;
    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfDot11nCfg.u2AMSDULimit =
        RadioIfGetDB.RadioIfGetAllDB.u2AMSDULimit;
    /* Invoke WLC Handler to send the config update request for Information
     * Element*/
    i4RetVal = WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_RADIO_CONF_UPDATE_REQ,
                                       pWlcHdlrMsgStruct);

    /* Invoke WLC Handler to send the config update request */
    if (i4RetVal == OSIX_FAILURE)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetFsDot11nExtParams: 11n config failed \r\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;
    }
    if (WssIfProcessRadioIfDBMsg (WSS_SET_RADIO_IF_DB_11N,
                                  pRadioIfGetDB) != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetFsDot11nExtParams: Radio DB Update failure\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;
    }

    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
    return OSIX_SUCCESS;
}
#endif /* __RADIOIFAC_C__ */
