
/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: radioifhwnpwr.c,v 1.6.2.1 2018/04/20 13:56:24 siva Exp $
 *
 * Description:This file contains the wrapper for
 *             for Hardware API's w.r.t RADIO
 *
 *******************************************************************/
#include "radioifinc.h"
#include "radionp.h"
#include "nputil.h"

#ifdef  NPAPI_WANTED

/*****************************************************************************
 * Function Name      : RadioNpWrHwProgram                                   *
 *                                                                           *
 * Description        : This function takes care of calling appropriate NP   *
 *                      call using the tRadioIfNpModInfo                     *
 *                                                                           *
 * Input(s)           : pFsHwNp - pointer the input structure                *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : FNP_SUCCESS, if initialization succeeds              *
 *                      FNP_FAILURE, otherwise                               *
 *****************************************************************************/

PUBLIC UINT1
RadioNpWrHwProgram (tFsHwNp * pFsHwNp)
{
    UINT1               u1RetVal = FNP_FAILURE;
    INT4                i4RetVal = FNP_FAILURE;
    UINT4               u4Opcode = 0;
    tRadioIfNpModInfo  *pRadioIfNpModInfo = NULL;
    INT4               *pi4ErrVal = NULL;

    if (NULL == pFsHwNp)
    {
        return FNP_FAILURE;
    }

    u4Opcode = pFsHwNp->u4Opcode;
    pRadioIfNpModInfo = &(pFsHwNp->RadioIfNpModInfo);

    if (NULL == pRadioIfNpModInfo)
    {
        return FNP_FAILURE;
    }

    switch (u4Opcode)
    {
        case FS_RADIO_HW_INIT:
        {
            i4RetVal = FsRadioHwInit ();
            if (i4RetVal == FNP_FAILURE)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            break;
        }

#ifdef HOSTAPD_WANTED
        case FS_RADIO_HW_HOSTAPD_RESET:
        {
            u1RetVal = FsHostApdReset ();
            break;
        }
#endif
        case FS_RADIO_HW_SET_LOCAL_ROUTING:
        {
            tRadioIfNpWrFsHwSetLocalRouting *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpWrSetLocalRouting;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwSetLocalRouting (pEntry->u1WtpLocalRouting);
            break;
        }
        case FS_RADIO_HW_UPDATE_LOCAL_ROUTING:
        {
            tRadioIfNpWrFsHwSetLocalRouting *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpWrSetLocalRouting;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwUpdateLocalRouting (pEntry->u1WtpLocalRouting);
            break;
        }
        case FS_RADIO_HW_DE_INIT:
        {
            FsRadioHwDeInit (pi4ErrVal);
            if (*pi4ErrVal < 0)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            break;
        }

        case FS_RADIO_HW_UPDATE_ADMIN_STATUS_ENABLE:
        {
            tRadioIfNpWrFsHwUpdateAdminStatusEnable *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpFsHwUpdateAdminStatusEnable;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwUpdateAdminStatusEnable (pEntry->RadInfo);
            break;
        }

        case FS_RADIO_HW_UPDATE_ADMIN_STATUS_DISABLE:
        {
            tRadioIfNpWrFsHwUpdateAdminStatusDisable *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpFsHwUpdateAdminStatusDisable;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwUpdateAdminStatusDisable (pEntry->RadInfo);
            break;
        }

        case FS_RADIO_HW_SET_ANTENNA_MODE:
        {
            tRadioIfNpWrFsHwSetAntennaMode *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpFsHwSetAntennaMode;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwSetAntennaMode (pEntry->RadInfo,
                                                pEntry->u1AntennaMode);
            break;
        }

        case FS_RADIO_HW_GET_ANTENNA_MODE:
        {
            tRadioIfNpWrFsHwGetAntennaMode *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpFsHwGetAntennaMode;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwGetAntennaMode (pEntry->RadInfo,
                                                pEntry->pu1AntennaMode);
            break;
        }

        case FS_RADIO_HW_GET_CURR_TX_ANTENNA:
        {
            tRadioIfNpWrFsHwGetCurrentTxAntenna *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpFsHwGetCurrentTxAntenna;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwGetCurrTxAntenna (pEntry->RadInfo,
                                                  pEntry->pu1AntennaCount);
            break;
        }

        case FS_RADIO_HW_SET_ANTENNA_SELECTION:
        {
            tRadioIfNpWrFsHwSetAntennaSelection *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpFsHwSetAntennaSelection;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwSetAntennaSelection (pEntry->RadInfo,
                                                     pEntry->u1AntennaIndex,
                                                     pEntry->
                                                     u1AntennaSelection);
            break;
        }

        case FS_RADIO_HW_GET_ANTENNA_SELECTION:
        {
            tRadioIfNpWrFsHwGetAntennaSelection *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpFsHwGetAntennaSelection;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwGetAntennaSelection (pEntry->RadInfo,
                                                     pEntry->u1AntennaIndex,
                                                     pEntry->
                                                     pu1AntennaSelection);
            break;
        }

        case FS_RADIO_HW_SET_ANTENNA_DIVERSITY:
        {
            tRadioIfNpWrFsHwSetAntennaDiversity *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpFsHwSetAntennaDiversity;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwSetAntennaDiversity (pEntry->RadInfo,
                                                     &pEntry->u4RcvDivsity);
            break;
        }

        case FS_RADIO_HW_GET_ANTENNA_DIVERSITY:
        {
            tRadioIfNpWrFsHwGetAntennaDiversity *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpFsHwGetAntennaDiversity;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwGetAntennaDiversity (pEntry->RadInfo,
                                                     &pEntry->u4RcvDivsity);
            break;
        }

        case FS_RADIO_HW_SET_RTS_THRESHOLD:
        {
            tRadioIfNpWrConfigSetDot11RTSThreshold *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpConfigSetDot11RTSThreshold;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            if (pEntry->u2RTSThreshold > RADIO_MAX_RTS_THRESHOLD)
            {
                return FNP_FAILURE;
            }

            u1RetVal = FsRadioHwSetDot11RTSThreshold (pEntry->RadInfo,
                                                      pEntry->u2RTSThreshold);
            break;
        }

        case FS_RADIO_HW_GET_RTS_THRESHOLD:
        {
            tRadioIfNpWrConfigGetDot11RTSThreshold *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpConfigGetDot11RTSThreshold;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }

            u1RetVal = FsRadioHwGetDot11RTSThreshold (pEntry->RadInfo,
                                                      &pEntry->u2RTSThreshold);
            break;
        }

        case FS_RADIO_HW_ADD_CHANNEL_SWIT_IE:
        {
            tRadioIfNpWrConfigSetDot11Channel *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpConfigSetDot11Channel;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwAddChanlSwitIE (pEntry->RadInfo,
                                                pEntry->u2ChannelNum);
            break;
        }
        case FS_RADIO_HW_REMOVE_CHANNEL_SWIT_IE:
        {
            tRadioIfNpWrConfigGetDot11Channel *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpConfigGetDot11Channel;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwRemoveChanlSwitIE (pEntry->RadInfo,
                                                   pEntry->u2ChannelNum);
            break;
        }

        case FS_RADIO_HW_SET_FRAG_THRESHOLD:
        {
            tRadioIfNpWrConfigSetDot11FragThreshold *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpConfigSetDot11FragThreshold;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            if ((pEntry->u2FragThreshold < RADIO_MIN_FRAGMENT_THRESHOLD)
                || (pEntry->u2FragThreshold > RADIO_MAX_FRAGMENT_THRESHOLD))
            {
                return FNP_FAILURE;
            }
            u1RetVal = FsRadioHwSetDot11FragThreshold (pEntry->RadInfo,
                                                       pEntry->u2FragThreshold);
            break;
        }

        case FS_RADIO_HW_GET_FRAG_THRESHOLD:
        {
            tRadioIfNpWrConfigGetDot11FragThreshold *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpConfigGetDot11FragThreshold;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwGetDot11FragThreshold (pEntry->RadInfo,
                                                       &pEntry->
                                                       u2FragThreshold);
            break;
        }

        case FS_RADIO_HW_SET_LONG_RETRY_LIMIT:
        {
            tRadioIfNpWrFsHwSetDot11LongRetryLimit *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpFsHwSetDot11LongRetryLimit;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            if (pEntry->u1LongRetryLimit == 0)
            {
                return FNP_FAILURE;
            }
            u1RetVal = FsRadioHwSetLongRetryLimit (pEntry->RadInfo,
                                                   pEntry->u1LongRetryLimit);
            break;
        }

        case FS_RADIO_HW_GET_LONG_RETRY_LIMIT:
        {
            tRadioIfNpWrFsHwGetDot11LongRetryLimit *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpFsHwGetDot11LongRetryLimit;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwGetLongRetryLimit (pEntry->RadInfo,
                                                   pEntry->pu1LongRetryLimit);
            break;
        }
        case FS_RADIO_HW_SET_TX_CURR_POWER:
        {
            tRadioIfNpWrFsHwSetTxCurrPower *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpFsHwSetTxCurrPower;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwSetTxCurrPower (pEntry->RadInfo,
                                                pEntry->i4TxCurrPower);
            break;
        }

        case FS_RADIO_HW_GET_TX_CURR_POWER:
        {
            tRadioIfNpWrFsHwSetTxCurrPower *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpFsHwSetTxCurrPower;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwGetTxCurrPower (pEntry->RadInfo,
                                                &(pEntry->i4TxCurrPower));
            break;
        }

        case FS_RADIO_HW_SET_OFDM_CHANNEL_WIDTH:
        {
            tRadioIfNpWrFsHwSetOFDMChannelWidth *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpFsHwSetOFDMChannelWidth;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwSetOFDMChannelWidth (pEntry->RadInfo,
                                                     &(pEntry->u4ChannelWidth));
            break;
        }

        case FS_RADIO_HW_GET_OFDM_CHANNEL_WIDTH:
        {
            tRadioIfNpWrFsHwGetOFDMChannelWidth *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpFsHwGetOFDMChannelWidth;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwGetOFDMChannelWidth (pEntry->RadInfo,
                                                     &(pEntry->u4ChannelWidth));
            break;
        }
        case FS_RADIO_HW_GET_SHORT_GI:
        {
            tRadioIfNpWrConfigGetDot11ShortGi *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpConfigGetDot11ShortGi;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwGetDot11ShortGi (pEntry->RadInfo, (UINT4 *) 20);
            break;
        }
        case FS_RADIO_HW_SET_BEACON_PERIOD:
        {
            tRadioIfNpWrFsHwSetBeaconInterval *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpFsHwSetBeaconInterval;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwSetBeaconInterval (pEntry->RadInfo,
                                                   &(pEntry->u4BeaconInterval));
            break;
        }
        case FS_RADIO_HW_GET_BEACON_PERIOD:
        {
            tRadioIfNpWrFsHwGetBeaconInterval *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpFsHwGetBeaconInterval;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwGetBeaconInterval (pEntry->RadInfo,
                                                   &(pEntry->u4BeaconInterval));
            break;
        }
        case FS_RADIO_HW_QOS_INFO:
        {
            tRadioIfNpWrConfigSetDot11QosInfoTableParams *pEntry = NULL;
            pEntry =
                (tRadioIfNpWrConfigSetDot11QosInfoTableParams *) &
                pRadioIfNpModInfo->RadioIfNpConfigSetDot11QosInfoTableParams;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwSetDot11QosInfoTableParams (pEntry->RadInfo,
                                                            &(pEntry->
                                                              u4QosInfo));
        }
            break;
        case FS_RADIO_HW_SET_EDCA_PARAMS:
        {
            tRadioIfNpWrConfigSetDot11EDCATableParams *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpConfigSetDot11EDCATableParams;

            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            if ((pEntry->i4ValDot11EDCATableCWmin < 0) ||
                (pEntry->i4ValDot11EDCATableCWmin > RADIO_QOS_CWMIN_MAX_VAL))
            {
                return FNP_FAILURE;
            }
            if ((pEntry->i4ValDot11EDCATableCWmax < 0) ||
                (pEntry->i4ValDot11EDCATableCWmax > RADIO_QOS_CWMAX_MAX_VAL))
            {
                return FNP_FAILURE;
            }
            if ((pEntry->i4ValDot11EDCATableAifs < RADIO_QOS_AIFSN_MIN_VAL)
                || (pEntry->i4ValDot11EDCATableAifs > RADIO_QOS_AIFSN_MAX_VAL))
            {
                return FNP_FAILURE;
            }
            if ((pEntry->i4ValDot11EDCATableTxOpLmt < 0) ||
                (pEntry->i4ValDot11EDCATableTxOpLmt > RADIO_QOS_TXOP_MAX_VAL))
            {
                return FNP_FAILURE;
            }
            u1RetVal = FsRadioHwSetDot11EDCATableParams (pEntry->RadInfo,
                                                         &(pEntry->
                                                           i4Dot11EDCATableIndex),
                                                         &(pEntry->
                                                           i4ValDot11EDCATableCWmin),
                                                         &(pEntry->
                                                           i4ValDot11EDCATableCWmax),
                                                         &(pEntry->
                                                           i4ValDot11EDCATableAifs),
                                                         &(pEntry->
                                                           i4ValDot11EDCATableTxOpLmt),
                                                         &(pEntry->
                                                           i4ValDot11EDCATableAdmsnCtrl));
        }
            break;
        case FS_RADIO_HW_SET_EDCA_CW_MIN:
        {
            tRadioIfNpWrConfigSetDot11EDCATableCWmin *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpConfigSetDot11EDCATableCWmin;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            if ((pEntry->i4ValDot11EDCATableCWmin < 0) ||
                (pEntry->i4ValDot11EDCATableCWmin > RADIO_QOS_CWMIN_MAX_VAL))
            {
                return FNP_FAILURE;
            }
            u1RetVal = FsRadioHwSetDot11EDCATableCWmin (pEntry->RadInfo,
                                                        &(pEntry->
                                                          i4Dot11EDCATableIndex),
                                                        &(pEntry->
                                                          i4ValDot11EDCATableCWmin));
            break;
        }
        case FS_RADIO_HW_GET_EDCA_CW_MIN:
        {
            tRadioIfNpWrConfigGetDot11EDCATableCWmin *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpConfigGetDot11EDCATableCWmin;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwGetDot11EDCATableCWmin (pEntry->RadInfo,
                                                        &(pEntry->
                                                          i4Dot11EDCATableIndex),
                                                        &(pEntry->
                                                          i4ValDot11EDCATableCWmin));
            break;
        }
        case FS_RADIO_HW_SET_EDCA_CW_MAX:
        {
            tRadioIfNpWrConfigSetDot11EDCATableCWmax *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpConfigSetDot11EDCATableCWmax;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            if ((pEntry->i4ValDot11EDCATableCWmax < 0) ||
                (pEntry->i4ValDot11EDCATableCWmax > RADIO_QOS_CWMAX_MAX_VAL))
            {
                return FNP_FAILURE;
            }
            u1RetVal = FsRadioHwSetDot11EDCATableCWmax (pEntry->RadInfo,
                                                        &(pEntry->
                                                          i4Dot11EDCATableIndex),
                                                        &(pEntry->
                                                          i4ValDot11EDCATableCWmax));
            break;
        }
        case FS_RADIO_HW_GET_EDCA_CW_MAX:
        {
            tRadioIfNpWrConfigGetDot11EDCATableCWmax *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpConfigGetDot11EDCATableCWmax;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwGetDot11EDCATableCWmax (pEntry->RadInfo,
                                                        &(pEntry->
                                                          i4Dot11EDCATableIndex),
                                                        &(pEntry->
                                                          i4ValDot11EDCATableCWmax));
            break;
        }
        case FS_RADIO_HW_SET_EDCA_AIFS:
        {
            tRadioIfNpWrConfigSetDot11EDCATableAifs *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpConfigSetDot11EDCATableAifs;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            if ((pEntry->i4ValDot11EDCATableAifs < RADIO_QOS_AIFSN_MIN_VAL)
                || (pEntry->i4ValDot11EDCATableAifs > RADIO_QOS_AIFSN_MAX_VAL))
            {
                return FNP_FAILURE;
            }
            u1RetVal = FsRadioHwSetDot11Aifs (pEntry->RadInfo,
                                              &(pEntry->i4Dot11EDCATableIndex),
                                              &(pEntry->
                                                i4ValDot11EDCATableAifs));
            break;
        }
        case FS_RADIO_HW_GET_EDCA_AIFS:
        {
            tRadioIfNpWrConfigGetDot11EDCATableAifs *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpConfigGetDot11EDCATableAifs;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwGetDot11Aifs (pEntry->RadInfo,
                                              &(pEntry->i4Dot11EDCATableIndex),
                                              &(pEntry->
                                                i4ValDot11EDCATableAifs));
            break;
        }
        case FS_RADIO_HW_SET_EDCA_TXOP_LIMIT:
        {
            tRadioIfNpWrConfigSetDot11EDCATableTxOpLmt *pEntry = NULL;
            pEntry =
                &pRadioIfNpModInfo->RadioIfNpConfigSetDot11EDCATableTxOpLmt;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            if ((pEntry->i4ValDot11EDCATableTxOpLmt < 0) ||
                (pEntry->i4ValDot11EDCATableTxOpLmt > RADIO_QOS_TXOP_MAX_VAL))
            {
                return FNP_FAILURE;
            }
            u1RetVal = FsRadioHwSetDot11EDCATableTxOpLmt (pEntry->RadInfo,
                                                          &(pEntry->
                                                            i4Dot11EDCATableIndex),
                                                          &(pEntry->
                                                            i4ValDot11EDCATableTxOpLmt));
            break;
        }

        case FS_RADIO_HW_GET_EDCA_TXOP_LIMIT:
        {
            tRadioIfNpWrConfigGetDot11EDCATableTxOpLmt *pEntry = NULL;
            pEntry =
                &pRadioIfNpModInfo->RadioIfNpConfigGetDot11EDCATableTxOpLmt;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwGetDot11EDCATableTxOpLmt (pEntry->RadInfo,
                                                          &(pEntry->
                                                            i4Dot11EDCATableIndex),
                                                          &(pEntry->
                                                            i4ValDot11EDCATableTxOpLmt));
            break;
        }
        case FS_RADIO_HW_SET_EDCA_ADMN_CTRL:
        {
            tRadioIfNpWrConfigSetDot11EDCATableAdmsnCtrl *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->
                RadioIfNpConfigSetDot11EDCATableAdmsnCtrl;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwSetDot11EDCATableAdmsnCtrl (pEntry->RadInfo,
                                                            &(pEntry->
                                                              i4Dot11EDCATableIndex),
                                                            &(pEntry->
                                                              i4ValDot11EDCATableAdmsnCtrl));
            break;
        }
        case FS_RADIO_HW_GET_EDCA_ADMN_CTRL:
        {
            tRadioIfNpWrConfigGetDot11EDCATableAdmsnCtrl *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->
                RadioIfNpConfigGetDot11EDCATableAdmsnCtrl;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwGetDot11EDCATableAdmsnCtrl
                (pEntry->RadInfo, &(pEntry->i4Dot11EDCATableIndex),
                 &(pEntry->i4ValDot11EDCATableAdmsnCtrl));
            break;
        }

        case FS_RADIO_HW_SET_HIDE_ESSID:
        {
            tRadioIfNpWrConfigSetDot11HideEssid *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpConfigSetDot11HideEssid;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwSetDot11HideEssid (pEntry->RadInfo,
                                                   &(pEntry->u1ValDot11Essid),
                                                   &(pEntry->
                                                     i4ValDot11HideEssidVal));
            break;
        }

        case FS_RADIO_HW_GET_HIDE_ESSID:
        {
            tRadioIfNpWrConfigGetDot11HideEssid *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpConfigGetDot11HideEssid;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwGetDot11HideEssid (pEntry->RadInfo,
                                                   &(pEntry->u1ValDot11Essid),
                                                   &pEntry->
                                                   i4ValDot11HideEssidVal);
            break;
        }
        case FS_RADIO_HW_SET_DTIM_PERIOD:
        {
            tRadioIfNpWrConfigSetDot11DtimPrd *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpWrConfigSetDot11DtimPrd;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            if ((pEntry->i4ValDot11DtimPrd < 0) ||
                (pEntry->i4ValDot11DtimPrd > RADIO_MAX_DTIM_PERIOD))
            {
                return FNP_FAILURE;
            }
            u1RetVal = FsRadioHwSetDot11DtimPrd (pEntry->RadInfo,
                                                 &pEntry->i4ValDot11DtimPrd);
            break;
        }

        case FS_RADIO_HW_GET_DTIM_PERIOD:
        {
            tRadioIfNpWrConfigGetDot11DtimPrd *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpWrConfigGetDot11DtimPrd;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwGetDot11DtimPrd (pEntry->RadInfo,
                                                 &(pEntry->i4ValDot11DtimPrd));
            break;
        }

        case FS_RADIO_HW_SET_STA_WEP_KEY_EN:
        {
            tRadioIfNpWrConfigSetDot11StaWepKeyEncryptn *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->
                RadioIfNpWrConfigSetDot11StaWepKeyEncryptn;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            if ((pEntry->i2ValDot11StaWepKeyEnIndex < 1) ||
                (pEntry->i2ValDot11StaWepKeyEnIndex > 4))
            {
                return FNP_FAILURE;
            }
            if (pEntry->i2ValDot11StaWepKeyAuthIndex > 4)
            {
                return FNP_FAILURE;
            }
            else if (pEntry->i2ValDot11StaWepKeyAuthIndex == 0)
            {
                pEntry->i2ValDot11StaWepKeyAuthIndex = 2;
            }

            u1RetVal = FsRadioHwSetDot11StWpKeyEncyptn (pEntry->RadInfo,
                                                        pEntry->
                                                        i2ValDot11StaWepKeyEnType,
                                                        pEntry->
                                                        au1ValDot11StaWepKeyEnVal,
                                                        pEntry->
                                                        i2ValDot11StaWepKeyEnIndex,
                                                        pEntry->
                                                        i2ValDot11StaWepKeyAuthIndex);
            break;
        }

        case FS_RADIO_HW_GET_STA_WEP_KEY_EN:
        {
            tRadioIfNpWrConfigGetDot11StaWepKeyEncryptn *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->
                RadioIfNpWrConfigGetDot11StaWepKeyEncryptn;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwGetDot11StWpKeyEncyptn (pEntry->RadInfo,
                                                        &pEntry->
                                                        i2ValDot11StaWepKeyEnType,
                                                        NULL,
                                                        pEntry->
                                                        i2ValDot11StaWepKeyEnIndex,
                                                        pEntry->
                                                        i2ValDot11StaWepKeyAuthIndex);
            break;
        }

        case FS_RADIO_HW_SET_STA_WEP_KEY_AUTH:
        {
            tRadioIfNpWrConfigSetDot11StWpAuthKeyType *pEntry = NULL;
            INT4                i4WpAuthKeyType = 0;
            pEntry = &pRadioIfNpModInfo->RadioIfNpConfigSetDot11StWpAuthKeyType;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            else if (pEntry->i2ValDot11StWpKeyAuthIndex == 0)
            {
                pEntry->i2ValDot11StWpKeyAuthIndex = 2;
            }
            else if ((pEntry->i2ValDot11StWpKeyAuthIndex < 1) ||
                     (pEntry->i2ValDot11StWpKeyAuthIndex > 4))
            {
                return FNP_FAILURE;
            }
            i4WpAuthKeyType = (INT4) pEntry->i2ValDot11StWpKeyAuthType;
            u1RetVal = FsRadioHwSetDot11StWpAuthKeyType (pEntry->RadInfo,
                                                         &i4WpAuthKeyType,
                                                         pEntry->
                                                         i2ValDot11StWpKeyAuthStat,
                                                         pEntry->
                                                         i2ValDot11StWpKeyAuthIndex);
            break;
        }
        case FS_RADIO_HW_GET_STA_WEP_KEY_AUTH:
        {
            tRadioIfNpWrConfigGetDot11StWpAuthKeyType *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpConfigGetDot11StWpAuthKeyType;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwGetDot11StWpAuthKeyType (pEntry->RadInfo,
                                                         &pEntry->
                                                         i4ValDot11StWpKeyAuthType,
                                                         pEntry->
                                                         i2ValDot11StWpKeyAuthStat,
                                                         pEntry->
                                                         i2ValDot11StWpKeyAuthIndex);
            break;
        }
        case FS_RADIO_HW_SET_STA_WEP_KEY_STAT:
        {
            tRadioIfNpWrConfigSetDot11StaWepKeyStatus *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpConfigSetDot11StaWepKeyStatus;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            if ((pEntry->i2ValDot11StaWepKeyIndex < 1) ||
                (pEntry->i2ValDot11StaWepKeyIndex > 10))
            {
                return FNP_FAILURE;
            }
            u1RetVal = FsRadioHwSetDot11StaWepKeyStatus (pEntry->RadInfo,
                                                         &pEntry->
                                                         i2ValDot11StaWepKeyStat,
                                                         pEntry->
                                                         i2ValDot11StaWepKeyIndex);
            break;
        }
        case FS_RADIO_HW_GET_STA_WEP_KEY_STAT:
        {
            tRadioIfNpWrConfigGetDot11StaWepKeyStatus *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpConfigGetDot11StaWepKeyStatus;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwGetDot11StaWepKeyStatus (pEntry->RadInfo,
                                                         &pEntry->
                                                         i2ValDot11StaWepKeyStat,
                                                         pEntry->
                                                         i2ValDot11StaWepKeyIndex);
            break;
        }
        case FS_RADIO_HW_SET_WLAN_AN_SUPP_MODE:
        {
            tRadioIfNpWrConfigSetDot11anSuppMode *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpConfigSetDot11anSuppMode;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwSetDot11anSuppMode (pEntry->RadInfo,
                                                    pEntry->
                                                    au1ValDot11anSuppMode);
            break;
        }
        case FS_RADIO_HW_GET_WLAN_AN_SUPP_MODE:
        {
            tRadioIfNpWrConfigGetDot11anSuppMode *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpConfigGetDot11anSuppMode;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwGetDot11anSuppMode (pEntry->RadInfo,
                                                    pEntry->
                                                    au1ValDot11anSuppMode);
            break;
        }
        case FS_RADIO_HW_SET_WLAN_N_AMPDU_SUP_STAT:

        {
            tRadioIfNpWrConfigSetDot11nAMPDUSuppStat *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpConfigSetDot11nAMPDUSuppStat;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwSetDot11nAMPDUSuppStat (pEntry->RadInfo,
                                                        &(pEntry->
                                                          i2ValDot11nAMPDUSuppStat));
            break;
        }
        case FS_RADIO_HW_GET_WLAN_N_AMPDU_SUP_STAT:

        {
            tRadioIfNpWrConfigGetDot11nAMPDUSuppStat *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpConfigGetDot11nAMPDUSuppStat;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwGetDot11nAMPDUSuppStat (pEntry->RadInfo,
                                                        &(pEntry->
                                                          i2ValDot11nAMPDUSuppStat));
            break;
        }
        case FS_RADIO_HW_SET_WLAN_G_SUPP_MODE:
        {
            tRadioIfNpWrConfigSetDot11gSuppMode *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpConfigSetDot11gSuppMode;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwSetDot11gSuppMode (pEntry->RadInfo,
                                                   pEntry->
                                                   au1ValDot11gSuppMode);
            break;
        }
        case FS_RADIO_HW_GET_WLAN_G_SUPP_MODE:
        {
            tRadioIfNpWrConfigGetDot11gSuppMode *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpConfigGetDot11gSuppMode;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwGetDot11gSuppMode (pEntry->RadInfo,
                                                   pEntry->
                                                   au1ValDot11gSuppMode);
            break;
        }
        case FS_RADIO_HW_SET_ESSID:
        {
            tRadioIfNpWrConfigSetDot11Essid *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpConfigSetDot11Essid;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwSetDot11Essid (pEntry->RadInfo,
                                               pEntry->au1ValDot11Essid);
            break;
        }
        case FS_RADIO_HW_SET_POWER_CONSTRAINT:
        {
            tRadioIfNpWrSetDot11PowerConstraint *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpConfigSetDot11PowerConstraint;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwSetDot11PowerConstraint (pEntry->RadInfo,
                                                         pEntry->
                                                         u1PowerConstraint);
            break;
        }
        case FS_RADIO_HW_SET_CAPABILITY:
        {
            tRadioIfNpWrSetDot11Capability *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpConfigSetDot11Capability;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }

            u1RetVal = FsRadioHwSetDot11Capability (pEntry->RadInfo,
                                                    pEntry->u2Capability);
            break;
        }
        case FS_RADIO_HW_MULTICAST_MODE:
        {
            tRadioIfNpWrSetDot11Multicast *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpWrSetDot11Multicast;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }

            u1RetVal = FsRadioHwSetDot11MulticastMode (pEntry->RadInfo,
                                                       (INT2 *) &(pEntry->
                                                                  u2WlanMulticastMode));
            break;
        }
        case FS_RADIO_HW_MULTICAST_LENGTH:
        {
            tRadioIfNpWrSetDot11Multicast *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpWrSetDot11Multicast;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }

            u1RetVal = FsRadioHwSetDot11MulticastLength (pEntry->RadInfo,
                                                         (INT2 *) &(pEntry->
                                                                    u2WlanSnoopTableLength));
            break;
        }
        case FS_RADIO_HW_MULTICAST_TIMER:
        {
            tRadioIfNpWrSetDot11Multicast *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpWrSetDot11Multicast;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }

            u1RetVal = FsRadioHwSetDot11MulticastTimer (pEntry->RadInfo,
                                                        (INT4 *) &(pEntry->
                                                                   u4WlanSnoopTimer));
            break;
        }
        case FS_RADIO_HW_MULTICAST_TIMEOUT:
        {
            tRadioIfNpWrSetDot11Multicast *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpWrSetDot11Multicast;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }

            u1RetVal = FsRadioHwSetDot11MulticastTimeout (pEntry->RadInfo,
                                                          (INT4 *) &(pEntry->
                                                                     u4WlanSnoopTimeout));
            break;
        }

        case FS_RADIO_HW_GET_ESSID:
        {
            tRadioIfNpWrConfigGetDot11Essid *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpConfigGetDot11Essid;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwGetDot11Essid (pEntry->RadInfo,
                                               pEntry->au1ValDot11Essid);
            break;
        }
        case FS_RADIO_HW_SET_WLAN_GN_SUPP_MODE:
        {
            tRadioIfNpWrConfigSetDot11gnSuppMode *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpConfigSetDot11gnSuppMode;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwSetDot11gnSuppMode (pEntry->RadInfo,
                                                    pEntry->
                                                    au1ValDot11SuppMode);
            break;
        }
        case FS_RADIO_HW_GET_WLAN_GN_SUPP_MODE:
        {
            tRadioIfNpWrConfigGetDot11gnSuppMode *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpConfigGetDot11gnSuppMode;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwGetDot11gnSuppMode (pEntry->RadInfo,
                                                    pEntry->
                                                    au1ValDot11SuppMode);
            break;
        }
        case FS_RADIO_HW_SET_WLAN_A_SUPP_MODE:
        {
            tRadioIfNpWrConfigSetDot11aSuppMode *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpConfigSetDot11aSuppMode;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwSetDot11aSuppMode (pEntry->RadInfo,
                                                   pEntry->au1ValDot11SuppMode);
            break;
        }
        case FS_RADIO_HW_GET_WLAN_A_SUPP_MODE:
        {
            tRadioIfNpWrConfigGetDot11aSuppMode *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpConfigGetDot11aSuppMode;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwGetDot11aSuppMode (pEntry->RadInfo,
                                                   pEntry->au1ValDot11SuppMode);
            break;
        }
        case FS_RADIO_HW_SET_WLAN_B_SUPP_MODE:
        {
            tRadioIfNpWrConfigSetDot11bSuppMode *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpConfigSetDot11bSuppMode;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwSetDot11bSuppMode (pEntry->RadInfo,
                                                   &(pEntry->
                                                     au1ValDot11SuppMode));
            break;
        }
        case FS_RADIO_HW_GET_WLAN_B_SUPP_MODE:
        {
            tRadioIfNpWrConfigGetDot11bSuppMode *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpConfigGetDot11bSuppMode;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwGetDot11bSuppMode (pEntry->RadInfo,
                                                   pEntry->au1ValDot11SuppMode);
            break;
        }
        case FS_RADIO_HW_SET_FREQUENCY:
        {
            tRadioIfNpWrConfigSetDot11Frequency *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpConfigSetDot11Frequency;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwSetDot11Frequency (pEntry->RadInfo,
                                                   pEntry->u1ValDot11Frequency);
            break;

        }
        case FS_RADIO_HW_GET_FREQUENCY:
        {
            tRadioIfNpWrConfigGetDot11Frequency *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpConfigGetDot11Frequency;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwGetDot11Frequency (pEntry->RadInfo,
                                                   &pEntry->
                                                   u1ValDot11Frequency);
            break;
        }
        case FS_RADIO_HW_WLAN_ADD_MAC:
        {
            tRadioIfNpWrConfigDot11AddMac *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpConfigDot11AddMac;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwDot11AddMac (pEntry->RadInfo,
                                             pEntry->au1ValDot11AddMac);
            break;

        }
        case FS_RADIO_HW_WLAN_DEL_MAC:
        {
            tRadioIfNpWrConfigDot11DelMac *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpConfigDot11DelMac;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwDot11AddMac (pEntry->RadInfo,
                                             pEntry->au1ValDot11DelMac);
            break;

        }
        case FS_RADIO_HW_WLAN_CREATE_ATH:
        {
            tRadioIfNpWrConfigDot11WlanCreate *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpConfigDot11WlanCreate;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwDot11WlanCreate (pEntry->RadInfo);
            break;

        }
        case FS_RADIO_HW_WLAN_CREATE_BR_INTERFACE:
        {
            tRadioIfNpWrConfigDot11CreateBrInterface *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpConfigDot11BrInterfaceCreate;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwDot11BrInterfaceCreate (pEntry);
            break;
        }
        case FS_RADIO_HW_WLAN_DESTROY_ATH:
        {
            tRadioIfNpWrConfigDot11WlanDestroy *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpConfigDot11WlanDestroy;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwDot11WlanDestroy (pEntry->RadInfo);
            break;

        }
        case FS_RADIO_HW_SET_CHANNEL_NUM:
        {
            tRadioIfNpWrConfigSetDot11Channel *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpConfigSetDot11Channel;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwSetDot11Channel (pEntry->RadInfo,
                                                 pEntry->u2ChannelNum);
            u1RetVal = FsRadioHwSetDot11Channel (pEntry->RadInfo,
                                                 pEntry->u2ChannelNum);
            break;
        }
        case FS_RADIO_HW_GET_CHANNEL_NUM:
        {
            tRadioIfNpWrConfigGetDot11Channel *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpConfigGetDot11Channel;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwGetDot11Channel (pEntry->RadInfo,
                                                 &pEntry->u2ChannelNum);
            break;
        }
        case FS_RADIO_HW_SET_MAX_TX_POW_2G:
        {
            tRadioIfNpWrConfigSetDot11MaxTxPow2G *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpConfigSetDot11MaxTxPow2G;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwSetDot11MaxTxPow2G (pEntry->RadInfo,
                                                    pEntry->u4MaxTxPow);
            break;
        }
        case FS_RADIO_HW_GET_MAX_TX_POW_2G:
        {
            tRadioIfNpWrConfigGetDot11MaxTxPow2G *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpConfigGetDot11MaxTxPow2G;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwGetDot11MaxTxPow2G (pEntry->RadInfo,
                                                    &(pEntry->u4MaxTxPow));
            break;
        }
        case FS_RADIO_HW_SET_MAX_TX_POW_5G:
        {
            tRadioIfNpWrConfigSetDot11MaxTxPow5G *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpConfigSetDot11MaxTxPow5G;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwSetDot11MaxTxPow5G (pEntry->RadInfo,
                                                    pEntry->u4MaxTxPow);
            break;
        }
        case FS_RADIO_HW_GET_MAX_TX_POW_5G:
        {
            tRadioIfNpWrConfigGetDot11MaxTxPow5G *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpConfigGetDot11MaxTxPow5G;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwGetDot11MaxTxPow5G (pEntry->RadInfo,
                                                    &(pEntry->u4MaxTxPow));
            break;
        }
        case FS_RADIO_HW_SET_COUNTRY:
        {
            tRadioIfNpWrConfigSetDot11Country *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpConfigSetDot11Country;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwSetDot11Country (pEntry->RadInfo,
                                                 pEntry->
                                                 au1ValDot11CountryName);
            break;
        }
        case FS_RADIO_HW_GET_COUNTRY:
        {
            tRadioIfNpWrConfigGetDot11Country *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpConfigGetDot11Country;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwGetDot11Country (pEntry->RadInfo,
                                                 pEntry->
                                                 au1ValDot11CountryName);
            break;
        }
        case FS_RADIO_HW_SET_WIFI_HW_ADDR:
        {
            tRadioIfNpWrConfigSetDot11HwAddr *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpConfigSetDot11HwAddr;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwSetDot11HwAddr (pEntry->RadInfo,
                                                pEntry->au1WifiHwAddr);
            break;
        }

        case FS_RADIO_HW_GET_WIFI_HW_ADDR:
        {
            tRadioIfNpWrConfigGetDot11HwAddr *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpConfigGetDot11HwAddr;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwGetDot11HwAddr (pEntry->RadInfo,
                                                pEntry->au1WifiHwAddr);
            break;
        }
        case FS_RADIO_HW_SET_ATH_HW_ADDR:
        {
            tRadioIfNpWrSetDot11AthHwAddr *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpSetDot11AthHwAddr;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwSetDot11AthHwAddr (pEntry->RadInfo,
                                                   pEntry->au1AthHwAddr);
            break;
        }
        case FS_RADIO_HW_GET_ATH_HW_ADDR:
        {
            tRadioIfNpWrGetDot11AthHwAddr *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpGetDot11AthHwAddr;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwGetDot11AthHwAddr (pEntry->RadInfo,
                                                   pEntry->au1AthHwAddr);
            break;
        }
        case FS_RADIO_HW_GET_BIT_RATES:
        {
            tRadioIfNpWrGetDot11BitRates *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpGetDot11BitRates;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwGetDot11BitRates (pEntry->RadInfo,
                                                  &(pEntry->u2BitRateNum),
                                                  (UINT4 *) &(pEntry->
                                                              au4BitRate),
                                                  &(pEntry->u4CurrBitRate));
            break;
        }
        case FS_RADIO_HW_SET_BIT_RATES:
        {
            tRadioIfNpWrSetDot11BitRates *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpSetDot11BitRates;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwSetDot11BitRates (pEntry->RadInfo, pEntry);
            break;
        }
        case FS_RADIO_HW_STA_AUTH_DEAUTH:
        {
            tRadioIfNpWrDot11StaAuthDeAuth *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpWrDot11StaAuthDeAuth;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwDot11StaAuthDeAuth (pEntry->RadInfo,
                                                    pEntry->u1AuthStat,
                                                    pEntry->au1StaAuthMac);
#ifdef KERNEL_CAPWAP_WANTED
            if (pEntry->u1AuthStat != WLAN_CLIENT_DELETE)
            {
                FsRadioHwMapVlanToWlanIntf (pEntry->u1AuthStat,
                                            pEntry->u2VlanId,
                                            pEntry->au1IntfName,
                                            (UINT1) pEntry->RadInfo.
                                            au2WlanIfIndex[0]);
            }
#endif
            break;
        }
        case FS_RADIO_HW_SET_STA_FWD:
        {
            tRadioIfNpWrSetDot11StaFwd *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpSetDot11StaFwd;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwSetDot11StaFwd (pEntry->RadInfo,
                                                &(pEntry->u4SetStaFwd));
            break;
        }
        case FS_RADIO_HW_GET_STA_FWD:
        {
            tRadioIfNpWrGetDot11StaFwd *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpGetDot11StaFwd;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwGetDot11StaFwd (pEntry->RadInfo,
                                                &(pEntry->u4GetStaFwd));
            break;
        }
        case FS_RADIO_HW_GET_RAD_STATS:
        {
            tRadioIfNpWrGet80211Stats *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpGet80211Stats;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsRadioHwGet80211Stats (pEntry->RadInfo, &(pEntry->Stats),
                                        &(pEntry->MacStats),
                                        &(pEntry->MacMulStats));
            break;
        }
#ifdef DEBUG_WANTED
        case FS_RADIO_HW_SET_11NRATES_STAT:
        {
            tRadioIfNpWrSetDot11NRatesStat *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpSetDot11NRatesStat;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwSetDot11NRatesStat (pEntry->RadInfo,
                                                    &(pEntry->u4SetRateStatus));
            break;
        }
#endif
        case FS_RADIO_HW_GET_11NRATES_STAT:
        {
            tRadioIfNpWrGetDot11NRatesStat *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpGetDot11NRatesStat;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwGetDot11NRatesStat (pEntry->RadInfo,
                                                    &(pEntry->u4GetRateStatus));
            break;
        }
#ifdef WPS_WTP_WANTED
        case FS_RADIO_HW_DISABLE_WPS_PUSH_BUTTON:
        {
            u1RetVal = FsRadioHwSetDot11WpsButtonPushedDisable ();
            break;
        }
        case FS_RADIO_HW_SET_WPS_IE_PARAMS:
        {
            tRadioIfNpWrSetWpsIEParams *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpSetDot11WpsIEParams;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwSetDot11WpsIEParams (pEntry);
            break;
        }
        case FS_RADIO_HW_SET_WPS_ADDITIONAL_PARAMS:
        {
            tRadioIfNpWrSetWpsAdditionalParams *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpSetDot11WpsAdditionalParams;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwSetDot11WpsAdditionalParams (pEntry);
            break;
        }
#endif
        case FS_RADIO_HW_SET_RSNA_IE_PARAMS:
        {
            tRadioIfNpWrSetRsnaIEParams *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpSetDot11RsnaIEParams;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwSetDot11RsnaIEParams (pEntry->RadInfo,
                                                      pEntry->u1RsnaStatus,
                                                      pEntry->
                                                      u2PairwiseCipherSuiteCount,
                                                      pEntry->u2AKMSuiteCount,
                                                      pEntry->au1PairwiseCipher,
                                                      pEntry->au1AKMSuite,
                                                      pEntry->u1GroupCipher,
                                                      pEntry->u1Privacy
#ifdef PMF_WANTED
                                                      ,
                                                      pEntry->u1GroupMgmtCipher,
                                                      pEntry->u2RsnCapab
#endif
                );

            break;
        }
        case FS_RADIO_HW_SET_RSNA_PAIRWISE_PARAMS:
        {
            tRadioIfNpWrSetRsnaPairwiseParams *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpSetDot11RsnaPairwiseParams;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwSetDot11RsnaPairwiseParams (pEntry->RadInfo,
                                                            pEntry->
                                                            u1RsnaStatus,
                                                            pEntry->
                                                            u2PairwiseCipherSuiteCount,
                                                            pEntry->
                                                            u2AKMSuiteCount,
                                                            pEntry->
                                                            au1PairwiseCipher,
                                                            pEntry->au1AKMSuite,
                                                            pEntry->
                                                            u1GroupCipher,
                                                            pEntry->au1PtkKey,
                                                            pEntry->u2KeyLength,
                                                            pEntry->u1KeyIndex,
                                                            pEntry->u1Privacy,
                                                            pEntry->
                                                            stationMacAddress);
            break;
        }
        case FS_RADIO_HW_SET_RSNA_GROUPWISE_PARAMS:
        {
            tRadioIfNpWrSetRsnaGroupwiseParams *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpSetDot11RsnaGroupwiseParams;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwSetDot11RsnaGroupwiseParams (pEntry->RadInfo,
                                                             pEntry->
                                                             u1GroupCipher,
                                                             pEntry->au1GtkKey,
                                                             pEntry->
                                                             u2KeyLength,
                                                             pEntry->
                                                             u1KeyIndex);
            break;
        }
#ifdef PMF_WANTED
        case FS_RADIO_HW_SET_RSNA_MGMT_GROUPWISE_PARAMS:
        {
            tRadioIfNpWrSetRsnaMgmtGroupwiseParams *pEntry = NULL;
            pEntry =
                &pRadioIfNpModInfo->RadioIfNpSetDot11RsnaMgmtGroupwiseParams;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsRadioHwSetDot11RsnaMgmtGroupwiseParams (pEntry->RadInfo,
                                                          pEntry->
                                                          u1MgmtGroupCipher,
                                                          pEntry->au1IGtkKey,
                                                          pEntry->u2KeyLength,
                                                          pEntry->u1KeyIndex);
            break;
        }
#endif

        case FS_RADIO_HW_GET_RADIO_CLIENT_STATS:
        {
            tRadioIfNpWrGetInterfaceInOctets *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpGetInterfaceInOctets;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwGetInterfaceInOctets (pEntry->RadInfo,
                                                      &(pEntry->u4IfInOctets));
            break;
        }
        case FS_RADIO_HW_GET_INT_OUT_OCTETS:
        {
            tRadioIfNpWrGetInterfaceOutOctets *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpGetInterfaceOutOctets;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwGetInterfaceOutOctets (pEntry->RadInfo,
                                                       &(pEntry->
                                                         u4IfOutOctets));
            break;
        }
        case FS_RADIO_HW_GET_CLIENT_RECVD_COUNT:
        {
            tRadioIfNpWrGetClientStatsBytesRecvdCount *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpGetClientStatsBytesRecvdCount;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwGetClientStatsBytesRecvdCount (pEntry->RadInfo,
                                                               pEntry->
                                                               stationMacAddress,
                                                               &(pEntry->
                                                                 u4ClientStatsBytesRecvdCount));
            break;
        }
        case FS_RADIO_HW_GET_CLIENT_SENT_COUNT:
        {
            tRadioIfNpWrGetClientStatsBytesSentCount *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpGetClientStatsBytesSentCount;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwGetClientStatsBytesSentCount (pEntry->RadInfo,
                                                              pEntry->
                                                              stationMacAddress,
                                                              &(pEntry->
                                                                u4ClientStatsBytesSentCount));
            break;
        }
        case FS_RADIO_HW_GET_RADIO_DEFAULT:
        {
            tRadioIfNpWrFsHwGetRadioDefault *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpWrHwGetRadioDefault;

            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }

            /* populate the default values */
            pEntry->u1AntennaSelection = RADIO_ANTENNA_INTERNAL;
            pEntry->u1TxAntenna = RADIOIF_DEF_ANT_COUNT;
            pEntry->u1AntennaDiveristy = RADIO_ANTENNA_DIVERSITY_DISABLED;
            pEntry->u1AntennaMode = RADIOIF_DEF_ANT_MODE;
            pEntry->u1CurrentCCA = 1;
            pEntry->u1ShortRetry = RADIOIF_DEF_SHORT_RETRY;
            pEntry->u1LongRetry = RADIOIF_DEF_LONG_RETRY;
            pEntry->u4TxMSDULifeTime = RADIOIF_DEF_TX_LIFETIME;
            pEntry->u4RxMSDULifeTime = RADIOIF_DEF_RX_LIFETIME;
            pEntry->u2FirstChanNo = 1;
            pEntry->u2NoOfChan = 2;
            pEntry->u2MaxTxPowerLvl = RADIOIF_MAX_POWER_LEVEL;
            pEntry->u1BandSupport = 15;
            pEntry->u1ShortPreamble = 1;
            pEntry->u1NoOfBSSID = 16;
            /*For 802.11n */
            pEntry->u1ChannelWidth = DOT11N_CHANNEL_WIDTH20;
            pEntry->u1ShortGi20 = DOT11N_GI_SHORT20_ENABLE;
            pEntry->u1ShortGi40 = DOT11N_GI_SHORT40_ENABLE;
            pEntry->u2HTCapInfo = RADIO_DOT11N_HTCAPINFO_DEFAULT;
            pEntry->u1AMPDUParam = RADIO_DOT11N_AMPDU;
            pEntry->u1TxSupportMcsSet = DOT11N_TX_SUPP_MCS_ENABLE;
            pEntry->u1HTInfoSecChanAbove = DOT11N_AN_HT40_CHANNEL_ABOVE;
            pEntry->u1HTInfoSecChanBelow = DOT11N_AN_HT40_CHANNEL_BELOW;
            pEntry->au1SuppMCSSet[RADIO_VALUE_0] = DOT11N_MCS_DEFAULT_VALUE;
            pEntry->au1SuppMCSSet[RADIO_VALUE_1] = DOT11N_MCS_DEFAULT_VALUE;
            pEntry->au1SuppMCSSet[RADIO_VALUE_2] = DOT11N_MCS_DEFAULT_VALUE;
            pEntry->au1ManMCSSet[RADIO_VALUE_0] = DOT11N_MCS_DEFAULT_VALUE;
            pEntry->au1ManMCSSet[RADIO_VALUE_1] = DOT11N_MCS_DEFAULT_VALUE;
            pEntry->au1ManMCSSet[RADIO_VALUE_2] = DOT11N_MCS_DEFAULT_VALUE;
            u1RetVal = FNP_SUCCESS;
            break;
        }
        case FS_RADIO_HW_STA_DOT11N_MCS_RATESET:
        {
            tRadioIfNpWrDot11StaMCSRate *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpWrDot11StaMCSRate;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwDot11MCSRateSet (pEntry->RadInfo,
                                                 pEntry->au1StaAuthMac,
                                                 pEntry->au1MCSRateSet);
            break;
        }
        case FS_RADIO_HW_SET_DOT11N:
        {
            tRadioIfNpWrConfigSetDot11n *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpConfigSetDot11n;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }

            u1RetVal = FsRadioHwSetDot11N (pEntry->RadInfo, pEntry);
            break;
        }
#ifdef ROGUEAP_WANTED

        case FS_RADIO_HW_SET_ROUGE_AP_DETECTION:
        {
            tRadioIfNpWrConfigSetRougeApDetection *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpWrSetRougeApDetectionGroupId;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }

            u1RetVal =
                FsRadioHwSetRougeApDetectionGroupId (pEntry->RadInfo, pEntry);
            break;
        }

#endif
        case FS_RADIO_HW_SET_WPA_PROTECTION:
        {
            tRadioIfNpWrSetWpaIEParams *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpWrSetWpaIEParams;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }

            u1RetVal = FsRadioHwSetWpaProtection (pEntry->RadInfo, pEntry);
            break;

        }

        case FS_RADIO_HW_SET_WLAN_AC_SUPP_MODE:
        {
            tRadioIfNpWrConfigSetDot11acSuppMode *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpConfigSetDot11acSuppMode;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwSetDot11acSuppMode (pEntry->RadInfo,
                                                    pEntry->
                                                    au1ValDot11SuppMode);
            break;
        }
        case FS_RADIO_HW_GET_WLAN_AC_SUPP_MODE:
        {
            tRadioIfNpWrConfigGetDot11acSuppMode *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpConfigGetDot11acSuppMode;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwGetDot11acSuppMode (pEntry->RadInfo,
                                                    pEntry->
                                                    au1ValDot11SuppMode);
            break;
        }

        case FS_RADIO_HW_SET_DOT11AC:
        {
            tRadioIfNpWrConfigSetDot11ac *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpConfigSetDot11ac;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }

            u1RetVal = FsRadioHwSetDot11AC (pEntry->RadInfo, pEntry);
            break;
        }
        case FS_RADIO_HW_GET_SUPP_RADIO_TYPE:
        {
            tRadioIfNpWrFsGetDot11SuppRadioType *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpWrGetDot11SuppRadioType;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }

            u1RetVal = FsRadioHwGetSuppRadioType (pEntry->RadInfo,
                                                  &pEntry->u4SuppRadioType);
            break;
        }
        case FS_RADIO_HW_GET_DRIVER_VHT_CAPABLITIES:
        {
            tRadioIfNpWrFsGetDot11AcCapaParams *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpWrGetDot11AcCapaParams;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }

            u1RetVal = FsRadioHwGetDriverVhtCapablities (pEntry->RadInfo,
                                                         &pEntry->u4VhtCapInfo);
            break;
        }
        case FS_RADIO_HW_GET_DRIVER_MAX_MPDU_LEN:
        {
            tRadioIfNpWrFsGetDot11AcCapaParams *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpWrGetDot11AcCapaParams;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }

            u1RetVal = FsRadioHwGetDriverMaxMPDULen (pEntry->RadInfo,
                                                     &pEntry->u1MaxMPDULen);
            break;
        }
        case FS_RADIO_HW_GET_DRIVER_RX_LDPC:
        {
            tRadioIfNpWrFsGetDot11AcCapaParams *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpWrGetDot11AcCapaParams;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }

            u1RetVal = FsRadioHwGetDriverRxLdpc (pEntry->RadInfo,
                                                 &pEntry->u1RxLpdc);
            break;
        }
        case FS_RADIO_HW_GET_DRIVER_SHORT_GI_80:
        {
            tRadioIfNpWrFsGetDot11AcCapaParams *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpWrGetDot11AcCapaParams;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }

            u1RetVal = FsRadioHwGetDriverShortGi80 (pEntry->RadInfo,
                                                    &pEntry->u1ShortGi80);
            break;
        }
        case FS_RADIO_HW_GET_DRIVER_SHORT_GI_160:
        {
            tRadioIfNpWrFsGetDot11AcCapaParams *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpWrGetDot11AcCapaParams;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }

            u1RetVal = FsRadioHwGetDriverShortGi160 (pEntry->RadInfo,
                                                     &pEntry->u1ShortGi160);
            break;
        }
        case FS_RADIO_HW_GET_DRIVER_TX_STBC:
        {
            tRadioIfNpWrFsGetDot11AcCapaParams *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpWrGetDot11AcCapaParams;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }

            u1RetVal = FsRadioHwGetDriverTxStbc (pEntry->RadInfo,
                                                 &pEntry->u1TxStbc);
            break;
        }
        case FS_RADIO_HW_GET_DRIVER_RX_STBC:
        {
            tRadioIfNpWrFsGetDot11AcCapaParams *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpWrGetDot11AcCapaParams;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }

            u1RetVal = FsRadioHwGetDriverRxStbc (pEntry->RadInfo,
                                                 &pEntry->u1RxStbc);
            break;
        }
        case FS_RADIO_HW_GET_DRIVER_SU_BEAMFORMER:
        {
            tRadioIfNpWrFsGetDot11AcCapaParams *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpWrGetDot11AcCapaParams;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }

            u1RetVal = FsRadioHwGetDriverSuBeamFormer (pEntry->RadInfo,
                                                       &pEntry->u1SuBeamFormer);
            break;
        }
        case FS_RADIO_HW_GET_DRIVER_SU_BEAMFORMEE:
        {
            tRadioIfNpWrFsGetDot11AcCapaParams *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpWrGetDot11AcCapaParams;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }

            u1RetVal = FsRadioHwGetDriverSuBeamFormee (pEntry->RadInfo,
                                                       &pEntry->u1SuBeamFormee);
            break;
        }
        case FS_RADIO_HW_GET_DRIVER_SUPP_BEAM_FROM_ANT:
        {
            tRadioIfNpWrFsGetDot11AcCapaParams *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpWrGetDot11AcCapaParams;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }

            u1RetVal = FsRadioHwGetDriverSuppBeamFormAnt (pEntry->RadInfo,
                                                          &pEntry->
                                                          u1SuppBeamFormAnt);
            break;
        }
        case FS_RADIO_HW_GET_DRIVER_SOUND_DIMENSION:
        {
            tRadioIfNpWrFsGetDot11AcCapaParams *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpWrGetDot11AcCapaParams;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }

            u1RetVal = FsRadioHwGetDriverSoundDimension (pEntry->RadInfo,
                                                         &pEntry->
                                                         u1SoundDimension);
            break;
        }
        case FS_RADIO_HW_GET_DRIVER_MU_BEAMFORMER:
        {
            tRadioIfNpWrFsGetDot11AcCapaParams *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpWrGetDot11AcCapaParams;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }

            u1RetVal = FsRadioHwGetDriverMuBeamFormer (pEntry->RadInfo,
                                                       &pEntry->u1MuBeamFormer);
            break;
        }
        case FS_RADIO_HW_GET_DRIVER_MU_BEAMFORMEE:
        {
            tRadioIfNpWrFsGetDot11AcCapaParams *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpWrGetDot11AcCapaParams;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }

            u1RetVal = FsRadioHwGetDriverMuBeamFormee (pEntry->RadInfo,
                                                       &pEntry->u1MuBeamFormee);
            break;
        }
        case FS_RADIO_HW_GET_DRIVER_VHT_TX_OP_PS:
        {
            tRadioIfNpWrFsGetDot11AcCapaParams *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpWrGetDot11AcCapaParams;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }

            u1RetVal = FsRadioHwGetDriverVhtTxOpPs (pEntry->RadInfo,
                                                    &pEntry->u1VhtTxOpPs);
            break;
        }
        case FS_RADIO_HW_GET_DRIVER_HTC_VHTCAPABLE:
        {
            tRadioIfNpWrFsGetDot11AcCapaParams *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpWrGetDot11AcCapaParams;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }

            u1RetVal = FsRadioHwGetDriverHtcVhtCapable (pEntry->RadInfo,
                                                        &pEntry->
                                                        u1HtcVhtCapable);
            break;
        }
        case FS_RADIO_HW_GET_DRIVER_VHT_MAXAMPDU:
        {
            tRadioIfNpWrFsGetDot11AcCapaParams *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpWrGetDot11AcCapaParams;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }

            u1RetVal = FsRadioHwGetDriverVhtMaxAMPDU (pEntry->RadInfo,
                                                      &pEntry->u1VhtMaxAMPDU);
            break;
        }
        case FS_RADIO_HW_GET_DRIVER_VHT_LINK_ADAPTION:
        {
            tRadioIfNpWrFsGetDot11AcCapaParams *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpWrGetDot11AcCapaParams;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }

            u1RetVal = FsRadioHwGetDriverVhtLinkAdaption (pEntry->RadInfo,
                                                          &pEntry->
                                                          u1VhtLinkAdaption);
            break;
        }
        case FS_RADIO_HW_GET_DRIVER_VHT_RX_ANTENNA:
        {
            tRadioIfNpWrFsGetDot11AcCapaParams *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpWrGetDot11AcCapaParams;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }

            u1RetVal = FsRadioHwGetDriverVhtRxAntenna (pEntry->RadInfo,
                                                       &pEntry->u1VhtRxAntenna);
            break;
        }
        case FS_RADIO_HW_GET_DRIVER_VHT_TX_ANTENNA:
        {
            tRadioIfNpWrFsGetDot11AcCapaParams *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpWrGetDot11AcCapaParams;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }

            u1RetVal = FsRadioHwGetDriverVhtTxAntenna (pEntry->RadInfo,
                                                       &pEntry->u1VhtTxAntenna);
            break;
        }
        case FS_RADIO_HW_GET_DRIVER_SUPP_CHANNEL_WIDTH:
        {
            tRadioIfNpWrFsGetDot11AcCapaParams *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpWrGetDot11AcCapaParams;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }

            u1RetVal = FsRadioHwGetDriverSuppChannelWidth (pEntry->RadInfo,
                                                           &pEntry->
                                                           u1SuppChanWidth);
            break;
        }
        case FS_RADIO_HW_GET_DRIVER_VHT_SUPP_MCS:
        {
            tRadioIfNpWrFsGetDot11AcCapaParams *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpWrGetDot11AcCapaParams;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }

            u1RetVal = FsRadioHwGetDriverVhtSuppMcs (pEntry->RadInfo,
                                                     pEntry->au1SuppVhtCapaMcs);
            break;
        }
        case FS_RADIO_HW_GET_MULTIDOMAIN_INFO:
        {
            tRadioIfNpWrFsHwGetMultiDomainInfo *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpWrGetMultiDomainInfo;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwGetMultiDomainCapabilityInfo (pEntry->RadInfo,
                                                              pEntry);
            break;
        }

        case FS_RADIO_HW_ASSEMBLE_BEACON_FRAMES:
        {
            tRadioIfNpWrFsHwGetMultiDomainInfo *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpWrGetMultiDomainInfo;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwGetMultiDomainCapabilityInfo (pEntry->RadInfo,
                                                              pEntry);
            FsRadioAssembleBeaconFrame (&(pEntry->RadInfo));
            break;
        }
        case FS_RADIO_HW_GET_SCAN_DETAILS:
        {
            tRadioIfNpWrFsHwGetScanDetails *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpWrGetScanDetails;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwGetScanDetails (pEntry->RadInfo);
            break;
        }
        case FS_RADIO_HW_GET_DRIVER_HT_CAPABLITIES:
        {
            tRadioIfNpWrFsGetDot11NCapaParams *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpWrGetDot11NCapaParams;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }

            u1RetVal = FsRadioHwGetDriverHtCapablities (pEntry->RadInfo,
                                                        pEntry);
            break;
        }
        case FS_RADIO_HW_GET_DRIVER_EXT_HT_CAPABLITIES:
        {
            tRadioIfNpWrFsGetDot11NExtCapaParams *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpConfigGetDot11nExtCap;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }

            u1RetVal = FsRadioHwGetDriverExtHtCapablities (pEntry->RadInfo,
                                                           &pEntry->
                                                           u2ExtSuppHtCapInfo);
            break;
        }
        case FS_RADIO_HW_GET_DRIVER_BEAMFORM_CAPABLITIES:
        {
            tRadioIfNpWrFsGetDot11NBeamFormCapaParams *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpConfigGetDot11nBeamFormCap;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }

            u1RetVal = FsRadioHwGetDriverBeamFormCapablities (pEntry->RadInfo,
                                                              &pEntry->
                                                              u4SuppBeamFormHtCapInfo);
            break;
        }

        case FS_RADIO_HW_GET_DRIVER_HT_SUPP_MCS:
        {
            tRadioIfNpWrFsGetSuppMcsParams *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpWrGetDot11NsuppMcsParams;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }

            u1RetVal = FsRadioHwGetDriverHtSuppMcs (pEntry->RadInfo,
                                                    pEntry->au1SuppHtCapaMcs);
            break;
        }
        case FS_RADIO_HW_GET_DRIVER_HT_AMPDU:
        {
            tRadioIfNpWrFsGetAmpduParams *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpWrGetDot11NampduParams;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }

            u1RetVal = FsRadioHwGetDriverHtAMPDU (pEntry->RadInfo, pEntry);
            break;
        }

        case FS_RADIO_HW_GET_DRIVER_CHNL_CAPABLITIES:
        {
            tRadioIfNpWrFsGetChannelInfo *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpWrGetChannelInfo;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwGetDriverChnlCapablities (pEntry->RadInfo);
            break;
        }
        case FS_RADIO_DFS_ADD_BEACON_FRAMES:
        {
            tRadioIfNpWrConfigGetDot11Channel *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpConfigGetDot11Channel;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwDFSResendBeacon (pEntry->RadInfo,
                                                 pEntry->u2ChannelNum);
            break;
        }
        case FS_RADIO_WLAN_ISOLATION:
        {
            tRadioIfNpWrSetWlanIsolation *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpWrSetWlanIsolation;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }

            u1RetVal = FsRadioWlanIsolationSet (pEntry->RadInfo,
                                                (INT2 *) &(pEntry->
                                                           u2WlanIsolationiEnable));
            break;
        }

#ifdef PMF_WANTED
        case FS_RADIO_HW_GET_RSNA_SEQ_NO:
        {
            tRadioIfNpWrFsGetSeqNum *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpWrGetSeqNum;

            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }

            u1RetVal = FsRadioHwGetSeqNum (&pEntry->RadInfo,
                                           &pEntry->i4KeyIndex,
                                           &pEntry->au1IGTKPktNum);
            break;
        }
#endif
        case FS_RADIO_HW_SET_DOT11N_PARAMS:
        {
            tRadioIfNpWrFsHwSetDot11nParams *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpWrSetDot11nParams;

            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }

            u1RetVal = FsRadioHwSetDot11nParams (pEntry->RadInfo,
                                                 pEntry->u1AMPDUStatus,
                                                 pEntry->u1AMPDUSubFrame,
                                                 pEntry->u2AMPDULimit,
                                                 pEntry->u1AMSDUStatus,
                                                 pEntry->u2AMSDULimit);
            break;
        }
        case FS_RADIO_DFS_UPDATE_RADAR_STAT:
        {
            tRadioIfNpWrFsHwUpdateRadarStatus *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpWrUpdateRadarStatus;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwSetRadarFound (pEntry->RadInfo);
            break;
        }

#ifdef BAND_SELECT_WANTED

        case FS_RADIO_DEL_STA_BANDSTEER:
        {
            tRadioIfNpWrFsDelStaBandSteer *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpWrDelStaBandSteer;

            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwStaDelBandSteer (pEntry->RadInfo,
                                                 pEntry->stationMacAddress);
            break;
        }
        case FS_RADIO_HW_SET_BANDSELECT:
        {
            tRadioIfNpWrFsBandSelectStatus *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpWrFsBandSelectStatus;

            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwSetBandwidthStatus (pEntry->RadInfo, pEntry);
            break;
        }

#endif
        case FS_RADIO_HW_SET_CHANNEL_NUM_WLAN:
        {
            tRadioIfNpWrConfigSetDot11Channel *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpConfigSetDot11Channel;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRadioHwSetDot11ChannelForWlan (pEntry->RadInfo,
                                                        pEntry->u2ChannelNum);
            break;
        }

        case FS_RADIO_HW_GET_WLAN_IFNAME:
        {
            tRadioIfNpWrGetWlanIfName *pEntry = NULL;
            pEntry = &pRadioIfNpModInfo->RadioIfNpWrGetWlanIfName;

            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }

            u1RetVal = FsRadioHwGetWlanIfName (pEntry->u1RadioId,
                                               pEntry->u1WlanIdInRadio,
                                               pEntry->au1WlanIntfName,
                                               &pEntry->u1WlanId);
            break;
        }
        case FS_RADIO_HW_WLAN_ADD_VLAN:
        {
            {
                tRadioIfNpWrWlanAddVlanIntf *pEntry = NULL;
                pEntry = &pRadioIfNpModInfo->RadioIfNpWrWlanAddVlanIntf;
                if (NULL == pEntry)
                {
                    u1RetVal = FNP_FAILURE;
                    break;
                }
                u1RetVal = FsRadioHwWlanAddVlanIntf (pEntry->WlanVlanInf);
                break;

            }
            break;
        }
    }
    return (u1RetVal);
}

#ifdef KERNEL_CAPWAP_WANTED
VOID
RadioNpWrHwVlantoWlanMapping (UINT1 u1CreateStatus,
                              UINT2 u2VlanId, UINT1 *pu1WlanName,
                              UINT1 u1WlanId)
{
    FsRadioHwMapVlanToWlanIntf (u1CreateStatus, u2VlanId, pu1WlanName,
                                u1WlanId);
    return;
}
#endif
#endif
