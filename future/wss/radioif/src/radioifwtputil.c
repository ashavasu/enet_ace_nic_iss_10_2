/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: radioifwtputil.c,v 1.8 2017/11/24 10:37:04 siva Exp $
 *
 * Description: This file handles all the configurations of NPAPI
 * other modules
 ***********************************************************************/
#ifndef __RADIOIFAPUTIL_C__
#define __RADIOIFAPUTIL_C__

#include "radioifinc.h"
#ifdef NPAPI_WANTED
#include "nputil.h"
extern VOID         WssWlanGetWlanIntfName (UINT1 u1RadioId, UINT1 u1WlanId,
                                            UINT1 *pu1WlanIntfName,
                                            UINT1 *pu1WlanId);

#endif

/*****************************************************************************
 * Function Name      : RadioIfSetAntennaDiversity                           *
 *                                                                           *
 * Description        : Configure the Antenna diversity                      *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to the Radio DB              *
 *                    : pu1RadioName  - Radio Interface name                 *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfSetAntennaDiversity (tRadioIfGetDB * pRadioIfGetDB, UINT1 *pu1RadioName)
{
#ifdef NPAPI_WANTED
    tFsHwNp             FsHwNp;

    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));

    NP_UTIL_FILL_PARAMS (FsHwNp,    /* Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_SET_ANTENNA_DIVERSITY,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sFsHwSetAntennaDiversity.
            RadInfo.au1RadioIfName, pu1RadioName, STRLEN (pu1RadioName));
    FsHwNp.RadioIfNpModInfo.unOpCode.sFsHwSetAntennaDiversity.u4RcvDivsity =
        pRadioIfGetDB->RadioIfGetAllDB.u1DiversitySupport;

    /*To Set in the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "NpUtilHwProgram : Values to set in Hw failed\r\n");
        return OSIX_FAILURE;
    }
#endif
    UNUSED_PARAM (pu1RadioName);
    pRadioIfGetDB->RadioIfIsGetAllDB.bDiversitySupport = OSIX_TRUE;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfSetOperationalRate                           *
 *                                                                           *
 * Description        : Configure the Operational Rate                      *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to the Radio DB              *
 *                    : pu1RadioName  - Radio Interface name                 *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfSetOperationalRate (tRadioIfGetDB * pRadioIfGetDB, UINT1 *pu1RadioName)
{
#ifdef NPAPI_WANTED
    tFsHwNp             FsHwNp;
    UINT1               au1Wname[WLAN_NAME_MAX_LEN];
    UINT1               u1Index;

    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));

    NP_UTIL_FILL_PARAMS (FsHwNp,    /* Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_SET_BIT_RATES,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11BitRates.RadInfo.
        u4RadioIfIndex = pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11BitRates.RadInfo.
            au1RadioIfName, pu1RadioName, STRLEN (pu1RadioName));

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11BitRates.
        RadInfo.au2WlanIfIndex[0] = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;
    SPRINTF ((CHR1 *) au1Wname, "%s%d", RADIO_ATH_INTF_NAME,
             pRadioIfGetDB->RadioIfGetAllDB.u1WlanId - 1);
    STRCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11BitRates.
            RadInfo.au1WlanIfname[0], au1Wname);
    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11BitRates.RadInfo.u1WlanId =
        pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;

    for (u1Index = 0; u1Index < 8; u1Index++)
    {
        FsHwNp.RadioIfNpModInfo.unOpCode.
            sConfigSetDot11BitRates.au4BitRate[u1Index] =
            pRadioIfGetDB->RadioIfGetAllDB.au1OperationalRate[u1Index];

        /*                         TODO                            
         * There is a mismatch in the data type and data length between
         * pRadioIfGetDB - UNIT1 and FsHwNp - UNIT4 data rate arrays, 
         * please check */
    }

    /*To Set in the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "NpUtilHwProgram : Values to set in Hw failed\r\n");
        return OSIX_FAILURE;
    }
#endif
    UNUSED_PARAM (pu1RadioName);
    pRadioIfGetDB->RadioIfIsGetAllDB.bOperationalRate = OSIX_TRUE;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfSetDSCCurrentChannel                          *
 *                                                                           *
 * Description        : Configure the channel for Radio B Types              *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to the Radio DB              *
 *                    : pu1RadioName  - Radio Interface name                 *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfSetDSCCurrentChannel (tRadioIfGetDB * pRadioIfGetDB, UINT1 *pu1RadioName)
{
#ifdef NPAPI_WANTED
    tFsHwNp             FsHwNp;
    UINT1               au1Wname[WLAN_NAME_MAX_LEN];

    MEMSET (au1Wname, 0, WLAN_NAME_MAX_LEN);
    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_SET_CHANNEL_NUM,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11Channel.
        RadInfo.u4RadioIfIndex = pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11Channel.
            RadInfo.au1RadioIfName, pu1RadioName, STRLEN (pu1RadioName));

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11Channel.
        RadInfo.au2WlanIfIndex[0] = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;

    WssWlanGetWlanIntfName (pRadioIfGetDB->RadioIfGetAllDB.u1RadioId,
                            pRadioIfGetDB->RadioIfGetAllDB.u1WlanId,
                            FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigSetDot11Channel.RadInfo.au1WlanIfname[0],
                            &FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigSetDot11Channel.RadInfo.u1WlanId);

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11Channel.u2ChannelNum =
        pRadioIfGetDB->RadioIfGetAllDB.u1CurrentChannel;

    /*To set in the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "NpUtilHwProgram : Values to set in Hw failed\r\n");
    }
#endif
    UNUSED_PARAM (pu1RadioName);
    pRadioIfGetDB->RadioIfIsGetAllDB.bCurrentChannel = OSIX_TRUE;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfSetRTSThreshold                               *
 *                                                                           *
 * Description        : Configure the RTS threshold for interface            *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to the Radio DB              *
 *                    : pu1RadioName  - Radio Interface name                 *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfSetRTSThreshold (tRadioIfGetDB * pRadioIfGetDB, UINT1 *pu1RadioName)
{
#ifdef NPAPI_WANTED
    tFsHwNp             FsHwNp;

    UINT1               au1Wname[WLAN_NAME_MAX_LEN];

    MEMSET (au1Wname, 0, WLAN_NAME_MAX_LEN);
    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_SET_RTS_THRESHOLD,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms
                                 */

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11RTSThreshold.RadInfo.
        u4RadioIfIndex = pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.
            sConfigSetDot11RTSThreshold.RadInfo.au1RadioIfName, pu1RadioName,
            STRLEN (pu1RadioName));
    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11RTSThreshold.
        u2RTSThreshold = pRadioIfGetDB->RadioIfGetAllDB.u2RTSThreshold;

    WssWlanGetWlanIntfName (pRadioIfGetDB->RadioIfGetAllDB.u1RadioId,
                            pRadioIfGetDB->RadioIfGetAllDB.u1WlanId,
                            FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigSetDot11RTSThreshold.RadInfo.
                            au1WlanIfname[0],
                            &FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigSetDot11RTSThreshold.RadInfo.u1WlanId);
    /*To set in the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "NpUtilHwProgram : Values to set in Hw failed\r\n");
        return OSIX_FAILURE;
    }
#endif
    UNUSED_PARAM (pu1RadioName);
    pRadioIfGetDB->RadioIfIsGetAllDB.bRTSThreshold = OSIX_TRUE;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfSetFragmentationThreshold                     *
 *                                                                           *
 * Description        : Configure the fragmentation threshold for interface  *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to the Radio DB              *
 *                    : pu1RadioName  - Radio Interface name                 *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfSetFragmentationThreshold (tRadioIfGetDB * pRadioIfGetDB,
                                  UINT1 *pu1RadioName)
{
#ifdef NPAPI_WANTED
    tFsHwNp             FsHwNp;
    UINT1               au1Wname[WLAN_NAME_MAX_LEN];

    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));
    MEMSET (au1Wname, 0, WLAN_NAME_MAX_LEN);

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_SET_FRAG_THRESHOLD,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms
                                 */

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11FragThreshold.
        RadInfo.u4RadioIfIndex = pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.
            sConfigSetDot11FragThreshold.RadInfo.au1RadioIfName, pu1RadioName,
            STRLEN (pu1RadioName));
    FsHwNp.RadioIfNpModInfo.unOpCode.
        sConfigSetDot11FragThreshold.u2FragThreshold =
        pRadioIfGetDB->RadioIfGetAllDB.u2FragmentationThreshold;

    WssWlanGetWlanIntfName (pRadioIfGetDB->RadioIfGetAllDB.u1RadioId,
                            pRadioIfGetDB->RadioIfGetAllDB.u1WlanId,
                            FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigSetDot11FragThreshold.RadInfo.
                            au1WlanIfname[0],
                            &FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigSetDot11FragThreshold.RadInfo.u1WlanId);

    /*To set in the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "NpUtilHwProgram : Values to set in Hw failed\r\n");
        return OSIX_FAILURE;
    }
#endif
    UNUSED_PARAM (pu1RadioName);
    pRadioIfGetDB->RadioIfIsGetAllDB.bFragmentationThreshold = OSIX_TRUE;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfSetOFDMCurrentChannel                         *
 *                                                                           *
 * Description        : Configure the Channel for Radio A Type               *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to the Radio DB              *
 *                    : pu1RadioName  - Radio Interface name                 *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfSetOFDMCurrentChannel (tRadioIfGetDB * pRadioIfGetDB,
                              UINT1 *pu1RadioName)
{
#ifdef NPAPI_WANTED
    tFsHwNp             FsHwNp;
    UINT1               au1Wname[WLAN_NAME_MAX_LEN];

    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));
    MEMSET (au1Wname, 0, WLAN_NAME_MAX_LEN);

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_SET_CHANNEL_NUM,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11Channel.RadInfo.
        u4RadioIfIndex = pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11Channel.RadInfo.
            au1RadioIfName, pu1RadioName, STRLEN (pu1RadioName));

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11Channel.
        RadInfo.au2WlanIfIndex[0] = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;

    WssWlanGetWlanIntfName (pRadioIfGetDB->RadioIfGetAllDB.u1RadioId,
                            pRadioIfGetDB->RadioIfGetAllDB.u1WlanId,
                            FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigSetDot11Channel.RadInfo.au1WlanIfname[0],
                            &FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigSetDot11Channel.RadInfo.u1WlanId);

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11Channel.u2ChannelNum =
        pRadioIfGetDB->RadioIfGetAllDB.u1CurrentChannel;

    /*To Set in the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "NpUtilHwProgram(OFDM - Channel): Values to be set "
                     "in Hw failed\r\n");
    }
#endif
    UNUSED_PARAM (pu1RadioName);
    pRadioIfGetDB->RadioIfIsGetAllDB.bCurrentChannel = OSIX_TRUE;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfSetOFDMCurrentChannelForWlan                  *
 *                                                                           *
 * Description        : Configure the Channel for Radio A Type               *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to the Radio DB              *
 *                    : pu1RadioName  - Radio Interface name                 *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfSetOFDMCurrentChannelForWlan (tRadioIfGetDB * pRadioIfGetDB,
                                     UINT1 *pu1RadioName)
{
#ifdef NPAPI_WANTED
    tFsHwNp             FsHwNp;
    UINT1               au1Wname[WLAN_NAME_MAX_LEN];

    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));
    MEMSET (au1Wname, 0, WLAN_NAME_MAX_LEN);

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_SET_CHANNEL_NUM_WLAN,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11Channel.RadInfo.
        u4RadioIfIndex = pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11Channel.RadInfo.
            au1RadioIfName, pu1RadioName, STRLEN (pu1RadioName));

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11Channel.
        RadInfo.au2WlanIfIndex[0] = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;

    WssWlanGetWlanIntfName (pRadioIfGetDB->RadioIfGetAllDB.u1RadioId,
                            pRadioIfGetDB->RadioIfGetAllDB.u1WlanId,
                            FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigSetDot11Channel.RadInfo.au1WlanIfname[0],
                            &FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigSetDot11Channel.RadInfo.u1WlanId);

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11Channel.u2ChannelNum =
        pRadioIfGetDB->RadioIfGetAllDB.u1CurrentChannel;

    /*To Set in the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "NpUtilHwProgram(OFDM - Channel): Values to be set "
                     "in Hw failed\r\n");
    }
#endif
    UNUSED_PARAM (pu1RadioName);
    pRadioIfGetDB->RadioIfIsGetAllDB.bCurrentChannel = OSIX_TRUE;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfAddChanlSwitIE                                *
 *                                                                           *
 * Description        : Add Channel Sitch IE in Beacon for Radio A Type      *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to the Radio DB              *
 *                    : pu1RadioName  - Radio Interface name                 *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *******************************************************************************/
UINT1
RadioIfAddChanlSwitIE (tRadioIfGetDB * pRadioIfGetDB, UINT1 *pu1RadioName)
{
#ifdef NPAPI_WANTED
    tFsHwNp             FsHwNp;
    UINT1               au1Wname[WLAN_NAME_MAX_LEN];

    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));
    MEMSET (au1Wname, 0, WLAN_NAME_MAX_LEN);

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_ADD_CHANNEL_SWIT_IE,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11Channel.RadInfo.
        u4RadioIfIndex = pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11Channel.RadInfo.
            au1RadioIfName, pu1RadioName, STRLEN (pu1RadioName));

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11Channel.
        RadInfo.au2WlanIfIndex[0] = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;

    SPRINTF ((CHR1 *) au1Wname, "%s%d", RADIO_ATH_INTF_NAME,
             pRadioIfGetDB->RadioIfGetAllDB.u1WlanId - 1);
    STRCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11Channel.
            RadInfo.au1WlanIfname[0], au1Wname);

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11Channel.u2ChannelNum =
        pRadioIfGetDB->RadioIfGetAllDB.u1CurrentChannel;

    /*To Set in the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "NpUtilHwProgram( CSA ): Values to be set "
                     "in Hw failed\r\n");
    }
#endif
    UNUSED_PARAM (pu1RadioName);
    pRadioIfGetDB->RadioIfIsGetAllDB.bCurrentChannel = OSIX_TRUE;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfRemoveChanlSwitIE                             *
 *                                                                           *
 * Description        : Remove Channel Sitch IE in Beacon for Radio A Type   *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to the Radio DB              *
 *                    : pu1RadioName  - Radio Interface name                 *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 ********************************************************************************/
UINT1
RadioIfRemoveChanlSwitIE (tRadioIfGetDB * pRadioIfGetDB)
{
#ifdef NPAPI_WANTED
    tFsHwNp             FsHwNp;
    UINT1               au1Wname[WLAN_NAME_MAX_LEN];
    UINT1               u1RadioId = 0;

    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));
    MEMSET (au1Wname, 0, WLAN_NAME_MAX_LEN);

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_REMOVE_CHANNEL_SWIT_IE,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetDot11Channel.RadInfo.
        u4RadioIfIndex = pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
    /*MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11Channel.RadInfo.
       au1RadioIfName, pu1RadioName, STRLEN (pu1RadioName)); */
    u1RadioId = pRadioIfGetDB->RadioIfGetAllDB.u1RadioId;

    SPRINTF ((CHR1 *) FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetDot11Channel.
             RadInfo.au1RadioIfName, "%s%d", RADIO_INTF_NAME, u1RadioId - 1);

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetDot11Channel.
        RadInfo.au2WlanIfIndex[0] = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;

    SPRINTF ((CHR1 *) au1Wname, "%s%d", RADIO_ATH_INTF_NAME,
             pRadioIfGetDB->RadioIfGetAllDB.u1WlanId - 1);
    STRCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetDot11Channel.
            RadInfo.au1WlanIfname[0], au1Wname);

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetDot11Channel.u2ChannelNum =
        pRadioIfGetDB->RadioIfGetAllDB.u1CurrentChannel;

    /*To Set in the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "NpUtilHwProgram( CSA ): Values to be set "
                     "in Hw failed\r\n");
    }
#endif
    pRadioIfGetDB->RadioIfIsGetAllDB.bCurrentChannel = OSIX_TRUE;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfAddChanlWrapIE                                *
 *                                                                           *
 * Description        : Add Channel Sitch IE in Beacon for Radio A Type      *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to the Radio DB              *
 *                    : pFsHwNp  - Pointer to tFsHwNp                        *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *******************************************************************************/
#ifdef NPAPI_WANTED
VOID
RadioIfAddChanlWrapIE (tRadioIfGetDB * pRadioIfGetDB, tFsHwNp * pFsHwNp)
{

/*Channel Switch Wrapper Element*/
    pFsHwNp->RadioIfNpModInfo.unOpCode.sConfigSetDot11ac.
        ChanSwitchWrapper.isOptional = OSIX_TRUE;

/*New Country sub element*/
    pFsHwNp->RadioIfNpModInfo.unOpCode.sConfigSetDot11ac.
        ChanSwitchWrapper.WrapperSubElem.NewCountry.isOptional = OSIX_TRUE;
/*Wide bandwidth Channel Switch sub element*/
    pFsHwNp->RadioIfNpModInfo.unOpCode.sConfigSetDot11ac.
        ChanSwitchWrapper.WrapperSubElem.ChanwidthSwitch.isOptional = OSIX_TRUE;
    pFsHwNp->RadioIfNpModInfo.unOpCode.sConfigSetDot11ac.
        ChanSwitchWrapper.WrapperSubElem.ChanwidthSwitch.
        u1ChanWidth = pRadioIfGetDB->RadioIfGetAllDB.
        Dot11AcOperParams.u1VhtChannelWidth;
    pFsHwNp->RadioIfNpModInfo.unOpCode.sConfigSetDot11ac.
        ChanSwitchWrapper.WrapperSubElem.ChanwidthSwitch.
        u1CenterFcy0 = pRadioIfGetDB->RadioIfGetAllDB.
        Dot11AcOperParams.u1CenterFcy0;
    pFsHwNp->RadioIfNpModInfo.unOpCode.sConfigSetDot11ac.
        ChanSwitchWrapper.WrapperSubElem.ChanwidthSwitch.
        u1CenterFcy1 = pRadioIfGetDB->RadioIfGetAllDB.
        Dot11AcOperParams.u1CenterFcy1;

/*Vht Transmit Power Envelope sub element*/
    MEMCPY (&(pFsHwNp->RadioIfNpModInfo.unOpCode.sConfigSetDot11ac.
              ChanSwitchWrapper.WrapperSubElem.VhtTxPower), &(pFsHwNp->
                                                              RadioIfNpModInfo.
                                                              unOpCode.
                                                              sConfigSetDot11ac.
                                                              VhtTxPower),
            sizeof (tVhtTxPower));
}
#endif
/*****************************************************************************
 * Function Name      : RadioIfSetTxPowerLevel                               *
 *                                                                           *
 * Description        : Configure the Tx power level for both Radio Types    *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to the Radio DB              *
 *                    : pu1RadioName  - Radio Interface name                 *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfSetTxPowerLevel (tRadioIfGetDB * pRadioIfGetDB, UINT1 *pu1RadioName)
{
#if NPAPI_WANTED
    tFsHwNp             FsHwNp;
    UINT1               au1Wname[WLAN_NAME_MAX_LEN];

    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));
    MEMSET (au1Wname, 0, WLAN_NAME_MAX_LEN);

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_SET_TX_CURR_POWER,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    FsHwNp.RadioIfNpModInfo.unOpCode.sFsHwSetTxCurrPower.RadInfo.u4RadioIfIndex
        = pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sFsHwSetTxCurrPower.RadInfo.
            au1RadioIfName, pu1RadioName, STRLEN (pu1RadioName));

    FsHwNp.RadioIfNpModInfo.unOpCode.sFsHwSetTxCurrPower.
        RadInfo.au2WlanIfIndex[0] = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;

    WssWlanGetWlanIntfName (pRadioIfGetDB->RadioIfGetAllDB.u1RadioId,
                            pRadioIfGetDB->RadioIfGetAllDB.u1WlanId,
                            FsHwNp.RadioIfNpModInfo.unOpCode.
                            sFsHwSetTxCurrPower.RadInfo.au1WlanIfname[0],
                            &FsHwNp.RadioIfNpModInfo.unOpCode.
                            sFsHwSetTxCurrPower.RadInfo.u1WlanId);

    FsHwNp.RadioIfNpModInfo.unOpCode.sFsHwSetTxCurrPower.i4TxCurrPower =
        (INT4) pRadioIfGetDB->RadioIfGetAllDB.i2CurrentTxPower;

    /*To set in the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "NpUtilHwProgram (Tx Power) : Values to set in Hw failed\r\n");
        return OSIX_FAILURE;
    }
#endif
    UNUSED_PARAM (pu1RadioName);
    pRadioIfGetDB->RadioIfIsGetAllDB.bTxPowerLevel = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bCurrentTxPower = OSIX_TRUE;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      :RadioIfSetQosInfoParams                               *
 *                                                                           *
 * Description        : Configure the QoS Params for the interface           *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to the Radio DB              *
 *                    : pu1RadioName  - Radio Interface name                 *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/

UINT1
RadioIfSetQosInfoParams (tRadioIfGetDB * pRadioIfGetDB, UINT1 *pu1RadioName)
{
#ifdef NPAPI_WANTED
    tFsHwNp             FsHwNp;
    UINT1               u1RadioId = 0;
    UINT1               au1Wname[WLAN_NAME_MAX_LEN];
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (au1Wname, 0, WLAN_NAME_MAX_LEN);
    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_QOS_INFO,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    u1RadioId = pRadioIfGetDB->RadioIfGetAllDB.u1RadioId;
    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11QosInfoTableParams.
        RadInfo.au2WlanIfIndex[0] = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;
    WssWlanGetWlanIntfName (u1RadioId,
                            pRadioIfGetDB->RadioIfGetAllDB.u1WlanId,
                            FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigSetDot11QosInfoTableParams.RadInfo.
                            au1WlanIfname[0],
                            &FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigSetDot11QosInfoTableParams.RadInfo.u1WlanId);

    FsHwNp.RadioIfNpModInfo.unOpCode.
        sConfigSetDot11QosInfoTableParams.u4QosInfo =
        pRadioIfGetDB->RadioIfGetAllDB.u4QosInfo;
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "NpUtilHwProgram : Values to set CwMin in Hw failed\r\n");
        return OSIX_FAILURE;
    }
#endif
    UNUSED_PARAM (pu1RadioName);
    UNUSED_PARAM (pRadioIfGetDB);
    return OSIX_SUCCESS;

}

/*****************************************************************************
 * Function Name      : RadioIfSetEdcaParams                                 *
 *                                                                           *
 * Description        : Configure the QoS Params for the interface           *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to the Radio DB              *
 *                    : pu1RadioName  - Radio Interface name                 *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfSetEdcaParams (tRadioIfGetDB * pRadioIfGetDB, UINT1 *pu1RadioName)
{
#ifdef NPAPI_WANTED
    tFsHwNp             FsHwNp;
    UINT1               u1RadioId = 0;
    UINT1               u1Index = 0;
    UINT1               au1Wname[WLAN_NAME_MAX_LEN];
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (au1Wname, 0, WLAN_NAME_MAX_LEN);
    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_SET_EDCA_PARAMS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    u1RadioId = pRadioIfGetDB->RadioIfGetAllDB.u1RadioId;
    WssWlanGetWlanIntfName (u1RadioId,
                            pRadioIfGetDB->RadioIfGetAllDB.u1WlanId,
                            FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigSetDot11EDCATableParams.RadInfo.
                            au1WlanIfname[0],
                            &FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigSetDot11EDCATableParams.RadInfo.u1WlanId);
    for (u1Index = 0; u1Index < RADIO_QOS_PROFILE; u1Index++)
    {
        /*filling EDCA params */
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11EDCATableParams.RadInfo.
            u4RadioIfIndex = pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
        MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.
                sConfigSetDot11EDCATableParams.RadInfo.au1RadioIfName,
                pu1RadioName, STRLEN (pu1RadioName));
        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
            pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
        FsHwNp.RadioIfNpModInfo.unOpCode.
            sConfigSetDot11EDCATableParams.i4Dot11EDCATableIndex = u1Index;
        FsHwNp.RadioIfNpModInfo.unOpCode.
            sConfigSetDot11EDCATableParams.i4ValDot11EDCATableCWmin =
            pRadioIfGetDB->RadioIfGetAllDB.au2CwMin[u1Index];
        FsHwNp.RadioIfNpModInfo.unOpCode.
            sConfigSetDot11EDCATableParams.i4ValDot11EDCATableCWmax =
            pRadioIfGetDB->RadioIfGetAllDB.au2CwMax[u1Index];
        FsHwNp.RadioIfNpModInfo.unOpCode.
            sConfigSetDot11EDCATableParams.i4ValDot11EDCATableAifs =
            pRadioIfGetDB->RadioIfGetAllDB.au1Aifsn[u1Index];

        FsHwNp.RadioIfNpModInfo.unOpCode.
            sConfigSetDot11EDCATableParams.i4ValDot11EDCATableTxOpLmt =
            pRadioIfGetDB->RadioIfGetAllDB.au2TxOpLimit[u1Index];

        FsHwNp.RadioIfNpModInfo.unOpCode.
            sConfigSetDot11EDCATableParams.i4ValDot11EDCATableAdmsnCtrl =
            pRadioIfGetDB->RadioIfGetAllDB.au1AdmissionControl[u1Index];
        /*To Set in the Hw */
        if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "NpUtilHwProgram : Values to set EDCA params in Hw failed\r\n");
            return OSIX_FAILURE;
        }
        pRadioIfGetDB->RadioIfIsGetAllDB.bCwMin = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bCwMax = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bAifsn = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bTxOpLimit = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bAdmissionControl = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bQosIndex = OSIX_TRUE;
        pRadioIfGetDB->RadioIfGetAllDB.u1QosIndex = u1Index;
        if (WssIfProcessRadioIfDBMsg
            (WSS_SET_RADIO_QOS_CONFIG_DB_WTP, pRadioIfGetDB) != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "Failed to SET IEEE Wtp QoS from RadioDB \r\n");
            return OSIX_FAILURE;
        }
    }
#endif
    UNUSED_PARAM (pu1RadioName);
    UNUSED_PARAM (pRadioIfGetDB);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfSetQosCwMin                                   *
 *                                                                           *
 * Description        : Configure the QoS Params for the interface           *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to the Radio DB              *
 *                    : pu1RadioName  - Radio Interface name                 *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfSetQosCwMin (tRadioIfGetDB * pRadioIfGetDB, UINT1 *pu1RadioName)
{
#ifdef NPAPI_WANTED
    tFsHwNp             FsHwNp;
    UINT1               u1Index = 0;
    UINT1               au1Wname[WLAN_NAME_MAX_LEN];

    MEMSET (au1Wname, 0, WLAN_NAME_MAX_LEN);
    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_SET_EDCA_CW_MIN,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    WssWlanGetWlanIntfName (pRadioIfGetDB->RadioIfGetAllDB.u1RadioId,
                            pRadioIfGetDB->RadioIfGetAllDB.u1WlanId,
                            FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigSetDot11EDCATableCWmin.RadInfo.
                            au1WlanIfname[0],
                            &FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigSetDot11EDCATableCWmin.RadInfo.u1WlanId);

    for (u1Index = 0; u1Index < RADIO_QOS_PROFILE; u1Index++)
    {
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11EDCATableCWmin.RadInfo.
            u4RadioIfIndex = pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
        MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.
                sConfigSetDot11EDCATableCWmin.RadInfo.au1RadioIfName,
                pu1RadioName, STRLEN (pu1RadioName));
        FsHwNp.RadioIfNpModInfo.unOpCode.
            sConfigSetDot11EDCATableCWmin.i4Dot11EDCATableIndex = u1Index;
        FsHwNp.RadioIfNpModInfo.unOpCode.
            sConfigSetDot11EDCATableCWmin.i4ValDot11EDCATableCWmin =
            pRadioIfGetDB->RadioIfGetAllDB.au2CwMin[u1Index];

        /*To Set in the Hw */
        if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "NpUtilHwProgram : Values to set CwMin in Hw failed\r\n");
            return OSIX_FAILURE;
        }
    }
#endif
    UNUSED_PARAM (pu1RadioName);
    pRadioIfGetDB->RadioIfIsGetAllDB.bCwMin = OSIX_TRUE;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfSetQosCwMax                                   *
 *                                                                           *
 * Description        : Configure the QoS Params for the interface           *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to the Radio DB              *
 *                    : pu1RadioName  - Radio Interface name                 *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfSetQosCwMax (tRadioIfGetDB * pRadioIfGetDB, UINT1 *pu1RadioName)
{
#ifdef NPAPI_WANTED
    tFsHwNp             FsHwNp;
    UINT1               u1Index = 0;
    UINT1               au1Wname[WLAN_NAME_MAX_LEN];

    MEMSET (au1Wname, 0, WLAN_NAME_MAX_LEN);
    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_SET_EDCA_CW_MAX,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    WssWlanGetWlanIntfName (pRadioIfGetDB->RadioIfGetAllDB.u1RadioId,
                            pRadioIfGetDB->RadioIfGetAllDB.u1WlanId,
                            FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigSetDot11EDCATableCWmax.RadInfo.
                            au1WlanIfname[0],
                            &FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigSetDot11EDCATableCWmax.RadInfo.u1WlanId);

    for (u1Index = 0; u1Index < RADIO_QOS_PROFILE; u1Index++)
    {
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11EDCATableCWmax.RadInfo.
            u4RadioIfIndex = pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
        MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.
                sConfigSetDot11EDCATableCWmax.RadInfo.au1RadioIfName,
                pu1RadioName, STRLEN (pu1RadioName));
        FsHwNp.RadioIfNpModInfo.unOpCode.
            sConfigSetDot11EDCATableCWmax.i4Dot11EDCATableIndex = u1Index;
        FsHwNp.RadioIfNpModInfo.unOpCode.
            sConfigSetDot11EDCATableCWmax.i4ValDot11EDCATableCWmax =
            pRadioIfGetDB->RadioIfGetAllDB.au2CwMax[u1Index];

        /*To Set in the Hw */
        if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "NpUtilHwProgram : Values to set CwMax in Hw failed\r\n");
            return OSIX_FAILURE;
        }
    }
#endif
    UNUSED_PARAM (pu1RadioName);
    pRadioIfGetDB->RadioIfIsGetAllDB.bCwMax = OSIX_TRUE;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfSetQosAifsn                                   *
 *                                                                           *
 * Description        : Configure the QoS Params for the interface           *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to the Radio DB              *
 *                    : pu1RadioName  - Radio Interface name                 *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfSetQosAifsn (tRadioIfGetDB * pRadioIfGetDB, UINT1 *pu1RadioName)
{
#ifdef NPAPI_WANTED
    tFsHwNp             FsHwNp;
    UINT1               u1Index = 0;
    UINT1               au1Wname[WLAN_NAME_MAX_LEN];

    MEMSET (au1Wname, 0, WLAN_NAME_MAX_LEN);
    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_SET_EDCA_AIFS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    WssWlanGetWlanIntfName (pRadioIfGetDB->RadioIfGetAllDB.u1RadioId,
                            pRadioIfGetDB->RadioIfGetAllDB.u1WlanId,
                            FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigSetDot11EDCATableAifs.RadInfo.
                            au1WlanIfname[0],
                            &FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigSetDot11EDCATableAifs.RadInfo.u1WlanId);

    for (u1Index = 0; u1Index < RADIO_QOS_PROFILE; u1Index++)
    {
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11EDCATableAifs.RadInfo.
            u4RadioIfIndex = pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
        MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.
                sConfigSetDot11EDCATableAifs.RadInfo.au1RadioIfName,
                pu1RadioName, STRLEN (pu1RadioName));
        FsHwNp.RadioIfNpModInfo.unOpCode.
            sConfigSetDot11EDCATableAifs.i4Dot11EDCATableIndex = u1Index;
        FsHwNp.RadioIfNpModInfo.unOpCode.
            sConfigSetDot11EDCATableAifs.i4ValDot11EDCATableAifs =
            pRadioIfGetDB->RadioIfGetAllDB.au1Aifsn[u1Index];

        /*To Set in the Hw */
        if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "NpUtilHwProgram : Values to set Aifsn in Hw failed\r\n");
            return OSIX_FAILURE;
        }
    }
#endif
    UNUSED_PARAM (pu1RadioName);
    pRadioIfGetDB->RadioIfIsGetAllDB.bAifsn = OSIX_TRUE;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfSetQosTxop                                    *
 *                                                                           *
 * Description        : Configure the QoS Params for the interface           *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to the Radio DB              *
 *                    : pu1RadioName  - Radio Interface name                 *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfSetQosTxop (tRadioIfGetDB * pRadioIfGetDB, UINT1 *pu1RadioName)
{
#ifdef NPAPI_WANTED
    tFsHwNp             FsHwNp;
    UINT1               u1Index = 0;
    UINT1               au1Wname[WLAN_NAME_MAX_LEN];

    MEMSET (au1Wname, 0, WLAN_NAME_MAX_LEN);
    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_SET_EDCA_TXOP_LIMIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    WssWlanGetWlanIntfName (pRadioIfGetDB->RadioIfGetAllDB.u1RadioId,
                            pRadioIfGetDB->RadioIfGetAllDB.u1WlanId,
                            FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigSetDot11EDCATableTxOpLmt.RadInfo.
                            au1WlanIfname[0],
                            &FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigSetDot11EDCATableTxOpLmt.RadInfo.u1WlanId);

    for (u1Index = 0; u1Index < RADIO_QOS_PROFILE; u1Index++)
    {

        FsHwNp.RadioIfNpModInfo.unOpCode.
            sConfigSetDot11EDCATableTxOpLmt.RadInfo.u4RadioIfIndex =
            pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
        MEMCPY (FsHwNp.RadioIfNpModInfo.
                unOpCode.sConfigSetDot11EDCATableTxOpLmt.RadInfo.au1RadioIfName,
                pu1RadioName, STRLEN (pu1RadioName));
        FsHwNp.RadioIfNpModInfo.unOpCode.
            sConfigSetDot11EDCATableTxOpLmt.i4Dot11EDCATableIndex = u1Index;
        FsHwNp.RadioIfNpModInfo.unOpCode.
            sConfigSetDot11EDCATableTxOpLmt.i4ValDot11EDCATableTxOpLmt =
            pRadioIfGetDB->RadioIfGetAllDB.au2TxOpLimit[u1Index];
        /*To Set in the Hw */
        if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "NpUtilHwProgram : Values to set Txop in Hw failed\r\n");
            return OSIX_FAILURE;
        }
    }
#endif
    UNUSED_PARAM (pu1RadioName);
    pRadioIfGetDB->RadioIfIsGetAllDB.bTxOpLimit = OSIX_TRUE;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfSetQosAdmsnControl                            *
 *                                                                           *
 * Description        : Configure the QoS Params for the interface           *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to the Radio DB              *
 *                    : pu1RadioName  - Radio Interface name                 *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfSetQosAdmsnControl (tRadioIfGetDB * pRadioIfGetDB, UINT1 *pu1RadioName)
{
#ifdef NPAPI_WANTED
    tFsHwNp             FsHwNp;
    UINT1               u1Index = 0;
    UINT1               au1Wname[WLAN_NAME_MAX_LEN];

    MEMSET (au1Wname, 0, WLAN_NAME_MAX_LEN);
    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_SET_EDCA_ADMN_CTRL,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    WssWlanGetWlanIntfName (pRadioIfGetDB->RadioIfGetAllDB.u1RadioId,
                            pRadioIfGetDB->RadioIfGetAllDB.u1WlanId,
                            FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigSetDot11EDCATableAdmsnCtrl.RadInfo.
                            au1WlanIfname[0],
                            &FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigSetDot11EDCATableAdmsnCtrl.RadInfo.u1WlanId);

    for (u1Index = 0; u1Index < RADIO_QOS_PROFILE; u1Index++)
    {
        FsHwNp.RadioIfNpModInfo.unOpCode.
            sConfigSetDot11EDCATableAdmsnCtrl.RadInfo.u4RadioIfIndex =
            pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
        MEMCPY (FsHwNp.RadioIfNpModInfo.
                unOpCode.sConfigSetDot11EDCATableAdmsnCtrl.RadInfo.
                au1RadioIfName, pu1RadioName, STRLEN (pu1RadioName));
        FsHwNp.RadioIfNpModInfo.unOpCode.
            sConfigSetDot11EDCATableAdmsnCtrl.i4Dot11EDCATableIndex = u1Index;
        FsHwNp.RadioIfNpModInfo.unOpCode.
            sConfigSetDot11EDCATableAdmsnCtrl.i4ValDot11EDCATableAdmsnCtrl =
            pRadioIfGetDB->RadioIfGetAllDB.au1AdmissionControl[u1Index];

        /*To Set in the Hw */
        if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "NpUtilHwProgram : Values to set Admsn control in Hw failed\r\n");
            return OSIX_FAILURE;
        }
    }
#endif
    UNUSED_PARAM (pu1RadioName);
    pRadioIfGetDB->RadioIfIsGetAllDB.bAdmissionControl = OSIX_TRUE;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfSetAdminStatus                                *
 *                                                                           *
 * Description        : Configure the Admin Status for the interface         *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to the Radio DB              *
 *                    : pu1RadioName  - Radio Interface name                 *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfSetAdminStatus (tRadioIfGetDB * pRadioIfGetDB, UINT1 *pu1RadioName)
{
#ifdef NPAPI_WANTED
    tFsHwNp             FsHwNp;

    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));

    if (pRadioIfGetDB->RadioIfGetAllDB.u1AdminStatus ==
        RADIOIF_ADMIN_STATUS_ENABLED)
    {
        NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                             NP_RADIO_MODULE,    /* Module ID */
                             FS_RADIO_HW_UPDATE_ADMIN_STATUS_ENABLE,    /* Function/OpCode */
                             0,    /* IfIndex value if applicable */
                             0,    /* No. of Port Params */
                             0);    /* No. of PortList Parms */

        FsHwNp.RadioIfNpModInfo.unOpCode.sFsHwUpdateAdminStatusEnable.RadInfo.
            u4RadioIfIndex = pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
        MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.
                sFsHwUpdateAdminStatusEnable.RadInfo.au1RadioIfName,
                pu1RadioName, STRLEN (pu1RadioName));

        /*To Set in the Hw */
        if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "NpUtilHwProgram : Values to set in Hw failed\r\n");
            return OSIX_FAILURE;
        }
    }
    else
    {
        NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                             NP_RADIO_MODULE,    /* Module ID */
                             FS_RADIO_HW_UPDATE_ADMIN_STATUS_DISABLE,    /* Function/OpCode */
                             0,    /* IfIndex value if applicable */
                             0,    /* No. of Port Params */
                             0);    /* No. of PortList Parms */

        FsHwNp.RadioIfNpModInfo.unOpCode.sFsHwUpdateAdminStatusDisable.RadInfo.
            u4RadioIfIndex = pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
        MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.
                sFsHwUpdateAdminStatusDisable.RadInfo.au1RadioIfName,
                pu1RadioName, STRLEN (pu1RadioName));

        /*To Set in the Hw */
        if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "NpUtilHwProgram : Values to set in Hw failed\r\n");
            return OSIX_FAILURE;
        }
    }
#endif
    UNUSED_PARAM (pu1RadioName);
    pRadioIfGetDB->RadioIfIsGetAllDB.bAdminStatus = OSIX_TRUE;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfSetMaxTxPowerLevel2G                          *
 *                                                                           *
 * Description        : Configure the max power level for DSSS               *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to the Radio DB              *
 *                    : pu1RadioName  - Radio Interface name                 *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfSetMaxTxPowerLevel2G (tRadioIfGetDB * pRadioIfGetDB, UINT1 *pu1RadioName)
{
#ifdef NPAPI_WANTED
    tFsHwNp             FsHwNp;

    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_SET_MAX_TX_POW_2G,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11MaxTxPow2G.
        RadInfo.u4RadioIfIndex = pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11MaxTxPow2G.RadInfo.
            au1RadioIfName, pu1RadioName, STRLEN (pu1RadioName));

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11MaxTxPow2G.u4MaxTxPow =
        pRadioIfGetDB->RadioIfGetAllDB.u2MaxTxPowerLevel;

    /*To set in the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "NpUtilHwProgram : Values to set in Hw failed\r\n");
        return OSIX_FAILURE;
    }
#endif
    UNUSED_PARAM (pu1RadioName);
    pRadioIfGetDB->RadioIfIsGetAllDB.bMaxTxPowerLevel = OSIX_TRUE;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfSetMaxTxPowerLevel5G                          *
 *                                                                           *
 * Description        : Configure the max power level for OFDM               *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to the Radio DB              *
 *                    : pu1RadioName  - Radio Interface name                 *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfSetMaxTxPowerLevel5G (tRadioIfGetDB * pRadioIfGetDB, UINT1 *pu1RadioName)
{
#ifdef NPAPI_WANTED
    tFsHwNp             FsHwNp;

    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_SET_MAX_TX_POW_5G,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11MaxTxPow5G.
        RadInfo.u4RadioIfIndex = pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11MaxTxPow5G.RadInfo.
            au1RadioIfName, pu1RadioName, STRLEN (pu1RadioName));

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11MaxTxPow5G.u4MaxTxPow
        = pRadioIfGetDB->RadioIfGetAllDB.u2MaxTxPowerLevel;

    /*To set in the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "NpUtilHwProgram : Values to set in Hw failed\r\n");
        return OSIX_FAILURE;
    }
#endif
    UNUSED_PARAM (pu1RadioName);
    pRadioIfGetDB->RadioIfIsGetAllDB.bMaxTxPowerLevel = OSIX_TRUE;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfSetBeaconPeriod                               *
 *                                                                           *
 * Description        : Configure the Beacon Period in the hardware          *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to the Radio DB              *
 *                    : pu1RadioName  - Radio Interface name                 *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfSetBeaconPeriod (tRadioIfGetDB * pRadioIfGetDB, UINT1 *pu1RadioName)
{
#ifdef NPAPI_WANTED
    tFsHwNp             FsHwNp;
    UINT1               au1Wname[WLAN_NAME_MAX_LEN];

    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));
    MEMSET (au1Wname, 0, WLAN_NAME_MAX_LEN);

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_SET_BEACON_PERIOD,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    FsHwNp.RadioIfNpModInfo.unOpCode.sFsHwSetBeaconInterval.RadInfo.
        u4RadioIfIndex = pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sFsHwSetBeaconInterval.RadInfo.
            au1RadioIfName, pu1RadioName, STRLEN (pu1RadioName));

    FsHwNp.RadioIfNpModInfo.unOpCode.sFsHwSetBeaconInterval.u4BeaconInterval =
        pRadioIfGetDB->RadioIfGetAllDB.u2BeaconPeriod;

    WssWlanGetWlanIntfName (pRadioIfGetDB->RadioIfGetAllDB.u1RadioId,
                            pRadioIfGetDB->RadioIfGetAllDB.u1WlanId,
                            FsHwNp.RadioIfNpModInfo.unOpCode.
                            sFsHwSetBeaconInterval.RadInfo.au1WlanIfname[0],
                            &FsHwNp.RadioIfNpModInfo.unOpCode.
                            sFsHwSetBeaconInterval.RadInfo.u1WlanId);

    /*To set in the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "NpUtilHwProgram : Values to set in Hw failed\r\n");
        return OSIX_FAILURE;
    }

#endif
    UNUSED_PARAM (pu1RadioName);
    pRadioIfGetDB->RadioIfIsGetAllDB.bBeaconPeriod = OSIX_TRUE;
    return OSIX_SUCCESS;
}

#ifdef HOSTAPD_WANTED
/*****************************************************************************
 * Function Name      : HosApdIfReset                                        *
 *                                                                           *
 * Description        : Configure Resetting the HostApd in the hardware      *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to the Radio DB              *
 *                    : pu1RadioName  - Radio Interface name                 *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfHostApdReset (void)
{
#ifdef NPAPI_WANTED
    tFsHwNp             FsHwNp;

    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_HOSTAPD_RESET,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    /*To set in the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "NpUtilHwProgram : Failed to Reset the HostApd\r\n");
        return OSIX_FAILURE;
    }
#endif
    return OSIX_SUCCESS;
}
#endif
/*****************************************************************************
 * Function Name      : RadioIfSetDTIMPeriod                                 *
 *                                                                           *
 * Description        : Configure the DTIM Period in the hardware            *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to the Radio DB              *
 *                    : pu1RadioName  - Radio Interface name                 *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfSetDTIMPeriod (tRadioIfGetDB * pRadioIfGetDB, UINT1 *pu1RadioName)
{
#ifdef NPAPI_WANTED
    tFsHwNp             FsHwNp;
    UINT1               au1Wname[WLAN_NAME_MAX_LEN];

    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));
    MEMSET (au1Wname, 0, WLAN_NAME_MAX_LEN);

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_SET_DTIM_PERIOD,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms
                                 */

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11DtimPrd.RadInfo.
        u4RadioIfIndex = pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11DtimPrd.RadInfo.
            au1RadioIfName, pu1RadioName, STRLEN (pu1RadioName));

    WssWlanGetWlanIntfName (pRadioIfGetDB->RadioIfGetAllDB.u1RadioId,
                            pRadioIfGetDB->RadioIfGetAllDB.u1WlanId,
                            FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigSetDot11DtimPrd.RadInfo.au1WlanIfname[0],
                            &FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigSetDot11DtimPrd.RadInfo.u1WlanId);

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11DtimPrd.i4ValDot11DtimPrd =
        pRadioIfGetDB->RadioIfGetAllDB.u1DTIMPeriod;

    /*To set in the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "NpUtilHwProgram : Values to set DTIM in Hw failed\r\n");
        return OSIX_FAILURE;
    }
#endif
    UNUSED_PARAM (pu1RadioName);
    pRadioIfGetDB->RadioIfIsGetAllDB.bDTIMPeriod = OSIX_TRUE;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfSetDot11AC                                    *
 *                                                                           *
 * Description        : Configure the 802.11ac details in the hardware       *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to the Radio DB              *
 *                    : pu1RadioName  - Radio Interface name                 *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfSetDot11AC (tRadioIfGetDB * pRadioIfGetDB, UINT1 *pu1RadioName)
{
#ifdef NPAPI_WANTED
    tFsHwNp             FsHwNp;
    UINT1               au1Wname[WLAN_NAME_MAX_LEN];
    UINT1               u1ChanWidth = 0;

    MEMSET (au1Wname, 0, WLAN_NAME_MAX_LEN);
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_SET_DOT11AC,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11ac.RadInfo.u4RadioIfIndex =
        pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11ac.
            RadInfo.au1RadioIfName, pu1RadioName, STRLEN (pu1RadioName));

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11ac.
        RadInfo.au2WlanIfIndex[0] = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;

    WssWlanGetWlanIntfName (pRadioIfGetDB->RadioIfGetAllDB.u1RadioId,
                            pRadioIfGetDB->RadioIfGetAllDB.u1WlanId,
                            FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11ac.
                            RadInfo.au1WlanIfname[0],
                            &FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11ac.
                            RadInfo.u1WlanId);

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11ac.VHTCapabilities.
        u4VhtCapaInfo =
        pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u4VhtCapInfo;

    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11ac.VHTCapabilities.
            au1SuppMCS,
            pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.au1VhtCapaMcs,
            sizeof (pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.
                    au1VhtCapaMcs));
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11ac.VHTOperation.
            au1VhtOperInfo,
            pRadioIfGetDB->RadioIfGetAllDB.Dot11AcOperParams.au1VhtOperInfo,
            sizeof (pRadioIfGetDB->RadioIfGetAllDB.Dot11AcOperParams.
                    au1VhtOperInfo));

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11ac.VHTOperation.u2BasicMCS
        = pRadioIfGetDB->RadioIfGetAllDB.Dot11AcOperParams.u2VhtOperMcsSet;
    u1ChanWidth = WssIfComputeBandWidth (pRadioIfGetDB);
    if ((pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.
         u1SpecMgmt == OSIX_TRUE) && (pRadioIfGetDB->
                                      RadioIfGetAllDB.Dot11AcOperParams.
                                      u1VhtOption == OSIX_TRUE))
    {
/*Vht Transmit Power Envelope element*/
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11ac.
            VhtTxPower.isOptional = OSIX_TRUE;
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11ac.
            VhtTxPower.u1TransPower = pRadioIfGetDB->
            RadioIfGetAllDB.VhtTransmitPower.u1TransPower;
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11ac.
            VhtTxPower.u1TxPower20 = pRadioIfGetDB->
            RadioIfGetAllDB.VhtTransmitPower.u1TxPower20;
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11ac.
            VhtTxPower.u1ElemId = WSSMAC_VHT_TRANSMIT_POWER_ID;
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11ac.
            VhtTxPower.u1ElemLen = WSSMAC_VHT_POWER_ELEM_LEN;
        switch (u1ChanWidth)
        {
            case RADIO_VALUE_1:
                FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11ac.
                    VhtTxPower.u1TxPower40 = pRadioIfGetDB->
                    RadioIfGetAllDB.VhtTransmitPower.u1TxPower40;
                FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11ac.
                    VhtTxPower.u1ElemLen += RADIO_VALUE_1;
                break;
            case RADIO_VALUE_2:
                FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11ac.
                    VhtTxPower.u1TxPower80 = pRadioIfGetDB->
                    RadioIfGetAllDB.VhtTransmitPower.u1TxPower80;
                FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11ac.
                    VhtTxPower.u1ElemLen += RADIO_VALUE_2;
                break;
            case RADIO_VALUE_3:
                FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11ac.
                    VhtTxPower.u1TxPower160 = pRadioIfGetDB->
                    RadioIfGetAllDB.VhtTransmitPower.u1TxPower160;
                FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11ac.
                    VhtTxPower.u1ElemLen += RADIO_VALUE_3;
                break;
            default:
                break;
        }
    }
    if (pRadioIfGetDB->RadioIfGetAllDB.Dot11AcOperParams.u1Action == OSIX_TRUE)
    {
/*Add Channel Switch Wrapper Element in beacon*/
        RadioIfAddChanlWrapIE (pRadioIfGetDB, &FsHwNp);
    }
    else
    {
/*Remove Channel Switch Wrapper Element in beacon*/
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11ac.
            ChanSwitchWrapper.isOptional = OSIX_FALSE;
    }

/*Operating Mode Notification Element*/
    if (pRadioIfGetDB->RadioIfGetAllDB.Dot11AcOperParams.
        u1VhtOperModeNotify == OSIX_TRUE)
    {
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11ac.
            OperModeNotify.isOptional = OSIX_TRUE;
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11ac.
            OperModeNotify.u1VhtOperModeElem = pRadioIfGetDB->
            RadioIfGetAllDB.Dot11AcOperParams.u1VhtOperModeElem;
    }

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11ac.u1Dot11acSupport =
        DOT11AC_SUPPORT_ENABLE;
    /*To set in the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "NpUtilHwProgram : Values to set in Hw failed\r\n");
        return OSIX_FAILURE;
    }

#endif
    UNUSED_PARAM (pu1RadioName);
    UNUSED_PARAM (pRadioIfGetDB);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfSetDot11N                                     *
 *                                                                           *
 * Description        : Configure the 802.11n details in the hardware        *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to the Radio DB              *
 *                    : pu1RadioName  - Radio Interface name                 *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfSetDot11N (tRadioIfGetDB * pRadioIfGetDB, UINT1 *pu1RadioName)
{
#ifdef NPAPI_WANTED
    tFsHwNp             FsHwNp;
    UINT1               au1Wname[WLAN_NAME_MAX_LEN];

    MEMSET (au1Wname, 0, WLAN_NAME_MAX_LEN);
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_SET_DOT11N,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11n.RadInfo.u4RadioIfIndex =
        pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11n.
            RadInfo.au1RadioIfName, pu1RadioName, STRLEN (pu1RadioName));

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11n.
        RadInfo.au2WlanIfIndex[0] = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;

    WssWlanGetWlanIntfName (pRadioIfGetDB->RadioIfGetAllDB.u1RadioId,
                            pRadioIfGetDB->RadioIfGetAllDB.u1WlanId,
                            FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11n.
                            RadInfo.au1WlanIfname[0],
                            &FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11n.
                            RadInfo.u1WlanId);
    if (pRadioIfGetDB->RadioIfGetAllDB.u1HTCapEnable ==
        DOT11_INFO_ELEM_HTCAPABILITY)
    {
        /*Configuring HT Capabilities */
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11n.u1HTCapEnable =
            pRadioIfGetDB->RadioIfGetAllDB.u1HTCapEnable;
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11n.HTCapabilities.
            u2HTCapInfo =
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u2HtCapInfo;
        MEMSET (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11n.
                HTCapabilities.au1SuppMCSSet, VALUE_0,
                sizeof (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11n.
                        HTCapabilities.au1SuppMCSSet));
        MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11n.
                HTCapabilities.au1SuppMCSSet,
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.au1HtCapaMcs,
                DOT11N_HT_CAP_MCS_LEN);
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11n.HTCapabilities.
            u1AMPDUParam =
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NampduParams.u1AmpduParam;
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11n.HTCapabilities.
            u2HTExtCap =
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.u2HtExtCap;
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11n.HTCapabilities.
            u4TranBeamformCap =
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
            u4TxBeamCapParam;
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11n.HTCapabilities.
            u1ASELCap = VALUE_0;
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11n.u1Dot11nSupport =
            DOT11N_SUPPORT_ENABLE;
    }

    if ((pRadioIfGetDB->RadioIfGetAllDB.u1HTOpeEnable
         == DOT11_INFO_ELEM_HTOPERATION) ||
        (pRadioIfGetDB->RadioIfGetAllDB.u1HTOpeEnable ==
         DOT11_HT_SCAN_BEACON_UPDATE))
    {
        /*Configuring the HT operation element */
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11n.u1HTOpeEnable =
            pRadioIfGetDB->RadioIfGetAllDB.u1HTOpeEnable;
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11n.HTOperation.
            u1PrimaryCh =
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.u1PrimaryChannel;
        MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11n.HTOperation.
                au1HTOpeInfo,
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.au1HTOpeInfo,
                WSSMAC_HTOPE_INFO);
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11n.HTOperation.
            au1HTOpeInfo[VALUE_1] |=
            (pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.
             u1IsGreenFieldPresent << BIT_2);

        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11n.
            HTOperation.au1HTOpeInfo[VALUE_1] |=
            (pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.
             u1ObssNonHtStasPresent << BIT_4);

        MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11n.HTOperation.
                au1BasicMCSSet,
                pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.au1BasicMCSSet,
                WSSMAC_BASIC_MCS_SET);
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11n.u1Dot11nSupport =
            DOT11N_SUPPORT_ENABLE;
    }

    /*To set in the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "NpUtilHwProgram : Values to set in Hw failed\r\n");
        return OSIX_FAILURE;
    }

#endif
    UNUSED_PARAM (pu1RadioName);
    pRadioIfGetDB->RadioIfIsGetAllDB.bHTCapInfo = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bManMCSSet = OSIX_TRUE;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfSetCountryString                              *
 *                                                                           *
 * Description        : Configure the country in the hardware                *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to the Radio DB              *
 *                    : pu1RadioName  - Radio Interface name                 *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfSetCountryString (tRadioIfGetDB * pRadioIfGetDB, UINT1 *pu1RadioName)
{
#ifdef NPAPI_WANTED
    tFsHwNp             FsHwNp;

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_SET_COUNTRY,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11Country.
        RadInfo.u4RadioIfIndex = pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11Country.
            RadInfo.au1RadioIfName, pu1RadioName, STRLEN (pu1RadioName));

    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.
            sConfigSetDot11Country.au1ValDot11CountryName,
            pRadioIfGetDB->RadioIfGetAllDB.au1CountryString,
            STRLEN (pRadioIfGetDB->RadioIfGetAllDB.au1CountryString));

    if ((pRadioIfGetDB->RadioIfGetAllDB.u1WlanId - 1) != 0)
    {
        return OSIX_SUCCESS;

    }

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11Country.RadInfo.
        u1WlanId = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;

    /*To set in the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "NpUtilHwProgram : Values to set in Hw failed\r\n");
        return OSIX_FAILURE;
    }

#endif
    UNUSED_PARAM (pu1RadioName);
    pRadioIfGetDB->RadioIfIsGetAllDB.bCountryString = OSIX_TRUE;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfSetLocalRoutingStatus                         *
 *                                                                           *
 * Description        : Configuration to be done in the hw for local routing *
 *                                                                           *
 * Input(s)           : u1WtpLocalRouting                                    *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfSetLocalRoutingStatus (UINT1 u1WtpLocalRouting)
{
#ifdef NPAPI_WANTED
    tFsHwNp             FsHwNp;

    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_SET_LOCAL_ROUTING,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetLocalRouting.u1WtpLocalRouting =
        u1WtpLocalRouting;
    /*To set in the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "NpUtilHwProgram : Failed to Reset the HostApd\r\n");
        return OSIX_FAILURE;
    }
#endif
    UNUSED_PARAM (u1WtpLocalRouting);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfUpdateLocalRoutingStatus                      *
 *                                                                           *
 * Description        : Configuration to be done in the hw for local routing *
 *                                                                           *
 * Input(s)           : u1WtpLocalRouting                                    *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfUpdateLocalRoutingStatus (UINT1 u1WtpLocalRouting)
{
#ifdef NPAPI_WANTED
    tFsHwNp             FsHwNp;

    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_UPDATE_LOCAL_ROUTING,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetLocalRouting.u1WtpLocalRouting =
        u1WtpLocalRouting;
    /*To set in the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "NpUtilHwProgram : Failed to Reset the HostApd\r\n");
        return OSIX_FAILURE;
    }
#endif
    UNUSED_PARAM (u1WtpLocalRouting);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfSetDot11nConfig                               *
 *                                                                           *
 * Description        : Configure the Dot11n hardware settings               *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to the Radio DB              *
 *                    : pu1RadioName  - Radio Interface name                 *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfSetDot11nConfig (tRadioIfGetDB * pRadioIfGetDB, UINT1 *pu1RadioName)
{
#ifdef NPAPI_WANTED
    tFsHwNp             FsHwNp;
    UINT1               au1Wname[WLAN_NAME_MAX_LEN];

    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));
    MEMSET (au1Wname, 0, WLAN_NAME_MAX_LEN);

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_SET_DOT11N_PARAMS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    FsHwNp.RadioIfNpModInfo.unOpCode.sFsHwSetDot11nParams.RadInfo.
        u4RadioIfIndex = pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sFsHwSetDot11nParams.RadInfo.
            au1RadioIfName, pu1RadioName, STRLEN (pu1RadioName));

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11NampduParams.
        RadInfo.au2WlanIfIndex[0] = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;
    SPRINTF ((CHR1 *) au1Wname, "%s%d", RADIO_ATH_INTF_NAME,
             pRadioIfGetDB->RadioIfGetAllDB.u1WlanId - 1);
    STRCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11NampduParams.RadInfo.
            au1WlanIfname[0], au1Wname);

    FsHwNp.RadioIfNpModInfo.unOpCode.sFsHwSetDot11nParams.u1AMPDUStatus =
        pRadioIfGetDB->RadioIfGetAllDB.u1AMPDUStatus;
    FsHwNp.RadioIfNpModInfo.unOpCode.sFsHwSetDot11nParams.u1AMPDUSubFrame =
        pRadioIfGetDB->RadioIfGetAllDB.u1AMPDUSubFrame;
    FsHwNp.RadioIfNpModInfo.unOpCode.sFsHwSetDot11nParams.u2AMPDULimit =
        pRadioIfGetDB->RadioIfGetAllDB.u2AMPDULimit;
    FsHwNp.RadioIfNpModInfo.unOpCode.sFsHwSetDot11nParams.u1AMSDUStatus =
        pRadioIfGetDB->RadioIfGetAllDB.u1AMSDUStatus;
    FsHwNp.RadioIfNpModInfo.unOpCode.sFsHwSetDot11nParams.u2AMSDULimit =
        pRadioIfGetDB->RadioIfGetAllDB.u2AMSDULimit;

    /*To set in the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "NpUtilHwProgram : Values to set in Hw failed\r\n");
        return OSIX_FAILURE;
    }

#endif
    UNUSED_PARAM (pu1RadioName);
    UNUSED_PARAM (pRadioIfGetDB);
    return OSIX_SUCCESS;
}

#ifndef NPAPI_WANTED
/*****************************************************************************
 * Function Name      : RadioIfGetMultiDomainAndAssembleBeacon               *
 *                                                                           *
 * Description        : Wrapper Func through which the configured country's  *
 *                      Multi Domain Capability will be received from NPAPI  *
 *                      and assemble Beacon Frames                           *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to input structure           *
 *                      pu1RadioName - Radio Interface Name                  *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfGetMultiDomainAndAssembleBeacon (tRadioIfGetDB * pRadioIfGetDB,
                                        UINT1 *pu1RadioName)
{
    UNUSED_PARAM (pRadioIfGetDB);
    UNUSED_PARAM (pu1RadioName);
    return OSIX_SUCCESS;

}
#endif

#ifdef BAND_SELECT_WANTED
/*****************************************************************************
 * Function Name      : RadioIfDelStaBandSteerDB                             *
 *                                                                           *
 * Description        : To Delete the station entry in the shadowDB          *
 *                                                                           *
 * Input(s)           : StaMacAddr                                           *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS                                         *
 *                      OSIX_FAILURE                                         *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfDelStaBandSteerDB (UINT1 *pu1StaMacAddr, UINT1 *pu1RadioName,
                          UINT1 *pu1WlanId)
{
#ifdef NPAPI_WANTED
    tFsHwNp             FsHwNp;

    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_DEL_STA_BANDSTEER,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDelStaBandSteer.
            RadInfo.au1RadioIfName, pu1RadioName, STRLEN (pu1RadioName));

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDelStaBandSteer.
        RadInfo.u1WlanId = *pu1WlanId;

    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDelStaBandSteer.
            stationMacAddress, pu1StaMacAddr, sizeof (tMacAddr));
    RADIOIF_TRC6 (RADIOIF_MGMT_TRC,
                  "Going to Remove %02x:%02x:%02x:%02x:%02x:%02x from RadioIfParams\n",
                  FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDelStaBandSteer.
                  stationMacAddress[0],
                  FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDelStaBandSteer.
                  stationMacAddress[1],
                  FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDelStaBandSteer.
                  stationMacAddress[2],
                  FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDelStaBandSteer.
                  stationMacAddress[3],
                  FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDelStaBandSteer.
                  stationMacAddress[4],
                  FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDelStaBandSteer.
                  stationMacAddress[5]);
    /*To set in the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "NpUtilHwProgram : Failed to Reset the HostApd\r\n");
        return OSIX_FAILURE;
    }
#endif
    UNUSED_PARAM (pu1StaMacAddr);
    UNUSED_PARAM (pu1RadioName);
    UNUSED_PARAM (pu1WlanId);
    return OSIX_SUCCESS;
}
#endif
#endif
