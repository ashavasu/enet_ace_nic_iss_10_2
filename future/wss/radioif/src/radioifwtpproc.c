/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: radioifwtpproc.c,v 1.7 2017/05/23 14:16:52 siva Exp $
 *
 * Description: This file handles all the configurations to be done in WTP
 ***********************************************************************/
#ifndef __RADIOIFAP_C__
#define __RADIOIFAP_C__

#include "radioifinc.h"
#include "aphdlr.h"
#include "msr.h"
#include "wssifproto.h"
#include "wssifstawtpprot.h"
#include "radioifhwnpwr.h"

/*****************************************************************************
 * Function Name      : RadioIfConfigStatusValidateRsp                       *
 *                                                                           *
 * Description        : Config Status validate Response from AC to WTP       *
 *                                                                           *
 * Input(s)           : pWssMsgStruct - Pointer to the input structure       *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfConfigStatusValidateRsp (tRadioIfMsgStruct * pWssMsgStruct)
{
    UINT1               u1AntennaCount = 0;
    UINT1               u1Index = 0;

    if (pWssMsgStruct == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "ConfigStatusValidateRsp : Message pointer is NULL\r\n");
        return OSIX_FAILURE;
    }

    tRadioIfAntenna    *pRadioIfAntenna =
        &(pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusRsp.RadioIfAntenna);
    tRadioIfDSSSPhy    *pRadioIfDSSSPhy =
        &(pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusRsp.RadioIfDSSPhy);
    tRadioIfMacOperation *pRadioIfMacOperation =
        &(pWssMsgStruct->unRadioIfMsg.
          RadioIfConfigStatusRsp.RadioIfMacOperation);
    tRadioIfMultiDomainCap *pRadioIfMultiDomCap =
        &(pWssMsgStruct->unRadioIfMsg.
          RadioIfConfigStatusRsp.RadioIfMultiDomainCap);
    tRadioIfOFDMPhy    *pRadioIfOFDMPhy =
        &(pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusRsp.RadioIfOFDMPhy);
    tRadioIfRateSet    *pRadioIfRateSet =
        &(pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusRsp.RadioIfRateSet);
    tRadioIfSupportedRate *pRadioIfSuppRate =
        &(pWssMsgStruct->unRadioIfMsg.
          RadioIfConfigStatusRsp.RadioIfSupportedRate);
    tRadioIfTxPower    *pRadioIfTxPower =
        &(pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusRsp.RadioIfTxPower);
    tRadioIfQos        *pRadioIfQos =
        &(pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusRsp.RadioIfQos);
    tRadioIfConfig     *pRadioIfConfig =
        &(pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusRsp.RadioIfConfig);

    /* Input Parameter Check */
    if (pRadioIfAntenna->isPresent != OSIX_FALSE)
    {
        if (pRadioIfAntenna->u2MessageType != RADIO_ANTENNA_MSG_TYPE)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "ConfigStatusValidateRsp : Invalid Msg type"
                         "for Radio Antenna\r\n");
            return OSIX_FAILURE;

        }
        if (pRadioIfAntenna->u2MessageLength < RADIO_ANTENNA_MSG_MIN_LEN)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "ConfigStatusValidateRsp : Invalid message length"
                         "received for Radio Antenna \r\n");
            return OSIX_FAILURE;
        }
        if ((pRadioIfAntenna->u1RadioId > MAX_NUM_OF_RADIO_PER_AP) ||
            (pRadioIfAntenna->u1RadioId == 0))
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "ConfigStatusValidateRsp:Invalid Radio Id rcvd\r\n");
            return OSIX_FAILURE;
        }

        if ((pRadioIfAntenna->u1ReceiveDiversity !=
             RADIO_ANTENNA_DIVERSITY_ENABLED)
            && (pRadioIfAntenna->u1ReceiveDiversity !=
                RADIO_ANTENNA_DIVERSITY_DISABLED))
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "ConfigStatusValidateRsp : Invalid diversity values"
                         "for antenna, \r\n");
            return OSIX_FAILURE;
        }

        if (pRadioIfAntenna->u1AntennaMode > RADIO_ANTENNA_MODE_MAX)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "ConfigStatusValidateRsp : Invalid diversity values"
                         "for antenna, Validation failed \r\n");
            return OSIX_FAILURE;

        }

        for (u1AntennaCount = 0; u1AntennaCount <
             pRadioIfAntenna->u1CurrentTxAntenna; u1AntennaCount++)
        {
            if ((pRadioIfAntenna->au1AntennaSelection[u1AntennaCount] !=
                 RADIO_ANTENNA_INTERNAL) &&
                (pRadioIfAntenna->au1AntennaSelection[u1AntennaCount]
                 != RADIO_ANTENNA_EXTERNAL))
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                             "ConfigStatusValidateRsp:Invalid antenna type\n");
                return OSIX_FAILURE;
            }
        }
    }

    if (pRadioIfDSSSPhy->isPresent != OSIX_FALSE)
    {
        if (pRadioIfDSSSPhy->u2MessageType != RADIO_DSSS_MSG_TYPE)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "ConfigStatusValidateRsp : Invalid message type"
                         "received for DSSS, Validation failed\r\n");
            return OSIX_FAILURE;

        }
        if (pRadioIfDSSSPhy->u2MessageLength != RADIO_DSSS_MSG_LEN)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "ConfigStatusValidateRsp : Invalid DSSS message"
                         "length received, Validation failed\r\n");
            return OSIX_FAILURE;

        }
        if ((pRadioIfDSSSPhy->u1RadioId > MAX_NUM_OF_RADIO_PER_AP) ||
            (pRadioIfDSSSPhy->u1RadioId == 0))
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "ConfigStatusValidateRsp:Invalid Radio Id received\r\n");
            return OSIX_FAILURE;
        }
        if (pRadioIfDSSSPhy->i1CurrentChannel > RADIO_MAX_DSSS_CHANNEL)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigStatusValidateRsp : Invalid DSSS Channel"
                         "received, Validation failed \r\n");
            return OSIX_FAILURE;
        }

    }

    if (pRadioIfMacOperation->isPresent != OSIX_FALSE)
    {
        if (pRadioIfMacOperation->u2MessageType != RADIO_MAC_OPER_MSG_TYPE)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "ConfigStatusValidateRsp : Invalid message type"
                         "received for MAC Table, Validation failed\r\n");
            return OSIX_FAILURE;

        }
        if (pRadioIfMacOperation->u2MessageLength != RADIO_MAC_OPER_MSG_LEN)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "ConfigStatusValidateRsp : Invalid message length"
                         "received for MAC Table, Validation failed\r\n");
            return OSIX_FAILURE;

        }
        if ((pRadioIfMacOperation->u1RadioId > MAX_NUM_OF_RADIO_PER_AP) ||
            (pRadioIfMacOperation->u1RadioId == 0))
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "ConfigStatusValidateRsp:Invalid Radio Id received\r\n");
            return OSIX_FAILURE;
        }
        if (pRadioIfMacOperation->u2RTSThreshold > RADIO_MAX_RTS_THRESHOLD)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "ConfigStatusValidateRsp : Invalid RTS Threshold"
                         "received, Validation failed \r\n");
            return OSIX_FAILURE;
        }
        if ((pRadioIfMacOperation->u2FragmentThreshold
             < RADIO_MIN_FRAGMENT_THRESHOLD) ||
            (pRadioIfMacOperation->u2FragmentThreshold
             > RADIO_MAX_FRAGMENT_THRESHOLD))
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "ConfigStatusValidateRsp:Invalid Fragment Threshold\r\n");
            return OSIX_FAILURE;
        }
    }
    if (pRadioIfMultiDomCap->isPresent != OSIX_FALSE)
    {
        if (pRadioIfMultiDomCap->u2MessageType != RADIO_MULTI_DOMAIN_MSG_TYPE)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigStatusValidateRsp : Invalid Multidomain Table"
                         "type received\r\n");
            return OSIX_FAILURE;

        }
        if (pRadioIfMultiDomCap->u2MessageLength != RADIO_MULTI_DOMAIN_MSG_LEN)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "ConfigStatusValidateRsp : Invalid message length"
                         "received for Multidomain Table, Validation failed\r\n");
            return OSIX_FAILURE;

        }
        if ((pRadioIfMultiDomCap->u1RadioId > MAX_NUM_OF_RADIO_PER_AP) ||
            (pRadioIfMultiDomCap->u1RadioId == 0))
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "ConfigStatusValidateRsp:Invalid Radio Id received,"
                         "Validation failed \r\n");
            return OSIX_FAILURE;
        }
    }

    if (pRadioIfOFDMPhy->isPresent != OSIX_FALSE)
    {
        if (pRadioIfOFDMPhy->u2MessageType != RADIO_OFDM_MSG_TYPE)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC, "RadioIfConfigStatusValidateRsp :"
                         "Invalid message type received for OFDM \r\n");
            return OSIX_FAILURE;

        }
        if (pRadioIfOFDMPhy->u2MessageLength != RADIO_OFDM_MSG_LEN)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC, "RadioIfConfigStatusValidateRsp:"
                         "Invalid message length received for OFDM\r\n");
            return OSIX_FAILURE;

        }
        if ((pRadioIfOFDMPhy->u1RadioId > MAX_NUM_OF_RADIO_PER_AP) ||
            (pRadioIfOFDMPhy->u1RadioId == 0))
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC, "RadioIfConfigStatusValidateRsp:"
                         "Invalid Radio Id received, Validation failed \r\n");
            return OSIX_FAILURE;
        }
        if (pRadioIfOFDMPhy->u1CurrentChannel > RADIO_MAX_OFDM_CHANNEL)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC, "RadioIfConfigStatusValidateRsp:"
                         "Invalid OFDM Channel received, Validation failed \r\n");
            return OSIX_FAILURE;
        }
    }
    if (pRadioIfRateSet->isPresent != OSIX_FALSE)
    {
        if (pRadioIfRateSet->u2MessageType != RADIO_RATE_SET_MSG_TYPE)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC, "RadioIfConfigStatusValidateRsp :"
                         "Invalid message type received for Supp Rate \r\n");
            return OSIX_FAILURE;

        }
        if (pRadioIfRateSet->u2MessageLength < RADIO_RATE_SET_MSG_LEN)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC, "RadioIfConfigStatusValidateRsp:"
                         "Invalid message length received for Supp Rate\r\n");
            return OSIX_FAILURE;
        }
        if ((pRadioIfRateSet->u1RadioId > MAX_NUM_OF_RADIO_PER_AP)
            || (pRadioIfRateSet->u1RadioId == 0))
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC, "RadioIfConfigStatusValidateRsp:"
                         "Invalid Radio Id received, Validation failed \r\n");
            return OSIX_FAILURE;
        }
    }
    if (pRadioIfSuppRate->isPresent != OSIX_FALSE)
    {
        if (pRadioIfSuppRate->u2MessageType != RADIO_SUPPORTED_RATE_MSG_TYPE)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC, "RadioIfConfigStatusValidateRsp:"
                         "Invalid message type received for Supp Rate\r\n");
            return OSIX_FAILURE;

        }
        if (pRadioIfSuppRate->u2MessageLength < RADIO_SUPPORTED_RATE_MSG_LEN)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC, "RadioIfConfigStatusValidateRsp:"
                         "Invalid message length received for Supp Rate\r\n");
            return OSIX_FAILURE;

        }
        if ((pRadioIfSuppRate->u1RadioId > MAX_NUM_OF_RADIO_PER_AP)
            || (pRadioIfSuppRate->u1RadioId == 0))
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC, "RadioIfConfigStatusValidateRsp:"
                         "Invalid Radio Id received, Validation failed \r\n");
            return OSIX_FAILURE;
        }
    }
    if (pRadioIfTxPower->isPresent != OSIX_FALSE)
    {
        if (pRadioIfTxPower->u2MessageType != RADIO_TXPOWER_MSG_TYPE)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC, "RadioIfConfigStatusValidateRsp:"
                         "Invalid message type received for Tx Power\r\n");
            return OSIX_FAILURE;

        }
        if (pRadioIfTxPower->u2MessageLength != RADIO_TXPOWER_MSG_LEN)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC, "RadioIfConfigStatusValidateRsp:"
                         "Invalid message length received for Tx Power\r\n");
            return OSIX_FAILURE;
        }

        if ((pRadioIfTxPower->u1RadioId > MAX_NUM_OF_RADIO_PER_AP)
            || (pRadioIfTxPower->u1RadioId == 0))
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC, "RadioIfConfigStatusValidateRsp:"
                         "Invalid Radio Id received, Validation failed \r\n");
            return OSIX_FAILURE;
        }
    }
    if (pRadioIfQos->isPresent != OSIX_FALSE)
    {
        if (pRadioIfQos->u2MessageType != RADIO_QOS_MSG_TYPE)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC, "RadioIfConfigStatusValidateRsp:"
                         "Invalid message type received for Radio Info\r\n");
            return OSIX_FAILURE;
        }
        if (pRadioIfQos->u2MessageLength != RADIO_QOS_MSG_LEN)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC, "RadioIfConfigStatusValidateRsp:"
                         "Invalid message length received for Radio Info\r\n");
            return OSIX_FAILURE;
        }
        if ((pRadioIfQos->u1RadioId > MAX_NUM_OF_RADIO_PER_AP)
            || (pRadioIfQos->u1RadioId == 0))
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC, "RadioIfConfigStatusValidateRsp :"
                         "Invalid Radio Id received, Validation failed \r\n");
            return OSIX_FAILURE;
        }

        for (u1Index = 0; u1Index < pRadioIfQos->u1Index; u1Index++)
        {
            if (pRadioIfQos->u2CwMin[u1Index] > RADIO_QOS_CWMIN_MAX_VAL)
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                             "RadioIfConfigStatusValidateRsp :"
                             "Invalid value for cwmin, Validation failed \r\n");
                return OSIX_FAILURE;
            }
            if ((pRadioIfQos->u1Aifsn[u1Index] > RADIO_QOS_AIFSN_MAX_VAL))
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                             "RadioIfConfigStatusValidateRsp :"
                             "Invalid value for aifsn, Validation failed \r\n");
                return OSIX_FAILURE;
            }
        }
    }
    if (pRadioIfConfig->isPresent != OSIX_FALSE)
    {
        if (pRadioIfConfig->u2MessageType != RADIO_CONFIG_MSG_TYPE)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC, "RadioIfConfigStatusValidateRsp:"
                         "Invalid message type received for Radio Config\r\n");
            return OSIX_FAILURE;
        }
        if (pRadioIfConfig->u2MessageLength != RADIO_CONFIG_MSG_LEN)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC, "RadioIfConfigStatusValidateRsp:"
                         "Invalid message length received for Radio Config\r\n");
            return OSIX_FAILURE;
        }
        if ((pRadioIfConfig->u1RadioId > MAX_NUM_OF_RADIO_PER_AP)
            || (pRadioIfConfig->u1RadioId == 0))
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC, "RadioIfConfigStatusValidateRsp:"
                         "Invalid Radio Id received, Validation failed \r\n");
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfConfigStatusRsp                               *
 *                                                                           *
 * Description        : Config Status Response processing from AC to WTP     *
 *                                                                           *
 * Input(s)           : pWssMsgStruct - Pointer to the input structure       *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfConfigStatusRsp (tRadioIfMsgStruct * pWssMsgStruct)
{
    tRadioIfGetDB       RadioIfGetDB;
    UINT1               u1Index = 0;
    UINT1               u1AntennaCount = 0;
    UINT2               u2RxMcs = 0;
    UINT2               u2TxMcs = 0;
    UINT2               u2RxDataRate = 0;
    UINT2               u2TxDataRate = 0;
    UINT1               u1Sts = 0;
    UINT1               au1RadioName[5] = { 0 };

#ifdef RFMGMT_WANTED
    tRfMgmtMsgStruct    RfMgmtMsgStruct;
    MEMSET (&RfMgmtMsgStruct, 0, sizeof (tRfMgmtMsgStruct));
#endif

    if (pWssMsgStruct == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC, "RadioIfConfigUpdateReq : "
                     "Input validation failed, return failure\r\n");
        return OSIX_FAILURE;
    }

    tRadioIfAntenna    *pRadioIfAntenna =
        &(pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusRsp.RadioIfAntenna);
    tRadioIfDSSSPhy    *pRadioIfDSSSPhy =
        &(pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusRsp.RadioIfDSSPhy);
    tRadioIfMacOperation *pRadioIfMacOperation =
        &(pWssMsgStruct->unRadioIfMsg.
          RadioIfConfigStatusRsp.RadioIfMacOperation);
    tRadioIfMultiDomainCap *pRadioIfMultiDomCap =
        &(pWssMsgStruct->unRadioIfMsg.
          RadioIfConfigStatusRsp.RadioIfMultiDomainCap);
    tRadioIfOFDMPhy    *pRadioIfOFDMPhy =
        &(pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusRsp.RadioIfOFDMPhy);
    tRadioIfRateSet    *pRadioIfRateSet =
        &(pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusRsp.RadioIfRateSet);
    tRadioIfSupportedRate *pRadioIfSupportedRate =
        &(pWssMsgStruct->unRadioIfMsg.
          RadioIfConfigStatusRsp.RadioIfSupportedRate);
    tRadioIfTxPower    *pRadioIfTxPower =
        &(pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusRsp.RadioIfTxPower);
    tRadioIfQos        *pRadioIfQos =
        &(pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusRsp.RadioIfQos);
    tRadioIfConfig     *pRadioIfConfig =
        &(pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusRsp.RadioIfConfig);
    tRadioIfInfoElement *pRadioIfInfoElem =
        &(pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusRsp.RadioIfInfoElem);
    tRadioIfDot11nCfg  *pRadioIfDot11nCfg =
        &(pWssMsgStruct->unRadioIfMsg.RadioIfConfigStatusRsp.RadioIfDot11nCfg);

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
        UtlShMemAllocAntennaSelectionBuf ();

    if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC, "\r%% Memory allocation for Antenna"
                     " selection failed.\r\n");
        return OSIX_FAILURE;
    }

    RadioIfGetDB.RadioIfIsGetAllDB.bBssIdCount = OSIX_TRUE;
    /* multi radio IfIndex */

    if (pRadioIfAntenna->isPresent != OSIX_FALSE)
    {
        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
            SYS_DEF_MAX_ENET_INTERFACES + pRadioIfAntenna->u1RadioId;
    }
    if (pRadioIfDSSSPhy->isPresent != OSIX_FALSE)
    {
        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
            SYS_DEF_MAX_ENET_INTERFACES + pRadioIfDSSSPhy->u1RadioId;
    }
    if (pRadioIfInfoElem->isPresent != OSIX_FALSE)
    {
        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
            SYS_DEF_MAX_ENET_INTERFACES + pRadioIfInfoElem->u1RadioId;
    }
    if (pRadioIfMacOperation->isPresent != OSIX_FALSE)
    {
        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
            SYS_DEF_MAX_ENET_INTERFACES + pRadioIfMacOperation->u1RadioId;
    }
    if (pRadioIfMultiDomCap->isPresent != OSIX_FALSE)
    {
        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
            SYS_DEF_MAX_ENET_INTERFACES + pRadioIfMultiDomCap->u1RadioId;
    }
    if (pRadioIfOFDMPhy->isPresent != OSIX_FALSE)
    {
        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
            SYS_DEF_MAX_ENET_INTERFACES + pRadioIfOFDMPhy->u1RadioId;
    }
    if (pRadioIfRateSet->isPresent != OSIX_FALSE)
    {
        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
            SYS_DEF_MAX_ENET_INTERFACES + pRadioIfRateSet->u1RadioId;
    }
    if (pRadioIfSupportedRate->isPresent != OSIX_FALSE)
    {
        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
            SYS_DEF_MAX_ENET_INTERFACES + pRadioIfSupportedRate->u1RadioId;
    }
    if (pRadioIfTxPower->isPresent != OSIX_FALSE)
    {
        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
            SYS_DEF_MAX_ENET_INTERFACES + pRadioIfTxPower->u1RadioId;
    }
    if (pRadioIfQos->isPresent != OSIX_FALSE)
    {
        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
            SYS_DEF_MAX_ENET_INTERFACES + pRadioIfQos->u1RadioId;
    }
    if (pRadioIfConfig->isPresent != OSIX_FALSE)
    {
        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
            SYS_DEF_MAX_ENET_INTERFACES + pRadioIfConfig->u1RadioId;
    }
    if (pRadioIfDot11nCfg->isPresent != OSIX_FALSE)
    {
        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
            SYS_DEF_MAX_ENET_INTERFACES + pRadioIfDot11nCfg->u1RadioId;
    }

    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, (&RadioIfGetDB)) !=
        OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC, "RadioIfProcessConfigUpdateReq:"
                     "RBTREE get of RadioIf DB failed \r\n");
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                         RadioIfGetAllDB.pu1AntennaSelection);
        return OSIX_FAILURE;
    }

    if (RadioIfGetDB.RadioIfGetAllDB.u1BssIdCount == 0)
    {
        RadioIfGetDB.RadioIfGetAllDB.u1WlanId = 1;
    }
    else
    {
        for (u1Index = 1; u1Index <= RADIO_MAX_BSSID_SUPPORTED; u1Index++)
        {
            RadioIfGetDB.RadioIfGetAllDB.u1WlanId = u1Index;
            if (WssIfProcessRadioIfDBMsg (WSS_GET_WLAN_BSS_IFINDEX_DB,
                                          (&RadioIfGetDB)) == OSIX_SUCCESS)
            {
                if (RadioIfGetDB.RadioIfGetAllDB.u4BssIfIndex != 0)
                    break;
            }
            else
                continue;
        }
    }
    if (pRadioIfAntenna->isPresent != OSIX_FALSE)
    {
        RadioIfGetDB.RadioIfIsGetAllDB.bDiversitySupport = OSIX_TRUE;
        RadioIfGetDB.RadioIfGetAllDB.u1DiversitySupport =
            pRadioIfAntenna->u1ReceiveDiversity;

        RadioIfGetDB.RadioIfIsGetAllDB.bAntennaMode = OSIX_TRUE;
        RadioIfGetDB.RadioIfGetAllDB.u1AntennaMode =
            pRadioIfAntenna->u1AntennaMode;

        for (u1AntennaCount = 0;
             u1AntennaCount < pRadioIfAntenna->u1CurrentTxAntenna;
             u1AntennaCount++)
        {
            RadioIfGetDB.RadioIfIsGetAllDB.bAntennaSelection[u1AntennaCount]
                = OSIX_TRUE;
            RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection[u1AntennaCount]
                = pRadioIfAntenna->au1AntennaSelection[u1AntennaCount];
        }
    }

    if (pRadioIfDSSSPhy->isPresent != OSIX_FALSE)
    {
        RadioIfGetDB.RadioIfIsGetAllDB.bCurrentChannel = OSIX_TRUE;
        RadioIfGetDB.RadioIfGetAllDB.u1CurrentChannel = (UINT1)
            pRadioIfDSSSPhy->i1CurrentChannel;

        RadioIfGetDB.RadioIfIsGetAllDB.bEDThreshold = OSIX_TRUE;
        RadioIfGetDB.RadioIfGetAllDB.i4EDThreshold =
            pRadioIfDSSSPhy->i4EDThreshold;
#ifdef RFMGMT_WANTED
        if (RadioIfGetDB.RadioIfGetAllDB.u1CurrentChannel != 0)
        {
            RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtIntfConfigReq.u4RadioIfIndex =
                RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex;
            RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtIntfConfigReq.u2Channel =
                (UINT2) RadioIfGetDB.RadioIfGetAllDB.u1CurrentChannel;
            if (WssIfProcessRfMgmtMsg (RFMGMT_CHANNEL_UPDATE_MSG,
                                       &RfMgmtMsgStruct) != OSIX_SUCCESS)
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                             "Update Channel to RF modul returns failure \r\n");
            }
        }

#endif
    }

    if (pRadioIfInfoElem->isPresent != OSIX_FALSE)
    {
        switch (pRadioIfInfoElem->u1EleId)
        {
            case DOT11_INFO_ELEM_HTCAPABILITY:

                RadioIfGetDB.RadioIfIsGetAllDB.bHtCapInfo = OSIX_TRUE;
                RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u2HtCapInfo =
                    pRadioIfInfoElem->unInfoElem.HTCapability.u2HTCapInfo;
                RadioIfGetDB.RadioIfIsGetAllDB.bAmpduParam = OSIX_TRUE;
                RadioIfGetDB.RadioIfGetAllDB.Dot11NampduParams.u1AmpduParam =
                    pRadioIfInfoElem->unInfoElem.HTCapability.u1AMPDUParam;

                RadioIfGetDB.RadioIfIsGetAllDB.bHtCapaMcs = OSIX_TRUE;
                MEMCPY (RadioIfGetDB.RadioIfGetAllDB.Dot11NsuppMcsParams.
                        au1HtCapaMcs,
                        pRadioIfInfoElem->unInfoElem.HTCapability.au1SuppMCSSet,
                        sizeof (RadioIfGetDB.RadioIfGetAllDB.
                                Dot11NsuppMcsParams.au1HtCapaMcs));

                RadioIfGetDB.RadioIfIsGetAllDB.bHtExtCap = OSIX_TRUE;
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtExtCapParams.u2HtExtCap =
                    pRadioIfInfoElem->unInfoElem.HTCapability.u2HTExtCap;

                RadioIfGetDB.RadioIfIsGetAllDB.bTxBeamCapParam = OSIX_TRUE;
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u4TxBeamCapParam =
                    pRadioIfInfoElem->unInfoElem.HTCapability.u4TranBeamformCap;

                RadioIfGetDB.RadioIfIsGetAllDB.bHtCapInfo = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bAmpduParam = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bHtCapaMcs = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bHtExtCap = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bTxBeamCapParam = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bPrimaryChannel = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bHTOpeInfo = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bBasicMCSSet = OSIX_TRUE;
                break;

            case DOT11_INFO_ELEM_HTOPERATION:
                RadioIfGetDB.RadioIfIsGetAllDB.bPrimaryChannel = OSIX_TRUE;
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.
                    u1PrimaryChannel =
                    pRadioIfInfoElem->unInfoElem.HTOperation.u1PrimaryCh;
                RadioIfGetDB.RadioIfIsGetAllDB.bHTOpeInfo = OSIX_TRUE;
                MEMCPY (RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.
                        au1HTOpeInfo,
                        pRadioIfInfoElem->unInfoElem.HTOperation.au1HTOpeInfo,
                        WSSMAC_HTOPE_INFO);

                RadioIfGetDB.RadioIfIsGetAllDB.bBasicMCSSet = OSIX_TRUE;
                MEMCPY (RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.
                        au1SuppBasicMCSSet,
                        pRadioIfInfoElem->unInfoElem.HTOperation.au1BasicMCSSet,
                        WSSMAC_BASIC_MCS_SET);
                break;

            case DOT11AC_INFO_ELEM_VHTCAPABILITY:

                RadioIfGetDB.RadioIfIsGetAllDB.bVhtCapInfo = OSIX_TRUE;
                RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u4VhtCapInfo =
                    pRadioIfInfoElem->unInfoElem.VHTCapability.u4VhtCapInfo;

                if (WssIfProcessRadioIfDBMsg (WSS_PARSE_11AC_CAPABILITY_INFO,
                                              (&RadioIfGetDB)) != OSIX_SUCCESS)
                {
                    RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                                 "RadioIfProcess11AcStatusReq: Parsing"
                                 " au1VhtOperInfo"
                                 "to individual elements failed\n");
                    UtlShMemFreeAntennaSelectionBuf
                        (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
                    return OSIX_FAILURE;
                }
                RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.
                    u1MaxMPDULen = RadioIfGetDB.RadioIfGetAllDB.
                    Dot11AcCapaParams.u1SuppMaxMPDULen;
                RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.
                    u1RxLpdc = RadioIfGetDB.RadioIfGetAllDB.
                    Dot11AcCapaParams.u1SuppRxLpdc;
                RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.
                    u1ShortGi80 = RadioIfGetDB.RadioIfGetAllDB.
                    Dot11AcCapaParams.u1SuppShortGi80;
                RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.
                    u1TxStbc = RadioIfGetDB.RadioIfGetAllDB.
                    Dot11AcCapaParams.u1SuppTxStbc;
                RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.
                    u1RxStbc = RadioIfGetDB.RadioIfGetAllDB.
                    Dot11AcCapaParams.u1SuppRxStbc;
                RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.
                    u1VhtMaxAMPDU = RadioIfGetDB.RadioIfGetAllDB.
                    Dot11AcCapaParams.u1SuppVhtMaxAMPDU;
                RadioIfGetDB.RadioIfIsGetAllDB.bMaxMPDULen = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bRxLpdc = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bShortGi80 = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bTxStbc = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bRxStbc = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bVhtMaxAMPDU = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bVhtOperMcsSet = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bVhtSTS = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bVhtCapaMcs = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bRxDataRate = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bTxDataRate = OSIX_TRUE;

                MEMCPY (RadioIfGetDB.RadioIfGetAllDB.
                        Dot11AcCapaParams.au1VhtCapaMcs,
                        pRadioIfInfoElem->unInfoElem.VHTCapability.au1SuppMCS,
                        DOT11AC_VHT_CAP_MCS_LEN);
                MEMCPY (&u2RxMcs, (pRadioIfInfoElem->unInfoElem.
                                   VHTCapability.au1SuppMCS), DOT11AC_MCS_MAP);
                MEMCPY (&u2TxMcs, &(pRadioIfInfoElem->unInfoElem.
                                    VHTCapability.au1SuppMCS[DOT11AC_MCS_MAP +
                                                             DOT11AC_MCS_MAP]),
                        DOT11AC_MCS_MAP);
                WssIfVhtSts (&RadioIfGetDB, &u1Sts);
                RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1VhtSTS = u1Sts;
                RadioIfGetDB.RadioIfGetAllDB.Dot11AcOperParams.
                    u2VhtOperMcsSet = (u2RxMcs & u2TxMcs);
                /*Highest Supported Long Gi Data Rate */
                MEMCPY (&u2RxDataRate, &(RadioIfGetDB.RadioIfGetAllDB.
                                         Dot11AcCapaParams.
                                         au1VhtCapaMcs[RADIO_VALUE_2]),
                        RADIO_VALUE_2);
                RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u2RxDataRate =
                    (UINT2) ((OSIX_NTOHS (u2RxDataRate)) >> RADIO_VALUE_3);
                MEMCPY (&u2TxDataRate,
                        &(RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.
                          au1VhtCapaMcs[RADIO_VALUE_6]), RADIO_VALUE_2);
                RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u2TxDataRate =
                    (UINT2) ((OSIX_NTOHS (u2TxDataRate)) >> RADIO_VALUE_3);

                break;

            case DOT11AC_INFO_ELEM_VHTOPERATION:

                RadioIfGetDB.RadioIfIsGetAllDB.bChannelWidth = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bLocalTxPower = OSIX_TRUE;
                if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                              (&RadioIfGetDB)) != OSIX_SUCCESS)
                {
                    RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                                 "RadioIfProcessConfigUpdateReq: RBTREE get of RadioIf "
                                 "DB failed in VHT Opeartion info element\r\n");
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    return OSIX_FAILURE;
                }
                RadioIfGetDB.RadioIfIsGetAllDB.bChannelWidth = OSIX_FALSE;
                RadioIfGetDB.RadioIfIsGetAllDB.bLocalTxPower = OSIX_FALSE;

                RadioIfGetDB.RadioIfIsGetAllDB.bVhtSTS = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bSpecMgmt = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bVhtOption = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bVhtOperModeNotify = OSIX_TRUE;
                if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11AC,
                                              (&RadioIfGetDB)) != OSIX_SUCCESS)
                {
                    RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                                 "RadioIfProcessConfigUpdateReq: RBTREE get of RadioIf "
                                 "DB failed in VHT Opeartion info element\r\n");
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    return OSIX_FAILURE;
                }
                RadioIfGetDB.RadioIfIsGetAllDB.bVhtSTS = OSIX_FALSE;
                RadioIfGetDB.RadioIfIsGetAllDB.bSpecMgmt = OSIX_FALSE;
                RadioIfGetDB.RadioIfIsGetAllDB.bVhtOption = OSIX_FALSE;
                RadioIfGetDB.RadioIfIsGetAllDB.bVhtOperModeNotify = OSIX_FALSE;

                RadioIfGetDB.RadioIfIsGetAllDB.bVhtOperInfo = OSIX_TRUE;
                MEMCPY ((RadioIfGetDB.RadioIfGetAllDB.
                         Dot11AcOperParams.au1VhtOperInfo),
                        (pRadioIfInfoElem->unInfoElem.
                         VHTOperation.au1VhtOperInfo),
                        DOT11AC_VHT_OPER_INFO_LEN);
                RadioIfGetDB.RadioIfIsGetAllDB.bVhtOperMcsSet = OSIX_TRUE;
                RadioIfGetDB.RadioIfGetAllDB.Dot11AcOperParams.u2VhtOperMcsSet =
                    pRadioIfInfoElem->unInfoElem.VHTOperation.u2BasicMCS;
                if (WssIfProcessRadioIfDBMsg (WSS_PARSE_11AC_OPER_INFO,
                                              (&RadioIfGetDB)) != OSIX_SUCCESS)
                {
                    RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                                 "RadioIfProcess11AcStatusReq: Parsing"
                                 "au1VhtOperInfo"
                                 "to individual elements failed\n");
                    UtlShMemFreeAntennaSelectionBuf
                        (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
                    return OSIX_FAILURE;
                }
                RadioIfGetDB.RadioIfGetAllDB.Dot11AcOperParams.
                    u1VhtChannelWidth =
                    RadioIfGetDB.RadioIfGetAllDB.Dot11AcOperParams.
                    u1SuppVhtChannelWidth;
                RadioIfGetDB.RadioIfGetAllDB.Dot11AcOperParams.u1CenterFcy0 =
                    RadioIfGetDB.RadioIfGetAllDB.Dot11AcOperParams.
                    u1SuppCenterFcy0;
                RadioIfGetDB.RadioIfGetAllDB.Dot11AcOperParams.
                    u1VhtOperModeNotify = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bVhtChannelWidth = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bCenterFcy0 = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bCenterFcy1 = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bVhtOperModeNotify = OSIX_TRUE;

                /* Assembling Operation Mode Notification Element */
                if (RadioIfGetDB.RadioIfGetAllDB.Dot11AcOperParams.
                    u1VhtOperModeNotify == OSIX_TRUE)
                {
                    WssIfAssembleOperModeNotify (&RadioIfGetDB);
                }
                /* Assembling Vht Power Envelope Element */
                if ((RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.
                     u1SpecMgmt == OSIX_TRUE) && (RadioIfGetDB.
                                                  RadioIfGetAllDB.
                                                  Dot11AcOperParams.
                                                  u1VhtOption == OSIX_TRUE))
                {
                    WssIfAssembleVhtPowerEnvelope (&RadioIfGetDB);
                }

                break;

            default:
                break;
        }

    }

    if (pRadioIfMacOperation->isPresent != OSIX_FALSE)
    {
        RadioIfGetDB.RadioIfIsGetAllDB.bRTSThreshold = OSIX_TRUE;
        RadioIfGetDB.RadioIfGetAllDB.u2RTSThreshold =
            pRadioIfMacOperation->u2RTSThreshold;

        RadioIfGetDB.RadioIfIsGetAllDB.bShortRetry = OSIX_TRUE;
        RadioIfGetDB.RadioIfGetAllDB.u1ShortRetry =
            pRadioIfMacOperation->u1ShortRetry;

        RadioIfGetDB.RadioIfIsGetAllDB.bLongRetry = OSIX_TRUE;
        RadioIfGetDB.RadioIfGetAllDB.u1LongRetry =
            pRadioIfMacOperation->u1LongRetry;

        RadioIfGetDB.RadioIfIsGetAllDB.bFragmentationThreshold = OSIX_TRUE;
        RadioIfGetDB.RadioIfGetAllDB.u2FragmentationThreshold =
            pRadioIfMacOperation->u2FragmentThreshold;

        RadioIfGetDB.RadioIfIsGetAllDB.bTxMsduLifetime = OSIX_TRUE;
        RadioIfGetDB.RadioIfGetAllDB.u4TxMsduLifetime =
            pRadioIfMacOperation->u4TxMsduLifetime;

        RadioIfGetDB.RadioIfIsGetAllDB.bRxMsduLifetime = OSIX_TRUE;
        RadioIfGetDB.RadioIfGetAllDB.u4RxMsduLifetime =
            pRadioIfMacOperation->u4RxMsduLifetime;
    }

    if (pRadioIfMultiDomCap->isPresent != OSIX_FALSE)
    {
        RadioIfGetDB.RadioIfIsGetAllDB.bFirstChannelNumber = OSIX_TRUE;
        RadioIfGetDB.RadioIfGetAllDB.u2FirstChannelNumber =
            pRadioIfMultiDomCap->u2FirstChannel;

        RadioIfGetDB.RadioIfIsGetAllDB.bNoOfChannels = OSIX_TRUE;
        RadioIfGetDB.RadioIfGetAllDB.u1NoOfSupportedPowerLevels =
            (UINT1) pRadioIfMultiDomCap->u2NumOfChannels;

        RadioIfGetDB.RadioIfIsGetAllDB.bMaxTxPowerLevel = OSIX_TRUE;

        RadioIfGetDB.RadioIfGetAllDB.u2MaxTxPowerLevel =
            pRadioIfMultiDomCap->u2MaxTxPowerLevel;
    }

    if (pRadioIfOFDMPhy->isPresent != OSIX_FALSE)
    {
        RadioIfGetDB.RadioIfIsGetAllDB.bCurrentFrequency = OSIX_TRUE;
        RadioIfGetDB.RadioIfGetAllDB.u1CurrentFrequency =
            pRadioIfOFDMPhy->u1CurrentChannel;

        RadioIfGetDB.RadioIfIsGetAllDB.bCurrentFrequency = OSIX_TRUE;
        RadioIfGetDB.RadioIfGetAllDB.u4T1Threshold =
            pRadioIfOFDMPhy->u4T1Threshold;

#ifdef RFMGMT_WANTED
        if (RadioIfGetDB.RadioIfGetAllDB.u1CurrentFrequency != 0)
        {
            RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtIntfConfigReq.u4RadioIfIndex =
                RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex;
            RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtIntfConfigReq.u2Channel =
                (UINT2) RadioIfGetDB.RadioIfGetAllDB.u1CurrentFrequency;
            if (WssIfProcessRfMgmtMsg (RFMGMT_CHANNEL_UPDATE_MSG,
                                       &RfMgmtMsgStruct) != OSIX_SUCCESS)
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                             "Update Channel to RF modul returns failure \r\n");
            }
        }

#endif

    }
    if (pRadioIfRateSet->isPresent != OSIX_FALSE)
    {
        RadioIfGetDB.RadioIfIsGetAllDB.bOperationalRate = OSIX_TRUE;
        MEMCPY (RadioIfGetDB.RadioIfGetAllDB.au1OperationalRate,
                pRadioIfRateSet->au1OperationalRate, RADIOIF_OPER_RATE);
    }
    if (pRadioIfSupportedRate->isPresent != OSIX_FALSE)
    {
        RadioIfGetDB.RadioIfIsGetAllDB.bSupportedRate = OSIX_TRUE;
        MEMCPY (RadioIfGetDB.RadioIfGetAllDB.au1SupportedRate,
                pRadioIfSupportedRate->au1SupportedRate, RADIOIF_OPER_RATE);
    }
    if (pRadioIfTxPower->isPresent != OSIX_FALSE)
    {
        RadioIfGetDB.RadioIfIsGetAllDB.bCurrentTxPower = OSIX_TRUE;
        RadioIfGetDB.RadioIfGetAllDB.i2CurrentTxPower =
            (INT2) pRadioIfTxPower->u2TxPowerLevel;
#ifdef RFMGMT_WANTED
        if (RadioIfGetDB.RadioIfGetAllDB.i2CurrentTxPower != 0)
        {
            RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtIntfConfigReq.u4RadioIfIndex =
                RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex;
            RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtIntfConfigReq.u2TxPower =
                (UINT2) RadioIfGetDB.RadioIfGetAllDB.i2CurrentTxPower;
            if (WssIfProcessRfMgmtMsg (RFMGMT_TXPOWER_UPDATE_MSG,
                                       &RfMgmtMsgStruct) != OSIX_SUCCESS)
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                             "Update TxPower to RF module returns failure\r\n");
            }
        }

#endif

    }
    if (pRadioIfQos->isPresent != OSIX_FALSE)
    {
        RadioIfGetDB.RadioIfIsGetAllDB.bQosIndex = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bQueueDepth = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bCwMin = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bCwMax = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bAifsn = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bTxOpLimit = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bQosPrio = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bDscp = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bTaggingPolicy = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bAdmissionControl = OSIX_TRUE;
        RadioIfGetDB.RadioIfGetAllDB.u1TaggingPolicy =
            pRadioIfQos->u1TaggingPolicy;

        for (u1Index = 0; u1Index < RADIO_QOS_PROFILE; u1Index++)
        {
            RadioIfGetDB.RadioIfGetAllDB.au1QueueDepth[u1Index] =
                pRadioIfQos->u1QueueDepth[u1Index];

            RadioIfGetDB.RadioIfGetAllDB.au1QueueDepth[u1Index] =
                pRadioIfQos->u2CwMin[u1Index];

            RadioIfGetDB.RadioIfGetAllDB.au2CwMax[u1Index] =
                pRadioIfQos->u2CwMax[u1Index];

            RadioIfGetDB.RadioIfGetAllDB.au1Aifsn[u1Index] =
                pRadioIfQos->u1Aifsn[u1Index];

            RadioIfGetDB.RadioIfGetAllDB.au1QosPrio[u1Index] =
                pRadioIfQos->u1Prio[u1Index];

            RadioIfGetDB.RadioIfGetAllDB.au1Dscp[u1Index] =
                pRadioIfQos->u1Dscp[u1Index];

            RadioIfGetDB.RadioIfGetAllDB.au1AdmissionControl[u1Index] =
                pRadioIfQos->u1AdmissionControl[u1Index];

            RadioIfGetDB.RadioIfGetAllDB.u1QosIndex = u1Index;
            /*To set the values in RadioIfDB */
            if (WssIfProcessRadioIfDBMsg (WSS_SET_RADIO_QOS_CONFIG_DB_WTP,
                                          &RadioIfGetDB) != OSIX_SUCCESS)
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                             "RadioDb Updation Failed \r\n");
                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                 RadioIfGetAllDB.
                                                 pu1AntennaSelection);
                return OSIX_FAILURE;
            }
        }
    }
    if (pRadioIfConfig->isPresent != OSIX_FALSE)
    {
        if (pRadioIfConfig->u1ShortPreamble != 0)
        {
            RadioIfGetDB.RadioIfGetAllDB.u1ShortPreamble =
                pRadioIfConfig->u1ShortPreamble;
            RadioIfGetDB.RadioIfIsGetAllDB.bShortPreamble = OSIX_TRUE;
        }
        if (pRadioIfConfig->u1DtimPeriod != 0)
        {
            RadioIfGetDB.RadioIfIsGetAllDB.bDTIMPeriod = OSIX_TRUE;
            RadioIfGetDB.RadioIfGetAllDB.u1DTIMPeriod =
                pRadioIfConfig->u1DtimPeriod;
        }
        if (pRadioIfConfig->u2BeaconPeriod != 0)
        {
            RadioIfGetDB.RadioIfIsGetAllDB.bBeaconPeriod = OSIX_TRUE;
            RadioIfGetDB.RadioIfGetAllDB.u2BeaconPeriod =
                pRadioIfConfig->u2BeaconPeriod;
        }
        if ((STRLEN (pRadioIfConfig->au1CountryString)) != 0)
        {
            RadioIfGetDB.RadioIfIsGetAllDB.bCountryString = OSIX_TRUE;
            STRNCPY (RadioIfGetDB.RadioIfGetAllDB.au1CountryString,
                     pRadioIfConfig->au1CountryString, RADIOIF_COUNTRY_STR_LEN);
            SPRINTF ((CHR1 *) au1RadioName, "%s%d", RADIO_INTF_NAME,
                     pRadioIfConfig->u1RadioId - 1);
            RadioIfGetDB.RadioIfGetAllDB.u1RadioId = pRadioIfConfig->u1RadioId;
            if (RadioIfSetCountryString (&RadioIfGetDB,
                                         (UINT1 *) &au1RadioName) !=
                OSIX_SUCCESS)
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                             "RadioIfConfigUpdateReq: Country String - HW call to set "
                             "the values failed\r\n");
                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                 RadioIfGetAllDB.
                                                 pu1AntennaSelection);
                return OSIX_FAILURE;
            }
            if (RadioIfGetMultiDomainAndAssembleBeacon (&RadioIfGetDB,
                                                        (UINT1 *) &au1RadioName)
                != OSIX_SUCCESS)
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                             "RadioIfWlanUpdate: Hw get for Multi Domain Capability Failed\n");
            }

            RadioIfGetDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_FALSE;
            RadioIfGetDB.RadioIfIsGetAllDB.bMultiDomainInfo = OSIX_TRUE;
            if (WssIfProcessRadioIfDBMsg (WSS_SET_RADIO_IF_DB, &RadioIfGetDB)
                != OSIX_SUCCESS)
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                             "RadioIfWlanUpdate: Value to be set in DB Failed \r\n");
                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                 RadioIfGetAllDB.
                                                 pu1AntennaSelection);
                return OSIX_FAILURE;
            }
        }
    }
    if (pRadioIfDot11nCfg->isPresent != OSIX_FALSE)
    {
        RadioIfGetDB.RadioIfGetAllDB.u1AMPDUStatus =
            pRadioIfDot11nCfg->u1AMPDUStatus;
        RadioIfGetDB.RadioIfGetAllDB.u1AMPDUSubFrame =
            pRadioIfDot11nCfg->u1AMPDUSubFrame;
        RadioIfGetDB.RadioIfGetAllDB.u2AMPDULimit =
            pRadioIfDot11nCfg->u2AMPDULimit;
        RadioIfGetDB.RadioIfGetAllDB.u1AMSDUStatus =
            pRadioIfDot11nCfg->u1AMSDUStatus;
        RadioIfGetDB.RadioIfGetAllDB.u2AMSDULimit =
            pRadioIfDot11nCfg->u2AMSDULimit;

        RadioIfGetDB.RadioIfIsGetAllDB.bAMPDUStatus = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bAMPDUSubFrame = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bAMPDULimit = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bAMSDUStatus = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bAMSDULimit = OSIX_TRUE;

        MEMSET (au1RadioName, 0, RADIO_INTF_NAME_LEN);
        SPRINTF ((CHR1 *) au1RadioName, "%s%d", RADIO_INTF_NAME,
                 (pRadioIfDot11nCfg->u1RadioId - 1));
        if (RadioIfSetDot11nConfig (&RadioIfGetDB,
                                    (UINT1 *) &au1RadioName) != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigUpdateReq: 80211n Config - HW call to set "
                         "the values failed\r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            return OSIX_FAILURE;
        }
    }
    /*To set the values in RadioIfDB */
    if (((pRadioIfInfoElem->u1EleId) == DOT11_INFO_ELEM_HTCAPABILITY) ||
        ((pRadioIfInfoElem->u1EleId) == DOT11_INFO_ELEM_HTOPERATION) ||
        (pRadioIfDot11nCfg->isPresent == OSIX_TRUE))
    {
        if (WssIfProcessRadioIfDBMsg (WSS_SET_RADIO_IF_DB_11N, &RadioIfGetDB)
            != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC, "RadioDb Updation Failed"
                         "for 11AC params\r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            return OSIX_FAILURE;
        }

    }
    else if (((pRadioIfInfoElem->u1EleId) == DOT11AC_INFO_ELEM_VHTCAPABILITY) ||
             ((pRadioIfInfoElem->u1EleId) == DOT11AC_INFO_ELEM_VHTOPERATION))
    {
        if (WssIfProcessRadioIfDBMsg (WSS_SET_RADIO_IF_DB_11AC, &RadioIfGetDB)
            != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC, "RadioDb Updation Failed"
                         "for 11AC params\r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            return OSIX_FAILURE;
        }

    }
    else
    {

        if (WssIfProcessRadioIfDBMsg (WSS_SET_RADIO_IF_DB, &RadioIfGetDB) !=
            OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC, "RadioDb Updation Failed \r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            return OSIX_FAILURE;
        }
    }
    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                     RadioIfGetAllDB.pu1AntennaSelection);
    return OSIX_SUCCESS;

}

/*****************************************************************************
 * Function Name      : RadioIfConfigUpdateReqValidate                       *
 *                                                                           *
 * Description        : Config Update Request validate from AC to WTP        *
 *                                                                           *
 * Input(s)           : pWssMsgStruct - Pointer to the input structure       *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfConfigUpdateReqValidate (tRadioIfMsgStruct * pWssMsgStruct)
{
    UINT2               u2Index = 0;
    UINT1               u1Index = 0;

    if (pWssMsgStruct == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC, "RadioIfConfigUpdateReq :"
                     "Input validation failed, return failure\r\n");
        return OSIX_FAILURE;
    }

    tRadioIfAdminStatus *pRadioIfAdminStatus =
        &(pWssMsgStruct->unRadioIfMsg.
          RadioIfConfigUpdateReq.RadioIfAdminStatus);
    tRadioIfAntenna    *pRadioIfAntenna =
        &(pWssMsgStruct->unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfAntenna);
    tRadioIfDSSSPhy    *pRadioIfDSSSPhy =
        &(pWssMsgStruct->unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfDSSSPhy);
    tRadioIfMacOperation *pRadioIfMacOperation =
        &(pWssMsgStruct->unRadioIfMsg.
          RadioIfConfigUpdateReq.RadioIfMacOperation);
    tRadioIfOFDMPhy    *pRadioIfOFDMPhy =
        &(pWssMsgStruct->unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfOFDMPhy);
    tRadioIfTxPower    *pRadioIfTxPower =
        &(pWssMsgStruct->unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfTxPower);
    tRadioIfQos        *pRadioIfQos =
        &(pWssMsgStruct->unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfQos);
    tRadioIfInfo       *pRadioIfInfo =
        &(pWssMsgStruct->unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfInfo);
    tRadioIfRateSet    *pRadioIfRateSet =
        &(pWssMsgStruct->unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfRateSet);
    tRadioIfMultiDomainCap *pRadioIfMultiDomainCap =
        &(pWssMsgStruct->unRadioIfMsg.
          RadioIfConfigUpdateReq.RadioIfMultiDomainCap);
    tRadioIfConfig     *pRadioIfConfig =
        &(pWssMsgStruct->unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfConfig);
    tRadioIfDot11nParam *pRadioIfDot11nParam =
        &(pWssMsgStruct->unRadioIfMsg.
          RadioIfConfigUpdateReq.RadioIfDot11nParam);
    tRadioIfInfoElement *pRadioIfInfoElem =
        &(pWssMsgStruct->unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfInfoElem);

    if (pRadioIfAdminStatus->isPresent == OSIX_TRUE)
    {
        if (pRadioIfAdminStatus->u2MessageType !=
            RADIOIF_CONFIG_UPDATE_REQ_MSG_TYPE)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC, "RadioIfConfigUpdateReq "
                         "(Admin Status) :Invalid Message Type\r\n");
            return OSIX_FAILURE;
        }

        if (pRadioIfAdminStatus->u2MessageLength != 2)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC, "RadioIfConfigUpdateReq"
                         "(Admin Status): Invalid Length\r\n");
            return OSIX_FAILURE;
        }

        if (pRadioIfAdminStatus->u1RadioId < RADIOIF_START_RADIOID ||
            pRadioIfAdminStatus->u1RadioId > RADIOIF_END_RADIOID)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigUpdateReq(Admin Status):Invalid Radio Id\n");
            return OSIX_FAILURE;
        }

        if (pRadioIfAdminStatus->u1AdminStatus != RADIOIF_ADMIN_STATUS_DISABLED
            && pRadioIfAdminStatus->u1AdminStatus !=
            RADIOIF_ADMIN_STATUS_ENABLED
            && pRadioIfAdminStatus->u1AdminStatus !=
            RADIOIF_ADMIN_STATUS_RESERVED)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigUpdateReq (Admin Status): Invalid Admin "
                         "Status\n");
            return OSIX_FAILURE;
        }

    }
    if (pRadioIfAntenna->isPresent == OSIX_TRUE)
    {

        if (pRadioIfAntenna->u2MessageType != RADIO_ANTENNA_MSG_TYPE)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC, "RadioIfConfigUpdateReq(Antenna):"
                         "Invalid Message Type\r\n");
            return OSIX_FAILURE;
        }

        if (pRadioIfAntenna->u2MessageLength < RADIO_ANTENNA_MSG_MIN_LEN)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC, "RadioIfConfigUpdateReq(Antenna):"
                         "Invalid Length\r\n");
            return OSIX_FAILURE;
        }

        if (pRadioIfAntenna->u1RadioId < RADIO_MIN_COUNT
            || pRadioIfAntenna->u1RadioId > RADIO_MAX_COUNT)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigUpdateReq (Antenna):"
                         "Invalid Radio Id\r\n");
            return OSIX_FAILURE;
        }

        if ((pRadioIfAntenna->u1ReceiveDiversity !=
             CLI_ANTENNA_DIVERSITY_ENABLE)
            && pRadioIfAntenna->u1ReceiveDiversity !=
            CLI_ANTENNA_DIVERSITY_DISABLE)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigUpdateReq (Antenna):"
                         "Invalid Receive Diversity\r\n");
            return OSIX_FAILURE;
        }

        if (pRadioIfAntenna->u1AntennaMode != CLI_ANTENNA_MODE_LEFTA &&
            pRadioIfAntenna->u1AntennaMode != CLI_ANTENNA_MODE_RIGHTA &&
            pRadioIfAntenna->u1AntennaMode != CLI_ANTENNA_MODE_OMNI &&
            pRadioIfAntenna->u1AntennaMode != CLI_ANTENNA_MODE_MIMO)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigUpdateReq (Antenna):"
                         "validation failed, Invalid Antenna Mode(Combiner)\r\n");
            return OSIX_FAILURE;
        }

        if (pRadioIfAntenna->u1CurrentTxAntenna < RADIO_ANTENNA_MIN_COUNT)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigUpdateReq (Antenna):"
                         "validation failed, Invalid Antenna Count\r\n");
            return OSIX_FAILURE;
        }

        for (u2Index = 0; u2Index < pRadioIfAntenna->u1CurrentTxAntenna;
             u2Index++)
        {
            if ((u2Index == CFA_CAPWAP_VIRT_RADIO) &&
                (pRadioIfAntenna->au1AntennaSelection[u2Index] !=
                 CLI_ANTENNA_INTERNAL) &&
                pRadioIfAntenna->au1AntennaSelection[u2Index] !=
                CLI_ANTENNA_EXTERNAL)
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC, "RadioIfConfigUpdateReq "
                             "(Antenna): , Invalid Antenna Selection\r\n");
                return OSIX_FAILURE;
            }
        }
    }

    if (pRadioIfDSSSPhy->isPresent == OSIX_TRUE)
    {
        if (pRadioIfDSSSPhy->u2MessageType != RADIO_CONF_UPDATE_DSSS_MSG_TYPE)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC, "RadioIfConfigUpdateReq (DSSS) :"
                         "validation failed, (DSSS : Invalid Message Type)\r\n");
            return OSIX_FAILURE;
        }

        if (pRadioIfDSSSPhy->u2MessageLength !=
            RADIO_CONF_UPDATE_DSSS_MSG_LENGTH)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC, "RadioIfConfigUpdateReq (DSSS): "
                         "validation failed, (DSSS : Invalid Length)\r\n");
            return OSIX_FAILURE;
        }

        if (pRadioIfDSSSPhy->u1RadioId < RADIO_MIN_COUNT ||
            pRadioIfDSSSPhy->u1RadioId > RADIO_MAX_COUNT)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC, "RadioIfConfigUpdateReq (DSSS): "
                         "validation failed, (DSSS : Invalid Radio Id)\r\n");
            return OSIX_FAILURE;
        }

        if (pRadioIfDSSSPhy->i1CurrentChannel < RADIO_MIN_DSSS_CHANNEL ||
            pRadioIfDSSSPhy->i1CurrentChannel > RADIO_MAX_DSSS_CHANNEL)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC, "RadioIfConfigUpdateReq (DSSS):"
                         "Validation failed, (DSS : Invalid CurrentChannel )\r\n");
            return OSIX_FAILURE;
        }
    }

    if (pRadioIfInfoElem->isPresent == OSIX_TRUE)
    {
        if (pRadioIfInfoElem->u2MessageType != RADIO_INFO_ELEM_MSG_TYPE)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigUpdateReq (Info Elem) :"
                         "validation failed, (Invalid Message Type)\r\n");
            return OSIX_FAILURE;
        }

        switch (pRadioIfInfoElem->u1EleId)
        {
            case DOT11_INFO_ELEM_HTCAPABILITY:
                if (pRadioIfInfoElem->u2MessageLength !=
                    RADIO_INFO_ELEM_FIXED_MSG_LEN + RADIO_INFO_HT_CAP_MSG_LEN)
                {
                    RADIOIF_TRC (RADIOIF_FAILURE_TRC, "RadioIfConfigUpdateReq"
                                 "(HT capability Info Elem): validation failed,"
                                 "return failure(Invalid Length)\r\n");
                    return OSIX_FAILURE;
                }
                if (pRadioIfInfoElem->unInfoElem.HTCapability.u1ElemLen !=
                    RADIO_INFO_HT_CAP_MSG_LEN)
                {
                    RADIOIF_TRC (RADIOIF_FAILURE_TRC, "RadioIfConfigUpdateReq"
                                 "(HT capability Mesg Length): validation failed,"
                                 "return failure(Invalid Length)\r\n");
                    return OSIX_FAILURE;
                }
                break;
            case DOT11_INFO_ELEM_HTOPERATION:
                if (pRadioIfInfoElem->u2MessageLength !=
                    RADIO_INFO_ELEM_FIXED_MSG_LEN + RADIO_INFO_HT_INFO_MSG_LEN)
                {
                    RADIOIF_TRC (RADIOIF_FAILURE_TRC, "RadioIfConfigUpdateReq"
                                 "(HT operation Info Elem): validation failed,"
                                 "return failure(Invalid Length)\r\n");
                    return OSIX_FAILURE;
                }
                if (pRadioIfInfoElem->unInfoElem.HTOperation.u1ElemLen !=
                    RADIO_INFO_HT_INFO_MSG_LEN)
                {
                    RADIOIF_TRC (RADIOIF_FAILURE_TRC, "RadioIfConfigUpdateReq"
                                 "(HT Operation Mesg Length): validation failed,"
                                 "return failure(Invalid Length)\r\n");
                    return OSIX_FAILURE;
                }
                break;

            case DOT11AC_INFO_ELEM_VHTCAPABILITY:
                if (pRadioIfInfoElem->u2MessageLength !=
                    RADIO_INFO_ELEM_FIXED_MSG_LEN +
                    DOT11AC_INFO_VHT_CAP_MSG_LEN)
                {
                    RADIOIF_TRC (RADIOIF_FAILURE_TRC, "RadioIfConfigUpdateReq"
                                 "(VHT capability Info Elem): validation failed,"
                                 "return failure(Invalid Length)\r\n");
                    return OSIX_FAILURE;
                }
                if (pRadioIfInfoElem->unInfoElem.VHTCapability.u1ElemLen !=
                    DOT11AC_INFO_VHT_CAP_MSG_LEN)
                {
                    RADIOIF_TRC (RADIOIF_FAILURE_TRC, "RadioIfConfigUpdateReq"
                                 "(VHT Capability Mesg Length): validation failed,"
                                 "return failure(Invalid Length)\r\n");
                    return OSIX_FAILURE;
                }
                break;

            case DOT11AC_INFO_ELEM_VHTOPERATION:
                if (pRadioIfInfoElem->u2MessageLength !=
                    RADIO_INFO_ELEM_FIXED_MSG_LEN +
                    DOT11AC_INFO_VHT_OPER_MSG_LEN)
                {
                    RADIOIF_TRC (RADIOIF_FAILURE_TRC, "RadioIfConfigUpdateReq"
                                 "(VHT operation Info Elem): validation failed,"
                                 "return failure(Invalid Length)\r\n");
                    return OSIX_FAILURE;
                }
                if (pRadioIfInfoElem->unInfoElem.VHTOperation.u1ElemLen !=
                    DOT11AC_INFO_VHT_OPER_MSG_LEN)
                {
                    RADIOIF_TRC (RADIOIF_FAILURE_TRC, "RadioIfConfigUpdateReq"
                                 "(VHT Operation Mesg Length): validation failed,"
                                 "return failure(Invalid Length)\r\n");
                    return OSIX_FAILURE;
                }
                break;

            default:
                break;
        }
        if (pRadioIfInfoElem->u1EleId > WSS_RADIOIF_INFO_ELEM_MAX_TYPE)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigUpdateReq (Info Elem):"
                         "validation failed, return failure(Invalid Element Id)\r\n");
            return OSIX_FAILURE;
        }

        if (pRadioIfInfoElem->u1RadioId < RADIO_MIN_COUNT ||
            pRadioIfInfoElem->u1RadioId > RADIO_MAX_COUNT)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigUpdateReq (Info Elem):"
                         "validation failed, return failure(Invalid Radio Id)\r\n");
            return OSIX_FAILURE;
        }
    }
    if (pRadioIfOFDMPhy->isPresent == OSIX_TRUE)
    {
        if (pRadioIfOFDMPhy->u2MessageType != RADIO_OFDM_MSG_TYPE)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC, "RadioIfConfigUpdateReq (OFDM) :"
                         "validation failed, (Invalid Message Type)\r\n");
            return OSIX_FAILURE;
        }

        if (pRadioIfOFDMPhy->u2MessageLength != RADIO_OFDM_MSG_LEN)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC, "RadioIfConfigUpdateReq (OFDM):"
                         "validation failed, return failure(Invalid Length)\r\n");
            return OSIX_FAILURE;
        }

        if (pRadioIfOFDMPhy->u1RadioId < RADIO_MIN_COUNT ||
            pRadioIfOFDMPhy->u1RadioId > RADIO_MAX_COUNT)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC, "RadioIfConfigUpdateReq (OFDM):"
                         "validation failed, return failure(Invalid Radio Id)\r\n");
            return OSIX_FAILURE;
        }

        if (pRadioIfOFDMPhy->u1CurrentChannel < RADIO_OFDM_CHANNEL_VALUE_MIN ||
            pRadioIfOFDMPhy->u1CurrentChannel > RADIO_OFDM_CHANNEL_VALUE_MAX)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC, "RadioIfConfigUpdateReq (OFDM): "
                         "validation failed, (Invalid CurrentChannel)\r\n");
            return OSIX_FAILURE;
        }

    }

    if (pRadioIfMacOperation->isPresent == OSIX_TRUE)
    {
        if (pRadioIfMacOperation->u2MessageType != RADIO_MAC_OPER_MSG_TYPE)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC, "RadioIfConfigUpdateReq(Mac):"
                         "validation failed, (Invalid Message Type)\r\n");
            return OSIX_FAILURE;
        }

        if (pRadioIfMacOperation->u2MessageLength != RADIO_MAC_OPER_MSG_LEN)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC, "RadioIfConfigUpdateReq (Mac):"
                         "validation failed, return failure(Invalid Length)\r\n");
            return OSIX_FAILURE;
        }

        if (pRadioIfMacOperation->u1RadioId < RADIO_MIN_COUNT
            || pRadioIfMacOperation->u1RadioId > RADIO_MAX_COUNT)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC, "RadioIfConfigUpdateReq (Mac):"
                         "validation failed, return failure(Invalid Radio Id)\r\n");
            return OSIX_FAILURE;
        }

        if (pRadioIfMacOperation->u2RTSThreshold == RADIO_MIN_RTS_THRESHOLD)

        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC, "RadioIfConfigUpdateReq (Mac):"
                         "Validation failed, (MAC: Invalid RTSThreshold)\r\n");
            return OSIX_FAILURE;
        }

        if (pRadioIfMacOperation->u1ShortRetry == 0)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC, "RadioIfConfigUpdateReq (Mac):"
                         "validation failed, (MAC : Invalid ShortRetry)\r\n");
            return OSIX_FAILURE;
        }

        if (pRadioIfMacOperation->u1LongRetry == 0)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC, "RadioIfConfigUpdateReq (Mac):"
                         "validation failed, (MAC : Invalid LongRetry )\r\n");
            return OSIX_FAILURE;
        }

        if (pRadioIfMacOperation->u2FragmentThreshold <
            RADIO_MIN_FRAGMENT_THRESHOLD ||
            pRadioIfMacOperation->u2FragmentThreshold >
            RADIO_MAX_FRAGMENT_THRESHOLD)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC, "RadioIfConfigUpdateReq (Mac):"
                         "validation failed, (MAC : Invalid FragmentThreshold)\r\n");
            return OSIX_FAILURE;
        }

        if (pRadioIfMacOperation->u4TxMsduLifetime < RADIO_MIN_TX_MSDU_LIFETIME
            || pRadioIfMacOperation->u4TxMsduLifetime >
            RADIO_MAX_TX_MSDU_LIFETIME)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC, "RadioIfConfigUpdateReq (Mac): "
                         "validation failed, (MAC : Invalid TxMsduLifetime)\r\n");
            return OSIX_FAILURE;
        }

        if (pRadioIfMacOperation->u4RxMsduLifetime < RADIO_MIN_RX_MSDU_LIFETIME
            || pRadioIfMacOperation->u4RxMsduLifetime >
            RADIO_MAX_RX_MSDU_LIFETIME)

        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC, "RadioIfConfigUpdateReq (Mac): "
                         "validation failed, (MAC : Invalid RxMsduLifetime)\r\n");
            return OSIX_FAILURE;
        }
    }
    if (pRadioIfTxPower->isPresent == OSIX_TRUE)
    {
        if (pRadioIfTxPower->u2MessageType !=
            RADIO_CONF_UPDATE_TX_POWER_MSG_TYPE)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC, "RadioIfConfigUpdateReq(TxPower):"
                         "validation failed,  (Invalid Message Type)\r\n");
            return OSIX_FAILURE;
        }

        if (pRadioIfTxPower->u2MessageLength !=
            RADIO_CONF_UPDATE_TX_POWER_LENGTH)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigUpdateReq (TxPower):"
                         "validation failed, return failure(Invalid Length)\r\n");
            return OSIX_FAILURE;
        }

        if (pRadioIfTxPower->u1RadioId < RADIO_MIN_COUNT ||
            pRadioIfTxPower->u1RadioId > RADIO_MAX_COUNT)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigUpdateReq (TxPower):"
                         "validation failed, return failure(Invalid Radio Id)\r\n");
            return OSIX_FAILURE;
        }
    }
    if (pRadioIfQos->isPresent == OSIX_TRUE)
    {
        if (pRadioIfQos->u2MessageType != RADIO_QOS_MSG_TYPE)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC, "RadioIfConfigUpdateReq(QOS):"
                         "validation failed, (Invalid Message Type)\r\n");
            return OSIX_FAILURE;
        }

        if (pRadioIfQos->u2MessageLength != RADIO_QOS_MSG_LEN)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC, "RadioIfConfigUpdateReq (QOS):"
                         "validation failed, return failure(Invalid Length)\r\n");
            return OSIX_FAILURE;
        }

        if (pRadioIfQos->u1RadioId < RADIO_MIN_COUNT
            || pRadioIfQos->u1RadioId > RADIO_MAX_COUNT)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC, "RadioIfConfigUpdateReq (QOS):"
                         "validation failed, return failure(Invalid Radio Id)\r\n");
            return OSIX_FAILURE;
        }
        for (u1Index = 0; u1Index < RADIO_QOS_PROFILE; u1Index++)
        {
            if (pRadioIfQos->u2CwMin[u1Index] > RADIO_QOS_CWMIN_MAX_VAL)
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC, "RadioIfConfigUpdateReq(QOS):"
                             "validation failed, (Invalid u2CwMin)\r\n");
                return OSIX_FAILURE;
            }
            if (pRadioIfQos->u1Aifsn[u1Index] > RADIO_QOS_AIFSN_MAX_VAL)
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC, "RadioIfConfigUpdateReq(QOS):"
                             "validation failed, (Invalid u1Aifsn)\r\n");
                return OSIX_FAILURE;
            }
            if (pRadioIfQos->u1Prio[u1Index] > RADIO_QOS_PRIO_MAX)
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC, "RadioIfConfigUpdateReq(QOS):"
                             "validation failed, (Invalid u1Prio)\r\n");
                return OSIX_FAILURE;
            }

            if (pRadioIfQos->u1Dscp[u1Index] > RADIO_DSCP_TAG_MAX)
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC, "RadioIfConfigUpdateReq(QOS):"
                             "validation failed, (Invalid u1Dscp)\r\n");
                return OSIX_FAILURE;
            }
        }
    }
    if (pRadioIfInfo->isPresent == OSIX_TRUE)
    {
        if (pRadioIfInfo->u2MessageType != RADIO_INFO_MSG_TYPE)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigUpdateReq: Invalid message type received "
                         "in radio info message \r\n");
            return OSIX_FAILURE;
        }

        if (pRadioIfInfo->u2MessageLength != RADIO_INFO_MSG_LEN)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigUpdateReq: Invalid length received in radio "
                         "info message\r\n");
            return OSIX_FAILURE;
        }

        if (pRadioIfInfo->u1RadioId < RADIO_MIN_COUNT ||
            pRadioIfInfo->u1RadioId > RADIO_MAX_COUNT)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigUpdateReq: Invalid radio id received in "
                         "radio info message\r\n");
            return OSIX_FAILURE;
        }
    }
    if (pRadioIfMultiDomainCap->isPresent == OSIX_TRUE)
    {
        if (pRadioIfMultiDomainCap->u2MessageType !=
            RADIO_MULTI_DOMAIN_MSG_TYPE)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigUpdateReq: validation failed, return "
                         "failure (Invalid Message Type)\r\n");
            return OSIX_FAILURE;
        }

        if (pRadioIfMultiDomainCap->u2MessageLength !=
            RADIO_MULTI_DOMAIN_MSG_LEN)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigUpdateReq : validation failed, return "
                         "failure(Invalid Length)\r\n");
            return OSIX_FAILURE;
        }

        if (pRadioIfMultiDomainCap->u1RadioId < RADIO_MIN_COUNT
            || pRadioIfMultiDomainCap->u1RadioId > RADIO_MAX_COUNT)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigUpdateReq: validation failed, return "
                         "failure(Invalid Radio Id)\r\n");
            return OSIX_FAILURE;
        }
    }
    if (pRadioIfConfig->isPresent == OSIX_TRUE)
    {
        if (pRadioIfConfig->u2MessageType != RADIO_CONFIG_MSG_TYPE)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigUpdateReq (RadioIfConfig) :validation "
                         "failed, return failure (Invalid Message Type)\r\n");
            return OSIX_FAILURE;
        }

        if (pRadioIfConfig->u2MessageLength != RADIO_CONFIG_MSG_LEN)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigUpdateReq (RadioIfConfig): validation "
                         "failed, return failure(Invalid Length)\r\n");
            return OSIX_FAILURE;
        }
        if (pRadioIfConfig->u1RadioId < RADIO_MIN_COUNT ||
            pRadioIfConfig->u1RadioId > RADIO_MAX_COUNT)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigUpdateReq (RadioIfConfig): validation "
                         "failed, return failure(Invalid Radio Id)\r\n");
            return OSIX_FAILURE;
        }
    }

    if (pRadioIfDot11nParam->isPresent == OSIX_TRUE)
    {
        if (pRadioIfDot11nParam->u2MessageType != RADIO_DOT11N_MSG_TYPE)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigUpdateReq (RadioIfDot11nParam): validation "
                         "failed, return failure (Invalid Message Type)\r\n");
            return OSIX_FAILURE;
        }

        if (pRadioIfDot11nParam->u2MessageLength != RADIO_DOT11N_MSG_LEN)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigUpdateReq (RadioIfDot11nParam): validation "
                         "failed, return failure(Invalid Length)\r\n");
            return OSIX_FAILURE;
        }

        if (pRadioIfDot11nParam->u4VendorId != VENDOR_ID)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigUpdateReq (RadioIfDot11nParam):  Invalid "
                         "Vendor Id received in radio info message\r\n");
            return OSIX_FAILURE;
        }

        if (pRadioIfDot11nParam->u1RadioId < RADIO_MIN_COUNT ||
            pRadioIfDot11nParam->u1RadioId > RADIO_MAX_COUNT)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigUpdateReq (RadioIfDot11nParam):  Invalid "
                         "radio id received in radio info message\r\n");
            return OSIX_FAILURE;
        }
    }

    if (pRadioIfRateSet->isPresent == OSIX_TRUE)
    {
        if (pRadioIfRateSet->u2MessageType != RADIO_RATE_SET_MSG_TYPE)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigUpdateReq: validation failed, return "
                         "failure (Invalid Message Type)\r\n");
            return OSIX_FAILURE;
        }

        if (pRadioIfRateSet->u2MessageLength != RADIO_RATE_SET_MSG_LEN)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigUpdateReq: validation failed, return "
                         "failure(Invalid Length)\r\n");
            return OSIX_FAILURE;
        }

        if (pRadioIfRateSet->u1RadioId < RADIOIF_START_RADIOID ||
            pRadioIfRateSet->u1RadioId > RADIOIF_END_RADIOID)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigUpdateReq: validation failed, return "
                         "failure(Invalid Radio Id)\r\n");
            return OSIX_FAILURE;
        }
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfProcessConfigUpdateReq                        *
 *                                                                           *
 * Description        : Config Update Request process  from AC to WTP        *
 *                                                                           *
 * Input(s)           : pWssMsgStruct - Pointer to the input structure       *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfProcessConfigUpdateReq (tRadioIfMsgStruct * pWssMsgStruct)
{
    tCfaMsgStruct       CfaMsgStruct;
    tRadioIfGetDB       RadioIfGetDB;
    tWssWlanDB         *pwssWlanDBMsg = NULL;
    INT4                i4RadioIfIndex = 1;
    INT4                i4IssConfigSaveStatus = 0;
    UINT4               u4FsCapwapWtpRadioConfigUpdateCount = 0;
    UINT4               u4FsCapwapWtpRadioChannelChangeCount = 0;
    UINT4               u4RadioType = 0;
    UINT1               u1Index = 0;
    UINT1               u1RadarFound = 0;
    UINT1               u1ChanlSwitStatus = 2;    /*Deafault value for CSA Status is DISABLED */
    UINT1               u111hDfsStatus = 2;    /*Deafault value for DFS Status is DISABLED */
    UINT1               au1RadioName[RADIO_INTF_NAME_LEN];
    UINT1               u1Channelnumber = 0;
    UINT1               u1MultiDomainIndex = 0;
    UINT2               u2RxMcs = 0;
    UINT2               u2TxMcs = 0;
    UINT2               u2RxDataRate = 0;
    UINT2               u2TxDataRate = 0;
    UINT1               u1ChanWidthDb = 0;
    UINT1               u1ChanWidth = 0;
    UINT1               u1Sts = 0;
    MEMSET (au1RadioName, 0, RADIO_INTF_NAME_LEN);
#ifdef RFMGMT_WANTED
    tRfMgmtMsgStruct    RfMgmtMsgStruct;

    MEMSET (&RfMgmtMsgStruct, 0, sizeof (tRfMgmtMsgStruct));
#endif

    MEMSET (&CfaMsgStruct, 0, sizeof (tCfaMsgStruct));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    if (pWssMsgStruct == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfConfigUpdateReq: Input validation failed, "
                     "return failure\r\n");
        return OSIX_FAILURE;
    }

    RadioIfGetDB.RadioIfIsGetAllDB.bBssIdCount = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
        SYS_DEF_MAX_ENET_INTERFACES + SYS_DEF_MAX_RADIO_INTERFACES;

    if (nmhGetFirstIndexFsCapwapWtpRadioStatisticsTable (&i4RadioIfIndex)
        != SNMP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "nmhGetFirstIndexFsCapwapWtpRadioStatisticsTable, returns "
                     "failure \r\n");
    }

    if (nmhGetFsCapwapWtpRadioConfigUpdateCount (i4RadioIfIndex,
                                                 &u4FsCapwapWtpRadioConfigUpdateCount)
        != SNMP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "nmhGetFsCapwapWtpRadioConfigUpdateCount, returns failure\r\n");
    }
    u4FsCapwapWtpRadioConfigUpdateCount++;
    if (nmhSetFsCapwapWtpRadioConfigUpdateCount (i4RadioIfIndex,
                                                 u4FsCapwapWtpRadioConfigUpdateCount)
        != SNMP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "nmhSetFsCapwapWtpRadioResetCount, returns failure \r\n");
    }

    /* initiating config save only. the save will happen only if config-save
     * option is not "noSave". similarly the configuration will be restored only
     * if the restore-option is "startupConfig". giving the precedence to the
     * snmp/cli settings of these two mib varibales */
    if (nmhSetIssInitiateConfigSave (ISS_TRUE) != SNMP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "nmhSetIssInitiateConfigSave, returns failure \r\n");
    }
    do
    {
        nmhGetIssConfigSaveStatus (&i4IssConfigSaveStatus);
    }
    while (i4IssConfigSaveStatus == MIB_SAVE_IN_PROGRESS);

    if (pWssMsgStruct->unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfAdminStatus.
        isPresent == OSIX_TRUE)
    {
        CfaMsgStruct.unCfaMsg.CfaCreateIfIndex.u4IfIndex = (UINT4)
            (pWssMsgStruct->unRadioIfMsg.
             RadioIfConfigUpdateReq.RadioIfAdminStatus.u1RadioId +
             SYS_DEF_MAX_ENET_INTERFACES);

        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
            CfaMsgStruct.unCfaMsg.CfaCreateIfIndex.u4IfIndex;

        CfaMsgStruct.unCfaMsg.CfaCreateIfIndex.u1AdminStatus =
            pWssMsgStruct->unRadioIfMsg.
            RadioIfConfigUpdateReq.RadioIfAdminStatus.u1AdminStatus;

        if (CfaProcessWssIfMsg (CFA_WSS_ADMIN_STATUS_CHG_MSG, &CfaMsgStruct)
            != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "CreateInterface (RadioIfConfigUpdateReq): "
                         "CfaProcessWssIfMsg failed\r\n");
            return OSIX_FAILURE;
        }

#ifdef RFMGMT_WANTED
        if (pWssMsgStruct->unRadioIfMsg.
            RadioIfConfigUpdateReq.RadioIfAdminStatus.u1AdminStatus ==
            RADIOIF_ADMIN_STATUS_DISABLED)
        {
            RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtIntfConfigReq.u4RadioIfIndex =
                RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex;

            if (WssIfProcessRfMgmtMsg (RFMGMT_RADIO_DOWN_MSG,
                                       &RfMgmtMsgStruct) != OSIX_SUCCESS)
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                             "Radio down update to RF module returns failure \r\n");
            }

        }
#endif
        RadioIfGetDB.RadioIfGetAllDB.u1AdminStatus =
            pWssMsgStruct->unRadioIfMsg.
            RadioIfConfigUpdateReq.RadioIfAdminStatus.u1AdminStatus;
        MEMSET (au1RadioName, 0, RADIO_INTF_NAME_LEN);
#ifdef NPAPI_WANTED
        SPRINTF ((CHR1 *) au1RadioName, "%s%d", RADIO_INTF_NAME,
                 pWssMsgStruct->unRadioIfMsg.
                 RadioIfConfigUpdateReq.RadioIfAdminStatus.u1RadioId - 1);
#endif
        if (RadioIfSetAdminStatus (&RadioIfGetDB, (UINT1 *) &au1RadioName) !=
            OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "CreateInterface (RadioIfConfigUpdateReq): "
                         "CfaProcessWssIfMsg failed\r\n");
            return OSIX_FAILURE;
        }
    }
    if (pWssMsgStruct->unRadioIfMsg.RadioIfConfigUpdateReq.
        RadioIfAntenna.isPresent == OSIX_TRUE)
    {
        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4)
            (pWssMsgStruct->unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfAntenna.
             u1RadioId + SYS_DEF_MAX_ENET_INTERFACES);

        RadioIfGetDB.RadioIfGetAllDB.u1DiversitySupport =
            pWssMsgStruct->unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfAntenna.
            u1ReceiveDiversity;
        MEMSET (au1RadioName, 0, RADIO_INTF_NAME_LEN);
#ifdef NPAPI_WANTED
        SPRINTF ((CHR1 *) au1RadioName, "%s%d", RADIO_INTF_NAME,
                 pWssMsgStruct->unRadioIfMsg.
                 RadioIfConfigUpdateReq.RadioIfAntenna.u1RadioId - 1);
#endif
        if (RadioIfSetAntennaDiversity (&RadioIfGetDB, (UINT1 *) &au1RadioName)
            != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigUpdateReq:(Antenna Diversity) HW call to "
                         "set the values failed\r\n");
            return OSIX_FAILURE;
        }

        RadioIfGetDB.RadioIfGetAllDB.u1AntennaMode =
            pWssMsgStruct->unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfAntenna.
            u1AntennaMode;
        RadioIfGetDB.RadioIfIsGetAllDB.bAntennaMode = OSIX_TRUE;
    }

    if (pWssMsgStruct->unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfRateSet.
        isPresent != OSIX_FALSE)
    {
        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4)
            (pWssMsgStruct->unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfRateSet.
             u1RadioId + SYS_DEF_MAX_ENET_INTERFACES);

        MEMCPY (RadioIfGetDB.RadioIfGetAllDB.au1OperationalRate,
                pWssMsgStruct->unRadioIfMsg.
                RadioIfConfigUpdateReq.RadioIfRateSet.au1OperationalRate,
                RADIOIF_OPER_RATE);

        MEMSET (au1RadioName, 0, RADIO_INTF_NAME_LEN);
#ifdef NPAPI_WANTED
        SPRINTF ((CHR1 *) au1RadioName, "%s%d", RADIO_INTF_NAME,
                 pWssMsgStruct->unRadioIfMsg.
                 RadioIfConfigUpdateReq.RadioIfRateSet.u1RadioId - 1);
#endif

#ifdef BAND_SELECT_WANTED
        RadioIfGetDB.RadioIfIsGetAllDB.bBssIdCount = OSIX_TRUE;

        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                      (&RadioIfGetDB)) != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfProcessConfigUpdateReq : RBTREE get of "
                         "RadioIf DB failed for BssIdCount\r\n");
            return OSIX_FAILURE;
        }

        if (RadioIfGetDB.RadioIfGetAllDB.u1BssIdCount == RADIO_VALUE_0)
        {
            RadioIfGetDB.RadioIfGetAllDB.u1WlanId = RADIO_VALUE_1;
        }
        else
        {
            for (u1Index = RADIO_VALUE_1; u1Index <= RADIO_MAX_BSSID_SUPPORTED;
                 u1Index++)
            {
                RadioIfGetDB.RadioIfGetAllDB.u1WlanId = u1Index;
                if (WssIfProcessRadioIfDBMsg (WSS_GET_WLAN_BSS_IFINDEX_DB,
                                              (&RadioIfGetDB)) == OSIX_SUCCESS)
                {
                    if (RadioIfGetDB.RadioIfGetAllDB.u4BssIfIndex != 0)
                        break;
                }
                else
                    continue;
            }

        }
#endif

        if (RadioIfSetOperationalRate (&RadioIfGetDB, (UINT1 *) &au1RadioName)
            != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigUpdateReq:(Operational Rate) HW call to "
                         "set the values failed\r\n");
            return OSIX_FAILURE;
        }
    }

    RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
        UtlShMemAllocAntennaSelectionBuf ();

    if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC, "\r%% Memory allocation for Antenna"
                     " selection failed.\r\n");
        return OSIX_FAILURE;
    }
    if (pWssMsgStruct->unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfDSSSPhy.
        isPresent == OSIX_TRUE)
    {
        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4)
            (pWssMsgStruct->unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfDSSSPhy.
             u1RadioId + SYS_DEF_MAX_ENET_INTERFACES);

        RadioIfGetDB.RadioIfGetAllDB.u1CurrentChannel = (UINT1)
            (pWssMsgStruct->unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfDSSSPhy.
             i1CurrentChannel);

        RadioIfGetDB.RadioIfIsGetAllDB.bBssIdCount = OSIX_TRUE;

        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                      (&RadioIfGetDB)) != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfProcessConfigUpdateReq : RBTREE get of "
                         "RadioIf DB failed \r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            return OSIX_FAILURE;
        }

        if (RadioIfGetDB.RadioIfGetAllDB.u1BssIdCount == 0)
        {
            RadioIfGetDB.RadioIfGetAllDB.u1WlanId = 1;
        }
        else
        {
            for (u1Index = 1; u1Index <= RADIO_MAX_BSSID_SUPPORTED; u1Index++)
            {
                RadioIfGetDB.RadioIfGetAllDB.u1WlanId = u1Index;
                if (WssIfProcessRadioIfDBMsg (WSS_GET_WLAN_BSS_IFINDEX_DB,
                                              (&RadioIfGetDB)) == OSIX_SUCCESS)
                {
                    if (RadioIfGetDB.RadioIfGetAllDB.u4BssIfIndex != 0)
                        break;
                }
                else
                    continue;
            }

        }

        MEMSET (au1RadioName, 0, RADIO_INTF_NAME_LEN);
#ifdef NPAPI_WANTED
        SPRINTF ((CHR1 *) au1RadioName, "%s%d", RADIO_INTF_NAME,
                 pWssMsgStruct->unRadioIfMsg.
                 RadioIfConfigUpdateReq.RadioIfDSSSPhy.u1RadioId - 1);

        RadioIfGetDB.RadioIfGetAllDB.u1RadioId =
            pWssMsgStruct->unRadioIfMsg.
            RadioIfConfigUpdateReq.RadioIfDSSSPhy.u1RadioId;
#endif
        if (RadioIfSetDSCCurrentChannel (&RadioIfGetDB, (UINT1 *) &au1RadioName)
            != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigUpdateReq:(Current Channel) HW call "
                         "to set the values failed\r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            return OSIX_FAILURE;
        }

        /*Code changes for updating Max tx power according to the channel assigned */

        RadioIfGetDB.RadioIfIsGetAllDB.bMultiDomainInfo = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bMaxTxPowerLevel = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bCurrentTxPowerLevel = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bCurrentTxPower = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bCurrentChannel = OSIX_FALSE;

        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                      &RadioIfGetDB) == OSIX_SUCCESS)
        {
            RadioIfGetDB.RadioIfIsGetAllDB.bMaxTxPowerLevel = OSIX_FALSE;
            RadioIfGetDB.RadioIfIsGetAllDB.bCurrentTxPowerLevel = OSIX_FALSE;
            RadioIfGetDB.RadioIfIsGetAllDB.bCurrentTxPower = OSIX_FALSE;
            u1Channelnumber = RadioIfGetDB.RadioIfGetAllDB.u1CurrentChannel;
            for (u1MultiDomainIndex = 0;
                 u1MultiDomainIndex < MAX_MULTIDOMAIN_INFO_ELEMENT;
                 u1MultiDomainIndex++)
            {
                if ((RadioIfGetDB.RadioIfGetAllDB.
                     MultiDomainInfo[u1MultiDomainIndex].u2FirstChannelNo <=
                     u1Channelnumber))
                {
                    if (((UINT1) (u1MultiDomainIndex + 1)) <
                        MAX_MULTIDOMAIN_INFO_ELEMENT)
                    {
                        if ((RadioIfGetDB.RadioIfGetAllDB.
                             MultiDomainInfo[u1MultiDomainIndex +
                                             1].u2FirstChannelNo >
                             u1Channelnumber)
                            || (RadioIfGetDB.RadioIfGetAllDB.
                                MultiDomainInfo[u1MultiDomainIndex +
                                                1].u2FirstChannelNo == 0))
                        {
                            RadioIfGetDB.RadioIfGetAllDB.u2MaxTxPowerLevel =
                                RadioIfGetDB.RadioIfGetAllDB.
                                MultiDomainInfo[u1MultiDomainIndex].
                                u2MaxTxPowerLevel;
                            RadioIfGetDB.RadioIfIsGetAllDB.bMaxTxPowerLevel =
                                OSIX_TRUE;
                            break;
                        }
                    }
                }
            }

            if (RadioIfGetDB.RadioIfGetAllDB.u2CurrentTxPowerLevel == 0)
            {
                RadioIfGetDB.RadioIfGetAllDB.u2CurrentTxPowerLevel = 1;
                RadioIfGetDB.RadioIfIsGetAllDB.bCurrentTxPowerLevel = OSIX_TRUE;
            }
            if (RadioIfGetDB.RadioIfGetAllDB.i2CurrentTxPower == 0)
            {
                RadioIfGetDB.RadioIfGetAllDB.i2CurrentTxPower =
                    RadioIfGetDB.RadioIfGetAllDB.u2MaxTxPowerLevel;
                RadioIfGetDB.RadioIfIsGetAllDB.bCurrentTxPower = OSIX_TRUE;
            }
        }
        RadioIfGetDB.RadioIfIsGetAllDB.bMultiDomainInfo = OSIX_FALSE;
        RadioIfGetDB.RadioIfIsGetAllDB.bCurrentChannel = OSIX_TRUE;

        if (nmhGetFsCapwapWtpRadioChannelChangeCount (i4RadioIfIndex,
                                                      &u4FsCapwapWtpRadioChannelChangeCount)
            != SNMP_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "nmhGetFsCapwapWtpRadioConfigUpdateCount, returns failure\r\n");
        }

        u4FsCapwapWtpRadioChannelChangeCount++;

        if (nmhSetFsCapwapWtpRadioChannelChangeCount (i4RadioIfIndex,
                                                      u4FsCapwapWtpRadioChannelChangeCount)
            != SNMP_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "nmhSetFsCapwapWtpRadioChannelChangeCount, returns failure \r\n");
        }
#ifdef RFMGMT_WANTED
        RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtIntfConfigReq.u4RadioIfIndex =
            RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex;
        RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtIntfConfigReq.u2Channel =
            (UINT2) RadioIfGetDB.RadioIfGetAllDB.u1CurrentChannel;

        if (WssIfProcessRfMgmtMsg (RFMGMT_CHANNEL_UPDATE_MSG,
                                   &RfMgmtMsgStruct) != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "Update Channel to RF modul returns failure \r\n");
        }
#endif
    }
    if (pWssMsgStruct->unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfOFDMPhy.
        isPresent == OSIX_TRUE)
    {
        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4)
            (pWssMsgStruct->unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfOFDMPhy.
             u1RadioId + SYS_DEF_MAX_ENET_INTERFACES);

        RadioIfGetDB.RadioIfGetAllDB.u1CurrentChannel =
            pWssMsgStruct->unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfOFDMPhy.
            u1CurrentChannel;

        RadioIfGetDB.RadioIfIsGetAllDB.bBssIdCount = OSIX_TRUE;

        RadioIfGetDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bRadarFound = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bChanlSwitStatus = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.b11hDfsStatus = OSIX_TRUE;

        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                      (&RadioIfGetDB)) != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfProcessConfigUpdateReq: RBTREE get of "
                         "RadioIf DB failed \r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            return OSIX_FAILURE;
        }
        RadioIfGetDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_FALSE;

        if (RadioIfGetDB.RadioIfGetAllDB.u1BssIdCount == 0)
        {
            RadioIfGetDB.RadioIfGetAllDB.u1WlanId = 1;
        }
        else
        {
            for (u1Index = 1; u1Index <= RADIO_MAX_BSSID_SUPPORTED; u1Index++)
            {
                RadioIfGetDB.RadioIfGetAllDB.u1WlanId = u1Index;
                if (WssIfProcessRadioIfDBMsg (WSS_GET_WLAN_BSS_IFINDEX_DB,
                                              (&RadioIfGetDB)) == OSIX_SUCCESS)
                {
                    if (RadioIfGetDB.RadioIfGetAllDB.u4BssIfIndex != 0)
                        break;
                }
                else
                    continue;
            }
        }
        /*multi radio */
        MEMSET (au1RadioName, 0, RADIO_INTF_NAME_LEN);
#ifdef NPAPI_WANTED
        SPRINTF ((CHR1 *) au1RadioName, "%s%d", RADIO_INTF_NAME,
                 pWssMsgStruct->unRadioIfMsg.
                 RadioIfConfigUpdateReq.RadioIfOFDMPhy.u1RadioId - 1);

        RadioIfGetDB.RadioIfGetAllDB.u1RadioId =
            pWssMsgStruct->unRadioIfMsg.
            RadioIfConfigUpdateReq.RadioIfOFDMPhy.u1RadioId;
#endif

        u1RadarFound = RadioIfGetDB.RadioIfGetAllDB.u1RadarFound;
        u1ChanlSwitStatus = RadioIfGetDB.RadioIfGetAllDB.u1ChanlSwitStatus;
        u111hDfsStatus = RadioIfGetDB.RadioIfGetAllDB.u111hDfsStatus;
        u4RadioType = RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType;
        if ((u111hDfsStatus == 1 && u1ChanlSwitStatus == 1 && u1RadarFound) &&
            ((u4RadioType == RADIO_TYPEA) || (u4RadioType == RADIO_TYPEAN)
             || (u4RadioType == RADIO_TYPEAC)))
        {
            WssStaConstructChanlSwitchAnnounce (pWssMsgStruct->unRadioIfMsg.
                                                RadioIfConfigUpdateReq.
                                                RadioIfOFDMPhy.u1RadioId,
                                                &RadioIfGetDB);
            if (RadioIfAddChanlSwitIE (&RadioIfGetDB, (UINT1 *) &au1RadioName)
                != OSIX_SUCCESS)
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                             "RadioIfConfigUpdateReq:(Channel Switch IE) HW call to add "
                             "Channel Switch IE failed\r\n");
                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                 RadioIfGetAllDB.
                                                 pu1AntennaSelection);
                return OSIX_FAILURE;
            }
            RfMgmtWtpTmrStart (RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex,
                               RFMGMT_CHAN_SWIT_ANNOUNCE_TMR,
                               RFMGMT_CHANL_SWIT_AGEOUT_PERIOD);

            /* Set the RadarFound flag to zero and write it to radio DB */
            RadioIfGetDB.RadioIfIsGetAllDB.bRadarFound = OSIX_TRUE;
            RadioIfGetDB.RadioIfGetAllDB.u1RadarFound = OSIX_FALSE;
            if (WssIfProcessRadioIfDBMsg (WSS_SET_RADIO_IF_DB,
                                          (&RadioIfGetDB)) != OSIX_SUCCESS)
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                             "RadioIfProcessConfigUpdateReq: RBTREE get of "
                             "RadioIf DB failed \r\n");
                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                 RadioIfGetAllDB.
                                                 pu1AntennaSelection);
                return OSIX_FAILURE;
            }
        }
        else
        {
            RadioIfGetDB.RadioIfGetAllDB.u1RadioId =
                pWssMsgStruct->unRadioIfMsg.RadioIfConfigUpdateReq.
                RadioIfOFDMPhy.u1RadioId;
            if (RadioIfSetOFDMCurrentChannel
                (&RadioIfGetDB, (UINT1 *) &au1RadioName) != OSIX_SUCCESS)
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                             "RadioIfConfigUpdateReq:(CurrentChannel) HW call to set "
                             "the values failed\r\n");
                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                 RadioIfGetAllDB.
                                                 pu1AntennaSelection);
                return OSIX_FAILURE;
            }

            /*Code changes for updating Max tx power according to the channel assigned */

            RadioIfGetDB.RadioIfIsGetAllDB.bMultiDomainInfo = OSIX_TRUE;
            RadioIfGetDB.RadioIfIsGetAllDB.bMaxTxPowerLevel = OSIX_TRUE;
            RadioIfGetDB.RadioIfIsGetAllDB.bCurrentTxPowerLevel = OSIX_TRUE;
            RadioIfGetDB.RadioIfIsGetAllDB.bCurrentTxPower = OSIX_TRUE;
            RadioIfGetDB.RadioIfIsGetAllDB.bCurrentChannel = OSIX_FALSE;

            if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                          &RadioIfGetDB) == OSIX_SUCCESS)
            {
                RadioIfGetDB.RadioIfIsGetAllDB.bMaxTxPowerLevel = OSIX_FALSE;
                RadioIfGetDB.RadioIfIsGetAllDB.bCurrentTxPowerLevel =
                    OSIX_FALSE;
                RadioIfGetDB.RadioIfIsGetAllDB.bCurrentTxPower = OSIX_FALSE;

                u1Channelnumber = RadioIfGetDB.RadioIfGetAllDB.u1CurrentChannel;
                for (u1MultiDomainIndex = 0;
                     u1MultiDomainIndex < MAX_MULTIDOMAIN_INFO_ELEMENT;
                     u1MultiDomainIndex++)
                {
                    if ((RadioIfGetDB.RadioIfGetAllDB.
                         MultiDomainInfo[u1MultiDomainIndex].u2FirstChannelNo <=
                         u1Channelnumber))
                    {
                        if (((UINT1) (u1MultiDomainIndex + 1)) <
                            MAX_MULTIDOMAIN_INFO_ELEMENT)
                        {
                            if ((RadioIfGetDB.RadioIfGetAllDB.
                                 MultiDomainInfo[u1MultiDomainIndex +
                                                 1].u2FirstChannelNo >
                                 u1Channelnumber)
                                || (RadioIfGetDB.RadioIfGetAllDB.
                                    MultiDomainInfo[u1MultiDomainIndex +
                                                    1].u2FirstChannelNo == 0))
                            {
                                RadioIfGetDB.RadioIfGetAllDB.u2MaxTxPowerLevel =
                                    RadioIfGetDB.RadioIfGetAllDB.
                                    MultiDomainInfo[u1MultiDomainIndex].
                                    u2MaxTxPowerLevel;
                                RadioIfGetDB.RadioIfIsGetAllDB.
                                    bMaxTxPowerLevel = OSIX_TRUE;
                                break;
                            }
                        }
                    }
                }

                if (RadioIfGetDB.RadioIfGetAllDB.u2CurrentTxPowerLevel == 0)
                {
                    RadioIfGetDB.RadioIfGetAllDB.u2CurrentTxPowerLevel = 1;
                    RadioIfGetDB.RadioIfIsGetAllDB.bCurrentTxPowerLevel =
                        OSIX_TRUE;
                }
                if (RadioIfGetDB.RadioIfGetAllDB.i2CurrentTxPower == 0)
                {
                    RadioIfGetDB.RadioIfGetAllDB.i2CurrentTxPower =
                        RadioIfGetDB.RadioIfGetAllDB.u2MaxTxPowerLevel;
                    RadioIfGetDB.RadioIfIsGetAllDB.bCurrentTxPower = OSIX_TRUE;
                }
            }
            RadioIfGetDB.RadioIfIsGetAllDB.bMultiDomainInfo = OSIX_FALSE;
            RadioIfGetDB.RadioIfIsGetAllDB.bCurrentChannel = OSIX_TRUE;

            if (nmhGetFsCapwapWtpRadioChannelChangeCount (i4RadioIfIndex,
                                                          &u4FsCapwapWtpRadioChannelChangeCount)
                != SNMP_SUCCESS)
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                             "nmhGetFsCapwapWtpRadioConfigUpdateCount, returns failure\r\n");
            }

            u4FsCapwapWtpRadioChannelChangeCount++;

            if (nmhSetFsCapwapWtpRadioChannelChangeCount (i4RadioIfIndex,
                                                          u4FsCapwapWtpRadioChannelChangeCount)
                != SNMP_SUCCESS)
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                             "nmhSetFsCapwapWtpRadioChannelChangeCount, returns failure \r\n");
            }
#ifdef RFMGMT_WANTED
            RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtIntfConfigReq.u4RadioIfIndex =
                RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex;
            RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtIntfConfigReq.u2Channel =
                (UINT2) RadioIfGetDB.RadioIfGetAllDB.u1CurrentChannel;

            if (WssIfProcessRfMgmtMsg (RFMGMT_CHANNEL_UPDATE_MSG,
                                       &RfMgmtMsgStruct) != OSIX_SUCCESS)
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                             "Update Channel to RF modul returns failure \r\n");
            }
#endif
        }
    }
    if (pWssMsgStruct->unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfMacOperation.
        isPresent == OSIX_TRUE)
    {
        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4)
            (pWssMsgStruct->unRadioIfMsg.
             RadioIfConfigUpdateReq.RadioIfMacOperation.u1RadioId +
             SYS_DEF_MAX_ENET_INTERFACES);

        RadioIfGetDB.RadioIfIsGetAllDB.bBssIdCount = OSIX_TRUE;

        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                      &RadioIfGetDB) != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfProcessConfigUpdateReq : RBTREE get of RadioIf "
                         "DB failed \r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            return OSIX_FAILURE;
        }

        if (RadioIfGetDB.RadioIfGetAllDB.u1BssIdCount == 0)
        {
            RadioIfGetDB.RadioIfGetAllDB.u1WlanId = 1;
        }
        else
        {
            for (u1Index = 1; u1Index <= RADIO_MAX_BSSID_SUPPORTED; u1Index++)
            {
                RadioIfGetDB.RadioIfGetAllDB.u1WlanId = u1Index;
                if (WssIfProcessRadioIfDBMsg (WSS_GET_WLAN_BSS_IFINDEX_DB,
                                              &RadioIfGetDB) == OSIX_SUCCESS)
                {
                    if (RadioIfGetDB.RadioIfGetAllDB.u4BssIfIndex != 0)
                        break;
                }
                else
                    continue;
            }
        }

        RadioIfGetDB.RadioIfGetAllDB.u1ShortRetry =
            pWssMsgStruct->unRadioIfMsg.
            RadioIfConfigUpdateReq.RadioIfMacOperation.u1ShortRetry;
        RadioIfGetDB.RadioIfIsGetAllDB.bShortRetry = OSIX_TRUE;

        RadioIfGetDB.RadioIfGetAllDB.u1LongRetry =
            pWssMsgStruct->unRadioIfMsg.
            RadioIfConfigUpdateReq.RadioIfMacOperation.u1LongRetry;
        RadioIfGetDB.RadioIfIsGetAllDB.bLongRetry = OSIX_TRUE;

        RadioIfGetDB.RadioIfGetAllDB.u4TxMsduLifetime =
            pWssMsgStruct->unRadioIfMsg.
            RadioIfConfigUpdateReq.RadioIfMacOperation.u4TxMsduLifetime;
        RadioIfGetDB.RadioIfIsGetAllDB.bTxMsduLifetime = OSIX_TRUE;

        RadioIfGetDB.RadioIfGetAllDB.u4RxMsduLifetime =
            pWssMsgStruct->unRadioIfMsg.
            RadioIfConfigUpdateReq.RadioIfMacOperation.u4RxMsduLifetime;
        RadioIfGetDB.RadioIfIsGetAllDB.bRxMsduLifetime = OSIX_TRUE;

        RadioIfGetDB.RadioIfGetAllDB.u2RTSThreshold =
            pWssMsgStruct->unRadioIfMsg.
            RadioIfConfigUpdateReq.RadioIfMacOperation.u2RTSThreshold;
        /*multi radio */
        MEMSET (au1RadioName, 0, RADIO_INTF_NAME_LEN);
#ifdef NPAPI_WANTED
        SPRINTF ((CHR1 *) au1RadioName, "%s%d", RADIO_INTF_NAME,
                 pWssMsgStruct->unRadioIfMsg.
                 RadioIfConfigUpdateReq.RadioIfMacOperation.u1RadioId - 1);
#endif

        if (RadioIfSetRTSThreshold (&RadioIfGetDB,
                                    (UINT1 *) &au1RadioName) != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigUpdateReq:(RTSThreshold) HW call to set "
                         "the values failed\r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            return OSIX_FAILURE;
        }

        RadioIfGetDB.RadioIfGetAllDB.u2FragmentationThreshold =
            pWssMsgStruct->unRadioIfMsg.
            RadioIfConfigUpdateReq.RadioIfMacOperation.u2FragmentThreshold;

        if (RadioIfSetFragmentationThreshold (&RadioIfGetDB,
                                              (UINT1 *) au1RadioName) !=
            OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigUpdateReq:(FragmentationThreshold) HW call "
                         "to set the values failed\r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            return OSIX_FAILURE;
        }
    }

    if (pWssMsgStruct->unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfTxPower.
        isPresent == OSIX_TRUE)
    {
        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4)
            (pWssMsgStruct->unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfTxPower.
             u1RadioId + SYS_DEF_MAX_ENET_INTERFACES);

        RadioIfGetDB.RadioIfGetAllDB.u1RadioId =
            pWssMsgStruct->unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfTxPower.
            u1RadioId;

        RadioIfGetDB.RadioIfIsGetAllDB.bBssIdCount = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bCountryString = OSIX_TRUE;

        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                      (&RadioIfGetDB)) != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfProcessConfigUpdateReq : RBTREE get of RadioIf DB "
                         "failed \r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            return OSIX_FAILURE;
        }

        if (RadioIfGetDB.RadioIfGetAllDB.u1BssIdCount == 0)
        {
            RadioIfGetDB.RadioIfGetAllDB.u1WlanId = 1;
        }
        else
        {
            for (u1Index = 1; u1Index <= 8; u1Index++)
            {
                RadioIfGetDB.RadioIfGetAllDB.u1WlanId = u1Index;
                if (WssIfProcessRadioIfDBMsg (WSS_GET_WLAN_BSS_IFINDEX_DB,
                                              (&RadioIfGetDB)) == OSIX_SUCCESS)
                {
                    if (RadioIfGetDB.RadioIfGetAllDB.u4BssIfIndex != 0)
                        break;
                }
                else
                    continue;
            }
        }

        RadioIfGetDB.RadioIfGetAllDB.i2CurrentTxPower =
            (UINT2) pWssMsgStruct->unRadioIfMsg.RadioIfConfigUpdateReq.
            RadioIfTxPower.u2TxPowerLevel;

        /*multi radio */
        MEMSET (au1RadioName, 0, RADIO_INTF_NAME_LEN);
#ifdef NPAPI_WANTED
        SPRINTF ((CHR1 *) au1RadioName, "%s%d", RADIO_INTF_NAME,
                 pWssMsgStruct->unRadioIfMsg.
                 RadioIfConfigUpdateReq.RadioIfTxPower.u1RadioId - 1);
#endif

        if (RadioIfSetTxPowerLevel (&RadioIfGetDB,
                                    (UINT1 *) &au1RadioName) != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigUpdateReq:(TxPowerLevel) HW call to "
                         "set the values failed\r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            return OSIX_FAILURE;
        }
#ifdef RFMGMT_WANTED
        RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtIntfConfigReq.u4RadioIfIndex =
            RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex;
        RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtIntfConfigReq.u2TxPower =
            pWssMsgStruct->unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfTxPower.
            u2TxPowerLevel;

        if (WssIfProcessRfMgmtMsg (RFMGMT_TXPOWER_UPDATE_MSG,
                                   &RfMgmtMsgStruct) != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "Update Rx Power to RF module returns failure \r\n");
        }

#endif
    }
    if (pWssMsgStruct->unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfQos.
        isPresent == OSIX_TRUE)
    {
        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4)
            (pWssMsgStruct->unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfQos.
             u1RadioId + SYS_DEF_MAX_ENET_INTERFACES);

        RadioIfGetDB.RadioIfIsGetAllDB.bBssIdCount = OSIX_TRUE;
        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                      (&RadioIfGetDB)) != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfProcessConfigUpdateReq : RBTREE get of "
                         "RadioIf DB failed \r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            return OSIX_FAILURE;
        }
        if (RadioIfGetDB.RadioIfGetAllDB.u1BssIdCount == 0)
        {
            RadioIfGetDB.RadioIfGetAllDB.u1WlanId = 1;
        }
        else
        {
            for (u1Index = 1; u1Index <= RADIO_MAX_BSSID_SUPPORTED; u1Index++)
            {
                RadioIfGetDB.RadioIfGetAllDB.u1WlanId = u1Index;
                if (WssIfProcessRadioIfDBMsg (WSS_GET_WLAN_BSS_IFINDEX_DB,
                                              (&RadioIfGetDB)) == OSIX_SUCCESS)
                {
                    if (RadioIfGetDB.RadioIfGetAllDB.u4BssIfIndex != 0)
                        break;
                }
                else
                    continue;
            }
        }

        MEMCPY (RadioIfGetDB.RadioIfGetAllDB.au2CwMin,
                pWssMsgStruct->unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfQos.
                u2CwMin, (RADIO_QOS_PROFILE * sizeof (UINT2)));
        RadioIfGetDB.RadioIfIsGetAllDB.bCwMin = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bCwMax = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bAifsn = OSIX_TRUE;
        MEMSET (au1RadioName, 0, RADIO_INTF_NAME_LEN);
#ifdef NPAPI_WANTED
        SPRINTF ((CHR1 *) au1RadioName, "%s%d", RADIO_INTF_NAME,
                 pWssMsgStruct->unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfQos.
                 u1RadioId - 1);
#endif
        RadioIfGetDB.RadioIfGetAllDB.u1RadioId =
            pWssMsgStruct->unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfQos.
            u1RadioId;
        for (u1Index = 0; u1Index < RADIO_QOS_PROFILE; u1Index++)
        {
            RadioIfGetDB.RadioIfGetAllDB.au1QueueDepth[u1Index] =
                pWssMsgStruct->unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfQos.
                u1QueueDepth[u1Index];
            RadioIfGetDB.RadioIfGetAllDB.au2CwMin[u1Index] =
                pWssMsgStruct->unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfQos.
                u2CwMin[u1Index];
            /*multi radio */
            if (RadioIfSetQosCwMin (&RadioIfGetDB, (UINT1 *) &au1RadioName) !=
                OSIX_SUCCESS)
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                             "RadioIfConfigUpdateReq(QOS): CwMin - HW call to "
                             "set the values failed\r\n");
                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                 RadioIfGetAllDB.
                                                 pu1AntennaSelection);
                return OSIX_FAILURE;
            }

            RadioIfGetDB.RadioIfGetAllDB.au2CwMax[u1Index] =
                pWssMsgStruct->unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfQos.
                u2CwMax[u1Index];
            if (RadioIfSetQosCwMax (&RadioIfGetDB, (UINT1 *) &au1RadioName) !=
                OSIX_SUCCESS)
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                             "RadioIfConfigUpdateReq(QOS): CwMax - HW call to "
                             "set the values failed\r\n");
                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                 RadioIfGetAllDB.
                                                 pu1AntennaSelection);
                return OSIX_FAILURE;
            }

            RadioIfGetDB.RadioIfGetAllDB.au1Aifsn[u1Index] =
                pWssMsgStruct->unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfQos.
                u1Aifsn[u1Index];
            if (RadioIfSetQosAifsn (&RadioIfGetDB, (UINT1 *) &au1RadioName) !=
                OSIX_SUCCESS)
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                             "RadioIfConfigUpdateReq(QOS): Aifsn - HW call to set "
                             "the values failed\r\n");
                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                 RadioIfGetAllDB.
                                                 pu1AntennaSelection);
                return OSIX_FAILURE;
            }

            RadioIfGetDB.RadioIfGetAllDB.au1QosPrio[u1Index] =
                pWssMsgStruct->unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfQos.
                u1Prio[u1Index];
            RadioIfGetDB.RadioIfGetAllDB.au1Dscp[u1Index] =
                pWssMsgStruct->unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfQos.
                u1Dscp[u1Index];
            RadioIfGetDB.RadioIfGetAllDB.u1QosIndex = u1Index;
        }

        /* For Tagging Policy */
        RadioIfGetDB.RadioIfGetAllDB.u1TaggingPolicy =
            pWssMsgStruct->unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfQos.
            u1TaggingPolicy;

        RadioIfGetDB.RadioIfIsGetAllDB.bTaggingPolicy = OSIX_TRUE;

        /*NPAPI Call to be added */
        for (u1Index = 0; u1Index < RADIO_QOS_PROFILE; u1Index++)
        {
            RadioIfGetDB.RadioIfGetAllDB.u1QosIndex = u1Index;
            RadioIfGetDB.RadioIfIsGetAllDB.bQosIndex = OSIX_TRUE;
            if (WssIfProcessRadioIfDBMsg (WSS_SET_RADIO_QOS_CONFIG_DB_WTP,
                                          &RadioIfGetDB) != OSIX_SUCCESS)
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                             "RadioDb Updation Failed \r\n");
                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                 RadioIfGetAllDB.
                                                 pu1AntennaSelection);
                return OSIX_FAILURE;
            }
        }
    }

    if (pWssMsgStruct->unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfInfo.
        isPresent == OSIX_TRUE)
    {
        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4)
            (pWssMsgStruct->unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfInfo.
             u1RadioId + SYS_DEF_MAX_ENET_INTERFACES);

        RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType =
            pWssMsgStruct->unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfInfo.
            u4RadioType;

    }
    if (pWssMsgStruct->unRadioIfMsg.
        RadioIfConfigUpdateReq.RadioIfMultiDomainCap.isPresent == OSIX_TRUE)
    {
        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4)
            (pWssMsgStruct->unRadioIfMsg.
             RadioIfConfigUpdateReq.RadioIfMultiDomainCap.u1RadioId +
             SYS_DEF_MAX_ENET_INTERFACES);

        RadioIfGetDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_TRUE;

        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                      &RadioIfGetDB) != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "Radio Get Failed (Config Update - MultiDomainCapab)\r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            return OSIX_FAILURE;
        }

        RadioIfGetDB.RadioIfGetAllDB.u2MaxTxPowerLevel =
            pWssMsgStruct->unRadioIfMsg.
            RadioIfConfigUpdateReq.RadioIfMultiDomainCap.u2MaxTxPowerLevel;

        /*multi radio */
        MEMSET (au1RadioName, 0, RADIO_INTF_NAME_LEN);
#ifdef NPAPI_WANTED
        SPRINTF ((CHR1 *) au1RadioName, "%s%d", RADIO_INTF_NAME,
                 pWssMsgStruct->unRadioIfMsg.
                 RadioIfConfigUpdateReq.RadioIfMultiDomainCap.u1RadioId - 1);
#endif
        if (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType == RADIO_TYPE_A)
        {
            if (RadioIfSetMaxTxPowerLevel2G (&RadioIfGetDB,
                                             (UINT1 *) &au1RadioName) !=
                OSIX_SUCCESS)
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                             "RadioIfConfigUpdateReq(MaxTxPowerLevel):  "
                             "Max Tx Power - HW call to set the values failed\r\n");
                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                 RadioIfGetAllDB.
                                                 pu1AntennaSelection);
                return OSIX_FAILURE;
            }
        }
        else
        {
            if (RadioIfSetMaxTxPowerLevel5G (&RadioIfGetDB,
                                             (UINT1 *) &au1RadioName) !=
                OSIX_SUCCESS)
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                             "RadioIfConfigUpdateReq(MultiDomainCapab):  Queue "
                             "Max Tx Power - HW to call set the values failed\r\n");
                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                 RadioIfGetAllDB.
                                                 pu1AntennaSelection);
                return OSIX_FAILURE;
            }
        }
    }

    if (pWssMsgStruct->unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfConfig.
        isPresent == OSIX_TRUE)
    {
        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4)
            (pWssMsgStruct->unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfConfig.
             u1RadioId + SYS_DEF_MAX_ENET_INTERFACES);

        RadioIfGetDB.RadioIfIsGetAllDB.bBssIdCount = OSIX_TRUE;

        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                      (&RadioIfGetDB)) != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfProcessConfigUpdateReq: RBTREE get of "
                         "RadioIf DB failed \r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            return OSIX_FAILURE;
        }

        if (RadioIfGetDB.RadioIfGetAllDB.u1BssIdCount == 0)
        {
            RadioIfGetDB.RadioIfGetAllDB.u1WlanId = 1;
        }
        else
        {
            for (u1Index = 1; u1Index <= RADIO_MAX_BSSID_SUPPORTED; u1Index++)
            {
                RadioIfGetDB.RadioIfGetAllDB.u1WlanId = u1Index;
                if (WssIfProcessRadioIfDBMsg (WSS_GET_WLAN_BSS_IFINDEX_DB,
                                              (&RadioIfGetDB)) == OSIX_SUCCESS)
                {
                    if (RadioIfGetDB.RadioIfGetAllDB.u4BssIfIndex != 0)
                        break;
                }
                else
                    continue;
            }
        }

        RadioIfGetDB.RadioIfGetAllDB.u2BeaconPeriod =
            pWssMsgStruct->unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfConfig.
            u2BeaconPeriod;

        /*multi radio */
        MEMSET (au1RadioName, 0, RADIO_INTF_NAME_LEN);
#ifdef NPAPI_WANTED
        SPRINTF ((CHR1 *) au1RadioName, "%s%d", RADIO_INTF_NAME,
                 pWssMsgStruct->unRadioIfMsg.
                 RadioIfConfigUpdateReq.RadioIfConfig.u1RadioId - 1);
#endif
        if (RadioIfSetBeaconPeriod (&RadioIfGetDB, (UINT1 *) &au1RadioName) !=
            OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigUpdateReq:  Beacon - HW call to set "
                         "the values failed\r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            return OSIX_FAILURE;
        }

        RadioIfGetDB.RadioIfGetAllDB.u1DTIMPeriod =
            pWssMsgStruct->unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfConfig.
            u1DtimPeriod;

        if (RadioIfSetDTIMPeriod (&RadioIfGetDB,
                                  (UINT1 *) &au1RadioName) != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigUpdateReq: DTIM HW call to set "
                         "the values failed\r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            return OSIX_FAILURE;
        }

        MEMCPY (RadioIfGetDB.RadioIfGetAllDB.au1CountryString,
                pWssMsgStruct->unRadioIfMsg.
                RadioIfConfigUpdateReq.RadioIfConfig.au1CountryString,
                STRLEN (pWssMsgStruct->unRadioIfMsg.
                        RadioIfConfigUpdateReq.RadioIfConfig.au1CountryString));

        if (RadioIfSetCountryString (&RadioIfGetDB,
                                     (UINT1 *) &au1RadioName) != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigUpdateReq: Country String - HW call to set "
                         "the values failed\r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            return OSIX_FAILURE;
        }
    }

    if (pWssMsgStruct->unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfInfoElem.
        isPresent == OSIX_TRUE)
    {
        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4)
            (pWssMsgStruct->unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfInfoElem.
             u1RadioId + SYS_DEF_MAX_ENET_INTERFACES);

        RadioIfGetDB.RadioIfIsGetAllDB.bBssIdCount = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_FALSE;
        RadioIfGetDB.RadioIfIsGetAllDB.bCurrentChannel = OSIX_TRUE;

        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                      (&RadioIfGetDB)) != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfProcessConfigUpdateReq: RBTREE get of RadioIf "
                         "DB failed in info element\r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            return OSIX_FAILURE;
        }

        if (RadioIfGetDB.RadioIfGetAllDB.u1BssIdCount == 0)
        {
            RadioIfGetDB.RadioIfGetAllDB.u1WlanId = 1;
        }
        else
        {
            for (u1Index = 1; u1Index <= RADIO_MAX_BSSID_SUPPORTED; u1Index++)
            {
                RadioIfGetDB.RadioIfGetAllDB.u1WlanId = u1Index;
                if (WssIfProcessRadioIfDBMsg (WSS_GET_WLAN_BSS_IFINDEX_DB,
                                              (&RadioIfGetDB)) == OSIX_SUCCESS)
                {
                    if (RadioIfGetDB.RadioIfGetAllDB.u4BssIfIndex != 0)
                        break;
                }
                else
                    continue;
            }

        }
        RadioIfGetDB.RadioIfGetAllDB.u1RadioId =
            pWssMsgStruct->unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfInfoElem.
            u1RadioId;
        RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;

        switch (pWssMsgStruct->unRadioIfMsg.
                RadioIfConfigUpdateReq.RadioIfInfoElem.u1EleId)
        {
            case DOT11_INFO_ELEM_HTCAPABILITY:
                /* HT Capabilities Element */
                RadioIfGetDB.RadioIfGetAllDB.u1HTCapEnable =
                    DOT11_INFO_ELEM_HTCAPABILITY;
                RadioIfGetDB.RadioIfGetAllDB.u1HTOpeEnable = 0;
                RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u2HtCapInfo =
                    pWssMsgStruct->unRadioIfMsg.
                    RadioIfConfigUpdateReq.RadioIfInfoElem.unInfoElem.
                    HTCapability.u2HTCapInfo;
                RadioIfGetDB.RadioIfIsGetAllDB.bHtCapInfo = OSIX_TRUE;

                RadioIfGetDB.RadioIfGetAllDB.Dot11NampduParams.u1AmpduParam =
                    pWssMsgStruct->unRadioIfMsg.
                    RadioIfConfigUpdateReq.RadioIfInfoElem.unInfoElem.
                    HTCapability.u1AMPDUParam;
                RadioIfGetDB.RadioIfIsGetAllDB.bAmpduParam = OSIX_TRUE;

                MEMCPY (RadioIfGetDB.RadioIfGetAllDB.Dot11NsuppMcsParams.
                        au1HtCapaMcs,
                        pWssMsgStruct->unRadioIfMsg.RadioIfConfigUpdateReq.
                        RadioIfInfoElem.unInfoElem.HTCapability.au1SuppMCSSet,
                        sizeof (RadioIfGetDB.RadioIfGetAllDB.
                                Dot11NsuppMcsParams.au1HtCapaMcs));
                RadioIfGetDB.RadioIfIsGetAllDB.bHtCapaMcs = OSIX_TRUE;
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtExtCapParams.u2HtExtCap =
                    pWssMsgStruct->unRadioIfMsg.
                    RadioIfConfigUpdateReq.RadioIfInfoElem.unInfoElem.
                    HTCapability.u2HTExtCap;
                RadioIfGetDB.RadioIfIsGetAllDB.bHtExtCap = OSIX_TRUE;

                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u4TxBeamCapParam =
                    pWssMsgStruct->unRadioIfMsg.RadioIfConfigUpdateReq.
                    RadioIfInfoElem.unInfoElem.HTCapability.u4TranBeamformCap;
                RadioIfGetDB.RadioIfIsGetAllDB.bTxBeamCapParam = OSIX_TRUE;

                MEMSET (au1RadioName, 0, RADIO_INTF_NAME_LEN);
#ifdef NPAPI_WANTED
                SPRINTF ((CHR1 *) au1RadioName, "%s%d", RADIO_INTF_NAME,
                         pWssMsgStruct->unRadioIfMsg.
                         RadioIfConfigUpdateReq.RadioIfInfoElem.u1RadioId - 1);
#endif
                RadioIfGetDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_FALSE;

                if (WssIfProcessRadioIfDBMsg (WSS_SET_RADIO_IF_DB_11N,
                                              (&RadioIfGetDB)) != OSIX_SUCCESS)
                {
                    RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                                 "RadioIfProcessConfigUpdateReq: RBTREE get of RadioIf "
                                 "DB failed in HT Operation info element\r\n");
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    return OSIX_FAILURE;
                }

                /*This function Call used for updating HT element details to shadowDB */
                if (RadioIfSetDot11N (&RadioIfGetDB,
                                      (UINT1 *) &au1RadioName) != OSIX_SUCCESS)
                {
                    RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                                 "RadioIfConfigUpdateReq: 80211n HT Capabilities - "
                                 "HW call to set " "the values failed\r\n");
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    return OSIX_FAILURE;
                }
                break;

            case DOT11_INFO_ELEM_HTOPERATION:

                RadioIfGetDB.RadioIfGetAllDB.u1HTOpeEnable =
                    DOT11_INFO_ELEM_HTOPERATION;
                RadioIfGetDB.RadioIfGetAllDB.u1HTCapEnable = 0;
                RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.
                    u1PrimaryChannel =
                    pWssMsgStruct->unRadioIfMsg.RadioIfConfigUpdateReq.
                    RadioIfInfoElem.unInfoElem.HTOperation.u1PrimaryCh;
                RadioIfGetDB.RadioIfIsGetAllDB.bPrimaryChannel = OSIX_TRUE;

                MEMCPY (RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.
                        au1HTOpeInfo,
                        pWssMsgStruct->unRadioIfMsg.RadioIfConfigUpdateReq.
                        RadioIfInfoElem.unInfoElem.HTOperation.au1HTOpeInfo,
                        WSSMAC_HTOPE_INFO);
                RadioIfGetDB.RadioIfIsGetAllDB.bHTOpeInfo = OSIX_TRUE;

                MEMCPY (RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.
                        au1BasicMCSSet,
                        pWssMsgStruct->unRadioIfMsg.RadioIfConfigUpdateReq.
                        RadioIfInfoElem.unInfoElem.HTOperation.au1BasicMCSSet,
                        WSSMAC_BASIC_MCS_SET);
                RadioIfGetDB.RadioIfIsGetAllDB.bBasicMCSSet = OSIX_TRUE;

                if (WssIfProcessRadioIfDBMsg (WSS_SET_RADIO_IF_DB_11N,
                                              (&RadioIfGetDB)) != OSIX_SUCCESS)
                {
                    RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                                 "RadioIfProcessConfigUpdateReq: RBTREE get of RadioIf "
                                 "DB failed in HT Operation info element\r\n");
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    return OSIX_FAILURE;
                }

                MEMSET (au1RadioName, 0, RADIO_INTF_NAME_LEN);
#ifdef NPAPI_WANTED
                SPRINTF ((CHR1 *) au1RadioName, "%s%d", RADIO_INTF_NAME,
                         pWssMsgStruct->unRadioIfMsg.
                         RadioIfConfigUpdateReq.RadioIfInfoElem.u1RadioId - 1);
#endif
                RadioIfGetDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_FALSE;
                /*This function Call used for updating HT Operation element details to shadowDB */
                if (RadioIfSetDot11N (&RadioIfGetDB,
                                      (UINT1 *) &au1RadioName) != OSIX_SUCCESS)
                {
                    RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                                 "RadioIfConfigUpdateReq: 80211n HT Operation - "
                                 "HW call to set " "the values failed\r\n");
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    return OSIX_FAILURE;
                }
                break;

            case DOT11AC_INFO_ELEM_VHTCAPABILITY:
                /*For 802.11AC - VHT Capability */

                RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u4VhtCapInfo =
                    pWssMsgStruct->unRadioIfMsg.
                    RadioIfConfigUpdateReq.RadioIfInfoElem.unInfoElem.
                    VHTCapability.u4VhtCapInfo;

                if (WssIfProcessRadioIfDBMsg (WSS_PARSE_11AC_CAPABILITY_INFO,
                                              (&RadioIfGetDB)) != OSIX_SUCCESS)
                {
                    RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                                 "RadioIfProcessConfigUpdateReq:"
                                 "Parsing of VHT Capability info element failed\r\n");
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    return OSIX_FAILURE;
                }
                RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1MaxMPDULen =
                    RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.
                    u1SuppMaxMPDULen;
                RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1RxLpdc =
                    RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1SuppRxLpdc;
                RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1ShortGi80 =
                    RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.
                    u1SuppShortGi80;
                RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1TxStbc =
                    RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1SuppTxStbc;
                RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1RxStbc =
                    RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1SuppRxStbc;
                RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1VhtMaxAMPDU =
                    RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.
                    u1SuppVhtMaxAMPDU;

                MEMCPY (RadioIfGetDB.RadioIfGetAllDB.
                        Dot11AcCapaParams.au1VhtCapaMcs,
                        pWssMsgStruct->unRadioIfMsg.
                        RadioIfConfigUpdateReq.RadioIfInfoElem.unInfoElem.
                        VHTCapability.au1SuppMCS, DOT11AC_VHT_CAP_MCS_LEN);
                MEMCPY (&u2RxMcs, (RadioIfGetDB.RadioIfGetAllDB.
                                   Dot11AcCapaParams.au1VhtCapaMcs),
                        DOT11AC_MCS_MAP);
                MEMCPY (&u2TxMcs,
                        &(RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.
                          au1VhtCapaMcs[DOT11AC_MCS_MAP + DOT11AC_MCS_MAP]),
                        DOT11AC_MCS_MAP);
                WssIfVhtSts (&RadioIfGetDB, &u1Sts);
                RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1VhtSTS = u1Sts;
                RadioIfGetDB.RadioIfGetAllDB.Dot11AcOperParams.
                    u2VhtOperMcsSet = (u2RxMcs & u2TxMcs);
                /*Highest Supported Long Gi Data Rate */
                MEMCPY (&u2RxDataRate, &(RadioIfGetDB.RadioIfGetAllDB.
                                         Dot11AcCapaParams.
                                         au1VhtCapaMcs[RADIO_VALUE_2]),
                        RADIO_VALUE_2);
                RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u2RxDataRate =
                    (UINT2) ((OSIX_NTOHS (u2RxDataRate)) >> RADIO_VALUE_3);
                MEMCPY (&u2TxDataRate,
                        &(RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.
                          au1VhtCapaMcs[RADIO_VALUE_6]), RADIO_VALUE_2);
                RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u2TxDataRate =
                    (UINT2) ((OSIX_NTOHS (u2TxDataRate)) >> RADIO_VALUE_3);

                RadioIfGetDB.RadioIfIsGetAllDB.bMaxMPDULen = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bRxLpdc = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bShortGi80 = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bTxStbc = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bRxStbc = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bVhtMaxAMPDU = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bVhtCapInfo = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bVhtCapaMcs = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bVhtOperMcsSet = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bVhtSTS = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bRxDataRate = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bTxDataRate = OSIX_TRUE;
                /*Have to set the value for VhtSTS after discussion with Raja */

                if (WssIfProcessRadioIfDBMsg (WSS_SET_RADIO_IF_DB_11AC,
                                              (&RadioIfGetDB)) != OSIX_SUCCESS)
                {
                    RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                                 "RadioIfProcessConfigUpdateReq: RBTREE get of RadioIf "
                                 "DB failed in VHT Capability info element\r\n");
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    return OSIX_FAILURE;
                }
                MEMSET (au1RadioName, 0, RADIO_INTF_NAME_LEN);
#ifdef NPAPI_WANTED
                SPRINTF ((CHR1 *) au1RadioName, "%s%d", RADIO_INTF_NAME,
                         pWssMsgStruct->unRadioIfMsg.
                         RadioIfConfigUpdateReq.RadioIfInfoElem.u1RadioId - 1);
#endif
                /*Re-setting all the varialbes and fetching only VhtOper elements 
                 * for beacon construction */
                MEMSET (&(RadioIfGetDB.RadioIfIsGetAllDB), 0,
                        sizeof (RadioIfGetDB.RadioIfIsGetAllDB));
                RadioIfGetDB.RadioIfIsGetAllDB.bVhtOperInfo = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bVhtOperMcsSet = OSIX_TRUE;

                if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11AC,
                                              (&RadioIfGetDB)) != OSIX_SUCCESS)
                {
                    RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                                 "RadioIfProcessConfigUpdateReq: RBTREE get of RadioIf "
                                 "DB failed in VHT Capability info element\r\n");
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    return OSIX_FAILURE;
                }

                if (RadioIfSetDot11AC (&RadioIfGetDB,
                                       (UINT1 *) &au1RadioName) != OSIX_SUCCESS)
                {
                    RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                                 "RadioIfConfigUpdateReq: 80211ac VHT Capabilities - HW call to set "
                                 "the values failed\r\n");
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    return OSIX_FAILURE;
                }

                break;

            case DOT11AC_INFO_ELEM_VHTOPERATION:
                /*For 802.11AC - VHT Operation */
                pwssWlanDBMsg =
                    (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();

                if (pwssWlanDBMsg == NULL)
                {
                    RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                                 "RadioIfProcessConfigUpdateReq: "
                                 "Memory allocation failure for WlanDb\r\n");
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    return OSIX_FAILURE;
                }

                MEMSET (pwssWlanDBMsg, 0, sizeof (tWssWlanDB));

                RadioIfGetDB.RadioIfIsGetAllDB.bChannelWidth = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bLocalTxPower = OSIX_TRUE;
                if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                              (&RadioIfGetDB)) != OSIX_SUCCESS)
                {
                    RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                                 "RadioIfProcessConfigUpdateReq: RBTREE get of RadioIf "
                                 "DB failed in VHT Opeartion info element\r\n");
                    UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDBMsg);
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    return OSIX_FAILURE;
                }
                RadioIfGetDB.RadioIfIsGetAllDB.bChannelWidth = OSIX_FALSE;
                RadioIfGetDB.RadioIfIsGetAllDB.bLocalTxPower = OSIX_FALSE;

                RadioIfGetDB.RadioIfIsGetAllDB.bVhtSTS = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bSpecMgmt = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bVhtOption = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bVhtOperModeNotify = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bVhtCapInfo = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bVhtChannelWidth = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bVhtCapaMcs = OSIX_TRUE;
                /*Fetch Vht Transmit Power Envelop element from Radio DB */
                RadioIfGetDB.RadioIfIsGetAllDB.bSpecMgmt = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bTransPower = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bTxPower20 = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bTxPower40 = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bTxPower80 = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bTxPower160 = OSIX_TRUE;

                if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11AC,
                                              (&RadioIfGetDB)) != OSIX_SUCCESS)
                {
                    RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                                 "RadioIfProcessConfigUpdateReq: RBTREE get of RadioIf "
                                 "DB failed in VHT Capability info element\r\n");
                    UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDBMsg);
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    return OSIX_FAILURE;
                }
                RadioIfGetDB.RadioIfIsGetAllDB.bVhtSTS = OSIX_FALSE;
                RadioIfGetDB.RadioIfIsGetAllDB.bSpecMgmt = OSIX_FALSE;
                RadioIfGetDB.RadioIfIsGetAllDB.bVhtOption = OSIX_FALSE;
                RadioIfGetDB.RadioIfIsGetAllDB.bVhtOperModeNotify = OSIX_FALSE;
                RadioIfGetDB.RadioIfIsGetAllDB.bVhtCapInfo = OSIX_FALSE;
                RadioIfGetDB.RadioIfIsGetAllDB.bVhtCapaMcs = OSIX_FALSE;
                RadioIfGetDB.RadioIfIsGetAllDB.bSpecMgmt = OSIX_FALSE;
                RadioIfGetDB.RadioIfIsGetAllDB.bTransPower = OSIX_FALSE;
                RadioIfGetDB.RadioIfIsGetAllDB.bTxPower20 = OSIX_FALSE;
                RadioIfGetDB.RadioIfIsGetAllDB.bTxPower40 = OSIX_FALSE;
                RadioIfGetDB.RadioIfIsGetAllDB.bTxPower80 = OSIX_FALSE;
                RadioIfGetDB.RadioIfIsGetAllDB.bTxPower160 = OSIX_FALSE;

                u1ChanWidthDb = WssIfComputeBandWidth (&RadioIfGetDB);
                /*Calculate channel width from VHT and HT configured
                 * Channel width*/
                MEMCPY (RadioIfGetDB.RadioIfGetAllDB.
                        Dot11AcOperParams.au1VhtOperInfo,
                        pWssMsgStruct->unRadioIfMsg.
                        RadioIfConfigUpdateReq.RadioIfInfoElem.unInfoElem.
                        VHTOperation.au1VhtOperInfo, DOT11AC_VHT_OPER_INFO_LEN);

                if (WssIfProcessRadioIfDBMsg (WSS_PARSE_11AC_OPER_INFO,
                                              (&RadioIfGetDB)) != OSIX_SUCCESS)
                {
                    RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                                 "RadioIfProcessConfigUpdateReq:"
                                 "Parsing of VHT Operation info element failed\r\n");
                    UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDBMsg);
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    return OSIX_FAILURE;
                }
                RadioIfGetDB.RadioIfGetAllDB.Dot11AcOperParams.
                    u1VhtChannelWidth =
                    RadioIfGetDB.RadioIfGetAllDB.
                    Dot11AcOperParams.u1SuppVhtChannelWidth;
                RadioIfGetDB.RadioIfGetAllDB.Dot11AcOperParams.u1CenterFcy0 =
                    RadioIfGetDB.RadioIfGetAllDB.Dot11AcOperParams.
                    u1SuppCenterFcy0;
                RadioIfGetDB.RadioIfGetAllDB.Dot11AcOperParams.u2VhtOperMcsSet =
                    pWssMsgStruct->unRadioIfMsg.
                    RadioIfConfigUpdateReq.RadioIfInfoElem.unInfoElem.
                    VHTOperation.u2BasicMCS;
                u1ChanWidth = WssIfComputeBandWidth (&RadioIfGetDB);
                /*  Code_for_action_frame */
                pwssWlanDBMsg->WssWlanAttributeDB.u1RadioId =
                    pWssMsgStruct->unRadioIfMsg.RadioIfConfigUpdateReq.
                    RadioIfInfoElem.u1RadioId;
                pwssWlanDBMsg->WssWlanAttributeDB.u1WlanId =
                    pWssMsgStruct->unRadioIfMsg.RadioIfConfigUpdateReq.
                    RadioIfInfoElem.u1WlanId;
                pwssWlanDBMsg->WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
                pwssWlanDBMsg->WssWlanIsPresentDB.bBssId = OSIX_TRUE;

                if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_INTERFACE_ENTRY,
                                              pwssWlanDBMsg) != OSIX_SUCCESS)
                {
                    RADIOIF_TRC (RADIOIF_INFO_TRC, "RadioIfConfigUpdateReq:"
                                 " WssIfProcessWssWlanDBMsg returned FAILURE \r\n");
                }

                pwssWlanDBMsg->WssWlanIsPresentDB.bCapability = OSIX_TRUE;

                if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY,
                                              pwssWlanDBMsg) != OSIX_SUCCESS)
                {
                    RADIOIF_TRC (RADIOIF_INFO_TRC, "RadioIfConfigUpdateReq:"
                                 " WssIfProcessWssWlanDBMsg returned FAILURE \r\n");
                }
                RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1SpecMgmt
                    = (UINT1) (((pwssWlanDBMsg->WssWlanAttributeDB.
                                 u2Capability) & WSS_WLAN_SPEC_MGMT) >>
                               WSSSTA_SHIFT_8);

                if ((RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.
                     u1SpecMgmt == OSIX_TRUE) && (u1ChanWidthDb != u1ChanWidth))
                {
                    RadioIfGetDB.RadioIfGetAllDB.Dot11AcOperParams.
                        u1Action = OSIX_TRUE;
                    WssStaConstructChanlSwitchAnnounce (pWssMsgStruct->
                                                        unRadioIfMsg.
                                                        RadioIfConfigUpdateReq.
                                                        RadioIfInfoElem.
                                                        u1RadioId - 1,
                                                        &RadioIfGetDB);
                    RadioIfGetDB.RadioIfGetAllDB.u1ExChanSwitch = OSIX_TRUE;
                    WssStaConstructChanlSwitchAnnounce (pWssMsgStruct->
                                                        unRadioIfMsg.
                                                        RadioIfConfigUpdateReq.
                                                        RadioIfInfoElem.
                                                        u1RadioId - 1,
                                                        &RadioIfGetDB);
                    MEMSET (au1RadioName, 0, RADIO_INTF_NAME_LEN);
#ifdef NPAPI_WANTED
                    SPRINTF ((CHR1 *) au1RadioName, "%s%d", RADIO_INTF_NAME,
                             pWssMsgStruct->unRadioIfMsg.
                             RadioIfConfigUpdateReq.RadioIfInfoElem.u1RadioId -
                             1);
#endif
                    if (RadioIfSetDot11AC
                        (&RadioIfGetDB, (UINT1 *) &au1RadioName)
                        != OSIX_SUCCESS)
                    {
                        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                                     "RadioIfConfigUpdateReq:(Channel Wrapper IE)"
                                     "HW call to add "
                                     "Channel Wrapper IE failed\r\n");
                        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDBMsg);
                        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                         RadioIfGetAllDB.
                                                         pu1AntennaSelection);
                        return OSIX_FAILURE;
                    }

                    RfMgmtWtpTmrStart (RadioIfGetDB.RadioIfGetAllDB.
                                       u4RadioIfIndex,
                                       RFMGMT_CHAN_SWIT_ANNOUNCE_TMR,
                                       RFMGMT_CHANL_SWIT_AGEOUT_PERIOD);
                }
                /* Assembling Operation Mode Notification Element */
                if (RadioIfGetDB.RadioIfGetAllDB.Dot11AcOperParams.
                    u1VhtOperModeNotify == OSIX_TRUE)
                {
                    WssIfAssembleOperModeNotify (&RadioIfGetDB);
                }
                /* Assembling Vht Power Envelope Element */
                if ((RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.
                     u1SpecMgmt == OSIX_TRUE) && (RadioIfGetDB.
                                                  RadioIfGetAllDB.
                                                  Dot11AcOperParams.
                                                  u1VhtOption == OSIX_TRUE))
                {
                    WssIfAssembleVhtPowerEnvelope (&RadioIfGetDB);
                }

                RadioIfGetDB.RadioIfIsGetAllDB.bVhtChannelWidth = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bCenterFcy0 = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bVhtOperInfo = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bVhtOperMcsSet = OSIX_TRUE;

                if (WssIfProcessRadioIfDBMsg (WSS_SET_RADIO_IF_DB_11AC,
                                              (&RadioIfGetDB)) != OSIX_SUCCESS)
                {
                    RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                                 "RadioIfProcessConfigUpdateReq: RBTREE get of RadioIf "
                                 "DB failed in VHT Opeartion info element\r\n");
                    UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDBMsg);
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    return OSIX_FAILURE;
                }
                MEMSET (au1RadioName, 0, RADIO_INTF_NAME_LEN);
#ifdef NPAPI_WANTED
                SPRINTF ((CHR1 *) au1RadioName, "%s%d", RADIO_INTF_NAME,
                         pWssMsgStruct->unRadioIfMsg.
                         RadioIfConfigUpdateReq.RadioIfInfoElem.u1RadioId - 1);
#endif
                /*Re-setting all the varialbes and fetching only VhtCapability elements
                 * for beacon construction */
                MEMSET (&(RadioIfGetDB.RadioIfIsGetAllDB), 0,
                        sizeof (RadioIfGetDB.RadioIfIsGetAllDB));

                if (RadioIfSetDot11AC (&RadioIfGetDB,
                                       (UINT1 *) &au1RadioName) != OSIX_SUCCESS)
                {
                    RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                                 "RadioIfConfigUpdateReq: 80211ac VHT Capabilities - HW call to set "
                                 "the values failed\r\n");
                    UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDBMsg);
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    return OSIX_FAILURE;
                }
                UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDBMsg);
                break;

            default:
                break;
        }
    }

    if (pWssMsgStruct->unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfDot11nParam.
        isPresent == OSIX_TRUE)
    {
        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4)
            (pWssMsgStruct->unRadioIfMsg.
             RadioIfConfigUpdateReq.RadioIfDot11nParam.u1RadioId +
             SYS_DEF_MAX_ENET_INTERFACES);

        RadioIfGetDB.RadioIfIsGetAllDB.bBssIdCount = OSIX_TRUE;
        /*For 802.11n */
        RadioIfGetDB.RadioIfIsGetAllDB.bChannelWidth = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bSupportedRate = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bShortGi20 = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bShortGi40 = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bMCSRateSet = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bCurrentChannel = OSIX_TRUE;
        /*For 802.11n */

        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                      (&RadioIfGetDB)) != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfProcessConfigUpdateReq: RBTREE get of RadioIf "
                         "DB failed \r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            return OSIX_FAILURE;
        }

        if (RadioIfGetDB.RadioIfGetAllDB.u1BssIdCount == 0)
        {
            RadioIfGetDB.RadioIfGetAllDB.u1WlanId = 1;
        }
        else
        {
            for (u1Index = 1; u1Index <= RADIO_MAX_BSSID_SUPPORTED; u1Index++)
            {
                RadioIfGetDB.RadioIfGetAllDB.u1WlanId = u1Index;
                if (WssIfProcessRadioIfDBMsg (WSS_GET_WLAN_BSS_IFINDEX_DB,
                                              (&RadioIfGetDB)) == OSIX_SUCCESS)
                {
                    if (RadioIfGetDB.RadioIfGetAllDB.u4BssIfIndex != 0)
                        break;
                }
                else
                    continue;
            }

        }
        RadioIfGetDB.RadioIfGetAllDB.u1RadioId =
            pWssMsgStruct->unRadioIfMsg.
            RadioIfConfigUpdateReq.RadioIfDot11nParam.u1RadioId;
        RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;

        RadioIfGetDB.RadioIfGetAllDB.u1HtFlag =
            pWssMsgStruct->unRadioIfMsg.
            RadioIfConfigUpdateReq.RadioIfDot11nParam.u1HtFlag;
        RadioIfGetDB.RadioIfIsGetAllDB.bHtFlag = OSIX_TRUE;

        RadioIfGetDB.RadioIfGetAllDB.u1MaxSuppMCS =
            pWssMsgStruct->unRadioIfMsg.
            RadioIfConfigUpdateReq.RadioIfDot11nParam.u1MaxSuppMCS;
        RadioIfGetDB.RadioIfIsGetAllDB.bMaxSuppMCS = OSIX_TRUE;

        RadioIfGetDB.RadioIfGetAllDB.u1MaxManMCS =
            pWssMsgStruct->unRadioIfMsg.
            RadioIfConfigUpdateReq.RadioIfDot11nParam.u1MaxManMCS;
        RadioIfGetDB.RadioIfIsGetAllDB.bMaxManMCS = OSIX_TRUE;

        RadioIfGetDB.RadioIfGetAllDB.u1TxAntenna =
            pWssMsgStruct->unRadioIfMsg.
            RadioIfConfigUpdateReq.RadioIfDot11nParam.u1TxAntenna;
        RadioIfGetDB.RadioIfIsGetAllDB.bTxAntenna = OSIX_TRUE;

        RadioIfGetDB.RadioIfGetAllDB.u1RxAntenna =
            pWssMsgStruct->unRadioIfMsg.
            RadioIfConfigUpdateReq.RadioIfDot11nParam.u1RxAntenna;
        RadioIfGetDB.RadioIfIsGetAllDB.bRxAntenna = OSIX_TRUE;

    }

    if (pWssMsgStruct->unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfDot11nCfg.
        isPresent == OSIX_TRUE)
    {
        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4)
            (pWssMsgStruct->unRadioIfMsg.
             RadioIfConfigUpdateReq.RadioIfDot11nCfg.u1RadioId +
             SYS_DEF_MAX_ENET_INTERFACES);

        RadioIfGetDB.RadioIfIsGetAllDB.bBssIdCount = OSIX_TRUE;
        /*For 802.11n */
        RadioIfGetDB.RadioIfIsGetAllDB.bAMPDUStatus = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bAMPDUSubFrame = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bAMPDULimit = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bAMSDUStatus = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bAMSDULimit = OSIX_TRUE;

        /*For 802.11n */

        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                      (&RadioIfGetDB)) != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfProcessConfigUpdateReq: RBTREE get of RadioIf "
                         "DB failed \r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            return OSIX_FAILURE;
        }

        RadioIfGetDB.RadioIfGetAllDB.u1RadioId =
            pWssMsgStruct->unRadioIfMsg.
            RadioIfConfigUpdateReq.RadioIfDot11nCfg.u1RadioId;
        RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;

        RadioIfGetDB.RadioIfGetAllDB.u1AMPDUStatus =
            pWssMsgStruct->unRadioIfMsg.
            RadioIfConfigUpdateReq.RadioIfDot11nCfg.u1AMPDUStatus;
        RadioIfGetDB.RadioIfIsGetAllDB.bAMPDUStatus = OSIX_TRUE;

        RadioIfGetDB.RadioIfGetAllDB.u1AMPDUSubFrame =
            pWssMsgStruct->unRadioIfMsg.
            RadioIfConfigUpdateReq.RadioIfDot11nCfg.u1AMPDUSubFrame;
        RadioIfGetDB.RadioIfIsGetAllDB.bAMPDUSubFrame = OSIX_TRUE;

        RadioIfGetDB.RadioIfGetAllDB.u2AMPDULimit =
            pWssMsgStruct->unRadioIfMsg.
            RadioIfConfigUpdateReq.RadioIfDot11nCfg.u2AMPDULimit;
        RadioIfGetDB.RadioIfIsGetAllDB.bAMPDULimit = OSIX_TRUE;

        RadioIfGetDB.RadioIfGetAllDB.u1AMSDUStatus =
            pWssMsgStruct->unRadioIfMsg.
            RadioIfConfigUpdateReq.RadioIfDot11nCfg.u1AMSDUStatus;
        RadioIfGetDB.RadioIfIsGetAllDB.bAMSDUStatus = OSIX_TRUE;

        RadioIfGetDB.RadioIfGetAllDB.u2AMSDULimit =
            pWssMsgStruct->unRadioIfMsg.
            RadioIfConfigUpdateReq.RadioIfDot11nCfg.u2AMSDULimit;
        RadioIfGetDB.RadioIfIsGetAllDB.bAMSDULimit = OSIX_TRUE;

        MEMSET (au1RadioName, 0, RADIO_INTF_NAME_LEN);
        SPRINTF ((CHR1 *) au1RadioName, "%s%d", RADIO_INTF_NAME,
                 pWssMsgStruct->unRadioIfMsg.
                 RadioIfConfigUpdateReq.RadioIfDot11nCfg.u1RadioId - 1);
        if (RadioIfSetDot11nConfig (&RadioIfGetDB,
                                    (UINT1 *) &au1RadioName) != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfConfigUpdateReq: 80211n Config - HW call to set "
                         "the values failed\r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            return OSIX_FAILURE;
        }
    }
    /* Reset the BssId count flag */
    RadioIfGetDB.RadioIfIsGetAllDB.bBssIdCount = OSIX_FALSE;

    /*To set the values in RadioIfDB */
    if (WssIfProcessRadioIfDBMsg (WSS_SET_RADIO_IF_DB,
                                  &RadioIfGetDB) != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC, "RadioDb Updation Failed \r\n");
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                         RadioIfGetAllDB.pu1AntennaSelection);
        return OSIX_FAILURE;
    }

#ifdef BCNMGR_WANTED
    if (WssifUpdateBeaconParamForAllBssId (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC, "Update Beacon Param Failed \r\n");
    }
#endif

    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                     RadioIfGetAllDB.pu1AntennaSelection);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfChangeStateEventReq                           *
 *                                                                           *
 * Description        : Function triggers change state event request when    *
 *                      there is a change in radio status                    *
 *                                                                           *
 * Input(s)           : pWssMsgStruct - Pointer to the input structure       *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfChangeStateEventReq (tRadioIfMsgStruct * pWssMsgStruct)
{
    tRadioIfGetDB       RadioIfGetDB;
    unApHdlrMsgStruct   ApHdlrMsgStruct;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    MEMSET (&ApHdlrMsgStruct, 0, sizeof (unApHdlrMsgStruct));

    if (pWssMsgStruct == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "Input validation failed, return failure\r\n");
        return OSIX_FAILURE;
    }

    if (pWssMsgStruct->unRadioIfMsg.RadioIfSetAdminOperStatus.u4IfIndex
        > SYS_DEF_MAX_PHYSICAL_INTERFACES)
    {
        return OSIX_SUCCESS;
    }
    RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
        UtlShMemAllocAntennaSelectionBuf ();

    if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC, "\r%% Memory allocation for Antenna"
                     " selection failed.\r\n");
        return OSIX_FAILURE;
    }

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
        pWssMsgStruct->unRadioIfMsg.RadioIfSetAdminOperStatus.u4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bOperStatus = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bBssIdCount = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;

    /*To get the RadioIfDB Value (Parameters needed here : BSSID COUNT ) */
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                  &RadioIfGetDB) != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfChangeStateEventReq: Get of Radio DB failed \r\n");
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                         RadioIfGetAllDB.pu1AntennaSelection);
        return OSIX_FAILURE;
    }

    if (RadioIfGetDB.RadioIfGetAllDB.u1OperStatus ==
        pWssMsgStruct->unRadioIfMsg.RadioIfSetAdminOperStatus.u1OperStatus)
    {
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                         RadioIfGetAllDB.pu1AntennaSelection);
        return OSIX_SUCCESS;
    }
    else
    {
        RadioIfGetDB.RadioIfGetAllDB.u1OperStatus =
            pWssMsgStruct->unRadioIfMsg.RadioIfSetAdminOperStatus.u1OperStatus;
    }
    if (pWssMsgStruct->unRadioIfMsg.RadioIfSetAdminOperStatus.u1IsFromMib ==
        CFA_TRUE)
    {
        if (WssIfProcessRadioIfDBMsg (WSS_SET_RADIO_IF_DB,
                                      &RadioIfGetDB) != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfChangeStateEventReq : RBTREE get of RadioIf "
                         "DB failed \r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            return OSIX_FAILURE;
        }
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                         RadioIfGetAllDB.pu1AntennaSelection);
        return OSIX_SUCCESS;
    }

    /* Constructing the message to be sent to HDLR */
    ApHdlrMsgStruct.RadioIfCheStateEvtReq.RadioIfOperStatus.isPresent =
        OSIX_TRUE;

    ApHdlrMsgStruct.RadioIfCheStateEvtReq.RadioIfOperStatus.u2MessageType
        = RADIOIF_CHANGE_STATE_EVENT_REQ_MSG_TYPE;

    ApHdlrMsgStruct.RadioIfCheStateEvtReq.RadioIfOperStatus.u2MessageLength
        = RADIOIF_CHANGE_STATE_EVENT_REQ_MSG_LEN;

    ApHdlrMsgStruct.RadioIfCheStateEvtReq.RadioIfOperStatus.u1RadioId
        = RadioIfGetDB.RadioIfGetAllDB.u1RadioId;

    ApHdlrMsgStruct.RadioIfCheStateEvtReq.RadioIfOperStatus.u1OperStatus =
        RadioIfGetDB.RadioIfGetAllDB.u1OperStatus;

    if (RadioIfGetDB.RadioIfGetAllDB.u1OperStatus == CFA_IF_DOWN)
        ApHdlrMsgStruct.RadioIfCheStateEvtReq.RadioIfOperStatus.u1FailureCause =
            RADIO_FAILURE_OCCURED;
    else
        ApHdlrMsgStruct.RadioIfCheStateEvtReq.RadioIfOperStatus.u1FailureCause =
            RADIO_FAILURE_CLEARED;

    ApHdlrMsgStruct.RadioIfCheStateEvtReq.RadioIfFailAlarm.isPresent
        = OSIX_TRUE;
    ApHdlrMsgStruct.RadioIfCheStateEvtReq.RadioIfFailAlarm.u2MessageType
        = RADIOIF_FAIL_ALARM_MSG_TYPE;
    ApHdlrMsgStruct.RadioIfCheStateEvtReq.RadioIfFailAlarm.u2MessageLength =
        RADIOIF_FAIL_ALARM_MSG_LEN;
    ApHdlrMsgStruct.RadioIfCheStateEvtReq.RadioIfFailAlarm.u1RadioId
        = RadioIfGetDB.RadioIfGetAllDB.u1RadioId;

    if (RadioIfGetDB.RadioIfGetAllDB.u1OperStatus == CFA_IF_DOWN)
        ApHdlrMsgStruct.RadioIfCheStateEvtReq.RadioIfFailAlarm.u1Status
            = RADIO_FAILURE_OCCURED;
    else
        ApHdlrMsgStruct.RadioIfCheStateEvtReq.RadioIfFailAlarm.u1Status
            = RADIO_FAILURE_CLEARED;

    if (WssIfProcessApHdlrMsg (WSS_APHDLR_CHANGE_STATE_EVENT_REQ,
                               &ApHdlrMsgStruct) != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfChangeStateEventReq: Sending Request failed\r\n");
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                         RadioIfGetAllDB.pu1AntennaSelection);
        return OSIX_FAILURE;
    }

    if (WssIfProcessRadioIfDBMsg (WSS_SET_RADIO_IF_DB,
                                  &RadioIfGetDB) != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfChangeStateEventReq: Get of Radio DB failed \r\n");
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                         RadioIfGetAllDB.pu1AntennaSelection);
        return OSIX_FAILURE;
    }
    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                     RadioIfGetAllDB.pu1AntennaSelection);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfWlanUpdate                                    *
 *                                                                           *
 * Description        : Function invokes the npapi calls to configure the    *
 *                      hardrware params after the wlan interface is created *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to the input structure       *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfWlanUpdate (tRadioIfGetDB * pRadioIfGetDB)
{
    RADIOIF_FN_ENTRY ();

    UINT1               au1RadioName[RADIO_INTF_NAME_LEN];
    UINT4               u4RadioType = 0;
    MEMSET (au1RadioName, 0, RADIO_INTF_NAME_LEN);
    /*Input Validation */
    if (pRadioIfGetDB == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfWlanUpdate : Input validation failed\n");
        return OSIX_FAILURE;
    }

    pRadioIfGetDB->RadioIfIsGetAllDB.bDot11RadioType = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bCurrentChannel = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bCurrentTxPower = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bRTSThreshold = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bFragmentationThreshold = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bBeaconPeriod = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bDTIMPeriod = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bCountryString = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                  pRadioIfGetDB) != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfWlanUpdate : RBTREE get of RadioIf DB failed \r\n");
    }

#ifdef NPAPI_WANTED
    /*multi radio */
    SPRINTF ((CHR1 *) au1RadioName, "%s%d", RADIO_INTF_NAME,
             pRadioIfGetDB->RadioIfGetAllDB.u1RadioId - 1);

#ifdef QORIQ_WANTED
    if (RadioIfSetOFDMCurrentChannelForWlan (pRadioIfGetDB,
                                             (UINT1 *) &au1RadioName) !=
        OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfWlanUpdate: Hw call to set OFDM channel failed\r\n");
    }
#endif
#endif
    if (RadioIfSetCountryString (pRadioIfGetDB,
                                 (UINT1 *) &au1RadioName) != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfConfigUpdateReq: Country String - HW call to set "
                     "the values failed\r\n");
    }

#ifdef T1023_WANTED
    u4RadioType = pRadioIfGetDB->RadioIfGetAllDB.u4Dot11RadioType;
    if ((u4RadioType == RADIO_TYPEA) || (u4RadioType == RADIO_TYPEAN)
        || (u4RadioType == RADIO_TYPEAC))
    {
        if (RadioIfSetOFDMCurrentChannel (pRadioIfGetDB,
                                          (UINT1 *) &au1RadioName) !=
            OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "radioifwlanupdate: hw call to set ofdm channel failed\r\n");
        }
    }
    else
    {
        if (RadioIfSetDSCCurrentChannel (pRadioIfGetDB,
                                         (UINT1 *) &au1RadioName) !=
            OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "radioifwlanupdate: hw call to set dsc channel failed\r\n");
        }
    }
#endif

    if (RadioIfSetTxPowerLevel (pRadioIfGetDB,
                                (UINT1 *) &au1RadioName) != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfWlanUpdate: Hw call to set TxPowerLevel failed\r\n");
    }

    if (RadioIfSetRTSThreshold (pRadioIfGetDB,
                                (UINT1 *) &au1RadioName) != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfWlanUpdate: Hw call to set RTSThreshold failed\n");
    }

    if (RadioIfSetFragmentationThreshold (pRadioIfGetDB,
                                          (UINT1 *) &au1RadioName) !=
        OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfWlanUpdate:Hw call to set Fragment threshold failed\n");
    }

    if (RadioIfSetBeaconPeriod (pRadioIfGetDB,
                                (UINT1 *) &au1RadioName) != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfWlanUpdate :Hw call to set beacon period failed\r\n");
    }

    if (RadioIfSetDTIMPeriod (pRadioIfGetDB,
                              (UINT1 *) &au1RadioName) != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfWlanUpdate: Hw call to set DTIM Period failed\r\n");
    }

    /* To store the Multi Domain Capability Information into RadioDB from gRadioIfDriverCapablities */
    if (RadioIfGetMultiDomainAndAssembleBeacon
        (pRadioIfGetDB, (UINT1 *) &au1RadioName) != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfWlanUpdate: Hw get for Multi Domain Capability Failed\n");
    }

    pRadioIfGetDB->RadioIfIsGetAllDB.bDot11RadioType = OSIX_FALSE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bMultiDomainInfo = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_SET_RADIO_IF_DB, pRadioIfGetDB)
        != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfWlanUpdate: Value to be set in DB Failed \r\n");
        return OSIX_FAILURE;
    }
    RADIOIF_FN_EXIT ();
    UNUSED_PARAM (u4RadioType);
    return OSIX_SUCCESS;
}

UINT1
RadioIfGetAllParams (tRadioIfGetDB * pRadioIfGetDB)
{

    if (pRadioIfGetDB == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfGetAllParams: Null Input received \r\n");
        return OSIX_FAILURE;
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bRadioIfIndex != OSIX_FALSE)
    {
        pRadioIfGetDB->RadioIfIsGetAllDB.bBeaconPeriod = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bCountryString = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bRTSThreshold = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bShortRetry = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bLongRetry = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bFragmentationThreshold = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bTxMsduLifetime = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bRxMsduLifetime = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bShortPreamble = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bDiversitySupport = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bBssIdCount = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bCurrentTxPowerLevel = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bRadioAntennaType = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bAntennaMode = OSIX_TRUE;

        /* get all the available antennas values */
        MEMSET (pRadioIfGetDB->RadioIfIsGetAllDB.bAntennaSelection, OSIX_TRUE,
                sizeof (pRadioIfGetDB->RadioIfIsGetAllDB.bAntennaSelection));
        pRadioIfGetDB->RadioIfIsGetAllDB.bCurrentTxAntenna = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bOperationalRate = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bSupportedBand = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bT1Threshold = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bCurrentFrequency = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bCurrentChannel = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bCurrentTxPower = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bNoOfSupportedPowerLevels = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bTaggingPolicy = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bQosIndex = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bQueueDepth = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bCwMin = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bCwMax = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bAifsn = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bTxOpLimit = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bQosPrio = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bDscp = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bDot11RadioType = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bHtFlag = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bMaxSuppMCS = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bMaxManMCS = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bTxAntenna = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bRxAntenna = OSIX_TRUE;

        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                      pRadioIfGetDB) != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfGetAllParams: Radio DB Access failure\n");
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfGetTxPowerTable                               *
 *                                                                           *
 * Description        : Function to get the Radio Qos related info from the  *
 *                      Radio DB                                             *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to Radio IF DB with ifIndex  *
 *                                                                           *
 * Output(s)          : pRadioIfGetDB - TxPower filled with Radio DB         *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfGetTxPowerTable (tRadioIfGetDB * pRadioIfGetDB)
{
    if (pRadioIfGetDB == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfGetTxPowerTable : Null Input received \r\n");
        return OSIX_FAILURE;
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bRadioIfIndex != OSIX_FALSE)
    {
        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                      pRadioIfGetDB) != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfGetTxPowerLevel : Radio IF DB Access failure \r\n");
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfGetOFDMTable                                  *
 *                                                                           *
 * Description        : Function to get the Radio A channel info from the    *
 *                      Radio DB                                             *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to Radio IF DB with ifIndex  *
 *                                                                           *
 * Output(s)          : pRadioIfGetDB - Channel filled with Radio DB         *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfGetOFDMTable (tRadioIfGetDB * pRadioIfGetDB)
{
    if (pRadioIfGetDB == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfGetOFDMTable : Null Input received \r\n");
        return OSIX_FAILURE;
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bRadioIfIndex != OSIX_FALSE)
    {
        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                      pRadioIfGetDB) != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfGetOFDMLevel : Radio IF DB Access failure \r\n");
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfGetDSSSTable                                  *
 *                                                                           *
 * Description        : Function to get the Radio B channel info from the    *
 *                      Radio DB                                             *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to Radio IF DB with ifIndex  *
 *                                                                           *
 * Output(s)          : pRadioIfGetDB - Channel filled with Radio DB         *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfGetDSSSTable (tRadioIfGetDB * pRadioIfGetDB)
{
    if (pRadioIfGetDB == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfGetDSSSTable : Null Input received \r\n");
        return OSIX_FAILURE;
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bRadioIfIndex != OSIX_FALSE)
    {
        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                      pRadioIfGetDB) != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfGetDSSS : Radio IF DB Access failure \r\n");
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfSetStationConfig                              *
 *                                                                           *
 * Description        : This function updates the Radio Config structure     *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to Radio IF DB with ifIndex  *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfSetStationConfig (tRadioIfGetDB * pRadioIfGetDB)
{
    UINT1               au1RadioName[RADIO_INTF_NAME_LEN];

    MEMSET (au1RadioName, 0, RADIO_INTF_NAME_LEN);

    if (pRadioIfGetDB == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetStationConfig : Null Input received \r\n");
        return OSIX_FAILURE;
    }

    /* Update DB */
    if (WssIfProcessRadioIfDBMsg (WSS_SET_RADIO_IF_DB,
                                  pRadioIfGetDB) != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetStationConfig(Operational Rate): Radio DB "
                     "Update failure \r\n");
        return OSIX_FAILURE;
    }

#ifdef NPAPI_WANTED
    if (pRadioIfGetDB->RadioIfIsGetAllDB.bBeaconPeriod != OSIX_FALSE)
    {
        SPRINTF ((CHR1 *) au1RadioName, "%s%d", RADIO_INTF_NAME,
                 pRadioIfGetDB->RadioIfGetAllDB.u1RadioId - 1);
        if (RadioIfSetBeaconPeriod (pRadioIfGetDB,
                                    (UINT1 *) &au1RadioName) != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfSetOperationTable: (Beacon) HW call to set "
                         "the values failed\r\n");
            return OSIX_FAILURE;
        }
    }
#endif

    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfSetOperationTable                             *
 *                                                                           *
 * Description        : This function updates MAC Parameters in Radio DB     *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to Radio IF DB with ifIndex  *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfSetOperationTable (tRadioIfGetDB * pRadioIfGetDB)
{
    UINT1               au1RadioName[RADIO_INTF_NAME_LEN];
    MEMSET (au1RadioName, 0, RADIO_INTF_NAME_LEN);

    if (pRadioIfGetDB == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetOperationTable : Null Input received \r\n");
        return OSIX_FAILURE;
    }

    /* Update DB */
    if (WssIfProcessRadioIfDBMsg (WSS_SET_RADIO_IF_DB,
                                  pRadioIfGetDB) != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetOperationTable : Radio IF DB Update failure \r\n");
        return OSIX_FAILURE;
    }

#ifdef NPAPI_WANTED
    SPRINTF ((CHR1 *) au1RadioName, "%s%d", RADIO_INTF_NAME,
             pRadioIfGetDB->RadioIfGetAllDB.u1RadioId - 1);
    if (pRadioIfGetDB->RadioIfIsGetAllDB.bFragmentationThreshold != OSIX_FALSE)
    {
        if (RadioIfSetFragmentationThreshold (pRadioIfGetDB,
                                              (UINT1 *) &au1RadioName) !=
            OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfSetOperationTable:(FragmentationThreshold) HW "
                         "call to set the values failed\r\n");
            return OSIX_FAILURE;
        }
    }

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bRTSThreshold != OSIX_FALSE)
    {
        if (RadioIfSetRTSThreshold (pRadioIfGetDB,
                                    (UINT1 *) &au1RadioName) != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfSetOperationTable:(RTSThreshold) HW call to "
                         "set the values failed\r\n");
            return OSIX_FAILURE;
        }
    }

#endif

    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfSetFsDot11nParams                             *
 *                                                                           *
 * Description        : This function updates Dot11n params in Radio DB      *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to Radio IF DB with ifIndex  *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfSetFsDot11nParams (tRadioIfGetDB * pRadioIfGetDB)
{
    if (pRadioIfGetDB == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetFsDot11nParams : Null Input received \r\n");
        return OSIX_FAILURE;
    }

    /* Update DB */
    if (WssIfProcessRadioIfDBMsg (WSS_SET_RADIO_IF_DB,
                                  pRadioIfGetDB) != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetFsDot11nParams : Radio IF DB Update failure \r\n");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfSetTxPowerTable                               *
 *                                                                           *
 * Description        : This function updates Txpower level in Radio DB      *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to Radio IF DB with ifIndex  *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfSetTxPowerTable (tRadioIfGetDB * pRadioIfGetDB)
{
    UINT1               au1RadioName[RADIO_INTF_NAME_LEN];

    MEMSET (au1RadioName, 0, RADIO_INTF_NAME_LEN);

    if (pRadioIfGetDB == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetTxPowerTable : Null Input received \r\n");
        return OSIX_FAILURE;
    }

    /* Update DB */
    if (WssIfProcessRadioIfDBMsg (WSS_SET_RADIO_IF_DB,
                                  pRadioIfGetDB) != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetTxPowerLevel : Radio IF DB Update failure \r\n");
        return OSIX_FAILURE;
    }

#ifdef NPAPI_WANTED
    if (pRadioIfGetDB->RadioIfIsGetAllDB.bTxPowerLevel != OSIX_FALSE)
    {
        SPRINTF ((CHR1 *) au1RadioName, "%s%d", RADIO_INTF_NAME,
                 pRadioIfGetDB->RadioIfGetAllDB.u1RadioId - 1);
        if (RadioIfSetTxPowerLevel (pRadioIfGetDB,
                                    (UINT1 *) &au1RadioName) != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfSetOperationTable:(TxPowerLevel) HW call to "
                         "set the values failed\r\n");
            return OSIX_FAILURE;
        }
    }
#endif

    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfSetDSSSTable                                  *
 *                                                                           *
 * Description        : This function updates Channel for B Radio            *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to Radio IF DB with ifIndex  *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfSetDSSSTable (tRadioIfGetDB * pRadioIfGetDB)
{
    if (pRadioIfGetDB == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetDSSSTable: Null Input received \r\n");
        return OSIX_FAILURE;
    }

    /* Update DB */
    if (WssIfProcessRadioIfDBMsg (WSS_SET_RADIO_IF_DB,
                                  pRadioIfGetDB) != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetDSSSTable: Radio IF DB Update failure \r\n");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfSetOFDMTable                                  *
 *                                                                           *
 * Description        : This function updates Channel for A Radio            *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to Radio IF DB with ifIndex  *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfSetOFDMTable (tRadioIfGetDB * pRadioIfGetDB)
{
    UINT1               au1RadioName[RADIO_INTF_NAME_LEN];

    MEMSET (au1RadioName, 0, RADIO_INTF_NAME_LEN);

    if (pRadioIfGetDB == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetOFDMTable: Null Input received \r\n");
        return OSIX_FAILURE;
    }

    /* Update DB */
    if (WssIfProcessRadioIfDBMsg (WSS_SET_RADIO_IF_DB,
                                  pRadioIfGetDB) != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetOFDMTable: Radio IF DB Update failure \r\n");
        return OSIX_FAILURE;
    }

#ifdef NPAPI_WANTED
    if (pRadioIfGetDB->RadioIfIsGetAllDB.bCurrentChannel != OSIX_FALSE)
    {
        SPRINTF ((CHR1 *) au1RadioName, "%s%d", RADIO_INTF_NAME,
                 pRadioIfGetDB->RadioIfGetAllDB.u1RadioId - 1);
        if (RadioIfSetOFDMCurrentChannel (pRadioIfGetDB,
                                          (UINT1 *) &au1RadioName) !=
            OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfSetOFDMTable:Hw call to set OFDM Channel failed\n");
            return OSIX_FAILURE;
        }
    }
#endif

    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfSetAntennasList                               *
 *                                                                           *
 * Description        : This function updates the antenna params in Radio DB *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to Radio IF DB with ifIndex  *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfSetAntennasList (tRadioIfGetDB * pRadioIfGetDB)
{
    UINT1               au1RadioName[RADIO_INTF_NAME_LEN];

    MEMSET (au1RadioName, 0, RADIO_INTF_NAME_LEN);

    if (pRadioIfGetDB == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetAntennasList: Null Input received \r\n");
        return OSIX_FAILURE;
    }

    /* Update DB */
    if (WssIfProcessRadioIfDBMsg (WSS_SET_RADIO_IF_DB,
                                  pRadioIfGetDB) != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetAntennasList: Radio IF DB Update failure \r\n");
        return OSIX_FAILURE;
    }

#ifdef NPAPI_WANTED
    if (pRadioIfGetDB->RadioIfIsGetAllDB.bDiversitySupport != OSIX_FALSE)
    {
        SPRINTF ((CHR1 *) au1RadioName, "%s%d", RADIO_INTF_NAME,
                 pRadioIfGetDB->RadioIfGetAllDB.u1RadioId - 1);
        if (RadioIfSetAntennaDiversity (pRadioIfGetDB,
                                        (UINT1 *) &au1RadioName) !=
            OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfSetAntennasList: (Antenna Diversity) Hw "
                         "set failed\r\n");
            return OSIX_FAILURE;
        }
    }

#endif

    return OSIX_SUCCESS;
}

UINT1
RadioIfUpdateDecryptReport (tRadioIfMsgStruct * pWssMsgStruct)
{
    tRadioIfGetDB       RadioIfGetDB;
    INT4                i4RadioIfIndex;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    /* Input Parameter Check */
    if (pWssMsgStruct == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfConfigUpdateReq: Null input received\n");
        return OSIX_FAILURE;
    }

    i4RadioIfIndex =
        pWssMsgStruct->unRadioIfMsg.DecryptReportTimer.u1RadioId +
        SYS_DEF_MAX_ENET_INTERFACES;
    RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
        UtlShMemAllocAntennaSelectionBuf ();

    if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC, "\r%% Memory allocation for Antenna"
                     " selection failed.\r\n");
        return OSIX_FAILURE;
    }

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4RadioIfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bReportInterval = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.u2ReportInterval =
        pWssMsgStruct->unRadioIfMsg.DecryptReportTimer.u2ReportInterval;

    if (WssIfProcessRadioIfDBMsg (WSS_SET_RADIO_IF_DB, (&RadioIfGetDB))
        != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfDecryptReport: DB get of RadioIf DB failed \n");
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                         RadioIfGetAllDB.pu1AntennaSelection);
        return OSIX_FAILURE;
    }
    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                     RadioIfGetAllDB.pu1AntennaSelection);
    return OSIX_SUCCESS;

}

/*****************************************************************************
 * Function Name      : RadioIfSetDot11nOperParams                           *
 *                                                                           *
 * Description        : This function constructs the 802.11n Operation       *
 *                      parameters and sends it to WLC Handler and update    *
 *                      the WSSIF DB in case the configuration is            *
 *                      succesfully applied in WTP                           *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to Radio IF DB with ifIndex  *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfSetDot11nOperParams (tRadioIfGetDB * pRadioIfGetDB)
{
    UNUSED_PARAM (pRadioIfGetDB);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfValidateAllowedChannels                       *
 *                                                                           *
 * Description        : This function validates the configured channel       *
 *                                                                           *
 * Input(s)           : u4RadioIfIndex - Radio interface index               *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfValidateAllowedChannels (UINT4 u4RadioIfIndex, UINT1 u1CurrentChannel,
                                UINT1 *pAllowedChannelList)
{
    UNUSED_PARAM (u4RadioIfIndex);
    UNUSED_PARAM (u1CurrentChannel);
    UNUSED_PARAM (pAllowedChannelList);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfSetAllowedChannel                            *
 *                                                                           *
 * Description        : This function sets the first available channel  if   *
 *                      RadioIfValidateAllowedChannels finction is failed    *
 *                                                                           *
 * Input(s)           : u4RadioIfIndex - Radio interface index               *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfSetAllowedChannel (UINT4 u4RadioIfIndex, UINT1 *pAllowedChannelList)
{
    UNUSED_PARAM (u4RadioIfIndex);
    UNUSED_PARAM (pAllowedChannelList);
    return OSIX_SUCCESS;
}

#endif /* __RADIOIFAP_C__ */
