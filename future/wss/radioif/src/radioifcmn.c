/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: radioifcmn.c,v 1.6 2017/11/24 10:37:04 siva Exp $
 *
 * Description: This files contains the interface functions for NPAPI module
 ***********************************************************************/
#ifndef __RADIOIFCMN_C__
#define __RADIOIFCMN_C__

#include "radioifinc.h"
#include "wsspmdb.h"
#include "wsspmprot.h"
#include "wsspm.h"
#include "iss.h"

#ifdef NPAPI_WANTED
/*#include "wasp.h"
#include "lnxwireless.h"*/
extern VOID         WssWlanGetWlanIntfName (UINT1 u1RadioId, UINT1 u1WlanId,
                                            UINT1 *pu1WlanIntfName,
                                            UINT1 *pu1WlanId);

/*****************************************************************************
 * Function Name      : RadioIfGetSupportedRate                              *
 *                                                                           *
 * Description        : Wrapper Func through which the supported rates       *
 *                      will be received from NPAPI                          *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to input structure           *
 *                      pu1RadioName - Radio Interface Name                  *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfGetSupportedRate (tRadioIfGetDB * pRadioIfGetDB, UINT1 *pu1RadioName)
{
    tFsHwNp             FsHwNp;
    UINT1               u1Index = 0;
    UINT1               au1Wname[WLAN_NAME_MAX_LEN];

    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));
    MEMSET (au1Wname, 0, WLAN_NAME_MAX_LEN);

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_GET_BIT_RATES,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetDot11BitRates.RadInfo.
        u4RadioIfIndex = pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;

    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetDot11BitRates.RadInfo.
            au1RadioIfName, pu1RadioName, STRLEN (pu1RadioName));

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetDot11BitRates.RadInfo.
        au2WlanIfIndex[0] = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;
    SPRINTF ((CHR1 *) au1Wname, "%s%d", RADIO_ATH_INTF_NAME,
             pRadioIfGetDB->RadioIfGetAllDB.u1WlanId - 1);
    STRCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetDot11BitRates.RadInfo.
            au1WlanIfname[0], au1Wname);

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetDot11BitRates.RadInfo.
        u1WlanId = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;

    /*To Get from the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "NpUtilHwProgram : Values to get from Hw failed\n");
        return OSIX_FAILURE;
    }
    for (u1Index = 0; u1Index < RADIOIF_OPER_RATE; u1Index++)
    {
        pRadioIfGetDB->RadioIfGetAllDB.au1SupportedRate[u1Index] =
            (FsHwNp.RadioIfNpModInfo.unOpCode.
             sConfigGetDot11BitRates.au4BitRate[u1Index]) /
            RADIO_SUPPORTED_BIT_RATE;
    }
    pRadioIfGetDB->RadioIfIsGetAllDB.bSupportedRate = OSIX_TRUE;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfGetMaxTxPowerLevel2G                          *
 *                                                                           *
 * Description        : Wrapper Func through which the Power level will be   *
 *                      received for Ratio Type b/g/n                        *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to input structure           *
 *                      pu1RadioName - Radio Interface Name                  *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfGetMaxTxPowerLevel2G (tRadioIfGetDB * pRadioIfGetDB, UINT1 *pu1RadioName)
{
    tFsHwNp             FsHwNp;

    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_GET_MAX_TX_POW_2G,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetDot11MaxTxPow2G.RadInfo.
        u4RadioIfIndex = pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetDot11MaxTxPow2G.RadInfo.
            au1RadioIfName, pu1RadioName, STRLEN (pu1RadioName));

    /*To Get from the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "NpUtilHwProgram : Values to get from Hw failed\r\n");
        return OSIX_FAILURE;
    }

    pRadioIfGetDB->RadioIfGetAllDB.u2MaxTxPowerLevel =
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetDot11MaxTxPow2G.u4MaxTxPow;

    pRadioIfGetDB->RadioIfIsGetAllDB.bMaxTxPowerLevel = OSIX_TRUE;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfGetMaxTxPowerLevel2G                          *
 *                                                                           *
 * Description        : Wrapper Func through which the Power level will be   *
 *                      received for Ratio Type a/n                          *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to input structure           *
 *                      pu1RadioName - Radio Interface Name                  *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfGetMaxTxPowerLevel5G (tRadioIfGetDB * pRadioIfGetDB, UINT1 *pu1RadioName)
{
    tFsHwNp             FsHwNp;

    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_GET_MAX_TX_POW_5G,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetDot11MaxTxPow5G.RadInfo.
        u4RadioIfIndex = pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetDot11MaxTxPow5G.RadInfo.
            au1RadioIfName, pu1RadioName, STRLEN (pu1RadioName));

    /*To Get from the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "NpUtilHwProgram : Values to get from Hw failed\r\n");
        return OSIX_FAILURE;
    }

    pRadioIfGetDB->RadioIfGetAllDB.u2MaxTxPowerLevel =
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetDot11MaxTxPow5G.u4MaxTxPow;

    pRadioIfGetDB->RadioIfIsGetAllDB.bMaxTxPowerLevel = OSIX_TRUE;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfGetCountryString                              *
 *                                                                           *
 * Description        : Wrapper Func through which the configured country    *
 *                      will be received from NPAPI                          *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to input structure           *
 *                      pu1RadioName - Radio Interface Name                  *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfGetCountryString (tRadioIfGetDB * pRadioIfGetDB, UINT1 *pu1RadioName)
{
#ifdef NPAPI_WANTED
    tFsHwNp             FsHwNp;

    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_GET_COUNTRY,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetDot11Country.RadInfo.
        u4RadioIfIndex = pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetDot11Country.RadInfo.
            au1RadioIfName, pu1RadioName, STRLEN (pu1RadioName));

    /*To Get from the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "NpUtilHwProgram : Values to get from Hw failed\r\n");
        return OSIX_FAILURE;
    }

    MEMCPY (pRadioIfGetDB->RadioIfGetAllDB.au1CountryString,
            FsHwNp.RadioIfNpModInfo.unOpCode.
            sConfigGetDot11Country.au1ValDot11CountryName,
            STRLEN (FsHwNp.RadioIfNpModInfo.
                    unOpCode.sConfigGetDot11Country.au1ValDot11CountryName));

#endif
    pRadioIfGetDB->RadioIfIsGetAllDB.bCountryString = OSIX_TRUE;

    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfGetMultiDomainInfo                            *
 *                                                                           *
 * Description        : Wrapper Func through which the configured country's  *
 *                      Multi Domain Capability will be received from NPAPI  *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to input structure           *
 *                      pu1RadioName - Radio Interface Name                  *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfGetMultiDomainInfo (tRadioIfGetDB * pRadioIfGetDB, UINT1 *pu1RadioName)
{
#ifdef NPAPI_WANTED
    tFsHwNp             FsHwNp;
    UINT1               au1Wname[WLAN_NAME_MAX_LEN];

    MEMSET (au1Wname, 0, WLAN_NAME_MAX_LEN);
    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_GET_MULTIDOMAIN_INFO,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetMultiDomainInfo.RadInfo.
        u4RadioIfIndex = pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetMultiDomainInfo.RadInfo.
            au1RadioIfName, pu1RadioName, STRLEN (pu1RadioName));

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetMultiDomainInfo.RadInfo.
        au2WlanIfIndex[0] = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;

    WssWlanGetWlanIntfName (pRadioIfGetDB->RadioIfGetAllDB.u1RadioId,
                            pRadioIfGetDB->RadioIfGetAllDB.u1WlanId,
                            FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigGetMultiDomainInfo.RadInfo.au1WlanIfname[0],
                            &FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigGetMultiDomainInfo.RadInfo.u1WlanId);
    /*To Get from the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "NpUtilHwProgram : Values to get from Hw failed\r\n");
        return OSIX_FAILURE;
    }
    MEMCPY (pRadioIfGetDB->RadioIfGetAllDB.MultiDomainInfo,
            FsHwNp.RadioIfNpModInfo.unOpCode.
            sConfigGetMultiDomainInfo.MultiDomainInfo,
            sizeof (FsHwNp.RadioIfNpModInfo.unOpCode.
                    sConfigGetMultiDomainInfo.MultiDomainInfo));
#endif
    pRadioIfGetDB->RadioIfIsGetAllDB.bMultiDomainInfo = OSIX_TRUE;

    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfGetMultiDomainAndAssembleBeacon               *
 *                                                                           *
 * Description        : Wrapper Func through which the configured country's  *
 *                      Multi Domain Capability will be received from NPAPI  *
 *                      and assemble Beacon Frames                           *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to input structure           *
 *                      pu1RadioName - Radio Interface Name                  *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfGetMultiDomainAndAssembleBeacon (tRadioIfGetDB * pRadioIfGetDB,
                                        UINT1 *pu1RadioName)
{
#ifdef NPAPI_WANTED
    tFsHwNp             FsHwNp;
    UINT1               au1Wname[WLAN_NAME_MAX_LEN];

    MEMSET (au1Wname, 0, WLAN_NAME_MAX_LEN);
    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_ASSEMBLE_BEACON_FRAMES,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetMultiDomainInfo.RadInfo.
        u4RadioIfIndex = pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetMultiDomainInfo.RadInfo.
            au1RadioIfName, pu1RadioName, STRLEN (pu1RadioName));

    FsHwNp.RadioIfNpModInfo.unOpCode.sFsHwGetBeaconInterval.RadInfo.
        au2WlanIfIndex[0] = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;

    WssWlanGetWlanIntfName (pRadioIfGetDB->RadioIfGetAllDB.u1RadioId,
                            pRadioIfGetDB->RadioIfGetAllDB.u1WlanId,
                            FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigGetMultiDomainInfo.RadInfo.au1WlanIfname[0],
                            &FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigGetMultiDomainInfo.RadInfo.u1WlanId);
    /*To Get from the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "NpUtilHwProgram : Values to get from Hw failed\r\n");
        return OSIX_FAILURE;
    }

    MEMCPY (pRadioIfGetDB->RadioIfGetAllDB.MultiDomainInfo,
            FsHwNp.RadioIfNpModInfo.unOpCode.
            sConfigGetMultiDomainInfo.MultiDomainInfo,
            sizeof (FsHwNp.RadioIfNpModInfo.unOpCode.
                    sConfigGetMultiDomainInfo.MultiDomainInfo));
#endif
    pRadioIfGetDB->RadioIfIsGetAllDB.bMultiDomainInfo = OSIX_TRUE;

    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfGetScanDetails                                *
 *                                                                           *
 * Description        : Wrapper Func through which the Power level will be   *
 *                      received for Ratio Type b/g/n                        *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to input structure           *
 *                      pu1RadioName - Radio Interface Name                  *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfGetScanDetails (tRadioIfGetDB * pRadioIfGetDB, UINT1 *pu1RadioName)
{
#ifdef NPAPI_WANTED
    tFsHwNp             FsHwNp;

    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_GET_SCAN_DETAILS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetScanDetails.RadInfo.
        u4RadioIfIndex = pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetScanDetails.RadInfo.
        u1WlanId = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;

    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetScanDetails.RadInfo.
            au1RadioIfName, pu1RadioName, STRLEN (pu1RadioName));

    /*To Get from the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "NpUtilHwProgram : Values to get from Hw failed\r\n");
        return OSIX_FAILURE;
    }
#endif
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfGetBeaconPeriod                               *
 *                                                                           *
 * Description        : Wrapper Func through which the beacon interval       *
 *                      value will be received from NPAPI                    *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to input structure           *
 *                      pu1RadioName - Radio Interface Name                  *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfGetBeaconPeriod (tRadioIfGetDB * pRadioIfGetDB, UINT1 *pu1RadioName)
{
    tFsHwNp             FsHwNp;
    UINT1               au1Wname[WLAN_NAME_MAX_LEN];

    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));
    MEMSET (au1Wname, 0, WLAN_NAME_MAX_LEN);

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_GET_BEACON_PERIOD,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    FsHwNp.RadioIfNpModInfo.unOpCode.sFsHwGetBeaconInterval.RadInfo.
        u4RadioIfIndex = pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;

    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sFsHwGetBeaconInterval.RadInfo.
            au1RadioIfName, pu1RadioName, STRLEN (pu1RadioName));

    FsHwNp.RadioIfNpModInfo.unOpCode.sFsHwGetBeaconInterval.RadInfo.
        au2WlanIfIndex[0] = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;

    SPRINTF ((CHR1 *) au1Wname, "%s%d", RADIO_ATH_INTF_NAME,
             pRadioIfGetDB->RadioIfGetAllDB.u1WlanId - 1);

    STRCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sFsHwGetBeaconInterval.RadInfo.
            au1WlanIfname[0], au1Wname);

    FsHwNp.RadioIfNpModInfo.unOpCode.sFsHwGetBeaconInterval.RadInfo.
        u1WlanId = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;

    /*To Get from the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "NpUtilHwProgram : Values to get from Hw failed\r\n");
        return OSIX_FAILURE;
    }

    pRadioIfGetDB->RadioIfGetAllDB.u2BeaconPeriod =
        FsHwNp.RadioIfNpModInfo.unOpCode.sFsHwGetBeaconInterval.
        u4BeaconInterval;
    pRadioIfGetDB->RadioIfIsGetAllDB.bBeaconPeriod = OSIX_TRUE;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfGetDTIMPeriod                                 *
 *                                                                           *
 * Description        : Wrapper Func through which the DTIM Period value     *
 *                      will be received from NPAPI                          *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to input structure           *
 *                      pu1RadioName - Radio Interface Name                  *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfGetDTIMPeriod (tRadioIfGetDB * pRadioIfGetDB, UINT1 *pu1RadioName)
{
    tFsHwNp             FsHwNp;
    UINT1               au1Wname[WLAN_NAME_MAX_LEN];

    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));
    MEMSET (au1Wname, 0, WLAN_NAME_MAX_LEN);

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_GET_DTIM_PERIOD,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetDot11DtimPrd.RadInfo.
        u4RadioIfIndex = pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetDot11DtimPrd.RadInfo.
            au1RadioIfName, pu1RadioName, STRLEN (pu1RadioName));

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetDot11DtimPrd.
        RadInfo.au2WlanIfIndex[0] = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;

    SPRINTF ((CHR1 *) au1Wname, "%s%d", RADIO_ATH_INTF_NAME,
             pRadioIfGetDB->RadioIfGetAllDB.u1WlanId - 1);
    STRCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetDot11DtimPrd.
            RadInfo.au1WlanIfname[0], au1Wname);

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetDot11DtimPrd.RadInfo.
        u1WlanId = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;

    /*To Get from the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "NpUtilHwProgram : Values to get from Hw failed\r\n");
        return OSIX_FAILURE;
    }

    pRadioIfGetDB->RadioIfGetAllDB.u1DTIMPeriod =
        FsHwNp.RadioIfNpModInfo.unOpCode.
        sConfigGetDot11DtimPrd.i4ValDot11DtimPrd;
    pRadioIfGetDB->RadioIfIsGetAllDB.bDTIMPeriod = OSIX_TRUE;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfGetCurrentChannel                             *
 *                                                                           *
 * Description        : Wrapper Func through which the current configured    *
 *                      channel will be received from NPAPI                  *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to input structure           *
 *                      pu1RadioName - Radio Interface Name                  *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfGetCurrentChannel (tRadioIfGetDB * pRadioIfGetDB, UINT1 *pu1RadioName)
{
#ifdef NPAPI_WANTED
    tFsHwNp             FsHwNp;
    UINT1               au1Wname[WLAN_NAME_MAX_LEN];

    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));
    MEMSET (au1Wname, 0, WLAN_NAME_MAX_LEN);

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_GET_CHANNEL_NUM,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetDot11Channel.
        RadInfo.u4RadioIfIndex = pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetDot11Channel.
            RadInfo.au1RadioIfName, pu1RadioName, STRLEN (pu1RadioName));

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetDot11Channel.
        RadInfo.au2WlanIfIndex[0] = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;

    SPRINTF ((CHR1 *) au1Wname, "%s%d", RADIO_ATH_INTF_NAME,
             pRadioIfGetDB->RadioIfGetAllDB.u1WlanId - 1);

    STRCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetDot11Channel.
            RadInfo.au1WlanIfname[0], au1Wname);

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetDot11Channel.RadInfo.
        u1WlanId = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;

    /*To Get from the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "NpUtilHwProgram(Channel) : Values to get from Hw failed\r\n");
        return OSIX_FAILURE;
    }

    pRadioIfGetDB->RadioIfGetAllDB.u1CurrentChannel =
        (UINT1) FsHwNp.RadioIfNpModInfo.unOpCode.
        sConfigGetDot11Channel.u2ChannelNum;
#endif
    pRadioIfGetDB->RadioIfIsGetAllDB.bCurrentChannel = OSIX_TRUE;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfGetCurrentTxPower                             *
 *                                                                           *
 * Description        : Wrapper Func through which the Current TxPower will  *
 *                      be received from NPAPI                               *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to input structure           *
 *                      pu1RadioName - Radio Interface Name                  *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfGetCurrentTxPower (tRadioIfGetDB * pRadioIfGetDB, UINT1 *pu1RadioName)
{
#ifdef NPAPI_WANTED
    tFsHwNp             FsHwNp;
    UINT1               au1Wname[WLAN_NAME_MAX_LEN];

    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));
    MEMSET (au1Wname, 0, sizeof (WLAN_NAME_MAX_LEN));

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_GET_TX_CURR_POWER,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    FsHwNp.RadioIfNpModInfo.unOpCode.sFsHwGetTxCurrPower.
        RadInfo.u4RadioIfIndex = pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sFsHwGetTxCurrPower.
            RadInfo.au1RadioIfName, pu1RadioName, STRLEN (pu1RadioName));

    FsHwNp.RadioIfNpModInfo.unOpCode.sFsHwGetTxCurrPower.
        RadInfo.au2WlanIfIndex[0] = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;

    SPRINTF ((CHR1 *) au1Wname, "%s%d", RADIO_ATH_INTF_NAME,
             pRadioIfGetDB->RadioIfGetAllDB.u1WlanId - 1);

    STRCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sFsHwGetTxCurrPower.
            RadInfo.au1WlanIfname[0], au1Wname);

    FsHwNp.RadioIfNpModInfo.unOpCode.sFsHwGetTxCurrPower.RadInfo.
        u1WlanId = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;

    /*To Get from the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "NpUtilHwProgram: Values to get from Hw failed\n");
        return OSIX_FAILURE;
    }

    pRadioIfGetDB->RadioIfGetAllDB.i2CurrentTxPower =
        (INT2) FsHwNp.RadioIfNpModInfo.unOpCode.
        sFsHwGetTxCurrPower.i4TxCurrPower;
#endif
    pRadioIfGetDB->RadioIfIsGetAllDB.bCurrentTxPower = OSIX_TRUE;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfGetFragmentationThreshold                     *
 *                                                                           *
 * Description        : Wrapper Func through which the fragmentation         *
 *                      threshold value will be returned                     *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to input structure           *
 *                      pu1RadioName - Radio Interface Name                  *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfGetFragmentationThreshold (tRadioIfGetDB * pRadioIfGetDB,
                                  UINT1 *pu1RadioName)
{
    tFsHwNp             FsHwNp;
    UINT1               au1Wname[WLAN_NAME_MAX_LEN];

    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));
    MEMSET (au1Wname, 0, sizeof (WLAN_NAME_MAX_LEN));

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_GET_FRAG_THRESHOLD,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetDot11FragThreshold.RadInfo.
        u4RadioIfIndex = pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.
            sConfigGetDot11FragThreshold.RadInfo.au1RadioIfName, pu1RadioName,
            STRLEN (pu1RadioName));

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetDot11FragThreshold.RadInfo.
        au2WlanIfIndex[0] = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;
    SPRINTF ((CHR1 *) au1Wname, "%s%d", RADIO_ATH_INTF_NAME,
             pRadioIfGetDB->RadioIfGetAllDB.u1WlanId - 1);
    STRCPY (FsHwNp.RadioIfNpModInfo.unOpCode.
            sConfigGetDot11FragThreshold.RadInfo.au1WlanIfname[0], au1Wname);

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetDot11FragThreshold.RadInfo.
        u1WlanId = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;

    /*To Get from the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "NpUtilHwProgram : Values to get from Hw failed\r\n");
        return OSIX_FAILURE;
    }

    pRadioIfGetDB->RadioIfGetAllDB.u2FragmentationThreshold =
        FsHwNp.RadioIfNpModInfo.unOpCode.
        sConfigGetDot11FragThreshold.u2FragThreshold;
    pRadioIfGetDB->RadioIfIsGetAllDB.bFragmentationThreshold = OSIX_TRUE;

    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfGetRTSThreshold                               *
 *                                                                           *
 * Description        : Wrapper Func through which the RTS threshold value   *
 *                      will be returned                                     *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to input structure           *
 *                      pu1RadioName - Radio Interface Name                  *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfGetRTSThreshold (tRadioIfGetDB * pRadioIfGetDB, UINT1 *pu1RadioName)
{
    tFsHwNp             FsHwNp;
    UINT1               au1Wname[WLAN_NAME_MAX_LEN];

    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));
    MEMSET (au1Wname, 0, sizeof (WLAN_NAME_MAX_LEN));

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_GET_RTS_THRESHOLD,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetDot11RTSThreshold.
        RadInfo.u4RadioIfIndex = pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.
            sConfigGetDot11RTSThreshold.RadInfo.au1RadioIfName, pu1RadioName,
            STRLEN (pu1RadioName));

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetDot11RTSThreshold.
        RadInfo.au2WlanIfIndex[0] = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;
    SPRINTF ((CHR1 *) au1Wname, "%s%d", RADIO_ATH_INTF_NAME,
             pRadioIfGetDB->RadioIfGetAllDB.u1WlanId - 1);
    STRCPY (FsHwNp.RadioIfNpModInfo.unOpCode.
            sConfigGetDot11RTSThreshold.RadInfo.au1WlanIfname[0], au1Wname);
    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetDot11RTSThreshold.RadInfo.
        u1WlanId = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;

    /*To Get from the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "NpUtilHwProgram : Values to get from Hw failed\r\n");
        return OSIX_FAILURE;
    }

    pRadioIfGetDB->RadioIfGetAllDB.u2RTSThreshold =
        FsHwNp.RadioIfNpModInfo.unOpCode.
        sConfigGetDot11RTSThreshold.u2RTSThreshold;
    pRadioIfGetDB->RadioIfIsGetAllDB.bRTSThreshold = OSIX_TRUE;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioHwGetSuppRadioType                              *
 *                                                                           *
 * Description        : Wrapper Func through which gets Supported RadioType  *
 *                      will be returned                                     *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to input structure           *
 *                      pu1RadioName - Radio Interface Name                  *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioHwGetSuppRadioType (tRadioIfGetDB * pRadioIfGetDB, UINT1 *pu1RadioName)
{
    tFsHwNp             FsHwNp;
    UINT1               au1Wname[WLAN_NAME_MAX_LEN];

    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));
    MEMSET (au1Wname, 0, sizeof (WLAN_NAME_MAX_LEN));

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_GET_SUPP_RADIO_TYPE,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11SuppRadioType.
        RadInfo.u4RadioIfIndex = pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11SuppRadioType.RadInfo.
            au1RadioIfName, pu1RadioName, STRLEN (pu1RadioName));

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11SuppRadioType.
        RadInfo.au2WlanIfIndex[0] = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;
    SPRINTF ((CHR1 *) au1Wname, "%s%d", RADIO_ATH_INTF_NAME,
             pRadioIfGetDB->RadioIfGetAllDB.u1WlanId - 1);
    STRCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11SuppRadioType.RadInfo.
            au1WlanIfname[0], au1Wname);

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11SuppRadioType.RadInfo.
        u1WlanId = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;

    /*To Get from the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "NpUtilHwProgram : Supp Radio Type  get from Hw failed\r\n");
        return OSIX_FAILURE;
    }
    pRadioIfGetDB->RadioIfGetAllDB.u4SuppRadioType =
        FsHwNp.RadioIfNpModInfo.unOpCode.
        sConfigDot11SuppRadioType.u4SuppRadioType;
    pRadioIfGetDB->RadioIfIsGetAllDB.bSuppRadioType = OSIX_TRUE;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioHwGetDriverHtCapablities                        *
 *                                                                           *
 * Description        : Wrapper Func through which the HT capablities        *
 *                      will be returned                                     *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to input structure           *
 *                      pu1RadioName - Radio Interface Name                  *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioHwGetDriverHtCapablities (tRadioIfGetDB * pRadioIfGetDB,
                               UINT1 *pu1RadioName)
{
    tFsHwNp             FsHwNp;
    UINT1               au1Wname[WLAN_NAME_MAX_LEN];

    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));
    MEMSET (au1Wname, 0, WLAN_NAME_MAX_LEN);

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_GET_DRIVER_HT_CAPABLITIES,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11NCapaParams.
        RadInfo.u4RadioIfIndex = pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11NCapaParams.RadInfo.
            au1RadioIfName, pu1RadioName, STRLEN (pu1RadioName));

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11NCapaParams.
        RadInfo.au2WlanIfIndex[0] = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;
    SPRINTF ((CHR1 *) au1Wname, "%s%d", RADIO_ATH_INTF_NAME,
             pRadioIfGetDB->RadioIfGetAllDB.u1WlanId - 1);
    STRCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11NCapaParams.RadInfo.
            au1WlanIfname[0], au1Wname);

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11NCapaParams.RadInfo.
        u1WlanId = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;

    /*To Get from the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "NpUtilHwProgram : HT Capablities get from Hw failed\r\n");
        return OSIX_FAILURE;
    }
    pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u2SuppHtCapInfo =
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11NCapaParams.u2HtCapInfo;
    pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u2HtCapInfo =
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11NCapaParams.u2HtCapInfo;
    pRadioIfGetDB->RadioIfIsGetAllDB.bSuppHtCapInfo = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bHtCapInfo = OSIX_TRUE;

    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioHwGetDriverExtHtCapabilities                    *
 *                                                                           *
 * Description        : Wrapper Func through which the HT capablities        *
 *                      will be returned                                     *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to input structure           *
 *                      pu1RadioName - Radio Interface Name                  *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioHwGetDriverExtHtCapabilities (tRadioIfGetDB * pRadioIfGetDB,
                                   UINT1 *pu1RadioName)
{
    tFsHwNp             FsHwNp;
    UINT1               au1Wname[WLAN_NAME_MAX_LEN];

    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));
    MEMSET (au1Wname, 0, WLAN_NAME_MAX_LEN);

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_GET_DRIVER_EXT_HT_CAPABLITIES,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11NCapaParams.
        RadInfo.u4RadioIfIndex = pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11NCapaParams.RadInfo.
            au1RadioIfName, pu1RadioName, STRLEN (pu1RadioName));

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11NCapaParams.
        RadInfo.au2WlanIfIndex[0] = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;
    SPRINTF ((CHR1 *) au1Wname, "%s%d", RADIO_ATH_INTF_NAME,
             pRadioIfGetDB->RadioIfGetAllDB.u1WlanId - 1);
    STRCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11NCapaParams.RadInfo.
            au1WlanIfname[0], au1Wname);

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11NCapaParams.RadInfo.
        u1WlanId = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;

    /*To Get from the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "NpUtilHwProgram : HT Capablities get from Hw failed\r\n");
        return OSIX_FAILURE;
    }
    pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.u2SuppHtExtCap =
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11NExtCapaParams.
        u2ExtSuppHtCapInfo;

    pRadioIfGetDB->RadioIfIsGetAllDB.bSuppHtExtCap = OSIX_TRUE;

    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioHwGetDriverBeamformCapability                   *
 *                                                                           *
 * Description        : Wrapper Func through which the HT capablities        *
 *                      will be returned                                     *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to input structure           *
 *                      pu1RadioName - Radio Interface Name                  *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioHwGetDriverBeamformCapability (tRadioIfGetDB * pRadioIfGetDB,
                                    UINT1 *pu1RadioName)
{
    tFsHwNp             FsHwNp;
    UINT1               au1Wname[WLAN_NAME_MAX_LEN];

    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));
    MEMSET (au1Wname, 0, WLAN_NAME_MAX_LEN);

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_GET_DRIVER_BEAMFORM_CAPABLITIES,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11NCapaParams.
        RadInfo.u4RadioIfIndex = pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11NCapaParams.RadInfo.
            au1RadioIfName, pu1RadioName, STRLEN (pu1RadioName));

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11NCapaParams.
        RadInfo.au2WlanIfIndex[0] = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;
    SPRINTF ((CHR1 *) au1Wname, "%s%d", RADIO_ATH_INTF_NAME,
             pRadioIfGetDB->RadioIfGetAllDB.u1WlanId - 1);
    STRCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11NCapaParams.RadInfo.
            au1WlanIfname[0], au1Wname);

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11NCapaParams.RadInfo.
        u1WlanId = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;

    /*To Get from the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "NpUtilHwProgram : HT Capablities get from Hw failed\r\n");
        return OSIX_FAILURE;
    }
    pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
        u4SuppTxBeamCapParam =
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11NBeamFormCapaParams.
        u4SuppBeamFormHtCapInfo;

    pRadioIfGetDB->RadioIfIsGetAllDB.bSuppTxBeamCapParam = OSIX_TRUE;

    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioHwGetDriverHtSuppMcs                           *
 *                                                                           *
 * Description        : Wrapper Func through which the Support MCS           *
 *                      will be returned                                     *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to input structure           *
 *                      pu1RadioName - Radio Interface Name                  *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioHwGetDriverHtSuppMcs (tRadioIfGetDB * pRadioIfGetDB, UINT1 *pu1RadioName)
{
    tFsHwNp             FsHwNp;
    UINT1               au1Wname[WLAN_NAME_MAX_LEN];

    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));
    MEMSET (au1Wname, 0, WLAN_NAME_MAX_LEN);

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_GET_DRIVER_HT_SUPP_MCS,    /* Function/Opode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11NsuppMcsParams.
        RadInfo.u4RadioIfIndex = pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11NsuppMcsParams.RadInfo.
            au1RadioIfName, pu1RadioName, STRLEN (pu1RadioName));

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11NsuppMcsParams.
        RadInfo.au2WlanIfIndex[0] = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;
    SPRINTF ((CHR1 *) au1Wname, "%s%d", RADIO_ATH_INTF_NAME,
             pRadioIfGetDB->RadioIfGetAllDB.u1WlanId - 1);
    STRCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11NsuppMcsParams.RadInfo.
            au1WlanIfname[0], au1Wname);

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11NsuppMcsParams.RadInfo.
        u1WlanId = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;

    /*To Get from the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "NpUtilHwProgram : HT Capablities  get from Hw failed\r\n");
        return OSIX_FAILURE;
    }

    MEMCPY (pRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.au1SuppHtCapaMcs,
            FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11NsuppMcsParams.
            au1SuppHtCapaMcs,
            sizeof (pRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                    au1SuppHtCapaMcs));

    pRadioIfGetDB->RadioIfIsGetAllDB.bSuppHtCapaMcs = OSIX_TRUE;

    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioHwGetDriverHtAMPDU                              *
 *                                                                           *
 * Description        : Wrapper Func to get details about AMPDU details      *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to input structure           *
 *                      pu1RadioName - Radio Interface Name                  *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioHwGetDriverHtAMPDU (tRadioIfGetDB * pRadioIfGetDB, UINT1 *pu1RadioName)
{
    tFsHwNp             FsHwNp;
    UINT1               au1Wname[WLAN_NAME_MAX_LEN];
    UINT1               u1retVal;

    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));
    MEMSET (au1Wname, 0, WLAN_NAME_MAX_LEN);

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_GET_DRIVER_HT_AMPDU,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11NampduParams.
        RadInfo.u4RadioIfIndex = pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11NampduParams.RadInfo.
            au1RadioIfName, pu1RadioName, STRLEN (pu1RadioName));

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11NampduParams.
        RadInfo.au2WlanIfIndex[0] = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;
    SPRINTF ((CHR1 *) au1Wname, "%s%d", RADIO_ATH_INTF_NAME,
             pRadioIfGetDB->RadioIfGetAllDB.u1WlanId - 1);
    STRCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11NampduParams.RadInfo.
            au1WlanIfname[0], au1Wname);

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11NampduParams.
        RadInfo.u1WlanId = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;

    /*To Get from the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "NpUtilHwProgram : ht Capablities get from Hw failed\r\n");
        return OSIX_FAILURE;
    }
    pRadioIfGetDB->RadioIfGetAllDB.Dot11NampduParams.u1SuppMaxAmpduLen =
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11NampduParams.
        u1SuppMaxAmpduLen;
    pRadioIfGetDB->RadioIfGetAllDB.Dot11NampduParams.u1SuppMinAmpduStart =
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11NampduParams.
        u1SuppMinAmpduStart;
    pRadioIfGetDB->RadioIfIsGetAllDB.bSuppMinAmpduStart = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bSuppMaxAmpduLen = OSIX_TRUE;

    u1retVal =
        WssIfProcessRadioIfDBMsg (WSS_ASSEMBLE_11N_AMPDU_INFO, pRadioIfGetDB);
    if (u1retVal == OSIX_FAILURE)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "NpUtilHwProgram : ht Capablities set radioDB failed\r\n");
        return OSIX_FAILURE;

    }
    pRadioIfGetDB->RadioIfIsGetAllDB.bSuppAmpduParam = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bAmpduParam = OSIX_TRUE;

    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioHwGetDriverVhtCapablities                       *
 *                                                                           *
 * Description        : Wrapper Func through which the vht capablities       *
 *                      will be returned                                     *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to input structure           *
 *                      pu1RadioName - Radio Interface Name                  *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioHwGetDriverVhtCapablities (tRadioIfGetDB * pRadioIfGetDB,
                                UINT1 *pu1RadioName)
{
    tFsHwNp             FsHwNp;
    UINT1               au1Wname[WLAN_NAME_MAX_LEN];

    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));
    MEMSET (au1Wname, 0, sizeof (WLAN_NAME_MAX_LEN));

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_GET_DRIVER_VHT_CAPABLITIES,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.
        RadInfo.u4RadioIfIndex = pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.RadInfo.
            au1RadioIfName, pu1RadioName, STRLEN (pu1RadioName));

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.
        RadInfo.au2WlanIfIndex[0] = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;
    SPRINTF ((CHR1 *) au1Wname, "%s%d", RADIO_ATH_INTF_NAME,
             pRadioIfGetDB->RadioIfGetAllDB.u1WlanId - 1);
    STRCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.RadInfo.
            au1WlanIfname[0], au1Wname);

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.RadInfo.
        u1WlanId = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;

    /*To Get from the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "NpUtilHwProgram : vht Capablities  get from Hw failed\r\n");
        return OSIX_FAILURE;
    }
    pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u4SuppVhtCapInfo =
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.u4VhtCapInfo;
    pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u4VhtCapInfo =
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.u4VhtCapInfo;
    pRadioIfGetDB->RadioIfGetAllDB.Dot11AcOperParams.u1VhtChannelWidth =
        RADIO_11AC_CHANWIDTH80;
    pRadioIfGetDB->RadioIfGetAllDB.u1ChannelWidth = RADIO_11N_CHANWIDTH40;
    pRadioIfGetDB->RadioIfIsGetAllDB.bSuppVhtCapInfo = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bVhtCapInfo = OSIX_TRUE;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioHwGetDriverMaxMPDULen                           *
 *                                                                           *
 * Description        : Wrapper Func through which the Max MPDU Len          *
 *                      will be returned                                     *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to input structure           *
 *                      pu1RadioName - Radio Interface Name                  *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioHwGetDriverMaxMPDULen (tRadioIfGetDB * pRadioIfGetDB, UINT1 *pu1RadioName)
{
    tFsHwNp             FsHwNp;
    UINT1               au1Wname[WLAN_NAME_MAX_LEN];

    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));
    MEMSET (au1Wname, 0, sizeof (WLAN_NAME_MAX_LEN));

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_GET_DRIVER_MAX_MPDU_LEN,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.
        RadInfo.u4RadioIfIndex = pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.RadInfo.
            au1RadioIfName, pu1RadioName, STRLEN (pu1RadioName));

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.
        RadInfo.au2WlanIfIndex[0] = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;
    SPRINTF ((CHR1 *) au1Wname, "%s%d", RADIO_ATH_INTF_NAME,
             pRadioIfGetDB->RadioIfGetAllDB.u1WlanId - 1);
    STRCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.RadInfo.
            au1WlanIfname[0], au1Wname);

    /*To Get from the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "NpUtilHwProgram : vht Capablities  get from Hw failed\r\n");
        return OSIX_FAILURE;
    }
    pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1SuppMaxMPDULen =
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.u1MaxMPDULen;
    pRadioIfGetDB->RadioIfIsGetAllDB.bSuppMaxMPDULen = OSIX_TRUE;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioHwGetDriverRxLdpc                               *
 *                                                                           *
 * Description        : Wrapper Func through which the Max MPDU Len          *
 *                      will be returned                                     *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to input structure           *
 *                      pu1RadioName - Radio Interface Name                  *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioHwGetDriverRxLdpc (tRadioIfGetDB * pRadioIfGetDB, UINT1 *pu1RadioName)
{
    tFsHwNp             FsHwNp;
    UINT1               au1Wname[WLAN_NAME_MAX_LEN];

    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));
    MEMSET (au1Wname, 0, sizeof (WLAN_NAME_MAX_LEN));

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_GET_DRIVER_RX_LDPC,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.
        RadInfo.u4RadioIfIndex = pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.RadInfo.
            au1RadioIfName, pu1RadioName, STRLEN (pu1RadioName));

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.
        RadInfo.au2WlanIfIndex[0] = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;
    SPRINTF ((CHR1 *) au1Wname, "%s%d", RADIO_ATH_INTF_NAME,
             pRadioIfGetDB->RadioIfGetAllDB.u1WlanId - 1);
    STRCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.RadInfo.
            au1WlanIfname[0], au1Wname);

    /*To Get from the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "NpUtilHwProgram : vht Capablities  get from Hw failed\r\n");
        return OSIX_FAILURE;
    }
    pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1SuppRxLpdc =
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.u1RxLpdc;
    pRadioIfGetDB->RadioIfIsGetAllDB.bSuppRxLpdc = OSIX_TRUE;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioHwGetDriverShortGi80                            *
 *                                                                           *
 * Description        : Wrapper Func through which the Max MPDU Len          *
 *                      will be returned                                     *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to input structure           *
 *                      pu1RadioName - Radio Interface Name                  *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioHwGetDriverShortGi80 (tRadioIfGetDB * pRadioIfGetDB, UINT1 *pu1RadioName)
{
    tFsHwNp             FsHwNp;
    UINT1               au1Wname[WLAN_NAME_MAX_LEN];

    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));
    MEMSET (au1Wname, 0, sizeof (WLAN_NAME_MAX_LEN));

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_GET_DRIVER_SHORT_GI_80,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.
        RadInfo.u4RadioIfIndex = pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.RadInfo.
            au1RadioIfName, pu1RadioName, STRLEN (pu1RadioName));

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.
        RadInfo.au2WlanIfIndex[0] = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;
    SPRINTF ((CHR1 *) au1Wname, "%s%d", RADIO_ATH_INTF_NAME,
             pRadioIfGetDB->RadioIfGetAllDB.u1WlanId - 1);
    STRCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.RadInfo.
            au1WlanIfname[0], au1Wname);

    /*To Get from the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "NpUtilHwProgram : vht Capablities  get from Hw failed\r\n");
        return OSIX_FAILURE;
    }
    pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1SuppShortGi80 =
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.u1ShortGi80;
    pRadioIfGetDB->RadioIfIsGetAllDB.bSuppShortGi80 = OSIX_TRUE;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioHwGetDriverShortGi160                           *
 *                                                                           *
 * Description        : Wrapper Func through which the Max MPDU Len          *
 *                      will be returned                                     *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to input structure           *
 *                      pu1RadioName - Radio Interface Name                  *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioHwGetDriverShortGi160 (tRadioIfGetDB * pRadioIfGetDB, UINT1 *pu1RadioName)
{
    tFsHwNp             FsHwNp;
    UINT1               au1Wname[WLAN_NAME_MAX_LEN];

    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));
    MEMSET (au1Wname, 0, sizeof (WLAN_NAME_MAX_LEN));

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_GET_DRIVER_SHORT_GI_160,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.
        RadInfo.u4RadioIfIndex = pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.RadInfo.
            au1RadioIfName, pu1RadioName, STRLEN (pu1RadioName));

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.
        RadInfo.au2WlanIfIndex[0] = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;
    SPRINTF ((CHR1 *) au1Wname, "%s%d", RADIO_ATH_INTF_NAME,
             pRadioIfGetDB->RadioIfGetAllDB.u1WlanId - 1);
    STRCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.RadInfo.
            au1WlanIfname[0], au1Wname);

    /*To Get from the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "NpUtilHwProgram : vht Capablities  get from Hw failed\r\n");
        return OSIX_FAILURE;
    }
    pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1ShortGi160 =
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.u1ShortGi160;
    pRadioIfGetDB->RadioIfIsGetAllDB.bShortGi160 = OSIX_TRUE;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioHwGetDriverTxStbc                               *
 *                                                                           *
 * Description        : Wrapper Func through which the Max MPDU Len          *
 *                      will be returned                                     *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to input structure           *
 *                      pu1RadioName - Radio Interface Name                  *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioHwGetDriverTxStbc (tRadioIfGetDB * pRadioIfGetDB, UINT1 *pu1RadioName)
{
    tFsHwNp             FsHwNp;
    UINT1               au1Wname[WLAN_NAME_MAX_LEN];

    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));
    MEMSET (au1Wname, 0, sizeof (WLAN_NAME_MAX_LEN));

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_GET_DRIVER_TX_STBC,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.
        RadInfo.u4RadioIfIndex = pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.RadInfo.
            au1RadioIfName, pu1RadioName, STRLEN (pu1RadioName));

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.
        RadInfo.au2WlanIfIndex[0] = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;
    SPRINTF ((CHR1 *) au1Wname, "%s%d", RADIO_ATH_INTF_NAME,
             pRadioIfGetDB->RadioIfGetAllDB.u1WlanId - 1);
    STRCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.RadInfo.
            au1WlanIfname[0], au1Wname);

    /*To Get from the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "NpUtilHwProgram : vht Capablities  get from Hw failed\r\n");
        return OSIX_FAILURE;
    }
    pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1TxStbc =
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.u1TxStbc;
    pRadioIfGetDB->RadioIfIsGetAllDB.bTxStbc = OSIX_TRUE;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioHwGetDriverRxStbc                               *
 *                                                                           *
 * Description        : Wrapper Func through which the Max MPDU Len          *
 *                      will be returned                                     *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to input structure           *
 *                      pu1RadioName - Radio Interface Name                  *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioHwGetDriverRxStbc (tRadioIfGetDB * pRadioIfGetDB, UINT1 *pu1RadioName)
{
    tFsHwNp             FsHwNp;
    UINT1               au1Wname[WLAN_NAME_MAX_LEN];

    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));
    MEMSET (au1Wname, 0, sizeof (WLAN_NAME_MAX_LEN));

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_GET_DRIVER_RX_STBC,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.
        RadInfo.u4RadioIfIndex = pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.RadInfo.
            au1RadioIfName, pu1RadioName, STRLEN (pu1RadioName));

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.
        RadInfo.au2WlanIfIndex[0] = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;
    SPRINTF ((CHR1 *) au1Wname, "%s%d", RADIO_ATH_INTF_NAME,
             pRadioIfGetDB->RadioIfGetAllDB.u1WlanId - 1);
    STRCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.RadInfo.
            au1WlanIfname[0], au1Wname);

    /*To Get from the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "NpUtilHwProgram : vht Capablities  get from Hw failed\r\n");
        return OSIX_FAILURE;
    }
    pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1RxStbc =
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.u1RxStbc;
    pRadioIfGetDB->RadioIfIsGetAllDB.bRxStbc = OSIX_TRUE;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioHwGetDriverSuBeamFormer                         *
 *                                                                           *
 * Description        : Wrapper Func through which the Max MPDU Len          *
 *                      will be returned                                     *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to input structure           *
 *                      pu1RadioName - Radio Interface Name                  *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioHwGetDriverSuBeamFormer (tRadioIfGetDB * pRadioIfGetDB,
                              UINT1 *pu1RadioName)
{
    tFsHwNp             FsHwNp;
    UINT1               au1Wname[WLAN_NAME_MAX_LEN];

    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));
    MEMSET (au1Wname, 0, sizeof (WLAN_NAME_MAX_LEN));

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_GET_DRIVER_SU_BEAMFORMER,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.
        RadInfo.u4RadioIfIndex = pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.RadInfo.
            au1RadioIfName, pu1RadioName, STRLEN (pu1RadioName));

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.
        RadInfo.au2WlanIfIndex[0] = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;
    SPRINTF ((CHR1 *) au1Wname, "%s%d", RADIO_ATH_INTF_NAME,
             pRadioIfGetDB->RadioIfGetAllDB.u1WlanId - 1);
    STRCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.RadInfo.
            au1WlanIfname[0], au1Wname);

    /*To Get from the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "NpUtilHwProgram : vht Capablities  get from Hw failed\r\n");
        return OSIX_FAILURE;
    }
    pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1SuBeamFormer =
        FsHwNp.RadioIfNpModInfo.unOpCode.
        sConfigDot11AcCapaParams.u1SuBeamFormer;
    pRadioIfGetDB->RadioIfIsGetAllDB.bSuBeamFormer = OSIX_TRUE;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioHwGetDriverSuBeamFormee                         *
 *                                                                           *
 * Description        : Wrapper Func through which the Max MPDU Len          *
 *                      will be returned                                     *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to input structure           *
 *                      pu1RadioName - Radio Interface Name                  *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioHwGetDriverSuBeamFormee (tRadioIfGetDB * pRadioIfGetDB,
                              UINT1 *pu1RadioName)
{
    tFsHwNp             FsHwNp;
    UINT1               au1Wname[WLAN_NAME_MAX_LEN];

    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));
    MEMSET (au1Wname, 0, sizeof (WLAN_NAME_MAX_LEN));

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_GET_DRIVER_SU_BEAMFORMEE,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.
        RadInfo.u4RadioIfIndex = pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.RadInfo.
            au1RadioIfName, pu1RadioName, STRLEN (pu1RadioName));

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.
        RadInfo.au2WlanIfIndex[0] = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;
    SPRINTF ((CHR1 *) au1Wname, "%s%d", RADIO_ATH_INTF_NAME,
             pRadioIfGetDB->RadioIfGetAllDB.u1WlanId - 1);
    STRCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.RadInfo.
            au1WlanIfname[0], au1Wname);

    /*To Get from the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "NpUtilHwProgram : vht Capablities  get from Hw failed\r\n");
        return OSIX_FAILURE;
    }
    pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1SuBeamFormee =
        FsHwNp.RadioIfNpModInfo.unOpCode.
        sConfigDot11AcCapaParams.u1SuBeamFormee;
    pRadioIfGetDB->RadioIfIsGetAllDB.bSuBeamFormee = OSIX_TRUE;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioHwGetDriverSuppBeamFormAnt                      *
 *                                                                           *
 * Description        : Wrapper Func through which the Max MPDU Len          *
 *                      will be returned                                     *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to input structure           *
 *                      pu1RadioName - Radio Interface Name                  *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioHwGetDriverSuppBeamFormAnt (tRadioIfGetDB * pRadioIfGetDB,
                                 UINT1 *pu1RadioName)
{
    tFsHwNp             FsHwNp;
    UINT1               au1Wname[WLAN_NAME_MAX_LEN];

    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));
    MEMSET (au1Wname, 0, sizeof (WLAN_NAME_MAX_LEN));

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_GET_DRIVER_SUPP_BEAM_FROM_ANT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.
        RadInfo.u4RadioIfIndex = pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.RadInfo.
            au1RadioIfName, pu1RadioName, STRLEN (pu1RadioName));

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.
        RadInfo.au2WlanIfIndex[0] = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;
    SPRINTF ((CHR1 *) au1Wname, "%s%d", RADIO_ATH_INTF_NAME,
             pRadioIfGetDB->RadioIfGetAllDB.u1WlanId - 1);
    STRCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.RadInfo.
            au1WlanIfname[0], au1Wname);

    /*To Get from the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "NpUtilHwProgram : vht Capablities  get from Hw failed\r\n");
        return OSIX_FAILURE;
    }
    pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1SuppBeamFormAnt =
        FsHwNp.RadioIfNpModInfo.unOpCode.
        sConfigDot11AcCapaParams.u1SuppBeamFormAnt;
    pRadioIfGetDB->RadioIfIsGetAllDB.bSuppBeamFormAnt = OSIX_TRUE;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioHwGetDriverSoundDimension                       *
 *                                                                           *
 * Description        : Wrapper Func through which the Max MPDU Len          *
 *                      will be returned                                     *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to input structure           *
 *                      pu1RadioName - Radio Interface Name                  *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioHwGetDriverSoundDimension (tRadioIfGetDB * pRadioIfGetDB,
                                UINT1 *pu1RadioName)
{
    tFsHwNp             FsHwNp;
    UINT1               au1Wname[WLAN_NAME_MAX_LEN];

    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));
    MEMSET (au1Wname, 0, sizeof (WLAN_NAME_MAX_LEN));

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_GET_DRIVER_SOUND_DIMENSION,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.
        RadInfo.u4RadioIfIndex = pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.RadInfo.
            au1RadioIfName, pu1RadioName, STRLEN (pu1RadioName));

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.
        RadInfo.au2WlanIfIndex[0] = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;
    SPRINTF ((CHR1 *) au1Wname, "%s%d", RADIO_ATH_INTF_NAME,
             pRadioIfGetDB->RadioIfGetAllDB.u1WlanId - 1);
    STRCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.RadInfo.
            au1WlanIfname[0], au1Wname);

    /*To Get from the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "NpUtilHwProgram : vht Capablities  get from Hw failed\r\n");
        return OSIX_FAILURE;
    }
    pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1SoundDimension =
        FsHwNp.RadioIfNpModInfo.unOpCode.
        sConfigDot11AcCapaParams.u1SoundDimension;
    pRadioIfGetDB->RadioIfIsGetAllDB.bSoundDimension = OSIX_TRUE;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioHwGetDriverMuBeamFormer                         *
 *                                                                           *
 * Description        : Wrapper Func through which the Max MPDU Len          *
 *                      will be returned                                     *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to input structure           *
 *                      pu1RadioName - Radio Interface Name                  *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioHwGetDriverMuBeamFormer (tRadioIfGetDB * pRadioIfGetDB,
                              UINT1 *pu1RadioName)
{
    tFsHwNp             FsHwNp;
    UINT1               au1Wname[WLAN_NAME_MAX_LEN];

    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));
    MEMSET (au1Wname, 0, sizeof (WLAN_NAME_MAX_LEN));

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_GET_DRIVER_MU_BEAMFORMER,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.
        RadInfo.u4RadioIfIndex = pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.RadInfo.
            au1RadioIfName, pu1RadioName, STRLEN (pu1RadioName));

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.
        RadInfo.au2WlanIfIndex[0] = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;
    SPRINTF ((CHR1 *) au1Wname, "%s%d", RADIO_ATH_INTF_NAME,
             pRadioIfGetDB->RadioIfGetAllDB.u1WlanId - 1);
    STRCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.RadInfo.
            au1WlanIfname[0], au1Wname);

    /*To Get from the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "NpUtilHwProgram : vht Capablities  get from Hw failed\r\n");
        return OSIX_FAILURE;
    }
    pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1MuBeamFormer =
        FsHwNp.RadioIfNpModInfo.unOpCode.
        sConfigDot11AcCapaParams.u1MuBeamFormer;
    pRadioIfGetDB->RadioIfIsGetAllDB.bMuBeamFormer = OSIX_TRUE;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioHwGetDriverMuBeamFormee                         *
 *                                                                           *
 * Description        : Wrapper Func through which the Max MPDU Len          *
 *                      will be returned                                     *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to input structure           *
 *                      pu1RadioName - Radio Interface Name                  *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioHwGetDriverMuBeamFormee (tRadioIfGetDB * pRadioIfGetDB,
                              UINT1 *pu1RadioName)
{
    tFsHwNp             FsHwNp;
    UINT1               au1Wname[WLAN_NAME_MAX_LEN];

    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));
    MEMSET (au1Wname, 0, sizeof (WLAN_NAME_MAX_LEN));

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_GET_DRIVER_MU_BEAMFORMEE,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.
        RadInfo.u4RadioIfIndex = pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.RadInfo.
            au1RadioIfName, pu1RadioName, STRLEN (pu1RadioName));

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.
        RadInfo.au2WlanIfIndex[0] = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;
    SPRINTF ((CHR1 *) au1Wname, "%s%d", RADIO_ATH_INTF_NAME,
             pRadioIfGetDB->RadioIfGetAllDB.u1WlanId - 1);
    STRCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.RadInfo.
            au1WlanIfname[0], au1Wname);

    /*To Get from the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "NpUtilHwProgram : vht Capablities  get from Hw failed\r\n");
        return OSIX_FAILURE;
    }
    pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1MuBeamFormee =
        FsHwNp.RadioIfNpModInfo.unOpCode.
        sConfigDot11AcCapaParams.u1MuBeamFormee;
    pRadioIfGetDB->RadioIfIsGetAllDB.bMuBeamFormee = OSIX_TRUE;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioHwGetDriverVhtTxOpPs                            *
 *                                                                           *
 * Description        : Wrapper Func through which the Max MPDU Len          *
 *                      will be returned                                     *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to input structure           *
 *                      pu1RadioName - Radio Interface Name                  *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioHwGetDriverVhtTxOpPs (tRadioIfGetDB * pRadioIfGetDB, UINT1 *pu1RadioName)
{
    tFsHwNp             FsHwNp;
    UINT1               au1Wname[WLAN_NAME_MAX_LEN];

    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));
    MEMSET (au1Wname, 0, sizeof (WLAN_NAME_MAX_LEN));

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_GET_DRIVER_VHT_TX_OP_PS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.
        RadInfo.u4RadioIfIndex = pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.RadInfo.
            au1RadioIfName, pu1RadioName, STRLEN (pu1RadioName));

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.
        RadInfo.au2WlanIfIndex[0] = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;
    SPRINTF ((CHR1 *) au1Wname, "%s%d", RADIO_ATH_INTF_NAME,
             pRadioIfGetDB->RadioIfGetAllDB.u1WlanId - 1);
    STRCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.RadInfo.
            au1WlanIfname[0], au1Wname);

    /*To Get from the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "NpUtilHwProgram : vht Capablities  get from Hw failed\r\n");
        return OSIX_FAILURE;
    }
    pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1VhtTxOpPs =
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.u1VhtTxOpPs;
    pRadioIfGetDB->RadioIfIsGetAllDB.bVhtTxOpPs = OSIX_TRUE;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioHwGetDriverHtcVhtCapable                        *
 *                                                                           *
 * Description        : Wrapper Func through which the Max MPDU Len          *
 *                      will be returned                                     *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to input structure           *
 *                      pu1RadioName - Radio Interface Name                  *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioHwGetDriverHtcVhtCapable (tRadioIfGetDB * pRadioIfGetDB,
                               UINT1 *pu1RadioName)
{
    tFsHwNp             FsHwNp;
    UINT1               au1Wname[WLAN_NAME_MAX_LEN];

    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));
    MEMSET (au1Wname, 0, sizeof (WLAN_NAME_MAX_LEN));

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_GET_DRIVER_HTC_VHTCAPABLE,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.
        RadInfo.u4RadioIfIndex = pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.RadInfo.
            au1RadioIfName, pu1RadioName, STRLEN (pu1RadioName));

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.
        RadInfo.au2WlanIfIndex[0] = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;
    SPRINTF ((CHR1 *) au1Wname, "%s%d", RADIO_ATH_INTF_NAME,
             pRadioIfGetDB->RadioIfGetAllDB.u1WlanId - 1);
    STRCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.RadInfo.
            au1WlanIfname[0], au1Wname);

    /*To Get from the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "NpUtilHwProgram : vht Capablities  get from Hw failed\r\n");
        return OSIX_FAILURE;
    }
    pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1HtcVhtCapable =
        FsHwNp.RadioIfNpModInfo.unOpCode.
        sConfigDot11AcCapaParams.u1HtcVhtCapable;
    pRadioIfGetDB->RadioIfIsGetAllDB.bHtcVhtCapable = OSIX_TRUE;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioHwGetDriverVhtMaxAMPDU                          *
 *                                                                           *
 * Description        : Wrapper Func through which the Max MPDU Len          *
 *                      will be returned                                     *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to input structure           *
 *                      pu1RadioName - Radio Interface Name                  *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioHwGetDriverVhtMaxAMPDU (tRadioIfGetDB * pRadioIfGetDB, UINT1 *pu1RadioName)
{
    tFsHwNp             FsHwNp;
    UINT1               au1Wname[WLAN_NAME_MAX_LEN];

    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));
    MEMSET (au1Wname, 0, sizeof (WLAN_NAME_MAX_LEN));

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_GET_DRIVER_VHT_MAXAMPDU,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.
        RadInfo.u4RadioIfIndex = pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.RadInfo.
            au1RadioIfName, pu1RadioName, STRLEN (pu1RadioName));

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.
        RadInfo.au2WlanIfIndex[0] = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;
    SPRINTF ((CHR1 *) au1Wname, "%s%d", RADIO_ATH_INTF_NAME,
             pRadioIfGetDB->RadioIfGetAllDB.u1WlanId - 1);
    STRCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.RadInfo.
            au1WlanIfname[0], au1Wname);

    /*To Get from the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "NpUtilHwProgram : vht Capablities  get from Hw failed\r\n");
        return OSIX_FAILURE;
    }
    pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1SuppVhtMaxAMPDU =
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.u1VhtMaxAMPDU;
    pRadioIfGetDB->RadioIfIsGetAllDB.bSuppVhtMaxAMPDU = OSIX_TRUE;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioHwGetDriverVhtLinkAdaption                      *
 *                                                                           *
 * Description        : Wrapper Func through which the Max MPDU Len          *
 *                      will be returned                                     *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to input structure           *
 *                      pu1RadioName - Radio Interface Name                  *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioHwGetDriverVhtLinkAdaption (tRadioIfGetDB * pRadioIfGetDB,
                                 UINT1 *pu1RadioName)
{
    tFsHwNp             FsHwNp;
    UINT1               au1Wname[WLAN_NAME_MAX_LEN];

    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));
    MEMSET (au1Wname, 0, sizeof (WLAN_NAME_MAX_LEN));

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_GET_DRIVER_VHT_LINK_ADAPTION,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.
        RadInfo.u4RadioIfIndex = pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.RadInfo.
            au1RadioIfName, pu1RadioName, STRLEN (pu1RadioName));

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.
        RadInfo.au2WlanIfIndex[0] = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;
    SPRINTF ((CHR1 *) au1Wname, "%s%d", RADIO_ATH_INTF_NAME,
             pRadioIfGetDB->RadioIfGetAllDB.u1WlanId - 1);
    STRCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.RadInfo.
            au1WlanIfname[0], au1Wname);

    /*To Get from the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "NpUtilHwProgram : vht Capablities  get from Hw failed\r\n");
        return OSIX_FAILURE;
    }
    pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1VhtLinkAdaption =
        FsHwNp.RadioIfNpModInfo.unOpCode.
        sConfigDot11AcCapaParams.u1VhtLinkAdaption;
    pRadioIfGetDB->RadioIfIsGetAllDB.bVhtLinkAdaption = OSIX_TRUE;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioHwGetDriverVhtRxAntenna                         *
 *                                                                           *
 * Description        : Wrapper Func through which the Max MPDU Len          *
 *                      will be returned                                     *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to input structure           *
 *                      pu1RadioName - Radio Interface Name                  *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioHwGetDriverVhtRxAntenna (tRadioIfGetDB * pRadioIfGetDB,
                              UINT1 *pu1RadioName)
{
    tFsHwNp             FsHwNp;
    UINT1               au1Wname[WLAN_NAME_MAX_LEN];

    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));
    MEMSET (au1Wname, 0, sizeof (WLAN_NAME_MAX_LEN));

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_GET_DRIVER_VHT_RX_ANTENNA,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.
        RadInfo.u4RadioIfIndex = pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.RadInfo.
            au1RadioIfName, pu1RadioName, STRLEN (pu1RadioName));

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.
        RadInfo.au2WlanIfIndex[0] = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;
    SPRINTF ((CHR1 *) au1Wname, "%s%d", RADIO_ATH_INTF_NAME,
             pRadioIfGetDB->RadioIfGetAllDB.u1WlanId - 1);
    STRCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.RadInfo.
            au1WlanIfname[0], au1Wname);

    /*To Get from the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "NpUtilHwProgram : vht Capablities  get from Hw failed\r\n");
        return OSIX_FAILURE;
    }
    pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1VhtRxAntenna =
        FsHwNp.RadioIfNpModInfo.unOpCode.
        sConfigDot11AcCapaParams.u1VhtRxAntenna;
    pRadioIfGetDB->RadioIfIsGetAllDB.bVhtRxAntenna = OSIX_TRUE;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioHwGetDriverVhtTxAntenna                         *
 *                                                                           *
 * Description        : Wrapper Func through which the Max MPDU Len          *
 *                      will be returned                                     *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to input structure           *
 *                      pu1RadioName - Radio Interface Name                  *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioHwGetDriverVhtTxAntenna (tRadioIfGetDB * pRadioIfGetDB,
                              UINT1 *pu1RadioName)
{
    tFsHwNp             FsHwNp;
    UINT1               au1Wname[WLAN_NAME_MAX_LEN];

    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));
    MEMSET (au1Wname, 0, sizeof (WLAN_NAME_MAX_LEN));

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_GET_DRIVER_VHT_TX_ANTENNA,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.
        RadInfo.u4RadioIfIndex = pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.RadInfo.
            au1RadioIfName, pu1RadioName, STRLEN (pu1RadioName));

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.
        RadInfo.au2WlanIfIndex[0] = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;
    SPRINTF ((CHR1 *) au1Wname, "%s%d", RADIO_ATH_INTF_NAME,
             pRadioIfGetDB->RadioIfGetAllDB.u1WlanId - 1);
    STRCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.RadInfo.
            au1WlanIfname[0], au1Wname);

    /*To Get from the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "NpUtilHwProgram : vht Capablities  get from Hw failed\r\n");
        return OSIX_FAILURE;
    }
    pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1VhtTxAntenna =
        FsHwNp.RadioIfNpModInfo.unOpCode.
        sConfigDot11AcCapaParams.u1VhtTxAntenna;
    pRadioIfGetDB->RadioIfIsGetAllDB.bVhtTxAntenna = OSIX_TRUE;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioHwGetDriverSuppChannelWidth                     *
 *                                                                           *
 * Description        : Wrapper Func through which the Max MPDU Len          *
 *                      will be returned                                     *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to input structure           *
 *                      pu1RadioName - Radio Interface Name                  *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioHwGetDriverSuppChannelWidth (tRadioIfGetDB * pRadioIfGetDB,
                                  UINT1 *pu1RadioName)
{
    tFsHwNp             FsHwNp;
    UINT1               au1Wname[WLAN_NAME_MAX_LEN];

    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));
    MEMSET (au1Wname, 0, sizeof (WLAN_NAME_MAX_LEN));

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_GET_DRIVER_SUPP_CHANNEL_WIDTH,    /* Function/Opode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.
        RadInfo.u4RadioIfIndex = pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.RadInfo.
            au1RadioIfName, pu1RadioName, STRLEN (pu1RadioName));

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.
        RadInfo.au2WlanIfIndex[0] = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;
    SPRINTF ((CHR1 *) au1Wname, "%s%d", RADIO_ATH_INTF_NAME,
             pRadioIfGetDB->RadioIfGetAllDB.u1WlanId - 1);
    STRCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.RadInfo.
            au1WlanIfname[0], au1Wname);

    /*To Get from the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "NpUtilHwProgram : vht Capablities  get from Hw failed\r\n");
        return OSIX_FAILURE;
    }
    pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1SuppChanWidth =
        FsHwNp.RadioIfNpModInfo.unOpCode.
        sConfigDot11AcCapaParams.u1SuppChanWidth;
    pRadioIfGetDB->RadioIfIsGetAllDB.bSuppChanWidth = OSIX_TRUE;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioHwGetDriverVhtSuppMcs                           *
 *                                                                           *
 * Description        : Wrapper Func through which the Max MPDU Len          *
 *                      will be returned                                     *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to input structure           *
 *                      pu1RadioName - Radio Interface Name                  *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioHwGetDriverVhtSuppMcs (tRadioIfGetDB * pRadioIfGetDB, UINT1 *pu1RadioName)
{
    tFsHwNp             FsHwNp;
    UINT1               au1Wname[WLAN_NAME_MAX_LEN];

    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));
    MEMSET (au1Wname, 0, sizeof (WLAN_NAME_MAX_LEN));

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_GET_DRIVER_VHT_SUPP_MCS,    /* Function/Opode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.
        RadInfo.u4RadioIfIndex = pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.RadInfo.
            au1RadioIfName, pu1RadioName, STRLEN (pu1RadioName));

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.
        RadInfo.au2WlanIfIndex[0] = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;
    SPRINTF ((CHR1 *) au1Wname, "%s%d", RADIO_ATH_INTF_NAME,
             pRadioIfGetDB->RadioIfGetAllDB.u1WlanId - 1);
    STRCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.RadInfo.
            au1WlanIfname[0], au1Wname);

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.RadInfo.
        u1WlanId = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;

    /*To Get from the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "NpUtilHwProgram : vht Capablities  get from Hw failed\r\n");
        return OSIX_FAILURE;
    }

    MEMCPY (pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.au1SuppVhtCapaMcs,
            FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11AcCapaParams.
            au1SuppVhtCapaMcs,
            sizeof (pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.
                    au1SuppVhtCapaMcs));
    MEMCPY ((pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.au1VhtCapaMcs),
            (pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.
             au1SuppVhtCapaMcs),
            sizeof (pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.
                    au1SuppVhtCapaMcs));

    pRadioIfGetDB->RadioIfIsGetAllDB.bSuppVhtCapaMcs = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bVhtCapaMcs = OSIX_TRUE;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfGetAntennaDiversity                           *
                                                                        
 * Description        : Wrapper Func through which the Antenna diversity val *
 *                      will be returned                                     *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to input structure           *
 *                      pu1RadioName - Radio Interface Name                  *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfGetAntennaDiversity (tRadioIfGetDB * pRadioIfGetDB, UINT1 *ppu1RadioName)
{
    tFsHwNp             FsHwNp;

    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_GET_ANTENNA_DIVERSITY,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    FsHwNp.RadioIfNpModInfo.unOpCode.sFsHwGetAntennaDiversity.RadInfo.
        u4RadioIfIndex = pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sFsHwGetAntennaDiversity.RadInfo.
            au1RadioIfName, ppu1RadioName, STRLEN (ppu1RadioName));

    /*To Get from the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "NpUtilHwProgram : Values to get from Hw failed\r\n");
        return OSIX_FAILURE;
    }

    pRadioIfGetDB->RadioIfIsGetAllDB.bDiversitySupport = OSIX_TRUE;
    return OSIX_SUCCESS;
}

#endif

/*****************************************************************************
 * Function Name      : RadioIfGetRadioDefault                               *
 *                                                                           *
 * Description        : Wrapper Func through which the default values will be*
 *                      will be returned                                     *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to input structure           *
 *                      pu1RadioName - Radio Interface Name                  *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfGetRadioDefault (tRadioIfGetDB * pRadioIfGetDB, UINT1 *pu1RadioName)
{
    UINT1               u1index = 0;

#ifdef NPAPI_WANTED
    tFsHwNp             FsHwNp;
    UINT1               au1Wname[WLAN_NAME_MAX_LEN];

    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));
    MEMSET (au1Wname, 0, sizeof (WLAN_NAME_MAX_LEN));

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_GET_RADIO_DEFAULT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    FsHwNp.RadioIfNpModInfo.unOpCode.sFsHwGetTxCurrPower.
        RadInfo.u4RadioIfIndex = pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sFsHwGetTxCurrPower.
            RadInfo.au1RadioIfName, pu1RadioName, STRLEN (pu1RadioName));

    FsHwNp.RadioIfNpModInfo.unOpCode.sFsHwGetTxCurrPower.
        RadInfo.au2WlanIfIndex[0] = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;
    SPRINTF ((CHR1 *) au1Wname, "%s%d", RADIO_ATH_INTF_NAME,
             pRadioIfGetDB->RadioIfGetAllDB.u1WlanId - 1);

    STRCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sFsHwGetTxCurrPower.
            RadInfo.au1WlanIfname[0], au1Wname);

    FsHwNp.RadioIfNpModInfo.unOpCode.sFsHwGetTxCurrPower.RadInfo.
        u1WlanId = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;
    /*To Get from the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "NpUtilHwProgram: Values to get from Hw failed\r\n");
        return OSIX_FAILURE;
    }

    pRadioIfGetDB->RadioIfGetAllDB.pu1AntennaSelection
        [FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetRadioDefault.u1TxAntenna -
         1] =
        (UINT1) FsHwNp.RadioIfNpModInfo.unOpCode.
        sConfigGetRadioDefault.u1AntennaSelection;

    pRadioIfGetDB->RadioIfGetAllDB.u1CurrentTxAntenna =
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetRadioDefault.u1TxAntenna;

    pRadioIfGetDB->RadioIfGetAllDB.u1DiversitySupport =
        FsHwNp.RadioIfNpModInfo.unOpCode.
        sConfigGetRadioDefault.u1AntennaDiveristy;

    pRadioIfGetDB->RadioIfGetAllDB.u1AntennaMode =
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetRadioDefault.u1AntennaMode;

    pRadioIfGetDB->RadioIfGetAllDB.u1CurrentCCAMode =
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetRadioDefault.u1CurrentCCA;

    pRadioIfGetDB->RadioIfGetAllDB.u1ShortRetry =
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetRadioDefault.u1ShortRetry;

    pRadioIfGetDB->RadioIfGetAllDB.u1LongRetry =
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetRadioDefault.u1LongRetry;

    pRadioIfGetDB->RadioIfGetAllDB.u4TxMsduLifetime =
        FsHwNp.RadioIfNpModInfo.unOpCode.
        sConfigGetRadioDefault.u4TxMSDULifeTime;

    pRadioIfGetDB->RadioIfGetAllDB.u4RxMsduLifetime =
        FsHwNp.RadioIfNpModInfo.unOpCode.
        sConfigGetRadioDefault.u4RxMSDULifeTime;

    pRadioIfGetDB->RadioIfGetAllDB.u2FirstChannelNumber =
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetRadioDefault.u2FirstChanNo;

    pRadioIfGetDB->RadioIfGetAllDB.u2NoOfChannels =
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetRadioDefault.u2NoOfChan;

    pRadioIfGetDB->RadioIfGetAllDB.u2MaxTxPowerLevel =
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetRadioDefault.u2MaxTxPowerLvl;

    pRadioIfGetDB->RadioIfGetAllDB.u1SupportedBand =
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetRadioDefault.u1BandSupport;

    pRadioIfGetDB->RadioIfGetAllDB.u1ShortPreamble =
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetRadioDefault.u1ShortPreamble;

    pRadioIfGetDB->RadioIfGetAllDB.u1NoOfBssId =
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetRadioDefault.u1NoOfBSSID;

    pRadioIfGetDB->RadioIfGetAllDB.u1BPFlag =
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetRadioDefault.u1BPFlag;

    pRadioIfGetDB->RadioIfGetAllDB.u1ChannelWidth =
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetRadioDefault.u1ChannelWidth;
    pRadioIfGetDB->RadioIfGetAllDB.u1ShortGi20 =
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetRadioDefault.u1ShortGi20;

    pRadioIfGetDB->RadioIfGetAllDB.u1ShortGi40 =
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetRadioDefault.u1ShortGi40;

    pRadioIfGetDB->RadioIfGetAllDB.u2HTCapInfo =
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetRadioDefault.u2HTCapInfo;

    pRadioIfGetDB->RadioIfGetAllDB.u1AMPDUParam =
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetRadioDefault.u1AMPDUParam;

    pRadioIfGetDB->RadioIfGetAllDB.u1HTInfoSecChanAbove =
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetRadioDefault.
        u1HTInfoSecChanAbove;

    pRadioIfGetDB->RadioIfGetAllDB.u1HTInfoSecChanBelow =
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetRadioDefault.
        u1HTInfoSecChanBelow;

    pRadioIfGetDB->RadioIfGetAllDB.u1TxSupportMcsSet =
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetRadioDefault.
        u1TxSupportMcsSet;

    MEMCPY (pRadioIfGetDB->RadioIfGetAllDB.au1SuppMCSSet,
            FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetRadioDefault.
            au1SuppMCSSet, RADIOIF_11N_MCS_RATE_LEN_PKT);

    MEMCPY (pRadioIfGetDB->RadioIfGetAllDB.au1ManMCSSet,
            FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetRadioDefault.
            au1ManMCSSet, RADIOIF_11N_MCS_RATE_LEN_PKT);

#else

    pRadioIfGetDB->RadioIfGetAllDB.pu1AntennaSelection[0] =
        RADIO_ANTENNA_INTERNAL;

    pRadioIfGetDB->RadioIfGetAllDB.u1CurrentTxAntenna = RADIOIF_DEF_ANT_COUNT;

    pRadioIfGetDB->RadioIfGetAllDB.u1DiversitySupport =
        RADIO_ANTENNA_DIVERSITY_DISABLED;

    pRadioIfGetDB->RadioIfGetAllDB.u1AntennaMode = RADIOIF_DEF_ANT_MODE;

    pRadioIfGetDB->RadioIfGetAllDB.u1CurrentCCAMode = RADIO_CCA_MODE_SUPPORTED;

    pRadioIfGetDB->RadioIfGetAllDB.u1ShortRetry = RADIOIF_DEF_SHORT_RETRY;

    pRadioIfGetDB->RadioIfGetAllDB.u1LongRetry = RADIOIF_DEF_LONG_RETRY;

    pRadioIfGetDB->RadioIfGetAllDB.u4TxMsduLifetime = RADIOIF_DEF_TX_LIFETIME;

    pRadioIfGetDB->RadioIfGetAllDB.u4RxMsduLifetime = RADIOIF_DEF_RX_LIFETIME;

    pRadioIfGetDB->RadioIfGetAllDB.u2FirstChannelNumber =
        RADIO_TYPEB_FIRST_CHANNEL;

    pRadioIfGetDB->RadioIfGetAllDB.u2NoOfChannels = RADIO_TYPEB_NO_OF_CHANNELS;

    pRadioIfGetDB->RadioIfGetAllDB.u2MaxTxPowerLevel = RADIOIF_MAX_POWER_LEVEL;

    pRadioIfGetDB->RadioIfGetAllDB.
        MultiDomainInfo[0].u2FirstChannelNo = RADIO_TYPEB_FIRST_CHANNEL;

    pRadioIfGetDB->RadioIfGetAllDB.
        MultiDomainInfo[1].u2FirstChannelNo = RADIO_TYPEA_FIRST_CHANNEL;

    pRadioIfGetDB->RadioIfGetAllDB.
        MultiDomainInfo[0].u2NoOfChannels = RADIO_TYPEB_NO_OF_CHANNELS;

    pRadioIfGetDB->RadioIfGetAllDB.
        MultiDomainInfo[1].u2NoOfChannels = RADIO_TYPEA_NO_OF_CHANNELS;

    pRadioIfGetDB->RadioIfGetAllDB.
        MultiDomainInfo[0].u2MaxTxPowerLevel = RADIOIF_MAX_POWER_LEVEL;

    pRadioIfGetDB->RadioIfGetAllDB.
        MultiDomainInfo[1].u2MaxTxPowerLevel = RADIOIF_MAX_POWER_LEVEL;

    pRadioIfGetDB->RadioIfGetAllDB.u1SupportedBand = RADIO_BAND_SUPPORTED;

    pRadioIfGetDB->RadioIfGetAllDB.u1ShortPreamble =
        RADIO_SHORT_PREAMBLE_ENABLE;

    pRadioIfGetDB->RadioIfGetAllDB.u1NoOfBssId = RADIO_MAX_BSSID_SUPPORTED;

    UNUSED_PARAM (pu1RadioName);
#endif
    for (u1index = 0;
         u1index < pRadioIfGetDB->RadioIfGetAllDB.u1CurrentTxAntenna; u1index++)
    {
        pRadioIfGetDB->RadioIfIsGetAllDB.bAntennaSelection[u1index] = OSIX_TRUE;
    }
    pRadioIfGetDB->RadioIfIsGetAllDB.bCurrentTxAntenna = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bDiversitySupport = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bAntennaMode = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bCurrentCCAMode = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bShortRetry = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bLongRetry = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bTxMsduLifetime = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bRxMsduLifetime = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bFirstChannelNumber = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bNoOfChannels = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bMaxTxPowerLevel = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bMultiDomainInfo = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bSupportedBand = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bShortPreamble = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bNoOfBssId = OSIX_TRUE;
    /*For 802.11n */
    pRadioIfGetDB->RadioIfIsGetAllDB.bBPFlag = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bChannelWidth = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bShortGi20 = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bShortGi40 = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bHTCapInfo = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bHTInfoSecChanAbove = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bHTInfoSecChanBelow = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bSuppMCSSet = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bAMPDUParam = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bTxSupportMcsSet = OSIX_TRUE;

    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfGetMacAddr                                    *
 *                                                                           *
 * Description        : Wrapper Func through which the radio interface MAC   *
 *                      address will be received from NPAPI                  *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to input structure           *
 *                      pu1RadioName - Radio Interface Name                  *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfGetMacAddr (tRadioIfGetDB * pRadioIfGetDB, UINT1 *pu1RadioName)
{
#ifdef NPAPI_WANTED
    tFsHwNp             FsHwNp;
    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_GET_WIFI_HW_ADDR,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms
                                 */

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetDot11HwAddr.RadInfo.
        u4RadioIfIndex = pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetDot11HwAddr.RadInfo.
            au1RadioIfName, pu1RadioName, STRLEN (pu1RadioName));
    SPRINTF ((CHR1 *) FsHwNp.RadioIfNpModInfo.unOpCode.
             sConfigGetDot11HwAddr.RadInfo.au1WlanIfname[0], "%s%d",
             RADIO_ATH_INTF_NAME, 0);
    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetDot11HwAddr.RadInfo.
        u1WlanId = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;

    /*To Get from the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "NpUtilHwProgram (Mac Addr): Values to get from Hw failed\r\n");
        return OSIX_FAILURE;
    }
    MEMCPY (pRadioIfGetDB->RadioIfGetAllDB.MacAddr,
            FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetDot11HwAddr.
            au1WifiHwAddr, sizeof (tMacAddr));
#else
    UINT1               u1RadioId = 0;

    IssGetVRMacAddr (0, &pRadioIfGetDB->RadioIfGetAllDB.MacAddr);
    u1RadioId = (UINT1) (pu1RadioName[RADIO_INTF_NAME_LEN - 1] - 48);

    pRadioIfGetDB->RadioIfGetAllDB.MacAddr[5] =
        (UINT1) (pRadioIfGetDB->RadioIfGetAllDB.MacAddr[5] +
                 (u1RadioId * RADIO_MAX_BSSID_SUPPORTED) + 1);
#endif
    pRadioIfGetDB->RadioIfIsGetAllDB.bMacAddr = OSIX_TRUE;

    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfInitDefaultValues                             *
 *                                                                           *
 * Description        : Function to get the values from hardware during      *
 *                      start up                                             *
 * Input(s)           : pRadioIfGetDB - Pointer to input structure           *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfInitDefaultValues (tRadioIfGetDB * pRadioIfGetDB)
{
    UINT1               u1index = 0;
#ifndef NPAPI_WANTED
    UINT1               au1VhtCapaMcs[DOT11AC_VHT_CAP_MCS_LEN] =
        RADIO_11AC_MCS_ENABLE;
#endif

    pRadioIfGetDB->RadioIfIsGetAllDB.bSupportedRate = OSIX_TRUE;
    pRadioIfGetDB->RadioIfGetAllDB.au1SupportedRate[0] = RADIO_SUPPORTED_RATE1;
    pRadioIfGetDB->RadioIfGetAllDB.au1SupportedRate[1] = RADIO_SUPPORTED_RATE2;
#ifndef NPAPI_WANTED
    pRadioIfGetDB->RadioIfIsGetAllDB.bCountryString = OSIX_TRUE;
    MEMCPY (pRadioIfGetDB->RadioIfGetAllDB.au1CountryString,
            RADIO_DEF_COUNTRY_NAME, RADIO_COUNTRY_LENGTH);
#endif
    pRadioIfGetDB->RadioIfGetAllDB.pu1AntennaSelection[0] =
        RADIO_ANTENNA_INTERNAL;

    pRadioIfGetDB->RadioIfGetAllDB.u1CurrentTxAntenna = RADIOIF_DEF_ANT_COUNT;

    pRadioIfGetDB->RadioIfGetAllDB.u1DiversitySupport =
        RADIO_ANTENNA_DIVERSITY_DISABLED;

    pRadioIfGetDB->RadioIfGetAllDB.u1AntennaMode = RADIOIF_DEF_ANT_MODE;

    pRadioIfGetDB->RadioIfGetAllDB.u2RTSThreshold = RADIOIF_DEF_RTS_THRESHOLD;

    pRadioIfGetDB->RadioIfGetAllDB.u2FragmentationThreshold =
        RADIOIF_DEF_FRAG_THRESHOLD;

    pRadioIfGetDB->RadioIfGetAllDB.u1CurrentCCAMode = RADIO_CCA_MODE_SUPPORTED;

    pRadioIfGetDB->RadioIfGetAllDB.u1ShortRetry = RADIOIF_DEF_SHORT_RETRY;

    pRadioIfGetDB->RadioIfGetAllDB.u1LongRetry = RADIOIF_DEF_LONG_RETRY;

    pRadioIfGetDB->RadioIfGetAllDB.u4TxMsduLifetime = RADIOIF_DEF_TX_LIFETIME;

    pRadioIfGetDB->RadioIfGetAllDB.u4RxMsduLifetime = RADIOIF_DEF_RX_LIFETIME;

    pRadioIfGetDB->RadioIfGetAllDB.u2FirstChannelNumber =
        RADIO_TYPEB_FIRST_CHANNEL;

    pRadioIfGetDB->RadioIfGetAllDB.u2NoOfChannels = RADIO_TYPEB_NO_OF_CHANNELS;
    pRadioIfGetDB->RadioIfGetAllDB.u2MaxTxPowerLevel = RADIOIF_DEF_POWER_LEVEL;
    pRadioIfGetDB->RadioIfGetAllDB.u1SupportedBand = RADIO_BAND_SUPPORTED;

    pRadioIfGetDB->RadioIfGetAllDB.u1ShortPreamble =
        RADIO_SHORT_PREAMBLE_ENABLE;

    pRadioIfGetDB->RadioIfGetAllDB.u1NoOfBssId = RADIO_MAX_BSSID_SUPPORTED;

    pRadioIfGetDB->RadioIfGetAllDB.u4T1Threshold =
        RADIO_OFDM_TI_THRESHOLD_VALUE_MIN;

    pRadioIfGetDB->RadioIfGetAllDB.u1BPFlag = RADIO_VALUE_3;

    pRadioIfGetDB->RadioIfGetAllDB.u1ChannelWidth =
        RADIO_DOT11N_CHANNELWIDTH_20;
    pRadioIfGetDB->RadioIfGetAllDB.u1HtFlag |= (1 << SHIFT_VALUE_4);
    pRadioIfGetDB->RadioIfGetAllDB.u2HTCapInfo |= (0 << SHIFT_VALUE_1);

    pRadioIfGetDB->RadioIfGetAllDB.u1ShortGi20 = RADIO_DOT11N_SGI_ENABLE;
    pRadioIfGetDB->RadioIfGetAllDB.u1HtFlag |= (1 << SHIFT_VALUE_3);
    pRadioIfGetDB->RadioIfGetAllDB.u2HTCapInfo |= (1 << SHIFT_VALUE_5);
    pRadioIfGetDB->RadioIfGetAllDB.u1AMPDUParam = RADIO_DOT11N_AMPDU;

    pRadioIfGetDB->RadioIfGetAllDB.u1MaxSuppMCS =
        RADIO_DOT11N_MAX_SUPP_MCS_MIMO3;
    pRadioIfGetDB->RadioIfGetAllDB.u1MaxManMCS =
        RADIO_DOT11N_MAX_SUPP_MCS_MIMO3;

    MEMSET (pRadioIfGetDB->RadioIfGetAllDB.au1SuppMCSSet,
            DOT11N_MCS_DEFAULT_VALUE, RADIO_VALUE_3);

#ifndef NPAPI_WANTED
    if (pRadioIfGetDB->RadioIfGetAllDB.u4Dot11RadioType == RADIO_TYPE_AC)
    {
        pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1SuppMaxMPDULen =
            RADIO_11AC_MPDU_DEFAULT;
        pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1SuppRxLpdc =
            RADIO_11AC_RX_LDPC_DEFAULT;
        pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1SuppShortGi80 =
            RADIO_11AC_SHORTGI80_ENABLE;
        pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1SuppVhtMaxAMPDU =
            RADIO_11AC_AMPDU_DEFAULT;
        MEMCPY ((pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.
                 au1VhtCapaMcs), au1VhtCapaMcs, DOT11AC_VHT_CAP_MCS_LEN);
        pRadioIfGetDB->RadioIfGetAllDB.Dot11AcOperParams.u1SuppVhtChannelWidth =
            RADIO_11AC_CHANWIDTH_DEFAULT;
        pRadioIfGetDB->RadioIfGetAllDB.Dot11AcOperParams.u1SuppCenterFcy0 =
            RADIO_11AC_CENTER_FCY_DEFAULT;
    }
#endif
    for (u1index = 0;
         u1index < pRadioIfGetDB->RadioIfGetAllDB.u1CurrentTxAntenna; u1index++)
    {
        pRadioIfGetDB->RadioIfIsGetAllDB.bAntennaSelection[u1index] = OSIX_TRUE;
    }
    pRadioIfGetDB->RadioIfIsGetAllDB.bCurrentTxAntenna = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bDiversitySupport = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bAntennaMode = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bCurrentCCAMode = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bShortRetry = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bLongRetry = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bTxMsduLifetime = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bRxMsduLifetime = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bFirstChannelNumber = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bNoOfChannels = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bMaxTxPowerLevel = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bSupportedBand = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bShortPreamble = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bNoOfBssId = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bRTSThreshold = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bFragmentationThreshold = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bT1Threshold = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bCurrentTxPower = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bBPFlag = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bChannelWidth = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bShortGi20 = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bHTCapInfo = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bHtFlag = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bSuppMCSSet = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bManMCSSet = OSIX_TRUE;

#ifndef NPAPI_WANTED
    if (pRadioIfGetDB->RadioIfGetAllDB.u4Dot11RadioType == RADIO_TYPE_AC)
    {
        pRadioIfGetDB->RadioIfIsGetAllDB.bMaxMPDULen = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bRxLpdc = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bShortGi80 = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bVhtMaxAMPDU = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bVhtCapaMcs = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bVhtChannelWidth = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bCenterFcy0 = OSIX_TRUE;

        if (WssIfProcessRadioIfDBMsg (WSS_SET_RADIO_IF_DB_11AC, pRadioIfGetDB)
            != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfCreateInterface : 11AC Value to be set in DB Failed \r\n");
            return OSIX_FAILURE;
        }

    }
#endif

    /*DB Updation */
    if (WssIfProcessRadioIfDBMsg (WSS_SET_RADIO_IF_DB, pRadioIfGetDB)
        != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfCreateInterface : Value to be set in DB Failed \r\n");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioHwGetDriverChannelCapablities                   *
 *                                                                           *
 * Description        : Function to get the values from hardware during      *
 *                      start up                                             *
 * Input(s)           : pRadioIfGetDB - Pointer to input structure           *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioHwGetDriverChannelCapablities (tRadioIfGetDB * pRadioIfGetDB,
                                    UINT1 *pu1RadioName)
{
    UNUSED_PARAM (pRadioIfGetDB);
    UNUSED_PARAM (pu1RadioName);
#ifdef NPAPI_WANTED
    tFsHwNp             FsHwNp;
    UINT1               au1Wname[WLAN_NAME_MAX_LEN];

    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));
    MEMSET (au1Wname, 0, sizeof (WLAN_NAME_MAX_LEN));

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_GET_DRIVER_CHNL_CAPABLITIES,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    FsHwNp.RadioIfNpModInfo.unOpCode.sChannelInfo.
        RadInfo.u4RadioIfIndex = pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sChannelInfo.RadInfo.
            au1RadioIfName, pu1RadioName, STRLEN (pu1RadioName));
    FsHwNp.RadioIfNpModInfo.unOpCode.sChannelInfo.RadInfo.
        u1WlanId = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;

    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "NpUtilHwProgram : chnl Capablities  get from Hw failed\r\n");
        return OSIX_FAILURE;
    }
#endif
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfCreateInterface                               *
 *                                                                           *
 * Description        : Function to return the default values in case of host*
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to input structure           *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfCreateInterface (tRadioIfMsgStruct * pWssMsgStruct)
{
    tRadioIfGetDB       RadioIfGetDB;
    tWssPMInfo          WssPmInfo;
#ifdef WTP_WANTED
    UINT1               au1RadioName[RADIO_INTF_NAME_LEN + 1] = { 0 };
#endif
#ifdef RFMGMT_WANTED
    tRfMgmtMsgStruct    RfMgmtMsgStruct;

    MEMSET (&RfMgmtMsgStruct, 0, sizeof (tRfMgmtMsgStruct));
#endif
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
        UtlShMemAllocAntennaSelectionBuf ();
    if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC, "\r%% Memory allocation for Antenna"
                     " selection failed.\r\n");
        return OSIX_FAILURE;
    }

    MEMSET (&WssPmInfo, 0, sizeof (tWssPMInfo));

    RADIOIF_FN_ENTRY ();
    /*Input Validation */
    if (pWssMsgStruct == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfCreateInterface : Invalid input\n");
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                         RadioIfGetAllDB.pu1AntennaSelection);
        return OSIX_FAILURE;
    }

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
        pWssMsgStruct->unRadioIfMsg.RadioIfCreate.u4IfIndex;
    RadioIfGetDB.RadioIfGetAllDB.u1IfType =
        pWssMsgStruct->unRadioIfMsg.RadioIfCreate.u1IfType;
    RadioIfGetDB.RadioIfGetAllDB.u1AdminStatus =
        pWssMsgStruct->unRadioIfMsg.RadioIfCreate.u1AdminStatus;
    RadioIfGetDB.RadioIfGetAllDB.u1RadioId =
        pWssMsgStruct->unRadioIfMsg.RadioIfCreate.u1RadioId;

    /*DB Updation */
    if (WssIfProcessRadioIfDBMsg (WSS_ADD_RADIO_IF_DB_PHY, &RadioIfGetDB)
        != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfCreateInterface: Value to be set in DB Failed\n");
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                         RadioIfGetAllDB.pu1AntennaSelection);
        return OSIX_FAILURE;
    }

    /*Hw Get calls to get the default values in AP */
    RadioIfGetDB.RadioIfGetAllDB.u1WlanId = 1;

#ifdef  RFMGMT_WANTED
    RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtIntfConfigReq.u4RadioIfIndex =
        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex;

    RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtIntfConfigReq.isPresent = OSIX_TRUE;

    if (WssIfProcessRfMgmtMsg (RFMGMT_CREATE_RADIO_ENTRY,
                               &RfMgmtMsgStruct) != RFMGMT_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "CreateProfile : Update RM Mgmt DB Failed\n");
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                         RadioIfGetAllDB.pu1AntennaSelection);
        return OSIX_FAILURE;
    }
#endif
#ifdef NPAPI_WANTED
    SPRINTF ((CHR1 *) au1RadioName, "%s%d", RADIO_INTF_NAME,
             pWssMsgStruct->unRadioIfMsg.RadioIfCreate.u1RadioId - 1);
    if (RadioIfGetMaxTxPowerLevel2G (&RadioIfGetDB, (UINT1 *) &au1RadioName) !=
        OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfCreateInterface: Hw get for 2G failed\n");
    }
    /*GetScanDetails */
    if (RadioIfGetScanDetails (&RadioIfGetDB, (UINT1 *) &au1RadioName)
        != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfCreateInterface: Hw get for 5G failed\n");
    }

    if (RadioIfGetMaxTxPowerLevel5G (&RadioIfGetDB, (UINT1 *) &au1RadioName)
        != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfCreateInterface: Hw get for 5G failed\n");
    }

    if (RadioIfGetCountryString (&RadioIfGetDB, (UINT1 *) &au1RadioName)
        != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfCreateInterface: Hw get for Country String Failed\n");
    }
    if (RadioIfGetMultiDomainInfo (&RadioIfGetDB, (UINT1 *) &au1RadioName)
        != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfCreateInterface: Hw get for Multi Domain Capability Failed\n");
    }
    if (RadioIfGetSupportedRate (&RadioIfGetDB, (UINT1 *) &au1RadioName)
        != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfCreateInterface: Hw get for Supported Rate Failed\n");
    }

    if (RadioIfGetAntennaDiversity (&RadioIfGetDB, (UINT1 *) &au1RadioName)
        != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfCreateInterface: Hw get for Antenna Diversity Failedn");
    }

    if (RadioIfGetCurrentChannel (&RadioIfGetDB, (UINT1 *) &au1RadioName)
        != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfCreateInterface: Hw get for Current Channel Failed\n");
    }

    if (RadioIfGetDTIMPeriod (&RadioIfGetDB, (UINT1 *) &au1RadioName)
        != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfCreateInterface: Hw get for DTIM Period Failed\n");
    }

    if (RadioIfGetBeaconPeriod (&RadioIfGetDB, (UINT1 *) &au1RadioName)
        != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfCreateInterface: Hw get for Beacon Period failed\n");
    }

    if (RadioIfGetRTSThreshold (&RadioIfGetDB, (UINT1 *) &au1RadioName)
        != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfCreateInterface : Hw get for RTS Threshold failed\r\n");
    }

    if (RadioIfGetFragmentationThreshold (&RadioIfGetDB,
                                          (UINT1 *) &au1RadioName) !=
        OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfCreateInterface:Get for Fragement threshold failed\n");
    }

    if (RadioIfGetCurrentTxPower (&RadioIfGetDB, (UINT1 *) &au1RadioName)
        != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfCreateInterface : Hw get for  Failed\n");
    }
#endif
#ifdef WTP_WANTED
    if (RadioIfGetRadioDefault (&RadioIfGetDB, (UINT1 *) &au1RadioName)
        != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfCreateInterface: Hw get for default params failed\n");
    }
#endif
#ifdef NPAPI_WANTED
    if (RadioHwGetSuppRadioType (&RadioIfGetDB, (UINT1 *) &au1RadioName)
        != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfCreateInterface: Hw get for vht capablities failed\n");
    }

    if ((((RadioIfGetDB.RadioIfGetAllDB.u4SuppRadioType) & RADIO_TYPE_N)
         == RADIO_TYPE_N) || (((RadioIfGetDB.RadioIfGetAllDB.
                                u4SuppRadioType) & RADIO_TYPE_AC) ==
                              RADIO_TYPE_AC))
    {
        if (RadioHwGetDriverHtCapablities
            (&RadioIfGetDB, (UINT1 *) &au1RadioName) != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfCreateInterface: Hw get for HT Capablities failed\n");
        }
        if (RadioHwGetDriverHtSuppMcs (&RadioIfGetDB, (UINT1 *) &au1RadioName)
            != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfCreateInterface: Hw get for HT Mcs failed\n");
        }
        if (RadioHwGetDriverHtAMPDU (&RadioIfGetDB, (UINT1 *) &au1RadioName)
            != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfCreateInterface: Hw get for HT A-MPDU failed\n");
        }
        if (RadioHwGetDriverExtHtCapabilities
            (&RadioIfGetDB, (UINT1 *) &au1RadioName) != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfCreateInterface: Hw get for Ext HT capability failed\n");
        }
        if (RadioHwGetDriverBeamformCapability
            (&RadioIfGetDB, (UINT1 *) &au1RadioName) != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfCreateInterface: Hw get for Beamform capability failed\n");
        }
    }

    /*    RadioIfGetDB.RadioIfGetAllDB.u4SuppRadioType = 0x80000000; */
    if (((RadioIfGetDB.RadioIfGetAllDB.u4SuppRadioType) & RADIO_TYPE_AC)
        == RADIO_TYPE_AC)
    {
        if (RadioHwGetDriverVhtCapablities
            (&RadioIfGetDB, (UINT1 *) &au1RadioName) != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfCreateInterface: Hw get for vht capablities failed\n");
        }
        if ((WssIfProcessRadioIfDBMsg (WSS_PARSE_11AC_CAPABILITY_INFO,
                                       &RadioIfGetDB) != OSIX_SUCCESS))
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfCreateInterface: Failed to  assemble 11AC"
                         " capability params\r\n");
            return OSIX_FAILURE;

        }
        RadioIfGetDB.RadioIfIsGetAllDB.bSuppChanWidth = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bShortGi160 = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bSuppVhtTxStbc = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bSuppVhtRxStbc = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bSuBeamFormer = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bSuBeamFormee = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bSuppBeamFormAnt = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bSoundDimension = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bMuBeamFormer = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bMuBeamFormee = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bVhtTxOpPs = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bHtcVhtCapable = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bVhtLinkAdaption = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bVhtRxAntenna = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bVhtTxAntenna = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bSuppMaxMPDULen = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bSuppRxLpdc = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bSuppShortGi80 = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bSuppVhtMaxAMPDU = OSIX_TRUE;

        if (RadioHwGetDriverVhtSuppMcs (&RadioIfGetDB, (UINT1 *) &au1RadioName)
            != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfCreateInterface: Hw get for Vht Mcs failed\n");
        }
        RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1ShortGi80 =
            RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1SuppShortGi80;
        /*Computation of Highest Supported Data Rate */
        WssIfVhtHighestSuppDataRate (&RadioIfGetDB);
        RadioIfGetDB.RadioIfIsGetAllDB.bSuppRxDataRate =
            RadioIfGetDB.RadioIfIsGetAllDB.bRxDataRate;
        RadioIfGetDB.RadioIfIsGetAllDB.bSuppTxDataRate =
            RadioIfGetDB.RadioIfIsGetAllDB.bTxDataRate;
        RadioIfGetDB.RadioIfIsGetAllDB.bSuppRxDataRate = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bSuppTxDataRate = OSIX_TRUE;
    }

#ifdef DEBUG_WANTED
    if (((RadioIfGetDB.RadioIfGetAllDB.u4SuppRadioType & RADIO_TYPE_A)
         == RADIO_TYPE_A) ||
        (((RadioIfGetDB.RadioIfGetAllDB.u4SuppRadioType & RADIO_TYPE_AC)
          == RADIO_TYPE_AC)) ||
        (((RadioIfGetDB.RadioIfGetAllDB.u4SuppRadioType & RADIO_TYPE_AN)
          == RADIO_TYPE_AN)))
    {
        RadioIfGetDB.RadioIfIsGetAllDB.b11hDfsStatus = OSIX_TRUE;

        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                      (&RadioIfGetDB)) != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfCreateInterface:" "RadioIf DB failed \r\n");
            return OSIX_FAILURE;
        }

        u111hDfsStatus = RadioIfGetDB.RadioIfGetAllDB.u111hDfsStatus;
        if (u111hDfsStatus == 1)
        {

            if (RadioHwGetDriverChannelCapablities (&RadioIfGetDB,
                                                    (UINT1 *) &au1RadioName) !=
                OSIX_SUCCESS)
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                             "RadioIfCreateInterface: Hw get for chnl capablities failed\n");
            }
        }
    }
#endif
#endif
#ifdef WTP_WANTED
    if (RadioIfGetMacAddr (&RadioIfGetDB, (UINT1 *) &au1RadioName)
        != OSIX_SUCCESS)
    {

        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfCreateInterface: Hw get for Mac Addr Failed\n");
    }
#endif
    if (((RadioIfGetDB.RadioIfGetAllDB.u4SuppRadioType) & RADIO_TYPE_AC)
        == RADIO_TYPE_AC)
    {
        if (WssIfProcessRadioIfDBMsg (WSS_SET_RADIO_IF_DB_11AC, &RadioIfGetDB)
            != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfCreateInterface : 11AC Values to be set in DB Failed \r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            return OSIX_FAILURE;
        }
    }
    if ((((RadioIfGetDB.RadioIfGetAllDB.u4SuppRadioType) & RADIO_TYPE_N)
         == RADIO_TYPE_N) ||
        (((RadioIfGetDB.RadioIfGetAllDB.u4SuppRadioType) & RADIO_TYPE_AC)
         == RADIO_TYPE_AC))
    {
        RadioIfGetDB.RadioIfIsGetAllDB.bSuppHtCapaMcs = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bSuppAmpduParam = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bAmpduParam = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bSuppHtCapInfo = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bSuppTxBeamCapParam = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bSuppHtExtCap = OSIX_TRUE;

        if (WssIfProcessRadioIfDBMsg (WSS_SET_RADIO_IF_DB_11N, &RadioIfGetDB)
            != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "RadioIfCreateInterface : 802.11n Values to be set in DB Failed \r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            return OSIX_FAILURE;
        }
    }
    if (WssIfProcessRadioIfDBMsg (WSS_SET_RADIO_IF_DB, &RadioIfGetDB)
        != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfCreateInterface : Value to be set in DB Failed \r\n");
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                         RadioIfGetAllDB.pu1AntennaSelection);
        return OSIX_FAILURE;
    }
    RadioIfInitDefaultValues (&RadioIfGetDB);

    WssPmInfo.u2ProfileId = 0;
    WssPmInfo.u4RadioIfIndex =
        pWssMsgStruct->unRadioIfMsg.RadioIfCreate.u4IfIndex;
    WssPmInfo.stats_timer = WSS_PM_DEF_STATS_TIMER_PERIOD;

    if (pmWTPInfoNotify (WTPPRF_ADD, &WssPmInfo) != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "CreateProfile : Update PM Stats DB Failed\n");
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                         RadioIfGetAllDB.pu1AntennaSelection);
        return OSIX_FAILURE;
    }

    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                     RadioIfGetAllDB.pu1AntennaSelection);
    RADIOIF_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfDFSChannelUpdate                              *
 *                                                                           *
 * Description        : Function to return the default values in case of host*
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to input structure           *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT4
RadioIfDFSChannelUpdate (tRadioIfMsgStruct * pWssMsgStruct)
{
    UINT1               u1Index = 0;
    tRadioIfGetDB       RadioIfGetDB;
    UINT1               au1RadioName[RADIO_INTF_NAME_LEN + 1];
    MEMSET (au1RadioName, 0, RADIO_INTF_NAME_LEN);
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
        pWssMsgStruct->unRadioIfMsg.RadioIfDfsChnlUpdate.u4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bBssIdCount = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                  &RadioIfGetDB) != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfDFSChannelUpdate: DB failed \n");
        return OSIX_FAILURE;
    }

    if (RadioIfGetDB.RadioIfGetAllDB.u1BssIdCount == 0)
    {
        RadioIfGetDB.RadioIfGetAllDB.u1WlanId = 1;
    }
    else
    {
        for (u1Index = 1; u1Index <= RADIO_MAX_BSSID_SUPPORTED; u1Index++)
        {
            RadioIfGetDB.RadioIfGetAllDB.u1WlanId = u1Index;
            if (WssIfProcessRadioIfDBMsg (WSS_GET_WLAN_BSS_IFINDEX_DB,
                                          (&RadioIfGetDB)) == OSIX_SUCCESS)
            {
                if (RadioIfGetDB.RadioIfGetAllDB.u4BssIfIndex != 0)
                    break;
            }
            else
                continue;
        }
    }
#ifdef NPAPI_WANTED
    tFsHwNp             FsHwNp;
    UINT1               au1Wname[WLAN_NAME_MAX_LEN];

    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));
    MEMSET (au1Wname, 0, WLAN_NAME_MAX_LEN);

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_DFS_ADD_BEACON_FRAMES,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    SPRINTF ((CHR1 *) au1RadioName, "%s%d", RADIO_INTF_NAME,
             RadioIfGetDB.RadioIfGetAllDB.u1RadioId - 1);

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11Channel.RadInfo.
        u4RadioIfIndex =
        pWssMsgStruct->unRadioIfMsg.RadioIfDfsChnlUpdate.u4IfIndex;

    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11Channel.RadInfo.
            au1RadioIfName, au1RadioName, STRLEN (au1RadioName));

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11Channel.
        RadInfo.au2WlanIfIndex[0] = RadioIfGetDB.RadioIfGetAllDB.u1WlanId;

    SPRINTF ((CHR1 *) au1Wname, "%s%d", RADIO_ATH_INTF_NAME,
             RadioIfGetDB.RadioIfGetAllDB.u1WlanId - 1);
    STRCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11Channel.
            RadInfo.au1WlanIfname[0], au1Wname);

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11Channel.u2ChannelNum =
        pWssMsgStruct->unRadioIfMsg.RadioIfDfsChnlUpdate.u1CurrentChannel;
    /*To Set in the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "NpUtilHwProgram( CSA ): Values to be set "
                     "in Hw failed\r\n");
    }
#endif
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfDFSRadarStatusUpdate                         *
 *                                                                           *
 * Description        : Function to return the default values in case of host*
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to input structure           *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT4
RadioIfDFSRadarStatusUpdate (tRadioIfMsgStruct * pWssMsgStruct)
{
    UINT1               u1Index = 0;
    tRadioIfGetDB       RadioIfGetDB;
    UINT1               au1RadioName[RADIO_INTF_NAME_LEN + 1];
    MEMSET (au1RadioName, 0, RADIO_INTF_NAME_LEN);
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
        pWssMsgStruct->unRadioIfMsg.RadioIfRadarStatUpdate.u4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bBssIdCount = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                  &RadioIfGetDB) != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfDFSRadarStatusUpdate: DB failed \n");
        return OSIX_FAILURE;
    }

    if (RadioIfGetDB.RadioIfGetAllDB.u1BssIdCount == 0)
    {
        RadioIfGetDB.RadioIfGetAllDB.u1WlanId = 1;
    }
    else
    {
        for (u1Index = 1; u1Index <= RADIO_MAX_BSSID_SUPPORTED; u1Index++)
        {
            RadioIfGetDB.RadioIfGetAllDB.u1WlanId = u1Index;
            if (WssIfProcessRadioIfDBMsg (WSS_GET_WLAN_BSS_IFINDEX_DB,
                                          (&RadioIfGetDB)) == OSIX_SUCCESS)
            {
                if (RadioIfGetDB.RadioIfGetAllDB.u4BssIfIndex != 0)
                    break;
            }
            else
                continue;
        }
    }

#ifdef NPAPI_WANTED
    tFsHwNp             FsHwNp;
    UINT1               au1Wname[WLAN_NAME_MAX_LEN];

    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));
    MEMSET (au1Wname, 0, WLAN_NAME_MAX_LEN);

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_DFS_UPDATE_RADAR_STAT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    SPRINTF ((CHR1 *) au1RadioName, "%s%d", RADIO_INTF_NAME,
             RadioIfGetDB.RadioIfGetAllDB.u1RadioId - 1);

    FsHwNp.RadioIfNpModInfo.unOpCode.sRadarStatus.RadInfo.
        u4RadioIfIndex =
        pWssMsgStruct->unRadioIfMsg.RadioIfRadarStatUpdate.u4IfIndex;

    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sRadarStatus.RadInfo.
            au1RadioIfName, au1RadioName, STRLEN (au1RadioName));

    FsHwNp.RadioIfNpModInfo.unOpCode.sRadarStatus.
        RadInfo.au2WlanIfIndex[0] = RadioIfGetDB.RadioIfGetAllDB.u1WlanId;

    SPRINTF ((CHR1 *) au1Wname, "%s%d", RADIO_ATH_INTF_NAME,
             RadioIfGetDB.RadioIfGetAllDB.u1WlanId - 1);
    STRCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sRadarStatus.
            RadInfo.au1WlanIfname[0], au1Wname);
    FsHwNp.RadioIfNpModInfo.unOpCode.sRadarStatus.u1RadarStatus = OSIX_TRUE;

    /*To Set in the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "NpUtilHwProgram: Values to be set " "in Hw failed\r\n");
    }
#endif

    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : RadioIfGetMaxTxPowerLevel2G                          *
 *                                                                           *
 * Description        : Wrapper Func through which the Power level will be   *
 *                      received for Ratio Type a/n                          *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to input structure           *
 *                      pu1RadioName - Radio Interface Name                  *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfGetChannelWidth (tRadioIfGetDB * pRadioIfGetDB, UINT1 *pu1RadioName)
{
#ifdef NPAPI_WANTED
    tFsHwNp             FsHwNp;
    UINT1               au1Wname[WLAN_NAME_MAX_LEN];

    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));
    MEMSET (au1Wname, 0, WLAN_NAME_MAX_LEN);

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_GET_OFDM_CHANNEL_WIDTH,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    FsHwNp.RadioIfNpModInfo.unOpCode.sFsHwGetOFDMChannelWidth.RadInfo.
        u4RadioIfIndex = pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetDot11MaxTxPow5G.RadInfo.
            au1RadioIfName, pu1RadioName, STRLEN (pu1RadioName));
    FsHwNp.RadioIfNpModInfo.unOpCode.sRadarStatus.
        RadInfo.au2WlanIfIndex[0] = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;

    SPRINTF ((CHR1 *) au1Wname, "%s%d", RADIO_ATH_INTF_NAME,
             pRadioIfGetDB->RadioIfGetAllDB.u1WlanId - 1);
    STRCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sRadarStatus.
            RadInfo.au1WlanIfname[0], au1Wname);

    /*To Get from the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "NpUtilHwProgram : Values to get CH Width failed\r\n");
        return OSIX_FAILURE;
    }

    pRadioIfGetDB->RadioIfIsGetAllDB.bMaxTxPowerLevel = OSIX_TRUE;
#else
    UNUSED_PARAM (pRadioIfGetDB);
    UNUSED_PARAM (pu1RadioName);
#endif
    return OSIX_SUCCESS;
}

UINT1
RadioIfGetShortGi (tRadioIfGetDB * pRadioIfGetDB, UINT1 *pu1RadioName)
{
#ifdef NPAPI_WANTED
    tFsHwNp             FsHwNp;
    UINT1               au1Wname[WLAN_NAME_MAX_LEN];

    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));
    MEMSET (au1Wname, 0, WLAN_NAME_MAX_LEN);

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_GET_SHORT_GI,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    FsHwNp.RadioIfNpModInfo.unOpCode.sFsHwGetOFDMChannelWidth.RadInfo.
        u4RadioIfIndex = pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetDot11MaxTxPow5G.RadInfo.
            au1RadioIfName, pu1RadioName, STRLEN (pu1RadioName));
    FsHwNp.RadioIfNpModInfo.unOpCode.sRadarStatus.
        RadInfo.au2WlanIfIndex[0] = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;

    SPRINTF ((CHR1 *) au1Wname, "%s%d", RADIO_ATH_INTF_NAME,
             pRadioIfGetDB->RadioIfGetAllDB.u1WlanId - 1);
    STRCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sRadarStatus.
            RadInfo.au1WlanIfname[0], au1Wname);

    /*To Get from the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "NpUtilHwProgram : Values to get CH Width failed\r\n");
        return OSIX_FAILURE;
    }

    pRadioIfGetDB->RadioIfIsGetAllDB.bMaxTxPowerLevel = OSIX_TRUE;
#else
    UNUSED_PARAM (pRadioIfGetDB);
    UNUSED_PARAM (pu1RadioName);
#endif
    return OSIX_SUCCESS;
}

#ifdef PMF_WANTED
/*****************************************************************************
 * Function Name      : RadioIfGetSeqNum                                     *
 *                                                                           *
 * Description        : Wrapper Func through which the IGTK Packet number    *
 *                      will be received from NPAPI.                         *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - Pointer to input structure           *
 *                      pu1RadioName - Radio Interface Name                  *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
RadioIfGetSeqNum (tRadioIfGetDB * pRadioIfGetDB, INT4 i4IGTKKeyIndex,
                  UINT1 *pu1IGTKPktNum, UINT1 *pu1RadioName)
{
#ifdef NPAPI_WANTED
    tFsHwNp             FsHwNp;
    UINT1               au1Wname[WLAN_NAME_MAX_LEN];

    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));
    MEMSET (au1Wname, 0, sizeof (WLAN_NAME_MAX_LEN));

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RADIO_MODULE,    /* Module ID */
                         FS_RADIO_HW_GET_RSNA_SEQ_NO,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetSeqNum.
        RadInfo.u4RadioIfIndex = pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;

    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetSeqNum.
            RadInfo.au1RadioIfName, pu1RadioName, STRLEN (pu1RadioName));

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetSeqNum.
        RadInfo.au2WlanIfIndex[0] = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;

    SPRINTF ((CHR1 *) au1Wname, "%s%d", RADIO_ATH_INTF_NAME,
             pRadioIfGetDB->RadioIfGetAllDB.u1WlanId - 1);

    STRCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetSeqNum.
            RadInfo.au1WlanIfname[0], au1Wname);

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetSeqNum.i4KeyIndex =
        i4IGTKKeyIndex;

    MEMSET (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetSeqNum.au1IGTKPktNum, 0,
            6);

    /*To Get from the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "NpUtilHwProgram: Values to get from Hw failed\n");
        return OSIX_FAILURE;
    }

    MEMCPY (pu1IGTKPktNum,
            FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetSeqNum.au1IGTKPktNum, 6);
#else
    UNUSED_PARAM (pRadioIfGetDB);
    UNUSED_PARAM (i4IGTKKeyIndex);
    UNUSED_PARAM (pu1IGTKPktNum);
    UNUSED_PARAM (pu1RadioName);
#endif
    return OSIX_SUCCESS;
}

#endif
#endif
