#!/bin/csh
# Copyright (C) 2006 Aricent Inc . All Rights Reserved
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : MAKE.H                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       : Aricent Inc.                     |
# |                                                                          |
# |   MAKE TOOL(S) USED      : Eg: GNU MAKE                                  |
# |                                                                          |
# |   TARGET ENVIRONMENT     : LINUX ( Slackware 1.2.1 )                     |
# |                                                                          |
# |   $Id: make.h,v 1.2 2017/05/23 14:16:47 siva Exp $                                                                  |
# |                                                                          |
# |   DATE                   : 07th Mar 2002                                 |
# |                                                                          |
# |   DESCRIPTION            : Provide the following information in order -  |
# |                            1. Number of Submodules present if Main       |
# |                               makefile.                                  |
# |                            2. Clean option                               |
# +--------------------------------------------------------------------------+

###########################################################################
#               COMPILATION SWITCHES                                      #
###########################################################################

APHDLR_SWITCHES = -DRAD_TEST

TOTAL_OPNS = ${APHDLR_SWITCHES} $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES)

############################################################################
#                         Directories                                      #
############################################################################

CFA_BASE_DIR   = ${BASE_DIR}/cfa2

APHDLR_BASE_DIR      = ${BASE_DIR}/wss/aphdlr
APHDLR_SRC_DIR       = ${APHDLR_BASE_DIR}/src
APHDLR_INC_DIR       = ${APHDLR_BASE_DIR}/inc
APHDLR_OBJ_DIR       = ${APHDLR_BASE_DIR}/obj
WSSIF_INC_DIR        = ${BASE_DIR}/wss/wssif/inc
CAPWAP_INC_DIR       = ${BASE_DIR}/wss/capwap/inc
WSSCFG_INC_DIR       = ${BASE_DIR}/wss/wsscfg/inc
RADIOIF_INC_DIR      = ${BASE_DIR}/wss/radioif/inc
WSSWLAN_INC_DIR      = ${BASE_DIR}/wss/wsswlan/inc
BCNMGR_INC_DIR       = ${BASE_DIR}/wss/bcnmgr/inc
CAPWAP_INC_DIR       = ${BASE_DIR}/wss/capwap/inc
WSSPM_INC_DIR       = ${BASE_DIR}/wss/wsspm/inc
WSSWLC_INC_DIR      = ${BASE_DIR}/wss/wlchdlr/inc
SNMP_INCL_DIR      = ${BASE_DIR}/inc/snmp
CFA_INCD           = ${CFA_BASE_DIR}/inc
CMN_INC_DIR        = ${BASE_DIR}/inc
DHCPS_INC_DIR        = ${BASE_DIR}/dhcp/dhcpsrv/inc
DHCPR_INC_DIR        = ${BASE_DIR}/dhcp/dhcprelay/inc
FWL_INC_DIR    = ${BASE_DIR}/firewall/inc
NETIP_INC_DIR    = ${BASE_DIR}/netip/ipmgmt/inc
DTLS_INC_DIR    = ${BASE_DIR}/wss/dtls/inc 
WSSWLAN_INC_DIR = ${BASE_DIR}/wss/wsswlan/inc
RFMGMT_INC_DIR       = ${BASE_DIR}/wss/rfmgmt/inc
ISS_INC_DIR = ${BASE_DIR}/ISS/common/system/inc
FCAPWAP_INC_DIR = ${BASE_DIR}/wss/fcapwap/inc

############################################################################
##                     INCLUDE OPTIONS                                    ##
############################################################################

GLOBAL_INCLUDES  =-I${DTLS_INC_DIR} -I${CFA_INCD}  -I${APHDLR_INC_DIR} -I${WSSIF_INC_DIR}  -I${CAPWAP_INC_DIR} -I${WSSMAC_INC_DIR} -I${WSSWLAN_INC_DIR}    -I${RADIOIF_INC_DIR}    -I${BCNMGR_INC_DIR} -I${WSSCFG_INC_DIR} -I${WSSPM_INC_DIR} -I${WSSWLC_INC_DIR} -I${WSSWLAN_INC_DIR}  -I${ISS_INC_DIR} -I${RFMGMT_INC_DIR} -I${DHCPS_INC_DIR} -I${DHCPR_INC_DIR} -I${NETIP_INC_DIR} -I${FWL_INC_DIR} -I${FCAPWAP_INC_DIR}
INCLUDES         = ${GLOBAL_INCLUDES} ${COMMON_INCLUDE_DIRS}

#############################################################################
