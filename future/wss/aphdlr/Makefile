#####################################################################
##########################################################################
# Copyright (C) 2010 Aricent Inc . All Rights Reserved                   #
# ------------------------------------------                             #
# $Id: Makefile,v 1.2 2017/05/23 14:16:46 siva Exp $                  #
##########################################################################
####  MAKEFILE HEADER ::                                         ####
##|###############################################################|##
##|    FILE NAME               ::  Makefile                       |##
##|                                                               |##
##|    PRINCIPAL    AUTHOR     ::  Aricent Inc.                   |##
##|                                                               |##
##|    SUBSYSTEM NAME          ::  AP HDLR                        |##
##|                                                               |##
##|    MODULE NAME             ::  APHDLR                         |##
##|                                                               |##
##|    TARGET ENVIRONMENT      ::  Linux                          |##
##|                                                               |##
##|    DATE OF FIRST RELEASE   ::  07 March 2013                  |##
##|                                                               |##
##|            DESCRIPTION     ::                                 |##
##|                                MAKEFILE for FutureAP HDLR    |##
##|                                Final Object                   |##
##|                                                               |##
##|###############################################################|##

.PHONY  : clean
include ../../LR/make.h
include ../../LR/make.rule
include ./make.h



ISS_C_FLAGS           =  ${CC_FLAGS} ${TOTAL_OPNS} ${INCLUDES}
APHDLR_FINAL_OBJ = ${APHDLR_OBJ_DIR}/FutureAPHDLR.o

#object module list

APHDLR_OBJS = \
       ${APHDLR_OBJ_DIR}/aphdlrmain.o\
       ${APHDLR_OBJ_DIR}/aphdlrtmr.o\
       ${APHDLR_OBJ_DIR}/aphdlrsz.o\
       ${APHDLR_OBJ_DIR}/aphdlrport.o\
       ${APHDLR_OBJ_DIR}/aphdlrstation.o\
       ${APHDLR_OBJ_DIR}/aphdlrreset.o\
       ${APHDLR_OBJ_DIR}/aphdlrclearconfig.o\
       ${APHDLR_OBJ_DIR}/aphdlrconfigupdate.o\
       ${APHDLR_OBJ_DIR}/aphdlrwtpevent.o\
       ${APHDLR_OBJ_DIR}/aphdlrpridisc.o\
       ${APHDLR_OBJ_DIR}/aphdlrdata.o


#object module dependency
${APHDLR_FINAL_OBJ} :obj ${APHDLR_OBJS} 
	${LD} ${LD_FLAGS} ${CC_COMMON_OPTIONS} -o ${APHDLR_FINAL_OBJ} ${APHDLR_OBJS}

obj:
	${MKDIR}  $(MKDIR_FLAGS) $(APHDLR_OBJ_DIR) 

EXTERNAL_DEPENDENCIES = \
   ${COMN_INCL_DIR}/lr.h        \
   ${COMN_INCL_DIR}/cfa.h       \
   ${SNMP_INCL_DIR}/snmctdfs.h  \
   ${SNMP_INCL_DIR}/snmccons.h  \
   ${SNMP_INCL_DIR}/snmcdefn.h  \
   ${COMN_INCL_DIR}/../vlangarp/vlan/inc/vlaninc.h

INTERNAL_DEPENDENCIES = \
	${APHDLR_BASE_DIR}/make.h \
	${APHDLR_BASE_DIR}/Makefile 
 
DEPENDENCIES =  \
    ${EXTERNAL_DEPENDENCIES} \
	 ${INTERNAL_DEPENDENCIES} \
	 ${COMMON_DEPENDENCIES}


$(APHDLR_OBJ_DIR)/aphdlrmain.o : \
   $(APHDLR_SRC_DIR)/aphdlrmain.c  \
        ${DEPENDENCIES}
	$(CC) $(ISS_C_FLAGS)  $(APHDLR_SRC_DIR)/aphdlrmain.c -o $@

$(APHDLR_OBJ_DIR)/aphdlrtmr.o : \
   $(APHDLR_SRC_DIR)/aphdlrtmr.c  \
        ${DEPENDENCIES}
	$(CC) $(ISS_C_FLAGS)  $(APHDLR_SRC_DIR)/aphdlrtmr.c -o $@


$(APHDLR_OBJ_DIR)/aphdlrsz.o : \
   $(APHDLR_SRC_DIR)/aphdlrsz.c  \
        ${DEPENDENCIES}
	$(CC) $(ISS_C_FLAGS)  $(APHDLR_SRC_DIR)/aphdlrsz.c -o $@


$(APHDLR_OBJ_DIR)/aphdlrport.o : \
   $(APHDLR_SRC_DIR)/aphdlrport.c  \
        ${DEPENDENCIES}
	$(CC) $(ISS_C_FLAGS)  $(APHDLR_SRC_DIR)/aphdlrport.c -o $@


$(APHDLR_OBJ_DIR)/aphdlrstation.o : \
   $(APHDLR_SRC_DIR)/aphdlrstation.c  \
	${DEPENDENCIES}
	$(CC) $(ISS_C_FLAGS)  $(APHDLR_SRC_DIR)/aphdlrstation.c -o $@

$(APHDLR_OBJ_DIR)/aphdlrreset.o : \
   $(APHDLR_SRC_DIR)/aphdlrreset.c  \
	${DEPENDENCIES}
	$(CC) $(ISS_C_FLAGS)  $(APHDLR_SRC_DIR)/aphdlrreset.c -o $@

$(APHDLR_OBJ_DIR)/aphdlrclearconfig.o : \
   $(APHDLR_SRC_DIR)/aphdlrclearconfig.c  \
	${DEPENDENCIES}
	$(CC) $(ISS_C_FLAGS)  $(APHDLR_SRC_DIR)/aphdlrclearconfig.c -o $@

$(APHDLR_OBJ_DIR)/aphdlrconfigupdate.o : \
   $(APHDLR_SRC_DIR)/aphdlrconfigupdate.c  \
	${DEPENDENCIES}
	$(CC) $(ISS_C_FLAGS)  $(APHDLR_SRC_DIR)/aphdlrconfigupdate.c -o $@

$(APHDLR_OBJ_DIR)/aphdlrwtpevent.o : \
   $(APHDLR_SRC_DIR)/aphdlrwtpevent.c  \
	${DEPENDENCIES}
	$(CC) $(ISS_C_FLAGS)  $(APHDLR_SRC_DIR)/aphdlrwtpevent.c -o $@

$(APHDLR_OBJ_DIR)/aphdlrpridisc.o : \
   $(APHDLR_SRC_DIR)/aphdlrpridisc.c \
	${DEPENDENCIES}     
	$(CC) $(ISS_C_FLAGS)  $(APHDLR_SRC_DIR)/aphdlrpridisc.c -o $@ 

$(APHDLR_OBJ_DIR)/aphdlrdata.o : \
   $(APHDLR_SRC_DIR)/aphdlrdata.c  \
	${DEPENDENCIES}
	$(CC) $(ISS_C_FLAGS)  $(APHDLR_SRC_DIR)/aphdlrdata.c -o $@
clean:
	rm -rf ${APHDLR_OBJ_DIR}/*.o
#	$(RM) $(RM_FLAGS) ${APHDLR_OBJ_DIR}/*.o

pkg:
	CUR_PWD=${PWD};\
        cd ${BASE_DIR}/../..;\
        tar cvzf $(BASE_DIR)/../../aphdlr.tgz -T ${BASE_DIR}/aphdlr/FILES.NEW;\
        cd ${CUR_PWD};

