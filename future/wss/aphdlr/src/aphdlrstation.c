/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: aphdlrstation.c,v 1.5 2017/11/24 10:37:01 siva Exp $
 *
 * Description: This file contains the APHDLR Station related routines.
 *
 *****************************************************************************/

#ifndef __CAPWAP_STATION_C__
#define __CAPWAP_STATION_C__

#include "aphdlrinc.h"
#ifdef BAND_SELECT_WANTED
#include "radioifinc.h"
#include "wssifstawtpprot.h"
#endif

/************************************************************************/
/*  Function Name   : CapwapProcessConfigStationReq                     */
/*  Description     : The function processes the received CAPWAP        */
/*                    Config Station request                            */
/*  Input(s)        : ConfigStationReqMsg -CAPWAP Station config request*/
/*                                            packet received           */
/*                    pSessEntry - Pointer to the session data base     */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS | OSIX_FAILURE                       */
/************************************************************************/
INT4
CapwapProcessConfigStationReq (tSegment * pBuf,
                               tCapwapControlPacket * pconfigStationReqMsg,
                               tRemoteSessionManager * pSessEntry)
{
    INT4                i4Status = OSIX_SUCCESS;
    tStationConfRsp     ConfigStationResp;
    UINT1              *pu1TxBuf = NULL, *pRcvdBuf = NULL;
    UINT1               u1SeqNum = 0;
    UINT4               u4MsgLen = 0;
    tCRU_BUF_CHAIN_HEADER *pTxBuf = NULL;
    UINT1               u1IsNotLinear = OSIX_FALSE;
    UINT2               u2PacketLen = 0;

    APHDLR_FN_ENTRY ();
    if (pSessEntry == NULL)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC,
                    "APHDLR Session Entry is NULL, return FAILURE \r\n");
        return OSIX_FAILURE;
    }
    if (pSessEntry->eCurrentState != CAPWAP_RUN)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC, "Session not in RUN state \r\n");
        return OSIX_FAILURE;
    }
    MEMSET (&ConfigStationResp, 0, sizeof (tStationConfRsp));

    u2PacketLen = (UINT2) APHDLR_GET_BUF_LEN (pBuf);
    pRcvdBuf = APHDLR_BUF_IF_LINEAR (pBuf, 0, u2PacketLen);

    if (pRcvdBuf == NULL)
    {
        pRcvdBuf = (UINT1 *) MemAllocMemBlk (APHDLR_PKTBUF_POOLID);
        CAPWAP_COPY_FROM_BUF (pBuf, pRcvdBuf, 0, u2PacketLen);
        u1IsNotLinear = OSIX_TRUE;
    }

    /* Recieved sequence number */
    u1SeqNum = pconfigStationReqMsg->capwapCtrlHdr.u1SeqNum;

    /* Validate the Conf Station request message. if it is success 
       construct the Config Status Response */
    if ((CapValidateConfStatReqMsgElems (pRcvdBuf, pconfigStationReqMsg,
                                         &ConfigStationResp) == OSIX_SUCCESS))
    {

        /* As Validtion is successful set the result code to SUCCESS */
        ConfigStationResp.resultCode.u4Value = CAPWAP_SUCCESS;
        ConfigStationResp.resultCode.u2MsgEleType = RESULT_CODE;
        ConfigStationResp.resultCode.u2MsgEleLen = RESULT_CODE_LEN;
        ConfigStationResp.u2CapwapMsgElemenLen = (UINT2)
            (ConfigStationResp.u2CapwapMsgElemenLen +
             ConfigStationResp.resultCode.u2MsgEleLen +
             CAPWAP_MSG_ELEM_TYPE_LEN);
    }
    else
    {
        ConfigStationResp.resultCode.u4Value = CAPWAP_FAILURE;
        ConfigStationResp.resultCode.u2MsgEleType = RESULT_CODE;
        ConfigStationResp.resultCode.u2MsgEleLen = RESULT_CODE_LEN;
        ConfigStationResp.u2CapwapMsgElemenLen = (UINT2)
            (ConfigStationResp.u2CapwapMsgElemenLen +
             ConfigStationResp.resultCode.u2MsgEleLen +
             CAPWAP_MSG_ELEM_TYPE_LEN);
    }

    if (u1IsNotLinear == OSIX_TRUE)
    {
        MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pRcvdBuf);
        u1IsNotLinear = OSIX_FALSE;
    }

    if (CapwapConstructCpHeader (&ConfigStationResp.capwapHdr) == OSIX_FAILURE)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC, "capwapProcessConfigStationRequest: "
                    "Failed to construct the capwap Header\n");
        return OSIX_FAILURE;
    }
    /* get message elements to construct the config status response */
    /* get Mandatory message elements */

    /* get optional message elements */

#ifdef PMF_DEBUG_WANTED
    ConfigStationResp.vendSpec.isOptional = OSIX_TRUE;
    ConfigStationResp.vendSpec.u2MsgEleType = VENDOR_SPECIFIC_PAYLOAD;
    ConfigStationResp.vendSpec.elementId = VENDOR_PMF_TYPE;

    if ((CapwapGetVendorPayload (&ConfigStationResp.vendSpec, &u4MsgLen)
         == OSIX_FAILURE))
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC,
                    "Failed to get Optional Station Configuration Response Message"
                    " Elements \r\n");
        return OSIX_FAILURE;

    }
#endif
    /* update Control Header */
    ConfigStationResp.u1SeqNum = pconfigStationReqMsg->capwapCtrlHdr.u1SeqNum;
    ConfigStationResp.u2CapwapMsgElemenLen = (UINT2)
        (ConfigStationResp.u2CapwapMsgElemenLen +
         u4MsgLen + CAPWAP_MSG_ELEM_PLUS_SEQ_LEN);
    ConfigStationResp.u1NumMsgBlocks = 0;    /* flags always zero */

    /* Allocate memory for packet linear buffer */
    pTxBuf = CAPWAP_ALLOCATE_CRU_BUF (CAPWAP_MAX_PKT_LEN, 0);
    if (pTxBuf == NULL)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC, "Memory Allocation Failed\n");
        return OSIX_FAILURE;
    }
    pu1TxBuf = APHDLR_BUF_IF_LINEAR (pTxBuf, 0, CAPWAP_MAX_PKT_LEN);
    if (pu1TxBuf == NULL)
    {
        pu1TxBuf = (UINT1 *) MemAllocMemBlk (APHDLR_PKTBUF_POOLID);
        if (pu1TxBuf == NULL)
        {
            CAPWAP_RELEASE_CRU_BUF (pTxBuf);
            APHDLR_TRC (APHDLR_FAILURE_TRC, "Memory Allocation Failed for "
                        "pu1TxBuf \n");
            return OSIX_FAILURE;
        }
        u1IsNotLinear = OSIX_TRUE;
    }

    if ((CapwapAssembleConfigStationResp
         (ConfigStationResp.u2CapwapMsgElemenLen, &ConfigStationResp,
          pu1TxBuf)) == OSIX_FAILURE)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC,
                    "Failed to assemble theConfig Station Response Message \r\n");
        if (u1IsNotLinear == OSIX_TRUE)
        {
            MemReleaseMemBlock (APHDLR_PKTBUF_POOLID, pu1TxBuf);
        }
        CAPWAP_RELEASE_CRU_BUF (pTxBuf);
        return OSIX_FAILURE;
    }

    if (u1IsNotLinear == OSIX_TRUE)
    {
        APHDLR_COPY_TO_BUF (pTxBuf, pu1TxBuf, 0, CAPWAP_MAX_PKT_LEN);
        MemReleaseMemBlock (APHDLR_PKTBUF_POOLID, pu1TxBuf);
    }

    /* Transmit Configuration Status Response Message */
    i4Status = CapwapTxMessage (pTxBuf,
                                pSessEntry,
                                (UINT4) (ConfigStationResp.
                                         u2CapwapMsgElemenLen +
                                         CAPWAP_CMN_HDR_LEN),
                                WSS_CAPWAP_802_11_CTRL_PKT);

    if (i4Status == OSIX_SUCCESS)
    {
        /* Update the last transmitted sequence number */
        pSessEntry->lastProcesseddSeqNum = ConfigStationResp.u1SeqNum;
        pSessEntry->lastTransmitedSeqNum++;
    }
    else
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC, "Failed to transmit the packet \r\n");
        CAPWAP_RELEASE_CRU_BUF (pTxBuf);
        pSessEntry->lastTransmittedPkt = NULL;
        pSessEntry->lastTransmittedPktLen = 0;
        return OSIX_FAILURE;
    }
    CAPWAP_RELEASE_CRU_BUF (pTxBuf);
    pSessEntry->lastTransmittedPkt = NULL;
    pSessEntry->lastTransmittedPktLen = 0;

    APHDLR_FN_EXIT ();
    UNUSED_PARAM (u1SeqNum);
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapValidateConfStatReqMsgElems                    */
/*  Description     : The function validates the functional             */
/*                    characteristics of the received CAPWAP            */
/*                    Station Configuaration Request                    */
/*  Input(s)        : ConfigStationReqMsg - parsed CAPWAP Station Config*/
/*                      request                                         */
/*                    pRcvBuf - Received Station Config packet          */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
CapValidateConfStatReqMsgElems (UINT1 *pRcvBuf,
                                tCapwapControlPacket * pParseMsg,
                                tStationConfRsp * ConfigStationResp)
{
    UINT2               u2NumOptionalMsg = 0, u2MsgType = 0;
    UINT1               u1Index = 0;

    APHDLR_FN_ENTRY ();

    u2NumOptionalMsg = pParseMsg->numOptionalElements;

    /* Validate IEEE radio optional elements */
    for (u1Index = 0; u1Index < u2NumOptionalMsg; u1Index++)
    {
        /* Get the message element type */
        u2MsgType = pParseMsg->
            capwapMsgElm[MAX_CONF_STATION_REQ_MAND_MSG_ELEMENTS +
                         u1Index].u2MsgType;
        switch (u2MsgType)
        {
            case ADD_STATION:
                if (CapwapSetAddStation (pRcvBuf, pParseMsg, ConfigStationResp,
                                         u1Index) != OSIX_SUCCESS)
                {
                    APHDLR_TRC (APHDLR_FAILURE_TRC,
                                "Failed to Set Add Station \r\n");
                    return OSIX_FAILURE;
                }
                break;
            case DELETE_STATION:
                if (CapwapSetDeleteStation (pRcvBuf, pParseMsg,
                                            ConfigStationResp,
                                            u1Index) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "Failed to Set Delete Station \r\n");
                    return OSIX_FAILURE;
                }
                break;
            case IEEE_STATION:
                if (CapwapSetIeeeStation (pRcvBuf, pParseMsg, ConfigStationResp,
                                          u1Index) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "Failed to Set IEEE STATION \r\n");
                    return OSIX_FAILURE;
                }
                break;
            case IEEE_STATION_SESSION_KEY:
                if (CapwapSetIeeeStaSessKey (pRcvBuf, pParseMsg,
                                             ConfigStationResp,
                                             u1Index) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "Failed to Set IEEE STATION SESSION KEY\r\n");
                    return OSIX_FAILURE;
                }
                break;
            case VENDOR_SPECIFIC_PAYLOAD:
                if (CapwapSetVendorPayload (pRcvBuf, pParseMsg,
                                            ConfigStationResp,
                                            u1Index) == OSIX_FAILURE)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "Failed to Set the Vendor Specific \r\n");
                    return OSIX_FAILURE;
                }
                break;
            default:

                break;
        }
    }

    APHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapSetAddStation                               */
/*  Description     : The function sets the Add Station message element */
/*                    in station DB.                                    */
/*  Input(s)        : pRcvBuf - Received Buf from CAPWAP                */
/*                    pCwMsg  - Capwap Ctrl Msg Struct                  */
/*                    ConfigStationResp - Station Conf Rsp              */
/*                    u2MsgIndex - Message Index                        */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/
INT4
CapwapSetAddStation (UINT1 *pRcvBuf, tCapwapControlPacket * pCwMsg,
                     tStationConfRsp * ConfigStationResp, UINT2 u2MsgIndex)
{
    UNUSED_PARAM (ConfigStationResp);

    unWssMsgStructs     WssMsgStructs;
    UINT4               u4Offset = 0;
    UINT2               u2NumMsgElems = 0;
    UINT1               u1Index = 0;
    UINT1              *pu1Buf = NULL;
#ifdef BAND_SELECT_WANTED
    UINT1               u1Index2 = 0;
    UINT1               au1RadioName[RADIO_INTF_NAME_LEN];
    tRadioIfGetDB       RadioIfGetDB;
    tWssStaBandSteerDB  WssStaBandSteerDB;
#endif

    APHDLR_FN_ENTRY ();

    MEMSET (&WssMsgStructs, 0, sizeof (unWssMsgStructs));
#ifdef BAND_SELECT_WANTED
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    MEMSET (&WssStaBandSteerDB, 0, sizeof (tWssStaBandSteerDB));
#endif

    pu1Buf = pRcvBuf;
    u2NumMsgElems = pCwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;

    for (u1Index = 0; u1Index < u2NumMsgElems; u1Index++)
    {
        u4Offset = pCwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[u1Index];
        pRcvBuf = pu1Buf;
        pRcvBuf += u4Offset;

        CAPWAP_GET_2BYTE (WssMsgStructs.StaInfoAp.AddSta.u2MessageType,
                          pRcvBuf);
        CAPWAP_GET_2BYTE (WssMsgStructs.StaInfoAp.AddSta.u2MessageLength,
                          pRcvBuf);
        CAPWAP_GET_1BYTE (WssMsgStructs.StaInfoAp.AddSta.u1RadioId, pRcvBuf);
        CAPWAP_GET_1BYTE (WssMsgStructs.StaInfoAp.AddSta.u1MacAddrLen, pRcvBuf);
        CAPWAP_GET_NBYTE (WssMsgStructs.StaInfoAp.AddSta.StaMacAddress, pRcvBuf,
                          sizeof (tMacAddr));
        CAPWAP_GET_2BYTE (WssMsgStructs.StaInfoAp.AddSta.u2VlanId, pRcvBuf);

        WssMsgStructs.StaInfoAp.AddSta.isPresent = OSIX_TRUE;

#ifdef BAND_SELECT_WANTED
        MEMCPY (WssStaBandSteerDB.stationMacAddress,
                WssMsgStructs.StaInfoAp.AddSta.StaMacAddress,
                sizeof (tMacAddr));
        while (WssStaUpdateAPBandSteerProcessDB (WSSSTA_GET_NEXT_BAND_STEER_DB,
                                                 &WssStaBandSteerDB) ==
               OSIX_SUCCESS)
        {
            if (MEMCMP (WssMsgStructs.StaInfoAp.DelSta.StaMacAddr,
                        WssStaBandSteerDB.stationMacAddress,
                        sizeof (tMacAddr)) == OSIX_SUCCESS)
            {
                if (WssStaUpdateAPBandSteerProcessDB
                    (WSSSTA_DESTROY_BAND_STEER_DB,
                     &WssStaBandSteerDB) == OSIX_SUCCESS)
                {

                    break;
                    RadioIfGetDB.RadioIfIsGetAllDB.bBssIdCount = OSIX_TRUE;
                    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
                        WssMsgStructs.StaInfoAp.DelSta.u1RadioId +
                        SYS_DEF_MAX_RADIO_INTERFACES;

                    RadioIfGetDB.RadioIfIsGetAllDB.bBssIdCount = OSIX_TRUE;

                    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                                  (&RadioIfGetDB)) !=
                        OSIX_SUCCESS)
                    {
                        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                    "RadioIfProcessConfigUpdateReq : RBTREE get of "
                                    "RadioIf DB failed \r\n");
                        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                         RadioIfGetAllDB.
                                                         pu1AntennaSelection);
                        return OSIX_FAILURE;
                    }

                    if (RadioIfGetDB.RadioIfGetAllDB.u1BssIdCount == 0)
                    {
                        RadioIfGetDB.RadioIfGetAllDB.u1WlanId = 1;
                    }
                    else
                    {
                        for (u1Index2 = 1;
                             u1Index2 <= RADIO_MAX_BSSID_SUPPORTED; u1Index2++)
                        {
                            RadioIfGetDB.RadioIfGetAllDB.u1WlanId = u1Index2;
                            if (WssIfProcessRadioIfDBMsg
                                (WSS_GET_WLAN_BSS_IFINDEX_DB,
                                 (&RadioIfGetDB)) == OSIX_SUCCESS)
                            {
                                if (RadioIfGetDB.RadioIfGetAllDB.u4BssIfIndex !=
                                    0)
                                    break;
                            }
                            else
                                continue;
                        }

                    }
#ifdef NPAPI_WANTED
                    MEMSET (au1RadioName, 0, RADIO_INTF_NAME_LEN);
                    SPRINTF ((CHR1 *) au1RadioName, "%s%d", RADIO_INTF_NAME,
                             WssMsgStructs.StaInfoAp.DelSta.u1RadioId - 1);
#endif
                    RadioIfDelStaBandSteerDB (WssStaBandSteerDB.
                                              stationMacAddress, au1RadioName,
                                              &RadioIfGetDB.RadioIfGetAllDB.
                                              u1WlanId);
                    ApHdlrProbeTmrStop (&WssStaBandSteerDB);
                }
            }

        }
#endif

        if (WssStaWtpProcessWssIfMsg (WSS_STA_VALIDATE_DB, &WssMsgStructs)
            != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "WSS_STA_VALIDATE_DB Failed \r\n");
            return OSIX_FAILURE;
        }
        if (WssStaWtpProcessWssIfMsg (WSS_WTP_STA_CONFIG_REQUEST_AP,
                                      &WssMsgStructs) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "WSS_WTP_STA_CONFIG_REQUEST_AP "
                        "Failed \r\n");
            return OSIX_FAILURE;
        }
    }
    APHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapSetDeleteStation                            */
/*  Description     : The function sets the Del Station message element */
/*                    in station DB.                                    */
/*  Input(s)        : pRcvBuf - Received Buf from CAPWAP                */
/*                    pCwMsg  - Capwap Ctrl Msg Struct                  */
/*                    ConfigStationResp - Station Conf Rsp              */
/*                    u2MsgIndex - Message Index                        */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/
INT4
CapwapSetDeleteStation (UINT1 *pRcvBuf, tCapwapControlPacket * pCwMsg,
                        tStationConfRsp * ConfigStationResp, UINT2 u2MsgIndex)
{
    UNUSED_PARAM (ConfigStationResp);
    unWssMsgStructs     WssMsgStructs;
    UINT4               u4Offset = 0;
    UINT2               u2NumMsgElems = 0;
    UINT1               u1Index = 0;
    UINT1              *pu1Buf = NULL;
#ifdef BAND_SELECT_WANTED
    UINT1               au1RadioName[RADIO_INTF_NAME_LEN];
    tWssStaBandSteerDB  WssStaBandSteerDB;
    tWssWlanDB          WssWlanDB;
    UINT1               u1RadioId = 0;
    UINT1               u1WlanId = 0;
#endif

    APHDLR_FN_ENTRY ();

    MEMSET (&WssMsgStructs, 0, sizeof (unWssMsgStructs));

#ifdef BAND_SELECT_WANTED
    MEMSET (&WssWlanDB, 0, sizeof (tWssWlanDB));
    MEMSET (&WssStaBandSteerDB, 0, sizeof (tWssStaBandSteerDB));
#endif
    pu1Buf = pRcvBuf;
    u2NumMsgElems = pCwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;

    for (u1Index = 0; u1Index < u2NumMsgElems; u1Index++)
    {
        u4Offset = pCwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[u1Index];
        pRcvBuf = pu1Buf;
        pRcvBuf += u4Offset;

        CAPWAP_GET_2BYTE (WssMsgStructs.StaInfoAp.DelSta.u2MessageType,
                          pRcvBuf);
        CAPWAP_GET_2BYTE (WssMsgStructs.StaInfoAp.DelSta.u2MessageLength,
                          pRcvBuf);
        CAPWAP_GET_1BYTE (WssMsgStructs.StaInfoAp.DelSta.u1RadioId, pRcvBuf);
        CAPWAP_GET_1BYTE (WssMsgStructs.StaInfoAp.DelSta.u1MacAddrLen, pRcvBuf);
        CAPWAP_GET_NBYTE (WssMsgStructs.StaInfoAp.DelSta.StaMacAddr,
                          pRcvBuf, sizeof (tMacAddr));
#ifdef BAND_SELECT_WANTED

        if ((WssStaGetRadioInfo (WssMsgStructs.StaInfoAp.DelSta.StaMacAddr,
                                 &u1RadioId, &u1WlanId)) == OSIX_SUCCESS)
        {
            WssWlanDB.WssWlanAttributeDB.u1RadioId = u1RadioId;
            WssWlanDB.WssWlanAttributeDB.u1WlanId = u1WlanId;
            WssWlanDB.WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
            WssWlanDB.WssWlanIsPresentDB.bBssId = OSIX_TRUE;

            if (WssIfProcessWssWlanDBMsg
                (WSS_WLAN_GET_INTERFACE_ENTRY, &WssWlanDB) == OSIX_SUCCESS)
            {
                MEMCPY (&(WssStaBandSteerDB.stationMacAddress),
                        WssMsgStructs.StaInfoAp.DelSta.StaMacAddr,
                        sizeof (tMacAddr));
                MEMCPY (&(WssStaBandSteerDB.BssIdMacAddr),
                        WssWlanDB.WssWlanAttributeDB.BssId, MAC_ADDR_LEN);

                if (WssStaUpdateAPBandSteerProcessDB
                    (WSSSTA_DESTROY_BAND_STEER_DB,
                     &WssStaBandSteerDB) == OSIX_SUCCESS)
                {
#ifdef NPAPI_WANTED
                    MEMSET (au1RadioName, 0, RADIO_INTF_NAME_LEN);
                    SPRINTF ((CHR1 *) au1RadioName, "%s%d", RADIO_INTF_NAME,
                             WssMsgStructs.StaInfoAp.DelSta.u1RadioId - 1);
#endif
                    RadioIfDelStaBandSteerDB (WssStaBandSteerDB.
                                              stationMacAddress, au1RadioName,
                                              &u1WlanId);
                }
            }
        }

#endif
        WssMsgStructs.StaInfoAp.DelSta.isPresent = OSIX_TRUE;

        if (WssStaWtpProcessWssIfMsg (WSS_STA_VALIDATE_DB, &WssMsgStructs)
            != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "WSS_STA_VALIDATE_DB Failed \r\n");
            return OSIX_FAILURE;
        }
        if (WssStaWtpProcessWssIfMsg (WSS_WTP_STA_CONFIG_REQUEST_AP,
                                      &WssMsgStructs) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "WSS_WTP_STA_CONFIG_REQUEST_AP "
                        "Failed \r\n");
            return OSIX_FAILURE;
        }
    }
    APHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapSetIeeeStation                              */
/*  Description     : The function sets the Ieee Station message element*/
/*                    in station DB.                                    */
/*  Input(s)        : pRcvBuf - Received Buf from CAPWAP                */
/*                    pCwMsg  - Capwap Ctrl Msg Struct                  */
/*                    ConfigStationResp - Station Conf Rsp              */
/*                    u2MsgIndex - Message Index                        */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/
INT4
CapwapSetIeeeStation (UINT1 *pRcvBuf, tCapwapControlPacket * pCwMsg,
                      tStationConfRsp * ConfigStationResp, UINT2 u2MsgIndex)
{
    /*UNUSED_PARAM (ConfigStationResp); */
    unWssMsgStructs     WssMsgStructs;
    UINT4               u4Offset = 0;
    UINT2               u2NumMsgElems = 0;
    UINT1               u1Index = 0;
    UINT1              *pu1Buf = NULL;

    APHDLR_FN_ENTRY ();

    MEMSET (&WssMsgStructs, 0, sizeof (unWssMsgStructs));

    pu1Buf = pRcvBuf;
    u2NumMsgElems = pCwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;

    for (u1Index = 0; u1Index < u2NumMsgElems; u1Index++)
    {
        u4Offset = pCwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[u1Index];
        pRcvBuf = pu1Buf;
        pRcvBuf += u4Offset;

        CAPWAP_GET_2BYTE (WssMsgStructs.StaInfoAp.StaMsg.u2MessageType,
                          pRcvBuf);
        CAPWAP_GET_2BYTE (WssMsgStructs.StaInfoAp.StaMsg.u2MessageLength,
                          pRcvBuf);
        CAPWAP_GET_1BYTE (WssMsgStructs.StaInfoAp.StaMsg.u1RadioId, pRcvBuf);
        CAPWAP_GET_2BYTE (WssMsgStructs.StaInfoAp.StaMsg.u2AssocId, pRcvBuf);
        CAPWAP_GET_1BYTE (WssMsgStructs.StaInfoAp.StaMsg.u1Flags, pRcvBuf);
        CAPWAP_GET_NBYTE (WssMsgStructs.StaInfoAp.StaMsg.stationMacAddress,
                          pRcvBuf, sizeof (tMacAddr));
        CAPWAP_GET_2BYTE (WssMsgStructs.StaInfoAp.StaMsg.u2Capab, pRcvBuf);
        CAPWAP_GET_1BYTE (WssMsgStructs.StaInfoAp.StaMsg.u1WlanId, pRcvBuf);
        CAPWAP_GET_NBYTE (WssMsgStructs.StaInfoAp.StaMsg.au1SuppRates,
                          pRcvBuf, WSSSTA_SUPP_RATES_LEN);
        WssMsgStructs.StaInfoAp.StaMsg.isPresent = OSIX_TRUE;

#ifdef PMF_WANTED
        ConfigStationResp->vendSpec.unVendorSpec.VendPMFPktNo.u1RadioId =
            WssMsgStructs.StaInfoAp.StaMsg.u1RadioId;

        ConfigStationResp->vendSpec.unVendorSpec.VendPMFPktNo.u1WlanId =
            WssMsgStructs.StaInfoAp.StaMsg.u1WlanId;
#endif

        if (WssStaWtpProcessWssIfMsg (WSS_STA_VALIDATE_DB, &WssMsgStructs)
            != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "WSS_STA_VALIDATE_DB Failed \r\n");
            return OSIX_FAILURE;
        }
        if (WssStaWtpProcessWssIfMsg (WSS_WTP_STA_CONFIG_REQUEST_AP,
                                      &WssMsgStructs) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "WSS_WTP_STA_CONFIG_REQUEST_AP"
                        " Failed \r\n");
            return OSIX_FAILURE;
        }
    }
    APHDLR_FN_EXIT ();
    UNUSED_PARAM (ConfigStationResp);
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapSetVendorPayload                            */
/*  Description     : The function sets the Vendor Specific payload     */
/*                    message element in station DB.                    */
/*  Input(s)        : pRcvBuf - Received Buf from CAPWAP                */
/*                    pCwMsg  - Capwap Ctrl Msg Struct                  */
/*                    ConfigStationResp - Station Conf Rsp              */
/*                    u2MsgIndex - Message Index                        */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/
INT4
CapwapSetVendorPayload (UINT1 *pRcvBuf, tCapwapControlPacket * pCwMsg,
                        tStationConfRsp * ConfigStationResp, UINT2 u2MsgIndex)
{
    UNUSED_PARAM (ConfigStationResp);
    unWssMsgStructs     WssMsgStructs;
    UINT4               u4Offset = 0;
    UINT2               u2NumMsgElems = 0;
    UINT2               u2ElementType = 0;
    UINT1               u1Index = 0;
    UINT1              *pu1Buf = NULL;

    APHDLR_FN_ENTRY ();

    MEMSET (&WssMsgStructs, 0, sizeof (unWssMsgStructs));

    pu1Buf = pRcvBuf;
    u2NumMsgElems = pCwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;

    for (u1Index = 0; u1Index < u2NumMsgElems; u1Index++)
    {
        u4Offset = pCwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[u1Index];
        pRcvBuf = pu1Buf;
        pRcvBuf += u4Offset;
        pRcvBuf += 4;
        CAPWAP_GET_2BYTE (u2ElementType, (pRcvBuf));

        if (u2ElementType == VENDOR_USERROLE_MSG)
        {
            WssMsgStructs.StaInfoAp.VendSpec.u2MsgEleType = u2ElementType;
            CAPWAP_GET_2BYTE (WssMsgStructs.StaInfoAp.VendSpec.
                              u2MsgEleLen, pRcvBuf);
            CAPWAP_GET_4BYTE (WssMsgStructs.StaInfoAp.VendSpec.
                              Dot11nStaVendor.u4VendorId, pRcvBuf);
            CAPWAP_GET_NBYTE (WssMsgStructs.StaInfoAp.VendSpec.
                              StaMacAddr, pRcvBuf, sizeof (tMacAddr));
            CAPWAP_GET_4BYTE (WssMsgStructs.StaInfoAp.VendSpec.
                              u4BandWidth, pRcvBuf);
            CAPWAP_GET_4BYTE (WssMsgStructs.StaInfoAp.VendSpec.
                              u4DLBandWidth, pRcvBuf);
            CAPWAP_GET_4BYTE (WssMsgStructs.StaInfoAp.VendSpec.
                              u4ULBandWidth, pRcvBuf);
            WssMsgStructs.StaInfoAp.VendSpec.isOptional = OSIX_TRUE;
        }
/*Parsing code for STA configuration request */
        else if (u2ElementType == VENDOR_WMM_MSG)
        {
            WssMsgStructs.StaInfoAp.VendSpec.
                WMMStaVendor.u2MsgEleType = u2ElementType;
            CAPWAP_GET_2BYTE (WssMsgStructs.StaInfoAp.VendSpec.
                              WMMStaVendor.u2MsgEleLen, pRcvBuf);
            CAPWAP_GET_4BYTE (WssMsgStructs.StaInfoAp.VendSpec.
                              WMMStaVendor.u4VendorId, pRcvBuf);

            CAPWAP_GET_NBYTE (WssMsgStructs.StaInfoAp.VendSpec.
                              WMMStaVendor.StaMacAddr, pRcvBuf,
                              sizeof (tMacAddr));
            CAPWAP_GET_1BYTE (WssMsgStructs.StaInfoAp.VendSpec.
                              WMMStaVendor.u1WmmStaFlag, pRcvBuf);
            CAPWAP_GET_2BYTE (WssMsgStructs.StaInfoAp.VendSpec.
                              WMMStaVendor.u2TcpPort, pRcvBuf);
            CAPWAP_GET_4BYTE (WssMsgStructs.StaInfoAp.VendSpec.
                              WMMStaVendor.u4BandwidthThresh, pRcvBuf);
            WssMsgStructs.StaInfoAp.VendSpec.WMMStaVendor.isOptional =
                OSIX_TRUE;
        }

        if (WssStaWtpProcessWssIfMsg (WSS_STA_VALIDATE_DB, &WssMsgStructs)
            != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "WSS_STA_VALIDATE_DB Failed \r\n");
            return OSIX_FAILURE;
        }
        if (WssStaWtpProcessWssIfMsg (WSS_WTP_STA_CONFIG_REQUEST_AP,
                                      &WssMsgStructs) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "WSS_WTP_STA_CONFIG_REQUEST_AP"
                        " Failed \r\n");
            return OSIX_FAILURE;
        }
    }
    APHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapSetIeeeStaSessKey                           */
/*  Description     : The function sets the Station Session Key         */
/*                    message element in station DB.                    */
/*  Input(s)        : pRcvBuf - Received Buf from CAPWAP                */
/*                    pCwMsg  - Capwap Ctrl Msg Struct                  */
/*                    ConfigStationResp - Station Conf Rsp              */
/*                    u2MsgIndex - Message Index                        */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/
INT4
CapwapSetIeeeStaSessKey (UINT1 *pRcvBuf, tCapwapControlPacket * pCwMsg,
                         tStationConfRsp * ConfigStationResp, UINT2 u2MsgIndex)
{
    UNUSED_PARAM (ConfigStationResp);
    unWssMsgStructs     WssMsgStructs;
    UINT4               u4Offset = 0;
    UINT2               u2NumMsgElems = 0;
    UINT1               u1Index = 0;
    UINT1               u1MsgLen = 0;
    UINT1               u1Count = 0;
    UINT1               u1RsnIELength = 0;
    UINT1              *pu1Buf = NULL;

    APHDLR_FN_ENTRY ();

    MEMSET (&WssMsgStructs, 0, sizeof (unWssMsgStructs));

    pu1Buf = pRcvBuf;
    u2NumMsgElems = pCwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;

    for (u1Index = 0; u1Index < u2NumMsgElems; u1Index++)
    {
        u4Offset = pCwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[u1Index];
        pRcvBuf = pu1Buf;
        pRcvBuf += u4Offset;
        u1MsgLen = 0;

        CAPWAP_GET_2BYTE (WssMsgStructs.StaInfoAp.StaSessKey.u2MessageType,
                          pRcvBuf);
        u1MsgLen = (UINT1) (u1MsgLen + 2);

        CAPWAP_GET_2BYTE (WssMsgStructs.StaInfoAp.StaSessKey.u2MessageLength,
                          pRcvBuf);
        u1MsgLen = (UINT1) (u1MsgLen + 2);

        CAPWAP_GET_NBYTE (WssMsgStructs.StaInfoAp.StaSessKey.stationMacAddress,
                          pRcvBuf, sizeof (tMacAddr));
        u1MsgLen = (UINT1) (u1MsgLen + sizeof (tMacAddr));

        CAPWAP_GET_2BYTE (WssMsgStructs.StaInfoAp.StaSessKey.u2Flags, pRcvBuf);
        u1MsgLen = (UINT1) (u1MsgLen + 2);

        CAPWAP_GET_NBYTE (WssMsgStructs.StaInfoAp.StaSessKey.au1PairwiseTSC,
                          pRcvBuf, WSSSTA_PAIRWISE_SC_LEN);
        u1MsgLen = (UINT1) (u1MsgLen + WSSSTA_PAIRWISE_SC_LEN);

        CAPWAP_GET_NBYTE (WssMsgStructs.StaInfoAp.StaSessKey.au1PairwiseRSC,
                          pRcvBuf, WSSSTA_PAIRWISE_SC_LEN);
        u1MsgLen = (UINT1) (u1MsgLen + WSSSTA_PAIRWISE_SC_LEN);

        if ((WssMsgStructs.StaInfoAp.StaSessKey.u2MessageLength ==
             (20 + CAPWAP_WEP_40_KEY_LEN))
            || (WssMsgStructs.StaInfoAp.StaSessKey.u2MessageLength ==
                (20 + CAPWAP_WEP_104_KEY_LEN)))
        {
            CAPWAP_GET_NBYTE (WssMsgStructs.StaInfoAp.StaSessKey.au1Key,
                              pRcvBuf,
                              (UINT4) ((WssMsgStructs.StaInfoAp.StaSessKey.
                                        u2MessageLength) - WSSSTA_KEY_LEN));
            u1MsgLen =
                (UINT1) (u1MsgLen +
                         ((WssMsgStructs.StaInfoAp.StaSessKey.u2MessageLength) -
                          WSSSTA_KEY_LEN));
        }
/*change for rsna*/
        if (WssMsgStructs.StaInfoAp.StaSessKey.u2Flags == 0)
        {
            if ((WssMsgStructs.StaInfoAp.StaSessKey.u2MessageLength ==
                 (CAPWAP_RSN_IE_LEN + CAPWAP_RSN_IE_LEN +
                  CAPWAP_RSNA_CCMP_KEY_LEN))
                || (WssMsgStructs.StaInfoAp.StaSessKey.u2MessageLength ==
                    CAPWAP_RSN_IE_LEN + CAPWAP_RSN_IE_LEN +
                    CAPWAP_RSNA_CCMP_KEY_LEN + PMF_TWO_BYTES))
            {
                CAPWAP_GET_NBYTE (WssMsgStructs.StaInfoAp.StaSessKey.au1Key,
                                  pRcvBuf, CAPWAP_RSNA_CCMP_KEY_LEN);

                u1MsgLen = (UINT1) (u1MsgLen + CAPWAP_RSNA_CCMP_KEY_LEN);
            }
            else if ((WssMsgStructs.StaInfoAp.StaSessKey.u2MessageLength ==
                      (CAPWAP_RSN_IE_LEN + CAPWAP_RSN_IE_LEN +
                       CAPWAP_RSNA_TKIP_KEY_LEN))
                     || (WssMsgStructs.StaInfoAp.StaSessKey.u2MessageLength ==
                         CAPWAP_RSN_IE_LEN + CAPWAP_RSN_IE_LEN +
                         CAPWAP_RSNA_TKIP_KEY_LEN + PMF_TWO_BYTES))
            {
                CAPWAP_GET_NBYTE (WssMsgStructs.StaInfoAp.StaSessKey.au1Key,
                                  pRcvBuf, CAPWAP_RSNA_TKIP_KEY_LEN);
                u1MsgLen = (UINT1) (u1MsgLen + CAPWAP_RSNA_TKIP_KEY_LEN);

            }
            else
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "No matches found for message \r\n");
                return OSIX_FAILURE;
            }

        }
        else
        {
#ifdef PMF_WANTED
            if ((WssMsgStructs.StaInfoAp.StaSessKey.u2MessageLength ==
                 (CAPWAP_RSN_IE_LEN + CAPWAP_RSN_IE_LEN + PMF_TWO_BYTES +
                  CAPWAP_RSNA_CCMP_KEY_LEN)))
#else
            if ((WssMsgStructs.StaInfoAp.StaSessKey.u2MessageLength ==
                 (CAPWAP_RSN_IE_LEN + CAPWAP_RSN_IE_LEN +
                  CAPWAP_RSNA_CCMP_KEY_LEN)))
#endif
            {
                CAPWAP_GET_NBYTE (WssMsgStructs.StaInfoAp.StaSessKey.au1Key,
                                  pRcvBuf, CAPWAP_RSNA_CCMP_KEY_LEN);

                u1MsgLen = (UINT1) (u1MsgLen + CAPWAP_RSNA_CCMP_KEY_LEN);
            }
#ifdef PMF_WANTED
            else if ((WssMsgStructs.StaInfoAp.StaSessKey.u2MessageLength ==
                      (CAPWAP_RSN_IE_LEN + CAPWAP_RSN_IE_LEN + PMF_TWO_BYTES +
                       CAPWAP_RSNA_TKIP_KEY_LEN)))
#else
            else if ((WssMsgStructs.StaInfoAp.StaSessKey.u2MessageLength ==
                      (CAPWAP_RSN_IE_LEN + CAPWAP_RSN_IE_LEN +
                       CAPWAP_RSNA_TKIP_KEY_LEN)))
#endif
            {
                CAPWAP_GET_NBYTE (WssMsgStructs.StaInfoAp.StaSessKey.au1Key,
                                  pRcvBuf, CAPWAP_RSNA_TKIP_KEY_LEN);
                u1MsgLen = (UINT1) (u1MsgLen + CAPWAP_RSNA_TKIP_KEY_LEN);

            }
            else
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "No matches found for message \r\n");
                return OSIX_FAILURE;
            }
        }
        u1RsnIELength = 0;
        if (u1MsgLen != WssMsgStructs.StaInfoAp.StaSessKey.u2MessageLength)
        {

            CAPWAP_GET_1BYTE (WssMsgStructs.StaInfoAp.StaSessKey.RsnaIEElements.
                              u1ElemId, pRcvBuf);
            u1RsnIELength = (UINT1) (u1RsnIELength + RSN_IE_ELEMENT_LENGTH);

            CAPWAP_GET_1BYTE (WssMsgStructs.StaInfoAp.StaSessKey.RsnaIEElements.
                              u1Len, pRcvBuf);
            u1RsnIELength = (UINT1) (u1RsnIELength + RSN_IE_LENGTH_FIELD);
            CAPWAP_GET_2BYTE (WssMsgStructs.StaInfoAp.StaSessKey.RsnaIEElements.
                              u2Ver, pRcvBuf);
            u1RsnIELength = (UINT1) (u1RsnIELength + RSN_IE_VERSION_LENGTH);

            CAPWAP_GET_NBYTE (WssMsgStructs.StaInfoAp.StaSessKey.RsnaIEElements.
                              au1GroupCipherSuite, pRcvBuf,
                              RSNA_CIPHER_SUITE_LEN);
            u1RsnIELength = (UINT1) (u1RsnIELength + RSNA_CIPHER_SUITE_LEN);
            CAPWAP_GET_2BYTE (WssMsgStructs.StaInfoAp.StaSessKey.RsnaIEElements.
                              u2PairwiseCipherSuiteCount, pRcvBuf);
            u1RsnIELength =
                (UINT1) (u1RsnIELength + RSNA_PAIRWISE_CIPHER_COUNT);

            for (u1Count = 0;
                 u1Count <
                 WssMsgStructs.StaInfoAp.StaSessKey.RsnaIEElements.
                 u2PairwiseCipherSuiteCount
                 && u1Count < RSNA_PAIRWISE_CIPHER_COUNT; u1Count++)
            {
                CAPWAP_GET_NBYTE (WssMsgStructs.StaInfoAp.StaSessKey.
                                  RsnaIEElements.aRsnaPwCipherDB[u1Count].
                                  au1PairwiseCipher, pRcvBuf,
                                  RSNA_CIPHER_SUITE_LEN);
                u1RsnIELength = (UINT1) (u1RsnIELength + RSNA_CIPHER_SUITE_LEN);
            }

            CAPWAP_GET_2BYTE (WssMsgStructs.StaInfoAp.StaSessKey.RsnaIEElements.
                              u2AKMSuiteCount, pRcvBuf);
            u1RsnIELength = (UINT1) (u1RsnIELength + RSNA_AKM_SUITE_COUNT);

            for (u1Count = 0;
                 u1Count <
                 WssMsgStructs.StaInfoAp.StaSessKey.RsnaIEElements.
                 u2AKMSuiteCount && u1Count < RSNA_AKM_SUITE_COUNT; u1Count++)
            {
                CAPWAP_GET_NBYTE (WssMsgStructs.StaInfoAp.StaSessKey.
                                  RsnaIEElements.aRsnaAkmDB[u1Count].
                                  au1AKMSuite, pRcvBuf, RSNA_AKM_SELECTOR_LEN);
                u1RsnIELength = (UINT1) (u1RsnIELength + RSNA_AKM_SELECTOR_LEN);
            }
            if (u1RsnIELength !=
                WssMsgStructs.StaInfoAp.StaSessKey.RsnaIEElements.u1Len)
            {
                CAPWAP_GET_2BYTE (WssMsgStructs.StaInfoAp.StaSessKey.
                                  RsnaIEElements.u2RsnCapab, pRcvBuf);
                CAPWAP_GET_2BYTE (WssMsgStructs.StaInfoAp.StaSessKey.
                                  RsnaIEElements.u2PmkidCount, pRcvBuf);
                CAPWAP_GET_NBYTE (WssMsgStructs.StaInfoAp.StaSessKey.
                                  RsnaIEElements.aPmkidList, pRcvBuf,
                                  RSNA_PMKID_LEN);
            }
        }

        WssMsgStructs.StaInfoAp.StaSessKey.isPresent = OSIX_TRUE;

        if (WssStaWtpProcessWssIfMsg (WSS_STA_VALIDATE_DB, &WssMsgStructs)
            != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "WSS_STA_VALIDATE_DB Failed \r\n");
            return OSIX_FAILURE;
        }
        if (WssStaWtpProcessWssIfMsg (WSS_WTP_STA_CONFIG_REQUEST_AP,
                                      &WssMsgStructs) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "WSS_WTP_STA_CONFIG_REQUEST_AP Failed \r\n");
            return OSIX_FAILURE;
        }
    }
    APHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapProcessConfigStationResp                    */
/*  Description     : The function is unused in WTP.                    */
/*  Input(s)        : pBuf - Received Buf from CAPWAP                   */
/*                    pConfigStationRespMsg - Station Conf Rsp          */
/*                    pSessEntry - Session Entry                        */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/
/* Dummy Fn in WTP - This Fn is required only in WLC*/
INT4
CapwapProcessConfigStationResp (tSegment * pBuf,
                                tCapwapControlPacket * pConfigStationRespMsg,
                                tRemoteSessionManager * pSessEntry)
{
    APHDLR_FN_ENTRY ();
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pConfigStationRespMsg);
    UNUSED_PARAM (pSessEntry);
    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                "CapwapProcessConfigStationResp - This Fn is invalid in WTP");
    APHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

#endif
