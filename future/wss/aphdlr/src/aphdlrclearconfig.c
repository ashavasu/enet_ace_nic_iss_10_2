/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: aphdlrclearconfig.c,v 1.3 2017/11/24 10:37:01 siva Exp $
 *
 * Description: This file contains the APHDLR Clear config routines.
 *
 *****************************************************************************/
#ifndef __APHDLR_CONFIG_C__
#define __APHDLR_CONFIG_C__

#include "aphdlrinc.h"
#include "wssifinc.h"
#include "wsswlanwtpproto.h"

/************************************************************************/
/*  Function Name   : CapwapProcessClearConfigRequest                   */
/*  Description     : The function processes the received APHDLR        */
/*                    Clear Config request                              */
/*  Input(s)        : clearconfigReqMsg - APHDLR clearconfig request    */
/*                                          packet received             */
/*                    pSessEntry - Pointer to the session data base     */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX SUCCESS | OSIX_FAILURE                       */
/************************************************************************/
INT4
CapwapProcessClearConfigRequest (tSegment * pBuf,
                                 tCapwapControlPacket * pclearconfigReqMsg,
                                 tRemoteSessionManager * pSessEntry)
{
    INT4                i4Status = OSIX_SUCCESS;
    tClearconfigRsp     ClearconfigResp;
    UINT1              *pu1TxBuf = NULL, *pRcvdBuf = NULL;
    UINT1               u1SeqNum = 0;
    UINT4               u4MsgLen = 0;
    tCRU_BUF_CHAIN_HEADER *pTxBuf = NULL;
    UINT1               u1IsNotLinear = OSIX_FALSE;
    UINT2               u2PacketLen = 0;

    APHDLR_FN_ENTRY ();

    MEMSET (&ClearconfigResp, 0, sizeof (tClearconfigRsp));

    if (pSessEntry == NULL)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC,
                    "APHDLR Session Entry is NULL, return FAILURE \r\n");
        return OSIX_FAILURE;
    }

    if (pSessEntry->eCurrentState != CAPWAP_RUN)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC, "Session not in RUN state \r\n");
        return OSIX_FAILURE;
    }

    MEMSET (&ClearconfigResp, 0, sizeof (tClearconfigRsp));

    u2PacketLen = (UINT2) APHDLR_GET_BUF_LEN (pBuf);
    pRcvdBuf = APHDLR_BUF_IF_LINEAR (pBuf, 0, u2PacketLen);
    if (pRcvdBuf == NULL)
    {
        pRcvdBuf = (UINT1 *) MemAllocMemBlk (APHDLR_PKTBUF_POOLID);
        APHDLR_COPY_FROM_BUF (pBuf, pRcvdBuf, 0, u2PacketLen);
        u1IsNotLinear = OSIX_TRUE;
    }

    /* Recieved sequence number */
    u1SeqNum = pclearconfigReqMsg->capwapCtrlHdr.u1SeqNum;

    /* if the message processing is not successful  
       the result code to FAILURE */
    ClearconfigResp.resultCode.u4Value = CAPWAP_FAILURE;
    ClearconfigResp.resultCode.u2MsgEleType = RESULT_CODE;
    ClearconfigResp.resultCode.u2MsgEleLen = RESULT_CODE_LEN;
    ClearconfigResp.u2CapwapMsgElemenLen =
        ClearconfigResp.resultCode.u2MsgEleLen + CAPWAP_MSG_ELEM_TYPE_LEN;

    /* Validate the Clear Config request message. if it is success construct 
       the Config Status Response */
    if ((CapValidateClearConfReqMsgElems (pRcvdBuf, pclearconfigReqMsg) ==
         OSIX_SUCCESS))
    {
        /* Delete all the Stations ans Wlan */
        if (CapwapClearWtpConfig () == OSIX_SUCCESS)
        {
            /* As Validtion is successful set the result code to SUCCESS */
            ClearconfigResp.resultCode.u4Value = CAPWAP_SUCCESS;
            ClearconfigResp.resultCode.u2MsgEleType = RESULT_CODE;
            ClearconfigResp.resultCode.u2MsgEleLen = RESULT_CODE_LEN;
            ClearconfigResp.u2CapwapMsgElemenLen =
                ClearconfigResp.resultCode.u2MsgEleLen
                + CAPWAP_MSG_ELEM_TYPE_LEN;
        }
    }

    if (u1IsNotLinear == OSIX_TRUE)
    {
        MemReleaseMemBlock (APHDLR_PKTBUF_POOLID, pRcvdBuf);
        u1IsNotLinear = OSIX_FALSE;
    }

    if (CapwapConstructCpHeader (&ClearconfigResp.capwapHdr) == OSIX_FAILURE)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC, "capwapProcessClearConfigRequest:"
                    "Failed to construct the capwap Header\n");
        return OSIX_FAILURE;
    }

    /* update Control Header */
    ClearconfigResp.u1SeqNum = u1SeqNum;
    ClearconfigResp.u2CapwapMsgElemenLen += u4MsgLen + APHDLR_MSG_ELEM_FLAG_LEN;
    ClearconfigResp.u1NumMsgBlocks = 0;    /* flags always zero */

    /* Allocate memory for packet linear buffer */
    pTxBuf = CAPWAP_ALLOCATE_CRU_BUF (CAPWAP_MAX_PKT_LEN, 0);
    if (pTxBuf == NULL)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC, "Memory Allocation Failed\n");
        return OSIX_FAILURE;
    }
    pu1TxBuf = APHDLR_BUF_IF_LINEAR (pTxBuf, 0, CAPWAP_MAX_PKT_LEN);
    if (pu1TxBuf == NULL)
    {
        pu1TxBuf = (UINT1 *) MemAllocMemBlk (APHDLR_PKTBUF_POOLID);
        if (pu1TxBuf == NULL)
        {
            APHDLR_TRC (APHDLR_FAILURE_TRC, "Memory Allocation Failed\n");
            CAPWAP_RELEASE_CRU_BUF (pTxBuf);
            return OSIX_FAILURE;
        }

        CAPWAP_COPY_FROM_BUF (pTxBuf, pu1TxBuf, 0, CAPWAP_MAX_PKT_LEN);
        u1IsNotLinear = OSIX_TRUE;
    }

    if ((CapwapAssembleClearConfigResp (ClearconfigResp.u2CapwapMsgElemenLen,
                                        &ClearconfigResp,
                                        pu1TxBuf)) == OSIX_FAILURE)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC, "Failed to assemble the"
                    " Clear Config Response Message \r\n");
        if (u1IsNotLinear == OSIX_TRUE)
        {
            MemReleaseMemBlock (APHDLR_PKTBUF_POOLID, pu1TxBuf);
            u1IsNotLinear = OSIX_FALSE;
        }
        CAPWAP_RELEASE_CRU_BUF (pTxBuf);
        return OSIX_FAILURE;
    }

    if (u1IsNotLinear == OSIX_TRUE)
    {
        APHDLR_COPY_TO_BUF (pTxBuf, pu1TxBuf, 0, CAPWAP_MAX_PKT_LEN);
        MemReleaseMemBlock (APHDLR_PKTBUF_POOLID, pu1TxBuf);
    }

    /* Transmit Configuration Status Response Message */
    i4Status = CapwapTxMessage (pTxBuf,
                                pSessEntry,
                                ClearconfigResp.u2CapwapMsgElemenLen
                                + CAPWAP_CMN_HDR_LEN,
                                WSS_CAPWAP_802_11_CTRL_PKT);
    if (i4Status == OSIX_FAILURE)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC, "Failed to Transmit the packet\n");

        if (u1IsNotLinear == OSIX_TRUE)
        {
            MemReleaseMemBlock (APHDLR_PKTBUF_POOLID, pu1TxBuf);
        }
        APHDLR_RELEASE_CRU_BUF (pTxBuf);
        return (i4Status);
    }
    else
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC, "Transmitted Clear Configuration "
                    "Reponse packet\r\n");
        pSessEntry->lastTransmitedSeqNum++;

        if (u1IsNotLinear == OSIX_TRUE)
        {
            MemReleaseMemBlock (APHDLR_PKTBUF_POOLID, pu1TxBuf);
        }
        APHDLR_RELEASE_CRU_BUF (pTxBuf);
        return OSIX_SUCCESS;
    }

    APHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapClearWtpConfig                              */
/*  Description     : The function is called when the WTP receives      */
/*                    clear configuration request from WLC              */
/*  Input(s)        : None                                              */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/
INT4
CapwapClearWtpConfig ()
{
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tWssWlanMsgStruct  *pWssWlanMsg = NULL;
    tRadioIfGetDB       RadioIfGetDB;
    tWssWlanDB         *pWssWlanDB = NULL;
    unWssMsgStructs     WssStaMsg;
    UINT2               i = 0;
    UINT1               u1Index = 0;
    UINT1               u1InterfaceName[24];
    MEMSET (u1InterfaceName, 0, 24);

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        pWssWlanMsg = (tWssWlanMsgStruct *) (VOID *) UtlShMemAllocWssWlanBuf ();
    if (pWssWlanMsg == NULL)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC,
                    "CapwapClearWtpConfig:- "
                    "UtlShMemAllocWssWlanBuf returned failure\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    pWssWlanDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
    if (pWssWlanDB == NULL)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC,
                    "CapwapClearWtpConfig:- "
                    "UtlShMemAllocWlanDbBuf returned failure\n");
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsg);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (pWssWlanDB, 0, sizeof (tWssWlanDB));
    MEMSET (&WssStaMsg, 0, sizeof (unWssMsgStructs));
    MEMSET (pWssWlanMsg, 0, sizeof (tWssWlanMsgStruct));

    APHDLR_FN_ENTRY ();
    /* Get the radio info */

    for (i = RADIOIF_START_RADIOID; i <= SYS_DEF_MAX_RADIO_INTERFACES; i++)
    {
        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
            SYS_DEF_MAX_ENET_INTERFACES + i;
        /* Check for the available Wlans and delete the stations for
         * each Radio Id + Wlan ID */
        for (u1Index = WSSWLAN_START_WLANID_PER_RADIO;
             u1Index <= WSSWLAN_END_WLANID_PER_RADIO; u1Index++)
        {
#ifdef NPAPI_WANTED
#ifdef QCAX_WANTED
            if (WssWlanGetBrIfName (i, u1Index, u1InterfaceName) ==
                OSIX_SUCCESS)
            {
                CfaGddClearInterface (i, u1Index, u1InterfaceName);
            }
#endif
#endif
            RadioIfGetDB.RadioIfGetAllDB.u1WlanId = u1Index;
            if (WssIfProcessRadioIfDBMsg (WSS_GET_WLAN_BSS_IFINDEX_DB,
                                          &RadioIfGetDB) != OSIX_SUCCESS)
            {
                APHDLR_TRC (APHDLR_DEBUG_TRC,
                            "CapwapClearWtpConfig:Get WlanIfIndex failed\r\n");
                continue;
            }
            pWssWlanDB->WssWlanAttributeDB.u1WlanId = u1Index;
            pWssWlanDB->WssWlanAttributeDB.u1RadioId = i;
            pWssWlanDB->WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
            pWssWlanDB->WssWlanIsPresentDB.bBssId = OSIX_TRUE;
            pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex =
                RadioIfGetDB.RadioIfGetAllDB.u4BssIfIndex;
            if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_INTERFACE_ENTRY,
                                          pWssWlanDB) != OSIX_SUCCESS)
            {
                APHDLR_TRC (APHDLR_DEBUG_TRC,
                            "CapwapClearWtpConfig:Get WlanIfIndex failed\r\n");
                continue;
            }
            /* Delete the stations associated to the Radio Id and
             * Wlan ID by sending a Deauth frame to the station */
            WssStaMsg.WssStaDeauthMsg.u1RadioId = i;
            WssStaMsg.WssStaDeauthMsg.u1WlanId = u1Index;
            MEMCPY (WssStaMsg.WssStaDeauthMsg.BssId,
                    pWssWlanDB->WssWlanAttributeDB.BssId, sizeof (tMacAddr));
            if (WssStaWtpProcessWssIfMsg (WSS_STA_DEAUTH_MSG,
                                          &WssStaMsg) != OSIX_SUCCESS)
            {
                APHDLR_TRC (APHDLR_FAILURE_TRC,
                            "CapwapClearWtpConfig:Station Deauthentication failed\r\n");
            }
            /* Station deletion is successful. Now delete the Wlans */
            /* Delete the WLAN BSSID mapping entry */
            pWssWlanMsg->unWssWlanMsg.WssWlanConfigReq.u1WlanOption
                = WSSWLAN_DEL_REQ;
            pWssWlanMsg->unWssWlanMsg.WssWlanConfigReq.unWlanConfReq.
                WssWlanDeleteReq.u1RadioId =
                pWssWlanDB->WssWlanAttributeDB.u1RadioId;
            pWssWlanMsg->unWssWlanMsg.WssWlanConfigReq.unWlanConfReq.
                WssWlanDeleteReq.u1WlanId =
                pWssWlanDB->WssWlanAttributeDB.u1WlanId;
            if (WssIfProcessWssWlanMsg (WSS_WLAN_CONFIG_REQ,
                                        pWssWlanMsg) != OSIX_SUCCESS)
            {
                APHDLR_TRC (APHDLR_FAILURE_TRC,
                            "CapwapClearWtpConfig: Destroy Wlan failed\n");
            }
        }
    }
    /* Initialize the WTP DB */
    WssIfCapwapDBInit ();
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsg);
    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
    APHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapValidateClearConfReqMsgElems                   */
/*  Description     : The function validates the functional             */
/*                    characteristics of the received APHDLR            */
/*                    Clearconfig request agains the configured         */
/*                    WTP profile                                       */
/*  Input(s)        : clearconfigReqMsg - parsed APHDLR clearconfig     */
/*                                          request                     */
/*                    pRcvBuf - Received Clearconfig packet             */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX SUCCESS | OSIX_FAILURE                       */
/************************************************************************/
INT4
CapValidateClearConfReqMsgElems (UINT1 *pRcvBuf,
                                 tCapwapControlPacket * pclearconfigReqMsg)
{

    UNUSED_PARAM (pRcvBuf);
    UNUSED_PARAM (pclearconfigReqMsg);
    tVendorSpecPayload  vendSpec;

    APHDLR_FN_ENTRY ();
    MEMSET (&vendSpec, 0, sizeof (tVendorSpecPayload));

    /* Validate the mandatory message elements */

#if APHDLR_UNUSED_CODE
    if (CapwapValidateVendSpecPld (pRcvBuf, pclearconfigReqMsg,
                                   &vendSpec,
                                   VENDOR_SPECIFIC_CLEAR_CONFIG_REQ_INDEX) ==
        OSIX_SUCCESS)
    {
        return OSIX_SUCCESS;
    }
#endif
    /* Profile not configured by the operator, increment the error count */
    APHDLR_FN_EXIT ();
    return OSIX_SUCCESS;

}

/************************************************************************/
/*  Function Name   : CapwapProcessClearConfigResp                      */
/*  Description     : The function is unused in WTP                     */
/*  Input(s)        : None                                              */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/
INT4
CapwapProcessClearConfigResp (tSegment * pBuf,
                              tCapwapControlPacket * pclearconfigReqMsg,
                              tRemoteSessionManager * pSessEntry)
{
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pclearconfigReqMsg);
    UNUSED_PARAM (pSessEntry);

    APHDLR_FN_ENTRY ();
    APHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

#endif
