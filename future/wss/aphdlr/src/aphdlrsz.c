/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: aphdlrsz.c,v 1.1.1.1 2016/09/20 10:42:38 siva Exp $
 *
 * Description: This file contains the APHDLR MemPool Init routines.
 *
 *****************************************************************************/

#define _APHDLRSZ_C
#include "aphdlrinc.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
AphdlrSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < APHDLR_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            MemCreateMemPool (FsAPHDLRSizingParams[i4SizingId].u4StructSize,
                              FsAPHDLRSizingParams[i4SizingId].
                              u4PreAllocatedUnits, MEM_DEFAULT_MEMORY_TYPE,
                              &(APHDLRMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            AphdlrSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
AphdlrSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsAPHDLRSizingParams);
    IssSzRegisterModulePoolId (pu1ModName, APHDLRMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
AphdlrSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < APHDLR_MAX_SIZING_ID; i4SizingId++)
    {
        if (APHDLRMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (APHDLRMemPoolIds[i4SizingId]);
            APHDLRMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
