/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: aphdlrwtpevent.c,v 1.1.1.1 2016/09/20 10:42:38 siva Exp $
 *
 * Description: This file contains the APHDLR routines for handling WTP Event.
 *
 *****************************************************************************/
#ifndef __APHDLR_WTP_C__
#define __APHDLR_WTP_C__

#include "aphdlrinc.h"

/************************************************************************/
/*  Function Name   : CapwapProcessWtpEventResponse                     */
/*  Description     : The function processes the received CAPWAP        */
/*                    Wtp Event response.                               */
/*  Input(s)        : WtpEveRespMsg- Wtp response packet received       */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/
INT4
CapwapProcessWtpEventResponse (tCRU_BUF_CHAIN_HEADER * pBuf,
                               tCapwapControlPacket * pWtpEveRespMsg,
                               tRemoteSessionManager * pSessEntry)
{
    UINT1              *pRcvBuf = NULL;
    UINT1               u1IsNotLinear = OSIX_FALSE;
    UINT2               u2PacketLen = 0;

    APHDLR_FN_ENTRY ();

    if (pSessEntry == NULL)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC,
                    "CAPWAP session Entry is Null, Return Failure \r\n");
        return OSIX_FAILURE;
    }

    /* stop the retransmit interval timer and clear the packet buffer pointer */
    if (ApHdlrTmrStop (pSessEntry, APHDLR_RETRANSMIT_INTERVAL_TMR)
        == OSIX_SUCCESS)
    {
        APHDLR_TRC (APHDLR_MGMT_TRC, "Stopped the retransmit timer \r\n");
        /* clear the packet buffer pointer */
        APHDLR_RELEASE_CRU_BUF (pSessEntry->lastTransmittedPkt);
        pSessEntry->lastTransmittedPkt = NULL;
        pSessEntry->lastTransmittedPktLen = 0;
    }

    u2PacketLen = (UINT2) APHDLR_GET_BUF_LEN (pBuf);
    pRcvBuf = APHDLR_BUF_IF_LINEAR (pBuf, 0, u2PacketLen);

    if (pRcvBuf == NULL)
    {
        pRcvBuf = (UINT1 *) MemAllocMemBlk (APHDLR_PKTBUF_POOLID);
        u1IsNotLinear = OSIX_TRUE;
    }

    if ((CapValidateWtpEventRespMsgElems (pRcvBuf, pWtpEveRespMsg)
         == OSIX_FAILURE))
    {

        APHDLR_TRC (APHDLR_FAILURE_TRC,
                    "Failed to Validate the WTP Event Req Msg Elements \r\n");
        if (u1IsNotLinear == OSIX_TRUE)
        {
            MemReleaseMemBlock (APHDLR_PKTBUF_POOLID, (UINT1 *) pRcvBuf);
        }
        return OSIX_FAILURE;
    }
    if (u1IsNotLinear == OSIX_TRUE)
    {
        MemReleaseMemBlock (APHDLR_PKTBUF_POOLID, (UINT1 *) pRcvBuf);
    }

    APHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapValidateWtpEventRespMsgElems                   */
/*  Description     : The function validates the functional             */
/*                    characteristics of the received CAPWAP            */
/*                    Wtp response agains the configured                */
/*                    WTP profile                                       */
/*  Input(s)        : WtpEveRespMsg - parsed CAPWAP Wtp Event response  */
/*                    pRcvBuf - Received Wtp Event packet               */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
CapValidateWtpEventRespMsgElems (UINT1 *pRcvBuf,
                                 tCapwapControlPacket * WtpEveResMsg)
{
    tVendorSpecPayload  vendSpec;

    APHDLR_FN_ENTRY ();
    MEMSET (&vendSpec, 0, sizeof (tVendorSpecPayload));
    UNUSED_PARAM (pRcvBuf);
    UNUSED_PARAM (WtpEveResMsg);

    /* Validate the Optional message elements */
#if APHDLR_UNUSED_CODE
    if ((CapwapValidateVendSpecPld (pRcvBuf, WtpEveResMsg,
                                    &vendSpec,
                                    VENDOR_SPECIFIC_WTP_EVENT_RESP_INDEX) !=
         OSIX_SUCCESS))
    {
        return OSIX_FAILURE;
    }
#endif
    /* Profile not configured by the operator
     * increment the error count*/
    APHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapProcessWtpEventRequest                      */
/*  Description     : The function is unused in WTP.                    */
/*  Input(s)        : pBuf, pWtpEveReqMsg, pSessEntry                   */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/
INT4
CapwapProcessWtpEventRequest (tCRU_BUF_CHAIN_HEADER * pBuf,
                              tCapwapControlPacket * pWtpEveReqMsg,
                              tRemoteSessionManager * pSessEntry)
{
    APHDLR_FN_ENTRY ();
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pWtpEveReqMsg);
    UNUSED_PARAM (pSessEntry);
    APHDLR_FN_EXIT ();
    return OSIX_FAILURE;
}

#endif
