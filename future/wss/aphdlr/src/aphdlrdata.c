/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 * $Id: aphdlrdata.c,v 1.2 2017/03/08 13:46:05 siva Exp $
 * Description: This file contains the APHDLR Data Transfer Request
 *
 *******************************************************************/
#ifndef __APHDLR_DATA_C__
#define __APHDLR_DATA_C__
#include "aphdlrinc.h"

/************************************************************************/
/*  Function Name   : ApHdlrProcessDataTransferRequest                  */
/*  Description     : The function processes the received APHDLR        */
/*                    Data Transfer request                             */
/*  Input(s)        : DataReqMsg - APHDLR Data request packet received  */
/*                    pSessEntry - Pointer to the session data base     */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/

INT4
CapwapProcessDataTransferRequest (tSegment * pBuf,
                                  tCapwapControlPacket * pDataReqMsg,
                                  tRemoteSessionManager * pSessEntry)
{
    INT4                i4Status;
    tDataRsp            DataResp;
    UINT1              *pu1TxBuf = NULL, *pRcvdBuf = NULL;
    UINT1               u1SeqNum = 0;
    UINT4               u4MsgLen = 0;
    tSegment           *pTxBuf = NULL;
    tSegment           *pTxBuf1 = NULL;
    UINT1              *pu1TxBuf1 = NULL;
    UINT1               u1IsNotLinear_Rx = OSIX_FALSE;
    UINT1               u1IsNotLinear_Tx = OSIX_FALSE;
    UINT2               u2PacketLen;
    UINT4               u4Offset;
    tDataReq            DataReq;
    tDataReq            DataReq_data;
    UINT2               u2MsgType = 0;

    APHDLR_FN_ENTRY ();

    MEMSET (&DataResp, 0, sizeof (tDataRsp));
    MEMSET (&DataReq, 0, sizeof (tDataReq));
    MEMSET (&DataReq_data, 0, sizeof (tDataReq));

    if (pSessEntry == NULL)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC, "APHDLR Session Entry is NULL, return"
                    "FAILURE \r\n");
        return OSIX_FAILURE;
    }
    if (pSessEntry->eCurrentState != CAPWAP_RUN)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC, "Session not in RUN state \r\n");
        return OSIX_FAILURE;
    }

    u2PacketLen = (UINT2) APHDLR_GET_BUF_LEN (pBuf);
    pRcvdBuf = APHDLR_BUF_IF_LINEAR (pBuf, 0, u2PacketLen);
    if (pRcvdBuf == NULL)
    {
        pRcvdBuf = (UINT1 *) MemAllocMemBlk (APHDLR_PKTBUF_POOLID);
        if (pRcvdBuf == NULL)
        {
            APHDLR_TRC (APHDLR_FAILURE_TRC, "CapwapProcessDataTransferRequest: "
                        "Memory Allocation Failed\r\n");
            return OSIX_FAILURE;
        }
        APHDLR_COPY_FROM_BUF (pBuf, pRcvdBuf, 0, u2PacketLen);
        u1IsNotLinear_Rx = OSIX_TRUE;
    }

    /* Recieved sequence number */
    u1SeqNum = pDataReqMsg->capwapCtrlHdr.u1SeqNum;

    u2MsgType =
        pDataReqMsg->capwapMsgElm[DATA_TRANSFER_MODE_REQ_INDEX].u2MsgType;
    u4Offset =
        pDataReqMsg->capwapMsgElm[DATA_TRANSFER_MODE_REQ_INDEX].pu2Offset[0];
    pRcvdBuf += u4Offset + 4;

    /* Get the Data Mode */
    if (u2MsgType == DATA_TRANSFER_MODE)
    {
        CAPWAP_GET_1BYTE (DataReq.datatransfermode.DataMode, pRcvdBuf);
    }

    /* Clear the received data memory, as we have already read the data mode */
    if (u1IsNotLinear_Rx == OSIX_TRUE)
    {
        MemReleaseMemBlock (APHDLR_PKTBUF_POOLID, pRcvdBuf);
    }

    /* Reset Read Side Control Parameters */
    CapwapClearDataTransferSessioEntries (pSessEntry);

    /* Get the values to Send the Data Transfer Response */
    if (CapwapGetWtpDataTransferRspMsgElements (&DataResp, &u4MsgLen,
                                                pSessEntry,
                                                DataReq.datatransfermode.
                                                DataMode) == OSIX_SUCCESS)
    {
        /* update Control Header */
        DataResp.u1SeqNum = pDataReqMsg->capwapCtrlHdr.u1SeqNum;
        DataResp.u2CapwapMsgElemenLen = (UINT2)
            (DataResp.u2CapwapMsgElemenLen + u4MsgLen + 3);
        DataResp.u1NumMsgBlocks = 0;    /* flags always zero */

        /* Allocate memory for packet linear buffer */
        pTxBuf = CAPWAP_ALLOCATE_CRU_BUF (CAPWAP_MAX_PKT_LEN, 0);
        if (pTxBuf == NULL)
        {
            APHDLR_TRC (APHDLR_FAILURE_TRC, "Memory Allocation Failed\n");
            return OSIX_FAILURE;
        }
        pu1TxBuf = APHDLR_BUF_IF_LINEAR (pTxBuf, 0, CAPWAP_MAX_PKT_LEN);
        if (pu1TxBuf == NULL)
        {
            pu1TxBuf = (UINT1 *) MemAllocMemBlk (APHDLR_PKTBUF_POOLID);
            if (pu1TxBuf == NULL)
            {
                APHDLR_TRC (APHDLR_FAILURE_TRC, "Memory Allocation Failed\n");
                CAPWAP_RELEASE_CRU_BUF (pTxBuf);
                return OSIX_FAILURE;
            }
            CAPWAP_COPY_FROM_BUF (pTxBuf, pu1TxBuf, 0, CAPWAP_MAX_PKT_LEN);
            u1IsNotLinear_Tx = OSIX_TRUE;
        }

        if ((CapwapAssembleDataTransferResp (DataResp.u2CapwapMsgElemenLen,
                                             &DataResp,
                                             pu1TxBuf)) == OSIX_FAILURE)
        {
            APHDLR_TRC (APHDLR_FAILURE_TRC, "Failed to assemble the Data "
                        "Transfer  Response Message \r\n");
            CAPWAP_RELEASE_CRU_BUF (pTxBuf);
            if (u1IsNotLinear_Tx == OSIX_TRUE)
            {
                MemReleaseMemBlock (APHDLR_PKTBUF_POOLID, pu1TxBuf);
            }
            return OSIX_FAILURE;
        }

        if (u1IsNotLinear_Tx == OSIX_TRUE)
        {
            /* If the CRU buffer memory is not linear */
            APHDLR_COPY_TO_BUF (pTxBuf, pu1TxBuf, 0, CAPWAP_MAX_PKT_LEN);
            MemReleaseMemBlock (APHDLR_PKTBUF_POOLID, pu1TxBuf);
        }

        /* Transmit Data Transfer Response Message */
        i4Status = CapwapTxMessage (pTxBuf, pSessEntry,
                                    (UINT4) (DataResp.u2CapwapMsgElemenLen +
                                    CAPWAP_CMN_HDR_LEN),WSS_CAPWAP_802_11_CTRL_PKT);
        if (i4Status == OSIX_SUCCESS)
        {
            /* Update the last transmitted sequence number */
            pSessEntry->lastProcesseddSeqNum = DataResp.u1SeqNum;
            /* store the packet buffer pointer in
             * the remote session DB copy the pointer */
            pSessEntry->lastTransmittedPkt = pTxBuf;
            pSessEntry->lastTransmittedPktLen = (UINT4)
                (DataResp.u2CapwapMsgElemenLen + CAPWAP_CMN_HDR_LEN);
            pSessEntry->lastTransmitedSeqNum++;
        }
        else
        {
            APHDLR_TRC (APHDLR_FAILURE_TRC, "Failed to transmit the packet"
                        "\r\n");
            CAPWAP_RELEASE_CRU_BUF (pTxBuf);
            pSessEntry->lastTransmittedPkt = NULL;
            pSessEntry->lastTransmittedPktLen = 0;
            return OSIX_FAILURE;
        }
        /* Release the pTxBuf memory before constructing Data Transfer Request
         * with the Crash or Memory Dump Data*/
        CAPWAP_RELEASE_CRU_BUF (pTxBuf);
        pSessEntry->lastTransmittedPkt = NULL;

    }
    else
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC, "capwapProcessDataRequest: Failed to "
                    "Send the Data Transfer Response Message\n");
        return OSIX_FAILURE;
    }

    /* After Sending Data Transfer Response, Send Crash or Memory Dump Data */
    if (DataResp.resultCode.u4Value == CAPWAP_SUCCESS)
    {

        if (CapwapGetWtpDataTransferReqMsgElements (&DataReq_data, pSessEntry,
                                                    DataReq.datatransfermode.
                                                    DataMode) != OSIX_SUCCESS)
        {
            APHDLR_TRC (APHDLR_FAILURE_TRC, "capwapProcessDataRequest: Failure"
                        "to get Data Transfer Request\n");
            return OSIX_FAILURE;
        }

        /* Assemble and Send the Data Transfer Request */
        DataReq_data.u1SeqNum = (UINT1) (pSessEntry->lastTransmitedSeqNum + 1);

        DataReq_data.u2CapwapMsgElemenLen = (UINT2)
            (DataReq_data.u2CapwapMsgElemenLen +
             DataReq_data.datatransferdata.u2MsgEleLen +
             CAPWAP_MSG_ELEM_TYPE_LEN+ CAPWAP_CMN_MSG_ELM_HEAD_LEN);

        /* Allocate memory for packet linear buffer */
        pTxBuf1 = CAPWAP_ALLOCATE_CRU_BUF (DATA_TRANSFER_MAX_BUF_LEN, 0);
        if (pTxBuf1 == NULL)
        {
            APHDLR_TRC (APHDLR_FAILURE_TRC, "Memory Allocation Failed\n");
            return OSIX_FAILURE;
        }

        pu1TxBuf1 = APHDLR_BUF_IF_LINEAR (pTxBuf1, 0,
                                          DATA_TRANSFER_MAX_BUF_LEN);
        if (pu1TxBuf1 == NULL)
        {
            pu1TxBuf1 =
                (UINT1 *) MemAllocMemBlk (APHDLR_DATA_TRANSFER_PKTBUF_POOLID);
            if (pu1TxBuf1 == NULL)
            {
                APHDLR_TRC (APHDLR_FAILURE_TRC, "Memory Allocation Failed\n");
                CAPWAP_RELEASE_CRU_BUF (pTxBuf1);
                return OSIX_FAILURE;
            }
            u1IsNotLinear_Tx = OSIX_TRUE;
        }

        if ((CapwapAssembleDataTransferReq (DataReq_data.u2CapwapMsgElemenLen,
                                            &DataReq_data,
                                            pu1TxBuf1)) != OSIX_SUCCESS)
        {
            APHDLR_TRC (APHDLR_FAILURE_TRC, "Failed to assemble the "
                        "Data Transfer Response Message \r\n");
            CAPWAP_RELEASE_CRU_BUF (pTxBuf1);
            if (u1IsNotLinear_Tx == OSIX_TRUE)
            {
                MemReleaseMemBlock (APHDLR_DATA_TRANSFER_PKTBUF_POOLID,
                                    pu1TxBuf1);
            }
            return OSIX_FAILURE;
        }

        if (u1IsNotLinear_Tx == OSIX_TRUE)
        {
            /* If the CRU buffer memory is not linear */
            APHDLR_COPY_TO_BUF (pTxBuf1, pu1TxBuf1, 0,
                                DataReq_data.u2CapwapMsgElemenLen +
                                CAPWAP_CMN_HDR_LEN );
            MemReleaseMemBlock (APHDLR_DATA_TRANSFER_PKTBUF_POOLID, pu1TxBuf1);
        }

        /* Transmit Data Transfer Request Message */
        i4Status = CapwapTxMessage (pTxBuf1, pSessEntry,
                                    (UINT4) (DataReq_data.u2CapwapMsgElemenLen +
                                     CAPWAP_CMN_HDR_LEN),WSS_CAPWAP_802_11_CTRL_PKT);
        if (i4Status == OSIX_SUCCESS)
        {
            /* Update the last processed sequence number */
            pSessEntry->lastProcesseddSeqNum = DataResp.u1SeqNum;
            pSessEntry->lastTransmitedSeqNum = DataReq_data.u1SeqNum;

            /* Before storing the packet check any packet is already stored in the
             * buffer. If yes then release the buffer and stop the timer. */
            if (pSessEntry->lastTransmittedPkt != NULL)
            {
                if (ApHdlrTmrStop (pSessEntry, APHDLR_RETRANSMIT_INTERVAL_TMR)
                        == OSIX_FAILURE)
                {
                    APHDLR_TRC (APHDLR_FAILURE_TRC, 
                            "Failed to stop the retransmit timer \r\n");
                }
                CAPWAP_RELEASE_CRU_BUF (pSessEntry->lastTransmittedPkt);
                pSessEntry->lastTransmittedPkt = NULL;
                pSessEntry->lastTransmittedPktLen = 0;
                pSessEntry->u1RetransmitCount = 0;
            }
            /* store the packet buffer pointer in
             * the remote session DB copy the pointer */
            pSessEntry->lastTransmittedPkt = pTxBuf1;
            pSessEntry->lastTransmittedPktLen = (UINT4)
                (DataReq_data.u2CapwapMsgElemenLen + 13);
            /*Start the Retransmit timer */
            ApHdlrTmrStart (pSessEntry,
                            APHDLR_RETRANSMIT_INTERVAL_TMR,
                            MAX_RETRANSMIT_INTERVAL_TIMEOUT);
            /* CAPWAP_RELEASE_CRU_BUF (pTxBuf1); */
        }
        else
        {
            APHDLR_TRC (APHDLR_FAILURE_TRC, "Failed to transmit"
                        "the packet \r\n");
            CAPWAP_RELEASE_CRU_BUF (pTxBuf1);
            pSessEntry->lastTransmittedPkt = NULL;
            pSessEntry->lastTransmittedPktLen = 0;
            return OSIX_FAILURE;
        }
    }
    else if (DataResp.resultCode.u4Value == DATATRANSFER_NO_INFO_TO_TRANSFER)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC, "capwapProcessDataRequest: No Crash "
                    "Data to Transfer\n");
    }
    else
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC, "capwapProcessDataRequest: Invalid "
                    "result code\n");
        return OSIX_FAILURE;
    }
    APHDLR_FN_EXIT ();
    UNUSED_PARAM (u1SeqNum);
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapProcessDataTransferResp                     */
/*  Description     : The function processes the received APHDLR        */
/*                    Data response.                                    */
/*  Input(s)        : DataRespMsg- Data response packet received        */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
CapwapProcessDataTransferResp (tSegment * pBuf,
                               tCapwapControlPacket * pDataRespMsg,
                               tRemoteSessionManager * pSessEntry)
{
    INT4                i4Status = OSIX_SUCCESS;
    UINT1              *pRcvdBuf = NULL;
    UINT2               u2PacketLen;
    UINT4               u4Offset;
    UINT1               u1IsNotLinear = OSIX_FALSE;
    tSegment           *pTxBuf1 = NULL;
    UINT1              *pu1TxBuf1 = NULL;
    UINT1               u1IsNotLinear_Tx = OSIX_FALSE;
    tDataRsp            DataResp;
    tDataReq            DataReq_data;

    MEMSET (&DataResp, 0, sizeof (tDataRsp));
    MEMSET (&DataReq_data, 0, sizeof (tDataReq));

    APHDLR_FN_ENTRY ();

    if (pSessEntry == NULL)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC, "CapwapProcessDataTransferResp :: "
                    "CAPWAP Session Entry is NULL, retrun FAILURE \r\n");
        return OSIX_FAILURE;
    }
    if (pSessEntry->eCurrentState != CAPWAP_RUN)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC, "CapwapProcessDataTransferResp :: "
                    "Session not in RUN state \r\n");
        if (ApHdlrTmrStop (pSessEntry, APHDLR_RETRANSMIT_INTERVAL_TMR) ==
            OSIX_FAILURE)
        {
            APHDLR_TRC (APHDLR_FAILURE_TRC, "CapwapProcessDataTransferRequest "
                        ":: Failed to stop the Retransmit Timer \r\n");
            /* CapwapClearDataTransferSessioEntries (pSessEntry); */
            APHDLR_RELEASE_CRU_BUF (pSessEntry->lastTransmittedPkt);
            pSessEntry->lastTransmittedPkt = NULL;
            pSessEntry->lastTransmittedPktLen = 0;
            pSessEntry->u1RetransmitCount = 0;
        }

        return OSIX_FAILURE;
    }

    /* Data Transfer Response Received. Stop the retransmit timer */
    if (ApHdlrTmrStop (pSessEntry, APHDLR_RETRANSMIT_INTERVAL_TMR)
        == OSIX_FAILURE)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC, "CapwapProcessDataTransferRequest :: "
                    "Failed to stop the Retransmit Timer \r\n");
        /* CapwapClearDataTransferSessioEntries (pSessEntry); */
        if (pSessEntry->lastTransmittedPkt != NULL)
        {
        APHDLR_RELEASE_CRU_BUF (pSessEntry->lastTransmittedPkt);
        pSessEntry->lastTransmittedPkt = NULL;
        pSessEntry->lastTransmittedPktLen = 0;
        pSessEntry->u1RetransmitCount = 0;
        }
        return OSIX_FAILURE;
    }

    /* Release the Last Transmited Packet Data */
    if (pSessEntry->lastTransmittedPkt != NULL)
    {
     CAPWAP_RELEASE_CRU_BUF (pSessEntry->lastTransmittedPkt);
     pSessEntry->lastTransmittedPkt = NULL;
     pSessEntry->lastTransmittedPktLen = 0;
     pSessEntry->u1RetransmitCount = 0;
    }

    u2PacketLen = (UINT2) APHDLR_GET_BUF_LEN (pBuf);
    pRcvdBuf = APHDLR_BUF_IF_LINEAR (pBuf, 0, u2PacketLen);
    if (pRcvdBuf == NULL)
    {
        pRcvdBuf = (UINT1 *) MemAllocMemBlk (APHDLR_PKTBUF_POOLID);
        if (pRcvdBuf == NULL)
        {
            APHDLR_TRC (APHDLR_FAILURE_TRC, "CapwapProcessDataTransferRequest "
                        ":: Memory Allocation Failed\r\n");
            return OSIX_FAILURE;
        }
        CAPWAP_COPY_FROM_BUF (pBuf, pRcvdBuf, 0, u2PacketLen);
        u1IsNotLinear = OSIX_TRUE;
    }

    u4Offset =
        pDataRespMsg->capwapMsgElm[RESULT_CODE_DATA_RESP_INDEX].pu2Offset[0];
    pRcvdBuf += u4Offset + 4;

    /* Get the Result Code */
    CAPWAP_GET_4BYTE (DataResp.resultCode.u4Value, pRcvdBuf);

    if (u1IsNotLinear == OSIX_TRUE)
    {
        MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pRcvdBuf);
    }

    if (DataResp.resultCode.u4Value != CAPWAP_SUCCESS &&
        DataResp.resultCode.u4Value != CAPWAP_FAILURE)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC, "CapwapProcessDataTransferRequest :: "
                    "Invalid Result Code Received\r\n");
        return OSIX_FAILURE;
    }

    /* If the Response Code is CAPWAP_FAILURE, clear the read side session 
     * entry for Data Transfer Request */
    if (DataResp.resultCode.u4Value == CAPWAP_FAILURE)
    {
        if (pSessEntry->u1CrashReadFileOpened == OSIX_TRUE)
        {
            if (pSessEntry->readCrashFd != NULL)
            {
                fclose (pSessEntry->readCrashFd);
                pSessEntry->readCrashFd = NULL;
            }
            pSessEntry->u1CrashReadFileOpened = OSIX_FALSE;
        }
        pSessEntry->u4CrashFileReadOffset = 0;
        pSessEntry->i4CrashFileSize = 0;
        pSessEntry->u1CrashEndofFileReached = OSIX_TRUE;
        return OSIX_SUCCESS;
    }

    /* If Crash Data is not completely sent, send the next set of data */
    /* We need to Identify the Data Mode (Crash Data or Memory Data) by the
     * respective End of File Flag from Session Entry */
    if (pSessEntry->u1CrashEndofFileReached != OSIX_TRUE &&
        pSessEntry->u1CrashReadFileOpened == OSIX_TRUE)
    {

        if (CapwapGetWtpDataTransferReqMsgElements (&DataReq_data, pSessEntry,
                                                    DATA_TRANS_MODE_WTP_CRASH)
            != OSIX_SUCCESS)
        {
            APHDLR_TRC (APHDLR_FAILURE_TRC, "capwapProcessDataRequest: Failure"
                        "to get Data Transfer Request\n");
            return OSIX_FAILURE;
        }

        /* Assemble and Send the Data Transfer Request */
        DataReq_data.u1SeqNum = (UINT1) (pSessEntry->lastTransmitedSeqNum + 1);
        /*DataReq_data.u2CapwapMsgElemenLen += 
           DataReq_data.datatransferdata.u2MsgEleLen + CAPWAP_MSG_ELEM_TYPE_LEN; */
        DataReq_data.u2CapwapMsgElemenLen = (UINT2)
            (DataReq_data.u2CapwapMsgElemenLen +
             DataReq_data.datatransferdata.u2MsgEleLen +
             CAPWAP_MSG_ELEM_TYPE_LEN+ CAPWAP_CMN_MSG_ELM_HEAD_LEN);
        /* Allocate memory for packet linear buffer */
        pTxBuf1 = CAPWAP_ALLOCATE_CRU_BUF (DATA_TRANSFER_MAX_BUF_LEN, 0);
        if (pTxBuf1 == NULL)
        {
            APHDLR_TRC (APHDLR_FAILURE_TRC, "Memory Allocation Failed\n");
            return OSIX_FAILURE;
        }

        pu1TxBuf1 = APHDLR_BUF_IF_LINEAR (pTxBuf1,
                                          0, DATA_TRANSFER_MAX_BUF_LEN);
        if (pu1TxBuf1 == NULL)
        {
            pu1TxBuf1 =
                (UINT1 *) MemAllocMemBlk (APHDLR_DATA_TRANSFER_PKTBUF_POOLID);
            if (pu1TxBuf1 == NULL)
            {
                APHDLR_TRC (APHDLR_FAILURE_TRC, "Memory Allocation Failed\n");
                CAPWAP_RELEASE_CRU_BUF (pTxBuf1);
                return OSIX_FAILURE;
            }
            u1IsNotLinear_Tx = OSIX_TRUE;
        }

        if ((CapwapAssembleDataTransferReq (DataReq_data.u2CapwapMsgElemenLen,
                                            &DataReq_data,
                                            pu1TxBuf1)) != OSIX_SUCCESS)
        {
            APHDLR_TRC (APHDLR_FAILURE_TRC, "Failed to assemble the Data "
                        "Transfer Response Message \r\n");
            CAPWAP_RELEASE_CRU_BUF (pTxBuf1);
            if (u1IsNotLinear_Tx == OSIX_TRUE)
            {
                MemReleaseMemBlock (APHDLR_DATA_TRANSFER_PKTBUF_POOLID,
                                    pu1TxBuf1);
            }
            return OSIX_FAILURE;
        }

        if (u1IsNotLinear_Tx == OSIX_TRUE)
        {
            /* If the CRU buffer memory is not linear */
            APHDLR_COPY_TO_BUF (pTxBuf1, pu1TxBuf1, 0,
                                DataReq_data.u2CapwapMsgElemenLen + 
                                CAPWAP_CMN_HDR_LEN);
            MemReleaseMemBlock (APHDLR_DATA_TRANSFER_PKTBUF_POOLID, pu1TxBuf1);
        }

        /* Transmit Data Transfer Response Message */
        i4Status = CapwapTxMessage (pTxBuf1, pSessEntry,
                                    (UINT4) (DataReq_data.u2CapwapMsgElemenLen +
                                   CAPWAP_CMN_HDR_LEN), WSS_CAPWAP_802_11_CTRL_PKT);
        if (i4Status == OSIX_SUCCESS)
        {
            /* Update the last processed sequence number */
            pSessEntry->lastProcesseddSeqNum = DataResp.u1SeqNum;
            pSessEntry->lastTransmitedSeqNum = DataReq_data.u1SeqNum;

            /* Before storing the packet check any packet is already stored 
             * in the buffer. If yes then release the buffer and stop 
             * the timer */
            if (pSessEntry->lastTransmittedPkt != NULL)
            {
                if (ApHdlrTmrStop (pSessEntry, APHDLR_RETRANSMIT_INTERVAL_TMR)
                        == OSIX_FAILURE)
                {
                    APHDLR_TRC (APHDLR_FAILURE_TRC, 
                            "Failed to stop the retransmit timer \r\n");
                }
                CAPWAP_RELEASE_CRU_BUF (pSessEntry->lastTransmittedPkt);
                pSessEntry->lastTransmittedPkt = NULL;
                pSessEntry->lastTransmittedPktLen = 0;
                pSessEntry->u1RetransmitCount = 0;
            }

            /* store the packet buffer pointer in
             * the remote session DB copy the pointer */
            pSessEntry->lastTransmittedPkt = pTxBuf1;
            pSessEntry->lastTransmittedPktLen = (UINT4)
                (DataReq_data.u2CapwapMsgElemenLen + 13);
            /*Start the Retransmit timer */
            ApHdlrTmrStart (pSessEntry,
                            APHDLR_RETRANSMIT_INTERVAL_TMR,
                            MAX_RETRANSMIT_INTERVAL_TIMEOUT);
            /* CAPWAP_RELEASE_CRU_BUF (pTxBuf1); */
        }
        else
        {
            APHDLR_TRC (APHDLR_FAILURE_TRC, "Failed to "
                        "transmit the packet \r\n");
            CAPWAP_RELEASE_CRU_BUF (pTxBuf1);
            pSessEntry->lastTransmittedPkt = NULL;
            pSessEntry->lastTransmittedPktLen = 0;
            return OSIX_FAILURE;
        }
    }

    APHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

INT4
CapwapGetDataTransferMode (tDataTransferMode * datatransfermode, UINT4 *pMsgLen)
{
    UNUSED_PARAM (datatransfermode);
    UNUSED_PARAM (pMsgLen);
    return OSIX_SUCCESS;
}

INT4
CapwapGetDataTransferData (tDataTransferData * datatransferdata, UINT4 *pMsgLen)
{
    UNUSED_PARAM (datatransferdata);
    UNUSED_PARAM (pMsgLen);
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapGetCrashDataSize                            */
/*  Description     : This function gets the Crash Data Size            */
/*  Input(s)        :                                                   */
/*  Output(s)       :                                                   */
/*  Returns         :                                                   */
/************************************************************************/
INT4
CapwapGetCrashDataSize (tRemoteSessionManager * pSess, CHR1 * pCrashFile)
{
    FILE               *fp = NULL;
    APHDLR_FN_ENTRY ();

    fp = fopen ((const char *) pCrashFile, "rb");
    if (NULL == fp)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC, "Failed to open the file \n");
        return OSIX_FAILURE;
    }

    if (fseek (fp, 0, SEEK_END) > 0)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC, "Failed to move the file pointer to "
                    "END \n");
        fclose (fp);
        fp = NULL;
        return OSIX_FAILURE;
    }

    pSess->i4CrashFileSize = ftell (fp);

    if (fseek (fp, 0, SEEK_SET) > 0)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC, "Failed to move the file pointer to "
                    "START \n");
        fclose (fp);
        fp = NULL;
        return OSIX_FAILURE;
    }
    pSess->readCrashFd = fp;
    pSess->u1CrashReadFileOpened = OSIX_TRUE;

    APHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapGetCrashFileName                            */
/*  Description     : This function gets the Crash Data Name            */
/*  Input(s)        :                                                   */
/*  Output(s)       :                                                   */
/*  Returns         :                                                   */
/************************************************************************/
INT4
CapwapGetCrashFileName (CHR1 * pCrashFile)
{
    FILE               *fp = NULL;
    CHR1                ac1Command[256];
    CHR1               *pi1Index = NULL;
    CHR1                ac1ReadBuffer[80];

    MEMSET (ac1Command, 0, sizeof (ac1Command));
    MEMSET (ac1ReadBuffer, 0, sizeof (ac1ReadBuffer));

    /* Get the Latest Core File */
    SPRINTF (ac1Command, "ls -t %s/%s* | head -n 1",
             CRASHDUMP, CORE_FILE_PREFIX);
    fp = popen (ac1Command, "r");
    if (fp != NULL)
    {
        fgets (ac1ReadBuffer, 80, fp);
        fclose (fp);
        pi1Index = strstr (ac1ReadBuffer, "ls:");
        if (pi1Index != NULL)
        {
            APHDLR_TRC (APHDLR_FAILURE_TRC, "Core File Not Found or "
                        "Not Accessible\r\n");
            return OSIX_FAILURE;
        }
    }
    else
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC, "Command to get the latest core file "
                    "failed\r\n");
        return OSIX_FAILURE;
    }

    /* Remove trailing spaces in the File Name */
    ac1ReadBuffer[STRLEN (ac1ReadBuffer) - 1] = '\0';
    STRCPY (pCrashFile, ac1ReadBuffer);

    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapGetCrashMemoryDataInfo                      */
/*  Description     : The function updates the Details of the Crash or  */
/*                    Memory Data                                       */
/*  Input(s)        :                                                   */
/*                                                                      */
/*  Output(s)       :                                                   */
/*  Returns         :                                                   */
/************************************************************************/
INT4
CapwapGetCrashMemoryDataInfo (tRemoteSessionManager * pSess, UINT1 DataMode)
{
    CHR1                ac1CrashFileName[80];
    MEMSET (ac1CrashFileName, 0, sizeof (ac1CrashFileName));

    /* Read the crash or memory dump file */
    if (DataMode == DATA_TRANS_MODE_WTP_CRASH)
    {
        if (CapwapGetCrashFileName (ac1CrashFileName) != OSIX_SUCCESS)
        {
            APHDLR_TRC (APHDLR_FAILURE_TRC, "CapwapGetCrashMemoryData: "
                        "Failed to get the Crash File Name\r\n");
            return OSIX_FAILURE;
        }
        /* Get the Crash Data Size */
        if (CapwapGetCrashDataSize (pSess, ac1CrashFileName) == OSIX_FAILURE)
        {
            APHDLR_TRC (APHDLR_FAILURE_TRC, "CapwapGetCrashMemoryData: "
                        "Failed to get the Crash File\r\n");
            return OSIX_FAILURE;
        }
    }
    else if (DataMode == DATA_TRANS_MODE_WTP_MEMORY)
    {
        /* Not Implemented */
        APHDLR_TRC (APHDLR_FAILURE_TRC, "CapwapGetCrashMemoryData: Memory "
                    "Data not implemented\r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapGetWtpDataTransferResResultCode             */
/*  Description     : The function constructs the result code and       */
/*                    updates the pSessionEntry read file descriptor    */
/*                    and other associated variables for Crash or       */
/*                    Memory Data Transfer to WLC                       */
/*  Input(s)        :                                                   */
/*  Output(s)       :                                                   */
/*  Returns         :                                                   */
/************************************************************************/
INT4
CapwapGetWtpDataTransferResResultCode (tResCode * pResCode,
                                       UINT4 *pMsgLen,
                                       tRemoteSessionManager * pSess,
                                       UINT1 DataMode)
{
    pResCode->u2MsgEleType = RESULT_CODE;
    pResCode->u2MsgEleLen = CAPWAP_MSG_ELEM_TYPE_LEN;

    /* Set the Crash/Image Data File Details */
    if (CapwapGetCrashMemoryDataInfo (pSess, DataMode) != OSIX_SUCCESS)
    {
        pResCode->u4Value = DATATRANSFER_NO_INFO_TO_TRANSFER;
    }
    else
    {
        pResCode->u4Value = CAPWAP_SUCCESS;
    }

    *pMsgLen = (UINT4)
        (*pMsgLen + pResCode->u2MsgEleLen + CAPWAP_MSG_ELEM_TYPE_LEN);
    pResCode->isOptional = OSIX_TRUE;

    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapGetWtpDataTransferRspMsgElements            */
/*  Description     : The function gets the DTRs Msg Elements           */
/*  Input(s)        :                                                   */
/*  Output(s)       :                                                   */
/*  Returns         :                                                   */
/************************************************************************/
INT4
CapwapGetWtpDataTransferRspMsgElements (tDataRsp * pDataTransferRsp,
                                        UINT4 *u4MsgLen,
                                        tRemoteSessionManager * pSess,
                                        UINT1 DataMode)
{
    APHDLR_FN_ENTRY ();
    /* Set the CapwapCpHeader */
    if (CapwapConstructCpHeader (&pDataTransferRsp->capwapHdr) == OSIX_FAILURE)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC, "CapwapGetWtpDataTransferRspMsgElements"
                    " :Failed to construct the capwap Header\n");
        return OSIX_FAILURE;
    }

    /* Set the Result Code */
    if (CapwapGetWtpDataTransferResResultCode (&pDataTransferRsp->resultCode,
                                               u4MsgLen, pSess,
                                               DataMode) == OSIX_FAILURE)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC, "CapwapGetWtpDataTransferRspMsgElements"
                    ": Failed to construct the Result Code\n");
        return OSIX_FAILURE;
    }

    APHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapWtpReadCrashData                            */
/*  Description     : This function reads the crash data in to input    */
/*                    buffer                                            */
/*  Input(s)        : */
/*  Output(s)       : */
/*  Returns         : */
/************************************************************************/
INT4
CapwapWtpReadCrashData (UINT1 *pBuf, tRemoteSessionManager * pSess)
{

    INT4                i4FileSize = 0;
    INT4                i4ReadSize = 0;
    INT4                i4SizeRemaining = 0;

    APHDLR_FN_ENTRY ();

    if (pSess == NULL)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC, "pSess is NULL\n");
        return 0;
    }
    if (pBuf == NULL)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC, "pBuf is NULL\n");
        return 0;
    }

    i4FileSize = pSess->i4CrashFileSize;
    if (i4FileSize == 0)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC, "CapwapWtpReadCrashData : The Crash "
                    "File Size is Zero\n");
        return 0;
    }
    i4SizeRemaining = (INT4) (i4FileSize - (INT4) pSess->u4CrashFileReadOffset);

    if (i4SizeRemaining > DATA_TRANSFER_MAX_DATA_SIZE)
    {
        if (pSess->readCrashFd == NULL)
        {
            APHDLR_TRC (APHDLR_FAILURE_TRC, "ReadCrashFd is NULL\n");
            return 0;
        }
        i4ReadSize = (INT4) (fread (pBuf, 1, DATA_TRANSFER_MAX_DATA_SIZE,
                                    pSess->readCrashFd));
        if (i4ReadSize != DATA_TRANSFER_MAX_DATA_SIZE)
        {
            APHDLR_TRC (APHDLR_FAILURE_TRC, "CapwapWtpReadCrashData : "
                        "Error in reading the crash file\n");
            return 0;
        }
        pSess->u4CrashFileReadOffset =
            pSess->u4CrashFileReadOffset + DATA_TRANSFER_MAX_DATA_SIZE;
    }
    else
    {
        if (pSess->readCrashFd == NULL)
        {
            APHDLR_TRC (APHDLR_FAILURE_TRC, "ReadCrashFd is NULL\n");
            return 0;
        }
        i4ReadSize =
            (INT4) (fread
                    (pBuf, 1, (size_t) i4SizeRemaining, pSess->readCrashFd));
        if (i4ReadSize != i4SizeRemaining)
        {
            APHDLR_TRC (APHDLR_FAILURE_TRC, "CapwapWtpReadCrashData : "
                        "Error in reading the crash file\n");
            return 0;
        }
        pSess->u1CrashEndofFileReached = OSIX_TRUE;
        pSess->u4CrashFileReadOffset = 0;
    }
    APHDLR_FN_EXIT ();
    return i4ReadSize;
}

/************************************************************************/
/*  Function Name   : CapwapWtpGetCrashData                             */
/*  Description     : This function reads the crash data in to the DTR  */
/*                    Data Structure                                    */
/*  Input(s)        :                                                   */
/*  Output(s)       :                                                   */
/*  Returns         :                                                   */
/************************************************************************/
INT4
CapwapWtpGetCrashData (tDataTransferData * pDataTransferData,
                       UINT4 *u4MsgLen, tRemoteSessionManager * pSess)
{
    INT4                i4ReadSize = 0;
    UINT1               au1FileReadBuf[DATA_TRANSFER_MAX_DATA_SIZE];

    APHDLR_FN_ENTRY ();

    MEMSET (au1FileReadBuf, 0, DATA_TRANSFER_MAX_DATA_SIZE);

    i4ReadSize = CapwapWtpReadCrashData (au1FileReadBuf, pSess);
    if (i4ReadSize > 0)
    {
        /* Set the Crash Data */
        MEMCPY (pDataTransferData->Data, au1FileReadBuf, i4ReadSize);

        /* Set the Crash Data Length */
        pDataTransferData->DataLength = (UINT2) i4ReadSize;
        *u4MsgLen = *u4MsgLen + (UINT4) i4ReadSize;
    }
    else
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC, "Read from Crash Data Failed, "
                    "Return Failure\r\n");
        return OSIX_FAILURE;
    }

    if (pSess->u1CrashEndofFileReached == OSIX_TRUE)
    {
        /* Set the Data Type */
        pDataTransferData->DataType = DATA_TRANS_EOF;
        fclose (pSess->readCrashFd);
        pSess->readCrashFd = NULL;
    }
    else
    {
        /* Set the Data Type */
        pDataTransferData->DataType = DATA_TRANS_DATA;
    }

    APHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapWtpGetDataTransferData                      */
/*  Description     : This function reads the crash data in to the DTR  */
/*                    Data Structure. Also the message length and       */
/*                    ispresent value is set(useful while assembling)   */
/*  Input(s)        :                                                   */
/*  Output(s)       :                                                   */
/*  Returns         :                                                   */
/************************************************************************/
INT4
CapwapWtpGetDataTransferData (tDataTransferData * pDataTransferData,
                              tRemoteSessionManager * pSess, UINT1 DataMode)
{
    UINT4               u4MsgLen = 0;
    APHDLR_FN_ENTRY ();

    pDataTransferData->u2MsgEleType = DATA_TRANSFER_DATA;
    u4MsgLen = DATA_TRANSFER_DATA_MIN_LEN - 1;

    /* Set the Data Mode */
    pDataTransferData->DataMode = DataMode;

    /* Set the DataType, DataLength and the Data */
    if (CapwapWtpGetCrashData (pDataTransferData, &u4MsgLen, pSess) !=
        OSIX_SUCCESS)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC, "CapwapWtpReadCrashData : Error in "
                    "reading the crash file\n");
        return OSIX_FAILURE;
    }

    /* Set the Transfer Data Length */
    pDataTransferData->u2MsgEleLen = (UINT2) u4MsgLen;
    pDataTransferData->isPresent = OSIX_TRUE;

    APHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapGetWtpDataTransferReqMsgElements            */
/*  Description     : This function updates the DTReq Msg Elements      */
/*  Input(s)        :                                                   */
/*  Output(s)       :                                                   */
/*  Returns         :                                                   */
/************************************************************************/
INT4
CapwapGetWtpDataTransferReqMsgElements (tDataReq * pDataTransferReq,
                                        tRemoteSessionManager * pSess,
                                        UINT1 DataMode)
{
    APHDLR_FN_ENTRY ();

    if (DataMode != DATA_TRANS_MODE_WTP_CRASH)
    {
        /* Only Crash Data Transfer is Supported now. Handle Memory 
         * Crash Data in future */
        APHDLR_TRC (APHDLR_FAILURE_TRC, "CapwapGetWtpDataTransferReqMsgElements"
                    ": Only Crash Data Transfer is Supported\n");
        return OSIX_FAILURE;
    }

    /* Set the CapwapCpHeader */
    if (CapwapConstructCpHeader (&pDataTransferReq->capwapHdr) == OSIX_FAILURE)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC, "CapwapGetWtpDataTransferRspMsgElements"
                    ": Failed to construct the capwap Header\n");
        return OSIX_FAILURE;
    }
    /* Set the datatransferdata */
    if (CapwapWtpGetDataTransferData (&pDataTransferReq->datatransferdata,
                                      pSess, DataMode) == OSIX_FAILURE)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC, "CapwapGetWtpDataTransferRspMsgElements"
                    ": Failed to construct the Result Code\n");
        return OSIX_FAILURE;
    }
    /* Vendor Specific Data is an Optional Message Elemenet - 
     * As of now we do not send any Vendor Specific Data */

    APHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapClearDataTransferSessioEntries              */
/*  Description     : This function clears the read side parameters     */
/*  Input(s)        :                                                   */
/*  Output(s)       :                                                   */
/*  Returns         :                                                   */
/************************************************************************/
VOID
CapwapClearDataTransferSessioEntries (tRemoteSessionManager * pSessEntry)
{
    APHDLR_FN_ENTRY ();

    if (pSessEntry == NULL)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC, "Session in Disconnected State \r\n");
        return;
    }

    /*Reset the Data Transfer Info fore the WTP(read) side */
    if (pSessEntry->u1CrashReadFileOpened == OSIX_TRUE &&
        pSessEntry->readCrashFd != NULL)
    {
        fclose (pSessEntry->readCrashFd);
        pSessEntry->readCrashFd = NULL;
    }
    pSessEntry->i4CrashFileSize = 0;
    pSessEntry->u1CrashEndofFileReached = OSIX_FALSE;
    pSessEntry->u1CrashReadFileOpened = OSIX_FALSE;
    pSessEntry->u4CrashFileReadOffset = 0;
    pSessEntry->u1CrashFileNameSet = OSIX_FALSE;

    APHDLR_FN_EXIT ();
}
#endif
