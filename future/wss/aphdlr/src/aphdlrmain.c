/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: aphdlrmain.c,v 1.5 2018/02/08 10:07:05 siva Exp $i
 *
 * Description: This file contains the APHDLR Init and Main Routines.
 *
 *****************************************************************************/
#ifndef __APHDLRMAIN_C__
#define __APHDLRMAIN_C__
#include "aphdlrinc.h"
#include "wsscfgtmr.h"
#include "wsscfgprot.h"
#ifdef NPAPI_WANTED
#include "radionp.h"
#endif

static tApHdlrTaskGlobals gApHdlrTaskGlobals;

/* CPU relinquish counter */
static UINT4        gu4CPUAphdlrRelinquishCounter = 0;

extern eState       gCapwapState;

/*****************************************************************************/
/* Function Name      : ApHdlrEnqueCtrlPkts                                 */
/*                                                                           */
/* Description        : This function is to enque received packets           */
/*                      to the AP HDLR task                                 */
/*                                                                           */
/* Input(s)           : pQMsg - Received queue message                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
ApHdlrEnqueCtrlPkts (unApHdlrMsgStruct * pMsg)
{
    INT1                i4Status = OSIX_SUCCESS;
    tApHdlrQueueReq    *pApHdlrQueueReq = NULL;
    tWssWlanDB          WssWlanDB;

    APHDLR_FN_ENTRY ();

    pApHdlrQueueReq = (tApHdlrQueueReq *) MemAllocMemBlk (APHDLR_QUEUE_POOLID);
    if (pApHdlrQueueReq == NULL)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC,
                    "Memory Allocation Failed in Enque\r\n");
        return OSIX_FAILURE;
    }
    pApHdlrQueueReq->u4MsgType = pMsg->ApHdlrQueueReq.u4MsgType;
    pApHdlrQueueReq->pRcvBuf = pMsg->ApHdlrQueueReq.pRcvBuf;

    switch (pApHdlrQueueReq->u4MsgType)
    {
        case APHDLR_CTRL_MSG:
        case APHDLR_NEIGHBOUR_AP_CTRL_MSG:
        case APHDLR_CLIENT_SCAN_CTRL_MSG:
        {
            if (OsixQueSend (gApHdlrTaskGlobals.ctrlRxMsgQId,
                             (UINT1 *) &pApHdlrQueueReq,
                             OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
            {
                APHDLR_TRC (APHDLR_FAILURE_TRC,
                            "Failed to send the message to CTRL Rx queue \r\n");
                MemReleaseMemBlock (APHDLR_QUEUE_POOLID,
                                    (UINT1 *) pApHdlrQueueReq);
                return OSIX_FAILURE;
            }
            /*post event to Service Task */
            if (OsixEvtSend (gApHdlrTaskGlobals.apHdlrTaskId,
                             APHDLR_CTRL_RX_MSGQ_EVENT) == OSIX_FAILURE)
            {
                APHDLR_TRC (APHDLR_FAILURE_TRC,
                            "ApHdlrEnqueCtrlPkts: Failed to send Event \r\n");
                i4Status = OSIX_FAILURE;
            }
            break;
        }
        case APHDLR_DATA_RX_MSG:
        {
            pApHdlrQueueReq->u1MacType = pMsg->ApHdlrQueueReq.u1MacType;
            if (pApHdlrQueueReq->u1MacType == LOCAL_MAC)
            {
                MEMCPY (pApHdlrQueueReq->BssIdMac,
                        pMsg->ApHdlrQueueReq.BssIdMac, sizeof (tMacAddr));
            }
            else if (pApHdlrQueueReq->u1MacType == LOCAL_ROUTING_TYPE)
            {
                MEMSET (&WssWlanDB, 0, sizeof (tWssWlanDB));

                WssWlanDB.WssWlanIsPresentDB.bBssId = OSIX_TRUE;
                WssWlanDB.WssWlanAttributeDB.u4WlanIfIndex =
                    pMsg->ApHdlrQueueReq.u4IfIndex;
                if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY,
                                              &WssWlanDB) != OSIX_SUCCESS)
                {
                    APHDLR_TRC (APHDLR_FAILURE_TRC,
                                "WSS_WLAN_GET_IFINDEX_ENTRY Failed\r\n");
                    MemReleaseMemBlock (APHDLR_QUEUE_POOLID,
                                        (UINT1 *) pApHdlrQueueReq);
                    return (CFA_FAILURE);
                }

                MEMCPY (pApHdlrQueueReq->BssIdMac,
                        WssWlanDB.WssWlanAttributeDB.BssId, MAC_ADDR_LEN);
                pApHdlrQueueReq->u1MacType = LOCAL_MAC;
            }

            if (OsixQueSend (gApHdlrTaskGlobals.dataRxMsgQId,
                             (UINT1 *) &pApHdlrQueueReq,
                             OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
            {
                APHDLR_TRC (APHDLR_FAILURE_TRC,
                            "Failed to send the message to DATA Rx queue \r\n");
                MemReleaseMemBlock (APHDLR_QUEUE_POOLID,
                                    (UINT1 *) pApHdlrQueueReq);
                return OSIX_FAILURE;
            }
            /*post event to Service Task */
            if (OsixEvtSend (gApHdlrTaskGlobals.apHdlrTaskId,
                             APHDLR_DATA_RX_MSGQ_EVENT) == OSIX_FAILURE)
            {
                APHDLR_TRC (APHDLR_FAILURE_TRC,
                            "ApHdlrEnqueCtrlPkts: Failed to send Event \r\n");
                i4Status = OSIX_FAILURE;
            }
            break;
        }
#ifdef WPS_WTP_WANTED
        case APHDLR_WPS_MSG:
#endif
        case APHDLR_CFA_RX_MSG:
        {
            if (OsixQueSend (gApHdlrTaskGlobals.dataRxMsgQId,
                             (UINT1 *) &pApHdlrQueueReq,
                             OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
            {
                APHDLR_TRC (APHDLR_FAILURE_TRC, "Failed to send the "
                            "message to DATA Rx queue from CFA\r\n");
                MemReleaseMemBlock (APHDLR_QUEUE_POOLID,
                                    (UINT1 *) pApHdlrQueueReq);
                return OSIX_FAILURE;
            }
            /*post event to Service Task */
            if (OsixEvtSend (gApHdlrTaskGlobals.apHdlrTaskId,
                             APHDLR_DATA_RX_MSGQ_EVENT) == OSIX_FAILURE)
            {
                APHDLR_TRC (APHDLR_FAILURE_TRC,
                            "ApHdlrEnqueCtrlPkts: Failed to send Event \r\n");
                i4Status = OSIX_FAILURE;
            }
            break;
        }

        case APHDLR_DATA_TX_MSG:
        {
            if (OsixQueSend (gApHdlrTaskGlobals.dataTxMsgQId,
                             (UINT1 *) &pApHdlrQueueReq,
                             OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
            {
                APHDLR_TRC (APHDLR_FAILURE_TRC,
                            "Failed to send the message to DATA Tx queue \r\n");
                MemReleaseMemBlock (APHDLR_QUEUE_POOLID,
                                    (UINT1 *) pApHdlrQueueReq);
                return OSIX_FAILURE;
            }
            /*post event to Service Task */
            if (OsixEvtSend (gApHdlrTaskGlobals.apHdlrTaskId,
                             APHDLR_DATA_TX_MSGQ_EVENT) == OSIX_FAILURE)
            {
                APHDLR_TRC (APHDLR_FAILURE_TRC,
                            "ApHdlrEnqueCtrlPkts: Failed to send Event \r\n");
                i4Status = OSIX_FAILURE;
            }
            break;
        }
        default:
        {
            APHDLR_TRC (APHDLR_FAILURE_TRC, "ApHdlrEnqueCtrlPkts: "
                        "unknown msg queue \r\n");
            CRU_BUF_Release_MsgBufChain (pApHdlrQueueReq->pRcvBuf, FALSE);
            MemReleaseMemBlock (APHDLR_QUEUE_POOLID, (UINT1 *) pApHdlrQueueReq);
            break;
        }

    }
    APHDLR_FN_EXIT ();
    return i4Status;
}

/*****************************************************************************
 *                                                                           *
 * Function     : APHandlerMainTaskInit                                      *
 *                                                                           *
 * Description  : AP Handler task initialization routine.                    *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS, if initialization succeeds                   *
 *                OSIX_FAILURE, otherwise                                    *
 *                                                                           *
 *****************************************************************************/
UINT4
ApHandlerMainTaskInit (VOID)
{
    UINT4               u4Event = 0;
    unWssMsgStructs     WssMsgStructs;
    tRadioIfMsgStruct   RadioIfMsgStruct;
    tWssWlanMsgStruct  *pWssWlanMsgStruct = NULL;
    tCfaMsgStruct       CfaMsgStruct;

    APHDLR_FN_ENTRY ();
    pWssWlanMsgStruct =
        (tWssWlanMsgStruct *) (VOID *) UtlShMemAllocWssWlanBuf ();
    if (pWssWlanMsgStruct == NULL)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC,
                    "ApHandlerMainTaskInit:- "
                    "UtlShMemAllocWssWlanBuf returned failure\n");
        return OSIX_FAILURE;
    }

    MEMSET (&WssMsgStructs, 0, sizeof (unWssMsgStructs));
    MEMSET (&RadioIfMsgStruct, 0, sizeof (tRadioIfMsgStruct));
    MEMSET (pWssWlanMsgStruct, 0, sizeof (tWssWlanMsgStruct));
    MEMSET (&CfaMsgStruct, 0, sizeof (tCfaMsgStruct));

    /* Create buffer pools for data structures */
    if (ApHdlrMemInit () == OSIX_FAILURE)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC, "Memory Pool Creation Failed\n");
        lrInitComplete (OSIX_FAILURE);
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        return OSIX_FAILURE;
    }

    if (OsixTskIdSelf (&gApHdlrTaskGlobals.apHdlrTaskId) == OSIX_FAILURE)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC,
                    " !!!!! AP HANDLER TASK INIT FAILURE  !!!!! \r\n");
        lrInitComplete (OSIX_FAILURE);
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        return OSIX_FAILURE;
    }
    /* Create 'CTRL RX Q' queue */
    if (OsixQueCrt (APHDLR_CTRL_Q_NAME, OSIX_MAX_Q_MSG_LEN,
                    APHDLR_QUE_DEPTH, &(gApHdlrTaskGlobals.ctrlRxMsgQId))
        != OSIX_SUCCESS)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC,
                    "ApHandlerMainTaskInit:Ctrl Rx Queue Creation failed \r\n");
        lrInitComplete (OSIX_FAILURE);
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        return OSIX_FAILURE;
    }

    /* Create Data Rx queue */
    if (OsixQueCrt (APHDLR_DATARX_Q_NAME, OSIX_MAX_Q_MSG_LEN,
                    MAX_APHDLR_PKT_QUE_SIZE, &(gApHdlrTaskGlobals.dataRxMsgQId))
        != OSIX_SUCCESS)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC,
                    "ApHandlerMainTaskInit:Data Rx Queue Creation failed \r\n");
        lrInitComplete (OSIX_FAILURE);
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        return OSIX_FAILURE;
    }

    /* Create Data Tx Queue */
    if (OsixQueCrt (APHDLR_DATATX_Q_NAME, OSIX_MAX_Q_MSG_LEN,
                    APHDLR_QUE_DEPTH, &(gApHdlrTaskGlobals.dataTxMsgQId))
        != OSIX_SUCCESS)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC,
                    "ApHandlerMainTaskInit:Data Tx Queue Creation failed \r\n");
        lrInitComplete (OSIX_FAILURE);
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        return OSIX_FAILURE;
    }

    MEMCPY (gApHdlrTaskGlobals.au1TaskSemName, APHDLR_MUT_EXCL_SEM_NAME,
            OSIX_NAME_LEN);
    if (OsixCreateSem
        (APHDLR_MUT_EXCL_SEM_NAME, APHDLR_SEM_CREATE_INIT_CNT, 0,
         &gApHdlrTaskGlobals.apHdlrTaskSemId) == OSIX_FAILURE)
    {
        APHDLR_TRC1 (APHDLR_FAILURE_TRC,
                     "Seamphore Creation failure for %s \n",
                     APHDLR_MUT_EXCL_SEM_NAME);
        return OSIX_FAILURE;
    }

    /* Create the Timer List */
    if (ApHdlrTmrInit () == OSIX_FAILURE)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC,
                    "Ap Handler Timer creatiion Failed \r\n");
        lrInitComplete (OSIX_FAILURE);
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        return OSIX_FAILURE;
    }

    /* initialize counter to zero */
    gu4CPUAphdlrRelinquishCounter = 0;

#if WSSIF_WANTED
    MEMSET (&WssMsgStructs, 0, sizeof (unWssMsgStructs));
    if (WssStaWtpProcessWssIfMsg (WSS_WTP_STA_INIT, &WssMsgStructs)
        != OSIX_SUCCESS)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC,
                    "ApHandlerMainTaskInit: Station init failed \r\n");
        lrInitComplete (OSIX_FAILURE);
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        return OSIX_FAILURE;
    }
#endif

    /* Initialize the WSS modules */
#ifdef WSSCFG_WANTED
    WsscfgMainTask ();
#endif

#ifdef NPAPI_WANTED
    FsRadioHwInit ();
#endif

    WssIfInit ();
    if (WssIfProcessRadioIfMsg (WSS_RADIOIF_INIT_MSG, &RadioIfMsgStruct)
        != OSIX_SUCCESS)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC,
                    "WLAN module initialization failed \r\n");
        lrInitComplete (OSIX_FAILURE);
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        return OSIX_FAILURE;
    }

    if (WssIfProcessWssWlanMsg (WSS_WLAN_INIT_MSG, pWssWlanMsgStruct)
        != OSIX_SUCCESS)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC,
                    "WLAN module initialization failed \r\n");
        lrInitComplete (OSIX_FAILURE);
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        return OSIX_FAILURE;
    }
    if (WssIfProcessCfaMsg (CFA_WSS_CREATE_RADIO_INTF_MSG, &CfaMsgStruct)
        != OSIX_SUCCESS)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC,
                    "WLAN module initialization failed \r\n");
        lrInitComplete (OSIX_FAILURE);
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        return OSIX_FAILURE;
    }

    if (ApHdlrTmrStart (NULL, APHDLR_IDLE_INTERVAL_TMR,
                        APHDLR_IDLE_INTERVAL_TMR_VAL) != OSIX_SUCCESS)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC, "APHDLR_IDLE_INTERVAL_TMR "
                    "Timer event send failure\n");
    }
    lrInitComplete (OSIX_SUCCESS);

    if (OsixEvtSend (gApHdlrTaskGlobals.apHdlrTaskId,
                     APHDLR_INIT_EVENT) != OSIX_SUCCESS)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC,
                    "APHDLR APHDLR_RELINQUISH_EVENT Timer event send failure\n");
    }
    UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);

    while (1)
    {
        if (OsixReceiveEvent (APHDLR_CTRL_RX_MSGQ_EVENT |
                              APHDLR_DATA_RX_MSGQ_EVENT |
                              APHDLR_TMR_EXP_EVENT | APHDLR_RELINQUISH_EVENT |
                              APHDLR_INIT_EVENT, OSIX_WAIT, (UINT4) 0,
                              (UINT4 *) &(u4Event)) == OSIX_SUCCESS)
        {
            APHDLR_LOCK;
            if ((u4Event & APHDLR_CTRL_RX_MSGQ_EVENT) ==
                APHDLR_CTRL_RX_MSGQ_EVENT)
            {
                ApHdlrProcessCtrlRxMsg ();
                ApHdlrCheckRelinquishCounters ();
            }
            if ((u4Event & APHDLR_DATA_RX_MSGQ_EVENT) ==
                APHDLR_DATA_RX_MSGQ_EVENT)
            {
                ApHdlrProcessDataRxMsg ();
                ApHdlrCheckRelinquishCounters ();
            }
            if ((u4Event & APHDLR_DATA_TX_MSGQ_EVENT) ==
                APHDLR_DATA_TX_MSGQ_EVENT)
            {
                ApHdlrProcessDataTxMsg ();
                ApHdlrCheckRelinquishCounters ();
            }
            if ((u4Event & APHDLR_TMR_EXP_EVENT) == APHDLR_TMR_EXP_EVENT)
            {
                ApHdlrTmrExpHandler ();
                ApHdlrCheckRelinquishCounters ();
            }
            if ((u4Event & APHDLR_INIT_EVENT) == APHDLR_INIT_EVENT)
            {
                if (ApHdlrTmrStart (NULL, APHDLR_CPU_RELINQUISH_TMR,
                                    CAPWAP_RELINQUISH_TIMER_VAL) !=
                    OSIX_SUCCESS)
                {
                    APHDLR_TRC (APHDLR_FAILURE_TRC, "%s::%d "
                                "APHDLR_RELINQUISH_EVENT Timer Start"
                                "failure\n");
                }
            }
            APHDLR_UNLOCK;
        }

    }
    APHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : ApHdlrMemInit                                              *
 *                                                                           *
 * Description  : Memory creation and initialization required for            *
 *                AP HDLR  module                                            *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
UINT4
ApHdlrMemInit (VOID)
{
    APHDLR_FN_ENTRY ();
    if (AphdlrSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    APHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/****************************************************************************/
/*                                                                           */
/* Function     : ApHdlrMainTaskLock                                         */
/*                                                                           */
/* Description  : Lock the AphdlrMain Task                                  */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SNMP_SUCCESS or SNMP_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
ApHdlrMainTaskLock (VOID)
{
    /*** For debug *****/
    APHDLR_TRC1 (APHDLR_DEBUG_TRC,
                 "Aphdlr task lock. Aphdlr Task Sem Id is %d \n",
                 gApHdlrTaskGlobals.apHdlrTaskSemId);
    /*** For debug *****/

    if (OsixSemTake (gApHdlrTaskGlobals.apHdlrTaskSemId) == OSIX_FAILURE)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC,
                    "TakeSem failure for ApHdlrMainTask \n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************/
/*                                                                           */
/* Function     : ApHdlrMainTaskUnLock                                       */
/*                                                                           */
/* Description  : Lock the AphdlrMain Task                                  */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SNMP_SUCCESS or SNMP_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
ApHdlrMainTaskUnLock (VOID)
{
    /*** For debug *****/
    APHDLR_TRC1 (APHDLR_DEBUG_TRC,
                 "Aphdlr task UnLock. Aphdlr Task Sem Id is %d \n",
                 gApHdlrTaskGlobals.apHdlrTaskSemId);

    OsixSemGive (gApHdlrTaskGlobals.apHdlrTaskSemId);

    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : ApHdlrMemClear                                            *
 *                                                                           *
 * Description  : Deletes all the memory pools in Capwap                     *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 *                                                                           *
 *****************************************************************************/
VOID
ApHdlrMemClear (VOID)
{
    APHDLR_FN_ENTRY ();
    AphdlrSizingMemDeleteMemPools ();
    APHDLR_FN_EXIT ();
    return;
}

/*****************************************************************************
 *                                                                           *
 * Function     : ApHdlrProcessCtrlRxMsg                                    *
 *                                                                           *
 * Description  : This function process the received control message         *
 *                The expected control message are,                          *
 *                1. Configuration Update Request,                           *
 *                2. WTP Event Response,                                     *
 *                3. Image Data Request,                                     *
 *                4. Image Data Response,                                    *
 *                5. Reset Request                                           *
 *                6. Data Transfer Request                                   *
 *                7. Data Transfer Response                                  *
 *                8. Clear Configuration Request                             *
 *                9. Station Configuration Request                           *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
ApHdlrProcessCtrlRxMsg ()
{
    tApHdlrQueueReq    *pApHdlrRxQMsg = NULL;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    UINT4               u4DestIpAddr = 0, u4DestPort = 0;
    UINT4               u4MsgType = 0;
    unCapwapMsgStruct   CapwapMsgStruct;
    unApHdlrMsgStruct   ApHdlrMsg;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tCapwapControlPacket ParseCtrlMsg;
    tRemoteSessionManager *pSessEntry = NULL;

    APHDLR_FN_ENTRY ();
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (&ParseCtrlMsg, 0, sizeof (tCapwapControlPacket));
    MEMSET (&CapwapMsgStruct, 0, sizeof (unCapwapMsgStruct));
    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&ApHdlrMsg, 0, sizeof (unApHdlrMsgStruct));

    while (OsixQueRecv (gApHdlrTaskGlobals.ctrlRxMsgQId,
                        (UINT1 *) (&pApHdlrRxQMsg),
                        OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        if (pApHdlrRxQMsg == NULL)
        {
            APHDLR_TRC (APHDLR_FAILURE_TRC, "Received pointer is NULL\r\n");
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
        if (pApHdlrRxQMsg->u4MsgType == APHDLR_CTRL_MSG)
        {
            pBuf = pApHdlrRxQMsg->pRcvBuf;
            u4DestIpAddr = CRU_BUF_Get_U4Reserved2 (pBuf);
            u4DestPort = CRU_BUF_Get_U4Reserved1 (pBuf);
            CapwapMsgStruct.CapwapParseReq.pData = pBuf;
            CapwapMsgStruct.CapwapParseReq.pCapwapCtrlMsg = &ParseCtrlMsg;
            if (WssIfProcessCapwapMsg (WSS_CAPWAP_PARSE_REQ, &CapwapMsgStruct)
                == OSIX_FAILURE)
            {
                APHDLR_TRC (APHDLR_FAILURE_TRC,
                            "Failed to parse the CAPWAP control packet\r\n");
                APHDLR_RELEASE_CRU_BUF (pBuf);
                MemReleaseMemBlock (APHDLR_QUEUE_POOLID,
                                    (UINT1 *) pApHdlrRxQMsg);
                continue;
            }
            if (ParseCtrlMsg.u4ReturnCode == OSIX_FAILURE)
            {
                APHDLR_TRC1 (APHDLR_FAILURE_TRC,
                             "Failed to parse the received CAPWAP control packet, "
                             "Return Code = %d \r\n",
                             ParseCtrlMsg.u4ReturnCode);
                APHDLR_RELEASE_CRU_BUF (pBuf);
                MemReleaseMemBlock (APHDLR_QUEUE_POOLID,
                                    (UINT1 *) pApHdlrRxQMsg);
                WssIfProcessCapwapMsg (WSS_CAPWAP_PARSE_MEMREL_REQ,
                                       &CapwapMsgStruct);
                continue;
            }
            pWssIfCapwapDB->u4DestIp = u4DestIpAddr;
            pWssIfCapwapDB->u4DestPort = u4DestPort;
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_RSM, pWssIfCapwapDB)
                == OSIX_FAILURE)
            {
                APHDLR_TRC (APHDLR_FAILURE_TRC,
                            "Failed to Retrive CAPWAP DB \r\n");
                APHDLR_RELEASE_CRU_BUF (pBuf);
                MemReleaseMemBlock (APHDLR_QUEUE_POOLID,
                                    (UINT1 *) pApHdlrRxQMsg);
                WssIfProcessCapwapMsg (WSS_CAPWAP_PARSE_MEMREL_REQ,
                                       &CapwapMsgStruct);
                continue;
            }
            pSessEntry = pWssIfCapwapDB->pSessEntry;
            if (pSessEntry == NULL)
            {
                APHDLR_TRC (APHDLR_FAILURE_TRC,
                            "Failed to Get entry in Remote Session Module \r\n");
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                MemReleaseMemBlock (APHDLR_QUEUE_POOLID,
                                    (UINT1 *) pApHdlrRxQMsg);
                WssIfProcessCapwapMsg (WSS_CAPWAP_PARSE_MEMREL_REQ,
                                       &CapwapMsgStruct);
                continue;
            }
            u4MsgType = ParseCtrlMsg.capwapCtrlHdr.u4MsgType;
            ParseCtrlMsg.u2IntProfileId = pSessEntry->u2CapwapRSMIndex;

            if ((u4MsgType >= WLAN_CONF_REQ) && (u4MsgType <= WLAN_CONF_RSP))
            {
                u4MsgType -= CAPWAP_IEEE_MSG_TYPE_BASE;
            }
            if (gCapwapStateMachine[pSessEntry->eCurrentState][u4MsgType]
                (pBuf, &ParseCtrlMsg, pSessEntry) == OSIX_FAILURE)
            {
                APHDLR_RELEASE_CRU_BUF (pBuf);
                MemReleaseMemBlock (APHDLR_QUEUE_POOLID,
                                    (UINT1 *) pApHdlrRxQMsg);
                WssIfProcessCapwapMsg (WSS_CAPWAP_PARSE_MEMREL_REQ,
                                       &CapwapMsgStruct);
                APHDLR_TRC2 (APHDLR_FAILURE_TRC,
                             "APHDLR Failed to Process the Received Packet\
                        eCurrentState %d u4MsgType %d\r\n", pSessEntry->eCurrentState, u4MsgType);
                continue;
            }
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            pBuf = NULL;
            MemReleaseMemBlock (APHDLR_QUEUE_POOLID, (UINT1 *) pApHdlrRxQMsg);
            WssIfProcessCapwapMsg (WSS_CAPWAP_PARSE_MEMREL_REQ,
                                   &CapwapMsgStruct);
        }
        else if ((pApHdlrRxQMsg->u4MsgType == APHDLR_NEIGHBOUR_AP_CTRL_MSG) ||
                 (pApHdlrRxQMsg->u4MsgType == APHDLR_CLIENT_SCAN_CTRL_MSG))
        {

            ApHdlrMsg.PmWtpEventReq.vendSpec.u2MsgEleType =
                VENDOR_SPECIFIC_PAYLOAD;

            if (pApHdlrRxQMsg->u4MsgType == APHDLR_NEIGHBOUR_AP_CTRL_MSG)
            {
                ApHdlrMsg.PmWtpEventReq.vendSpec.unVendorSpec.VendNeighAP.
                    isOptional = OSIX_TRUE;
                ApHdlrMsg.PmWtpEventReq.vendSpec.elementId =
                    VENDOR_NEIGHBOR_AP_TYPE;
                ApHdlrMsg.PmWtpEventReq.vendSpec.u1RFVendorMsgEleType =
                    VENDOR_NEIGHBOR_AP_TYPE;

                if (ApHdlrProcessWtpEventReq
                    (WSS_APHDLR_PM_WTP_EVENT_REQ, &ApHdlrMsg) == OSIX_FAILURE)
                {
                    APHDLR_TRC (APHDLR_FAILURE_TRC, "Failed to process "
                                "WTP Event Request \r\n");
                }

            }
            else if (pApHdlrRxQMsg->u4MsgType == APHDLR_CLIENT_SCAN_CTRL_MSG)
            {
                ApHdlrMsg.PmWtpEventReq.vendSpec.unVendorSpec.VendClientScan.
                    isOptional = OSIX_TRUE;

                ApHdlrMsg.PmWtpEventReq.vendSpec.elementId =
                    VENDOR_CLIENT_SCAN_TYPE;
                ApHdlrMsg.PmWtpEventReq.vendSpec.u1RFVendorMsgEleType =
                    VENDOR_CLIENT_SCAN_TYPE;
            }
            if (pApHdlrRxQMsg->u4MsgType == APHDLR_NEIGHBOUR_AP_CTRL_MSG)
            {
                ApHdlrMsg.PmWtpEventReq.vendSpec.u1RFVendorMsgEleType =
                    VENDOR_NEIGHBOR_RADIO2_AP_TYPE;
            }

            if (ApHdlrProcessWtpEventReq
                (WSS_APHDLR_PM_WTP_EVENT_REQ, &ApHdlrMsg) == OSIX_FAILURE)
            {
                APHDLR_TRC (APHDLR_DEBUG_TRC, "Failed to process "
                            "WTP Event Request \r\n");
                MemReleaseMemBlock (APHDLR_QUEUE_POOLID,
                                    (UINT1 *) pApHdlrRxQMsg);
                continue;
            }

            MemReleaseMemBlock (APHDLR_QUEUE_POOLID, (UINT1 *) pApHdlrRxQMsg);
        }
        else
        {
            APHDLR_TRC (APHDLR_FAILURE_TRC, "Received unknown queue type\r\n");
            APHDLR_RELEASE_CRU_BUF (pBuf);
            MemReleaseMemBlock (APHDLR_QUEUE_POOLID, (UINT1 *) pApHdlrRxQMsg);
            continue;
        }
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    APHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : ApHdlrProcessDataRxMsg                                     *
 *                                                                           *
 * Description  : This function process the received data messages           *
 *                The expected data messages are,                            *
 *                1. CAPWAP Data Msg,                                        *
 *                2. 802.11 Msg from CFA received from STA,                  *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
ApHdlrProcessDataRxMsg (VOID)
{
    tApHdlrQueueReq    *pApHdlrQMsg = NULL;
    tWssMacMsgStruct   *pWssMacMsgStruct = NULL;
#ifdef WPS_WTP_WANTED
    INT4                i4Status = OSIX_SUCCESS;
    unApHdlrMsgStruct  *pApHdlrMsgStruct;
#endif

    APHDLR_FN_ENTRY ();
    pWssMacMsgStruct =
        (tWssMacMsgStruct *) (VOID *) UtlShMemAllocMacMsgStructBuf ();
    if (pWssMacMsgStruct == NULL)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC,
                    "ApHdlrProcessDataRxMsg:-"
                    "UtlShMemAllocMacMsgStructBuf returned failure\n");
        return OSIX_FAILURE;
    }

    MEMSET (pWssMacMsgStruct, 0, sizeof (tWssMacMsgStruct));

    while (OsixQueRecv (gApHdlrTaskGlobals.dataRxMsgQId,
                        (UINT1 *) (&pApHdlrQMsg),
                        OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        if (pApHdlrQMsg != NULL)
        {

            if (pApHdlrQMsg->u4MsgType == APHDLR_DATA_RX_MSG)
            {
#ifdef WSSMAC_WANTED
                pWssMacMsgStruct->unMacMsg.MacDot11PktBuf.pDot11MacPdu =
                    pApHdlrQMsg->pRcvBuf;
                pWssMacMsgStruct->unMacMsg.MacDot11PktBuf.u1MacType =
                    pApHdlrQMsg->u1MacType;

                if (pApHdlrQMsg->u1MacType == LOCAL_MAC)
                {
                    MEMCPY (&
                            (pWssMacMsgStruct->unMacMsg.MacDot11PktBuf.
                             BssIdMac), &(pApHdlrQMsg->BssIdMac),
                            sizeof (tMacAddr));
                }

                if (WssIfProcessWssMacMsg (WSS_MAC_CAPWAP_MSG,
                                           pWssMacMsgStruct) == OSIX_FAILURE)
                {
                    APHDLR_TRC (APHDLR_FAILURE_TRC,
                                "Failed to parse received CAPWAP Data Pkt \r\n");
                    /*GddWrite frees the CRU Buffer */
                    /*CRU_BUF_Release_MsgBufChain (pApHdlrQMsg->pRcvBuf,
                     * FALSE);*/
                    MemReleaseMemBlock (APHDLR_QUEUE_POOLID,
                                        (UINT1 *) pApHdlrQMsg);
                    continue;
                }
#endif

                MemReleaseMemBlock (APHDLR_QUEUE_POOLID, (UINT1 *) pApHdlrQMsg);
            }
            else if (pApHdlrQMsg->u4MsgType == APHDLR_CFA_RX_MSG)
            {
#ifdef WSSMAC_WANTED
                pWssMacMsgStruct->unMacMsg.MacDot11PktBuf.pDot11MacPdu =
                    pApHdlrQMsg->pRcvBuf;
#ifdef BAND_SELECT_WANTED
                pWssMacMsgStruct->unMacMsg.MacDot11PktBuf.u4IfIndex =
                    pApHdlrQMsg->pRcvBuf->ModuleData.InterfaceId.u4IfIndex;
#endif

                if (WssIfProcessWssMacMsg (WSS_MAC_CFA_MSG, pWssMacMsgStruct)
                    == OSIX_FAILURE)
                {
                    APHDLR_TRC (APHDLR_FAILURE_TRC,
                                "Failed to parse the received CFA Pkt Buf \r\n");
                    CRU_BUF_Release_MsgBufChain (pApHdlrQMsg->pRcvBuf, FALSE);
                    MemReleaseMemBlock (APHDLR_QUEUE_POOLID,
                                        (UINT1 *) pApHdlrQMsg);
                    continue;
                }
#endif

                MemReleaseMemBlock (APHDLR_QUEUE_POOLID, (UINT1 *) pApHdlrQMsg);
            }
#ifdef WPS_WTP_WANTED
            else if (pApHdlrQMsg->u4MsgType == APHDLR_WPS_MSG)
            {
                pApHdlrMsgStruct = (unApHdlrMsgStruct *) pApHdlrQMsg->pRcvBuf;

                i4Status = ApHdlrTmrStart (NULL, APHDLR_WPS_PBC_TMR,
                                           CAPWAP_WPS_PBC_TIMER_VAL);
                if (i4Status == OSIX_FAILURE)
                {
                    APHDLR_TRC (APHDLR_FAILURE_TRC,
                                "Failed to start WPS PBC timer \r\n");
                }
                if (ApHdlrProcessWtpEventReq
                    (WSS_APHDLR_WPS_WTP_EVENT_REQ,
                     (unApHdlrMsgStruct *) pApHdlrQMsg->pRcvBuf) ==
                    OSIX_FAILURE)
                {
                    APHDLR_TRC (APHDLR_FAILURE_TRC, "Failed to process "
                                "WTP Event Request \r\n");
                    MemReleaseMemBlock (APHDLR_QUEUE_POOLID,
                                        (UINT1 *) pApHdlrQMsg);
                    continue;
                }

                MemReleaseMemBlock (APHDLR_QUEUE_POOLID, (UINT1 *) pApHdlrQMsg);
            }
#endif
            else
            {
                APHDLR_TRC (APHDLR_FAILURE_TRC,
                            "Received unknown queue type\r\n");
                if (pApHdlrQMsg->pRcvBuf)
                {
                    CRU_BUF_Release_MsgBufChain (pApHdlrQMsg->pRcvBuf, FALSE);
                }
                MemReleaseMemBlock (APHDLR_QUEUE_POOLID, (UINT1 *) pApHdlrQMsg);
                continue;
            }
        }
        else
        {
            APHDLR_TRC (APHDLR_FAILURE_TRC, "pApHdlrQMsg is NULL \r\n");
        }
    }
    UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssMacMsgStruct);
    APHDLR_FN_ENTRY ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : ApHdlrProcessDataTxMsg                                     *
 *                                                                           *
 * Description  : This function is unused                                    *
 *                                                                           *
 * Input        : NONE                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
ApHdlrProcessDataTxMsg (VOID)
{
    APHDLR_FN_ENTRY ();
    APHDLR_FN_EXIT ();

    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : ApHdlrCheckRelinquishCounters                              *
 *                                                                           *
 * Description  : Check relinquish counters and take action                  *
 *                                                                           *
 * Input        : NONE                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
VOID
ApHdlrCheckRelinquishCounters (VOID)
{
    UINT4               u4Event = 0;
    UINT1               U1RelEventRev = OSIX_FALSE;

    APHDLR_FN_ENTRY ();

    gu4CPUAphdlrRelinquishCounter++;
    {
        if (gCapwapState != CAPWAP_IMAGEDATA)
        {
            if (gu4CPUAphdlrRelinquishCounter == CAPWAP_RELINQUISH_MSG_COUNT)
            {
                while (U1RelEventRev != OSIX_TRUE)
                {
                    if (OsixEvtRecv (gApHdlrTaskGlobals.apHdlrTaskId,
                                     APHDLR_TMR_EXP_EVENT |
                                     APHDLR_RELINQUISH_EVENT, OSIX_WAIT,
                                     &u4Event) == OSIX_SUCCESS)
                    {
                        if ((u4Event & APHDLR_TMR_EXP_EVENT) ==
                            APHDLR_TMR_EXP_EVENT)
                        {
                            ApHdlrTmrExpHandler ();
                        }

                        if ((u4Event & APHDLR_RELINQUISH_EVENT) ==
                            APHDLR_RELINQUISH_EVENT)
                        {
                            U1RelEventRev = OSIX_TRUE;
                        }
                    }
                }
            }
        }
    }
    APHDLR_FN_EXIT ();
    return;
}

/*****************************************************************************
 * Function     : ApHdlrCPURelinquishTmrExp                                  *
 *                                                                           *
 * Description  : Handles the Aphdlr Relinquish Timer expiry                 *
 *                                                                           *
 * Input        : NONE                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
VOID
ApHdlrCPURelinquishTmrExp (VOID *pArg)
{
    UNUSED_PARAM (pArg);
    APHDLR_FN_ENTRY ();

    gu4CPUAphdlrRelinquishCounter = 0;
    if (OsixEvtSend (gApHdlrTaskGlobals.apHdlrTaskId,
                     APHDLR_RELINQUISH_EVENT) != OSIX_SUCCESS)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC,
                    "APHDLR_RELINQUISH_EVENT Timer event send failure \r\n");
    }

    if (ApHdlrTmrStart (NULL, APHDLR_CPU_RELINQUISH_TMR,
                        CAPWAP_RELINQUISH_TIMER_VAL) != OSIX_SUCCESS)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC,
                    "APHDLR_RELINQUISH_EVENT Timer Start Failure \r\n");
    }
    APHDLR_FN_EXIT ();
    return;
}

#ifdef WPS_WTP_WANTED
/*****************************************************************************
 * Function     : ApHdlrWpsPbcTmrExp                                  *
 *                                                                           *
 * Description  : Handles the WPS PBC timer expiry                 *
 *                                                                           *
 * Input        : NONE                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
VOID
ApHdlrWpsPbcTmrExp (VOID *pArg)
{
    UNUSED_PARAM (pArg);
    tWssWlanMsgStruct  *pWssWlanMsgStruct = NULL;

    APHDLR_FN_ENTRY ();
    pWssWlanMsgStruct =
        (tWssWlanMsgStruct *) (VOID *) UtlShMemAllocWssWlanBuf ();
    if (pWssWlanMsgStruct == NULL)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC,
                    "ApHdlrWpsPbcTmrExp:- "
                    "UtlShMemAllocWssWlanBuf returned failure\n");
        return;
    }
    if (WssIfProcessWssWlanMsg (WSS_WLAN_UNSET_WPS_PBC, pWssWlanMsgStruct)
        != OSIX_SUCCESS)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC,
                    "WLAN module WPS button unset failed \r\n");
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
    }
    UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
    APHDLR_FN_EXIT ();
}
#endif
/****************************************************************************
 *                                                                          *
 * Function     : AphdlrFreeTransmittedPacket                              *
 * Description  : This function is to release the last transmitted CRU Buff *
 * Input        : None
 * Output       : None
 * Returns      : None
 *****************************************************************************/
INT4
AphdlrFreeTransmittedPacket (tRemoteSessionManager * pSessEntry)
{
    APHDLR_FN_ENTRY ();

    if (pSessEntry->lastTransmittedPkt != NULL)
    {
        CRU_BUF_Release_MsgBufChain (pSessEntry->lastTransmittedPkt, 0);
        pSessEntry->lastTransmittedPkt = NULL;
        pSessEntry->lastTransmittedPktLen = 0;
        pSessEntry->u1RetransmitCount = 0;
    }

    APHDLR_FN_EXIT ();

    return OSIX_SUCCESS;
}

#endif /* _APHDLRMAIN_C__ */
