/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: aphdlrport.c,v 1.3 2017/05/23 14:16:47 siva Exp $
 *
 * Description: This file contains the APHDLR Interface functions to other
 *              WSS modules.
 *
 *****************************************************************************/
#ifndef __APHDLRPORT_C__
#define __APHDLRPORT_C__

#include "aphdlrinc.h"

/************************************************************************/
/*  Function Name   : ApHdlrProcessWssIfMsg                             */
/*  Description     : This function is the ApHdlr's interface fn to     */
/*                    other WSS modules.                                */
/*                                                                      */
/*  Input(s)        : MsgType,                                          */
/*                    Pointer to ApHdlrMsgStruct                        */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS | OSIX_FAILURE                       */
/************************************************************************/
INT4
ApHdlrProcessWssIfMsg (UINT1 MsgType, unApHdlrMsgStruct * pApHdlrMsgStruct)
{
    tWssWlanMsgStruct  *pWssWlanMsgStruct = NULL;
    tWssMacMsgStruct   *pWssMacMsgStruct = NULL;
    tCfaMsgStruct       CfaMsgStruct;
    unCapwapMsgStruct   CapwapMsgStruct;

    APHDLR_FN_ENTRY ();
    pWssWlanMsgStruct =
        (tWssWlanMsgStruct *) (VOID *) UtlShMemAllocWssWlanBuf ();
    if (pWssWlanMsgStruct == NULL)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC,
                    "ApHdlrProcessWssIfMsg:- "
                    "UtlShMemAllocWssWlanBuf returned failure\n");
        return OSIX_FAILURE;
    }
    pWssMacMsgStruct =
        (tWssMacMsgStruct *) (VOID *) UtlShMemAllocMacMsgStructBuf ();
    if (pWssMacMsgStruct == NULL)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC,
                    "ApHdlrProcessWssIfMsg:-"
                    "UtlShMemAllocMacMsgStructBuf returned failure\n");
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        return OSIX_FAILURE;
    }
    MEMSET (pWssWlanMsgStruct, 0, sizeof (tWssWlanMsgStruct));
    MEMSET (pWssMacMsgStruct, 0, sizeof (tWssMacMsgStruct));
    MEMSET (&CfaMsgStruct, 0, sizeof (tCfaMsgStruct));
    MEMSET (&CapwapMsgStruct, 0, sizeof (unCapwapMsgStruct));

    switch (MsgType)
    {
        case WSS_APHDLR_CHANGE_STATE_EVENT_REQ:
            if (ApHdlrProcessRadioIfChnageStateEventReq
                (&pApHdlrMsgStruct->RadioIfCheStateEvtReq) == OSIX_FAILURE)
            {
                APHDLR_TRC (APHDLR_FAILURE_TRC, "Failed to process "
                            "Change State Event Request \r\n");
                UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssMacMsgStruct);
                return OSIX_FAILURE;
            }
            break;
        case WSS_APHDLR_MACHDLR_WTP_EVENT_REQ:
        case WSS_APHDLR_ARP_WTP_EVENT_REQ:
        case WSS_APHDLR_PM_WTP_EVENT_REQ:
        case WSS_APHDLR_STAION_WTP_EVENT_REQ:
        case WSS_APHDLR_RADIO_WTP_EVENT_REQ:
            if (ApHdlrProcessWtpEventReq (MsgType, pApHdlrMsgStruct)
                == OSIX_FAILURE)
            {
                APHDLR_TRC (APHDLR_FAILURE_TRC, "Failed to process "
                            "WTP Event Request \r\n");
                UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssMacMsgStruct);
                return OSIX_FAILURE;
            }
            break;
        case WSS_APHDLR_PRIMARY_DISC_REQ:
            if (ApHdlrProcessPrimaryDiscoveryReq (pApHdlrMsgStruct)
                == OSIX_FAILURE)
            {
                APHDLR_TRC (APHDLR_FAILURE_TRC, "Failed to process "
                            "Primary Discovery Request \r\n");
                UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssMacMsgStruct);
                return OSIX_FAILURE;
            }
            break;
#ifdef WPS_WTP_WANTED
        case WSS_APHDLR_WPS_WTP_EVENT_REQ:
#endif
        case WSS_APHDLR_CFA_QUEUE_REQ:
            if (ApHdlrEnqueCtrlPkts (pApHdlrMsgStruct) != OSIX_SUCCESS)
            {
                APHDLR_TRC (APHDLR_FAILURE_TRC, "Failed to Process the Queue "
                            "Message Received from CFA \r\n");
                UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssMacMsgStruct);
                return OSIX_FAILURE;
            }
            break;
        case WSS_APHDLR_CAPWAP_QUEUE_REQ:
        case WSS_APHDLR_RF_WTP_EVENT_REQ:
            if (ApHdlrEnqueCtrlPkts (pApHdlrMsgStruct) != OSIX_SUCCESS)
            {
                APHDLR_TRC (APHDLR_FAILURE_TRC, "Failed to Process the Queue "
                            "Message Received from CAPWAP \r\n");
                UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssMacMsgStruct);
                return OSIX_FAILURE;
            }
            break;
        case WSS_APHDLR_CFA_TX_BUF:
            CfaMsgStruct.unCfaMsg.CfaDot11PktBuf.pTxPktBuf =
                pApHdlrMsgStruct->WssMacMsgStruct.unMacMsg.
                MacDot11PktBuf.pDot11MacPdu;
            CfaMsgStruct.unCfaMsg.CfaDot11PktBuf.u4IfIndex =
                pApHdlrMsgStruct->WssMacMsgStruct.unMacMsg.
                MacDot11PktBuf.u4IfIndex;
            if (pApHdlrMsgStruct->WssMacMsgStruct.unMacMsg.
                MacDot11PktBuf.u1MacType == LOCAL_ROUTING_ENABLED)
            {
                CfaMsgStruct.unCfaMsg.CfaDot11PktBuf.u2VlanId =
                    pApHdlrMsgStruct->WssMacMsgStruct.unMacMsg.
                    MacDot11PktBuf.u2VlanId;
                if (WssIfProcessCfaMsg (CFA_WSS_TX_DOT3_PKT, &CfaMsgStruct)
                    != OSIX_SUCCESS)
                {
                    APHDLR_TRC (APHDLR_FAILURE_TRC, "Failed to Process the CFA "
                                "Tx Msg Received from WSS MAC \r\n");
                    UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
                    UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssMacMsgStruct);
                    return OSIX_FAILURE;
                }
            }
            else
            {
                if (WssIfProcessCfaMsg (CFA_WSS_TX_DOT11_PKT, &CfaMsgStruct)
                    != OSIX_SUCCESS)
                {
                    APHDLR_TRC (APHDLR_FAILURE_TRC, "Failed to Process the CFA "
                                "Tx Msg Received from WSS MAC \r\n");
                    UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
                    UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssMacMsgStruct);
                    return OSIX_FAILURE;
                }
            }
            break;
        case WSS_APHDLR_WLAN_PROBE_REQ:
#ifdef WSSWLAN_WANTED
            MEMCPY (&(pWssWlanMsgStruct->unWssWlanMsg.ProbReqMacFrame),
                    &(pApHdlrMsgStruct->WssMacMsgStruct.unMacMsg.
                      ProbReqMacFrame), sizeof (tDot11ProbReqMacFrame));
            if (WssIfProcessWssWlanMsg (WSS_WLAN_MAC_PROBE_REQUEST,
                                        pWssWlanMsgStruct) != OSIX_SUCCESS)
            {
                APHDLR_TRC (APHDLR_FAILURE_TRC, "Failed to Process the Probe "
                            "Req Msg Received from WSSMAC \r\n");
                UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssMacMsgStruct);
                return OSIX_FAILURE;
            }
#endif
            break;

        case WSS_APHDLR_MAC_PROBE_RSP:
#ifdef WSSMAC_WANTED
            MEMCPY (&(pWssMacMsgStruct->unMacMsg.ProbRspMacFrame),
                    &(pApHdlrMsgStruct->WssMacMsgStruct.unMacMsg.
                      ProbRspMacFrame), sizeof (tDot11ProbRspMacFrame));
            if (WssIfProcessWssMacMsg (WSS_DOT11_MGMT_PROBE_RSP,
                                       pWssMacMsgStruct) != OSIX_SUCCESS)
            {
                APHDLR_TRC (APHDLR_FAILURE_TRC, "Failed to Process the Probe "
                            "Rsp Msg Received from WSS WLAN \r\n");
                UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssMacMsgStruct);
                return OSIX_FAILURE;
            }
#endif
            break;

        case WSS_APHDLR_MAC_DEAUTH_MSG:
#ifdef WSSMAC_WANTED
            MEMCPY (&(pWssMacMsgStruct->unMacMsg.DeauthMacFrame),
                    &(pApHdlrMsgStruct->WssMacMsgStruct.unMacMsg.
                      DeauthMacFrame), sizeof (tDot11DeauthMacFrame));
            pWssMacMsgStruct->msgType =
                pApHdlrMsgStruct->WssMacMsgStruct.msgType;
            if (WssIfProcessWssMacMsg
                (WSS_DOT11_MGMT_DEAUTH_MSG, pWssMacMsgStruct) != OSIX_SUCCESS)
            {
                APHDLR_TRC (APHDLR_FAILURE_TRC, "Failed to Process the "
                            "Deauth Msg Received from WSS STA \r\n");
                UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssMacMsgStruct);
                return OSIX_FAILURE;
            }
#endif
            break;

        case WSS_APHDLR_CAPWAP_MAC_MSG:
            MEMSET (&CapwapMsgStruct, 0, sizeof (unCapwapMsgStruct));
            CapwapMsgStruct.CapwapTxPkt.pData =
                pApHdlrMsgStruct->ApHdlrQueueReq.pRcvBuf;
            CapwapMsgStruct.CapwapTxPkt.u1MacType =
                pApHdlrMsgStruct->ApHdlrQueueReq.u1MacType;
            MEMCPY (&(CapwapMsgStruct.CapwapTxPkt.BssIdMac),
                    &(pApHdlrMsgStruct->ApHdlrQueueReq.BssIdMac),
                    sizeof (tMacAddr));
            CapwapMsgStruct.CapwapTxPkt.u1DscpValue =
                pApHdlrMsgStruct->ApHdlrQueueReq.u1DscpValue;

            if (pApHdlrMsgStruct->ApHdlrQueueReq.u4MsgType ==
                WSS_APHDLR_CAPWAP_MAC_MSG)
            {
                /* DOT11 Mgmt Pkts */
                CapwapMsgStruct.CapwapTxPkt.u4MsgType = WSS_CAPWAP_TX_CTRL_PKT;
            }
            else if (pApHdlrMsgStruct->ApHdlrQueueReq.u4MsgType ==
                     WSS_APHDLR_MAC_DATA_MSG)
            {
                /* DOT11 or DOT3 Data Pkts */
                CapwapMsgStruct.CapwapTxPkt.u4MsgType =
                    WSS_CAPWAP_WTP_TX_DATA_PKT;
            }

            if (pApHdlrMsgStruct->ApHdlrQueueReq.pRcvBuf == NULL)
            {
                APHDLR_TRC (APHDLR_FAILURE_TRC, "APHDLR Received "
                            "NULL Pointer \r\n");
                UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssMacMsgStruct);
                return OSIX_FAILURE;
            }
            if (WssIfProcessCapwapMsg (WSS_CAPWAP_WTP_TX_DATA_PKT,
                                       &CapwapMsgStruct) != OSIX_SUCCESS)
            {
                APHDLR_TRC (APHDLR_FAILURE_TRC, "Failed to Process the CAPWAP "
                            "MAC Msg Received from WSS MAC \r\n");
                UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssMacMsgStruct);
                return OSIX_FAILURE;
            }
            CRU_BUF_Release_MsgBufChain (pApHdlrMsgStruct->ApHdlrQueueReq.
                                         pRcvBuf, FALSE);
            break;

        default:
            APHDLR_TRC (APHDLR_FAILURE_TRC, "Received the unknown event \r\n");
            /* return OSIX_FAILURE; */
    }
    UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
    UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssMacMsgStruct);
    APHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : ApHdlrProcessRadioIfChnageStateEventReq           */
/*  Description     : This function processes the Change State Event    */
/*                    request from RadioIf module.                      */
/*                                                                      */
/*  Input(s)        : Pointer to tRadioIfChangeStateEventReq            */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS | OSIX_FAILURE                       */
/************************************************************************/
INT4
ApHdlrProcessRadioIfChnageStateEventReq (tRadioIfChangeStateEventReq
                                         * pRadioChangeStateEvtReq)
{
    UINT1              *pu1TxBuf = NULL;
    UINT4               u4MsgLen = 0;
    tRemoteSessionManager *pSessEntry = NULL;
    unCapwapMsgStruct   CapwapMsgStruct;
    UINT1               au1Frame[CAPWAP_MAX_PKT_LEN] = { 0 };
    UINT1               u1IsNotLinear = OSIX_FALSE;
    UINT2               u2PacketLen = 0;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tChangeStateEvtReq  ChangeStateEvtReq;

    APHDLR_FN_ENTRY ();

    MEMSET (&CapwapMsgStruct, 0, sizeof (unCapwapMsgStruct));
    MEMSET (&ChangeStateEvtReq, 0, sizeof (tChangeStateEvtReq));

    /* CAPWAP Header not updated in AP HDLR module. CAPWAP will 
       update the capwap header and call the assembler */

    /* update Control Header */
    ChangeStateEvtReq.u2CapwapMsgElemenLen =
        (UINT2) (u4MsgLen + CAPWAP_MSG_ELEM_PLUS_SEQ_LEN);
    ChangeStateEvtReq.u1NumMsgBlocks = 0;    /* flags */

    ChangeStateEvtReq.u1SeqNum = (UINT1) (pSessEntry->lastTransmitedSeqNum + 1);

    /* update the Message elements */
    MEMCPY (&ChangeStateEvtReq.RadioIfChaStateEvtReq, pRadioChangeStateEvtReq,
            sizeof (tRadioIfChangeStateEventReq));
    pBuf = CRU_BUF_Allocate_MsgBufChain (CAPWAP_MAX_PKT_LEN, 0);
    if (pBuf == NULL)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC, "Memory Allocation Failed \r\n");
        return OSIX_FAILURE;
    }
    u2PacketLen = (UINT2) APHDLR_GET_BUF_LEN (pBuf);
    pu1TxBuf = APHDLR_BUF_IF_LINEAR (pBuf, 0, u2PacketLen);
    if (pu1TxBuf == NULL)
    {
        MEMSET (au1Frame, 0x00, u2PacketLen);
        APHDLR_COPY_FROM_BUF (pBuf, au1Frame, 0, u2PacketLen);
        pu1TxBuf = au1Frame;
        u1IsNotLinear = OSIX_TRUE;
    }
    CapwapMsgStruct.CapAssembleChangeStateReq.pData = pu1TxBuf;
    CapwapMsgStruct.CapAssembleChangeStateReq.pCapwapCtrlMsg =
        &ChangeStateEvtReq;

    if (WssIfProcessCapwapMsg (WSS_CAPWAP_ASSEMBLE_CHANGESTATE_EVENT_REQ,
                               &CapwapMsgStruct) == OSIX_FAILURE)
    {
        APHDLR_RELEASE_CRU_BUF (pBuf);
        APHDLR_TRC (APHDLR_FAILURE_TRC,
                    "Failed to Assemble change state event Req packet \r\n");
        return OSIX_FAILURE;
    }

    if (u1IsNotLinear == OSIX_TRUE)
    {
        /* If the CRU buffer memory is not linear */
        APHDLR_COPY_TO_BUF (pBuf, pu1TxBuf, 0, CAPWAP_MAX_PKT_LEN);
    }

    CapwapMsgStruct.CapwapTxPkt.pData = pBuf;
    CapwapMsgStruct.CapwapTxPkt.u4pktLen =
        (UINT4) ChangeStateEvtReq.u2CapwapMsgElemenLen + CAPWAP_CMN_HDR_LEN;
    CapwapMsgStruct.CapwapTxPkt.u4IpAddr = pSessEntry->remoteIpAddr.u4_addr[0];
    CapwapMsgStruct.CapwapTxPkt.u4DestPort = pSessEntry->u4RemoteCtrlPort;
    if (WssIfProcessCapwapMsg (WSS_CAPWAP_TX_CTRL_PKT, &CapwapMsgStruct)
        == OSIX_FAILURE)
    {
        APHDLR_RELEASE_CRU_BUF (pBuf);
        APHDLR_TRC (APHDLR_FAILURE_TRC, "Failed to Transmit the packet \r\n");
        return OSIX_FAILURE;
    }
    /* store the packet buffer pointer in
     * the remote session DB copy the pointer */
    pSessEntry->lastTransmittedPkt = pBuf;
    pSessEntry->lastTransmittedPktLen =
        (UINT4) (ChangeStateEvtReq.u2CapwapMsgElemenLen + CAPWAP_CMN_HDR_LEN);
    /*Start the Retransmit timer */
    ApHdlrTmrStart (pSessEntry,
                    CAPWAP_RETRANSMIT_INTERVAL_TMR,
                    MAX_RETRANSMIT_INTERVAL_TIMEOUT);

    /* update the seq number */
    pSessEntry->lastTransmitedSeqNum++;

    /*APHDLR_RELEASE_CRU_BUF (pBuf); */

    APHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : ApHdlrProcessWtpEventReq                          */
/*  Description     : This function processes the WTP Event Request.    */
/*                                                                      */
/*  Input(s)        : MsgType,                                          */
/*                    Pointer to ApHdlrMsgStruct                        */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS | OSIX_FAILURE                       */
/************************************************************************/
INT4
ApHdlrProcessWtpEventReq (UINT1 MsgType, unApHdlrMsgStruct * pApHdlrMsgStruct)
{
    UINT1              *pu1TxBuf = NULL;
    UINT1               u1IsNotLinear = OSIX_FALSE;
    unCapwapMsgStruct   CapwapMsgStruct;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tRemoteSessionManager *pSessEntry = NULL;
    tWtpEveReq          WtpEveReq;
    tStationWtpEventReq *pStationEvtReq = NULL;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;

    APHDLR_FN_ENTRY ();

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (&CapwapMsgStruct, 0, sizeof (unCapwapMsgStruct));
    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&WtpEveReq, 0, sizeof (tWtpEveReq));

    switch (MsgType)
    {
        case WSS_APHDLR_PM_WTP_EVENT_REQ:
            MEMCPY (&(WtpEveReq.PmWtpEventReq),
                    &(pApHdlrMsgStruct->PmWtpEventReq),
                    sizeof (pApHdlrMsgStruct->PmWtpEventReq));
            break;
        case WSS_APHDLR_STAION_WTP_EVENT_REQ:
        {
            pStationEvtReq = &(pApHdlrMsgStruct->StationWtpEventReq);
            MEMCPY (&WtpEveReq.StationWtpEventReq,
                    pStationEvtReq, sizeof (tStationWtpEventReq));
            break;
        }
#ifdef WPS_WATNED
        case WSS_APHDLR_WPS_WTP_EVENT_REQ:
            MEMCPY (&(WtpEveReq.vendSpec),
                    &(pApHdlrMsgStruct->PmWtpEventReq.vendSpec),
                    sizeof (pApHdlrMsgStruct->PmWtpEventReq.vendSpec));
            break;
#endif
        default:
            APHDLR_TRC (APHDLR_FAILURE_TRC, "Received the unknown event \r\n");
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
    }

    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_RSM, pWssIfCapwapDB) ==
        OSIX_FAILURE)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC, "Failed to Get RSM Entry for WTP \r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    pSessEntry = pWssIfCapwapDB->pSessEntry;
    if (pSessEntry->eCurrentState != CAPWAP_RUN)
    {
        APHDLR_TRC (APHDLR_DEBUG_TRC, "CAPWAP Session Entry is "
                    "Not in Run State \r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return CAPWAP_NO_AP_PRESENT;
    }

    /* Allocate memory for packet linear buffer */
    pBuf = CRU_BUF_Allocate_MsgBufChain (CAPWAP_MAX_PKT_LEN, 0);
    if (pBuf == NULL)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC, "Memory Allocation Failed \r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    pu1TxBuf = APHDLR_BUF_IF_LINEAR (pBuf, 0, CAPWAP_MAX_PKT_LEN);
    if (pu1TxBuf == NULL)
    {
        pu1TxBuf = (UINT1 *) MemAllocMemBlk (APHDLR_PKTBUF_POOLID);
        if (pu1TxBuf == NULL)
        {
            APHDLR_TRC (APHDLR_FAILURE_TRC, "Memory Alloc Failed \r\n");
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
        u1IsNotLinear = OSIX_TRUE;
    }
    CapwapMsgStruct.CapAssembleWtpEventReq.pData = pu1TxBuf;
    CapwapMsgStruct.CapAssembleWtpEventReq.pCapwapCtrlMsg = &WtpEveReq;
    CapwapMsgStruct.CapAssembleWtpEventReq.u2MsgLen =
        WtpEveReq.u2CapwapMsgElemenLen;

    if (WssIfProcessCapwapMsg (WSS_CAPWAP_ASSEMBLE_WTP_EVENT_REQ,
                               &CapwapMsgStruct) == OSIX_FAILURE)
    {
        APHDLR_RELEASE_CRU_BUF (pBuf);
        if (u1IsNotLinear == OSIX_TRUE)
        {
            MemReleaseMemBlock (APHDLR_PKTBUF_POOLID, pu1TxBuf);
        }
        APHDLR_TRC (APHDLR_DEBUG_TRC,
                    "Failed to Assemble wtp event Req packet \r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    /* CAPWAP Header not updated in AP HDLR module. CAPWAP will
       update the capwap header and call the assembler */

    /* update Control Header */
    WtpEveReq.u1NumMsgBlocks = 0;    /* flags */
    WtpEveReq.u1SeqNum = (UINT1) (pSessEntry->lastTransmitedSeqNum + 1);

    if (u1IsNotLinear == OSIX_TRUE)
    {
        /* If the CRU buffer memory is not linear */
        APHDLR_COPY_TO_BUF (pBuf, pu1TxBuf, 0, CapwapMsgStruct.
                            CapAssembleWtpEventReq.pCapwapCtrlMsg->
                            u2CapwapMsgElemenLen + CAPWAP_CMN_HDR_LEN);
        MemReleaseMemBlock (APHDLR_PKTBUF_POOLID, pu1TxBuf);
    }

    CapwapMsgStruct.CapwapTxPkt.pData = pBuf;

    /*multi radio length correction for dot11Stats & wtpRadiostats */
    CapwapMsgStruct.CapwapTxPkt.u4pktLen =
        (UINT4) (WtpEveReq.u2CapwapMsgElemenLen + CAPWAP_CMN_HDR_LEN);

    CapwapMsgStruct.CapwapTxPkt.u4IpAddr = pSessEntry->remoteIpAddr.u4_addr[0];
    CapwapMsgStruct.CapwapTxPkt.u4DestPort = pSessEntry->u4RemoteCtrlPort;
    if (WssIfProcessCapwapMsg (WSS_CAPWAP_TX_CTRL_PKT,
                               &CapwapMsgStruct) == OSIX_FAILURE)
    {
        APHDLR_RELEASE_CRU_BUF (pBuf);
        APHDLR_TRC (ALL_FAILURE_TRC, "Failed to Transmit the packet \r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    /* Before storing the packet check any packet is already stored in the
     * buffer. If yes then release the buffer and stop the timer. */
    if (pSessEntry->lastTransmittedPkt != NULL)
    {
        if (ApHdlrTmrStop (pSessEntry, APHDLR_RETRANSMIT_INTERVAL_TMR)
            == OSIX_FAILURE)
        {
            APHDLR_TRC (APHDLR_FAILURE_TRC,
                        "Failed to stop the retransmit timer \r\n");
        }
        CAPWAP_RELEASE_CRU_BUF (pSessEntry->lastTransmittedPkt);
        pSessEntry->lastTransmittedPkt = NULL;
        pSessEntry->lastTransmittedPktLen = 0;
        pSessEntry->u1RetransmitCount = 0;
    }

    /* store the packet buffer pointer in
     * the remote session DB copy the pointer */
    pSessEntry->lastTransmittedPkt = pBuf;
    /*Start the Retransmit timer */
    ApHdlrTmrStart (pSessEntry, APHDLR_RETRANSMIT_INTERVAL_TMR,
                    MAX_RETRANSMIT_INTERVAL_TIMEOUT);

    /* update the seq number */
    pSessEntry->lastTransmitedSeqNum++;

    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    APHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

#endif /* __APHDLRPORT_C__ */
