/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: aphdlrreset.c,v 1.2 2017/03/08 13:46:05 siva Exp $
 *
 * Description: This file contains the APHDLR Reset message handling routines.
 *
 *****************************************************************************/

#ifndef __APHDLR_RESET_C__
#define __APHDLR_RESET_C__

#include "aphdlrinc.h"
#include "msr.h"
#include "isslow.h"
#include <sys/reboot.h>

/************************************************************************/
/*  Function Name   : CapwapProcessResetRequest                         */
/*  Description     : The function processes the received APHDLR        */
/*                    Reset request                                     */
/*  Input(s)        : resetReqMsg - APHDLR reset request packet received*/
/*                    pSessEntry - Pointer to the session data base     */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/

INT4
CapwapProcessResetRequest (tSegment * pBuf,
                           tCapwapControlPacket * presetReqMsg,
                           tRemoteSessionManager * pSessEntry)
{
    INT4                i4Status = OSIX_SUCCESS;
    tResetRsp           resetResp;
    UINT1              *pu1TxBuf = NULL, *pRcvdBuf = NULL;
    UINT1               u1SeqNum = 0;
    UINT4               u4MsgLen = 0;
    tCRU_BUF_CHAIN_HEADER *pTxBuf = NULL;
    UINT1               u1IsNotLinear = OSIX_FALSE;
    UINT2               u2PacketLen = 0;
    UINT4               u4CapwapBaseWtpProfileId = 0;
    UINT4               u4FsCapwapWtpRebootStatisticsRebootCount = 0;
    UINT4               u4FsCapwapWtpRebootStatisticsAcInitiatedCount = 0;
    INT4                u4FsCapwapWtpRebootStatisticsLastFailureType = 0;
    INT4                i4RadioIfIndex = 1;
    UINT4               u4FsCapwapWtpRadioResetCount = 0;
    INT4                i4IssConfigSaveStatus = 0;
    INT4                u4FsCapwapWtpRadioLastFailureType = 0;

    APHDLR_FN_ENTRY ();
    if (pSessEntry == NULL)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC,
                    "APHDLR Session Entry is NULL, retrun FAILURE \r\n");
        return OSIX_FAILURE;
    }

    MEMSET (&resetResp, 0, sizeof (tResetRsp));

    u2PacketLen = (UINT2) APHDLR_GET_BUF_LEN (pBuf);

    pRcvdBuf = APHDLR_BUF_IF_LINEAR (pBuf, 0, u2PacketLen);
    if (pRcvdBuf == NULL)
    {
        pRcvdBuf = (UINT1 *) MemAllocMemBlk (APHDLR_PKTBUF_POOLID);
        APHDLR_COPY_FROM_BUF (pBuf, pRcvdBuf, 0, u2PacketLen);
        u1IsNotLinear = OSIX_TRUE;
    }

    /* Recieved sequence number */
    u1SeqNum = presetReqMsg->capwapCtrlHdr.u1SeqNum;

    /* Validate the reset message. if it is success construct the 
       Config Status Response */
    if ((CapValidateResetReqMsgElems (pRcvdBuf, presetReqMsg) == OSIX_SUCCESS))
    {
        /* As Validtion is successful set the result code to SUCCESS */
        resetResp.resultCode.u4Value = CAPWAP_SUCCESS;
        resetResp.resultCode.u2MsgEleType = RESULT_CODE;
        resetResp.resultCode.u2MsgEleLen = RESULT_CODE_LEN;
        resetResp.resultCode.isOptional = OSIX_TRUE;
        resetResp.u2CapwapMsgElemenLen =
            (UINT2) (resetResp.resultCode.u2MsgEleLen +
                     CAPWAP_MSG_ELEM_TYPE_LEN);
    }

    else
    {
        /* As Validtion is successful set the result code to SUCCESS */
        resetResp.resultCode.u4Value = CAPWAP_FAILURE;
        resetResp.resultCode.u2MsgEleType = RESULT_CODE;
        resetResp.resultCode.u2MsgEleLen = RESULT_CODE_LEN;
        resetResp.resultCode.isOptional = OSIX_TRUE;
        resetResp.u2CapwapMsgElemenLen =
            (UINT2) (resetResp.resultCode.u2MsgEleLen +
                     CAPWAP_MSG_ELEM_TYPE_LEN);
    }

    /* #ifndef NPAPI_WANTED */
    if (nmhGetFirstIndexFsCapwapWtpRebootStatisticsTable
        (&u4CapwapBaseWtpProfileId) != SNMP_SUCCESS)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC,
                    "nmhGetFirstIndex WtpRebootStatisticsTable FAILURE \r\n");
    }
    if (nmhGetFsCapwapWtpRebootStatisticsRebootCount (u4CapwapBaseWtpProfileId,
                                                      &u4FsCapwapWtpRebootStatisticsRebootCount)
        != SNMP_SUCCESS)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC,
                    "nmhGetFsCapwapWtpRebootStatisticsRebootCount FAILURE \r\n");
    }
    if (nmhGetFsCapwapWtpRebootStatisticsAcInitiatedCount
        (u4CapwapBaseWtpProfileId,
         &u4FsCapwapWtpRebootStatisticsAcInitiatedCount) != SNMP_SUCCESS)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC,
                    "nmhGet WtpRebootStatisticsAcInitiatedCount FAILURE \r\n");
    }
    if (nmhGetFsCapwapWtpRebootStatisticsLastFailureType
        (u4CapwapBaseWtpProfileId,
         &u4FsCapwapWtpRebootStatisticsLastFailureType) != SNMP_SUCCESS)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC,
                    "nmhGet WtpRebootStatisticsLastFailureType FAILURE \r\n");
    }

    /* increment the reboot count by one */
    u4FsCapwapWtpRebootStatisticsRebootCount++;
    if (nmhSetFsCapwapWtpRebootStatisticsRebootCount (u4CapwapBaseWtpProfileId,
                                                      u4FsCapwapWtpRebootStatisticsRebootCount)
        != SNMP_SUCCESS)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC,
                    "nmhSetFsCapwapWtpRebootStatisticsRebootCount FAILURE \r\n");
    }

    u4FsCapwapWtpRebootStatisticsAcInitiatedCount++;
    if (nmhSetFsCapwapWtpRebootStatisticsAcInitiatedCount
        (u4CapwapBaseWtpProfileId,
         u4FsCapwapWtpRebootStatisticsAcInitiatedCount) != SNMP_SUCCESS)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC,
                    "nmhSetFsCapwapWtpRebootStatisticsRebootCount FAILURE \r\n");
    }

    /* AC Initiated Failure */
    u4FsCapwapWtpRebootStatisticsLastFailureType = 1;
    if (nmhSetFsCapwapWtpRebootStatisticsLastFailureType
        (u4CapwapBaseWtpProfileId,
         u4FsCapwapWtpRebootStatisticsLastFailureType) != SNMP_SUCCESS)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC,
                    "nmhSet WtpRebootStatisticsLastFailureType FAILURE \r\n");
    }

    if (nmhGetFirstIndexFsCapwapWtpRadioStatisticsTable (&i4RadioIfIndex)
        != SNMP_SUCCESS)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC,
                    "nmhGetFirstIndexFsCapwapWtpRadioStatisticsTable FAILURE \r\n");
    }

    if (nmhGetFsCapwapWtpRadioResetCount (i4RadioIfIndex,
                                          &u4FsCapwapWtpRadioResetCount) !=
        SNMP_SUCCESS)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC,
                    "nmhGetFsCapwapWtpRadioResetCount, returns FAILURE \r\n");
    }
    u4FsCapwapWtpRadioResetCount++;
    if (nmhSetFsCapwapWtpRadioResetCount (i4RadioIfIndex,
                                          u4FsCapwapWtpRadioResetCount) !=
        SNMP_SUCCESS)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC,
                    "nmhSetFsCapwapWtpRadioResetCount, returns FAILURE \r\n");
    }

    if (nmhGetFsCapwapWtpRadioLastFailType (i4RadioIfIndex,
                                            &u4FsCapwapWtpRadioLastFailureType)
        != SNMP_SUCCESS)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC,
                    "nmhGetFsCapwapWtpRadioLastFailType, returns FAILURE \r\n");
    }
    u4FsCapwapWtpRadioLastFailureType = 1;
    if (nmhSetFsCapwapWtpRadioLastFailType (i4RadioIfIndex,
                                            u4FsCapwapWtpRadioLastFailureType)
        != SNMP_SUCCESS)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC,
                    "nmhSetFsCapwapWtpRadioResetCount, returns FAILURE \r\n");
    }

#if APHDLR_UNUSED_CODE
    if (nmhSetIssConfigSaveOption (ISS_CONFIG_STARTUP_SAVE) != SNMP_SUCCESS)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC,
                    "nmhSetIssConfigSaveOption, returns FAILURE \r\n");
    }
    if (nmhSetIssConfigRestoreOption (ISS_CONFIG_RESTORE) != SNMP_SUCCESS)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC,
                    "nmhSetIssConfigRestoreOption, returns FAILURE \r\n");
    }
#endif

    /* initiating config save only. the save will happen only if config-save
     * option is not "noSave". similarly the configuration will be restored only
     * if the restore-option is "startupConfig". giving the precedence to the
     * snmp/cli settings of these two mib varibales */
    if (nmhSetIssInitiateConfigSave (ISS_TRUE) != SNMP_SUCCESS)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC,
                    "nmhSetIssInitiateConfigSave, returns FAILURE \r\n");
    }
    do
    {
        nmhGetIssConfigSaveStatus (&i4IssConfigSaveStatus);
    }
    while (i4IssConfigSaveStatus == MIB_SAVE_IN_PROGRESS);
    /* #endif *//* #ifndef NPAPI_WANTED */

    if (u1IsNotLinear == OSIX_TRUE)
    {
        MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pRcvdBuf);
    }

    if (CapwapConstructCpHeader (&resetResp.capwapHdr) == OSIX_FAILURE)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC,
                    "Failed to construct the capwap Header\n");
        return OSIX_FAILURE;
    }
    /* update Control Header */
    resetResp.u1SeqNum = presetReqMsg->capwapCtrlHdr.u1SeqNum;
    resetResp.u2CapwapMsgElemenLen = (UINT2) (resetResp.u2CapwapMsgElemenLen
                                              + u4MsgLen +
                                              CAPWAP_MSG_ELEM_PLUS_SEQ_LEN);
    resetResp.u1NumMsgBlocks = 0;    /* flags always zero */

    /* Allocate memory for packet linear buffer */
    pTxBuf = CAPWAP_ALLOCATE_CRU_BUF (CAPWAP_MAX_PKT_LEN, 0);
    if (pTxBuf == NULL)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC, "Memory Allocation Failed\n");
        return OSIX_FAILURE;
    }
    pu1TxBuf = APHDLR_BUF_IF_LINEAR (pTxBuf, 0, CAPWAP_MAX_PKT_LEN);
    if (pu1TxBuf == NULL)
    {
        pu1TxBuf = (UINT1 *) MemAllocMemBlk (APHDLR_PKTBUF_POOLID);
        if (pu1TxBuf == NULL)
        {
            APHDLR_TRC (APHDLR_FAILURE_TRC,
                        "pu1TxBuf - Memory Allocation Failed\n");
            CAPWAP_RELEASE_CRU_BUF (pTxBuf);
            return OSIX_FAILURE;
        }
        CAPWAP_COPY_FROM_BUF (pTxBuf, pu1TxBuf, 0, CAPWAP_MAX_PKT_LEN);
        u1IsNotLinear = OSIX_TRUE;
    }

    if ((CapwapAssembleResetResponse (resetResp.u2CapwapMsgElemenLen,
                                      &resetResp, pu1TxBuf)) == OSIX_FAILURE)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC,
                    "Failed to assemble the Reset Response Message \r\n");
        if (u1IsNotLinear == OSIX_TRUE)
        {
            MemReleaseMemBlock (APHDLR_PKTBUF_POOLID, pu1TxBuf);
        }
        CAPWAP_RELEASE_CRU_BUF (pTxBuf);
        return OSIX_FAILURE;
    }

    if (u1IsNotLinear == OSIX_TRUE)
    {
        APHDLR_COPY_TO_BUF (pTxBuf, pu1TxBuf, 0, CAPWAP_MAX_PKT_LEN);
        MemReleaseMemBlock (APHDLR_PKTBUF_POOLID, pu1TxBuf);
    }

    i4Status = CapwapTxMessage (pTxBuf,
                                pSessEntry,
                                (UINT4) (resetResp.u2CapwapMsgElemenLen +
                                         CAPWAP_CMN_HDR_LEN),
                                WSS_CAPWAP_802_11_CTRL_PKT);
    if (i4Status == OSIX_FAILURE)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC, "Failed to Transmit the packet\n");

        if (u1IsNotLinear == OSIX_TRUE)
        {
            MemReleaseMemBlock (APHDLR_PKTBUF_POOLID, pu1TxBuf);
        }
        APHDLR_RELEASE_CRU_BUF (pTxBuf);
        return (i4Status);
    }
    else
    {
        pSessEntry->lastTransmitedSeqNum++;
        if (u1IsNotLinear == OSIX_TRUE)
        {
            MemReleaseMemBlock (APHDLR_PKTBUF_POOLID, pu1TxBuf);
        }

        APHDLR_RELEASE_CRU_BUF (pTxBuf);
        ApHdlrEnterReset ();
        return OSIX_SUCCESS;
    }

    APHDLR_FN_EXIT ();
    UNUSED_PARAM (u1SeqNum);
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapValidateResetReqMsgElems                       */
/*  Description     : The function validates the functional             */
/*                    characteristics of the received APHDLR            */
/*                    reset request agains the configured               */
/*                    WTP profile                                       */
/*  Input(s)        : resetReqMsg - parsed APHDLR reset request         */
/*                    pRcvBuf - Received reset packet                   */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
CapValidateResetReqMsgElems (UINT1 *pRcvBuf, tCapwapControlPacket * resetReqMsg)
{

    tImageId            imageId;
    tVendorSpecPayload  vendSpec;
    UNUSED_PARAM (pRcvBuf);
    UNUSED_PARAM (resetReqMsg);

    APHDLR_FN_ENTRY ();
    MEMSET (&imageId, 0, sizeof (tImageId));
    MEMSET (&vendSpec, 0, sizeof (tVendorSpecPayload));

#if APHDLR_UNUSED_CODE
    /* Validate the mandatory message elements */
    if (CapwapValidateImageId (pRcvBuf, resetReqMsg,
                               IMAGE_IDENTIFIER_RESET_REQ_INDEX) ==
        OSIX_SUCCESS)
    {
        return OSIX_SUCCESS;
    }

    /* Validate the Optional message elements */
    if (CapwapValidateVendSpecPld (pRcvBuf, resetReqMsg,
                                   &vendSpec,
                                   VENDOR_SPECIFIC_PAYLOAD_RESET_REQ_INDEX) ==
        OSIX_SUCCESS)
    {
        return OSIX_SUCCESS;
    }
#endif
    /* Profile not configured by the operator
     * ncrement the error count*/
    APHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapProcessResetResponse                        */
/*  Description     : The function is unused in WTP.                    */
/*                                                                      */
/*  Input(s)        : None                                              */
/*  Output(s)       : None                                              */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/
INT4
CapwapProcessResetResponse (tSegment * pBuf,
                            tCapwapControlPacket * presetReqMsg,
                            tRemoteSessionManager * pSessEntry)
{
    APHDLR_FN_ENTRY ();
    UNUSED_PARAM (pBuf);
    if (presetReqMsg != NULL)
    {
        UNUSED_PARAM (presetReqMsg);
    }
    UNUSED_PARAM (pSessEntry);
    APHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : ApHdlrEnterReset                                  */
/*  Description     : The function resets the system.                   */
/*  Input(s)        : None                                              */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/
INT4
ApHdlrEnterReset (VOID)
{

    APHDLR_FN_ENTRY ();
    printf (" \n\tSYSTEM GOING FOR REBOOT !!! \n");
#ifdef NPAPI_WANTED
    INT4                i4retVal = 0;

    i4retVal = system ("reboot");
    if (i4retVal < 0)
    {
        perror ("system(reboot):");
        printf ("Taking direct step of calling syscall...\n\r");
        fflush (stdout);
        reboot (LINUX_REBOOT_CMD_RESTART);
        perror ("reboot(LINUX_REBOOT_CMD_RESTART):");
    }
#else
    exit (0);
#endif
    APHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

#endif
