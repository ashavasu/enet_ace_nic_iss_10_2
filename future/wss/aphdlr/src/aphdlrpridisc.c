/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: aphdlrpridisc.c,v 1.1.1.1 2016/09/20 10:42:38 siva Exp $
 *
 * Description: This file contains the APHDLR Primary Discovery routines.
 *
 *****************************************************************************/
#ifndef __APHDLR_PRIDISC_C__
#define __APHDLR_PRIDISC_C__

#include "aphdlrinc.h"
#include "wtpnvram.h"

extern tACInfoAfterDiscovery gACInfoAfterDiscovery[MAX_DISC_RESPONSE];

/************************************************************************/
/*  Function Name   : CapwapProcessPrimaryDiscoveryResponse             */
/*  Description     : The function processes the received CAPWAP        */
/*                    discovery response. This function validates the   */
/*                    received discovery response and updates WSS-AP.   */
/*  Input(s)        : discReqMsg - CAPWAP Discovery packet received     */
/*                    u4DestPort - Received UDP port                    */
/*                    u4DestIp - Dest IP Address                        */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/

INT4
CapwapProcessPrimaryDiscoveryResponse (tSegment * pBuf,
                                       tCapwapControlPacket * pDiscRespMsg,
                                       tRemoteSessionManager * pSessEntry)
{
    UINT4               u4Offset = 0;
    UINT2               u2NumMsgElems = 0;
    UINT1              *pRcvBuf = NULL;
    INT4                i4Status = OSIX_SUCCESS;
    tPriDiscRsp         DiscResponse;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT2               u2Timeout = 0;

    CAPWAP_FN_ENTRY ();
    if (pSessEntry == NULL || pSessEntry->eCurrentState != CAPWAP_RUN)
    {
        /* The FSM may be in sulking state. In sulking state
           WTP should not process received discovery responses */
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "FSM is in wrong State, Return Failure \r\n");
        return OSIX_FAILURE;
    }
    MEMSET (&DiscResponse, 0, sizeof (tPriDiscRsp));
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE);
    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    if (CapwapTmrStop (pSessEntry, CAPWAP_PRIMARY_DISC_INTERVAL_TMR) ==
        OSIX_FAILURE)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FAILURE,
                    "Failed to stop the Primary Disc timer \r\n");
        return OSIX_FAILURE;
    }

    pRcvBuf = CRU_BUF_GetDataPtr (pBuf->pFirstValidDataDesc);

    /* Get AC Descriptor from Buffer */
    u2NumMsgElems =
        pDiscRespMsg->capwapMsgElm[AC_DESCRIPTOR_DISC_RESP_INDEX].
        numMsgElemInstance;
    u4Offset =
        pDiscRespMsg->capwapMsgElm[AC_DESCRIPTOR_DISC_RESP_INDEX].
        pu2Offset[u2NumMsgElems - 1];
    CapwapGetAcDescriptorFromBuf (pRcvBuf, u4Offset, &(DiscResponse.acDesc),
                                  u2NumMsgElems);

    /* Get AC Name from Buffer */
    u2NumMsgElems =
        pDiscRespMsg->capwapMsgElm[AC_NAME_DISC_RESP_INDEX].numMsgElemInstance;
    u4Offset =
        pDiscRespMsg->capwapMsgElm[AC_NAME_DISC_RESP_INDEX].
        pu2Offset[u2NumMsgElems - 1];
    CapwapGetAcNameFromBuf (pRcvBuf, u4Offset, &(DiscResponse.acName),
                            u2NumMsgElems);

    /* Get WTP Radio Information */
    u2NumMsgElems =
        pDiscRespMsg->capwapMsgElm[WTP_RADIO_INFO_DISC_RESP_INDEX].
        numMsgElemInstance;
    u4Offset =
        pDiscRespMsg->capwapMsgElm[WTP_RADIO_INFO_DISC_RESP_INDEX].
        pu2Offset[u2NumMsgElems - 1];
    CapwapGetRadioInfoFromBuf (pRcvBuf, u4Offset, &(DiscResponse.RadioIfInfo),
                               u2NumMsgElems);
/* Get Control IPv4 Address */
    u2NumMsgElems =
        pDiscRespMsg->capwapMsgElm[CAPWAP_CTRL_IP_ADDR_DISC_RESP_INDEX].
        numMsgElemInstance;
    u4Offset =
        pDiscRespMsg->capwapMsgElm[CAPWAP_CTRL_IP_ADDR_DISC_RESP_INDEX].
        pu2Offset[u2NumMsgElems - 1];
    CapwapGetCtrlIP4AddrFromBuf (pRcvBuf, u4Offset, &(DiscResponse.ctrlAddr),
                                 u2NumMsgElems);

    /*WssSetWlcIpAddrToWtpNvRam (DiscResponse.ctrlAddr.ipAddr.u4_addr[0]); */
    WssSetWlcIpAddrToWtpNvRam (CRU_BUF_Get_U4Reserved2 (pBuf));

    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpFallbackEnable = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpMaxDiscoveryInterval = OSIX_TRUE;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB))
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to Set the Received WtpFallback \r\n");
        return OSIX_SUCCESS;
    }
    u2Timeout = pWssIfCapwapDB->CapwapGetDB.u2WtpMaxDiscoveryInterval;

    if (pWssIfCapwapDB->CapwapGetDB.u1WtpFallbackEnable == 1)
    {
        if (CapwapShutDownDTLS (pSessEntry) == OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Disconnect the"
                        "Session \r\n");
        }
        CapwapStopAllSessionTimers (pSessEntry);

        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

        pWssIfCapwapDB->u4DestIp = pSessEntry->remoteIpAddr.u4_addr[0];
        pWssIfCapwapDB->u4DestPort = pSessEntry->u4RemoteCtrlPort;
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_DELETE_RSM,
                                     pWssIfCapwapDB) == OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Process"
                        "CAPWAP DB Message \r\n");
        }
        CAPWAP_TRC (CAPWAP_FSM_TRC, "Entered CAPWAP IDLE State \r\n");
        pSessEntry->eCurrentState = CAPWAP_IDLE;

        pSessEntry->remoteIpAddr.u4_addr[0] = WssGetWlcIpAddrFromWtpNvRam ();
        CapwapConstructDiscAndSend (pSessEntry);

        /* Enable the entry in RSM */
        pSessEntry->u1Used = OSIX_TRUE;
        pSessEntry->lastTransmitedSeqNum++;
        CapwapDiscTmrStart (CAPWAP_MAX_DISC_INTERVAL_TMR, u2Timeout);
    }
    else
    {
        CapwapDiscTmrStart (CAPWAP_PRIMARY_DISC_INTERVAL_TMR,
                            PRIMARY_DISC_INTERVAL_TIMEOUT);
    }

    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    CAPWAP_FN_EXIT ();
    return i4Status;
}

/*****************************************************************************
 * Function     : CapwapConstructPrimaryDiscReq                              *
 *                                                                           *
 * Description  : This function constructs the Primary discovery  request    *
 *                Capability Parameters                                      *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
CapwapConstructPrimaryDiscReq (tPriDiscReq * pDiscRequest)
{

    UINT4               u4MsgLen = 0;

#ifdef WTP_WANTED
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
#endif

    CAPWAP_FN_ENTRY ();

    /* update CAPWAP Header */
    if (CapwapConstructCpHeader (&pDiscRequest->capwapHdr) == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "capwapConstructDiscReq:Failed to construct capwap Header \r\n");
        return OSIX_FAILURE;
    }

    /* update Mandatory Message Elements */
    if ((CapwapGetDiscoveryType (&pDiscRequest->discType, &u4MsgLen)
         == OSIX_FAILURE) ||
        (CapwapGetWtpFrameTunnelMode (&pDiscRequest->wtpTunnelMode,
                                      &u4MsgLen)
         == OSIX_FAILURE) ||
        (CapwapGetWtpMacType (&pDiscRequest->wtpMacType, &u4MsgLen)
         == OSIX_FAILURE) ||
        (CapwapGetWtpBoardData (&pDiscRequest->wtpBoardData, &u4MsgLen)
         == OSIX_FAILURE) ||
        (CapwapGetWtpDescriptor (&pDiscRequest->wtpDescriptor, &u4MsgLen)
         == OSIX_FAILURE))
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to get Mandatory Discovery Request Message Elements \n");
        return OSIX_FAILURE;
    }

    /* update Optional Message Elements */
    if ((CapwapGetWtpDiscPadding (&pDiscRequest->mtuDiscPad, &u4MsgLen)
         == OSIX_FAILURE) ||
        (CapwapGetVendorPayload (&pDiscRequest->vendSpec, &u4MsgLen)
         == OSIX_FAILURE))
    {
        /* With out optinal message elements
         * It should continue */
        return OSIX_SUCCESS;
    }
    /* update Control Header */
    pDiscRequest->u2CapwapMsgElemenLen = u4MsgLen + CAPWAP_CMN_MSG_ELM_HEAD_LEN;
    pDiscRequest->u1NumMsgBlocks = 0;    /* flags */

    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : ApHdlrProcessPrimaryDiscoveryReq                  */
/*  Description     : This function processes the WTP Event Request.    */
/*                                                                      */
/*  Input(s)        : MsgType,                                          */
/*                    Pointer to ApHdlrMsgStruct                        */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS | OSIX_FAILURE                       */
/************************************************************************/

INT4
ApHdlrProcessPrimaryDiscoveryReq (unApHdlrMsgStruct * pApHdlrMsgStruct)
{
    UINT1              *pu1TxBuf = NULL;
    UINT4               u4MsgLen = 0;
    tRemoteSessionManager *pSessEntry = NULL;
    unCapwapMsgStruct   CapwapMsgStruct;
    UINT1               au1Frame[CAPWAP_MAX_PKT_LEN] = { 0 };
    UINT1               u1IsNotLinear = OSIX_FALSE;
    UINT2               u2PacketLen = 0;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tPriDiscReq         PriDiscReq;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT1               u1WlcIndex = 0;

    APHDLR_FN_ENTRY ();

    MEMSET (&CapwapMsgStruct, 0, sizeof (unCapwapMsgStruct));
    MEMSET (&PriDiscReq, 0, sizeof (tPriDiscReq));

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE);
    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_RSM, pWssIfCapwapDB) ==
        OSIX_FAILURE)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC, "Failed to Get RSM Entry for WTP \r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    pSessEntry = pWssIfCapwapDB->pSessEntry;
    if (pSessEntry->eCurrentState != CAPWAP_RUN)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC, "CAPWAP Session Entry is "
                    "Not in Run State \r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return CAPWAP_NO_AP_PRESENT;
    }
    /* CAPWAP Header not updated in AP HDLR module. CAPWAP will
       update the capwap header and call the assembler */

    if (pApHdlrMsgStruct->PriDiscReq.mtuDiscPad.isOptional == OSIX_TRUE)
    {
        u4MsgLen += pApHdlrMsgStruct->PriDiscReq.mtuDiscPad.u2MsgEleLen
            + CAPWAP_MSG_ELEM_TYPE_LEN;
    }
    if (pApHdlrMsgStruct->PriDiscReq.vendSpec.isOptional == OSIX_TRUE)
    {
        u4MsgLen += pApHdlrMsgStruct->PriDiscReq.vendSpec.u2MsgEleLen
            + CAPWAP_MSG_ELEM_TYPE_LEN;
    }

    /* update Control Header */
    PriDiscReq.u2CapwapMsgElemenLen =
        (UINT2) (u4MsgLen + CAPWAP_MSG_ELEM_PLUS_SEQ_LEN);
    PriDiscReq.u1NumMsgBlocks = 0;    /* flags */

    PriDiscReq.u1SeqNum = (UINT1) (pSessEntry->lastTransmitedSeqNum + 1);

    /* update the Message elements */
    MEMCPY (&PriDiscReq, &(pApHdlrMsgStruct->PriDiscReq),
            sizeof (pApHdlrMsgStruct->PriDiscReq));
    pBuf = CRU_BUF_Allocate_MsgBufChain (CAPWAP_MAX_PKT_LEN, 0);
    if (pBuf == NULL)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        APHDLR_TRC (APHDLR_FAILURE_TRC, "Memory Allocation Failed \r\n");
        return OSIX_FAILURE;
    }
    u2PacketLen = (UINT2) APHDLR_GET_BUF_LEN (pBuf);
    pu1TxBuf = APHDLR_BUF_IF_LINEAR (pBuf, 0, u2PacketLen);
    if (pu1TxBuf == NULL)
    {
        MEMSET (au1Frame, 0x00, u2PacketLen);
        APHDLR_COPY_FROM_BUF (pBuf, au1Frame, 0, u2PacketLen);
        pu1TxBuf = au1Frame;
        u1IsNotLinear = OSIX_TRUE;
    }
    CapwapMsgStruct.CapAssemblePrimaryDiscReq.pData = pu1TxBuf;
    CapwapMsgStruct.CapAssemblePrimaryDiscReq.pCapwapCtrlMsg = &PriDiscReq;

    if (WssIfProcessCapwapMsg (WSS_CAPWAP_ASSEMBLE_PRIMARY_DISCOVERY_REQ,
                               &CapwapMsgStruct) == OSIX_FAILURE)
    {
        APHDLR_RELEASE_CRU_BUF (pBuf);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        APHDLR_TRC (APHDLR_FAILURE_TRC, "Failed to Assemble Primary discovery packet \r\n");
        return OSIX_FAILURE;
    }

    if (u1IsNotLinear == OSIX_TRUE)
    {
        /* If the CRU buffer memory is not linear */
        APHDLR_COPY_TO_BUF (pBuf, pu1TxBuf, 0, CAPWAP_MAX_PKT_LEN);
    }

    CapwapMsgStruct.CapwapTxPkt.pData = pBuf;
    CapwapMsgStruct.CapwapTxPkt.u4pktLen =
        (UINT4) PriDiscReq.u2CapwapMsgElemenLen + CAPWAP_CMN_HDR_LEN;
    CapwapMsgStruct.CapwapTxPkt.u4IpAddr = pSessEntry->remoteIpAddr.u4_addr[0];

    u1WlcIndex = pApHdlrMsgStruct->PriDiscReq.u1AcRefOption;
    CapwapMsgStruct.CapwapTxPkt.u4IpAddr =
        gACInfoAfterDiscovery[u1WlcIndex].incomingAddr.u4_addr[0];
    CapwapMsgStruct.CapwapTxPkt.u4DestPort = pSessEntry->u4RemoteCtrlPort;
    if (CapwapCtrlUdpTxPacket (pBuf,
                               CapwapMsgStruct.CapwapTxPkt.u4IpAddr,
                               CapwapMsgStruct.CapwapTxPkt.u4DestPort,
                               CapwapMsgStruct.CapwapTxPkt.u4pktLen)
        == OSIX_FAILURE)
    {
        APHDLR_RELEASE_CRU_BUF (pBuf);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        APHDLR_TRC (APHDLR_FAILURE_TRC, "Failed to Transmit the packet \r\n");
        return OSIX_FAILURE;
    }
    APHDLR_RELEASE_CRU_BUF (pBuf);

    CapwapDiscTmrStart (CAPWAP_PRIMARY_DISC_INTERVAL_TMR,
                        PRIMARY_DISC_INTERVAL_TIMEOUT);

    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    APHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

INT4
CapwapProcessPrimaryDiscoveryRequest (tSegment * pBuf,
                                      tCapwapControlPacket * pDiscReqMsg,
                                      tRemoteSessionManager * pSessEntry)
{
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pDiscReqMsg);
    UNUSED_PARAM (pSessEntry);
    return OSIX_SUCCESS;
}

#endif
