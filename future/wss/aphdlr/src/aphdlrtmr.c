/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: aphdlrtmr.c,v 1.5 2017/11/24 10:37:01 siva Exp $
 *
 * Description: This file contains the APHDLR Timer Related Routines.
 *
 *****************************************************************************/
#ifndef __APHDLRTMR_C__
#define __APHDLRTMR_C__
#include "aphdlrinc.h"
#ifdef NPAPI_WANTED
#include "nputil.h"
#include "wssifstawtpprot.h"
#endif
#include "wssifwtpwlandb.h"
#include "fssocket.h"
#if defined (IP_WANTED)
#if defined (OS_PTHREADS) || defined (OS_CPSS_MAIN_OS)
#include <linux/if.h>
#include <linux/if_packet.h>
#include <linux/if_ether.h>
#endif
#endif

static tApHdlrTimerList gApHdlrTmrList;
/* timer block */
static tTmrBlk      tmrAphdlrTmrBlk;
static tTmrBlk      tmrIdleTmrBlk;
#ifdef HOSTAPD_WANTED
static tTmrBlk      tmrHostapdTmrBlk;
#endif

extern tWssStaWtpDBWalk gaWssStaWtpDBWalk[MAX_STA_SUPP_PER_AP];
extern UINT1        gu1WtpClientWalkIndex;
UINT1               gau1IfName[MAX_IFNAME_SIZE];
#ifdef BAND_SELECT_WANTED
extern UINT4        WssStaUpdateAPBandSteerProcessDB (eProcessDB action,
                                                      tWssStaBandSteerDB *
                                                      pWssStaBandSteerDB);
extern INT4         WssStaGetRadioInfo (UINT1 *pu1StaMac, UINT1 *pu1RadioId,
                                        UINT1 *u1WlanId);
#endif

#ifdef HOSTAPD_WANTED
extern UINT1        gu1HostApdReset;
#endif
/*****************************************************************************
 *                                                                           *
 * Function     : ApHdlrpTmrInit                                            *
 *                                                                           *
 * Description  :  This function creates a timer list for all the timers     *
 *                          in AP HDLR module.                               *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
ApHdlrTmrInit (VOID)
{
    APHDLR_FN_ENTRY ();
    /* Timer List for State Machine Timers */
    if (TmrCreateTimerList ((CONST UINT1 *) APHDLR_TASK_NAME,
                            APHDLR_TMR_EXP_EVENT,
                            NULL,
                            (tTimerListId *) & (gApHdlrTmrList.ApHdlrTmrListId))
        == TMR_FAILURE)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC, "ApHdlrTmrInit: "
                    "Failed to creat the Application Timer List \r\n");
        return OSIX_FAILURE;
    }

    ApHdlrTmrInitTmrDesc ();
    APHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : ApHdlrTmrInitTmrDesc                                       *
 *                                                                           *
 * Description  : This function intializes the timer desc for all            *
 *                the timers in APHDLR module.                               *
 *                                                                           *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
VOID
ApHdlrTmrInitTmrDesc (VOID)
{
    APHDLR_FN_ENTRY ();
    gApHdlrTmrList.aApHdlrTmrDesc[APHDLR_RETRANSMIT_INTERVAL_TMR].TmrExpFn
        = ApHdlrRetransmitIntervalTmrExp;
    gApHdlrTmrList.aApHdlrTmrDesc[APHDLR_RETRANSMIT_INTERVAL_TMR].i2Offset =
        (INT2) FSAP_OFFSETOF (tRemoteSessionManager, RetransIntervalTmr);

    gApHdlrTmrList.aApHdlrTmrDesc[APHDLR_STATS_INTERVAL_TMR].TmrExpFn
        = ApHdlrStatsIntervalTmrExp;
    gApHdlrTmrList.aApHdlrTmrDesc[APHDLR_STATS_INTERVAL_TMR].i2Offset =
        (INT2) FSAP_OFFSETOF (tRemoteSessionManager, StatsIntervalTmr);

    gApHdlrTmrList.aApHdlrTmrDesc[APHDLR_IDLE_INTERVAL_TMR].TmrExpFn
        = ApHdlrIdleIntervalTmrExp;
    gApHdlrTmrList.aApHdlrTmrDesc[APHDLR_IDLE_INTERVAL_TMR].i2Offset =
        /*(INT2) FSAP_OFFSETOF (tRemoteSessionManager, IdleIntervalTmr); */
        (INT2) ((INT4) &(tmrIdleTmrBlk));

    gApHdlrTmrList.aApHdlrTmrDesc[APHDLR_CPU_RELINQUISH_TMR].TmrExpFn
        = ApHdlrCPURelinquishTmrExp;
    gApHdlrTmrList.aApHdlrTmrDesc[APHDLR_CPU_RELINQUISH_TMR].i2Offset =
        (INT2) ((INT4) &(tmrAphdlrTmrBlk));

#ifdef HOSTAPD_WANTED
    gApHdlrTmrList.aApHdlrTmrDesc[APHDLR_HOSTAPD_INTERVAL_TMR].TmrExpFn
        = ApHdlrHostapdIntervalTmrExp;
    gApHdlrTmrList.aApHdlrTmrDesc[APHDLR_HOSTAPD_INTERVAL_TMR].i2Offset =
        (INT2) ((INT4) &(tmrHostapdTmrBlk));
#endif
#ifdef WPS_WTP_WANTED
    gApHdlrTmrList.aApHdlrTmrDesc[APHDLR_WPS_PBC_TMR].TmrExpFn
        = ApHdlrWpsPbcTmrExp;
    gApHdlrTmrList.aApHdlrTmrDesc[APHDLR_WPS_PBC_TMR].i2Offset =
        (INT2) ((INT4) &(tmrAphdlrTmrBlk));
#endif
#ifdef BAND_SELECT_WANTED
    gApHdlrTmrList.aApHdlrTmrDesc[APHDLR_PROBE_REQ_TMR].TmrExpFn
        = ApHdlrStaProbeExp;
    gApHdlrTmrList.aApHdlrTmrDesc[APHDLR_PROBE_REQ_TMR].i2Offset =
        (INT2) FSAP_OFFSETOF (tWssStaBandSteerDB, ProbeEntryTmr);

#endif
    APHDLR_FN_EXIT ();
}

/*****************************************************************************
 *                                                                           *
 * Function     : ApHdlrTmrExpHandler                                       *
 *                                                                           *
 * Description  :  This function is called whenever a timer expiry           *
 *                 message is received by Service task. Different timer      *
 *                 expiry handlers are called based on the timer type.       *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
VOID
ApHdlrTmrExpHandler (VOID)
{
    tTmrAppTimer       *pExpiredTimers = NULL;
    UINT1               u1TimerId = 0;
    INT2                i2Offset = 0;

    APHDLR_FN_ENTRY ();

    while ((pExpiredTimers =
            TmrGetNextExpiredTimer (gApHdlrTmrList.ApHdlrTmrListId)) != NULL)
    {

        u1TimerId = ((tTmrBlk *) pExpiredTimers)->u1TimerId;

        APHDLR_TRC1 (APHDLR_DEBUG_TRC, "Timer to be processed %d\r\n",
                     u1TimerId);

        if (u1TimerId < APHDLR_MAX_TMR)
        {
            i2Offset = gApHdlrTmrList.aApHdlrTmrDesc[u1TimerId].i2Offset;

            if (i2Offset == -1)
            {
                /* The timer function does not take any parameter. */
                (*(gApHdlrTmrList.aApHdlrTmrDesc[u1TimerId].TmrExpFn)) (NULL);
            }
            else
            {
                (*(gApHdlrTmrList.aApHdlrTmrDesc[u1TimerId].TmrExpFn))
                    ((UINT1 *) pExpiredTimers - i2Offset);
            }
        }
    }
    APHDLR_FN_EXIT ();
    return;
}

/*****************************************************************************
 *                                                                           *
 * Function     : ApHdlrTmrStart                                             *
 *                                                                           *
 * Description  : This function used to start the aphdlr state machiune      *
 *                specified timers.                                          *
 *                                                                           *
 * Input        : pSessEntry - pointer to session entry table                *
 *                u4TmrInterval - Time interval for which timer must run     *
 *               (in seconds)                                                *
 *                u1TmrType - Indicates which timer to start.                *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
ApHdlrTmrStart (tRemoteSessionManager * pSessEntry, UINT1 u1TmrType,
                UINT4 u4TmrInterval)
{
    APHDLR_FN_ENTRY ();
    switch (u1TmrType)
    {
        case APHDLR_RETRANSMIT_INTERVAL_TMR:
            if (TmrStart (gApHdlrTmrList.ApHdlrTmrListId,
                          &(pSessEntry->RetransIntervalTmr),
                          u1TmrType, u4TmrInterval, 0) == TMR_FAILURE)
            {
                APHDLR_TRC1 (APHDLR_FAILURE_TRC, "ApHdlrTmrStart: "
                             "Failed to start the Timer Type = %d \r\n",
                             u1TmrType);
                return OSIX_FAILURE;
            }
            break;
        case APHDLR_STATS_INTERVAL_TMR:
            if (TmrStart (gApHdlrTmrList.ApHdlrTmrListId,
                          &(pSessEntry->StatsIntervalTmr),
                          u1TmrType, u4TmrInterval, 0) == TMR_FAILURE)
            {
                APHDLR_TRC1 (APHDLR_FAILURE_TRC, "ApHdlrTmrStart: "
                             "Failed to start the Timer Type = %d \r\n",
                             u1TmrType);
                return OSIX_FAILURE;
            }
            break;
        case APHDLR_IDLE_INTERVAL_TMR:
            if (TmrStart (gApHdlrTmrList.ApHdlrTmrListId,
                          &(tmrIdleTmrBlk),
                          u1TmrType, u4TmrInterval, 0) == TMR_FAILURE)
            {
                APHDLR_TRC1 (APHDLR_FAILURE_TRC, "ApHdlrTmrStart: "
                             "Failed to start the Timer Type = %d \r\n",
                             u1TmrType);
                return OSIX_FAILURE;
            }
            break;
        case APHDLR_CPU_RELINQUISH_TMR:
            if (TmrStart (gApHdlrTmrList.ApHdlrTmrListId,
                          &tmrAphdlrTmrBlk,
                          u1TmrType, u4TmrInterval, 0) == TMR_FAILURE)
            {
                APHDLR_TRC1 (APHDLR_FAILURE_TRC, "ApHdlrTmrStart: "
                             "Failed to start the Timer Type = %d \r\n",
                             u1TmrType);
                return OSIX_FAILURE;
            }
            break;
#ifdef HOSTAPD_WANTED
        case APHDLR_HOSTAPD_INTERVAL_TMR:
            if (TmrStart (gApHdlrTmrList.ApHdlrTmrListId,
                          &tmrHostapdTmrBlk,
                          u1TmrType, u4TmrInterval, 0) == TMR_FAILURE)
            {
                APHDLR_TRC1 (APHDLR_FAILURE_TRC, "ApHdlrTmrStart: "
                             "Failed to start the Timer Type = %d \r\n",
                             u1TmrType);
                return OSIX_FAILURE;
            }
            break;
#endif
#ifdef WPS_WTP_WANTED
        case APHDLR_WPS_PBC_TMR:
            if (TmrStart (gApHdlrTmrList.ApHdlrTmrListId,
                          &tmrAphdlrTmrBlk,
                          u1TmrType, u4TmrInterval, 0) == TMR_FAILURE)
            {
                APHDLR_TRC1 (APHDLR_FAILURE_TRC, "ApHdlrTmrStart: "
                             "Failed to start the Timer Type = %d \r\n",
                             u1TmrType);
                return OSIX_FAILURE;
            }
            break;
#endif
        default:
            APHDLR_TRC (APHDLR_FAILURE_TRC,
                        "ApHdlrTmrStart:Invalid Timer type FAILED !!!\r\n");
            return OSIX_FAILURE;
    }
    APHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function                  : ApHdlrTmrStop                                 *
 *                                                                           *
 * Description               : This routine stops the given discovery timer  *
 *                                                                           *
 * Input                     : u1TmrType - Indicates which timer to stop     *
 *                             pSessEntry - pointer to session entry table   *
 *                                                                           *
 * Output                    : None                                          *
 *                                                                           *
 * Returns                   : None.                                         *
 *                                                                           *
 *****************************************************************************/
INT4
ApHdlrTmrStop (tRemoteSessionManager * pSessEntry, UINT1 u1TmrType)
{
    UINT4               u4RemainingTime = 0;
    UINT4               u4TmrRetVal = TMR_SUCCESS;
    INT4                i4RetVal = OSIX_FAILURE;

    APHDLR_FN_ENTRY ();

    switch (u1TmrType)
    {
        case APHDLR_RETRANSMIT_INTERVAL_TMR:
            u4TmrRetVal = TmrGetRemainingTime (gApHdlrTmrList.ApHdlrTmrListId,
                                               &(pSessEntry->RetransIntervalTmr.
                                                 TimerNode), &u4RemainingTime);
            if (u4TmrRetVal != TMR_FAILURE)
            {
                if (TmrStop (gApHdlrTmrList.ApHdlrTmrListId,
                             &(pSessEntry->RetransIntervalTmr)) != TMR_SUCCESS)
                {
                    APHDLR_TRC (APHDLR_FAILURE_TRC, "AphdlrTmrStop: "
                                "Failure to stop MaxRetransmitIntervalr\r\n");
                }
                u4RemainingTime = 0;
                i4RetVal = OSIX_SUCCESS;
            }
            break;
        case APHDLR_STATS_INTERVAL_TMR:
            u4TmrRetVal = TmrGetRemainingTime (gApHdlrTmrList.ApHdlrTmrListId,
                                               &(pSessEntry->StatsIntervalTmr.
                                                 TimerNode), &u4RemainingTime);
            if (u4TmrRetVal != TMR_FAILURE)
            {
                if (TmrStop (gApHdlrTmrList.ApHdlrTmrListId,
                             &(pSessEntry->StatsIntervalTmr)) != TMR_SUCCESS)
                {
                    APHDLR_TRC (APHDLR_FAILURE_TRC, "APhdlrTmrStop: "
                                "Failure to stop MaxRetransmitIntervalr\r\n");
                }
                u4RemainingTime = 0;
                i4RetVal = OSIX_SUCCESS;
            }
            break;
        case APHDLR_IDLE_INTERVAL_TMR:
            u4TmrRetVal = TmrGetRemainingTime (gApHdlrTmrList.ApHdlrTmrListId,
                                               &(tmrIdleTmrBlk.TimerNode),
                                               &u4RemainingTime);
            if (u4TmrRetVal != TMR_FAILURE)
            {
                if (TmrStop (gApHdlrTmrList.ApHdlrTmrListId,
                             &(tmrIdleTmrBlk)) != TMR_SUCCESS)
                {
                    APHDLR_TRC (APHDLR_FAILURE_TRC, "AphdlrTmrStop: "
                                "Failure to stop MaxRetransmitIntervalr\r\n");
                }
                u4RemainingTime = 0;
                i4RetVal = OSIX_SUCCESS;
            }
            break;
        case APHDLR_CPU_RELINQUISH_TMR:
            u4TmrRetVal = TmrGetRemainingTime (gApHdlrTmrList.ApHdlrTmrListId,
                                               &tmrAphdlrTmrBlk.TimerNode,
                                               &u4RemainingTime);
            if (u4TmrRetVal != TMR_FAILURE)
            {
                if (TmrStop (gApHdlrTmrList.ApHdlrTmrListId, &tmrAphdlrTmrBlk)
                    != TMR_SUCCESS)
                {
                    APHDLR_TRC (APHDLR_FAILURE_TRC, "AphdlrTmrStop: "
                                "Failure to stop MaxRetransmitIntervalr\r\n");
                }
                u4RemainingTime = 0;
                i4RetVal = OSIX_SUCCESS;
            }
            break;
#ifdef HOSTAPD_WANTED
        case APHDLR_HOSTAPD_INTERVAL_TMR:
            u4TmrRetVal = TmrGetRemainingTime (gApHdlrTmrList.ApHdlrTmrListId,
                                               &tmrHostapdTmrBlk.TimerNode,
                                               &u4RemainingTime);
            if (u4TmrRetVal != TMR_FAILURE)
            {
                if (TmrStop (gApHdlrTmrList.ApHdlrTmrListId, &tmrHostapdTmrBlk)
                    != TMR_SUCCESS)
                {
                    APHDLR_TRC (APHDLR_FAILURE_TRC, "AphdlrTmrStop: "
                                "Failure to stop MaxRetransmitIntervalr\r\n");
                }
                u4RemainingTime = 0;
                i4RetVal = OSIX_SUCCESS;
            }
            break;
#endif
        default:
            APHDLR_TRC (APHDLR_FAILURE_TRC,
                        "ApHdlrTmrStop:Invalid Timer type FAILED !!!\r\n");
            return OSIX_FAILURE;
    }
    APHDLR_FN_EXIT ();
    return i4RetVal;
}

/*****************************************************************************
 * Function                  : ApHdlrTmrStop                                 *
 *                                                                           *
 * Description               : This routine handles the Retransmit Timer     *
 *                             Expiry.                                       *
 *                                                                           *
 * Input                     : u1TmrType - Indicates which timer to stop     *
 *                             pSessEntry - pointer to session entry table   *
 *                                                                           *
 * Output                    : None                                          *
 *                                                                           *
 * Returns                   : None.                                         *
 *                                                                           *
 *****************************************************************************/
VOID
ApHdlrRetransmitIntervalTmrExp (VOID *pArg)
{
    tRemoteSessionManager *pSessEntry = NULL;

    APHDLR_FN_ENTRY ();
    pSessEntry = (tRemoteSessionManager *) pArg;

    UNUSED_PARAM (pSessEntry);
    APHDLR_FN_EXIT ();
}

/*****************************************************************************
 * Function                  : ApHdlrIdleIntervalTmrExp                      *
 *                                                                           *
 * Description               : This routine handles the Idle Timer Expiry.   *
 *                                                                           *
 * Input                     : None                                          *
 *                                                                           *
 * Output                    : None                                          *
 *                                                                           *
 * Returns                   : None.                                         *
 *                                                                           *
 *****************************************************************************/
VOID
ApHdlrIdleIntervalTmrExp (VOID *pArg)
{
    UINT2               u2IntId = 0;
    UINT4               u4IdleInterval = 0;
    INT4                i4Status = OSIX_SUCCESS;
    tRemoteSessionManager *pSessEntry = NULL;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    APHDLR_FN_ENTRY ();
    pSessEntry = (tRemoteSessionManager *) pArg;
    WSS_IF_DB_TEMP_MEM_ALLOC_VOID_RETURN (pWssIfCapwapDB)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    WssStaWtpProcessWssIfMsg (WSS_WTP_STA_IDLE_TIMEOUT, NULL);

    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpIdleTimeout = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2IntId;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
        == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Unable to get Idle Timeout from DB \r\n");
    }

    u4IdleInterval = pWssIfCapwapDB->CapwapGetDB.u4WtpIdleTimeout;
    i4Status = ApHdlrTmrStart (NULL, APHDLR_IDLE_INTERVAL_TMR, u4IdleInterval);
    if (i4Status == OSIX_FAILURE)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC, "capwapProcessConfigUpdateRequest: "
                    "Failed to start the Stats Timer \n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return;
    }

    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    UNUSED_PARAM (pSessEntry);
    APHDLR_FN_EXIT ();
    return;
}

#ifdef HOSTAPD_WANTED
/*****************************************************************************
 * Function                  : ApHdlrHostapdIntervalTmrExp                   *
 *                                                                           *
 * Description               : This routine handles the hostapd Timer Expiry *
 *                                                                           *
 * Input                     : None                                          *
 *                                                                           *
 * Output                    : None                                          *
 *                                                                           *
 * Returns                   : None.                                         *
 *                                                                           *
 *****************************************************************************/
VOID
ApHdlrHostapdIntervalTmrExp (VOID *pArg)
{
    APHDLR_FN_ENTRY ();

    UNUSED_PARAM (pArg);
    INT4                i4Status = OSIX_FAILURE;

    if (gu1HostApdReset == OSIX_TRUE)
    {
#ifdef NPAPI_WANTED
        if (RadioIfHostApdReset () != OSIX_SUCCESS)
        {
            APHDLR_TRC (APHDLR_FAILURE_TRC, "ApHdlrHostapdIntervalTmrExp: "
                        "RadioIfHostApdReset failed \n");
        }
#endif
    }
    gu1HostApdReset = OSIX_FALSE;
    i4Status =
        ApHdlrTmrStart (NULL, APHDLR_HOSTAPD_INTERVAL_TMR,
                        APHDLR_HOSTAPD_INTERVAL_TMR_VAL);
    if (i4Status == OSIX_FAILURE)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC, "ApHdlrHostapdIntervalTmrExp: "
                    "Failed to start the hostapd Timer \n");
    }
    APHDLR_FN_EXIT ();
    return;
}
#endif

/*****************************************************************************
 * Function                  : ApHdlrStatsGetNumOfInterfaces                 *
 *                                                                           *
 * Description               : This routine handles the number of interfaces *
 *                                                                           *
 * Input                     : None                                          *
 *                                                                           *
 * Output                    : None                                          *
 *                                                                           *
 * Returns                   : None.                                         *
 *                                                                           *
 *****************************************************************************/

UINT1
ApHdlrStatsGetNumOfInterfaces (VOID)
{
    struct ifreq       *ifr = NULL;
    struct ifconf       ifc;
    INT4                i4Sck = 0;
    char                Buf[200] = { 0 };
    INT4                i = 0;
    INT4                i4Interfaces = 0;
    UINT1               u1NumInterfaces = 0;
    struct ifreq       *item;

    i4Sck = socket (PF_INET, SOCK_DGRAM, 0);
    if (i4Sck < 0)
    {
        perror ("socket");
        return 0;
    }
    memset (&ifc, 0, sizeof (struct ifconf));

    ifc.ifc_len = sizeof (Buf);
    ifc.ifc_buf = Buf;

    if (ioctl (i4Sck, SIOCGIFCONF, &ifc) < 0)
    {
        close (i4Sck);
        perror ("ioctl(SIOCGIFCONF)");
        return 0;
    }
    ifr = ifc.ifc_req;
    i4Interfaces = ifc.ifc_len / sizeof (struct ifreq);

    for (i = 0; i < i4Interfaces; i++)
    {
        item = &ifr[i];
        /* skip the loopback interface */
        if (!memcmp (item->ifr_name, "lo", sizeof ("lo")))
        {
            continue;
        }

        /* skip the vlan interface */
        if (!memcmp (item->ifr_name, "vlan", (sizeof ("vlan") - 1)))
        {
            continue;
        }

        /* skip the sim interface */
        if (!memcmp (item->ifr_name, "sim", (sizeof ("sim") - 1)))
        {
            continue;
        }

        /* skip the sim interface */
        if (!memcmp (item->ifr_name, "virbr", (sizeof ("virbr") - 1)))
        {
            continue;
        }
        u1NumInterfaces++;
    }
    close (i4Sck);
    return u1NumInterfaces;
}

/*****************************************************************************
 * Function                  : ApHdlrGetApSentBytes                          *
 *                                                                           *
 * Description               : This routine handles the Sent bytes from an Ap*
 *                                                                           *
 * Input                     : au1IfName                                     *
 *                                                                           *
 * Output                    : None                                          *
 *                                                                           *
 * Returns                   : Number bytes sent from an AP                  *
 *                                                                           *
 *****************************************************************************/

UINT4
ApHdlrGetApSentBytes (UINT1 *au1IfName)
{
    UINT4               u4RetVal = 0;
    FILE               *fp = NULL;
    UINT1               au1Path2[25] = "/statistics/tx_bytes";
    UINT1               au1PathName[75] = "/sys/class/net/";

    MEMCPY (au1PathName + 15, au1IfName, STRLEN (au1IfName));
    MEMCPY (au1PathName + 15 + STRLEN (au1IfName), au1Path2, sizeof (au1Path2));

    if ((fp = fopen ((const char *) au1PathName, "r")) == NULL)
    {
        return 0;
    }
    fscanf (fp, "%d", &u4RetVal);
    fclose (fp);
    return u4RetVal;
}

/*****************************************************************************
 * Function                  : ApHdlrGetApRcvdBytes                          *
 *                                                                           *
 * Description               : This routine handles the Recvd bytes by an Ap *
 *                                                                           *
 * Input                     : au1IfName                                     *
 *                                                                           *
 * Output                    : None                                          *
 *                                                                           *
 * Returns                   : Number bytes Recvd by an AP                   *
 *                                                                           *
 *****************************************************************************/

UINT4
ApHdlrGetApRcvdBytes (UINT1 *au1IfName)
{
    UINT4               u4RetVal = 0;
    FILE               *fp = NULL;
    UINT1               au1Path2[25] = "/statistics/rx_bytes";
    UINT1               au1PathName[75] = "/sys/class/net/";

    MEMCPY (au1PathName + 15, au1IfName, STRLEN (au1IfName));
    MEMCPY (au1PathName + 15 + STRLEN (au1IfName), au1Path2, sizeof (au1Path2));

    if ((fp = fopen ((const char *) au1PathName, "r")) == NULL)
    {
        return 0;
    }
    fscanf (fp, "%d", &u4RetVal);
    fclose (fp);
    return u4RetVal;
}

/*****************************************************************************
 * Function                  : ApHdlrGetApGatewayIpAddr                      *
 *                                                                           *
 * Description               : This routine handles the AP gateway IP        *
 *                                                                           *
 * Input                     : None                                          *
 *                                                                           *
 * Output                    : None                                          *
 *                                                                           *
 * Returns                   : None.                                         *
 *                                                                           *
 *****************************************************************************/
UINT4
ApHdlrGetApGatewayIpAddr (VOID)
{
    tRemoteSessionManager *pSessEntry = NULL;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tNetIpv4RtInfo      NetIpRtInfo;
    tNetIpv4IfInfo      NetIpIfInfo;
    tRtInfoQueryMsg     RtQuery;
    UINT4               u4WlcAddr = 0;
    UINT4               u4IpAddr = 0;
    INT4                i4IfIndex = 0;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    MEMSET (&NetIpIfInfo, 0, sizeof (tNetIpv4IfInfo));
    MEMSET (&NetIpRtInfo, 0, sizeof (tNetIpv4RtInfo));
    MEMSET (&RtQuery, 0, sizeof (tRtInfoQueryMsg));

    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_RSM,
                                 pWssIfCapwapDB) != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "WssIfProcessCapwapDBMsg failed \r\n");
    }

    pSessEntry = pWssIfCapwapDB->pSessEntry;

    u4WlcAddr = pSessEntry->remoteIpAddr.u4_addr[0];
    RtQuery.u4DestinationIpAddress = u4WlcAddr;
    RtQuery.u4DestinationSubnetMask = 0xFFFFFFFF;
    RtQuery.u1QueryFlag = 0x01;

    if (NetIpv4GetRoute (&RtQuery, &NetIpRtInfo) == NETIPV4_SUCCESS)
    {
        i4IfIndex = (INT4) NetIpRtInfo.u4RtIfIndx;
        u4IpAddr = NetIpRtInfo.u4NextHop;
        /*When AC and AP are directly connected the AP gateway should be the AC
           IP Address */
        if (!u4IpAddr)
        {
            u4IpAddr = RtQuery.u4DestinationIpAddress;
        }
    }
    else
    {
        u4IpAddr = RtQuery.u4DestinationIpAddress;
        STRCPY (gau1IfName, "eth0");
    }

    if (NetIpv4GetIfInfo ((UINT4) i4IfIndex, &NetIpIfInfo) == NETIPV4_SUCCESS)
    {
        STRCPY (gau1IfName, NetIpIfInfo.au1IfName);
    }

    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return u4IpAddr;
}

/*****************************************************************************
 * Function                  : ApHdlrGetApNetMask                            *
 *                                                                           *
 * Description               : This routine handles the AP NETMASK           *
 *                                                                           *
 * Input                     : None                                          *
 *                                                                           *
 * Output                    : None                                          *
 *                                                                           *
 * Returns                   : None.                                         *
 *                                                                           *
 *****************************************************************************/
UINT4
ApHdlrGetApNetMask (VOID)
{
    tRemoteSessionManager *pSessEntry = NULL;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tNetIpv4RtInfo      NetIpRtInfo;
    tNetIpv4IfInfo      NetIpIfInfo;
    tRtInfoQueryMsg     RtQuery;
    UINT4               u4WlcAddr = 0;
    UINT4               u4IpAddr = 0;
    UINT4               u4NetMask = 0;
    INT4                i4IfIndex = 0;

    UNUSED_PARAM (u4IpAddr);

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    MEMSET (&NetIpIfInfo, 0, sizeof (tNetIpv4IfInfo));
    MEMSET (&NetIpRtInfo, 0, sizeof (tNetIpv4RtInfo));
    MEMSET (&RtQuery, 0, sizeof (tRtInfoQueryMsg));

    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_RSM,
                                 pWssIfCapwapDB) != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "WssIfProcessCapwapDBMsg failed \r\n");
    }
    pSessEntry = pWssIfCapwapDB->pSessEntry;
    u4WlcAddr = pSessEntry->remoteIpAddr.u4_addr[0];
    RtQuery.u4DestinationIpAddress = u4WlcAddr;
    RtQuery.u4DestinationSubnetMask = 0xFFFFFFFF;
    RtQuery.u1QueryFlag = 0x01;
    if (NetIpv4GetRoute (&RtQuery, &NetIpRtInfo) == NETIPV4_SUCCESS)
    {
        i4IfIndex = (INT4) NetIpRtInfo.u4RtIfIndx;
    }
    else
    {
        u4IpAddr = RtQuery.u4DestinationIpAddress;
    }

    if (NetIpv4GetIfInfo ((UINT4) i4IfIndex, &NetIpIfInfo) == NETIPV4_SUCCESS)
    {
        u4NetMask = NetIpIfInfo.u4NetMask;
    }

    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return u4NetMask;
}

/*****************************************************************************
 * Function                  : ApHdlrStatsIntervalTmrExp                     *
 *                                                                           *
 * Description               : This routine handles the Stats Timer Expiry.  *
 *                                                                           *
 * Input                     : None                                          *
 *                                                                           *
 * Output                    : None                                          *
 *                                                                           *
 * Returns                   : None.                                         *
 *                                                                           *
 *****************************************************************************/
VOID
ApHdlrStatsIntervalTmrExp (VOID *pArg)
{
    tRemoteSessionManager *pSessEntry = NULL;
    unApHdlrMsgStruct   ApHdlrMsg;
    tRadioIfGetDB       RadioIfGetDB;
    tDot11Statistics    ieee80211Stats;
    tWtpRadioStats      radioStats;
    INT4                i4Status = OSIX_SUCCESS;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT2               u2StatsInterval = 0;
    tWssIfPMDB          WssIfPMDB;
    MEMSET (&WssIfPMDB, 0, sizeof (tWssIfPMDB));

    APHDLR_FN_ENTRY ();
    pSessEntry = (tRemoteSessionManager *) pArg;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    MEMSET (&ieee80211Stats, 0, sizeof (ieee80211Stats));
    MEMSET (&radioStats, 0, sizeof (radioStats));

    ApHdlrMsg.PmWtpEventReq.wtpRadiostatsElement.isOptional = OSIX_TRUE;
    ApHdlrMsg.PmWtpEventReq.dot11StatsElement.isOptional = OSIX_TRUE;
    ApHdlrMsg.PmWtpEventReq.MacHdlrStatsElement.isOptional = OSIX_TRUE;
    ApHdlrMsg.PmWtpEventReq.RadioClientStatsElement.isOptional = OSIX_TRUE;
    ApHdlrMsg.PmWtpEventReq.wtpRebootstatsElement.isOptional = OSIX_TRUE;
    ApHdlrMsg.PmWtpEventReq.capwapStatsElement.isOptional = OSIX_TRUE;
    ApHdlrMsg.PmWtpEventReq.ApParamsElement.isOptional = OSIX_TRUE;
    ApHdlrMsg.PmWtpEventReq.wtpRebootstatsElement.isOptional = OSIX_TRUE;
    ApHdlrMsg.PmWtpEventReq.vendSpec.unVendorSpec.VendMultiDomain.isOptional =
        OSIX_TRUE;

    if (WssIfProcessApHdlrMsg (WSS_APHDLR_PM_WTP_EVENT_REQ, &ApHdlrMsg)
        == OSIX_FAILURE)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC,
                    "Failed to send the packet to the Ap Hdlr Module \r\n");
        return;
    }
    WSS_IF_DB_TEMP_MEM_ALLOC_VOID_RETURN (pWssIfCapwapDB)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpStatisticsTimer = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
        == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Unable to get statistics timer from DB \r\n");
    }

    u2StatsInterval = pWssIfCapwapDB->CapwapGetDB.u2WtpStatisticsTimer;
    i4Status = ApHdlrTmrStart (pSessEntry,
                               APHDLR_STATS_INTERVAL_TMR, u2StatsInterval);
    if (i4Status == OSIX_FAILURE)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC, "Failed to start the Stats Timer \n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return;
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    APHDLR_FN_EXIT ();
    return;
}

VOID
ApHdlrGetApAndRadioStats ()
{
    unApHdlrMsgStruct   ApHdlrMsg;
    tRadioIfGetDB       RadioIfGetDB;
    tWssIfApHdlrDB      WssIfApHdlrDB;
    UINT4               u4RadioInOctets = 0;
    UINT4               u4RadioOutOctets = 0;
#ifdef NPAPI_WANTED
    UINT1               u1RadioCount = 0;
    UINT4               u4HwCount = 0;
    UINT4               u4HwCount1 = 0;
    tFsHwNp             FsHwNp;
    UINT1               u2Index = 0;
#endif
    UINT4               u4CurrSentBytes = 0;
    UINT4               u4CurrRcvdBytes = 0;
    MEMSET (&WssIfApHdlrDB, 0, sizeof (tWssIfApHdlrDB));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    MEMSET (&ApHdlrMsg, 0, sizeof (unApHdlrMsgStruct));
#ifdef NPAPI_WANTED
    u1RadioCount = SYS_DEF_MAX_RADIO_INTERFACES;
    ApHdlrMsg.PmWtpEventReq.RadioClientStatsElement.
        numRadioCount = u1RadioCount;
    for (u2Index = 0; u2Index < u1RadioCount; u2Index++)
    {
        FsHwNp.u4Opcode = FS_RADIO_HW_GET_RADIO_CLIENT_STATS;
        RadioNpWrHwProgram (&FsHwNp);
        u4HwCount = FsHwNp.RadioIfNpModInfo.unOpCode.
            sConfigGetInterfaceInOctets.u4IfInOctets;

        RadioIfGetDB.RadioIfGetAllDB.u1RadioId = u1RadioCount;
        RadioIfGetDB.RadioIfIsGetAllDB.bRadioIfIndex = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bRadioCounters = OSIX_TRUE;
        if (WssIfProcessRadioIfDBMsg (WSS_GET_PHY_RADIO_IF_DB, &RadioIfGetDB)
            != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to get Radio Operationa State from RadioIf \r\n");
            return;
        }

        ApHdlrMsg.PmWtpEventReq.RadioClientStatsElement.
            radioClientStats[u2Index].u4IfInOctets = (u4HwCount -
                                                      RadioIfGetDB.
                                                      RadioIfGetAllDB.
                                                      u4RadioInOctets);
        u4RadioInOctets +=
            FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetInterfaceInOctets.
            u4IfInOctets;

        RadioNpWrHwProgram (&FsHwNp);
        u4HwCount1 = FsHwNp.RadioIfNpModInfo.unOpCode.
            sConfigGetInterfaceOutOctets.u4IfOutOctets;

        ApHdlrMsg.PmWtpEventReq.RadioClientStatsElement.
            radioClientStats[u2Index].u4IfOutOctets = (u4HwCount1 -
                                                       RadioIfGetDB.
                                                       RadioIfGetAllDB.
                                                       u4RadioOutOctets);
        u4RadioOutOctets +=
            FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetInterfaceOutOctets.
            u4IfOutOctets;

        RadioIfGetDB.RadioIfGetAllDB.u1RadioId = u1RadioCount;
        RadioIfGetDB.RadioIfIsGetAllDB.bRadioCounters = OSIX_TRUE;
        RadioIfGetDB.RadioIfGetAllDB.u4RadioInOctets = u4HwCount;
        RadioIfGetDB.RadioIfGetAllDB.u4RadioOutOctets = u4HwCount1;
        if (WssIfProcessRadioIfDBMsg (WSS_SET_PHY_RADIO_IF_DB, &RadioIfGetDB)
            != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to get Radio Operationa State from RadioIf \r\n");
            return;
        }
    }
#endif
    u4CurrSentBytes = ApHdlrGetApSentBytes (gau1IfName) + u4RadioInOctets;

    if (WssIfProcessApHdlrDBMsg (WSS_APHDLR_GET_AP_STATS,
                                 &WssIfApHdlrDB) != OSIX_SUCCESS)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC, "Failed to Get WTP Reboot Stats \r\n");
        return;
    }
    ApHdlrMsg.PmWtpEventReq.ApParamsElement.ApElement.u4SentBytes =
        (u4CurrSentBytes - WssIfApHdlrDB.ApStats.u4SentBytes);

    u4CurrRcvdBytes = ApHdlrGetApRcvdBytes (gau1IfName) + u4RadioOutOctets;

    if (WssIfProcessApHdlrDBMsg (WSS_APHDLR_GET_AP_STATS,
                                 &WssIfApHdlrDB) != OSIX_SUCCESS)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC, "Failed to Get WTP Reboot Stats \r\n");
        return;
    }
    ApHdlrMsg.PmWtpEventReq.ApParamsElement.ApElement.u4RcvdBytes =
        (u4CurrRcvdBytes - WssIfApHdlrDB.ApStats.u4RcvdBytes);

    WssIfApHdlrDB.ApStats.u4SentBytes = u4CurrSentBytes;
    WssIfApHdlrDB.ApStats.u4RcvdBytes = u4CurrRcvdBytes;

    if (WssIfProcessApHdlrDBMsg (WSS_APHDLR_SET_AP_STATS,
                                 &WssIfApHdlrDB) != OSIX_SUCCESS)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC, "Failed to Get WTP Reboot Stats \r\n");
        return;
    }
    return;
}

#ifdef BAND_SELECT_WANTED
/*****************************************************************************
 * Function                  : ApHdlrProbeTmrStart                           *
 *                                                                           *
 * Description               : This routine start the APprobe   timer        *
 *                                                                           *
 *                                                                           *
 * Input                     : None                                          *
 *                                                                           *
 * Output                    : None                                          *
 *                                                                           *
 * Returns                   : None.                                         *
 *                                                                           *
 *****************************************************************************/
INT4
ApHdlrProbeTmrStart (tWssStaBandSteerDB * pWssStaBandSteerDB,
                     UINT1 u1AgeOutSuppression)
{
    APHDLR_TRC (APHDLR_MGMT_TRC,
                "BAND_STEER :- Starting Probe validity timer ....\r\n");

    if (TmrStart
        (gApHdlrTmrList.ApHdlrTmrListId, &(pWssStaBandSteerDB->ProbeEntryTmr),
         APHDLR_PROBE_REQ_TMR, u1AgeOutSuppression, 0) == TMR_FAILURE)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC, "ApHdlrProbeTmrStart: "
                    "Failed to start the Timer Type \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function                  : APHdlrProbeTmrStop                            *
 *                                                                           *
 * Description               : This routine stops the probe timer.           *
 *                                                                           *
 *                                                                           *
 * Input                     : None                                          *
 *                                                                           *
 * Output                    : None                                          *
 *                                                                           *
 * Returns                   : None.                                         *
 *                                                                           *
 *****************************************************************************/
INT4
ApHdlrProbeTmrStop (tWssStaBandSteerDB * pWssStaBandSteerDB)
{
    UINT4               u4RemainingTime = 0;
    UINT4               u4TmrRetVal = TMR_SUCCESS;
    INT4                i4RetVal = OSIX_FAILURE;
    u4TmrRetVal = TmrGetRemainingTime (gApHdlrTmrList.ApHdlrTmrListId,
                                       &(pWssStaBandSteerDB->ProbeEntryTmr.
                                         TimerNode), &u4RemainingTime);
    if (u4TmrRetVal != TMR_FAILURE)
    {
        if (TmrStop (gApHdlrTmrList.ApHdlrTmrListId,
                     &(pWssStaBandSteerDB->ProbeEntryTmr)) != TMR_SUCCESS)
        {
            APHDLR_TRC (APHDLR_FAILURE_TRC, "AphdlrTmrStop: "
                        "Failure to stop Probe request entry Timer\r\n");
            return i4RetVal;
        }
        u4RemainingTime = 0;
        i4RetVal = OSIX_SUCCESS;
    }
    return i4RetVal;
}

/*****************************************************************************
 * Function                  : ApHdlrStaProbeExp                            *
 *                                                                           *
 * Description               : This routine handles the probe request entry  *
 *                             Expiry.                                       *
 *                                                                           *
 * Input                     : None                                          *
 *                                                                           *
 * Output                    : None                                          *
 *                                                                           *
 * Returns                   : None.                                         *
 *                                                                           *
 *****************************************************************************/

VOID
ApHdlrStaProbeExp (VOID *pArg)
{
    APHDLR_TRC (APHDLR_MGMT_TRC,
                "\n BAND_STEER :- Station Entry validity timer expired....\r\n");

    tWssStaBandSteerDB *pWssStaBandSteerDB = NULL;
    UINT1               u1WlanId = 0;
    UINT1               au1RadioName[RADIO_INTF_NAME_LEN] = { 0 };

    APHDLR_FN_ENTRY ();

    pWssStaBandSteerDB = (tWssStaBandSteerDB *) pArg;

    if (pWssStaBandSteerDB == NULL)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC, "ApHdlrStaProbeExp: "
                    "Failed BandSteerDB is null\r\n");
        return;
    }
    if (WssStaUpdateAPBandSteerProcessDB (WSSSTA_GET_BAND_STEER_DB,
                                          pWssStaBandSteerDB) == OSIX_SUCCESS)
    {
        APHDLR_TRC6 (APHDLR_MGMT_TRC,
                     "Remove Probe entry for %02x:%02x:%02x:%02x:%02x:%02x\n",
                     pWssStaBandSteerDB->stationMacAddress[0],
                     pWssStaBandSteerDB->stationMacAddress[1],
                     pWssStaBandSteerDB->stationMacAddress[2],
                     pWssStaBandSteerDB->stationMacAddress[3],
                     pWssStaBandSteerDB->stationMacAddress[4],
                     pWssStaBandSteerDB->stationMacAddress[5]);
#ifdef NPAPI_WANTED
        SPRINTF ((CHR1 *) au1RadioName, "%s%d", RADIO_INTF_NAME,
                 (pWssStaBandSteerDB->u1RadioId) - 1);
#endif
        u1WlanId = pWssStaBandSteerDB->u1WlanId;
        RadioIfDelStaBandSteerDB (pWssStaBandSteerDB->stationMacAddress,
                                  au1RadioName, &u1WlanId);
        if (WssStaUpdateAPBandSteerProcessDB (WSSSTA_DESTROY_BAND_STEER_DB,
                                              pWssStaBandSteerDB) ==
            OSIX_FAILURE)
        {
            APHDLR_TRC6 (APHDLR_MGMT_TRC,
                         "Remove Probe entry failed for %02x:%02x:%02x:%02x:%02x:%02x\n",
                         pWssStaBandSteerDB->stationMacAddress[0],
                         pWssStaBandSteerDB->stationMacAddress[1],
                         pWssStaBandSteerDB->stationMacAddress[2],
                         pWssStaBandSteerDB->stationMacAddress[3],
                         pWssStaBandSteerDB->stationMacAddress[4],
                         pWssStaBandSteerDB->stationMacAddress[5]);
            APHDLR_TRC (APHDLR_FAILURE_TRC, "ApHdlrStaProbeExp: "
                        "Failed to delete entry in BandSteerBD \r\n");
            return;
        }

        APHDLR_TRC (APHDLR_MGMT_TRC,
                    "BAND_STEER :- Removed the STA from Band steer DB ...\r\n");

    }
    APHDLR_FN_EXIT ();
    return;
}
#endif
#endif
