/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: aphdlrconfigupdate.c,v 1.5.2.1 2018/03/15 13:02:41 siva Exp $
 *
 * Description: This file contains the APHDLR Config Update routines.
 *
 *****************************************************************************/
#ifndef __APHDLR_UPDATE_C__
#define __APHDLR_UPDATE_C__

#include "aphdlrinc.h"
#include "firewall.h"
#include "dhcpslow.h"
#include "fsmidhlw.h"
#ifdef DHCP_RLY_WANTED
#include "dhcpincs.h"
#endif
#include "dhcp.h"
#include "stdiplow.h"
#include "fsfwllw.h"
#include "wsswlanwtpproto.h"
#ifdef KERNEL_CAPWAP_WANTED
#include "fcusprot.h"
#endif
extern eState       gCapwapState;

#ifdef HOSTAPD_WANTED
UINT1               gu1HostApdReset = OSIX_FALSE;
#endif

extern UINT1
       CfaGddGetIfIndexforInterface (UINT1 *u1InterfaceName, INT4 *i4Ifindex);
extern INT1
 
 
 
  nmhSetIfIvrBridgedIface (INT4 i4IfMainIndex, INT4 i4SetValIfIvrBridgedIface);
extern INT1         nmhSetIfIpAddr (INT4 i4IfMainIndex, UINT4 u4SetValIfIpAddr);
extern INT1
        nmhSetIfIpSubnetMask (INT4 i4IfMainIndex, UINT4 u4SetValIfIpSubnetMask);
extern INT1 nmhGetIfMainNetworkType ARG_LIST ((INT4, INT4 *));
extern INT1 nmhTestv2IfMainNetworkType ARG_LIST ((UINT4 *, INT4, INT4));
extern INT1 nmhGetIfIpAddr ARG_LIST ((INT4, UINT4 *));

extern INT1 nmhGetIfIpSubnetMask ARG_LIST ((INT4, UINT4 *));
extern INT1 nmhTestv2IfIpAddr ARG_LIST ((UINT4 *, INT4, UINT4));

extern INT1 nmhTestv2IfIpSubnetMask ARG_LIST ((UINT4 *, INT4, UINT4));

extern INT1 nmhTestv2IfIpBroadcastAddr ARG_LIST ((UINT4 *, INT4, UINT4));

extern INT1 nmhSetIfAdminStatus ARG_LIST ((INT4, INT4));
extern INT1 nmhTestv2IfAdminStatus ARG_LIST ((UINT4 *, INT4, INT4));

extern INT4
 
 
 
 
VlanConvertToLocalPortList (tPortList IfPortList, tLocalPortList LocalPortList);

extern INT1 nmhSetDot1qVlanStaticRowStatus ARG_LIST ((UINT4, INT4));

extern INT1 nmhTestv2Dot1qVlanStaticRowStatus ARG_LIST ((UINT4 *, UINT4, INT4));
extern INT1 nmhGetDot1qVlanStaticRowStatus ARG_LIST ((UINT4, INT4 *));
extern INT1
     
     
     
     
    nmhSetDot1qVlanStaticEgressPorts
ARG_LIST ((UINT4, tSNMP_OCTET_STRING_TYPE *));

extern INT4
 
      VlanUtilIsPortListValid (UINT4 u4ContextId, UINT1 *pu1Ports, INT4 i4Len);
extern INT1 nmhSetIfMainRowStatus ARG_LIST ((INT4, INT4));

extern INT1 nmhTestv2IfMainType ARG_LIST ((UINT4 *, INT4, INT4));

extern INT1 nmhSetIfMainType ARG_LIST ((INT4, INT4));
extern INT4 CfaTestIfIpAddrAndIfIpSubnetMask PROTO ((INT4, UINT4, UINT4));
extern INT1 nmhSetIfMainEncapDot1qVlanId ARG_LIST ((INT4, INT4));

extern INT1 nmhTestv2IfMainEncapDot1qVlanId ARG_LIST ((UINT4 *, INT4, INT4));

extern INT1 nmhSetIfAlias ARG_LIST ((INT4, tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhSetIfMainNetworkType ARG_LIST ((INT4, INT4));

extern INT1 nmhSetIfIpBroadcastAddr ARG_LIST ((INT4, UINT4));

extern INT1 nmhTestv2IfMainRowStatus ARG_LIST ((UINT4 *, INT4, INT4));

extern INT1         nmhGetIfAdminStatus (INT4, INT4 *);

extern INT4
        DhcpRelaySetServerOnly (tCliHandle CliHandle, INT4 i4Status);

extern INT1         nmhSetFwlGlobalMasterControlSwitch (INT4
                                                        i4SetValFwlGlobalMasterControlSwitch);
/************************************************************************/
/*  Function Name   : CapwapProcessConfigUpdateRequest                  */
/*  Description     : The function processes the received APHDLR        */
/*                    Config Update request                             */
/*  Input(s)        : ConfigUpdateReqMsg - APHDLR Config Update request */
/*                                           packet received            */
/*                    pSessEntry - Pointer to the session data base     */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
CapwapProcessConfigUpdateRequest (tSegment * pBuf,
                                  tCapwapControlPacket * pConfigUpdateReqMsg,
                                  tRemoteSessionManager * pSessEntry)
{
    INT4                i4Status = OSIX_SUCCESS;
    tConfigUpdateRsp    ConfigUpdateResp;
    UINT1              *pu1TxBuf = NULL, *pRcvdBuf = NULL;
    UINT1              *pu1TxBuf1 = NULL;
    UINT1               u1SeqNum = 0;
    UINT4               u4MsgLen = 0;
    tCRU_BUF_CHAIN_HEADER *pTxBuf = NULL;
    UINT1               u1IsNotLinear = OSIX_FALSE;
    UINT2               u2PacketLen = 0;
    UINT2               u2NumOptionalMsg = 0;
    UINT1               u1Index = 0;
    UINT4               u4Offset = 0;
    UINT2               u2Type = 0;
    UINT2               u2Len = 0;
    UINT4               u4VendorId = 0;
    UINT1               ImageId[CAPWAP_IMAGE_DATA_LEN];
    UINT2               u2MsgType = 0;
    UINT1               au1FlashFile[IMAGE_DATA_FLASH_FILE_NAME_LEN];
    UINT1               u1DestLen = 0;
    FILE               *pFlashFp = NULL;
    tImageDataReq       ImageDataReq;
    UINT1               u1ImageIDElementPresent = OSIX_FALSE;
    UINT1               u1StartImageDataTransfer = OSIX_FALSE;
    UINT2               u2ImageIdLen = 0;
    UINT2               u2StatsInterval = 0;
    UINT1               u1StartStatsProcessFlag = OSIX_FALSE;

    APHDLR_FN_ENTRY ();
    MEMSET (ImageId, 0, CAPWAP_IMAGE_DATA_LEN);
    MEMSET (&ImageDataReq, 0, sizeof (ImageDataReq));
    MEMSET (au1FlashFile, 0, IMAGE_DATA_FLASH_FILE_NAME_LEN);
    MEMSET (&ConfigUpdateResp, 0, sizeof (tConfigUpdateRsp));

    if (pSessEntry == NULL)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC, "APHDLR Session Entry is NULL \r\n");
        return OSIX_FAILURE;
    }
    if (pSessEntry->eCurrentState != CAPWAP_RUN)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC, "Session not in RUN state \r\n");
        return OSIX_FAILURE;
    }
    MEMSET (&ConfigUpdateResp, 0, sizeof (tConfigUpdateRsp));

    u2PacketLen = (UINT2) APHDLR_GET_BUF_LEN (pBuf);
    pRcvdBuf = APHDLR_BUF_IF_LINEAR (pBuf, 0, u2PacketLen);
    if (pRcvdBuf == NULL)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC, "Entered Precived buf is NUll \r\n");
        pRcvdBuf = (UINT1 *) MemAllocMemBlk (APHDLR_PKTBUF_POOLID);
        if (pRcvdBuf == NULL)
        {
            APHDLR_TRC (APHDLR_FAILURE_TRC,
                        "Memory Allocation for pRcvdBuf FAILURE\r\n");
            return OSIX_FAILURE;
        }
        APHDLR_COPY_FROM_BUF (pBuf, pRcvdBuf, 0, u2PacketLen);
        u1IsNotLinear = OSIX_TRUE;
    }
    /* Recieved sequence number */
    u1SeqNum = pConfigUpdateReqMsg->capwapCtrlHdr.u1SeqNum;

    /* Validate the Config Update Request message. if it is success 
     * construct the Config Update Response */
    if ((CapValidateConfUpdateReqMsgElem (pRcvdBuf, pConfigUpdateReqMsg,
                                          &ConfigUpdateResp) == OSIX_SUCCESS))
    {
        /* As Validtion is successful set the result code to SUCCESS */
        ConfigUpdateResp.resultCode.u4Value = CAPWAP_SUCCESS;
        ConfigUpdateResp.resultCode.u2MsgEleType = RESULT_CODE;
        ConfigUpdateResp.resultCode.u2MsgEleLen = RESULT_CODE_LEN;
        ConfigUpdateResp.u2CapwapMsgElemenLen = (UINT2)
            (ConfigUpdateResp.u2CapwapMsgElemenLen +
             ConfigUpdateResp.resultCode.u2MsgEleLen +
             CAPWAP_MSG_ELEM_TYPE_LEN);
    }
    else
    {
        /* As Validtion is failure set the result code to appr.failure code */
        ConfigUpdateResp.resultCode.u4Value = CONFIG_FAIL_SERVICE_NOTPROVIDED;
        ConfigUpdateResp.resultCode.u2MsgEleType = RESULT_CODE;
        ConfigUpdateResp.resultCode.u2MsgEleLen = RESULT_CODE_LEN;
        ConfigUpdateResp.u2CapwapMsgElemenLen = (UINT2)
            (ConfigUpdateResp.resultCode.u2MsgEleLen +
             CAPWAP_MSG_ELEM_TYPE_LEN);
    }

    pSessEntry->wtpCapwapStats.cfgReqSent++;

    u2NumOptionalMsg = pConfigUpdateReqMsg->numOptionalElements;
    for (u1Index = 0; u1Index < u2NumOptionalMsg; u1Index++)
    {
        /* Get the message element type */
        u2MsgType = pConfigUpdateReqMsg->capwapMsgElm[u1Index].u2MsgType;

        if (u2MsgType == IMAGE_IDENTIFIER)
        {
            u4Offset = pConfigUpdateReqMsg->capwapMsgElm[u1Index].pu2Offset[0];
            pRcvdBuf += u4Offset;
            CAPWAP_GET_2BYTE (u2Type, pRcvdBuf);
            CAPWAP_GET_2BYTE (u2Len, pRcvdBuf);
            CAPWAP_GET_4BYTE (u4VendorId, pRcvdBuf);
            u2ImageIdLen = (UINT2) (u2Len - CAPWAP_MSG_ELEM_TYPE_LEN);
            CAPWAP_GET_NBYTE (ImageId, pRcvdBuf, u2ImageIdLen);
            u1ImageIDElementPresent = OSIX_TRUE;
            SPRINTF ((char *) au1FlashFile, "rm -f %s%s",
                     IMAGE_DATA_FLASH_CONF_LOC, pSessEntry->ImageId.data);

            system ((char *) au1FlashFile);
            system ("/LogCleanup.sh");
            system ("sync");
            MEMSET (au1FlashFile, 0, IMAGE_DATA_FLASH_FILE_NAME_LEN);

            MEMSET (&pSessEntry->ImageId.data, 0, CAPWAP_MAX_IMAGEID_DATA_LEN);
            MEMCPY (&pSessEntry->ImageId.data, &ImageId, u2ImageIdLen);
            pSessEntry->ImageId.u2MsgEleLen = u2ImageIdLen;
            break;
        }
    }

    u2NumOptionalMsg = pConfigUpdateReqMsg->numOptionalElements;
    for (u1Index = 0; u1Index < u2NumOptionalMsg; u1Index++)
    {
        /* Get the message element type */
        u2MsgType = pConfigUpdateReqMsg->capwapMsgElm[u1Index].u2MsgType;
        if (u2MsgType == STATS_TIMER)
        {
            u4Offset = pConfigUpdateReqMsg->capwapMsgElm[u1Index].pu2Offset[0];
            pRcvdBuf += u4Offset;
            CAPWAP_GET_2BYTE (u2Type, pRcvdBuf);
            CAPWAP_GET_2BYTE (u2Len, pRcvdBuf);
            CAPWAP_GET_2BYTE (u2StatsInterval, pRcvdBuf);
            u1StartStatsProcessFlag = OSIX_TRUE;
            break;
        }
    }

    if (OSIX_TRUE == u1ImageIDElementPresent)
    {
#ifdef DEBUG_WANTED
        if (STRCMP (ImageId, WTP_IMAGE_ID) != 0)
        {
#endif
            APHDLR_TRC (APHDLR_MGMT_TRC, " Start Image Transfer \n");

            u1DestLen =
                (UINT1) (u2ImageIdLen + STRLEN (IMAGE_DATA_FLASH_CONF_LOC));
            SNPRINTF ((char *) au1FlashFile, (UINT4) (u1DestLen + 1), "%s%s",
                      IMAGE_DATA_FLASH_CONF_LOC, ImageId);
            if ((pFlashFp = FOPEN ((char *) au1FlashFile, "r")) == NULL)
            {
                APHDLR_TRC (APHDLR_MGMT_TRC, "File not found in Flash \r\n");
                u1StartImageDataTransfer = OSIX_TRUE;
            }
            else
            {
                APHDLR_TRC (APHDLR_MGMT_TRC, "Requested Image is found in "
                            "Flash so wait for reset request with it \r\n");
                ConfigUpdateResp.resultCode.u4Value = IMAGEDATA_ALREADY_PRESENT;
            }
#ifdef DEBUG_WANTED
        }
        else
        {
            u1DestLen =
                (UINT1) (u2ImageIdLen + STRLEN (IMAGE_DATA_FLASH_CONF_LOC));
            SNPRINTF ((CHR1 *) au1FlashFile, (UINT4) (u1DestLen + 1), "%s%s",
                      IMAGE_DATA_FLASH_CONF_LOC, ImageId);
            /* If Image is not found in Flash */
            if ((pFlashFp = FOPEN ((char *) au1FlashFile, "r")) == NULL)
            {
                APHDLR_TRC (APHDLR_MGMT_TRC, "File not found in Flash \r\n");
                u1StartImageDataTransfer = OSIX_TRUE;
            }
            else
            {
                APHDLR_TRC (APHDLR_MGMT_TRC,
                            "File is found in Flash so reset with that \r\n");
            }
        }
#endif
        if (pFlashFp != NULL)
        {
            FCLOSE (pFlashFp);
        }
    }

    if (u1StartStatsProcessFlag == OSIX_TRUE)
    {

        if (u2StatsInterval != 0)
        {
            i4Status = ApHdlrTmrStart (pSessEntry, APHDLR_STATS_INTERVAL_TMR,
                                       u2StatsInterval);
            if (i4Status == OSIX_FAILURE)
            {
                if (u1IsNotLinear == OSIX_TRUE)
                {
                    MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pRcvdBuf);
                }
                APHDLR_TRC (APHDLR_FAILURE_TRC,
                            "Failed to start the Stats Timer \n");
                return OSIX_FAILURE;
            }
        }
        else
        {
            ApHdlrGetApAndRadioStats ();
        }
    }

    if (u1IsNotLinear == OSIX_TRUE)
    {
        MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pRcvdBuf);
    }

    if (CapwapConstructCpHeader (&ConfigUpdateResp.capwapHdr) == OSIX_FAILURE)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC,
                    "Failed to construct the capwap Header\n");
        return OSIX_FAILURE;
    }
    /* update Control Header */
    ConfigUpdateResp.u1SeqNum = pConfigUpdateReqMsg->capwapCtrlHdr.u1SeqNum;
    ConfigUpdateResp.u2CapwapMsgElemenLen = (UINT2)
        (ConfigUpdateResp.u2CapwapMsgElemenLen +
         u4MsgLen + CAPWAP_MSG_ELEM_PLUS_SEQ_LEN);
    ConfigUpdateResp.u1NumMsgBlocks = 0;    /* flags always zero */

    /* Allocate memory for packet linear buffer */
    pTxBuf = CAPWAP_ALLOCATE_CRU_BUF (CAPWAP_MAX_PKT_LEN, 0);
    if (pTxBuf == NULL)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC, "Memory Allocation Failed\n");
        return OSIX_FAILURE;
    }
    pu1TxBuf = APHDLR_BUF_IF_LINEAR (pTxBuf, 0, CAPWAP_MAX_PKT_LEN);
    if (pu1TxBuf == NULL)
    {
        pu1TxBuf = (UINT1 *) MemAllocMemBlk (APHDLR_PKTBUF_POOLID);
        if (pu1TxBuf == NULL)
        {
            APHDLR_TRC (APHDLR_FAILURE_TRC, "Memory Allocation Failed\n");
            CAPWAP_RELEASE_CRU_BUF (pTxBuf);
            return OSIX_FAILURE;
        }

        CAPWAP_COPY_FROM_BUF (pTxBuf, pu1TxBuf, 0, CAPWAP_MAX_PKT_LEN);
        u1IsNotLinear = OSIX_TRUE;
    }

#ifdef HOSTAPD_WANTED
    /* Config update request is recieved. This indicates that
     * hostapd has to be reset. Once the hostapd timer expires, if the
     * below varaibale is set, NPAPI call to restart the hostapd 
     * process will be called */
    gu1HostApdReset = OSIX_TRUE;
#endif

    if ((CapwapAssembleConfigUpdateResp (ConfigUpdateResp.u2CapwapMsgElemenLen,
                                         &ConfigUpdateResp,
                                         pu1TxBuf)) == OSIX_FAILURE)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC,
                    "Failed to assemble the Config Update Response Message \r\n");
        if (u1IsNotLinear == OSIX_TRUE)
        {
            MemReleaseMemBlock (APHDLR_PKTBUF_POOLID, pu1TxBuf);
        }
        CAPWAP_RELEASE_CRU_BUF (pTxBuf);
        return OSIX_FAILURE;
    }

    if (u1IsNotLinear == OSIX_TRUE)
    {
        APHDLR_COPY_TO_BUF (pTxBuf, pu1TxBuf, 0, CAPWAP_MAX_PKT_LEN);
        MemReleaseMemBlock (APHDLR_PKTBUF_POOLID, pu1TxBuf);
    }

    /* Transmit Configuration Update Response Message */
    i4Status = CapwapTxMessage (pTxBuf,
                                pSessEntry,
                                (UINT4) (ConfigUpdateResp.u2CapwapMsgElemenLen +
                                         CAPWAP_CMN_HDR_LEN),
                                WSS_CAPWAP_802_11_CTRL_PKT);

    if (i4Status == OSIX_FAILURE)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC, "Failed to Transmit the packet\n");
    }
    CAPWAP_RELEASE_CRU_BUF (pTxBuf);
    pSessEntry->lastTransmitedSeqNum++;
    pSessEntry->wtpCapwapStats.cfgRespRxd++;
    /* After sending response now send Image Data Req if Image Id is present */
    /* Checking if the Image Version in AP is same as the version WLC is
     * requesting AP to upgrade */
    if (u1StartImageDataTransfer == OSIX_TRUE)
    {
        u4MsgLen = 0;
        /* Received the valid Join Response so change the
         * current state in the RSM */

        if (pSessEntry->eCurrentState != CAPWAP_RUN)
        {
            APHDLR_TRC (APHDLR_MGMT_TRC,
                        "Entered CAPWAP IMAGE Data State \r\n");
            pSessEntry->eCurrentState = CAPWAP_IMAGEDATA;
            gCapwapState = CAPWAP_IMAGEDATA;
        }

        MEMCPY (&ImageDataReq.ImageId.data, ImageId, u2ImageIdLen);
        ImageDataReq.ImageId.u2MsgEleLen = u2ImageIdLen;
        if (CapwapGetImageDataReqElements (&ImageDataReq, &u4MsgLen)
            == OSIX_SUCCESS)
        {
            /* update the seq number */
            ImageDataReq.u1SeqNum =
                (UINT1) (pSessEntry->lastTransmitedSeqNum + 1);
            ImageDataReq.u2CapwapMsgElemenLen =
                (UINT2) (u4MsgLen + CAPWAP_MSG_ELEM_PLUS_SEQ_LEN);

            pTxBuf = CAPWAP_ALLOCATE_CRU_BUF (CAPWAP_MAX_PKT_LEN, 0);
            if (pTxBuf == NULL)
            {
                APHDLR_TRC (APHDLR_FAILURE_TRC, "Memory Alloc Failed \r\n");
                return OSIX_FAILURE;
            }
            pu1TxBuf1 = CAPWAP_BUF_IF_LINEAR (pBuf, 0, CAPWAP_MAX_PKT_LEN);
            if (pu1TxBuf1 == NULL)
            {
                pu1TxBuf1 = (UINT1 *) MemAllocMemBlk (CAPWAP_PKTBUF_POOLID);
                if (pu1TxBuf1 == NULL)
                {
                    APHDLR_TRC (APHDLR_FAILURE_TRC, "Memory Alloc Failed \r\n");
                    CAPWAP_RELEASE_CRU_BUF (pTxBuf);
                    return OSIX_FAILURE;
                }
                CAPWAP_COPY_FROM_BUF (pTxBuf, pu1TxBuf1, 0, CAPWAP_MAX_PKT_LEN);
                u1IsNotLinear = OSIX_TRUE;
            }

            if ((CapwapAssembleWTPImageDataReq
                 (ImageDataReq.u2CapwapMsgElemenLen, &ImageDataReq,
                  pu1TxBuf1)) == OSIX_FAILURE)
            {
                CAPWAP_RELEASE_CRU_BUF (pTxBuf);
                return OSIX_FAILURE;
            }
            if (u1IsNotLinear == OSIX_TRUE)
            {
                /* If the CRU buffer memory is not linear */
                CAPWAP_COPY_TO_BUF (pTxBuf, pu1TxBuf1, 0, CAPWAP_MAX_PKT_LEN);
                MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pu1TxBuf1);
            }

            i4Status = CapwapTxMessage (pTxBuf,
                                        pSessEntry,
                                        (UINT4) (ImageDataReq.
                                                 u2CapwapMsgElemenLen +
                                                 CAPWAP_CMN_HDR_LEN),
                                        WSS_CAPWAP_802_11_CTRL_PKT);
            if (i4Status == OSIX_SUCCESS)
            {
                /* Update the last transmitted sequence number */
                pSessEntry->lastTransmitedSeqNum++;
            }
            else
            {
                /* Handle the failure */
                APHDLR_TRC (APHDLR_FAILURE_TRC,
                            "Failed to Transmit the packet \r\n");
                return OSIX_FAILURE;
            }
            CAPWAP_RELEASE_CRU_BUF (pTxBuf);
        }
    }

    UNUSED_PARAM (u1SeqNum);
    APHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapValidateConfUpdateReqMsgElem                   */
/*  Description     : The function validates the functional             */
/*                    characteristics of the received APHDLR            */
/*                    Config Update request agains the configured       */
/*                    WTP profile                                       */
/*  Input(s)        : ConfigUpdateReqMsg - parsed APHDLR Config Update  */
/*                                           request                    */
/*                    pRcvBuf - Received Config Update packet           */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
CapValidateConfUpdateReqMsgElem (UINT1 *pRcvBuf,
                                 tCapwapControlPacket * pConfigUpdateReqMsg,
                                 tConfigUpdateRsp * pConfigUpdateRsp)
{
    UINT2               u2NumOptionalMsg = 0, u2MsgType = 0;
    UINT1               u1Index = 0;
    tVendorSpecPayload  vendSpec;

    APHDLR_FN_ENTRY ();

    if (pRcvBuf == NULL)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC, "pRcvBuf is not assigned \r\n");
        return OSIX_FAILURE;
    }
    if (pConfigUpdateReqMsg == NULL)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC,
                    "pConfigUpdateReqMsg is not assigned \r\n");
        return OSIX_FAILURE;
    }
    u2NumOptionalMsg = pConfigUpdateReqMsg->numOptionalElements;
    MEMSET (&vendSpec, 0, sizeof (tVendorSpecPayload));

    /* Validate IEEE radio optional elements */
    for (u1Index = 0; u1Index < u2NumOptionalMsg; u1Index++)
    {
        /* Get the message element type */
        u2MsgType = pConfigUpdateReqMsg->
            capwapMsgElm[MAX_CONF_UPDATE_REQ_MAND_MSG_ELEMENTS + u1Index].
            u2MsgType;
        switch (u2MsgType)
        {
            case RADIO_ADMIN_STATE:
                if (CapwapSetRadioAdminStateUpdate (pRcvBuf,
                                                    pConfigUpdateReqMsg,
                                                    pConfigUpdateRsp,
                                                    u1Index) == OSIX_FAILURE)
                {
                    APHDLR_TRC (APHDLR_FAILURE_TRC,
                                "Failed to Set the Radio Admin State \r\n");
                    return OSIX_FAILURE;
                }
                break;
            case IEEE_ANTENNA:
                if (CapwapSetRadioAntennaUpdate (pRcvBuf, pConfigUpdateReqMsg,
                                                 pConfigUpdateRsp,
                                                 u1Index) == OSIX_FAILURE)
                {
                    APHDLR_TRC (APHDLR_FAILURE_TRC,
                                "Failed to Set the Radio Admin State \r\n");
                    return OSIX_FAILURE;
                }
                break;
            case IEEE_DIRECT_SEQUENCE_CONTROL:
                if (CapwapSetIeeeDSCUpdate (pRcvBuf, pConfigUpdateReqMsg,
                                            pConfigUpdateRsp,
                                            u1Index) == OSIX_FAILURE)
                {
                    APHDLR_TRC (APHDLR_FAILURE_TRC,
                                "Failed to Set Direct Sequence control \r\n");
                    return OSIX_FAILURE;
                }
                break;
            case IEEE_INFORMATION_ELEMENT:
                if (CapwapSetIeeeInfoElemUpdate (pRcvBuf, pConfigUpdateReqMsg,
                                                 pConfigUpdateRsp,
                                                 u1Index) == OSIX_FAILURE)
                {
                    APHDLR_TRC (APHDLR_FAILURE_TRC, "Failed to Set "
                                "Information Element \r\n");
                    return OSIX_FAILURE;
                }
                break;

            case IEEE_MAC_OPERATION:
                if (CapwapSetMacOperationUpdate (pRcvBuf, pConfigUpdateReqMsg,
                                                 pConfigUpdateRsp,
                                                 u1Index) == OSIX_FAILURE)
                {
                    APHDLR_TRC (APHDLR_FAILURE_TRC,
                                "Failed to Set the Radio Admin State \r\n");
                    return OSIX_FAILURE;
                }
                break;
            case IEEE_MULTIDOMAIN_CAPABILITY:
                if (CapwapSetIeeeMultiDomainCapabilityUpdate (pRcvBuf,
                                                              pConfigUpdateReqMsg,
                                                              pConfigUpdateRsp,
                                                              u1Index) ==
                    OSIX_FAILURE)
                {
                    APHDLR_TRC (APHDLR_FAILURE_TRC,
                                "Failed to Set the Radio Admin State \r\n");
                    return OSIX_FAILURE;
                }
                break;
            case IEEE_OFDM_CONTROL:
                if (CapwapSetIeeeOFDMControlUpdate (pRcvBuf,
                                                    pConfigUpdateReqMsg,
                                                    pConfigUpdateRsp,
                                                    u1Index) == OSIX_FAILURE)
                {
                    APHDLR_TRC (APHDLR_FAILURE_TRC,
                                "Failed to Set OFDM Control \r\n");
                    return OSIX_FAILURE;
                }
                break;
            case IEEE_RATE_SET:
                if (CapwapSetIeeeRateSetUpdate (pRcvBuf, pConfigUpdateReqMsg,
                                                pConfigUpdateRsp,
                                                u1Index) == OSIX_FAILURE)
                {
                    APHDLR_TRC (APHDLR_FAILURE_TRC,
                                "Failed to Set OFDM Control \r\n");
                    return OSIX_FAILURE;
                }
                break;
            case IEEE_TX_POWER:
                if (CapwapSetIeeeTxPowerUpdate (pRcvBuf, pConfigUpdateReqMsg,
                                                pConfigUpdateRsp,
                                                u1Index) == OSIX_FAILURE)
                {
                    APHDLR_TRC (APHDLR_FAILURE_TRC,
                                "Failed to Set Tx Power \r\n");
                    return OSIX_FAILURE;
                }
                break;

            case IEEE_WTP_QOS:
                if (CapwapSetIeeeQualityUpdate (pRcvBuf, pConfigUpdateReqMsg,
                                                pConfigUpdateRsp,
                                                u1Index) == OSIX_FAILURE)
                {
                    APHDLR_TRC (APHDLR_FAILURE_TRC,
                                "Failed to Set WTP QoS \r\n");
                    return OSIX_FAILURE;
                }
                break;

            case IEEE_WTP_RADIO_CONFIGURATION:
                if (CapwapSetIeeeRadioConfigUpdate (pRcvBuf,
                                                    pConfigUpdateReqMsg,
                                                    pConfigUpdateRsp,
                                                    u1Index) == OSIX_FAILURE)
                {
                    APHDLR_TRC (APHDLR_FAILURE_TRC,
                                "Failed to Set Radio Config \r\n");
                    return OSIX_FAILURE;
                }
                break;

            case WTP_RADIO_INFO:
                if (CapwapSetIeeeRadioInfoUpdate (pRcvBuf,
                                                  pConfigUpdateReqMsg,
                                                  pConfigUpdateRsp,
                                                  u1Index) == OSIX_FAILURE)
                {
                    APHDLR_TRC (APHDLR_FAILURE_TRC,
                                "Failed to Set Radio Info \r\n");
                    return OSIX_FAILURE;
                }
                break;
            case LOCATION_DATA:
                if (CapwapValidateLocationData (pRcvBuf,
                                                pConfigUpdateReqMsg,
                                                u1Index) == OSIX_FAILURE)
                {
                    APHDLR_TRC (APHDLR_FAILURE_TRC,
                                "Failed to Set Location Data \r\n");
                    return OSIX_FAILURE;
                }
                break;
            case VENDOR_SPECIFIC_PAYLOAD:
                if (CapwapValidateConfigUpdateVendSpecPld (pRcvBuf,
                                                           pConfigUpdateReqMsg,
                                                           &vendSpec,
                                                           u1Index) ==
                    OSIX_FAILURE)
                {
                    APHDLR_TRC (APHDLR_FAILURE_TRC,
                                "Failed to Set vendor specific payload \r\n");
                    return OSIX_FAILURE;
                }
                break;
            case WTP_NAME:
                if (CapwapValidateWtpName (pRcvBuf,
                                           pConfigUpdateReqMsg,
                                           u1Index) == OSIX_FAILURE)
                {
                    APHDLR_TRC (APHDLR_FAILURE_TRC,
                                "Failed to Set WTP name \r\n");
                    return OSIX_FAILURE;
                }
                break;
            case IMAGE_IDENTIFIER:
                if (CapwapValidateImageId (pRcvBuf,
                                           pConfigUpdateReqMsg,
                                           u1Index) == OSIX_FAILURE)
                {
                    APHDLR_TRC (APHDLR_FAILURE_TRC,
                                "Failed to Set Image Id \r\n");
                    return OSIX_FAILURE;
                }
                break;
            case CAPWAP_TIMERS:
                if (CapwapValidateCapwapTimer (pRcvBuf,
                                               pConfigUpdateReqMsg,
                                               u1Index) == OSIX_FAILURE)
                {
                    APHDLR_TRC (APHDLR_FAILURE_TRC,
                                "Failed to Set capwap timers \r\n");
                    return OSIX_FAILURE;
                }
                break;

            case DECRYPTION_ERROR_REPORT_PERIOD:
                if (CapwapValidateDecryptErrReportPeriod (pRcvBuf,
                                                          pConfigUpdateReqMsg,
                                                          u1Index) ==
                    OSIX_FAILURE)
                {
                    APHDLR_TRC (APHDLR_FAILURE_TRC,
                                "Failed to Set decrypt error period report. \r\n");
                    return OSIX_FAILURE;
                }
                break;

            case IDLE_TIMEOUT:

                if (CapwapValidateIdleTimeout (pRcvBuf,
                                               pConfigUpdateReqMsg,
                                               u1Index) == OSIX_FAILURE)
                {
                    APHDLR_TRC (APHDLR_FAILURE_TRC,
                                "Failed to Set decrypt error period report. \r\n");
                    return OSIX_FAILURE;
                }
                break;
            case WTP_FALLBACK:
                if (CapwapValidateWtpFallback (pRcvBuf,
                                               pConfigUpdateReqMsg,
                                               u1Index) == OSIX_FAILURE)
                {
                    APHDLR_TRC (APHDLR_FAILURE_TRC,
                                "Failed to Set WTP fallback. \r\n");
                    return OSIX_FAILURE;
                }
                break;

            case STATS_TIMER:
                if (CapwapValidateStatsTimer (pRcvBuf,
                                              pConfigUpdateReqMsg,
                                              u1Index) == OSIX_FAILURE)
                {
                    APHDLR_TRC (APHDLR_FAILURE_TRC,
                                "Failed to Set Stats Timer. \r\n");
                    return OSIX_FAILURE;
                }
                break;

            case AC_NAME_WITH_PRIO:
                if (CapwapValidateAcNamePriority (pRcvBuf,
                                                  pConfigUpdateReqMsg,
                                                  u1Index) == OSIX_FAILURE)
                {
                    APHDLR_TRC (APHDLR_FAILURE_TRC,
                                "Failed to Set AC Name with Priority. \r\n");
                    return OSIX_FAILURE;
                }
                break;
            case AC_TIMESTAMP:
                if (CapwapValidateAcTimeStamp (pRcvBuf,
                                               pConfigUpdateReqMsg,
                                               u1Index) == OSIX_FAILURE)
                {
                    APHDLR_TRC (APHDLR_FAILURE_TRC,
                                "Failed to Set AC Timestamp. \r\n");
                    return OSIX_FAILURE;
                }
                break;
            case ADD_MAC_ACL_ENTRY:
                if (CapwapValidateAddMacEntry (pRcvBuf,
                                               pConfigUpdateReqMsg,
                                               u1Index) == OSIX_FAILURE)
                {
                    APHDLR_TRC (APHDLR_FAILURE_TRC,
                                "Failed to set add ACL Mac Entry. \r\n");
                    return OSIX_FAILURE;
                }
                break;
            case DELETE_MAC_ACL_ENTRY:
                if (CapwapValidateDeleteMacEntry (pRcvBuf,
                                                  pConfigUpdateReqMsg,
                                                  u1Index) == OSIX_FAILURE)
                {
                    APHDLR_TRC (APHDLR_FAILURE_TRC,
                                "Failed to set delete ACL Mac Entry. \r\n");
                    return OSIX_FAILURE;
                }
                break;
            case WTP_STATIC_IP_ADDR_INFO:
                if (CapwapValidateWtpStaticIpAddress (pRcvBuf,
                                                      pConfigUpdateReqMsg,
                                                      u1Index) == OSIX_FAILURE)
                {
                    APHDLR_TRC (APHDLR_FAILURE_TRC,
                                "Failed to set static MAC entry. \r\n");
                    return OSIX_FAILURE;
                }
                break;
            default:
                APHDLR_TRC (APHDLR_FAILURE_TRC, "Failed to validate.\r\n");
                break;
        }
    }

    APHDLR_FN_EXIT ();
    return OSIX_SUCCESS;

}

/************************************************************************/
/*  Function Name   : CapwapSetRadioAdminStateUpdate                    */
/*  Description     : The function sets the Radio Admin State in DB     */
/*  Input(s)        : pRcvBuf - Received Buf                            */
/*                    pCwMsg - Pointer to Capwap Ctrl Msg               */
/*                    u2MsgIndex - Message Index                        */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS | OSIX_FAILURE                       */
/************************************************************************/
INT4
CapwapSetRadioAdminStateUpdate (UINT1 *pRcvBuf,
                                tCapwapControlPacket * pCwMsg,
                                tConfigUpdateRsp * pConfigUpdateRsp,
                                UINT2 u2MsgIndex)
{
    tRadioIfMsgStruct   RadioIfMsgStruct;
    UINT1              *pu1Buf = NULL;
    UINT2               u2NumMsgElems = 0;
    UINT1               u1Index = 0;
    UINT4               u4Offset = 0;

    UNUSED_PARAM (pConfigUpdateRsp);

    APHDLR_FN_ENTRY ();
    MEMSET (&RadioIfMsgStruct, 0, sizeof (tRadioIfMsgStruct));

    pu1Buf = pRcvBuf;
    u2NumMsgElems = pCwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;

    for (u1Index = 1; u1Index <= u2NumMsgElems; u1Index++)
    {
        u4Offset = pCwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[u1Index - 1];
        pRcvBuf = pu1Buf;
        pRcvBuf += u4Offset;

        CAPWAP_GET_2BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                          RadioIfAdminStatus.u2MessageType, pRcvBuf);
        CAPWAP_GET_2BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                          RadioIfAdminStatus.u2MessageLength, pRcvBuf);
        CAPWAP_GET_1BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                          RadioIfAdminStatus.u1RadioId, pRcvBuf);
        CAPWAP_GET_1BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                          RadioIfAdminStatus.u1AdminStatus, pRcvBuf);
        RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
            u2WtpInternalId = pCwMsg->u2IntProfileId;
        RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
            RadioIfAdminStatus.isPresent = OSIX_TRUE;

        if (WssIfProcessRadioIfMsg (WSS_RADIOIF_CONFIG_UPDATE_REQ_VALIDATE,
                                    &RadioIfMsgStruct) == OSIX_SUCCESS)
        {
            if (WssIfProcessRadioIfMsg (WSS_RADIOIF_CONFIG_UPDATE_REQ,
                                        &RadioIfMsgStruct) != OSIX_SUCCESS)
            {
                APHDLR_TRC (APHDLR_FAILURE_TRC,
                            "Failed to Set RADIO ADMIN STATE in Radio IF \r\n");
                return OSIX_FAILURE;
            }
        }
    }
    APHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapSetRadioAntennaUpdate                       */
/*  Description     : The function sets the Radio Antenna in DB         */
/*  Input(s)        : pRcvBuf - Received Buf                            */
/*                    pCwMsg - Pointer to Capwap Ctrl Msg               */
/*                    u2MsgIndex - Message Index                        */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS | OSIX_FAILURE                       */
/************************************************************************/
INT4
CapwapSetRadioAntennaUpdate (UINT1 *pRcvBuf,
                             tCapwapControlPacket * pCwMsg,
                             tConfigUpdateRsp * pConfigUpdateRsp,
                             UINT2 u2MsgIndex)
{
    tRadioIfMsgStruct   RadioIfMsgStruct;
    UINT1              *pu1Buf = NULL;
    UINT2               u2NumRadioInfo = 0;
    UINT1               u1Index = 0;
    UINT4               u4Offset = 0;
    UINT2               u2MsgLen = 0;

    UNUSED_PARAM (pConfigUpdateRsp);

    APHDLR_FN_ENTRY ();
    MEMSET (&RadioIfMsgStruct, 0, sizeof (tRadioIfMsgStruct));

    pu1Buf = pRcvBuf;

    u2NumRadioInfo = pCwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;

    for (u1Index = 1; u1Index <= u2NumRadioInfo; u1Index++)
    {
        u4Offset = pCwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[u1Index - 1];
        pRcvBuf = pu1Buf;
        pRcvBuf += u4Offset;

        CAPWAP_GET_2BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                          RadioIfAntenna.u2MessageType, pRcvBuf);
        CAPWAP_GET_2BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                          RadioIfAntenna.u2MessageLength, pRcvBuf);
        CAPWAP_GET_1BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                          RadioIfAntenna.u1RadioId, pRcvBuf);
        CAPWAP_GET_1BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                          RadioIfAntenna.u1ReceiveDiversity, pRcvBuf);
        CAPWAP_GET_1BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                          RadioIfAntenna.u1AntennaMode, pRcvBuf);
        CAPWAP_GET_1BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                          RadioIfAntenna.u1CurrentTxAntenna, pRcvBuf);

        u2MsgLen = RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
            RadioIfAntenna.u1CurrentTxAntenna;

        CAPWAP_GET_NBYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                          RadioIfAntenna.au1AntennaSelection, pRcvBuf,
                          u2MsgLen);

        RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.u2WtpInternalId
            = pCwMsg->u2IntProfileId;
        RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfAntenna.
            isPresent = OSIX_TRUE;

        if (WssIfProcessRadioIfMsg (WSS_RADIOIF_CONFIG_UPDATE_REQ_VALIDATE,
                                    &RadioIfMsgStruct) == OSIX_SUCCESS)
        {
            if (WssIfProcessRadioIfMsg (WSS_RADIOIF_CONFIG_UPDATE_REQ,
                                        &RadioIfMsgStruct) != OSIX_SUCCESS)
            {
                APHDLR_TRC (APHDLR_FAILURE_TRC,
                            "Failed to Set RADIO INFO in Radio IF \r\n");
                return OSIX_FAILURE;
            }
        }
        else
        {
            APHDLR_TRC (APHDLR_FAILURE_TRC,
                        "Failed to Validate Radio Config Update Request \r\n");
            return OSIX_FAILURE;
        }
    }
    APHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapSetMacOperationUpdate                       */
/*  Description     : The function sets the Mac Operation in DB         */
/*  Input(s)        : pRcvBuf - Received Buf                            */
/*                    pCwMsg - Pointer to Capwap Ctrl Msg               */
/*                    u2MsgIndex - Message Index                        */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS | OSIX_FAILURE                       */
/************************************************************************/
INT4
CapwapSetMacOperationUpdate (UINT1 *pRcvBuf,
                             tCapwapControlPacket * pCwMsg,
                             tConfigUpdateRsp * pConfigUpdateRsp,
                             UINT2 u2MsgIndex)
{
    tRadioIfMsgStruct   RadioIfMsgStruct;
    UINT1              *pu1Buf = NULL;
    UINT2               u2NumRadioInfo = 0;
    UINT1               u1Index = 0;
    UINT4               u4Offset = 0;
    UNUSED_PARAM (pConfigUpdateRsp);

    APHDLR_FN_ENTRY ();
    MEMSET (&RadioIfMsgStruct, 0, sizeof (tRadioIfMsgStruct));

    pu1Buf = pRcvBuf;

    u2NumRadioInfo = pCwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;

    for (u1Index = 1; u1Index <= u2NumRadioInfo; u1Index++)
    {
        u4Offset = pCwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[u1Index - 1];
        pRcvBuf = pu1Buf;
        pRcvBuf += u4Offset;

        CAPWAP_GET_2BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                          RadioIfMacOperation.u2MessageType, pRcvBuf);
        CAPWAP_GET_2BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                          RadioIfMacOperation.u2MessageLength, pRcvBuf);
        CAPWAP_GET_1BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                          RadioIfMacOperation.u1RadioId, pRcvBuf);
        CAPWAP_GET_1BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                          RadioIfMacOperation.u1Reserved, pRcvBuf);
        CAPWAP_GET_2BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                          RadioIfMacOperation.u2RTSThreshold, pRcvBuf);
        CAPWAP_GET_1BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                          RadioIfMacOperation.u1ShortRetry, pRcvBuf);
        CAPWAP_GET_1BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                          RadioIfMacOperation.u1LongRetry, pRcvBuf);
        CAPWAP_GET_2BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                          RadioIfMacOperation.u2FragmentThreshold, pRcvBuf);
        CAPWAP_GET_4BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                          RadioIfMacOperation.u4TxMsduLifetime, pRcvBuf);
        CAPWAP_GET_4BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                          RadioIfMacOperation.u4RxMsduLifetime, pRcvBuf);
        RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.u2WtpInternalId
            = pCwMsg->u2IntProfileId;
        RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
            RadioIfMacOperation.isPresent = OSIX_TRUE;
        if (WssIfProcessRadioIfMsg (WSS_RADIOIF_CONFIG_UPDATE_REQ_VALIDATE,
                                    &RadioIfMsgStruct) == OSIX_SUCCESS)
        {
            if (WssIfProcessRadioIfMsg (WSS_RADIOIF_CONFIG_UPDATE_REQ,
                                        &RadioIfMsgStruct) != OSIX_SUCCESS)
            {
                APHDLR_TRC (APHDLR_FAILURE_TRC,
                            "Failed to Set RADIO INFO in Radio IF \r\n");
                return OSIX_FAILURE;
            }
        }
    }
    APHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapSetIeeeMultiDomainCapabilityUpdate          */
/*  Description     : The function sets the Ieee MultiDomainCapability  */
/*                    in DB                                             */
/*  Input(s)        : pRcvBuf - Received Buf                            */
/*                    pCwMsg - Pointer to Capwap Ctrl Msg               */
/*                    u2MsgIndex - Message Index                        */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS | OSIX_FAILURE                       */
/************************************************************************/
INT4
CapwapSetIeeeMultiDomainCapabilityUpdate (UINT1 *pRcvBuf,
                                          tCapwapControlPacket * pCwMsg,
                                          tConfigUpdateRsp * pConfigUpdateRsp,
                                          UINT2 u2MsgIndex)
{
    tRadioIfMsgStruct   RadioIfMsgStruct;
    UINT1              *pu1Buf = NULL;
    UINT2               u2NumRadioInfo = 0;
    UINT1               u1Index = 0;
    UINT4               u4Offset = 0;

    UNUSED_PARAM (pConfigUpdateRsp);

    APHDLR_FN_ENTRY ();
    MEMSET (&RadioIfMsgStruct, 0, sizeof (tRadioIfMsgStruct));

    pu1Buf = pRcvBuf;

    u2NumRadioInfo = pCwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;

    for (u1Index = 1; u1Index <= u2NumRadioInfo; u1Index++)
    {
        u4Offset = pCwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[u1Index - 1];
        pRcvBuf = pu1Buf;
        pRcvBuf += u4Offset;

        CAPWAP_GET_2BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                          RadioIfMultiDomainCap.u2MessageType, pRcvBuf);
        CAPWAP_GET_2BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                          RadioIfMultiDomainCap.u2MessageLength, pRcvBuf);
        CAPWAP_GET_1BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                          RadioIfMultiDomainCap.u1RadioId, pRcvBuf);
        CAPWAP_GET_1BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                          RadioIfMultiDomainCap.u1Reserved, pRcvBuf);
        CAPWAP_GET_2BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                          RadioIfMultiDomainCap.u2FirstChannel, pRcvBuf);
        CAPWAP_GET_2BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                          RadioIfMultiDomainCap.u2NumOfChannels, pRcvBuf);
        CAPWAP_GET_2BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                          RadioIfMultiDomainCap.u2MaxTxPowerLevel, pRcvBuf);

        RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
            RadioIfMultiDomainCap.u1RadioId = u1Index;
        RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.u2WtpInternalId
            = pCwMsg->u2IntProfileId;
        RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
            RadioIfMultiDomainCap.isPresent = OSIX_TRUE;

        if (WssIfProcessRadioIfMsg (WSS_RADIOIF_CONFIG_UPDATE_REQ_VALIDATE,
                                    &RadioIfMsgStruct) == OSIX_SUCCESS)
        {
            if (WssIfProcessRadioIfMsg (WSS_RADIOIF_CONFIG_UPDATE_REQ,
                                        &RadioIfMsgStruct) != OSIX_SUCCESS)
            {
                APHDLR_TRC (APHDLR_FAILURE_TRC,
                            "Failed to Set RADIO INFO in Radio IF \r\n");
                return OSIX_FAILURE;
            }
        }
    }
    APHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapSetIeeeRateSetUpdate                        */
/*  Description     : The function sets the Ieee Rate Set in DB         */
/*  Input(s)        : pRcvBuf - Received Buf                            */
/*                    pCwMsg - Pointer to Capwap Ctrl Msg               */
/*                    u2MsgIndex - Message Index                        */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS | OSIX_FAILURE                       */
/************************************************************************/
INT4
CapwapSetIeeeRateSetUpdate (UINT1 *pRcvBuf,
                            tCapwapControlPacket * pCwMsg,
                            tConfigUpdateRsp * pConfigUpdateRsp,
                            UINT2 u2MsgIndex)
{
    tRadioIfMsgStruct   RadioIfMsgStruct;
    UINT1              *pu1Buf = NULL;
    UINT2               u2NumRadioInfo = 0;
    UINT1               u1Index = 0;
    UINT4               u4Offset = 0;
    UINT2               u2MsgLen = 0;
    UNUSED_PARAM (pConfigUpdateRsp);

    APHDLR_FN_ENTRY ();
    MEMSET (&RadioIfMsgStruct, 0, sizeof (tRadioIfMsgStruct));

    pu1Buf = pRcvBuf;
    u2NumRadioInfo =
        (UINT2) (pCwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance);
    u2MsgLen =
        (UINT2) (sizeof
                 (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                  RadioIfRateSet.au1OperationalRate));

    for (u1Index = 1; u1Index <= u2NumRadioInfo; u1Index++)
    {
        u4Offset = pCwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[u1Index - 1];
        pRcvBuf = pu1Buf;
        pRcvBuf += u4Offset;

        CAPWAP_GET_2BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                          RadioIfRateSet.u2MessageType, pRcvBuf);
        CAPWAP_GET_2BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                          RadioIfRateSet.u2MessageLength, pRcvBuf);
        CAPWAP_GET_1BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                          RadioIfRateSet.u1RadioId, pRcvBuf);
        CAPWAP_GET_NBYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                          RadioIfRateSet.au1OperationalRate, pRcvBuf, u2MsgLen);

        RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.u2WtpInternalId
            = pCwMsg->u2IntProfileId;
        RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
            RadioIfRateSet.isPresent = OSIX_TRUE;

        if (WssIfProcessRadioIfMsg (WSS_RADIOIF_CONFIG_UPDATE_REQ_VALIDATE,
                                    &RadioIfMsgStruct) == OSIX_SUCCESS)
        {
            if (WssIfProcessRadioIfMsg (WSS_RADIOIF_CONFIG_UPDATE_REQ,
                                        &RadioIfMsgStruct) != OSIX_SUCCESS)
            {
                APHDLR_TRC (APHDLR_FAILURE_TRC,
                            "Failed to Set Operational rate in Radio IF \r\n");
                return OSIX_FAILURE;
            }
        }
        else
        {
            APHDLR_TRC (APHDLR_FAILURE_TRC,
                        "Failed to validate the Operational Rate in Radio IF \r\n");
            return OSIX_FAILURE;
        }
    }

    APHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapSetIeeeRadioConfigUpdate                    */
/*  Description     : The function sets the Ieee Radio Config in DB     */
/*  Input(s)        : pRcvBuf - Received Buf                            */
/*                    pCwMsg - Pointer to Capwap Ctrl Msg               */
/*                    u2MsgIndex - Message Index                        */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS | OSIX_FAILURE                       */
/************************************************************************/
INT4
CapwapSetIeeeRadioConfigUpdate (UINT1 *pRcvBuf,
                                tCapwapControlPacket * pCwMsg,
                                tConfigUpdateRsp * pConfigUpdateRsp,
                                UINT2 u2MsgIndex)
{
    tRadioIfMsgStruct   RadioIfMsgStruct;
    UINT1              *pu1Buf = NULL;
    UINT2               u2NumRadioInfo = 0;
    UINT1               u1Index = 0;
    UINT4               u4Offset = 0;
    UINT2               u2MsgLen = 0;

    UNUSED_PARAM (pConfigUpdateRsp);

    APHDLR_FN_ENTRY ();
    MEMSET (&RadioIfMsgStruct, 0, sizeof (tRadioIfMsgStruct));

    pu1Buf = pRcvBuf;

    u2NumRadioInfo =
        (UINT2) (pCwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance);
    u2MsgLen = RADIOIF_COUNTRY_STR_LEN;

    for (u1Index = 1; u1Index <= u2NumRadioInfo; u1Index++)
    {
        u4Offset = pCwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[u1Index - 1];
        pRcvBuf = pu1Buf;
        pRcvBuf += u4Offset;

        CAPWAP_GET_2BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                          RadioIfConfig.u2MessageType, pRcvBuf);
        CAPWAP_GET_2BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                          RadioIfConfig.u2MessageLength, pRcvBuf);
        CAPWAP_GET_1BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                          RadioIfConfig.u1RadioId, pRcvBuf);
        CAPWAP_GET_1BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                          RadioIfConfig.u1ShortPreamble, pRcvBuf);
        CAPWAP_GET_1BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                          RadioIfConfig.u1NoOfBssId, pRcvBuf);
        CAPWAP_GET_1BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                          RadioIfConfig.u1DtimPeriod, pRcvBuf);
        CAPWAP_GET_NBYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                          RadioIfConfig.MacAddr, pRcvBuf, sizeof (tMacAddr));
        CAPWAP_GET_2BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                          RadioIfConfig.u2BeaconPeriod, pRcvBuf);
        CAPWAP_GET_NBYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                          RadioIfConfig.au1CountryString, pRcvBuf, u2MsgLen);

        RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.u2WtpInternalId
            = pCwMsg->u2IntProfileId;
        RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfConfig.
            isPresent = OSIX_TRUE;

        if (WssIfProcessRadioIfMsg (WSS_RADIOIF_CONFIG_UPDATE_REQ_VALIDATE,
                                    &RadioIfMsgStruct) == OSIX_SUCCESS)
        {
            if (WssIfProcessRadioIfMsg (WSS_RADIOIF_CONFIG_UPDATE_REQ,
                                        &RadioIfMsgStruct) != OSIX_SUCCESS)
            {
                APHDLR_TRC (APHDLR_FAILURE_TRC,
                            "Failed to Set the RADIO Config in Radio IF \r\n");
                return OSIX_FAILURE;
            }
        }
        else
        {
            APHDLR_TRC (APHDLR_FAILURE_TRC,
                        "Failed to validate the RADIO Config in Radio IF \r\n");
            return OSIX_FAILURE;
        }
    }

    APHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapSetIeeeRadioInfoUpdate                      */
/*  Description     : The function sets the Ieee Radio Info in DB       */
/*  Input(s)        : pRcvBuf - Received Buf                            */
/*                    pCwMsg - Pointer to Capwap Ctrl Msg               */
/*                    u2MsgIndex - Message Index                        */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS | OSIX_FAILURE                       */
/************************************************************************/
INT4
CapwapSetIeeeRadioInfoUpdate (UINT1 *pRcvBuf,
                              tCapwapControlPacket * pCwMsg,
                              tConfigUpdateRsp * pConfigUpdateRsp,
                              UINT2 u2MsgIndex)
{
    tRadioIfMsgStruct   RadioIfMsgStruct;
    UINT1              *pu1Buf = NULL;
    UINT2               u2NumRadioInfo = 0;
    UINT1               u1Index = 0;
    UINT4               u4Offset = 0;

    UNUSED_PARAM (pConfigUpdateRsp);

    APHDLR_FN_ENTRY ();
    MEMSET (&RadioIfMsgStruct, 0, sizeof (tRadioIfMsgStruct));

    pu1Buf = pRcvBuf;

    u2NumRadioInfo = pCwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;

    for (u1Index = 1; u1Index <= u2NumRadioInfo; u1Index++)
    {
        u4Offset = pCwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[u1Index - 1];
        pRcvBuf = pu1Buf;
        pRcvBuf += u4Offset;

        CAPWAP_GET_2BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                          RadioIfInfo.u2MessageType, pRcvBuf);
        CAPWAP_GET_2BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                          RadioIfInfo.u2MessageLength, pRcvBuf);
        CAPWAP_GET_1BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                          RadioIfInfo.u1RadioId, pRcvBuf);
        CAPWAP_GET_4BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                          RadioIfInfo.u4RadioType, pRcvBuf);
        RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
            RadioIfInfo.u1RadioId = u1Index;
        RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.u2WtpInternalId
            = pCwMsg->u2IntProfileId;
        RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
            RadioIfInfo.isPresent = OSIX_TRUE;

        if (WssIfProcessRadioIfMsg (WSS_RADIOIF_CONFIG_UPDATE_REQ_VALIDATE,
                                    &RadioIfMsgStruct) == OSIX_SUCCESS)
        {
            if (WssIfProcessRadioIfMsg (WSS_RADIOIF_CONFIG_UPDATE_REQ,
                                        &RadioIfMsgStruct) != OSIX_SUCCESS)
            {
                APHDLR_TRC (APHDLR_FAILURE_TRC,
                            "Failed to Set the RADIO INFO in Radio IF \r\n");
                return OSIX_FAILURE;
            }
        }
    }
    APHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapSetIeeeQualityUpdate                        */
/*  Description     : The function sets the Ieee Quality Update         */
/*  Input(s)        : pRcvBuf - Received Buf                            */
/*                    pCwMsg - Pointer to Capwap Ctrl Msg               */
/*                    u2MsgIndex - Message Index                        */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS | OSIX_FAILURE                       */
/************************************************************************/
INT4
CapwapSetIeeeQualityUpdate (UINT1 *pRcvBuf,
                            tCapwapControlPacket * pCwMsg,
                            tConfigUpdateRsp * pConfigUpdateRsp,
                            UINT2 u2MsgIndex)
{
    tRadioIfMsgStruct   RadioIfMsgStruct;
    UINT1              *pu1Buf = NULL;
    UINT2               u2NumRadioQosProfile = 0;
    UINT1               u1Index = 0;
    UINT1               u1QosIndex = 0;
    UINT4               u4Offset = 0;
    UNUSED_PARAM (pConfigUpdateRsp);

    APHDLR_FN_ENTRY ();
    MEMSET (&RadioIfMsgStruct, 0, sizeof (tRadioIfMsgStruct));

    pu1Buf = pRcvBuf;
    u2NumRadioQosProfile = pCwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;

    for (u1Index = 1; u1Index <= u2NumRadioQosProfile; u1Index++)
    {
        u4Offset = pCwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[u1Index - 1];
        pRcvBuf = pu1Buf;
        pRcvBuf += u4Offset;

        CAPWAP_GET_2BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                          RadioIfQos.u2MessageType, pRcvBuf);
        CAPWAP_GET_2BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                          RadioIfQos.u2MessageLength, pRcvBuf);
        CAPWAP_GET_1BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                          RadioIfQos.u1RadioId, pRcvBuf);
        CAPWAP_GET_1BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                          RadioIfQos.u1TaggingPolicy, pRcvBuf);
        for (u1QosIndex = 0;
             u1QosIndex < RADIOIF_MAX_QOS_PROFILE_IND; u1QosIndex++)
        {
            CAPWAP_GET_1BYTE (RadioIfMsgStruct.unRadioIfMsg.
                              RadioIfConfigUpdateReq.RadioIfQos.
                              u1QueueDepth[u1QosIndex], pRcvBuf);
            CAPWAP_GET_2BYTE (RadioIfMsgStruct.unRadioIfMsg.
                              RadioIfConfigUpdateReq.RadioIfQos.
                              u2CwMin[u1QosIndex], pRcvBuf);
            CAPWAP_GET_2BYTE (RadioIfMsgStruct.unRadioIfMsg.
                              RadioIfConfigUpdateReq.RadioIfQos.
                              u2CwMax[u1QosIndex], pRcvBuf);
            CAPWAP_GET_1BYTE (RadioIfMsgStruct.unRadioIfMsg.
                              RadioIfConfigUpdateReq.RadioIfQos.
                              u1Aifsn[u1QosIndex], pRcvBuf);
            CAPWAP_GET_1BYTE (RadioIfMsgStruct.unRadioIfMsg.
                              RadioIfConfigUpdateReq.RadioIfQos.
                              u1Prio[u1QosIndex], pRcvBuf);
            CAPWAP_GET_1BYTE (RadioIfMsgStruct.unRadioIfMsg.
                              RadioIfConfigUpdateReq.RadioIfQos.
                              u1Dscp[u1QosIndex], pRcvBuf);
        }
        RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.u2WtpInternalId
            = pCwMsg->u2IntProfileId;
        RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
            RadioIfQos.isPresent = OSIX_TRUE;

        if (WssIfProcessRadioIfMsg (WSS_RADIOIF_CONFIG_UPDATE_REQ_VALIDATE,
                                    &RadioIfMsgStruct) == OSIX_SUCCESS)
        {
            if (WssIfProcessRadioIfMsg (WSS_RADIOIF_CONFIG_UPDATE_REQ,
                                        &RadioIfMsgStruct) != OSIX_SUCCESS)
            {
                APHDLR_TRC (APHDLR_FAILURE_TRC,
                            "Failed to Set the RADIO QUALITY in  Radio IF \r\n");
                return OSIX_FAILURE;
            }
        }
        else
        {
            APHDLR_TRC (APHDLR_FAILURE_TRC,
                        "Failed to Validate the RADIO QUALITY in Qos \r\n");
            return OSIX_FAILURE;
        }
    }
    APHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapSetIeeeInfoElemUpdate                       */
/*  Description     : The function sets the Ieee Info elem Update in DB */
/*  Input(s)        : pRcvBuf - Received Buf                            */
/*                    pCwMsg - Pointer to Capwap Ctrl Msg               */
/*                    u2MsgIndex - Message Index                        */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS | OSIX_FAILURE                       */
/************************************************************************/
INT4
CapwapSetIeeeInfoElemUpdate (UINT1 *pRcvBuf,
                             tCapwapControlPacket * pCwMsg,
                             tConfigUpdateRsp * pConfigUpdateRsp,
                             UINT2 u2MsgIndex)
{
    tRadioIfMsgStruct   RadioIfMsgStruct;
    UINT1              *pu1Buf = NULL;
    UINT2               u2NumOfRadios = 0;
    UINT1               u1Index = 0;
    UINT4               u4Offset;
#ifdef WPS_WTP_WANTED
    UINT1               u1WpsIndex = 0;
#endif

    UNUSED_PARAM (pConfigUpdateRsp);

    APHDLR_FN_ENTRY ();
    MEMSET (&RadioIfMsgStruct, 0, sizeof (tRadioIfMsgStruct));
    pu1Buf = pRcvBuf;
    u2NumOfRadios = pCwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;

    for (u1Index = 1; u1Index <= u2NumOfRadios; u1Index++)
    {
        u4Offset = pCwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[u1Index - 1];
        pRcvBuf = pu1Buf;
        pRcvBuf += u4Offset;

        CAPWAP_GET_2BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                          RadioIfInfoElem.u2MessageType, pRcvBuf);
        CAPWAP_GET_2BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                          RadioIfInfoElem.u2MessageLength, pRcvBuf);
        CAPWAP_GET_1BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                          RadioIfInfoElem.u1RadioId, pRcvBuf);
        CAPWAP_GET_1BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                          RadioIfInfoElem.u1WlanId, pRcvBuf);
        CAPWAP_GET_1BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                          RadioIfInfoElem.u1BPFlag, pRcvBuf);
        CAPWAP_GET_1BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                          RadioIfInfoElem.u1EleId, pRcvBuf);

        switch (RadioIfMsgStruct.unRadioIfMsg.
                RadioIfConfigUpdateReq.RadioIfInfoElem.u1EleId)
        {
            case DOT11_INFO_ELEM_HTCAPABILITY:
                RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                    RadioIfInfoElem.unInfoElem.HTCapability.u1ElemId =
                    RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                    RadioIfInfoElem.u1EleId;
                CAPWAP_GET_1BYTE (RadioIfMsgStruct.unRadioIfMsg.
                                  RadioIfConfigUpdateReq.RadioIfInfoElem.
                                  unInfoElem.HTCapability.u1ElemLen, pRcvBuf);
                CAPWAP_GET_2BYTE (RadioIfMsgStruct.unRadioIfMsg.
                                  RadioIfConfigUpdateReq.RadioIfInfoElem.
                                  unInfoElem.HTCapability.u2HTCapInfo, pRcvBuf);
                CAPWAP_GET_1BYTE (RadioIfMsgStruct.unRadioIfMsg.
                                  RadioIfConfigUpdateReq.RadioIfInfoElem.
                                  unInfoElem.HTCapability.u1AMPDUParam,
                                  pRcvBuf);
                CAPWAP_GET_NBYTE (RadioIfMsgStruct.unRadioIfMsg.
                                  RadioIfConfigUpdateReq.RadioIfInfoElem.
                                  unInfoElem.HTCapability.au1SuppMCSSet,
                                  pRcvBuf, 16);
                CAPWAP_GET_2BYTE (RadioIfMsgStruct.unRadioIfMsg.
                                  RadioIfConfigUpdateReq.RadioIfInfoElem.
                                  unInfoElem.HTCapability.u2HTExtCap, pRcvBuf);
                CAPWAP_GET_4BYTE (RadioIfMsgStruct.unRadioIfMsg.
                                  RadioIfConfigUpdateReq.RadioIfInfoElem.
                                  unInfoElem.HTCapability.u4TranBeamformCap,
                                  pRcvBuf);
                CAPWAP_GET_1BYTE (RadioIfMsgStruct.unRadioIfMsg.
                                  RadioIfConfigUpdateReq.RadioIfInfoElem.
                                  unInfoElem.HTCapability.u1ASELCap, pRcvBuf);
                RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                    RadioIfInfoElem.isPresent = OSIX_TRUE;
                RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                    RadioIfInfoElem.unInfoElem.HTCapability.isOptional =
                    OSIX_TRUE;
                RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                    u2WtpInternalId = pCwMsg->u2IntProfileId;

                if (WssIfProcessRadioIfMsg
                    (WSS_RADIOIF_CONFIG_UPDATE_REQ_VALIDATE,
                     &RadioIfMsgStruct) == OSIX_SUCCESS)
                {
                    if (WssIfProcessRadioIfMsg (WSS_RADIOIF_CONFIG_UPDATE_REQ,
                                                &RadioIfMsgStruct) !=
                        OSIX_SUCCESS)
                    {
                        APHDLR_TRC (APHDLR_FAILURE_TRC,
                                    "Failed to Set the IEEE Information"
                                    "Element in Radio IF \r\n");
                        return OSIX_FAILURE;
                    }
                }
                break;

            case DOT11_INFO_ELEM_HTOPERATION:
                RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                    RadioIfInfoElem.unInfoElem.HTOperation.u1ElemId =
                    RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                    RadioIfInfoElem.u1EleId;
                CAPWAP_GET_1BYTE (RadioIfMsgStruct.unRadioIfMsg.
                                  RadioIfConfigUpdateReq.RadioIfInfoElem.
                                  unInfoElem.HTOperation.u1ElemLen, pRcvBuf);
                CAPWAP_GET_1BYTE (RadioIfMsgStruct.unRadioIfMsg.
                                  RadioIfConfigUpdateReq.RadioIfInfoElem.
                                  unInfoElem.HTOperation.u1PrimaryCh, pRcvBuf);
                CAPWAP_GET_NBYTE (RadioIfMsgStruct.unRadioIfMsg.
                                  RadioIfConfigUpdateReq.RadioIfInfoElem.
                                  unInfoElem.HTOperation.au1HTOpeInfo, pRcvBuf,
                                  WSSMAC_HTOPE_INFO);
                CAPWAP_GET_NBYTE (RadioIfMsgStruct.unRadioIfMsg.
                                  RadioIfConfigUpdateReq.RadioIfInfoElem.
                                  unInfoElem.HTOperation.au1BasicMCSSet,
                                  pRcvBuf, WSSMAC_BASIC_MCS_SET);
                RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                    RadioIfInfoElem.isPresent = OSIX_TRUE;
                RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                    RadioIfInfoElem.unInfoElem.VHTOperation.isOptional =
                    OSIX_TRUE;
                RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                    RadioIfInfoElem.u1RadioId = u1Index;
                RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                    u2WtpInternalId = pCwMsg->u2IntProfileId;

                if (WssIfProcessRadioIfMsg
                    (WSS_RADIOIF_CONFIG_UPDATE_REQ_VALIDATE,
                     &RadioIfMsgStruct) == OSIX_SUCCESS)
                {
                    if (WssIfProcessRadioIfMsg (WSS_RADIOIF_CONFIG_UPDATE_REQ,
                                                &RadioIfMsgStruct) !=
                        OSIX_SUCCESS)
                    {
                        APHDLR_TRC (APHDLR_FAILURE_TRC,
                                    "Failed to Set the IEEE Information"
                                    "Element in Radio IF \r\n");
                        return OSIX_FAILURE;
                    }
                }
                break;

            case DOT11AC_INFO_ELEM_VHTCAPABILITY:
                RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                    RadioIfInfoElem.unInfoElem.VHTCapability.u1ElemId =
                    RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                    RadioIfInfoElem.u1EleId;
                CAPWAP_GET_1BYTE (RadioIfMsgStruct.unRadioIfMsg.
                                  RadioIfConfigUpdateReq.RadioIfInfoElem.
                                  unInfoElem.VHTCapability.u1ElemLen, pRcvBuf);
                CAPWAP_GET_4BYTE (RadioIfMsgStruct.unRadioIfMsg.
                                  RadioIfConfigUpdateReq.RadioIfInfoElem.
                                  unInfoElem.VHTCapability.u4VhtCapInfo,
                                  pRcvBuf);
                CAPWAP_GET_NBYTE (RadioIfMsgStruct.unRadioIfMsg.
                                  RadioIfConfigUpdateReq.RadioIfInfoElem.
                                  unInfoElem.VHTCapability.au1SuppMCS, pRcvBuf,
                                  DOT11AC_VHT_CAP_MCS_LEN);
                RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                    RadioIfInfoElem.isPresent = OSIX_TRUE;
                RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                    RadioIfInfoElem.unInfoElem.VHTCapability.isOptional =
                    OSIX_TRUE;
                RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                    u2WtpInternalId = pCwMsg->u2IntProfileId;

                if (WssIfProcessRadioIfMsg
                    (WSS_RADIOIF_CONFIG_UPDATE_REQ_VALIDATE,
                     &RadioIfMsgStruct) == OSIX_SUCCESS)
                {
                    if (WssIfProcessRadioIfMsg (WSS_RADIOIF_CONFIG_UPDATE_REQ,
                                                &RadioIfMsgStruct) !=
                        OSIX_SUCCESS)
                    {
                        APHDLR_TRC (APHDLR_FAILURE_TRC,
                                    "Failed to Set the IEEE Information"
                                    "Element in Radio IF \r\n");
                        return OSIX_FAILURE;
                    }
                }

                break;

            case DOT11AC_INFO_ELEM_VHTOPERATION:
                RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                    RadioIfInfoElem.unInfoElem.VHTOperation.u1ElemId =
                    RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                    RadioIfInfoElem.u1EleId;
                CAPWAP_GET_1BYTE (RadioIfMsgStruct.unRadioIfMsg.
                                  RadioIfConfigUpdateReq.RadioIfInfoElem.
                                  unInfoElem.VHTOperation.u1ElemLen, pRcvBuf);
                CAPWAP_GET_NBYTE (RadioIfMsgStruct.unRadioIfMsg.
                                  RadioIfConfigUpdateReq.RadioIfInfoElem.
                                  unInfoElem.VHTOperation.au1VhtOperInfo,
                                  pRcvBuf, DOT11AC_VHT_OPER_INFO_LEN);
                CAPWAP_GET_2BYTE (RadioIfMsgStruct.unRadioIfMsg.
                                  RadioIfConfigUpdateReq.RadioIfInfoElem.
                                  unInfoElem.VHTOperation.u2BasicMCS, pRcvBuf);
                RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                    RadioIfInfoElem.isPresent = OSIX_TRUE;
                RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                    RadioIfInfoElem.unInfoElem.VHTOperation.isOptional =
                    OSIX_TRUE;
                RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                    u2WtpInternalId = pCwMsg->u2IntProfileId;

                if (WssIfProcessRadioIfMsg
                    (WSS_RADIOIF_CONFIG_UPDATE_REQ_VALIDATE,
                     &RadioIfMsgStruct) == OSIX_SUCCESS)
                {
                    if (WssIfProcessRadioIfMsg (WSS_RADIOIF_CONFIG_UPDATE_REQ,
                                                &RadioIfMsgStruct) !=
                        OSIX_SUCCESS)
                    {
                        APHDLR_TRC (APHDLR_FAILURE_TRC,
                                    "Failed to Set the IEEE Information"
                                    "Element in Radio IF \r\n");
                        return OSIX_FAILURE;
                    }
                }

                break;
#ifdef WPS_WTP_WANTED
            case WPS_VENDOR_ELEMENT_ID:
                RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                    RadioIfInfoElem.unInfoElem.WpsIEElements.u1ElemId =
                    RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                    RadioIfInfoElem.u1EleId;
                CAPWAP_GET_1BYTE (RadioIfMsgStruct.unRadioIfMsg.
                                  RadioIfConfigUpdateReq.RadioIfInfoElem.
                                  unInfoElem.WpsIEElements.u1Len, pRcvBuf);
                CAPWAP_GET_1BYTE (RadioIfMsgStruct.unRadioIfMsg.
                                  RadioIfConfigUpdateReq.RadioIfInfoElem.
                                  unInfoElem.WpsIEElements.bWpsEnabled,
                                  pRcvBuf);
                CAPWAP_GET_1BYTE (RadioIfMsgStruct.unRadioIfMsg.
                                  RadioIfConfigUpdateReq.RadioIfInfoElem.
                                  unInfoElem.WpsIEElements.wscState, pRcvBuf);
                CAPWAP_GET_1BYTE (RadioIfMsgStruct.unRadioIfMsg.
                                  RadioIfConfigUpdateReq.RadioIfInfoElem.
                                  unInfoElem.WpsIEElements.bWpsAdditionalIE,
                                  pRcvBuf);
                CAPWAP_GET_1BYTE (RadioIfMsgStruct.unRadioIfMsg.
                                  RadioIfConfigUpdateReq.RadioIfInfoElem.
                                  unInfoElem.WpsIEElements.noOfAuthMac,
                                  pRcvBuf);
                for (u1WpsIndex = 0;
                     u1WpsIndex <
                     RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                     RadioIfInfoElem.unInfoElem.WpsIEElements.noOfAuthMac;
                     u1WpsIndex++)
                {
                    CAPWAP_GET_NBYTE (RadioIfMsgStruct.unRadioIfMsg.
                                      RadioIfConfigUpdateReq.RadioIfInfoElem.
                                      unInfoElem.WpsIEElements.
                                      authorized_macs[u1WpsIndex], pRcvBuf,
                                      WPS_ETH_ALEN);
                }
                CAPWAP_GET_NBYTE (RadioIfMsgStruct.unRadioIfMsg.
                                  RadioIfConfigUpdateReq.RadioIfInfoElem.
                                  unInfoElem.WpsIEElements.uuid_e, pRcvBuf,
                                  WPS_UUID_LEN);
                CAPWAP_GET_2BYTE (RadioIfMsgStruct.unRadioIfMsg.
                                  RadioIfConfigUpdateReq.RadioIfInfoElem.
                                  unInfoElem.WpsIEElements.configMethods,
                                  pRcvBuf);
                RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                    RadioIfInfoElem.isPresent = OSIX_TRUE;
                RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                    RadioIfInfoElem.unInfoElem.WpsIEElements.isOptional =
                    OSIX_TRUE;
                RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                    u2WtpInternalId = pCwMsg->u2IntProfileId;

                if (WssIfProcessRadioIfMsg
                    (WSS_RADIOIF_CONFIG_UPDATE_REQ_VALIDATE,
                     &RadioIfMsgStruct) == OSIX_SUCCESS)
                {
                    if (WssIfProcessRadioIfMsg (WSS_RADIOIF_CONFIG_UPDATE_REQ,
                                                &RadioIfMsgStruct) !=
                        OSIX_SUCCESS)
                    {
                        APHDLR_TRC (APHDLR_FAILURE_TRC,
                                    "Failed to Set the IEEE Information"
                                    "Element in Radio IF \r\n");
                        return OSIX_FAILURE;
                    }
                }
                break;
#endif
            default:
                break;

        }
    }
    APHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapSetIeeeDSCUpdate                            */
/*  Description     : The function sets the Ieee DSC Update in DB       */
/*  Input(s)        : pRcvBuf - Received Buf                            */
/*                    pCwMsg - Pointer to Capwap Ctrl Msg               */
/*                    u2MsgIndex - Message Index                        */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS | OSIX_FAILURE                       */
/************************************************************************/
INT4
CapwapSetIeeeDSCUpdate (UINT1 *pRcvBuf,
                        tCapwapControlPacket * pCwMsg,
                        tConfigUpdateRsp * pConfigUpdateRsp, UINT2 u2MsgIndex)
{
    tRadioIfMsgStruct   RadioIfMsgStruct;
    UINT1              *pu1Buf = NULL;
    UINT2               u2NumOfRadios = 0;
    UINT1               u1Index = 0;
    UINT4               u4Offset = 0;

    UNUSED_PARAM (pConfigUpdateRsp);

    APHDLR_FN_ENTRY ();
    MEMSET (&RadioIfMsgStruct, 0, sizeof (tRadioIfMsgStruct));

    pu1Buf = pRcvBuf;
    u2NumOfRadios = pCwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;

    for (u1Index = 1; u1Index <= u2NumOfRadios; u1Index++)
    {
        u4Offset = pCwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[u1Index - 1];
        pRcvBuf = pu1Buf;
        pRcvBuf += u4Offset;

        CAPWAP_GET_2BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                          RadioIfDSSSPhy.u2MessageType, pRcvBuf);
        CAPWAP_GET_2BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                          RadioIfDSSSPhy.u2MessageLength, pRcvBuf);
        CAPWAP_GET_1BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                          RadioIfDSSSPhy.u1RadioId, pRcvBuf);
        CAPWAP_GET_1BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                          RadioIfDSSSPhy.u1Reserved, pRcvBuf);
        CAPWAP_GET_1BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                          RadioIfDSSSPhy.i1CurrentChannel, pRcvBuf);
        CAPWAP_GET_1BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                          RadioIfDSSSPhy.i1CurrentCCAMode, pRcvBuf);
        CAPWAP_GET_4BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                          RadioIfDSSSPhy.i4EDThreshold, pRcvBuf);
        RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
            RadioIfDSSSPhy.isPresent = OSIX_TRUE;

        RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.u2WtpInternalId
            = pCwMsg->u2IntProfileId;

        if (WssIfProcessRadioIfMsg (WSS_RADIOIF_CONFIG_UPDATE_REQ_VALIDATE,
                                    &RadioIfMsgStruct) == OSIX_SUCCESS)
        {
            if (WssIfProcessRadioIfMsg (WSS_RADIOIF_CONFIG_UPDATE_REQ,
                                        &RadioIfMsgStruct) != OSIX_SUCCESS)
            {
                APHDLR_TRC (APHDLR_FAILURE_TRC,
                            "Failed to Set the IEEE OFDM Control in Radio IF \r\n");
                return OSIX_FAILURE;
            }
        }

    }
    APHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapSetIeeeOFDMControlUpdate                    */
/*  Description     : The function sets the Ieee OFDM Ctrl in DB        */
/*  Input(s)        : pRcvBuf - Received Buf                            */
/*                    pCwMsg - Pointer to Capwap Ctrl Msg               */
/*                    u2MsgIndex - Message Index                        */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS | OSIX_FAILURE                       */
/************************************************************************/
INT4
CapwapSetIeeeOFDMControlUpdate (UINT1 *pRcvBuf,
                                tCapwapControlPacket * pCwMsg,
                                tConfigUpdateRsp * pConfigUpdateRsp,
                                UINT2 u2MsgIndex)
{
    tRadioIfMsgStruct   RadioIfMsgStruct;
    UINT1              *pu1Buf = NULL;
    UINT2               u2NumOfRadios = 0;
    UINT1               u1Index = 0;
    UINT4               u4Offset = 0;
    UNUSED_PARAM (pConfigUpdateRsp);

    APHDLR_FN_ENTRY ();
    MEMSET (&RadioIfMsgStruct, 0, sizeof (tRadioIfMsgStruct));

    pu1Buf = pRcvBuf;
    u2NumOfRadios = pCwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;

    for (u1Index = 1; u1Index <= u2NumOfRadios; u1Index++)
    {
        u4Offset = pCwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[u1Index - 1];
        pRcvBuf = pu1Buf;
        pRcvBuf += u4Offset;

        CAPWAP_GET_2BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                          RadioIfOFDMPhy.u2MessageType, pRcvBuf);
        CAPWAP_GET_2BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                          RadioIfOFDMPhy.u2MessageLength, pRcvBuf);
        CAPWAP_GET_1BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                          RadioIfOFDMPhy.u1RadioId, pRcvBuf);
        CAPWAP_GET_1BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                          RadioIfOFDMPhy.u1Reserved, pRcvBuf);
        CAPWAP_GET_1BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                          RadioIfOFDMPhy.u1CurrentChannel, pRcvBuf);
        CAPWAP_GET_1BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                          RadioIfOFDMPhy.u1SupportedBand, pRcvBuf);
        CAPWAP_GET_4BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                          RadioIfOFDMPhy.u4T1Threshold, pRcvBuf);
        RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.u2WtpInternalId =
            pCwMsg->u2IntProfileId;
        RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
            RadioIfOFDMPhy.isPresent = OSIX_TRUE;
        if (WssIfProcessRadioIfMsg (WSS_RADIOIF_CONFIG_UPDATE_REQ_VALIDATE,
                                    &RadioIfMsgStruct) == OSIX_SUCCESS)
        {
            if (WssIfProcessRadioIfMsg (WSS_RADIOIF_CONFIG_UPDATE_REQ,
                                        &RadioIfMsgStruct) != OSIX_SUCCESS)
            {
                APHDLR_TRC (APHDLR_FAILURE_TRC, "Failed to Set the "
                            "IEEE OFDM Control in Radio IF \r\n");
                return OSIX_FAILURE;
            }
        }
    }
    APHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapSetIeeeTxPowerUpdate                        */
/*  Description     : The function sets the Ieee Tx Power in DB         */
/*  Input(s)        : pRcvBuf - Received Buf                            */
/*                    pCwMsg - Pointer to Capwap Ctrl Msg               */
/*                    u2MsgIndex - Message Index                        */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS | OSIX_FAILURE                       */
/************************************************************************/
INT4
CapwapSetIeeeTxPowerUpdate (UINT1 *pRcvBuf,
                            tCapwapControlPacket * pCwMsg,
                            tConfigUpdateRsp * pConfigUpdateRsp,
                            UINT2 u2MsgIndex)
{
    tRadioIfMsgStruct   RadioIfMsgStruct;
    UINT1              *pu1Buf = NULL;
    UINT2               u2NumOfRadios = 0;
    UINT4               u4Offset = 0;

    UNUSED_PARAM (pConfigUpdateRsp);

    APHDLR_FN_ENTRY ();
    MEMSET (&RadioIfMsgStruct, 0, sizeof (tRadioIfMsgStruct));

    pu1Buf = pRcvBuf;
    u2NumOfRadios = pCwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;

    u4Offset = pCwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[0];
    pRcvBuf = pu1Buf;
    pRcvBuf += u4Offset;

    CAPWAP_GET_2BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                      RadioIfTxPower.u2MessageType, pRcvBuf);
    CAPWAP_GET_2BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                      RadioIfTxPower.u2MessageLength, pRcvBuf);
    CAPWAP_GET_1BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                      RadioIfTxPower.u1RadioId, pRcvBuf);
    CAPWAP_GET_1BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                      RadioIfTxPower.u1Reserved, pRcvBuf);
    CAPWAP_GET_2BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                      RadioIfTxPower.u2TxPowerLevel, pRcvBuf);
    RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.u2WtpInternalId
        = pCwMsg->u2IntProfileId;
    RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
        RadioIfTxPower.isPresent = OSIX_TRUE;

    if (WssIfProcessRadioIfMsg (WSS_RADIOIF_CONFIG_UPDATE_REQ_VALIDATE,
                                &RadioIfMsgStruct) == OSIX_SUCCESS)
    {
        if (WssIfProcessRadioIfMsg (WSS_RADIOIF_CONFIG_UPDATE_REQ,
                                    &RadioIfMsgStruct) != OSIX_SUCCESS)
        {
            APHDLR_TRC (APHDLR_FAILURE_TRC,
                        "Failed to Set the IEEE Tx Power in Radio IF \r\n");
            return OSIX_FAILURE;
        }
    }
    APHDLR_FN_EXIT ();
    UNUSED_PARAM (u2NumOfRadios);
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapProcessConfigUpdateResp                     */
/*  Description     : The function processes the received CAPWAP        */
/*                    Config Update response.                           */
/*  Input(s)        : ConfigUpdateRespMsg- ConfigUpdate response packet */
/*                                           received                   */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
CapwapProcessConfigUpdateResp (tSegment * pBuf,
                               tCapwapControlPacket * pConfigUpdateRespMsg,
                               tRemoteSessionManager * pSessEntry)
{

    UINT1              *pRcvBuf = NULL;
    INT4                i4Status = OSIX_SUCCESS;
    UINT2               u2PacketLen = 0;

    APHDLR_FN_ENTRY ();

    if (pSessEntry == NULL || (pSessEntry->eCurrentState != CAPWAP_RUN))
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC,
                    "Session Entry is NULL, Return Failure\r\n");
        return OSIX_FAILURE;
    }

    u2PacketLen = (UINT2) CAPWAP_GET_BUF_LEN (pBuf);
    pRcvBuf = CRU_BUF_GetDataPtr (pBuf->pFirstValidDataDesc);

    /* Validate the Clear Config response message */
    if ((CapValidateConfUpdateRespMsgElem (pRcvBuf, pConfigUpdateRespMsg)
         == OSIX_SUCCESS))
    {
        return OSIX_SUCCESS;
    }

    APHDLR_FN_EXIT ();

    UNUSED_PARAM (u2PacketLen);
    return (i4Status);
}

/************************************************************************/
/*  Function Name   : CapValidateConfUpdateRespMsgElem                  */
/*  Description     : The function validates the functional             */
/*                    characteristics of the received CAPWAP            */
/*                    Config Update request agains the configured       */
/*                    WTP profile                                       */
/*  Input(s)        : ConfigUpdateRespMsg - parsed CAPWAP Config Update */
/*                                            response                  */
/*                    pRcvBuf - Received Config Update packet           */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
CapValidateConfUpdateRespMsgElem (UINT1 *pRcvBuf,
                                  tCapwapControlPacket * ConfigUpdateRespMsg)
{
    tResCode            resultCode;
    tVendorSpecPayload  vendSpec;
    tRadioIfOperStatus  radioOper;

    APHDLR_FN_ENTRY ();
    MEMSET (&resultCode, 0, sizeof (tResCode));
    MEMSET (&vendSpec, 0, sizeof (tVendorSpecPayload));
    MEMSET (&radioOper, 0, sizeof (tRadioIfOperStatus));

#if APHDLR_UNUSED_CODE
    /* Validate the mandatory message elements */
    if ((CapwapValidateResultCode (pRcvBuf, ConfigUpdateRespMsg, &resultCode,
                                   RESULT_CODE_CONF_UPDATE_RESP_INDEX) ==
         OSIX_SUCCESS))
    {
        return OSIX_SUCCESS;
    }

    /* Validate the Optional message elements */
    if ((CapwapValidateVendSpecPld (pRcvBuf, ConfigUpdateRespMsg, &vendSpec,
                                    VENDOR_SPECIFIC_PAYLOAD_CONF_UPDATE_RESP_INDEX)
         == OSIX_SUCCESS))

#endif
        if (CapwapValidateRadioOperState (pRcvBuf, ConfigUpdateRespMsg,
                                          &radioOper,
                                          RADIO_OPER_STATE_CONF_UPDATE_RESP_INDEX)
            == OSIX_SUCCESS)
        {
            return OSIX_SUCCESS;
        }

    APHDLR_FN_EXIT ();
    return OSIX_FAILURE;
}

/************************************************************************/
/*  Function Name   : CapwapGetIeeeAssignedWtpBssID                     */
/*  Description     : The function gets the Ieee Assigned BssId from DB */
/*  Input(s)        : pWssWlanRsp - Pointer to Wlan Conf Rsp            */
/*                    u1RadioId - Radio ID                              */
/*                    u1WlanId - Wlan ID                                */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS | OSIX_FAILURE                       */
/************************************************************************/
INT4
CapwapGetIeeeAssignedWtpBssID (tWssWlanRsp * pWssWlanRsp, UINT1 u1RadioId,
                               UINT1 u1WlanId)
{
    tWssWlanDB          WssWlanDB;

    APHDLR_FN_ENTRY ();
    MEMSET (&WssWlanDB, 0, sizeof (tWssWlanDB));

    WssWlanDB.WssWlanAttributeDB.u1RadioId = u1RadioId;
    WssWlanDB.WssWlanAttributeDB.u1WlanId = u1WlanId;
    WssWlanDB.WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
    WssWlanDB.WssWlanIsPresentDB.bBssId = OSIX_TRUE;

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_INTERFACE_ENTRY, &WssWlanDB)
        == OSIX_SUCCESS)
    {
        /* Get BSS Id from WSS WLAN DB */
        pWssWlanRsp->u2MessageType = IEEE_ASSIGNED_WTPBSSID;
        pWssWlanRsp->u2Length = IEEE_ASSIGNED_WTPBSSID_LEN;
        pWssWlanRsp->u1WlanId = u1WlanId;
        pWssWlanRsp->u1RadioId = u1RadioId;
        MEMCPY (pWssWlanRsp->BssId, WssWlanDB.WssWlanAttributeDB.BssId,
                MAC_ADDR_LEN);
        pWssWlanRsp->u1IsPresent = OSIX_TRUE;
        return OSIX_SUCCESS;
    }
    else
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC,
                    "Failed to Get BSSID from WSS WLAB DB \r\n");
        return OSIX_FAILURE;
    }

    APHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapValidateWlanConfReqMsgElem                     */
/*  Description     : The function validates the Wlan Conf Request.     */
/*  Input(s)        : pRcvdBuf - Buffer pointer                         */
/*                    pWlanConfReq - Pointer to Wlan Conf Req struct    */
/*                    pWssWlanConfigRsp - Pointer to Wlan Conf Rsp      */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS | OSIX_FAILURE                       */
/************************************************************************/
INT4
CapValidateWlanConfReqMsgElem (UINT1 *pRcvdBuf,
                               tCapwapControlPacket * pWlanConfReq,
                               tWssWlanConfigRsp * pWssWlanConfigRsp)
{
    UINT2               u2MsgType = 0;
    UINT4               u4Offset = 0;
    tWssWlanMsgStruct   WssWlanMsgStruct;
    UINT1               u1RadioId = 1, u1WlanId = 1;
    UINT1               u1IPRadioId = 1, u1IPWlanId = 1;
    UINT2               u2NumOptionalMsg = 0;
    UINT1               u1Index = 0;
    UINT1               u1Count = 0;
    UINT1               u1RsnIELength = 0;
    UINT1              *pIeeeRadioRcvdBuf = NULL;
    UINT1              *pVendRcvdBuf = NULL;
    UINT1              *pu1StartPtr = NULL;
#ifdef WPS_WTP_WANTED
    UINT1               u1WpsIndex = 0;
#endif

    APHDLR_FN_ENTRY ();
    MEMSET (&WssWlanMsgStruct, 0, sizeof (tWssWlanMsgStruct));

    /* Validate IEEE radio optional elements */
    /* Get the message element type */
    /* u2MsgType = pWlanConfReq->capwapMsgElm[0].u2MsgType; */
    /* u4Offset = pWlanConfReq->capwapMsgElm[0].pu2Offset[0]; */
    pIeeeRadioRcvdBuf = pRcvdBuf;
    pVendRcvdBuf = pRcvdBuf;
    u2NumOptionalMsg = (UINT2) (pWlanConfReq->numOptionalElements +
                                pWlanConfReq->numMandatoryElements);
    for (u1Index = 0; u1Index < u2NumOptionalMsg; u1Index++)
    {
        u2MsgType = pWlanConfReq->capwapMsgElm[u1Index].u2MsgType;
        u4Offset = pWlanConfReq->capwapMsgElm[u1Index].pu2Offset[0];
        switch (u2MsgType)
        {
            case IEEE_ADD_WLAN:
                pRcvdBuf += u4Offset;
                pu1StartPtr = pRcvdBuf;
                CAPWAP_GET_2BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                                  u2MessageType, pRcvdBuf);
                CAPWAP_GET_2BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                                  u2MessageLength, pRcvdBuf);
                CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                                  u1RadioId, pRcvdBuf);
                CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                                  u1WlanId, pRcvdBuf);
                u1WlanId =
                    WssWlanMsgStruct.unWssWlanMsg.WssWlanConfigReq.
                    unWlanConfReq.WssWlanAddReq.u1WlanId;
                u1IPWlanId =
                    WssWlanMsgStruct.unWssWlanMsg.WssWlanConfigReq.
                    unWlanConfReq.WssWlanAddReq.u1WlanId;
                u1IPRadioId =
                    WssWlanMsgStruct.unWssWlanMsg.WssWlanConfigReq.
                    unWlanConfReq.WssWlanAddReq.u1RadioId;
                CAPWAP_GET_2BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                                  u2Capability, pRcvdBuf);
                CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                                  u1KeyIndex, pRcvdBuf);
                CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                                  u1KeyStatus, pRcvdBuf);
                CAPWAP_GET_2BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                                  u2KeyLength, pRcvdBuf);
                CAPWAP_GET_NBYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                                  au1Key, pRcvdBuf,
                                  WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                                  u2KeyLength);
                CAPWAP_GET_NBYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                                  au1GroupTsc, pRcvdBuf,
                                  WSSSTA_PAIRWISE_SC_LEN);
                CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                                  u1Qos, pRcvdBuf);
                CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                                  u1AuthType, pRcvdBuf);
                CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                                  u1MacMode, pRcvdBuf);
                CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                                  u1TunnelMode, pRcvdBuf);
                CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                                  u1SupressSsid, pRcvdBuf);
                CAPWAP_GET_NBYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                                  au1Ssid, pRcvdBuf, WSSWLAN_SSID_NAME_LEN);

                CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                                  WssWlanPowerConstraint.u1ElementId, pRcvdBuf);
                CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                                  WssWlanPowerConstraint.u1Length, pRcvdBuf);
                CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                                  WssWlanPowerConstraint.u1LocalPowerConstraint,
                                  pRcvdBuf);

                CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                                  WssWlanQosCapab.u1ElementId, pRcvdBuf);
                CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                                  WssWlanQosCapab.u1Length, pRcvdBuf);
                CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                                  WssWlanQosCapab.u1QosInfo, pRcvdBuf);

                CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                                  WssWlanEdcaParam.u1ElementId, pRcvdBuf);
                CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                                  WssWlanEdcaParam.u1Length, pRcvdBuf);
                CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                                  WssWlanEdcaParam.u1QosInfo, pRcvdBuf);
                CAPWAP_GET_2BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                                  WssWlanEdcaParam.AC_BE_ParameterRecord.
                                  u2TxOpLimit, pRcvdBuf);
                CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                                  WssWlanEdcaParam.AC_BE_ParameterRecord.
                                  u1ACI_AIFSN, pRcvdBuf);
                CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                                  WssWlanEdcaParam.AC_BE_ParameterRecord.
                                  u1ECWmin_ECWmax, pRcvdBuf);
                CAPWAP_GET_2BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                                  WssWlanEdcaParam.AC_BK_ParameterRecord.
                                  u2TxOpLimit, pRcvdBuf);
                CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                                  WssWlanEdcaParam.AC_BK_ParameterRecord.
                                  u1ACI_AIFSN, pRcvdBuf);
                CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                                  WssWlanEdcaParam.AC_BK_ParameterRecord.
                                  u1ECWmin_ECWmax, pRcvdBuf);
                CAPWAP_GET_2BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                                  WssWlanEdcaParam.AC_VI_ParameterRecord.
                                  u2TxOpLimit, pRcvdBuf);
                CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                                  WssWlanEdcaParam.AC_VI_ParameterRecord.
                                  u1ACI_AIFSN, pRcvdBuf);
                CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                                  WssWlanEdcaParam.AC_VI_ParameterRecord.
                                  u1ECWmin_ECWmax, pRcvdBuf);
                CAPWAP_GET_2BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                                  WssWlanEdcaParam.AC_VO_ParameterRecord.
                                  u2TxOpLimit, pRcvdBuf);
                CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                                  WssWlanEdcaParam.AC_VO_ParameterRecord.
                                  u1ACI_AIFSN, pRcvdBuf);
                CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                                  WssWlanEdcaParam.AC_VO_ParameterRecord.
                                  u1ECWmin_ECWmax, pRcvdBuf);

                u1RsnIELength = 0;
                if ((WssWlanMsgStruct.unWssWlanMsg.WssWlanConfigReq.
                     unWlanConfReq.WssWlanAddReq.u1KeyStatus == 0)
                    && ((pRcvdBuf - pu1StartPtr) <
                        WssWlanMsgStruct.unWssWlanMsg.WssWlanConfigReq.
                        unWlanConfReq.WssWlanAddReq.u2MessageLength))
                {

                    CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                      WssWlanConfigReq.unWlanConfReq.
                                      WssWlanAddReq.RsnaIEElements.u1ElemId,
                                      pRcvdBuf);
                    u1RsnIELength =
                        (UINT1) (u1RsnIELength + RSN_IE_ELEMENT_LENGTH);

                    CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                      WssWlanConfigReq.unWlanConfReq.
                                      WssWlanAddReq.RsnaIEElements.u1Len,
                                      pRcvdBuf);
                    u1RsnIELength =
                        (UINT1) (u1RsnIELength + RSN_IE_LENGTH_FIELD);

                    CAPWAP_GET_2BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                      WssWlanConfigReq.unWlanConfReq.
                                      WssWlanAddReq.RsnaIEElements.u2Ver,
                                      pRcvdBuf);
                    u1RsnIELength =
                        (UINT1) (u1RsnIELength + RSN_IE_VERSION_LENGTH);

                    CAPWAP_GET_NBYTE (WssWlanMsgStruct.unWssWlanMsg.
                                      WssWlanConfigReq.unWlanConfReq.
                                      WssWlanAddReq.RsnaIEElements.
                                      au1GroupCipherSuite, pRcvdBuf,
                                      RSNA_CIPHER_SUITE_LEN);
                    u1RsnIELength =
                        (UINT1) (u1RsnIELength + RSNA_CIPHER_SUITE_LEN);

                    CAPWAP_GET_2BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                      WssWlanConfigReq.unWlanConfReq.
                                      WssWlanAddReq.RsnaIEElements.
                                      u2PairwiseCipherSuiteCount, pRcvdBuf);
                    u1RsnIELength =
                        (UINT1) (u1RsnIELength + RSNA_PAIRWISE_CIPHER_COUNT);

                    for (u1Count = 0;
                         u1Count <
                         WssWlanMsgStruct.unWssWlanMsg.WssWlanConfigReq.
                         unWlanConfReq.WssWlanAddReq.RsnaIEElements.
                         u2PairwiseCipherSuiteCount; u1Count++)
                    {
                        CAPWAP_GET_NBYTE (WssWlanMsgStruct.unWssWlanMsg.
                                          WssWlanConfigReq.unWlanConfReq.
                                          WssWlanAddReq.RsnaIEElements.
                                          aRsnaPwCipherDB[u1Count].
                                          au1PairwiseCipher, pRcvdBuf,
                                          RSNA_CIPHER_SUITE_LEN);
                        u1RsnIELength =
                            (UINT1) (u1RsnIELength + RSNA_CIPHER_SUITE_LEN);
                    }

                    CAPWAP_GET_2BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                      WssWlanConfigReq.unWlanConfReq.
                                      WssWlanAddReq.RsnaIEElements.
                                      u2AKMSuiteCount, pRcvdBuf);
                    u1RsnIELength =
                        (UINT1) (u1RsnIELength + RSNA_AKM_SUITE_COUNT);

                    for (u1Count = 0;
                         u1Count <
                         WssWlanMsgStruct.unWssWlanMsg.WssWlanConfigReq.
                         unWlanConfReq.WssWlanAddReq.RsnaIEElements.
                         u2AKMSuiteCount; u1Count++)
                    {
                        CAPWAP_GET_NBYTE (WssWlanMsgStruct.unWssWlanMsg.
                                          WssWlanConfigReq.unWlanConfReq.
                                          WssWlanAddReq.RsnaIEElements.
                                          aRsnaAkmDB[u1Count].au1AKMSuite,
                                          pRcvdBuf, RSNA_AKM_SELECTOR_LEN);
                        u1RsnIELength =
                            (UINT1) (u1RsnIELength + RSNA_AKM_SELECTOR_LEN);
                    }

                    CAPWAP_GET_2BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                      WssWlanConfigReq.unWlanConfReq.
                                      WssWlanAddReq.RsnaIEElements.u2RsnCapab,
                                      pRcvdBuf);

#ifdef PMF_WANTED
                    WssWlanMsgStruct.unWssWlanMsg.WssWlanConfigReq.
                        unWlanConfReq.WssWlanAddReq.RsnaIEElements.u2RsnCapab =
                        OSIX_HTONS (WssWlanMsgStruct.unWssWlanMsg.
                                    WssWlanConfigReq.unWlanConfReq.
                                    WssWlanAddReq.RsnaIEElements.u2RsnCapab);
#endif

                    u1RsnIELength =
                        (UINT1) (u1RsnIELength + RSN_IE_CAPAB_LENGTH);

                    if (u1RsnIELength !=
                        WssWlanMsgStruct.unWssWlanMsg.WssWlanConfigReq.
                        unWlanConfReq.WssWlanAddReq.RsnaIEElements.u1Len)
                    {
                        CAPWAP_GET_2BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                          WssWlanConfigReq.unWlanConfReq.
                                          WssWlanAddReq.RsnaIEElements.
                                          u2PmkidCount, pRcvdBuf);

                        CAPWAP_GET_NBYTE (WssWlanMsgStruct.unWssWlanMsg.
                                          WssWlanConfigReq.unWlanConfReq.
                                          WssWlanAddReq.RsnaIEElements.
                                          aPmkidList, pRcvdBuf, RSNA_PMKID_LEN);
                    }
                }
                WssWlanMsgStruct.unWssWlanMsg.WssWlanConfigReq.u1WlanOption
                    = WSSWLAN_ADD_REQ;
                u1RadioId =
                    WssWlanMsgStruct.unWssWlanMsg.WssWlanConfigReq.
                    unWlanConfReq.WssWlanAddReq.u1RadioId;
#ifdef DEBUG_WANTED
                if (WssIfProcessWssWlanMsg (WSS_WLAN_TEST_CONFIG_REQ,
                                            &WssWlanMsgStruct) == OSIX_SUCCESS)
#endif
                {
                    if (WssIfProcessWssWlanMsg (WSS_WLAN_CONFIG_REQ,
                                                &WssWlanMsgStruct) ==
                        OSIX_FAILURE)
                    {
                        APHDLR_TRC (APHDLR_FAILURE_TRC, "Failed to Set the "
                                    "Received WLAN Configuration Request \r\n");
                        /*return OSIX_FAILURE; */
                    }
                    /* Fill the BSS ID for WLAN configuration Response */
                    if (CapwapGetIeeeAssignedWtpBssID
                        (&pWssWlanConfigRsp->WssWlanConfRsp.WssWlanRsp,
                         u1RadioId, u1WlanId) == OSIX_FAILURE)
                    {
                        APHDLR_TRC (APHDLR_FAILURE_TRC,
                                    "Failed to Get IEEE Assgined WTP BSSID \r\n");
                        return OSIX_FAILURE;
                    }
                }
                break;
            case IEEE_DELETE_WLAN:
                pRcvdBuf += u4Offset;
                CAPWAP_GET_2BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.
                                  WssWlanDeleteReq.u2MessageType, pRcvdBuf);
                CAPWAP_GET_2BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.
                                  WssWlanDeleteReq.u2MessageLength, pRcvdBuf);
                CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.
                                  WssWlanDeleteReq.u1RadioId, pRcvdBuf);
                CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.
                                  WssWlanDeleteReq.u1WlanId, pRcvdBuf);
                WssWlanMsgStruct.unWssWlanMsg.WssWlanConfigReq.u1WlanOption
                    = WSSWLAN_DEL_REQ;
                if (WssIfProcessWssWlanMsg
                    (WSS_WLAN_TEST_CONFIG_REQ,
                     &WssWlanMsgStruct) == OSIX_SUCCESS)
                {
                    if (WssIfProcessWssWlanMsg (WSS_WLAN_CONFIG_REQ,
                                                &WssWlanMsgStruct) ==
                        OSIX_FAILURE)
                    {
                        APHDLR_TRC (APHDLR_FAILURE_TRC,
                                    "Failed to Set Received WLAN Conf Req \r\n");
                        return OSIX_FAILURE;
                    }
                }
                u1IPWlanId =
                    WssWlanMsgStruct.unWssWlanMsg.WssWlanConfigReq.
                    unWlanConfReq.WssWlanDeleteReq.u1WlanId;
                u1IPRadioId =
                    WssWlanMsgStruct.unWssWlanMsg.WssWlanConfigReq.
                    unWlanConfReq.WssWlanDeleteReq.u1RadioId;

                break;
            case IEEE_UPDATE_WLAN:
                pRcvdBuf += u4Offset;
                pu1StartPtr = pRcvdBuf;
                CAPWAP_GET_2BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.
                                  WssWlanUpdateReq.u2MessageType, pRcvdBuf);
                CAPWAP_GET_2BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.
                                  WssWlanUpdateReq.u2MessageLength, pRcvdBuf);
                CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.
                                  WssWlanUpdateReq.u1RadioId, pRcvdBuf);
                CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.
                                  WssWlanUpdateReq.u1WlanId, pRcvdBuf);
                CAPWAP_GET_2BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.
                                  WssWlanUpdateReq.u2Capability, pRcvdBuf);
                CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.
                                  WssWlanUpdateReq.u1KeyIndex, pRcvdBuf);
                CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.
                                  WssWlanUpdateReq.u1KeyStatus, pRcvdBuf);
                CAPWAP_GET_2BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.
                                  WssWlanUpdateReq.u2KeyLength, pRcvdBuf);
                CAPWAP_GET_NBYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.
                                  WssWlanUpdateReq.au1Key, pRcvdBuf,
                                  WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.
                                  WssWlanUpdateReq.u2KeyLength);
                CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.
                                  WssWlanUpdateReq.u1Qos, pRcvdBuf);
                CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.
                                  WssWlanUpdateReq.WssWlanPowerConstraint.
                                  u1ElementId, pRcvdBuf);
                CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.
                                  WssWlanUpdateReq.WssWlanPowerConstraint.
                                  u1Length, pRcvdBuf);
                CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.
                                  WssWlanUpdateReq.WssWlanPowerConstraint.
                                  u1LocalPowerConstraint, pRcvdBuf);

                CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.
                                  WssWlanUpdateReq.WssWlanQosCapab.u1ElementId,
                                  pRcvdBuf);
                CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.
                                  WssWlanUpdateReq.WssWlanQosCapab.u1Length,
                                  pRcvdBuf);
                CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.
                                  WssWlanUpdateReq.WssWlanQosCapab.u1QosInfo,
                                  pRcvdBuf);

                CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.
                                  WssWlanUpdateReq.WssWlanEdcaParam.u1ElementId,
                                  pRcvdBuf);
                CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.
                                  WssWlanUpdateReq.WssWlanEdcaParam.u1Length,
                                  pRcvdBuf);
                CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.
                                  WssWlanUpdateReq.WssWlanEdcaParam.u1QosInfo,
                                  pRcvdBuf);
                CAPWAP_GET_2BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.
                                  WssWlanUpdateReq.WssWlanEdcaParam.
                                  AC_BE_ParameterRecord.u2TxOpLimit, pRcvdBuf);
                CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.
                                  WssWlanUpdateReq.WssWlanEdcaParam.
                                  AC_BE_ParameterRecord.u1ACI_AIFSN, pRcvdBuf);
                CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.
                                  WssWlanUpdateReq.WssWlanEdcaParam.
                                  AC_BE_ParameterRecord.u1ECWmin_ECWmax,
                                  pRcvdBuf);
                CAPWAP_GET_2BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.
                                  WssWlanUpdateReq.WssWlanEdcaParam.
                                  AC_BK_ParameterRecord.u2TxOpLimit, pRcvdBuf);
                CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.
                                  WssWlanUpdateReq.WssWlanEdcaParam.
                                  AC_BK_ParameterRecord.u1ACI_AIFSN, pRcvdBuf);
                CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.
                                  WssWlanUpdateReq.WssWlanEdcaParam.
                                  AC_BK_ParameterRecord.u1ECWmin_ECWmax,
                                  pRcvdBuf);
                CAPWAP_GET_2BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.
                                  WssWlanUpdateReq.WssWlanEdcaParam.
                                  AC_VI_ParameterRecord.u2TxOpLimit, pRcvdBuf);
                CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.
                                  WssWlanUpdateReq.WssWlanEdcaParam.
                                  AC_VI_ParameterRecord.u1ACI_AIFSN, pRcvdBuf);
                CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.
                                  WssWlanUpdateReq.WssWlanEdcaParam.
                                  AC_VI_ParameterRecord.u1ECWmin_ECWmax,
                                  pRcvdBuf);
                CAPWAP_GET_2BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.
                                  WssWlanUpdateReq.WssWlanEdcaParam.
                                  AC_VO_ParameterRecord.u2TxOpLimit, pRcvdBuf);
                CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.
                                  WssWlanUpdateReq.WssWlanEdcaParam.
                                  AC_VO_ParameterRecord.u1ACI_AIFSN, pRcvdBuf);
                CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.
                                  WssWlanUpdateReq.WssWlanEdcaParam.
                                  AC_VO_ParameterRecord.u1ECWmin_ECWmax,
                                  pRcvdBuf);
                u1IPWlanId =
                    WssWlanMsgStruct.unWssWlanMsg.WssWlanConfigReq.
                    unWlanConfReq.WssWlanUpdateReq.u1WlanId;
                u1IPRadioId =
                    WssWlanMsgStruct.unWssWlanMsg.WssWlanConfigReq.
                    unWlanConfReq.WssWlanUpdateReq.u1RadioId;

                u1RsnIELength = 0;
                if ((WssWlanMsgStruct.unWssWlanMsg.WssWlanConfigReq.
                     unWlanConfReq.WssWlanUpdateReq.u1KeyStatus == 0)
                    && ((pRcvdBuf - pu1StartPtr) <
                        WssWlanMsgStruct.unWssWlanMsg.WssWlanConfigReq.
                        unWlanConfReq.WssWlanUpdateReq.u2MessageLength))
                {
                    CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                      WssWlanConfigReq.unWlanConfReq.
                                      WssWlanUpdateReq.RsnaIEElements.u1ElemId,
                                      pRcvdBuf);
                    u1RsnIELength =
                        (UINT1) (u1RsnIELength + RSN_IE_ELEMENT_LENGTH);

                    CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                      WssWlanConfigReq.unWlanConfReq.
                                      WssWlanUpdateReq.RsnaIEElements.u1Len,
                                      pRcvdBuf);
                    u1RsnIELength =
                        (UINT1) (u1RsnIELength + RSN_IE_LENGTH_FIELD);

                    CAPWAP_GET_2BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                      WssWlanConfigReq.unWlanConfReq.
                                      WssWlanUpdateReq.RsnaIEElements.u2Ver,
                                      pRcvdBuf);
                    u1RsnIELength =
                        (UINT1) (u1RsnIELength + RSN_IE_VERSION_LENGTH);

                    CAPWAP_GET_NBYTE (WssWlanMsgStruct.unWssWlanMsg.
                                      WssWlanConfigReq.unWlanConfReq.
                                      WssWlanUpdateReq.RsnaIEElements.
                                      au1GroupCipherSuite, pRcvdBuf, 4);
                    u1RsnIELength =
                        (UINT1) (u1RsnIELength + RSNA_CIPHER_SUITE_LEN);

                    CAPWAP_GET_2BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                      WssWlanConfigReq.unWlanConfReq.
                                      WssWlanUpdateReq.RsnaIEElements.
                                      u2PairwiseCipherSuiteCount, pRcvdBuf);
                    u1RsnIELength =
                        (UINT1) (u1RsnIELength + RSNA_PAIRWISE_CIPHER_COUNT);

                    for (u1Count = 0;
                         u1Count <
                         WssWlanMsgStruct.unWssWlanMsg.WssWlanConfigReq.
                         unWlanConfReq.WssWlanUpdateReq.RsnaIEElements.
                         u2PairwiseCipherSuiteCount; u1Count++)
                    {
                        CAPWAP_GET_NBYTE (WssWlanMsgStruct.unWssWlanMsg.
                                          WssWlanConfigReq.unWlanConfReq.
                                          WssWlanUpdateReq.RsnaIEElements.
                                          aRsnaPwCipherDB[u1Count].
                                          au1PairwiseCipher, pRcvdBuf,
                                          RSNA_CIPHER_SUITE_LEN);
                        u1RsnIELength =
                            (UINT1) (u1RsnIELength + RSNA_CIPHER_SUITE_LEN);
                    }

                    CAPWAP_GET_2BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                      WssWlanConfigReq.unWlanConfReq.
                                      WssWlanUpdateReq.RsnaIEElements.
                                      u2AKMSuiteCount, pRcvdBuf);
                    u1RsnIELength =
                        (UINT1) (u1RsnIELength + RSNA_AKM_SUITE_COUNT);

                    for (u1Count = 0;
                         u1Count <
                         WssWlanMsgStruct.unWssWlanMsg.WssWlanConfigReq.
                         unWlanConfReq.WssWlanUpdateReq.RsnaIEElements.
                         u2AKMSuiteCount; u1Count++)
                    {
                        CAPWAP_GET_NBYTE (WssWlanMsgStruct.unWssWlanMsg.
                                          WssWlanConfigReq.unWlanConfReq.
                                          WssWlanUpdateReq.RsnaIEElements.
                                          aRsnaAkmDB[u1Count].au1AKMSuite,
                                          pRcvdBuf, RSNA_AKM_SELECTOR_LEN);
                        u1RsnIELength =
                            (UINT1) (u1RsnIELength + RSNA_AKM_SELECTOR_LEN);
                    }
                    CAPWAP_GET_2BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                      WssWlanConfigReq.unWlanConfReq.
                                      WssWlanUpdateReq.RsnaIEElements.
                                      u2RsnCapab, pRcvdBuf);
                    u1RsnIELength =
                        (UINT1) (u1RsnIELength + RSN_IE_CAPAB_LENGTH);

                    if (u1RsnIELength !=
                        WssWlanMsgStruct.unWssWlanMsg.WssWlanConfigReq.
                        unWlanConfReq.WssWlanAddReq.RsnaIEElements.u1Len)
                    {
                        CAPWAP_GET_2BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                          WssWlanConfigReq.unWlanConfReq.
                                          WssWlanUpdateReq.RsnaIEElements.
                                          u2PmkidCount, pRcvdBuf);

                        CAPWAP_GET_NBYTE (WssWlanMsgStruct.unWssWlanMsg.
                                          WssWlanConfigReq.unWlanConfReq.
                                          WssWlanUpdateReq.RsnaIEElements.
                                          aPmkidList, pRcvdBuf, RSNA_PMKID_LEN);
                    }
                }

                WssWlanMsgStruct.unWssWlanMsg.WssWlanConfigReq.u1WlanOption
                    = WSSWLAN_UPDATE_REQ;
                if (WssIfProcessWssWlanMsg
                    (WSS_WLAN_TEST_CONFIG_REQ,
                     &WssWlanMsgStruct) == OSIX_SUCCESS)
                {
                    if (WssIfProcessWssWlanMsg (WSS_WLAN_CONFIG_REQ,
                                                &WssWlanMsgStruct) ==
                        OSIX_FAILURE)
                    {
                        APHDLR_TRC (APHDLR_FAILURE_TRC,
                                    "Failed to Set Received WLAN Conf Req \r\n");
                        return OSIX_FAILURE;
                    }
                }
                break;
#ifdef WPS_WTP_WANTED
            case IEEE_INFORMATION_ELEMENT:
                MEMSET (&WssWlanMsgStruct, 0, sizeof (tWssWlanMsgStruct));
                pIeeeRadioRcvdBuf += u4Offset;
                CAPWAP_GET_2BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.RadioIfInfoElement.
                                  u2MessageType, pIeeeRadioRcvdBuf);
                CAPWAP_GET_2BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.RadioIfInfoElement.
                                  u2MessageLength, pIeeeRadioRcvdBuf);
                CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.RadioIfInfoElement.u1RadioId,
                                  pIeeeRadioRcvdBuf);
                CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.RadioIfInfoElement.u1WlanId,
                                  pIeeeRadioRcvdBuf);
                CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.RadioIfInfoElement.u1BPFlag,
                                  pIeeeRadioRcvdBuf);
                CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                  WssWlanConfigReq.RadioIfInfoElement.u1EleId,
                                  pIeeeRadioRcvdBuf);
                if (WssWlanMsgStruct.unWssWlanMsg.WssWlanConfigReq.
                    RadioIfInfoElement.u1EleId == WPS_VENDOR_ELEMENT_ID)
                {
                    CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                      WssWlanConfigReq.RadioIfInfoElement.
                                      unInfoElem.pWpsIEElements.u1Len,
                                      pIeeeRadioRcvdBuf);
                    CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                      WssWlanConfigReq.RadioIfInfoElement.
                                      unInfoElem.pWpsIEElements.bWpsEnabled,
                                      pIeeeRadioRcvdBuf);
                    CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                      WssWlanConfigReq.RadioIfInfoElement.
                                      unInfoElem.pWpsIEElements.wscState,
                                      pIeeeRadioRcvdBuf);
                    CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                      WssWlanConfigReq.RadioIfInfoElement.
                                      unInfoElem.pWpsIEElements.
                                      bWpsAdditionalIE, pIeeeRadioRcvdBuf);
                    CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                      WssWlanConfigReq.RadioIfInfoElement.
                                      unInfoElem.pWpsIEElements.noOfAuthMac,
                                      pIeeeRadioRcvdBuf);
                    for (u1WpsIndex = 0;
                         u1WpsIndex <
                         WssWlanMsgStruct.unWssWlanMsg.WssWlanConfigReq.
                         RadioIfInfoElement.unInfoElem.pWpsIEElements.
                         noOfAuthMac; u1WpsIndex++)
                    {
                        CAPWAP_GET_NBYTE (WssWlanMsgStruct.unWssWlanMsg.
                                          WssWlanConfigReq.RadioIfInfoElement.
                                          unInfoElem.pWpsIEElements.
                                          authorized_macs[u1WpsIndex][ZERO],
                                          pIeeeRadioRcvdBuf, WPS_ETH_ALEN);
                    }
                    CAPWAP_GET_NBYTE (WssWlanMsgStruct.unWssWlanMsg.
                                      WssWlanConfigReq.RadioIfInfoElement.
                                      unInfoElem.pWpsIEElements.uuid_e,
                                      pIeeeRadioRcvdBuf, WPS_UUID_LEN);
                    CAPWAP_GET_2BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                      WssWlanConfigReq.RadioIfInfoElement.
                                      unInfoElem.pWpsIEElements.configMethods,
                                      pIeeeRadioRcvdBuf);
                    CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                      WssWlanConfigReq.RadioIfInfoElement.
                                      unInfoElem.pWpsIEElements.statusOrAddIE,
                                      pIeeeRadioRcvdBuf);
                    WssWlanMsgStruct.unWssWlanMsg.WssWlanConfigReq.
                        RadioIfInfoElement.unInfoElem.pWpsIEElements.
                        isOptional = OSIX_TRUE;
                }
                WssWlanMsgStruct.unWssWlanMsg.WssWlanConfigReq.
                    RadioIfInfoElement.isPresent = OSIX_TRUE;

                if (WssIfProcessWssWlanMsg (WSS_WLAN_CONFIG_REQ,
                                            &WssWlanMsgStruct) == OSIX_FAILURE)
                {
                    APHDLR_TRC (APHDLR_FAILURE_TRC,
                                "Failed to Set Received WLAN ConfReq\r\n");
                    return OSIX_FAILURE;
                }
                break;
#endif
            case VENDOR_SPECIFIC_PAYLOAD:
                MEMSET (&WssWlanMsgStruct, 0, sizeof (tWssWlanMsgStruct));
                pVendRcvdBuf += u4Offset;
                for (u1Index = 0; u1Index < VEND_CONF_MAX; u1Index++)
                {
                    CAPWAP_GET_2BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                      WssWlanConfigReq.vendSpec[u1Index].
                                      u2MsgEleType, pVendRcvdBuf);
                    CAPWAP_GET_2BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                      WssWlanConfigReq.vendSpec[u1Index].
                                      u2MsgEleLen, pVendRcvdBuf);
                    if (WssWlanMsgStruct.unWssWlanMsg.WssWlanConfigReq.
                        vendSpec[u1Index].u2MsgEleLen != 0)
                    {
                        CAPWAP_GET_2BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                          WssWlanConfigReq.vendSpec[u1Index].
                                          elementId, pVendRcvdBuf);
                        /*WPA code */
                        if (WssWlanMsgStruct.unWssWlanMsg.WssWlanConfigReq.
                            vendSpec[u1Index].elementId == VENDOR_WPA_MSG)
                        {
                            WssWlanMsgStruct.unWssWlanMsg.WssWlanConfigReq.
                                vendSpec[u1Index].elementId = VENDOR_WPA_TYPE;
                            WssWlanMsgStruct.unWssWlanMsg.WssWlanConfigReq.
                                vendSpec[u1Index].unVendorSpec.
                                WpaIEElements.u2MsgEleType = VENDOR_WPA_TYPE;
                            WssWlanMsgStruct.unWssWlanMsg.WssWlanConfigReq.
                                vendSpec[u1Index].unVendorSpec.WpaIEElements.
                                u1ElemId = WPA_IE_ID;
                            CAPWAP_GET_2BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                              WssWlanConfigReq.
                                              vendSpec[u1Index].unVendorSpec.
                                              WpaIEElements.u2MsgEleLen,
                                              pVendRcvdBuf);
                            CAPWAP_GET_4BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                              WssWlanConfigReq.
                                              vendSpec[u1Index].unVendorSpec.
                                              WpaIEElements.u4VendorId,
                                              pVendRcvdBuf);
                            CAPWAP_GET_2BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                              WssWlanConfigReq.
                                              vendSpec[u1Index].unVendorSpec.
                                              WpaIEElements.u2Ver,
                                              pVendRcvdBuf);
                            CAPWAP_GET_NBYTE (WssWlanMsgStruct.unWssWlanMsg.
                                              WssWlanConfigReq.
                                              vendSpec[u1Index].unVendorSpec.
                                              WpaIEElements.au1GroupCipherSuite,
                                              pVendRcvdBuf, 4);

                            CAPWAP_GET_2BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                              WssWlanConfigReq.
                                              vendSpec[u1Index].unVendorSpec.
                                              WpaIEElements.
                                              u2PairwiseCipherSuiteCount,
                                              pVendRcvdBuf);
                            WssWlanMsgStruct.unWssWlanMsg.WssWlanConfigReq.
                                vendSpec[u1Index].unVendorSpec.WpaIEElements.
                                u1RadioId = u1RadioId;
                            for (u1Count = 0;
                                 u1Count <
                                 WssWlanMsgStruct.unWssWlanMsg.WssWlanConfigReq.
                                 vendSpec[u1Index].unVendorSpec.WpaIEElements.
                                 u2PairwiseCipherSuiteCount; u1Count++)
                            {
                                CAPWAP_GET_NBYTE (WssWlanMsgStruct.unWssWlanMsg.
                                                  WssWlanConfigReq.
                                                  vendSpec[u1Index].
                                                  unVendorSpec.WpaIEElements.
                                                  aWpaPwCipherDB[u1Index].
                                                  au1PairwiseCipher,
                                                  pVendRcvdBuf, 4);
                            }
                            CAPWAP_GET_2BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                              WssWlanConfigReq.
                                              vendSpec[u1Index].unVendorSpec.
                                              WpaIEElements.u2AKMSuiteCount,
                                              pVendRcvdBuf);
                            for (u1Count = 0;
                                 u1Count <
                                 WssWlanMsgStruct.unWssWlanMsg.WssWlanConfigReq.
                                 vendSpec[u1Index].unVendorSpec.WpaIEElements.
                                 u2AKMSuiteCount; u1Count++)
                            {
                                CAPWAP_GET_NBYTE (WssWlanMsgStruct.unWssWlanMsg.
                                                  WssWlanConfigReq.
                                                  vendSpec[u1Index].
                                                  unVendorSpec.WpaIEElements.
                                                  aWpaAkmDB[u1Index].
                                                  au1AKMSuite, pVendRcvdBuf, 4);
                            }
                            WssWlanMsgStruct.unWssWlanMsg.WssWlanConfigReq.
                                vendSpec[u1Index].isOptional = OSIX_TRUE;
                        }

                        if (WssWlanMsgStruct.unWssWlanMsg.WssWlanConfigReq.
                            vendSpec[u1Index].elementId ==
                            SSID_ISOLATION_VENDOR_MSG)
                        {

                            WssWlanMsgStruct.unWssWlanMsg.WssWlanConfigReq.
                                vendSpec[u1Index].unVendorSpec.
                                VendSsidIsolation.u2MsgEleType =
                                VENDOR_SSID_ISOLATION_MSG;
                            CAPWAP_GET_2BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                              WssWlanConfigReq.
                                              vendSpec[u1Index].unVendorSpec.
                                              VendSsidIsolation.u2MsgEleLen,
                                              pVendRcvdBuf);
                            CAPWAP_GET_4BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                              WssWlanConfigReq.
                                              vendSpec[u1Index].unVendorSpec.
                                              VendSsidIsolation.vendorId,
                                              pVendRcvdBuf);
                            CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                              WssWlanConfigReq.
                                              vendSpec[u1Index].unVendorSpec.
                                              VendSsidIsolation.u1Val,
                                              pVendRcvdBuf);
                            CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                              WssWlanConfigReq.
                                              vendSpec[u1Index].unVendorSpec.
                                              VendSsidIsolation.u1RadioId,
                                              pVendRcvdBuf);
                            CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                              WssWlanConfigReq.
                                              vendSpec[u1Index].unVendorSpec.
                                              VendSsidIsolation.u1WlanId,
                                              pVendRcvdBuf);

                            WssWlanMsgStruct.unWssWlanMsg.WssWlanConfigReq.
                                vendSpec[u1Index].isOptional = OSIX_TRUE;
                        }

                        /* PARSE Wlan Vlan Information and bind the ath
                         * interface */
                        if (WssWlanMsgStruct.unWssWlanMsg.WssWlanConfigReq.
                            vendSpec[u1Index].elementId == VENDOR_WLAN_VLAN_MSG)
                        {

                            WssWlanMsgStruct.unWssWlanMsg.WssWlanConfigReq.
                                vendSpec[u1Index].unVendorSpec.VendWlanVlan.
                                u2MsgEleType = VENDOR_WLAN_VLAN_TYPE;
                            CAPWAP_GET_2BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                              WssWlanConfigReq.
                                              vendSpec[u1Index].unVendorSpec.
                                              VendWlanVlan.u2MsgEleLen,
                                              pVendRcvdBuf);
                            CAPWAP_GET_4BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                              WssWlanConfigReq.
                                              vendSpec[u1Index].unVendorSpec.
                                              VendWlanVlan.vendorId,
                                              pVendRcvdBuf);
                            CAPWAP_GET_2BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                              WssWlanConfigReq.
                                              vendSpec[u1Index].unVendorSpec.
                                              VendWlanVlan.u2VlanId,
                                              pVendRcvdBuf);
                            CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                              WssWlanConfigReq.
                                              vendSpec[u1Index].unVendorSpec.
                                              VendWlanVlan.u1RadioId,
                                              pVendRcvdBuf);
                            CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                              WssWlanConfigReq.
                                              vendSpec[u1Index].unVendorSpec.
                                              VendWlanVlan.u1WlanId,
                                              pVendRcvdBuf);

                            WssWlanMsgStruct.unWssWlanMsg.WssWlanConfigReq.
                                vendSpec[u1Index].isOptional = OSIX_TRUE;
                            APHDLR_TRC3 (APHDLR_DEBUG_TRC,
                                         "\n WLAN_VLAN_VENDOR_MSG :: Vlan : %d, Radio : %d, Wlan : %d\n",
                                         WssWlanMsgStruct.unWssWlanMsg.
                                         WssWlanConfigReq.vendSpec[u1Index].
                                         unVendorSpec.VendWlanVlan.u2VlanId,
                                         WssWlanMsgStruct.unWssWlanMsg.
                                         WssWlanConfigReq.vendSpec[u1Index].
                                         unVendorSpec.VendWlanVlan.u1RadioId,
                                         WssWlanMsgStruct.unWssWlanMsg.
                                         WssWlanConfigReq.vendSpec[u1Index].
                                         unVendorSpec.VendWlanVlan.u1WlanId);
                        }

                        if (WssWlanMsgStruct.unWssWlanMsg.WssWlanConfigReq.
                            vendSpec[u1Index].elementId == SSID_RATE_VENDOR_MSG)
                        {
                            WssWlanMsgStruct.unWssWlanMsg.WssWlanConfigReq.
                                vendSpec[u1Index].unVendorSpec.VendSsidRate.
                                u2MsgEleType = VENDOR_SSID_RATE_MSG;
                            CAPWAP_GET_2BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                              WssWlanConfigReq.
                                              vendSpec[u1Index].unVendorSpec.
                                              VendSsidRate.u2MsgEleLen,
                                              pVendRcvdBuf);
                            CAPWAP_GET_4BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                              WssWlanConfigReq.
                                              vendSpec[u1Index].unVendorSpec.
                                              VendSsidRate.vendorId,
                                              pVendRcvdBuf);
                            CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                              WssWlanConfigReq.
                                              vendSpec[u1Index].unVendorSpec.
                                              VendSsidRate.u1QosRateLimit,
                                              pVendRcvdBuf);
                            CAPWAP_GET_4BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                              WssWlanConfigReq.
                                              vendSpec[u1Index].unVendorSpec.
                                              VendSsidRate.u4QosUpStreamCIR,
                                              pVendRcvdBuf);
                            CAPWAP_GET_4BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                              WssWlanConfigReq.
                                              vendSpec[u1Index].unVendorSpec.
                                              VendSsidRate.u4QosUpStreamCBS,
                                              pVendRcvdBuf);
                            CAPWAP_GET_4BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                              WssWlanConfigReq.
                                              vendSpec[u1Index].unVendorSpec.
                                              VendSsidRate.u4QosUpStreamEIR,
                                              pVendRcvdBuf);
                            CAPWAP_GET_4BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                              WssWlanConfigReq.
                                              vendSpec[u1Index].unVendorSpec.
                                              VendSsidRate.u4QosUpStreamEBS,
                                              pVendRcvdBuf);
                            CAPWAP_GET_4BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                              WssWlanConfigReq.
                                              vendSpec[u1Index].unVendorSpec.
                                              VendSsidRate.u4QosDownStreamCIR,
                                              pVendRcvdBuf);
                            CAPWAP_GET_4BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                              WssWlanConfigReq.
                                              vendSpec[u1Index].unVendorSpec.
                                              VendSsidRate.u4QosDownStreamCBS,
                                              pVendRcvdBuf);
                            CAPWAP_GET_4BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                              WssWlanConfigReq.
                                              vendSpec[u1Index].unVendorSpec.
                                              VendSsidRate.u4QosDownStreamEIR,
                                              pVendRcvdBuf);
                            CAPWAP_GET_4BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                              WssWlanConfigReq.
                                              vendSpec[u1Index].unVendorSpec.
                                              VendSsidRate.u4QosDownStreamEBS,
                                              pVendRcvdBuf);
                            CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                              WssWlanConfigReq.
                                              vendSpec[u1Index].unVendorSpec.
                                              VendSsidRate.u1PassengerTrustMode,
                                              pVendRcvdBuf);
                            CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                              WssWlanConfigReq.
                                              vendSpec[u1Index].unVendorSpec.
                                              VendSsidRate.u1RadioId,
                                              pVendRcvdBuf);
                            CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                              WssWlanConfigReq.
                                              vendSpec[u1Index].unVendorSpec.
                                              VendSsidRate.u1WlanId,
                                              pVendRcvdBuf);

                            WssWlanMsgStruct.unWssWlanMsg.WssWlanConfigReq.
                                vendSpec[u1Index].isOptional = OSIX_TRUE;
                        }

                        if (WssWlanMsgStruct.unWssWlanMsg.WssWlanConfigReq.
                            vendSpec[u1Index].elementId == MULTICAST_VENDOR_MSG)
                        {
                            WssWlanMsgStruct.unWssWlanMsg.WssWlanConfigReq.
                                vendSpec[u1Index].unVendorSpec.
                                VendorMulticast.u2MsgEleType =
                                VENDOR_MULTICAST_MSG;
                            CAPWAP_GET_2BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                              WssWlanConfigReq.
                                              vendSpec[u1Index].unVendorSpec.
                                              VendorMulticast.u2MsgEleLen,
                                              pVendRcvdBuf);
                            CAPWAP_GET_4BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                              WssWlanConfigReq.
                                              vendSpec[u1Index].unVendorSpec.
                                              VendorMulticast.vendorId,
                                              pVendRcvdBuf);
                            CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                              WssWlanConfigReq.
                                              vendSpec[u1Index].unVendorSpec.
                                              VendorMulticast.u1RadioId,
                                              pVendRcvdBuf);
                            CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                              WssWlanConfigReq.
                                              vendSpec[u1Index].unVendorSpec.
                                              VendorMulticast.u1WlanId,
                                              pVendRcvdBuf);
                            CAPWAP_GET_2BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                              WssWlanConfigReq.
                                              vendSpec[u1Index].unVendorSpec.
                                              VendorMulticast.
                                              u2WlanMulticastMode,
                                              pVendRcvdBuf);
                            CAPWAP_GET_2BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                              WssWlanConfigReq.
                                              vendSpec[u1Index].unVendorSpec.
                                              VendorMulticast.
                                              u2WlanSnoopTableLength,
                                              pVendRcvdBuf);
                            CAPWAP_GET_4BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                              WssWlanConfigReq.
                                              vendSpec[u1Index].unVendorSpec.
                                              VendorMulticast.u4WlanSnoopTimer,
                                              pVendRcvdBuf);
                            CAPWAP_GET_4BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                              WssWlanConfigReq.
                                              vendSpec[u1Index].unVendorSpec.
                                              VendorMulticast.
                                              u4WlanSnoopTimeout, pVendRcvdBuf);
                        }
                        if (WssWlanMsgStruct.unWssWlanMsg.WssWlanConfigReq.
                            vendSpec[u1Index].elementId == WLAN_INTERFACE_IP)
                        {
                            WssWlanMsgStruct.unWssWlanMsg.WssWlanConfigReq.
                                vendSpec[u1Index].unVendorSpec.
                                VendorWlanInterfaceIp.u2MsgEleType =
                                VENDOR_WLAN_INTERFACE_IP;

                            CAPWAP_GET_2BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                              WssWlanConfigReq.
                                              vendSpec[u1Index].unVendorSpec.
                                              VendorWlanInterfaceIp.u2MsgEleLen,
                                              pVendRcvdBuf);
                            CAPWAP_GET_4BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                              WssWlanConfigReq.
                                              vendSpec[u1Index].unVendorSpec.
                                              VendorWlanInterfaceIp.u4VendorId,
                                              pVendRcvdBuf);
                            CAPWAP_GET_4BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                              WssWlanConfigReq.
                                              vendSpec[u1Index].unVendorSpec.
                                              VendorWlanInterfaceIp.u4IpAddr,
                                              pVendRcvdBuf);
                            CAPWAP_GET_4BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                              WssWlanConfigReq.
                                              vendSpec[u1Index].unVendorSpec.
                                              VendorWlanInterfaceIp.u4SubMask,
                                              pVendRcvdBuf);
                            CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                              WssWlanConfigReq.
                                              vendSpec[u1Index].unVendorSpec.
                                              VendorWlanInterfaceIp.
                                              b1AdminStatus, pVendRcvdBuf);
                            CapSetWlanIpAddr (WssWlanMsgStruct.unWssWlanMsg.
                                              WssWlanConfigReq.
                                              vendSpec[u1Index].unVendorSpec.
                                              VendorWlanInterfaceIp.u4IpAddr,
                                              WssWlanMsgStruct.unWssWlanMsg.
                                              WssWlanConfigReq.
                                              vendSpec[u1Index].unVendorSpec.
                                              VendorWlanInterfaceIp.u4SubMask,
                                              u1IPRadioId, u1IPWlanId);
                        }
                        if (WssWlanMsgStruct.unWssWlanMsg.WssWlanConfigReq.
                            vendSpec[u1Index].elementId == VENDOR_DIFF_SERV_MSG)
                        {
                            WssWlanMsgStruct.unWssWlanMsg.WssWlanConfigReq.
                                vendSpec[u1Index].unVendorSpec.
                                VendorSpecDiffServ.u2MsgEleType =
                                VENDOR_DIFF_SERV_TYPE;

                            CAPWAP_GET_2BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                              WssWlanConfigReq.
                                              vendSpec[u1Index].unVendorSpec.
                                              VendorSpecDiffServ.u2MsgEleLen,
                                              pVendRcvdBuf);
                            CAPWAP_GET_4BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                              WssWlanConfigReq.
                                              vendSpec[u1Index].unVendorSpec.
                                              VendorSpecDiffServ.u4VendorId,
                                              pVendRcvdBuf);
                            CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                              WssWlanConfigReq.
                                              vendSpec[u1Index].unVendorSpec.
                                              VendorSpecDiffServ.u1EntryStatus,
                                              pVendRcvdBuf);
                            CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                              WssWlanConfigReq.
                                              vendSpec[u1Index].unVendorSpec.
                                              VendorSpecDiffServ.u1RadioId,
                                              pVendRcvdBuf);
                            CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                              WssWlanConfigReq.
                                              vendSpec[u1Index].unVendorSpec.
                                              VendorSpecDiffServ.u1InPriority,
                                              pVendRcvdBuf);
                            CAPWAP_GET_1BYTE (WssWlanMsgStruct.unWssWlanMsg.
                                              WssWlanConfigReq.
                                              vendSpec[u1Index].unVendorSpec.
                                              VendorSpecDiffServ.u1OutDscp,
                                              pVendRcvdBuf);
                            CapSetDiffServ (&WssWlanMsgStruct.unWssWlanMsg.
                                            WssWlanConfigReq.vendSpec[u1Index].
                                            unVendorSpec.VendorSpecDiffServ,
                                            u1IPWlanId);
                        }

                    }

                }
                if (WssIfProcessWssWlanMsg (WSS_WLAN_TEST_CONFIG_REQ,
                                            &WssWlanMsgStruct) == OSIX_SUCCESS)
                {
                    if (WssIfProcessWssWlanMsg (WSS_WLAN_CONFIG_REQ,
                                                &WssWlanMsgStruct) ==
                        OSIX_FAILURE)
                    {
                        APHDLR_TRC (APHDLR_FAILURE_TRC,
                                    "Failed to Set Received WLAN ConfReq\r\n");
                        return OSIX_FAILURE;
                    }
                }
                break;
            default:
                APHDLR_TRC (APHDLR_FAILURE_TRC,
                            "CapValidateWlanConfReqMsgElem: "
                            "Invalid WlanConfig Update Request\r\n");
                return OSIX_FAILURE;
        }
    }
    UNUSED_PARAM (pIeeeRadioRcvdBuf);
    APHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapProcessWlanConfigReq                        */
/*  Description     : The function handles the Wlan Conf Req from WLAN. */
/*  Input(s)        : pBuf - Buffer pointer                             */
/*                    pWlanConfReq - Pointer to Wlan Conf Req struct    */
/*                    pSessEntry - Remote Session Entry                 */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS | OSIX_FAILURE                       */
/************************************************************************/
INT4
CapwapProcessWlanConfigReq (tCRU_BUF_CHAIN_HEADER * pBuf,
                            tCapwapControlPacket * pWlanConfReq,
                            tRemoteSessionManager * pSessEntry)
{
    tWssWlanConfigRsp   WlanConfRsp;
    UINT1              *pu1TxBuf = NULL, *pRcvdBuf = NULL;
    UINT1               u1SeqNum = 0;
    UINT4               u4MsgLen = 0;
    tCRU_BUF_CHAIN_HEADER *pTxBuf = NULL;
    UINT1               u1IsNotLinear = OSIX_FALSE;
    UINT2               u2PacketLen = 0;
    unCapwapMsgStruct   CapwapMsgStruct;

    APHDLR_FN_ENTRY ();
    if (pSessEntry == NULL)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC, "APHDLR Session Entry is NULL,"
                    "return FAILURE \r\n");
        return OSIX_FAILURE;
    }
    if (pSessEntry->eCurrentState != CAPWAP_RUN)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC, "Session not in RUN state \r\n");
        return OSIX_FAILURE;
    }
    MEMSET (&WlanConfRsp, 0, sizeof (tWssWlanConfigRsp));
    MEMSET (&CapwapMsgStruct, 0, sizeof (unCapwapMsgStruct));

    /* Recieved sequence number */
    u1SeqNum = pWlanConfReq->capwapCtrlHdr.u1SeqNum;

    if (pWlanConfReq->u4ReturnCode == MISSING_MANDATORY_MSG_ELEMENT)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC,
                    "Missing Mandatory Message Elements \r\n");
        WlanConfRsp.resultCode.u4Value = MISSING_MANDATORY_MSG_ELEMENT;
        WlanConfRsp.resultCode.u2MsgEleType = RESULT_CODE;
        WlanConfRsp.resultCode.u2MsgEleLen = RESULT_CODE_LEN;
        WlanConfRsp.resultCode.isOptional = OSIX_TRUE;
        WlanConfRsp.u2CapwapMsgElemenLen = (UINT2)
            (WlanConfRsp.u2CapwapMsgElemenLen +
             (CAPWAP_MSG_ELEM_TYPE_LEN + CAPWAP_MSG_ELEM_TYPE_LEN));
    }
    else
    {
        u2PacketLen = (UINT2) APHDLR_GET_BUF_LEN (pBuf);
        pRcvdBuf = APHDLR_BUF_IF_LINEAR (pBuf, 0, u2PacketLen);
        if (pRcvdBuf == NULL)
        {
            pRcvdBuf = (UINT1 *) MemAllocMemBlk (APHDLR_PKTBUF_POOLID);
            APHDLR_COPY_FROM_BUF (pBuf, pRcvdBuf, 0, u2PacketLen);
            u1IsNotLinear = OSIX_TRUE;
        }
        /* Validate the WLAN Config Request message. if it is success 
         * construct the WLAN Config Response */
        if ((CapValidateWlanConfReqMsgElem
             (pRcvdBuf, pWlanConfReq, &WlanConfRsp) == OSIX_SUCCESS))
        {
            /* As Validtion is successful set the result code to SUCCESS */
            WlanConfRsp.resultCode.u4Value = CAPWAP_SUCCESS;
            WlanConfRsp.resultCode.u2MsgEleType = RESULT_CODE;
            WlanConfRsp.resultCode.u2MsgEleLen = RESULT_CODE_LEN;
            WlanConfRsp.resultCode.isOptional = OSIX_TRUE;
            WlanConfRsp.u2CapwapMsgElemenLen = (UINT2)
                (WlanConfRsp.u2CapwapMsgElemenLen +
                 (CAPWAP_MSG_ELEM_TYPE_LEN + CAPWAP_MSG_ELEM_TYPE_LEN));
        }
        else
        {
            /* As Validtion is successful set the result code to SUCCESS */
            WlanConfRsp.resultCode.u4Value = CAPWAP_FAILURE;
            WlanConfRsp.resultCode.u2MsgEleType = RESULT_CODE;
            WlanConfRsp.resultCode.u2MsgEleLen = RESULT_CODE_LEN;
            WlanConfRsp.resultCode.isOptional = OSIX_TRUE;
            WlanConfRsp.u2CapwapMsgElemenLen = (UINT2)
                (WlanConfRsp.u2CapwapMsgElemenLen +
                 (CAPWAP_MSG_ELEM_TYPE_LEN + CAPWAP_MSG_ELEM_TYPE_LEN));
        }

        if (u1IsNotLinear == OSIX_TRUE)
        {
            MemReleaseMemBlock (APHDLR_PKTBUF_POOLID, pRcvdBuf);
        }

        /* get message elements to construct the WLAN Config Update response */
        /* update optional Messege Elements */
        if (WlanConfRsp.WssWlanConfRsp.WssWlanRsp.u1IsPresent == OSIX_TRUE)
        {
            u4MsgLen =
                (UINT4) (u4MsgLen +
                         WlanConfRsp.WssWlanConfRsp.WssWlanRsp.u2Length +
                         CAPWAP_MSG_ELEM_TYPE_LEN);
        }
        if (CapwapGetVendorPayload (&WlanConfRsp.vendSpec, &u4MsgLen) ==
            OSIX_FAILURE)
        {
            APHDLR_TRC (APHDLR_FAILURE_TRC,
                        "Failed to get Optional Conf Update Message Elements \r\n");
            return OSIX_FAILURE;
        }
    }

    /* update Control Header */
    WlanConfRsp.u1SeqNum = pWlanConfReq->capwapCtrlHdr.u1SeqNum;
    WlanConfRsp.u2CapwapMsgElemenLen = (UINT2)
        (WlanConfRsp.u2CapwapMsgElemenLen +
         u4MsgLen + CAPWAP_MSG_ELEM_PLUS_SEQ_LEN);
    WlanConfRsp.u1NumMsgBlocks = 0;    /* flags always zero */

    /* Allocate memory for packet linear buffer */
    pTxBuf = CAPWAP_ALLOCATE_CRU_BUF (CAPWAP_MAX_PKT_LEN, 0);
    if (pTxBuf == NULL)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC, "Memory Allocation Failed\n");
        return OSIX_FAILURE;
    }
    pu1TxBuf = APHDLR_BUF_IF_LINEAR (pTxBuf, 0, CAPWAP_MAX_PKT_LEN);
    if (pu1TxBuf == NULL)
    {
        pu1TxBuf = (UINT1 *) MemAllocMemBlk (APHDLR_PKTBUF_POOLID);
        CAPWAP_COPY_FROM_BUF (pTxBuf, pu1TxBuf, 0, CAPWAP_MAX_PKT_LEN);
        u1IsNotLinear = OSIX_TRUE;
    }

    CapwapMsgStruct.CapAssembleWlanUpdateRsp.pData = pu1TxBuf;
    CapwapMsgStruct.CapAssembleWlanUpdateRsp.pCapwapCtrlMsg = &WlanConfRsp;
    if (WssIfProcessCapwapMsg (WSS_CAPWAP_ASSEMBLE_WLAN_CONF_RSP,
                               &CapwapMsgStruct) == OSIX_FAILURE)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC,
                    "Failed to assemble the Config Update Response Message \r\n");
        if (u1IsNotLinear == OSIX_TRUE)
        {
            MemReleaseMemBlock (APHDLR_PKTBUF_POOLID, pu1TxBuf);
        }
        CAPWAP_RELEASE_CRU_BUF (pTxBuf);
        return OSIX_FAILURE;
    }

    if (u1IsNotLinear == OSIX_TRUE)
    {
        APHDLR_COPY_TO_BUF (pTxBuf, pu1TxBuf, 0, CAPWAP_MAX_PKT_LEN);
        MemReleaseMemBlock (APHDLR_PKTBUF_POOLID, pu1TxBuf);
    }

    /* Transmit Configuration Update Response Message */
    CapwapMsgStruct.CapwapTxPkt.pData = pTxBuf;
    CapwapMsgStruct.CapwapTxPkt.u4pktLen = (UINT4)
        (WlanConfRsp.u2CapwapMsgElemenLen + CAPWAP_CMN_HDR_LEN);
    CapwapMsgStruct.CapwapTxPkt.u4IpAddr = pSessEntry->remoteIpAddr.u4_addr[0];
    CapwapMsgStruct.CapwapTxPkt.u4DestPort = pSessEntry->u4RemoteCtrlPort;
    if (WssIfProcessCapwapMsg (WSS_CAPWAP_TX_CTRL_PKT, &CapwapMsgStruct)
        == OSIX_FAILURE)
    {
        APHDLR_RELEASE_CRU_BUF (pTxBuf);
        pSessEntry->lastTransmittedPkt = NULL;
        pSessEntry->lastTransmittedPktLen = 0;
        APHDLR_TRC (APHDLR_FAILURE_TRC, "Failed to Transmit the packet \r\n");
        return OSIX_FAILURE;
    }
    APHDLR_RELEASE_CRU_BUF (pTxBuf);
    pSessEntry->lastTransmitedSeqNum++;
    pSessEntry->lastTransmittedPkt = NULL;
    pSessEntry->lastTransmittedPktLen = 0;

    UNUSED_PARAM (u1SeqNum);
    APHDLR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapProcessWlanConfigRsp                        */
/*  Description     : The function is unused in WTP                     */
/*  Input(s)        : None                                              */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/
INT4
CapwapProcessWlanConfigRsp (tCRU_BUF_CHAIN_HEADER * pBuf,
                            tCapwapControlPacket * pWlanConfRsp,
                            tRemoteSessionManager * pSessEntry)
{
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pWlanConfRsp);
    UNUSED_PARAM (pSessEntry);
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapSetWlanIpAddr                                  */
/*  Description     : Set WLAN IP Address                               */
/*  Input(s)        : None                                              */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/
INT4
CapSetWlanIpAddr (UINT4 u4IpAddr, UINT4 u4SubnetMask, UINT1 u1RadioId,
                  UINT1 u1WlanId)
{
    INT4                i4Ifindex = 0;
    UINT1               u1InterfaceName[24];
    INT4                i4BridgedIfStat = CFA_DISABLED;
    tWssWlanDB          WssWlanDB;
    MEMSET (&WssWlanDB, 0, sizeof (tWssWlanDB));
    MEMSET (u1InterfaceName, 0, 24);
    WssWlanDB.WssWlanAttributeDB.u1RadioId = u1RadioId;
    WssWlanDB.WssWlanAttributeDB.u1WlanId = u1WlanId;
    WssWlanDB.WssWlanIsPresentDB.bInterfaceName = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_INTERFACE_ENTRY, &WssWlanDB) ==
        OSIX_FAILURE)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC, "WssIfProcessWssWlanDBMsg failed \r\n");
    }
    MEMCPY (u1InterfaceName, WssWlanDB.WssWlanAttributeDB.au1InterfaceName, 24);
#ifdef QCAX_WANTED
    if (CfaGddGetIfIndexforInterface (u1InterfaceName, &i4Ifindex) ==
        CFA_FAILURE)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC,
                    "CfaGddGetIfIndexforInterface failed \r\n");
    }
#endif
    if (nmhSetIfAdminStatus (i4Ifindex, CFA_IF_DOWN) == SNMP_FAILURE)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC, "nmhSetIfAdminStatus failed \r\n");
    }
    if (nmhSetIfIvrBridgedIface (i4Ifindex, i4BridgedIfStat) == SNMP_FAILURE)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC, "nmhSetIfIvrBridgedIface failed \r\n");
    }
    if (nmhSetIfIpAddr (i4Ifindex, u4IpAddr) == SNMP_FAILURE)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC, "nmhSetIfIpAddr failed \r\n");
    }
    if (nmhSetIfIpSubnetMask (i4Ifindex, u4SubnetMask) == SNMP_FAILURE)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC, "nmhSetIfIpSubnetMask failed \r\n");
    }
    if (nmhSetIfAdminStatus (i4Ifindex, CFA_IF_UP) == SNMP_FAILURE)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC, "nmhSetIfAdminStatus failed \r\n");
    }
    return OSIX_SUCCESS;
}

#ifdef DHCP_SRV_WANTED
/************************************************************************/
/*  Function Name   : CapSetDhcpSrv                                     */
/*  Description     : This function configures the DHCP Pool            */
/*  Input(s)        : None                                              */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/
INT4
CapSetDhcpSrv (tDhcpPoolConfigUpdateReq * pDhcpPoolConfigUpdateReq,
               INT2 i2index)
{
    UINT4               u4DhcpPoolIndex = 0;
    INT4                i4RetVal = 0;
    UINT4               u4Subnet = 0;
    UINT4               u4SubnetMask = 0;
    UINT4               u4StartIp = 0;
    UINT4               u4EndIp = 0;
    UINT4               u4DefaultIp = 0;
    UINT4               u4DnsIp = 0;
    UINT4               u4LeaseTime =
        pDhcpPoolConfigUpdateReq->u4LeaseExprTime[i2index];
    UINT2               u2PoolStatus = 0;
    UINT4               u4Status = 1;
    INT4                i4RowStatus = 0;
    UINT1               u1SubnetOptn[255];
    tSNMP_OCTET_STRING_TYPE OctetValue;
    MEMSET (u1SubnetOptn, 0, 255);
    u4DhcpPoolIndex = pDhcpPoolConfigUpdateReq->u4DhcpPoolId[i2index];
    u4Subnet = pDhcpPoolConfigUpdateReq->u4Subnet[i2index];
    u4SubnetMask = pDhcpPoolConfigUpdateReq->u4Mask[i2index];
    u4StartIp = pDhcpPoolConfigUpdateReq->u4StartIp[i2index];
    u4EndIp = pDhcpPoolConfigUpdateReq->u4EndIp[i2index];
    u2PoolStatus = pDhcpPoolConfigUpdateReq->u2PoolStatus[i2index];
    u4Status = (UINT4) pDhcpPoolConfigUpdateReq->u1ServerStatus;

    if (nmhSetDhcpSrvEnable (u4Status) == SNMP_FAILURE)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC, "nmhSetDhcpSrvEnable failed \r\n");

    }

    if (pDhcpPoolConfigUpdateReq->u2NoofPools != 0)
    {
        if (u2PoolStatus == ACTIVE)
        {
            if (nmhGetDhcpSrvSubnetPoolRowStatus ((INT4) u4DhcpPoolIndex,
                                                  &i4RowStatus) != SNMP_SUCCESS)
            {
                i4RetVal =
                    nmhSetDhcpSrvSubnetPoolRowStatus (u4DhcpPoolIndex,
                                                      CREATE_AND_WAIT);
                if (i4RetVal == SNMP_FAILURE)
                {
                    APHDLR_TRC (APHDLR_FAILURE_TRC,
                                "nmhSetDhcpSrvSubnetPoolRowStatus failed \r\n");
                }
            }
            i4RetVal = nmhSetDhcpSrvSubnetSubnet (u4DhcpPoolIndex, u4Subnet);
            if (i4RetVal == SNMP_FAILURE)
            {
                APHDLR_TRC (APHDLR_FAILURE_TRC,
                            "nmhSetDhcpSrvSubnetSubnet failed \r\n");
            }
            i4RetVal = nmhSetDhcpSrvSubnetMask (u4DhcpPoolIndex, u4SubnetMask);
            if (i4RetVal == SNMP_FAILURE)
            {
                APHDLR_TRC (APHDLR_FAILURE_TRC,
                            "nmhSetDhcpSrvSubnetSubnet failed \r\n");
            }
            i4RetVal =
                nmhSetDhcpSrvSubnetStartIpAddress (u4DhcpPoolIndex, u4StartIp);
            if (i4RetVal == SNMP_FAILURE)
            {
                APHDLR_TRC (APHDLR_FAILURE_TRC,
                            "nmhSetDhcpSrvSubnetStartIpAddress failed \r\n");
            }
            i4RetVal =
                nmhSetDhcpSrvSubnetEndIpAddress (u4DhcpPoolIndex, u4EndIp);
            if (i4RetVal == SNMP_FAILURE)
            {
                APHDLR_TRC (APHDLR_FAILURE_TRC,
                            "nmhSetDhcpSrvSubnetEndIpAddress failed \r\n");
            }
            i4RetVal =
                nmhSetDhcpSrvSubnetLeaseTime (u4DhcpPoolIndex, u4LeaseTime);
            if (i4RetVal == SNMP_FAILURE)
            {
                APHDLR_TRC (APHDLR_FAILURE_TRC,
                            "nmhSetDhcpSrvSubnetLeaseTime failed \r\n");
            }
            if (nmhSetDhcpSrvSubnetOptRowStatus (u4DhcpPoolIndex, 3, 5) !=
                SNMP_FAILURE)
            {
                OctetValue.pu1_OctetList = u1SubnetOptn;
                OctetValue.i4_Length = 4;

                u4DefaultIp =
                    OSIX_HTONL (pDhcpPoolConfigUpdateReq->u4DefaultIp[i2index]);
                MEMCPY (u1SubnetOptn, &u4DefaultIp, 4);

                if (nmhSetDhcpSrvSubnetOptVal (u4DhcpPoolIndex, 3, &OctetValue)
                    == SNMP_FAILURE)
                {
                    APHDLR_TRC (APHDLR_FAILURE_TRC,
                                "nmhSetDhcpSrvSubnetOptVal failed \r\n");
                }
                if (nmhSetDhcpSrvSubnetOptRowStatus (u4DhcpPoolIndex, 3, 1) ==
                    SNMP_FAILURE)
                {
                    APHDLR_TRC (APHDLR_FAILURE_TRC,
                                "nmhSetDhcpSrvSubnetOptRowStatus failed \r\n");
                }
            }
            if (nmhSetDhcpSrvSubnetOptRowStatus (u4DhcpPoolIndex, 6, 5) !=
                SNMP_FAILURE)
            {
                OctetValue.pu1_OctetList = u1SubnetOptn;
                OctetValue.i4_Length = 4;

                u4DnsIp =
                    OSIX_HTONL (pDhcpPoolConfigUpdateReq->
                                u4DnsServerIp[i2index]);
                MEMCPY (u1SubnetOptn, &u4DnsIp, 4);

                if (nmhSetDhcpSrvSubnetOptVal (u4DhcpPoolIndex, 6, &OctetValue)
                    == SNMP_FAILURE)
                {
                    APHDLR_TRC (APHDLR_FAILURE_TRC,
                                "nmhSetDhcpSrvSubnetOptVal failed \r\n");
                }
                if (nmhSetDhcpSrvSubnetOptRowStatus (u4DhcpPoolIndex, 6, 1) ==
                    SNMP_FAILURE)
                {
                    APHDLR_TRC (APHDLR_FAILURE_TRC,
                                "nmhSetDhcpSrvSubnetOptRowStatus failed \r\n");
                }
            }
            i4RetVal =
                nmhSetDhcpSrvSubnetPoolRowStatus (u4DhcpPoolIndex, ACTIVE);
            if (i4RetVal == SNMP_FAILURE)
            {
                APHDLR_TRC (APHDLR_FAILURE_TRC,
                            "nmhSetDhcpSrvSubnetPoolRowStatus ACTIVE failed \r\n");
            }
        }
        if (u2PoolStatus == DESTROY)
        {
            i4RetVal =
                nmhSetDhcpSrvSubnetPoolRowStatus (u4DhcpPoolIndex, DESTROY);
            if (i4RetVal == SNMP_FAILURE)
            {
                APHDLR_TRC (APHDLR_FAILURE_TRC,
                            "nmhSetDhcpSrvSubnetPoolRowStatus failed \r\n");
            }
        }
        else
        {
        }
    }
    return OSIX_SUCCESS;
}
#endif

#ifdef DHCP_RLY_WANTED
/************************************************************************/
/*  Function Name   : CapwapSetDhcpRelay                                */
/*  Description     : This function configures the DHCP Relay in ISS    */
/*  Input(s)        : i4Status - DHCP Relay Status                      */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/
INT4
CapwapSetDhcpRelay (INT4 i4Status)
{
    UINT4               u4ErrCode = 0;
    INT4                i4CxtId = 0;
    INT4                i4RowStatus = 0;

    if (i4Status == DHCP_ENABLE)
    {

        if (nmhGetFsMIDhcpRelayContextRowStatus (i4CxtId, &i4RowStatus) !=
            SNMP_SUCCESS)
        {
            /* While enabling relay, if row status get failed, then no
               relay is enabled on that context */
            if (nmhTestv2FsMIDhcpRelayContextRowStatus (&u4ErrCode, i4CxtId,
                                                        CREATE_AND_WAIT) ==
                SNMP_FAILURE)
            {
                return OSIX_FAILURE;
            }
            if (nmhSetFsMIDhcpRelayContextRowStatus (i4CxtId, CREATE_AND_WAIT)
                == SNMP_FAILURE)
            {
                return OSIX_FAILURE;
            }
            if (nmhTestv2FsMIDhcpRelaying (&u4ErrCode, i4CxtId, i4Status)
                == SNMP_FAILURE)
            {
                if (nmhSetFsMIDhcpRelayContextRowStatus (i4CxtId, DESTROY)
                    == SNMP_FAILURE)
                {
                    return OSIX_FAILURE;
                }
            }
            if (nmhSetFsMIDhcpRelaying (i4CxtId, i4Status) == SNMP_FAILURE)
            {
                if (nmhSetFsMIDhcpRelayContextRowStatus (i4CxtId, DESTROY)
                    == SNMP_FAILURE)
                {
                    return OSIX_FAILURE;
                }
            }
            if (nmhTestv2FsMIDhcpRelayContextRowStatus (&u4ErrCode, i4CxtId,
                                                        ACTIVE) == SNMP_FAILURE)
            {
                if (nmhSetFsMIDhcpRelayContextRowStatus (i4CxtId, DESTROY)
                    == SNMP_FAILURE)
                {
                    return OSIX_FAILURE;
                }
            }
            if (nmhSetFsMIDhcpRelayContextRowStatus (i4CxtId, ACTIVE)
                == SNMP_FAILURE)
            {
                return OSIX_FAILURE;
            }
        }
        if (i4RowStatus != ACTIVE)
        {
            if (nmhTestv2FsMIDhcpRelaying (&u4ErrCode, i4CxtId, i4Status)
                == SNMP_SUCCESS)
            {
                if (nmhSetFsMIDhcpRelaying (i4CxtId, i4Status) == SNMP_FAILURE)
                {
                    return OSIX_FAILURE;
                }
            }
            else
            {
                return OSIX_FAILURE;
            }
        }
    }
    else if (i4Status == DHCP_DISABLE)
    {
        /* DHCP Disable Case */
        if (nmhGetFsMIDhcpRelayContextRowStatus (i4CxtId, &i4RowStatus) !=
            SNMP_SUCCESS)
        {
            /* Relay Agent for the given context is not present */
            return OSIX_FAILURE;
        }
        if (nmhTestv2FsMIDhcpRelaying (&u4ErrCode, i4CxtId, i4Status)
            == SNMP_FAILURE)
        {
            return OSIX_FAILURE;
        }
        if (nmhSetFsMIDhcpRelaying (i4CxtId, i4Status) == SNMP_FAILURE)
        {
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapSetDhcpRelayNxtSrvIp                        */
/*  Description     : This function configures the DHCP Relay Next      */
/*              Server Ip address in ISS            */
/*  Input(s)        : u4BootSrvIpAddr (Server Ip Address)               */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/
INT4
CapwapSetDhcpRelayNxtSrvIp (UINT4 u4BootSrvIpAddr)
{
    /* code enhancement for Dhcp relay multiple servers 
     * support is added in this function  */

    INT1                i1Flag = DHCP_DISABLE;
    INT4                i4RowStatus;
    UINT4               u4ErrorCode;
    INT4                i4CxtId = 0;
    tCliHandle          CliHandle = 0;

    if (nmhGetFsMIDhcpRelaySrvAddressRowStatus
        (i4CxtId, u4BootSrvIpAddr, &i4RowStatus) == SNMP_SUCCESS)
    {
        /*Entry already exists */
        return (OSIX_SUCCESS);
    }
    if (DHCP_RLY_CXT_NODE[i4CxtId] != NULL)
    {
        if (DHCP_RLY_CXT_NODE[i4CxtId]->pServersIp == NULL)
        {
            i1Flag = DHCP_ENABLE;
        }
    }
    else
    {
        return OSIX_FAILURE;
    }
    if (nmhTestv2FsMIDhcpRelaySrvAddressRowStatus
        (&u4ErrorCode, i4CxtId, u4BootSrvIpAddr, CREATE_AND_GO) == SNMP_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    if (nmhSetFsMIDhcpRelaySrvAddressRowStatus
        (i4CxtId, u4BootSrvIpAddr, CREATE_AND_GO) == SNMP_FAILURE)
    {
        return (OSIX_FAILURE);
    }
    if (i1Flag == DHCP_ENABLE)
        /*First server configured, Enable the Serversonly Flag */
    {
        DhcpRelaySetServerOnly (CliHandle, DHCP_ENABLE);
    }
    return (OSIX_SUCCESS);
}

/************************************************************************/
/*  Function Name   : CapSetDhcpRelay                                   */
/*  Description     : This function configures the DHCP Relay in ISS    */
/*  Input(s)        : Pointer to pDhcpRelayConfigUpdateReq              */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/
INT4
CapSetDhcpRelay (tDhcpRelayConfigUpdateReq * pDhcpRelayConfigUpdateReq)
{
    UINT4               u4BootSrvIpAddr = 0;
    INT4                i4Status = 0;

    i4Status = (INT4) pDhcpRelayConfigUpdateReq->u1RelayStatus;
    u4BootSrvIpAddr = pDhcpRelayConfigUpdateReq->u4NextIpAddress;

    if (i4Status == 0)
    {
        i4Status = 2;
    }
    if (CapwapSetDhcpRelay (i4Status) == OSIX_FAILURE)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC, "CapwapSetDhcpRelay failed \r\n");
    }
    if (nmhSetDhcpSrvBootServerAddress (u4BootSrvIpAddr) == SNMP_FAILURE)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC,
                    "nmhSetDhcpSrvBootServerAddress failed \r\n");
    }
    if (CapwapSetDhcpRelayNxtSrvIp (u4BootSrvIpAddr) == OSIX_FAILURE)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC,
                    "CapwapSetDhcpRelayNxtSrvIp failed \r\n");
    }
    return OSIX_SUCCESS;
}
#endif
/************************************************************************/
/*  Function Name   : CapSetFirewallStatus                              */
/*  Description     : This function configures the firewall status      */
/*  Input(s)        : Pointer to pFireWallUpdateReq                     */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/
INT4
CapSetFirewallStatus (tFireWallUpdateReq * pFireWallUpdateReq)
{
#ifdef FIREWALL_WANTED
    INT4                i4FirewallStatus = 0;

    i4FirewallStatus = (INT4) (pFireWallUpdateReq->u1FirewallStatus);

    if ((nmhSetFwlGlobalMasterControlSwitch (i4FirewallStatus)) == SNMP_FAILURE)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC, "FwlStatusSet failed \r\n");
    }
#else
    UNUSED_PARAM (pFireWallUpdateReq);
#endif
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapSetNatStatus                                   */
/*  Description     : This function configures the NAT status           */
/*  Input(s)        : Pointer to pNatUpdateReq                          */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/
INT4
CapSetNatStatus (tNatUpdateReq * pNatUpdateReq)
{
    INT4                i4IfMainIndex = (INT4) pNatUpdateReq->u4NATConfigIndex;
    INT4                i4SetValIfMainNetworkType =
        (INT4) pNatUpdateReq->i2WanType;

    if (nmhSetIfAdminStatus (i4IfMainIndex, CFA_IF_DOWN) == CFA_FAILURE)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC, "nmhSetIfAdminStatus failed \r\n");
    }

    if (i4SetValIfMainNetworkType == 2)
    {
        if (nmhSetIfMainNetworkType (i4IfMainIndex, i4SetValIfMainNetworkType)
            == SNMP_FAILURE)
        {
            APHDLR_TRC (APHDLR_FAILURE_TRC,
                        "nmhSetIfMainNetworkType failed \r\n");
        }
    }
    if (nmhSetIfMainNetworkType (i4IfMainIndex, i4SetValIfMainNetworkType) ==
        SNMP_FAILURE)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC, "nmhSetIfMainNetworkType failed \r\n");
    }
    if (nmhSetIfAdminStatus (i4IfMainIndex, CFA_IF_UP) == CFA_FAILURE)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC, "nmhSetIfAdminStatus failed \r\n");
    }
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapSetIpRoute                                     */
/*  Description     : This function configures the static route in linux*/
/*  Input(s)        : Pointer to pRouteTableUpdateReq and interface idx */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/
INT4
CapSetIpRoute (tRouteTableUpdateReq * pRouteTableUpdateReq, INT2 i2Index)
{
    INT4                i4RouteStatus = 0;
    UINT4               u4Subnet = 0;
    UINT4               u4NetMask = 0;
    UINT4               u4Gateway = 0;

    u4Subnet = pRouteTableUpdateReq->u4Subnet[i2Index];
    u4NetMask = pRouteTableUpdateReq->u4NetMask[i2Index];
    u4Gateway = pRouteTableUpdateReq->u4Gateway[i2Index];
    if (pRouteTableUpdateReq->u1EntryStatus == ACTIVE)
    {
        i4RouteStatus = CREATE_AND_GO;
    }
    else
    {
        i4RouteStatus = DESTROY;
    }
    if (nmhSetIpCidrRouteStatus (u4Subnet, u4NetMask,
                                 0, u4Gateway, i4RouteStatus) != SNMP_SUCCESS)
    {
        APHDLR_TRC (APHDLR_FAILURE_TRC, "nmhSetIfAdminStatus failed\r\n");
    }
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapSetFirewallFilter                              */
/*  Description     : The function is used to configure firewall filter */
/*  Input(s)        : Pointer to Struct tFirewallFilterUpdateReq        */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/

INT4
CapSetFirewallFilter (tFirewallFilterUpdateReq * pFirewallFilterUpdateReq)
{
#ifdef FIREWALL_WANTED
    tSNMP_OCTET_STRING_TYPE FilterName;
    tSNMP_OCTET_STRING_TYPE SrcAddr;
    tSNMP_OCTET_STRING_TYPE DestAddr;
    tSNMP_OCTET_STRING_TYPE SrcPort;
    tSNMP_OCTET_STRING_TYPE DestPort;
    UINT1               au1FilterName[AP_FWL_MAX_FILTER_NAME_LEN];
    UINT1               au1SrcAddr[AP_FWL_MAX_ADDR_LEN];
    UINT1               au1SrcPort[AP_FWL_MAX_PORT_LEN];
    UINT1               au1DestPort[AP_FWL_MAX_PORT_LEN];
    UINT1               au1DestAddr[AP_FWL_MAX_ADDR_LEN];
    INT4                i4Proto = (INT4) pFirewallFilterUpdateReq->u1Proto;
    INT4                i4AddrType =
        (INT4) pFirewallFilterUpdateReq->u2AddrType;
    UINT2               u2PoolStatus = pFirewallFilterUpdateReq->u1EntryStatus;
    INT4                i4RetVal = 0;
    INT4                i4RowStatus = 0;
    UINT4               u4Error = 0;

    MEMSET (au1FilterName, 0, AP_FWL_MAX_FILTER_NAME_LEN);
    MEMSET (au1SrcAddr, 0, AP_FWL_MAX_ADDR_LEN);
    MEMSET (au1SrcPort, 0, AP_FWL_MAX_PORT_LEN);
    MEMSET (au1DestPort, 0, AP_FWL_MAX_PORT_LEN);
    MEMSET (au1DestAddr, 0, AP_FWL_MAX_ADDR_LEN);

    FilterName.pu1_OctetList = au1FilterName;
    SrcAddr.pu1_OctetList = au1SrcAddr;
    DestAddr.pu1_OctetList = au1DestAddr;
    SrcPort.pu1_OctetList = au1SrcPort;
    DestPort.pu1_OctetList = au1DestPort;

    FilterName.i4_Length =
        (INT4) STRLEN (pFirewallFilterUpdateReq->au1FilterName);
    SrcAddr.i4_Length = (INT4) STRLEN (pFirewallFilterUpdateReq->au1SrcAddr);
    DestAddr.i4_Length = (INT4) STRLEN (pFirewallFilterUpdateReq->au1DestAddr);
    SrcPort.i4_Length = (INT4) STRLEN (pFirewallFilterUpdateReq->au1SrcPort);
    DestPort.i4_Length = (INT4) STRLEN (pFirewallFilterUpdateReq->au1DestPort);

    MEMCPY (FilterName.pu1_OctetList, pFirewallFilterUpdateReq->au1FilterName,
            FilterName.i4_Length);
    MEMCPY (SrcAddr.pu1_OctetList, pFirewallFilterUpdateReq->au1SrcAddr,
            SrcAddr.i4_Length);
    MEMCPY (DestAddr.pu1_OctetList, pFirewallFilterUpdateReq->au1DestAddr,
            DestAddr.i4_Length);
    MEMCPY (SrcPort.pu1_OctetList, pFirewallFilterUpdateReq->au1SrcPort,
            SrcPort.i4_Length);
    MEMCPY (DestPort.pu1_OctetList, pFirewallFilterUpdateReq->au1DestPort,
            DestPort.i4_Length);
    if (u2PoolStatus == ACTIVE)
    {
        if ((i4RetVal = nmhGetFwlFilterRowStatus (&FilterName, &i4RowStatus))
            != SNMP_SUCCESS)
        {
            /* ICSA Fix -S-
             * filter name should not be same as already 
             * existing acl name */
            INT4                i4IfaceIndex = 0;

            if (nmhGetFwlAclRowStatus (i4IfaceIndex, &FilterName,
                                       1, &i4RowStatus) == SNMP_SUCCESS)
            {
                APHDLR_TRC (APHDLR_FAILURE_TRC,
                            "nmhGetFwlAclRowStatus failed\r\n");
                return OSIX_FAILURE;
            }

            if (nmhGetFwlAclRowStatus (i4IfaceIndex, &FilterName,
                                       2, &i4RowStatus) == SNMP_SUCCESS)
            {
                APHDLR_TRC (APHDLR_FAILURE_TRC,
                            "nmhGetFwlAclRowStatus failed\r\n");
                return OSIX_FAILURE;
            }
            /* ICSA Fix -E- */

            if (nmhSetFwlFilterRowStatus (&FilterName, CREATE_AND_WAIT)
                != SNMP_SUCCESS)
            {
                APHDLR_TRC (APHDLR_FAILURE_TRC,
                            "nmhSetFwlFilterRowStatus failed\r\n");
                return OSIX_FAILURE;
            }
        }
        else
        {
            if (u2PoolStatus == ACTIVE)
            {
                nmhSetFwlFilterRowStatus (&FilterName, NOT_IN_SERVICE);
            }
        }

        /* Set the Address type */
        if (nmhTestv2FwlFilterAddrType (&u4Error, &FilterName, 4) !=
            SNMP_SUCCESS)
        {

            nmhSetFwlFilterRowStatus (&FilterName, DESTROY);
            APHDLR_TRC (APHDLR_FAILURE_TRC,
                        "nmhTestv2FwlFilterAddrType failed\r\n");
            return OSIX_FAILURE;
        }
        else
        {
            nmhSetFwlFilterAddrType (&FilterName, 4);
        }

        if (nmhTestv2FwlFilterSrcAddress (&u4Error, &FilterName, &SrcAddr) !=
            SNMP_SUCCESS)
        {
            if (i4RetVal != SNMP_SUCCESS)
            {
                nmhSetFwlFilterRowStatus (&FilterName, DESTROY);
            }
            else if (u2PoolStatus == ACTIVE)
            {
                nmhSetFwlFilterRowStatus (&FilterName, ACTIVE);
            }
            APHDLR_TRC (APHDLR_FAILURE_TRC,
                        "nmhTestv2FwlFilterSrcAddress failed\r\n");
            return OSIX_FAILURE;
        }

        if (nmhTestv2FwlFilterDestAddress (&u4Error, &FilterName, &DestAddr) !=
            SNMP_SUCCESS)
        {
            if (i4RetVal != SNMP_SUCCESS)
            {
                nmhSetFwlFilterRowStatus (&FilterName, DESTROY);
            }
            else if (u2PoolStatus == ACTIVE)
            {
                nmhSetFwlFilterRowStatus (&FilterName, ACTIVE);
            }
            APHDLR_TRC (APHDLR_FAILURE_TRC,
                        "nmhTestv2FwlFilterDestAddress failed\r\n");
            return OSIX_FAILURE;
        }

        if (nmhTestv2FwlFilterProtocol (&u4Error,
                                        &FilterName,
                                        (INT4) i4Proto) != SNMP_SUCCESS)
        {
            if (i4RetVal != SNMP_SUCCESS)
            {
                nmhSetFwlFilterRowStatus (&FilterName, DESTROY);
            }
            else if (i4RowStatus == ACTIVE)
            {
                nmhSetFwlFilterRowStatus (&FilterName, ACTIVE);
            }

            return OSIX_FAILURE;
        }

        /* Now update the source, destination port ranges */

        if (nmhTestv2FwlFilterSrcPort (&u4Error, &FilterName, &SrcPort) !=
            SNMP_SUCCESS)
        {
            nmhSetFwlFilterRowStatus (&FilterName, DESTROY);
            APHDLR_TRC (APHDLR_FAILURE_TRC,
                        "nmhTestv2FwlFilterSrcPort failed\r\n");
            return (OSIX_FAILURE);
        }

        if (nmhTestv2FwlFilterDestPort (&u4Error, &FilterName, &DestPort) !=
            SNMP_SUCCESS)
        {
            nmhSetFwlFilterRowStatus (&FilterName, DESTROY);
            APHDLR_TRC (APHDLR_FAILURE_TRC,
                        "nmhTestv2FwlFilterDestPort failed\r\n");
            return (OSIX_FAILURE);
        }

        /*if (nmhTestv2FwlFilterAckBit (&u4Error, &FilterName, 1 ) !=
           SNMP_SUCCESS)
           {
           PRINTF("\n\n Sk nmhTestv2FwlFilterAckBit failed \n\n");
           nmhSetFwlFilterRowStatus (&FilterName, DESTROY);
           return (OSIX_FAILURE);
           }

           THIS IS PURPOSEFULLY COMMENTED - TO BE CROSS VERIFIED WITH ELAYARAJA

           if (nmhTestv2FwlFilterRstBit (&u4Error, &FilterName, 2) !=
           SNMP_SUCCESS)
           {
           PRINTF("\n\n Sk nmhTestv2FwlFilterRstBit failed \n\n");
           nmhSetFwlFilterRowStatus (&FilterName, DESTROY);
           return (OSIX_FAILURE);
           } */

        if (nmhGetFwlFilterRowStatus (&FilterName, &i4RowStatus) !=
            SNMP_SUCCESS)
        {
            /* NOTE: this section of the code will never be entered */
            return (OSIX_FAILURE);
        }
        else
        {
            if (u2PoolStatus == ACTIVE)
            {
                nmhSetFwlFilterRowStatus (&FilterName, NOT_IN_SERVICE);
            }
        }

        nmhSetFwlFilterSrcPort (&FilterName, &SrcPort);

        nmhSetFwlFilterDestPort (&FilterName, &DestPort);

        /*nmhSetFwlFilterAckBit (&FilterName, 1);

           nmhSetFwlFilterRstBit (&FilterName, 2);

           nmhSetFwlFilterRowStatus (&FilterName, ACTIVE); */

        nmhSetFwlFilterSrcAddress (&FilterName, &SrcAddr);
        nmhSetFwlFilterDestAddress (&FilterName, &DestAddr);
        nmhSetFwlFilterProtocol (&FilterName, i4Proto);
        nmhSetFwlFilterRowStatus (&FilterName, ACTIVE);

    }
    else
    {
        i4RetVal = nmhSetFwlFilterRowStatus (&FilterName, DESTROY);
        if (i4RetVal == SNMP_FAILURE)
        {
            APHDLR_TRC (APHDLR_FAILURE_TRC,
                        "nmhSetFwlFilterRowStatus failed for destroy\r\n");
        }
    }
#else

    UNUSED_PARAM (pFirewallFilterUpdateReq);
#endif
    return OSIX_SUCCESS;
}

INT4
CapSetFirewallAcl (tFirewallAclUpdateReq * pFirewallAclUpdateReq)
{
#ifdef FIREWALL_WANTED
    tSNMP_OCTET_STRING_TYPE AclName;
    tSNMP_OCTET_STRING_TYPE FilterSet;
    UINT1               au1AclName[AP_FWL_MAX_ACL_NAME_LEN];
    UINT1               au1FilterSet[AP_FWL_MAX_FILTER_SET_LEN];
    UINT4               u4Error = 0;
    UINT4               u4NewAclFlag = 0;
    INT4                i4AclIfIndex = pFirewallAclUpdateReq->i4AclIfIndex;
    UINT1               u1AclDirection = pFirewallAclUpdateReq->u1AclDirection;
    UINT2               u2SeqNum = pFirewallAclUpdateReq->u2SeqNum;
    UINT1               u1Action = pFirewallAclUpdateReq->u1Action;
    UINT2               u2PoolStatus = pFirewallAclUpdateReq->u1EntryStatus;
    INT4                i4RetVal = 0;
    INT4                i4RowStatus = 0;

    MEMSET (au1AclName, 0, AP_FWL_MAX_ACL_NAME_LEN);
    MEMSET (au1FilterSet, 0, AP_FWL_MAX_FILTER_SET_LEN);

    AclName.pu1_OctetList = au1AclName;
    FilterSet.pu1_OctetList = au1FilterSet;

    AclName.i4_Length = STRLEN (pFirewallAclUpdateReq->au1AclName);
    FilterSet.i4_Length = STRLEN (pFirewallAclUpdateReq->au1FilterSet);
    MEMCPY (AclName.pu1_OctetList, pFirewallAclUpdateReq->au1AclName,
            AclName.i4_Length);
    MEMCPY (FilterSet.pu1_OctetList, pFirewallAclUpdateReq->au1FilterSet,
            FilterSet.i4_Length);
    if (u2PoolStatus == ACTIVE)
    {
        if ((i4RetVal = nmhGetFwlFilterRowStatus (&AclName, &i4RowStatus))
            == SNMP_SUCCESS)
        {
            APHDLR_TRC (APHDLR_FAILURE_TRC,
                        "nmhGetFwlFilterRowStatus failed \r\n");
        }
        /* ICSA Fix -E- */

        if ((i4RetVal = nmhGetFwlRuleRowStatus (&AclName, &i4RowStatus))
            != SNMP_SUCCESS)
        {
            if (nmhSetFwlRuleRowStatus (&AclName, CREATE_AND_WAIT)
                != SNMP_SUCCESS)
            {
                APHDLR_TRC (APHDLR_FAILURE_TRC,
                            "nmhSetFwlRuleRowStatus failed \r\n");

            }
            if (nmhTestv2FwlRuleFilterSet (&u4Error, &AclName, &FilterSet) !=
                SNMP_SUCCESS)
            {
                APHDLR_TRC (APHDLR_FAILURE_TRC,
                            "nmhTestv2FwlRuleFilterSet failed \r\n");
                if (i4RetVal != SNMP_SUCCESS)
                {
                    nmhSetFwlRuleRowStatus (&AclName, DESTROY);
                }

            }

            if (nmhSetFwlRuleFilterSet (&AclName, &FilterSet) != SNMP_SUCCESS)
            {
                nmhSetFwlRuleRowStatus (&AclName, DESTROY);
                APHDLR_TRC (APHDLR_FAILURE_TRC,
                            "nmhSetFwlRuleFilterSet failed \r\n");
            }
        }
        else
        {
            tSNMP_OCTET_STRING_TYPE RuleFilterSet;
            UINT1               au1RuleFilterSet[MAX_OCTETSTRING_SIZE] = { 0 };

            RuleFilterSet.i4_Length = 256;
            RuleFilterSet.pu1_OctetList = au1RuleFilterSet;

            /* Rule exists. Now get the filter set for the corresponding acl name */
            if (nmhGetFwlRuleFilterSet (&AclName, &RuleFilterSet) ==
                SNMP_SUCCESS)
            {
                RuleFilterSet.pu1_OctetList[RuleFilterSet.i4_Length] = 0;
                FilterSet.pu1_OctetList[FilterSet.i4_Length] = 0;

                /* Check if the filter set is different from the user specified one.
                 * If so dont allow the user to modify the filter set */
                if (STRCMP (RuleFilterSet.pu1_OctetList,
                            FilterSet.pu1_OctetList) != 0)
                {
                    APHDLR_TRC (APHDLR_FAILURE_TRC,
                                "Pre-existing FilterSet Can't be Modified\r\n");
                }
            }
        }
        nmhSetFwlRuleRowStatus (&AclName, ACTIVE);
        if (nmhGetFwlAclRowStatus (i4AclIfIndex, &AclName,
                                   (INT4) u1AclDirection,
                                   &i4RowStatus) != SNMP_SUCCESS)
        {
            if (nmhTestv2FwlAclRowStatus
                (&u4Error, i4AclIfIndex, &AclName, (INT4) u1AclDirection,
                 CREATE_AND_WAIT) != SNMP_SUCCESS)
            {
                nmhSetFwlRuleRowStatus (&AclName, DESTROY);
                APHDLR_TRC (APHDLR_FAILURE_TRC,
                            "nmhTestv2FwlAclRowStatus failed\r\n");
            }
            if (nmhSetFwlAclRowStatus
                (i4AclIfIndex, &AclName, (INT4) u1AclDirection,
                 CREATE_AND_WAIT) != SNMP_SUCCESS)
            {
                APHDLR_TRC (APHDLR_FAILURE_TRC,
                            "nmhSetFwlAclRowStatus failed\r\n");
            }
            u4NewAclFlag = 1;
        }
        else
        {
            if (i4RowStatus == ACTIVE)
            {
                nmhSetFwlAclRowStatus (i4AclIfIndex, &AclName,
                                       (INT4) u1AclDirection, NOT_IN_SERVICE);
            }
        }

        nmhSetFwlAclAction (i4AclIfIndex, &AclName, (INT4) u1AclDirection,
                            (INT4) u1Action);

        i4RetVal =
            nmhTestv2FwlAclSequenceNumber (&u4Error, i4AclIfIndex, &AclName,
                                           (INT4) u1AclDirection,
                                           (INT4) u2SeqNum);
        if (SNMP_FAILURE == i4RetVal)
        {
            if (1 == u4NewAclFlag)
            {
                nmhSetFwlAclRowStatus (i4AclIfIndex, &AclName,
                                       (INT4) u1AclDirection, DESTROY);
            }
            APHDLR_TRC (APHDLR_FAILURE_TRC,
                        "nmhTestv2FwlAclSequenceNumber failed\r\n");
        }

        if (nmhSetFwlAclSequenceNumber
            (i4AclIfIndex, &AclName, (INT4) u1AclDirection,
             (INT4) u2SeqNum) != SNMP_SUCCESS)
        {
            APHDLR_TRC (APHDLR_FAILURE_TRC,
                        "nmhSetFwlAclSequenceNumber failed\r\n");
        }

        nmhSetFwlAclRowStatus (i4AclIfIndex, &AclName, (INT4) u1AclDirection,
                               ACTIVE);
    }
    else
    {
        i4RetVal =
            nmhSetFwlAclRowStatus (i4AclIfIndex, &AclName,
                                   (INT4) u1AclDirection, DESTROY);
        i4RetVal = nmhSetFwlRuleRowStatus (&AclName, DESTROY);
        if (i4RetVal == SNMP_FAILURE)
        {
            APHDLR_TRC (APHDLR_FAILURE_TRC,
                        "nmhSetFwlAclRowStatus failed \r\n");
        }
    }
#else
    UNUSED_PARAM (pFirewallAclUpdateReq);
#endif
    return OSIX_SUCCESS;
}

/***********************************************************************
*  Function Name   : CapSetDiffServ                                    *
*  Description     : This function invoke the API to set DSCP value in *
*                    kernel for wireless interface                     *
*  Input(s)        : Pointer to pVendorSpecDiffServ                    *
*  Output(s)       : None                                              *
*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         *
************************************************************************/
INT4
CapSetDiffServ (tVendorSpecDiffServ * pVendorSpecDiffServ, UINT1 u1WlanId)
{
    tCapwapDscpMapTable CapDscpMap;
    UINT1               u1InterfaceName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               u1RadioId = pVendorSpecDiffServ->u1RadioId;

    MEMSET (u1InterfaceName, 0, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (&CapDscpMap, 0, sizeof (tCapwapDscpMapTable));

    if (u1WlanId != 0)
    {
        WssWlanGetBrIfName (u1RadioId, u1WlanId, u1InterfaceName);
        MEMCPY (CapDscpMap.au1InterfaceName, u1InterfaceName,
                CFA_MAX_PORT_NAME_LENGTH);
    }
    else
    {
        /* Get ethernet interface name */
    }

    CapDscpMap.u1InPriority = pVendorSpecDiffServ->u1InPriority;
    CapDscpMap.u1OutDscp = pVendorSpecDiffServ->u1OutDscp;
#ifdef KERNEL_CAPWAP_WANTED
    if (pVendorSpecDiffServ->u1EntryStatus == 1)
    {
        WssQosUpdateKernelDB (&CapDscpMap);
    }
    else
    {
        WssQosRemoveKernelDB (&CapDscpMap);
    }
#endif
    return OSIX_SUCCESS;
}

/***********************************************************************
*  Function Name   : CapSetCapwDiffServ                                *
*  Description     : This function invoke the API to set DSCP value in *
*                    kernel for ethernet interface                     *
*  Input(s)        : Pointer to pVendorSpecDiffServ                    *
*  Output(s)       : None                                              *
*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         *
************************************************************************/
INT4
CapSetCapwDiffServ (tVendorCapwSpecDiffServ * pVendorSpecDiffServ)
{
    tCapwapDscpMapTable CapDscpMap;
    UINT1               au1InterfaceName[CFA_MAX_PORT_NAME_LENGTH];

    MEMSET (au1InterfaceName, 0, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (&CapDscpMap, 0, sizeof (tCapwapDscpMapTable));

    CapDscpMap.u1InPriority = pVendorSpecDiffServ->u1InPriority;
    CapDscpMap.u1OutDscp = pVendorSpecDiffServ->u1OutDscp;

#ifdef KERNEL_CAPWAP_WANTED
    if (pVendorSpecDiffServ->u1EntryStatus == 1)
    {
        WssQosUpdateKernelDB (&CapDscpMap);
    }
    else
    {
        WssQosRemoveKernelDB (&CapDscpMap);
    }
#endif
    return OSIX_SUCCESS;
}

/***********************************************************************
*  Function Name   : CapCreateL3SubIf                                   *
*  Description     : Create L3 SUB If on AP                            *
*  Input(s)        : pL3SubIfEntry - l3SubIF struct,                    *
*                     i2Index - Index of the interface                    *
*  Output(s)       : None                                              *
*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         *
************************************************************************/
INT4
CapCreateL3SubIf (tL3SubIfUpdateReq * pL3SubIfEntry, INT2 i2Index)
{
    UINT4               u4IfIndex = 0;
    UINT4               u4L2CxtId = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4PrevIpAddr = 0;
    UINT4               u4PrevSubnetMask = 0;
    UINT4               u4BcastAddr = 0;
    UINT1               pu1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT4               u4IpAddr = 0;
    UINT4               u4SubnetMask = 0;
    INT4                i4AdminStatus = 0;
    INT4                i4NwType = 0;
    INT4                i4Dot1qVlanStaticRowStatus = 0;
    tSNMP_OCTET_STRING_TYPE Alias;

    MEMSET (pu1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);

    /* Get interface Alias from Port and Vlan Id */
    SPRINTF ((CHR1 *) pu1IfName, "Slot0/%d.%d",
             pL3SubIfEntry->u4PhyPort[i2Index],
             pL3SubIfEntry->u2VlanId[i2Index]);
    u4IpAddr = pL3SubIfEntry->u4IpAddr[i2Index];
    u4SubnetMask = pL3SubIfEntry->u4SubnetMask[i2Index];

    if (pL3SubIfEntry->u1EntryStatus == ACTIVE)
    {
        /* Check If the Interface is already exists */
        if (CfaGetInterfaceIndexFromName (pu1IfName, &u4IfIndex) !=
            OSIX_SUCCESS)
        {
            /* Check whether VLAN is already associated with L3 Interface VLAN */
            if (CfaGetVlanInterfaceIndex
                ((UINT4) pL3SubIfEntry->u2VlanId[i2Index]) != CFA_INVALID_INDEX)
            {
                APHDLR_TRC (APHDLR_FAILURE_TRC,
                            "VLAN ID is associated with L3 Interface\r\n");
                return OSIX_FAILURE;
            }

            /* Create VLAN and assign the member port */
            if (CapCreateL2Vlan (pL3SubIfEntry->u4PhyPort[i2Index],
                                 pL3SubIfEntry->u2VlanId[i2Index]) !=
                OSIX_SUCCESS)
            {
                APHDLR_TRC1 (APHDLR_FAILURE_TRC,
                             "Failed to create VLAN : %d.\r\n",
                             pL3SubIfEntry->u2VlanId[i2Index]);
                return OSIX_FAILURE;
            }

            /* Create L3 SUB Iface */
            Alias.i4_Length = (INT4) STRLEN (pu1IfName);
            Alias.pu1_OctetList = pu1IfName;

            /* Get Interface Index and create new interface */
            if (CfaGetFreeInterfaceIndex (&u4IfIndex, CFA_L3SUB_INTF) !=
                OSIX_SUCCESS)
            {
                APHDLR_TRC (APHDLR_FAILURE_TRC,
                            "No L3 free indices are available.\r\n");
                return OSIX_FAILURE;
            }

            if (nmhSetIfMainRowStatus (u4IfIndex, CREATE_AND_WAIT) ==
                SNMP_FAILURE)
            {
                APHDLR_TRC (APHDLR_FAILURE_TRC,
                            "Failed to set L3 SUB IF Row status \r\n");
                return OSIX_FAILURE;
            }

            /* Set the l2 context Id */
            if (VcmTestL2CxtId (&u4ErrCode, u4IfIndex, u4L2CxtId) ==
                VCM_FAILURE)
            {
                VcmCfaDeleteInterfaceMapping (u4IfIndex);
                nmhSetIfMainRowStatus (u4IfIndex, DESTROY);
                APHDLR_TRC (APHDLR_FAILURE_TRC,
                            "Failed to set L3 SUB IF Row status \r\n");
                return OSIX_FAILURE;
            }

            if (VcmSetL2CxtId (u4IfIndex, u4L2CxtId) == VCM_FAILURE)
            {
                VcmCfaDeleteInterfaceMapping (u4IfIndex);
                nmhSetIfMainRowStatus (u4IfIndex, DESTROY);
                APHDLR_TRC (APHDLR_FAILURE_TRC,
                            "Failed to set L3 SUB IF Row status \r\n");
                return OSIX_FAILURE;
            }

            /* We can directly call set routine, because validation is done already
             * in the function CfaCreateInterface() */
            nmhSetIfAlias (u4IfIndex, &Alias);

            /* Set the Type */
            if (nmhTestv2IfMainType
                (&u4ErrCode, (INT4) u4IfIndex, CFA_L3SUB_INTF) == SNMP_FAILURE)
            {
                VcmCfaDeleteIPIfaceMapping (u4IfIndex, CFA_TRUE);
                VcmCfaDeleteInterfaceMapping (u4IfIndex);
                nmhSetIfMainRowStatus (u4IfIndex, DESTROY);
                APHDLR_TRC (APHDLR_FAILURE_TRC,
                            "test validation failed for L3 SUB IF MainType \r\n");
                return OSIX_FAILURE;
            }

            if (nmhSetIfMainType ((INT4) u4IfIndex, CFA_L3SUB_INTF) ==
                SNMP_FAILURE)
            {
                VcmCfaDeleteInterfaceMapping (u4IfIndex);
                nmhSetIfMainRowStatus (u4IfIndex, DESTROY);
                APHDLR_TRC (APHDLR_FAILURE_TRC,
                            "Failed to set L3 SUB IF MainType \r\n");
                return OSIX_FAILURE;
            }
            nmhSetIfMainRowStatus (u4IfIndex, ACTIVE);

            /* Set Encapsualtion Vlan Id */
            if (nmhTestv2IfMainEncapDot1qVlanId
                (&u4ErrCode, u4IfIndex,
                 (INT4) pL3SubIfEntry->u2VlanId[i2Index]) == SNMP_FAILURE)
            {
                APHDLR_TRC (APHDLR_FAILURE_TRC,
                            "Test validation failed for L3 Encap vlan \r\n");
                return OSIX_FAILURE;
            }

            if (nmhSetIfMainEncapDot1qVlanId
                (u4IfIndex,
                 (INT4) pL3SubIfEntry->u2VlanId[i2Index]) == SNMP_FAILURE)
            {
                APHDLR_TRC (APHDLR_FAILURE_TRC,
                            "Failed to set L3 SUB IF encap vlan \r\n");
                return OSIX_FAILURE;
            }

            APHDLR_TRC (APHDLR_FAILURE_TRC,
                        "L3SUB IFace is created successfully \r\n");
        }

        /* Update Network Type */
        if (nmhGetIfMainNetworkType (u4IfIndex, &i4NwType) != SNMP_FAILURE)
        {
            if (i4NwType != (INT4) pL3SubIfEntry->u1IfNwType[i2Index])
            {
                if (nmhTestv2IfMainNetworkType
                    (&u4ErrCode, (INT4) u4IfIndex,
                     (INT4) pL3SubIfEntry->u1IfNwType[i2Index]) == SNMP_FAILURE)
                {
                    APHDLR_TRC (APHDLR_FAILURE_TRC,
                                "Failed to set network type \r\n");
                    return OSIX_FAILURE;
                }

                if (nmhSetIfMainNetworkType
                    (u4IfIndex,
                     (INT4) pL3SubIfEntry->u1IfNwType[i2Index]) == SNMP_FAILURE)
                {
                    APHDLR_TRC (APHDLR_FAILURE_TRC,
                                "Failed to set network type \r\n");
                    return OSIX_FAILURE;
                }
            }
        }

        if ((nmhGetIfIpAddr ((INT4) u4IfIndex, &u4PrevIpAddr) != SNMP_FAILURE)
            && (nmhGetIfIpSubnetMask ((INT4) u4IfIndex, &u4PrevSubnetMask) !=
                SNMP_FAILURE))
        {
            if ((u4PrevIpAddr != u4IpAddr)
                && (u4PrevSubnetMask != u4SubnetMask))
            {
                /* Update Interface IP Address */
                if (nmhTestv2IfIpAddr (&u4ErrCode, (INT4) u4IfIndex, u4IpAddr)
                    == SNMP_FAILURE)
                {
                    APHDLR_TRC (APHDLR_FAILURE_TRC,
                                "Invalid IP ADDRESS for l3Sub Iface \r\n");
                    return OSIX_FAILURE;
                }

                /* Test Ip addr and Sub net mask */
                if (CfaTestIfIpAddrAndIfIpSubnetMask
                    ((INT4) u4IfIndex, u4IpAddr, u4SubnetMask) == CLI_FAILURE)
                {
                    APHDLR_TRC (APHDLR_FAILURE_TRC,
                                "Invalid IP ADDRESS for l3Sub Iface \r\n");
                    return OSIX_FAILURE;
                }

                nmhGetIfIpAddr ((INT4) u4IfIndex, &u4PrevIpAddr);

                if (nmhSetIfIpAddr (u4IfIndex, u4IpAddr) == SNMP_FAILURE)
                {
                    APHDLR_TRC (APHDLR_FAILURE_TRC,
                                "Invalid IP ADDRESS for l3Sub Iface \r\n");
                    return OSIX_FAILURE;
                }

                nmhGetIfIpSubnetMask ((INT4) u4IfIndex, &u4PrevSubnetMask);

                if (nmhSetIfIpSubnetMask (u4IfIndex, u4SubnetMask) ==
                    SNMP_FAILURE)
                {
                    nmhSetIfIpAddr (u4IfIndex, u4PrevIpAddr);
                    APHDLR_TRC (APHDLR_FAILURE_TRC,
                                "Invalid subnet mask for l3Sub Iface \r\n");
                    return OSIX_FAILURE;
                }

                u4BcastAddr = (u4IpAddr) | (~(u4SubnetMask));

                if (nmhTestv2IfIpBroadcastAddr (&u4ErrCode, (INT4) u4IfIndex,
                                                u4BcastAddr) == SNMP_FAILURE)
                {
                    nmhSetIfIpAddr (u4IfIndex, u4PrevIpAddr);
                    nmhSetIfIpSubnetMask (u4IfIndex, u4PrevSubnetMask);
                    APHDLR_TRC (APHDLR_FAILURE_TRC,
                                "Invalid broadcast address for l3Sub Iface \r\n");
                    return OSIX_FAILURE;
                }

                if (nmhSetIfIpBroadcastAddr (u4IfIndex, u4BcastAddr) ==
                    SNMP_FAILURE)
                {
                    APHDLR_TRC (APHDLR_FAILURE_TRC,
                                "Failed to set broadcast address for l3Sub Iface \r\n");
                    return OSIX_FAILURE;
                }
            }
        }

        /* Update Interface Admin Status */
        if (nmhGetIfAdminStatus ((INT4) u4IfIndex, &i4AdminStatus) !=
            SNMP_FAILURE)
        {
            if (i4AdminStatus != (INT4) pL3SubIfEntry->u1IfAdminStatus[i2Index])
            {
                if (nmhTestv2IfAdminStatus
                    (&u4ErrCode, (INT4) u4IfIndex,
                     (INT4) pL3SubIfEntry->u1IfAdminStatus[i2Index]) ==
                    SNMP_FAILURE)
                {
                    APHDLR_TRC (APHDLR_FAILURE_TRC,
                                "Failed to set admin status of L3 SUB IFace\r\n");
                    return OSIX_FAILURE;
                }
                if (nmhSetIfAdminStatus
                    (u4IfIndex,
                     (INT4) pL3SubIfEntry->u1IfAdminStatus[i2Index]) ==
                    SNMP_FAILURE)
                {
                    APHDLR_TRC (APHDLR_FAILURE_TRC,
                                "Failed to set admin status of L3 SUB IFace\r\n");
                    return OSIX_FAILURE;
                }
            }
        }
    }
    else
    {
        /* Delete the interface */
        /* Check If the Interface is exists */
        if (CfaGetInterfaceIndexFromName (pu1IfName, &u4IfIndex) !=
            OSIX_SUCCESS)
        {
            APHDLR_TRC (APHDLR_FAILURE_TRC, "Invalid Interface \r\n");
            return OSIX_FAILURE;
        }

        if (nmhTestv2IfMainRowStatus (&u4ErrCode, (INT4) u4IfIndex,
                                      (INT4) DESTROY) == SNMP_FAILURE)
        {
            APHDLR_TRC (APHDLR_FAILURE_TRC,
                        "Failed to delete l3Sub Iface \r\n");
            return OSIX_FAILURE;
        }
        nmhSetIfMainRowStatus (u4IfIndex, DESTROY);

        VlanLock ();

        /* Delete VLAN */
        if ((nmhGetDot1qVlanStaticRowStatus
             ((INT4) pL3SubIfEntry->u2VlanId[i2Index],
              &i4Dot1qVlanStaticRowStatus)) == SNMP_SUCCESS)
        {
            if (nmhTestv2Dot1qVlanStaticRowStatus
                (&u4ErrCode, (INT4) pL3SubIfEntry->u2VlanId[i2Index],
                 DESTROY) == SNMP_FAILURE)
            {
                VlanUnLock ();
                return OSIX_FAILURE;
            }

            /* Delete VLAN */
            if (nmhSetDot1qVlanStaticRowStatus
                ((INT4) pL3SubIfEntry->u2VlanId[i2Index],
                 DESTROY) == SNMP_FAILURE)
            {
                VlanUnLock ();
                APHDLR_TRC (APHDLR_FAILURE_TRC, "Failed to delete l2 VLAN\r\n");
                return OSIX_FAILURE;
            }
        }
        VlanUnLock ();
    }
    return OSIX_SUCCESS;
}

/***********************************************************************
*  Function Name   : CapCreateL2Vlan                           *
*  Description     : Create L2VLAN  on AP                            *
*  Input(s)        : u4PhyPort - Phy Port As member port           *
*                    u4VlanId - Vlan ID to be created                    *
*  Output(s)       : None                                              *
*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         *
************************************************************************/
INT4
CapCreateL2Vlan (UINT4 u4PhyPort, UINT4 u4VlanId)
{
    INT4                i4Dot1qVlanStaticRowStatus = 0;
    UINT1              *pau1LocalMemberPorts = NULL;
    UINT4               u4ContextId = 0;
    UINT4               u4ErrCode = 0;
    tPortList          *pMemberPorts = NULL;
    tSNMP_OCTET_STRING_TYPE EgressPorts;

    /* Considering  GI_ENET for AP. This will get changed based on the interface type */
    UINT1               u1IfName[CFA_MAX_PORT_NAME_LENGTH] = "gigabitethernet ";
    UINT1               u1IfList[CFA_MAX_PORT_NAME_LENGTH];

    MEMSET (u1IfList, 0, CFA_MAX_PORT_NAME_LENGTH);

    /* Considering Slot 0 alone. This will get changed if more slots are added */
    SPRINTF ((CHR1 *) u1IfList, "0/%d", u4PhyPort);

    /* Select VLAN Default Context */
    VlanLock ();
    if ((nmhGetDot1qVlanStaticRowStatus ((INT4) u4VlanId,
                                         &i4Dot1qVlanStaticRowStatus))
        != SNMP_SUCCESS)
    {
        /* VLAN NOT present - CREATE */
        if (nmhTestv2Dot1qVlanStaticRowStatus (&u4ErrCode, (INT4) u4VlanId,
                                               CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            VlanUnLock ();
            return OSIX_FAILURE;
        }

        /* CREATE VLAN */
        if (nmhSetDot1qVlanStaticRowStatus ((INT4) u4VlanId, CREATE_AND_WAIT)
            == SNMP_FAILURE)
        {
            VlanUnLock ();
            return OSIX_FAILURE;
        }

        /* Set Member Ports for VLAN */
        pMemberPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

        if (pMemberPorts == NULL)
        {
            APHDLR_TRC (APHDLR_FAILURE_TRC,
                        "\r%% Error in Allocating memory for bitlist \r\n");
            VlanUnLock ();
            return OSIX_FAILURE;
        }
        MEMSET (pMemberPorts, 0, sizeof (tPortList));

        if ((CfaCliGetIfList ((INT1 *) u1IfName, (INT1 *) u1IfList,
                              *pMemberPorts,
                              sizeof (tPortList))) == CLI_FAILURE)
        {
            APHDLR_TRC (APHDLR_FAILURE_TRC,
                        "\r%% Invalid Member" " Port(s) \r\n");
            VlanUnLock ();
            FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
            return OSIX_FAILURE;
        }

        pau1LocalMemberPorts =
            UtilPlstAllocLocalPortList (CONTEXT_PORT_LIST_SIZE);
        if (pau1LocalMemberPorts == NULL)
        {
            APHDLR_TRC (APHDLR_FAILURE_TRC,
                        " Error in allocating memory "
                        "for pau1LocalMemberPorts\r\n");
            VlanUnLock ();
            FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
            return OSIX_FAILURE;
        }
        MEMSET (pau1LocalMemberPorts, 0, CONTEXT_PORT_LIST_SIZE);

        if (VlanConvertToLocalPortList (*pMemberPorts,
                                        pau1LocalMemberPorts) != VLAN_SUCCESS)
        {
            APHDLR_TRC (APHDLR_FAILURE_TRC,
                        "Some ports in this portlist are "
                        "not mapped to this context or part of "
                        "port-channel.\r\n");
            FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
            UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
            VlanUnLock ();
            return OSIX_FAILURE;
        }
        EgressPorts.i4_Length = VLAN_PORT_LIST_SIZE;
        EgressPorts.pu1_OctetList = pau1LocalMemberPorts;

        /* Added for pseudo wire visibility */
        if (VlanUtilIsPortListValid (u4ContextId, EgressPorts.pu1_OctetList,
                                     EgressPorts.i4_Length) != VLAN_TRUE)
        {
            APHDLR_TRC (APHDLR_FAILURE_TRC,
                        "\r Unknown Member Port(s) present\r\n");
            FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
            UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
            VlanUnLock ();
            return OSIX_FAILURE;
        }

        if (nmhSetDot1qVlanStaticEgressPorts (u4VlanId, &EgressPorts)
            == SNMP_FAILURE)
        {
            APHDLR_TRC (APHDLR_FAILURE_TRC, "Failed to set member port \r\n");
            FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
            UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
            VlanUnLock ();
            return OSIX_FAILURE;
        }

        if (nmhSetDot1qVlanStaticRowStatus ((INT4) u4VlanId, ACTIVE)
            == SNMP_FAILURE)
        {
            APHDLR_TRC (APHDLR_FAILURE_TRC, "Failed to active VLAN  \r\n");
            FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
            UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
            VlanUnLock ();
            return OSIX_FAILURE;
        }
    }
    VlanUnLock ();
    return OSIX_SUCCESS;
}
#endif
