/******************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 *  $Id: aphdlrconst.h,v 1.3 2017/05/23 14:16:47 siva Exp $
 *
 * Description: This file contains APHDLR Constants
 *******************************************************************/

#ifndef _APHDLR_CONST_H_
#define _APHDLR_CONST_H_

#define APHDLR_MAX_PKT_LEN         CAPWAP_MAX_PKT_LEN
#define MAX_APHDLR_PKTBUF          10
#define MAX_APHDLR_PKT_QUE_SIZE    512
#define APHDLR_QUE_DEPTH           100
#define APHDLR_MAX_DATA_TRANSFER_PKT_LEN 6000
#define MAX_APHDLR_DATA_TRANSFER_PKTBUF  10
#define APHDLR_IDLE_INTERVAL_TMR_VAL 100

#ifdef HOSTAPD_WANTED
#define APHDLR_HOSTAPD_INTERVAL_TMR_VAL 30
#endif

#define APHDLR_UNUSED_CODE         0
#define PMF_TWO_BYTES     2
#define APHDLR_MSG_ELEM_FLAG_LEN   3

/* Config Update response - Mandatory */
#define  RESULT_CODE_CONF_UPDATE_RESP_INDEX 0

/* Config Update response - Optional */

/* Echo request - Mandatory */
#define  VENDOR_SPECIFIC_PAYLOAD_ECHO_REQ_INDEX 0

/* Echo response - Mandatory */
#define  VENDOR_SPECIFIC_PAYLOAD_ECHO_RESP_INDEX 0

/* Reset request - Mandatory */
#define  IMAGE_IDENTIFIER_RESET_REQ_INDEX 0 
#define VENDOR_SPECIFIC_PAYLOAD_RESET_REQ_INDEX 1

/* Reset response - Mandatory */
#define  RESULT_CODE_RESET_RESP_INDEX 0  
#define  IMAGE_IDENTIFIER_RESET_RESP_INDEX 1

/* Config Station request - Mandatory */
#define  ADD_STATION_CONF_STATION_REQ_INDEX 0
#define  DELETE_STATION_CONF_STATION_REQ_INDEX 1
#define  VENDOR_SPECIFIC_PAYLOAD_CONF_STATION_REQ_INDEX 2

/* Config Station response - Mandatory */
#define  RESULT_CODE_CONF_STATION_RESP_INDEX 0
#define  VENDOR_SPECIFIC_CONF_STATION_RESP_INDEX 1

/* Data request - Mandatory */
#define  VENDOR_SPECIFIC_PAYLOAD_DATA_REQ_INDEX 1
#define  DATA_TRANSFER_MODE_REQ_INDEX 0
#define  DATA_TRANSFER_DATA_REQ_INDEX 0

/* Data response - Mandatory */
#define  VENDOR_SPECIFIC_PAYLOAD_DATA_RESP_INDEX 1
#define  RESULT_CODE_DATA_RESP_INDEX 0

/* Clear Config request - Mandatory */
#define  VENDOR_SPECIFIC_CLEAR_CONFIG_REQ_INDEX 0

/* Clear Config response - Mandatory */
#define  RESULT_CODE_CLEAR_CONFIG_RESP_INDEX 0
#define  VENDOR_SPECIFIC_PAYLOAD_CLEAR_CONFIG_RESP_INDEX 1

/* WTP Event request - Mandatory */
#define VENDOR_SPECIFIC_PAYLOAD_WTP_EVENT_REQ_INDEX 3
/* #define DELETE_STATION_WTP_EVENT_REQ_INDEX 0 */
#define WTP_REBOOT_STATS_WTP_EVENT_REQ_INDEX 1
#define DESC_ERR_REPORT_WTP_EVENT_REQ_INDEX 3
#define RADIO_STATS_WTP_EVENT_REQ_INDEX 0
#define DUP_IPV4_ADDR_WTP_EVENT_REQ_INDEX 5
#define DUP_IPV6_ADDR_WTP_EVENT_REQ_INDEX 6
#define DOT11_STATISTICS_INDEX 2

/* WTP Event response - Mandatory */
#define VENDOR_SPECIFIC_WTP_EVENT_RESP_INDEX 0

#define CRASHDUMP "/tmp"
#define MEMORYDUMP "/tmp" 
#define CORE_FILE_PREFIX "core"

#endif

