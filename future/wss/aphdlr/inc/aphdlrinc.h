/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 * $Id: aphdlrinc.h,v 1.1.1.1 2016/09/20 10:42:38 siva Exp $
 * Description: This file contains header files included in
 *              AP HDLR module.
 *********************************************************************/
#ifndef _APHDLRINC_H__
#define _APHDLRINC_H__

#include "lr.h"
#include "cfa.h"
#include "ipv6.h"
#include "tcp.h"
#include "cli.h"
#include "trace.h"
#include "fssocket.h"
#include "fssyslog.h"
#include "utilrand.h"
#include "radioif.h"

#include "wsswlan.h"

#include "capwap.h"

#include "aphdlr.h"
#include "capwapinc.h"
#include "wssifinc.h"
#include "aphdlrmacr.h"
#include "aphdlrtdfs.h"
#include "aphdlrprot.h"
#include "aphdlrtrc.h"
#include "aphdlrdef.h"
#include "aphdlrconst.h"
#include "aphdlrsz.h"
#include "radioifproto.h"
#endif

