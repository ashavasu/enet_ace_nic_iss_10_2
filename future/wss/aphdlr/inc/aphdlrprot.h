/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 * $Id: aphdlrprot.h,v 1.3 2017/11/24 10:37:00 siva Exp $
 * Description: This file includes the and prototypes for APHDLR Module
 *******************************************************************/
#ifndef __APHDLPROT_H__
#define __APHDLPROT_H__
INT4
ApHdlrProcessWtpEventReq (UINT1 MsgType, unApHdlrMsgStruct *pWssMsgStruct);

INT4
ApHdlrProcessRadioIfChnageStateEventReq (
        tRadioIfChangeStateEventReq  *pRadioChangeStateEvtReq);
INT4 ApHdlrProcessPrimaryDiscoveryReq(unApHdlrMsgStruct *pApHdlrMsgStruct);
INT4 ApHdlrEnqueCtrlPkts (unApHdlrMsgStruct *);
INT4 CapwapGetDataTransferMode(tDataTransferMode *, UINT4 *);
INT4 CapwapGetDataTransferData(tDataTransferData *, UINT4 *);
VOID tmrAphdlrCallback (tTimerListId timerListId);
INT4 CapwapGetCrashFileName (CHR1 *);
INT4 CapwapGetCrashMemoryDataInfo (tRemoteSessionManager *, UINT1);
INT4 CapwapGetWtpDataTransferResResultCode(tResCode *, UINT4 *, tRemoteSessionManager *, UINT1);
INT4 CapwapGetWtpDataTransferRspMsgElements(tDataRsp *, UINT4 *, tRemoteSessionManager *, UINT1);
INT4 CapwapGetCrashDataSize (tRemoteSessionManager *, CHR1 *);
INT4 CapwapGetWtpDataTransferReqMsgElements(tDataReq *, tRemoteSessionManager *, UINT1);
INT4 CapwapWtpGetDataTransferData (tDataTransferData *, tRemoteSessionManager *, UINT1);
INT4 CapwapWtpGetCrashData (tDataTransferData *, UINT4 *, tRemoteSessionManager *);
INT4 CapwapWtpReadCrashData(UINT1 *, tRemoteSessionManager  *);
VOID CapwapClearDataTransferSessionEntries(tRemoteSessionManager *);
VOID CapwapClearDataTransferSessioEntries (tRemoteSessionManager *pSessEntry);
UINT1 CfaGddClearInterface(UINT1 u1RadioId,UINT1 u1WlanId,UINT1 *pu1InterfaceName);

UINT4 ApHdlrMemInit (VOID);
VOID ApHdlrMemClear (VOID);
INT4 ApHdlrProcessCtrlRxMsg (VOID);
INT4 ApHdlrProcessDataRxMsg (VOID);
INT4 ApHdlrProcessDataTxMsg (VOID);
VOID ApHdlrCheckRelinquishCounters(VOID);
INT4 ApHdlrTmrInit(VOID);
VOID ApHdlrTmrInitTmrDesc (VOID);
VOID ApHdlrTmrExpHandler (VOID);
INT4 ApHdlrTmrStart (tRemoteSessionManager *, UINT1, UINT4);
INT4 ApHdlrTmrStop (tRemoteSessionManager *, UINT1 );
VOID ApHdlrRetransmitIntervalTmrExp(VOID *pArg);
VOID ApHdlrStatsIntervalTmrExp(VOID *pArg);
VOID ApHdlrIdleIntervalTmrExp(VOID *pArg);
VOID ApHdlrCPURelinquishTmrExp(VOID *pArg);
INT4 AphdlrFreeTransmittedPacket (tRemoteSessionManager *pSessEntry);

#ifdef HOSTAPD_WANTED
VOID ApHdlrHostapdIntervalTmrExp (VOID *pArg);
#endif
#ifdef WPS_WTP_WANTED
VOID ApHdlrWpsPbcTmrExp(VOID *pArg);
#endif
INT4 CapwapValidateAddStation(UINT1 *, tCapwapControlPacket *,
                              tAddstation *, UINT2);
INT4 CapwapValidateDeleteStation(UINT1 *, tCapwapControlPacket *,
                                 tDeletestation *, UINT2);
INT4 ApHdlrEnterReset(VOID);
INT4 CapwapClearWtpConfig(VOID);
INT4 CapwapGetIeeeAssignedWtpBssID (tWssWlanRsp *, UINT1 , UINT1);
INT4 CapValidateWlanConfReqMsgElem(UINT1 * ,tCapwapControlPacket * ,
                                   tWssWlanConfigRsp *);

INT4 CapwapConstructConfigStationReq (tConfigStatusReq *);
INT4 CapwapConstructClearConfigReq (tClearconfigReq *);
INT4 CapwapConstructResetRequest (tResetReq *);
INT4 CapwapConstructWtpEventRequest(tWtpEveReq *);
INT4 CapwapConstructConfigUpdateReq (tConfigUpdateReq *);
INT4 CapwapConstructDataTransRequest (tDataReq *);
INT4 CapwapConstructEchoReq (tEchoReq *);


INT4 CapValidateConfStatReqMsgElems(UINT1 *,tCapwapControlPacket *, 
        tStationConfRsp *);
INT4 CapValidateConfStatRespMsgElems(UINT1 *,tCapwapControlPacket *);
INT4 CapValidateConfUpdateReqMsgElem(UINT1 *,tCapwapControlPacket *, 
        tConfigUpdateRsp *);
INT4 CapValidateConfUpdateRespMsgElem(UINT1 *,tCapwapControlPacket *);
INT4 CapValidateDataTransReqMsgElems (UINT1 *,tCapwapControlPacket *);
INT4 CapValidateDataTransRespMsgElems(UINT1 *,tCapwapControlPacket *);
INT4 CapValidateClearConfReqMsgElems(UINT1 *,tCapwapControlPacket *);
INT4 CapValidateClearConfRespMsgElems(UINT1 *,tCapwapControlPacket *);
INT4 capwapProcessWtpEventRequest (tSegment *,tCapwapControlPacket *, 
        tRemoteSessionManager  *);
INT4 CapValidateResetReqMsgElems (UINT1 *, tCapwapControlPacket *);
INT4 CapValidateResetRespMsgElems (UINT1 *, tCapwapControlPacket *);
INT4 CapValidateWtpEventReqMsgElems(UINT1 *,tCapwapControlPacket *);
INT4 CapValidateWtpEventRespMsgElems(UINT1 *,tCapwapControlPacket *);
INT4 test_capwapAssembleConfigStationRequest (VOID);
INT4 WtpEnterConfigStationState (VOID);
INT4 WtpEnterConfigUpdateState (VOID);
INT4 WtpEnterResetState (VOID);
INT4 WtpEnterDataTransferState (VOID);
INT4 WtpEnterClearConfigState (VOID);
INT4 WtpEnterWtpEventRequestState (VOID);
INT4 WtpEnterEchoState (VOID);
INT4
capwapValidateAddStation(UINT1 *,
        tCapwapControlPacket *,
        UINT2);
INT4
capwapValidateDeleteStation(UINT1 *,
        tCapwapControlPacket *,
        UINT2);

INT4 capwapGetAddStation (tAddstation *, UINT4 *);
INT4 capwapGetDeleteStation (tDeletestation *, UINT4 *);
INT4 capwapGetDataTransferMode (tDataTransferMode *, UINT4 *);
INT4 capwapGetDataTransferData (tDataTransferData *, UINT4 *);
INT4 capwapGetDecryptErrReport(tDecryptErrReport  *,UINT4 *);
INT4 ApHdlrMainTaskLock (VOID);
INT4 ApHdlrMainTaskUnLock (VOID);

INT4
CapSetDiffServ (tVendorSpecDiffServ *pVendorSpecDiffServ, UINT1 u1WlanId);
INT4
CapSetIpRoute (tRouteTableUpdateReq *pRouteTableUpdateReq, INT2 i2Index);
INT4
CapSetFirewallFilter (tFirewallFilterUpdateReq *pFirewallFilterUpdateReq);
INT4
CapSetFirewallAcl (tFirewallAclUpdateReq *pFirewallAclUpdateReq);
INT4
CapSetNatStatus (tNatUpdateReq *pNatUpdateReq);
INT4
CapSetFirewallStatus (tFireWallUpdateReq *pFireWallUpdateReq);
INT4
CapSetDhcpRelay(tDhcpRelayConfigUpdateReq *pDhcpRelayConfigUpdateReq);
INT4
CapSetDhcpSrv(tDhcpPoolConfigUpdateReq *pDhcpPoolConfigUpdateReq,INT2 index);
INT4
CapSetWlanIpAddr (UINT4 u4IpAddr,UINT4 u4SubnetMask, UINT1 u1RadioId, UINT1 u1WlanId);
INT4
CapCreateL2Vlan (UINT4 u4VlanId, UINT4 u4PhyPort);
INT4
CapCreateL3SubIf (tL3SubIfUpdateReq *pL3SubIfEntry, INT2 i2Index);
INT4
CapwapSetDhcpRelay(INT4 i4Status);
INT4
CapwapFwlStatusSet(INT4 );
INT4
CapwapSetDhcpRelayNxtSrvIp(UINT4); 
INT4
CapSetCapwDiffServ (tVendorCapwSpecDiffServ *pVendorSpecDiffServ);

#ifdef BAND_SELECT_WANTED
VOID
ApHdlrStaProbeExp (VOID *pArg);
INT4
ApHdlrProbeTmrStart (tWssStaBandSteerDB * pWssStaBandSteerDB, UINT1);
INT4
ApHdlrProbeTmrStop (tWssStaBandSteerDB * pWssStaBandSteerDB);
#endif
#endif
