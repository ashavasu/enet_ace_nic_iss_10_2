/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 * $Id: aphdlrmacr.h,v 1.1.1.1 2016/09/20 10:42:38 siva Exp $
 * Description: This file contains the Macros for APHDLR Module
 *******************************************************************/

#ifndef _APHDLRMACR_H__
#define _APHDLRMACR_H__

#define APHDLR_PKTBUF_POOLID     \
    APHDLRMemPoolIds[MAX_APHDLR_PKTBUF_SIZING_ID]
#define APHDLR_DATA_TRANSFER_PKTBUF_POOLID  \
    APHDLRMemPoolIds[MAX_APHDLR_DATA_TRANSFER_PKTBUF_SIZING_ID]
#define APHDLR_QUEUE_POOLID      \
    APHDLRMemPoolIds[MAX_APHDLR_PKT_QUE_SIZE_SIZING_ID]

#define  APHDLR_PKTBUF_ALLOC_MEM_BLOCK(pu1Block)\
    (pu1Block = \
     (UINT1 *)(MemAllocMemBlk (APHDLR_PKTBUF_POOLID)))
#define  APHDLR_PKTBUF_DATA_TRANSFER_ALLOC_MEM_BLOCK(pu1Block)\
         (pu1Block = \
          (UINT1 *)(MemAllocMemBlk (APHDLR_DATA_TRANSFER_PKTBUF_POOLID)))

#define APHDLR_GET_BUF_LEN(pBuf) \
    CRU_BUF_Get_ChainValidByteCount(pBuf)

#define APHDLR_BUF_IF_LINEAR(pBuf, u4OffSet, u4Size) \
    CRU_BUF_Get_DataPtr_IfLinear ((tCRU_BUF_CHAIN_HEADER *)(pBuf), \
            (u4OffSet), (u4Size))

#define APHDLR_COPY_FROM_BUF(pBuf, pu1Dst, u4Offset, u4Size) \
    CRU_BUF_Copy_FromBufChain ((pBuf), (UINT1 *)(pu1Dst), u4Offset, u4Size)


#define APHDLR_RELEASE_CRU_BUF(pBuf) \
    CRU_BUF_Release_MsgBufChain ((pBuf), 0)

#define APHDLR_COPY_TO_BUF(pBuf, pu1Src, u4Offset, u4Size) \
    CRU_BUF_Copy_OverBufChain ((pBuf), (UINT1 *)(pu1Src), u4Offset, u4Size)

#endif
