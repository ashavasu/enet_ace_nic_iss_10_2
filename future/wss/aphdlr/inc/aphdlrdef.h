/************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved                 *
 * 
 * $Id: aphdlrdef.h,v 1.1.1.1 2016/09/20 10:42:38 siva Exp $
 *                                                                      *
 * DESCRIPTION    : This file contains the  definitions used for AP     *
 *                  Handler  module                                     *
 ************************************************************************/

#ifndef  __APHDLR_DEF_H__
#define  __APHDLR_DEF_H__

#define APHDLR_TASK_NAME                              "APH"
#define APHDLR_RECV_TASK_PRIORITY                       100
#define APHDLR_STACK_SIZE                  OSIX_DEFAULT_STACK_SIZE

#define APHDLR_CTRL_Q_NAME                 (UINT1 *) "APCQ"
#define APHDLR_DATARX_Q_NAME               (UINT1 *) "APDR"
#define APHDLR_DATATX_Q_NAME               (UINT1 *) "APDT"
#define APHDLR_MUT_EXCL_SEM_NAME           (const UINT1 *)"APHM"
#define APHDLR_SEM_CREATE_INIT_CNT         1

#define APHDLR_CTRL_RX_MSGQ_EVENT          0x00000001
#define APHDLR_DATA_RX_MSGQ_EVENT          0x00000002
#define APHDLR_DATA_TX_MSGQ_EVENT          0x00000004
#define APHDLR_TMR_EXP_EVENT               0x00000008
#define APHDLR_RELINQUISH_EVENT            0x00000010
#define APHDLR_INIT_EVENT                  0x00000020

#define APHDLR_LOCK    ApHdlrMainTaskLock () 
#define APHDLR_UNLOCK  ApHdlrMainTaskUnLock ()

#endif

