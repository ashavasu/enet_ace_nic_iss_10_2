/******************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 * $Id: aphdlrsz.h,v 1.2 2016/10/13 14:00:23 siva Exp $
 * Description: This file contains APHDLR Sizing parameters
 *******************************************************************/

enum {
    MAX_APHDLR_DATA_TRANSFER_PKTBUF_SIZING_ID,
    MAX_APHDLR_PKTBUF_SIZING_ID,
    MAX_APHDLR_PKT_QUE_SIZE_SIZING_ID,
    APHDLR_MAX_SIZING_ID
};


#ifdef  _APHDLRSZ_C
tMemPoolId APHDLRMemPoolIds[ APHDLR_MAX_SIZING_ID];
INT4  AphdlrSizingMemCreateMemPools(VOID);
VOID  AphdlrSizingMemDeleteMemPools(VOID);
INT4  AphdlrSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _APHDLRSZ_C  */
extern tMemPoolId APHDLRMemPoolIds[ ];
extern INT4  AphdlrSizingMemCreateMemPools(VOID);
extern VOID  AphdlrSizingMemDeleteMemPools(VOID);
#endif /*  _APHDLRSZ_C  */


#ifdef  _APHDLRSZ_C
tFsModSizingParams FsAPHDLRSizingParams [] = {
{ "UINT1[6000]", "MAX_APHDLR_DATA_TRANSFER_PKTBUF", sizeof(UINT1[6000]),MAX_APHDLR_DATA_TRANSFER_PKTBUF, MAX_APHDLR_DATA_TRANSFER_PKTBUF,0 },
{ "UINT1[2000]", "MAX_APHDLR_PKTBUF", sizeof(UINT1[2000]),MAX_APHDLR_PKTBUF, MAX_APHDLR_PKTBUF,0 },
{ "tApHdlrQueueReq", "MAX_APHDLR_PKT_QUE_SIZE", sizeof(tApHdlrQueueReq),MAX_APHDLR_PKT_QUE_SIZE, MAX_APHDLR_PKT_QUE_SIZE,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _APHDLRSZ_C  */
extern tFsModSizingParams FsAPHDLRSizingParams [];
#endif /*  _APHDLRSZ_C  */


