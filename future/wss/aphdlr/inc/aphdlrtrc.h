/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 * $Id: aphdlrtrc.h,v 1.2 2017/11/24 10:37:01 siva Exp $
 * Description:This file contains procedures and definitions
 *             used for debugging.
 *******************************************************************/

#ifndef _APHDLRTRC_H_
#define _APHDLRTRC_H_

#define  APHDLR_MOD                ((const char *)"APHDLR")

#define APHDLR_MASK                APHDLR_MGMT_TRC | APHDLR_FAILURE_TRC

/* Trace and debug flags */
/* #define  APHDLR_MASK gu4CapwapDebugMask */

#define APHDLR_PKT_FLOW_IN              1
#define APHDLR_PKT_FLOW_OUT             2
/*Trace Level*/
#define APHDLR_MGMT_TRC                    0x00000001
#define APHDLR_INIT_TRC                    0x00000002
#define APHDLR_ENTRY_TRC                   0x00000004
#define APHDLR_EXIT_TRC                    0x00000008
#define APHDLR_DEBUG_TRC                   0x00000010
#define APHDLR_FAILURE_TRC                 0x00000020

#define APHDLR_FN_ENTRY() \
    MOD_FN_ENTRY (APHDLR_MASK, APHDLR_ENTRY_TRC,APHDLR_MOD)

#define APHDLR_FN_EXIT() MOD_FN_EXIT (APHDLR_MASK, APHDLR_EXIT_TRC,APHDLR_MOD)

#define APHDLR_PKT_DUMP(mask, pBuf, Length, fmt)                           \
    MOD_PKT_DUMP(APHDLR_PKT_DUMP_TRC,mask, APHDLR_MOD, pBuf, Length, fmt)
#define APHDLR_TRC(mask, fmt)\
    MOD_TRC(APHDLR_MASK, mask, APHDLR_MOD, fmt)
#define APHDLR_TRC1(mask,fmt,arg1)\
    MOD_TRC_ARG1(APHDLR_MASK,mask,APHDLR_MOD,fmt,arg1)
#define APHDLR_TRC2(mask,fmt,arg1,arg2)\
    MOD_TRC_ARG2(APHDLR_MASK,mask,APHDLR_MOD,fmt,arg1,arg2)
#define APHDLR_TRC3(mask,fmt,arg1,arg2,arg3)\
    MOD_TRC_ARG3(APHDLR_MASK,mask,APHDLR_MOD,fmt,arg1,arg2,arg3)
#define APHDLR_TRC4(mask,fmt,arg1,arg2,arg3,arg4)\
    MOD_TRC_ARG4(APHDLR_MASK,mask,APHDLR_MOD,fmt,arg1,arg2,arg3,arg4)
#define APHDLR_TRC5(mask,fmt,arg1,arg2,arg3,arg4,arg5)\
    MOD_TRC_ARG5(APHDLR_MASK,mask,APHDLR_MOD,fmt,arg1,arg2,arg3,arg4,arg5)
#define APHDLR_TRC6(mask,fmt,arg1,arg2,arg3,arg4,arg5,arg6)\
    MOD_TRC_ARG6(APHDLR_MASK,mask,APHDLR_MOD,fmt,arg1,arg2,arg3,arg4,arg5,arg6)
#define APHDLR_TRC7(mask,fmt,arg1,arg2,arg3,arg4,arg5,arg6,arg7)\
    MOD_TRC_ARG7(APHDLR_MASK,mask,APHDLR_MOD,fmt,arg1,arg2,arg3,arg4,arg5,arg6,arg7)


#endif

