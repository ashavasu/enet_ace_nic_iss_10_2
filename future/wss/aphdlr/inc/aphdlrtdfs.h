/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
* $Id: aphdlrtdfs.h,v 1.1.1.1 2016/09/20 10:42:38 siva Exp $
* Description: This file contains the type defines for AP HDLR module
*********************************************************************/
#ifndef __APHDLRTDFS_H__
#define __APHDLRTDFS_H__

typedef enum {
    APHDLR_RETRANSMIT_INTERVAL_TMR = 0,
    APHDLR_STATS_INTERVAL_TMR = 1,
    APHDLR_IDLE_INTERVAL_TMR = 2,
    APHDLR_CPU_RELINQUISH_TMR = 3,
    APHDLR_HOSTAPD_INTERVAL_TMR = 4,
#ifdef WPS_WTP_WANTED    
    APHDLR_WPS_PBC_TMR = 5,
#endif    
    APHDLR_PROBE_REQ_TMR,
    APHDLR_MAX_TMR
}tApHdlrTmrId;

typedef struct{
    tTimerListId        ApHdlrTmrListId;
    tTmrDesc            aApHdlrTmrDesc[APHDLR_MAX_TMR];
    /* Timer data struct that contains
     * func ptrs for timer handling and

     * offsets to identify the data
     * struct containing timer block.
     * Timer ID is the index to this
     * data structure */
}tApHdlrTimerList;

typedef struct {
    tOsixTaskId         apHdlrTaskId;
    tOsixQId            ctrlRxMsgQId;           /*Ctrl Rx Queue */
    tOsixQId            ctrlRxFragMsgQId;       /*Ctrl Fragment RX Queue */
    tOsixQId            dataRxMsgQId;           /* Data Rx Queue */
    tOsixQId            dataTxMsgQId;           /* Data Tx Queue */
    tOsixSemId          apHdlrTaskSemId;        /* ApHdlr Task Semaphore */
    UINT1               au1TaskSemName[8];      /* ApHdlr Sem Name*/
}tApHdlrTaskGlobals;

#endif
