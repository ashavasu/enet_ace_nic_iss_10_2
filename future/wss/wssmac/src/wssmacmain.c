#ifndef _WSSMAC_MAIN_C_
#define _WSSMAC_MAIN_C_
/***************************************************************************** 
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved 
 * 
 * $Id: wssmacmain.c,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $ 
 * 
 * Description: This file contains the WSSMAC INIT routine 
 * 
 *****************************************************************************/

#include "wssmacinc.h"

UINT1
WssMacMainInit (VOID)
{
    WSSMAC_FN_ENTRY ();

    WSSMAC_FN_EXIT ();

    return WSSMAC_SUCCESS;
}

#endif /* _WSSMAC_MAIN_C_ */

