/*************************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *  $Id: wssmaccmn.c,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $   
 * 
 * DESCRIPTION : Contains WSSMAC Module functions common for WTP and WLC
 * ***********************************************************************/
#ifndef __WSSMAC_CMN_C__
#define __WSSMAC_CMN_C__

#include "wssmacinc.h"

/* ***************************************************************************
 *                                                                           *
 * Function     : WssMacProcessWssIfMsg                                      *
 *                                                                           *
 * Description  : Process WssIf Msg                                          *
 *                                                                           *
 * Input(s)     : MAC message type and union of struct wilth CRU_BUF         *                                                 
 *                                                                           *
 * Output(s)    : None                                                       *
 *                                                                           *
 * Returns      : SUCCESS/FAILURE                                                       *
 *                                                                           *
 * ***************************************************************************/
UINT1
WssMacProcessWssIfMsg (UINT4 eMsgType, tWssMacMsgStruct *pMsgStruct)
{
    if(eMsgType < WSS_MAC_MAX_MSG)
    {
        if(gaWssMacFrameHandling[eMsgType] != NULL)
        {
            if(gaWssMacFrameHandling[eMsgType]((UINT1)eMsgType, pMsgStruct) != 
                    WSSMAC_SUCCESS)
            {
                WSSMAC_TRC1 (WSSMAC_FAILURE_TRC, "WssMacProcessWssMsg: "
                        "Error in process of WSS message for eMsgType = %d\r\n",eMsgType);
                return WSSMAC_FAILURE;
            }
        }
        else
        {
            WSSMAC_TRC1 (WSSMAC_FAILURE_TRC, "WssMacProcessWssMsg:"
                    "gaWssMacFrameHandling is NULL for MsgType %d\r\n", eMsgType);
            return WSSMAC_FAILURE;            
        }
    }
    else
    {     WSSMAC_TRC1 (WSSMAC_FAILURE_TRC, "WssMacProcessWssMsg:"
	    "gaWssMacFrameHandling is NULL for MsgType %d\r\n", eMsgType);
    	  return WSSMAC_FAILURE;
    }
    
    return WSSMAC_SUCCESS;
}

/* ***************************************************************************
 *                                                                           *
 * Function     : WssMacGetFrameType                                         *
 *                                                                           *
 * Description  : This function is used to get the frame type                *
 *                                                                           *
 * Input(s)    : Frame Control                                              *
 *                                                                           *
 * Output(s)    : Frame type                                                 *
 *                                                                           *
 * Returns      : Frame type - UINT2                                         *
 *                                                                           *
 * ***************************************************************************/
UINT1
WssMacGetFrameType (UINT1 u1FrmCtrl)
{
    u1FrmCtrl &= WSSMAC_GET_FRAME_TYPE_MASK;

    return (u1FrmCtrl >> WSSMAC_FRAM_TYPE_SHIFT);
}

/* ***************************************************************************
 *                                                                           *
 * Function     : WssMacGetFrameSubType                                      *
 *                                                                           *
 * Description  : This function is used to get the frame sub type            *
 *                                                                           *
 * Input(s)    : Frame Control                                              *
 *                                                                           *
 * Output(s)    : Frame subtype                                              *
 *                                                                           *
 * Returns      : Frame sub type - UINT2                                     *
 *                                                                           *
 * ***************************************************************************/
UINT1
WssMacGetFrameSubType (UINT1 u1FrmCtrl)
{
    u1FrmCtrl &= WSSMAC_GET_FRAME_SUB_TYPE_MASK;

    return (u1FrmCtrl >> WSSMAC_FRAM_SUB_TYPE_SHIFT);
}

/* ***************************************************************************
 *                                                                           *
 * Function     : WssMacParseMACFrame                                        *
 *                                                                           *
 * Description  : This function is used to get MAC message type              *
 *                                                                           *
 * Input(s)     : Frame buffer                                               *
 *                                                                           *
 * Output(s)    : MAC message type                                              *
 *                                                                           *
 * Returns      : MAC message type - UINT1                                   *
 *                                                                           *
 * ***************************************************************************/
UINT1
WssMacParseMACFrame (UINT1 *pktBuf)
{
    UINT1    *pu1ReadPtr = NULL;
    UINT1    u1FrameCtrl = 0; 
    UINT1    u1FrameType = 0, u1FrameSubType = 0;
    UINT1    u1eMsgType = WSSMAC_INVALID_MSG_TYPE;		
#if WSSMAC_PKT_DUMP
    UINT1    u1i = 0;
#endif

    WSSMAC_FN_ENTRY ();

    /* Copy the pointer locally and use it */
    pu1ReadPtr = pktBuf;

    /* Get the 802.11 MAC Header */
    /* Get the first octet which contains type/subtypeFrame control field */
    WSSMAC_GET_1BYTE(u1FrameCtrl, pu1ReadPtr);    

    /* Get Type and Subtype value from Frame control field */
    u1FrameType = WssMacGetFrameType(u1FrameCtrl);
    u1FrameSubType = WssMacGetFrameSubType(u1FrameCtrl);

    if (u1FrameType == WSS_MAC_FRAME_TYPE_MGMT)
    {		
	if ( (u1FrameSubType != WSSMAC_RESERVED6)&&
             (u1FrameSubType != WSSMAC_RESERVED7)&&
             (u1FrameSubType != WSSMAC_RESERVED9)&&
             (u1FrameSubType != WSSMAC_RESERVED14)&& 
             (u1FrameSubType != WSSMAC_RESERVED15) )
        {
            u1eMsgType = u1FrameSubType;

#if WSSMAC_PKT_DUMP
            /* Copy the pointer locally and use it */
            pu1ReadPtr = pktBuf;

            printf("\nStart of MGMT pkt dump\n");
            for (u1i=0;u1i<= 128;pu1ReadPtr++,u1i++) {
                printf(" %x", *pu1ReadPtr);
                if ((u1i !=0) &&(u1i % 8)==0)
                {
                printf("\n");
                 }
            }
            printf("\nEnd of pkt dump\n");
#endif
        }
	else
        {
           WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                   "WssMacParseMACFrame: INVALID u1FrameType\r\n");
           u1eMsgType = WSSMAC_INVALID_MSG_TYPE;

           return (u1eMsgType);      
        }
    }
    else if (u1FrameType == WSS_MAC_FRAME_TYPE_CTRL)
    {
        u1eMsgType = WSS_DOT11_CTRL_MSG;
    }	
    else if (u1FrameType == WSS_MAC_FRAME_TYPE_DATA)
    {
        u1eMsgType = WSS_DOT11_DATA_MSG;

#if WSSMAC_PKT_DUMP
            /* Copy the pointer locally and use it */
            pu1ReadPtr = pktBuf;

            printf("\nStart of DATA pkt dump\n");
            for (u1i=0;u1i<= 128;pu1ReadPtr++,u1i++) {
                printf(" %x", *pu1ReadPtr);
                if ((u1i !=0) &&(u1i % 8)==0)
                {
                printf("\n");
                }
            }
            printf("\nEnd of pkt dump\n");
#endif
    }
    else
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                   "WssMacParseMACFrame: INVALID u1FrameType\r\n");        
    }

    WSSMAC_FN_EXIT ();

    return (u1eMsgType);
}

#endif
