/*************************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *  
 *  $Id: wssmacwtpparse.c,v 1.6 2017/11/24 10:37:11 siva Exp $
 *
 * DESCRIPTION : Contains WSSMAC Module functions for WTP 
 * ***********************************************************************/
#ifndef __WSSMAC_WTPPARSE_C__
#define __WSSMAC_WTPPARSE_C__

#include "wssmacinc.h"
#include "wssifpmdb.h"
#include "wsspm.h"
#ifdef BAND_SELECT_WANTED
#include "radioif.h"
#include "aphdlrprot.h"
#endif
PUBLIC tRBTree      gWssStaAPBandSteerDB;

/* ***************************************************************************
 *                                                                           *
 * Function     : WssMacProcessCfaMsg                                        *
 *                                                                           *
 * Description  : Process the CFA Msg from AP Hdlr                           *
 *                                                                           *
 * Input(s)     : MAC message type and union of struct with CRU_BUF          *
 *                                                                           *
 * Output(s)    : None                                                       *
 *                                                                           *
 * Returns      : SUCCESS/FAILURE                                            *
 *                                                                           *
 * ***************************************************************************/
UINT1
WssMacProcessCfaMsg (UINT1 eMsgType, tWssMacMsgStruct * pMsgStruct)
{
    UINT2               u2PacketLen = 0;
    UINT1              *pu1ReadPtr = NULL;
    UINT1              *pu1Frame = NULL;
    tCRU_BUF_CHAIN_DESC *pBuf = NULL;
    tWssStaActiveStatus StaStatusInfo;

    MEMSET (&StaStatusInfo, 0, sizeof (tWssStaActiveStatus));
    pBuf = pMsgStruct->unMacMsg.MacDot11PktBuf.pDot11MacPdu;

    u2PacketLen = (UINT2) WSSMAC_GET_BUF_LEN (pBuf);
    pu1ReadPtr = WSSMAC_BUF_IF_LINEAR (pBuf, 0, u2PacketLen);
    pu1Frame = UtlShMemAllocCapwapPktBuf ();

    if (pu1Frame == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC, "WssMacProcessCfaMsg:"
                    "UtlShMemAllocCapwapPktBuf Allocation failed \r\n");
        return WSSMAC_FAILURE;
    }

    if (pu1ReadPtr == NULL)
    {
        WSSMAC_COPY_FROM_BUF (pBuf, pu1Frame, 0, u2PacketLen);
        pu1ReadPtr = pu1Frame;
    }

    MEMCPY (StaStatusInfo.StaMacAddr, pu1ReadPtr + 10, sizeof (tMacAddr));
    if (WssStaSetStaActiveFlag (StaStatusInfo) != OSIX_SUCCESS)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                    "WssMacProcessCfaMsg: Error in set Active flag for station \r\n");
        /* return WSSMAC_FAILURE; */
    }

    /* Parse the 802.11 MAC frame to get the Mac message type */
    eMsgType = WssMacParseMACFrame (pu1ReadPtr);

    if (eMsgType != WSSMAC_INVALID_MSG_TYPE)
    {
        if (gaWssMacFrameHandling[eMsgType] != NULL)
        {
            if (gaWssMacFrameHandling[eMsgType] (eMsgType, pMsgStruct) !=
                WSSMAC_SUCCESS)
            {
                WSSMAC_TRC1 (WSSMAC_FAILURE_TRC, "WssMacProcessCfaMsg:"
                             "Error in processing CFA message for eMsgType = %d \r\n",
                             eMsgType);
                UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1Frame);
                return WSSMAC_FAILURE;
            }
        }
        else
        {
            WSSMAC_TRC1 (WSSMAC_FAILURE_TRC, "WssMacProcessCfaMsg:"
                         " gaWssMacFrameHandling is NULL for MsgType %d\r\n",
                         eMsgType);
            UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1Frame);
            return WSSMAC_FAILURE;
        }
    }
    else
    {
        WSSMAC_TRC1 (WSSMAC_FAILURE_TRC,
                     "WssMacProcessCfaMsg: INVALID Msg Type %d\r\n", eMsgType);
        UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1Frame);
        return WSSMAC_FAILURE;
    }
    UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1Frame);
    return WSSMAC_SUCCESS;
}

/* ***************************************************************************
 *                                                                           *
 * Function     : WssMacProcessProbeReq                                      *
 *                                                                           *
 * Description  : Process the probe request frame                            *
 *                                                                           *
 * Input(s)     : MAC message type and union of struct with CRU_BUF          *
 *                                                                           *
 * Output(s)    : Probe request structure                                    *
 *                                                                           *
 * Returns      : SUCCESS/FAILURE                                            *
 *                                                                           *
 * ***************************************************************************/
UINT1
WssMacProcessProbeReq (UINT1 eMsgType, tWssMacMsgStruct * pMsgStruct)
{
    tWssMacMsgStruct   *pProbeReqStruct = NULL;
    tCRU_BUF_CHAIN_DESC *pBuf = NULL;
    tWssWlanDB         *pWssWlanDBMsg = NULL;
    UINT2               u2PacketLen = 0, u2Count = 0, u2Count1 = 0;
    UINT1              *pu1ReadPtr = NULL;
    UINT1              *pu1Frame = NULL;
    UINT1               u1MacType = 3;
    UINT2               u2Len = 0;
    unApHdlrMsgStruct   ApHdlrMsgStruct;
    unApHdlrMsgStruct   CapwapMsgStruct;
    tWssIfPMDB          WssIfPMDB;
    tWssWlanDB         *pWssWlanDB = NULL;
#ifdef BAND_SELECT_WANTED
    tMacAddr            BcastAddr = { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
    tRadioIfGetDB       RadioIfGetDB;
    tWssStaBandSteerDB  WssStaBandSteerDB;
    tWssStaBandSteerDB *pBandSteerDB = NULL;
    UINT1               au1Radio[2];
    UINT1               u1RadioId = 0;
    UINT1               u1WlanId = 0;

    MEMSET (au1Radio, 0, 2);
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
#endif

    pWssWlanDBMsg = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
    if (pWssWlanDBMsg == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                    "WssMacProcessProbeReq:- "
                    "UtlShMemAllocWlanDbBuf returned failure\n");
        return OSIX_FAILURE;
    }
    pWssWlanDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
    if (pWssWlanDB == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                    "WssMacProcessProbeReq:- "
                    "UtlShMemAllocWlanDbBuf returned failure\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDBMsg);
        return OSIX_FAILURE;
    }
    pProbeReqStruct =
        (tWssMacMsgStruct *) (VOID *) UtlShMemAllocMacMsgStructBuf ();
    if (pProbeReqStruct == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                    "WssMacProcessProbeReq:-"
                    "UtlShMemAllocMacMsgStructBuf returned failure\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDBMsg);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        return OSIX_FAILURE;
    }

    MEMSET (pProbeReqStruct, 0, sizeof (tWssMacMsgStruct));
    MEMSET (pWssWlanDBMsg, 0, sizeof (tWssWlanDB));
    MEMSET (&ApHdlrMsgStruct, 0, sizeof (unApHdlrMsgStruct));
    MEMSET (&CapwapMsgStruct, 0, sizeof (unApHdlrMsgStruct));
    MEMSET (&WssIfPMDB, 0, sizeof (tWssIfPMDB));
    MEMSET (pWssWlanDB, 0, sizeof (tWssWlanDB));
#ifdef BAND_SELECT_WANTED
    MEMSET (&WssStaBandSteerDB, 0, sizeof (tWssStaBandSteerDB));
#endif

    UNUSED_PARAM (eMsgType);

    pBuf = pMsgStruct->unMacMsg.MacDot11PktBuf.pDot11MacPdu;
    pu1Frame = UtlShMemAllocCapwapPktBuf ();

    if (pu1Frame == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC, "WssMacProcessProbeReq:"
                    "UtlShMemAllocCapwapPktBuf Allocation failed \r\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDBMsg);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pProbeReqStruct);
        return WSSMAC_FAILURE;
    }

    u2PacketLen = (UINT2) WSSMAC_GET_BUF_LEN (pBuf);
    pu1ReadPtr = WSSMAC_BUF_IF_LINEAR (pBuf, 0, u2PacketLen);

    if (pu1ReadPtr == NULL)
    {
        WSSMAC_COPY_FROM_BUF (pBuf, pu1Frame, 0, u2PacketLen);
        pu1ReadPtr = pu1Frame;
    }

    /* *************** Construct Probe Request structure ************* */
    /* Fill the 802.11 MAC Header details */
    WSSMAC_GET_2BYTES (pProbeReqStruct->unMacMsg.ProbReqMacFrame.MacMgmtFrmHdr.u2MacFrameCtrl, pu1ReadPtr);    /* Get Frame control field */
    WSSMAC_GET_2BYTES (pProbeReqStruct->unMacMsg.ProbReqMacFrame.MacMgmtFrmHdr.u2MacFrameDurId, pu1ReadPtr);    /* Get Duration ID field */
    WSSMAC_GET_NBYTES (pProbeReqStruct->unMacMsg.ProbReqMacFrame.MacMgmtFrmHdr.u1DA, pu1ReadPtr, WSSMAC_MAC_ADDR_LEN);    /* Get Addr1 field */
    WSSMAC_GET_NBYTES (pProbeReqStruct->unMacMsg.ProbReqMacFrame.MacMgmtFrmHdr.u1SA, pu1ReadPtr, WSSMAC_MAC_ADDR_LEN);    /* Get Addr2 field */
    WSSMAC_GET_NBYTES (pProbeReqStruct->unMacMsg.ProbReqMacFrame.MacMgmtFrmHdr.u1BssId, pu1ReadPtr, WSSMAC_MAC_ADDR_LEN);    /* Get Addr3 field */
    WSSMAC_GET_2BYTES (pProbeReqStruct->unMacMsg.ProbReqMacFrame.MacMgmtFrmHdr.u2MacFrameSeqCtrl, pu1ReadPtr);    /* Get Sequence control field */

    /* Fill the SSID details */
    if ((*pu1ReadPtr) == WSSMAC_SSID_ELEMENT_ID)
    {
        WSSMAC_GET_1BYTE (pProbeReqStruct->unMacMsg.ProbReqMacFrame.Ssid.
                          u1MacFrameElemId, pu1ReadPtr);
        WSSMAC_GET_1BYTE (pProbeReqStruct->unMacMsg.ProbReqMacFrame.Ssid.
                          u1MacFrameElemLen, pu1ReadPtr);
        u2Len =
            pProbeReqStruct->unMacMsg.ProbReqMacFrame.Ssid.u1MacFrameElemLen;
        WSSMAC_GET_NBYTES (pProbeReqStruct->unMacMsg.ProbReqMacFrame.Ssid.
                           au1MacFrameSSID, pu1ReadPtr, u2Len);
    }
    /* Fill the Supported Rates details */
    if ((*pu1ReadPtr) == WSSMAC_SUPP_RATE_ELEMENT_ID)
    {
        WSSMAC_GET_1BYTE (pProbeReqStruct->unMacMsg.ProbReqMacFrame.SuppRate.
                          u1MacFrameElemId, pu1ReadPtr);
        WSSMAC_GET_1BYTE (pProbeReqStruct->unMacMsg.ProbReqMacFrame.SuppRate.
                          u1MacFrameElemLen, pu1ReadPtr);
        u2Len =
            pProbeReqStruct->unMacMsg.ProbReqMacFrame.SuppRate.
            u1MacFrameElemLen;
        u2Count = 0;
        while (u2Count < u2Len)
        {
            WSSMAC_GET_1BYTE (pProbeReqStruct->unMacMsg.ProbReqMacFrame.
                              SuppRate.au1MacFrameSuppRate[u2Count],
                              pu1ReadPtr);
            u2Count++;
        }
    }

    /* Fill the Request information if Element ID == WSSMAC_REQINFO_ELEMENTID */
    if ((*pu1ReadPtr) == WSSMAC_REQINFO_ELEMENTID)
    {
        WSSMAC_GET_1BYTE (pProbeReqStruct->unMacMsg.ProbReqMacFrame.ReqInfo.
                          u1MacFrameElemId, pu1ReadPtr);
        WSSMAC_GET_1BYTE (pProbeReqStruct->unMacMsg.ProbReqMacFrame.ReqInfo.
                          u1MacFrameElemLen, pu1ReadPtr);
        u2Count = 0;
        u2Len =
            pProbeReqStruct->unMacMsg.ProbReqMacFrame.ReqInfo.u1MacFrameElemLen;
        while (u2Count < u2Len)
        {
            WSSMAC_GET_1BYTE (pProbeReqStruct->unMacMsg.ProbReqMacFrame.ReqInfo.
                              au1MacFrameReqID[u2Count], pu1ReadPtr);
            u2Count++;
        }
    }

    /* Fill the Extended Supported Rates details if Supported rates 
     * Element ID > WSSMAC_MAX_SUPP_RATE_LEN  */
    if ((*pu1ReadPtr) == WSSMAC_EXT_SUPP_RATE_ELEMENTID)
    {
        WSSMAC_GET_1BYTE (pProbeReqStruct->unMacMsg.ProbReqMacFrame.
                          ExtSuppRates.u1MacFrameElemId, pu1ReadPtr);
        WSSMAC_GET_1BYTE (pProbeReqStruct->unMacMsg.ProbReqMacFrame.
                          ExtSuppRates.u1MacFrameElemLen, pu1ReadPtr);
        u2Count = 0;
        u2Len = pProbeReqStruct->unMacMsg.ProbReqMacFrame.ExtSuppRates.
            u1MacFrameElemLen;

        while (u2Count < u2Len)
        {
            WSSMAC_GET_1BYTE (pProbeReqStruct->unMacMsg.ProbReqMacFrame.
                              ExtSuppRates.au1MacFrameExtSuppRates[u2Count],
                              pu1ReadPtr);
            u2Count++;
        }
    }

    /* Fill the HT capabilities of Element ID == WSSMAC_HT_CAPAB_ELEMENT_ID */
    if ((*pu1ReadPtr) == WSSMAC_HT_CAPAB_ELEMENT_ID)
    {
        WSSMAC_GET_1BYTE (pProbeReqStruct->unMacMsg.ProbReqMacFrame.
                          HTCapabilities.u1MacFrameElemId, pu1ReadPtr);
        WSSMAC_GET_1BYTE (pProbeReqStruct->unMacMsg.ProbReqMacFrame.
                          HTCapabilities.u1MacFrameElemLen, pu1ReadPtr);
        WSSMAC_GET_2BYTES (pProbeReqStruct->unMacMsg.ProbReqMacFrame.
                           HTCapabilities.u2HTCapInfo, pu1ReadPtr);
        WSSMAC_GET_1BYTE (pProbeReqStruct->unMacMsg.ProbReqMacFrame.
                          HTCapabilities.u1AMPDUParam, pu1ReadPtr);
        WSSMAC_GET_NBYTES (pProbeReqStruct->unMacMsg.ProbReqMacFrame.
                           HTCapabilities.au1SuppMCSSet, pu1ReadPtr,
                           WSSMAC_SUPP_MCS_SET);
        WSSMAC_GET_2BYTES (pProbeReqStruct->unMacMsg.ProbReqMacFrame.
                           HTCapabilities.u2HTExtCap, pu1ReadPtr);
        WSSMAC_GET_4BYTES (pProbeReqStruct->unMacMsg.ProbReqMacFrame.
                           HTCapabilities.u4TranBeamformCap, pu1ReadPtr);
        WSSMAC_GET_1BYTE (pProbeReqStruct->unMacMsg.ProbReqMacFrame.
                          HTCapabilities.u1ASELCap, pu1ReadPtr);
    }

    /* Fill the HT 20/40 BSS Coexistence if Element ID == 
     * WSSMAC_BSS_COEXIST_ELEMENT_ID */
    if ((*pu1ReadPtr) == WSSMAC_BSS_COEXIST_ELEMENT_ID)
    {
        WSSMAC_GET_1BYTE (pProbeReqStruct->unMacMsg.ProbReqMacFrame.
                          BssCoexistence.u1MacFrameElemId, pu1ReadPtr);
        WSSMAC_GET_1BYTE (pProbeReqStruct->unMacMsg.ProbReqMacFrame.
                          BssCoexistence.u1MacFrameElemLen, pu1ReadPtr);
        WSSMAC_GET_1BYTE (pProbeReqStruct->unMacMsg.ProbReqMacFrame.
                          BssCoexistence.u1BSSCoexistInfoFields, pu1ReadPtr);
    }

    /* Fill the Extended Capabilities if Element ID == 
     * WSSMAC_EXT_CAPAB_ELEMENT_ID */
    if ((*pu1ReadPtr) == WSSMAC_EXT_CAPAB_ELEMENT_ID)
    {
        WSSMAC_GET_1BYTE (pProbeReqStruct->unMacMsg.ProbReqMacFrame.
                          ExtCapabilities.u1MacFrameElemId, pu1ReadPtr);
        WSSMAC_GET_1BYTE (pProbeReqStruct->unMacMsg.ProbReqMacFrame.
                          ExtCapabilities.u1MacFrameElemLen, pu1ReadPtr);
        WSSMAC_GET_NBYTES (pProbeReqStruct->unMacMsg.ProbReqMacFrame.
                           ExtCapabilities.au1Capabilities, pu1ReadPtr,
                           WSSMAC_EXT_CAP);
    }

    /* Fill the Vendor info if Element ID ==  WSSMAC_VENDOR_INFO_ELEMENTID */
    u2Count1 = 0;
    while ((*pu1ReadPtr) == WSSMAC_VENDOR_INFO_ELEMENTID)
    {
        WSSMAC_GET_1BYTE (pProbeReqStruct->unMacMsg.ProbReqMacFrame.
                          aVendSpecInfo[u2Count1].u1MacFrameElemId, pu1ReadPtr);
        WSSMAC_GET_1BYTE (pProbeReqStruct->unMacMsg.ProbReqMacFrame.
                          aVendSpecInfo[u2Count1].u1MacFrameElemLen,
                          pu1ReadPtr);
        WSSMAC_GET_NBYTES (pProbeReqStruct->unMacMsg.ProbReqMacFrame.
                           aVendSpecInfo[u2Count1].au1MacFrameVend_OUI,
                           pu1ReadPtr, WSSMAC_MAX_OUI_SIZE);
        u2Count = 0;
        u2Len = pProbeReqStruct->unMacMsg.ProbReqMacFrame.
            aVendSpecInfo[u2Count1].u1MacFrameElemLen - WSSMAC_MAX_OUI_SIZE;
        while (u2Count < u2Len)
        {
            WSSMAC_GET_1BYTE (pProbeReqStruct->unMacMsg.ProbReqMacFrame.
                              aVendSpecInfo[u2Count1].
                              au1MacFrameVend_Content[u2Count], pu1ReadPtr);
            u2Count++;
        }
        u2Count1++;
    }

#ifdef BAND_SELECT_WANTED

    MEMCPY (&(WssStaBandSteerDB.stationMacAddress),
            pProbeReqStruct->unMacMsg.ProbReqMacFrame.MacMgmtFrmHdr.u1SA,
            WSSMAC_MAC_ADDR_LEN);
    /* If Recvd BSSID is broadcast */
    if (MEMCMP (pProbeReqStruct->unMacMsg.ProbReqMacFrame.MacMgmtFrmHdr.u1BssId,
                BcastAddr, sizeof (tMacAddr)) == 0)
    {
        /* Get Radio Id for 5GHZ Bandwidth. If both Radios are 5 GHz or 2.4 GHz do not 
           store the value */
        for (u1RadioId = 1; u1RadioId <= SYS_DEF_MAX_RADIO_INTERFACES;
             u1RadioId++)
        {
            RadioIfGetDB.RadioIfGetAllDB.u1RadioId = u1RadioId;
            RadioIfGetDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_TRUE;
            if (WssIfProcessRadioIfDBMsg
                (WSS_GET_PHY_RADIO_IF_DB, &RadioIfGetDB) == OSIX_SUCCESS)
            {
                if ((RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                     RADIO_TYPE_A)
                    || (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                        RADIO_TYPE_AN)
                    || (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                        RADIO_TYPE_AC))
                {
                    au1Radio[u1RadioId - 1] = RADIO_TYPE_A;
                }
                else
                {
                    au1Radio[u1RadioId - 1] = RADIO_TYPE_B;
                }
            }
        }
        /* The below logic shall not work for more than 2 radios. 
         *  A utility needs to be written if more than 2 RADIO is suppported */
        u1RadioId = 1;
        if (au1Radio[u1RadioId] != au1Radio[u1RadioId - 1])
        {
            if (au1Radio[u1RadioId] == RADIO_TYPE_A)
            {
                WssStaBandSteerDB.u1RadioId = u1RadioId + 1;
                pWssWlanDB->WssWlanAttributeDB.u1RadioId = u1RadioId + 1;
            }
            else
            {
                WssStaBandSteerDB.u1RadioId = u1RadioId;
                pWssWlanDB->WssWlanAttributeDB.u1RadioId = u1RadioId;
            }
        }
        for (u1WlanId = WSSWLAN_START_WLANID_PER_RADIO;
             u1WlanId < WSSWLAN_END_WLANID_PER_RADIO; u1WlanId++)
        {
            pWssWlanDB->WssWlanAttributeDB.u1WlanId = u1WlanId;
            pWssWlanDB->WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
            pWssWlanDB->WssWlanIsPresentDB.bBssId = OSIX_TRUE;
            if (WssIfProcessWssWlanDBMsg
                (WSS_WLAN_GET_INTERFACE_ENTRY, pWssWlanDB) == OSIX_SUCCESS)
            {
                pWssWlanDB->WssWlanIsPresentDB.bDesiredSsid = OSIX_TRUE;
                pWssWlanDB->WssWlanIsPresentDB.bAgeOutSuppression = OSIX_TRUE;
                if (WssIfProcessWssWlanDBMsg
                    (WSS_WLAN_GET_IFINDEX_ENTRY, pWssWlanDB) == OSIX_SUCCESS)
                {
                    if (MEMCMP (pWssWlanDB->WssWlanAttributeDB.au1DesiredSsid,
                                pProbeReqStruct->unMacMsg.ProbReqMacFrame.Ssid.
                                au1MacFrameSSID, WSSWLAN_SSID_NAME_LEN) == 0)
                    {
                        /* Updated Bandsteer DB with WLAN ID, Radio Id and BSSID */
                        MEMCPY (WssStaBandSteerDB.BssIdMacAddr,
                                pWssWlanDB->WssWlanAttributeDB.BssId,
                                MAC_ADDR_LEN);
                        WssStaBandSteerDB.u1WlanId = u1WlanId;
                        break;
                    }
                }

            }
        }
    }
    /*Assumption */
    WssStaBandSteerDB.u1SsidLen =
        pProbeReqStruct->unMacMsg.ProbReqMacFrame.Ssid.u1MacFrameElemLen;
    MEMCPY (&(WssStaBandSteerDB.au1Ssid),
            pProbeReqStruct->unMacMsg.ProbReqMacFrame.Ssid.au1MacFrameSSID,
            WssStaBandSteerDB.u1SsidLen);
    if (WssStaUpdateAPBandSteerProcessDB (WSSSTA_CREATE_BAND_STEER_DB,
                                          &WssStaBandSteerDB) == OSIX_FAILURE)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC, "WssMacProcessAssocReq:-"
                    "WssIfProcessApHdlrMsg returned FAILURE\r\n");
        UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1Frame);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pProbeReqStruct);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDBMsg);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        return WSSMAC_FAILURE;
    }
    /*We Dont want probe response construction here for BandSelect */
    CapwapMsgStruct.ApHdlrQueueReq.pRcvBuf =
        pMsgStruct->unMacMsg.MacDot11PktBuf.pDot11MacPdu;
    CapwapMsgStruct.ApHdlrQueueReq.u4MsgType = WSS_APHDLR_CAPWAP_MAC_MSG;
    if (WssIfProcessApHdlrMsg (WSS_APHDLR_CAPWAP_MAC_MSG, &CapwapMsgStruct) !=
        WSSMAC_SUCCESS)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC, "WssMacProcessAssocReq:-"
                    "WssIfProcessApHdlrMsg returned FAILURE\r\n");
        UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1Frame);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pProbeReqStruct);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDBMsg);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        return WSSMAC_FAILURE;
    }
    else
    {
        pBandSteerDB =
            RBTreeGet (gWssStaAPBandSteerDB, (tRBElem *) & WssStaBandSteerDB);

        if (pBandSteerDB == NULL)
        {
            WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                        "RBTreeGet failed in the BandSteer Process DB \r\n");
            UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1Frame);
            UtlShMemFreeMacMsgStructBuf ((UINT1 *) pProbeReqStruct);
            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDBMsg);
            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
            return WSSMAC_FAILURE;
        }

        ApHdlrProbeTmrStart (pBandSteerDB,
                             pWssWlanDB->WssWlanAttributeDB.
                             u1AgeOutSuppression);

        UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1Frame);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pProbeReqStruct);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDBMsg);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        return WSSMAC_SUCCESS;
    }
#endif
    u1MacType = pWssWlanDBMsg->WssWlanAttributeDB.u1MacType;

    if (u1MacType == WSS_MAC_SPLITMAC_MODE)    /* Need to get from wsswlan DB */
    {
        CapwapMsgStruct.ApHdlrQueueReq.pRcvBuf =
            pMsgStruct->unMacMsg.MacDot11PktBuf.pDot11MacPdu;
        CapwapMsgStruct.ApHdlrQueueReq.u4MsgType = WSS_APHDLR_CAPWAP_MAC_MSG;
        if (WssIfProcessApHdlrMsg (WSS_APHDLR_CAPWAP_MAC_MSG, &CapwapMsgStruct)
            != WSSMAC_SUCCESS)
        {
            UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1Frame);
            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDBMsg);
            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
            UtlShMemFreeMacMsgStructBuf ((UINT1 *) pProbeReqStruct);
            return WSSMAC_FAILURE;
        }
    }
    else
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
    }

    MEMCPY (&(pWssWlanDB->WssWlanAttributeDB.BssId),
            &(pProbeReqStruct->unMacMsg.ProbReqMacFrame.MacMgmtFrmHdr.u1BssId),
            sizeof (tMacAddr));
    pWssWlanDB->WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSSID_MAPPING_ENTRY,
                                  pWssWlanDB) == WSSMAC_SUCCESS)
    {
        WssIfPMDB.u2ProfileBSSID = pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex;

        if (WssIfProcessPMDBMsg (WSS_PM_WTP_EVT_VSP_BSSID_STATS_GET, &WssIfPMDB)
            != OSIX_SUCCESS)
        {
            UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1Frame);
            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDBMsg);
            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
            UtlShMemFreeMacMsgStructBuf ((UINT1 *) pProbeReqStruct);
            return OSIX_FAILURE;
        }

        WssIfPMDB.tWtpVSPBSSIDStats.wlanBSSIDProbeReqRcvdCount++;
        if (WssIfProcessPMDBMsg (WSS_PM_WTP_EVT_VSP_BSSID_STATS_SET, &WssIfPMDB)
            != OSIX_SUCCESS)
        {
            UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1Frame);
            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDBMsg);
            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
            UtlShMemFreeMacMsgStructBuf ((UINT1 *) pProbeReqStruct);
            return OSIX_FAILURE;
        }
    }
    UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1Frame);
    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDBMsg);
    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
    UtlShMemFreeMacMsgStructBuf ((UINT1 *) pProbeReqStruct);
    return WSSMAC_SUCCESS;
}

/* ***************************************************************************
 *                                                                           *
 * Function     : WssMacProcessProbeResp                                     *
 *                                                                           *
 * Description  : Process the probe response frame                           *
 *                                                                           *
 * Input(s)     : MAC message type and union of struct with probe            *
 *                response structure                                         *
 *                                                                           *
 * Output(s)    : Probe response Char buffer                                 *
 *                                                                           *
 * Returns      : SUCCESS/FAILURE                                            *
 *                                                                           *
 * ***************************************************************************/
UINT1
WssMacProcessProbeResp (UINT1 eMsgType, tWssMacMsgStruct * pProbeRspStruct)
{
    tWssMacMsgStruct   *pProbeRspBuf = NULL;
    unApHdlrMsgStruct   ApHdlrMsgStruct;
    tWssIfPMDB          WssIfPMDB;
    tWssWlanDB          WssWlanDB;

    pProbeRspBuf =
        (tWssMacMsgStruct *) (VOID *) UtlShMemAllocMacMsgStructBuf ();
    if (pProbeRspBuf == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                    "WssMacProcessProbeResp:-"
                    "UtlShMemAllocMacMsgStructBuf returned failure\n");
        return OSIX_FAILURE;
    }
    MEMSET (pProbeRspBuf, 0, sizeof (tWssMacMsgStruct));
    MEMSET (&ApHdlrMsgStruct, 0, sizeof (unApHdlrMsgStruct));
    MEMSET (&WssIfPMDB, 0, sizeof (tWssIfPMDB));
    MEMSET (&WssWlanDB, 0, sizeof (tWssWlanDB));
    UNUSED_PARAM (eMsgType);

    WssMacAssembleProbeRspFrame (pProbeRspStruct, pProbeRspBuf);

    if (pProbeRspBuf->unMacMsg.MacDot11PktBuf.pDot11MacPdu != NULL)
    {
        ApHdlrMsgStruct.WssMacMsgStruct.unMacMsg.MacDot11PktBuf.pDot11MacPdu =
            pProbeRspBuf->unMacMsg.MacDot11PktBuf.pDot11MacPdu;
        ApHdlrMsgStruct.WssMacMsgStruct.unMacMsg.MacDot11PktBuf.u4IfIndex =
            pProbeRspBuf->unMacMsg.MacDot11PktBuf.u4IfIndex;
        ApHdlrMsgStruct.WssMacMsgStruct.unMacMsg.MacDot11PktBuf.
            u1MacType = LOCAL_ROUTING_DISABLED;
        if (WssIfProcessApHdlrMsg (WSS_APHDLR_CFA_TX_BUF, &ApHdlrMsgStruct) !=
            WSSMAC_SUCCESS)
        {
            UtlShMemFreeMacMsgStructBuf ((UINT1 *) pProbeRspBuf);
            return WSSMAC_FAILURE;
        }
    }
    else
    {
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pProbeRspBuf);
        return WSSMAC_FAILURE;
    }

    MEMCPY (&(WssWlanDB.WssWlanAttributeDB.BssId),
            &(pProbeRspStruct->unMacMsg.ProbRspMacFrame.MacMgmtFrmHdr.u1BssId),
            sizeof (tMacAddr));
    WssWlanDB.WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSSID_MAPPING_ENTRY,
                                  &WssWlanDB) != WSSMAC_SUCCESS)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC, "WssMacProcessCapwapMsg: "
                    "WssIfProcessWssWlanDBMsg returned FAILURE \r\n");
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pProbeRspBuf);
        return WSSMAC_FAILURE;
    }
    WssIfPMDB.u2ProfileBSSID = WssWlanDB.WssWlanAttributeDB.u4WlanIfIndex;

    if (WssIfProcessPMDBMsg (WSS_PM_WTP_EVT_VSP_BSSID_STATS_GET, &WssIfPMDB) !=
        OSIX_SUCCESS)
    {
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pProbeRspBuf);
        return OSIX_FAILURE;
    }

    WssIfPMDB.tWtpVSPBSSIDStats.wlanBSSIDProbeRespSentCount++;
    if (WssIfProcessPMDBMsg (WSS_PM_WTP_EVT_VSP_BSSID_STATS_SET, &WssIfPMDB) !=
        OSIX_SUCCESS)
    {
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pProbeRspBuf);
        return OSIX_FAILURE;
    }

    UtlShMemFreeMacMsgStructBuf ((UINT1 *) pProbeRspBuf);
    return WSSMAC_SUCCESS;
}

/* ***************************************************************************
 *                                                                           *
 * Function     : WssMacConstructProbeRsp                                    *
 *                                                                           *
 * Description  : Constructs probes response message                         *
 *                                                                           *
 * Input(s)     : MAC message type and union of struct with probe            *
 *                response structure                                         *
 *                                                                           *
 * Output(s)    : Probe response Char buffer                                 *
 *                                                                           *
 * Returns      : SUCCESS/FAILURE                                            *
 *                                                                           *
 * ***************************************************************************/
UINT1
WssMacConstructProbeRsp (tWssMacMsgStruct * pProbeReqStruct,
                         tWssMacMsgStruct * pProbeRspStruct)
{
    UINT2               u2Count = 0;
    tWssWlanDB          WssWlanDBMsg;
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&WssWlanDBMsg, 0, sizeof (tWssWlanDB));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    /* Get Mac frame header */
    pProbeRspStruct->unMacMsg.ProbRspMacFrame.MacMgmtFrmHdr.u2MacFrameCtrl =
        pProbeReqStruct->unMacMsg.ProbReqMacFrame.MacMgmtFrmHdr.u2MacFrameCtrl;

    pProbeRspStruct->unMacMsg.ProbRspMacFrame.MacMgmtFrmHdr.u2MacFrameCtrl &=
        WSSMAC_SET_PROBRSP_FRAME_CTRL1;
    pProbeRspStruct->unMacMsg.ProbRspMacFrame.MacMgmtFrmHdr.u2MacFrameCtrl |=
        WSSMAC_SET_PROBRSP_FRAME_CTRL2;

    pProbeRspStruct->unMacMsg.ProbRspMacFrame.MacMgmtFrmHdr.u2MacFrameDurId =
        pProbeReqStruct->unMacMsg.ProbReqMacFrame.MacMgmtFrmHdr.u2MacFrameDurId;

    /* Change the source and destination address of the response frame */
    WSSMAC_MEMCPY (pProbeRspStruct->unMacMsg.ProbRspMacFrame.MacMgmtFrmHdr.u1DA,
                   pProbeReqStruct->unMacMsg.ProbReqMacFrame.MacMgmtFrmHdr.u1SA,
                   WSSMAC_MAC_ADDR_LEN);
    WSSMAC_MEMCPY (pProbeRspStruct->unMacMsg.ProbRspMacFrame.MacMgmtFrmHdr.u1SA,
                   pProbeReqStruct->unMacMsg.ProbReqMacFrame.MacMgmtFrmHdr.u1DA,
                   WSSMAC_MAC_ADDR_LEN);

    WSSMAC_MEMCPY (pProbeRspStruct->unMacMsg.ProbRspMacFrame.
                   MacMgmtFrmHdr.u1BssId,
                   pProbeReqStruct->unMacMsg.ProbReqMacFrame.
                   MacMgmtFrmHdr.u1BssId, WSSMAC_MAC_ADDR_LEN);

    pProbeRspStruct->unMacMsg.ProbRspMacFrame.MacMgmtFrmHdr.u2MacFrameSeqCtrl =
        pProbeReqStruct->unMacMsg.ProbReqMacFrame.MacMgmtFrmHdr.
        u2MacFrameSeqCtrl;

    /*Get Wlan DB parameters */
    WSSMAC_MEMCPY (WssWlanDBMsg.WssWlanAttributeDB.BssId,
                   pProbeRspStruct->unMacMsg.ProbRspMacFrame.MacMgmtFrmHdr.
                   u1BssId, WSSMAC_MAC_ADDR_LEN);

    WssWlanDBMsg.WssWlanIsPresentDB.bWlanIfIndex = WSSMAC_TRUE;
    WssWlanDBMsg.WssWlanIsPresentDB.bDesiredSsid = WSSMAC_TRUE;
    WssWlanDBMsg.WssWlanIsPresentDB.bWssWlanQosCapab = WSSMAC_TRUE;
    WssWlanDBMsg.WssWlanIsPresentDB.bWssWlanEdcaParam = WSSMAC_TRUE;
    WssWlanDBMsg.WssWlanIsPresentDB.bCapability = WSSMAC_TRUE;
    WssWlanDBMsg.WssWlanIsPresentDB.bPowerConstraint = WSSMAC_TRUE;
    WssWlanDBMsg.WssWlanIsPresentDB.bRadioIfIndex = WSSMAC_TRUE;

    WssWlanDBMsg.WssWlanAttributeDB.u1WlanId = 1;
    WssWlanDBMsg.WssWlanAttributeDB.u1RadioId = 1;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_INTERFACE_ENTRY, &WssWlanDBMsg)
        != WSSMAC_SUCCESS)
    {
        return WSSMAC_FAILURE;
    }

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY, &WssWlanDBMsg)
        != WSSMAC_SUCCESS)
    {
        return WSSMAC_FAILURE;
    }
    /*Get Radio If DB parameters */
    RadioIfGetDB.RadioIfGetAllDB.u1RadioId =
        WssWlanDBMsg.WssWlanAttributeDB.u1RadioId;

    RadioIfGetDB.RadioIfIsGetAllDB.bBeaconPeriod = WSSMAC_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bSupportedRate = WSSMAC_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bCountryString = WSSMAC_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bMultiDomainInfo = WSSMAC_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bCurrentChannel = WSSMAC_TRUE;

    if (WssIfProcessRadioIfDBMsg (WSS_GET_PHY_RADIO_IF_DB, &RadioIfGetDB) !=
        WSSMAC_SUCCESS)
    {
        return WSSMAC_FAILURE;
    }

    /* Get probe response frame body details */
    MEMCPY (pProbeRspStruct->unMacMsg.ProbRspMacFrame.TimeStamp.au1MacFrameTimestamp, "00000000", 8);    /* Need to calculate */

    pProbeRspStruct->unMacMsg.ProbRspMacFrame.BeaconInt.u2MacFrameBeaconInt =
        RadioIfGetDB.RadioIfGetAllDB.u2BeaconPeriod;

    pProbeRspStruct->unMacMsg.ProbRspMacFrame.Capability.u2MacFrameCapability =
        WssWlanDBMsg.WssWlanAttributeDB.u2Capability;

    pProbeRspStruct->unMacMsg.ProbRspMacFrame.Ssid.u1MacFrameElemId =
        WSSMAC_SSID_ELEMENT_ID;
    pProbeRspStruct->unMacMsg.ProbRspMacFrame.Ssid.u1MacFrameElemLen =
        STRLEN (WssWlanDBMsg.WssWlanAttributeDB.au1DesiredSsid);
    MEMCPY (pProbeRspStruct->unMacMsg.ProbRspMacFrame.Ssid.au1MacFrameSSID,
            WssWlanDBMsg.WssWlanAttributeDB.au1DesiredSsid,
            STRLEN (WssWlanDBMsg.WssWlanAttributeDB.au1DesiredSsid));

    pProbeRspStruct->unMacMsg.ProbRspMacFrame.SuppRate.u1MacFrameElemId =
        WSSMAC_SUPP_RATE_ELEMENT_ID;
    pProbeRspStruct->unMacMsg.ProbRspMacFrame.SuppRate.u1MacFrameElemLen =
        STRLEN (RadioIfGetDB.RadioIfGetAllDB.au1SupportedRate);
    MEMCPY (pProbeRspStruct->unMacMsg.ProbRspMacFrame.SuppRate.
            au1MacFrameSuppRate,
            RadioIfGetDB.RadioIfGetAllDB.au1SupportedRate,
            STRLEN (RadioIfGetDB.RadioIfGetAllDB.au1SupportedRate));

    pProbeRspStruct->unMacMsg.ProbRspMacFrame.DSParamSet.u1MacFrameElemId =
        WSSMAC_DS_PARAM_ELEMENT_ID;
    pProbeRspStruct->unMacMsg.ProbRspMacFrame.DSParamSet.u1MacFrameElemLen = 1;
    pProbeRspStruct->unMacMsg.ProbRspMacFrame.DSParamSet.u1MacFrameDS_CurChannel
        = RadioIfGetDB.RadioIfGetAllDB.u1CurrentChannel;

    pProbeRspStruct->unMacMsg.ProbRspMacFrame.Country.u1MacFrameElemId =
        WSSMAC_COUNTRY_ELEMENT_ID;
    pProbeRspStruct->unMacMsg.ProbRspMacFrame.Country.u1MacFrameElemLen = 6;
    u2Count = 0;
    while (u2Count < WSSMAC_MAX_COUNTRY_STR_LEN)
    {
        pProbeRspStruct->unMacMsg.ProbRspMacFrame.Country.
            au1MacFrameCountryStr[u2Count]
            = RadioIfGetDB.RadioIfGetAllDB.au1CountryString[u2Count];
        u2Count++;
    }
    pProbeRspStruct->unMacMsg.ProbRspMacFrame.Country.u1MacFrameFirChanlNum_RegExtnId    /* size mismatch in radio db */
        = (UINT1) RadioIfGetDB.RadioIfGetAllDB.
        MultiDomainInfo[RADIOIF_OFFSET_B].u2FirstChannelNo;
    pProbeRspStruct->unMacMsg.ProbRspMacFrame.Country.u1MacFrameNumOfChanl_RegClass    /* size  mismatch in radio db */
        = (UINT1) RadioIfGetDB.RadioIfGetAllDB.
        MultiDomainInfo[RADIOIF_OFFSET_B].u2NoOfChannels;
    pProbeRspStruct->unMacMsg.ProbRspMacFrame.Country.u1MacFrameMaxTxPowLev_CovClass    /*size  mismatch in radio db */
        = (UINT1) RadioIfGetDB.RadioIfGetAllDB.
        MultiDomainInfo[RADIOIF_OFFSET_B].u2MaxTxPowerLevel;

    pProbeRspStruct->unMacMsg.ProbRspMacFrame.PowConstraint.u1MacFrameElemId =
        WSSMAC_POWCONST_ELEMENT_ID;
    pProbeRspStruct->unMacMsg.ProbRspMacFrame.PowConstraint.u1MacFrameElemLen =
        sizeof (UINT1);
    pProbeRspStruct->unMacMsg.ProbRspMacFrame.PowConstraint.
        u1MacFrameLocPowConst =
        WssWlanDBMsg.WssWlanAttributeDB.u1PowerConstraint;

#if WSSMAC_UNUSED_CODE
    /* ChanlSwitAnnounce: not available in DB */
    pProbeRspStruct->unMacMsg.ProbRspMacFrame.ChanlSwitAnnounce.u1MacFrameElemId
        = WSSMAC_CHSWITCHANN_ELEMENT_ID;
    pProbeRspStruct->unMacMsg.ProbRspMacFrame.ChanlSwitAnnounce.
        u1MacFrameElemLen = 3;
    pProbeRspStruct->unMacMsg.ProbRspMacFrame.ChanlSwitAnnounce.
        u1MacFrameChanlSwit_Mode = 0;
    pProbeRspStruct->unMacMsg.ProbRspMacFrame.ChanlSwitAnnounce.
        u1MacFrameChanlSwit_NewChanlNum = 0;
    pProbeRspStruct->unMacMsg.ProbRspMacFrame.ChanlSwitAnnounce.
        u1MacFrameChanlSwit_Cnt = 0;

    /* Quiet: not available in DB */
    pProbeRspStruct->unMacMsg.ProbRspMacFrame.Quiet.u1MacFrameElemId =
        WSSMAC_QUIET_ELEMENT_ID;
    pProbeRspStruct->unMacMsg.ProbRspMacFrame.Quiet.u1MacFrameElemLen = 6;
    pProbeRspStruct->unMacMsg.ProbRspMacFrame.Quiet.u1MacFrameQuiet_Count = 0;
    pProbeRspStruct->unMacMsg.ProbRspMacFrame.Quiet.u1MacFrameQuiet_Period = 0;
    pProbeRspStruct->unMacMsg.ProbRspMacFrame.Quiet.u2MacFrameQuiet_Duration
        = 0;
    pProbeRspStruct->unMacMsg.ProbRspMacFrame.Quiet.u2MacFrameQuiet_Offset = 0;

    /* TPCReport: not available in DB */
    pProbeRspStruct->unMacMsg.ProbRspMacFrame.TPCReport.u1MacFrameElemId =
        WSSMAC_TPC_REPORT_ELEMENT_ID;
    pProbeRspStruct->unMacMsg.ProbRspMacFrame.TPCReport.u1MacFrameElemLen = 2;
    pProbeRspStruct->unMacMsg.ProbRspMacFrame.TPCReport.u1MacFrameTPCRep_TxPow =
        0;
    pProbeRspStruct->unMacMsg.ProbRspMacFrame.TPCReport.
        u1MacFrameTPCRep_LnkMargin = 0;
    /* ExtSuppRates: not available in DB */
    if (pProbeReqStruct->unMacMsg.ProbReqMacFrame.ExtSuppRates.u1MacFrameElemId
        == WSSMAC_EXT_SUPP_RATE_ELEMENTID)
        pProbeRspStruct->unMacMsg.ProbRspMacFrame.ExtSuppRates =
            pProbeReqStruct->unMacMsg.ProbReqMacFrame.ExtSuppRates;

    pProbeRspStruct->unMacMsg.ProbRspMacFrame.BSSLoad.u1MacFrameElemId =
        WSSMAC_BSS_LOAD_ELEMENT_ID;
    pProbeRspStruct->unMacMsg.ProbRspMacFrame.BSSLoad.u1MacFrameElemLen = 5;
    pProbeRspStruct->unMacMsg.ProbRspMacFrame.BSSLoad.u2MacFrameBSSLoad_StatnCnt
        = 0;
    pProbeRspStruct->unMacMsg.ProbRspMacFrame.BSSLoad.
        u1MacFrameBSSLoad_ChanlUtil = 0;
    pProbeRspStruct->unMacMsg.ProbRspMacFrame.BSSLoad.u2MacFrameBSSLoad_AdmnCap
        = 0;
#endif

    pProbeRspStruct->unMacMsg.ProbRspMacFrame.EDCAParam.u1MacFrameElemId =
        WSSMAC_EDCA_PARAM_ELEMENT_ID;
    pProbeRspStruct->unMacMsg.ProbRspMacFrame.EDCAParam.u1MacFrameElemLen = 18;
    pProbeRspStruct->unMacMsg.ProbRspMacFrame.EDCAParam.u1MacFrameEDCA_QosInfo =
        WssWlanDBMsg.WssWlanAttributeDB.WssWlanQosCapab.u1QosInfo;
    MEMCPY (&pProbeRspStruct->unMacMsg.ProbRspMacFrame.EDCAParam.
            MacFrameEDCA_AC_BE,
            &WssWlanDBMsg.WssWlanAttributeDB.WssWlanEdcaParam.
            AC_BE_ParameterRecord, sizeof (tAC_BE));
    MEMCPY (&pProbeRspStruct->unMacMsg.ProbRspMacFrame.EDCAParam.
            MacFrameEDCA_AC_BK,
            &WssWlanDBMsg.WssWlanAttributeDB.WssWlanEdcaParam.
            AC_BK_ParameterRecord, sizeof (tAC_BK));
    MEMCPY (&pProbeRspStruct->unMacMsg.ProbRspMacFrame.EDCAParam.
            MacFrameEDCA_AC_VI,
            &WssWlanDBMsg.WssWlanAttributeDB.WssWlanEdcaParam.
            AC_VI_ParameterRecord, sizeof (tAC_VI));
    MEMCPY (&pProbeRspStruct->unMacMsg.ProbRspMacFrame.EDCAParam.
            MacFrameEDCA_AC_VO,
            &WssWlanDBMsg.WssWlanAttributeDB.WssWlanEdcaParam.
            AC_VO_ParameterRecord, sizeof (tAC_VO));

    pProbeRspStruct->unMacMsg.ProbRspMacFrame.HTCapabilities.u1MacFrameElemId =
        WSSMAC_HT_CAPAB_ELEMENT_ID;
    pProbeRspStruct->unMacMsg.ProbRspMacFrame.HTCapabilities.u1MacFrameElemLen
        = 28;
    pProbeRspStruct->unMacMsg.ProbRspMacFrame.HTCapabilities.u1AMPDUParam =
        RadioIfGetDB.RadioIfGetAllDB.u1AMPDUSupport;
    MEMCPY (&pProbeRspStruct->unMacMsg.ProbRspMacFrame.HTCapabilities.
            au1SuppMCSSet,
            &RadioIfGetDB.RadioIfGetAllDB.au1MCSRateSet,
            STRLEN (RadioIfGetDB.RadioIfGetAllDB.au1MCSRateSet));

#if WSSMAC_UNUSED_CODE
    /* VendSpecInfo: not available in wlan and radio db */
    u2Count1 = 0;
    while (u2Count1 < WSSMAC_VEND_INFO_ELEM)    /* Need to udpate */
    {
        if (pProbeReqStruct->unMacMsg.ProbReqMacFrame.aVendSpecInfo[u2Count1].
            u1MacFrameElemId == WSSMAC_VENDOR_INFO_ELEMENTID)
        {
            pProbeRspStruct->unMacMsg.ProbRspMacFrame.aVendSpecInfo[u2Count1]
                = pProbeReqStruct->unMacMsg.ProbReqMacFrame.
                aVendSpecInfo[u2Count1];
        }
        u2Count1++;
    }

    /* ReqInfo: not available in wlan and radio db */
    if (pProbeReqStruct->unMacMsg.ProbReqMacFrame.ReqInfo.u1MacFrameElemId == WSSMAC_REQINFO_ELEMENTID)    /* Need to udpate */
    {
        u2Count = 0;
        while (u2Count < pProbeReqStruct->unMacMsg.ProbReqMacFrame.ReqInfo.
               u1MacFrameElemLen)
        {
            pProbeRspStruct->unMacMsg.ProbRspMacFrame.aReqInfo[u2Count].
                u1MacFrameElemId = WSSMAC_REQINFO_ELEMENTID;
            pProbeRspStruct->unMacMsg.ProbRspMacFrame.aReqInfo[u2Count].
                u1MacFrameElemLen = 0;
            pProbeRspStruct->unMacMsg.ProbRspMacFrame.aReqInfo[u2Count].
                au1MacFrameReqID[u2Count] = 0;

            u2Count++;
        }

    }
#endif

    return WSSMAC_SUCCESS;
}

/* ***************************************************************************
 *                                                                           *
 * Function     : WssMacAssembleProbeRspFrame                                *
 *                                                                           *
 * Description  : Assemble probe response structure                          *
 *                                                                           *
 * Input(s)     : Union of struct with probe response structure and char     *
 *                buffer                                                     *
 *                                                                           *
 * Output(s)    : Probe response Char buffer                                 *
 *                                                                           *
 * Returns      : NONE                                                       *
 *                                                                           *
 * ***************************************************************************/
VOID
WssMacAssembleProbeRspFrame (tWssMacMsgStruct * pProbeRspStruct,
                             tWssMacMsgStruct * pProbeRspBuf)
{
    UINT1              *pu1ReadPtr = NULL;
    UINT4               u4Len = 0;
    UINT1              *pu1pBuf = NULL;
    UINT2               u2Count = 0, u2Len = 0;
    UINT1               u1ElementId = 0;
    tWssWlanDB          WssWlanDBMsg;
    pu1pBuf = UtlShMemAllocCapwapPktBuf ();

    if (pu1pBuf == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC, "WssMacAssembleProbeRspFrame:"
                    "UtlShMemAllocCapwapPktBuf Allocation failed \r\n");
        return;
    }

    MEMSET (&WssWlanDBMsg, 0, sizeof (tWssWlanDB));
    MEMSET (pu1pBuf, 0, WSSMAC_MAX_PKT_LEN);

    pu1ReadPtr = pu1pBuf;

    /* Put the 802.11 MAC Header */
    WSSMAC_PUT_2BYTES (pu1ReadPtr, pProbeRspStruct->unMacMsg.ProbRspMacFrame.MacMgmtFrmHdr.u2MacFrameCtrl);    /* Put Frame control field */
    WSSMAC_PUT_2BYTES (pu1ReadPtr, pProbeRspStruct->unMacMsg.ProbRspMacFrame.MacMgmtFrmHdr.u2MacFrameDurId);    /* Put Duration ID field */
    WSSMAC_PUT_NBYTES (pu1ReadPtr, pProbeRspStruct->unMacMsg.ProbRspMacFrame.MacMgmtFrmHdr.u1DA, WSSMAC_MAC_ADDR_LEN);    /* Put Addr1 field */
    WSSMAC_PUT_NBYTES (pu1ReadPtr, pProbeRspStruct->unMacMsg.ProbRspMacFrame.MacMgmtFrmHdr.u1SA, WSSMAC_MAC_ADDR_LEN);    /* Put Addr2 field */
    WSSMAC_PUT_NBYTES (pu1ReadPtr, pProbeRspStruct->unMacMsg.ProbRspMacFrame.MacMgmtFrmHdr.u1BssId, WSSMAC_MAC_ADDR_LEN);    /* Put Addr3 field */
    WSSMAC_PUT_2BYTES (pu1ReadPtr, pProbeRspStruct->unMacMsg.ProbRspMacFrame.MacMgmtFrmHdr.u2MacFrameSeqCtrl);    /* Put Sequence control field */

    /* Put the 802.11 MAC Frame Body */
    /* Put Timestamp */
    WSSMAC_PUT_NBYTES (pu1ReadPtr, pProbeRspStruct->unMacMsg.ProbRspMacFrame.
                       TimeStamp.au1MacFrameTimestamp, WSSMAC_TIMESTAMP_LEN);

    /* Put Beacon Interval */
    WSSMAC_PUT_2BYTES (pu1ReadPtr, pProbeRspStruct->unMacMsg.ProbRspMacFrame.
                       BeaconInt.u2MacFrameBeaconInt);

    /* Put Capability info */
    WSSMAC_PUT_2BYTES (pu1ReadPtr, pProbeRspStruct->unMacMsg.ProbRspMacFrame.
                       Capability.u2MacFrameCapability);

    /* Put SSID */
    u1ElementId =
        pProbeRspStruct->unMacMsg.ProbRspMacFrame.Ssid.u1MacFrameElemId;
    if (u1ElementId == WSSMAC_SSID_ELEMENT_ID)
    {
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pProbeRspStruct->unMacMsg.ProbRspMacFrame.
                          Ssid.u1MacFrameElemId);
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pProbeRspStruct->unMacMsg.ProbRspMacFrame.
                          Ssid.u1MacFrameElemLen);
        u2Len =
            pProbeRspStruct->unMacMsg.ProbRspMacFrame.Ssid.u1MacFrameElemLen;
        WSSMAC_PUT_NBYTES (pu1ReadPtr,
                           pProbeRspStruct->unMacMsg.ProbRspMacFrame.Ssid.
                           au1MacFrameSSID, u2Len);
    }
    /* Put Supported rates */
    u1ElementId =
        pProbeRspStruct->unMacMsg.ProbRspMacFrame.SuppRate.u1MacFrameElemId;
    if (u1ElementId == WSSMAC_SUPP_RATE_ELEMENT_ID)
    {
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pProbeRspStruct->unMacMsg.ProbRspMacFrame.
                          SuppRate.u1MacFrameElemId);
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pProbeRspStruct->unMacMsg.ProbRspMacFrame.
                          SuppRate.u1MacFrameElemLen);
        u2Len = pProbeRspStruct->unMacMsg.ProbRspMacFrame.SuppRate.
            u1MacFrameElemLen;
        u2Count = 0;

        while (u2Count < u2Len)
        {
            WSSMAC_PUT_1BYTE (pu1ReadPtr, pProbeRspStruct->unMacMsg.
                              ProbRspMacFrame.SuppRate.
                              au1MacFrameSuppRate[u2Count]);
            u2Count++;
        }
    }

    /* Put DS parameters */
    u1ElementId =
        pProbeRspStruct->unMacMsg.ProbRspMacFrame.DSParamSet.u1MacFrameElemId;
    if (u1ElementId == WSSMAC_DS_PARAM_ELEMENT_ID)
    {
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pProbeRspStruct->unMacMsg.ProbRspMacFrame.
                          DSParamSet.u1MacFrameElemId);
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pProbeRspStruct->unMacMsg.ProbRspMacFrame.
                          DSParamSet.u1MacFrameElemLen);
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pProbeRspStruct->unMacMsg.ProbRspMacFrame.
                          DSParamSet.u1MacFrameDS_CurChannel);
    }

    /* Put Country info */
    u1ElementId =
        pProbeRspStruct->unMacMsg.ProbRspMacFrame.Country.u1MacFrameElemId;
    if (u1ElementId == WSSMAC_COUNTRY_ELEMENT_ID)
    {
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pProbeRspStruct->unMacMsg.ProbRspMacFrame.
                          Country.u1MacFrameElemId);
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pProbeRspStruct->unMacMsg.ProbRspMacFrame.
                          Country.u1MacFrameElemLen);
        u2Count = 0;
        while (u2Count < WSSMAC_MAX_COUNTRY_STR_LEN)
        {
            WSSMAC_PUT_1BYTE (pu1ReadPtr, pProbeRspStruct->unMacMsg.
                              ProbRspMacFrame.Country.
                              au1MacFrameCountryStr[u2Count]);
            u2Count++;
        }
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pProbeRspStruct->unMacMsg.ProbRspMacFrame.
                          Country.u1MacFrameFirChanlNum_RegExtnId);
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pProbeRspStruct->unMacMsg.ProbRspMacFrame.
                          Country.u1MacFrameNumOfChanl_RegClass);
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pProbeRspStruct->unMacMsg.ProbRspMacFrame.
                          Country.u1MacFrameMaxTxPowLev_CovClass);
    }

#if WSSMAC_UNUSED_CODE
    /* Put Power constraint */
    u1ElementId = pProbeRspStruct->unMacMsg.ProbRspMacFrame.PowConstraint.
        u1MacFrameElemId;
    if (u1ElementId == WSSMAC_POWCONST_ELEMENT_ID)
    {
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pProbeRspStruct->unMacMsg.ProbRspMacFrame.
                          PowConstraint.u1MacFrameElemId);
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pProbeRspStruct->unMacMsg.ProbRspMacFrame.
                          PowConstraint.u1MacFrameElemLen);
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pProbeRspStruct->unMacMsg.ProbRspMacFrame.
                          PowConstraint.u1MacFrameLocPowConst);
    }

    /* Put channel switch announcement */
    u1ElementId = pProbeRspStruct->unMacMsg.ProbRspMacFrame.ChanlSwitAnnounce.
        u1MacFrameElemId;
    if (u1ElementId == WSSMAC_CHSWITCHANN_ELEMENT_ID)
    {
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pProbeRspStruct->unMacMsg.ProbRspMacFrame.
                          ChanlSwitAnnounce.u1MacFrameElemId);
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pProbeRspStruct->unMacMsg.ProbRspMacFrame.
                          ChanlSwitAnnounce.u1MacFrameElemLen);
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pProbeRspStruct->unMacMsg.ProbRspMacFrame.
                          ChanlSwitAnnounce.u1MacFrameChanlSwit_Mode);
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pProbeRspStruct->unMacMsg.ProbRspMacFrame.
                          ChanlSwitAnnounce.u1MacFrameChanlSwit_NewChanlNum);
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pProbeRspStruct->unMacMsg.ProbRspMacFrame.
                          ChanlSwitAnnounce.u1MacFrameChanlSwit_Cnt);
    }

    /* Put Quiet */
    u1ElementId = pProbeRspStruct->unMacMsg.ProbRspMacFrame.Quiet.
        u1MacFrameElemId;
    if (u1ElementId == WSSMAC_QUIET_ELEMENT_ID)
    {
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pProbeRspStruct->unMacMsg.
                          ProbRspMacFrame.Quiet.u1MacFrameElemId);
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pProbeRspStruct->unMacMsg.
                          ProbRspMacFrame.Quiet.u1MacFrameElemLen);
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pProbeRspStruct->unMacMsg.
                          ProbRspMacFrame.Quiet.u1MacFrameQuiet_Count);
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pProbeRspStruct->unMacMsg.
                          ProbRspMacFrame.Quiet.u1MacFrameQuiet_Period);
        WSSMAC_PUT_2BYTES (pu1ReadPtr, pProbeRspStruct->unMacMsg.
                           ProbRspMacFrame.Quiet.u2MacFrameQuiet_Duration);
        WSSMAC_PUT_2BYTES (pu1ReadPtr, pProbeRspStruct->unMacMsg.
                           ProbRspMacFrame.Quiet.u2MacFrameQuiet_Offset);
    }

    /* Put TPC Report */
    u1ElementId = pProbeRspStruct->unMacMsg.ProbRspMacFrame.TPCReport.
        u1MacFrameElemId;
    if (u1ElementId == WSSMAC_TPC_REPORT_ELEMENT_ID)
    {
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pProbeRspStruct->unMacMsg.
                          ProbRspMacFrame.TPCReport.u1MacFrameElemId);
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pProbeRspStruct->unMacMsg.
                          ProbRspMacFrame.TPCReport.u1MacFrameElemLen);
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pProbeRspStruct->unMacMsg.
                          ProbRspMacFrame.TPCReport.u1MacFrameTPCRep_TxPow);
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pProbeRspStruct->unMacMsg.
                          ProbRspMacFrame.TPCReport.u1MacFrameTPCRep_LnkMargin);
    }

    /* Get the Extended Supported Rates details */
    u1ElementId = pProbeRspStruct->unMacMsg.ProbRspMacFrame.ExtSuppRates.
        u1MacFrameElemId;
    if (u1ElementId == WSSMAC_EXT_SUPP_RATE_ELEMENTID)
    {
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pProbeRspStruct->unMacMsg.ProbRspMacFrame.
                          ExtSuppRates.u1MacFrameElemId);
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pProbeRspStruct->unMacMsg.ProbRspMacFrame.
                          ExtSuppRates.u1MacFrameElemLen);
        u2Count = 0;
        u2Len = pProbeRspStruct->unMacMsg.ProbRspMacFrame.ExtSuppRates.
            u1MacFrameElemLen;
        while (u2Count < u2Len)
        {
            WSSMAC_PUT_1BYTE (pu1ReadPtr, pProbeRspStruct->unMacMsg.
                              ProbRspMacFrame.ExtSuppRates.
                              au1MacFrameExtSuppRates[u2Count]);
            u2Count++;
        }
    }

    while (u2Count < WSSMAC_MAX_COUNTRY_STR_LEN)
    {
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pProbeRspStruct->unMacMsg.
                          ProbRspMacFrame.Country.
                          au1MacFrameCountryStr[u2Count]);
        u2Count++;
    }
    WSSMAC_PUT_1BYTE (pu1ReadPtr, pProbeRspStruct->unMacMsg.ProbRspMacFrame.
                      Country.u1MacFrameFirChanlNum_RegExtnId);
    WSSMAC_PUT_1BYTE (pu1ReadPtr, pProbeRspStruct->unMacMsg.ProbRspMacFrame.
                      Country.u1MacFrameNumOfChanl_RegClass);
    WSSMAC_PUT_1BYTE (pu1ReadPtr, pProbeRspStruct->unMacMsg.ProbRspMacFrame.
                      Country.u1MacFrameMaxTxPowLev_CovClass);
}
#endif

#if WSSMAC_UNUSED_CODE
    /* Put EDCA Parameters */
u1ElementId = pProbeRspStruct->unMacMsg.ProbRspMacFrame.EDCAParam.
    u1MacFrameElemId;
if (u1ElementId == WSSMAC_EDCA_PARAM_ELEMENT_ID)
{
    WSSMAC_PUT_1BYTE (pu1ReadPtr, pProbeRspStruct->unMacMsg.ProbRspMacFrame.
                      EDCAParam.u1MacFrameElemId);
    WSSMAC_PUT_1BYTE (pu1ReadPtr, pProbeRspStruct->unMacMsg.ProbRspMacFrame.
                      EDCAParam.u1MacFrameElemLen);
    WSSMAC_PUT_1BYTE (pu1ReadPtr, pProbeRspStruct->unMacMsg.ProbRspMacFrame.
                      EDCAParam.u1MacFrameEDCA_QosInfo);
    WSSMAC_PUT_1BYTE (pu1ReadPtr, pProbeRspStruct->unMacMsg.ProbRspMacFrame.
                      EDCAParam.u1MacFrameEDCA_Rsrvd);
    WSSMAC_PUT_1BYTE (pu1ReadPtr, pProbeRspStruct->unMacMsg.ProbRspMacFrame.
                      EDCAParam.MacFrameEDCA_AC_BE.u1MacFrameEDCA_AC_ACI_AFSN);
    WSSMAC_PUT_1BYTE (pu1ReadPtr, pProbeRspStruct->unMacMsg.ProbRspMacFrame.
                      EDCAParam.MacFrameEDCA_AC_BE.
                      u1MacFrameEDCA_AC_ECWMin_Max);
    WSSMAC_PUT_2BYTES (pu1ReadPtr,
                       pProbeRspStruct->unMacMsg.ProbRspMacFrame.EDCAParam.
                       MacFrameEDCA_AC_BE.u2MacFrameEDCA_AC_TxOpLimit);
    WSSMAC_PUT_1BYTE (pu1ReadPtr,
                      pProbeRspStruct->unMacMsg.ProbRspMacFrame.EDCAParam.
                      MacFrameEDCA_AC_BK.u1MacFrameEDCA_AC_ACI_AFSN);
    WSSMAC_PUT_1BYTE (pu1ReadPtr,
                      pProbeRspStruct->unMacMsg.ProbRspMacFrame.EDCAParam.
                      MacFrameEDCA_AC_BK.u1MacFrameEDCA_AC_ECWMin_Max);
    WSSMAC_PUT_2BYTES (pu1ReadPtr,
                       pProbeRspStruct->unMacMsg.ProbRspMacFrame.EDCAParam.
                       MacFrameEDCA_AC_BK.u2MacFrameEDCA_AC_TxOpLimit);

    WSSMAC_PUT_1BYTE (pu1ReadPtr, pProbeRspStruct->unMacMsg.ProbRspMacFrame.
                      EDCAParam.MacFrameEDCA_AC_VI.u1MacFrameEDCA_AC_ACI_AFSN);
    WSSMAC_PUT_1BYTE (pu1ReadPtr, pProbeRspStruct->unMacMsg.ProbRspMacFrame.
                      EDCAParam.MacFrameEDCA_AC_VI.
                      u1MacFrameEDCA_AC_ECWMin_Max);
    WSSMAC_PUT_2BYTES (pu1ReadPtr,
                       pProbeRspStruct->unMacMsg.ProbRspMacFrame.EDCAParam.
                       MacFrameEDCA_AC_VI.u2MacFrameEDCA_AC_TxOpLimit);

    WSSMAC_PUT_1BYTE (pu1ReadPtr, pProbeRspStruct->unMacMsg.ProbRspMacFrame.
                      EDCAParam.MacFrameEDCA_AC_VO.u1MacFrameEDCA_AC_ACI_AFSN);
    WSSMAC_PUT_1BYTE (pu1ReadPtr, pProbeRspStruct->unMacMsg.ProbRspMacFrame.
                      EDCAParam.MacFrameEDCA_AC_VO.
                      u1MacFrameEDCA_AC_ECWMin_Max);
    WSSMAC_PUT_2BYTES (pu1ReadPtr,
                       pProbeRspStruct->unMacMsg.ProbRspMacFrame.EDCAParam.
                       MacFrameEDCA_AC_VO.u2MacFrameEDCA_AC_TxOpLimit);
}
#endif

    /* Put HT capabilities */
u1ElementId = pProbeRspStruct->unMacMsg.ProbRspMacFrame.HTCapabilities.
    u1MacFrameElemId;
if (u1ElementId == WSSMAC_HT_CAPAB_ELEMENT_ID)
{
    WSSMAC_PUT_1BYTE (pu1ReadPtr, pProbeRspStruct->unMacMsg.ProbRspMacFrame.
                      HTCapabilities.u1MacFrameElemId);
    WSSMAC_PUT_1BYTE (pu1ReadPtr, pProbeRspStruct->unMacMsg.ProbRspMacFrame.
                      HTCapabilities.u1MacFrameElemLen);
    WSSMAC_PUT_2BYTES (pu1ReadPtr, pProbeRspStruct->unMacMsg.ProbRspMacFrame.
                       HTCapabilities.u2HTCapInfo);
    WSSMAC_PUT_1BYTE (pu1ReadPtr, pProbeRspStruct->unMacMsg.ProbRspMacFrame.
                      HTCapabilities.u1AMPDUParam);
    WSSMAC_PUT_NBYTES (pu1ReadPtr, pProbeRspStruct->unMacMsg.ProbRspMacFrame.
                       HTCapabilities.au1SuppMCSSet, WSSMAC_SUPP_MCS_SET);
    WSSMAC_PUT_2BYTES (pu1ReadPtr, pProbeRspStruct->unMacMsg.ProbRspMacFrame.
                       HTCapabilities.u2HTExtCap);
    WSSMAC_PUT_4BYTES (pu1ReadPtr, pProbeRspStruct->unMacMsg.ProbRspMacFrame.
                       HTCapabilities.u4TranBeamformCap);
    WSSMAC_PUT_1BYTE (pu1ReadPtr, pProbeRspStruct->unMacMsg.ProbRspMacFrame.
                      HTCapabilities.u1ASELCap);
}

    /* Put HT operation */
u1ElementId =
    pProbeRspStruct->unMacMsg.ProbRspMacFrame.HTOperation.u1MacFrameElemId;
if (u1ElementId == WSSMAC_HT_OPE_ELEMENT_ID)
{
    WSSMAC_PUT_1BYTE (pu1ReadPtr, pProbeRspStruct->unMacMsg.ProbRspMacFrame.
                      HTOperation.u1MacFrameElemId);
    WSSMAC_PUT_1BYTE (pu1ReadPtr, pProbeRspStruct->unMacMsg.ProbRspMacFrame.
                      HTOperation.u1MacFrameElemLen);
    WSSMAC_PUT_1BYTE (pu1ReadPtr, pProbeRspStruct->unMacMsg.ProbRspMacFrame.
                      HTOperation.u1PrimaryCh);
    WSSMAC_PUT_NBYTES (pu1ReadPtr, pProbeRspStruct->unMacMsg.ProbRspMacFrame.
                       HTOperation.au1HTOpeInfo, WSSMAC_HTOPE_INFO);
    WSSMAC_PUT_NBYTES (pu1ReadPtr, pProbeRspStruct->unMacMsg.ProbRspMacFrame.
                       HTOperation.au1BasicMCSSet, WSSMAC_BASIC_MCS_SET);
}

    /* Put HT 20/40 BSS Coexistence */
u1ElementId = pProbeRspStruct->unMacMsg.ProbRspMacFrame.BssCoexistence.
    u1MacFrameElemId;
if (u1ElementId == WSSMAC_BSS_COEXIST_ELEMENT_ID)
{
    WSSMAC_PUT_1BYTE (pu1ReadPtr, pProbeRspStruct->unMacMsg.ProbRspMacFrame.
                      BssCoexistence.u1MacFrameElemId);
    WSSMAC_PUT_1BYTE (pu1ReadPtr, pProbeRspStruct->unMacMsg.ProbRspMacFrame.
                      BssCoexistence.u1MacFrameElemLen);
    WSSMAC_PUT_1BYTE (pu1ReadPtr, pProbeRspStruct->unMacMsg.ProbRspMacFrame.
                      BssCoexistence.u1BSSCoexistInfoFields);
}

    /* Put OBSS Scan parameters */
u1ElementId = pProbeRspStruct->unMacMsg.ProbRspMacFrame.OBSSScanParams.
    u1MacFrameElemId;
if (u1ElementId == WSSMAC_OBSS_SCAN_PARAM_ELEMENT_ID)
{
    WSSMAC_PUT_1BYTE (pu1ReadPtr, pProbeRspStruct->unMacMsg.ProbRspMacFrame.
                      OBSSScanParams.u1MacFrameElemId);
    WSSMAC_PUT_1BYTE (pu1ReadPtr, pProbeRspStruct->unMacMsg.ProbRspMacFrame.
                      OBSSScanParams.u1MacFrameElemLen);
    WSSMAC_PUT_2BYTES (pu1ReadPtr, pProbeRspStruct->unMacMsg.ProbRspMacFrame.
                       OBSSScanParams.u2OBSSScanPassiveDwell);
    WSSMAC_PUT_2BYTES (pu1ReadPtr, pProbeRspStruct->unMacMsg.ProbRspMacFrame.
                       OBSSScanParams.u2OBSSScanActiveDwell);
    WSSMAC_PUT_2BYTES (pu1ReadPtr, pProbeRspStruct->unMacMsg.ProbRspMacFrame.
                       OBSSScanParams.u2BSSChaWidTriScanInt);
    WSSMAC_PUT_2BYTES (pu1ReadPtr, pProbeRspStruct->unMacMsg.ProbRspMacFrame.
                       OBSSScanParams.u2OBSSScanPassiveTotalPerCh);
    WSSMAC_PUT_2BYTES (pu1ReadPtr, pProbeRspStruct->unMacMsg.ProbRspMacFrame.
                       OBSSScanParams.u2OBSSScanActiveTotalPerCh);
    WSSMAC_PUT_2BYTES (pu1ReadPtr, pProbeRspStruct->unMacMsg.ProbRspMacFrame.
                       OBSSScanParams.u2BSSWidChTransDelayFactor);
    WSSMAC_PUT_2BYTES (pu1ReadPtr, pProbeRspStruct->unMacMsg.ProbRspMacFrame.
                       OBSSScanParams.u2OBSSScanActivityThreshold);
}

    /* Put Extended Capabilities */
u1ElementId = pProbeRspStruct->unMacMsg.ProbRspMacFrame.ExtCapabilities.
    u1MacFrameElemId;
if (u1ElementId == WSSMAC_EXT_CAPAB_ELEMENT_ID)
{
    WSSMAC_PUT_1BYTE (pu1ReadPtr, pProbeRspStruct->unMacMsg.ProbRspMacFrame.
                      ExtCapabilities.u1MacFrameElemId);
    WSSMAC_PUT_1BYTE (pu1ReadPtr, pProbeRspStruct->unMacMsg.ProbRspMacFrame.
                      ExtCapabilities.u1MacFrameElemLen);
    WSSMAC_PUT_NBYTES (pu1ReadPtr, pProbeRspStruct->unMacMsg.ProbRspMacFrame.
                       ExtCapabilities.au1Capabilities, WSSMAC_EXT_CAP);
}

#if WSSMAC_UNUSED_CODE
    /* Put vendor info */
u2Count1 = 0;
while (u2Count1 < WSSMAC_VEND_INFO_ELEM)
{
    u1ElementId = pProbeRspStruct->unMacMsg.ProbRspMacFrame.
        aVendSpecInfo[u2Count1].u1MacFrameElemId;
    if (u1ElementId == WSSMAC_VENDOR_INFO_ELEMENTID)
    {
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pProbeRspStruct->unMacMsg.
                          ProbRspMacFrame.aVendSpecInfo[u2Count1].
                          u1MacFrameElemId);
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          pProbeRspStruct->unMacMsg.ProbRspMacFrame.
                          aVendSpecInfo[u2Count1].u1MacFrameElemLen);
        WSSMAC_PUT_NBYTES (pu1ReadPtr,
                           pProbeRspStruct->unMacMsg.ProbRspMacFrame.
                           aVendSpecInfo[u2Count1].au1MacFrameVend_OUI,
                           WSSMAC_MAX_OUI_SIZE);
        u2Count = 0;
        u2Len = pProbeRspStruct->unMacMsg.ProbRspMacFrame.
            aVendSpecInfo[u2Count1].u1MacFrameElemLen - WSSMAC_MAX_OUI_SIZE;
        while (u2Count < u2Len)
        {
            WSSMAC_PUT_1BYTE (pu1ReadPtr, pProbeRspStruct->unMacMsg.
                              ProbRspMacFrame.aVendSpecInfo[u2Count1].
                              au1MacFrameVend_Content[u2Count]);
            u2Count++;
        }
    }
    u2Count1++;
}

    /* Put Request information */
u2Count1 = 0;
while (u2Count1 < n)
{
    u1ElementId = pProbeRspStruct->unMacMsg.ProbRspMacFrame.
        aReqInfo[u2Count1].u1MacFrameElemId;
    if (u1ElementId == WSSMAC_REQINFO_ELEMENTID)
    {
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pProbeRspStruct->unMacMsg.
                          ProbRspMacFrame.aReqInfo[u2Count1].u1MacFrameElemId);
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pProbeRspStruct->unMacMsg.
                          ProbRspMacFrame.aReqInfo[u2Count1].u1MacFrameElemLen);
        u2Count = 0;
        u2Len = pProbeRspStruct->unMacMsg.ProbRspMacFrame.
            aReqInfo[u2Count1].u1MacFrameElemLen;
        while (u2Count < u2Len)
        {
            WSSMAC_PUT_1BYTE (pu1ReadPtr, pProbeRspStruct->unMacMsg.
                              ProbRspMacFrame.aReqInfo[u2Count1].
                              au1MacFrameReqID[u2Count]);
            u2Count++;
        }
    }
    u2Count1++;
}
#endif

u4Len = pu1ReadPtr - pu1pBuf;

    /* Allocate memory for packet linear buffer */
pProbeRspBuf->unMacMsg.MacDot11PktBuf.pDot11MacPdu =
WSSMAC_ALLOCATE_CRU_BUF (u4Len);

if (pProbeRspBuf->unMacMsg.MacDot11PktBuf.pDot11MacPdu == NULL)
{
    WSSMAC_TRC (WSSMAC_FAILURE_TRC, "Memory Allocation Failed \r\n");
    UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1pBuf);
    return;
}

WSSMAC_COPY_TO_BUF (pProbeRspBuf->unMacMsg.MacDot11PktBuf.pDot11MacPdu,
                    pu1pBuf, 0, u4Len);

STRCPY (WssWlanDBMsg.WssWlanAttributeDB.BssId,
        pProbeRspStruct->unMacMsg.ProbRspMacFrame.MacMgmtFrmHdr.u1BssId);
WssWlanDBMsg.WssWlanIsPresentDB.bBssIfIndex = WSSMAC_TRUE;
WssWlanDBMsg.WssWlanIsPresentDB.bWlanIfIndex = WSSMAC_TRUE;

if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSSID_MAPPING_ENTRY,
                              &WssWlanDBMsg) != WSSMAC_SUCCESS)
{
    UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1pBuf);
    return;
}

pProbeRspBuf->unMacMsg.MacDot11PktBuf.u4IfIndex =
    WssWlanDBMsg.WssWlanAttributeDB.u4WlanIfIndex;
UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1pBuf);
return;
}

/* ***************************************************************************
 *                                                                           *
 * Function     : WssMacProcessAuthenMsg                                     *
 *                                                                           *
 * Description  : Process the authentication frame                           *
 *                                                                           *
 * Input(s)     : MAC message type and union of struct with CRU_BUF          *
 *                                                                           *
 * Output(s)    : NONE                                                       *
 *                                                                           *
 * Returns      : SUCCESS/FAILURE                                            *
 *                                                                           *
 * ***************************************************************************/
UINT1
WssMacProcessAuthenMsg (UINT1 eMsgType, tWssMacMsgStruct * pMsgStruct)
{
    unApHdlrMsgStruct   CapwapMsgStruct;

    MEMSET (&CapwapMsgStruct, 0, sizeof (unApHdlrMsgStruct));
    UNUSED_PARAM (eMsgType);

    CapwapMsgStruct.ApHdlrQueueReq.pRcvBuf =
        pMsgStruct->unMacMsg.MacDot11PktBuf.pDot11MacPdu;
    CapwapMsgStruct.ApHdlrQueueReq.u4MsgType = WSS_APHDLR_CAPWAP_MAC_MSG;
    if (pMsgStruct->unMacMsg.MacDot11PktBuf.pDot11MacPdu == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC, "Received the NULL Pointer \r\n");
    }
    if (WssIfProcessApHdlrMsg (WSS_APHDLR_CAPWAP_MAC_MSG, &CapwapMsgStruct) !=
        WSSMAC_SUCCESS)
    {
        return WSSMAC_FAILURE;
    }

    return WSSMAC_SUCCESS;
}

/* ***************************************************************************
 *                                                                           *
 * Function     : WssMacProcessCapwapMsg                                     *
 *                                                                           *
 * Description  : Process the Capwap message                                 *
 *                                                                           *
 * Input(s)     : MAC message type and union of struct with CRU_BUF          *
 *                                                                           *
 * Output(s)    : NONE                                                       *
 *                                                                           *
 * Returns      : SUCCESS/FAILURE                                            *
 *                                                                           *
 * ***************************************************************************/
UINT1
WssMacProcessCapwapMsg (UINT1 eMsgType, tWssMacMsgStruct * pMsgStruct)
{
    unApHdlrMsgStruct   ApHdlrMsgStruct;
    tWssWlanDB          WssWlanDBMsg;
    tMacAddr            BssId = { 0 };
    UINT2               u2PacketLen = 0;
    UINT2               u2Index;
    UINT1              *pu1ReadPtr = NULL, *pTempPtr = NULL;
    UINT1              *pu1Frame = NULL;
    UINT1               au1String[WSSMAC_MAC_STR_LEN];
    UINT4               u4WlanIndex = 0;
    UINT4               u4Len = 0;
    tMacAddr            DestMac = { 0 };
    tMacAddr            BSSIDMac = { 0 };
    tMacAddr            SrcMac = { 0 };
    tWssWlanDB          WssWlanDB;
    tWssIfPMDB          WssIfPMDB;
    MEMSET (&ApHdlrMsgStruct, 0, sizeof (unApHdlrMsgStruct));
    MEMSET (&WssWlanDBMsg, 0, sizeof (tWssWlanDB));
    MEMSET (&WssWlanDB, 0, sizeof (tWssWlanDB));
    UNUSED_PARAM (eMsgType);
    MEMSET (&BssId, 0, sizeof (tMacAddr));
    MEMSET (&WssIfPMDB, 0, sizeof (tWssIfPMDB));
    MEMSET (&au1String, 0, WSSMAC_MAC_STR_LEN);
    pu1Frame = UtlShMemAllocCapwapPktBuf ();

    if (pu1Frame == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC, "WssMacProcessCapwapMsg:"
                    "UtlShMemAllocCapwapPktBuf Allocation failed \r\n");
        WSSMAC_RELEASE_CRU_BUF (pMsgStruct->unMacMsg.MacDot11PktBuf.
                                pDot11MacPdu);
        return WSSMAC_FAILURE;
    }

    /* Update the u4IfIndex with the BssIfIndex value */
    u2PacketLen =
        (UINT2) WSSMAC_GET_BUF_LEN (pMsgStruct->unMacMsg.MacDot11PktBuf.
                                    pDot11MacPdu);

    WSSMAC_COPY_FROM_BUF (pMsgStruct->unMacMsg.MacDot11PktBuf.pDot11MacPdu,
                          pu1Frame, 0, u2PacketLen);
    pu1ReadPtr = pu1Frame;

    if (pMsgStruct->unMacMsg.MacDot11PktBuf.u1MacType == WSS_MAC_LOCALMAC_MODE)
    {
        MEMCPY (&BssId, &(pMsgStruct->unMacMsg.MacDot11PktBuf.BssIdMac),
                sizeof (tMacAddr));

        if (WssMacDot3ToDot11 (&pu1ReadPtr, BssId, &u2PacketLen)
            != OSIX_SUCCESS)
        {
            WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                        "WssMacProcessCapwapMsg: WssMacDot3ToDot11 FAILED \r\n");
            UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1Frame);
            WSSMAC_RELEASE_CRU_BUF (pMsgStruct->unMacMsg.MacDot11PktBuf.
                                    pDot11MacPdu);
            return WSSMAC_FAILURE;
        }

        pTempPtr = pu1ReadPtr;

        u4Len = (UINT4) u2PacketLen;
        ApHdlrMsgStruct.WssMacMsgStruct.unMacMsg.MacDot11PktBuf.pDot11MacPdu
            = WSSMAC_ALLOCATE_CRU_BUF (u4Len);
        if (ApHdlrMsgStruct.WssMacMsgStruct.unMacMsg.MacDot11PktBuf.
            pDot11MacPdu == NULL)
        {
            WSSMAC_TRC (WSSMAC_FAILURE_TRC, "WssMacProcessCapwapMsg :: "
                        "Memory Allocation Failure \r\n");
            WSSMAC_RELEASE_CRU_BUF (pMsgStruct->unMacMsg.MacDot11PktBuf.
                                    pDot11MacPdu);
            UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1Frame);
            return WSSMAC_FAILURE;
        }

        WSSMAC_COPY_TO_BUF (ApHdlrMsgStruct.WssMacMsgStruct.unMacMsg.
                            MacDot11PktBuf.pDot11MacPdu,
                            pu1ReadPtr, 0, u2PacketLen);

        WSSMAC_RELEASE_CRU_BUF (pMsgStruct->unMacMsg.MacDot11PktBuf.
                                pDot11MacPdu);
    }
    else if (pMsgStruct->unMacMsg.MacDot11PktBuf.u1MacType ==
             WSS_MAC_SPLITMAC_MODE)
    {
        MEMCPY (&BssId, pu1ReadPtr + WSSMAC_SMAC_IND, sizeof (tMacAddr));
        ApHdlrMsgStruct.WssMacMsgStruct.unMacMsg.MacDot11PktBuf.pDot11MacPdu =
            pMsgStruct->unMacMsg.MacDot11PktBuf.pDot11MacPdu;
    }

    MEMCPY (&DestMac, pu1ReadPtr + WSSMAC_BSSID_IND, sizeof (tMacAddr));
    MEMCPY (&BSSIDMac, pu1ReadPtr + WSSMAC_SMAC_IND, sizeof (tMacAddr));
    MEMCPY (&SrcMac, pu1ReadPtr + WSSMAC_DMAC_IND, sizeof (tMacAddr));

    MEMCPY (&(WssWlanDBMsg.WssWlanAttributeDB.BssId), &BssId,
            sizeof (tMacAddr));
    WssWlanDBMsg.WssWlanIsPresentDB.bWlanIfIndex = WSSMAC_TRUE;

    if ((MEMCMP (BssId, "\0\0\0\0\0\0", sizeof (tMacAddr))) != 0)
    {
        if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSSID_MAPPING_ENTRY,
                                      &WssWlanDBMsg) != WSSMAC_SUCCESS)
        {
            WSSMAC_TRC (WSSMAC_FAILURE_TRC, "WssMacProcessCapwapMsg: "
                        "WssIfProcessWssWlanDBMsg returned FAILURE \r\n");
            WssMacMacToStr (WssWlanDBMsg.WssWlanAttributeDB.BssId, au1String);
            WSSMAC_TRC1 (WSSMAC_FAILURE_TRC, "WLAN BSSID %s \r\n", au1String);
            WssMacMacToStr (SrcMac, au1String);
            WSSMAC_TRC1 (WSSMAC_FAILURE_TRC, "SRC MAC %s \r\n", au1String);
            WssMacMacToStr (DestMac, au1String);
            WSSMAC_TRC1 (WSSMAC_FAILURE_TRC, "DST MAC %s \r\n", au1String);

            for (u2Index = 0; u2Index <= u2PacketLen; u2Index++)
            {
                WSSMAC_TRC4 (WSSMAC_FAILURE_TRC, "%2x %2x %2x %2x\n",
                             *(pu1ReadPtr + 1),
                             *(pu1ReadPtr + 2),
                             *(pu1ReadPtr + 3), *(pu1ReadPtr + 4));

                pu1ReadPtr = pu1ReadPtr + 4;

                WSSMAC_TRC (WSSMAC_FAILURE_TRC, "\n");
            }

            UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1Frame);
            WSSMAC_RELEASE_CRU_BUF (ApHdlrMsgStruct.WssMacMsgStruct.unMacMsg.
                                    MacDot11PktBuf.pDot11MacPdu);

            return WSSMAC_FAILURE;
        }
        ApHdlrMsgStruct.WssMacMsgStruct.unMacMsg.MacDot11PktBuf.u4IfIndex =
            WssWlanDBMsg.WssWlanAttributeDB.u4WlanIfIndex;
    }

    if ((MEMCMP (BSSIDMac, "\0\0\0\0\0\0", sizeof (tMacAddr))) == 0)
    {
        if ((DestMac[0] == WSSMAC_BROADCAST_ADDR) || (DestMac[0] == 0x01))
        {
            for (u4WlanIndex = 1; u4WlanIndex <= 1; u4WlanIndex++)
            {
                WssWlanDB.WssWlanAttributeDB.u1RadioId = 1;
                WssWlanDB.WssWlanAttributeDB.u1WlanId = u4WlanIndex;
                WssWlanDB.WssWlanIsPresentDB.bBssId = OSIX_TRUE;
                WssWlanDB.WssWlanIsPresentDB.bWlanIfIndex = WSSMAC_TRUE;

                if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_INTERFACE_ENTRY,
                                              &WssWlanDB) != OSIX_FAILURE)
                {
                    if (ApHdlrMsgStruct.WssMacMsgStruct.unMacMsg.MacDot11PktBuf.
                        pDot11MacPdu == NULL)
                    {
                        WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                                    "WssMacProcessCapwapMsg :: "
                                    "Memory Allocation Failure \r\n");
                        UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1Frame);
                        return WSSMAC_FAILURE;
                    }

                    WSSMAC_COPY_TO_BUF (ApHdlrMsgStruct.WssMacMsgStruct.
                                        unMacMsg.MacDot11PktBuf.pDot11MacPdu,
                                        WssWlanDB.WssWlanAttributeDB.BssId,
                                        WSSMAC_SMAC_IND, WSSMAC_MAC_ADDR_LEN);
                    ApHdlrMsgStruct.WssMacMsgStruct.unMacMsg.MacDot11PktBuf.
                        u4IfIndex = WssWlanDB.WssWlanAttributeDB.u4WlanIfIndex;
                    ApHdlrMsgStruct.WssMacMsgStruct.unMacMsg.MacDot11PktBuf.
                        u1MacType = LOCAL_ROUTING_DISABLED;

                    if (WssIfProcessApHdlrMsg (WSS_APHDLR_CFA_TX_BUF,
                                               &ApHdlrMsgStruct) !=
                        WSSMAC_SUCCESS)
                    {
                        WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                                    "WssMacProcessCapwapMsg:"
                                    " WssIfProcessApHdlrMsg returns FAILURE \r\n");
                        UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1Frame);
                        WSSMAC_RELEASE_CRU_BUF (ApHdlrMsgStruct.WssMacMsgStruct.
                                                unMacMsg.MacDot11PktBuf.
                                                pDot11MacPdu);
                        return WSSMAC_FAILURE;
                    }
                }
                else
                {
                    WSSMAC_TRC (WSSMAC_FAILURE_TRC, "WssMacProcessCapwapMsg:"
                                " WssIfProcessWssWlanDBMsg returns FAILURE \r\n");
                    UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1Frame);
                    WSSMAC_RELEASE_CRU_BUF (ApHdlrMsgStruct.WssMacMsgStruct.
                                            unMacMsg.MacDot11PktBuf.
                                            pDot11MacPdu);
                    return WSSMAC_FAILURE;

                }
            }                    /*for (u4WlanIndex = 1; u4WlanIndex <= 1; u4WlanIndex++ ) */
        }                        /* if ((DestMac[0] == 0xff) || (DestMac[0] == 0x01)) */
    }
    else
    {
        ApHdlrMsgStruct.WssMacMsgStruct.unMacMsg.MacDot11PktBuf.
            u1MacType = LOCAL_ROUTING_DISABLED;
        if (WssIfProcessApHdlrMsg (WSS_APHDLR_CFA_TX_BUF, &ApHdlrMsgStruct) !=
            WSSMAC_SUCCESS)
        {
            WSSMAC_TRC (WSSMAC_FAILURE_TRC, "WssMacProcessCapwapMsg:"
                        " WssIfProcessApHdlrMsg returned FAILURE \r\n");
            UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1Frame);
            WSSMAC_RELEASE_CRU_BUF (ApHdlrMsgStruct.WssMacMsgStruct.unMacMsg.
                                    MacDot11PktBuf.pDot11MacPdu);
            return WSSMAC_FAILURE;
        }
    }
#if 0                            /*Commented temporarily for PING issue */
    MEMSET (&WssWlanDB, 0, sizeof (tWssWlanDB));
    MEMCPY (&(WssWlanDB.WssWlanAttributeDB.BssId),
            (pMsgStruct->unMacMsg.MacDot11PktBuf.BssIdMac), sizeof (tMacAddr));
    WssWlanDB.WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSSID_MAPPING_ENTRY,
                                  &WssWlanDB) != WSSMAC_SUCCESS)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC, "WssMacProcessCapwapMsg: "
                    "WssIfProcessWssWlanDBMsg returned FAILURE \r\n");
        UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1Frame);
        return WSSMAC_FAILURE;
    }
    WssIfPMDB.u2ProfileBSSID = WssWlanDB.WssWlanAttributeDB.u4WlanIfIndex;

    if (WssIfProcessPMDBMsg (WSS_PM_WTP_EVT_VSP_BSSID_STATS_GET, &WssIfPMDB) !=
        OSIX_SUCCESS)
    {
        UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1Frame);
        return OSIX_FAILURE;
    }

    WssIfPMDB.tWtpVSPBSSIDStats.wlanBSSIDDataPktSentCount++;
    if (WssIfProcessPMDBMsg (WSS_PM_WTP_EVT_VSP_BSSID_STATS_SET, &WssIfPMDB) !=
        OSIX_SUCCESS)
    {
        UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1Frame);
        return OSIX_FAILURE;
    }
#endif
    UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1Frame);
    UNUSED_PARAM (pTempPtr);
    return WSSMAC_SUCCESS;
}

#ifdef WPS_WTP_WANTED
/* ***************************************************************************
 *                                                                           *
 * Function     : WssMacProcessWpsProbeReq*
 *                                                                           *
 * Description  : Process Association request frame                          *
 *                                                                           *
 * Input(s)     : MAC message type and union of struct with CRU_BUF          *
 *                                                                           *
 * Output(s)    : NONE                                                       *
 *                                                                           *
 * Returns      : SUCCESS/FAILURE                                            *
 *                                                                           *
 * ***************************************************************************/
UINT1
WssMacProcessWpsProbeReq (UINT1 eMsgType, tWssMacMsgStruct * pMsgStruct)
{
    unApHdlrMsgStruct   CapwapMsgStruct;

    MEMSET (&CapwapMsgStruct, 0, sizeof (unApHdlrMsgStruct));
    UNUSED_PARAM (eMsgType);

    CapwapMsgStruct.ApHdlrQueueReq.pRcvBuf =
        pMsgStruct->unMacMsg.MacDot11PktBuf.pDot11MacPdu;
    CapwapMsgStruct.ApHdlrQueueReq.u4MsgType = WSS_APHDLR_CAPWAP_MAC_MSG;

    if (WssIfProcessApHdlrMsg (WSS_APHDLR_CAPWAP_MAC_MSG, &CapwapMsgStruct) !=
        WSSMAC_SUCCESS)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC, "WssMacProcessAssocReq:-"
                    "WssIfProcessApHdlrMsg returned FAILURE\r\n");
        return WSSMAC_FAILURE;
    }

    return WSSMAC_SUCCESS;
}
#endif
/* ***************************************************************************
 *                                                                           *
 * Function     : WssMacProcessAssocReq                                      *
 *                                                                           *
 * Description  : Process Association request frame                          *
 *                                                                           *
 * Input(s)     : MAC message type and union of struct with CRU_BUF          *
 *                                                                           *
 * Output(s)    : NONE                                                       *
 *                                                                           *
 * Returns      : SUCCESS/FAILURE                                            *
 *                                                                           *
 * ***************************************************************************/
UINT1
WssMacProcessAssocReq (UINT1 eMsgType, tWssMacMsgStruct * pMsgStruct)
{
    unApHdlrMsgStruct   CapwapMsgStruct;

    MEMSET (&CapwapMsgStruct, 0, sizeof (unApHdlrMsgStruct));
    UNUSED_PARAM (eMsgType);

    CapwapMsgStruct.ApHdlrQueueReq.pRcvBuf =
        pMsgStruct->unMacMsg.MacDot11PktBuf.pDot11MacPdu;
    CapwapMsgStruct.ApHdlrQueueReq.u4MsgType = WSS_APHDLR_CAPWAP_MAC_MSG;

    if (WssIfProcessApHdlrMsg (WSS_APHDLR_CAPWAP_MAC_MSG, &CapwapMsgStruct) !=
        WSSMAC_SUCCESS)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC, "WssMacProcessAssocReq:-"
                    "WssIfProcessApHdlrMsg returned FAILURE\r\n");
        return WSSMAC_FAILURE;
    }

    return WSSMAC_SUCCESS;
}

/* ***************************************************************************
 *                                                                           *
 * Function     : WssMacProcessReassocReq                                    *
 *                                                                           *
 * Description  : Process Reassociation request frame                        *
 *                                                                           *
 * Input(s)     : MAC message type and union of struct with CRU_BUF          *
 *                                                                           *
 * Output(s)    : NONE                                                       *
 *                                                                           *
 * Returns      : SUCCESS/FAILURE                                            *
 *                                                                           *
 * ***************************************************************************/
UINT1
WssMacProcessReassocReq (UINT1 eMsgType, tWssMacMsgStruct * pMsgStruct)
{
    unApHdlrMsgStruct   CapwapMsgStruct;

    MEMSET (&CapwapMsgStruct, 0, sizeof (unApHdlrMsgStruct));
    UNUSED_PARAM (eMsgType);

    CapwapMsgStruct.ApHdlrQueueReq.pRcvBuf =
        pMsgStruct->unMacMsg.MacDot11PktBuf.pDot11MacPdu;
    CapwapMsgStruct.ApHdlrQueueReq.u4MsgType = WSS_APHDLR_CAPWAP_MAC_MSG;
    if (WssIfProcessApHdlrMsg (WSS_APHDLR_CAPWAP_MAC_MSG, &CapwapMsgStruct) !=
        WSSMAC_SUCCESS)
    {
        return WSSMAC_FAILURE;
    }

    return WSSMAC_SUCCESS;
}

/* ***************************************************************************
 *                                                                           *
 * Function     : WssMacProcessDeauthenMsg                                   *
 *                                                                           *
 * Description  : Process Deauthentication frame                             *
 *                                                                           *
 * Input(s)     : MAC message type and union of struct with CRU_BUF          *
 *                                                                           *
 * Output(s)    : NONE                                                       *
 *                                                                           *
 * Returns      : SUCCESS/FAILURE                                            *
 *                                                                           *
 * ***************************************************************************/
UINT1
WssMacProcessDeauthenMsg (UINT1 eMsgType, tWssMacMsgStruct * pMsgStruct)
{
    unApHdlrMsgStruct   CapwapMsgStruct;
    UINT2               u2Count = 0, u2Count1 = 0;
    UINT1              *pu1ReadPtr = NULL;
    UINT1              *pu1Frame = NULL;
    UINT2               u2Len = 0;
    UINT4               u4Len = 0;
    UINT1               u1ElementId = 0;
    tWssMacMsgStruct   *pDeauthMsgStruct = NULL;

    UNUSED_PARAM (eMsgType);
    pDeauthMsgStruct =
        (tWssMacMsgStruct *) (VOID *) UtlShMemAllocMacMsgStructBuf ();
    if (pDeauthMsgStruct == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                    "WssMacProcessDeauthenMsg:-"
                    "UtlShMemAllocMacMsgStructBuf returned failure\n");
        return OSIX_FAILURE;
    }

    MEMSET (&CapwapMsgStruct, 0x00, sizeof (unApHdlrMsgStruct));
    MEMSET (pDeauthMsgStruct, 0x00, sizeof (tWssMacMsgStruct));

    pu1Frame = UtlShMemAllocCapwapPktBuf ();

    if (pu1Frame == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC, "WssMacProcessDeauthenMsg:"
                    "UtlShMemAllocCapwapPktBuf Allocation failed \r\n");
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pDeauthMsgStruct);
        return WSSMAC_FAILURE;
    }
    if (pMsgStruct->msgType == 0)    /* Deauth Msg received from STA */
    {
        CapwapMsgStruct.ApHdlrQueueReq.pRcvBuf =
            pMsgStruct->unMacMsg.MacDot11PktBuf.pDot11MacPdu;
        CapwapMsgStruct.ApHdlrQueueReq.u4MsgType = WSS_APHDLR_CAPWAP_MAC_MSG;
        /* Send it to CAPWAP to be sent to WLC */
        if (WssIfProcessApHdlrMsg (WSS_APHDLR_CAPWAP_MAC_MSG, &CapwapMsgStruct)
            != WSSMAC_SUCCESS)
        {
            WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                        "Deauth: WSS_APHDLR_CAPWAP_MAC_MSG Failed \r\n");
            UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1Frame);
            UtlShMemFreeMacMsgStructBuf ((UINT1 *) pDeauthMsgStruct);
            return WSSMAC_FAILURE;
        }
    }
    else                        /* Deauth Msg received from WSS STA Module after Delete WLAN */
    {
        MEMSET (pu1Frame, 0, WSSMAC_MAX_PKT_LEN);

        pu1ReadPtr = pu1Frame;
        /* Get the 802.11 MAC Header */
        pMsgStruct->unMacMsg.DeauthMacFrame.MacMgmtFrmHdr.u2MacFrameCtrl =
            WSSMAC_SET_DEAUTH_FRAME_CTRL;
        WSSMAC_PUT_2BYTES (pu1ReadPtr, pMsgStruct->unMacMsg.DeauthMacFrame.MacMgmtFrmHdr.u2MacFrameCtrl);    /* Get Frame control field */
        WSSMAC_PUT_2BYTES (pu1ReadPtr, pMsgStruct->unMacMsg.DeauthMacFrame.MacMgmtFrmHdr.u2MacFrameDurId);    /* Get Duration ID field */
        WSSMAC_PUT_NBYTES (pu1ReadPtr, pMsgStruct->unMacMsg.DeauthMacFrame.MacMgmtFrmHdr.u1DA, WSSMAC_MAC_ADDR_LEN);    /* Get Address1 field */
        WSSMAC_PUT_NBYTES (pu1ReadPtr, pMsgStruct->unMacMsg.DeauthMacFrame.MacMgmtFrmHdr.u1SA, WSSMAC_MAC_ADDR_LEN);    /* Get Address2 field */
        WSSMAC_PUT_NBYTES (pu1ReadPtr, pMsgStruct->unMacMsg.DeauthMacFrame.MacMgmtFrmHdr.u1BssId, WSSMAC_MAC_ADDR_LEN);    /*Get Addr3 field */
        WSSMAC_PUT_2BYTES (pu1ReadPtr, pMsgStruct->unMacMsg.DeauthMacFrame.MacMgmtFrmHdr.u2MacFrameSeqCtrl);    /*Get Sequence control field */

        WSSMAC_PUT_2BYTES (pu1ReadPtr, pMsgStruct->unMacMsg.DeauthMacFrame.ReasonCode.u2MacFrameReasonCode);    /* Get Reason code */

        /*Put Vendor info */
        u2Count1 = 0;
        while (u2Count1 < WSSMAC_VEND_INFO_ELEM)
        {
            u1ElementId = pMsgStruct->unMacMsg.DeauthMacFrame.
                aVendSpecInfo[u2Count1].u1MacFrameElemId;
            if (u1ElementId == WSSMAC_VENDOR_INFO_ELEMENTID)
            {
                WSSMAC_PUT_1BYTE (pu1ReadPtr,
                                  pMsgStruct->unMacMsg.DeauthMacFrame.
                                  aVendSpecInfo[u2Count1].u1MacFrameElemId);
                WSSMAC_PUT_1BYTE (pu1ReadPtr,
                                  pMsgStruct->unMacMsg.DeauthMacFrame.
                                  aVendSpecInfo[u2Count1].u1MacFrameElemLen);
                WSSMAC_PUT_NBYTES (pu1ReadPtr,
                                   pMsgStruct->unMacMsg.DeauthMacFrame.
                                   aVendSpecInfo[u2Count1].au1MacFrameVend_OUI,
                                   WSSMAC_MAX_OUI_SIZE);
                u2Count = 0;
                u2Len = pMsgStruct->unMacMsg.DeauthMacFrame.
                    aVendSpecInfo[u2Count1].u1MacFrameElemLen -
                    WSSMAC_MAX_OUI_SIZE;

                while (u2Count < u2Len)
                {
                    WSSMAC_PUT_1BYTE (pu1ReadPtr, pMsgStruct->unMacMsg.
                                      DeauthMacFrame.aVendSpecInfo[u2Count1].
                                      au1MacFrameVend_Content[u2Count]);
                    u2Count++;
                }
            }
            u2Count1++;
        }
        u4Len = pu1ReadPtr - pu1Frame;

        /* Allocate memory for packet linear buffer */
        pDeauthMsgStruct->unMacMsg.MacDot11PktBuf.pDot11MacPdu =
            WSSMAC_ALLOCATE_CRU_BUF (u4Len);
        if (pDeauthMsgStruct->unMacMsg.MacDot11PktBuf.pDot11MacPdu == NULL)
        {
            WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                        "Deauth: Memory Allocation Failed \r\n");
            UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1Frame);
            UtlShMemFreeMacMsgStructBuf ((UINT1 *) pDeauthMsgStruct);
            return WSSMAC_FAILURE;
        }

        WSSMAC_COPY_TO_BUF (pDeauthMsgStruct->unMacMsg.MacDot11PktBuf.
                            pDot11MacPdu, pu1Frame, 0, u4Len);

        if (pDeauthMsgStruct->unMacMsg.MacDot11PktBuf.pDot11MacPdu != NULL)
        {
            if (WssMacProcessCapwapMsg (WSS_APHDLR_CAPWAP_MAC_MSG,
                                        pDeauthMsgStruct) != WSSMAC_SUCCESS)
            {
                WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                            "Deauth: WssMacProcessCapwapMsg Failed \r\n");
                UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1Frame);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pDeauthMsgStruct);
                return WSSMAC_FAILURE;
            }
        }
        else
        {
            WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                        "Deauth: MacDot11PktBuf iS NULL \r\n");
            UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1Frame);
            UtlShMemFreeMacMsgStructBuf ((UINT1 *) pDeauthMsgStruct);
            return WSSMAC_FAILURE;
        }

    }

    UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1Frame);
    UtlShMemFreeMacMsgStructBuf ((UINT1 *) pDeauthMsgStruct);
    return WSSMAC_SUCCESS;
}

/* ***************************************************************************
 *                                                                           *
 * Function     : WssMacProcessDisassocMsg                                   *
 *                                                                           *
 * Description  : Process Disassociation frame                               *
 *                                                                           *
 * Input(s)     : MAC message type and union of struct with CRU_BUF          *
 *                                                                           *
 * Output(s)    : NONE                                                       *
 *                                                                           *
 * Returns      : SUCCESS/FAILURE                                            *
 *                                                                           *
 * ***************************************************************************/
UINT1
WssMacProcessDisassocMsg (UINT1 eMsgType, tWssMacMsgStruct * pMsgStruct)
{
    unApHdlrMsgStruct   CapwapMsgStruct;
    MEMSET (&CapwapMsgStruct, 0, sizeof (unApHdlrMsgStruct));

    UNUSED_PARAM (eMsgType);

    CapwapMsgStruct.ApHdlrQueueReq.pRcvBuf =
        pMsgStruct->unMacMsg.MacDot11PktBuf.pDot11MacPdu;
    CapwapMsgStruct.ApHdlrQueueReq.u4MsgType = WSS_APHDLR_CAPWAP_MAC_MSG;
    if (WssIfProcessApHdlrMsg (WSS_APHDLR_CAPWAP_MAC_MSG, &CapwapMsgStruct) !=
        WSSMAC_SUCCESS)
    {
        return WSSMAC_FAILURE;
    }

    return WSSMAC_SUCCESS;
}

/* ***************************************************************************
 *                                                                           *
 * Function     : WssMacProcessDataMsg                                       *
 *                                                                           *
 * Description  : Process Data frame                                         *
 *                                                                           *
 * Input(s)     : MAC message type and union of struct with CRU_BUF          *
 *                                                                           *
 * Output(s)    : NONE                                                       *
 *                                                                           *
 * Returns      : SUCCESS/FAILURE                                            *
 *                                                                           *
 * ***************************************************************************/
UINT1
WssMacProcessDataMsg (UINT1 eMsgType, tWssMacMsgStruct * pMsgStruct)
{
    unApHdlrMsgStruct   ApHdlrMsgStruct;
    tWssWlanDB          WssWlanDBMsg;
    tMacAddr            BssId;
    tMacAddr            StaMacAddr;
    UINT2               u2PacketLen = 0;
    UINT1              *pu1ReadPtr = NULL, *pTempPtr = NULL;
    UINT1              *pu1TmpReadPtr = NULL;
    UINT1              *pu1Frame = NULL;
    UINT1              *pu1TmpFrame = NULL;
    UINT1               u1AuthMethod = 0;
    tWssIfPMDB          WssIfPMDB;
    tWssWlanDB          WssWlanDB;
    UINT2               u2FrameType = 0;
    tCRU_BUF_CHAIN_HEADER *pCruBuf = NULL;

    MEMSET (&ApHdlrMsgStruct, 0, sizeof (unApHdlrMsgStruct));
    MEMSET (&WssWlanDBMsg, 0, sizeof (tWssWlanDB));
    UINT2               u2FrameCtrl = 0;
    tRadioIfGetDB       RadioIfGetDB;
    tWssIfCapDB         WssIfCapDB;
    UINT1               u1IsLocalRouting = OSIX_FALSE;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    MEMSET (&WssIfCapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&WssIfPMDB, 0, sizeof (tWssIfPMDB));
    MEMSET (&WssWlanDB, 0, sizeof (tWssWlanDB));

    UNUSED_PARAM (eMsgType);

    if (pMsgStruct == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC, "pMsgStruct is NULL \r\n");
        return WSSMAC_FAILURE;
    }

    if (pMsgStruct->unMacMsg.MacDot11PktBuf.pDot11MacPdu == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC, "pDot11MacPdu is NULL \r\n");
        return WSSMAC_FAILURE;
    }

    u2PacketLen = (UINT2) WSSMAC_GET_BUF_LEN
        (pMsgStruct->unMacMsg.MacDot11PktBuf.pDot11MacPdu);
    pu1ReadPtr = WSSMAC_BUF_IF_LINEAR
        (pMsgStruct->unMacMsg.MacDot11PktBuf.pDot11MacPdu, 0, u2PacketLen);
    pu1Frame = UtlShMemAllocCapwapPktBuf ();

    if (pu1Frame == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC, "WssMacProcessDataMsg:"
                    "UtlShMemAllocCapwapPktBuf Allocation failed \r\n");
        return WSSMAC_FAILURE;
    }
    pu1TmpFrame = UtlShMemAllocCapwapPktBuf ();

    if (pu1TmpFrame == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC, "WssMacProcessDataMsg:"
                    "UtlShMemAllocCapwapPktBuf Allocation failed \r\n");
        UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1Frame);
        return WSSMAC_FAILURE;
    }

    if (pu1ReadPtr == NULL)
    {
        WSSMAC_COPY_FROM_BUF (pMsgStruct->unMacMsg.MacDot11PktBuf.pDot11MacPdu,
                              pu1Frame, 0, u2PacketLen);
        pu1ReadPtr = pu1Frame;
    }

    MEMCPY (&StaMacAddr, pu1ReadPtr + WSSMAC_SMAC_IND, sizeof (tMacAddr));

    if (WssStaGetBssid (&StaMacAddr, &BssId) == OSIX_FAILURE)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC, "WssMacProcessDataMsg:"
                    "No BSSID found for this Station MAC addr \r\n");
        UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1Frame);
        UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1TmpFrame);
        return WSSMAC_FAILURE;
    }

    MEMCPY (pu1ReadPtr + WSSMAC_BSSID_IND, &BssId, sizeof (tMacAddr));

    MEMCPY (&(WssWlanDBMsg.WssWlanAttributeDB.BssId), &BssId,
            sizeof (tMacAddr));
    WssWlanDBMsg.WssWlanIsPresentDB.bWlanIfIndex = WSSMAC_TRUE;

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSSID_MAPPING_ENTRY,
                                  &WssWlanDBMsg) != WSSMAC_SUCCESS)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC, "WssMacProcessDataMsg:"
                    " WssIfProcessWssWlanDBMsg returned FAILURE \r\n");
        UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1Frame);
        UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1TmpFrame);
        return WSSMAC_FAILURE;
    }
    WssWlanDBMsg.WssWlanIsPresentDB.bMacType = WSSMAC_TRUE;
    WssWlanDBMsg.WssWlanIsPresentDB.bQosProfileId = WSSMAC_TRUE;
    WssWlanDBMsg.WssWlanIsPresentDB.bRadioId = WSSMAC_TRUE;
    WssWlanDBMsg.WssWlanIsPresentDB.bAuthMethod = WSSMAC_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY, &WssWlanDBMsg) !=
        WSSMAC_SUCCESS)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC, "WssMacProcessDataMsg:"
                    " WSS_WLAN_GET_IFINDEX_ENTRY Failed \r\n");
        UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1Frame);
        UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1TmpFrame);
        return WSSMAC_FAILURE;
    }
    u1AuthMethod = WssWlanDBMsg.WssWlanAttributeDB.u1AuthMethod;

    MEMCPY (&u2FrameCtrl, pu1ReadPtr, sizeof (UINT2));
    if (((u2FrameCtrl & WSSMAC_DATA_ENC_FRAME_CTRL_MASK)
         >> WSSMAC_DATA_PROT_BIT_SHIFT) == WSSMAC_FRAME_CTRL_PROT_BIT_SET)
    {
        if (u1AuthMethod == WSSWLAN_AUTH_METHOD_SHARED)
        {
            /* Turn off the Protected Bit in Frame CTrl */
            u2FrameCtrl &= WSSMAC_FRAME_CTRL_PROT_BIT_OFF;
            MEMCPY (pu1ReadPtr, &u2FrameCtrl, sizeof (UINT2));

            /* Skip the 4 Bytes for HT field at the end of the 802.11 Hdr */
            MEMCPY (pu1TmpFrame, pu1ReadPtr, WSSMAC_HT_START_IND);
            MEMCPY (pu1TmpFrame + WSSMAC_HT_START_IND,
                    pu1ReadPtr + WSSMAC_HT_END_IND,
                    (u2PacketLen - WSSMAC_HT_END_IND));
            pu1ReadPtr = pu1TmpFrame;
            u2PacketLen -= WSSMAC_HT_HDR_LEN;
            WSSMAC_COPY_TO_BUF (pMsgStruct->unMacMsg.MacDot11PktBuf.
                                pDot11MacPdu, pu1TmpFrame, 0, u2PacketLen);

            CRU_BUF_Delete_BufChainAtEnd (pMsgStruct->unMacMsg.MacDot11PktBuf.
                                          pDot11MacPdu, WSSMAC_HT_HDR_LEN);

        }
        else
        {
            /* Turn off the Protected Bit in Frame CTrl */
            u2FrameCtrl &= WSSMAC_FRAME_CTRL_PROT_BIT_OFF;
            MEMCPY (pu1ReadPtr, &u2FrameCtrl, sizeof (UINT2));
            pu1TmpReadPtr = pu1ReadPtr + WSSMAC_HT_RSNA_END_IND;
            MEMCPY ((pu1ReadPtr + WSSMAC_HT_RSNA_START_IND), pu1TmpReadPtr,
                    (u2PacketLen - WSSMAC_HT_RSNA_END_IND));
            /* Decrease the packet len accordingly,
             * 8 bytes (due to protected bit)  + 8 bytes at the
             * end (padding) have to be removed*/
            u2PacketLen = u2PacketLen - WSSMAC_HT_RSNA_HDR_LEN;

            WSSMAC_COPY_TO_BUF (pMsgStruct->unMacMsg.MacDot11PktBuf.
                                pDot11MacPdu, pu1ReadPtr, 0, u2PacketLen);

            CRU_BUF_Delete_BufChainAtEnd (pMsgStruct->unMacMsg.MacDot11PktBuf.
                                          pDot11MacPdu, WSSMAC_HT_RSNA_HDR_LEN);
        }
    }

    if (WssWlanDBMsg.WssWlanAttributeDB.u1MacType == WSS_MAC_SPLITMAC_MODE)
    {
        WSSMAC_TRC (WSSMAC_INIT_TRC, "MAC Mode = SPLIT\r\n");
    }
    else if (WssWlanDBMsg.WssWlanAttributeDB.u1MacType == WSS_MAC_LOCALMAC_MODE)
    {
        WSSMAC_TRC (WSSMAC_INIT_TRC, "MAC Mode = LOCAL\r\n");

        if (WssMacDot11ToDot3 (&pu1ReadPtr, &u2PacketLen) != OSIX_SUCCESS)
        {
            WSSMAC_TRC (WSSMAC_FAILURE_TRC, "WssMacProcessDataMsg:"
                        " WssMacDot11ToDot3 FAILED \r\n");
            UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1Frame);
            UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1TmpFrame);
            return WSSMAC_FAILURE;
        }

        pCruBuf =
            (tCRU_BUF_CHAIN_HEADER *) CRU_BUF_Allocate_MsgBufChain (u2PacketLen,
                                                                    0);
        if (pCruBuf == NULL)
        {
            WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                        "\r\n New Buffer allocation failed\r\n");
            UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1Frame);
            UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1TmpFrame);
            return WSSMAC_FAILURE;
        }
        CRU_BUF_Copy_OverBufChain (pCruBuf, pu1ReadPtr, 0, u2PacketLen);

        pCruBuf->ModuleData.InterfaceId.u4IfIndex =
            pMsgStruct->unMacMsg.MacDot11PktBuf.pDot11MacPdu->ModuleData.
            InterfaceId.u4IfIndex;

        CRU_BUF_Release_MsgBufChain (pMsgStruct->unMacMsg.MacDot11PktBuf.
                                     pDot11MacPdu, FALSE);

        pMsgStruct->unMacMsg.MacDot11PktBuf.pDot11MacPdu = pCruBuf;

        pTempPtr = pu1ReadPtr;
        /* WSSMAC_COPY_TO_BUF (pMsgStruct->unMacMsg.MacDot11PktBuf.pDot11MacPdu,
           pu1ReadPtr, 0, u2PacketLen); */

        WSSMAC_COPY_FROM_BUF (pMsgStruct->unMacMsg.MacDot11PktBuf.pDot11MacPdu,
                              pu1Frame, 0, u2PacketLen);
        pTempPtr = pu1Frame + ETY_TYPE_OFFSET;
        WSSMAC_GET_2BYTES (u2FrameType, pTempPtr);

        /* Local Routing Changes */
        WssIfCapDB.CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
        WssIfCapDB.CapwapGetDB.u2WtpInternalId = 0;
        WssIfCapDB.CapwapIsGetAllDB.bWtpLocalRouting = OSIX_TRUE;
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, &WssIfCapDB)
            != OSIX_SUCCESS)
        {
            WSSMAC_TRC (WSSMAC_FAILURE_TRC, "WssMacProcessDataMsg:"
                        "WSS_CAPWAP_GET_DB Failed\r\n");
            UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1Frame);
            UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1TmpFrame);
            return OSIX_FAILURE;
        }

        /* If Local Routing is enabled, fwd the data pkt to CFA */
        if (((WssIfCapDB.CapwapGetDB.u1WtpLocalRouting == LOCAL_ROUTING_ENABLED)
             && (u2FrameType != DOT1X_ETHTYPE)) ||
            ((WssIfCapDB.CapwapGetDB.u1WtpLocalRouting ==
              LOCAL_BRIDGING_ENABLED) && (u2FrameType != DOT1X_ETHTYPE)))
        {
            u1IsLocalRouting = OSIX_TRUE;

            WssWlanDBMsg.WssWlanIsPresentDB.bVlanId = OSIX_TRUE;
            WssWlanDBMsg.WssWlanIsPresentDB.bVlanFlag = OSIX_TRUE;
            if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY,
                                          &WssWlanDBMsg) != OSIX_SUCCESS)
            {
                WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                            "WssWlanSetVlanIdMapping: Get VlanFlag FAILED\n");
            }

            if (WssWlanDBMsg.WssWlanAttributeDB.u1VlanFlag == OSIX_TRUE)
            {
                ApHdlrMsgStruct.WssMacMsgStruct.unMacMsg.
                    MacDot11PktBuf.pDot11MacPdu =
                    pMsgStruct->unMacMsg.MacDot11PktBuf.pDot11MacPdu;
                ApHdlrMsgStruct.WssMacMsgStruct.unMacMsg.MacDot11PktBuf.
                    u4IfIndex = WssWlanDBMsg.WssWlanAttributeDB.u4WlanIfIndex;
                ApHdlrMsgStruct.WssMacMsgStruct.unMacMsg.MacDot11PktBuf.
                    u1MacType = WssIfCapDB.CapwapGetDB.u1WtpLocalRouting;
                ApHdlrMsgStruct.WssMacMsgStruct.unMacMsg.MacDot11PktBuf.
                    u2VlanId = WssWlanDBMsg.WssWlanAttributeDB.u2VlanId;

                if (WssIfProcessApHdlrMsg (WSS_APHDLR_CFA_TX_BUF,
                                           &ApHdlrMsgStruct) != WSSMAC_SUCCESS)
                {
                    WSSMAC_TRC (WSSMAC_FAILURE_TRC, "WssMacProcessDataMsg:"
                                "WSS_APHDLR_CFA_TX_BUF returned FAILURE \r\n");
                    UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1Frame);
                    UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1TmpFrame);
                    return WSSMAC_FAILURE;
                }
            }
            else
            {
                WSSMAC_TRC (WSSMAC_FAILURE_TRC, "WssMacProcessDataMsg:"
                            "Vlan Flag is FALSE is WLAN DB - FAILURE \r\n");
                UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1Frame);
                UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1TmpFrame);
                return WSSMAC_FAILURE;
            }
        }
    }

    if (u1IsLocalRouting == OSIX_FALSE)
    {
        /* Fill the QoS parameters */
        if (WssWlanDBMsg.WssWlanAttributeDB.u1QosProfileId <=
            RADIOIF_MAX_QOS_PROFILE_IND)
        {
            switch (WssWlanDBMsg.WssWlanAttributeDB.u1QosProfileId)
            {
                case WLAN_QOS_INDEX_BE:
                    RadioIfGetDB.RadioIfGetAllDB.u1QosIndex =
                        RADIO_QOS_INDEX_BE;
                    break;
                case WLAN_QOS_INDEX_VI:
                    RadioIfGetDB.RadioIfGetAllDB.u1QosIndex =
                        RADIO_QOS_INDEX_VI;
                    break;
                case WLAN_QOS_INDEX_VO:
                    RadioIfGetDB.RadioIfGetAllDB.u1QosIndex =
                        RADIO_QOS_INDEX_VO;
                    break;
                case WLAN_QOS_INDEX_BG:
                    RadioIfGetDB.RadioIfGetAllDB.u1QosIndex =
                        RADIO_QOS_INDEX_BG;
                    break;
            }

            RadioIfGetDB.RadioIfGetAllDB.u1RadioId =
                WssWlanDBMsg.WssWlanAttributeDB.u1RadioId;
            RadioIfGetDB.RadioIfIsGetAllDB.bRadioIfIndex = OSIX_TRUE;
            if (WssIfProcessRadioIfDBMsg
                (WSS_GET_PHY_RADIO_IF_DB, &RadioIfGetDB) != WSSMAC_SUCCESS)
            {
                WSSMAC_TRC (WSSMAC_FAILURE_TRC, "WssMacProcessDataMsg:"
                            " WSS_GET_PHY_RADIO_IF_DB Failed \r\n");
                UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1Frame);
                UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1TmpFrame);
                return WSSMAC_FAILURE;
            }

            RadioIfGetDB.RadioIfIsGetAllDB.bQosIndex = OSIX_TRUE;
            RadioIfGetDB.RadioIfIsGetAllDB.bDscp = OSIX_TRUE;
            RadioIfGetDB.RadioIfIsGetAllDB.bTaggingPolicy = OSIX_TRUE;
            RadioIfGetDB.RadioIfIsGetAllDB.bQosPrio = OSIX_TRUE;
            if (WssIfProcessRadioIfDBMsg
                (WSS_GET_RADIO_QOS_CONFIG_DB, &RadioIfGetDB) != WSSMAC_SUCCESS)
            {
                WSSMAC_TRC (WSSMAC_FAILURE_TRC, "WssMacProcessDataMsg:"
                            " WSS_GET_RADIO_QOS_CONFIG_DB Failed \r\n");
                UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1Frame);
                UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1TmpFrame);
                return WSSMAC_FAILURE;
            }
            if ((RadioIfGetDB.RadioIfGetAllDB.u1TaggingPolicy ==
                 WSS_MAC_QOS_DSCP_VAL) ||
                (RadioIfGetDB.RadioIfGetAllDB.u1TaggingPolicy ==
                 WSS_MAC_QOS_BOTH_VAL))
            {
                ApHdlrMsgStruct.ApHdlrQueueReq.u1DscpValue =
                    RadioIfGetDB.RadioIfGetAllDB.au1Dscp[RadioIfGetDB.
                                                         RadioIfGetAllDB.
                                                         u1QosIndex - 1];
            }
            else
            {
                ApHdlrMsgStruct.ApHdlrQueueReq.u1DscpValue = 0;
            }
        }

        ApHdlrMsgStruct.ApHdlrQueueReq.pRcvBuf =
            pMsgStruct->unMacMsg.MacDot11PktBuf.pDot11MacPdu;
        ApHdlrMsgStruct.ApHdlrQueueReq.u4MsgType = WSS_APHDLR_MAC_DATA_MSG;
        ApHdlrMsgStruct.ApHdlrQueueReq.u1MacType =
            WssWlanDBMsg.WssWlanAttributeDB.u1MacType;
        MEMCPY (&(ApHdlrMsgStruct.ApHdlrQueueReq.BssIdMac), &BssId,
                sizeof (tMacAddr));

        if (ApHdlrMsgStruct.ApHdlrQueueReq.pRcvBuf != NULL)
        {
            if (WssIfProcessApHdlrMsg
                (WSS_APHDLR_CAPWAP_MAC_MSG, &ApHdlrMsgStruct) != WSSMAC_SUCCESS)
            {
                WSSMAC_TRC (WSSMAC_FAILURE_TRC, "WssMacProcessDataMsg: "
                            "WSS_APHDLR_CAPWAP_MAC_MSG Failed \r\n");
                UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1Frame);
                UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1TmpFrame);
                return WSSMAC_FAILURE;
            }
        }
        else
        {
            WSSMAC_TRC (WSSMAC_FAILURE_TRC, "DATA: Pkt Buf iS NULL \r\n");
            UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1Frame);
            UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1TmpFrame);
            return WSSMAC_FAILURE;
        }

#if 0                            /*Commented temporarily for PING issue */
        MEMCPY (&(WssWlanDB.WssWlanAttributeDB.BssId),
                (pMsgStruct->unMacMsg.MacDot11PktBuf.BssIdMac),
                sizeof (tMacAddr));
        WssWlanDB.WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
        if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSSID_MAPPING_ENTRY,
                                      &WssWlanDB) != WSSMAC_SUCCESS)
        {
            WSSMAC_TRC (WSSMAC_FAILURE_TRC, "WssMacProcessCapwapMsg: "
                        "WssIfProcessWssWlanDBMsg returned FAILURE \r\n");
            UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1Frame);
            UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1TmpFrame);
            return WSSMAC_FAILURE;
        }
        WssIfPMDB.u2ProfileBSSID = WssWlanDB.WssWlanAttributeDB.u4WlanIfIndex;

        if (WssIfProcessPMDBMsg (WSS_PM_WTP_EVT_VSP_BSSID_STATS_GET, &WssIfPMDB)
            != OSIX_SUCCESS)
        {
            UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1Frame);
            UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1TmpFrame);
            return OSIX_FAILURE;
        }

        WssIfPMDB.tWtpVSPBSSIDStats.wlanBSSIDDataPktRcvdCount++;
        if (WssIfProcessPMDBMsg (WSS_PM_WTP_EVT_VSP_BSSID_STATS_SET, &WssIfPMDB)
            != OSIX_SUCCESS)
        {
            UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1Frame);
            UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1TmpFrame);
            return OSIX_FAILURE;
        }
#endif
    }
    UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1Frame);
    UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1TmpFrame);
    return WSSMAC_SUCCESS;
}

/***************************************************************************
 *                                                                         *
 *     Function Name : WssMacMacToStr                                      *
 *                                                                         *
 *     Description   : This function converts the given mac address        *
 *                     in to a string of form aa:aa:aa:aa:aa:aa            *
 *                                                                         *
 *     Input(s)      : pMacAdrr   : Pointer to the Mac address value array *
 *                     pu1Temp    : Pointer to the converted mac address   *
 *                                  string.(The string must be of length   *
 *                                  21 bytes minimum)                      *
 *                                                                         *
 *     Output(s)     : NULL                                                *
 *                                                                         *
 *     Returns       : NULL                                                *
 *                                                                         *
 ***************************************************************************/

VOID
WssMacMacToStr (UINT1 *pMacAddr, UINT1 *pu1Temp)
{

    UINT1               u1Byte;

    if (!(pMacAddr) || !(pu1Temp))
        return;

    for (u1Byte = 0; u1Byte < MAC_LEN; u1Byte++)
    {
        pu1Temp += SPRINTF ((CHR1 *) pu1Temp, "%02x:", *(pMacAddr + u1Byte));
    }
    SPRINTF ((CHR1 *) (pu1Temp - 1), "   ");

}

/* Msg Functions not applicable for WTP */
/* ***************************************************************************
 *                                                                           *
 * Function     : WssMacProcessAssocRsp                                      *
 *                                                                           *
 * Description  : Process Association response - not used for AP             *
 *                                                                           *
 * Input(s)     : MAC message type and union of struct with CRU_BUF          *
 *                                                                           *
 * Output(s)    : NONE                                                       *
 *                                                                           *
 * Returns      : SUCCESS                                                    *
 *                                                                           *
 * ***************************************************************************/
UINT1
WssMacProcessAssocRsp (UINT1 eMsgType, tWssMacMsgStruct * pMsgStruct)
{
    UNUSED_PARAM (eMsgType);
    UNUSED_PARAM (pMsgStruct);

    return WSSMAC_SUCCESS;
}

/* ***************************************************************************
 *                                                                           *
 * Function     : WssMacProcessReassocRsp                                    *
 *                                                                           *
 * Description  : Process Reassociation response - not used for AP           *
 *                                                                           *
 * Input(s)     : MAC message type and union of struct with CRU_BUF          *
 *                                                                           *
 * Output(s)    : NONE                                                       *
 *                                                                           *
 * Returns      : SUCCESS                                                    *
 *                                                                           *
 * ***************************************************************************/
UINT1
WssMacProcessReassocRsp (UINT1 eMsgType, tWssMacMsgStruct * pMsgStruct)
{
    UNUSED_PARAM (eMsgType);
    UNUSED_PARAM (pMsgStruct);

    return WSSMAC_SUCCESS;
}

/* Other Stubs */
/* ***************************************************************************
 *                                                                           *
 * Function     : WssMacProcessBeaconMsg                                     *
 *                                                                           *
 * Description  : Process Beacon Message       - not used for AP             *
 *                                                                           *
 * Input(s)     : MAC message type and union of struct with CRU_BUF          *
 *                                                                           *
 * Output(s)    : NONE                                                       *
 *                                                                           *
 * Returns      : SUCCESS                                                    *
 *                                                                           *
 * ***************************************************************************/
UINT1
WssMacProcessBeaconMsg (UINT1 eMsgType, tWssMacMsgStruct * pMsgStruct)
{
    UNUSED_PARAM (eMsgType);
    UNUSED_PARAM (pMsgStruct);

    return WSSMAC_SUCCESS;
}

/* ***************************************************************************
 *                                                                           *
 * Function     : WssMacProcessChanlSwitAnnounce                            *
 *                                                                           *
 * Description  : Process the CFA Msg from AP Hdlr                           *
 *                                                                           *
 * Input(s)     : MAC message type and union of struct with CRU_BUF          *
 *                                                                           *
 * Output(s)    : None                                                       *
 *                                                                           *
 * Returns      : SUCCESS/FAILURE                                            *
 *                                                                           *
 ** ***************************************************************************/
UINT1
WssMacProcessChanlSwitAnnounce (UINT1 eMsgType, tWssMacMsgStruct * pMsgStruct)
{
    UINT4               u4Len = 0;
    UINT1              *pu1Frame = NULL;
    UINT1              *pu1ReadPtr = NULL;
    UINT1               u1ElementId = 0;
    UINT1               u1Length = 3;
    tWssWlanDB          WssWlanDBMsg;
    unApHdlrMsgStruct   ChanlSwitMsgStruct;

    UNUSED_PARAM (eMsgType);
    MEMSET (&WssWlanDBMsg, 0, sizeof (tWssWlanDB));
    MEMSET (&ChanlSwitMsgStruct, 0, sizeof (unApHdlrMsgStruct));

    pu1Frame = UtlShMemAllocCapwapPktBuf ();

    if (pu1Frame == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC, "WssMacProcessChanSwitchAnnounce"
                    "UtlShMemAllocCapwapPktBuf Allocation failed \r\n");
        return WSSMAC_FAILURE;
    }

    MEMSET (pu1Frame, 0, WSSMAC_MAX_PKT_LEN);
    pu1ReadPtr = pu1Frame;
    pMsgStruct->unMacMsg.ActionChanlSwitchFrame.MacMgmtFrmHdr.u2MacFrameCtrl =
        WSSMAC_SET_ACTION_FRAME_CTRL;
    pMsgStruct->unMacMsg.ActionChanlSwitchFrame.u1CategoryCode =
        DOT11_SPECTRUM_MGMT;
    pMsgStruct->unMacMsg.ActionChanlSwitchFrame.u1ActionField =
        DOT11_CHANL_SWITCH;
    u1ElementId = WSSMAC_CHSWITCHANN_ELEMENT_ID;

    /* Frame control field */
    WSSMAC_PUT_2BYTES (pu1ReadPtr, pMsgStruct->unMacMsg.ActionChanlSwitchFrame.
                       MacMgmtFrmHdr.u2MacFrameCtrl);
    /* Duration ID field */
    WSSMAC_PUT_2BYTES (pu1ReadPtr, pMsgStruct->unMacMsg.ActionChanlSwitchFrame.
                       MacMgmtFrmHdr.u2MacFrameDurId);
    /* Address1 field - Destination MAC */
    WSSMAC_PUT_NBYTES (pu1ReadPtr, pMsgStruct->unMacMsg.ActionChanlSwitchFrame.
                       MacMgmtFrmHdr.u1DA, WSSMAC_MAC_ADDR_LEN);
    /* Address2 field - Source MAC */
    WSSMAC_PUT_NBYTES (pu1ReadPtr, pMsgStruct->unMacMsg.ActionChanlSwitchFrame.
                       MacMgmtFrmHdr.u1SA, WSSMAC_MAC_ADDR_LEN);
    /* Addr3 field - BSSID */
    WSSMAC_PUT_NBYTES (pu1ReadPtr, pMsgStruct->unMacMsg.ActionChanlSwitchFrame.
                       MacMgmtFrmHdr.u1BssId, WSSMAC_MAC_ADDR_LEN);
    /* Sequence control field */
    WSSMAC_PUT_2BYTES (pu1ReadPtr, pMsgStruct->unMacMsg.ActionChanlSwitchFrame.
                       MacMgmtFrmHdr.u2MacFrameSeqCtrl);
    /* Category */
    WSSMAC_PUT_1BYTE (pu1ReadPtr, pMsgStruct->unMacMsg.ActionChanlSwitchFrame.
                      u1CategoryCode);
    /* Action Field */
    WSSMAC_PUT_1BYTE (pu1ReadPtr, pMsgStruct->unMacMsg.ActionChanlSwitchFrame.
                      u1ActionField);
    /*Element Id */
    WSSMAC_PUT_1BYTE (pu1ReadPtr, u1ElementId);
    /*Length */
    WSSMAC_PUT_1BYTE (pu1ReadPtr, u1Length);

    WSSMAC_PUT_1BYTE (pu1ReadPtr,
                      pMsgStruct->unMacMsg.ActionChanlSwitchFrame.
                      ChanlSwitAnnounce.u1MacFrameChanlSwit_Mode);
    WSSMAC_PUT_1BYTE (pu1ReadPtr,
                      pMsgStruct->unMacMsg.ActionChanlSwitchFrame.
                      ChanlSwitAnnounce.u1MacFrameChanlSwit_NewChanlNum);
    WSSMAC_PUT_1BYTE (pu1ReadPtr,
                      pMsgStruct->unMacMsg.ActionChanlSwitchFrame.
                      ChanlSwitAnnounce.u1MacFrameChanlSwit_Cnt);

    if (pMsgStruct->unMacMsg.ActionChanlSwitchFrame.
        ChanwidthSwitch.isOptional == OSIX_TRUE)
    {
        /*Element Id */
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pMsgStruct->unMacMsg.
                          ActionChanlSwitchFrame.ChanwidthSwitch.u1ElemId);
        /*Length */
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pMsgStruct->unMacMsg.
                          ActionChanlSwitchFrame.ChanwidthSwitch.u1ElemLen);
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pMsgStruct->unMacMsg.
                          ActionChanlSwitchFrame.ChanwidthSwitch.u1ElemLen);
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pMsgStruct->unMacMsg.
                          ActionChanlSwitchFrame.ChanwidthSwitch.u1ChanWidth);
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pMsgStruct->unMacMsg.
                          ActionChanlSwitchFrame.ChanwidthSwitch.u1CenterFcy0);
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pMsgStruct->unMacMsg.
                          ActionChanlSwitchFrame.ChanwidthSwitch.u1CenterFcy1);
    }
    if (pMsgStruct->unMacMsg.ActionChanlSwitchFrame.
        VhtTxPower.isOptional == OSIX_TRUE)
    {
        /*Element Id */
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pMsgStruct->unMacMsg.
                          ActionChanlSwitchFrame.VhtTxPower.u1ElemId);
        /*Length */
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pMsgStruct->unMacMsg.
                          ActionChanlSwitchFrame.VhtTxPower.u1ElemLen);
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pMsgStruct->unMacMsg.
                          ActionChanlSwitchFrame.VhtTxPower.u1TransPower);
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pMsgStruct->unMacMsg.
                          ActionChanlSwitchFrame.VhtTxPower.u1TxPower20);
        switch (pMsgStruct->unMacMsg.ActionChanlSwitchFrame.
                VhtTxPower.u1ElemLen)
        {
            case WSSMAC_VALUE_3:
                WSSMAC_PUT_1BYTE (pu1ReadPtr, pMsgStruct->unMacMsg.
                                  ActionChanlSwitchFrame.VhtTxPower.
                                  u1TxPower40);
                break;
            case WSSMAC_VALUE_4:
                WSSMAC_PUT_1BYTE (pu1ReadPtr, pMsgStruct->unMacMsg.
                                  ActionChanlSwitchFrame.VhtTxPower.
                                  u1TxPower80);
                break;
            case WSSMAC_VALUE_5:
                WSSMAC_PUT_1BYTE (pu1ReadPtr, pMsgStruct->unMacMsg.
                                  ActionChanlSwitchFrame.VhtTxPower.
                                  u1TxPower160);
                break;
        }
    }
    u4Len = (UINT4) (pu1ReadPtr - pu1Frame);
    ChanlSwitMsgStruct.WssMacMsgStruct.unMacMsg.MacDot11PktBuf.pDot11MacPdu
        = WSSMAC_ALLOCATE_CRU_BUF (u4Len);
    if (ChanlSwitMsgStruct.WssMacMsgStruct.unMacMsg.MacDot11PktBuf.
        pDot11MacPdu == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC, "WssMacProcessChanSwitchAnnounce: "
                    "Memory Allocation Failure \r\n");
        UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1Frame);
        return WSSMAC_FAILURE;
    }
    WSSMAC_COPY_TO_BUF (ChanlSwitMsgStruct.WssMacMsgStruct.unMacMsg.
                        MacDot11PktBuf.pDot11MacPdu,
                        pu1Frame, 0, (UINT2) u4Len);
    MEMCPY (&(WssWlanDBMsg.WssWlanAttributeDB.BssId), pMsgStruct->
            unMacMsg.ActionChanlSwitchFrame.MacMgmtFrmHdr.u1BssId,
            sizeof (tMacAddr));
    WssWlanDBMsg.WssWlanIsPresentDB.bWlanIfIndex = WSSMAC_TRUE;
    WssWlanDBMsg.WssWlanIsPresentDB.bBssIfIndex = WSSMAC_TRUE;

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSSID_MAPPING_ENTRY,
                                  &WssWlanDBMsg) != WSSMAC_SUCCESS)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC, "WssMacProcessChanSwitchAnnounce:"
                    "WssIfProcessWssWlanDBMsg returned FAILURE \r\n");
        UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1Frame);
        return WSSMAC_FAILURE;
    }
    ChanlSwitMsgStruct.WssMacMsgStruct.unMacMsg.MacDot11PktBuf.u4IfIndex =
        WssWlanDBMsg.WssWlanAttributeDB.u4WlanIfIndex;
    ChanlSwitMsgStruct.WssMacMsgStruct.unMacMsg.MacDot11PktBuf.
        u1MacType = LOCAL_ROUTING_DISABLED;
    if (WssIfProcessApHdlrMsg (WSS_APHDLR_CFA_TX_BUF, &ChanlSwitMsgStruct) !=
        WSSMAC_SUCCESS)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC, "WssMacProcessChanSwitchAnnounce:"
                    " WssIfProcessApHdlrMsg returned FAILURE \r\n");
        UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1Frame);
        WSSMAC_RELEASE_CRU_BUF (ChanlSwitMsgStruct.WssMacMsgStruct.unMacMsg.
                                MacDot11PktBuf.pDot11MacPdu);
        return WSSMAC_FAILURE;
    }

    UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1Frame);
    return WSSMAC_SUCCESS;
}

/* ***************************************************************************
 *                                                                           *
 * Function     : WssMacProcessExChanlSwitAnnounce                            *
 *                                                                           *
 * Description  : Process the CFA Msg from AP Hdlr                           *
 *                                                                           *
 * Input(s)     : MAC message type and union of struct with CRU_BUF          *
 *                                                                           *
 * Output(s)    : None                                                       *
 *                                                                           *
 * Returns      : SUCCESS/FAILURE                                            *
 *                                                                           *
 ** ***************************************************************************/
UINT1
WssMacProcessExChanlSwitAnnounce (UINT1 eMsgType, tWssMacMsgStruct * pMsgStruct)
{
    UINT4               u4Len = 0;
    UINT1              *pu1Frame = NULL;
    UINT1              *pu1ReadPtr = NULL;
    UINT1               u1ElementId = 0;
    UINT1               u1Length = 0;
    tWssWlanDB          WssWlanDBMsg;
    unApHdlrMsgStruct   ChanlSwitMsgStruct;

    UNUSED_PARAM (eMsgType);
    MEMSET (&WssWlanDBMsg, 0, sizeof (tWssWlanDB));
    MEMSET (&ChanlSwitMsgStruct, 0, sizeof (unApHdlrMsgStruct));

    pu1Frame = UtlShMemAllocCapwapPktBuf ();

    if (pu1Frame == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC, "WssMacProcessChanSwitchAnnounce"
                    "UtlShMemAllocCapwapPktBuf Allocation failed \r\n");
        return WSSMAC_FAILURE;
    }

    MEMSET (pu1Frame, 0, WSSMAC_MAX_PKT_LEN);
    pu1ReadPtr = pu1Frame;
    pMsgStruct->unMacMsg.ActionChanlSwitchFrame.MacMgmtFrmHdr.u2MacFrameCtrl =
        WSSMAC_SET_ACTION_FRAME_CTRL;
    pMsgStruct->unMacMsg.ActionChanlSwitchFrame.u1CategoryCode =
        DOT11_PUBLIC_ACTION;
    pMsgStruct->unMacMsg.ActionChanlSwitchFrame.u1ActionField =
        DOT11_EX_CHAN_ACTION;
    u1ElementId = WSSMAC_CHSWITCHANN_ELEMENT_ID;

    /* Frame control field */
    WSSMAC_PUT_2BYTES (pu1ReadPtr, pMsgStruct->unMacMsg.ActionChanlSwitchFrame.
                       MacMgmtFrmHdr.u2MacFrameCtrl);
    /* Duration ID field */
    WSSMAC_PUT_2BYTES (pu1ReadPtr, pMsgStruct->unMacMsg.ActionChanlSwitchFrame.
                       MacMgmtFrmHdr.u2MacFrameDurId);
    /* Address1 field - Destination MAC */
    WSSMAC_PUT_NBYTES (pu1ReadPtr, pMsgStruct->unMacMsg.ActionChanlSwitchFrame.
                       MacMgmtFrmHdr.u1DA, WSSMAC_MAC_ADDR_LEN);
    /* Address2 field - Source MAC */
    WSSMAC_PUT_NBYTES (pu1ReadPtr, pMsgStruct->unMacMsg.ActionChanlSwitchFrame.
                       MacMgmtFrmHdr.u1SA, WSSMAC_MAC_ADDR_LEN);
    /* Addr3 field - BSSID */
    WSSMAC_PUT_NBYTES (pu1ReadPtr, pMsgStruct->unMacMsg.ActionChanlSwitchFrame.
                       MacMgmtFrmHdr.u1BssId, WSSMAC_MAC_ADDR_LEN);
    /* Sequence control field */
    WSSMAC_PUT_2BYTES (pu1ReadPtr, pMsgStruct->unMacMsg.ActionChanlSwitchFrame.
                       MacMgmtFrmHdr.u2MacFrameSeqCtrl);
    /* Category */
    WSSMAC_PUT_1BYTE (pu1ReadPtr, pMsgStruct->unMacMsg.ActionChanlSwitchFrame.
                      u1CategoryCode);
    /* Action Field */
    WSSMAC_PUT_1BYTE (pu1ReadPtr, pMsgStruct->unMacMsg.ActionChanlSwitchFrame.
                      u1ActionField);
    /*Element Id */
    WSSMAC_PUT_1BYTE (pu1ReadPtr, u1ElementId);
    /*Length */
    WSSMAC_PUT_1BYTE (pu1ReadPtr, u1Length);
    /*Channel Switch Mode */
    WSSMAC_PUT_1BYTE (pu1ReadPtr,
                      pMsgStruct->unMacMsg.ActionChanlSwitchFrame.
                      ChanlSwitAnnounce.u1MacFrameChanlSwit_Mode);
    /*New Channel Number */
    WSSMAC_PUT_1BYTE (pu1ReadPtr,
                      pMsgStruct->unMacMsg.ActionChanlSwitchFrame.
                      ChanlSwitAnnounce.u1MacFrameChanlSwit_NewChanlNum);
    /*Channel Switch Count */
    WSSMAC_PUT_1BYTE (pu1ReadPtr,
                      pMsgStruct->unMacMsg.ActionChanlSwitchFrame.
                      ChanlSwitAnnounce.u1MacFrameChanlSwit_Cnt);

/*Incomplete CSA message construction code*/

    /*Operating Class */
    WSSMAC_PUT_1BYTE (pu1ReadPtr, pMsgStruct->unMacMsg.
                      ActionChanlSwitchFrame.ChanlSwitAnnounce.u1OperClass);
    if (pMsgStruct->unMacMsg.ActionChanlSwitchFrame.
        Country.isOptional == OSIX_TRUE)
    {
        /*11d code has to be added here */
    }
    if (pMsgStruct->unMacMsg.ActionChanlSwitchFrame.
        ChanwidthSwitch.isOptional == OSIX_TRUE)
    {
        /*Element Id */
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pMsgStruct->unMacMsg.
                          ActionChanlSwitchFrame.ChanwidthSwitch.u1ElemId);
        /*Length */
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pMsgStruct->unMacMsg.
                          ActionChanlSwitchFrame.ChanwidthSwitch.u1ElemLen);
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pMsgStruct->unMacMsg.
                          ActionChanlSwitchFrame.ChanwidthSwitch.u1ElemLen);
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pMsgStruct->unMacMsg.
                          ActionChanlSwitchFrame.ChanwidthSwitch.u1ChanWidth);
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pMsgStruct->unMacMsg.
                          ActionChanlSwitchFrame.ChanwidthSwitch.u1CenterFcy0);
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pMsgStruct->unMacMsg.
                          ActionChanlSwitchFrame.ChanwidthSwitch.u1CenterFcy1);
    }
    if (pMsgStruct->unMacMsg.ActionChanlSwitchFrame.
        VhtTxPower.isOptional == OSIX_TRUE)
    {
        /*Element Id */
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pMsgStruct->unMacMsg.
                          ActionChanlSwitchFrame.VhtTxPower.u1ElemId);
        /*Length */
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pMsgStruct->unMacMsg.
                          ActionChanlSwitchFrame.VhtTxPower.u1ElemLen);
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pMsgStruct->unMacMsg.
                          ActionChanlSwitchFrame.VhtTxPower.u1TransPower);
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pMsgStruct->unMacMsg.
                          ActionChanlSwitchFrame.VhtTxPower.u1TxPower20);
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pMsgStruct->unMacMsg.
                          ActionChanlSwitchFrame.VhtTxPower.u1TxPower40);
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pMsgStruct->unMacMsg.
                          ActionChanlSwitchFrame.VhtTxPower.u1TxPower80);
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pMsgStruct->unMacMsg.
                          ActionChanlSwitchFrame.VhtTxPower.u1TxPower160);

    }
    u4Len = (UINT4) (pu1ReadPtr - pu1Frame);
    ChanlSwitMsgStruct.WssMacMsgStruct.unMacMsg.MacDot11PktBuf.pDot11MacPdu
        = WSSMAC_ALLOCATE_CRU_BUF (u4Len);
    if (ChanlSwitMsgStruct.WssMacMsgStruct.unMacMsg.MacDot11PktBuf.
        pDot11MacPdu == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC, "WssMacProcessChanSwitchAnnounce: "
                    "Memory Allocation Failure \r\n");
        UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1Frame);
        return WSSMAC_FAILURE;
    }
    WSSMAC_COPY_TO_BUF (ChanlSwitMsgStruct.WssMacMsgStruct.unMacMsg.
                        MacDot11PktBuf.pDot11MacPdu,
                        pu1Frame, 0, (UINT2) u4Len);
    MEMCPY (&(WssWlanDBMsg.WssWlanAttributeDB.BssId), pMsgStruct->
            unMacMsg.ActionChanlSwitchFrame.MacMgmtFrmHdr.u1BssId,
            sizeof (tMacAddr));
    WssWlanDBMsg.WssWlanIsPresentDB.bWlanIfIndex = WSSMAC_TRUE;
    WssWlanDBMsg.WssWlanIsPresentDB.bBssIfIndex = WSSMAC_TRUE;

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSSID_MAPPING_ENTRY,
                                  &WssWlanDBMsg) != WSSMAC_SUCCESS)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC, "WssMacProcessChanSwitchAnnounce:"
                    "WssIfProcessWssWlanDBMsg returned FAILURE \r\n");
        UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1Frame);
        return WSSMAC_FAILURE;
    }
    ChanlSwitMsgStruct.WssMacMsgStruct.unMacMsg.MacDot11PktBuf.u4IfIndex =
        WssWlanDBMsg.WssWlanAttributeDB.u4WlanIfIndex;
    ChanlSwitMsgStruct.WssMacMsgStruct.unMacMsg.MacDot11PktBuf.
        u1MacType = LOCAL_ROUTING_DISABLED;
    if (WssIfProcessApHdlrMsg (WSS_APHDLR_CFA_TX_BUF, &ChanlSwitMsgStruct) !=
        WSSMAC_SUCCESS)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC, "WssMacProcessChanSwitchAnnounce:"
                    " WssIfProcessApHdlrMsg returned FAILURE \r\n");
        UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1Frame);
        WSSMAC_RELEASE_CRU_BUF (ChanlSwitMsgStruct.WssMacMsgStruct.unMacMsg.
                                MacDot11PktBuf.pDot11MacPdu);
        return WSSMAC_FAILURE;
    }

    UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1Frame);
    return WSSMAC_SUCCESS;
}

/* ***************************************************************************
 *                                                                           *
 * Function     : WssMacProcessActionMsg                                     *
 *                                                                           *
 * Description  : Process the Action frame                                       *
 *                                                                           *
 * Input(s)     : MAC message type and union of struct with CRU_BUF          *
 *                                                                           *
 * Output(s)    : NONE                                                       *
 *                                                                           *
 * Returns      : SUCCESS/FAILURE                                            *
 *                                                                           *
 * ***************************************************************************/
UINT1
WssMacProcessActionMsg (UINT1 eMsgType, tWssMacMsgStruct * pMsgStruct)
{
    unApHdlrMsgStruct   CapwapMsgStruct;
    tCRU_BUF_CHAIN_DESC *pBuf = NULL;
    tMacAddr            StaAddr;
    UINT2               u2PacketLen = 0;
    UINT1              *pu1Frame = NULL;
    UINT1               u1CategoryCode = 0;
    UINT1              *pu1ReadPtr = NULL;
    UINT1               u1ActionField = 0;
    UINT1               u1IsChanged = 0;
    INT1                i1LinkMargin = 0;

    MEMSET (&CapwapMsgStruct, 0, sizeof (unApHdlrMsgStruct));
    MEMSET (StaAddr, 0, WSSMAC_MAC_ADDR_LEN);
    UNUSED_PARAM (eMsgType);

    pBuf = pMsgStruct->unMacMsg.MacDot11PktBuf.pDot11MacPdu;
    pu1Frame = UtlShMemAllocCapwapPktBuf ();

    if (pu1Frame == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC, "WssMacProcessActionMsg:"
                    "UtlShMemAllocCapwapPktBuf Allocation failed \r\n");
        return WSSMAC_FAILURE;
    }

    u2PacketLen = (UINT2) WSSMAC_GET_BUF_LEN (pBuf);
    pu1ReadPtr = WSSMAC_BUF_IF_LINEAR (pBuf, 0, u2PacketLen);

    if (pu1ReadPtr == NULL)
    {
        WSSMAC_COPY_FROM_BUF (pBuf, pu1Frame, 0, u2PacketLen);
        pu1ReadPtr = pu1Frame;
    }

    /*Obtain the Source Mac, Category code and Action field skipping all other
     * fields*/
    WSSMAC_SKIP_NBYTES (VALUE_10, pu1ReadPtr);
    WSSMAC_GET_NBYTES (StaAddr, pu1ReadPtr, WSSMAC_MAC_ADDR_LEN);
    WSSMAC_SKIP_NBYTES (VALUE_8, pu1ReadPtr);
    WSSMAC_GET_1BYTE (u1CategoryCode, pu1ReadPtr);
    WSSMAC_GET_1BYTE (u1ActionField, pu1ReadPtr);

    /*Parse the TPC Response and check if the link margin present in 
     * the frame is same as it is updated in the Station DB.
     * If the link margin is same need not forward it to WLC.*/
    if ((u1CategoryCode == DOT11_SPECTRUM_MGMT) &&
        (u1ActionField == DOT11_TPC_REPORT))
    {
        WSSMAC_SKIP_NBYTES (VALUE_4, pu1ReadPtr);
        WSSMAC_GET_1BYTE (i1LinkMargin, pu1ReadPtr);

        u1IsChanged = WssStaCheckIsLinkMarginUpdated (&StaAddr, i1LinkMargin);

        if (u1IsChanged == WSSMAC_SUCCESS)
        {
            UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1Frame);
            return WSSMAC_SUCCESS;
        }
    }
    if ((u1CategoryCode == DOT11_SPECTRUM_MGMT) &&
        (u1ActionField == DOT11_MEAS_REPORT))
    {
        printf ("debug:meas report rcvd\n");
        WSSMAC_SKIP_NBYTES (2, pu1ReadPtr);
    }

    CapwapMsgStruct.ApHdlrQueueReq.pRcvBuf =
        pMsgStruct->unMacMsg.MacDot11PktBuf.pDot11MacPdu;
    CapwapMsgStruct.ApHdlrQueueReq.u4MsgType = WSS_APHDLR_CAPWAP_MAC_MSG;

    if (WssIfProcessApHdlrMsg (WSS_APHDLR_CAPWAP_MAC_MSG, &CapwapMsgStruct) !=
        WSSMAC_SUCCESS)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC, "WssMacProcessActionMsg"
                    "WssIfProcessApHdlrMsg returned FAILURE\r\n");
        UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1Frame);
        return WSSMAC_FAILURE;
    }

    UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1Frame);
    return WSSMAC_SUCCESS;
}

/* ***************************************************************************
 *                                                                           *
 * Function     : WssMacProcessTpcRequest                                    *
 *                                                                           *
 * Description  : Construct the TPC Request and send it to CFA               *
 *                                                                           *
 * Input(s)     : MAC message type and union of struct with CRU_BUF          *
 *                                                                           *
 * Output(s)    : None                                                       *
 *                                                                           *
 * Returns      : SUCCESS/FAILURE                                            *
 *                                                                           *
 * ***************************************************************************/
UINT1
WssMacProcessTpcRequest (UINT1 eMsgType, tWssMacMsgStruct * pMsgStruct)
{
    UINT4               u4Len = 0;
    UINT1              *pu1Frame = NULL;
    UINT1              *pu1ReadPtr = NULL;
    UINT1               u1ElementId = 0;
    UINT1               u1Length = 0;
    tWssWlanDB          WssWlanDBMsg;
    unApHdlrMsgStruct   TpcReqMsgStruct;

    UNUSED_PARAM (eMsgType);
    MEMSET (&WssWlanDBMsg, 0, sizeof (tWssWlanDB));
    MEMSET (&TpcReqMsgStruct, 0, sizeof (unApHdlrMsgStruct));

    pu1Frame = UtlShMemAllocCapwapPktBuf ();

    if (pu1Frame == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC, "WssMacProcessTpcRequest"
                    "UtlShMemAllocCapwapPktBuf Allocation failed \r\n");
        return WSSMAC_FAILURE;
    }

    MEMSET (pu1Frame, 0, WSSMAC_MAX_PKT_LEN);
    pu1ReadPtr = pu1Frame;
    pMsgStruct->unMacMsg.ActionReqFrame.MacMgmtFrmHdr.u2MacFrameCtrl =
        WSSMAC_SET_ACTION_FRAME_CTRL;
    pMsgStruct->unMacMsg.ActionReqFrame.u1CategoryCode = DOT11_SPECTRUM_MGMT;
    pMsgStruct->unMacMsg.ActionReqFrame.u1ActionField = DOT11_TPC_REQUEST;
    u1ElementId = WSSMAC_TPC_REQUEST_ELEMENT_ID;

    /* Frame control field */
    WSSMAC_PUT_2BYTES (pu1ReadPtr, pMsgStruct->unMacMsg.ActionReqFrame.
                       MacMgmtFrmHdr.u2MacFrameCtrl);
    /* Duration ID field */
    WSSMAC_PUT_2BYTES (pu1ReadPtr, pMsgStruct->unMacMsg.ActionReqFrame.
                       MacMgmtFrmHdr.u2MacFrameDurId);
    /* Address1 field - Destination MAC */
    WSSMAC_PUT_NBYTES (pu1ReadPtr, pMsgStruct->unMacMsg.ActionReqFrame.
                       MacMgmtFrmHdr.u1DA, WSSMAC_MAC_ADDR_LEN);
    /* Address2 field - Source MAC */
    WSSMAC_PUT_NBYTES (pu1ReadPtr, pMsgStruct->unMacMsg.ActionReqFrame.
                       MacMgmtFrmHdr.u1SA, WSSMAC_MAC_ADDR_LEN);
    /* Addr3 field - BSSID */
    WSSMAC_PUT_NBYTES (pu1ReadPtr, pMsgStruct->unMacMsg.ActionReqFrame.
                       MacMgmtFrmHdr.u1BssId, WSSMAC_MAC_ADDR_LEN);
    /* Sequence control field */
    WSSMAC_PUT_2BYTES (pu1ReadPtr, pMsgStruct->unMacMsg.ActionReqFrame.
                       MacMgmtFrmHdr.u2MacFrameSeqCtrl);
    /* Category */
    WSSMAC_PUT_1BYTE (pu1ReadPtr, pMsgStruct->unMacMsg.ActionReqFrame.
                      u1CategoryCode);
    /* Action Field */
    WSSMAC_PUT_1BYTE (pu1ReadPtr, pMsgStruct->unMacMsg.ActionReqFrame.
                      u1ActionField);
    /*Dialog Token */
    WSSMAC_PUT_1BYTE (pu1ReadPtr, pMsgStruct->unMacMsg.ActionReqFrame.
                      TPCRequest.u1DialogToken);
    /*Element Id */
    WSSMAC_PUT_1BYTE (pu1ReadPtr, u1ElementId);
    /*Length */
    WSSMAC_PUT_1BYTE (pu1ReadPtr, u1Length);

    u4Len = (UINT4) (pu1ReadPtr - pu1Frame);
    TpcReqMsgStruct.WssMacMsgStruct.unMacMsg.MacDot11PktBuf.pDot11MacPdu
        = WSSMAC_ALLOCATE_CRU_BUF (u4Len);

    if (TpcReqMsgStruct.WssMacMsgStruct.unMacMsg.MacDot11PktBuf.
        pDot11MacPdu == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC, "WssMacProcessTpcRequest: "
                    "Memory Allocation Failure \r\n");
        UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1Frame);
        return WSSMAC_FAILURE;
    }
    WSSMAC_COPY_TO_BUF (TpcReqMsgStruct.WssMacMsgStruct.unMacMsg.
                        MacDot11PktBuf.pDot11MacPdu,
                        pu1Frame, 0, (UINT2) u4Len);

    MEMCPY (&(WssWlanDBMsg.WssWlanAttributeDB.BssId), pMsgStruct->
            unMacMsg.ActionReqFrame.MacMgmtFrmHdr.u1BssId, sizeof (tMacAddr));
    WssWlanDBMsg.WssWlanIsPresentDB.bWlanIfIndex = WSSMAC_TRUE;
    WssWlanDBMsg.WssWlanIsPresentDB.bBssIfIndex = WSSMAC_TRUE;

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSSID_MAPPING_ENTRY,
                                  &WssWlanDBMsg) != WSSMAC_SUCCESS)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC, "WssMacProcessTpcRequest:"
                    "WssIfProcessWssWlanDBMsg returned FAILURE \r\n");
        UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1Frame);
        WSSMAC_RELEASE_CRU_BUF (TpcReqMsgStruct.WssMacMsgStruct.unMacMsg.
                                MacDot11PktBuf.pDot11MacPdu);
        return WSSMAC_FAILURE;
    }

    TpcReqMsgStruct.WssMacMsgStruct.unMacMsg.MacDot11PktBuf.u4IfIndex =
        WssWlanDBMsg.WssWlanAttributeDB.u4WlanIfIndex;
    TpcReqMsgStruct.WssMacMsgStruct.unMacMsg.MacDot11PktBuf.
        u1MacType = LOCAL_ROUTING_DISABLED;

    if (WssIfProcessApHdlrMsg (WSS_APHDLR_CFA_TX_BUF, &TpcReqMsgStruct) !=
        WSSMAC_SUCCESS)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC, "WssMacProcessCapwapMsg:"
                    " WssIfProcessApHdlrMsg returned FAILURE \r\n");
        UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1Frame);
        WSSMAC_RELEASE_CRU_BUF (TpcReqMsgStruct.WssMacMsgStruct.unMacMsg.
                                MacDot11PktBuf.pDot11MacPdu);
        return WSSMAC_FAILURE;
    }

    UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1Frame);
    return WSSMAC_SUCCESS;
}

/* ***************************************************************************
 * Function     : WssMacProcessMeasRequest                                    *
 *                                                                           *
 * Description  : Process the CFA Msg from AP Hdlr                           *
 *
 * Input(s)     : MAC message type and union of struct with CRU_BUF          *
 *                                                                           *
 * Output(s)    : None                                                       *
 * Returns      : SUCCESS/FAILURE                                            *
 *                                                                           *
 ***************************************************************************/
UINT1
WssMacProcessMeasRequest (UINT1 eMsgType, tWssMacMsgStruct * pMsgStruct)
{
    UINT4               u4Len = 0;
    UINT1              *pu1Frame = NULL;
    UINT1              *pu1ReadPtr = NULL;
    UINT1               u1ElementId = 0;
    UINT1               u1Length = 0;
    tWssWlanDB          WssWlanDBMsg;
    unApHdlrMsgStruct   MeasReqMsgStruct;

    UNUSED_PARAM (eMsgType);
    MEMSET (&WssWlanDBMsg, 0, sizeof (tWssWlanDB));
    MEMSET (&MeasReqMsgStruct, 0, sizeof (unApHdlrMsgStruct));

    pu1Frame = UtlShMemAllocCapwapPktBuf ();
    if (pu1Frame == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC, "WssMacProcessMeasRequest"
                    "UtlShMemAllocCapwapPktBuf Allocation failed \r\n");
        return WSSMAC_FAILURE;
    }

    MEMSET (pu1Frame, 0, WSSMAC_MAX_PKT_LEN);
    pu1ReadPtr = pu1Frame;
    pMsgStruct->unMacMsg.ActionMeasReqFrame.MacMgmtFrmHdr.u2MacFrameCtrl =
        WSSMAC_SET_ACTION_FRAME_CTRL;
    pMsgStruct->unMacMsg.ActionMeasReqFrame.u1CategoryCode =
        DOT11_SPECTRUM_MGMT;
    pMsgStruct->unMacMsg.ActionMeasReqFrame.u1ActionField = DOT11_MEAS_REQUEST;
    u1ElementId = WSSMAC_MEAS_REQUEST_ELEMENT_ID;
    u1Length = 14;
    /* Frame control field */
    WSSMAC_PUT_2BYTES (pu1ReadPtr, pMsgStruct->unMacMsg.ActionMeasReqFrame.
                       MacMgmtFrmHdr.u2MacFrameCtrl);
    /* Duration ID field */
    WSSMAC_PUT_2BYTES (pu1ReadPtr, pMsgStruct->unMacMsg.ActionMeasReqFrame.
                       MacMgmtFrmHdr.u2MacFrameDurId);
    /* Address1 field - Destination MAC */
    WSSMAC_PUT_NBYTES (pu1ReadPtr, pMsgStruct->unMacMsg.ActionMeasReqFrame.
                       MacMgmtFrmHdr.u1DA, WSSMAC_MAC_ADDR_LEN);
    /* Address2 field - Source MAC */
    WSSMAC_PUT_NBYTES (pu1ReadPtr, pMsgStruct->unMacMsg.ActionMeasReqFrame.
                       MacMgmtFrmHdr.u1SA, WSSMAC_MAC_ADDR_LEN);
    /* Addr3 field - BSSID */
    WSSMAC_PUT_NBYTES (pu1ReadPtr, pMsgStruct->unMacMsg.ActionMeasReqFrame.
                       MacMgmtFrmHdr.u1BssId, WSSMAC_MAC_ADDR_LEN);
    /* Sequence control field */
    WSSMAC_PUT_2BYTES (pu1ReadPtr, pMsgStruct->unMacMsg.ActionMeasReqFrame.
                       MacMgmtFrmHdr.u2MacFrameSeqCtrl);
    /* Category */
    WSSMAC_PUT_1BYTE (pu1ReadPtr, pMsgStruct->unMacMsg.ActionMeasReqFrame.
                      u1CategoryCode);
    /* Action Field */
    WSSMAC_PUT_1BYTE (pu1ReadPtr, pMsgStruct->unMacMsg.ActionMeasReqFrame.
                      u1ActionField);
    /*Dialog Token */
    WSSMAC_PUT_1BYTE (pu1ReadPtr, pMsgStruct->unMacMsg.ActionMeasReqFrame.
                      MeasRequest.u1DialogToken);
    /*Element Id */
    WSSMAC_PUT_1BYTE (pu1ReadPtr, u1ElementId);
    /*Length */
    WSSMAC_PUT_1BYTE (pu1ReadPtr, u1Length);
    /*Meas Token */
    WSSMAC_PUT_1BYTE (pu1ReadPtr, pMsgStruct->unMacMsg.ActionMeasReqFrame.
                      MeasRequest.u1MacFrameMESReq_MesToken);
    /*Meas ReqMod */
    WSSMAC_PUT_1BYTE (pu1ReadPtr, pMsgStruct->unMacMsg.ActionMeasReqFrame.
                      MeasRequest.u1MacFrameMESReq_MesReqMode);
    /*Meas Type */
    WSSMAC_PUT_1BYTE (pu1ReadPtr, pMsgStruct->unMacMsg.ActionMeasReqFrame.
                      MeasRequest.u1MacFrameMESReq_MesType);

    WSSMAC_PUT_1BYTE (pu1ReadPtr, pMsgStruct->unMacMsg.ActionMeasReqFrame.
                      MeasRequest.WssMESReqMesRequest.u1MESReqType_ChannelNum);
    WSSMAC_PUT_4BYTES (pu1ReadPtr, pMsgStruct->unMacMsg.ActionMeasReqFrame.
                       MeasRequest.WssMESReqMesRequest.u8MESReqType_StartTime.
                       u4HiWord);
    WSSMAC_PUT_4BYTES (pu1ReadPtr,
                       pMsgStruct->unMacMsg.ActionMeasReqFrame.MeasRequest.
                       WssMESReqMesRequest.u8MESReqType_StartTime.u4LoWord);
    WSSMAC_PUT_2BYTES (pu1ReadPtr,
                       pMsgStruct->unMacMsg.ActionMeasReqFrame.MeasRequest.
                       WssMESReqMesRequest.u2MESReqType_Duration);
    u4Len = (UINT4) (pu1ReadPtr - pu1Frame);
    MeasReqMsgStruct.WssMacMsgStruct.unMacMsg.MacDot11PktBuf.pDot11MacPdu
        = WSSMAC_ALLOCATE_CRU_BUF (u4Len);

    if (MeasReqMsgStruct.WssMacMsgStruct.unMacMsg.MacDot11PktBuf.
        pDot11MacPdu == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC, "WssMacProcessMeasRequest: "
                    "Memory Allocation Failure \r\n");
        UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1Frame);
        return WSSMAC_FAILURE;
    }
    WSSMAC_COPY_TO_BUF (MeasReqMsgStruct.WssMacMsgStruct.unMacMsg.
                        MacDot11PktBuf.pDot11MacPdu,
                        pu1Frame, 0, (UINT2) u4Len);
    MEMCPY (&(WssWlanDBMsg.WssWlanAttributeDB.BssId), pMsgStruct->
            unMacMsg.ActionReqFrame.MacMgmtFrmHdr.u1BssId, sizeof (tMacAddr));
    WssWlanDBMsg.WssWlanIsPresentDB.bWlanIfIndex = WSSMAC_TRUE;
    WssWlanDBMsg.WssWlanIsPresentDB.bBssIfIndex = WSSMAC_TRUE;

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSSID_MAPPING_ENTRY,
                                  &WssWlanDBMsg) != WSSMAC_SUCCESS)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC, "WssMacProcessMeasRequest:"
                    "WssIfProcessWssWlanDBMsg returned FAILURE \r\n");
        UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1Frame);
        return WSSMAC_FAILURE;
    }
    MeasReqMsgStruct.WssMacMsgStruct.unMacMsg.MacDot11PktBuf.u4IfIndex =
        WssWlanDBMsg.WssWlanAttributeDB.u4WlanIfIndex;
    MeasReqMsgStruct.WssMacMsgStruct.unMacMsg.MacDot11PktBuf.
        u1MacType = LOCAL_ROUTING_DISABLED;
    if (WssIfProcessApHdlrMsg (WSS_APHDLR_CFA_TX_BUF, &MeasReqMsgStruct) !=
        WSSMAC_SUCCESS)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC, "WssMacProcessMeasRequest:"
                    " WssIfProcessApHdlrMsg returned FAILURE \r\n");
        UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1Frame);
        WSSMAC_RELEASE_CRU_BUF (MeasReqMsgStruct.WssMacMsgStruct.unMacMsg.
                                MacDot11PktBuf.pDot11MacPdu);
        return WSSMAC_FAILURE;
    }

    UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1Frame);
    return WSSMAC_SUCCESS;
}

#ifdef PMF_WANTED
/* ***************************************************************************
 *                                                                           *
 * Function     : WssMacProcessSAQueryReq                                    *
 *                                                                           *
 * Description  : Process SA Query Req - not used for AP                     *
 *                                                                           *
 * Input(s)     : MAC message type and union of struct with CRU_BUF          *
 *                                                                           *
 * Output(s)    : NONE                                                       *
 *                                                                           *
 * Returns      : SUCCESS                                                    *
 *                                                                           *
 * ***************************************************************************/
UINT1
WssMacProcessSAQueryReq (UINT1 eMsgType, tWssMacMsgStruct * pSAQueryReq)
{
    UNUSED_PARAM (eMsgType);
    UNUSED_PARAM (pSAQueryReq);

    return WSSMAC_SUCCESS;
}

/* ***************************************************************************
 *                                                                           *
 * Function     : WssMacProcessSAQueryResp                                   *
 *                                                                           *
 * Description  : Process SA Query Response - not used for AP                *
 *                                                                           *
 * Input(s)     : MAC message type and union of struct with CRU_BUF          *
 *                                                                           *
 * Output(s)    : NONE                                                       *
 *                                                                           *
 * Returns      : SUCCESS                                                    *
 *                                                                           *
 * ***************************************************************************/
UINT1
WssMacProcessSAQueryResp (UINT1 eMsgType, tWssMacMsgStruct * pSAQueryRspStruct)
{
    UNUSED_PARAM (eMsgType);
    UNUSED_PARAM (pSAQueryRspStruct);

    return WSSMAC_SUCCESS;
}
#endif
#endif
