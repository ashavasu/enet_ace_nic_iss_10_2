/*************************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 * 
 *  $Id: wssmacwlcparse.c,v 1.8 2018/02/09 09:41:52 siva Exp $
 *
 * DESCRIPTION : Contains WSSMAC Module functions for WLC
 * ***********************************************************************/
#ifndef __WSSMAC_WLCPARSE_C__
#define __WSSMAC_WLCPARSE_C__

#include "wssmacinc.h"
#include "wlchdlrsz.h"
#include "wlchdlrmacr.h"
#ifdef BAND_SELECT_WANTED
#include "wssstawlcinc.h"
#include "wsscfgcli.h"
#include "wlchdlrproto.h"
#endif
#ifdef WPS_WANTED
#define wps_oui 0x04
#include "wps_defs.h"
#endif
#include "rfmproto.h"
#include "wlchdlr.h"
#include "capwapglob.h"
#include "wssstawlcprot.h"
/* Test Wep */
extern tRBTree      gWssStaWepProcessDB;
UINT1               gau1TempBuf[10] = "\0";
#ifdef BAND_SELECT_WANTED
extern tRBTree      gWssStaBandSteerDB;
#endif
extern UINT4        gu4WlcHdlrSysLogId;

/* ***************************************************************************
 *                                                                           *
 * Function     : WssMacProcessCapwapMsg                                     *
 *                                                                           *
 * Description  : Process the Capwap message                                 *
 *                                                                           *
 * Input(s)     : MAC message type and union of struct with CRU_BUF          *
 *                                                                           *
 * Output(s)    : NONE                                                       *
 *                                                                           *
 * Returns      : SUCCESS/FAILURE                                            *
 *                                                                           *
 * ***************************************************************************/
UINT1
WssMacProcessCapwapMsg (UINT1 eMsgType, tWssMacMsgStruct * pMsgStruct)
{
    UINT2               u2PacketLen = 0;
    UINT1              *pu1ReadPtr = NULL;
    UINT1              *pu1Buf = NULL;
    tCRU_BUF_CHAIN_DESC *pBuf = NULL;

    if (pMsgStruct == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC, "WssMacProcessCapwapMsg: "
                    "NULL input received \r\n");
        return WSSMAC_FAILURE;
    }
    WLCHDLR_PKTBUF_ALLOC_MEM_BLOCK (pu1Buf);
    if (pu1Buf == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                    "WssMacProcessCapwapMsg:-"
                    "UtlShMemAllocMacMsgStructBuf returned failure\n");
        return WSSMAC_FAILURE;
    }

    pBuf = pMsgStruct->unMacMsg.MacDot11PktBuf.pDot11MacPdu;

    u2PacketLen = (UINT2) WSSMAC_GET_BUF_LEN (pBuf);
    pu1ReadPtr = WSSMAC_BUF_IF_LINEAR (pBuf, 0, u2PacketLen);
    if (pu1ReadPtr == NULL)
    {
        MEMSET (pu1Buf, 0x00, u2PacketLen);
        WSSMAC_COPY_FROM_BUF (pBuf, pu1Buf, 0, u2PacketLen);
        pu1ReadPtr = pu1Buf;
    }

    if (pMsgStruct->unMacMsg.MacDot11PktBuf.u1MacType == 0)
        /* WSS_CAPWAP_MAC_TYPE_LOCAL */
    {
        eMsgType = WSS_DOT11_DATA_MSG;
    }
    else
    {
        /* Parse the Received MAC frame to get the Message type */
        eMsgType = WssMacParseMACFrame (pu1ReadPtr);
    }
    if (eMsgType != WSSMAC_INVALID_MSG_TYPE)
    {
        if (gaWssMacFrameHandling[eMsgType] != NULL)
        {
            if (gaWssMacFrameHandling[eMsgType] (eMsgType, pMsgStruct) !=
                WSSMAC_SUCCESS)
            {
                WSSMAC_TRC1 (WSSMAC_FAILURE_TRC, "WssMacProcessCapwapMsg: "
                             "Error in process of WSS message %d\r\n",
                             eMsgType);
                if (eMsgType != WSS_DOT11_DATA_MSG)
                {
                    WSSMAC_RELEASE_CRU_BUF (pBuf);
                }
                WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pu1Buf);
                return WSSMAC_FAILURE;
            }
        }
        else
        {
            WSSMAC_TRC1 (WSSMAC_FAILURE_TRC, "WssMacProcessCapwapMsg:"
                         "gaWssMacFrameHandling is NULL for MsgType %d\r\n",
                         eMsgType);
            if (eMsgType != WSS_DOT11_DATA_MSG)
            {
                WSSMAC_RELEASE_CRU_BUF (pBuf);
            }
            WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pu1Buf);
            return WSSMAC_FAILURE;
        }
    }
    else
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                    "WssMacProcessCapwapMsg: INVALID Msg Type \r\n");
        if (eMsgType != WSS_DOT11_DATA_MSG)
        {
            WSSMAC_RELEASE_CRU_BUF (pBuf);
        }
        WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pu1Buf);
        return WSSMAC_FAILURE;
    }

    if (eMsgType != WSS_DOT11_DATA_MSG)
    {
        WSSMAC_RELEASE_CRU_BUF (pBuf);
    }
    WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pu1Buf);
    return WSSMAC_SUCCESS;
}

/* ***************************************************************************
 *                                                                           *
 * Function     : WssMacProcessProbeReq                                      *
 *                                                                           *
 * Description  : Process the probe request frame                            *
 *                                                                           *
 * Input(s)     : MAC message type and union of struct with CRU_BUF          *
 *                                                                           *
 * Output(s)    : Probe request structure                                    *
 *                                                                           *
 * Returns      : SUCCESS/FAILURE                                            *
 *                                                                           *
 * ***************************************************************************/
UINT1
WssMacProcessProbeReq (UINT1 eMsgType, tWssMacMsgStruct * pMsgStruct)
{
    tWssMacMsgStruct   *pProbeReqStruct = NULL;
    INT2                i2PacketLen = 0;
    UINT2               u2Count = 0, u2Count1 = 0;
    UINT1              *pu1ReadPtr = NULL;
    UINT1               au1Frame[WSSMAC_MAX_PKT_LEN] = "\0";
    UINT1               elemID;
    UINT1               elemLen;
    tCRU_BUF_CHAIN_DESC *pBuf = NULL;
    UINT2               u2Len = 0;
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;
#ifdef BAND_SELECT_WANTED
    UINT1               u1RadioId = 0;
    UINT1               u1WlanId = 0;
    tRadioIfGetDB       RadioIfGetDB;
    tWssWlanDB         *pwssWlanDB = NULL;
    UINT1               au1ProbeSSID[WSSWLAN_SSID_NAME_LEN];
    UINT1               u1BandSelectGlobStatus = 0;
    tWssStaBandSteerDB *pBandSteerDB = NULL;
    tMacAddr            WlanBssid;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tWssStaBandSteerDB  WssStaBandSteerDB;
#endif
#ifdef WPS_WANTED
    tWssWpsNotifyParams WssWpsNotifyParams;
    BOOL1               bWpsPresent = OSIX_FALSE;
    UINT2               tag = 0;
    UINT1               extTag = 0;
    UINT1               venID[3] = { 0x00, 0x37, 0x2a };
    UINT1               extLen = 0;
    UINT2               Len = 0;
    UINT1               vendorID[3];
    MEMSET (&WssWpsNotifyParams, 0, sizeof (tWssWpsNotifyParams));
#endif

#ifdef BAND_SELECT_WANTED
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    MEMSET (au1ProbeSSID, 0, WSSWLAN_SSID_NAME_LEN);
    MEMSET (WlanBssid, 0, sizeof (tMacAddr));
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE);
    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&WssStaBandSteerDB, 0, sizeof (tWssStaBandSteerDB));
#endif

    pWlcHdlrMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pWlcHdlrMsgStruct == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                    "WssMacProcessProbeReq:- "
                    "UtlShMemAllocWlcBuf returned failure\n");
#ifdef BAND_SELECT_WANTED
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
#endif
        return WSSMAC_FAILURE;
    }
    pProbeReqStruct =
        (tWssMacMsgStruct *) (VOID *) UtlShMemAllocMacMsgStructBuf ();
    if (pProbeReqStruct == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                    "WssMacProcessProbeReq:-"
                    "UtlShMemAllocMacMsgStructBuf returned failure\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
#ifdef BAND_SELECT_WANTED
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
#endif
        return OSIX_FAILURE;
    }

#ifdef BAND_SELECT_WANTED
    pwssWlanDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
    if (pwssWlanDB == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                    "WssMacProcessSpectrumMgmtframe"
                    "UtlShMemAllocWlanDbBuf returned failure\n");
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pProbeReqStruct);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return WSSMAC_FAILURE;
    }
#endif

    MEMSET (pProbeReqStruct, 0, sizeof (tWssMacMsgStruct));
    MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));
    UNUSED_PARAM (eMsgType);

    pBuf = pMsgStruct->unMacMsg.MacDot11PktBuf.pDot11MacPdu;

    i2PacketLen = (UINT2) WSSMAC_GET_BUF_LEN (pBuf);
    pu1ReadPtr = WSSMAC_BUF_IF_LINEAR (pBuf, 0, i2PacketLen);
    if (pu1ReadPtr == NULL)
    {
        MEMSET (au1Frame, 0x00, i2PacketLen);
        WSSMAC_COPY_FROM_BUF (pBuf, au1Frame, 0, i2PacketLen);
        pu1ReadPtr = au1Frame;
    }
    /* Discard Probe Request if PDU size exceeds maximum allowed length */
    if (i2PacketLen > WSSMAC_MAX_PDU_LEN)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                    "WssMacProcessProbeReq:-"
                    "PDU size exceeds maximum allowed length, Discarding Probe Request\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pProbeReqStruct);
        return WSSMAC_FAILURE;
    }

    /* Get the 802.11 MAC Header */
    WSSMAC_GET_2BYTES (pProbeReqStruct->unMacMsg.ProbReqMacFrame.MacMgmtFrmHdr.u2MacFrameCtrl, pu1ReadPtr);    /* Get Frame control field */
    WSSMAC_GET_2BYTES (pProbeReqStruct->unMacMsg.ProbReqMacFrame.MacMgmtFrmHdr.u2MacFrameDurId, pu1ReadPtr);    /* Get Duration ID field */
    WSSMAC_GET_NBYTES (pProbeReqStruct->unMacMsg.ProbReqMacFrame.MacMgmtFrmHdr.u1DA, pu1ReadPtr, WSSMAC_MAC_ADDR_LEN);    /* Get Address1 field */
    WSSMAC_GET_NBYTES (pProbeReqStruct->unMacMsg.ProbReqMacFrame.MacMgmtFrmHdr.u1SA, pu1ReadPtr, WSSMAC_MAC_ADDR_LEN);    /* Get Address2 field */
    WSSMAC_GET_NBYTES (pProbeReqStruct->unMacMsg.ProbReqMacFrame.MacMgmtFrmHdr.u1BssId, pu1ReadPtr, WSSMAC_MAC_ADDR_LEN);    /* Get Address3 field */
    WSSMAC_GET_2BYTES (pProbeReqStruct->unMacMsg.ProbReqMacFrame.MacMgmtFrmHdr.u2MacFrameSeqCtrl, pu1ReadPtr);    /* Get Address3 field */

#ifdef WPS_WANTED
    MEMCPY (WssWpsNotifyParams.WPSPROBEREQIND.au1StaMac,
            pProbeReqStruct->unMacMsg.ProbReqMacFrame.MacMgmtFrmHdr.u1SA,
            WSSMAC_MAC_ADDR_LEN);
#endif
    i2PacketLen = (UINT2) (i2PacketLen - DOT11_REG_HDR_SIZE);

    /*extract the SSID whose TAG is 0x00 */
    WSSMAC_GET_1BYTE (elemID, pu1ReadPtr);
    WSSMAC_GET_1BYTE (elemLen, pu1ReadPtr);
    i2PacketLen -= 2;
    if (elemID == WSSMAC_SSID_ELEMENT_ID)
    {
        pProbeReqStruct->unMacMsg.ProbReqMacFrame.Ssid.u1MacFrameElemId =
            elemID;
        pProbeReqStruct->unMacMsg.ProbReqMacFrame.Ssid.u1MacFrameElemLen =
            elemLen;
        u2Len =
            pProbeReqStruct->unMacMsg.ProbReqMacFrame.Ssid.u1MacFrameElemLen;
        WSSMAC_GET_NBYTES (pProbeReqStruct->unMacMsg.ProbReqMacFrame.Ssid.
                           au1MacFrameSSID, pu1ReadPtr, u2Len);
        i2PacketLen -= u2Len;
    }

    while ((i2PacketLen > 0) && (*pu1ReadPtr != 0))
    {
        WSSMAC_GET_1BYTE (elemID, pu1ReadPtr);
        WSSMAC_GET_1BYTE (elemLen, pu1ReadPtr);
        switch (elemID)
        {
            case WSSMAC_SSID_ELEMENT_ID:
                pProbeReqStruct->unMacMsg.ProbReqMacFrame.Ssid.
                    u1MacFrameElemId = elemID;
                pProbeReqStruct->unMacMsg.ProbReqMacFrame.Ssid.
                    u1MacFrameElemLen = elemLen;
                u2Len =
                    pProbeReqStruct->unMacMsg.ProbReqMacFrame.Ssid.
                    u1MacFrameElemLen;
                WSSMAC_GET_NBYTES (pProbeReqStruct->unMacMsg.ProbReqMacFrame.
                                   Ssid.au1MacFrameSSID, pu1ReadPtr, u2Len);
                break;

            case WSSMAC_SUPP_RATE_ELEMENT_ID:
                pProbeReqStruct->unMacMsg.ProbReqMacFrame.SuppRate.
                    u1MacFrameElemId = elemID;
                pProbeReqStruct->unMacMsg.ProbReqMacFrame.SuppRate.
                    u1MacFrameElemLen = elemLen;
                u2Len =
                    pProbeReqStruct->unMacMsg.ProbReqMacFrame.SuppRate.
                    u1MacFrameElemLen;
                u2Count = 0;
                while (u2Count < u2Len)
                {
                    WSSMAC_GET_1BYTE (pProbeReqStruct->unMacMsg.ProbReqMacFrame.
                                      SuppRate.au1MacFrameSuppRate[u2Count],
                                      pu1ReadPtr);
                    u2Count++;
                }
                break;
            case WSSMAC_REQINFO_ELEMENTID:
                pProbeReqStruct->unMacMsg.ProbReqMacFrame.ReqInfo.
                    u1MacFrameElemId = elemID;
                pProbeReqStruct->unMacMsg.ProbReqMacFrame.ReqInfo.
                    u1MacFrameElemLen = elemLen;
                u2Count = 0;
                u2Len = pProbeReqStruct->unMacMsg.ProbReqMacFrame.ReqInfo.
                    u1MacFrameElemLen;
                while (u2Count < u2Len)
                {
                    WSSMAC_GET_1BYTE (pProbeReqStruct->unMacMsg.ProbReqMacFrame.
                                      ReqInfo.au1MacFrameReqID[u2Count],
                                      pu1ReadPtr);
                    u2Count++;
                }
                break;
            case WSSMAC_EXT_SUPP_RATE_ELEMENTID:
                pProbeReqStruct->unMacMsg.ProbReqMacFrame.ExtSuppRates.
                    u1MacFrameElemId = elemID;
                pProbeReqStruct->unMacMsg.ProbReqMacFrame.ExtSuppRates.
                    u1MacFrameElemLen = elemLen;
                u2Count = 0;
                u2Len = pProbeReqStruct->unMacMsg.ProbReqMacFrame.ExtSuppRates.
                    u1MacFrameElemLen;
                while (u2Count < u2Len)
                {
                    WSSMAC_GET_1BYTE (pProbeReqStruct->unMacMsg.ProbReqMacFrame.
                                      ExtSuppRates.
                                      au1MacFrameExtSuppRates[u2Count],
                                      pu1ReadPtr);
                    u2Count++;
                }
                break;
            case WSSMAC_HT_CAPAB_ELEMENT_ID:
                pProbeReqStruct->unMacMsg.ProbReqMacFrame.ExtSuppRates.
                    u1MacFrameElemId = elemID;
                pProbeReqStruct->unMacMsg.ProbReqMacFrame.ExtSuppRates.
                    u1MacFrameElemLen = elemLen;
                u2Count = 0;
                u2Len = pProbeReqStruct->unMacMsg.ProbReqMacFrame.ExtSuppRates.
                    u1MacFrameElemLen;
                while (u2Count < u2Len)
                {
                    WSSMAC_GET_1BYTE (pProbeReqStruct->unMacMsg.ProbReqMacFrame.
                                      ExtSuppRates.
                                      au1MacFrameExtSuppRates[u2Count],
                                      pu1ReadPtr);
                    u2Count++;
                }
                break;
            case WSSMAC_BSS_COEXIST_ELEMENT_ID:
                pProbeReqStruct->unMacMsg.ProbReqMacFrame.BssCoexistence.
                    u1MacFrameElemId = elemID;
                pProbeReqStruct->unMacMsg.ProbReqMacFrame.BssCoexistence.
                    u1MacFrameElemLen = elemLen;
                WSSMAC_GET_1BYTE (pProbeReqStruct->unMacMsg.ProbReqMacFrame.
                                  BssCoexistence.u1BSSCoexistInfoFields,
                                  pu1ReadPtr);
                break;
            case WSSMAC_EXT_CAPAB_ELEMENT_ID:
                pProbeReqStruct->unMacMsg.ProbReqMacFrame.ExtCapabilities.
                    u1MacFrameElemId = elemID;
                pProbeReqStruct->unMacMsg.ProbReqMacFrame.ExtCapabilities.
                    u1MacFrameElemLen = elemLen;
                WSSMAC_GET_NBYTES (pProbeReqStruct->unMacMsg.ProbReqMacFrame.
                                   ExtCapabilities.au1Capabilities, pu1ReadPtr,
                                   WSSMAC_EXT_CAP);
                break;
            case WSSMAC_VENDOR_INFO_ELEMENTID:
                pProbeReqStruct->unMacMsg.ProbReqMacFrame.
                    aVendSpecInfo[u2Count1].u1MacFrameElemId = elemID;
                pProbeReqStruct->unMacMsg.ProbReqMacFrame.
                    aVendSpecInfo[u2Count1].u1MacFrameElemLen = elemLen;
                WSSMAC_GET_NBYTES (pProbeReqStruct->unMacMsg.ProbReqMacFrame.
                                   aVendSpecInfo[u2Count1].au1MacFrameVend_OUI,
                                   pu1ReadPtr, WSSMAC_MAX_OUI_SIZE);
                u2Count = 0;
                u2Len = (UINT2) ((pProbeReqStruct->unMacMsg.ProbReqMacFrame.
                                  aVendSpecInfo[u2Count1].u1MacFrameElemLen) -
                                 WSSMAC_MAX_OUI_SIZE);
                WSSMAC_GET_1BYTE (pProbeReqStruct->unMacMsg.ProbReqMacFrame.
                                  aVendSpecInfo[u2Count1].
                                  au1MacFrameVend_Content[u2Count], pu1ReadPtr);
#ifdef WPS_WANTED
                bWpsPresent = OSIX_FALSE;
                if ((pProbeReqStruct->unMacMsg.ProbReqMacFrame.
                     aVendSpecInfo[u2Count1].
                     au1MacFrameVend_Content[u2Count]) == wps_oui)
                {
                    bWpsPresent = OSIX_TRUE;
                    WssWpsNotifyParams.WPSPROBEREQIND.bWpsPresent = OSIX_TRUE;
                }
#endif
                u2Count++;
                while (u2Count < u2Len)
                {
#ifdef WPS_WANTED
                    if (bWpsPresent == OSIX_TRUE)
                    {
                        WSSMAC_GET_2BYTES (tag, pu1ReadPtr);
                        WSSMAC_GET_2BYTES (Len, pu1ReadPtr);
                        u2Count += 4;
                        switch (tag)
                        {
                            case WPS_VENDOR_EXTENSION_TAG:
                                WSSMAC_GET_NBYTES (vendorID, pu1ReadPtr, 3);
                                Len -= 3;
                                u2Count += 3;
                                if (MEMCMP (vendorID, venID, 3) == 0)
                                {
                                    while (Len > 0)
                                    {
                                        WSSMAC_GET_1BYTE (extTag, pu1ReadPtr);
                                        WSSMAC_GET_1BYTE (extLen, pu1ReadPtr);
                                        Len -= 2;
                                        u2Count += 2;
                                        if (extTag ==
                                            WFA_ELEM_REQUEST_TO_ENROLL)
                                        {
                                            WSSMAC_GET_1BYTE
                                                (WssWpsNotifyParams.
                                                 WPSPROBEREQIND.
                                                 request_to_enroll, pu1ReadPtr);
                                            Len--;
                                            u2Count += 1;
                                        }
                                        else
                                        {
                                            WSSMAC_GET_NBYTES
                                                (WssWpsNotifyParams.
                                                 WPSPROBEREQIND.au1Pad,
                                                 pu1ReadPtr, extLen);
                                            Len -= extLen;
                                            u2Count += extLen;
                                        }
                                    }
                                }
                                break;
                            case WPS_UUID_E_TAG:
                                WSSMAC_GET_NBYTES (WssWpsNotifyParams.
                                                   WPSPROBEREQIND.uuid_e,
                                                   pu1ReadPtr, Len);
                                u2Count += Len;
                                break;
                            case WPS_DEVICE_PASSWORD_ID_TAG:
                                WSSMAC_GET_2BYTES (WssWpsNotifyParams.
                                                   WPSPROBEREQIND.
                                                   dev_password_id, pu1ReadPtr);
                                u2Count += Len;
                                break;
                            default:
                                WSSMAC_GET_NBYTES (WssWpsNotifyParams.
                                                   WPSPROBEREQIND.au1Pad,
                                                   pu1ReadPtr, Len);
                                u2Count += Len;
                                break;

                        }
                    }
                    else
#endif
                    {
                        WSSMAC_GET_1BYTE (pProbeReqStruct->unMacMsg.
                                          ProbReqMacFrame.
                                          aVendSpecInfo[u2Count1].
                                          au1MacFrameVend_Content[u2Count],
                                          pu1ReadPtr);
                        u2Count++;
                    }
                }
#ifdef WPS_WANTED
                if (bWpsPresent == OSIX_TRUE)
                {
                    break;
                }
#endif
                u2Count1++;
                break;
            default:
                u2Len = elemLen;
                WSSMAC_GET_NBYTES (pProbeReqStruct->unMacMsg.ProbReqMacFrame.
                                   au1Pad, pu1ReadPtr, u2Len);
                break;
        }
        i2PacketLen = (UINT2) (i2PacketLen - elemLen - 2);
    }

#ifdef WPS_WANTED
    if (WssWpsNotifyParams.WPSPROBEREQIND.bWpsPresent == OSIX_TRUE)
    {
        WssWpsNotifyParams.eWssWpsNotifyType = WSSWPS_PROBREQ_IND;
        /* Send Notification msg to Wps */
        if (WpsProcessWssNotification (&WssWpsNotifyParams) == OSIX_FAILURE)
        {
            WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                        "WssStaProcessProbeReqFrames:- WpsProcessWssNotification"
                        "returned Failure !!!! \n");
            UtlShMemFreeMacMsgStructBuf ((UINT1 *) pProbeReqStruct);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
#ifdef BAND_SELECT_WANTED
            UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
#endif
            return (OSIX_FAILURE);
        }
    }
#else
#ifdef BAND_SELECT_WANTED
    if (WssIfGetBandSelectStatus (&u1BandSelectGlobStatus) != OSIX_SUCCESS)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC, "ValidateAssocFrame: "
                    "Failed to get status of bandsteer \r\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pProbeReqStruct);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_SUCCESS;
    }
    if (u1BandSelectGlobStatus == WLAN_DISABLE)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC, "ValidateAssocFrame: "
                    "Failed to get status of bandsteer \r\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pProbeReqStruct);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_SUCCESS;
    }

    /* For WTP only one session Entry exists at at time */
    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
        pMsgStruct->unMacMsg.MacDot11PktBuf.u2SessId;

    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpDescriptor = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bNoOfRadio = OSIX_TRUE;
/*Getting the MAXIMUM numner of radios in CAPWAPDB */
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                 pWssIfCapwapDB) == OSIX_FAILURE)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC, "Failed to Retrive CAPWAP DB \r\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pProbeReqStruct);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    if (pWssIfCapwapDB->CapwapGetDB.u1WtpNoofRadio <= 1)
    {
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pProbeReqStruct);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_SUCCESS;
    }
/*For each radio*/
    for (u1RadioId = 1; u1RadioId <= pWssIfCapwapDB->CapwapGetDB.u1WtpNoofRadio;
         u1RadioId++)
    {
        pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;
        pWssIfCapwapDB->CapwapGetDB.u1RadioId = u1RadioId;
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                     pWssIfCapwapDB) == OSIX_FAILURE)
        {

            WSSMAC_TRC (WSSMAC_FAILURE_TRC, "Failed to Retrive CAPWAP DB \r\n");
            continue;
        }
        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex
            = pWssIfCapwapDB->CapwapGetDB.u4IfIndex;
        RadioIfGetDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bBssIdCount = OSIX_TRUE;
        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                      &RadioIfGetDB) == OSIX_SUCCESS)
        {
            if ((RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                 RADIO_TYPE_B) || (RadioIfGetDB.RadioIfGetAllDB.
                                   u4Dot11RadioType == RADIO_TYPE_BGN)
                || (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                    RADIO_TYPE_BG))
            {
                continue;
            }
            else
            {
                /*GETTING bssIFIndex for each WLAN id from radio db */
                for (u1WlanId = WSSWLAN_START_WLANID_PER_RADIO;
                     u1WlanId <= WSSWLAN_END_WLANID_PER_RADIO; u1WlanId++)
                {
                    MEMSET (pwssWlanDB, 0, sizeof (tWssWlanDB));
                    RadioIfGetDB.RadioIfGetAllDB.u1WlanId = u1WlanId;
                    if (WssIfProcessRadioIfDBMsg (WSS_GET_WLAN_BSS_IFINDEX_DB,
                                                  &RadioIfGetDB) !=
                        OSIX_SUCCESS)
                    {
                        /*          WSSMAC_TRC(WSSMAC_FAILURE_TRC, "WssMacProcessProbeReq:\
                           RadioDBMsg failed to return BssIfIndex\r\n"); */
                        continue;
                    }
                    pwssWlanDB->WssWlanAttributeDB.u4BssIfIndex =
                        RadioIfGetDB.RadioIfGetAllDB.u4BssIfIndex;
                    /*GETTING bssid and WlanIfIndex for each WLAN id from WLAN db */
                    pwssWlanDB->WssWlanIsPresentDB.bBssId = OSIX_TRUE;
                    pwssWlanDB->WssWlanIsPresentDB.bWlanInternalId = OSIX_TRUE;
                    pwssWlanDB->WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
                    if (WssIfProcessWssWlanDBMsg
                        (WSS_WLAN_GET_BSS_IFINDEX_ENTRY,
                         pwssWlanDB) != OSIX_SUCCESS)
                    {
                        WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                                    "WssMacProcessProbeReq: "
                                    "Failed to retrieve BssId from WssWlanDB\r\n");
                        continue;
                    }
                    pwssWlanDB->WssWlanIsPresentDB.bDesiredSsid = OSIX_TRUE;
                    pwssWlanDB->WssWlanIsPresentDB.bBandSelectStatus =
                        OSIX_TRUE;
                    pwssWlanDB->WssWlanIsPresentDB.bAgeOutSuppression =
                        OSIX_TRUE;
                    pwssWlanDB->WssWlanIsPresentDB.bAssocRejectCount =
                        OSIX_TRUE;
                    pwssWlanDB->WssWlanIsPresentDB.bAssocResetTime = OSIX_TRUE;
                    /*GETTING ssid for each WLAN id */
                    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY,
                                                  pwssWlanDB) != OSIX_SUCCESS)
                    {
                        WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                                    "WssMacProcessProbeReq: "
                                    "Failed to retrieve ssId from WssWlanDB\r\n");
                        continue;
                    }
                    if (pwssWlanDB->WssWlanAttributeDB.u1BandSelectStatus
                        == BAND_SELECT_ENABLED)
                    {
                        SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gi4StaSysLogId,
                                      "Update Station entry in Bandsteer DB !!!"));
                        if (MEMCMP
                            (pProbeReqStruct->unMacMsg.ProbReqMacFrame.Ssid.
                             au1MacFrameSSID,
                             pwssWlanDB->WssWlanAttributeDB.au1DesiredSsid,
                             WSSWLAN_SSID_NAME_LEN) == 0)
                        {
                            MEMCPY (WlanBssid,
                                    pwssWlanDB->WssWlanAttributeDB.BssId,
                                    sizeof (tMacAddr));
                            MEMCPY (au1ProbeSSID,
                                    pProbeReqStruct->unMacMsg.ProbReqMacFrame.
                                    Ssid.au1MacFrameSSID,
                                    WSSWLAN_SSID_NAME_LEN);
                            MEMCPY (WssStaBandSteerDB.stationMacAddress,
                                    pProbeReqStruct->unMacMsg.ProbReqMacFrame.
                                    MacMgmtFrmHdr.u1SA, sizeof (tMacAddr));
                            WSSMAC_TRC6 (WSSMAC_MGMT_TRC,
                                         "Probe Req Received from Station with mac %02x:%02x:%02x:%02x:%02x:%02x\n",
                                         pProbeReqStruct->unMacMsg.
                                         ProbReqMacFrame.MacMgmtFrmHdr.u1SA[0],
                                         WssStaBandSteerDB.stationMacAddress[1],
                                         WssStaBandSteerDB.stationMacAddress[2],
                                         WssStaBandSteerDB.stationMacAddress[3],
                                         WssStaBandSteerDB.stationMacAddress[4],
                                         WssStaBandSteerDB.
                                         stationMacAddress[5]);
                            MEMCPY (WssStaBandSteerDB.BssIdMacAddr,
                                    pwssWlanDB->WssWlanAttributeDB.BssId,
                                    sizeof (tMacAddr));
                            WSSMAC_TRC6 (WSSMAC_MGMT_TRC,
                                         "Probe Req Received from above Station in bssid %02x:%02x:%02x:%02x:%02x:%02x\n",
                                         WssStaBandSteerDB.BssIdMacAddr[0],
                                         WssStaBandSteerDB.BssIdMacAddr[1],
                                         WssStaBandSteerDB.BssIdMacAddr[2],
                                         WssStaBandSteerDB.BssIdMacAddr[3],
                                         WssStaBandSteerDB.BssIdMacAddr[4],
                                         WssStaBandSteerDB.BssIdMacAddr[5]);
                            WssStaBandSteerDB.u1StaStatus = 1;
                            WssStaUpdateBandSteerProcessDB
                                (WSSSTA_CREATE_BAND_STEER_DB,
                                 &WssStaBandSteerDB);
                            pBandSteerDB =
                                RBTreeGet (gWssStaBandSteerDB,
                                           (tRBElem *) & WssStaBandSteerDB);
                            if (pBandSteerDB != NULL)
                            {
                                WlcHdlrProbeTmrStart (pBandSteerDB,
                                                      pwssWlanDB->
                                                      WssWlanAttributeDB.
                                                      u1AgeOutSuppression);
                            }
                            break;
                        }
                    }
                }
            }
        }
    }
#endif
#endif
#if 0
    /* Fill the session id */
    pProbeReqStruct->unMacMsg.ProbReqMacFrame.u2SessId =
        pMsgStruct->unMacMsg.MacDot11PktBuf.u2SessId;

    /* Copy the ProbeReq to WlcHdlr Struct and send to WlcHdlr */
    MEMCPY (&(pWlcHdlrMsgStruct->WssMacMsgStruct.unMacMsg.ProbReqMacFrame),
            &(pProbeReqStruct->unMacMsg.ProbReqMacFrame),
            sizeof (tDot11ProbReqMacFrame));

    if (WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_WLAN_PROBE_REQ, pWlcHdlrMsgStruct)
        != WSSMAC_SUCCESS)
    {
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pProbeReqStruct);
        return WSSMAC_FAILURE;
    }
#endif
    UtlShMemFreeMacMsgStructBuf ((UINT1 *) pProbeReqStruct);
    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
#ifdef BAND_SELECT_WANTED
    UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
#endif
    return WSSMAC_SUCCESS;
}

/* ***************************************************************************
 *                                                                           *
 * Function     : WssMacProcessAuthenMsg                                     *
 *                                                                           *
 * Description  : Process the authentication frame                           *
 *                                                                           *
 * Input(s)     : MAC message type and union of struct with CRU_BUF          *
 *                                                                           *
 * Output(s)    : Authentication message structure                           *
 *                                                                           *
 * Returns      : SUCCESS/FAILURE                                            *
 *                                                                           *
 * ***************************************************************************/
UINT1
WssMacProcessAuthenMsg (UINT1 eMsgType, tWssMacMsgStruct * pMsgStruct)
{
    UINT2               u2AuthAlgo = 0, u2AuthTransSeqNum = 0;
    UINT2               u2PacketLen = 0;
    UINT1              *pu1ReadPtr = NULL;
    UINT1              *pu1Buf = NULL;
    tCRU_BUF_CHAIN_DESC *pBuf = NULL;

    WLCHDLR_PKTBUF_ALLOC_MEM_BLOCK (pu1Buf);
    if (pu1Buf == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                    "WssMacProcessAuthenMsg:-"
                    "UtlShMemAllocMacMsgStructBuf returned failure\n");
        return WSSMAC_FAILURE;
    }
    if (pMsgStruct->msgType == 0)
    {
        pBuf = pMsgStruct->unMacMsg.MacDot11PktBuf.pDot11MacPdu;

        u2PacketLen = (UINT2) WSSMAC_GET_BUF_LEN (pBuf);
        pu1ReadPtr = WSSMAC_BUF_IF_LINEAR (pBuf, 0, u2PacketLen);

        if (pu1ReadPtr == NULL)
        {
            MEMSET (pu1Buf, 0x00, u2PacketLen);
            WSSMAC_COPY_FROM_BUF (pBuf, pu1Buf, 0, u2PacketLen);
            pu1ReadPtr = pu1Buf;
        }
        pu1ReadPtr += WSSMAC_MGMT_HEADER_LEN;
        WSSMAC_GET_2BYTES (u2AuthAlgo, pu1ReadPtr);
        WSSMAC_GET_2BYTES (u2AuthTransSeqNum, pu1ReadPtr);
        if (((u2AuthAlgo == WSSMAC_AUTH_ALGO_OPEN) &&
             (u2AuthTransSeqNum == WSSMAC_AUTH_TRAN_SEQ_NO1)) ||
            ((u2AuthAlgo == WSSMAC_AUTH_ALGO_SHARED) &&
             (u2AuthTransSeqNum == WSSMAC_AUTH_TRAN_SEQ_NO1)))
        {
            if (WssMacProcessAuthenSeqIMsg (eMsgType, pMsgStruct)
                != WSSMAC_SUCCESS)
            {
                WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pu1Buf);
                return WSSMAC_FAILURE;
            }
        }
        else
        {
            if (WssMacProcessAuthenSeqIIIMsg (eMsgType, pMsgStruct) !=
                WSSMAC_SUCCESS)
            {
                WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pu1Buf);
                return WSSMAC_FAILURE;
            }
        }
    }
    else
    {
        u2AuthAlgo =
            pMsgStruct->unMacMsg.AuthMacFrame.AuthAlgoNum.u2MacFrameAuthAlgoNum;
        u2AuthTransSeqNum = pMsgStruct->unMacMsg.AuthMacFrame.AuthTransSeqNum.
            u2MacFrameAuthTransSeqNum;

        if (((u2AuthAlgo == WSSMAC_AUTH_ALGO_OPEN) &&
             (u2AuthTransSeqNum == WSSMAC_AUTH_TRAN_SEQ_NO2)) ||
            ((u2AuthAlgo == WSSMAC_AUTH_ALGO_SHARED) &&
             (u2AuthTransSeqNum == WSSMAC_AUTH_TRAN_SEQ_NO2)))
        {
            if (WssMacProcessAuthenSeqIIMsg (eMsgType, pMsgStruct) !=
                WSSMAC_SUCCESS)
            {
                WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pu1Buf);
                return WSSMAC_FAILURE;
            }
        }
        else if (((u2AuthAlgo == WSSMAC_AUTH_ALGO_SHARED) &&
                  (u2AuthTransSeqNum == WSSMAC_AUTH_TRAN_SEQ_NO4)))
        {
            if (WssMacProcessAuthenSeqIVMsg (eMsgType, pMsgStruct) !=
                WSSMAC_SUCCESS)
            {
                WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pu1Buf);
                return WSSMAC_FAILURE;
            }
        }
        else
        {
            WSSMAC_TRC (WSSMAC_FAILURE_TRC, "WssMacProcessAuthMsg:"
                        "Error WssMacProcessAuthMsg message\r\n");
            WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pu1Buf);
            return WSSMAC_FAILURE;
        }
    }
    WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pu1Buf);
    return WSSMAC_SUCCESS;
}

/* ***************************************************************************
 *                                                                           *
 * Function     : WssMacProcessAuthenSeqIMsg                                 *
 *                                                                           *
 * Description  : Process the authentication Sequence I message              *
 *                                                                           *
 * Input(s)     : MAC message type and union of struct with CRU_BUF          *
 *                                                                           *
 * Output(s)    : Authentication message structure                           *
 *                                                                           *
 * Returns      : SUCCESS/FAILURE                                            *
 *                                                                           *
 * ***************************************************************************/
UINT1
WssMacProcessAuthenSeqIMsg (UINT1 eMsgType, tWssMacMsgStruct * pMsgStruct)
{
    tWssMacMsgStruct   *pAuthMsgStruct = NULL;
    UINT2               u2PacketLen = 0, u2Count = 0, u2Count1 = 0;
    UINT1              *pu1ReadPtr = NULL;
    UINT2               u2Len = 0;
    UINT1              *pu1Buf = NULL;
    tCRU_BUF_CHAIN_DESC *pBuf = NULL;
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;

    pWlcHdlrMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pWlcHdlrMsgStruct == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                    "WssMacProcessAuthenSeqIMsg:- "
                    "UtlShMemAllocWlcBuf returned failure\n");
        return WSSMAC_FAILURE;
    }

    pAuthMsgStruct =
        (tWssMacMsgStruct *) (VOID *) UtlShMemAllocMacMsgStructBuf ();
    if (pAuthMsgStruct == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                    "WssMacProcessAuthenSeqIMsg:-"
                    "UtlShMemAllocMacMsgStructBuf returned failure\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;
    }
    WLCHDLR_PKTBUF_ALLOC_MEM_BLOCK (pu1Buf);
    if (pu1Buf == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                    "WssMacProcessAuthenSeqIMsg:-"
                    "UtlShMemAllocMacMsgStructBuf returned failure\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pAuthMsgStruct);
        return OSIX_FAILURE;
    }
    MEMSET (pAuthMsgStruct, 0, sizeof (tWssMacMsgStruct));
    MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));
    UNUSED_PARAM (eMsgType);

    pBuf = pMsgStruct->unMacMsg.MacDot11PktBuf.pDot11MacPdu;

    u2PacketLen = (UINT2) WSSMAC_GET_BUF_LEN (pBuf);
    pu1ReadPtr = WSSMAC_BUF_IF_LINEAR (pBuf, 0, u2PacketLen);

    if (pu1ReadPtr == NULL)
    {
        MEMSET (pu1Buf, 0x00, u2PacketLen);
        WSSMAC_COPY_FROM_BUF (pBuf, pu1Buf, 0, u2PacketLen);
        pu1ReadPtr = pu1Buf;
    }
    /* Get the 802.11 MAC Header */
    WSSMAC_GET_2BYTES (pAuthMsgStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.u2MacFrameCtrl, pu1ReadPtr);    /* Get Frame control field */
    WSSMAC_GET_2BYTES (pAuthMsgStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.u2MacFrameDurId, pu1ReadPtr);    /* Get Duration ID field */
    WSSMAC_GET_NBYTES (pAuthMsgStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.u1DA, pu1ReadPtr, WSSMAC_MAC_ADDR_LEN);    /* Get Address1 field */
    WSSMAC_GET_NBYTES (pAuthMsgStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.u1SA, pu1ReadPtr, WSSMAC_MAC_ADDR_LEN);    /* Get Address2 field */
    WSSMAC_GET_NBYTES (pAuthMsgStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.u1BssId, pu1ReadPtr, WSSMAC_MAC_ADDR_LEN);    /* Get Address3 field */
    WSSMAC_GET_2BYTES (pAuthMsgStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.u2MacFrameSeqCtrl, pu1ReadPtr);    /* Get Sequence control field */
    WSSMAC_GET_2BYTES (pAuthMsgStruct->unMacMsg.AuthMacFrame.AuthAlgoNum.u2MacFrameAuthAlgoNum, pu1ReadPtr);    /* Get authentication algo num */
    WSSMAC_GET_2BYTES (pAuthMsgStruct->unMacMsg.AuthMacFrame.AuthTransSeqNum.u2MacFrameAuthTransSeqNum, pu1ReadPtr);    /*Get auth trans seqnum */

#if FUTURE_WLC_WANTED

    CAPWAP_TRC6 (CAPWAP_STATION_TRC,
                 "\n AuthenSeqIMsg triggered by Station MAC:%x:%x:%x:%x:%x:%x\n",
                 pAuthMsgStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.u1SA[0],
                 pAuthMsgStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.u1SA[1],
                 pAuthMsgStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.u1SA[2],
                 pAuthMsgStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.u1SA[3],
                 pAuthMsgStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.u1SA[4],
                 pAuthMsgStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.u1SA[5]);
    SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4WlcHdlrSysLogId,
                  " AuthenSeqIMsg triggered by Station MAC:%02x:%02x:%02x:%02x:%02x:%02x",
                  pAuthMsgStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.u1SA[0],
                  pAuthMsgStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.u1SA[1],
                  pAuthMsgStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.u1SA[2],
                  pAuthMsgStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.u1SA[3],
                  pAuthMsgStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.u1SA[4],
                  pAuthMsgStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.u1SA[5]));
#endif
    /* Fill the Vendor info if Element ID ==  WSSMAC_VENDOR_INFO_ELEMENTID */
    u2Count1 = 0;
    while ((*pu1ReadPtr) == WSSMAC_VENDOR_INFO_ELEMENTID)
    {
        WSSMAC_GET_1BYTE (pAuthMsgStruct->unMacMsg.AuthMacFrame.
                          aVendSpecInfo[u2Count1].u1MacFrameElemId, pu1ReadPtr);
        WSSMAC_GET_1BYTE (pAuthMsgStruct->unMacMsg.AuthMacFrame.
                          aVendSpecInfo[u2Count1].u1MacFrameElemLen,
                          pu1ReadPtr);
        WSSMAC_GET_NBYTES (pAuthMsgStruct->unMacMsg.AuthMacFrame.
                           aVendSpecInfo[u2Count1].au1MacFrameVend_OUI,
                           pu1ReadPtr, WSSMAC_MAX_OUI_SIZE);
        u2Count = 0;
        u2Len =
            (UINT2) (pAuthMsgStruct->unMacMsg.AuthMacFrame.
                     aVendSpecInfo[u2Count1].u1MacFrameElemLen -
                     WSSMAC_MAX_OUI_SIZE);
        while (u2Count < u2Len)
        {
            WSSMAC_GET_1BYTE (pAuthMsgStruct->unMacMsg.AuthMacFrame.
                              aVendSpecInfo[u2Count1].
                              au1MacFrameVend_Content[u2Count], pu1ReadPtr);
            u2Count++;
        }
        u2Count1++;
    }

    /* Fill the session id */
    pAuthMsgStruct->unMacMsg.AuthMacFrame.u2SessId =
        pMsgStruct->unMacMsg.MacDot11PktBuf.u2SessId;
    MEMCPY (&(pWlcHdlrMsgStruct->WssMacMsgStruct.unMacMsg.AuthMacFrame),
            &(pAuthMsgStruct->unMacMsg.AuthMacFrame),
            sizeof (tDot11AuthMacFrame));

    if (WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_STA_AUTH_MSG, pWlcHdlrMsgStruct) !=
        WSSMAC_SUCCESS)
    {
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pAuthMsgStruct);
        WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pu1Buf);
        return WSSMAC_FAILURE;
    }
    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
    UtlShMemFreeMacMsgStructBuf ((UINT1 *) pAuthMsgStruct);
    WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pu1Buf);
    return WSSMAC_SUCCESS;
}

/* ***************************************************************************
 *                                                                           *
 * Function     : WssMacProcessAuthenSeqIIMsg                                *
 *                                                                           *
 * Description  : Process the authentication Sequence message II             *
 *                                                                           *
 * Input(s)     : MAC message type and union of struct with CRU_BUF          *
 *                                                                           *
 * Output(s)    : Authentication message structure                           *
 *                                                                           *
 * Returns      : SUCCESS/FAILURE                                            *
 *                                                                           *
 * ***************************************************************************/
UINT1
WssMacProcessAuthenSeqIIMsg (UINT1 eMsgType, tWssMacMsgStruct * pMsgStruct)
{
    tWssMacMsgStruct   *pAuthMsgBuf = NULL;
    unWlcHdlrMsgStruct *pCapwapMsgStruct = NULL;

    pCapwapMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pCapwapMsgStruct == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                    "WssMacProcessAuthenSeqIIMsg:- "
                    "UtlShMemAllocWlcBuf returned failure\n");
        return WSSMAC_FAILURE;
    }

    pAuthMsgBuf = (tWssMacMsgStruct *) (VOID *) UtlShMemAllocMacMsgStructBuf ();
    if (pAuthMsgBuf == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                    "WssMacProcessAuthenSeqIIMsg:-"
                    "UtlShMemAllocMacMsgStructBuf returned failure\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pCapwapMsgStruct);
        return OSIX_FAILURE;
    }
    MEMSET (pCapwapMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));
    UNUSED_PARAM (eMsgType);

    WssMacAssembleAuthFrame (pMsgStruct, pAuthMsgBuf);

    if (pAuthMsgBuf->unMacMsg.MacDot11PktBuf.pDot11MacPdu != NULL)
    {
        pCapwapMsgStruct->WlcHdlrQueueReq.pRcvBuf =
            pAuthMsgBuf->unMacMsg.MacDot11PktBuf.pDot11MacPdu;
        pCapwapMsgStruct->WlcHdlrQueueReq.u2SessId =
            pMsgStruct->unMacMsg.AuthMacFrame.u2SessId;

        pCapwapMsgStruct->WlcHdlrQueueReq.u4MsgType =
            WSS_CAPWAP_802_11_MGMT_PKT;

#if FUTURE_WLC_WANTED
        CAPWAP_TRC6 (CAPWAP_STATION_TRC,
                     "\n AuthenSeqIIMsg triggered for Station MAC:%x:%x:%x:%x:%x:%x\n",
                     pMsgStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.u1SA[0],
                     pMsgStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.u1SA[1],
                     pMsgStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.u1SA[2],
                     pMsgStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.u1SA[3],
                     pMsgStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.u1SA[4],
                     pMsgStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.u1SA[5]);
        SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4WlcHdlrSysLogId,
                      " AuthenSeqIIMsg triggered by Station MAC:%02x:%02x:%02x:%02x:%02x:%02x",
                      pMsgStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.u1SA[0],
                      pMsgStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.u1SA[1],
                      pMsgStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.u1SA[2],
                      pMsgStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.u1SA[3],
                      pMsgStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.u1SA[4],
                      pMsgStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.u1SA[5]));
#endif

        if (WssIfProcessWlcHdlrMsg
            (WSS_WLCHDLR_CAPWAP_MAC_MSG, pCapwapMsgStruct) != WSSMAC_SUCCESS)
        {
            UtlShMemFreeWlcBuf ((UINT1 *) pCapwapMsgStruct);
            UtlShMemFreeMacMsgStructBuf ((UINT1 *) pAuthMsgBuf);
            return WSSMAC_FAILURE;
        }
    }
    else
    {
        UtlShMemFreeWlcBuf ((UINT1 *) pCapwapMsgStruct);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pAuthMsgBuf);
        return WSSMAC_FAILURE;
    }
    UtlShMemFreeWlcBuf ((UINT1 *) pCapwapMsgStruct);
    UtlShMemFreeMacMsgStructBuf ((UINT1 *) pAuthMsgBuf);
    return WSSMAC_SUCCESS;
}

/* ***************************************************************************
 *                                                                           *
 * Function     : WssMacAssembleAuthFrame                                    *
 *                                                                           *
 * Description  : Assemble Authentication structure                          *
 *                                                                           *
 * Input(s)     : Union of struct with Authentication structure and char     *
 *                buffer                                                     *
 *                                                                           *
 * Output(s)    : Authentication Message Char buffer                         *
 *                                                                           *
 * Returns      : NONE                                                       *
 *                                                                           *
 * ***************************************************************************/
VOID
WssMacAssembleAuthFrame (tWssMacMsgStruct * pAuthMsgStruct,
                         tWssMacMsgStruct * pAuthMsgBuf)
{
    UINT1              *pu1ReadPtr = NULL;
    UINT1              *pu1Buf = NULL;
    UINT4               u4Len = 0;
    UINT2               u2Len = 0, u2ElementId = 0;
    UINT2               u2Count = 0, u2AuthAlgo = 0, u2AuthTransSeqNum =
        0, u2Count1 = 0;

    WLCHDLR_PKTBUF_ALLOC_MEM_BLOCK (pu1Buf);
    if (pu1Buf == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                    "WssMacProcessDeauthenMsg:-"
                    "UtlShMemAllocMacMsgStructBuf returned failure\n");
        return;
    }
    pu1ReadPtr = pu1Buf;
    /* Get the 802.11 MAC Header */
    pAuthMsgStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.u2MacFrameCtrl = 0xb000;
    WSSMAC_PUT_2BYTES (pu1ReadPtr, pAuthMsgStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.u2MacFrameCtrl);    /* Get Frame control field */
    WSSMAC_PUT_2BYTES (pu1ReadPtr, pAuthMsgStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.u2MacFrameDurId);    /* Get Duration ID field */
    WSSMAC_PUT_NBYTES (pu1ReadPtr, pAuthMsgStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.u1DA, WSSMAC_MAC_ADDR_LEN);    /* Get Address1 field */
    WSSMAC_PUT_NBYTES (pu1ReadPtr, pAuthMsgStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.u1SA, WSSMAC_MAC_ADDR_LEN);    /* Get Address2 field */
    WSSMAC_PUT_NBYTES (pu1ReadPtr, pAuthMsgStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.u1BssId, WSSMAC_MAC_ADDR_LEN);    /* Get Address3 field */
    WSSMAC_PUT_2BYTES (pu1ReadPtr, pAuthMsgStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.u2MacFrameSeqCtrl);    /* Get Sequence control field */

    WSSMAC_PUT_2BYTES (pu1ReadPtr, pAuthMsgStruct->unMacMsg.AuthMacFrame.AuthAlgoNum.u2MacFrameAuthAlgoNum);    /* Get authentication algo num */
    WSSMAC_PUT_2BYTES (pu1ReadPtr, pAuthMsgStruct->unMacMsg.AuthMacFrame.AuthTransSeqNum.u2MacFrameAuthTransSeqNum);    /* Get auth seq num */
    WSSMAC_PUT_2BYTES (pu1ReadPtr, pAuthMsgStruct->unMacMsg.AuthMacFrame.StatCode.u2MacFrameStatusCode);    /* Get status code */
    /* Get challenge text */
    u2AuthAlgo = pAuthMsgStruct->unMacMsg.AuthMacFrame.AuthAlgoNum.
        u2MacFrameAuthAlgoNum;
    u2AuthTransSeqNum = pAuthMsgStruct->unMacMsg.AuthMacFrame.AuthTransSeqNum.
        u2MacFrameAuthTransSeqNum;
    if (((u2AuthAlgo == WSSMAC_AUTH_ALGO_SHARED) &&
         (u2AuthTransSeqNum == WSSMAC_AUTH_TRAN_SEQ_NO2)))
    {
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pAuthMsgStruct->unMacMsg.
                          AuthMacFrame.ChalngTxt.u1MacFrameElemId);
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pAuthMsgStruct->unMacMsg.AuthMacFrame.
                          ChalngTxt.u1MacFrameElemLen);
        u2Len =
            pAuthMsgStruct->unMacMsg.AuthMacFrame.ChalngTxt.u1MacFrameElemLen;
        WSSMAC_PUT_NBYTES (pu1ReadPtr, pAuthMsgStruct->unMacMsg.AuthMacFrame.
                           ChalngTxt.au1MacFrameChalngTxt, u2Len);
    }

    /*Put Vendor info */
    u2Count1 = 0;
    while (u2Count1 < WSSMAC_VEND_INFO_ELEM)
    {
        u2ElementId = pAuthMsgStruct->unMacMsg.AuthMacFrame.
            aVendSpecInfo[u2Count1].u1MacFrameElemLen;
        if (u2ElementId == WSSMAC_VENDOR_INFO_ELEMENTID)
        {
            WSSMAC_PUT_1BYTE (pu1ReadPtr, pAuthMsgStruct->unMacMsg.AuthMacFrame.
                              aVendSpecInfo[u2Count1].u1MacFrameElemId);
            WSSMAC_PUT_1BYTE (pu1ReadPtr, pAuthMsgStruct->unMacMsg.AuthMacFrame.
                              aVendSpecInfo[u2Count1].u1MacFrameElemLen);
            WSSMAC_PUT_NBYTES (pu1ReadPtr,
                               pAuthMsgStruct->unMacMsg.AuthMacFrame.
                               aVendSpecInfo[u2Count1].au1MacFrameVend_OUI,
                               WSSMAC_MAX_OUI_SIZE);
            u2Count = 0;
            u2Len = (UINT2) (pAuthMsgStruct->unMacMsg.AuthMacFrame.
                             aVendSpecInfo[u2Count1].u1MacFrameElemLen -
                             WSSMAC_MAX_OUI_SIZE);
            while (u2Count < u2Len)
            {
                WSSMAC_PUT_1BYTE (pu1ReadPtr, pAuthMsgStruct->unMacMsg.
                                  AuthMacFrame.aVendSpecInfo[u2Count1].
                                  au1MacFrameVend_Content[u2Count]);
                u2Count++;
            }
        }
        u2Count1++;
    }
    u4Len = (UINT4) (pu1ReadPtr - pu1Buf);

    /* Allocate memory for packet linear buffer */
    pAuthMsgBuf->unMacMsg.MacDot11PktBuf.pDot11MacPdu =
        WSSMAC_ALLOCATE_CRU_BUF (u4Len);
    if (pAuthMsgBuf->unMacMsg.MacDot11PktBuf.pDot11MacPdu == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC, "Memory Allocation Failed \r\n");
        WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pu1Buf);
        return;
    }

    WSSMAC_COPY_TO_BUF (pAuthMsgBuf->unMacMsg.MacDot11PktBuf.pDot11MacPdu,
                        pu1Buf, 0, u4Len);
    WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pu1Buf);
    return;
}

/* ***************************************************************************
 *                                                                           *
 * Function     : WssMacProcessAuthenSeqIIIMsg                               *
 *                                                                           *
 * Description  : Process the authentication Sequence message III            *
 *                                                                           *
 * Input(s)     : MAC message type and union of struct with CRU_BUF          *
 *                                                                           *
 * Output(s)    : Authentication message structure                           *
 *                                                                           *
 * Returns      : SUCCESS/FAILURE                                            *
 *                                                                           *
 * ***************************************************************************/
UINT1
WssMacProcessAuthenSeqIIIMsg (UINT1 eMsgType, tWssMacMsgStruct * pMsgStruct)
{
    tWssMacMsgStruct   *pAuthMsgStruct = NULL;
    UINT2               u2PacketLen = 0, u2Count = 0, u2Count1 = 0;
    UINT1              *pu1ReadPtr = NULL;
    UINT2               u2Len = 0;
    UINT1              *pu1Buf = NULL;
    tCRU_BUF_CHAIN_DESC *pBuf = NULL;
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;

    pWlcHdlrMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pWlcHdlrMsgStruct == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                    "WssMacProcessAuthenSeqIIIMsg:- "
                    "UtlShMemAllocWlcBuf returned failure\n");
        return WSSMAC_FAILURE;
    }

    pAuthMsgStruct =
        (tWssMacMsgStruct *) (VOID *) UtlShMemAllocMacMsgStructBuf ();
    if (pAuthMsgStruct == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                    "WssMacProcessAuthenSeqIIIMsg:-"
                    "UtlShMemAllocMacMsgStructBuf returned failure\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;
    }
    WLCHDLR_PKTBUF_ALLOC_MEM_BLOCK (pu1Buf);
    if (pu1Buf == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                    "WssMacProcessAuthenSeqIIIMsg:-"
                    "UtlShMemAllocMacMsgStructBuf returned failure\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pAuthMsgStruct);
        return WSSMAC_FAILURE;
    }
    MEMSET (pAuthMsgStruct, 0, sizeof (tWssMacMsgStruct));
    MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));
    UNUSED_PARAM (eMsgType);

    pBuf = pMsgStruct->unMacMsg.MacDot11PktBuf.pDot11MacPdu;

    u2PacketLen = (UINT2) WSSMAC_GET_BUF_LEN (pBuf);
    pu1ReadPtr = WSSMAC_BUF_IF_LINEAR (pBuf, 0, u2PacketLen);

    if (pu1ReadPtr == NULL)
    {
        MEMSET (pu1Buf, 0x00, u2PacketLen);
        WSSMAC_COPY_FROM_BUF (pBuf, pu1Buf, 0, u2PacketLen);
        pu1ReadPtr = pu1Buf;
    }
    /* Get the 802.11 MAC Header */
    WSSMAC_GET_2BYTES (pAuthMsgStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.u2MacFrameCtrl, pu1ReadPtr);    /* Get Frame control field */
    WSSMAC_GET_2BYTES (pAuthMsgStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.u2MacFrameDurId, pu1ReadPtr);    /* Get Duration ID field */
    WSSMAC_GET_NBYTES (pAuthMsgStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.u1DA, pu1ReadPtr, WSSMAC_MAC_ADDR_LEN);    /* Get Address1 field */
    WSSMAC_GET_NBYTES (pAuthMsgStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.u1SA, pu1ReadPtr, WSSMAC_MAC_ADDR_LEN);    /* Get Address2 field */
    WSSMAC_GET_NBYTES (pAuthMsgStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.u1BssId, pu1ReadPtr, WSSMAC_MAC_ADDR_LEN);    /* Get Address3 field */
    WSSMAC_GET_2BYTES (pAuthMsgStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.u2MacFrameSeqCtrl, pu1ReadPtr);    /* Get Sequence control field */

    WSSMAC_GET_NBYTES (gau1TempBuf, pu1ReadPtr, 4);

    WSSMAC_GET_2BYTES (pAuthMsgStruct->unMacMsg.AuthMacFrame.AuthAlgoNum.u2MacFrameAuthAlgoNum, pu1ReadPtr);    /* Get authentication algo num */
    WSSMAC_GET_2BYTES (pAuthMsgStruct->unMacMsg.AuthMacFrame.AuthTransSeqNum.u2MacFrameAuthTransSeqNum, pu1ReadPtr);    /* Get auth sequence num */

    WSSMAC_GET_2BYTES (pAuthMsgStruct->unMacMsg.AuthMacFrame.StatCode.
                       u2MacFrameStatusCode, pu1ReadPtr);

    WSSMAC_GET_1BYTE (pAuthMsgStruct->unMacMsg.AuthMacFrame.ChalngTxt.
                      u1MacFrameElemId, pu1ReadPtr);
    WSSMAC_GET_1BYTE (pAuthMsgStruct->unMacMsg.AuthMacFrame.ChalngTxt.
                      u1MacFrameElemLen, pu1ReadPtr);
    u2Len = pAuthMsgStruct->unMacMsg.AuthMacFrame.ChalngTxt.u1MacFrameElemLen;
    WSSMAC_GET_NBYTES (pAuthMsgStruct->unMacMsg.AuthMacFrame.ChalngTxt.
                       au1MacFrameChalngTxt, pu1ReadPtr, u2Len);
#if FUTURE_WLC_WANTED
    CAPWAP_TRC6 (CAPWAP_STATION_TRC,
                 "\n AuthenSeqIIIMsg triggered for Station MAC:%x:%x:%x:%x:%x:%x\n",
                 pAuthMsgStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.u1SA[0],
                 pAuthMsgStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.u1SA[1],
                 pAuthMsgStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.u1SA[2],
                 pAuthMsgStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.u1SA[3],
                 pAuthMsgStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.u1SA[4],
                 pAuthMsgStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.u1SA[5]);
    SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4WlcHdlrSysLogId,
                  " AuthenSeqIIIMsg triggered by Station MAC:%02x:%02x:%02x:%02x:%02x:%02x",
                  pAuthMsgStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.u1SA[0],
                  pAuthMsgStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.u1SA[1],
                  pAuthMsgStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.u1SA[2],
                  pAuthMsgStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.u1SA[3],
                  pAuthMsgStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.u1SA[4],
                  pAuthMsgStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.u1SA[5]));
#endif

    /* Fill the Vendor info if Element ID ==  WSSMAC_VENDOR_INFO_ELEMENTID */
    u2Count1 = 0;
    while ((*pu1ReadPtr) == WSSMAC_VENDOR_INFO_ELEMENTID)
    {
        WSSMAC_GET_1BYTE (pAuthMsgStruct->unMacMsg.AuthMacFrame.
                          aVendSpecInfo[u2Count1].u1MacFrameElemId, pu1ReadPtr);
        WSSMAC_GET_1BYTE (pAuthMsgStruct->unMacMsg.AuthMacFrame.
                          aVendSpecInfo[u2Count1].u1MacFrameElemLen,
                          pu1ReadPtr);
        WSSMAC_GET_NBYTES (pAuthMsgStruct->unMacMsg.AuthMacFrame.
                           aVendSpecInfo[u2Count1].au1MacFrameVend_OUI,
                           pu1ReadPtr, WSSMAC_MAX_OUI_SIZE);
        u2Count = 0;
        u2Len =
            (UINT2) (pAuthMsgStruct->unMacMsg.AuthMacFrame.
                     aVendSpecInfo[u2Count1].u1MacFrameElemLen -
                     WSSMAC_MAX_OUI_SIZE);
        while (u2Count < u2Len)
        {
            WSSMAC_GET_1BYTE (pAuthMsgStruct->unMacMsg.AuthMacFrame.
                              aVendSpecInfo[u2Count1].
                              au1MacFrameVend_Content[u2Count], pu1ReadPtr);
            u2Count++;
        }
        u2Count1++;
    }

    /* Fill the session id */
    pAuthMsgStruct->unMacMsg.AuthMacFrame.u2SessId =
        pMsgStruct->unMacMsg.MacDot11PktBuf.u2SessId;

    MEMCPY (&(pWlcHdlrMsgStruct->WssMacMsgStruct.unMacMsg.AuthMacFrame),
            &(pAuthMsgStruct->unMacMsg.AuthMacFrame),
            sizeof (tDot11AuthMacFrame));

    if (WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_STA_AUTH_MSG, pWlcHdlrMsgStruct) !=
        WSSMAC_SUCCESS)
    {
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pAuthMsgStruct);
        WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pu1Buf);
        return WSSMAC_FAILURE;
    }
    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
    UtlShMemFreeMacMsgStructBuf ((UINT1 *) pAuthMsgStruct);
    WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pu1Buf);
    return WSSMAC_SUCCESS;
}

/* ***************************************************************************
 *                                                                           *
 * Function     : WssMacProcessAuthenSeqIVMsg                                *
 *                                                                           *
 * Description  : Process the authentication Sequence message IV             *
 *                                                                           *
 * Input(s)     : MAC message type and union of struct with CRU_BUF          *
 *                                                                           *
 * Output(s)    : Authentication message structure                           *
 *                                                                           *
 * Returns      : SUCCESS/FAILURE                                            *
 *                                                                           *
 * ***************************************************************************/
UINT1
WssMacProcessAuthenSeqIVMsg (UINT1 eMsgType, tWssMacMsgStruct * pMsgStruct)
{
    tWssMacMsgStruct   *pAuthMsgBuf = NULL;
    unWlcHdlrMsgStruct *pCapwapMsgStruct = NULL;

    pCapwapMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pCapwapMsgStruct == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                    "WssMacProcessAuthenSeqIVMsg:- "
                    "UtlShMemAllocWlcBuf returned failure\n");
        return WSSMAC_FAILURE;
    }
    pAuthMsgBuf = (tWssMacMsgStruct *) (VOID *) UtlShMemAllocMacMsgStructBuf ();
    if (pAuthMsgBuf == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                    "WssMacProcessAuthenSeqIVMsg:-"
                    "UtlShMemAllocMacMsgStructBuf returned failure\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pCapwapMsgStruct);
        return OSIX_FAILURE;
    }

    MEMSET (pAuthMsgBuf, 0, sizeof (tWssMacMsgStruct));
    MEMSET (pCapwapMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));
    UNUSED_PARAM (eMsgType);

    WssMacAssembleAuthFrame (pMsgStruct, pAuthMsgBuf);

    if (pAuthMsgBuf->unMacMsg.MacDot11PktBuf.pDot11MacPdu != NULL)
    {
        pCapwapMsgStruct->WlcHdlrQueueReq.pRcvBuf =
            pAuthMsgBuf->unMacMsg.MacDot11PktBuf.pDot11MacPdu;
        pCapwapMsgStruct->WlcHdlrQueueReq.u2SessId =
            pMsgStruct->unMacMsg.AuthMacFrame.u2SessId;

        pCapwapMsgStruct->WlcHdlrQueueReq.u4MsgType =
            WSS_CAPWAP_802_11_MGMT_PKT;
#if FUTURE_WLC_WANTED
        CAPWAP_TRC6 (CAPWAP_STATION_TRC,
                     "\n AuthenSeqIVMsg triggered for Station MAC:%x:%x:%x:%x:%x:%x\n",
                     pMsgStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.u1SA[0],
                     pMsgStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.u1SA[1],
                     pMsgStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.u1SA[2],
                     pMsgStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.u1SA[3],
                     pMsgStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.u1SA[4],
                     pMsgStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.u1SA[5]);
        SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4WlcHdlrSysLogId,
                      " AuthenSeqIVMsg triggered by Station MAC:%02x:%02x:%02x:%02x:%02x:%02x",
                      pMsgStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.u1SA[0],
                      pMsgStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.u1SA[1],
                      pMsgStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.u1SA[2],
                      pMsgStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.u1SA[3],
                      pMsgStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.u1SA[4],
                      pMsgStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.u1SA[5]));

#endif

        if (WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_CAPWAP_MAC_MSG,
                                    pCapwapMsgStruct) != WSSMAC_SUCCESS)
        {
            UtlShMemFreeWlcBuf ((UINT1 *) pCapwapMsgStruct);
            UtlShMemFreeMacMsgStructBuf ((UINT1 *) pAuthMsgBuf);
            return WSSMAC_FAILURE;
        }
    }
    else
    {
        UtlShMemFreeWlcBuf ((UINT1 *) pCapwapMsgStruct);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pAuthMsgBuf);
        return WSSMAC_FAILURE;
    }
    UtlShMemFreeWlcBuf ((UINT1 *) pCapwapMsgStruct);
    UtlShMemFreeMacMsgStructBuf ((UINT1 *) pAuthMsgBuf);
    return WSSMAC_SUCCESS;
}

/* ***************************************************************************
 *                                                                           *
 * Function     : WssMacProcessAssocReq                                      *
 *                                                                           *
 * Description  : Process Association request frame                          *
 *                                                                           *
 * Input(s)     : MAC message type and union of struct with CRU_BUF          *
 *                                                                           *
 * Output(s)    : Association request structure                              *
 *                                                                           *
 * Returns      : SUCCESS/FAILURE                                            *
 *                                                                           *
 * ***************************************************************************/
UINT1
WssMacProcessAssocReq (UINT1 eMsgType, tWssMacMsgStruct * pMsgStruct)
{
    tWssMacMsgStruct   *pAssocReqStruct = NULL;
    UINT2               u2PacketLen = 0;
    UINT2               u2Count = 0;
    UINT2               u2SupportedChannelSet = 0;
#ifdef RSNA_WANTED
    UINT1               u1RsnIELenParsed = 0;
    UINT2               u2PmkidCount = 0;
#endif
#ifdef WPA_WANTED
    UINT1               au1WpaTmpOui[WSSMAC_MAX_OUI_SIZE + 1];
    UINT1               au1wpaOui[WSSMAC_MAX_OUI_SIZE + 1] =
        { 0x00, 0x50, 0xf2, 0x01 };
#endif
#ifdef WPS_WANTED
    INT4                WpsTmpOui = 0;
    INT4                wpsOui = 0;
#endif
    UINT1               u1Tag = 0;
    UINT1               u1UnknownTagMsgLen = 0;
    UINT1              *pu1ReadPtr = NULL;
    UINT1              *pu1Buf = NULL;
    UINT4               u4Oui = 0;
    UINT4               u4WmmOui = 0;

    tCRU_BUF_CHAIN_DESC *pBuf = NULL;
    UINT2               u2Len = 0;
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;

    pWlcHdlrMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pWlcHdlrMsgStruct == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                    "WssMacProcessAssocReq:- "
                    "UtlShMemAllocWlcBuf returned failure\n");
        return WSSMAC_FAILURE;
    }

    pAssocReqStruct =
        (tWssMacMsgStruct *) (VOID *) UtlShMemAllocMacMsgStructBuf ();
    if (pAssocReqStruct == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                    "WssMacProcessAuthenSeqIIIMsg:-"
                    "UtlShMemAllocMacMsgStructBuf returned failure\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;
    }
    WLCHDLR_PKTBUF_ALLOC_MEM_BLOCK (pu1Buf);
    if (pu1Buf == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                    "WssMacProcessAssocReq:-"
                    "UtlShMemAllocMacMsgStructBuf returned failure\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pAssocReqStruct);
        return WSSMAC_FAILURE;
    }
    MEMSET (pAssocReqStruct, 0, sizeof (tWssMacMsgStruct));
    MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));
    UNUSED_PARAM (eMsgType);

    pBuf = pMsgStruct->unMacMsg.MacDot11PktBuf.pDot11MacPdu;

    u2PacketLen = (UINT2) WSSMAC_GET_BUF_LEN (pBuf);
    pu1ReadPtr = WSSMAC_BUF_IF_LINEAR (pBuf, 0, u2PacketLen);

    if (pu1ReadPtr == NULL)
    {
        MEMSET (pu1Buf, 0x00, u2PacketLen);
        WSSMAC_COPY_FROM_BUF (pBuf, pu1Buf, 0, u2PacketLen);
        pu1ReadPtr = pu1Buf;
    }
    /* Discard Assoc Request if PDU size exceeds maximum allowed length */
    if (u2PacketLen > WSSMAC_MAX_PDU_LEN)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                    "WssMacProcessAssocReq:-"
                    "PDU size exceeds maximum allowed length, Discarding Assoc Request\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pAssocReqStruct);
        WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pu1Buf);
        return WSSMAC_FAILURE;
    }

    /* ********* Construct Association Request structure ********* */
    /* Fill the 802.11 MAC Header */
    WSSMAC_GET_2BYTES (pAssocReqStruct->unMacMsg.AssocReqMacFrame.MacMgmtFrmHdr.u2MacFrameCtrl, pu1ReadPtr);    /* Get Frame control field */
    WSSMAC_GET_2BYTES (pAssocReqStruct->unMacMsg.AssocReqMacFrame.MacMgmtFrmHdr.u2MacFrameDurId, pu1ReadPtr);    /* Get Duration ID field */
    WSSMAC_GET_NBYTES (pAssocReqStruct->unMacMsg.AssocReqMacFrame.MacMgmtFrmHdr.u1DA, pu1ReadPtr, WSSMAC_MAC_ADDR_LEN);    /* Get Address1 field */
    WSSMAC_GET_NBYTES (pAssocReqStruct->unMacMsg.AssocReqMacFrame.MacMgmtFrmHdr.u1SA, pu1ReadPtr, WSSMAC_MAC_ADDR_LEN);    /* Get Address2 field */
    WSSMAC_GET_NBYTES (pAssocReqStruct->unMacMsg.AssocReqMacFrame.MacMgmtFrmHdr.u1BssId, pu1ReadPtr, WSSMAC_MAC_ADDR_LEN);    /* Get Address3 field */
    WSSMAC_GET_2BYTES (pAssocReqStruct->unMacMsg.AssocReqMacFrame.MacMgmtFrmHdr.u2MacFrameSeqCtrl, pu1ReadPtr);    /* Get Sequence control field */

    /* Get Capability info */
    WSSMAC_GET_2BYTES (pAssocReqStruct->unMacMsg.AssocReqMacFrame.Capability.
                       u2MacFrameCapability, pu1ReadPtr);

    /* Get Listen Interval */
    WSSMAC_GET_2BYTES (pAssocReqStruct->unMacMsg.AssocReqMacFrame.ListenInt.
                       u2MacFrameListnInt, pu1ReadPtr);

    u2PacketLen =
        (UINT2) (u2PacketLen - DOT11_REG_HDR_SIZE - WSSMAC_CAPABILITY_INFO_LEN -
                 WSSMAC_LISTEN_INTERVAL_LEN);

    while (u2PacketLen > 0)
    {

        u1Tag = *pu1ReadPtr;
        switch (u1Tag)
        {

            case WSSMAC_SSID_ELEMENT_ID:
                /* Fill the SSID details */
                WSSMAC_GET_1BYTE (pAssocReqStruct->unMacMsg.AssocReqMacFrame.
                                  Ssid.u1MacFrameElemId, pu1ReadPtr);
                WSSMAC_GET_1BYTE (pAssocReqStruct->unMacMsg.AssocReqMacFrame.
                                  Ssid.u1MacFrameElemLen, pu1ReadPtr);
                u2Len =
                    pAssocReqStruct->unMacMsg.AssocReqMacFrame.Ssid.
                    u1MacFrameElemLen;
                WSSMAC_GET_NBYTES (pAssocReqStruct->unMacMsg.AssocReqMacFrame.
                                   Ssid.au1MacFrameSSID, pu1ReadPtr, u2Len);
                break;
            case WSSMAC_SUPP_RATE_ELEMENT_ID:
                /* Fill the Supported Rates details */
                WSSMAC_GET_1BYTE (pAssocReqStruct->unMacMsg.AssocReqMacFrame.
                                  SuppRate.u1MacFrameElemId, pu1ReadPtr);
                WSSMAC_GET_1BYTE (pAssocReqStruct->unMacMsg.AssocReqMacFrame.
                                  SuppRate.u1MacFrameElemLen, pu1ReadPtr);
                u2Len = pAssocReqStruct->unMacMsg.AssocReqMacFrame.SuppRate.
                    u1MacFrameElemLen;
                u2Count = 0;
                while (u2Count < u2Len)
                {
                    WSSMAC_GET_1BYTE (pAssocReqStruct->unMacMsg.
                                      AssocReqMacFrame.SuppRate.
                                      au1MacFrameSuppRate[u2Count], pu1ReadPtr);
                    u2Count++;
                }
                break;

            case WSSMAC_POW_CAP_ELEMENT_ID:
                /* Fill Power capability info */
                WSSMAC_GET_1BYTE (pAssocReqStruct->unMacMsg.AssocReqMacFrame.
                                  PowCapability.u1MacFrameElemId, pu1ReadPtr);
                WSSMAC_GET_1BYTE (pAssocReqStruct->unMacMsg.AssocReqMacFrame.
                                  PowCapability.u1MacFrameElemLen, pu1ReadPtr);
                u2Len =
                    pAssocReqStruct->unMacMsg.AssocReqMacFrame.PowCapability.
                    u1MacFrameElemLen;
                WSSMAC_GET_1BYTE (pAssocReqStruct->unMacMsg.AssocReqMacFrame.
                                  PowCapability.u1MacFramePowCap_MinTxPow,
                                  pu1ReadPtr);
                WSSMAC_GET_1BYTE (pAssocReqStruct->unMacMsg.AssocReqMacFrame.
                                  PowCapability.u1MacFramePowCap_MaxTxPow,
                                  pu1ReadPtr);
                break;

            case WSSMAC_SUPP_CH_ELEMENT_ID:
                /* Fill Supported channels info */
                WSSMAC_GET_1BYTE (pAssocReqStruct->unMacMsg.AssocReqMacFrame.
                                  SuppChanls.u1MacFrameElemId, pu1ReadPtr);
                WSSMAC_GET_1BYTE (pAssocReqStruct->unMacMsg.AssocReqMacFrame.
                                  SuppChanls.u1MacFrameElemLen, pu1ReadPtr);
                u2Count = 0;
                u2Len = (pAssocReqStruct->unMacMsg.AssocReqMacFrame.SuppChanls.
                         u1MacFrameElemLen);
                u2SupportedChannelSet =
                    (pAssocReqStruct->unMacMsg.AssocReqMacFrame.SuppChanls.
                     u1MacFrameElemLen) / 2;
                while (u2Count < u2SupportedChannelSet)
                {
                    WSSMAC_GET_1BYTE (pAssocReqStruct->unMacMsg.
                                      AssocReqMacFrame.SuppChanls.
                                      SubBandDes[u2Count].
                                      u1MacFrameSuppChanl_FirChanlNum,
                                      pu1ReadPtr);
                    WSSMAC_GET_1BYTE (pAssocReqStruct->unMacMsg.
                                      AssocReqMacFrame.SuppChanls.
                                      SubBandDes[u2Count].
                                      u1MacFrameSuppChanl_NumOfChanl,
                                      pu1ReadPtr);
                    u2Count++;
                }
                break;

            case WSSMAC_EXT_SUPP_RATE_ELEMENTID:
                /* Fill the Extended Supported Rates details if Supported rates 
                 * Element ID > WSSMAC_MAX_SUPP_RATE_LEN  */
                WSSMAC_GET_1BYTE (pAssocReqStruct->unMacMsg.AssocReqMacFrame.
                                  ExtSuppRates.u1MacFrameElemId, pu1ReadPtr);
                WSSMAC_GET_1BYTE (pAssocReqStruct->unMacMsg.AssocReqMacFrame.
                                  ExtSuppRates.u1MacFrameElemLen, pu1ReadPtr);
                u2Count = 0;
                u2Len = pAssocReqStruct->unMacMsg.AssocReqMacFrame.ExtSuppRates.
                    u1MacFrameElemLen;
                while (u2Count < u2Len)
                {
                    WSSMAC_GET_1BYTE (pAssocReqStruct->unMacMsg.
                                      AssocReqMacFrame.ExtSuppRates.
                                      au1MacFrameExtSuppRates[u2Count],
                                      pu1ReadPtr);
                    u2Count++;
                }
                break;

#ifdef RSNA_WANTED
            case WSSMAC_RSN_ELEMENT_ID:
                /* Fill RSN info */
                WSSMAC_GET_1BYTE (pAssocReqStruct->unMacMsg.AssocReqMacFrame.
                                  RSNInfo.u1MacFrameElemId, pu1ReadPtr);
                WSSMAC_GET_1BYTE (pAssocReqStruct->unMacMsg.AssocReqMacFrame.
                                  RSNInfo.u1MacFrameElemLen, pu1ReadPtr);
                u2Len =
                    pAssocReqStruct->unMacMsg.AssocReqMacFrame.RSNInfo.
                    u1MacFrameElemLen;
                WSSMAC_GET_2BYTES (pAssocReqStruct->unMacMsg.AssocReqMacFrame.
                                   RSNInfo.u2MacFrameRSN_Version, pu1ReadPtr);
                u1RsnIELenParsed = (UINT1) (u1RsnIELenParsed + 2);
                pAssocReqStruct->unMacMsg.AssocReqMacFrame.RSNInfo.
                    u2MacFrameRSN_Version =
                    OSIX_HTONS (pAssocReqStruct->unMacMsg.AssocReqMacFrame.
                                RSNInfo.u2MacFrameRSN_Version);
                WSSMAC_GET_4BYTES (pAssocReqStruct->unMacMsg.AssocReqMacFrame.
                                   RSNInfo.u4MacFrameRSN_GrpCipherSuit,
                                   pu1ReadPtr);
                u1RsnIELenParsed = (UINT1) (u1RsnIELenParsed + 4);
                pAssocReqStruct->unMacMsg.AssocReqMacFrame.RSNInfo.
                    u4MacFrameRSN_GrpCipherSuit =
                    OSIX_HTONL (pAssocReqStruct->unMacMsg.AssocReqMacFrame.
                                RSNInfo.u4MacFrameRSN_GrpCipherSuit);
                WSSMAC_GET_2BYTES (pAssocReqStruct->unMacMsg.AssocReqMacFrame.
                                   RSNInfo.u2MacFrameRSN_PairCiphSuitCnt,
                                   pu1ReadPtr);
                u1RsnIELenParsed = (UINT1) (u1RsnIELenParsed + 2);
                pAssocReqStruct->unMacMsg.AssocReqMacFrame.RSNInfo.
                    u2MacFrameRSN_PairCiphSuitCnt =
                    OSIX_HTONS (pAssocReqStruct->unMacMsg.AssocReqMacFrame.
                                RSNInfo.u2MacFrameRSN_PairCiphSuitCnt);
                u2Count = 0;
                WSSMAC_GET_NBYTES (pAssocReqStruct->unMacMsg.AssocReqMacFrame.
                                   RSNInfo.
                                   aMacFrameRSN_PairCiphSuitList[u2Count].
                                   au1MacFrameRSN_CiphSuit_OUI, pu1ReadPtr,
                                   WSSMAC_MAX_OUI_SIZE);
                WSSMAC_GET_1BYTE (pAssocReqStruct->unMacMsg.AssocReqMacFrame.
                                  RSNInfo.
                                  aMacFrameRSN_PairCiphSuitList[u2Count].
                                  u1MacFrameRSN_CiphSuit_Type, pu1ReadPtr);
                u2Count++;
                u1RsnIELenParsed = (UINT1) (u1RsnIELenParsed + 4);
                WSSMAC_GET_2BYTES (pAssocReqStruct->unMacMsg.AssocReqMacFrame.
                                   RSNInfo.u2MacFrameRSN_AKMSuitCnt,
                                   pu1ReadPtr);
                u1RsnIELenParsed = (UINT1) (u1RsnIELenParsed + 2);
                pAssocReqStruct->unMacMsg.AssocReqMacFrame.RSNInfo.
                    u2MacFrameRSN_AKMSuitCnt =
                    OSIX_HTONS (pAssocReqStruct->unMacMsg.AssocReqMacFrame.
                                RSNInfo.u2MacFrameRSN_AKMSuitCnt);
                u2Count = 0;
                WSSMAC_GET_NBYTES (pAssocReqStruct->unMacMsg.AssocReqMacFrame.
                                   RSNInfo.aMacFrameRSN_AKMSuitList[u2Count].
                                   au1MacFrameRSN_AkmSuit_OUI, pu1ReadPtr,
                                   WSSMAC_MAX_OUI_SIZE);
                WSSMAC_GET_1BYTE (pAssocReqStruct->unMacMsg.AssocReqMacFrame.
                                  RSNInfo.aMacFrameRSN_AKMSuitList[u2Count].
                                  u1MacFrameRSN_AkmSuit_Type, pu1ReadPtr);
                u1RsnIELenParsed = (UINT1) (u1RsnIELenParsed + 4);
                WSSMAC_GET_2BYTES (pAssocReqStruct->unMacMsg.AssocReqMacFrame.
                                   RSNInfo.u2MacFrameRSN_Capability,
                                   pu1ReadPtr);
                u1RsnIELenParsed = (UINT1) (u1RsnIELenParsed + 2);
                pAssocReqStruct->unMacMsg.AssocReqMacFrame.RSNInfo.
                    u2MacFrameRSN_Capability =
                    OSIX_HTONS (pAssocReqStruct->unMacMsg.AssocReqMacFrame.
                                RSNInfo.u2MacFrameRSN_Capability);
                if (u1RsnIELenParsed < u2Len)
                {
                    WSSMAC_GET_2BYTES (pAssocReqStruct->unMacMsg.
                                       AssocReqMacFrame.RSNInfo.
                                       u2MacFrameRSN_PMKIdCnt, pu1ReadPtr);
                    u1RsnIELenParsed = (UINT1) (u1RsnIELenParsed + 2);
                    pAssocReqStruct->unMacMsg.AssocReqMacFrame.RSNInfo.
                        u2MacFrameRSN_PMKIdCnt =
                        OSIX_HTONS (pAssocReqStruct->unMacMsg.AssocReqMacFrame.
                                    RSNInfo.u2MacFrameRSN_PMKIdCnt);

                    u2PmkidCount =
                        pAssocReqStruct->unMacMsg.AssocReqMacFrame.RSNInfo.
                        u2MacFrameRSN_PMKIdCnt;

                    if (u2PmkidCount == WSSMAC_MAX_PKMID_CNT)
                    {
                        WSSMAC_GET_NBYTES (pAssocReqStruct->unMacMsg.
                                           AssocReqMacFrame.RSNInfo.
                                           au1MacFrameRSN_PMKIdList, pu1ReadPtr,
                                           (size_t) (16 * (u2PmkidCount)));
                        u1RsnIELenParsed = (UINT1) (u1RsnIELenParsed + 16);
                    }
                }
                break;
#endif

            case WSSMAC_QOS_CAP_ELEMENT_ID:
                /* Fill Qos capability */
                WSSMAC_GET_1BYTE (pAssocReqStruct->unMacMsg.AssocReqMacFrame.
                                  QosCap.u1MacFrameElemId, pu1ReadPtr);
                WSSMAC_GET_1BYTE (pAssocReqStruct->unMacMsg.AssocReqMacFrame.
                                  QosCap.u1MacFrameElemLen, pu1ReadPtr);
                u2Len =
                    pAssocReqStruct->unMacMsg.AssocReqMacFrame.QosCap.
                    u1MacFrameElemLen;
                WSSMAC_GET_1BYTE (pAssocReqStruct->unMacMsg.AssocReqMacFrame.
                                  QosCap.u1MacFrameQosInfo, pu1ReadPtr);
                break;

            case WSSMAC_BSS_COEXIST_ELEMENT_ID:
                /* Fill the HT 20/40 BSS Coexistence if Element ID == */
                WSSMAC_GET_1BYTE (pAssocReqStruct->unMacMsg.AssocReqMacFrame.
                                  BssCoexistence.u1MacFrameElemId, pu1ReadPtr);
                WSSMAC_GET_1BYTE (pAssocReqStruct->unMacMsg.AssocReqMacFrame.
                                  BssCoexistence.u1MacFrameElemLen, pu1ReadPtr);
                u2Len = pAssocReqStruct->unMacMsg.AssocReqMacFrame.
                    BssCoexistence.u1MacFrameElemLen;
                WSSMAC_GET_1BYTE (pAssocReqStruct->unMacMsg.AssocReqMacFrame.
                                  BssCoexistence.u1BSSCoexistInfoFields,
                                  pu1ReadPtr);
                break;

            case WSSMAC_EXT_CAPAB_ELEMENT_ID:
                /* Fill the Extended Capabilities if Element ID == */
                WSSMAC_GET_1BYTE (pAssocReqStruct->unMacMsg.AssocReqMacFrame.
                                  ExtCapabilities.u1MacFrameElemId, pu1ReadPtr);
                WSSMAC_GET_1BYTE (pAssocReqStruct->unMacMsg.AssocReqMacFrame.
                                  ExtCapabilities.u1MacFrameElemLen,
                                  pu1ReadPtr);
                u2Len =
                    pAssocReqStruct->unMacMsg.AssocReqMacFrame.ExtCapabilities.
                    u1MacFrameElemLen;
                WSSMAC_GET_NBYTES (pAssocReqStruct->unMacMsg.AssocReqMacFrame.
                                   ExtCapabilities.au1Capabilities, pu1ReadPtr,
                                   u2Len);
                break;

            case WSSMAC_VENDOR_INFO_ELEMENTID:
                /* Fill the Vendor info if Element ID ==  WSSMAC_VENDOR_INFO_ELEMENTID */
                WSSMAC_GET_1BYTE (pAssocReqStruct->unMacMsg.AssocReqMacFrame.
                                  WMMParam.u1MacFrameElemId, pu1ReadPtr);
                WSSMAC_GET_1BYTE (pAssocReqStruct->unMacMsg.AssocReqMacFrame.
                                  WMMParam.u1MacFrameElemLen, pu1ReadPtr);
                u2Len =
                    pAssocReqStruct->unMacMsg.AssocReqMacFrame.WMMParam.
                    u1MacFrameElemLen;
#ifdef WPA_WANTED
                MEMCPY (&au1WpaTmpOui, pu1ReadPtr, 4);
                u2Count = 0;
                if (MEMCMP (&au1WpaTmpOui, &au1wpaOui, 4) == 0)
                {
                    pAssocReqStruct->unMacMsg.AssocReqMacFrame.WpaInfo.elemId =
                        pAssocReqStruct->unMacMsg.AssocReqMacFrame.WMMParam.
                        u1MacFrameElemId;
                    pAssocReqStruct->unMacMsg.AssocReqMacFrame.WpaInfo.Len =
                        pAssocReqStruct->unMacMsg.AssocReqMacFrame.WMMParam.
                        u1MacFrameElemLen;
                    WSSMAC_GET_NBYTES (pAssocReqStruct->unMacMsg.
                                       AssocReqMacFrame.WpaInfo.Oui, pu1ReadPtr,
                                       4);
                    u2Count = u2Count + 4;
                    WSSMAC_GET_2BYTES (pAssocReqStruct->unMacMsg.
                                       AssocReqMacFrame.WpaInfo.u2WpaVersion,
                                       pu1ReadPtr);
                    u2Count = u2Count + 2;
                    pAssocReqStruct->unMacMsg.AssocReqMacFrame.WpaInfo.
                        u2WpaVersion =
                        OSIX_HTONS (pAssocReqStruct->unMacMsg.AssocReqMacFrame.
                                    WpaInfo.u2WpaVersion);
                    WSSMAC_GET_NBYTES (pAssocReqStruct->unMacMsg.
                                       AssocReqMacFrame.WpaInfo.
                                       au1MultiCastCipherSuite, pu1ReadPtr, 4);
                    u2Count = u2Count + 4;
                    WSSMAC_GET_2BYTES (pAssocReqStruct->unMacMsg.
                                       AssocReqMacFrame.WpaInfo.
                                       u2UniCastCipherCount, pu1ReadPtr);
                    u2Count = u2Count + 2;
                    pAssocReqStruct->unMacMsg.AssocReqMacFrame.WpaInfo.
                        u2UniCastCipherCount =
                        OSIX_HTONS (pAssocReqStruct->unMacMsg.AssocReqMacFrame.
                                    WpaInfo.u2UniCastCipherCount);

                    WSSMAC_GET_NBYTES (pAssocReqStruct->unMacMsg.
                                       AssocReqMacFrame.WpaInfo.
                                       au1UniCastCipherSuite, pu1ReadPtr, 4);
                    u2Count = u2Count + 4;
                    WSSMAC_GET_2BYTES (pAssocReqStruct->unMacMsg.
                                       AssocReqMacFrame.WpaInfo.
                                       u2ManagementKeyCount, pu1ReadPtr);
                    u2Count = u2Count + 2;
                    pAssocReqStruct->unMacMsg.AssocReqMacFrame.WpaInfo.
                        u2ManagementKeyCount =
                        OSIX_HTONS (pAssocReqStruct->unMacMsg.AssocReqMacFrame.
                                    WpaInfo.u2ManagementKeyCount);
                    WSSMAC_GET_NBYTES (pAssocReqStruct->unMacMsg.
                                       AssocReqMacFrame.WpaInfo.
                                       au1ManagementCastCipherSuite, pu1ReadPtr,
                                       4);
                    u2Count = u2Count + 4;
                    if (u2Count < u2Len)
                    {
                        WSSMAC_GET_2BYTES (pAssocReqStruct->unMacMsg.
                                           AssocReqMacFrame.WpaInfo.
                                           u2MacFrameRSN_Capability,
                                           pu1ReadPtr);
                        pAssocReqStruct->unMacMsg.AssocReqMacFrame.WpaInfo.
                            u2MacFrameRSN_Capability =
                            OSIX_HTONS (pAssocReqStruct->unMacMsg.
                                        AssocReqMacFrame.WpaInfo.
                                        u2MacFrameRSN_Capability);
                    }
                    u2Len = pAssocReqStruct->unMacMsg.AssocReqMacFrame.WpaInfo.
                        Len;
                    break;
                }
#endif
#ifdef WPS_WANTED
                MEMCPY (&WpsTmpOui, pu1ReadPtr, 4);
                wpsOui = WSSMAC_WPS_OUI;
                if (MEMCMP (&WpsTmpOui, &wpsOui, 4) == 0)
                {
                    pAssocReqStruct->unMacMsg.AssocReqMacFrame.WpsCapability.
                        elemId =
                        pAssocReqStruct->unMacMsg.AssocReqMacFrame.WMMParam.
                        u1MacFrameElemId;
                    pAssocReqStruct->unMacMsg.AssocReqMacFrame.WpsCapability.
                        Len =
                        pAssocReqStruct->unMacMsg.AssocReqMacFrame.WMMParam.
                        u1MacFrameElemLen;
                    WSSMAC_GET_NBYTES (pAssocReqStruct->unMacMsg.
                                       AssocReqMacFrame.WpsCapability.Oui,
                                       pu1ReadPtr, 4);
                    WSSMAC_GET_NBYTES (pAssocReqStruct->unMacMsg.
                                       AssocReqMacFrame.WpsCapability.Version,
                                       pu1ReadPtr, 5);
                    WSSMAC_GET_NBYTES (pAssocReqStruct->unMacMsg.
                                       AssocReqMacFrame.WpsCapability.
                                       ReqTypeTag, pu1ReadPtr, 2);
                    WSSMAC_GET_NBYTES (pAssocReqStruct->unMacMsg.
                                       AssocReqMacFrame.WpsCapability.
                                       ReqTypeLen, pu1ReadPtr, 2);
                    WSSMAC_GET_1BYTE (pAssocReqStruct->unMacMsg.
                                      AssocReqMacFrame.WpsCapability.ReqType,
                                      pu1ReadPtr);
                    if (pAssocReqStruct->unMacMsg.AssocReqMacFrame.
                        WpsCapability.Len > 14)
                    {
                        pAssocReqStruct->unMacMsg.AssocReqMacFrame.
                            WpsCapability.extLen =
                            pAssocReqStruct->unMacMsg.AssocReqMacFrame.
                            WpsCapability.Len - 14;
                        WSSMAC_GET_NBYTES (pAssocReqStruct->unMacMsg.
                                           AssocReqMacFrame.WpsCapability.
                                           extVal, pu1ReadPtr,
                                           pAssocReqStruct->unMacMsg.
                                           AssocReqMacFrame.WpsCapability.
                                           extLen);
                    }
                }
                else
#endif
                {
                    MEMCPY (&u4Oui, pu1ReadPtr, WSSMAC_MAX_OUI_SIZE);
                    u4WmmOui = WSSMAC_WMM_OUI;
                    if (((pAssocReqStruct->unMacMsg.AssocReqMacFrame.WMMParam.
                          u1MacFrameElemLen) != WSSSTA_WMM_ASSOC_REQ_TAG_LENGTH)
                        && ((MEMCMP (&u4Oui, &u4WmmOui, sizeof (u4Oui)))))
                    {
                        WSSMAC_SKIP_NBYTES (u2Len, pu1ReadPtr);
                        /* Skipping the unknown packet content   
                           until the Message Length */
                        break;
                    }
                    WSSMAC_GET_NBYTES (pAssocReqStruct->unMacMsg.
                                       AssocReqMacFrame.WMMParam.au1OUI,
                                       pu1ReadPtr, WSSMAC_MAX_OUI_SIZE);
                    WSSMAC_GET_1BYTE (pAssocReqStruct->unMacMsg.
                                      AssocReqMacFrame.WMMParam.u1OUIType,
                                      pu1ReadPtr);
                    WSSMAC_GET_1BYTE (pAssocReqStruct->unMacMsg.
                                      AssocReqMacFrame.WMMParam.u1OUISubType,
                                      pu1ReadPtr);
                    WSSMAC_GET_1BYTE (pAssocReqStruct->unMacMsg.
                                      AssocReqMacFrame.WMMParam.u1WMMVersion,
                                      pu1ReadPtr);
                    WSSMAC_GET_1BYTE (pAssocReqStruct->unMacMsg.
                                      AssocReqMacFrame.WMMParam.u1QOSInfo,
                                      pu1ReadPtr);
                    u2Len =
                        pAssocReqStruct->unMacMsg.AssocReqMacFrame.WMMParam.
                        u1MacFrameElemLen;
                }
                break;
            case WSSMAC_HT_CAPAB_ELEMENT_ID:
                /* Fill the HT capabilities of Element ID == WSSMAC_HT_CAPAB_ELEMENT_ID */
                WSSMAC_GET_1BYTE (pAssocReqStruct->unMacMsg.AssocReqMacFrame.
                                  HTCapabilities.u1MacFrameElemId, pu1ReadPtr);
                WSSMAC_GET_1BYTE (pAssocReqStruct->unMacMsg.AssocReqMacFrame.
                                  HTCapabilities.u1MacFrameElemLen, pu1ReadPtr);
                u2Len = pAssocReqStruct->unMacMsg.AssocReqMacFrame.
                    HTCapabilities.u1MacFrameElemLen;
                WSSMAC_GET_2BYTES (pAssocReqStruct->unMacMsg.AssocReqMacFrame.
                                   HTCapabilities.u2HTCapInfo, pu1ReadPtr);
                WSSMAC_GET_1BYTE (pAssocReqStruct->unMacMsg.AssocReqMacFrame.
                                  HTCapabilities.u1AMPDUParam, pu1ReadPtr);
                WSSMAC_GET_NBYTES (pAssocReqStruct->unMacMsg.AssocReqMacFrame.
                                   HTCapabilities.au1SuppMCSSet, pu1ReadPtr,
                                   WSSMAC_SUPP_MCS_SET);
                WSSMAC_GET_2BYTES (pAssocReqStruct->unMacMsg.AssocReqMacFrame.
                                   HTCapabilities.u2HTExtCap, pu1ReadPtr);
                WSSMAC_GET_4BYTES (pAssocReqStruct->unMacMsg.AssocReqMacFrame.
                                   HTCapabilities.u4TranBeamformCap,
                                   pu1ReadPtr);
                WSSMAC_GET_1BYTE (pAssocReqStruct->unMacMsg.AssocReqMacFrame.
                                  HTCapabilities.u1ASELCap, pu1ReadPtr);
                break;
            case WSSMAC_VHT_CAPAB_ELEMENT_ID:
                WSSMAC_GET_1BYTE (pAssocReqStruct->unMacMsg.AssocReqMacFrame.
                                  VHTCapabilities.u1ElemId, pu1ReadPtr);
                WSSMAC_GET_1BYTE (pAssocReqStruct->unMacMsg.AssocReqMacFrame.
                                  VHTCapabilities.u1ElemLen, pu1ReadPtr);
                u2Len = pAssocReqStruct->unMacMsg.AssocReqMacFrame.
                    VHTCapabilities.u1ElemLen;
                WSSMAC_GET_4BYTES (pAssocReqStruct->unMacMsg.AssocReqMacFrame.
                                   VHTCapabilities.u4VhtCapaInfo, pu1ReadPtr);
                WSSMAC_GET_NBYTES (pAssocReqStruct->unMacMsg.AssocReqMacFrame.
                                   VHTCapabilities.au1SuppMCS, pu1ReadPtr, 8);
                break;
            case WSSMAC_VHT_OPERATING_MODE_NOTIFICATION:
                WSSMAC_GET_1BYTE (pAssocReqStruct->unMacMsg.AssocReqMacFrame.
                                  OperModeNotify.u1ElemId, pu1ReadPtr);
                WSSMAC_GET_1BYTE (pAssocReqStruct->unMacMsg.AssocReqMacFrame.
                                  OperModeNotify.u1ElemLen, pu1ReadPtr);
                u2Len = pAssocReqStruct->unMacMsg.AssocReqMacFrame.
                    OperModeNotify.u1ElemLen;
                WSSMAC_GET_1BYTE (pAssocReqStruct->unMacMsg.AssocReqMacFrame.
                                  OperModeNotify.u1VhtOperModeElem, pu1ReadPtr);
                break;
            default:
                WSSMAC_TRC1 (WSSMAC_FAILURE_TRC,
                             "WssMacProcessAssocReq : Unknown"
                             "802.11 Tag %d\r\n", u1Tag);
                WSSMAC_SKIP_NBYTES (1, pu1ReadPtr);    /*Skipping the Type */
                WSSMAC_GET_1BYTE (u1UnknownTagMsgLen, pu1ReadPtr);
                u2Len = u1UnknownTagMsgLen;
                WSSMAC_SKIP_NBYTES (u2Len, pu1ReadPtr);    /* Skipping the packet content
                                                           until the Message Length */
                break;
        }
        u2PacketLen = (UINT2) (u2PacketLen - u2Len - 2);
    }

    pAssocReqStruct->unMacMsg.AssocReqMacFrame.u2SessId =
        pMsgStruct->unMacMsg.MacDot11PktBuf.u2SessId;

    MEMCPY (&(pWlcHdlrMsgStruct->WssMacMsgStruct.unMacMsg.AssocReqMacFrame),
            &(pAssocReqStruct->unMacMsg.AssocReqMacFrame),
            sizeof (tDot11AssocReqMacFrame));

    if (WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_STA_ASSOC_REQ, pWlcHdlrMsgStruct) !=
        WSSMAC_SUCCESS)
    {
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pAssocReqStruct);
        WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pu1Buf);
        return WSSMAC_FAILURE;
    }
    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
    UtlShMemFreeMacMsgStructBuf ((UINT1 *) pAssocReqStruct);
    WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pu1Buf);
    return WSSMAC_SUCCESS;
}

/* ***************************************************************************
 *                                                                           *
 * Function     : WssMacProcessAssocRsp                                      *
 *                                                                           *
 * Description  : Process Association response frame                         *
 *                                                                           *
 * Input(s)     : MAC message type and union of struct with Association      *
 *                response structure                                         *
 *                                                                           *
 * Output(s)    : Association response message buffer                        *
 *                                                                           *
 * Returns      : SUCCESS/FAILURE                                            *
 *                                                                           *
 * ***************************************************************************/
UINT1
WssMacProcessAssocRsp (UINT1 eMsgType, tWssMacMsgStruct * pAssocRspStruct)
{
    tWssMacMsgStruct   *pAssocRspBuf = NULL;
    unWlcHdlrMsgStruct *pCapwapMsgStruct = NULL;

    pCapwapMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pCapwapMsgStruct == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                    "WssMacProcessAssocRsp:- "
                    "UtlShMemAllocWlcBuf returned failure\n");
        return WSSMAC_FAILURE;
    }

    pAssocRspBuf =
        (tWssMacMsgStruct *) (VOID *) UtlShMemAllocMacMsgStructBuf ();
    if (pAssocRspBuf == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                    "WssMacProcessAssocRsp:-"
                    "UtlShMemAllocMacMsgStructBuf returned failure\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pCapwapMsgStruct);
        return OSIX_FAILURE;
    }
    MEMSET (pAssocRspBuf, 0, sizeof (tWssMacMsgStruct));
    MEMSET (pCapwapMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));
    UNUSED_PARAM (eMsgType);

    pAssocRspStruct->unMacMsg.AssocRspMacFrame.MacMgmtFrmHdr.u2MacFrameCtrl &=
        WSSMAC_SET_ASSOCRSP_FRAME_CTRL1;
    pAssocRspStruct->unMacMsg.AssocRspMacFrame.MacMgmtFrmHdr.u2MacFrameCtrl |=
        WSSMAC_SET_ASSOCRSP_FRAME_CTRL2;

    WssMacAssembleAssocRspFrame (pAssocRspStruct, pAssocRspBuf);
    if (pAssocRspBuf->unMacMsg.MacDot11PktBuf.pDot11MacPdu != NULL)
    {
        pCapwapMsgStruct->WlcHdlrQueueReq.pRcvBuf =
            pAssocRspBuf->unMacMsg.MacDot11PktBuf.pDot11MacPdu;
        pCapwapMsgStruct->WlcHdlrQueueReq.u2SessId =
            pAssocRspStruct->unMacMsg.AssocRspMacFrame.u2SessId;

        pCapwapMsgStruct->WlcHdlrQueueReq.u4MsgType =
            WSS_CAPWAP_802_11_MGMT_PKT;

        CAPWAP_TRC6 (CAPWAP_STATION_TRC,
                     "ASSOC Resp sent to %02x:%02x:%02x:%02x:%02x:%02x\n",
                     pAssocRspStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.
                     u1DA[0],
                     pAssocRspStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.
                     u1DA[1],
                     pAssocRspStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.
                     u1DA[2],
                     pAssocRspStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.
                     u1DA[3],
                     pAssocRspStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.
                     u1DA[4],
                     pAssocRspStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.
                     u1DA[5]);
        SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4WlcHdlrSysLogId,
                      " ASSOC Resp sent to %02x:%02x:%02x:%02x:%02x:%02x",
                      pAssocRspStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.
                      u1DA[0],
                      pAssocRspStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.
                      u1DA[1],
                      pAssocRspStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.
                      u1DA[2],
                      pAssocRspStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.
                      u1DA[3],
                      pAssocRspStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.
                      u1DA[4],
                      pAssocRspStruct->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.
                      u1DA[5]));

        if (WssIfProcessWlcHdlrMsg
            (WSS_WLCHDLR_CAPWAP_MAC_MSG, pCapwapMsgStruct) != WSSMAC_SUCCESS)
        {
            UtlShMemFreeWlcBuf ((UINT1 *) pCapwapMsgStruct);
            UtlShMemFreeMacMsgStructBuf ((UINT1 *) pAssocRspBuf);
            return WSSMAC_FAILURE;
        }
    }
    else
    {
        UtlShMemFreeWlcBuf ((UINT1 *) pCapwapMsgStruct);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pAssocRspBuf);
        return WSSMAC_FAILURE;
    }
    UtlShMemFreeWlcBuf ((UINT1 *) pCapwapMsgStruct);
    UtlShMemFreeMacMsgStructBuf ((UINT1 *) pAssocRspBuf);
    return WSSMAC_SUCCESS;
}

/* ***************************************************************************
 *                                                                           *
 * Function     : WssMacAssembleAssocRspFrame                                *
 *                                                                           *
 * Description  : Assemble Association response structure                    *
 *                                                                           *
 * Input(s)     : Union of struct with Association response structure and    *
 *                char buffer                                                *
 *                                                                           *
 * Output(s)    : Association response message buffer                        *
 *                                                                           *
 * Returns      : NONE                                                       *
 *                                                                           *
 * ***************************************************************************/
VOID
WssMacAssembleAssocRspFrame (tWssMacMsgStruct * pAssocRspStruct,
                             tWssMacMsgStruct * pAssocRspBuf)
{
    UINT1              *pu1ReadPtr = NULL;
    UINT4               u4Len = 0;
    UINT2               u2Count = 0, u2Len = 0, u2Count1 = 0;
    UINT1               u1ElementId = 0;
    UINT1              *pu1Buf = NULL;
    WLCHDLR_PKTBUF_ALLOC_MEM_BLOCK (pu1Buf);
    if (pu1Buf == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                    "WssMacAssembleAssocRspFrame:-"
                    "UtlShMemAllocMacMsgStructBuf returned failure\n");
        return;
    }
    pu1ReadPtr = pu1Buf;
    /* Put the 802.11 MAC Header */
    WSSMAC_PUT_2BYTES (pu1ReadPtr, pAssocRspStruct->unMacMsg.AssocRspMacFrame.MacMgmtFrmHdr.u2MacFrameCtrl);    /* Put Frame control field */
    WSSMAC_PUT_2BYTES (pu1ReadPtr, pAssocRspStruct->unMacMsg.AssocRspMacFrame.MacMgmtFrmHdr.u2MacFrameDurId);    /* Put Duration ID field */
    WSSMAC_PUT_NBYTES (pu1ReadPtr, pAssocRspStruct->unMacMsg.AssocRspMacFrame.MacMgmtFrmHdr.u1DA, WSSMAC_MAC_ADDR_LEN);    /* Put Address1 field */
    WSSMAC_PUT_NBYTES (pu1ReadPtr, pAssocRspStruct->unMacMsg.AssocRspMacFrame.MacMgmtFrmHdr.u1SA, WSSMAC_MAC_ADDR_LEN);    /* Put Address2 field */
    WSSMAC_PUT_NBYTES (pu1ReadPtr, pAssocRspStruct->unMacMsg.AssocRspMacFrame.MacMgmtFrmHdr.u1BssId, WSSMAC_MAC_ADDR_LEN);    /* Put Address3 field */
    WSSMAC_PUT_2BYTES (pu1ReadPtr, pAssocRspStruct->unMacMsg.AssocRspMacFrame.MacMgmtFrmHdr.u2MacFrameSeqCtrl);    /* Put Sequence control field */

    /*  Get Capability info */
    WSSMAC_PUT_2BYTES (pu1ReadPtr, pAssocRspStruct->unMacMsg.AssocRspMacFrame.
                       Capability.u2MacFrameCapability);

    /* Get Status code */
    WSSMAC_PUT_2BYTES (pu1ReadPtr, pAssocRspStruct->unMacMsg.AssocRspMacFrame.
                       StatCode.u2MacFrameStatusCode);

    /* Get Association ID */

    WSSMAC_PUT_2BYTES (pu1ReadPtr,
                       pAssocRspStruct->unMacMsg.AssocRspMacFrame.Aid.
                       u2MacFrameAID);

    u1ElementId =
        pAssocRspStruct->unMacMsg.AssocRspMacFrame.SuppRate.u1MacFrameElemId;
    if (u1ElementId == WSSMAC_SUPP_RATE_ELEMENT_ID)
    {
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          pAssocRspStruct->unMacMsg.AssocRspMacFrame.SuppRate.
                          u1MacFrameElemId);
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          pAssocRspStruct->unMacMsg.AssocRspMacFrame.SuppRate.
                          u1MacFrameElemLen);
        u2Len =
            pAssocRspStruct->unMacMsg.AssocRspMacFrame.SuppRate.
            u1MacFrameElemLen;
        u2Count = 0;
        while (u2Count < u2Len)
        {
            WSSMAC_PUT_1BYTE (pu1ReadPtr, pAssocRspStruct->unMacMsg.
                              AssocRspMacFrame.SuppRate.
                              au1MacFrameSuppRate[u2Count]);
            u2Count++;
        }
    }

    /* Get the Extended Supported Rates details */
    u1ElementId = pAssocRspStruct->unMacMsg.AssocRspMacFrame.ExtSuppRates.
        u1MacFrameElemId;
    if (u1ElementId == WSSMAC_EXT_SUPP_RATE_ELEMENTID)
    {
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          pAssocRspStruct->unMacMsg.AssocRspMacFrame.
                          ExtSuppRates.u1MacFrameElemId);
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          pAssocRspStruct->unMacMsg.AssocRspMacFrame.
                          ExtSuppRates.u1MacFrameElemLen);
        u2Count = 0;
        u2Len = pAssocRspStruct->unMacMsg.AssocRspMacFrame.ExtSuppRates.
            u1MacFrameElemLen;
        while (u2Count < u2Len)
        {
            WSSMAC_PUT_1BYTE (pu1ReadPtr, pAssocRspStruct->unMacMsg.
                              AssocRspMacFrame.ExtSuppRates.
                              au1MacFrameExtSuppRates[u2Count]);
            u2Count++;
        }
    }

    /* Put HT capabilities */
    u1ElementId = pAssocRspStruct->unMacMsg.AssocRspMacFrame.
        HTCapabilities.u1MacFrameElemId;
    if (u1ElementId == WSSMAC_HT_CAPAB_ELEMENT_ID)
    {
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          pAssocRspStruct->unMacMsg.AssocRspMacFrame.
                          HTCapabilities.u1MacFrameElemId);
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          pAssocRspStruct->unMacMsg.AssocRspMacFrame.
                          HTCapabilities.u1MacFrameElemLen);
        WSSMAC_PUT_2BYTES (pu1ReadPtr,
                           pAssocRspStruct->unMacMsg.AssocRspMacFrame.
                           HTCapabilities.u2HTCapInfo);
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          pAssocRspStruct->unMacMsg.AssocRspMacFrame.
                          HTCapabilities.u1AMPDUParam);
        WSSMAC_PUT_NBYTES (pu1ReadPtr,
                           pAssocRspStruct->unMacMsg.AssocRspMacFrame.
                           HTCapabilities.au1SuppMCSSet, WSSMAC_SUPP_MCS_SET);
        WSSMAC_PUT_2BYTES (pu1ReadPtr,
                           pAssocRspStruct->unMacMsg.AssocRspMacFrame.
                           HTCapabilities.u2HTExtCap);
        WSSMAC_PUT_4BYTES (pu1ReadPtr,
                           pAssocRspStruct->unMacMsg.AssocRspMacFrame.
                           HTCapabilities.u4TranBeamformCap);
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          pAssocRspStruct->unMacMsg.AssocRspMacFrame.
                          HTCapabilities.u1ASELCap);
    }

    /* Put HT operation */
    u1ElementId = pAssocRspStruct->unMacMsg.AssocRspMacFrame.HTOperation.
        u1MacFrameElemId;
    if (u1ElementId == WSSMAC_HT_OPE_ELEMENT_ID)
    {
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          pAssocRspStruct->unMacMsg.AssocRspMacFrame.
                          HTOperation.u1MacFrameElemId);
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          pAssocRspStruct->unMacMsg.AssocRspMacFrame.
                          HTOperation.u1MacFrameElemLen);
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          pAssocRspStruct->unMacMsg.AssocRspMacFrame.
                          HTOperation.u1PrimaryCh);
        WSSMAC_PUT_NBYTES (pu1ReadPtr,
                           pAssocRspStruct->unMacMsg.AssocRspMacFrame.
                           HTOperation.au1HTOpeInfo, WSSMAC_HTOPE_INFO);
        WSSMAC_PUT_NBYTES (pu1ReadPtr,
                           pAssocRspStruct->unMacMsg.AssocRspMacFrame.
                           HTOperation.au1BasicMCSSet, WSSMAC_BASIC_MCS_SET);
    }

    /* Put VHT capabilities */
    u1ElementId = pAssocRspStruct->unMacMsg.AssocRspMacFrame.
        VHTCapabilities.u1ElemId;
    if (u1ElementId == WSSMAC_VHT_CAPAB_ELEMENT_ID)
    {
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          pAssocRspStruct->unMacMsg.AssocRspMacFrame.
                          VHTCapabilities.u1ElemId);
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          pAssocRspStruct->unMacMsg.AssocRspMacFrame.
                          VHTCapabilities.u1ElemLen);
        WSSMAC_PUT_4BYTES (pu1ReadPtr,
                           pAssocRspStruct->unMacMsg.AssocRspMacFrame.
                           VHTCapabilities.u4VhtCapaInfo);
        WSSMAC_PUT_NBYTES (pu1ReadPtr,
                           pAssocRspStruct->unMacMsg.AssocRspMacFrame.
                           VHTCapabilities.au1SuppMCS, DOT11AC_VHT_CAP_MCS_LEN);
    }

    /* Put VHT operation */
    u1ElementId = pAssocRspStruct->unMacMsg.AssocRspMacFrame.VHTOperation.
        u1ElemId;
    if (u1ElementId == WSSMAC_VHT_OPE_ELEMENT_ID)
    {
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          pAssocRspStruct->unMacMsg.AssocRspMacFrame.
                          VHTOperation.u1ElemId);
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          pAssocRspStruct->unMacMsg.AssocRspMacFrame.
                          VHTOperation.u1ElemLen);
        WSSMAC_PUT_NBYTES (pu1ReadPtr,
                           pAssocRspStruct->unMacMsg.AssocRspMacFrame.
                           VHTOperation.au1VhtOperInfo,
                           DOT11AC_VHT_OPER_INFO_LEN);
        WSSMAC_PUT_2BYTES (pu1ReadPtr,
                           pAssocRspStruct->unMacMsg.AssocRspMacFrame.
                           VHTOperation.u2BasicMCS);
    }

    /* Put HT 20/40 BSS Coexistence */
    u1ElementId = pAssocRspStruct->unMacMsg.AssocRspMacFrame.BssCoexistence.
        u1MacFrameElemId;
    if (u1ElementId == WSSMAC_BSS_COEXIST_ELEMENT_ID)
    {
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          pAssocRspStruct->unMacMsg.AssocRspMacFrame.
                          BssCoexistence.u1MacFrameElemId);
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          pAssocRspStruct->unMacMsg.AssocRspMacFrame.
                          BssCoexistence.u1MacFrameElemLen);
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          pAssocRspStruct->unMacMsg.AssocRspMacFrame.
                          BssCoexistence.u1BSSCoexistInfoFields);
    }

    /* Put OBSS Scan parameters */
    u1ElementId = pAssocRspStruct->unMacMsg.AssocRspMacFrame.OBSSScanParams.
        u1MacFrameElemId;
    if (u1ElementId == WSSMAC_OBSS_SCAN_PARAM_ELEMENT_ID)
    {
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          pAssocRspStruct->unMacMsg.AssocRspMacFrame.
                          OBSSScanParams.u1MacFrameElemId);
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          pAssocRspStruct->unMacMsg.AssocRspMacFrame.
                          OBSSScanParams.u1MacFrameElemLen);
        WSSMAC_PUT_2BYTES (pu1ReadPtr,
                           pAssocRspStruct->unMacMsg.AssocRspMacFrame.
                           OBSSScanParams.u2OBSSScanPassiveDwell);
        WSSMAC_PUT_2BYTES (pu1ReadPtr,
                           pAssocRspStruct->unMacMsg.AssocRspMacFrame.
                           OBSSScanParams.u2OBSSScanActiveDwell);
        WSSMAC_PUT_2BYTES (pu1ReadPtr,
                           pAssocRspStruct->unMacMsg.AssocRspMacFrame.
                           OBSSScanParams.u2BSSChaWidTriScanInt);
        WSSMAC_PUT_2BYTES (pu1ReadPtr,
                           pAssocRspStruct->unMacMsg.AssocRspMacFrame.
                           OBSSScanParams.u2OBSSScanPassiveTotalPerCh);
        WSSMAC_PUT_2BYTES (pu1ReadPtr,
                           pAssocRspStruct->unMacMsg.AssocRspMacFrame.
                           OBSSScanParams.u2OBSSScanActiveTotalPerCh);
        WSSMAC_PUT_2BYTES (pu1ReadPtr,
                           pAssocRspStruct->unMacMsg.AssocRspMacFrame.
                           OBSSScanParams.u2BSSWidChTransDelayFactor);
        WSSMAC_PUT_2BYTES (pu1ReadPtr,
                           pAssocRspStruct->unMacMsg.AssocRspMacFrame.
                           OBSSScanParams.u2OBSSScanActivityThreshold);
    }

    /* Put Extended Capabilities */
    u1ElementId = pAssocRspStruct->unMacMsg.AssocRspMacFrame.ExtCapabilities.
        u1MacFrameElemId;
    if (u1ElementId == WSSMAC_EXT_CAPAB_ELEMENT_ID)
    {
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          pAssocRspStruct->unMacMsg.AssocRspMacFrame.
                          ExtCapabilities.u1MacFrameElemId);
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          pAssocRspStruct->unMacMsg.AssocRspMacFrame.
                          ExtCapabilities.u1MacFrameElemLen);
        WSSMAC_PUT_NBYTES (pu1ReadPtr,
                           pAssocRspStruct->unMacMsg.AssocRspMacFrame.
                           ExtCapabilities.au1Capabilities, pAssocRspStruct->
                           unMacMsg.AssocRspMacFrame.
                           ExtCapabilities.u1MacFrameElemLen);
    }

    /* Put Operating Mode Notification Element */
    u1ElementId = pAssocRspStruct->unMacMsg.AssocRspMacFrame.OperModeNotify.
        u1ElemId;
    if (u1ElementId == WSSMAC_VHT_OPERATING_MODE_NOTIFICATION)
    {
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          pAssocRspStruct->unMacMsg.AssocRspMacFrame.
                          OperModeNotify.u1ElemId);
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          pAssocRspStruct->unMacMsg.AssocRspMacFrame.
                          OperModeNotify.u1ElemLen);
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          pAssocRspStruct->unMacMsg.AssocRspMacFrame.
                          OperModeNotify.u1VhtOperModeElem);
    }

    /* Put EDCA Parameters */

    u1ElementId = pAssocRspStruct->unMacMsg.AssocRspMacFrame.
        WMMParam.u1MacFrameElemId;
    if (u1ElementId == WSSMAC_VENDOR_INFO_ELEMENTID)
    {

        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          pAssocRspStruct->unMacMsg.AssocRspMacFrame.WMMParam.
                          u1MacFrameElemId);
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          pAssocRspStruct->unMacMsg.AssocRspMacFrame.WMMParam.
                          u1MacFrameElemLen);
        WSSMAC_PUT_NBYTES (pu1ReadPtr,
                           pAssocRspStruct->unMacMsg.AssocRspMacFrame.WMMParam.
                           au1OUI, WSSMAC_MAX_OUI_SIZE);
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          pAssocRspStruct->unMacMsg.AssocRspMacFrame.WMMParam.
                          u1OUIType);
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          pAssocRspStruct->unMacMsg.AssocRspMacFrame.WMMParam.
                          u1OUISubType);
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          pAssocRspStruct->unMacMsg.AssocRspMacFrame.WMMParam.
                          u1WMMVersion);
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          pAssocRspStruct->unMacMsg.AssocRspMacFrame.WMMParam.
                          u1QOSInfo);
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          pAssocRspStruct->unMacMsg.AssocRspMacFrame.WMMParam.
                          u1Rsvrd);

        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          pAssocRspStruct->unMacMsg.AssocRspMacFrame.EDCAParam.
                          MacFrameEDCA_AC_BE.u1MacFrameEDCA_AC_ACI_AFSN);
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          pAssocRspStruct->unMacMsg.AssocRspMacFrame.EDCAParam.
                          MacFrameEDCA_AC_BE.u1MacFrameEDCA_AC_ECWMin_Max);
        WSSMAC_PUT_2BYTES (pu1ReadPtr,
                           pAssocRspStruct->unMacMsg.AssocRspMacFrame.EDCAParam.
                           MacFrameEDCA_AC_BE.u2MacFrameEDCA_AC_TxOpLimit);

        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          pAssocRspStruct->unMacMsg.AssocRspMacFrame.EDCAParam.
                          MacFrameEDCA_AC_BK.u1MacFrameEDCA_AC_ACI_AFSN);
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          pAssocRspStruct->unMacMsg.AssocRspMacFrame.EDCAParam.
                          MacFrameEDCA_AC_BK.u1MacFrameEDCA_AC_ECWMin_Max);
        WSSMAC_PUT_2BYTES (pu1ReadPtr,
                           pAssocRspStruct->unMacMsg.AssocRspMacFrame.EDCAParam.
                           MacFrameEDCA_AC_BK.u2MacFrameEDCA_AC_TxOpLimit);

        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          pAssocRspStruct->unMacMsg.AssocRspMacFrame.EDCAParam.
                          MacFrameEDCA_AC_VI.u1MacFrameEDCA_AC_ACI_AFSN);
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          pAssocRspStruct->unMacMsg.AssocRspMacFrame.EDCAParam.
                          MacFrameEDCA_AC_VI.u1MacFrameEDCA_AC_ECWMin_Max);
        WSSMAC_PUT_2BYTES (pu1ReadPtr,
                           pAssocRspStruct->unMacMsg.AssocRspMacFrame.EDCAParam.
                           MacFrameEDCA_AC_VI.u2MacFrameEDCA_AC_TxOpLimit);

        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          pAssocRspStruct->unMacMsg.AssocRspMacFrame.EDCAParam.
                          MacFrameEDCA_AC_VO.u1MacFrameEDCA_AC_ACI_AFSN);
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          pAssocRspStruct->unMacMsg.AssocRspMacFrame.EDCAParam.
                          MacFrameEDCA_AC_VO.u1MacFrameEDCA_AC_ECWMin_Max);
        WSSMAC_PUT_2BYTES (pu1ReadPtr,
                           pAssocRspStruct->unMacMsg.AssocRspMacFrame.EDCAParam.
                           MacFrameEDCA_AC_VO.u2MacFrameEDCA_AC_TxOpLimit);
    }
#ifdef WPS_WANTED
    u1ElementId = pAssocRspStruct->unMacMsg.AssocRspMacFrame.
        WpsCapability.elemId;
    if (u1ElementId == WSSMAC_VENDOR_INFO_ELEMENTID)
    {
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          pAssocRspStruct->unMacMsg.AssocRspMacFrame.
                          WpsCapability.elemId);
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          pAssocRspStruct->unMacMsg.AssocRspMacFrame.
                          WpsCapability.Len);
        WSSMAC_PUT_NBYTES (pu1ReadPtr,
                           pAssocRspStruct->unMacMsg.AssocRspMacFrame.
                           WpsCapability.Oui, 4);
        WSSMAC_PUT_NBYTES (pu1ReadPtr,
                           pAssocRspStruct->unMacMsg.AssocRspMacFrame.
                           WpsCapability.Version, 5);
        WSSMAC_PUT_NBYTES (pu1ReadPtr,
                           pAssocRspStruct->unMacMsg.AssocRspMacFrame.
                           WpsCapability.ReqTypeTag, 2);
        WSSMAC_PUT_NBYTES (pu1ReadPtr,
                           pAssocRspStruct->unMacMsg.AssocRspMacFrame.
                           WpsCapability.ReqTypeLen, 2);
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          pAssocRspStruct->unMacMsg.AssocRspMacFrame.
                          WpsCapability.ReqType);
        WSSMAC_PUT_NBYTES (pu1ReadPtr,
                           pAssocRspStruct->unMacMsg.AssocRspMacFrame.
                           WpsCapability.extVal,
                           pAssocRspStruct->unMacMsg.AssocRspMacFrame.
                           WpsCapability.extLen);
    }
#endif
    /* Put vendor info */
    u2Count1 = 0;
    while (u2Count1 < WSSMAC_VEND_INFO_ELEM)
    {
        u1ElementId = pAssocRspStruct->unMacMsg.AssocRspMacFrame.
            aVendSpecInfo[u2Count1].u1MacFrameElemId;
        if (u1ElementId == WSSMAC_VENDOR_INFO_ELEMENTID)
        {
            WSSMAC_PUT_1BYTE (pu1ReadPtr, pAssocRspStruct->unMacMsg.
                              AssocRspMacFrame.aVendSpecInfo[u2Count1].
                              u1MacFrameElemId);
            WSSMAC_PUT_1BYTE (pu1ReadPtr,
                              pAssocRspStruct->unMacMsg.AssocRspMacFrame.
                              aVendSpecInfo[u2Count1].u1MacFrameElemLen);
            WSSMAC_PUT_NBYTES (pu1ReadPtr,
                               pAssocRspStruct->unMacMsg.AssocRspMacFrame.
                               aVendSpecInfo[u2Count1].au1MacFrameVend_OUI,
                               WSSMAC_MAX_OUI_SIZE);
            u2Count = 0;
            u2Len = (UINT2) (pAssocRspStruct->unMacMsg.AssocRspMacFrame.
                             aVendSpecInfo[u2Count1].u1MacFrameElemLen -
                             WSSMAC_MAX_OUI_SIZE);
            while (u2Count < u2Len)
            {
                WSSMAC_PUT_1BYTE (pu1ReadPtr, pAssocRspStruct->unMacMsg.
                                  AssocRspMacFrame.aVendSpecInfo[u2Count1].
                                  au1MacFrameVend_Content[u2Count]);
                u2Count++;
            }
        }
        u2Count1++;
    }

#ifdef PMF_WANTED

    u1ElementId = pAssocRspStruct->unMacMsg.AssocRspMacFrame.
        TimeoutInterval.u1MacFrameElemId;

    if (u1ElementId == WSSMAC_TIMEOUT_INTERVAL_ELEMENT_ID)
    {
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          pAssocRspStruct->unMacMsg.AssocRspMacFrame.
                          TimeoutInterval.u1MacFrameElemId);
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          pAssocRspStruct->unMacMsg.AssocRspMacFrame.
                          TimeoutInterval.u1MacFrameElemLen);
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          pAssocRspStruct->unMacMsg.AssocRspMacFrame.
                          TimeoutInterval.u1TimeoutIntervalType);

        WSSMAC_PUT_4BYTES (pu1ReadPtr,
                           pAssocRspStruct->unMacMsg.AssocRspMacFrame.
                           TimeoutInterval.u4TimeoutIntervalValue);
    }
#endif
    /* u4Len = sizeof (pu1ReadPtr); */
    u4Len = (UINT4) (pu1ReadPtr - pu1Buf);

    /* Allocate memory for packet linear buffer */
    pAssocRspBuf->unMacMsg.MacDot11PktBuf.pDot11MacPdu =
        WSSMAC_ALLOCATE_CRU_BUF (u4Len);
    if (pAssocRspBuf->unMacMsg.MacDot11PktBuf.pDot11MacPdu == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC, "Memory Allocation Failed \r\n");
        WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pu1Buf);
        return;
    }

    WSSMAC_COPY_TO_BUF (pAssocRspBuf->unMacMsg.MacDot11PktBuf.pDot11MacPdu,
                        pu1Buf, 0, u4Len);
    WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pu1Buf);
    return;
}

/* ***************************************************************************
 *                                                                           *
 * Function     : WssMacProcessReassocReq                                    *
 *                                                                           *
 * Description  : Process Reassociation request frame                        *
 *                                                                           *
 * Input(s)     : MAC message type and union of struct with CRU_BUF          *
 *                                                                           *
 * Output(s)    : Reassociation request structure                            *
 *                                                                           *
 * Returns      : SUCCESS/FAILURE                                            *
 *                                                                           *
 * ***************************************************************************/
UINT1
WssMacProcessReassocReq (UINT1 eMsgType, tWssMacMsgStruct * pMsgStruct)
{
    tWssMacMsgStruct   *pReassocReqStruct = NULL;
    UINT2               u2PacketLen = 0;
    UINT2               u2Count = 0;
    UINT2               u2SupportedChannelSet = 0;
    UINT1               u1UnknownTagMsgLen = 0;
#ifdef RSNA_WANTED
    UINT1               u1RsnIELenParsed = 0;
    UINT2               u2PmkidCount = 0;
#endif
#ifdef WPA_WANTED
    INT4                WpaTmpOui = 0;
    INT4                wpaOui = 0;
#endif
    UINT1              *pu1ReadPtr = NULL;
    UINT1              *pu1Buf = NULL;
    tCRU_BUF_CHAIN_DESC *pBuf = NULL;
    UINT2               u2Len = 0;
    UINT1               u1Tag = 0;
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;
    UINT4               u4Oui = 0;
    UINT4               u4WmmOui = 0;

    pWlcHdlrMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pWlcHdlrMsgStruct == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                    "WssMacProcessReassocReq:- "
                    "UtlShMemAllocWlcBuf returned failure\n");
        return WSSMAC_FAILURE;
    }

    pReassocReqStruct =
        (tWssMacMsgStruct *) (VOID *) UtlShMemAllocMacMsgStructBuf ();
    if (pReassocReqStruct == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                    "WssMacProcessReassocReq:-"
                    "UtlShMemAllocMacMsgStructBuf returned failure\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;
    }
    WLCHDLR_PKTBUF_ALLOC_MEM_BLOCK (pu1Buf);
    if (pu1Buf == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                    "WssMacProcessReassocReq:-"
                    "UtlShMemAllocMacMsgStructBuf returned failure\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pReassocReqStruct);
        return WSSMAC_FAILURE;
    }
    MEMSET (pReassocReqStruct, 0, sizeof (tWssMacMsgStruct));
    MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));
    UNUSED_PARAM (eMsgType);

    pBuf = pMsgStruct->unMacMsg.MacDot11PktBuf.pDot11MacPdu;

    u2PacketLen = (UINT2) WSSMAC_GET_BUF_LEN (pBuf);
    pu1ReadPtr = WSSMAC_BUF_IF_LINEAR (pBuf, 0, u2PacketLen);

    if (pu1ReadPtr == NULL)
    {
        MEMSET (pu1Buf, 0x00, u2PacketLen);
        WSSMAC_COPY_FROM_BUF (pBuf, pu1Buf, 0, u2PacketLen);
        pu1ReadPtr = pu1Buf;
    }
    /* Discard Reassoc Request if PDU size exceeds maximum allowed length */
    if (u2PacketLen > WSSMAC_MAX_PDU_LEN)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                    "WssMacProcessReassocReq:-"
                    "PDU size exceeds maximum allowed length, Discarding Reassoc Request\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pReassocReqStruct);
        WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pu1Buf);
        return WSSMAC_FAILURE;
    }

    /* Get the 802.11 MAC Header */
    WSSMAC_GET_2BYTES (pReassocReqStruct->unMacMsg.ReassocReqMacFrame.MacMgmtFrmHdr.u2MacFrameCtrl, pu1ReadPtr);    /* Get Frame control field */
    WSSMAC_GET_2BYTES (pReassocReqStruct->unMacMsg.ReassocReqMacFrame.MacMgmtFrmHdr.u2MacFrameDurId, pu1ReadPtr);    /* Get Duration ID field */
    WSSMAC_GET_NBYTES (pReassocReqStruct->unMacMsg.ReassocReqMacFrame.MacMgmtFrmHdr.u1DA, pu1ReadPtr, WSSMAC_MAC_ADDR_LEN);    /* Get Address1 field */
    WSSMAC_GET_NBYTES (pReassocReqStruct->unMacMsg.ReassocReqMacFrame.MacMgmtFrmHdr.u1SA, pu1ReadPtr, WSSMAC_MAC_ADDR_LEN);    /* Get Address2 field */
    WSSMAC_GET_NBYTES (pReassocReqStruct->unMacMsg.ReassocReqMacFrame.MacMgmtFrmHdr.u1BssId, pu1ReadPtr, WSSMAC_MAC_ADDR_LEN);    /* Get Address3 field */
    WSSMAC_GET_2BYTES (pReassocReqStruct->unMacMsg.ReassocReqMacFrame.MacMgmtFrmHdr.u2MacFrameSeqCtrl, pu1ReadPtr);    /* Get Sequence control field */

    /* Get Capability info */
    WSSMAC_GET_2BYTES (pReassocReqStruct->unMacMsg.ReassocReqMacFrame.
                       Capability.u2MacFrameCapability, pu1ReadPtr);

    /* Get Listen Interval */
    WSSMAC_GET_2BYTES (pReassocReqStruct->unMacMsg.ReassocReqMacFrame.ListenInt.
                       u2MacFrameListnInt, pu1ReadPtr);

    /* Get Current AP MAC Address */
    WSSMAC_GET_NBYTES (pReassocReqStruct->unMacMsg.ReassocReqMacFrame.
                       CurrAPaddr.au1MacFrameCurrAPAddr, pu1ReadPtr,
                       WSSMAC_MAC_ADDR_LEN);

    u2PacketLen = (UINT2) (u2PacketLen - DOT11_REG_HDR_SIZE
                           - WSSMAC_CAPABILITY_INFO_LEN -
                           WSSMAC_LISTEN_INTERVAL_LEN - WSSMAC_MAC_ADDR_LEN);

    while (u2PacketLen > 0)
    {
        u1Tag = *pu1ReadPtr;
        switch (u1Tag)
        {
            case WSSMAC_SSID_ELEMENT_ID:
                /* Fill the SSID details */
                WSSMAC_GET_1BYTE (pReassocReqStruct->unMacMsg.
                                  ReassocReqMacFrame.Ssid.u1MacFrameElemId,
                                  pu1ReadPtr);
                WSSMAC_GET_1BYTE (pReassocReqStruct->unMacMsg.
                                  ReassocReqMacFrame.Ssid.u1MacFrameElemLen,
                                  pu1ReadPtr);
                u2Len =
                    pReassocReqStruct->unMacMsg.ReassocReqMacFrame.Ssid.
                    u1MacFrameElemLen;
                WSSMAC_GET_NBYTES (pReassocReqStruct->unMacMsg.
                                   ReassocReqMacFrame.Ssid.au1MacFrameSSID,
                                   pu1ReadPtr, u2Len);
                break;

            case WSSMAC_SUPP_RATE_ELEMENT_ID:
                /* Fill the Supported Rates details */
                WSSMAC_GET_1BYTE (pReassocReqStruct->unMacMsg.
                                  ReassocReqMacFrame.SuppRate.u1MacFrameElemId,
                                  pu1ReadPtr);
                WSSMAC_GET_1BYTE (pReassocReqStruct->unMacMsg.
                                  ReassocReqMacFrame.SuppRate.u1MacFrameElemLen,
                                  pu1ReadPtr);
                u2Len =
                    pReassocReqStruct->unMacMsg.ReassocReqMacFrame.SuppRate.
                    u1MacFrameElemLen;
                u2Count = 0;
                while (u2Count < u2Len)
                {
                    WSSMAC_GET_1BYTE (pReassocReqStruct->unMacMsg.
                                      ReassocReqMacFrame.SuppRate.
                                      au1MacFrameSuppRate[u2Count], pu1ReadPtr);
                    u2Count++;
                }
                break;

            case WSSMAC_POW_CAP_ELEMENT_ID:
                /* Fill Power capability info */
                WSSMAC_GET_1BYTE (pReassocReqStruct->unMacMsg.
                                  ReassocReqMacFrame.PowCapability.
                                  u1MacFrameElemId, pu1ReadPtr);
                WSSMAC_GET_1BYTE (pReassocReqStruct->unMacMsg.
                                  ReassocReqMacFrame.PowCapability.
                                  u1MacFrameElemLen, pu1ReadPtr);
                u2Len =
                    pReassocReqStruct->unMacMsg.ReassocReqMacFrame.
                    PowCapability.u1MacFrameElemLen;
                WSSMAC_GET_1BYTE (pReassocReqStruct->unMacMsg.
                                  ReassocReqMacFrame.PowCapability.
                                  u1MacFramePowCap_MinTxPow, pu1ReadPtr);
                WSSMAC_GET_1BYTE (pReassocReqStruct->unMacMsg.
                                  ReassocReqMacFrame.PowCapability.
                                  u1MacFramePowCap_MaxTxPow, pu1ReadPtr);
                break;

            case WSSMAC_SUPP_CH_ELEMENT_ID:
                /* Fill Supported channels info */
                WSSMAC_GET_1BYTE (pReassocReqStruct->unMacMsg.
                                  ReassocReqMacFrame.SuppChanls.
                                  u1MacFrameElemId, pu1ReadPtr);
                WSSMAC_GET_1BYTE (pReassocReqStruct->unMacMsg.
                                  ReassocReqMacFrame.SuppChanls.
                                  u1MacFrameElemLen, pu1ReadPtr);

                u2Count = 0;
                u2Len =
                    (pReassocReqStruct->unMacMsg.ReassocReqMacFrame.SuppChanls.
                     u1MacFrameElemLen);
                u2SupportedChannelSet =
                    (pReassocReqStruct->unMacMsg.ReassocReqMacFrame.SuppChanls.
                     u1MacFrameElemLen) / 2;
                while (u2Count < u2SupportedChannelSet)
                {
                    WSSMAC_GET_1BYTE (pReassocReqStruct->unMacMsg.
                                      ReassocReqMacFrame.SuppChanls.
                                      SubBandDes[u2Count].
                                      u1MacFrameSuppChanl_FirChanlNum,
                                      pu1ReadPtr);
                    WSSMAC_GET_1BYTE (pReassocReqStruct->unMacMsg.
                                      ReassocReqMacFrame.SuppChanls.
                                      SubBandDes[u2Count].
                                      u1MacFrameSuppChanl_NumOfChanl,
                                      pu1ReadPtr);
                    u2Count++;
                }
                break;

            case WSSMAC_EXT_SUPP_RATE_ELEMENTID:
                /* Fill the Extended Supported Rates details if Supported rates 
                 * Element ID > WSSMAC_MAX_SUPP_RATE_LEN  */
                WSSMAC_GET_1BYTE (pReassocReqStruct->unMacMsg.
                                  ReassocReqMacFrame.ExtSuppRates.
                                  u1MacFrameElemId, pu1ReadPtr);
                WSSMAC_GET_1BYTE (pReassocReqStruct->unMacMsg.
                                  ReassocReqMacFrame.ExtSuppRates.
                                  u1MacFrameElemLen, pu1ReadPtr);
                u2Count = 0;
                u2Len =
                    pReassocReqStruct->unMacMsg.ReassocReqMacFrame.ExtSuppRates.
                    u1MacFrameElemLen;
                while (u2Count < u2Len)
                {
                    WSSMAC_GET_1BYTE (pReassocReqStruct->unMacMsg.
                                      ReassocReqMacFrame.ExtSuppRates.
                                      au1MacFrameExtSuppRates[u2Count],
                                      pu1ReadPtr);
                    u2Count++;
                }
                break;

#ifdef RSNA_WANTED

            case WSSMAC_RSN_ELEMENT_ID:
                /* Get RSN info */
                WSSMAC_GET_1BYTE (pReassocReqStruct->unMacMsg.
                                  ReassocReqMacFrame.RSNInfo.u1MacFrameElemId,
                                  pu1ReadPtr);
                WSSMAC_GET_1BYTE (pReassocReqStruct->unMacMsg.
                                  ReassocReqMacFrame.RSNInfo.u1MacFrameElemLen,
                                  pu1ReadPtr);
                u2Len =
                    pReassocReqStruct->unMacMsg.ReassocReqMacFrame.RSNInfo.
                    u1MacFrameElemLen;

                WSSMAC_GET_2BYTES (pReassocReqStruct->unMacMsg.
                                   ReassocReqMacFrame.RSNInfo.
                                   u2MacFrameRSN_Version, pu1ReadPtr);
                u1RsnIELenParsed = (UINT1) (u1RsnIELenParsed + 2);
                pReassocReqStruct->unMacMsg.ReassocReqMacFrame.RSNInfo.
                    u2MacFrameRSN_Version =
                    OSIX_HTONS (pReassocReqStruct->unMacMsg.ReassocReqMacFrame.
                                RSNInfo.u2MacFrameRSN_Version);
                WSSMAC_GET_4BYTES (pReassocReqStruct->unMacMsg.
                                   ReassocReqMacFrame.RSNInfo.
                                   u4MacFrameRSN_GrpCipherSuit, pu1ReadPtr);
                u1RsnIELenParsed = (UINT1) (u1RsnIELenParsed + 4);
                pReassocReqStruct->unMacMsg.ReassocReqMacFrame.RSNInfo.
                    u4MacFrameRSN_GrpCipherSuit =
                    OSIX_HTONL (pReassocReqStruct->unMacMsg.ReassocReqMacFrame.
                                RSNInfo.u4MacFrameRSN_GrpCipherSuit);
                WSSMAC_GET_2BYTES (pReassocReqStruct->unMacMsg.
                                   ReassocReqMacFrame.RSNInfo.
                                   u2MacFrameRSN_PairCiphSuitCnt, pu1ReadPtr);

                u1RsnIELenParsed = (UINT1) (u1RsnIELenParsed + 2);
                pReassocReqStruct->unMacMsg.ReassocReqMacFrame.RSNInfo.
                    u2MacFrameRSN_PairCiphSuitCnt =
                    OSIX_HTONS (pReassocReqStruct->unMacMsg.ReassocReqMacFrame.
                                RSNInfo.u2MacFrameRSN_PairCiphSuitCnt);
                u2Count = 0;
                WSSMAC_GET_NBYTES (pReassocReqStruct->unMacMsg.
                                   ReassocReqMacFrame.RSNInfo.
                                   aMacFrameRSN_PairCiphSuitList[u2Count].
                                   au1MacFrameRSN_CiphSuit_OUI, pu1ReadPtr,
                                   WSSMAC_MAX_OUI_SIZE);
                WSSMAC_GET_1BYTE (pReassocReqStruct->unMacMsg.
                                  ReassocReqMacFrame.RSNInfo.
                                  aMacFrameRSN_PairCiphSuitList[u2Count].
                                  u1MacFrameRSN_CiphSuit_Type, pu1ReadPtr);
                u2Count++;
                u1RsnIELenParsed = (UINT1) (u1RsnIELenParsed + 4);
                WSSMAC_GET_2BYTES (pReassocReqStruct->unMacMsg.
                                   ReassocReqMacFrame.RSNInfo.
                                   u2MacFrameRSN_AKMSuitCnt, pu1ReadPtr);
                u1RsnIELenParsed = (UINT1) (u1RsnIELenParsed + 2);
                pReassocReqStruct->unMacMsg.ReassocReqMacFrame.RSNInfo.
                    u2MacFrameRSN_AKMSuitCnt =
                    OSIX_HTONS (pReassocReqStruct->unMacMsg.ReassocReqMacFrame.
                                RSNInfo.u2MacFrameRSN_AKMSuitCnt);

                u2Count = 0;
                WSSMAC_GET_NBYTES (pReassocReqStruct->unMacMsg.
                                   ReassocReqMacFrame.RSNInfo.
                                   aMacFrameRSN_AKMSuitList[u2Count].
                                   au1MacFrameRSN_AkmSuit_OUI, pu1ReadPtr,
                                   WSSMAC_MAX_OUI_SIZE);
                WSSMAC_GET_1BYTE (pReassocReqStruct->unMacMsg.
                                  ReassocReqMacFrame.RSNInfo.
                                  aMacFrameRSN_AKMSuitList[u2Count].
                                  u1MacFrameRSN_AkmSuit_Type, pu1ReadPtr);

                u1RsnIELenParsed = (UINT1) (u1RsnIELenParsed + 4);

                WSSMAC_GET_2BYTES (pReassocReqStruct->unMacMsg.
                                   ReassocReqMacFrame.RSNInfo.
                                   u2MacFrameRSN_Capability, pu1ReadPtr);
                u1RsnIELenParsed = (UINT1) (u1RsnIELenParsed + 2);
                pReassocReqStruct->unMacMsg.ReassocReqMacFrame.RSNInfo.
                    u2MacFrameRSN_Capability =
                    OSIX_HTONS (pReassocReqStruct->unMacMsg.ReassocReqMacFrame.
                                RSNInfo.u2MacFrameRSN_Capability);
                if (u1RsnIELenParsed < u2Len)
                {
                    WSSMAC_GET_2BYTES (pReassocReqStruct->unMacMsg.
                                       ReassocReqMacFrame.RSNInfo.
                                       u2MacFrameRSN_PMKIdCnt, pu1ReadPtr);
                    u1RsnIELenParsed = (UINT1) (u1RsnIELenParsed + 2);
                    pReassocReqStruct->unMacMsg.ReassocReqMacFrame.RSNInfo.
                        u2MacFrameRSN_PMKIdCnt =
                        OSIX_HTONS (pReassocReqStruct->unMacMsg.
                                    ReassocReqMacFrame.RSNInfo.
                                    u2MacFrameRSN_PMKIdCnt);

                    u2PmkidCount =
                        pReassocReqStruct->unMacMsg.ReassocReqMacFrame.RSNInfo.
                        u2MacFrameRSN_PMKIdCnt;

                    if (u2PmkidCount == WSSMAC_MAX_PKMID_CNT)
                    {
                        WSSMAC_GET_NBYTES (pReassocReqStruct->unMacMsg.
                                           ReassocReqMacFrame.RSNInfo.
                                           au1MacFrameRSN_PMKIdList, pu1ReadPtr,
                                           (size_t) (16 * (u2PmkidCount)));
                        u1RsnIELenParsed = (UINT1) (u1RsnIELenParsed + 16);
                    }
                }
                break;
#endif

            case WSSMAC_QOS_CAP_ELEMENT_ID:
                /* Fill Qos capability */
                WSSMAC_GET_1BYTE (pReassocReqStruct->unMacMsg.
                                  ReassocReqMacFrame.QosCap.u1MacFrameElemId,
                                  pu1ReadPtr);
                WSSMAC_GET_1BYTE (pReassocReqStruct->unMacMsg.
                                  ReassocReqMacFrame.QosCap.u1MacFrameElemLen,
                                  pu1ReadPtr);
                u2Len =
                    pReassocReqStruct->unMacMsg.ReassocReqMacFrame.QosCap.
                    u1MacFrameElemLen;
                WSSMAC_GET_1BYTE (pReassocReqStruct->unMacMsg.
                                  ReassocReqMacFrame.QosCap.u1MacFrameQosInfo,
                                  pu1ReadPtr);
                break;

            case WSSMAC_HT_CAPAB_ELEMENT_ID:
                /* Fill the HT capabilities of Element ID == WSSMAC_HT_CAPAB_ELEMENT_ID */
                WSSMAC_GET_1BYTE (pReassocReqStruct->unMacMsg.
                                  ReassocReqMacFrame.HTCapabilities.
                                  u1MacFrameElemId, pu1ReadPtr);
                WSSMAC_GET_1BYTE (pReassocReqStruct->unMacMsg.
                                  ReassocReqMacFrame.HTCapabilities.
                                  u1MacFrameElemLen, pu1ReadPtr);
                u2Len =
                    pReassocReqStruct->unMacMsg.ReassocReqMacFrame.
                    HTCapabilities.u1MacFrameElemLen;
                WSSMAC_GET_2BYTES (pReassocReqStruct->unMacMsg.
                                   ReassocReqMacFrame.HTCapabilities.
                                   u2HTCapInfo, pu1ReadPtr);
                WSSMAC_GET_1BYTE (pReassocReqStruct->unMacMsg.
                                  ReassocReqMacFrame.HTCapabilities.
                                  u1AMPDUParam, pu1ReadPtr);
                WSSMAC_GET_NBYTES (pReassocReqStruct->unMacMsg.
                                   ReassocReqMacFrame.HTCapabilities.
                                   au1SuppMCSSet, pu1ReadPtr,
                                   WSSMAC_SUPP_MCS_SET);
                WSSMAC_GET_2BYTES (pReassocReqStruct->unMacMsg.
                                   ReassocReqMacFrame.HTCapabilities.u2HTExtCap,
                                   pu1ReadPtr);
                WSSMAC_GET_4BYTES (pReassocReqStruct->unMacMsg.
                                   ReassocReqMacFrame.HTCapabilities.
                                   u4TranBeamformCap, pu1ReadPtr);
                WSSMAC_GET_1BYTE (pReassocReqStruct->unMacMsg.
                                  ReassocReqMacFrame.HTCapabilities.u1ASELCap,
                                  pu1ReadPtr);
                break;
            case WSSMAC_VHT_CAPAB_ELEMENT_ID:
                WSSMAC_GET_1BYTE (pReassocReqStruct->unMacMsg.
                                  ReassocReqMacFrame.VHTCapabilities.u1ElemId,
                                  pu1ReadPtr);
                WSSMAC_GET_1BYTE (pReassocReqStruct->unMacMsg.
                                  ReassocReqMacFrame.VHTCapabilities.u1ElemLen,
                                  pu1ReadPtr);
                u2Len =
                    pReassocReqStruct->unMacMsg.ReassocReqMacFrame.
                    VHTCapabilities.u1ElemLen;
                WSSMAC_GET_4BYTES (pReassocReqStruct->unMacMsg.
                                   ReassocReqMacFrame.VHTCapabilities.
                                   u4VhtCapaInfo, pu1ReadPtr);
                WSSMAC_GET_NBYTES (pReassocReqStruct->unMacMsg.
                                   ReassocReqMacFrame.VHTCapabilities.
                                   au1SuppMCS, pu1ReadPtr, 8);
                break;
            case WSSMAC_BSS_COEXIST_ELEMENT_ID:
                /* Fill the HT 20/40 BSS Coexistence if Element ID == */
                WSSMAC_GET_1BYTE (pReassocReqStruct->unMacMsg.
                                  ReassocReqMacFrame.BssCoexistence.
                                  u1MacFrameElemId, pu1ReadPtr);
                WSSMAC_GET_1BYTE (pReassocReqStruct->unMacMsg.
                                  ReassocReqMacFrame.BssCoexistence.
                                  u1MacFrameElemLen, pu1ReadPtr);
                u2Len =
                    pReassocReqStruct->unMacMsg.ReassocReqMacFrame.
                    BssCoexistence.u1MacFrameElemLen;
                WSSMAC_GET_1BYTE (pReassocReqStruct->unMacMsg.
                                  ReassocReqMacFrame.BssCoexistence.
                                  u1BSSCoexistInfoFields, pu1ReadPtr);
                break;

            case WSSMAC_EXT_CAPAB_ELEMENT_ID:
                /* Fill the Extended Capabilities if Element ID == */
                WSSMAC_GET_1BYTE (pReassocReqStruct->unMacMsg.
                                  ReassocReqMacFrame.ExtCapabilities.
                                  u1MacFrameElemId, pu1ReadPtr);
                WSSMAC_GET_1BYTE (pReassocReqStruct->unMacMsg.
                                  ReassocReqMacFrame.ExtCapabilities.
                                  u1MacFrameElemLen, pu1ReadPtr);
                u2Len =
                    pReassocReqStruct->unMacMsg.ReassocReqMacFrame.
                    ExtCapabilities.u1MacFrameElemLen;
                WSSMAC_GET_NBYTES (pReassocReqStruct->unMacMsg.
                                   ReassocReqMacFrame.ExtCapabilities.
                                   au1Capabilities, pu1ReadPtr, u2Len);
                break;
            case WSSMAC_VENDOR_INFO_ELEMENTID:
                /* Fill the Vendor info if Element ID ==  WSSMAC_VENDOR_INFO_ELEMENTID */
                WSSMAC_GET_1BYTE (pReassocReqStruct->unMacMsg.
                                  ReassocReqMacFrame.WMMParam.u1MacFrameElemId,
                                  pu1ReadPtr);
                WSSMAC_GET_1BYTE (pReassocReqStruct->unMacMsg.
                                  ReassocReqMacFrame.WMMParam.u1MacFrameElemLen,
                                  pu1ReadPtr);
                MEMCPY (&u4Oui, pu1ReadPtr, WSSMAC_MAX_OUI_SIZE);
                u4WmmOui = WSSMAC_WMM_OUI;
                u2Len = pReassocReqStruct->unMacMsg.ReassocReqMacFrame.WMMParam.
                    u1MacFrameElemLen;
#ifdef WPA_WANTED
                wpaOui = WSSMAC_WPA_OUI;
                MEMCPY (&WpaTmpOui, pu1ReadPtr, 4);
                if (MEMCMP (&WpaTmpOui, &wpaOui, 4) == 0)
                {
                    pReassocReqStruct->unMacMsg.ReassocReqMacFrame.WpaInfo.
                        elemId =
                        pReassocReqStruct->unMacMsg.ReassocReqMacFrame.WMMParam.
                        u1MacFrameElemId;
                    pReassocReqStruct->unMacMsg.ReassocReqMacFrame.WpaInfo.Len =
                        pReassocReqStruct->unMacMsg.ReassocReqMacFrame.WMMParam.
                        u1MacFrameElemLen;
                    WSSMAC_GET_NBYTES (pReassocReqStruct->unMacMsg.
                                       ReassocReqMacFrame.WpaInfo.Oui,
                                       pu1ReadPtr, 4);
                    WSSMAC_GET_2BYTES (pReassocReqStruct->unMacMsg.
                                       ReassocReqMacFrame.WpaInfo.u2WpaVersion,
                                       pu1ReadPtr);
                    pReassocReqStruct->unMacMsg.ReassocReqMacFrame.WpaInfo.
                        u2WpaVersion =
                        OSIX_HTONS (pReassocReqStruct->unMacMsg.
                                    ReassocReqMacFrame.WpaInfo.u2WpaVersion);
                    WSSMAC_GET_NBYTES (pReassocReqStruct->unMacMsg.
                                       ReassocReqMacFrame.WpaInfo.
                                       au1MultiCastCipherSuite, pu1ReadPtr, 4);
                    WSSMAC_GET_2BYTES (pReassocReqStruct->unMacMsg.
                                       ReassocReqMacFrame.WpaInfo.
                                       u2UniCastCipherCount, pu1ReadPtr);
                    pReassocReqStruct->unMacMsg.ReassocReqMacFrame.WpaInfo.
                        u2UniCastCipherCount =
                        OSIX_HTONS (pReassocReqStruct->unMacMsg.
                                    ReassocReqMacFrame.WpaInfo.
                                    u2UniCastCipherCount);

                    WSSMAC_GET_NBYTES (pReassocReqStruct->unMacMsg.
                                       ReassocReqMacFrame.WpaInfo.
                                       au1UniCastCipherSuite, pu1ReadPtr, 4);
                    WSSMAC_GET_2BYTES (pReassocReqStruct->unMacMsg.
                                       ReassocReqMacFrame.WpaInfo.
                                       u2ManagementKeyCount, pu1ReadPtr);
                    pReassocReqStruct->unMacMsg.ReassocReqMacFrame.WpaInfo.
                        u2ManagementKeyCount =
                        OSIX_HTONS (pReassocReqStruct->unMacMsg.
                                    ReassocReqMacFrame.WpaInfo.
                                    u2ManagementKeyCount);
                    WSSMAC_GET_NBYTES (pReassocReqStruct->unMacMsg.
                                       ReassocReqMacFrame.WpaInfo.
                                       au1ManagementCastCipherSuite, pu1ReadPtr,
                                       4);
                    WSSMAC_GET_2BYTES (pReassocReqStruct->unMacMsg.
                                       ReassocReqMacFrame.WpaInfo.
                                       u2MacFrameRSN_Capability, pu1ReadPtr);
                    pReassocReqStruct->unMacMsg.ReassocReqMacFrame.WpaInfo.
                        u2MacFrameRSN_Capability =
                        OSIX_HTONS (pReassocReqStruct->unMacMsg.
                                    ReassocReqMacFrame.WpaInfo.
                                    u2MacFrameRSN_Capability);
                    u2Len =
                        pReassocReqStruct->unMacMsg.ReassocReqMacFrame.WpaInfo.
                        Len;
                }
#endif

                if (((pReassocReqStruct->unMacMsg.ReassocReqMacFrame.WMMParam.
                      u1MacFrameElemLen) != WSSSTA_WMM_ASSOC_REQ_TAG_LENGTH)
                    && ((MEMCMP (&u4Oui, &u4WmmOui, sizeof (u4Oui)))))
                {
                    WSSMAC_SKIP_NBYTES (u2Len, pu1ReadPtr);
                    /* Skipping the unknown packet content    
                       until the Message Length */
                    break;
                }
                WSSMAC_GET_NBYTES (pReassocReqStruct->unMacMsg.
                                   ReassocReqMacFrame.WMMParam.au1OUI,
                                   pu1ReadPtr, WSSMAC_MAX_OUI_SIZE);
                WSSMAC_GET_1BYTE (pReassocReqStruct->unMacMsg.
                                  ReassocReqMacFrame.WMMParam.u1OUIType,
                                  pu1ReadPtr);
                WSSMAC_GET_1BYTE (pReassocReqStruct->unMacMsg.
                                  ReassocReqMacFrame.WMMParam.u1OUISubType,
                                  pu1ReadPtr);
                WSSMAC_GET_1BYTE (pReassocReqStruct->unMacMsg.
                                  ReassocReqMacFrame.WMMParam.u1WMMVersion,
                                  pu1ReadPtr);
                WSSMAC_GET_1BYTE (pReassocReqStruct->unMacMsg.
                                  ReassocReqMacFrame.WMMParam.u1QOSInfo,
                                  pu1ReadPtr);
                u2Len =
                    pReassocReqStruct->unMacMsg.ReassocReqMacFrame.WMMParam.
                    u1MacFrameElemLen;

                break;
            case WSSMAC_VHT_OPERATING_MODE_NOTIFICATION:
                WSSMAC_GET_1BYTE (pReassocReqStruct->unMacMsg.
                                  ReassocReqMacFrame.OperModeNotify.u1ElemId,
                                  pu1ReadPtr);
                WSSMAC_GET_1BYTE (pReassocReqStruct->unMacMsg.
                                  ReassocReqMacFrame.OperModeNotify.u1ElemLen,
                                  pu1ReadPtr);
                u2Len =
                    pReassocReqStruct->unMacMsg.ReassocReqMacFrame.
                    OperModeNotify.u1ElemLen;
                WSSMAC_GET_1BYTE (pReassocReqStruct->unMacMsg.
                                  ReassocReqMacFrame.OperModeNotify.
                                  u1VhtOperModeElem, pu1ReadPtr);
                break;
            default:
                WSSMAC_TRC1 (WSSMAC_FAILURE_TRC,
                             "WssMacProcessReassocReq: Unknown"
                             "802.11 Tag %d\r\n", u1Tag);
                WSSMAC_SKIP_NBYTES (1, pu1ReadPtr);    /*Skipping the Type */
                WSSMAC_GET_1BYTE (u1UnknownTagMsgLen, pu1ReadPtr);
                u2Len = u1UnknownTagMsgLen;
                WSSMAC_SKIP_NBYTES (u2Len, pu1ReadPtr);    /* Skipping the packet content
                                                           until the Message Length */
                break;
        }
        u2PacketLen = (UINT2) (u2PacketLen - u2Len - 2);
    }
    /* Fill the session id */
    pReassocReqStruct->unMacMsg.ReassocReqMacFrame.u2SessId =
        pMsgStruct->unMacMsg.MacDot11PktBuf.u2SessId;

    MEMCPY (&(pWlcHdlrMsgStruct->WssMacMsgStruct.unMacMsg.ReassocReqMacFrame),
            &(pReassocReqStruct->unMacMsg.ReassocReqMacFrame),
            sizeof (tDot11ReassocReqMacFrame));

    if (WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_STA_REASSOC_REQ, pWlcHdlrMsgStruct)
        != WSSMAC_SUCCESS)
    {
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pReassocReqStruct);
        WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pu1Buf);
        return WSSMAC_FAILURE;
    }
    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
    UtlShMemFreeMacMsgStructBuf ((UINT1 *) pReassocReqStruct);
    WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pu1Buf);
    return WSSMAC_SUCCESS;
}

/* ***************************************************************************
 *                                                                           *
 * Function     : WssMacProcessReassocRsp                                    *
 *                                                                           *
 * Description  : Process Reassociation response frame                       *
 *                                                                           *
 * Input(s)     : MAC message type and union of struct with Reassociation    *
 *                response structure                                         *
 *                                                                           *
 * Output(s)    : Reassociation response message buffer                      *
 *                                                                           *
 * Returns      : SUCCESS/FAILURE                                            *
 *                                                                           *
 * ***************************************************************************/
UINT1
WssMacProcessReassocRsp (UINT1 eMsgType, tWssMacMsgStruct * pReassocRspStruct)
{
    tWssMacMsgStruct   *pReassocRspBuf = NULL;
    unWlcHdlrMsgStruct *pCapwapMsgStruct = NULL;

    pCapwapMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pCapwapMsgStruct == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                    "WssMacProcessReassocRsp:- "
                    "UtlShMemAllocWlcBuf returned failure\n");
        return WSSMAC_FAILURE;
    }

    pReassocRspBuf =
        (tWssMacMsgStruct *) (VOID *) UtlShMemAllocMacMsgStructBuf ();
    if (pReassocRspBuf == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                    "WssMacProcessReassocRsp:-"
                    "UtlShMemAllocMacMsgStructBuf returned failure\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pCapwapMsgStruct);
        return OSIX_FAILURE;
    }
    MEMSET (pReassocRspBuf, 0, sizeof (tWssMacMsgStruct));
    MEMSET (pCapwapMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));
    UNUSED_PARAM (eMsgType);

    pReassocRspStruct->unMacMsg.ReassocRspMacFrame.MacMgmtFrmHdr.
        u2MacFrameCtrl &= WSSMAC_SET_REASSOCRSP_FRAME_CTRL1;
    pReassocRspStruct->unMacMsg.ReassocRspMacFrame.MacMgmtFrmHdr.
        u2MacFrameCtrl |= WSSMAC_SET_REASSOCRSP_FRAME_CTRL2;

    WssMacAssembleReassocRspFrame (pReassocRspStruct, pReassocRspBuf);

    if (pReassocRspBuf->unMacMsg.MacDot11PktBuf.pDot11MacPdu != NULL)
    {
        pCapwapMsgStruct->WlcHdlrQueueReq.pRcvBuf =
            pReassocRspBuf->unMacMsg.MacDot11PktBuf.pDot11MacPdu;
        pCapwapMsgStruct->WlcHdlrQueueReq.u2SessId =
            pReassocRspStruct->unMacMsg.ReassocRspMacFrame.u2SessId;

        pCapwapMsgStruct->WlcHdlrQueueReq.u4MsgType =
            WSS_CAPWAP_802_11_MGMT_PKT;

        if (WssIfProcessWlcHdlrMsg
            (WSS_WLCHDLR_CAPWAP_MAC_MSG, pCapwapMsgStruct) != WSSMAC_SUCCESS)
        {
            UtlShMemFreeWlcBuf ((UINT1 *) pCapwapMsgStruct);
            UtlShMemFreeMacMsgStructBuf ((UINT1 *) pReassocRspBuf);
            return WSSMAC_FAILURE;
        }
    }
    else
    {
        UtlShMemFreeWlcBuf ((UINT1 *) pCapwapMsgStruct);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pReassocRspBuf);
        return WSSMAC_FAILURE;
    }
    UtlShMemFreeWlcBuf ((UINT1 *) pCapwapMsgStruct);
    UtlShMemFreeMacMsgStructBuf ((UINT1 *) pReassocRspBuf);
    return WSSMAC_SUCCESS;
}

/* ***************************************************************************
 *                                                                           *
 * Function     : WssMacAssembleReassocRspFrame                              *
 *                                                                           *
 * Description  : Assemble Reassociation response structure                  *
 *                                                                           *
 * Input(s)     : Union of struct with Reassociation response structure and  *
 *                char buffer                                                *
 *                                                                           *
 * Output(s)    : Reassociation response message buffer                      *
 *                                                                           *
 * Returns      : NONE                                                       *
 *                                                                           *
 * ***************************************************************************/
VOID
WssMacAssembleReassocRspFrame (tWssMacMsgStruct * pReassocRspStruct,
                               tWssMacMsgStruct * pReassocRspBuf)
{
    UINT1              *pu1ReadPtr = NULL;
    UINT4               u4Len = 0;
    UINT2               u2Count = 0, u2Count1 = 0;
    UINT1               u1ElementId = 0;
    UINT2               u2Len = 0;
    UINT1              *pu1Buf = NULL;

    WLCHDLR_PKTBUF_ALLOC_MEM_BLOCK (pu1Buf);
    if (pu1Buf == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                    "WssMacAssembleReassocRspFrame:-"
                    "UtlShMemAllocMacMsgStructBuf returned failure\n");
        return;
    }
    pu1ReadPtr = pu1Buf;
    /* Put the 802.11 MAC Header */
    WSSMAC_PUT_2BYTES (pu1ReadPtr, pReassocRspStruct->unMacMsg.ReassocRspMacFrame.MacMgmtFrmHdr.u2MacFrameCtrl);    /* Put Frame control field */
    WSSMAC_PUT_2BYTES (pu1ReadPtr, pReassocRspStruct->unMacMsg.ReassocRspMacFrame.MacMgmtFrmHdr.u2MacFrameDurId);    /* Put Duration ID field */
    WSSMAC_PUT_NBYTES (pu1ReadPtr, pReassocRspStruct->unMacMsg.ReassocRspMacFrame.MacMgmtFrmHdr.u1DA, WSSMAC_MAC_ADDR_LEN);    /* Put Address1 field */
    WSSMAC_PUT_NBYTES (pu1ReadPtr, pReassocRspStruct->unMacMsg.ReassocRspMacFrame.MacMgmtFrmHdr.u1SA, WSSMAC_MAC_ADDR_LEN);    /* Put Address2 field */
    WSSMAC_PUT_NBYTES (pu1ReadPtr, pReassocRspStruct->unMacMsg.ReassocRspMacFrame.MacMgmtFrmHdr.u1BssId, WSSMAC_MAC_ADDR_LEN);    /* Put Address3 field */
    WSSMAC_PUT_2BYTES (pu1ReadPtr, pReassocRspStruct->unMacMsg.ReassocRspMacFrame.MacMgmtFrmHdr.u2MacFrameSeqCtrl);    /* Put Sequence control field */

    /* Get Capability info */
    WSSMAC_PUT_2BYTES (pu1ReadPtr,
                       pReassocRspStruct->unMacMsg.ReassocRspMacFrame.
                       Capability.u2MacFrameCapability);

    /* Get Status code */
    WSSMAC_PUT_2BYTES (pu1ReadPtr,
                       pReassocRspStruct->unMacMsg.ReassocRspMacFrame.StatCode.
                       u2MacFrameStatusCode);

    /* Get Association IiD */
    WSSMAC_PUT_2BYTES (pu1ReadPtr,
                       pReassocRspStruct->unMacMsg.ReassocRspMacFrame.Aid.
                       u2MacFrameAID);

    /* Put Supported rates */
    u1ElementId = pReassocRspStruct->unMacMsg.ReassocRspMacFrame.SuppRate.
        u1MacFrameElemId;
    if (u1ElementId == WSSMAC_SUPP_RATE_ELEMENT_ID)
    {
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pReassocRspStruct->unMacMsg.
                          ReassocRspMacFrame.SuppRate.u1MacFrameElemId);
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pReassocRspStruct->unMacMsg.
                          ReassocRspMacFrame.SuppRate.u1MacFrameElemLen);
        u2Len = pReassocRspStruct->unMacMsg.ReassocRspMacFrame.SuppRate.
            u1MacFrameElemLen;
        u2Count = 0;
        while (u2Count < u2Len)
        {
            WSSMAC_PUT_1BYTE (pu1ReadPtr, pReassocRspStruct->unMacMsg.
                              ReassocRspMacFrame.SuppRate.
                              au1MacFrameSuppRate[u2Count]);
            u2Count++;
        }
    }

    /* Get the Extended Supported Rates details */
    u1ElementId = pReassocRspStruct->unMacMsg.ReassocRspMacFrame.ExtSuppRates.
        u1MacFrameElemId;
    if (u1ElementId == WSSMAC_EXT_SUPP_RATE_ELEMENTID)
    {
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pReassocRspStruct->unMacMsg.
                          ReassocRspMacFrame.ExtSuppRates.u1MacFrameElemId);
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pReassocRspStruct->unMacMsg.
                          ReassocRspMacFrame.ExtSuppRates.u1MacFrameElemLen);
        u2Count = 0;
        u2Len = pReassocRspStruct->unMacMsg.ReassocRspMacFrame.ExtSuppRates.
            u1MacFrameElemLen;
        while (u2Count < u2Len)
        {
            WSSMAC_PUT_1BYTE (pu1ReadPtr,
                              pReassocRspStruct->unMacMsg.ReassocRspMacFrame.
                              ExtSuppRates.au1MacFrameExtSuppRates[u2Count]);
            u2Count++;
        }
    }

    /* Put HT capabilities */
    u1ElementId = pReassocRspStruct->unMacMsg.ReassocRspMacFrame.HTCapabilities.
        u1MacFrameElemId;
    if (u1ElementId == WSSMAC_HT_CAPAB_ELEMENT_ID)
    {
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pReassocRspStruct->unMacMsg.
                          ReassocRspMacFrame.HTCapabilities.u1MacFrameElemId);
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pReassocRspStruct->unMacMsg.
                          ReassocRspMacFrame.HTCapabilities.u1MacFrameElemLen);
        WSSMAC_PUT_2BYTES (pu1ReadPtr, pReassocRspStruct->unMacMsg.
                           ReassocRspMacFrame.HTCapabilities.u2HTCapInfo);
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pReassocRspStruct->unMacMsg.
                          ReassocRspMacFrame.HTCapabilities.u1AMPDUParam);
        WSSMAC_PUT_NBYTES (pu1ReadPtr, pReassocRspStruct->unMacMsg.
                           ReassocRspMacFrame.HTCapabilities.au1SuppMCSSet,
                           WSSMAC_SUPP_MCS_SET);
        WSSMAC_PUT_2BYTES (pu1ReadPtr, pReassocRspStruct->unMacMsg.
                           ReassocRspMacFrame.HTCapabilities.u2HTExtCap);
        WSSMAC_PUT_4BYTES (pu1ReadPtr, pReassocRspStruct->unMacMsg.
                           ReassocRspMacFrame.HTCapabilities.u4TranBeamformCap);
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pReassocRspStruct->unMacMsg.
                          ReassocRspMacFrame.HTCapabilities.u1ASELCap);
    }

    /* Put HT operation */
    u1ElementId = pReassocRspStruct->unMacMsg.ReassocRspMacFrame.HTOperation.
        u1MacFrameElemId;
    if (u1ElementId == WSSMAC_HT_OPE_ELEMENT_ID)
    {
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pReassocRspStruct->unMacMsg.
                          ReassocRspMacFrame.HTOperation.u1MacFrameElemId);
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pReassocRspStruct->unMacMsg.
                          ReassocRspMacFrame.HTOperation.u1MacFrameElemLen);
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pReassocRspStruct->unMacMsg.
                          ReassocRspMacFrame.HTOperation.u1PrimaryCh);
        WSSMAC_PUT_NBYTES (pu1ReadPtr, pReassocRspStruct->unMacMsg.
                           ReassocRspMacFrame.HTOperation.au1HTOpeInfo,
                           WSSMAC_HTOPE_INFO);
        WSSMAC_PUT_NBYTES (pu1ReadPtr,
                           pReassocRspStruct->unMacMsg.ReassocRspMacFrame.
                           HTOperation.au1BasicMCSSet, WSSMAC_BASIC_MCS_SET);
    }

/* Put VHT capabilities */
    u1ElementId = pReassocRspStruct->unMacMsg.ReassocRspMacFrame.
        VHTCapabilities.u1ElemId;
    if (u1ElementId == WSSMAC_VHT_CAPAB_ELEMENT_ID)
    {
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          pReassocRspStruct->unMacMsg.ReassocRspMacFrame.
                          VHTCapabilities.u1ElemId);
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          pReassocRspStruct->unMacMsg.ReassocRspMacFrame.
                          VHTCapabilities.u1ElemLen);
        WSSMAC_PUT_4BYTES (pu1ReadPtr,
                           pReassocRspStruct->unMacMsg.ReassocRspMacFrame.
                           VHTCapabilities.u4VhtCapaInfo);
        WSSMAC_PUT_NBYTES (pu1ReadPtr,
                           pReassocRspStruct->unMacMsg.ReassocRspMacFrame.
                           VHTCapabilities.au1SuppMCS, DOT11AC_VHT_CAP_MCS_LEN);
    }

    /* Put VHT operation */
    u1ElementId = pReassocRspStruct->unMacMsg.ReassocRspMacFrame.VHTOperation.
        u1ElemId;
    if (u1ElementId == WSSMAC_VHT_OPE_ELEMENT_ID)
    {
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          pReassocRspStruct->unMacMsg.ReassocRspMacFrame.
                          VHTOperation.u1ElemId);
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          pReassocRspStruct->unMacMsg.ReassocRspMacFrame.
                          VHTOperation.u1ElemLen);
        WSSMAC_PUT_NBYTES (pu1ReadPtr,
                           pReassocRspStruct->unMacMsg.ReassocRspMacFrame.
                           VHTOperation.au1VhtOperInfo,
                           DOT11AC_VHT_OPER_INFO_LEN);
        WSSMAC_PUT_2BYTES (pu1ReadPtr,
                           pReassocRspStruct->unMacMsg.ReassocRspMacFrame.
                           VHTOperation.u2BasicMCS);
    }

    /* Put HT 20/40 BSS Coexistence */
    u1ElementId = pReassocRspStruct->unMacMsg.ReassocRspMacFrame.
        BssCoexistence.u1MacFrameElemId;
    if (u1ElementId == WSSMAC_BSS_COEXIST_ELEMENT_ID)
    {
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pReassocRspStruct->unMacMsg.
                          ReassocRspMacFrame.BssCoexistence.u1MacFrameElemId);
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pReassocRspStruct->unMacMsg.
                          ReassocRspMacFrame.BssCoexistence.u1MacFrameElemLen);
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pReassocRspStruct->unMacMsg.
                          ReassocRspMacFrame.BssCoexistence.
                          u1BSSCoexistInfoFields);
    }

    /* Put OBSS Scan parameters */
    u1ElementId = pReassocRspStruct->unMacMsg.ReassocRspMacFrame.OBSSScanParams.
        u1MacFrameElemId;
    if (u1ElementId == WSSMAC_OBSS_SCAN_PARAM_ELEMENT_ID)
    {
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pReassocRspStruct->unMacMsg.
                          ReassocRspMacFrame.OBSSScanParams.u1MacFrameElemId);
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pReassocRspStruct->unMacMsg.
                          ReassocRspMacFrame.OBSSScanParams.u1MacFrameElemLen);
        WSSMAC_PUT_2BYTES (pu1ReadPtr, pReassocRspStruct->unMacMsg.
                           ReassocRspMacFrame.OBSSScanParams.
                           u2OBSSScanPassiveDwell);
        WSSMAC_PUT_2BYTES (pu1ReadPtr,
                           pReassocRspStruct->unMacMsg.ReassocRspMacFrame.
                           OBSSScanParams.u2OBSSScanActiveDwell);
        WSSMAC_PUT_2BYTES (pu1ReadPtr,
                           pReassocRspStruct->unMacMsg.ReassocRspMacFrame.
                           OBSSScanParams.u2BSSChaWidTriScanInt);
        WSSMAC_PUT_2BYTES (pu1ReadPtr,
                           pReassocRspStruct->unMacMsg.ReassocRspMacFrame.
                           OBSSScanParams.u2OBSSScanPassiveTotalPerCh);
        WSSMAC_PUT_2BYTES (pu1ReadPtr,
                           pReassocRspStruct->unMacMsg.ReassocRspMacFrame.
                           OBSSScanParams.u2OBSSScanActiveTotalPerCh);
        WSSMAC_PUT_2BYTES (pu1ReadPtr,
                           pReassocRspStruct->unMacMsg.ReassocRspMacFrame.
                           OBSSScanParams.u2BSSWidChTransDelayFactor);
        WSSMAC_PUT_2BYTES (pu1ReadPtr,
                           pReassocRspStruct->unMacMsg.ReassocRspMacFrame.
                           OBSSScanParams.u2OBSSScanActivityThreshold);
    }

    /* Put Extended Capabilities */
    u1ElementId = pReassocRspStruct->unMacMsg.ReassocRspMacFrame.
        ExtCapabilities.u1MacFrameElemId;
    if (u1ElementId == WSSMAC_EXT_CAPAB_ELEMENT_ID)
    {
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pReassocRspStruct->unMacMsg.
                          ReassocRspMacFrame.ExtCapabilities.u1MacFrameElemId);
        WSSMAC_PUT_1BYTE (pu1ReadPtr, pReassocRspStruct->unMacMsg.
                          ReassocRspMacFrame.ExtCapabilities.u1MacFrameElemLen);
        WSSMAC_PUT_NBYTES (pu1ReadPtr, pReassocRspStruct->unMacMsg.
                           ReassocRspMacFrame.ExtCapabilities.au1Capabilities,
                           pReassocRspStruct->unMacMsg.
                           ReassocRspMacFrame.ExtCapabilities.
                           u1MacFrameElemLen);
    }

    /* Put Operating Mode Notification Element */
    u1ElementId = pReassocRspStruct->unMacMsg.ReassocRspMacFrame.OperModeNotify.
        u1ElemId;
    if (u1ElementId == WSSMAC_VHT_OPERATING_MODE_NOTIFICATION)
    {
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          pReassocRspStruct->unMacMsg.ReassocRspMacFrame.
                          OperModeNotify.u1ElemId);
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          pReassocRspStruct->unMacMsg.ReassocRspMacFrame.
                          OperModeNotify.u1ElemLen);
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          pReassocRspStruct->unMacMsg.ReassocRspMacFrame.
                          OperModeNotify.u1VhtOperModeElem);
    }

    /* Put EDCA Parameters */

    u1ElementId = pReassocRspStruct->unMacMsg.AssocRspMacFrame.
        WMMParam.u1MacFrameElemId;
    if (u1ElementId == WSSMAC_VENDOR_INFO_ELEMENTID)
    {

        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          pReassocRspStruct->unMacMsg.ReassocRspMacFrame.
                          WMMParam.u1MacFrameElemId);
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          pReassocRspStruct->unMacMsg.ReassocRspMacFrame.
                          WMMParam.u1MacFrameElemLen);
        WSSMAC_PUT_NBYTES (pu1ReadPtr,
                           pReassocRspStruct->unMacMsg.ReassocRspMacFrame.
                           WMMParam.au1OUI, WSSMAC_MAX_OUI_SIZE);
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          pReassocRspStruct->unMacMsg.ReassocRspMacFrame.
                          WMMParam.u1OUIType);
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          pReassocRspStruct->unMacMsg.ReassocRspMacFrame.
                          WMMParam.u1OUISubType);
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          pReassocRspStruct->unMacMsg.ReassocRspMacFrame.
                          WMMParam.u1WMMVersion);
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          pReassocRspStruct->unMacMsg.ReassocRspMacFrame.
                          WMMParam.u1QOSInfo);
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          pReassocRspStruct->unMacMsg.ReassocRspMacFrame.
                          WMMParam.u1Rsvrd);

        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          pReassocRspStruct->unMacMsg.ReassocRspMacFrame.
                          EDCAParam.MacFrameEDCA_AC_BE.
                          u1MacFrameEDCA_AC_ACI_AFSN);
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          pReassocRspStruct->unMacMsg.ReassocRspMacFrame.
                          EDCAParam.MacFrameEDCA_AC_BE.
                          u1MacFrameEDCA_AC_ECWMin_Max);
        WSSMAC_PUT_2BYTES (pu1ReadPtr,
                           pReassocRspStruct->unMacMsg.ReassocRspMacFrame.
                           EDCAParam.MacFrameEDCA_AC_BE.
                           u2MacFrameEDCA_AC_TxOpLimit);

        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          pReassocRspStruct->unMacMsg.ReassocRspMacFrame.
                          EDCAParam.MacFrameEDCA_AC_BK.
                          u1MacFrameEDCA_AC_ACI_AFSN);
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          pReassocRspStruct->unMacMsg.ReassocRspMacFrame.
                          EDCAParam.MacFrameEDCA_AC_BK.
                          u1MacFrameEDCA_AC_ECWMin_Max);
        WSSMAC_PUT_2BYTES (pu1ReadPtr,
                           pReassocRspStruct->unMacMsg.ReassocRspMacFrame.
                           EDCAParam.MacFrameEDCA_AC_BK.
                           u2MacFrameEDCA_AC_TxOpLimit);

        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          pReassocRspStruct->unMacMsg.ReassocRspMacFrame.
                          EDCAParam.MacFrameEDCA_AC_VI.
                          u1MacFrameEDCA_AC_ACI_AFSN);
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          pReassocRspStruct->unMacMsg.ReassocRspMacFrame.
                          EDCAParam.MacFrameEDCA_AC_VI.
                          u1MacFrameEDCA_AC_ECWMin_Max);
        WSSMAC_PUT_2BYTES (pu1ReadPtr,
                           pReassocRspStruct->unMacMsg.ReassocRspMacFrame.
                           EDCAParam.MacFrameEDCA_AC_VI.
                           u2MacFrameEDCA_AC_TxOpLimit);

        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          pReassocRspStruct->unMacMsg.ReassocRspMacFrame.
                          EDCAParam.MacFrameEDCA_AC_VO.
                          u1MacFrameEDCA_AC_ACI_AFSN);
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          pReassocRspStruct->unMacMsg.ReassocRspMacFrame.
                          EDCAParam.MacFrameEDCA_AC_VO.
                          u1MacFrameEDCA_AC_ECWMin_Max);
        WSSMAC_PUT_2BYTES (pu1ReadPtr,
                           pReassocRspStruct->unMacMsg.ReassocRspMacFrame.
                           EDCAParam.MacFrameEDCA_AC_VO.
                           u2MacFrameEDCA_AC_TxOpLimit);
    }

    /* Put vendor info */
    u2Count1 = 0;
    while (u2Count1 < WSSMAC_VEND_INFO_ELEM)
    {
        u1ElementId = pReassocRspStruct->unMacMsg.ReassocRspMacFrame.
            aVendSpecInfo[u2Count1].u1MacFrameElemId;
        if (u1ElementId == WSSMAC_VENDOR_INFO_ELEMENTID)
        {
            WSSMAC_PUT_1BYTE (pu1ReadPtr,
                              pReassocRspStruct->unMacMsg.ReassocRspMacFrame.
                              aVendSpecInfo[u2Count1].u1MacFrameElemId);
            WSSMAC_PUT_1BYTE (pu1ReadPtr,
                              pReassocRspStruct->unMacMsg.ReassocRspMacFrame.
                              aVendSpecInfo[u2Count1].u1MacFrameElemLen);
            WSSMAC_PUT_NBYTES (pu1ReadPtr,
                               pReassocRspStruct->unMacMsg.ReassocRspMacFrame.
                               aVendSpecInfo[u2Count1].au1MacFrameVend_OUI,
                               WSSMAC_MAX_OUI_SIZE);
            u2Count = 0;
            u2Len = (UINT2) (pReassocRspStruct->unMacMsg.ReassocRspMacFrame.
                             aVendSpecInfo[u2Count1].u1MacFrameElemLen -
                             WSSMAC_MAX_OUI_SIZE);
            while (u2Count < u2Len)
            {
                WSSMAC_PUT_1BYTE (pu1ReadPtr, pReassocRspStruct->unMacMsg.
                                  ReassocRspMacFrame.aVendSpecInfo[u2Count1].
                                  au1MacFrameVend_Content[u2Count]);
                u2Count++;
            }
        }
        u2Count1++;
    }
    u4Len = (UINT4) (pu1ReadPtr - pu1Buf);
    /* Allocate memory for packet linear buffer */
    pReassocRspBuf->unMacMsg.MacDot11PktBuf.pDot11MacPdu =
        WSSMAC_ALLOCATE_CRU_BUF (u4Len);

    if (pReassocRspBuf->unMacMsg.MacDot11PktBuf.pDot11MacPdu == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC, "Memory Allocation Failed \r\n");
        WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pu1Buf);
        return;
    }

    WSSMAC_COPY_TO_BUF (pReassocRspBuf->unMacMsg.MacDot11PktBuf.pDot11MacPdu,
                        pu1Buf, 0, u4Len);
    WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pu1Buf);
    return;
}

/* ***************************************************************************
 *                                                                           *
 * Function     : WssMacProcessDeauthenMsg                                   *
 *                                                                           *
 * Description  : Process Deauthentication frame                             *
 *                                                                           *
 * Input(s)     : MAC message type and union of struct with CRU_BUF          *
 *                                                                           *
 * Output(s)    : Deauthentication message structure                         *
 *                                                                           *
 * Returns      : SUCCESS/FAILURE                                            *
 *                                                                           *
 * ***************************************************************************/
UINT1
WssMacProcessDeauthenMsg (UINT1 eMsgType, tWssMacMsgStruct * pMsgStruct)
{
    tWssMacMsgStruct   *pDeauthMsgStruct = NULL;
    UINT2               u2PacketLen = 0, u2Count = 0, u2Count1 = 0;
    UINT1              *pu1ReadPtr = NULL;
    tCRU_BUF_CHAIN_DESC *pBuf = NULL;
    UINT2               u2Len = 0;
    UINT4               u4Len = 0;
    UINT1               u1ElementId = 0;
    UINT1              *pu1Buf = NULL;
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;
    unWlcHdlrMsgStruct *pCapwapMsgStruct = NULL;
    CHR1                ac1TimeStr[MAX_TIME_LEN] = "\0";

    pWlcHdlrMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pWlcHdlrMsgStruct == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                    "WssMacProcessDeauthenMsg:- "
                    "UtlShMemAllocWlcBuf returned failure\n");
        return WSSMAC_FAILURE;
    }

    pCapwapMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pCapwapMsgStruct == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                    "WssMacProcessDeauthenMsg:- "
                    "UtlShMemAllocWlcBuf returned failure\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return WSSMAC_FAILURE;
    }
    pDeauthMsgStruct =
        (tWssMacMsgStruct *) (VOID *) UtlShMemAllocMacMsgStructBuf ();
    if (pDeauthMsgStruct == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                    "WssMacProcessDeauthenMsg:-"
                    "UtlShMemAllocMacMsgStructBuf returned failure\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeWlcBuf ((UINT1 *) pCapwapMsgStruct);
        return WSSMAC_FAILURE;
    }
    WLCHDLR_PKTBUF_ALLOC_MEM_BLOCK (pu1Buf);
    if (pu1Buf == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                    "WssMacProcessDeauthenMsg:-"
                    "UtlShMemAllocMacMsgStructBuf returned failure\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeWlcBuf ((UINT1 *) pCapwapMsgStruct);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pDeauthMsgStruct);
        return OSIX_FAILURE;
    }

    MEMSET (pDeauthMsgStruct, 0, sizeof (tWssMacMsgStruct));
    MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));
    MEMSET (pCapwapMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));
    UNUSED_PARAM (eMsgType);
    if (pMsgStruct->msgType == 0)
    {
        pBuf = pMsgStruct->unMacMsg.MacDot11PktBuf.pDot11MacPdu;

        u2PacketLen = (UINT2) WSSMAC_GET_BUF_LEN (pBuf);
        pu1ReadPtr = WSSMAC_BUF_IF_LINEAR (pBuf, 0, u2PacketLen);
        if (pu1ReadPtr == NULL)
        {
            MEMSET (pu1Buf, 0x00, u2PacketLen);
            WSSMAC_COPY_FROM_BUF (pBuf, pu1Buf, 0, u2PacketLen);
            pu1ReadPtr = pu1Buf;
        }

        /* Get the 802.11 MAC Header */
        WSSMAC_GET_2BYTES (pDeauthMsgStruct->unMacMsg.DeauthMacFrame.
                           MacMgmtFrmHdr.u2MacFrameCtrl, pu1ReadPtr);
        WSSMAC_GET_2BYTES (pDeauthMsgStruct->unMacMsg.DeauthMacFrame.
                           MacMgmtFrmHdr.u2MacFrameDurId, pu1ReadPtr);
        WSSMAC_GET_NBYTES (pDeauthMsgStruct->unMacMsg.DeauthMacFrame.
                           MacMgmtFrmHdr.u1DA, pu1ReadPtr, WSSMAC_MAC_ADDR_LEN);
        WSSMAC_GET_NBYTES (pDeauthMsgStruct->unMacMsg.DeauthMacFrame.
                           MacMgmtFrmHdr.u1SA, pu1ReadPtr, WSSMAC_MAC_ADDR_LEN);
        WSSMAC_GET_NBYTES (pDeauthMsgStruct->unMacMsg.DeauthMacFrame.
                           MacMgmtFrmHdr.u1BssId, pu1ReadPtr,
                           WSSMAC_MAC_ADDR_LEN);
        WSSMAC_GET_2BYTES (pDeauthMsgStruct->unMacMsg.DeauthMacFrame.
                           MacMgmtFrmHdr.u2MacFrameSeqCtrl, pu1ReadPtr);

        /* Get Reason code */
        WSSMAC_GET_2BYTES (pDeauthMsgStruct->unMacMsg.DeauthMacFrame.ReasonCode.
                           u2MacFrameReasonCode, pu1ReadPtr);

        /* Fill the Vendor info if Element ID ==  WSSMAC_VENDOR_INFO_ELEMENTID */
        u2Count1 = 0;
        while ((*pu1ReadPtr) == WSSMAC_VENDOR_INFO_ELEMENTID)
        {
            WSSMAC_GET_1BYTE (pDeauthMsgStruct->unMacMsg.DeauthMacFrame.
                              aVendSpecInfo[u2Count1].u1MacFrameElemId,
                              pu1ReadPtr);
            WSSMAC_GET_1BYTE (pDeauthMsgStruct->unMacMsg.DeauthMacFrame.
                              aVendSpecInfo[u2Count1].u1MacFrameElemLen,
                              pu1ReadPtr);
            WSSMAC_GET_NBYTES (pDeauthMsgStruct->unMacMsg.DeauthMacFrame.
                               aVendSpecInfo[u2Count1].au1MacFrameVend_OUI,
                               pu1ReadPtr, WSSMAC_MAX_OUI_SIZE);
            u2Count = 0;
            u2Len = (UINT2) (pDeauthMsgStruct->unMacMsg.DeauthMacFrame.
                             aVendSpecInfo[u2Count1].
                             u1MacFrameElemLen - WSSMAC_MAX_OUI_SIZE);
            while (u2Count < u2Len)
            {
                WSSMAC_GET_1BYTE (pDeauthMsgStruct->unMacMsg.DeauthMacFrame.
                                  aVendSpecInfo[u2Count1].
                                  au1MacFrameVend_Content[u2Count], pu1ReadPtr);
                u2Count++;
            }
            u2Count1++;
        }

        /* Fill the session id */
        pDeauthMsgStruct->unMacMsg.DeauthMacFrame.u2SessId =
            pMsgStruct->unMacMsg.MacDot11PktBuf.u2SessId;

        MEMCPY (&(pWlcHdlrMsgStruct->WssMacMsgStruct.unMacMsg.DeauthMacFrame),
                &(pDeauthMsgStruct->unMacMsg.DeauthMacFrame),
                sizeof (tDot11DeauthMacFrame));
        if (WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_STA_DEAUTH_MSG,
                                    pWlcHdlrMsgStruct) != WSSMAC_SUCCESS)
        {
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            UtlShMemFreeWlcBuf ((UINT1 *) pCapwapMsgStruct);
            UtlShMemFreeMacMsgStructBuf ((UINT1 *) pDeauthMsgStruct);
            WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pu1Buf);
            return WSSMAC_FAILURE;
        }
    }
    else
    {
        MEMSET (pu1Buf, 0, WSSMAC_MAX_PKT_LEN);
        pu1ReadPtr = pu1Buf;
        /* Get the 802.11 MAC Header */
        pMsgStruct->unMacMsg.DeauthMacFrame.MacMgmtFrmHdr.u2MacFrameCtrl =
            WSSMAC_SET_DEAUTH_FRAME_CTRL;
        WSSMAC_PUT_2BYTES (pu1ReadPtr, pMsgStruct->unMacMsg.DeauthMacFrame.MacMgmtFrmHdr.u2MacFrameCtrl);    /* Get Frame control field */
        WSSMAC_PUT_2BYTES (pu1ReadPtr, pMsgStruct->unMacMsg.DeauthMacFrame.MacMgmtFrmHdr.u2MacFrameDurId);    /* Get Duration ID field */
        WSSMAC_PUT_NBYTES (pu1ReadPtr, pMsgStruct->unMacMsg.DeauthMacFrame.MacMgmtFrmHdr.u1DA, WSSMAC_MAC_ADDR_LEN);    /* Get Address1 field */
        WSSMAC_PUT_NBYTES (pu1ReadPtr, pMsgStruct->unMacMsg.DeauthMacFrame.MacMgmtFrmHdr.u1SA, WSSMAC_MAC_ADDR_LEN);    /* Get Address2 field */
        WSSMAC_PUT_NBYTES (pu1ReadPtr, pMsgStruct->unMacMsg.DeauthMacFrame.MacMgmtFrmHdr.u1BssId, WSSMAC_MAC_ADDR_LEN);    /* Get Addr3 field */
        WSSMAC_PUT_2BYTES (pu1ReadPtr, pMsgStruct->unMacMsg.DeauthMacFrame.MacMgmtFrmHdr.u2MacFrameSeqCtrl);    /* Get Sequence ctrl field */

        WSSMAC_PUT_2BYTES (pu1ReadPtr, pMsgStruct->unMacMsg.DeauthMacFrame.ReasonCode.u2MacFrameReasonCode);    /* Get Reason code */
        WssUtilGetTimeStr (ac1TimeStr);
        CAPWAP_TRC7 (CAPWAP_STATION_TRC,
                     "%s : DEAUTH sent by WLC to %02x:%02x:%02x:%02x:%02x:%02x\n",
                     ac1TimeStr,
                     pMsgStruct->unMacMsg.DeauthMacFrame.MacMgmtFrmHdr.u1DA[0],
                     pMsgStruct->unMacMsg.DeauthMacFrame.MacMgmtFrmHdr.u1DA[1],
                     pMsgStruct->unMacMsg.DeauthMacFrame.MacMgmtFrmHdr.u1DA[2],
                     pMsgStruct->unMacMsg.DeauthMacFrame.MacMgmtFrmHdr.u1DA[3],
                     pMsgStruct->unMacMsg.DeauthMacFrame.MacMgmtFrmHdr.u1DA[4],
                     pMsgStruct->unMacMsg.DeauthMacFrame.MacMgmtFrmHdr.u1DA[5]);
        SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4WlcHdlrSysLogId,
                      "%s : DEAUTH sent by WLC to %02x:%02x:%02x:%02x:%02x:%02x",
                      ac1TimeStr,
                      pMsgStruct->unMacMsg.DisassocMacFrame.MacMgmtFrmHdr.
                      u1DA[0],
                      pMsgStruct->unMacMsg.DisassocMacFrame.MacMgmtFrmHdr.
                      u1DA[1],
                      pMsgStruct->unMacMsg.DisassocMacFrame.MacMgmtFrmHdr.
                      u1DA[2],
                      pMsgStruct->unMacMsg.DisassocMacFrame.MacMgmtFrmHdr.
                      u1DA[3],
                      pMsgStruct->unMacMsg.DisassocMacFrame.MacMgmtFrmHdr.
                      u1DA[4],
                      pMsgStruct->unMacMsg.DisassocMacFrame.MacMgmtFrmHdr.
                      u1DA[5]));

        /*Put Vendor info */
        u2Count1 = 0;
        while (u2Count1 < WSSMAC_VEND_INFO_ELEM)
        {
            u1ElementId = pMsgStruct->unMacMsg.DeauthMacFrame.
                aVendSpecInfo[u2Count1].u1MacFrameElemId;
            if (u1ElementId == WSSMAC_VENDOR_INFO_ELEMENTID)
            {
                WSSMAC_PUT_1BYTE (pu1ReadPtr,
                                  pMsgStruct->unMacMsg.DeauthMacFrame.
                                  aVendSpecInfo[u2Count1].u1MacFrameElemId);
                WSSMAC_PUT_1BYTE (pu1ReadPtr,
                                  pMsgStruct->unMacMsg.DeauthMacFrame.
                                  aVendSpecInfo[u2Count1].u1MacFrameElemLen);
                WSSMAC_PUT_NBYTES (pu1ReadPtr,
                                   pMsgStruct->unMacMsg.DeauthMacFrame.
                                   aVendSpecInfo[u2Count1].au1MacFrameVend_OUI,
                                   WSSMAC_MAX_OUI_SIZE);
                u2Count = 0;
                u2Len =
                    (UINT2) (pMsgStruct->unMacMsg.DeauthMacFrame.
                             aVendSpecInfo[u2Count1].u1MacFrameElemLen -
                             WSSMAC_MAX_OUI_SIZE);
                while (u2Count < u2Len)
                {
                    WSSMAC_PUT_1BYTE (pu1ReadPtr, pMsgStruct->unMacMsg.
                                      DeauthMacFrame.aVendSpecInfo[u2Count1].
                                      au1MacFrameVend_Content[u2Count]);
                    u2Count++;
                }
            }
            u2Count1++;
        }
        u4Len = (UINT4) (pu1ReadPtr - pu1Buf);

        /* Allocate memory for packet linear buffer */
        pDeauthMsgStruct->unMacMsg.MacDot11PktBuf.pDot11MacPdu =
            WSSMAC_ALLOCATE_CRU_BUF (u4Len);
        if (pDeauthMsgStruct->unMacMsg.MacDot11PktBuf.pDot11MacPdu == NULL)
        {
            WSSMAC_TRC (WSSMAC_FAILURE_TRC, "Memory Allocation Failed \r\n");
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            UtlShMemFreeWlcBuf ((UINT1 *) pCapwapMsgStruct);
            UtlShMemFreeMacMsgStructBuf ((UINT1 *) pDeauthMsgStruct);
            WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pu1Buf);
            return WSSMAC_FAILURE;
        }

        WSSMAC_COPY_TO_BUF (pDeauthMsgStruct->unMacMsg.MacDot11PktBuf.
                            pDot11MacPdu, pu1Buf, 0, u4Len);
        if (pDeauthMsgStruct->unMacMsg.MacDot11PktBuf.pDot11MacPdu != NULL)
        {
            pCapwapMsgStruct->WlcHdlrQueueReq.pRcvBuf =
                pDeauthMsgStruct->unMacMsg.MacDot11PktBuf.pDot11MacPdu;
            pCapwapMsgStruct->WlcHdlrQueueReq.u2SessId =
                pMsgStruct->unMacMsg.DeauthMacFrame.u2SessId;

            pCapwapMsgStruct->WlcHdlrQueueReq.u4MsgType =
                WSS_CAPWAP_802_11_MGMT_PKT;

            if (WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_CAPWAP_MAC_MSG,
                                        pCapwapMsgStruct) != WSSMAC_SUCCESS)
            {
                UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                UtlShMemFreeWlcBuf ((UINT1 *) pCapwapMsgStruct);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pDeauthMsgStruct);
                WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pu1Buf);
                return WSSMAC_FAILURE;
            }
        }
        else
        {
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            UtlShMemFreeWlcBuf ((UINT1 *) pCapwapMsgStruct);
            UtlShMemFreeMacMsgStructBuf ((UINT1 *) pDeauthMsgStruct);
            WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pu1Buf);
            return WSSMAC_FAILURE;
        }
    }
    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
    UtlShMemFreeWlcBuf ((UINT1 *) pCapwapMsgStruct);
    UtlShMemFreeMacMsgStructBuf ((UINT1 *) pDeauthMsgStruct);
    WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pu1Buf);
    return WSSMAC_SUCCESS;
}

/* ***************************************************************************
 *                                                                           *
 * Function     : WssMacProcessDisassocMsg                                   *
 *                                                                           *
 * Description  : Process Disassociation frame                               *
 *                                                                           *
 * Input(s)     : MAC message type and union of struct with CRU_BUF          *
 *                                                                           *
 * Output(s)    : Disassociation message structure                           *
 *                                                                           *
 * Returns      : SUCCESS/FAILURE                                            *
 *                                                                           *
 * ***************************************************************************/
UINT1
WssMacProcessDisassocMsg (UINT1 eMsgType, tWssMacMsgStruct * pMsgStruct)
{
    tWssMacMsgStruct   *pDisassocMsgStruct = NULL;
    UINT2               u2PacketLen = 0, u2Count = 0, u2Count1 = 0;
    UINT1              *pu1ReadPtr = NULL;
    tCRU_BUF_CHAIN_DESC *pBuf = NULL;
    UINT2               u2Len = 0;
    UINT4               u4Len = 0;
    UINT1               u1ElementId = 0;
    UINT1              *pu1Buf = NULL;
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;
    unWlcHdlrMsgStruct *pCapwapMsgStruct = NULL;
    CHR1                ac1TimeStr[MAX_TIME_LEN] = "\0";

    pWlcHdlrMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pWlcHdlrMsgStruct == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                    "WssMacProcessDisassocMsg:- "
                    "UtlShMemAllocWlcBuf returned failure\n");
        return WSSMAC_FAILURE;
    }

    pCapwapMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pCapwapMsgStruct == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                    "WssMacProcessDisassocMsg:- "
                    "UtlShMemAllocWlcBuf returned failure\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return WSSMAC_FAILURE;
    }
    WLCHDLR_PKTBUF_ALLOC_MEM_BLOCK (pu1Buf);
    if (pu1Buf == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                    "WssMacProcessDisassocMsg:-"
                    "UtlShMemAllocMacMsgStructBuf returned failure\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeWlcBuf ((UINT1 *) pCapwapMsgStruct);
        return WSSMAC_FAILURE;
    }
    pDisassocMsgStruct =
        (tWssMacMsgStruct *) (VOID *) UtlShMemAllocMacMsgStructBuf ();
    if (pDisassocMsgStruct == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                    "WssMacProcessDisassocMsg:-"
                    "UtlShMemAllocMacMsgStructBuf returned failure\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeWlcBuf ((UINT1 *) pCapwapMsgStruct);
        WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pu1Buf);
        return OSIX_FAILURE;
    }
    MEMSET (pDisassocMsgStruct, 0, sizeof (tWssMacMsgStruct));
    MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));
    MEMSET (pCapwapMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));
    UNUSED_PARAM (eMsgType);

    if (pMsgStruct->msgType == 0)
    {
        pBuf = pMsgStruct->unMacMsg.MacDot11PktBuf.pDot11MacPdu;

        u2PacketLen = (UINT2) WSSMAC_GET_BUF_LEN (pBuf);
        pu1ReadPtr = WSSMAC_BUF_IF_LINEAR (pBuf, 0, u2PacketLen);
        if (pu1ReadPtr == NULL)
        {
            MEMSET (pu1Buf, 0x00, u2PacketLen);
            WSSMAC_COPY_FROM_BUF (pBuf, pu1Buf, 0, u2PacketLen);
            pu1ReadPtr = pu1Buf;
        }

        /* Fill the 802.11 MAC Header */
        WSSMAC_GET_2BYTES (pDisassocMsgStruct->unMacMsg.DisassocMacFrame.
                           MacMgmtFrmHdr.u2MacFrameCtrl, pu1ReadPtr);
        WSSMAC_GET_2BYTES (pDisassocMsgStruct->unMacMsg.DisassocMacFrame.
                           MacMgmtFrmHdr.u2MacFrameDurId, pu1ReadPtr);
        WSSMAC_GET_NBYTES (pDisassocMsgStruct->unMacMsg.DisassocMacFrame.
                           MacMgmtFrmHdr.u1DA, pu1ReadPtr, WSSMAC_MAC_ADDR_LEN);
        WSSMAC_GET_NBYTES (pDisassocMsgStruct->unMacMsg.DisassocMacFrame.
                           MacMgmtFrmHdr.u1SA, pu1ReadPtr, WSSMAC_MAC_ADDR_LEN);
        WSSMAC_GET_NBYTES (pDisassocMsgStruct->unMacMsg.DisassocMacFrame.
                           MacMgmtFrmHdr.u1BssId, pu1ReadPtr,
                           WSSMAC_MAC_ADDR_LEN);
        WSSMAC_GET_2BYTES (pDisassocMsgStruct->unMacMsg.DisassocMacFrame.
                           MacMgmtFrmHdr.u2MacFrameSeqCtrl, pu1ReadPtr);

        /* Fill Reason code */
        WSSMAC_GET_2BYTES (pDisassocMsgStruct->unMacMsg.DisassocMacFrame.
                           ReasonCode.u2MacFrameReasonCode, pu1ReadPtr);
        CAPWAP_TRC7 (CAPWAP_STATION_TRC,
                     "WssMacProcessDisassocMsg::: SRC Station MAC : %02x:%02x:%02x:%02x:%02x:%02x :: Reason Code : %d \r\n",
                     pDisassocMsgStruct->unMacMsg.DisassocMacFrame.
                     MacMgmtFrmHdr.u1DA[0],
                     pDisassocMsgStruct->unMacMsg.DisassocMacFrame.
                     MacMgmtFrmHdr.u1DA[1],
                     pDisassocMsgStruct->unMacMsg.DisassocMacFrame.
                     MacMgmtFrmHdr.u1DA[2],
                     pDisassocMsgStruct->unMacMsg.DisassocMacFrame.
                     MacMgmtFrmHdr.u1DA[3],
                     pDisassocMsgStruct->unMacMsg.DisassocMacFrame.
                     MacMgmtFrmHdr.u1DA[4],
                     pDisassocMsgStruct->unMacMsg.DisassocMacFrame.
                     MacMgmtFrmHdr.u1DA[5],
                     pDisassocMsgStruct->unMacMsg.DisassocMacFrame.ReasonCode.
                     u2MacFrameReasonCode);

        /* Fill the Vendor info if Element ID ==  WSSMAC_VENDOR_INFO_ELEMENTID */
        u2Count1 = 0;
        while ((*pu1ReadPtr) == WSSMAC_VENDOR_INFO_ELEMENTID)
        {
            WSSMAC_GET_1BYTE (pDisassocMsgStruct->unMacMsg.DisassocMacFrame.
                              aVendSpecInfo[u2Count1].u1MacFrameElemId,
                              pu1ReadPtr);
            WSSMAC_GET_1BYTE (pDisassocMsgStruct->unMacMsg.DisassocMacFrame.
                              aVendSpecInfo[u2Count1].u1MacFrameElemLen,
                              pu1ReadPtr);
            WSSMAC_GET_NBYTES (pDisassocMsgStruct->unMacMsg.DisassocMacFrame.
                               aVendSpecInfo[u2Count1].au1MacFrameVend_OUI,
                               pu1ReadPtr, WSSMAC_MAX_OUI_SIZE);
            u2Count = 0;
            u2Len = (UINT2) (pDisassocMsgStruct->unMacMsg.DisassocMacFrame.
                             aVendSpecInfo[u2Count1].u1MacFrameElemLen -
                             WSSMAC_MAX_OUI_SIZE);
            while (u2Count < u2Len)
            {
                WSSMAC_GET_1BYTE (pDisassocMsgStruct->unMacMsg.DisassocMacFrame.
                                  aVendSpecInfo[u2Count1].
                                  au1MacFrameVend_Content[u2Count], pu1ReadPtr);
                u2Count++;
            }
            u2Count1++;
        }

        /* Fill the session id */
        pDisassocMsgStruct->unMacMsg.DisassocMacFrame.u2SessId =
            pMsgStruct->unMacMsg.MacDot11PktBuf.u2SessId;

        MEMCPY (&(pWlcHdlrMsgStruct->WssMacMsgStruct.unMacMsg.DisassocMacFrame),
                &(pDisassocMsgStruct->unMacMsg.DisassocMacFrame),
                sizeof (tDot11DisassocMacFrame));
        if (WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_STA_DISASSOC_MSG,
                                    pWlcHdlrMsgStruct) != WSSMAC_SUCCESS)
        {
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            UtlShMemFreeWlcBuf ((UINT1 *) pCapwapMsgStruct);
            WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pu1Buf);
            UtlShMemFreeMacMsgStructBuf ((UINT1 *) pDisassocMsgStruct);
            return WSSMAC_FAILURE;
        }
    }
    else
    {
        MEMSET (pu1Buf, 0, WSSMAC_MAX_PKT_LEN);
        pu1ReadPtr = pu1Buf;
        /* Get the 802.11 MAC Header */
        pMsgStruct->unMacMsg.DisassocMacFrame.MacMgmtFrmHdr.u2MacFrameCtrl =
            WSSMAC_SET_DISASSOC_FRAME_CTRL;
        WSSMAC_PUT_2BYTES (pu1ReadPtr, pMsgStruct->unMacMsg.DisassocMacFrame.MacMgmtFrmHdr.u2MacFrameCtrl);    /* Get Frame control field */
        WSSMAC_PUT_2BYTES (pu1ReadPtr, pMsgStruct->unMacMsg.DisassocMacFrame.MacMgmtFrmHdr.u2MacFrameDurId);    /* Get Duration ID field */
        WSSMAC_PUT_NBYTES (pu1ReadPtr, pMsgStruct->unMacMsg.DisassocMacFrame.MacMgmtFrmHdr.u1DA, WSSMAC_MAC_ADDR_LEN);    /* Get Address1 field */
        WSSMAC_PUT_NBYTES (pu1ReadPtr, pMsgStruct->unMacMsg.DisassocMacFrame.MacMgmtFrmHdr.u1SA, WSSMAC_MAC_ADDR_LEN);    /* Get Addr2 field */
        WSSMAC_PUT_NBYTES (pu1ReadPtr, pMsgStruct->unMacMsg.DisassocMacFrame.MacMgmtFrmHdr.u1BssId, WSSMAC_MAC_ADDR_LEN);    /* Get Addr3field */
        WSSMAC_PUT_2BYTES (pu1ReadPtr, pMsgStruct->unMacMsg.DisassocMacFrame.MacMgmtFrmHdr.u2MacFrameSeqCtrl);    /* Get Sequence ctrl field */

        WSSMAC_PUT_2BYTES (pu1ReadPtr, pMsgStruct->unMacMsg.DisassocMacFrame.ReasonCode.u2MacFrameReasonCode);    /* Get Reason code */
        WssUtilGetTimeStr (ac1TimeStr);
        CAPWAP_TRC7 (CAPWAP_STATION_TRC,
                     "%s : DISASSOC sent by WLC to %02x:%02x:%02x:%02x:%02x:%02x\n",
                     ac1TimeStr,
                     pMsgStruct->unMacMsg.DisassocMacFrame.MacMgmtFrmHdr.
                     u1DA[0],
                     pMsgStruct->unMacMsg.DisassocMacFrame.MacMgmtFrmHdr.
                     u1DA[1],
                     pMsgStruct->unMacMsg.DisassocMacFrame.MacMgmtFrmHdr.
                     u1DA[2],
                     pMsgStruct->unMacMsg.DisassocMacFrame.MacMgmtFrmHdr.
                     u1DA[3],
                     pMsgStruct->unMacMsg.DisassocMacFrame.MacMgmtFrmHdr.
                     u1DA[4],
                     pMsgStruct->unMacMsg.DisassocMacFrame.MacMgmtFrmHdr.
                     u1DA[5]);
        SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4WlcHdlrSysLogId,
                      "%s : DISASSOC sent by WLC to %02x:%02x:%02x:%02x:%02x:%02x",
                      ac1TimeStr,
                      pMsgStruct->unMacMsg.DisassocMacFrame.MacMgmtFrmHdr.
                      u1DA[0],
                      pMsgStruct->unMacMsg.DisassocMacFrame.MacMgmtFrmHdr.
                      u1DA[1],
                      pMsgStruct->unMacMsg.DisassocMacFrame.MacMgmtFrmHdr.
                      u1DA[2],
                      pMsgStruct->unMacMsg.DisassocMacFrame.MacMgmtFrmHdr.
                      u1DA[3],
                      pMsgStruct->unMacMsg.DisassocMacFrame.MacMgmtFrmHdr.
                      u1DA[4],
                      pMsgStruct->unMacMsg.DisassocMacFrame.MacMgmtFrmHdr.
                      u1DA[5]));

        /*Put Vendor info */
        u2Count1 = 0;

        while (u2Count1 < WSSMAC_VEND_INFO_ELEM)
        {
            u1ElementId = pMsgStruct->unMacMsg.DisassocMacFrame.
                aVendSpecInfo[u2Count1].u1MacFrameElemLen;
            if (u1ElementId == WSSMAC_VENDOR_INFO_ELEMENTID)
            {
                WSSMAC_PUT_1BYTE (pu1ReadPtr,
                                  pMsgStruct->unMacMsg.DisassocMacFrame.
                                  aVendSpecInfo[u2Count1].u1MacFrameElemId);
                WSSMAC_PUT_1BYTE (pu1ReadPtr,
                                  pMsgStruct->unMacMsg.DisassocMacFrame.
                                  aVendSpecInfo[u2Count1].u1MacFrameElemLen);
                WSSMAC_PUT_NBYTES (pu1ReadPtr,
                                   pMsgStruct->unMacMsg.DisassocMacFrame.
                                   aVendSpecInfo[u2Count1].au1MacFrameVend_OUI,
                                   WSSMAC_MAX_OUI_SIZE);
                u2Count = 0;
                u2Len = (UINT2) (pMsgStruct->unMacMsg.DisassocMacFrame.
                                 aVendSpecInfo[u2Count1].u1MacFrameElemLen -
                                 WSSMAC_MAX_OUI_SIZE);

                while (u2Count < u2Len)
                {
                    WSSMAC_PUT_1BYTE (pu1ReadPtr, pMsgStruct->unMacMsg.
                                      DisassocMacFrame.aVendSpecInfo[u2Count1].
                                      au1MacFrameVend_Content[u2Count]);
                    u2Count++;
                }
            }
            u2Count1++;
        }
        u4Len = (UINT4) (pu1ReadPtr - pu1Buf);

        /* Allocate memory for packet linear buffer */
        pDisassocMsgStruct->unMacMsg.MacDot11PktBuf.pDot11MacPdu =
            WSSMAC_ALLOCATE_CRU_BUF (u4Len);
        if (pDisassocMsgStruct->unMacMsg.MacDot11PktBuf.pDot11MacPdu == NULL)
        {
            WSSMAC_TRC (WSSMAC_FAILURE_TRC, "Memory Allocation Failed \r\n");
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            UtlShMemFreeWlcBuf ((UINT1 *) pCapwapMsgStruct);
            WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pu1Buf);
            UtlShMemFreeMacMsgStructBuf ((UINT1 *) pDisassocMsgStruct);
            return WSSMAC_FAILURE;
        }

        WSSMAC_COPY_TO_BUF (pDisassocMsgStruct->unMacMsg.MacDot11PktBuf.
                            pDot11MacPdu, pu1Buf, 0, u4Len);

        if (pDisassocMsgStruct->unMacMsg.MacDot11PktBuf.pDot11MacPdu != NULL)
        {
            pCapwapMsgStruct->WlcHdlrQueueReq.pRcvBuf =
                pDisassocMsgStruct->unMacMsg.MacDot11PktBuf.pDot11MacPdu;
            pCapwapMsgStruct->WlcHdlrQueueReq.u2SessId =
                pMsgStruct->unMacMsg.DisassocMacFrame.u2SessId;

            pCapwapMsgStruct->WlcHdlrQueueReq.u4MsgType =
                WSS_CAPWAP_802_11_MGMT_PKT;

            if (WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_CAPWAP_MAC_MSG,
                                        pCapwapMsgStruct) != WSSMAC_SUCCESS)
            {
                UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                UtlShMemFreeWlcBuf ((UINT1 *) pCapwapMsgStruct);
                WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pu1Buf);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pDisassocMsgStruct);
                return WSSMAC_FAILURE;
            }
        }
        else
        {
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            UtlShMemFreeWlcBuf ((UINT1 *) pCapwapMsgStruct);
            WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pu1Buf);
            UtlShMemFreeMacMsgStructBuf ((UINT1 *) pDisassocMsgStruct);
            return WSSMAC_FAILURE;
        }
    }
    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
    UtlShMemFreeWlcBuf ((UINT1 *) pCapwapMsgStruct);
    WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pu1Buf);
    UtlShMemFreeMacMsgStructBuf ((UINT1 *) pDisassocMsgStruct);
    return WSSMAC_SUCCESS;
}

/* ***************************************************************************
 *                                                                           *
 * Function     : WssMacProcessDataMsg                                       *
 *                                                                           *
 * Description  : Process Data frame                                         *
 *                                                                           *
 * Input(s)     : MAC message type and union of struct with CRU_BUF          *
 *                                                                           *
 * Output(s)    : NONE                                                       *
 *                                                                           *
 * Returns      : SUCCESS/FAILURE                                            *
 *                                                                           *
 * ***************************************************************************/
UINT1
WssMacProcessDataMsg (UINT1 eMsgType, tWssMacMsgStruct * pMsgStruct)
{
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;
    UINT1              *pu1ReadPtr = NULL;
    UINT1               u1IsNotLinear = OSIX_FALSE;
    tCRU_BUF_CHAIN_DESC *pBuf = NULL;
    UINT2               u2PacketLen = 0;
    UINT4               u4Len = 0;

    pWlcHdlrMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pWlcHdlrMsgStruct == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                    "WssMacProcessDataMsg:- "
                    "UtlShMemAllocWlcBuf returned failure\n");
        return WSSMAC_FAILURE;
    }

    MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));
    UNUSED_PARAM (eMsgType);

    pBuf = pMsgStruct->unMacMsg.MacDot11PktBuf.pDot11MacPdu;
    u2PacketLen = (UINT2) WSSMAC_GET_BUF_LEN (pBuf);
    pu1ReadPtr = WSSMAC_BUF_IF_LINEAR (pBuf, 0, u2PacketLen);

    if (pu1ReadPtr == NULL)
    {
        WLCHDLR_PKTBUF_ALLOC_MEM_BLOCK (pu1ReadPtr);
        if (pu1ReadPtr == NULL)
        {
            WSSMAC_TRC (WSSMAC_FAILURE_TRC, "pu1ReadPtr"
                        ":: Memory Allocation Failed\r\n");
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            return OSIX_FAILURE;
        }
        WSSMAC_COPY_FROM_BUF (pBuf, pu1ReadPtr, 0, u2PacketLen);
        u1IsNotLinear = OSIX_TRUE;
    }

    if (WSS_MAC_SPLITMAC_MODE == pMsgStruct->unMacMsg.MacDot11PktBuf.u1MacType)
    {
        /* Convert 802.11 to 802.3 */
        if (WssMacDot11ToDot3 (&pu1ReadPtr, &u2PacketLen) != WSSMAC_SUCCESS)
        {
            WSSMAC_TRC (WSSMAC_FAILURE_TRC, "WssMacProcessDataMsg:\
                    WssMacDot11ToDot3 FAILED \r\n");

            if (u1IsNotLinear == OSIX_TRUE)
            {
                WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pu1ReadPtr);
            }
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            return WSSMAC_FAILURE;
        }

        u4Len = (UINT4) u2PacketLen;
        pWlcHdlrMsgStruct->WssMacMsgStruct.unMacMsg.MacDot11PktBuf.pDot11MacPdu
            = WSSMAC_ALLOCATE_CRU_BUF (u4Len);
        if (pWlcHdlrMsgStruct->WssMacMsgStruct.unMacMsg.MacDot11PktBuf.
            pDot11MacPdu == NULL)
        {
            WSSMAC_TRC (WSSMAC_FAILURE_TRC, "WssMacProcessDataMsg :: "
                        "Memory Allocation Failure \r\n");
            if (u1IsNotLinear == OSIX_TRUE)
            {
                WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pu1ReadPtr);
            }
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            return WSSMAC_FAILURE;
        }

        pWlcHdlrMsgStruct->WssMacMsgStruct.unMacMsg.MacDot11PktBuf.u4IfIndex =
            pMsgStruct->unMacMsg.MacDot11PktBuf.u4IfIndex;
        pWlcHdlrMsgStruct->WssMacMsgStruct.unMacMsg.MacDot11PktBuf.u2SessId =
            pMsgStruct->unMacMsg.MacDot11PktBuf.u2SessId;
        pWlcHdlrMsgStruct->WssMacMsgStruct.unMacMsg.MacDot11PktBuf.u2VlanId =
            pMsgStruct->unMacMsg.MacDot11PktBuf.u2VlanId;
        pWlcHdlrMsgStruct->WssMacMsgStruct.unMacMsg.MacDot11PktBuf.u1MacType =
            pMsgStruct->unMacMsg.MacDot11PktBuf.u1MacType;
        MEMCPY (pWlcHdlrMsgStruct->WssMacMsgStruct.unMacMsg.MacDot11PktBuf.
                BssIdMac, pMsgStruct->unMacMsg.MacDot11PktBuf.BssIdMac,
                sizeof (tMacAddr));
        /*
           memcpy(&(pWlcHdlrMsgStruct->WssMacMsgStruct.unMacMsg.MacDot11PktBuf),
           &pMsgStruct->unMacMsg.MacDot11PktBuf, 
           sizeof(tWssMacDot11PktBuf));
         */
        /* 802.3 packet is ready, send it out on VLAN interface */
        WSSMAC_COPY_TO_BUF (pWlcHdlrMsgStruct->WssMacMsgStruct.unMacMsg.
                            MacDot11PktBuf.pDot11MacPdu,
                            pu1ReadPtr, 0, u2PacketLen);
        WSSMAC_RELEASE_CRU_BUF (pMsgStruct->unMacMsg.MacDot11PktBuf.
                                pDot11MacPdu);
    }
    else
    {
        memcpy (&(pWlcHdlrMsgStruct->WssMacMsgStruct.unMacMsg.MacDot11PktBuf),
                &pMsgStruct->unMacMsg.MacDot11PktBuf,
                sizeof (tWssMacDot11PktBuf));
    }

    if (u1IsNotLinear == OSIX_TRUE)
    {
        WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pu1ReadPtr);
    }

    if (WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_CFA_TX_BUF, pWlcHdlrMsgStruct) ==
        WSSMAC_FAILURE)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC, "WSS_WLCHDLR_CFA_TX_BUF Failed \r\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return WSSMAC_FAILURE;
    }
    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
    return WSSMAC_SUCCESS;
}

/* ***************************************************************************
 *                                                                           *
 * Function     : WssMacProcessCfaMsg                                        *
 *                                                                           *
 * Description  : Process the CFA Msg from WLC Hdlr                          *
 *                                                                           *
 * Input(s)     : MAC message type and union of struct with CRU_BUF          *
 *                                                                           *
 * Output(s)    : Data message buffer                                        *
 *                                                                           *
 * Returns      : SUCCESS/FAILURE                                            *
 *                                                                           *
 * ***************************************************************************/
UINT1
WssMacProcessCfaMsg (UINT1 eMsgType, tWssMacMsgStruct * pMsgStruct)
{
    unWlcHdlrMsgStruct *pCapwapMsgStruct = NULL;
    tWssWlanDB         *pwssWlanDB = NULL;
    tRadioIfGetDB       RadioIfGetDB;
    UINT4               u4Len = 0;
    UINT2               u2PacketLen = 0;
    UINT1              *pu1ReadPtr = NULL;
    tCRU_BUF_CHAIN_DESC *pRcvBuf = NULL;
    UINT1              *pu1Buf = NULL;
    BOOL1               bFlagReleaseBuf = OSIX_FALSE;
#ifdef DEBUG_WANTED
    UINT1              *pu1ReadTempPtr = NULL;
    tCRU_BUF_CHAIN_DESC *pBuf = NULL;
    UINT2               u2arp = 0;
#endif

    pCapwapMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pCapwapMsgStruct == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                    "WssMacProcessCfaMsg:- "
                    "UtlShMemAllocWlcBuf returned failure\n");
        return WSSMAC_FAILURE;
    }

    pwssWlanDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
    if (pwssWlanDB == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                    "WssMacProcessCfaMsg:- "
                    "UtlShMemAllocWlanDbBuf returned failure\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pCapwapMsgStruct);
        return OSIX_FAILURE;
    }
    WLCHDLR_PKTBUF_ALLOC_MEM_BLOCK (pu1Buf);
    if (pu1Buf == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                    "WssMacProcessCfaMsg:-"
                    "UtlShMemAllocMacMsgStructBuf returned failure\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pwssWlanDB);
        UtlShMemFreeWlcBuf ((UINT1 *) pCapwapMsgStruct);
        return WSSMAC_FAILURE;
    }

    MEMSET (pCapwapMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));
    MEMSET (pwssWlanDB, 0, sizeof (tWssWlanDB));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    UNUSED_PARAM (eMsgType);

#ifdef DEBUG_WANTED
    pBuf = pMsgStruct->unMacMsg.MacDot11PktBuf.pDot11MacPdu;

    u2PacketLen = (UINT2) WSSMAC_GET_BUF_LEN (pBuf);
    pu1ReadTempPtr = WSSMAC_BUF_IF_LINEAR (pBuf, 0, u2PacketLen);

    if (pu1ReadTempPtr == NULL)
    {
        UtlShMemFreeWlcBuf ((UINT1 *) pCapwapMsgStruct);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pu1Buf);
        return OSIX_SUCCESS;
    }
    MEMCPY (&u2arp, &pu1ReadTempPtr[12], 2);

    if (u2arp == 0x800)
    {
        /* TCP Packets not required to be CAPWAP */
        if (pu1ReadTempPtr[14 + 9] == 0x06)
        {
            UINT4               u4SrcIP = 0;
            UINT1               au1SrcMac[MAC_ADDR_LEN];

            MEMCPY (&u4SrcIP, &pu1ReadTempPtr[14 + 12], sizeof (UINT4));

            RBTreeWalk (gWssStaWepProcessDB, (tRBWalkFn) WssStaGetClientMac,
                        &u4SrcIP, &au1SrcMac);

            if (ArpResolve (u4SrcIP, (INT1 *) au1SrcMac, &u1EncapType) ==
                OSIX_SUCCESS)
            {
                pWssStaWepProcessDB = WssStaProcessEntryGet (au1SrcMac);
                if (pWssStaWepProcessDB != NULL)
                {
                    UtlShMemFreeWlcBuf ((UINT1 *) pCapwapMsgStruct);
                    UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
                    WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pu1Buf);
                    return OSIX_SUCCESS;
                }
            }
        }
    }
#endif
    /*The IF index is passed through Vlan */
    pCapwapMsgStruct->WlcHdlrQueueReq.u4IfIndex =
        (UINT4) pMsgStruct->unMacMsg.MacDot11PktBuf.u2VlanId;

    pwssWlanDB->WssWlanAttributeDB.u4BssIfIndex =
        pMsgStruct->unMacMsg.MacDot11PktBuf.u2VlanId;

    pwssWlanDB->WssWlanIsPresentDB.bBssId = OSIX_TRUE;
    pwssWlanDB->WssWlanIsPresentDB.bWlanInternalId = OSIX_TRUE;
    pwssWlanDB->WssWlanIsPresentDB.bRadioIfIndex = OSIX_TRUE;

    if (OSIX_SUCCESS !=
        WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_IFINDEX_ENTRY, pwssWlanDB))
    {
        WSSMAC_TRC2 (WSSMAC_FAILURE_TRC, "WssMacProcessCfaMsg:\
                WSS_WLAN_GET_BSS_IFINDEX_ENTRY Failed - %d %d\r\n", pwssWlanDB->WssWlanAttributeDB.u4BssIfIndex, pMsgStruct->unMacMsg.MacDot11PktBuf.u2VlanId);
        UtlShMemFreeWlcBuf ((UINT1 *) pCapwapMsgStruct);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pu1Buf);
        return OSIX_FAILURE;
    }

    RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
        UtlShMemAllocAntennaSelectionBuf ();

    if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC, "\r%% Memory allocation for Antenna"
                    " selection failed.\r\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pCapwapMsgStruct);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pu1Buf);
        return OSIX_FAILURE;
    }
    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
        pwssWlanDB->WssWlanAttributeDB.u4RadioIfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                  &RadioIfGetDB) != OSIX_SUCCESS)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC, "WssMacProcessCfaMsg:\
                WSS_GET_RADIO_IF_DB Failed \r\n");
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                         pu1AntennaSelection);
        UtlShMemFreeWlcBuf ((UINT1 *) pCapwapMsgStruct);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pu1Buf);
        return OSIX_FAILURE;
    }
    pCapwapMsgStruct->WlcHdlrQueueReq.u2SessId =
        RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;

    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                     pu1AntennaSelection);
    MEMCPY (&(pCapwapMsgStruct->WlcHdlrQueueReq.BssId),
            &(pwssWlanDB->WssWlanAttributeDB.BssId), sizeof (tMacAddr));
    pwssWlanDB->WssWlanIsPresentDB.bWlanMacType = OSIX_TRUE;
    if (OSIX_SUCCESS != WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_PROFILE_ENTRY,
                                                  pwssWlanDB))
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC, "WssMacProcessCfaMsg:\
                WSS_WLAN_GET_PROFILE_ENTRY Failed \r\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pCapwapMsgStruct);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pu1Buf);
        return OSIX_FAILURE;
    }
    if (WSS_MAC_SPLITMAC_MODE == pwssWlanDB->WssWlanAttributeDB.u1MacType)
    {
        u2PacketLen =
            (UINT2) WSSMAC_GET_BUF_LEN (pMsgStruct->unMacMsg.MacDot11PktBuf.
                                        pDot11MacPdu);
        MEMSET (pu1Buf, 0x00, u2PacketLen);
        WSSMAC_COPY_FROM_BUF (pMsgStruct->unMacMsg.MacDot11PktBuf.pDot11MacPdu,
                              pu1Buf, 0, u2PacketLen);
        pu1ReadPtr = pu1Buf;
        if (WssMacDot3ToDot11 (&pu1ReadPtr,
                               pwssWlanDB->WssWlanAttributeDB.BssId,
                               &u2PacketLen) != OSIX_SUCCESS)
        {
            WSSMAC_TRC (WSSMAC_FAILURE_TRC, "WssMacProcessCfaMsg:\
                    WssMacDot3ToDot11 Failed \r\n");
            UtlShMemFreeWlcBuf ((UINT1 *) pCapwapMsgStruct);
            UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
            WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pu1Buf);
            return OSIX_FAILURE;
        }
        u4Len = (UINT4) u2PacketLen;
        pRcvBuf = WSSMAC_ALLOCATE_CRU_BUF (u4Len);
        if (NULL == pRcvBuf)
        {
            WSSMAC_TRC (WSSMAC_FAILURE_TRC, "WssMacProcessDataMsg :: "
                        "Memory Allocation Failure \r\n");
            UtlShMemFreeWlcBuf ((UINT1 *) pCapwapMsgStruct);
            UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
            WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pu1Buf);
            return OSIX_FAILURE;
        }
        WSSMAC_COPY_TO_BUF (pRcvBuf, pu1ReadPtr, 0, u4Len);
        pCapwapMsgStruct->WlcHdlrQueueReq.pRcvBuf = pRcvBuf;
        pCapwapMsgStruct->WlcHdlrQueueReq.u1MacType = 1;
        bFlagReleaseBuf = OSIX_TRUE;
    }
    else
    {
        pCapwapMsgStruct->WlcHdlrQueueReq.pRcvBuf =
            pMsgStruct->unMacMsg.MacDot11PktBuf.pDot11MacPdu;
        pCapwapMsgStruct->WlcHdlrQueueReq.u1MacType = 0;
    }

    pCapwapMsgStruct->WlcHdlrQueueReq.u4MsgType = WSS_CAPWAP_802_11_DATA_PKT;

    if (WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_CAPWAP_MAC_MSG,
                                pCapwapMsgStruct) != WSSMAC_SUCCESS)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                    "WSS_WLCHDLR_CAPWAP_MAC_MSG Failed \r\n");
        if (bFlagReleaseBuf == OSIX_TRUE)
        {
            WSSMAC_RELEASE_CRU_BUF (pRcvBuf);
        }
        UtlShMemFreeWlcBuf ((UINT1 *) pCapwapMsgStruct);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pu1Buf);
        return OSIX_FAILURE;
    }
    if (bFlagReleaseBuf == OSIX_TRUE)
    {
        WSSMAC_RELEASE_CRU_BUF (pRcvBuf);
    }
    UtlShMemFreeWlcBuf ((UINT1 *) pCapwapMsgStruct);
    UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
    WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pu1Buf);
    return OSIX_SUCCESS;
}

/* ***************************************************************************
 *                                                                           *
 * Function     : WssMacAssembleDataFrame                                    *
 *                                                                           *
 * Description  : Assemble Data frame                                        *
 *                                                                           *
 * Input(s)     : Union of struct with Data frame buffer                     *
 *                                                                           *
 * Output(s)    : Data message buffer                                        *
 *                                                                           *
 * Returns      : NONE                                                       *
 *                                                                           *
 * ***************************************************************************/
VOID
WssMacAssembleDataFrame (tWssMacMsgStruct * pMsgStruct,
                         tWssMacMsgStruct * pMsgStructDataBuf)
{
    UINT4               u4Len = 0;

    u4Len =
        WSSMAC_GET_BUF_LEN (pMsgStruct->unMacMsg.MacDot11PktBuf.pDot11MacPdu);
    pMsgStructDataBuf->unMacMsg.MacDot11PktBuf.pDot11MacPdu =
        WSSMAC_ALLOCATE_CRU_BUF (u4Len);

    if (pMsgStructDataBuf->unMacMsg.MacDot11PktBuf.pDot11MacPdu == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC, "Memory Allocation Failed \r\n");
        return;
    }

    WSSMAC_COPY_TO_BUF (pMsgStructDataBuf->unMacMsg.MacDot11PktBuf.pDot11MacPdu,
                        pMsgStruct->unMacMsg.MacDot11PktBuf.pDot11MacPdu,
                        0, u4Len);
    return;
}

/* Msg Functions not applicable for WLC */
/* ***************************************************************************
 *                                                                           *
 * Function     : WssMacProcessProbeResp                                     *
 *                                                                           *
 * Description  : Unused function for WLC                                    *
 *                                                                           *
 * Input(s)     : Union of struct with Data frame buffer                     *
 *                                                                           *
 * Output(s)    : Data message buffer                                        *
 *                                                                           *
 * Returns      : WSSMAC_SUCCESS                                             *
 *                                                                           *
 * ***************************************************************************/
UINT1
WssMacProcessProbeResp (UINT1 eMsgType, tWssMacMsgStruct * pMsgStruct)
{
    UNUSED_PARAM (eMsgType);
    UNUSED_PARAM (pMsgStruct);

    return WSSMAC_SUCCESS;
}

/* ***************************************************************************
 *                                                                           *
 * Function     : WssMacConstructProbeRsp                                    *
 *                                                                           *
 * Description  : Unused function for WLC                                    *
 *                                                                           *
 * Input(s)     : Union of struct with Data frame buffer                     *
 *                                                                           *
 * Output(s)    : Data message buffer                                        *
 *                                                                           *
 * Returns      : WSSMAC_SUCCESS                                             *
 *                                                                           *
 * ***************************************************************************/
UINT1
WssMacConstructProbeRsp (tWssMacMsgStruct * pProbeReqStruct,
                         tWssMacMsgStruct * pProbeRspStruct)
{
    UNUSED_PARAM (pProbeReqStruct);
    UNUSED_PARAM (pProbeRspStruct);

    return WSSMAC_SUCCESS;
}

/* ***************************************************************************
 *                                                                           *
 * Function     : WssMacProcessBeaconMsg                                     *
 *                                                                           *
 * Description  : Unused function for WLC                                    *
 *                                                                           *
 * Input(s)     : Union of struct with Data frame buffer                     *
 *                                                                           *
 * Output(s)    : Data message buffer                                        *
 *                                                                           *
 * Returns      : WSSMAC_SUCCESS                                             *
 *                                                                           *
 * ***************************************************************************/
UINT1
WssMacProcessBeaconMsg (UINT1 eMsgType, tWssMacMsgStruct * pMsgStruct)
{
    UNUSED_PARAM (eMsgType);
    UNUSED_PARAM (pMsgStruct);

    return WSSMAC_SUCCESS;
}

/* ***************************************************************************
 *                                                                           *
 * Function     : WlcHdlrPostPktFromWss                                      *
 *                                                                           *
 * Description  : Unused function for WLC                                    *
 *                                                                           *
 * Input(s)     : Union of struct with Data frame buffer                     *
 *                                                                           *
 * Output(s)    : Data message buffer                                        *
 *                                                                           *
 * Returns      : WSSMAC_SUCCESS                                             *
 *                                                                           *
 * ***************************************************************************/
UINT1
WlcHdlrPostPktFromWss (UINT1 eMsgType, tWssMacMsgStruct * pMsgStruct)
{
    UNUSED_PARAM (eMsgType);
    UNUSED_PARAM (pMsgStruct);

    return WSSMAC_SUCCESS;
}

/* ***************************************************************************
 *                                                                           *
 * Function     : WssMacProcessActionMsg                                     *
 *                                                                           *
 * Description  : Processes the Action frames                                *
 *                                                                           *
 * Input(s)     : MAC message type and union of struct with CRU_BUF          *
 *                                                                           *
 * Output(s)    : NULL                                           *
 *                                                                           *
 * Returns      : SUCCESS/FAILURE                                            *
 *                                                                           *
 * ***************************************************************************/
UINT1
WssMacProcessActionMsg (UINT1 eMsgType, tWssMacMsgStruct * pMsgStruct)
{
    tWssMacMgmtFrmHdr   MacMgmtFrmHdr;
    tCRU_BUF_CHAIN_DESC *pBuf = NULL;
    UINT2               u2PacketLen = 0;
    UINT1              *pu1ReadPtr = NULL;
    UINT1               u1CategoryCode = 0;
    UINT1               u1RetVal = 0;
    UINT1              *pu1Buf = NULL;
#ifdef PMF_WANTED
    UINT2               u2SessId;
#endif

    UNUSED_PARAM (eMsgType);
    MEMSET (&MacMgmtFrmHdr, 0, sizeof (tWssMacMgmtFrmHdr));

    WLCHDLR_PKTBUF_ALLOC_MEM_BLOCK (pu1Buf);
    if (pu1Buf == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                    "WssMacProcessActionMsg:-"
                    "UtlShMemAllocMacMsgStructBuf returned failure\n");
        return WSSMAC_FAILURE;
    }
    pBuf = pMsgStruct->unMacMsg.MacDot11PktBuf.pDot11MacPdu;
#ifdef PMF_WANTED
    u2SessId = pMsgStruct->unMacMsg.MacDot11PktBuf.u2SessId;
#endif

    u2PacketLen = (UINT2) WSSMAC_GET_BUF_LEN (pBuf);
    pu1ReadPtr = WSSMAC_BUF_IF_LINEAR (pBuf, 0, u2PacketLen);

    if (pu1ReadPtr == NULL)
    {
        MEMSET (pu1Buf, 0x00, u2PacketLen);
        WSSMAC_COPY_FROM_BUF (pBuf, pu1Buf, 0, u2PacketLen);
        pu1ReadPtr = pu1Buf;
    }

    /* Get the 802.11 MAC Header */
    WSSMAC_GET_2BYTES (MacMgmtFrmHdr.u2MacFrameCtrl, pu1ReadPtr);    /* Get Frame control field */
    WSSMAC_GET_2BYTES (MacMgmtFrmHdr.u2MacFrameDurId, pu1ReadPtr);    /* Get Duration ID field */
    WSSMAC_GET_NBYTES (MacMgmtFrmHdr.u1DA, pu1ReadPtr, WSSMAC_MAC_ADDR_LEN);    /* Get Address1 field */
    WSSMAC_GET_NBYTES (MacMgmtFrmHdr.u1SA, pu1ReadPtr, WSSMAC_MAC_ADDR_LEN);    /* Get Address2 field */
    WSSMAC_GET_NBYTES (MacMgmtFrmHdr.u1BssId, pu1ReadPtr, WSSMAC_MAC_ADDR_LEN);    /* Get Address3 field */
    WSSMAC_GET_2BYTES (MacMgmtFrmHdr.u2MacFrameSeqCtrl, pu1ReadPtr);    /* Get Sequence control field */

    WSSMAC_GET_1BYTE (u1CategoryCode, pu1ReadPtr);

    switch (u1CategoryCode)
    {
        case DOT11_SPECTRUM_MGMT:
            u1RetVal = WssMacProcessSpectrumMgmtframe
                (&MacMgmtFrmHdr, &pu1ReadPtr);
            break;
#ifdef RSNA_WANTED
#ifdef PMF_WANTED
        case DOT11_SA_QUERY:
            u1RetVal =
                WssMacProcessSAQueryMgmtFrame (&MacMgmtFrmHdr, &pu1ReadPtr,
                                               u2SessId);
            break;
#endif
#endif
        default:
        {
            WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                        "WssMacProcessActionMsg :"
                        "unknown category code received\n");
            WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pu1Buf);
            return WSSMAC_FAILURE;
        }
    }
    if (u1RetVal != OSIX_SUCCESS)

    {
        WLCHDLR_PKTBUF_RELEASE_MEM_BLOCK (pu1Buf);
        return WSSMAC_FAILURE;
    }
    MemReleaseMemBlock (WLCHDLR_PKTBUF_POOLID, pu1Buf);
    return WSSMAC_SUCCESS;
}

/* ***************************************************************************
 *                                                                           *
 * Function     : WssMacProcessSpectrumMgmtframe                             *
 *                                                                           *
 * Description  : Processes the Spectrum Management frames                   *
 *                                                                           *
 * Input(s)     : MAC message type and union of struct with CRU_BUF          *
 *                                                                           *
 * Output(s)    : Association request structure                              *
 *                                                                           *
 * Returns      : SUCCESS/FAILURE                                            *
 *                                                                           *
 * ***************************************************************************/
UINT1
WssMacProcessSpectrumMgmtframe (tWssMacMgmtFrmHdr * pMacMgmtFrmHdr,
                                UINT1 **pu1ReadPtr)
{
    tWssWlanDB         *pwssWlanDB = NULL;
    tRfMgmtMsgStruct    RfMgmtMsgStruct;
    INT1                i1LinkMargin = 0;
    INT1                i1TxPower = 0;
    UINT1               u1ActionField = 0;
    UINT1               u1RetVal = 0;
    UINT1               u1ChannelNum = 0;
    UINT1               u1MapField = 0;
    BOOL1               isRadarPresent = OSIX_FALSE;

    MEMSET (&RfMgmtMsgStruct, 0, sizeof (tRfMgmtMsgStruct));
    pwssWlanDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();

    if (pwssWlanDB == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                    "WssMacProcessSpectrumMgmtframe"
                    "UtlShMemAllocWlanDbBuf returned failure\n");
        return WSSMAC_FAILURE;
    }

    MEMSET (pwssWlanDB, 0, sizeof (tWssWlanDB));

    /*Get the Action Field */
    WSSMAC_GET_1BYTE (u1ActionField, *pu1ReadPtr);

    switch (u1ActionField)
    {
        case DOT11_TPC_REPORT:
            /*Get the RadioifIndex from BSSID */
            MEMCPY (pwssWlanDB->WssWlanAttributeDB.BssId,
                    pMacMgmtFrmHdr->u1BssId, WSSMAC_MAC_ADDR_LEN);

            pwssWlanDB->WssWlanIsPresentDB.bBssIfIndex = OSIX_TRUE;
            if (OSIX_SUCCESS !=
                WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSSID_MAPPING_ENTRY,
                                          pwssWlanDB))
            {
                WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                            "WssMacProcessSpectrumMgmtframe"
                            "WssIfProcessWssWlanDBMsg returned failure\n");
                UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
                return WSSMAC_FAILURE;
            }
            WSSMAC_SKIP_NBYTES (VALUE_3, *pu1ReadPtr);
            /*Get the Link Margin and TxPower */
            WSSMAC_GET_1BYTE (i1TxPower, *pu1ReadPtr);
            WSSMAC_GET_1BYTE (i1LinkMargin, *pu1ReadPtr);

            /*Copy the contents and post it to RF module */
            RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtQueueReq.u4MsgType =
                RFMGMT_TPC_RSP_MSG;

            MEMCPY (RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtQueueReq.TpcRspInfo.
                    StaMac, pMacMgmtFrmHdr->u1SA, WSSMAC_MAC_ADDR_LEN);
            RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtQueueReq.TpcRspInfo.
                u4RadioIfIndex = pwssWlanDB->WssWlanAttributeDB.u4RadioIfIndex;
            RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtQueueReq.TpcRspInfo.i1TxPower =
                i1TxPower;
            RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtQueueReq.TpcRspInfo.i1LinkMargin =
                i1LinkMargin;

            u1RetVal = (UINT1) RfMgmtWlcEnquePkts (&RfMgmtMsgStruct);
            break;

        case DOT11_MEAS_REPORT:
            /*Get the RadioifIndex from BSSID */
            MEMCPY (pwssWlanDB->WssWlanAttributeDB.BssId,
                    pMacMgmtFrmHdr->u1BssId, WSSMAC_MAC_ADDR_LEN);

            pwssWlanDB->WssWlanIsPresentDB.bBssIfIndex = OSIX_TRUE;
            if (OSIX_SUCCESS !=
                WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSSID_MAPPING_ENTRY,
                                          pwssWlanDB))
            {
                WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                            "WssMacProcessSpectrumMgmtframe"
                            "WssIfProcessWssWlanDBMsg returned failure\n");
                UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
                return WSSMAC_FAILURE;
            }
            WSSMAC_SKIP_NBYTES (1, *pu1ReadPtr);
            /*Get the Channel Num */
            WSSMAC_GET_1BYTE (u1ChannelNum, *pu1ReadPtr);
            WSSMAC_SKIP_NBYTES (10, *pu1ReadPtr);
            /*Get the Map Field */
            WSSMAC_GET_1BYTE (u1MapField, *pu1ReadPtr);
            isRadarPresent = u1MapField & 0x08;
            RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtQueueReq.u4MsgType =
                RFMGMT_MEAS_REPORT_MSG;
            MEMCPY (RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtQueueReq.MeasRep.
                    StationMacAddr, pMacMgmtFrmHdr->u1SA, WSSMAC_MAC_ADDR_LEN);
            RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtQueueReq.MeasRep.u4RadioIfIndex =
                pwssWlanDB->WssWlanAttributeDB.u4RadioIfIndex;
            RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtQueueReq.MeasRep.u1ChannelNum =
                u1ChannelNum;
            RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtQueueReq.MeasRep.isRadarPresent =
                isRadarPresent;

            u1RetVal = RfMgmtWlcEnquePkts (&RfMgmtMsgStruct);
            break;

        default:
            WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                        "WssMacProcessSpectrumMgmtframe"
                        "Invalid Action Filed received\n");
            break;
    }
    UNUSED_PARAM (u1RetVal);
    UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
    return WSSMAC_SUCCESS;

}

/***************************************************************************
 *                                                                           *
 * Function     : WssMacProcessChanlSwitAnnounce                             *
 *                                                                           *
 * Description  : Process the Channel Switch announcement frames             *
 *                                                                           *
 * Input(s)     : MAC message type and union of struct with CRU_BUF          *
 *                                                                           *
 * Output(s)    : None                                                       *
 *                                                                           *
 * Returns      : SUCCESS                                                    *
 *                                                                           *
 ***************************************************************************/
UINT1
WssMacProcessChanlSwitAnnounce (UINT1 eMsgType, tWssMacMsgStruct * pMsgStruct)
{
    UNUSED_PARAM (eMsgType);
    UNUSED_PARAM (pMsgStruct);

    return OSIX_SUCCESS;
}

/* ***************************************************************************
 *                                                                           *
 * Function     : WssMacProcessExChanlSwitAnnounce                           *
 *                                                                           *
 * Description  : Process the External Channel Switch announcement frames    *
 *                                                                           *
 * Input(s)     : MAC message type and union of struct with CRU_BUF          *
 *                                                                           *
 * Output(s)    : None                                                       *
 *                                                                           *
 * Returns      : SUCCESS/FAILURE                                            *
 *                                                                           *
 ** ***************************************************************************/
UINT1
WssMacProcessExChanlSwitAnnounce (UINT1 eMsgType, tWssMacMsgStruct * pMsgStruct)
{
    UNUSED_PARAM (eMsgType);
    UNUSED_PARAM (pMsgStruct);
    return OSIX_SUCCESS;
}

/* ***************************************************************************
 *                                                                           *
 * Function     : WssMacProcessTpcRequest                                    *
 *                                                                           *
 * Description  : Process the CFA Msg from AP Hdlr                           *
 *                                                                           *
 * Input(s)     : MAC message type and union of struct with CRU_BUF          *
 *                                                                           *
 * Output(s)    : None                                                       *
 *                                                                           *
 * Returns      : SUCCESS/FAILURE                                            *
 *                                                                           *
 * ***************************************************************************/
UINT1
WssMacProcessTpcRequest (UINT1 eMsgType, tWssMacMsgStruct * pMsgStruct)
{
    /*Stubs have been added for WLC side */
    UNUSED_PARAM (eMsgType);
    UNUSED_PARAM (pMsgStruct);
    return WSSMAC_SUCCESS;
}

/***************************************************************************
 * Function     : WssMacProcessMeasRequest                                   *
 *                                                                           *
 * Description  : Process the MeasReq                                        *
 *                                                                           *
 * Input(s)     : MAC message type and union of struct with CRU_BUF          *
 *                                                                           *
 * Output(s)    : None                                                       *
 *                                                                           *
 * Returns      : SUCCESS                                                    *
 *                                                                           *
 ***************************************************************************/
UINT1
WssMacProcessMeasRequest (UINT1 eMsgType, tWssMacMsgStruct * pMsgStruct)
{
    UNUSED_PARAM (eMsgType);
    UNUSED_PARAM (pMsgStruct);
    return WSSMAC_SUCCESS;
}

#ifdef RSNA_WANTED
#ifdef PMF_WANTED
/* ***************************************************************************
 *                                                                           *
 * Function     : WssMacProcessSAQueryMgmtFrame                              *
 *                                                                           *
 * Description  : Processes the SA Query Management frames                   *
 *                                                                           *
 * Input(s)     : MAC message type and union of struct with CRU_BUF          *
 *                                                                           *
 * Output(s)    : None                                                       *
 *                                                                           *
 * Returns      : SUCCESS/FAILURE                                            *
 *                                                                           *
 * ***************************************************************************/
UINT1
WssMacProcessSAQueryMgmtFrame (tWssMacMgmtFrmHdr * pMacMgmtFrmHdr,
                               UINT1 **pu1ReadPtr, UINT2 u2SessId)
{
    UINT1               u1ActionField = 0;
    tWssWlanDB         *pwssWlanDB = NULL;
    tWssRSNANotifyParams WssRSNANotifyParams;

    pwssWlanDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();

    if (pwssWlanDB == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                    "WssMacProcessSpectrumMgmtframe"
                    "UtlShMemAllocWlanDbBuf returned failure\n");
        return WSSMAC_FAILURE;
    }

    MEMSET (pwssWlanDB, 0, sizeof (tWssWlanDB));
    MEMSET (&WssRSNANotifyParams, 0, sizeof (tWssRSNANotifyParams));

    /*Get the Action Field */
    WSSMAC_GET_1BYTE (u1ActionField, *pu1ReadPtr);

    switch (u1ActionField)
    {
        case DOT11_SA_QUERY_REQ:

            /*Get the BssifIndex from BSSID */

            MEMCPY (pwssWlanDB->WssWlanAttributeDB.BssId,
                    pMacMgmtFrmHdr->u1BssId, WSSMAC_MAC_ADDR_LEN);

            pwssWlanDB->WssWlanIsPresentDB.bBssIfIndex = OSIX_TRUE;

            if ((WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSSID_MAPPING_ENTRY,
                                           pwssWlanDB)) != OSIX_SUCCESS)
            {
                WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                            "WssMacProcessSpectrumMgmtframe"
                            "WssIfProcessWssWlanDBMsg returned failure\n");
                UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
                return WSSMAC_FAILURE;
            }

            MEMSET (WssRSNANotifyParams.MLMESAQUERYIND.au1StaMac, 0, 6);
            MEMCPY (WssRSNANotifyParams.MLMESAQUERYIND.au1StaMac,
                    pMacMgmtFrmHdr->u1SA, 6);

            MEMCPY (&WssRSNANotifyParams.MLMESAQUERYIND.u2TransactionId,
                    *pu1ReadPtr, 2);
            WssRSNANotifyParams.MLMESAQUERYIND.u2SessId = u2SessId;

            WssRSNANotifyParams.u4IfIndex =
                pwssWlanDB->WssWlanAttributeDB.u4BssIfIndex;
            WssRSNANotifyParams.eWssRsnaNotifyType = WSSRSNA_SAQUERY_REQ_IND;

            /* Send Notification msg to Rsna */
            if (RsnaApiSAQueryInd (&WssRSNANotifyParams) == OSIX_FAILURE)
            {
                WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                            "WssStaProcessAssocFrames:- RsnaProcessWssNotification"
                            "returned Failure !!!! \n");
                UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
                return (OSIX_FAILURE);
            }

            break;

        case DOT11_SA_QUERY_RESP:
            /*Get the BssifIndex from BSSID */

            MEMCPY (pwssWlanDB->WssWlanAttributeDB.BssId,
                    pMacMgmtFrmHdr->u1BssId, WSSMAC_MAC_ADDR_LEN);

            pwssWlanDB->WssWlanIsPresentDB.bBssIfIndex = OSIX_TRUE;

            if ((WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSSID_MAPPING_ENTRY,
                                           pwssWlanDB)) != OSIX_SUCCESS)
            {
                WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                            "WssMacProcessSpectrumMgmtframe"
                            "WssIfProcessWssWlanDBMsg returned failure\n");
                UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
                return WSSMAC_FAILURE;
            }

            MEMSET (WssRSNANotifyParams.MLMESAQUERYIND.au1StaMac, 0, 6);
            MEMCPY (WssRSNANotifyParams.MLMESAQUERYIND.au1StaMac,
                    pMacMgmtFrmHdr->u1SA, 6);

            MEMCPY (&WssRSNANotifyParams.MLMESAQUERYIND.u2TransactionId,
                    *pu1ReadPtr, 2);

            WssRSNANotifyParams.u4IfIndex =
                pwssWlanDB->WssWlanAttributeDB.u4BssIfIndex;
            WssRSNANotifyParams.eWssRsnaNotifyType = WSSRSNA_SAQUERY_RESP_IND;

            /* Send Notification msg to Rsna */
            if (RsnaApiSAQueryInd (&WssRSNANotifyParams) == OSIX_FAILURE)
            {
                WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                            "WssStaProcessAssocFrames:- RsnaProcessWssNotification"
                            "returned Failure !!!! \n");
                UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
                return (OSIX_FAILURE);
            }
            break;
        default:
            WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                        "WssMacProcessSpectrumMgmtframe"
                        "Invalid Action Filed received\n");
            break;
    }
    UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
    return WSSMAC_SUCCESS;

}

/* ***************************************************************************
 *                                                                           *
 * Function     : WssMacProcessSAQueryResp                                   *
 *                                                                           *
 * Description  : Process SA Query response frame                            *
 *                                                                           *
 * Input(s)     : MAC message type and union of struct with SA Query         *
 *                response structure                                         *
 *                                                                           *
 * Output(s)    : SA Query response message buffer                           *
 *                                                                           *
 * Returns      : SUCCESS/FAILURE                                            *
 *                                                                           *
 * ***************************************************************************/

UINT1
WssMacProcessSAQueryResp (UINT1 eMsgType, tWssMacMsgStruct * pSAQueryRspStruct)
{
    tWssMacMsgStruct   *pSAQueryRspBuf = NULL;
    unWlcHdlrMsgStruct *pCapwapMsgStruct = NULL;

    pCapwapMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pCapwapMsgStruct == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                    "WssMacProcessAssocRsp:- "
                    "UtlShMemAllocWlcBuf returned failure\n");
        return WSSMAC_FAILURE;
    }

    pSAQueryRspBuf =
        (tWssMacMsgStruct *) (VOID *) UtlShMemAllocMacMsgStructBuf ();
    if (pSAQueryRspBuf == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                    "WssMacProcessAssocRsp:-"
                    "UtlShMemAllocMacMsgStructBuf returned failure\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pCapwapMsgStruct);
        return OSIX_FAILURE;
    }
    MEMSET (pSAQueryRspBuf, 0, sizeof (tWssMacMsgStruct));
    MEMSET (pCapwapMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));
    UNUSED_PARAM (eMsgType);

    WssMacAssembleSAQueryFrame (pSAQueryRspStruct, pSAQueryRspBuf);

    if (pSAQueryRspBuf->unMacMsg.MacDot11PktBuf.pDot11MacPdu != NULL)
    {
        pCapwapMsgStruct->WlcHdlrQueueReq.pRcvBuf =
            pSAQueryRspBuf->unMacMsg.MacDot11PktBuf.pDot11MacPdu;
        pCapwapMsgStruct->WlcHdlrQueueReq.u2SessId =
            pSAQueryRspStruct->unMacMsg.MacDot11PktBuf.u2SessId;

        pCapwapMsgStruct->WlcHdlrQueueReq.u4MsgType =
            WSS_CAPWAP_802_11_MGMT_PKT;

        if (WssIfProcessWlcHdlrMsg
            (WSS_WLCHDLR_CAPWAP_MAC_MSG, pCapwapMsgStruct) != WSSMAC_SUCCESS)
        {
            UtlShMemFreeWlcBuf ((UINT1 *) pCapwapMsgStruct);
            UtlShMemFreeMacMsgStructBuf ((UINT1 *) pSAQueryRspBuf);
            return WSSMAC_FAILURE;
        }
    }
    else
    {
        UtlShMemFreeWlcBuf ((UINT1 *) pCapwapMsgStruct);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pSAQueryRspBuf);
        return WSSMAC_FAILURE;
    }
    UtlShMemFreeWlcBuf ((UINT1 *) pCapwapMsgStruct);
    UtlShMemFreeMacMsgStructBuf ((UINT1 *) pSAQueryRspBuf);
    return WSSMAC_SUCCESS;
}

/* ***************************************************************************
 *                                                                           *
 * Function     : WssMacProcessSAQueryReq                                    *
 *                                                                           *
 * Description  : Process SA Query req frame                                 *
 *                                                                           *
 * Input(s)     : MAC message type and union of struct with SA Query         *
 *                response structure                                         *
 *                                                                           *
 * Output(s)    : SA Query request message buffer                            *
 *                                                                           *
 * Returns      : SUCCESS/FAILURE                                            *
 *                                                                           *
 * ***************************************************************************/

UINT1
WssMacProcessSAQueryReq (UINT1 eMsgType, tWssMacMsgStruct * pSAQueryReqStruct)
{
    tWssMacMsgStruct   *pSAQueryReqBuf = NULL;
    unWlcHdlrMsgStruct *pCapwapMsgStruct = NULL;

    pCapwapMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pCapwapMsgStruct == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                    "WssMacProcessAssocRsp:- "
                    "UtlShMemAllocWlcBuf returned failure\n");
        return WSSMAC_FAILURE;
    }

    pSAQueryReqBuf =
        (tWssMacMsgStruct *) (VOID *) UtlShMemAllocMacMsgStructBuf ();
    if (pSAQueryReqBuf == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC,
                    "WssMacProcessAssocRsp:-"
                    "UtlShMemAllocMacMsgStructBuf returned failure\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pCapwapMsgStruct);
        return OSIX_FAILURE;
    }
    MEMSET (pSAQueryReqBuf, 0, sizeof (tWssMacMsgStruct));
    MEMSET (pCapwapMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));
    UNUSED_PARAM (eMsgType);

    WssMacAssembleSAQueryFrame (pSAQueryReqStruct, pSAQueryReqBuf);

    if (pSAQueryReqBuf->unMacMsg.MacDot11PktBuf.pDot11MacPdu != NULL)
    {
        pCapwapMsgStruct->WlcHdlrQueueReq.pRcvBuf =
            pSAQueryReqBuf->unMacMsg.MacDot11PktBuf.pDot11MacPdu;
        pCapwapMsgStruct->WlcHdlrQueueReq.u2SessId =
            pSAQueryReqStruct->unMacMsg.MacDot11PktBuf.u2SessId;

        pCapwapMsgStruct->WlcHdlrQueueReq.u4MsgType =
            WSS_CAPWAP_802_11_MGMT_PKT;

        if (WssIfProcessWlcHdlrMsg
            (WSS_WLCHDLR_CAPWAP_MAC_MSG, pCapwapMsgStruct) != WSSMAC_SUCCESS)
        {
            UtlShMemFreeWlcBuf ((UINT1 *) pCapwapMsgStruct);
            UtlShMemFreeMacMsgStructBuf ((UINT1 *) pSAQueryReqBuf);
            return WSSMAC_FAILURE;
        }
    }
    else
    {
        UtlShMemFreeWlcBuf ((UINT1 *) pCapwapMsgStruct);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pSAQueryReqBuf);
        return WSSMAC_FAILURE;
    }
    UtlShMemFreeWlcBuf ((UINT1 *) pCapwapMsgStruct);
    UtlShMemFreeMacMsgStructBuf ((UINT1 *) pSAQueryReqBuf);
    return WSSMAC_SUCCESS;
}

/* ***************************************************************************
 *                                                                           *
 * Function     : WssMacAssembleSAQueryFrame                                 *
 *                                                                           *
 * Description  : Assemble SA Query structure                                *
 *                                                                           *
 * Input(s)     : Union of struct with SA Query  structure and               *
 *                char buffer                                                *
 *                                                                           *
 * Output(s)    : SA Query  message buffer                                   *
 *                                                                           *
 * Returns      : NONE                                                       *
 *                                                                           *
 * ***************************************************************************/
VOID
WssMacAssembleSAQueryFrame (tWssMacMsgStruct * pSAQueryStruct,
                            tWssMacMsgStruct * pSAQueryBuf)
{
    UINT1              *pu1ReadPtr = NULL;
    UINT4               u4Len = 0;
    UINT1               au1pBuf[WSSMAC_MAX_PKT_LEN] = { 0 };
    MEMSET (au1pBuf, 0, WSSMAC_MAX_PKT_LEN);

    pu1ReadPtr = au1pBuf;

    /* Put the 802.11 MAC Header */
    WSSMAC_PUT_2BYTES (pu1ReadPtr, pSAQueryStruct->unMacMsg.StationQueryActionFrame.MacMgmtFrmHdr.u2MacFrameCtrl);    /* Put Frame control field */
    WSSMAC_PUT_2BYTES (pu1ReadPtr, pSAQueryStruct->unMacMsg.StationQueryActionFrame.MacMgmtFrmHdr.u2MacFrameDurId);    /* Put Duration ID field */
    WSSMAC_PUT_NBYTES (pu1ReadPtr, pSAQueryStruct->unMacMsg.StationQueryActionFrame.MacMgmtFrmHdr.u1DA, WSSMAC_MAC_ADDR_LEN);    /* Put Address1 field */
    WSSMAC_PUT_NBYTES (pu1ReadPtr, pSAQueryStruct->unMacMsg.StationQueryActionFrame.MacMgmtFrmHdr.u1SA, WSSMAC_MAC_ADDR_LEN);    /* Put Address2 field */
    WSSMAC_PUT_NBYTES (pu1ReadPtr, pSAQueryStruct->unMacMsg.StationQueryActionFrame.MacMgmtFrmHdr.u1BssId, WSSMAC_MAC_ADDR_LEN);    /* Put Address3 field */
    WSSMAC_PUT_2BYTES (pu1ReadPtr, pSAQueryStruct->unMacMsg.StationQueryActionFrame.MacMgmtFrmHdr.u2MacFrameSeqCtrl);    /* Put Sequence control field */

    /*  Put Category code */
    WSSMAC_PUT_1BYTE (pu1ReadPtr,
                      pSAQueryStruct->unMacMsg.StationQueryActionFrame.
                      u1CategoryCode);

    /* Put Action Field */
    WSSMAC_PUT_1BYTE (pu1ReadPtr,
                      pSAQueryStruct->unMacMsg.StationQueryActionFrame.
                      u1ActionField);

    /* Put Transaction Id */

    WSSMAC_PUT_2BYTES (pu1ReadPtr,
                       pSAQueryStruct->unMacMsg.StationQueryActionFrame.
                       u2TransactionId);

    /* u4Len = sizeof (pu1ReadPtr); */
    u4Len = (UINT4) (pu1ReadPtr - au1pBuf);

    /* Allocate memory for packet linear buffer */
    pSAQueryBuf->unMacMsg.MacDot11PktBuf.pDot11MacPdu =
        WSSMAC_ALLOCATE_CRU_BUF (u4Len);

    if (pSAQueryBuf->unMacMsg.MacDot11PktBuf.pDot11MacPdu == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC, "Memory Allocation Failed \r\n");
        return;
    }

    WSSMAC_COPY_TO_BUF (pSAQueryBuf->unMacMsg.MacDot11PktBuf.pDot11MacPdu,
                        au1pBuf, 0, u4Len);

    return;
}
#endif
#endif
#endif
