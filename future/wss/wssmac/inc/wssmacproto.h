/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
*   $Id: wssmacproto.h,v 1.2 2017/05/23 14:16:58 siva Exp $  
 * Description: This file contains the prototypes used by WSSMAC Module
 * *******************************************************************/
#ifndef __WSSMAC_PROTO_H__
#define __WSSMAC_PROTO_H__

#include "wssmacconst.h"
/* ********************** WSSMAC Common API's ******************************* */

UINT1
WssMacMainInit PROTO ((VOID));

/* ******** WLC APIs ************* */

/* Process WssIf Msg */
UINT1
WssMacProcessWssIfMsg PROTO ((UINT4, tWssMacMsgStruct *));

/* Parse Wss MAC Frame */
UINT1
WssMacParseMACFrame PROTO ((UINT1 *));

/* Get frame type from frame body */
UINT1
WssMacGetFrameType PROTO ((UINT1));

/* Get frame subtype from frame body */
UINT1
WssMacGetFrameSubType PROTO ((UINT1));

UINT1
WssMacProcessCapwapMsg PROTO ((UINT1, tWssMacMsgStruct *));
#ifdef WPS_WTP_WANTED
UINT1
WssMacProcessWpsProbeReq PROTO ((UINT1, tWssMacMsgStruct *));
#endif
UINT1
WssMacProcessProbeReq PROTO ((UINT1, tWssMacMsgStruct *));
UINT1
WssMacProcessAuthenMsg PROTO ((UINT1, tWssMacMsgStruct *));

UINT1
WssMacProcessAuthenSeqIMsg PROTO ((UINT1, tWssMacMsgStruct *));

UINT1
WssMacProcessAuthenSeqIIMsg PROTO ((UINT1, tWssMacMsgStruct *));

UINT1
WssMacProcessAuthenSeqIIIMsg PROTO ((UINT1, tWssMacMsgStruct *));

UINT1
WssMacProcessAuthenSeqIVMsg PROTO ((UINT1, tWssMacMsgStruct *));

UINT1
WssMacProcessWssIfAuthMsg PROTO ((UINT1, tWssMacMsgStruct *));

/* Assemble Authentication response frame  */
VOID
WssMacAssembleAuthFrame PROTO ((tWssMacMsgStruct *,  tWssMacMsgStruct *));

UINT1
WssMacProcessAssocReq PROTO ((UINT1, tWssMacMsgStruct *));

UINT1
WssMacProcessAssocRsp PROTO ((UINT1, tWssMacMsgStruct *));

/* Assemble Association response frame  */
VOID
WssMacAssembleAssocRspFrame PROTO ((tWssMacMsgStruct *, tWssMacMsgStruct *));

UINT1
WssMacProcessReassocReq PROTO ((UINT1, tWssMacMsgStruct *));

UINT1
WssMacProcessReassocRsp PROTO ((UINT1, tWssMacMsgStruct *));

/* Assemble Reassociation response frame  */
VOID
WssMacAssembleReassocRspFrame PROTO ((tWssMacMsgStruct *, tWssMacMsgStruct *));

UINT1
WssMacProcessDeauthenMsg PROTO ((UINT1, tWssMacMsgStruct *));

UINT1
WssMacProcessDisassocMsg PROTO ((UINT1, tWssMacMsgStruct *));

UINT1
WssMacProcessDataMsg PROTO ((UINT1, tWssMacMsgStruct *));

UINT1
WssMacProcessCfaMsg PROTO ((UINT1, tWssMacMsgStruct *));

VOID
WssMacAssembleDataFrame PROTO ((tWssMacMsgStruct *,  tWssMacMsgStruct *));

/* Other Stubs */
UINT1
WlcHdlrPostPktFromWss (UINT1 eMsgType,tWssMacMsgStruct* pMsgStruct);

VOID
WssMacMacToStr (UINT1 *pMacAddr, UINT1 *pu1Temp);

/* ******** WTP APIs ************* */

UINT1
WssMacProcessBeaconMsg PROTO ((UINT1, tWssMacMsgStruct *));

UINT1
WssMacProcessProbeResp PROTO ((UINT1, tWssMacMsgStruct *));

UINT1
WssMacConstructProbeRsp PROTO ((tWssMacMsgStruct *, tWssMacMsgStruct *));

/* Assemble Probe response frame  */
VOID
WssMacAssembleProbeRspFrame PROTO ((tWssMacMsgStruct *, tWssMacMsgStruct *));

#ifdef PMF_WANTED
/* Assemble SA Query Action Frames */

VOID
WssMacAssembleSAQueryRspFrame PROTO ((tWssMacMsgStruct * pSAQueryRspStruct,
                             tWssMacMsgStruct * pSAQueryRspBuf));
#endif
/* Process 802.11 Frames */
typedef UINT1 (*tWssMacFrameHandling)(UINT1, tWssMacMsgStruct *);

UINT1
WssMacProcessActionMsg PROTO ((UINT1, tWssMacMsgStruct *));

UINT1
WssMacProcessTpcRequest PROTO ((UINT1, tWssMacMsgStruct *));

UINT1
WssMacProcessSpectrumMgmtframe PROTO ((tWssMacMgmtFrmHdr *,
                                UINT1 **));

UINT1
WssMacProcessChanlSwitAnnounce PROTO ((UINT1, tWssMacMsgStruct *));
UINT1
WssMacProcessExChanlSwitAnnounce PROTO ((UINT1, tWssMacMsgStruct *));

#ifdef PMF_WANTED
UINT1
WssMacProcessSAQueryMgmtFrame PROTO ((tWssMacMgmtFrmHdr *,
                                UINT1 **,UINT2));

VOID WssMacAssembleSAQueryFrame PROTO ((tWssMacMsgStruct *pSAQueryRspStruct, 
                                        tWssMacMsgStruct *pSAQueryRspBuf));


/* SA Query Action Frames */

UINT1
WssMacProcessSAQueryResp PROTO ((UINT1, tWssMacMsgStruct *));

UINT1
WssMacProcessSAQueryReq PROTO ((UINT1, tWssMacMsgStruct *));
#endif

UINT1
WssMacProcessMeasRequest PROTO ((UINT1, tWssMacMsgStruct *));


#ifdef _WSSMAC_MAIN_C_
tWssMacFrameHandling gaWssMacFrameHandling[WSSMAC_MAX_WSS_MSG_TYPES] =
{
        WssMacProcessAssocReq,          /* WSS_DOT11_MGMT_ASSOC_REQ */
        WssMacProcessAssocRsp,          /* WSS_DOT11_MGMT_ASSOC_RSP */
        WssMacProcessReassocReq,        /* WSS_DOT11_MGMT_REASSOC_REQ */
        WssMacProcessReassocRsp,        /* WSS_DOT11_MGMT_REASSOC_RSP */
#ifdef WPS_WTP_WANTED        
        WssMacProcessWpsProbeReq,          /* WSS_DOT11_MGMT_PROBE_REQ */
#else
        WssMacProcessProbeReq,          /* WSS_DOT11_MGMT_PROBE_REQ */
#endif        
        WssMacProcessProbeResp,         /* WSS_DOT11_MGMT_PROBE_RSP */
        NULL,
        NULL,
        WssMacProcessBeaconMsg,         /* WSS_DOT11_MGMT_BEACON_MSG */
        NULL,
        WssMacProcessDisassocMsg,       /* WSS_DOT11_MGMT_DISASSOC_MSG */
        WssMacProcessAuthenMsg,         /* WSS_DOT11_MGMT_AUTH_MSG */
        WssMacProcessDeauthenMsg,       /* WSS_DOT11_MGMT_DEAUTH_MSG */
        WssMacProcessActionMsg,         /*WSS_DOT11_MGMT_ACTION_MSG*/
        NULL,/* WssMacProcessCtrlMsg */
        WssMacProcessDataMsg,           /* WSS_DOT11_DATA_MSG */
        WssMacProcessCfaMsg,            /* WSS_MAC_CFA_MSG */
        WssMacProcessCapwapMsg,         /* WSS_MAC_CAPWAP_MSG */
        NULL,                           /* WSS_MAC_APHDLR_MSG */
        WssMacProcessTpcRequest, /*WSS_DOT11_MGMT_TPC_REQ*/
        WssMacProcessChanlSwitAnnounce, /* WSS_DOT11_MGMT_CHANL_SWITCH */
        WssMacProcessExChanlSwitAnnounce, /* WSS_DOT11_MGMT_EX_CHANL_SWITCH */
        WssMacProcessMeasRequest,        /*WSS_DOT11_MGMT_MEAS_REQ*/
#ifdef RSNA_WANTED
#ifdef PMF_WANTED
        WssMacProcessSAQueryResp,         /*WSS_DOT11_MGMT_SAQUERY_ACTION_RSP*/
        WssMacProcessSAQueryReq         /*WSS_DOT11_MGMT_SAQUERY_ACTION_REQ*/
#endif
#endif
};
#else
extern tWssMacFrameHandling  gaWssMacFrameHandling[WSSMAC_MAX_WSS_MSG_TYPES];
#endif /* _WSSMAC_MAIN_C_ */
#endif /* __WSSMAC_PROTO_H__ */
