 /********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *  $Id: wssmactrc.h,v 1.2 2017/11/24 10:37:11 siva Exp $   
 * Description: common trace related macros for WSSMAC Module
 * *******************************************************************/

#ifndef __WSSMAC_TRC_H__
#define __WSSMAC_TRC_H__

#define  WSSMAC_MOD                ((const char *)"WSSMAC")

#define WSSMAC_MASK                WSSMAC_MGMT_TRC | WSSMAC_FAILURE_TRC

/* Trace and debug flags */

#define WSSMAC_FN_ENTRY() MOD_FN_ENTRY (WSSMAC_MASK, WSSMAC_ENTRY_TRC,WSSMAC_MOD)

#define WSSMAC_FN_EXIT() MOD_FN_EXIT (WSSMAC_MASK, WSSMAC_EXIT_TRC,WSSMAC_MOD)

#define WSSMAC_TRC(mask, fmt)\
         MOD_TRC(WSSMAC_MASK, mask, WSSMAC_MOD, fmt)

#define WSSMAC_TRC1(mask,fmt,arg1)\
         MOD_TRC_ARG1(WSSMAC_MASK,mask,WSSMAC_MOD,fmt,arg1)

#define WSSMAC_TRC2(mask,fmt,arg1,arg2)\
         MOD_TRC_ARG2(WSSMAC_MASK,mask,WSSMAC_MOD,fmt,arg1,arg2)

#define WSSMAC_TRC3(mask,fmt,arg1,arg2,arg3)\
         MOD_TRC_ARG3(WSSMAC_MASK,mask,WSSMAC_MOD,fmt,arg1,arg2,arg3)

#define WSSMAC_TRC4(mask,fmt,arg1,arg2,arg3,arg4)\
         MOD_TRC_ARG4(WSSMAC_MASK,mask,WSSMAC_MOD,fmt,arg1,arg2,arg3,arg4)

#define WSSMAC_TRC5(mask,fmt,arg1,arg2,arg3,arg4,arg5)\
         MOD_TRC_ARG5(WSSMAC_MASK,mask,WSSMAC_MOD,fmt,arg1,arg2,arg3,arg4,arg5)
#define WSSMAC_TRC6(mask,fmt,arg1,arg2,arg3,arg4,arg5,arg6)\
         MOD_TRC_ARG6(WSSMAC_MASK,mask,WSSMAC_MOD,fmt,arg1,arg2,arg3,arg4,arg5,arg6)
#define WSSMAC_TRC7(mask,fmt,arg1,arg2,arg3,arg4,arg5,arg6,arg7)\
         MOD_TRC_ARG7(WSSMAC_MASK,mask,WSSMAC_MOD,fmt,arg1,arg2,arg3,arg4,arg5,arg6,arg7)
#endif
