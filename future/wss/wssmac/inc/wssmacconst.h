/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *  $Id: wssmacconst.h,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $   
 * Description: constant macros for WSSMAC Module
 * *******************************************************************/

#ifndef  __WSSMAC_CONST_H__
#define  __WSSMAC_CONST_H__

#define WSSMAC_AUTH_ALGO_OPEN                0
#define WSSMAC_AUTH_ALGO_SHARED       0x0100
#define WSSMAC_AUTH_TRAN_SEQ_NO1       0x0100
#define WSSMAC_AUTH_TRAN_SEQ_NO2       0x0200
#define WSSMAC_AUTH_TRAN_SEQ_NO3       0x0300
#define WSSMAC_AUTH_TRAN_SEQ_NO4       0x0400
#define WSSMAC_RESERVED6       0x6
#define WSSMAC_RESERVED7                     0x7
#define WSSMAC_RESERVED9        0x9
#define WSSMAC_RESERVED14       0xe
#define WSSMAC_RESERVED15        0xf

#define WSSMAC_MGMT_HEADER_LEN 24

#define WSSMAC_MAX_PKT_LEN      2350
#define WSSMAC_MAX_PDU_LEN      500
#define WSS_MAC_FCS_SIZE                    4

#define WSSMAC_MAX_WSS_MSG_TYPES     25

#define WSS_MAC_SPLITMAC_MODE      1 
#define WSS_MAC_LOCALMAC_MODE      0
#define WSS_MAC_WLAN_LOCAL_MODE         2

/* QoS */
#define WSS_MAC_QOS_PBIT_VAL            0X01
#define WSS_MAC_QOS_DSCP_VAL            0X02
#define WSS_MAC_QOS_BOTH_VAL            0X04

/* MASK Values */
#define WSSMAC_GET_FRAME_TYPE_MASK 0x0c
#define WSSMAC_GET_FRAME_SUB_TYPE_MASK  0xf0


#define WSSMAC_SET_PROBRSP_FRAME_CTRL1  0xf0ff
#define WSSMAC_SET_PROBRSP_FRAME_CTRL2  0x5000

#define WSSMAC_SET_ASSOCRSP_FRAME_CTRL1  0xf0ff
#define WSSMAC_SET_ASSOCRSP_FRAME_CTRL2  0x1000

#define WSSMAC_SET_REASSOCRSP_FRAME_CTRL1  0xf0ff
#define WSSMAC_SET_REASSOCRSP_FRAME_CTRL2  0x3000

#define WSSMAC_SET_DISASSOC_FRAME_CTRL  0xa000
#define WSSMAC_SET_DEAUTH_FRAME_CTRL    0xc000
#define WSSMAC_SET_ACTION_FRAME_CTRL    0xd000

#define WSSMAC_DATA_ENC_FRAME_CTRL_MASK 0x0040
#define WSSMAC_DATA_PROT_BIT_SHIFT      0x06
#define WSSMAC_FRAME_CTRL_PROT_BIT_SET  0x0001
#define WSSMAC_FRAME_CTRL_PROT_BIT_OFF  0xFFBF

/*Trace Level*/
#define WSSMAC_MGMT_TRC                 0x00000001
#define WSSMAC_INIT_TRC                 0x00000002
#define WSSMAC_ENTRY_TRC                0x00000004
#define WSSMAC_EXIT_TRC                 0x00000008
#define WSSMAC_FAILURE_TRC              0x00000010
#define WSSMAC_BUF_TRC                  0x00000020
#define WSSMAC_SESS_TRC                 0x00000040
#define WSSMAC_INFO_TRC                 0x00000080

#define WSSMAC_SUCCESS                  OSIX_SUCCESS
#define WSSMAC_FAILURE                  OSIX_FAILURE

#define WSSMAC_FALSE                    OSIX_FALSE
#define WSSMAC_TRUE          OSIX_TRUE

#define WSSMAC_INVALID_MSG_TYPE  100

#define WSSMAC_PKT_DUMP                 0
#define WSSMAC_UNUSED_CODE              0

#define WLAN_QOS_INDEX_BE    0
#define WLAN_QOS_INDEX_VI    1
#define WLAN_QOS_INDEX_VO    2
#define WLAN_QOS_INDEX_BG    3

#define RADIO_QOS_INDEX_BE      3
#define RADIO_QOS_INDEX_VI      2
#define RADIO_QOS_INDEX_VO      1
#define RADIO_QOS_INDEX_BG      4

#define WSSMAC_BSSID_IND    4
#define WSSMAC_SMAC_IND     10
#define WSSMAC_DMAC_IND     16

#define WSSMAC_MAC_ADDR_LEN 6

#define WSSMAC_BROADCAST_ADDR 0xFF

#define WSSMAC_HT_START_IND 24
#define WSSMAC_HT_END_IND 28
#define WSSMAC_HT_HDR_LEN 4


#define WSSMAC_HT_RSNA_START_IND 22
#define WSSMAC_HT_RSNA_END_IND 30
#define WSSMAC_HT_RSNA_HDR_LEN 16

#define WSSMAC_FRAM_TYPE_SHIFT 2
#define WSSMAC_FRAM_SUB_TYPE_SHIFT 4
#define WSSMAC_MAC_STR_LEN       21

#endif 
