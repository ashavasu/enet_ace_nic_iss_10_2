/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *  $Id: wssmacmacr.h,v 1.3 2017/11/24 10:37:11 siva Exp $   
 * Description: This file contains the macros used by WSSMAC Module  
 * *******************************************************************/

#ifndef __WSSMAC_MACR_H__
#define __WSSMAC_MACR_H__

#define   WSSMAC_SWAP_BYTES(x) OSIX_HTONS(x)
#define   WSSMAC_MEMCPY        memcpy
#define   WSSMAC_HTONS             OSIX_HTONS
#define   WSSMAC_HTONL             OSIX_HTONL
#define   WSSMAC_NTOHS             OSIX_NTOHS
#define   WSSMAC_NTOHL             OSIX_NTOHL
#define   WSSMAC_MEMSET            MEMSET


#define WSSMAC_GET_1BYTE(u1Val, pu1PktBuf) \
        do {                             \
           u1Val = *pu1PktBuf;           \
           pu1PktBuf += 1;        \
        } while(0)

#define WSSMAC_GET_2BYTES(u2Val, pu1PktBuf)            \
        do {                                        \
           WSSMAC_MEMCPY (&u2Val, pu1PktBuf, 2);\
           u2Val = (UINT2)WSSMAC_NTOHS(u2Val);               \
           pu1PktBuf += 2;                   \
        } while(0)

#define WSSMAC_GET_4BYTES(u4Val, pu1PktBuf)            \
        do {                                        \
           WSSMAC_MEMCPY (&u4Val, pu1PktBuf, 4);\
           u4Val = WSSMAC_NTOHL(u4Val);               \
           pu1PktBuf += 4;                   \
        } while(0)

#define WSSMAC_GET_NBYTES(uNVal, pu1PktBuf, uNLen)    \
        do {                                         \
            WSSMAC_MEMCPY (&uNVal,pu1PktBuf,uNLen);  \
            pu1PktBuf += uNLen;                      \
        }while (0)


/* Encode Module Related Macros used to assign fields to buffer */
#define WSSMAC_PUT_1BYTE(pu1PktBuf, u1Val) \
        do {                             \
           *pu1PktBuf = u1Val;           \
            pu1PktBuf += 1;        \
        } while(0)

#define WSSMAC_PUT_2BYTES(pu1PktBuf, u2Val)        \
        do {                                    \
           *((UINT2 *) ((VOID*)pu1PktBuf)) = WSSMAC_HTONS(u2Val);\
           pu1PktBuf += 2;\
        } while(0)
#define WSSMAC_PUT_4BYTES(pu1PktBuf, u4Val)       \
        do {                                   \
           *((UINT4 *) ((VOID*)pu1PktBuf)) = WSSMAC_HTONL(u4Val);\
           pu1PktBuf += 4;\
        }while(0)

#define WSSMAC_PUT_NBYTES(pu1PktBuf, pu1PktBuf1, length)  \
        do {                                   \
           WSSMAC_MEMCPY (pu1PktBuf, &pu1PktBuf1, length); \
    pu1PktBuf += length;             \
        }while(0)
#define WSSMAC_SKIP_NBYTES(u2Val,pu1PktBuf)   \
    do {                     \
         pu1PktBuf += u2Val; \
    }while(0)
/* CRU BUF Macros */
#define WSSMAC_GET_BUF_LEN(pBuf) \
        CRU_BUF_Get_ChainValidByteCount(pBuf)

#define WSSMAC_BUF_IF_LINEAR(pBuf, u4OffSet, u4Size) \
        CRU_BUF_Get_DataPtr_IfLinear ((tCRU_BUF_CHAIN_HEADER *)(pBuf), \
                                      (u4OffSet), (u4Size))

#define WSSMAC_COPY_FROM_BUF(pBuf, pu1Dst, u4Offset, u4Size) \
        CRU_BUF_Copy_FromBufChain ((pBuf), (UINT1 *)(pu1Dst), u4Offset, u4Size)


#define WSSMAC_RELEASE_CRU_BUF(pBuf) \
        CRU_BUF_Release_MsgBufChain ((pBuf), 0)/*;\
 printf("---%s===%d\n",__func__,__LINE__)*/

#define WSSMAC_COPY_TO_BUF(pBuf, pu1Src, u4Offset, u4Size) \
        CRU_BUF_Copy_OverBufChain ((pBuf), (UINT1 *)(pu1Src), u4Offset, u4Size)

#define WSSMAC_ALLOCATE_CRU_BUF(u4len) \
        CRU_BUF_Allocate_MsgBufChain (u4Len, 0);\
/* printf("+++%s===%d\n",__func__,__LINE__);*/

#endif
