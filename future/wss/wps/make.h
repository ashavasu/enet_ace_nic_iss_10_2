#!/bin/csh
# (C) 2002 Future Software Pvt. Ltd.
#/*$Id: make.h,v 1.2 2017/12/15 09:50:51 siva Exp $*/
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : MAKE.H                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       : Future Software Pvt. Ltd.                     |
# |                                                                          |
# |   MAKE TOOL(S) USED      : Eg: GNU MAKE                                  |
# |                                                                          |
# |   TARGET ENVIRONMENT     : LINUX ( Slackware 1.2.1 )                     |
# |                                                                          |
# |   DATE                   :                                               |
# |                                                                          |
# |   DESCRIPTION            : Provide the following information in order -  |
# |                            1. Number of Submodules present if Main       |
# |                               makefile.                                  |
# |                            2. Clean option                               |
# +--------------------------------------------------------------------------+

###########################################################################
#               COMPILATION SWITCHES                                      #
###########################################################################


TOTAL_OPNS = $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES)

############################################################################
#                         Directories                                      #
############################################################################

CFA_BASE_DIR   = ${BASE_DIR}/cfa2

WPS_BASE_DIR      = ${BASE_DIR}/wss/wps
FSAP_BASE_DIR      = ${BASE_DIR}/fsap2
PNAC_BASE_DIR      = ${BASE_DIR}/pnac
RSNA_BASE_DIR       = ${BASE_DIR}/wss/rsna
WSSIF_BASE_DIR       = ${BASE_DIR}/wss/wssif
WSSCFG_BASE_DIR       = ${BASE_DIR}/wss/wsscfg
WLAN_BASE_DIR      = ${BASE_DIR}/wss
WPS_SRC_DIR       = ${WPS_BASE_DIR}/src
WPS_INC_DIR       = ${WPS_BASE_DIR}/inc
WPS_OBJ_DIR       = ${WPS_BASE_DIR}/obj
CFA_INCD           = ${CFA_BASE_DIR}/inc
CMN_INC_DIR        = ${BASE_DIR}/inc
PNAC_INC_DIR       = ${PNAC_BASE_DIR}/inc
WLAN_INC_DIR       = ${WLAN_BASE_DIR}/wsswlan/inc
WSSSTA_INC_DIR     = ${WLAN_BASE_DIR}/wsssta/inc
RSNA_INC_DIR       = ${RSNA_BASE_DIR}/inc
WSSIF_INC_DIR       = ${WSSIF_BASE_DIR}/inc
WSSCFG_INC_DIR       = ${WSSCFG_BASE_DIR}/inc
FSAP_CMN_DIR      = ${FSAP_BASE_DIR}/cmn
CRYPTO_BASE_DIR = ${BASE_DIR}/../opensource/ssl/crypto
CRYPTO_INC_DIR = ${BASE_DIR}/../opensource/ssl/crypto/hmac
WPS_SUPP_INC_DIR = /opt/poky/1.7/sysroots/ppce500v2-poky-linux-gnuspe/usr/src/debug/wpa-supplicant/2.2-r0/wpa_supplicant-2.2/src/wps
#CRYPTO_BASE_DIR = ${BASE_DIR}/../opensource/v1_0_1c/ssl/crypto
#CRYPTO_INC_DIR = ${BASE_DIR}/../opensource/v1_0_1c/ssl/crypto/hmac

############################################################################
##                     INCLUDE OPTIONS                                    ##
############################################################################

GLOBAL_INCLUDES  =  -I${WPS_INC_DIR} -I${CFA_INCD} -I${CMN_INC_DIR} -I${PNAC_INC_DIR} -I${WLAN_INC_DIR} -I${WSSSTA_INC_DIR} -I${RSNA_INC_DIR} -I${WSSIF_INC_DIR} -I${WSSCFG_INC_DIR} -I${CRYPTO_BASE_DIR} -I${FSAP_CMN_DIR}  -I${CRYPTO_INC_DIR} -I${AES_CRYP} -I${AES_CRYP_SRC} -I${WPS_SUPP_INC_DIR}
INCLUDES         = ${GLOBAL_INCLUDES} ${COMMON_INCLUDE_DIRS} -I$(BASE_DIR)/util/aes 

#############################################################################
