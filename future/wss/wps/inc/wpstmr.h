/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: wpstmr.h,v 1.1.1.1 2016/09/20 10:42:41 siva Exp $
*
* Description: WPS File.
*********************************************************************/

#ifndef _WPSTMR_H
#define _WPSTMR_H

#define WPS_SESSION_TIME 300
#define WPS_PROTOCOL_TIME 120
#define WPS_RETRANS_TIME 5
#define WPS_MSG_TIME 15
#define WPS_PBC_TIME 120
#define WPS_RETRANSMISSION_COUNT 4

typedef enum { 
    WPS_MIN_TIMER_ID           =   0,
    WPS_SESSION_TIMER_ID       =   1,
    WPS_PROTOCOL_TIMER_ID    =     2,
    WPS_MSG_TIMER_ID         =     3,
    WPS_RETRANS_TIMER_ID       =   4,
    WPS_PBC_TIMER_ID           =   5,
    WPS_MAX_TIMER_ID           =   6,
} eWpsTimerId;


typedef struct  WPSTIMER {
     tTmrAppTimer Node;
     VOID *pContext;              /* Refers to the context to which 
                                       timer is attached */
     UINT4 u4ProfileId;
     UINT1  u1TimerId;              /* Identification of Timer */
     UINT1 u1Status;                /* Validity of the structure 
                                       stored */
     UINT2 u2Pad;                   /* Padding for 4 Byte allignment */
}tWpsTimer;



#define WPS_TIMER_ACTIVE     1
#define WPS_TIMER_INACTIVE   0

#define WPS_INIT_TIMER(pWpsTimer)      \
    ((pWpsTimer)->u1Status = WPS_TIMER_INACTIVE)
#define WPS_ACTIVATE_TIMER(pWpsTimer)      \
    ((pWpsTimer)->u1Status = WPS_TIMER_ACTIVE)
#define WPS_IS_TIMER_ACTIVE(pWpsTimer) \
    (((pWpsTimer)->u1Status == WPS_TIMER_INACTIVE)?FALSE:TRUE)



UINT4 WpsTmrInit                     PROTO ((VOID));
UINT4 WpsTmrStartTimer (tWpsTimer * pWpsTimer, 
                        eWpsTimerId TimerId,
                        UINT4 u4TimeOut, 
                        VOID *pTmrContext,
                        UINT4 u4ProfileId);
UINT4 WpsTmrStopTimer                   PROTO ((tWpsTimer *pWpsTmr));
VOID  WpsTmrProcRetransTimer  PROTO ((VOID *pTmrContext));
VOID  WpsTmrProcSessionTimer(VOID *pTmrContext);
VOID WpsTmrProcProtocolTimer(VOID *pTmrContext);
VOID WpsTmrProcPbcTimer(VOID *pTmrContext, UINT4 u4ProfileId);
VOID WpsTmrProcMsgTimer(VOID *pTmrContext);
VOID WpsTmrProcessTimer (VOID);


#endif /* _WPSTMR_H */
