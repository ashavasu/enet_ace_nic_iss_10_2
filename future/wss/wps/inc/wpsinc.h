/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 *  $Id: wpsinc.h,v 1.2 2017/12/15 09:50:51 siva Exp $
 *
 *  Description: WPS File
 ********************************************************************/
#ifndef __WPS_INC__
#define __WPS_INC__

#include "wpscli.h"
#include "lr.h"
#include "ip.h"
#include "sec.h"
#include "cfa.h"
#include "araeswrap.h" /*Wrapper file for aes in opensource v1_0_1c*/
#include "arrc4skip.h" /*Wrapper file for rc4 in opensource v1_0_1c*/
#include "utilipvx.h"
#include "radius.h"
#include "pnac.h"
#include "pnachdrs.h"
#include "pnactdfs.h"
#include "wlchdlr.h"
#include "wsssta.h"
#include "wsswlan.h"
#include "wssifinc.h"
#include "rsna.h"
#include "rsnamacr.h"
#include "rsnatmr.h"
#include "rsnatdfs.h"
#include "rsnatrc.h"
#include "rsnarty.h"

/* To include latest sha1 & md5 */
#include "arHmac_api.h"
#include "wps.h"
#include "wps_defs.h"
#include "wpsmacr.h"
#include "wpstmr.h"
#include "wpstdfs.h"
#include "wpstrc.h"
#ifndef _WPSINIT_C
#include "wpsextn.h"
#endif
#include "wpsprot.h"
#include "wpssz.h"
#include "fswpswr.h"
#endif
