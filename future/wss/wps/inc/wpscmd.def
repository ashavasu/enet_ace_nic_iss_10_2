/********************************************************************
*copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: wpscmd.def,v 1.1.1.1 2016/09/20 10:42:41 siva Exp $
*
* Description: WPS File.
*********************************************************************/


DEFINE GROUP: WPS_CONFIG_CMDS

   COMMAND  : wps {enable | disable} <integer(1-512)> profile_name <string(32)>
   ACTION   : {
                 UINT1 u1Status=0;

                 if ($1 != NULL)
                 {
                    u1Status = WPS_ENABLED;
                 }
                 else if ($2 != NULL)
                 {
                    u1Status = WPS_DISABLED;
                 }
                 if($5 != NULL)
                 {
                    cli_process_wps_command(CliHandle, CLI_WPS_STATUS, NULL,&u1Status,$3,$5);
                 }
               }

   SYNTAX   : wps {enable | disable} <ssid> profile_name <string(32)>
   HELP     : Configures the wps status
   CXT_HELP : Configures wps related information |
              enable Sets the wps status as enable |
              disable Sets the wps status as disable |
              (1-512) ssid |
              profile_name Name of the AP Profile |
              <string(32)> Length of the AP Profile name |
              <CR> Configures the wps status

   COMMAND  : wps set additionalIE {pbc | pin} <integer(1-512)> profile_name <string(32)>
   ACTION   : {
                 UINT1 u1Status=0;

                 if ($3 != NULL)
                 {
                    u1Status = WPS_PBC;
                 }
                 else if ($4 != NULL)
                 {
                    u1Status = WPS_PIN;
                 }
                        cli_process_wps_command(CliHandle, CLI_WPS_ADDITIONALIE_SET, NULL,&u1Status,$5,$7);

               }

   SYNTAX   : wps set additionalIE {pbc| pin} <ssid> profile_name <string(32)>
   HELP     : Configures the wps additional IEs as pbc or pin 
   CXT_HELP : Configures wps related information |
              set Sets the configured value |
              additionalIE Extra IEs that are supported in beacon and probe frames | 
              pbc Sets the wps method as push |
              pin Sets the wps method as pin|
              (1-512) ssid |
              profile_name Name of the AP Profile |
              <string(32)> Length of the AP Profile name |
              <CR> Configures the wps additional IEs as pbc or pin
   
END GROUP

DEFINE GROUP : WPS_EXEC_CMDS
   COMMAND  : show wps config <integer(1-512)> [profile_name <string(32)>] 
   ACTION   : {   
                  UINT4 u4CapwapWlanIndex = 0;
                  
                  if ($3 !=NULL)
                  {
                      u4CapwapWlanIndex = *(UINT4 *)($3);
                  }
                  if($4 != NULL)
                  cli_process_wps_command(CliHandle,
                                                CLI_WPS_SHOW_CONFIG, NULL,&u4CapwapWlanIndex,$5);
                  else if ($4 == NULL) 
                  cli_process_wps_command(CliHandle,
                                            CLI_WPS_SHOW_CONFIG, NULL,&u4CapwapWlanIndex,NULL);
              }
   SYNTAX   : show wps config <ssid> [profile_name <string(32)>]
   HELP     : Displays the WPS configuration for a particular wlanid 
   CXT_HELP : show Displays the information of wps configuration |
              wps Wireless protected setup related information |
              config WPS Configuration |
              (1-512) ssid |
              profile_name Name of the AP Profile |
              <string(32) Length of the AP Profile name |
              <CR> Displays the WPS configuration for a particular wlanid 

   COMMAND  : show wps authentication details <integer(1-512)>
   ACTION   : {
                  UINT4 u4CapwapWlanIndex = 0;

                  if ($4 !=NULL)
                  {
                      u4CapwapWlanIndex = *(UINT4 *)($4);
                  }
                  else
                  {
                      u4CapwapWlanIndex = 0;
                  }

                  cli_process_wps_command(CliHandle,
                                            CLI_WPS_SHOW_AUTH_INFO, NULL,&u4CapwapWlanIndex);
              }
   SYNTAX   : show wps authentication details <ssid>
   HELP     : Displays the information of WPS authentication for a particular ssid
   CXT_HELP : show Dispalys the information of WPS authentication |
              wps wireless protected setup related information |
              authentication authentication related information |
              details authenticated stations information |
              (1-512) ssid |
              <CR> Displays the information of WPS authentication for a particular ssid 


   COMMAND  : show wps status <integer(1-512)> [profile_name <string(32)>]
   ACTION   : {
                  UINT4 u4CapwapWlanIndex = 0;

                  if ($3 !=NULL)
                  {
                      u4CapwapWlanIndex = *(UINT4 *)($3);
                  }
                  else
                  {
                      u4CapwapWlanIndex = 0;
                  }
                  if($4 != NULL)
                  cli_process_wps_command(CliHandle,
                                                CLI_WPS_SHOW_STATUS, NULL,&u4CapwapWlanIndex,$5);
                  else if ($4 == NULL)
                  cli_process_wps_command(CliHandle,
                                            CLI_WPS_SHOW_STATUS, NULL,&u4CapwapWlanIndex,NULL);
              }
   SYNTAX   : show wps status <ssid> [profile_name <string(32)>]
   HELP     : Displays the WPS status for a particular AP Profile 
   CXT_HELP : show Displays wps status |
              wps wireless protected setup related information |
              status wps state related information |
              (1-512) ssid |
              profile_name Name of the AP Profile |
              <string(32)> Length of the AP Profile name |
              <CR> Displays the wps status for a particular AP Profile
 
   COMMAND   : debug wps  { init-shutdown | mgmt | data | events | packet | os | failall | buffer | all } 
   ACTION    : {
                UINT4 u4TraceValue = 0;

                if ($2 != NULL)
                {
                    u4TraceValue = WPS_INIT_SHUT_TRC;
                }
                else if ($3 != NULL)
                {
                    u4TraceValue = WPS_MGMT_TRC;
                }
                else if ($4 != NULL)
                {
                    u4TraceValue = WPS_DATA_PATH_TRC;
                }
                else if ($5 != NULL)
                {
                    u4TraceValue = WPS_CONTROL_PATH_TRC;
                }
                else if ($6 != NULL)
                {
                    u4TraceValue = WPS_DUMP_TRC;
                }
                else if ($7 != NULL)
                {
                    u4TraceValue = WPS_OS_RESOURCE_TRC;
                }
                else if ($8 != NULL)
                {
                    u4TraceValue = WPS_ALL_FAILURE_TRC;
                }
                else if ($9 != NULL)
                {
                    u4TraceValue = WPS_BUFFER_TRC;
                }
                else
                {
                     /*If no option is given, all traces will be enabled*/
                    u4TraceValue = WPS_INIT_SHUT_TRC| WPS_MGMT_TRC| WPS_DATA_PATH_TRC| WPS_CONTROL_PATH_TRC| WPS_DUMP_TRC|
                                   WPS_OS_RESOURCE_TRC| WPS_ALL_FAILURE_TRC| WPS_BUFFER_TRC;
                }
                
               cli_process_wps_command (CliHandle, CLI_WPS_DEBUG, NULL, u4TraceValue);

             }
   SYNTAX    : debug wps  { init-shutdown | mgmt | data | events | packet | os | failall | buffer | all }
   PRVID     : 15
   HELP      : Enables trace messages for wps 
   CXT_HELP : debug Configures trace for the protocol|
            wps wireless protected setup related configuration|
            init-shutdown Initialization and shutdown traces|
            mgmt Management traces|
            data Data path traces|
            events Event traces|
            packet Packet dump traces|
            os Traces related to all resources except buffers|
            failall All failure traces|
            buffer buffer traces|
            all All traces|
            <CR> Enables trace messages for wps.

 COMMAND   : no debug wps  [ { init-shutdown | mgmt | data | events | packet | os | failall | buffer | all } ]
 ACTION    : {
                UINT4 u4TraceValue = 0;

                if ($3 != NULL)
                {
                    u4TraceValue = WPS_INIT_SHUT_TRC;
                }
                else if ($4 != NULL)
                {
                    u4TraceValue = WPS_MGMT_TRC;
                }
                else if ($5 != NULL)
                {
                    u4TraceValue = WPS_DATA_PATH_TRC;
                }
                else if ($6 != NULL)
                {
                    u4TraceValue =WPS_CONTROL_PATH_TRC;
                }
                else if ($7 != NULL)
                {
                    u4TraceValue = WPS_DUMP_TRC;
                }
                else if ($8 != NULL)
                {
                    u4TraceValue = WPS_OS_RESOURCE_TRC;
                }
                else if ($9 != NULL)
                {
                    u4TraceValue = WPS_ALL_FAILURE_TRC;
                }
                else if ($10 != NULL)
                {
                    u4TraceValue = WPS_BUFFER_TRC;
                }
                else
                {
                    u4TraceValue = WPS_INIT_SHUT_TRC| WPS_MGMT_TRC| WPS_DATA_PATH_TRC| WPS_CONTROL_PATH_TRC| WPS_DUMP_TRC|
                                   WPS_OS_RESOURCE_TRC| WPS_ALL_FAILURE_TRC| WPS_BUFFER_TRC;
                }
                
               cli_process_wps_command  (CliHandle, CLI_WPS_NO_DEBUG, NULL, u4TraceValue);

             }
 SYNTAX    : no debug wps [ { init-shutdown | mgmt | data | events | packet | os | failall | buffer | all } ]
 PRVID     : 15
 HELP      : Disables trace messages for wps 
 CXT_HELP : no Disables the configuration and deletes the entry |
            debug Configures trace for the protocol|
            wps wireless protected setup related configuration|
            init-shutdown Initialization and shutdown traces|
            mgmt Management traces|
            data Data path traces|
            events Event traces|
            packet Packet dump traces|
            os Traces related to all resources except buffers|
            failall All failure traces|
            buffer buffer traces|
            all All traces|
            <CR> Disables trace messages for wps.
END GROUP

