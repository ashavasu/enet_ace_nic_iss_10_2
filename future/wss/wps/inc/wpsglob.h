/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: wpsglob.h,v 1.1.1.1 2016/09/20 10:42:41 siva Exp $
*
* Description: WPS File.
* *********************************************************************/

#ifndef __WPS_GLOB__
#define __WPS_GLOB__ 

tWpsGlobals gWpsGlobals;
tWpsOsixSemId    gWpsSemId;

#endif
