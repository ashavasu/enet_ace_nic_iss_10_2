/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: wpsmacr.h,v 1.1.1.1 2016/09/20 10:42:41 siva Exp $
 *
 * Description: WPS File.
 *********************************************************************/


#ifndef _WPSMACR_H
#define _WPSMACR_H

#define  EAP_REQUEST                        1  
#define  EAP_RESPONSE                       2  
#define  EAP_SUCCESS                        3  
#define  EAP_FAILURE                        4  

#define  WSC_START_LEN                      13
#define  WPS_STA_ASSOCIATED                 1
#define  WPS_STA_NOT_ASSOCIATED             0

#define  WPS_PROTOCOL_SEMAPHORE             ((UINT1*)"WPSSEM")
#define  WPS_BINARY_SEM                     1
#define  WPS_SEM_FLAGS                      0 /* unused */
#define  WPS_EAP_HEADER_SIZE                14


#define  WPS_MEMORY_TYPE                    MEM_DEFAULT_MEMORY_TYPE
#define  WPS_PARSE_ATTR_INFO_MEMPOOL_ID     gWpsGlobals.WpsPasrseAttrPoolId
#define  WPS_DATA_INFO_MEMPOOL_ID           gWpsGlobals.WpsDataInfoPoolId
#define  WPS_CONTEXT_INFO_MEMPOOL_ID        gWpsGlobals.WpsContextInfoPoolId
#define  WPS_WSS_INFO_MEMPOOL_ID            gWpsGlobals.WpsWssInfoPoolId
#define  WPS_STAT_INFO_MEMPOOL_ID           gWpsGlobals.WpsStatInfoPoolId
#define  WPS_EAP_PACKET_INFO_MEMPOOL_ID     gWpsGlobals.WpsEapPacketinfoPoolId
#define  WPS_TEMP_MEMPOOL_ID                gWpsGlobals.WpsTempPoolId
#define  WPS_PROBEREQ_MEMPOOL_ID           gWpsGlobals.WpsProbeReqPoolId


#define  WPS_PARSE_ATTR_INFO_MEMBLK_SIZE    sizeof(tWpsParseAttr)
#define  WPS_DATA_INFO_MEMBLK_SIZE          sizeof(tWpsSessionNode)
#define  WPS_CONTEXT_INFO_MEMBLK_SIZE       sizeof(tWpsContext)
#define  WPS_WSS_INFO_MEMBLK_SIZE           sizeof(tWssWpsNotifyParams)
#define  WPS_STAT_INFO_MEMBLK_SIZE          sizeof(tWpsStat)
#define  WPS_EAP_PACKET_MEMBLK_SIZE         sizeof(tWpsEapPacketinfo)
#define  WPS_TEMP_MEMBLK_SIZE               sizeof(tWpsBuf);
#define  WPS_PROBEREQ_MEMBLK_SIZE           sizeof(tWpsProbeReq)

#define  WPS_PORTINFO_MEMBLK_COUNT           BRG_MAX_PHY_PORTS
#define  WPS_SUPP_INFO_MEMBLK_COUNT          WPS_MAX_SUPP

#define  WPS_UNUSED(x)                       x = x; 

#define  WPS_HTONS                           OSIX_HTONS
#define  WPS_HTONL                           OSIX_HTONL
#define  WPS_NTOHS                           OSIX_NTOHS
#define  WPS_NTOHL                           OSIX_NTOHL
#define  WPS_MEMCPY                          MEMCPY
#define  WPS_MEMCMP                          MEMCMP
#define  WPS_MEMSET                          MEMSET

#define  WPS_GREATER                         1
#define  WPS_EQUAL                           0
#define  WPS_LESSER                         -1

#define  WPS_SUCCESS                         0
#define  WPS_FAILURE                         1

#define  WPS_INIT_VAL                        0
#define  WPS_INIT_COMPLETE(u4Status)         lrInitComplete(u4Status)
#define  WPS_QUEUE_EVENT                     0x01
#define  WPS_TIMER_EXP_EVENT                 0x02
#define  WPS_ALL_EVENTS                      (WPS_QUEUE_EVENT|WPS_TIMER_EXP_EVENT)

/* WPS task related macros */
#define  WPS_TASK_NAME                       ((UINT1 *)"WpsT")
#define  WPS_TASK_QUEUE_NAME                 ((UINT1 *)"WpsQ")
#define  WPS_TASK_ID                         gWpsGlobals.gWpsTaskId
#define  WPS_TASK_QUEUE_ID                   gWpsGlobals.gWpsTaskQId
#define  WPS_TASK_QUEUE_DEPTH                50

/*FSAP related calls*/
#define  WPS_GET_TASK_ID                     OsixGetTaskId

#define  WPS_CREATE_SEMAPHORE(au1Name, count, flags, psemid)         \
    OsixCreateSem((au1Name), (count), (flags), (psemid))


/*FSAP related calls*/
#define  tWpsOsixSemId                       tOsixSemId

#define  WPS_CREATE_SEMAPHORE(au1Name, count, flags, psemid)         \
    OsixCreateSem((au1Name), (count), (flags), (psemid))

#define  WPS_CREATE_MEM_POOL(x,y,pPoolId)                            \
    MemCreateMemPool(x,y,WPS_MEMORY_TYPE,(tMemPoolId*)pPoolId)

#define  WPS_DELETE_MEM_POOL(pPoolId)                                \
    MemDeleteMemPool((tMemPoolId) pPoolId)


/*WPS related constants*/
#define  WPS_INIT_VAL                        0
#define  WPS_INIT_COMPLETE(u4Status)         lrInitComplete(u4Status)

/* Macro for allocating memory block */
#define  WPS_MEM_ALLOCATE_MEM_BLK(WPS_MEMPOOL_ID)                    \
    MemAllocMemBlk(WPS_MEMPOOL_ID)

/*Macro for releasing memory block*/
#define  WPS_MEM_RELEASE_MEM_BLK(WPS_MEMPOOL_ID,pNode)               \
    MemReleaseMemBlock(WPS_MEMPOOL_ID,                          \
            (UINT1 *)pNode)

/***************** Pointer to Variable ******************/

#define  WPS_GET_1BYTE_PV(u1Val, pU1Buf, offset)                     \
    do{                                                         \
        u1Val = *((UINT1 *)((void *)(pU1Buf) + offset));         \
        offset += 1;                                             \
    }while(0)

#define  WPS_GET_2BYTE_PV(u2Val, pU2Buf, offset)                     \
    do{                                                         \
        u2Val = *((UINT2 *)((void *)(pU2Buf) + offset));         \
        u2Val = (UINT2)WPS_NTOHS(u2Val);                         \
        offset += 2;                                             \
    }while(0)

#define  WPS_GET_4BYTE_PV(u4Val, pU4Buf, offset)                     \
    do{                                                         \
        u4Val = *((UINT4 *)((void *)(pU4Buf) + offset));         \
        u4Val = (UINT4)WPS_NTOHL(u4Val)                          \
        offset += 4;                                             \
    }while(0)



/***************** Variable to Pointer ******************/

#define  WPS_GET_1BYTE_VP(pU1Buf, u1Val, offset)                     \
    do{                                                         \
        *((UINT1 *)((void *)(pU1Buf) + offset)) = u1Val;         \
        offset += 1;                                             \
    }while(0)

#define  WPS_GET_2BYTE_VP(pU2Buf, u2Val, offset)                     \
    do{                                                         \
        *((UINT2 *)((void *)pU2Buf + offset)) =                  \
        (UINT2)WPS_NTOHS(u2Val);                                 \
        offset += 2;                                             \
    }while(0)

#define  WPS_GET_4BYTE_VP(pU4Buf, u4Val, offset)                     \
    do{                                                         \
        *((UINT4 *)((void *)pU4Buf + offset))                    \
        = (UINT4)WPS_NTOHL(u4Val);                               \
        offset += 4;                                             \
    }while(0)


/***************** Pointer to Pointer ******************/

#define  WPS_GET_1BYTE_PP(p1U1Buf, p2U1Buf, offset)                  \
    do{                                                         \
        *((UINT1 *)((void *)p1U1Buf + offset)) =                 \
        *((UINT1 *)((void *)p2U1Buf + offset));                  \
        offset += 1;                                             \
    }while(0)

#define  WPS_GET_2BYTE_PP(p1U1Buf, p2U2Buf, offset)                  \
    do{                                                         \
        *((UINT2 *)((void *)p1U1Buf + offset)) =                 \
        (UINT2)WPS_NTOHS(*((UINT2 *)((void *)p2U2Buf + offset)));\
        offset += 2;                                             \
    }while(0)

#define  WPS_GET_4BYTE_PP(p1U4Buf, p2U4Buf, offset)                  \
    do{                                                         \
        *((UINT4 *)((void *)p1U4Buf + offset)) =                 \
        (UINT4)WPS_NTOHL(*((UINT4 *)((void *)p2U4Buf + offset)));\
        offset += 4;                                             \
    }while(0)



/*********************  PUT BYTE  ***********************/
/***************** Pointer to Variable ******************/

#define WPS_PUT_1BYTE_PV(u1Val, pU1Buf, offset)                      \
    do{                                                              \
        u1Val = *((UINT1 *)((void *)pU1Buf + offset));               \
        offset += 1;                                                 \
    }while(0)

#define WPS_PUT_2BYTE_PV(u2Val, pU2Buf, offset)                      \
    do{                                                              \
        u2Val = *((UINT2 *)((void *)pU2Buf + offset));               \
        u2Val = (UINT2)WPS_HTONS(u2Val)                              \
        offset += 2;                                                 \
    }while(0)

#define WPS_PUT_4BYTE_PV(u4Val, pU4Buf, offset)                      \
    do{                                                              \
        u4Val = *((UINT4 *)((void *)pU4Buf + offset));               \
        u4Val = (UINT4)WPS_HTONL(u4Val)                              \
        offset += 4;                                                 \
    }while(0)


/***************** Variable to Pointer ******************/

#define WPS_PUT_1BYTE_VP(pU1Buf, u1Val, offset)                      \
    do{                                                              \
        *((UINT1 *)((void *)pU1Buf + offset)) = u1Val;               \
        offset += 1;                                                 \
    }while(0)

#define WPS_PUT_2BYTE_VP(pU2Buf, u2Val, offset)                      \
    do{                                                              \
        *((UINT2 *)((void *)pU2Buf + offset)) =                      \
        (UINT2)WPS_HTONS(u2Val);                                     \
        offset += 2;                                                 \
    }while(0)

#define WPS_PUT_4BYTE_VP(pU4Buf, u4Val, offset)                      \
    do{                                                              \
        *((UINT4 *)((void *)pU4Buf + offset)) =                      \
        (UINT4)WPS_HTONL(u4Val);                                     \
        offset += 4;                                                 \
    }while(0)


/***************** Pointer to Pointer ******************/

#define WPS_PUT_1BYTE_PP(p1U1Buf, p2U2Buf, offset)                   \
    do{                                                              \
        *((UINT1 *)((void *)p1U1Buf + offset)) =                     \
        *((UINT1 *)((void *)p2U1Buf + offset));                      \
        offset += 1;                                                 \
    }while(0)

#define WPS_PUT_2BYTE_PP(p1U1Buf, p2U2Buf, offset)                   \
    do{                                                              \
        *((UINT2 *)((void *)p1U1Buf + offset)) =                     \
        (UINT2)WPS_HTONS(*((UINT2 *)((void *)p2U2Buf + offset)));    \
        offset += 2;                                                 \
    }while(0)

#define WPS_PUT_4BYTE_PP(p1U4Buf, p2U4Buf, offset)                   \
    do{                                                              \
        *((UINT4 *)((void *)p1U4Buf + offset)) =                     \
        (UINT4)WPS_HTONL(*((UINT4 *)((void *)p2U4Buf + offset)));    \
        offset += 4;                                                 \
    }while(0)

#define WPS_PUT_NBYTE_PP(p1U1Buf, p2U1Buf, unLen, offset) \
    do{ \
        for ( i=0; i<unLen; i++){ \
            *((UINT1 *)((void *)p1U1Buf + offset)) = *((UINT1 *)((void *)p2U1Buf + i)); \
            offset += 1; \
        } \
    }while(0)
#define WPS_GET_NBYTE_PP(unVal, pu1PktBuf, unLen, offset)                            \
    do{                                                                     \
        MEMCPY (unVal, pu1PktBuf+offset, unLen); \
        offset += unLen;  \
    }while(0)

/* Macros for getting data from linear buffer */
#define WPS_GET_1BYTE(u1Val, pu1PktBuf,offset) \
    do {                             \
        u1Val = *(pu1PktBuf+offset);           \
        offset += 1;        \
    } while(0)

#define WPS_GET_2BYTE(u2Val, pu1PktBuf,offset)            \
    do {                                        \
        WPS_MEMCPY (&u2Val, pu1PktBuf+offset, 2);\
        u2Val = (UINT2)WPS_NTOHS(u2Val);               \
        offset += 2;                   \
    } while(0)

#define WPS_GET_4BYTE(u4Val, pu1PktBuf,offset)              \
    do {                                          \
        WPS_MEMCPY (&u4Val, pu1PktBuf+offset, 4); \
        u4Val = WPS_NTOHL(u4Val);                 \
        offset += 4;                    \
    } while(0)


#endif /* _WPSMACR_H */
