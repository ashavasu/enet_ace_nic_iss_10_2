/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: wpssz.h,v 1.1.1.1 2016/09/20 10:42:41 siva Exp $
*
* Description: WPS File.
*********************************************************************/
#ifndef _WPSSZ_H_
#define _WPSSZ_H_

enum {
    MAX_WPS_PARSE_ATTR_INFO_SIZING_ID,
    MAX_WPS_DATA_INFO_SIZING_ID,
    MAX_WPS_CONTEXT_INFO_SIZING_ID,
    MAX_WPS_WSS_INFO_SIZING_ID,
    MAX_WPS_STAT_INFO_SIZING_ID,
    MAX_WPS_EAP_PACKET_INFO_SIZING_ID,    
    MAX_WPS_TEMP_INFO_SIZING_ID,    
    MAX_WPS_PROBEREQ_INFO_SIZING_ID,    
    WPS_MAX_SIZING_ID
};


#ifdef  _WPSSZ_C
tMemPoolId WPSMemPoolIds[ WPS_MAX_SIZING_ID];
INT4  WpsSizingMemCreateMemPools(VOID);
VOID  WpsSizingMemDeleteMemPools(VOID);
INT4  WpsSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _WPSSZ_C  */
extern tMemPoolId WPSMemPoolIds[ ];
extern INT4  WpsSizingMemCreateMemPools(VOID);
extern VOID  WpsSizingMemDeleteMemPools(VOID);
#endif /*  _WPSSZ_C  */


#ifdef  _WPSSZ_C
tFsModSizingParams FsWPSSizingParams [] = {
{ "tWpsParseAttr", "MAX_WPS_PARSE_ATTR_INFO", sizeof(tWpsParseAttr),MAX_WPS_PARSE_ATTR_INFO, MAX_WPS_PARSE_ATTR_INFO,0 },
{ "tWpsSessionNode", "MAX_WPS_DATA_INFO", sizeof(tWpsSessionNode),MAX_WPS_DATA_INFO, MAX_WPS_DATA_INFO,0 },
{ "tWpsContext", "MAX_WPS_CONTEXT_INFO", sizeof(tWpsContext),MAX_WPS_CONTEXT_INFO, MAX_WPS_CONTEXT_INFO,0 },
{ "tWssWpsNotifyParams", "MAX_WPS_WSS_INFO", sizeof(tWssWpsNotifyParams),MAX_WPS_WSS_INFO, MAX_WPS_WSS_INFO,0 },
{ "tWpsStat", "MAX_WPS_STAT_PACKET_INFO", sizeof(tWpsStat),MAX_WPS_STAT_INFO, MAX_WPS_STAT_INFO,0 },
{ "tWpsEapPacketInfo", "MAX_WPS_EAP_PACKET_INFO", sizeof(tWpsEapPacketInfo),MAX_WPS_EAP_PACKET_INFO, MAX_WPS_EAP_PACKET_INFO,0 },
{ "tWpsBuf", "MAX_WPS_TEMP_INFO", sizeof(tWpsBuf),MAX_WPS_TEMP_INFO, MAX_WPS_TEMP_INFO,0 },
{ "tWpsProbeReq", "MAX_WPS_PROBEREQ_INFO", sizeof(tWpsProbeReq),MAX_WPS_PROBEREQ_INFO, MAX_WPS_PROBEREQ_INFO,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _WPSSZ_C  */
extern tFsModSizingParams FsWPSSizingParams [];
#endif /*  _WPSSZ_C  */

#endif /* _WPSSZ_H_ */
