/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fswpslw.h,v 1.1.1.1 2016/09/20 10:42:41 siva Exp $
 *
 * Description: Proto types for Low Level  Routines
 *********************************************************************/


/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsWPSTraceOption ARG_LIST((INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsWPSTraceOption ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsWPSTable. */
INT1
nmhValidateIndexInstanceFsWPSTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsWPSTable  */

INT1
nmhGetFirstIndexFsWPSTable ARG_LIST((INT4 * , UINT4 *));


/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsWPSStatus ARG_LIST((INT4  , UINT4 ,INT4 *));


/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsWPSTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsWPSAuthSTAInfoTable. */
INT1
nmhValidateIndexInstanceFsWPSAuthSTAInfoTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsWPSAuthSTAInfoTable  */

INT1
nmhGetFirstIndexFsWPSAuthSTAInfoTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsWPSAuthSTAInfoTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/*INT1 nmhGetFsWPSTraceOption(INT4 *pi4RetValFsWPSTraceOption);
INT1 nmhTestv2FsWPSTraceOption(UINT4 *pu4ErrorCode, 
                               INT4 i4TestValFsWPSTraceOption);
*/
