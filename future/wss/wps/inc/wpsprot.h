/********************************************************************
 * * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * *
 * * $Id: wpsprot.h,v 1.2 2017/12/15 09:50:51 siva Exp $
 * *
 * * Description: WPS File.
 * *********************************************************************/
#ifndef _WPSPROT_H
#define _WPSPROT_H

#ifndef ETH_ALEN
#define ETH_ALEN 6
#endif

enum {
    MSG_EXCESSIVE, MSG_MSGDUMP, MSG_DEBUG, MSG_INFO, MSG_WARNING, MSG_ERROR
};

#define MACSTR "%02x:%02x:%02x:%02x:%02x:%02x"
#define WPS_SEC_DEVICE_TYPES 5

typedef UINT4 size_t;

typedef struct wpsbuf {
    INT4 i4used; 
    UINT1 au1Buf[1024]; 
}tWpsBuf;

typedef struct wpsProbeReq tWpsProbeReq;

typedef struct wpsConfigDb {
    UINT4 u4PushButtonStatus;
    UINT4 u4WpsWtpProfileId;
    UINT2 u2ConfigMethods;
    UINT2 u2WtpInternalId;
    BOOL1 b1WpsEnabled;
    BOOL1 b1WpsAdditionalIE;
    UINT1 au1Pad[2];
}tWpsConfigDb;

typedef struct WpsCredential {
    UINT4 u2ssidLen;
    UINT4 u4KeyLen;
    UINT4 u4CredAttrLen;
    UINT2 u2AuthType;
    UINT2 u2EncrType;
    UINT1 u1ssid[32];
    UINT1 u1Key[64];
    UINT1 *pCredAttr;
    UINT1 au1MacAddr[ETH_ALEN];
    UINT1  u1Au1Pad[2];
}tWpsCredential;

typedef struct wps_device_data {
    UINT4 u4OsVersion;
    INT4 p2p;
    UINT1 u1DeviceName[32];
    UINT1 u1Manufacturer[32];
    UINT1 u1ModelName[32];
    UINT1 u1ModelNumber[32];
    UINT1 u1SerialNumber[32];
    UINT1 u1PriDevType[WPS_DEV_TYPE_LEN];
    UINT1 u1SecDevType[WPS_SEC_DEVICE_TYPES][WPS_DEV_TYPE_LEN];
    UINT1 u1MacAddr[ETH_ALEN];
    UINT1 u1DeviceNameLen;
    UINT1 u1NumSecDevTypes;
    UINT1 rf_bands;
    UINT1 config_methods[2];
    UINT1 au1Pad[1];
}tWpsDeviceData;

typedef struct WpsContext {
    tRBNodeEmbd WpsContextNode;
    tWpsConfigDb pWpsConfigDb[NUM_OF_AP_SUPPORTED];
    tTMO_SLL  wpsStationInfoList; 
    tWpsTimer WpsPbcTmr;
    enum wps_state e1WpsState;
    UINT2 u2AuthTypeFlags;
    UINT2 u2EncrTypeFlags;
    UINT2 u2ApEncrType;
    UINT2 u2ApAuthType;
    UINT2 u2Port;
    UINT2 u2ap;
    UINT2 u2SelectedRegistrar;
    UINT2 u2ssidLen;
    UINT1  au1SrcMac[ETH_ALEN];    /* Mac Address of the  Interface */
    UINT1 u1AuthorizedMacs[WPS_MAX_AUTHORIZED_MACS][ETH_ALEN];
    UINT1 au1uuid[16];
    UINT1 au1ssid[32];
    UINT1 au1Psk[RSNA_MAX_PASS_PHRASE_LEN];
    UINT1 u1noOfAuthMac;
    UINT1 au1Pad[3];
}tWpsContext;


typedef struct WpsParseAttr {
    tWpsBuf M8Cred;
    UINT4 u4OsVersion; /* 4 octets */
    UINT4 u4EncrSettingsLen;
    UINT2 u2AuthTypeFlags; /* 2 octets */
    UINT2 u2EncrTypeFlags; /* 2 octets */
    UINT2 u2AssocState; /* 2 octets */
    UINT2 u2ConfigError; /* 2 octets */
    UINT2 u2DevPasswordId; /* 2 octets */
    UINT2 u2AuthType; /* 2 octets */
    UINT2 u2EncrType; /* 2 octets */
    UINT2 u2ApChannel; /* 2 octets */
    UINT2 u2RegistrarConfigurationMethods; /* 2 octets */
    UINT2 u2Manufacturer_len;
    UINT2 u2ModelNameLen;
    UINT2 u2ModelNumberLen;
    UINT2 u2DevNameLen;
    UINT2 u2SerialNumberLen;
    UINT2 u2PublicKeyLen; 
    UINT2 u2ssidLen;
    UINT2 u2SecDevTypeListLen;
    UINT1 au1EnrolleeNonce[16]; /* WPS_NONCE_LEN (16) octets */
    UINT1 au1RegistrarNonce[16]; /* WPS_NONCE_LEN (16) octets */
    UINT1 au1uuidR[16]; /* WPS_UUID_LEN (16) octets */
    UINT1 au1uuidE[16]; /* WPS_UUID_LEN (16) octets */
    UINT1 au1ConfigMethods[2]; /* 2 octets */
    UINT1 au1SelRegConfigMethods[2]; /* 2 octets */
    UINT1 au1PrimaryDevType[8]; /* 8 octets */
    UINT1 au1Authenticator[8]; /* WPS_AUTHENTICATOR_LEN (8) octets */
    UINT1 au1Rhash1[32]; /* WPS_HASH_LEN (32) octets */
    UINT1 au1Rhash2[32]; /* WPS_HASH_LEN (32) octets */
    UINT1 au1Ehash1[32]; /* WPS_HASH_LEN (32) octets */
    UINT1 au1Ehash2[32]; /* WPS_HASH_LEN (32) octets */
    UINT1 au1Rsnonce1[16]; /* WPS_SECRET_NONCE_LEN (16) octets */
    UINT1 au1Rsnonce2[16]; /* WPS_SECRET_NONCE_LEN (16) octets */
    UINT1 au1Esnonce1[16]; /* WPS_SECRET_NONCE_LEN (16) octets */
    UINT1 au1Esnonce2[16]; /* WPS_SECRET_NONCE_LEN (16) octets */
    UINT1 au1KeyWrapAuth[8]; /* WPS_KWA_LEN (8) octets */
    UINT1 au1MacAddr[6]; /* ETH_ALEN (6) octets */
    UINT1 au1Manufacturer[64];
    UINT1 au1ModelName[32];
    UINT1 au1ModelNumber[32];
    UINT1 au1SerialNumber[32];
    UINT1 au1DevName[32];
    UINT1 au1PublicKey[192];
    UINT1 au1EncrSettings[1024];
    UINT1 au1IV[16];
    UINT1 au1ssid[32]; /* <= 32 octets */
    UINT1 au1SecDevTypeList[128]; /* <= 128 octets */
    UINT1 u1Version; /* 1 octet */
    UINT1 u1Version2; /* 1 octet */
    UINT1 u1MsgType; /* 1 octet */
    UINT1 u1ConnTypeFlags; /* 1 octet */
    UINT1 u1WpsState; /* 1 octet */
    UINT1 u1RfBands; /* 1 octet */
    UINT1 u1NetworkIdx; /* 1 octet */
    UINT1 u1NetworkKeyIdx; /* 1 octet */
    UINT1 u1SelectedRegistrar; /* 1 octet (Bool) */
    UINT1 u1RequestType; /* 1 octet */
    UINT1 u1ResponseType; /* 1 octet */
    UINT1 u1ApSetupLocked; /* 1 octet */
    UINT1 u1SettingsDelayTime; /* 1 octet */
    UINT1 u1NetworkKeyShareable; /* 1 octet (Bool) */
    UINT1 u1RequestToEnroll; /* 1 octet (Bool) */
    UINT1 u1NumVendorExt;
}tWpsParseAttr;

typedef struct WpsSessionNode{
 tTMO_SLL_NODE   NextNode;
 tWpsParseAttr *pWpsParseAttr;
 tWpsContext *pWpsContext;
 struct wpsbuf last_msg;
 struct wpsbuf cur_msg;
 tWpsCredential cred;
 struct wps_device_data peer_dev;
 tWpsTimer WpsProtocolTmr; 
 tWpsTimer WpsReTransTmr; 
 tWpsTimer WpsMsgTmr;
 tPnacAuthSessionNode *pPnacAuthSessionNode;
 UINT4           u4SuppState;
 tWpsProbeReq *pWpsProbeReq;
 tWpsBuf *pDhSharedSecret;
        tPnacAuthSessionNode *pAuthSessNode;
 enum {
  RECV_M1, SEND_M2, RECV_M3, SEND_M4, RECV_M5, SEND_M6,
  RECV_M7, SEND_M8, RECV_DONE, SEND_M2D, RECV_M2D_ACK
 } e1state;
        UINT4 u4WtpProfileId;
 UINT2 u2ssidLen;
        UINT2 u2WtpInternalId;
 UINT2 u2SessId;
 UINT2 u2PortNum;
        UINT2 u2ConfigMethods;
 UINT2 au2DevPassword;
 UINT2 u2EncrType;
 UINT2 u2AuthType;
 UINT2 u2ConfigError;
 UINT2 u2ErrorIndication;
 UINT2 u2UsePskKey;
 UINT1 au1SuppMacAddr[ETH_ALEN]; 
 UINT1 au1ssid[32];
 UINT1 au1DeviceName[32];
 UINT1 manufacturer[32];
 UINT1 au1ModelName[32];
 UINT1 au1ModelNumber[32];
 UINT1 au1SerialNumber[32];
 UINT1 au1OsVersion[32];
 UINT1 au1uuidE[WPS_UUID_LEN];
 UINT1 au1uuidR[WPS_UUID_LEN];
 UINT1 au1NonceE[WPS_NONCE_LEN];
 UINT1 au1NonceR[WPS_NONCE_LEN];
 UINT1 au1Psk1[WPS_PSK_LEN];
 UINT1 au1Psk2[WPS_PSK_LEN];
 UINT1 au1snonce1[WPS_NONCE_LEN];
 UINT1 au1snonce2[WPS_NONCE_LEN];
 UINT1 au1PeerHash1[WPS_HASH_LEN];
 UINT1 au1PeerHash2[WPS_HASH_LEN];
 UINT1 au1DhPrivkey[32];
 UINT1 au1DhPubKeyE[192];
 UINT1 u1DhPubKeyELen;
 UINT1 au1DhPubkeyR[192];
 UINT1 u1DhPubKeyRlen;
 UINT1 au1WpsKdk[32];
 UINT1 au1AuthKey[WPS_AUTHKEY_LEN];
 UINT1 au1Keywrapkey[WPS_KEYWRAPKEY_LEN];
 UINT1 u1RfBands;
 UINT1 u1CurMsgLen;
 UINT1 u1MsgCount;
 BOOL1 b1Pbc;
 UINT1 u1Request_type;
 UINT1 au1Pad[1];
}tWpsSessionNode;

struct wpsProbeReq
{
 tRBNodeEmbd wpsProbeReqNode;
 tWpsTimer WpsSessionTmr; 
 tWpsSessionNode *pWpsSessionNode;
        UINT2           u2ssidLen;
 UINT2  u2SuppDeviePwd;
 UINT1           au1SuppMacAddr[ETH_ALEN];
 UINT1  au1ConfigMethods[2];
 UINT1  au1uuidE[16];
        UINT1           au1ssid[32];
 UINT1  au1VendorID[3];
        BOOL1           b1Pbc;
 BOOL1           b1ReqToEnroll;
 UINT1     au1Pad[3];
};

typedef struct WpsEapHdr {
    UINT1 u1Code;
    UINT1 u1Identifier;
    UINT2 u2Length;
    UINT1 u1Type;
    UINT1 au1VendorID[3];
    UINT1 au1VendorType[4];
    UINT1 u1OpCode;
    UINT1 u1Flags;
    UINT1 au1Data[1022];
}tWpsEapHdr;

typedef struct WpsEapPacketinfo {
    UINT1 u1Version;
    UINT1 u1Type;
    UINT2 u2Length;
    tWpsEapHdr WpsEapHdr;
    UINT2 u2MsgLen;
    UINT1 u4Retry;
    UINT1 u4PktLen;
    UINT1 *pBufffer;
    UINT4 u2PortNum;
    UINT1 *pWpsEapRetransmitTmr;
}tWpsEapPacketInfo;


INT4 WpsCompareProbeIndex(tRBElem * e1, tRBElem * e2);
INT4 WpsCompareDataIndex(tRBElem * e1, tRBElem * e2);
INT4 WpsCompareContextIndex(tRBElem * e1, tRBElem * e2);

VOID WpsAssignMempoolIds (VOID);
INT4 WpsCreateSemaphore (VOID);
VOID WpsUtilGetRandom (UINT1 *pu1Ptr, UINT1 u1Len);
INT4 WpsAddPortEntry (UINT4 u4IfIndex);
INT4 CapwapGetWtpBoardData (tWtpBoardData * pWtpBoardData, UINT4 *pMsgLen);
UINT1 wpsProcessEapMsg(tWpsEapPacketInfo *pWpsEapPacketInfo,
                       tWpsSessionNode *pWpsSessionNode,
                       UINT2 u2PortNum,
         UINT1 *pu1SrcAddr);
UINT1 wpsBuildM2(tWpsSessionNode  *pWpsSessionNode, tWpsEapPacketInfo *pWpsEapPacketinfo);
UINT1 wpsBuildM2D(tWpsSessionNode  *pWpsSessionNode, tWpsEapPacketInfo *pWpsEapPacketinfo);
UINT1 wpsBuildM4(tWpsSessionNode *pWpsSessionNode, tWpsEapPacketInfo *pWpsEapPacketinfo);
UINT1 wps_parse_vendor_ext_wfa(tWpsSessionNode *pWpsSessionNode, tWpsEapPacketInfo *pWpsEapPacketinfo, UINT4 elen, UINT4 offset);
UINT1 wps_parse_vendor_ext(tWpsSessionNode *pWpsSessionNode, tWpsEapPacketInfo *pWpsEapPacketinfo, UINT4 len,UINT4 offset);
UINT1 wpsValidateMsg(tWpsEapPacketInfo *pWpsEapPacketinfo, tWpsSessionNode *pWpsSessionNode);
UINT1 wpsProcessM1(tWpsSessionNode *pWpsSessionNode);
UINT1 wpsProcessM3(tWpsSessionNode *pWpsSessionNode, tWpsEapPacketInfo *pWpsEapPacketinfo);
UINT1 wpsBuildEncrSettings(tWpsSessionNode *pWpsSessionNode,struct wpsbuf *msg,struct wpsbuf *plain);
VOID wpsBuildKeyWrapAuth(tWpsSessionNode *pWpsSessionNode, UINT1 nonce[16], UINT1 nonceType);
UINT1 wpsDecryptNonce(tWpsSessionNode *pWpsSessionNode, UINT1 wpsValidatePsk);
UINT1 wpsProcessM5(tWpsSessionNode *pWpsSessionNode, tWpsEapPacketInfo *pWpsEapPacketinfo);
UINT4 wpsProcessESnonce1(tWpsSessionNode *pWpsSessionNode);
UINT4 wpsProcessKeyWrapAuth(tWpsSessionNode *pWpsSessionNode, 
                            tWpsBuf *message);
UINT1 wpsProcessM7(tWpsSessionNode *pWpsSessionNode, tWpsEapPacketInfo *pWpsEapPacketinfo);
struct wpsbuf * wpsDecryptEncrSettings(tWpsSessionNode *pWpsSessionNode,UINT1 *encr_settings,UINT2 encr_settings_len);
UINT4 wpsProcessESnonce2(tWpsSessionNode *pWpsSessionNode);
UINT1 wpsBuildM6(tWpsSessionNode  *pWpsSessionNode, tWpsEapPacketInfo *pWpsEapPacketinfo);
UINT1 wpsBuildM8(tWpsSessionNode  *pWpsSessionNode, tWpsEapPacketInfo *pWpsEapPacketinfo);
VOID wpsAes128CbcEncrypt(UINT1 *keyWrapKey,UINT1 *data,UINT1 *length);
VOID wpsBuildCredential(tWpsBuf * cred, 
                         tWpsCredential * pWpsCredential,INT4 *i4CredOffset);
VOID wpsBuildCred(tWpsSessionNode *pWpsSessionNode);

tWpsStat*
WpsUtilGetStationFromStatsTable (UINT2 u2PortNum,UINT1 *pu1Addr);
INT4 wpsSha256(const UINT1 *key, INT4 key_len, INT4 num_elem,
                       const UINT1 *addr[], const INT4 *len, UINT1 *mac);

UINT1 WpsHandleM1(tWpsSessionNode *pWpsSessionNode, 
                  tWpsEapPacketInfo *pWpsEapPacketInfo);
UINT1 WpsHandleM3(tWpsSessionNode *pWpsSessionNode, tWpsEapPacketInfo *pWpsEapPacketinfo);
UINT1 WpsHandleM5(tWpsSessionNode *pWpsSessionNode, tWpsEapPacketInfo *pWpsEapPacketinfo);
UINT1 WpsHandleM7(tWpsSessionNode *pWpsSessionNode, tWpsEapPacketInfo *pWpsEapPacketinfo);
UINT1 WpsHandleDone(tWpsSessionNode *pWpsSessionNode, tWpsEapPacketInfo *pWpsEapPacketinfo);
VOID wpsBuildMsgResp(UINT1 MsgType,tWpsSessionNode *pWpsSessionNode,tWpsEapPacketInfo *pWpsEapPacketInfo);
tWpsStat*
WpsUtilAddStaToStatsTable(UINT2 u2PortNum,UINT4 u4StatsIndex,UINT1 *pu1SuppAddr);
UINT4
WpsUtilDeleteSuppStatsInfo(tWpsStat * pWpsStat);
VOID
WpsUtilGetFirstSuppStatsEntry (tWpsStat** ppWpsStat);
VOID
WpsUtilGetNextSuppStatsEntry (tWpsStat * pWpsStat,
                           tWpsStat ** ppWpsStat);
UINT4
WpsTxFrame (tWpsEapPacketInfo *pu1Frame, tMacAddr StaMac,
             UINT4 u4BssIfIndex, INT4 i4BufLen);
UINT4
WpsPnacTxFrame (UINT1 *pu1Frame, UINT4 u4PktLen, UINT2 u2PortNum);
tPnacAuthSessionNode *
WpsSessCreatePnacAuthSess (UINT1 *pu1SrcAddr,
                            UINT2 u2PortNum, BOOL1 bTriggerPnacStateMachine);
UINT1
wpsDeleteSession(tWpsSessionNode *pWpsSessionNode);
VOID
WpsGetMacEntry (UINT1 *macAddr, tWpsProbeReq ** ppProbeInfo);
INT4
WpsDeletePortEntry (UINT4 u4IfIndex);
VOID
WpsGetFirstPortEntry (tWpsContext ** pWpsPortEntry);
VOID
WpsGetNextPortEntry (tWpsContext* pWpsPortEntry,
                      tWpsContext** pNextWpsPortEntry);
tWpsSessionNode *
WpsUtilGetStaInfoFromStaAddr (tWpsContext* pWpsContextinfo,
                               UINT1 *pu1Addr);
tWpsSessionNode *
WpsUtilCreateAndAddStaToList (tWpsContext* pWpsContextInfo,
                               UINT1 *pu1SuppAddr);
UINT1
WpsSessHandleNewStation
            (tWpsContext *pWpsPaePortInfo, tWpsSessionNode *pWpsDataInfo,UINT4 u4BssIfIndex);
UINT1
WpsProcAssocInd (tWssWpsNotifyParams * pWssWpsNotifyParams);
VOID
WpsProcProbeReqInd (tWssWpsNotifyParams * pWssWpsNotifyParams);
UINT4 WpsSessDeleteSession(tWpsSessionNode *pWpsSessionNode);
VOID
WpsGetPortEntry (UINT2 u2PortNum, tWpsContext ** ppPortInfo);
VOID WpsBuildEapHdr(tWpsEapHdr *WpsEapHdr,
                    UINT1 op_code);
UINT1 sessionOverlap(tWpsSessionNode *pWpsSessionNode);
VOID wpsSendEapFail(tWpsEapPacketInfo *pWpsEapPacketInfo,
                    UINT2 u2PortNum,
                    tMacAddr macAddr);
VOID wpsEapSendStart(tMacAddr macAddr,
                     UINT2 u2PortNo );
UINT1 WpsProcessEap(UINT1 *pu1Pkt,
                    UINT2 u2PktLen,
                    UINT2 u2PortNum,
                    UINT1 *pu1SrcAddr);
UINT1 wpsGenerateKeys(tWpsSessionNode *wps_data);
VOID generateRHASH(tWpsSessionNode *wps_data);
VOID wpsBuildAuthenticator(tWpsSessionNode  *pWpsSessionNode, 
                           tWpsEapPacketInfo *pWpsEapPacketInfo,
                           INT4 i4offset, 
                           UINT1 *au1Hash);
INT1 WpsSetWPSAdditionalIE(INT4 i4IfIndex,
                           INT4 pi4RetValFsWPSAdditionalIE);
INT1 WpsTestv2WPSEnabled(UINT4 *pu4ErrorCode,
                         INT4 i4IfIndex,
                         INT4 i4TestValFsWPSStatus);
INT1 WpsTestv2WPSAdditionalIE(UINT4 *pu4ErrorCode,
                              INT4 i4IfIndex,
                              INT4 i4TestValFsWPSAdditionalIE);
INT1 WpsGetWPSAdditionalIE(INT4 i4IfIndex, 
                           UINT2 u2WtpInternalId,
                           INT4 *pi4RetValFsWPSAdditionalIE);
INT1 WpsSetWPSEnabled(INT4 i4IfIndex,
                      INT4 i4SetValFsWPSStatus);
VOID WpsEnQFrameToWpsTask (tWssWpsNotifyParams* pMsg);
INT1
WpsValidateIndexInstanceFsWPSAuthSTAInfoTable(INT4 i4IfIndex, UINT4 u4FsWPSStatsIndex);
INT1
WpsGetFirstIndexFsWPSAuthSTAInfoTable(INT4 *pi4IfIndex, UINT4 *pu4FsWPSStatsIndex);
INT1 WpsGetPBCStatus(INT4 i4IfIndex,
                     UINT2 u2WtpInternalId,
                     INT4 *pi4RetValFsWPSPushButtonStatus);
UINT1
WpsProcDisAssocInd (tWssWpsNotifyParams * pWssWpsNotifyParams);
VOID WpsAlgoHmacSha2(eShaVersion whichSha,UINT1 *u1Data[],INT4 *i4len,
                     UINT1 u1NoElements,UINT1 *u1Key,UINT2 u2KeyLen,
                     UINT1 *u1Pu1Digest);
VOID
WpsProcPnacMsgInd (tWssWpsNotifyParams * pWssWpsNotifyParams);
VOID
WpsProcPnacReqInd (tWssWpsNotifyParams * pWssWpsNotifyParams);
VOID WpsInterfaceHandleQueueEvent (VOID);
tWpsBuf * wpsDeriveSharedKey(void *ctx,tWpsBuf *peerPublic);
void * wpsDhInitilization(tWpsBuf **privKey, tWpsBuf **pubKey);
UINT1 wpsGenerateDHSecretKey(tWpsSessionNode *wps_data);
VOID  wpsGenerateKDK(tWpsSessionNode *wps_data);
void wpsKdf(UINT1 *u1Key, UINT1 *u1LabelPrefix, INT4 u4LabelPrefixLen,
      INT1 *u1Label, UINT1 *u1Res, INT4 u4ResLen);
void wpsDerivePsk(tWpsSessionNode *wps, UINT1 *u1DevPasswd,
      INT4 u4DevPasswdLen);
tWpsBuf * wpabuf_zeropad(tWpsBuf *buf, INT4 u4Len);
UINT1
WpsProcPbcInd (tWssWpsNotifyParams * pWssWpsNotifyParams);

VOID wpsBuildM2DIEs(tWpsSessionNode *pWpsSessionNode,tWpsEapPacketInfo *pWpsEapPacketInfo,
                   INT4 *i4offset);
VOID wpsBuildEncSettings(tWpsSessionNode *pWpsSessionNode,tWpsEapPacketInfo *pWpsEapPacketInfo,
                   INT4 *i4offset);
VOID wpsBuildRHash(tWpsSessionNode *pWpsSessionNode,tWpsEapPacketInfo *pWpsEapPacketInfo,
                   INT4 *i4offset);
VOID wpsBuildM2IEs(tWpsSessionNode *pWpsSessionNode,tWpsEapPacketInfo *pWpsEapPacketInfo,
                   INT4 *i4offset);


PUBLIC VOID
RsnaGetPortEntry (UINT2 u2PortNum, tRsnaPaePortEntry ** ppPortInfo);
PUBLIC INT4         CapwapCheckModuleStatus (VOID);
PUBLIC UINT1
AesArCbcEncrypt (UINT1 *pu1ArBuffer, INT4 i4ArSize, UINT2 u2ArInKeyLen,
                 UINT1 *pu1ArCryptoKey, UINT1 *pu1ArInitVect);
PUBLIC UINT1
AesArCbcDecrypt (UINT1 *pu1ArBuffer, INT4 i4ArSize, UINT2 u2ArInKeyLen,
                 UINT1 *pu1ArCryptoKey, UINT1 *pu1ArInitVect);
PUBLIC INT1
nmhGetCapwapDot11WlanProfileIfIndex (UINT4 u4CapwapDot11WlanProfileId,
                                     INT4
                                     *pi4RetValCapwapDot11WlanProfileIfIndex);
PUBLIC INT4 dh5DerivedSharedKey(UINT1 *sharedSecretKey,INT4 *secretKeyLen,void *ctx, 
    UINT1 *peerPublicKey,INT4 enKeyLen);
PUBLIC VOID * dhInit(INT4 *publicKeyLen,INT4 *privateKeyLen,UINT1 *publicKey,
       UINT1 *privateKey);
PUBLIC UINT1
WssWlanGetWlanInternalId (UINT4 u4WlanProfileId, UINT2 *pu2WlanInternalId);
PUBLIC VOID
WssWlanGetWlanProfileId (UINT2 u2WlanInternalId, UINT2 *pu2WlanProfileId);



#endif /* _WPSPROT_H_ */
