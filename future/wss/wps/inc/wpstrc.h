/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: wpstrc.h,v 1.1.1.1 2016/09/20 10:42:41 siva Exp $
*
* Description: WPS File.
*********************************************************************/
#ifndef _WPSTRC_H_
#define _WPSTRC_H_

/* Trace and debug flags */
#define   WPS_TRC_FLAG       gWpsGlobals.u4WpsTrcOpt     

/* Module names */
#define   WPS_MOD_NAME           ((const char *)"WPS")

#define   WPS_INIT_SHUT_TRC      INIT_SHUT_TRC     
#define   WPS_MGMT_TRC           MGMT_TRC          
#define   WPS_DATA_PATH_TRC      DATA_PATH_TRC     
#define   WPS_CONTROL_PATH_TRC   CONTROL_PLANE_TRC 
#define   WPS_DUMP_TRC           DUMP_TRC          
#define   WPS_OS_RESOURCE_TRC    OS_RESOURCE_TRC   
#define   WPS_ALL_FAILURE_TRC    ALL_FAILURE_TRC   
#define   WPS_BUFFER_TRC         BUFFER_TRC        

/* Trace definitions */
#ifdef  TRACE_WANTED

#define WPS_PKT_DUMP(TraceType, pBuf, Length, Str)                           \
        MOD_PKT_DUMP(WPS_TRC_FLAG, TraceType, WPS_MOD_NAME, pBuf, Length, (const char *)Str)

#define WPS_TRC(TraceType, Str)                                              \
        MOD_TRC(WPS_TRC_FLAG, TraceType, WPS_MOD_NAME, (const char *)Str)

#define WPS_TRC_ARG1(TraceType, Str, Arg1)                                   \
        MOD_TRC_ARG1(WPS_TRC_FLAG, TraceType, WPS_MOD_NAME, (const char *)Str, Arg1)

#define WPS_TRC_ARG2(TraceType, Str, Arg1, Arg2)                             \
        MOD_TRC_ARG2(WPS_TRC_FLAG, TraceType, WPS_MOD_NAME, (const char *)Str, Arg1, Arg2)

#define WPS_TRC_ARG3(TraceType, Str, Arg1, Arg2, Arg3)                       \
        MOD_TRC_ARG3(WPS_TRC_FLAG, TraceType, WPS_MOD_NAME, (const char *)Str, Arg1, Arg2, Arg3)

#define WPS_TRC_ARG4(TraceType, Str, Arg1, Arg2, Arg3,Arg4)                       \
        MOD_TRC_ARG4(WPS_TRC_FLAG, TraceType, WPS_MOD_NAME, (const char *)Str, Arg1, Arg2, Arg3,Arg4)

#define WPS_TRC_ARG5(TraceType, Str, Arg1, Arg2, Arg3,Arg4,Arg5)                       \
        MOD_TRC_ARG5(WPS_TRC_FLAG, TraceType, WPS_MOD_NAME, (const char *)Str, Arg1, Arg2, Arg3,Arg4,Arg5)

#define WPS_TRC_ARG6(TraceType, Str, Arg1, Arg2, Arg3,Arg4,Arg5,Arg6)                       \
        MOD_TRC_ARG6(WPS_TRC_FLAG, TraceType, WPS_MOD_NAME, (const char *)Str, Arg1, Arg2, Arg3,Arg4,Arg5,Arg6)


#define   WPS_DEBUG(x) {x}

#else  /* TRACE_WANTED */

#define WPS_PKT_DUMP(TraceType, pBuf, Length, Str)
#define WPS_TRC(TraceType, Str)
#define WPS_TRC_ARG1(TraceType, Str, Arg1)
#define WPS_TRC_ARG2(TraceType, Str, Arg1, Arg2)
#define WPS_TRC_ARG3(TraceType, Str, Arg1, Arg2, Arg3)
#define WPS_TRC_ARG4(TraceType, Str, Arg1, Arg2, Arg3,Arg4)
#define WPS_TRC_ARG5(TraceType, Str, Arg1, Arg2, Arg3,Arg4,Arg5)
#define WPS_TRC_ARG6(TraceType, Str, Arg1, Arg2, Arg3,Arg4,Arg5,Arg6)

#define   WPS_DEBUG(x)

#endif /* TRACE_WANTED */

#endif/* _WPSTRC_H_ */

