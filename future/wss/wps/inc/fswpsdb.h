/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fswpsdb.h,v 1.1.1.1 2016/09/20 10:42:41 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/

#ifndef _FSWPSDB_H
#define _FSWPSDB_H

UINT1 FsWPSTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsWPSAuthSTAInfoTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};

UINT4 fswps [] ={1,3,6,1,4,1,29601,2,106};
tSNMP_OID_TYPE fswpsOID = {9, fswps};


UINT4 FsWPSTraceOption [ ] ={1,3,6,1,4,1,29601,2,106,1};
UINT4 FsWPSSetStatus [ ] ={1,3,6,1,4,1,29601,2,106,2,1,1};
UINT4 FsWPSPushButtonStatus [ ] ={1,3,6,1,4,1,29601,2,106,2,1,2};
UINT4 FsWPSAdditionalIE [ ] ={1,3,6,1,4,1,29601,2,106,2,1,3};
UINT4 FsWPSStatus [ ] ={1,3,6,1,4,1,29601,2,106,2,1,4};
UINT4 FsWPSStatsIndex [ ] ={1,3,6,1,4,1,29601,2,106,3,1,1};
UINT4 FsWpsStaMacAddress [ ] ={1,3,6,1,4,1,29601,2,106,3,1,2};




tMbDbEntry fswpsMibEntry[]= {

{{10,FsWPSTraceOption}, NULL, FsWPSTraceOptionGet, FsWPSTraceOptionSet, FsWPSTraceOptionTest, FsWPSTraceOptionDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{12,FsWPSSetStatus}, GetNextIndexFsWPSTable, FsWPSSetStatusGet, FsWPSSetStatusSet, FsWPSSetStatusTest, FsWPSTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsWPSTableINDEX, 2, 0, 0, "2"},

{{12,FsWPSPushButtonStatus}, GetNextIndexFsWPSTable, FsWPSPushButtonStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsWPSTableINDEX, 2, 0, 0, "4"},

{{12,FsWPSAdditionalIE}, GetNextIndexFsWPSTable, FsWPSAdditionalIEGet, FsWPSAdditionalIESet, FsWPSAdditionalIETest, FsWPSTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsWPSTableINDEX, 2, 0, 0, "2"},

{{12,FsWPSStatus}, GetNextIndexFsWPSTable, FsWPSStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsWPSTableINDEX, 2, 0, 0, "2"},

{{12,FsWPSStatsIndex}, GetNextIndexFsWPSAuthSTAInfoTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsWPSAuthSTAInfoTableINDEX, 2, 0, 0, NULL},

{{12,FsWpsStaMacAddress}, GetNextIndexFsWPSAuthSTAInfoTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_NOACCESS, FsWPSAuthSTAInfoTableINDEX, 2, 0, 0, NULL},
};
tMibData fswpsEntry = { 7, fswpsMibEntry };

#endif /* _FSWPSDB_H */

