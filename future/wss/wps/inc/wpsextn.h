/*******************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: wpsextn.h,v 1.1.1.1 2016/09/20 10:42:41 siva Exp $
*
* Description: WPS File.
* *********************************************************************/

#ifndef _WPSEXTN_H_
#define _WPSEXTN_H_

extern tWpsGlobals gWpsGlobals;
extern tWpsOsixSemId    gWpsSemId;

#endif
