/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fswpswr.h,v 1.1.1.1 2016/09/20 10:42:41 siva Exp $
*
* Description: WPS File.
**********************************************************************/

#ifndef _FSWPSWR_H
#define _FSWPSWR_H

VOID RegisterFSWPS(VOID);

VOID UnRegisterFSWPS(VOID);
INT4 FsWPSTraceOptionGet(tSnmpIndex *, tRetVal *);
INT4 FsWPSTraceOptionSet(tSnmpIndex *, tRetVal *);
INT4 FsWPSTraceOptionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsWPSTraceOptionDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 GetNextIndexFsWPSTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsWPSSetStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsWPSPushButtonStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsWPSAdditionalIEGet(tSnmpIndex *, tRetVal *);
INT4 FsWPSStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsWPSSetStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsWPSAdditionalIESet(tSnmpIndex *, tRetVal *);
INT4 FsWPSSetStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsWPSAdditionalIETest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsWPSTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsWPSAuthSTAInfoTable(tSnmpIndex *, tSnmpIndex *);
#endif /* _FSWPSWR_H */
