/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: wpsinit.c,v 1.1.1.1 2016/09/20 10:42:41 siva Exp $
 *
 * Description : This file contains init functions for Wps Module 
 *********************************************************************/

#ifndef _WPSINIT_C
#define _WPSINIT_C

#include "wpsinc.h"
tWpsGlobals gWpsGlobals;
tWpsOsixSemId    gWpsSemId;

/****************************************************************************
 *                                                                           *
 * Function     : WpsInitModuleStart                                        *
 *                                                                           *
 * Description  : This function creates mempools used by the wps module     *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : WPS_SUCCESS/WPS_FAILURE                                  *
 *                                                                           *
 *****************************************************************************/
UINT4 WpsInitModuleStart (VOID)
{

    WPS_MEMSET (&gWpsGlobals, WPS_INIT_VAL, sizeof (tWpsGlobals));
    gWpsGlobals.u4WpsTrcOpt = 0;
    WPS_TRC (WPS_INIT_SHUT_TRC | WPS_CONTROL_PATH_TRC, 
            "Initializing WPS.....\n");

    if (WpsSizingMemCreateMemPools () != WPS_SUCCESS)
    {
        WPS_TRC (WPS_INIT_SHUT_TRC | WPS_CONTROL_PATH_TRC |
                WPS_ALL_FAILURE_TRC, " Create mem pool for Pnac failed\n");
        return WPS_FAILURE;
    }

    WpsAssignMempoolIds ();

    /* Initialise the timer module */
    if (WpsTmrInit () != WPS_SUCCESS)
    {
        WPS_TRC (WPS_INIT_SHUT_TRC | WPS_CONTROL_PATH_TRC |
                WPS_ALL_FAILURE_TRC, " Timer Init Failed\n");
        WpsSizingMemDeleteMemPools ();
        return WPS_FAILURE;
    }

    gWpsGlobals.WpsContextTable=
        RBTreeCreateEmbedded ((FSAP_OFFSETOF (tWpsContext, 
                        WpsContextNode)), 
                WpsCompareContextIndex);

    if (gWpsGlobals.WpsContextTable== NULL)
    {
        WPS_TRC (WPS_INIT_SHUT_TRC | WPS_CONTROL_PATH_TRC |
                WPS_ALL_FAILURE_TRC, "RB Tree Creation for "
                " WPS context Table Failed\n");
        WpsSizingMemDeleteMemPools ();
        return WPS_FAILURE;
    }

    gWpsGlobals.WpsDataTable=
        RBTreeCreateEmbedded ((FSAP_OFFSETOF (tWpsStat, 
                        WpsStatsNode)), 
                WpsCompareDataIndex);
    if (gWpsGlobals.WpsDataTable== NULL)
    {
        WPS_TRC (WPS_INIT_SHUT_TRC | WPS_CONTROL_PATH_TRC |
                WPS_ALL_FAILURE_TRC, "RB Tree Creation for "
                " WPS data Table Failed\n");
        WpsSizingMemDeleteMemPools ();
        return WPS_FAILURE;
    }
    gWpsGlobals.WpsProbeReqTable =
      		RBTreeCreateEmbedded ((FSAP_OFFSETOF (tWpsProbeReq, 
      					wpsProbeReqNode)), 
				      WpsCompareProbeIndex);
      if (gWpsGlobals.WpsProbeReqTable == NULL)
      {
      WPS_TRC (WPS_INIT_SHUT_TRC | WPS_CONTROL_PATH_TRC |
      WPS_ALL_FAILURE_TRC, "RB Tree Creation for "
      " WPS EAP Table Failed\n");
      WpsSizingMemDeleteMemPools ();
      return WPS_FAILURE;
      }

    if (WpsCreateSemaphore () != WPS_SUCCESS)
    {
        WPS_TRC (WPS_INIT_SHUT_TRC | WPS_CONTROL_PATH_TRC |
                WPS_ALL_FAILURE_TRC, " Create Semaphore for Wps failed\n");
        return WPS_FAILURE;

    }

    WPS_TRC (WPS_INIT_SHUT_TRC | WPS_CONTROL_PATH_TRC, 
            "Initialized WPS.....\n");

    return WPS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : WpsCreateSemaphore                                  */
/*                                                                           */
/* Description        : This function creates the protocol semaphore.        */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : All variables in the GLobal structure are            */
/*                      initialised.                                         */
/*                                                                           */
/* Return Value(s)    : WPS_SUCCESS - On success                            */
/*                      WPS_FAILURE - On failure                            */
/*****************************************************************************/

INT4 WpsCreateSemaphore (VOID)
{

    if (WPS_CREATE_SEMAPHORE
            (WPS_PROTOCOL_SEMAPHORE, WPS_BINARY_SEM, WPS_SEM_FLAGS, 
             &gWpsSemId) != OSIX_SUCCESS)
    {
        WPS_TRC (WPS_INIT_SHUT_TRC | WPS_CONTROL_PATH_TRC |
                WPS_ALL_FAILURE_TRC, " Create Semaphore for Wps failed\n");
        return WPS_FAILURE;

    }

    return WPS_SUCCESS;

}
/*****************************************************************************/
/* Function Name      : WpsAssignMempoolIds                                 */
/*                                                                           */
/* Description        : This function assign mempools from sz.c ie.          */
/*                      WpsMemPoolIds to global mempoolIds.                 */
/*                                                                           */
/* Input(s)           :  None                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/

VOID WpsAssignMempoolIds (VOID)
{

    WPS_PARSE_ATTR_INFO_MEMPOOL_ID = WPSMemPoolIds[MAX_WPS_PARSE_ATTR_INFO_SIZING_ID];

    WPS_DATA_INFO_MEMPOOL_ID = WPSMemPoolIds[MAX_WPS_DATA_INFO_SIZING_ID];

    WPS_CONTEXT_INFO_MEMPOOL_ID = WPSMemPoolIds[MAX_WPS_CONTEXT_INFO_SIZING_ID];

    WPS_WSS_INFO_MEMPOOL_ID = WPSMemPoolIds[MAX_WPS_WSS_INFO_SIZING_ID];

    WPS_STAT_INFO_MEMPOOL_ID = WPSMemPoolIds[MAX_WPS_STAT_INFO_SIZING_ID];

    WPS_EAP_PACKET_INFO_MEMPOOL_ID = WPSMemPoolIds[MAX_WPS_EAP_PACKET_INFO_SIZING_ID];

    WPS_TEMP_MEMPOOL_ID = WPSMemPoolIds[MAX_WPS_TEMP_INFO_SIZING_ID];

    WPS_PROBEREQ_MEMPOOL_ID = WPSMemPoolIds[MAX_WPS_PROBEREQ_INFO_SIZING_ID];
}

#endif /* _WPSINIT_C */
