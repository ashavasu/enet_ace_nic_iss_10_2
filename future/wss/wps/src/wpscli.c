/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: wpscli.c,v 1.1.1.1 2016/09/20 10:42:41 siva Exp $
 *
 * Description: Protocol Low Level Routines
 *********************************************************************/

/* SOURCE FILE HEADER :
 *
 *  ---------------------------------------------------------------------------
 * |  FILE NAME             : wpscli.c                                         |
 * |                                                                           |
 * |  PRINCIPAL AUTHOR      :                                                  |
 * |                                                                           |
 * |  SUBSYSTEM NAME        : WPS                                              |
 * |                                                                           |
 * |  MODULE NAME           : WPS CLI configurations                           |
 * |                                                                           |
 * |  LANGUAGE              : C                                                |
 * |                                                                           |
 * |  TARGET ENVIRONMENT    :                                                  |
 * |                                                                           |
 * |  DATE OF FIRST RELEASE :                                                  |
 * |                                                                           |
 * |  DESCRIPTION           : CLI interfaces for WPS MIB.                      |
 *  ---------------------------------------------------------------------------
 *
 */

#ifndef WPSCLI_C
#define WPSCLI_C

#include "wpsinc.h"
#include "fswpslw.h"
#include "capwapclilwg.h"
#include "capwapcli.h"

/*****************************************************************************/
/*                                                                           */
/* FUNCTION NAME    : cli_process_wps_cmd                                    */
/*                                                                           */
/* DESCRIPTION      : This function takes in variable no. of arguments       */
/*                    and process the commands for the WPS module.           */
/*                    defined in wpscli.h                                    */
/*                                                                           */
/* INPUT            : CliHandle -  CLIHandler                                */
/*                    u4Command -  Command Identifier                        */
/*                                                                           */
/* OUTPUT           : NONE                                                   */
/*                                                                           */
/* RETURNS          : CLI_FAILURE/CLI_SUCCESS                                */
/*                                                                           */
/*****************************************************************************/

INT4 cli_process_wps_command (tCliHandle CliHandle, 
                              INT4 u4Command, 
                              ...)
{
    va_list             ap;
    INT1              *args[CLI_MAX_ARGS] = { NULL };
    UINT4               u4WlanId = 0;    /* Wlan Interface Id */
    UINT4               u4ProfileId = 0; /* Profile Id */
    UINT4               u4ErrCode = 0;    /* Used to set the 
                                          * error code */
    INT4                i4RetStatus = 0;
    INT1               u1Status = 0;    /* Enable / Disable */
    INT4               u4IsWpsEnabled = 0;    /* Enable / Disable */
    INT1               u1Argno = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               currentProfileId = 0;
    UINT4               u4ApProfileId =0;

    va_start (ap, u4Command);

    while (u1Argno < CLI_MAX_ARGS)
    {
        args[u1Argno++] = va_arg (ap, INT1 *);
    }

    CLI_SET_ERR (0);
    va_end (ap);

    CliRegisterLock (CliHandle, PnacLock, PnacUnLock);
    PNAC_LOCK ();
    switch (u4Command)
    {
	/*Enables or disables the WPS fetaure */
	case CLI_WPS_STATUS:
	    u1Status = (*(INT1 *) (args[WPS_CLI_FIRST_ARG]));

	    WPS_MEMCPY (&u4WlanId, args[WPS_CLI_SECOND_ARG], sizeof (INT4));

	    if (nmhGetCapwapDot11WlanProfileIfIndex
		    (u4WlanId, ((INT4 *) &u4ProfileId)) != SNMP_SUCCESS)
	    {
		CLI_SET_ERR (CLI_WPS_WLAN_NOT_CREATED);
		i4RetStatus = CLI_FAILURE;
		break;
	    }
	    if(args[3] != NULL)
	    {
		if (CapwapGetWtpProfileIdFromProfileName
			((UINT1 *) args[3], &u4WtpProfileId) != OSIX_SUCCESS)
		{
		    CliPrintf (CliHandle, "\r\n WTP Profile not found. \r\n");
		    CLI_FATAL_ERROR (CliHandle);
		    i4RetStatus = OSIX_FAILURE;
		    break;
		}
		u4ApProfileId = u4WtpProfileId;			
	    }  
	    else if(args[3] == NULL)
	    {
		u4ApProfileId = WPS_ALL_AP; 
	    }    
            nmhGetFsWPSSetStatus(u4ProfileId,u4ApProfileId,&u4IsWpsEnabled);
	    if ((u4IsWpsEnabled == WPS_ENABLED) &&(u1Status == WPS_ENABLED))
	    {
		CliPrintf (CliHandle, "\r\n WPS is already enabled on one SSID. \r\n");
		CLI_FATAL_ERROR (CliHandle);
		i4RetStatus = OSIX_FAILURE;
		break;
	    }
	    i4RetStatus = WpsCliSetStatus (CliHandle, u1Status, u4ProfileId,u4ApProfileId);
	    break;

	case CLI_WPS_ADDITIONALIE_SET:
	    u1Status = (*(INT1 *) (args[WPS_CLI_FIRST_ARG]));

	    WPS_MEMCPY (&u4WlanId, args[WPS_CLI_SECOND_ARG], sizeof (INT4));

	    if (nmhGetCapwapDot11WlanProfileIfIndex
		    (u4WlanId, ((INT4 *) &u4ProfileId)) != SNMP_SUCCESS)
	    {
		CLI_SET_ERR (CLI_WPS_WLAN_NOT_CREATED);
		i4RetStatus = CLI_FAILURE;
		break;
	    }
	    if(args[3] != NULL)
	    {
		if (CapwapGetWtpProfileIdFromProfileName
			((UINT1 *) args[3], &u4WtpProfileId) != OSIX_SUCCESS)
		{
		    CliPrintf (CliHandle, "\r\n WTP Profile not found. \r\n");
		    CLI_FATAL_ERROR (CliHandle);
		    i4RetStatus = OSIX_FAILURE;
		    break;
		}
		u4ApProfileId = u4WtpProfileId;
	    }

	    i4RetStatus = WpsCliSetAdditionalIE (CliHandle,
		    u1Status, u4ProfileId,u4ApProfileId);
	    break;

	case CLI_WPS_SHOW_CONFIG:
	    WPS_MEMCPY (&u4WlanId, args[WPS_CLI_FIRST_ARG], sizeof (UINT4));

	    if (u4WlanId != 0)
	    {
		if (nmhGetCapwapDot11WlanProfileIfIndex
			(u4WlanId, ((INT4 *) &u4ProfileId)) != SNMP_SUCCESS)
		{
		    CLI_SET_ERR (CLI_WPS_WLAN_NOT_CREATED);
		    i4RetStatus = CLI_FAILURE;
		    break;
		}

		if (u4ProfileId == 0)
		{
		    CLI_SET_ERR (CLI_WPS_WLAN_NOT_CREATED);
		    i4RetStatus = CLI_FAILURE;
		    break;

		}
	    }
	    if(args[2] != NULL)
	    {
		if (CapwapGetWtpProfileIdFromProfileName
			((UINT1 *) args[2], &u4WtpProfileId) != OSIX_SUCCESS)
		{
		    CliPrintf (CliHandle, "\r\n WTP Profile not found. \r\n");
		    CLI_FATAL_ERROR (CliHandle);
		    i4RetStatus = OSIX_FAILURE;
		    break;
		}
		i4RetStatus = WpsCliShowConfig (CliHandle, u4ProfileId,u4WtpProfileId);
	    }
	    else if(args[2] == NULL)
	    {
		i4RetStatus = nmhGetFirstIndexCapwapBaseWtpProfileTable (&u4WtpProfileId);
		if (i4RetStatus == SNMP_FAILURE)
		{
		    CliPrintf (CliHandle, "\r\n Entry not found\r\n");
		}
		do
		{
		    currentProfileId = u4WtpProfileId;
		    i4RetStatus = WpsCliShowConfig (CliHandle, u4ProfileId,u4WtpProfileId);

		}
		while (nmhGetNextIndexCapwapBaseWtpProfileTable
			(currentProfileId, &u4WtpProfileId) == SNMP_SUCCESS);
	    }
	    break;

	case CLI_WPS_SHOW_STATUS:
	    WPS_MEMCPY (&u4WlanId, args[WPS_CLI_FIRST_ARG], sizeof (UINT4));

	    if (u4WlanId != 0)
	    {
		if (nmhGetCapwapDot11WlanProfileIfIndex
			(u4WlanId, ((INT4 *) &u4ProfileId)) != SNMP_SUCCESS)
		{
		    CLI_SET_ERR (CLI_WPS_WLAN_NOT_CREATED);
		    i4RetStatus = CLI_FAILURE;
		    break;
		}

		if (u4ProfileId == 0)
		{
		    CLI_SET_ERR (CLI_WPS_WLAN_NOT_CREATED);
		    i4RetStatus = CLI_FAILURE;
		    break;

		}
	    }
	    if(args[2] != NULL)
	    {
		if (CapwapGetWtpProfileIdFromProfileName
			((UINT1 *) args[2], &u4WtpProfileId) != OSIX_SUCCESS)
		{
		    CliPrintf (CliHandle, "\r\n WTP Profile not found. \r\n");
		    CLI_FATAL_ERROR (CliHandle);
		    i4RetStatus = OSIX_FAILURE;
		    break;
		}
		u4ApProfileId = u4WtpProfileId;
		i4RetStatus = WpsCliShowPBCStatus (CliHandle, u4ProfileId,u4WtpProfileId);
	    }
	    else if(args[2] == NULL)
	    {
		i4RetStatus = nmhGetFirstIndexCapwapBaseWtpProfileTable (&u4WtpProfileId);
		if (i4RetStatus == SNMP_FAILURE)
		{
		    CliPrintf (CliHandle, "\r\n Entry not found\r\n");
		}
		do
		{
		    currentProfileId = u4WtpProfileId;
		    i4RetStatus = WpsCliShowPBCStatus(CliHandle, u4ProfileId,u4WtpProfileId);

		}
		while (nmhGetNextIndexCapwapBaseWtpProfileTable
			(currentProfileId, &u4WtpProfileId) == SNMP_SUCCESS);
	    }

	    break;

	case CLI_WPS_DEBUG:
	    i4RetStatus = WpsCliSetDebugs (CliHandle,
		    CLI_PTR_TO_I4 (args[1]),
		    CLI_ENABLE);
	    break;

	case CLI_WPS_NO_DEBUG:
	    i4RetStatus =WpsCliSetDebugs (CliHandle,
		    CLI_PTR_TO_I4 (args[1]),
		    CLI_DISABLE);
	    break;
	case CLI_WPS_SHOW_AUTH_INFO:
	    WPS_MEMCPY (&u4WlanId, args[WPS_CLI_FIRST_ARG], sizeof (UINT4));
	    if (u4WlanId != 0)
	    {
		if (nmhGetCapwapDot11WlanProfileIfIndex
			(u4WlanId, ((INT4 *) &u4ProfileId)) != SNMP_SUCCESS)
		{
		    CLI_SET_ERR (CLI_WPS_WLAN_NOT_CREATED);
		    i4RetStatus = CLI_FAILURE;
		    break;
		}

		if (u4ProfileId == 0)
		{
		    CLI_SET_ERR (CLI_WPS_WLAN_NOT_CREATED);
		    i4RetStatus = CLI_FAILURE;
		    break;

		}
	    }

	    i4RetStatus = WpsShowAuthInfo (CliHandle, u4ProfileId);
	    break;
	default:
	    CliPrintf (CliHandle, "\r%%invalid messages\r\n");
	    i4RetStatus = CLI_FAILURE;
            break;
    }

    if ((i4RetStatus == CLI_FAILURE)
            && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_WPS_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%s", WpsCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }

    CliUnRegisterLock (CliHandle);
    PNAC_UNLOCK ();
    return i4RetStatus;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : WpsCliSetDebugs                                        */
/*                                                                           */
/* Description     : Set trace level                                         */
/* Input(s)        : CliHandle - Handler                                     */
/*                 : i4CliTraceVal - trace level                             */
/*                 : u1TraceFlag   - Enable / Disable                        */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/

INT4 WpsCliSetDebugs (tCliHandle CliHandle, 
                      INT4 i4CliTraceVal, 
                      INT4 u1TraceFlag)
{
    INT4                i4TraceVal = 0;
    UINT4               u4ErrorCode = 0;

    /* Get Wps Trace Option */
    nmhGetFsWPSTraceOption (&i4TraceVal);

    if (u1TraceFlag == CLI_ENABLE)
    {
        i4TraceVal |= i4CliTraceVal;
    }
    else
    {
        i4TraceVal &= (~(i4CliTraceVal));
    }

    if (nmhTestv2FsWPSTraceOption (&u4ErrorCode, i4TraceVal) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r%%Unable to set trace messages\r\n");
        return CLI_FAILURE;
    }
    if (nmhSetFsWPSTraceOption (i4TraceVal) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r%% Unable to set trace messages\r\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                       */
/* Function Name   : WpsCliSetStatus                                     */
/*                                                                       */
/* Description     : It enables and disables the wps status              */
/* Input(s)    : CliHandle    - CLI Handler                              */
/*               u1Status     - WPS status                               */
/*               u4ProfileId  - index of wlan interface                  */
/*               u4ApProfileId - Profile Id of AP which is unique to each*/
/*                               AP                                      */
/*                                                                       */
/* Output(s)       : NONE                                                */
/*                                                                       */
/* Returns     : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                       */
/*****************************************************************************/
INT4 WpsCliSetStatus(tCliHandle CliHandle, 
                     INT1 u1Status, 
                     UINT4 u4ProfileId, UINT4 u4ApProfileId)
{

    UINT4               u4ErrorCode = 0;
    UNUSED_PARAM (CliHandle);

    /* This function checks whether the given wlan index is available
     * and also verfies the value of u1Status*/

    if (nmhTestv2FsWPSSetStatus(&u4ErrorCode, (INT4) u4ProfileId,u4ApProfileId,
                (INT1) u1Status) != SNMP_SUCCESS)
    {
        CLI_SET_ERR (CLI_WPS_INVALID_SSID);
        return CLI_FAILURE;
    }

    /* This function sets the status of the fsWPSStatus object to either
     * enable or disable*/

    if (nmhSetFsWPSSetStatus((INT4) u4ProfileId, u4ApProfileId,
                (INT1) u1Status) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                       */
/* Function Name   : WpsCliSetAdditionalIE                               */
/*                                                                       */
/* Description     : It et the additionaIEs as enable or disable         */
/* Input(s)    : CliHandle    - CLI Handler                              */
/*               u1Status     - WPS method                               */
/*               u4ProfileId  - index of wlan interface                  */
/*                                                                       */
/* Output(s)       : NONE                                                */
/*                                                                       */
/* Returns     : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                       */
/*****************************************************************************/
INT4 WpsCliSetAdditionalIE(tCliHandle CliHandle, 
                           INT1 u1Status, 
                           UINT4 u4ProfileId, UINT4 u4ApProfileId)
{

    UINT4               u4ErrorCode = 0;
    UNUSED_PARAM (CliHandle);

    /* This function checks whether the given wlan index is available
     * and also verfies the value of u1Status*/

    if (nmhTestv2FsWPSAdditionalIE(&u4ErrorCode, (INT4) u4ProfileId,u4ApProfileId,
                (INT1) u1Status) != SNMP_SUCCESS)
    {
        CLI_SET_ERR (CLI_WPS_INVALID_SSID);
        return CLI_FAILURE;
    }

    /* This function sets the status of the fsWPSAdditionalIE object to either
     * enable or disable*/

    if (nmhSetFsWPSAdditionalIE((INT4) u4ProfileId,u4ApProfileId,
                (INT1) u1Status) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}
/*****************************************************************************/
/*                                                                           */
/* Function Name   : WpsCliShowConfig                                        */
/*                                                                           */
/* Description     : This function displays the WPS config status                 */
/*                                                                           */
/* Input(s)        : CliHandle    - CLI Handler                              */
/*                   WlanIndex    - Index of the wlan interface              */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/
INT4 WpsCliShowConfig (tCliHandle CliHandle, UINT4 u4ProfileId, UINT4 u4ApProfileId)
{
    UINT4               u4Status = 0;
    UINT4               u4AdditionalIEStatus = 0;
    tSNMP_OCTET_STRING_TYPE WtpProfileName;
    UINT1               au1WtpName[256];
    UINT4               u4WlanId = 0;
 
    MEMSET (au1WtpName, 0, 256);
    MEMSET (&WtpProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    WtpProfileName.pu1_OctetList = au1WtpName;
    if (nmhGetCapwapBaseWtpProfileName
        (u4ApProfileId, &WtpProfileName) != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    /* Newly added code */
        WssGetCapwapWlanId (u4ProfileId, &u4WlanId);
        nmhGetFsWPSSetStatus((INT4)u4ProfileId, u4ApProfileId,(INT4 *) &u4Status);
        nmhGetFsWPSAdditionalIE((INT4)u4ProfileId,u4ApProfileId, (INT4 *) &u4AdditionalIEStatus); 
        CliPrintf (CliHandle, " \n\t\t Wlan Id                   : %d \n", u4WlanId);
        
        CliPrintf (CliHandle, " \t\t AP name                   : %s \n", WtpProfileName.pu1_OctetList);
        if (u4Status == WPS_ENABLED)
        {
            CliPrintf (CliHandle, " \t\t WPS Status                : Enabled\n");
        }
        else 
        {
            CliPrintf (CliHandle, " \t\t WPS Status                : Disabled\n");
        }
        if (u4AdditionalIEStatus == WPS_PBC)
        {
            CliPrintf (CliHandle, " \t\t WPS AdditionalIE Status   : PBC\n");
        }
        else if (u4AdditionalIEStatus == WPS_PIN)
        {
            CliPrintf (CliHandle, " \t\t WPS AdditionalIE Status   : PIN\n");
        }
        else 
        {
            CliPrintf (CliHandle, " \t\t WPS AdditionalIE Status   : NONE\n");
        }


    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : WpsCliShowPBCStatus                                        */
/*                                                                           */
/* Description     : This function displays the WPS push button status       */
/*                                                                           */
/* Input(s)        : CliHandle    - CLI Handler                              */
/*                   WlanIndex    - Index of the wlan interface              */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/

INT4 WpsCliShowPBCStatus (tCliHandle CliHandle, UINT4 u4ProfileId, UINT4 u4ApProfileId)
{
    UINT4               u4Status = 0;
    UINT4               u4WlanId = 0;
    UINT4 		u4WtpProfileId =0;
    UINT1               au1WtpName[256];
    tSNMP_OCTET_STRING_TYPE WtpProfileName;

    MEMSET (au1WtpName, 0, 256);
    MEMSET (&WtpProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    WtpProfileName.pu1_OctetList = au1WtpName;
     if (nmhGetCapwapBaseWtpProfileName
        (u4ApProfileId, &WtpProfileName) != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }

        WssGetCapwapWlanId (u4ProfileId, &u4WlanId);
        /*Fetches entries from the table and displays it */
        nmhGetFsWPSPushButtonStatus((INT4) u4ProfileId,(UINT4) u4ApProfileId, (INT4 *) &u4Status);
        CliPrintf (CliHandle, " \n\t\t WlanIndex                     : %d \n", u4WlanId);
        CliPrintf (CliHandle, " \t\t Ap name                       : %s \n", WtpProfileName.pu1_OctetList);
        if (u4Status == WPS_PBC_SUCCESS)
        {
            CliPrintf (CliHandle, " \t\t WPS PBC Status                : Success\n");
        }
        else if (u4Status == WPS_PBC_OVERLAP)
        {
            CliPrintf (CliHandle, " \t\t WPS PBC Status                : Overlap\n");
        }

        else if (u4Status == WPS_PBC_INPROGRESS)
        {
            CliPrintf (CliHandle, " \t\t WPS PBC Status                : In-progress\n");
        }

        else if (u4Status == WPS_PBC_INACTIVE)
        {
            CliPrintf (CliHandle, " \t\t WPS PBC Status                : Inactive\n");
        }
        else if(u4Status == WPS_PBC_ERROR)
        {
            CliPrintf (CliHandle, " \t\t WPS PBC Status                : Error\n");
        }
	else
	{
            CliPrintf (CliHandle, " \t\t WPS PBC Status                : Inactive\n");
	}

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : WpsShowAuthInfo                                         */
/*                                                                           */
/* Description     : This function displays the WPS Authenticated station info*/
/*                                                                           */
/* Input(s)        : CliHandle    - CLI Handler                              */
/*                   WlanIndex    - Index of the wlan interface              */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/

INT4 WpsShowAuthInfo (tCliHandle CliHandle, UINT4 u4ProfileId)
{
    INT4               i4CurrentWlanIndex = (INT4)u4ProfileId;
    INT4               i4NextWlanIndex = 0;
    UINT4               u4CurrentWPSStatsIndex = 0;
    UINT4               u4NextWPSStatsIndex = 0;
    UINT4               u4WlanId = 0;

    WssGetCapwapWlanId ((UINT4)i4CurrentWlanIndex, &u4WlanId);
    while ((WpsGetNextIndexFsWPSAuthSTAInfoTable(i4CurrentWlanIndex,
            &i4NextWlanIndex,u4CurrentWPSStatsIndex,&u4NextWPSStatsIndex))
            == SNMP_SUCCESS)
    {
        if (i4CurrentWlanIndex == i4NextWlanIndex)
        {
        WpsCliShowStatsDisplay(CliHandle,(UINT4)i4NextWlanIndex,
                                u4NextWPSStatsIndex,u4WlanId);
        i4CurrentWlanIndex = i4NextWlanIndex;
        u4CurrentWPSStatsIndex = u4NextWPSStatsIndex;
        }
        else
        {
            break;
        }
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : WpsCliShowStatsDisplay                                  */
/*                                                                           */
/* Description     : This function displays the values from the              */
/*                   WPS Station info based using WLANIndex, StatsIndex      */
/*                                                                           */
/* Input(s)        : u4ProfileIdValue      - Wlan Interface Index            */
/*                   u4WPSStatsIndexValue - WPS Statistics Index             */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/
 INT4
WpsCliShowStatsDisplay (tCliHandle CliHandle, UINT4 u4ProfileIdValue,
                         UINT4 u4WPSStatsIndexValue, UINT4 u4WlanId)
{
   tWpsStat *pTmpWpsStat= NULL;
   tMacAddr            WPSStatsSTAAddress = "";
   UINT1               au1MacAddr[WPS_CLI_MAC_ADDR_LEN] = "";
   INT4               wps_status;
   INT4               wps_pbc_status;

   WpsUtilGetSuppStatsInfo((UINT2) u4ProfileIdValue,u4WPSStatsIndexValue,
                             &pTmpWpsStat);

   if(pTmpWpsStat == NULL)
       return SNMP_FAILURE;

   WPS_MEMSET (au1MacAddr, '\0', WPS_CLI_MAC_ADDR_LEN);
   WPS_MEMCPY (WPSStatsSTAAddress,pTmpWpsStat->au1SuppMacAddr,
            ETH_ALEN);
   CliMacToStr (WPSStatsSTAAddress, au1MacAddr);

   wps_status = pTmpWpsStat->e1Status;
   wps_pbc_status = pTmpWpsStat->e1PbcStatus;

   /* Display the retrieved WPS Station Info Entries using cliPrintf */
   CliPrintf (CliHandle, "\r\r Wlan Index            : %d \r\n", u4WlanId);
    CliPrintf (CliHandle, "\r\r Stats Index           : %d \r\n",
               u4WPSStatsIndexValue);
    CliPrintf (CliHandle, "\r\r Station MAC address   : %s \r\n", au1MacAddr);
 if(wps_status == 0)	
    CliPrintf (CliHandle, "\r\r WPS Status            : Success\n");
    else if(wps_status == 1)
    CliPrintf (CliHandle, "\r\r WPS Status            : Failure\n");
 else
   CliPrintf (CliHandle, "\r\r WPS Status            : -\n");

   if(wps_pbc_status == 1)
    CliPrintf (CliHandle, "\r\r WPS PBC Status        : Success\n");		
    else if(wps_pbc_status == 2)
    CliPrintf (CliHandle, "\r\r WPS PBC Status        : Overlap\n");			
    else if(wps_pbc_status == 3)
    CliPrintf (CliHandle, "\r\r WPS PBC Status        : In-Progress\n");	
    else if(wps_pbc_status == 4)
    CliPrintf (CliHandle, "\r\r WPS PBC Status        : Inactive\n");
    else if(wps_pbc_status == 5)
    CliPrintf (CliHandle, "\r\r WPS PBC Status        : Error\n");

   return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : WpsCliShowRunningConfig                                 */
/*                                                                           */
/* Description     : This function displays the WPS config status            */
/*                                                                           */
/* Input(s)        : CliHandle    - CLI Handler                              */
/*                   WlanIndex    - Index of the wlan interface              */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/
INT4 WpsCliShowRunningConfig (tCliHandle CliHandle, UINT4 u4ProfileId, UINT4 u4ApProfileId,UINT4 u4WlanId)
{
    UINT4               u4Status = 0;
    UINT4               u4AdditionalIEStatus = 0;
    tSNMP_OCTET_STRING_TYPE WtpProfileName;
    UINT1               au1WtpName[256];
 
    MEMSET (au1WtpName, 0, 256);
    MEMSET (&WtpProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    WtpProfileName.pu1_OctetList = au1WtpName;
    if (nmhGetCapwapBaseWtpProfileName
        (u4ApProfileId, &WtpProfileName) != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    /* Newly added code */
        nmhGetFsWPSSetStatus((INT4)u4ProfileId, u4ApProfileId,(INT4 *) &u4Status);
        nmhGetFsWPSAdditionalIE((INT4)u4ProfileId,u4ApProfileId, (INT4 *) &u4AdditionalIEStatus); 
        
        if (u4Status == WPS_ENABLED)
        {
            CliPrintf (CliHandle, "\n!\n\r\n wps enable %d profile_name %s",u4WlanId,WtpProfileName.pu1_OctetList);
        }
        else 
        {
            CliPrintf (CliHandle, "\n!\r\n wps disable %d",u4WlanId);
        }
        if (u4AdditionalIEStatus == WPS_PBC)
        {
            CliPrintf (CliHandle, "\r\n wps set additionalIE pbc %d profile_name %s\n",u4WlanId,WtpProfileName.pu1_OctetList);
        }
        else if (u4AdditionalIEStatus == WPS_PIN)
        {
            CliPrintf (CliHandle, "\r\n wps set additionalIE pin %d profile_name %s",u4WlanId,WtpProfileName.pu1_OctetList);
        }


    return CLI_SUCCESS;
}
/*****************************************************************************/
/*                                                                           */
/*  Function Name   : WpsShowRunningConfig                                  */
/*                                                                           */
/*  Description     : This function displays current WPS configurations     */
/*                                                                           */
/*  Input(s)        : CliHandle        - Handle to the CLI Context           */
/*                    u4ProfileIdValue - Specified Wlan Interface Index      */
/*                                                                           */
/*  Output(s)       : NONE                                                   */
/*                                                                           */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                                */
/*                                                                           */
/*****************************************************************************/

INT4
WpsShowRunningConfig (tCliHandle CliHandle, UINT4 u4WlanId)
{
    UINT4               u4Status = 0;
    UINT4               u4ProfileId = 0;
    UINT4               u4WtpProfileId = 0;
    INT4		i4RetStatus = SNMP_FAILURE;
    UINT4               currentProfileId = 0;
    UINT4               u4CurrentWlanIndex = 0;
    UINT4               u4NextWlanIndex = 0;
    UINT4               u4TableWalk = 1;
    if (u4WlanId != 0)
    {
	if (nmhGetCapwapDot11WlanProfileIfIndex
		(u4WlanId, (INT4 *) &u4ProfileId) != SNMP_SUCCESS)
	{
	    CLI_FATAL_ERROR (CliHandle);
	    return CLI_FAILURE;
	}

	if (u4ProfileId == 0)
	{
	    CLI_FATAL_ERROR (CliHandle);
	    return CLI_FAILURE;

	}
    }

    u4CurrentWlanIndex = u4ProfileId;

    /* Loop through the Dot11Privacy Table to fetch WPS Configurations */
    do
    {
	if (u4ProfileId != 0)
	{
	    u4TableWalk = 0;
	}
	else
	{
	    /* Fetches the next WlanIndex from the Dot11Privacy Table 
	     * by Passing the current WlanIndex */
	    if (nmhGetNextIndexDot11PrivacyTable ((INT4) u4CurrentWlanIndex,
			(INT4 *) &u4NextWlanIndex)
		    != SNMP_SUCCESS)
	    {
		/* Breaking the loop after printing the appropriate values
		 * for specific WlanIndex */
		break;
	    }

	    /* Assign the next wlanindex pointer to current to
	     * get the appropriate value from GetnextIndex */
	    u4CurrentWlanIndex = u4NextWlanIndex;
	}

	WssGetCapwapWlanId (u4CurrentWlanIndex, &u4WlanId);

	if (nmhGetCapwapDot11WlanProfileIfIndex
		(u4WlanId, ((INT4 *) &u4ProfileId)) != SNMP_SUCCESS)
	{
	    break;
	}
	i4RetStatus = nmhGetFirstIndexCapwapBaseWtpProfileTable (&u4WtpProfileId);
	if (i4RetStatus == SNMP_FAILURE)
	{
	    CliPrintf (CliHandle, "\r\n Entry not found\r\n");
	}
	do
	{
	    currentProfileId = u4WtpProfileId;
	    nmhGetFsWPSSetStatus((INT4)u4ProfileId, u4WtpProfileId,(INT4 *) &u4Status);
	    if (u4Status == WPS_ENABLED)
	    {
		i4RetStatus = WpsCliShowRunningConfig (CliHandle, u4ProfileId,u4WtpProfileId,u4WlanId);
	    }
	}
	while (nmhGetNextIndexCapwapBaseWtpProfileTable
		(currentProfileId, &u4WtpProfileId) == SNMP_SUCCESS);
        u4ProfileId = 0;

    }
    while (u4TableWalk != 0);

    return CLI_SUCCESS;
}
#endif /*WPSCLI_C */
