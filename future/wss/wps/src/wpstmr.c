/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: wpstmr.c,v 1.1.1.1 2016/09/20 10:42:41 siva Exp $
 *
 * Description: WPS File.
 *********************************************************************/

#include "wpsinc.h"


/****************************************************************************
 *                                                                           *
 * Function     : WpsTmrProcSessionTimer                                     *
 *                                                                           *
 * Description  : Function invoked when the session timer expires which      *
 * 		  deletes the corrsponding session                           *
 *                                                                           *
 * Input        : pTmrContext: expired timer context                         *
 *                                                                           *
 * Output       : NONE                                                       *
 *                                                                           *
 * Returns      : NONE                                                       *
 *                                                                           *
 *****************************************************************************/
VOID  WpsTmrProcSessionTimer(VOID *pTmrContext)
{
    tWpsProbeReq *pWpsProbeReq = NULL;
    pWpsProbeReq = (tWpsProbeReq*)pTmrContext;

    if (pWpsProbeReq == NULL)
    {
        WPS_TRC (WPS_CONTROL_PATH_TRC | WPS_ALL_FAILURE_TRC,
                "pWpsSessionNode\n");
        return;
    }
    WPS_TRC (WPS_CONTROL_PATH_TRC | WPS_ALL_FAILURE_TRC,
            "session timer expired \n");
    if (pWpsProbeReq->pWpsSessionNode != NULL)
    wpsDeleteSession(pWpsProbeReq->pWpsSessionNode); 

    return;
}
/****************************************************************************
 *                                                                           *
 * Function     : WpsTmrProcProtocolTimer                                    *
 *                                                                           *
 * Description  : Function invoked when the protocol timer expires           * 
 * 		  deletes the corrsponding session                           *
 *                                                                           *
 * Input        : pTmrContext: expired timer context                         *
 *                                                                           *
 * Output       : NONE                                                       *
 *                                                                           *
 * Returns      : NONE                                                       *
 *                                                                           *
 *****************************************************************************/
VOID WpsTmrProcProtocolTimer(VOID *pTmrContext)
{
    WPS_TRC (WPS_CONTROL_PATH_TRC | WPS_ALL_FAILURE_TRC,
            "protocol timer expired for \n");
    tWpsSessionNode *pWpsSessionNode= NULL;
    pWpsSessionNode = (tWpsSessionNode *)pTmrContext;
    if (pWpsSessionNode == NULL)
    {
        WPS_TRC (WPS_CONTROL_PATH_TRC | WPS_ALL_FAILURE_TRC,
                "pWpsSessionNode\n");
        return ;
    }
    WPS_TRC (WPS_CONTROL_PATH_TRC | WPS_ALL_FAILURE_TRC,
            "session timer expired for \n");
    wpsDeleteSession(pWpsSessionNode); 
}

/****************************************************************************
 *                                                                           *
 * Function     : WpsTmrProcPbcTimer                                         *
 *                                                                           *
 * Description  : Function invoked when the pbc timer expires which          *
 * 		  deletes the corrsponding session                           *
 *                                                                           *
 * Input        : pTmrContext: expired timer context                         *
 *                                                                           *
 * Output       : NONE                                                       *
 *                                                                           *
 * Returns      : NONE                                                       *
 *                                                                           *
 *****************************************************************************/
VOID WpsTmrProcPbcTimer(VOID *pTmrContext, UINT4 u4ProfileId)
{
    tWpsContext *pWpsContext = NULL;
    pWpsContext = (tWpsContext*)pTmrContext;
    UINT2 u2Index = 0;
    if (pWpsContext == NULL)
    {
	WPS_TRC (WPS_CONTROL_PATH_TRC | WPS_ALL_FAILURE_TRC,
		"pWpsContext\n");
	return ;
    }
    for (u2Index = 0; u2Index < NUM_OF_AP_SUPPORTED; u2Index++)
    {
	if (u4ProfileId == 
		pWpsContext->pWpsConfigDb[u2Index].u4WpsWtpProfileId)
	{
	    pWpsContext->pWpsConfigDb[u2Index].b1WpsAdditionalIE = OSIX_FALSE;
	    pWpsContext->pWpsConfigDb[u2Index].u4PushButtonStatus = WPS_PBC_STATUS_INACTIVE;
	    break;
	}
    }
    return; 
}

/****************************************************************************
 *                                                                           *
 * Function     : WpsTmrProcMsgTimer                                         *
 *                                                                           *
 * Description  : Function invoked when the protocol message timer expires   *
 * 		  deletes the corrsponding session                           *
 *                                                                           *
 * Input        : pTmrContext: expired timer context                         *
 *                                                                           *
 * Output       : NONE                                                       *
 *                                                                           *
 * Returns      : NONE                                                       *
 *                                                                           *
 *****************************************************************************/
VOID WpsTmrProcMsgTimer(VOID *pTmrContext)
{
    tWpsSessionNode *pWpsSessionNode= NULL;
    pWpsSessionNode = (tWpsSessionNode *)pTmrContext;
    if (pWpsSessionNode == NULL)
    {
	WPS_TRC (WPS_CONTROL_PATH_TRC | WPS_ALL_FAILURE_TRC,
		"pWpsSessionNode\n");
	return ;
    }
    WPS_TRC (WPS_CONTROL_PATH_TRC | WPS_ALL_FAILURE_TRC,
            "protocol timer expired \n");
    WpsTmrProcProtocolTimer(pTmrContext);
}

/****************************************************************************
 *                                                                           *
 * Function     : WpsTmrProcRetransTimer                                     *
 *                                                                           *
 * Description  : Function invoked when the protocol message retransmisson   *
 *                timer expires, then deletes the corrsponding session after * 
 *                max retransmissions or msg is retransmitted                *
 *                                                                           *
 * Input        : pTmrContext: expired timer context                         *
 *                                                                           *
 * Output       : NONE                                                       *
 *                                                                           *
 * Returns      : NONE                                                       *
 *                                                                           *
 *****************************************************************************/
VOID WpsTmrProcRetransTimer(VOID *pTmrContext)
{
    tWpsSessionNode *pWpsSessionNode= NULL;
    pWpsSessionNode = (tWpsSessionNode *)pTmrContext;
    if (pWpsSessionNode == NULL)
    {
	WPS_TRC (WPS_CONTROL_PATH_TRC | WPS_ALL_FAILURE_TRC,
		"pWpsSessionNode\n");
	return ;
    }
    WPS_TRC (WPS_CONTROL_PATH_TRC | WPS_ALL_FAILURE_TRC,
	    "session timer expired for \n");
    if (pWpsSessionNode->u1MsgCount < WPS_RETRANSMISSION_COUNT)
    {
	if (WpsTxFrame ((tWpsEapPacketInfo *)pWpsSessionNode->cur_msg.au1Buf, pWpsSessionNode->au1SuppMacAddr, pWpsSessionNode->u2PortNum, pWpsSessionNode->cur_msg.i4used ) != WPS_SUCCESS)
	{	
	    WPS_TRC (WPS_CONTROL_PATH_TRC | WPS_ALL_FAILURE_TRC, 
		    " WPS: WpsTxFrame returned Failure\n");
	    return ;

	}	
	pWpsSessionNode->u1MsgCount++;
	WpsTmrStartTimer(&pWpsSessionNode->WpsReTransTmr, WPS_RETRANS_TIMER_ID,
		WPS_RETRANS_TIME, pWpsSessionNode,NUM_OF_AP_SUPPORTED);
    }
    else
    {
	WpsTmrProcProtocolTimer(pTmrContext);
    }
}

PRIVATE VOID (*gaWpsTimerRoutines[]) (VOID *) =
{
    NULL,
    WpsTmrProcSessionTimer,
    WpsTmrProcProtocolTimer,
    WpsTmrProcMsgTimer,
    WpsTmrProcRetransTimer,
};

/****************************************************************************
 *                                                                           *
 * Function     : WpsTmrInit                                                *
 *                                                                           *
 * Description  : Function to create wps timer list                         *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : WPS_SUCCESS/WPS_FAILURE                                  *
 *                                                                           *
 *****************************************************************************/
UINT4 WpsTmrInit (VOID)
{

    if (TmrCreateTimerList (WPS_TASK_NAME, WPS_TIMER_EXP_EVENT,
                NULL,
                (tTimerListId) & gWpsGlobals.u4WpsTimerId) ==
            TMR_FAILURE)
    {
        WPS_TRC (WPS_INIT_SHUT_TRC | WPS_ALL_FAILURE_TRC |
                WPS_OS_RESOURCE_TRC | WPS_CONTROL_PATH_TRC,
                " TMR: Timer list creation failed\n");
        return (WPS_FAILURE);
    }

    return (WPS_SUCCESS);
}

/****************************************************************************
 *                                                                           *
 * Function     : WpsTmrProcessTimer                                        *
 *                                                                           *
 * Description  : Function invoked to process the expired timers             *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
VOID WpsTmrProcessTimer (VOID)
{
    tWpsTimer         *pWpsTimer = NULL;
    tTmrAppTimer       *pExpTimer = NULL;

    pExpTimer =
        TmrGetNextExpiredTimer ((tTimerListId) gWpsGlobals.u4WpsTimerId);
    while (pExpTimer != NULL)
    {
        pWpsTimer = (tWpsTimer *) pExpTimer->u4Data;

        if (pWpsTimer != NULL)
        {
            WPS_INIT_TIMER (pWpsTimer);
            if ((pWpsTimer->u1TimerId > WPS_MIN_TIMER_ID) &&
                    (pWpsTimer->u1TimerId < WPS_MAX_TIMER_ID))
            {
                if (pWpsTimer->u1TimerId == WPS_PBC_TIMER_ID)
                {
		    WpsTmrProcPbcTimer(pWpsTimer->pContext,pWpsTimer->u4ProfileId);
                }
                else
                {
                (*gaWpsTimerRoutines[pWpsTimer->u1TimerId])
                    (pWpsTimer->pContext);
                }
            }
        }
        pExpTimer =
            TmrGetNextExpiredTimer ((tTimerListId) gWpsGlobals.u4WpsTimerId);
    }
}

/****************************************************************************
 *                                                                           *
 * Function     : WpsTmrStartTimer                                          *
 *                                                                           *
 * Description  : Function to start the timer                                *
 *                                                                           *
 * Input        : pWpsTimer :- Points to the wps timer                     *
 *                TimerId    :- Timer Id                                     *
 *                u4Timeout  :- The valu for which the timer to be started   *
 *                pTmrContext :- Refers to the context that will be used     *
 *                               for processing the timer                    *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : WPS_SUCCESS/WPS_FAILURE                                  *
 *                                                                           *
 *****************************************************************************/
UINT4 WpsTmrStartTimer (tWpsTimer * pWpsTimer, 
                        eWpsTimerId TimerId,
                        UINT4 u4TimeOut, 
                        VOID *pTmrContext,
                        UINT4 u4ProfileId)
{
    if (WPS_IS_TIMER_ACTIVE (pWpsTimer) == TRUE)
    {
        WPS_TRC (DATA_PATH_TRC, "WpsTmrStartTimer:Attempt to start"
                "already running timer\n");
        return WPS_FAILURE;
    }

    WPS_ACTIVATE_TIMER (pWpsTimer);
    pWpsTimer->u1TimerId = (UINT1) TimerId;
    pWpsTimer->pContext = pTmrContext;
    pWpsTimer->Node.u4Data = (FS_ULONG) pWpsTimer;
    pWpsTimer->u4ProfileId = u4ProfileId;

    if (TmrStartTimer
            ((tTimerListId) gWpsGlobals.u4WpsTimerId, &pWpsTimer->Node,
             (u4TimeOut * SYS_NUM_OF_TIME_UNITS_IN_A_SEC)) == TMR_SUCCESS)
    {
        return (WPS_SUCCESS);
    }
    else
    {
        return (WPS_FAILURE);
    }
}

/****************************************************************************
 *                                                                           *
 * Function     : WpsTmrStopTimer                                           *
 *                                                                           *
 * Description  : Function invoked to stop timer                             *
 *                                                                           *
 * Input        : pWpsTimer :- Points to the timer to be stopped            *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : WPS_SUCCESS/WPS_FAILURE                                  *
 *                                                                           *
 *****************************************************************************/
UINT4 WpsTmrStopTimer (tWpsTimer * pWpsTimer)
{
    if (WPS_IS_TIMER_ACTIVE (pWpsTimer) == TRUE)
    {
        if (TmrStopTimer
                ((tTimerListId) gWpsGlobals.u4WpsTimerId,
                 &pWpsTimer->Node) == TMR_FAILURE)
        {
            WPS_TRC (DATA_PATH_TRC, "WpsTmrStopTimer:TmrStopTimer failed!\n");
            return WPS_FAILURE;
        }
        WPS_INIT_TIMER (pWpsTimer);
    }
    return WPS_SUCCESS;
}

