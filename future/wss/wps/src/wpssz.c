/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: wpssz.c,v 1.1.1.1 2016/09/20 10:42:41 siva Exp $
 *
 * Description: WPS File.
 *********************************************************************/
#define _WPSSZ_C
#include "wpsinc.h"

extern INT4 IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                             tFsModSizingParams *pModSizingParams);

INT4 WpsSizingMemCreateMemPools ()
{
    INT4                i4RetVal = 0;
    INT4                i4SizingId = 0;

    for (i4SizingId = 0; i4SizingId < WPS_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            (INT4) MemCreateMemPool (FsWPSSizingParams[i4SizingId].
                    u4StructSize,
                    FsWPSSizingParams[i4SizingId].
                    u4PreAllocatedUnits,
                    MEM_DEFAULT_MEMORY_TYPE,
                    &(WPSMemPoolIds[i4SizingId]));
        if (i4RetVal != 0)
        {
            WpsSizingMemDeleteMemPools ();
            return WPS_FAILURE;
        }
    }
    return WPS_SUCCESS;
}

INT4 WpsSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsWPSSizingParams);
    return WPS_SUCCESS;
}

VOID WpsSizingMemDeleteMemPools ()
{
    INT4                i4SizingId = 0;

    for (i4SizingId = 0; i4SizingId < WPS_MAX_SIZING_ID; i4SizingId++)
    {
        if (WPSMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (WPSMemPoolIds[i4SizingId]);
            WPSMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
