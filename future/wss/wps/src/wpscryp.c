/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: wpscryp.c,v 1.2 2018/01/03 13:44:57 siva Exp $
*
* Description: WPS File.
*********************************************************************/

#include "wpsinc.h"

#define PRF_DIGEST_SIZE 256
#define WPS_KDF_BITS 640
#define PERSONALIZED_STRING "Wi-Fi Easy and Secure Key Derivation"

/*****************************************************************************/
/* Function Name      : wpabuf_zeropad                                       */
/*                                                                           */
/* Description        : This function adds the padding if string is <192     */
/*                                                                           */
/* Input(s)           : buf: input buf, len: buf len                         */
/*                                                                           */
/* Output(s)          : if buf len> 192 return padded string or return same  */
/*                                                                           */
/* Return Value(s)    : buf                                                  */
/*****************************************************************************/
tWpsBuf            *
wpabuf_zeropad (tWpsBuf * buf, INT4 i4Len)
{
    tWpsBuf            *ret;
    INT4                i4Blen;

    if (buf == NULL)
        return NULL;

    i4Blen = buf->i4used;
    if (i4Blen >= i4Len)
        return buf;

    if ((ret = WPS_MEM_ALLOCATE_MEM_BLK (WPS_TEMP_MEMPOOL_ID)) == NULL)
    {
        WPS_TRC (WPS_CONTROL_PATH_TRC | WPS_ALL_FAILURE_TRC,
                 " WpsUtilAddStaToStatsTable :- MemAllocMemBlk Failed \n");
        return NULL;
    }
    if (ret)
    {
        WPS_MEMSET (ret->au1Buf, 0, i4Len - i4Blen);
        WPS_MEMCPY (ret->au1Buf + i4Len - i4Blen, buf->au1Buf, buf->i4used);
        ret->i4used = i4Len;
    }
    WPS_MEM_RELEASE_MEM_BLK (WPS_TEMP_MEMPOOL_ID, buf);
    return ret;
}

/*****************************************************************************/
/* Function Name      : wpsDeriveSharedKey                                   */
/*                                                                           */
/* Description        : This function computes gAMmodP using peerpublic      */
/*                                                                           */
/* Input(s)           : *ctx: registrar DH context                           */
/*                      *peerPublic : enrollees public key                   */
/*                                                                           */
/* Output(s)          : shared secret context                                */
/*                                                                           */
/* Return Value(s)    : wps buf                                              */
/*****************************************************************************/
tWpsBuf            *
wpsDeriveSharedKey (void *ctx, tWpsBuf * peerPublic)
{
    tWpsBuf            *sharedSecretKey = NULL;

    if ((sharedSecretKey =
         WPS_MEM_ALLOCATE_MEM_BLK (WPS_TEMP_MEMPOOL_ID)) == NULL)
    {
        WPS_TRC (WPS_CONTROL_PATH_TRC | WPS_ALL_FAILURE_TRC,
                 " WpsUtilAddStaToStatsTable :- MemAllocMemBlk Failed \n");
        return NULL;
    }

    WPS_MEMSET (sharedSecretKey->au1Buf, '\0', WPS_BUFFER_SIZE);
    dh5DerivedSharedKey (sharedSecretKey->au1Buf,
                         &sharedSecretKey->i4used,
                         ctx, peerPublic->au1Buf, peerPublic->i4used);
    return sharedSecretKey;
}

/*****************************************************************************/
/* Function Name      : wpsDhInitilization                                   */
/*                                                                           */
/* Description        : This function computes DH private and public key of  */
/*                      the registrar                                        */
/*                                                                           */
/* Input(s)           : *ctx: registrar DH context                           */
/*                      *peerPublic : enrollees public key                   */
/*                                                                           */
/* Output(s)          : shared secret context                                */
/*                                                                           */
/* Return Value(s)    : wps buf                                              */
/*****************************************************************************/
void               *
wpsDhInitilization (tWpsBuf ** privKey, tWpsBuf ** pubKey)
{
    void               *dhInfo = NULL;

    if ((*pubKey = WPS_MEM_ALLOCATE_MEM_BLK (WPS_TEMP_MEMPOOL_ID)) == NULL)
    {
        WPS_TRC (WPS_CONTROL_PATH_TRC | WPS_ALL_FAILURE_TRC,
                 " WpsUtilAddStaToStatsTable :- MemAllocMemBlk Failed \n");
        return NULL;
    }

    if ((*privKey = WPS_MEM_ALLOCATE_MEM_BLK (WPS_TEMP_MEMPOOL_ID)) == NULL)
    {
        WPS_TRC (WPS_CONTROL_PATH_TRC | WPS_ALL_FAILURE_TRC,
                 " WpsUtilAddStaToStatsTable :- MemAllocMemBlk Failed \n");
        return NULL;
    }

    dhInfo =
        (void *) dhInit (&(*pubKey)->i4used, &(*privKey)->i4used,
                         (*pubKey)->au1Buf, (*privKey)->au1Buf);

    return dhInfo;
}

/*****************************************************************************/
/* Function Name      : wpsGenerateDHSecretKey                               */
/*                                                                           */
/* Description        : This function computes DH secret key                 */
/*                                                                           */
/* Input(s)           : wps_data: pointer to wps session node                */
/*                                                                           */
/* Output(s)          : shared secret context                                */
/*                                                                           */
/* Return Value(s)    : WPS_SUCCESS/WPS_FAILURE                              */
/*****************************************************************************/
UINT1
wpsGenerateDHSecretKey (tWpsSessionNode * wps_data)
{
    void               *pDhInfo;

    tWpsBuf            *privkey;
    tWpsBuf            *pubkey;
    tWpsBuf            *enrkey;

    pDhInfo = wpsDhInitilization (&privkey, &pubkey);

    pubkey = wpabuf_zeropad (pubkey, 192);
    WPS_MEMCPY (wps_data->au1DhPubkeyR, pubkey->au1Buf, pubkey->i4used);
    wps_data->u1DhPubKeyRlen = (UINT1) pubkey->i4used;

    if ((enrkey = WPS_MEM_ALLOCATE_MEM_BLK (WPS_TEMP_MEMPOOL_ID)) == NULL)
    {
        WPS_TRC (WPS_CONTROL_PATH_TRC | WPS_ALL_FAILURE_TRC,
                 " WpsUtilAddStaToStatsTable :- MemAllocMemBlk Failed \n");
    }
    WPS_MEMCPY (enrkey->au1Buf, wps_data->au1DhPubKeyE,
                wps_data->u1DhPubKeyELen);
    enrkey->i4used = wps_data->u1DhPubKeyELen;

    wps_data->pDhSharedSecret = wpsDeriveSharedKey (pDhInfo, enrkey);
    wps_data->pDhSharedSecret = wpabuf_zeropad (wps_data->pDhSharedSecret, 192);
    WPS_MEM_RELEASE_MEM_BLK (WPS_TEMP_MEMPOOL_ID, pubkey);
    WPS_MEM_RELEASE_MEM_BLK (WPS_TEMP_MEMPOOL_ID, privkey);
    WPS_MEM_RELEASE_MEM_BLK (WPS_TEMP_MEMPOOL_ID, enrkey);

    if (wps_data->pDhSharedSecret == NULL)
    {
        return WPS_FAILURE;
    }
    return WPS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : WpsAlgoHmacSha2                                      */
/*                                                                           */
/* Description        : This function computes HASHMAC256 of a string        */
/*                                                                           */
/* Input(s)           : whichSha: which SHA algo                             */
/*                      *data : input string                                 */
/*                      *len : input string len                              */
/*                      *noElements : no of input strings                    */
/*                      *key : authkey used for SHA                          */
/*                      *keyLen : authkey len used for SHA                   */
/*                      *pu1Digest : output of SHA                           */
/*                                                                           */
/* Output(s)          : HASHMAC of a string                                  */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*****************************************************************************/
VOID
WpsAlgoHmacSha2 (eShaVersion whichSha, UINT1 *u1Data[], INT4 *i4len,
                 UINT1 u1NoElements, UINT1 *u1Key, UINT2 u2KeyLen,
                 UINT1 *u1Pu1Digest)
{
    tHmacContext        HmacCtx;
    UINT1               u1i = 0;

    arHmacReset (&HmacCtx, whichSha, u1Key, u2KeyLen);

    for (u1i = 0; u1i < u1NoElements; u1i++)
        arHmacInput (&HmacCtx, u1Data[u1i], i4len[u1i]);

    arHmacResult (&HmacCtx, u1Pu1Digest);
}

/*****************************************************************************/
/* Function Name      : wpsGenerateKeys                                      */
/*                                                                           */
/* Description        : This function computes keys required for registration*/
/*                      protocol                                             */
/*                                                                           */
/* Input(s)           : *wps_data: wps session node                          */
/*                                                                           */
/* Output(s)          : updates the keys in context                          */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
UINT1
wpsGenerateKeys (tWpsSessionNode * wps_data)
{
    UINT1               u1Status = wpsGenerateDHSecretKey (wps_data);
    if (u1Status == WPS_FAILURE)
        return u1Status;
    wpsGenerateKDK (wps_data);
    return u1Status;
}

/*****************************************************************************/
/* Function Name      : WPS_PUT_BE32                                         */
/*                                                                           */
/* Description        : This function reverses the string                    */
/*                                                                           */
/* Input(s)           : *a: output string                                    */
/*                      val: input string                                    */
/*                                                                           */
/* Output(s)          : a                                                    */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*****************************************************************************/
static void
WPS_PUT_BE32 (INT1 *i1a, INT4 i4Val)
{
    i1a[0] = (INT1) ((i4Val >> 24) & 0xff);
    i1a[1] = (INT1) ((i4Val >> 16) & 0xff);
    i1a[2] = (INT1) ((i4Val >> 8) & 0xff);
    i1a[3] = (INT1) (i4Val & 0xff);
}

/*****************************************************************************/
/* Function Name      : wpsKdf                                               */
/*                                                                           */
/* Description        : key derivation function used by kdk to derive auth   */
/*                      and keywrapkey                                       */
/*                                                                           */
/* Input(s)           : *key: DH secrate context                             */
/*                      *lable_prefix: predefine label prefix                */
/*                      *lable_prefix_len: predefine label length            */
/*                      *lable: predefine label                              */
/*                      *res: generated hash string                          */
/*                      *res_len: generated hash string len                  */
/*                                                                           */
/* Output(s)          : hash string res                                      */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*****************************************************************************/
void
wpsKdf (UINT1 *u1Key, UINT1 *u1LabelPrefix, INT4 i4LabelPrefixLen,
        INT1 *u1Label, UINT1 *u1Res, INT4 i4ResLen)
{
    UINT1               au1Ibuf[4];
    UINT1              *au1Addr[4];
    INT4                ai4Len[4];
    INT1                ai1KeyBits[4];
    INT4                i4i, i4Iter;
    UINT1               u1Hash[SHA256_MAC_LEN], *u1Opos;
    INT4                i4left;

    WPS_PUT_BE32 (ai1KeyBits, i4ResLen * 8);

    au1Addr[0] = au1Ibuf;
    ai4Len[0] = sizeof (au1Ibuf);
    au1Addr[1] = u1LabelPrefix;
    ai4Len[1] = i4LabelPrefixLen;
    au1Addr[2] = (UINT1 *) u1Label;
    ai4Len[2] = (INT4) STRLEN (u1Label);
    au1Addr[3] = (UINT1 *) ai1KeyBits;
    ai4Len[3] = sizeof (ai1KeyBits);

    i4Iter = (i4ResLen + SHA256_MAC_LEN - 1) / SHA256_MAC_LEN;
    u1Opos = u1Res;
    i4left = i4ResLen;

    for (i4i = 1; i4i <= i4Iter; i4i++)
    {
        WPS_PUT_BE32 ((INT1 *) au1Ibuf, i4i);
        WpsAlgoHmacSha2 (AR_SHA256_ALGO, au1Addr, ai4Len, 4, u1Key,
                         SHA256_MAC_LEN, u1Hash);
        if (i4i < i4Iter)
        {
            WPS_MEMCPY (u1Opos, u1Hash, SHA256_MAC_LEN);
            u1Opos += SHA256_MAC_LEN;
            i4left -= SHA256_MAC_LEN;
        }
        else
            WPS_MEMCPY (u1Opos, u1Hash, i4left);
    }
}

/*****************************************************************************/
/* Function Name      : wpsGenerateKDK                                       */
/*                                                                           */
/* Description        : This function computes  auth and keywrapkey          */
/*                                                                           */
/* Input(s)           : *wps_data: wps session node pointer                  */
/*                                                                           */
/* Output(s)          : auth key and keywrapkey                              */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*****************************************************************************/
VOID
wpsGenerateKDK (tWpsSessionNode * wps_data)
{
    UINT1              *au1Addr[3];
    INT4                ai4Len[3];
    UINT1               u1Dhkey[SHA256_MAC_LEN + 1];
    UINT1               u1Keys[WPS_AUTHKEY_LEN + WPS_KEYWRAPKEY_LEN +
                               WPS_EMSK_LEN];

    au1Addr[0] = wps_data->pDhSharedSecret->au1Buf;
    ai4Len[0] = wps_data->pDhSharedSecret->i4used;
    WPS_MEMSET (u1Dhkey, 0, SHA256_MAC_LEN + 1);
    Sha256ArAlgo (wps_data->pDhSharedSecret->au1Buf,
                  wps_data->pDhSharedSecret->i4used, u1Dhkey);

    WpsUtilGetRandom (wps_data->au1NonceR, WPS_SECRET_NONCE_LEN);
    au1Addr[0] = wps_data->au1NonceE;
    ai4Len[0] = WPS_SECRET_NONCE_LEN;
    au1Addr[1] = wps_data->au1SuppMacAddr;
    ai4Len[1] = ETH_ALEN;
    au1Addr[2] = wps_data->au1NonceR;
    ai4Len[2] = WPS_SECRET_NONCE_LEN;

    WPS_MEMSET (wps_data->au1WpsKdk, 0, 32);

    WpsAlgoHmacSha2 (AR_SHA256_ALGO, au1Addr, ai4Len, 3, u1Dhkey,
                     SHA256_MAC_LEN, wps_data->au1WpsKdk);

    wpsKdf (wps_data->au1WpsKdk, NULL, 0, (INT1 *) PERSONALIZED_STRING,
            u1Keys, sizeof (u1Keys));
    WPS_MEMCPY (wps_data->au1AuthKey, u1Keys, WPS_AUTHKEY_LEN);
    WPS_MEMCPY (wps_data->au1Keywrapkey, u1Keys + WPS_AUTHKEY_LEN,
                WPS_KEYWRAPKEY_LEN);
    WPS_MEM_RELEASE_MEM_BLK (WPS_TEMP_MEMPOOL_ID, wps_data->pDhSharedSecret);
}

/*****************************************************************************/
/* Function Name      : wpsDerivePsk                                         */
/*                                                                           */
/* Description        : This function computes PSK                           */
/*                                                                           */
/* Input(s)           : *wps : wps session node                              */
/*                      *dev_passwd :entered device password                 */
/*                      dev_passwd_len :entered device password len          */
/*                                                                           */
/* Output(s)          : psk                                                  */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*****************************************************************************/
void
wpsDerivePsk (tWpsSessionNode * wps, UINT1 *u1DevPasswd, INT4 i4DevPasswdLen)
{
    UINT1               au1Hash[SHA256_MAC_LEN];
    UINT1              *u1TmpStr[1];
    INT4                i4Len[1];
    u1TmpStr[0] = u1DevPasswd;
    i4Len[0] = (i4DevPasswdLen + 1) / 2;
    WpsAlgoHmacSha2 (AR_SHA256_ALGO, u1TmpStr, i4Len, 1, wps->au1AuthKey,
                     WPS_AUTHKEY_LEN, au1Hash);
    WPS_MEMCPY (wps->au1Psk1, au1Hash, WPS_PSK_LEN);
    u1TmpStr[0] = u1DevPasswd + (i4DevPasswdLen + 1) / 2;
    i4Len[0] = i4DevPasswdLen / 2;
    WpsAlgoHmacSha2 (AR_SHA256_ALGO, u1TmpStr, i4Len, 1, wps->au1AuthKey,
                     WPS_AUTHKEY_LEN, au1Hash);
    WPS_MEMCPY (wps->au1Psk2, au1Hash, WPS_PSK_LEN);
}

/*****************************************************************************/
/* Function Name      : generateRHASH                                        */
/*                                                                           */
/* Description        : This function generates RHASH and EHASH              */
/*                                                                           */
/* Input(s)           : *wps_data: pointer to session node                   */
/*                                                                           */
/* Output(s)          : RHASH                                                */
/*                                                                           */
/* Return Value(s)    : VOID                                                 */
/*****************************************************************************/
void
generateRHASH (tWpsSessionNode * wps_data)
{
    UINT1               au1DevPassword[8] = { 0 };
    UINT1              *au1TmpStr[4];
    INT4                ai4Len[4];

    WpsUtilGetRandom (wps_data->au1snonce1, WPS_SECRET_NONCE_LEN);
    WpsUtilGetRandom (wps_data->au1snonce2, WPS_SECRET_NONCE_LEN);

    WPS_MEMCPY (au1DevPassword, "00000000", 8);
    wpsDerivePsk (wps_data, au1DevPassword, 8);
    au1TmpStr[0] = wps_data->au1snonce1;
    ai4Len[0] = WPS_SECRET_NONCE_LEN;
    au1TmpStr[1] = wps_data->au1Psk1;
    ai4Len[1] = WPS_PSK_LEN;
    au1TmpStr[2] = wps_data->au1DhPubKeyE;
    ai4Len[2] = wps_data->u1DhPubKeyELen;
    au1TmpStr[3] = wps_data->au1DhPubkeyR;
    ai4Len[3] = WPS_PUBLIC_KEY_LEN;

    WpsAlgoHmacSha2 (AR_SHA256_ALGO, au1TmpStr, ai4Len, 4, wps_data->au1AuthKey,
                     WPS_AUTHKEY_LEN, wps_data->pWpsParseAttr->au1Rhash1);
    au1TmpStr[0] = wps_data->au1snonce2;
    ai4Len[0] = WPS_SECRET_NONCE_LEN;
    au1TmpStr[1] = wps_data->au1Psk2;
    ai4Len[1] = WPS_PSK_LEN;
    au1TmpStr[2] = wps_data->au1DhPubKeyE;
    ai4Len[2] = wps_data->u1DhPubKeyELen;
    au1TmpStr[3] = wps_data->au1DhPubkeyR;
    ai4Len[3] = WPS_PUBLIC_KEY_LEN;

    WpsAlgoHmacSha2 (AR_SHA256_ALGO, au1TmpStr, ai4Len, 4, wps_data->au1AuthKey,
                     WPS_AUTHKEY_LEN, wps_data->pWpsParseAttr->au1Rhash2);
}
