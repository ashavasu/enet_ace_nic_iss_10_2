/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: wpsprot.c,v 1.1.1.1 2016/09/20 10:42:41 siva Exp $
 *
 * Description: WPS File.
 *********************************************************************/

#include "wpsinc.h"
UINT1 wscIdentifier = 0;

PRIVATE UINT1 (*gaWpsProtocolMsgHandlers[]) (tWpsSessionNode *, tWpsEapPacketInfo *) =
{
    NULL,
    NULL,
    NULL,
    NULL,
    WpsHandleM1,
    NULL,
    NULL,
    WpsHandleM3,
    NULL,
    WpsHandleM5,
    NULL,
    WpsHandleM7,
};
/****************************************************************************
 *                                                                           *
 * Function     : WpsBuildEapHdr                                             *
 *                                                                           *
 * Description  : Function invoked to process to fill the EAP header         *
 *                                                                           *
 * Input        : WpsEapHdr, op_code                                         *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/

VOID WpsBuildEapHdr(tWpsEapHdr *WpsEapHdr, 
                    UINT1 op_code)
{
    WPS_TRC(MSG_DEBUG, "\nWpsBuildEapHdr\n");
    UINT1 au1SrcVendorID[3] = {0x00,0x37,0x2A};
    UINT1 au1SrcVendorType[4] = {0x00,0x00,0x00,0x01};
    WpsEapHdr->u1Code = EAP_REQUEST;
    WpsEapHdr->u1Identifier = wscIdentifier;
    wscIdentifier++;
    WpsEapHdr->u2Length = WPS_EAP_HEADER_SIZE;
    WpsEapHdr->u1Type = PNAC_EAP_TYPE_EXTENDED;
    WPS_MEMCPY(WpsEapHdr->au1VendorID, &au1SrcVendorID, 3);
    WPS_MEMCPY(WpsEapHdr->au1VendorType, &au1SrcVendorType, 4);
    WpsEapHdr->u1OpCode = op_code;	
    WpsEapHdr->u1Flags = 0;
}
/****************************************************************************
 *                                                                           *
 * Function     : sessionOverlap                                             *
 *                                                                           *
 * Description  : Function invoked to check for PBC flag in all the session  *
 *                entries, returns success if overlap is not found           *
 *                                                                           *
 * Input        : pWpsSessionNode                                            *
 *                                                                           *
 * Output       : WPS_SUCCESS if no overlap                                  *
 *                WPS_FAILURE if overlap is present                          *
 *                                                                           *
 * Returns      : WPS_SUCCESS/WPS_FAILURE                                    *
 *                                                                           *
 *****************************************************************************/

UINT1 sessionOverlap(tWpsSessionNode *pWpsSessionNode)
{
    WPS_TRC(MSG_DEBUG, "\nsessionOverlap\n");
    tWpsSessionNode *WpsSessionNode= NULL;
    UINT1 u1Count = 0;

    TMO_SLL_Scan (&(pWpsSessionNode->pWpsContext->wpsStationInfoList), 
	    WpsSessionNode, tWpsSessionNode*)
    {
	if (WpsSessionNode->b1Pbc == 1)
	{
	    u1Count++;
	}
    }
    if (u1Count > 1)
	return WPS_FAILURE;
    else
	return WPS_SUCCESS;
}
/****************************************************************************
 *                                                                           *
 * Function     : wpsSendEapFail                                             *
 *                                                                           *
 * Description  : Function invoked to send EAP fail to signal the end of     *
 * 		  Registration protocol                                      *
 *                                                                           *
 * Input        : pWpsEapPacketInfo, u2PortNum, macAddr of suppplicant       *
 *                                                                           *
 * Output       : NONE                                                       *
 *                                                                           *
 * Returns      : NONE                                                       *
 *                                                                           *
 *****************************************************************************/
VOID wpsSendEapFail(tWpsEapPacketInfo *pWpsEapPacketInfo,
                    UINT2 u2PortNum,
                    tMacAddr macAddr)
{
    WPS_TRC(MSG_DEBUG, "\nwpsSendEapFail\n");

    pWpsEapPacketInfo->WpsEapHdr.u1Code = EAP_FAILURE;
    wscIdentifier--;
    pWpsEapPacketInfo->WpsEapHdr.u1Identifier = wscIdentifier;
    wscIdentifier = 0;
    pWpsEapPacketInfo->WpsEapHdr.u2Length = WPS_EAP_FAIL_MSGLEN;
    pWpsEapPacketInfo->u1Version = EAP_VERSION;
    pWpsEapPacketInfo->u1Type = EAP_MSG_TYPE;
    pWpsEapPacketInfo->u2MsgLen = pWpsEapPacketInfo->WpsEapHdr.u2Length;
    pWpsEapPacketInfo->WpsEapHdr.u2Length = 
	WPS_HTONS(pWpsEapPacketInfo->WpsEapHdr.u2Length);
    pWpsEapPacketInfo->u2Length = pWpsEapPacketInfo->WpsEapHdr.u2Length;
    if (WpsTxFrame (pWpsEapPacketInfo,macAddr, u2PortNum, 
		(pWpsEapPacketInfo->u2MsgLen + EAP_HEADER_LEN)) != WPS_SUCCESS) 
    {
	WPS_TRC (WPS_CONTROL_PATH_TRC | WPS_ALL_FAILURE_TRC,
		" wpsSendEapFail: WpsTxFrame returned Failure\n");
	return ;

    }
}

/****************************************************************************
 *                                                                           *
 * Function     : wpsEapSendStart                                            *
 *                                                                           *
 * Description  : Function invoked to send EAP start to signal the start of  *

 * 		  Registration protocol                                      *
 *                                                                           *
 * Input        : supplicant macAddr and port number                         *
 *                                                                           *
 * Output       : NONE                                                       *
 *                                                                           *
 * Returns      : NONE                                                       *
 *                                                                           *
 *****************************************************************************/
VOID wpsEapSendStart(tMacAddr macAddr, 
                     UINT2 u2PortNo )
{
    WPS_TRC(MSG_DEBUG, "\nwpsEapSendStart\n");
    tWpsContext *pWpsContext = NULL;
    tWpsSessionNode    *pWpsSessionNode= NULL;
    tWpsEapPacketInfo *pWpsEapPacketInfo;
    UINT4 u4ProfileIndex = 0;
    UINT2 u2pktLen = 0;

    if (WssIfGetProfileIfIndex (u2PortNo, &u4ProfileIndex) == OSIX_FAILURE)
    {
	WPS_TRC (WPS_CONTROL_PATH_TRC | WPS_ALL_FAILURE_TRC, 
		"wpsEapSendStart:- "
		"WssIfGetProfileIfIndex Failed !!!! \n");
	return ;
    }
    WpsGetPortEntry ((UINT2) u4ProfileIndex, &pWpsContext);
    if (pWpsContext == NULL)
    {
	WPS_TRC ((WPS_CONTROL_PATH_TRC | WPS_ALL_FAILURE_TRC), 
		"wpsEapSendStart:No WPS Info for the given port!! ... \n");
	return ;
    }
    pWpsSessionNode = WpsUtilGetStaInfoFromStaAddr(pWpsContext, macAddr);
    if (pWpsSessionNode == NULL)
    {
	WPS_TRC (WPS_CONTROL_PATH_TRC | WPS_ALL_FAILURE_TRC, 
		"wpsEapSendStart:No session Info for the given port!! ... \n");
	return ;
    } 

    pWpsSessionNode->e1state = RECV_M1;
    WpsTmrStartTimer(&pWpsSessionNode->WpsProtocolTmr, WPS_PROTOCOL_TIMER_ID, 
	    WPS_PROTOCOL_TIME, pWpsSessionNode,NUM_OF_AP_SUPPORTED);

    if ((pWpsEapPacketInfo = (tWpsEapPacketInfo*) WPS_MEM_ALLOCATE_MEM_BLK
		(WPS_EAP_PACKET_INFO_MEMPOOL_ID)) == NULL)
    {
	WPS_TRC (WPS_INIT_SHUT_TRC,
		"wpsEapSendStart:- WPS_EAP_PACKET_INFO_MEMPOOL_ID returned Failure\n");
	wpsDeleteSession(pWpsSessionNode);
	return ;
    }
    memset(pWpsEapPacketInfo,0,sizeof(tWpsEapPacketInfo));

    pWpsEapPacketInfo->u1Version = EAP_VERSION;
    pWpsEapPacketInfo->u1Type = EAP_MSG_TYPE;

    WpsBuildEapHdr(&(pWpsEapPacketInfo->WpsEapHdr), WpsStart); 
 
    u2pktLen = pWpsEapPacketInfo->WpsEapHdr.u2Length;

    pWpsEapPacketInfo->WpsEapHdr.u2Length = WPS_HTONS(pWpsEapPacketInfo->WpsEapHdr.u2Length);
    pWpsEapPacketInfo->u2Length = pWpsEapPacketInfo->WpsEapHdr.u2Length;

    if (WpsTxFrame (pWpsEapPacketInfo,macAddr, u2PortNo, 
		(u2pktLen + EAP_HEADER_LEN)) != WPS_SUCCESS) 
    {
	if (WPS_MEM_RELEASE_MEM_BLK (WPS_EAP_PACKET_INFO_MEMPOOL_ID,
		    pWpsEapPacketInfo) == MEM_FAILURE)
	{ 
	    WPS_TRC (WPS_CONTROL_PATH_TRC | WPS_ALL_FAILURE_TRC,
		    "wpsEapSendStart:- WPS_EAP_PACKET_INFO_MEMPOOL_ID release"
		    "Failed !!!! \n");
	}
	WPS_TRC (WPS_CONTROL_PATH_TRC | WPS_ALL_FAILURE_TRC, 
		" wpsEapSendStart: WpsTxFrame returned Failure\n");
	wpsDeleteSession(pWpsSessionNode);
	return ;
    }
    if (WPS_MEM_RELEASE_MEM_BLK (WPS_EAP_PACKET_INFO_MEMPOOL_ID,
		pWpsEapPacketInfo) == MEM_FAILURE)
    {
	WPS_TRC (WPS_CONTROL_PATH_TRC | WPS_ALL_FAILURE_TRC,
		"wpsEapSendStart:- WPS_EAP_PACKET_INFO_MEMPOOL_ID release"
		"Failed !!!! \n");
    }
}

/****************************************************************************
 *                                                                           *
 * Function     : WpsProcessEap                                              *
 *                                                                           *
 * Description  : Function invoked to process the received EAP Packet        *
 *                                                                           *
 * Input        : pu1Pkt : Received EAP packet                               *
 *                u2PktLen: Received EAP packet Len                          *
 *                u2PortNum : port on which the packet is received           *
 *                pu1SrcAddr : supplicant macAddr                            *
 *                                                                           *
 * Output       : WPS_SUCCESS/WPS_FAILURE                                    *
 *                                                                           *
 * Returns      : WPS_SUCCESS/WPS_FAILURE                                    *
 *                                                                           *
 *****************************************************************************/
UINT1 WpsProcessEap(UINT1 *pu1Pkt, 
                    UINT2 u2PktLen, 
                    UINT2 u2PortNum, 
                    UINT1 *pu1SrcAddr)
{
    WPS_TRC(MSG_DEBUG, "\nWpsProcessEap\n");
    tWpsContext *pWpsContext = NULL;
    tWpsSessionNode    *pWpsSessionNode= NULL;
    tWpsEapPacketInfo *pWpsEapPacketInfo = NULL;
    tWpsStat * pWpsStat= NULL;
    UINT4 u4ProfileIndex = 0;
    UINT2 u2Index = 0;
    UINT1 u1RetVal = 0;

    if (WssIfGetProfileIfIndex (u2PortNum, &u4ProfileIndex) == OSIX_FAILURE)
    {
	WPS_TRC (WPS_CONTROL_PATH_TRC | WPS_ALL_FAILURE_TRC, 
		"WpsProcessEap:- "
		"WssIfGetProfileIfIndex Failed !!!! \n");
	return (OSIX_FAILURE);
    }
    WpsGetPortEntry ((UINT2) u4ProfileIndex, &pWpsContext); 
    if (pWpsContext == NULL)
    {
	WPS_TRC ((WPS_CONTROL_PATH_TRC | WPS_ALL_FAILURE_TRC), 
		"WpsProcessEap:No WPS Info for the given port!! ... \n");
	return (OSIX_FAILURE);
    }
    pWpsSessionNode = WpsUtilGetStaInfoFromStaAddr(pWpsContext, pu1SrcAddr);
    if (pWpsSessionNode == NULL)
    {
	WPS_TRC (WPS_CONTROL_PATH_TRC | WPS_ALL_FAILURE_TRC, 
		"WpsProcessEap: No Session Info for the given port!! ... \n");
	return (OSIX_FAILURE);
    } 
    if ((pWpsEapPacketInfo = (tWpsEapPacketInfo*) WPS_MEM_ALLOCATE_MEM_BLK
		(WPS_EAP_PACKET_INFO_MEMPOOL_ID)) == NULL)
    {
	WPS_TRC (WPS_CONTROL_PATH_TRC | WPS_ALL_FAILURE_TRC, 
		"WpsProcessEap: WPS_EAP_PACKET_INFO_MEMPOOL_ID returned Failure!! ... \n");
	wpsDeleteSession(pWpsSessionNode);
	return (WPS_FAILURE);
    }
    memset(pWpsEapPacketInfo,0,sizeof(tWpsEapPacketInfo)); 
    WPS_MEMCPY(&pWpsEapPacketInfo->WpsEapHdr,pu1Pkt,u2PktLen);
    if (pWpsEapPacketInfo->WpsEapHdr.u1Type != PNAC_EAP_TYPE_EXTENDED)
    {
	WPS_TRC (WPS_CONTROL_PATH_TRC | WPS_ALL_FAILURE_TRC,
		"WpsProcessEap:- wrong packet routed from PNAC, returned Failure\n");
	return WPS_FAILURE;
    }
    if (pWpsEapPacketInfo->WpsEapHdr.u1Code == EAP_REQUEST)
    {
	WPS_TRC (WPS_CONTROL_PATH_TRC | WPS_ALL_FAILURE_TRC,
		"WpsProcessEap:- EAP_REQ is not expected at AP, ignore MSg\n");
	return WPS_SUCCESS;
    }
    pWpsEapPacketInfo->WpsEapHdr.u2Length = 
	(UINT2)WPS_NTOHS(pWpsEapPacketInfo->WpsEapHdr.u2Length);

    WpsTmrStopTimer (&(pWpsSessionNode->WpsReTransTmr));
    WpsTmrStopTimer (&(pWpsSessionNode->WpsMsgTmr));
    switch(pWpsEapPacketInfo->WpsEapHdr.u1OpCode)
    {
	case WSC_NACK:
	    wpsSendEapFail(pWpsEapPacketInfo,  u2PortNum, pu1SrcAddr);
	    for (u2Index = 0; u2Index < NUM_OF_AP_SUPPORTED; u2Index++)
	    {
		if (pWpsSessionNode->u2WtpInternalId == 
			pWpsSessionNode->pWpsContext->pWpsConfigDb[u2Index].u2WtpInternalId)
		{
		    break;
		}
	    }
	    if (u2Index != NUM_OF_AP_SUPPORTED)
	    {
		pWpsSessionNode->pWpsContext->pWpsConfigDb[u2Index].u4PushButtonStatus = WPS_PBC_STATUS_ERROR;
		pWpsContext->pWpsConfigDb[u2Index].b1WpsAdditionalIE = WPS_ADDITIONLALIE_DISABLE;
		WssSendUpdateWlanMsgfromWps ((UINT4) pWpsSessionNode->pWpsContext->u2Port, 
			pWpsSessionNode->u2WtpInternalId,
			SET_ADDITIONAL_IE);
		pWpsStat = WpsUtilGetStationFromStatsTable( pWpsSessionNode->pWpsContext->u2Port,
			pWpsSessionNode->au1SuppMacAddr);
		if (pWpsStat != NULL)
		{
		    pWpsStat->e1PbcStatus = pWpsSessionNode->pWpsContext->pWpsConfigDb[u2Index].u4PushButtonStatus;
		    pWpsStat->e1Status = WPS_STATUS_FAILURE;
		}
	    }
	    break;

	case WpsMSG:
	    u1RetVal = wpsProcessEapMsg(pWpsEapPacketInfo, pWpsSessionNode, u2PortNum,pu1SrcAddr);
		if(u1RetVal == WPS_FAILURE)	
		  {
			
            		WPS_MEM_RELEASE_MEM_BLK (WPS_EAP_PACKET_INFO_MEMPOOL_ID,pWpsEapPacketInfo); 
			WPS_TRC (WPS_CONTROL_PATH_TRC | WPS_ALL_FAILURE_TRC, 
			"WpsProcessEap: WPS_EAP_PACKET_INFO_MEMPOOL_ID Registration Protocl Failure!! ... \n");
			return (WPS_FAILURE);
		
		  }
	    break;

	case WpsACK:
	    break;

	case WpsDone:
	    wpsSendEapFail(pWpsEapPacketInfo,  u2PortNum, pu1SrcAddr);
	    for (u2Index = 0; u2Index < NUM_OF_AP_SUPPORTED; u2Index++)
	    {
		if (pWpsSessionNode->u2WtpInternalId == 
			pWpsSessionNode->pWpsContext->pWpsConfigDb[u2Index].u2WtpInternalId)
		{
		    break;
		}
	    }
	    if (u2Index != NUM_OF_AP_SUPPORTED)
	    {
		pWpsSessionNode->pWpsContext->pWpsConfigDb[u2Index].u4PushButtonStatus = WPS_PBC_SUCCESS;
		pWpsContext->pWpsConfigDb[u2Index].b1WpsAdditionalIE = WPS_ADDITIONLALIE_DISABLE;
		WssSendUpdateWlanMsgfromWps ((UINT4) pWpsSessionNode->pWpsContext->u2Port,
			pWpsSessionNode->u2WtpInternalId,
			SET_ADDITIONAL_IE);
		WssWpsSendDeAuthMsg (pWpsSessionNode->pWpsContext->au1SrcMac,
			pu1SrcAddr,pWpsSessionNode->u2SessId);
		pWpsStat = WpsUtilGetStationFromStatsTable( 
			pWpsSessionNode->pWpsContext->u2Port,
			pWpsSessionNode->au1SuppMacAddr);
		if (pWpsStat != NULL)
		{
		    pWpsStat->e1PbcStatus = pWpsSessionNode->pWpsContext->pWpsConfigDb[u2Index].u4PushButtonStatus;
		    pWpsStat->e1Status = WPS_STATUS_SUCCESS;
		}
	    }
	    wpsDeleteSession(pWpsSessionNode);

	    pWpsSessionNode->e1state = RECV_DONE;

	case WpsFragAck:
	    break;
	default:
	    WPS_TRC (WPS_CONTROL_PATH_TRC | WPS_ALL_FAILURE_TRC,
		    " WpsProcessEap: invalid msg type\n");

    }
    return WPS_SUCCESS;
}

/****************************************************************************
 *                                                                           *
 * Function     : wpsProcessEapMsg                                           *
 *                                                                           *
 * Description  : Function invoked to process the received registration      *
 * 		  Protocol message                                           *
 *                                                                           *
 * Input        : pWpsEapPacketInfo : Received EAP packet                    *
 *                pWpsSessionNode: supplicant's session node                    *
 *                u2PortNum : port on which the packet is received           *
 *                pu1SrcAddr : supplicant macAddr                            *
 *                                                                           *
 * Output       : WPS_SUCCESS/WPS_FAILURE                                    *
 *                                                                           *
 * Returns      : WPS_SUCCESS/WPS_FAILURE                                    *
 *                                                                           *
 *****************************************************************************/
UINT1 wpsProcessEapMsg(tWpsEapPacketInfo *pWpsEapPacketInfo,
                       tWpsSessionNode *pWpsSessionNode,
                       UINT2 u2PortNum,
		       UINT1 *pu1SrcAddr)
{
    WPS_TRC(MSG_DEBUG, "\nwpsProcessEapMsg\n");
    UINT1 u1ReturnType = 0;

    u1ReturnType = wpsValidateMsg(pWpsEapPacketInfo, pWpsSessionNode);
    pWpsSessionNode->pWpsParseAttr->u2ConfigError = WPS_CFG_NO_ERROR;
    if(u1ReturnType != WPS_SUCCESS)
    {
	WPS_TRC(MSG_DEBUG, "\nwpsProcessEapMsg: Invalid message received\n ");
	wpsBuildMsgResp(WSC_NACK,pWpsSessionNode, pWpsEapPacketInfo);
	wpsDeleteSession(pWpsSessionNode);
	return WPS_FAILURE;
    }
    gaWpsProtocolMsgHandlers[pWpsSessionNode->pWpsParseAttr->u1MsgType]
	(pWpsSessionNode,pWpsEapPacketInfo);

    pWpsEapPacketInfo->u1Version = EAP_VERSION;
    pWpsEapPacketInfo->u1Type = EAP_MSG_TYPE;
    pWpsEapPacketInfo->u2MsgLen = pWpsEapPacketInfo->WpsEapHdr.u2Length;
    pWpsEapPacketInfo->WpsEapHdr.u2Length = 
	WPS_HTONS(pWpsEapPacketInfo->WpsEapHdr.u2Length);
    pWpsEapPacketInfo->u2Length = pWpsEapPacketInfo->WpsEapHdr.u2Length;

    if (WpsTxFrame (pWpsEapPacketInfo,pu1SrcAddr, u2PortNum, 
		(pWpsEapPacketInfo->u2MsgLen + EAP_HEADER_LEN)) != WPS_SUCCESS) 
    {
	WPS_TRC (WPS_CONTROL_PATH_TRC | WPS_ALL_FAILURE_TRC,
		" wpsProcessEapMsg: WpsTxFrame returned Failure\n");
	wpsDeleteSession(pWpsSessionNode);
	return WPS_FAILURE;
    }
    WPS_MEMCPY(pWpsSessionNode->cur_msg.au1Buf, pWpsEapPacketInfo, 
	    pWpsEapPacketInfo->u2MsgLen);
    pWpsSessionNode->cur_msg.i4used= pWpsEapPacketInfo->WpsEapHdr.u2Length;
    pWpsSessionNode->u2PortNum = u2PortNum;
    pWpsSessionNode->u1MsgCount = 1;
    if ((pWpsEapPacketInfo->WpsEapHdr.au1Data[MSG_TYPE_OFFSET] == WSC_NACK) || (pWpsEapPacketInfo->WpsEapHdr.au1Data[MSG_TYPE_OFFSET] == M2D))
    {
    if ((pWpsEapPacketInfo = (tWpsEapPacketInfo*) WPS_MEM_ALLOCATE_MEM_BLK
		(WPS_EAP_PACKET_INFO_MEMPOOL_ID)) == NULL)
    {
	WPS_TRC (WPS_INIT_SHUT_TRC,
		"wpsEapSendStart:- WPS_EAP_PACKET_INFO_MEMPOOL_ID returned Failure\n");
	wpsDeleteSession(pWpsSessionNode);
	return WPS_FAILURE;
    }
    memset(pWpsEapPacketInfo,0,sizeof(tWpsEapPacketInfo));
    wpsSendEapFail(pWpsEapPacketInfo,  u2PortNum, pu1SrcAddr);
    if (WPS_MEM_RELEASE_MEM_BLK (WPS_EAP_PACKET_INFO_MEMPOOL_ID,
		pWpsEapPacketInfo) == MEM_FAILURE)
    {
	WPS_TRC (WPS_CONTROL_PATH_TRC | WPS_ALL_FAILURE_TRC,
		"wpsEapSendStart:- WPS_EAP_PACKET_INFO_MEMPOOL_ID release"
		"Failed !!!! \n");
    }
	wpsDeleteSession(pWpsSessionNode);
    }
    else
    {
	WpsTmrStartTimer(&pWpsSessionNode->WpsReTransTmr, WPS_RETRANS_TIMER_ID,
		WPS_RETRANS_TIME, pWpsSessionNode,NUM_OF_AP_SUPPORTED);
	WpsTmrStartTimer(&pWpsSessionNode->WpsMsgTmr, WPS_MSG_TIMER_ID,
		WPS_MSG_TIMER_ID, pWpsSessionNode,NUM_OF_AP_SUPPORTED);
    }
    return WPS_SUCCESS;
}

/****************************************************************************
 *                                                                           *
 * Function     : wpsProcessEapMsg                                           *
 *                                                                           *
 * Description  : Function invoked to process the received M1 message        *
 *                                                                           *
 * Input        : pWpsEapPacketInfo : Received EAP packet                    *
 *                pWpsSessionNode: supplicant's session node                 *
 *                                                                           *
 * Output       : M2/NACK                                                    *
 *                                                                           *
 * Returns      : NONE                                                       *
 *                                                                           *
 *****************************************************************************/
UINT1 WpsHandleM1(tWpsSessionNode *pWpsSessionNode, 
                  tWpsEapPacketInfo *pWpsEapPacketInfo)
{
    WPS_TRC(MSG_DEBUG, "\nWpsHandleM1\n");
    UINT2 u2AuthTypes = 0;
    UINT2 u2EncrTypes = 0;
    UINT1 u1MsgType = 0;
    UINT1 u1ReturnType = 0;
    UINT2 u2Index = 0;
    UINT1 u1Index = 0;

    if(pWpsSessionNode->pWpsParseAttr->u1MsgType != M1)
    {
	WPS_TRC (WPS_ALL_FAILURE_TRC,"\nWpsHandleM1:unexpected state\n");
	goto sendNack;
    }	
    if(pWpsSessionNode->e1state != RECV_M1)
    {
	WPS_TRC (WPS_ALL_FAILURE_TRC,"\nWpsHandleM1:must be a duplicate ignore\n");
	return WPS_FAILURE;
    }	

    WPS_MEMCPY(pWpsSessionNode->peer_dev.u1MacAddr,
	    pWpsSessionNode->pWpsParseAttr->au1MacAddr,ETH_ALEN);
    WPS_MEMCPY(pWpsSessionNode->au1NonceE,
	    pWpsSessionNode->pWpsParseAttr->au1EnrolleeNonce,
	    WPS_ENROLLEE_NONCE_LEN);
    for (u2Index = 0; u2Index < NUM_OF_AP_SUPPORTED; u2Index++)
    {
	if (pWpsSessionNode->u2WtpInternalId == 
		pWpsSessionNode->pWpsContext->pWpsConfigDb[u2Index].u2WtpInternalId)
	{
	    if (((sessionOverlap(pWpsSessionNode)) != WPS_SUCCESS) ||  
		    (pWpsSessionNode->pWpsContext->pWpsConfigDb[u2Index].u4PushButtonStatus == WPS_PBC_OVERLAP))
	    {
		pWpsSessionNode->pWpsParseAttr->u2ConfigError = WPS_CFG_MULTIPLE_PBC_DETECTED;
		pWpsSessionNode->pWpsContext->pWpsConfigDb[u2Index].u4PushButtonStatus = WPS_PBC_OVERLAP;
		goto sendNack;
	    }
	    break;
	}
    }
    if(pWpsSessionNode->pWpsParseAttr->u2PublicKeyLen != 0)
    {	
	WPS_MEMCPY(pWpsSessionNode->au1DhPubKeyE,
		pWpsSessionNode->pWpsParseAttr->au1PublicKey,
		pWpsSessionNode->pWpsParseAttr->u2PublicKeyLen);
	pWpsSessionNode->u1DhPubKeyELen = 
	    pWpsSessionNode->pWpsParseAttr->u2PublicKeyLen;
    }
    if(pWpsSessionNode->pWpsParseAttr->u2AuthTypeFlags != 0)
    {
	u2AuthTypes = (pWpsSessionNode->pWpsParseAttr->u2AuthTypeFlags) & 
	    (pWpsSessionNode->pWpsContext->u2ApAuthType);
	if ( u2AuthTypes == 0)
	{
	    goto sendNack;
	}
	pWpsSessionNode->u2AuthType = pWpsSessionNode->pWpsContext->u2ApAuthType;
    }
    if(pWpsSessionNode->pWpsParseAttr->u2EncrTypeFlags != 0)
    {
	u2EncrTypes = (pWpsSessionNode->pWpsParseAttr->u2EncrTypeFlags)&
	    (pWpsSessionNode->pWpsContext->u2ApEncrType);
	if (u2EncrTypes == 0)
	{
	    goto sendNack;
	}
	pWpsSessionNode->u2EncrType = pWpsSessionNode->pWpsContext->u2ApEncrType;
	pWpsSessionNode->u2EncrType = WPS_ENCR_AES;
    }
    if(pWpsSessionNode->pWpsParseAttr->u1ConnTypeFlags == 0)
    {
	goto sendNack;
    }
    if(pWpsSessionNode->pWpsParseAttr->u1WpsState == 0)
    {
	goto sendNack;
    }		
    WPS_MEMCPY(pWpsSessionNode->peer_dev.u1Manufacturer, 
	    pWpsSessionNode->pWpsParseAttr->au1Manufacturer, 
	    pWpsSessionNode->pWpsParseAttr->u2Manufacturer_len);
    WPS_MEMCPY(pWpsSessionNode->peer_dev.u1ModelName,
	    pWpsSessionNode->pWpsParseAttr->au1ModelName,
	    pWpsSessionNode->pWpsParseAttr->u2ModelNameLen);
    WPS_MEMCPY(pWpsSessionNode->peer_dev.u1ModelNumber,
	    pWpsSessionNode->pWpsParseAttr->au1ModelNumber,
	    pWpsSessionNode->pWpsParseAttr->u2ModelNumberLen);
    WPS_MEMCPY(pWpsSessionNode->peer_dev.u1SerialNumber, 
	    pWpsSessionNode->pWpsParseAttr->au1SerialNumber, 
	    pWpsSessionNode->pWpsParseAttr->u2SerialNumberLen);
    WPS_MEMCPY(pWpsSessionNode->peer_dev.u1PriDevType, 
	    pWpsSessionNode->pWpsParseAttr->au1PrimaryDevType, 
	    WPS_DEV_TYPE_LEN);
    WPS_MEMCPY(pWpsSessionNode->peer_dev.u1DeviceName, 
	    pWpsSessionNode->pWpsParseAttr->au1DevName, 
	    pWpsSessionNode->pWpsParseAttr->u2DevNameLen);
    if(pWpsSessionNode->pWpsParseAttr->u1RfBands != 0)
    {
	pWpsSessionNode->peer_dev.rf_bands = pWpsSessionNode->pWpsParseAttr->u1RfBands;
    }
    WPS_MEMCPY(pWpsSessionNode->au1uuidR,pWpsSessionNode->pWpsContext->au1uuid,WPS_UUID_LEN);
    WpsUtilGetRandom(pWpsSessionNode->au1NonceR,WPS_NONCE_LEN);
    WPS_MEMCPY(pWpsSessionNode->last_msg.au1Buf, 
	    pWpsEapPacketInfo->WpsEapHdr.au1Data, 
	    pWpsEapPacketInfo->u2MsgLen);
    pWpsSessionNode->last_msg.i4used=pWpsEapPacketInfo->u2MsgLen; 

    for (u2Index = 0; u2Index < NUM_OF_AP_SUPPORTED; u2Index++)
    {
	if (pWpsSessionNode->u2WtpInternalId == 
		pWpsSessionNode->pWpsContext->pWpsConfigDb[u2Index].u2WtpInternalId)
	{
	    pWpsSessionNode->u2ConfigMethods = 
		pWpsSessionNode->pWpsContext->pWpsConfigDb[u2Index].u2ConfigMethods;
	    break;
	}
    }
    if ((pWpsSessionNode->pWpsParseAttr->u2DevPasswordId == DEV_PW_PUSHBUTTON) &&
	    (u2Index != NUM_OF_AP_SUPPORTED) &&
	    (pWpsSessionNode->pWpsContext->pWpsConfigDb[u2Index].u4PushButtonStatus == WPS_PBC_INPROGRESS)) 
    {
	u1MsgType = M2;
	u1ReturnType = wpsGenerateKeys(pWpsSessionNode);
	if (u1ReturnType == WPS_FAILURE)
	{
	    goto sendNack;
	}
    }
    else
    {
	u1Index = (UINT1)u2Index;
	WPS_TRC_ARG4(MSG_DEBUG, "\nWpsHandleM1: sending M2D \n"
			"deviced PWD = %d\n req to enroll = %d\n u4PushButtonStatus = %d\n u2Index = %d", 
                         pWpsSessionNode->pWpsParseAttr->u2DevPasswordId,
                         pWpsSessionNode->pWpsParseAttr->u1RequestToEnroll,
                         pWpsSessionNode->pWpsContext->pWpsConfigDb[u1Index].u4PushButtonStatus,
                         u2Index);
	u1MsgType = M2D;
    }

    wpsBuildMsgResp(u1MsgType,pWpsSessionNode, pWpsEapPacketInfo);
    return WPS_SUCCESS;

sendNack:
    u1MsgType = WSC_NACK;
    wpsBuildMsgResp(u1MsgType,pWpsSessionNode, pWpsEapPacketInfo);   
    return WPS_FAILURE;
}
/****************************************************************************
 *                                                                           *
 * Function     : wpsProcessEapMsg                                           *
 *                                                                           *
 * Description  : Function invoked to process the received M3 message        *
 *                                                                           *
 * Input        : pWpsEapPacketInfo : Received EAP packet                    *
 *                pWpsSessionNode: supplicant's session node                    *
 *                                                                           *
 * Output       : WPS_SUCCESS/WPS_FAILURE                                    *
 *                                                                           *
 * Returns      : WPS_SUCCESS/WPS_FAILURE                                    *
 *                                                                           *
 *****************************************************************************/
UINT1 WpsHandleM3(tWpsSessionNode *pWpsSessionNode, tWpsEapPacketInfo *pWpsEapPacketInfo)
{
    WPS_TRC(MSG_DEBUG, "\nWpsHandleM3\n");
    UINT1 au1Hash[32] = {0};
    UINT1 *pTmpStr[2] = {NULL};
    INT4 ai4len[2] = {0};
    UINT1 MsgType = 0;
    UINT1 nonceType = 0;
    UINT2 u2Index = 0;
    WPS_TRC(MSG_DEBUG, "\nWpsHandleM3: Received M3\n");

    if (pWpsSessionNode->pWpsParseAttr->u1MsgType != M3) {
	WPS_TRC_ARG1(MSG_DEBUG, "\nWpsHandleM3: Unexpected state (%d) for "
		"receiving M3", pWpsSessionNode->e1state);
	goto sendNack;
    }
    if(pWpsSessionNode->e1state != RECV_M3)
    {
	WPS_TRC (WPS_ALL_FAILURE_TRC,"\nWpsHandleM3:must be a duplicate ignore\n");
	return WPS_FAILURE;
    }	

    for (u2Index = 0; u2Index < NUM_OF_AP_SUPPORTED; u2Index++)
    {
	if (pWpsSessionNode->u2WtpInternalId == 
		pWpsSessionNode->pWpsContext->pWpsConfigDb[u2Index].u2WtpInternalId)
	{
	    break;
	}
    }
    if (u2Index == NUM_OF_AP_SUPPORTED)
    {
	WPS_TRC(MSG_DEBUG, "\nWpsHandleM3: button is not pushed yet\n");
	goto sendNack;
    }
    if (((sessionOverlap(pWpsSessionNode)) != WPS_SUCCESS) ||  
	    (pWpsSessionNode->pWpsContext->pWpsConfigDb[u2Index].u4PushButtonStatus == WPS_PBC_OVERLAP))
    {
	pWpsSessionNode->pWpsParseAttr->u2ConfigError = WPS_CFG_MULTIPLE_PBC_DETECTED;
	pWpsSessionNode->pWpsContext->pWpsConfigDb[u2Index].u4PushButtonStatus = WPS_PBC_OVERLAP;
	goto sendNack;
    }
    if(0 !=MEMCMP(pWpsSessionNode->pWpsParseAttr->au1RegistrarNonce,
		pWpsSessionNode->au1NonceR,WPS_NONCE_LEN))
    {
	goto sendNack;
    }
    WPS_MEMCPY(pWpsSessionNode->au1PeerHash1,
	    pWpsSessionNode->pWpsParseAttr->au1Ehash1,
	    WPS_HASH_LEN);
    WPS_MEMCPY(pWpsSessionNode->au1PeerHash2,
	    pWpsSessionNode->pWpsParseAttr->au1Ehash2,
	    WPS_HASH_LEN);
    pTmpStr[0] = pWpsSessionNode->last_msg.au1Buf;
    ai4len[0] = pWpsSessionNode->last_msg.i4used;
    pTmpStr[1] = pWpsEapPacketInfo->WpsEapHdr.au1Data;
    ai4len[1] = (INT4) (pWpsEapPacketInfo->u2MsgLen - 4 - 
	    WPS_AUTHENTICATOR_LEN); 
    WpsAlgoHmacSha2(AR_SHA256_ALGO,pTmpStr,ai4len,2,
	    pWpsSessionNode->au1AuthKey,WPS_AUTHKEY_LEN,au1Hash); 

    if(MEMCMP(au1Hash, pWpsSessionNode->pWpsParseAttr->au1Authenticator, 
		WPS_AUTHENTICATOR_LEN)!=0 )
    {
	goto sendNack;
    }
    WPS_MEMCPY(pWpsSessionNode->last_msg.au1Buf, pWpsEapPacketInfo->WpsEapHdr.au1Data, 
	    pWpsEapPacketInfo->u2MsgLen);
    pWpsSessionNode->last_msg.i4used=pWpsEapPacketInfo->u2MsgLen; 
    generateRHASH(pWpsSessionNode);
    nonceType = TYPE1;
    wpsBuildKeyWrapAuth(pWpsSessionNode,pWpsSessionNode->au1snonce1, nonceType);
    wpsBuildMsgResp(M4,pWpsSessionNode, pWpsEapPacketInfo);
    return WPS_SUCCESS;

sendNack:
    MsgType = WSC_NACK;
    wpsBuildMsgResp(MsgType,pWpsSessionNode, pWpsEapPacketInfo);   
    return WPS_FAILURE;
}
/****************************************************************************
 *                                                                           *
 * Function     : wpsBuildKeyWrapAuth                                        *
 *                                                                           *
 * Description  : build the encryption settings for M4 and M6                *
 *                                                                           *
 * Input        : pWpsSessionNode: supplicant's session node                    *
 *  		  nonce : Snonce of the registrar                            *
 *  		  nonceType: Snonce1/Snonce2                                 *
 *                                                                           *
 * Output       :                                                            *
 *                                                                           *
 * Returns      :                                                            *
 *                                                                           *
 *****************************************************************************/
VOID wpsBuildKeyWrapAuth(tWpsSessionNode *pWpsSessionNode, 
					UINT1 nonce[16], UINT1 nonceType)
{
    WPS_TRC(MSG_DEBUG, "\nwpsBuildKeyWrapAuth\n");
    INT4 i4BufferOffset = 0;
    struct wpsbuf plain;
    UINT1 au1InitVector[WPS_INITIALIZATION_VECTOR_LEN] = {0};
    UINT1 *pTmpstr[1] = {NULL};
    INT4 i4len[1] = {0};
    INT4 i4PadLen = 0;
    const UINT2 u2BlockSize = 16;
    UINT2 i = 0;

    WPS_MEMSET(plain.au1Buf,'\0',1024);
    if (nonceType == TYPE1)
    {
	WPS_PUT_2BYTE_VP(plain.au1Buf,WPS_R_SNONCE1_TAG,i4BufferOffset);
    }
    else
    {
	WPS_PUT_2BYTE_VP(plain.au1Buf,WPS_R_SNONCE2_TAG,i4BufferOffset);
    }
    WPS_PUT_2BYTE_VP(plain.au1Buf,WPS_SECRET_NONCE_LEN,i4BufferOffset);
    WPS_PUT_NBYTE_PP(plain.au1Buf,nonce,WPS_SECRET_NONCE_LEN,i4BufferOffset);

    pTmpstr[0] = plain.au1Buf;
    i4len[0] = i4BufferOffset;

    WpsAlgoHmacSha2(AR_SHA256_ALGO,pTmpstr,i4len,1,pWpsSessionNode->au1AuthKey,
	    WPS_AUTHKEY_LEN,pWpsSessionNode->pWpsParseAttr->au1KeyWrapAuth); 
    WPS_PUT_2BYTE_VP(plain.au1Buf,WPS_KEY_WRAP_AUTHENTICATOR_TAG,i4BufferOffset);
    WPS_PUT_2BYTE_VP(plain.au1Buf,WPS_KEY_WRAP_AUTHENTICATOR_LEN,i4BufferOffset);
    WPS_PUT_NBYTE_PP(plain.au1Buf,pWpsSessionNode->pWpsParseAttr->au1KeyWrapAuth,
    WPS_KEY_WRAP_AUTHENTICATOR_LEN,i4BufferOffset);
    plain.i4used = i4BufferOffset;
    i4PadLen = u2BlockSize - (plain.i4used % u2BlockSize);
	WPS_MEMSET(plain.au1Buf+plain.i4used, i4PadLen, i4PadLen);
	plain.i4used += i4PadLen;

	WPS_MEMSET(au1InitVector,0,WPS_INITIALIZATION_VECTOR_LEN);
	WpsUtilGetRandom(au1InitVector,WPS_INITIALIZATION_VECTOR_LEN);
	WPS_MEMCPY(pWpsSessionNode->pWpsParseAttr->au1IV,au1InitVector,WPS_INITIALIZATION_VECTOR_LEN);

	if(AesArCbcEncrypt(plain.au1Buf,plain.i4used,AES_128_KEY_LENGTH,
		pWpsSessionNode->au1Keywrapkey,au1InitVector))        
        {
		WPS_MEMSET(pWpsSessionNode->pWpsParseAttr->au1EncrSettings,'\0',
		sizeof(pWpsSessionNode->pWpsParseAttr->au1EncrSettings));
		WPS_MEMCPY(pWpsSessionNode->pWpsParseAttr->au1EncrSettings,plain.au1Buf,plain.i4used);
		pWpsSessionNode->pWpsParseAttr->u4EncrSettingsLen = (UINT4)plain.i4used;
	}
    }

/****************************************************************************
 *                                                                           *
 * Function     : WpsHandleM5                                                *
 *                                                                           *
 * Description  : Function invoked to process the received M5 message        *
 *                                                                           *
 * Input        : pWpsEapPacketInfo : Received EAP packet                    *
 *                pWpsSessionNode: supplicant's session node                    *
 *                                                                           *
 * Output       : WPS_SUCCESS/WPS_FAILURE                                    *
 *                                                                           *
 * Returns      : WPS_SUCCESS/WPS_FAILURE                                    *
 *                                                                           *
 *****************************************************************************/

UINT1 WpsHandleM5(tWpsSessionNode *pWpsSessionNode, tWpsEapPacketInfo *pWpsEapPacketInfo)
{
    WPS_TRC(MSG_DEBUG, "\nWpsHandleM5\n");
    UINT1 au1Hash[32] = {0};
    UINT1 u1ReturnType = 0;
    UINT1 u1MsgType = 0;
    UINT1 u1WpsValidatePsk = 0;
    UINT2 u2Index = 0;


    if (pWpsSessionNode->pWpsParseAttr->u1MsgType != M5) {
	WPS_TRC_ARG1(MSG_DEBUG, "\nWpsHandleM5: Unexpected state (%d) for "
		"receiving M5", pWpsSessionNode->e1state);
	pWpsSessionNode->e1state = WSC_NACK;
	goto sendNack;
    }
    if(pWpsSessionNode->e1state != RECV_M5)
    {
	WPS_TRC (WPS_ALL_FAILURE_TRC,"\nWpsHandleM5:must be a duplicate ignore\n");
	return WPS_FAILURE;
    }	
    if(MEMCMP(pWpsSessionNode->au1NonceR,
		pWpsSessionNode->pWpsParseAttr->au1RegistrarNonce,
		WPS_REGISTRAR_NONCE_LEN))
    {
	goto sendNack;
    }  
    for (u2Index = 0; u2Index < NUM_OF_AP_SUPPORTED; u2Index++)
    {
	if (pWpsSessionNode->u2WtpInternalId == 
		pWpsSessionNode->pWpsContext->pWpsConfigDb[u2Index].u2WtpInternalId)
	{
	    break;
	}
    }
    if (u2Index == NUM_OF_AP_SUPPORTED)
    {
	WPS_TRC(MSG_DEBUG, "\nWpsHandleM5: button is not pushed yet\n");
	goto sendNack;
    }
    if (((sessionOverlap(pWpsSessionNode)) != WPS_SUCCESS) ||  
	    (pWpsSessionNode->pWpsContext->pWpsConfigDb[u2Index].u4PushButtonStatus == WPS_PBC_OVERLAP))
    {
	pWpsSessionNode->pWpsParseAttr->u2ConfigError = WPS_CFG_MULTIPLE_PBC_DETECTED;
	pWpsSessionNode->pWpsContext->pWpsConfigDb[u2Index].u4PushButtonStatus = WPS_PBC_OVERLAP;
	goto sendNack;
    }
    if(STRLEN(pWpsSessionNode->pWpsParseAttr->au1Authenticator) > 0)
    {
	wpsBuildAuthenticator(pWpsSessionNode,pWpsEapPacketInfo, 
		(UINT2)(pWpsEapPacketInfo->u2MsgLen-4-WPS_AUTHENTICATOR_LEN),au1Hash);

	if(MEMCMP(au1Hash, pWpsSessionNode->pWpsParseAttr->au1Authenticator,
		    WPS_AUTHENTICATOR_LEN != 0))

	{
	    goto sendNack;
	}
    }
    u1WpsValidatePsk = PSK1; 
    u1ReturnType = wpsDecryptNonce(pWpsSessionNode, u1WpsValidatePsk);
    if(u1ReturnType == WPS_FAILURE)
    {
	goto sendNack;
    }

    wpsBuildKeyWrapAuth(pWpsSessionNode,pWpsSessionNode->au1snonce2,TYPE2);

    WPS_MEMCPY(pWpsSessionNode->last_msg.au1Buf, pWpsEapPacketInfo->WpsEapHdr.au1Data, 
	    pWpsEapPacketInfo->u2MsgLen);
    pWpsSessionNode->last_msg.i4used=pWpsEapPacketInfo->u2MsgLen; 

    wpsBuildMsgResp(M6,pWpsSessionNode, pWpsEapPacketInfo);
    return WPS_SUCCESS;

sendNack:
    u1MsgType = WSC_NACK;
    wpsBuildMsgResp(u1MsgType,pWpsSessionNode, pWpsEapPacketInfo);   
    return WPS_FAILURE;
}
/****************************************************************************
 *                                                                           *
 * Function     : wpsBuildCredential                                         *
 *                                                                           *
 * Description  : Build the credential for M8                                *
 *                                                                           *
 * Input        : cred : pointer to the packet to be formed                  *
 *                pWpsCredential: pointer to the datasturcture having credentials *
 *                cred_offset : offser to build the credential packet        *
 *                                                                           *
 * Output       : WPS_SUCCESS/WPS_FAILURE                                    *
 *                                                                           *
 * Returns      : WPS_SUCCESS/WPS_FAILURE                                    *
 *                                                                           *
 *****************************************************************************/
VOID wpsBuildCredential(tWpsBuf * cred, 
                         tWpsCredential * pWpsCredential,INT4 *i4CredOffset)
{
    WPS_TRC(MSG_DEBUG, "\nwpsBuildCredential\n");
    UINT1 u1Val = 1;
    INT4 i4Temp = 0;
    UINT2 i = 0;
    WPS_PUT_2BYTE_VP(cred->au1Buf, WPS_CRED_TAG, *i4CredOffset);
    i4Temp = *i4CredOffset;
    WPS_PUT_2BYTE_VP(cred->au1Buf,0, *i4CredOffset);
    WPS_TRC(MSG_DEBUG, "\nwpsBuildCredential: * Network index\n");
    WPS_PUT_2BYTE_VP(cred->au1Buf, WPS_NETWORK_INDEX_TAG, *i4CredOffset);
    WPS_PUT_2BYTE_VP(cred->au1Buf,1, *i4CredOffset);
    WPS_PUT_1BYTE_VP(cred->au1Buf, u1Val, *i4CredOffset);

    WPS_TRC(MSG_DEBUG, "\nwpsBuildCredential: SSID\n");
    WPS_PUT_2BYTE_VP(cred->au1Buf, WPS_SSID_TAG, *i4CredOffset);
    WPS_PUT_2BYTE_VP(cred->au1Buf, pWpsCredential->u2ssidLen, *i4CredOffset);
    WPS_PUT_NBYTE_PP(cred->au1Buf, pWpsCredential->u1ssid, 
				pWpsCredential->u2ssidLen, *i4CredOffset);

    WPS_TRC(MSG_DEBUG, "\nwpsBuildCredential: Authentication type\n");
    WPS_PUT_2BYTE_VP(cred->au1Buf, WPS_AUTHENTICATION_TYPE_TAG, *i4CredOffset);
    WPS_PUT_2BYTE_VP(cred->au1Buf, 2, *i4CredOffset);
    WPS_PUT_2BYTE_VP(cred->au1Buf, pWpsCredential->u2AuthType, *i4CredOffset);

    WPS_TRC(MSG_DEBUG, "\nwpsBuildCredential: Encryption type\n");
    WPS_PUT_2BYTE_VP(cred->au1Buf, WPS_ENCRYPTION_TYPE_TAG, *i4CredOffset);
    WPS_PUT_2BYTE_VP(cred->au1Buf, 2, *i4CredOffset);
    WPS_PUT_2BYTE_VP(cred->au1Buf, pWpsCredential->u2EncrType, *i4CredOffset);

    WPS_TRC(MSG_DEBUG, "\nwpsBuildCredential: Network Key\n\n");
    WPS_PUT_2BYTE_VP(cred->au1Buf, WPS_NETWORK_KEY_TAG, *i4CredOffset);
    WPS_PUT_2BYTE_VP(cred->au1Buf, pWpsCredential->u4KeyLen, *i4CredOffset);
    WPS_PUT_NBYTE_PP(cred->au1Buf, pWpsCredential->u1Key, 
				pWpsCredential->u4KeyLen, *i4CredOffset);

    WPS_TRC(MSG_DEBUG, "\nwpsBuildCredential: MAC ADRESS\n");
    WPS_PUT_2BYTE_VP(cred->au1Buf, WPS_MAC_ADDRESS_TAG, *i4CredOffset);
    WPS_PUT_2BYTE_VP(cred->au1Buf, ETH_ALEN, *i4CredOffset);
    WPS_PUT_NBYTE_PP(cred->au1Buf, pWpsCredential->au1MacAddr, ETH_ALEN, *i4CredOffset);

    WPS_PUT_2BYTE_VP(cred->au1Buf,(*i4CredOffset-4),i4Temp);
}
/****************************************************************************
 *                                                                           *
 * Function     : wpsBuildCred                                               *
 *                                                                           *
 * Description  : Build the credential container for M8                      *
 *                                                                           *
 * Input        : pWpsSessionNode: supplicant session node                   *
 *                                                                           *
 * Output       : WPS_SUCCESS/WPS_FAILURE                                    *
 *                                                                           *
 * Returns      : WPS_SUCCESS/WPS_FAILURE                                    *
 *                                                                           *
 *****************************************************************************/
VOID wpsBuildCred(tWpsSessionNode *pWpsSessionNode )
{
	WPS_TRC(MSG_DEBUG, "\nwpsBuildCred: Build credential\n");
	struct wpsbuf cred;
	UINT1 au1InitVector[WPS_INITIALIZATION_VECTOR_LEN];
	INT4 i4CredOffset = 0;
	UINT1 au1Hash[32];
	UINT1 *pTmpstr[1] = {NULL};
	INT4 i4len[1] = {0};
	UINT2 u2PadLen = 0;
	UINT2 u2BlockSize = 16;
	UINT2 i = 0;
	WPS_MEMSET(&pWpsSessionNode->cred, 0, sizeof(tWpsCredential));
	WPS_MEMCPY(pWpsSessionNode->cred.u1ssid, 
			pWpsSessionNode->pWpsContext->au1ssid, 
			STRLEN(pWpsSessionNode->pWpsContext->au1ssid));

	pWpsSessionNode->cred.u2ssidLen = STRLEN(pWpsSessionNode->pWpsContext->au1ssid);

	pWpsSessionNode->cred.u2AuthType = pWpsSessionNode->u2AuthType;

	pWpsSessionNode->cred.u2EncrType = pWpsSessionNode->u2EncrType;

	WPS_MEMCPY(pWpsSessionNode->cred.au1MacAddr, pWpsSessionNode->au1SuppMacAddr, ETH_ALEN);
	WPS_MEMCPY(pWpsSessionNode->cred.u1Key,
			pWpsSessionNode->pWpsContext->au1Psk, 
			RSNA_MAX_PASS_PHRASE_LEN);
	pWpsSessionNode->cred.u4KeyLen = RSNA_MAX_PASS_PHRASE_LEN;

	WPS_MEMSET(&cred, 0, sizeof(struct wpsbuf));

	WPS_MEMSET(cred.au1Buf, 0, 1024);

	wpsBuildCredential(&cred, &(pWpsSessionNode->cred),&i4CredOffset);

		pTmpstr[0] = cred.au1Buf;
	i4len[0] = i4CredOffset;

	WpsAlgoHmacSha2(AR_SHA256_ALGO,pTmpstr,i4len,1,
			pWpsSessionNode->au1AuthKey,WPS_AUTHKEY_LEN,au1Hash); 
		WPS_PUT_2BYTE_VP(cred.au1Buf,WPS_KEY_WRAP_AUTHENTICATOR_TAG,i4CredOffset);
		WPS_PUT_2BYTE_VP(cred.au1Buf,WPS_KEY_WRAP_AUTHENTICATOR_LEN,i4CredOffset);   
		WPS_MEMCPY(pWpsSessionNode->pWpsParseAttr->au1KeyWrapAuth,au1Hash,
				WPS_KEY_WRAP_AUTHENTICATOR_LEN);
		WPS_PUT_NBYTE_PP(cred.au1Buf,pWpsSessionNode->pWpsParseAttr->au1KeyWrapAuth,
				WPS_KEY_WRAP_AUTHENTICATOR_LEN,i4CredOffset);
		cred.i4used = i4CredOffset;

		u2PadLen = (UINT2)(u2BlockSize - (cred.i4used % u2BlockSize));
		WPS_MEMSET(cred.au1Buf + cred.i4used, u2PadLen, u2PadLen);
		cred.i4used += u2PadLen;

		WPS_MEMSET(au1InitVector,0,WPS_INITIALIZATION_VECTOR_LEN);
		WpsUtilGetRandom(au1InitVector,WPS_INITIALIZATION_VECTOR_LEN);    
		WPS_MEMCPY(pWpsSessionNode->pWpsParseAttr->au1IV,
				au1InitVector,
				WPS_INITIALIZATION_VECTOR_LEN);

		if(AesArCbcEncrypt(cred.au1Buf,cred.i4used,AES_128_KEY_LENGTH,
				pWpsSessionNode->au1Keywrapkey,au1InitVector))        
		{
			pWpsSessionNode->pWpsParseAttr->M8Cred.i4used = cred.i4used; 
			WPS_MEMCPY(pWpsSessionNode->pWpsParseAttr->M8Cred.au1Buf,cred.au1Buf,cred.i4used);
		}
}
/****************************************************************************
 *                                                                           *
 * Function     : wpsDecryptNonce                                            *
 *                                                                           *
 * Description  : Decrypt the Snonce recevied in M5 and M7                   *
 *                validate the psk                                           *
 *                                                                           *
 * Input        : pWpsSessionNode: supplicant session node                      *
 *                wpsValidatePsk : psk to be validated that is psk1 or psk2  *
 *                                                                           *
 * Output       : WPS_SUCCESS/WPS_FAILURE                                    *
 *                                                                           *
 * Returns      : WPS_SUCCESS/WPS_FAILURE                                    *
 *                                                                           *
 *****************************************************************************/
UINT1 wpsDecryptNonce(tWpsSessionNode *pWpsSessionNode, UINT1 u1WpsValidatePsk)
{
    WPS_TRC(MSG_DEBUG, "\nwpsDecryptNonce\n");
    struct wpsbuf decrypted;
    UINT1 au1Hash[32] = {0};
    UINT1 au1IV[WPS_INITIALIZATION_VECTOR_LEN] = {0};
    const size_t u2BlockSize = 16;
    UINT1 *pPos = NULL;
    UINT1 u1PadVal = 0;
    UINT1 u1i = 0;
    UINT1 *pTmpStr[1] = {NULL};
    INT4 i4len[1] = {0};
    UINT1 *pTmpStr1[4] = {NULL};
    INT4 i4len1[4] = {0};
    UINT1 u1RetrunVal = WPS_SUCCESS;

    WPS_MEMSET(au1IV,0,WPS_INITIALIZATION_VECTOR_LEN);
    WPS_MEMCPY(au1IV, pWpsSessionNode->pWpsParseAttr->au1EncrSettings, 
					WPS_INITIALIZATION_VECTOR_LEN);
    WPS_MEMSET(&decrypted,0,sizeof(struct wpsbuf));
    WPS_MEMSET(decrypted.au1Buf,0,1024);
    WPS_MEMCPY(decrypted.au1Buf,
	  (pWpsSessionNode->pWpsParseAttr->au1EncrSettings + u2BlockSize),
	  (pWpsSessionNode->pWpsParseAttr->u4EncrSettingsLen - u2BlockSize));

    decrypted.i4used = (INT4)(pWpsSessionNode->pWpsParseAttr->u4EncrSettingsLen - u2BlockSize);

    u1RetrunVal = AesArCbcDecrypt(decrypted.au1Buf, decrypted.i4used, 
		AES_128_KEY_LENGTH ,pWpsSessionNode->au1Keywrapkey, au1IV);
    if (u1RetrunVal == AES_FAILURE)
    {
        return WPS_FAILURE;
    }
    pPos = decrypted.au1Buf + decrypted.i4used - 1;
    u1PadVal = *pPos;

    if(u1PadVal > decrypted.i4used)
    {
	return WPS_FAILURE;
    }

    for(u1i = 0; u1i < u1PadVal; u1i++)
    {
	if(*pPos-- != u1PadVal)
	{
	    return WPS_FAILURE;
	}
    }

    decrypted.i4used -= u1PadVal;

    pTmpStr[0] = decrypted.au1Buf;
    i4len[0] = decrypted.i4used - 4 - WPS_KEY_WRAP_AUTHENTICATOR_LEN;

    WpsAlgoHmacSha2(AR_SHA256_ALGO,pTmpStr,i4len,1,
		pWpsSessionNode->au1AuthKey,WPS_AUTHKEY_LEN,au1Hash); 

    if(MEMCMP(au1Hash,decrypted.au1Buf + 4 + 20,WPS_KEY_WRAP_AUTHENTICATOR_LEN))
    {
	return WPS_FAILURE;
    }
    pTmpStr1[0] = decrypted.au1Buf + 4;
    i4len1[0] = WPS_SECRET_NONCE_LEN;
    if (u1WpsValidatePsk == 1)
    {
	pTmpStr1[1] = pWpsSessionNode->au1Psk1;
    }
    else
    {
	pTmpStr1[1] = pWpsSessionNode->au1Psk2;
    }
    i4len1[1] = WPS_PSK_LEN;
    pTmpStr1[2] = pWpsSessionNode->au1DhPubKeyE;
    i4len1[2] = WPS_PUBLIC_KEY_LEN;
    pTmpStr1[3] = pWpsSessionNode->au1DhPubkeyR;
    i4len1[3] = WPS_PUBLIC_KEY_LEN;
    WpsAlgoHmacSha2(AR_SHA256_ALGO,pTmpStr1,i4len1,4,
		pWpsSessionNode->au1AuthKey,WPS_AUTHKEY_LEN,au1Hash); 
    if (u1WpsValidatePsk == PSK1)
    {
	if(MEMCMP(pWpsSessionNode->au1PeerHash1,au1Hash,WPS_HASH_LEN)!= 0)
	{
	    return WPS_FAILURE;
	}
    }
    else
    {
	if(MEMCMP(pWpsSessionNode->au1PeerHash2,au1Hash,WPS_HASH_LEN)!= 0)
	{
	    return WPS_FAILURE;
	}
    }
    return WPS_SUCCESS;
}

/****************************************************************************
 *                                                                           *
 * Function     : WpsHandleM7                                                *
 *                                                                           *
 * Description  : Function invoked to process the received M7 message        *
 *                                                                           *
 * Input        : pWpsEapPacketInfo : Received EAP packet                    *
 *                pWpsSessionNode: supplicant's session node                    *
 *                                                                           *
 * Output       : WPS_SUCCESS/WPS_FAILURE                                    *
 *                                                                           *
 * Returns      : WPS_SUCCESS/WPS_FAILURE                                    *
 *                                                                           *
 *****************************************************************************/
UINT1 WpsHandleM7(tWpsSessionNode *pWpsSessionNode, 
                  tWpsEapPacketInfo *pWpsEapPacketInfo)
{
    WPS_TRC(MSG_DEBUG, "\nWpsHandleM7\n");
    UINT1 u1WpsValidatePsk = 0;
    UINT1 u1ReturnType = 0;
    UINT1 u1MsgType = 0;
    UINT1 au1Hash[32] = {0};
    UINT1 *pTmpStr[2] = {NULL,NULL};
    INT4 i4len[2] = {0};
    WPS_TRC(MSG_DEBUG, "\nWpsHandleM7: Received M7\n");
    if (pWpsSessionNode->pWpsParseAttr->u1MsgType != M7) {
	WPS_TRC_ARG1(MSG_DEBUG, "WpsHandleM7: Unexpected state (%d) for "
		"receiving M7", pWpsSessionNode->e1state);
	pWpsSessionNode->e1state = WSC_NACK;
	goto sendNack;
    }
    if(pWpsSessionNode->e1state != RECV_M7)
    {
	WPS_TRC (WPS_ALL_FAILURE_TRC,"\nWpsHandleM7:must be a duplicate ignore\n");
	return WPS_FAILURE;
    }	

    if(MEMCMP(pWpsSessionNode->au1NonceR,
		pWpsSessionNode->pWpsParseAttr->au1RegistrarNonce,
		WPS_REGISTRAR_NONCE_LEN))
    {
	goto sendNack;
    }  

    if(STRLEN(pWpsSessionNode->pWpsParseAttr->au1Authenticator) > 0)
    {
	pTmpStr[0] = pWpsSessionNode->last_msg.au1Buf;
	i4len[0] = pWpsSessionNode->last_msg.i4used;
	pTmpStr[1] = pWpsEapPacketInfo->WpsEapHdr.au1Data;
	i4len[1] = (pWpsEapPacketInfo->u2MsgLen - 4 - WPS_AUTHENTICATOR_LEN);
	WpsAlgoHmacSha2(AR_SHA256_ALGO,pTmpStr,i4len,2,
		pWpsSessionNode->au1AuthKey,WPS_AUTHKEY_LEN,au1Hash); 
	if(MEMCMP(au1Hash, pWpsSessionNode->pWpsParseAttr->au1Authenticator, 
		    WPS_AUTHENTICATOR_LEN)!=0 )
	{
	    goto sendNack;
	}
    }
    u1WpsValidatePsk = PSK2;
    u1ReturnType = wpsDecryptNonce(pWpsSessionNode, u1WpsValidatePsk);

    if(u1ReturnType == WPS_FAILURE)
    {
	goto sendNack;
    }
    if(pWpsSessionNode->u2AuthType & WPS_AUTH_WPA2PSK)
    {
	pWpsSessionNode->u2AuthType = WPS_AUTH_WPA2PSK;
    }
    else if (pWpsSessionNode->u2AuthType & WPS_AUTH_OPEN)
    {
	pWpsSessionNode->u2AuthType = WPS_AUTH_OPEN;
    }
    else
    {
	WPS_TRC(MSG_DEBUG, "\nWpsHandleM7: unsupported auth type\n");
	goto sendNack;
    }
    if(pWpsSessionNode->u2AuthType == WPS_AUTH_WPA2PSK)
    {
	if(pWpsSessionNode->u2EncrType & WPS_ENCR_AES)
	    pWpsSessionNode->u2EncrType = WPS_ENCR_AES;
	else if(pWpsSessionNode->u2EncrType & WPS_ENCR_TKIP)
	    pWpsSessionNode->u2EncrType = WPS_ENCR_TKIP;
	else
	{
	    WPS_TRC(MSG_DEBUG, "\nWpsHandleM7: No appropriate encryption type for WPA2\n");
	    goto sendNack;
	}
    }
    else
    {
	if(pWpsSessionNode->u2EncrType & WPS_ENCR_NONE)
	    pWpsSessionNode->u2EncrType = WPS_ENCR_NONE;
	else if(pWpsSessionNode->u2EncrType & WPS_ENCR_WEP)
	    pWpsSessionNode->u2EncrType = WPS_ENCR_WEP;
	else
	{
	    WPS_TRC(MSG_DEBUG, "\nWpsHandleM7: No appropriate encryption type for non- WPA/WPA2 mode\n");
	    goto sendNack;
	}
    }
    WPS_MEMCPY(pWpsSessionNode->last_msg.au1Buf, pWpsEapPacketInfo->WpsEapHdr.au1Data, 
	    pWpsEapPacketInfo->u2MsgLen);
    pWpsSessionNode->last_msg.i4used=pWpsEapPacketInfo->u2MsgLen; 
    wpsBuildCred(pWpsSessionNode);
    wpsBuildMsgResp(M8,pWpsSessionNode, pWpsEapPacketInfo);
    return WPS_SUCCESS;
sendNack:
    u1MsgType = WSC_NACK;
    wpsBuildMsgResp(u1MsgType,pWpsSessionNode, pWpsEapPacketInfo);   
    return WPS_FAILURE;
}

/****************************************************************************
 *                                                                           *
 * Function     : wpsBuildM2DIEs                                              *
 *                                                                           *
 * Description  : Function invoked to build the M2 response messages for     *
 * 		  the registration protocol                                          *
 *                                                                           *
 * Input        : pWpsEapPacketInfo : Received EAP packet                    *
 *                pWpsSessionNode: supplicant's session node                 *
 *                                                                           *
 * Output       : NONE                                                       *
 *                                                                           *
 * Returns      : NONE                                                       *
 *                                                                           *
 *****************************************************************************/
VOID wpsBuildM2DIEs(tWpsSessionNode *pWpsSessionNode,tWpsEapPacketInfo *pWpsEapPacketInfo,
                   INT4 *i4offset)
{
    UINT1 i = 0;
    WPS_PUT_2BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data, WPS_REGISTRAR_NONCE_TAG, *i4offset);
    WPS_PUT_2BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data, WPS_REGISTRAR_NONCE_LEN, *i4offset);
    WPS_PUT_NBYTE_PP(pWpsEapPacketInfo->WpsEapHdr.au1Data, pWpsSessionNode->au1NonceR,
            WPS_REGISTRAR_NONCE_LEN, *i4offset);

    WPS_PUT_2BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data, WPS_UUID_R_TAG, *i4offset);
    WPS_PUT_2BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data, WPS_UUID_R_LEN, *i4offset);
    WPS_PUT_NBYTE_PP(pWpsEapPacketInfo->WpsEapHdr.au1Data, 
            pWpsSessionNode->au1uuidR,WPS_UUID_R_LEN, *i4offset);

    WPS_PUT_2BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data, 
            WPS_AUTHENTICATION_TYPE_FLAGS_TAG, *i4offset);
    WPS_PUT_2BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data, 
            WPS_AUTHENTICATION_TYPE_FLAGS_LEN, *i4offset);
    WPS_PUT_2BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data, 
            pWpsSessionNode->pWpsContext->u2AuthTypeFlags, *i4offset);

    WPS_PUT_2BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data, 
            WPS_ENCRYPTION_TYPE_FLAGS_TAG, *i4offset);
    WPS_PUT_2BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data, 
            WPS_ENCRYPTION_TYPE_FLAGS_LEN, *i4offset);
    WPS_PUT_2BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data, 
            pWpsSessionNode->u2EncrType, *i4offset);

    WPS_PUT_2BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data, 
            WPS_CONNECTION_TYPE_FLAGS_TAG, *i4offset);
    WPS_PUT_2BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data, 
            WPS_CONNECTION_TYPE_FLAGS_LEN, *i4offset);
    WPS_PUT_1BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data, 
            pWpsSessionNode->pWpsParseAttr->u1ConnTypeFlags, 
            *i4offset);

    WPS_PUT_2BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data, WPS_CONFIG_METHODS_TAG, *i4offset);
    WPS_PUT_2BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data, WPS_CONFIG_METHODS_LEN, *i4offset);
    WPS_PUT_2BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data,
            pWpsSessionNode->u2ConfigMethods,*i4offset);

    WPS_PUT_2BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data, WPS_MANUFACTURER_TAG, *i4offset);
    WPS_PUT_2BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data, WPS_MANUFACTURER_LEN, *i4offset);
    WPS_PUT_NBYTE_PP(pWpsEapPacketInfo->WpsEapHdr.au1Data, 
            pWpsSessionNode->pWpsParseAttr->au1Manufacturer,
            WPS_MANUFACTURER_LEN, *i4offset);

    WPS_PUT_2BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data, WPS_MODEL_NAME_TAG, *i4offset);
    WPS_PUT_2BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data, WPS_MODEL_NAME_LEN, *i4offset);
    WPS_PUT_NBYTE_PP(pWpsEapPacketInfo->WpsEapHdr.au1Data, 
            pWpsSessionNode->au1ModelName, 
            WPS_MODEL_NAME_LEN, *i4offset);

    WPS_PUT_2BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data, WPS_MODEL_NUMBER_TAG, *i4offset);
    WPS_PUT_2BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data, WPS_MODEL_NUMBER_LEN, *i4offset);
    WPS_PUT_NBYTE_PP(pWpsEapPacketInfo->WpsEapHdr.au1Data, 
            pWpsSessionNode->au1ModelNumber,
            WPS_MODEL_NUMBER_LEN, *i4offset);

    WPS_PUT_2BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data, WPS_SERIAL_NUMBER_TAG, *i4offset);
    WPS_PUT_2BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data, WPS_SERIAL_NUMBER_LEN, *i4offset);
    WPS_PUT_NBYTE_PP(pWpsEapPacketInfo->WpsEapHdr.au1Data, 
            pWpsSessionNode->au1SerialNumber,
            WPS_SERIAL_NUMBER_LEN, *i4offset);

    WPS_PUT_2BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data, WPS_PRIMARY_DEVICE_TYPE_TAG, *i4offset);
    WPS_PUT_2BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data, WPS_PRIMARY_DEVICE_TYPE_LEN, *i4offset);
    WPS_PUT_NBYTE_PP(pWpsEapPacketInfo->WpsEapHdr.au1Data, 
            pWpsSessionNode->pWpsParseAttr->au1PrimaryDevType,
            WPS_PRIMARY_DEVICE_TYPE_LEN, *i4offset);

    WPS_PUT_2BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data, WPS_DEVICE_NAME_TAG, *i4offset);
    WPS_PUT_2BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data, WPS_DEVICE_NAME_LEN, *i4offset);
    WPS_PUT_NBYTE_PP(pWpsEapPacketInfo->WpsEapHdr.au1Data, 
            pWpsSessionNode->au1DeviceName, 
            WPS_DEVICE_NAME_LEN, *i4offset);

    WPS_PUT_2BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data, WPS_RF_BANDS_TAG, *i4offset);
    WPS_PUT_2BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data, WPS_RF_BANDS_LEN, *i4offset);
    WPS_PUT_1BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data, 
            pWpsSessionNode->u1RfBands, *i4offset);

    WPS_PUT_2BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data, WPS_ASSOCIATION_STATE_TAG, *i4offset);
    WPS_PUT_2BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data, WPS_ASSOCIATION_STATE_LEN, *i4offset);
    WPS_PUT_2BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data,1, *i4offset);

    WPS_PUT_2BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data, WPS_CONFIGURATION_ERROR_TAG, *i4offset);
    WPS_PUT_2BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data, WPS_CONFIGURATION_ERROR_LEN, *i4offset);
    WPS_PUT_2BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data, 
            pWpsSessionNode->pWpsParseAttr->u2ConfigError, *i4offset);

    WPS_PUT_2BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data, WPS_OS_VERSION_TAG, *i4offset);
    WPS_PUT_2BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data, WPS_OS_VERSION_LEN, *i4offset);
    WPS_PUT_4BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data, 
            pWpsSessionNode->pWpsParseAttr->u4OsVersion, *i4offset);

}
/****************************************************************************
 *                                                                           *
 * Function     : wpsBuildEncSettings                                        *
 *                                                                           *
 * Description  : Function invoked to build the M4 response messages for     *
 * 		  the registration protocol                                          *
 *                                                                           *
 * Input        : pWpsEapPacketInfo : Received EAP packet                    *
 *                pWpsSessionNode: supplicant's session node                 *
 *                                                                           *
 * Output       : NONE                                                       *
 *                                                                           *
 * Returns      : NONE                                                       *
 *                                                                           *
 *****************************************************************************/
VOID wpsBuildEncSettings(tWpsSessionNode *pWpsSessionNode,tWpsEapPacketInfo *pWpsEapPacketInfo,
                   INT4 *i4offset)
{
    UINT1 i = 0;
    WPS_PUT_2BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data,WPS_ENCRYPTED_SETTINGS_TAG,*i4offset);
    WPS_PUT_2BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data,
            (WPS_INITIALIZATION_VECTOR_LEN + 
             pWpsSessionNode->pWpsParseAttr->u4EncrSettingsLen),*i4offset);
    WPS_PUT_NBYTE_PP(pWpsEapPacketInfo->WpsEapHdr.au1Data,
            pWpsSessionNode->pWpsParseAttr->au1IV,
            WPS_INITIALIZATION_VECTOR_LEN,*i4offset);
    WPS_PUT_NBYTE_PP(pWpsEapPacketInfo->WpsEapHdr.au1Data,
            pWpsSessionNode->pWpsParseAttr->au1EncrSettings,
            pWpsSessionNode->pWpsParseAttr->u4EncrSettingsLen,*i4offset);
}
/****************************************************************************
 *                                                                           *
 * Function     : wpsBuildRHash                                              *
 *                                                                           *
 * Description  : Function invoked to build the M4 response messages for     *
 * 		  the registration protocol                                          *
 *                                                                           *
 * Input        : pWpsEapPacketInfo : Received EAP packet                    *
 *                pWpsSessionNode: supplicant's session node                 *
 *                                                                           *
 * Output       : NONE                                                       *
 *                                                                           *
 * Returns      : NONE                                                       *
 *                                                                           *
 *****************************************************************************/
VOID wpsBuildRHash(tWpsSessionNode *pWpsSessionNode,tWpsEapPacketInfo *pWpsEapPacketInfo,
                   INT4 *i4offset)
{
    UINT1 i = 0;
    WPS_PUT_2BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data, WPS_R_HASH1_TAG, *i4offset);
    WPS_PUT_2BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data, WPS_R_HASH1_LEN, *i4offset);
    WPS_PUT_NBYTE_PP(pWpsEapPacketInfo->WpsEapHdr.au1Data, 
            pWpsSessionNode->pWpsParseAttr->au1Rhash1,
            WPS_R_HASH1_LEN, *i4offset);

    WPS_PUT_2BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data, WPS_R_HASH2_TAG, *i4offset);
    WPS_PUT_2BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data, WPS_R_HASH2_LEN, *i4offset);
    WPS_PUT_NBYTE_PP(pWpsEapPacketInfo->WpsEapHdr.au1Data, 
            pWpsSessionNode->pWpsParseAttr->au1Rhash2,
            WPS_R_HASH2_LEN, *i4offset);

}
/****************************************************************************
 *                                                                           *
 * Function     : wpsBuildM2IEs                                              *
 *                                                                           *
 * Description  : Function invoked to build the M2 response messages for     *
 * 		  the registration protocol                                          *
 *                                                                           *
 * Input        : pWpsEapPacketInfo : Received EAP packet                    *
 *                pWpsSessionNode: supplicant's session node                 *
 *                                                                           *
 * Output       : NONE                                                       *
 *                                                                           *
 * Returns      : NONE                                                       *
 *                                                                           *
 *****************************************************************************/
VOID wpsBuildM2IEs(tWpsSessionNode *pWpsSessionNode,tWpsEapPacketInfo *pWpsEapPacketInfo,
                   INT4 *i4offset)
{
    UINT1 i = 0;
    WPS_PUT_2BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data, WPS_PUBLIC_KEY_TAG, *i4offset);
    WPS_PUT_2BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data, WPS_PUBLIC_KEY_LEN, *i4offset);
    WPS_PUT_NBYTE_PP(pWpsEapPacketInfo->WpsEapHdr.au1Data, pWpsSessionNode->au1DhPubkeyR, 
            WPS_PUBLIC_KEY_LEN, *i4offset);

    WPS_PUT_2BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data, 
            WPS_DEVICE_PASSWORD_ID_TAG, *i4offset);
    WPS_PUT_2BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data, 
            WPS_DEVICE_PASSWORD_ID_LEN, *i4offset);
    WPS_PUT_2BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data, 
            pWpsSessionNode->pWpsParseAttr->u2DevPasswordId, *i4offset);

}
/****************************************************************************
 *                                                                           *
 * Function     : wpsBuildMsgResp                                            *
 *                                                                           *
 * Description  : Function invoked to build the response messages for        *
 * 		  the registration protocol                                  *
 *                                                                           *
 * Input        : pWpsEapPacketInfo : Received EAP packet                    *
 *                pWpsSessionNode: supplicant's session node                    *
 *                MsgType: M2/M4/M6/M8 or nack msg                           *
 *                                                                           *
 * Output       : WPS_SUCCESS/WPS_FAILURE                                    *
 *                                                                           *
 * Returns      : WPS_SUCCESS/WPS_FAILURE                                    *
 *                                                                           *
 *****************************************************************************/
VOID wpsBuildMsgResp(UINT1 MsgType,tWpsSessionNode *pWpsSessionNode,
                                           tWpsEapPacketInfo *pWpsEapPacketInfo)
{
    WPS_TRC_ARG1(MSG_DEBUG, "\nwpsBuildMsgResp : msgType = %d\n", MsgType);
    INT4 i4offset = 0;
    INT4 i4temp = 0;
    UINT1 *pTmpStr[2] = {NULL};
    INT4 i4len[2] = {0};
    tWpsStat * pWpsStat= NULL;
    UINT2 i = 0;
    UINT2 u2Index = 0;

    memset(pWpsEapPacketInfo,0,sizeof(tWpsEapPacketInfo ));
    WpsBuildEapHdr(&(pWpsEapPacketInfo->WpsEapHdr),WpsMSG);

    WPS_PUT_2BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data, WPS_VERSION_TAG, i4offset);
    WPS_PUT_2BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data, WPS_VERSION_LEN, i4offset);
    WPS_PUT_1BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data, WPS_VERSION_OLD, i4offset);

    WPS_PUT_2BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data, WPS_MESSAGE_TYPE_TAG, i4offset);
    WPS_PUT_2BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data, WPS_MESSAGE_TYPE_LEN, i4offset);
    WPS_PUT_1BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data, MsgType, i4offset);

    WPS_PUT_2BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data, WPS_ENROLLEE_NONCE_TAG, i4offset);
    WPS_PUT_2BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data, WPS_ENROLLEE_NONCE_LEN, i4offset);
    WPS_PUT_NBYTE_PP(pWpsEapPacketInfo->WpsEapHdr.au1Data, pWpsSessionNode->au1NonceE, 
	    WPS_ENROLLEE_NONCE_LEN,i4offset);

    switch(MsgType)
    {
	case M2:
	    wpsBuildM2IEs(pWpsSessionNode,pWpsEapPacketInfo,&i4offset);
	    wpsBuildM2DIEs(pWpsSessionNode,pWpsEapPacketInfo,&i4offset);
            pWpsSessionNode->e1state = RECV_M3;
	    break;

	case M2D:
	    wpsBuildM2DIEs(pWpsSessionNode,pWpsEapPacketInfo,&i4offset);
            pWpsSessionNode->e1state = RECV_DONE;
	    break;
	case M4:
	    wpsBuildRHash(pWpsSessionNode,pWpsEapPacketInfo,&i4offset);
	    wpsBuildEncSettings(pWpsSessionNode,pWpsEapPacketInfo,&i4offset);
            pWpsSessionNode->e1state = RECV_M5;
	    break;

	case M6 :
	    wpsBuildEncSettings(pWpsSessionNode,pWpsEapPacketInfo,&i4offset);
            pWpsSessionNode->e1state = RECV_M7;
	    break;
	case M8 :
	    WPS_PUT_2BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data,WPS_ENCRYPTED_SETTINGS_TAG,i4offset);
	    WPS_PUT_2BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data,(WPS_INITIALIZATION_VECTOR_LEN + 
			pWpsSessionNode->pWpsParseAttr->M8Cred.i4used),i4offset);
	    WPS_PUT_NBYTE_PP(pWpsEapPacketInfo->WpsEapHdr.au1Data,pWpsSessionNode->pWpsParseAttr->au1IV,
		    WPS_INITIALIZATION_VECTOR_LEN,i4offset);
	    WPS_PUT_NBYTE_PP(pWpsEapPacketInfo->WpsEapHdr.au1Data,pWpsSessionNode->pWpsParseAttr->M8Cred.au1Buf,
		    pWpsSessionNode->pWpsParseAttr->M8Cred.i4used,
		    i4offset);
            pWpsSessionNode->e1state = RECV_DONE;
	    break;
	case WSC_NACK:
	    for (u2Index = 0; u2Index < NUM_OF_AP_SUPPORTED; u2Index++)
	    {
		if (pWpsSessionNode->u2WtpInternalId == 
			pWpsSessionNode->pWpsContext->pWpsConfigDb[u2Index].u2WtpInternalId)
		{
		    break;
		}
	    }
	    if (u2Index != NUM_OF_AP_SUPPORTED)
	    {
		if (pWpsSessionNode->pWpsContext->pWpsConfigDb[u2Index].u4PushButtonStatus 
			!= WPS_PBC_OVERLAP)
		{
		    pWpsSessionNode->pWpsContext->pWpsConfigDb[u2Index].u4PushButtonStatus = WPS_PBC_ERROR;
		}
		pWpsStat = WpsUtilGetStationFromStatsTable( pWpsSessionNode->pWpsContext->u2Port,
			pWpsSessionNode->au1SuppMacAddr);
		if (pWpsStat != NULL)
		{
		    pWpsStat->e1PbcStatus = 
                       pWpsSessionNode->pWpsContext->pWpsConfigDb[u2Index].u4PushButtonStatus;
		    pWpsStat->e1Status = WPS_STATUS_FAILURE;
		}
	    }
	    WPS_PUT_2BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data, WPS_REGISTRAR_NONCE_TAG, i4offset);
	    WPS_PUT_2BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data, WPS_REGISTRAR_NONCE_LEN, i4offset);
	    WPS_PUT_NBYTE_PP(pWpsEapPacketInfo->WpsEapHdr.au1Data, 
		    pWpsSessionNode->pWpsParseAttr->au1RegistrarNonce, 
		    WPS_REGISTRAR_NONCE_LEN, i4offset);

	    WPS_PUT_2BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data, WPS_CONFIGURATION_ERROR_TAG, i4offset);
	    WPS_PUT_2BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data, WPS_CONFIGURATION_ERROR_LEN, i4offset);
	    WPS_PUT_2BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data, 
		    pWpsSessionNode->pWpsParseAttr->u2ConfigError, i4offset);
	    break;
	default:
	    WPS_TRC (WPS_CONTROL_PATH_TRC | WPS_ALL_FAILURE_TRC,
		    " wpsBuildMsgResp: invalid msg type\n");
    }

    WPS_PUT_2BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data, WPS_VENDOR_EXTENSION_TAG, i4offset);
    UINT1 src_vendorID[3]={0x00,0x37,0x2A};
    i4temp=i4offset;
    WPS_PUT_2BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data, 0, i4offset);
    WPS_PUT_NBYTE_PP(pWpsEapPacketInfo->WpsEapHdr.au1Data,src_vendorID,3,i4offset);
    WPS_PUT_1BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data, WFA_ELEM_VERSION2, i4offset);
    WPS_PUT_1BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data, 1, i4offset);
    WPS_PUT_1BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data, WPS_VERSION2, i4offset);
    WPS_PUT_2BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data,(i4offset-i4temp-2),i4temp);

    if ((MsgType != M2D) && (MsgType != WSC_NACK))
    {
	pTmpStr[0] = pWpsSessionNode->last_msg.au1Buf;
	i4len[0] = pWpsSessionNode->last_msg.i4used;
	pTmpStr[1] = pWpsEapPacketInfo->WpsEapHdr.au1Data;
	i4len[1] = i4offset;


	WpsAlgoHmacSha2(AR_SHA256_ALGO,pTmpStr,i4len,2,
		pWpsSessionNode->au1AuthKey,
		WPS_AUTHKEY_LEN,
		pWpsSessionNode->pWpsParseAttr->au1Authenticator);

	WPS_PUT_2BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data, WPS_AUTHENTICATOR_TAG, i4offset);
	WPS_PUT_2BYTE_VP(pWpsEapPacketInfo->WpsEapHdr.au1Data, WPS_AUTHENTICATOR_LEN, i4offset);
	WPS_PUT_NBYTE_PP(pWpsEapPacketInfo->WpsEapHdr.au1Data, 
		pWpsSessionNode->pWpsParseAttr->au1Authenticator, 
		WPS_AUTHENTICATOR_LEN, i4offset);
    }
    memset(pWpsSessionNode->last_msg.au1Buf,0,1024);
    pWpsSessionNode->last_msg.i4used=0;
    WPS_MEMCPY(pWpsSessionNode->last_msg.au1Buf,pWpsEapPacketInfo->WpsEapHdr.au1Data,i4offset);
    pWpsSessionNode->last_msg.i4used=i4offset;
    pWpsEapPacketInfo->u2MsgLen = (UINT2)i4offset;
    pWpsEapPacketInfo->WpsEapHdr.u2Length = (UINT2)(i4offset);
    pWpsEapPacketInfo->WpsEapHdr.u2Length += (UINT2)WPS_EAP_HEADER_SIZE;
}
/****************************************************************************
 *                                                                           *
 * Function     : wpsBuildAuthenticator                                      *
 *                                                                           *
 * Description  : Function invoked to build to build the authenticator       *
 *                                                                           *
 * Input        : pWpsEapPacketInfo : Received EAP packet                    *
 *                pWpsSessionNode: supplicant's session node                    *
 *                hash : output of the hash funcion                          *
 *                                                                           *
 * Output       :                                                            *
 *                                                                           *
 * Returns      :                                                            *
 *                                                                           *
 *****************************************************************************/
VOID wpsBuildAuthenticator(tWpsSessionNode  *pWpsSessionNode, 
                           tWpsEapPacketInfo *pWpsEapPacketInfo,
                           INT4 i4offset, 
                           UINT1 *au1Hash)
{
    WPS_TRC(MSG_DEBUG, "\nwpsBuildAuthenticator\n");
    UINT1 *pTmpStr[2];
    INT4 i4len[2];
    pTmpStr[0] = pWpsSessionNode->last_msg.au1Buf;
    i4len[0] = pWpsSessionNode->last_msg.i4used;
    pTmpStr[1] = pWpsEapPacketInfo->WpsEapHdr.au1Data;
    i4len[1] = i4offset;

    WpsAlgoHmacSha2(AR_SHA256_ALGO,pTmpStr,i4len,2,
		pWpsSessionNode->au1AuthKey,WPS_AUTHKEY_LEN,au1Hash); 
}
/****************************************************************************
 *                                                                           *
 * Function     : wps_parse_vendor_ext_wfa                                   *
 *                                                                           *
 * Description  : Function invoked to parse and validate the WFA ext         *
 *                                                                           *
 * Input        : pWpsEapPacketInfo : Received EAP packet                    *
 *                pWpsSessionNode: supplicant's session node                    *
 *                elen : length of the WFA ext                               *
 *                offset : offset from where the WFA ext starts              *
 *                                                                           *
 * Output       : WPS_SUCCESS/WPS_FAILURE                                    *
 *                                                                           *
 * Returns      : WPS_SUCCESS/WPS_FAILURE                                    *
 *                                                                           *
 *****************************************************************************/
UINT1 wps_parse_vendor_ext_wfa(tWpsSessionNode *pWpsSessionNode, 
                               tWpsEapPacketInfo *pWpsEapPacketInfo, 
                               UINT4 u4elen, 
                               UINT4 u4offset)
{
    WPS_TRC(MSG_DEBUG, "\nwps_parse_vendor_ext_wfa\n");
    UINT1 u1Id = 0, u1len = 0;
    UINT4 u1vendorLen = u4elen + u4offset;    
    while(u4offset < u1vendorLen)
    {
	WPS_GET_1BYTE(u1Id,pWpsEapPacketInfo->WpsEapHdr.au1Data,u4offset);
	WPS_GET_1BYTE(u1len,pWpsEapPacketInfo->WpsEapHdr.au1Data,u4offset);
	switch (u1Id) {
	    case WFA_ELEM_VERSION2:
		if (u1len != 1) {
		    WPS_TRC_ARG1(MSG_DEBUG, "\nwps_parse_vendor_ext_wfa: Invalid Version2 length "
			    "%u", u1len);
		    return WPS_FAILURE;
		}
		WPS_GET_1BYTE(pWpsSessionNode->pWpsParseAttr->u1Version2,
					pWpsEapPacketInfo->WpsEapHdr.au1Data,u4offset);
		break;
	    case WFA_ELEM_REQUEST_TO_ENROLL:
		if (u1len != 1) {
		    WPS_TRC_ARG1(MSG_DEBUG, "\nwps_parse_vendor_ext_wfa: Invalid Request to Enroll "
			    "length %u", u1len);
		    return WPS_FAILURE;
		}
		WPS_GET_1BYTE(pWpsSessionNode->pWpsParseAttr->u1RequestToEnroll,
						pWpsEapPacketInfo->WpsEapHdr.au1Data,u4offset);
		break;
	    case WFA_ELEM_SETTINGS_DELAY_TIME:
		if (u1len != 1) {
		    WPS_TRC_ARG1(MSG_DEBUG, "\nwps_parse_vendor_ext_wfa: Invalid Settings Delay "
			    "Time length %u", u1len);
		    return WPS_FAILURE;
		}
		WPS_GET_1BYTE(pWpsSessionNode->pWpsParseAttr->u1SettingsDelayTime,
                                                   pWpsEapPacketInfo->WpsEapHdr.au1Data,u4offset);
		break;
	    default:
		WPS_TRC_ARG1(MSG_MSGDUMP, "\nwps_parse_vendor_ext_wfa: Skipped unknown WFA Vendor "
			"Extension subelement %u", u1Id);
		break;
	}
    }
    return WPS_SUCCESS;
}


/****************************************************************************
 *                                                                           *
 * Function     : wps_parse_vendor_ext                                       *
 *                                                                           *
 * Description  : Function invoked to parse and validate the vendor ext      *
 *                                                                           *
 * Input        : pWpsEapPacketInfo : Received EAP packet                    *
 *                pWpsSessionNode: supplicant's session node                    *
 *                len : length of the vendor ext                             *
 *                offset : offset from where the vendor ext starts           *
 *                                                                           *
 * Output       : WPS_SUCCESS/WPS_FAILURE                                    *
 *                                                                           *
 * Returns      : WPS_SUCCESS/WPS_FAILURE                                    *
 *                                                                           *
 *****************************************************************************/
UINT1 wps_parse_vendor_ext(tWpsSessionNode *pWpsSessionNode, 
                           tWpsEapPacketInfo *pWpsEapPacketInfo, 
                           UINT4 u4len,
                           UINT4 u4offset)
{
    WPS_TRC(MSG_DEBUG, "\nwps_parse_vendor_ext\n");
    UINT2 u2VendorId;

    if (u4len < 3) {
	WPS_TRC(MSG_DEBUG, "\nwps_parse_vendor_ext: Skip invalid Vendor Extension");
	return WPS_FAILURE;
    }
    u4offset++;
    WPS_GET_2BYTE(u2VendorId,pWpsEapPacketInfo->WpsEapHdr.au1Data,u4offset);

    if (u2VendorId == WPS_VENDOR_ID_WFA)
	return wps_parse_vendor_ext_wfa(pWpsSessionNode, 
                            pWpsEapPacketInfo, u4len - 3,u4offset);


    WPS_TRC_ARG1(MSG_MSGDUMP, "\nwps_parse_vendor_ext: Unknown Vendor Extension (Vendor ID %u)",
	    u2VendorId);

    if (u4len > WPS_MAX_VENDOR_EXT_LEN) {
	WPS_TRC_ARG1(MSG_DEBUG, "\nwps_parse_vendor_ext: Too long Vendor Extension (%u)",
		u4len);
	return WPS_FAILURE;
    }

    if (pWpsSessionNode->pWpsParseAttr->u1NumVendorExt >= MAX_WPS_PARSE_VENDOR_EXT) {
	WPS_TRC_ARG1(MSG_DEBUG, "\nwps_parse_vendor_ext: Skipped Vendor Extension "
		"attribute (max %d vendor extensions)",
		MAX_WPS_PARSE_VENDOR_EXT);
	return WPS_FAILURE;
    }
    return WPS_SUCCESS;
}

/****************************************************************************
 *                                                                           *
 * Function     : wpsValidateMsg                                             *
 *                                                                           *
 * Description  : Function invoked to parse and validate the recevived pkt   *
 *                                                                           *
 * Input        : pWpsEapPacketInfo : Received EAP packet                    *
 *                pWpsSessionNode: supplicant's session node                    *
 *                                                                           *
 * Output       : WPS_SUCCESS/WPS_FAILURE                                    *
 *                                                                           *
 * Returns      : WPS_SUCCESS/WPS_FAILURE                                    *
 *                                                                           *
 *****************************************************************************/

UINT1 wpsValidateMsg(tWpsEapPacketInfo *pWpsEapPacketInfo, 
				tWpsSessionNode *pWpsSessionNode)
{
    WPS_TRC(MSG_DEBUG, "\nwpsValidateMsg\n");
    UINT2 u2wpsTag;
    UINT2 u2WpsLen;
    UINT1 *pTemp = pWpsEapPacketInfo->WpsEapHdr.au1Data;
    UINT4 u4offset = 0;
    if ((pWpsEapPacketInfo->WpsEapHdr.u1Flags & 0x01) == 0)
    {
	pWpsEapPacketInfo->u2MsgLen = (UINT2)(pWpsEapPacketInfo->WpsEapHdr.u2Length 
							- WPS_EAP_HEADER_SIZE);
    }
    else
    {
	WPS_GET_2BYTE_PV(pWpsEapPacketInfo->u2MsgLen,pWpsEapPacketInfo->WpsEapHdr.au1Data,u4offset);
    }
    while(u4offset <pWpsEapPacketInfo->u2MsgLen)
    {
	WPS_GET_2BYTE_PV(u2wpsTag,pWpsEapPacketInfo->WpsEapHdr.au1Data,u4offset);
	WPS_GET_2BYTE_PV(u2WpsLen,pWpsEapPacketInfo->WpsEapHdr.au1Data,u4offset);
	switch(u2wpsTag)
	{
	    case WPS_VERSION_TAG:
		if (u2WpsLen != 1) {
		    WPS_TRC_ARG1(MSG_DEBUG, "\nwpsValidateMsg: Invalid Version u2WpsLength %u",
			    u2WpsLen);
		    return WPS_FAILURE;
		}
		WPS_GET_NBYTE_PP(&pWpsSessionNode->pWpsParseAttr->u1Version,
							pTemp,u2WpsLen,u4offset);
		break;

	    case WPS_MESSAGE_TYPE_TAG:
		if (u2WpsLen != 1) {
		    WPS_TRC_ARG1(MSG_DEBUG, "\nwpsValidateMsg: Invalid Message Type "
			    "u2WpsLength %u",u2WpsLen);
		   return WPS_FAILURE;
		}
		WPS_GET_NBYTE_PP(&pWpsSessionNode->pWpsParseAttr->u1MsgType,
							pTemp,u2WpsLen,u4offset);
		break;

	    case WPS_ENROLLEE_NONCE_TAG:
		if(u2WpsLen != WPS_NONCE_LEN) {
		    WPS_TRC_ARG1(MSG_DEBUG, "\nwpsValidateMsg: Invalid Enrollee Nonce "
			    "u2WpsLength %u", u2WpsLen);
		    return WPS_FAILURE;
		}
		WPS_GET_NBYTE_PP(pWpsSessionNode->pWpsParseAttr->au1EnrolleeNonce,
							     pTemp,u2WpsLen,u4offset);
		break;

	    case WPS_REGISTRAR_NONCE_TAG:
		if(u2WpsLen != WPS_REGISTRAR_NONCE_LEN) {
		    WPS_TRC_ARG1(MSG_DEBUG, "\nwpsValidateMsg: Invalid REISTARAR Nonce "
			    "u2WpsLength %u", u2WpsLen);
		    return WPS_FAILURE;
		}
		WPS_GET_NBYTE_PP(pWpsSessionNode->pWpsParseAttr->au1RegistrarNonce,
								pTemp,u2WpsLen,u4offset);
		break;

	    case WPS_UUID_E_TAG:
		if (u2WpsLen != WPS_UUID_LEN){
		    WPS_TRC_ARG1(MSG_DEBUG, "\nwpsValidateMsg: Invalid UUID-E u2WpsLength %u",
			    u2WpsLen);
		    return WPS_FAILURE;
		}		

		WPS_GET_NBYTE_PP(pWpsSessionNode->pWpsParseAttr->au1uuidE,pTemp,
                                                                   u2WpsLen,u4offset);
		break;

	    case WPS_UUID_R_TAG:
		if (u2WpsLen != WPS_UUID_LEN){
		    WPS_TRC_ARG1(MSG_DEBUG, "\nwpsValidateMsg: Invalid UUID-E u2WpsLength %u",
			    u2WpsLen);
		    return WPS_FAILURE;
		}
		WPS_GET_NBYTE_PP(pWpsSessionNode->pWpsParseAttr->au1uuidR,pTemp,
								u2WpsLen,u4offset);
		break;


	    case WPS_AUTHENTICATION_TYPE_TAG:
		if (u2WpsLen != 2) {
		    WPS_TRC_ARG1(MSG_DEBUG, "\nwpsValidateMsg: Invalid Authentication "
			    "Type Flags u2WpsLength %u", u2WpsLen);
		    return WPS_FAILURE;
		}
		WPS_GET_2BYTE(pWpsSessionNode->pWpsParseAttr->u2AuthType,
								pTemp,u4offset);    
		break;		

	    case WPS_ENCRYPTION_TYPE_FLAGS_TAG:
		if (u2WpsLen != 2) {
		    WPS_TRC_ARG1(MSG_DEBUG, "\nwpsValidateMsg: Invalid Encryption Type "
			    "Flags u2WpsLength %u",u2WpsLen);
		    return WPS_FAILURE;
		}	

		WPS_GET_2BYTE_PV(pWpsSessionNode->pWpsParseAttr->u2EncrTypeFlags,
								     pTemp,u4offset);
		break;
	    case WPS_CONNECTION_TYPE_FLAGS_TAG:
		if (u2WpsLen != 1) {
		    WPS_TRC_ARG1(MSG_DEBUG, "\nwpsValidateMsg: Invalid Connection Type "
			    "Flags u2WpsLength %u", u2WpsLen);
		    return WPS_FAILURE;
		}	
		WPS_GET_NBYTE_PP(&pWpsSessionNode->pWpsParseAttr->u1ConnTypeFlags,
								pTemp,u2WpsLen,u4offset);
		break;
	    case WPS_CONFIG_METHODS_TAG:
		if (u2WpsLen != 2) {
		    WPS_TRC_ARG1(MSG_DEBUG, "\nwpsValidateMsg: Invalid Config Methods "
			    "u2WpsLength %u", u2WpsLen);
		    return WPS_FAILURE;
		}
		WPS_GET_NBYTE_PP(&pWpsSessionNode->pWpsParseAttr->au1ConfigMethods,
								pTemp,u2WpsLen,u4offset);
		break;
	    case WPS_SELECTED_REGISTRAR_CONFIG_METHODS_TAG:
		if (u2WpsLen != 2) {
		    WPS_TRC_ARG1(MSG_DEBUG, "\nwpsValidateMsg: Invalid Selected "
			    "Registrar Config Methods u2WpsLength %u", u2WpsLen);
		    return WPS_FAILURE;
		}
		WPS_GET_NBYTE_PP(&pWpsSessionNode->pWpsParseAttr->au1SelRegConfigMethods,
								pTemp,u2WpsLen,u4offset);
		break;

	    case WPS_PRIMARY_DEVICE_TYPE_TAG:
		if (u2WpsLen != WPS_DEV_TYPE_LEN) {
		    WPS_TRC_ARG1(MSG_DEBUG, "\nwpsValidateMsg: Invalid Primary Device "
			    "Type u2WpsLength %u",u2WpsLen);
		    return WPS_FAILURE;
		}	
		WPS_GET_NBYTE_PP(pWpsSessionNode->pWpsParseAttr->au1PrimaryDevType,pTemp,u2WpsLen,u4offset);
		break;

	    case WPS_RF_BANDS_TAG:
		if (u2WpsLen != 1) {
		    WPS_TRC_ARG1(MSG_DEBUG, "\nwpsValidateMsg: Invalid RF Bands u2WpsLength "
			    "%u", u2WpsLen);
		    return WPS_FAILURE;
		}
		WPS_GET_1BYTE(pWpsSessionNode->pWpsParseAttr->u1RfBands,pTemp, u4offset);
		break;


	    case WPS_AUTHENTICATION_TYPE_FLAGS_TAG:
		if (u2WpsLen != 2) {
		    WPS_TRC_ARG1(MSG_DEBUG, "\nwpsValidateMsg: Invalid WPS_AUTHENTICATION_TYPE_FLAGS_TAG "
			    "length %u", u2WpsLen);
		    return WPS_FAILURE;
		}
		WPS_GET_2BYTE(pWpsSessionNode->pWpsParseAttr->u2AuthTypeFlags,pTemp,u4offset);
		break;

	    case WPS_ASSOCIATION_STATE_TAG:
		if (u2WpsLen != 2) {
		    WPS_TRC_ARG1(MSG_DEBUG, "\nwpsValidateMsg: Invalid Association State "
			    "length %u", u2WpsLen);
		    return WPS_FAILURE;
		}
		WPS_GET_2BYTE(pWpsSessionNode->pWpsParseAttr->u2AssocState,pTemp,u4offset); 
		break;

	    case WPS_CONFIGURATION_ERROR_TAG:
		if (u2WpsLen != 2) {
		    WPS_TRC_ARG1(MSG_DEBUG, "\nwpsValidateMsg: Invalid Configuration "
			    "Error u2WpsLength %u", u2WpsLen);
		    return WPS_FAILURE;
		}	
		WPS_GET_2BYTE(pWpsSessionNode->pWpsParseAttr->u2ConfigError,pTemp,u4offset);
		break;

	    case WPS_DEVICE_PASSWORD_ID_TAG:
		if (u2WpsLen != 2) {
		    WPS_TRC_ARG1(MSG_DEBUG, "\nwpsValidateMsg: Invalid Device Password "
			    "ID u2WpsLength %u", u2WpsLen);
		    return WPS_FAILURE;
		}
		WPS_GET_2BYTE(pWpsSessionNode->pWpsParseAttr->u2DevPasswordId,pTemp,u4offset);
                if (pWpsSessionNode->pWpsParseAttr->u2DevPasswordId == DEV_PW_PUSHBUTTON)
		{
		    pWpsSessionNode->b1Pbc = 1;
		}
		break;

	    case WPS_OS_VERSION_TAG:
		if (u2WpsLen != 4) {
		    WPS_TRC_ARG1(MSG_DEBUG, "\nwpsValidateMsg: Invalid OS Version u2WpsLength "
			    "%u", u2WpsLen);
		    return WPS_FAILURE;
		}	
		WPS_GET_NBYTE_PP(&pWpsSessionNode->pWpsParseAttr->u4OsVersion,pTemp,u2WpsLen,u4offset);
		pWpsSessionNode->pWpsParseAttr->u4OsVersion=
                          WPS_NTOHL(pWpsSessionNode->pWpsParseAttr->u4OsVersion);
		break;

	    case WPS_WPS_STATE_TAG:
		if (u2WpsLen != 1) {
		    WPS_TRC_ARG1(MSG_DEBUG, "\nwpsValidateMsg: Invalid Wi-Fi Protected "
			    "Setup State u2WpsLength %u", u2WpsLen);
		    return WPS_FAILURE;
		}
		WPS_GET_NBYTE_PP(&pWpsSessionNode->pWpsParseAttr->u1WpsState,pTemp,u2WpsLen,u4offset);
		break;	

	    case WPS_AUTHENTICATOR_TAG:
		if (u2WpsLen != WPS_AUTHENTICATOR_LEN) {
		    WPS_TRC_ARG1(MSG_DEBUG, "\nwpsValidateMsg: Invalid Authenticator "
			    "u2WpsLength %u", u2WpsLen);
		    return WPS_FAILURE;
		}	
		WPS_GET_NBYTE_PP(pWpsSessionNode->pWpsParseAttr->au1Authenticator,
                                                           pTemp,u2WpsLen,u4offset);

		break;

	    case WPS_E_HASH1_TAG:
		if (u2WpsLen != WPS_HASH_LEN) {
		    WPS_TRC_ARG1(MSG_DEBUG, "\nwpsValidateMsg: Invalid E-Hash1 u2WpsLength %u",
			    u2WpsLen);
		    return WPS_FAILURE;
		}
		WPS_GET_NBYTE_PP(pWpsSessionNode->pWpsParseAttr->au1Ehash1,
                                                      pTemp,u2WpsLen,u4offset);
		break;

	    case WPS_E_HASH2_TAG:
		if (u2WpsLen != WPS_HASH_LEN) {
		    WPS_TRC_ARG1(MSG_DEBUG, "wpsValidateMsg: Invalid E-Hash2 u2WpsLength %u",
			    u2WpsLen);
		    return WPS_FAILURE;
		}	
		WPS_GET_NBYTE_PP(pWpsSessionNode->pWpsParseAttr->au1Ehash2,
                                                      pTemp,u2WpsLen,u4offset);
		break;	

	    case WPS_E_SNONCE1_TAG:
		if (u2WpsLen != WPS_SECRET_NONCE_LEN) {
		    WPS_TRC_ARG1(MSG_DEBUG, "wpsValidateMsg: Invalid E-SNonce1 u2WpsLength "
			    "%u", u2WpsLen);
		    return WPS_FAILURE;
		}	
		WPS_GET_NBYTE_PP(pWpsSessionNode->pWpsParseAttr->au1Esnonce1,
                                                            pTemp,u2WpsLen,u4offset);
		break;

	    case WPS_E_SNONCE2_TAG:
		if (u2WpsLen != WPS_SECRET_NONCE_LEN) {
		    WPS_TRC_ARG1(MSG_DEBUG, "wpsValidateMsg: Invalid E-SNonce2 u2WpsLength "
			    "%u", u2WpsLen);
		    return WPS_FAILURE;
		}
		WPS_GET_NBYTE_PP(pWpsSessionNode->pWpsParseAttr->au1Esnonce2,
                                                        pTemp,u2WpsLen,u4offset);
		break;

	    case WPS_REQUEST_TYPE_TAG:
		if (u2WpsLen != 1) {
		    WPS_TRC_ARG1(MSG_DEBUG, "wpsValidateMsg: Invalid Request Type "
			    "length %u", u2WpsLen);
		    return WPS_FAILURE;
		}
		WPS_GET_NBYTE_PP(&pWpsSessionNode->pWpsParseAttr->u1RequestType,
                                                            pTemp,u2WpsLen,u4offset);
		break;

	    case WPS_RESPONSE_TYPE_TAG:
		if (u2WpsLen != 1) {
		    WPS_TRC_ARG1(MSG_DEBUG, "wpsValidateMsg: Invalid Response Type "
			    "length %u", u2WpsLen);
		    return WPS_FAILURE;
		}
		WPS_GET_NBYTE_PP(&pWpsSessionNode->pWpsParseAttr->u1ResponseType,
                                                             pTemp,u2WpsLen,u4offset);
		break;

	    case WPS_KEY_WRAP_AUTHENTICATOR_TAG:
		if (u2WpsLen != WPS_KEY_WRAP_AUTHENTICATOR_LEN) {
		    WPS_TRC_ARG1(MSG_DEBUG, "wpsValidateMsg: Invalid WPS_KEY_WRAP_AUTHENTICATOR_LEN "
			    "%u", u2WpsLen);
		    return WPS_FAILURE;
		}
		WPS_GET_NBYTE_PP(pWpsSessionNode->pWpsParseAttr->au1KeyWrapAuth,
                                                            pTemp,u2WpsLen,u4offset);
		break;

	    case  WPS_ENCRYPTION_TYPE_TAG:
		if (u2WpsLen !=  WPS_ENCRYPTION_TYPE_LEN) {
		    WPS_TRC_ARG1(MSG_DEBUG, "wpsValidateMsg: Invalid  WPS_ENCRYPTION_TYPE_LEN "
			    "%u", u2WpsLen);
		    return WPS_FAILURE;
		}
		WPS_GET_NBYTE_PP(&pWpsSessionNode->pWpsParseAttr->u2EncrType,
                                                         pTemp,u2WpsLen,u4offset);
		break;

	    case  WPS_SSID_TAG:
		WPS_GET_NBYTE_PP(pWpsSessionNode->pWpsParseAttr->au1ssid,
                                                  pTemp,u2WpsLen,u4offset);
		pWpsSessionNode->pWpsParseAttr->u2ssidLen=u2WpsLen;
		break;

	    case  WPS_SECONDARY_DEV_TYPE_LIST_TAG:
		if (u2WpsLen !=  WPS_DEV_TYPE_LEN) {
		    WPS_TRC_ARG1(MSG_DEBUG, "wpsValidateMsg: Invalid   WPS_DEV_TYPE_LEN"
			    "%u", u2WpsLen);
		    return WPS_FAILURE;
		}
		WPS_GET_NBYTE_PP(&pWpsSessionNode->pWpsParseAttr->au1SecDevTypeList,
                                                                pTemp,u2WpsLen,u4offset);
		pWpsSessionNode->pWpsParseAttr->u2SecDevTypeListLen=u2WpsLen;
		break;

	    case WPS_AP_CHANNEL_TAG:
		if (u2WpsLen != 2) {
		    WPS_TRC_ARG1(MSG_DEBUG, "wpsValidateMsg: Invalid AP Channel "
			    "length %u", u2WpsLen);
		    return WPS_FAILURE;
		}
		WPS_GET_2BYTE(pWpsSessionNode->pWpsParseAttr->u2ApChannel,
                                                            pTemp,u4offset);
		break;

	    case WPS_MAC_ADDRESS_TAG:
		if (u2WpsLen != ETH_ALEN) {
		    WPS_TRC_ARG1(MSG_DEBUG, "wpsValidateMsg: Invalid MAC Address "
			    "u2WpsLength %u", u2WpsLen);
		    return WPS_FAILURE;
		}
		WPS_GET_NBYTE_PP(pWpsSessionNode->pWpsParseAttr->au1MacAddr,
                                                      pTemp,u2WpsLen,u4offset);
		break;

	    case WPS_MANUFACTURER_TAG:
		WPS_GET_NBYTE_PP(pWpsSessionNode->pWpsParseAttr->au1Manufacturer,
                                                          pTemp,u2WpsLen,u4offset);
		pWpsSessionNode->pWpsParseAttr->u2Manufacturer_len = u2WpsLen;
		break;

	    case WPS_MODEL_NAME_TAG:

		WPS_GET_NBYTE_PP(pWpsSessionNode->pWpsParseAttr->au1ModelName,
                                                         pTemp,u2WpsLen,u4offset);
		pWpsSessionNode->pWpsParseAttr->u2ModelNameLen = u2WpsLen;
		break;

	    case WPS_MODEL_NUMBER_TAG:

		WPS_GET_NBYTE_PP(pWpsSessionNode->pWpsParseAttr->au1ModelNumber,
                                                           pTemp,u2WpsLen,u4offset);
		pWpsSessionNode->pWpsParseAttr->u2ModelNumberLen = u2WpsLen;
		break;

	    case WPS_SERIAL_NUMBER_TAG:
		WPS_GET_NBYTE_PP(pWpsSessionNode->pWpsParseAttr->au1SerialNumber,
                                                           pTemp,u2WpsLen,u4offset);
		pWpsSessionNode->pWpsParseAttr->u2SerialNumberLen = u2WpsLen;
		break;

	    case WPS_DEVICE_NAME_TAG :
		WPS_GET_NBYTE_PP(pWpsSessionNode->pWpsParseAttr->au1DevName,
                                                      pTemp,u2WpsLen,u4offset);
		pWpsSessionNode->pWpsParseAttr->u2DevNameLen = u2WpsLen;
		break;

	    case WPS_PUBLIC_KEY_TAG :
		WPS_GET_NBYTE_PP(pWpsSessionNode->pWpsParseAttr->au1PublicKey,
                                                        pTemp,u2WpsLen,u4offset);
		pWpsSessionNode->pWpsParseAttr->u2PublicKeyLen = (UINT1)u2WpsLen;
		break;

	    case WPS_ENCRYPTED_SETTINGS_TAG:
		WPS_GET_NBYTE_PP(pWpsSessionNode->pWpsParseAttr->au1EncrSettings,
                                                            pTemp,u2WpsLen,u4offset);
		pWpsSessionNode->pWpsParseAttr->u4EncrSettingsLen = (UINT4)u2WpsLen;
		break;

	    case WPS_VENDOR_EXTENSION_TAG:
		if (wps_parse_vendor_ext(pWpsSessionNode,pWpsEapPacketInfo,u2WpsLen,
                                                         u4offset) == WPS_FAILURE)
		{
		    return WPS_FAILURE;
		}
		u4offset = u4offset + u2WpsLen;
		break;

	    default:
		WPS_TRC_ARG2(MSG_DEBUG, "wpsValidateMsg: Unsupported attribute type 0x%x "
			"u2WpsLen=%u", u2wpsTag, u2WpsLen);
		break;
	}
    }
    return WPS_SUCCESS;
}
