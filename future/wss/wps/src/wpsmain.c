/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: wpsmain.c,v 1.1.1.1 2016/09/20 10:42:41 siva Exp $
 *
 * Description: WPS File.
 *********************************************************************/

#include "wpsinc.h"
/*****************************************************************************/
/*                                                                           */
/*    Function Name       : WpsMainTask                                     */
/*                                                                           */
/*    Description         : This function will perform following task in WPS*/
/*                          Module:                                          */
/*                          o  Initialized the WPS task (Sem, queue,        */
/*                             mempool creation).                            */
/*                          o  Register WPS Mib with SNMP module            */
/*                          o  Wait for external event and call the          */
/*                             corresponding even handler routines           */
/*                             on receiving the events.                      */
/*                                                                           */
/*    Input(s)            : pi1Arg -  Pointer to input arguments             */
/*                                                                           */
/*    Output(s)           : sends success/failure to LRmain (caller)         */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID WpsTaskMain (INT1 *pi1Arg)
{

    UINT4               u4Events = 0;

    UNUSED_PARAM (pi1Arg);

    if (WpsInitModuleStart () != WPS_SUCCESS)
    {
        /* WPS Initialization Failed.  Return SUCCESS */
        WPS_TRC (ALL_FAILURE_TRC, 
                "WpsTaskMain: WpsInitModuleStart failed!\n");
        WPS_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    /* Get tht Task Id of the WPS Module */
    if (WPS_GET_TASK_ID (SELF, WPS_TASK_NAME, 
                &(WPS_TASK_ID)) != OSIX_SUCCESS)
    {
        WPS_TRC (ALL_FAILURE_TRC, "WpsTaskMain: Get Task ID Failed!\n");
        WPS_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }


    if (OsixQueCrt ((UINT1 *) WPS_TASK_QUEUE_NAME, OSIX_MAX_Q_MSG_LEN, 
                WPS_TASK_QUEUE_DEPTH, 
                &(WPS_TASK_QUEUE_ID)) == OSIX_FAILURE)
    {
        WPS_TRC (ALL_FAILURE_TRC, "WpsTaskMain: Osix Queue Crt Failed!\n");
        WPS_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
    RegisterFSWPS ();

    WPS_INIT_COMPLETE (OSIX_SUCCESS);

    while (1)
    {
        if (OsixEvtRecv (WPS_TASK_ID, WPS_ALL_EVENTS, 
                    OSIX_WAIT, &u4Events) == OSIX_SUCCESS)
        {
            if ((u4Events & WPS_QUEUE_EVENT) != 0)
            {
                WpsInterfaceHandleQueueEvent ();
            }
            if ((u4Events & WPS_TIMER_EXP_EVENT) != 0)
            {
		WpsTmrProcessTimer ();
            }
        }
    }
}

/*****************************************************************************/
/* Function Name      : WpsEnQFrameToWpsTask                               */
/*                                                                           */
/* Description        : This enqueues a received frame to Wps Interface     */
/*                      Queue and sends an event WPS_QUEUE_EVENT to the     */
/*                      Interface task.                                      */
/*                                                                           */
/* Input(s)           : CRU Buffer, PortIndex, Frame type and protocol       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID WpsEnQFrameToWpsTask (tWssWpsNotifyParams* pMsg)
{
    if (OsixQueSend (WPS_TASK_QUEUE_ID, (UINT1 *) &pMsg, 
                OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        WPS_TRC (ALL_FAILURE_TRC, "WpsEnQFrameToWpsTask: "
                "Osix Queue Send Failed!\n");
        return;
    }

    if (OsixEvtSend (WPS_TASK_ID, WPS_QUEUE_EVENT) == OSIX_FAILURE)
    {
        WPS_TRC (ALL_FAILURE_TRC, "WpsEnQFrameToWpsTask: "
                "Osix Event Send Failed!\n");
        return;
    }

    return;
}

/*****************************************************************************/
/* Function Name      : WpsInterfaceHandleQueueEvent                        */
/*                                                                           */
/* Description        : This Initiates the Action for the Queue Event. It    */
/*                      receives the information from the buffer received    */
/*                      from the Queue and calls appropriate function        */
/*                      to handle it                                         */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID WpsInterfaceHandleQueueEvent (VOID)
{
    tWssWpsNotifyParams *pMsg = NULL;

    while (OsixQueRecv (WPS_TASK_QUEUE_ID, 
		(UINT1 *) &pMsg, 
		OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
	WPS_TRC (ALL_FAILURE_TRC, 
		" INF: Message received from Queue ... Processing...\n");

	if (pMsg == NULL)
	{
	    continue;
	}
	switch (pMsg->eWssWpsNotifyType)
	{

	    case WSSWPS_PROFILEIF_ADD:
		WpsAddPortEntry (pMsg->u4IfIndex);
		WPS_MEM_RELEASE_MEM_BLK (WPS_WSS_INFO_MEMPOOL_ID, pMsg);

		break;
	    case WSSWPS_PROFILEIF_DEL:
		WpsDeletePortEntry (pMsg->u4IfIndex);
		WPS_MEM_RELEASE_MEM_BLK (WPS_WSS_INFO_MEMPOOL_ID, pMsg);
		break;
	    case WSSWPS_REASSOC_IND:
	    case WSSWPS_ASSOC_IND:
		WpsProcAssocInd (pMsg);
		break;
	    case WSSWPS_DISSOC_IND:
		WpsProcDisAssocInd (pMsg);
		break;
	    case WSSWPS_PROBREQ_IND:
		WpsProcProbeReqInd (pMsg);
		break;
	    case WSSWPS_PBC_IND:
		WpsProcPbcInd (pMsg);
		break;
	    case PNACWPS_EAP_MSG_IND:
		WpsProcPnacReqInd (pMsg);
		break;
	    case PNACWPS_EAP_REQ_IND:
		WpsProcPnacMsgInd (pMsg);
		break;
	    default:
		WPS_TRC_ARG1 (ALL_FAILURE_TRC, 
			"WpsInterfaceHandleQueueEvent:- "
			"Invalid Message Type %d\n", 
			pMsg->eWssWpsNotifyType);
		break;
	}
	MemReleaseMemBlock (WPS_WSS_INFO_MEMPOOL_ID, (UINT1 *) pMsg);

    }
}

