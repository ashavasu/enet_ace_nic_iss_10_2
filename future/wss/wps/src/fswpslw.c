/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fswpslw.c,v 1.1.1.1 2016/09/20 10:42:41 siva Exp $
 *
 * Description: Protocol Low Level Routines
 *********************************************************************/
# include  "lr.h" 
# include  "fssnmp.h" 
# include  "fswpslw.h"
# include "wpsinc.h"

extern tRsnaGlobals gRsnaGlobals;

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsWPSTraceOption
Input       :  The Indices

The Object 
retValFsWPSTraceOption
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1 nmhGetFsWPSTraceOption(INT4 *pi4RetValFsWPSTraceOption)
{
     *pi4RetValFsWPSTraceOption = (INT4) gWpsGlobals.u4WpsTrcOpt;
    return SNMP_SUCCESS;
}
/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetFsWPSTraceOption
Input       :  The Indices

The Object 
setValFsWPSTraceOption
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1 nmhSetFsWPSTraceOption(INT4 i4SetValFsWPSTraceOption)
{
    gWpsGlobals.u4WpsTrcOpt = (UINT4) i4SetValFsWPSTraceOption;
    return SNMP_SUCCESS;
}
/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2FsWPSTraceOption
Input       :  The Indices

The Object 
testValFsWPSTraceOption
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1 nmhTestv2FsWPSTraceOption(UINT4 *pu4ErrorCode, 
                               INT4 i4TestValFsWPSTraceOption)
{
    if (i4TestValFsWPSTraceOption < 0) 
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}
/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2FsWPSTraceOption
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1 nmhDepv2FsWPSTraceOption(UINT4 *pu4ErrorCode, 
                              tSnmpIndexList *pSnmpIndexList, 
                              tSNMP_VAR_BIND *pSnmpVarBind)
{
    UNUSED_PARAM(pu4ErrorCode);
    UNUSED_PARAM(pSnmpIndexList);
    UNUSED_PARAM(pSnmpVarBind);
    return SNMP_SUCCESS;
}
/* LOW LEVEL Routines for Table : FsWPSTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsWPSTable
 Input       :  The Indices
                IfIndex
                CapwapBaseWtpProfileId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 nmhValidateIndexInstanceFsWPSTable(INT4 i4IfIndex, UINT4 u4CapwapBaseWtpProfileId)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsWPSTable
 Input       :  The Indices
                IfIndex
                CapwapBaseWtpProfileId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 nmhGetFirstIndexFsWPSTable(INT4 *pi4IfIndex, UINT4 *pu4CapwapBaseWtpProfileId)
{
    tWpsContext *pWpsContext = NULL;

    WpsGetFirstPortEntry (&pWpsContext);
    if (pWpsContext == NULL)
    {
        return (SNMP_FAILURE);
    }
    else
    {
        *pi4IfIndex = pWpsContext->u2Port;
    }
    UNUSED_PARAM (pu4CapwapBaseWtpProfileId);
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetNextIndexFsWPSTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
                CapwapBaseWtpProfileId
                nextCapwapBaseWtpProfileId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 nmhGetNextIndexFsWPSTable(INT4 i4IfIndex, 
                               INT4 *pi4NextIfIndex, UINT4 u4CapwapBaseWtpProfileId ,UINT4 *pu4NextCapwapBaseWtpProfileId )
{
    tWpsContext *pWpsContext = NULL;
    tWpsContext *pNextWpsContext = NULL;

    if (i4IfIndex == 0)
    {
        /* Fetch the data structure of the first index */
        WpsGetNextPortEntry (NULL, &pNextWpsContext);

        if (pNextWpsContext == NULL)
        {
            return SNMP_FAILURE;
        }
        else
        {
            *pi4NextIfIndex = pNextWpsContext->u2Port;
        }
    }
    else
    {
        /* fetch the data structure of the current index */
        WpsGetPortEntry ((UINT2) i4IfIndex, &pWpsContext);

        if (pWpsContext == NULL)
        {
            WPS_TRC (WPS_CONTROL_PATH_TRC | WPS_ALL_FAILURE_TRC,
                      "No Wps Info for the given port!! ... \n");
            return (SNMP_FAILURE);
        }

        /* pass the data structure of the current index and get 
 *          * the data structure of the next index*/
        WpsGetNextPortEntry (pWpsContext, &pNextWpsContext);

        /* if next index data structure is not NULL assign the
 *          * value for next index*/
        if (pNextWpsContext == NULL)
        {
            return SNMP_FAILURE;
        }
        else
        {
            *pi4NextIfIndex = pNextWpsContext->u2Port;
        }

    }

    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (pu4NextCapwapBaseWtpProfileId);
    return SNMP_SUCCESS;
}
/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsWPSSetStatus
Input       :  The Indices
IfIndex
CapwapBaseWtpProfileId
The Object 
retValFsWPSSetStatus
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1 nmhGetFsWPSSetStatus(INT4 i4IfIndex, UINT4 u4CapwapBaseWtpProfileId,
                          INT4 *pi4RetValFsWPSSetStatus)
{
#ifdef WPS_WANTED
    UINT2 u2WtpInternalId = 0;
    CapwapGetInternalProfildId (&u4CapwapBaseWtpProfileId, 
	    &u2WtpInternalId);
    if (WpsGetWPSEnabled((UINT4)i4IfIndex, u2WtpInternalId, pi4RetValFsWPSSetStatus) !=
	    SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
#endif

return SNMP_SUCCESS;
}
/****************************************************************************
Function    :  nmhGetFsWPSPushButtonStatus
Input       :  The Indices
IfIndex
CapwapBaseWtpProfileId
The Object 
retValFsWPSPushButtonStatus
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1 nmhGetFsWPSPushButtonStatus(INT4 i4IfIndex, UINT4 u4CapwapBaseWtpProfileId, 
                                 INT4 *pi4RetValFsWPSPushButtonStatus)
{
#ifdef WPS_WANTED
    UINT2 u2WtpInternalId = 0;
    CapwapGetInternalProfildId (&u4CapwapBaseWtpProfileId, 
	    &u2WtpInternalId);
    if (WpsGetPBCStatus(i4IfIndex,u2WtpInternalId, pi4RetValFsWPSPushButtonStatus) !=
            SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
#endif
   
return SNMP_SUCCESS;
 
}
/****************************************************************************
Function    :  nmhGetFsWPSAdditionalIE
Input       :  The Indices
IfIndex
CapwapBaseWtpProfileId
The Object 
retValFsWPSAdditionalIE
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1 nmhGetFsWPSAdditionalIE(INT4 i4IfIndex, UINT4 u4CapwapBaseWtpProfileId,
                             INT4 *pi4RetValFsWPSAdditionalIE)
{
#ifdef WPS_WANTED
    UINT2 u2WtpInternalId = 0;
    CapwapGetInternalProfildId (&u4CapwapBaseWtpProfileId, 
	    &u2WtpInternalId);
    if (WpsGetWPSAdditionalIE(i4IfIndex,u2WtpInternalId,pi4RetValFsWPSAdditionalIE) !=
            SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
#endif

return SNMP_SUCCESS;

}
/****************************************************************************
 * Function    :  nmhGetFsWPSStatus
 * Input       :  The Indices
 * IfIndex
 * CapwapBaseWtpProfileId
 *
 * The Object
 * retValFsWPSStatus
 * Output      :  The Get Low Lev Routine Take the Indices &
 * store the Value requested in the Return val.
 * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *  ****************************************************************************/
INT1 nmhGetFsWPSStatus(INT4 i4IfIndex, UINT4 u4CapwapBaseWtpProfileId,
                       INT4 *pi4RetValFsWPSStatus)
{
    UNUSED_PARAM(i4IfIndex);
    UNUSED_PARAM(pi4RetValFsWPSStatus); 
    UNUSED_PARAM(u4CapwapBaseWtpProfileId);
    return WPS_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsWPSSetStatus
Input       :  The Indices
IfIndex
CapwapBaseWtpProfileId
The Object 
setValFsWPSSetStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1 nmhSetFsWPSSetStatus(INT4 i4IfIndex, UINT4 u4CapwapBaseWtpProfileId,
                          INT4 i4SetValFsWPSSetStatus)
{
    tWpsSessionNode *pWpsSessionNode= NULL;
    UINT4               u4BssIfIndex = 0;
    UINT1               au1BssId[ETH_ALEN];
    INT4               i4Dot11RSNAEnabled;
    tWssWlanDB         *pWssWlanDB = NULL;
    UINT2 		u2Index = 0;
    UINT1               au1PskVal[8] = "";
    UINT4               i4RetStatus = 0;
    UINT2		u2WpsInternalIndex = 255;
    tSNMP_OCTET_STRING_TYPE tCipherType = { NULL, 0 };

    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    tWpsContext *pWpsContext = NULL;

    WpsGetPortEntry ((UINT2) i4IfIndex, &pWpsContext);

    if (pWpsContext == NULL)
    {
        WPS_TRC (WPS_CONTROL_PATH_TRC | WPS_ALL_FAILURE_TRC, 
                "No Wps Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry != NULL)
    {
	RsnaGetDot11RSNAEnabled (i4IfIndex,&i4Dot11RSNAEnabled);
	if (i4Dot11RSNAEnabled == RSNA_DISABLED)
	{
            pWpsContext->u2ApAuthType |= WPS_AUTH_WPA2PSK;
            
            i4RetStatus = RsnaCliSetSecurity (0, RSNA_ENABLED, i4IfIndex);
            i4RetStatus = RsnaCliSetAuthSuiteStatus (0, RSNA_AUTHSUITE_PSK,    /*psk/802.1x */
                                                     RSNA_ENABLED);    /*enable/disable */
            i4RetStatus = RsnaCliSetGroupwiseCipher (0,
                                                     RSNA_CCMP, i4IfIndex);
            i4RetStatus =
                RsnaCliSetPairwiseCipherStatus (0, RSNA_CCMP,
                                                RSNA_ENABLED, i4IfIndex);
            WpsUtilGetRandom(au1PskVal, 8); 
            i4RetStatus = RsnaCliSetAkmPsk (0,
                                            RSNA_AKM_PSK_SETKEY_ASCII,
                                            au1PskVal, i4IfIndex);
	}
	else
	{
		pWpsContext->u2ApAuthType |= WPS_AUTH_WPA2PSK;

		if (pRsnaPaePortEntry->aRsnaAkmDB[RSNA_AUTHSUITE_PSK - 1].bAkmSuiteEnabled 
                                                        != OSIX_TRUE)
		{
			WPS_TRC (WPS_CONTROL_PATH_TRC | WPS_ALL_FAILURE_TRC, 
					"RSNA WPA2 is not enabled!! ... \n");
			return (SNMP_FAILURE);
		}
		if (!((pRsnaPaePortEntry->aRsnaPwCipherDB[RSNA_TKIP - 1]
                        .bPairwiseCipherEnabled == OSIX_TRUE) ||
                	(pRsnaPaePortEntry->aRsnaPwCipherDB[RSNA_CCMP - 1]
                        .bPairwiseCipherEnabled == OSIX_TRUE)))
		{
			WPS_TRC (WPS_CONTROL_PATH_TRC | WPS_ALL_FAILURE_TRC, 
					"RSNA TKIP/CCMP is not enabled!! ... \n");
			return (SNMP_FAILURE);
		}
	}
    }
    else
    {
	WPS_TRC (WPS_CONTROL_PATH_TRC | WPS_ALL_FAILURE_TRC, 
		"RSNA is not Supported!! ... \n");
	return (SNMP_FAILURE);

    }
    pWssWlanDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
    WPS_MEMSET (pWssWlanDB, 0, sizeof (tWssWlanDB));
    pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex = (UINT4)i4IfIndex;
    pWssWlanDB->WssWlanIsPresentDB.bDesiredSsid = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY, pWssWlanDB))
    {
	WPS_TRC (WPS_CONTROL_PATH_TRC | WPS_ALL_FAILURE_TRC,
		"not able to get the SSID!!..\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        return (SNMP_FAILURE);
    }
    WPS_MEMCPY(pWpsContext->au1ssid,pWssWlanDB->WssWlanAttributeDB.
                       au1DesiredSsid,WSSWLAN_SSID_NAME_LEN);
    for (u2Index = 0; u2Index < NUM_OF_AP_SUPPORTED; u2Index++)
    {
	    if (u2Index == u4CapwapBaseWtpProfileId)
	    {
		pWpsContext->pWpsConfigDb[u2Index].b1WpsEnabled =
		    (BOOL1) i4SetValFsWPSSetStatus;
		pWpsContext->pWpsConfigDb[u2Index].u4PushButtonStatus =
		    WPS_PBC_INACTIVE;
		pWpsContext->pWpsConfigDb[u2Index].u2ConfigMethods =
		    WPS_AP_CONFIG_METHOD;
		if (CapwapGetInternalProfildId (&u4CapwapBaseWtpProfileId, 
			    &pWpsContext->pWpsConfigDb[u2Index].u2WtpInternalId) == OSIX_FAILURE)
		{
		    pWpsContext->pWpsConfigDb[u2Index].u2WtpInternalId = NUM_OF_AP_SUPPORTED;
		}
            u2WpsInternalIndex = u2Index;
	}
	else
	{
            if (pWpsContext->pWpsConfigDb[u2Index].b1WpsEnabled != WPS_ENABLED)
		{
	    pWpsContext->pWpsConfigDb[u2Index].u2WtpInternalId = NUM_OF_AP_SUPPORTED;
	}
    }
    }

    /* Checks whether the WTP is binded to the wlan, if binded then UPDATE WLAN message
     *        is triggered. */

    if (WssUtilIsProfileIndexApBinded (i4IfIndex, &u4BssIfIndex, au1BssId) ==
            OSIX_TRUE)
    {
        if (i4SetValFsWPSSetStatus == WPS_DISABLED)
        {

            TMO_SLL_Scan (&(pWpsContext->wpsStationInfoList), 
                    pWpsSessionNode, tWpsSessionNode*)
            {
                if ((pWpsSessionNode != NULL) && (pWpsSessionNode->u2SessId == pWpsContext->pWpsConfigDb[u2WpsInternalIndex].u2WtpInternalId))
                {
                    wpsDeleteSession (pWpsSessionNode);
                    TMO_SLL_Delete (&(pWpsContext->wpsStationInfoList), 
                            (tTMO_SLL_NODE *) pWpsSessionNode);
                    if (WPS_MEM_RELEASE_MEM_BLK (WPS_DATA_INFO_MEMPOOL_ID, 
                                pWpsSessionNode->pWpsParseAttr) == MEM_FAILURE)
                    {
                        WPS_TRC (WPS_CONTROL_PATH_TRC | WPS_ALL_FAILURE_TRC, 
                                "wpsDeleteSession:- WPS_RELEASE_SUPP_INFO "
                                "Failed !!!! \n");
                        return (WPS_FAILURE);
                    }	
                }

                pWpsSessionNode = NULL;
            }

        }
	WssSendUpdateWlanMsgfromWps ((UINT4) i4IfIndex,
		pWpsContext->pWpsConfigDb[u2WpsInternalIndex].u2WtpInternalId, 
		WPS_ENABLED);
    }
    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);

    return SNMP_SUCCESS;
}
/****************************************************************************
Function    :  nmhSetFsWPSAdditionalIE
Input       :  The Indices
IfIndex
CapwapBaseWtpProfileId
The Object 
setValFsWPSAdditionalIE
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1 nmhSetFsWPSAdditionalIE(INT4 i4IfIndex, UINT4 u4CapwapBaseWtpProfileId ,
                             INT4 i4SetValFsWPSAdditionalIE)
{
    tWpsSessionNode *pWpsSessionNode= NULL;
    UINT4               u4BssIfIndex = 0;
    UINT1               au1BssId[ETH_ALEN];
    UINT2               u2WtpInternalId = 0;
    tWpsContext *pWpsContext = NULL;
    UINT2        u2Index = 0;

    WpsGetPortEntry ((UINT2) i4IfIndex, &pWpsContext);

    if (pWpsContext != NULL)
    {
	for (u2Index = 0; u2Index < NUM_OF_AP_SUPPORTED; u2Index++)
	{
	    if (pWpsContext->pWpsConfigDb[u2Index].u4WpsWtpProfileId == u4CapwapBaseWtpProfileId)
	    {
		pWpsContext->pWpsConfigDb[u2Index].b1WpsAdditionalIE =
		    (BOOL1) i4SetValFsWPSAdditionalIE;
		if (CapwapGetInternalProfildId (&u4CapwapBaseWtpProfileId, 
			    &pWpsContext->pWpsConfigDb[u2Index].u2WtpInternalId) == OSIX_FAILURE)
		{
		    pWpsContext->pWpsConfigDb[u2Index].u2WtpInternalId = NUM_OF_AP_SUPPORTED;
		}
		if (pWpsContext->pWpsConfigDb[u2Index].b1WpsAdditionalIE == WPS_PBC)
		{
                                
				WPS_TRC (WPS_CONTROL_PATH_TRC , 
                                        "nmhSetFsWPSAdditionalIE:- button pushed\n");
                    pWpsContext->pWpsConfigDb[u2Index].u4PushButtonStatus =
                        WPS_PBC_INPROGRESS;
                    pWpsContext->pWpsConfigDb[u2Index].u2ConfigMethods =
                        WPS_CONFIG_PUSHBUTTON;
		}
                break;
	    }
	}

	/* Checks whether the WTP is binded to the wlan, if binded then UPDATE WLAN message
	 *        is triggered. */

	if (WssUtilIsProfileIndexApBinded (i4IfIndex, &u4BssIfIndex, au1BssId) ==
		OSIX_TRUE)
	{
            if (u2Index != NUM_OF_AP_SUPPORTED)
	    {
		if (pWpsContext->pWpsConfigDb[u2Index].b1WpsEnabled ==
			WPS_DISABLED)
		{

		    TMO_SLL_Scan (&(pWpsContext->wpsStationInfoList), 
			    pWpsSessionNode, tWpsSessionNode*)
		    {
			if (pWpsSessionNode != NULL)
			{
			    wpsDeleteSession (pWpsSessionNode);
			    TMO_SLL_Delete (&(pWpsContext->wpsStationInfoList), 
				    (tTMO_SLL_NODE *) pWpsSessionNode);
			    if (WPS_MEM_RELEASE_MEM_BLK (WPS_DATA_INFO_MEMPOOL_ID, 
					pWpsSessionNode->pWpsParseAttr) == MEM_FAILURE)
			    {
				WPS_TRC (WPS_CONTROL_PATH_TRC | WPS_ALL_FAILURE_TRC, 
					"wpsDeleteSession:- WPS_RELEASE_SUPP_INFO "
					"Failed !!!! \n");
				return (WPS_FAILURE);
			    }	
			}

			pWpsSessionNode = NULL;
		    }

		}
	    }

            WssSendUpdateWlanMsgfromWps ((UINT4) i4IfIndex,
                    pWpsContext->pWpsConfigDb[u2Index].u2WtpInternalId, 
                    SET_ADDITIONAL_IE);
            WpsTmrStartTimer(&pWpsContext->WpsPbcTmr, WPS_PBC_TIMER_ID, 
                    WPS_PBC_TIME, pWpsContext,u4CapwapBaseWtpProfileId);
      }
    }
    else
    {
        WPS_TRC (WPS_CONTROL_PATH_TRC | WPS_ALL_FAILURE_TRC, 
                "No Wps Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }


    return SNMP_SUCCESS;
}
/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2FsWPSSetStatus
Input       :  The Indices
IfIndex
CapwapBaseWtpProfileId
The Object 
testValFsWPSSetStatus
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1 nmhTestv2FsWPSSetStatus(UINT4 *pu4ErrorCode, 
                             INT4 i4IfIndex, 
	             	     UINT4 u4CapwapBaseWtpProfileId ,
                             INT4 i4TestValFsWPSSetStatus)
{
    tWpsContext  *pWpsContext = NULL;

    if ((i4IfIndex < CFA_MIN_WSS_IF_INDEX) ||
            (i4IfIndex > CFA_MAX_WSS_IF_INDEX))
    {
        *pu4ErrorCode = CLI_WPS_INVALID_WLAN_ID;
        CLI_SET_ERR (CLI_WPS_INVALID_WLAN_ID);

        return SNMP_FAILURE;
    }

    WpsGetPortEntry ((UINT2) i4IfIndex, &pWpsContext);

    if (pWpsContext == NULL)
    {
        WPS_TRC (WPS_CONTROL_PATH_TRC | WPS_ALL_FAILURE_TRC, 
                "No Wps Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    if (i4TestValFsWPSSetStatus != WPS_ENABLED)
    {
        if (i4TestValFsWPSSetStatus != WPS_DISABLED)
        {
            return SNMP_FAILURE;
        }

    }
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);

    return SNMP_SUCCESS;
}
/****************************************************************************
Function    :  nmhTestv2FsWPSAdditionalIE
Input       :  The Indices
IfIndex
CapwapBaseWtpProfileId
The Object 
testValFsWPSAdditionalIE
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1 nmhTestv2FsWPSAdditionalIE(UINT4 *pu4ErrorCode, 
                                INT4 i4IfIndex, 
								UINT4 u4CapwapBaseWtpProfileId ,
                                INT4 i4TestValFsWPSAdditionalIE)
{
    tWpsContext  *pWpsContext = NULL;

    if ((i4IfIndex < CFA_MIN_WSS_IF_INDEX) ||
            (i4IfIndex > CFA_MAX_WSS_IF_INDEX))
    {
        *pu4ErrorCode = CLI_WPS_INVALID_WLAN_ID;
        CLI_SET_ERR (CLI_WPS_INVALID_WLAN_ID);

        return SNMP_FAILURE;
    }

    WpsGetPortEntry ((UINT2) i4IfIndex, &pWpsContext);

    if (pWpsContext == NULL)
    {
        WPS_TRC (WPS_CONTROL_PATH_TRC | WPS_ALL_FAILURE_TRC, 
                "No Wps Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    if (i4TestValFsWPSAdditionalIE != WPS_PBC)
    {
        if (i4TestValFsWPSAdditionalIE != WPS_PIN)
        {
            return SNMP_FAILURE;
        }

    }
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);

    return SNMP_SUCCESS;
}
/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2FsWPSTable
Input       :  The Indices
IfIndex
CapwapBaseWtpProfileId
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1 nmhDepv2FsWPSTable(UINT4 *pu4ErrorCode, 
                        tSnmpIndexList *pSnmpIndexList, 
                        tSNMP_VAR_BIND *pSnmpVarBind)
{
    UNUSED_PARAM(pu4ErrorCode);
    UNUSED_PARAM(pSnmpIndexList);
    UNUSED_PARAM(pSnmpVarBind);
    return SNMP_SUCCESS;
}
/* LOW LEVEL Routines for Table : FsWPSAuthSTAInfoTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsWPSAuthSTAInfoTable
Input       :  The Indices
               IfIndex
               FsWPSStatsIndex
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 nmhValidateIndexInstanceFsWPSAuthSTAInfoTable(INT4 i4IfIndex,UINT4 u4FsWPSStatsIndex)
{
#ifdef WPS_WANTED 
    if (WpsValidateIndexInstanceFsWPSAuthSTAInfoTable
        (i4IfIndex, u4FsWPSStatsIndex) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4FsWPSStatsIndex);
    return SNMP_SUCCESS;
#endif
}
/****************************************************************************
Function    :  nmhGetFirstIndexFsWPSAuthSTAInfoTable
Input       :  The Indices
               IfIndex
               FsWPSStatsIndex
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1 nmhGetFirstIndexFsWPSAuthSTAInfoTable(INT4 *pi4IfIndex, UINT4 *pu4FsWPSStatsIndex)
{
#ifdef WPS_WANTED 
    if (WpsGetFirstIndexFsWPSAuthSTAInfoTable(pi4IfIndex,
                                              pu4FsWPSStatsIndex) !=
        SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (pi4IfIndex);
    UNUSED_PARAM (pu4FsWPSStatsIndex);
    return SNMP_FAILURE;
#endif
}
/****************************************************************************
Function    :  nmhGetNextIndexFsWPSAuthSTAInfoTable
Input       :  The Indices
               IfIndex
               nextIfIndex
               FsWPSStatsIndex
               nextFsWPSStatsIndex
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1 nmhGetNextIndexFsWPSAuthSTAInfoTable(INT4 i4IfIndex, 
                                          INT4 *pi4NextIfIndex, UINT4 u4FsWPSStatsIndex ,UINT4 *pu4NextFsWPSStatsIndex  )
{
#ifdef WPS_WANTED 
    if (WpsGetNextIndexFsWPSAuthSTAInfoTable(i4IfIndex,
                                             pi4NextIfIndex,
                                             u4FsWPSStatsIndex,
                                             pu4NextFsWPSStatsIndex) !=
        SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pi4NextIfIndex);
    UNUSED_PARAM (u4FsWPSStatsIndex);
    UNUSED_PARAM (pu4NextFsWPSStatsIndex);
    return SNMP_FAILURE;
#endif

}
