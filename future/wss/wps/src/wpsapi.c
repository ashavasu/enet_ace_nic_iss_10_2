/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: wpsapi.c,v 1.1.1.1 2016/09/20 10:42:41 siva Exp $
 *
 * Description: Protocol Low Level Routines
 *********************************************************************/
#include  "lr.h"
#include  "fssnmp.h"
#include  "wpsinc.h"
/****************************************************************************
Function    : WpsGetWPSEnabled 
Input       :  The Indices
IfIndex

The Object 
retValWPSEnabled
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1 WpsGetWPSEnabled(UINT4 u4IfIndex, 
                      UINT2 u2WtpInternalId,
                      INT4 *pi4RetValFsWPSStatus)
{
    tWpsContext *pWpsContext = NULL;
    UINT2 u2Index = 0;
    UINT2 u2profileId;

    WpsGetPortEntry ((UINT2) u4IfIndex, &pWpsContext);

    if (pWpsContext == NULL)
    {
        WPS_TRC (WPS_CONTROL_PATH_TRC | WPS_ALL_FAILURE_TRC, 
                "No Wps Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    for (u2Index = 0; u2Index < NUM_OF_AP_SUPPORTED; u2Index++)
    {
	if (u2WtpInternalId == 
		pWpsContext->pWpsConfigDb[u2Index].u2WtpInternalId)
	{
            *pi4RetValFsWPSStatus = pWpsContext->pWpsConfigDb[u2Index].b1WpsEnabled;
	    break;
	}
    }
    if (u2Index == NUM_OF_AP_SUPPORTED)
    {
	WssWlanGetWlanProfileId(u2WtpInternalId,&u2profileId);
	    for (u2Index = 0; u2Index < NUM_OF_AP_SUPPORTED; u2Index++)
	    {                     
		if ((UINT4)u2profileId ==
			pWpsContext->pWpsConfigDb[u2Index].u4WpsWtpProfileId)
		{ 
		    *pi4RetValFsWPSStatus = pWpsContext->pWpsConfigDb[u2Index].b1WpsEnabled;
		    break;
		}
	    }
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    : WpsGetWPSAdditionalIE 
Input       :  The Indices
IfIndex

The Object
retValWPSEnabled
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1 WpsGetWPSAdditionalIE(INT4 i4IfIndex, 
                           UINT2 u2WtpInternalId,
                           INT4 *pi4RetValFsWPSAdditionalIE)
{
    tWpsContext *pWpsContext = NULL;
    UINT2 u2Index = 0;

    WpsGetPortEntry ((UINT2) i4IfIndex, &pWpsContext);

    if (pWpsContext == NULL)
    {
        WPS_TRC (WPS_CONTROL_PATH_TRC | WPS_ALL_FAILURE_TRC, 
                "No Wps Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    for (u2Index = 0; u2Index < NUM_OF_AP_SUPPORTED; u2Index++)
    {
	if (u2WtpInternalId == 
		pWpsContext->pWpsConfigDb[u2Index].u2WtpInternalId)
	{
            *pi4RetValFsWPSAdditionalIE = pWpsContext->pWpsConfigDb[u2Index].b1WpsAdditionalIE;
	    break;
	}
    }
    if (u2Index == NUM_OF_AP_SUPPORTED)
    	return SNMP_FAILURE;

    return SNMP_SUCCESS;
}
/****************************************************************************
 *                                                                           *
 * Function     : WpsGetWpsIEParams                                         *
 *                                                                           *
 * Description  : Function to get the WPS IE elements                        *
 *                                                                           *
 * Input        : u4WlanProfileIfIndex - Wlan Profile If index               * 
 *                                                                           *
 * Output       : pWpsIEElements - Pointer to WPS params                   *
 *                                                                           *
 * Returns     :  WPS_SUCCESS or WPS_FAILURE                               *
 *****************************************************************************/
UINT1 WpsGetWpsIEParams (UINT4 u4WlanProfileIfIndex,
			 UINT2 u2WtpInternalId, 
                         tWpsIEElements* pWpsIEElements)
{
    tWpsContext *pWpsContext = NULL;
    INT4                pi4RetValFsWPSStatus = 0;
    UINT2               u2Index = 0;
    UINT1               u1Count = 0;
    INT4               i4Length = 0;
    UINT1 		defaultMac[6] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};

    WpsGetPortEntry ((UINT2) u4WlanProfileIfIndex, &pWpsContext);

    if (pWpsContext == NULL)
    {
        WPS_TRC (ALL_FAILURE_TRC, "WpsGetWpsIEParams:- "
                "WpsGetPortEntry Failed \n");
        return WPS_FAILURE;
    }

    /* Filling the WPS Element id */
    pWpsIEElements->u1ElemId = WPS_VENDOR_ELEMENT_ID;
    i4Length++;

    pWpsIEElements->wscState = (UINT1) pWpsContext->e1WpsState;
    i4Length=  (i4Length + 1);

    for (u1Count = 0; u1Count < pWpsContext->u1noOfAuthMac; u1Count++)
    {
        WPS_MEMCPY(pWpsIEElements->authorized_macs[u1Count], pWpsContext->u1AuthorizedMacs[u1Count], ETH_ALEN);
        i4Length+= ETH_ALEN;
    }
    pWpsIEElements->noOfAuthMac = pWpsContext->u1noOfAuthMac;
    if (pWpsContext->u1noOfAuthMac == 0)
    {
        WPS_MEMCPY(pWpsIEElements->authorized_macs[0], defaultMac, ETH_ALEN);
        i4Length+= ETH_ALEN;
        pWpsIEElements->noOfAuthMac = 1;
    }
    i4Length++;
    WPS_MEMCPY(pWpsIEElements->uuid_e, pWpsContext->au1uuid, WPS_UUID_LEN);
    i4Length+= (WPS_UUID_LEN + 2);

    WpsGetWPSEnabled (u4WlanProfileIfIndex,u2WtpInternalId, 
            &pi4RetValFsWPSStatus);
    pWpsIEElements->bWpsEnabled = (BOOL1)pi4RetValFsWPSStatus;

    i4Length++;

    for (u2Index = 0; u2Index < NUM_OF_AP_SUPPORTED; u2Index++)
    {
	if (u2WtpInternalId == 
		pWpsContext->pWpsConfigDb[u2Index].u2WtpInternalId)
	{
	    pWpsIEElements->bWpsAdditionalIE = pWpsContext->pWpsConfigDb[u2Index].b1WpsAdditionalIE;
            pWpsIEElements->configMethods = pWpsContext->pWpsConfigDb[u2Index].u2ConfigMethods;
	    i4Length++;
	    break;
	}
    }
    /* Updating the length field and its counter value */
    pWpsIEElements->u1Len = (UINT1)i4Length;

    return WPS_SUCCESS;
}


/****************************************************************************
 * *                                                                           *
 * * Function     : WpsProcessWssNotification                                  *
 * *                                                                           *
 * * Description  : Function to handle WSS-WPS and WPS-WSS interactions        *
 * *                                                                           *
 * * Input        : pWssWpsNotifyParams:- Pointer to wps msg from Apme         *
 * *                                                                           *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 * *                                                                           *
 * *****************************************************************************/

UINT4 WpsProcessWssNotification (tWssWpsNotifyParams * pWssWpsNotifyParams)
{
    tWssWpsNotifyParams *pMsg = NULL;

    if ((pMsg = (tWssWpsNotifyParams *) WPS_MEM_ALLOCATE_MEM_BLK
                (WPS_WSS_INFO_MEMPOOL_ID)) == NULL)

    {
        WPS_TRC (ALL_FAILURE_TRC, "WpsProcessWssNotification:- "
                "WPS_MEM_ALLOCATE_MEM_BLK Failed \n");
        return (OSIX_FAILURE);
    }

    MEMCPY (pMsg, pWssWpsNotifyParams, sizeof (tWssWpsNotifyParams));

    switch (pMsg->eWssWpsNotifyType)
    {

        /*******************************************************
         *        *              Indication from WSS --> WPS 
         *               *****************************************************/

        case WSSWPS_PROFILEIF_ADD:
        case WSSWPS_PROFILEIF_DEL:
        case WSSWPS_PROBREQ_IND:
        case WSSWPS_ASSOC_IND:
        case WSSWPS_REASSOC_IND:
        case WSSWPS_DISSOC_IND:
        case PNACWPS_EAP_REQ_IND:
        case PNACWPS_EAP_MSG_IND:
        case WSSWPS_PBC_IND:
        
            /* The above cases falls under the category of WSS --> 
             *              * WPS  messages Since it is called by an external WSS  
             *                           * module, posting to the
             *                                        * WPS queue is required */
            WpsEnQFrameToWpsTask (pMsg);
            break;
        default:
            WPS_TRC_ARG1 (ALL_FAILURE_TRC, 
                    "WpsInterfaceHandleQueueEvent:- "
                    "Invalid Message Type %d\n", 
                    pMsg->eWssWpsNotifyType);
            break;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                           *
 * Function     : wpsRecvIdentity                                            *
 *                                                                           *
 * Description  : Function invoked from PNAC when Response identiy  is       *
 *                received                                                   *
 *                                                                           *
 * Input        : u2PortNo : recevied port no                                *
 *                macAddr  : supplicant mac addr                             *
 *                                                                           *
 * Output       : NONE                                                       *
 *                                                                           *
 * Returns     :  NONE                                                       *
 *****************************************************************************/
VOID wpsRecvIdentity(UINT2 u2PortNo, tMacAddr macAddr)
{
    wpsEapSendStart(macAddr, u2PortNo);
}

/****************************************************************************
 *                                                                           *
 * Function     : wpsRecvEapPkt                                              *
 *                                                                           *
 * Description  : Function invoked from PNAC when WSC EAP messages are       *
 *                received                                                   *
 *                                                                           *
 * Input        : u2PortNo : recevied port no                                *
 *                macAddr  : supplicant mac addr                             *
 *                pu1PktPtr: recevied packet                                 *
 *                u2PktLen  :recevied packet len                             *
 *                                                                           *
 * Output       : NONE                                                       *
 *                                                                           *
 * Returns     :  NONE                                                       *
 *****************************************************************************/
VOID wpsRecvEapPkt(UINT2 u2PortNo,
		   tMacAddr macAddr, 
                   UINT1 *pu1PktPtr, 
                   UINT2 u2PktLen)
{
    WpsProcessEap(pu1PktPtr, u2PktLen, u2PortNo, macAddr);
}

/****************************************************************************
 *                                                                           *
 * Function     : wpsSendUpdateFromRsna                                      *
 *                                                                           *
 * Description  : Function invoked from RSNA to update the auth type         *
 *                                                                           *
 * Input        : u4WlanIfIndex : wlan ifIndex                              *
 *                                                                           *
 * Output       : NONE                                                       *
 *                                                                           *
 * Returns     :  NONE                                                       *
 *****************************************************************************/
VOID
wpsSendUpdateFromRsna(INT4 i4WlanIfIndex)
{
    tWpsContext *pWpsContext = NULL;
    INT4 i4Dot11RSNAEnabled = RSNA_DISABLED;

    WpsGetPortEntry ((UINT2) i4WlanIfIndex, &pWpsContext);

    if (pWpsContext == NULL)
    {
	WPS_TRC (WPS_CONTROL_PATH_TRC | WPS_ALL_FAILURE_TRC, 
		"No Wps Info for the given port!! ... \n");
	return ;
    }
    RsnaGetDot11RSNAEnabled (i4WlanIfIndex,&i4Dot11RSNAEnabled);
    if (i4Dot11RSNAEnabled == RSNA_DISABLED)
    {
	pWpsContext->u2ApAuthType &=(UINT2)(~WPS_AUTH_WPA2PSK);
    }
    else
    {
	pWpsContext->u2ApAuthType |= (UINT2)WPS_AUTH_WPA2PSK;
    }
}
/****************************************************************************
 *                                                                           *
 * Function     : wpsSendEncrypUpdateFromRsna                                *
 *                                                                           *
 * Description  : Function invoked from RSNA to update the encryption type   *
 *                                                                           *
 * Input        : i4IfIndex: wlan ifIndex                                    *
 *                u4Dot11RSNAConfigPairwiseCipherIndex :                     *
 *                index of cipher indicating the type of cipher              *
 *                i4SetValDot11RSNAConfigPairwiseCipherEnabled :             *
 *                cipher enabled/disabled                                    *
 *                                                                           *
 * Output       : NONE                                                       *
 *                                                                           *
 * Returns     :  NONE                                                       *
 *****************************************************************************/
INT1
wpsSendEncrypUpdateFromRsna (INT4 i4IfIndex,
                             UINT4      
                             u4Dot11RSNAConfigPairwiseCipherIndex,
                             INT4       
                             i4SetValDot11RSNAConfigPairwiseCipherEnabled)
{
    tWpsContext *pWpsContext = NULL;

    WpsGetPortEntry ((UINT2) i4IfIndex, &pWpsContext);

    if (pWpsContext == NULL)
    {
        WPS_TRC (WPS_CONTROL_PATH_TRC | WPS_ALL_FAILURE_TRC, 
                "No Wps Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }
    if (i4SetValDot11RSNAConfigPairwiseCipherEnabled == RSNA_ENABLED)
    {
	if (u4Dot11RSNAConfigPairwiseCipherIndex == RSNA_TKIP)
	    pWpsContext->u2ApEncrType |= WPS_ENCR_TKIP;
        else 
	    pWpsContext->u2ApEncrType |= WPS_ENCR_AES;
    }    
    else
    {
	if (u4Dot11RSNAConfigPairwiseCipherIndex == RSNA_TKIP)
            pWpsContext->u2ApEncrType &= (UINT2)(~WPS_ENCR_TKIP);
        else
            pWpsContext->u2ApEncrType &= (UINT2)(~WPS_ENCR_AES);
    }
    pWpsContext->u2ApEncrType = WPS_ENCR_AES;
    return WPS_SUCCESS;
}
/****************************************************************************
 *                                                                           *
 * Function     : wpsSendPskUpdateFromRsna                                   *
 *                                                                           *
 * Description  : Function invoked from RSNA to update the psk/passphrase    *
 *                                                                           *
 * Input        : i4IfIndex: wlan ifIndex                                    *
 *                                                                           *
 * Output       : NONE                                                       *
 *                                                                           *
 * Returns     :  NONE                                                       *
 *****************************************************************************/
VOID
wpsSendPskUpdateFromRsna(INT4 i4IfIndex)
{
    tWpsContext *pWpsContext = NULL;
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    WpsGetPortEntry ((UINT2) i4IfIndex, &pWpsContext);

    if (pWpsContext == NULL)
    {
        WPS_TRC (WPS_CONTROL_PATH_TRC | WPS_ALL_FAILURE_TRC, 
                "No Wps Info for the given port!! ... \n");
        return; 
    }

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);
        
    if (pRsnaPaePortEntry == NULL)
    {   
        WPS_TRC (WPS_CONTROL_PATH_TRC | WPS_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return;
    }
    WPS_MEMCPY (pWpsContext->au1Psk,pRsnaPaePortEntry->RsnaConfigDB.au1PskPassPhrase, RSNA_MAX_PASS_PHRASE_LEN); 
    return;
}

/* LOW LEVEL Routines for Table : WPSAuthSTAInfoTable. */

/****************************************************************************
 Function    :  WpsValidateIndexInstanceFsWPSAuthSTAInfoTable
 Input       :  The Indices
                IfIndex
                u4FsWPSStatsIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
WpsValidateIndexInstanceFsWPSAuthSTAInfoTable(INT4 i4IfIndex, UINT4 u4FsWPSStatsIndex)
{
    if ((i4IfIndex < CFA_MIN_WSS_IF_INDEX) ||
        (i4IfIndex > CFA_MAX_WSS_IF_INDEX))
    {
        CLI_SET_ERR (CLI_WPS_INVALID_WLAN_ID);

        return SNMP_FAILURE;
    }
    UNUSED_PARAM (u4FsWPSStatsIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  WpsGetFirstIndexFsWPSAuthSTAInfoTable
 Input       :  The Indices
                IfIndex
                FsWPSStatsIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
WpsGetFirstIndexFsWPSAuthSTAInfoTable(INT4 *pi4IfIndex, UINT4 *pu4FsWPSStatsIndex)
{
    tWpsStat * pTmpWpsStat = NULL;

    WpsUtilGetFirstSuppStatsEntry(&pTmpWpsStat);

    if (pTmpWpsStat == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4IfIndex = pTmpWpsStat->u2Port;
    *pu4FsWPSStatsIndex = pTmpWpsStat->u4StatsIndex;

     return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  WpsGetNextIndexFsWPSAuthSTAInfoTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
                u4FsWPSStatsIndex
                pu4NextFsWPSStatsIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

/* GET_NEXT Routine.  */
INT1
WpsGetNextIndexFsWPSAuthSTAInfoTable(INT4 i4IfIndex, INT4 *pi4NextIfIndex,
                                     UINT4 u4FsWPSStatsIndex,
                                     UINT4 *pu4NextFsWPSStatsIndex)
{
    tWpsStat  TmpWpsStat;
    tWpsStat * pNextTmpWpsStat = NULL;

    WPS_MEMSET (&TmpWpsStat, 0, sizeof (tWpsStat));

    TmpWpsStat.u2Port = (UINT2)i4IfIndex;
    TmpWpsStat.u4StatsIndex = u4FsWPSStatsIndex;

    WpsUtilGetNextSuppStatsEntry(&TmpWpsStat,&pNextTmpWpsStat);

    if (pNextTmpWpsStat == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4NextIfIndex = pNextTmpWpsStat->u2Port;
    *pu4NextFsWPSStatsIndex = pNextTmpWpsStat->u4StatsIndex;

    return SNMP_SUCCESS;
}

/****************************************************************************
 * Function    :  WpsGetPBCStatus
 * Input       :  The Indices
 * IfIndex
 *
 * The Object
 * retValPBCStatus
 * Output      :  The Get Low Lev Routine Take the Indices &
 * store the Value requested in the Return val.
 * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *  ****************************************************************************/

INT1 WpsGetPBCStatus(INT4 i4IfIndex,
                     UINT2 u2WtpInternalId,
                     INT4 *pi4RetValFsWPSPushButtonStatus)
{
    tWpsContext *pWpsContext = NULL;
    UINT2 u2Index = 0;

    WpsGetPortEntry ((UINT2) i4IfIndex, &pWpsContext);

    if (pWpsContext == NULL)
    {
        WPS_TRC (WPS_CONTROL_PATH_TRC | WPS_ALL_FAILURE_TRC,
                "No Wps Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }


    for (u2Index = 0; u2Index < NUM_OF_AP_SUPPORTED; u2Index++)
    {
	if (u2WtpInternalId == 
		pWpsContext->pWpsConfigDb[u2Index].u2WtpInternalId)
	{
            pWpsContext->pWpsConfigDb[u2Index].u2WtpInternalId = u2WtpInternalId;
            *pi4RetValFsWPSPushButtonStatus = (INT4)pWpsContext->pWpsConfigDb[u2Index].u4PushButtonStatus;
	    break;
	}
    }
    if (u2Index == NUM_OF_AP_SUPPORTED)
    	return SNMP_FAILURE;

    return SNMP_SUCCESS;
}

