/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: wpsutil.c,v 1.1.1.1 2016/09/20 10:42:41 siva Exp $
 *
 * Description: Protocol Low Level Routines
 *********************************************************************/
#include "wpsinc.h"

/****************************************************************************
 * *                                                                           *
 * * Function     : WpsUtilAddStaToStatsTable                                 *
 * *                                                                           *
 * * Description  : Function to add station to the  stats table                *
 * *                                                                           *
 * * Input        : u2PortNum    :- ProfileIfIndex                             *
 * *                u4StatsIndex :- Auxiliary Index                            *
 * *                pu1SuppAddr  :- Refers to the supplicant address           *
 * *                                                                           *
 * * Output       : tWpsSuppStatsInfo                                          *
 * *                                                                           *
 * * Returns      : tWpsSuppStatsInfo                                         * 
 * *                                                                           *
 * *****************************************************************************/
tWpsStat*
WpsUtilAddStaToStatsTable(UINT2 u2PortNum,UINT4 u4StatsIndex,UINT1 *pu1SuppAddr)
{

    WPS_TRC(MSG_DEBUG, "\nWpsUtilAddStaToStatsTable\n");
    tWpsStat * pWpsStat= NULL;

    if ((pWpsStat = WPS_MEM_ALLOCATE_MEM_BLK(WPS_STAT_INFO_MEMPOOL_ID)) == NULL)
    {
        WPS_TRC (WPS_CONTROL_PATH_TRC | WPS_ALL_FAILURE_TRC,
                  " WpsUtilAddStaToStatsTable :- MemAllocMemBlk Failed \n");
        return NULL;
    }

    /* Before adding the node to the RB Tree.check whether the node is already
     * present */
    if ((WpsUtilGetStationFromStatsTable(u2PortNum,
                  pu1SuppAddr)) == NULL)
    {
        WPS_MEMSET (pWpsStat, 0, sizeof (tWpsStat));
        WPS_MEMCPY (pWpsStat->au1SuppMacAddr, pu1SuppAddr, ETH_ALEN);
        pWpsStat->u4StatsIndex = u4StatsIndex;
        pWpsStat->u2Port = u2PortNum;
        pWpsStat->e1PbcStatus = WPS_PBC_STATUS_INACTIVE;
        if (RBTreeAdd (gWpsGlobals.WpsDataTable,pWpsStat) == 
                RB_FAILURE)
        {
            WPS_TRC (WPS_INIT_SHUT_TRC,
                    "\nWpsUtilAddStaToStatsTable:- RBTreeAdd" "returned Failure\n");
            MemReleaseMemBlock (WPS_STAT_INFO_MEMPOOL_ID, 
                    (UINT1 *) pWpsStat);
            return (NULL);

        }
    }
    else
    {
            WPS_TRC (WPS_MGMT_TRC,
                    "\nWpsUtilAddStaToStatsTable:- STA entry" "already exists in Supplicant Stats table\n");


    }

    return (pWpsStat);
}


/****************************************************************************
 * *                                                                           *
 * * Function     : WpsUtilGetStationFromStatsTable                           *
 * *                                                                           *
 * * Description  : Function to get the Supplicant Stats info  from the Stats  *
 * *                Table                                                      *
 * *                                                                           *
 * * Input        : u2PortNum :-  ProfileIfIndex                               *
 * *                pu1Addr   :-  Refers to the supplicant mac addr            *
 * *                                                                           *
 * * Output       : tWpsSuppStatsInfo                                          *
 * *                                                                           *
 * * Returns      : tWpsSuppStatsInfo                                         *
 * *                                                                           *
 * *****************************************************************************/
tWpsStat*
WpsUtilGetStationFromStatsTable (UINT2 u2PortNum,UINT1 *pu1Addr)
{
    WPS_TRC(MSG_DEBUG, "\nWpsUtilGetStationFromStatsTable\n");
    tWpsStat* pWpsStat= NULL;
    tWpsStat  InWpsStat;

    InWpsStat.u2Port = u2PortNum;
    WPS_MEMCPY (InWpsStat.au1SuppMacAddr, pu1Addr, ETH_ALEN);

    pWpsStat = RBTreeGet (gWpsGlobals.WpsDataTable, 
                             ((tRBElem *) &InWpsStat));

    return pWpsStat;

}
/****************************************************************************
 * *                                                                           *
 * * Function     : WpsUtilDeleteStationFromStatsTable                        *
 * *                                                                           *
 * * Description  : Function to delete the Supplicant Stats info  from the     *
 * *                Stats Table                                                *
 * *                                                                           *
 * * Input        : pWpsStat:- SupplicantStatsInfo for the STA to              *
 * *                                      be deleted.                          *
 * *                                                                           *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : WPS_SUCCESS/WPS_FAILURE                                    *
 * *                                                                           *
 * *****************************************************************************/
UINT4
WpsUtilDeleteSuppStatsInfo(tWpsStat * pWpsStat)
{
    WPS_TRC(MSG_DEBUG, "\nWpsUtilDeleteSuppStatsInfo\n");
    RBTreeRemove (gWpsGlobals.WpsDataTable, (tRBElem *)pWpsStat);

    if ((WPS_MEM_RELEASE_MEM_BLK (WPS_STAT_INFO_MEMPOOL_ID, 
         pWpsStat)) == MEM_FAILURE)
    {
        WPS_TRC (WPS_CONTROL_PATH_TRC | WPS_ALL_FAILURE_TRC,
                  "\nWpsUtilDeleteSuppStatsInfo:-WPS_STAT_INFO_MEMPOOL_ID"
                  "Failed !!!! \n");
        return (WPS_FAILURE);
    }
    return (WPS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : WpsUtilGetFirstPortEntry                            */
/*                                                                           */
/* Description        : This function returns pointer to the first Wps port */
/*                      entry                                                */
/*                                                                           */
/* Input (s)          : ppWpsStat - Pointer to WPS port Entry                */
/*                                                                           */
/* Output(s)          : pWpsPortEntry - Wps Port Entry pointer for         */
/*                      which first node should be returned                  */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
WpsUtilGetFirstSuppStatsEntry (tWpsStat** ppWpsStat)
{
    WPS_TRC(MSG_DEBUG, "\nWpsUtilGetFirstSuppStatsEntry\n");
    tWpsStat * pTmpWpsStat = NULL;

    pTmpWpsStat = (tWpsStat*) RBTreeGetFirst
        (gWpsGlobals.WpsDataTable);

    *ppWpsStat = pTmpWpsStat;

    return;
}

/****************************************************************************
 * *                                                                           *
 * * Function     : WpsUtilGetNextSuppStatsEntry                              *
 * *                                                                           *
 * * Description  : Function to get the next  Supplicant Stats info  from the  *
 * *                Stats Table                                                *
 * *                                                                           *
 * * Input        : pWpsStat:- SupplicantStatsInfo of the STA to    *
 * *                                      which next stats entry has to be     *
 * *                                      fetched.                             *
 * *                ppWpsStat:- Pointer for NextSuppStats entry *
 * *                                                                           *
 * * Output       : get WPS next supplicant stats info                         *
 * *                                                                           *
 * * Returns      : None                                                       *
 * *                                                                           *
 * *****************************************************************************/


VOID
WpsUtilGetNextSuppStatsEntry (tWpsStat * pWpsStat,
                           tWpsStat ** ppWpsStat)
{
    WPS_TRC(MSG_DEBUG, "\nWpsUtilGetNextSuppStatsEntry\n");
    tWpsStat *pTmpWpsStat= NULL;

    if (pWpsStat == NULL)
    {

        pTmpWpsStat = (tWpsStat *) RBTreeGetFirst
            (gWpsGlobals.WpsDataTable);
    }
    else
    {

        pTmpWpsStat = (tWpsStat *) RBTreeGetNext
            (gWpsGlobals.WpsDataTable, (tRBElem *) pWpsStat, NULL);
    }

    *ppWpsStat = pTmpWpsStat;
    return;
}


/****************************************************************************
 * * Function Name      : WpsUtilGetSuppStatsInfo                             *
 * *                                                                           *
 * * Description        : Gets the pointer to the next SuppStats entry         *
 * *                                                                           *
 * * Input              : u2PortNum        - Port Number.                      *
 * *                      u4StatsIndex     - An auxiliary index                *
 * *                                                                           *
 * * Output             : ppSuppStatsInfo  - double pointer for the stats      *
 * *                                         entry to be fetched from the      *
 * *                                         table                             *
 * *                                                                           *
 * * Return Value(s)    : None                                                 *
 * *                                                                           *
 * *****************************************************************************/

VOID
WpsUtilGetSuppStatsInfo (UINT2 u2PortNum, UINT4 u4SuppStatsIndex,
                      tWpsStat ** ppWpsStat)
{
    WPS_TRC(MSG_DEBUG, "\nWpsUtilGetSuppStatsInfo\n");

    tWpsStat * pTmpWpsStat= NULL;
    tWpsStat WpsStat;

    WPS_MEMSET (&WpsStat, 0, sizeof (tWpsStat));

    WpsStat.u2Port = u2PortNum;
    WpsStat.u4StatsIndex = u4SuppStatsIndex;

    pTmpWpsStat = RBTreeGet (gWpsGlobals.WpsDataTable,
                              (tRBElem *)(&WpsStat));

    *ppWpsStat = pTmpWpsStat;

    return;
}

/****************************************************************************
*                                                                           *
* Function     : WpsTxFrame                                                *
*                                                                           *
* Description  : Sends an EAP frame                              *
*                                                                           *
* Input        : pWpsSessNode :- Pointer to WPS session node              *
*                u2KeyInfo :- Specifies the characteristics of the key      *
*                pu1KeyRsc :- Contains the receive sequence counter         *
*                pu1Nonce  :- Nonce to be conveyed to the supplicant        *
*                bGtk      :- Flag indicates whether gtk is part of exch    *
*                bIE       :- Flag Indicates whether IE is part of exch     *
*                bPmkIdKde :- Flag Indicates whether PmkId is part of exch  *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : WPS_SUCCESS/WPS_FAILURE                                  *
*                                                                           *
*****************************************************************************/
UINT4
WpsTxFrame (tWpsEapPacketInfo *pu1Frame, tMacAddr StaMac,
             UINT4 u4BssIfIndex, INT4 i4BufLen)
{
    WPS_TRC(MSG_DEBUG, "\nWpsTxFrame\n");
    unWlcHdlrMsgStruct *pCapwapMsgStruct = NULL;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    UINT2               u2OffSet = 0;
    UINT2               u2PktLen = 0;
    UINT2               u2PacketType = 0x888E;
    UINT2               u2Val = 0;
    tBssid              BssMac = {0};
    tCfaIfInfo          IfInfo; 

    /* Compute the EAPOL Key packet length by considering, */
    /* its Layer 2 header also */
    u2PktLen = (UINT2) (i4BufLen + 14);

    /* Allocate a CRU buffer message chain for this KeyEapol frame */
    if ((pBuf = CRU_BUF_Allocate_MsgBufChain (u2PktLen, u2OffSet)) == NULL)
    {
        return WPS_FAILURE;
    }

    CfaGetIfInfo (u4BssIfIndex, &IfInfo);

    WPS_MEMCPY (BssMac, IfInfo.au1MacAddr, RSNA_ETH_ADDR_LEN);

    u2Val = (UINT2) OSIX_HTONS (u2PacketType);

    CRU_BUF_Copy_OverBufChain (pBuf, StaMac, 0, 6);
    CRU_BUF_Copy_OverBufChain (pBuf, BssMac, 6, 6);
    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2Val, 12, 2);
    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) pu1Frame, 14, (UINT4)i4BufLen);

    pCapwapMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();

    if (pCapwapMsgStruct == NULL)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        WPS_TRC (WPS_ALL_FAILURE_TRC | WPS_DATA_PATH_TRC,
                  "\nWpsTxFrame:- " "UtlShMemAllocWlcBuf returned failure\n");
        return WPS_FAILURE;
    }

    WPS_MEMSET (pCapwapMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));

    pCapwapMsgStruct->WlcHdlrQueueReq.pRcvBuf = pBuf;
    pCapwapMsgStruct->WlcHdlrQueueReq.u2SessId = (UINT2) u4BssIfIndex;
    pCapwapMsgStruct->WlcHdlrQueueReq.u4MsgType = WLCHDLR_CFA_RX_MSG;

    /* Invoke the WSSIF module to send the packet to MAC Handler module */
    if (WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_CFA_QUEUE_REQ, pCapwapMsgStruct)
        != OSIX_SUCCESS)
    {
        UtlShMemFreeWlcBuf ((UINT1 *) pCapwapMsgStruct);
        return WPS_FAILURE;
    }
    UtlShMemFreeWlcBuf ((UINT1 *) pCapwapMsgStruct);
    UNUSED_PARAM (u4BssIfIndex);

    return WPS_SUCCESS;

}
/****************************************************************************
 * *                                                                           *
 * * Function     : WpsSessCreatePnacAuthSess                                 *
 * *                                                                           *
 * * Description  : Function to create pnac auth session                       *
 * *                                                                           *
 * * Input        : srcAddr :- Refers to the source address                    *
 * *                u2PortNum :- Port number on which the pkt recvd            *
 * *                                                                           *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : WPS_SUCCESS/WPS_FAILURE                                  *
 * *                                                                           *
 * *****************************************************************************/

tPnacAuthSessionNode *
WpsSessCreatePnacAuthSess (UINT1 *pu1SrcAddr,
                            UINT2 u2PortNum, BOOL1 bTriggerPnacStateMachine)
{
    WPS_TRC(MSG_DEBUG, "\nWpsSessCreatePnacAuthSess\n");
    tPnacAuthSessionNode *pAuthSessNode = NULL;
    tPnacPaePortEntry  *pPortInfo = NULL;
    tPnacAuthFsmInfo   *pAuthFsmInfo = NULL;
    UINT4               u4Index = 0;

    if (PnacGetPortEntry (u2PortNum, &pPortInfo) != PNAC_SUCCESS)
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " CreateSession: Invalid Port Number \n");
        return (NULL);
    }
    if (PNAC_ALLOCATE_AUTHSESSNODE_MEMBLK (pAuthSessNode) == NULL)
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " CreateSession: Allocate auth sess node failed \n");
        return (NULL);
    }
    PNAC_MEMSET (pAuthSessNode, PNAC_INIT_VAL, sizeof (tPnacAuthSessionNode));
    PnacAuthSessionNodeInit (pAuthSessNode, u2PortNum);
    WPS_MEMCPY (pAuthSessNode->rmtSuppMacAddr, pu1SrcAddr, ETH_ALEN);
    pAuthFsmInfo = &(pAuthSessNode->authFsmInfo);
    PnacAuthCalcSessionTblIndex (pAuthSessNode->rmtSuppMacAddr, &u4Index);
    (pPortInfo->u2SuppCount)++;

    pAuthFsmInfo->u2AuthControlPortStatus = PNAC_PORTSTATUS_UNAUTHORIZED;
    pAuthSessNode->u1IsAuthorizedOnce = PNAC_FALSE;
    pAuthFsmInfo->bKeyRun = TRUE;
    pAuthFsmInfo->bKeyAvailable = TRUE;
    pAuthFsmInfo->u2AuthControlPortStatus = PNAC_PORTSTATUS_UNAUTHORIZED;
    pAuthFsmInfo->u2AuthPaeState = PNAC_ASM_STATE_AUTHENTICATED;
    pAuthFsmInfo->u2PortMode = PNAC_PORT_AUTHMODE_MACBASED;

    if (bTriggerPnacStateMachine == TRUE)
    {
        if (PnacAuthAddToAuthInProgressTbl (pAuthSessNode) != PNAC_SUCCESS)
        {
            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                      " CreateSession: Adding Node in AIP Table failed\n");
            PnacAuthDeleteSession (pAuthSessNode);
            return (NULL);
        }

        if (PnacAuthStateMachine (PNAC_ASM_EV_INITIALIZE,
                                  u2PortNum,
                                  pAuthSessNode, NULL,
                                  PNAC_NO_VAL) != PNAC_SUCCESS)
        {
            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                      " CreateSession: ASM returned failure\n");
            PnacAuthDeleteSession (pAuthSessNode);
            return (NULL);
        }
        if (PnacAuthBackendStateMachine (PNAC_BSM_EV_INITIALIZE,
                                         u2PortNum, pAuthSessNode,
                                         NULL, PNAC_NO_VAL,
                                         NULL) != PNAC_SUCCESS)
        {
            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                      " CreateSession: BSM returned failure\n");
            PnacAuthDeleteSession (pAuthSessNode);
            return (NULL);
        }
    }
    else
    {
        PNAC_HASH_ADD_NODE (PNAC_AUTH_SESSION_TABLE,
                            pAuthSessNode, u4Index, NULL);
        if (PnacL2IwfSetSuppMacAuthStatus
            (pAuthSessNode->rmtSuppMacAddr,
             PNAC_PORTSTATUS_AUTHORIZED) != PNAC_SUCCESS)
        {
            PNAC_HASH_DEL_NODE (PNAC_AUTH_SESSION_TABLE, pAuthSessNode,
                                u4Index);
            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                      "Set in L2Iwf returned failure \n");
            return NULL;
        }
        pAuthFsmInfo->u2AuthBackendAuthState = PNAC_BSM_STATE_IDLE;
        pAuthFsmInfo->u2AuthControlPortStatus = PNAC_PORTSTATUS_AUTHORIZED;
    }
    return (pAuthSessNode);
}

/****************************************************************************
 * *                                                                           *
 * * Function     : wpsDeleteSession                                           *
 * *                                                                           *
 * * Description  : Function to delete WPS Session                             *
 * *                                                                           *
 * * Input        : pWpsSessionNode : pointer to the session node to be deleted*
 * *                                                                           * 
 * *                                                                           *
 * * Output       : NONE                                                       *
 * *                                                                           *
 * * Returns      : WPS_FAILURE/WPS_SUCCESS                                    *
 * *                                                                           *
 * *****************************************************************************/
UINT1 wpsDeleteSession(tWpsSessionNode *pWpsSessionNode)
{
    WPS_TRC(MSG_DEBUG, "\nwpsDeleteSession\n");
    UINT2 u2Index = 0;
    WpsTmrStopTimer (&(pWpsSessionNode->WpsProtocolTmr));
    WpsTmrStopTimer (&(pWpsSessionNode->WpsReTransTmr));
    WpsTmrStopTimer (&(pWpsSessionNode->WpsMsgTmr));
    if (pWpsSessionNode->pAuthSessNode != NULL)
    {
            PnacAuthDeleteSession (pWpsSessionNode->pAuthSessNode);
    }
    if (pWpsSessionNode->pWpsProbeReq != NULL)
    {
	WpsTmrStopTimer (&(pWpsSessionNode->pWpsProbeReq->WpsSessionTmr));
	RBTreeRemove (gWpsGlobals.WpsProbeReqTable, (tRBElem *) pWpsSessionNode->pWpsProbeReq);
	if (WPS_MEM_RELEASE_MEM_BLK (WPS_PROBEREQ_MEMPOOL_ID,
		    pWpsSessionNode->pWpsProbeReq) == MEM_FAILURE)
	{
	    WPS_TRC (WPS_CONTROL_PATH_TRC | WPS_ALL_FAILURE_TRC,
		    "\nwpsDeleteSession:- WPS_PROBEREQ_MEMPOOL_ID release"
		    "Failed !!!! \n");
	}
        pWpsSessionNode->pWpsProbeReq = NULL;
    }
    TMO_SLL_Delete(&(pWpsSessionNode->pWpsContext->wpsStationInfoList),
	    (tTMO_SLL_NODE *) pWpsSessionNode);
    WssWpsSendDeAuthMsg (pWpsSessionNode->pWpsContext->au1SrcMac,
	    pWpsSessionNode->au1SuppMacAddr,pWpsSessionNode->u2SessId);
    for (u2Index = 0; u2Index < NUM_OF_AP_SUPPORTED; u2Index++)
    {
	if (pWpsSessionNode->u2WtpInternalId == 
		pWpsSessionNode->pWpsContext->pWpsConfigDb[u2Index].u2WtpInternalId)
	{
		pWpsSessionNode->pWpsContext->pWpsConfigDb[u2Index].b1WpsAdditionalIE = 
			WPS_ADDITIONLALIE_DISABLE;
		pWpsSessionNode->pWpsContext->pWpsConfigDb[u2Index].u4PushButtonStatus = 
			WPS_PBC_INACTIVE;
		break;
	}
    }
    WssSendUpdateWlanMsgfromWps ((UINT4) pWpsSessionNode->pWpsContext->u2Port,
			         pWpsSessionNode->u2WtpInternalId,
				 SET_ADDITIONAL_IE);
    if (pWpsSessionNode->pWpsParseAttr != NULL)
    {
	if (WPS_MEM_RELEASE_MEM_BLK (WPS_PARSE_ATTR_INFO_MEMPOOL_ID,
		    pWpsSessionNode->pWpsParseAttr) == MEM_FAILURE)
	{
	    WPS_TRC (WPS_CONTROL_PATH_TRC | WPS_ALL_FAILURE_TRC,
		    "\nwpsDeleteSession:- WPS_RELEASE_SUPP_INFO "
		    "Failed !!!! \n");
	}	
    }
    if (WPS_MEM_RELEASE_MEM_BLK (WPS_DATA_INFO_MEMPOOL_ID,
		pWpsSessionNode) == MEM_FAILURE)
    {
	WPS_TRC (WPS_CONTROL_PATH_TRC | WPS_ALL_FAILURE_TRC,
		"\nwpsDeleteSession:- WPS_RELEASE_SUPP_INFO "
		"Failed !!!! \n");
    }
    return WPS_SUCCESS;
}


/****************************************************************************
 * *                                                                           *
 * * Function     : WpsCompareContextIndex                                     *
 * *                                                                           *
 * * Description  : compares the port numbers to traverse the tree             *                                   * *                                                                           * 
 * * Input        : e1, e2 : context to be compared                            *
 * *                                                                           *
 * * Output       : NONE                                                       *
 * *                                                                           *
 * * Returns      : WPS_LESSER/WPS_GREATER/WPS_EQUAL                           *
 * *                                                                           *
 * *****************************************************************************/
INT4 WpsCompareContextIndex(tRBElem * e1, tRBElem * e2)
{
    tWpsContext *pRBNode1 = (tWpsContext *) e1;
    tWpsContext *pRBNode2 = (tWpsContext *) e2;

    if (pRBNode1->u2Port < pRBNode2->u2Port)
    {
        return WPS_LESSER;
    }
    else if (pRBNode1->u2Port > pRBNode2->u2Port)
    {
        return WPS_GREATER;
    }
    return WPS_EQUAL;
}
/****************************************************************************
 * *                                                                           *
 * * Function     : WpsCompareProbeIndex                                       *
 * *                                                                           *
 * * Description  : compare the macAddress to traverse the tree                *
 * *                                                                           *     
 * * Input        : e1, e2 : context to be compared                            *
 * *                                                                           *
 * * Output       : NONE                                                       *
 * *                                                                           *
 * * Returns      : WPS_LESSER/WPS_GREATER/WPS_EQUAL                           *
 * *                                                                           *
 * *****************************************************************************/
INT4 WpsCompareProbeIndex(tRBElem * e1, tRBElem * e2)
{
    tWpsProbeReq *pRBNode1 = (tWpsProbeReq*) e1;
    tWpsProbeReq *pRBNode2 = (tWpsProbeReq*) e2;

    UINT1               au1ZeroMac[ETH_ALEN];

    WPS_MEMSET (au1ZeroMac, 0, ETH_ALEN);

    if (MEMCMP (pRBNode1->au1SuppMacAddr, pRBNode2->au1SuppMacAddr, 
                ETH_ALEN) < 0)
    {
        return WPS_LESSER;
    }
    else if (MEMCMP (pRBNode1->au1SuppMacAddr, pRBNode2->au1SuppMacAddr,
              ETH_ALEN) > 0)
    {
        return WPS_GREATER;
    }

    return WPS_EQUAL;
}
/****************************************************************************
 * *                                                                           *
 * * Function     : WpsCompareDataIndex                                        *
 * *                                                                           *
 * * Description  : compare the macAddress to traverse the tree                *
 * *                                                                           *     
 * * Input        : e1, e2 : context to be compared                            *
 * *                                                                           *
 * * Output       : NONE                                                       *
 * *                                                                           *
 * * Returns      : WPS_LESSER/WPS_GREATER/WPS_EQUAL                           *
 * *                                                                           *
 * *****************************************************************************/
INT4 WpsCompareDataIndex(tRBElem * e1, tRBElem * e2)
{
    tWpsStat *pRBNode1 = (tWpsStat*) e1;
    tWpsStat *pRBNode2 = (tWpsStat*) e2;

    UINT1               au1ZeroMac[ETH_ALEN];

    WPS_MEMSET (au1ZeroMac, 0, ETH_ALEN);


    if (pRBNode1->u2Port < pRBNode2->u2Port)
    {
        return WPS_LESSER;
    }
    else if (pRBNode1->u2Port > pRBNode2->u2Port)
    {
        return WPS_GREATER;
    }

    if (MEMCMP (pRBNode1->au1SuppMacAddr,au1ZeroMac,ETH_ALEN) == 0)
    {

        if (pRBNode1->u4StatsIndex < pRBNode2->u4StatsIndex)
        {
            return WPS_LESSER;
        }
        else if (pRBNode1->u4StatsIndex > pRBNode2->u4StatsIndex)
        {
            return WPS_GREATER;
        }

        return WPS_EQUAL;
    }

    if (MEMCMP (pRBNode1->au1SuppMacAddr, pRBNode2->au1SuppMacAddr, 
                ETH_ALEN) < 0)
    {
        return WPS_LESSER;
    }
    else if (MEMCMP (pRBNode1->au1SuppMacAddr, pRBNode2->au1SuppMacAddr,
              ETH_ALEN) > 0)
    {
        return WPS_GREATER;
    }

    return WPS_EQUAL;
}


/****************************************************************************
 * *                                                                           *
 * * Function     : WpsUtilGetRandom                                          *
 * *                                                                           *
 * * Description  : Function to generate random number                         *
 * *                                                                           *
 * * Input        : pu1Ptr :- Pointer to the generated random number           *
 * *                u1Len  :- Length of the random number                      *
 * *                                                                           *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : None                                                       *
 * *                                                                           *
 * *****************************************************************************/
VOID
WpsUtilGetRandom (UINT1 *pu1Ptr, UINT1 u1Len)
{
    WPS_TRC(MSG_DEBUG, "\nWpsUtilGetRandom\n");
    UINT4               u4Temp = 0;
    UINT1              *pu1Temp = NULL;

    pu1Temp = pu1Ptr;

    while (u1Len > sizeof (UINT4))
    {
        u4Temp = (UINT4) RAND ();
        WPS_MEMCPY (pu1Temp, &u4Temp, sizeof (UINT4));
        pu1Temp += sizeof (UINT4);
        u1Len = (UINT1) (u1Len - sizeof (UINT4));
    }

    u4Temp = (UINT4) RAND ();
    WPS_MEMCPY (pu1Temp, &u4Temp, sizeof (UINT4));
    return;
}
/*****************************************************************************/
/* Function Name      : WpsAddPortEntry                                     */
/*                                                                           */
/* Description        : This function adds the entry to the WPS             */
/*                      port table                                           */
/*                                                                           */
/* Input(s)           : u2PortNum -u2Port Index                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : WPS_SUCCESS/ WPS_FAILURE                             */
/*****************************************************************************/
INT4
WpsAddPortEntry (UINT4 u4IfIndex)
{
    WPS_TRC(MSG_DEBUG, "\nWpsAddPortEntry\n");
    tWpsContext  *pWpsContext = NULL;
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;
    tCfaIfInfo          IfInfo;
    UINT1         uuid[WPS_UUID_LEN] = {0};
    UINT2 u2Index = 0;

    if ((pWpsContext = (tWpsContext *) WPS_MEM_ALLOCATE_MEM_BLK
		(WPS_CONTEXT_INFO_MEMPOOL_ID)) == NULL)
    {
	WPS_TRC (WPS_INIT_SHUT_TRC,
		"\nWpsAddPortEntry:- MemAllocMemBlk" "returned Failure\n");
	return (WPS_FAILURE);
    }

    WPS_MEMSET (pWpsContext, 0, sizeof (tWpsContext));
    pWpsContext->u2Port = (UINT2) u4IfIndex;

    if (RBTreeAdd (gWpsGlobals.WpsContextTable, pWpsContext)
	    == RB_FAILURE)
    {

	WPS_TRC (WPS_INIT_SHUT_TRC,
		"\nWpsAddPortEntry:- RBTreeAdd" "returned Failure\n");
	MemReleaseMemBlock (WPS_CONTEXT_INFO_MEMPOOL_ID,
		(UINT1 *) pWpsContext);
	return (WPS_FAILURE);
    }

    TMO_SLL_Init (&(pWpsContext->wpsStationInfoList));
    CfaGetIfInfo (u4IfIndex, &IfInfo);

    WPS_MEMCPY (pWpsContext->au1SrcMac, IfInfo.au1MacAddr, ETH_ALEN);

    pWpsContext->e1WpsState =  WPS_CONFIGURED;

    for (u2Index = 0; u2Index < NUM_OF_AP_SUPPORTED; u2Index++)
    {
	pWpsContext->pWpsConfigDb[u2Index].b1WpsEnabled = WPS_DISABLED;
	pWpsContext->pWpsConfigDb[u2Index].b1WpsAdditionalIE = WPS_ADDITIONLALIE_DISABLE;
	pWpsContext->pWpsConfigDb[u2Index].u4PushButtonStatus = WPS_PBC_INACTIVE;
	pWpsContext->pWpsConfigDb[u2Index].u4WpsWtpProfileId = u2Index;
    }

    WpsUtilGetRandom(uuid, WPS_UUID_LEN); 
    WPS_MEMCPY(pWpsContext->au1uuid,uuid, WPS_UUID_LEN); 

    RsnaGetPortEntry ((UINT2) u4IfIndex, &pRsnaPaePortEntry);

    pWpsContext->u2ApAuthType = WPS_AUTH_OPEN;
    if (pRsnaPaePortEntry == NULL)
    {
	pWpsContext->u2ApAuthType = WPS_AUTH_OPEN;
	pWpsContext->u2AuthTypeFlags |= WPS_AUTH_OPEN;
	pWpsContext->u2EncrTypeFlags = WPS_ENCR_AES;
    }
    else
    {
	if ((pRsnaPaePortEntry->aRsnaPwCipherDB[RSNA_TKIP - 1].
		    bPairwiseCipherEnabled) == OSIX_TRUE)
	{
	    pWpsContext->u2ApEncrType = WPS_ENCR_TKIP;
	}

	if ((pRsnaPaePortEntry->aRsnaPwCipherDB[RSNA_CCMP - 1].
		    bPairwiseCipherEnabled) == OSIX_TRUE)
	{
	    pWpsContext->u2ApEncrType = WPS_ENCR_AES;
	}
	WPS_MEMCPY (pWpsContext->au1Psk,pRsnaPaePortEntry->RsnaConfigDB.au1ConfigPsk, RSNA_PSK_LEN);

	pWpsContext->u2ApAuthType = WPS_AUTH_WPA2PSK;
	pWpsContext->u2AuthTypeFlags |= (WPS_AUTH_WPA2PSK|WPS_AUTH_OPEN);
    }

    if (CapwapCheckModuleStatus () == OSIX_FAILURE)
    {
	MemReleaseMemBlock (WPS_CONTEXT_INFO_MEMPOOL_ID,
		(UINT1 *) pWpsContext);
	return SNMP_FAILURE;
    }
    return WPS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : WpsGetMacEntry                                     */
/*                                                                           */
/* Description        : Gets the pointer to the MAC entry from probeReq  table */
/*                                                                           */
/* Input              : macAddr                                              */
/*                                                                           */
/* Output             : ppPortInfo - double Pointer to the port table entry  */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPnacSystemInfo.aPnacPaePortInfo                     */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/

VOID
WpsGetMacEntry (UINT1 *pMacAddr, tWpsProbeReq ** ppProbeInfo)
{
    tWpsProbeReq *pTmpWpsProbeReq = NULL;
    tWpsProbeReq *pProbeReqEntry = NULL;

    if ((pProbeReqEntry = (tWpsProbeReq*) WPS_MEM_ALLOCATE_MEM_BLK
         (WPS_PROBEREQ_MEMPOOL_ID)) == NULL)
    {
        WPS_TRC (WPS_INIT_SHUT_TRC,
                  "\nWpsGetMacEntry:- MemAllocMemBlk" "returned Failure\n");
        return ;
    }

    WPS_MEMSET (pProbeReqEntry, 0, sizeof (tWpsProbeReq));

    WPS_MEMCPY(pProbeReqEntry->au1SuppMacAddr,pMacAddr,ETH_ALEN);

    pTmpWpsProbeReq = RBTreeGet (gWpsGlobals.WpsProbeReqTable,
                              (tRBElem *) pProbeReqEntry);

    *ppProbeInfo = pTmpWpsProbeReq;

    WPS_MEM_RELEASE_MEM_BLK (WPS_PROBEREQ_MEMPOOL_ID, pProbeReqEntry);
    return;
}
/*****************************************************************************/
/* Function Name      : WpsGetPortEntry                                     */
/*                                                                           */
/* Description        : Gets the pointer to the port table entry             */
/*                                                                           */
/* Input              : u2PortNum - Port Number                              */
/*                                                                           */
/* Output             : ppPortInfo - double Pointer to the port table entry  */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/

VOID
WpsGetPortEntry (UINT2 u2PortNum, tWpsContext ** ppPortInfo)
{
    WPS_TRC(MSG_DEBUG, "\nWpsGetPortEntry\n");

    tWpsContext  *pTmpPortInfo  = NULL;
    tWpsContext  *pPortEntry= NULL;

    if ((pPortEntry = (tWpsContext*) WPS_MEM_ALLOCATE_MEM_BLK
         (WPS_CONTEXT_INFO_MEMPOOL_ID)) == NULL)
    {
        WPS_TRC (WPS_INIT_SHUT_TRC,
                  "\nWpsGetPortEntry:- MemAllocMemBlk" "returned Failure\n");
        return ;
    }

    WPS_MEMSET (pPortEntry, 0, sizeof (tWpsContext));

    pPortEntry->u2Port = u2PortNum;

    pTmpPortInfo = RBTreeGet (gWpsGlobals.WpsContextTable,
                              (tRBElem *) pPortEntry);

    *ppPortInfo = pTmpPortInfo;

    WPS_MEM_RELEASE_MEM_BLK (WPS_CONTEXT_INFO_MEMPOOL_ID, pPortEntry);
    return;
}
/*****************************************************************************/
/* Function Name      : WpsDeletePortEntry                                   */
/*                                                                           */
/* Description        : This function removes the entry from                 */
/*                      WpsContextTable                                      */
/*                                                                           */
/* Input(s)           : u4IfIndex                                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : WPS_SUCCESS / WPS_FAILURE                            */
/*****************************************************************************/
INT4
WpsDeletePortEntry (UINT4 u4IfIndex)
{
    WPS_TRC(MSG_DEBUG, "\nWpsDeletePortEntry\n");
    tWpsContext *pWpsContext = NULL;

    WpsGetPortEntry ((UINT2) u4IfIndex, &pWpsContext);
    if (pWpsContext == NULL)
    {
        WPS_TRC (WPS_INIT_SHUT_TRC, "\nWpsGetPortEntry" "returned Failure\n");
        return WPS_FAILURE;
    }

    if (RBTreeRemove (gWpsGlobals.WpsContextTable,
                      pWpsContext) == RB_FAILURE)
    {
        WPS_TRC (WPS_INIT_SHUT_TRC,
                  "\nWpsDeletePortEntry :- RBTreeRemove" "returned Failure\n");
        return WPS_FAILURE;
    }

    MemReleaseMemBlock (WPS_CONTEXT_INFO_MEMPOOL_ID, (UINT1 *) pWpsContext);
    return WPS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : WpsGetFirstPortEntry                                 */
/*                                                                           */
/* Description        : This function returns pointer to the first remote port*/
/*                      entry                                                */
/*                                                                           */
/*                                                                           */
/* Input(s)           : pWpsPortEntry - WPS context pointer for         */
/*                      which next node should be returned                   */
/*                                                                           */
/* Output(s)          : pNextWpsPortEntry - pointer to the next Node        */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
WpsGetFirstPortEntry (tWpsContext ** pWpsPortEntry)
{
    WPS_TRC(MSG_DEBUG, "\nWpsGetFirstPortEntry\n");
    tWpsContext *pTmpWpsPortEntry = NULL;

    pTmpWpsPortEntry = (tWpsContext*) RBTreeGetFirst
        (gWpsGlobals.WpsContextTable);

    *pWpsPortEntry = pTmpWpsPortEntry;
    return;
}

/*****************************************************************************/
/* Function Name      : WpsGetNextPortEntry                                 */
/*                                                                           */
/* Description        : This function returns pointer to the next remote port*/
/*                      entry                                                */
/*                      If this function called with NULL as pWpsPortEntry  */
/*                      then, pointer to first node is returned as next node */
/*                                                                           */
/*                                                                           */
/* Input(s)           : pWpsPortEntry - WPS context pointer for         */
/*                      which next node should be returned                   */
/*                                                                           */
/* Output(s)          : pNextWpsPortEntry - pointer to the next Node        */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
WpsGetNextPortEntry (tWpsContext* pWpsPortEntry,
                      tWpsContext** pNextWpsPortEntry)
{
    WPS_TRC(MSG_DEBUG, "\nWpsGetNextPortEntry\n");
    tWpsContext *pTmpWpsPortEntry = NULL;

    if (pWpsPortEntry == NULL)
    {

        pTmpWpsPortEntry = (tWpsContext*) RBTreeGetFirst
            (gWpsGlobals.WpsContextTable);
    }
    else
    {

        pTmpWpsPortEntry = (tWpsContext*) RBTreeGetNext
            (gWpsGlobals.WpsContextTable, (tRBElem *) pWpsPortEntry, NULL);
    }

    *pNextWpsPortEntry = pTmpWpsPortEntry;
    return;

}
/****************************************************************************
 * *                                                                           *
 * * Function     : WpsUtilGetStaInfoFromStaAddr                              *
 * *                                                                           *
 * * Description  : Function to get station info from mac address              *
 * *                                                                           *
 * * Input        : pWpsContextinfo :- Pointer to Wps context              *
 * *                pu1Addr          :- Refers to station mac address          *
 * *                                                                           *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : tRsnaSupInfo                                               * 
 * *                                                                           *
 * *****************************************************************************/
tWpsSessionNode *
WpsUtilGetStaInfoFromStaAddr (tWpsContext* pWpsContextinfo,
                               UINT1 *pu1Addr)
{

    WPS_TRC(MSG_DEBUG, "\nWpsUtilGetStaInfoFromStaAddr\n");
    tWpsSessionNode *pWpsSessionNode= NULL;

    TMO_SLL_Scan (&(pWpsContextinfo->wpsStationInfoList),
                  pWpsSessionNode, tWpsSessionNode*)
    {

        if (MEMCMP (pWpsSessionNode->au1SuppMacAddr, pu1Addr, ETH_ALEN)
            == 0)
        {
            return (pWpsSessionNode);
        }
    }
    return (NULL);
}

/****************************************************************************
 * *                                                                           *
 * * Function     : WpsUtilCreateAndAddStaToList                              *
 * *                                                                           *
 * * Description  : Function to create and station to the list                 *
 * *                                                                           *
 * * Input        : pWpsContextInfo :- Pointer to Wps context              *
 * *                pu1SuppAddr :- Refers to the supplicant address            *
 * *                                                                           *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : tRsnaSupInfo                                               * 
 * *                                                                           *
 * *****************************************************************************/
tWpsSessionNode *
WpsUtilCreateAndAddStaToList (tWpsContext* pWpsContextInfo,
                               UINT1 *pu1SuppAddr)
{
    WPS_TRC(MSG_DEBUG, "\nWpsUtilCreateAndAddStaToList\n");

    tWpsSessionNode *pWpsSessionNode= NULL;
    UINT1 macIndex = 0;
    
    if ((pWpsSessionNode = WPS_MEM_ALLOCATE_MEM_BLK (WPS_DATA_INFO_MEMPOOL_ID))
        == NULL)
    {
        WPS_TRC (WPS_CONTROL_PATH_TRC | WPS_ALL_FAILURE_TRC,
                  "\nWpsUtilCreateAndAddStaToList:- MemAllocMemBlk Failed \n");
        return (NULL);
    }

    WPS_MEMSET (pWpsSessionNode, 0, sizeof (tWpsSessionNode));

    if ((pWpsSessionNode->pWpsParseAttr = WPS_MEM_ALLOCATE_MEM_BLK (WPS_PARSE_ATTR_INFO_MEMPOOL_ID))
        == NULL)
    {
	WPS_TRC (WPS_CONTROL_PATH_TRC | WPS_ALL_FAILURE_TRC,
		"\nWpsSessHandleNewStation:- MemAllocMemBlk Failed \n");
	WPS_MEM_RELEASE_MEM_BLK (WPS_DATA_INFO_MEMPOOL_ID,
		pWpsSessionNode);
	return (NULL);
    }

    WPS_MEMSET (pWpsSessionNode->pWpsParseAttr, 0, sizeof (tWpsParseAttr));
    WPS_MEMCPY (pWpsSessionNode->au1SuppMacAddr, pu1SuppAddr, ETH_ALEN);
    pWpsSessionNode->pWpsContext = pWpsContextInfo;
    pWpsSessionNode->pWpsContext->u1noOfAuthMac++;
    macIndex = pWpsSessionNode->pWpsContext->u1noOfAuthMac % WPS_MAX_AUTHORIZED_MACS;
    if (pWpsSessionNode->pWpsContext->u1noOfAuthMac >= WPS_MAX_AUTHORIZED_MACS)
    {
	pWpsSessionNode->pWpsContext->u1noOfAuthMac = WPS_MAX_AUTHORIZED_MACS;
    }
    
    WPS_MEMCPY (pWpsSessionNode->pWpsContext->u1AuthorizedMacs[macIndex], pu1SuppAddr, ETH_ALEN);
    WPS_MEMCPY (pWpsSessionNode->au1SuppMacAddr, pu1SuppAddr, ETH_ALEN);
  

    TMO_SLL_Add (&(pWpsContextInfo->wpsStationInfoList),
                 (tTMO_SLL_NODE *) pWpsSessionNode);
    return (pWpsSessionNode);
}
/****************************************************************************
 * *                                                                           *
 * * Function     : WpsSessHandleNewStation*
 * *                                                                           *
 * * Description  : Function to allocate memory for new station                *
 * *                                                                           *
 * * Input        : pWssWpsNotifyParams:- Pointer to WPS msg from Apme         *
 * *                                                                           *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : WPS_FAILURE/WPS_SUCCESS                                    *
 * *                                                                           *
 * *****************************************************************************/

UINT1
WpsSessHandleNewStation
            (tWpsContext *pWpsPaePortInfo, tWpsSessionNode *pWpsDataInfo,UINT4 u4BssIfIndex)
{
    WPS_TRC(MSG_DEBUG, "\nWpsSessHandleNewStation\n");
    UNUSED_PARAM(pWpsPaePortInfo);
    UNUSED_PARAM(u4BssIfIndex);
    tWpsParseAttr *pWpsParseAttr;
    if ((pWpsParseAttr = WPS_MEM_ALLOCATE_MEM_BLK (WPS_PARSE_ATTR_INFO_MEMPOOL_ID))
        == NULL)
    {
        WPS_TRC (WPS_CONTROL_PATH_TRC | WPS_ALL_FAILURE_TRC,
                  "\nWpsSessHandleNewStation:- MemAllocMemBlk Failed \n");
        return WPS_FAILURE;
    }

    WPS_MEMSET (pWpsParseAttr, 0, sizeof (tWpsParseAttr));
    pWpsDataInfo->pWpsParseAttr = pWpsParseAttr;
    return WPS_SUCCESS;
}
/****************************************************************************
 * *                                                                           *
 * * Function     : WpsProcDisAssocInd                                            *
 * *                                                                           *
 * * Description  : Function to Process Mlme DisAssoc Indication                  *
 * *                                                                           *
 * * Input        : pWssWpsNotifyParams:- Pointer to WPS msg from Apme         *
 * *                                                                           *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : None                                                       *
 * *                                                                           *
 * *****************************************************************************/

UINT1 
WpsProcDisAssocInd (tWssWpsNotifyParams * pWssWpsNotifyParams)
{
    WPS_TRC(MSG_DEBUG, "\nWpsProcDisAssocInd\n");
    tWpsContext *pWpsPaePortInfo = NULL;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tWpsSessionNode    *pWpsDataInfo = NULL;
    UINT1               au1StaAddr[] = { 0xff, 0xff, 0xff, 0xff, 0xff, 0xff };
    UINT4               u4ProfileIndex = 0;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    if (WssIfGetProfileIfIndex (pWssWpsNotifyParams->u4IfIndex,
		&u4ProfileIndex) == OSIX_FAILURE)
    {
	WPS_TRC (ALL_FAILURE_TRC,
		"\nWpsProcDisAssocInd :- WssIfGetProfileIfIndex"
		"returned Failure\n");
	WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
	WPS_MEM_RELEASE_MEM_BLK (WPS_WSS_INFO_MEMPOOL_ID, pWssWpsNotifyParams);
	return WPS_FAILURE;
    }

    WpsGetPortEntry ((UINT2) u4ProfileIndex, &pWpsPaePortInfo);

    if (pWpsPaePortInfo == NULL)
    {
	WPS_TRC (ALL_FAILURE_TRC, "\nWpsProcDisAssocInd:-"
		"No WPS entry for the given port");
	WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
	WPS_MEM_RELEASE_MEM_BLK (WPS_WSS_INFO_MEMPOOL_ID, pWssWpsNotifyParams);
	return WPS_SUCCESS;
    }

    WPS_MEMCPY (au1StaAddr,
	    pWssWpsNotifyParams->WPSDISSOCIND.au1StaMac, ETH_ALEN);

    WPS_TRC_ARG6 (WPS_CONTROL_PATH_TRC | WPS_ALL_FAILURE_TRC,
	    " iWpsProcDisAssocInd: Received DisAssoc Indication from Station MAC:%x:%x:%x:%x:%x:%x\n",
	    au1StaAddr[0],
	    au1StaAddr[1],
	    au1StaAddr[2], au1StaAddr[3], au1StaAddr[4], au1StaAddr[5]);

    pWpsDataInfo = WpsUtilGetStaInfoFromStaAddr (pWpsPaePortInfo, au1StaAddr);

    if (pWpsDataInfo != NULL)
    {
        //wpsDeleteSession(pWpsDataInfo);
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return WPS_SUCCESS;
}
/****************************************************************************
 * *                                                                           *
 * * Function     : WpsProcAssocInd                                            *
 * *                                                                           *
 * * Description  : Function to Process Mlme Assoc Indication                  *
 * *                                                                           *
 * * Input        : pWssWpsNotifyParams:- Pointer to WPS msg from Apme         *
 * *                                                                           *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : None                                                       *
 * *                                                                           *
 * *****************************************************************************/

UINT1
WpsProcAssocInd (tWssWpsNotifyParams * pWssWpsNotifyParams)
{
    WPS_TRC(MSG_DEBUG, "\nWpsProcAssocInd\n");
    tWpsContext *pWpsPaePortInfo = NULL;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tWpsProbeReq *pWpsProbeReq= NULL;
    tWpsSessionNode    *pWpsDataInfo = NULL;
    UINT1               au1StaAddr[] = { 0xff, 0xff, 0xff, 0xff, 0xff, 0xff };
    UINT4               u4ProfileIndex = 0;
    UINT4               u4BssIfIndex = 0;
    tPnacAuthSessionNode *pAuthSessNode = NULL;
    UINT1               u2Index = 0;

    u4BssIfIndex = pWssWpsNotifyParams->u4IfIndex;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
	MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    if (WssIfGetProfileIfIndex (pWssWpsNotifyParams->u4IfIndex,
		&u4ProfileIndex) == OSIX_FAILURE)
    {
	WPS_TRC (ALL_FAILURE_TRC,
		"\nWpsProcAssocInd:- WssIfGetProfileIfIndex"
		"returned Failure\n");
	WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
	WPS_MEM_RELEASE_MEM_BLK (WPS_WSS_INFO_MEMPOOL_ID, pWssWpsNotifyParams);
        return WPS_SUCCESS;
    }

    WpsGetPortEntry ((UINT2) u4ProfileIndex, &pWpsPaePortInfo);

    if (pWpsPaePortInfo == NULL)
    {
	WPS_TRC (ALL_FAILURE_TRC, "\nWpsProcessMlmeAssocInd:-"
		"No WPS entry for the given port\n");
	WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
	WPS_MEM_RELEASE_MEM_BLK (WPS_WSS_INFO_MEMPOOL_ID, pWssWpsNotifyParams);
        return WPS_SUCCESS;
    }

    WPS_MEMCPY (au1StaAddr,
	    pWssWpsNotifyParams->WPSASSOCIND.au1StaMac, ETH_ALEN);

    WPS_TRC_ARG6 (WPS_CONTROL_PATH_TRC | WPS_ALL_FAILURE_TRC,
	    " Received Assoc Indication from Station MAC:%x:%x:%x:%x:%x:%x\n",
	    au1StaAddr[0],
	    au1StaAddr[1],
	    au1StaAddr[2], au1StaAddr[3], au1StaAddr[4], au1StaAddr[5]);

    WpsGetMacEntry(au1StaAddr,&pWpsProbeReq);

    if (pWpsProbeReq == NULL)
    {
	WPS_TRC (ALL_FAILURE_TRC, "\nWpsProcessMlmeAssocInd:-"
		"No  entry for the given MAC address\n");
	if (pWssWpsNotifyParams->WPSASSOCIND.bWpsPresent != OSIX_TRUE)
	{
	    if((pWssWpsNotifyParams->WPSASSOCIND.bRsnaPresent == OSIX_TRUE) || 
		(pWpsPaePortInfo->u2ApAuthType == WPS_WSS_AUTH_OPEN))
		{
		    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
		    WPS_MEM_RELEASE_MEM_BLK (WPS_WSS_INFO_MEMPOOL_ID, pWssWpsNotifyParams);
		    return WPS_SUCCESS;
		}
               else
		{
			WPS_TRC_ARG2 (ALL_FAILURE_TRC, " inside RSNA = %d, authtype = %d", pWssWpsNotifyParams->WPSASSOCIND.bRsnaPresent,pWpsPaePortInfo->u2ApAuthType);
		}
	}
	else
			WPS_TRC_ARG2 (ALL_FAILURE_TRC, " outside RSNA = %d, authtype = %d", pWssWpsNotifyParams->WPSASSOCIND.bRsnaPresent,pWpsPaePortInfo->u2ApAuthType);

    }

    pWpsDataInfo = WpsUtilGetStaInfoFromStaAddr (pWpsPaePortInfo, au1StaAddr);

    if (pWpsDataInfo == NULL)
    {
	pWpsDataInfo =
	    WpsUtilCreateAndAddStaToList (pWpsPaePortInfo, au1StaAddr);
	if (pWpsDataInfo == NULL)
	{
	    WPS_TRC (ALL_FAILURE_TRC, "\nWpsProcessMlmeAssocInd:-"
		    "WpsUtilCreateAndAddStaToList returned Failure \n");
	    WPS_MEM_RELEASE_MEM_BLK (WPS_WSS_INFO_MEMPOOL_ID, pWssWpsNotifyParams);
	    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
	    goto sendDeAuth;
	}
	gWpsGlobals.u4WpsStatsIndex++;
	gWpsGlobals.u4WpsStatsIndex = gWpsGlobals.u4WpsStatsIndex % WPS_MAX_SUPP; 
	WpsUtilAddStaToStatsTable(pWpsPaePortInfo->u2Port,gWpsGlobals.u4WpsStatsIndex,au1StaAddr);
    }

    else
    {
        if (pWpsDataInfo->e1state == RECV_DONE)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            WPS_MEM_RELEASE_MEM_BLK (WPS_WSS_INFO_MEMPOOL_ID, pWssWpsNotifyParams);
            return WPS_SUCCESS;
        }
    }
    if (pWpsProbeReq != NULL)
    {
	WPS_MEMCPY(pWpsDataInfo->pWpsParseAttr->au1ConfigMethods,
                   pWpsProbeReq->au1ConfigMethods,2);
	WPS_MEMCPY(pWpsDataInfo->au1uuidE,pWpsProbeReq->au1uuidE,WPS_UUID_LEN);
	pWpsDataInfo->pWpsParseAttr->u2DevPasswordId = pWpsProbeReq->u2SuppDeviePwd;
	pWpsDataInfo->b1Pbc = pWpsProbeReq->b1Pbc;
	pWpsProbeReq->pWpsSessionNode = pWpsDataInfo;
	pWpsDataInfo->pWpsProbeReq = pWpsProbeReq;
	pWpsDataInfo->pWpsParseAttr->u1RequestToEnroll = (UINT1)pWpsProbeReq->b1ReqToEnroll;
	pWpsDataInfo->u2SessId = pWssWpsNotifyParams->WPSASSOCIND.u2SessId;
    }
    pWpsDataInfo->u2WtpInternalId = pWssWpsNotifyParams->WPSASSOCIND.u2WtpId;

    WPS_MEMCPY(pWpsDataInfo->pWpsParseAttr->au1uuidR,pWpsDataInfo->pWpsContext->au1uuid,WPS_NONCE_LEN);
    WpsUtilGetRandom(pWpsDataInfo->pWpsParseAttr->au1RegistrarNonce,WPS_NONCE_LEN);

    pWpsDataInfo->u4SuppState = WPS_STA_ASSOCIATED;
    WPS_MEMCPY (pWpsDataInfo->pWpsContext->au1SrcMac,
	    pWssWpsNotifyParams->WPSASSOCIND.au1ApMac, ETH_ALEN);

    pWssIfCapwapDB->CapwapGetDB.u1RadioId = pWssWpsNotifyParams->WPSASSOCIND.u1RadioId;
    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = pWssWpsNotifyParams->WPSASSOCIND.u2WtpId;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpDescriptor = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bProfileName = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpModelNumber = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpSerialNumber = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpName = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bNoOfRadio = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpRadioType =OSIX_TRUE;

    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
		pWssIfCapwapDB) !=
	    OSIX_SUCCESS)
    {
	    WPS_TRC (ALL_FAILURE_TRC, "\nWpsProcAssocInd:- : WlcHdlr DB \
		access failed. \r\n");
	WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
	WPS_MEM_RELEASE_MEM_BLK (WPS_WSS_INFO_MEMPOOL_ID, pWssWpsNotifyParams);
	WPS_MEM_RELEASE_MEM_BLK (WPS_DATA_INFO_MEMPOOL_ID,
		pWpsDataInfo);
	WPS_MEM_RELEASE_MEM_BLK (WPS_PARSE_ATTR_INFO_MEMPOOL_ID,
		pWpsDataInfo->pWpsParseAttr);
	    goto sendDeAuth;
    }
    STRCPY(pWpsDataInfo->au1DeviceName,pWssIfCapwapDB->CapwapGetDB.au1ProfileName);
    STRCPY(pWpsDataInfo->au1ModelName,pWssIfCapwapDB->CapwapGetDB.au1WtpName);
    STRCPY(pWpsDataInfo->au1ModelNumber,pWssIfCapwapDB->CapwapGetDB.au1WtpModelNumber);
    STRCPY(pWpsDataInfo->au1SerialNumber,pWssIfCapwapDB->CapwapGetDB.au1WtpSerialNumber);
    for (u2Index = 0;
	    u2Index < pWssIfCapwapDB->CapwapGetDB.u1WtpNoofRadio;
	    u2Index++) 
    {
	pWpsDataInfo->u1RfBands |= (UINT1)pWssIfCapwapDB->CapwapGetDB.au4WtpRadioType[u2Index];
    }
    WPS_MEMCPY(pWpsDataInfo->au1OsVersion,pWssIfCapwapDB->CapwapGetDB.au1WtpBootversion,STRLEN(pWssIfCapwapDB->CapwapGetDB.au1WtpBootversion));
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);

    if (PnacSnmpLowGetAuthSessionNode (pWpsDataInfo->au1SuppMacAddr,
		&pAuthSessNode) == PNAC_FAILURE)
    {
	pAuthSessNode = WpsSessCreatePnacAuthSess (pWpsDataInfo->au1SuppMacAddr,(UINT2) u4BssIfIndex,TRUE);
    }
    if (pAuthSessNode == NULL)
    {
	WPS_TRC (WPS_CONTROL_PATH_TRC | WPS_ALL_FAILURE_TRC,
		" AuthCreateSession returned failure\n");
	WPS_MEM_RELEASE_MEM_BLK (WPS_WSS_INFO_MEMPOOL_ID, pWssWpsNotifyParams);
	wpsDeleteSession(pWpsDataInfo);
	goto sendDeAuth;
    }
    WPS_MEM_RELEASE_MEM_BLK (WPS_WSS_INFO_MEMPOOL_ID, pWssWpsNotifyParams);
    return WPS_SUCCESS;
sendDeAuth:
    WssWpsSendDeAuthMsg (pWssWpsNotifyParams->WPSASSOCIND.au1ApMac,
	    pWssWpsNotifyParams->WPSASSOCIND.au1StaMac,
	    pWssWpsNotifyParams->WPSASSOCIND.u2SessId);
    return WPS_SUCCESS;
}
/****************************************************************************
 * *                                                                           *
 * * Function     : WpsProcPnacMsgInd*
 * *                                                                           *
 * * Description  : Function to Process pnac msg Indication                  *
 * *                                                                           *
 * * Input        : pWssWpsNotifyParams:- Pointer to WPS msg from Apme         *
 * *                                                                           *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : None                                                       *
 * *                                                                           *
 * *****************************************************************************/

VOID
WpsProcPnacMsgInd (tWssWpsNotifyParams * pWssWpsNotifyParams)
{
    WPS_TRC(MSG_DEBUG, "\nWpsProcPnacMsgInd\n");

    tWpsProbeReq *pWpsProbeReq= NULL;
    UINT1               au1StaAddr[] = { 0xff, 0xff, 0xff, 0xff, 0xff, 0xff };

    WPS_MEMCPY (au1StaAddr,
	    pWssWpsNotifyParams->WPSPNACIND.au1StaMac, ETH_ALEN);

    WPS_TRC_ARG6 (WPS_CONTROL_PATH_TRC | WPS_ALL_FAILURE_TRC,
	    " Received Assoc Indication from Station MAC:%x:%x:%x:%x:%x:%x\n",
	    au1StaAddr[0],
	    au1StaAddr[1],
	    au1StaAddr[2], au1StaAddr[3], au1StaAddr[4], au1StaAddr[5]);

    WpsGetMacEntry(au1StaAddr,&pWpsProbeReq);
    if (pWpsProbeReq == NULL)
    {
	WPS_TRC (ALL_FAILURE_TRC, "\nWpsProcPnacMsgInd:-"
		" MAC address not present\n");
    }
    WpsProcessEap(pWssWpsNotifyParams->WPSPNACIND.pu1PktPtr, 
                  pWssWpsNotifyParams->WPSPNACIND.u2PktLen, 
                  pWssWpsNotifyParams->WPSPNACIND.u2PortNum, 
                  au1StaAddr);
}
/****************************************************************************
 * *                                                                           *
 * * Function     : WpsProcPnacReqInd*
 * *                                                                           *
 * * Description  : Function to Process pnac identity request Indication                  *
 * *                                                                           *
 * * Input        : pWssWpsNotifyParams:- Pointer to WPS msg from Apme         *
 * *                                                                           *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : None                                                       *
 * *                                                                           *
 * *****************************************************************************/

VOID
WpsProcPnacReqInd (tWssWpsNotifyParams * pWssWpsNotifyParams)
{
    WPS_TRC(MSG_DEBUG, "\nWpsProcPnacReqInd\n");

    tWpsProbeReq *pWpsProbeReq= NULL;
    UINT1               au1StaAddr[] = { 0xff, 0xff, 0xff, 0xff, 0xff, 0xff };

    WPS_MEMCPY (au1StaAddr,
	    pWssWpsNotifyParams->WPSPNACIND.au1StaMac, ETH_ALEN);

    WPS_TRC_ARG6 (WPS_CONTROL_PATH_TRC | WPS_ALL_FAILURE_TRC,
	    " Received Assoc Indication from Station MAC:%x:%x:%x:%x:%x:%x\n",
	    au1StaAddr[0],
	    au1StaAddr[1],
	    au1StaAddr[2], au1StaAddr[3], au1StaAddr[4], au1StaAddr[5]);

    WpsGetMacEntry(au1StaAddr,&pWpsProbeReq);
    if (pWpsProbeReq == NULL)
    {
	WPS_TRC (ALL_FAILURE_TRC, "\nWpsProcPnacReqInd:-"
		" MAC address not present\n");
    }
    wpsEapSendStart(au1StaAddr,pWssWpsNotifyParams->WPSPNACIND.u2PortNum);
}
    
/****************************************************************************
 * *                                                                           *
 * * Function     : WpsProcProbeReqInd*
 * *                                                                           *
 * * Description  : Function to Process Mlme probeReq Indication                  *
 * *                                                                           *
 * * Input        : pWssWpsNotifyParams:- Pointer to WPS msg from Apme         *
 * *                                                                           *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : None                                                       *
 * *                                                                           *
 * *****************************************************************************/

VOID
WpsProcProbeReqInd (tWssWpsNotifyParams * pWssWpsNotifyParams)
{

    WPS_TRC(MSG_DEBUG, "\nWpsProcProbeReqInd\n");
    tWpsProbeReq *pWpsProbeReq= NULL;
    UINT1               au1StaAddr[] = { 0xff, 0xff, 0xff, 0xff, 0xff, 0xff };


    WPS_MEMCPY (au1StaAddr,
	    pWssWpsNotifyParams->WPSPROBEREQIND.au1StaMac, ETH_ALEN);

    WPS_TRC_ARG6 (WPS_CONTROL_PATH_TRC | WPS_ALL_FAILURE_TRC,
	    " Received Assoc Indication from Station MAC:%x:%x:%x:%x:%x:%x\n",
	    au1StaAddr[0],
	    au1StaAddr[1],
	    au1StaAddr[2], au1StaAddr[3], au1StaAddr[4], au1StaAddr[5]);

    if (pWssWpsNotifyParams->WPSPROBEREQIND.request_to_enroll != 1)
    {
	WPS_TRC (WPS_CONTROL_PATH_TRC, "\nWpsProcProbeReqInd:-"
		" wps is not requested\n");
	WPS_MEM_RELEASE_MEM_BLK (WPS_WSS_INFO_MEMPOOL_ID, pWssWpsNotifyParams);
	return;
    }
    WpsGetMacEntry(au1StaAddr,&pWpsProbeReq);

    if (pWpsProbeReq != NULL)
    {
	WPS_TRC (ALL_FAILURE_TRC, "\nWpsProcProbeReqInd:-"
		" MAC address already present\n");
	WPS_MEM_RELEASE_MEM_BLK (WPS_WSS_INFO_MEMPOOL_ID, pWssWpsNotifyParams);
	return;
    }
    if ((pWpsProbeReq = (tWpsProbeReq*) WPS_MEM_ALLOCATE_MEM_BLK
		(WPS_PROBEREQ_MEMPOOL_ID)) == NULL)
    {
	WPS_TRC (WPS_INIT_SHUT_TRC,
		"\nWpsProcProbeReqInd:- MemAllocMemBlk" "returned Failure\n");
	WPS_MEM_RELEASE_MEM_BLK (WPS_WSS_INFO_MEMPOOL_ID, pWssWpsNotifyParams);
	return; 
    }
    memset(pWpsProbeReq,0,sizeof(tWpsProbeReq));

    WPS_MEMCPY (pWpsProbeReq->au1SuppMacAddr,
	    pWssWpsNotifyParams->WPSPROBEREQIND.au1StaMac, ETH_ALEN);
    WPS_MEMCPY(pWpsProbeReq->au1uuidE,pWssWpsNotifyParams->WPSPROBEREQIND.uuid_e,16);
    pWpsProbeReq->u2SuppDeviePwd = pWssWpsNotifyParams->WPSPROBEREQIND.dev_password_id;
    pWpsProbeReq->b1ReqToEnroll = pWssWpsNotifyParams->WPSPROBEREQIND.request_to_enroll;
    pWpsProbeReq->pWpsSessionNode = NULL;
    if (pWpsProbeReq->u2SuppDeviePwd == DEV_PW_PUSHBUTTON)
    {
	pWpsProbeReq->b1Pbc = 1;
    }
    if (RBTreeAdd (gWpsGlobals.WpsProbeReqTable, pWpsProbeReq)
	    == RB_FAILURE)
    {

	WPS_TRC (WPS_INIT_SHUT_TRC,
		"\nWpsProcProbeReqInd:- RBTreeAdd" "returned Failure\n");
	MemReleaseMemBlock (WPS_PROBEREQ_MEMPOOL_ID,
		(UINT1 *) pWpsProbeReq);
	WPS_MEM_RELEASE_MEM_BLK (WPS_WSS_INFO_MEMPOOL_ID, pWssWpsNotifyParams);
	return;
    }
    WpsTmrStartTimer(&pWpsProbeReq->WpsSessionTmr,WPS_SESSION_TIMER_ID,
	    WPS_SESSION_TIME,pWpsProbeReq,NUM_OF_AP_SUPPORTED);
    WPS_MEM_RELEASE_MEM_BLK (WPS_WSS_INFO_MEMPOOL_ID, pWssWpsNotifyParams);
    return;
}
/****************************************************************************
 * *                                                                           *
 * * Function     : WpsProcPbcInd                                            *
 * *                                                                           *
 * * Description  : Function to Process Mlme PBC Indication                  *
 * *                                                                           *
 * * Input        : pWssWpsNotifyParams:- Pointer to WPS msg from Apme         *
 * *                                                                           *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : None                                                       *
 * *                                                                           *
 * *****************************************************************************/

UINT1 
WpsProcPbcInd (tWssWpsNotifyParams * pWssWpsNotifyParams)
{
    WPS_TRC(MSG_DEBUG, "\nWpsProcPbcInd\n");
    tWpsContext *pWpsContext = NULL;
    UINT4       u4ProfileId = 0;
    UINT2       u2Index = 0;

    if (nmhGetCapwapDot11WlanProfileIfIndex
	    ((UINT4)pWssWpsNotifyParams->WPSPBCIND.WlanId, ((INT4 *) &u4ProfileId)) != SNMP_SUCCESS)
    {
	WPS_TRC (ALL_FAILURE_TRC, "\nWpsProcessPbcInd:-"
		"No WPS entry for the given port,could not enable pushbutton\n");
	return WPS_FAILURE;
    }

    WpsGetPortEntry ((UINT2) u4ProfileId, &pWpsContext);

    if (pWpsContext == NULL)
    {
	WPS_TRC (ALL_FAILURE_TRC, "\nWpsProcessPbcInd:-"
		"No WPS entry for the given port,could not enable pushbutton\n");
	return WPS_FAILURE;
    }
    for (u2Index = 0; u2Index < NUM_OF_AP_SUPPORTED; u2Index++)
    {
	if (pWssWpsNotifyParams->WPSPBCIND.u2WtpInternalId == pWpsContext->pWpsConfigDb[u2Index].u2WtpInternalId)
	{
	    pWpsContext->pWpsConfigDb[u2Index].u4PushButtonStatus =
		WPS_PBC_INPROGRESS;
	    pWpsContext->pWpsConfigDb[u2Index].u2ConfigMethods =
		WPS_CONFIG_PUSHBUTTON;
	    break;
	}
    }
    WpsTmrStartTimer(&pWpsContext->WpsPbcTmr, WPS_PBC_TIMER_ID, 
	    WPS_PBC_TIME, pWpsContext,pWssWpsNotifyParams->WPSPBCIND.u2WtpInternalId);
    if (pWpsContext->pWpsConfigDb[u2Index].b1WpsEnabled != WPS_ENABLED)
    {
	if (nmhSetFsWPSSetStatus((INT4) u4ProfileId, WPS_ALL_AP,
		    WPS_ENABLED) != SNMP_SUCCESS)
	{
	    return CLI_FAILURE;
	}
    }
    return WPS_SUCCESS;
}
