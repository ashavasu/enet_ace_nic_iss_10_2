/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: capwapcliwrg.c,v 1.3.2.1 2018/03/21 12:57:26 siva Exp $       
*
* Description: This file contains the wrapperfunctions for Capwap 
*********************************************************************/

#include "lr.h"
#include "fssnmp.h"
#include "capwapclitdfsg.h"
#include "capwapclitdfs.h"
#include "capwapclilwg.h"
#include "capwapcliwrg.h"
#include "capwapcliprotg.h"
#include "capwapcliprot.h"
#include "capwapclitmr.h"
#include "stdwtpdb.h"
#include "fscapwdb.h"

VOID
RegisterSTDWTP ()
{
    SNMPRegisterMibWithLock (&stdwtpOID, &stdwtpEntry, CapwapMainTaskLock,
                             CapwapMainTaskUnLock, SNMP_MSR_TGR_TRUE);

    SNMPAddSysorEntry (&stdwtpOID, (const UINT1 *) "stdwtp");
}

VOID
UnRegisterSTDWTP ()
{
    SNMPUnRegisterMib (&stdwtpOID, &stdwtpEntry);
    SNMPDelSysorEntry (&stdwtpOID, (const UINT1 *) "stdwtp");
}

VOID
RegisterFSCAPW ()
{
    SNMPRegisterMibWithLock (&fscapwOID, &fscapwEntry, CapwapMainTaskLock,
                             CapwapMainTaskUnLock, SNMP_MSR_TGR_TRUE);

    SNMPAddSysorEntry (&fscapwOID, (const UINT1 *) "fscapw");
}

VOID
UnRegisterFSCAPW ()
{
    SNMPUnRegisterMib (&fscapwOID, &fscapwEntry);
    SNMPDelSysorEntry (&fscapwOID, (const UINT1 *) "fscapw");
}

INT4
GetNextIndexCapwapBaseAcNameListTable (tSnmpIndex * pFirstMultiIndex,
                                       tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexCapwapBaseAcNameListTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexCapwapBaseAcNameListTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
GetNextIndexCapwapBaseMacAclTable (tSnmpIndex * pFirstMultiIndex,
                                   tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexCapwapBaseMacAclTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexCapwapBaseMacAclTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
GetNextIndexCapwapBaseWtpProfileTable (tSnmpIndex * pFirstMultiIndex,
                                       tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexCapwapBaseWtpProfileTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexCapwapBaseWtpProfileTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
GetNextIndexCapwapBaseWtpStateTable (tSnmpIndex * pFirstMultiIndex,
                                     tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexCapwapBaseWtpStateTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexCapwapBaseWtpStateTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
GetNextIndexCapwapBaseWtpTable (tSnmpIndex * pFirstMultiIndex,
                                tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexCapwapBaseWtpTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexCapwapBaseWtpTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
GetNextIndexCapwapBaseWirelessBindingTable (tSnmpIndex * pFirstMultiIndex,
                                            tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        /* Accessing the proprietary MIB DB, as code creates/manages only the
         * RB tree for proprietary MIB. Both have same entries & indices */
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        /* Accessing the proprietary MIB DB, as code creates/manages only the
         * RB tree for proprietary MIB. Both have same entries & indices */
        if (nmhGetNextIndexFsCapwapWirelessBindingTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
GetNextIndexCapwapBaseStationTable (tSnmpIndex * pFirstMultiIndex,
                                    tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexCapwapBaseStationTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexCapwapBaseStationTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
GetNextIndexCapwapBaseWtpEventsStatsTable (tSnmpIndex * pFirstMultiIndex,
                                           tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexCapwapBaseWtpEventsStatsTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexCapwapBaseWtpEventsStatsTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
GetNextIndexCapwapBaseRadioEventsStatsTable (tSnmpIndex * pFirstMultiIndex,
                                             tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexCapwapBaseRadioEventsStatsTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexCapwapBaseRadioEventsStatsTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue,
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsWtpModelTable (tSnmpIndex * pFirstMultiIndex,
                             tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsWtpModelTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsWtpModelTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsWtpRadioTable (tSnmpIndex * pFirstMultiIndex,
                             tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsWtpRadioTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsWtpRadioTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue,
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsWtpExtModelTable (tSnmpIndex * pFirstMultiIndex,
                                tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsWtpExtModelTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsWtpExtModelTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsWtpModelWlanBindingTable (tSnmpIndex * pFirstMultiIndex,
                                        tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsWtpModelWlanBindingTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsWtpModelWlanBindingTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue,
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsCapwapWhiteListTable (tSnmpIndex * pFirstMultiIndex,
                                    tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWhiteListTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsCapwapWhiteListTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsCapwapBlackListTable (tSnmpIndex * pFirstMultiIndex,
                                    tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsCapwapBlackList
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsCapwapBlackList
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsCapwapWtpConfigTable (tSnmpIndex * pFirstMultiIndex,
                                    tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWtpConfigTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsCapwapWtpConfigTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsCapwapExtWtpConfigTable (tSnmpIndex * pFirstMultiIndex,
                                       tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsCapwapExtWtpConfigTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsCapwapExtWtpConfigTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsCapwapLinkEncryptionTable (tSnmpIndex * pFirstMultiIndex,
                                         tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsCapwapLinkEncryptionTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsCapwapLinkEncryptionTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsCapwapDefaultWtpProfileTable (tSnmpIndex * pFirstMultiIndex,
                                            tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsCawapDefaultWtpProfileTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsCawapDefaultWtpProfileTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsCapwapDnsProfileTable (tSnmpIndex * pFirstMultiIndex,
                                     tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsCapwapDnsProfileTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsCapwapDnsProfileTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsWtpNativeVlanIdTable (tSnmpIndex * pFirstMultiIndex,
                                    tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsWtpNativeVlanIdTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsWtpNativeVlanIdTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsWtpLocalRoutingTable (tSnmpIndex * pFirstMultiIndex,
                                    tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsWtpLocalRoutingTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsWtpLocalRoutingTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsCapwapFsAcTimestampTriggerTable (tSnmpIndex * pFirstMultiIndex,
                                               tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsCapwapFsAcTimestampTriggerTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsCapwapFsAcTimestampTriggerTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsCawapDiscStatsTable (tSnmpIndex * pFirstMultiIndex,
                                   tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsCawapDiscStatsTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsCawapDiscStatsTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsCawapJoinStatsTable (tSnmpIndex * pFirstMultiIndex,
                                   tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsCawapJoinStatsTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsCawapJoinStatsTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsCawapConfigStatsTable (tSnmpIndex * pFirstMultiIndex,
                                     tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsCawapConfigStatsTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsCawapConfigStatsTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsCawapRunStatsTable (tSnmpIndex * pFirstMultiIndex,
                                  tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsCawapRunStatsTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsCawapRunStatsTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsCapwapWirelessBindingTable (tSnmpIndex * pFirstMultiIndex,
                                          tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsCapwapWirelessBindingTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsCapwapStationWhiteListTable (tSnmpIndex * pFirstMultiIndex,
                                           tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {

        if (nmhGetFirstIndexFsCapwapStationWhiteListTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {

        if (nmhGetNextIndexFsCapwapStationWhiteListTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsCapwapWtpRebootStatisticsTable (tSnmpIndex * pFirstMultiIndex,
                                              tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWtpRebootStatisticsTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsCapwapWtpRebootStatisticsTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsCapwapWtpRadioStatisticsTable (tSnmpIndex * pFirstMultiIndex,
                                             tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWtpRadioStatisticsTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsCapwapWtpRadioStatisticsTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsCapwapDot11StatisticsTable (tSnmpIndex * pFirstMultiIndex,
                                          tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsCapwapDot11StatisticsTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsCapwapDot11StatisticsTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
CapwapBaseWtpSessionsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetCapwapBaseWtpSessions (&(pMultiData->u4_ULongValue)));
}

INT4
CapwapBaseWtpSessionsLimitGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetCapwapBaseWtpSessionsLimit (&(pMultiData->u4_ULongValue)));
}

INT4
CapwapBaseStationSessionsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetCapwapBaseStationSessions (&(pMultiData->u4_ULongValue)));
}

INT4
CapwapBaseStationSessionsLimitGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetCapwapBaseStationSessionsLimit
            (&(pMultiData->u4_ULongValue)));
}

INT4
CapwapBaseDataChannelDTLSPolicyOptionsGet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetCapwapBaseDataChannelDTLSPolicyOptions
            (pMultiData->pOctetStrValue));
}

INT4
CapwapBaseControlChannelAuthenOptionsGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetCapwapBaseControlChannelAuthenOptions
            (pMultiData->pOctetStrValue));
}

INT4
CapwapBaseAcNameListNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceCapwapBaseAcNameListTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetCapwapBaseAcNameListName
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
CapwapBaseAcNameListPriorityGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceCapwapBaseAcNameListTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetCapwapBaseAcNameListPriority
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
CapwapBaseAcNameListRowStatusGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceCapwapBaseAcNameListTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetCapwapBaseAcNameListRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
CapwapBaseMacAclStationIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceCapwapBaseMacAclTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetCapwapBaseMacAclStationId
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
CapwapBaseMacAclRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceCapwapBaseMacAclTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetCapwapBaseMacAclRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
CapwapBaseWtpProfileNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceCapwapBaseWtpProfileTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetCapwapBaseWtpProfileName
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
CapwapBaseWtpProfileWtpMacAddressGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceCapwapBaseWtpProfileTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetCapwapBaseWtpProfileWtpMacAddress
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
CapwapBaseWtpProfileWtpModelNumberGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceCapwapBaseWtpProfileTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetCapwapBaseWtpProfileWtpModelNumber
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
CapwapBaseWtpProfileWtpNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceCapwapBaseWtpProfileTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetCapwapBaseWtpProfileWtpName
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
CapwapBaseWtpProfileWtpLocationGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceCapwapBaseWtpProfileTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetCapwapBaseWtpProfileWtpLocation
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
CapwapBaseWtpProfileWtpStaticIpEnableGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceCapwapBaseWtpProfileTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetCapwapBaseWtpProfileWtpStaticIpEnable
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
CapwapBaseWtpProfileWtpStaticIpTypeGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceCapwapBaseWtpProfileTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetCapwapBaseWtpProfileWtpStaticIpType
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
CapwapBaseWtpProfileWtpStaticIpAddressGet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceCapwapBaseWtpProfileTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetCapwapBaseWtpProfileWtpStaticIpAddress
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
CapwapBaseWtpProfileWtpNetmaskGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceCapwapBaseWtpProfileTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetCapwapBaseWtpProfileWtpNetmask
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
CapwapBaseWtpProfileWtpGatewayGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceCapwapBaseWtpProfileTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetCapwapBaseWtpProfileWtpGateway
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
CapwapBaseWtpProfileWtpFallbackEnableGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceCapwapBaseWtpProfileTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetCapwapBaseWtpProfileWtpFallbackEnable
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
CapwapBaseWtpProfileWtpEchoIntervalGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceCapwapBaseWtpProfileTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetCapwapBaseWtpProfileWtpEchoInterval
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
CapwapBaseWtpProfileWtpIdleTimeoutGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceCapwapBaseWtpProfileTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetCapwapBaseWtpProfileWtpIdleTimeout
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
CapwapBaseWtpProfileWtpMaxDiscoveryIntervalGet (tSnmpIndex * pMultiIndex,
                                                tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceCapwapBaseWtpProfileTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetCapwapBaseWtpProfileWtpMaxDiscoveryInterval
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
CapwapBaseWtpProfileWtpReportIntervalGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceCapwapBaseWtpProfileTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetCapwapBaseWtpProfileWtpReportInterval
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
CapwapBaseWtpProfileWtpStatisticsTimerGet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceCapwapBaseWtpProfileTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetCapwapBaseWtpProfileWtpStatisticsTimer
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
CapwapBaseWtpProfileWtpEcnSupportGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceCapwapBaseWtpProfileTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetCapwapBaseWtpProfileWtpEcnSupport
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
CapwapBaseWtpProfileRowStatusGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceCapwapBaseWtpProfileTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetCapwapBaseWtpProfileRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
CapwapBaseWtpStateWtpIpAddressTypeGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceCapwapBaseWtpStateTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetCapwapBaseWtpStateWtpIpAddressType
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
CapwapBaseWtpStateWtpIpAddressGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceCapwapBaseWtpStateTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetCapwapBaseWtpStateWtpIpAddress
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
CapwapBaseWtpStateWtpLocalIpAddressTypeGet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceCapwapBaseWtpStateTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetCapwapBaseWtpStateWtpLocalIpAddressType
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
CapwapBaseWtpStateWtpLocalIpAddressGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceCapwapBaseWtpStateTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetCapwapBaseWtpStateWtpLocalIpAddress
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
CapwapBaseWtpStateWtpBaseMacAddressGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceCapwapBaseWtpStateTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetCapwapBaseWtpStateWtpBaseMacAddress
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
CapwapBaseWtpStateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceCapwapBaseWtpStateTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetCapwapBaseWtpState (pMultiIndex->pIndex[0].pOctetStrValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
CapwapBaseWtpStateWtpUpTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceCapwapBaseWtpStateTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetCapwapBaseWtpStateWtpUpTime
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
CapwapBaseWtpStateWtpCurrWtpProfileIdGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceCapwapBaseWtpStateTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetCapwapBaseWtpStateWtpCurrWtpProfileId
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
CapwapBaseWtpPhyIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceCapwapBaseWtpTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetCapwapBaseWtpPhyIndex (pMultiIndex->pIndex[0].pOctetStrValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
CapwapBaseWtpBaseMacAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceCapwapBaseWtpTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetCapwapBaseWtpBaseMacAddress
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
CapwapBaseWtpTunnelModeOptionsGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceCapwapBaseWtpTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetCapwapBaseWtpTunnelModeOptions
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
CapwapBaseWtpMacTypeOptionsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceCapwapBaseWtpTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetCapwapBaseWtpMacTypeOptions
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
CapwapBaseWtpDiscoveryTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceCapwapBaseWtpTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetCapwapBaseWtpDiscoveryType
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
CapwapBaseWtpRadiosInUseNumGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceCapwapBaseWtpTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetCapwapBaseWtpRadiosInUseNum
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
CapwapBaseWtpRadioNumLimitGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceCapwapBaseWtpTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetCapwapBaseWtpRadioNumLimit
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
CapwapBaseWtpRetransmitCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceCapwapBaseWtpTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetCapwapBaseWtpRetransmitCount
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
CapwapBaseWirelessBindingVirtualRadioIfIndexGet (tSnmpIndex * pMultiIndex,
                                                 tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceCapwapBaseWirelessBindingTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
CapwapBaseWirelessBindingTypeGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceCapwapBaseWirelessBindingTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetCapwapBaseWirelessBindingType
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
CapwapBaseStationWtpIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceCapwapBaseStationTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetCapwapBaseStationWtpId (pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiData->pOctetStrValue));

}

INT4
CapwapBaseStationWtpRadioIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceCapwapBaseStationTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetCapwapBaseStationWtpRadioId
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
CapwapBaseStationAddedTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceCapwapBaseStationTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetCapwapBaseStationAddedTime
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
CapwapBaseStationVlanNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceCapwapBaseStationTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetCapwapBaseStationVlanName
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
CapwapBaseWtpEventsStatsRebootCountGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceCapwapBaseWtpEventsStatsTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetCapwapBaseWtpEventsStatsRebootCount
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
CapwapBaseWtpEventsStatsInitCountGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceCapwapBaseWtpEventsStatsTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetCapwapBaseWtpEventsStatsInitCount
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
CapwapBaseWtpEventsStatsLinkFailureCountGet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceCapwapBaseWtpEventsStatsTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetCapwapBaseWtpEventsStatsLinkFailureCount
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
CapwapBaseWtpEventsStatsSwFailureCountGet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceCapwapBaseWtpEventsStatsTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetCapwapBaseWtpEventsStatsSwFailureCount
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
CapwapBaseWtpEventsStatsHwFailureCountGet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceCapwapBaseWtpEventsStatsTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetCapwapBaseWtpEventsStatsHwFailureCount
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
CapwapBaseWtpEventsStatsOtherFailureCountGet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceCapwapBaseWtpEventsStatsTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetCapwapBaseWtpEventsStatsOtherFailureCount
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
CapwapBaseWtpEventsStatsUnknownFailureCountGet (tSnmpIndex * pMultiIndex,
                                                tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceCapwapBaseWtpEventsStatsTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetCapwapBaseWtpEventsStatsUnknownFailureCount
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
CapwapBaseWtpEventsStatsLastFailureTypeGet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceCapwapBaseWtpEventsStatsTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetCapwapBaseWtpEventsStatsLastFailureType
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
CapwapBaseRadioEventsStatsResetCountGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceCapwapBaseRadioEventsStatsTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetCapwapBaseRadioEventsStatsResetCount
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
CapwapBaseRadioEventsStatsSwFailureCountGet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceCapwapBaseRadioEventsStatsTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetCapwapBaseRadioEventsStatsSwFailureCount
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
CapwapBaseRadioEventsStatsHwFailureCountGet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceCapwapBaseRadioEventsStatsTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetCapwapBaseRadioEventsStatsHwFailureCount
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
CapwapBaseRadioEventsStatsOtherFailureCountGet (tSnmpIndex * pMultiIndex,
                                                tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceCapwapBaseRadioEventsStatsTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetCapwapBaseRadioEventsStatsOtherFailureCount
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
CapwapBaseRadioEventsStatsUnknownFailureCountGet (tSnmpIndex * pMultiIndex,
                                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceCapwapBaseRadioEventsStatsTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetCapwapBaseRadioEventsStatsUnknownFailureCount
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
CapwapBaseRadioEventsStatsConfigUpdateCountGet (tSnmpIndex * pMultiIndex,
                                                tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceCapwapBaseRadioEventsStatsTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetCapwapBaseRadioEventsStatsConfigUpdateCount
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
CapwapBaseRadioEventsStatsChannelChangeCountGet (tSnmpIndex * pMultiIndex,
                                                 tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceCapwapBaseRadioEventsStatsTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetCapwapBaseRadioEventsStatsChannelChangeCount
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
CapwapBaseRadioEventsStatsBandChangeCountGet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceCapwapBaseRadioEventsStatsTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetCapwapBaseRadioEventsStatsBandChangeCount
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
CapwapBaseRadioEventsStatsCurrNoiseFloorGet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceCapwapBaseRadioEventsStatsTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetCapwapBaseRadioEventsStatsCurrNoiseFloor
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
CapwapBaseRadioEventsStatsDecryptErrorCountGet (tSnmpIndex * pMultiIndex,
                                                tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceCapwapBaseRadioEventsStatsTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetCapwapBaseRadioEventsStatsDecryptErrorCount
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
CapwapBaseRadioEventsStatsLastFailureTypeGet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceCapwapBaseRadioEventsStatsTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetCapwapBaseRadioEventsStatsLastFailureType
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
CapwapBaseAcMaxRetransmitGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetCapwapBaseAcMaxRetransmit (&(pMultiData->u4_ULongValue)));
}

INT4
CapwapBaseAcChangeStatePendingTimerGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetCapwapBaseAcChangeStatePendingTimer
            (&(pMultiData->u4_ULongValue)));
}

INT4
CapwapBaseAcDataCheckTimerGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetCapwapBaseAcDataCheckTimer (&(pMultiData->u4_ULongValue)));
}

INT4
CapwapBaseAcDTLSSessionDeleteTimerGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetCapwapBaseAcDTLSSessionDeleteTimer
            (&(pMultiData->u4_ULongValue)));
}

INT4
CapwapBaseAcEchoIntervalGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetCapwapBaseAcEchoInterval (&(pMultiData->u4_ULongValue)));
}

INT4
CapwapBaseAcRetransmitIntervalGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetCapwapBaseAcRetransmitInterval
            (&(pMultiData->u4_ULongValue)));
}

INT4
CapwapBaseAcSilentIntervalGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetCapwapBaseAcSilentInterval (&(pMultiData->u4_ULongValue)));
}

INT4
CapwapBaseAcWaitDTLSTimerGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetCapwapBaseAcWaitDTLSTimer (&(pMultiData->u4_ULongValue)));
}

INT4
CapwapBaseAcWaitJoinTimerGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetCapwapBaseAcWaitJoinTimer (&(pMultiData->u4_ULongValue)));
}

INT4
CapwapBaseAcEcnSupportGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetCapwapBaseAcEcnSupport (&(pMultiData->i4_SLongValue)));
}

INT4
CapwapBaseFailedDTLSAuthFailureCountGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetCapwapBaseFailedDTLSAuthFailureCount
            (&(pMultiData->u4_ULongValue)));
}

INT4
CapwapBaseFailedDTLSSessionCountGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetCapwapBaseFailedDTLSSessionCount
            (&(pMultiData->u4_ULongValue)));
}

INT4
CapwapBaseChannelUpDownNotifyEnableGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetCapwapBaseChannelUpDownNotifyEnable
            (&(pMultiData->i4_SLongValue)));
}

INT4
CapwapBaseDecryptErrorNotifyEnableGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetCapwapBaseDecryptErrorNotifyEnable
            (&(pMultiData->i4_SLongValue)));
}

INT4
CapwapBaseJoinFailureNotifyEnableGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetCapwapBaseJoinFailureNotifyEnable
            (&(pMultiData->i4_SLongValue)));
}

INT4
CapwapBaseImageUpgradeFailureNotifyEnableGet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetCapwapBaseImageUpgradeFailureNotifyEnable
            (&(pMultiData->i4_SLongValue)));
}

INT4
CapwapBaseConfigMsgErrorNotifyEnableGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetCapwapBaseConfigMsgErrorNotifyEnable
            (&(pMultiData->i4_SLongValue)));
}

INT4
CapwapBaseRadioOperableStatusNotifyEnableGet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetCapwapBaseRadioOperableStatusNotifyEnable
            (&(pMultiData->i4_SLongValue)));
}

INT4
CapwapBaseAuthenFailureNotifyEnableGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetCapwapBaseAuthenFailureNotifyEnable
            (&(pMultiData->i4_SLongValue)));
}

INT4
CapwapBaseWtpSessionsLimitTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2CapwapBaseWtpSessionsLimit
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
CapwapBaseStationSessionsLimitTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2CapwapBaseStationSessionsLimit
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
CapwapBaseAcNameListNameTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2CapwapBaseAcNameListName (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               u4_ULongValue,
                                               pMultiData->pOctetStrValue));

}

INT4
FsCapwapModuleStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsCapwapEnable (&(pMultiData->i4_SLongValue)));
}

INT4
FsCapwapSystemControlGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsCapwapShutdown (&(pMultiData->i4_SLongValue)));
}

INT4
FsCapwapControlUdpPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsCapwapControlUdpPort (&(pMultiData->u4_ULongValue)));
}

INT4
FsCapwapControlChannelDTLSPolicyOptionsGet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsCapwapControlChannelDTLSPolicyOptions
            (pMultiData->pOctetStrValue));
}

INT4
FsCapwapDataChannelDTLSPolicyOptionsGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsCapwapDataChannelDTLSPolicyOptions
            (pMultiData->pOctetStrValue));
}

INT4
FsWlcDiscoveryModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsWlcDiscoveryMode (pMultiData->pOctetStrValue));
}

INT4
FsCapwapWtpModeIgnoreGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsCapwapWtpModeIgnore (&(pMultiData->i4_SLongValue)));
}

INT4
FsCapwapDebugMaskGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsCapwapDebugMask (&(pMultiData->i4_SLongValue)));
}

INT4
FsDtlsDebugMaskGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsDtlsDebugMask (&(pMultiData->i4_SLongValue)));
}

INT4
FsDtlsEncryptionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsDtlsEncryption (&(pMultiData->i4_SLongValue)));
}

INT4
FsDtlsEncryptAlgorithmGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsDtlsEncryptAlogritham (&(pMultiData->i4_SLongValue)));
}

INT4
FsStationTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsStationType (&(pMultiData->i4_SLongValue)));
}

INT4
FsAcTimestampTriggerGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsAcTimestampTrigger (&(pMultiData->i4_SLongValue)));
}

INT4
FsApRunStateCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsApRunStateCount (&(pMultiData->i4_SLongValue)));
}

INT4
FsApIdleStateCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsApIdleStateCount (&(pMultiData->i4_SLongValue)));
}

INT4
FsCapwapWssDataDebugMaskGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsCapwapWssDataDebugMask (&(pMultiData->i4_SLongValue)));
}

INT4
FsNoOfRadioGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWtpModelTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsNoOfRadio (pMultiIndex->pIndex[0].pOctetStrValue,
                               &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapWtpMacTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWtpModelTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapWtpMacType (pMultiIndex->pIndex[0].pOctetStrValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
FsCapwapWtpTunnelModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWtpModelTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapWtpTunnelMode (pMultiIndex->pIndex[0].pOctetStrValue,
                                         pMultiData->pOctetStrValue));

}

INT4
FsCapwapSwVersionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWtpModelTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapSwVersion (pMultiIndex->pIndex[0].pOctetStrValue,
                                     pMultiData->pOctetStrValue));

}

INT4
FsCapwapImageNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWtpModelTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapImageName (pMultiIndex->pIndex[0].pOctetStrValue,
                                     pMultiData->pOctetStrValue));

}

INT4
FsCapwapQosProfileNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWtpModelTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapQosProfileName (pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiData->pOctetStrValue));

}

INT4
FsMaxStationsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWtpModelTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMaxStations (pMultiIndex->pIndex[0].pOctetStrValue,
                                 &(pMultiData->i4_SLongValue)));

}

INT4
FsWtpModelRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWtpModelTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWtpModelRowStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
FsCapwapHwVersionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWtpModelTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapHwVersion (pMultiIndex->pIndex[0].pOctetStrValue,
                                     pMultiData->pOctetStrValue));

}

INT4
FsWtpRadioTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWtpRadioTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWtpRadioType (pMultiIndex->pIndex[0].pOctetStrValue,
                                  pMultiIndex->pIndex[1].u4_ULongValue,
                                  &(pMultiData->u4_ULongValue)));

}

INT4
FsWtpModelLocalRoutingGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWtpExtModelTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWtpModelLocalRouting (pMultiIndex->pIndex[0].pOctetStrValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
FsWtpModelWlanBindingRowStatusGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWtpModelWlanBindingTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWtpModelWlanBindingRowStatus
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsRadioAdminStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWtpRadioTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRadioAdminStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
FsMaxSSIDSupportedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWtpRadioTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMaxSSIDSupported (pMultiIndex->pIndex[0].pOctetStrValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
FsWtpRadioRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWtpRadioTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWtpRadioRowStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
FsCapwapWhiteListWtpBaseMacGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwapWhiteListTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapWhiteListWtpBaseMac
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
FsCapwapWhiteListRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwapWhiteListTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapWhiteListRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsCapwapBlackListWtpBaseMacGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwapBlackList
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapBlackListWtpBaseMac
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
FsCapwapBlackListRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwapBlackList
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapBlackListRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsCapwapWtpResetGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwapWtpConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapWtpReset (pMultiIndex->pIndex[0].u4_ULongValue,
                                    &(pMultiData->i4_SLongValue)));

}

INT4
FsCapwapClearConfigGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwapWtpConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapClearConfig (pMultiIndex->pIndex[0].u4_ULongValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
FsWtpDiscoveryTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwapWtpConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWtpDiscoveryType (pMultiIndex->pIndex[0].u4_ULongValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
FsWtpCountryStringGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwapWtpConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWtpCountryString (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiData->pOctetStrValue));

}

INT4
FsCapwapClearApStatsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwapWtpConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapClearApStats (pMultiIndex->pIndex[0].u4_ULongValue,
                                        &(pMultiData->i4_SLongValue)));
}

INT4
FsWtpCrashDumpFileNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwapWtpConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWtpCrashDumpFileName (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiData->pOctetStrValue));

}

INT4
FsWtpMemoryDumpFileNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwapWtpConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWtpMemoryDumpFileName (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiData->pOctetStrValue));

}

INT4
FsWtpDeleteOperationGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwapWtpConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWtpDeleteOperation (pMultiIndex->pIndex[0].u4_ULongValue,
                                        &(pMultiData->i4_SLongValue)));

}

INT4
FsCapwapWtpConfigRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwapWtpConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapWtpConfigRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsWlcIpAddressTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwapWtpConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWlcIpAddressType (pMultiIndex->pIndex[0].u4_ULongValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
FsWlcIpAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwapWtpConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWlcIpAddress (pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiData->pOctetStrValue));

}

INT4
FsCapwapEncryptChannelStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwapLinkEncryptionTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapEncryptChannelStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsCapwapEncryptChannelRowStatusGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwapLinkEncryptionTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapEncryptChannelRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsCapwapEncryptChannelRxPacketsGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwapLinkEncryptionTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapEncryptChannelRxPackets
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapDefaultWtpProfileWtpFallbackEnableGet (tSnmpIndex * pMultiIndex,
                                               tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapDefaultWtpProfileTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapDefaultWtpProfileWtpFallbackEnable
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsCapwapDefaultWtpProfileWtpEchoIntervalGet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapDefaultWtpProfileTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapDefaultWtpProfileWtpEchoInterval
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapDefaultWtpProfileWtpIdleTimeoutGet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapDefaultWtpProfileTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapDefaultWtpProfileWtpIdleTimeout
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapDefaultWtpProfileWtpMaxDiscoveryIntervalGet (tSnmpIndex * pMultiIndex,
                                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapDefaultWtpProfileTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapDefaultWtpProfileWtpMaxDiscoveryInterval
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapDefaultWtpProfileWtpReportIntervalGet (tSnmpIndex * pMultiIndex,
                                               tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapDefaultWtpProfileTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapDefaultWtpProfileWtpReportInterval
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapDefaultWtpProfileWtpStatisticsTimerGet (tSnmpIndex * pMultiIndex,
                                                tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapDefaultWtpProfileTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapDefaultWtpProfileWtpStatisticsTimer
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapDefaultWtpProfileWtpEcnSupportGet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapDefaultWtpProfileTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapDefaultWtpProfileWtpEcnSupport
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsCapwapDefaultWtpProfileRowStatusGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapDefaultWtpProfileTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapDefaultWtpProfileRowStatus
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsCapwapDnsServerIpGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwapDnsProfileTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapDnsServerIp (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiData->pOctetStrValue));

}

INT4
FsCapwapDnsDomainNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwapDnsProfileTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapDnsDomainName (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiData->pOctetStrValue));

}

INT4
FsCapwapDnsProfileRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwapDnsProfileTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapDnsProfileRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsWtpNativeVlanIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWtpNativeVlanIdTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWtpNativeVlanId (pMultiIndex->pIndex[0].u4_ULongValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
FsWtpNativeVlanIdRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWtpNativeVlanIdTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWtpNativeVlanIdRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsWtpLocalRoutingGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWtpLocalRoutingTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWtpLocalRouting (pMultiIndex->pIndex[0].u4_ULongValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
FsWtpLocalRoutingRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWtpLocalRoutingTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWtpLocalRoutingRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsCapwapFsAcTimestampTriggerGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwapFsAcTimestampTriggerTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapFsAcTimestampTrigger
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsCapwapFragReassembleStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwapExtWtpConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapFragReassembleStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsCapwapApSerialNumberGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwapExtWtpConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapApSerialNumber (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiData->pOctetStrValue));

}

INT4
FsCapwapWtpImageNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwapExtWtpConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapWtpImageName (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiData->pOctetStrValue));

}

INT4
FsCapwapDiscReqReceivedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapDiscStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapDiscReqReceived (pMultiIndex->pIndex[0].u4_ULongValue,
                                           &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapDiscRspReceivedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapDiscStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapDiscRspReceived (pMultiIndex->pIndex[0].u4_ULongValue,
                                           &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapDiscReqTransmittedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapDiscStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapDiscReqTransmitted
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapDiscRspTransmittedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapDiscStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapDiscRspTransmitted
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapDiscunsuccessfulProcessedGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapDiscStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapDiscunsuccessfulProcessed
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapDiscLastUnsuccAttemptReasonGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapDiscStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapDiscLastUnsuccAttemptReason
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapDiscLastSuccAttemptTimeGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapDiscStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapDiscLastSuccAttemptTime
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapDiscLastUnsuccessfulAttemptTimeGet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapDiscStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapDiscLastUnsuccessfulAttemptTime
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapDiscStatsRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapDiscStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapDiscStatsRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsCapwapJoinReqReceivedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapJoinStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapJoinReqReceived (pMultiIndex->pIndex[0].u4_ULongValue,
                                           &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapJoinRspReceivedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapJoinStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapJoinRspReceived (pMultiIndex->pIndex[0].u4_ULongValue,
                                           &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapJoinReqTransmittedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapJoinStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapJoinReqTransmitted
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapJoinRspTransmittedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapJoinStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapJoinRspTransmitted
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapJoinunsuccessfulProcessedGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapJoinStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapJoinunsuccessfulProcessed
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapJoinReasonLastUnsuccAttemptGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapJoinStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapJoinReasonLastUnsuccAttempt
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapJoinLastSuccAttemptTimeGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapJoinStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapJoinLastSuccAttemptTime
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapJoinLastUnsuccAttemptTimeGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapJoinStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapJoinLastUnsuccAttemptTime
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapJoinStatsRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapJoinStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapJoinStatsRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsCapwapConfigReqReceivedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapConfigStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapConfigReqReceived
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapConfigRspReceivedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapConfigStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapConfigRspReceived
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapConfigReqTransmittedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapConfigStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapConfigReqTransmitted
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapConfigRspTransmittedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapConfigStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapConfigRspTransmitted
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapConfigunsuccessfulProcessedGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapConfigStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapConfigunsuccessfulProcessed
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapConfigReasonLastUnsuccAttemptGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapConfigStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapConfigReasonLastUnsuccAttempt
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapConfigLastSuccAttemptTimeGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapConfigStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapConfigLastSuccAttemptTime
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapConfigLastUnsuccessfulAttemptTimeGet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapConfigStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapConfigLastUnsuccessfulAttemptTime
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapConfigStatsRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapConfigStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapConfigStatsRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsCapwapRunConfigUpdateReqReceivedGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapRunStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapRunConfigUpdateReqReceived
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapRunConfigUpdateRspReceivedGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapRunStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapRunConfigUpdateRspReceived
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapRunConfigUpdateReqTransmittedGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapRunStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapRunConfigUpdateReqTransmitted
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapRunConfigUpdateRspTransmittedGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapRunStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapRunConfigUpdateRspTransmitted
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapRunConfigUpdateunsuccessfulProcessedGet (tSnmpIndex * pMultiIndex,
                                                 tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapRunStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapRunConfigUpdateunsuccessfulProcessed
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapRunConfigUpdateReasonLastUnsuccAttemptGet (tSnmpIndex * pMultiIndex,
                                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapRunStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapRunConfigUpdateReasonLastUnsuccAttempt
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
FsCapwapRunConfigUpdateLastSuccAttemptTimeGet (tSnmpIndex * pMultiIndex,
                                               tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapRunStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapRunConfigUpdateLastSuccAttemptTime
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
FsCapwapRunConfigUpdateLastUnsuccessfulAttemptTimeGet (tSnmpIndex * pMultiIndex,
                                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapRunStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapRunConfigUpdateLastUnsuccessfulAttemptTime
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
FsCapwapRunStationConfigReqReceivedGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapRunStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapRunStationConfigReqReceived
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapRunStationConfigRspReceivedGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapRunStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapRunStationConfigRspReceived
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapRunStationConfigReqTransmittedGet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapRunStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapRunStationConfigReqTransmitted
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapRunStationConfigRspTransmittedGet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapRunStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapRunStationConfigRspTransmitted
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapRunStationConfigunsuccessfulProcessedGet (tSnmpIndex * pMultiIndex,
                                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapRunStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapRunStationConfigunsuccessfulProcessed
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapRunStationConfigReasonLastUnsuccAttemptGet (tSnmpIndex * pMultiIndex,
                                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapRunStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapRunStationConfigReasonLastUnsuccAttempt
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
FsCapwapRunStationConfigLastSuccAttemptTimeGet (tSnmpIndex * pMultiIndex,
                                                tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapRunStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapRunStationConfigLastSuccAttemptTime
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
FsCapwapRunStationConfigLastUnsuccessfulAttemptTimeGet (tSnmpIndex *
                                                        pMultiIndex,
                                                        tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapRunStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapRunStationConfigLastUnsuccessfulAttemptTime
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
FsCapwapRunClearConfigReqReceivedGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapRunStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapRunClearConfigReqReceived
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapRunClearConfigRspReceivedGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapRunStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapRunClearConfigRspReceived
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapRunClearConfigReqTransmittedGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapRunStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapRunClearConfigReqTransmitted
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapRunClearConfigRspTransmittedGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapRunStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapRunClearConfigRspTransmitted
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapRunClearConfigunsuccessfulProcessedGet (tSnmpIndex * pMultiIndex,
                                                tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapRunStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapRunClearConfigunsuccessfulProcessed
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapRunClearConfigReasonLastUnsuccAttemptGet (tSnmpIndex * pMultiIndex,
                                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapRunStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapRunClearConfigReasonLastUnsuccAttempt
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
FsCapwapRunClearConfigLastSuccAttemptTimeGet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapRunStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapRunClearConfigLastSuccAttemptTime
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
FsCapwapRunClearConfigLastUnsuccessfulAttemptTimeGet (tSnmpIndex * pMultiIndex,
                                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapRunStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapRunClearConfigLastUnsuccessfulAttemptTime
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
FsCapwapRunDataTransferReqReceivedGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapRunStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapRunDataTransferReqReceived
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapRunDataTransferRspReceivedGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapRunStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapRunDataTransferRspReceived
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapRunDataTransferReqTransmittedGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapRunStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapRunDataTransferReqTransmitted
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapRunDataTransferRspTransmittedGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapRunStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapRunDataTransferRspTransmitted
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapRunDataTransferunsuccessfulProcessedGet (tSnmpIndex * pMultiIndex,
                                                 tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapRunStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapRunDataTransferunsuccessfulProcessed
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapRunDataTransferReasonLastUnsuccAttemptGet (tSnmpIndex * pMultiIndex,
                                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapRunStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapRunDataTransferReasonLastUnsuccAttempt
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
FsCapwapRunDataTransferLastSuccAttemptTimeGet (tSnmpIndex * pMultiIndex,
                                               tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapRunStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapRunDataTransferLastSuccAttemptTime
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
FsCapwapRunDataTransferLastUnsuccessfulAttemptTimeGet (tSnmpIndex * pMultiIndex,
                                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapRunStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapRunDataTransferLastUnsuccessfulAttemptTime
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
FsCapwapRunResetReqReceivedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapRunStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapRunResetReqReceived
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapRunResetRspReceivedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapRunStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapRunResetRspReceived
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapRunResetReqTransmittedGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapRunStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapRunResetReqTransmitted
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapRunResetRspTransmittedGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapRunStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapRunResetRspTransmitted
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapRunResetunsuccessfulProcessedGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapRunStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapRunResetunsuccessfulProcessed
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapRunResetReasonLastUnsuccAttemptGet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapRunStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapRunResetReasonLastUnsuccAttempt
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
FsCapwapRunResetLastSuccAttemptTimeGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapRunStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapRunResetLastSuccAttemptTime
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
FsCapwapRunResetLastUnsuccessfulAttemptTimeGet (tSnmpIndex * pMultiIndex,
                                                tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapRunStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapRunResetLastUnsuccessfulAttemptTime
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
FsCapwapRunPriDiscReqReceivedGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapRunStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapRunPriDiscReqReceived
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapRunPriDiscRspReceivedGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapRunStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapRunPriDiscRspReceived
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapRunPriDiscReqTransmittedGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapRunStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapRunPriDiscReqTransmitted
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapRunPriDiscRspTransmittedGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapRunStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapRunPriDiscRspTransmitted
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapRunPriDiscunsuccessfulProcessedGet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapRunStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapRunPriDiscunsuccessfulProcessed
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapRunPriDiscReasonLastUnsuccAttemptGet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapRunStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapRunPriDiscReasonLastUnsuccAttempt
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
FsCapwapRunPriDiscLastSuccAttemptTimeGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapRunStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapRunPriDiscLastSuccAttemptTime
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
FsCapwapRunPriDiscLastUnsuccessfulAttemptTimeGet (tSnmpIndex * pMultiIndex,
                                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapRunStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapRunPriDiscLastUnsuccessfulAttemptTime
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
FsCapwapRunEchoReqReceivedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapRunStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapRunEchoReqReceived
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapRunEchoRspReceivedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapRunStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapRunEchoRspReceived
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapRunEchoReqTransmittedGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapRunStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapRunEchoReqTransmitted
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapRunEchoRspTransmittedGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapRunStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapRunEchoRspTransmitted
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapRunEchounsuccessfulProcessedGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapRunStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapRunEchounsuccessfulProcessed
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapRunEchoReasonLastUnsuccAttemptGet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapRunStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapRunEchoReasonLastUnsuccAttempt
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
FsCapwapRunEchoLastSuccAttemptTimeGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapRunStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapRunEchoLastSuccAttemptTime
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
FsCapwapRunEchoLastUnsuccessfulAttemptTimeGet (tSnmpIndex * pMultiIndex,
                                               tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapRunStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapRunEchoLastUnsuccessfulAttemptTime
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
FsCapwapRunStatsRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCawapRunStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapRunStatsRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsCapwapWirelessBindingVirtualRadioIfIndexGet (tSnmpIndex * pMultiIndex,
                                               tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwapWirelessBindingTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapWirelessBindingVirtualRadioIfIndex
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsCapwapWirelessBindingTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwapWirelessBindingTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapWirelessBindingType
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsCapwapWirelessBindingRowStatusGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwapWirelessBindingTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapWirelessBindingRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsCapwapStationWhiteListRowStatusGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwapStationWhiteListTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapStationWhiteListRowStatus
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsCapwapWtpRebootStatisticsRebootCountGet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwapWtpRebootStatisticsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapWtpRebootStatisticsRebootCount
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapWtpRebootStatisticsAcInitiatedCountGet (tSnmpIndex * pMultiIndex,
                                                tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwapWtpRebootStatisticsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapWtpRebootStatisticsAcInitiatedCount
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapWtpRebootStatisticsLinkFailureCountGet (tSnmpIndex * pMultiIndex,
                                                tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwapWtpRebootStatisticsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapWtpRebootStatisticsLinkFailureCount
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapWtpRebootStatisticsSwFailureCountGet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwapWtpRebootStatisticsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapWtpRebootStatisticsSwFailureCount
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapWtpRebootStatisticsHwFailureCountGet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwapWtpRebootStatisticsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapWtpRebootStatisticsHwFailureCount
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapWtpRebootStatisticsOtherFailureCountGet (tSnmpIndex * pMultiIndex,
                                                 tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwapWtpRebootStatisticsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapWtpRebootStatisticsOtherFailureCount
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapWtpRebootStatisticsUnknownFailureCountGet (tSnmpIndex * pMultiIndex,
                                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwapWtpRebootStatisticsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapWtpRebootStatisticsUnknownFailureCount
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapWtpRebootStatisticsLastFailureTypeGet (tSnmpIndex * pMultiIndex,
                                               tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwapWtpRebootStatisticsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapWtpRebootStatisticsLastFailureType
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsCapwapWtpRebootStatisticsRowStatusGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwapWtpRebootStatisticsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapWtpRebootStatisticsRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsCapwapWtpRadioLastFailTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwapWtpRadioStatisticsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapWtpRadioLastFailType
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsCapwapWtpRadioResetCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwapWtpRadioStatisticsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapWtpRadioResetCount
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapWtpRadioSwFailureCountGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwapWtpRadioStatisticsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapWtpRadioSwFailureCount
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapWtpRadioHwFailureCountGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwapWtpRadioStatisticsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapWtpRadioHwFailureCount
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapWtpRadioOtherFailureCountGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwapWtpRadioStatisticsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapWtpRadioOtherFailureCount
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapWtpRadioUnknownFailureCountGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwapWtpRadioStatisticsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapWtpRadioUnknownFailureCount
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapWtpRadioConfigUpdateCountGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwapWtpRadioStatisticsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapWtpRadioConfigUpdateCount
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapWtpRadioChannelChangeCountGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwapWtpRadioStatisticsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapWtpRadioChannelChangeCount
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapWtpRadioBandChangeCountGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwapWtpRadioStatisticsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapWtpRadioBandChangeCount
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapWtpRadioCurrentNoiseFloorGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwapWtpRadioStatisticsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapWtpRadioCurrentNoiseFloor
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapWtpRadioStatRowStatusGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwapWtpRadioStatisticsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapWtpRadioStatRowStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapDot11TxFragmentCntGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwapDot11StatisticsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapDot11TxFragmentCnt
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapDot11MulticastTxCntGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwapDot11StatisticsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapDot11MulticastTxCnt
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapDot11FailedCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwapDot11StatisticsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapDot11FailedCount
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapDot11RetryCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwapDot11StatisticsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapDot11RetryCount (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapDot11MultipleRetryCountGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwapDot11StatisticsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapDot11MultipleRetryCount
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapDot11FrameDupCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwapDot11StatisticsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapDot11FrameDupCount
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapDot11RTSSuccessCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwapDot11StatisticsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapDot11RTSSuccessCount
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapDot11RTSFailCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwapDot11StatisticsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapDot11RTSFailCount
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapDot11ACKFailCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwapDot11StatisticsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapDot11ACKFailCount
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapDot11RxFragmentCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwapDot11StatisticsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapDot11RxFragmentCount
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapDot11MulticastRxCountGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwapDot11StatisticsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapDot11MulticastRxCount
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapDot11FCSErrCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwapDot11StatisticsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapDot11FCSErrCount
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapDot11TxFrameCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwapDot11StatisticsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapDot11TxFrameCount
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapDot11DecryptionErrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwapDot11StatisticsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapDot11DecryptionErr
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapDot11DiscardQosFragmentCntGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwapDot11StatisticsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapDot11DiscardQosFragmentCnt
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapDot11AssociatedStaCountGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwapDot11StatisticsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapDot11AssociatedStaCount
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapDot11QosCFPollsRecvdCountGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwapDot11StatisticsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapDot11QosCFPollsRecvdCount
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapDot11QosCFPollsUnusedCountGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwapDot11StatisticsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapDot11QosCFPollsUnusedCount
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapDot11QosCFPollsUnusableCountGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwapDot11StatisticsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapDot11QosCFPollsUnusableCount
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwapDot11RadioIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwapDot11StatisticsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapDot11RadioId (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
CapwapBaseAcNameListPriorityTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2CapwapBaseAcNameListPriority (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   u4_ULongValue,
                                                   pMultiData->u4_ULongValue));

}

INT4
CapwapBaseAcNameListRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2CapwapBaseAcNameListRowStatus (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    u4_ULongValue,
                                                    pMultiData->i4_SLongValue));

}

INT4
CapwapBaseMacAclStationIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2CapwapBaseMacAclStationId (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                u4_ULongValue,
                                                pMultiData->pOctetStrValue));

}

INT4
CapwapBaseMacAclRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2CapwapBaseMacAclRowStatus (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                u4_ULongValue,
                                                pMultiData->i4_SLongValue));

}

INT4
CapwapBaseWtpProfileNameTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2CapwapBaseWtpProfileName (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               u4_ULongValue,
                                               pMultiData->pOctetStrValue));

}

INT4
CapwapBaseWtpProfileWtpMacAddressTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhTestv2CapwapBaseWtpProfileWtpMacAddress (pu4Error,
                                                        pMultiIndex->pIndex[0].
                                                        u4_ULongValue,
                                                        pMultiData->
                                                        pOctetStrValue));

}

INT4
CapwapBaseWtpProfileWtpModelNumberTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhTestv2CapwapBaseWtpProfileWtpModelNumber (pu4Error,
                                                         pMultiIndex->pIndex[0].
                                                         u4_ULongValue,
                                                         pMultiData->
                                                         pOctetStrValue));

}

INT4
CapwapBaseWtpProfileWtpNameTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2CapwapBaseWtpProfileWtpName (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  u4_ULongValue,
                                                  pMultiData->pOctetStrValue));

}

INT4
CapwapBaseWtpProfileWtpLocationTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhTestv2CapwapBaseWtpProfileWtpLocation (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      u4_ULongValue,
                                                      pMultiData->
                                                      pOctetStrValue));

}

INT4
CapwapBaseWtpProfileWtpStaticIpEnableTest (UINT4 *pu4Error,
                                           tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    return (nmhTestv2CapwapBaseWtpProfileWtpStaticIpEnable (pu4Error,
                                                            pMultiIndex->
                                                            pIndex[0].
                                                            u4_ULongValue,
                                                            pMultiData->
                                                            i4_SLongValue));

}

INT4
CapwapBaseWtpProfileWtpStaticIpTypeTest (UINT4 *pu4Error,
                                         tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    return (nmhTestv2CapwapBaseWtpProfileWtpStaticIpType (pu4Error,
                                                          pMultiIndex->
                                                          pIndex[0].
                                                          u4_ULongValue,
                                                          pMultiData->
                                                          i4_SLongValue));

}

INT4
CapwapBaseWtpProfileWtpStaticIpAddressTest (UINT4 *pu4Error,
                                            tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    return (nmhTestv2CapwapBaseWtpProfileWtpStaticIpAddress (pu4Error,
                                                             pMultiIndex->
                                                             pIndex[0].
                                                             u4_ULongValue,
                                                             pMultiData->
                                                             pOctetStrValue));

}

INT4
CapwapBaseWtpProfileWtpNetmaskTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2CapwapBaseWtpProfileWtpNetmask (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     u4_ULongValue,
                                                     pMultiData->
                                                     pOctetStrValue));

}

INT4
CapwapBaseWtpProfileWtpGatewayTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2CapwapBaseWtpProfileWtpGateway (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     u4_ULongValue,
                                                     pMultiData->
                                                     pOctetStrValue));

}

INT4
CapwapBaseWtpProfileWtpFallbackEnableTest (UINT4 *pu4Error,
                                           tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    return (nmhTestv2CapwapBaseWtpProfileWtpFallbackEnable (pu4Error,
                                                            pMultiIndex->
                                                            pIndex[0].
                                                            u4_ULongValue,
                                                            pMultiData->
                                                            i4_SLongValue));

}

INT4
CapwapBaseWtpProfileWtpEchoIntervalTest (UINT4 *pu4Error,
                                         tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    return (nmhTestv2CapwapBaseWtpProfileWtpEchoInterval (pu4Error,
                                                          pMultiIndex->
                                                          pIndex[0].
                                                          u4_ULongValue,
                                                          pMultiData->
                                                          u4_ULongValue));

}

INT4
CapwapBaseWtpProfileWtpIdleTimeoutTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhTestv2CapwapBaseWtpProfileWtpIdleTimeout (pu4Error,
                                                         pMultiIndex->pIndex[0].
                                                         u4_ULongValue,
                                                         pMultiData->
                                                         u4_ULongValue));

}

INT4
CapwapBaseWtpProfileWtpMaxDiscoveryIntervalTest (UINT4 *pu4Error,
                                                 tSnmpIndex * pMultiIndex,
                                                 tRetVal * pMultiData)
{
    return (nmhTestv2CapwapBaseWtpProfileWtpMaxDiscoveryInterval (pu4Error,
                                                                  pMultiIndex->
                                                                  pIndex[0].
                                                                  u4_ULongValue,
                                                                  pMultiData->
                                                                  u4_ULongValue));

}

INT4
CapwapBaseWtpProfileWtpReportIntervalTest (UINT4 *pu4Error,
                                           tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    return (nmhTestv2CapwapBaseWtpProfileWtpReportInterval (pu4Error,
                                                            pMultiIndex->
                                                            pIndex[0].
                                                            u4_ULongValue,
                                                            pMultiData->
                                                            u4_ULongValue));

}

INT4
CapwapBaseWtpProfileWtpStatisticsTimerTest (UINT4 *pu4Error,
                                            tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    return (nmhTestv2CapwapBaseWtpProfileWtpStatisticsTimer (pu4Error,
                                                             pMultiIndex->
                                                             pIndex[0].
                                                             u4_ULongValue,
                                                             pMultiData->
                                                             u4_ULongValue));

}

INT4
CapwapBaseWtpProfileWtpEcnSupportTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhTestv2CapwapBaseWtpProfileWtpEcnSupport (pu4Error,
                                                        pMultiIndex->pIndex[0].
                                                        u4_ULongValue,
                                                        pMultiData->
                                                        i4_SLongValue));

}

INT4
CapwapBaseWtpProfileRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2CapwapBaseWtpProfileRowStatus (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    u4_ULongValue,
                                                    pMultiData->i4_SLongValue));

}

INT4
CapwapBaseAcMaxRetransmitTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2CapwapBaseAcMaxRetransmit
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
CapwapBaseAcChangeStatePendingTimerTest (UINT4 *pu4Error,
                                         tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2CapwapBaseAcChangeStatePendingTimer
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
CapwapBaseAcDataCheckTimerTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2CapwapBaseAcDataCheckTimer
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
CapwapBaseAcDTLSSessionDeleteTimerTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2CapwapBaseAcDTLSSessionDeleteTimer
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
CapwapBaseAcEchoIntervalTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2CapwapBaseAcEchoInterval
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
CapwapBaseAcRetransmitIntervalTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2CapwapBaseAcRetransmitInterval
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
CapwapBaseAcSilentIntervalTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2CapwapBaseAcSilentInterval
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
CapwapBaseAcWaitDTLSTimerTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2CapwapBaseAcWaitDTLSTimer
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
CapwapBaseAcWaitJoinTimerTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2CapwapBaseAcWaitJoinTimer
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
CapwapBaseAcEcnSupportTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2CapwapBaseAcEcnSupport
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
CapwapBaseChannelUpDownNotifyEnableTest (UINT4 *pu4Error,
                                         tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2CapwapBaseChannelUpDownNotifyEnable
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
CapwapBaseDecryptErrorNotifyEnableTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2CapwapBaseDecryptErrorNotifyEnable
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
CapwapBaseJoinFailureNotifyEnableTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2CapwapBaseJoinFailureNotifyEnable
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
CapwapBaseImageUpgradeFailureNotifyEnableTest (UINT4 *pu4Error,
                                               tSnmpIndex * pMultiIndex,
                                               tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2CapwapBaseImageUpgradeFailureNotifyEnable
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
CapwapBaseConfigMsgErrorNotifyEnableTest (UINT4 *pu4Error,
                                          tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2CapwapBaseConfigMsgErrorNotifyEnable
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
CapwapBaseRadioOperableStatusNotifyEnableTest (UINT4 *pu4Error,
                                               tSnmpIndex * pMultiIndex,
                                               tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2CapwapBaseRadioOperableStatusNotifyEnable
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
CapwapBaseAuthenFailureNotifyEnableTest (UINT4 *pu4Error,
                                         tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2CapwapBaseAuthenFailureNotifyEnable
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsCapwapModuleStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsCapwapEnable (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsCapwapSystemControlTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsCapwapShutdown (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsCapwapControlUdpPortTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsCapwapControlUdpPort
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
FsCapwapControlChannelDTLSPolicyOptionsTest (UINT4 *pu4Error,
                                             tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsCapwapControlChannelDTLSPolicyOptions
            (pu4Error, pMultiData->pOctetStrValue));
}

INT4
FsCapwapDataChannelDTLSPolicyOptionsTest (UINT4 *pu4Error,
                                          tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsCapwapDataChannelDTLSPolicyOptions
            (pu4Error, pMultiData->pOctetStrValue));
}

INT4
FsWlcDiscoveryModeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsWlcDiscoveryMode (pu4Error, pMultiData->pOctetStrValue));
}

INT4
FsCapwapWtpModeIgnoreTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsCapwapWtpModeIgnore
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsCapwapDebugMaskTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsCapwapDebugMask (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsDtlsDebugMaskTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsDtlsDebugMask (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsDtlsEncryptionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsDtlsEncryption (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsDtlsEncryptAlgorithmTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsDtlsEncryptAlogritham
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsStationTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsStationType (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsAcTimestampTriggerTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsAcTimestampTrigger
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsCapwapWssDataDebugMaskTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsCapwapWssDataDebugMask
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsNoOfRadioTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                 tRetVal * pMultiData)
{
    return (nmhTestv2FsNoOfRadio (pu4Error,
                                  pMultiIndex->pIndex[0].pOctetStrValue,
                                  pMultiData->u4_ULongValue));

}

INT4
FsCapwapWtpMacTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapWtpMacType (pu4Error,
                                         pMultiIndex->pIndex[0].pOctetStrValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FsCapwapWtpTunnelModeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapWtpTunnelMode (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            pOctetStrValue,
                                            pMultiData->pOctetStrValue));

}

INT4
FsCapwapImageNameTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapImageName (pu4Error,
                                        pMultiIndex->pIndex[0].pOctetStrValue,
                                        pMultiData->pOctetStrValue));

}

INT4
FsCapwapQosProfileNameTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapQosProfileName (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             pOctetStrValue,
                                             pMultiData->pOctetStrValue));

}

INT4
FsMaxStationsTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                   tRetVal * pMultiData)
{
    return (nmhTestv2FsMaxStations (pu4Error,
                                    pMultiIndex->pIndex[0].pOctetStrValue,
                                    pMultiData->i4_SLongValue));

}

INT4
FsWtpModelRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2FsWtpModelRowStatus (pu4Error,
                                          pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsWtpRadioTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    return (nmhTestv2FsWtpRadioType (pu4Error,
                                     pMultiIndex->pIndex[0].pOctetStrValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiData->u4_ULongValue));

}

INT4
FsWtpModelLocalRoutingTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2FsWtpModelLocalRouting (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             pOctetStrValue,
                                             pMultiData->i4_SLongValue));

}

INT4
FsWtpModelWlanBindingRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2FsWtpModelWlanBindingRowStatus (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     pOctetStrValue,
                                                     pMultiIndex->pIndex[1].
                                                     u4_ULongValue,
                                                     pMultiIndex->pIndex[2].
                                                     u4_ULongValue,
                                                     pMultiData->
                                                     i4_SLongValue));

}

INT4
FsRadioAdminStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2FsRadioAdminStatus (pu4Error,
                                         pMultiIndex->pIndex[0].pOctetStrValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FsMaxSSIDSupportedTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2FsMaxSSIDSupported (pu4Error,
                                         pMultiIndex->pIndex[0].pOctetStrValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FsWtpRadioRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2FsWtpRadioRowStatus (pu4Error,
                                          pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsCapwapWhiteListWtpBaseMacTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapWhiteListWtpBaseMac (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  u4_ULongValue,
                                                  pMultiData->pOctetStrValue));

}

INT4
FsCapwapWhiteListRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapWhiteListRowStatus (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 u4_ULongValue,
                                                 pMultiData->i4_SLongValue));

}

INT4
FsCapwapBlackListWtpBaseMacTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapBlackListWtpBaseMac (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  u4_ULongValue,
                                                  pMultiData->pOctetStrValue));

}

INT4
FsCapwapBlackListRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapBlackListRowStatus (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 u4_ULongValue,
                                                 pMultiData->i4_SLongValue));

}

INT4
FsCapwapWtpResetTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapWtpReset (pu4Error,
                                       pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
FsCapwapClearConfigTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapClearConfig (pu4Error,
                                          pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsWtpDiscoveryTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2FsWtpDiscoveryType (pu4Error,
                                         pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FsWtpCountryStringTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2FsWtpCountryString (pu4Error,
                                         pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiData->pOctetStrValue));

}

INT4
FsCapwapClearApStatsTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapClearApStats (pu4Error,
                                           pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiData->i4_SLongValue));
}

INT4
FsWtpCrashDumpFileNameTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2FsWtpCrashDumpFileName (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             u4_ULongValue,
                                             pMultiData->pOctetStrValue));

}

INT4
FsWtpMemoryDumpFileNameTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2FsWtpMemoryDumpFileName (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              u4_ULongValue,
                                              pMultiData->pOctetStrValue));

}

INT4
FsWtpDeleteOperationTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2FsWtpDeleteOperation (pu4Error,
                                           pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
FsCapwapWtpConfigRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapWtpConfigRowStatus (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 u4_ULongValue,
                                                 pMultiData->i4_SLongValue));

}

INT4
FsWlcIpAddressTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2FsWlcIpAddressType (pu4Error,
                                         pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FsWlcIpAddressTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    return (nmhTestv2FsWlcIpAddress (pu4Error,
                                     pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiData->pOctetStrValue));

}

INT4
FsCapwapEncryptChannelStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapEncryptChannelStatus (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   u4_ULongValue,
                                                   pMultiIndex->pIndex[1].
                                                   i4_SLongValue,
                                                   pMultiData->i4_SLongValue));

}

INT4
FsCapwapEncryptChannelRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapEncryptChannelRowStatus (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      u4_ULongValue,
                                                      pMultiIndex->pIndex[1].
                                                      i4_SLongValue,
                                                      pMultiData->
                                                      i4_SLongValue));

}

INT4
FsCapwapDefaultWtpProfileModelNumberTest (UINT4 *pu4Error,
                                          tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapDefaultWtpProfileModelNumber (pu4Error,
                                                           pMultiIndex->
                                                           pIndex[0].
                                                           pOctetStrValue,
                                                           pMultiData->
                                                           pOctetStrValue));

}

INT4
FsCapwapDefaultWtpProfileWtpFallbackEnableTest (UINT4 *pu4Error,
                                                tSnmpIndex * pMultiIndex,
                                                tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapDefaultWtpProfileWtpFallbackEnable (pu4Error,
                                                                 pMultiIndex->
                                                                 pIndex[0].
                                                                 pOctetStrValue,
                                                                 pMultiData->
                                                                 i4_SLongValue));

}

INT4
FsCapwapDefaultWtpProfileWtpEchoIntervalTest (UINT4 *pu4Error,
                                              tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapDefaultWtpProfileWtpEchoInterval (pu4Error,
                                                               pMultiIndex->
                                                               pIndex[0].
                                                               pOctetStrValue,
                                                               pMultiData->
                                                               u4_ULongValue));

}

INT4
FsCapwapDefaultWtpProfileWtpIdleTimeoutTest (UINT4 *pu4Error,
                                             tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapDefaultWtpProfileWtpIdleTimeout (pu4Error,
                                                              pMultiIndex->
                                                              pIndex[0].
                                                              pOctetStrValue,
                                                              pMultiData->
                                                              u4_ULongValue));

}

INT4
FsCapwapDefaultWtpProfileWtpMaxDiscoveryIntervalTest (UINT4 *pu4Error,
                                                      tSnmpIndex * pMultiIndex,
                                                      tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapDefaultWtpProfileWtpMaxDiscoveryInterval (pu4Error,
                                                                       pMultiIndex->
                                                                       pIndex
                                                                       [0].
                                                                       pOctetStrValue,
                                                                       pMultiData->
                                                                       u4_ULongValue));

}

INT4
FsCapwapDefaultWtpProfileWtpReportIntervalTest (UINT4 *pu4Error,
                                                tSnmpIndex * pMultiIndex,
                                                tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapDefaultWtpProfileWtpReportInterval (pu4Error,
                                                                 pMultiIndex->
                                                                 pIndex[0].
                                                                 pOctetStrValue,
                                                                 pMultiData->
                                                                 u4_ULongValue));

}

INT4
FsCapwapDefaultWtpProfileWtpStatisticsTimerTest (UINT4 *pu4Error,
                                                 tSnmpIndex * pMultiIndex,
                                                 tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapDefaultWtpProfileWtpStatisticsTimer (pu4Error,
                                                                  pMultiIndex->
                                                                  pIndex[0].
                                                                  pOctetStrValue,
                                                                  pMultiData->
                                                                  u4_ULongValue));

}

INT4
FsCapwapDefaultWtpProfileWtpEcnSupportTest (UINT4 *pu4Error,
                                            tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapDefaultWtpProfileWtpEcnSupport (pu4Error,
                                                             pMultiIndex->
                                                             pIndex[0].
                                                             pOctetStrValue,
                                                             pMultiData->
                                                             i4_SLongValue));

}

INT4
FsCapwapDefaultWtpProfileRowStatusTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapDefaultWtpProfileRowStatus (pu4Error,
                                                         pMultiIndex->pIndex[0].
                                                         pOctetStrValue,
                                                         pMultiData->
                                                         i4_SLongValue));

}

INT4
FsCapwapDnsServerIpTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapDnsServerIp (pu4Error,
                                          pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiData->pOctetStrValue));

}

INT4
FsCapwapDnsDomainNameTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapDnsDomainName (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            u4_ULongValue,
                                            pMultiData->pOctetStrValue));

}

INT4
FsCapwapDnsProfileRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapDnsProfileRowStatus (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  u4_ULongValue,
                                                  pMultiData->i4_SLongValue));

}

INT4
FsWtpNativeVlanIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2FsWtpNativeVlanId (pu4Error,
                                        pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
FsWtpNativeVlanIdRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2FsWtpNativeVlanIdRowStatus (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 u4_ULongValue,
                                                 pMultiData->i4_SLongValue));

}

INT4
FsWtpLocalRoutingTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2FsWtpLocalRouting (pu4Error,
                                        pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
FsWtpLocalRoutingRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2FsWtpLocalRoutingRowStatus (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 u4_ULongValue,
                                                 pMultiData->i4_SLongValue));

}

INT4
FsCapwapFsAcTimestampTriggerTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapFsAcTimestampTrigger (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   u4_ULongValue,
                                                   pMultiData->i4_SLongValue));

}

INT4
FsCapwapFragReassembleStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapFragReassembleStatus (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   u4_ULongValue,
                                                   pMultiData->i4_SLongValue));

}

INT4
FsCapwapWtpImageNameTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapWtpImageName (pu4Error,
                                           pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiData->pOctetStrValue));

}

INT4
FsCapwapDiscStatsRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapDiscStatsRowStatus (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 u4_ULongValue,
                                                 pMultiData->i4_SLongValue));

}

INT4
FsCapwapJoinStatsRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapJoinStatsRowStatus (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 u4_ULongValue,
                                                 pMultiData->i4_SLongValue));

}

INT4
FsCapwapConfigStatsRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapConfigStatsRowStatus (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   u4_ULongValue,
                                                   pMultiData->i4_SLongValue));

}

INT4
FsCapwapRunStatsRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapRunStatsRowStatus (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                u4_ULongValue,
                                                pMultiData->i4_SLongValue));

}

INT4
FsCapwapWirelessBindingVirtualRadioIfIndexTest (UINT4 *pu4Error,
                                                tSnmpIndex * pMultiIndex,
                                                tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapWirelessBindingVirtualRadioIfIndex (pu4Error,
                                                                 pMultiIndex->
                                                                 pIndex[0].
                                                                 u4_ULongValue,
                                                                 pMultiIndex->
                                                                 pIndex[1].
                                                                 u4_ULongValue,
                                                                 pMultiData->
                                                                 i4_SLongValue));

}

INT4
FsCapwapWirelessBindingTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapWirelessBindingType (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  u4_ULongValue,
                                                  pMultiIndex->pIndex[1].
                                                  u4_ULongValue,
                                                  pMultiData->i4_SLongValue));

}

INT4
FsCapwapWirelessBindingRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapWirelessBindingRowStatus (pu4Error,
                                                       pMultiIndex->pIndex[0].
                                                       u4_ULongValue,
                                                       pMultiIndex->pIndex[1].
                                                       u4_ULongValue,
                                                       pMultiData->
                                                       i4_SLongValue));

}

INT4
FsCapwapStationWhiteListRowStatusTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapStationWhiteListRowStatus (pu4Error,
                                                        pMultiIndex->pIndex[0].
                                                        pOctetStrValue,
                                                        pMultiData->
                                                        i4_SLongValue));

}

INT4
FsCapwapWtpRebootStatisticsRebootCountTest (UINT4 *pu4Error,
                                            tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapWtpRebootStatisticsRebootCount (pu4Error,
                                                             pMultiIndex->
                                                             pIndex[0].
                                                             u4_ULongValue,
                                                             pMultiData->
                                                             u4_ULongValue));

}

INT4
FsCapwapWtpRebootStatisticsAcInitiatedCountTest (UINT4 *pu4Error,
                                                 tSnmpIndex * pMultiIndex,
                                                 tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapWtpRebootStatisticsAcInitiatedCount (pu4Error,
                                                                  pMultiIndex->
                                                                  pIndex[0].
                                                                  u4_ULongValue,
                                                                  pMultiData->
                                                                  u4_ULongValue));

}

INT4
FsCapwapWtpRebootStatisticsLinkFailureCountTest (UINT4 *pu4Error,
                                                 tSnmpIndex * pMultiIndex,
                                                 tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapWtpRebootStatisticsLinkFailureCount (pu4Error,
                                                                  pMultiIndex->
                                                                  pIndex[0].
                                                                  u4_ULongValue,
                                                                  pMultiData->
                                                                  u4_ULongValue));

}

INT4
FsCapwapWtpRebootStatisticsSwFailureCountTest (UINT4 *pu4Error,
                                               tSnmpIndex * pMultiIndex,
                                               tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapWtpRebootStatisticsSwFailureCount (pu4Error,
                                                                pMultiIndex->
                                                                pIndex[0].
                                                                u4_ULongValue,
                                                                pMultiData->
                                                                u4_ULongValue));

}

INT4
FsCapwapWtpRebootStatisticsHwFailureCountTest (UINT4 *pu4Error,
                                               tSnmpIndex * pMultiIndex,
                                               tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapWtpRebootStatisticsHwFailureCount (pu4Error,
                                                                pMultiIndex->
                                                                pIndex[0].
                                                                u4_ULongValue,
                                                                pMultiData->
                                                                u4_ULongValue));

}

INT4
FsCapwapWtpRebootStatisticsOtherFailureCountTest (UINT4 *pu4Error,
                                                  tSnmpIndex * pMultiIndex,
                                                  tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapWtpRebootStatisticsOtherFailureCount (pu4Error,
                                                                   pMultiIndex->
                                                                   pIndex[0].
                                                                   u4_ULongValue,
                                                                   pMultiData->
                                                                   u4_ULongValue));

}

INT4
FsCapwapWtpRebootStatisticsUnknownFailureCountTest (UINT4 *pu4Error,
                                                    tSnmpIndex * pMultiIndex,
                                                    tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapWtpRebootStatisticsUnknownFailureCount (pu4Error,
                                                                     pMultiIndex->
                                                                     pIndex[0].
                                                                     u4_ULongValue,
                                                                     pMultiData->
                                                                     u4_ULongValue));

}

INT4
FsCapwapWtpRebootStatisticsLastFailureTypeTest (UINT4 *pu4Error,
                                                tSnmpIndex * pMultiIndex,
                                                tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapWtpRebootStatisticsLastFailureType (pu4Error,
                                                                 pMultiIndex->
                                                                 pIndex[0].
                                                                 u4_ULongValue,
                                                                 pMultiData->
                                                                 i4_SLongValue));

}

INT4
FsCapwapWtpRebootStatisticsRowStatusTest (UINT4 *pu4Error,
                                          tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapWtpRebootStatisticsRowStatus (pu4Error,
                                                           pMultiIndex->
                                                           pIndex[0].
                                                           u4_ULongValue,
                                                           pMultiData->
                                                           i4_SLongValue));

}

INT4
FsCapwapWtpRadioLastFailTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapWtpRadioLastFailType (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   i4_SLongValue,
                                                   pMultiData->i4_SLongValue));

}

INT4
FsCapwapWtpRadioResetCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapWtpRadioResetCount (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 i4_SLongValue,
                                                 pMultiData->u4_ULongValue));

}

INT4
FsCapwapWtpRadioSwFailureCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapWtpRadioSwFailureCount (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     i4_SLongValue,
                                                     pMultiData->
                                                     u4_ULongValue));

}

INT4
FsCapwapWtpRadioHwFailureCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapWtpRadioHwFailureCount (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     i4_SLongValue,
                                                     pMultiData->
                                                     u4_ULongValue));

}

INT4
FsCapwapWtpRadioOtherFailureCountTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapWtpRadioOtherFailureCount (pu4Error,
                                                        pMultiIndex->pIndex[0].
                                                        i4_SLongValue,
                                                        pMultiData->
                                                        u4_ULongValue));

}

INT4
FsCapwapWtpRadioUnknownFailureCountTest (UINT4 *pu4Error,
                                         tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapWtpRadioUnknownFailureCount (pu4Error,
                                                          pMultiIndex->
                                                          pIndex[0].
                                                          i4_SLongValue,
                                                          pMultiData->
                                                          u4_ULongValue));

}

INT4
FsCapwapWtpRadioConfigUpdateCountTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapWtpRadioConfigUpdateCount (pu4Error,
                                                        pMultiIndex->pIndex[0].
                                                        i4_SLongValue,
                                                        pMultiData->
                                                        u4_ULongValue));

}

INT4
FsCapwapWtpRadioChannelChangeCountTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapWtpRadioChannelChangeCount (pu4Error,
                                                         pMultiIndex->pIndex[0].
                                                         i4_SLongValue,
                                                         pMultiData->
                                                         u4_ULongValue));

}

INT4
FsCapwapWtpRadioBandChangeCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapWtpRadioBandChangeCount (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      i4_SLongValue,
                                                      pMultiData->
                                                      u4_ULongValue));

}

INT4
FsCapwapWtpRadioCurrentNoiseFloorTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapWtpRadioCurrentNoiseFloor (pu4Error,
                                                        pMultiIndex->pIndex[0].
                                                        i4_SLongValue,
                                                        pMultiData->
                                                        u4_ULongValue));

}

INT4
FsCapwapWtpRadioStatRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapWtpRadioStatRowStatus (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    i4_SLongValue,
                                                    pMultiData->u4_ULongValue));

}

INT4
FsCapwapDot11TxFragmentCntTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapDot11TxFragmentCnt (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 i4_SLongValue,
                                                 pMultiData->u4_ULongValue));

}

INT4
FsCapwapDot11MulticastTxCntTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapDot11MulticastTxCnt (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  i4_SLongValue,
                                                  pMultiData->u4_ULongValue));

}

INT4
FsCapwapDot11FailedCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapDot11FailedCount (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiData->u4_ULongValue));

}

INT4
FsCapwapDot11RetryCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapDot11RetryCount (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiData->u4_ULongValue));

}

INT4
FsCapwapDot11MultipleRetryCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapDot11MultipleRetryCount (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      i4_SLongValue,
                                                      pMultiData->
                                                      u4_ULongValue));

}

INT4
FsCapwapDot11FrameDupCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapDot11FrameDupCount (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 i4_SLongValue,
                                                 pMultiData->u4_ULongValue));

}

INT4
FsCapwapDot11RTSSuccessCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapDot11RTSSuccessCount (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   i4_SLongValue,
                                                   pMultiData->u4_ULongValue));

}

INT4
FsCapwapDot11RTSFailCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapDot11RTSFailCount (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiData->u4_ULongValue));

}

INT4
FsCapwapDot11ACKFailCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapDot11ACKFailCount (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiData->u4_ULongValue));

}

INT4
FsCapwapDot11RxFragmentCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapDot11RxFragmentCount (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   i4_SLongValue,
                                                   pMultiData->u4_ULongValue));

}

INT4
FsCapwapDot11MulticastRxCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapDot11MulticastRxCount (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    i4_SLongValue,
                                                    pMultiData->u4_ULongValue));

}

INT4
FsCapwapDot11FCSErrCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapDot11FCSErrCount (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiData->u4_ULongValue));

}

INT4
FsCapwapDot11TxFrameCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapDot11TxFrameCount (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiData->u4_ULongValue));

}

INT4
FsCapwapDot11DecryptionErrTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapDot11DecryptionErr (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 i4_SLongValue,
                                                 pMultiData->u4_ULongValue));

}

INT4
FsCapwapDot11DiscardQosFragmentCntTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapDot11DiscardQosFragmentCnt (pu4Error,
                                                         pMultiIndex->pIndex[0].
                                                         i4_SLongValue,
                                                         pMultiData->
                                                         u4_ULongValue));

}

INT4
FsCapwapDot11AssociatedStaCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapDot11AssociatedStaCount (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      i4_SLongValue,
                                                      pMultiData->
                                                      u4_ULongValue));

}

INT4
FsCapwapDot11QosCFPollsRecvdCountTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapDot11QosCFPollsRecvdCount (pu4Error,
                                                        pMultiIndex->pIndex[0].
                                                        i4_SLongValue,
                                                        pMultiData->
                                                        u4_ULongValue));

}

INT4
FsCapwapDot11QosCFPollsUnusedCountTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapDot11QosCFPollsUnusedCount (pu4Error,
                                                         pMultiIndex->pIndex[0].
                                                         i4_SLongValue,
                                                         pMultiData->
                                                         u4_ULongValue));

}

INT4
FsCapwapDot11QosCFPollsUnusableCountTest (UINT4 *pu4Error,
                                          tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapDot11QosCFPollsUnusableCount (pu4Error,
                                                           pMultiIndex->
                                                           pIndex[0].
                                                           i4_SLongValue,
                                                           pMultiData->
                                                           u4_ULongValue));

}

INT4
FsCapwapDot11RadioIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapDot11RadioId (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->u4_ULongValue));

}

INT4
CapwapBaseWtpSessionsLimitSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetCapwapBaseWtpSessionsLimit (pMultiData->u4_ULongValue));
}

INT4
CapwapBaseStationSessionsLimitSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetCapwapBaseStationSessionsLimit (pMultiData->u4_ULongValue));
}

INT4
CapwapBaseAcNameListNameSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetCapwapBaseAcNameListName
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
CapwapBaseAcNameListPrioritySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetCapwapBaseAcNameListPriority
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
CapwapBaseAcNameListRowStatusSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetCapwapBaseAcNameListRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
CapwapBaseMacAclStationIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetCapwapBaseMacAclStationId
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
CapwapBaseMacAclRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetCapwapBaseMacAclRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
CapwapBaseWtpProfileNameSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetCapwapBaseWtpProfileName
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
CapwapBaseWtpProfileWtpMacAddressSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhSetCapwapBaseWtpProfileWtpMacAddress
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
CapwapBaseWtpProfileWtpModelNumberSet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhSetCapwapBaseWtpProfileWtpModelNumber
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
CapwapBaseWtpProfileWtpNameSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetCapwapBaseWtpProfileWtpName
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
CapwapBaseWtpProfileWtpLocationSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhSetCapwapBaseWtpProfileWtpLocation
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
CapwapBaseWtpProfileWtpStaticIpEnableSet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    return (nmhSetCapwapBaseWtpProfileWtpStaticIpEnable
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
CapwapBaseWtpProfileWtpStaticIpTypeSet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhSetCapwapBaseWtpProfileWtpStaticIpType
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
CapwapBaseWtpProfileWtpStaticIpAddressSet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    return (nmhSetCapwapBaseWtpProfileWtpStaticIpAddress
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
CapwapBaseWtpProfileWtpNetmaskSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetCapwapBaseWtpProfileWtpNetmask
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
CapwapBaseWtpProfileWtpGatewaySet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetCapwapBaseWtpProfileWtpGateway
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
CapwapBaseWtpProfileWtpFallbackEnableSet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    return (nmhSetCapwapBaseWtpProfileWtpFallbackEnable
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
CapwapBaseWtpProfileWtpEchoIntervalSet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhSetCapwapBaseWtpProfileWtpEchoInterval
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
CapwapBaseWtpProfileWtpIdleTimeoutSet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhSetCapwapBaseWtpProfileWtpIdleTimeout
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
CapwapBaseWtpProfileWtpMaxDiscoveryIntervalSet (tSnmpIndex * pMultiIndex,
                                                tRetVal * pMultiData)
{
    return (nmhSetCapwapBaseWtpProfileWtpMaxDiscoveryInterval
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
CapwapBaseWtpProfileWtpReportIntervalSet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    return (nmhSetCapwapBaseWtpProfileWtpReportInterval
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
CapwapBaseWtpProfileWtpStatisticsTimerSet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    return (nmhSetCapwapBaseWtpProfileWtpStatisticsTimer
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
CapwapBaseWtpProfileWtpEcnSupportSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhSetCapwapBaseWtpProfileWtpEcnSupport
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
CapwapBaseWtpProfileRowStatusSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetCapwapBaseWtpProfileRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
CapwapBaseAcMaxRetransmitSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetCapwapBaseAcMaxRetransmit (pMultiData->u4_ULongValue));
}

INT4
CapwapBaseAcChangeStatePendingTimerSet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetCapwapBaseAcChangeStatePendingTimer
            (pMultiData->u4_ULongValue));
}

INT4
CapwapBaseAcDataCheckTimerSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetCapwapBaseAcDataCheckTimer (pMultiData->u4_ULongValue));
}

INT4
CapwapBaseAcDTLSSessionDeleteTimerSet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetCapwapBaseAcDTLSSessionDeleteTimer
            (pMultiData->u4_ULongValue));
}

INT4
CapwapBaseAcEchoIntervalSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetCapwapBaseAcEchoInterval (pMultiData->u4_ULongValue));
}

INT4
CapwapBaseAcRetransmitIntervalSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetCapwapBaseAcRetransmitInterval (pMultiData->u4_ULongValue));
}

INT4
CapwapBaseAcSilentIntervalSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetCapwapBaseAcSilentInterval (pMultiData->u4_ULongValue));
}

INT4
CapwapBaseAcWaitDTLSTimerSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetCapwapBaseAcWaitDTLSTimer (pMultiData->u4_ULongValue));
}

INT4
CapwapBaseAcWaitJoinTimerSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetCapwapBaseAcWaitJoinTimer (pMultiData->u4_ULongValue));
}

INT4
CapwapBaseAcEcnSupportSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetCapwapBaseAcEcnSupport (pMultiData->i4_SLongValue));
}

INT4
CapwapBaseChannelUpDownNotifyEnableSet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetCapwapBaseChannelUpDownNotifyEnable
            (pMultiData->i4_SLongValue));
}

INT4
CapwapBaseDecryptErrorNotifyEnableSet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetCapwapBaseDecryptErrorNotifyEnable
            (pMultiData->i4_SLongValue));
}

INT4
CapwapBaseJoinFailureNotifyEnableSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetCapwapBaseJoinFailureNotifyEnable
            (pMultiData->i4_SLongValue));
}

INT4
CapwapBaseImageUpgradeFailureNotifyEnableSet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetCapwapBaseImageUpgradeFailureNotifyEnable
            (pMultiData->i4_SLongValue));
}

INT4
CapwapBaseConfigMsgErrorNotifyEnableSet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetCapwapBaseConfigMsgErrorNotifyEnable
            (pMultiData->i4_SLongValue));
}

INT4
CapwapBaseRadioOperableStatusNotifyEnableSet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetCapwapBaseRadioOperableStatusNotifyEnable
            (pMultiData->i4_SLongValue));
}

INT4
CapwapBaseAuthenFailureNotifyEnableSet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetCapwapBaseAuthenFailureNotifyEnable
            (pMultiData->i4_SLongValue));
}

INT4
FsCapwapModuleStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsCapwapEnable (pMultiData->i4_SLongValue));
}

INT4
FsCapwapSystemControlSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsCapwapShutdown (pMultiData->i4_SLongValue));
}

INT4
FsCapwapControlUdpPortSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsCapwapControlUdpPort (pMultiData->u4_ULongValue));
}

INT4
FsCapwapControlChannelDTLSPolicyOptionsSet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsCapwapControlChannelDTLSPolicyOptions
            (pMultiData->pOctetStrValue));
}

INT4
FsCapwapDataChannelDTLSPolicyOptionsSet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsCapwapDataChannelDTLSPolicyOptions
            (pMultiData->pOctetStrValue));
}

INT4
FsWlcDiscoveryModeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsWlcDiscoveryMode (pMultiData->pOctetStrValue));
}

INT4
FsCapwapWtpModeIgnoreSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsCapwapWtpModeIgnore (pMultiData->i4_SLongValue));
}

INT4
FsCapwapDebugMaskSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsCapwapDebugMask (pMultiData->i4_SLongValue));
}

INT4
FsDtlsDebugMaskSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsDtlsDebugMask (pMultiData->i4_SLongValue));
}

INT4
FsDtlsEncryptionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsDtlsEncryption (pMultiData->i4_SLongValue));
}

INT4
FsDtlsEncryptAlgorithmSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsDtlsEncryptAlogritham (pMultiData->i4_SLongValue));
}

INT4
FsStationTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsStationType (pMultiData->i4_SLongValue));
}

INT4
FsAcTimestampTriggerSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsAcTimestampTrigger (pMultiData->i4_SLongValue));
}

INT4
FsCapwapWssDataDebugMaskSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsCapwapWssDataDebugMask (pMultiData->i4_SLongValue));
}

INT4
FsNoOfRadioSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsNoOfRadio
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapWtpMacTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsCapwapWtpMacType (pMultiIndex->pIndex[0].pOctetStrValue,
                                      pMultiData->i4_SLongValue));

}

INT4
FsCapwapWtpTunnelModeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsCapwapWtpTunnelMode (pMultiIndex->pIndex[0].pOctetStrValue,
                                         pMultiData->pOctetStrValue));

}

INT4
FsCapwapImageNameSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsCapwapImageName (pMultiIndex->pIndex[0].pOctetStrValue,
                                     pMultiData->pOctetStrValue));

}

INT4
FsCapwapQosProfileNameSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsCapwapQosProfileName (pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiData->pOctetStrValue));

}

INT4
FsMaxStationsSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMaxStations (pMultiIndex->pIndex[0].pOctetStrValue,
                                 pMultiData->i4_SLongValue));

}

INT4
FsWtpModelRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsWtpModelRowStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                       pMultiData->i4_SLongValue));

}

INT4
FsWtpRadioTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsWtpRadioType (pMultiIndex->pIndex[0].pOctetStrValue,
                                  pMultiIndex->pIndex[1].u4_ULongValue,
                                  pMultiData->u4_ULongValue));

}

INT4
FsWtpModelLocalRoutingSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsWtpModelLocalRouting (pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsWtpModelWlanBindingRowStatusSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetFsWtpModelWlanBindingRowStatus
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsRadioAdminStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRadioAdminStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiData->i4_SLongValue));

}

INT4
FsMaxSSIDSupportedSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMaxSSIDSupported (pMultiIndex->pIndex[0].pOctetStrValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiData->i4_SLongValue));

}

INT4
FsWtpRadioRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsWtpRadioRowStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
FsCapwapWhiteListWtpBaseMacSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsCapwapWhiteListWtpBaseMac
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
FsCapwapWhiteListRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsCapwapWhiteListRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsCapwapBlackListWtpBaseMacSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsCapwapBlackListWtpBaseMac
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
FsCapwapBlackListRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsCapwapBlackListRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsCapwapWtpResetSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsCapwapWtpReset (pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiData->i4_SLongValue));

}

INT4
FsCapwapClearConfigSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsCapwapClearConfig (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
FsWtpDiscoveryTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsWtpDiscoveryType (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiData->i4_SLongValue));

}

INT4
FsWtpCountryStringSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsWtpCountryString (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiData->pOctetStrValue));

}

INT4
FsCapwapClearApStatsSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsCapwapClearApStats (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiData->i4_SLongValue));
}

INT4
FsWtpCrashDumpFileNameSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsWtpCrashDumpFileName (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiData->pOctetStrValue));

}

INT4
FsWtpMemoryDumpFileNameSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsWtpMemoryDumpFileName (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiData->pOctetStrValue));

}

INT4
FsWtpDeleteOperationSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsWtpDeleteOperation (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
FsCapwapWtpConfigRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsCapwapWtpConfigRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsWlcIpAddressTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsWlcIpAddressType (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiData->i4_SLongValue));

}

INT4
FsWlcIpAddressSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsWlcIpAddress (pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiData->pOctetStrValue));

}

INT4
FsCapwapEncryptChannelStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsCapwapEncryptChannelStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsCapwapEncryptChannelRowStatusSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhSetFsCapwapEncryptChannelRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsCapwapDefaultWtpProfileModelNumberSet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    return (nmhSetFsCapwapDefaultWtpProfileModelNumber
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
FsCapwapDefaultWtpProfileWtpFallbackEnableSet (tSnmpIndex * pMultiIndex,
                                               tRetVal * pMultiData)
{
    return (nmhSetFsCapwapDefaultWtpProfileWtpFallbackEnable
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsCapwapDefaultWtpProfileWtpEchoIntervalSet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    return (nmhSetFsCapwapDefaultWtpProfileWtpEchoInterval
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapDefaultWtpProfileWtpIdleTimeoutSet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    return (nmhSetFsCapwapDefaultWtpProfileWtpIdleTimeout
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapDefaultWtpProfileWtpMaxDiscoveryIntervalSet (tSnmpIndex * pMultiIndex,
                                                     tRetVal * pMultiData)
{
    return (nmhSetFsCapwapDefaultWtpProfileWtpMaxDiscoveryInterval
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapDefaultWtpProfileWtpReportIntervalSet (tSnmpIndex * pMultiIndex,
                                               tRetVal * pMultiData)
{
    return (nmhSetFsCapwapDefaultWtpProfileWtpReportInterval
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapDefaultWtpProfileWtpStatisticsTimerSet (tSnmpIndex * pMultiIndex,
                                                tRetVal * pMultiData)
{
    return (nmhSetFsCapwapDefaultWtpProfileWtpStatisticsTimer
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapDefaultWtpProfileWtpEcnSupportSet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    return (nmhSetFsCapwapDefaultWtpProfileWtpEcnSupport
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsCapwapDefaultWtpProfileRowStatusSet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhSetFsCapwapDefaultWtpProfileRowStatus
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsCapwapDnsServerIpSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsCapwapDnsServerIp (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiData->pOctetStrValue));

}

INT4
FsCapwapDnsDomainNameSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsCapwapDnsDomainName (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiData->pOctetStrValue));

}

INT4
FsCapwapDnsProfileRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsCapwapDnsProfileRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsWtpNativeVlanIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsWtpNativeVlanId (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiData->i4_SLongValue));

}

INT4
FsWtpNativeVlanIdRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsWtpNativeVlanIdRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsWtpLocalRoutingSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsWtpLocalRouting (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiData->i4_SLongValue));

}

INT4
FsWtpLocalRoutingRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsWtpLocalRoutingRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsCapwapFsAcTimestampTriggerSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsCapwapFsAcTimestampTrigger
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsCapwapFragReassembleStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsCapwapFragReassembleStatus
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsCapwapWtpImageNameSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsCapwapWtpImageName (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiData->pOctetStrValue));

}

INT4
FsCapwapDiscStatsRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsCapwapDiscStatsRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsCapwapJoinStatsRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsCapwapJoinStatsRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsCapwapConfigStatsRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsCapwapConfigStatsRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsCapwapRunStatsRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsCapwapRunStatsRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsCapwapWirelessBindingVirtualRadioIfIndexSet (tSnmpIndex * pMultiIndex,
                                               tRetVal * pMultiData)
{
    return (nmhSetFsCapwapWirelessBindingVirtualRadioIfIndex
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsCapwapWirelessBindingTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsCapwapWirelessBindingType
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsCapwapWirelessBindingRowStatusSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhSetFsCapwapWirelessBindingRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsCapwapStationWhiteListRowStatusSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhSetFsCapwapStationWhiteListRowStatus
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsCapwapWtpRebootStatisticsRebootCountSet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    return (nmhSetFsCapwapWtpRebootStatisticsRebootCount
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapWtpRebootStatisticsAcInitiatedCountSet (tSnmpIndex * pMultiIndex,
                                                tRetVal * pMultiData)
{
    return (nmhSetFsCapwapWtpRebootStatisticsAcInitiatedCount
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapWtpRebootStatisticsLinkFailureCountSet (tSnmpIndex * pMultiIndex,
                                                tRetVal * pMultiData)
{
    return (nmhSetFsCapwapWtpRebootStatisticsLinkFailureCount
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapWtpRebootStatisticsSwFailureCountSet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    return (nmhSetFsCapwapWtpRebootStatisticsSwFailureCount
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapWtpRebootStatisticsHwFailureCountSet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    return (nmhSetFsCapwapWtpRebootStatisticsHwFailureCount
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapWtpRebootStatisticsOtherFailureCountSet (tSnmpIndex * pMultiIndex,
                                                 tRetVal * pMultiData)
{
    return (nmhSetFsCapwapWtpRebootStatisticsOtherFailureCount
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapWtpRebootStatisticsUnknownFailureCountSet (tSnmpIndex * pMultiIndex,
                                                   tRetVal * pMultiData)
{
    return (nmhSetFsCapwapWtpRebootStatisticsUnknownFailureCount
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapWtpRebootStatisticsLastFailureTypeSet (tSnmpIndex * pMultiIndex,
                                               tRetVal * pMultiData)
{
    return (nmhSetFsCapwapWtpRebootStatisticsLastFailureType
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsCapwapWtpRebootStatisticsRowStatusSet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    return (nmhSetFsCapwapWtpRebootStatisticsRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsCapwapWtpRadioLastFailTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsCapwapWtpRadioLastFailType
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsCapwapWtpRadioResetCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsCapwapWtpRadioResetCount
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapWtpRadioSwFailureCountSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetFsCapwapWtpRadioSwFailureCount
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapWtpRadioHwFailureCountSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetFsCapwapWtpRadioHwFailureCount
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapWtpRadioOtherFailureCountSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhSetFsCapwapWtpRadioOtherFailureCount
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapWtpRadioUnknownFailureCountSet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhSetFsCapwapWtpRadioUnknownFailureCount
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapWtpRadioConfigUpdateCountSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhSetFsCapwapWtpRadioConfigUpdateCount
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapWtpRadioChannelChangeCountSet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhSetFsCapwapWtpRadioChannelChangeCount
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapWtpRadioBandChangeCountSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhSetFsCapwapWtpRadioBandChangeCount
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapWtpRadioCurrentNoiseFloorSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhSetFsCapwapWtpRadioCurrentNoiseFloor
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapWtpRadioStatRowStatusSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetFsCapwapWtpRadioStatRowStatus
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapDot11TxFragmentCntSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsCapwapDot11TxFragmentCnt
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapDot11MulticastTxCntSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsCapwapDot11MulticastTxCnt
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapDot11FailedCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsCapwapDot11FailedCount
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapDot11RetryCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsCapwapDot11RetryCount (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->u4_ULongValue));

}

INT4
FsCapwapDot11MultipleRetryCountSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhSetFsCapwapDot11MultipleRetryCount
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapDot11FrameDupCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsCapwapDot11FrameDupCount
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapDot11RTSSuccessCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsCapwapDot11RTSSuccessCount
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapDot11RTSFailCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsCapwapDot11RTSFailCount
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapDot11ACKFailCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsCapwapDot11ACKFailCount
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapDot11RxFragmentCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsCapwapDot11RxFragmentCount
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapDot11MulticastRxCountSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetFsCapwapDot11MulticastRxCount
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapDot11FCSErrCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsCapwapDot11FCSErrCount
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapDot11TxFrameCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsCapwapDot11TxFrameCount
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapDot11DecryptionErrSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsCapwapDot11DecryptionErr
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapDot11DiscardQosFragmentCntSet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhSetFsCapwapDot11DiscardQosFragmentCnt
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapDot11AssociatedStaCountSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhSetFsCapwapDot11AssociatedStaCount
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapDot11QosCFPollsRecvdCountSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhSetFsCapwapDot11QosCFPollsRecvdCount
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapDot11QosCFPollsUnusedCountSet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhSetFsCapwapDot11QosCFPollsUnusedCount
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapDot11QosCFPollsUnusableCountSet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    return (nmhSetFsCapwapDot11QosCFPollsUnusableCount
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapDot11RadioIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsCapwapDot11RadioId (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->u4_ULongValue));

}

INT4
CapwapBaseWtpSessionsLimitDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2CapwapBaseWtpSessionsLimit
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
CapwapBaseStationSessionsLimitDep (UINT4 *pu4Error,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2CapwapBaseStationSessionsLimit
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
CapwapBaseAcNameListTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2CapwapBaseAcNameListTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
CapwapBaseMacAclTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2CapwapBaseMacAclTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
CapwapBaseWtpProfileTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2CapwapBaseWtpProfileTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
CapwapBaseAcMaxRetransmitDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2CapwapBaseAcMaxRetransmit
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
CapwapBaseAcChangeStatePendingTimerDep (UINT4 *pu4Error,
                                        tSnmpIndexList * pSnmpIndexList,
                                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2CapwapBaseAcChangeStatePendingTimer
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
CapwapBaseAcDataCheckTimerDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2CapwapBaseAcDataCheckTimer
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
CapwapBaseAcDTLSSessionDeleteTimerDep (UINT4 *pu4Error,
                                       tSnmpIndexList * pSnmpIndexList,
                                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2CapwapBaseAcDTLSSessionDeleteTimer
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
CapwapBaseAcEchoIntervalDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2CapwapBaseAcEchoInterval
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
CapwapBaseAcRetransmitIntervalDep (UINT4 *pu4Error,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2CapwapBaseAcRetransmitInterval
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
CapwapBaseAcSilentIntervalDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2CapwapBaseAcSilentInterval
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
CapwapBaseAcWaitDTLSTimerDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2CapwapBaseAcWaitDTLSTimer
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
CapwapBaseAcWaitJoinTimerDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2CapwapBaseAcWaitJoinTimer
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
CapwapBaseAcEcnSupportDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2CapwapBaseAcEcnSupport
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
CapwapBaseChannelUpDownNotifyEnableDep (UINT4 *pu4Error,
                                        tSnmpIndexList * pSnmpIndexList,
                                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2CapwapBaseChannelUpDownNotifyEnable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
CapwapBaseDecryptErrorNotifyEnableDep (UINT4 *pu4Error,
                                       tSnmpIndexList * pSnmpIndexList,
                                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2CapwapBaseDecryptErrorNotifyEnable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
CapwapBaseJoinFailureNotifyEnableDep (UINT4 *pu4Error,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2CapwapBaseJoinFailureNotifyEnable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
CapwapBaseImageUpgradeFailureNotifyEnableDep (UINT4 *pu4Error,
                                              tSnmpIndexList * pSnmpIndexList,
                                              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2CapwapBaseImageUpgradeFailureNotifyEnable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
CapwapBaseConfigMsgErrorNotifyEnableDep (UINT4 *pu4Error,
                                         tSnmpIndexList * pSnmpIndexList,
                                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2CapwapBaseConfigMsgErrorNotifyEnable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
CapwapBaseRadioOperableStatusNotifyEnableDep (UINT4 *pu4Error,
                                              tSnmpIndexList * pSnmpIndexList,
                                              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2CapwapBaseRadioOperableStatusNotifyEnable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
CapwapBaseAuthenFailureNotifyEnableDep (UINT4 *pu4Error,
                                        tSnmpIndexList * pSnmpIndexList,
                                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2CapwapBaseAuthenFailureNotifyEnable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsCapwapModuleStatusDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsCapwapEnable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsCapwapSystemControlDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsCapwapShutdown (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsCapwapControlUdpPortDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsCapwapControlUdpPort
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsCapwapControlChannelDTLSPolicyOptionsDep (UINT4 *pu4Error,
                                            tSnmpIndexList * pSnmpIndexList,
                                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsCapwapControlChannelDTLSPolicyOptions
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsCapwapDataChannelDTLSPolicyOptionsDep (UINT4 *pu4Error,
                                         tSnmpIndexList * pSnmpIndexList,
                                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsCapwapDataChannelDTLSPolicyOptions
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsWlcDiscoveryModeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsWlcDiscoveryMode
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsCapwapWtpModeIgnoreDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsCapwapWtpModeIgnore
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsCapwapDebugMaskDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsCapwapDebugMask
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsDtlsDebugMaskDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsDtlsDebugMask (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsDtlsEncryptionDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsDtlsEncryption (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsDtlsEncryptAlgorithmDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsDtlsEncryptAlogritham
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsStationTypeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                  tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsStationType (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsAcTimestampTriggerDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsAcTimestampTrigger
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsCapwapWssDataDebugMaskDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsCapwapWssDataDebugMask
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsWtpModelTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsWtpModelTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsWtpRadioTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsWtpRadioTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsWtpExtModelTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsWtpExtModelTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsWtpModelWlanBindingTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsWtpModelWlanBindingTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsCapwapWhiteListTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsCapwapWhiteListTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsCapwapBlackListTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsCapwapBlackList
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsCapwapWtpConfigTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsCapwapWtpConfigTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsCapwapExtWtpConfigTableDep (UINT4 *pu4Error,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsCapwapExtWtpConfigTable (pu4Error,
                                               pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsCapwapLinkEncryptionTableDep (UINT4 *pu4Error,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsCapwapLinkEncryptionTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsCapwapDefaultWtpProfileTableDep (UINT4 *pu4Error,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsCawapDefaultWtpProfileTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsCapwapDnsProfileTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsCapwapDnsProfileTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsWtpNativeVlanIdTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsWtpNativeVlanIdTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsWtpLocalRoutingTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsWtpLocalRoutingTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsCapwapFsAcTimestampTriggerTableDep (UINT4 *pu4Error,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsCapwapFsAcTimestampTriggerTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsCawapDiscStatsTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsCawapDiscStatsTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsCawapJoinStatsTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsCawapJoinStatsTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsCawapConfigStatsTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsCawapConfigStatsTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsCawapRunStatsTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsCawapRunStatsTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsCapwapWirelessBindingTableDep (UINT4 *pu4Error,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsCapwapWirelessBindingTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsCapwapStationWhiteListTableDep (UINT4 *pu4Error,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsCapwapStationWhiteListTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsCapwapWtpRebootStatisticsTableDep (UINT4 *pu4Error,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsCapwapWtpRebootStatisticsTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsCapwapWtpRadioStatisticsTableDep (UINT4 *pu4Error,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsCapwapWtpRadioStatisticsTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsCapwapDot11StatisticsTableDep (UINT4 *pu4Error,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsCapwapDot11StatisticsTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsCapwATPStatsTable (tSnmpIndex * pFirstMultiIndex,
                                 tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsCapwATPStatsTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsCapwATPStatsTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsCapwATPStatsSentBytesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwATPStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwATPStatsSentBytes (pMultiIndex->pIndex[0].u4_ULongValue,
                                           &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwATPStatsRcvdBytesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwATPStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwATPStatsRcvdBytes (pMultiIndex->pIndex[0].u4_ULongValue,
                                           &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwATPStatsWiredSentBytesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwATPStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwATPStatsWiredSentBytes
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwATPStatsWirelessSentBytesGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwATPStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwATPStatsWirelessSentBytes
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwATPStatsWiredRcvdBytesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwATPStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwATPStatsWiredRcvdBytes
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwATPStatsWirelessRcvdBytesGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwATPStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwATPStatsWirelessRcvdBytes
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwATPStatsSentTrafficRateGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwATPStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwATPStatsSentTrafficRate
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwATPStatsRcvdTrafficRateGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwATPStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwATPStatsRcvdTrafficRate
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwATPStatsTotalClientGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwATPStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwATPStatsTotalClient
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwATPStatsMaxClientPerAPRadioGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwATPStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwATPStatsMaxClientPerAPRadio
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwATPStatsMaxClientPerSSIDGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwATPStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwATPStatsMaxClientPerSSID
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCapwATPStatsMaxClientPerAPRadioSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhSetFsCapwATPStatsMaxClientPerAPRadio
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwATPStatsMaxClientPerSSIDSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetFsCapwATPStatsMaxClientPerSSID
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwATPStatsMaxClientPerAPRadioTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwATPStatsMaxClientPerAPRadio (pu4Error,
                                                        pMultiIndex->pIndex[0].
                                                        u4_ULongValue,
                                                        pMultiData->
                                                        u4_ULongValue));

}

INT4
FsCapwATPStatsMaxClientPerSSIDTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwATPStatsMaxClientPerSSID (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     u4_ULongValue,
                                                     pMultiData->
                                                     u4_ULongValue));

}

INT4
FsCapwATPStatsTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsCapwATPStatsTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsCapwapMaxRetransmitCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsCapwapMaxRetransmitCount (&(pMultiData->u4_ULongValue)));
}

INT4
FsCapwapMaxRetransmitCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsCapwapMaxRetransmitCount (pMultiData->u4_ULongValue));
}

INT4
FsCapwapMaxRetransmitCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsCapwapMaxRetransmitCount
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
FsCapwapMaxRetransmitCountDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsCapwapMaxRetransmitCount
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsCapwTrapStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsCapwTrapStatus (&(pMultiData->i4_SLongValue)));
}

INT4
FsCapwCriticalTrapStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsCapwCriticalTrapStatus (&(pMultiData->i4_SLongValue)));
}

INT4
FsCapwTrapStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsCapwTrapStatus (pMultiData->i4_SLongValue));
}

INT4
FsCapwCriticalTrapStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsCapwCriticalTrapStatus (pMultiData->i4_SLongValue));
}

INT4
FsCapwTrapStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsCapwTrapStatus (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsCapwCriticalTrapStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsCapwCriticalTrapStatus
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsCapwTrapStatusDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsCapwTrapStatus (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsCapwCriticalTrapStatusDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsCapwCriticalTrapStatus
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsCapwapStationBlackListTable (tSnmpIndex * pFirstMultiIndex,
                                           tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsCapwapStationBlackListTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsCapwapStationBlackListTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsCapwapStationBlackListRowStatusGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwapStationBlackListTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapStationBlackListRowStatus
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsCapwapStationBlackListRowStatusSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhSetFsCapwapStationBlackListRowStatus
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsCapwapStationBlackListRowStatusTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapStationBlackListRowStatus (pu4Error,
                                                        pMultiIndex->pIndex[0].
                                                        pOctetStrValue,
                                                        pMultiData->
                                                        i4_SLongValue));

}

INT4
FsCapwapStationBlackListTableDep (UINT4 *pu4Error,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsCapwapStationBlackListTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

/*---------------------------------------------------------------------------------------------------------------*/
INT4
FsCapwapDefaultQosProfileGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwapDefaultWtpProfileTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapDefaultQosProfile
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
FsCapwapDefaultQosProfileSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsCapwapDefaultQosProfile
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
FsCapwapDefaultQosProfileTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapDefaultQosProfile (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                pOctetStrValue,
                                                pMultiData->pOctetStrValue));

}

INT4
FsCapwapDnsAddressTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCapwapDnsProfileTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCapwapDnsAddressType (pMultiIndex->pIndex[0].u4_ULongValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
FsCapwapDnsAddressTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsCapwapDnsAddressType (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsCapwapDnsAddressTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapDnsAddressType (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             u4_ULongValue,
                                             pMultiData->i4_SLongValue));

}

INT4
FsCapwapDiscReqReceivedSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsCapwapDiscReqReceived (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiData->u4_ULongValue));

}

INT4
FsCapwapDiscReqReceivedTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapDiscReqReceived (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              u4_ULongValue,
                                              pMultiData->u4_ULongValue));

}

INT4
FsCapwapDiscRspReceivedSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsCapwapDiscRspReceived (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiData->u4_ULongValue));

}

INT4
FsCapwapDiscRspReceivedTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapDiscRspReceived (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              u4_ULongValue,
                                              pMultiData->u4_ULongValue));

}

INT4
FsCapwapDiscReqTransmittedSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsCapwapDiscReqTransmitted
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapDiscReqTransmittedTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapDiscReqTransmitted (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 u4_ULongValue,
                                                 pMultiData->u4_ULongValue));

}

INT4
FsCapwapDiscRspTransmittedSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsCapwapDiscRspTransmitted
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapDiscRspTransmittedTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapDiscRspTransmitted (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 u4_ULongValue,
                                                 pMultiData->u4_ULongValue));

}

INT4
FsCapwapDiscunsuccessfulProcessedSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhSetFsCapwapDiscunsuccessfulProcessed
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapDiscunsuccessfulProcessedTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapDiscunsuccessfulProcessed (pu4Error,
                                                        pMultiIndex->pIndex[0].
                                                        u4_ULongValue,
                                                        pMultiData->
                                                        u4_ULongValue));

}

INT4
FsCapwapDiscLastUnsuccAttemptReasonSet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhSetFsCapwapDiscLastUnsuccAttemptReason
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsCapwapDiscLastUnsuccAttemptReasonTest (UINT4 *pu4Error,
                                         tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapDiscLastUnsuccAttemptReason (pu4Error,
                                                          pMultiIndex->
                                                          pIndex[0].
                                                          u4_ULongValue,
                                                          pMultiData->
                                                          i4_SLongValue));

}

INT4
FsCapwapDiscLastSuccAttemptTimeSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhSetFsCapwapDiscLastSuccAttemptTime
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapDiscLastSuccAttemptTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapDiscLastSuccAttemptTime (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      u4_ULongValue,
                                                      pMultiData->
                                                      u4_ULongValue));

}

INT4
FsCapwapDiscLastUnsuccessfulAttemptTimeSet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    return (nmhSetFsCapwapDiscLastUnsuccessfulAttemptTime
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapDiscLastUnsuccessfulAttemptTimeTest (UINT4 *pu4Error,
                                             tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapDiscLastUnsuccessfulAttemptTime (pu4Error,
                                                              pMultiIndex->
                                                              pIndex[0].
                                                              u4_ULongValue,
                                                              pMultiData->
                                                              u4_ULongValue));

}

INT4
FsCapwapJoinReqReceivedSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsCapwapJoinReqReceived (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiData->u4_ULongValue));

}

INT4
FsCapwapJoinReqReceivedTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapJoinReqReceived (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              u4_ULongValue,
                                              pMultiData->u4_ULongValue));

}

INT4
FsCapwapJoinRspReceivedSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsCapwapJoinRspReceived (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiData->u4_ULongValue));

}

INT4
FsCapwapJoinRspReceivedTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapJoinRspReceived (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              u4_ULongValue,
                                              pMultiData->u4_ULongValue));

}

INT4
FsCapwapJoinReqTransmittedSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsCapwapJoinReqTransmitted
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapJoinReqTransmittedTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapJoinReqTransmitted (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 u4_ULongValue,
                                                 pMultiData->u4_ULongValue));

}

INT4
FsCapwapJoinRspTransmittedSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsCapwapJoinRspTransmitted
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapJoinRspTransmittedTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapJoinRspTransmitted (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 u4_ULongValue,
                                                 pMultiData->u4_ULongValue));

}

INT4
FsCapwapJoinunsuccessfulProcessedSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhSetFsCapwapJoinunsuccessfulProcessed
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapJoinunsuccessfulProcessedTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapJoinunsuccessfulProcessed (pu4Error,
                                                        pMultiIndex->pIndex[0].
                                                        u4_ULongValue,
                                                        pMultiData->
                                                        u4_ULongValue));

}

INT4
FsCapwapJoinReasonLastUnsuccAttemptSet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhSetFsCapwapJoinReasonLastUnsuccAttempt
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsCapwapJoinReasonLastUnsuccAttemptTest (UINT4 *pu4Error,
                                         tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapJoinReasonLastUnsuccAttempt (pu4Error,
                                                          pMultiIndex->
                                                          pIndex[0].
                                                          u4_ULongValue,
                                                          pMultiData->
                                                          i4_SLongValue));

}

INT4
FsCapwapJoinLastSuccAttemptTimeSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhSetFsCapwapJoinLastSuccAttemptTime
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapJoinLastSuccAttemptTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapJoinLastSuccAttemptTime (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      u4_ULongValue,
                                                      pMultiData->
                                                      u4_ULongValue));

}

INT4
FsCapwapJoinLastUnsuccAttemptTimeSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhSetFsCapwapJoinLastUnsuccAttemptTime
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapJoinLastUnsuccAttemptTimeTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapJoinLastUnsuccAttemptTime (pu4Error,
                                                        pMultiIndex->pIndex[0].
                                                        u4_ULongValue,
                                                        pMultiData->
                                                        u4_ULongValue));

}

INT4
FsCapwapConfigReqReceivedSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsCapwapConfigReqReceived
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapConfigReqReceivedTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapConfigReqReceived (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                u4_ULongValue,
                                                pMultiData->u4_ULongValue));

}

INT4
FsCapwapConfigRspReceivedSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsCapwapConfigRspReceived
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapConfigRspReceivedTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapConfigRspReceived (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                u4_ULongValue,
                                                pMultiData->u4_ULongValue));

}

INT4
FsCapwapConfigReqTransmittedSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsCapwapConfigReqTransmitted
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapConfigReqTransmittedTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapConfigReqTransmitted (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   u4_ULongValue,
                                                   pMultiData->u4_ULongValue));

}

INT4
FsCapwapConfigRspTransmittedSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsCapwapConfigRspTransmitted
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapConfigRspTransmittedTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapConfigRspTransmitted (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   u4_ULongValue,
                                                   pMultiData->u4_ULongValue));

}

INT4
FsCapwapConfigunsuccessfulProcessedSet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhSetFsCapwapConfigunsuccessfulProcessed
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapConfigunsuccessfulProcessedTest (UINT4 *pu4Error,
                                         tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapConfigunsuccessfulProcessed (pu4Error,
                                                          pMultiIndex->
                                                          pIndex[0].
                                                          u4_ULongValue,
                                                          pMultiData->
                                                          u4_ULongValue));

}

INT4
FsCapwapConfigReasonLastUnsuccAttemptSet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    return (nmhSetFsCapwapConfigReasonLastUnsuccAttempt
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsCapwapConfigReasonLastUnsuccAttemptTest (UINT4 *pu4Error,
                                           tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapConfigReasonLastUnsuccAttempt (pu4Error,
                                                            pMultiIndex->
                                                            pIndex[0].
                                                            u4_ULongValue,
                                                            pMultiData->
                                                            i4_SLongValue));

}

INT4
FsCapwapConfigLastSuccAttemptTimeSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhSetFsCapwapConfigLastSuccAttemptTime
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapConfigLastSuccAttemptTimeTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapConfigLastSuccAttemptTime (pu4Error,
                                                        pMultiIndex->pIndex[0].
                                                        u4_ULongValue,
                                                        pMultiData->
                                                        u4_ULongValue));

}

INT4
FsCapwapConfigLastUnsuccessfulAttemptTimeSet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    return (nmhSetFsCapwapConfigLastUnsuccessfulAttemptTime
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapConfigLastUnsuccessfulAttemptTimeTest (UINT4 *pu4Error,
                                               tSnmpIndex * pMultiIndex,
                                               tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapConfigLastUnsuccessfulAttemptTime (pu4Error,
                                                                pMultiIndex->
                                                                pIndex[0].
                                                                u4_ULongValue,
                                                                pMultiData->
                                                                u4_ULongValue));

}

INT4
FsCapwapRunConfigUpdateReqReceivedSet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhSetFsCapwapRunConfigUpdateReqReceived
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapRunConfigUpdateReqReceivedTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapRunConfigUpdateReqReceived (pu4Error,
                                                         pMultiIndex->pIndex[0].
                                                         u4_ULongValue,
                                                         pMultiData->
                                                         u4_ULongValue));

}

INT4
FsCapwapRunConfigUpdateRspReceivedSet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhSetFsCapwapRunConfigUpdateRspReceived
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapRunConfigUpdateRspReceivedTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapRunConfigUpdateRspReceived (pu4Error,
                                                         pMultiIndex->pIndex[0].
                                                         u4_ULongValue,
                                                         pMultiData->
                                                         u4_ULongValue));

}

INT4
FsCapwapRunConfigUpdateReqTransmittedSet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    return (nmhSetFsCapwapRunConfigUpdateReqTransmitted
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapRunConfigUpdateReqTransmittedTest (UINT4 *pu4Error,
                                           tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapRunConfigUpdateReqTransmitted (pu4Error,
                                                            pMultiIndex->
                                                            pIndex[0].
                                                            u4_ULongValue,
                                                            pMultiData->
                                                            u4_ULongValue));

}

INT4
FsCapwapRunConfigUpdateRspTransmittedSet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    return (nmhSetFsCapwapRunConfigUpdateRspTransmitted
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapRunConfigUpdateRspTransmittedTest (UINT4 *pu4Error,
                                           tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapRunConfigUpdateRspTransmitted (pu4Error,
                                                            pMultiIndex->
                                                            pIndex[0].
                                                            u4_ULongValue,
                                                            pMultiData->
                                                            u4_ULongValue));

}

INT4
FsCapwapRunConfigUpdateunsuccessfulProcessedSet (tSnmpIndex * pMultiIndex,
                                                 tRetVal * pMultiData)
{
    return (nmhSetFsCapwapRunConfigUpdateunsuccessfulProcessed
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapRunConfigUpdateunsuccessfulProcessedTest (UINT4 *pu4Error,
                                                  tSnmpIndex * pMultiIndex,
                                                  tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapRunConfigUpdateunsuccessfulProcessed (pu4Error,
                                                                   pMultiIndex->
                                                                   pIndex[0].
                                                                   u4_ULongValue,
                                                                   pMultiData->
                                                                   u4_ULongValue));

}

INT4
FsCapwapRunConfigUpdateReasonLastUnsuccAttemptSet (tSnmpIndex * pMultiIndex,
                                                   tRetVal * pMultiData)
{
    return (nmhSetFsCapwapRunConfigUpdateReasonLastUnsuccAttempt
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
FsCapwapRunConfigUpdateReasonLastUnsuccAttemptTest (UINT4 *pu4Error,
                                                    tSnmpIndex * pMultiIndex,
                                                    tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapRunConfigUpdateReasonLastUnsuccAttempt (pu4Error,
                                                                     pMultiIndex->
                                                                     pIndex[0].
                                                                     u4_ULongValue,
                                                                     pMultiData->
                                                                     pOctetStrValue));

}

INT4
FsCapwapRunConfigUpdateLastSuccAttemptTimeSet (tSnmpIndex * pMultiIndex,
                                               tRetVal * pMultiData)
{
    return (nmhSetFsCapwapRunConfigUpdateLastSuccAttemptTime
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
FsCapwapRunConfigUpdateLastSuccAttemptTimeTest (UINT4 *pu4Error,
                                                tSnmpIndex * pMultiIndex,
                                                tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapRunConfigUpdateLastSuccAttemptTime (pu4Error,
                                                                 pMultiIndex->
                                                                 pIndex[0].
                                                                 u4_ULongValue,
                                                                 pMultiData->
                                                                 pOctetStrValue));

}

INT4
FsCapwapRunConfigUpdateLastUnsuccessfulAttemptTimeSet (tSnmpIndex * pMultiIndex,
                                                       tRetVal * pMultiData)
{
    return (nmhSetFsCapwapRunConfigUpdateLastUnsuccessfulAttemptTime
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
FsCapwapRunConfigUpdateLastUnsuccessfulAttemptTimeTest (UINT4 *pu4Error,
                                                        tSnmpIndex *
                                                        pMultiIndex,
                                                        tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapRunConfigUpdateLastUnsuccessfulAttemptTime
            (pu4Error, pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiData->pOctetStrValue));

}

INT4
FsCapwapRunStationConfigReqReceivedSet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhSetFsCapwapRunStationConfigReqReceived
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapRunStationConfigReqReceivedTest (UINT4 *pu4Error,
                                         tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapRunStationConfigReqReceived (pu4Error,
                                                          pMultiIndex->
                                                          pIndex[0].
                                                          u4_ULongValue,
                                                          pMultiData->
                                                          u4_ULongValue));

}

INT4
FsCapwapRunStationConfigRspReceivedSet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhSetFsCapwapRunStationConfigRspReceived
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapRunStationConfigRspReceivedTest (UINT4 *pu4Error,
                                         tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapRunStationConfigRspReceived (pu4Error,
                                                          pMultiIndex->
                                                          pIndex[0].
                                                          u4_ULongValue,
                                                          pMultiData->
                                                          u4_ULongValue));

}

INT4
FsCapwapRunStationConfigReqTransmittedSet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    return (nmhSetFsCapwapRunStationConfigReqTransmitted
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapRunStationConfigReqTransmittedTest (UINT4 *pu4Error,
                                            tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapRunStationConfigReqTransmitted (pu4Error,
                                                             pMultiIndex->
                                                             pIndex[0].
                                                             u4_ULongValue,
                                                             pMultiData->
                                                             u4_ULongValue));

}

INT4
FsCapwapRunStationConfigRspTransmittedSet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    return (nmhSetFsCapwapRunStationConfigRspTransmitted
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapRunStationConfigRspTransmittedTest (UINT4 *pu4Error,
                                            tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapRunStationConfigRspTransmitted (pu4Error,
                                                             pMultiIndex->
                                                             pIndex[0].
                                                             u4_ULongValue,
                                                             pMultiData->
                                                             u4_ULongValue));

}

INT4
FsCapwapRunStationConfigunsuccessfulProcessedSet (tSnmpIndex * pMultiIndex,
                                                  tRetVal * pMultiData)
{
    return (nmhSetFsCapwapRunStationConfigunsuccessfulProcessed
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapRunStationConfigunsuccessfulProcessedTest (UINT4 *pu4Error,
                                                   tSnmpIndex * pMultiIndex,
                                                   tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapRunStationConfigunsuccessfulProcessed (pu4Error,
                                                                    pMultiIndex->
                                                                    pIndex[0].
                                                                    u4_ULongValue,
                                                                    pMultiData->
                                                                    u4_ULongValue));

}

INT4
FsCapwapRunStationConfigReasonLastUnsuccAttemptSet (tSnmpIndex * pMultiIndex,
                                                    tRetVal * pMultiData)
{
    return (nmhSetFsCapwapRunStationConfigReasonLastUnsuccAttempt
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
FsCapwapRunStationConfigReasonLastUnsuccAttemptTest (UINT4 *pu4Error,
                                                     tSnmpIndex * pMultiIndex,
                                                     tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapRunStationConfigReasonLastUnsuccAttempt (pu4Error,
                                                                      pMultiIndex->
                                                                      pIndex[0].
                                                                      u4_ULongValue,
                                                                      pMultiData->
                                                                      pOctetStrValue));

}

INT4
FsCapwapRunStationConfigLastSuccAttemptTimeSet (tSnmpIndex * pMultiIndex,
                                                tRetVal * pMultiData)
{
    return (nmhSetFsCapwapRunStationConfigLastSuccAttemptTime
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
FsCapwapRunStationConfigLastSuccAttemptTimeTest (UINT4 *pu4Error,
                                                 tSnmpIndex * pMultiIndex,
                                                 tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapRunStationConfigLastSuccAttemptTime (pu4Error,
                                                                  pMultiIndex->
                                                                  pIndex[0].
                                                                  u4_ULongValue,
                                                                  pMultiData->
                                                                  pOctetStrValue));

}

INT4
FsCapwapRunStationConfigLastUnsuccessfulAttemptTimeSet (tSnmpIndex *
                                                        pMultiIndex,
                                                        tRetVal * pMultiData)
{
    return (nmhSetFsCapwapRunStationConfigLastUnsuccessfulAttemptTime
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
FsCapwapRunStationConfigLastUnsuccessfulAttemptTimeTest (UINT4 *pu4Error,
                                                         tSnmpIndex *
                                                         pMultiIndex,
                                                         tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapRunStationConfigLastUnsuccessfulAttemptTime
            (pu4Error, pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiData->pOctetStrValue));

}

INT4
FsCapwapRunClearConfigReqReceivedSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhSetFsCapwapRunClearConfigReqReceived
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapRunClearConfigReqReceivedTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapRunClearConfigReqReceived (pu4Error,
                                                        pMultiIndex->pIndex[0].
                                                        u4_ULongValue,
                                                        pMultiData->
                                                        u4_ULongValue));

}

INT4
FsCapwapRunClearConfigRspReceivedSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhSetFsCapwapRunClearConfigRspReceived
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapRunClearConfigRspReceivedTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapRunClearConfigRspReceived (pu4Error,
                                                        pMultiIndex->pIndex[0].
                                                        u4_ULongValue,
                                                        pMultiData->
                                                        u4_ULongValue));

}

INT4
FsCapwapRunClearConfigReqTransmittedSet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    return (nmhSetFsCapwapRunClearConfigReqTransmitted
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapRunClearConfigReqTransmittedTest (UINT4 *pu4Error,
                                          tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapRunClearConfigReqTransmitted (pu4Error,
                                                           pMultiIndex->
                                                           pIndex[0].
                                                           u4_ULongValue,
                                                           pMultiData->
                                                           u4_ULongValue));

}

INT4
FsCapwapRunClearConfigRspTransmittedSet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    return (nmhSetFsCapwapRunClearConfigRspTransmitted
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapRunClearConfigRspTransmittedTest (UINT4 *pu4Error,
                                          tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapRunClearConfigRspTransmitted (pu4Error,
                                                           pMultiIndex->
                                                           pIndex[0].
                                                           u4_ULongValue,
                                                           pMultiData->
                                                           u4_ULongValue));

}

INT4
FsCapwapRunClearConfigunsuccessfulProcessedSet (tSnmpIndex * pMultiIndex,
                                                tRetVal * pMultiData)
{
    return (nmhSetFsCapwapRunClearConfigunsuccessfulProcessed
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapRunClearConfigunsuccessfulProcessedTest (UINT4 *pu4Error,
                                                 tSnmpIndex * pMultiIndex,
                                                 tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapRunClearConfigunsuccessfulProcessed (pu4Error,
                                                                  pMultiIndex->
                                                                  pIndex[0].
                                                                  u4_ULongValue,
                                                                  pMultiData->
                                                                  u4_ULongValue));

}

INT4
FsCapwapRunClearConfigReasonLastUnsuccAttemptSet (tSnmpIndex * pMultiIndex,
                                                  tRetVal * pMultiData)
{
    return (nmhSetFsCapwapRunClearConfigReasonLastUnsuccAttempt
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
FsCapwapRunClearConfigReasonLastUnsuccAttemptTest (UINT4 *pu4Error,
                                                   tSnmpIndex * pMultiIndex,
                                                   tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapRunClearConfigReasonLastUnsuccAttempt (pu4Error,
                                                                    pMultiIndex->
                                                                    pIndex[0].
                                                                    u4_ULongValue,
                                                                    pMultiData->
                                                                    pOctetStrValue));

}

INT4
FsCapwapRunClearConfigLastSuccAttemptTimeSet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    return (nmhSetFsCapwapRunClearConfigLastSuccAttemptTime
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
FsCapwapRunClearConfigLastSuccAttemptTimeTest (UINT4 *pu4Error,
                                               tSnmpIndex * pMultiIndex,
                                               tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapRunClearConfigLastSuccAttemptTime (pu4Error,
                                                                pMultiIndex->
                                                                pIndex[0].
                                                                u4_ULongValue,
                                                                pMultiData->
                                                                pOctetStrValue));

}

INT4
FsCapwapRunClearConfigLastUnsuccessfulAttemptTimeSet (tSnmpIndex * pMultiIndex,
                                                      tRetVal * pMultiData)
{
    return (nmhSetFsCapwapRunClearConfigLastUnsuccessfulAttemptTime
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
FsCapwapRunClearConfigLastUnsuccessfulAttemptTimeTest (UINT4 *pu4Error,
                                                       tSnmpIndex * pMultiIndex,
                                                       tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapRunClearConfigLastUnsuccessfulAttemptTime
            (pu4Error, pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiData->pOctetStrValue));

}

INT4
FsCapwapRunDataTransferReqReceivedSet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhSetFsCapwapRunDataTransferReqReceived
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapRunDataTransferReqReceivedTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapRunDataTransferReqReceived (pu4Error,
                                                         pMultiIndex->pIndex[0].
                                                         u4_ULongValue,
                                                         pMultiData->
                                                         u4_ULongValue));

}

INT4
FsCapwapRunDataTransferRspReceivedSet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhSetFsCapwapRunDataTransferRspReceived
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapRunDataTransferRspReceivedTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapRunDataTransferRspReceived (pu4Error,
                                                         pMultiIndex->pIndex[0].
                                                         u4_ULongValue,
                                                         pMultiData->
                                                         u4_ULongValue));

}

INT4
FsCapwapRunDataTransferReqTransmittedSet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    return (nmhSetFsCapwapRunDataTransferReqTransmitted
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapRunDataTransferReqTransmittedTest (UINT4 *pu4Error,
                                           tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapRunDataTransferReqTransmitted (pu4Error,
                                                            pMultiIndex->
                                                            pIndex[0].
                                                            u4_ULongValue,
                                                            pMultiData->
                                                            u4_ULongValue));

}

INT4
FsCapwapRunDataTransferRspTransmittedSet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    return (nmhSetFsCapwapRunDataTransferRspTransmitted
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapRunDataTransferRspTransmittedTest (UINT4 *pu4Error,
                                           tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapRunDataTransferRspTransmitted (pu4Error,
                                                            pMultiIndex->
                                                            pIndex[0].
                                                            u4_ULongValue,
                                                            pMultiData->
                                                            u4_ULongValue));

}

INT4
FsCapwapRunDataTransferunsuccessfulProcessedSet (tSnmpIndex * pMultiIndex,
                                                 tRetVal * pMultiData)
{
    return (nmhSetFsCapwapRunDataTransferunsuccessfulProcessed
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapRunDataTransferunsuccessfulProcessedTest (UINT4 *pu4Error,
                                                  tSnmpIndex * pMultiIndex,
                                                  tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapRunDataTransferunsuccessfulProcessed (pu4Error,
                                                                   pMultiIndex->
                                                                   pIndex[0].
                                                                   u4_ULongValue,
                                                                   pMultiData->
                                                                   u4_ULongValue));

}

INT4
FsCapwapRunDataTransferReasonLastUnsuccAttemptSet (tSnmpIndex * pMultiIndex,
                                                   tRetVal * pMultiData)
{
    return (nmhSetFsCapwapRunDataTransferReasonLastUnsuccAttempt
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
FsCapwapRunDataTransferReasonLastUnsuccAttemptTest (UINT4 *pu4Error,
                                                    tSnmpIndex * pMultiIndex,
                                                    tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapRunDataTransferReasonLastUnsuccAttempt (pu4Error,
                                                                     pMultiIndex->
                                                                     pIndex[0].
                                                                     u4_ULongValue,
                                                                     pMultiData->
                                                                     pOctetStrValue));

}

INT4
FsCapwapRunDataTransferLastSuccAttemptTimeSet (tSnmpIndex * pMultiIndex,
                                               tRetVal * pMultiData)
{
    return (nmhSetFsCapwapRunDataTransferLastSuccAttemptTime
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
FsCapwapRunDataTransferLastSuccAttemptTimeTest (UINT4 *pu4Error,
                                                tSnmpIndex * pMultiIndex,
                                                tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapRunDataTransferLastSuccAttemptTime (pu4Error,
                                                                 pMultiIndex->
                                                                 pIndex[0].
                                                                 u4_ULongValue,
                                                                 pMultiData->
                                                                 pOctetStrValue));

}

INT4
FsCapwapRunDataTransferLastUnsuccessfulAttemptTimeSet (tSnmpIndex * pMultiIndex,
                                                       tRetVal * pMultiData)
{
    return (nmhSetFsCapwapRunDataTransferLastUnsuccessfulAttemptTime
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
FsCapwapRunDataTransferLastUnsuccessfulAttemptTimeTest (UINT4 *pu4Error,
                                                        tSnmpIndex *
                                                        pMultiIndex,
                                                        tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapRunDataTransferLastUnsuccessfulAttemptTime
            (pu4Error, pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiData->pOctetStrValue));

}

INT4
FsCapwapRunResetReqReceivedSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsCapwapRunResetReqReceived
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapRunResetReqReceivedTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapRunResetReqReceived (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  u4_ULongValue,
                                                  pMultiData->u4_ULongValue));

}

INT4
FsCapwapRunResetRspReceivedSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsCapwapRunResetRspReceived
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapRunResetRspReceivedTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapRunResetRspReceived (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  u4_ULongValue,
                                                  pMultiData->u4_ULongValue));

}

INT4
FsCapwapRunResetReqTransmittedSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetFsCapwapRunResetReqTransmitted
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapRunResetReqTransmittedTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapRunResetReqTransmitted (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     u4_ULongValue,
                                                     pMultiData->
                                                     u4_ULongValue));

}

INT4
FsCapwapRunResetRspTransmittedSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetFsCapwapRunResetRspTransmitted
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapRunResetRspTransmittedTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapRunResetRspTransmitted (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     u4_ULongValue,
                                                     pMultiData->
                                                     u4_ULongValue));

}

INT4
FsCapwapRunResetunsuccessfulProcessedSet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    return (nmhSetFsCapwapRunResetunsuccessfulProcessed
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapRunResetunsuccessfulProcessedTest (UINT4 *pu4Error,
                                           tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapRunResetunsuccessfulProcessed (pu4Error,
                                                            pMultiIndex->
                                                            pIndex[0].
                                                            u4_ULongValue,
                                                            pMultiData->
                                                            u4_ULongValue));

}

INT4
FsCapwapRunResetReasonLastUnsuccAttemptSet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    return (nmhSetFsCapwapRunResetReasonLastUnsuccAttempt
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
FsCapwapRunResetReasonLastUnsuccAttemptTest (UINT4 *pu4Error,
                                             tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapRunResetReasonLastUnsuccAttempt (pu4Error,
                                                              pMultiIndex->
                                                              pIndex[0].
                                                              u4_ULongValue,
                                                              pMultiData->
                                                              pOctetStrValue));

}

INT4
FsCapwapRunResetLastSuccAttemptTimeSet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhSetFsCapwapRunResetLastSuccAttemptTime
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
FsCapwapRunResetLastSuccAttemptTimeTest (UINT4 *pu4Error,
                                         tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapRunResetLastSuccAttemptTime (pu4Error,
                                                          pMultiIndex->
                                                          pIndex[0].
                                                          u4_ULongValue,
                                                          pMultiData->
                                                          pOctetStrValue));

}

INT4
FsCapwapRunResetLastUnsuccessfulAttemptTimeSet (tSnmpIndex * pMultiIndex,
                                                tRetVal * pMultiData)
{
    return (nmhSetFsCapwapRunResetLastUnsuccessfulAttemptTime
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
FsCapwapRunResetLastUnsuccessfulAttemptTimeTest (UINT4 *pu4Error,
                                                 tSnmpIndex * pMultiIndex,
                                                 tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapRunResetLastUnsuccessfulAttemptTime (pu4Error,
                                                                  pMultiIndex->
                                                                  pIndex[0].
                                                                  u4_ULongValue,
                                                                  pMultiData->
                                                                  pOctetStrValue));

}

INT4
FsCapwapRunPriDiscReqReceivedSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetFsCapwapRunPriDiscReqReceived
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapRunPriDiscReqReceivedTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapRunPriDiscReqReceived (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    u4_ULongValue,
                                                    pMultiData->u4_ULongValue));

}

INT4
FsCapwapRunPriDiscRspReceivedSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetFsCapwapRunPriDiscRspReceived
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapRunPriDiscRspReceivedTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapRunPriDiscRspReceived (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    u4_ULongValue,
                                                    pMultiData->u4_ULongValue));

}

INT4
FsCapwapRunPriDiscReqTransmittedSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhSetFsCapwapRunPriDiscReqTransmitted
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapRunPriDiscReqTransmittedTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapRunPriDiscReqTransmitted (pu4Error,
                                                       pMultiIndex->pIndex[0].
                                                       u4_ULongValue,
                                                       pMultiData->
                                                       u4_ULongValue));

}

INT4
FsCapwapRunPriDiscRspTransmittedSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhSetFsCapwapRunPriDiscRspTransmitted
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapRunPriDiscRspTransmittedTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapRunPriDiscRspTransmitted (pu4Error,
                                                       pMultiIndex->pIndex[0].
                                                       u4_ULongValue,
                                                       pMultiData->
                                                       u4_ULongValue));

}

INT4
FsCapwapRunPriDiscunsuccessfulProcessedSet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    return (nmhSetFsCapwapRunPriDiscunsuccessfulProcessed
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapRunPriDiscunsuccessfulProcessedTest (UINT4 *pu4Error,
                                             tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapRunPriDiscunsuccessfulProcessed (pu4Error,
                                                              pMultiIndex->
                                                              pIndex[0].
                                                              u4_ULongValue,
                                                              pMultiData->
                                                              u4_ULongValue));

}

INT4
FsCapwapRunPriDiscReasonLastUnsuccAttemptSet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    return (nmhSetFsCapwapRunPriDiscReasonLastUnsuccAttempt
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
FsCapwapRunPriDiscReasonLastUnsuccAttemptTest (UINT4 *pu4Error,
                                               tSnmpIndex * pMultiIndex,
                                               tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapRunPriDiscReasonLastUnsuccAttempt (pu4Error,
                                                                pMultiIndex->
                                                                pIndex[0].
                                                                u4_ULongValue,
                                                                pMultiData->
                                                                pOctetStrValue));

}

INT4
FsCapwapRunPriDiscLastSuccAttemptTimeSet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    return (nmhSetFsCapwapRunPriDiscLastSuccAttemptTime
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
FsCapwapRunPriDiscLastSuccAttemptTimeTest (UINT4 *pu4Error,
                                           tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapRunPriDiscLastSuccAttemptTime (pu4Error,
                                                            pMultiIndex->
                                                            pIndex[0].
                                                            u4_ULongValue,
                                                            pMultiData->
                                                            pOctetStrValue));

}

INT4
FsCapwapRunPriDiscLastUnsuccessfulAttemptTimeSet (tSnmpIndex * pMultiIndex,
                                                  tRetVal * pMultiData)
{
    return (nmhSetFsCapwapRunPriDiscLastUnsuccessfulAttemptTime
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
FsCapwapRunPriDiscLastUnsuccessfulAttemptTimeTest (UINT4 *pu4Error,
                                                   tSnmpIndex * pMultiIndex,
                                                   tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapRunPriDiscLastUnsuccessfulAttemptTime (pu4Error,
                                                                    pMultiIndex->
                                                                    pIndex[0].
                                                                    u4_ULongValue,
                                                                    pMultiData->
                                                                    pOctetStrValue));

}

INT4
FsCapwapRunEchoReqReceivedSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsCapwapRunEchoReqReceived
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapRunEchoReqReceivedTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapRunEchoReqReceived (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 u4_ULongValue,
                                                 pMultiData->u4_ULongValue));

}

INT4
FsCapwapRunEchoRspReceivedSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsCapwapRunEchoRspReceived
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapRunEchoRspReceivedTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapRunEchoRspReceived (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 u4_ULongValue,
                                                 pMultiData->u4_ULongValue));

}

INT4
FsCapwapRunEchoReqTransmittedSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetFsCapwapRunEchoReqTransmitted
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapRunEchoReqTransmittedTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapRunEchoReqTransmitted (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    u4_ULongValue,
                                                    pMultiData->u4_ULongValue));

}

INT4
FsCapwapRunEchoRspTransmittedSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetFsCapwapRunEchoRspTransmitted
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapRunEchoRspTransmittedTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapRunEchoRspTransmitted (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    u4_ULongValue,
                                                    pMultiData->u4_ULongValue));

}

INT4
FsCapwapRunEchounsuccessfulProcessedSet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    return (nmhSetFsCapwapRunEchounsuccessfulProcessed
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsCapwapRunEchounsuccessfulProcessedTest (UINT4 *pu4Error,
                                          tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapRunEchounsuccessfulProcessed (pu4Error,
                                                           pMultiIndex->
                                                           pIndex[0].
                                                           u4_ULongValue,
                                                           pMultiData->
                                                           u4_ULongValue));

}

INT4
FsCapwapRunEchoReasonLastUnsuccAttemptSet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    return (nmhSetFsCapwapRunEchoReasonLastUnsuccAttempt
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
FsCapwapRunEchoReasonLastUnsuccAttemptTest (UINT4 *pu4Error,
                                            tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapRunEchoReasonLastUnsuccAttempt (pu4Error,
                                                             pMultiIndex->
                                                             pIndex[0].
                                                             u4_ULongValue,
                                                             pMultiData->
                                                             pOctetStrValue));

}

INT4
FsCapwapRunEchoLastSuccAttemptTimeSet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhSetFsCapwapRunEchoLastSuccAttemptTime
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
FsCapwapRunEchoLastSuccAttemptTimeTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapRunEchoLastSuccAttemptTime (pu4Error,
                                                         pMultiIndex->pIndex[0].
                                                         u4_ULongValue,
                                                         pMultiData->
                                                         pOctetStrValue));

}

INT4
FsCapwapRunEchoLastUnsuccessfulAttemptTimeSet (tSnmpIndex * pMultiIndex,
                                               tRetVal * pMultiData)
{
    return (nmhSetFsCapwapRunEchoLastUnsuccessfulAttemptTime
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
FsCapwapRunEchoLastUnsuccessfulAttemptTimeTest (UINT4 *pu4Error,
                                                tSnmpIndex * pMultiIndex,
                                                tRetVal * pMultiData)
{
    return (nmhTestv2FsCapwapRunEchoLastUnsuccessfulAttemptTime (pu4Error,
                                                                 pMultiIndex->
                                                                 pIndex[0].
                                                                 u4_ULongValue,
                                                                 pMultiData->
                                                                 pOctetStrValue));

}
