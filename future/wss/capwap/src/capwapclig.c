#ifndef __CAPWAPCLIG_C__
#define __CAPWAPCLIG_C__
/********************************************************************
* Copyright (C) 2013  Aricent Inc . All Rights Reserved
*
* $Id: capwapclig.c,v 1.11.2.1 2018/03/08 14:01:22 siva Exp $       
*
* Description: This file contains the Capwap CLI related routines 
*********************************************************************/

#include "capwapinc.h"
#include "capwapcli.h"
#include "ifmmacs.h"
#include "wlchdlr.h"
#include "isscli.h"
#include "iss.h"
#include "capwapclilwg.h"
#include "capwapcliprotg.h"
#include "wssifpmdb.h"
#include "wsscfgtdfsg.h"
#include "wsscfgtdfs.h"
#include "wsscfgprotg.h"
#include "wsscfglwg.h"
#include "std802lw.h"
#include "wsscfgcli.h"
#include "wsscfgextn.h"
#include "capwapclimacr.h"
#include "wsscfgdefg.h"
#ifdef KERNEL_CAPWAP_WANTED
#ifdef WLC_WANTED
#include "fcusprot.h"
#endif
#endif

extern tWssClientSummary gaWssClientSummary[MAX_STA_SUPP_PER_WLC];
extern UINT4        gu4ClientWalkIndex;
extern VOID
        WlcHdlrStaConfClearTmrControl (UINT4 u4Status, UINT4 u4TimerValue);
extern INT4         WssStaShowClient (VOID);
#ifdef WLC_WANTED
extern UINT4        gu4HttpMask;
#endif /* WLC_WANTED */
extern UINT4        gu4WssStaWebAuthDebugMask;

/****************************************************************************
 * Function    :  cli_process_Capwap_cmd
 * Description :  This function is exported to CLI module to handle the
                CAPWAP cli commands to take the corresponding action.

 * Input       :  Variable arguments

 * Output      :  None 

 * Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
cli_process_Capwap_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *args[CAPWAP_CLI_MAX_ARGS];
    INT1                argno = 0;
    UINT4               u4ErrCode = 0, u4IpAddr = 0;
    INT4                i4RetStatus = 1;
    UINT4               u4CmdType = 0;
    INT4                i4Inst;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4PrevWtpProfileId = 0;
    UINT4               u4NoOfRadio = 0;
    UINT1               u1RadioIndex = 0;
    UINT4               u4Trigger = 0;
    tMacAddr            au1InMacAddr;
    tCapwapFsWtpModelEntry CapwapSetFsWtpModelEntry;
    tCapwapIsSetFsWtpModelEntry CapwapIsSetFsWtpModelEntry;
    tCapwapFsWtpModelEntry CapwapGetFsWtpModelEntry;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;

    tCapwapFsWtpRadioEntry CapwapSetFsWtpRadioEntry;
    tCapwapIsSetFsWtpRadioEntry CapwapIsSetFsWtpRadioEntry;

    tCapwapFsCapwapWhiteListEntry CapwapSetFsCapwapWhiteListEntry;
    tCapwapIsSetFsCapwapWhiteListEntry CapwapIsSetFsCapwapWhiteListEntry;

    tCapwapFsCapwapBlackListEntry CapwapSetFsCapwapBlackListEntry;
    tCapwapIsSetFsCapwapBlackListEntry CapwapIsSetFsCapwapBlackListEntry;

    tCapwapFsCapwapWtpConfigEntry CapwapSetFsCapwapWtpConfigEntry;
    tCapwapIsSetFsCapwapWtpConfigEntry CapwapIsSetFsCapwapWtpConfigEntry;

    tCapwapFsCapwapLinkEncryptionEntry CapwapSetFsCapwapLinkEncryptionEntry;
    tCapwapIsSetFsCapwapLinkEncryptionEntry
        CapwapIsSetFsCapwapLinkEncryptionEntry;

    tCapwapFsCapwapDefaultWtpProfileEntry
        CapwapSetFsCapwapDefaultWtpProfileEntry;
    tCapwapIsSetFsCapwapDefaultWtpProfileEntry
        CapwapIsSetFsCapwapDefaultWtpProfileEntry;

    tCapwapFsCapwapDnsProfileEntry CapwapSetFsCapwapDnsProfileEntry;
    tCapwapIsSetFsCapwapDnsProfileEntry CapwapIsSetFsCapwapDnsProfileEntry;

    tCapwapFsWtpNativeVlanIdTable CapwapSetFsWtpNativeVlanIdTable;
    tCapwapIsSetFsWtpNativeVlanIdTable CapwapIsSetFsWtpNativeVlanIdTable;

    tCapwapFsWtpLocalRoutingTable CapwapSetFsWtpLocalRoutingTable;
    tCapwapIsSetFsWtpLocalRoutingTable CapwapIsSetFsWtpLocalRoutingTable;

    tCapwapFsCawapDiscStatsEntry CapwapSetFsCawapDiscStatsEntry;
    tCapwapIsSetFsCawapDiscStatsEntry CapwapIsSetFsCawapDiscStatsEntry;

    tCapwapFsCawapJoinStatsEntry CapwapSetFsCawapJoinStatsEntry;
    tCapwapIsSetFsCawapJoinStatsEntry CapwapIsSetFsCawapJoinStatsEntry;

    tCapwapFsCawapConfigStatsEntry CapwapSetFsCawapConfigStatsEntry;
    tCapwapIsSetFsCawapConfigStatsEntry CapwapIsSetFsCawapConfigStatsEntry;

    tCapwapFsCawapRunStatsEntry *pCapwapSetFsCawapRunStatsEntry = NULL;
    tCapwapIsSetFsCawapRunStatsEntry CapwapIsSetFsCawapRunStatsEntry;

    tCapwapFsCapwapWirelessBindingEntry CapwapSetFsCapwapWirelessBindingEntry;
    tCapwapIsSetFsCapwapWirelessBindingEntry
        CapwapIsSetFsCapwapWirelessBindingEntry;

    tCapwapFsCapwapWtpRebootStatisticsEntry
        CapwapSetFsCapwapWtpRebootStatisticsEntry;
    tCapwapIsSetFsCapwapWtpRebootStatisticsEntry
        CapwapIsSetFsCapwapWtpRebootStatisticsEntry;

    tCapwapFsCapwapWtpRadioStatisticsEntry
        CapwapSetFsCapwapWtpRadioStatisticsEntry;
    tCapwapIsSetFsCapwapWtpRadioStatisticsEntry
        CapwapIsSetFsCapwapWtpRadioStatisticsEntry;

    tCapwapCapwapBaseAcNameListEntry CapwapSetCapwapBaseAcNameListEntry;
    tCapwapIsSetCapwapBaseAcNameListEntry CapwapIsSetCapwapBaseAcNameListEntry;
    UINT4               u4NextProfileId = 0;

    tCapwapCapwapBaseMacAclEntry CapwapSetCapwapBaseMacAclEntry;
    tCapwapIsSetCapwapBaseMacAclEntry CapwapIsSetCapwapBaseMacAclEntry;

    tCapwapCapwapBaseWtpProfileEntry CapwapSetCapwapBaseWtpProfileEntry;
    tCapwapIsSetCapwapBaseWtpProfileEntry CapwapIsSetCapwapBaseWtpProfileEntry;
    UINT1               au1PromptName[32];
    CHR1               *pu1PromptName = NULL;
    CHR1               *saveptr = NULL;
    UINT1              *pu1Name = NULL;
    UINT4              *dummy_var = NULL;
    UINT4               u4Len = 0;
    UINT4               u4RowStatus = NOT_IN_SERVICE;
    UINT4               u1RadioId = 0;
    UINT1               au1ModelNumber[256];

    tSNMP_OCTET_STRING_TYPE MacAddress;
    tSNMP_OCTET_STRING_TYPE ModelName;

    UINT1               au1MacArray[18];
    UINT4               u4BlackListId;
    UINT4               u4WhiteListId;
    UINT4              *pu4RowStatus = NULL;
    INT4                i4RowStatus;
    UINT4               length = 0;
    UINT1               au1FsWtpCrashFileName[256];
    UINT4               u4FsWtpCrashFileNameLen = 0;
    INT4                i4WtpModelMacType = 0;
    INT4                i4WtpProfileRowVal = 0;
    UINT4              *pu4WtpProfileId = NULL;
    UINT4              *pu4WtpRowStatus = NULL;
    UINT4              *pu4WhiteListId = NULL;
    UINT4              *pu4BlackListId = NULL;
    UINT4              *pu4IpAddr = NULL;
#ifdef WLC_WANTED
    INT1                i1RetStatus = 0;
#endif
    pu4WtpProfileId = &u4WtpProfileId;
    pu4WtpRowStatus = &u4RowStatus;
    pu4WhiteListId = &u4WhiteListId;
    pu4BlackListId = &u4BlackListId;
    pu4IpAddr = &u4IpAddr;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&au1ModelNumber, 0, 256);
    MEMSET (au1MacArray, 0, 18);
    MEMSET (&ModelName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    MEMSET (au1PromptName, 0, sizeof (32));
    MEMSET (au1FsWtpCrashFileName, 0, sizeof (au1FsWtpCrashFileName));

    ModelName.pu1_OctetList = au1ModelNumber;
    MacAddress.pu1_OctetList = au1MacArray;
    pu1PromptName = (CHR1 *) au1PromptName;
    UNUSED_PARAM (u4CmdType);

    /* check if the capwap module is started. i.e "no shutdown". else all the
     * commands are considered invalid except shutdown related commands */
    if (CapwapCliCheckModuleStatus (CliHandle, u4Command) != CLI_SUCCESS)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return CLI_FAILURE;
    }
#ifdef CLI_WANTED
    CliRegisterLock (CliHandle, CapwapMainTaskLock, CapwapMainTaskUnLock);
#endif
    CAPWAP_LOCK;

    va_start (ap, u4Command);

    i4Inst = va_arg (ap, INT4);

    if (i4Inst != 0)
    {
        /* Coverity fix - It is analayzed that all the 
         * Capwap CLI commands function use the first argument as NULL.
         * So the extraction of the first index is not required in this case  */
    }

    while (1)
    {
        args[argno++] = va_arg (ap, UINT4 *);
        if (argno == CAPWAP_CLI_MAX_ARGS)
            break;
    }

    va_end (ap);
    switch (u4Command)
    {
        case CLI_CAPWAP_CAPWAPBASEACNAMELISTTABLE:
            MEMSET (&CapwapSetCapwapBaseAcNameListEntry, 0,
                    sizeof (tCapwapCapwapBaseAcNameListEntry));
            MEMSET (&CapwapIsSetCapwapBaseAcNameListEntry, 0,
                    sizeof (tCapwapIsSetCapwapBaseAcNameListEntry));

            i4WtpProfileRowVal = 0;

            if (args[0] != NULL)
            {
                if (nmhGetCapwapBaseAcNameListRowStatus
                    (*args[0], &i4WtpProfileRowVal) == SNMP_SUCCESS)
                {
                    if (*args[4] == CREATE_AND_GO)
                    {
                        *args[4] = ACTIVE;
                    }
                    CAPWAP_FILL_CAPWAPBASEACNAMELISTTABLE_ARGS ((&CapwapSetCapwapBaseAcNameListEntry), (&CapwapIsSetCapwapBaseAcNameListEntry), args[0], args[1], args[2], args[3], args[4]);
                    i4RetStatus =
                        CapwapCliSetCapwapBaseAcNameListTable (CliHandle,
                                                               (&CapwapSetCapwapBaseAcNameListEntry),
                                                               (&CapwapIsSetCapwapBaseAcNameListEntry));
                }
                else
                {
                    CAPWAP_FILL_CAPWAPBASEACNAMELISTTABLE_ARGS ((&CapwapSetCapwapBaseAcNameListEntry), (&CapwapIsSetCapwapBaseAcNameListEntry), args[0], args[1], args[2], args[3], args[4]);
                    i4RetStatus =
                        CapwapCliSetCapwapBaseAcNameListTable (CliHandle,
                                                               (&CapwapSetCapwapBaseAcNameListEntry),
                                                               (&CapwapIsSetCapwapBaseAcNameListEntry));
                }
            }

            break;

        case CLI_CAPWAP_CAPWAPBASEMACACLTABLE:
            MEMSET (&CapwapSetCapwapBaseMacAclEntry, 0,
                    sizeof (tCapwapCapwapBaseMacAclEntry));
            MEMSET (&CapwapIsSetCapwapBaseMacAclEntry, 0,
                    sizeof (tCapwapIsSetCapwapBaseMacAclEntry));

            CAPWAP_FILL_CAPWAPBASEMACACLTABLE_ARGS ((&CapwapSetCapwapBaseMacAclEntry), (&CapwapIsSetCapwapBaseMacAclEntry), args[0], args[1], args[2], args[3]);

            i4RetStatus =
                CapwapCliSetCapwapBaseMacAclTable (CliHandle,
                                                   (&CapwapSetCapwapBaseMacAclEntry),
                                                   (&CapwapIsSetCapwapBaseMacAclEntry));
            break;

        case CLI_CAPWAP_CAPWAPBASEWTPPROFILETABLE:
            MEMSET (&CapwapSetCapwapBaseWtpProfileEntry, 0,
                    sizeof (tCapwapCapwapBaseWtpProfileEntry));
            MEMSET (&CapwapIsSetCapwapBaseWtpProfileEntry, 0,
                    sizeof (tCapwapIsSetCapwapBaseWtpProfileEntry));

            if ((args[0] == NULL) && (args[1] == NULL))
            {
                CliPrintf (CliHandle, "\r\n Index missing \r\n");
                CLI_FATAL_ERROR (CliHandle);
                i4RetStatus = OSIX_FAILURE;
                break;
            }
            if ((args[3]) != NULL && (args[4] != NULL))
            {
                CliDotStrToMac ((UINT1 *) args[3], au1InMacAddr);
                if (nmhTestv2MacAddress (&u4ErrCode, au1InMacAddr)
                    == CLI_FAILURE)
                {
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
            }
            if ((args[0] == NULL) && (args[1] != NULL))
            {
                /* When Profile name is received as input get the profile id
                 * from mapping table and pass the profile name as NULL */
                if (CapwapGetWtpProfileIdFromProfileName ((UINT1 *) args[1],
                                                          &u4WtpProfileId) !=
                    OSIX_SUCCESS)
                {

                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }

                if (args[26] != NULL)
                {
                    if (CLI_CAPWAP_RADIO_CREATE == *(args[26]))
                    {
                        CAPWAP_FILL_CAPWAPBASEWTPPROFILETABLE_ARGS
                            ((&CapwapSetCapwapBaseWtpProfileEntry),
                             (&CapwapIsSetCapwapBaseWtpProfileEntry),
                             pu4WtpProfileId, dummy_var, dummy_var,
                             args[3], args[4], args[5], args[6], args[7],
                             args[8], args[9], args[10], args[11], args[12],
                             args[13], args[14], args[15], args[16], args[17],
                             args[18], args[19], args[20], args[21], args[22],
                             args[23], args[24], args[25], dummy_var);
                    }
                    else if (CLI_CAPWAP_RADIO_DELETE == *(args[26]))
                    {
                        CAPWAP_FILL_CAPWAPBASEWTPPROFILETABLE_ARGS
                            ((&CapwapSetCapwapBaseWtpProfileEntry),
                             (&CapwapIsSetCapwapBaseWtpProfileEntry),
                             pu4WtpProfileId, args[1], args[2],
                             args[3], args[4], args[5], args[6], args[7],
                             args[8], args[9], args[10], args[11], args[12],
                             args[13], args[14], args[15], args[16], args[17],
                             args[18], args[19], args[20], args[21], args[22],
                             args[23], args[24], args[25], args[26]);
                    }
                }
                else if (args[27] != NULL)
                {
                    pWssIfCapwapDB->CapwapGetDB.u2ProfileId =
                        (UINT2) u4WtpProfileId;
                    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpProfileId = OSIX_TRUE;

                    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_INTERNAL_ID,
                                                 pWssIfCapwapDB) !=
                        OSIX_SUCCESS)
                    {
                        i4RetStatus = 1;
                    }

                    CAPWAP_FILL_CAPWAPBASEWTPPROFILETABLE_ARGS
                        ((&CapwapSetCapwapBaseWtpProfileEntry),
                         (&CapwapIsSetCapwapBaseWtpProfileEntry),
                         pu4WtpProfileId, args[1], args[2],
                         args[3], args[4], args[5], args[6], args[7],
                         args[8], args[9], args[10], args[11], args[12],
                         args[13], args[14], args[15], args[16], args[17],
                         args[18], args[19], args[20], args[21], args[22],
                         args[23], args[24], args[25], args[26]);

                    u1RadioId = CLI_PTR_TO_U4 (*args[27]);
                    if (u1RadioId == 0)
                    {
                        if (nmhGetCapwapBaseWtpProfileWtpModelNumber
                            (u4WtpProfileId, &ModelName) != SNMP_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nModel No not successfully obtained ,"
                                       " (Invalid Input, Check Profile name)\r\n");
                            break;
                        }

                        if (nmhGetFsNoOfRadio (&ModelName, &u4NoOfRadio) !=
                            OSIX_FAILURE)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nNo.of Radios present not successfully"
                                       " obtained, Operation Failed\r\n");
                            break;

                        }
                        for (u1RadioIndex = 1; u1RadioIndex <= u4NoOfRadio;
                             u1RadioIndex++)
                        {
                            pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB =
                                OSIX_TRUE;
                            pWssIfCapwapDB->CapwapGetDB.u1RadioId =
                                u1RadioIndex;

                            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                                         pWssIfCapwapDB) !=
                                OSIX_SUCCESS)
                            {
                                continue;
                            }

                            CapwapSetCapwapBaseWtpProfileEntry.u4RadioIndex =
                                u1RadioIndex;
                            i4RetStatus =
                                CapwapCliSetCapwapBaseWtpProfileTable
                                (CliHandle,
                                 (&CapwapSetCapwapBaseWtpProfileEntry),
                                 (&CapwapIsSetCapwapBaseWtpProfileEntry));
                        }
                        break;
                    }
                    else
                    {
                        CapwapSetCapwapBaseWtpProfileEntry.u4RadioIndex =
                            u1RadioId;
                        i4RetStatus =
                            CapwapCliSetCapwapBaseWtpProfileTable (CliHandle,
                                                                   (&CapwapSetCapwapBaseWtpProfileEntry),
                                                                   (&CapwapIsSetCapwapBaseWtpProfileEntry));
                        break;
                    }
                }
                else
                {
                    CAPWAP_FILL_CAPWAPBASEWTPPROFILETABLE_ARGS
                        ((&CapwapSetCapwapBaseWtpProfileEntry),
                         (&CapwapIsSetCapwapBaseWtpProfileEntry),
                         pu4WtpProfileId, dummy_var, dummy_var,
                         args[3], args[4], args[5], args[6], args[7], args[8],
                         args[9], args[10], args[11], args[12], args[13],
                         args[14], args[15], args[16], args[17], args[18],
                         args[19], args[20], args[21], args[22], args[23],
                         args[24], args[25], args[26]);
                }
            }
            else
            {
                if ((args[0] != NULL) && (args[1] != NULL))
                {
                    if (args[26] != NULL)
                    {
                        if (CLI_CAPWAP_RADIO_CREATE == *(args[26]))
                        {
                            if ((args[5] == NULL))
                            {
                                CliPrintf (CliHandle,
                                           "\r\n Model number mandatory for "
                                           "profile creation \r\n");
                                CLI_FATAL_ERROR (CliHandle);
                                i4RetStatus = OSIX_FAILURE;
                                break;
                            }
                            CapwapSetCapwapBaseWtpProfileEntry.u4SystemUpTime =
                                OsixGetSysUpTime ();
                        }
                        else
                        {
                            if ((args[5] != NULL))
                            {
                                CliPrintf (CliHandle,
                                           "\r\n Ignore Model number and "
                                           "proceeding..\r\n");

                                args[5] = NULL;
                                args[6] = NULL;
                            }
                        }
                    }
                }
                CAPWAP_FILL_CAPWAPBASEWTPPROFILETABLE_ARGS
                    ((&CapwapSetCapwapBaseWtpProfileEntry),
                     (&CapwapIsSetCapwapBaseWtpProfileEntry), args[0], args[1],
                     args[2], args[3], args[4], args[5], args[6], args[7],
                     args[8], args[9], args[10], args[11], args[12], args[13],
                     args[14], args[15], args[16], args[17], args[18], args[19],
                     args[20], args[21], args[22], args[23], args[24], args[25],
                     args[26]);
            }

            i4RetStatus =
                CapwapCliSetCapwapBaseWtpProfileTable (CliHandle,
                                                       (&CapwapSetCapwapBaseWtpProfileEntry),
                                                       (&CapwapIsSetCapwapBaseWtpProfileEntry));
            break;
#ifdef WLC_WANTED
        case CLI_CAPWAP_APGROUP:
        {
            if ((args[0] != NULL))
            {
                i1RetStatus =
                    nmhSetFsApGroupEnabledStatus (CLI_PTR_TO_I4 (*args[0]));
                if (i1RetStatus == SNMP_FAILURE)
                {
                    if (CLI_PTR_TO_I4 (*args[0]))
                    {
                        CliPrintf (CliHandle,
                                   "\r\n Failed to Enable the AP group Feature \r\n ");
                    }
                    else
                    {
                        CliPrintf (CliHandle,
                                   "\r\n Failed to Disable the AP group Feature \r\n ");

                    }
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
            }
        }
            break;
#endif
        case CLI_CAPWAP_CAPWAPBASEWTPSESSIONSLIMIT:

            i4RetStatus =
                CapwapCliSetCapwapBaseWtpSessionsLimit (CliHandle, args[0]);
            break;

        case CLI_CAPWAP_CAPWAPBASESTATIONSESSIONSLIMIT:

            i4RetStatus =
                CapwapCliSetCapwapBaseStationSessionsLimit (CliHandle, args[0]);
            break;

        case CLI_CAPWAP_CAPWAPBASEACMAXRETRANSMIT:

            i4RetStatus =
                CapwapCliSetCapwapBaseAcMaxRetransmit (CliHandle, args[0]);
            break;

        case CLI_CAPWAP_CAPWAPBASEACCHANGESTATEPENDINGTIMER:

            i4RetStatus =
                CapwapCliSetCapwapBaseAcChangeStatePendingTimer (CliHandle,
                                                                 args[0]);
            break;

        case CLI_CAPWAP_CAPWAPBASEACDATACHECKTIMER:

            i4RetStatus =
                CapwapCliSetCapwapBaseAcDataCheckTimer (CliHandle, args[0]);
            break;

        case CLI_CAPWAP_CAPWAPBASEACDTLSSESSIONDELETETIMER:

            i4RetStatus =
                CapwapCliSetCapwapBaseAcDTLSSessionDeleteTimer (CliHandle,
                                                                args[0]);
            break;

        case CLI_CAPWAP_CAPWAPBASEACECHOINTERVAL:

            i4RetStatus =
                CapwapCliSetCapwapBaseAcEchoInterval (CliHandle, args[0]);
            break;

        case CLI_CAPWAP_CAPWAPBASEACRETRANSMITINTERVAL:

            i4RetStatus =
                CapwapCliSetCapwapBaseAcRetransmitInterval (CliHandle, args[0]);
            break;

        case CLI_CAPWAP_CAPWAPBASEACSILENTINTERVAL:

            i4RetStatus =
                CapwapCliSetCapwapBaseAcSilentInterval (CliHandle, args[0]);
            break;

        case CLI_CAPWAP_CAPWAPBASEACWAITDTLSTIMER:

            i4RetStatus =
                CapwapCliSetCapwapBaseAcWaitDTLSTimer (CliHandle, args[0]);
            break;

        case CLI_CAPWAP_CAPWAPBASEACWAITJOINTIMER:
            i4RetStatus =
                CapwapCliSetCapwapBaseAcWaitJoinTimer (CliHandle, args[0]);
            break;

        case CLI_CAPWAP_CAPWAPBASEACECNSUPPORT:
            i4RetStatus =
                CapwapCliSetCapwapBaseAcEcnSupport (CliHandle, args[0]);
            break;

        case CLI_CAPWAP_CAPWAPBASECHANNELUPDOWNNOTIFYENABLE:

            i4RetStatus =
                CapwapCliSetCapwapBaseChannelUpDownNotifyEnable (CliHandle,
                                                                 args[0]);
            break;

        case CLI_CAPWAP_CAPWAPBASEDECRYPTERRORNOTIFYENABLE:

            i4RetStatus =
                CapwapCliSetCapwapBaseDecryptErrorNotifyEnable (CliHandle,
                                                                args[0]);
            break;

        case CLI_CAPWAP_CAPWAPBASEJOINFAILURENOTIFYENABLE:

            i4RetStatus =
                CapwapCliSetCapwapBaseJoinFailureNotifyEnable (CliHandle,
                                                               args[0]);
            break;

        case CLI_CAPWAP_CAPWAPBASEIMAGEUPGRADEFAILURENOTIFYENABLE:

            i4RetStatus =
                CapwapCliSetCapwapBaseImageUpgradeFailureNotifyEnable
                (CliHandle, args[0]);
            break;

        case CLI_CAPWAP_CAPWAPBASECONFIGMSGERRORNOTIFYENABLE:

            i4RetStatus =
                CapwapCliSetCapwapBaseConfigMsgErrorNotifyEnable (CliHandle,
                                                                  args[0]);
            break;

        case CLI_CAPWAP_CAPWAPBASERADIOOPERABLESTATUSNOTIFYENABLE:

            i4RetStatus =
                CapwapCliSetCapwapBaseRadioOperableStatusNotifyEnable
                (CliHandle, args[0]);
            break;

        case CLI_CAPWAP_CAPWAPBASEAUTHENFAILURENOTIFYENABLE:

            i4RetStatus =
                CapwapCliSetCapwapBaseAuthenFailureNotifyEnable (CliHandle,
                                                                 args[0]);
            break;
            /* newly added ******************************************** */
        case CLI_CAPWAP_FSDTLSENCAUTHENCRYPTALGORITHM:
            i4RetStatus =
                CapwapCliSetFsDtlsEncAuthEncryptAlgorithm (CliHandle, args[0]);
            break;

        case CLI_CAPWAP_FSWTPMODELTABLE:
            MEMSET (&CapwapGetFsWtpModelEntry, 0,
                    sizeof (tCapwapFsWtpModelEntry));
            MEMSET (&CapwapSetFsWtpModelEntry, 0,
                    sizeof (tCapwapFsWtpModelEntry));
            MEMSET (&CapwapIsSetFsWtpModelEntry, 0,
                    sizeof (tCapwapIsSetFsWtpModelEntry));
#ifdef CLI_WANTED
            CLI_GET_MODELNAME ((INT1 *) au1PromptName);
#endif
            pu1Name = (UINT1 *) STRTOK_R (pu1PromptName, "/", &saveptr);
            if (pu1Name == NULL)
            {
                break;
            }
            u4Len = STRLEN (pu1Name);

            CAPWAP_FILL_FSWTPMODELTABLE_ARGS ((&CapwapSetFsWtpModelEntry),
                                              (&CapwapIsSetFsWtpModelEntry),
                                              pu1Name, &u4Len, args[2],
                                              args[3], args[4], args[5],
                                              args[6], args[7], args[8],
                                              args[9], args[10], args[11],
                                              args[12], pu4WtpRowStatus);

            i4RetStatus =
                CapwapCliSetFsWtpModelTable (CliHandle,
                                             (&CapwapSetFsWtpModelEntry),
                                             (&CapwapIsSetFsWtpModelEntry));

            MEMCPY (CapwapGetFsWtpModelEntry.MibObject.
                    au1FsCapwapWtpModelNumber, pu1Name, u4Len);
            CapwapGetFsWtpModelEntry.MibObject.i4FsCapwapWtpModelNumberLen =
                (INT4) u4Len;
            if (CapwapGetAllFsWtpModelTable (&CapwapGetFsWtpModelEntry) ==
                OSIX_SUCCESS)
            {
                if ((CapwapGetFsWtpModelEntry.MibObject.u4FsNoOfRadio != 0) &&
                    (CapwapGetFsWtpModelEntry.MibObject.
                     au1FsCapwapWtpTunnelMode[0] != 0))
                {
                    CAPWAP_FILL_FSWTPMODELTABLE_ARGS ((&CapwapSetFsWtpModelEntry), (&CapwapIsSetFsWtpModelEntry), pu1Name, &u4Len, dummy_var, dummy_var, dummy_var, dummy_var, args[6], args[7], dummy_var, dummy_var, args[10], args[11], args[12], args[13]);

                    i4RetStatus =
                        CapwapCliSetFsWtpModelTable (CliHandle,
                                                     (&CapwapSetFsWtpModelEntry),
                                                     (&CapwapIsSetFsWtpModelEntry));
                }
            }
            break;

        case CLI_CAPWAP_FSWTPRADIOTABLE:
            MEMSET (&CapwapSetFsWtpRadioEntry, 0,
                    sizeof (tCapwapFsWtpRadioEntry));
            MEMSET (&CapwapIsSetFsWtpRadioEntry, 0,
                    sizeof (tCapwapIsSetFsWtpRadioEntry));

#ifdef CLI_WANTED
            CLI_GET_MODELNAME ((INT1 *) au1PromptName);
#endif
            pu1Name = (UINT1 *) STRTOK_R (pu1PromptName, "/", &saveptr);
            if (NULL == pu1Name)
            {
                break;
            }
            u4Len = STRLEN (pu1Name);
            CAPWAP_FILL_FSWTPRADIOTABLE_ARGS ((&CapwapSetFsWtpRadioEntry),
                                              (&CapwapIsSetFsWtpRadioEntry),
                                              args[0], args[1], args[2],
                                              args[3], pu4WtpRowStatus, pu1Name,
                                              &u4Len);
            i4RetStatus =
                CapwapCliSetFsWtpRadioTable (CliHandle,
                                             (&CapwapSetFsWtpRadioEntry),
                                             (&CapwapIsSetFsWtpRadioEntry));
            if (i4RetStatus != OSIX_FAILURE)
            {
                CAPWAP_FILL_FSWTPRADIOTABLE_ARGS ((&CapwapSetFsWtpRadioEntry),
                                                  (&CapwapIsSetFsWtpRadioEntry),
                                                  args[0], NULL, NULL,
                                                  NULL, args[4], pu1Name,
                                                  &u4Len);
                i4RetStatus =
                    CapwapCliSetFsWtpRadioTable (CliHandle,
                                                 (&CapwapSetFsWtpRadioEntry),
                                                 (&CapwapIsSetFsWtpRadioEntry));
            }
            break;

        case CLI_CAPWAP_FSCAPWAPWHITELISTTABLE:
        {
            MEMSET (&CapwapSetFsCapwapWhiteListEntry, 0,
                    sizeof (tCapwapFsCapwapWhiteListEntry));
            MEMSET (&CapwapIsSetFsCapwapWhiteListEntry, 0,
                    sizeof (tCapwapIsSetFsCapwapWhiteListEntry));
            if (args[1] != NULL)
            {
                CliDotStrToMac ((UINT1 *) args[1], au1InMacAddr);
                if (nmhTestv2MacAddress (&u4ErrCode, au1InMacAddr)
                    == CLI_FAILURE)
                {
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
            }
            length = MAC_ADDR_LEN;
            pu4RowStatus = args[3];
            if (*pu4RowStatus == DESTROY)
            {
                for (u4WhiteListId = 1; u4WhiteListId <= CLI_WTP_MAX_ACL_ID;
                     u4WhiteListId++)
                {
                    MEMSET (au1MacArray, 0, 18);
                    MacAddress.pu1_OctetList = au1MacArray;
                    if (nmhGetFsCapwapWhiteListWtpBaseMac (u4WhiteListId,
                                                           &MacAddress) !=
                        SNMP_SUCCESS)
                    {
                        continue;
                    }
                    if (MEMCMP (MacAddress.pu1_OctetList,
                                au1InMacAddr, STRLEN (au1InMacAddr)) != 0)
                    {
                        continue;
                    }
                    else
                    {
                        /* Entry found. Break form the loop */
                        break;
                    }
                }
                CAPWAP_FILL_FSCAPWAPWHITELISTTABLE_ARGS ((&CapwapSetFsCapwapWhiteListEntry), (&CapwapIsSetFsCapwapWhiteListEntry), pu4WhiteListId, au1InMacAddr, &length, args[3]);
                i4RetStatus =
                    CapwapCliSetFsCapwapWhiteListTable (CliHandle,
                                                        (&CapwapSetFsCapwapWhiteListEntry),
                                                        (&CapwapIsSetFsCapwapWhiteListEntry));

            }
            else
            {
                for (u4WhiteListId = 1; u4WhiteListId <= CLI_WTP_MAX_ACL_ID;
                     u4WhiteListId++)
                {

                    if (nmhGetFsCapwapWhiteListRowStatus
                        (u4WhiteListId, &i4RowStatus) != SNMP_SUCCESS)

                    {
                        CAPWAP_FILL_FSCAPWAPWHITELISTTABLE_ARGS ((&CapwapSetFsCapwapWhiteListEntry), (&CapwapIsSetFsCapwapWhiteListEntry), pu4WhiteListId, au1InMacAddr, &length, args[3]);
                        i4RetStatus = CapwapCliSetFsCapwapWhiteListTable
                            (CliHandle, (&CapwapSetFsCapwapWhiteListEntry),
                             (&CapwapIsSetFsCapwapWhiteListEntry));

                        break;

                    }
                }
                if (u4WhiteListId > CLI_WTP_MAX_ACL_ID)
                {
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
            }
        }
            break;
        case CLI_CAPWAP_FSCAPWAPBLACKLIST:
        {
            MEMSET (&CapwapSetFsCapwapBlackListEntry, 0,
                    sizeof (tCapwapFsCapwapBlackListEntry));
            MEMSET (&CapwapIsSetFsCapwapBlackListEntry, 0,
                    sizeof (tCapwapIsSetFsCapwapBlackListEntry));
            if (args[1] != NULL)
            {
                CliDotStrToMac ((UINT1 *) args[1], au1InMacAddr);
                if (nmhTestv2MacAddress (&u4ErrCode, au1InMacAddr)
                    == CLI_FAILURE)
                {
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
            }
            length = MAC_ADDR_LEN;
            pu4RowStatus = args[3];
            if (*pu4RowStatus == DESTROY)
            {
                for (u4BlackListId = 1; u4BlackListId <= CLI_WTP_MAX_ACL_ID;
                     u4BlackListId++)
                {
                    MEMSET (au1MacArray, 0, 18);
                    MacAddress.pu1_OctetList = au1MacArray;
                    if (nmhGetFsCapwapBlackListWtpBaseMac (u4BlackListId,
                                                           &MacAddress) !=
                        SNMP_SUCCESS)
                    {
                        continue;
                    }
                    if (MEMCMP (MacAddress.pu1_OctetList,
                                au1InMacAddr, STRLEN (au1InMacAddr)) != 0)
                    {
                        continue;
                    }
                    else
                    {
                        /* Entry found. Break form the loop */
                        break;
                    }
                }
                CAPWAP_FILL_FSCAPWAPBLACKLIST_ARGS ((&CapwapSetFsCapwapBlackListEntry), (&CapwapIsSetFsCapwapBlackListEntry), pu4BlackListId, au1InMacAddr, &length, args[3]);
                i4RetStatus =
                    CapwapCliSetFsCapwapBlackList (CliHandle,
                                                   (&CapwapSetFsCapwapBlackListEntry),
                                                   (&CapwapIsSetFsCapwapBlackListEntry));

                pu4RowStatus = args[2];

            }
            else
            {
                for (u4BlackListId = 1; u4BlackListId <= CLI_WTP_MAX_ACL_ID;
                     u4BlackListId++)
                {

                    if (nmhGetFsCapwapBlackListRowStatus
                        (u4BlackListId, &i4RowStatus) != SNMP_SUCCESS)

                    {
                        CAPWAP_FILL_FSCAPWAPBLACKLIST_ARGS ((&CapwapSetFsCapwapBlackListEntry), (&CapwapIsSetFsCapwapBlackListEntry), pu4BlackListId, au1InMacAddr, &length, args[3]);
                        i4RetStatus = CapwapCliSetFsCapwapBlackList
                            (CliHandle, (&CapwapSetFsCapwapBlackListEntry),
                             (&CapwapIsSetFsCapwapBlackListEntry));

                        break;

                    }
                }
                if (u4BlackListId > CLI_WTP_MAX_ACL_ID)
                {
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
            }
        }
            break;
        case CLI_CAPWAP_FSCAPWAPWTPCONFIGTABLE:
            MEMSET (&CapwapSetFsCapwapWtpConfigEntry, 0,
                    sizeof (tCapwapFsCapwapWtpConfigEntry));
            MEMSET (&CapwapIsSetFsCapwapWtpConfigEntry, 0,
                    sizeof (tCapwapIsSetFsCapwapWtpConfigEntry));
            UINT1               au1WtpName[256];
            MEMSET (&au1WtpName, 0, 256);
            if (args[9] != NULL)
            {
                if (((*(UINT4 *) args[9]) == CAPWAP_CLEAR_AP_STATS)
                    && (args[13] == NULL))
                {
                    /* Clear the stats for all APs */
                    i4RetStatus = CapwapClearAllAPStats (CliHandle);
                    break;
                }
            }

            if (CapwapGetWtpProfileIdFromProfileName
                ((UINT1 *) args[13], &u4WtpProfileId) != OSIX_SUCCESS)
            {
                CliPrintf (CliHandle, "\r\n Wtp Profile not found \r\n");
                CLI_FATAL_ERROR (CliHandle);
                i4RetStatus = OSIX_FAILURE;
                break;
            }
            if (u4WtpProfileId == 0)
            {
                CliPrintf (CliHandle, "\r\n Entry not found \r\n");
                CLI_FATAL_ERROR (CliHandle);
                i4RetStatus = OSIX_FAILURE;
                break;
            }
            if (nmhGetFsCapwapWtpConfigRowStatus
                (u4WtpProfileId, (INT4 *) &u4RowStatus) != SNMP_SUCCESS)
            {
                u4RowStatus = (CREATE_AND_GO);
            }
            else
            {
                u4RowStatus = (ACTIVE);
            }
            if ((UINT4 *) args[14] != NULL)
            {
                u4IpAddr = *(UINT4 *) (args[14]);
            }
            if ((UINT4 *) args[12] != NULL)
            {
                SPRINTF ((CHR1 *) au1FsWtpCrashFileName, "%s.%d",
                         CORE_FILE_PREFIX, u4WtpProfileId);
                u4FsWtpCrashFileNameLen = STRLEN (au1FsWtpCrashFileName);
                CAPWAP_FILL_FSCAPWAPWTPCONFIGTABLE_ARGS ((&CapwapSetFsCapwapWtpConfigEntry), (&CapwapIsSetFsCapwapWtpConfigEntry), args[0], args[1], args[2], args[3], args[4], au1FsWtpCrashFileName, &u4FsWtpCrashFileNameLen, args[7], args[8], args[9], pu4WtpRowStatus, args[12], pu4WtpProfileId, pu4IpAddr);
            }
            else
            {
                CAPWAP_FILL_FSCAPWAPWTPCONFIGTABLE_ARGS ((&CapwapSetFsCapwapWtpConfigEntry), (&CapwapIsSetFsCapwapWtpConfigEntry), args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9], pu4WtpRowStatus, args[12], pu4WtpProfileId, pu4IpAddr);
            }
            i4RetStatus =
                CapwapCliSetFsCapwapWtpConfigTable (CliHandle,
                                                    (&CapwapSetFsCapwapWtpConfigEntry),
                                                    (&CapwapIsSetFsCapwapWtpConfigEntry));

            break;

        case CLI_CAPWAP_FSCAPWAPLINKENCRYPTIONTABLE:
            MEMSET (&CapwapSetFsCapwapLinkEncryptionEntry, 0,
                    sizeof (tCapwapFsCapwapLinkEncryptionEntry));
            MEMSET (&CapwapIsSetFsCapwapLinkEncryptionEntry, 0,
                    sizeof (tCapwapIsSetFsCapwapLinkEncryptionEntry));

            if (args[3] == NULL)
            {
                /* run thro'a all the wtp profile */
                if (nmhGetFirstIndexCapwapBaseWtpProfileTable (&u4WtpProfileId)
                    != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r\n Entry not found \r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                do
                {
                    CAPWAP_FILL_FSCAPWAPLINKENCRYPTIONTABLE_ARGS ((&CapwapSetFsCapwapLinkEncryptionEntry), (&CapwapIsSetFsCapwapLinkEncryptionEntry), args[0], args[1], args[2], pu4WtpProfileId);
                    i4RetStatus =
                        CapwapCliSetFsCapwapLinkEncryptionTable (CliHandle,
                                                                 (&CapwapSetFsCapwapLinkEncryptionEntry),
                                                                 (&CapwapIsSetFsCapwapLinkEncryptionEntry));
                    if (i4RetStatus != CLI_SUCCESS)
                    {
                        break;
                    }

                    u4PrevWtpProfileId = u4WtpProfileId;
                    u4WtpProfileId = 0;
                }
                while (nmhGetNextIndexCapwapBaseWtpProfileTable
                       (u4PrevWtpProfileId, &u4WtpProfileId) == SNMP_SUCCESS);
            }
            else
            {
                if (CapwapGetWtpProfileIdFromProfileName
                    ((UINT1 *) args[3], &u4WtpProfileId) != OSIX_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r\n Entry not found \r\n");
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                if (u4WtpProfileId == 0)
                {
                    CliPrintf (CliHandle, "\r\n Entry not found \r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                CAPWAP_FILL_FSCAPWAPLINKENCRYPTIONTABLE_ARGS ((&CapwapSetFsCapwapLinkEncryptionEntry), (&CapwapIsSetFsCapwapLinkEncryptionEntry), args[0], args[1], args[2], pu4WtpProfileId);
                i4RetStatus =
                    CapwapCliSetFsCapwapLinkEncryptionTable (CliHandle,
                                                             (&CapwapSetFsCapwapLinkEncryptionEntry),
                                                             (&CapwapIsSetFsCapwapLinkEncryptionEntry));
            }

/*

            CAPWAP_FILL_FSCAPWAPLINKENCRYPTIONTABLE_ARGS((&CapwapSetFsCapwapLinkEncryptionEntry),(&CapwapIsSetFsCapwapLinkEncryptionEntry),
                    args[0],                     args[1],                     args[2],                     args[3],                    args[4]);

            i4RetStatus = CapwapCliSetFsCapwapLinkEncryptionTable (CliHandle,(&CapwapSetFsCapwapLinkEncryptionEntry),(&CapwapIsSetFsCapwapLinkEncryptionEntry));
*/

            break;

        case CLI_CAPWAP_FSCAWAPDEFAULTWTPPROFILETABLE:
            MEMSET (&CapwapSetFsCapwapDefaultWtpProfileEntry, 0,
                    sizeof (tCapwapFsCapwapDefaultWtpProfileEntry));
            MEMSET (&CapwapIsSetFsCapwapDefaultWtpProfileEntry, 0,
                    sizeof (tCapwapIsSetFsCapwapDefaultWtpProfileEntry));

#ifdef CLI_WANTED
            CLI_GET_MODELNAME ((INT1 *) au1PromptName);
#endif
            pu1Name = (UINT1 *) STRTOK_R (pu1PromptName, "/", &saveptr);
            u4Len = STRLEN (pu1Name);

            CAPWAP_FILL_FSCAWAPDEFAULTWTPPROFILETABLE_ARGS ((&CapwapSetFsCapwapDefaultWtpProfileEntry), (&CapwapIsSetFsCapwapDefaultWtpProfileEntry), pu1Name, &u4Len, args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9]);

            i4RetStatus =
                CapwapCliSetFsCawapDefaultWtpProfileTable (CliHandle,
                                                           (&CapwapSetFsCapwapDefaultWtpProfileEntry),
                                                           (&CapwapIsSetFsCapwapDefaultWtpProfileEntry));
            break;

        case CLI_CAPWAP_FSCAPWAPDNSPROFILETABLE:
        {
            u4WtpProfileId = 0;
            u4RowStatus = 0;

            MEMSET (&CapwapSetFsCapwapDnsProfileEntry, 0,
                    sizeof (tCapwapFsCapwapDnsProfileEntry));
            MEMSET (&CapwapIsSetFsCapwapDnsProfileEntry, 0,
                    sizeof (tCapwapIsSetFsCapwapDnsProfileEntry));

            if (NULL != args[6])
            {
                if (CapwapGetWtpProfileIdFromProfileName
                    ((UINT1 *) args[6], &u4WtpProfileId) != OSIX_SUCCESS)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    break;
                    /* i4RetStatus = OSIX_FAILURE; */
                    /* return OSIX_FAILURE; */
                }
                if (u4WtpProfileId == 0)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    break;
                    /* i4RetStatus = 1; */
                    /* return OSIX_FAILURE; */
                }

                if ((nmhGetFsCapwapDnsProfileRowStatus
                     (u4WtpProfileId, (INT4 *) &u4RowStatus)) != SNMP_SUCCESS)
                {
                    u4RowStatus = CREATE_AND_GO;
                    CAPWAP_FILL_FSCAPWAPDNSPROFILETABLE_ARGS ((&CapwapSetFsCapwapDnsProfileEntry), (&CapwapIsSetFsCapwapDnsProfileEntry), dummy_var, dummy_var, dummy_var, dummy_var, pu4WtpRowStatus, pu4WtpProfileId);
                    i4RetStatus =
                        CapwapCliSetFsCapwapDnsProfileTable (CliHandle,
                                                             (&CapwapSetFsCapwapDnsProfileEntry),
                                                             (&CapwapIsSetFsCapwapDnsProfileEntry));

                    if (i4RetStatus == OSIX_FAILURE)
                    {
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = 1;
                        UNUSED_PARAM (i4RetStatus);
                        return i4RetStatus;
                    }
                }
                u4RowStatus = ACTIVE;
                CAPWAP_FILL_FSCAPWAPDNSPROFILETABLE_ARGS ((&CapwapSetFsCapwapDnsProfileEntry), (&CapwapIsSetFsCapwapDnsProfileEntry), args[0], args[1], args[2], args[3], pu4WtpRowStatus, pu4WtpProfileId);

                i4RetStatus =
                    CapwapCliSetFsCapwapDnsProfileTable (CliHandle,
                                                         (&CapwapSetFsCapwapDnsProfileEntry),
                                                         (&CapwapIsSetFsCapwapDnsProfileEntry));
            }
            else if (NULL == args[6])
            {
                i4RetStatus =
                    nmhGetFirstIndexCapwapBaseWtpProfileTable
                    (&u4NextProfileId);
                if (i4RetStatus == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle, "\r\n Entry not found\r\n");
                    break;
                }
                do
                {
                    u4WtpProfileId = u4NextProfileId;
                    if (u4WtpProfileId == 0)
                    {
                        CLI_FATAL_ERROR (CliHandle);
                        continue;
                        /* return OSIX_FAILURE; */
                    }

                    if ((nmhGetFsCapwapDnsProfileRowStatus
                         (u4WtpProfileId,
                          (INT4 *) &u4RowStatus)) != SNMP_SUCCESS)
                    {
                        u4RowStatus = CREATE_AND_GO;
                        CAPWAP_FILL_FSCAPWAPDNSPROFILETABLE_ARGS ((&CapwapSetFsCapwapDnsProfileEntry), (&CapwapIsSetFsCapwapDnsProfileEntry), dummy_var, dummy_var, dummy_var, dummy_var, pu4WtpRowStatus, pu4WtpProfileId);
                        i4RetStatus =
                            CapwapCliSetFsCapwapDnsProfileTable (CliHandle,
                                                                 (&CapwapSetFsCapwapDnsProfileEntry),
                                                                 (&CapwapIsSetFsCapwapDnsProfileEntry));

                        if (i4RetStatus == OSIX_FAILURE)
                        {
                            CLI_FATAL_ERROR (CliHandle);
                            continue;
                        }
                    }
                    u4RowStatus = ACTIVE;
                    CAPWAP_FILL_FSCAPWAPDNSPROFILETABLE_ARGS ((&CapwapSetFsCapwapDnsProfileEntry), (&CapwapIsSetFsCapwapDnsProfileEntry), args[0], args[1], args[2], args[3], pu4WtpRowStatus, pu4WtpProfileId);

                    i4RetStatus =
                        CapwapCliSetFsCapwapDnsProfileTable (CliHandle,
                                                             (&CapwapSetFsCapwapDnsProfileEntry),
                                                             (&CapwapIsSetFsCapwapDnsProfileEntry));
                }
                while (nmhGetNextIndexCapwapBaseWtpProfileTable
                       (u4WtpProfileId, &u4NextProfileId) == SNMP_SUCCESS);
            }
        }
            break;

        case CLI_CAPWAP_FSWTPNATIVEVLANIDTABLE:
        {
            u4WtpProfileId = 0;
            u4RowStatus = 0;

            MEMSET (&CapwapSetFsWtpNativeVlanIdTable, 0,
                    sizeof (tCapwapFsWtpNativeVlanIdTable));
            MEMSET (&CapwapIsSetFsWtpNativeVlanIdTable, 0,
                    sizeof (tCapwapIsSetFsWtpNativeVlanIdTable));

            if (args[1] != NULL)
            {
                if (CapwapGetWtpProfileIdFromProfileName
                    ((UINT1 *) args[1], &u4WtpProfileId) != OSIX_SUCCESS)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                if (u4WtpProfileId == 0)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }

                if ((nmhGetFsWtpNativeVlanIdRowStatus
                     (u4WtpProfileId, (INT4 *) &u4RowStatus)) != SNMP_SUCCESS)
                {
                    u4RowStatus = CREATE_AND_GO;
                }
                else
                {
                    u4RowStatus = ACTIVE;
                }
            }

            CAPWAP_FILL_FSWTPNATIVEVLANIDTABLE_ARGS ((&CapwapSetFsWtpNativeVlanIdTable), (&CapwapIsSetFsWtpNativeVlanIdTable), args[0], pu4WtpRowStatus, pu4WtpProfileId);

            i4RetStatus =
                CapwapCliSetFsWtpNativeVlanIdTable (CliHandle,
                                                    (&CapwapSetFsWtpNativeVlanIdTable),
                                                    (&CapwapIsSetFsWtpNativeVlanIdTable));
            break;
        }

        case CLI_CAPWAP_FSWTPLOCALROUTINGTABLE:
        {
            u4WtpProfileId = 0;
            u4RowStatus = 0;

            MEMSET (&CapwapSetFsWtpLocalRoutingTable, 0,
                    sizeof (tCapwapFsWtpLocalRoutingTable));
            MEMSET (&CapwapIsSetFsWtpLocalRoutingTable, 0,
                    sizeof (tCapwapIsSetFsWtpLocalRoutingTable));

            if (args[1] != NULL)
            {
                if (CapwapGetWtpProfileIdFromProfileName
                    ((UINT1 *) args[1], &u4WtpProfileId) != OSIX_SUCCESS)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                if (u4WtpProfileId == 0)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }

                if ((nmhGetFsWtpLocalRoutingRowStatus
                     (u4WtpProfileId, (INT4 *) &u4RowStatus)) != SNMP_SUCCESS)
                {
                    u4RowStatus = CREATE_AND_GO;
                }
                else
                {
                    u4RowStatus = ACTIVE;
                }
            }

            if (nmhGetCapwapBaseWtpProfileWtpModelNumber (u4WtpProfileId,
                                                          &ModelName) ==
                SNMP_SUCCESS)
            {
                if (ModelName.pu1_OctetList != NULL)
                {
                    nmhGetFsCapwapWtpMacType (&ModelName, &i4WtpModelMacType);
                    if (CLI_MAC_TYPE_SPLIT == i4WtpModelMacType)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nCannot enable local routing to split mac\r\n");
                        i4RetStatus = 1;
                        break;

                    }
                }
            }

            CAPWAP_FILL_FSWTPLOCALROUTINGTABLE_ARGS ((&CapwapSetFsWtpLocalRoutingTable), (&CapwapIsSetFsWtpLocalRoutingTable), args[0], pu4WtpRowStatus, pu4WtpProfileId);

            i4RetStatus =
                CapwapCliSetFsWtpLocalRoutingTable (CliHandle,
                                                    (&CapwapSetFsWtpLocalRoutingTable),
                                                    (&CapwapIsSetFsWtpLocalRoutingTable));
            break;
        }
        case CLI_CAPWAP_FSCAWAPDISCSTATSTABLE:
            MEMSET (&CapwapSetFsCawapDiscStatsEntry, 0,
                    sizeof (tCapwapFsCawapDiscStatsEntry));
            MEMSET (&CapwapIsSetFsCawapDiscStatsEntry, 0,
                    sizeof (tCapwapIsSetFsCawapDiscStatsEntry));

            CAPWAP_FILL_FSCAWAPDISCSTATSTABLE_ARGS ((&CapwapSetFsCawapDiscStatsEntry), (&CapwapIsSetFsCawapDiscStatsEntry), args[0], args[1]);

            i4RetStatus =
                CapwapCliSetFsCawapDiscStatsTable (CliHandle,
                                                   (&CapwapSetFsCawapDiscStatsEntry),
                                                   (&CapwapIsSetFsCawapDiscStatsEntry));
            break;

        case CLI_CAPWAP_FSCAWAPJOINSTATSTABLE:
            MEMSET (&CapwapSetFsCawapJoinStatsEntry, 0,
                    sizeof (tCapwapFsCawapJoinStatsEntry));
            MEMSET (&CapwapIsSetFsCawapJoinStatsEntry, 0,
                    sizeof (tCapwapIsSetFsCawapJoinStatsEntry));

            CAPWAP_FILL_FSCAWAPJOINSTATSTABLE_ARGS ((&CapwapSetFsCawapJoinStatsEntry), (&CapwapIsSetFsCawapJoinStatsEntry), args[0], args[1]);

            i4RetStatus =
                CapwapCliSetFsCawapJoinStatsTable (CliHandle,
                                                   (&CapwapSetFsCawapJoinStatsEntry),
                                                   (&CapwapIsSetFsCawapJoinStatsEntry));
            break;

        case CLI_CAPWAP_FSCAWAPCONFIGSTATSTABLE:
            MEMSET (&CapwapSetFsCawapConfigStatsEntry, 0,
                    sizeof (tCapwapFsCawapConfigStatsEntry));
            MEMSET (&CapwapIsSetFsCawapConfigStatsEntry, 0,
                    sizeof (tCapwapIsSetFsCawapConfigStatsEntry));

            CAPWAP_FILL_FSCAWAPCONFIGSTATSTABLE_ARGS ((&CapwapSetFsCawapConfigStatsEntry), (&CapwapIsSetFsCawapConfigStatsEntry), args[0], args[1]);

            i4RetStatus =
                CapwapCliSetFsCawapConfigStatsTable (CliHandle,
                                                     (&CapwapSetFsCawapConfigStatsEntry),
                                                     (&CapwapIsSetFsCawapConfigStatsEntry));
            break;

        case CLI_CAPWAP_FSCAWAPRUNSTATSTABLE:
            pCapwapSetFsCawapRunStatsEntry =
                (tCapwapFsCawapRunStatsEntry *)
                MemAllocMemBlk (CAPWAP_FSCAWAPRUNSTATSTABLE_POOLID);
            if (pCapwapSetFsCawapRunStatsEntry == NULL)
            {
                i4RetStatus = OSIX_FAILURE;
                break;
            }
            MEMSET (pCapwapSetFsCawapRunStatsEntry, 0,
                    sizeof (tCapwapFsCawapRunStatsEntry));
            MEMSET (&CapwapIsSetFsCawapRunStatsEntry, 0,
                    sizeof (tCapwapIsSetFsCawapRunStatsEntry));

            CAPWAP_FILL_FSCAWAPRUNSTATSTABLE_ARGS ((pCapwapSetFsCawapRunStatsEntry), (&CapwapIsSetFsCawapRunStatsEntry), args[0], args[1]);

            i4RetStatus =
                CapwapCliSetFsCawapRunStatsTable (CliHandle,
                                                  (pCapwapSetFsCawapRunStatsEntry),
                                                  (&CapwapIsSetFsCawapRunStatsEntry));
            MemReleaseMemBlock (CAPWAP_FSCAWAPRUNSTATSTABLE_POOLID,
                                (UINT1 *) pCapwapSetFsCawapRunStatsEntry);
            break;

        case CLI_CAPWAP_FSCAPWAPWIRELESSBINDINGTABLE:
            MEMSET (&CapwapSetFsCapwapWirelessBindingEntry, 0,
                    sizeof (tCapwapFsCapwapWirelessBindingEntry));
            MEMSET (&CapwapIsSetFsCapwapWirelessBindingEntry, 0,
                    sizeof (tCapwapIsSetFsCapwapWirelessBindingEntry));

            CAPWAP_FILL_FSCAPWAPWIRELESSBINDINGTABLE_ARGS ((&CapwapSetFsCapwapWirelessBindingEntry), (&CapwapIsSetFsCapwapWirelessBindingEntry), args[0], args[1], args[2], args[3], args[4]);

            i4RetStatus =
                CapwapCliSetFsCapwapWirelessBindingTable (CliHandle,
                                                          (&CapwapSetFsCapwapWirelessBindingEntry),
                                                          (&CapwapIsSetFsCapwapWirelessBindingEntry));
            break;

        case CLI_CAPWAP_FSCAPWAPSTATIONWHITELIST:
        {
            CliDotStrToMac ((UINT1 *) args[1], au1InMacAddr);
            if (nmhTestv2MacAddress (&u4ErrCode, au1InMacAddr) == CLI_FAILURE)
            {
                return CLI_FAILURE;
            }
            MEMCPY (MacAddress.pu1_OctetList, au1InMacAddr, MAC_ADDR_LEN);
            i4RowStatus = (INT4) *args[3];
            if (nmhTestv2FsCapwapStationWhiteListRowStatus (&u4ErrCode,
                                                            &MacAddress,
                                                            i4RowStatus) !=
                SNMP_FAILURE)
            {
                if (nmhSetFsCapwapStationWhiteListRowStatus
                    (&MacAddress, i4RowStatus) != SNMP_FAILURE)
                {
                    i4RetStatus = CLI_SUCCESS;
                }
                else
                {
                    CliPrintf (CliHandle, "\nStation Configuration Failed\n");
                    i4RetStatus = CLI_FAILURE;
                    CLI_FATAL_ERROR (CliHandle);
                }
            }
            else
            {
                if (nmhGetFsCapwapStationBlackListRowStatus
                    (&MacAddress, &i4RowStatus) != SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\nEntered Mac Address already exists in Blacklist Table\n");
                    i4RetStatus = CLI_FAILURE;
                    CLI_FATAL_ERROR (CliHandle);
                }
                else
                {
                    CliPrintf (CliHandle, "\nStation Validation Failed\n");
                    i4RetStatus = CLI_FAILURE;
                    CLI_FATAL_ERROR (CliHandle);
                }
            }
        }
            break;

        case CLI_CAPWAP_FSCAPWAPSTATIONBLACKLIST:
        {
            CliDotStrToMac ((UINT1 *) args[1], au1InMacAddr);
            if (nmhTestv2MacAddress (&u4ErrCode, au1InMacAddr) == CLI_FAILURE)
            {
                return CLI_FAILURE;
            }
            MEMCPY (MacAddress.pu1_OctetList, au1InMacAddr, MAC_ADDR_LEN);
            i4RowStatus = (INT4) *args[3];
            if (nmhTestv2FsCapwapStationBlackListRowStatus (&u4ErrCode,
                                                            &MacAddress,
                                                            i4RowStatus) !=
                SNMP_FAILURE)
            {
                if (nmhSetFsCapwapStationBlackListRowStatus
                    (&MacAddress, i4RowStatus) != SNMP_FAILURE)
                {
                    i4RetStatus = CLI_SUCCESS;
                }
                else
                {
                    CliPrintf (CliHandle, "\nStation Configuration Failed\n");
                    i4RetStatus = CLI_FAILURE;
                    CLI_FATAL_ERROR (CliHandle);
                }
            }
            else
            {
                if (nmhGetFsCapwapStationWhiteListRowStatus
                    (&MacAddress, &i4RowStatus) != SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\nEntered Mac Address already exists in Whitelist Table\n");
                    i4RetStatus = CLI_FAILURE;
                    CLI_FATAL_ERROR (CliHandle);
                }
                else
                {

                    CliPrintf (CliHandle, "\nStation Validation Failed\n");
                    i4RetStatus = CLI_FAILURE;
                    CLI_FATAL_ERROR (CliHandle);
                }
            }
        }
            break;
        case CLI_CAPWAP_FSCAPWAPWTPREBOOTSTATISTICSTABLE:
            MEMSET (&CapwapSetFsCapwapWtpRebootStatisticsEntry, 0,
                    sizeof (tCapwapFsCapwapWtpRebootStatisticsEntry));
            MEMSET (&CapwapIsSetFsCapwapWtpRebootStatisticsEntry, 0,
                    sizeof (tCapwapIsSetFsCapwapWtpRebootStatisticsEntry));

            CAPWAP_FILL_FSCAPWAPWTPREBOOTSTATISTICSTABLE_ARGS ((&CapwapSetFsCapwapWtpRebootStatisticsEntry), (&CapwapIsSetFsCapwapWtpRebootStatisticsEntry), args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9]);

            i4RetStatus =
                CapwapCliSetFsCapwapWtpRebootStatisticsTable (CliHandle,
                                                              (&CapwapSetFsCapwapWtpRebootStatisticsEntry),
                                                              (&CapwapIsSetFsCapwapWtpRebootStatisticsEntry));
            break;

        case CLI_CAPWAP_FSCAPWAPWTPRADIOSTATISTICSTABLE:
            MEMSET (&CapwapSetFsCapwapWtpRadioStatisticsEntry, 0,
                    sizeof (tCapwapFsCapwapWtpRadioStatisticsEntry));
            MEMSET (&CapwapIsSetFsCapwapWtpRadioStatisticsEntry, 0,
                    sizeof (tCapwapIsSetFsCapwapWtpRadioStatisticsEntry));

            CAPWAP_FILL_FSCAPWAPWTPRADIOSTATISTICSTABLE_ARGS ((&CapwapSetFsCapwapWtpRadioStatisticsEntry), (&CapwapIsSetFsCapwapWtpRadioStatisticsEntry), args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9], args[10], args[11]);

            i4RetStatus =
                CapwapCliSetFsCapwapWtpRadioStatisticsTable (CliHandle,
                                                             (&CapwapSetFsCapwapWtpRadioStatisticsEntry),
                                                             (&CapwapIsSetFsCapwapWtpRadioStatisticsEntry));
            break;

        case CLI_CAPWAP_FSCAPWAPENABLE:

            i4RetStatus = CapwapCliSetFsCapwapEnable (CliHandle, args[0]);
            break;

        case CLI_CAPWAP_FSCAPWAPSHUTDOWN:

            i4RetStatus = CapwapCliSetFsCapwapShutdown (CliHandle, args[0]);
            break;

        case CLI_CAPWAP_FSCAPWAPCONTROLUDPPORT:

            i4RetStatus =
                CapwapCliSetFsCapwapControlUdpPort (CliHandle, args[0]);
            break;

        case CLI_CAPWAP_FSCAPWAPCONTROLCHANNELDTLSPOLICYOPTIONS:

            i4RetStatus =
                CapwapCliSetFsCapwapControlChannelDTLSPolicyOptions (CliHandle,
                                                                     args[0]);
            break;

        case CLI_CAPWAP_FSCAPWAPDATACHANNELDTLSPOLICYOPTIONS:

            i4RetStatus =
                CapwapCliSetFsCapwapDataChannelDTLSPolicyOptions (CliHandle,
                                                                  args[0]);
            break;

        case CLI_CAPWAP_FSWLCDISCOVERYMODE:

            i4RetStatus =
                CapwapCliSetFsWlcDiscoveryMode (CliHandle, (UINT1 *) args[0]);
            break;

        case CLI_CAPWAP_FSCAPWAPWTPMODEIGNORE:

            i4RetStatus =
                CapwapCliSetFsCapwapWtpModeIgnore (CliHandle, args[0]);
            break;

        case CLI_CAPWAP_FSCAPWAPDEBUGMASK:

            i4RetStatus =
                CapwapCliSetFsCapwapDebugMask (CliHandle, args[0], args[1],
                                               args[2]);
            break;
        case CLI_CAPWAP_FSCAPWAPNODEBUGMASK:
            i4RetStatus =
                CapwapCliSetFsCapwapNoDebugMask (CliHandle, args[0], args[1],
                                                 args[2]);
            break;
        case CLI_CAPWAP_FSCAPWAPWSSDATA:

            i4RetStatus =
                CapwapCliSetFsCapwapWssDataDebugMask (CliHandle, args[0]);
            break;
        case CLI_CAPWAP_FSCAPWAPNOWSSDATA:

            i4RetStatus =
                CapwapCliSetFsCapwapNoWssDataDebugMask (CliHandle, args[0]);
            break;
        case CLI_CAPWAP_STATION_CONF_TIMER:

            i4RetStatus =
                CapwapCliSetStationConfigTimer (CliHandle, *args[0], *args[1]);
            break;
        case CLI_CAPWAP_FSDTLSDEBUGMASK:

            i4RetStatus = CapwapCliSetFsDtlsDebugMask (CliHandle, args[0]);
            break;

        case CLI_CAPWAP_FSDTLSENCRYPTION:

            i4RetStatus = CapwapCliSetFsDtlsEncryption (CliHandle, args[0]);
            break;

        case CLI_CAPWAP_FSDTLSECAUTHENCRYPTALOGRITHAM:

            i4RetStatus =
                CapwapCliSetFsDtlsEncryptAlogritham (CliHandle, args[0]);
            break;

        case CLI_CAPWAP_FSSTATIONTYPE:

            i4RetStatus = CapwapCliSetFsStationType (CliHandle, args[0]);
            break;
        case CLI_CAPWAP_FSACTIMESTAMPTRIGGER:
            MEMSET (&CapwapSetCapwapBaseWtpProfileEntry, 0,
                    sizeof (tCapwapCapwapBaseWtpProfileEntry));
            MEMSET (&CapwapIsSetCapwapBaseWtpProfileEntry, 0,
                    sizeof (tCapwapIsSetCapwapBaseWtpProfileEntry));

            if (args[0] == NULL)
            {
                u4Trigger = CLI_FSCAPWAP_ENABLE;
                i4RetStatus =
                    CapwapCliSetFsAcTimestampTrigger (CliHandle, &u4Trigger);
            }
            else
            {
                if (CapwapGetWtpProfileIdFromProfileName ((UINT1 *) args[0],
                                                          &u4WtpProfileId) !=
                    OSIX_SUCCESS)
                {
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }

                CapwapSetCapwapBaseWtpProfileEntry.MibObject.
                    u4CapwapBaseWtpProfileId = u4WtpProfileId;
                CapwapIsSetCapwapBaseWtpProfileEntry.bCapwapBaseWtpProfileId =
                    OSIX_TRUE;
                CapwapSetCapwapBaseWtpProfileEntry.MibObject.
                    i4FsCapwapFsAcTimestampTrigger = CLI_FSCAPWAP_ENABLE;
                CapwapIsSetCapwapBaseWtpProfileEntry.
                    bFsCapwapFsAcTimestampTrigger = OSIX_TRUE;
                i4RetStatus =
                    CapwapCliSetCapwapBaseWtpProfileTable (CliHandle,
                                                           (&CapwapSetCapwapBaseWtpProfileEntry),
                                                           (&CapwapIsSetCapwapBaseWtpProfileEntry));

            }

            break;

        case CLI_CAPWAP_FSFRAGMENTREASSEMBLE:

            if (CapwapGetWtpProfileIdFromProfileName ((UINT1 *) args[0],
                                                      &u4WtpProfileId) !=
                OSIX_SUCCESS)
            {
                i4RetStatus = OSIX_FAILURE;
                break;
            }
            if (args[1] != NULL)
            {
                if (nmhTestv2FsCapwapFragReassembleStatus (&u4ErrCode,
                                                           u4WtpProfileId,
                                                           (INT4) (*args[1])) ==
                    SNMP_SUCCESS)
                {
                    if (CapwapSetFragReassembleStatus
                        (u4WtpProfileId, (UINT1) (*args[1])) != OSIX_SUCCESS)
                    {
                        i4RetStatus = OSIX_FAILURE;
                        break;
                    }
                }
                else
                {
                    CLI_FATAL_ERROR (CliHandle);
                    CLI_SET_ERR (CAPW_CLI_FRAG_REASSEMBLE_ERR);
                }

            }

            break;
        case CLI_CAPWAP_FSWTPMODELCONFIGURE:

            i4RetStatus =
                CapwapModelTableConfigure (CliHandle, (UINT1 *) args[0]);
            break;
        case CLI_CAPWAP_FSWTPMODELDELETE:

            i4RetStatus = CapwapModelTableDelete (CliHandle, (UINT1 *) args[0]);
            break;
        case CLI_CAPWAP_RETRANSMIT_COUNT:
            if (args[0] != NULL)
            {
                if (nmhTestv2FsCapwapMaxRetransmitCount (&u4ErrCode, *args[0])
                    != SNMP_FAILURE)
                {
                    if (nmhSetFsCapwapMaxRetransmitCount (*args[0]) ==
                        SNMP_FAILURE)
                    {
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = OSIX_FAILURE;
                    }
                }
            }
            else
            {
                i4RetStatus = OSIX_FAILURE;
            }
            break;

        case CLI_CAPWAP_WTP_TRAP_STATUS:

            i4RetStatus = CapwapCliSetFsCapwTrapStatus (CliHandle, args[0]);
            break;

        case CLI_CAPWAP_CRITICAL_TRAP_STATUS:

            i4RetStatus =
                CapwapCliSetFsCapwCriticalTrapStatus (CliHandle, args[0]);
            break;
        case CLI_CAPWAP_BINDING_TABLE:
            if (args[0] != NULL)
            {
                u4RowStatus = (CREATE_AND_GO);
#ifdef CLI_WANTED
                CLI_GET_MODELNAME ((INT1 *) au1PromptName);
#endif
                pu1Name = (UINT1 *) STRTOK_R (pu1PromptName, "/", &saveptr);
                u4Len = STRLEN (pu1Name);
                MEMCPY (ModelName.pu1_OctetList, pu1Name, u4Len);
                ModelName.i4_Length = u4Len;

                if (nmhTestv2FsWtpModelWlanBindingRowStatus
                    (&u4ErrCode, &ModelName, *args[1], *args[2],
                     u4RowStatus) != SNMP_FAILURE)
                {
                    if (*args[0] == CREATE_WLAN_BINDING)
                    {
                        u4RowStatus = CREATE_AND_GO;
                    }
                    else if (*args[0] == DESTROY_WLAN_BINDING)
                    {
                        u4RowStatus = DESTROY;
                    }
                    if (nmhSetFsWtpModelWlanBindingRowStatus
                        (&ModelName, *args[1], *args[2],
                         u4RowStatus) == SNMP_FAILURE)
                    {
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = OSIX_FAILURE;
                    }
                    else
                    {
                        i4RetStatus = OSIX_SUCCESS;
                    }
                }
                else
                {
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                }
            }
            else
            {
                i4RetStatus = OSIX_FAILURE;
            }
            break;
        case CLI_CAPWAP_LOCAL_ROUTING:
            if (args[0] != NULL)
            {
                u4RowStatus = (CREATE_AND_GO);
#ifdef CLI_WANTED
                CLI_GET_MODELNAME ((INT1 *) au1PromptName);
#endif
                pu1Name = (UINT1 *) STRTOK_R (pu1PromptName, "/", &saveptr);
                u4Len = STRLEN (pu1Name);
                MEMCPY (ModelName.pu1_OctetList, pu1Name, u4Len);
                ModelName.i4_Length = u4Len;
                if (nmhTestv2FsWtpModelLocalRouting
                    (&u4ErrCode, &ModelName, *args[0]) != SNMP_FAILURE)
                {
                    if (nmhSetFsWtpModelLocalRouting (&ModelName, *args[0]) ==
                        SNMP_FAILURE)
                    {
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = OSIX_FAILURE;
                    }
                }
                else
                {
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                }
            }
            else
            {
                i4RetStatus = OSIX_FAILURE;
            }
            break;
        default:
            break;
    }
    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_CAPWAP_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%s", CapwapCliErrString[u4ErrCode - 1]);
        }
        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS ((UINT4) i4RetStatus);

#ifdef CLI_WANTED
    CliUnRegisterLock (CliHandle);
#endif
    CAPWAP_UNLOCK;
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    UNUSED_PARAM (i4RetStatus);
    return i4RetStatus;

}

/****************************************************************************
 *  Function    :  CapwapModelTableConfigure
 *  Description :  This function will change the prompt name from config
 *                terminal to wtp model name
 *
 * Input       :  CliHandle - CLI Handler
 *                pu1Prompt - Prompt Name
 *
 * Output      :  None
 *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE
 ****************************************************************************/

INT4
CapwapModelTableConfigure (tCliHandle CliHandle, UINT1 *pu1Prompt)
{
    UINT1               au1Cmd[MAX_PROMPT_LEN];
    UINT4               u4ErrCode;
    tSNMP_OCTET_STRING_TYPE ModelName;
    INT4                i4FsWtpModelRowStatus;

    MEMSET (au1Cmd, 0, MAX_PROMPT_LEN);
    MEMSET (&ModelName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    ModelName.i4_Length = (INT4) (STRLEN (pu1Prompt));

    ModelName.pu1_OctetList = pu1Prompt;

    /* Check if the profile is already present */
    if ((nmhGetFsWtpModelRowStatus (&ModelName,
                                    &i4FsWtpModelRowStatus)) != SNMP_SUCCESS)
    {
        /* Profile NOT present - CREATE */

        if (nmhTestv2FsWtpModelRowStatus (&u4ErrCode, &ModelName,
                                          CREATE_AND_GO) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n Entry Creation failed \r\n");
            CLI_FATAL_ERROR (CliHandle);
            return OSIX_FAILURE;
        }
        /* CREATE Profile */
        if (nmhSetFsWtpModelRowStatus (&ModelName, CREATE_AND_GO)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n Row Status Creation failed \r\n");
            CLI_FATAL_ERROR (CliHandle);
            return OSIX_FAILURE;
        }
    }

    /* ENTER WTP Model */
    SPRINTF ((CHR1 *) au1Cmd, "%s%s", CAPWAP_CLI_WTP_MODEL, pu1Prompt);
#ifdef CLI_WANTED
    if (CliChangePath ((CHR1 *) au1Cmd) == CLI_FAILURE)
    {
        CliPrintf (CliHandle, "/r%% Unable to enter into wtp model mode\r\n");
        return OSIX_FAILURE;
    }
#endif
    return OSIX_SUCCESS;
}

#ifdef WLC_WANTED

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CapwapGetBatchDhcpPrompt                           */
/*                                                                           */
/*     DESCRIPTION      : This function Checks for dhcp pool id  and         */
/*                        return the prompt to be displayed.                 */
/*                                                                           */
/*     INPUT            : pi1ModelName - Mode to be configured.              */
/*                                                                           */
/*     OUTPUT           : pi1DispStr  - Prompt to be displayed.              */
/*                                                                           */
/*     RETURNS          : TRUE or FALSE                                      */
/*                                                                           */
/*****************************************************************************/

INT1
CapwapGetBatchDhcpPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len = STRLEN (CLI_AP_DHCP_POOL);
    UINT4               u4Poolid = 0;
    if ((!pi1DispStr) || (!pi1ModeName))
    {
        return FALSE;
    }

    if (STRNCMP (pi1ModeName, CLI_AP_DHCP_POOL, u4Len) != 0)
    {
        return FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    u4Poolid = (UINT4) CLI_ATOI (pi1ModeName);

    /*
     ** No need to take lock here, since it is taken by
     ** Cli in cli_process_wssuser_cmd.
     **/
    CLI_SET_POOLID (u4Poolid);

    STRCPY (pi1DispStr, "(config-dhcp-ap-pool)#");

    return TRUE;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CapwapGetBatchFirewallPrompt                       */
/*                                                                           */
/*     DESCRIPTION      : This function Checks for firewall configurations   */
/*                        and return the prompt to be displayed.             */
/*                                                                           */
/*     INPUT            : pi1ModelName - Mode to be configured.              */
/*                                                                           */
/*     OUTPUT           : pi1DispStr  - Prompt to be displayed.              */
/*                                                                           */
/*     RETURNS          : TRUE or FALSE                                      */
/*                                                                           */
/*****************************************************************************/

INT1
CapwapGetBatchFirewallPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len = STRLEN (CLI_AP_FIREWALL_PROMPT);
    if ((!pi1DispStr) || (!pi1ModeName))
    {
        return FALSE;
    }

    if (STRNCMP (pi1ModeName, CLI_AP_FIREWALL_PROMPT, u4Len) != 0)
    {
        return FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    STRCPY (pi1DispStr, "(config-firewall)#");
    return TRUE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CapwapGetBatchNatPrompt                            */
/*                                                                           */
/*     DESCRIPTION      : This function Checks for Nat pool id  and          */
/*                        return the prompt to be displayed.                 */
/*                                                                           */
/*     INPUT            : pi1ModelName - Mode to be configured.              */
/*                                                                           */
/*     OUTPUT           : pi1DispStr  - Prompt to be displayed.              */
/*                                                                           */
/*     RETURNS          : TRUE or FALSE                                      */
/*                                                                           */
/*****************************************************************************/

INT1
CapwapGetBatchNatPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len = STRLEN (CLI_NAT_POOL);
    UINT4               u4Poolid = 0;
    if ((!pi1DispStr) || (!pi1ModeName))
    {
        return FALSE;
    }

    if (STRNCMP (pi1ModeName, CLI_NAT_POOL, u4Len) != 0)
    {
        return FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    u4Poolid = (UINT4) CLI_ATOI (pi1ModeName);

    u4Poolid = (UINT4) CLI_ATOI (pi1ModeName);

    /*
     * * No need to take lock here, since it is taken by
     * * Cli in cli_process_wssuser_cmd.
     * */

    CLI_SET_NATID (u4Poolid);

    STRCPY (pi1DispStr, "(config-nat-pool)#");

    return TRUE;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CapwapGetBatchL3SubIfPrompt                        */
/*                                                                           */
/*     DESCRIPTION      : This function Checks for Nat pool id  and          */
/*                        return the prompt to be displayed.                 */
/*                                                                           */
/*     INPUT            : pi1ModelName - Mode to be configured.              */
/*                                                                           */
/*     OUTPUT           : pi1DispStr  - Prompt to be displayed.              */
/*                                                                           */
/*     RETURNS          : TRUE or FALSE                                      */
/*                                                                           */
/*****************************************************************************/

INT1
CapwapGetBatchL3SubIfPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len = STRLEN (CLI_AP_L3SUBIF_MODE);
    UINT4               u4IfIndex = 0;
    if ((!pi1DispStr) || (!pi1ModeName))
    {
        return FALSE;
    }

    if (STRNCMP (pi1ModeName, CLI_AP_L3SUBIF_MODE, u4Len) != 0)
    {
        return FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    u4IfIndex = (UINT4) CLI_ATOI (pi1ModeName);

    /*
     * * No need to take lock here, since it is taken by
     * * Cli in cli_process_wssuser_cmd.
     * */
    CLI_SET_IFINDEX (u4IfIndex);

    STRCPY (pi1DispStr, "(config-l3subif)#");

    return TRUE;

}
#endif

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CapwapGetWtpModelCfgPrompt                         */
/*                                                                           */
/*     DESCRIPTION      : This function Checks for Wtp model  and            */
/*                        return the prompt to be displayed.                 */
/*                                                                           */
/*     INPUT            : pi1ModelName - Mode to be configured.               */
/*                                                                           */
/*     OUTPUT           : pi1DispStr  - Prompt to be displayed.              */
/*                                                                           */
/*     RETURNS          : TRUE or FALSE                                      */
/*                                                                           */
/*****************************************************************************/
INT1
CapwapGetWtpModelCfgPrompt (INT1 *pi1ModelName, INT1 *pi1DispStr)
{
    UINT4               u4Len = STRLEN (CAPWAP_CLI_WTP_MODEL);
    INT4                i4FsWtpModelRowStatus;
    tSNMP_OCTET_STRING_TYPE ModelName;

    MEMSET (&ModelName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    if (!pi1DispStr || !pi1ModelName)
    {
        return FALSE;
    }

    if (STRNCMP (pi1ModelName, CAPWAP_CLI_WTP_MODEL, u4Len) != 0)
    {
        return FALSE;
    }
    pi1ModelName = pi1ModelName + u4Len;

    ModelName.i4_Length = (INT4) (STRLEN (pi1ModelName));
    ModelName.pu1_OctetList = (UINT1 *) pi1ModelName;
    /*
     * No need to take lock here, since it is taken by
     * Cli in cli_process_vlan_cmd.
     * */
    if ((nmhGetFsWtpModelRowStatus (&ModelName,
                                    &i4FsWtpModelRowStatus)) != SNMP_SUCCESS)
    {
        return FALSE;
    }

#ifdef CLI_WANTED
    CLI_SET_MODELNAME (pi1ModelName);
    CLI_SET_CXT_ID (0);
#endif
    STRCPY (pi1DispStr, "(config-wtp-model)#");

    return TRUE;
}

/****************************************************************************
 * Function    :  CapwapModelTableDelete 
 * Description :  This function will delete the entry for the given wtp model
 *                name
 *
 * Input       :  CliHandle - CLI Handler
 *                pu1Prompt - Prompt Name
 *
 * Output      :  None
 *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE
 * ****************************************************************************/
INT4
CapwapModelTableDelete (tCliHandle CliHandle, UINT1 *pu1Prompt)
{
    UINT1               au1Cmd[MAX_PROMPT_LEN];
    UINT4               u4ErrCode;
    tSNMP_OCTET_STRING_TYPE ModelName;
    MEMSET (au1Cmd, 0, MAX_PROMPT_LEN);
    MEMSET (&ModelName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    ModelName.i4_Length = (INT4) (STRLEN (pu1Prompt));

    ModelName.pu1_OctetList = pu1Prompt;

    if (nmhTestv2FsWtpModelRowStatus (&u4ErrCode, &ModelName,
                                      DESTROY) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return OSIX_FAILURE;
    }
    /* DELETE Profile */
    if (nmhSetFsWtpModelRowStatus (&ModelName, DESTROY) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n Row Status Deletion failed \r\n");
        CLI_FATAL_ERROR (CliHandle);
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

INT4
cli_process_Capwap_show_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *args[CAPWAP_CLI_MAX_ARGS];
    INT1                argno = 0;
    INT4                i4RetStatus = CLI_FAILURE;
    UINT4               u4CmdType = 0;
    INT4                i4Inst;

    UNUSED_PARAM (u4CmdType);
    /* check if the capwap module is started. i.e "no shutdown". else all the
     * commands are considered invalid */
    if (CapwapCliCheckModuleStatusShow (CliHandle) != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }
#ifdef CLI_WANTED
    CliRegisterLock (CliHandle, CapwapMainTaskLock, CapwapMainTaskUnLock);
#endif
    CAPWAP_LOCK;

    va_start (ap, u4Command);

    i4Inst = va_arg (ap, INT4);

    if (i4Inst != 0)
    {
        /* Coverity fix - It is analayzed that all the 
         * Capwap CLI commands function use the first argument as NULL.
         * So the extraction of the first index is not required in this case  */
    }

    while (1)
    {

        args[argno++] = va_arg (ap, UINT4 *);
        if (argno == CAPWAP_CLI_MAX_ARGS)
            break;
    }

    va_end (ap);
    switch (u4Command)
    {
        case CLI_CAPWAP_SHOW_ADVANCED_TIMERS:
            i4RetStatus = CapwapShowAdvancedTimers (CliHandle);
            break;
        default:
            break;
    }

    CLI_SET_CMD_STATUS ((UINT4) i4RetStatus);

#ifdef CLI_WANTED
    CliUnRegisterLock (CliHandle);
#endif
    CAPWAP_UNLOCK;
    UNUSED_PARAM (args);
    return i4RetStatus;
}

/****************************************************************************
* Function    : CapwapShowAdvancedTimers
* Description :
* Input       : NONE
* Output      :  None
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
CapwapShowAdvancedTimers (tCliHandle CliHandle)
{
    UINT4               u4state_timers = 0;
    UINT4               u4data_timers = 0;
    UINT4               u4dtls_timers = 0;
    UINT4               u4echo_timers = 0;
    UINT4               u4retransmit_timers = 0;
    UINT4               u4silent_timers = 0;
    UINT4               u4waitdtls_timers = 0;
    UINT4               u4waitjoin_timers = 0;
    UINT4               u4RetransmitCount = 0;

    if (CapwapGetCapwapBaseAcChangeStatePendingTimer (&u4state_timers) !=
        OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (CapwapGetCapwapBaseAcDataCheckTimer (&u4data_timers) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (CapwapGetCapwapBaseAcDTLSSessionDeleteTimer (&u4dtls_timers) !=
        OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (CapwapGetCapwapBaseAcEchoInterval (&u4echo_timers) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (CapwapGetCapwapBaseAcRetransmitInterval (&u4retransmit_timers) !=
        OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (CapwapGetCapwapBaseAcSilentInterval (&u4silent_timers) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (CapwapGetCapwapBaseAcWaitDTLSTimer (&u4waitdtls_timers) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (CapwapGetCapwapBaseAcWaitJoinTimer (&u4waitjoin_timers) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (CapwapGetCapwapBaseAcMaxRetransmit (&u4RetransmitCount) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    CliPrintf (CliHandle, "\r\nState Timer           : %d\r\n", u4state_timers);
    CliPrintf (CliHandle, "Data Timer            : %d\r\n", u4data_timers);
    CliPrintf (CliHandle, "DTLS Timer            : %d\r\n", u4dtls_timers);
    CliPrintf (CliHandle, "Echo Timer            : %d\r\n", u4echo_timers);
    CliPrintf (CliHandle, "Retransmit Timer      : %d\r\n",
               u4retransmit_timers);
    CliPrintf (CliHandle, "Silent Timer          : %d\r\n", u4silent_timers);
    CliPrintf (CliHandle, "WAIT DTLS Timer       : %d\r\n", u4waitdtls_timers);
    CliPrintf (CliHandle, "WAIT JOIN Timer       : %d\r\n", u4waitjoin_timers);
    CliPrintf (CliHandle, "Retransmit Count      : %d\r\n", u4RetransmitCount);

    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : CapwapShowGlobals
* Description :
* Input       : NONE
* Output      :  None
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
CapwapShowGlobals (tCliHandle CliHandle)
{
#ifdef WLC_WANTED
    INT4                i4Mode_Ignore = 0;
    INT4                i4Capwap_Enable = 0;
    UINT1               u1Wlc_Discovery_Mode = 0;
#endif
    UINT4               u4Control_UDP_Port = 0;

#ifdef WLC_WANTED
    if (CapwapGetFsCapwapWtpModeIgnore (&i4Mode_Ignore) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (CapwapGetFsWlcDiscoveryMode (&u1Wlc_Discovery_Mode) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (CapwapGetFsCapwapEnable (&i4Capwap_Enable) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
#endif
    if (CapwapGetFsCapwapControlUdpPort (&u4Control_UDP_Port) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

#ifdef WLC_WANTED
    if (i4Capwap_Enable == CLI_FSCAPWAP_ENABLE)
        CliPrintf (CliHandle, "\r\nCAPWAP                : ENABLE\r\n");
    else if (i4Capwap_Enable == CLI_FSCAPWAP_DISABLE)
        CliPrintf (CliHandle, "\r\nCAPWAP                : DISABLE\r\n");

    if (i4Mode_Ignore == CLI_FSCAPWAP_MODEL_ENABLE)
        CliPrintf (CliHandle, "\r\nAP MODEL CHECK        : ENABLE\r\n");
    else if (i4Mode_Ignore == CLI_FSCAPWAP_MODEL_DISABLE)
        CliPrintf (CliHandle, "\r\nAP MODEL CHECK        : DISABLE\r\n");

    if (u1Wlc_Discovery_Mode == CLI_CAPWAP_DISC_MODE_MAC)
    {
        CliPrintf (CliHandle, "\r\nDISCOVERY MODE        : MAC\r\n");
    }
    else if (u1Wlc_Discovery_Mode == CLI_CAPWAP_DISC_MODE_MAC_WHITELIST)
    {
        CliPrintf (CliHandle, "\r\nDISCOVERY MODE        : MAC WHITELIST\r\n");
    }
    else if (u1Wlc_Discovery_Mode == CLI_CAPWAP_DISC_MODE_MAC_BLACKLIST)
    {
        CliPrintf (CliHandle, "\r\nDISCOVERY MODE        : MAC BLACKLIST\r\n");
    }
    else
    {
        CliPrintf (CliHandle,
                   "\r\nDISCOVERY MODE        :  Not configured\r\n");
    }
#else
    CliPrintf (CliHandle, "\r\nCAPWAP                : NA\r\n");
    CliPrintf (CliHandle, "\r\nAP MODEL CHECK        : NA\r\n");
    CliPrintf (CliHandle, "\r\nDISCOVERY MODE        : NA\r\n");
#endif
    CliPrintf (CliHandle, "\r\nCONTROL UDP PORT      : %d\r\n",
               u4Control_UDP_Port);
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : CapwapShowWhiteList
* Description :
* Input       : NONE
* Output      :  None
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
CapwapShowWhiteList (tCliHandle CliHandle)
{

    tSNMP_OCTET_STRING_TYPE MacAddress;
    UINT1               au1String[20];
    UINT1               au1Temp[20];
    UINT4               u4NextWhiteListId = 0;
    UINT4               u4WhiteListId = 0;

    MEMSET (&MacAddress, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1String, 0, sizeof (au1String));
    MEMSET (au1Temp, 0, sizeof (au1Temp));

    MacAddress.pu1_OctetList = au1Temp;
    MacAddress.i4_Length = sizeof (au1Temp);
    CliPrintf (CliHandle, "\r\n ID    WhiteList Mac Addresses \r\n");
    if (nmhGetFirstIndexFsCapwapWhiteListTable (&u4NextWhiteListId) !=
        SNMP_FAILURE)
    {
        do
        {
            u4WhiteListId = u4NextWhiteListId;
            nmhGetFsCapwapWhiteListWtpBaseMac (u4WhiteListId, &MacAddress);
            CliMacToStr (MacAddress.pu1_OctetList, au1String);

            CliPrintf (CliHandle, "\r\n %d     %s \r\n", u4WhiteListId,
                       au1String);
        }
        while (nmhGetNextIndexFsCapwapWhiteListTable
               (u4WhiteListId, &u4NextWhiteListId) == SNMP_SUCCESS);
    }
    else
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : CapwapShowBlackList
* Description :
* Input       : NONE
* Output      :  None
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
CapwapShowBlackList (tCliHandle CliHandle)
{

    tSNMP_OCTET_STRING_TYPE MacAddress;
    UINT1               au1String[20];
    UINT1               au1Temp[20];
    UINT4               u4NextBlackListId = 0;
    UINT4               u4BlackListId = 0;

    MEMSET (&MacAddress, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1String, 0, sizeof (au1String));
    MEMSET (au1Temp, 0, sizeof (au1Temp));

    MacAddress.pu1_OctetList = au1Temp;
    MacAddress.i4_Length = sizeof (au1Temp);
    CliPrintf (CliHandle, "\r\n ID    BlackList Mac Addresses \r\n");

    if (nmhGetFirstIndexFsCapwapBlackList (&u4NextBlackListId) != SNMP_FAILURE)
    {
        do
        {
            u4BlackListId = u4NextBlackListId;
            nmhGetFsCapwapBlackListWtpBaseMac (u4BlackListId, &MacAddress);
            CliMacToStr (MacAddress.pu1_OctetList, au1String);

            CliPrintf (CliHandle, "\r\n %d     %s \r\n", u4BlackListId,
                       au1String);
        }
        while (nmhGetNextIndexFsCapwapBlackList
               (u4BlackListId, &u4NextBlackListId) == SNMP_SUCCESS);
    }
    else
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;

}

/****************************************************************************
* Function    :  CapwapCliSetCapwapBaseAcNameListTable
* Description :
* Input       :  CliHandle 
*            pCapwapSetCapwapBaseAcNameListEntry
*            pCapwapIsSetCapwapBaseAcNameListEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
CapwapCliSetCapwapBaseAcNameListTable (tCliHandle CliHandle,
                                       tCapwapCapwapBaseAcNameListEntry *
                                       pCapwapSetCapwapBaseAcNameListEntry,
                                       tCapwapIsSetCapwapBaseAcNameListEntry *
                                       pCapwapIsSetCapwapBaseAcNameListEntry)
{
    UINT4               u4ErrorCode;

    if (CapwapTestAllCapwapBaseAcNameListTable
        (&u4ErrorCode, pCapwapSetCapwapBaseAcNameListEntry,
         pCapwapIsSetCapwapBaseAcNameListEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (CapwapSetAllCapwapBaseAcNameListTable
        (pCapwapSetCapwapBaseAcNameListEntry,
         pCapwapIsSetCapwapBaseAcNameListEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  CapwapCliSetCapwapBaseMacAclTable
* Description :
* Input       :  CliHandle 
*            pCapwapSetCapwapBaseMacAclEntry
*            pCapwapIsSetCapwapBaseMacAclEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
CapwapCliSetCapwapBaseMacAclTable (tCliHandle CliHandle,
                                   tCapwapCapwapBaseMacAclEntry *
                                   pCapwapSetCapwapBaseMacAclEntry,
                                   tCapwapIsSetCapwapBaseMacAclEntry *
                                   pCapwapIsSetCapwapBaseMacAclEntry)
{
    UINT4               u4ErrorCode;

    if (CapwapTestAllCapwapBaseMacAclTable
        (&u4ErrorCode, pCapwapSetCapwapBaseMacAclEntry,
         pCapwapIsSetCapwapBaseMacAclEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (CapwapSetAllCapwapBaseMacAclTable
        (pCapwapSetCapwapBaseMacAclEntry, pCapwapIsSetCapwapBaseMacAclEntry,
         OSIX_TRUE, OSIX_TRUE) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  CapwapCliSetCapwapBaseWtpProfileTable
* Description :
* Input       :  CliHandle 
*            pCapwapSetCapwapBaseWtpProfileEntry
*            pCapwapIsSetCapwapBaseWtpProfileEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
CapwapCliSetCapwapBaseWtpProfileTable (tCliHandle CliHandle,
                                       tCapwapCapwapBaseWtpProfileEntry *
                                       pCapwapSetCapwapBaseWtpProfileEntry,
                                       tCapwapIsSetCapwapBaseWtpProfileEntry *
                                       pCapwapIsSetCapwapBaseWtpProfileEntry)
{
    UINT4               u4ErrorCode;
    UINT4               u4WtpProfileId = 0;

    if (pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
        i4CapwapBaseWtpProfileRowStatus == CLI_CAPWAP_RADIO_DELETE)
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
             au1CapwapBaseWtpProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\n Inconsistent Input \r\n");
            return OSIX_FAILURE;
        }

        if (pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
            u4CapwapBaseWtpProfileId != u4WtpProfileId)
        {
            CliPrintf (CliHandle, "\r\n  Inconsistent Input\r\n");
            return OSIX_FAILURE;
        }
    }

    if (pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
        i4CapwapBaseWtpProfileRowStatus == CLI_CAPWAP_RADIO_ENABLE)
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
             au1CapwapBaseWtpProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\n No profile present \r\n");
            return OSIX_FAILURE;
        }

        if (pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
            u4CapwapBaseWtpProfileId != u4WtpProfileId)
        {
            CliPrintf (CliHandle, "\r\n  No profile present \r\n");
            return OSIX_FAILURE;
        }
    }

    if (pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
        i4CapwapBaseWtpProfileRowStatus == CLI_CAPWAP_RADIO_DISABLE)
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
             au1CapwapBaseWtpProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\n No profile present \r\n");
            return OSIX_FAILURE;
        }

        if (pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
            u4CapwapBaseWtpProfileId != u4WtpProfileId)
        {
            CliPrintf (CliHandle, "\r\n  No profile present \r\n");
            return OSIX_FAILURE;
        }
    }

    if (CapwapTestAllCapwapBaseWtpProfileTable
        (&u4ErrorCode, pCapwapSetCapwapBaseWtpProfileEntry,
         pCapwapIsSetCapwapBaseWtpProfileEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (CapwapSetAllCapwapBaseWtpProfileTable
        (pCapwapSetCapwapBaseWtpProfileEntry,
         pCapwapIsSetCapwapBaseWtpProfileEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  CapwapCliSetCapwapBaseWtpSessionsLimit
* Description :
* Input       :  CliHandle 
*            pCapwapSetScalar
*            pCapwapIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
CapwapCliSetCapwapBaseWtpSessionsLimit (tCliHandle CliHandle,
                                        UINT4 *pCapwapBaseWtpSessionsLimit)
{
    UINT4               u4ErrorCode;
    UINT4               u4CapwapBaseWtpSessionsLimit = 0;

    CAPWAP_FILL_CAPWAPBASEWTPSESSIONSLIMIT (u4CapwapBaseWtpSessionsLimit,
                                            pCapwapBaseWtpSessionsLimit);

    if (CapwapTestCapwapBaseWtpSessionsLimit
        (&u4ErrorCode, u4CapwapBaseWtpSessionsLimit) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (CapwapSetCapwapBaseWtpSessionsLimit (u4CapwapBaseWtpSessionsLimit) !=
        OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  CapwapCliSetCapwapBaseStationSessionsLimit
* Description :
* Input       :  CliHandle 
*            pCapwapSetScalar
*            pCapwapIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
CapwapCliSetCapwapBaseStationSessionsLimit (tCliHandle CliHandle,
                                            UINT4
                                            *pCapwapBaseStationSessionsLimit)
{
    UINT4               u4ErrorCode;
    UINT4               u4CapwapBaseStationSessionsLimit = 0;

    CAPWAP_FILL_CAPWAPBASESTATIONSESSIONSLIMIT
        (u4CapwapBaseStationSessionsLimit, pCapwapBaseStationSessionsLimit);

    if (CapwapTestCapwapBaseStationSessionsLimit
        (&u4ErrorCode, u4CapwapBaseStationSessionsLimit) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (CapwapSetCapwapBaseStationSessionsLimit
        (u4CapwapBaseStationSessionsLimit) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/* newly added*/
/*******************************************************************************
* Function    : CapwapCliSetFsdtlsencrption
* Description : 
* Input       : CliHandle
*               
*Output       : NONE
*Returns     :  CLI_SUCCESS/CLI_FAILURE
*************************************************************************************/
INT4
CapwapCliSetFsDtlsEncrption (tCliHandle CliHandle, UINT4 *pFsDtlsEncrption)
{
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (pFsDtlsEncrption);
    return CLI_SUCCESS;
}

/* newly added*/
/*******************************************************************************
* Function    : CapwapCliSetFsDtlsEncAuthEncryptAlgorithm
* Description :
* Input       : CliHandle
*Output       : NONE
*Returns     :  CLI_SUCCESS/CLI_FAILURE
*************************************************************************************/
INT4
CapwapCliSetFsDtlsEncAuthEncryptAlgorithm (tCliHandle CliHandle, UINT4
                                           *pFsDtlsEncAuthEncryptAlgorithm)
{
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (pFsDtlsEncAuthEncryptAlgorithm);
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  CapwapCliSetCapwapBaseAcMaxRetransmit
* Description :
* Input       :  CliHandle 
*            pCapwapSetScalar
*            pCapwapIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
CapwapCliSetCapwapBaseAcMaxRetransmit (tCliHandle CliHandle,
                                       UINT4 *pCapwapBaseAcMaxRetransmit)
{
    UINT4               u4ErrorCode;
    UINT4               u4CapwapBaseAcMaxRetransmit = 0;

    CAPWAP_FILL_CAPWAPBASEACMAXRETRANSMIT (u4CapwapBaseAcMaxRetransmit,
                                           pCapwapBaseAcMaxRetransmit);

    if (CapwapTestCapwapBaseAcMaxRetransmit
        (&u4ErrorCode, u4CapwapBaseAcMaxRetransmit) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (CapwapSetCapwapBaseAcMaxRetransmit (u4CapwapBaseAcMaxRetransmit) !=
        OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  CapwapCliSetCapwapBaseAcChangeStatePendingTimer
* Description :
* Input       :  CliHandle 
*            pCapwapSetScalar
*            pCapwapIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
CapwapCliSetCapwapBaseAcChangeStatePendingTimer (tCliHandle CliHandle,
                                                 UINT4
                                                 *pCapwapBaseAcChangeStatePendingTimer)
{
    UINT4               u4ErrorCode;
    UINT4               u4CapwapBaseAcChangeStatePendingTimer = 0;

    CAPWAP_FILL_CAPWAPBASEACCHANGESTATEPENDINGTIMER
        (u4CapwapBaseAcChangeStatePendingTimer,
         pCapwapBaseAcChangeStatePendingTimer);

    if (CapwapTestCapwapBaseAcChangeStatePendingTimer
        (&u4ErrorCode, u4CapwapBaseAcChangeStatePendingTimer) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (CapwapSetCapwapBaseAcChangeStatePendingTimer
        (u4CapwapBaseAcChangeStatePendingTimer) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  CapwapCliSetCapwapBaseAcDataCheckTimer
* Description :
* Input       :  CliHandle 
*            pCapwapSetScalar
*            pCapwapIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
CapwapCliSetCapwapBaseAcDataCheckTimer (tCliHandle CliHandle,
                                        UINT4 *pCapwapBaseAcDataCheckTimer)
{
    UINT4               u4ErrorCode;
    UINT4               u4CapwapBaseAcDataCheckTimer = 0;

    CAPWAP_FILL_CAPWAPBASEACDATACHECKTIMER (u4CapwapBaseAcDataCheckTimer,
                                            pCapwapBaseAcDataCheckTimer);
    if (CapwapTestCapwapBaseAcDataCheckTimer
        (&u4ErrorCode, u4CapwapBaseAcDataCheckTimer) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (CapwapSetCapwapBaseAcDataCheckTimer (u4CapwapBaseAcDataCheckTimer) !=
        OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  CapwapCliSetCapwapBaseAcDTLSSessionDeleteTimer
* Description :
* Input       :  CliHandle 
*            pCapwapSetScalar
*            pCapwapIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
CapwapCliSetCapwapBaseAcDTLSSessionDeleteTimer (tCliHandle CliHandle,
                                                UINT4
                                                *pCapwapBaseAcDTLSSessionDeleteTimer)
{
    UINT4               u4ErrorCode;
    UINT4               u4CapwapBaseAcDTLSSessionDeleteTimer = 0;

    CAPWAP_FILL_CAPWAPBASEACDTLSSESSIONDELETETIMER
        (u4CapwapBaseAcDTLSSessionDeleteTimer,
         pCapwapBaseAcDTLSSessionDeleteTimer);

    if (CapwapTestCapwapBaseAcDTLSSessionDeleteTimer
        (&u4ErrorCode, u4CapwapBaseAcDTLSSessionDeleteTimer) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (CapwapSetCapwapBaseAcDTLSSessionDeleteTimer
        (u4CapwapBaseAcDTLSSessionDeleteTimer) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  CapwapCliSetCapwapBaseAcEchoInterval
* Description :
* Input       :  CliHandle 
*            pCapwapSetScalar
*            pCapwapIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
CapwapCliSetCapwapBaseAcEchoInterval (tCliHandle CliHandle,
                                      UINT4 *pCapwapBaseAcEchoInterval)
{
    UINT4               u4ErrorCode;
    UINT4               u4CapwapBaseAcEchoInterval = 0;

    CAPWAP_FILL_CAPWAPBASEACECHOINTERVAL (u4CapwapBaseAcEchoInterval,
                                          pCapwapBaseAcEchoInterval);

    if (CapwapTestCapwapBaseAcEchoInterval
        (&u4ErrorCode, u4CapwapBaseAcEchoInterval) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\nInvalid Inputs\r\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (CapwapSetCapwapBaseAcEchoInterval (u4CapwapBaseAcEchoInterval) !=
        OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\nDB Access failed\r\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  CapwapCliSetCapwapBaseAcRetransmitInterval
* Description :
* Input       :  CliHandle 
*            pCapwapSetScalar
*            pCapwapIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
CapwapCliSetCapwapBaseAcRetransmitInterval (tCliHandle CliHandle,
                                            UINT4
                                            *pCapwapBaseAcRetransmitInterval)
{
    UINT4               u4ErrorCode;
    UINT4               u4CapwapBaseAcRetransmitInterval = 0;

    CAPWAP_FILL_CAPWAPBASEACRETRANSMITINTERVAL
        (u4CapwapBaseAcRetransmitInterval, pCapwapBaseAcRetransmitInterval);

    if (CapwapTestCapwapBaseAcRetransmitInterval
        (&u4ErrorCode, u4CapwapBaseAcRetransmitInterval) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (CapwapSetCapwapBaseAcRetransmitInterval
        (u4CapwapBaseAcRetransmitInterval) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  CapwapCliSetCapwapBaseAcSilentInterval
* Description :
* Input       :  CliHandle 
*            pCapwapSetScalar
*            pCapwapIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
CapwapCliSetCapwapBaseAcSilentInterval (tCliHandle CliHandle,
                                        UINT4 *pCapwapBaseAcSilentInterval)
{
    UINT4               u4ErrorCode;
    UINT4               u4CapwapBaseAcSilentInterval = 0;

    CAPWAP_FILL_CAPWAPBASEACSILENTINTERVAL (u4CapwapBaseAcSilentInterval,
                                            pCapwapBaseAcSilentInterval);

    if (CapwapTestCapwapBaseAcSilentInterval
        (&u4ErrorCode, u4CapwapBaseAcSilentInterval) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (CapwapSetCapwapBaseAcSilentInterval (u4CapwapBaseAcSilentInterval) !=
        OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  CapwapCliSetCapwapBaseAcWaitDTLSTimer
* Description :
* Input       :  CliHandle 
*            pCapwapSetScalar
*            pCapwapIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
CapwapCliSetCapwapBaseAcWaitDTLSTimer (tCliHandle CliHandle,
                                       UINT4 *pCapwapBaseAcWaitDTLSTimer)
{
    UINT4               u4ErrorCode;
    UINT4               u4CapwapBaseAcWaitDTLSTimer = 0;

    CAPWAP_FILL_CAPWAPBASEACWAITDTLSTIMER (u4CapwapBaseAcWaitDTLSTimer,
                                           pCapwapBaseAcWaitDTLSTimer);

    if (CapwapTestCapwapBaseAcWaitDTLSTimer
        (&u4ErrorCode, u4CapwapBaseAcWaitDTLSTimer) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (CapwapSetCapwapBaseAcWaitDTLSTimer (u4CapwapBaseAcWaitDTLSTimer) !=
        OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  CapwapCliSetCapwapBaseAcWaitJoinTimer
* Description :
* Input       :  CliHandle 
*            pCapwapSetScalar
*            pCapwapIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
CapwapCliSetCapwapBaseAcWaitJoinTimer (tCliHandle CliHandle,
                                       UINT4 *pCapwapBaseAcWaitJoinTimer)
{
    UINT4               u4ErrorCode;
    UINT4               u4CapwapBaseAcWaitJoinTimer = 0;

    CAPWAP_FILL_CAPWAPBASEACWAITJOINTIMER (u4CapwapBaseAcWaitJoinTimer,
                                           pCapwapBaseAcWaitJoinTimer);

    if (CapwapTestCapwapBaseAcWaitJoinTimer
        (&u4ErrorCode, u4CapwapBaseAcWaitJoinTimer) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (CapwapSetCapwapBaseAcWaitJoinTimer (u4CapwapBaseAcWaitJoinTimer) !=
        OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  CapwapCliSetCapwapBaseAcEcnSupport
* Description :
* Input       :  CliHandle 
*            pCapwapSetScalar
*            pCapwapIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
CapwapCliSetCapwapBaseAcEcnSupport (tCliHandle CliHandle,
                                    UINT4 *pCapwapBaseAcEcnSupport)
{
    UINT4               u4ErrorCode;
    INT4                i4CapwapBaseAcEcnSupport = 0;

    CAPWAP_FILL_CAPWAPBASEACECNSUPPORT (i4CapwapBaseAcEcnSupport,
                                        pCapwapBaseAcEcnSupport);

    if (CapwapTestCapwapBaseAcEcnSupport
        (&u4ErrorCode, i4CapwapBaseAcEcnSupport) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (CapwapSetCapwapBaseAcEcnSupport (i4CapwapBaseAcEcnSupport) !=
        OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  CapwapCliSetCapwapBaseChannelUpDownNotifyEnable
* Description :
* Input       :  CliHandle 
*            pCapwapSetScalar
*            pCapwapIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
CapwapCliSetCapwapBaseChannelUpDownNotifyEnable (tCliHandle CliHandle,
                                                 UINT4
                                                 *pCapwapBaseChannelUpDownNotifyEnable)
{
    UINT4               u4ErrorCode;
    INT4                i4CapwapBaseChannelUpDownNotifyEnable = 0;

    CAPWAP_FILL_CAPWAPBASECHANNELUPDOWNNOTIFYENABLE
        (i4CapwapBaseChannelUpDownNotifyEnable,
         pCapwapBaseChannelUpDownNotifyEnable);

    if (CapwapTestCapwapBaseChannelUpDownNotifyEnable
        (&u4ErrorCode, i4CapwapBaseChannelUpDownNotifyEnable) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (CapwapSetCapwapBaseChannelUpDownNotifyEnable
        (i4CapwapBaseChannelUpDownNotifyEnable) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  CapwapCliSetCapwapBaseDecryptErrorNotifyEnable
* Description :
* Input       :  CliHandle 
*            pCapwapSetScalar
*            pCapwapIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
CapwapCliSetCapwapBaseDecryptErrorNotifyEnable (tCliHandle CliHandle,
                                                UINT4
                                                *pCapwapBaseDecryptErrorNotifyEnable)
{
    UINT4               u4ErrorCode;
    INT4                i4CapwapBaseDecryptErrorNotifyEnable = 0;

    CAPWAP_FILL_CAPWAPBASEDECRYPTERRORNOTIFYENABLE
        (i4CapwapBaseDecryptErrorNotifyEnable,
         pCapwapBaseDecryptErrorNotifyEnable);

    if (CapwapTestCapwapBaseDecryptErrorNotifyEnable
        (&u4ErrorCode, i4CapwapBaseDecryptErrorNotifyEnable) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (CapwapSetCapwapBaseDecryptErrorNotifyEnable
        (i4CapwapBaseDecryptErrorNotifyEnable) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  CapwapCliSetCapwapBaseJoinFailureNotifyEnable
* Description :
* Input       :  CliHandle 
*            pCapwapSetScalar
*            pCapwapIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
CapwapCliSetCapwapBaseJoinFailureNotifyEnable (tCliHandle CliHandle,
                                               UINT4
                                               *pCapwapBaseJoinFailureNotifyEnable)
{
    UINT4               u4ErrorCode;
    INT4                i4CapwapBaseJoinFailureNotifyEnable = 0;

    CAPWAP_FILL_CAPWAPBASEJOINFAILURENOTIFYENABLE
        (i4CapwapBaseJoinFailureNotifyEnable,
         pCapwapBaseJoinFailureNotifyEnable);

    if (CapwapTestCapwapBaseJoinFailureNotifyEnable
        (&u4ErrorCode, i4CapwapBaseJoinFailureNotifyEnable) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (CapwapSetCapwapBaseJoinFailureNotifyEnable
        (i4CapwapBaseJoinFailureNotifyEnable) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  CapwapCliSetCapwapBaseImageUpgradeFailureNotifyEnable
* Description :
* Input       :  CliHandle 
*            pCapwapSetScalar
*            pCapwapIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
CapwapCliSetCapwapBaseImageUpgradeFailureNotifyEnable (tCliHandle CliHandle,
                                                       UINT4
                                                       *pCapwapBaseImageUpgradeFailureNotifyEnable)
{
    UINT4               u4ErrorCode;
    INT4                i4CapwapBaseImageUpgradeFailureNotifyEnable = 0;

    CAPWAP_FILL_CAPWAPBASEIMAGEUPGRADEFAILURENOTIFYENABLE
        (i4CapwapBaseImageUpgradeFailureNotifyEnable,
         pCapwapBaseImageUpgradeFailureNotifyEnable);

    if (CapwapTestCapwapBaseImageUpgradeFailureNotifyEnable
        (&u4ErrorCode,
         i4CapwapBaseImageUpgradeFailureNotifyEnable) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (CapwapSetCapwapBaseImageUpgradeFailureNotifyEnable
        (i4CapwapBaseImageUpgradeFailureNotifyEnable) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  CapwapCliSetCapwapBaseConfigMsgErrorNotifyEnable
* Description :
* Input       :  CliHandle 
*            pCapwapSetScalar
*            pCapwapIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
CapwapCliSetCapwapBaseConfigMsgErrorNotifyEnable (tCliHandle CliHandle,
                                                  UINT4
                                                  *pCapwapBaseConfigMsgErrorNotifyEnable)
{
    UINT4               u4ErrorCode;
    INT4                i4CapwapBaseConfigMsgErrorNotifyEnable = 0;

    CAPWAP_FILL_CAPWAPBASECONFIGMSGERRORNOTIFYENABLE
        (i4CapwapBaseConfigMsgErrorNotifyEnable,
         pCapwapBaseConfigMsgErrorNotifyEnable);

    if (CapwapTestCapwapBaseConfigMsgErrorNotifyEnable
        (&u4ErrorCode, i4CapwapBaseConfigMsgErrorNotifyEnable) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (CapwapSetCapwapBaseConfigMsgErrorNotifyEnable
        (i4CapwapBaseConfigMsgErrorNotifyEnable) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  CapwapCliSetCapwapBaseRadioOperableStatusNotifyEnable
* Description :
* Input       :  CliHandle 
*            pCapwapSetScalar
*            pCapwapIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
CapwapCliSetCapwapBaseRadioOperableStatusNotifyEnable (tCliHandle CliHandle,
                                                       UINT4
                                                       *pCapwapBaseRadioOperableStatusNotifyEnable)
{
    UINT4               u4ErrorCode;
    INT4                i4CapwapBaseRadioOperableStatusNotifyEnable = 0;

    CAPWAP_FILL_CAPWAPBASERADIOOPERABLESTATUSNOTIFYENABLE
        (i4CapwapBaseRadioOperableStatusNotifyEnable,
         pCapwapBaseRadioOperableStatusNotifyEnable);

    if (CapwapTestCapwapBaseRadioOperableStatusNotifyEnable
        (&u4ErrorCode,
         i4CapwapBaseRadioOperableStatusNotifyEnable) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (CapwapSetCapwapBaseRadioOperableStatusNotifyEnable
        (i4CapwapBaseRadioOperableStatusNotifyEnable) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  CapwapCliSetCapwapBaseAuthenFailureNotifyEnable
* Description :
* Input       :  CliHandle 
*            pCapwapSetScalar
*            pCapwapIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
CapwapCliSetCapwapBaseAuthenFailureNotifyEnable (tCliHandle CliHandle,
                                                 UINT4
                                                 *pCapwapBaseAuthenFailureNotifyEnable)
{
    UINT4               u4ErrorCode;
    INT4                i4CapwapBaseAuthenFailureNotifyEnable = 0;

    CAPWAP_FILL_CAPWAPBASEAUTHENFAILURENOTIFYENABLE
        (i4CapwapBaseAuthenFailureNotifyEnable,
         pCapwapBaseAuthenFailureNotifyEnable);

    if (CapwapTestCapwapBaseAuthenFailureNotifyEnable
        (&u4ErrorCode, i4CapwapBaseAuthenFailureNotifyEnable) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (CapwapSetCapwapBaseAuthenFailureNotifyEnable
        (i4CapwapBaseAuthenFailureNotifyEnable) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  CapwapCliSetFsWtpModelTable
* Description :
* Input       :  CliHandle 
*            pCapwapSetFsWtpModelEntry
*            pCapwapIsSetFsWtpModelEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
CapwapCliSetFsWtpModelTable (tCliHandle CliHandle,
                             tCapwapFsWtpModelEntry * pCapwapSetFsWtpModelEntry,
                             tCapwapIsSetFsWtpModelEntry *
                             pCapwapIsSetFsWtpModelEntry)
{
    UINT4               u4ErrorCode;
    tSNMP_OCTET_STRING_TYPE ModelName;
    MEMSET (&ModelName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    if (CapwapTestAllFsWtpModelTable (&u4ErrorCode, pCapwapSetFsWtpModelEntry,
                                      pCapwapIsSetFsWtpModelEntry, OSIX_TRUE,
                                      OSIX_TRUE) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\nInvalid input data\r\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    if ((pCapwapIsSetFsWtpModelEntry->bFsNoOfRadio != OSIX_FALSE) ||
        (pCapwapIsSetFsWtpModelEntry->bFsCapwapWtpMacType != OSIX_FALSE) ||
        (pCapwapIsSetFsWtpModelEntry->bFsCapwapWtpTunnelMode != OSIX_FALSE) ||
        (pCapwapIsSetFsWtpModelEntry->bFsCapwapImageName != OSIX_FALSE) ||
        (pCapwapIsSetFsWtpModelEntry->bFsMaxStations != OSIX_FALSE))
    {
        ModelName.i4_Length =
            pCapwapSetFsWtpModelEntry->MibObject.i4FsCapwapWtpModelNumberLen;
        ModelName.pu1_OctetList =
            pCapwapSetFsWtpModelEntry->MibObject.au1FsCapwapWtpModelNumber;
        if (nmhSetFsWtpModelRowStatus (&ModelName, NOT_IN_SERVICE) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n Row Status Updation to Not-In-Service failed \r\n");
            CLI_FATAL_ERROR (CliHandle);
            return OSIX_FAILURE;
        }

    }

    if (CapwapSetAllFsWtpModelTable
        (pCapwapSetFsWtpModelEntry, pCapwapIsSetFsWtpModelEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\nDB Updation Failed\r\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    if ((pCapwapIsSetFsWtpModelEntry->bFsNoOfRadio != OSIX_FALSE) ||
        (pCapwapIsSetFsWtpModelEntry->bFsCapwapWtpMacType != OSIX_FALSE) ||
        (pCapwapIsSetFsWtpModelEntry->bFsCapwapWtpTunnelMode != OSIX_FALSE) ||
        (pCapwapIsSetFsWtpModelEntry->bFsCapwapImageName != OSIX_FALSE) ||
        (pCapwapIsSetFsWtpModelEntry->bFsMaxStations != OSIX_FALSE))
    {
        if (nmhSetFsWtpModelRowStatus (&ModelName, ACTIVE) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n Row Status Updation to ACTIVE failed \r\n");
            CLI_FATAL_ERROR (CliHandle);
            return OSIX_FAILURE;
        }

    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  CapwapCliSetFsWtpRadioTable
* Description :
* Input       :  CliHandle 
*            pCapwapSetFsWtpRadioEntry
*            pCapwapIsSetFsWtpRadioEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
CapwapCliSetFsWtpRadioTable (tCliHandle CliHandle,
                             tCapwapFsWtpRadioEntry * pCapwapSetFsWtpRadioEntry,
                             tCapwapIsSetFsWtpRadioEntry *
                             pCapwapIsSetFsWtpRadioEntry)
{
    UINT4               u4ErrorCode;

    if (CapwapTestAllFsWtpRadioTable (&u4ErrorCode, pCapwapSetFsWtpRadioEntry,
                                      pCapwapIsSetFsWtpRadioEntry, OSIX_TRUE,
                                      OSIX_TRUE) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (CapwapSetAllFsWtpRadioTable
        (pCapwapSetFsWtpRadioEntry, pCapwapIsSetFsWtpRadioEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  CapwapCliSetFsCapwapWhiteListTable
* Description :
* Input       :  CliHandle 
*            pCapwapSetFsCapwapWhiteListEntry
*            pCapwapIsSetFsCapwapWhiteListEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
CapwapCliSetFsCapwapWhiteListTable (tCliHandle CliHandle,
                                    tCapwapFsCapwapWhiteListEntry *
                                    pCapwapSetFsCapwapWhiteListEntry,
                                    tCapwapIsSetFsCapwapWhiteListEntry *
                                    pCapwapIsSetFsCapwapWhiteListEntry)
{
    UINT4               u4ErrorCode;

    if (CapwapTestAllFsCapwapWhiteListTable
        (&u4ErrorCode, pCapwapSetFsCapwapWhiteListEntry,
         pCapwapIsSetFsCapwapWhiteListEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\nInvalid Inputs\r\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (CapwapSetAllFsCapwapWhiteListTable
        (pCapwapSetFsCapwapWhiteListEntry, pCapwapIsSetFsCapwapWhiteListEntry,
         OSIX_TRUE, OSIX_TRUE) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\nDB Updation failed\r\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  CapwapCliSetFsCapwapBlackList
* Description :
* Input       :  CliHandle 
*            pCapwapSetFsCapwapBlackListEntry
*            pCapwapIsSetFsCapwapBlackListEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
CapwapCliSetFsCapwapBlackList (tCliHandle CliHandle,
                               tCapwapFsCapwapBlackListEntry *
                               pCapwapSetFsCapwapBlackListEntry,
                               tCapwapIsSetFsCapwapBlackListEntry *
                               pCapwapIsSetFsCapwapBlackListEntry)
{
    UINT4               u4ErrorCode;

    if (CapwapTestAllFsCapwapBlackList
        (&u4ErrorCode, pCapwapSetFsCapwapBlackListEntry,
         pCapwapIsSetFsCapwapBlackListEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\nInvalid Inputs\r\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (CapwapSetAllFsCapwapBlackList
        (pCapwapSetFsCapwapBlackListEntry, pCapwapIsSetFsCapwapBlackListEntry,
         OSIX_TRUE, OSIX_TRUE) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\nDB Updation failed\r\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  CapwapCliSetFsCapwapWtpConfigTable
* Description :
* Input       :  CliHandle 
*            pCapwapSetFsCapwapWtpConfigEntry
*            pCapwapIsSetFsCapwapWtpConfigEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
CapwapCliSetFsCapwapWtpConfigTable (tCliHandle CliHandle,
                                    tCapwapFsCapwapWtpConfigEntry *
                                    pCapwapSetFsCapwapWtpConfigEntry,
                                    tCapwapIsSetFsCapwapWtpConfigEntry *
                                    pCapwapIsSetFsCapwapWtpConfigEntry)
{
    UINT4               u4ErrorCode;

    if (CapwapTestAllFsCapwapWtpConfigTable
        (&u4ErrorCode, pCapwapSetFsCapwapWtpConfigEntry,
         pCapwapIsSetFsCapwapWtpConfigEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (CapwapSetAllFsCapwapWtpConfigTable
        (pCapwapSetFsCapwapWtpConfigEntry, pCapwapIsSetFsCapwapWtpConfigEntry,
         OSIX_TRUE, OSIX_TRUE) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  CapwapCliSetFsCapwapLinkEncryptionTable
* Description :
* Input       :  CliHandle 
*            pCapwapSetFsCapwapLinkEncryptionEntry
*            pCapwapIsSetFsCapwapLinkEncryptionEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
CapwapCliSetFsCapwapLinkEncryptionTable (tCliHandle CliHandle,
                                         tCapwapFsCapwapLinkEncryptionEntry *
                                         pCapwapSetFsCapwapLinkEncryptionEntry,
                                         tCapwapIsSetFsCapwapLinkEncryptionEntry
                                         *
                                         pCapwapIsSetFsCapwapLinkEncryptionEntry)
{
    UINT4               u4ErrorCode;

    if (CapwapTestAllFsCapwapLinkEncryptionTable
        (&u4ErrorCode, pCapwapSetFsCapwapLinkEncryptionEntry,
         pCapwapIsSetFsCapwapLinkEncryptionEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (CapwapSetAllFsCapwapLinkEncryptionTable
        (pCapwapSetFsCapwapLinkEncryptionEntry,
         pCapwapIsSetFsCapwapLinkEncryptionEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  CapwapCliSetFsCawapDefaultWtpProfileTable
* Description :
* Input       :  CliHandle 
*            pCapwapSetFsCapwapDefaultWtpProfileEntry
*            pCapwapIsSetFsCapwapDefaultWtpProfileEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
CapwapCliSetFsCawapDefaultWtpProfileTable (tCliHandle CliHandle,
                                           tCapwapFsCapwapDefaultWtpProfileEntry
                                           *
                                           pCapwapSetFsCapwapDefaultWtpProfileEntry,
                                           tCapwapIsSetFsCapwapDefaultWtpProfileEntry
                                           *
                                           pCapwapIsSetFsCapwapDefaultWtpProfileEntry)
{
    UINT4               u4ErrorCode;

    if (CapwapTestAllFsCawapDefaultWtpProfileTable
        (&u4ErrorCode, pCapwapSetFsCapwapDefaultWtpProfileEntry,
         pCapwapIsSetFsCapwapDefaultWtpProfileEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (CapwapSetAllFsCawapDefaultWtpProfileTable
        (pCapwapSetFsCapwapDefaultWtpProfileEntry,
         pCapwapIsSetFsCapwapDefaultWtpProfileEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  CapwapCliSetFsCapwapDnsProfileTable
* Description :
* Input       :  CliHandle 
*            pCapwapSetFsCapwapDnsProfileEntry
*            pCapwapIsSetFsCapwapDnsProfileEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
CapwapCliSetFsCapwapDnsProfileTable (tCliHandle CliHandle,
                                     tCapwapFsCapwapDnsProfileEntry *
                                     pCapwapSetFsCapwapDnsProfileEntry,
                                     tCapwapIsSetFsCapwapDnsProfileEntry *
                                     pCapwapIsSetFsCapwapDnsProfileEntry)
{
    UINT4               u4ErrorCode;

    if (CapwapTestAllFsCapwapDnsProfileTable
        (&u4ErrorCode, pCapwapSetFsCapwapDnsProfileEntry,
         pCapwapIsSetFsCapwapDnsProfileEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (CapwapSetAllFsCapwapDnsProfileTable
        (pCapwapSetFsCapwapDnsProfileEntry, pCapwapIsSetFsCapwapDnsProfileEntry,
         OSIX_TRUE, OSIX_TRUE) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  CapwapCliSetFsWtpNativeVlanIdTable
* Description :
* Input       :  CliHandle 
*            pCapwapSetFsWtpNativeVlanIdTable
*            pCapwapIsSetFsWtpNativeVlanIdTable
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
CapwapCliSetFsWtpNativeVlanIdTable (tCliHandle CliHandle,
                                    tCapwapFsWtpNativeVlanIdTable *
                                    pCapwapSetFsWtpNativeVlanIdTable,
                                    tCapwapIsSetFsWtpNativeVlanIdTable *
                                    pCapwapIsSetFsWtpNativeVlanIdTable)
{
    UINT4               u4ErrorCode;

    if (CapwapTestAllFsWtpNativeVlanIdTable
        (&u4ErrorCode, pCapwapSetFsWtpNativeVlanIdTable,
         pCapwapIsSetFsWtpNativeVlanIdTable, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\nInvalid Inputs\r\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    if (CapwapSetAllFsWtpNativeVlanIdTable
        (pCapwapSetFsWtpNativeVlanIdTable, pCapwapIsSetFsWtpNativeVlanIdTable,
         OSIX_TRUE, OSIX_TRUE) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\nDB Updation Failed\r\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  CapwapCliSetFsWtpLocalRoutingTable
* Description :
* Input       :  CliHandle 
*            pCapwapSetFsWtpLocalRoutingTable
*            pCapwapIsSetFsWtpLocalRoutingTable
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
CapwapCliSetFsWtpLocalRoutingTable (tCliHandle CliHandle,
                                    tCapwapFsWtpLocalRoutingTable *
                                    pCapwapSetFsWtpLocalRoutingTable,
                                    tCapwapIsSetFsWtpLocalRoutingTable *
                                    pCapwapIsSetFsWtpLocalRoutingTable)
{
    UINT4               u4ErrorCode;

    if (CapwapTestAllFsWtpLocalRoutingTable
        (&u4ErrorCode, pCapwapSetFsWtpLocalRoutingTable,
         pCapwapIsSetFsWtpLocalRoutingTable, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\nInvalid Inputs\r\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    if (CapwapSetAllFsWtpLocalRoutingTable
        (pCapwapSetFsWtpLocalRoutingTable, pCapwapIsSetFsWtpLocalRoutingTable,
         OSIX_TRUE, OSIX_TRUE) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\nDB Updation Failed\r\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  CapwapCliSetFsCawapDiscStatsTable
* Description :
* Input       :  CliHandle 
*            pCapwapSetFsCawapDiscStatsEntry
*            pCapwapIsSetFsCawapDiscStatsEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
CapwapCliSetFsCawapDiscStatsTable (tCliHandle CliHandle,
                                   tCapwapFsCawapDiscStatsEntry *
                                   pCapwapSetFsCawapDiscStatsEntry,
                                   tCapwapIsSetFsCawapDiscStatsEntry *
                                   pCapwapIsSetFsCawapDiscStatsEntry)
{
    UINT4               u4ErrorCode;

    if (CapwapTestAllFsCawapDiscStatsTable
        (&u4ErrorCode, pCapwapSetFsCawapDiscStatsEntry,
         pCapwapIsSetFsCawapDiscStatsEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (CapwapSetAllFsCawapDiscStatsTable
        (pCapwapSetFsCawapDiscStatsEntry, pCapwapIsSetFsCawapDiscStatsEntry,
         OSIX_TRUE, OSIX_TRUE) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  CapwapCliSetFsCawapJoinStatsTable
* Description :
* Input       :  CliHandle 
*            pCapwapSetFsCawapJoinStatsEntry
*            pCapwapIsSetFsCawapJoinStatsEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
CapwapCliSetFsCawapJoinStatsTable (tCliHandle CliHandle,
                                   tCapwapFsCawapJoinStatsEntry *
                                   pCapwapSetFsCawapJoinStatsEntry,
                                   tCapwapIsSetFsCawapJoinStatsEntry *
                                   pCapwapIsSetFsCawapJoinStatsEntry)
{
    UINT4               u4ErrorCode;

    if (CapwapTestAllFsCawapJoinStatsTable
        (&u4ErrorCode, pCapwapSetFsCawapJoinStatsEntry,
         pCapwapIsSetFsCawapJoinStatsEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (CapwapSetAllFsCawapJoinStatsTable
        (pCapwapSetFsCawapJoinStatsEntry, pCapwapIsSetFsCawapJoinStatsEntry,
         OSIX_TRUE, OSIX_TRUE) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  CapwapCliSetFsCawapConfigStatsTable
* Description :
* Input       :  CliHandle 
*            pCapwapSetFsCawapConfigStatsEntry
*            pCapwapIsSetFsCawapConfigStatsEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
CapwapCliSetFsCawapConfigStatsTable (tCliHandle CliHandle,
                                     tCapwapFsCawapConfigStatsEntry *
                                     pCapwapSetFsCawapConfigStatsEntry,
                                     tCapwapIsSetFsCawapConfigStatsEntry *
                                     pCapwapIsSetFsCawapConfigStatsEntry)
{
    UINT4               u4ErrorCode;

    if (CapwapTestAllFsCawapConfigStatsTable
        (&u4ErrorCode, pCapwapSetFsCawapConfigStatsEntry,
         pCapwapIsSetFsCawapConfigStatsEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (CapwapSetAllFsCawapConfigStatsTable
        (pCapwapSetFsCawapConfigStatsEntry, pCapwapIsSetFsCawapConfigStatsEntry,
         OSIX_TRUE, OSIX_TRUE) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  CapwapCliSetFsCawapRunStatsTable
* Description :
* Input       :  CliHandle 
*            pCapwapSetFsCawapRunStatsEntry
*            pCapwapIsSetFsCawapRunStatsEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
CapwapCliSetFsCawapRunStatsTable (tCliHandle CliHandle,
                                  tCapwapFsCawapRunStatsEntry *
                                  pCapwapSetFsCawapRunStatsEntry,
                                  tCapwapIsSetFsCawapRunStatsEntry *
                                  pCapwapIsSetFsCawapRunStatsEntry)
{
    UINT4               u4ErrorCode;

    if (CapwapTestAllFsCawapRunStatsTable
        (&u4ErrorCode, pCapwapSetFsCawapRunStatsEntry,
         pCapwapIsSetFsCawapRunStatsEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (CapwapSetAllFsCawapRunStatsTable
        (pCapwapSetFsCawapRunStatsEntry, pCapwapIsSetFsCawapRunStatsEntry,
         OSIX_TRUE, OSIX_TRUE) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  CapwapCliSetFsCapwapWirelessBindingTable
* Description :
* Input       :  CliHandle 
*            pCapwapSetFsCapwapWirelessBindingEntry
*            pCapwapIsSetFsCapwapWirelessBindingEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
CapwapCliSetFsCapwapWirelessBindingTable (tCliHandle CliHandle,
                                          tCapwapFsCapwapWirelessBindingEntry *
                                          pCapwapSetFsCapwapWirelessBindingEntry,
                                          tCapwapIsSetFsCapwapWirelessBindingEntry
                                          *
                                          pCapwapIsSetFsCapwapWirelessBindingEntry)
{
    UINT4               u4ErrorCode;

    if (CapwapTestAllFsCapwapWirelessBindingTable
        (&u4ErrorCode, pCapwapSetFsCapwapWirelessBindingEntry,
         pCapwapIsSetFsCapwapWirelessBindingEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (CapwapSetAllFsCapwapWirelessBindingTable
        (pCapwapSetFsCapwapWirelessBindingEntry,
         pCapwapIsSetFsCapwapWirelessBindingEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  CapwapCliSetFsCapwapStationWhiteList
* Description :
* Input       :  CliHandle 
*            pCapwapSetFsCapwapStationWhiteListEntry
*            pCapwapIsSetFsCapwapStationWhiteListEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
CapwapCliSetFsCapwapStationWhiteList (tCliHandle CliHandle,
                                      tCapwapFsCapwapStationWhiteListEntry *
                                      pCapwapSetFsCapwapStationWhiteListEntry,
                                      tCapwapIsSetFsCapwapStationWhiteListEntry
                                      *
                                      pCapwapIsSetFsCapwapStationWhiteListEntry)
{
    UINT4               u4ErrorCode;

    if (CapwapTestAllFsCapwapStationWhiteList
        (&u4ErrorCode, pCapwapSetFsCapwapStationWhiteListEntry,
         pCapwapIsSetFsCapwapStationWhiteListEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (CapwapSetAllFsCapwapStationWhiteList
        (pCapwapSetFsCapwapStationWhiteListEntry,
         pCapwapIsSetFsCapwapStationWhiteListEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  CapwapCliSetFsCapwapWtpRebootStatisticsTable
* Description :
* Input       :  CliHandle 
*            pCapwapSetFsCapwapWtpRebootStatisticsEntry
*            pCapwapIsSetFsCapwapWtpRebootStatisticsEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
CapwapCliSetFsCapwapWtpRebootStatisticsTable (tCliHandle CliHandle,
                                              tCapwapFsCapwapWtpRebootStatisticsEntry
                                              *
                                              pCapwapSetFsCapwapWtpRebootStatisticsEntry,
                                              tCapwapIsSetFsCapwapWtpRebootStatisticsEntry
                                              *
                                              pCapwapIsSetFsCapwapWtpRebootStatisticsEntry)
{
    UINT4               u4ErrorCode;

    if (CapwapTestAllFsCapwapWtpRebootStatisticsTable
        (&u4ErrorCode, pCapwapSetFsCapwapWtpRebootStatisticsEntry,
         pCapwapIsSetFsCapwapWtpRebootStatisticsEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (CapwapSetAllFsCapwapWtpRebootStatisticsTable
        (pCapwapSetFsCapwapWtpRebootStatisticsEntry,
         pCapwapIsSetFsCapwapWtpRebootStatisticsEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  CapwapCliSetFsCapwapWtpRadioStatisticsTable
* Description :
* Input       :  CliHandle 
*            pCapwapSetFsCapwapWtpRadioStatisticsEntry
*            pCapwapIsSetFsCapwapWtpRadioStatisticsEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
CapwapCliSetFsCapwapWtpRadioStatisticsTable (tCliHandle CliHandle,
                                             tCapwapFsCapwapWtpRadioStatisticsEntry
                                             *
                                             pCapwapSetFsCapwapWtpRadioStatisticsEntry,
                                             tCapwapIsSetFsCapwapWtpRadioStatisticsEntry
                                             *
                                             pCapwapIsSetFsCapwapWtpRadioStatisticsEntry)
{
    UINT4               u4ErrorCode;

    if (CapwapTestAllFsCapwapWtpRadioStatisticsTable
        (&u4ErrorCode, pCapwapSetFsCapwapWtpRadioStatisticsEntry,
         pCapwapIsSetFsCapwapWtpRadioStatisticsEntry) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (CapwapSetAllFsCapwapWtpRadioStatisticsTable
        (pCapwapSetFsCapwapWtpRadioStatisticsEntry,
         pCapwapIsSetFsCapwapWtpRadioStatisticsEntry) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  CapwapCliSetFsCapwapEnable
* Description :
* Input       :  CliHandle 
*            pCapwapSetScalar
*            pCapwapIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
CapwapCliSetFsCapwapEnable (tCliHandle CliHandle, UINT4 *pFsCapwapEnable)
{
    UINT4               u4ErrorCode;
    INT4                i4FsCapwapEnable = 0;

    CAPWAP_FILL_FSCAPWAPENABLE (i4FsCapwapEnable, pFsCapwapEnable);

    if (CapwapTestFsCapwapEnable (&u4ErrorCode, i4FsCapwapEnable) !=
        OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (CapwapSetFsCapwapEnable (i4FsCapwapEnable) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  CapwapCliSetFsCapwapShutdown
* Description :
* Input       :  CliHandle 
*            pCapwapSetScalar
*            pCapwapIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
CapwapCliSetFsCapwapShutdown (tCliHandle CliHandle, UINT4 *pFsCapwapShutdown)
{
    UINT4               u4ErrorCode;
    INT4                i4FsCapwapShutdown = 0;

    CAPWAP_FILL_FSCAPWAPSHUTDOWN (i4FsCapwapShutdown, pFsCapwapShutdown);

    if (CapwapTestFsCapwapShutdown (&u4ErrorCode, i4FsCapwapShutdown) !=
        OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (CapwapSetFsCapwapShutdown (i4FsCapwapShutdown) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  CapwapCliSetFsCapwapControlUdpPort
* Description :
* Input       :  CliHandle 
*            pCapwapSetScalar
*            pCapwapIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
CapwapCliSetFsCapwapControlUdpPort (tCliHandle CliHandle,
                                    UINT4 *pFsCapwapControlUdpPort)
{
    UINT4               u4ErrorCode;
    UINT4               u4FsCapwapControlUdpPort = 0;

    CAPWAP_FILL_FSCAPWAPCONTROLUDPPORT (u4FsCapwapControlUdpPort,
                                        pFsCapwapControlUdpPort);

    if (CapwapTestFsCapwapControlUdpPort
        (&u4ErrorCode, u4FsCapwapControlUdpPort) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (CapwapSetFsCapwapControlUdpPort (u4FsCapwapControlUdpPort) !=
        OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  CapwapCliSetFsCapwapControlChannelDTLSPolicyOptions
* Description :
* Input       :  CliHandle 
*            pCapwapSetScalar
*            pCapwapIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
CapwapCliSetFsCapwapControlChannelDTLSPolicyOptions (tCliHandle CliHandle,
                                                     UINT4
                                                     *pFsCapwapControlChannelDTLSPolicyOptions)
{
    UINT4               u4ErrorCode;

    if (CapwapTestFsCapwapControlChannelDTLSPolicyOptions
        (&u4ErrorCode,
         (UINT1 *) pFsCapwapControlChannelDTLSPolicyOptions) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (CapwapSetFsCapwapControlChannelDTLSPolicyOptions
        ((UINT1 *) pFsCapwapControlChannelDTLSPolicyOptions) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  CapwapCliSetFsCapwapDataChannelDTLSPolicyOptions
* Description :
* Input       :  CliHandle 
*            pCapwapSetScalar
*            pCapwapIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
CapwapCliSetFsCapwapDataChannelDTLSPolicyOptions (tCliHandle CliHandle,
                                                  UINT4
                                                  *pFsCapwapDataChannelDTLSPolicyOptions)
{
    UINT4               u4ErrorCode;

    if (CapwapTestFsCapwapDataChannelDTLSPolicyOptions
        (&u4ErrorCode,
         (UINT1 *) pFsCapwapDataChannelDTLSPolicyOptions) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (CapwapSetFsCapwapDataChannelDTLSPolicyOptions
        ((UINT1 *) pFsCapwapDataChannelDTLSPolicyOptions) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  CapwapCliSetFsWlcDiscoveryMode
* Description :
* Input       :  CliHandle 
*            pCapwapSetScalar
*            pCapwapIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
CapwapCliSetFsWlcDiscoveryMode (tCliHandle CliHandle,
                                UINT1 *pFsWlcDiscoveryMode)
{
    UINT4               u4ErrorCode;

    if (CapwapTestFsWlcDiscoveryMode (&u4ErrorCode, pFsWlcDiscoveryMode) !=
        OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\nInvalid Inputs\r\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (CapwapSetFsWlcDiscoveryMode (pFsWlcDiscoveryMode) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\nDB Updation Failed\r\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  CapwapCliSetFsCapwapWtpModeIgnore
* Description :
* Input       :  CliHandle 
*            pCapwapSetScalar
*            pCapwapIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
CapwapCliSetFsCapwapWtpModeIgnore (tCliHandle CliHandle,
                                   UINT4 *pFsCapwapWtpModeIgnore)
{
    UINT4               u4ErrorCode;
    INT4                i4FsCapwapWtpModeIgnore = 0;

    CAPWAP_FILL_FSCAPWAPWTPMODEIGNORE (i4FsCapwapWtpModeIgnore,
                                       pFsCapwapWtpModeIgnore);

    if (CapwapTestFsCapwapWtpModeIgnore (&u4ErrorCode, i4FsCapwapWtpModeIgnore)
        != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (CapwapSetFsCapwapWtpModeIgnore (i4FsCapwapWtpModeIgnore) !=
        OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  CapwapCliSetFsCapwapDebugMask
* Description :
* Input       :  CliHandle 
*            pCapwapSetScalar
*            pCapwapIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
CapwapCliSetFsCapwapDebugMask (tCliHandle CliHandle, UINT4 *pFsCapwapDebugMask,
                               UINT4 *pFsWebauthMask, UINT4 *pFsHttpMask)
{
    UINT4               u4ErrorCode;
    INT4                i4FsCapwapDebugMask = 0;
    INT4                i4OldFsCapwapDebugMask = 0;
    CAPWAP_FILL_FSCAPWAPDEBUGMASK (i4FsCapwapDebugMask, pFsCapwapDebugMask);
    if (CapwapTestFsCapwapDebugMask (&u4ErrorCode, i4FsCapwapDebugMask) !=
        OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        CliPrintf (CliHandle, "\r\n Invalid Inputs\r\n");
        return CLI_FAILURE;
    }
    if (CapwapGetFsCapwapDebugMask (&i4OldFsCapwapDebugMask) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }
    i4FsCapwapDebugMask = i4FsCapwapDebugMask | i4OldFsCapwapDebugMask;

    if (CapwapSetFsCapwapDebugMask (i4FsCapwapDebugMask) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        CliPrintf (CliHandle, "\r\n Unable to Update DB\r\n");
        return CLI_FAILURE;
    }

#ifdef WLC_WANTED
#if 0
    if (pFsWebauthMask != NULL)
    {
        if (*pFsWebauthMask != 0)
        {
            gu4WssStaWebAuthDebugMask = *pFsWebauthMask;
        }
    }
    if (pFsWebauthMask != NULL)
    {
        if (*pFsWebauthMask != 0)
        {
            gu4HttpMask = *pFsHttpMask;
        }
    }
#endif
    UNUSED_PARAM (pFsWebauthMask);
    UNUSED_PARAM (pFsHttpMask);
#else
    UNUSED_PARAM (pFsWebauthMask);
    UNUSED_PARAM (pFsHttpMask);
#endif /* WLC_WANTED */
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  CapwapCliSetFsCapwapNoDebugMask
* Description :
* Input       :  CliHandle 
*            pCapwapSetScalar
*            pCapwapIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
CapwapCliSetFsCapwapNoDebugMask (tCliHandle CliHandle,
                                 UINT4 *pFsCapwapNoDebugMask,
                                 UINT4 *pFsWebauthMask, UINT4 *pFsHttpMask)
{
    UINT4               u4ErrorCode;
    INT4                i4FsCapwapNoDebugMask = 0;
    INT4                i4OldFsCapwapDebugMask = 0;
    CAPWAP_FILL_FSCAPWAPDEBUGMASK (i4FsCapwapNoDebugMask, pFsCapwapNoDebugMask);
    if (CapwapTestFsCapwapDebugMask (&u4ErrorCode, i4FsCapwapNoDebugMask) !=
        OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        CliPrintf (CliHandle, "\r\n Invalid Inputs\r\n");
        return CLI_FAILURE;
    }

    if (CapwapGetFsCapwapDebugMask (&i4OldFsCapwapDebugMask) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }
    i4FsCapwapNoDebugMask = ~i4FsCapwapNoDebugMask;
    i4FsCapwapNoDebugMask = i4FsCapwapNoDebugMask & i4OldFsCapwapDebugMask;

    if (CapwapSetFsCapwapDebugMask (i4FsCapwapNoDebugMask) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        CliPrintf (CliHandle, "\r\n Unable to Update DB\r\n");
        return CLI_FAILURE;
    }
#ifdef WLC_WANTED
#if 0
    if (pFsWebauthMask != NULL)
    {
        gu4WssStaWebAuthDebugMask = *pFsWebauthMask;
    }
    if (pFsHttpMask != NULL)
    {
        if (pFsWebauthMask != NULL)
        {
            gu4HttpMask = *pFsWebauthMask;
        }
    }
#endif
    UNUSED_PARAM (pFsWebauthMask);
    UNUSED_PARAM (pFsHttpMask);
#else
    UNUSED_PARAM (pFsWebauthMask);
    UNUSED_PARAM (pFsHttpMask);
#endif /* WLC_WANTED */
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  CapwapCliSetFsCapwapWssDataDebugMask
* Description :
* Input       :  CliHandle 
*            pCapwapSetScalar
*            pCapwapIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
CapwapCliSetFsCapwapWssDataDebugMask (tCliHandle CliHandle,
                                      UINT4 *pFsCapwapWssDataDebugMask)
{
    UINT4               u4ErrorCode;
    INT4                i4FsCapwapWssDataDebugMask = 0;
    INT4                i4OldFsCapwapWssDataDebugMask = 0;

    CAPWAP_FILL_FSCAPWAPWSSDATA (i4FsCapwapWssDataDebugMask,
                                 pFsCapwapWssDataDebugMask);
    if (nmhTestv2FsCapwapWssDataDebugMask
        (&u4ErrorCode, i4FsCapwapWssDataDebugMask) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        CliPrintf (CliHandle, "\r\n Invalid Inputs\r\n");
        return CLI_FAILURE;
    }
    if (nmhGetFsCapwapWssDataDebugMask (&i4OldFsCapwapWssDataDebugMask) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    i4FsCapwapWssDataDebugMask =
        i4FsCapwapWssDataDebugMask | i4OldFsCapwapWssDataDebugMask;

    if (nmhSetFsCapwapWssDataDebugMask (i4FsCapwapWssDataDebugMask) !=
        SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        CliPrintf (CliHandle, "\r\n Unable to Update DB\r\n");
        return CLI_FAILURE;
    }
#ifdef KERNEL_CAPWAP_WANTED
#ifdef WLC_WANTED
    if (CapwapUpdateKernelDebugInfo () == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapUpdateKernelDebuginfo: Kernel updation failed\r\n");
    }
#endif
#endif
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  CapwapCliSetFsCapwapNoWssDataDebugMask
* Description :
* Input       :  CliHandle 
*            pCapwapSetScalar
*            pCapwapIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
CapwapCliSetFsCapwapNoWssDataDebugMask (tCliHandle CliHandle,
                                        UINT4 *pFsCapwapNoWssDataDebugMask)
{
    UINT4               u4ErrorCode;
    INT4                i4FsCapwapNoWssDataDebugMask = 0;
    INT4                i4OldFsCapwapWssDataDebugMask = 0;
    CAPWAP_FILL_FSCAPWAPWSSDATA (i4FsCapwapNoWssDataDebugMask,
                                 pFsCapwapNoWssDataDebugMask);
    if (nmhTestv2FsCapwapWssDataDebugMask
        (&u4ErrorCode, i4FsCapwapNoWssDataDebugMask) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        CliPrintf (CliHandle, "\r\n Invalid Inputs\r\n");
        return CLI_FAILURE;
    }

    if (nmhGetFsCapwapWssDataDebugMask (&i4OldFsCapwapWssDataDebugMask) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    i4FsCapwapNoWssDataDebugMask = ~i4FsCapwapNoWssDataDebugMask;
    i4FsCapwapNoWssDataDebugMask =
        i4FsCapwapNoWssDataDebugMask & i4OldFsCapwapWssDataDebugMask;

    if (nmhSetFsCapwapWssDataDebugMask (i4FsCapwapNoWssDataDebugMask) !=
        SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        CliPrintf (CliHandle, "\r\n Unable to Update DB\r\n");
        return CLI_FAILURE;
    }
#ifdef KERNEL_CAPWAP_WANTED
#ifdef WLC_WANTED
    if (CapwapUpdateKernelDebugInfo () == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapUpdateKernelDebuginfo: Kernel updation failed\r\n");
    }
#endif
#endif
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  CapwapCliSetFsDtlsDebugMask
* Description :
* Input       :  CliHandle 
*            pCapwapSetScalar
*            pCapwapIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
CapwapCliSetFsDtlsDebugMask (tCliHandle CliHandle, UINT4 *pFsDtlsDebugMask)
{
    UINT4               u4ErrorCode;
    INT4                i4FsDtlsDebugMask = 0;
    INT4                i4OldFsDtlsDebugMask = 0;

    CAPWAP_FILL_FSDTLSDEBUGMASK (i4FsDtlsDebugMask, pFsDtlsDebugMask);

    if (CapwapTestFsDtlsDebugMask (&u4ErrorCode, i4FsDtlsDebugMask) !=
        OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    /* OR with old value if its not ZERO */
    if (i4FsDtlsDebugMask != 0)
    {
        if (CapwapGetFsDtlsDebugMask (&i4OldFsDtlsDebugMask) != OSIX_SUCCESS)
        {
            return CLI_FAILURE;
        }
        i4FsDtlsDebugMask = i4FsDtlsDebugMask | i4OldFsDtlsDebugMask;
    }

    if (CapwapSetFsDtlsDebugMask (i4FsDtlsDebugMask) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  CapwapCliSetFsDtlsEncryption
* Description :
* Input       :  CliHandle 
*            pCapwapSetScalar
*            pCapwapIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
CapwapCliSetFsDtlsEncryption (tCliHandle CliHandle, UINT4 *pFsDtlsEncryption)
{
    UINT4               u4ErrorCode;
    INT4                i4FsDtlsEncryption = 0;

    CAPWAP_FILL_FSDTLSENCRYPTION (i4FsDtlsEncryption, pFsDtlsEncryption);

    if (CapwapTestFsDtlsEncryption (&u4ErrorCode, i4FsDtlsEncryption) !=
        OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (CapwapSetFsDtlsEncryption (i4FsDtlsEncryption) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  CapwapCliSetFsDtlsEncryptAlogritham
* Description :
* Input       :  CliHandle 
*            pCapwapSetScalar
*            pCapwapIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
CapwapCliSetFsDtlsEncryptAlogritham (tCliHandle CliHandle,
                                     UINT4 *pFsDtlsEncryptAlogritham)
{
    UINT4               u4ErrorCode;
    INT4                i4FsDtlsEncryptAlogritham = 0;

    CAPWAP_FILL_FSDTLSENCRYPTALOGRITHAM (i4FsDtlsEncryptAlogritham,
                                         pFsDtlsEncryptAlogritham);

    if (CapwapTestFsDtlsEncryptAlogritham
        (&u4ErrorCode, i4FsDtlsEncryptAlogritham) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (CapwapSetFsDtlsEncryptAlogritham (i4FsDtlsEncryptAlogritham) !=
        OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  CapwapCliSetFsStationType
* Description :
* Input       :  CliHandle 
*            pCapwapSetScalar
*            pCapwapIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
CapwapCliSetFsStationType (tCliHandle CliHandle, UINT4 *pFsStationType)
{
    UINT4               u4ErrorCode;
    INT4                i4FsStationType = 0;

    CAPWAP_FILL_FSSTATIONTYPE (i4FsStationType, pFsStationType);

    if (CapwapTestFsStationType (&u4ErrorCode, i4FsStationType) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (CapwapSetFsStationType (i4FsStationType) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  CapwapCliSetFsAcTimestampTrigger
* Description :
* Input       :  CliHandle 
*            pCapwapSetScalar
*            pCapwapIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
CapwapCliSetFsAcTimestampTrigger (tCliHandle CliHandle,
                                  UINT4 *pFsAcTimestampTrigger)
{
    UINT4               u4ErrorCode;
    if (CapwapTestFsAcTimestampTrigger
        (&u4ErrorCode, (INT4) (*pFsAcTimestampTrigger)) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    if (CapwapSetFsAcTimestampTrigger ((INT4) (*pFsAcTimestampTrigger)) !=
        OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  CapwapCliSetFsCapwTrapStatus
* Description :
* Input       :  CliHandle 
*                pFsCapwTrapStatus
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
CapwapCliSetFsCapwTrapStatus (tCliHandle CliHandle, UINT4 *pFsCapwTrapStatus)
{
    UINT4               u4ErrorCode;
    INT4                i4FsCapwTrapStatus = 0;

    i4FsCapwTrapStatus = *(INT4 *) (pFsCapwTrapStatus);
    if (nmhTestv2FsCapwTrapStatus (&u4ErrorCode, i4FsCapwTrapStatus) !=
        SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhSetFsCapwTrapStatus (i4FsCapwTrapStatus) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  CapwapCliSetFsCapwCriticalTrapStatus
* Description :
* Input       :  CliHandle 
*                pFsCapwCriticalTrapStatus
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
CapwapCliSetFsCapwCriticalTrapStatus (tCliHandle CliHandle,
                                      UINT4 *pFsCapwCriticalTrapStatus)
{
    UINT4               u4ErrorCode;
    INT4                i4FsCapwCriticalTrapStatus = 0;

    i4FsCapwCriticalTrapStatus = *(INT4 *) (pFsCapwCriticalTrapStatus);
    if (nmhTestv2FsCapwCriticalTrapStatus
        (&u4ErrorCode, i4FsCapwCriticalTrapStatus) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhSetFsCapwCriticalTrapStatus (i4FsCapwCriticalTrapStatus) !=
        SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/* SHOW COMMANDS*/
/****************************************************************************
 * Function    :  cli_process_capwap_show_cmd
 * Description :  This function is exported to CLI module to handle the
                CAPWAP show commands to take the corresponding action.

 * Input       :  Variable arguments

 * Output      :  None 

 * Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
cli_process_capwap_show_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *args[CAPWAP_CLI_MAX_ARGS];
    INT1                argno = 0;
    UINT4               u4ErrCode = 0;
    INT4                i4RetStatus = CLI_FAILURE;
    UINT4               u4CmdType = 0;
    UINT4               u4WtpProfileId = 0, u4NextProfileId = 0;
    INT4                i4OutCome = 0;
    UINT4               currentProfileId = 0;
    INT4                i4Inst;
    INT4                i4RetvalStationBlackListStatus = 0;
    INT4                i4RetvalStationWhiteListStatus = 0;
    INT4                i4RetValStationType = 0;
    tMacAddr            au1InMacAddr;
    UINT1               au1MacArray1[18];
    UINT1               au1Mac[20];
    tSNMP_OCTET_STRING_TYPE WhiteListStationId;
    MEMSET (&au1MacArray1, 0, 18);
    WhiteListStationId.pu1_OctetList = au1MacArray1;
    UINT1               au1MacArray2[18];
    tSNMP_OCTET_STRING_TYPE NextWhiteListStationId;
    MEMSET (&au1MacArray2, 0, 18);
    MEMSET (&au1Mac, 0, 20);
    NextWhiteListStationId.pu1_OctetList = au1MacArray2;
    tSNMP_OCTET_STRING_TYPE BlackListStationId;
    BlackListStationId.pu1_OctetList = au1MacArray1;
    tSNMP_OCTET_STRING_TYPE NextBlackListStationId;
    NextBlackListStationId.pu1_OctetList = au1MacArray2;

    u4ErrCode = 0;
    tMacAddr            BaseMacAddr;
    UNUSED_PARAM (u4CmdType);
    MEMSET (BaseMacAddr, 0, sizeof (tMacAddr));
    /* check if the capwap module is started. i.e "no shutdown". else all the
     * commands are considered invalid */
    if (CapwapCliCheckModuleStatusShow (CliHandle) != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }
#ifdef CLI_WANTED
    CliRegisterLock (CliHandle, CapwapMainTaskLock, CapwapMainTaskUnLock);
#endif
    /********DEBUG ****/
    CAPWAP_TRC (CAPWAP_MGMT_TRC, "CLI Register Lock successful \n");
    /********DEBUG ****/
    CAPWAP_LOCK;

    va_start (ap, u4Command);

    i4Inst = va_arg (ap, INT4);
    if (i4Inst != 0)
    {
        /* Coverity fix - It is analayzed that all the 
         * Capwap CLI commands function use the first argument as NULL.
         * So the extraction of the first index is not required in this case  */
    }

    while (1)
    {
        args[argno++] = va_arg (ap, UINT4 *);
        if (argno == CAPWAP_CLI_MAX_ARGS)
            break;
    }

    va_end (ap);
    switch (u4Command)
    {
        case CLI_CAPWAP_SHOW_AP_SUMMARY:
            if ((args[0]) != NULL)
            {
                i4RetStatus =
                    CapwapShowApSummary (CliHandle, (UINT1 *) args[0]);
            }
            else
            {
                i4RetStatus = CapwapShowApSummaryAll (CliHandle);
            }
            break;
        case CLI_CAPWAP_SHOW_AP_CONFIG_GENERAL:
            if (args[0] != NULL)
            {
                i4RetStatus =
                    CapwapShowApConfigGeneral (CliHandle, (UINT1 *) args[0]);
            }
            else
            {
                i4RetStatus = CapwapShowApConfigGeneral (CliHandle, NULL);
            }
            break;

        case CLI_CAPWAP_SHOW_DTLS_CONNECTIONS:
            i4RetStatus = CapwapShowDtlsConnectons (CliHandle);
            break;
        case CLI_CAPWAP_SHOW_LINK_ENCRYPTION:

            if (args[0] != NULL)
            {
                if (CapwapGetWtpProfileIdFromProfileName
                    ((UINT1 *) args[0], &u4WtpProfileId) != OSIX_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r\n Entry not found\r\n");
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }

                i4RetStatus =
                    CapwapShowLinkEncryption (CliHandle, (UINT1 *) args[0]);
            }
            else
            {
                i4RetStatus = CapwapShowLinkEncryption (CliHandle, NULL);
            }
            break;

        case CLI_CAPWAP_SHOW_WTP_MODEL:

            i4RetStatus = CapwapShowWtpModel (CliHandle, (UINT1 *) args[0]);
            break;

        case CLI_CAPWAP_SHOW_DISCOVERY_DETAILED_AP:

            if (args[0] != NULL)
            {
                if (CapwapGetWtpProfileIdFromProfileName
                    ((UINT1 *) args[0], &u4WtpProfileId) != OSIX_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r\n Entry not found\r\n");
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                if (u4WtpProfileId == 0)
                {
                    CliPrintf (CliHandle, "\r\n Entry not found \r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }

                i4RetStatus =
                    CapwapShowDiscoveryDetailedAP (CliHandle, &u4WtpProfileId);
            }
            else
            {
                i4OutCome =
                    nmhGetFirstIndexCapwapBaseWtpProfileTable (&u4WtpProfileId);
                if (i4OutCome == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle, "\r\n Entry not found\r\n");
                    break;
                }

                do
                {

                    currentProfileId = u4WtpProfileId;
                    i4RetStatus =
                        CapwapShowDiscoveryDetailedAP (CliHandle,
                                                       &u4WtpProfileId);

                }
                while (nmhGetNextIndexCapwapBaseWtpProfileTable
                       (currentProfileId, &u4WtpProfileId) == SNMP_SUCCESS);
            }
            break;

        case CLI_CAPWAP_SHOW_DISCOVERY_DETAILED:

            i4OutCome =
                nmhGetFirstIndexCapwapBaseWtpProfileTable (&u4WtpProfileId);
            if (i4OutCome == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r\n Entry not found\r\n");
            }

            do
            {

                currentProfileId = u4WtpProfileId;
                i4RetStatus =
                    CapwapShowDiscoveryDetailed (CliHandle, &u4WtpProfileId);
            }
            while (nmhGetNextIndexCapwapBaseWtpProfileTable
                   (currentProfileId, &u4WtpProfileId) == SNMP_SUCCESS);

            break;

        case CLI_CAPWAP_SHOW_JOIN_STATS_DETAILED:

            if (args[0] != NULL)
            {
                if (CapwapGetWtpProfileIdFromProfileName
                    ((UINT1 *) args[0], &u4WtpProfileId) != OSIX_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r\n Entry not found\r\n");
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                if (u4WtpProfileId == 0)
                {
                    CliPrintf (CliHandle, "\r\n Entry not found \r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                i4RetStatus =
                    CapwapShowJoinStatsDetailed (CliHandle, &u4WtpProfileId);
            }
            else
            {
                i4OutCome =
                    nmhGetFirstIndexCapwapBaseWtpProfileTable (&u4WtpProfileId);
                if (i4OutCome == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle, "\r\n Entry not found\r\n");
                }

                do
                {

                    currentProfileId = u4WtpProfileId;
                    i4RetStatus =
                        CapwapShowJoinStatsDetailed (CliHandle,
                                                     &u4WtpProfileId);

                }
                while (nmhGetNextIndexCapwapBaseWtpProfileTable
                       (currentProfileId, &u4WtpProfileId) == SNMP_SUCCESS);
            }
            break;

        case CLI_CAPWAP_SHOW_STATIONS:
        {
            if (NULL != args[0])
            {
                StrToMac ((UINT1 *) args[0], BaseMacAddr);
            }
            i4RetStatus = CapwapShowStations (CliHandle, BaseMacAddr);
        }
            break;

        case CLI_CAPWAP_SHOW_CONFIG_STATS_DETAILED:

            if (args[0] != NULL)
            {
                if (CapwapGetWtpProfileIdFromProfileName
                    ((UINT1 *) args[0], &u4WtpProfileId) != OSIX_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r\n Entry not found\r\n");
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                if (u4WtpProfileId == 0)
                {
                    CliPrintf (CliHandle, "\r\n Entry not found \r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                i4RetStatus =
                    CapwapShowConfigStatsDetailed (CliHandle, &u4WtpProfileId);
            }
            else
            {
                i4OutCome =
                    nmhGetFirstIndexCapwapBaseWtpProfileTable (&u4WtpProfileId);
                if (i4OutCome == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle, "\r\n Entry not found\r\n");
                }
                do
                {
                    currentProfileId = u4WtpProfileId;
                    i4RetStatus =
                        CapwapShowConfigStatsDetailed (CliHandle,
                                                       &u4WtpProfileId);
                }
                while (nmhGetNextIndexCapwapBaseWtpProfileTable
                       (currentProfileId, &u4WtpProfileId) == SNMP_SUCCESS);
            }
            break;

        case CLI_CAPWAP_SHOW_RUNSTATS:
            if (args[0] != NULL)
            {
                if (CapwapGetWtpProfileIdFromProfileName
                    ((UINT1 *) args[0], &u4WtpProfileId) != OSIX_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r\n Entry not found\r\n");
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                if (u4WtpProfileId == 0)
                {
                    CliPrintf (CliHandle, "\r\n Entry not found \r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                i4RetStatus = CapwapShowrunstats (CliHandle, &u4WtpProfileId);
            }
            else
            {
                i4OutCome =
                    nmhGetFirstIndexCapwapBaseWtpProfileTable (&u4WtpProfileId);
                if (i4OutCome == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle, "\r\n Entry not found\r\n");
                }

                do
                {

                    currentProfileId = u4WtpProfileId;
                    i4RetStatus =
                        CapwapShowrunstats (CliHandle, &u4WtpProfileId);

                }
                while (nmhGetNextIndexCapwapBaseWtpProfileTable
                       (currentProfileId, &u4WtpProfileId) == SNMP_SUCCESS);
            }
            break;

        case CLI_CAPWAP_SHOW_AP_STATS:

            if (args[0] == NULL)
            {
                /* Show stats for all APS */
                i4OutCome =
                    nmhGetFirstIndexCapwapBaseWtpProfileTable
                    (&u4NextProfileId);
                if (i4OutCome == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle, "\r\n Entry not found\r\n");
                    break;
                }
                do
                {
                    u4WtpProfileId = u4NextProfileId;
                    i4RetStatus = CapwapShowAPStats (CliHandle, u4WtpProfileId);

                }
                while (nmhGetNextIndexCapwapBaseWtpProfileTable
                       (u4WtpProfileId, &u4NextProfileId) == SNMP_SUCCESS);
                break;

            }
            else
            {
                if (CapwapGetWtpProfileIdFromProfileName
                    ((UINT1 *) args[0], &u4WtpProfileId) != OSIX_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r\n Entry not found\r\n");
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                if (u4WtpProfileId == 0)
                {
                    CliPrintf (CliHandle, "\r\n Entry not found \r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                i4RetStatus = CapwapShowAPStats (CliHandle, u4WtpProfileId);
                break;
            }

        case CLI_CAPWAP_SHOW_ADVANCED_TIMERS:
            i4RetStatus = CapwapShowAdvancedTimers (CliHandle);
            break;

        case CLI_CAPWAP_SHOW_GLOBALS:
            i4RetStatus = CapwapShowGlobals (CliHandle);
            break;

        case CLI_CAPWAP_SHOW_BACKUP_CONTROLLER:
            if (args[0] != NULL)
            {
                i4RetStatus =
                    CapwapShowBackUpController (CliHandle, (UINT1 *) args[0]);
            }

            break;

        case CLI_CAPWAP_SHOW_WHITELIST:
            i4RetStatus = CapwapShowWhiteList (CliHandle);
            break;
        case CLI_CAPWAP_SHOW_BLACKLIST:
            i4RetStatus = CapwapShowBlackList (CliHandle);
            break;
        case CLI_STATION_SHOW_DISCOVERY_MODE:
        {
            if (nmhGetFsStationType (&i4RetValStationType) != SNMP_FAILURE)
            {
                if (i4RetValStationType == 0)
                    CliPrintf (CliHandle, "\r\nDiscovery Mode : Auto\r\n");
                else if (i4RetValStationType == 1)
                    CliPrintf (CliHandle, "\r\nDiscovery Mode : Blacklist\r\n");
                else if (i4RetValStationType == 2)
                    CliPrintf (CliHandle, "\r\nDiscovery Mode : Whitelist\r\n");
            }
        }
            break;
        case CLI_STATION_SHOW_WHITELIST:
        {
            CliPrintf (CliHandle, "\r\nMacAddress        Status");
            CliPrintf (CliHandle, "\r\n----------              ------ \r\n");
            if (args[0] != NULL)
            {
                CliDotStrToMac ((UINT1 *) args[0], au1InMacAddr);
                MEMCPY (NextWhiteListStationId.pu1_OctetList, au1InMacAddr,
                        MAC_ADDR_LEN);
                if (nmhGetFsCapwapStationWhiteListRowStatus
                    (&NextWhiteListStationId,
                     &i4RetvalStationWhiteListStatus) != SNMP_FAILURE)
                {
                    if (i4RetvalStationWhiteListStatus == ACTIVE)
                    {
                        CliMacToStr (NextWhiteListStationId.pu1_OctetList,
                                     au1Mac);
                        CliPrintf (CliHandle, "\r\n%s    Enabled\r\n", au1Mac);
                    }
                    else
                    {
                        CliMacToStr (NextWhiteListStationId.pu1_OctetList,
                                     au1Mac);
                        CliPrintf (CliHandle, "\r\n%s    Disabled\r\n", au1Mac);
                    }
                }
                else
                {
                    CliPrintf (CliHandle, "\r\nWhiteList Table is Empty\r\n");
                }

            }
            else
            {
                if (nmhGetFirstIndexFsCapwapStationWhiteListTable
                    (&NextWhiteListStationId) != SNMP_FAILURE)
                {
                    while (MEMCMP
                           (WhiteListStationId.pu1_OctetList,
                            NextWhiteListStationId.pu1_OctetList,
                            MAC_ADDR_LEN) != 0)
                    {
                        if (nmhGetFsCapwapStationWhiteListRowStatus
                            (&NextWhiteListStationId,
                             &i4RetvalStationWhiteListStatus) != SNMP_FAILURE)
                        {
                            if (i4RetvalStationWhiteListStatus == ACTIVE)
                            {
                                CliMacToStr (NextWhiteListStationId.
                                             pu1_OctetList, au1Mac);
                                CliPrintf (CliHandle, "\r\n%s    Enabled\r\n",
                                           au1Mac);
                            }
                            else
                            {
                                CliMacToStr (NextWhiteListStationId.
                                             pu1_OctetList, au1Mac);
                                CliPrintf (CliHandle, "\r\n%s    Disabled\r\n",
                                           au1Mac);
                            }
                            MEMCPY (WhiteListStationId.pu1_OctetList,
                                    NextWhiteListStationId.pu1_OctetList,
                                    MAC_ADDR_LEN);
                            NextWhiteListStationId.pu1_OctetList = au1MacArray2;
                            if (nmhGetNextIndexFsCapwapStationWhiteListTable
                                (&WhiteListStationId,
                                 &NextWhiteListStationId) != SNMP_SUCCESS)
                            {
                            }
                        }
                    }
                }
                else
                {
                    CliPrintf (CliHandle, "\r\nWhiteList Table is Empty\r\n");
                }
            }
        }
            break;
        case CLI_STATION_SHOW_BLACKLIST:
        {
            CliPrintf (CliHandle, "\r\nMacAddress            Status");
            CliPrintf (CliHandle, "\r\n----------            ------ \r\n");
            if (args[0] != NULL)
            {
                CliDotStrToMac ((UINT1 *) args[0], au1InMacAddr);
                MEMCPY (NextBlackListStationId.pu1_OctetList, au1InMacAddr,
                        MAC_ADDR_LEN);
                if (nmhGetFsCapwapStationBlackListRowStatus
                    (&NextBlackListStationId,
                     &i4RetvalStationBlackListStatus) != SNMP_FAILURE)
                {
                    if (i4RetvalStationBlackListStatus == ACTIVE)
                    {
                        CliMacToStr (NextBlackListStationId.pu1_OctetList,
                                     au1Mac);
                        CliPrintf (CliHandle, "\r\n%s     Enabled\r\n", au1Mac);
                    }
                    else
                    {
                        CliMacToStr (NextBlackListStationId.pu1_OctetList,
                                     au1Mac);
                        CliPrintf (CliHandle, "\r\n%s    Disabled\r\n", au1Mac);
                    }
                }
                else
                {
                    CliPrintf (CliHandle, "\r\nBlackList Table is Empty\r\n");
                }

            }
            else
            {
                if (nmhGetFirstIndexFsCapwapStationBlackListTable
                    (&NextBlackListStationId) != SNMP_FAILURE)
                {
                    while (MEMCMP (BlackListStationId.pu1_OctetList,
                                   NextBlackListStationId.pu1_OctetList,
                                   MAC_ADDR_LEN) != 0)
                    {
                        if (nmhGetFsCapwapStationBlackListRowStatus
                            (&NextBlackListStationId,
                             &i4RetvalStationBlackListStatus) != SNMP_FAILURE)
                        {
                            if (i4RetvalStationBlackListStatus == ACTIVE)
                            {
                                CliMacToStr (NextWhiteListStationId.
                                             pu1_OctetList, au1Mac);
                                CliPrintf (CliHandle, "\r\n%s  Enabled\r\n",
                                           au1Mac);
                            }
                            else
                            {
                                CliMacToStr (NextWhiteListStationId.
                                             pu1_OctetList, au1Mac);
                                CliPrintf (CliHandle, "\r\n%s  Disabled\r\n",
                                           au1Mac);
                            }
                            MEMCPY (BlackListStationId.pu1_OctetList,
                                    NextBlackListStationId.pu1_OctetList,
                                    MAC_ADDR_LEN);
                            NextBlackListStationId.pu1_OctetList = au1MacArray2;
                            if (nmhGetNextIndexFsCapwapStationBlackListTable
                                (&BlackListStationId,
                                 &NextBlackListStationId) != SNMP_SUCCESS)
                            {
                            }
                        }
                    }
                }
                else
                {
                    CliPrintf (CliHandle, "\r\nBlackList Table is empty\r\n");
                }
            }
        }
            break;
        case CLI_CAPWAP_SHOW_AP_CONFIG:
            i4RetStatus = CapwapShowApConfiguration (CliHandle, *args[0],
                                                     (UINT1 *) args[1],
                                                     *args[2]);
            break;
        case CLI_CAPWAP_SHOW_AP_TRAFFIC:

            if (args[0] != NULL)
            {
                if (CapwapGetWtpProfileIdFromProfileName
                    ((UINT1 *) args[0], &u4WtpProfileId) != OSIX_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r\n Entry not found\r\n");
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                if (u4WtpProfileId == 0)
                {
                    CliPrintf (CliHandle, "\r\n Entry not found \r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }

                i4RetStatus = CapwapShowApTraffic (CliHandle, &u4WtpProfileId);
            }
            else
            {
                i4OutCome =
                    nmhGetFirstIndexCapwapBaseWtpProfileTable (&u4WtpProfileId);
                if (i4OutCome == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle, "\r\n Entry not found\r\n");
                    break;
                }

                do
                {

                    currentProfileId = u4WtpProfileId;
                    i4RetStatus =
                        CapwapShowApTraffic (CliHandle, &u4WtpProfileId);

                }
                while (nmhGetNextIndexCapwapBaseWtpProfileTable
                       (currentProfileId, &u4WtpProfileId) == SNMP_SUCCESS);
            }
            break;

        default:
            break;

    }
    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS ((UINT4) i4RetStatus);

#ifdef CLI_WANTED
    CliUnRegisterLock (CliHandle);
#endif
    CAPWAP_UNLOCK;

    return i4RetStatus;

}

/****************************************************************************
* Function    : CapwapShowApSummary
* Description :
* Input       :  CliHandle ,pu1CapwapBaseWtpProfileName
*            
*            
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/

INT4
CapwapShowApSummary (tCliHandle CliHandle, UINT1 *pu1CapwapBaseWtpProfileName)
{

    UINT4               u4WtpProfileId = 0;
    UINT4               u4NoOfRadio = 0;
    INT4                i4RowStatus = 0;
    UINT1               au1RowStatus[256];
    tSNMP_OCTET_STRING_TYPE MacAddress;
    tSNMP_OCTET_STRING_TYPE ModelNumber;
    tSNMP_OCTET_STRING_TYPE Location;
    tSNMP_OCTET_STRING_TYPE CountryName;
    UINT1               au1ModelNumber[256];
    UINT1               au1Location[1024];
    UINT1               Macaddr[20];
    UINT1               au1CountryName[3];

    MEMSET (&MacAddress, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&ModelNumber, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1ModelNumber, 0, sizeof (au1ModelNumber));
    MEMSET (&Location, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1Location, 0, sizeof (au1Location));
    MEMSET (Macaddr, 0, sizeof (Macaddr));
    MEMSET (&CountryName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1CountryName, 0, sizeof (au1CountryName));

    MacAddress.pu1_OctetList = Macaddr;
    ModelNumber.pu1_OctetList = au1ModelNumber;
    ModelNumber.i4_Length = 0;
    CountryName.pu1_OctetList = au1CountryName;
    CountryName.i4_Length = 0;
    Location.pu1_OctetList = au1Location;
    Location.i4_Length = 0;

    if (CapwapGetWtpProfileIdFromProfileName
        (pu1CapwapBaseWtpProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n Entry not found\r\n");
        return OSIX_FAILURE;
    }

    if (nmhGetCapwapBaseWtpProfileWtpMacAddress (u4WtpProfileId, &MacAddress) !=
        SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    if (nmhGetCapwapBaseWtpProfileWtpModelNumber (u4WtpProfileId, &ModelNumber)
        != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    if (nmhGetCapwapBaseWtpProfileWtpLocation (u4WtpProfileId, &Location) !=
        SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
#ifdef WTP_WANTED
    u4NoOfRadio = SYS_DEF_MAX_RADIO_INTERFACES;
#else
    if (nmhGetFsNoOfRadio (&ModelNumber, &u4NoOfRadio) != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
#endif
    if (nmhGetFsWtpCountryString (u4WtpProfileId, &CountryName) != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    if (nmhGetCapwapBaseWtpProfileRowStatus (u4WtpProfileId, &i4RowStatus) ==
        SNMP_SUCCESS)
    {
        if (i4RowStatus == ACTIVE)
            SPRINTF ((CHR1 *) & au1RowStatus, "%s", "Enabled");
        else
            SPRINTF ((CHR1 *) & au1RowStatus, "%s", "Disabled");
    }

    CliPrintf (CliHandle, "\r\nAP Profile Name      :       %s",
               pu1CapwapBaseWtpProfileName);
    CliPrintf (CliHandle, "\r\nRadio Count          :       %d", u4NoOfRadio);
    CliPrintf (CliHandle, "\r\nAP Model             :       %s",
               au1ModelNumber);
    CliPrintf (CliHandle, "\r\nEthernet MAC         :       %s", Macaddr);
    CliPrintf (CliHandle, "\r\nCountry              :       %s",
               au1CountryName);
    CliPrintf (CliHandle, "\r\nLocation             :       %s", au1Location);
    CliPrintf (CliHandle, "\r\nStatus               :       %s", au1RowStatus);
    CliPrintf (CliHandle, "\r\n");

    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : CapwapShowApSummaryAll
* Description :
* Input       :  CliHandle ,pu1CapwapBaseWtpProfileName
*            
*            
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/

INT4
CapwapShowApSummaryAll (tCliHandle CliHandle)
{
    UINT4               u4WtpProfileId = 0;
    UINT4               u4NextProfileId = 0;
    UINT4               u4NoOfRadio = 0;
    INT4                i4OutCome = 0;
    INT4                i4RowStatus = 0;
    INT4                i4TrapStatus = 0;
    INT4                i4CriticalTrapStatus = 0;
    UINT1               Macaddr[20];
    UINT1               au1RowStatus[256];
    UINT1               au1ModelNumber[256];
    UINT1               au1Location[1024];
    UINT1               au1WtpName[512];
    UINT1               au1CountryName[3];
    tSNMP_OCTET_STRING_TYPE MacAddress;
    tSNMP_OCTET_STRING_TYPE ModelNumber;
    tSNMP_OCTET_STRING_TYPE Location;
    tSNMP_OCTET_STRING_TYPE WtpName;
    tSNMP_OCTET_STRING_TYPE CountryName;

    if (nmhGetFsCapwTrapStatus (&i4TrapStatus) == SNMP_SUCCESS)
    {
        if (i4TrapStatus == CAPWAP_WTP_TRAP_ENABLE)
        {
            CliPrintf (CliHandle, "\r\nTrap Status          :       Enabled");
        }
        else
        {
            CliPrintf (CliHandle, "\r\nTrap Status          :       Disabled");
        }
    }

    if (nmhGetFsCapwCriticalTrapStatus (&i4CriticalTrapStatus) == SNMP_SUCCESS)
    {
        if (i4CriticalTrapStatus == CAPWAP_CRITICAL_TRAP_ENABLE)
        {
            CliPrintf (CliHandle, "\r\nCritical Trap Status :       Enabled\n");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nCritical Trap Status :       Disabled\n");
        }
    }

    i4OutCome = nmhGetFirstIndexCapwapBaseWtpProfileTable (&u4NextProfileId);
    if (i4OutCome == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n Entry not found\r\n");
        return OSIX_SUCCESS;
    }

    do
    {
        u4WtpProfileId = u4NextProfileId;
        MEMSET (&MacAddress, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        MEMSET (&ModelNumber, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        MEMSET (au1ModelNumber, 0, sizeof (au1ModelNumber));
        MEMSET (&Location, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        MEMSET (au1Location, 0, sizeof (au1Location));
        MEMSET (&WtpName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        MEMSET (au1WtpName, 0, sizeof (au1WtpName));
        MEMSET (&CountryName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        MEMSET (au1CountryName, 0, sizeof (au1CountryName));
        MEMSET (Macaddr, 0, sizeof (Macaddr));

        MacAddress.pu1_OctetList = Macaddr;
        ModelNumber.pu1_OctetList = au1ModelNumber;
        ModelNumber.i4_Length = 0;
        Location.pu1_OctetList = au1Location;
        Location.i4_Length = 0;
        WtpName.pu1_OctetList = au1WtpName;
        WtpName.i4_Length = 0;
        CountryName.pu1_OctetList = au1CountryName;
        CountryName.i4_Length = 0;
        u4NoOfRadio = 0;

        if (nmhGetCapwapBaseWtpProfileName (u4WtpProfileId, &WtpName) !=
            SNMP_SUCCESS)
        {
            continue;
        }
        if (nmhGetCapwapBaseWtpProfileWtpMacAddress
            (u4WtpProfileId, &MacAddress) != SNMP_SUCCESS)
        {

            continue;
        }

        if (nmhGetCapwapBaseWtpProfileWtpModelNumber
            (u4WtpProfileId, &ModelNumber) != SNMP_SUCCESS)
        {
            continue;
        }

        if (nmhGetCapwapBaseWtpProfileWtpLocation (u4WtpProfileId, &Location) !=
            SNMP_SUCCESS)
        {
            continue;
        }

#ifdef WTP_WANTED
        u4NoOfRadio = SYS_DEF_MAX_RADIO_INTERFACES;
#else
        if (nmhGetFsNoOfRadio (&ModelNumber, &u4NoOfRadio) != SNMP_SUCCESS)
        {
            continue;
        }
#endif
        if (nmhGetFsWtpCountryString (u4WtpProfileId, &CountryName) !=
            SNMP_SUCCESS)
        {
            continue;
        }
        if (nmhGetCapwapBaseWtpProfileRowStatus (u4WtpProfileId, &i4RowStatus)
            == SNMP_SUCCESS)
        {
            if (i4RowStatus == ACTIVE)
                SPRINTF ((CHR1 *) & au1RowStatus, "%s", "Enabled");
            else
                SPRINTF ((CHR1 *) & au1RowStatus, "%s", "Disabled");
        }

        CliPrintf (CliHandle, "\r\n  \r\n");
        CliPrintf (CliHandle, "\r\nAP Profile Name      :       %s",
                   au1WtpName);
        CliPrintf (CliHandle, "\r\nRadio Count          :       %d",
                   u4NoOfRadio);
        CliPrintf (CliHandle, "\r\nAP Model             :       %s",
                   au1ModelNumber);
        CliPrintf (CliHandle, "\r\nEthernet MAC         :       %s", Macaddr);
        CliPrintf (CliHandle, "\r\nCountry              :       %s",
                   au1CountryName);
        CliPrintf (CliHandle, "\r\nLocation             :       %s",
                   au1Location);
        CliPrintf (CliHandle, "\r\nProfile ID           :       %d",
                   u4WtpProfileId);
        CliPrintf (CliHandle, "\r\nStatus               :       %s",
                   au1RowStatus);
        CliPrintf (CliHandle, "\r\n");
    }
    while (nmhGetNextIndexCapwapBaseWtpProfileTable (u4WtpProfileId,
                                                     &u4NextProfileId) ==
           SNMP_SUCCESS);

    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : CapwapShowWtpModel 
* Description :
* Input       :  CliHandle 
*            
*            
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
CapwapShowWtpModel (tCliHandle CliHandle, UINT1 *pau1FsCapwapWtpModelNumber)
{
    tCapwapFsWtpModelEntry CapwapGetFsWtpModelEntry;
    tCapwapFsWtpRadioEntry CapwapGetFsWtpRadioEntry;
    tSNMP_OCTET_STRING_TYPE ModelNumber;
    tSNMP_OCTET_STRING_TYPE WtpSwVersion;
    tSNMP_OCTET_STRING_TYPE WtpHwVersion;
    tSNMP_OCTET_STRING_TYPE ImageName;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tCapwapBindingTable *pCapwapBindingTable = NULL;
    tCapwapBindingTable CapwapBindingTable;
    UINT4               u4Index = 0;
    UINT4               u4RadioNo = 0;
#ifdef WLC_WANTED
    UINT4               u4GetRadioType = 0;
#endif
    UINT1               au1SwVersion[ISS_STR_LEN + 1];
    UINT1               au1HwVersion[ISS_STR_LEN + 1];
    UINT1               au1ImageName[ISS_STR_LEN + 1];
    UINT1               au1ModelNumber[CLI_MAX_MODEL_NAME_LEN + 1];
    INT4                i4RetValFsWtpModelLocalRouting = 0;
    MEMSET (&ModelNumber, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&WtpSwVersion, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&WtpHwVersion, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&ImageName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&CapwapGetFsWtpModelEntry, 0, sizeof (CapwapGetFsWtpModelEntry));
    MEMSET (&CapwapGetFsWtpRadioEntry, 0, sizeof (tCapwapFsWtpRadioEntry));
    MEMSET (au1SwVersion, 0, ISS_STR_LEN + 1);
    MEMSET (au1HwVersion, 0, ISS_STR_LEN + 1);
    MEMSET (au1ModelNumber, 0, ISS_STR_LEN + 1);
    MEMSET (au1ImageName, 0, ISS_STR_LEN + 1);
    MEMSET (&CapwapBindingTable, 0, sizeof (tCapwapBindingTable));

    WtpSwVersion.pu1_OctetList = au1SwVersion;
    WtpHwVersion.pu1_OctetList = au1HwVersion;
    ModelNumber.pu1_OctetList = au1ModelNumber;
    ImageName.pu1_OctetList = au1ImageName;
    if (STRLEN (pau1FsCapwapWtpModelNumber) > CLI_MAX_MODEL_NAME_LEN)
    {
        CliPrintf (CliHandle, "\r\nInvalid Wtp Model Number\r\n");
        return CLI_SUCCESS;
    }
    MEMCPY (au1ModelNumber, pau1FsCapwapWtpModelNumber,
            STRLEN (pau1FsCapwapWtpModelNumber));
    ModelNumber.i4_Length = (INT4) STRLEN (pau1FsCapwapWtpModelNumber);
    MEMCPY (CapwapGetFsWtpModelEntry.MibObject.au1FsCapwapWtpModelNumber,
            pau1FsCapwapWtpModelNumber, STRLEN (pau1FsCapwapWtpModelNumber));
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        CapwapGetFsWtpModelEntry.MibObject.i4FsCapwapWtpModelNumberLen =
        (INT4) (STRLEN (pau1FsCapwapWtpModelNumber));

    if (CapwapGetAllFsWtpModelTable (&CapwapGetFsWtpModelEntry) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\nNo Entry Found\r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return CLI_SUCCESS;
    }

    MEMCPY (CapwapGetFsWtpRadioEntry.MibObject.au1FsCapwapWtpModelNumber,
            pau1FsCapwapWtpModelNumber, STRLEN (pau1FsCapwapWtpModelNumber));

    CapwapGetFsWtpRadioEntry.MibObject.i4FsCapwapWtpModelNumberLen =
        (INT4) (STRLEN (pau1FsCapwapWtpModelNumber));

    MEMCPY (au1SwVersion, IssGetSoftwareVersion (),
            STRLEN ((IssGetSoftwareVersion ()) - 1));

    if (CapwapGetFsWtpModelEntry.MibObject.i4FsCapwapWtpMacType ==
        CLI_MAC_TYPE_SPLIT)
    {
        CliPrintf (CliHandle, "\r\nMac Type                    : Split");
    }
    else if ((CapwapGetFsWtpModelEntry.MibObject.i4FsCapwapWtpMacType ==
              CLI_MAC_TYPE_LOCAL)
             && (CapwapGetFsWtpModelEntry.MibObject.
                 au1FsCapwapWtpTunnelMode[0] != 0))
    {
        CliPrintf (CliHandle, "\r\nMac Type                    : Local");
    }
    else if ((CapwapGetFsWtpModelEntry.MibObject.i4FsCapwapWtpMacType ==
              CLI_MAC_TYPE_LOCAL)
             && (CapwapGetFsWtpModelEntry.MibObject.
                 au1FsCapwapWtpTunnelMode[0] == 0))
    {
        CliPrintf (CliHandle,
                   "\r\nMac Type                    : Not Configured");
    }
    else
    {
        CliPrintf (CliHandle, "\r\n Mac Type                    : Invalid");
    }

    if (CapwapGetFsWtpModelEntry.MibObject.au1FsCapwapWtpTunnelMode[0] ==
        CLI_TUNNEL_MODE_BRIDGE)
    {
        CliPrintf (CliHandle, "\r\nTunnel Mode                 : Bridge");
    }
    else if (CapwapGetFsWtpModelEntry.MibObject.au1FsCapwapWtpTunnelMode[0] ==
             CLI_TUNNEL_MODE_DOT3)
    {
        CliPrintf (CliHandle, "\r\nTunnel Mode                 : 802.3");
    }
    else if (CapwapGetFsWtpModelEntry.MibObject.au1FsCapwapWtpTunnelMode[0] ==
             CLI_TUNNEL_MODE_NATIVE)
    {
        CliPrintf (CliHandle, "\r\nTunnel Mode                 : Native");
    }
    else
    {
        CliPrintf (CliHandle, "\r\nTunnel Mode                 : Invalid");
    }

    if (CapwapGetFsWtpModelEntry.MibObject.u4FsNoOfRadio == 0)
    {
        CliPrintf (CliHandle,
                   "\r\nNo. of Radios               : Not Configured");
    }
    else
    {
        CliPrintf (CliHandle, "\r\nNo. of Radios               : %d",
                   CapwapGetFsWtpModelEntry.MibObject.u4FsNoOfRadio);
    }

    if (nmhGetFsCapwapSwVersion (&ModelNumber, &WtpSwVersion) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\nSoftware Version            : %s",
                   WtpSwVersion.pu1_OctetList);
    }
    if (nmhGetFsCapwapHwVersion (&ModelNumber, &WtpHwVersion) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\nHardware Version            : %s",
                   WtpHwVersion.pu1_OctetList);
    }
    if (nmhGetFsCapwapImageName (&ModelNumber, &ImageName) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\nImage Name                  : %s",
                   ImageName.pu1_OctetList);
    }

    u4RadioNo = CapwapGetFsWtpModelEntry.MibObject.u4FsNoOfRadio;

    for (u4Index = 1; u4Index <= u4RadioNo; u4Index++)
    {
        CapwapGetFsWtpRadioEntry.MibObject.u4FsNumOfRadio = u4Index;
        if (CapwapGetAllFsWtpRadioTable (&CapwapGetFsWtpRadioEntry) !=
            OSIX_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\nRadio Entry not Found\r\n");
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return CLI_SUCCESS;
        }
        CliPrintf (CliHandle, "\r\n");
        CliPrintf (CliHandle, "\r\nRadio : %d", u4Index);
        CliPrintf (CliHandle, "\r\n----------");
        if (CapwapGetFsWtpRadioEntry.MibObject.u4FsWtpRadioType ==
            CLI_RADIO_TYPEA)
        {
            CliPrintf (CliHandle, "\r\nRadio Type                  : A");
        }
        else if (CapwapGetFsWtpRadioEntry.MibObject.u4FsWtpRadioType ==
                 CLI_RADIO_TYPEB)
        {
            CliPrintf (CliHandle, "\r\nRadio Type                  : B");
        }
        else if (CapwapGetFsWtpRadioEntry.MibObject.u4FsWtpRadioType ==
                 CLI_RADIO_TYPEG)
        {
            CliPrintf (CliHandle, "\r\nRadio Type                  : G");
        }
        else if (CapwapGetFsWtpRadioEntry.MibObject.u4FsWtpRadioType ==
                 CLI_RADIO_TYPEBG)
        {
            CliPrintf (CliHandle, "\r\nRadio Type                  : BG");
        }
        else if (CapwapGetFsWtpRadioEntry.MibObject.u4FsWtpRadioType ==
                 CLI_RADIO_TYPEAN)
        {
            CliPrintf (CliHandle, "\r\nRadio Type                  : AN");
        }
        else if (CapwapGetFsWtpRadioEntry.MibObject.u4FsWtpRadioType ==
                 CLI_RADIO_TYPEBGN)
        {
            CliPrintf (CliHandle, "\r\nRadio Type                  : BGN");
        }
        else if (CapwapGetFsWtpRadioEntry.MibObject.u4FsWtpRadioType ==
                 CLI_RADIO_TYPEAC)
        {
            CliPrintf (CliHandle, "\r\nRadio Type                  : AC");
        }
        else
        {
            CliPrintf (CliHandle, "\r\nRadio Type                  : GENERAL");
        }

#ifdef WLC_WANTED

        u4GetRadioType = CapwapGetFsWtpRadioEntry.MibObject.u4FsWtpRadioType;

        if (!(WSSCFG_RADIO_TYPE_COMP (CLI_RADIO_TYPEB)))
        {
            if (CapwapGetFsWtpRadioEntry.MibObject.i4FsRadioAdminStatus ==
                CLI_CAPWAP_RADIO_DISABLE
                || gWsscfgGlobals.WsscfgGlbMib.i4FsDot11bNetworkEnable ==
                CLI_CAPWAP_RADIO_DISABLE)
            {
                CliPrintf (CliHandle,
                           "\r\nAdmin Status                : DISABLE\r\n");
            }
            else
            {
                CliPrintf (CliHandle,
                           "\r\nAdmin Status                : ENABLE\r\n");
            }
        }

        else if (!(WSSCFG_RADIO_TYPE_COMP (CLI_RADIO_TYPEA)))
        {
            if (CapwapGetFsWtpRadioEntry.MibObject.i4FsRadioAdminStatus ==
                CLI_CAPWAP_RADIO_DISABLE
                || gWsscfgGlobals.WsscfgGlbMib.i4FsDot11aNetworkEnable ==
                CLI_CAPWAP_RADIO_DISABLE)
            {
                CliPrintf (CliHandle,
                           "\r\nAdmin Status                : DISABLE\r\n");
            }
            else
            {
                CliPrintf (CliHandle,
                           "\r\nAdmin Status                : ENABLE\r\n");
            }
        }

        else
        {
            if (CapwapGetFsWtpRadioEntry.MibObject.i4FsRadioAdminStatus ==
                CLI_CAPWAP_RADIO_DISABLE)
            {
                CliPrintf (CliHandle,
                           "\r\nAdmin Status                : DISABLE\r\n");
            }
            else
            {
                CliPrintf (CliHandle,
                           "\r\nAdmin Status                : ENABLE\r\n");
            }
        }
#endif

    }
    pWssIfCapwapDB->CapwapIsGetAllDB.bDiscMode = OSIX_TRUE;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                 pWssIfCapwapDB) == OSIX_SUCCESS)
    {
        if (pWssIfCapwapDB->u1DiscMode == AUTO_DISCOVERY_MODE)
        {
            if (nmhGetFsWtpModelLocalRouting
                (&ModelNumber, &i4RetValFsWtpModelLocalRouting) == SNMP_SUCCESS)
            {
                if (i4RetValFsWtpModelLocalRouting == 1)
                {
                    CliPrintf (CliHandle,
                               "\r\nLocal Routing               : Enable");
                }
                else
                {
                    CliPrintf (CliHandle,
                               "\r\nLocal Routing               : Disable");
                }
            }

            pCapwapBindingTable = CapwapGetFirstFsWlanBindingTable ();
            if (pCapwapBindingTable != NULL)
            {
                CliPrintf (CliHandle, "\r\n");
                CliPrintf (CliHandle,
                           "Radio Id     Wlan Profile Id         Status");
                CliPrintf (CliHandle, "\r\n");
                CliPrintf (CliHandle,
                           "--------     --------------          ------");
                CliPrintf (CliHandle, "\r\n");
                do
                {
                    if (MEMCMP
                        (pau1FsCapwapWtpModelNumber,
                         pCapwapBindingTable->au1ModelNumber,
                         MAX_WTP_MODELNUMBER_LEN) == 0)
                    {
                        if (pCapwapBindingTable->u1RowStatus == ACTIVE)
                        {
                            CliPrintf (CliHandle,
                                       "%4d %17d                Enable",
                                       pCapwapBindingTable->u1RadioId,
                                       pCapwapBindingTable->u2WlanProfileId);
                        }
                        else
                        {
                            CliPrintf (CliHandle,
                                       "%4d %17d                Disable",
                                       pCapwapBindingTable->u1RadioId,
                                       pCapwapBindingTable->u2WlanProfileId);
                        }
                        CliPrintf (CliHandle, "\r\n");
                    }
                    MEMSET (&CapwapBindingTable, 0,
                            sizeof (tCapwapBindingTable));
                    MEMCPY (&CapwapBindingTable, pCapwapBindingTable,
                            sizeof (tCapwapBindingTable));
                    pCapwapBindingTable =
                        CapwapGetNextFsWlanBindingTable (&CapwapBindingTable);
                }
                while (pCapwapBindingTable != NULL);
            }
        }
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    : CapwapShowApConfigGeneral 
* Description :
* Input       :  CliHandle 
*            
*            
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
CapwapShowApConfigGeneral (tCliHandle CliHandle,
                           UINT1 *pu1CapwapBaseWtpProfileName)
{
    UINT4               u4WtpProfileId = 0;
    CHR1               *pu1LocalIpString = NULL;
    CHR1               *pu1NetmaskString = NULL;
    CHR1               *pu1GatewayIpString = NULL;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tMacAddr            MacAddr;
    UINT1               au1MacAddr[20];
    tMacAddr            BaseMacAddr;
    INT4                i4AddressType = 0, i4WtpState = 0;
    UINT4               u4Seconds = 0;
    UINT4               u4Sec = 0;
    UINT4               u4Min = 0;
    UINT4               u4Hrs = 0;
    UINT4               u4Days = 0;
    UINT1               au1DateString[100];
    UINT4               u4IpAddr = 0, u4LocalIpAddr = 0;
    tSNMP_OCTET_STRING_TYPE MacAddress;
    tSNMP_OCTET_STRING_TYPE BaseMacAddress;
    tSNMP_OCTET_STRING_TYPE IpAddress;
    tSNMP_OCTET_STRING_TYPE LocalIpAddress;
    tSNMP_OCTET_STRING_TYPE WtpProfileName;
    tSNMP_OCTET_STRING_TYPE WtpSerialNum;
    UINT1               au1WtpSerialNum[128];
    UINT1               au1WtpName[256];
    tSNMP_OCTET_STRING_TYPE WtpName;
    UINT1               au1WtpName1[512];
    UINT1               au1WtpImageName[256];
    tSNMP_OCTET_STRING_TYPE WtpImageName;
    UINT1               au1WtpImageName1[512];
    UINT1               au1TmpMac[MAC_ADDR_LEN];
    UINT1               au1TmpBaseMac[MAC_ADDR_LEN];
    UINT1               au1IpId[4];
    UINT1               au1LocalIpId[16];
    INT4                i4EcnSupport = 0, i4FsWtpForwarding = 0;
    INT4                i4FsWtpDiscoveryType = 0, i4FsWtpNativeVlanId = 0;
    tCapwapCapwapBaseWtpProfileEntry *pCapwapCapwapBaseWtpProfileEntry = NULL;
    WtpName.pu1_OctetList = au1WtpName1;
    WtpName.i4_Length = 0;
    MEMSET (&WtpImageName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    WtpImageName.pu1_OctetList = au1WtpImageName1;
    WtpImageName.i4_Length = 0;
    tWssIfPMDB          WssIfPMDB;
    MEMSET (&WssIfPMDB, 0, sizeof (tWssIfPMDB));

#ifdef WTP_WANTED
    UINT1               u1index = 0;
    UINT4               u4RadioIfIndex = 0;
    UINT2               u2ReportInterval = 0;
#endif

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, CLI_FAILURE)
        MEMSET (au1DateString, 0, 100);
    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (MacAddr, 0, sizeof (tMacAddr));
    MEMSET (BaseMacAddr, 0, sizeof (tMacAddr));
    MEMSET (&MacAddress, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&WtpProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&BaseMacAddress, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&IpAddress, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&LocalIpAddress, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&WtpSerialNum, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1WtpSerialNum, 0, 128);
    MEMSET (au1TmpMac, 0, MAC_ADDR_LEN);
    MEMSET (au1TmpBaseMac, 0, MAC_ADDR_LEN);
    MEMSET (au1IpId, 0, 4);
    MEMSET (au1LocalIpId, 0, 16);
    MEMSET (au1WtpName, 0, 256);
    MEMSET (au1WtpName1, 0, 256);
    MEMSET (au1WtpImageName, 0, 256);
    MEMSET (au1WtpImageName1, 0, 256);
    MEMSET (&au1MacAddr, 0, 20);
    MacAddress.pu1_OctetList = au1MacAddr;
    BaseMacAddress.pu1_OctetList = BaseMacAddr;
    IpAddress.pu1_OctetList = (UINT1 *) &u4IpAddr;
    LocalIpAddress.pu1_OctetList = (UINT1 *) &u4LocalIpAddr;
    WtpSerialNum.pu1_OctetList = au1WtpSerialNum;

    pCapwapCapwapBaseWtpProfileEntry =
        (tCapwapCapwapBaseWtpProfileEntry *)
        MemAllocMemBlk (CAPWAP_CAPWAPBASEWTPPROFILETABLE_POOLID);
    if (pCapwapCapwapBaseWtpProfileEntry == NULL)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    MEMSET (pCapwapCapwapBaseWtpProfileEntry, 0,
            sizeof (tCapwapCapwapBaseWtpProfileEntry));
    if (pu1CapwapBaseWtpProfileName != NULL)
    {

        if (CapwapGetWtpProfileIdFromProfileName
            (pu1CapwapBaseWtpProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\nProfile entry not found\r\n");
            MemReleaseMemBlock (CAPWAP_CAPWAPBASEWTPPROFILETABLE_POOLID,
                                (UINT1 *) pCapwapCapwapBaseWtpProfileEntry);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
        pWssIfCapwapDB->CapwapGetDB.u2WtpProfileId = (UINT2) u4WtpProfileId;
        WtpProfileName.pu1_OctetList = au1WtpName;
        if (nmhGetCapwapBaseWtpProfileName (u4WtpProfileId, &WtpProfileName) !=
            SNMP_SUCCESS)
        {
        }

        if (nmhGetCapwapBaseWtpProfileWtpMacAddress
            (u4WtpProfileId, &MacAddress) != SNMP_SUCCESS)
        {
            /* Kloc Fix Start */
            MemReleaseMemBlock (CAPWAP_CAPWAPBASEWTPPROFILETABLE_POOLID,
                                (UINT1 *) pCapwapCapwapBaseWtpProfileEntry);
            /* Kloc Fix Ends */
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
        pCapwapCapwapBaseWtpProfileEntry->MibObject.u4CapwapBaseWtpProfileId =
            u4WtpProfileId;

        if (CapwapGetAllCapwapBaseWtpProfileTable
            (pCapwapCapwapBaseWtpProfileEntry) != OSIX_SUCCESS)
        {
            MemReleaseMemBlock (CAPWAP_CAPWAPBASEWTPPROFILETABLE_POOLID,
                                (UINT1 *) pCapwapCapwapBaseWtpProfileEntry);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
        i4EcnSupport =
            pCapwapCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileWtpEcnSupport;

        if (nmhGetCapwapBaseWtpProfileWtpStaticIpEnable
            (u4WtpProfileId, &i4AddressType) == SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\nIP Address Type        : STATIC");
            if (nmhGetCapwapBaseWtpProfileWtpStaticIpAddress
                (u4WtpProfileId, &IpAddress) != SNMP_SUCCESS)
            {
                /* Kloc Fix Start */
                MemReleaseMemBlock (CAPWAP_CAPWAPBASEWTPPROFILETABLE_POOLID,
                                    (UINT1 *) pCapwapCapwapBaseWtpProfileEntry);
                /* Kloc Fix Ends */
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
        }
        else
        {
            CliPrintf (CliHandle, "\r\nIP Address Type        : DHCP");
        }

        if (nmhGetCapwapBaseWtpProfileWtpName (u4WtpProfileId, &WtpName) !=
            SNMP_SUCCESS)
        {
            /* Kloc Fix Start */
            MemReleaseMemBlock (CAPWAP_CAPWAPBASEWTPPROFILETABLE_POOLID,
                                (UINT1 *) pCapwapCapwapBaseWtpProfileEntry);
            /* Kloc Fix Ends */
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
        WtpImageName.pu1_OctetList = au1WtpImageName;
        if (nmhGetFsCapwapWtpImageName (u4WtpProfileId, &WtpImageName) !=
            SNMP_SUCCESS)
        {

        }

        CliPrintf (CliHandle, "\r\nAP Profile Name        : %s",
                   WtpProfileName.pu1_OctetList);
        CliPrintf (CliHandle, "\r\nAP Name                : %s",
                   WtpName.pu1_OctetList);
        CliPrintf (CliHandle, "\r\nAP Profile ID          : %d",
                   u4WtpProfileId);
        CliPrintf (CliHandle, "\r\nAP Mac Address         : %s",
                   MacAddress.pu1_OctetList);
        CliPrintf (CliHandle, "\r\nAP Image name          : %s",
                   WtpImageName.pu1_OctetList);

        pWssIfCapwapDB->CapwapGetDB.u2WtpProfileId = (UINT2) u4WtpProfileId;
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpDescriptor = OSIX_TRUE;
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpStatisticsTimer = OSIX_TRUE;

        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
            == OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to Set the Received WtpFallback \r\n");
        }
        CliPrintf (CliHandle, "\r\nAP radio Number        : %d",
                   pWssIfCapwapDB->CapwapGetDB.u1WtpNoofRadio);

        if (nmhGetFsCapwapApSerialNumber (u4WtpProfileId, &WtpSerialNum) !=
            SNMP_SUCCESS)
        {
            MemReleaseMemBlock (CAPWAP_CAPWAPBASEWTPPROFILETABLE_POOLID,
                                (UINT1 *) pCapwapCapwapBaseWtpProfileEntry);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }

        CliPrintf (CliHandle, "\r\nAP Serial Number       : %s",
                   WtpSerialNum.pu1_OctetList);

        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpProfileId = OSIX_TRUE;
        pWssIfCapwapDB->CapwapGetDB.u2ProfileId = (UINT2) u4WtpProfileId;
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_INTERNAL_ID,
                                     pWssIfCapwapDB) != OSIX_SUCCESS)
        {
        }

        WssIfPMDB.u2ProfileId = pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId;
        if (WssIfProcessPMDBMsg
            (WSS_PM_WTP_EVT_VSP_AP_STATS_GET, &WssIfPMDB) != OSIX_SUCCESS)
        {
        }
        CliPrintf (CliHandle, "\r\nNumber of interfaces   : %d",
                   WssIfPMDB.ApElement.u1TotalInterfaces);

        CLI_CONVERT_IPADDR_TO_STR (pu1NetmaskString,
                                   WssIfPMDB.ApElement.u4Subnetmask);
        CliPrintf (CliHandle, "\r\nAP netmask             : %s",
                   pu1NetmaskString);
        CLI_CONVERT_IPADDR_TO_STR (pu1GatewayIpString,
                                   WssIfPMDB.ApElement.u4GatewayIp);

        CliPrintf (CliHandle, "\r\nAP Gateway             : %s",
                   pu1GatewayIpString);

        pWssIfCapwapDB->CapwapGetDB.u2WtpProfileId = (UINT2) u4WtpProfileId;
        pWssIfCapwapDB->CapwapIsGetAllDB.bIpAddressType = OSIX_TRUE;
        pWssIfCapwapDB->CapwapIsGetAllDB.bIpAddress = OSIX_TRUE;
        if (WssIfProcessCapwapDBMsg
            (WSS_CAPWAP_SHOW_WTPSTATE_TABLE, pWssIfCapwapDB) == OSIX_SUCCESS)
        {

            CliPrintf (CliHandle, "\r\nLocal IP Address Type  : IPV4");

            CLI_CONVERT_IPADDR_TO_STR (pu1LocalIpString,
                                       pWssIfCapwapDB->CapwapGetDB.
                                       WtpStaticIpAddress.u4_addr[0]);
            CliPrintf (CliHandle, "\r\nAP IP Address          : %s",
                       pu1LocalIpString);
            CliPrintf (CliHandle, "\r\nBase Mac Address       : %s",
                       MacAddress.pu1_OctetList);
            if (i4EcnSupport == CAPWAP_LIMITED_ECN_SUPPORT)
            {
                CliPrintf (CliHandle, "\r\nEcn support is         : LIMITED");
            }
            else if (i4EcnSupport == CAPWAP_FULL_ENC_SUPPORT)
            {
                CliPrintf (CliHandle, "\r\nEcn support is     : FULL");
            }
            if (nmhGetCapwapBaseWtpState (&MacAddress, &i4WtpState) !=
                SNMP_SUCCESS)
            {
            }
            if (i4WtpState == CAPWAP_START)
            {
                CliPrintf (CliHandle, "\r\nAP State               : START");
            }
            else if (i4WtpState == CAPWAP_IDLE)
            {
                CliPrintf (CliHandle, "\r\nAP State               : IDLE");
            }
            else if (i4WtpState == CAPWAP_DISCOVERY)
            {
                CliPrintf (CliHandle, "\r\nAP State               : DISCOVERY");
            }
            else if (i4WtpState == CAPWAP_DTLS)
            {
                CliPrintf (CliHandle, "\r\nAP State               : DTLS");
            }
            else if (i4WtpState == CAPWAP_JOIN)
            {
                CliPrintf (CliHandle, "\r\nAP State               : JOIN");
            }
            else if (i4WtpState == CAPWAP_IMAGEDATA)
            {
                CliPrintf (CliHandle, "\r\nAP State               : IMAGE");
            }
            else if (i4WtpState == CAPWAP_CONFIGURE)
            {
                CliPrintf (CliHandle, "\r\nAP State               : CONFIGURE");
            }
            else if (i4WtpState == CAPWAP_DATACHECK)
            {
                CliPrintf (CliHandle, "\r\nAP State               : DATACHECK");
            }
            else if (i4WtpState == CAPWAP_RUN)
            {
                CliPrintf (CliHandle, "\r\nAP State               : RUN");
            }
            else if (i4WtpState == CAPWAP_RESET)
            {
                CliPrintf (CliHandle, "\r\nAP State               : RESET");
            }
            else if (i4WtpState == CAPWAP_DTLSTD)
            {
                CliPrintf (CliHandle,
                           "\r\nAP State               : DTLSTEARDOWN");
            }
            else if (i4WtpState == CAPWAP_SULKING)
            {
                CliPrintf (CliHandle, "\r\nAP State               : SULKING");
            }
            else if (i4WtpState == CAPWAP_DEAD)
            {
                CliPrintf (CliHandle, "\r\nAP State               : DEAD");
            }
            else
            {
                CliPrintf (CliHandle, "\r\nAP State               : Unknown");
            }
        }
        else
        {
            CliPrintf (CliHandle, "\r\nAP State               : Not Connected");

        }
    }

    /* Assign the index */
    pCapwapCapwapBaseWtpProfileEntry->MibObject.u4CapwapBaseWtpProfileId =
        u4WtpProfileId;

    if (CapwapGetAllCapwapBaseWtpProfileTable (pCapwapCapwapBaseWtpProfileEntry)
        != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (CAPWAP_CAPWAPBASEWTPPROFILETABLE_POOLID,
                            (UINT1 *) pCapwapCapwapBaseWtpProfileEntry);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    CliPrintf (CliHandle, "\r\nAP Location            : %s",
               pCapwapCapwapBaseWtpProfileEntry->MibObject.
               au1CapwapBaseWtpProfileWtpLocation);
    CliPrintf (CliHandle, "\r\nAP Model Number        : %s",
               pCapwapCapwapBaseWtpProfileEntry->MibObject.
               au1CapwapBaseWtpProfileWtpModelNumber);
    u4Seconds = OsixGetSysUpTime ();
    if (pCapwapCapwapBaseWtpProfileEntry->u4SystemUpTime == 0)
    {
        u4Seconds = pCapwapCapwapBaseWtpProfileEntry->u4SystemUpTime;
    }
    else
    {
        u4Seconds -= pCapwapCapwapBaseWtpProfileEntry->u4SystemUpTime;
    }

    u4Sec = (UINT4) (u4Seconds % SECS_IN_MINUTE);
    u4Min = (UINT4) ((u4Seconds / SECS_IN_MINUTE) % MINUTES_IN_HOUR);
    u4Hrs = (UINT4) ((u4Seconds / SECS_IN_HOUR) % HOURS_IN_DAY);
    u4Days = (UINT4) (u4Seconds / SECS_IN_DAY);

    SPRINTF ((CHR1 *) au1DateString, "%d Days %d Hrs, %d Mins, %d Secs",
             u4Days, u4Hrs, u4Min, u4Sec);

    CliPrintf (CliHandle, "\r\nAP Up Time             : %s", au1DateString);

    CliPrintf (CliHandle, "\r\nAP Statistics Timer    : %d",
               pWssIfCapwapDB->CapwapGetDB.u2WtpStatisticsTimer);

#ifdef WLC_WANTED
    nmhGetFsWtpDiscoveryType
        (pCapwapCapwapBaseWtpProfileEntry->MibObject.u4CapwapBaseWtpProfileId,
         &i4FsWtpDiscoveryType);
#endif
    if (i4FsWtpDiscoveryType == 1)
        CliPrintf (CliHandle, "\r\nDiscovery Type         : Static");
    else if (i4FsWtpDiscoveryType == 2)
        CliPrintf (CliHandle, "\r\nDiscovery Type         : DHCP");
    else if (i4FsWtpDiscoveryType == 3)
        CliPrintf (CliHandle, "\r\nDiscovery Type         : Dns");
    else if (i4FsWtpDiscoveryType == 4)
        CliPrintf (CliHandle, "\r\nDiscovery Type         : AcReferral");

    CliPrintf (CliHandle, "\r\nDiscovery Interval     : %d",
               pCapwapCapwapBaseWtpProfileEntry->MibObject.
               u4CapwapBaseWtpProfileWtpMaxDiscoveryInterval);
#ifdef WLC_WANTED
    CliPrintf (CliHandle, "\r\nWTP Report Interval    : %d",
               pCapwapCapwapBaseWtpProfileEntry->MibObject.
               u4CapwapBaseWtpProfileWtpReportInterval);
#endif
    CliPrintf (CliHandle, "\r\nWTP Idle Timeout       : %d",
               pCapwapCapwapBaseWtpProfileEntry->MibObject.
               u4CapwapBaseWtpProfileWtpIdleTimeout);
    CliPrintf (CliHandle, "\r\nEcho Interval          : %d",
               pCapwapCapwapBaseWtpProfileEntry->MibObject.
               u4CapwapBaseWtpProfileWtpEchoInterval);
    if (1 ==
        pCapwapCapwapBaseWtpProfileEntry->MibObject.
        i4CapwapBaseWtpProfileWtpFallbackEnable)
        CliPrintf (CliHandle, "\r\nFallback               : Enable");
    else
        CliPrintf (CliHandle, "\r\nFallback               : Disable");

    nmhGetFsWtpNativeVlanId (pCapwapCapwapBaseWtpProfileEntry->MibObject.
                             u4CapwapBaseWtpProfileId, &i4FsWtpNativeVlanId);

    CliPrintf (CliHandle, "\r\nNative Vlan            : %d",
               i4FsWtpNativeVlanId);

    nmhGetFsWtpLocalRouting (pCapwapCapwapBaseWtpProfileEntry->MibObject.
                             u4CapwapBaseWtpProfileId, &i4FsWtpForwarding);
    if (i4FsWtpForwarding == 1)
    {
        CliPrintf (CliHandle, "\r\nForwarding State       : Local Routing\n");
    }
    else if (i4FsWtpForwarding == 2)
    {
        CliPrintf (CliHandle,
                   "\r\nForwarding State       : Centralized Forwarding\n");
    }
    else if (i4FsWtpForwarding == 3)
    {
        CliPrintf (CliHandle, "\r\nForwarding State       : Local Bridging\n");
    }
#ifdef WTP_WANTED
    CliPrintf (CliHandle, "\r\nWTP Report Interval");
    CliPrintf (CliHandle, "\r\n-------------------");
    for (u1index = 1; u1index <= SYS_DEF_MAX_RADIO_INTERFACES; u1index++)
    {
        u4RadioIfIndex = u1index + SYS_DEF_MAX_ENET_INTERFACES;
        u2ReportInterval = 0;
        CapwapUtilGetReportInterval (u4RadioIfIndex, &u2ReportInterval);

        CliPrintf (CliHandle, "\r\nRadio %d - %d", u1index, u2ReportInterval);
        CliPrintf (CliHandle, "\r\n");
    }
#endif
    MemReleaseMemBlock (CAPWAP_CAPWAPBASEWTPPROFILETABLE_POOLID,
                        (UINT1 *) pCapwapCapwapBaseWtpProfileEntry);

    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    : CapwapShowStations 
* Description :
* Input       :  CliHandle 
*            
*            
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
CapwapShowStations (tCliHandle CliHandle, tMacAddr staMacAddr)
{
#ifdef WLC_WANTED
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT4               u4BssIfIndex = 0;
    UINT4               u4RadioIfIndex = 0;
    UINT4               u4RadioId = 0;
    UINT4               u4WtpInternalId = 0;
    UINT1              *pu1TempMac = NULL;
    UINT1               au1TempMac[21];
    UINT1               au1WtpMac[21];
    tMacAddr            WtpMacAddr;
    tMacAddr            zeroAddr;
    UINT1              *pu1WtpMacAddr = NULL;
    UINT1               u1Index = 0;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (WtpMacAddr, 0, sizeof (tMacAddr));
    MEMSET (zeroAddr, 0, sizeof (tMacAddr));
    MEMSET (&au1WtpMac, 0, sizeof (au1WtpMac));
    MEMSET (&au1TempMac, 0, sizeof (au1TempMac));
    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    pu1WtpMacAddr = au1WtpMac;
    pu1TempMac = au1TempMac;

    CliPrintf (CliHandle, "\r\nStation Mac Address  Radio Id    WTP Mac ");
    CliPrintf (CliHandle, "\r\n");
    CliPrintf (CliHandle, "-------------------  --------    -------- ");
    CliPrintf (CliHandle, "\r\n");

    if (MEMCMP (staMacAddr, zeroAddr, CFA_ENET_ADDR_LEN) == 0)
    {
        WssStaShowClient ();
        if (gu4ClientWalkIndex == 0)
        {
            CliPrintf (CliHandle, "\r\n Entry not found\r\n");
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return CLI_FAILURE;
        }
    }
    else
    {
        gu4ClientWalkIndex = 1;
        MEMCPY (gaWssClientSummary[u1Index].staMacAddr, staMacAddr,
                sizeof (tMacAddr));
    }
    for (u1Index = 0; u1Index < gu4ClientWalkIndex; u1Index++)
    {
        MEMSET (&au1WtpMac, 0, sizeof (au1WtpMac));
        MEMSET (&au1TempMac, 0, sizeof (au1TempMac));
        pu1WtpMacAddr = au1WtpMac;
        pu1TempMac = au1TempMac;

        if (CapwapGetBssifIndexFromStationMac
            (gaWssClientSummary[u1Index].staMacAddr,
             &u4BssIfIndex) != OSIX_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\n Error in getting BssIfIndex \r\n");
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            CLI_FATAL_ERROR (CliHandle);
            return OSIX_FAILURE;

        }

        if (CapwapGetRadioIfIndexFromBssIfIndex (u4BssIfIndex, &u4RadioIfIndex)
            != OSIX_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\n Error in getting RadioIfIndex \r\n");
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            CLI_FATAL_ERROR (CliHandle);
            return OSIX_FAILURE;
        }

        if (CapwapGetRadioIdFromRadioIfIndex
            (u4RadioIfIndex, &u4RadioId, &u4WtpInternalId) != OSIX_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\n Error in Accessing RadioDB \r\n");
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            CLI_FATAL_ERROR (CliHandle);
            return OSIX_FAILURE;
        }

        if (CapwapGetWtpMacFromWtpInternalId (u4WtpInternalId, &WtpMacAddr) !=
            OSIX_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\n Error in Accessing CapwapDB \r\n");
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            CLI_FATAL_ERROR (CliHandle);
            return OSIX_FAILURE;
        }
        PrintMacAddress ((UINT1 *) &gaWssClientSummary[u1Index].staMacAddr,
                         pu1TempMac);
        PrintMacAddress (WtpMacAddr, pu1WtpMacAddr);
        CliPrintf (CliHandle, "\r\n");
        CliPrintf (CliHandle, "%-21s", pu1TempMac);
        CliPrintf (CliHandle, "%-12d", u4RadioId);
        CliPrintf (CliHandle, "%-20s", pu1WtpMacAddr);
        CliPrintf (CliHandle, "\r\n");
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
#else
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (staMacAddr);
#endif
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    : CapwapShowDtlsConnectons 
* Description :
* Input       :  CliHandle 
*            
*            
* Output      :  None 
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
CapwapShowDtlsConnectons (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE DtlsDataChannel;
    UINT1               au1DtlsDataChannel[256];
    tSNMP_OCTET_STRING_TYPE DtlsControlChannel;
    UINT1               au1DtlsControlChannel[256];

    MEMSET (&DtlsDataChannel, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1DtlsDataChannel, 0, sizeof (256));
    MEMSET (&DtlsControlChannel, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1DtlsControlChannel, 0, sizeof (256));

    DtlsDataChannel.pu1_OctetList = au1DtlsDataChannel;
    DtlsControlChannel.pu1_OctetList = au1DtlsControlChannel;

    if (nmhGetCapwapBaseDataChannelDTLSPolicyOptions (&DtlsDataChannel) !=
        SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    if (nmhGetFsCapwapControlChannelDTLSPolicyOptions (&DtlsControlChannel) !=
        SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    if (au1DtlsControlChannel[0] == 0)
    {
        CliPrintf (CliHandle, "\r\nCAPWAP Control Channel    : Plain Text\n");
    }
    else if (au1DtlsControlChannel[0] == 1)
    {
        CliPrintf (CliHandle, "\r\nCAPWAP Control Channel    : Plain Text\n");
    }
    else if (au1DtlsControlChannel[0] == 2)
    {
        CliPrintf (CliHandle,
                   "\r\nCAPWAP Control Channel    : DTLS Encrypted\n");
    }

    if (au1DtlsDataChannel[0] == 0)
    {
        CliPrintf (CliHandle, "\r\nCAPWAP Data Channel       : Plain Text\n");
    }
    else if (au1DtlsDataChannel[0] == 1)
    {
        CliPrintf (CliHandle, "\r\nCAPWAP Data Channel       : Plain Text\n");
    }
    else if (au1DtlsDataChannel[0] == 2)
    {
        CliPrintf (CliHandle,
                   "\r\nCAPWAP Data Channel       : DTLS Encrypted\n");
    }
    return CLI_SUCCESS;

}

/****************************************************************************
* Function    : CapwapShowLinkEncryption 
* Description :
* Input       :  CliHandle 
*            
*            
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
CapwapShowLinkEncryption (tCliHandle CliHandle,
                          UINT1 *pu1CapwapBaseWtpProfileName)
{
    tSNMP_OCTET_STRING_TYPE WtpName;
    tSNMP_OCTET_STRING_TYPE DtlsDataChannel;
    tSNMP_OCTET_STRING_TYPE DtlsControlChannel;
    UINT1               au1WtpName[512];
    UINT1               au1DtlsDataChannel[256];
    UINT1               au1DtlsControlChannel[256];
    tCapwapFsCapwapLinkEncryptionEntry *pCapwapFsCapwapLinkEncryptionEntry =
        NULL;
    UINT4               currentProfileId = 0, nextProfileId = 0;
    INT4                i4OutCome = 0;
    UINT2               ChannelStatus = 0;
    UINT2               DataStatus = 0;

    MEMSET (&DtlsDataChannel, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1DtlsDataChannel, 0, sizeof (256));
    MEMSET (&DtlsControlChannel, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1DtlsControlChannel, 0, sizeof (256));

    pCapwapFsCapwapLinkEncryptionEntry = (tCapwapFsCapwapLinkEncryptionEntry *)
        MemAllocMemBlk (CAPWAP_FSCAPWAPLINKENCRYPTIONTABLE_POOLID);

    if (pCapwapFsCapwapLinkEncryptionEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pCapwapFsCapwapLinkEncryptionEntry, 0,
            sizeof (tCapwapFsCapwapLinkEncryptionEntry));

    DtlsDataChannel.pu1_OctetList = au1DtlsDataChannel;
    DtlsControlChannel.pu1_OctetList = au1DtlsControlChannel;

    if (NULL == pu1CapwapBaseWtpProfileName)
    {
        /* This is a show command to print the details of all the APs  */

        i4OutCome = nmhGetFirstIndexCapwapBaseWtpProfileTable (&nextProfileId);
        if (i4OutCome == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n Entry not found\r\n");
        }

        do
        {

            currentProfileId = nextProfileId;

            MEMSET (&WtpName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
            MEMSET (au1WtpName, 0, sizeof (au1WtpName));
            WtpName.pu1_OctetList = au1WtpName;
            WtpName.i4_Length = 0;

            pCapwapFsCapwapLinkEncryptionEntry->MibObject.
                u4CapwapBaseWtpProfileId = currentProfileId;
            pCapwapFsCapwapLinkEncryptionEntry->MibObject.
                i4FsCapwapEncryptChannel = CLI_FSCAPWAP_ENCRYPTION_CONTROL;

            if (nmhGetCapwapBaseWtpProfileName (currentProfileId, &WtpName) !=
                SNMP_SUCCESS)
            {
                MemReleaseMemBlock (CAPWAP_FSCAPWAPLINKENCRYPTIONTABLE_POOLID,
                                    (UINT1 *)
                                    pCapwapFsCapwapLinkEncryptionEntry);
                return OSIX_FAILURE;
            }
            CliPrintf (CliHandle,
                       "\r\n**********************************************\r\n");
            CliPrintf (CliHandle, "\r\nAP Name                : %s\r\n",
                       WtpName);
            if (CapwapGetAllFsCapwapLinkEncryptionTable
                (pCapwapFsCapwapLinkEncryptionEntry) == OSIX_SUCCESS)
            {
                ChannelStatus =
                    (UINT2) (pCapwapFsCapwapLinkEncryptionEntry->MibObject.
                             i4FsCapwapEncryptChannelStatus);
                if (ChannelStatus == 0)
                {
                    CliPrintf (CliHandle,
                               "\r\nCAPWAP Control Channel    : Unknown Encryption\n");
                }
                else if (ChannelStatus == 1)
                {
                    CliPrintf (CliHandle,
                               "\r\nCAPWAP Control Channel    : DTLS Encrypted\n");
                }
                else if (ChannelStatus == 2)
                {
                    CliPrintf (CliHandle,
                               "\r\nCAPWAP Control Channel    : Plain Text\n");
                }
            }
            else
            {
                CliPrintf (CliHandle,
                           "\r\nCAPWAP Control Channel    : Not configured\n");
            }
            pCapwapFsCapwapLinkEncryptionEntry->MibObject.
                i4FsCapwapEncryptChannel = CLI_FSCAPWAP_ENCRYPTION_DATA;
            if (CapwapGetAllFsCapwapLinkEncryptionTable
                (pCapwapFsCapwapLinkEncryptionEntry) == OSIX_SUCCESS)
            {
                DataStatus =
                    (UINT2) (pCapwapFsCapwapLinkEncryptionEntry->MibObject.
                             i4FsCapwapEncryptChannelStatus);

                if (DataStatus == 0)
                {
                    CliPrintf (CliHandle,
                               "\r\nCAPWAP Data Channel       : Unknown Encryption\n");
                }
                else if (DataStatus == 1)
                {
                    CliPrintf (CliHandle,
                               "\r\nCAPWAP Data Channel       : DTLS Encrypted\n");
                }
                else if (DataStatus == 2)
                {
                    CliPrintf (CliHandle,
                               "\r\nCAPWAP Data Channel       : Plain Text\n");
                }
            }
            else
            {
                CliPrintf (CliHandle,
                           "\r\nCAPWAP Data Channel    : Not configured\n");
            }
            CliPrintf (CliHandle,
                       "\r\n-------------------------------------------\r\n");
        }
        while (nmhGetNextIndexCapwapBaseWtpProfileTable
               (currentProfileId, &nextProfileId) == SNMP_SUCCESS);

    }

    else
    {

        i4OutCome = nmhGetFirstIndexCapwapBaseWtpProfileTable (&nextProfileId);
        if (i4OutCome == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n Entry not found\r\n");
        }

        currentProfileId = nextProfileId;

        if (CapwapGetWtpProfileIdFromProfileName
            (pu1CapwapBaseWtpProfileName, &currentProfileId) != OSIX_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\n Entry not found\r\n");
        }

        CliPrintf (CliHandle,
                   "\r\n**********************************************\r\n");
        CliPrintf (CliHandle, "\r\nAP Name                : %s\r\n",
                   pu1CapwapBaseWtpProfileName);

        pCapwapFsCapwapLinkEncryptionEntry->MibObject.u4CapwapBaseWtpProfileId =
            currentProfileId;
        pCapwapFsCapwapLinkEncryptionEntry->MibObject.i4FsCapwapEncryptChannel =
            CLI_FSCAPWAP_ENCRYPTION_CONTROL;

        if (CapwapGetAllFsCapwapLinkEncryptionTable
            (pCapwapFsCapwapLinkEncryptionEntry) == OSIX_SUCCESS)
        {
            ChannelStatus =
                (UINT2) (pCapwapFsCapwapLinkEncryptionEntry->MibObject.
                         i4FsCapwapEncryptChannelStatus);
            if (ChannelStatus == 0)
            {
                CliPrintf (CliHandle,
                           "\r\nCAPWAP Control Channel    : Unknown Encryption\n");
            }
            else if (ChannelStatus == 1)
            {
                CliPrintf (CliHandle,
                           "\r\nCAPWAP Control Channel    : DTLS Encrypted\n");
            }
            else if (ChannelStatus == 2)
            {
                CliPrintf (CliHandle,
                           "\r\nCAPWAP Control Channel    : Plain Text\n");
            }

        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nCAPWAP Control Channel    : Not configured\n");
        }

        pCapwapFsCapwapLinkEncryptionEntry->MibObject.i4FsCapwapEncryptChannel =
            CLI_FSCAPWAP_ENCRYPTION_DATA;
        if (CapwapGetAllFsCapwapLinkEncryptionTable
            (pCapwapFsCapwapLinkEncryptionEntry) == OSIX_SUCCESS)
        {
            DataStatus =
                (UINT2) (pCapwapFsCapwapLinkEncryptionEntry->MibObject.
                         i4FsCapwapEncryptChannelStatus);
            if (DataStatus == 0)
            {
                CliPrintf (CliHandle,
                           "\r\nCAPWAP Data Channel       : Unknown Encryption\n");
            }
            else if (DataStatus == 1)
            {
                CliPrintf (CliHandle,
                           "\r\nCAPWAP Data Channel       : DTLS Encrypted \n");
            }
            else if (DataStatus == 2)
            {
                CliPrintf (CliHandle,
                           "\r\nCAPWAP Data Channel       : Plain Text\n");
            }

        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nCAPWAP Data Channel    : Not configured\n");
        }

    }

    MemReleaseMemBlock (CAPWAP_FSCAPWAPLINKENCRYPTIONTABLE_POOLID,
                        (UINT1 *) pCapwapFsCapwapLinkEncryptionEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    : CapwapShowDiscoveryDetailedAP
 * Description :
 * Input       :  CliHandle
 *
 *
 * Output      :  None
 * Returns     :  CLI_SUCCESS/CLI_FAILURE
 ****************************************************************************/
INT4
CapwapShowDiscoveryDetailedAP (tCliHandle CliHandle,
                               UINT4 *pau4CapwapBaseWtpProfileId)
{
    UINT4               u4WtpProfileId = 0;
    UINT4               u4LastSuccAtt = 0;
    UINT4               u4LastUnSuccAtt = 0;
    UINT1               au1WtpName[256];
    tSNMP_OCTET_STRING_TYPE WtpProfileName;
    UINT1               au1TimeSuccess[MAX_DATE_LEN];
    UINT1               au1TimeUnSuccess[MAX_DATE_LEN];
#ifdef WLC_WANTED
    UINT4               u4ReqRxd = 0, u4RspSent = 0;
    UINT4               u4ReqNotPros = 0;
    UINT4               u4LastUnsucAttReason = 0;
#endif
#ifdef WTP_WANTED
    UINT4               u4ReqSent = 0, u4RspRxd = 0;
#endif

    MEMSET (au1WtpName, 0, 256);
    MEMSET (&WtpProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1TimeSuccess, 0, MAX_DATE_LEN);
    MEMSET (au1TimeUnSuccess, 0, MAX_DATE_LEN);

    WtpProfileName.pu1_OctetList = au1WtpName;

    u4WtpProfileId = *pau4CapwapBaseWtpProfileId;

    if (nmhGetCapwapBaseWtpProfileName
        (u4WtpProfileId, &WtpProfileName) != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    nmhGetFsCapwapDiscLastSuccAttemptTime (u4WtpProfileId, &u4LastSuccAtt);
    nmhGetFsCapwapDiscLastUnsuccessfulAttemptTime (u4WtpProfileId,
                                                   &u4LastUnSuccAtt);
    if (u4LastSuccAtt != 0)
    {
        CapwapUtilGetTimeForTicks (u4LastSuccAtt, au1TimeSuccess);
    }
    if (u4LastUnSuccAtt != 0)
    {
        CapwapUtilGetTimeForTicks (u4LastUnSuccAtt, au1TimeUnSuccess);
    }

#ifdef WLC_WANTED

    nmhGetFsCapwapDiscReqReceived (u4WtpProfileId, &u4ReqRxd);
    nmhGetFsCapwapDiscRspTransmitted (u4WtpProfileId, &u4RspSent);
    nmhGetFsCapwapDiscunsuccessfulProcessed (u4WtpProfileId, &u4ReqNotPros);
    nmhGetFsCapwapDiscLastUnsuccAttemptReason (u4WtpProfileId,
                                               &u4LastUnsucAttReason);

    CliPrintf (CliHandle,
               "\r\nAP name                                          :   %s",
               WtpProfileName.pu1_OctetList);
    CliPrintf (CliHandle,
               "\r\nDiscovery Requests Received                      :   %d",
               u4ReqRxd);
    CliPrintf (CliHandle,
               "\r\nDiscovery Responses Sent                         :   %d",
               u4RspSent);
    CliPrintf (CliHandle,
               "\r\nDiscovery Unsuccessful Request Processed         :   %d",
               u4ReqNotPros);
    CliPrintf (CliHandle,
               "\r\nDiscovery Reason For Last Unsuccessful Attempt   :   %s",
               "Unknown Failure");
    CliPrintf (CliHandle,
               "\r\nDiscovery Last Successful Attempt Time           :   %-12s",
               au1TimeSuccess);
    CliPrintf (CliHandle,
               "\r\nDiscovery Last Unsuccessful Attempt Time         :   %-12s\n",
               au1TimeUnSuccess);
#endif
#ifdef WTP_WANTED
    nmhGetFsCapwapDiscReqTransmitted (u4WtpProfileId, &u4ReqSent);
    nmhGetFsCapwapDiscRspReceived (u4WtpProfileId, &u4RspRxd);

    CliPrintf (CliHandle,
               "\r\nAP name                                          :   %s",
               WtpProfileName.pu1_OctetList);
    CliPrintf (CliHandle,
               "\r\nDiscovery Requests Sent                          :   %d",
               u4ReqSent);
    CliPrintf (CliHandle,
               "\r\nDiscovery Responses Received                     :   %d",
               u4RspRxd);
    CliPrintf (CliHandle,
               "\r\nDiscovery Last Successful Attempt Time           :   %-12s",
               au1TimeSuccess);
    CliPrintf (CliHandle,
               "\r\nDiscovery Last Unsuccessful Attempt Time         :   %-12s\n",
               au1TimeUnSuccess);
#endif

    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    : CapwapShowDiscoveryDetailed
 * Description :
 * Input       :  CliHandle
 *
 *
 * Output      :  None
 * Returns     :  CLI_SUCCESS/CLI_FAILURE
 ****************************************************************************/
INT4
CapwapShowDiscoveryDetailed (tCliHandle CliHandle,
                             UINT4 *pau4CapwapBaseWtpProfileId)
{
    UINT4               u4WtpProfileId = 0;
    UINT4               u4LastSuccAtt = 0;
    UINT4               u4LastUnSuccAtt = 0;
    UINT1               au1WtpName[256];
    tSNMP_OCTET_STRING_TYPE WtpProfileName;
    UINT1               au1TimeSuccess[MAX_DATE_LEN];
    UINT1               au1TimeUnSuccess[MAX_DATE_LEN];
#ifdef WLC_WANTED
    UINT4               u4ReqRxd = 0, u4RspSent = 0;
    UINT4               u4ReqNotPros = 0;
    UINT4               u4LastUnsucAttReason = 0;
#endif
#ifdef WTP_WANTED
    UINT4               u4ReqSent = 0, u4RspRxd = 0;
#endif

    MEMSET (au1WtpName, 0, 256);
    MEMSET (&WtpProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1TimeSuccess, 0, MAX_DATE_LEN);
    MEMSET (au1TimeUnSuccess, 0, MAX_DATE_LEN);

    WtpProfileName.pu1_OctetList = au1WtpName;

    u4WtpProfileId = *pau4CapwapBaseWtpProfileId;

    if (nmhGetCapwapBaseWtpProfileName
        (u4WtpProfileId, &WtpProfileName) != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    nmhGetFsCapwapDiscLastSuccAttemptTime (u4WtpProfileId, &u4LastSuccAtt);
    nmhGetFsCapwapDiscLastUnsuccessfulAttemptTime (u4WtpProfileId,
                                                   &u4LastUnSuccAtt);
    if (u4LastSuccAtt != 0)
    {
        CapwapUtilGetTimeForTicks (u4LastSuccAtt, au1TimeSuccess);
    }
    if (u4LastUnSuccAtt != 0)
    {
        CapwapUtilGetTimeForTicks (u4LastUnSuccAtt, au1TimeUnSuccess);
    }
#ifdef WLC_WANTED

    nmhGetFsCapwapDiscReqReceived (u4WtpProfileId, &u4ReqRxd);
    nmhGetFsCapwapDiscRspTransmitted (u4WtpProfileId, &u4RspSent);
    nmhGetFsCapwapDiscunsuccessfulProcessed (u4WtpProfileId, &u4ReqNotPros);
    nmhGetFsCapwapDiscLastUnsuccAttemptReason (u4WtpProfileId,
                                               &u4LastUnsucAttReason);

    CliPrintf (CliHandle,
               "\r\nAP name                                          :   %s",
               WtpProfileName.pu1_OctetList);
    CliPrintf (CliHandle,
               "\r\nDiscovery Requests Received                      :   %d",
               u4ReqRxd);
    CliPrintf (CliHandle,
               "\r\nDiscovery Responses Sent                         :   %d",
               u4RspSent);
    CliPrintf (CliHandle,
               "\r\nDiscovery Unsuccessful Request Processed         :   %d",
               u4ReqNotPros);
    CliPrintf (CliHandle,
               "\r\nDiscovery Reason For Last Unsuccessful Attempt   :   %s",
               "Unknown Failure");
    CliPrintf (CliHandle,
               "\r\nDiscovery Last Successful Attempt Time           :   %-12s",
               au1TimeSuccess);
    CliPrintf (CliHandle,
               "\r\nDiscovery Last Unsuccessful Attempt Time         :   %-12s\n",
               au1TimeUnSuccess);
#endif
#ifdef WTP_WANTED
    nmhGetFsCapwapDiscReqTransmitted (u4WtpProfileId, &u4ReqSent);
    nmhGetFsCapwapDiscRspReceived (u4WtpProfileId, &u4RspRxd);

    CliPrintf (CliHandle,
               "\r\nAP name                                          :   %s",
               WtpProfileName.pu1_OctetList);
    CliPrintf (CliHandle,
               "\r\nDiscovery Requests Sent                          :   %d",
               u4ReqSent);
    CliPrintf (CliHandle,
               "\r\nDiscovery Responses Received                     :   %d",
               u4RspRxd);
    CliPrintf (CliHandle,
               "\r\nDiscovery Last Successful Attempt Time           :   %-12s",
               au1TimeSuccess);
    CliPrintf (CliHandle,
               "\r\nDiscovery Last Unsuccessful Attempt Time         :   %-12s\n",
               au1TimeUnSuccess);
#endif

    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    :CapwapShowJoinStatsDetailedAll
 * Description :
 * Input       :  CliHandle
 *
 *
 * Output      :  None
 * Returns     :  CLI_SUCCESS/CLI_FAILURE
 ****************************************************************************/
INT4
CapwapShowJoinStatsDetailedAll (tCliHandle CliHandle,
                                UINT4 *pau4CapwapBaseWtpProfileId)
{
    UINT4               u4WtpProfileId = 0;
    UINT4               u4ReqRxd = 0, u4RspSent = 0;
    UINT4               u4ReqNotPros = 0, u4LastSuccAtt = 0;
    UINT4               u4LastUnsucAttReason = 0, u4LastUnSuccAtt = 0;
    UINT1               au1WtpName[256];
    tSNMP_OCTET_STRING_TYPE WtpProfileName;
    UINT1               au1TimeSuccess[MAX_DATE_LEN];
    UINT1               au1TimeUnSuccess[MAX_DATE_LEN];

    MEMSET (au1WtpName, 0, 256);
    MEMSET (&WtpProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1TimeSuccess, 0, MAX_DATE_LEN);
    MEMSET (au1TimeUnSuccess, 0, MAX_DATE_LEN);

    WtpProfileName.pu1_OctetList = au1WtpName;

    u4WtpProfileId = *pau4CapwapBaseWtpProfileId;

    if (nmhGetCapwapBaseWtpProfileName
        (u4WtpProfileId, &WtpProfileName) != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (nmhGetFsCapwapJoinReqReceived (u4WtpProfileId, &u4ReqRxd) !=
        SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (nmhGetFsCapwapJoinRspTransmitted (u4WtpProfileId, &u4RspSent) !=
        SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (nmhGetFsCapwapJoinunsuccessfulProcessed (u4WtpProfileId, &u4ReqNotPros)
        != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (nmhGetFsCapwapJoinReasonLastUnsuccAttempt
        (u4WtpProfileId, &u4LastUnsucAttReason) != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (nmhGetFsCapwapJoinLastSuccAttemptTime (u4WtpProfileId, &u4LastSuccAtt)
        != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (nmhGetFsCapwapJoinLastUnsuccAttemptTime
        (u4WtpProfileId, &u4LastUnSuccAtt) != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    UtlGetTimeStrForTicks (u4LastSuccAtt, (CHR1 *) au1TimeSuccess);
    UtlGetTimeStrForTicks (u4LastUnSuccAtt, (CHR1 *) au1TimeUnSuccess);

    CliPrintf (CliHandle,
               "\r\nAP name                                     :   %s",
               WtpProfileName.pu1_OctetList);
    CliPrintf (CliHandle,
               "\r\nJoin Requests Received                      :   %d",
               u4ReqRxd);
    CliPrintf (CliHandle,
               "\r\nJoin Responses Sent                         :   %d",
               u4RspSent);
    CliPrintf (CliHandle,
               "\r\nJoin Unsuccessful Request Processed         :   %d",
               u4ReqNotPros);
    CliPrintf (CliHandle,
               "\r\nJoin Reason For Last Unsuccessful Attempt   :   %s",
               "Unknown Failure");
    CliPrintf (CliHandle,
               "\r\nJoin Last Successful Attempt Time           :   %-12s",
               au1TimeSuccess);
    CliPrintf (CliHandle,
               "\r\nJoin Last Unsuccessful Attempt Time         :   %-12s\n",
               au1TimeUnSuccess);

    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    :CapwapShowJoinStatsDetailed
 * Description :
 * Input       :  CliHandle
 *
 *
 * Output      :  None
 * Returns     :  CLI_SUCCESS/CLI_FAILURE
 ****************************************************************************/
INT4
CapwapShowJoinStatsDetailed (tCliHandle CliHandle,
                             UINT4 *pau4CapwapBaseWtpProfileId)
{
    UINT4               u4WtpProfileId = 0;
    UINT4               u4LastSuccAtt = 0;
    UINT4               u4LastUnSuccAtt = 0;
    UINT1               au1WtpName[256];
    tSNMP_OCTET_STRING_TYPE WtpProfileName;
    UINT1               au1TimeSuccess[MAX_DATE_LEN];
    UINT1               au1TimeUnSuccess[MAX_DATE_LEN];
#ifdef WLC_WANTED
    UINT4               u4ReqRxd = 0, u4RspSent = 0;
    UINT4               u4ReqNotPros = 0;
    UINT4               u4LastUnsucAttReason = 0;
#endif
#ifdef WTP_WANTED
    UINT4               u4ReqSent = 0, u4RspRxd = 0;
#endif

    MEMSET (au1WtpName, 0, 256);
    MEMSET (&WtpProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1TimeSuccess, 0, MAX_DATE_LEN);
    MEMSET (au1TimeUnSuccess, 0, MAX_DATE_LEN);

    WtpProfileName.pu1_OctetList = au1WtpName;

    u4WtpProfileId = *pau4CapwapBaseWtpProfileId;

    if (nmhGetCapwapBaseWtpProfileName
        (u4WtpProfileId, &WtpProfileName) != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    nmhGetFsCapwapJoinLastSuccAttemptTime (u4WtpProfileId, &u4LastSuccAtt);
    nmhGetFsCapwapJoinLastUnsuccAttemptTime (u4WtpProfileId, &u4LastUnSuccAtt);

    if (u4LastSuccAtt != 0)
    {
        CapwapUtilGetTimeForTicks (u4LastSuccAtt, au1TimeSuccess);
    }
    if (u4LastUnSuccAtt != 0)
    {
        CapwapUtilGetTimeForTicks (u4LastUnSuccAtt, au1TimeUnSuccess);
    }

#ifdef WLC_WANTED

    nmhGetFsCapwapJoinReqReceived (u4WtpProfileId, &u4ReqRxd);
    nmhGetFsCapwapJoinRspTransmitted (u4WtpProfileId, &u4RspSent);
    nmhGetFsCapwapJoinunsuccessfulProcessed (u4WtpProfileId, &u4ReqNotPros);
    nmhGetFsCapwapJoinReasonLastUnsuccAttempt (u4WtpProfileId,
                                               &u4LastUnsucAttReason);

    CliPrintf (CliHandle,
               "\r\nAP name                                     :   %s",
               WtpProfileName.pu1_OctetList);
    CliPrintf (CliHandle,
               "\r\nJoin Requests Received                      :   %d",
               u4ReqRxd);
    CliPrintf (CliHandle,
               "\r\nJoin Responses Sent                         :   %d",
               u4RspSent);
    CliPrintf (CliHandle,
               "\r\nJoin Unsuccessful Request Processed         :   %d",
               u4ReqNotPros);
    CliPrintf (CliHandle,
               "\r\nJoin Reason For Last Unsuccessful Attempt   :   %s",
               "Unknown Failure");
    CliPrintf (CliHandle,
               "\r\nJoin Last Successful Attempt Time           :   %-12s",
               au1TimeSuccess);
    CliPrintf (CliHandle,
               "\r\nJoin Last Unsuccessful Attempt Time         :   %-12s\n",
               au1TimeUnSuccess);
#endif
#ifdef WTP_WANTED
    nmhGetFsCapwapJoinReqTransmitted (u4WtpProfileId, &u4ReqSent);
    nmhGetFsCapwapJoinRspReceived (u4WtpProfileId, &u4RspRxd);

    CliPrintf (CliHandle,
               "\r\nAP name                                     :   %s",
               WtpProfileName.pu1_OctetList);
    CliPrintf (CliHandle,
               "\r\nJoin Requests Sent                          :   %d",
               u4ReqSent);
    CliPrintf (CliHandle,
               "\r\nJoin Responses Received                     :   %d",
               u4RspRxd);
    CliPrintf (CliHandle,
               "\r\nJoin Last Successful Attempt Time           :   %-12s",
               au1TimeSuccess);
    CliPrintf (CliHandle,
               "\r\nJoin Last Unsuccessful Attempt Time         :   %-12s\n",
               au1TimeUnSuccess);
#endif

    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    : CapwapShowConfigStatsDetailedAll
 * Description :
 * Input       :  CliHandle
 *
 *
 * Output      :  None
 * Returns     :  CLI_SUCCESS/CLI_FAILURE
 ****************************************************************************/
INT4
CapwapShowConfigStatsDetailedAll (tCliHandle CliHandle,
                                  UINT4 *pau4CapwapBaseWtpProfileId)
{
    UINT4               u4WtpProfileId = 0;
    UINT4               u4ReqRxd = 0, u4RspSent = 0;
    UINT4               u4ReqNotPros = 0, u4LastSuccAtt = 0;
    UINT4               u4LastUnsucAttReason = 0, u4LastUnSuccAtt = 0;
    UINT1               au1WtpName[256];
    tSNMP_OCTET_STRING_TYPE WtpProfileName;
    UINT1               au1TimeSuccess[MAX_DATE_LEN];
    UINT1               au1TimeUnSuccess[MAX_DATE_LEN];

    MEMSET (au1WtpName, 0, 256);
    MEMSET (&WtpProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1TimeSuccess, 0, MAX_DATE_LEN);
    MEMSET (au1TimeUnSuccess, 0, MAX_DATE_LEN);

    WtpProfileName.pu1_OctetList = au1WtpName;

    u4WtpProfileId = *pau4CapwapBaseWtpProfileId;

    if (nmhGetCapwapBaseWtpProfileName
        (u4WtpProfileId, &WtpProfileName) != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (nmhGetFsCapwapConfigReqReceived (u4WtpProfileId, &u4ReqRxd) !=
        SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (nmhGetFsCapwapConfigRspTransmitted (u4WtpProfileId, &u4RspSent) !=
        SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (nmhGetFsCapwapConfigunsuccessfulProcessed
        (u4WtpProfileId, &u4ReqNotPros) != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (nmhGetFsCapwapConfigReasonLastUnsuccAttempt
        (u4WtpProfileId, &u4LastUnsucAttReason) != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (nmhGetFsCapwapConfigLastSuccAttemptTime
        (u4WtpProfileId, &u4LastSuccAtt) != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (nmhGetFsCapwapConfigLastUnsuccessfulAttemptTime
        (u4WtpProfileId, &u4LastUnSuccAtt) != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    UtlGetTimeStrForTicks (u4LastSuccAtt, (CHR1 *) au1TimeSuccess);
    UtlGetTimeStrForTicks (u4LastUnSuccAtt, (CHR1 *) au1TimeUnSuccess);

    CliPrintf (CliHandle,
               "\r\nAP name                                       :   %s",
               WtpProfileName.pu1_OctetList);
    CliPrintf (CliHandle,
               "\r\nConfig Requests Received                      :   %d",
               u4ReqRxd);
    CliPrintf (CliHandle,
               "\r\nConfig Responses Sent                         :   %d",
               u4RspSent);
    CliPrintf (CliHandle,
               "\r\nConfig Unsuccessful Request Processed         :   %d",
               u4ReqNotPros);
    CliPrintf (CliHandle,
               "\r\nConfig Reason For Last Unsuccessful Attempt   :   %s",
               "Unknown Failure");
    CliPrintf (CliHandle,
               "\r\nConfig Last Successful Attempt Time           :   %-12s",
               au1TimeSuccess);
    CliPrintf (CliHandle,
               "\r\nConfig Last Unsuccessful Attempt Time         :   %-12s\n",
               au1TimeUnSuccess);

    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    : CapwapShowConfigStatsDetailed
 * Description :
 * Input       :  CliHandle
 *
 *
 * Output      :  None
 * Returns     :  CLI_SUCCESS/CLI_FAILURE
 ****************************************************************************/
INT4
CapwapShowConfigStatsDetailed (tCliHandle CliHandle,
                               UINT4 *pau4CapwapBaseWtpProfileId)
{
    UINT4               u4WtpProfileId = 0;
    UINT4               u4LastSuccAtt = 0;
    UINT4               u4LastUnSuccAtt = 0;
    UINT1               au1WtpName[256];
    tSNMP_OCTET_STRING_TYPE WtpProfileName;
    UINT1               au1TimeSuccess[MAX_DATE_LEN];
    UINT1               au1TimeUnSuccess[MAX_DATE_LEN];
#ifdef WLC_WANTED
    UINT4               u4ReqRxd = 0, u4RspSent = 0;
    UINT4               u4ReqNotPros = 0;
    UINT4               u4LastUnsucAttReason = 0;
#endif
#ifdef WTP_WANTED
    UINT4               u4ReqSent = 0, u4RspRxd = 0;
#endif

    MEMSET (au1WtpName, 0, 256);
    MEMSET (&WtpProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1TimeSuccess, 0, MAX_DATE_LEN);
    MEMSET (au1TimeUnSuccess, 0, MAX_DATE_LEN);

    WtpProfileName.pu1_OctetList = au1WtpName;

    u4WtpProfileId = *pau4CapwapBaseWtpProfileId;

    if (nmhGetCapwapBaseWtpProfileName
        (u4WtpProfileId, &WtpProfileName) != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    nmhGetFsCapwapConfigLastSuccAttemptTime (u4WtpProfileId, &u4LastSuccAtt);
    nmhGetFsCapwapConfigLastUnsuccessfulAttemptTime (u4WtpProfileId,
                                                     &u4LastUnSuccAtt);

    if (u4LastSuccAtt != 0)
    {
        CapwapUtilGetTimeForTicks (u4LastSuccAtt, au1TimeSuccess);
    }
    if (u4LastUnSuccAtt != 0)
    {
        CapwapUtilGetTimeForTicks (u4LastUnSuccAtt, au1TimeUnSuccess);
    }

#ifdef WLC_WANTED

    nmhGetFsCapwapConfigReqReceived (u4WtpProfileId, &u4ReqRxd);
    nmhGetFsCapwapConfigRspTransmitted (u4WtpProfileId, &u4RspSent);
    nmhGetFsCapwapConfigunsuccessfulProcessed (u4WtpProfileId, &u4ReqNotPros);
    nmhGetFsCapwapConfigReasonLastUnsuccAttempt (u4WtpProfileId,
                                                 &u4LastUnsucAttReason);

    CliPrintf (CliHandle,
               "\r\nAP name                                       :   %s",
               WtpProfileName.pu1_OctetList);
    CliPrintf (CliHandle,
               "\r\nConfig Requests Received                      :   %d",
               u4ReqRxd);
    CliPrintf (CliHandle,
               "\r\nConfig Responses Sent                         :   %d",
               u4RspSent);
    CliPrintf (CliHandle,
               "\r\nConfig Unsuccessful Request Processed         :   %d",
               u4ReqNotPros);
    CliPrintf (CliHandle,
               "\r\nConfig Reason For Last Unsuccessful Attempt   :   %s",
               "Unknown Failure");
    CliPrintf (CliHandle,
               "\r\nConfig Last Successful Attempt Time           :   %-12s",
               au1TimeSuccess);
    CliPrintf (CliHandle,
               "\r\nConfig Last Unsuccessful Attempt Time         :   %-12s\n",
               au1TimeUnSuccess);
#endif
#ifdef WTP_WANTED
    nmhGetFsCapwapConfigReqTransmitted (u4WtpProfileId, &u4ReqSent);
    nmhGetFsCapwapConfigRspReceived (u4WtpProfileId, &u4RspRxd);

    CliPrintf (CliHandle,
               "\r\nAP name                                       :   %s",
               WtpProfileName.pu1_OctetList);
    CliPrintf (CliHandle,
               "\r\nConfig Requests Received                      :   %d",
               u4ReqSent);
    CliPrintf (CliHandle,
               "\r\nConfig Responses Sent                         :   %d",
               u4RspRxd);
    CliPrintf (CliHandle,
               "\r\nConfig Last Successful Attempt Time           :   %-12s",
               au1TimeSuccess);
    CliPrintf (CliHandle,
               "\r\nConfig Last Unsuccessful Attempt Time         :   %-12s\n",
               au1TimeUnSuccess);
#endif
    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    : CapwapShowrunstats
 * Description :
 * Input       :  CliHandle
 *
 * Output      :  None
 * Returns     :  CLI_SUCCESS/CLI_FAILURE
 ****************************************************************************/
INT4
CapwapShowrunstats (tCliHandle CliHandle, UINT4 *pau4CapwapBaseWtpProfileId)
{
    tCapwapFsCawapRunStatsEntry *pCapwapFsCawapRunStatsEntry = NULL;
    UINT4               u4WtpProfileId = 0;
    UINT4               keepAliveMsgRcvd = 0;
    UINT4               u4WtpInternalId;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT1               au1WtpName[256];
    tSNMP_OCTET_STRING_TYPE WtpProfileName;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        pCapwapFsCawapRunStatsEntry =
        (tCapwapFsCawapRunStatsEntry *)
        MemAllocMemBlk (CAPWAP_FSCAWAPRUNSTATSTABLE_POOLID);

    if (pCapwapFsCawapRunStatsEntry == NULL)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return CLI_FAILURE;
    }
    MEMSET (au1WtpName, 0, 256);
    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&WtpProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (pCapwapFsCawapRunStatsEntry, 0,
            sizeof (tCapwapFsCawapRunStatsEntry));
    WtpProfileName.pu1_OctetList = au1WtpName;

    u4WtpProfileId = *pau4CapwapBaseWtpProfileId;

    pWssIfCapwapDB->CapwapGetDB.u2ProfileId = (UINT2) u4WtpProfileId;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpProfileId = OSIX_TRUE;

    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_INTERNAL_ID, pWssIfCapwapDB) !=
        OSIX_SUCCESS)
    {

        MemReleaseMemBlock (CAPWAP_FSCAWAPRUNSTATSTABLE_POOLID,
                            (UINT1 *) pCapwapFsCawapRunStatsEntry);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    u4WtpInternalId = (UINT4) pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId;

    if (nmhGetCapwapBaseWtpProfileName (u4WtpProfileId, &WtpProfileName) !=
        SNMP_SUCCESS)
    {
        MemReleaseMemBlock (CAPWAP_FSCAWAPRUNSTATSTABLE_POOLID,
                            (UINT1 *) pCapwapFsCawapRunStatsEntry);

        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    if (nmhGetFsCapwapRunStatsKeepAliveMsgReceived (u4WtpInternalId,
                                                    (INT4 *) &keepAliveMsgRcvd)
        != SNMP_SUCCESS)
    {
        MemReleaseMemBlock (CAPWAP_FSCAWAPRUNSTATSTABLE_POOLID,
                            (UINT1 *) pCapwapFsCawapRunStatsEntry);

        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    pCapwapFsCawapRunStatsEntry->MibObject.u4CapwapBaseWtpProfileId =
        u4WtpProfileId;

    if (CapwapGetAllFsCawapRunStatsTable (pCapwapFsCawapRunStatsEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (CAPWAP_FSCAWAPRUNSTATSTABLE_POOLID,
                            (UINT1 *) pCapwapFsCawapRunStatsEntry);

        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    CliPrintf (CliHandle,
               "\r\nAP name                                          :   %-15s\n",
               WtpProfileName.pu1_OctetList);
    CliPrintf (CliHandle,
               "\r\nKeep alive packets received........              :   %d",
               keepAliveMsgRcvd);

    CliPrintf (CliHandle,
               "\r\nConfig update Requests Received                  :   %d",
               pCapwapFsCawapRunStatsEntry->MibObject.
               u4FsCapwapRunConfigUpdateReqReceived);
    CliPrintf (CliHandle,
               "\r\nConfig update Responses Sent                     :   %d",
               pCapwapFsCawapRunStatsEntry->MibObject.
               u4FsCapwapRunConfigUpdateRspTransmitted);
    CliPrintf (CliHandle,
               "\r\nConfig update Responses Received                 :   %d",
               pCapwapFsCawapRunStatsEntry->MibObject.
               u4FsCapwapRunConfigUpdateRspReceived);
    CliPrintf (CliHandle,
               "\r\nConfig update Requests Sent                      :   %d",
               pCapwapFsCawapRunStatsEntry->MibObject.
               u4FsCapwapRunConfigUpdateReqTransmitted);
    CliPrintf (CliHandle,
               "\r\nConfig Unsuccessful Request Processed            :   %d",
               pCapwapFsCawapRunStatsEntry->MibObject.
               u4FsCapwapRunConfigUpdateunsuccessfulProcessed);
    CliPrintf (CliHandle,
               "\r\nConfig Reason For Last Unsuccessful Attempt      :   %s",
               "Unknown Failure");
    CliPrintf (CliHandle,
               "\r\nConfig Last Successful Attempt Time              :   %-12s",
               pCapwapFsCawapRunStatsEntry->MibObject.
               au1FsCapwapRunConfigUpdateLastSuccAttemptTime);
    CliPrintf (CliHandle,
               "\r\nConfig Last Unsuccessful Attempt Time            :   %-12s\n",
               pCapwapFsCawapRunStatsEntry->MibObject.
               au1FsCapwapRunConfigUpdateLastUnsuccessfulAttemptTime);

    CliPrintf (CliHandle,
               "\r\nEcho Requests Received                           :   %d",
               pCapwapFsCawapRunStatsEntry->MibObject.
               u4FsCapwapRunEchoReqReceived);
    CliPrintf (CliHandle,
               "\r\nEcho Responses Sent                              :   %d",
               pCapwapFsCawapRunStatsEntry->MibObject.
               u4FsCapwapRunEchoRspTransmitted);
    CliPrintf (CliHandle,
               "\r\nEcho Responses Received                          :   %d",
               pCapwapFsCawapRunStatsEntry->MibObject.
               u4FsCapwapRunEchoRspReceived);
    CliPrintf (CliHandle,
               "\r\nEcho Requests Sent                               :   %d",
               pCapwapFsCawapRunStatsEntry->MibObject.
               u4FsCapwapRunEchoReqTransmitted);
    CliPrintf (CliHandle,
               "\r\nEcho Unsuccessful Request Processed              :   %d",
               pCapwapFsCawapRunStatsEntry->MibObject.
               u4FsCapwapRunEchounsuccessfulProcessed);
    CliPrintf (CliHandle,
               "\r\nEcho Reason For Last Unsuccessful Attempt        :   %s",
               "Unknown Failure");
    CliPrintf (CliHandle,
               "\r\nEcho Last Successful Attempt Time                :   %-12s",
               pCapwapFsCawapRunStatsEntry->MibObject.
               au1FsCapwapRunEchoLastSuccAttemptTime);
    CliPrintf (CliHandle,
               "\r\nEcho Last Unsuccessful Attempt Time              :   %-12s\n",
               pCapwapFsCawapRunStatsEntry->MibObject.
               au1FsCapwapRunEchoLastUnsuccessfulAttemptTime);

    CliPrintf (CliHandle,
               "\r\nPrimary Discovery Requests Received              :   %d",
               pCapwapFsCawapRunStatsEntry->MibObject.
               u4FsCapwapRunPriDiscReqReceived);
    CliPrintf (CliHandle,
               "\r\nPrimary Discovery Responses Sent                 :   %d",
               pCapwapFsCawapRunStatsEntry->MibObject.
               u4FsCapwapRunPriDiscRspTransmitted);
    CliPrintf (CliHandle,
               "\r\nPrimary Discovery  Responses Received            :   %d",
               pCapwapFsCawapRunStatsEntry->MibObject.
               u4FsCapwapRunPriDiscRspReceived);
    CliPrintf (CliHandle,
               "\r\nPrimary Discovery Requests Sent                  :   %d",
               pCapwapFsCawapRunStatsEntry->MibObject.
               u4FsCapwapRunPriDiscReqTransmitted);
    CliPrintf (CliHandle,
               "\r\nPrimary Discovery Unsuccessful Request Processed :   %d",
               pCapwapFsCawapRunStatsEntry->MibObject.
               u4FsCapwapRunPriDiscunsuccessfulProcessed);
    CliPrintf (CliHandle,
               "\r\nPriDisc Reason For Last Unsuccessful Attempt     :   %s",
               "Unknown Failure");
    CliPrintf (CliHandle,
               "\r\nPriDisc Last Successful Attempt Time             :   %-12s",
               pCapwapFsCawapRunStatsEntry->MibObject.
               au1FsCapwapRunPriDiscLastSuccAttemptTime);
    CliPrintf (CliHandle,
               "\r\nPriDisc Last Unsuccessful Attempt Time           :   %-12s\n",
               pCapwapFsCawapRunStatsEntry->MibObject.
               au1FsCapwapRunPriDiscLastUnsuccessfulAttemptTime);

    CliPrintf (CliHandle,
               "\r\nStation Config Update Requests Received          :   %d",
               pCapwapFsCawapRunStatsEntry->MibObject.
               u4FsCapwapRunStationConfigReqReceived);
    CliPrintf (CliHandle,
               "\r\nStation Config Update Responses Sent             :   %d",
               pCapwapFsCawapRunStatsEntry->MibObject.
               u4FsCapwapRunStationConfigRspTransmitted);
    CliPrintf (CliHandle,
               "\r\nStation Config Update Responses Received         :   %d",
               pCapwapFsCawapRunStatsEntry->MibObject.
               u4FsCapwapRunStationConfigRspReceived);
    CliPrintf (CliHandle,
               "\r\nStation Config Update Requests Sent              :   %d",
               pCapwapFsCawapRunStatsEntry->MibObject.
               u4FsCapwapRunStationConfigReqTransmitted);
    CliPrintf (CliHandle,
               "\r\nStation Config Unsuccessful Request Processed    :   %d",
               pCapwapFsCawapRunStatsEntry->MibObject.
               u4FsCapwapRunStationConfigunsuccessfulProcessed);
    CliPrintf (CliHandle,
               "\r\nSta Config Reason For Last Unsuccessful Attempt  :   %s",
               "Unknown Failure");
    CliPrintf (CliHandle,
               "\r\nStation Config Last Successful Attempt Time      :   %-12s",
               pCapwapFsCawapRunStatsEntry->MibObject.
               au1FsCapwapRunStationConfigLastSuccAttemptTime);
    CliPrintf (CliHandle,
               "\r\nStation Config Last Unsuccessful Attempt Time    :   %-12s\n",
               pCapwapFsCawapRunStatsEntry->MibObject.
               au1FsCapwapRunStationConfigLastUnsuccessfulAttemptTime);

    CliPrintf (CliHandle,
               "\r\nClear Config Update Requests Received            :   %d",
               pCapwapFsCawapRunStatsEntry->MibObject.
               u4FsCapwapRunClearConfigReqReceived);
    CliPrintf (CliHandle,
               "\r\nClear Config Update Responses Sent               :   %d",
               pCapwapFsCawapRunStatsEntry->MibObject.
               u4FsCapwapRunClearConfigRspTransmitted);
    CliPrintf (CliHandle,
               "\r\nClear Config Update Responses Received           :   %d",
               pCapwapFsCawapRunStatsEntry->MibObject.
               u4FsCapwapRunClearConfigRspReceived);
    CliPrintf (CliHandle,
               "\r\nClear Config Update Requests Sent                :   %d",
               pCapwapFsCawapRunStatsEntry->MibObject.
               u4FsCapwapRunClearConfigReqTransmitted);
    CliPrintf (CliHandle,
               "\r\nClear Config Unsuccessful Request Processed      :   %d",
               pCapwapFsCawapRunStatsEntry->MibObject.
               u4FsCapwapRunClearConfigunsuccessfulProcessed);
    CliPrintf (CliHandle,
               "\r\nClear Config Reason For Last Unsuccessful Attempt:   %s",
               "Unknown Failure");
    CliPrintf (CliHandle,
               "\r\nClear Config Last Successful Attempt Time        :   %-12s",
               pCapwapFsCawapRunStatsEntry->MibObject.
               au1FsCapwapRunClearConfigLastSuccAttemptTime);
    CliPrintf (CliHandle,
               "\r\nClear Config Last Unsuccessful Attempt Time      :   %-12s\n",
               pCapwapFsCawapRunStatsEntry->MibObject.
               au1FsCapwapRunClearConfigLastUnsuccessfulAttemptTime);

    CliPrintf (CliHandle,
               "\r\nData Transfer Requests Received                  :   %d",
               pCapwapFsCawapRunStatsEntry->MibObject.
               u4FsCapwapRunDataTransferReqReceived);
    CliPrintf (CliHandle,
               "\r\nData Transfer Responses Sent                     :   %d",
               pCapwapFsCawapRunStatsEntry->MibObject.
               u4FsCapwapRunDataTransferRspTransmitted);
    CliPrintf (CliHandle,
               "\r\nData Transfer Responses Received                 :   %d",
               pCapwapFsCawapRunStatsEntry->MibObject.
               u4FsCapwapRunDataTransferRspReceived);
    CliPrintf (CliHandle,
               "\r\nData Transfer Requests Sent                      :   %d",
               pCapwapFsCawapRunStatsEntry->MibObject.
               u4FsCapwapRunDataTransferReqTransmitted);
    CliPrintf (CliHandle,
               "\r\nData Transfer Unsuccessful Request Processed     :   %d",
               pCapwapFsCawapRunStatsEntry->MibObject.
               u4FsCapwapRunDataTransferunsuccessfulProcessed);
    CliPrintf (CliHandle,
               "\r\nDataTransfer Reason For Last Unsuccessful Attempt:   %s",
               "Unknown Failure");
    CliPrintf (CliHandle,
               "\r\nData Transfer Last Successful Attempt Time       :   %-12s",
               pCapwapFsCawapRunStatsEntry->MibObject.
               au1FsCapwapRunDataTransferLastSuccAttemptTime);
    CliPrintf (CliHandle,
               "\r\nData Transfer Last Unsuccessful Attempt Time     :   %-12s\n",
               pCapwapFsCawapRunStatsEntry->MibObject.
               au1FsCapwapRunDataTransferLastUnsuccessfulAttemptTime);

    CliPrintf (CliHandle,
               "\r\nReset Requests Received                          :   %d",
               pCapwapFsCawapRunStatsEntry->MibObject.
               u4FsCapwapRunResetReqReceived);
    CliPrintf (CliHandle,
               "\r\nReset Responses Sent                             :   %d",
               pCapwapFsCawapRunStatsEntry->MibObject.
               u4FsCapwapRunResetRspTransmitted);
    CliPrintf (CliHandle,
               "\r\nReset Responses Received                         :   %d",
               pCapwapFsCawapRunStatsEntry->MibObject.
               u4FsCapwapRunResetRspReceived);
    CliPrintf (CliHandle,
               "\r\nReset Requests Sent                              :   %d",
               pCapwapFsCawapRunStatsEntry->MibObject.
               u4FsCapwapRunResetReqTransmitted);
    CliPrintf (CliHandle,
               "\r\nReset Unsuccessful Request Processed             :   %d",
               pCapwapFsCawapRunStatsEntry->MibObject.
               u4FsCapwapRunResetunsuccessfulProcessed);
    CliPrintf (CliHandle,
               "\r\nReset Reason For Last Unsuccessful Attempt       :   %s",
               "Unknown Failure");
    CliPrintf (CliHandle,
               "\r\nReset Last Successful Attempt Time               :   %-12s",
               pCapwapFsCawapRunStatsEntry->MibObject.
               au1FsCapwapRunResetLastSuccAttemptTime);
    CliPrintf (CliHandle,
               "\r\nReset Last Unsuccessful Attempt Time             :   %-12s\n",
               pCapwapFsCawapRunStatsEntry->MibObject.
               au1FsCapwapRunResetLastUnsuccessfulAttemptTime);

    CliPrintf (CliHandle, "\r\n");
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    MemReleaseMemBlock (CAPWAP_FSCAWAPRUNSTATSTABLE_POOLID,
                        (UINT1 *) pCapwapFsCawapRunStatsEntry);

    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    : CapwapShowAPStats
 * Description :
 * Input       :  CliHandle
 *
 * Output      :  None
 * Returns     :  CLI_SUCCESS/CLI_FAILURE
 ****************************************************************************/
INT4
CapwapShowAPStats (tCliHandle CliHandle, UINT4 u4WtpProfileId)
{
    UNUSED_PARAM (CliHandle);
    UINT2               u2WtpInternalId = 0;
    UINT4               u4discReqSent = 0, u4discRspReceived =
        0, u4JoinReqSent = 0, u4JoinRespReceived = 0, u4CfgReqSent =
        0, u4CfgRespReceived = 0, keepAliveMsgRcvd = 0;
    UINT4               RebootCount = 0, InitCount = 0, LinkFailureCount =
        0, SwFailureCount = 0, HwFailureCount = 0, OtherFailureCount =
        0, UnknownFailure = 0, LastFailureType = 0;
    UINT4               radioResetCount = 0, radioSwFailure =
        0, radioHwFailure = 0, radioOtherFailure = 0, radioUnknownFailure =
        0, CfgUpdateCount = 0, ChannelChangeCount = 0, BandChangeCount =
        0, CurrNoiseFloor = 0;
    UINT4               u4OutCome = 0;
    INT4                i4IfIndex = 0;
    UINT4               currentProfileId = 0, nextProfileId = 0;
    UINT4               u4currentBindingId = 0, u4nextBindingId = 0;

    UINT4               u4Dot11TransmittedFragmentCount =
        0, u4Dot11MulticastTransmittedFrameCount = 0, u4Dot11FailedCount =
        0, u4Dot11MultipleRetryCount = 0, u4Dot11FrameDuplicateCount =
        0, u4Dot11RTSSuccessCount = 0, u4Dot11RTSFailureCount =
        0, u4Dot11ACKFailureCount = 0, u4Dot11ReceivedFragmentCount =
        0, u4Dot11MulticastReceivedFrameCount = 0, u4Dot11FCSErrorCount =
        0, u4Dot11TransmittedFrameCount = 0, u4Dot11WEPUndecryptableCount =
        0, u4Dot11QosDiscardedFragmentCount = 0, u4Dot11AssociatedStationCount =
        0, u4Dot11QosCFPollsReceivedCount = 0, u4Dot11QosCFPollsUnusedCount =
        0, u4Dot11QosCFPollsUnusableCount = 0;

    UINT1               au1WtpName[256], au1WtpMacAddr[256];
    tWssIfCapDB        *pWssIfCapwapDB = NULL;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (au1WtpName, 0, 256);
    MEMSET (au1WtpMacAddr, 0, 256);
    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    tSNMP_OCTET_STRING_TYPE WtpMacAddr;
    tSNMP_OCTET_STRING_TYPE WtpProfileName;
    MEMSET (&WtpProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&WtpMacAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    WtpProfileName.pu1_OctetList = au1WtpName;
    WtpMacAddr.pu1_OctetList = au1WtpMacAddr;

    if (nmhGetCapwapBaseWtpProfileName (u4WtpProfileId, &WtpProfileName) !=
        SNMP_SUCCESS)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    pWssIfCapwapDB->CapwapGetDB.u2ProfileId = (UINT2) u4WtpProfileId;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpProfileId = OSIX_TRUE;

    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_INTERNAL_ID, pWssIfCapwapDB) !=
        OSIX_SUCCESS)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FSM_TRC, "Getting Internal Id Failed\r\n");
        return OSIX_FAILURE;
    }

    u2WtpInternalId = pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId;

#ifdef WLC_WANTED
    nmhGetFsCapwapDiscReqReceived (u4WtpProfileId, &u4discReqSent);
    nmhGetFsCapwapDiscRspTransmitted (u4WtpProfileId, &u4discRspReceived);
    nmhGetFsCapwapJoinReqReceived (u4WtpProfileId, &u4JoinReqSent);
    nmhGetFsCapwapJoinRspTransmitted (u4WtpProfileId, &u4JoinRespReceived);
    nmhGetFsCapwapConfigReqReceived (u4WtpProfileId, &u4CfgReqSent);
    nmhGetFsCapwapConfigRspTransmitted (u4WtpProfileId, &u4CfgRespReceived);
    nmhGetFsCapwapRunStatsKeepAliveMsgReceived (u2WtpInternalId,
                                                (INT4 *) &keepAliveMsgRcvd);
    if (nmhGetCapwapBaseWtpProfileWtpMacAddress (u4WtpProfileId, &WtpMacAddr)
        != SNMP_SUCCESS)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;

    }

    nmhGetCapwapBaseWtpEventsStatsRebootCount (&WtpMacAddr, &RebootCount);
    nmhGetCapwapBaseWtpEventsStatsInitCount (&WtpMacAddr, &InitCount);
    nmhGetCapwapBaseWtpEventsStatsLinkFailureCount (&WtpMacAddr,
                                                    &LinkFailureCount);
    nmhGetCapwapBaseWtpEventsStatsSwFailureCount (&WtpMacAddr, &SwFailureCount);
    nmhGetCapwapBaseWtpEventsStatsHwFailureCount (&WtpMacAddr, &HwFailureCount);
    nmhGetCapwapBaseWtpEventsStatsOtherFailureCount (&WtpMacAddr,
                                                     &OtherFailureCount);
    nmhGetCapwapBaseWtpEventsStatsUnknownFailureCount (&WtpMacAddr,
                                                       &UnknownFailure);
    nmhGetCapwapBaseWtpEventsStatsLastFailureType (&WtpMacAddr,
                                                   (INT4 *) &LastFailureType);
#endif
#ifdef WTP_WANTED
    nmhGetFsCapwapDiscReqTransmitted (u4WtpProfileId, &u4discReqSent);
    nmhGetFsCapwapDiscRspReceived (u4WtpProfileId, &u4discRspReceived);
    nmhGetFsCapwapJoinReqTransmitted (u4WtpProfileId, &u4JoinReqSent);
    nmhGetFsCapwapJoinRspReceived (u4WtpProfileId, &u4JoinRespReceived);
    nmhGetFsCapwapConfigReqTransmitted (u4WtpProfileId, &u4CfgReqSent);
    nmhGetFsCapwapConfigRspReceived (u4WtpProfileId, &u4CfgRespReceived);
    nmhGetFsCapwapRunStatsKeepAliveMsgReceived (u2WtpInternalId,
                                                (INT4 *) &keepAliveMsgRcvd);
    nmhGetFsCapwapWtpRebootStatisticsRebootCount (u4WtpProfileId, &RebootCount);
    nmhGetFsCapwapWtpRebootStatisticsAcInitiatedCount (u4WtpProfileId,
                                                       &InitCount);
    nmhGetFsCapwapWtpRebootStatisticsLinkFailureCount (u4WtpProfileId,
                                                       &LinkFailureCount);
    nmhGetFsCapwapWtpRebootStatisticsSwFailureCount (u4WtpProfileId,
                                                     &SwFailureCount);
    nmhGetFsCapwapWtpRebootStatisticsHwFailureCount (u4WtpProfileId,
                                                     &HwFailureCount);
    nmhGetFsCapwapWtpRebootStatisticsOtherFailureCount (u4WtpProfileId,
                                                        &OtherFailureCount);
    nmhGetFsCapwapWtpRebootStatisticsUnknownFailureCount (u4WtpProfileId,
                                                          &UnknownFailure);
    nmhGetFsCapwapWtpRebootStatisticsLastFailureType (u4WtpProfileId,
                                                      (INT4 *)
                                                      &LastFailureType);
#endif
    CliPrintf (CliHandle, "\r\nAP Name                             :%s",
               WtpProfileName.pu1_OctetList);
    CliPrintf (CliHandle, "\r\nDiscovery requests sent             :%d",
               u4discReqSent);
    CliPrintf (CliHandle, "\r\nDiscovery responses received        :%d",
               u4discRspReceived);
    CliPrintf (CliHandle, "\r\nJoin requests sent                  :%d",
               u4JoinReqSent);
    CliPrintf (CliHandle, "\r\nJoin responses received             :%d",
               u4JoinRespReceived);
    CliPrintf (CliHandle, "\r\nConfig requests sent                :%d",
               u4CfgReqSent);
    CliPrintf (CliHandle, "\r\nConfig responses received           :%d",
               u4CfgRespReceived);
    CliPrintf (CliHandle, "\r\nKeep alive packets received         :%d",
               keepAliveMsgRcvd);

    CliPrintf (CliHandle, "\r\n\nReboot count                        :%d",
               RebootCount);
    CliPrintf (CliHandle, "\r\nAC Initiated Count                  :%d",
               InitCount);
    CliPrintf (CliHandle, "\r\nLink Failure Count                  :%d",
               LinkFailureCount);
    CliPrintf (CliHandle, "\r\nSofware Failure Count               :%d",
               SwFailureCount);
    CliPrintf (CliHandle, "\r\nHardware Failure Count              :%d",
               HwFailureCount);
    CliPrintf (CliHandle, "\r\nOther Failure Count                 :%d",
               OtherFailureCount);
    CliPrintf (CliHandle, "\r\nUnknown Failure Count               :%d",
               UnknownFailure);
    CliPrintf (CliHandle, "\r\nLast Failure Type                   :%d",
               LastFailureType);

    u4OutCome = (UINT4) nmhGetFirstIndexFsCapwapWirelessBindingTable
        (&nextProfileId, &u4nextBindingId);
    if (u4OutCome == SNMP_FAILURE)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    do
    {
        currentProfileId = nextProfileId;
        u4currentBindingId = u4nextBindingId;
        if (currentProfileId != u4WtpProfileId)
        {
            continue;
        }
        if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
            (nextProfileId, u4nextBindingId, &i4IfIndex) != SNMP_SUCCESS)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }

#ifdef WLC_WANTED
        nmhGetCapwapBaseRadioEventsStatsResetCount (&WtpMacAddr,
                                                    u4currentBindingId,
                                                    &radioResetCount);
        nmhGetCapwapBaseRadioEventsStatsSwFailureCount (&WtpMacAddr,
                                                        u4currentBindingId,
                                                        &radioSwFailure);
        nmhGetCapwapBaseRadioEventsStatsHwFailureCount (&WtpMacAddr,
                                                        u4currentBindingId,
                                                        &radioHwFailure);
        nmhGetCapwapBaseRadioEventsStatsOtherFailureCount (&WtpMacAddr,
                                                           u4currentBindingId,
                                                           &radioOtherFailure);
        nmhGetCapwapBaseRadioEventsStatsUnknownFailureCount (&WtpMacAddr,
                                                             u4currentBindingId,
                                                             &radioUnknownFailure);
        nmhGetCapwapBaseRadioEventsStatsConfigUpdateCount (&WtpMacAddr,
                                                           u4currentBindingId,
                                                           &CfgUpdateCount);
        nmhGetCapwapBaseRadioEventsStatsChannelChangeCount (&WtpMacAddr,
                                                            u4currentBindingId,
                                                            &ChannelChangeCount);
        nmhGetCapwapBaseRadioEventsStatsBandChangeCount (&WtpMacAddr,
                                                         u4currentBindingId,
                                                         &BandChangeCount);
        nmhGetCapwapBaseRadioEventsStatsCurrNoiseFloor (&WtpMacAddr,
                                                        u4currentBindingId,
                                                        (INT4 *)
                                                        &CurrNoiseFloor);
#endif
#ifdef WTP_WANTED
        nmhGetFsCapwapWtpRadioResetCount (i4IfIndex, &radioResetCount);
        nmhGetFsCapwapWtpRadioSwFailureCount (i4IfIndex, &radioSwFailure);
        nmhGetFsCapwapWtpRadioHwFailureCount (i4IfIndex, &radioHwFailure);
        nmhGetFsCapwapWtpRadioOtherFailureCount (i4IfIndex, &radioOtherFailure);
        nmhGetFsCapwapWtpRadioUnknownFailureCount (i4IfIndex,
                                                   &radioUnknownFailure);
        nmhGetFsCapwapWtpRadioConfigUpdateCount (i4IfIndex, &CfgUpdateCount);
        nmhGetFsCapwapWtpRadioChannelChangeCount (i4IfIndex,
                                                  &ChannelChangeCount);
        nmhGetFsCapwapWtpRadioBandChangeCount (i4IfIndex, &BandChangeCount);
        nmhGetFsCapwapWtpRadioCurrentNoiseFloor (i4IfIndex, &CurrNoiseFloor);
#endif

        CliPrintf (CliHandle, "\r\n\nFor Radio %d", u4currentBindingId);
        CliPrintf (CliHandle, "\r\nReset Count                         :%d",
                   radioResetCount);
        CliPrintf (CliHandle, "\r\nSoftware Failure Count              :%d",
                   radioSwFailure);
        CliPrintf (CliHandle, "\r\nHardware Failure Count              :%d",
                   radioHwFailure);
        CliPrintf (CliHandle, "\r\nOther Failure Count                 :%d",
                   radioOtherFailure);
        CliPrintf (CliHandle, "\r\nUnknown Failure Count               :%d",
                   radioUnknownFailure);
        CliPrintf (CliHandle, "\r\nCfg Update Count                    :%d",
                   CfgUpdateCount);
        CliPrintf (CliHandle, "\r\nChannel Change Count                :%d",
                   ChannelChangeCount);
        CliPrintf (CliHandle, "\r\nBand Change Count                   :%d",
                   BandChangeCount);
        CliPrintf (CliHandle, "\r\nCurr Noise Floor                    :%d",
                   CurrNoiseFloor);

#ifdef WLC_WANTED
        nmhGetDot11TransmittedFragmentCount (i4IfIndex,
                                             &u4Dot11TransmittedFragmentCount);
        nmhGetDot11GroupTransmittedFrameCount (i4IfIndex,
                                               &u4Dot11MulticastTransmittedFrameCount);
        nmhGetDot11FailedCount (i4IfIndex, &u4Dot11FailedCount);
        nmhGetDot11MultipleRetryCount (i4IfIndex, &u4Dot11MultipleRetryCount);
        nmhGetDot11FrameDuplicateCount (i4IfIndex, &u4Dot11FrameDuplicateCount);
        nmhGetDot11RTSSuccessCount (i4IfIndex, &u4Dot11RTSSuccessCount);
        nmhGetDot11RTSFailureCount (i4IfIndex, &u4Dot11RTSFailureCount);
        nmhGetDot11ACKFailureCount (i4IfIndex, &u4Dot11ACKFailureCount);
        nmhGetDot11ReceivedFragmentCount (i4IfIndex,
                                          &u4Dot11ReceivedFragmentCount);
        nmhGetDot11GroupReceivedFrameCount (i4IfIndex,
                                            &u4Dot11MulticastReceivedFrameCount);
        nmhGetDot11FCSErrorCount (i4IfIndex, &u4Dot11FCSErrorCount);
        nmhGetDot11TransmittedFrameCount (i4IfIndex,
                                          &u4Dot11TransmittedFrameCount);
        nmhGetDot11WEPUndecryptableCount (i4IfIndex,
                                          &u4Dot11WEPUndecryptableCount);
        nmhGetDot11QosDiscardedFragmentCount (i4IfIndex,
                                              &u4Dot11QosDiscardedFragmentCount);
        nmhGetDot11AssociatedStationCount (i4IfIndex,
                                           &u4Dot11AssociatedStationCount);
        nmhGetDot11QosCFPollsReceivedCount (i4IfIndex,
                                            &u4Dot11QosCFPollsReceivedCount);
        nmhGetDot11QosCFPollsUnusedCount (i4IfIndex,
                                          &u4Dot11QosCFPollsUnusedCount);
        nmhGetDot11QosCFPollsUnusableCount (i4IfIndex,
                                            &u4Dot11QosCFPollsUnusableCount);
#endif
#ifdef WTP_WANTED
        nmhGetFsCapwapDot11TxFragmentCnt (i4IfIndex,
                                          &u4Dot11TransmittedFragmentCount);
        nmhGetFsCapwapDot11MulticastTxCnt (i4IfIndex,
                                           &u4Dot11MulticastTransmittedFrameCount);
        nmhGetFsCapwapDot11FailedCount (i4IfIndex, &u4Dot11FailedCount);
        nmhGetFsCapwapDot11MultipleRetryCount (i4IfIndex,
                                               &u4Dot11MultipleRetryCount);
        nmhGetFsCapwapDot11FrameDupCount (i4IfIndex,
                                          &u4Dot11FrameDuplicateCount);
        nmhGetFsCapwapDot11RTSSuccessCount (i4IfIndex, &u4Dot11RTSSuccessCount);
        nmhGetFsCapwapDot11RTSFailCount (i4IfIndex, &u4Dot11RTSFailureCount);
        nmhGetFsCapwapDot11ACKFailCount (i4IfIndex, &u4Dot11ACKFailureCount);
        nmhGetFsCapwapDot11RxFragmentCount (i4IfIndex,
                                            &u4Dot11ReceivedFragmentCount);
        nmhGetFsCapwapDot11MulticastRxCount (i4IfIndex,
                                             &u4Dot11MulticastReceivedFrameCount);
        nmhGetFsCapwapDot11FCSErrCount (i4IfIndex, &u4Dot11FCSErrorCount);
        nmhGetFsCapwapDot11TxFrameCount (i4IfIndex,
                                         &u4Dot11TransmittedFrameCount);
        nmhGetFsCapwapDot11DecryptionErr (i4IfIndex,
                                          &u4Dot11WEPUndecryptableCount);
        nmhGetFsCapwapDot11DiscardQosFragmentCnt (i4IfIndex,
                                                  &u4Dot11QosDiscardedFragmentCount);
        nmhGetFsCapwapDot11AssociatedStaCount (i4IfIndex,
                                               &u4Dot11AssociatedStationCount);
        nmhGetFsCapwapDot11QosCFPollsRecvdCount (i4IfIndex,
                                                 &u4Dot11QosCFPollsReceivedCount);
        nmhGetFsCapwapDot11QosCFPollsUnusedCount (i4IfIndex,
                                                  &u4Dot11QosCFPollsUnusedCount);
        nmhGetFsCapwapDot11QosCFPollsUnusableCount (i4IfIndex,
                                                    &u4Dot11QosCFPollsUnusableCount);
#endif
        CliPrintf (CliHandle, "\r\n\nDot11 Counters");
        CliPrintf (CliHandle, "\r\nFragments Transmitted               :%d",
                   u4Dot11TransmittedFragmentCount);
        CliPrintf (CliHandle, "\r\nMulticast Fragments Transmitted     :%d",
                   u4Dot11MulticastTransmittedFrameCount);
        CliPrintf (CliHandle, "\r\nFailed Packets                      :%d",
                   u4Dot11FailedCount);
        CliPrintf (CliHandle, "\r\nMulitple Retried Transmission       :%d",
                   u4Dot11MultipleRetryCount);
        CliPrintf (CliHandle, "\r\nDuplicate Frames                    :%d",
                   u4Dot11FrameDuplicateCount);
        CliPrintf (CliHandle, "\r\nRTS Success Count                   :%d",
                   u4Dot11RTSSuccessCount);
        CliPrintf (CliHandle, "\r\nRTS Failure Count                   :%d",
                   u4Dot11RTSFailureCount);
        CliPrintf (CliHandle, "\r\nACK Failure Count                   :%d",
                   u4Dot11ACKFailureCount);
        CliPrintf (CliHandle, "\r\nFragments Received                  :%d",
                   u4Dot11ReceivedFragmentCount);
        CliPrintf (CliHandle, "\r\nMulticast Fragments Received        :%d",
                   u4Dot11MulticastReceivedFrameCount);
        CliPrintf (CliHandle, "\r\nFCS Errors                          :%d",
                   u4Dot11FCSErrorCount);
        CliPrintf (CliHandle, "\r\nNo.Of Frames Transmitted            :%d",
                   u4Dot11TransmittedFrameCount);
        CliPrintf (CliHandle, "\r\nUndecryptable WEP Count             :%d",
                   u4Dot11WEPUndecryptableCount);
        CliPrintf (CliHandle, "\r\nQos Fragments Discarded             :%d",
                   u4Dot11QosDiscardedFragmentCount);
        CliPrintf (CliHandle, "\r\nAssociated Stations                 :%d",
                   u4Dot11AssociatedStationCount);
        CliPrintf (CliHandle, "\r\nQos CF Polls Received               :%d",
                   u4Dot11QosCFPollsReceivedCount);
        CliPrintf (CliHandle, "\r\nUnused Qos CF Polls                 :%d",
                   u4Dot11QosCFPollsUnusedCount);
        CliPrintf (CliHandle, "\r\nUnusable Qos CF Polls               :%d",
                   u4Dot11QosCFPollsUnusableCount);

    }
    while (nmhGetNextIndexFsCapwapWirelessBindingTable
           (currentProfileId, &nextProfileId, u4currentBindingId,
            &u4nextBindingId) == SNMP_SUCCESS);

    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : CapwapShowBackUpController 
* Description :
* Input       : NONE
* Output      :  None
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
CapwapShowBackUpController (tCliHandle CliHandle, UINT1 *pu1ProfileName)
{
    UINT4               u4CapwapBaseAcNameListId = 1;
    tSNMP_OCTET_STRING_TYPE CapwapBaseAcName;
    UINT1               au1CapwapBaseAcName[CAPWAP_ACL_MAX_SIZE];
    UINT4               u4WtpProfileId = 0;

    MEMSET (&CapwapBaseAcName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1CapwapBaseAcName, 0, CAPWAP_ACL_MAX_SIZE);
    CapwapBaseAcName.pu1_OctetList = (UINT1 *) au1CapwapBaseAcName;

    if (CapwapGetWtpProfileIdFromProfileName (pu1ProfileName, &u4WtpProfileId)
        != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n WTP Profile not found. \r\n");
        CLI_FATAL_ERROR (CliHandle);
        return OSIX_FAILURE;
    }

    nmhGetCapwapBaseAcNameListName (u4CapwapBaseAcNameListId,
                                    &CapwapBaseAcName);

    CliPrintf (CliHandle, "\r\nPrimary Controller      : %s\r\n",
               CapwapBaseAcName.pu1_OctetList);

    MEMSET (&CapwapBaseAcName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1CapwapBaseAcName, 0, CAPWAP_ACL_MAX_SIZE);

    CapwapBaseAcName.pu1_OctetList = (UINT1 *) au1CapwapBaseAcName;
    u4CapwapBaseAcNameListId = u4CapwapBaseAcNameListId + 1;

    nmhGetCapwapBaseAcNameListName (u4CapwapBaseAcNameListId,
                                    &CapwapBaseAcName);

    CliPrintf (CliHandle, "\r\nSecondary Controller    : %s\r\n",
               CapwapBaseAcName.pu1_OctetList);

    MEMSET (&CapwapBaseAcName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1CapwapBaseAcName, 0, CAPWAP_ACL_MAX_SIZE);

    CapwapBaseAcName.pu1_OctetList = (UINT1 *) au1CapwapBaseAcName;

    u4CapwapBaseAcNameListId = u4CapwapBaseAcNameListId + 1;
    nmhGetCapwapBaseAcNameListName (u4CapwapBaseAcNameListId,
                                    &CapwapBaseAcName);

    CliPrintf (CliHandle, "\r\nTertiary COntroller     : %s\r\n",
               CapwapBaseAcName.pu1_OctetList);

    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : CapwapShowRunningConfig 
* Description :
* Input       : NONE
* Output      :  None
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
CapwapShowRunningConfig (tCliHandle CliHandle)
{

    if (CapwapShowRunningFsCapwapTrapStatus (CliHandle) != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (CapwapShowRunningScalarsConfig (CliHandle) != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (CapwapShowRunningBlackListConfig (CliHandle) != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (CapwapShowRunningWhiteListConfig (CliHandle) != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (CapwapShowRunningStationBlackListConfig (CliHandle) != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (CapwapShowRunningStationWhiteListConfig (CliHandle) != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (CapwapShowRunningStationTypeConfig (CliHandle) != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (CapwapShowRunningAcNameListConfig (CliHandle) != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (CapwapShowRunningWtpModelConfig (CliHandle) != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (CapwapShowRunningWtpProfileConfig (CliHandle) != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (CapwapShowRunningWtpLinkEncryptConfig (CliHandle) != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (CapwapShowRunningWtpNativeVlanConfig (CliHandle) != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (CapwapShowRunningWtpFsCapwapConfig (CliHandle) != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

INT4
CapwapShowRunningScalarsConfig (tCliHandle CliHandle)
{
    INT4                i4WtpProfileRowVal = 0;
    INT4                i4WtpOtherRowVal = 0;
    UINT4               u4WtpProfileRowVal = 0;
    UINT4               u4Index = 0;
    UINT1               au1WtpStringVal[512] = { 0 };
    tSNMP_OCTET_STRING_TYPE WtpOctetString = { NULL, 0 };

    WtpOctetString.pu1_OctetList = au1WtpStringVal;

    CliPrintf (CliHandle, "!\r\n\r\n");

    u4WtpProfileRowVal = 0;
    nmhGetCapwapBaseAcMaxRetransmit (&u4WtpProfileRowVal);
    if (u4WtpProfileRowVal != 0)
    {
        CliPrintf (CliHandle, "advanced retransmit-count %d \r\n\r\n",
                   u4WtpProfileRowVal);
    }

    u4WtpProfileRowVal = 25;
    nmhGetCapwapBaseAcChangeStatePendingTimer (&u4WtpProfileRowVal);
    if (u4WtpProfileRowVal != 25)
    {
        CliPrintf (CliHandle,
                   "advanced timers change-state-pending %d \r\n\r\n",
                   u4WtpProfileRowVal);
    }

    u4WtpProfileRowVal = 30;
    nmhGetCapwapBaseAcDataCheckTimer (&u4WtpProfileRowVal);
    if (u4WtpProfileRowVal != 30)
    {
        CliPrintf (CliHandle, "advanced timers data-check-interval %d \r\n\r\n",
                   u4WtpProfileRowVal);
    }

    u4WtpProfileRowVal = 5;
    nmhGetCapwapBaseAcDTLSSessionDeleteTimer (&u4WtpProfileRowVal);
    if (u4WtpProfileRowVal != 5)
    {
        CliPrintf (CliHandle, "advanced timers dtlssessiondelete %d \r\n\r\n",
                   u4WtpProfileRowVal);
    }

    u4WtpProfileRowVal = 30;
    nmhGetCapwapBaseAcEchoInterval (&u4WtpProfileRowVal);
    if (u4WtpProfileRowVal != 30)
    {
        CliPrintf (CliHandle, "advanced timers echointerval %d \r\n\r\n",
                   u4WtpProfileRowVal);
    }

    u4WtpProfileRowVal = 3;
    nmhGetCapwapBaseAcRetransmitInterval (&u4WtpProfileRowVal);
    if (u4WtpProfileRowVal != 3)
    {
        CliPrintf (CliHandle, "advanced timers retransmit %d \r\n\r\n",
                   u4WtpProfileRowVal);
    }

    u4WtpProfileRowVal = 30;
    nmhGetCapwapBaseAcSilentInterval (&u4WtpProfileRowVal);
    if (u4WtpProfileRowVal != 30)
    {
        CliPrintf (CliHandle, "advanced timers silentinterval %d \r\n\r\n",
                   u4WtpProfileRowVal);
    }

    u4WtpProfileRowVal = 60;
    nmhGetCapwapBaseAcWaitDTLSTimer (&u4WtpProfileRowVal);
    if (u4WtpProfileRowVal != 60)
    {
        CliPrintf (CliHandle, "advanced timers waitdtls %d \r\n\r\n",
                   u4WtpProfileRowVal);
    }

    u4WtpProfileRowVal = 60;
    nmhGetCapwapBaseAcWaitJoinTimer (&u4WtpProfileRowVal);
    if (u4WtpProfileRowVal != 60)
    {
        CliPrintf (CliHandle, "advanced timers waitjoin %d \r\n\r\n",
                   u4WtpProfileRowVal);
    }

    i4WtpProfileRowVal = CLI_CAPWAP_ECN_LIMITED;
    nmhGetCapwapBaseAcEcnSupport (&i4WtpProfileRowVal);
    if (i4WtpProfileRowVal != CLI_CAPWAP_ECN_LIMITED)
    {
        CliPrintf (CliHandle, "ecn full \r\n\r\n");
    }

    /* scalar variables from the fscapwap.mib */
    i4WtpProfileRowVal = CLI_FSCAPWAP_ENABLE;
    nmhGetFsCapwapEnable (&i4WtpProfileRowVal);
    if (i4WtpProfileRowVal != CLI_FSCAPWAP_ENABLE)
    {
        CliPrintf (CliHandle, "capwap disable \r\n\r\n");
    }

    i4WtpProfileRowVal = CLI_CAPWAP_NO_SHUTDOWN;
    nmhGetFsCapwapShutdown (&i4WtpProfileRowVal);
    if (i4WtpProfileRowVal != CLI_CAPWAP_NO_SHUTDOWN)
    {
        CliPrintf (CliHandle, "capwap shutdown \r\n\r\n");
    }

    u4WtpProfileRowVal = CLI_UDP_PORT_DEF;
    nmhGetFsCapwapControlUdpPort (&u4WtpProfileRowVal);
    if (u4WtpProfileRowVal != CLI_UDP_PORT_DEF)
    {
        CliPrintf (CliHandle, "ap control-udp-port %u \r\n\r\n",
                   u4WtpProfileRowVal);
    }

    MEMSET (au1WtpStringVal, 0, sizeof (au1WtpStringVal));
    WtpOctetString.i4_Length = 512;
    nmhGetFsWlcDiscoveryMode (&WtpOctetString);
    if (WtpOctetString.i4_Length != 0)
    {
        u4WtpProfileRowVal = CLI_CAPWAP_DISC_MODE_MAC;
        if (!MEMCMP
            (&u4WtpProfileRowVal, au1WtpStringVal, WtpOctetString.i4_Length))
        {
            CliPrintf (CliHandle, "ap discovery auto \r\n\r\n",
                       u4WtpProfileRowVal);
        }

        u4WtpProfileRowVal = CLI_CAPWAP_DISC_MODE_MAC_WHITELIST;
        if (!MEMCMP
            (&u4WtpProfileRowVal, au1WtpStringVal, WtpOctetString.i4_Length))
        {
            CliPrintf (CliHandle, "ap discovery mac whitelist \r\n\r\n",
                       u4WtpProfileRowVal);
        }

        u4WtpProfileRowVal = CLI_CAPWAP_DISC_MODE_MAC_BLACKLIST;
        if (!MEMCMP
            (&u4WtpProfileRowVal, au1WtpStringVal, WtpOctetString.i4_Length))
        {
            CliPrintf (CliHandle, "ap discovery mac blacklist \r\n\r\n",
                       u4WtpProfileRowVal);
        }
    }

    i4WtpProfileRowVal = 0;
    nmhGetFsCapwapWtpModeIgnore (&i4WtpProfileRowVal);
    if (i4WtpProfileRowVal != 0)
    {
        CliPrintf (CliHandle, "ap wtpmodelcheck %s \r\n\r\n",
                   (i4WtpProfileRowVal ==
                    CLI_FSCAPWAP_MODEL_ENABLE) ? "enable" : "disable");
    }

    i4WtpProfileRowVal = CAPWAP_NO_TRC;
    nmhGetFsCapwapDebugMask (&i4WtpProfileRowVal);
    if (i4WtpProfileRowVal != CAPWAP_NO_TRC)
    {
        u4Index = 0;
        if ((i4WtpProfileRowVal & CAPWAP_ENTRY_TRC) == CAPWAP_ENTRY_TRC)
        {
            SPRINTF ((CHR1 *) & au1WtpStringVal[u4Index], "%s ", "entry");
            u4Index += STRLEN ("entry") + 1;
        }

        if ((i4WtpProfileRowVal & CAPWAP_EXIT_TRC) == CAPWAP_EXIT_TRC)
        {
            SPRINTF ((CHR1 *) & au1WtpStringVal[u4Index], "%s ", "exit");
            u4Index += STRLEN ("exit") + 1;
        }

        if ((i4WtpProfileRowVal & CAPWAP_FSM_TRC) == CAPWAP_FSM_TRC)
        {
            SPRINTF ((CHR1 *) & au1WtpStringVal[u4Index], "%s ", "fsm");
            u4Index += STRLEN ("fsm") + 1;
        }

        if ((i4WtpProfileRowVal & CAPWAP_PKTDUMP_TRC) == CAPWAP_PKTDUMP_TRC)
        {
            SPRINTF ((CHR1 *) & au1WtpStringVal[u4Index], "%s ", "packets");
            u4Index += STRLEN ("packets") + 1;
        }

        if ((i4WtpProfileRowVal & CAPWAP_STATS_TRC) == CAPWAP_STATS_TRC)
        {
            SPRINTF ((CHR1 *) & au1WtpStringVal[u4Index], "%s ", "stats");
            u4Index += STRLEN ("stats") + 1;
        }

        if ((i4WtpProfileRowVal & CAPWAP_MGMT_TRC) == CAPWAP_MGMT_TRC)
        {
            SPRINTF ((CHR1 *) & au1WtpStringVal[u4Index], "%s ", "mgmt");
            u4Index += STRLEN ("mgmt") + 1;
        }

        if ((i4WtpProfileRowVal & CAPWAP_FAILURE_TRC) == CAPWAP_FAILURE_TRC)
        {
            SPRINTF ((CHR1 *) & au1WtpStringVal[u4Index], "%s ", "fail");
            u4Index += STRLEN ("fail") + 1;
        }

        if ((i4WtpProfileRowVal & CAPWAP_INIT_TRC) == CAPWAP_INIT_TRC)
        {
            SPRINTF ((CHR1 *) & au1WtpStringVal[u4Index], "%s ", "init");
            u4Index += STRLEN ("init") + 1;
        }

        if ((i4WtpProfileRowVal & CAPWAP_BUF_TRC) == CAPWAP_BUF_TRC)
        {
            SPRINTF ((CHR1 *) & au1WtpStringVal[u4Index], "%s ", "buf");
            u4Index += STRLEN ("buf") + 1;
        }

        if ((i4WtpProfileRowVal & CAPWAP_SESS_TRC) == CAPWAP_SESS_TRC)
        {
            SPRINTF ((CHR1 *) & au1WtpStringVal[u4Index], "%s ", "sess");
            u4Index += STRLEN ("sess") + 1;
        }

        if ((i4WtpProfileRowVal & CAPWAP_MAIN_TRC) == CAPWAP_MAIN_TRC)
        {
            SPRINTF ((CHR1 *) & au1WtpStringVal[u4Index], "%s ", "main");
            u4Index += STRLEN ("main") + 1;
        }

        if ((i4WtpProfileRowVal & CAPWAP_UTIL_TRC) == CAPWAP_UTIL_TRC)
        {
            SPRINTF ((CHR1 *) & au1WtpStringVal[u4Index], "%s ", "util");
            u4Index += STRLEN ("util") + 1;
        }

        if (i4WtpProfileRowVal == CAPWAP_ALL_TRC)
        {
            CliPrintf (CliHandle, "debug capwap all \r\n\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "debug capwap %s \r\n\r\n", au1WtpStringVal);
        }
    }

    i4WtpProfileRowVal = CAPWAP_NO_TRC;
    nmhGetFsDtlsDebugMask (&i4WtpProfileRowVal);
    if (i4WtpProfileRowVal != CAPWAP_NO_TRC)
    {
        u4Index = 0;
        if ((i4WtpProfileRowVal & CAPWAP_ENTRY_TRC) == CAPWAP_ENTRY_TRC)
        {
            SPRINTF ((CHR1 *) & au1WtpStringVal[u4Index], "%s ", "entry");
            u4Index += STRLEN ("entry") + 1;
        }

        if ((i4WtpProfileRowVal & CAPWAP_EXIT_TRC) == CAPWAP_EXIT_TRC)
        {
            SPRINTF ((CHR1 *) & au1WtpStringVal[u4Index], "%s ", "exit");
            u4Index += STRLEN ("exit") + 1;
        }

        if ((i4WtpProfileRowVal & CAPWAP_FSM_TRC) == CAPWAP_FSM_TRC)
        {
            SPRINTF ((CHR1 *) & au1WtpStringVal[u4Index], "%s ", "fsm");
            u4Index += STRLEN ("fsm") + 1;
        }

        if ((i4WtpProfileRowVal & CAPWAP_PKTDUMP_TRC) == CAPWAP_PKTDUMP_TRC)
        {
            SPRINTF ((CHR1 *) & au1WtpStringVal[u4Index], "%s ", "packets");
            u4Index += STRLEN ("packets") + 1;
        }

        if ((i4WtpProfileRowVal & CAPWAP_STATS_TRC) == CAPWAP_STATS_TRC)
        {
            SPRINTF ((CHR1 *) & au1WtpStringVal[u4Index], "%s ", "stats");
            u4Index += STRLEN ("stats") + 1;
        }

        if ((i4WtpProfileRowVal & CAPWAP_MGMT_TRC) == CAPWAP_MGMT_TRC)
        {
            SPRINTF ((CHR1 *) & au1WtpStringVal[u4Index], "%s ", "debug");
            u4Index += STRLEN ("debug") + 1;
        }

        if ((i4WtpProfileRowVal & CAPWAP_FAILURE_TRC) == CAPWAP_FAILURE_TRC)
        {
            SPRINTF ((CHR1 *) & au1WtpStringVal[u4Index], "%s ", "fail");
            u4Index += STRLEN ("fail") + 1;
        }

        if (i4WtpProfileRowVal == 0xFFFF)
        {
            CliPrintf (CliHandle, "debug dtls all \r\n\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "debug dtls %s \r\n\r\n", au1WtpStringVal);
        }
    }

    i4WtpProfileRowVal = 0;
    nmhGetFsDtlsEncryption (&i4WtpProfileRowVal);
    if (i4WtpProfileRowVal != 0)
    {
        i4WtpOtherRowVal = 0;
        nmhGetFsDtlsEncryptAlogritham (&i4WtpOtherRowVal);
        if (i4WtpOtherRowVal != 0)
        {
            CliPrintf (CliHandle, "dtls %s %s \r\n\r\n",
                       (i4WtpProfileRowVal ==
                        CLI_CAPWAP_FSDTLSENCRPTION_PSK) ? "psk"
                       : (i4WtpProfileRowVal ==
                          CLI_CAPWAP_FSDTLSENCRPTION_CER) ? "certificates" : "",
                       (i4WtpOtherRowVal ==
                        CLI_CAPWAP_FSDTLSENCAUTHENCRYPTALGORITHM_AES) ? "aes128"
                       : (i4WtpOtherRowVal ==
                          CLI_CAPWAP_FSDTLSENCAUTHENCRYPTALGORITHM_DHE_AES) ?
                       "dheaes128" : (i4WtpOtherRowVal ==
                                      CLI_CAPWAP_FSDTLSENCAUTHENCRYPTALGORITHM_AES256)
                       ? "aes256" : (i4WtpOtherRowVal ==
                                     CLI_CAPWAP_FSDTLSENCAUTHENCRYPTALGORITHM_DHE_AES256)
                       ? "dhaaes256" : "");
        }
    }

    CliPrintf (CliHandle, "!\r\n\r\n");
    return CLI_SUCCESS;
}

INT4
CapwapShowRunningBlackListConfig (tCliHandle CliHandle)
{
    UINT4               u4BlackListId = 0;
    UINT4               u4PrevBlackListId = 0;
    INT4                i4WtpProfileRowVal = 0;
    UINT1               au1WtpStringVal[512] = { 0 };
    UINT1               au1TmpWtpStringVal[512] = { 0 };
    tSNMP_OCTET_STRING_TYPE WtpOctetString = { NULL, 0 };

    WtpOctetString.pu1_OctetList = au1WtpStringVal;

    if (nmhGetFirstIndexFsCapwapBlackList (&u4BlackListId) != SNMP_SUCCESS)
    {
        return CLI_SUCCESS;
    }

    do
    {
        CliPrintf (CliHandle, "!\r\n\r\n");

        /* checking row-status, always active if configured from cli */
        i4WtpProfileRowVal = 0;
        nmhGetFsCapwapBlackListRowStatus (u4BlackListId, &i4WtpProfileRowVal);
        if (i4WtpProfileRowVal == ACTIVE)
        {
            MEMSET (au1WtpStringVal, 0, sizeof (au1WtpStringVal));
            WtpOctetString.i4_Length = 512;
            nmhGetFsCapwapBlackListWtpBaseMac (u4BlackListId, &WtpOctetString);
            if (WtpOctetString.i4_Length != 0)
            {
                CliMacToStr (au1WtpStringVal, au1TmpWtpStringVal);
                CliPrintf (CliHandle, "ap blacklist add %s \r\n\r\n",
                           au1TmpWtpStringVal);
            }
        }

        u4PrevBlackListId = u4BlackListId;
        /* clear the old values */
        u4BlackListId = 0;
    }
    while (nmhGetNextIndexFsCapwapBlackList (u4PrevBlackListId,
                                             &u4BlackListId) == SNMP_SUCCESS);

    CliPrintf (CliHandle, "!\r\n\r\n");
    return CLI_SUCCESS;
}

INT4
CapwapShowRunningWhiteListConfig (tCliHandle CliHandle)
{
    UINT4               u4WhiteListId = 0;
    UINT4               u4PrevWhiteListId = 0;
    INT4                i4WtpProfileRowVal = 0;
    UINT1               au1WtpStringVal[512] = { 0 };
    UINT1               au1TmpWtpStringVal[512] = { 0 };
    tSNMP_OCTET_STRING_TYPE WtpOctetString = { NULL, 0 };

    WtpOctetString.pu1_OctetList = au1WtpStringVal;

    if (nmhGetFirstIndexFsCapwapWhiteListTable (&u4WhiteListId) != SNMP_SUCCESS)
    {
        return CLI_SUCCESS;
    }

    do
    {
        CliPrintf (CliHandle, "!\r\n\r\n");

        /* checking row-status, always active if configured from cli */
        i4WtpProfileRowVal = 0;
        nmhGetFsCapwapWhiteListRowStatus (u4WhiteListId, &i4WtpProfileRowVal);
        if (i4WtpProfileRowVal == ACTIVE)
        {
            MEMSET (au1WtpStringVal, 0, sizeof (au1WtpStringVal));
            WtpOctetString.i4_Length = 512;
            nmhGetFsCapwapWhiteListWtpBaseMac (u4WhiteListId, &WtpOctetString);
            if (WtpOctetString.i4_Length != 0)
            {
                CliMacToStr (au1WtpStringVal, au1TmpWtpStringVal);
                CliPrintf (CliHandle, "ap whitelist add %s \r\n\r\n",
                           au1TmpWtpStringVal);
            }
        }

        u4PrevWhiteListId = u4WhiteListId;
        /* clear the old values */
        u4WhiteListId = 0;
    }
    while (nmhGetNextIndexFsCapwapWhiteListTable (u4PrevWhiteListId,
                                                  &u4WhiteListId) ==
           SNMP_SUCCESS);

    CliPrintf (CliHandle, "!\r\n\r\n");
    return CLI_SUCCESS;
}

/****************************************************************************
 * * Function    :  CapwapShowRunningStationWhiteListConfig
 * * Description :  Shows Station Whitelist Configurations
 * * Input       :  NONE
 * * Output      :  None
 * * Returns     :  CLI_SUCCESS/CLI_FAILURE
 * ****************************************************************************/

INT4
CapwapShowRunningStationWhiteListConfig (tCliHandle CliHandle)
{
    INT4                i4WtpProfileRowVal = 0;
    UINT1               au1WtpStringVal[21] = { 0 };
    UINT1               au1TmpWtpStringVal[21] = { 0 };
    UINT1               au1ClearString[21] = { 0 };
    tSNMP_OCTET_STRING_TYPE StaMacAddr = { NULL, 0 };
    tSNMP_OCTET_STRING_TYPE NextStaMacAddr = { NULL, 0 };
    tMacAddr            MacAddr;
    tMacAddr            NextMacAddr;

    MEMSET (au1WtpStringVal, 0, sizeof (au1WtpStringVal));
    MEMSET (au1TmpWtpStringVal, 0, sizeof (au1TmpWtpStringVal));
    MEMSET (au1ClearString, 0, sizeof (au1ClearString));
    MEMSET (&MacAddr, 0, sizeof (tMacAddr));
    MEMSET (&NextMacAddr, 0, sizeof (NextMacAddr));

    StaMacAddr.pu1_OctetList = MacAddr;
    NextStaMacAddr.pu1_OctetList = NextMacAddr;

    if (nmhGetFirstIndexFsCapwapStationWhiteListTable (&NextStaMacAddr) !=
        SNMP_SUCCESS)
    {
        return CLI_SUCCESS;
    }

    CliPrintf (CliHandle, "!\r\n\r\n");
    do
    {

        /* checking row-status, always active if configured from cli */
        i4WtpProfileRowVal = 0;

        if ((nmhGetFsCapwapStationWhiteListRowStatus
             (&NextStaMacAddr, &i4WtpProfileRowVal) != SNMP_SUCCESS))
        {
        }
        if (i4WtpProfileRowVal == ACTIVE)
        {
            if (NextStaMacAddr.i4_Length != 0)
            {
                CliMacToStr (NextStaMacAddr.pu1_OctetList, au1TmpWtpStringVal);
                CliPrintf (CliHandle, "sta whitelist add %s \r\n\r\n",
                           au1TmpWtpStringVal);
            }
        }
        MEMCPY (StaMacAddr.pu1_OctetList, NextStaMacAddr.pu1_OctetList,
                MAC_ADDR_LEN);
    }
    while (nmhGetNextIndexFsCapwapStationWhiteListTable
           (&StaMacAddr, &NextStaMacAddr) == SNMP_SUCCESS);

    CliPrintf (CliHandle, "!\r\n\r\n");
    return CLI_SUCCESS;
}

/****************************************************************************
 * * Function    :  CapwapShowRunningStationBlackListConfig
 * * Description :  Shows Station Blacklist Configurations
 * * Input       :  NONE
 * * Output      :  None
 * * Returns     :  CLI_SUCCESS/CLI_FAILURE
 ******************************************************************************/

INT4
CapwapShowRunningStationBlackListConfig (tCliHandle CliHandle)
{
    INT4                i4WtpProfileRowVal = 0;
    UINT1               au1WtpStringVal[21] = { 0 };
    UINT1               au1TmpWtpStringVal[21] = { 0 };
    UINT1               au1ClearString[21] = { 0 };
    tSNMP_OCTET_STRING_TYPE StaMacAddr = { NULL, 0 };
    tSNMP_OCTET_STRING_TYPE NextStaMacAddr = { NULL, 0 };
    tMacAddr            MacAddr;
    tMacAddr            NextMacAddr;

    MEMSET (au1WtpStringVal, 0, sizeof (au1WtpStringVal));
    MEMSET (au1TmpWtpStringVal, 0, sizeof (au1TmpWtpStringVal));
    MEMSET (au1ClearString, 0, sizeof (au1ClearString));
    MEMSET (&MacAddr, 0, sizeof (tMacAddr));
    MEMSET (&NextMacAddr, 0, sizeof (NextMacAddr));

    StaMacAddr.pu1_OctetList = MacAddr;
    NextStaMacAddr.pu1_OctetList = NextMacAddr;

    if (nmhGetFirstIndexFsCapwapStationBlackListTable (&NextStaMacAddr) !=
        SNMP_SUCCESS)
    {
        return CLI_SUCCESS;
    }

    CliPrintf (CliHandle, "!\r\n\r\n");
    do
    {

        /* checking row-status, always active if configured from cli */
        i4WtpProfileRowVal = 0;

        if ((nmhGetFsCapwapStationBlackListRowStatus
             (&NextStaMacAddr, &i4WtpProfileRowVal) != SNMP_SUCCESS))
        {
        }
        if (i4WtpProfileRowVal == ACTIVE)
        {
            if (NextStaMacAddr.i4_Length != 0)
            {
                CliMacToStr (NextStaMacAddr.pu1_OctetList, au1TmpWtpStringVal);
                CliPrintf (CliHandle, "sta blacklist add %s \r\n\r\n",
                           au1TmpWtpStringVal);
            }
        }
        MEMCPY (StaMacAddr.pu1_OctetList, NextStaMacAddr.pu1_OctetList,
                MAC_ADDR_LEN);
        MEMCPY (NextStaMacAddr.pu1_OctetList, au1ClearString, MAC_ADDR_LEN);
    }
    while (nmhGetNextIndexFsCapwapStationBlackListTable
           (&StaMacAddr, &NextStaMacAddr) == SNMP_SUCCESS);

    CliPrintf (CliHandle, "!\r\n\r\n");
    return CLI_SUCCESS;
}

/****************************************************************************
 * * Function    :  CapwapShowRunningStationTypeConfig
 * * Description :  Shows Station Type Configuration
 * * Input       :  NONE
 * * Output      :  None
 * * Returns     :  CLI_SUCCESS/CLI_FAILURE
 ******************************************************************************/

INT4
CapwapShowRunningStationTypeConfig (tCliHandle CliHandle)
{
    INT4                i4StationType = 0;
    if (nmhGetFsStationType (&i4StationType) != SNMP_FAILURE)
    {
        if (i4StationType == 1)
        {
            CliPrintf (CliHandle, "!\r\n\r\n");
            CliPrintf (CliHandle, "sta discovery mode blacklist\r\n\r\n");
        }
        else if (i4StationType == 2)
        {
            CliPrintf (CliHandle, "!\r\n\r\n");
            CliPrintf (CliHandle, "sta discovery mode whitelist\r\n\r\n");
        }
    }
    CliPrintf (CliHandle, "!\r\n\r\n");
    return CLI_SUCCESS;
}

INT4
CapwapShowRunningAcNameListConfig (tCliHandle CliHandle)
{
    UINT4               u4AcNameListId = 0;
    UINT4               u4PrevAcNameListId = 0;
    UINT4               u4WtpProfileRowVal = 0;
    INT4                i4WtpProfileRowVal = 0;
    UINT1               au1WtpStringVal[512] = { 0 };
    tSNMP_OCTET_STRING_TYPE WtpOctetString = { NULL, 0 };

    WtpOctetString.pu1_OctetList = au1WtpStringVal;

    if (nmhGetFirstIndexCapwapBaseAcNameListTable (&u4AcNameListId) !=
        SNMP_SUCCESS)
    {
        return CLI_SUCCESS;
    }

    do
    {
        CliPrintf (CliHandle, "!\r\n\r\n");

        /* checking row-status, always active if configured from cli */
        i4WtpProfileRowVal = 0;
        if ((nmhGetCapwapBaseAcNameListRowStatus (u4AcNameListId,
                                                  &i4WtpProfileRowVal)) !=
            SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
        if (i4WtpProfileRowVal == ACTIVE)
        {
            MEMSET (au1WtpStringVal, 0, sizeof (au1WtpStringVal));
            WtpOctetString.i4_Length = 512;
            nmhGetCapwapBaseAcNameListName (u4AcNameListId, &WtpOctetString);

            u4WtpProfileRowVal = 0;
            nmhGetCapwapBaseAcNameListPriority (u4AcNameListId,
                                                &u4WtpProfileRowVal);
            if ((u4WtpProfileRowVal != 0) && (WtpOctetString.i4_Length != 0))
            {
                CliPrintf (CliHandle, "ap %s controller_name %s \r\n\r\n",
                           (u4WtpProfileRowVal == 1) ? "primary-base" :
                           (u4WtpProfileRowVal == 2) ? "secondary-base" :
                           (u4WtpProfileRowVal == 3) ? "tertiary-base" : "",
                           au1WtpStringVal);
            }
        }

        u4PrevAcNameListId = u4AcNameListId;
        /* clear the old values */
        u4AcNameListId = 0;
    }
    while (nmhGetNextIndexCapwapBaseAcNameListTable (u4PrevAcNameListId,
                                                     &u4AcNameListId) ==
           SNMP_SUCCESS);

    CliPrintf (CliHandle, "!\r\n\r\n");
    return CLI_SUCCESS;
}

INT4
CapwapShowRunningWtpModelConfig (tCliHandle CliHandle)
{
    UINT1               au1WtpModelName[256] = { 0 };
    UINT1               au1PrevWtpModelName[256] = { 0 };
    UINT1               au1WtpModelImageName[256] = { 0 };
    UINT1               au1WtpModelTunnelMode[256] = { 0 };
    UINT1               u1WtpTunnelMode = 0;
    INT4                i4WtpModelMacType = 0xFF;
    UINT4               u4WtpModelNoOfRadio = 0;
    tSNMP_OCTET_STRING_TYPE WtpModelName = { NULL, 0 };
    tSNMP_OCTET_STRING_TYPE PrevWtpModelName = { NULL, 0 };
    tSNMP_OCTET_STRING_TYPE WtpModelImageName = { NULL, 0 };
    tSNMP_OCTET_STRING_TYPE WtpModelTunnelMode = { NULL, 0 };

    WtpModelName.pu1_OctetList = au1WtpModelName;
    WtpModelName.i4_Length = 256;
    PrevWtpModelName.pu1_OctetList = au1PrevWtpModelName;
    PrevWtpModelName.i4_Length = 256;
    WtpModelImageName.pu1_OctetList = au1WtpModelImageName;
    WtpModelImageName.i4_Length = 256;
    WtpModelTunnelMode.pu1_OctetList = au1WtpModelTunnelMode;
    WtpModelTunnelMode.i4_Length = 256;

    if (nmhGetFirstIndexFsWtpModelTable (&WtpModelName) != SNMP_SUCCESS)
    {
        return CLI_SUCCESS;
    }

    do
    {
        CliPrintf (CliHandle, "!\r\n\r\n");
        if (WtpModelName.i4_Length != 0)
        {
            CliPrintf (CliHandle, "wtp modelname %s \r\n\r\n", au1WtpModelName);
        }

        nmhGetFsCapwapImageName (&WtpModelName, &WtpModelImageName);
        if (WtpModelImageName.i4_Length != 0)
        {
            CliPrintf (CliHandle, "set wtp image version %s \r\n\r\n",
                       au1WtpModelImageName);
        }

        nmhGetFsCapwapWtpMacType (&WtpModelName, &i4WtpModelMacType);
        if (CLI_MAC_TYPE_SPLIT == i4WtpModelMacType)
        {
            CliPrintf (CliHandle, "set wtp mac split \r\n\r\n");
        }
        else if (CLI_MAC_TYPE_LOCAL == i4WtpModelMacType)
        {
            nmhGetFsCapwapWtpTunnelMode (&WtpModelName, &WtpModelTunnelMode);
            if (WtpModelTunnelMode.i4_Length != 0)
            {
                u1WtpTunnelMode = CLI_TUNNEL_MODE_BRIDGE;
                if (MEMCMP (au1WtpModelTunnelMode, &u1WtpTunnelMode, 1) == 0)
                {
                    CliPrintf (CliHandle,
                               "set wtp mac local tunnel bridge \r\n\r\n");
                }

                u1WtpTunnelMode = CLI_TUNNEL_MODE_DOT3;
                if (MEMCMP (au1WtpModelTunnelMode, &u1WtpTunnelMode, 1) == 0)
                {
                    CliPrintf (CliHandle,
                               "set wtp mac local tunnel dot3tunnel \r\n\r\n");
                }
            }
        }

        if (nmhGetFsNoOfRadio (&WtpModelName, &u4WtpModelNoOfRadio) !=
            SNMP_SUCCESS)
        {
            return OSIX_FAILURE;
        }
        if ((u4WtpModelNoOfRadio >= 1) && (u4WtpModelNoOfRadio <= 31))
        {
            CliPrintf (CliHandle, "set radio count %d \r\n\r\n",
                       u4WtpModelNoOfRadio);
        }

        /* search for radio table configurations */
        CapwapShowRunningWtpModelRadioConfig (CliHandle, &WtpModelName);

        CliPrintf (CliHandle, "exit\r\n\r\n");

        MEMCPY (PrevWtpModelName.pu1_OctetList, WtpModelName.pu1_OctetList,
                WtpModelName.i4_Length);
        PrevWtpModelName.i4_Length = WtpModelName.i4_Length;

        /* clear the old values */
        MEMSET (au1WtpModelName, 0, sizeof (au1WtpModelName));
        MEMSET (au1WtpModelImageName, 0, sizeof (au1WtpModelImageName));
        MEMSET (au1WtpModelTunnelMode, 0, sizeof (au1WtpModelTunnelMode));
        WtpModelName.i4_Length = 256;
        WtpModelImageName.i4_Length = 256;
        WtpModelTunnelMode.i4_Length = 256;
        i4WtpModelMacType = 0xFF;
        u4WtpModelNoOfRadio = 0;
    }
    while (nmhGetNextIndexFsWtpModelTable (&PrevWtpModelName,
                                           &WtpModelName) == SNMP_SUCCESS);

    CliPrintf (CliHandle, "!\r\n\r\n");
    return CLI_SUCCESS;
}

INT4
CapwapShowRunningWtpModelRadioConfig (tCliHandle CliHandle,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pCurrWtpModelName)
{
    UINT1               au1WtpModelName[256] = { 0 };
    UINT1               au1PrevWtpModelName[256] = { 0 };
    UINT4               u4WtpModelNoOfRadio = 0;
    UINT4               u4PrevWtpModelNoOfRadio = 0;
    INT4                i4WtpModelRadioRowStatus = 0;
    UINT4               u4WtpModelRadioType = 0;
    INT4                i4WtpModelRadioAdminStatus = 0;
    tSNMP_OCTET_STRING_TYPE WtpModelName = { NULL, 0 };
    tSNMP_OCTET_STRING_TYPE PrevWtpModelName = { NULL, 0 };

    WtpModelName.pu1_OctetList = au1WtpModelName;
    WtpModelName.i4_Length = 256;
    PrevWtpModelName.pu1_OctetList = au1PrevWtpModelName;
    PrevWtpModelName.i4_Length = 256;

    /* run throu' all the radio table configurations */
    if (nmhGetFirstIndexFsWtpRadioTable (&WtpModelName,
                                         &u4WtpModelNoOfRadio) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    do
    {
        /* matching model names only */
        if (MEMCMP (pCurrWtpModelName->pu1_OctetList, au1WtpModelName,
                    MAX_WTP_MODEL_NAME_LENGTH) == 0)
        {
            if ((nmhGetFsWtpRadioRowStatus (&WtpModelName, u4WtpModelNoOfRadio,
                                            &i4WtpModelRadioRowStatus)) !=
                SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }
            /* active rows only */
            if (i4WtpModelRadioRowStatus == ACTIVE)
            {
                nmhGetFsWtpRadioType (&WtpModelName, u4WtpModelNoOfRadio,
                                      &u4WtpModelRadioType);
                if (u4WtpModelRadioType < CLI_RADIO_TYPEMAX)
                {
                    switch (u4WtpModelRadioType)
                    {
                        case CLI_RADIO_TYPEA:
                            CliPrintf (CliHandle,
                                       "set radio %d dot11a \r\n\r\n",
                                       u4WtpModelNoOfRadio);
                            break;
                        case CLI_RADIO_TYPEG:
                            CliPrintf (CliHandle,
                                       "set radio %d dot11g \r\n\r\n",
                                       u4WtpModelNoOfRadio);
                            break;
                        case CLI_RADIO_TYPEBG:
                            CliPrintf (CliHandle,
                                       "set radio %d dot11bg \r\n\r\n",
                                       u4WtpModelNoOfRadio);
                            break;
                        case CLI_RADIO_TYPEAN:
                            CliPrintf (CliHandle,
                                       "set radio %d dot11an \r\n\r\n",
                                       u4WtpModelNoOfRadio);
                            break;
                        case CLI_RADIO_TYPEBGN:
                            CliPrintf (CliHandle,
                                       "set radio %d dot11bgn \r\n\r\n",
                                       u4WtpModelNoOfRadio);
                            break;
                        case CLI_RADIO_TYPEAC:
                            CliPrintf (CliHandle,
                                       "set radio %d dot11ac \r\n\r\n",
                                       u4WtpModelNoOfRadio);
                            break;
                        case CLI_RADIO_TYPEB:
                            CliPrintf (CliHandle,
                                       "set radio %d dot11b \r\n\r\n",
                                       u4WtpModelNoOfRadio);
                            break;
                        default:
                            break;
                    }
                }

                nmhGetFsRadioAdminStatus (&WtpModelName, u4WtpModelNoOfRadio,
                                          &i4WtpModelRadioAdminStatus);
                if (i4WtpModelRadioAdminStatus == CLI_CAPWAP_RADIO_ENABLE)
                {
                    /* default case is "enable". so, do nothing */
                }
                else if (i4WtpModelRadioAdminStatus == CLI_CAPWAP_RADIO_DISABLE)
                {
                    CliPrintf (CliHandle, "set radio %d disable \r\n\r\n",
                               u4WtpModelNoOfRadio);
                }
            }
        }

        MEMCPY (PrevWtpModelName.pu1_OctetList, WtpModelName.pu1_OctetList,
                WtpModelName.i4_Length);
        PrevWtpModelName.i4_Length = WtpModelName.i4_Length;
        u4PrevWtpModelNoOfRadio = u4WtpModelNoOfRadio;

        /* clear the old values */
        MEMSET (au1WtpModelName, 0, sizeof (au1WtpModelName));
        WtpModelName.i4_Length = 256;
        u4WtpModelRadioType = CLI_RADIO_TYPEB;
        i4WtpModelRadioAdminStatus = CLI_CAPWAP_RADIO_ENABLE;
    }
    while (nmhGetNextIndexFsWtpRadioTable (&PrevWtpModelName, &WtpModelName,
                                           u4PrevWtpModelNoOfRadio,
                                           &u4WtpModelNoOfRadio) ==
           SNMP_SUCCESS);

    return CLI_SUCCESS;
}

INT4
CapwapShowRunningWtpProfileConfig (tCliHandle CliHandle)
{
    UINT4               u4WtpProfileId = 0;
    UINT4               u4PrevWtpProfileId = 0;
    INT4                i4WtpProfileRowVal = 0;
    INT4                i4WtpForwarding = 0;
    UINT4               u4WtpProfileRowVal = 0;
    UINT1               au1WtpStringVal[256] = { 0 };
    UINT1               au1WtpProfileVal[256] = { 0 };
    UINT1               au1WtpNameVal[256] = { 0 };
    UINT1               au1WtpMaskVal[256] = { 0 };
    INT1                ai1WtpIpTmpVal[56] = "";
    INT1                ai1WtpMaskTmpVal[56] = "";
    INT1                ai1WtpGatewayTmpVal[56] = "";
    tSNMP_OCTET_STRING_TYPE WtpOctetString = { NULL, 0 };
    tSNMP_OCTET_STRING_TYPE WtpOctetProfile = { NULL, 0 };
    tSNMP_OCTET_STRING_TYPE WtpOctetName = { NULL, 0 };
    tSNMP_OCTET_STRING_TYPE WtpOctetMask = { NULL, 0 };

    WtpOctetString.pu1_OctetList = au1WtpStringVal;
    WtpOctetProfile.pu1_OctetList = au1WtpProfileVal;
    WtpOctetName.pu1_OctetList = au1WtpNameVal;
    WtpOctetMask.pu1_OctetList = au1WtpMaskVal;

    if (nmhGetFirstIndexCapwapBaseWtpProfileTable (&u4WtpProfileId) !=
        SNMP_SUCCESS)
    {
        return CLI_SUCCESS;
    }

    do
    {
        CliPrintf (CliHandle, "!\r\n\r\n");

        MEMSET (au1WtpProfileVal, 0, sizeof (au1WtpProfileVal));
        WtpOctetProfile.i4_Length = 256;
        if (nmhGetCapwapBaseWtpProfileName (u4WtpProfileId, &WtpOctetProfile) !=
            SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
        if (WtpOctetProfile.i4_Length != 0)
        {
            CliPrintf (CliHandle, "ap create profile_id %d profile %s \r\n\r\n",
                       u4WtpProfileId, au1WtpProfileVal);
        }

        MEMSET (au1WtpStringVal, 0, sizeof (au1WtpStringVal));
        WtpOctetString.i4_Length = 256;
        if (nmhGetCapwapBaseWtpProfileWtpMacAddress (u4WtpProfileId,
                                                     &WtpOctetString) !=
            SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
        if (WtpOctetString.i4_Length != 0)
        {
            CliPrintf (CliHandle, "ap mac-address %s %s \r\n\r\n",
                       au1WtpStringVal, au1WtpProfileVal);
        }

        MEMSET (au1WtpStringVal, 0, sizeof (au1WtpStringVal));
        WtpOctetString.i4_Length = 256;
        if (nmhGetCapwapBaseWtpProfileWtpModelNumber (u4WtpProfileId,
                                                      &WtpOctetString) !=
            SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
        MEMSET (au1WtpNameVal, 0, sizeof (au1WtpNameVal));
        WtpOctetName.i4_Length = 256;
        if (nmhGetCapwapBaseWtpProfileWtpName (u4WtpProfileId, &WtpOctetName) !=
            SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
        if ((WtpOctetString.i4_Length != 0) && (WtpOctetName.i4_Length != 0))
        {
            CliPrintf (CliHandle, "ap profile %s model %s %s \r\n\r\n",
                       au1WtpProfileVal, au1WtpStringVal, au1WtpNameVal);
        }

        MEMSET (au1WtpStringVal, 0, sizeof (au1WtpStringVal));
        WtpOctetString.i4_Length = 256;
        if (nmhGetCapwapBaseWtpProfileWtpLocation (u4WtpProfileId,
                                                   &WtpOctetString) !=
            SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
        if (WtpOctetString.i4_Length != 0)
        {
            CliPrintf (CliHandle, "ap location %s %s \r\n\r\n",
                       au1WtpStringVal, au1WtpProfileVal);
        }

        i4WtpProfileRowVal = CLI_CAPWAP_IP_DISABLE;
        nmhGetCapwapBaseWtpProfileWtpStaticIpEnable (u4WtpProfileId,
                                                     &i4WtpProfileRowVal);
        if (i4WtpProfileRowVal != CLI_CAPWAP_IP_DISABLE)
        {
            i4WtpProfileRowVal = 0;
            nmhGetCapwapBaseWtpProfileWtpStaticIpType (u4WtpProfileId,
                                                       &i4WtpProfileRowVal);
            /* only ipv4 supported for now */
            if (i4WtpProfileRowVal == CLI_CAPWAP_AP_TYPE_IPV4)
            {
                MEMSET (au1WtpStringVal, 0, sizeof (au1WtpStringVal));
                WtpOctetString.i4_Length = 256;
                nmhGetCapwapBaseWtpProfileWtpStaticIpAddress (u4WtpProfileId,
                                                              &WtpOctetString);
                if (WtpOctetString.i4_Length != 0)
                {
                    SPRINTF ((CHR1 *) ai1WtpIpTmpVal, "%d.%d.%d.%d",
                             au1WtpStringVal[0], au1WtpStringVal[1],
                             au1WtpStringVal[2], au1WtpStringVal[3]);
                }

                MEMSET (au1WtpMaskVal, 0, sizeof (au1WtpMaskVal));
                WtpOctetMask.i4_Length = 256;
                nmhGetCapwapBaseWtpProfileWtpNetmask (u4WtpProfileId,
                                                      &WtpOctetMask);
                if (WtpOctetMask.i4_Length != 0)
                {
                    SPRINTF ((CHR1 *) ai1WtpMaskTmpVal, "%d.%d.%d.%d",
                             au1WtpMaskVal[0], au1WtpMaskVal[1],
                             au1WtpMaskVal[2], au1WtpMaskVal[3]);
                }

                /* reusing 'au1WtpNameVal' variable to get gateway value */
                MEMSET (au1WtpNameVal, 0, sizeof (au1WtpNameVal));
                WtpOctetName.i4_Length = 256;
                nmhGetCapwapBaseWtpProfileWtpGateway (u4WtpProfileId,
                                                      &WtpOctetName);
                if (WtpOctetName.i4_Length != 0)
                {
                    SPRINTF ((CHR1 *) ai1WtpGatewayTmpVal, "%d.%d.%d.%d",
                             au1WtpNameVal[0], au1WtpNameVal[1],
                             au1WtpNameVal[2], au1WtpNameVal[3]);
                }

                CliPrintf (CliHandle,
                           "ap type ipv4 static-ip enable %s %s %s %s \r\n\r\n",
                           ai1WtpIpTmpVal, ai1WtpMaskTmpVal,
                           ai1WtpGatewayTmpVal, au1WtpProfileVal);
            }
        }

        i4WtpProfileRowVal = CLI_CAPWAP_FALLBACK_ENABLE;
        nmhGetCapwapBaseWtpProfileWtpFallbackEnable (u4WtpProfileId,
                                                     &i4WtpProfileRowVal);
        if (i4WtpProfileRowVal != CLI_CAPWAP_FALLBACK_ENABLE)
        {
            CliPrintf (CliHandle, "ap fallback disable %s \r\n\r\n",
                       au1WtpProfileVal);
        }

        u4WtpProfileRowVal = 30;
        nmhGetCapwapBaseWtpProfileWtpEchoInterval (u4WtpProfileId,
                                                   &u4WtpProfileRowVal);
        if (u4WtpProfileRowVal != 30)
        {
            CliPrintf (CliHandle, "ap echo-timer period %d %s \r\n\r\n",
                       u4WtpProfileRowVal, au1WtpProfileVal);
        }

        u4WtpProfileRowVal = 300;
        nmhGetCapwapBaseWtpProfileWtpIdleTimeout (u4WtpProfileId,
                                                  &u4WtpProfileRowVal);
        if (u4WtpProfileRowVal != 300)
        {
            CliPrintf (CliHandle, "ap idle-timer period %d %s \r\n\r\n",
                       u4WtpProfileRowVal, au1WtpProfileVal);
        }

        u4WtpProfileRowVal = 20;
        nmhGetCapwapBaseWtpProfileWtpMaxDiscoveryInterval (u4WtpProfileId,
                                                           &u4WtpProfileRowVal);
        if (u4WtpProfileRowVal != 20)
        {
            CliPrintf (CliHandle, "ap discovery-timer period %d %s \r\n\r\n",
                       u4WtpProfileRowVal, au1WtpProfileVal);
        }

        u4WtpProfileRowVal = 120;
        nmhGetCapwapBaseWtpProfileWtpReportInterval (u4WtpProfileId,
                                                     &u4WtpProfileRowVal);
        if (u4WtpProfileRowVal != 120)
        {
            CliPrintf (CliHandle, "ap report-timer period %d %s \r\n\r\n",
                       u4WtpProfileRowVal, au1WtpProfileVal);
        }

        u4WtpProfileRowVal = 120;
        nmhGetCapwapBaseWtpProfileWtpStatisticsTimer (u4WtpProfileId,
                                                      &u4WtpProfileRowVal);
        if (u4WtpProfileRowVal != 120)
        {
            CliPrintf (CliHandle, "ap stats-timer period %d %s \r\n\r\n",
                       u4WtpProfileRowVal, au1WtpProfileVal);
        }

        i4WtpProfileRowVal = CLI_CAPWAP_ECN_LIMITED;
        if (nmhGetCapwapBaseWtpProfileWtpEcnSupport (u4WtpProfileId,
                                                     &i4WtpProfileRowVal) !=
            SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
        if (i4WtpProfileRowVal != CLI_CAPWAP_ECN_LIMITED)
        {
            CliPrintf (CliHandle, "ap ecn full %s \r\n\r\n", au1WtpProfileVal);
        }

        i4WtpProfileRowVal = 0;
        if (nmhGetCapwapBaseWtpProfileRowStatus (u4WtpProfileId,
                                                 &i4WtpProfileRowVal) !=
            SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
        if (i4WtpProfileRowVal != NOT_IN_SERVICE)
        {
            CliPrintf (CliHandle, "ap enable profile_id %d profile %s \r\n\r\n",
                       u4WtpProfileId, au1WtpProfileVal);
        }
        if (nmhGetFsWtpLocalRouting (u4WtpProfileId,
                                     &i4WtpForwarding) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
        if (i4WtpForwarding == 1)
        {
            CliPrintf (CliHandle, "ap local-routing enable %s \r\n\r\n",
                       au1WtpProfileVal);
        }
        else if (i4WtpForwarding == 3)
        {
            CliPrintf (CliHandle, "ap local-bridging enable %s \r\n\r\n",
                       au1WtpProfileVal);
        }

        u4PrevWtpProfileId = u4WtpProfileId;
        /* clear the old values */
        u4WtpProfileId = 0;
    }
    while (nmhGetNextIndexCapwapBaseWtpProfileTable (u4PrevWtpProfileId,
                                                     &u4WtpProfileId) ==
           SNMP_SUCCESS);

    CliPrintf (CliHandle, "!\r\n\r\n");
    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    : CapwapClearAllAPStats
 * Description :
 * Input       :  CliHandle
 *
 *
 * Output      :  None
 * Returns     :  CLI_SUCCESS/CLI_FAILURE
 * ****************************************************************************/

INT4
CapwapClearAllAPStats (tCliHandle CliHandle)
{
    tCapwapFsCapwapWtpConfigEntry CapwapSetFsCapwapWtpConfigEntry;
    tCapwapIsSetFsCapwapWtpConfigEntry CapwapIsSetFsCapwapWtpConfigEntry;
    UINT4               u4WtpProfileId = 0, nextProfileId = 0, u4RowStatus = 0;
    UINT4               u4ClearApStats = CAPWAP_CLEAR_AP_STATS;
    INT4                i4OutCome = 0;
    INT4                i4RetStatus = 0;
    UINT4              *pu4Args = NULL;
    UINT4              *pu4RowStatus = NULL;
    UINT4              *pu4ClearApStats = NULL;
    UINT4              *pu4WtpProfileId = NULL;

    pu4WtpProfileId = &u4WtpProfileId;
    pu4RowStatus = &u4RowStatus;
    pu4ClearApStats = &u4ClearApStats;

    MEMSET (&CapwapSetFsCapwapWtpConfigEntry, 0,
            sizeof (tCapwapFsCapwapWtpConfigEntry));
    MEMSET (&CapwapIsSetFsCapwapWtpConfigEntry, 0,
            sizeof (tCapwapIsSetFsCapwapWtpConfigEntry));
    /* Get the first profile Id */
    i4OutCome = nmhGetFirstIndexCapwapBaseWtpProfileTable (&nextProfileId);
    if (i4OutCome == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n Entry not found\r\n");
        return OSIX_SUCCESS;
    }

    do
    {
        /*Get the profile Id */
        u4WtpProfileId = nextProfileId;
        if (nmhGetFsCapwapWtpConfigRowStatus
            (u4WtpProfileId, (INT4 *) &u4RowStatus) != SNMP_SUCCESS)
        {
            u4RowStatus = (CREATE_AND_GO);
        }
        else
        {
            u4RowStatus = (ACTIVE);
        }

        CAPWAP_FILL_FSCAPWAPWTPCONFIGTABLE_ARGS ((&CapwapSetFsCapwapWtpConfigEntry), (&CapwapIsSetFsCapwapWtpConfigEntry), pu4Args, pu4Args, pu4Args, pu4Args, pu4Args, pu4Args, pu4Args, pu4Args, pu4Args, pu4ClearApStats, pu4RowStatus, pu4Args, pu4WtpProfileId, pu4Args);
#ifdef WTP_WANTED
        i4RetStatus =
            CapwapCliSetFsCapwapWtpConfigTable (CliHandle,
                                                (&CapwapSetFsCapwapWtpConfigEntry),
                                                (&CapwapIsSetFsCapwapWtpConfigEntry));
#endif

    }
    while (nmhGetNextIndexCapwapBaseWtpProfileTable
           (u4WtpProfileId, &nextProfileId) == SNMP_SUCCESS);
    UNUSED_PARAM (i4RetStatus);
    return OSIX_SUCCESS;
}

INT4
CapwapShowRunningWtpLinkEncryptConfig (tCliHandle CliHandle)
{
    UINT4               u4WtpProfileId = 0;
    UINT4               u4PrevWtpProfileId = 0;
    INT4                i4LinkEncryptChnId = 0;
    INT4                i4PrevLinkEncryptChnId = 0;
    INT4                i4WtpProfileRowVal = 0;
    UINT1               au1WtpStringVal[256] = { 0 };
    tSNMP_OCTET_STRING_TYPE WtpOctetString = { NULL, 0 };

    WtpOctetString.pu1_OctetList = au1WtpStringVal;

    if (nmhGetFirstIndexFsCapwapLinkEncryptionTable (&u4WtpProfileId,
                                                     &i4LinkEncryptChnId) !=
        SNMP_SUCCESS)
    {
        return CLI_SUCCESS;
    }

    do
    {
        CliPrintf (CliHandle, "!\r\n\r\n");

        i4WtpProfileRowVal = 0;
        nmhGetFsCapwapEncryptChannelRowStatus (u4WtpProfileId,
                                               i4LinkEncryptChnId,
                                               &i4WtpProfileRowVal);
        if (i4WtpProfileRowVal == ACTIVE)
        {
            i4WtpProfileRowVal = CLI_FSCAPWAP_ENCRYPTION_DISABLE;
            nmhGetFsCapwapEncryptChannelStatus (u4WtpProfileId,
                                                i4LinkEncryptChnId,
                                                &i4WtpProfileRowVal);
            if (i4WtpProfileRowVal != CLI_FSCAPWAP_ENCRYPTION_DISABLE)
            {
                MEMSET (au1WtpStringVal, 0, sizeof (au1WtpStringVal));
                WtpOctetString.i4_Length = 256;
                if ((nmhGetCapwapBaseWtpProfileName (u4WtpProfileId,
                                                     &WtpOctetString)) !=
                    SNMP_SUCCESS)
                {
                    return CLI_FAILURE;
                }
                if (WtpOctetString.i4_Length != 0)
                {
                    CliPrintf (CliHandle,
                               "ap link-encryption enable %s %s\r\n\r\n",
                               au1WtpStringVal,
                               (i4LinkEncryptChnId ==
                                CLI_FSCAPWAP_ENCRYPTION_DATA) ? "data"
                               : (i4LinkEncryptChnId ==
                                  CLI_FSCAPWAP_ENCRYPTION_CONTROL) ? "control" :
                               "");
                }
            }
        }

        u4PrevWtpProfileId = u4WtpProfileId;
        i4PrevLinkEncryptChnId = i4LinkEncryptChnId;
        /* clear the old values */
        u4WtpProfileId = 0;
        i4LinkEncryptChnId = 0;
    }
    while (nmhGetNextIndexFsCapwapLinkEncryptionTable (u4PrevWtpProfileId,
                                                       &u4WtpProfileId,
                                                       i4PrevLinkEncryptChnId,
                                                       &i4LinkEncryptChnId) ==
           SNMP_SUCCESS);

    CliPrintf (CliHandle, "!\r\n\r\n");
    return CLI_SUCCESS;
}

INT4
CapwapShowRunningWtpNativeVlanConfig (tCliHandle CliHandle)
{
    UINT4               u4WtpProfileId = 0;
    UINT4               u4PrevWtpProfileId = 0;
    INT4                i4WtpProfileRowVal = 0;
    UINT1               au1WtpStringVal[256] = { 0 };
    tSNMP_OCTET_STRING_TYPE WtpOctetString = { NULL, 0 };

    WtpOctetString.pu1_OctetList = au1WtpStringVal;

    if (nmhGetFirstIndexFsWtpNativeVlanIdTable (&u4WtpProfileId)
        != SNMP_SUCCESS)
    {
        return CLI_SUCCESS;
    }

    do
    {
        CliPrintf (CliHandle, "!\r\n\r\n");

        i4WtpProfileRowVal = 0;
        nmhGetFsWtpNativeVlanIdRowStatus (u4WtpProfileId, &i4WtpProfileRowVal);
        if (i4WtpProfileRowVal == ACTIVE)
        {
            i4WtpProfileRowVal = 0;
            nmhGetFsWtpNativeVlanId (u4WtpProfileId, &i4WtpProfileRowVal);
            if (i4WtpProfileRowVal != 0)
            {
                MEMSET (au1WtpStringVal, 0, sizeof (au1WtpStringVal));
                WtpOctetString.i4_Length = 256;
                if ((nmhGetCapwapBaseWtpProfileName (u4WtpProfileId,
                                                     &WtpOctetString)) !=
                    SNMP_SUCCESS)
                {
                    return CLI_FAILURE;
                }
                if (WtpOctetString.i4_Length != 0)
                {
                    CliPrintf (CliHandle, "ap native vlan %d %s\r\n\r\n",
                               i4WtpProfileRowVal, au1WtpStringVal);
                }
            }
        }

        u4PrevWtpProfileId = u4WtpProfileId;
        /* clear the old values */
        u4WtpProfileId = 0;
    }
    while (nmhGetNextIndexFsWtpNativeVlanIdTable (u4PrevWtpProfileId,
                                                  &u4WtpProfileId) ==
           SNMP_SUCCESS);

    CliPrintf (CliHandle, "!\r\n\r\n");
    return CLI_SUCCESS;
}

INT4
CapwapShowRunningWtpFsCapwapConfig (tCliHandle CliHandle)
{
    UINT4               u4WtpProfileId = 0;
    UINT4               u4PrevWtpProfileId = 0;
    INT4                i4WtpProfileRowVal = 0;
    UINT1               au1WtpStringVal[256] = { 0 };
    UINT1               au1WtpCntryStringVal[256] = { 0 };
    tSNMP_OCTET_STRING_TYPE WtpOctetString = { NULL, 0 };
    tSNMP_OCTET_STRING_TYPE WtpOctetCntryString = { NULL, 0 };

    WtpOctetString.pu1_OctetList = au1WtpStringVal;
    WtpOctetCntryString.pu1_OctetList = au1WtpCntryStringVal;

    if (nmhGetFirstIndexFsCapwapWtpConfigTable (&u4WtpProfileId)
        != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    do
    {
        CliPrintf (CliHandle, "!\r\n\r\n");

        i4WtpProfileRowVal = 0;
        if ((nmhGetFsCapwapWtpConfigRowStatus
             (u4WtpProfileId, &i4WtpProfileRowVal)) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
        if (i4WtpProfileRowVal == ACTIVE)
        {
            MEMSET (au1WtpStringVal, 0, sizeof (au1WtpStringVal));
            WtpOctetString.i4_Length = 256;
            if (nmhGetCapwapBaseWtpProfileName (u4WtpProfileId,
                                                &WtpOctetString) !=
                SNMP_SUCCESS)
            {
            }

            i4WtpProfileRowVal = CLI_WTP_DISC_TYPE_REFERRAL;
            if ((nmhGetFsWtpDiscoveryType (u4WtpProfileId, &i4WtpProfileRowVal))
                != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }
            if ((i4WtpProfileRowVal != CLI_WTP_DISC_TYPE_REFERRAL) &&
                (WtpOctetString.i4_Length != 0))
            {
                CliPrintf (CliHandle, "ap %s discovery %s\r\n\r\n",
                           au1WtpStringVal,
                           (i4WtpProfileRowVal ==
                            CLI_WTP_DISC_TYPE_STATIC) ? "static"
                           : (i4WtpProfileRowVal ==
                              CLI_WTP_DISC_TYPE_DHCP) ? "dhcp"
                           : (i4WtpProfileRowVal ==
                              CLI_WTP_DISC_TYPE_DNS) ? "dns" : "");
            }

            MEMSET (au1WtpCntryStringVal, 0, sizeof (au1WtpCntryStringVal));
            WtpOctetCntryString.i4_Length = 256;
            nmhGetFsWtpCountryString (u4WtpProfileId, &WtpOctetCntryString);
            if ((WtpOctetCntryString.i4_Length != 0) &&
                (WtpOctetString.i4_Length != 0))
            {
                if (STRCMP (au1WtpCntryStringVal, DEFAULT_COUNTRY) != 0)
                {
                    CliPrintf (CliHandle, "country %s %s\r\n\r\n",
                               au1WtpCntryStringVal, au1WtpStringVal);
                }
            }
        }

        u4PrevWtpProfileId = u4WtpProfileId;
        /* clear the old values */
        u4WtpProfileId = 0;
    }
    while (nmhGetNextIndexFsCapwapWtpConfigTable (u4PrevWtpProfileId,
                                                  &u4WtpProfileId) ==
           SNMP_SUCCESS);

    CliPrintf (CliHandle, "!\r\n\r\n");
    return CLI_SUCCESS;
}

INT4
CapwapShowRunningFsCapwapTrapStatus (tCliHandle CliHandle)
{
    INT4                i4TrapStatus = 0;
    INT4                i4CriticalTrapStatus = 0;

    nmhGetFsCapwTrapStatus (&i4TrapStatus);

    if (CAPWAP_WTP_DEFAULT_TRAP_STATUS == CAPWAP_WTP_TRAP_DISABLE)
    {
        if (i4TrapStatus != CAPWAP_WTP_DEFAULT_TRAP_STATUS)
        {
            CliPrintf (CliHandle, "capwap traps enable \r\n\r\n");
        }
    }
    else if (CAPWAP_WTP_DEFAULT_TRAP_STATUS == CAPWAP_WTP_TRAP_ENABLE)
    {
        if (i4TrapStatus != CAPWAP_WTP_DEFAULT_TRAP_STATUS)
        {
            CliPrintf (CliHandle, "no capwap traps enable \r\n\r\n");
        }
    }

    nmhGetFsCapwCriticalTrapStatus (&i4CriticalTrapStatus);

    if (CAPWAP_WTP_DEFAULT_CRITICAL_TRAP_STATUS == CAPWAP_CRITICAL_TRAP_DISABLE)
    {
        if (i4CriticalTrapStatus != CAPWAP_WTP_DEFAULT_CRITICAL_TRAP_STATUS)
        {
            CliPrintf (CliHandle, "capwap traps critical enable \r\n\r\n");
        }
    }
    else if (CAPWAP_WTP_DEFAULT_CRITICAL_TRAP_STATUS ==
             CAPWAP_CRITICAL_TRAP_ENABLE)
    {
        if (i4CriticalTrapStatus != CAPWAP_WTP_DEFAULT_CRITICAL_TRAP_STATUS)
        {
            CliPrintf (CliHandle, "no capwap traps critical enable \r\n\r\n");
        }
    }
    CliPrintf (CliHandle, "!\r\n\r\n");
    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    :  CapwapCliSetStationConfigTimer
 * Description :  This function enables/disables the timer for clearing station
 *                config DB

 * Input       :  CliHandle - CLI Handler
 *                u4Status - Timer enable/disable
 *                u4TimerValue - timer interval

 * Output      :  None

 * Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/

INT4
CapwapCliSetStationConfigTimer (tCliHandle CliHandle, UINT4 u4Status,
                                UINT4 u4TimerValue)
{
#ifdef WLC_WANTED
    if (u4Status == 1 && ((u4TimerValue < CAPWAP_STATION_CONFIG_MIN) ||
                          (u4TimerValue > CAPWAP_STATION_CONFIG_MAX)))
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    if (u4Status == 1)
    {
        /*The timer is started by default with a default timeout value. So to change the timer value the timer is stopped first */
        WlcHdlrStaConfClearTmrControl (0, u4TimerValue);
    }
    WlcHdlrStaConfClearTmrControl (u4Status, u4TimerValue);
#endif
    return CLI_SUCCESS;
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (u4Status);
    UNUSED_PARAM (u4TimerValue);
}
#endif
