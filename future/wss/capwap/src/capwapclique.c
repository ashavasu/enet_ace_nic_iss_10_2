/******************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * $Id: capwapclique.c,v 1.1.1.1 2016/09/20 10:42:39 siva Exp $
 * Description : This file contains the processing of 
 *               messages queued
 *****************************************************************************/
#include "capwapinc.h"

/****************************************************************************
*                                                                           *
* Function     : CapwapQueProcessMsgs                                         *
*                                                                           *
* Description  : Deques and Process the msgs                                *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*****************************************************************************/

PUBLIC VOID
CapwapQueProcessMsgs ()
{
    CHR1        *pCapwapQueMsg = NULL;
    CAPWAP_FN_ENTRY ();

    while (OsixQueRecv (gCapwapGlobals.capwapQueId,
                        (UINT1 *) &pCapwapQueMsg, OSIX_DEF_MSG_LEN,
                        0) == OSIX_SUCCESS)
    {
/*
        if (pCapwapQueMsg->eCapwapQMsgType == CAPWAP_QUE_MSG1)
        {
            CAPWAP_TRC ((CAPWAP_QUE_TRC, "Process Capwap Que Msg1\n"));
            CapwapQueProcessApmePrims (pCapwapQueMsg->unCapwapMsg.pCapwapQMsg1);
        }
        MemReleaseMemBlock (gCapwapGlobals.queMsgMemPoolId,
                            (UINT1 *) pCapwapQueMsg);
*/
    }
    CAPWAP_FN_EXIT ();

}

/*-----------------------------------------------------------------------*/
/*                       End of the file  capwapque.c                      */
/*-----------------------------------------------------------------------*/
