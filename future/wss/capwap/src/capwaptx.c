/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved                      *
 * $Id: capwaptx.c,v 1.2 2017/05/23 14:16:49 siva Exp $       
 * Description: This file contains the CAPWAP Packet TX related functions    *
 *****************************************************************************/
#ifndef _CAPWAPTX_C_
#define _CAPWAPTX_C_

#include "capwapinc.h"
#define CPU_MAX_PKT_LEN 1500
extern INT1         nmhGetIfMtu (INT4, INT4 *);
/*****************************************************************************
 * Function     : CapwapTxMessage                                            *
 * Description  : Transmits the received capwap packet                       *
 * Input        : pBuf, pSess, u4PktLen                                      *
 * Output       : None                                                       *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *****************************************************************************/
INT4
CapwapTxMessage (tCRU_BUF_CHAIN_HEADER * pBuf,
                 tRemoteSessionManager * pSess, UINT4 u4PktLen, UINT1 u1PktType)
{
    INT4                i4RetVal = OSIX_SUCCESS;
    INT4                i4PMTU = 0;
#ifdef WLC_WANTED
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
#endif
    CAPWAP_FN_ENTRY ();

    if ((pBuf == NULL) || (pSess == NULL))
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapTxMessage: Invalid session entry \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "CapwapTxMessage: Invalid session entry \r\n"));
#ifdef WLC_WANTED
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
#endif
        return OSIX_FAILURE;

    }
#ifdef WLC_WANTED
    pWssIfCapwapDB->CapwapIsGetAllDB.bFragReassembleStatus = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = pSess->u2IntProfileId;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
        != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "WssIfProcessCapwapDBMsg failed"
                    "to get Fragmentation Reassemble status \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "WssIfProcessCapwapDBMsg failed"
                      "to get Fragmentation Reassemble status \r\n"));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    if (pWssIfCapwapDB->CapwapGetDB.u1FragReassembleStatus == OSIX_TRUE)
    {
        i4PMTU = (INT4) pSess->PMTU;
    }
    else
    {
        i4PMTU = CPU_MAX_PKT_LEN;
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
#else
    i4PMTU = pSess->PMTU;
    nmhGetIfMtu (1, &i4PMTU);
#endif

    if ((INT4) u4PktLen > i4PMTU)
    {
        /* Fragment the packet */
        if (CapwapFragmentAndSend
            (pBuf, (UINT4) i4PMTU, pSess, (INT2) u4PktLen,
             u1PktType) == OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "CapwapTxMessage:: Failed to Fragment the packet \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "CapwapTxMessage:: Failed to Fragment the packet \r\n"));
            i4RetVal = OSIX_FAILURE;
        }
        return i4RetVal;
    }
    else
    {
        i4RetVal = CapwapTxPktAfterFrag (pBuf, pSess, u4PktLen, u1PktType);
    }
    CAPWAP_FN_EXIT ();
    return i4RetVal;
}

/*****************************************************************************
 * Function     : CapwapTxPktAfterFrag                                       *
 * Description  : tx CAPWAP frag pkts                                        *
 * Input        : pBuf, pSess, u4PktLen                                      *
 * Output       : None                                                       *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *****************************************************************************/
INT4
CapwapTxPktAfterFrag (tCRU_BUF_CHAIN_HEADER * pBuf,
                      tRemoteSessionManager * pSess, UINT4 u4PktLen,
                      UINT1 u1PktType)
{
    INT4                i4RetVal = OSIX_SUCCESS;
    tQueData            QueData;

    CAPWAP_FN_ENTRY ();

    if (pSess->eCurrentState == CAPWAP_DISCOVERY)
    {
        /* Discovery Packets always sent as Plain Text */
        if (u1PktType == WSS_CAPWAP_802_11_CTRL_PKT)
        {
            i4RetVal = CapwapCtrlUdpTxPacket (pBuf,
                                              pSess->remoteIpAddr.
                                              u4_addr[0],
                                              pSess->u4RemoteCtrlPort,
                                              u4PktLen);
        }
        else
        {

            i4RetVal = CapwapDataUdpTxPacket (pBuf,
                                              pSess->remoteIpAddr.u4_addr[0],
                                              pSess->u4RemoteDataPort, u4PktLen,
                                              CAPWAP_CTRL_PKT_DSCP_CODE);
        }

    }
    else
    {
        if ((pSess->isRequestMsg == OSIX_TRUE) && (pSess->isResponsePending
                                                   == OSIX_TRUE))
        {
            if (UtlDynSllAdd (pSess->CapwapTxList, pBuf) == DYN_SLL_FAILURE)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Failed to add the received packet to the tx List \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to add the received packet to the tx List \r\n"));
                return OSIX_FAILURE;
            }
        }
        else
        {
#ifdef WLC_WANTED
            if ((pSess->u4DtlsSessId == 0) ||
                (pSess->u1DtlsFlag == DTLS_CFG_DISABLE))
#else
            if ((pSess->u1DtlsFlag == DTLS_CFG_DISABLE))
#endif
            {
                CAPWAP_TRC (CAPWAP_MGMT_TRC,
                            "DTLS is disabled send the plain text packet \r\n");
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                              "DTLS is disabled send the plain text packet \r\n"));
                if (u1PktType == WSS_CAPWAP_802_11_CTRL_PKT)
                {
                    i4RetVal = CapwapCtrlUdpTxPacket (pBuf,
                                                      pSess->remoteIpAddr.
                                                      u4_addr[0],
                                                      pSess->u4RemoteCtrlPort,
                                                      u4PktLen);
                }
                else
                {

                    i4RetVal = CapwapDataUdpTxPacket (pBuf,
                                                      pSess->remoteIpAddr.
                                                      u4_addr[0],
                                                      pSess->u4RemoteDataPort,
                                                      u4PktLen,
                                                      CAPWAP_CTRL_PKT_DSCP_CODE);
                }
            }
            else
            {
                QueData.pBuffer = pBuf;
                QueData.eQueDataType = DATA_ENCRYPT;
                QueData.u4BufSize = u4PktLen;
                QueData.u4IpAddr = pSess->remoteIpAddr.u4_addr[0];
                QueData.u4Id = pSess->u4DtlsSessId;

                if (u1PktType == WSS_CAPWAP_802_11_CTRL_PKT)
                {
                    QueData.u4Channel = DTLS_CTRL_CHANNEL;
                    QueData.u4PortNo = pSess->u4RemoteCtrlPort;
                    if (CapwapDtlsDecryptNotify (&QueData) == OSIX_FAILURE)
                    {
                        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                    "DTLS decryption Failed \r\n");
                        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                      "DTLS decryption Failed \r\n"));
                        return OSIX_FAILURE;
                    }
                }
                else
                {
                    if (pSess->u1DataDtlsFlag == DTLS_CFG_ENABLE)
                    {
                        QueData.u4Channel = DTLS_DATA_CHANNEL;
                        QueData.u4PortNo = pSess->u4RemoteDataPort;
                        if (CapwapDtlsDecryptNotify (&QueData) == OSIX_FAILURE)
                        {
                            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                        "DTLS decryption Failed \r\n");
                            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                          "DTLS decryption Failed \r\n"));
                            return OSIX_FAILURE;
                        }
                    }
                    else
                    {
                        i4RetVal = CapwapDataUdpTxPacket (pBuf,
                                                          pSess->remoteIpAddr.
                                                          u4_addr[0],
                                                          pSess->
                                                          u4RemoteDataPort,
                                                          u4PktLen,
                                                          CAPWAP_CTRL_PKT_DSCP_CODE);
                    }
                }
            }
        }
    }
    CAPWAP_FN_EXIT ();
    return i4RetVal;
}
#endif
