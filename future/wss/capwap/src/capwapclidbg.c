/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: capwapclidbg.c,v 1.2 2017/05/23 14:16:48 siva Exp $       
*
* Description: This file contains the routines for the protocol Database Access for the module Capwap 
*********************************************************************/

#include "capwapinc.h"
#include "wssifinc.h"
/****************************************************************************
 Function    :  CapwapGetAllCapwapBaseAcNameListTable
 Input       :  pCapwapGetCapwapBaseAcNameListEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetAllCapwapBaseAcNameListTable (tCapwapCapwapBaseAcNameListEntry *
                                       pCapwapGetCapwapBaseAcNameListEntry)
{
    tCapwapCapwapBaseAcNameListEntry *pCapwapCapwapBaseAcNameListEntry = NULL;

    /* Check whether the node is already present */
    pCapwapCapwapBaseAcNameListEntry =
        RBTreeGet (gCapwapGlobals.CapwapGlbMib.CapwapBaseAcNameListTable,
                   (tRBElem *) pCapwapGetCapwapBaseAcNameListEntry);

    if (pCapwapCapwapBaseAcNameListEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapGetAllCapwapBaseAcNameListTable: Entry doesn't exist\r\n");
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    MEMCPY (pCapwapGetCapwapBaseAcNameListEntry->MibObject.
            au1CapwapBaseAcNameListName,
            pCapwapCapwapBaseAcNameListEntry->MibObject.
            au1CapwapBaseAcNameListName,
            pCapwapCapwapBaseAcNameListEntry->MibObject.
            i4CapwapBaseAcNameListNameLen);

    pCapwapGetCapwapBaseAcNameListEntry->MibObject.
        i4CapwapBaseAcNameListNameLen =
        pCapwapCapwapBaseAcNameListEntry->MibObject.
        i4CapwapBaseAcNameListNameLen;

    pCapwapGetCapwapBaseAcNameListEntry->MibObject.
        u4CapwapBaseAcNameListPriority =
        pCapwapCapwapBaseAcNameListEntry->MibObject.
        u4CapwapBaseAcNameListPriority;

    pCapwapGetCapwapBaseAcNameListEntry->MibObject.
        i4CapwapBaseAcNameListRowStatus =
        pCapwapCapwapBaseAcNameListEntry->MibObject.
        i4CapwapBaseAcNameListRowStatus;

    if (CapwapGetAllUtlCapwapBaseAcNameListTable
        (pCapwapGetCapwapBaseAcNameListEntry,
         pCapwapCapwapBaseAcNameListEntry) == OSIX_FAILURE)

    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC, "CapwapGetAllCapwapBaseAcNameListTable:"
                    "CapwapGetAllUtlCapwapBaseAcNameListTable Returns Failure\r\n");

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetAllCapwapBaseMacAclTable
 Input       :  pCapwapGetCapwapBaseMacAclEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetAllCapwapBaseMacAclTable (tCapwapCapwapBaseMacAclEntry *
                                   pCapwapGetCapwapBaseMacAclEntry)
{
    tCapwapCapwapBaseMacAclEntry *pCapwapCapwapBaseMacAclEntry = NULL;

    /* Check whether the node is already present */
    pCapwapCapwapBaseMacAclEntry =
        RBTreeGet (gCapwapGlobals.CapwapGlbMib.CapwapBaseMacAclTable,
                   (tRBElem *) pCapwapGetCapwapBaseMacAclEntry);

    if (pCapwapCapwapBaseMacAclEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapGetAllCapwapBaseMacAclTable: Entry doesn't exist\r\n");
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    MEMCPY (pCapwapGetCapwapBaseMacAclEntry->MibObject.
            au1CapwapBaseMacAclStationId,
            pCapwapCapwapBaseMacAclEntry->MibObject.
            au1CapwapBaseMacAclStationId,
            pCapwapCapwapBaseMacAclEntry->MibObject.
            i4CapwapBaseMacAclStationIdLen);

    pCapwapGetCapwapBaseMacAclEntry->MibObject.i4CapwapBaseMacAclStationIdLen =
        pCapwapCapwapBaseMacAclEntry->MibObject.i4CapwapBaseMacAclStationIdLen;

    pCapwapGetCapwapBaseMacAclEntry->MibObject.i4CapwapBaseMacAclRowStatus =
        pCapwapCapwapBaseMacAclEntry->MibObject.i4CapwapBaseMacAclRowStatus;

    if (CapwapGetAllUtlCapwapBaseMacAclTable
        (pCapwapGetCapwapBaseMacAclEntry,
         pCapwapCapwapBaseMacAclEntry) == OSIX_FAILURE)

    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC, "CapwapGetAllCapwapBaseMacAclTable:"
                    "CapwapGetAllUtlCapwapBaseMacAclTable Returns Failure\r\n");

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetAllCapwapBaseWtpProfileTable
 Input       :  pCapwapGetCapwapBaseWtpProfileEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetAllCapwapBaseWtpProfileTable (tCapwapCapwapBaseWtpProfileEntry *
                                       pCapwapGetCapwapBaseWtpProfileEntry)
{
    tCapwapCapwapBaseWtpProfileEntry *pCapwapCapwapBaseWtpProfileEntry = NULL;
    UINT4               u4Wtpipaddr = 0;
    UINT4               u4Wtpnetmask = 0;
    UINT4               u4WtpGateway = 0;
    /* Check whether the node is already present */
    pCapwapCapwapBaseWtpProfileEntry =
        RBTreeGet (gCapwapGlobals.CapwapGlbMib.CapwapBaseWtpProfileTable,
                   (tRBElem *) pCapwapGetCapwapBaseWtpProfileEntry);

    if (pCapwapCapwapBaseWtpProfileEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapGetAllCapwapBaseWtpProfileTable: Entry doesn't exist\r\n");
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    MEMCPY (pCapwapGetCapwapBaseWtpProfileEntry->MibObject.
            au1CapwapBaseWtpProfileName,
            pCapwapCapwapBaseWtpProfileEntry->MibObject.
            au1CapwapBaseWtpProfileName,
            pCapwapCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileNameLen);

    pCapwapGetCapwapBaseWtpProfileEntry->MibObject.
        i4CapwapBaseWtpProfileNameLen =
        pCapwapCapwapBaseWtpProfileEntry->MibObject.
        i4CapwapBaseWtpProfileNameLen;

    MEMCPY (pCapwapGetCapwapBaseWtpProfileEntry->MibObject.
            au1CapwapBaseWtpProfileWtpMacAddress,
            pCapwapCapwapBaseWtpProfileEntry->MibObject.
            au1CapwapBaseWtpProfileWtpMacAddress,
            pCapwapCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileWtpMacAddressLen);

    pCapwapGetCapwapBaseWtpProfileEntry->MibObject.
        i4CapwapBaseWtpProfileWtpMacAddressLen =
        pCapwapCapwapBaseWtpProfileEntry->MibObject.
        i4CapwapBaseWtpProfileWtpMacAddressLen;

    MEMCPY (pCapwapGetCapwapBaseWtpProfileEntry->MibObject.
            au1CapwapBaseWtpProfileWtpModelNumber,
            pCapwapCapwapBaseWtpProfileEntry->MibObject.
            au1CapwapBaseWtpProfileWtpModelNumber,
            pCapwapCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileWtpModelNumberLen);

    pCapwapGetCapwapBaseWtpProfileEntry->MibObject.
        i4CapwapBaseWtpProfileWtpModelNumberLen =
        pCapwapCapwapBaseWtpProfileEntry->MibObject.
        i4CapwapBaseWtpProfileWtpModelNumberLen;

    MEMCPY (pCapwapGetCapwapBaseWtpProfileEntry->MibObject.
            au1CapwapBaseWtpProfileWtpName,
            pCapwapCapwapBaseWtpProfileEntry->MibObject.
            au1CapwapBaseWtpProfileWtpName,
            pCapwapCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileWtpNameLen);

    pCapwapGetCapwapBaseWtpProfileEntry->MibObject.
        i4CapwapBaseWtpProfileWtpNameLen =
        pCapwapCapwapBaseWtpProfileEntry->MibObject.
        i4CapwapBaseWtpProfileWtpNameLen;

    MEMCPY (pCapwapGetCapwapBaseWtpProfileEntry->MibObject.
            au1CapwapBaseWtpProfileWtpLocation,
            pCapwapCapwapBaseWtpProfileEntry->MibObject.
            au1CapwapBaseWtpProfileWtpLocation,
            pCapwapCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileWtpLocationLen);

    pCapwapGetCapwapBaseWtpProfileEntry->MibObject.
        i4CapwapBaseWtpProfileWtpLocationLen =
        pCapwapCapwapBaseWtpProfileEntry->MibObject.
        i4CapwapBaseWtpProfileWtpLocationLen;

    pCapwapGetCapwapBaseWtpProfileEntry->MibObject.
        i4CapwapBaseWtpProfileWtpStaticIpEnable =
        pCapwapCapwapBaseWtpProfileEntry->MibObject.
        i4CapwapBaseWtpProfileWtpStaticIpEnable;

    pCapwapGetCapwapBaseWtpProfileEntry->MibObject.
        i4FsCapwapFsAcTimestampTrigger =
        pCapwapCapwapBaseWtpProfileEntry->MibObject.
        i4FsCapwapFsAcTimestampTrigger;
    pCapwapGetCapwapBaseWtpProfileEntry->MibObject.
        i4FsCapwapFsAcTimestampTrigger =
        pCapwapCapwapBaseWtpProfileEntry->MibObject.
        i4FsCapwapFsAcTimestampTrigger;

    pCapwapGetCapwapBaseWtpProfileEntry->MibObject.
        i4CapwapBaseWtpProfileWtpStaticIpType =
        pCapwapCapwapBaseWtpProfileEntry->MibObject.
        i4CapwapBaseWtpProfileWtpStaticIpType;

    MEMCPY (&u4Wtpipaddr, pCapwapCapwapBaseWtpProfileEntry->MibObject.
            au1CapwapBaseWtpProfileWtpStaticIpAddress,
            pCapwapCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileWtpStaticIpAddressLen);
    u4Wtpipaddr = OSIX_HTONL (u4Wtpipaddr);

    MEMCPY (pCapwapGetCapwapBaseWtpProfileEntry->MibObject.
            au1CapwapBaseWtpProfileWtpStaticIpAddress,
            &u4Wtpipaddr, sizeof (u4Wtpipaddr));

    pCapwapGetCapwapBaseWtpProfileEntry->MibObject.
        i4CapwapBaseWtpProfileWtpStaticIpAddressLen =
        pCapwapCapwapBaseWtpProfileEntry->MibObject.
        i4CapwapBaseWtpProfileWtpStaticIpAddressLen;

    MEMCPY (&u4Wtpnetmask, pCapwapCapwapBaseWtpProfileEntry->MibObject.
            au1CapwapBaseWtpProfileWtpNetmask,
            pCapwapCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileWtpNetmaskLen);
    u4Wtpnetmask = OSIX_HTONL (u4Wtpnetmask);

    MEMCPY (pCapwapGetCapwapBaseWtpProfileEntry->MibObject.
            au1CapwapBaseWtpProfileWtpNetmask,
            &u4Wtpnetmask, sizeof (u4Wtpnetmask));

    pCapwapGetCapwapBaseWtpProfileEntry->MibObject.
        i4CapwapBaseWtpProfileWtpNetmaskLen =
        pCapwapCapwapBaseWtpProfileEntry->MibObject.
        i4CapwapBaseWtpProfileWtpNetmaskLen;

    MEMCPY (&u4WtpGateway, pCapwapCapwapBaseWtpProfileEntry->MibObject.
            au1CapwapBaseWtpProfileWtpGateway,
            pCapwapCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileWtpGatewayLen);
    u4WtpGateway = OSIX_HTONL (u4WtpGateway);

    MEMCPY (pCapwapGetCapwapBaseWtpProfileEntry->MibObject.
            au1CapwapBaseWtpProfileWtpGateway,
            &u4WtpGateway, sizeof (u4WtpGateway));

    pCapwapGetCapwapBaseWtpProfileEntry->MibObject.
        i4CapwapBaseWtpProfileWtpGatewayLen =
        pCapwapCapwapBaseWtpProfileEntry->MibObject.
        i4CapwapBaseWtpProfileWtpGatewayLen;

    pCapwapGetCapwapBaseWtpProfileEntry->MibObject.
        i4CapwapBaseWtpProfileWtpFallbackEnable =
        pCapwapCapwapBaseWtpProfileEntry->MibObject.
        i4CapwapBaseWtpProfileWtpFallbackEnable;

    pCapwapGetCapwapBaseWtpProfileEntry->MibObject.
        u4CapwapBaseWtpProfileWtpEchoInterval =
        pCapwapCapwapBaseWtpProfileEntry->MibObject.
        u4CapwapBaseWtpProfileWtpEchoInterval;

    pCapwapGetCapwapBaseWtpProfileEntry->MibObject.
        u4CapwapBaseWtpProfileWtpIdleTimeout =
        pCapwapCapwapBaseWtpProfileEntry->MibObject.
        u4CapwapBaseWtpProfileWtpIdleTimeout;

    pCapwapGetCapwapBaseWtpProfileEntry->MibObject.
        u4CapwapBaseWtpProfileWtpMaxDiscoveryInterval =
        pCapwapCapwapBaseWtpProfileEntry->MibObject.
        u4CapwapBaseWtpProfileWtpMaxDiscoveryInterval;

    pCapwapGetCapwapBaseWtpProfileEntry->MibObject.
        u4CapwapBaseWtpProfileWtpReportInterval =
        pCapwapCapwapBaseWtpProfileEntry->MibObject.
        u4CapwapBaseWtpProfileWtpReportInterval;

    pCapwapGetCapwapBaseWtpProfileEntry->MibObject.
        u4CapwapBaseWtpProfileWtpStatisticsTimer =
        pCapwapCapwapBaseWtpProfileEntry->MibObject.
        u4CapwapBaseWtpProfileWtpStatisticsTimer;

    pCapwapGetCapwapBaseWtpProfileEntry->MibObject.
        i4CapwapBaseWtpProfileWtpEcnSupport =
        pCapwapCapwapBaseWtpProfileEntry->MibObject.
        i4CapwapBaseWtpProfileWtpEcnSupport;

    pCapwapGetCapwapBaseWtpProfileEntry->MibObject.
        i4CapwapBaseWtpProfileRowStatus =
        pCapwapCapwapBaseWtpProfileEntry->MibObject.
        i4CapwapBaseWtpProfileRowStatus;

    pCapwapGetCapwapBaseWtpProfileEntry->u4SystemUpTime =
        pCapwapCapwapBaseWtpProfileEntry->u4SystemUpTime;

    if (CapwapGetAllUtlCapwapBaseWtpProfileTable
        (pCapwapGetCapwapBaseWtpProfileEntry,
         pCapwapCapwapBaseWtpProfileEntry) == OSIX_FAILURE)

    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC, "CapwapGetAllCapwapBaseWtpProfileTable:"
                    "CapwapGetAllUtlCapwapBaseWtpProfileTable Returns Failure\r\n");

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetAllCapwapBaseWtpStateTable
 Input       :  pCapwapGetCapwapBaseWtpStateEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetAllCapwapBaseWtpStateTable (tCapwapCapwapBaseWtpStateEntry *
                                     pCapwapGetCapwapBaseWtpStateEntry)
{
    tCapwapCapwapBaseWtpProfileEntry CapwapCapwapBaseWtpProfileEntry;
    tCapwapCapwapBaseWtpStateEntry *pCapwapCapwapBaseWtpStateEntry = NULL;
    tWssIfCapDB        *pWssIfCapDB = NULL;
    UINT4               u4WtpProfileId = 0;
    tMacAddr            WtpMacAddr;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&WtpMacAddr, 0, sizeof (tMacAddr));
    MEMSET (&CapwapCapwapBaseWtpProfileEntry, 0,
            sizeof (tCapwapCapwapBaseWtpProfileEntry));

    MEMCPY (WtpMacAddr,
            pCapwapGetCapwapBaseWtpStateEntry->MibObject.
            au1CapwapBaseWtpStateWtpId, MAC_ADDR_LEN);

    if (CapwapGetWtpProfileIdFromProfileMac (WtpMacAddr, &u4WtpProfileId) !=
        OSIX_SUCCESS)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
        return SNMP_FAILURE;
    }

    CapwapCapwapBaseWtpProfileEntry.MibObject.u4CapwapBaseWtpProfileId =
        u4WtpProfileId;

    if (CapwapGetAllCapwapBaseWtpProfileTable (&CapwapCapwapBaseWtpProfileEntry)
        != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC, "CapwapGetAllCapwapBaseWtpProfileTable:"
                    "CapwapGetAllUtlCapwapBaseWtpProfileTable Returns Failure\r\n");
        return OSIX_FAILURE;
    }

    pWssIfCapDB->CapwapGetDB.u2ProfileId = (UINT2) u4WtpProfileId;
    pWssIfCapDB->CapwapIsGetAllDB.bIpAddress = OSIX_TRUE;
    pWssIfCapDB->CapwapIsGetAllDB.bIpAddressType = OSIX_TRUE;
    pWssIfCapDB->CapwapIsGetAllDB.bCapwapState = OSIX_TRUE;
    pWssIfCapDB->CapwapIsGetAllDB.bWtpMacAddress = OSIX_TRUE;

    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_SHOW_WTPSTATE_TABLE,
                                 pWssIfCapDB) == OSIX_SUCCESS)
    {
        pCapwapGetCapwapBaseWtpStateEntry->MibObject.
            i4CapwapBaseWtpStateWtpIpAddressType = 1;

        MEMCPY (pCapwapGetCapwapBaseWtpStateEntry->MibObject.
                au1CapwapBaseWtpStateWtpIpAddress,
                pWssIfCapDB->CapwapGetDB.WtpStaticIpAddress.u4_addr,
                sizeof (pWssIfCapDB->CapwapGetDB.WtpStaticIpAddress.u4_addr));

        pCapwapGetCapwapBaseWtpStateEntry->MibObject.
            i4CapwapBaseWtpStateWtpIpAddressLen =
            sizeof (pWssIfCapDB->CapwapGetDB.WtpStaticIpAddress.u4_addr);

        pCapwapGetCapwapBaseWtpStateEntry->MibObject.
            i4CapwapBaseWtpStateWtpLocalIpAddressType = 0;

        MEMCPY (pCapwapGetCapwapBaseWtpStateEntry->MibObject.
                au1CapwapBaseWtpStateWtpLocalIpAddress,
                pWssIfCapDB->CapwapGetDB.WtpStaticIpAddress.u4_addr,
                sizeof (pWssIfCapDB->CapwapGetDB.WtpStaticIpAddress.u4_addr));

        pCapwapGetCapwapBaseWtpStateEntry->MibObject.
            i4CapwapBaseWtpStateWtpLocalIpAddressLen =
            sizeof (pWssIfCapDB->CapwapGetDB.WtpStaticIpAddress.u4_addr);

        MEMCPY (pCapwapGetCapwapBaseWtpStateEntry->MibObject.
                au1CapwapBaseWtpStateWtpBaseMacAddress,
                pWssIfCapDB->CapwapGetDB.WtpMacAddress, MAC_ADDR_LEN);

        pCapwapGetCapwapBaseWtpStateEntry->MibObject.
            i4CapwapBaseWtpStateWtpBaseMacAddressLen = MAC_ADDR_LEN;

        pCapwapGetCapwapBaseWtpStateEntry->MibObject.i4CapwapBaseWtpState =
            (INT4) pWssIfCapDB->CapwapGetDB.u4CapwapState;

        pCapwapGetCapwapBaseWtpStateEntry->MibObject.
            u4CapwapBaseWtpStateWtpUpTime =
            CapwapCapwapBaseWtpProfileEntry.u4SystemUpTime;
    }
    else
    {
        pCapwapGetCapwapBaseWtpStateEntry->MibObject.
            i4CapwapBaseWtpStateWtpIpAddressType = 1;

        pWssIfCapDB->CapwapGetDB.WtpStaticIpAddress.u4_addr[0] = 0;

        MEMCPY (pCapwapGetCapwapBaseWtpStateEntry->MibObject.
                au1CapwapBaseWtpStateWtpIpAddress,
                &pWssIfCapDB->CapwapGetDB.WtpStaticIpAddress.u4_addr[0],
                sizeof (pWssIfCapDB->CapwapGetDB.WtpStaticIpAddress.
                        u4_addr[0]));

        pCapwapGetCapwapBaseWtpStateEntry->MibObject.
            i4CapwapBaseWtpStateWtpIpAddressLen =
            sizeof (pWssIfCapDB->CapwapGetDB.WtpStaticIpAddress.u4_addr[0]);

        pCapwapGetCapwapBaseWtpStateEntry->MibObject.
            i4CapwapBaseWtpStateWtpLocalIpAddressType = 0;
        MEMCPY (pCapwapGetCapwapBaseWtpStateEntry->MibObject.
                au1CapwapBaseWtpStateWtpLocalIpAddress,
                &pWssIfCapDB->CapwapGetDB.WtpStaticIpAddress.u4_addr[0],
                sizeof (pWssIfCapDB->CapwapGetDB.WtpStaticIpAddress.
                        u4_addr[0]));

        pCapwapGetCapwapBaseWtpStateEntry->MibObject.
            i4CapwapBaseWtpStateWtpLocalIpAddressLen =
            sizeof (pWssIfCapDB->CapwapGetDB.WtpStaticIpAddress.u4_addr[0]);

        MEMCPY (pCapwapGetCapwapBaseWtpStateEntry->MibObject.
                au1CapwapBaseWtpStateWtpBaseMacAddress,
                pCapwapGetCapwapBaseWtpStateEntry->MibObject.
                au1CapwapBaseWtpStateWtpId, MAC_ADDR_LEN);

        pCapwapGetCapwapBaseWtpStateEntry->MibObject.
            i4CapwapBaseWtpStateWtpBaseMacAddressLen = MAC_ADDR_LEN;

        pCapwapGetCapwapBaseWtpStateEntry->MibObject.i4CapwapBaseWtpState = 0;

        pCapwapGetCapwapBaseWtpStateEntry->MibObject.
            u4CapwapBaseWtpStateWtpUpTime =
            CapwapCapwapBaseWtpProfileEntry.u4SystemUpTime;
    }

    /* Assign values from the existing node */

    pCapwapGetCapwapBaseWtpStateEntry->MibObject.
        u4CapwapBaseWtpStateWtpCurrWtpProfileId = u4WtpProfileId;

    if (CapwapGetAllUtlCapwapBaseWtpStateTable
        (pCapwapGetCapwapBaseWtpStateEntry,
         pCapwapCapwapBaseWtpStateEntry) == OSIX_FAILURE)

    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC, "CapwapGetAllCapwapBaseWtpStateTable:"
                    "CapwapGetAllUtlCapwapBaseWtpStateTable Returns Failure\r\n");

        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
        return OSIX_FAILURE;
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetAllCapwapBaseWtpTable
 Input       :  pCapwapGetCapwapBaseWtpEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetAllCapwapBaseWtpTable (tCapwapCapwapBaseWtpEntry *
                                pCapwapGetCapwapBaseWtpEntry)
{

    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    tCapwapCapwapBaseWtpEntry *pCapwapCapwapBaseWtpEntry = NULL;

    MEMCPY (pWssIfCapwapDB->CapwapGetDB.WtpMacAddress,
            pCapwapGetCapwapBaseWtpEntry->MibObject.au1CapwapBaseWtpCurrId,
            MAC_ADDR_LEN);

    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_MACADDR_MAPPING_ENTRY,
                                 pWssIfCapwapDB) != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapGetAllCapwap: Entry doesn't exist\r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;

    }

    pWssIfCapwapDB->CapwapGetDB.u2ProfileId =
        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId;

    pWssIfCapwapDB->CapwapIsGetAllDB.bNoOfRadio = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpPhyIndex = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bMacType = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpDiscType = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpMacAddress = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bTunnelMode = OSIX_TRUE;

    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                 pWssIfCapwapDB) != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapGetAllCapwapBaseWtpTable: Entry doesn't exist\r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;

    }

    /* Assign values from the existing node */
    pCapwapGetCapwapBaseWtpEntry->MibObject.i4CapwapBaseWtpPhyIndex = 0;

    MEMCPY (pCapwapGetCapwapBaseWtpEntry->MibObject.
            au1CapwapBaseWtpBaseMacAddress,
            pWssIfCapwapDB->CapwapGetDB.WtpMacAddress, MAC_ADDR_LEN);

    pCapwapGetCapwapBaseWtpEntry->MibObject.i4CapwapBaseWtpBaseMacAddressLen =
        MAC_ADDR_LEN;

    pCapwapGetCapwapBaseWtpEntry->MibObject.
        au1CapwapBaseWtpTunnelModeOptions[0] =
        pWssIfCapwapDB->CapwapGetDB.u1WtpTunnelMode;

    pCapwapGetCapwapBaseWtpEntry->MibObject.
        i4CapwapBaseWtpTunnelModeOptionsLen =
        sizeof (pWssIfCapwapDB->CapwapGetDB.u1WtpTunnelMode);

    pCapwapGetCapwapBaseWtpEntry->MibObject.i4CapwapBaseWtpMacTypeOptions =
        pWssIfCapwapDB->CapwapGetDB.u1WtpMacType;

    pCapwapGetCapwapBaseWtpEntry->MibObject.i4CapwapBaseWtpDiscoveryType =
        pWssIfCapwapDB->CapwapGetDB.u1WtpDiscType;

    pCapwapGetCapwapBaseWtpEntry->MibObject.u4CapwapBaseWtpRadiosInUseNum =
        pWssIfCapwapDB->CapwapGetDB.u1WtpNoofRadio;

    pCapwapGetCapwapBaseWtpEntry->MibObject.u4CapwapBaseWtpRadioNumLimit =
        pWssIfCapwapDB->CapwapGetDB.u1WtpNoofRadio;

    pCapwapGetCapwapBaseWtpEntry->MibObject.u4CapwapBaseWtpRetransmitCount = 0;

    if (CapwapGetAllUtlCapwapBaseWtpTable
        (pCapwapGetCapwapBaseWtpEntry,
         pCapwapCapwapBaseWtpEntry) == OSIX_FAILURE)

    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC, "CapwapGetAllCapwapBaseWtpTable:"
                    "CapwapGetAllUtlCapwapBaseWtpTable Returns Failure\r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetAllCapwapBaseWirelessBindingTable
 Input       :  pCapwapGetCapwapBaseWirelessBindingEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    CapwapGetAllCapwapBaseWirelessBindingTable
    (tCapwapCapwapBaseWirelessBindingEntry *
     pCapwapGetCapwapBaseWirelessBindingEntry)
{
    tCapwapCapwapBaseWirelessBindingEntry *pCapwapCapwapBaseWirelessBindingEntry
        = NULL;
    tCapwapFsCapwapWirelessBindingEntry CapwapFsCapwapWirelessBindingEntry;

    MEMSET (&CapwapFsCapwapWirelessBindingEntry, 0,
            sizeof (tCapwapFsCapwapWirelessBindingEntry));
#if 0
    /* Check whether the node is already present */
    pCapwapCapwapBaseWirelessBindingEntry =
        RBTreeGet (gCapwapGlobals.CapwapGlbMib.CapwapBaseWirelessBindingTable,
                   (tRBElem *) pCapwapGetCapwapBaseWirelessBindingEntry);

    if (pCapwapCapwapBaseWirelessBindingEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapGetAllCapwapBaseWirelessBindingTable: Entry doesn't exist\r\n");
        return OSIX_FAILURE;
    }
    /* Assign values from the existing node */
    pCapwapGetCapwapBaseWirelessBindingEntry->MibObject.
        i4CapwapBaseWirelessBindingVirtualRadioIfIndex =
        pCapwapCapwapBaseWirelessBindingEntry->MibObject.
        i4CapwapBaseWirelessBindingVirtualRadioIfIndex;

    pCapwapGetCapwapBaseWirelessBindingEntry->MibObject.
        i4CapwapBaseWirelessBindingType =
        pCapwapCapwapBaseWirelessBindingEntry->MibObject.
        i4CapwapBaseWirelessBindingType;

    if (CapwapGetAllUtlCapwapBaseWirelessBindingTable
        (pCapwapGetCapwapBaseWirelessBindingEntry,
         pCapwapCapwapBaseWirelessBindingEntry) == OSIX_FAILURE)

    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapGetAllCapwapBaseWirelessBindingTable:"
                    "CapwapGetAllUtlCapwapBaseWirelessBindingTable Returns Failure\r\n");

        return OSIX_FAILURE;
    }
#endif
    CapwapFsCapwapWirelessBindingEntry.MibObject.u4CapwapBaseWtpProfileId =
        pCapwapGetCapwapBaseWirelessBindingEntry->MibObject.
        u4CapwapBaseWtpProfileId;

    CapwapFsCapwapWirelessBindingEntry.MibObject.
        u4CapwapBaseWirelessBindingRadioId =
        pCapwapGetCapwapBaseWirelessBindingEntry->MibObject.
        u4CapwapBaseWirelessBindingRadioId;

    if (CapwapGetAllFsCapwapWirelessBindingTable
        (&CapwapFsCapwapWirelessBindingEntry) != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapGetAllCapwapBaseWirelessBindingTable: Entry doesn't exist\r\n");
        return OSIX_FAILURE;

    }
    /* Assign values from the existing node */
    pCapwapGetCapwapBaseWirelessBindingEntry->MibObject.
        i4CapwapBaseWirelessBindingVirtualRadioIfIndex =
        CapwapFsCapwapWirelessBindingEntry.MibObject.
        i4FsCapwapWirelessBindingVirtualRadioIfIndex;

    pCapwapGetCapwapBaseWirelessBindingEntry->MibObject.
        i4CapwapBaseWirelessBindingType =
        CapwapFsCapwapWirelessBindingEntry.MibObject.
        i4FsCapwapWirelessBindingType;

    if (CapwapGetAllUtlCapwapBaseWirelessBindingTable
        (pCapwapGetCapwapBaseWirelessBindingEntry,
         pCapwapCapwapBaseWirelessBindingEntry) == OSIX_FAILURE)

    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapGetAllCapwapBaseWirelessBindingTable:"
                    "CapwapGetAllUtlCapwapBaseWirelessBindingTable Returns Failure\r\n");

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetAllCapwapBaseStationTable
 Input       :  pCapwapGetCapwapBaseStationEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetAllCapwapBaseStationTable (tCapwapCapwapBaseStationEntry *
                                    pCapwapGetCapwapBaseStationEntry)
{
    tCapwapCapwapBaseStationEntry *pCapwapCapwapBaseStationEntry = NULL;

    /* Check whether the node is already present */
    pCapwapCapwapBaseStationEntry =
        RBTreeGet (gCapwapGlobals.CapwapGlbMib.CapwapBaseStationTable,
                   (tRBElem *) pCapwapGetCapwapBaseStationEntry);

    if (pCapwapCapwapBaseStationEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapGetAllCapwapBaseStationTable: Entry doesn't exist\r\n");
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    MEMCPY (pCapwapGetCapwapBaseStationEntry->MibObject.
            au1CapwapBaseStationWtpId,
            pCapwapCapwapBaseStationEntry->MibObject.au1CapwapBaseStationWtpId,
            pCapwapCapwapBaseStationEntry->MibObject.
            i4CapwapBaseStationWtpIdLen);

    pCapwapGetCapwapBaseStationEntry->MibObject.i4CapwapBaseStationWtpIdLen =
        pCapwapCapwapBaseStationEntry->MibObject.i4CapwapBaseStationWtpIdLen;

    pCapwapGetCapwapBaseStationEntry->MibObject.u4CapwapBaseStationWtpRadioId =
        pCapwapCapwapBaseStationEntry->MibObject.u4CapwapBaseStationWtpRadioId;

    MEMCPY (pCapwapGetCapwapBaseStationEntry->MibObject.
            au1CapwapBaseStationAddedTime,
            pCapwapCapwapBaseStationEntry->MibObject.
            au1CapwapBaseStationAddedTime,
            pCapwapCapwapBaseStationEntry->MibObject.
            i4CapwapBaseStationAddedTimeLen);

    pCapwapGetCapwapBaseStationEntry->MibObject.
        i4CapwapBaseStationAddedTimeLen =
        pCapwapCapwapBaseStationEntry->MibObject.
        i4CapwapBaseStationAddedTimeLen;

    MEMCPY (pCapwapGetCapwapBaseStationEntry->MibObject.
            au1CapwapBaseStationVlanName,
            pCapwapCapwapBaseStationEntry->MibObject.
            au1CapwapBaseStationVlanName,
            pCapwapCapwapBaseStationEntry->MibObject.
            i4CapwapBaseStationVlanNameLen);

    pCapwapGetCapwapBaseStationEntry->MibObject.i4CapwapBaseStationVlanNameLen =
        pCapwapCapwapBaseStationEntry->MibObject.i4CapwapBaseStationVlanNameLen;

    if (CapwapGetAllUtlCapwapBaseStationTable
        (pCapwapGetCapwapBaseStationEntry,
         pCapwapCapwapBaseStationEntry) == OSIX_FAILURE)

    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC, "CapwapGetAllCapwapBaseStationTable:"
                    "CapwapGetAllUtlCapwapBaseStationTable Returns Failure\r\n");

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetAllCapwapBaseWtpEventsStatsTable
 Input       :  pCapwapGetCapwapBaseWtpEventsStatsEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetAllCapwapBaseWtpEventsStatsTable (tCapwapCapwapBaseWtpEventsStatsEntry
                                           *
                                           pCapwapGetCapwapBaseWtpEventsStatsEntry)
{
    tCapwapCapwapBaseWtpEventsStatsEntry *pCapwapCapwapBaseWtpEventsStatsEntry =
        NULL;

    /* Check whether the node is already present */
    pCapwapCapwapBaseWtpEventsStatsEntry =
        RBTreeGet (gCapwapGlobals.CapwapGlbMib.CapwapBaseWtpEventsStatsTable,
                   (tRBElem *) pCapwapGetCapwapBaseWtpEventsStatsEntry);

    if (pCapwapCapwapBaseWtpEventsStatsEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapGetAllCapwapBaseWtpEventsStatsTable: Entry doesn't exist\r\n");
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pCapwapGetCapwapBaseWtpEventsStatsEntry->MibObject.
        u4CapwapBaseWtpEventsStatsRebootCount =
        pCapwapCapwapBaseWtpEventsStatsEntry->MibObject.
        u4CapwapBaseWtpEventsStatsRebootCount;

    pCapwapGetCapwapBaseWtpEventsStatsEntry->MibObject.
        u4CapwapBaseWtpEventsStatsInitCount =
        pCapwapCapwapBaseWtpEventsStatsEntry->MibObject.
        u4CapwapBaseWtpEventsStatsInitCount;

    pCapwapGetCapwapBaseWtpEventsStatsEntry->MibObject.
        u4CapwapBaseWtpEventsStatsLinkFailureCount =
        pCapwapCapwapBaseWtpEventsStatsEntry->MibObject.
        u4CapwapBaseWtpEventsStatsLinkFailureCount;

    pCapwapGetCapwapBaseWtpEventsStatsEntry->MibObject.
        u4CapwapBaseWtpEventsStatsSwFailureCount =
        pCapwapCapwapBaseWtpEventsStatsEntry->MibObject.
        u4CapwapBaseWtpEventsStatsSwFailureCount;

    pCapwapGetCapwapBaseWtpEventsStatsEntry->MibObject.
        u4CapwapBaseWtpEventsStatsHwFailureCount =
        pCapwapCapwapBaseWtpEventsStatsEntry->MibObject.
        u4CapwapBaseWtpEventsStatsHwFailureCount;

    pCapwapGetCapwapBaseWtpEventsStatsEntry->MibObject.
        u4CapwapBaseWtpEventsStatsOtherFailureCount =
        pCapwapCapwapBaseWtpEventsStatsEntry->MibObject.
        u4CapwapBaseWtpEventsStatsOtherFailureCount;

    pCapwapGetCapwapBaseWtpEventsStatsEntry->MibObject.
        u4CapwapBaseWtpEventsStatsUnknownFailureCount =
        pCapwapCapwapBaseWtpEventsStatsEntry->MibObject.
        u4CapwapBaseWtpEventsStatsUnknownFailureCount;

    pCapwapGetCapwapBaseWtpEventsStatsEntry->MibObject.
        i4CapwapBaseWtpEventsStatsLastFailureType =
        pCapwapCapwapBaseWtpEventsStatsEntry->MibObject.
        i4CapwapBaseWtpEventsStatsLastFailureType;

    if (CapwapGetAllUtlCapwapBaseWtpEventsStatsTable
        (pCapwapGetCapwapBaseWtpEventsStatsEntry,
         pCapwapCapwapBaseWtpEventsStatsEntry) == OSIX_FAILURE)

    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapGetAllCapwapBaseWtpEventsStatsTable:"
                    "CapwapGetAllUtlCapwapBaseWtpEventsStatsTable Returns Failure\r\n");

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetAllCapwapBaseRadioEventsStatsTable
 Input       :  pCapwapGetCapwapBaseRadioEventsStatsEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    CapwapGetAllCapwapBaseRadioEventsStatsTable
    (tCapwapCapwapBaseRadioEventsStatsEntry *
     pCapwapGetCapwapBaseRadioEventsStatsEntry)
{
    tCapwapCapwapBaseRadioEventsStatsEntry
        * pCapwapCapwapBaseRadioEventsStatsEntry = NULL;

    /* Check whether the node is already present */
    pCapwapCapwapBaseRadioEventsStatsEntry =
        RBTreeGet (gCapwapGlobals.CapwapGlbMib.CapwapBaseRadioEventsStatsTable,
                   (tRBElem *) pCapwapGetCapwapBaseRadioEventsStatsEntry);

    if (pCapwapCapwapBaseRadioEventsStatsEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapGetAllCapwapBaseRadioEventsStatsTable: Entry doesn't exist\r\n");
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pCapwapGetCapwapBaseRadioEventsStatsEntry->MibObject.
        u4CapwapBaseRadioEventsStatsResetCount =
        pCapwapCapwapBaseRadioEventsStatsEntry->MibObject.
        u4CapwapBaseRadioEventsStatsResetCount;

    pCapwapGetCapwapBaseRadioEventsStatsEntry->MibObject.
        u4CapwapBaseRadioEventsStatsSwFailureCount =
        pCapwapCapwapBaseRadioEventsStatsEntry->MibObject.
        u4CapwapBaseRadioEventsStatsSwFailureCount;

    pCapwapGetCapwapBaseRadioEventsStatsEntry->MibObject.
        u4CapwapBaseRadioEventsStatsHwFailureCount =
        pCapwapCapwapBaseRadioEventsStatsEntry->MibObject.
        u4CapwapBaseRadioEventsStatsHwFailureCount;

    pCapwapGetCapwapBaseRadioEventsStatsEntry->MibObject.
        u4CapwapBaseRadioEventsStatsOtherFailureCount =
        pCapwapCapwapBaseRadioEventsStatsEntry->MibObject.
        u4CapwapBaseRadioEventsStatsOtherFailureCount;

    pCapwapGetCapwapBaseRadioEventsStatsEntry->MibObject.
        u4CapwapBaseRadioEventsStatsUnknownFailureCount =
        pCapwapCapwapBaseRadioEventsStatsEntry->MibObject.
        u4CapwapBaseRadioEventsStatsUnknownFailureCount;

    pCapwapGetCapwapBaseRadioEventsStatsEntry->MibObject.
        u4CapwapBaseRadioEventsStatsConfigUpdateCount =
        pCapwapCapwapBaseRadioEventsStatsEntry->MibObject.
        u4CapwapBaseRadioEventsStatsConfigUpdateCount;

    pCapwapGetCapwapBaseRadioEventsStatsEntry->MibObject.
        u4CapwapBaseRadioEventsStatsChannelChangeCount =
        pCapwapCapwapBaseRadioEventsStatsEntry->MibObject.
        u4CapwapBaseRadioEventsStatsChannelChangeCount;

    pCapwapGetCapwapBaseRadioEventsStatsEntry->MibObject.
        u4CapwapBaseRadioEventsStatsBandChangeCount =
        pCapwapCapwapBaseRadioEventsStatsEntry->MibObject.
        u4CapwapBaseRadioEventsStatsBandChangeCount;

    pCapwapGetCapwapBaseRadioEventsStatsEntry->MibObject.
        i4CapwapBaseRadioEventsStatsCurrNoiseFloor =
        pCapwapCapwapBaseRadioEventsStatsEntry->MibObject.
        i4CapwapBaseRadioEventsStatsCurrNoiseFloor;

    pCapwapGetCapwapBaseRadioEventsStatsEntry->MibObject.
        u4CapwapBaseRadioEventsStatsDecryptErrorCount =
        pCapwapCapwapBaseRadioEventsStatsEntry->MibObject.
        u4CapwapBaseRadioEventsStatsDecryptErrorCount;

    pCapwapGetCapwapBaseRadioEventsStatsEntry->MibObject.
        i4CapwapBaseRadioEventsStatsLastFailureType =
        pCapwapCapwapBaseRadioEventsStatsEntry->MibObject.
        i4CapwapBaseRadioEventsStatsLastFailureType;

    if (CapwapGetAllUtlCapwapBaseRadioEventsStatsTable
        (pCapwapGetCapwapBaseRadioEventsStatsEntry,
         pCapwapCapwapBaseRadioEventsStatsEntry) == OSIX_FAILURE)

    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapGetAllCapwapBaseRadioEventsStatsTable:"
                    "CapwapGetAllUtlCapwapBaseRadioEventsStatsTable Returns Failure\r\n");

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetAllFsWtpModelTable
 Input       :  pCapwapGetFsWtpModelEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetAllFsWtpModelTable (tCapwapFsWtpModelEntry * pCapwapGetFsWtpModelEntry)
{
    tCapwapFsWtpModelEntry *pCapwapFsWtpModelEntry = NULL;

    /* Check whether the node is already present */
    pCapwapFsWtpModelEntry =
        RBTreeGet (gCapwapGlobals.CapwapGlbMib.FsWtpModelTable,
                   (tRBElem *) pCapwapGetFsWtpModelEntry);

    if (pCapwapFsWtpModelEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapGetAllFsWtpModelTable: Entry doesn't exist\r\n");
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pCapwapGetFsWtpModelEntry->MibObject.u4FsNoOfRadio =
        pCapwapFsWtpModelEntry->MibObject.u4FsNoOfRadio;

    pCapwapGetFsWtpModelEntry->MibObject.i4FsCapwapWtpMacType =
        pCapwapFsWtpModelEntry->MibObject.i4FsCapwapWtpMacType;

    MEMCPY (pCapwapGetFsWtpModelEntry->MibObject.au1FsCapwapWtpTunnelMode,
            pCapwapFsWtpModelEntry->MibObject.au1FsCapwapWtpTunnelMode,
            pCapwapFsWtpModelEntry->MibObject.i4FsCapwapWtpTunnelModeLen);

    pCapwapGetFsWtpModelEntry->MibObject.i4FsCapwapWtpTunnelModeLen =
        pCapwapFsWtpModelEntry->MibObject.i4FsCapwapWtpTunnelModeLen;

    MEMCPY (pCapwapGetFsWtpModelEntry->MibObject.au1FsCapwapImageName,
            pCapwapFsWtpModelEntry->MibObject.au1FsCapwapImageName,
            pCapwapFsWtpModelEntry->MibObject.i4FsCapwapImageNameLen);

    pCapwapGetFsWtpModelEntry->MibObject.i4FsCapwapImageNameLen =
        pCapwapFsWtpModelEntry->MibObject.i4FsCapwapImageNameLen;

    MEMCPY (pCapwapGetFsWtpModelEntry->MibObject.au1FsCapwapQosProfileName,
            pCapwapFsWtpModelEntry->MibObject.au1FsCapwapQosProfileName,
            pCapwapFsWtpModelEntry->MibObject.i4FsCapwapQosProfileNameLen);

    pCapwapGetFsWtpModelEntry->MibObject.i4FsCapwapQosProfileNameLen =
        pCapwapFsWtpModelEntry->MibObject.i4FsCapwapQosProfileNameLen;

    pCapwapGetFsWtpModelEntry->MibObject.i4FsMaxStations =
        pCapwapFsWtpModelEntry->MibObject.i4FsMaxStations;

    pCapwapGetFsWtpModelEntry->MibObject.i4FsWtpModelRowStatus =
        pCapwapFsWtpModelEntry->MibObject.i4FsWtpModelRowStatus;

    MEMCPY (pCapwapGetFsWtpModelEntry->au1LastUpdated,
            pCapwapFsWtpModelEntry->au1LastUpdated,
            pCapwapFsWtpModelEntry->i4LastUpdatedLen);
    pCapwapGetFsWtpModelEntry->i4LastUpdatedLen =
        pCapwapFsWtpModelEntry->i4LastUpdatedLen;

    if (CapwapGetAllUtlFsWtpModelTable
        (pCapwapGetFsWtpModelEntry, pCapwapFsWtpModelEntry) == OSIX_FAILURE)

    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC, "CapwapGetAllFsWtpModelTable:"
                    "CapwapGetAllUtlFsWtpModelTable Returns Failure\r\n");

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetAllFsWtpRadioTable
 Input       :  pCapwapGetFsWtpRadioEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetAllFsWtpRadioTable (tCapwapFsWtpRadioEntry * pCapwapGetFsWtpRadioEntry)
{
    tCapwapFsWtpRadioEntry *pCapwapFsWtpRadioEntry = NULL;

    /* Check whether the node is already present */
    pCapwapFsWtpRadioEntry =
        RBTreeGet (gCapwapGlobals.CapwapGlbMib.FsWtpRadioTable,
                   (tRBElem *) pCapwapGetFsWtpRadioEntry);

    if (pCapwapFsWtpRadioEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapGetAllFsWtpRadioTable: Entry doesn't exist\r\n");
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pCapwapGetFsWtpRadioEntry->MibObject.u4FsWtpRadioType =
        pCapwapFsWtpRadioEntry->MibObject.u4FsWtpRadioType;

    pCapwapGetFsWtpRadioEntry->MibObject.i4FsRadioAdminStatus =
        pCapwapFsWtpRadioEntry->MibObject.i4FsRadioAdminStatus;

    pCapwapGetFsWtpRadioEntry->MibObject.i4FsMaxSSIDSupported =
        pCapwapFsWtpRadioEntry->MibObject.i4FsMaxSSIDSupported;

    pCapwapGetFsWtpRadioEntry->MibObject.i4FsWtpRadioRowStatus =
        pCapwapFsWtpRadioEntry->MibObject.i4FsWtpRadioRowStatus;

    if (CapwapGetAllUtlFsWtpRadioTable
        (pCapwapGetFsWtpRadioEntry, pCapwapFsWtpRadioEntry) == OSIX_FAILURE)

    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC, "CapwapGetAllFsWtpRadioTable:"
                    "CapwapGetAllUtlFsWtpRadioTable Returns Failure\r\n");

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetAllFsCapwapWhiteListTable
 Input       :  pCapwapGetFsCapwapWhiteListEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetAllFsCapwapWhiteListTable (tCapwapFsCapwapWhiteListEntry *
                                    pCapwapGetFsCapwapWhiteListEntry)
{
    tCapwapFsCapwapWhiteListEntry *pCapwapFsCapwapWhiteListEntry = NULL;

    /* Check whether the node is already present */
    pCapwapFsCapwapWhiteListEntry =
        RBTreeGet (gCapwapGlobals.CapwapGlbMib.FsCapwapWhiteListTable,
                   (tRBElem *) pCapwapGetFsCapwapWhiteListEntry);

    if (pCapwapFsCapwapWhiteListEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapGetAllFsCapwapWhiteListTable: Entry doesn't exist\r\n");
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    MEMCPY (pCapwapGetFsCapwapWhiteListEntry->MibObject.
            au1FsCapwapWhiteListWtpBaseMac,
            pCapwapFsCapwapWhiteListEntry->MibObject.
            au1FsCapwapWhiteListWtpBaseMac,
            pCapwapFsCapwapWhiteListEntry->MibObject.
            i4FsCapwapWhiteListWtpBaseMacLen);

    pCapwapGetFsCapwapWhiteListEntry->MibObject.
        i4FsCapwapWhiteListWtpBaseMacLen =
        pCapwapFsCapwapWhiteListEntry->MibObject.
        i4FsCapwapWhiteListWtpBaseMacLen;

    pCapwapGetFsCapwapWhiteListEntry->MibObject.i4FsCapwapWhiteListRowStatus =
        pCapwapFsCapwapWhiteListEntry->MibObject.i4FsCapwapWhiteListRowStatus;

    if (CapwapGetAllUtlFsCapwapWhiteListTable
        (pCapwapGetFsCapwapWhiteListEntry,
         pCapwapFsCapwapWhiteListEntry) == OSIX_FAILURE)

    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC, "CapwapGetAllFsCapwapWhiteListTable:"
                    "CapwapGetAllUtlFsCapwapWhiteListTable Returns Failure\r\n");

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetAllFsCapwapBlackList
 Input       :  pCapwapGetFsCapwapBlackListEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetAllFsCapwapBlackList (tCapwapFsCapwapBlackListEntry *
                               pCapwapGetFsCapwapBlackListEntry)
{
    tCapwapFsCapwapBlackListEntry *pCapwapFsCapwapBlackListEntry = NULL;

    /* Check whether the node is already present */
    pCapwapFsCapwapBlackListEntry =
        RBTreeGet (gCapwapGlobals.CapwapGlbMib.FsCapwapBlackList,
                   (tRBElem *) pCapwapGetFsCapwapBlackListEntry);

    if (pCapwapFsCapwapBlackListEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapGetAllFsCapwapBlackList: Entry doesn't exist\r\n");
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    MEMCPY (pCapwapGetFsCapwapBlackListEntry->MibObject.
            au1FsCapwapBlackListWtpBaseMac,
            pCapwapFsCapwapBlackListEntry->MibObject.
            au1FsCapwapBlackListWtpBaseMac,
            pCapwapFsCapwapBlackListEntry->MibObject.
            i4FsCapwapBlackListWtpBaseMacLen);

    pCapwapGetFsCapwapBlackListEntry->MibObject.
        i4FsCapwapBlackListWtpBaseMacLen =
        pCapwapFsCapwapBlackListEntry->MibObject.
        i4FsCapwapBlackListWtpBaseMacLen;

    pCapwapGetFsCapwapBlackListEntry->MibObject.i4FsCapwapBlackListRowStatus =
        pCapwapFsCapwapBlackListEntry->MibObject.i4FsCapwapBlackListRowStatus;

    if (CapwapGetAllUtlFsCapwapBlackList
        (pCapwapGetFsCapwapBlackListEntry,
         pCapwapFsCapwapBlackListEntry) == OSIX_FAILURE)

    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC, "CapwapGetAllFsCapwapBlackList:"
                    "CapwapGetAllUtlFsCapwapBlackList Returns Failure\r\n");

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetAllFsCapwapWtpConfigTable
 Input       :  pCapwapGetFsCapwapWtpConfigEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetAllFsCapwapWtpConfigTable (tCapwapFsCapwapWtpConfigEntry *
                                    pCapwapGetFsCapwapWtpConfigEntry)
{
    tCapwapFsCapwapWtpConfigEntry *pCapwapFsCapwapWtpConfigEntry = NULL;

    /* Check whether the node is already present */
    pCapwapFsCapwapWtpConfigEntry =
        RBTreeGet (gCapwapGlobals.CapwapGlbMib.FsCapwapWtpConfigTable,
                   (tRBElem *) pCapwapGetFsCapwapWtpConfigEntry);

    if (pCapwapFsCapwapWtpConfigEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapGetAllFsCapwapWtpConfigTable: Entry doesn't exist\r\n");
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pCapwapGetFsCapwapWtpConfigEntry->MibObject.i4FsCapwapWtpReset =
        pCapwapFsCapwapWtpConfigEntry->MibObject.i4FsCapwapWtpReset;

    pCapwapGetFsCapwapWtpConfigEntry->MibObject.i4FsCapwapClearConfig =
        pCapwapFsCapwapWtpConfigEntry->MibObject.i4FsCapwapClearConfig;

    pCapwapGetFsCapwapWtpConfigEntry->MibObject.i4FsWtpDiscoveryType =
        pCapwapFsCapwapWtpConfigEntry->MibObject.i4FsWtpDiscoveryType;

    MEMCPY (pCapwapGetFsCapwapWtpConfigEntry->MibObject.au1FsWtpCountryString,
            pCapwapFsCapwapWtpConfigEntry->MibObject.au1FsWtpCountryString,
            pCapwapFsCapwapWtpConfigEntry->MibObject.i4FsWtpCountryStringLen);

    pCapwapGetFsCapwapWtpConfigEntry->MibObject.i4FsWtpCountryStringLen =
        pCapwapFsCapwapWtpConfigEntry->MibObject.i4FsWtpCountryStringLen;

    MEMCPY (pCapwapGetFsCapwapWtpConfigEntry->MibObject.
            au1FsWtpCrashDumpFileName,
            pCapwapFsCapwapWtpConfigEntry->MibObject.au1FsWtpCrashDumpFileName,
            pCapwapFsCapwapWtpConfigEntry->MibObject.
            i4FsWtpCrashDumpFileNameLen);

    pCapwapGetFsCapwapWtpConfigEntry->MibObject.i4FsWtpCrashDumpFileNameLen =
        pCapwapFsCapwapWtpConfigEntry->MibObject.i4FsWtpCrashDumpFileNameLen;

    MEMCPY (pCapwapGetFsCapwapWtpConfigEntry->MibObject.
            au1FsWtpMemoryDumpFileName,
            pCapwapFsCapwapWtpConfigEntry->MibObject.au1FsWtpMemoryDumpFileName,
            pCapwapFsCapwapWtpConfigEntry->MibObject.
            i4FsWtpMemoryDumpFileNameLen);

    pCapwapGetFsCapwapWtpConfigEntry->MibObject.i4FsWtpMemoryDumpFileNameLen =
        pCapwapFsCapwapWtpConfigEntry->MibObject.i4FsWtpMemoryDumpFileNameLen;

    pCapwapGetFsCapwapWtpConfigEntry->MibObject.i4FsCapwapClearApStats =
        pCapwapFsCapwapWtpConfigEntry->MibObject.i4FsCapwapClearApStats;

    pCapwapGetFsCapwapWtpConfigEntry->MibObject.i4FsCapwapWtpConfigRowStatus =
        pCapwapFsCapwapWtpConfigEntry->MibObject.i4FsCapwapWtpConfigRowStatus;

    pCapwapGetFsCapwapWtpConfigEntry->MibObject.u4FsWtpDeleteOperation =
        pCapwapFsCapwapWtpConfigEntry->MibObject.u4FsWtpDeleteOperation;

    pCapwapGetFsCapwapWtpConfigEntry->MibObject.u4FsWlcStaticIpAddress =
        pCapwapFsCapwapWtpConfigEntry->MibObject.u4FsWlcStaticIpAddress;

    if (CapwapGetAllUtlFsCapwapWtpConfigTable
        (pCapwapGetFsCapwapWtpConfigEntry,
         pCapwapFsCapwapWtpConfigEntry) == OSIX_FAILURE)

    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC, "CapwapGetAllFsCapwapWtpConfigTable:"
                    "CapwapGetAllUtlFsCapwapWtpConfigTable Returns Failure\r\n");

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetAllFsCapwapLinkEncryptionTable
 Input       :  pCapwapGetFsCapwapLinkEncryptionEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetAllFsCapwapLinkEncryptionTable (tCapwapFsCapwapLinkEncryptionEntry *
                                         pCapwapGetFsCapwapLinkEncryptionEntry)
{
    tCapwapFsCapwapLinkEncryptionEntry *pCapwapFsCapwapLinkEncryptionEntry =
        NULL;

    /* Check whether the node is already present */
    pCapwapFsCapwapLinkEncryptionEntry =
        RBTreeGet (gCapwapGlobals.CapwapGlbMib.FsCapwapLinkEncryptionTable,
                   (tRBElem *) pCapwapGetFsCapwapLinkEncryptionEntry);

    if (pCapwapFsCapwapLinkEncryptionEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapGetAllFsCapwapLinkEncryptionTable: Entry doesn't exist\r\n");
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pCapwapGetFsCapwapLinkEncryptionEntry->MibObject.
        i4FsCapwapEncryptChannelStatus =
        pCapwapFsCapwapLinkEncryptionEntry->MibObject.
        i4FsCapwapEncryptChannelStatus;

    pCapwapGetFsCapwapLinkEncryptionEntry->MibObject.
        i4FsCapwapEncryptChannelRowStatus =
        pCapwapFsCapwapLinkEncryptionEntry->MibObject.
        i4FsCapwapEncryptChannelRowStatus;

    if (CapwapGetAllUtlFsCapwapLinkEncryptionTable
        (pCapwapGetFsCapwapLinkEncryptionEntry,
         pCapwapFsCapwapLinkEncryptionEntry) == OSIX_FAILURE)

    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC, "CapwapGetAllFsCapwapLinkEncryptionTable:"
                    "CapwapGetAllUtlFsCapwapLinkEncryptionTable Returns Failure\r\n");

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetAllFsCawapDefaultWtpProfileTable
 Input       :  pCapwapGetFsCapwapDefaultWtpProfileEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetAllFsCawapDefaultWtpProfileTable (tCapwapFsCapwapDefaultWtpProfileEntry
                                           *
                                           pCapwapGetFsCapwapDefaultWtpProfileEntry)
{
    tCapwapFsCapwapDefaultWtpProfileEntry *pCapwapFsCapwapDefaultWtpProfileEntry
        = NULL;

    /* Check whether the node is already present */
    pCapwapFsCapwapDefaultWtpProfileEntry =
        RBTreeGet (gCapwapGlobals.CapwapGlbMib.FsCawapDefaultWtpProfileTable,
                   (tRBElem *) pCapwapGetFsCapwapDefaultWtpProfileEntry);

    if (pCapwapFsCapwapDefaultWtpProfileEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapGetAllFsCawapDefaultWtpProfileTable: Entry doesn't exist\r\n");
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pCapwapGetFsCapwapDefaultWtpProfileEntry->MibObject.
        i4FsCapwapDefaultWtpProfileWtpFallbackEnable =
        pCapwapFsCapwapDefaultWtpProfileEntry->MibObject.
        i4FsCapwapDefaultWtpProfileWtpFallbackEnable;

    pCapwapGetFsCapwapDefaultWtpProfileEntry->MibObject.
        u4FsCapwapDefaultWtpProfileWtpEchoInterval =
        pCapwapFsCapwapDefaultWtpProfileEntry->MibObject.
        u4FsCapwapDefaultWtpProfileWtpEchoInterval;

    pCapwapGetFsCapwapDefaultWtpProfileEntry->MibObject.
        u4FsCapwapDefaultWtpProfileWtpIdleTimeout =
        pCapwapFsCapwapDefaultWtpProfileEntry->MibObject.
        u4FsCapwapDefaultWtpProfileWtpIdleTimeout;

    pCapwapGetFsCapwapDefaultWtpProfileEntry->MibObject.
        u4FsCapwapDefaultWtpProfileWtpMaxDiscoveryInterval =
        pCapwapFsCapwapDefaultWtpProfileEntry->MibObject.
        u4FsCapwapDefaultWtpProfileWtpMaxDiscoveryInterval;

    pCapwapGetFsCapwapDefaultWtpProfileEntry->MibObject.
        u4FsCapwapDefaultWtpProfileWtpReportInterval =
        pCapwapFsCapwapDefaultWtpProfileEntry->MibObject.
        u4FsCapwapDefaultWtpProfileWtpReportInterval;

    pCapwapGetFsCapwapDefaultWtpProfileEntry->MibObject.
        u4FsCapwapDefaultWtpProfileWtpStatisticsTimer =
        pCapwapFsCapwapDefaultWtpProfileEntry->MibObject.
        u4FsCapwapDefaultWtpProfileWtpStatisticsTimer;

    pCapwapGetFsCapwapDefaultWtpProfileEntry->MibObject.
        i4FsCapwapDefaultWtpProfileWtpEcnSupport =
        pCapwapFsCapwapDefaultWtpProfileEntry->MibObject.
        i4FsCapwapDefaultWtpProfileWtpEcnSupport;

    pCapwapGetFsCapwapDefaultWtpProfileEntry->MibObject.
        i4FsCapwapDefaultWtpProfileRowStatus =
        pCapwapFsCapwapDefaultWtpProfileEntry->MibObject.
        i4FsCapwapDefaultWtpProfileRowStatus;

    if (CapwapGetAllUtlFsCawapDefaultWtpProfileTable
        (pCapwapGetFsCapwapDefaultWtpProfileEntry,
         pCapwapFsCapwapDefaultWtpProfileEntry) == OSIX_FAILURE)

    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapGetAllFsCawapDefaultWtpProfileTable:"
                    "CapwapGetAllUtlFsCawapDefaultWtpProfileTable Returns Failure\r\n");

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetAllFsCapwapDnsProfileTable
 Input       :  pCapwapGetFsCapwapDnsProfileEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetAllFsCapwapDnsProfileTable (tCapwapFsCapwapDnsProfileEntry *
                                     pCapwapGetFsCapwapDnsProfileEntry)
{
    tCapwapFsCapwapDnsProfileEntry *pCapwapFsCapwapDnsProfileEntry = NULL;

    /* Check whether the node is already present */
    pCapwapFsCapwapDnsProfileEntry =
        RBTreeGet (gCapwapGlobals.CapwapGlbMib.FsCapwapDnsProfileTable,
                   (tRBElem *) pCapwapGetFsCapwapDnsProfileEntry);

    if (pCapwapFsCapwapDnsProfileEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapGetAllFsCapwapDnsProfileTable: Entry doesn't exist\r\n");
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    MEMCPY (pCapwapGetFsCapwapDnsProfileEntry->MibObject.au1FsCapwapDnsServerIp,
            pCapwapFsCapwapDnsProfileEntry->MibObject.au1FsCapwapDnsServerIp,
            pCapwapFsCapwapDnsProfileEntry->MibObject.i4FsCapwapDnsServerIpLen);

    pCapwapGetFsCapwapDnsProfileEntry->MibObject.i4FsCapwapDnsServerIpLen =
        pCapwapFsCapwapDnsProfileEntry->MibObject.i4FsCapwapDnsServerIpLen;

    MEMCPY (pCapwapGetFsCapwapDnsProfileEntry->MibObject.
            au1FsCapwapDnsDomainName,
            pCapwapFsCapwapDnsProfileEntry->MibObject.au1FsCapwapDnsDomainName,
            pCapwapFsCapwapDnsProfileEntry->MibObject.
            i4FsCapwapDnsDomainNameLen);

    pCapwapGetFsCapwapDnsProfileEntry->MibObject.i4FsCapwapDnsDomainNameLen =
        pCapwapFsCapwapDnsProfileEntry->MibObject.i4FsCapwapDnsDomainNameLen;

    pCapwapGetFsCapwapDnsProfileEntry->MibObject.i4FsCapwapDnsProfileRowStatus =
        pCapwapFsCapwapDnsProfileEntry->MibObject.i4FsCapwapDnsProfileRowStatus;

    if (CapwapGetAllUtlFsCapwapDnsProfileTable
        (pCapwapGetFsCapwapDnsProfileEntry,
         pCapwapFsCapwapDnsProfileEntry) == OSIX_FAILURE)

    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC, "CapwapGetAllFsCapwapDnsProfileTable:"
                    "CapwapGetAllUtlFsCapwapDnsProfileTable Returns Failure\r\n");

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetAllFsWtpNativeVlanIdTable
 Input       :  pCapwapGetFsWtpNativeVlanIdTable
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetAllFsWtpNativeVlanIdTable (tCapwapFsWtpNativeVlanIdTable *
                                    pCapwapGetFsWtpNativeVlanIdTable)
{
    tCapwapFsWtpNativeVlanIdTable *pCapwapFsWtpNativeVlanIdTable = NULL;

    /* Check whether the node is already present */
    pCapwapFsWtpNativeVlanIdTable =
        RBTreeGet (gCapwapGlobals.CapwapGlbMib.FsWtpNativeVlanIdTable,
                   (tRBElem *) pCapwapGetFsWtpNativeVlanIdTable);

    if (pCapwapFsWtpNativeVlanIdTable == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapGetAllFsWtpNativeVlanIdTable: Entry doesn't exist\r\n");
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pCapwapGetFsWtpNativeVlanIdTable->MibObject.i4FsWtpNativeVlanId =
        pCapwapFsWtpNativeVlanIdTable->MibObject.i4FsWtpNativeVlanId;

    pCapwapGetFsWtpNativeVlanIdTable->MibObject.i4FsWtpNativeVlanIdRowStatus =
        pCapwapFsWtpNativeVlanIdTable->MibObject.i4FsWtpNativeVlanIdRowStatus;

    if (CapwapGetAllUtlFsWtpNativeVlanIdTable
        (pCapwapGetFsWtpNativeVlanIdTable,
         pCapwapFsWtpNativeVlanIdTable) == OSIX_FAILURE)

    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC, "CapwapGetAllFsWtpNativeVlanIdTable:"
                    "CapwapGetAllUtlFsWtpNativeVlanIdTable Returns Failure\r\n");

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetAllFsWtpLocalRoutingTable
 Input       :  pCapwapGetFsWtpLocalRoutingTable
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetAllFsWtpLocalRoutingTable (tCapwapFsWtpLocalRoutingTable *
                                    pCapwapGetFsWtpLocalRoutingTable)
{
    tCapwapFsWtpLocalRoutingTable *pCapwapFsWtpLocalRoutingTable = NULL;

    /* Check whether the node is already present */
    pCapwapFsWtpLocalRoutingTable =
        RBTreeGet (gCapwapGlobals.CapwapGlbMib.FsWtpLocalRoutingTable,
                   (tRBElem *) pCapwapGetFsWtpLocalRoutingTable);

    if (pCapwapFsWtpLocalRoutingTable == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapGetAllFsWtpLocalRoutingTable: Entry doesn't exist\r\n");
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pCapwapGetFsWtpLocalRoutingTable->MibObject.i4FsWtpLocalRouting =
        pCapwapFsWtpLocalRoutingTable->MibObject.i4FsWtpLocalRouting;

    pCapwapGetFsWtpLocalRoutingTable->MibObject.i4FsWtpLocalRoutingRowStatus =
        pCapwapFsWtpLocalRoutingTable->MibObject.i4FsWtpLocalRoutingRowStatus;

    if (CapwapGetAllUtlFsWtpLocalRoutingTable
        (pCapwapGetFsWtpLocalRoutingTable,
         pCapwapFsWtpLocalRoutingTable) == OSIX_FAILURE)

    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC, "CapwapGetAllFsWtpLocalRoutingTable:"
                    "CapwapGetAllUtlFsWtpLocalRoutingTable Returns Failure\r\n");

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetAllFsCawapDiscStatsTable
 Input       :  pCapwapGetFsCawapDiscStatsEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetAllFsCawapDiscStatsTable (tCapwapFsCawapDiscStatsEntry *
                                   pCapwapGetFsCawapDiscStatsEntry)
{
    tCapwapFsCawapDiscStatsEntry *pCapwapFsCawapDiscStatsEntry = NULL;

    /* Check whether the node is already present */
    pCapwapFsCawapDiscStatsEntry =
        RBTreeGet (gCapwapGlobals.CapwapGlbMib.FsCawapDiscStatsTable,
                   (tRBElem *) pCapwapGetFsCawapDiscStatsEntry);

    if (pCapwapFsCawapDiscStatsEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapGetAllFsCawapDiscStatsTable: Entry doesn't exist\r\n");
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pCapwapGetFsCawapDiscStatsEntry->MibObject.u4FsCapwapDiscReqReceived =
        pCapwapFsCawapDiscStatsEntry->MibObject.u4FsCapwapDiscReqReceived;

    pCapwapGetFsCawapDiscStatsEntry->MibObject.u4FsCapwapDiscRspReceived =
        pCapwapFsCawapDiscStatsEntry->MibObject.u4FsCapwapDiscRspReceived;

    pCapwapGetFsCawapDiscStatsEntry->MibObject.u4FsCapwapDiscReqTransmitted =
        pCapwapFsCawapDiscStatsEntry->MibObject.u4FsCapwapDiscReqTransmitted;

    pCapwapGetFsCawapDiscStatsEntry->MibObject.u4FsCapwapDiscRspTransmitted =
        pCapwapFsCawapDiscStatsEntry->MibObject.u4FsCapwapDiscRspTransmitted;

    pCapwapGetFsCawapDiscStatsEntry->MibObject.
        u4FsCapwapDiscunsuccessfulProcessed =
        pCapwapFsCawapDiscStatsEntry->MibObject.
        u4FsCapwapDiscunsuccessfulProcessed;

    MEMCPY (pCapwapGetFsCawapDiscStatsEntry->MibObject.
            au1FsCapwapDiscLastUnsuccAttemptReason,
            pCapwapFsCawapDiscStatsEntry->MibObject.
            au1FsCapwapDiscLastUnsuccAttemptReason,
            pCapwapFsCawapDiscStatsEntry->MibObject.
            i4FsCapwapDiscLastUnsuccAttemptReasonLen);

    pCapwapGetFsCawapDiscStatsEntry->MibObject.
        i4FsCapwapDiscLastUnsuccAttemptReasonLen =
        pCapwapFsCawapDiscStatsEntry->MibObject.
        i4FsCapwapDiscLastUnsuccAttemptReasonLen;

    MEMCPY (pCapwapGetFsCawapDiscStatsEntry->MibObject.
            au1FsCapwapDiscLastSuccAttemptTime,
            pCapwapFsCawapDiscStatsEntry->MibObject.
            au1FsCapwapDiscLastSuccAttemptTime,
            pCapwapFsCawapDiscStatsEntry->MibObject.
            i4FsCapwapDiscLastSuccAttemptTimeLen);

    pCapwapGetFsCawapDiscStatsEntry->MibObject.
        i4FsCapwapDiscLastSuccAttemptTimeLen =
        pCapwapFsCawapDiscStatsEntry->MibObject.
        i4FsCapwapDiscLastSuccAttemptTimeLen;

    MEMCPY (pCapwapGetFsCawapDiscStatsEntry->MibObject.
            au1FsCapwapDiscLastUnsuccessfulAttemptTime,
            pCapwapFsCawapDiscStatsEntry->MibObject.
            au1FsCapwapDiscLastUnsuccessfulAttemptTime,
            pCapwapFsCawapDiscStatsEntry->MibObject.
            i4FsCapwapDiscLastUnsuccessfulAttemptTimeLen);

    pCapwapGetFsCawapDiscStatsEntry->MibObject.
        i4FsCapwapDiscLastUnsuccessfulAttemptTimeLen =
        pCapwapFsCawapDiscStatsEntry->MibObject.
        i4FsCapwapDiscLastUnsuccessfulAttemptTimeLen;

    pCapwapGetFsCawapDiscStatsEntry->MibObject.i4FsCapwapDiscStatsRowStatus =
        pCapwapFsCawapDiscStatsEntry->MibObject.i4FsCapwapDiscStatsRowStatus;

    if (CapwapGetAllUtlFsCawapDiscStatsTable
        (pCapwapGetFsCawapDiscStatsEntry,
         pCapwapFsCawapDiscStatsEntry) == OSIX_FAILURE)

    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC, "CapwapGetAllFsCawapDiscStatsTable:"
                    "CapwapGetAllUtlFsCawapDiscStatsTable Returns Failure\r\n");

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetAllFsCawapJoinStatsTable
 Input       :  pCapwapGetFsCawapJoinStatsEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetAllFsCawapJoinStatsTable (tCapwapFsCawapJoinStatsEntry *
                                   pCapwapGetFsCawapJoinStatsEntry)
{
    tCapwapFsCawapJoinStatsEntry *pCapwapFsCawapJoinStatsEntry = NULL;

    /* Check whether the node is already present */
    pCapwapFsCawapJoinStatsEntry =
        RBTreeGet (gCapwapGlobals.CapwapGlbMib.FsCawapJoinStatsTable,
                   (tRBElem *) pCapwapGetFsCawapJoinStatsEntry);

    if (pCapwapFsCawapJoinStatsEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapGetAllFsCawapJoinStatsTable: Entry doesn't exist\r\n");
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pCapwapGetFsCawapJoinStatsEntry->MibObject.u4FsCapwapJoinReqReceived =
        pCapwapFsCawapJoinStatsEntry->MibObject.u4FsCapwapJoinReqReceived;

    pCapwapGetFsCawapJoinStatsEntry->MibObject.u4FsCapwapJoinRspReceived =
        pCapwapFsCawapJoinStatsEntry->MibObject.u4FsCapwapJoinRspReceived;

    pCapwapGetFsCawapJoinStatsEntry->MibObject.u4FsCapwapJoinReqTransmitted =
        pCapwapFsCawapJoinStatsEntry->MibObject.u4FsCapwapJoinReqTransmitted;

    pCapwapGetFsCawapJoinStatsEntry->MibObject.u4FsCapwapJoinRspTransmitted =
        pCapwapFsCawapJoinStatsEntry->MibObject.u4FsCapwapJoinRspTransmitted;

    pCapwapGetFsCawapJoinStatsEntry->MibObject.
        u4FsCapwapJoinunsuccessfulProcessed =
        pCapwapFsCawapJoinStatsEntry->MibObject.
        u4FsCapwapJoinunsuccessfulProcessed;

    MEMCPY (pCapwapGetFsCawapJoinStatsEntry->MibObject.
            au1FsCapwapJoinReasonLastUnsuccAttempt,
            pCapwapFsCawapJoinStatsEntry->MibObject.
            au1FsCapwapJoinReasonLastUnsuccAttempt,
            pCapwapFsCawapJoinStatsEntry->MibObject.
            i4FsCapwapJoinReasonLastUnsuccAttemptLen);

    pCapwapGetFsCawapJoinStatsEntry->MibObject.
        i4FsCapwapJoinReasonLastUnsuccAttemptLen =
        pCapwapFsCawapJoinStatsEntry->MibObject.
        i4FsCapwapJoinReasonLastUnsuccAttemptLen;

    MEMCPY (pCapwapGetFsCawapJoinStatsEntry->MibObject.
            au1FsCapwapJoinLastSuccAttemptTime,
            pCapwapFsCawapJoinStatsEntry->MibObject.
            au1FsCapwapJoinLastSuccAttemptTime,
            pCapwapFsCawapJoinStatsEntry->MibObject.
            i4FsCapwapJoinLastSuccAttemptTimeLen);

    pCapwapGetFsCawapJoinStatsEntry->MibObject.
        i4FsCapwapJoinLastSuccAttemptTimeLen =
        pCapwapFsCawapJoinStatsEntry->MibObject.
        i4FsCapwapJoinLastSuccAttemptTimeLen;

    MEMCPY (pCapwapGetFsCawapJoinStatsEntry->MibObject.
            au1FsCapwapJoinLastUnsuccAttemptTime,
            pCapwapFsCawapJoinStatsEntry->MibObject.
            au1FsCapwapJoinLastUnsuccAttemptTime,
            pCapwapFsCawapJoinStatsEntry->MibObject.
            i4FsCapwapJoinLastUnsuccAttemptTimeLen);

    pCapwapGetFsCawapJoinStatsEntry->MibObject.
        i4FsCapwapJoinLastUnsuccAttemptTimeLen =
        pCapwapFsCawapJoinStatsEntry->MibObject.
        i4FsCapwapJoinLastUnsuccAttemptTimeLen;

    pCapwapGetFsCawapJoinStatsEntry->MibObject.i4FsCapwapJoinStatsRowStatus =
        pCapwapFsCawapJoinStatsEntry->MibObject.i4FsCapwapJoinStatsRowStatus;

    if (CapwapGetAllUtlFsCawapJoinStatsTable
        (pCapwapGetFsCawapJoinStatsEntry,
         pCapwapFsCawapJoinStatsEntry) == OSIX_FAILURE)

    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC, "CapwapGetAllFsCawapJoinStatsTable:"
                    "CapwapGetAllUtlFsCawapJoinStatsTable Returns Failure\r\n");

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetAllFsCawapConfigStatsTable
 Input       :  pCapwapGetFsCawapConfigStatsEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetAllFsCawapConfigStatsTable (tCapwapFsCawapConfigStatsEntry *
                                     pCapwapGetFsCawapConfigStatsEntry)
{
    tCapwapFsCawapConfigStatsEntry *pCapwapFsCawapConfigStatsEntry = NULL;

    /* Check whether the node is already present */
    pCapwapFsCawapConfigStatsEntry =
        RBTreeGet (gCapwapGlobals.CapwapGlbMib.FsCawapConfigStatsTable,
                   (tRBElem *) pCapwapGetFsCawapConfigStatsEntry);

    if (pCapwapFsCawapConfigStatsEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapGetAllFsCawapConfigStatsTable: Entry doesn't exist\r\n");
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pCapwapGetFsCawapConfigStatsEntry->MibObject.u4FsCapwapConfigReqReceived =
        pCapwapFsCawapConfigStatsEntry->MibObject.u4FsCapwapConfigReqReceived;

    pCapwapGetFsCawapConfigStatsEntry->MibObject.u4FsCapwapConfigRspReceived =
        pCapwapFsCawapConfigStatsEntry->MibObject.u4FsCapwapConfigRspReceived;

    pCapwapGetFsCawapConfigStatsEntry->MibObject.
        u4FsCapwapConfigReqTransmitted =
        pCapwapFsCawapConfigStatsEntry->MibObject.
        u4FsCapwapConfigReqTransmitted;

    pCapwapGetFsCawapConfigStatsEntry->MibObject.
        u4FsCapwapConfigRspTransmitted =
        pCapwapFsCawapConfigStatsEntry->MibObject.
        u4FsCapwapConfigRspTransmitted;

    pCapwapGetFsCawapConfigStatsEntry->MibObject.
        u4FsCapwapConfigunsuccessfulProcessed =
        pCapwapFsCawapConfigStatsEntry->MibObject.
        u4FsCapwapConfigunsuccessfulProcessed;

    MEMCPY (pCapwapGetFsCawapConfigStatsEntry->MibObject.
            au1FsCapwapConfigReasonLastUnsuccAttempt,
            pCapwapFsCawapConfigStatsEntry->MibObject.
            au1FsCapwapConfigReasonLastUnsuccAttempt,
            pCapwapFsCawapConfigStatsEntry->MibObject.
            i4FsCapwapConfigReasonLastUnsuccAttemptLen);

    pCapwapGetFsCawapConfigStatsEntry->MibObject.
        i4FsCapwapConfigReasonLastUnsuccAttemptLen =
        pCapwapFsCawapConfigStatsEntry->MibObject.
        i4FsCapwapConfigReasonLastUnsuccAttemptLen;

    MEMCPY (pCapwapGetFsCawapConfigStatsEntry->MibObject.
            au1FsCapwapConfigLastSuccAttemptTime,
            pCapwapFsCawapConfigStatsEntry->MibObject.
            au1FsCapwapConfigLastSuccAttemptTime,
            pCapwapFsCawapConfigStatsEntry->MibObject.
            i4FsCapwapConfigLastSuccAttemptTimeLen);

    pCapwapGetFsCawapConfigStatsEntry->MibObject.
        i4FsCapwapConfigLastSuccAttemptTimeLen =
        pCapwapFsCawapConfigStatsEntry->MibObject.
        i4FsCapwapConfigLastSuccAttemptTimeLen;

    MEMCPY (pCapwapGetFsCawapConfigStatsEntry->MibObject.
            au1FsCapwapConfigLastUnsuccessfulAttemptTime,
            pCapwapFsCawapConfigStatsEntry->MibObject.
            au1FsCapwapConfigLastUnsuccessfulAttemptTime,
            pCapwapFsCawapConfigStatsEntry->MibObject.
            i4FsCapwapConfigLastUnsuccessfulAttemptTimeLen);

    pCapwapGetFsCawapConfigStatsEntry->MibObject.
        i4FsCapwapConfigLastUnsuccessfulAttemptTimeLen =
        pCapwapFsCawapConfigStatsEntry->MibObject.
        i4FsCapwapConfigLastUnsuccessfulAttemptTimeLen;

    pCapwapGetFsCawapConfigStatsEntry->MibObject.
        i4FsCapwapConfigStatsRowStatus =
        pCapwapFsCawapConfigStatsEntry->MibObject.
        i4FsCapwapConfigStatsRowStatus;

    if (CapwapGetAllUtlFsCawapConfigStatsTable
        (pCapwapGetFsCawapConfigStatsEntry,
         pCapwapFsCawapConfigStatsEntry) == OSIX_FAILURE)

    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC, "CapwapGetAllFsCawapConfigStatsTable:"
                    "CapwapGetAllUtlFsCawapConfigStatsTable Returns Failure\r\n");

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetAllFsCawapRunStatsTable
 Input       :  pCapwapGetFsCawapRunStatsEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetAllFsCawapRunStatsTable (tCapwapFsCawapRunStatsEntry *
                                  pCapwapGetFsCawapRunStatsEntry)
{
    if (CapwapGetAllUtlFsCawapRunStatsTable
        (pCapwapGetFsCawapRunStatsEntry, NULL) == OSIX_FAILURE)

    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC, "CapwapGetAllFsCawapRunStatsTable:"
                    "CapwapGetAllUtlFsCawapRunStatsTable Returns Failure\r\n");

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetAllFsCapwapWirelessBindingTable
 Input       :  pCapwapGetFsCapwapWirelessBindingEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetAllFsCapwapWirelessBindingTable (tCapwapFsCapwapWirelessBindingEntry *
                                          pCapwapGetFsCapwapWirelessBindingEntry)
{
    tCapwapFsCapwapWirelessBindingEntry *pCapwapFsCapwapWirelessBindingEntry =
        NULL;

    /* Check whether the node is already present */
    pCapwapFsCapwapWirelessBindingEntry =
        RBTreeGet (gCapwapGlobals.CapwapGlbMib.FsCapwapWirelessBindingTable,
                   (tRBElem *) pCapwapGetFsCapwapWirelessBindingEntry);

    if (pCapwapFsCapwapWirelessBindingEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapGetAllFsCapwapWirelessBindingTable: Entry doesn't exist\r\n");
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pCapwapGetFsCapwapWirelessBindingEntry->MibObject.
        i4FsCapwapWirelessBindingVirtualRadioIfIndex =
        pCapwapFsCapwapWirelessBindingEntry->MibObject.
        i4FsCapwapWirelessBindingVirtualRadioIfIndex;

    pCapwapGetFsCapwapWirelessBindingEntry->MibObject.
        i4FsCapwapWirelessBindingType =
        pCapwapFsCapwapWirelessBindingEntry->MibObject.
        i4FsCapwapWirelessBindingType;

    pCapwapGetFsCapwapWirelessBindingEntry->MibObject.
        i4FsCapwapWirelessBindingRowStatus =
        pCapwapFsCapwapWirelessBindingEntry->MibObject.
        i4FsCapwapWirelessBindingRowStatus;

    if (CapwapGetAllUtlFsCapwapWirelessBindingTable
        (pCapwapGetFsCapwapWirelessBindingEntry,
         pCapwapFsCapwapWirelessBindingEntry) == OSIX_FAILURE)

    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC, "CapwapGetAllFsCapwapWirelessBindingTable:"
                    "CapwapGetAllUtlFsCapwapWirelessBindingTable Returns Failure\r\n");

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetAllFsCapwapStationWhiteList
 Input       :  pCapwapGetFsCapwapStationWhiteListEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetAllFsCapwapStationWhiteList (tCapwapFsCapwapStationWhiteListEntry *
                                      pCapwapGetFsCapwapStationWhiteListEntry)
{
    tCapwapFsCapwapStationWhiteListEntry *pCapwapFsCapwapStationWhiteListEntry =
        NULL;

    /* Check whether the node is already present */
    pCapwapFsCapwapStationWhiteListEntry =
        RBTreeGet (gCapwapGlobals.CapwapGlbMib.FsCapwapStationWhiteList,
                   (tRBElem *) pCapwapGetFsCapwapStationWhiteListEntry);

    if (pCapwapFsCapwapStationWhiteListEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapGetAllFsCapwapStationWhiteList: Entry doesn't exist\r\n");
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    MEMCPY (pCapwapGetFsCapwapStationWhiteListEntry->MibObject.
            au1FsCapwapStationWhiteListStationId,
            pCapwapFsCapwapStationWhiteListEntry->MibObject.
            au1FsCapwapStationWhiteListStationId,
            pCapwapFsCapwapStationWhiteListEntry->MibObject.
            i4FsCapwapStationWhiteListStationIdLen);

    pCapwapGetFsCapwapStationWhiteListEntry->MibObject.
        i4FsCapwapStationWhiteListStationIdLen =
        pCapwapFsCapwapStationWhiteListEntry->MibObject.
        i4FsCapwapStationWhiteListStationIdLen;

    pCapwapGetFsCapwapStationWhiteListEntry->MibObject.
        i4FsCapwapStationWhiteListRowStatus =
        pCapwapFsCapwapStationWhiteListEntry->MibObject.
        i4FsCapwapStationWhiteListRowStatus;

    if (CapwapGetAllUtlFsCapwapStationWhiteList
        (pCapwapGetFsCapwapStationWhiteListEntry,
         pCapwapFsCapwapStationWhiteListEntry) == OSIX_FAILURE)

    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC, "CapwapGetAllFsCapwapStationWhiteList:"
                    "CapwapGetAllUtlFsCapwapStationWhiteList Returns Failure\r\n");

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetAllFsCapwapWtpRebootStatisticsTable
 Input       :  pCapwapGetFsCapwapWtpRebootStatisticsEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    CapwapGetAllFsCapwapWtpRebootStatisticsTable
    (tCapwapFsCapwapWtpRebootStatisticsEntry *
     pCapwapGetFsCapwapWtpRebootStatisticsEntry)
{
    tCapwapFsCapwapWtpRebootStatisticsEntry
        * pCapwapFsCapwapWtpRebootStatisticsEntry = NULL;

#if 0
    /* Check whether the node is already present */
    pCapwapFsCapwapWtpRebootStatisticsEntry =
        RBTreeGet (gCapwapGlobals.CapwapGlbMib.FsCapwapWtpRebootStatisticsTable,
                   (tRBElem *) pCapwapGetFsCapwapWtpRebootStatisticsEntry);

    if (pCapwapFsCapwapWtpRebootStatisticsEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapGetAllFsCapwapWtpRebootStatisticsTable: Entry doesn't exist\r\n");
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pCapwapGetFsCapwapWtpRebootStatisticsEntry->MibObject.
        u4FsCapwapWtpRebootStatisticsRebootCount =
        pCapwapFsCapwapWtpRebootStatisticsEntry->MibObject.
        u4FsCapwapWtpRebootStatisticsRebootCount;

    pCapwapGetFsCapwapWtpRebootStatisticsEntry->MibObject.
        u4FsCapwapWtpRebootStatisticsAcInitiatedCount =
        pCapwapFsCapwapWtpRebootStatisticsEntry->MibObject.
        u4FsCapwapWtpRebootStatisticsAcInitiatedCount;

    pCapwapGetFsCapwapWtpRebootStatisticsEntry->MibObject.
        u4FsCapwapWtpRebootStatisticsLinkFailureCount =
        pCapwapFsCapwapWtpRebootStatisticsEntry->MibObject.
        u4FsCapwapWtpRebootStatisticsLinkFailureCount;

    pCapwapGetFsCapwapWtpRebootStatisticsEntry->MibObject.
        u4FsCapwapWtpRebootStatisticsSwFailureCount =
        pCapwapFsCapwapWtpRebootStatisticsEntry->MibObject.
        u4FsCapwapWtpRebootStatisticsSwFailureCount;

    pCapwapGetFsCapwapWtpRebootStatisticsEntry->MibObject.
        u4FsCapwapWtpRebootStatisticsHwFailureCount =
        pCapwapFsCapwapWtpRebootStatisticsEntry->MibObject.
        u4FsCapwapWtpRebootStatisticsHwFailureCount;

    pCapwapGetFsCapwapWtpRebootStatisticsEntry->MibObject.
        u4FsCapwapWtpRebootStatisticsOtherFailureCount =
        pCapwapFsCapwapWtpRebootStatisticsEntry->MibObject.
        u4FsCapwapWtpRebootStatisticsOtherFailureCount;

    pCapwapGetFsCapwapWtpRebootStatisticsEntry->MibObject.
        u4FsCapwapWtpRebootStatisticsUnknownFailureCount =
        pCapwapFsCapwapWtpRebootStatisticsEntry->MibObject.
        u4FsCapwapWtpRebootStatisticsUnknownFailureCount;

    pCapwapGetFsCapwapWtpRebootStatisticsEntry->MibObject.
        i4FsCapwapWtpRebootStatisticsLastFailureType =
        pCapwapFsCapwapWtpRebootStatisticsEntry->MibObject.
        i4FsCapwapWtpRebootStatisticsLastFailureType;

    pCapwapGetFsCapwapWtpRebootStatisticsEntry->MibObject.
        i4FsCapwapWtpRebootStatisticsRowStatus =
        pCapwapFsCapwapWtpRebootStatisticsEntry->MibObject.
        i4FsCapwapWtpRebootStatisticsRowStatus;
#endif

    if (CapwapGetAllUtlFsCapwapWtpRebootStatisticsTable
        (pCapwapGetFsCapwapWtpRebootStatisticsEntry,
         pCapwapFsCapwapWtpRebootStatisticsEntry) == OSIX_FAILURE)

    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapGetAllFsCapwapWtpRebootStatisticsTable:"
                    "CapwapGetAllUtlFsCapwapWtpRebootStatisticsTable Returns Failure\r\n");

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetAllFsCapwapWtpRadioStatisticsTable
 Input       :  pCapwapGetFsCapwapWtpRadioStatisticsEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    CapwapGetAllFsCapwapWtpRadioStatisticsTable
    (tCapwapFsCapwapWtpRadioStatisticsEntry *
     pCapwapGetFsCapwapWtpRadioStatisticsEntry)
{
    tCapwapFsCapwapWtpRadioStatisticsEntry
        * pCapwapFsCapwapWtpRadioStatisticsEntry = NULL;

#if 0
    /* Check whether the node is already present */
    pCapwapFsCapwapWtpRadioStatisticsEntry =
        RBTreeGet (gCapwapGlobals.CapwapGlbMib.FsCapwapWtpRadioStatisticsTable,
                   (tRBElem *) pCapwapGetFsCapwapWtpRadioStatisticsEntry);

    if (pCapwapFsCapwapWtpRadioStatisticsEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapGetAllFsCapwapWtpRadioStatisticsTable: Entry doesn't exist\r\n");
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pCapwapGetFsCapwapWtpRadioStatisticsEntry->MibObject.
        i4FsCapwapWtpRadioLastFailType =
        pCapwapFsCapwapWtpRadioStatisticsEntry->MibObject.
        i4FsCapwapWtpRadioLastFailType;

    pCapwapGetFsCapwapWtpRadioStatisticsEntry->MibObject.
        u4FsCapwapWtpRadioResetCount =
        pCapwapFsCapwapWtpRadioStatisticsEntry->MibObject.
        u4FsCapwapWtpRadioResetCount;

    pCapwapGetFsCapwapWtpRadioStatisticsEntry->MibObject.
        u4FsCapwapWtpRadioSwFailureCount =
        pCapwapFsCapwapWtpRadioStatisticsEntry->MibObject.
        u4FsCapwapWtpRadioSwFailureCount;

    pCapwapGetFsCapwapWtpRadioStatisticsEntry->MibObject.
        u4FsCapwapWtpRadioHwFailureCount =
        pCapwapFsCapwapWtpRadioStatisticsEntry->MibObject.
        u4FsCapwapWtpRadioHwFailureCount;

    pCapwapGetFsCapwapWtpRadioStatisticsEntry->MibObject.
        u4FsCapwapWtpRadioOtherFailureCount =
        pCapwapFsCapwapWtpRadioStatisticsEntry->MibObject.
        u4FsCapwapWtpRadioOtherFailureCount;

    pCapwapGetFsCapwapWtpRadioStatisticsEntry->MibObject.
        u4FsCapwapWtpRadioUnknownFailureCount =
        pCapwapFsCapwapWtpRadioStatisticsEntry->MibObject.
        u4FsCapwapWtpRadioUnknownFailureCount;

    pCapwapGetFsCapwapWtpRadioStatisticsEntry->MibObject.
        u4FsCapwapWtpRadioConfigUpdateCount =
        pCapwapFsCapwapWtpRadioStatisticsEntry->MibObject.
        u4FsCapwapWtpRadioConfigUpdateCount;

    pCapwapGetFsCapwapWtpRadioStatisticsEntry->MibObject.
        u4FsCapwapWtpRadioChannelChangeCount =
        pCapwapFsCapwapWtpRadioStatisticsEntry->MibObject.
        u4FsCapwapWtpRadioChannelChangeCount;

    pCapwapGetFsCapwapWtpRadioStatisticsEntry->MibObject.
        u4FsCapwapWtpRadioBandChangeCount =
        pCapwapFsCapwapWtpRadioStatisticsEntry->MibObject.
        u4FsCapwapWtpRadioBandChangeCount;

    pCapwapGetFsCapwapWtpRadioStatisticsEntry->MibObject.
        u4FsCapwapWtpRadioCurrentNoiseFloor =
        pCapwapFsCapwapWtpRadioStatisticsEntry->MibObject.
        u4FsCapwapWtpRadioCurrentNoiseFloor;

    pCapwapGetFsCapwapWtpRadioStatisticsEntry->MibObject.
        u4FsCapwapWtpRadioStatRowStatus =
        pCapwapFsCapwapWtpRadioStatisticsEntry->MibObject.
        u4FsCapwapWtpRadioStatRowStatus;
#endif

    if (CapwapGetAllUtlFsCapwapWtpRadioStatisticsTable
        (pCapwapGetFsCapwapWtpRadioStatisticsEntry,
         pCapwapFsCapwapWtpRadioStatisticsEntry) == OSIX_FAILURE)

    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapGetAllFsCapwapWtpRadioStatisticsTable:"
                    "CapwapGetAllUtlFsCapwapWtpRadioStatisticsTable Returns Failure\r\n");

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapSetAllCapwapBaseAcNameListTable
 Input       :  pCapwapSetCapwapBaseAcNameListEntry
                pCapwapIsSetCapwapBaseAcNameListEntry
                i4RowStatusLogic
                i4RowCreateOption
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapSetAllCapwapBaseAcNameListTable (tCapwapCapwapBaseAcNameListEntry *
                                       pCapwapSetCapwapBaseAcNameListEntry,
                                       tCapwapIsSetCapwapBaseAcNameListEntry *
                                       pCapwapIsSetCapwapBaseAcNameListEntry,
                                       INT4 i4RowStatusLogic,
                                       INT4 i4RowCreateOption)
{
    tCapwapCapwapBaseAcNameListEntry *pCapwapCapwapBaseAcNameListEntry = NULL;
    tCapwapCapwapBaseAcNameListEntry *pCapwapOldCapwapBaseAcNameListEntry =
        NULL;
    tCapwapCapwapBaseAcNameListEntry *pCapwapTrgCapwapBaseAcNameListEntry =
        NULL;
    tCapwapIsSetCapwapBaseAcNameListEntry
        * pCapwapTrgIsSetCapwapBaseAcNameListEntry = NULL;
    INT4                i4RowMakeActive = FALSE;

    pCapwapOldCapwapBaseAcNameListEntry =
        (tCapwapCapwapBaseAcNameListEntry *)
        MemAllocMemBlk (CAPWAP_CAPWAPBASEACNAMELISTTABLE_POOLID);
    if (pCapwapOldCapwapBaseAcNameListEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    pCapwapTrgCapwapBaseAcNameListEntry =
        (tCapwapCapwapBaseAcNameListEntry *)
        MemAllocMemBlk (CAPWAP_CAPWAPBASEACNAMELISTTABLE_POOLID);
    if (pCapwapTrgCapwapBaseAcNameListEntry == NULL)
    {
        MemReleaseMemBlock (CAPWAP_CAPWAPBASEACNAMELISTTABLE_POOLID,
                            (UINT1 *) pCapwapOldCapwapBaseAcNameListEntry);
        return OSIX_FAILURE;
    }
    pCapwapTrgIsSetCapwapBaseAcNameListEntry =
        (tCapwapIsSetCapwapBaseAcNameListEntry *)
        MemAllocMemBlk (CAPWAP_CAPWAPBASEACNAMELISTTABLE_ISSET_POOLID);
    if (pCapwapTrgIsSetCapwapBaseAcNameListEntry == NULL)
    {
        MemReleaseMemBlock (CAPWAP_CAPWAPBASEACNAMELISTTABLE_POOLID,
                            (UINT1 *) pCapwapOldCapwapBaseAcNameListEntry);
        MemReleaseMemBlock (CAPWAP_CAPWAPBASEACNAMELISTTABLE_POOLID,
                            (UINT1 *) pCapwapTrgCapwapBaseAcNameListEntry);
        return OSIX_FAILURE;
    }
    MEMSET (pCapwapOldCapwapBaseAcNameListEntry, 0,
            sizeof (tCapwapCapwapBaseAcNameListEntry));
    MEMSET (pCapwapTrgCapwapBaseAcNameListEntry, 0,
            sizeof (tCapwapCapwapBaseAcNameListEntry));
    MEMSET (pCapwapTrgIsSetCapwapBaseAcNameListEntry, 0,
            sizeof (tCapwapIsSetCapwapBaseAcNameListEntry));

    /* Check whether the node is already present */
    pCapwapCapwapBaseAcNameListEntry =
        RBTreeGet (gCapwapGlobals.CapwapGlbMib.CapwapBaseAcNameListTable,
                   (tRBElem *) pCapwapSetCapwapBaseAcNameListEntry);

    if (pCapwapCapwapBaseAcNameListEntry == NULL)
    {
        /* Create the node if the RowStatus given is CREATE_AND_WAIT or CREATE_AND_GO */
        if ((pCapwapSetCapwapBaseAcNameListEntry->MibObject.
             i4CapwapBaseAcNameListRowStatus == CREATE_AND_WAIT)
            || (pCapwapSetCapwapBaseAcNameListEntry->MibObject.
                i4CapwapBaseAcNameListRowStatus == CREATE_AND_GO)
            ||
            ((pCapwapSetCapwapBaseAcNameListEntry->MibObject.
              i4CapwapBaseAcNameListRowStatus == ACTIVE)
             && (i4RowCreateOption == 1)))
        {
            /* Allocate memory for the new node */
            pCapwapCapwapBaseAcNameListEntry =
                (tCapwapCapwapBaseAcNameListEntry *)
                MemAllocMemBlk (CAPWAP_CAPWAPBASEACNAMELISTTABLE_POOLID);
            if (pCapwapCapwapBaseAcNameListEntry == NULL)
            {
                if (CapwapSetAllCapwapBaseAcNameListTableTrigger
                    (pCapwapSetCapwapBaseAcNameListEntry,
                     pCapwapIsSetCapwapBaseAcNameListEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllCapwapBaseAcNameListTable:CapwapSetAllCapwapBaseAcNameListTableTrigger function fails\r\n");

                }
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllCapwapBaseAcNameListTable: Fail to Allocate Memory.\r\n");
                MemReleaseMemBlock (CAPWAP_CAPWAPBASEACNAMELISTTABLE_POOLID,
                                    (UINT1 *)
                                    pCapwapOldCapwapBaseAcNameListEntry);
                MemReleaseMemBlock (CAPWAP_CAPWAPBASEACNAMELISTTABLE_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgCapwapBaseAcNameListEntry);
                MemReleaseMemBlock
                    (CAPWAP_CAPWAPBASEACNAMELISTTABLE_ISSET_POOLID,
                     (UINT1 *) pCapwapTrgIsSetCapwapBaseAcNameListEntry);
                return OSIX_FAILURE;
            }

            MEMSET (pCapwapCapwapBaseAcNameListEntry, 0,
                    sizeof (tCapwapCapwapBaseAcNameListEntry));
            if ((CapwapInitializeCapwapBaseAcNameListTable
                 (pCapwapCapwapBaseAcNameListEntry)) == OSIX_FAILURE)
            {
                if (CapwapSetAllCapwapBaseAcNameListTableTrigger
                    (pCapwapSetCapwapBaseAcNameListEntry,
                     pCapwapIsSetCapwapBaseAcNameListEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllCapwapBaseAcNameListTable:CapwapSetAllCapwapBaseAcNameListTableTrigger function fails\r\n");

                }
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllCapwapBaseAcNameListTable: Fail to Initialize the Objects.\r\n");

                MemReleaseMemBlock (CAPWAP_CAPWAPBASEACNAMELISTTABLE_POOLID,
                                    (UINT1 *) pCapwapCapwapBaseAcNameListEntry);
                MemReleaseMemBlock (CAPWAP_CAPWAPBASEACNAMELISTTABLE_POOLID,
                                    (UINT1 *)
                                    pCapwapOldCapwapBaseAcNameListEntry);
                MemReleaseMemBlock (CAPWAP_CAPWAPBASEACNAMELISTTABLE_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgCapwapBaseAcNameListEntry);
                MemReleaseMemBlock
                    (CAPWAP_CAPWAPBASEACNAMELISTTABLE_ISSET_POOLID,
                     (UINT1 *) pCapwapTrgIsSetCapwapBaseAcNameListEntry);
                return OSIX_FAILURE;
            }
            /* Assign values for the new node */
            if (pCapwapIsSetCapwapBaseAcNameListEntry->
                bCapwapBaseAcNameListId != OSIX_FALSE)
            {
                pCapwapCapwapBaseAcNameListEntry->MibObject.
                    u4CapwapBaseAcNameListId =
                    pCapwapSetCapwapBaseAcNameListEntry->MibObject.
                    u4CapwapBaseAcNameListId;
            }

            if (pCapwapIsSetCapwapBaseAcNameListEntry->
                bCapwapBaseAcNameListName != OSIX_FALSE)
            {
                MEMCPY (pCapwapCapwapBaseAcNameListEntry->MibObject.
                        au1CapwapBaseAcNameListName,
                        pCapwapSetCapwapBaseAcNameListEntry->MibObject.
                        au1CapwapBaseAcNameListName,
                        pCapwapSetCapwapBaseAcNameListEntry->MibObject.
                        i4CapwapBaseAcNameListNameLen);

                pCapwapCapwapBaseAcNameListEntry->MibObject.
                    i4CapwapBaseAcNameListNameLen =
                    pCapwapSetCapwapBaseAcNameListEntry->MibObject.
                    i4CapwapBaseAcNameListNameLen;
            }

            if (pCapwapIsSetCapwapBaseAcNameListEntry->
                bCapwapBaseAcNameListPriority != OSIX_FALSE)
            {
                pCapwapCapwapBaseAcNameListEntry->MibObject.
                    u4CapwapBaseAcNameListPriority =
                    pCapwapSetCapwapBaseAcNameListEntry->MibObject.
                    u4CapwapBaseAcNameListPriority;
            }

            if (pCapwapIsSetCapwapBaseAcNameListEntry->
                bCapwapBaseAcNameListRowStatus != OSIX_FALSE)
            {
                pCapwapCapwapBaseAcNameListEntry->MibObject.
                    i4CapwapBaseAcNameListRowStatus =
                    pCapwapSetCapwapBaseAcNameListEntry->MibObject.
                    i4CapwapBaseAcNameListRowStatus;
            }

            if ((pCapwapSetCapwapBaseAcNameListEntry->MibObject.
                 i4CapwapBaseAcNameListRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pCapwapSetCapwapBaseAcNameListEntry->MibObject.
                        i4CapwapBaseAcNameListRowStatus == ACTIVE)))
            {
                pCapwapCapwapBaseAcNameListEntry->MibObject.
                    i4CapwapBaseAcNameListRowStatus = ACTIVE;
            }
            else if (pCapwapSetCapwapBaseAcNameListEntry->MibObject.
                     i4CapwapBaseAcNameListRowStatus == CREATE_AND_WAIT)
            {
                pCapwapCapwapBaseAcNameListEntry->MibObject.
                    i4CapwapBaseAcNameListRowStatus = NOT_READY;
            }

            /* Add the new node to the database */
            if (RBTreeAdd
                (gCapwapGlobals.CapwapGlbMib.CapwapBaseAcNameListTable,
                 (tRBElem *) pCapwapCapwapBaseAcNameListEntry) != RB_SUCCESS)
            {
                if (CapwapSetAllCapwapBaseAcNameListTableTrigger
                    (pCapwapSetCapwapBaseAcNameListEntry,
                     pCapwapIsSetCapwapBaseAcNameListEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllCapwapBaseAcNameListTable: CapwapSetAllCapwapBaseAcNameListTableTrigger function returns failure.\r\n");
                }
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllCapwapBaseAcNameListTable: RBTreeAdd is failed.\r\n");

                MemReleaseMemBlock (CAPWAP_CAPWAPBASEACNAMELISTTABLE_POOLID,
                                    (UINT1 *) pCapwapCapwapBaseAcNameListEntry);
                MemReleaseMemBlock (CAPWAP_CAPWAPBASEACNAMELISTTABLE_POOLID,
                                    (UINT1 *)
                                    pCapwapOldCapwapBaseAcNameListEntry);
                MemReleaseMemBlock (CAPWAP_CAPWAPBASEACNAMELISTTABLE_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgCapwapBaseAcNameListEntry);
                MemReleaseMemBlock
                    (CAPWAP_CAPWAPBASEACNAMELISTTABLE_ISSET_POOLID,
                     (UINT1 *) pCapwapTrgIsSetCapwapBaseAcNameListEntry);
                return OSIX_FAILURE;
            }
            if (CapwapUtilUpdateCapwapBaseAcNameListTable
                (NULL, pCapwapCapwapBaseAcNameListEntry,
                 pCapwapIsSetCapwapBaseAcNameListEntry) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllCapwapBaseAcNameListTable: CapwapUtilUpdateCapwapBaseAcNameListTable function returns failure.\r\n");

                if (CapwapSetAllCapwapBaseAcNameListTableTrigger
                    (pCapwapSetCapwapBaseAcNameListEntry,
                     pCapwapIsSetCapwapBaseAcNameListEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllCapwapBaseAcNameListTable: CapwapSetAllCapwapBaseAcNameListTableTrigger function returns failure.\r\n");

                }
                RBTreeRem (gCapwapGlobals.CapwapGlbMib.
                           CapwapBaseAcNameListTable,
                           pCapwapCapwapBaseAcNameListEntry);
                MemReleaseMemBlock (CAPWAP_CAPWAPBASEACNAMELISTTABLE_POOLID,
                                    (UINT1 *) pCapwapCapwapBaseAcNameListEntry);
                MemReleaseMemBlock (CAPWAP_CAPWAPBASEACNAMELISTTABLE_POOLID,
                                    (UINT1 *)
                                    pCapwapOldCapwapBaseAcNameListEntry);
                MemReleaseMemBlock (CAPWAP_CAPWAPBASEACNAMELISTTABLE_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgCapwapBaseAcNameListEntry);
                MemReleaseMemBlock
                    (CAPWAP_CAPWAPBASEACNAMELISTTABLE_ISSET_POOLID,
                     (UINT1 *) pCapwapTrgIsSetCapwapBaseAcNameListEntry);
                return OSIX_FAILURE;
            }

            if ((pCapwapSetCapwapBaseAcNameListEntry->MibObject.
                 i4CapwapBaseAcNameListRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pCapwapSetCapwapBaseAcNameListEntry->MibObject.
                        i4CapwapBaseAcNameListRowStatus == ACTIVE)))
            {

                /* For MSR and RM Trigger */
                pCapwapTrgCapwapBaseAcNameListEntry->MibObject.
                    u4CapwapBaseAcNameListId =
                    pCapwapSetCapwapBaseAcNameListEntry->MibObject.
                    u4CapwapBaseAcNameListId;
                pCapwapTrgCapwapBaseAcNameListEntry->MibObject.
                    i4CapwapBaseAcNameListRowStatus = CREATE_AND_WAIT;
                pCapwapTrgIsSetCapwapBaseAcNameListEntry->
                    bCapwapBaseAcNameListRowStatus = OSIX_TRUE;

                if (CapwapSetAllCapwapBaseAcNameListTableTrigger
                    (pCapwapTrgCapwapBaseAcNameListEntry,
                     pCapwapTrgIsSetCapwapBaseAcNameListEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllCapwapBaseAcNameListTable: CapwapSetAllCapwapBaseAcNameListTableTrigger function returns failure.\r\n");

                    MemReleaseMemBlock (CAPWAP_CAPWAPBASEACNAMELISTTABLE_POOLID,
                                        (UINT1 *)
                                        pCapwapCapwapBaseAcNameListEntry);
                    MemReleaseMemBlock (CAPWAP_CAPWAPBASEACNAMELISTTABLE_POOLID,
                                        (UINT1 *)
                                        pCapwapOldCapwapBaseAcNameListEntry);
                    MemReleaseMemBlock (CAPWAP_CAPWAPBASEACNAMELISTTABLE_POOLID,
                                        (UINT1 *)
                                        pCapwapTrgCapwapBaseAcNameListEntry);
                    MemReleaseMemBlock
                        (CAPWAP_CAPWAPBASEACNAMELISTTABLE_ISSET_POOLID,
                         (UINT1 *) pCapwapTrgIsSetCapwapBaseAcNameListEntry);
                    return OSIX_FAILURE;
                }
            }
            else if (pCapwapSetCapwapBaseAcNameListEntry->MibObject.
                     i4CapwapBaseAcNameListRowStatus == CREATE_AND_WAIT)
            {

                /* For MSR and RM Trigger */
                pCapwapTrgCapwapBaseAcNameListEntry->MibObject.
                    i4CapwapBaseAcNameListRowStatus = CREATE_AND_WAIT;
                pCapwapTrgIsSetCapwapBaseAcNameListEntry->
                    bCapwapBaseAcNameListRowStatus = OSIX_TRUE;

                if (CapwapSetAllCapwapBaseAcNameListTableTrigger
                    (pCapwapTrgCapwapBaseAcNameListEntry,
                     pCapwapTrgIsSetCapwapBaseAcNameListEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllCapwapBaseAcNameListTable: CapwapSetAllCapwapBaseAcNameListTableTrigger function returns failure.\r\n");

                    MemReleaseMemBlock (CAPWAP_CAPWAPBASEACNAMELISTTABLE_POOLID,
                                        (UINT1 *)
                                        pCapwapCapwapBaseAcNameListEntry);
                    MemReleaseMemBlock (CAPWAP_CAPWAPBASEACNAMELISTTABLE_POOLID,
                                        (UINT1 *)
                                        pCapwapOldCapwapBaseAcNameListEntry);
                    MemReleaseMemBlock (CAPWAP_CAPWAPBASEACNAMELISTTABLE_POOLID,
                                        (UINT1 *)
                                        pCapwapTrgCapwapBaseAcNameListEntry);
                    MemReleaseMemBlock
                        (CAPWAP_CAPWAPBASEACNAMELISTTABLE_ISSET_POOLID,
                         (UINT1 *) pCapwapTrgIsSetCapwapBaseAcNameListEntry);
                    return OSIX_FAILURE;
                }
            }

            if (pCapwapSetCapwapBaseAcNameListEntry->MibObject.
                i4CapwapBaseAcNameListRowStatus == CREATE_AND_GO)
            {
                pCapwapSetCapwapBaseAcNameListEntry->MibObject.
                    i4CapwapBaseAcNameListRowStatus = ACTIVE;
            }

            if (CapwapSetAllCapwapBaseAcNameListTableTrigger
                (pCapwapSetCapwapBaseAcNameListEntry,
                 pCapwapIsSetCapwapBaseAcNameListEntry,
                 SNMP_SUCCESS) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllCapwapBaseAcNameListTable:  CapwapSetAllCapwapBaseAcNameListTableTrigger function returns failure.\r\n");
            }
            MemReleaseMemBlock (CAPWAP_CAPWAPBASEACNAMELISTTABLE_POOLID,
                                (UINT1 *) pCapwapOldCapwapBaseAcNameListEntry);
            MemReleaseMemBlock (CAPWAP_CAPWAPBASEACNAMELISTTABLE_POOLID,
                                (UINT1 *) pCapwapTrgCapwapBaseAcNameListEntry);
            MemReleaseMemBlock (CAPWAP_CAPWAPBASEACNAMELISTTABLE_ISSET_POOLID,
                                (UINT1 *)
                                pCapwapTrgIsSetCapwapBaseAcNameListEntry);
            return OSIX_SUCCESS;

        }
        else
        {
            if (CapwapSetAllCapwapBaseAcNameListTableTrigger
                (pCapwapSetCapwapBaseAcNameListEntry,
                 pCapwapIsSetCapwapBaseAcNameListEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllCapwapBaseAcNameListTable: CapwapSetAllCapwapBaseAcNameListTableTrigger function returns failure.\r\n");
            }
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllCapwapBaseAcNameListTable: Failure.\r\n");
            MemReleaseMemBlock (CAPWAP_CAPWAPBASEACNAMELISTTABLE_POOLID,
                                (UINT1 *) pCapwapOldCapwapBaseAcNameListEntry);
            MemReleaseMemBlock (CAPWAP_CAPWAPBASEACNAMELISTTABLE_POOLID,
                                (UINT1 *) pCapwapTrgCapwapBaseAcNameListEntry);
            MemReleaseMemBlock (CAPWAP_CAPWAPBASEACNAMELISTTABLE_ISSET_POOLID,
                                (UINT1 *)
                                pCapwapTrgIsSetCapwapBaseAcNameListEntry);
            return OSIX_FAILURE;
        }
    }
    else if ((pCapwapSetCapwapBaseAcNameListEntry->MibObject.
              i4CapwapBaseAcNameListRowStatus == CREATE_AND_WAIT)
             || (pCapwapSetCapwapBaseAcNameListEntry->MibObject.
                 i4CapwapBaseAcNameListRowStatus == CREATE_AND_GO))
    {
        if (CapwapSetAllCapwapBaseAcNameListTableTrigger
            (pCapwapSetCapwapBaseAcNameListEntry,
             pCapwapIsSetCapwapBaseAcNameListEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)

        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllCapwapBaseAcNameListTable: CapwapSetAllCapwapBaseAcNameListTableTrigger function returns failure.\r\n");
        }
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapSetAllCapwapBaseAcNameListTable: The row is already present.\r\n");
        MemReleaseMemBlock (CAPWAP_CAPWAPBASEACNAMELISTTABLE_POOLID,
                            (UINT1 *) pCapwapOldCapwapBaseAcNameListEntry);
        MemReleaseMemBlock (CAPWAP_CAPWAPBASEACNAMELISTTABLE_POOLID,
                            (UINT1 *) pCapwapTrgCapwapBaseAcNameListEntry);
        MemReleaseMemBlock (CAPWAP_CAPWAPBASEACNAMELISTTABLE_ISSET_POOLID,
                            (UINT1 *) pCapwapTrgIsSetCapwapBaseAcNameListEntry);
        return OSIX_FAILURE;
    }
    /* Copy the previous values before setting the new values */
    MEMCPY (pCapwapOldCapwapBaseAcNameListEntry,
            pCapwapCapwapBaseAcNameListEntry,
            sizeof (tCapwapCapwapBaseAcNameListEntry));

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pCapwapSetCapwapBaseAcNameListEntry->MibObject.
        i4CapwapBaseAcNameListRowStatus == DESTROY)
    {
        pCapwapCapwapBaseAcNameListEntry->MibObject.
            i4CapwapBaseAcNameListRowStatus = DESTROY;

        if (CapwapUtilUpdateCapwapBaseAcNameListTable
            (pCapwapOldCapwapBaseAcNameListEntry,
             pCapwapCapwapBaseAcNameListEntry,
             pCapwapIsSetCapwapBaseAcNameListEntry) != OSIX_SUCCESS)
        {

            if (CapwapSetAllCapwapBaseAcNameListTableTrigger
                (pCapwapSetCapwapBaseAcNameListEntry,
                 pCapwapIsSetCapwapBaseAcNameListEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllCapwapBaseAcNameListTable: CapwapSetAllCapwapBaseAcNameListTableTrigger function returns failure.\r\n");
            }
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllCapwapBaseAcNameListTable: CapwapUtilUpdateCapwapBaseAcNameListTable function returns failure.\r\n");
        }
        RBTreeRem (gCapwapGlobals.CapwapGlbMib.CapwapBaseAcNameListTable,
                   pCapwapCapwapBaseAcNameListEntry);
        if (CapwapSetAllCapwapBaseAcNameListTableTrigger
            (pCapwapSetCapwapBaseAcNameListEntry,
             pCapwapIsSetCapwapBaseAcNameListEntry,
             SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllCapwapBaseAcNameListTable: CapwapSetAllCapwapBaseAcNameListTableTrigger function returns failure.\r\n");
            MemReleaseMemBlock (CAPWAP_CAPWAPBASEACNAMELISTTABLE_POOLID,
                                (UINT1 *) pCapwapOldCapwapBaseAcNameListEntry);
            MemReleaseMemBlock (CAPWAP_CAPWAPBASEACNAMELISTTABLE_POOLID,
                                (UINT1 *) pCapwapTrgCapwapBaseAcNameListEntry);
            MemReleaseMemBlock (CAPWAP_CAPWAPBASEACNAMELISTTABLE_ISSET_POOLID,
                                (UINT1 *)
                                pCapwapTrgIsSetCapwapBaseAcNameListEntry);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (CAPWAP_CAPWAPBASEACNAMELISTTABLE_POOLID,
                            (UINT1 *) pCapwapCapwapBaseAcNameListEntry);
        MemReleaseMemBlock (CAPWAP_CAPWAPBASEACNAMELISTTABLE_POOLID,
                            (UINT1 *) pCapwapOldCapwapBaseAcNameListEntry);
        MemReleaseMemBlock (CAPWAP_CAPWAPBASEACNAMELISTTABLE_POOLID,
                            (UINT1 *) pCapwapTrgCapwapBaseAcNameListEntry);
        MemReleaseMemBlock (CAPWAP_CAPWAPBASEACNAMELISTTABLE_ISSET_POOLID,
                            (UINT1 *) pCapwapTrgIsSetCapwapBaseAcNameListEntry);
        return OSIX_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (CapwapBaseAcNameListTableFilterInputs
        (pCapwapCapwapBaseAcNameListEntry, pCapwapSetCapwapBaseAcNameListEntry,
         pCapwapIsSetCapwapBaseAcNameListEntry) != OSIX_TRUE)
    {
        MemReleaseMemBlock (CAPWAP_CAPWAPBASEACNAMELISTTABLE_POOLID,
                            (UINT1 *) pCapwapOldCapwapBaseAcNameListEntry);
        MemReleaseMemBlock (CAPWAP_CAPWAPBASEACNAMELISTTABLE_POOLID,
                            (UINT1 *) pCapwapTrgCapwapBaseAcNameListEntry);
        MemReleaseMemBlock (CAPWAP_CAPWAPBASEACNAMELISTTABLE_ISSET_POOLID,
                            (UINT1 *) pCapwapTrgIsSetCapwapBaseAcNameListEntry);
        return OSIX_SUCCESS;
    }

    /*This condtion is to make the row NOT_IN_SERVICE before modifying the values */
    if ((i4RowStatusLogic == TRUE) &&
        (pCapwapCapwapBaseAcNameListEntry->MibObject.
         i4CapwapBaseAcNameListRowStatus == ACTIVE)
        && (pCapwapSetCapwapBaseAcNameListEntry->MibObject.
            i4CapwapBaseAcNameListRowStatus != NOT_IN_SERVICE))
    {
        pCapwapCapwapBaseAcNameListEntry->MibObject.
            i4CapwapBaseAcNameListRowStatus = NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        pCapwapTrgCapwapBaseAcNameListEntry->MibObject.
            i4CapwapBaseAcNameListRowStatus = NOT_IN_SERVICE;
        pCapwapTrgIsSetCapwapBaseAcNameListEntry->
            bCapwapBaseAcNameListRowStatus = OSIX_TRUE;

        if (CapwapUtilUpdateCapwapBaseAcNameListTable
            (pCapwapOldCapwapBaseAcNameListEntry,
             pCapwapCapwapBaseAcNameListEntry,
             pCapwapIsSetCapwapBaseAcNameListEntry) != OSIX_SUCCESS)
        {
            /*Restore back with previous values */
            MEMCPY (pCapwapCapwapBaseAcNameListEntry,
                    pCapwapOldCapwapBaseAcNameListEntry,
                    sizeof (tCapwapCapwapBaseAcNameListEntry));
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllCapwapBaseAcNameListTable:                 CapwapUtilUpdateCapwapBaseAcNameListTable Function returns failure.\r\n");

            if (CapwapSetAllCapwapBaseAcNameListTableTrigger
                (pCapwapSetCapwapBaseAcNameListEntry,
                 pCapwapIsSetCapwapBaseAcNameListEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllCapwapBaseAcNameListTable: CapwapSetAllCapwapBaseAcNameListTableTrigger function returns failure.\r\n");
            }
            MemReleaseMemBlock (CAPWAP_CAPWAPBASEACNAMELISTTABLE_POOLID,
                                (UINT1 *) pCapwapOldCapwapBaseAcNameListEntry);
            MemReleaseMemBlock (CAPWAP_CAPWAPBASEACNAMELISTTABLE_POOLID,
                                (UINT1 *) pCapwapTrgCapwapBaseAcNameListEntry);
            MemReleaseMemBlock (CAPWAP_CAPWAPBASEACNAMELISTTABLE_ISSET_POOLID,
                                (UINT1 *)
                                pCapwapTrgIsSetCapwapBaseAcNameListEntry);
            return OSIX_FAILURE;
        }

        if (CapwapSetAllCapwapBaseAcNameListTableTrigger
            (pCapwapTrgCapwapBaseAcNameListEntry,
             pCapwapTrgIsSetCapwapBaseAcNameListEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllCapwapBaseAcNameListTable: CapwapSetAllCapwapBaseAcNameListTableTrigger function returns failure.\r\n");
        }
    }

    if (pCapwapSetCapwapBaseAcNameListEntry->MibObject.
        i4CapwapBaseAcNameListRowStatus == ACTIVE)
    {
        i4RowMakeActive = TRUE;
    }

    /* Assign values for the existing node */
    if (pCapwapIsSetCapwapBaseAcNameListEntry->bCapwapBaseAcNameListName !=
        OSIX_FALSE)
    {
        MEMSET (pCapwapCapwapBaseAcNameListEntry->MibObject.
                au1CapwapBaseAcNameListName, 0,
                pCapwapCapwapBaseAcNameListEntry->MibObject.
                i4CapwapBaseAcNameListNameLen);
        MEMCPY (pCapwapCapwapBaseAcNameListEntry->MibObject.
                au1CapwapBaseAcNameListName,
                pCapwapSetCapwapBaseAcNameListEntry->MibObject.
                au1CapwapBaseAcNameListName,
                pCapwapSetCapwapBaseAcNameListEntry->MibObject.
                i4CapwapBaseAcNameListNameLen);

        pCapwapCapwapBaseAcNameListEntry->MibObject.
            i4CapwapBaseAcNameListNameLen =
            pCapwapSetCapwapBaseAcNameListEntry->MibObject.
            i4CapwapBaseAcNameListNameLen;
    }
    if (pCapwapIsSetCapwapBaseAcNameListEntry->bCapwapBaseAcNameListPriority !=
        OSIX_FALSE)
    {
        pCapwapCapwapBaseAcNameListEntry->MibObject.
            u4CapwapBaseAcNameListPriority =
            pCapwapSetCapwapBaseAcNameListEntry->MibObject.
            u4CapwapBaseAcNameListPriority;
    }
    if (pCapwapIsSetCapwapBaseAcNameListEntry->bCapwapBaseAcNameListRowStatus !=
        OSIX_FALSE)
    {
        pCapwapCapwapBaseAcNameListEntry->MibObject.
            i4CapwapBaseAcNameListRowStatus =
            pCapwapSetCapwapBaseAcNameListEntry->MibObject.
            i4CapwapBaseAcNameListRowStatus;
    }

    /*This condtion is to make the row back to ACTIVE after modifying the values */
    if (i4RowMakeActive == TRUE)
    {
        pCapwapCapwapBaseAcNameListEntry->MibObject.
            i4CapwapBaseAcNameListRowStatus = ACTIVE;
    }

    if (CapwapUtilUpdateCapwapBaseAcNameListTable
        (pCapwapOldCapwapBaseAcNameListEntry, pCapwapCapwapBaseAcNameListEntry,
         pCapwapIsSetCapwapBaseAcNameListEntry) != OSIX_SUCCESS)
    {

        if (CapwapSetAllCapwapBaseAcNameListTableTrigger
            (pCapwapSetCapwapBaseAcNameListEntry,
             pCapwapIsSetCapwapBaseAcNameListEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)

        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllCapwapBaseAcNameListTable: CapwapSetAllCapwapBaseAcNameListTableTrigger function returns failure.\r\n");

        }
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapSetAllCapwapBaseAcNameListTable: CapwapUtilUpdateCapwapBaseAcNameListTable function returns failure.\r\n");
        /*Restore back with previous values */
        MEMCPY (pCapwapCapwapBaseAcNameListEntry,
                pCapwapOldCapwapBaseAcNameListEntry,
                sizeof (tCapwapCapwapBaseAcNameListEntry));
        MemReleaseMemBlock (CAPWAP_CAPWAPBASEACNAMELISTTABLE_POOLID,
                            (UINT1 *) pCapwapOldCapwapBaseAcNameListEntry);
        MemReleaseMemBlock (CAPWAP_CAPWAPBASEACNAMELISTTABLE_POOLID,
                            (UINT1 *) pCapwapTrgCapwapBaseAcNameListEntry);
        MemReleaseMemBlock (CAPWAP_CAPWAPBASEACNAMELISTTABLE_ISSET_POOLID,
                            (UINT1 *) pCapwapTrgIsSetCapwapBaseAcNameListEntry);
        return OSIX_FAILURE;

    }
    if (CapwapSetAllCapwapBaseAcNameListTableTrigger
        (pCapwapSetCapwapBaseAcNameListEntry,
         pCapwapIsSetCapwapBaseAcNameListEntry, SNMP_SUCCESS) != OSIX_SUCCESS)
    {

        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapSetAllCapwapBaseAcNameListTable: CapwapSetAllCapwapBaseAcNameListTableTrigger function returns failure.\r\n");
    }

    MemReleaseMemBlock (CAPWAP_CAPWAPBASEACNAMELISTTABLE_POOLID,
                        (UINT1 *) pCapwapOldCapwapBaseAcNameListEntry);
    MemReleaseMemBlock (CAPWAP_CAPWAPBASEACNAMELISTTABLE_POOLID,
                        (UINT1 *) pCapwapTrgCapwapBaseAcNameListEntry);
    MemReleaseMemBlock (CAPWAP_CAPWAPBASEACNAMELISTTABLE_ISSET_POOLID,
                        (UINT1 *) pCapwapTrgIsSetCapwapBaseAcNameListEntry);
    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  CapwapSetAllCapwapBaseMacAclTable
 Input       :  pCapwapSetCapwapBaseMacAclEntry
                pCapwapIsSetCapwapBaseMacAclEntry
                i4RowStatusLogic
                i4RowCreateOption
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapSetAllCapwapBaseMacAclTable (tCapwapCapwapBaseMacAclEntry *
                                   pCapwapSetCapwapBaseMacAclEntry,
                                   tCapwapIsSetCapwapBaseMacAclEntry *
                                   pCapwapIsSetCapwapBaseMacAclEntry,
                                   INT4 i4RowStatusLogic,
                                   INT4 i4RowCreateOption)
{
    tCapwapCapwapBaseMacAclEntry *pCapwapCapwapBaseMacAclEntry = NULL;
    tCapwapCapwapBaseMacAclEntry *pCapwapOldCapwapBaseMacAclEntry = NULL;
    tCapwapCapwapBaseMacAclEntry *pCapwapTrgCapwapBaseMacAclEntry = NULL;
    tCapwapIsSetCapwapBaseMacAclEntry *pCapwapTrgIsSetCapwapBaseMacAclEntry =
        NULL;
    INT4                i4RowMakeActive = FALSE;

    pCapwapOldCapwapBaseMacAclEntry =
        (tCapwapCapwapBaseMacAclEntry *)
        MemAllocMemBlk (CAPWAP_CAPWAPBASEMACACLTABLE_POOLID);
    if (pCapwapOldCapwapBaseMacAclEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    pCapwapTrgCapwapBaseMacAclEntry =
        (tCapwapCapwapBaseMacAclEntry *)
        MemAllocMemBlk (CAPWAP_CAPWAPBASEMACACLTABLE_POOLID);
    if (pCapwapTrgCapwapBaseMacAclEntry == NULL)
    {
        MemReleaseMemBlock (CAPWAP_CAPWAPBASEMACACLTABLE_POOLID,
                            (UINT1 *) pCapwapOldCapwapBaseMacAclEntry);
        return OSIX_FAILURE;
    }
    pCapwapTrgIsSetCapwapBaseMacAclEntry =
        (tCapwapIsSetCapwapBaseMacAclEntry *)
        MemAllocMemBlk (CAPWAP_CAPWAPBASEMACACLTABLE_ISSET_POOLID);
    if (pCapwapTrgIsSetCapwapBaseMacAclEntry == NULL)
    {
        MemReleaseMemBlock (CAPWAP_CAPWAPBASEMACACLTABLE_POOLID,
                            (UINT1 *) pCapwapOldCapwapBaseMacAclEntry);
        MemReleaseMemBlock (CAPWAP_CAPWAPBASEMACACLTABLE_POOLID,
                            (UINT1 *) pCapwapTrgCapwapBaseMacAclEntry);
        return OSIX_FAILURE;
    }
    MEMSET (pCapwapOldCapwapBaseMacAclEntry, 0,
            sizeof (tCapwapCapwapBaseMacAclEntry));
    MEMSET (pCapwapTrgCapwapBaseMacAclEntry, 0,
            sizeof (tCapwapCapwapBaseMacAclEntry));
    MEMSET (pCapwapTrgIsSetCapwapBaseMacAclEntry, 0,
            sizeof (tCapwapIsSetCapwapBaseMacAclEntry));

    /* Check whether the node is already present */
    pCapwapCapwapBaseMacAclEntry =
        RBTreeGet (gCapwapGlobals.CapwapGlbMib.CapwapBaseMacAclTable,
                   (tRBElem *) pCapwapSetCapwapBaseMacAclEntry);

    if (pCapwapCapwapBaseMacAclEntry == NULL)
    {
        /* Create the node if the RowStatus given is CREATE_AND_WAIT or CREATE_AND_GO */
        if ((pCapwapSetCapwapBaseMacAclEntry->MibObject.
             i4CapwapBaseMacAclRowStatus == CREATE_AND_WAIT)
            || (pCapwapSetCapwapBaseMacAclEntry->MibObject.
                i4CapwapBaseMacAclRowStatus == CREATE_AND_GO)
            ||
            ((pCapwapSetCapwapBaseMacAclEntry->MibObject.
              i4CapwapBaseMacAclRowStatus == ACTIVE)
             && (i4RowCreateOption == 1)))
        {
            /* Allocate memory for the new node */
            pCapwapCapwapBaseMacAclEntry =
                (tCapwapCapwapBaseMacAclEntry *)
                MemAllocMemBlk (CAPWAP_CAPWAPBASEMACACLTABLE_POOLID);
            if (pCapwapCapwapBaseMacAclEntry == NULL)
            {
                if (CapwapSetAllCapwapBaseMacAclTableTrigger
                    (pCapwapSetCapwapBaseMacAclEntry,
                     pCapwapIsSetCapwapBaseMacAclEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllCapwapBaseMacAclTable:CapwapSetAllCapwapBaseMacAclTableTrigger function fails\r\n");

                }
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllCapwapBaseMacAclTable: Fail to Allocate Memory.\r\n");
                MemReleaseMemBlock (CAPWAP_CAPWAPBASEMACACLTABLE_POOLID,
                                    (UINT1 *) pCapwapOldCapwapBaseMacAclEntry);
                MemReleaseMemBlock (CAPWAP_CAPWAPBASEMACACLTABLE_POOLID,
                                    (UINT1 *) pCapwapTrgCapwapBaseMacAclEntry);
                MemReleaseMemBlock (CAPWAP_CAPWAPBASEMACACLTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgIsSetCapwapBaseMacAclEntry);
                return OSIX_FAILURE;
            }

            MEMSET (pCapwapCapwapBaseMacAclEntry, 0,
                    sizeof (tCapwapCapwapBaseMacAclEntry));
            if ((CapwapInitializeCapwapBaseMacAclTable
                 (pCapwapCapwapBaseMacAclEntry)) == OSIX_FAILURE)
            {
                if (CapwapSetAllCapwapBaseMacAclTableTrigger
                    (pCapwapSetCapwapBaseMacAclEntry,
                     pCapwapIsSetCapwapBaseMacAclEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllCapwapBaseMacAclTable:CapwapSetAllCapwapBaseMacAclTableTrigger function fails\r\n");

                }
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllCapwapBaseMacAclTable: Fail to Initialize the Objects.\r\n");

                MemReleaseMemBlock (CAPWAP_CAPWAPBASEMACACLTABLE_POOLID,
                                    (UINT1 *) pCapwapCapwapBaseMacAclEntry);
                MemReleaseMemBlock (CAPWAP_CAPWAPBASEMACACLTABLE_POOLID,
                                    (UINT1 *) pCapwapOldCapwapBaseMacAclEntry);
                MemReleaseMemBlock (CAPWAP_CAPWAPBASEMACACLTABLE_POOLID,
                                    (UINT1 *) pCapwapTrgCapwapBaseMacAclEntry);
                MemReleaseMemBlock (CAPWAP_CAPWAPBASEMACACLTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgIsSetCapwapBaseMacAclEntry);
                return OSIX_FAILURE;
            }
            /* Assign values for the new node */
            if (pCapwapIsSetCapwapBaseMacAclEntry->bCapwapBaseMacAclId !=
                OSIX_FALSE)
            {
                pCapwapCapwapBaseMacAclEntry->MibObject.u4CapwapBaseMacAclId =
                    pCapwapSetCapwapBaseMacAclEntry->MibObject.
                    u4CapwapBaseMacAclId;
            }

            if (pCapwapIsSetCapwapBaseMacAclEntry->bCapwapBaseMacAclStationId !=
                OSIX_FALSE)
            {
                MEMCPY (pCapwapCapwapBaseMacAclEntry->MibObject.
                        au1CapwapBaseMacAclStationId,
                        pCapwapSetCapwapBaseMacAclEntry->MibObject.
                        au1CapwapBaseMacAclStationId,
                        pCapwapSetCapwapBaseMacAclEntry->MibObject.
                        i4CapwapBaseMacAclStationIdLen);

                pCapwapCapwapBaseMacAclEntry->MibObject.
                    i4CapwapBaseMacAclStationIdLen =
                    pCapwapSetCapwapBaseMacAclEntry->MibObject.
                    i4CapwapBaseMacAclStationIdLen;
            }

            if (pCapwapIsSetCapwapBaseMacAclEntry->bCapwapBaseMacAclRowStatus !=
                OSIX_FALSE)
            {
                pCapwapCapwapBaseMacAclEntry->MibObject.
                    i4CapwapBaseMacAclRowStatus =
                    pCapwapSetCapwapBaseMacAclEntry->MibObject.
                    i4CapwapBaseMacAclRowStatus;
            }

            if ((pCapwapSetCapwapBaseMacAclEntry->MibObject.
                 i4CapwapBaseMacAclRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pCapwapSetCapwapBaseMacAclEntry->MibObject.
                        i4CapwapBaseMacAclRowStatus == ACTIVE)))
            {
                pCapwapCapwapBaseMacAclEntry->MibObject.
                    i4CapwapBaseMacAclRowStatus = ACTIVE;
            }
            else if (pCapwapSetCapwapBaseMacAclEntry->MibObject.
                     i4CapwapBaseMacAclRowStatus == CREATE_AND_WAIT)
            {
                pCapwapCapwapBaseMacAclEntry->MibObject.
                    i4CapwapBaseMacAclRowStatus = NOT_READY;
            }

            /* Add the new node to the database */
            if (RBTreeAdd
                (gCapwapGlobals.CapwapGlbMib.CapwapBaseMacAclTable,
                 (tRBElem *) pCapwapCapwapBaseMacAclEntry) != RB_SUCCESS)
            {
                if (CapwapSetAllCapwapBaseMacAclTableTrigger
                    (pCapwapSetCapwapBaseMacAclEntry,
                     pCapwapIsSetCapwapBaseMacAclEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllCapwapBaseMacAclTable: CapwapSetAllCapwapBaseMacAclTableTrigger function returns failure.\r\n");
                }
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllCapwapBaseMacAclTable: RBTreeAdd is failed.\r\n");

                MemReleaseMemBlock (CAPWAP_CAPWAPBASEMACACLTABLE_POOLID,
                                    (UINT1 *) pCapwapCapwapBaseMacAclEntry);
                MemReleaseMemBlock (CAPWAP_CAPWAPBASEMACACLTABLE_POOLID,
                                    (UINT1 *) pCapwapOldCapwapBaseMacAclEntry);
                MemReleaseMemBlock (CAPWAP_CAPWAPBASEMACACLTABLE_POOLID,
                                    (UINT1 *) pCapwapTrgCapwapBaseMacAclEntry);
                MemReleaseMemBlock (CAPWAP_CAPWAPBASEMACACLTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgIsSetCapwapBaseMacAclEntry);
                return OSIX_FAILURE;
            }
            if (CapwapUtilUpdateCapwapBaseMacAclTable
                (NULL, pCapwapCapwapBaseMacAclEntry,
                 pCapwapIsSetCapwapBaseMacAclEntry) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllCapwapBaseMacAclTable: CapwapUtilUpdateCapwapBaseMacAclTable function returns failure.\r\n");

                if (CapwapSetAllCapwapBaseMacAclTableTrigger
                    (pCapwapSetCapwapBaseMacAclEntry,
                     pCapwapIsSetCapwapBaseMacAclEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllCapwapBaseMacAclTable: CapwapSetAllCapwapBaseMacAclTableTrigger function returns failure.\r\n");

                }
                RBTreeRem (gCapwapGlobals.CapwapGlbMib.CapwapBaseMacAclTable,
                           pCapwapCapwapBaseMacAclEntry);
                MemReleaseMemBlock (CAPWAP_CAPWAPBASEMACACLTABLE_POOLID,
                                    (UINT1 *) pCapwapCapwapBaseMacAclEntry);
                MemReleaseMemBlock (CAPWAP_CAPWAPBASEMACACLTABLE_POOLID,
                                    (UINT1 *) pCapwapOldCapwapBaseMacAclEntry);
                MemReleaseMemBlock (CAPWAP_CAPWAPBASEMACACLTABLE_POOLID,
                                    (UINT1 *) pCapwapTrgCapwapBaseMacAclEntry);
                MemReleaseMemBlock (CAPWAP_CAPWAPBASEMACACLTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgIsSetCapwapBaseMacAclEntry);
                return OSIX_FAILURE;
            }

            if ((pCapwapSetCapwapBaseMacAclEntry->MibObject.
                 i4CapwapBaseMacAclRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pCapwapSetCapwapBaseMacAclEntry->MibObject.
                        i4CapwapBaseMacAclRowStatus == ACTIVE)))
            {

                /* For MSR and RM Trigger */
                pCapwapTrgCapwapBaseMacAclEntry->MibObject.
                    u4CapwapBaseMacAclId =
                    pCapwapSetCapwapBaseMacAclEntry->MibObject.
                    u4CapwapBaseMacAclId;
                pCapwapTrgCapwapBaseMacAclEntry->MibObject.
                    i4CapwapBaseMacAclRowStatus = CREATE_AND_WAIT;
                pCapwapTrgIsSetCapwapBaseMacAclEntry->
                    bCapwapBaseMacAclRowStatus = OSIX_TRUE;

                if (CapwapSetAllCapwapBaseMacAclTableTrigger
                    (pCapwapTrgCapwapBaseMacAclEntry,
                     pCapwapTrgIsSetCapwapBaseMacAclEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllCapwapBaseMacAclTable: CapwapSetAllCapwapBaseMacAclTableTrigger function returns failure.\r\n");

                    MemReleaseMemBlock (CAPWAP_CAPWAPBASEMACACLTABLE_POOLID,
                                        (UINT1 *) pCapwapCapwapBaseMacAclEntry);
                    MemReleaseMemBlock (CAPWAP_CAPWAPBASEMACACLTABLE_POOLID,
                                        (UINT1 *)
                                        pCapwapOldCapwapBaseMacAclEntry);
                    MemReleaseMemBlock (CAPWAP_CAPWAPBASEMACACLTABLE_POOLID,
                                        (UINT1 *)
                                        pCapwapTrgCapwapBaseMacAclEntry);
                    MemReleaseMemBlock
                        (CAPWAP_CAPWAPBASEMACACLTABLE_ISSET_POOLID,
                         (UINT1 *) pCapwapTrgIsSetCapwapBaseMacAclEntry);
                    return OSIX_FAILURE;
                }
            }
            else if (pCapwapSetCapwapBaseMacAclEntry->MibObject.
                     i4CapwapBaseMacAclRowStatus == CREATE_AND_WAIT)
            {

                /* For MSR and RM Trigger */
                pCapwapTrgCapwapBaseMacAclEntry->MibObject.
                    i4CapwapBaseMacAclRowStatus = CREATE_AND_WAIT;
                pCapwapTrgIsSetCapwapBaseMacAclEntry->
                    bCapwapBaseMacAclRowStatus = OSIX_TRUE;

                if (CapwapSetAllCapwapBaseMacAclTableTrigger
                    (pCapwapTrgCapwapBaseMacAclEntry,
                     pCapwapTrgIsSetCapwapBaseMacAclEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllCapwapBaseMacAclTable: CapwapSetAllCapwapBaseMacAclTableTrigger function returns failure.\r\n");

                    MemReleaseMemBlock (CAPWAP_CAPWAPBASEMACACLTABLE_POOLID,
                                        (UINT1 *) pCapwapCapwapBaseMacAclEntry);
                    MemReleaseMemBlock (CAPWAP_CAPWAPBASEMACACLTABLE_POOLID,
                                        (UINT1 *)
                                        pCapwapOldCapwapBaseMacAclEntry);
                    MemReleaseMemBlock (CAPWAP_CAPWAPBASEMACACLTABLE_POOLID,
                                        (UINT1 *)
                                        pCapwapTrgCapwapBaseMacAclEntry);
                    MemReleaseMemBlock
                        (CAPWAP_CAPWAPBASEMACACLTABLE_ISSET_POOLID,
                         (UINT1 *) pCapwapTrgIsSetCapwapBaseMacAclEntry);
                    return OSIX_FAILURE;
                }
            }

            if (pCapwapSetCapwapBaseMacAclEntry->MibObject.
                i4CapwapBaseMacAclRowStatus == CREATE_AND_GO)
            {
                pCapwapSetCapwapBaseMacAclEntry->MibObject.
                    i4CapwapBaseMacAclRowStatus = ACTIVE;
            }

            if (CapwapSetAllCapwapBaseMacAclTableTrigger
                (pCapwapSetCapwapBaseMacAclEntry,
                 pCapwapIsSetCapwapBaseMacAclEntry,
                 SNMP_SUCCESS) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllCapwapBaseMacAclTable:  CapwapSetAllCapwapBaseMacAclTableTrigger function returns failure.\r\n");
            }
            MemReleaseMemBlock (CAPWAP_CAPWAPBASEMACACLTABLE_POOLID,
                                (UINT1 *) pCapwapOldCapwapBaseMacAclEntry);
            MemReleaseMemBlock (CAPWAP_CAPWAPBASEMACACLTABLE_POOLID,
                                (UINT1 *) pCapwapTrgCapwapBaseMacAclEntry);
            MemReleaseMemBlock (CAPWAP_CAPWAPBASEMACACLTABLE_ISSET_POOLID,
                                (UINT1 *) pCapwapTrgIsSetCapwapBaseMacAclEntry);
            return OSIX_SUCCESS;

        }
        else
        {
            if (CapwapSetAllCapwapBaseMacAclTableTrigger
                (pCapwapSetCapwapBaseMacAclEntry,
                 pCapwapIsSetCapwapBaseMacAclEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllCapwapBaseMacAclTable: CapwapSetAllCapwapBaseMacAclTableTrigger function returns failure.\r\n");
            }
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllCapwapBaseMacAclTable: Failure.\r\n");
            MemReleaseMemBlock (CAPWAP_CAPWAPBASEMACACLTABLE_POOLID,
                                (UINT1 *) pCapwapOldCapwapBaseMacAclEntry);
            MemReleaseMemBlock (CAPWAP_CAPWAPBASEMACACLTABLE_POOLID,
                                (UINT1 *) pCapwapTrgCapwapBaseMacAclEntry);
            MemReleaseMemBlock (CAPWAP_CAPWAPBASEMACACLTABLE_ISSET_POOLID,
                                (UINT1 *) pCapwapTrgIsSetCapwapBaseMacAclEntry);
            return OSIX_FAILURE;
        }
    }
    else if ((pCapwapSetCapwapBaseMacAclEntry->MibObject.
              i4CapwapBaseMacAclRowStatus == CREATE_AND_WAIT)
             || (pCapwapSetCapwapBaseMacAclEntry->MibObject.
                 i4CapwapBaseMacAclRowStatus == CREATE_AND_GO))
    {
        if (CapwapSetAllCapwapBaseMacAclTableTrigger
            (pCapwapSetCapwapBaseMacAclEntry, pCapwapIsSetCapwapBaseMacAclEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)

        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllCapwapBaseMacAclTable: CapwapSetAllCapwapBaseMacAclTableTrigger function returns failure.\r\n");
        }
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapSetAllCapwapBaseMacAclTable: The row is already present.\r\n");
        MemReleaseMemBlock (CAPWAP_CAPWAPBASEMACACLTABLE_POOLID,
                            (UINT1 *) pCapwapOldCapwapBaseMacAclEntry);
        MemReleaseMemBlock (CAPWAP_CAPWAPBASEMACACLTABLE_POOLID,
                            (UINT1 *) pCapwapTrgCapwapBaseMacAclEntry);
        MemReleaseMemBlock (CAPWAP_CAPWAPBASEMACACLTABLE_ISSET_POOLID,
                            (UINT1 *) pCapwapTrgIsSetCapwapBaseMacAclEntry);
        return OSIX_FAILURE;
    }
    /* Copy the previous values before setting the new values */
    MEMCPY (pCapwapOldCapwapBaseMacAclEntry, pCapwapCapwapBaseMacAclEntry,
            sizeof (tCapwapCapwapBaseMacAclEntry));

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pCapwapSetCapwapBaseMacAclEntry->MibObject.
        i4CapwapBaseMacAclRowStatus == DESTROY)
    {
        pCapwapCapwapBaseMacAclEntry->MibObject.i4CapwapBaseMacAclRowStatus =
            DESTROY;

        if (CapwapUtilUpdateCapwapBaseMacAclTable
            (pCapwapOldCapwapBaseMacAclEntry, pCapwapCapwapBaseMacAclEntry,
             pCapwapIsSetCapwapBaseMacAclEntry) != OSIX_SUCCESS)
        {

            if (CapwapSetAllCapwapBaseMacAclTableTrigger
                (pCapwapSetCapwapBaseMacAclEntry,
                 pCapwapIsSetCapwapBaseMacAclEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllCapwapBaseMacAclTable: CapwapSetAllCapwapBaseMacAclTableTrigger function returns failure.\r\n");
            }
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllCapwapBaseMacAclTable: CapwapUtilUpdateCapwapBaseMacAclTable function returns failure.\r\n");
        }
        RBTreeRem (gCapwapGlobals.CapwapGlbMib.CapwapBaseMacAclTable,
                   pCapwapCapwapBaseMacAclEntry);
        if (CapwapSetAllCapwapBaseMacAclTableTrigger
            (pCapwapSetCapwapBaseMacAclEntry, pCapwapIsSetCapwapBaseMacAclEntry,
             SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllCapwapBaseMacAclTable: CapwapSetAllCapwapBaseMacAclTableTrigger function returns failure.\r\n");
            MemReleaseMemBlock (CAPWAP_CAPWAPBASEMACACLTABLE_POOLID,
                                (UINT1 *) pCapwapOldCapwapBaseMacAclEntry);
            MemReleaseMemBlock (CAPWAP_CAPWAPBASEMACACLTABLE_POOLID,
                                (UINT1 *) pCapwapTrgCapwapBaseMacAclEntry);
            MemReleaseMemBlock (CAPWAP_CAPWAPBASEMACACLTABLE_ISSET_POOLID,
                                (UINT1 *) pCapwapTrgIsSetCapwapBaseMacAclEntry);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (CAPWAP_CAPWAPBASEMACACLTABLE_POOLID,
                            (UINT1 *) pCapwapCapwapBaseMacAclEntry);
        MemReleaseMemBlock (CAPWAP_CAPWAPBASEMACACLTABLE_POOLID,
                            (UINT1 *) pCapwapOldCapwapBaseMacAclEntry);
        MemReleaseMemBlock (CAPWAP_CAPWAPBASEMACACLTABLE_POOLID,
                            (UINT1 *) pCapwapTrgCapwapBaseMacAclEntry);
        MemReleaseMemBlock (CAPWAP_CAPWAPBASEMACACLTABLE_ISSET_POOLID,
                            (UINT1 *) pCapwapTrgIsSetCapwapBaseMacAclEntry);
        return OSIX_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (CapwapBaseMacAclTableFilterInputs
        (pCapwapCapwapBaseMacAclEntry, pCapwapSetCapwapBaseMacAclEntry,
         pCapwapIsSetCapwapBaseMacAclEntry) != OSIX_TRUE)
    {
        MemReleaseMemBlock (CAPWAP_CAPWAPBASEMACACLTABLE_POOLID,
                            (UINT1 *) pCapwapOldCapwapBaseMacAclEntry);
        MemReleaseMemBlock (CAPWAP_CAPWAPBASEMACACLTABLE_POOLID,
                            (UINT1 *) pCapwapTrgCapwapBaseMacAclEntry);
        MemReleaseMemBlock (CAPWAP_CAPWAPBASEMACACLTABLE_ISSET_POOLID,
                            (UINT1 *) pCapwapTrgIsSetCapwapBaseMacAclEntry);
        return OSIX_SUCCESS;
    }

    /*This condtion is to make the row NOT_IN_SERVICE before modifying the values */
    if ((i4RowStatusLogic == TRUE) &&
        (pCapwapCapwapBaseMacAclEntry->MibObject.i4CapwapBaseMacAclRowStatus ==
         ACTIVE)
        && (pCapwapSetCapwapBaseMacAclEntry->MibObject.
            i4CapwapBaseMacAclRowStatus != NOT_IN_SERVICE))
    {
        pCapwapCapwapBaseMacAclEntry->MibObject.i4CapwapBaseMacAclRowStatus =
            NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        pCapwapTrgCapwapBaseMacAclEntry->MibObject.i4CapwapBaseMacAclRowStatus =
            NOT_IN_SERVICE;
        pCapwapTrgIsSetCapwapBaseMacAclEntry->bCapwapBaseMacAclRowStatus =
            OSIX_TRUE;

        if (CapwapUtilUpdateCapwapBaseMacAclTable
            (pCapwapOldCapwapBaseMacAclEntry, pCapwapCapwapBaseMacAclEntry,
             pCapwapIsSetCapwapBaseMacAclEntry) != OSIX_SUCCESS)
        {
            /*Restore back with previous values */
            MEMCPY (pCapwapCapwapBaseMacAclEntry,
                    pCapwapOldCapwapBaseMacAclEntry,
                    sizeof (tCapwapCapwapBaseMacAclEntry));
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllCapwapBaseMacAclTable:                 CapwapUtilUpdateCapwapBaseMacAclTable Function returns failure.\r\n");

            if (CapwapSetAllCapwapBaseMacAclTableTrigger
                (pCapwapSetCapwapBaseMacAclEntry,
                 pCapwapIsSetCapwapBaseMacAclEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllCapwapBaseMacAclTable: CapwapSetAllCapwapBaseMacAclTableTrigger function returns failure.\r\n");
            }
            MemReleaseMemBlock (CAPWAP_CAPWAPBASEMACACLTABLE_POOLID,
                                (UINT1 *) pCapwapOldCapwapBaseMacAclEntry);
            MemReleaseMemBlock (CAPWAP_CAPWAPBASEMACACLTABLE_POOLID,
                                (UINT1 *) pCapwapTrgCapwapBaseMacAclEntry);
            MemReleaseMemBlock (CAPWAP_CAPWAPBASEMACACLTABLE_ISSET_POOLID,
                                (UINT1 *) pCapwapTrgIsSetCapwapBaseMacAclEntry);
            return OSIX_FAILURE;
        }

        if (CapwapSetAllCapwapBaseMacAclTableTrigger
            (pCapwapTrgCapwapBaseMacAclEntry,
             pCapwapTrgIsSetCapwapBaseMacAclEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllCapwapBaseMacAclTable: CapwapSetAllCapwapBaseMacAclTableTrigger function returns failure.\r\n");
        }
    }

    if (pCapwapSetCapwapBaseMacAclEntry->MibObject.
        i4CapwapBaseMacAclRowStatus == ACTIVE)
    {
        i4RowMakeActive = TRUE;
    }

    /* Assign values for the existing node */
    if (pCapwapIsSetCapwapBaseMacAclEntry->bCapwapBaseMacAclStationId !=
        OSIX_FALSE)
    {
        MEMCPY (pCapwapCapwapBaseMacAclEntry->MibObject.
                au1CapwapBaseMacAclStationId,
                pCapwapSetCapwapBaseMacAclEntry->MibObject.
                au1CapwapBaseMacAclStationId,
                pCapwapSetCapwapBaseMacAclEntry->MibObject.
                i4CapwapBaseMacAclStationIdLen);

        pCapwapCapwapBaseMacAclEntry->MibObject.i4CapwapBaseMacAclStationIdLen =
            pCapwapSetCapwapBaseMacAclEntry->MibObject.
            i4CapwapBaseMacAclStationIdLen;
    }
    if (pCapwapIsSetCapwapBaseMacAclEntry->bCapwapBaseMacAclRowStatus !=
        OSIX_FALSE)
    {
        pCapwapCapwapBaseMacAclEntry->MibObject.i4CapwapBaseMacAclRowStatus =
            pCapwapSetCapwapBaseMacAclEntry->MibObject.
            i4CapwapBaseMacAclRowStatus;
    }

    /*This condtion is to make the row back to ACTIVE after modifying the values */
    if (i4RowMakeActive == TRUE)
    {
        pCapwapCapwapBaseMacAclEntry->MibObject.i4CapwapBaseMacAclRowStatus =
            ACTIVE;
    }

    if (CapwapUtilUpdateCapwapBaseMacAclTable (pCapwapOldCapwapBaseMacAclEntry,
                                               pCapwapCapwapBaseMacAclEntry,
                                               pCapwapIsSetCapwapBaseMacAclEntry)
        != OSIX_SUCCESS)
    {

        if (CapwapSetAllCapwapBaseMacAclTableTrigger
            (pCapwapSetCapwapBaseMacAclEntry, pCapwapIsSetCapwapBaseMacAclEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)

        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllCapwapBaseMacAclTable: CapwapSetAllCapwapBaseMacAclTableTrigger function returns failure.\r\n");

        }
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapSetAllCapwapBaseMacAclTable: CapwapUtilUpdateCapwapBaseMacAclTable function returns failure.\r\n");
        /*Restore back with previous values */
        MEMCPY (pCapwapCapwapBaseMacAclEntry, pCapwapOldCapwapBaseMacAclEntry,
                sizeof (tCapwapCapwapBaseMacAclEntry));
        MemReleaseMemBlock (CAPWAP_CAPWAPBASEMACACLTABLE_POOLID,
                            (UINT1 *) pCapwapOldCapwapBaseMacAclEntry);
        MemReleaseMemBlock (CAPWAP_CAPWAPBASEMACACLTABLE_POOLID,
                            (UINT1 *) pCapwapTrgCapwapBaseMacAclEntry);
        MemReleaseMemBlock (CAPWAP_CAPWAPBASEMACACLTABLE_ISSET_POOLID,
                            (UINT1 *) pCapwapTrgIsSetCapwapBaseMacAclEntry);
        return OSIX_FAILURE;

    }
    if (CapwapSetAllCapwapBaseMacAclTableTrigger
        (pCapwapSetCapwapBaseMacAclEntry, pCapwapIsSetCapwapBaseMacAclEntry,
         SNMP_SUCCESS) != OSIX_SUCCESS)
    {

        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapSetAllCapwapBaseMacAclTable: CapwapSetAllCapwapBaseMacAclTableTrigger function returns failure.\r\n");
    }

    MemReleaseMemBlock (CAPWAP_CAPWAPBASEMACACLTABLE_POOLID,
                        (UINT1 *) pCapwapOldCapwapBaseMacAclEntry);
    MemReleaseMemBlock (CAPWAP_CAPWAPBASEMACACLTABLE_POOLID,
                        (UINT1 *) pCapwapTrgCapwapBaseMacAclEntry);
    MemReleaseMemBlock (CAPWAP_CAPWAPBASEMACACLTABLE_ISSET_POOLID,
                        (UINT1 *) pCapwapTrgIsSetCapwapBaseMacAclEntry);
    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  CapwapSetAllCapwapBaseWtpProfileTable
 Input       :  pCapwapSetCapwapBaseWtpProfileEntry
                pCapwapIsSetCapwapBaseWtpProfileEntry
                i4RowStatusLogic
                i4RowCreateOption
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapSetAllCapwapBaseWtpProfileTable (tCapwapCapwapBaseWtpProfileEntry *
                                       pCapwapSetCapwapBaseWtpProfileEntry,
                                       tCapwapIsSetCapwapBaseWtpProfileEntry *
                                       pCapwapIsSetCapwapBaseWtpProfileEntry,
                                       INT4 i4RowStatusLogic,
                                       INT4 i4RowCreateOption)
{
    tCapwapCapwapBaseWtpProfileEntry *pCapwapCapwapBaseWtpProfileEntry = NULL;
    tCapwapCapwapBaseWtpProfileEntry *pCapwapOldCapwapBaseWtpProfileEntry =
        NULL;
    tCapwapCapwapBaseWtpProfileEntry *pCapwapTrgCapwapBaseWtpProfileEntry =
        NULL;
    tCapwapIsSetCapwapBaseWtpProfileEntry
        * pCapwapTrgIsSetCapwapBaseWtpProfileEntry = NULL;
    INT4                i4RowMakeActive = FALSE;

    pCapwapOldCapwapBaseWtpProfileEntry =
        (tCapwapCapwapBaseWtpProfileEntry *)
        MemAllocMemBlk (CAPWAP_CAPWAPBASEWTPPROFILETABLE_POOLID);
    if (pCapwapOldCapwapBaseWtpProfileEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    pCapwapTrgCapwapBaseWtpProfileEntry =
        (tCapwapCapwapBaseWtpProfileEntry *)
        MemAllocMemBlk (CAPWAP_CAPWAPBASEWTPPROFILETABLE_POOLID);
    if (pCapwapTrgCapwapBaseWtpProfileEntry == NULL)
    {
        MemReleaseMemBlock (CAPWAP_CAPWAPBASEWTPPROFILETABLE_POOLID,
                            (UINT1 *) pCapwapOldCapwapBaseWtpProfileEntry);
        return OSIX_FAILURE;
    }
    pCapwapTrgIsSetCapwapBaseWtpProfileEntry =
        (tCapwapIsSetCapwapBaseWtpProfileEntry *)
        MemAllocMemBlk (CAPWAP_CAPWAPBASEWTPPROFILETABLE_ISSET_POOLID);
    if (pCapwapTrgIsSetCapwapBaseWtpProfileEntry == NULL)
    {
        MemReleaseMemBlock (CAPWAP_CAPWAPBASEWTPPROFILETABLE_POOLID,
                            (UINT1 *) pCapwapOldCapwapBaseWtpProfileEntry);
        MemReleaseMemBlock (CAPWAP_CAPWAPBASEWTPPROFILETABLE_POOLID,
                            (UINT1 *) pCapwapTrgCapwapBaseWtpProfileEntry);
        return OSIX_FAILURE;
    }
    MEMSET (pCapwapOldCapwapBaseWtpProfileEntry, 0,
            sizeof (tCapwapCapwapBaseWtpProfileEntry));
    MEMSET (pCapwapTrgCapwapBaseWtpProfileEntry, 0,
            sizeof (tCapwapCapwapBaseWtpProfileEntry));
    MEMSET (pCapwapTrgIsSetCapwapBaseWtpProfileEntry, 0,
            sizeof (tCapwapIsSetCapwapBaseWtpProfileEntry));

    /* Check whether the node is already present */
    pCapwapCapwapBaseWtpProfileEntry =
        RBTreeGet (gCapwapGlobals.CapwapGlbMib.CapwapBaseWtpProfileTable,
                   (tRBElem *) pCapwapSetCapwapBaseWtpProfileEntry);

    if (pCapwapCapwapBaseWtpProfileEntry == NULL)
    {
        /* Create the node if the RowStatus given is CREATE_AND_WAIT or CREATE_AND_GO */
        if ((pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
             i4CapwapBaseWtpProfileRowStatus == CREATE_AND_WAIT)
            || (pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                i4CapwapBaseWtpProfileRowStatus == CREATE_AND_GO)
            ||
            ((pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
              i4CapwapBaseWtpProfileRowStatus == ACTIVE)
             && (i4RowCreateOption == 1)))
        {
            /* Allocate memory for the new node */
            pCapwapCapwapBaseWtpProfileEntry =
                (tCapwapCapwapBaseWtpProfileEntry *)
                MemAllocMemBlk (CAPWAP_CAPWAPBASEWTPPROFILETABLE_POOLID);
            if (pCapwapCapwapBaseWtpProfileEntry == NULL)
            {
                if (CapwapSetAllCapwapBaseWtpProfileTableTrigger
                    (pCapwapSetCapwapBaseWtpProfileEntry,
                     pCapwapIsSetCapwapBaseWtpProfileEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllCapwapBaseWtpProfileTable:CapwapSetAllCapwapBaseWtpProfileTableTrigger function fails\r\n");

                }
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllCapwapBaseWtpProfileTable: Fail to Allocate Memory.\r\n");
                MemReleaseMemBlock (CAPWAP_CAPWAPBASEWTPPROFILETABLE_POOLID,
                                    (UINT1 *)
                                    pCapwapOldCapwapBaseWtpProfileEntry);
                MemReleaseMemBlock (CAPWAP_CAPWAPBASEWTPPROFILETABLE_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgCapwapBaseWtpProfileEntry);
                MemReleaseMemBlock
                    (CAPWAP_CAPWAPBASEWTPPROFILETABLE_ISSET_POOLID,
                     (UINT1 *) pCapwapTrgIsSetCapwapBaseWtpProfileEntry);
                return OSIX_FAILURE;
            }

            MEMSET (pCapwapCapwapBaseWtpProfileEntry, 0,
                    sizeof (tCapwapCapwapBaseWtpProfileEntry));
            if ((CapwapInitializeCapwapBaseWtpProfileTable
                 (pCapwapCapwapBaseWtpProfileEntry)) == OSIX_FAILURE)
            {
                if (CapwapSetAllCapwapBaseWtpProfileTableTrigger
                    (pCapwapSetCapwapBaseWtpProfileEntry,
                     pCapwapIsSetCapwapBaseWtpProfileEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllCapwapBaseWtpProfileTable:CapwapSetAllCapwapBaseWtpProfileTableTrigger function fails\r\n");

                }
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllCapwapBaseWtpProfileTable: Fail to Initialize the Objects.\r\n");

                MemReleaseMemBlock (CAPWAP_CAPWAPBASEWTPPROFILETABLE_POOLID,
                                    (UINT1 *) pCapwapCapwapBaseWtpProfileEntry);
                MemReleaseMemBlock (CAPWAP_CAPWAPBASEWTPPROFILETABLE_POOLID,
                                    (UINT1 *)
                                    pCapwapOldCapwapBaseWtpProfileEntry);
                MemReleaseMemBlock (CAPWAP_CAPWAPBASEWTPPROFILETABLE_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgCapwapBaseWtpProfileEntry);
                MemReleaseMemBlock
                    (CAPWAP_CAPWAPBASEWTPPROFILETABLE_ISSET_POOLID,
                     (UINT1 *) pCapwapTrgIsSetCapwapBaseWtpProfileEntry);
                return OSIX_FAILURE;
            }
            /* Assign values for the new node */
            if (pCapwapIsSetCapwapBaseWtpProfileEntry->
                bCapwapBaseWtpProfileId != OSIX_FALSE)
            {
                pCapwapCapwapBaseWtpProfileEntry->MibObject.
                    u4CapwapBaseWtpProfileId =
                    pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                    u4CapwapBaseWtpProfileId;
            }

            if (pCapwapIsSetCapwapBaseWtpProfileEntry->
                bCapwapBaseWtpProfileName != OSIX_FALSE)
            {
                MEMCPY (pCapwapCapwapBaseWtpProfileEntry->MibObject.
                        au1CapwapBaseWtpProfileName,
                        pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                        au1CapwapBaseWtpProfileName,
                        pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                        i4CapwapBaseWtpProfileNameLen);

                pCapwapCapwapBaseWtpProfileEntry->MibObject.
                    i4CapwapBaseWtpProfileNameLen =
                    pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                    i4CapwapBaseWtpProfileNameLen;
            }

            if (pCapwapIsSetCapwapBaseWtpProfileEntry->
                bCapwapBaseWtpProfileWtpMacAddress != OSIX_FALSE)
            {
                MEMCPY (pCapwapCapwapBaseWtpProfileEntry->MibObject.
                        au1CapwapBaseWtpProfileWtpMacAddress,
                        pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                        au1CapwapBaseWtpProfileWtpMacAddress,
                        pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                        i4CapwapBaseWtpProfileWtpMacAddressLen);

                pCapwapCapwapBaseWtpProfileEntry->MibObject.
                    i4CapwapBaseWtpProfileWtpMacAddressLen =
                    pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                    i4CapwapBaseWtpProfileWtpMacAddressLen;
            }

            if (pCapwapIsSetCapwapBaseWtpProfileEntry->
                bCapwapBaseWtpProfileWtpModelNumber != OSIX_FALSE)
            {
                MEMCPY (pCapwapCapwapBaseWtpProfileEntry->MibObject.
                        au1CapwapBaseWtpProfileWtpModelNumber,
                        pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                        au1CapwapBaseWtpProfileWtpModelNumber,
                        pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                        i4CapwapBaseWtpProfileWtpModelNumberLen);

                pCapwapCapwapBaseWtpProfileEntry->MibObject.
                    i4CapwapBaseWtpProfileWtpModelNumberLen =
                    pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                    i4CapwapBaseWtpProfileWtpModelNumberLen;
            }

            if (pCapwapIsSetCapwapBaseWtpProfileEntry->
                bCapwapBaseWtpProfileWtpName != OSIX_FALSE)
            {
                MEMCPY (pCapwapCapwapBaseWtpProfileEntry->MibObject.
                        au1CapwapBaseWtpProfileWtpName,
                        pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                        au1CapwapBaseWtpProfileWtpName,
                        pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                        i4CapwapBaseWtpProfileWtpNameLen);

                pCapwapCapwapBaseWtpProfileEntry->MibObject.
                    i4CapwapBaseWtpProfileWtpNameLen =
                    pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                    i4CapwapBaseWtpProfileWtpNameLen;
            }

            if (pCapwapIsSetCapwapBaseWtpProfileEntry->
                bCapwapBaseWtpProfileWtpLocation != OSIX_FALSE)
            {
                MEMCPY (pCapwapCapwapBaseWtpProfileEntry->MibObject.
                        au1CapwapBaseWtpProfileWtpLocation,
                        pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                        au1CapwapBaseWtpProfileWtpLocation,
                        pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                        i4CapwapBaseWtpProfileWtpLocationLen);

                pCapwapCapwapBaseWtpProfileEntry->MibObject.
                    i4CapwapBaseWtpProfileWtpLocationLen =
                    pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                    i4CapwapBaseWtpProfileWtpLocationLen;
            }

            if (pCapwapIsSetCapwapBaseWtpProfileEntry->
                bCapwapBaseWtpProfileWtpStaticIpEnable != OSIX_FALSE)
            {
                pCapwapCapwapBaseWtpProfileEntry->MibObject.
                    i4CapwapBaseWtpProfileWtpStaticIpEnable =
                    pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                    i4CapwapBaseWtpProfileWtpStaticIpEnable;
            }
            if (pCapwapIsSetCapwapBaseWtpProfileEntry->
                bFsCapwapFsAcTimestampTrigger != OSIX_FALSE)
            {
                pCapwapCapwapBaseWtpProfileEntry->MibObject.
                    i4FsCapwapFsAcTimestampTrigger =
                    pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                    i4FsCapwapFsAcTimestampTrigger;
            }

            if (pCapwapIsSetCapwapBaseWtpProfileEntry->
                bCapwapBaseWtpProfileWtpStaticIpType != OSIX_FALSE)
            {
                pCapwapCapwapBaseWtpProfileEntry->MibObject.
                    i4CapwapBaseWtpProfileWtpStaticIpType =
                    pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                    i4CapwapBaseWtpProfileWtpStaticIpType;
            }

            if (pCapwapIsSetCapwapBaseWtpProfileEntry->
                bCapwapBaseWtpProfileWtpStaticIpAddress != OSIX_FALSE)
            {
                MEMCPY (pCapwapCapwapBaseWtpProfileEntry->MibObject.
                        au1CapwapBaseWtpProfileWtpStaticIpAddress,
                        pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                        au1CapwapBaseWtpProfileWtpStaticIpAddress,
                        pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                        i4CapwapBaseWtpProfileWtpStaticIpAddressLen);

                pCapwapCapwapBaseWtpProfileEntry->MibObject.
                    i4CapwapBaseWtpProfileWtpStaticIpAddressLen =
                    pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                    i4CapwapBaseWtpProfileWtpStaticIpAddressLen;
            }

            if (pCapwapIsSetCapwapBaseWtpProfileEntry->
                bCapwapBaseWtpProfileWtpNetmask != OSIX_FALSE)
            {
                MEMCPY (pCapwapCapwapBaseWtpProfileEntry->MibObject.
                        au1CapwapBaseWtpProfileWtpNetmask,
                        pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                        au1CapwapBaseWtpProfileWtpNetmask,
                        pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                        i4CapwapBaseWtpProfileWtpNetmaskLen);

                pCapwapCapwapBaseWtpProfileEntry->MibObject.
                    i4CapwapBaseWtpProfileWtpNetmaskLen =
                    pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                    i4CapwapBaseWtpProfileWtpNetmaskLen;
            }

            if (pCapwapIsSetCapwapBaseWtpProfileEntry->
                bCapwapBaseWtpProfileWtpGateway != OSIX_FALSE)
            {
                MEMCPY (pCapwapCapwapBaseWtpProfileEntry->MibObject.
                        au1CapwapBaseWtpProfileWtpGateway,
                        pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                        au1CapwapBaseWtpProfileWtpGateway,
                        pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                        i4CapwapBaseWtpProfileWtpGatewayLen);

                pCapwapCapwapBaseWtpProfileEntry->MibObject.
                    i4CapwapBaseWtpProfileWtpGatewayLen =
                    pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                    i4CapwapBaseWtpProfileWtpGatewayLen;
            }

            if (pCapwapIsSetCapwapBaseWtpProfileEntry->
                bCapwapBaseWtpProfileWtpFallbackEnable != OSIX_FALSE)
            {
                pCapwapCapwapBaseWtpProfileEntry->MibObject.
                    i4CapwapBaseWtpProfileWtpFallbackEnable =
                    pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                    i4CapwapBaseWtpProfileWtpFallbackEnable;
            }

            if (pCapwapIsSetCapwapBaseWtpProfileEntry->
                bCapwapBaseWtpProfileWtpEchoInterval != OSIX_FALSE)
            {
                pCapwapCapwapBaseWtpProfileEntry->MibObject.
                    u4CapwapBaseWtpProfileWtpEchoInterval =
                    pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                    u4CapwapBaseWtpProfileWtpEchoInterval;
            }

            if (pCapwapIsSetCapwapBaseWtpProfileEntry->
                bCapwapBaseWtpProfileWtpIdleTimeout != OSIX_FALSE)
            {
                pCapwapCapwapBaseWtpProfileEntry->MibObject.
                    u4CapwapBaseWtpProfileWtpIdleTimeout =
                    pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                    u4CapwapBaseWtpProfileWtpIdleTimeout;
            }

            if (pCapwapIsSetCapwapBaseWtpProfileEntry->
                bCapwapBaseWtpProfileWtpMaxDiscoveryInterval != OSIX_FALSE)
            {
                pCapwapCapwapBaseWtpProfileEntry->MibObject.
                    u4CapwapBaseWtpProfileWtpMaxDiscoveryInterval =
                    pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                    u4CapwapBaseWtpProfileWtpMaxDiscoveryInterval;
            }

            if (pCapwapIsSetCapwapBaseWtpProfileEntry->
                bCapwapBaseWtpProfileWtpReportInterval != OSIX_FALSE)
            {
                pCapwapCapwapBaseWtpProfileEntry->MibObject.
                    u4CapwapBaseWtpProfileWtpReportInterval =
                    pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                    u4CapwapBaseWtpProfileWtpReportInterval;
            }

            if (pCapwapIsSetCapwapBaseWtpProfileEntry->
                bCapwapBaseWtpProfileWtpStatisticsTimer != OSIX_FALSE)
            {
                pCapwapCapwapBaseWtpProfileEntry->MibObject.
                    u4CapwapBaseWtpProfileWtpStatisticsTimer =
                    pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                    u4CapwapBaseWtpProfileWtpStatisticsTimer;
            }

            if (pCapwapIsSetCapwapBaseWtpProfileEntry->
                bCapwapBaseWtpProfileWtpEcnSupport != OSIX_FALSE)
            {
                pCapwapCapwapBaseWtpProfileEntry->MibObject.
                    i4CapwapBaseWtpProfileWtpEcnSupport =
                    pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                    i4CapwapBaseWtpProfileWtpEcnSupport;
            }

            if (pCapwapIsSetCapwapBaseWtpProfileEntry->
                bCapwapBaseWtpProfileRowStatus != OSIX_FALSE)
            {
                pCapwapCapwapBaseWtpProfileEntry->MibObject.
                    i4CapwapBaseWtpProfileRowStatus =
                    pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                    i4CapwapBaseWtpProfileRowStatus;
            }

            if ((pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                 i4CapwapBaseWtpProfileRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                        i4CapwapBaseWtpProfileRowStatus == ACTIVE)))
            {
                pCapwapCapwapBaseWtpProfileEntry->MibObject.
                    i4CapwapBaseWtpProfileRowStatus = ACTIVE;
            }
            else if (pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                     i4CapwapBaseWtpProfileRowStatus == CREATE_AND_WAIT)
            {
                pCapwapCapwapBaseWtpProfileEntry->MibObject.
                    i4CapwapBaseWtpProfileRowStatus = NOT_READY;
            }
            pCapwapCapwapBaseWtpProfileEntry->u4SystemUpTime =
                pCapwapSetCapwapBaseWtpProfileEntry->u4SystemUpTime;

            /* Add the new node to the database */
            if (RBTreeAdd
                (gCapwapGlobals.CapwapGlbMib.CapwapBaseWtpProfileTable,
                 (tRBElem *) pCapwapCapwapBaseWtpProfileEntry) != RB_SUCCESS)
            {
                if (CapwapSetAllCapwapBaseWtpProfileTableTrigger
                    (pCapwapSetCapwapBaseWtpProfileEntry,
                     pCapwapIsSetCapwapBaseWtpProfileEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllCapwapBaseWtpProfileTable: CapwapSetAllCapwapBaseWtpProfileTableTrigger function returns failure.\r\n");
                }
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllCapwapBaseWtpProfileTable: RBTreeAdd is failed.\r\n");

                MemReleaseMemBlock (CAPWAP_CAPWAPBASEWTPPROFILETABLE_POOLID,
                                    (UINT1 *) pCapwapCapwapBaseWtpProfileEntry);
                MemReleaseMemBlock (CAPWAP_CAPWAPBASEWTPPROFILETABLE_POOLID,
                                    (UINT1 *)
                                    pCapwapOldCapwapBaseWtpProfileEntry);
                MemReleaseMemBlock (CAPWAP_CAPWAPBASEWTPPROFILETABLE_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgCapwapBaseWtpProfileEntry);
                MemReleaseMemBlock
                    (CAPWAP_CAPWAPBASEWTPPROFILETABLE_ISSET_POOLID,
                     (UINT1 *) pCapwapTrgIsSetCapwapBaseWtpProfileEntry);
                return OSIX_FAILURE;
            }
            if (CapwapUtilUpdateCapwapBaseWtpProfileTable
                (NULL, pCapwapCapwapBaseWtpProfileEntry,
                 pCapwapIsSetCapwapBaseWtpProfileEntry) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllCapwapBaseWtpProfileTable: CapwapUtilUpdateCapwapBaseWtpProfileTable function returns failure.\r\n");

                if (CapwapSetAllCapwapBaseWtpProfileTableTrigger
                    (pCapwapSetCapwapBaseWtpProfileEntry,
                     pCapwapIsSetCapwapBaseWtpProfileEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllCapwapBaseWtpProfileTable: CapwapSetAllCapwapBaseWtpProfileTableTrigger function returns failure.\r\n");

                }
                RBTreeRem (gCapwapGlobals.CapwapGlbMib.
                           CapwapBaseWtpProfileTable,
                           pCapwapCapwapBaseWtpProfileEntry);
                MemReleaseMemBlock (CAPWAP_CAPWAPBASEWTPPROFILETABLE_POOLID,
                                    (UINT1 *) pCapwapCapwapBaseWtpProfileEntry);
                MemReleaseMemBlock (CAPWAP_CAPWAPBASEWTPPROFILETABLE_POOLID,
                                    (UINT1 *)
                                    pCapwapOldCapwapBaseWtpProfileEntry);
                MemReleaseMemBlock (CAPWAP_CAPWAPBASEWTPPROFILETABLE_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgCapwapBaseWtpProfileEntry);
                MemReleaseMemBlock
                    (CAPWAP_CAPWAPBASEWTPPROFILETABLE_ISSET_POOLID,
                     (UINT1 *) pCapwapTrgIsSetCapwapBaseWtpProfileEntry);
                return OSIX_FAILURE;
            }

            if ((pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                 i4CapwapBaseWtpProfileRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                        i4CapwapBaseWtpProfileRowStatus == ACTIVE)))
            {

                /* For MSR and RM Trigger */
                pCapwapTrgCapwapBaseWtpProfileEntry->MibObject.
                    u4CapwapBaseWtpProfileId =
                    pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                    u4CapwapBaseWtpProfileId;
                pCapwapTrgCapwapBaseWtpProfileEntry->MibObject.
                    i4CapwapBaseWtpProfileRowStatus = CREATE_AND_WAIT;
                pCapwapTrgIsSetCapwapBaseWtpProfileEntry->
                    bCapwapBaseWtpProfileRowStatus = OSIX_TRUE;

                if (CapwapSetAllCapwapBaseWtpProfileTableTrigger
                    (pCapwapTrgCapwapBaseWtpProfileEntry,
                     pCapwapTrgIsSetCapwapBaseWtpProfileEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllCapwapBaseWtpProfileTable: CapwapSetAllCapwapBaseWtpProfileTableTrigger function returns failure.\r\n");

                    MemReleaseMemBlock (CAPWAP_CAPWAPBASEWTPPROFILETABLE_POOLID,
                                        (UINT1 *)
                                        pCapwapCapwapBaseWtpProfileEntry);
                    MemReleaseMemBlock (CAPWAP_CAPWAPBASEWTPPROFILETABLE_POOLID,
                                        (UINT1 *)
                                        pCapwapOldCapwapBaseWtpProfileEntry);
                    MemReleaseMemBlock (CAPWAP_CAPWAPBASEWTPPROFILETABLE_POOLID,
                                        (UINT1 *)
                                        pCapwapTrgCapwapBaseWtpProfileEntry);
                    MemReleaseMemBlock
                        (CAPWAP_CAPWAPBASEWTPPROFILETABLE_ISSET_POOLID,
                         (UINT1 *) pCapwapTrgIsSetCapwapBaseWtpProfileEntry);
                    return OSIX_FAILURE;
                }
            }
            else if (pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                     i4CapwapBaseWtpProfileRowStatus == CREATE_AND_WAIT)
            {

                /* For MSR and RM Trigger */
                pCapwapTrgCapwapBaseWtpProfileEntry->MibObject.
                    i4CapwapBaseWtpProfileRowStatus = CREATE_AND_WAIT;
                pCapwapTrgIsSetCapwapBaseWtpProfileEntry->
                    bCapwapBaseWtpProfileRowStatus = OSIX_TRUE;

                if (CapwapSetAllCapwapBaseWtpProfileTableTrigger
                    (pCapwapTrgCapwapBaseWtpProfileEntry,
                     pCapwapTrgIsSetCapwapBaseWtpProfileEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllCapwapBaseWtpProfileTable: CapwapSetAllCapwapBaseWtpProfileTableTrigger function returns failure.\r\n");

                    MemReleaseMemBlock (CAPWAP_CAPWAPBASEWTPPROFILETABLE_POOLID,
                                        (UINT1 *)
                                        pCapwapCapwapBaseWtpProfileEntry);
                    MemReleaseMemBlock (CAPWAP_CAPWAPBASEWTPPROFILETABLE_POOLID,
                                        (UINT1 *)
                                        pCapwapOldCapwapBaseWtpProfileEntry);
                    MemReleaseMemBlock (CAPWAP_CAPWAPBASEWTPPROFILETABLE_POOLID,
                                        (UINT1 *)
                                        pCapwapTrgCapwapBaseWtpProfileEntry);
                    MemReleaseMemBlock
                        (CAPWAP_CAPWAPBASEWTPPROFILETABLE_ISSET_POOLID,
                         (UINT1 *) pCapwapTrgIsSetCapwapBaseWtpProfileEntry);
                    return OSIX_FAILURE;
                }
            }

            if (pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                i4CapwapBaseWtpProfileRowStatus == CREATE_AND_GO)
            {
                pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                    i4CapwapBaseWtpProfileRowStatus = ACTIVE;
            }

            if (CapwapSetAllCapwapBaseWtpProfileTableTrigger
                (pCapwapSetCapwapBaseWtpProfileEntry,
                 pCapwapIsSetCapwapBaseWtpProfileEntry,
                 SNMP_SUCCESS) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllCapwapBaseWtpProfileTable:  CapwapSetAllCapwapBaseWtpProfileTableTrigger function returns failure.\r\n");
            }
            MemReleaseMemBlock (CAPWAP_CAPWAPBASEWTPPROFILETABLE_POOLID,
                                (UINT1 *) pCapwapOldCapwapBaseWtpProfileEntry);
            MemReleaseMemBlock (CAPWAP_CAPWAPBASEWTPPROFILETABLE_POOLID,
                                (UINT1 *) pCapwapTrgCapwapBaseWtpProfileEntry);
            MemReleaseMemBlock (CAPWAP_CAPWAPBASEWTPPROFILETABLE_ISSET_POOLID,
                                (UINT1 *)
                                pCapwapTrgIsSetCapwapBaseWtpProfileEntry);
            return OSIX_SUCCESS;

        }
        else
        {
            if (CapwapSetAllCapwapBaseWtpProfileTableTrigger
                (pCapwapSetCapwapBaseWtpProfileEntry,
                 pCapwapIsSetCapwapBaseWtpProfileEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllCapwapBaseWtpProfileTable: CapwapSetAllCapwapBaseWtpProfileTableTrigger function returns failure.\r\n");
            }
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllCapwapBaseWtpProfileTable: Failure.\r\n");
            MemReleaseMemBlock (CAPWAP_CAPWAPBASEWTPPROFILETABLE_POOLID,
                                (UINT1 *) pCapwapOldCapwapBaseWtpProfileEntry);
            MemReleaseMemBlock (CAPWAP_CAPWAPBASEWTPPROFILETABLE_POOLID,
                                (UINT1 *) pCapwapTrgCapwapBaseWtpProfileEntry);
            MemReleaseMemBlock (CAPWAP_CAPWAPBASEWTPPROFILETABLE_ISSET_POOLID,
                                (UINT1 *)
                                pCapwapTrgIsSetCapwapBaseWtpProfileEntry);
            return OSIX_FAILURE;
        }
    }
    else if ((pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
              i4CapwapBaseWtpProfileRowStatus == CREATE_AND_WAIT)
             || (pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                 i4CapwapBaseWtpProfileRowStatus == CREATE_AND_GO))
    {
        if (CapwapSetAllCapwapBaseWtpProfileTableTrigger
            (pCapwapSetCapwapBaseWtpProfileEntry,
             pCapwapIsSetCapwapBaseWtpProfileEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)

        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllCapwapBaseWtpProfileTable: CapwapSetAllCapwapBaseWtpProfileTableTrigger function returns failure.\r\n");
        }
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapSetAllCapwapBaseWtpProfileTable: The row is already present.\r\n");
        MemReleaseMemBlock (CAPWAP_CAPWAPBASEWTPPROFILETABLE_POOLID,
                            (UINT1 *) pCapwapOldCapwapBaseWtpProfileEntry);
        MemReleaseMemBlock (CAPWAP_CAPWAPBASEWTPPROFILETABLE_POOLID,
                            (UINT1 *) pCapwapTrgCapwapBaseWtpProfileEntry);
        MemReleaseMemBlock (CAPWAP_CAPWAPBASEWTPPROFILETABLE_ISSET_POOLID,
                            (UINT1 *) pCapwapTrgIsSetCapwapBaseWtpProfileEntry);
        return OSIX_FAILURE;
    }
    /* Copy the previous values before setting the new values */
    MEMCPY (pCapwapOldCapwapBaseWtpProfileEntry,
            pCapwapCapwapBaseWtpProfileEntry,
            sizeof (tCapwapCapwapBaseWtpProfileEntry));

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
        i4CapwapBaseWtpProfileRowStatus == DESTROY)
    {
        pCapwapCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileRowStatus = DESTROY;

        if (CapwapUtilUpdateCapwapBaseWtpProfileTable
            (pCapwapOldCapwapBaseWtpProfileEntry,
             pCapwapCapwapBaseWtpProfileEntry,
             pCapwapIsSetCapwapBaseWtpProfileEntry) != OSIX_SUCCESS)
        {

            if (CapwapSetAllCapwapBaseWtpProfileTableTrigger
                (pCapwapSetCapwapBaseWtpProfileEntry,
                 pCapwapIsSetCapwapBaseWtpProfileEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllCapwapBaseWtpProfileTable: CapwapSetAllCapwapBaseWtpProfileTableTrigger function returns failure.\r\n");
            }
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllCapwapBaseWtpProfileTable: CapwapUtilUpdateCapwapBaseWtpProfileTable function returns failure.\r\n");
        }
        RBTreeRem (gCapwapGlobals.CapwapGlbMib.CapwapBaseWtpProfileTable,
                   pCapwapCapwapBaseWtpProfileEntry);
        if (CapwapSetAllCapwapBaseWtpProfileTableTrigger
            (pCapwapSetCapwapBaseWtpProfileEntry,
             pCapwapIsSetCapwapBaseWtpProfileEntry,
             SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllCapwapBaseWtpProfileTable: CapwapSetAllCapwapBaseWtpProfileTableTrigger function returns failure.\r\n");
            MemReleaseMemBlock (CAPWAP_CAPWAPBASEWTPPROFILETABLE_POOLID,
                                (UINT1 *) pCapwapOldCapwapBaseWtpProfileEntry);
            MemReleaseMemBlock (CAPWAP_CAPWAPBASEWTPPROFILETABLE_POOLID,
                                (UINT1 *) pCapwapTrgCapwapBaseWtpProfileEntry);
            MemReleaseMemBlock (CAPWAP_CAPWAPBASEWTPPROFILETABLE_ISSET_POOLID,
                                (UINT1 *)
                                pCapwapTrgIsSetCapwapBaseWtpProfileEntry);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (CAPWAP_CAPWAPBASEWTPPROFILETABLE_POOLID,
                            (UINT1 *) pCapwapCapwapBaseWtpProfileEntry);
        MemReleaseMemBlock (CAPWAP_CAPWAPBASEWTPPROFILETABLE_POOLID,
                            (UINT1 *) pCapwapOldCapwapBaseWtpProfileEntry);
        MemReleaseMemBlock (CAPWAP_CAPWAPBASEWTPPROFILETABLE_POOLID,
                            (UINT1 *) pCapwapTrgCapwapBaseWtpProfileEntry);
        MemReleaseMemBlock (CAPWAP_CAPWAPBASEWTPPROFILETABLE_ISSET_POOLID,
                            (UINT1 *) pCapwapTrgIsSetCapwapBaseWtpProfileEntry);
        return OSIX_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (pCapwapCapwapBaseWtpProfileEntry->MibObject.
        u4CapwapBaseWtpProfileWtpStatisticsTimer != 0)
    {
        if (CapwapBaseWtpProfileTableFilterInputs
            (pCapwapCapwapBaseWtpProfileEntry,
             pCapwapSetCapwapBaseWtpProfileEntry,
             pCapwapIsSetCapwapBaseWtpProfileEntry) != OSIX_TRUE)
        {
            MemReleaseMemBlock (CAPWAP_CAPWAPBASEWTPPROFILETABLE_POOLID,
                                (UINT1 *) pCapwapOldCapwapBaseWtpProfileEntry);
            MemReleaseMemBlock (CAPWAP_CAPWAPBASEWTPPROFILETABLE_POOLID,
                                (UINT1 *) pCapwapTrgCapwapBaseWtpProfileEntry);
            MemReleaseMemBlock (CAPWAP_CAPWAPBASEWTPPROFILETABLE_ISSET_POOLID,
                                (UINT1 *)
                                pCapwapTrgIsSetCapwapBaseWtpProfileEntry);
            return OSIX_SUCCESS;
        }
    }
    /*This condtion is to make the row NOT_IN_SERVICE before modifying the values */
    if ((i4RowStatusLogic == TRUE) &&
        (pCapwapCapwapBaseWtpProfileEntry->MibObject.
         i4CapwapBaseWtpProfileRowStatus == ACTIVE)
        && (pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileRowStatus != NOT_IN_SERVICE))
    {
        pCapwapCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileRowStatus = NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        pCapwapTrgCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileRowStatus = NOT_IN_SERVICE;
        pCapwapTrgIsSetCapwapBaseWtpProfileEntry->
            bCapwapBaseWtpProfileRowStatus = OSIX_TRUE;

        if (CapwapUtilUpdateCapwapBaseWtpProfileTable
            (pCapwapOldCapwapBaseWtpProfileEntry,
             pCapwapCapwapBaseWtpProfileEntry,
             pCapwapIsSetCapwapBaseWtpProfileEntry) != OSIX_SUCCESS)
        {
            /*Restore back with previous values */
            MEMCPY (pCapwapCapwapBaseWtpProfileEntry,
                    pCapwapOldCapwapBaseWtpProfileEntry,
                    sizeof (tCapwapCapwapBaseWtpProfileEntry));
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllCapwapBaseWtpProfileTable:                 CapwapUtilUpdateCapwapBaseWtpProfileTable Function returns failure.\r\n");

            if (CapwapSetAllCapwapBaseWtpProfileTableTrigger
                (pCapwapSetCapwapBaseWtpProfileEntry,
                 pCapwapIsSetCapwapBaseWtpProfileEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllCapwapBaseWtpProfileTable: CapwapSetAllCapwapBaseWtpProfileTableTrigger function returns failure.\r\n");
            }
            MemReleaseMemBlock (CAPWAP_CAPWAPBASEWTPPROFILETABLE_POOLID,
                                (UINT1 *) pCapwapOldCapwapBaseWtpProfileEntry);
            MemReleaseMemBlock (CAPWAP_CAPWAPBASEWTPPROFILETABLE_POOLID,
                                (UINT1 *) pCapwapTrgCapwapBaseWtpProfileEntry);
            MemReleaseMemBlock (CAPWAP_CAPWAPBASEWTPPROFILETABLE_ISSET_POOLID,
                                (UINT1 *)
                                pCapwapTrgIsSetCapwapBaseWtpProfileEntry);
            return OSIX_FAILURE;
        }
        if (CapwapSetAllCapwapBaseWtpProfileTableTrigger
            (pCapwapTrgCapwapBaseWtpProfileEntry,
             pCapwapTrgIsSetCapwapBaseWtpProfileEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllCapwapBaseWtpProfileTable: CapwapSetAllCapwapBaseWtpProfileTableTrigger function returns failure.\r\n");
        }
    }

    if (pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
        i4CapwapBaseWtpProfileRowStatus == ACTIVE)
    {
        i4RowMakeActive = TRUE;
    }

    /* Assign values for the existing node */
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->bCapwapBaseWtpProfileName !=
        OSIX_FALSE)
    {
        MEMCPY (pCapwapCapwapBaseWtpProfileEntry->MibObject.
                au1CapwapBaseWtpProfileName,
                pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                au1CapwapBaseWtpProfileName,
                pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                i4CapwapBaseWtpProfileNameLen);

        pCapwapCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileNameLen =
            pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileNameLen;
    }
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpMacAddress != OSIX_FALSE)
    {
        MEMCPY (pCapwapCapwapBaseWtpProfileEntry->MibObject.
                au1CapwapBaseWtpProfileWtpMacAddress,
                pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                au1CapwapBaseWtpProfileWtpMacAddress,
                pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                i4CapwapBaseWtpProfileWtpMacAddressLen);

        pCapwapCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileWtpMacAddressLen =
            pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileWtpMacAddressLen;
    }
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpModelNumber != OSIX_FALSE)
    {
        MEMCPY (pCapwapCapwapBaseWtpProfileEntry->MibObject.
                au1CapwapBaseWtpProfileWtpModelNumber,
                pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                au1CapwapBaseWtpProfileWtpModelNumber,
                pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                i4CapwapBaseWtpProfileWtpModelNumberLen);

        pCapwapCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileWtpModelNumberLen =
            pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileWtpModelNumberLen;
    }
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->bCapwapBaseWtpProfileWtpName !=
        OSIX_FALSE)
    {
        MEMCPY (pCapwapCapwapBaseWtpProfileEntry->MibObject.
                au1CapwapBaseWtpProfileWtpName,
                pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                au1CapwapBaseWtpProfileWtpName,
                pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                i4CapwapBaseWtpProfileWtpNameLen);

        pCapwapCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileWtpNameLen =
            pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileWtpNameLen;
    }
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpLocation != OSIX_FALSE)
    {
        MEMCPY (pCapwapCapwapBaseWtpProfileEntry->MibObject.
                au1CapwapBaseWtpProfileWtpLocation,
                pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                au1CapwapBaseWtpProfileWtpLocation,
                pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                i4CapwapBaseWtpProfileWtpLocationLen);

        pCapwapCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileWtpLocationLen =
            pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileWtpLocationLen;
    }
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpStaticIpEnable != OSIX_FALSE)
    {
        pCapwapCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileWtpStaticIpEnable =
            pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileWtpStaticIpEnable;
    }
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bFsCapwapFsAcTimestampTrigger != OSIX_FALSE)
    {
        pCapwapCapwapBaseWtpProfileEntry->MibObject.
            i4FsCapwapFsAcTimestampTrigger =
            pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
            i4FsCapwapFsAcTimestampTrigger;
    }

    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpStaticIpType != OSIX_FALSE)
    {
        pCapwapCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileWtpStaticIpType =
            pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileWtpStaticIpType;
    }
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpStaticIpAddress != OSIX_FALSE)
    {
        MEMCPY (pCapwapCapwapBaseWtpProfileEntry->MibObject.
                au1CapwapBaseWtpProfileWtpStaticIpAddress,
                pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                au1CapwapBaseWtpProfileWtpStaticIpAddress,
                pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                i4CapwapBaseWtpProfileWtpStaticIpAddressLen);

        pCapwapCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileWtpStaticIpAddressLen =
            pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileWtpStaticIpAddressLen;
    }
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpNetmask != OSIX_FALSE)
    {
        MEMCPY (pCapwapCapwapBaseWtpProfileEntry->MibObject.
                au1CapwapBaseWtpProfileWtpNetmask,
                pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                au1CapwapBaseWtpProfileWtpNetmask,
                pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                i4CapwapBaseWtpProfileWtpNetmaskLen);

        pCapwapCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileWtpNetmaskLen =
            pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileWtpNetmaskLen;
    }
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpGateway != OSIX_FALSE)
    {
        MEMCPY (pCapwapCapwapBaseWtpProfileEntry->MibObject.
                au1CapwapBaseWtpProfileWtpGateway,
                pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                au1CapwapBaseWtpProfileWtpGateway,
                pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                i4CapwapBaseWtpProfileWtpGatewayLen);

        pCapwapCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileWtpGatewayLen =
            pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileWtpGatewayLen;
    }
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpFallbackEnable != OSIX_FALSE)
    {
        pCapwapCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileWtpFallbackEnable =
            pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileWtpFallbackEnable;
    }
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpEchoInterval != OSIX_FALSE)
    {
        pCapwapCapwapBaseWtpProfileEntry->MibObject.
            u4CapwapBaseWtpProfileWtpEchoInterval =
            pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
            u4CapwapBaseWtpProfileWtpEchoInterval;
    }
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpIdleTimeout != OSIX_FALSE)
    {
        pCapwapCapwapBaseWtpProfileEntry->MibObject.
            u4CapwapBaseWtpProfileWtpIdleTimeout =
            pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
            u4CapwapBaseWtpProfileWtpIdleTimeout;
    }
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpMaxDiscoveryInterval != OSIX_FALSE)
    {
        pCapwapCapwapBaseWtpProfileEntry->MibObject.
            u4CapwapBaseWtpProfileWtpMaxDiscoveryInterval =
            pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
            u4CapwapBaseWtpProfileWtpMaxDiscoveryInterval;
    }
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpReportInterval != OSIX_FALSE)
    {
        pCapwapCapwapBaseWtpProfileEntry->MibObject.
            u4CapwapBaseWtpProfileWtpReportInterval =
            pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
            u4CapwapBaseWtpProfileWtpReportInterval;
    }
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpStatisticsTimer != OSIX_FALSE)
    {
        pCapwapCapwapBaseWtpProfileEntry->MibObject.
            u4CapwapBaseWtpProfileWtpStatisticsTimer =
            pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
            u4CapwapBaseWtpProfileWtpStatisticsTimer;
    }
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpEcnSupport != OSIX_FALSE)
    {
        pCapwapCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileWtpEcnSupport =
            pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileWtpEcnSupport;
    }
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->bCapwapBaseWtpProfileRowStatus !=
        OSIX_FALSE)
    {
        pCapwapCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileRowStatus =
            pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileRowStatus;
    }

    pCapwapCapwapBaseWtpProfileEntry->u4SystemUpTime =
        pCapwapSetCapwapBaseWtpProfileEntry->u4SystemUpTime;

    pCapwapCapwapBaseWtpProfileEntry->u4RadioIndex =
        pCapwapSetCapwapBaseWtpProfileEntry->u4RadioIndex;
    /*This condtion is to make the row back to ACTIVE after modifying the values */
    if (i4RowMakeActive == TRUE)
    {
        pCapwapCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileRowStatus = ACTIVE;
    }

    if (CapwapUtilUpdateCapwapBaseWtpProfileTable
        (pCapwapOldCapwapBaseWtpProfileEntry, pCapwapCapwapBaseWtpProfileEntry,
         pCapwapIsSetCapwapBaseWtpProfileEntry) != OSIX_SUCCESS)
    {

        if (CapwapSetAllCapwapBaseWtpProfileTableTrigger
            (pCapwapSetCapwapBaseWtpProfileEntry,
             pCapwapIsSetCapwapBaseWtpProfileEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)

        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllCapwapBaseWtpProfileTable: CapwapSetAllCapwapBaseWtpProfileTableTrigger function returns failure.\r\n");

        }
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapSetAllCapwapBaseWtpProfileTable: CapwapUtilUpdateCapwapBaseWtpProfileTable function returns failure.\r\n");
        /*Restore back with previous values */
        MEMCPY (pCapwapCapwapBaseWtpProfileEntry,
                pCapwapOldCapwapBaseWtpProfileEntry,
                sizeof (tCapwapCapwapBaseWtpProfileEntry));
        MemReleaseMemBlock (CAPWAP_CAPWAPBASEWTPPROFILETABLE_POOLID,
                            (UINT1 *) pCapwapOldCapwapBaseWtpProfileEntry);
        MemReleaseMemBlock (CAPWAP_CAPWAPBASEWTPPROFILETABLE_POOLID,
                            (UINT1 *) pCapwapTrgCapwapBaseWtpProfileEntry);
        MemReleaseMemBlock (CAPWAP_CAPWAPBASEWTPPROFILETABLE_ISSET_POOLID,
                            (UINT1 *) pCapwapTrgIsSetCapwapBaseWtpProfileEntry);
        return OSIX_FAILURE;

    }
    if (CapwapSetAllCapwapBaseWtpProfileTableTrigger
        (pCapwapSetCapwapBaseWtpProfileEntry,
         pCapwapIsSetCapwapBaseWtpProfileEntry, SNMP_SUCCESS) != OSIX_SUCCESS)
    {

        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapSetAllCapwapBaseWtpProfileTable: CapwapSetAllCapwapBaseWtpProfileTableTrigger function returns failure.\r\n");
    }

    MemReleaseMemBlock (CAPWAP_CAPWAPBASEWTPPROFILETABLE_POOLID,
                        (UINT1 *) pCapwapOldCapwapBaseWtpProfileEntry);
    MemReleaseMemBlock (CAPWAP_CAPWAPBASEWTPPROFILETABLE_POOLID,
                        (UINT1 *) pCapwapTrgCapwapBaseWtpProfileEntry);
    MemReleaseMemBlock (CAPWAP_CAPWAPBASEWTPPROFILETABLE_ISSET_POOLID,
                        (UINT1 *) pCapwapTrgIsSetCapwapBaseWtpProfileEntry);
    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  CapwapSetAllFsWtpModelTable
 Input       :  pCapwapSetFsWtpModelEntry
                pCapwapIsSetFsWtpModelEntry
                i4RowStatusLogic
                i4RowCreateOption
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapSetAllFsWtpModelTable (tCapwapFsWtpModelEntry * pCapwapSetFsWtpModelEntry,
                             tCapwapIsSetFsWtpModelEntry *
                             pCapwapIsSetFsWtpModelEntry, INT4 i4RowStatusLogic,
                             INT4 i4RowCreateOption)
{
    tCapwapFsWtpModelEntry *pCapwapFsWtpModelEntry = NULL;
    tCapwapFsWtpModelEntry *pCapwapOldFsWtpModelEntry = NULL;
    tCapwapFsWtpModelEntry *pCapwapTrgFsWtpModelEntry = NULL;
    tCapwapIsSetFsWtpModelEntry *pCapwapTrgIsSetFsWtpModelEntry = NULL;
    INT4                i4RowMakeActive = FALSE;

    pCapwapOldFsWtpModelEntry =
        (tCapwapFsWtpModelEntry *)
        MemAllocMemBlk (CAPWAP_FSWTPMODELTABLE_POOLID);
    if (pCapwapOldFsWtpModelEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    pCapwapTrgFsWtpModelEntry =
        (tCapwapFsWtpModelEntry *)
        MemAllocMemBlk (CAPWAP_FSWTPMODELTABLE_POOLID);
    if (pCapwapTrgFsWtpModelEntry == NULL)
    {
        MemReleaseMemBlock (CAPWAP_FSWTPMODELTABLE_POOLID,
                            (UINT1 *) pCapwapOldFsWtpModelEntry);
        return OSIX_FAILURE;
    }
    pCapwapTrgIsSetFsWtpModelEntry =
        (tCapwapIsSetFsWtpModelEntry *)
        MemAllocMemBlk (CAPWAP_FSWTPMODELTABLE_ISSET_POOLID);
    if (pCapwapTrgIsSetFsWtpModelEntry == NULL)
    {
        MemReleaseMemBlock (CAPWAP_FSWTPMODELTABLE_POOLID,
                            (UINT1 *) pCapwapOldFsWtpModelEntry);
        MemReleaseMemBlock (CAPWAP_FSWTPMODELTABLE_POOLID,
                            (UINT1 *) pCapwapTrgFsWtpModelEntry);
        return OSIX_FAILURE;
    }
    MEMSET (pCapwapOldFsWtpModelEntry, 0, sizeof (tCapwapFsWtpModelEntry));
    MEMSET (pCapwapTrgFsWtpModelEntry, 0, sizeof (tCapwapFsWtpModelEntry));
    MEMSET (pCapwapTrgIsSetFsWtpModelEntry, 0,
            sizeof (tCapwapIsSetFsWtpModelEntry));

    /* Check whether the node is already present */
    pCapwapFsWtpModelEntry =
        RBTreeGet (gCapwapGlobals.CapwapGlbMib.FsWtpModelTable,
                   (tRBElem *) pCapwapSetFsWtpModelEntry);

    if (pCapwapFsWtpModelEntry == NULL)
    {
        /* Create the node if the RowStatus given is CREATE_AND_WAIT or CREATE_AND_GO */
        if ((pCapwapSetFsWtpModelEntry->MibObject.i4FsWtpModelRowStatus ==
             CREATE_AND_WAIT)
            || (pCapwapSetFsWtpModelEntry->MibObject.i4FsWtpModelRowStatus ==
                CREATE_AND_GO)
            ||
            ((pCapwapSetFsWtpModelEntry->MibObject.i4FsWtpModelRowStatus ==
              ACTIVE) && (i4RowCreateOption == 1)))
        {
            /* Allocate memory for the new node */
            pCapwapFsWtpModelEntry =
                (tCapwapFsWtpModelEntry *)
                MemAllocMemBlk (CAPWAP_FSWTPMODELTABLE_POOLID);
            if (pCapwapFsWtpModelEntry == NULL)
            {
                if (CapwapSetAllFsWtpModelTableTrigger
                    (pCapwapSetFsWtpModelEntry, pCapwapIsSetFsWtpModelEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsWtpModelTable:"
                                "CapwapSetAllFsWtpModelTableTrigger function fails\r\n");

                }
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsWtpModelTable:"
                            "Fail to Allocate Memory.\r\n");
                MemReleaseMemBlock (CAPWAP_FSWTPMODELTABLE_POOLID,
                                    (UINT1 *) pCapwapOldFsWtpModelEntry);
                MemReleaseMemBlock (CAPWAP_FSWTPMODELTABLE_POOLID,
                                    (UINT1 *) pCapwapTrgFsWtpModelEntry);
                MemReleaseMemBlock (CAPWAP_FSWTPMODELTABLE_ISSET_POOLID,
                                    (UINT1 *) pCapwapTrgIsSetFsWtpModelEntry);
                return OSIX_FAILURE;
            }

            MEMSET (pCapwapFsWtpModelEntry, 0, sizeof (tCapwapFsWtpModelEntry));
            if ((CapwapInitializeFsWtpModelTable (pCapwapFsWtpModelEntry)) ==
                OSIX_FAILURE)
            {
                if (CapwapSetAllFsWtpModelTableTrigger
                    (pCapwapSetFsWtpModelEntry, pCapwapIsSetFsWtpModelEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsWtpModelTable:"
                                "CapwapSetAllFsWtpModelTableTrigger function fails\r\n");

                }
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsWtpModelTable:"
                            " Fail to Initialize the Objects.\r\n");

                MemReleaseMemBlock (CAPWAP_FSWTPMODELTABLE_POOLID,
                                    (UINT1 *) pCapwapFsWtpModelEntry);
                MemReleaseMemBlock (CAPWAP_FSWTPMODELTABLE_POOLID,
                                    (UINT1 *) pCapwapOldFsWtpModelEntry);
                MemReleaseMemBlock (CAPWAP_FSWTPMODELTABLE_POOLID,
                                    (UINT1 *) pCapwapTrgFsWtpModelEntry);
                MemReleaseMemBlock (CAPWAP_FSWTPMODELTABLE_ISSET_POOLID,
                                    (UINT1 *) pCapwapTrgIsSetFsWtpModelEntry);
                return OSIX_FAILURE;
            }
            /* Assign values for the new node */
            if (pCapwapIsSetFsWtpModelEntry->bFsCapwapWtpModelNumber !=
                OSIX_FALSE)
            {
                MEMCPY (pCapwapFsWtpModelEntry->MibObject.
                        au1FsCapwapWtpModelNumber,
                        pCapwapSetFsWtpModelEntry->MibObject.
                        au1FsCapwapWtpModelNumber,
                        pCapwapSetFsWtpModelEntry->MibObject.
                        i4FsCapwapWtpModelNumberLen);

                pCapwapFsWtpModelEntry->MibObject.i4FsCapwapWtpModelNumberLen =
                    pCapwapSetFsWtpModelEntry->MibObject.
                    i4FsCapwapWtpModelNumberLen;
            }

            if (pCapwapIsSetFsWtpModelEntry->bFsNoOfRadio != OSIX_FALSE)
            {
                pCapwapFsWtpModelEntry->MibObject.u4FsNoOfRadio =
                    pCapwapSetFsWtpModelEntry->MibObject.u4FsNoOfRadio;
            }

            if (pCapwapIsSetFsWtpModelEntry->bFsCapwapWtpMacType != OSIX_FALSE)
            {
                pCapwapFsWtpModelEntry->MibObject.i4FsCapwapWtpMacType =
                    pCapwapSetFsWtpModelEntry->MibObject.i4FsCapwapWtpMacType;
            }

            if (pCapwapIsSetFsWtpModelEntry->bFsCapwapWtpTunnelMode !=
                OSIX_FALSE)
            {
                MEMCPY (pCapwapFsWtpModelEntry->MibObject.
                        au1FsCapwapWtpTunnelMode,
                        pCapwapSetFsWtpModelEntry->MibObject.
                        au1FsCapwapWtpTunnelMode,
                        pCapwapSetFsWtpModelEntry->MibObject.
                        i4FsCapwapWtpTunnelModeLen);

                pCapwapFsWtpModelEntry->MibObject.i4FsCapwapWtpTunnelModeLen =
                    pCapwapSetFsWtpModelEntry->MibObject.
                    i4FsCapwapWtpTunnelModeLen;
            }

            if (pCapwapIsSetFsWtpModelEntry->bFsCapwapImageName != OSIX_FALSE)
            {
                MEMCPY (pCapwapFsWtpModelEntry->MibObject.au1FsCapwapImageName,
                        pCapwapSetFsWtpModelEntry->MibObject.
                        au1FsCapwapImageName,
                        pCapwapSetFsWtpModelEntry->MibObject.
                        i4FsCapwapImageNameLen);

                pCapwapFsWtpModelEntry->MibObject.i4FsCapwapImageNameLen =
                    pCapwapSetFsWtpModelEntry->MibObject.i4FsCapwapImageNameLen;
            }

            if (pCapwapIsSetFsWtpModelEntry->bFsCapwapQosProfileName !=
                OSIX_FALSE)
            {
                MEMCPY (pCapwapFsWtpModelEntry->MibObject.
                        au1FsCapwapQosProfileName,
                        pCapwapSetFsWtpModelEntry->MibObject.
                        au1FsCapwapQosProfileName,
                        pCapwapSetFsWtpModelEntry->MibObject.
                        i4FsCapwapQosProfileNameLen);

                pCapwapFsWtpModelEntry->MibObject.i4FsCapwapQosProfileNameLen =
                    pCapwapSetFsWtpModelEntry->MibObject.
                    i4FsCapwapQosProfileNameLen;
            }

            if (pCapwapIsSetFsWtpModelEntry->bFsMaxStations != OSIX_FALSE)
            {
                pCapwapFsWtpModelEntry->MibObject.i4FsMaxStations =
                    pCapwapSetFsWtpModelEntry->MibObject.i4FsMaxStations;
            }

            if (pCapwapIsSetFsWtpModelEntry->bFsWtpModelRowStatus != OSIX_FALSE)
            {
                pCapwapFsWtpModelEntry->MibObject.i4FsWtpModelRowStatus =
                    pCapwapSetFsWtpModelEntry->MibObject.i4FsWtpModelRowStatus;
            }

            MEMCPY (pCapwapFsWtpModelEntry->au1LastUpdated,
                    pCapwapSetFsWtpModelEntry->au1LastUpdated,
                    pCapwapSetFsWtpModelEntry->i4LastUpdatedLen);
            pCapwapFsWtpModelEntry->i4LastUpdatedLen =
                pCapwapSetFsWtpModelEntry->i4LastUpdatedLen;

            if ((pCapwapSetFsWtpModelEntry->MibObject.i4FsWtpModelRowStatus ==
                 CREATE_AND_GO) || ((i4RowCreateOption == 1)
                                    && (pCapwapSetFsWtpModelEntry->MibObject.
                                        i4FsWtpModelRowStatus == ACTIVE)))
            {
                pCapwapFsWtpModelEntry->MibObject.i4FsWtpModelRowStatus =
                    ACTIVE;
            }
            else if (pCapwapSetFsWtpModelEntry->MibObject.
                     i4FsWtpModelRowStatus == CREATE_AND_WAIT)
            {
                pCapwapFsWtpModelEntry->MibObject.i4FsWtpModelRowStatus =
                    NOT_READY;
            }

            /* Add the new node to the database */
            if (RBTreeAdd
                (gCapwapGlobals.CapwapGlbMib.FsWtpModelTable,
                 (tRBElem *) pCapwapFsWtpModelEntry) != RB_SUCCESS)
            {
                if (CapwapSetAllFsWtpModelTableTrigger
                    (pCapwapSetFsWtpModelEntry, pCapwapIsSetFsWtpModelEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsWtpModelTable:"
                                " CapwapSetAllFsWtpModelTableTrigger function returns failure.\r\n");
                }
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsWtpModelTable: RBTreeAdd is failed.\r\n");

                MemReleaseMemBlock (CAPWAP_FSWTPMODELTABLE_POOLID,
                                    (UINT1 *) pCapwapFsWtpModelEntry);
                MemReleaseMemBlock (CAPWAP_FSWTPMODELTABLE_POOLID,
                                    (UINT1 *) pCapwapOldFsWtpModelEntry);
                MemReleaseMemBlock (CAPWAP_FSWTPMODELTABLE_POOLID,
                                    (UINT1 *) pCapwapTrgFsWtpModelEntry);
                MemReleaseMemBlock (CAPWAP_FSWTPMODELTABLE_ISSET_POOLID,
                                    (UINT1 *) pCapwapTrgIsSetFsWtpModelEntry);
                return OSIX_FAILURE;
            }
            if (CapwapUtilUpdateFsWtpModelTable
                (NULL, pCapwapFsWtpModelEntry,
                 pCapwapIsSetFsWtpModelEntry) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsWtpModelTable: CapwapUtilUpdateFsWtpModelTable function returns failure.\r\n");

                if (CapwapSetAllFsWtpModelTableTrigger
                    (pCapwapSetFsWtpModelEntry, pCapwapIsSetFsWtpModelEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsWtpModelTable:"
                                " CapwapSetAllFsWtpModelTableTrigger function returns failure.\r\n");

                }
                RBTreeRem (gCapwapGlobals.CapwapGlbMib.FsWtpModelTable,
                           pCapwapFsWtpModelEntry);
                MemReleaseMemBlock (CAPWAP_FSWTPMODELTABLE_POOLID,
                                    (UINT1 *) pCapwapFsWtpModelEntry);
                MemReleaseMemBlock (CAPWAP_FSWTPMODELTABLE_POOLID,
                                    (UINT1 *) pCapwapOldFsWtpModelEntry);
                MemReleaseMemBlock (CAPWAP_FSWTPMODELTABLE_POOLID,
                                    (UINT1 *) pCapwapTrgFsWtpModelEntry);
                MemReleaseMemBlock (CAPWAP_FSWTPMODELTABLE_ISSET_POOLID,
                                    (UINT1 *) pCapwapTrgIsSetFsWtpModelEntry);
                return OSIX_FAILURE;
            }

            if ((pCapwapSetFsWtpModelEntry->MibObject.i4FsWtpModelRowStatus ==
                 CREATE_AND_GO) || ((i4RowCreateOption == 1)
                                    && (pCapwapSetFsWtpModelEntry->MibObject.
                                        i4FsWtpModelRowStatus == ACTIVE)))
            {

                /* For MSR and RM Trigger */
                MEMCPY (pCapwapTrgFsWtpModelEntry->MibObject.
                        au1FsCapwapWtpModelNumber,
                        pCapwapSetFsWtpModelEntry->MibObject.
                        au1FsCapwapWtpModelNumber,
                        pCapwapSetFsWtpModelEntry->MibObject.
                        i4FsCapwapWtpModelNumberLen);

                pCapwapTrgFsWtpModelEntry->MibObject.
                    i4FsCapwapWtpModelNumberLen =
                    pCapwapSetFsWtpModelEntry->MibObject.
                    i4FsCapwapWtpModelNumberLen;
                pCapwapTrgFsWtpModelEntry->MibObject.i4FsWtpModelRowStatus =
                    CREATE_AND_WAIT;
                pCapwapTrgIsSetFsWtpModelEntry->bFsWtpModelRowStatus =
                    OSIX_TRUE;

                if (CapwapSetAllFsWtpModelTableTrigger
                    (pCapwapTrgFsWtpModelEntry, pCapwapTrgIsSetFsWtpModelEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsWtpModelTable:"
                                " CapwapSetAllFsWtpModelTableTrigger function returns failure.\r\n");

                    MemReleaseMemBlock (CAPWAP_FSWTPMODELTABLE_POOLID,
                                        (UINT1 *) pCapwapFsWtpModelEntry);
                    MemReleaseMemBlock (CAPWAP_FSWTPMODELTABLE_POOLID,
                                        (UINT1 *) pCapwapOldFsWtpModelEntry);
                    MemReleaseMemBlock (CAPWAP_FSWTPMODELTABLE_POOLID,
                                        (UINT1 *) pCapwapTrgFsWtpModelEntry);
                    MemReleaseMemBlock (CAPWAP_FSWTPMODELTABLE_ISSET_POOLID,
                                        (UINT1 *)
                                        pCapwapTrgIsSetFsWtpModelEntry);
                    return OSIX_FAILURE;
                }
            }
            else if (pCapwapSetFsWtpModelEntry->MibObject.
                     i4FsWtpModelRowStatus == CREATE_AND_WAIT)
            {

                /* For MSR and RM Trigger */
                pCapwapTrgFsWtpModelEntry->MibObject.i4FsWtpModelRowStatus =
                    CREATE_AND_WAIT;
                pCapwapTrgIsSetFsWtpModelEntry->bFsWtpModelRowStatus =
                    OSIX_TRUE;

                if (CapwapSetAllFsWtpModelTableTrigger
                    (pCapwapTrgFsWtpModelEntry, pCapwapTrgIsSetFsWtpModelEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsWtpModelTable:"
                                " CapwapSetAllFsWtpModelTableTrigger function returns failure.\r\n");

                    MemReleaseMemBlock (CAPWAP_FSWTPMODELTABLE_POOLID,
                                        (UINT1 *) pCapwapFsWtpModelEntry);
                    MemReleaseMemBlock (CAPWAP_FSWTPMODELTABLE_POOLID,
                                        (UINT1 *) pCapwapOldFsWtpModelEntry);
                    MemReleaseMemBlock (CAPWAP_FSWTPMODELTABLE_POOLID,
                                        (UINT1 *) pCapwapTrgFsWtpModelEntry);
                    MemReleaseMemBlock (CAPWAP_FSWTPMODELTABLE_ISSET_POOLID,
                                        (UINT1 *)
                                        pCapwapTrgIsSetFsWtpModelEntry);
                    return OSIX_FAILURE;
                }
            }

            if (pCapwapSetFsWtpModelEntry->MibObject.i4FsWtpModelRowStatus ==
                CREATE_AND_GO)
            {
                pCapwapSetFsWtpModelEntry->MibObject.i4FsWtpModelRowStatus =
                    ACTIVE;
            }

            if (CapwapSetAllFsWtpModelTableTrigger (pCapwapSetFsWtpModelEntry,
                                                    pCapwapIsSetFsWtpModelEntry,
                                                    SNMP_SUCCESS) !=
                OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsWtpModelTable:"
                            "CapwapSetAllFsWtpModelTableTrigger function returns failure.\r\n");
            }
            MemReleaseMemBlock (CAPWAP_FSWTPMODELTABLE_POOLID,
                                (UINT1 *) pCapwapOldFsWtpModelEntry);
            MemReleaseMemBlock (CAPWAP_FSWTPMODELTABLE_POOLID,
                                (UINT1 *) pCapwapTrgFsWtpModelEntry);
            MemReleaseMemBlock (CAPWAP_FSWTPMODELTABLE_ISSET_POOLID,
                                (UINT1 *) pCapwapTrgIsSetFsWtpModelEntry);
            return OSIX_SUCCESS;

        }
        else
        {
            if (CapwapSetAllFsWtpModelTableTrigger (pCapwapSetFsWtpModelEntry,
                                                    pCapwapIsSetFsWtpModelEntry,
                                                    SNMP_FAILURE) !=
                OSIX_SUCCESS)

            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsWtpModelTable:"
                            "CapwapSetAllFsWtpModelTableTrigger function returns failure.\r\n");
            }
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsWtpModelTable: Failure.\r\n");
            MemReleaseMemBlock (CAPWAP_FSWTPMODELTABLE_POOLID,
                                (UINT1 *) pCapwapOldFsWtpModelEntry);
            MemReleaseMemBlock (CAPWAP_FSWTPMODELTABLE_POOLID,
                                (UINT1 *) pCapwapTrgFsWtpModelEntry);
            MemReleaseMemBlock (CAPWAP_FSWTPMODELTABLE_ISSET_POOLID,
                                (UINT1 *) pCapwapTrgIsSetFsWtpModelEntry);
            return OSIX_FAILURE;
        }
    }
    else if ((pCapwapSetFsWtpModelEntry->MibObject.i4FsWtpModelRowStatus ==
              CREATE_AND_WAIT)
             || (pCapwapSetFsWtpModelEntry->MibObject.i4FsWtpModelRowStatus ==
                 CREATE_AND_GO))
    {
        if (CapwapSetAllFsWtpModelTableTrigger (pCapwapSetFsWtpModelEntry,
                                                pCapwapIsSetFsWtpModelEntry,
                                                SNMP_FAILURE) != OSIX_SUCCESS)

        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsWtpModelTable:"
                        " CapwapSetAllFsWtpModelTableTrigger function returns failure.\r\n");
        }
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapSetAllFsWtpModelTable: The row is already present.\r\n");
        MemReleaseMemBlock (CAPWAP_FSWTPMODELTABLE_POOLID,
                            (UINT1 *) pCapwapOldFsWtpModelEntry);
        MemReleaseMemBlock (CAPWAP_FSWTPMODELTABLE_POOLID,
                            (UINT1 *) pCapwapTrgFsWtpModelEntry);
        MemReleaseMemBlock (CAPWAP_FSWTPMODELTABLE_ISSET_POOLID,
                            (UINT1 *) pCapwapTrgIsSetFsWtpModelEntry);
        return OSIX_FAILURE;
    }
    /* Copy the previous values before setting the new values */
    MEMCPY (pCapwapOldFsWtpModelEntry, pCapwapFsWtpModelEntry,
            sizeof (tCapwapFsWtpModelEntry));

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pCapwapSetFsWtpModelEntry->MibObject.i4FsWtpModelRowStatus == DESTROY)
    {
        pCapwapFsWtpModelEntry->MibObject.i4FsWtpModelRowStatus = DESTROY;

        if (CapwapUtilUpdateFsWtpModelTable (pCapwapOldFsWtpModelEntry,
                                             pCapwapFsWtpModelEntry,
                                             pCapwapIsSetFsWtpModelEntry) !=
            OSIX_SUCCESS)
        {

            if (CapwapSetAllFsWtpModelTableTrigger (pCapwapSetFsWtpModelEntry,
                                                    pCapwapIsSetFsWtpModelEntry,
                                                    SNMP_FAILURE) !=
                OSIX_SUCCESS)

            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsWtpModelTable:"
                            "CapwapSetAllFsWtpModelTableTrigger function returns failure.\r\n");
            }
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsWtpModelTable: CapwapUtilUpdateFsWtpModelTable function returns failure.\r\n");
        }
        RBTreeRem (gCapwapGlobals.CapwapGlbMib.FsWtpModelTable,
                   pCapwapFsWtpModelEntry);
        if (CapwapSetAllFsWtpModelTableTrigger
            (pCapwapSetFsWtpModelEntry, pCapwapIsSetFsWtpModelEntry,
             SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsWtpModelTable:"
                        " CapwapSetAllFsWtpModelTableTrigger function returns failure.\r\n");
            MemReleaseMemBlock (CAPWAP_FSWTPMODELTABLE_POOLID,
                                (UINT1 *) pCapwapOldFsWtpModelEntry);
            MemReleaseMemBlock (CAPWAP_FSWTPMODELTABLE_POOLID,
                                (UINT1 *) pCapwapTrgFsWtpModelEntry);
            MemReleaseMemBlock (CAPWAP_FSWTPMODELTABLE_ISSET_POOLID,
                                (UINT1 *) pCapwapTrgIsSetFsWtpModelEntry);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (CAPWAP_FSWTPMODELTABLE_POOLID,
                            (UINT1 *) pCapwapFsWtpModelEntry);
        MemReleaseMemBlock (CAPWAP_FSWTPMODELTABLE_POOLID,
                            (UINT1 *) pCapwapOldFsWtpModelEntry);
        MemReleaseMemBlock (CAPWAP_FSWTPMODELTABLE_POOLID,
                            (UINT1 *) pCapwapTrgFsWtpModelEntry);
        MemReleaseMemBlock (CAPWAP_FSWTPMODELTABLE_ISSET_POOLID,
                            (UINT1 *) pCapwapTrgIsSetFsWtpModelEntry);
        return OSIX_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (FsWtpModelTableFilterInputs
        (pCapwapFsWtpModelEntry, pCapwapSetFsWtpModelEntry,
         pCapwapIsSetFsWtpModelEntry) != OSIX_TRUE)
    {
        MemReleaseMemBlock (CAPWAP_FSWTPMODELTABLE_POOLID,
                            (UINT1 *) pCapwapOldFsWtpModelEntry);
        MemReleaseMemBlock (CAPWAP_FSWTPMODELTABLE_POOLID,
                            (UINT1 *) pCapwapTrgFsWtpModelEntry);
        MemReleaseMemBlock (CAPWAP_FSWTPMODELTABLE_ISSET_POOLID,
                            (UINT1 *) pCapwapTrgIsSetFsWtpModelEntry);
        return OSIX_SUCCESS;
    }

    /*This condtion is to make the row NOT_IN_SERVICE before modifying the values */
    if ((i4RowStatusLogic == TRUE) &&
        (pCapwapFsWtpModelEntry->MibObject.i4FsWtpModelRowStatus == ACTIVE) &&
        (pCapwapSetFsWtpModelEntry->MibObject.i4FsWtpModelRowStatus !=
         NOT_IN_SERVICE))
    {
        pCapwapFsWtpModelEntry->MibObject.i4FsWtpModelRowStatus =
            NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        pCapwapTrgFsWtpModelEntry->MibObject.i4FsWtpModelRowStatus =
            NOT_IN_SERVICE;
        pCapwapTrgIsSetFsWtpModelEntry->bFsWtpModelRowStatus = OSIX_TRUE;

        if (CapwapUtilUpdateFsWtpModelTable (pCapwapOldFsWtpModelEntry,
                                             pCapwapFsWtpModelEntry,
                                             pCapwapIsSetFsWtpModelEntry) !=
            OSIX_SUCCESS)
        {
            /*Restore back with previous values */
            MEMCPY (pCapwapFsWtpModelEntry, pCapwapOldFsWtpModelEntry,
                    sizeof (tCapwapFsWtpModelEntry));
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsWtpModelTable:"
                        " CapwapUtilUpdateFsWtpModelTable Function returns failure.\r\n");

            if (CapwapSetAllFsWtpModelTableTrigger (pCapwapSetFsWtpModelEntry,
                                                    pCapwapIsSetFsWtpModelEntry,
                                                    SNMP_FAILURE) !=
                OSIX_SUCCESS)

            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsWtpModelTable: "
                            "CapwapSetAllFsWtpModelTableTrigger function returns failure.\r\n");
            }
            MemReleaseMemBlock (CAPWAP_FSWTPMODELTABLE_POOLID,
                                (UINT1 *) pCapwapOldFsWtpModelEntry);
            MemReleaseMemBlock (CAPWAP_FSWTPMODELTABLE_POOLID,
                                (UINT1 *) pCapwapTrgFsWtpModelEntry);
            MemReleaseMemBlock (CAPWAP_FSWTPMODELTABLE_ISSET_POOLID,
                                (UINT1 *) pCapwapTrgIsSetFsWtpModelEntry);
            return OSIX_FAILURE;
        }

        if (CapwapSetAllFsWtpModelTableTrigger (pCapwapTrgFsWtpModelEntry,
                                                pCapwapTrgIsSetFsWtpModelEntry,
                                                SNMP_FAILURE) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsWtpModelTable:"
                        " CapwapSetAllFsWtpModelTableTrigger function returns failure.\r\n");
        }
    }

    if (pCapwapSetFsWtpModelEntry->MibObject.i4FsWtpModelRowStatus == ACTIVE)
    {
        i4RowMakeActive = TRUE;
    }

    /* Assign values for the existing node */
    if (pCapwapIsSetFsWtpModelEntry->bFsNoOfRadio != OSIX_FALSE)
    {
        pCapwapFsWtpModelEntry->MibObject.u4FsNoOfRadio =
            pCapwapSetFsWtpModelEntry->MibObject.u4FsNoOfRadio;
    }
    if (pCapwapIsSetFsWtpModelEntry->bFsCapwapWtpMacType != OSIX_FALSE)
    {
        pCapwapFsWtpModelEntry->MibObject.i4FsCapwapWtpMacType =
            pCapwapSetFsWtpModelEntry->MibObject.i4FsCapwapWtpMacType;
    }
    if (pCapwapIsSetFsWtpModelEntry->bFsCapwapWtpTunnelMode != OSIX_FALSE)
    {
        MEMCPY (pCapwapFsWtpModelEntry->MibObject.au1FsCapwapWtpTunnelMode,
                pCapwapSetFsWtpModelEntry->MibObject.au1FsCapwapWtpTunnelMode,
                pCapwapSetFsWtpModelEntry->MibObject.
                i4FsCapwapWtpTunnelModeLen);

        pCapwapFsWtpModelEntry->MibObject.i4FsCapwapWtpTunnelModeLen =
            pCapwapSetFsWtpModelEntry->MibObject.i4FsCapwapWtpTunnelModeLen;
    }
    if (pCapwapIsSetFsWtpModelEntry->bFsCapwapImageName != OSIX_FALSE)
    {
        MEMCPY (pCapwapFsWtpModelEntry->MibObject.au1FsCapwapImageName,
                pCapwapSetFsWtpModelEntry->MibObject.au1FsCapwapImageName,
                pCapwapSetFsWtpModelEntry->MibObject.i4FsCapwapImageNameLen);

        pCapwapFsWtpModelEntry->MibObject.i4FsCapwapImageNameLen =
            pCapwapSetFsWtpModelEntry->MibObject.i4FsCapwapImageNameLen;
    }
    if (pCapwapIsSetFsWtpModelEntry->bFsCapwapQosProfileName != OSIX_FALSE)
    {
        MEMCPY (pCapwapFsWtpModelEntry->MibObject.au1FsCapwapQosProfileName,
                pCapwapSetFsWtpModelEntry->MibObject.au1FsCapwapQosProfileName,
                pCapwapSetFsWtpModelEntry->MibObject.
                i4FsCapwapQosProfileNameLen);

        pCapwapFsWtpModelEntry->MibObject.i4FsCapwapQosProfileNameLen =
            pCapwapSetFsWtpModelEntry->MibObject.i4FsCapwapQosProfileNameLen;
    }
    if (pCapwapIsSetFsWtpModelEntry->bFsMaxStations != OSIX_FALSE)
    {
        pCapwapFsWtpModelEntry->MibObject.i4FsMaxStations =
            pCapwapSetFsWtpModelEntry->MibObject.i4FsMaxStations;
    }

    MEMCPY (pCapwapFsWtpModelEntry->au1LastUpdated,
            pCapwapSetFsWtpModelEntry->au1LastUpdated,
            pCapwapSetFsWtpModelEntry->i4LastUpdatedLen);
    pCapwapFsWtpModelEntry->i4LastUpdatedLen =
        pCapwapSetFsWtpModelEntry->i4LastUpdatedLen;

    if (pCapwapIsSetFsWtpModelEntry->bFsWtpModelRowStatus != OSIX_FALSE)
    {
        pCapwapFsWtpModelEntry->MibObject.i4FsWtpModelRowStatus =
            pCapwapSetFsWtpModelEntry->MibObject.i4FsWtpModelRowStatus;
    }

    /*This condtion is to make the row back to ACTIVE after modifying the values */
    if (i4RowMakeActive == TRUE)
    {
        pCapwapFsWtpModelEntry->MibObject.i4FsWtpModelRowStatus = ACTIVE;
    }

    if (CapwapUtilUpdateFsWtpModelTable (pCapwapOldFsWtpModelEntry,
                                         pCapwapFsWtpModelEntry,
                                         pCapwapIsSetFsWtpModelEntry) !=
        OSIX_SUCCESS)
    {

        if (CapwapSetAllFsWtpModelTableTrigger (pCapwapSetFsWtpModelEntry,
                                                pCapwapIsSetFsWtpModelEntry,
                                                SNMP_FAILURE) != OSIX_SUCCESS)

        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsWtpModelTable:"
                        " CapwapSetAllFsWtpModelTableTrigger function returns failure.\r\n");

        }
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapSetAllFsWtpModelTable: "
                    "CapwapUtilUpdateFsWtpModelTable function returns failure.\r\n");
        /*Restore back with previous values */
        MEMCPY (pCapwapFsWtpModelEntry, pCapwapOldFsWtpModelEntry,
                sizeof (tCapwapFsWtpModelEntry));
        MemReleaseMemBlock (CAPWAP_FSWTPMODELTABLE_POOLID,
                            (UINT1 *) pCapwapOldFsWtpModelEntry);
        MemReleaseMemBlock (CAPWAP_FSWTPMODELTABLE_POOLID,
                            (UINT1 *) pCapwapTrgFsWtpModelEntry);
        MemReleaseMemBlock (CAPWAP_FSWTPMODELTABLE_ISSET_POOLID,
                            (UINT1 *) pCapwapTrgIsSetFsWtpModelEntry);
        return OSIX_FAILURE;

    }
    if (CapwapSetAllFsWtpModelTableTrigger (pCapwapSetFsWtpModelEntry,
                                            pCapwapIsSetFsWtpModelEntry,
                                            SNMP_SUCCESS) != OSIX_SUCCESS)
    {

        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapSetAllFsWtpModelTable:"
                    "CapwapSetAllFsWtpModelTableTrigger function returns failure.\r\n");
    }

    MemReleaseMemBlock (CAPWAP_FSWTPMODELTABLE_POOLID,
                        (UINT1 *) pCapwapOldFsWtpModelEntry);
    MemReleaseMemBlock (CAPWAP_FSWTPMODELTABLE_POOLID,
                        (UINT1 *) pCapwapTrgFsWtpModelEntry);
    MemReleaseMemBlock (CAPWAP_FSWTPMODELTABLE_ISSET_POOLID,
                        (UINT1 *) pCapwapTrgIsSetFsWtpModelEntry);
    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  CapwapSetAllFsWtpRadioTable
 Input       :  pCapwapSetFsWtpRadioEntry
                pCapwapIsSetFsWtpRadioEntry
                i4RowStatusLogic
                i4RowCreateOption
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapSetAllFsWtpRadioTable (tCapwapFsWtpRadioEntry * pCapwapSetFsWtpRadioEntry,
                             tCapwapIsSetFsWtpRadioEntry *
                             pCapwapIsSetFsWtpRadioEntry, INT4 i4RowStatusLogic,
                             INT4 i4RowCreateOption)
{
    tCapwapFsWtpRadioEntry *pCapwapFsWtpRadioEntry = NULL;
    tCapwapFsWtpRadioEntry *pCapwapOldFsWtpRadioEntry = NULL;
    tCapwapFsWtpRadioEntry *pCapwapTrgFsWtpRadioEntry = NULL;
    tCapwapIsSetFsWtpRadioEntry *pCapwapTrgIsSetFsWtpRadioEntry = NULL;
    INT4                i4RowMakeActive = FALSE;

    pCapwapOldFsWtpRadioEntry =
        (tCapwapFsWtpRadioEntry *)
        MemAllocMemBlk (CAPWAP_FSWTPRADIOTABLE_POOLID);
    if (pCapwapOldFsWtpRadioEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    pCapwapTrgFsWtpRadioEntry =
        (tCapwapFsWtpRadioEntry *)
        MemAllocMemBlk (CAPWAP_FSWTPRADIOTABLE_POOLID);
    if (pCapwapTrgFsWtpRadioEntry == NULL)
    {
        MemReleaseMemBlock (CAPWAP_FSWTPRADIOTABLE_POOLID,
                            (UINT1 *) pCapwapOldFsWtpRadioEntry);
        return OSIX_FAILURE;
    }
    pCapwapTrgIsSetFsWtpRadioEntry =
        (tCapwapIsSetFsWtpRadioEntry *)
        MemAllocMemBlk (CAPWAP_FSWTPRADIOTABLE_ISSET_POOLID);
    if (pCapwapTrgIsSetFsWtpRadioEntry == NULL)
    {
        MemReleaseMemBlock (CAPWAP_FSWTPRADIOTABLE_POOLID,
                            (UINT1 *) pCapwapOldFsWtpRadioEntry);
        MemReleaseMemBlock (CAPWAP_FSWTPRADIOTABLE_POOLID,
                            (UINT1 *) pCapwapTrgFsWtpRadioEntry);
        return OSIX_FAILURE;
    }
    MEMSET (pCapwapOldFsWtpRadioEntry, 0, sizeof (tCapwapFsWtpRadioEntry));
    MEMSET (pCapwapTrgFsWtpRadioEntry, 0, sizeof (tCapwapFsWtpRadioEntry));
    MEMSET (pCapwapTrgIsSetFsWtpRadioEntry, 0,
            sizeof (tCapwapIsSetFsWtpRadioEntry));

    /* Check whether the node is already present */
    pCapwapFsWtpRadioEntry =
        RBTreeGet (gCapwapGlobals.CapwapGlbMib.FsWtpRadioTable,
                   (tRBElem *) pCapwapSetFsWtpRadioEntry);

    if (pCapwapFsWtpRadioEntry == NULL)
    {
        /* Create the node if the RowStatus given is CREATE_AND_WAIT or CREATE_AND_GO */
        if ((pCapwapSetFsWtpRadioEntry->MibObject.i4FsWtpRadioRowStatus ==
             CREATE_AND_WAIT)
            || (pCapwapSetFsWtpRadioEntry->MibObject.i4FsWtpRadioRowStatus ==
                CREATE_AND_GO)
            ||
            ((pCapwapSetFsWtpRadioEntry->MibObject.i4FsWtpRadioRowStatus ==
              ACTIVE) && (i4RowCreateOption == 1)))
        {
            /* Allocate memory for the new node */
            pCapwapFsWtpRadioEntry =
                (tCapwapFsWtpRadioEntry *)
                MemAllocMemBlk (CAPWAP_FSWTPRADIOTABLE_POOLID);
            if (pCapwapFsWtpRadioEntry == NULL)
            {
                if (CapwapSetAllFsWtpRadioTableTrigger
                    (pCapwapSetFsWtpRadioEntry, pCapwapIsSetFsWtpRadioEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsWtpRadioTable:CapwapSetAllFsWtpRadioTableTrigger function fails\r\n");

                }
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsWtpRadioTable: Fail to Allocate Memory.\r\n");
                MemReleaseMemBlock (CAPWAP_FSWTPRADIOTABLE_POOLID,
                                    (UINT1 *) pCapwapOldFsWtpRadioEntry);
                MemReleaseMemBlock (CAPWAP_FSWTPRADIOTABLE_POOLID,
                                    (UINT1 *) pCapwapTrgFsWtpRadioEntry);
                MemReleaseMemBlock (CAPWAP_FSWTPRADIOTABLE_ISSET_POOLID,
                                    (UINT1 *) pCapwapTrgIsSetFsWtpRadioEntry);
                return OSIX_FAILURE;
            }

            MEMSET (pCapwapFsWtpRadioEntry, 0, sizeof (tCapwapFsWtpRadioEntry));
            if ((CapwapInitializeFsWtpRadioTable (pCapwapFsWtpRadioEntry)) ==
                OSIX_FAILURE)
            {
                if (CapwapSetAllFsWtpRadioTableTrigger
                    (pCapwapSetFsWtpRadioEntry, pCapwapIsSetFsWtpRadioEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsWtpRadioTable:CapwapSetAllFsWtpRadioTableTrigger function fails\r\n");

                }
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsWtpRadioTable: Fail to Initialize the Objects.\r\n");

                MemReleaseMemBlock (CAPWAP_FSWTPRADIOTABLE_POOLID,
                                    (UINT1 *) pCapwapFsWtpRadioEntry);
                MemReleaseMemBlock (CAPWAP_FSWTPRADIOTABLE_POOLID,
                                    (UINT1 *) pCapwapOldFsWtpRadioEntry);
                MemReleaseMemBlock (CAPWAP_FSWTPRADIOTABLE_POOLID,
                                    (UINT1 *) pCapwapTrgFsWtpRadioEntry);
                MemReleaseMemBlock (CAPWAP_FSWTPRADIOTABLE_ISSET_POOLID,
                                    (UINT1 *) pCapwapTrgIsSetFsWtpRadioEntry);
                return OSIX_FAILURE;
            }
            /* Assign values for the new node */
            if (pCapwapIsSetFsWtpRadioEntry->bFsCapwapWtpModelNumber !=
                OSIX_FALSE)
            {
                MEMCPY (pCapwapFsWtpRadioEntry->MibObject.
                        au1FsCapwapWtpModelNumber,
                        pCapwapSetFsWtpRadioEntry->MibObject.
                        au1FsCapwapWtpModelNumber,
                        pCapwapSetFsWtpRadioEntry->MibObject.
                        i4FsCapwapWtpModelNumberLen);

                pCapwapFsWtpRadioEntry->MibObject.i4FsCapwapWtpModelNumberLen =
                    pCapwapSetFsWtpRadioEntry->MibObject.
                    i4FsCapwapWtpModelNumberLen;
            }

            if (pCapwapIsSetFsWtpRadioEntry->bFsWtpRadioType != OSIX_FALSE)
            {
                pCapwapFsWtpRadioEntry->MibObject.u4FsWtpRadioType =
                    pCapwapSetFsWtpRadioEntry->MibObject.u4FsWtpRadioType;
            }

            if (pCapwapIsSetFsWtpRadioEntry->bFsRadioAdminStatus != OSIX_FALSE)
            {
                pCapwapFsWtpRadioEntry->MibObject.i4FsRadioAdminStatus =
                    pCapwapSetFsWtpRadioEntry->MibObject.i4FsRadioAdminStatus;
            }

            if (pCapwapIsSetFsWtpRadioEntry->bFsMaxSSIDSupported != OSIX_FALSE)
            {
                pCapwapFsWtpRadioEntry->MibObject.i4FsMaxSSIDSupported =
                    pCapwapSetFsWtpRadioEntry->MibObject.i4FsMaxSSIDSupported;
            }

            if (pCapwapIsSetFsWtpRadioEntry->bFsWtpRadioRowStatus != OSIX_FALSE)
            {
                pCapwapFsWtpRadioEntry->MibObject.i4FsWtpRadioRowStatus =
                    pCapwapSetFsWtpRadioEntry->MibObject.i4FsWtpRadioRowStatus;
            }

            if (pCapwapIsSetFsWtpRadioEntry->bFsNumOfRadio != OSIX_FALSE)
            {
                pCapwapFsWtpRadioEntry->MibObject.u4FsNumOfRadio =
                    pCapwapSetFsWtpRadioEntry->MibObject.u4FsNumOfRadio;
            }

            if ((pCapwapSetFsWtpRadioEntry->MibObject.i4FsWtpRadioRowStatus ==
                 CREATE_AND_GO) || ((i4RowCreateOption == 1)
                                    && (pCapwapSetFsWtpRadioEntry->MibObject.
                                        i4FsWtpRadioRowStatus == ACTIVE)))
            {
                pCapwapFsWtpRadioEntry->MibObject.i4FsWtpRadioRowStatus =
                    ACTIVE;
            }
            else if (pCapwapSetFsWtpRadioEntry->MibObject.
                     i4FsWtpRadioRowStatus == CREATE_AND_WAIT)
            {
                pCapwapFsWtpRadioEntry->MibObject.i4FsWtpRadioRowStatus =
                    NOT_READY;
            }

            /* Add the new node to the database */
            if (RBTreeAdd
                (gCapwapGlobals.CapwapGlbMib.FsWtpRadioTable,
                 (tRBElem *) pCapwapFsWtpRadioEntry) != RB_SUCCESS)
            {
                if (CapwapSetAllFsWtpRadioTableTrigger
                    (pCapwapSetFsWtpRadioEntry, pCapwapIsSetFsWtpRadioEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsWtpRadioTable: CapwapSetAllFsWtpRadioTableTrigger function returns failure.\r\n");
                }
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsWtpRadioTable: RBTreeAdd is failed.\r\n");

                MemReleaseMemBlock (CAPWAP_FSWTPRADIOTABLE_POOLID,
                                    (UINT1 *) pCapwapFsWtpRadioEntry);
                MemReleaseMemBlock (CAPWAP_FSWTPRADIOTABLE_POOLID,
                                    (UINT1 *) pCapwapOldFsWtpRadioEntry);
                MemReleaseMemBlock (CAPWAP_FSWTPRADIOTABLE_POOLID,
                                    (UINT1 *) pCapwapTrgFsWtpRadioEntry);
                MemReleaseMemBlock (CAPWAP_FSWTPRADIOTABLE_ISSET_POOLID,
                                    (UINT1 *) pCapwapTrgIsSetFsWtpRadioEntry);
                return OSIX_FAILURE;
            }
            if (CapwapUtilUpdateFsWtpRadioTable
                (NULL, pCapwapFsWtpRadioEntry,
                 pCapwapIsSetFsWtpRadioEntry) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsWtpRadioTable: CapwapUtilUpdateFsWtpRadioTable function returns failure.\r\n");

                if (CapwapSetAllFsWtpRadioTableTrigger
                    (pCapwapSetFsWtpRadioEntry, pCapwapIsSetFsWtpRadioEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsWtpRadioTable: CapwapSetAllFsWtpRadioTableTrigger function returns failure.\r\n");

                }
                RBTreeRem (gCapwapGlobals.CapwapGlbMib.FsWtpRadioTable,
                           pCapwapFsWtpRadioEntry);
                MemReleaseMemBlock (CAPWAP_FSWTPRADIOTABLE_POOLID,
                                    (UINT1 *) pCapwapFsWtpRadioEntry);
                MemReleaseMemBlock (CAPWAP_FSWTPRADIOTABLE_POOLID,
                                    (UINT1 *) pCapwapOldFsWtpRadioEntry);
                MemReleaseMemBlock (CAPWAP_FSWTPRADIOTABLE_POOLID,
                                    (UINT1 *) pCapwapTrgFsWtpRadioEntry);
                MemReleaseMemBlock (CAPWAP_FSWTPRADIOTABLE_ISSET_POOLID,
                                    (UINT1 *) pCapwapTrgIsSetFsWtpRadioEntry);
                return OSIX_FAILURE;
            }

            if ((pCapwapSetFsWtpRadioEntry->MibObject.i4FsWtpRadioRowStatus ==
                 CREATE_AND_GO) || ((i4RowCreateOption == 1)
                                    && (pCapwapSetFsWtpRadioEntry->MibObject.
                                        i4FsWtpRadioRowStatus == ACTIVE)))
            {

                /* For MSR and RM Trigger */
                MEMCPY (pCapwapTrgFsWtpRadioEntry->MibObject.
                        au1FsCapwapWtpModelNumber,
                        pCapwapSetFsWtpRadioEntry->MibObject.
                        au1FsCapwapWtpModelNumber,
                        pCapwapSetFsWtpRadioEntry->MibObject.
                        i4FsCapwapWtpModelNumberLen);

                pCapwapTrgFsWtpRadioEntry->MibObject.
                    i4FsCapwapWtpModelNumberLen =
                    pCapwapSetFsWtpRadioEntry->MibObject.
                    i4FsCapwapWtpModelNumberLen;
                pCapwapTrgFsWtpRadioEntry->MibObject.u4FsNumOfRadio =
                    pCapwapSetFsWtpRadioEntry->MibObject.u4FsNumOfRadio;
                pCapwapTrgFsWtpRadioEntry->MibObject.i4FsWtpRadioRowStatus =
                    CREATE_AND_WAIT;
                pCapwapTrgIsSetFsWtpRadioEntry->bFsWtpRadioRowStatus =
                    OSIX_TRUE;

                if (CapwapSetAllFsWtpRadioTableTrigger
                    (pCapwapTrgFsWtpRadioEntry, pCapwapTrgIsSetFsWtpRadioEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsWtpRadioTable: CapwapSetAllFsWtpRadioTableTrigger function returns failure.\r\n");

                    MemReleaseMemBlock (CAPWAP_FSWTPRADIOTABLE_POOLID,
                                        (UINT1 *) pCapwapFsWtpRadioEntry);
                    MemReleaseMemBlock (CAPWAP_FSWTPRADIOTABLE_POOLID,
                                        (UINT1 *) pCapwapOldFsWtpRadioEntry);
                    MemReleaseMemBlock (CAPWAP_FSWTPRADIOTABLE_POOLID,
                                        (UINT1 *) pCapwapTrgFsWtpRadioEntry);
                    MemReleaseMemBlock (CAPWAP_FSWTPRADIOTABLE_ISSET_POOLID,
                                        (UINT1 *)
                                        pCapwapTrgIsSetFsWtpRadioEntry);
                    return OSIX_FAILURE;
                }
            }
            else if (pCapwapSetFsWtpRadioEntry->MibObject.
                     i4FsWtpRadioRowStatus == CREATE_AND_WAIT)
            {

                /* For MSR and RM Trigger */
                pCapwapTrgFsWtpRadioEntry->MibObject.i4FsWtpRadioRowStatus =
                    CREATE_AND_WAIT;
                pCapwapTrgIsSetFsWtpRadioEntry->bFsWtpRadioRowStatus =
                    OSIX_TRUE;

                if (CapwapSetAllFsWtpRadioTableTrigger
                    (pCapwapTrgFsWtpRadioEntry, pCapwapTrgIsSetFsWtpRadioEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsWtpRadioTable: CapwapSetAllFsWtpRadioTableTrigger function returns failure.\r\n");

                    MemReleaseMemBlock (CAPWAP_FSWTPRADIOTABLE_POOLID,
                                        (UINT1 *) pCapwapFsWtpRadioEntry);
                    MemReleaseMemBlock (CAPWAP_FSWTPRADIOTABLE_POOLID,
                                        (UINT1 *) pCapwapOldFsWtpRadioEntry);
                    MemReleaseMemBlock (CAPWAP_FSWTPRADIOTABLE_POOLID,
                                        (UINT1 *) pCapwapTrgFsWtpRadioEntry);
                    MemReleaseMemBlock (CAPWAP_FSWTPRADIOTABLE_ISSET_POOLID,
                                        (UINT1 *)
                                        pCapwapTrgIsSetFsWtpRadioEntry);
                    return OSIX_FAILURE;
                }
            }

            if (pCapwapSetFsWtpRadioEntry->MibObject.i4FsWtpRadioRowStatus ==
                CREATE_AND_GO)
            {
                pCapwapSetFsWtpRadioEntry->MibObject.i4FsWtpRadioRowStatus =
                    ACTIVE;
            }

            if (CapwapSetAllFsWtpRadioTableTrigger (pCapwapSetFsWtpRadioEntry,
                                                    pCapwapIsSetFsWtpRadioEntry,
                                                    SNMP_SUCCESS) !=
                OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsWtpRadioTable:  CapwapSetAllFsWtpRadioTableTrigger function returns failure.\r\n");
            }
            MemReleaseMemBlock (CAPWAP_FSWTPRADIOTABLE_POOLID,
                                (UINT1 *) pCapwapOldFsWtpRadioEntry);
            MemReleaseMemBlock (CAPWAP_FSWTPRADIOTABLE_POOLID,
                                (UINT1 *) pCapwapTrgFsWtpRadioEntry);
            MemReleaseMemBlock (CAPWAP_FSWTPRADIOTABLE_ISSET_POOLID,
                                (UINT1 *) pCapwapTrgIsSetFsWtpRadioEntry);
            return OSIX_SUCCESS;

        }
        else
        {
            if (CapwapSetAllFsWtpRadioTableTrigger (pCapwapSetFsWtpRadioEntry,
                                                    pCapwapIsSetFsWtpRadioEntry,
                                                    SNMP_FAILURE) !=
                OSIX_SUCCESS)

            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsWtpRadioTable: CapwapSetAllFsWtpRadioTableTrigger function returns failure.\r\n");
            }
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsWtpRadioTable: Failure.\r\n");
            MemReleaseMemBlock (CAPWAP_FSWTPRADIOTABLE_POOLID,
                                (UINT1 *) pCapwapOldFsWtpRadioEntry);
            MemReleaseMemBlock (CAPWAP_FSWTPRADIOTABLE_POOLID,
                                (UINT1 *) pCapwapTrgFsWtpRadioEntry);
            MemReleaseMemBlock (CAPWAP_FSWTPRADIOTABLE_ISSET_POOLID,
                                (UINT1 *) pCapwapTrgIsSetFsWtpRadioEntry);
            return OSIX_FAILURE;
        }
    }
    else if ((pCapwapSetFsWtpRadioEntry->MibObject.i4FsWtpRadioRowStatus ==
              CREATE_AND_WAIT)
             || (pCapwapSetFsWtpRadioEntry->MibObject.i4FsWtpRadioRowStatus ==
                 CREATE_AND_GO))
    {
        if (CapwapSetAllFsWtpRadioTableTrigger (pCapwapSetFsWtpRadioEntry,
                                                pCapwapIsSetFsWtpRadioEntry,
                                                SNMP_FAILURE) != OSIX_SUCCESS)

        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsWtpRadioTable: CapwapSetAllFsWtpRadioTableTrigger function returns failure.\r\n");
        }
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapSetAllFsWtpRadioTable: The row is already present.\r\n");
        MemReleaseMemBlock (CAPWAP_FSWTPRADIOTABLE_POOLID,
                            (UINT1 *) pCapwapOldFsWtpRadioEntry);
        MemReleaseMemBlock (CAPWAP_FSWTPRADIOTABLE_POOLID,
                            (UINT1 *) pCapwapTrgFsWtpRadioEntry);
        MemReleaseMemBlock (CAPWAP_FSWTPRADIOTABLE_ISSET_POOLID,
                            (UINT1 *) pCapwapTrgIsSetFsWtpRadioEntry);
        return OSIX_FAILURE;
    }
    /* Copy the previous values before setting the new values */
    MEMCPY (pCapwapOldFsWtpRadioEntry, pCapwapFsWtpRadioEntry,
            sizeof (tCapwapFsWtpRadioEntry));

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pCapwapSetFsWtpRadioEntry->MibObject.i4FsWtpRadioRowStatus == DESTROY)
    {
        pCapwapFsWtpRadioEntry->MibObject.i4FsWtpRadioRowStatus = DESTROY;

        if (CapwapUtilUpdateFsWtpRadioTable (pCapwapOldFsWtpRadioEntry,
                                             pCapwapFsWtpRadioEntry,
                                             pCapwapIsSetFsWtpRadioEntry) !=
            OSIX_SUCCESS)
        {

            if (CapwapSetAllFsWtpRadioTableTrigger (pCapwapSetFsWtpRadioEntry,
                                                    pCapwapIsSetFsWtpRadioEntry,
                                                    SNMP_FAILURE) !=
                OSIX_SUCCESS)

            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsWtpRadioTable: CapwapSetAllFsWtpRadioTableTrigger function returns failure.\r\n");
            }
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsWtpRadioTable: CapwapUtilUpdateFsWtpRadioTable function returns failure.\r\n");
        }
        RBTreeRem (gCapwapGlobals.CapwapGlbMib.FsWtpRadioTable,
                   pCapwapFsWtpRadioEntry);
        if (CapwapSetAllFsWtpRadioTableTrigger
            (pCapwapSetFsWtpRadioEntry, pCapwapIsSetFsWtpRadioEntry,
             SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsWtpRadioTable: CapwapSetAllFsWtpRadioTableTrigger function returns failure.\r\n");
            MemReleaseMemBlock (CAPWAP_FSWTPRADIOTABLE_POOLID,
                                (UINT1 *) pCapwapOldFsWtpRadioEntry);
            MemReleaseMemBlock (CAPWAP_FSWTPRADIOTABLE_POOLID,
                                (UINT1 *) pCapwapTrgFsWtpRadioEntry);
            MemReleaseMemBlock (CAPWAP_FSWTPRADIOTABLE_ISSET_POOLID,
                                (UINT1 *) pCapwapTrgIsSetFsWtpRadioEntry);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (CAPWAP_FSWTPRADIOTABLE_POOLID,
                            (UINT1 *) pCapwapFsWtpRadioEntry);
        MemReleaseMemBlock (CAPWAP_FSWTPRADIOTABLE_POOLID,
                            (UINT1 *) pCapwapOldFsWtpRadioEntry);
        MemReleaseMemBlock (CAPWAP_FSWTPRADIOTABLE_POOLID,
                            (UINT1 *) pCapwapTrgFsWtpRadioEntry);
        MemReleaseMemBlock (CAPWAP_FSWTPRADIOTABLE_ISSET_POOLID,
                            (UINT1 *) pCapwapTrgIsSetFsWtpRadioEntry);
        return OSIX_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (FsWtpRadioTableFilterInputs
        (pCapwapFsWtpRadioEntry, pCapwapSetFsWtpRadioEntry,
         pCapwapIsSetFsWtpRadioEntry) != OSIX_TRUE)
    {
        MemReleaseMemBlock (CAPWAP_FSWTPRADIOTABLE_POOLID,
                            (UINT1 *) pCapwapOldFsWtpRadioEntry);
        MemReleaseMemBlock (CAPWAP_FSWTPRADIOTABLE_POOLID,
                            (UINT1 *) pCapwapTrgFsWtpRadioEntry);
        MemReleaseMemBlock (CAPWAP_FSWTPRADIOTABLE_ISSET_POOLID,
                            (UINT1 *) pCapwapTrgIsSetFsWtpRadioEntry);
        return OSIX_SUCCESS;
    }

    /*This condtion is to make the row NOT_IN_SERVICE before modifying the values */
    if ((i4RowStatusLogic == TRUE) &&
        (pCapwapFsWtpRadioEntry->MibObject.i4FsWtpRadioRowStatus == ACTIVE) &&
        (pCapwapSetFsWtpRadioEntry->MibObject.i4FsWtpRadioRowStatus !=
         NOT_IN_SERVICE))
    {
        pCapwapFsWtpRadioEntry->MibObject.i4FsWtpRadioRowStatus =
            NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        pCapwapTrgFsWtpRadioEntry->MibObject.i4FsWtpRadioRowStatus =
            NOT_IN_SERVICE;
        pCapwapTrgIsSetFsWtpRadioEntry->bFsWtpRadioRowStatus = OSIX_TRUE;

        if (CapwapUtilUpdateFsWtpRadioTable (pCapwapOldFsWtpRadioEntry,
                                             pCapwapFsWtpRadioEntry,
                                             pCapwapIsSetFsWtpRadioEntry) !=
            OSIX_SUCCESS)
        {
            /*Restore back with previous values */
            MEMCPY (pCapwapFsWtpRadioEntry, pCapwapOldFsWtpRadioEntry,
                    sizeof (tCapwapFsWtpRadioEntry));
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsWtpRadioTable: CapwapUtilUpdateFsWtpRadioTable Function returns failure.\r\n");

            if (CapwapSetAllFsWtpRadioTableTrigger (pCapwapSetFsWtpRadioEntry,
                                                    pCapwapIsSetFsWtpRadioEntry,
                                                    SNMP_FAILURE) !=
                OSIX_SUCCESS)

            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsWtpRadioTable: CapwapSetAllFsWtpRadioTableTrigger function returns failure.\r\n");
            }
            MemReleaseMemBlock (CAPWAP_FSWTPRADIOTABLE_POOLID,
                                (UINT1 *) pCapwapOldFsWtpRadioEntry);
            MemReleaseMemBlock (CAPWAP_FSWTPRADIOTABLE_POOLID,
                                (UINT1 *) pCapwapTrgFsWtpRadioEntry);
            MemReleaseMemBlock (CAPWAP_FSWTPRADIOTABLE_ISSET_POOLID,
                                (UINT1 *) pCapwapTrgIsSetFsWtpRadioEntry);
            return OSIX_FAILURE;
        }

        if (CapwapSetAllFsWtpRadioTableTrigger (pCapwapTrgFsWtpRadioEntry,
                                                pCapwapTrgIsSetFsWtpRadioEntry,
                                                SNMP_FAILURE) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsWtpRadioTable: CapwapSetAllFsWtpRadioTableTrigger function returns failure.\r\n");
        }
    }

    if (pCapwapSetFsWtpRadioEntry->MibObject.i4FsWtpRadioRowStatus == ACTIVE)
    {
        i4RowMakeActive = TRUE;
    }

    /* Assign values for the existing node */
    if (pCapwapIsSetFsWtpRadioEntry->bFsWtpRadioType != OSIX_FALSE)
    {
        pCapwapFsWtpRadioEntry->MibObject.u4FsWtpRadioType =
            pCapwapSetFsWtpRadioEntry->MibObject.u4FsWtpRadioType;
    }
    if (pCapwapIsSetFsWtpRadioEntry->bFsRadioAdminStatus != OSIX_FALSE)
    {
        pCapwapFsWtpRadioEntry->MibObject.i4FsRadioAdminStatus =
            pCapwapSetFsWtpRadioEntry->MibObject.i4FsRadioAdminStatus;
    }
    if (pCapwapIsSetFsWtpRadioEntry->bFsMaxSSIDSupported != OSIX_FALSE)
    {
        pCapwapFsWtpRadioEntry->MibObject.i4FsMaxSSIDSupported =
            pCapwapSetFsWtpRadioEntry->MibObject.i4FsMaxSSIDSupported;
    }
    if (pCapwapIsSetFsWtpRadioEntry->bFsWtpRadioRowStatus != OSIX_FALSE)
    {
        pCapwapFsWtpRadioEntry->MibObject.i4FsWtpRadioRowStatus =
            pCapwapSetFsWtpRadioEntry->MibObject.i4FsWtpRadioRowStatus;
    }

    /*This condtion is to make the row back to ACTIVE after modifying the values */
    if (i4RowMakeActive == TRUE)
    {
        pCapwapFsWtpRadioEntry->MibObject.i4FsWtpRadioRowStatus = ACTIVE;
    }

    if (CapwapUtilUpdateFsWtpRadioTable (pCapwapOldFsWtpRadioEntry,
                                         pCapwapFsWtpRadioEntry,
                                         pCapwapIsSetFsWtpRadioEntry) !=
        OSIX_SUCCESS)
    {

        if (CapwapSetAllFsWtpRadioTableTrigger (pCapwapSetFsWtpRadioEntry,
                                                pCapwapIsSetFsWtpRadioEntry,
                                                SNMP_FAILURE) != OSIX_SUCCESS)

        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsWtpRadioTable: CapwapSetAllFsWtpRadioTableTrigger function returns failure.\r\n");

        }
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapSetAllFsWtpRadioTable: CapwapUtilUpdateFsWtpRadioTable function returns failure.\r\n");
        /*Restore back with previous values */
        MEMCPY (pCapwapFsWtpRadioEntry, pCapwapOldFsWtpRadioEntry,
                sizeof (tCapwapFsWtpRadioEntry));
        MemReleaseMemBlock (CAPWAP_FSWTPRADIOTABLE_POOLID,
                            (UINT1 *) pCapwapOldFsWtpRadioEntry);
        MemReleaseMemBlock (CAPWAP_FSWTPRADIOTABLE_POOLID,
                            (UINT1 *) pCapwapTrgFsWtpRadioEntry);
        MemReleaseMemBlock (CAPWAP_FSWTPRADIOTABLE_ISSET_POOLID,
                            (UINT1 *) pCapwapTrgIsSetFsWtpRadioEntry);
        return OSIX_FAILURE;

    }
    if (CapwapSetAllFsWtpRadioTableTrigger (pCapwapSetFsWtpRadioEntry,
                                            pCapwapIsSetFsWtpRadioEntry,
                                            SNMP_SUCCESS) != OSIX_SUCCESS)
    {

        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapSetAllFsWtpRadioTable: CapwapSetAllFsWtpRadioTableTrigger function returns failure.\r\n");
    }

    MemReleaseMemBlock (CAPWAP_FSWTPRADIOTABLE_POOLID,
                        (UINT1 *) pCapwapOldFsWtpRadioEntry);
    MemReleaseMemBlock (CAPWAP_FSWTPRADIOTABLE_POOLID,
                        (UINT1 *) pCapwapTrgFsWtpRadioEntry);
    MemReleaseMemBlock (CAPWAP_FSWTPRADIOTABLE_ISSET_POOLID,
                        (UINT1 *) pCapwapTrgIsSetFsWtpRadioEntry);
    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  CapwapSetAllFsCapwapWhiteListTable
 Input       :  pCapwapSetFsCapwapWhiteListEntry
                pCapwapIsSetFsCapwapWhiteListEntry
                i4RowStatusLogic
                i4RowCreateOption
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapSetAllFsCapwapWhiteListTable (tCapwapFsCapwapWhiteListEntry *
                                    pCapwapSetFsCapwapWhiteListEntry,
                                    tCapwapIsSetFsCapwapWhiteListEntry *
                                    pCapwapIsSetFsCapwapWhiteListEntry,
                                    INT4 i4RowStatusLogic,
                                    INT4 i4RowCreateOption)
{
    tCapwapFsCapwapWhiteListEntry *pCapwapFsCapwapWhiteListEntry = NULL;
    tCapwapFsCapwapWhiteListEntry *pCapwapOldFsCapwapWhiteListEntry = NULL;
    tCapwapFsCapwapWhiteListEntry *pCapwapTrgFsCapwapWhiteListEntry = NULL;
    tCapwapIsSetFsCapwapWhiteListEntry *pCapwapTrgIsSetFsCapwapWhiteListEntry =
        NULL;
    INT4                i4RowMakeActive = FALSE;

    pCapwapOldFsCapwapWhiteListEntry =
        (tCapwapFsCapwapWhiteListEntry *)
        MemAllocMemBlk (CAPWAP_FSCAPWAPWHITELISTTABLE_POOLID);
    if (pCapwapOldFsCapwapWhiteListEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    pCapwapTrgFsCapwapWhiteListEntry =
        (tCapwapFsCapwapWhiteListEntry *)
        MemAllocMemBlk (CAPWAP_FSCAPWAPWHITELISTTABLE_POOLID);
    if (pCapwapTrgFsCapwapWhiteListEntry == NULL)
    {
        MemReleaseMemBlock (CAPWAP_FSCAPWAPWHITELISTTABLE_POOLID,
                            (UINT1 *) pCapwapOldFsCapwapWhiteListEntry);
        return OSIX_FAILURE;
    }
    pCapwapTrgIsSetFsCapwapWhiteListEntry =
        (tCapwapIsSetFsCapwapWhiteListEntry *)
        MemAllocMemBlk (CAPWAP_FSCAPWAPWHITELISTTABLE_ISSET_POOLID);
    if (pCapwapTrgIsSetFsCapwapWhiteListEntry == NULL)
    {
        MemReleaseMemBlock (CAPWAP_FSCAPWAPWHITELISTTABLE_POOLID,
                            (UINT1 *) pCapwapOldFsCapwapWhiteListEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPWHITELISTTABLE_POOLID,
                            (UINT1 *) pCapwapTrgFsCapwapWhiteListEntry);
        return OSIX_FAILURE;
    }
    MEMSET (pCapwapOldFsCapwapWhiteListEntry, 0,
            sizeof (tCapwapFsCapwapWhiteListEntry));
    MEMSET (pCapwapTrgFsCapwapWhiteListEntry, 0,
            sizeof (tCapwapFsCapwapWhiteListEntry));
    MEMSET (pCapwapTrgIsSetFsCapwapWhiteListEntry, 0,
            sizeof (tCapwapIsSetFsCapwapWhiteListEntry));

    /* Check whether the node is already present */
    pCapwapFsCapwapWhiteListEntry =
        RBTreeGet (gCapwapGlobals.CapwapGlbMib.FsCapwapWhiteListTable,
                   (tRBElem *) pCapwapSetFsCapwapWhiteListEntry);

    if (pCapwapFsCapwapWhiteListEntry == NULL)
    {
        /* Create the node if the RowStatus given is CREATE_AND_WAIT or CREATE_AND_GO */
        if ((pCapwapSetFsCapwapWhiteListEntry->MibObject.
             i4FsCapwapWhiteListRowStatus == CREATE_AND_WAIT)
            || (pCapwapSetFsCapwapWhiteListEntry->MibObject.
                i4FsCapwapWhiteListRowStatus == CREATE_AND_GO)
            ||
            ((pCapwapSetFsCapwapWhiteListEntry->MibObject.
              i4FsCapwapWhiteListRowStatus == ACTIVE)
             && (i4RowCreateOption == 1)))
        {
            /* Allocate memory for the new node */
            pCapwapFsCapwapWhiteListEntry =
                (tCapwapFsCapwapWhiteListEntry *)
                MemAllocMemBlk (CAPWAP_FSCAPWAPWHITELISTTABLE_POOLID);
            if (pCapwapFsCapwapWhiteListEntry == NULL)
            {
                if (CapwapSetAllFsCapwapWhiteListTableTrigger
                    (pCapwapSetFsCapwapWhiteListEntry,
                     pCapwapIsSetFsCapwapWhiteListEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCapwapWhiteListTable:CapwapSetAllFsCapwapWhiteListTableTrigger function fails\r\n");

                }
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCapwapWhiteListTable: Fail to Allocate Memory.\r\n");
                MemReleaseMemBlock (CAPWAP_FSCAPWAPWHITELISTTABLE_POOLID,
                                    (UINT1 *) pCapwapOldFsCapwapWhiteListEntry);
                MemReleaseMemBlock (CAPWAP_FSCAPWAPWHITELISTTABLE_POOLID,
                                    (UINT1 *) pCapwapTrgFsCapwapWhiteListEntry);
                MemReleaseMemBlock (CAPWAP_FSCAPWAPWHITELISTTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgIsSetFsCapwapWhiteListEntry);
                return OSIX_FAILURE;
            }

            MEMSET (pCapwapFsCapwapWhiteListEntry, 0,
                    sizeof (tCapwapFsCapwapWhiteListEntry));
            if ((CapwapInitializeFsCapwapWhiteListTable
                 (pCapwapFsCapwapWhiteListEntry)) == OSIX_FAILURE)
            {
                if (CapwapSetAllFsCapwapWhiteListTableTrigger
                    (pCapwapSetFsCapwapWhiteListEntry,
                     pCapwapIsSetFsCapwapWhiteListEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCapwapWhiteListTable:CapwapSetAllFsCapwapWhiteListTableTrigger function fails\r\n");

                }
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCapwapWhiteListTable: Fail to Initialize the Objects.\r\n");

                MemReleaseMemBlock (CAPWAP_FSCAPWAPWHITELISTTABLE_POOLID,
                                    (UINT1 *) pCapwapFsCapwapWhiteListEntry);
                MemReleaseMemBlock (CAPWAP_FSCAPWAPWHITELISTTABLE_POOLID,
                                    (UINT1 *) pCapwapOldFsCapwapWhiteListEntry);
                MemReleaseMemBlock (CAPWAP_FSCAPWAPWHITELISTTABLE_POOLID,
                                    (UINT1 *) pCapwapTrgFsCapwapWhiteListEntry);
                MemReleaseMemBlock (CAPWAP_FSCAPWAPWHITELISTTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgIsSetFsCapwapWhiteListEntry);
                return OSIX_FAILURE;
            }
            /* Assign values for the new node */
            if (pCapwapIsSetFsCapwapWhiteListEntry->bFsCapwapWhiteListId !=
                OSIX_FALSE)
            {
                pCapwapFsCapwapWhiteListEntry->MibObject.u4FsCapwapWhiteListId =
                    pCapwapSetFsCapwapWhiteListEntry->MibObject.
                    u4FsCapwapWhiteListId;
            }

            if (pCapwapIsSetFsCapwapWhiteListEntry->
                bFsCapwapWhiteListWtpBaseMac != OSIX_FALSE)
            {
                MEMCPY (pCapwapFsCapwapWhiteListEntry->MibObject.
                        au1FsCapwapWhiteListWtpBaseMac,
                        pCapwapSetFsCapwapWhiteListEntry->MibObject.
                        au1FsCapwapWhiteListWtpBaseMac,
                        pCapwapSetFsCapwapWhiteListEntry->MibObject.
                        i4FsCapwapWhiteListWtpBaseMacLen);

                pCapwapFsCapwapWhiteListEntry->MibObject.
                    i4FsCapwapWhiteListWtpBaseMacLen =
                    pCapwapSetFsCapwapWhiteListEntry->MibObject.
                    i4FsCapwapWhiteListWtpBaseMacLen;
            }

            if (pCapwapIsSetFsCapwapWhiteListEntry->
                bFsCapwapWhiteListRowStatus != OSIX_FALSE)
            {
                pCapwapFsCapwapWhiteListEntry->MibObject.
                    i4FsCapwapWhiteListRowStatus =
                    pCapwapSetFsCapwapWhiteListEntry->MibObject.
                    i4FsCapwapWhiteListRowStatus;
            }

            if ((pCapwapSetFsCapwapWhiteListEntry->MibObject.
                 i4FsCapwapWhiteListRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pCapwapSetFsCapwapWhiteListEntry->MibObject.
                        i4FsCapwapWhiteListRowStatus == ACTIVE)))
            {
                pCapwapFsCapwapWhiteListEntry->MibObject.
                    i4FsCapwapWhiteListRowStatus = ACTIVE;
            }
            else if (pCapwapSetFsCapwapWhiteListEntry->MibObject.
                     i4FsCapwapWhiteListRowStatus == CREATE_AND_WAIT)
            {
                pCapwapFsCapwapWhiteListEntry->MibObject.
                    i4FsCapwapWhiteListRowStatus = NOT_READY;
            }

            /* Add the new node to the database */
            if (RBTreeAdd
                (gCapwapGlobals.CapwapGlbMib.FsCapwapWhiteListTable,
                 (tRBElem *) pCapwapFsCapwapWhiteListEntry) != RB_SUCCESS)
            {
                if (CapwapSetAllFsCapwapWhiteListTableTrigger
                    (pCapwapSetFsCapwapWhiteListEntry,
                     pCapwapIsSetFsCapwapWhiteListEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCapwapWhiteListTable: CapwapSetAllFsCapwapWhiteListTableTrigger function returns failure.\r\n");
                }
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCapwapWhiteListTable: RBTreeAdd is failed.\r\n");

                MemReleaseMemBlock (CAPWAP_FSCAPWAPWHITELISTTABLE_POOLID,
                                    (UINT1 *) pCapwapFsCapwapWhiteListEntry);
                MemReleaseMemBlock (CAPWAP_FSCAPWAPWHITELISTTABLE_POOLID,
                                    (UINT1 *) pCapwapOldFsCapwapWhiteListEntry);
                MemReleaseMemBlock (CAPWAP_FSCAPWAPWHITELISTTABLE_POOLID,
                                    (UINT1 *) pCapwapTrgFsCapwapWhiteListEntry);
                MemReleaseMemBlock (CAPWAP_FSCAPWAPWHITELISTTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgIsSetFsCapwapWhiteListEntry);
                return OSIX_FAILURE;
            }
            if (CapwapUtilUpdateFsCapwapWhiteListTable
                (NULL, pCapwapFsCapwapWhiteListEntry,
                 pCapwapIsSetFsCapwapWhiteListEntry) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCapwapWhiteListTable: CapwapUtilUpdateFsCapwapWhiteListTable function returns failure.\r\n");

                if (CapwapSetAllFsCapwapWhiteListTableTrigger
                    (pCapwapSetFsCapwapWhiteListEntry,
                     pCapwapIsSetFsCapwapWhiteListEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCapwapWhiteListTable: CapwapSetAllFsCapwapWhiteListTableTrigger function returns failure.\r\n");

                }
                RBTreeRem (gCapwapGlobals.CapwapGlbMib.FsCapwapWhiteListTable,
                           pCapwapFsCapwapWhiteListEntry);
                MemReleaseMemBlock (CAPWAP_FSCAPWAPWHITELISTTABLE_POOLID,
                                    (UINT1 *) pCapwapFsCapwapWhiteListEntry);
                MemReleaseMemBlock (CAPWAP_FSCAPWAPWHITELISTTABLE_POOLID,
                                    (UINT1 *) pCapwapOldFsCapwapWhiteListEntry);
                MemReleaseMemBlock (CAPWAP_FSCAPWAPWHITELISTTABLE_POOLID,
                                    (UINT1 *) pCapwapTrgFsCapwapWhiteListEntry);
                MemReleaseMemBlock (CAPWAP_FSCAPWAPWHITELISTTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgIsSetFsCapwapWhiteListEntry);
                return OSIX_FAILURE;
            }

            if ((pCapwapSetFsCapwapWhiteListEntry->MibObject.
                 i4FsCapwapWhiteListRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pCapwapSetFsCapwapWhiteListEntry->MibObject.
                        i4FsCapwapWhiteListRowStatus == ACTIVE)))
            {

                /* For MSR and RM Trigger */
                pCapwapTrgFsCapwapWhiteListEntry->MibObject.
                    u4FsCapwapWhiteListId =
                    pCapwapSetFsCapwapWhiteListEntry->MibObject.
                    u4FsCapwapWhiteListId;
                pCapwapTrgFsCapwapWhiteListEntry->MibObject.
                    i4FsCapwapWhiteListRowStatus = CREATE_AND_WAIT;
                pCapwapTrgIsSetFsCapwapWhiteListEntry->
                    bFsCapwapWhiteListRowStatus = OSIX_TRUE;

                if (CapwapSetAllFsCapwapWhiteListTableTrigger
                    (pCapwapTrgFsCapwapWhiteListEntry,
                     pCapwapTrgIsSetFsCapwapWhiteListEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCapwapWhiteListTable: CapwapSetAllFsCapwapWhiteListTableTrigger function returns failure.\r\n");

                    MemReleaseMemBlock (CAPWAP_FSCAPWAPWHITELISTTABLE_POOLID,
                                        (UINT1 *)
                                        pCapwapFsCapwapWhiteListEntry);
                    MemReleaseMemBlock (CAPWAP_FSCAPWAPWHITELISTTABLE_POOLID,
                                        (UINT1 *)
                                        pCapwapOldFsCapwapWhiteListEntry);
                    MemReleaseMemBlock (CAPWAP_FSCAPWAPWHITELISTTABLE_POOLID,
                                        (UINT1 *)
                                        pCapwapTrgFsCapwapWhiteListEntry);
                    MemReleaseMemBlock
                        (CAPWAP_FSCAPWAPWHITELISTTABLE_ISSET_POOLID,
                         (UINT1 *) pCapwapTrgIsSetFsCapwapWhiteListEntry);
                    return OSIX_FAILURE;
                }
            }
            else if (pCapwapSetFsCapwapWhiteListEntry->MibObject.
                     i4FsCapwapWhiteListRowStatus == CREATE_AND_WAIT)
            {

                /* For MSR and RM Trigger */
                pCapwapTrgFsCapwapWhiteListEntry->MibObject.
                    i4FsCapwapWhiteListRowStatus = CREATE_AND_WAIT;
                pCapwapTrgIsSetFsCapwapWhiteListEntry->
                    bFsCapwapWhiteListRowStatus = OSIX_TRUE;

                if (CapwapSetAllFsCapwapWhiteListTableTrigger
                    (pCapwapTrgFsCapwapWhiteListEntry,
                     pCapwapTrgIsSetFsCapwapWhiteListEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCapwapWhiteListTable: CapwapSetAllFsCapwapWhiteListTableTrigger function returns failure.\r\n");

                    MemReleaseMemBlock (CAPWAP_FSCAPWAPWHITELISTTABLE_POOLID,
                                        (UINT1 *)
                                        pCapwapFsCapwapWhiteListEntry);
                    MemReleaseMemBlock (CAPWAP_FSCAPWAPWHITELISTTABLE_POOLID,
                                        (UINT1 *)
                                        pCapwapOldFsCapwapWhiteListEntry);
                    MemReleaseMemBlock (CAPWAP_FSCAPWAPWHITELISTTABLE_POOLID,
                                        (UINT1 *)
                                        pCapwapTrgFsCapwapWhiteListEntry);
                    MemReleaseMemBlock
                        (CAPWAP_FSCAPWAPWHITELISTTABLE_ISSET_POOLID,
                         (UINT1 *) pCapwapTrgIsSetFsCapwapWhiteListEntry);
                    return OSIX_FAILURE;
                }
            }

            if (pCapwapSetFsCapwapWhiteListEntry->MibObject.
                i4FsCapwapWhiteListRowStatus == CREATE_AND_GO)
            {
                pCapwapSetFsCapwapWhiteListEntry->MibObject.
                    i4FsCapwapWhiteListRowStatus = ACTIVE;
            }

            if (CapwapSetAllFsCapwapWhiteListTableTrigger
                (pCapwapSetFsCapwapWhiteListEntry,
                 pCapwapIsSetFsCapwapWhiteListEntry,
                 SNMP_SUCCESS) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCapwapWhiteListTable:  CapwapSetAllFsCapwapWhiteListTableTrigger function returns failure.\r\n");
            }
            MemReleaseMemBlock (CAPWAP_FSCAPWAPWHITELISTTABLE_POOLID,
                                (UINT1 *) pCapwapOldFsCapwapWhiteListEntry);
            MemReleaseMemBlock (CAPWAP_FSCAPWAPWHITELISTTABLE_POOLID,
                                (UINT1 *) pCapwapTrgFsCapwapWhiteListEntry);
            MemReleaseMemBlock (CAPWAP_FSCAPWAPWHITELISTTABLE_ISSET_POOLID,
                                (UINT1 *)
                                pCapwapTrgIsSetFsCapwapWhiteListEntry);
            return OSIX_SUCCESS;

        }
        else
        {
            if (CapwapSetAllFsCapwapWhiteListTableTrigger
                (pCapwapSetFsCapwapWhiteListEntry,
                 pCapwapIsSetFsCapwapWhiteListEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCapwapWhiteListTable: CapwapSetAllFsCapwapWhiteListTableTrigger function returns failure.\r\n");
            }
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCapwapWhiteListTable: Failure.\r\n");
            MemReleaseMemBlock (CAPWAP_FSCAPWAPWHITELISTTABLE_POOLID,
                                (UINT1 *) pCapwapOldFsCapwapWhiteListEntry);
            MemReleaseMemBlock (CAPWAP_FSCAPWAPWHITELISTTABLE_POOLID,
                                (UINT1 *) pCapwapTrgFsCapwapWhiteListEntry);
            MemReleaseMemBlock (CAPWAP_FSCAPWAPWHITELISTTABLE_ISSET_POOLID,
                                (UINT1 *)
                                pCapwapTrgIsSetFsCapwapWhiteListEntry);
            return OSIX_FAILURE;
        }
    }
    else if ((pCapwapSetFsCapwapWhiteListEntry->MibObject.
              i4FsCapwapWhiteListRowStatus == CREATE_AND_WAIT)
             || (pCapwapSetFsCapwapWhiteListEntry->MibObject.
                 i4FsCapwapWhiteListRowStatus == CREATE_AND_GO))
    {
        if (CapwapSetAllFsCapwapWhiteListTableTrigger
            (pCapwapSetFsCapwapWhiteListEntry,
             pCapwapIsSetFsCapwapWhiteListEntry, SNMP_FAILURE) != OSIX_SUCCESS)

        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCapwapWhiteListTable: CapwapSetAllFsCapwapWhiteListTableTrigger function returns failure.\r\n");
        }
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapSetAllFsCapwapWhiteListTable: The row is already present.\r\n");
        MemReleaseMemBlock (CAPWAP_FSCAPWAPWHITELISTTABLE_POOLID,
                            (UINT1 *) pCapwapOldFsCapwapWhiteListEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPWHITELISTTABLE_POOLID,
                            (UINT1 *) pCapwapTrgFsCapwapWhiteListEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPWHITELISTTABLE_ISSET_POOLID,
                            (UINT1 *) pCapwapTrgIsSetFsCapwapWhiteListEntry);
        return OSIX_FAILURE;
    }
    /* Copy the previous values before setting the new values */
    MEMCPY (pCapwapOldFsCapwapWhiteListEntry, pCapwapFsCapwapWhiteListEntry,
            sizeof (tCapwapFsCapwapWhiteListEntry));

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pCapwapSetFsCapwapWhiteListEntry->MibObject.
        i4FsCapwapWhiteListRowStatus == DESTROY)
    {
        pCapwapFsCapwapWhiteListEntry->MibObject.i4FsCapwapWhiteListRowStatus =
            DESTROY;

        if (CapwapUtilUpdateFsCapwapWhiteListTable
            (pCapwapOldFsCapwapWhiteListEntry, pCapwapFsCapwapWhiteListEntry,
             pCapwapIsSetFsCapwapWhiteListEntry) != OSIX_SUCCESS)
        {

            if (CapwapSetAllFsCapwapWhiteListTableTrigger
                (pCapwapSetFsCapwapWhiteListEntry,
                 pCapwapIsSetFsCapwapWhiteListEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCapwapWhiteListTable: CapwapSetAllFsCapwapWhiteListTableTrigger function returns failure.\r\n");
            }
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCapwapWhiteListTable: CapwapUtilUpdateFsCapwapWhiteListTable function returns failure.\r\n");
        }
        RBTreeRem (gCapwapGlobals.CapwapGlbMib.FsCapwapWhiteListTable,
                   pCapwapFsCapwapWhiteListEntry);
        if (CapwapSetAllFsCapwapWhiteListTableTrigger
            (pCapwapSetFsCapwapWhiteListEntry,
             pCapwapIsSetFsCapwapWhiteListEntry, SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCapwapWhiteListTable: CapwapSetAllFsCapwapWhiteListTableTrigger function returns failure.\r\n");
            MemReleaseMemBlock (CAPWAP_FSCAPWAPWHITELISTTABLE_POOLID,
                                (UINT1 *) pCapwapOldFsCapwapWhiteListEntry);
            MemReleaseMemBlock (CAPWAP_FSCAPWAPWHITELISTTABLE_POOLID,
                                (UINT1 *) pCapwapTrgFsCapwapWhiteListEntry);
            MemReleaseMemBlock (CAPWAP_FSCAPWAPWHITELISTTABLE_ISSET_POOLID,
                                (UINT1 *)
                                pCapwapTrgIsSetFsCapwapWhiteListEntry);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (CAPWAP_FSCAPWAPWHITELISTTABLE_POOLID,
                            (UINT1 *) pCapwapFsCapwapWhiteListEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPWHITELISTTABLE_POOLID,
                            (UINT1 *) pCapwapOldFsCapwapWhiteListEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPWHITELISTTABLE_POOLID,
                            (UINT1 *) pCapwapTrgFsCapwapWhiteListEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPWHITELISTTABLE_ISSET_POOLID,
                            (UINT1 *) pCapwapTrgIsSetFsCapwapWhiteListEntry);
        return OSIX_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (FsCapwapWhiteListTableFilterInputs
        (pCapwapFsCapwapWhiteListEntry, pCapwapSetFsCapwapWhiteListEntry,
         pCapwapIsSetFsCapwapWhiteListEntry) != OSIX_TRUE)
    {
        MemReleaseMemBlock (CAPWAP_FSCAPWAPWHITELISTTABLE_POOLID,
                            (UINT1 *) pCapwapOldFsCapwapWhiteListEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPWHITELISTTABLE_POOLID,
                            (UINT1 *) pCapwapTrgFsCapwapWhiteListEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPWHITELISTTABLE_ISSET_POOLID,
                            (UINT1 *) pCapwapTrgIsSetFsCapwapWhiteListEntry);
        return OSIX_SUCCESS;
    }

    /*This condtion is to make the row NOT_IN_SERVICE before modifying the values */
    if ((i4RowStatusLogic == TRUE) &&
        (pCapwapFsCapwapWhiteListEntry->MibObject.
         i4FsCapwapWhiteListRowStatus == ACTIVE)
        && (pCapwapSetFsCapwapWhiteListEntry->MibObject.
            i4FsCapwapWhiteListRowStatus != NOT_IN_SERVICE))
    {
        pCapwapFsCapwapWhiteListEntry->MibObject.i4FsCapwapWhiteListRowStatus =
            NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        pCapwapTrgFsCapwapWhiteListEntry->MibObject.
            i4FsCapwapWhiteListRowStatus = NOT_IN_SERVICE;
        pCapwapTrgIsSetFsCapwapWhiteListEntry->bFsCapwapWhiteListRowStatus =
            OSIX_TRUE;

        if (CapwapUtilUpdateFsCapwapWhiteListTable
            (pCapwapOldFsCapwapWhiteListEntry, pCapwapFsCapwapWhiteListEntry,
             pCapwapIsSetFsCapwapWhiteListEntry) != OSIX_SUCCESS)
        {
            /*Restore back with previous values */
            MEMCPY (pCapwapFsCapwapWhiteListEntry,
                    pCapwapOldFsCapwapWhiteListEntry,
                    sizeof (tCapwapFsCapwapWhiteListEntry));
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCapwapWhiteListTable:                 CapwapUtilUpdateFsCapwapWhiteListTable Function returns failure.\r\n");

            if (CapwapSetAllFsCapwapWhiteListTableTrigger
                (pCapwapSetFsCapwapWhiteListEntry,
                 pCapwapIsSetFsCapwapWhiteListEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCapwapWhiteListTable: CapwapSetAllFsCapwapWhiteListTableTrigger function returns failure.\r\n");
            }
            MemReleaseMemBlock (CAPWAP_FSCAPWAPWHITELISTTABLE_POOLID,
                                (UINT1 *) pCapwapOldFsCapwapWhiteListEntry);
            MemReleaseMemBlock (CAPWAP_FSCAPWAPWHITELISTTABLE_POOLID,
                                (UINT1 *) pCapwapTrgFsCapwapWhiteListEntry);
            MemReleaseMemBlock (CAPWAP_FSCAPWAPWHITELISTTABLE_ISSET_POOLID,
                                (UINT1 *)
                                pCapwapTrgIsSetFsCapwapWhiteListEntry);
            return OSIX_FAILURE;
        }

        if (CapwapSetAllFsCapwapWhiteListTableTrigger
            (pCapwapTrgFsCapwapWhiteListEntry,
             pCapwapTrgIsSetFsCapwapWhiteListEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCapwapWhiteListTable: CapwapSetAllFsCapwapWhiteListTableTrigger function returns failure.\r\n");
        }
    }

    if (pCapwapSetFsCapwapWhiteListEntry->MibObject.
        i4FsCapwapWhiteListRowStatus == ACTIVE)
    {
        i4RowMakeActive = TRUE;
    }

    /* Assign values for the existing node */
    if (pCapwapIsSetFsCapwapWhiteListEntry->bFsCapwapWhiteListWtpBaseMac !=
        OSIX_FALSE)
    {
        MEMCPY (pCapwapFsCapwapWhiteListEntry->MibObject.
                au1FsCapwapWhiteListWtpBaseMac,
                pCapwapSetFsCapwapWhiteListEntry->MibObject.
                au1FsCapwapWhiteListWtpBaseMac,
                pCapwapSetFsCapwapWhiteListEntry->MibObject.
                i4FsCapwapWhiteListWtpBaseMacLen);

        pCapwapFsCapwapWhiteListEntry->MibObject.
            i4FsCapwapWhiteListWtpBaseMacLen =
            pCapwapSetFsCapwapWhiteListEntry->MibObject.
            i4FsCapwapWhiteListWtpBaseMacLen;
    }
    if (pCapwapIsSetFsCapwapWhiteListEntry->bFsCapwapWhiteListRowStatus !=
        OSIX_FALSE)
    {
        pCapwapFsCapwapWhiteListEntry->MibObject.i4FsCapwapWhiteListRowStatus =
            pCapwapSetFsCapwapWhiteListEntry->MibObject.
            i4FsCapwapWhiteListRowStatus;
    }

    /*This condtion is to make the row back to ACTIVE after modifying the values */
    if (i4RowMakeActive == TRUE)
    {
        pCapwapFsCapwapWhiteListEntry->MibObject.i4FsCapwapWhiteListRowStatus =
            ACTIVE;
    }

    if (CapwapUtilUpdateFsCapwapWhiteListTable
        (pCapwapOldFsCapwapWhiteListEntry, pCapwapFsCapwapWhiteListEntry,
         pCapwapIsSetFsCapwapWhiteListEntry) != OSIX_SUCCESS)
    {

        if (CapwapSetAllFsCapwapWhiteListTableTrigger
            (pCapwapSetFsCapwapWhiteListEntry,
             pCapwapIsSetFsCapwapWhiteListEntry, SNMP_FAILURE) != OSIX_SUCCESS)

        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCapwapWhiteListTable: CapwapSetAllFsCapwapWhiteListTableTrigger function returns failure.\r\n");

        }
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapSetAllFsCapwapWhiteListTable: CapwapUtilUpdateFsCapwapWhiteListTable function returns failure.\r\n");
        /*Restore back with previous values */
        MEMCPY (pCapwapFsCapwapWhiteListEntry, pCapwapOldFsCapwapWhiteListEntry,
                sizeof (tCapwapFsCapwapWhiteListEntry));
        MemReleaseMemBlock (CAPWAP_FSCAPWAPWHITELISTTABLE_POOLID,
                            (UINT1 *) pCapwapOldFsCapwapWhiteListEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPWHITELISTTABLE_POOLID,
                            (UINT1 *) pCapwapTrgFsCapwapWhiteListEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPWHITELISTTABLE_ISSET_POOLID,
                            (UINT1 *) pCapwapTrgIsSetFsCapwapWhiteListEntry);
        return OSIX_FAILURE;

    }
    if (CapwapSetAllFsCapwapWhiteListTableTrigger
        (pCapwapSetFsCapwapWhiteListEntry, pCapwapIsSetFsCapwapWhiteListEntry,
         SNMP_SUCCESS) != OSIX_SUCCESS)
    {

        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapSetAllFsCapwapWhiteListTable: CapwapSetAllFsCapwapWhiteListTableTrigger function returns failure.\r\n");
    }

    MemReleaseMemBlock (CAPWAP_FSCAPWAPWHITELISTTABLE_POOLID,
                        (UINT1 *) pCapwapOldFsCapwapWhiteListEntry);
    MemReleaseMemBlock (CAPWAP_FSCAPWAPWHITELISTTABLE_POOLID,
                        (UINT1 *) pCapwapTrgFsCapwapWhiteListEntry);
    MemReleaseMemBlock (CAPWAP_FSCAPWAPWHITELISTTABLE_ISSET_POOLID,
                        (UINT1 *) pCapwapTrgIsSetFsCapwapWhiteListEntry);
    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  CapwapSetAllFsCapwapBlackList
 Input       :  pCapwapSetFsCapwapBlackListEntry
                pCapwapIsSetFsCapwapBlackListEntry
                i4RowStatusLogic
                i4RowCreateOption
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapSetAllFsCapwapBlackList (tCapwapFsCapwapBlackListEntry *
                               pCapwapSetFsCapwapBlackListEntry,
                               tCapwapIsSetFsCapwapBlackListEntry *
                               pCapwapIsSetFsCapwapBlackListEntry,
                               INT4 i4RowStatusLogic, INT4 i4RowCreateOption)
{
    tCapwapFsCapwapBlackListEntry *pCapwapFsCapwapBlackListEntry = NULL;
    tCapwapFsCapwapBlackListEntry *pCapwapOldFsCapwapBlackListEntry = NULL;
    tCapwapFsCapwapBlackListEntry *pCapwapTrgFsCapwapBlackListEntry = NULL;
    tCapwapIsSetFsCapwapBlackListEntry *pCapwapTrgIsSetFsCapwapBlackListEntry =
        NULL;
    INT4                i4RowMakeActive = FALSE;

    pCapwapOldFsCapwapBlackListEntry =
        (tCapwapFsCapwapBlackListEntry *)
        MemAllocMemBlk (CAPWAP_FSCAPWAPBLACKLIST_POOLID);
    if (pCapwapOldFsCapwapBlackListEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    pCapwapTrgFsCapwapBlackListEntry =
        (tCapwapFsCapwapBlackListEntry *)
        MemAllocMemBlk (CAPWAP_FSCAPWAPBLACKLIST_POOLID);
    if (pCapwapTrgFsCapwapBlackListEntry == NULL)
    {
        MemReleaseMemBlock (CAPWAP_FSCAPWAPBLACKLIST_POOLID,
                            (UINT1 *) pCapwapOldFsCapwapBlackListEntry);
        return OSIX_FAILURE;
    }
    pCapwapTrgIsSetFsCapwapBlackListEntry =
        (tCapwapIsSetFsCapwapBlackListEntry *)
        MemAllocMemBlk (CAPWAP_FSCAPWAPBLACKLIST_ISSET_POOLID);
    if (pCapwapTrgIsSetFsCapwapBlackListEntry == NULL)
    {
        MemReleaseMemBlock (CAPWAP_FSCAPWAPBLACKLIST_POOLID,
                            (UINT1 *) pCapwapOldFsCapwapBlackListEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPBLACKLIST_POOLID,
                            (UINT1 *) pCapwapTrgFsCapwapBlackListEntry);
        return OSIX_FAILURE;
    }
    MEMSET (pCapwapOldFsCapwapBlackListEntry, 0,
            sizeof (tCapwapFsCapwapBlackListEntry));
    MEMSET (pCapwapTrgFsCapwapBlackListEntry, 0,
            sizeof (tCapwapFsCapwapBlackListEntry));
    MEMSET (pCapwapTrgIsSetFsCapwapBlackListEntry, 0,
            sizeof (tCapwapIsSetFsCapwapBlackListEntry));

    /* Check whether the node is already present */
    pCapwapFsCapwapBlackListEntry =
        RBTreeGet (gCapwapGlobals.CapwapGlbMib.FsCapwapBlackList,
                   (tRBElem *) pCapwapSetFsCapwapBlackListEntry);

    if (pCapwapFsCapwapBlackListEntry == NULL)
    {
        /* Create the node if the RowStatus given is CREATE_AND_WAIT or CREATE_AND_GO */
        if ((pCapwapSetFsCapwapBlackListEntry->MibObject.
             i4FsCapwapBlackListRowStatus == CREATE_AND_WAIT)
            || (pCapwapSetFsCapwapBlackListEntry->MibObject.
                i4FsCapwapBlackListRowStatus == CREATE_AND_GO)
            ||
            ((pCapwapSetFsCapwapBlackListEntry->MibObject.
              i4FsCapwapBlackListRowStatus == ACTIVE)
             && (i4RowCreateOption == 1)))
        {
            /* Allocate memory for the new node */
            pCapwapFsCapwapBlackListEntry =
                (tCapwapFsCapwapBlackListEntry *)
                MemAllocMemBlk (CAPWAP_FSCAPWAPBLACKLIST_POOLID);
            if (pCapwapFsCapwapBlackListEntry == NULL)
            {
                if (CapwapSetAllFsCapwapBlackListTrigger
                    (pCapwapSetFsCapwapBlackListEntry,
                     pCapwapIsSetFsCapwapBlackListEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCapwapBlackList:CapwapSetAllFsCapwapBlackListTrigger function fails\r\n");

                }
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCapwapBlackList: Fail to Allocate Memory.\r\n");
                MemReleaseMemBlock (CAPWAP_FSCAPWAPBLACKLIST_POOLID,
                                    (UINT1 *) pCapwapOldFsCapwapBlackListEntry);
                MemReleaseMemBlock (CAPWAP_FSCAPWAPBLACKLIST_POOLID,
                                    (UINT1 *) pCapwapTrgFsCapwapBlackListEntry);
                MemReleaseMemBlock (CAPWAP_FSCAPWAPBLACKLIST_ISSET_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgIsSetFsCapwapBlackListEntry);
                return OSIX_FAILURE;
            }

            MEMSET (pCapwapFsCapwapBlackListEntry, 0,
                    sizeof (tCapwapFsCapwapBlackListEntry));
            if ((CapwapInitializeFsCapwapBlackList
                 (pCapwapFsCapwapBlackListEntry)) == OSIX_FAILURE)
            {
                if (CapwapSetAllFsCapwapBlackListTrigger
                    (pCapwapSetFsCapwapBlackListEntry,
                     pCapwapIsSetFsCapwapBlackListEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCapwapBlackList:CapwapSetAllFsCapwapBlackListTrigger function fails\r\n");

                }
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCapwapBlackList: Fail to Initialize the Objects.\r\n");

                MemReleaseMemBlock (CAPWAP_FSCAPWAPBLACKLIST_POOLID,
                                    (UINT1 *) pCapwapFsCapwapBlackListEntry);
                MemReleaseMemBlock (CAPWAP_FSCAPWAPBLACKLIST_POOLID,
                                    (UINT1 *) pCapwapOldFsCapwapBlackListEntry);
                MemReleaseMemBlock (CAPWAP_FSCAPWAPBLACKLIST_POOLID,
                                    (UINT1 *) pCapwapTrgFsCapwapBlackListEntry);
                MemReleaseMemBlock (CAPWAP_FSCAPWAPBLACKLIST_ISSET_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgIsSetFsCapwapBlackListEntry);
                return OSIX_FAILURE;
            }
            /* Assign values for the new node */
            if (pCapwapIsSetFsCapwapBlackListEntry->bFsCapwapBlackListId !=
                OSIX_FALSE)
            {
                pCapwapFsCapwapBlackListEntry->MibObject.u4FsCapwapBlackListId =
                    pCapwapSetFsCapwapBlackListEntry->MibObject.
                    u4FsCapwapBlackListId;
            }

            if (pCapwapIsSetFsCapwapBlackListEntry->
                bFsCapwapBlackListWtpBaseMac != OSIX_FALSE)
            {
                MEMCPY (pCapwapFsCapwapBlackListEntry->MibObject.
                        au1FsCapwapBlackListWtpBaseMac,
                        pCapwapSetFsCapwapBlackListEntry->MibObject.
                        au1FsCapwapBlackListWtpBaseMac,
                        pCapwapSetFsCapwapBlackListEntry->MibObject.
                        i4FsCapwapBlackListWtpBaseMacLen);

                pCapwapFsCapwapBlackListEntry->MibObject.
                    i4FsCapwapBlackListWtpBaseMacLen =
                    pCapwapSetFsCapwapBlackListEntry->MibObject.
                    i4FsCapwapBlackListWtpBaseMacLen;
            }

            if (pCapwapIsSetFsCapwapBlackListEntry->
                bFsCapwapBlackListRowStatus != OSIX_FALSE)
            {
                pCapwapFsCapwapBlackListEntry->MibObject.
                    i4FsCapwapBlackListRowStatus =
                    pCapwapSetFsCapwapBlackListEntry->MibObject.
                    i4FsCapwapBlackListRowStatus;
            }

            if ((pCapwapSetFsCapwapBlackListEntry->MibObject.
                 i4FsCapwapBlackListRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pCapwapSetFsCapwapBlackListEntry->MibObject.
                        i4FsCapwapBlackListRowStatus == ACTIVE)))
            {
                pCapwapFsCapwapBlackListEntry->MibObject.
                    i4FsCapwapBlackListRowStatus = ACTIVE;
            }
            else if (pCapwapSetFsCapwapBlackListEntry->MibObject.
                     i4FsCapwapBlackListRowStatus == CREATE_AND_WAIT)
            {
                pCapwapFsCapwapBlackListEntry->MibObject.
                    i4FsCapwapBlackListRowStatus = NOT_READY;
            }

            /* Add the new node to the database */
            if (RBTreeAdd
                (gCapwapGlobals.CapwapGlbMib.FsCapwapBlackList,
                 (tRBElem *) pCapwapFsCapwapBlackListEntry) != RB_SUCCESS)
            {
                if (CapwapSetAllFsCapwapBlackListTrigger
                    (pCapwapSetFsCapwapBlackListEntry,
                     pCapwapIsSetFsCapwapBlackListEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCapwapBlackList: CapwapSetAllFsCapwapBlackListTrigger function returns failure.\r\n");
                }
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCapwapBlackList: RBTreeAdd is failed.\r\n");

                MemReleaseMemBlock (CAPWAP_FSCAPWAPBLACKLIST_POOLID,
                                    (UINT1 *) pCapwapFsCapwapBlackListEntry);
                MemReleaseMemBlock (CAPWAP_FSCAPWAPBLACKLIST_POOLID,
                                    (UINT1 *) pCapwapOldFsCapwapBlackListEntry);
                MemReleaseMemBlock (CAPWAP_FSCAPWAPBLACKLIST_POOLID,
                                    (UINT1 *) pCapwapTrgFsCapwapBlackListEntry);
                MemReleaseMemBlock (CAPWAP_FSCAPWAPBLACKLIST_ISSET_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgIsSetFsCapwapBlackListEntry);
                return OSIX_FAILURE;
            }
            if (CapwapUtilUpdateFsCapwapBlackList
                (NULL, pCapwapFsCapwapBlackListEntry,
                 pCapwapIsSetFsCapwapBlackListEntry) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCapwapBlackList: CapwapUtilUpdateFsCapwapBlackList function returns failure.\r\n");

                if (CapwapSetAllFsCapwapBlackListTrigger
                    (pCapwapSetFsCapwapBlackListEntry,
                     pCapwapIsSetFsCapwapBlackListEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCapwapBlackList: CapwapSetAllFsCapwapBlackListTrigger function returns failure.\r\n");

                }
                RBTreeRem (gCapwapGlobals.CapwapGlbMib.FsCapwapBlackList,
                           pCapwapFsCapwapBlackListEntry);
                MemReleaseMemBlock (CAPWAP_FSCAPWAPBLACKLIST_POOLID,
                                    (UINT1 *) pCapwapFsCapwapBlackListEntry);
                MemReleaseMemBlock (CAPWAP_FSCAPWAPBLACKLIST_POOLID,
                                    (UINT1 *) pCapwapOldFsCapwapBlackListEntry);
                MemReleaseMemBlock (CAPWAP_FSCAPWAPBLACKLIST_POOLID,
                                    (UINT1 *) pCapwapTrgFsCapwapBlackListEntry);
                MemReleaseMemBlock (CAPWAP_FSCAPWAPBLACKLIST_ISSET_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgIsSetFsCapwapBlackListEntry);
                return OSIX_FAILURE;
            }

            if ((pCapwapSetFsCapwapBlackListEntry->MibObject.
                 i4FsCapwapBlackListRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pCapwapSetFsCapwapBlackListEntry->MibObject.
                        i4FsCapwapBlackListRowStatus == ACTIVE)))
            {

                /* For MSR and RM Trigger */
                pCapwapTrgFsCapwapBlackListEntry->MibObject.
                    u4FsCapwapBlackListId =
                    pCapwapSetFsCapwapBlackListEntry->MibObject.
                    u4FsCapwapBlackListId;
                pCapwapTrgFsCapwapBlackListEntry->MibObject.
                    i4FsCapwapBlackListRowStatus = CREATE_AND_WAIT;
                pCapwapTrgIsSetFsCapwapBlackListEntry->
                    bFsCapwapBlackListRowStatus = OSIX_TRUE;

                if (CapwapSetAllFsCapwapBlackListTrigger
                    (pCapwapTrgFsCapwapBlackListEntry,
                     pCapwapTrgIsSetFsCapwapBlackListEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCapwapBlackList: CapwapSetAllFsCapwapBlackListTrigger function returns failure.\r\n");

                    MemReleaseMemBlock (CAPWAP_FSCAPWAPBLACKLIST_POOLID,
                                        (UINT1 *)
                                        pCapwapFsCapwapBlackListEntry);
                    MemReleaseMemBlock (CAPWAP_FSCAPWAPBLACKLIST_POOLID,
                                        (UINT1 *)
                                        pCapwapOldFsCapwapBlackListEntry);
                    MemReleaseMemBlock (CAPWAP_FSCAPWAPBLACKLIST_POOLID,
                                        (UINT1 *)
                                        pCapwapTrgFsCapwapBlackListEntry);
                    MemReleaseMemBlock (CAPWAP_FSCAPWAPBLACKLIST_ISSET_POOLID,
                                        (UINT1 *)
                                        pCapwapTrgIsSetFsCapwapBlackListEntry);
                    return OSIX_FAILURE;
                }
            }
            else if (pCapwapSetFsCapwapBlackListEntry->MibObject.
                     i4FsCapwapBlackListRowStatus == CREATE_AND_WAIT)
            {

                /* For MSR and RM Trigger */
                pCapwapTrgFsCapwapBlackListEntry->MibObject.
                    i4FsCapwapBlackListRowStatus = CREATE_AND_WAIT;
                pCapwapTrgIsSetFsCapwapBlackListEntry->
                    bFsCapwapBlackListRowStatus = OSIX_TRUE;

                if (CapwapSetAllFsCapwapBlackListTrigger
                    (pCapwapTrgFsCapwapBlackListEntry,
                     pCapwapTrgIsSetFsCapwapBlackListEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCapwapBlackList: CapwapSetAllFsCapwapBlackListTrigger function returns failure.\r\n");

                    MemReleaseMemBlock (CAPWAP_FSCAPWAPBLACKLIST_POOLID,
                                        (UINT1 *)
                                        pCapwapFsCapwapBlackListEntry);
                    MemReleaseMemBlock (CAPWAP_FSCAPWAPBLACKLIST_POOLID,
                                        (UINT1 *)
                                        pCapwapOldFsCapwapBlackListEntry);
                    MemReleaseMemBlock (CAPWAP_FSCAPWAPBLACKLIST_POOLID,
                                        (UINT1 *)
                                        pCapwapTrgFsCapwapBlackListEntry);
                    MemReleaseMemBlock (CAPWAP_FSCAPWAPBLACKLIST_ISSET_POOLID,
                                        (UINT1 *)
                                        pCapwapTrgIsSetFsCapwapBlackListEntry);
                    return OSIX_FAILURE;
                }
            }

            if (pCapwapSetFsCapwapBlackListEntry->MibObject.
                i4FsCapwapBlackListRowStatus == CREATE_AND_GO)
            {
                pCapwapSetFsCapwapBlackListEntry->MibObject.
                    i4FsCapwapBlackListRowStatus = ACTIVE;
            }

            if (CapwapSetAllFsCapwapBlackListTrigger
                (pCapwapSetFsCapwapBlackListEntry,
                 pCapwapIsSetFsCapwapBlackListEntry,
                 SNMP_SUCCESS) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCapwapBlackList:  CapwapSetAllFsCapwapBlackListTrigger function returns failure.\r\n");
            }
            MemReleaseMemBlock (CAPWAP_FSCAPWAPBLACKLIST_POOLID,
                                (UINT1 *) pCapwapOldFsCapwapBlackListEntry);
            MemReleaseMemBlock (CAPWAP_FSCAPWAPBLACKLIST_POOLID,
                                (UINT1 *) pCapwapTrgFsCapwapBlackListEntry);
            MemReleaseMemBlock (CAPWAP_FSCAPWAPBLACKLIST_ISSET_POOLID,
                                (UINT1 *)
                                pCapwapTrgIsSetFsCapwapBlackListEntry);
            return OSIX_SUCCESS;

        }
        else
        {
            if (CapwapSetAllFsCapwapBlackListTrigger
                (pCapwapSetFsCapwapBlackListEntry,
                 pCapwapIsSetFsCapwapBlackListEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCapwapBlackList: CapwapSetAllFsCapwapBlackListTrigger function returns failure.\r\n");
            }
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCapwapBlackList: Failure.\r\n");
            MemReleaseMemBlock (CAPWAP_FSCAPWAPBLACKLIST_POOLID,
                                (UINT1 *) pCapwapOldFsCapwapBlackListEntry);
            MemReleaseMemBlock (CAPWAP_FSCAPWAPBLACKLIST_POOLID,
                                (UINT1 *) pCapwapTrgFsCapwapBlackListEntry);
            MemReleaseMemBlock (CAPWAP_FSCAPWAPBLACKLIST_ISSET_POOLID,
                                (UINT1 *)
                                pCapwapTrgIsSetFsCapwapBlackListEntry);
            return OSIX_FAILURE;
        }
    }
    else if ((pCapwapSetFsCapwapBlackListEntry->MibObject.
              i4FsCapwapBlackListRowStatus == CREATE_AND_WAIT)
             || (pCapwapSetFsCapwapBlackListEntry->MibObject.
                 i4FsCapwapBlackListRowStatus == CREATE_AND_GO))
    {
        if (CapwapSetAllFsCapwapBlackListTrigger
            (pCapwapSetFsCapwapBlackListEntry,
             pCapwapIsSetFsCapwapBlackListEntry, SNMP_FAILURE) != OSIX_SUCCESS)

        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCapwapBlackList: CapwapSetAllFsCapwapBlackListTrigger function returns failure.\r\n");
        }
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapSetAllFsCapwapBlackList: The row is already present.\r\n");
        MemReleaseMemBlock (CAPWAP_FSCAPWAPBLACKLIST_POOLID,
                            (UINT1 *) pCapwapOldFsCapwapBlackListEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPBLACKLIST_POOLID,
                            (UINT1 *) pCapwapTrgFsCapwapBlackListEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPBLACKLIST_ISSET_POOLID,
                            (UINT1 *) pCapwapTrgIsSetFsCapwapBlackListEntry);
        return OSIX_FAILURE;
    }
    /* Copy the previous values before setting the new values */
    MEMCPY (pCapwapOldFsCapwapBlackListEntry, pCapwapFsCapwapBlackListEntry,
            sizeof (tCapwapFsCapwapBlackListEntry));

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pCapwapSetFsCapwapBlackListEntry->MibObject.
        i4FsCapwapBlackListRowStatus == DESTROY)
    {
        pCapwapFsCapwapBlackListEntry->MibObject.i4FsCapwapBlackListRowStatus =
            DESTROY;

        if (CapwapUtilUpdateFsCapwapBlackList (pCapwapOldFsCapwapBlackListEntry,
                                               pCapwapFsCapwapBlackListEntry,
                                               pCapwapIsSetFsCapwapBlackListEntry)
            != OSIX_SUCCESS)
        {

            if (CapwapSetAllFsCapwapBlackListTrigger
                (pCapwapSetFsCapwapBlackListEntry,
                 pCapwapIsSetFsCapwapBlackListEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCapwapBlackList: CapwapSetAllFsCapwapBlackListTrigger function returns failure.\r\n");
            }
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCapwapBlackList: CapwapUtilUpdateFsCapwapBlackList function returns failure.\r\n");
        }
        RBTreeRem (gCapwapGlobals.CapwapGlbMib.FsCapwapBlackList,
                   pCapwapFsCapwapBlackListEntry);
        if (CapwapSetAllFsCapwapBlackListTrigger
            (pCapwapSetFsCapwapBlackListEntry,
             pCapwapIsSetFsCapwapBlackListEntry, SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCapwapBlackList: CapwapSetAllFsCapwapBlackListTrigger function returns failure.\r\n");
            MemReleaseMemBlock (CAPWAP_FSCAPWAPBLACKLIST_POOLID,
                                (UINT1 *) pCapwapOldFsCapwapBlackListEntry);
            MemReleaseMemBlock (CAPWAP_FSCAPWAPBLACKLIST_POOLID,
                                (UINT1 *) pCapwapTrgFsCapwapBlackListEntry);
            MemReleaseMemBlock (CAPWAP_FSCAPWAPBLACKLIST_ISSET_POOLID,
                                (UINT1 *)
                                pCapwapTrgIsSetFsCapwapBlackListEntry);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (CAPWAP_FSCAPWAPBLACKLIST_POOLID,
                            (UINT1 *) pCapwapFsCapwapBlackListEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPBLACKLIST_POOLID,
                            (UINT1 *) pCapwapOldFsCapwapBlackListEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPBLACKLIST_POOLID,
                            (UINT1 *) pCapwapTrgFsCapwapBlackListEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPBLACKLIST_ISSET_POOLID,
                            (UINT1 *) pCapwapTrgIsSetFsCapwapBlackListEntry);
        return OSIX_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (FsCapwapBlackListFilterInputs
        (pCapwapFsCapwapBlackListEntry, pCapwapSetFsCapwapBlackListEntry,
         pCapwapIsSetFsCapwapBlackListEntry) != OSIX_TRUE)
    {
        MemReleaseMemBlock (CAPWAP_FSCAPWAPBLACKLIST_POOLID,
                            (UINT1 *) pCapwapOldFsCapwapBlackListEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPBLACKLIST_POOLID,
                            (UINT1 *) pCapwapTrgFsCapwapBlackListEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPBLACKLIST_ISSET_POOLID,
                            (UINT1 *) pCapwapTrgIsSetFsCapwapBlackListEntry);
        return OSIX_SUCCESS;
    }

    /*This condtion is to make the row NOT_IN_SERVICE before modifying the values */
    if ((i4RowStatusLogic == TRUE) &&
        (pCapwapFsCapwapBlackListEntry->MibObject.
         i4FsCapwapBlackListRowStatus == ACTIVE)
        && (pCapwapSetFsCapwapBlackListEntry->MibObject.
            i4FsCapwapBlackListRowStatus != NOT_IN_SERVICE))
    {
        pCapwapFsCapwapBlackListEntry->MibObject.i4FsCapwapBlackListRowStatus =
            NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        pCapwapTrgFsCapwapBlackListEntry->MibObject.
            i4FsCapwapBlackListRowStatus = NOT_IN_SERVICE;
        pCapwapTrgIsSetFsCapwapBlackListEntry->bFsCapwapBlackListRowStatus =
            OSIX_TRUE;

        if (CapwapUtilUpdateFsCapwapBlackList (pCapwapOldFsCapwapBlackListEntry,
                                               pCapwapFsCapwapBlackListEntry,
                                               pCapwapIsSetFsCapwapBlackListEntry)
            != OSIX_SUCCESS)
        {
            /*Restore back with previous values */
            MEMCPY (pCapwapFsCapwapBlackListEntry,
                    pCapwapOldFsCapwapBlackListEntry,
                    sizeof (tCapwapFsCapwapBlackListEntry));
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCapwapBlackList:                 CapwapUtilUpdateFsCapwapBlackList Function returns failure.\r\n");

            if (CapwapSetAllFsCapwapBlackListTrigger
                (pCapwapSetFsCapwapBlackListEntry,
                 pCapwapIsSetFsCapwapBlackListEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCapwapBlackList: CapwapSetAllFsCapwapBlackListTrigger function returns failure.\r\n");
            }
            MemReleaseMemBlock (CAPWAP_FSCAPWAPBLACKLIST_POOLID,
                                (UINT1 *) pCapwapOldFsCapwapBlackListEntry);
            MemReleaseMemBlock (CAPWAP_FSCAPWAPBLACKLIST_POOLID,
                                (UINT1 *) pCapwapTrgFsCapwapBlackListEntry);
            MemReleaseMemBlock (CAPWAP_FSCAPWAPBLACKLIST_ISSET_POOLID,
                                (UINT1 *)
                                pCapwapTrgIsSetFsCapwapBlackListEntry);
            return OSIX_FAILURE;
        }

        if (CapwapSetAllFsCapwapBlackListTrigger
            (pCapwapTrgFsCapwapBlackListEntry,
             pCapwapTrgIsSetFsCapwapBlackListEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCapwapBlackList: CapwapSetAllFsCapwapBlackListTrigger function returns failure.\r\n");
        }
    }

    if (pCapwapSetFsCapwapBlackListEntry->MibObject.
        i4FsCapwapBlackListRowStatus == ACTIVE)
    {
        i4RowMakeActive = TRUE;
    }

    /* Assign values for the existing node */
    if (pCapwapIsSetFsCapwapBlackListEntry->bFsCapwapBlackListWtpBaseMac !=
        OSIX_FALSE)
    {
        MEMCPY (pCapwapFsCapwapBlackListEntry->MibObject.
                au1FsCapwapBlackListWtpBaseMac,
                pCapwapSetFsCapwapBlackListEntry->MibObject.
                au1FsCapwapBlackListWtpBaseMac,
                pCapwapSetFsCapwapBlackListEntry->MibObject.
                i4FsCapwapBlackListWtpBaseMacLen);

        pCapwapFsCapwapBlackListEntry->MibObject.
            i4FsCapwapBlackListWtpBaseMacLen =
            pCapwapSetFsCapwapBlackListEntry->MibObject.
            i4FsCapwapBlackListWtpBaseMacLen;
    }
    if (pCapwapIsSetFsCapwapBlackListEntry->bFsCapwapBlackListRowStatus !=
        OSIX_FALSE)
    {
        pCapwapFsCapwapBlackListEntry->MibObject.i4FsCapwapBlackListRowStatus =
            pCapwapSetFsCapwapBlackListEntry->MibObject.
            i4FsCapwapBlackListRowStatus;
    }

    /*This condtion is to make the row back to ACTIVE after modifying the values */
    if (i4RowMakeActive == TRUE)
    {
        pCapwapFsCapwapBlackListEntry->MibObject.i4FsCapwapBlackListRowStatus =
            ACTIVE;
    }

    if (CapwapUtilUpdateFsCapwapBlackList (pCapwapOldFsCapwapBlackListEntry,
                                           pCapwapFsCapwapBlackListEntry,
                                           pCapwapIsSetFsCapwapBlackListEntry)
        != OSIX_SUCCESS)
    {

        if (CapwapSetAllFsCapwapBlackListTrigger
            (pCapwapSetFsCapwapBlackListEntry,
             pCapwapIsSetFsCapwapBlackListEntry, SNMP_FAILURE) != OSIX_SUCCESS)

        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCapwapBlackList: CapwapSetAllFsCapwapBlackListTrigger function returns failure.\r\n");

        }
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapSetAllFsCapwapBlackList: CapwapUtilUpdateFsCapwapBlackList function returns failure.\r\n");
        /*Restore back with previous values */
        MEMCPY (pCapwapFsCapwapBlackListEntry, pCapwapOldFsCapwapBlackListEntry,
                sizeof (tCapwapFsCapwapBlackListEntry));
        MemReleaseMemBlock (CAPWAP_FSCAPWAPBLACKLIST_POOLID,
                            (UINT1 *) pCapwapOldFsCapwapBlackListEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPBLACKLIST_POOLID,
                            (UINT1 *) pCapwapTrgFsCapwapBlackListEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPBLACKLIST_ISSET_POOLID,
                            (UINT1 *) pCapwapTrgIsSetFsCapwapBlackListEntry);
        return OSIX_FAILURE;

    }
    if (CapwapSetAllFsCapwapBlackListTrigger (pCapwapSetFsCapwapBlackListEntry,
                                              pCapwapIsSetFsCapwapBlackListEntry,
                                              SNMP_SUCCESS) != OSIX_SUCCESS)
    {

        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapSetAllFsCapwapBlackList: CapwapSetAllFsCapwapBlackListTrigger function returns failure.\r\n");
    }

    MemReleaseMemBlock (CAPWAP_FSCAPWAPBLACKLIST_POOLID,
                        (UINT1 *) pCapwapOldFsCapwapBlackListEntry);
    MemReleaseMemBlock (CAPWAP_FSCAPWAPBLACKLIST_POOLID,
                        (UINT1 *) pCapwapTrgFsCapwapBlackListEntry);
    MemReleaseMemBlock (CAPWAP_FSCAPWAPBLACKLIST_ISSET_POOLID,
                        (UINT1 *) pCapwapTrgIsSetFsCapwapBlackListEntry);
    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  CapwapSetAllFsCapwapWtpConfigTable
 Input       :  pCapwapSetFsCapwapWtpConfigEntry
                pCapwapIsSetFsCapwapWtpConfigEntry
                i4RowStatusLogic
                i4RowCreateOption
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapSetAllFsCapwapWtpConfigTable (tCapwapFsCapwapWtpConfigEntry *
                                    pCapwapSetFsCapwapWtpConfigEntry,
                                    tCapwapIsSetFsCapwapWtpConfigEntry *
                                    pCapwapIsSetFsCapwapWtpConfigEntry,
                                    INT4 i4RowStatusLogic,
                                    INT4 i4RowCreateOption)
{
    tCapwapFsCapwapWtpConfigEntry *pCapwapFsCapwapWtpConfigEntry = NULL;
    tCapwapFsCapwapWtpConfigEntry *pCapwapOldFsCapwapWtpConfigEntry = NULL;
    tCapwapFsCapwapWtpConfigEntry *pCapwapTrgFsCapwapWtpConfigEntry = NULL;
    tCapwapIsSetFsCapwapWtpConfigEntry *pCapwapTrgIsSetFsCapwapWtpConfigEntry =
        NULL;
    INT4                i4RowMakeActive = FALSE;

    pCapwapOldFsCapwapWtpConfigEntry =
        (tCapwapFsCapwapWtpConfigEntry *)
        MemAllocMemBlk (CAPWAP_FSCAPWAPWTPCONFIGTABLE_POOLID);
    if (pCapwapOldFsCapwapWtpConfigEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    pCapwapTrgFsCapwapWtpConfigEntry =
        (tCapwapFsCapwapWtpConfigEntry *)
        MemAllocMemBlk (CAPWAP_FSCAPWAPWTPCONFIGTABLE_POOLID);
    if (pCapwapTrgFsCapwapWtpConfigEntry == NULL)
    {
        MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPCONFIGTABLE_POOLID,
                            (UINT1 *) pCapwapOldFsCapwapWtpConfigEntry);
        return OSIX_FAILURE;
    }
    pCapwapTrgIsSetFsCapwapWtpConfigEntry =
        (tCapwapIsSetFsCapwapWtpConfigEntry *)
        MemAllocMemBlk (CAPWAP_FSCAPWAPWTPCONFIGTABLE_ISSET_POOLID);
    if (pCapwapTrgIsSetFsCapwapWtpConfigEntry == NULL)
    {
        MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPCONFIGTABLE_POOLID,
                            (UINT1 *) pCapwapOldFsCapwapWtpConfigEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPCONFIGTABLE_POOLID,
                            (UINT1 *) pCapwapTrgFsCapwapWtpConfigEntry);
        return OSIX_FAILURE;
    }
    MEMSET (pCapwapOldFsCapwapWtpConfigEntry, 0,
            sizeof (tCapwapFsCapwapWtpConfigEntry));
    MEMSET (pCapwapTrgFsCapwapWtpConfigEntry, 0,
            sizeof (tCapwapFsCapwapWtpConfigEntry));
    MEMSET (pCapwapTrgIsSetFsCapwapWtpConfigEntry, 0,
            sizeof (tCapwapIsSetFsCapwapWtpConfigEntry));

    /* Check whether the node is already present */
    pCapwapFsCapwapWtpConfigEntry =
        RBTreeGet (gCapwapGlobals.CapwapGlbMib.FsCapwapWtpConfigTable,
                   (tRBElem *) pCapwapSetFsCapwapWtpConfigEntry);

    if (pCapwapFsCapwapWtpConfigEntry == NULL)
    {
        /* Create the node if the RowStatus given is CREATE_AND_WAIT or CREATE_AND_GO */
        if ((pCapwapSetFsCapwapWtpConfigEntry->MibObject.
             i4FsCapwapWtpConfigRowStatus == CREATE_AND_WAIT)
            || (pCapwapSetFsCapwapWtpConfigEntry->MibObject.
                i4FsCapwapWtpConfigRowStatus == CREATE_AND_GO)
            ||
            ((pCapwapSetFsCapwapWtpConfigEntry->MibObject.
              i4FsCapwapWtpConfigRowStatus == ACTIVE)
             && (i4RowCreateOption == 1)))
        {
            /* Allocate memory for the new node */
            pCapwapFsCapwapWtpConfigEntry =
                (tCapwapFsCapwapWtpConfigEntry *)
                MemAllocMemBlk (CAPWAP_FSCAPWAPWTPCONFIGTABLE_POOLID);
            if (pCapwapFsCapwapWtpConfigEntry == NULL)
            {
                if (CapwapSetAllFsCapwapWtpConfigTableTrigger
                    (pCapwapSetFsCapwapWtpConfigEntry,
                     pCapwapIsSetFsCapwapWtpConfigEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCapwapWtpConfigTable:CapwapSetAllFsCapwapWtpConfigTableTrigger function fails\r\n");

                }
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCapwapWtpConfigTable: Fail to Allocate Memory.\r\n");
                MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPCONFIGTABLE_POOLID,
                                    (UINT1 *) pCapwapOldFsCapwapWtpConfigEntry);
                MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPCONFIGTABLE_POOLID,
                                    (UINT1 *) pCapwapTrgFsCapwapWtpConfigEntry);
                MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPCONFIGTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgIsSetFsCapwapWtpConfigEntry);
                return OSIX_FAILURE;
            }

            MEMSET (pCapwapFsCapwapWtpConfigEntry, 0,
                    sizeof (tCapwapFsCapwapWtpConfigEntry));
            if ((CapwapInitializeFsCapwapWtpConfigTable
                 (pCapwapFsCapwapWtpConfigEntry)) == OSIX_FAILURE)
            {
                if (CapwapSetAllFsCapwapWtpConfigTableTrigger
                    (pCapwapSetFsCapwapWtpConfigEntry,
                     pCapwapIsSetFsCapwapWtpConfigEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCapwapWtpConfigTable:CapwapSetAllFsCapwapWtpConfigTableTrigger function fails\r\n");

                }
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCapwapWtpConfigTable: Fail to Initialize the Objects.\r\n");

                MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPCONFIGTABLE_POOLID,
                                    (UINT1 *) pCapwapFsCapwapWtpConfigEntry);
                MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPCONFIGTABLE_POOLID,
                                    (UINT1 *) pCapwapOldFsCapwapWtpConfigEntry);
                MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPCONFIGTABLE_POOLID,
                                    (UINT1 *) pCapwapTrgFsCapwapWtpConfigEntry);
                MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPCONFIGTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgIsSetFsCapwapWtpConfigEntry);
                return OSIX_FAILURE;
            }
            /* Assign values for the new node */
            if (pCapwapIsSetFsCapwapWtpConfigEntry->bFsCapwapWtpReset !=
                OSIX_FALSE)
            {
                pCapwapFsCapwapWtpConfigEntry->MibObject.i4FsCapwapWtpReset =
                    pCapwapSetFsCapwapWtpConfigEntry->MibObject.
                    i4FsCapwapWtpReset;
            }

            if (pCapwapIsSetFsCapwapWtpConfigEntry->bFsCapwapClearConfig !=
                OSIX_FALSE)
            {
                pCapwapFsCapwapWtpConfigEntry->MibObject.i4FsCapwapClearConfig =
                    pCapwapSetFsCapwapWtpConfigEntry->MibObject.
                    i4FsCapwapClearConfig;
            }

            if (pCapwapIsSetFsCapwapWtpConfigEntry->bFsWtpDiscoveryType !=
                OSIX_FALSE)
            {
                pCapwapFsCapwapWtpConfigEntry->MibObject.i4FsWtpDiscoveryType =
                    pCapwapSetFsCapwapWtpConfigEntry->MibObject.
                    i4FsWtpDiscoveryType;
            }

            if (pCapwapIsSetFsCapwapWtpConfigEntry->bFsWtpCountryString !=
                OSIX_FALSE)
            {
                MEMCPY (pCapwapFsCapwapWtpConfigEntry->MibObject.
                        au1FsWtpCountryString,
                        pCapwapSetFsCapwapWtpConfigEntry->MibObject.
                        au1FsWtpCountryString,
                        pCapwapSetFsCapwapWtpConfigEntry->MibObject.
                        i4FsWtpCountryStringLen);

                pCapwapFsCapwapWtpConfigEntry->MibObject.
                    i4FsWtpCountryStringLen =
                    pCapwapSetFsCapwapWtpConfigEntry->MibObject.
                    i4FsWtpCountryStringLen;
            }

            if (pCapwapIsSetFsCapwapWtpConfigEntry->bFsWtpCrashDumpFileName !=
                OSIX_FALSE)
            {
                MEMCPY (pCapwapFsCapwapWtpConfigEntry->MibObject.
                        au1FsWtpCrashDumpFileName,
                        pCapwapSetFsCapwapWtpConfigEntry->MibObject.
                        au1FsWtpCrashDumpFileName,
                        pCapwapSetFsCapwapWtpConfigEntry->MibObject.
                        i4FsWtpCrashDumpFileNameLen);

                pCapwapFsCapwapWtpConfigEntry->MibObject.
                    i4FsWtpCrashDumpFileNameLen =
                    pCapwapSetFsCapwapWtpConfigEntry->MibObject.
                    i4FsWtpCrashDumpFileNameLen;
            }

            if (pCapwapIsSetFsCapwapWtpConfigEntry->bFsWtpMemoryDumpFileName !=
                OSIX_FALSE)
            {
                MEMCPY (pCapwapFsCapwapWtpConfigEntry->MibObject.
                        au1FsWtpMemoryDumpFileName,
                        pCapwapSetFsCapwapWtpConfigEntry->MibObject.
                        au1FsWtpMemoryDumpFileName,
                        pCapwapSetFsCapwapWtpConfigEntry->MibObject.
                        i4FsWtpMemoryDumpFileNameLen);

                pCapwapFsCapwapWtpConfigEntry->MibObject.
                    i4FsWtpMemoryDumpFileNameLen =
                    pCapwapSetFsCapwapWtpConfigEntry->MibObject.
                    i4FsWtpMemoryDumpFileNameLen;
            }

            if (pCapwapIsSetFsCapwapWtpConfigEntry->bFsWlcStaticIpAddress !=
                OSIX_FALSE)
            {
                pCapwapFsCapwapWtpConfigEntry->MibObject.
                    u4FsWlcStaticIpAddress =
                    pCapwapSetFsCapwapWtpConfigEntry->MibObject.
                    u4FsWlcStaticIpAddress;
            }
            if (pCapwapIsSetFsCapwapWtpConfigEntry->bFsWtpDeleteOperation !=
                OSIX_FALSE)
            {
                pCapwapFsCapwapWtpConfigEntry->MibObject.
                    u4FsWtpDeleteOperation =
                    pCapwapSetFsCapwapWtpConfigEntry->MibObject.
                    u4FsWtpDeleteOperation;
            }
            if (pCapwapIsSetFsCapwapWtpConfigEntry->bFsCapwapClearApStats !=
                OSIX_FALSE)
            {
                pCapwapFsCapwapWtpConfigEntry->MibObject.
                    i4FsCapwapClearApStats =
                    pCapwapSetFsCapwapWtpConfigEntry->MibObject.
                    i4FsCapwapClearApStats;
            }

            if (pCapwapIsSetFsCapwapWtpConfigEntry->
                bFsCapwapWtpConfigRowStatus != OSIX_FALSE)
            {
                pCapwapFsCapwapWtpConfigEntry->MibObject.
                    i4FsCapwapWtpConfigRowStatus =
                    pCapwapSetFsCapwapWtpConfigEntry->MibObject.
                    i4FsCapwapWtpConfigRowStatus;
            }

            if (pCapwapIsSetFsCapwapWtpConfigEntry->bCapwapBaseWtpProfileId !=
                OSIX_FALSE)
            {
                pCapwapFsCapwapWtpConfigEntry->MibObject.
                    u4CapwapBaseWtpProfileId =
                    pCapwapSetFsCapwapWtpConfigEntry->MibObject.
                    u4CapwapBaseWtpProfileId;
            }

            if ((pCapwapSetFsCapwapWtpConfigEntry->MibObject.
                 i4FsCapwapWtpConfigRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pCapwapSetFsCapwapWtpConfigEntry->MibObject.
                        i4FsCapwapWtpConfigRowStatus == ACTIVE)))
            {
                pCapwapFsCapwapWtpConfigEntry->MibObject.
                    i4FsCapwapWtpConfigRowStatus = ACTIVE;
            }
            else if (pCapwapSetFsCapwapWtpConfigEntry->MibObject.
                     i4FsCapwapWtpConfigRowStatus == CREATE_AND_WAIT)
            {
                pCapwapFsCapwapWtpConfigEntry->MibObject.
                    i4FsCapwapWtpConfigRowStatus = NOT_READY;
            }

            /* Add the new node to the database */
            if (RBTreeAdd
                (gCapwapGlobals.CapwapGlbMib.FsCapwapWtpConfigTable,
                 (tRBElem *) pCapwapFsCapwapWtpConfigEntry) != RB_SUCCESS)
            {
                if (CapwapSetAllFsCapwapWtpConfigTableTrigger
                    (pCapwapSetFsCapwapWtpConfigEntry,
                     pCapwapIsSetFsCapwapWtpConfigEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCapwapWtpConfigTable: CapwapSetAllFsCapwapWtpConfigTableTrigger function returns failure.\r\n");
                }
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCapwapWtpConfigTable: RBTreeAdd is failed.\r\n");

                MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPCONFIGTABLE_POOLID,
                                    (UINT1 *) pCapwapFsCapwapWtpConfigEntry);
                MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPCONFIGTABLE_POOLID,
                                    (UINT1 *) pCapwapOldFsCapwapWtpConfigEntry);
                MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPCONFIGTABLE_POOLID,
                                    (UINT1 *) pCapwapTrgFsCapwapWtpConfigEntry);
                MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPCONFIGTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgIsSetFsCapwapWtpConfigEntry);
                return OSIX_FAILURE;
            }
            if (CapwapUtilUpdateFsCapwapWtpConfigTable
                (NULL, pCapwapFsCapwapWtpConfigEntry,
                 pCapwapIsSetFsCapwapWtpConfigEntry) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCapwapWtpConfigTable: CapwapUtilUpdateFsCapwapWtpConfigTable function returns failure.\r\n");

                if (CapwapSetAllFsCapwapWtpConfigTableTrigger
                    (pCapwapSetFsCapwapWtpConfigEntry,
                     pCapwapIsSetFsCapwapWtpConfigEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCapwapWtpConfigTable: CapwapSetAllFsCapwapWtpConfigTableTrigger function returns failure.\r\n");

                }
                RBTreeRem (gCapwapGlobals.CapwapGlbMib.FsCapwapWtpConfigTable,
                           pCapwapFsCapwapWtpConfigEntry);
                MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPCONFIGTABLE_POOLID,
                                    (UINT1 *) pCapwapFsCapwapWtpConfigEntry);
                MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPCONFIGTABLE_POOLID,
                                    (UINT1 *) pCapwapOldFsCapwapWtpConfigEntry);
                MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPCONFIGTABLE_POOLID,
                                    (UINT1 *) pCapwapTrgFsCapwapWtpConfigEntry);
                MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPCONFIGTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgIsSetFsCapwapWtpConfigEntry);
                return OSIX_FAILURE;
            }

            if ((pCapwapSetFsCapwapWtpConfigEntry->MibObject.
                 i4FsCapwapWtpConfigRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pCapwapSetFsCapwapWtpConfigEntry->MibObject.
                        i4FsCapwapWtpConfigRowStatus == ACTIVE)))
            {

                /* For MSR and RM Trigger */
                pCapwapTrgFsCapwapWtpConfigEntry->MibObject.
                    u4CapwapBaseWtpProfileId =
                    pCapwapSetFsCapwapWtpConfigEntry->MibObject.
                    u4CapwapBaseWtpProfileId;
                pCapwapTrgFsCapwapWtpConfigEntry->MibObject.
                    i4FsCapwapWtpConfigRowStatus = CREATE_AND_WAIT;
                pCapwapTrgIsSetFsCapwapWtpConfigEntry->
                    bFsCapwapWtpConfigRowStatus = OSIX_TRUE;

                if (CapwapSetAllFsCapwapWtpConfigTableTrigger
                    (pCapwapTrgFsCapwapWtpConfigEntry,
                     pCapwapTrgIsSetFsCapwapWtpConfigEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCapwapWtpConfigTable: CapwapSetAllFsCapwapWtpConfigTableTrigger function returns failure.\r\n");

                    MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPCONFIGTABLE_POOLID,
                                        (UINT1 *)
                                        pCapwapFsCapwapWtpConfigEntry);
                    MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPCONFIGTABLE_POOLID,
                                        (UINT1 *)
                                        pCapwapOldFsCapwapWtpConfigEntry);
                    MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPCONFIGTABLE_POOLID,
                                        (UINT1 *)
                                        pCapwapTrgFsCapwapWtpConfigEntry);
                    MemReleaseMemBlock
                        (CAPWAP_FSCAPWAPWTPCONFIGTABLE_ISSET_POOLID,
                         (UINT1 *) pCapwapTrgIsSetFsCapwapWtpConfigEntry);
                    return OSIX_FAILURE;
                }
            }
            else if (pCapwapSetFsCapwapWtpConfigEntry->MibObject.
                     i4FsCapwapWtpConfigRowStatus == CREATE_AND_WAIT)
            {

                /* For MSR and RM Trigger */
                pCapwapTrgFsCapwapWtpConfigEntry->MibObject.
                    i4FsCapwapWtpConfigRowStatus = CREATE_AND_WAIT;
                pCapwapTrgIsSetFsCapwapWtpConfigEntry->
                    bFsCapwapWtpConfigRowStatus = OSIX_TRUE;

                if (CapwapSetAllFsCapwapWtpConfigTableTrigger
                    (pCapwapTrgFsCapwapWtpConfigEntry,
                     pCapwapTrgIsSetFsCapwapWtpConfigEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCapwapWtpConfigTable: CapwapSetAllFsCapwapWtpConfigTableTrigger function returns failure.\r\n");

                    MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPCONFIGTABLE_POOLID,
                                        (UINT1 *)
                                        pCapwapFsCapwapWtpConfigEntry);
                    MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPCONFIGTABLE_POOLID,
                                        (UINT1 *)
                                        pCapwapOldFsCapwapWtpConfigEntry);
                    MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPCONFIGTABLE_POOLID,
                                        (UINT1 *)
                                        pCapwapTrgFsCapwapWtpConfigEntry);
                    MemReleaseMemBlock
                        (CAPWAP_FSCAPWAPWTPCONFIGTABLE_ISSET_POOLID,
                         (UINT1 *) pCapwapTrgIsSetFsCapwapWtpConfigEntry);
                    return OSIX_FAILURE;
                }
            }

            if (pCapwapSetFsCapwapWtpConfigEntry->MibObject.
                i4FsCapwapWtpConfigRowStatus == CREATE_AND_GO)
            {
                pCapwapSetFsCapwapWtpConfigEntry->MibObject.
                    i4FsCapwapWtpConfigRowStatus = ACTIVE;
            }

            if (CapwapSetAllFsCapwapWtpConfigTableTrigger
                (pCapwapSetFsCapwapWtpConfigEntry,
                 pCapwapIsSetFsCapwapWtpConfigEntry,
                 SNMP_SUCCESS) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCapwapWtpConfigTable:  CapwapSetAllFsCapwapWtpConfigTableTrigger function returns failure.\r\n");
            }
            MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPCONFIGTABLE_POOLID,
                                (UINT1 *) pCapwapOldFsCapwapWtpConfigEntry);
            MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPCONFIGTABLE_POOLID,
                                (UINT1 *) pCapwapTrgFsCapwapWtpConfigEntry);
            MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPCONFIGTABLE_ISSET_POOLID,
                                (UINT1 *)
                                pCapwapTrgIsSetFsCapwapWtpConfigEntry);
            return OSIX_SUCCESS;

        }
        else
        {
            if (CapwapSetAllFsCapwapWtpConfigTableTrigger
                (pCapwapSetFsCapwapWtpConfigEntry,
                 pCapwapIsSetFsCapwapWtpConfigEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCapwapWtpConfigTable: CapwapSetAllFsCapwapWtpConfigTableTrigger function returns failure.\r\n");
            }
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCapwapWtpConfigTable: Failure.\r\n");
            MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPCONFIGTABLE_POOLID,
                                (UINT1 *) pCapwapOldFsCapwapWtpConfigEntry);
            MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPCONFIGTABLE_POOLID,
                                (UINT1 *) pCapwapTrgFsCapwapWtpConfigEntry);
            MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPCONFIGTABLE_ISSET_POOLID,
                                (UINT1 *)
                                pCapwapTrgIsSetFsCapwapWtpConfigEntry);
            return OSIX_FAILURE;
        }
    }
    else if ((pCapwapSetFsCapwapWtpConfigEntry->MibObject.
              i4FsCapwapWtpConfigRowStatus == CREATE_AND_WAIT)
             || (pCapwapSetFsCapwapWtpConfigEntry->MibObject.
                 i4FsCapwapWtpConfigRowStatus == CREATE_AND_GO))
    {
        if (CapwapSetAllFsCapwapWtpConfigTableTrigger
            (pCapwapSetFsCapwapWtpConfigEntry,
             pCapwapIsSetFsCapwapWtpConfigEntry, SNMP_FAILURE) != OSIX_SUCCESS)

        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCapwapWtpConfigTable: CapwapSetAllFsCapwapWtpConfigTableTrigger function returns failure.\r\n");
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCapwapWtpConfigTable: The row is already present.\r\n");
            MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPCONFIGTABLE_POOLID,
                                (UINT1 *) pCapwapOldFsCapwapWtpConfigEntry);
            MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPCONFIGTABLE_POOLID,
                                (UINT1 *) pCapwapTrgFsCapwapWtpConfigEntry);
            MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPCONFIGTABLE_ISSET_POOLID,
                                (UINT1 *)
                                pCapwapTrgIsSetFsCapwapWtpConfigEntry);
            return OSIX_FAILURE;
        }
    }
    /* Copy the previous values before setting the new values */
    MEMCPY (pCapwapOldFsCapwapWtpConfigEntry, pCapwapFsCapwapWtpConfigEntry,
            sizeof (tCapwapFsCapwapWtpConfigEntry));

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pCapwapSetFsCapwapWtpConfigEntry->MibObject.
        i4FsCapwapWtpConfigRowStatus == DESTROY)
    {
        pCapwapFsCapwapWtpConfigEntry->MibObject.i4FsCapwapWtpConfigRowStatus =
            DESTROY;

        if (CapwapUtilUpdateFsCapwapWtpConfigTable
            (pCapwapOldFsCapwapWtpConfigEntry, pCapwapFsCapwapWtpConfigEntry,
             pCapwapIsSetFsCapwapWtpConfigEntry) != OSIX_SUCCESS)
        {

            if (CapwapSetAllFsCapwapWtpConfigTableTrigger
                (pCapwapSetFsCapwapWtpConfigEntry,
                 pCapwapIsSetFsCapwapWtpConfigEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCapwapWtpConfigTable: CapwapSetAllFsCapwapWtpConfigTableTrigger function returns failure.\r\n");
            }
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCapwapWtpConfigTable: CapwapUtilUpdateFsCapwapWtpConfigTable function returns failure.\r\n");
        }
        RBTreeRem (gCapwapGlobals.CapwapGlbMib.FsCapwapWtpConfigTable,
                   pCapwapFsCapwapWtpConfigEntry);
        if (CapwapSetAllFsCapwapWtpConfigTableTrigger
            (pCapwapSetFsCapwapWtpConfigEntry,
             pCapwapIsSetFsCapwapWtpConfigEntry, SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCapwapWtpConfigTable: CapwapSetAllFsCapwapWtpConfigTableTrigger function returns failure.\r\n");
            MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPCONFIGTABLE_POOLID,
                                (UINT1 *) pCapwapOldFsCapwapWtpConfigEntry);
            MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPCONFIGTABLE_POOLID,
                                (UINT1 *) pCapwapTrgFsCapwapWtpConfigEntry);
            MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPCONFIGTABLE_ISSET_POOLID,
                                (UINT1 *)
                                pCapwapTrgIsSetFsCapwapWtpConfigEntry);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPCONFIGTABLE_POOLID,
                            (UINT1 *) pCapwapFsCapwapWtpConfigEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPCONFIGTABLE_POOLID,
                            (UINT1 *) pCapwapOldFsCapwapWtpConfigEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPCONFIGTABLE_POOLID,
                            (UINT1 *) pCapwapTrgFsCapwapWtpConfigEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPCONFIGTABLE_ISSET_POOLID,
                            (UINT1 *) pCapwapTrgIsSetFsCapwapWtpConfigEntry);
        return OSIX_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (FsCapwapWtpConfigTableFilterInputs
        (pCapwapFsCapwapWtpConfigEntry, pCapwapSetFsCapwapWtpConfigEntry,
         pCapwapIsSetFsCapwapWtpConfigEntry) != OSIX_TRUE)
    {
        MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPCONFIGTABLE_POOLID,
                            (UINT1 *) pCapwapOldFsCapwapWtpConfigEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPCONFIGTABLE_POOLID,
                            (UINT1 *) pCapwapTrgFsCapwapWtpConfigEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPCONFIGTABLE_ISSET_POOLID,
                            (UINT1 *) pCapwapTrgIsSetFsCapwapWtpConfigEntry);
        return OSIX_SUCCESS;
    }

    /*This condtion is to make the row NOT_IN_SERVICE before modifying the values */
    if ((i4RowStatusLogic == TRUE) &&
        (pCapwapFsCapwapWtpConfigEntry->MibObject.
         i4FsCapwapWtpConfigRowStatus == ACTIVE)
        && (pCapwapSetFsCapwapWtpConfigEntry->MibObject.
            i4FsCapwapWtpConfigRowStatus != NOT_IN_SERVICE))
    {
        pCapwapFsCapwapWtpConfigEntry->MibObject.i4FsCapwapWtpConfigRowStatus =
            NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        pCapwapTrgFsCapwapWtpConfigEntry->MibObject.
            i4FsCapwapWtpConfigRowStatus = NOT_IN_SERVICE;
        pCapwapTrgIsSetFsCapwapWtpConfigEntry->bFsCapwapWtpConfigRowStatus =
            OSIX_TRUE;

        if (CapwapUtilUpdateFsCapwapWtpConfigTable
            (pCapwapOldFsCapwapWtpConfigEntry, pCapwapFsCapwapWtpConfigEntry,
             pCapwapIsSetFsCapwapWtpConfigEntry) != OSIX_SUCCESS)
        {
            /*Restore back with previous values */
            MEMCPY (pCapwapFsCapwapWtpConfigEntry,
                    pCapwapOldFsCapwapWtpConfigEntry,
                    sizeof (tCapwapFsCapwapWtpConfigEntry));
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCapwapWtpConfigTable:                 CapwapUtilUpdateFsCapwapWtpConfigTable Function returns failure.\r\n");

            if (CapwapSetAllFsCapwapWtpConfigTableTrigger
                (pCapwapSetFsCapwapWtpConfigEntry,
                 pCapwapIsSetFsCapwapWtpConfigEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCapwapWtpConfigTable: CapwapSetAllFsCapwapWtpConfigTableTrigger function returns failure.\r\n");
            }
            MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPCONFIGTABLE_POOLID,
                                (UINT1 *) pCapwapOldFsCapwapWtpConfigEntry);
            MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPCONFIGTABLE_POOLID,
                                (UINT1 *) pCapwapTrgFsCapwapWtpConfigEntry);
            MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPCONFIGTABLE_ISSET_POOLID,
                                (UINT1 *)
                                pCapwapTrgIsSetFsCapwapWtpConfigEntry);
            return OSIX_FAILURE;
        }

        if (CapwapSetAllFsCapwapWtpConfigTableTrigger
            (pCapwapTrgFsCapwapWtpConfigEntry,
             pCapwapTrgIsSetFsCapwapWtpConfigEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCapwapWtpConfigTable: CapwapSetAllFsCapwapWtpConfigTableTrigger function returns failure.\r\n");
        }
    }

    if (pCapwapSetFsCapwapWtpConfigEntry->MibObject.
        i4FsCapwapWtpConfigRowStatus == ACTIVE)
    {
        i4RowMakeActive = TRUE;
    }

    /* Assign values for the existing node */
    if (pCapwapIsSetFsCapwapWtpConfigEntry->bFsCapwapWtpReset != OSIX_FALSE)
    {
        pCapwapFsCapwapWtpConfigEntry->MibObject.i4FsCapwapWtpReset =
            pCapwapSetFsCapwapWtpConfigEntry->MibObject.i4FsCapwapWtpReset;
    }
    if (pCapwapIsSetFsCapwapWtpConfigEntry->bFsCapwapClearConfig != OSIX_FALSE)
    {
        pCapwapFsCapwapWtpConfigEntry->MibObject.i4FsCapwapClearConfig =
            pCapwapSetFsCapwapWtpConfigEntry->MibObject.i4FsCapwapClearConfig;
    }
    if (pCapwapIsSetFsCapwapWtpConfigEntry->bFsWtpDiscoveryType != OSIX_FALSE)
    {
        pCapwapFsCapwapWtpConfigEntry->MibObject.i4FsWtpDiscoveryType =
            pCapwapSetFsCapwapWtpConfigEntry->MibObject.i4FsWtpDiscoveryType;
    }
    if (pCapwapIsSetFsCapwapWtpConfigEntry->bFsWtpCountryString != OSIX_FALSE)
    {
        MEMCPY (pCapwapFsCapwapWtpConfigEntry->MibObject.au1FsWtpCountryString,
                pCapwapSetFsCapwapWtpConfigEntry->MibObject.
                au1FsWtpCountryString,
                pCapwapSetFsCapwapWtpConfigEntry->MibObject.
                i4FsWtpCountryStringLen);

        pCapwapFsCapwapWtpConfigEntry->MibObject.i4FsWtpCountryStringLen =
            pCapwapSetFsCapwapWtpConfigEntry->MibObject.i4FsWtpCountryStringLen;
    }
    if (pCapwapIsSetFsCapwapWtpConfigEntry->bFsWtpCrashDumpFileName !=
        OSIX_FALSE)
    {
        MEMCPY (pCapwapFsCapwapWtpConfigEntry->MibObject.
                au1FsWtpCrashDumpFileName,
                pCapwapSetFsCapwapWtpConfigEntry->MibObject.
                au1FsWtpCrashDumpFileName,
                pCapwapSetFsCapwapWtpConfigEntry->MibObject.
                i4FsWtpCrashDumpFileNameLen);

        pCapwapFsCapwapWtpConfigEntry->MibObject.i4FsWtpCrashDumpFileNameLen =
            pCapwapSetFsCapwapWtpConfigEntry->MibObject.
            i4FsWtpCrashDumpFileNameLen;
    }
    if (pCapwapIsSetFsCapwapWtpConfigEntry->bFsWtpMemoryDumpFileName !=
        OSIX_FALSE)
    {
        MEMCPY (pCapwapFsCapwapWtpConfigEntry->MibObject.
                au1FsWtpMemoryDumpFileName,
                pCapwapSetFsCapwapWtpConfigEntry->MibObject.
                au1FsWtpMemoryDumpFileName,
                pCapwapSetFsCapwapWtpConfigEntry->MibObject.
                i4FsWtpMemoryDumpFileNameLen);

        pCapwapFsCapwapWtpConfigEntry->MibObject.i4FsWtpMemoryDumpFileNameLen =
            pCapwapSetFsCapwapWtpConfigEntry->MibObject.
            i4FsWtpMemoryDumpFileNameLen;
    }
    if (pCapwapIsSetFsCapwapWtpConfigEntry->bFsWtpDeleteOperation != OSIX_FALSE)
    {
        pCapwapFsCapwapWtpConfigEntry->MibObject.u4FsWtpDeleteOperation =
            pCapwapSetFsCapwapWtpConfigEntry->MibObject.u4FsWtpDeleteOperation;
    }
    if (pCapwapIsSetFsCapwapWtpConfigEntry->bFsCapwapClearApStats != OSIX_FALSE)
    {
        pCapwapFsCapwapWtpConfigEntry->MibObject.i4FsCapwapClearApStats =
            pCapwapSetFsCapwapWtpConfigEntry->MibObject.i4FsCapwapClearApStats;
    }

    if (pCapwapIsSetFsCapwapWtpConfigEntry->bFsCapwapWtpConfigRowStatus !=
        OSIX_FALSE)
    {
        pCapwapFsCapwapWtpConfigEntry->MibObject.i4FsCapwapWtpConfigRowStatus =
            pCapwapSetFsCapwapWtpConfigEntry->MibObject.
            i4FsCapwapWtpConfigRowStatus;
    }
    if (pCapwapIsSetFsCapwapWtpConfigEntry->bFsWlcStaticIpAddress != OSIX_FALSE)
    {
        pCapwapFsCapwapWtpConfigEntry->MibObject.u4FsWlcStaticIpAddress =
            pCapwapSetFsCapwapWtpConfigEntry->MibObject.u4FsWlcStaticIpAddress;
    }

    /*This condtion is to make the row back to ACTIVE after modifying the values */
    if (i4RowMakeActive == TRUE)
    {
        pCapwapFsCapwapWtpConfigEntry->MibObject.i4FsCapwapWtpConfigRowStatus =
            ACTIVE;
    }

    if (CapwapUtilUpdateFsCapwapWtpConfigTable
        (pCapwapOldFsCapwapWtpConfigEntry, pCapwapFsCapwapWtpConfigEntry,
         pCapwapIsSetFsCapwapWtpConfigEntry) != OSIX_SUCCESS)
    {

        if (CapwapSetAllFsCapwapWtpConfigTableTrigger
            (pCapwapSetFsCapwapWtpConfigEntry,
             pCapwapIsSetFsCapwapWtpConfigEntry, SNMP_FAILURE) != OSIX_SUCCESS)

        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCapwapWtpConfigTable: CapwapSetAllFsCapwapWtpConfigTableTrigger function returns failure.\r\n");

        }
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapSetAllFsCapwapWtpConfigTable: CapwapUtilUpdateFsCapwapWtpConfigTable function returns failure.\r\n");
        /*Restore back with previous values */
        MEMCPY (pCapwapFsCapwapWtpConfigEntry, pCapwapOldFsCapwapWtpConfigEntry,
                sizeof (tCapwapFsCapwapWtpConfigEntry));
        MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPCONFIGTABLE_POOLID,
                            (UINT1 *) pCapwapOldFsCapwapWtpConfigEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPCONFIGTABLE_POOLID,
                            (UINT1 *) pCapwapTrgFsCapwapWtpConfigEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPCONFIGTABLE_ISSET_POOLID,
                            (UINT1 *) pCapwapTrgIsSetFsCapwapWtpConfigEntry);
        return OSIX_FAILURE;

    }
    if (CapwapSetAllFsCapwapWtpConfigTableTrigger
        (pCapwapSetFsCapwapWtpConfigEntry, pCapwapIsSetFsCapwapWtpConfigEntry,
         SNMP_SUCCESS) != OSIX_SUCCESS)
    {

        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapSetAllFsCapwapWtpConfigTable: CapwapSetAllFsCapwapWtpConfigTableTrigger function returns failure.\r\n");
    }

    MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPCONFIGTABLE_POOLID,
                        (UINT1 *) pCapwapOldFsCapwapWtpConfigEntry);
    MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPCONFIGTABLE_POOLID,
                        (UINT1 *) pCapwapTrgFsCapwapWtpConfigEntry);
    MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPCONFIGTABLE_ISSET_POOLID,
                        (UINT1 *) pCapwapTrgIsSetFsCapwapWtpConfigEntry);
    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  CapwapSetAllFsCapwapLinkEncryptionTable
 Input       :  pCapwapSetFsCapwapLinkEncryptionEntry
                pCapwapIsSetFsCapwapLinkEncryptionEntry
                i4RowStatusLogic
                i4RowCreateOption
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapSetAllFsCapwapLinkEncryptionTable (tCapwapFsCapwapLinkEncryptionEntry *
                                         pCapwapSetFsCapwapLinkEncryptionEntry,
                                         tCapwapIsSetFsCapwapLinkEncryptionEntry
                                         *
                                         pCapwapIsSetFsCapwapLinkEncryptionEntry,
                                         INT4 i4RowStatusLogic,
                                         INT4 i4RowCreateOption)
{
    tCapwapFsCapwapLinkEncryptionEntry *pCapwapFsCapwapLinkEncryptionEntry =
        NULL;
    tCapwapFsCapwapLinkEncryptionEntry *pCapwapOldFsCapwapLinkEncryptionEntry =
        NULL;
    tCapwapFsCapwapLinkEncryptionEntry *pCapwapTrgFsCapwapLinkEncryptionEntry =
        NULL;
    tCapwapIsSetFsCapwapLinkEncryptionEntry
        * pCapwapTrgIsSetFsCapwapLinkEncryptionEntry = NULL;
    INT4                i4RowMakeActive = FALSE;

    pCapwapOldFsCapwapLinkEncryptionEntry =
        (tCapwapFsCapwapLinkEncryptionEntry *)
        MemAllocMemBlk (CAPWAP_FSCAPWAPLINKENCRYPTIONTABLE_POOLID);
    if (pCapwapOldFsCapwapLinkEncryptionEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    pCapwapTrgFsCapwapLinkEncryptionEntry =
        (tCapwapFsCapwapLinkEncryptionEntry *)
        MemAllocMemBlk (CAPWAP_FSCAPWAPLINKENCRYPTIONTABLE_POOLID);
    if (pCapwapTrgFsCapwapLinkEncryptionEntry == NULL)
    {
        MemReleaseMemBlock (CAPWAP_FSCAPWAPLINKENCRYPTIONTABLE_POOLID,
                            (UINT1 *) pCapwapOldFsCapwapLinkEncryptionEntry);
        return OSIX_FAILURE;
    }
    pCapwapTrgIsSetFsCapwapLinkEncryptionEntry =
        (tCapwapIsSetFsCapwapLinkEncryptionEntry *)
        MemAllocMemBlk (CAPWAP_FSCAPWAPLINKENCRYPTIONTABLE_ISSET_POOLID);
    if (pCapwapTrgIsSetFsCapwapLinkEncryptionEntry == NULL)
    {
        MemReleaseMemBlock (CAPWAP_FSCAPWAPLINKENCRYPTIONTABLE_POOLID,
                            (UINT1 *) pCapwapOldFsCapwapLinkEncryptionEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPLINKENCRYPTIONTABLE_POOLID,
                            (UINT1 *) pCapwapTrgFsCapwapLinkEncryptionEntry);
        return OSIX_FAILURE;
    }
    MEMSET (pCapwapOldFsCapwapLinkEncryptionEntry, 0,
            sizeof (tCapwapFsCapwapLinkEncryptionEntry));
    MEMSET (pCapwapTrgFsCapwapLinkEncryptionEntry, 0,
            sizeof (tCapwapFsCapwapLinkEncryptionEntry));
    MEMSET (pCapwapTrgIsSetFsCapwapLinkEncryptionEntry, 0,
            sizeof (tCapwapIsSetFsCapwapLinkEncryptionEntry));

    /* Check whether the node is already present */
    pCapwapFsCapwapLinkEncryptionEntry =
        RBTreeGet (gCapwapGlobals.CapwapGlbMib.FsCapwapLinkEncryptionTable,
                   (tRBElem *) pCapwapSetFsCapwapLinkEncryptionEntry);

    if (pCapwapFsCapwapLinkEncryptionEntry == NULL)
    {
        /* Create the node if the RowStatus given is CREATE_AND_WAIT or CREATE_AND_GO */
        if ((pCapwapSetFsCapwapLinkEncryptionEntry->MibObject.
             i4FsCapwapEncryptChannelRowStatus == CREATE_AND_WAIT)
            || (pCapwapSetFsCapwapLinkEncryptionEntry->MibObject.
                i4FsCapwapEncryptChannelRowStatus == CREATE_AND_GO)
            ||
            ((pCapwapSetFsCapwapLinkEncryptionEntry->MibObject.
              i4FsCapwapEncryptChannelRowStatus == ACTIVE)
             && (i4RowCreateOption == 1)))
        {
            /* Allocate memory for the new node */
            pCapwapFsCapwapLinkEncryptionEntry =
                (tCapwapFsCapwapLinkEncryptionEntry *)
                MemAllocMemBlk (CAPWAP_FSCAPWAPLINKENCRYPTIONTABLE_POOLID);
            if (pCapwapFsCapwapLinkEncryptionEntry == NULL)
            {
                if (CapwapSetAllFsCapwapLinkEncryptionTableTrigger
                    (pCapwapSetFsCapwapLinkEncryptionEntry,
                     pCapwapIsSetFsCapwapLinkEncryptionEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCapwapLinkEncryptionTable:CapwapSetAllFsCapwapLinkEncryptionTableTrigger function fails\r\n");

                }
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCapwapLinkEncryptionTable: Fail to Allocate Memory.\r\n");
                MemReleaseMemBlock (CAPWAP_FSCAPWAPLINKENCRYPTIONTABLE_POOLID,
                                    (UINT1 *)
                                    pCapwapOldFsCapwapLinkEncryptionEntry);
                MemReleaseMemBlock (CAPWAP_FSCAPWAPLINKENCRYPTIONTABLE_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgFsCapwapLinkEncryptionEntry);
                MemReleaseMemBlock
                    (CAPWAP_FSCAPWAPLINKENCRYPTIONTABLE_ISSET_POOLID,
                     (UINT1 *) pCapwapTrgIsSetFsCapwapLinkEncryptionEntry);
                return OSIX_FAILURE;
            }

            MEMSET (pCapwapFsCapwapLinkEncryptionEntry, 0,
                    sizeof (tCapwapFsCapwapLinkEncryptionEntry));
            if ((CapwapInitializeFsCapwapLinkEncryptionTable
                 (pCapwapFsCapwapLinkEncryptionEntry)) == OSIX_FAILURE)
            {
                if (CapwapSetAllFsCapwapLinkEncryptionTableTrigger
                    (pCapwapSetFsCapwapLinkEncryptionEntry,
                     pCapwapIsSetFsCapwapLinkEncryptionEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCapwapLinkEncryptionTable:CapwapSetAllFsCapwapLinkEncryptionTableTrigger function fails\r\n");

                }
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCapwapLinkEncryptionTable: Fail to Initialize the Objects.\r\n");

                MemReleaseMemBlock (CAPWAP_FSCAPWAPLINKENCRYPTIONTABLE_POOLID,
                                    (UINT1 *)
                                    pCapwapFsCapwapLinkEncryptionEntry);
                MemReleaseMemBlock (CAPWAP_FSCAPWAPLINKENCRYPTIONTABLE_POOLID,
                                    (UINT1 *)
                                    pCapwapOldFsCapwapLinkEncryptionEntry);
                MemReleaseMemBlock (CAPWAP_FSCAPWAPLINKENCRYPTIONTABLE_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgFsCapwapLinkEncryptionEntry);
                MemReleaseMemBlock
                    (CAPWAP_FSCAPWAPLINKENCRYPTIONTABLE_ISSET_POOLID,
                     (UINT1 *) pCapwapTrgIsSetFsCapwapLinkEncryptionEntry);
                return OSIX_FAILURE;
            }
            /* Assign values for the new node */
            if (pCapwapIsSetFsCapwapLinkEncryptionEntry->
                bFsCapwapEncryptChannel != OSIX_FALSE)
            {
                pCapwapFsCapwapLinkEncryptionEntry->MibObject.
                    i4FsCapwapEncryptChannel =
                    pCapwapSetFsCapwapLinkEncryptionEntry->MibObject.
                    i4FsCapwapEncryptChannel;
            }

            if (pCapwapIsSetFsCapwapLinkEncryptionEntry->
                bFsCapwapEncryptChannelStatus != OSIX_FALSE)
            {
                pCapwapFsCapwapLinkEncryptionEntry->MibObject.
                    i4FsCapwapEncryptChannelStatus =
                    pCapwapSetFsCapwapLinkEncryptionEntry->MibObject.
                    i4FsCapwapEncryptChannelStatus;
            }

            if (pCapwapIsSetFsCapwapLinkEncryptionEntry->
                bFsCapwapEncryptChannelRowStatus != OSIX_FALSE)
            {
                pCapwapFsCapwapLinkEncryptionEntry->MibObject.
                    i4FsCapwapEncryptChannelRowStatus =
                    pCapwapSetFsCapwapLinkEncryptionEntry->MibObject.
                    i4FsCapwapEncryptChannelRowStatus;
            }

            if (pCapwapIsSetFsCapwapLinkEncryptionEntry->
                bCapwapBaseWtpProfileId != OSIX_FALSE)
            {
                pCapwapFsCapwapLinkEncryptionEntry->MibObject.
                    u4CapwapBaseWtpProfileId =
                    pCapwapSetFsCapwapLinkEncryptionEntry->MibObject.
                    u4CapwapBaseWtpProfileId;
            }

            if ((pCapwapSetFsCapwapLinkEncryptionEntry->MibObject.
                 i4FsCapwapEncryptChannelRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pCapwapSetFsCapwapLinkEncryptionEntry->MibObject.
                        i4FsCapwapEncryptChannelRowStatus == ACTIVE)))
            {
                pCapwapFsCapwapLinkEncryptionEntry->MibObject.
                    i4FsCapwapEncryptChannelRowStatus = ACTIVE;
            }
            else if (pCapwapSetFsCapwapLinkEncryptionEntry->MibObject.
                     i4FsCapwapEncryptChannelRowStatus == CREATE_AND_WAIT)
            {
                pCapwapFsCapwapLinkEncryptionEntry->MibObject.
                    i4FsCapwapEncryptChannelRowStatus = NOT_READY;
            }

            /* Add the new node to the database */
            if (RBTreeAdd
                (gCapwapGlobals.CapwapGlbMib.FsCapwapLinkEncryptionTable,
                 (tRBElem *) pCapwapFsCapwapLinkEncryptionEntry) != RB_SUCCESS)
            {
                if (CapwapSetAllFsCapwapLinkEncryptionTableTrigger
                    (pCapwapSetFsCapwapLinkEncryptionEntry,
                     pCapwapIsSetFsCapwapLinkEncryptionEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCapwapLinkEncryptionTable: CapwapSetAllFsCapwapLinkEncryptionTableTrigger function returns failure.\r\n");
                }
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCapwapLinkEncryptionTable: RBTreeAdd is failed.\r\n");

                MemReleaseMemBlock (CAPWAP_FSCAPWAPLINKENCRYPTIONTABLE_POOLID,
                                    (UINT1 *)
                                    pCapwapFsCapwapLinkEncryptionEntry);
                MemReleaseMemBlock (CAPWAP_FSCAPWAPLINKENCRYPTIONTABLE_POOLID,
                                    (UINT1 *)
                                    pCapwapOldFsCapwapLinkEncryptionEntry);
                MemReleaseMemBlock (CAPWAP_FSCAPWAPLINKENCRYPTIONTABLE_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgFsCapwapLinkEncryptionEntry);
                MemReleaseMemBlock
                    (CAPWAP_FSCAPWAPLINKENCRYPTIONTABLE_ISSET_POOLID,
                     (UINT1 *) pCapwapTrgIsSetFsCapwapLinkEncryptionEntry);
                return OSIX_FAILURE;
            }
            if (CapwapUtilUpdateFsCapwapLinkEncryptionTable
                (NULL, pCapwapFsCapwapLinkEncryptionEntry,
                 pCapwapIsSetFsCapwapLinkEncryptionEntry) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCapwapLinkEncryptionTable: CapwapUtilUpdateFsCapwapLinkEncryptionTable function returns failure.\r\n");

                if (CapwapSetAllFsCapwapLinkEncryptionTableTrigger
                    (pCapwapSetFsCapwapLinkEncryptionEntry,
                     pCapwapIsSetFsCapwapLinkEncryptionEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCapwapLinkEncryptionTable: CapwapSetAllFsCapwapLinkEncryptionTableTrigger function returns failure.\r\n");

                }
                RBTreeRem (gCapwapGlobals.CapwapGlbMib.
                           FsCapwapLinkEncryptionTable,
                           pCapwapFsCapwapLinkEncryptionEntry);
                MemReleaseMemBlock (CAPWAP_FSCAPWAPLINKENCRYPTIONTABLE_POOLID,
                                    (UINT1 *)
                                    pCapwapFsCapwapLinkEncryptionEntry);
                MemReleaseMemBlock (CAPWAP_FSCAPWAPLINKENCRYPTIONTABLE_POOLID,
                                    (UINT1 *)
                                    pCapwapOldFsCapwapLinkEncryptionEntry);
                MemReleaseMemBlock (CAPWAP_FSCAPWAPLINKENCRYPTIONTABLE_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgFsCapwapLinkEncryptionEntry);
                MemReleaseMemBlock
                    (CAPWAP_FSCAPWAPLINKENCRYPTIONTABLE_ISSET_POOLID,
                     (UINT1 *) pCapwapTrgIsSetFsCapwapLinkEncryptionEntry);
                return OSIX_FAILURE;
            }

            if ((pCapwapSetFsCapwapLinkEncryptionEntry->MibObject.
                 i4FsCapwapEncryptChannelRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pCapwapSetFsCapwapLinkEncryptionEntry->MibObject.
                        i4FsCapwapEncryptChannelRowStatus == ACTIVE)))
            {

                /* For MSR and RM Trigger */
                pCapwapTrgFsCapwapLinkEncryptionEntry->MibObject.
                    i4FsCapwapEncryptChannel =
                    pCapwapSetFsCapwapLinkEncryptionEntry->MibObject.
                    i4FsCapwapEncryptChannel;

                /* Coverity Fix Start 9-7-13 *//* Self Assignment ( No Effect ) - Jai */
                /* pCapwapTrgFsCapwapLinkEncryptionEntry->MibObject.u4CapwapBaseWtpProfileId = pCapwapTrgFsCapwapLinkEncryptionEntry->MibObject.u4CapwapBaseWtpProfileId; */
                /* Coverity Fix End 9-7-13 */

                pCapwapTrgFsCapwapLinkEncryptionEntry->MibObject.
                    i4FsCapwapEncryptChannelRowStatus = CREATE_AND_WAIT;
                pCapwapTrgIsSetFsCapwapLinkEncryptionEntry->
                    bFsCapwapEncryptChannelRowStatus = OSIX_TRUE;

                if (CapwapSetAllFsCapwapLinkEncryptionTableTrigger
                    (pCapwapTrgFsCapwapLinkEncryptionEntry,
                     pCapwapTrgIsSetFsCapwapLinkEncryptionEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCapwapLinkEncryptionTable: CapwapSetAllFsCapwapLinkEncryptionTableTrigger function returns failure.\r\n");

                    MemReleaseMemBlock
                        (CAPWAP_FSCAPWAPLINKENCRYPTIONTABLE_POOLID,
                         (UINT1 *) pCapwapFsCapwapLinkEncryptionEntry);
                    MemReleaseMemBlock
                        (CAPWAP_FSCAPWAPLINKENCRYPTIONTABLE_POOLID,
                         (UINT1 *) pCapwapOldFsCapwapLinkEncryptionEntry);
                    MemReleaseMemBlock
                        (CAPWAP_FSCAPWAPLINKENCRYPTIONTABLE_POOLID,
                         (UINT1 *) pCapwapTrgFsCapwapLinkEncryptionEntry);
                    MemReleaseMemBlock
                        (CAPWAP_FSCAPWAPLINKENCRYPTIONTABLE_ISSET_POOLID,
                         (UINT1 *) pCapwapTrgIsSetFsCapwapLinkEncryptionEntry);
                    return OSIX_FAILURE;
                }
            }
            else if (pCapwapSetFsCapwapLinkEncryptionEntry->MibObject.
                     i4FsCapwapEncryptChannelRowStatus == CREATE_AND_WAIT)
            {

                /* For MSR and RM Trigger */
                pCapwapTrgFsCapwapLinkEncryptionEntry->MibObject.
                    i4FsCapwapEncryptChannelRowStatus = CREATE_AND_WAIT;
                pCapwapTrgIsSetFsCapwapLinkEncryptionEntry->
                    bFsCapwapEncryptChannelRowStatus = OSIX_TRUE;

                if (CapwapSetAllFsCapwapLinkEncryptionTableTrigger
                    (pCapwapTrgFsCapwapLinkEncryptionEntry,
                     pCapwapTrgIsSetFsCapwapLinkEncryptionEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCapwapLinkEncryptionTable: CapwapSetAllFsCapwapLinkEncryptionTableTrigger function returns failure.\r\n");

                    MemReleaseMemBlock
                        (CAPWAP_FSCAPWAPLINKENCRYPTIONTABLE_POOLID,
                         (UINT1 *) pCapwapFsCapwapLinkEncryptionEntry);
                    MemReleaseMemBlock
                        (CAPWAP_FSCAPWAPLINKENCRYPTIONTABLE_POOLID,
                         (UINT1 *) pCapwapOldFsCapwapLinkEncryptionEntry);
                    MemReleaseMemBlock
                        (CAPWAP_FSCAPWAPLINKENCRYPTIONTABLE_POOLID,
                         (UINT1 *) pCapwapTrgFsCapwapLinkEncryptionEntry);
                    MemReleaseMemBlock
                        (CAPWAP_FSCAPWAPLINKENCRYPTIONTABLE_ISSET_POOLID,
                         (UINT1 *) pCapwapTrgIsSetFsCapwapLinkEncryptionEntry);
                    return OSIX_FAILURE;
                }
            }

            if (pCapwapSetFsCapwapLinkEncryptionEntry->MibObject.
                i4FsCapwapEncryptChannelRowStatus == CREATE_AND_GO)
            {
                pCapwapSetFsCapwapLinkEncryptionEntry->MibObject.
                    i4FsCapwapEncryptChannelRowStatus = ACTIVE;
            }

            if (CapwapSetAllFsCapwapLinkEncryptionTableTrigger
                (pCapwapSetFsCapwapLinkEncryptionEntry,
                 pCapwapIsSetFsCapwapLinkEncryptionEntry,
                 SNMP_SUCCESS) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCapwapLinkEncryptionTable:  CapwapSetAllFsCapwapLinkEncryptionTableTrigger function returns failure.\r\n");
            }
            MemReleaseMemBlock (CAPWAP_FSCAPWAPLINKENCRYPTIONTABLE_POOLID,
                                (UINT1 *)
                                pCapwapOldFsCapwapLinkEncryptionEntry);
            MemReleaseMemBlock (CAPWAP_FSCAPWAPLINKENCRYPTIONTABLE_POOLID,
                                (UINT1 *)
                                pCapwapTrgFsCapwapLinkEncryptionEntry);
            MemReleaseMemBlock (CAPWAP_FSCAPWAPLINKENCRYPTIONTABLE_ISSET_POOLID,
                                (UINT1 *)
                                pCapwapTrgIsSetFsCapwapLinkEncryptionEntry);
            return OSIX_SUCCESS;

        }
        else
        {
            if (CapwapSetAllFsCapwapLinkEncryptionTableTrigger
                (pCapwapSetFsCapwapLinkEncryptionEntry,
                 pCapwapIsSetFsCapwapLinkEncryptionEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCapwapLinkEncryptionTable: CapwapSetAllFsCapwapLinkEncryptionTableTrigger function returns failure.\r\n");
            }
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCapwapLinkEncryptionTable: Failure.\r\n");
            MemReleaseMemBlock (CAPWAP_FSCAPWAPLINKENCRYPTIONTABLE_POOLID,
                                (UINT1 *)
                                pCapwapOldFsCapwapLinkEncryptionEntry);
            MemReleaseMemBlock (CAPWAP_FSCAPWAPLINKENCRYPTIONTABLE_POOLID,
                                (UINT1 *)
                                pCapwapTrgFsCapwapLinkEncryptionEntry);
            MemReleaseMemBlock (CAPWAP_FSCAPWAPLINKENCRYPTIONTABLE_ISSET_POOLID,
                                (UINT1 *)
                                pCapwapTrgIsSetFsCapwapLinkEncryptionEntry);
            return OSIX_FAILURE;
        }
    }
    else if ((pCapwapSetFsCapwapLinkEncryptionEntry->MibObject.
              i4FsCapwapEncryptChannelRowStatus == CREATE_AND_WAIT)
             || (pCapwapSetFsCapwapLinkEncryptionEntry->MibObject.
                 i4FsCapwapEncryptChannelRowStatus == CREATE_AND_GO))
    {

        if (pCapwapIsSetFsCapwapLinkEncryptionEntry->
            bFsCapwapEncryptChannelStatus != OSIX_FALSE)
        {
            pCapwapFsCapwapLinkEncryptionEntry->MibObject.
                i4FsCapwapEncryptChannelStatus =
                pCapwapSetFsCapwapLinkEncryptionEntry->MibObject.
                i4FsCapwapEncryptChannelStatus;

        }
        if (CapwapSetAllFsCapwapLinkEncryptionTableTrigger
            (pCapwapSetFsCapwapLinkEncryptionEntry,
             pCapwapIsSetFsCapwapLinkEncryptionEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)

        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCapwapLinkEncryptionTable: CapwapSetAllFsCapwapLinkEncryptionTableTrigger function returns failure.\r\n");
        }
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapSetAllFsCapwapLinkEncryptionTable: The row is already present.\r\n");
        MemReleaseMemBlock (CAPWAP_FSCAPWAPLINKENCRYPTIONTABLE_POOLID,
                            (UINT1 *) pCapwapOldFsCapwapLinkEncryptionEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPLINKENCRYPTIONTABLE_POOLID,
                            (UINT1 *) pCapwapTrgFsCapwapLinkEncryptionEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPLINKENCRYPTIONTABLE_ISSET_POOLID,
                            (UINT1 *)
                            pCapwapTrgIsSetFsCapwapLinkEncryptionEntry);
        return OSIX_FAILURE;
    }
    /* Copy the previous values before setting the new values */
    MEMCPY (pCapwapOldFsCapwapLinkEncryptionEntry,
            pCapwapFsCapwapLinkEncryptionEntry,
            sizeof (tCapwapFsCapwapLinkEncryptionEntry));

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pCapwapSetFsCapwapLinkEncryptionEntry->MibObject.
        i4FsCapwapEncryptChannelRowStatus == DESTROY)
    {
        pCapwapFsCapwapLinkEncryptionEntry->MibObject.
            i4FsCapwapEncryptChannelRowStatus = DESTROY;

        if (pCapwapIsSetFsCapwapLinkEncryptionEntry->
            bFsCapwapEncryptChannelStatus != OSIX_FALSE)
        {
            pCapwapFsCapwapLinkEncryptionEntry->MibObject.
                i4FsCapwapEncryptChannelStatus =
                pCapwapSetFsCapwapLinkEncryptionEntry->MibObject.
                i4FsCapwapEncryptChannelStatus;

        }

        if (CapwapUtilUpdateFsCapwapLinkEncryptionTable
            (pCapwapOldFsCapwapLinkEncryptionEntry,
             pCapwapFsCapwapLinkEncryptionEntry,
             pCapwapIsSetFsCapwapLinkEncryptionEntry) != OSIX_SUCCESS)
        {

            if (CapwapSetAllFsCapwapLinkEncryptionTableTrigger
                (pCapwapSetFsCapwapLinkEncryptionEntry,
                 pCapwapIsSetFsCapwapLinkEncryptionEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCapwapLinkEncryptionTable: CapwapSetAllFsCapwapLinkEncryptionTableTrigger function returns failure.\r\n");
            }
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCapwapLinkEncryptionTable: CapwapUtilUpdateFsCapwapLinkEncryptionTable function returns failure.\r\n");
        }
        RBTreeRem (gCapwapGlobals.CapwapGlbMib.FsCapwapLinkEncryptionTable,
                   pCapwapFsCapwapLinkEncryptionEntry);
        if (CapwapSetAllFsCapwapLinkEncryptionTableTrigger
            (pCapwapSetFsCapwapLinkEncryptionEntry,
             pCapwapIsSetFsCapwapLinkEncryptionEntry,
             SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCapwapLinkEncryptionTable: CapwapSetAllFsCapwapLinkEncryptionTableTrigger function returns failure.\r\n");
            MemReleaseMemBlock (CAPWAP_FSCAPWAPLINKENCRYPTIONTABLE_POOLID,
                                (UINT1 *)
                                pCapwapOldFsCapwapLinkEncryptionEntry);
            MemReleaseMemBlock (CAPWAP_FSCAPWAPLINKENCRYPTIONTABLE_POOLID,
                                (UINT1 *)
                                pCapwapTrgFsCapwapLinkEncryptionEntry);
            MemReleaseMemBlock (CAPWAP_FSCAPWAPLINKENCRYPTIONTABLE_ISSET_POOLID,
                                (UINT1 *)
                                pCapwapTrgIsSetFsCapwapLinkEncryptionEntry);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (CAPWAP_FSCAPWAPLINKENCRYPTIONTABLE_POOLID,
                            (UINT1 *) pCapwapFsCapwapLinkEncryptionEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPLINKENCRYPTIONTABLE_POOLID,
                            (UINT1 *) pCapwapOldFsCapwapLinkEncryptionEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPLINKENCRYPTIONTABLE_POOLID,
                            (UINT1 *) pCapwapTrgFsCapwapLinkEncryptionEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPLINKENCRYPTIONTABLE_ISSET_POOLID,
                            (UINT1 *)
                            pCapwapTrgIsSetFsCapwapLinkEncryptionEntry);
        return OSIX_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (FsCapwapLinkEncryptionTableFilterInputs
        (pCapwapFsCapwapLinkEncryptionEntry,
         pCapwapSetFsCapwapLinkEncryptionEntry,
         pCapwapIsSetFsCapwapLinkEncryptionEntry) != OSIX_TRUE)
    {
        MemReleaseMemBlock (CAPWAP_FSCAPWAPLINKENCRYPTIONTABLE_POOLID,
                            (UINT1 *) pCapwapOldFsCapwapLinkEncryptionEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPLINKENCRYPTIONTABLE_POOLID,
                            (UINT1 *) pCapwapTrgFsCapwapLinkEncryptionEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPLINKENCRYPTIONTABLE_ISSET_POOLID,
                            (UINT1 *)
                            pCapwapTrgIsSetFsCapwapLinkEncryptionEntry);
        return OSIX_SUCCESS;
    }

    /*This condtion is to make the row NOT_IN_SERVICE before modifying the values */
    if ((i4RowStatusLogic == TRUE) &&
        (pCapwapFsCapwapLinkEncryptionEntry->MibObject.
         i4FsCapwapEncryptChannelRowStatus == ACTIVE)
        && (pCapwapSetFsCapwapLinkEncryptionEntry->MibObject.
            i4FsCapwapEncryptChannelRowStatus != NOT_IN_SERVICE))
    {
        pCapwapFsCapwapLinkEncryptionEntry->MibObject.
            i4FsCapwapEncryptChannelRowStatus = NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        pCapwapTrgFsCapwapLinkEncryptionEntry->MibObject.
            i4FsCapwapEncryptChannelRowStatus = NOT_IN_SERVICE;
        pCapwapTrgIsSetFsCapwapLinkEncryptionEntry->
            bFsCapwapEncryptChannelRowStatus = OSIX_TRUE;

        if (CapwapUtilUpdateFsCapwapLinkEncryptionTable
            (pCapwapOldFsCapwapLinkEncryptionEntry,
             pCapwapFsCapwapLinkEncryptionEntry,
             pCapwapIsSetFsCapwapLinkEncryptionEntry) != OSIX_SUCCESS)
        {
            /*Restore back with previous values */
            MEMCPY (pCapwapFsCapwapLinkEncryptionEntry,
                    pCapwapOldFsCapwapLinkEncryptionEntry,
                    sizeof (tCapwapFsCapwapLinkEncryptionEntry));
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCapwapLinkEncryptionTable:                 CapwapUtilUpdateFsCapwapLinkEncryptionTable Function returns failure.\r\n");

            if (CapwapSetAllFsCapwapLinkEncryptionTableTrigger
                (pCapwapSetFsCapwapLinkEncryptionEntry,
                 pCapwapIsSetFsCapwapLinkEncryptionEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCapwapLinkEncryptionTable: CapwapSetAllFsCapwapLinkEncryptionTableTrigger function returns failure.\r\n");
            }
            MemReleaseMemBlock (CAPWAP_FSCAPWAPLINKENCRYPTIONTABLE_POOLID,
                                (UINT1 *)
                                pCapwapOldFsCapwapLinkEncryptionEntry);
            MemReleaseMemBlock (CAPWAP_FSCAPWAPLINKENCRYPTIONTABLE_POOLID,
                                (UINT1 *)
                                pCapwapTrgFsCapwapLinkEncryptionEntry);
            MemReleaseMemBlock (CAPWAP_FSCAPWAPLINKENCRYPTIONTABLE_ISSET_POOLID,
                                (UINT1 *)
                                pCapwapTrgIsSetFsCapwapLinkEncryptionEntry);
            return OSIX_FAILURE;
        }

        if (CapwapSetAllFsCapwapLinkEncryptionTableTrigger
            (pCapwapTrgFsCapwapLinkEncryptionEntry,
             pCapwapTrgIsSetFsCapwapLinkEncryptionEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCapwapLinkEncryptionTable: CapwapSetAllFsCapwapLinkEncryptionTableTrigger function returns failure.\r\n");
        }
    }

    if (pCapwapSetFsCapwapLinkEncryptionEntry->MibObject.
        i4FsCapwapEncryptChannelRowStatus == ACTIVE)
    {
        i4RowMakeActive = TRUE;
    }

    /* Assign values for the existing node */
    if (pCapwapIsSetFsCapwapLinkEncryptionEntry->
        bFsCapwapEncryptChannelStatus != OSIX_FALSE)
    {
        pCapwapFsCapwapLinkEncryptionEntry->MibObject.
            i4FsCapwapEncryptChannelStatus =
            pCapwapSetFsCapwapLinkEncryptionEntry->MibObject.
            i4FsCapwapEncryptChannelStatus;
    }
    if (pCapwapIsSetFsCapwapLinkEncryptionEntry->
        bFsCapwapEncryptChannelRowStatus != OSIX_FALSE)
    {
        pCapwapFsCapwapLinkEncryptionEntry->MibObject.
            i4FsCapwapEncryptChannelRowStatus =
            pCapwapSetFsCapwapLinkEncryptionEntry->MibObject.
            i4FsCapwapEncryptChannelRowStatus;
    }

    /*This condtion is to make the row back to ACTIVE after modifying the values */
    if (i4RowMakeActive == TRUE)
    {
        pCapwapFsCapwapLinkEncryptionEntry->MibObject.
            i4FsCapwapEncryptChannelRowStatus = ACTIVE;
    }

    if (CapwapUtilUpdateFsCapwapLinkEncryptionTable
        (pCapwapOldFsCapwapLinkEncryptionEntry,
         pCapwapFsCapwapLinkEncryptionEntry,
         pCapwapIsSetFsCapwapLinkEncryptionEntry) != OSIX_SUCCESS)
    {

        if (CapwapSetAllFsCapwapLinkEncryptionTableTrigger
            (pCapwapSetFsCapwapLinkEncryptionEntry,
             pCapwapIsSetFsCapwapLinkEncryptionEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)

        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCapwapLinkEncryptionTable: CapwapSetAllFsCapwapLinkEncryptionTableTrigger function returns failure.\r\n");

        }
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapSetAllFsCapwapLinkEncryptionTable: CapwapUtilUpdateFsCapwapLinkEncryptionTable function returns failure.\r\n");
        /*Restore back with previous values */
        MEMCPY (pCapwapFsCapwapLinkEncryptionEntry,
                pCapwapOldFsCapwapLinkEncryptionEntry,
                sizeof (tCapwapFsCapwapLinkEncryptionEntry));
        MemReleaseMemBlock (CAPWAP_FSCAPWAPLINKENCRYPTIONTABLE_POOLID,
                            (UINT1 *) pCapwapOldFsCapwapLinkEncryptionEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPLINKENCRYPTIONTABLE_POOLID,
                            (UINT1 *) pCapwapTrgFsCapwapLinkEncryptionEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPLINKENCRYPTIONTABLE_ISSET_POOLID,
                            (UINT1 *)
                            pCapwapTrgIsSetFsCapwapLinkEncryptionEntry);
        return OSIX_FAILURE;

    }
    if (CapwapSetAllFsCapwapLinkEncryptionTableTrigger
        (pCapwapSetFsCapwapLinkEncryptionEntry,
         pCapwapIsSetFsCapwapLinkEncryptionEntry, SNMP_SUCCESS) != OSIX_SUCCESS)
    {

        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapSetAllFsCapwapLinkEncryptionTable: CapwapSetAllFsCapwapLinkEncryptionTableTrigger function returns failure.\r\n");
    }

    MemReleaseMemBlock (CAPWAP_FSCAPWAPLINKENCRYPTIONTABLE_POOLID,
                        (UINT1 *) pCapwapOldFsCapwapLinkEncryptionEntry);
    MemReleaseMemBlock (CAPWAP_FSCAPWAPLINKENCRYPTIONTABLE_POOLID,
                        (UINT1 *) pCapwapTrgFsCapwapLinkEncryptionEntry);
    MemReleaseMemBlock (CAPWAP_FSCAPWAPLINKENCRYPTIONTABLE_ISSET_POOLID,
                        (UINT1 *) pCapwapTrgIsSetFsCapwapLinkEncryptionEntry);
    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  CapwapSetAllFsCawapDefaultWtpProfileTable
 Input       :  pCapwapSetFsCapwapDefaultWtpProfileEntry
                pCapwapIsSetFsCapwapDefaultWtpProfileEntry
                i4RowStatusLogic
                i4RowCreateOption
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapSetAllFsCawapDefaultWtpProfileTable (tCapwapFsCapwapDefaultWtpProfileEntry
                                           *
                                           pCapwapSetFsCapwapDefaultWtpProfileEntry,
                                           tCapwapIsSetFsCapwapDefaultWtpProfileEntry
                                           *
                                           pCapwapIsSetFsCapwapDefaultWtpProfileEntry,
                                           INT4 i4RowStatusLogic,
                                           INT4 i4RowCreateOption)
{
    tCapwapFsCapwapDefaultWtpProfileEntry *pCapwapFsCapwapDefaultWtpProfileEntry
        = NULL;
    tCapwapFsCapwapDefaultWtpProfileEntry
        * pCapwapOldFsCapwapDefaultWtpProfileEntry = NULL;
    tCapwapFsCapwapDefaultWtpProfileEntry
        * pCapwapTrgFsCapwapDefaultWtpProfileEntry = NULL;
    tCapwapIsSetFsCapwapDefaultWtpProfileEntry
        * pCapwapTrgIsSetFsCapwapDefaultWtpProfileEntry = NULL;
    INT4                i4RowMakeActive = FALSE;

    pCapwapOldFsCapwapDefaultWtpProfileEntry =
        (tCapwapFsCapwapDefaultWtpProfileEntry *)
        MemAllocMemBlk (CAPWAP_FSCAWAPDEFAULTWTPPROFILETABLE_POOLID);
    if (pCapwapOldFsCapwapDefaultWtpProfileEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    pCapwapTrgFsCapwapDefaultWtpProfileEntry =
        (tCapwapFsCapwapDefaultWtpProfileEntry *)
        MemAllocMemBlk (CAPWAP_FSCAWAPDEFAULTWTPPROFILETABLE_POOLID);
    if (pCapwapTrgFsCapwapDefaultWtpProfileEntry == NULL)
    {
        MemReleaseMemBlock (CAPWAP_FSCAWAPDEFAULTWTPPROFILETABLE_POOLID,
                            (UINT1 *) pCapwapOldFsCapwapDefaultWtpProfileEntry);
        return OSIX_FAILURE;
    }
    pCapwapTrgIsSetFsCapwapDefaultWtpProfileEntry =
        (tCapwapIsSetFsCapwapDefaultWtpProfileEntry *)
        MemAllocMemBlk (CAPWAP_FSCAWAPDEFAULTWTPPROFILETABLE_ISSET_POOLID);
    if (pCapwapTrgIsSetFsCapwapDefaultWtpProfileEntry == NULL)
    {
        MemReleaseMemBlock (CAPWAP_FSCAWAPDEFAULTWTPPROFILETABLE_POOLID,
                            (UINT1 *) pCapwapOldFsCapwapDefaultWtpProfileEntry);
        MemReleaseMemBlock (CAPWAP_FSCAWAPDEFAULTWTPPROFILETABLE_POOLID,
                            (UINT1 *) pCapwapTrgFsCapwapDefaultWtpProfileEntry);
        return OSIX_FAILURE;
    }
    MEMSET (pCapwapOldFsCapwapDefaultWtpProfileEntry, 0,
            sizeof (tCapwapFsCapwapDefaultWtpProfileEntry));
    MEMSET (pCapwapTrgFsCapwapDefaultWtpProfileEntry, 0,
            sizeof (tCapwapFsCapwapDefaultWtpProfileEntry));
    MEMSET (pCapwapTrgIsSetFsCapwapDefaultWtpProfileEntry, 0,
            sizeof (tCapwapIsSetFsCapwapDefaultWtpProfileEntry));

    /* Check whether the node is already present */
    pCapwapFsCapwapDefaultWtpProfileEntry =
        RBTreeGet (gCapwapGlobals.CapwapGlbMib.FsCawapDefaultWtpProfileTable,
                   (tRBElem *) pCapwapSetFsCapwapDefaultWtpProfileEntry);

    if (pCapwapFsCapwapDefaultWtpProfileEntry == NULL)
    {
        /* Create the node if the RowStatus given is CREATE_AND_WAIT or CREATE_AND_GO */
        if ((pCapwapSetFsCapwapDefaultWtpProfileEntry->MibObject.
             i4FsCapwapDefaultWtpProfileRowStatus == CREATE_AND_WAIT)
            || (pCapwapSetFsCapwapDefaultWtpProfileEntry->MibObject.
                i4FsCapwapDefaultWtpProfileRowStatus == CREATE_AND_GO)
            ||
            ((pCapwapSetFsCapwapDefaultWtpProfileEntry->MibObject.
              i4FsCapwapDefaultWtpProfileRowStatus == ACTIVE)
             && (i4RowCreateOption == 1)))
        {
            /* Allocate memory for the new node */
            pCapwapFsCapwapDefaultWtpProfileEntry =
                (tCapwapFsCapwapDefaultWtpProfileEntry *)
                MemAllocMemBlk (CAPWAP_FSCAWAPDEFAULTWTPPROFILETABLE_POOLID);
            if (pCapwapFsCapwapDefaultWtpProfileEntry == NULL)
            {
                if (CapwapSetAllFsCawapDefaultWtpProfileTableTrigger
                    (pCapwapSetFsCapwapDefaultWtpProfileEntry,
                     pCapwapIsSetFsCapwapDefaultWtpProfileEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCawapDefaultWtpProfileTable:CapwapSetAllFsCawapDefaultWtpProfileTableTrigger function fails\r\n");

                }
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCawapDefaultWtpProfileTable: Fail to Allocate Memory.\r\n");
                MemReleaseMemBlock (CAPWAP_FSCAWAPDEFAULTWTPPROFILETABLE_POOLID,
                                    (UINT1 *)
                                    pCapwapOldFsCapwapDefaultWtpProfileEntry);
                MemReleaseMemBlock (CAPWAP_FSCAWAPDEFAULTWTPPROFILETABLE_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgFsCapwapDefaultWtpProfileEntry);
                MemReleaseMemBlock
                    (CAPWAP_FSCAWAPDEFAULTWTPPROFILETABLE_ISSET_POOLID,
                     (UINT1 *) pCapwapTrgIsSetFsCapwapDefaultWtpProfileEntry);
                return OSIX_FAILURE;
            }

            MEMSET (pCapwapFsCapwapDefaultWtpProfileEntry, 0,
                    sizeof (tCapwapFsCapwapDefaultWtpProfileEntry));
            if ((CapwapInitializeFsCawapDefaultWtpProfileTable
                 (pCapwapFsCapwapDefaultWtpProfileEntry)) == OSIX_FAILURE)
            {
                if (CapwapSetAllFsCawapDefaultWtpProfileTableTrigger
                    (pCapwapSetFsCapwapDefaultWtpProfileEntry,
                     pCapwapIsSetFsCapwapDefaultWtpProfileEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCawapDefaultWtpProfileTable:CapwapSetAllFsCawapDefaultWtpProfileTableTrigger function fails\r\n");

                }
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCawapDefaultWtpProfileTable: Fail to Initialize the Objects.\r\n");

                MemReleaseMemBlock (CAPWAP_FSCAWAPDEFAULTWTPPROFILETABLE_POOLID,
                                    (UINT1 *)
                                    pCapwapFsCapwapDefaultWtpProfileEntry);
                MemReleaseMemBlock (CAPWAP_FSCAWAPDEFAULTWTPPROFILETABLE_POOLID,
                                    (UINT1 *)
                                    pCapwapOldFsCapwapDefaultWtpProfileEntry);
                MemReleaseMemBlock (CAPWAP_FSCAWAPDEFAULTWTPPROFILETABLE_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgFsCapwapDefaultWtpProfileEntry);
                MemReleaseMemBlock
                    (CAPWAP_FSCAWAPDEFAULTWTPPROFILETABLE_ISSET_POOLID,
                     (UINT1 *) pCapwapTrgIsSetFsCapwapDefaultWtpProfileEntry);
                return OSIX_FAILURE;
            }
            /* Assign values for the new node */
            if (pCapwapIsSetFsCapwapDefaultWtpProfileEntry->
                bFsCapwapDefaultWtpProfileModelNumber != OSIX_FALSE)
            {
                MEMCPY (pCapwapFsCapwapDefaultWtpProfileEntry->MibObject.
                        au1FsCapwapDefaultWtpProfileModelNumber,
                        pCapwapSetFsCapwapDefaultWtpProfileEntry->MibObject.
                        au1FsCapwapDefaultWtpProfileModelNumber,
                        pCapwapSetFsCapwapDefaultWtpProfileEntry->MibObject.
                        i4FsCapwapDefaultWtpProfileModelNumberLen);

                pCapwapFsCapwapDefaultWtpProfileEntry->MibObject.
                    i4FsCapwapDefaultWtpProfileModelNumberLen =
                    pCapwapSetFsCapwapDefaultWtpProfileEntry->MibObject.
                    i4FsCapwapDefaultWtpProfileModelNumberLen;
            }

            if (pCapwapIsSetFsCapwapDefaultWtpProfileEntry->
                bFsCapwapDefaultWtpProfileWtpFallbackEnable != OSIX_FALSE)
            {
                pCapwapFsCapwapDefaultWtpProfileEntry->MibObject.
                    i4FsCapwapDefaultWtpProfileWtpFallbackEnable =
                    pCapwapSetFsCapwapDefaultWtpProfileEntry->MibObject.
                    i4FsCapwapDefaultWtpProfileWtpFallbackEnable;
            }

            if (pCapwapIsSetFsCapwapDefaultWtpProfileEntry->
                bFsCapwapDefaultWtpProfileWtpEchoInterval != OSIX_FALSE)
            {
                pCapwapFsCapwapDefaultWtpProfileEntry->MibObject.
                    u4FsCapwapDefaultWtpProfileWtpEchoInterval =
                    pCapwapSetFsCapwapDefaultWtpProfileEntry->MibObject.
                    u4FsCapwapDefaultWtpProfileWtpEchoInterval;
            }

            if (pCapwapIsSetFsCapwapDefaultWtpProfileEntry->
                bFsCapwapDefaultWtpProfileWtpIdleTimeout != OSIX_FALSE)
            {
                pCapwapFsCapwapDefaultWtpProfileEntry->MibObject.
                    u4FsCapwapDefaultWtpProfileWtpIdleTimeout =
                    pCapwapSetFsCapwapDefaultWtpProfileEntry->MibObject.
                    u4FsCapwapDefaultWtpProfileWtpIdleTimeout;
            }

            if (pCapwapIsSetFsCapwapDefaultWtpProfileEntry->
                bFsCapwapDefaultWtpProfileWtpMaxDiscoveryInterval != OSIX_FALSE)
            {
                pCapwapFsCapwapDefaultWtpProfileEntry->MibObject.
                    u4FsCapwapDefaultWtpProfileWtpMaxDiscoveryInterval =
                    pCapwapSetFsCapwapDefaultWtpProfileEntry->MibObject.
                    u4FsCapwapDefaultWtpProfileWtpMaxDiscoveryInterval;
            }

            if (pCapwapIsSetFsCapwapDefaultWtpProfileEntry->
                bFsCapwapDefaultWtpProfileWtpReportInterval != OSIX_FALSE)
            {
                pCapwapFsCapwapDefaultWtpProfileEntry->MibObject.
                    u4FsCapwapDefaultWtpProfileWtpReportInterval =
                    pCapwapSetFsCapwapDefaultWtpProfileEntry->MibObject.
                    u4FsCapwapDefaultWtpProfileWtpReportInterval;
            }

            if (pCapwapIsSetFsCapwapDefaultWtpProfileEntry->
                bFsCapwapDefaultWtpProfileWtpStatisticsTimer != OSIX_FALSE)
            {
                pCapwapFsCapwapDefaultWtpProfileEntry->MibObject.
                    u4FsCapwapDefaultWtpProfileWtpStatisticsTimer =
                    pCapwapSetFsCapwapDefaultWtpProfileEntry->MibObject.
                    u4FsCapwapDefaultWtpProfileWtpStatisticsTimer;
            }

            if (pCapwapIsSetFsCapwapDefaultWtpProfileEntry->
                bFsCapwapDefaultWtpProfileWtpEcnSupport != OSIX_FALSE)
            {
                pCapwapFsCapwapDefaultWtpProfileEntry->MibObject.
                    i4FsCapwapDefaultWtpProfileWtpEcnSupport =
                    pCapwapSetFsCapwapDefaultWtpProfileEntry->MibObject.
                    i4FsCapwapDefaultWtpProfileWtpEcnSupport;
            }

            if (pCapwapIsSetFsCapwapDefaultWtpProfileEntry->
                bFsCapwapDefaultWtpProfileRowStatus != OSIX_FALSE)
            {
                pCapwapFsCapwapDefaultWtpProfileEntry->MibObject.
                    i4FsCapwapDefaultWtpProfileRowStatus =
                    pCapwapSetFsCapwapDefaultWtpProfileEntry->MibObject.
                    i4FsCapwapDefaultWtpProfileRowStatus;
            }

            if ((pCapwapSetFsCapwapDefaultWtpProfileEntry->MibObject.
                 i4FsCapwapDefaultWtpProfileRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pCapwapSetFsCapwapDefaultWtpProfileEntry->MibObject.
                        i4FsCapwapDefaultWtpProfileRowStatus == ACTIVE)))
            {
                pCapwapFsCapwapDefaultWtpProfileEntry->MibObject.
                    i4FsCapwapDefaultWtpProfileRowStatus = ACTIVE;
            }
            else if (pCapwapSetFsCapwapDefaultWtpProfileEntry->MibObject.
                     i4FsCapwapDefaultWtpProfileRowStatus == CREATE_AND_WAIT)
            {
                pCapwapFsCapwapDefaultWtpProfileEntry->MibObject.
                    i4FsCapwapDefaultWtpProfileRowStatus = NOT_READY;
            }

            /* Add the new node to the database */
            if (RBTreeAdd
                (gCapwapGlobals.CapwapGlbMib.FsCawapDefaultWtpProfileTable,
                 (tRBElem *) pCapwapFsCapwapDefaultWtpProfileEntry) !=
                RB_SUCCESS)
            {
                if (CapwapSetAllFsCawapDefaultWtpProfileTableTrigger
                    (pCapwapSetFsCapwapDefaultWtpProfileEntry,
                     pCapwapIsSetFsCapwapDefaultWtpProfileEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCawapDefaultWtpProfileTable: CapwapSetAllFsCawapDefaultWtpProfileTableTrigger function returns failure.\r\n");
                }
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCawapDefaultWtpProfileTable: RBTreeAdd is failed.\r\n");

                MemReleaseMemBlock (CAPWAP_FSCAWAPDEFAULTWTPPROFILETABLE_POOLID,
                                    (UINT1 *)
                                    pCapwapFsCapwapDefaultWtpProfileEntry);
                MemReleaseMemBlock (CAPWAP_FSCAWAPDEFAULTWTPPROFILETABLE_POOLID,
                                    (UINT1 *)
                                    pCapwapOldFsCapwapDefaultWtpProfileEntry);
                MemReleaseMemBlock (CAPWAP_FSCAWAPDEFAULTWTPPROFILETABLE_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgFsCapwapDefaultWtpProfileEntry);
                MemReleaseMemBlock
                    (CAPWAP_FSCAWAPDEFAULTWTPPROFILETABLE_ISSET_POOLID,
                     (UINT1 *) pCapwapTrgIsSetFsCapwapDefaultWtpProfileEntry);
                return OSIX_FAILURE;
            }
            if (CapwapUtilUpdateFsCawapDefaultWtpProfileTable
                (NULL, pCapwapFsCapwapDefaultWtpProfileEntry,
                 pCapwapIsSetFsCapwapDefaultWtpProfileEntry) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCawapDefaultWtpProfileTable: CapwapUtilUpdateFsCawapDefaultWtpProfileTable function returns failure.\r\n");

                if (CapwapSetAllFsCawapDefaultWtpProfileTableTrigger
                    (pCapwapSetFsCapwapDefaultWtpProfileEntry,
                     pCapwapIsSetFsCapwapDefaultWtpProfileEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCawapDefaultWtpProfileTable: CapwapSetAllFsCawapDefaultWtpProfileTableTrigger function returns failure.\r\n");

                }
                RBTreeRem (gCapwapGlobals.CapwapGlbMib.
                           FsCawapDefaultWtpProfileTable,
                           pCapwapFsCapwapDefaultWtpProfileEntry);
                MemReleaseMemBlock (CAPWAP_FSCAWAPDEFAULTWTPPROFILETABLE_POOLID,
                                    (UINT1 *)
                                    pCapwapFsCapwapDefaultWtpProfileEntry);
                MemReleaseMemBlock (CAPWAP_FSCAWAPDEFAULTWTPPROFILETABLE_POOLID,
                                    (UINT1 *)
                                    pCapwapOldFsCapwapDefaultWtpProfileEntry);
                MemReleaseMemBlock (CAPWAP_FSCAWAPDEFAULTWTPPROFILETABLE_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgFsCapwapDefaultWtpProfileEntry);
                MemReleaseMemBlock
                    (CAPWAP_FSCAWAPDEFAULTWTPPROFILETABLE_ISSET_POOLID,
                     (UINT1 *) pCapwapTrgIsSetFsCapwapDefaultWtpProfileEntry);
                return OSIX_FAILURE;
            }

            if ((pCapwapSetFsCapwapDefaultWtpProfileEntry->MibObject.
                 i4FsCapwapDefaultWtpProfileRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pCapwapSetFsCapwapDefaultWtpProfileEntry->MibObject.
                        i4FsCapwapDefaultWtpProfileRowStatus == ACTIVE)))
            {

                /* For MSR and RM Trigger */
                MEMCPY (pCapwapTrgFsCapwapDefaultWtpProfileEntry->MibObject.
                        au1FsCapwapDefaultWtpProfileModelNumber,
                        pCapwapSetFsCapwapDefaultWtpProfileEntry->MibObject.
                        au1FsCapwapDefaultWtpProfileModelNumber,
                        pCapwapSetFsCapwapDefaultWtpProfileEntry->MibObject.
                        i4FsCapwapDefaultWtpProfileModelNumberLen);

                pCapwapTrgFsCapwapDefaultWtpProfileEntry->MibObject.
                    i4FsCapwapDefaultWtpProfileModelNumberLen =
                    pCapwapSetFsCapwapDefaultWtpProfileEntry->MibObject.
                    i4FsCapwapDefaultWtpProfileModelNumberLen;
                pCapwapTrgFsCapwapDefaultWtpProfileEntry->MibObject.
                    i4FsCapwapDefaultWtpProfileRowStatus = CREATE_AND_WAIT;
                pCapwapTrgIsSetFsCapwapDefaultWtpProfileEntry->
                    bFsCapwapDefaultWtpProfileRowStatus = OSIX_TRUE;

                if (CapwapSetAllFsCawapDefaultWtpProfileTableTrigger
                    (pCapwapTrgFsCapwapDefaultWtpProfileEntry,
                     pCapwapTrgIsSetFsCapwapDefaultWtpProfileEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCawapDefaultWtpProfileTable: CapwapSetAllFsCawapDefaultWtpProfileTableTrigger function returns failure.\r\n");

                    MemReleaseMemBlock
                        (CAPWAP_FSCAWAPDEFAULTWTPPROFILETABLE_POOLID,
                         (UINT1 *) pCapwapFsCapwapDefaultWtpProfileEntry);
                    MemReleaseMemBlock
                        (CAPWAP_FSCAWAPDEFAULTWTPPROFILETABLE_POOLID,
                         (UINT1 *) pCapwapOldFsCapwapDefaultWtpProfileEntry);
                    MemReleaseMemBlock
                        (CAPWAP_FSCAWAPDEFAULTWTPPROFILETABLE_POOLID,
                         (UINT1 *) pCapwapTrgFsCapwapDefaultWtpProfileEntry);
                    MemReleaseMemBlock
                        (CAPWAP_FSCAWAPDEFAULTWTPPROFILETABLE_ISSET_POOLID,
                         (UINT1 *)
                         pCapwapTrgIsSetFsCapwapDefaultWtpProfileEntry);
                    return OSIX_FAILURE;
                }
            }
            else if (pCapwapSetFsCapwapDefaultWtpProfileEntry->MibObject.
                     i4FsCapwapDefaultWtpProfileRowStatus == CREATE_AND_WAIT)
            {

                /* For MSR and RM Trigger */
                pCapwapTrgFsCapwapDefaultWtpProfileEntry->MibObject.
                    i4FsCapwapDefaultWtpProfileRowStatus = CREATE_AND_WAIT;
                pCapwapTrgIsSetFsCapwapDefaultWtpProfileEntry->
                    bFsCapwapDefaultWtpProfileRowStatus = OSIX_TRUE;

                if (CapwapSetAllFsCawapDefaultWtpProfileTableTrigger
                    (pCapwapTrgFsCapwapDefaultWtpProfileEntry,
                     pCapwapTrgIsSetFsCapwapDefaultWtpProfileEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCawapDefaultWtpProfileTable: CapwapSetAllFsCawapDefaultWtpProfileTableTrigger function returns failure.\r\n");

                    MemReleaseMemBlock
                        (CAPWAP_FSCAWAPDEFAULTWTPPROFILETABLE_POOLID,
                         (UINT1 *) pCapwapFsCapwapDefaultWtpProfileEntry);
                    MemReleaseMemBlock
                        (CAPWAP_FSCAWAPDEFAULTWTPPROFILETABLE_POOLID,
                         (UINT1 *) pCapwapOldFsCapwapDefaultWtpProfileEntry);
                    MemReleaseMemBlock
                        (CAPWAP_FSCAWAPDEFAULTWTPPROFILETABLE_POOLID,
                         (UINT1 *) pCapwapTrgFsCapwapDefaultWtpProfileEntry);
                    MemReleaseMemBlock
                        (CAPWAP_FSCAWAPDEFAULTWTPPROFILETABLE_ISSET_POOLID,
                         (UINT1 *)
                         pCapwapTrgIsSetFsCapwapDefaultWtpProfileEntry);
                    return OSIX_FAILURE;
                }
            }

            if (pCapwapSetFsCapwapDefaultWtpProfileEntry->MibObject.
                i4FsCapwapDefaultWtpProfileRowStatus == CREATE_AND_GO)
            {
                pCapwapSetFsCapwapDefaultWtpProfileEntry->MibObject.
                    i4FsCapwapDefaultWtpProfileRowStatus = ACTIVE;
            }

            if (CapwapSetAllFsCawapDefaultWtpProfileTableTrigger
                (pCapwapSetFsCapwapDefaultWtpProfileEntry,
                 pCapwapIsSetFsCapwapDefaultWtpProfileEntry,
                 SNMP_SUCCESS) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCawapDefaultWtpProfileTable:  CapwapSetAllFsCawapDefaultWtpProfileTableTrigger function returns failure.\r\n");
            }
            MemReleaseMemBlock (CAPWAP_FSCAWAPDEFAULTWTPPROFILETABLE_POOLID,
                                (UINT1 *)
                                pCapwapOldFsCapwapDefaultWtpProfileEntry);
            MemReleaseMemBlock (CAPWAP_FSCAWAPDEFAULTWTPPROFILETABLE_POOLID,
                                (UINT1 *)
                                pCapwapTrgFsCapwapDefaultWtpProfileEntry);
            MemReleaseMemBlock
                (CAPWAP_FSCAWAPDEFAULTWTPPROFILETABLE_ISSET_POOLID,
                 (UINT1 *) pCapwapTrgIsSetFsCapwapDefaultWtpProfileEntry);
            return OSIX_SUCCESS;

        }
        else
        {
            if (CapwapSetAllFsCawapDefaultWtpProfileTableTrigger
                (pCapwapSetFsCapwapDefaultWtpProfileEntry,
                 pCapwapIsSetFsCapwapDefaultWtpProfileEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCawapDefaultWtpProfileTable: CapwapSetAllFsCawapDefaultWtpProfileTableTrigger function returns failure.\r\n");
            }
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCawapDefaultWtpProfileTable: Failure.\r\n");
            MemReleaseMemBlock (CAPWAP_FSCAWAPDEFAULTWTPPROFILETABLE_POOLID,
                                (UINT1 *)
                                pCapwapOldFsCapwapDefaultWtpProfileEntry);
            MemReleaseMemBlock (CAPWAP_FSCAWAPDEFAULTWTPPROFILETABLE_POOLID,
                                (UINT1 *)
                                pCapwapTrgFsCapwapDefaultWtpProfileEntry);
            MemReleaseMemBlock
                (CAPWAP_FSCAWAPDEFAULTWTPPROFILETABLE_ISSET_POOLID,
                 (UINT1 *) pCapwapTrgIsSetFsCapwapDefaultWtpProfileEntry);
            return OSIX_FAILURE;
        }
    }
    else if ((pCapwapSetFsCapwapDefaultWtpProfileEntry->MibObject.
              i4FsCapwapDefaultWtpProfileRowStatus == CREATE_AND_WAIT)
             || (pCapwapSetFsCapwapDefaultWtpProfileEntry->MibObject.
                 i4FsCapwapDefaultWtpProfileRowStatus == CREATE_AND_GO))
    {
        if (CapwapSetAllFsCawapDefaultWtpProfileTableTrigger
            (pCapwapSetFsCapwapDefaultWtpProfileEntry,
             pCapwapIsSetFsCapwapDefaultWtpProfileEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)

        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCawapDefaultWtpProfileTable: CapwapSetAllFsCawapDefaultWtpProfileTableTrigger function returns failure.\r\n");
        }
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapSetAllFsCawapDefaultWtpProfileTable: The row is already present.\r\n");
        MemReleaseMemBlock (CAPWAP_FSCAWAPDEFAULTWTPPROFILETABLE_POOLID,
                            (UINT1 *) pCapwapOldFsCapwapDefaultWtpProfileEntry);
        MemReleaseMemBlock (CAPWAP_FSCAWAPDEFAULTWTPPROFILETABLE_POOLID,
                            (UINT1 *) pCapwapTrgFsCapwapDefaultWtpProfileEntry);
        MemReleaseMemBlock (CAPWAP_FSCAWAPDEFAULTWTPPROFILETABLE_ISSET_POOLID,
                            (UINT1 *)
                            pCapwapTrgIsSetFsCapwapDefaultWtpProfileEntry);
        return OSIX_FAILURE;
    }
    /* Copy the previous values before setting the new values */
    MEMCPY (pCapwapOldFsCapwapDefaultWtpProfileEntry,
            pCapwapFsCapwapDefaultWtpProfileEntry,
            sizeof (tCapwapFsCapwapDefaultWtpProfileEntry));

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pCapwapSetFsCapwapDefaultWtpProfileEntry->MibObject.
        i4FsCapwapDefaultWtpProfileRowStatus == DESTROY)
    {
        pCapwapFsCapwapDefaultWtpProfileEntry->MibObject.
            i4FsCapwapDefaultWtpProfileRowStatus = DESTROY;

        if (CapwapUtilUpdateFsCawapDefaultWtpProfileTable
            (pCapwapOldFsCapwapDefaultWtpProfileEntry,
             pCapwapFsCapwapDefaultWtpProfileEntry,
             pCapwapIsSetFsCapwapDefaultWtpProfileEntry) != OSIX_SUCCESS)
        {

            if (CapwapSetAllFsCawapDefaultWtpProfileTableTrigger
                (pCapwapSetFsCapwapDefaultWtpProfileEntry,
                 pCapwapIsSetFsCapwapDefaultWtpProfileEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCawapDefaultWtpProfileTable: CapwapSetAllFsCawapDefaultWtpProfileTableTrigger function returns failure.\r\n");
            }
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCawapDefaultWtpProfileTable: CapwapUtilUpdateFsCawapDefaultWtpProfileTable function returns failure.\r\n");
        }
        RBTreeRem (gCapwapGlobals.CapwapGlbMib.FsCawapDefaultWtpProfileTable,
                   pCapwapFsCapwapDefaultWtpProfileEntry);
        if (CapwapSetAllFsCawapDefaultWtpProfileTableTrigger
            (pCapwapSetFsCapwapDefaultWtpProfileEntry,
             pCapwapIsSetFsCapwapDefaultWtpProfileEntry,
             SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCawapDefaultWtpProfileTable: CapwapSetAllFsCawapDefaultWtpProfileTableTrigger function returns failure.\r\n");
            MemReleaseMemBlock (CAPWAP_FSCAWAPDEFAULTWTPPROFILETABLE_POOLID,
                                (UINT1 *)
                                pCapwapOldFsCapwapDefaultWtpProfileEntry);
            MemReleaseMemBlock (CAPWAP_FSCAWAPDEFAULTWTPPROFILETABLE_POOLID,
                                (UINT1 *)
                                pCapwapTrgFsCapwapDefaultWtpProfileEntry);
            MemReleaseMemBlock
                (CAPWAP_FSCAWAPDEFAULTWTPPROFILETABLE_ISSET_POOLID,
                 (UINT1 *) pCapwapTrgIsSetFsCapwapDefaultWtpProfileEntry);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (CAPWAP_FSCAWAPDEFAULTWTPPROFILETABLE_POOLID,
                            (UINT1 *) pCapwapFsCapwapDefaultWtpProfileEntry);
        MemReleaseMemBlock (CAPWAP_FSCAWAPDEFAULTWTPPROFILETABLE_POOLID,
                            (UINT1 *) pCapwapOldFsCapwapDefaultWtpProfileEntry);
        MemReleaseMemBlock (CAPWAP_FSCAWAPDEFAULTWTPPROFILETABLE_POOLID,
                            (UINT1 *) pCapwapTrgFsCapwapDefaultWtpProfileEntry);
        MemReleaseMemBlock (CAPWAP_FSCAWAPDEFAULTWTPPROFILETABLE_ISSET_POOLID,
                            (UINT1 *)
                            pCapwapTrgIsSetFsCapwapDefaultWtpProfileEntry);
        return OSIX_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (FsCawapDefaultWtpProfileTableFilterInputs
        (pCapwapFsCapwapDefaultWtpProfileEntry,
         pCapwapSetFsCapwapDefaultWtpProfileEntry,
         pCapwapIsSetFsCapwapDefaultWtpProfileEntry) != OSIX_TRUE)
    {
        MemReleaseMemBlock (CAPWAP_FSCAWAPDEFAULTWTPPROFILETABLE_POOLID,
                            (UINT1 *) pCapwapOldFsCapwapDefaultWtpProfileEntry);
        MemReleaseMemBlock (CAPWAP_FSCAWAPDEFAULTWTPPROFILETABLE_POOLID,
                            (UINT1 *) pCapwapTrgFsCapwapDefaultWtpProfileEntry);
        MemReleaseMemBlock (CAPWAP_FSCAWAPDEFAULTWTPPROFILETABLE_ISSET_POOLID,
                            (UINT1 *)
                            pCapwapTrgIsSetFsCapwapDefaultWtpProfileEntry);
        return OSIX_SUCCESS;
    }

    /*This condtion is to make the row NOT_IN_SERVICE before modifying the values */
    if ((i4RowStatusLogic == TRUE) &&
        (pCapwapFsCapwapDefaultWtpProfileEntry->MibObject.
         i4FsCapwapDefaultWtpProfileRowStatus == ACTIVE)
        && (pCapwapSetFsCapwapDefaultWtpProfileEntry->MibObject.
            i4FsCapwapDefaultWtpProfileRowStatus != NOT_IN_SERVICE))
    {
        pCapwapFsCapwapDefaultWtpProfileEntry->MibObject.
            i4FsCapwapDefaultWtpProfileRowStatus = NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        pCapwapTrgFsCapwapDefaultWtpProfileEntry->MibObject.
            i4FsCapwapDefaultWtpProfileRowStatus = NOT_IN_SERVICE;
        pCapwapTrgIsSetFsCapwapDefaultWtpProfileEntry->
            bFsCapwapDefaultWtpProfileRowStatus = OSIX_TRUE;

        if (CapwapUtilUpdateFsCawapDefaultWtpProfileTable
            (pCapwapOldFsCapwapDefaultWtpProfileEntry,
             pCapwapFsCapwapDefaultWtpProfileEntry,
             pCapwapIsSetFsCapwapDefaultWtpProfileEntry) != OSIX_SUCCESS)
        {
            /*Restore back with previous values */
            MEMCPY (pCapwapFsCapwapDefaultWtpProfileEntry,
                    pCapwapOldFsCapwapDefaultWtpProfileEntry,
                    sizeof (tCapwapFsCapwapDefaultWtpProfileEntry));
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCawapDefaultWtpProfileTable:                 CapwapUtilUpdateFsCawapDefaultWtpProfileTable Function returns failure.\r\n");

            if (CapwapSetAllFsCawapDefaultWtpProfileTableTrigger
                (pCapwapSetFsCapwapDefaultWtpProfileEntry,
                 pCapwapIsSetFsCapwapDefaultWtpProfileEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCawapDefaultWtpProfileTable: CapwapSetAllFsCawapDefaultWtpProfileTableTrigger function returns failure.\r\n");
            }
            MemReleaseMemBlock (CAPWAP_FSCAWAPDEFAULTWTPPROFILETABLE_POOLID,
                                (UINT1 *)
                                pCapwapOldFsCapwapDefaultWtpProfileEntry);
            MemReleaseMemBlock (CAPWAP_FSCAWAPDEFAULTWTPPROFILETABLE_POOLID,
                                (UINT1 *)
                                pCapwapTrgFsCapwapDefaultWtpProfileEntry);
            MemReleaseMemBlock
                (CAPWAP_FSCAWAPDEFAULTWTPPROFILETABLE_ISSET_POOLID,
                 (UINT1 *) pCapwapTrgIsSetFsCapwapDefaultWtpProfileEntry);
            return OSIX_FAILURE;
        }

        if (CapwapSetAllFsCawapDefaultWtpProfileTableTrigger
            (pCapwapTrgFsCapwapDefaultWtpProfileEntry,
             pCapwapTrgIsSetFsCapwapDefaultWtpProfileEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCawapDefaultWtpProfileTable: CapwapSetAllFsCawapDefaultWtpProfileTableTrigger function returns failure.\r\n");
        }
    }

    if (pCapwapSetFsCapwapDefaultWtpProfileEntry->MibObject.
        i4FsCapwapDefaultWtpProfileRowStatus == ACTIVE)
    {
        i4RowMakeActive = TRUE;
    }

    /* Assign values for the existing node */
    if (pCapwapIsSetFsCapwapDefaultWtpProfileEntry->
        bFsCapwapDefaultWtpProfileWtpFallbackEnable != OSIX_FALSE)
    {
        pCapwapFsCapwapDefaultWtpProfileEntry->MibObject.
            i4FsCapwapDefaultWtpProfileWtpFallbackEnable =
            pCapwapSetFsCapwapDefaultWtpProfileEntry->MibObject.
            i4FsCapwapDefaultWtpProfileWtpFallbackEnable;
    }
    if (pCapwapIsSetFsCapwapDefaultWtpProfileEntry->
        bFsCapwapDefaultWtpProfileWtpEchoInterval != OSIX_FALSE)
    {
        pCapwapFsCapwapDefaultWtpProfileEntry->MibObject.
            u4FsCapwapDefaultWtpProfileWtpEchoInterval =
            pCapwapSetFsCapwapDefaultWtpProfileEntry->MibObject.
            u4FsCapwapDefaultWtpProfileWtpEchoInterval;
    }
    if (pCapwapIsSetFsCapwapDefaultWtpProfileEntry->
        bFsCapwapDefaultWtpProfileWtpIdleTimeout != OSIX_FALSE)
    {
        pCapwapFsCapwapDefaultWtpProfileEntry->MibObject.
            u4FsCapwapDefaultWtpProfileWtpIdleTimeout =
            pCapwapSetFsCapwapDefaultWtpProfileEntry->MibObject.
            u4FsCapwapDefaultWtpProfileWtpIdleTimeout;
    }
    if (pCapwapIsSetFsCapwapDefaultWtpProfileEntry->
        bFsCapwapDefaultWtpProfileWtpMaxDiscoveryInterval != OSIX_FALSE)
    {
        pCapwapFsCapwapDefaultWtpProfileEntry->MibObject.
            u4FsCapwapDefaultWtpProfileWtpMaxDiscoveryInterval =
            pCapwapSetFsCapwapDefaultWtpProfileEntry->MibObject.
            u4FsCapwapDefaultWtpProfileWtpMaxDiscoveryInterval;
    }
    if (pCapwapIsSetFsCapwapDefaultWtpProfileEntry->
        bFsCapwapDefaultWtpProfileWtpReportInterval != OSIX_FALSE)
    {
        pCapwapFsCapwapDefaultWtpProfileEntry->MibObject.
            u4FsCapwapDefaultWtpProfileWtpReportInterval =
            pCapwapSetFsCapwapDefaultWtpProfileEntry->MibObject.
            u4FsCapwapDefaultWtpProfileWtpReportInterval;
    }
    if (pCapwapIsSetFsCapwapDefaultWtpProfileEntry->
        bFsCapwapDefaultWtpProfileWtpStatisticsTimer != OSIX_FALSE)
    {
        pCapwapFsCapwapDefaultWtpProfileEntry->MibObject.
            u4FsCapwapDefaultWtpProfileWtpStatisticsTimer =
            pCapwapSetFsCapwapDefaultWtpProfileEntry->MibObject.
            u4FsCapwapDefaultWtpProfileWtpStatisticsTimer;
    }
    if (pCapwapIsSetFsCapwapDefaultWtpProfileEntry->
        bFsCapwapDefaultWtpProfileWtpEcnSupport != OSIX_FALSE)
    {
        pCapwapFsCapwapDefaultWtpProfileEntry->MibObject.
            i4FsCapwapDefaultWtpProfileWtpEcnSupport =
            pCapwapSetFsCapwapDefaultWtpProfileEntry->MibObject.
            i4FsCapwapDefaultWtpProfileWtpEcnSupport;
    }
    if (pCapwapIsSetFsCapwapDefaultWtpProfileEntry->
        bFsCapwapDefaultWtpProfileRowStatus != OSIX_FALSE)
    {
        pCapwapFsCapwapDefaultWtpProfileEntry->MibObject.
            i4FsCapwapDefaultWtpProfileRowStatus =
            pCapwapSetFsCapwapDefaultWtpProfileEntry->MibObject.
            i4FsCapwapDefaultWtpProfileRowStatus;
    }

    /*This condtion is to make the row back to ACTIVE after modifying the values */
    if (i4RowMakeActive == TRUE)
    {
        pCapwapFsCapwapDefaultWtpProfileEntry->MibObject.
            i4FsCapwapDefaultWtpProfileRowStatus = ACTIVE;
    }

    if (CapwapUtilUpdateFsCawapDefaultWtpProfileTable
        (pCapwapOldFsCapwapDefaultWtpProfileEntry,
         pCapwapFsCapwapDefaultWtpProfileEntry,
         pCapwapIsSetFsCapwapDefaultWtpProfileEntry) != OSIX_SUCCESS)
    {

        if (CapwapSetAllFsCawapDefaultWtpProfileTableTrigger
            (pCapwapSetFsCapwapDefaultWtpProfileEntry,
             pCapwapIsSetFsCapwapDefaultWtpProfileEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)

        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCawapDefaultWtpProfileTable: CapwapSetAllFsCawapDefaultWtpProfileTableTrigger function returns failure.\r\n");

        }
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapSetAllFsCawapDefaultWtpProfileTable: CapwapUtilUpdateFsCawapDefaultWtpProfileTable function returns failure.\r\n");
        /*Restore back with previous values */
        MEMCPY (pCapwapFsCapwapDefaultWtpProfileEntry,
                pCapwapOldFsCapwapDefaultWtpProfileEntry,
                sizeof (tCapwapFsCapwapDefaultWtpProfileEntry));
        MemReleaseMemBlock (CAPWAP_FSCAWAPDEFAULTWTPPROFILETABLE_POOLID,
                            (UINT1 *) pCapwapOldFsCapwapDefaultWtpProfileEntry);
        MemReleaseMemBlock (CAPWAP_FSCAWAPDEFAULTWTPPROFILETABLE_POOLID,
                            (UINT1 *) pCapwapTrgFsCapwapDefaultWtpProfileEntry);
        MemReleaseMemBlock (CAPWAP_FSCAWAPDEFAULTWTPPROFILETABLE_ISSET_POOLID,
                            (UINT1 *)
                            pCapwapTrgIsSetFsCapwapDefaultWtpProfileEntry);
        return OSIX_FAILURE;

    }
    if (CapwapSetAllFsCawapDefaultWtpProfileTableTrigger
        (pCapwapSetFsCapwapDefaultWtpProfileEntry,
         pCapwapIsSetFsCapwapDefaultWtpProfileEntry,
         SNMP_SUCCESS) != OSIX_SUCCESS)
    {

        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapSetAllFsCawapDefaultWtpProfileTable: CapwapSetAllFsCawapDefaultWtpProfileTableTrigger function returns failure.\r\n");
    }

    MemReleaseMemBlock (CAPWAP_FSCAWAPDEFAULTWTPPROFILETABLE_POOLID,
                        (UINT1 *) pCapwapOldFsCapwapDefaultWtpProfileEntry);
    MemReleaseMemBlock (CAPWAP_FSCAWAPDEFAULTWTPPROFILETABLE_POOLID,
                        (UINT1 *) pCapwapTrgFsCapwapDefaultWtpProfileEntry);
    MemReleaseMemBlock (CAPWAP_FSCAWAPDEFAULTWTPPROFILETABLE_ISSET_POOLID,
                        (UINT1 *)
                        pCapwapTrgIsSetFsCapwapDefaultWtpProfileEntry);
    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  CapwapSetAllFsCapwapDnsProfileTable
 Input       :  pCapwapSetFsCapwapDnsProfileEntry
                pCapwapIsSetFsCapwapDnsProfileEntry
                i4RowStatusLogic
                i4RowCreateOption
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapSetAllFsCapwapDnsProfileTable (tCapwapFsCapwapDnsProfileEntry *
                                     pCapwapSetFsCapwapDnsProfileEntry,
                                     tCapwapIsSetFsCapwapDnsProfileEntry *
                                     pCapwapIsSetFsCapwapDnsProfileEntry,
                                     INT4 i4RowStatusLogic,
                                     INT4 i4RowCreateOption)
{
    tCapwapFsCapwapDnsProfileEntry *pCapwapFsCapwapDnsProfileEntry = NULL;
    tCapwapFsCapwapDnsProfileEntry *pCapwapOldFsCapwapDnsProfileEntry = NULL;
    tCapwapFsCapwapDnsProfileEntry *pCapwapTrgFsCapwapDnsProfileEntry = NULL;
    tCapwapIsSetFsCapwapDnsProfileEntry *pCapwapTrgIsSetFsCapwapDnsProfileEntry
        = NULL;
    INT4                i4RowMakeActive = FALSE;

    pCapwapOldFsCapwapDnsProfileEntry =
        (tCapwapFsCapwapDnsProfileEntry *)
        MemAllocMemBlk (CAPWAP_FSCAPWAPDNSPROFILETABLE_POOLID);
    if (pCapwapOldFsCapwapDnsProfileEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    pCapwapTrgFsCapwapDnsProfileEntry =
        (tCapwapFsCapwapDnsProfileEntry *)
        MemAllocMemBlk (CAPWAP_FSCAPWAPDNSPROFILETABLE_POOLID);
    if (pCapwapTrgFsCapwapDnsProfileEntry == NULL)
    {
        MemReleaseMemBlock (CAPWAP_FSCAPWAPDNSPROFILETABLE_POOLID,
                            (UINT1 *) pCapwapOldFsCapwapDnsProfileEntry);
        return OSIX_FAILURE;
    }
    pCapwapTrgIsSetFsCapwapDnsProfileEntry =
        (tCapwapIsSetFsCapwapDnsProfileEntry *)
        MemAllocMemBlk (CAPWAP_FSCAPWAPDNSPROFILETABLE_ISSET_POOLID);
    if (pCapwapTrgIsSetFsCapwapDnsProfileEntry == NULL)
    {
        MemReleaseMemBlock (CAPWAP_FSCAPWAPDNSPROFILETABLE_POOLID,
                            (UINT1 *) pCapwapOldFsCapwapDnsProfileEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPDNSPROFILETABLE_POOLID,
                            (UINT1 *) pCapwapTrgFsCapwapDnsProfileEntry);
        return OSIX_FAILURE;
    }
    MEMSET (pCapwapOldFsCapwapDnsProfileEntry, 0,
            sizeof (tCapwapFsCapwapDnsProfileEntry));
    MEMSET (pCapwapTrgFsCapwapDnsProfileEntry, 0,
            sizeof (tCapwapFsCapwapDnsProfileEntry));
    MEMSET (pCapwapTrgIsSetFsCapwapDnsProfileEntry, 0,
            sizeof (tCapwapIsSetFsCapwapDnsProfileEntry));

    /* Check whether the node is already present */
    pCapwapFsCapwapDnsProfileEntry =
        RBTreeGet (gCapwapGlobals.CapwapGlbMib.FsCapwapDnsProfileTable,
                   (tRBElem *) pCapwapSetFsCapwapDnsProfileEntry);

    if (pCapwapFsCapwapDnsProfileEntry == NULL)
    {
        /* Create the node if the RowStatus given is CREATE_AND_WAIT or CREATE_AND_GO */
        if ((pCapwapSetFsCapwapDnsProfileEntry->MibObject.
             i4FsCapwapDnsProfileRowStatus == CREATE_AND_WAIT)
            || (pCapwapSetFsCapwapDnsProfileEntry->MibObject.
                i4FsCapwapDnsProfileRowStatus == CREATE_AND_GO)
            ||
            ((pCapwapSetFsCapwapDnsProfileEntry->MibObject.
              i4FsCapwapDnsProfileRowStatus == ACTIVE)
             && (i4RowCreateOption == 1)))
        {
            /* Allocate memory for the new node */
            pCapwapFsCapwapDnsProfileEntry =
                (tCapwapFsCapwapDnsProfileEntry *)
                MemAllocMemBlk (CAPWAP_FSCAPWAPDNSPROFILETABLE_POOLID);
            if (pCapwapFsCapwapDnsProfileEntry == NULL)
            {
                if (CapwapSetAllFsCapwapDnsProfileTableTrigger
                    (pCapwapSetFsCapwapDnsProfileEntry,
                     pCapwapIsSetFsCapwapDnsProfileEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCapwapDnsProfileTable:CapwapSetAllFsCapwapDnsProfileTableTrigger function fails\r\n");

                }
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCapwapDnsProfileTable: Fail to Allocate Memory.\r\n");
                MemReleaseMemBlock (CAPWAP_FSCAPWAPDNSPROFILETABLE_POOLID,
                                    (UINT1 *)
                                    pCapwapOldFsCapwapDnsProfileEntry);
                MemReleaseMemBlock (CAPWAP_FSCAPWAPDNSPROFILETABLE_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgFsCapwapDnsProfileEntry);
                MemReleaseMemBlock (CAPWAP_FSCAPWAPDNSPROFILETABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgIsSetFsCapwapDnsProfileEntry);
                return OSIX_FAILURE;
            }

            MEMSET (pCapwapFsCapwapDnsProfileEntry, 0,
                    sizeof (tCapwapFsCapwapDnsProfileEntry));
            if ((CapwapInitializeFsCapwapDnsProfileTable
                 (pCapwapFsCapwapDnsProfileEntry)) == OSIX_FAILURE)
            {
                if (CapwapSetAllFsCapwapDnsProfileTableTrigger
                    (pCapwapSetFsCapwapDnsProfileEntry,
                     pCapwapIsSetFsCapwapDnsProfileEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCapwapDnsProfileTable:CapwapSetAllFsCapwapDnsProfileTableTrigger function fails\r\n");

                }
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCapwapDnsProfileTable: Fail to Initialize the Objects.\r\n");

                MemReleaseMemBlock (CAPWAP_FSCAPWAPDNSPROFILETABLE_POOLID,
                                    (UINT1 *) pCapwapFsCapwapDnsProfileEntry);
                MemReleaseMemBlock (CAPWAP_FSCAPWAPDNSPROFILETABLE_POOLID,
                                    (UINT1 *)
                                    pCapwapOldFsCapwapDnsProfileEntry);
                MemReleaseMemBlock (CAPWAP_FSCAPWAPDNSPROFILETABLE_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgFsCapwapDnsProfileEntry);
                MemReleaseMemBlock (CAPWAP_FSCAPWAPDNSPROFILETABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgIsSetFsCapwapDnsProfileEntry);
                return OSIX_FAILURE;
            }
            /* Assign values for the new node */
            if (pCapwapIsSetFsCapwapDnsProfileEntry->bFsCapwapDnsServerIp !=
                OSIX_FALSE)
            {
                MEMCPY (pCapwapFsCapwapDnsProfileEntry->MibObject.
                        au1FsCapwapDnsServerIp,
                        pCapwapSetFsCapwapDnsProfileEntry->MibObject.
                        au1FsCapwapDnsServerIp,
                        pCapwapSetFsCapwapDnsProfileEntry->MibObject.
                        i4FsCapwapDnsServerIpLen);

                pCapwapFsCapwapDnsProfileEntry->MibObject.
                    i4FsCapwapDnsServerIpLen =
                    pCapwapSetFsCapwapDnsProfileEntry->MibObject.
                    i4FsCapwapDnsServerIpLen;
            }

            if (pCapwapIsSetFsCapwapDnsProfileEntry->bFsCapwapDnsDomainName !=
                OSIX_FALSE)
            {
                MEMCPY (pCapwapFsCapwapDnsProfileEntry->MibObject.
                        au1FsCapwapDnsDomainName,
                        pCapwapSetFsCapwapDnsProfileEntry->MibObject.
                        au1FsCapwapDnsDomainName,
                        pCapwapSetFsCapwapDnsProfileEntry->MibObject.
                        i4FsCapwapDnsDomainNameLen);

                pCapwapFsCapwapDnsProfileEntry->MibObject.
                    i4FsCapwapDnsDomainNameLen =
                    pCapwapSetFsCapwapDnsProfileEntry->MibObject.
                    i4FsCapwapDnsDomainNameLen;
            }

            if (pCapwapIsSetFsCapwapDnsProfileEntry->
                bFsCapwapDnsProfileRowStatus != OSIX_FALSE)
            {
                pCapwapFsCapwapDnsProfileEntry->MibObject.
                    i4FsCapwapDnsProfileRowStatus =
                    pCapwapSetFsCapwapDnsProfileEntry->MibObject.
                    i4FsCapwapDnsProfileRowStatus;
            }

            if (pCapwapIsSetFsCapwapDnsProfileEntry->bCapwapBaseWtpProfileId !=
                OSIX_FALSE)
            {
                pCapwapFsCapwapDnsProfileEntry->MibObject.
                    u4CapwapBaseWtpProfileId =
                    pCapwapSetFsCapwapDnsProfileEntry->MibObject.
                    u4CapwapBaseWtpProfileId;
            }

            if ((pCapwapSetFsCapwapDnsProfileEntry->MibObject.
                 i4FsCapwapDnsProfileRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pCapwapSetFsCapwapDnsProfileEntry->MibObject.
                        i4FsCapwapDnsProfileRowStatus == ACTIVE)))
            {
                pCapwapFsCapwapDnsProfileEntry->MibObject.
                    i4FsCapwapDnsProfileRowStatus = ACTIVE;
            }
            else if (pCapwapSetFsCapwapDnsProfileEntry->MibObject.
                     i4FsCapwapDnsProfileRowStatus == CREATE_AND_WAIT)
            {
                pCapwapFsCapwapDnsProfileEntry->MibObject.
                    i4FsCapwapDnsProfileRowStatus = NOT_READY;
            }

            /* Add the new node to the database */
            if (RBTreeAdd
                (gCapwapGlobals.CapwapGlbMib.FsCapwapDnsProfileTable,
                 (tRBElem *) pCapwapFsCapwapDnsProfileEntry) != RB_SUCCESS)
            {
                if (CapwapSetAllFsCapwapDnsProfileTableTrigger
                    (pCapwapSetFsCapwapDnsProfileEntry,
                     pCapwapIsSetFsCapwapDnsProfileEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCapwapDnsProfileTable: CapwapSetAllFsCapwapDnsProfileTableTrigger function returns failure.\r\n");
                }
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCapwapDnsProfileTable: RBTreeAdd is failed.\r\n");

                MemReleaseMemBlock (CAPWAP_FSCAPWAPDNSPROFILETABLE_POOLID,
                                    (UINT1 *) pCapwapFsCapwapDnsProfileEntry);
                MemReleaseMemBlock (CAPWAP_FSCAPWAPDNSPROFILETABLE_POOLID,
                                    (UINT1 *)
                                    pCapwapOldFsCapwapDnsProfileEntry);
                MemReleaseMemBlock (CAPWAP_FSCAPWAPDNSPROFILETABLE_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgFsCapwapDnsProfileEntry);
                MemReleaseMemBlock (CAPWAP_FSCAPWAPDNSPROFILETABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgIsSetFsCapwapDnsProfileEntry);
                return OSIX_FAILURE;
            }
            if (CapwapUtilUpdateFsCapwapDnsProfileTable
                (NULL, pCapwapFsCapwapDnsProfileEntry,
                 pCapwapIsSetFsCapwapDnsProfileEntry) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCapwapDnsProfileTable: CapwapUtilUpdateFsCapwapDnsProfileTable function returns failure.\r\n");

                if (CapwapSetAllFsCapwapDnsProfileTableTrigger
                    (pCapwapSetFsCapwapDnsProfileEntry,
                     pCapwapIsSetFsCapwapDnsProfileEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCapwapDnsProfileTable: CapwapSetAllFsCapwapDnsProfileTableTrigger function returns failure.\r\n");

                }
                RBTreeRem (gCapwapGlobals.CapwapGlbMib.FsCapwapDnsProfileTable,
                           pCapwapFsCapwapDnsProfileEntry);
                MemReleaseMemBlock (CAPWAP_FSCAPWAPDNSPROFILETABLE_POOLID,
                                    (UINT1 *) pCapwapFsCapwapDnsProfileEntry);
                MemReleaseMemBlock (CAPWAP_FSCAPWAPDNSPROFILETABLE_POOLID,
                                    (UINT1 *)
                                    pCapwapOldFsCapwapDnsProfileEntry);
                MemReleaseMemBlock (CAPWAP_FSCAPWAPDNSPROFILETABLE_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgFsCapwapDnsProfileEntry);
                MemReleaseMemBlock (CAPWAP_FSCAPWAPDNSPROFILETABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgIsSetFsCapwapDnsProfileEntry);
                return OSIX_FAILURE;
            }

            if ((pCapwapSetFsCapwapDnsProfileEntry->MibObject.
                 i4FsCapwapDnsProfileRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pCapwapSetFsCapwapDnsProfileEntry->MibObject.
                        i4FsCapwapDnsProfileRowStatus == ACTIVE)))
            {

                /* For MSR and RM Trigger */
                pCapwapTrgFsCapwapDnsProfileEntry->MibObject.
                    u4CapwapBaseWtpProfileId =
                    pCapwapSetFsCapwapDnsProfileEntry->MibObject.
                    u4CapwapBaseWtpProfileId;
                pCapwapTrgFsCapwapDnsProfileEntry->MibObject.
                    i4FsCapwapDnsProfileRowStatus = CREATE_AND_WAIT;
                pCapwapTrgIsSetFsCapwapDnsProfileEntry->
                    bFsCapwapDnsProfileRowStatus = OSIX_TRUE;

                if (CapwapSetAllFsCapwapDnsProfileTableTrigger
                    (pCapwapTrgFsCapwapDnsProfileEntry,
                     pCapwapTrgIsSetFsCapwapDnsProfileEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCapwapDnsProfileTable: CapwapSetAllFsCapwapDnsProfileTableTrigger function returns failure.\r\n");

                    MemReleaseMemBlock (CAPWAP_FSCAPWAPDNSPROFILETABLE_POOLID,
                                        (UINT1 *)
                                        pCapwapFsCapwapDnsProfileEntry);
                    MemReleaseMemBlock (CAPWAP_FSCAPWAPDNSPROFILETABLE_POOLID,
                                        (UINT1 *)
                                        pCapwapOldFsCapwapDnsProfileEntry);
                    MemReleaseMemBlock (CAPWAP_FSCAPWAPDNSPROFILETABLE_POOLID,
                                        (UINT1 *)
                                        pCapwapTrgFsCapwapDnsProfileEntry);
                    MemReleaseMemBlock
                        (CAPWAP_FSCAPWAPDNSPROFILETABLE_ISSET_POOLID,
                         (UINT1 *) pCapwapTrgIsSetFsCapwapDnsProfileEntry);
                    return OSIX_FAILURE;
                }
            }
            else if (pCapwapSetFsCapwapDnsProfileEntry->MibObject.
                     i4FsCapwapDnsProfileRowStatus == CREATE_AND_WAIT)
            {

                /* For MSR and RM Trigger */
                pCapwapTrgFsCapwapDnsProfileEntry->MibObject.
                    i4FsCapwapDnsProfileRowStatus = CREATE_AND_WAIT;
                pCapwapTrgIsSetFsCapwapDnsProfileEntry->
                    bFsCapwapDnsProfileRowStatus = OSIX_TRUE;

                if (CapwapSetAllFsCapwapDnsProfileTableTrigger
                    (pCapwapTrgFsCapwapDnsProfileEntry,
                     pCapwapTrgIsSetFsCapwapDnsProfileEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCapwapDnsProfileTable: CapwapSetAllFsCapwapDnsProfileTableTrigger function returns failure.\r\n");

                    MemReleaseMemBlock (CAPWAP_FSCAPWAPDNSPROFILETABLE_POOLID,
                                        (UINT1 *)
                                        pCapwapFsCapwapDnsProfileEntry);
                    MemReleaseMemBlock (CAPWAP_FSCAPWAPDNSPROFILETABLE_POOLID,
                                        (UINT1 *)
                                        pCapwapOldFsCapwapDnsProfileEntry);
                    MemReleaseMemBlock (CAPWAP_FSCAPWAPDNSPROFILETABLE_POOLID,
                                        (UINT1 *)
                                        pCapwapTrgFsCapwapDnsProfileEntry);
                    MemReleaseMemBlock
                        (CAPWAP_FSCAPWAPDNSPROFILETABLE_ISSET_POOLID,
                         (UINT1 *) pCapwapTrgIsSetFsCapwapDnsProfileEntry);
                    return OSIX_FAILURE;
                }
            }

            if (pCapwapSetFsCapwapDnsProfileEntry->MibObject.
                i4FsCapwapDnsProfileRowStatus == CREATE_AND_GO)
            {
                pCapwapSetFsCapwapDnsProfileEntry->MibObject.
                    i4FsCapwapDnsProfileRowStatus = ACTIVE;
            }

            if (CapwapSetAllFsCapwapDnsProfileTableTrigger
                (pCapwapSetFsCapwapDnsProfileEntry,
                 pCapwapIsSetFsCapwapDnsProfileEntry,
                 SNMP_SUCCESS) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCapwapDnsProfileTable:  CapwapSetAllFsCapwapDnsProfileTableTrigger function returns failure.\r\n");
            }
            MemReleaseMemBlock (CAPWAP_FSCAPWAPDNSPROFILETABLE_POOLID,
                                (UINT1 *) pCapwapOldFsCapwapDnsProfileEntry);
            MemReleaseMemBlock (CAPWAP_FSCAPWAPDNSPROFILETABLE_POOLID,
                                (UINT1 *) pCapwapTrgFsCapwapDnsProfileEntry);
            MemReleaseMemBlock (CAPWAP_FSCAPWAPDNSPROFILETABLE_ISSET_POOLID,
                                (UINT1 *)
                                pCapwapTrgIsSetFsCapwapDnsProfileEntry);
            return OSIX_SUCCESS;

        }
        else
        {
            if (CapwapSetAllFsCapwapDnsProfileTableTrigger
                (pCapwapSetFsCapwapDnsProfileEntry,
                 pCapwapIsSetFsCapwapDnsProfileEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCapwapDnsProfileTable: CapwapSetAllFsCapwapDnsProfileTableTrigger function returns failure.\r\n");
            }
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCapwapDnsProfileTable: Failure.\r\n");
            MemReleaseMemBlock (CAPWAP_FSCAPWAPDNSPROFILETABLE_POOLID,
                                (UINT1 *) pCapwapOldFsCapwapDnsProfileEntry);
            MemReleaseMemBlock (CAPWAP_FSCAPWAPDNSPROFILETABLE_POOLID,
                                (UINT1 *) pCapwapTrgFsCapwapDnsProfileEntry);
            MemReleaseMemBlock (CAPWAP_FSCAPWAPDNSPROFILETABLE_ISSET_POOLID,
                                (UINT1 *)
                                pCapwapTrgIsSetFsCapwapDnsProfileEntry);
            return OSIX_FAILURE;
        }
    }
    else if ((pCapwapSetFsCapwapDnsProfileEntry->MibObject.
              i4FsCapwapDnsProfileRowStatus == CREATE_AND_WAIT)
             || (pCapwapSetFsCapwapDnsProfileEntry->MibObject.
                 i4FsCapwapDnsProfileRowStatus == CREATE_AND_GO))
    {
        if (CapwapSetAllFsCapwapDnsProfileTableTrigger
            (pCapwapSetFsCapwapDnsProfileEntry,
             pCapwapIsSetFsCapwapDnsProfileEntry, SNMP_FAILURE) != OSIX_SUCCESS)

        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCapwapDnsProfileTable: CapwapSetAllFsCapwapDnsProfileTableTrigger function returns failure.\r\n");
        }
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapSetAllFsCapwapDnsProfileTable: The row is already present.\r\n");
        MemReleaseMemBlock (CAPWAP_FSCAPWAPDNSPROFILETABLE_POOLID,
                            (UINT1 *) pCapwapOldFsCapwapDnsProfileEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPDNSPROFILETABLE_POOLID,
                            (UINT1 *) pCapwapTrgFsCapwapDnsProfileEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPDNSPROFILETABLE_ISSET_POOLID,
                            (UINT1 *) pCapwapTrgIsSetFsCapwapDnsProfileEntry);
        return OSIX_FAILURE;
    }
    /* Copy the previous values before setting the new values */
    MEMCPY (pCapwapOldFsCapwapDnsProfileEntry, pCapwapFsCapwapDnsProfileEntry,
            sizeof (tCapwapFsCapwapDnsProfileEntry));

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pCapwapSetFsCapwapDnsProfileEntry->MibObject.
        i4FsCapwapDnsProfileRowStatus == DESTROY)
    {
        pCapwapFsCapwapDnsProfileEntry->MibObject.
            i4FsCapwapDnsProfileRowStatus = DESTROY;

        if (CapwapUtilUpdateFsCapwapDnsProfileTable
            (pCapwapOldFsCapwapDnsProfileEntry, pCapwapFsCapwapDnsProfileEntry,
             pCapwapIsSetFsCapwapDnsProfileEntry) != OSIX_SUCCESS)
        {

            if (CapwapSetAllFsCapwapDnsProfileTableTrigger
                (pCapwapSetFsCapwapDnsProfileEntry,
                 pCapwapIsSetFsCapwapDnsProfileEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCapwapDnsProfileTable: CapwapSetAllFsCapwapDnsProfileTableTrigger function returns failure.\r\n");
            }
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCapwapDnsProfileTable: CapwapUtilUpdateFsCapwapDnsProfileTable function returns failure.\r\n");
        }
        RBTreeRem (gCapwapGlobals.CapwapGlbMib.FsCapwapDnsProfileTable,
                   pCapwapFsCapwapDnsProfileEntry);
        if (CapwapSetAllFsCapwapDnsProfileTableTrigger
            (pCapwapSetFsCapwapDnsProfileEntry,
             pCapwapIsSetFsCapwapDnsProfileEntry, SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCapwapDnsProfileTable: CapwapSetAllFsCapwapDnsProfileTableTrigger function returns failure.\r\n");
            MemReleaseMemBlock (CAPWAP_FSCAPWAPDNSPROFILETABLE_POOLID,
                                (UINT1 *) pCapwapOldFsCapwapDnsProfileEntry);
            MemReleaseMemBlock (CAPWAP_FSCAPWAPDNSPROFILETABLE_POOLID,
                                (UINT1 *) pCapwapTrgFsCapwapDnsProfileEntry);
            MemReleaseMemBlock (CAPWAP_FSCAPWAPDNSPROFILETABLE_ISSET_POOLID,
                                (UINT1 *)
                                pCapwapTrgIsSetFsCapwapDnsProfileEntry);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (CAPWAP_FSCAPWAPDNSPROFILETABLE_POOLID,
                            (UINT1 *) pCapwapFsCapwapDnsProfileEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPDNSPROFILETABLE_POOLID,
                            (UINT1 *) pCapwapOldFsCapwapDnsProfileEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPDNSPROFILETABLE_POOLID,
                            (UINT1 *) pCapwapTrgFsCapwapDnsProfileEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPDNSPROFILETABLE_ISSET_POOLID,
                            (UINT1 *) pCapwapTrgIsSetFsCapwapDnsProfileEntry);
        return OSIX_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (FsCapwapDnsProfileTableFilterInputs
        (pCapwapFsCapwapDnsProfileEntry, pCapwapSetFsCapwapDnsProfileEntry,
         pCapwapIsSetFsCapwapDnsProfileEntry) != OSIX_TRUE)
    {
        MemReleaseMemBlock (CAPWAP_FSCAPWAPDNSPROFILETABLE_POOLID,
                            (UINT1 *) pCapwapOldFsCapwapDnsProfileEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPDNSPROFILETABLE_POOLID,
                            (UINT1 *) pCapwapTrgFsCapwapDnsProfileEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPDNSPROFILETABLE_ISSET_POOLID,
                            (UINT1 *) pCapwapTrgIsSetFsCapwapDnsProfileEntry);
        return OSIX_SUCCESS;
    }

    /*This condtion is to make the row NOT_IN_SERVICE before modifying the values */
    if ((i4RowStatusLogic == TRUE) &&
        (pCapwapFsCapwapDnsProfileEntry->MibObject.
         i4FsCapwapDnsProfileRowStatus == ACTIVE)
        && (pCapwapSetFsCapwapDnsProfileEntry->MibObject.
            i4FsCapwapDnsProfileRowStatus != NOT_IN_SERVICE))
    {
        pCapwapFsCapwapDnsProfileEntry->MibObject.
            i4FsCapwapDnsProfileRowStatus = NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        pCapwapTrgFsCapwapDnsProfileEntry->MibObject.
            i4FsCapwapDnsProfileRowStatus = NOT_IN_SERVICE;
        pCapwapTrgIsSetFsCapwapDnsProfileEntry->bFsCapwapDnsProfileRowStatus =
            OSIX_TRUE;

        if (CapwapUtilUpdateFsCapwapDnsProfileTable
            (pCapwapOldFsCapwapDnsProfileEntry, pCapwapFsCapwapDnsProfileEntry,
             pCapwapIsSetFsCapwapDnsProfileEntry) != OSIX_SUCCESS)
        {
            /*Restore back with previous values */
            MEMCPY (pCapwapFsCapwapDnsProfileEntry,
                    pCapwapOldFsCapwapDnsProfileEntry,
                    sizeof (tCapwapFsCapwapDnsProfileEntry));
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCapwapDnsProfileTable:                 CapwapUtilUpdateFsCapwapDnsProfileTable Function returns failure.\r\n");

            if (CapwapSetAllFsCapwapDnsProfileTableTrigger
                (pCapwapSetFsCapwapDnsProfileEntry,
                 pCapwapIsSetFsCapwapDnsProfileEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCapwapDnsProfileTable: CapwapSetAllFsCapwapDnsProfileTableTrigger function returns failure.\r\n");
            }
            MemReleaseMemBlock (CAPWAP_FSCAPWAPDNSPROFILETABLE_POOLID,
                                (UINT1 *) pCapwapOldFsCapwapDnsProfileEntry);
            MemReleaseMemBlock (CAPWAP_FSCAPWAPDNSPROFILETABLE_POOLID,
                                (UINT1 *) pCapwapTrgFsCapwapDnsProfileEntry);
            MemReleaseMemBlock (CAPWAP_FSCAPWAPDNSPROFILETABLE_ISSET_POOLID,
                                (UINT1 *)
                                pCapwapTrgIsSetFsCapwapDnsProfileEntry);
            return OSIX_FAILURE;
        }

        if (CapwapSetAllFsCapwapDnsProfileTableTrigger
            (pCapwapTrgFsCapwapDnsProfileEntry,
             pCapwapTrgIsSetFsCapwapDnsProfileEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCapwapDnsProfileTable: CapwapSetAllFsCapwapDnsProfileTableTrigger function returns failure.\r\n");
        }
    }

    if (pCapwapSetFsCapwapDnsProfileEntry->MibObject.
        i4FsCapwapDnsProfileRowStatus == ACTIVE)
    {
        i4RowMakeActive = TRUE;
    }

    /* Assign values for the existing node */
    if (pCapwapIsSetFsCapwapDnsProfileEntry->bFsCapwapDnsServerIp != OSIX_FALSE)
    {
        MEMCPY (pCapwapFsCapwapDnsProfileEntry->MibObject.
                au1FsCapwapDnsServerIp,
                pCapwapSetFsCapwapDnsProfileEntry->MibObject.
                au1FsCapwapDnsServerIp,
                pCapwapSetFsCapwapDnsProfileEntry->MibObject.
                i4FsCapwapDnsServerIpLen);

        pCapwapFsCapwapDnsProfileEntry->MibObject.i4FsCapwapDnsServerIpLen =
            pCapwapSetFsCapwapDnsProfileEntry->MibObject.
            i4FsCapwapDnsServerIpLen;
    }
    if (pCapwapIsSetFsCapwapDnsProfileEntry->bFsCapwapDnsDomainName !=
        OSIX_FALSE)
    {
        MEMCPY (pCapwapFsCapwapDnsProfileEntry->MibObject.
                au1FsCapwapDnsDomainName,
                pCapwapSetFsCapwapDnsProfileEntry->MibObject.
                au1FsCapwapDnsDomainName,
                pCapwapSetFsCapwapDnsProfileEntry->MibObject.
                i4FsCapwapDnsDomainNameLen);

        pCapwapFsCapwapDnsProfileEntry->MibObject.i4FsCapwapDnsDomainNameLen =
            pCapwapSetFsCapwapDnsProfileEntry->MibObject.
            i4FsCapwapDnsDomainNameLen;
    }
    if (pCapwapIsSetFsCapwapDnsProfileEntry->bFsCapwapDnsProfileRowStatus !=
        OSIX_FALSE)
    {
        pCapwapFsCapwapDnsProfileEntry->MibObject.
            i4FsCapwapDnsProfileRowStatus =
            pCapwapSetFsCapwapDnsProfileEntry->MibObject.
            i4FsCapwapDnsProfileRowStatus;
    }

    /*This condtion is to make the row back to ACTIVE after modifying the values */
    if (i4RowMakeActive == TRUE)
    {
        pCapwapFsCapwapDnsProfileEntry->MibObject.
            i4FsCapwapDnsProfileRowStatus = ACTIVE;
    }

    if (CapwapUtilUpdateFsCapwapDnsProfileTable
        (pCapwapOldFsCapwapDnsProfileEntry, pCapwapFsCapwapDnsProfileEntry,
         pCapwapIsSetFsCapwapDnsProfileEntry) != OSIX_SUCCESS)
    {

        if (CapwapSetAllFsCapwapDnsProfileTableTrigger
            (pCapwapSetFsCapwapDnsProfileEntry,
             pCapwapIsSetFsCapwapDnsProfileEntry, SNMP_FAILURE) != OSIX_SUCCESS)

        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCapwapDnsProfileTable: CapwapSetAllFsCapwapDnsProfileTableTrigger function returns failure.\r\n");

        }
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapSetAllFsCapwapDnsProfileTable: CapwapUtilUpdateFsCapwapDnsProfileTable function returns failure.\r\n");
        /*Restore back with previous values */
        MEMCPY (pCapwapFsCapwapDnsProfileEntry,
                pCapwapOldFsCapwapDnsProfileEntry,
                sizeof (tCapwapFsCapwapDnsProfileEntry));
        MemReleaseMemBlock (CAPWAP_FSCAPWAPDNSPROFILETABLE_POOLID,
                            (UINT1 *) pCapwapOldFsCapwapDnsProfileEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPDNSPROFILETABLE_POOLID,
                            (UINT1 *) pCapwapTrgFsCapwapDnsProfileEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPDNSPROFILETABLE_ISSET_POOLID,
                            (UINT1 *) pCapwapTrgIsSetFsCapwapDnsProfileEntry);
        return OSIX_FAILURE;

    }
    if (CapwapSetAllFsCapwapDnsProfileTableTrigger
        (pCapwapSetFsCapwapDnsProfileEntry, pCapwapIsSetFsCapwapDnsProfileEntry,
         SNMP_SUCCESS) != OSIX_SUCCESS)
    {

        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapSetAllFsCapwapDnsProfileTable: CapwapSetAllFsCapwapDnsProfileTableTrigger function returns failure.\r\n");
    }

    MemReleaseMemBlock (CAPWAP_FSCAPWAPDNSPROFILETABLE_POOLID,
                        (UINT1 *) pCapwapOldFsCapwapDnsProfileEntry);
    MemReleaseMemBlock (CAPWAP_FSCAPWAPDNSPROFILETABLE_POOLID,
                        (UINT1 *) pCapwapTrgFsCapwapDnsProfileEntry);
    MemReleaseMemBlock (CAPWAP_FSCAPWAPDNSPROFILETABLE_ISSET_POOLID,
                        (UINT1 *) pCapwapTrgIsSetFsCapwapDnsProfileEntry);
    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  CapwapSetAllFsWtpNativeVlanIdTable
 Input       :  pCapwapSetFsWtpNativeVlanIdTable
                pCapwapIsSetFsWtpNativeVlanIdTable
                i4RowStatusLogic
                i4RowCreateOption
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapSetAllFsWtpNativeVlanIdTable (tCapwapFsWtpNativeVlanIdTable *
                                    pCapwapSetFsWtpNativeVlanIdTable,
                                    tCapwapIsSetFsWtpNativeVlanIdTable *
                                    pCapwapIsSetFsWtpNativeVlanIdTable,
                                    INT4 i4RowStatusLogic,
                                    INT4 i4RowCreateOption)
{
    tCapwapFsWtpNativeVlanIdTable *pCapwapFsWtpNativeVlanIdTable = NULL;
    tCapwapFsWtpNativeVlanIdTable *pCapwapOldFsWtpNativeVlanIdTable = NULL;
    tCapwapFsWtpNativeVlanIdTable *pCapwapTrgFsWtpNativeVlanIdTable = NULL;
    tCapwapIsSetFsWtpNativeVlanIdTable *pCapwapTrgIsSetFsWtpNativeVlanIdTable =
        NULL;
    INT4                i4RowMakeActive = FALSE;

    pCapwapOldFsWtpNativeVlanIdTable =
        (tCapwapFsWtpNativeVlanIdTable *)
        MemAllocMemBlk (CAPWAP_FSWTPNATIVEVLANIDTABLE_POOLID);
    if (pCapwapOldFsWtpNativeVlanIdTable == NULL)
    {
        return OSIX_FAILURE;
    }
    pCapwapTrgFsWtpNativeVlanIdTable =
        (tCapwapFsWtpNativeVlanIdTable *)
        MemAllocMemBlk (CAPWAP_FSWTPNATIVEVLANIDTABLE_POOLID);
    if (pCapwapTrgFsWtpNativeVlanIdTable == NULL)
    {
        MemReleaseMemBlock (CAPWAP_FSWTPNATIVEVLANIDTABLE_POOLID,
                            (UINT1 *) pCapwapOldFsWtpNativeVlanIdTable);
        return OSIX_FAILURE;
    }
    pCapwapTrgIsSetFsWtpNativeVlanIdTable =
        (tCapwapIsSetFsWtpNativeVlanIdTable *)
        MemAllocMemBlk (CAPWAP_FSWTPNATIVEVLANIDTABLE_ISSET_POOLID);
    if (pCapwapTrgIsSetFsWtpNativeVlanIdTable == NULL)
    {
        MemReleaseMemBlock (CAPWAP_FSWTPNATIVEVLANIDTABLE_POOLID,
                            (UINT1 *) pCapwapOldFsWtpNativeVlanIdTable);
        MemReleaseMemBlock (CAPWAP_FSWTPNATIVEVLANIDTABLE_POOLID,
                            (UINT1 *) pCapwapTrgFsWtpNativeVlanIdTable);
        return OSIX_FAILURE;
    }
    MEMSET (pCapwapOldFsWtpNativeVlanIdTable, 0,
            sizeof (tCapwapFsWtpNativeVlanIdTable));
    MEMSET (pCapwapTrgFsWtpNativeVlanIdTable, 0,
            sizeof (tCapwapFsWtpNativeVlanIdTable));
    MEMSET (pCapwapTrgIsSetFsWtpNativeVlanIdTable, 0,
            sizeof (tCapwapIsSetFsWtpNativeVlanIdTable));

    /* Check whether the node is already present */
    pCapwapFsWtpNativeVlanIdTable =
        RBTreeGet (gCapwapGlobals.CapwapGlbMib.FsWtpNativeVlanIdTable,
                   (tRBElem *) pCapwapSetFsWtpNativeVlanIdTable);

    if (pCapwapFsWtpNativeVlanIdTable == NULL)
    {
        /* Create the node if the RowStatus given is CREATE_AND_WAIT or CREATE_AND_GO */
        if ((pCapwapSetFsWtpNativeVlanIdTable->MibObject.
             i4FsWtpNativeVlanIdRowStatus == CREATE_AND_WAIT)
            || (pCapwapSetFsWtpNativeVlanIdTable->MibObject.
                i4FsWtpNativeVlanIdRowStatus == CREATE_AND_GO)
            ||
            ((pCapwapSetFsWtpNativeVlanIdTable->MibObject.
              i4FsWtpNativeVlanIdRowStatus == ACTIVE)
             && (i4RowCreateOption == 1)))
        {
            /* Allocate memory for the new node */
            pCapwapFsWtpNativeVlanIdTable =
                (tCapwapFsWtpNativeVlanIdTable *)
                MemAllocMemBlk (CAPWAP_FSWTPNATIVEVLANIDTABLE_POOLID);
            if (pCapwapFsWtpNativeVlanIdTable == NULL)
            {
                if (CapwapSetAllFsWtpNativeVlanIdTableTrigger
                    (pCapwapSetFsWtpNativeVlanIdTable,
                     pCapwapIsSetFsWtpNativeVlanIdTable,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsWtpNativeVlanIdTable:CapwapSetAllFsWtpNativeVlanIdTableTrigger function fails\r\n");

                }
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsWtpNativeVlanIdTable: Fail to Allocate Memory.\r\n");
                MemReleaseMemBlock (CAPWAP_FSWTPNATIVEVLANIDTABLE_POOLID,
                                    (UINT1 *) pCapwapOldFsWtpNativeVlanIdTable);
                MemReleaseMemBlock (CAPWAP_FSWTPNATIVEVLANIDTABLE_POOLID,
                                    (UINT1 *) pCapwapTrgFsWtpNativeVlanIdTable);
                MemReleaseMemBlock (CAPWAP_FSWTPNATIVEVLANIDTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgIsSetFsWtpNativeVlanIdTable);
                return OSIX_FAILURE;
            }

            MEMSET (pCapwapFsWtpNativeVlanIdTable, 0,
                    sizeof (tCapwapFsWtpNativeVlanIdTable));
            if ((CapwapInitializeFsWtpNativeVlanIdTable
                 (pCapwapFsWtpNativeVlanIdTable)) == OSIX_FAILURE)
            {
                if (CapwapSetAllFsWtpNativeVlanIdTableTrigger
                    (pCapwapSetFsWtpNativeVlanIdTable,
                     pCapwapIsSetFsWtpNativeVlanIdTable,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsWtpNativeVlanIdTable:CapwapSetAllFsWtpNativeVlanIdTableTrigger function fails\r\n");

                }
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsWtpNativeVlanIdTable: Fail to Initialize the Objects.\r\n");

                MemReleaseMemBlock (CAPWAP_FSWTPNATIVEVLANIDTABLE_POOLID,
                                    (UINT1 *) pCapwapFsWtpNativeVlanIdTable);
                MemReleaseMemBlock (CAPWAP_FSWTPNATIVEVLANIDTABLE_POOLID,
                                    (UINT1 *) pCapwapOldFsWtpNativeVlanIdTable);
                MemReleaseMemBlock (CAPWAP_FSWTPNATIVEVLANIDTABLE_POOLID,
                                    (UINT1 *) pCapwapTrgFsWtpNativeVlanIdTable);
                MemReleaseMemBlock (CAPWAP_FSWTPNATIVEVLANIDTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgIsSetFsWtpNativeVlanIdTable);
                return OSIX_FAILURE;
            }
            /* Assign values for the new node */
            if (pCapwapIsSetFsWtpNativeVlanIdTable->bFsWtpNativeVlanId !=
                OSIX_FALSE)
            {
                pCapwapFsWtpNativeVlanIdTable->MibObject.i4FsWtpNativeVlanId =
                    pCapwapSetFsWtpNativeVlanIdTable->MibObject.
                    i4FsWtpNativeVlanId;
            }

            if (pCapwapIsSetFsWtpNativeVlanIdTable->
                bFsWtpNativeVlanIdRowStatus != OSIX_FALSE)
            {
                pCapwapFsWtpNativeVlanIdTable->MibObject.
                    i4FsWtpNativeVlanIdRowStatus =
                    pCapwapSetFsWtpNativeVlanIdTable->MibObject.
                    i4FsWtpNativeVlanIdRowStatus;
            }

            if (pCapwapIsSetFsWtpNativeVlanIdTable->bCapwapBaseWtpProfileId !=
                OSIX_FALSE)
            {
                pCapwapFsWtpNativeVlanIdTable->MibObject.
                    u4CapwapBaseWtpProfileId =
                    pCapwapSetFsWtpNativeVlanIdTable->MibObject.
                    u4CapwapBaseWtpProfileId;
            }

            if ((pCapwapSetFsWtpNativeVlanIdTable->MibObject.
                 i4FsWtpNativeVlanIdRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pCapwapSetFsWtpNativeVlanIdTable->MibObject.
                        i4FsWtpNativeVlanIdRowStatus == ACTIVE)))
            {
                pCapwapFsWtpNativeVlanIdTable->MibObject.
                    i4FsWtpNativeVlanIdRowStatus = ACTIVE;
            }
            else if (pCapwapSetFsWtpNativeVlanIdTable->MibObject.
                     i4FsWtpNativeVlanIdRowStatus == CREATE_AND_WAIT)
            {
                pCapwapFsWtpNativeVlanIdTable->MibObject.
                    i4FsWtpNativeVlanIdRowStatus = NOT_READY;
            }

            /* Add the new node to the database */
            if (RBTreeAdd
                (gCapwapGlobals.CapwapGlbMib.FsWtpNativeVlanIdTable,
                 (tRBElem *) pCapwapFsWtpNativeVlanIdTable) != RB_SUCCESS)
            {
                if (CapwapSetAllFsWtpNativeVlanIdTableTrigger
                    (pCapwapSetFsWtpNativeVlanIdTable,
                     pCapwapIsSetFsWtpNativeVlanIdTable,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsWtpNativeVlanIdTable: CapwapSetAllFsWtpNativeVlanIdTableTrigger function returns failure.\r\n");
                }
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsWtpNativeVlanIdTable: RBTreeAdd is failed.\r\n");

                MemReleaseMemBlock (CAPWAP_FSWTPNATIVEVLANIDTABLE_POOLID,
                                    (UINT1 *) pCapwapFsWtpNativeVlanIdTable);
                MemReleaseMemBlock (CAPWAP_FSWTPNATIVEVLANIDTABLE_POOLID,
                                    (UINT1 *) pCapwapOldFsWtpNativeVlanIdTable);
                MemReleaseMemBlock (CAPWAP_FSWTPNATIVEVLANIDTABLE_POOLID,
                                    (UINT1 *) pCapwapTrgFsWtpNativeVlanIdTable);
                MemReleaseMemBlock (CAPWAP_FSWTPNATIVEVLANIDTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgIsSetFsWtpNativeVlanIdTable);
                return OSIX_FAILURE;
            }
            if (CapwapUtilUpdateFsWtpNativeVlanIdTable
                (NULL, pCapwapFsWtpNativeVlanIdTable,
                 pCapwapIsSetFsWtpNativeVlanIdTable) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsWtpNativeVlanIdTable: CapwapUtilUpdateFsWtpNativeVlanIdTable function returns failure.\r\n");

                if (CapwapSetAllFsWtpNativeVlanIdTableTrigger
                    (pCapwapSetFsWtpNativeVlanIdTable,
                     pCapwapIsSetFsWtpNativeVlanIdTable,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsWtpNativeVlanIdTable: CapwapSetAllFsWtpNativeVlanIdTableTrigger function returns failure.\r\n");

                }
                RBTreeRem (gCapwapGlobals.CapwapGlbMib.FsWtpNativeVlanIdTable,
                           pCapwapFsWtpNativeVlanIdTable);
                MemReleaseMemBlock (CAPWAP_FSWTPNATIVEVLANIDTABLE_POOLID,
                                    (UINT1 *) pCapwapFsWtpNativeVlanIdTable);
                MemReleaseMemBlock (CAPWAP_FSWTPNATIVEVLANIDTABLE_POOLID,
                                    (UINT1 *) pCapwapOldFsWtpNativeVlanIdTable);
                MemReleaseMemBlock (CAPWAP_FSWTPNATIVEVLANIDTABLE_POOLID,
                                    (UINT1 *) pCapwapTrgFsWtpNativeVlanIdTable);
                MemReleaseMemBlock (CAPWAP_FSWTPNATIVEVLANIDTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgIsSetFsWtpNativeVlanIdTable);
                return OSIX_FAILURE;
            }

            if ((pCapwapSetFsWtpNativeVlanIdTable->MibObject.
                 i4FsWtpNativeVlanIdRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pCapwapSetFsWtpNativeVlanIdTable->MibObject.
                        i4FsWtpNativeVlanIdRowStatus == ACTIVE)))
            {

                /* For MSR and RM Trigger */
                pCapwapTrgFsWtpNativeVlanIdTable->MibObject.
                    u4CapwapBaseWtpProfileId =
                    pCapwapSetFsWtpNativeVlanIdTable->MibObject.
                    u4CapwapBaseWtpProfileId;
                pCapwapTrgFsWtpNativeVlanIdTable->MibObject.
                    i4FsWtpNativeVlanIdRowStatus = CREATE_AND_WAIT;
                pCapwapTrgIsSetFsWtpNativeVlanIdTable->
                    bFsWtpNativeVlanIdRowStatus = OSIX_TRUE;

                if (CapwapSetAllFsWtpNativeVlanIdTableTrigger
                    (pCapwapTrgFsWtpNativeVlanIdTable,
                     pCapwapTrgIsSetFsWtpNativeVlanIdTable,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsWtpNativeVlanIdTable: CapwapSetAllFsWtpNativeVlanIdTableTrigger function returns failure.\r\n");

                    MemReleaseMemBlock (CAPWAP_FSWTPNATIVEVLANIDTABLE_POOLID,
                                        (UINT1 *)
                                        pCapwapFsWtpNativeVlanIdTable);
                    MemReleaseMemBlock (CAPWAP_FSWTPNATIVEVLANIDTABLE_POOLID,
                                        (UINT1 *)
                                        pCapwapOldFsWtpNativeVlanIdTable);
                    MemReleaseMemBlock (CAPWAP_FSWTPNATIVEVLANIDTABLE_POOLID,
                                        (UINT1 *)
                                        pCapwapTrgFsWtpNativeVlanIdTable);
                    MemReleaseMemBlock
                        (CAPWAP_FSWTPNATIVEVLANIDTABLE_ISSET_POOLID,
                         (UINT1 *) pCapwapTrgIsSetFsWtpNativeVlanIdTable);
                    return OSIX_FAILURE;
                }
            }
            else if (pCapwapSetFsWtpNativeVlanIdTable->MibObject.
                     i4FsWtpNativeVlanIdRowStatus == CREATE_AND_WAIT)
            {

                /* For MSR and RM Trigger */
                pCapwapTrgFsWtpNativeVlanIdTable->MibObject.
                    i4FsWtpNativeVlanIdRowStatus = CREATE_AND_WAIT;
                pCapwapTrgIsSetFsWtpNativeVlanIdTable->
                    bFsWtpNativeVlanIdRowStatus = OSIX_TRUE;

                if (CapwapSetAllFsWtpNativeVlanIdTableTrigger
                    (pCapwapTrgFsWtpNativeVlanIdTable,
                     pCapwapTrgIsSetFsWtpNativeVlanIdTable,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsWtpNativeVlanIdTable: CapwapSetAllFsWtpNativeVlanIdTableTrigger function returns failure.\r\n");

                    MemReleaseMemBlock (CAPWAP_FSWTPNATIVEVLANIDTABLE_POOLID,
                                        (UINT1 *)
                                        pCapwapFsWtpNativeVlanIdTable);
                    MemReleaseMemBlock (CAPWAP_FSWTPNATIVEVLANIDTABLE_POOLID,
                                        (UINT1 *)
                                        pCapwapOldFsWtpNativeVlanIdTable);
                    MemReleaseMemBlock (CAPWAP_FSWTPNATIVEVLANIDTABLE_POOLID,
                                        (UINT1 *)
                                        pCapwapTrgFsWtpNativeVlanIdTable);
                    MemReleaseMemBlock
                        (CAPWAP_FSWTPNATIVEVLANIDTABLE_ISSET_POOLID,
                         (UINT1 *) pCapwapTrgIsSetFsWtpNativeVlanIdTable);
                    return OSIX_FAILURE;
                }
            }

            if (pCapwapSetFsWtpNativeVlanIdTable->MibObject.
                i4FsWtpNativeVlanIdRowStatus == CREATE_AND_GO)
            {
                pCapwapSetFsWtpNativeVlanIdTable->MibObject.
                    i4FsWtpNativeVlanIdRowStatus = ACTIVE;
            }

            if (CapwapSetAllFsWtpNativeVlanIdTableTrigger
                (pCapwapSetFsWtpNativeVlanIdTable,
                 pCapwapIsSetFsWtpNativeVlanIdTable,
                 SNMP_SUCCESS) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsWtpNativeVlanIdTable:  CapwapSetAllFsWtpNativeVlanIdTableTrigger function returns failure.\r\n");
            }
            MemReleaseMemBlock (CAPWAP_FSWTPNATIVEVLANIDTABLE_POOLID,
                                (UINT1 *) pCapwapOldFsWtpNativeVlanIdTable);
            MemReleaseMemBlock (CAPWAP_FSWTPNATIVEVLANIDTABLE_POOLID,
                                (UINT1 *) pCapwapTrgFsWtpNativeVlanIdTable);
            MemReleaseMemBlock (CAPWAP_FSWTPNATIVEVLANIDTABLE_ISSET_POOLID,
                                (UINT1 *)
                                pCapwapTrgIsSetFsWtpNativeVlanIdTable);
            return OSIX_SUCCESS;

        }
        else
        {
            if (CapwapSetAllFsWtpNativeVlanIdTableTrigger
                (pCapwapSetFsWtpNativeVlanIdTable,
                 pCapwapIsSetFsWtpNativeVlanIdTable,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsWtpNativeVlanIdTable: CapwapSetAllFsWtpNativeVlanIdTableTrigger function returns failure.\r\n");
            }
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsWtpNativeVlanIdTable: Failure.\r\n");
            MemReleaseMemBlock (CAPWAP_FSWTPNATIVEVLANIDTABLE_POOLID,
                                (UINT1 *) pCapwapOldFsWtpNativeVlanIdTable);
            MemReleaseMemBlock (CAPWAP_FSWTPNATIVEVLANIDTABLE_POOLID,
                                (UINT1 *) pCapwapTrgFsWtpNativeVlanIdTable);
            MemReleaseMemBlock (CAPWAP_FSWTPNATIVEVLANIDTABLE_ISSET_POOLID,
                                (UINT1 *)
                                pCapwapTrgIsSetFsWtpNativeVlanIdTable);
            return OSIX_FAILURE;
        }
    }
    else if ((pCapwapSetFsWtpNativeVlanIdTable->MibObject.
              i4FsWtpNativeVlanIdRowStatus == CREATE_AND_WAIT)
             || (pCapwapSetFsWtpNativeVlanIdTable->MibObject.
                 i4FsWtpNativeVlanIdRowStatus == CREATE_AND_GO))
    {
        if (CapwapSetAllFsWtpNativeVlanIdTableTrigger
            (pCapwapSetFsWtpNativeVlanIdTable,
             pCapwapIsSetFsWtpNativeVlanIdTable, SNMP_FAILURE) != OSIX_SUCCESS)

        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsWtpNativeVlanIdTable: CapwapSetAllFsWtpNativeVlanIdTableTrigger function returns failure.\r\n");
        }
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapSetAllFsWtpNativeVlanIdTable: The row is already present.\r\n");
        MemReleaseMemBlock (CAPWAP_FSWTPNATIVEVLANIDTABLE_POOLID,
                            (UINT1 *) pCapwapOldFsWtpNativeVlanIdTable);
        MemReleaseMemBlock (CAPWAP_FSWTPNATIVEVLANIDTABLE_POOLID,
                            (UINT1 *) pCapwapTrgFsWtpNativeVlanIdTable);
        MemReleaseMemBlock (CAPWAP_FSWTPNATIVEVLANIDTABLE_ISSET_POOLID,
                            (UINT1 *) pCapwapTrgIsSetFsWtpNativeVlanIdTable);
        return OSIX_FAILURE;
    }
    /* Copy the previous values before setting the new values */
    MEMCPY (pCapwapOldFsWtpNativeVlanIdTable, pCapwapFsWtpNativeVlanIdTable,
            sizeof (tCapwapFsWtpNativeVlanIdTable));

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pCapwapSetFsWtpNativeVlanIdTable->MibObject.
        i4FsWtpNativeVlanIdRowStatus == DESTROY)
    {
        pCapwapFsWtpNativeVlanIdTable->MibObject.i4FsWtpNativeVlanIdRowStatus =
            DESTROY;

        if (CapwapUtilUpdateFsWtpNativeVlanIdTable
            (pCapwapOldFsWtpNativeVlanIdTable, pCapwapFsWtpNativeVlanIdTable,
             pCapwapIsSetFsWtpNativeVlanIdTable) != OSIX_SUCCESS)
        {

            if (CapwapSetAllFsWtpNativeVlanIdTableTrigger
                (pCapwapSetFsWtpNativeVlanIdTable,
                 pCapwapIsSetFsWtpNativeVlanIdTable,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsWtpNativeVlanIdTable: CapwapSetAllFsWtpNativeVlanIdTableTrigger function returns failure.\r\n");
            }
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsWtpNativeVlanIdTable: CapwapUtilUpdateFsWtpNativeVlanIdTable function returns failure.\r\n");
        }
        RBTreeRem (gCapwapGlobals.CapwapGlbMib.FsWtpNativeVlanIdTable,
                   pCapwapFsWtpNativeVlanIdTable);
        if (CapwapSetAllFsWtpNativeVlanIdTableTrigger
            (pCapwapSetFsWtpNativeVlanIdTable,
             pCapwapIsSetFsWtpNativeVlanIdTable, SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsWtpNativeVlanIdTable: CapwapSetAllFsWtpNativeVlanIdTableTrigger function returns failure.\r\n");
            MemReleaseMemBlock (CAPWAP_FSWTPNATIVEVLANIDTABLE_POOLID,
                                (UINT1 *) pCapwapOldFsWtpNativeVlanIdTable);
            MemReleaseMemBlock (CAPWAP_FSWTPNATIVEVLANIDTABLE_POOLID,
                                (UINT1 *) pCapwapTrgFsWtpNativeVlanIdTable);
            MemReleaseMemBlock (CAPWAP_FSWTPNATIVEVLANIDTABLE_ISSET_POOLID,
                                (UINT1 *)
                                pCapwapTrgIsSetFsWtpNativeVlanIdTable);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (CAPWAP_FSWTPNATIVEVLANIDTABLE_POOLID,
                            (UINT1 *) pCapwapFsWtpNativeVlanIdTable);
        MemReleaseMemBlock (CAPWAP_FSWTPNATIVEVLANIDTABLE_POOLID,
                            (UINT1 *) pCapwapOldFsWtpNativeVlanIdTable);
        MemReleaseMemBlock (CAPWAP_FSWTPNATIVEVLANIDTABLE_POOLID,
                            (UINT1 *) pCapwapTrgFsWtpNativeVlanIdTable);
        MemReleaseMemBlock (CAPWAP_FSWTPNATIVEVLANIDTABLE_ISSET_POOLID,
                            (UINT1 *) pCapwapTrgIsSetFsWtpNativeVlanIdTable);
        return OSIX_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (FsWtpNativeVlanIdTableFilterInputs
        (pCapwapFsWtpNativeVlanIdTable, pCapwapSetFsWtpNativeVlanIdTable,
         pCapwapIsSetFsWtpNativeVlanIdTable) != OSIX_TRUE)
    {
        MemReleaseMemBlock (CAPWAP_FSWTPNATIVEVLANIDTABLE_POOLID,
                            (UINT1 *) pCapwapOldFsWtpNativeVlanIdTable);
        MemReleaseMemBlock (CAPWAP_FSWTPNATIVEVLANIDTABLE_POOLID,
                            (UINT1 *) pCapwapTrgFsWtpNativeVlanIdTable);
        MemReleaseMemBlock (CAPWAP_FSWTPNATIVEVLANIDTABLE_ISSET_POOLID,
                            (UINT1 *) pCapwapTrgIsSetFsWtpNativeVlanIdTable);
        return OSIX_SUCCESS;
    }

    /*This condtion is to make the row NOT_IN_SERVICE before modifying the values */
    if ((i4RowStatusLogic == TRUE) &&
        (pCapwapFsWtpNativeVlanIdTable->MibObject.
         i4FsWtpNativeVlanIdRowStatus == ACTIVE)
        && (pCapwapSetFsWtpNativeVlanIdTable->MibObject.
            i4FsWtpNativeVlanIdRowStatus != NOT_IN_SERVICE))
    {
        pCapwapFsWtpNativeVlanIdTable->MibObject.i4FsWtpNativeVlanIdRowStatus =
            NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        pCapwapTrgFsWtpNativeVlanIdTable->MibObject.
            i4FsWtpNativeVlanIdRowStatus = NOT_IN_SERVICE;
        pCapwapTrgIsSetFsWtpNativeVlanIdTable->bFsWtpNativeVlanIdRowStatus =
            OSIX_TRUE;

        if (CapwapUtilUpdateFsWtpNativeVlanIdTable
            (pCapwapOldFsWtpNativeVlanIdTable, pCapwapFsWtpNativeVlanIdTable,
             pCapwapIsSetFsWtpNativeVlanIdTable) != OSIX_SUCCESS)
        {
            /*Restore back with previous values */
            MEMCPY (pCapwapFsWtpNativeVlanIdTable,
                    pCapwapOldFsWtpNativeVlanIdTable,
                    sizeof (tCapwapFsWtpNativeVlanIdTable));
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsWtpNativeVlanIdTable:                 CapwapUtilUpdateFsWtpNativeVlanIdTable Function returns failure.\r\n");

            if (CapwapSetAllFsWtpNativeVlanIdTableTrigger
                (pCapwapSetFsWtpNativeVlanIdTable,
                 pCapwapIsSetFsWtpNativeVlanIdTable,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsWtpNativeVlanIdTable: CapwapSetAllFsWtpNativeVlanIdTableTrigger function returns failure.\r\n");
            }
            MemReleaseMemBlock (CAPWAP_FSWTPNATIVEVLANIDTABLE_POOLID,
                                (UINT1 *) pCapwapOldFsWtpNativeVlanIdTable);
            MemReleaseMemBlock (CAPWAP_FSWTPNATIVEVLANIDTABLE_POOLID,
                                (UINT1 *) pCapwapTrgFsWtpNativeVlanIdTable);
            MemReleaseMemBlock (CAPWAP_FSWTPNATIVEVLANIDTABLE_ISSET_POOLID,
                                (UINT1 *)
                                pCapwapTrgIsSetFsWtpNativeVlanIdTable);
            return OSIX_FAILURE;
        }

        if (CapwapSetAllFsWtpNativeVlanIdTableTrigger
            (pCapwapTrgFsWtpNativeVlanIdTable,
             pCapwapTrgIsSetFsWtpNativeVlanIdTable,
             SNMP_FAILURE) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsWtpNativeVlanIdTable: CapwapSetAllFsWtpNativeVlanIdTableTrigger function returns failure.\r\n");
        }
    }

    if (pCapwapSetFsWtpNativeVlanIdTable->MibObject.
        i4FsWtpNativeVlanIdRowStatus == ACTIVE)
    {
        i4RowMakeActive = TRUE;
    }

    /* Assign values for the existing node */
    if (pCapwapIsSetFsWtpNativeVlanIdTable->bFsWtpNativeVlanId != OSIX_FALSE)
    {
        pCapwapFsWtpNativeVlanIdTable->MibObject.i4FsWtpNativeVlanId =
            pCapwapSetFsWtpNativeVlanIdTable->MibObject.i4FsWtpNativeVlanId;
    }
    if (pCapwapIsSetFsWtpNativeVlanIdTable->bFsWtpNativeVlanIdRowStatus !=
        OSIX_FALSE)
    {
        pCapwapFsWtpNativeVlanIdTable->MibObject.i4FsWtpNativeVlanIdRowStatus =
            pCapwapSetFsWtpNativeVlanIdTable->MibObject.
            i4FsWtpNativeVlanIdRowStatus;
    }

    /*This condtion is to make the row back to ACTIVE after modifying the values */
    if (i4RowMakeActive == TRUE)
    {
        pCapwapFsWtpNativeVlanIdTable->MibObject.i4FsWtpNativeVlanIdRowStatus =
            ACTIVE;
    }

    if (CapwapUtilUpdateFsWtpNativeVlanIdTable
        (pCapwapOldFsWtpNativeVlanIdTable, pCapwapFsWtpNativeVlanIdTable,
         pCapwapIsSetFsWtpNativeVlanIdTable) != OSIX_SUCCESS)
    {

        if (CapwapSetAllFsWtpNativeVlanIdTableTrigger
            (pCapwapSetFsWtpNativeVlanIdTable,
             pCapwapIsSetFsWtpNativeVlanIdTable, SNMP_FAILURE) != OSIX_SUCCESS)

        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsWtpNativeVlanIdTable: CapwapSetAllFsWtpNativeVlanIdTableTrigger function returns failure.\r\n");

        }
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapSetAllFsWtpNativeVlanIdTable: CapwapUtilUpdateFsWtpNativeVlanIdTable function returns failure.\r\n");
        /*Restore back with previous values */
        MEMCPY (pCapwapFsWtpNativeVlanIdTable, pCapwapOldFsWtpNativeVlanIdTable,
                sizeof (tCapwapFsWtpNativeVlanIdTable));
        MemReleaseMemBlock (CAPWAP_FSWTPNATIVEVLANIDTABLE_POOLID,
                            (UINT1 *) pCapwapOldFsWtpNativeVlanIdTable);
        MemReleaseMemBlock (CAPWAP_FSWTPNATIVEVLANIDTABLE_POOLID,
                            (UINT1 *) pCapwapTrgFsWtpNativeVlanIdTable);
        MemReleaseMemBlock (CAPWAP_FSWTPNATIVEVLANIDTABLE_ISSET_POOLID,
                            (UINT1 *) pCapwapTrgIsSetFsWtpNativeVlanIdTable);
        return OSIX_FAILURE;

    }
    if (CapwapSetAllFsWtpNativeVlanIdTableTrigger
        (pCapwapSetFsWtpNativeVlanIdTable, pCapwapIsSetFsWtpNativeVlanIdTable,
         SNMP_SUCCESS) != OSIX_SUCCESS)
    {

        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapSetAllFsWtpNativeVlanIdTable: CapwapSetAllFsWtpNativeVlanIdTableTrigger function returns failure.\r\n");
    }

    MemReleaseMemBlock (CAPWAP_FSWTPNATIVEVLANIDTABLE_POOLID,
                        (UINT1 *) pCapwapOldFsWtpNativeVlanIdTable);
    MemReleaseMemBlock (CAPWAP_FSWTPNATIVEVLANIDTABLE_POOLID,
                        (UINT1 *) pCapwapTrgFsWtpNativeVlanIdTable);
    MemReleaseMemBlock (CAPWAP_FSWTPNATIVEVLANIDTABLE_ISSET_POOLID,
                        (UINT1 *) pCapwapTrgIsSetFsWtpNativeVlanIdTable);
    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  CapwapSetAllFsWtpLocalRoutingTable
 Input       :  pCapwapSetFsWtpLocalRoutingTable
                pCapwapIsSetFsWtpLocalRoutingTable
                i4RowStatusLogic
                i4RowCreateOption
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapSetAllFsWtpLocalRoutingTable (tCapwapFsWtpLocalRoutingTable *
                                    pCapwapSetFsWtpLocalRoutingTable,
                                    tCapwapIsSetFsWtpLocalRoutingTable *
                                    pCapwapIsSetFsWtpLocalRoutingTable,
                                    INT4 i4RowStatusLogic,
                                    INT4 i4RowCreateOption)
{
    tCapwapFsWtpLocalRoutingTable *pCapwapFsWtpLocalRoutingTable = NULL;
    tCapwapFsWtpLocalRoutingTable *pCapwapOldFsWtpLocalRoutingTable = NULL;
    tCapwapFsWtpLocalRoutingTable *pCapwapTrgFsWtpLocalRoutingTable = NULL;
    tCapwapIsSetFsWtpLocalRoutingTable *pCapwapTrgIsSetFsWtpLocalRoutingTable =
        NULL;
    INT4                i4RowMakeActive = FALSE;

    pCapwapOldFsWtpLocalRoutingTable =
        (tCapwapFsWtpLocalRoutingTable *)
        MemAllocMemBlk (CAPWAP_FSWTPLOCALROUTINGTABLE_POOLID);
    if (pCapwapOldFsWtpLocalRoutingTable == NULL)
    {
        return OSIX_FAILURE;
    }
    pCapwapTrgFsWtpLocalRoutingTable =
        (tCapwapFsWtpLocalRoutingTable *)
        MemAllocMemBlk (CAPWAP_FSWTPLOCALROUTINGTABLE_POOLID);
    if (pCapwapTrgFsWtpLocalRoutingTable == NULL)
    {
        MemReleaseMemBlock (CAPWAP_FSWTPLOCALROUTINGTABLE_POOLID,
                            (UINT1 *) pCapwapOldFsWtpLocalRoutingTable);
        return OSIX_FAILURE;
    }
    pCapwapTrgIsSetFsWtpLocalRoutingTable =
        (tCapwapIsSetFsWtpLocalRoutingTable *)
        MemAllocMemBlk (CAPWAP_FSWTPLOCALROUTINGTABLE_ISSET_POOLID);
    if (pCapwapTrgIsSetFsWtpLocalRoutingTable == NULL)
    {
        MemReleaseMemBlock (CAPWAP_FSWTPLOCALROUTINGTABLE_POOLID,
                            (UINT1 *) pCapwapOldFsWtpLocalRoutingTable);
        MemReleaseMemBlock (CAPWAP_FSWTPLOCALROUTINGTABLE_POOLID,
                            (UINT1 *) pCapwapTrgFsWtpLocalRoutingTable);
        return OSIX_FAILURE;
    }
    MEMSET (pCapwapOldFsWtpLocalRoutingTable, 0,
            sizeof (tCapwapFsWtpLocalRoutingTable));
    MEMSET (pCapwapTrgFsWtpLocalRoutingTable, 0,
            sizeof (tCapwapFsWtpLocalRoutingTable));
    MEMSET (pCapwapTrgIsSetFsWtpLocalRoutingTable, 0,
            sizeof (tCapwapIsSetFsWtpLocalRoutingTable));

    /* Check whether the node is already present */
    pCapwapFsWtpLocalRoutingTable =
        RBTreeGet (gCapwapGlobals.CapwapGlbMib.FsWtpLocalRoutingTable,
                   (tRBElem *) pCapwapSetFsWtpLocalRoutingTable);

    if (pCapwapFsWtpLocalRoutingTable == NULL)
    {
        /* Create the node if the RowStatus given is CREATE_AND_WAIT or CREATE_AND_GO */
        if ((pCapwapSetFsWtpLocalRoutingTable->MibObject.
             i4FsWtpLocalRoutingRowStatus == CREATE_AND_WAIT)
            || (pCapwapSetFsWtpLocalRoutingTable->MibObject.
                i4FsWtpLocalRoutingRowStatus == CREATE_AND_GO)
            ||
            ((pCapwapSetFsWtpLocalRoutingTable->MibObject.
              i4FsWtpLocalRoutingRowStatus == ACTIVE)
             && (i4RowCreateOption == 1)))
        {
            /* Allocate memory for the new node */
            pCapwapFsWtpLocalRoutingTable =
                (tCapwapFsWtpLocalRoutingTable *)
                MemAllocMemBlk (CAPWAP_FSWTPLOCALROUTINGTABLE_POOLID);
            if (pCapwapFsWtpLocalRoutingTable == NULL)
            {
                if (CapwapSetAllFsWtpLocalRoutingTableTrigger
                    (pCapwapSetFsWtpLocalRoutingTable,
                     pCapwapIsSetFsWtpLocalRoutingTable,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsWtpLocalRoutingTable:CapwapSetAllFsWtpLocalRoutingTableTrigger function fails\r\n");

                }
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsWtpLocalRoutingTable: Fail to Allocate Memory.\r\n");
                MemReleaseMemBlock (CAPWAP_FSWTPLOCALROUTINGTABLE_POOLID,
                                    (UINT1 *) pCapwapOldFsWtpLocalRoutingTable);
                MemReleaseMemBlock (CAPWAP_FSWTPLOCALROUTINGTABLE_POOLID,
                                    (UINT1 *) pCapwapTrgFsWtpLocalRoutingTable);
                MemReleaseMemBlock (CAPWAP_FSWTPLOCALROUTINGTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgIsSetFsWtpLocalRoutingTable);
                return OSIX_FAILURE;
            }

            MEMSET (pCapwapFsWtpLocalRoutingTable, 0,
                    sizeof (tCapwapFsWtpLocalRoutingTable));
            if ((CapwapInitializeFsWtpLocalRoutingTable
                 (pCapwapFsWtpLocalRoutingTable)) == OSIX_FAILURE)
            {
                if (CapwapSetAllFsWtpLocalRoutingTableTrigger
                    (pCapwapSetFsWtpLocalRoutingTable,
                     pCapwapIsSetFsWtpLocalRoutingTable,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsWtpLocalRoutingTable:CapwapSetAllFsWtpLocalRoutingTableTrigger function fails\r\n");

                }
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsWtpLocalRoutingTable: Fail to Initialize the Objects.\r\n");

                MemReleaseMemBlock (CAPWAP_FSWTPLOCALROUTINGTABLE_POOLID,
                                    (UINT1 *) pCapwapFsWtpLocalRoutingTable);
                MemReleaseMemBlock (CAPWAP_FSWTPLOCALROUTINGTABLE_POOLID,
                                    (UINT1 *) pCapwapOldFsWtpLocalRoutingTable);
                MemReleaseMemBlock (CAPWAP_FSWTPLOCALROUTINGTABLE_POOLID,
                                    (UINT1 *) pCapwapTrgFsWtpLocalRoutingTable);
                MemReleaseMemBlock (CAPWAP_FSWTPLOCALROUTINGTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgIsSetFsWtpLocalRoutingTable);
                return OSIX_FAILURE;
            }
            /* Assign values for the new node */
            if (pCapwapIsSetFsWtpLocalRoutingTable->bFsWtpLocalRouting !=
                OSIX_FALSE)
            {
                pCapwapFsWtpLocalRoutingTable->MibObject.i4FsWtpLocalRouting =
                    pCapwapSetFsWtpLocalRoutingTable->MibObject.
                    i4FsWtpLocalRouting;
            }

            if (pCapwapIsSetFsWtpLocalRoutingTable->
                bFsWtpLocalRoutingRowStatus != OSIX_FALSE)
            {
                pCapwapFsWtpLocalRoutingTable->MibObject.
                    i4FsWtpLocalRoutingRowStatus =
                    pCapwapSetFsWtpLocalRoutingTable->MibObject.
                    i4FsWtpLocalRoutingRowStatus;
            }

            if (pCapwapIsSetFsWtpLocalRoutingTable->bCapwapBaseWtpProfileId !=
                OSIX_FALSE)
            {
                pCapwapFsWtpLocalRoutingTable->MibObject.
                    u4CapwapBaseWtpProfileId =
                    pCapwapSetFsWtpLocalRoutingTable->MibObject.
                    u4CapwapBaseWtpProfileId;
            }

            if ((pCapwapSetFsWtpLocalRoutingTable->MibObject.
                 i4FsWtpLocalRoutingRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pCapwapSetFsWtpLocalRoutingTable->MibObject.
                        i4FsWtpLocalRoutingRowStatus == ACTIVE)))
            {
                pCapwapFsWtpLocalRoutingTable->MibObject.
                    i4FsWtpLocalRoutingRowStatus = ACTIVE;
            }
            else if (pCapwapSetFsWtpLocalRoutingTable->MibObject.
                     i4FsWtpLocalRoutingRowStatus == CREATE_AND_WAIT)
            {
                pCapwapFsWtpLocalRoutingTable->MibObject.
                    i4FsWtpLocalRoutingRowStatus = NOT_READY;
            }

            /* Add the new node to the database */
            if (RBTreeAdd
                (gCapwapGlobals.CapwapGlbMib.FsWtpLocalRoutingTable,
                 (tRBElem *) pCapwapFsWtpLocalRoutingTable) != RB_SUCCESS)
            {
                if (CapwapSetAllFsWtpLocalRoutingTableTrigger
                    (pCapwapSetFsWtpLocalRoutingTable,
                     pCapwapIsSetFsWtpLocalRoutingTable,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsWtpLocalRoutingTable: CapwapSetAllFsWtpLocalRoutingTableTrigger function returns failure.\r\n");
                }
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsWtpLocalRoutingTable: RBTreeAdd is failed.\r\n");

                MemReleaseMemBlock (CAPWAP_FSWTPLOCALROUTINGTABLE_POOLID,
                                    (UINT1 *) pCapwapFsWtpLocalRoutingTable);
                MemReleaseMemBlock (CAPWAP_FSWTPLOCALROUTINGTABLE_POOLID,
                                    (UINT1 *) pCapwapOldFsWtpLocalRoutingTable);
                MemReleaseMemBlock (CAPWAP_FSWTPLOCALROUTINGTABLE_POOLID,
                                    (UINT1 *) pCapwapTrgFsWtpLocalRoutingTable);
                MemReleaseMemBlock (CAPWAP_FSWTPLOCALROUTINGTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgIsSetFsWtpLocalRoutingTable);
                return OSIX_FAILURE;
            }
            if (CapwapUtilUpdateFsWtpLocalRoutingTable
                (NULL, pCapwapFsWtpLocalRoutingTable,
                 pCapwapIsSetFsWtpLocalRoutingTable) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsWtpLocalRoutingTable: CapwapUtilUpdateFsWtpLocalRoutingTable function returns failure.\r\n");

                if (CapwapSetAllFsWtpLocalRoutingTableTrigger
                    (pCapwapSetFsWtpLocalRoutingTable,
                     pCapwapIsSetFsWtpLocalRoutingTable,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsWtpLocalRoutingTable: CapwapSetAllFsWtpLocalRoutingTableTrigger function returns failure.\r\n");

                }
                RBTreeRem (gCapwapGlobals.CapwapGlbMib.FsWtpLocalRoutingTable,
                           pCapwapFsWtpLocalRoutingTable);
                MemReleaseMemBlock (CAPWAP_FSWTPLOCALROUTINGTABLE_POOLID,
                                    (UINT1 *) pCapwapFsWtpLocalRoutingTable);
                MemReleaseMemBlock (CAPWAP_FSWTPLOCALROUTINGTABLE_POOLID,
                                    (UINT1 *) pCapwapOldFsWtpLocalRoutingTable);
                MemReleaseMemBlock (CAPWAP_FSWTPLOCALROUTINGTABLE_POOLID,
                                    (UINT1 *) pCapwapTrgFsWtpLocalRoutingTable);
                MemReleaseMemBlock (CAPWAP_FSWTPLOCALROUTINGTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgIsSetFsWtpLocalRoutingTable);
                return OSIX_FAILURE;
            }

            if ((pCapwapSetFsWtpLocalRoutingTable->MibObject.
                 i4FsWtpLocalRoutingRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pCapwapSetFsWtpLocalRoutingTable->MibObject.
                        i4FsWtpLocalRoutingRowStatus == ACTIVE)))
            {

                /* For MSR and RM Trigger */
                pCapwapTrgFsWtpLocalRoutingTable->MibObject.
                    u4CapwapBaseWtpProfileId =
                    pCapwapSetFsWtpLocalRoutingTable->MibObject.
                    u4CapwapBaseWtpProfileId;
                pCapwapTrgFsWtpLocalRoutingTable->MibObject.
                    i4FsWtpLocalRoutingRowStatus = CREATE_AND_WAIT;
                pCapwapTrgIsSetFsWtpLocalRoutingTable->
                    bFsWtpLocalRoutingRowStatus = OSIX_TRUE;

                if (CapwapSetAllFsWtpLocalRoutingTableTrigger
                    (pCapwapTrgFsWtpLocalRoutingTable,
                     pCapwapTrgIsSetFsWtpLocalRoutingTable,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsWtpLocalRoutingTable: CapwapSetAllFsWtpLocalRoutingTableTrigger function returns failure.\r\n");

                    MemReleaseMemBlock (CAPWAP_FSWTPLOCALROUTINGTABLE_POOLID,
                                        (UINT1 *)
                                        pCapwapFsWtpLocalRoutingTable);
                    MemReleaseMemBlock (CAPWAP_FSWTPLOCALROUTINGTABLE_POOLID,
                                        (UINT1 *)
                                        pCapwapOldFsWtpLocalRoutingTable);
                    MemReleaseMemBlock (CAPWAP_FSWTPLOCALROUTINGTABLE_POOLID,
                                        (UINT1 *)
                                        pCapwapTrgFsWtpLocalRoutingTable);
                    MemReleaseMemBlock
                        (CAPWAP_FSWTPLOCALROUTINGTABLE_ISSET_POOLID,
                         (UINT1 *) pCapwapTrgIsSetFsWtpLocalRoutingTable);
                    return OSIX_FAILURE;
                }
            }
            else if (pCapwapSetFsWtpLocalRoutingTable->MibObject.
                     i4FsWtpLocalRoutingRowStatus == CREATE_AND_WAIT)
            {

                /* For MSR and RM Trigger */
                pCapwapTrgFsWtpLocalRoutingTable->MibObject.
                    i4FsWtpLocalRoutingRowStatus = CREATE_AND_WAIT;
                pCapwapTrgIsSetFsWtpLocalRoutingTable->
                    bFsWtpLocalRoutingRowStatus = OSIX_TRUE;

                if (CapwapSetAllFsWtpLocalRoutingTableTrigger
                    (pCapwapTrgFsWtpLocalRoutingTable,
                     pCapwapTrgIsSetFsWtpLocalRoutingTable,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsWtpLocalRoutingTable: CapwapSetAllFsWtpLocalRoutingTableTrigger function returns failure.\r\n");

                    MemReleaseMemBlock (CAPWAP_FSWTPLOCALROUTINGTABLE_POOLID,
                                        (UINT1 *)
                                        pCapwapFsWtpLocalRoutingTable);
                    MemReleaseMemBlock (CAPWAP_FSWTPLOCALROUTINGTABLE_POOLID,
                                        (UINT1 *)
                                        pCapwapOldFsWtpLocalRoutingTable);
                    MemReleaseMemBlock (CAPWAP_FSWTPLOCALROUTINGTABLE_POOLID,
                                        (UINT1 *)
                                        pCapwapTrgFsWtpLocalRoutingTable);
                    MemReleaseMemBlock
                        (CAPWAP_FSWTPLOCALROUTINGTABLE_ISSET_POOLID,
                         (UINT1 *) pCapwapTrgIsSetFsWtpLocalRoutingTable);
                    return OSIX_FAILURE;
                }
            }

            if (pCapwapSetFsWtpLocalRoutingTable->MibObject.
                i4FsWtpLocalRoutingRowStatus == CREATE_AND_GO)
            {
                pCapwapSetFsWtpLocalRoutingTable->MibObject.
                    i4FsWtpLocalRoutingRowStatus = ACTIVE;
            }

            if (CapwapSetAllFsWtpLocalRoutingTableTrigger
                (pCapwapSetFsWtpLocalRoutingTable,
                 pCapwapIsSetFsWtpLocalRoutingTable,
                 SNMP_SUCCESS) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsWtpLocalRoutingTable:  CapwapSetAllFsWtpLocalRoutingTableTrigger function returns failure.\r\n");
            }
            MemReleaseMemBlock (CAPWAP_FSWTPLOCALROUTINGTABLE_POOLID,
                                (UINT1 *) pCapwapOldFsWtpLocalRoutingTable);
            MemReleaseMemBlock (CAPWAP_FSWTPLOCALROUTINGTABLE_POOLID,
                                (UINT1 *) pCapwapTrgFsWtpLocalRoutingTable);
            MemReleaseMemBlock (CAPWAP_FSWTPLOCALROUTINGTABLE_ISSET_POOLID,
                                (UINT1 *)
                                pCapwapTrgIsSetFsWtpLocalRoutingTable);
            return OSIX_SUCCESS;

        }
        else
        {
            if (CapwapSetAllFsWtpLocalRoutingTableTrigger
                (pCapwapSetFsWtpLocalRoutingTable,
                 pCapwapIsSetFsWtpLocalRoutingTable,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsWtpLocalRoutingTable: CapwapSetAllFsWtpLocalRoutingTableTrigger function returns failure.\r\n");
            }
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsWtpLocalRoutingTable: Failure.\r\n");
            MemReleaseMemBlock (CAPWAP_FSWTPLOCALROUTINGTABLE_POOLID,
                                (UINT1 *) pCapwapOldFsWtpLocalRoutingTable);
            MemReleaseMemBlock (CAPWAP_FSWTPLOCALROUTINGTABLE_POOLID,
                                (UINT1 *) pCapwapTrgFsWtpLocalRoutingTable);
            MemReleaseMemBlock (CAPWAP_FSWTPLOCALROUTINGTABLE_ISSET_POOLID,
                                (UINT1 *)
                                pCapwapTrgIsSetFsWtpLocalRoutingTable);
            return OSIX_FAILURE;
        }
    }
    else if ((pCapwapSetFsWtpLocalRoutingTable->MibObject.
              i4FsWtpLocalRoutingRowStatus == CREATE_AND_WAIT)
             || (pCapwapSetFsWtpLocalRoutingTable->MibObject.
                 i4FsWtpLocalRoutingRowStatus == CREATE_AND_GO))
    {
        if (CapwapSetAllFsWtpLocalRoutingTableTrigger
            (pCapwapSetFsWtpLocalRoutingTable,
             pCapwapIsSetFsWtpLocalRoutingTable, SNMP_FAILURE) != OSIX_SUCCESS)

        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsWtpLocalRoutingTable: CapwapSetAllFsWtpLocalRoutingTableTrigger function returns failure.\r\n");
        }
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapSetAllFsWtpLocalRoutingTable: The row is already present.\r\n");
        MemReleaseMemBlock (CAPWAP_FSWTPLOCALROUTINGTABLE_POOLID,
                            (UINT1 *) pCapwapOldFsWtpLocalRoutingTable);
        MemReleaseMemBlock (CAPWAP_FSWTPLOCALROUTINGTABLE_POOLID,
                            (UINT1 *) pCapwapTrgFsWtpLocalRoutingTable);
        MemReleaseMemBlock (CAPWAP_FSWTPLOCALROUTINGTABLE_ISSET_POOLID,
                            (UINT1 *) pCapwapTrgIsSetFsWtpLocalRoutingTable);
        return OSIX_FAILURE;
    }
    /* Copy the previous values before setting the new values */
    MEMCPY (pCapwapOldFsWtpLocalRoutingTable, pCapwapFsWtpLocalRoutingTable,
            sizeof (tCapwapFsWtpLocalRoutingTable));

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pCapwapSetFsWtpLocalRoutingTable->MibObject.
        i4FsWtpLocalRoutingRowStatus == DESTROY)
    {
        pCapwapFsWtpLocalRoutingTable->MibObject.i4FsWtpLocalRoutingRowStatus =
            DESTROY;

        if (CapwapUtilUpdateFsWtpLocalRoutingTable
            (pCapwapOldFsWtpLocalRoutingTable, pCapwapFsWtpLocalRoutingTable,
             pCapwapIsSetFsWtpLocalRoutingTable) != OSIX_SUCCESS)
        {

            if (CapwapSetAllFsWtpLocalRoutingTableTrigger
                (pCapwapSetFsWtpLocalRoutingTable,
                 pCapwapIsSetFsWtpLocalRoutingTable,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsWtpLocalRoutingTable: CapwapSetAllFsWtpLocalRoutingTableTrigger function returns failure.\r\n");
            }
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsWtpLocalRoutingTable: CapwapUtilUpdateFsWtpLocalRoutingTable function returns failure.\r\n");
        }
        RBTreeRem (gCapwapGlobals.CapwapGlbMib.FsWtpLocalRoutingTable,
                   pCapwapFsWtpLocalRoutingTable);
        if (CapwapSetAllFsWtpLocalRoutingTableTrigger
            (pCapwapSetFsWtpLocalRoutingTable,
             pCapwapIsSetFsWtpLocalRoutingTable, SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsWtpLocalRoutingTable: CapwapSetAllFsWtpLocalRoutingTableTrigger function returns failure.\r\n");
            MemReleaseMemBlock (CAPWAP_FSWTPLOCALROUTINGTABLE_POOLID,
                                (UINT1 *) pCapwapOldFsWtpLocalRoutingTable);
            MemReleaseMemBlock (CAPWAP_FSWTPLOCALROUTINGTABLE_POOLID,
                                (UINT1 *) pCapwapTrgFsWtpLocalRoutingTable);
            MemReleaseMemBlock (CAPWAP_FSWTPLOCALROUTINGTABLE_ISSET_POOLID,
                                (UINT1 *)
                                pCapwapTrgIsSetFsWtpLocalRoutingTable);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (CAPWAP_FSWTPLOCALROUTINGTABLE_POOLID,
                            (UINT1 *) pCapwapFsWtpLocalRoutingTable);
        MemReleaseMemBlock (CAPWAP_FSWTPLOCALROUTINGTABLE_POOLID,
                            (UINT1 *) pCapwapOldFsWtpLocalRoutingTable);
        MemReleaseMemBlock (CAPWAP_FSWTPLOCALROUTINGTABLE_POOLID,
                            (UINT1 *) pCapwapTrgFsWtpLocalRoutingTable);
        MemReleaseMemBlock (CAPWAP_FSWTPLOCALROUTINGTABLE_ISSET_POOLID,
                            (UINT1 *) pCapwapTrgIsSetFsWtpLocalRoutingTable);
        return OSIX_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (FsWtpLocalRoutingTableFilterInputs
        (pCapwapFsWtpLocalRoutingTable, pCapwapSetFsWtpLocalRoutingTable,
         pCapwapIsSetFsWtpLocalRoutingTable) != OSIX_TRUE)
    {
        MemReleaseMemBlock (CAPWAP_FSWTPLOCALROUTINGTABLE_POOLID,
                            (UINT1 *) pCapwapOldFsWtpLocalRoutingTable);
        MemReleaseMemBlock (CAPWAP_FSWTPLOCALROUTINGTABLE_POOLID,
                            (UINT1 *) pCapwapTrgFsWtpLocalRoutingTable);
        MemReleaseMemBlock (CAPWAP_FSWTPLOCALROUTINGTABLE_ISSET_POOLID,
                            (UINT1 *) pCapwapTrgIsSetFsWtpLocalRoutingTable);
        return OSIX_SUCCESS;
    }

    /*This condtion is to make the row NOT_IN_SERVICE before modifying the values */
    if ((i4RowStatusLogic == TRUE) &&
        (pCapwapFsWtpLocalRoutingTable->MibObject.
         i4FsWtpLocalRoutingRowStatus == ACTIVE)
        && (pCapwapSetFsWtpLocalRoutingTable->MibObject.
            i4FsWtpLocalRoutingRowStatus != NOT_IN_SERVICE))
    {
        pCapwapFsWtpLocalRoutingTable->MibObject.i4FsWtpLocalRoutingRowStatus =
            NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        pCapwapTrgFsWtpLocalRoutingTable->MibObject.
            i4FsWtpLocalRoutingRowStatus = NOT_IN_SERVICE;
        pCapwapTrgIsSetFsWtpLocalRoutingTable->bFsWtpLocalRoutingRowStatus =
            OSIX_TRUE;

        if (CapwapUtilUpdateFsWtpLocalRoutingTable
            (pCapwapOldFsWtpLocalRoutingTable, pCapwapFsWtpLocalRoutingTable,
             pCapwapIsSetFsWtpLocalRoutingTable) != OSIX_SUCCESS)
        {
            /*Restore back with previous values */
            MEMCPY (pCapwapFsWtpLocalRoutingTable,
                    pCapwapOldFsWtpLocalRoutingTable,
                    sizeof (tCapwapFsWtpLocalRoutingTable));
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsWtpLocalRoutingTable:                 CapwapUtilUpdateFsWtpLocalRoutingTable Function returns failure.\r\n");
            if (CapwapSetAllFsWtpLocalRoutingTableTrigger
                (pCapwapSetFsWtpLocalRoutingTable,
                 pCapwapIsSetFsWtpLocalRoutingTable,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsWtpLocalRoutingTable: CapwapSetAllFsWtpLocalRoutingTableTrigger function returns failure.\r\n");
            }
            MemReleaseMemBlock (CAPWAP_FSWTPLOCALROUTINGTABLE_POOLID,
                                (UINT1 *) pCapwapOldFsWtpLocalRoutingTable);
            MemReleaseMemBlock (CAPWAP_FSWTPLOCALROUTINGTABLE_POOLID,
                                (UINT1 *) pCapwapTrgFsWtpLocalRoutingTable);
            MemReleaseMemBlock (CAPWAP_FSWTPLOCALROUTINGTABLE_ISSET_POOLID,
                                (UINT1 *)
                                pCapwapTrgIsSetFsWtpLocalRoutingTable);
            return OSIX_FAILURE;
        }

        if (CapwapSetAllFsWtpLocalRoutingTableTrigger
            (pCapwapTrgFsWtpLocalRoutingTable,
             pCapwapTrgIsSetFsWtpLocalRoutingTable,
             SNMP_FAILURE) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsWtpLocalRoutingTable: CapwapSetAllFsWtpLocalRoutingTableTrigger function returns failure.\r\n");
        }
    }

    if (pCapwapSetFsWtpLocalRoutingTable->MibObject.
        i4FsWtpLocalRoutingRowStatus == ACTIVE)
    {
        i4RowMakeActive = TRUE;
    }

    /* Assign values for the existing node */
    if (pCapwapIsSetFsWtpLocalRoutingTable->bFsWtpLocalRouting != OSIX_FALSE)
    {
        pCapwapFsWtpLocalRoutingTable->MibObject.i4FsWtpLocalRouting =
            pCapwapSetFsWtpLocalRoutingTable->MibObject.i4FsWtpLocalRouting;
    }
    if (pCapwapIsSetFsWtpLocalRoutingTable->bFsWtpLocalRoutingRowStatus !=
        OSIX_FALSE)
    {
        pCapwapFsWtpLocalRoutingTable->MibObject.i4FsWtpLocalRoutingRowStatus =
            pCapwapSetFsWtpLocalRoutingTable->MibObject.
            i4FsWtpLocalRoutingRowStatus;
    }

    /*This condtion is to make the row back to ACTIVE after modifying the values */
    if (i4RowMakeActive == TRUE)
    {
        pCapwapFsWtpLocalRoutingTable->MibObject.i4FsWtpLocalRoutingRowStatus =
            ACTIVE;
    }

    if (CapwapUtilUpdateFsWtpLocalRoutingTable
        (pCapwapOldFsWtpLocalRoutingTable, pCapwapFsWtpLocalRoutingTable,
         pCapwapIsSetFsWtpLocalRoutingTable) != OSIX_SUCCESS)
    {

        if (CapwapSetAllFsWtpLocalRoutingTableTrigger
            (pCapwapSetFsWtpLocalRoutingTable,
             pCapwapIsSetFsWtpLocalRoutingTable, SNMP_FAILURE) != OSIX_SUCCESS)

        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsWtpLocalRoutingTable: CapwapSetAllFsWtpLocalRoutingTableTrigger function returns failure.\r\n");

        }
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapSetAllFsWtpLocalRoutingTable: CapwapUtilUpdateFsWtpLocalRoutingTable function returns failure.\r\n");
        /*Restore back with previous values */
        MEMCPY (pCapwapFsWtpLocalRoutingTable, pCapwapOldFsWtpLocalRoutingTable,
                sizeof (tCapwapFsWtpLocalRoutingTable));
        MemReleaseMemBlock (CAPWAP_FSWTPLOCALROUTINGTABLE_POOLID,
                            (UINT1 *) pCapwapOldFsWtpLocalRoutingTable);
        MemReleaseMemBlock (CAPWAP_FSWTPLOCALROUTINGTABLE_POOLID,
                            (UINT1 *) pCapwapTrgFsWtpLocalRoutingTable);

    }
    if (CapwapSetAllFsWtpLocalRoutingTableTrigger
        (pCapwapSetFsWtpLocalRoutingTable, pCapwapIsSetFsWtpLocalRoutingTable,
         SNMP_SUCCESS) != OSIX_SUCCESS)
    {

        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapSetAllFsWtpLocalRoutingTable: CapwapSetAllFsWtpLocalRoutingTableTrigger function returns failure.\r\n");
    }

    MemReleaseMemBlock (CAPWAP_FSWTPLOCALROUTINGTABLE_POOLID,
                        (UINT1 *) pCapwapOldFsWtpLocalRoutingTable);
    MemReleaseMemBlock (CAPWAP_FSWTPLOCALROUTINGTABLE_POOLID,
                        (UINT1 *) pCapwapTrgFsWtpLocalRoutingTable);
    MemReleaseMemBlock (CAPWAP_FSWTPLOCALROUTINGTABLE_ISSET_POOLID,
                        (UINT1 *) pCapwapTrgIsSetFsWtpLocalRoutingTable);
    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  CapwapSetAllFsCawapDiscStatsTable
 Input       :  pCapwapSetFsCawapDiscStatsEntry
                pCapwapIsSetFsCawapDiscStatsEntry
                i4RowStatusLogic
                i4RowCreateOption
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapSetAllFsCawapDiscStatsTable (tCapwapFsCawapDiscStatsEntry *
                                   pCapwapSetFsCawapDiscStatsEntry,
                                   tCapwapIsSetFsCawapDiscStatsEntry *
                                   pCapwapIsSetFsCawapDiscStatsEntry,
                                   INT4 i4RowStatusLogic,
                                   INT4 i4RowCreateOption)
{
    tCapwapFsCawapDiscStatsEntry *pCapwapFsCawapDiscStatsEntry = NULL;
    tCapwapFsCawapDiscStatsEntry *pCapwapOldFsCawapDiscStatsEntry = NULL;
    tCapwapFsCawapDiscStatsEntry *pCapwapTrgFsCawapDiscStatsEntry = NULL;
    tCapwapIsSetFsCawapDiscStatsEntry *pCapwapTrgIsSetFsCawapDiscStatsEntry =
        NULL;
    INT4                i4RowMakeActive = FALSE;

    pCapwapOldFsCawapDiscStatsEntry =
        (tCapwapFsCawapDiscStatsEntry *)
        MemAllocMemBlk (CAPWAP_FSCAWAPDISCSTATSTABLE_POOLID);
    if (pCapwapOldFsCawapDiscStatsEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    pCapwapTrgFsCawapDiscStatsEntry =
        (tCapwapFsCawapDiscStatsEntry *)
        MemAllocMemBlk (CAPWAP_FSCAWAPDISCSTATSTABLE_POOLID);
    if (pCapwapTrgFsCawapDiscStatsEntry == NULL)
    {
        MemReleaseMemBlock (CAPWAP_FSCAWAPDISCSTATSTABLE_POOLID,
                            (UINT1 *) pCapwapOldFsCawapDiscStatsEntry);
        return OSIX_FAILURE;
    }
    pCapwapTrgIsSetFsCawapDiscStatsEntry =
        (tCapwapIsSetFsCawapDiscStatsEntry *)
        MemAllocMemBlk (CAPWAP_FSCAWAPDISCSTATSTABLE_ISSET_POOLID);
    if (pCapwapTrgIsSetFsCawapDiscStatsEntry == NULL)
    {
        MemReleaseMemBlock (CAPWAP_FSCAWAPDISCSTATSTABLE_POOLID,
                            (UINT1 *) pCapwapOldFsCawapDiscStatsEntry);
        MemReleaseMemBlock (CAPWAP_FSCAWAPDISCSTATSTABLE_POOLID,
                            (UINT1 *) pCapwapTrgFsCawapDiscStatsEntry);
        return OSIX_FAILURE;
    }
    MEMSET (pCapwapOldFsCawapDiscStatsEntry, 0,
            sizeof (tCapwapFsCawapDiscStatsEntry));
    MEMSET (pCapwapTrgFsCawapDiscStatsEntry, 0,
            sizeof (tCapwapFsCawapDiscStatsEntry));
    MEMSET (pCapwapTrgIsSetFsCawapDiscStatsEntry, 0,
            sizeof (tCapwapIsSetFsCawapDiscStatsEntry));

    /* Check whether the node is already present */
    pCapwapFsCawapDiscStatsEntry =
        RBTreeGet (gCapwapGlobals.CapwapGlbMib.FsCawapDiscStatsTable,
                   (tRBElem *) pCapwapSetFsCawapDiscStatsEntry);

    if (pCapwapFsCawapDiscStatsEntry == NULL)
    {
        /* Create the node if the RowStatus given is CREATE_AND_WAIT or CREATE_AND_GO */
        if ((pCapwapSetFsCawapDiscStatsEntry->MibObject.
             i4FsCapwapDiscStatsRowStatus == CREATE_AND_WAIT)
            || (pCapwapSetFsCawapDiscStatsEntry->MibObject.
                i4FsCapwapDiscStatsRowStatus == CREATE_AND_GO)
            ||
            ((pCapwapSetFsCawapDiscStatsEntry->MibObject.
              i4FsCapwapDiscStatsRowStatus == ACTIVE)
             && (i4RowCreateOption == 1)))
        {
            /* Allocate memory for the new node */
            pCapwapFsCawapDiscStatsEntry =
                (tCapwapFsCawapDiscStatsEntry *)
                MemAllocMemBlk (CAPWAP_FSCAWAPDISCSTATSTABLE_POOLID);
            if (pCapwapFsCawapDiscStatsEntry == NULL)
            {
                if (CapwapSetAllFsCawapDiscStatsTableTrigger
                    (pCapwapSetFsCawapDiscStatsEntry,
                     pCapwapIsSetFsCawapDiscStatsEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCawapDiscStatsTable:CapwapSetAllFsCawapDiscStatsTableTrigger function fails\r\n");

                }
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCawapDiscStatsTable: Fail to Allocate Memory.\r\n");
                MemReleaseMemBlock (CAPWAP_FSCAWAPDISCSTATSTABLE_POOLID,
                                    (UINT1 *) pCapwapOldFsCawapDiscStatsEntry);
                MemReleaseMemBlock (CAPWAP_FSCAWAPDISCSTATSTABLE_POOLID,
                                    (UINT1 *) pCapwapTrgFsCawapDiscStatsEntry);
                MemReleaseMemBlock (CAPWAP_FSCAWAPDISCSTATSTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgIsSetFsCawapDiscStatsEntry);
                return OSIX_FAILURE;
            }

            MEMSET (pCapwapFsCawapDiscStatsEntry, 0,
                    sizeof (tCapwapFsCawapDiscStatsEntry));
            if ((CapwapInitializeFsCawapDiscStatsTable
                 (pCapwapFsCawapDiscStatsEntry)) == OSIX_FAILURE)
            {
                if (CapwapSetAllFsCawapDiscStatsTableTrigger
                    (pCapwapSetFsCawapDiscStatsEntry,
                     pCapwapIsSetFsCawapDiscStatsEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCawapDiscStatsTable:CapwapSetAllFsCawapDiscStatsTableTrigger function fails\r\n");

                }
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCawapDiscStatsTable: Fail to Initialize the Objects.\r\n");

                MemReleaseMemBlock (CAPWAP_FSCAWAPDISCSTATSTABLE_POOLID,
                                    (UINT1 *) pCapwapFsCawapDiscStatsEntry);
                MemReleaseMemBlock (CAPWAP_FSCAWAPDISCSTATSTABLE_POOLID,
                                    (UINT1 *) pCapwapOldFsCawapDiscStatsEntry);
                MemReleaseMemBlock (CAPWAP_FSCAWAPDISCSTATSTABLE_POOLID,
                                    (UINT1 *) pCapwapTrgFsCawapDiscStatsEntry);
                MemReleaseMemBlock (CAPWAP_FSCAWAPDISCSTATSTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgIsSetFsCawapDiscStatsEntry);
                return OSIX_FAILURE;
            }
            /* Assign values for the new node */
            if (pCapwapIsSetFsCawapDiscStatsEntry->
                bFsCapwapDiscStatsRowStatus != OSIX_FALSE)
            {
                pCapwapFsCawapDiscStatsEntry->MibObject.
                    i4FsCapwapDiscStatsRowStatus =
                    pCapwapSetFsCawapDiscStatsEntry->MibObject.
                    i4FsCapwapDiscStatsRowStatus;
            }

            if (pCapwapIsSetFsCawapDiscStatsEntry->bCapwapBaseWtpProfileId !=
                OSIX_FALSE)
            {
                pCapwapFsCawapDiscStatsEntry->MibObject.
                    u4CapwapBaseWtpProfileId =
                    pCapwapSetFsCawapDiscStatsEntry->MibObject.
                    u4CapwapBaseWtpProfileId;
            }

            if ((pCapwapSetFsCawapDiscStatsEntry->MibObject.
                 i4FsCapwapDiscStatsRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pCapwapSetFsCawapDiscStatsEntry->MibObject.
                        i4FsCapwapDiscStatsRowStatus == ACTIVE)))
            {
                pCapwapFsCawapDiscStatsEntry->MibObject.
                    i4FsCapwapDiscStatsRowStatus = ACTIVE;
            }
            else if (pCapwapSetFsCawapDiscStatsEntry->MibObject.
                     i4FsCapwapDiscStatsRowStatus == CREATE_AND_WAIT)
            {
                pCapwapFsCawapDiscStatsEntry->MibObject.
                    i4FsCapwapDiscStatsRowStatus = NOT_READY;
            }

            /* Add the new node to the database */
            if (RBTreeAdd
                (gCapwapGlobals.CapwapGlbMib.FsCawapDiscStatsTable,
                 (tRBElem *) pCapwapFsCawapDiscStatsEntry) != RB_SUCCESS)
            {
                if (CapwapSetAllFsCawapDiscStatsTableTrigger
                    (pCapwapSetFsCawapDiscStatsEntry,
                     pCapwapIsSetFsCawapDiscStatsEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCawapDiscStatsTable: CapwapSetAllFsCawapDiscStatsTableTrigger function returns failure.\r\n");
                }
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCawapDiscStatsTable: RBTreeAdd is failed.\r\n");

                MemReleaseMemBlock (CAPWAP_FSCAWAPDISCSTATSTABLE_POOLID,
                                    (UINT1 *) pCapwapFsCawapDiscStatsEntry);
                MemReleaseMemBlock (CAPWAP_FSCAWAPDISCSTATSTABLE_POOLID,
                                    (UINT1 *) pCapwapOldFsCawapDiscStatsEntry);
                MemReleaseMemBlock (CAPWAP_FSCAWAPDISCSTATSTABLE_POOLID,
                                    (UINT1 *) pCapwapTrgFsCawapDiscStatsEntry);
                MemReleaseMemBlock (CAPWAP_FSCAWAPDISCSTATSTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgIsSetFsCawapDiscStatsEntry);
                return OSIX_FAILURE;
            }
            if (CapwapUtilUpdateFsCawapDiscStatsTable
                (NULL, pCapwapFsCawapDiscStatsEntry,
                 pCapwapIsSetFsCawapDiscStatsEntry) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCawapDiscStatsTable: CapwapUtilUpdateFsCawapDiscStatsTable function returns failure.\r\n");

                if (CapwapSetAllFsCawapDiscStatsTableTrigger
                    (pCapwapSetFsCawapDiscStatsEntry,
                     pCapwapIsSetFsCawapDiscStatsEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCawapDiscStatsTable: CapwapSetAllFsCawapDiscStatsTableTrigger function returns failure.\r\n");

                }
                RBTreeRem (gCapwapGlobals.CapwapGlbMib.FsCawapDiscStatsTable,
                           pCapwapFsCawapDiscStatsEntry);
                MemReleaseMemBlock (CAPWAP_FSCAWAPDISCSTATSTABLE_POOLID,
                                    (UINT1 *) pCapwapFsCawapDiscStatsEntry);
                MemReleaseMemBlock (CAPWAP_FSCAWAPDISCSTATSTABLE_POOLID,
                                    (UINT1 *) pCapwapOldFsCawapDiscStatsEntry);
                MemReleaseMemBlock (CAPWAP_FSCAWAPDISCSTATSTABLE_POOLID,
                                    (UINT1 *) pCapwapTrgFsCawapDiscStatsEntry);
                MemReleaseMemBlock (CAPWAP_FSCAWAPDISCSTATSTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgIsSetFsCawapDiscStatsEntry);
                return OSIX_FAILURE;
            }

            if ((pCapwapSetFsCawapDiscStatsEntry->MibObject.
                 i4FsCapwapDiscStatsRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pCapwapSetFsCawapDiscStatsEntry->MibObject.
                        i4FsCapwapDiscStatsRowStatus == ACTIVE)))
            {

                /* For MSR and RM Trigger */
                pCapwapTrgFsCawapDiscStatsEntry->MibObject.
                    u4CapwapBaseWtpProfileId =
                    pCapwapSetFsCawapDiscStatsEntry->MibObject.
                    u4CapwapBaseWtpProfileId;
                pCapwapTrgFsCawapDiscStatsEntry->MibObject.
                    i4FsCapwapDiscStatsRowStatus = CREATE_AND_WAIT;
                pCapwapTrgIsSetFsCawapDiscStatsEntry->
                    bFsCapwapDiscStatsRowStatus = OSIX_TRUE;

                if (CapwapSetAllFsCawapDiscStatsTableTrigger
                    (pCapwapTrgFsCawapDiscStatsEntry,
                     pCapwapTrgIsSetFsCawapDiscStatsEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCawapDiscStatsTable: CapwapSetAllFsCawapDiscStatsTableTrigger function returns failure.\r\n");

                    MemReleaseMemBlock (CAPWAP_FSCAWAPDISCSTATSTABLE_POOLID,
                                        (UINT1 *) pCapwapFsCawapDiscStatsEntry);
                    MemReleaseMemBlock (CAPWAP_FSCAWAPDISCSTATSTABLE_POOLID,
                                        (UINT1 *)
                                        pCapwapOldFsCawapDiscStatsEntry);
                    MemReleaseMemBlock (CAPWAP_FSCAWAPDISCSTATSTABLE_POOLID,
                                        (UINT1 *)
                                        pCapwapTrgFsCawapDiscStatsEntry);
                    MemReleaseMemBlock
                        (CAPWAP_FSCAWAPDISCSTATSTABLE_ISSET_POOLID,
                         (UINT1 *) pCapwapTrgIsSetFsCawapDiscStatsEntry);
                    return OSIX_FAILURE;
                }
            }
            else if (pCapwapSetFsCawapDiscStatsEntry->MibObject.
                     i4FsCapwapDiscStatsRowStatus == CREATE_AND_WAIT)
            {

                /* For MSR and RM Trigger */
                pCapwapTrgFsCawapDiscStatsEntry->MibObject.
                    i4FsCapwapDiscStatsRowStatus = CREATE_AND_WAIT;
                pCapwapTrgIsSetFsCawapDiscStatsEntry->
                    bFsCapwapDiscStatsRowStatus = OSIX_TRUE;

                if (CapwapSetAllFsCawapDiscStatsTableTrigger
                    (pCapwapTrgFsCawapDiscStatsEntry,
                     pCapwapTrgIsSetFsCawapDiscStatsEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCawapDiscStatsTable: CapwapSetAllFsCawapDiscStatsTableTrigger function returns failure.\r\n");

                    MemReleaseMemBlock (CAPWAP_FSCAWAPDISCSTATSTABLE_POOLID,
                                        (UINT1 *) pCapwapFsCawapDiscStatsEntry);
                    MemReleaseMemBlock (CAPWAP_FSCAWAPDISCSTATSTABLE_POOLID,
                                        (UINT1 *)
                                        pCapwapOldFsCawapDiscStatsEntry);
                    MemReleaseMemBlock (CAPWAP_FSCAWAPDISCSTATSTABLE_POOLID,
                                        (UINT1 *)
                                        pCapwapTrgFsCawapDiscStatsEntry);
                    MemReleaseMemBlock
                        (CAPWAP_FSCAWAPDISCSTATSTABLE_ISSET_POOLID,
                         (UINT1 *) pCapwapTrgIsSetFsCawapDiscStatsEntry);
                    return OSIX_FAILURE;
                }
            }

            if (pCapwapSetFsCawapDiscStatsEntry->MibObject.
                i4FsCapwapDiscStatsRowStatus == CREATE_AND_GO)
            {
                pCapwapSetFsCawapDiscStatsEntry->MibObject.
                    i4FsCapwapDiscStatsRowStatus = ACTIVE;
            }

            if (CapwapSetAllFsCawapDiscStatsTableTrigger
                (pCapwapSetFsCawapDiscStatsEntry,
                 pCapwapIsSetFsCawapDiscStatsEntry,
                 SNMP_SUCCESS) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCawapDiscStatsTable:  CapwapSetAllFsCawapDiscStatsTableTrigger function returns failure.\r\n");
            }
            MemReleaseMemBlock (CAPWAP_FSCAWAPDISCSTATSTABLE_POOLID,
                                (UINT1 *) pCapwapOldFsCawapDiscStatsEntry);
            MemReleaseMemBlock (CAPWAP_FSCAWAPDISCSTATSTABLE_POOLID,
                                (UINT1 *) pCapwapTrgFsCawapDiscStatsEntry);
            MemReleaseMemBlock (CAPWAP_FSCAWAPDISCSTATSTABLE_ISSET_POOLID,
                                (UINT1 *) pCapwapTrgIsSetFsCawapDiscStatsEntry);
            return OSIX_SUCCESS;

        }
        else
        {
            if (CapwapSetAllFsCawapDiscStatsTableTrigger
                (pCapwapSetFsCawapDiscStatsEntry,
                 pCapwapIsSetFsCawapDiscStatsEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCawapDiscStatsTable: CapwapSetAllFsCawapDiscStatsTableTrigger function returns failure.\r\n");
            }
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCawapDiscStatsTable: Failure.\r\n");
            MemReleaseMemBlock (CAPWAP_FSCAWAPDISCSTATSTABLE_POOLID,
                                (UINT1 *) pCapwapOldFsCawapDiscStatsEntry);
            MemReleaseMemBlock (CAPWAP_FSCAWAPDISCSTATSTABLE_POOLID,
                                (UINT1 *) pCapwapTrgFsCawapDiscStatsEntry);
            MemReleaseMemBlock (CAPWAP_FSCAWAPDISCSTATSTABLE_ISSET_POOLID,
                                (UINT1 *) pCapwapTrgIsSetFsCawapDiscStatsEntry);
            return OSIX_FAILURE;
        }
    }
    else if ((pCapwapSetFsCawapDiscStatsEntry->MibObject.
              i4FsCapwapDiscStatsRowStatus == CREATE_AND_WAIT)
             || (pCapwapSetFsCawapDiscStatsEntry->MibObject.
                 i4FsCapwapDiscStatsRowStatus == CREATE_AND_GO))
    {
        if (CapwapSetAllFsCawapDiscStatsTableTrigger
            (pCapwapSetFsCawapDiscStatsEntry, pCapwapIsSetFsCawapDiscStatsEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)

        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCawapDiscStatsTable: CapwapSetAllFsCawapDiscStatsTableTrigger function returns failure.\r\n");
        }
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapSetAllFsCawapDiscStatsTable: The row is already present.\r\n");
        MemReleaseMemBlock (CAPWAP_FSCAWAPDISCSTATSTABLE_POOLID,
                            (UINT1 *) pCapwapOldFsCawapDiscStatsEntry);
        MemReleaseMemBlock (CAPWAP_FSCAWAPDISCSTATSTABLE_POOLID,
                            (UINT1 *) pCapwapTrgFsCawapDiscStatsEntry);
        MemReleaseMemBlock (CAPWAP_FSCAWAPDISCSTATSTABLE_ISSET_POOLID,
                            (UINT1 *) pCapwapTrgIsSetFsCawapDiscStatsEntry);
        return OSIX_FAILURE;
    }
    /* Copy the previous values before setting the new values */
    MEMCPY (pCapwapOldFsCawapDiscStatsEntry, pCapwapFsCawapDiscStatsEntry,
            sizeof (tCapwapFsCawapDiscStatsEntry));

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pCapwapSetFsCawapDiscStatsEntry->MibObject.
        i4FsCapwapDiscStatsRowStatus == DESTROY)
    {
        pCapwapFsCawapDiscStatsEntry->MibObject.i4FsCapwapDiscStatsRowStatus =
            DESTROY;

        if (CapwapUtilUpdateFsCawapDiscStatsTable
            (pCapwapOldFsCawapDiscStatsEntry, pCapwapFsCawapDiscStatsEntry,
             pCapwapIsSetFsCawapDiscStatsEntry) != OSIX_SUCCESS)
        {

            if (CapwapSetAllFsCawapDiscStatsTableTrigger
                (pCapwapSetFsCawapDiscStatsEntry,
                 pCapwapIsSetFsCawapDiscStatsEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCawapDiscStatsTable: CapwapSetAllFsCawapDiscStatsTableTrigger function returns failure.\r\n");
            }
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCawapDiscStatsTable: CapwapUtilUpdateFsCawapDiscStatsTable function returns failure.\r\n");
        }
        RBTreeRem (gCapwapGlobals.CapwapGlbMib.FsCawapDiscStatsTable,
                   pCapwapFsCawapDiscStatsEntry);
        if (CapwapSetAllFsCawapDiscStatsTableTrigger
            (pCapwapSetFsCawapDiscStatsEntry, pCapwapIsSetFsCawapDiscStatsEntry,
             SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCawapDiscStatsTable: CapwapSetAllFsCawapDiscStatsTableTrigger function returns failure.\r\n");
            MemReleaseMemBlock (CAPWAP_FSCAWAPDISCSTATSTABLE_POOLID,
                                (UINT1 *) pCapwapOldFsCawapDiscStatsEntry);
            MemReleaseMemBlock (CAPWAP_FSCAWAPDISCSTATSTABLE_POOLID,
                                (UINT1 *) pCapwapTrgFsCawapDiscStatsEntry);
            MemReleaseMemBlock (CAPWAP_FSCAWAPDISCSTATSTABLE_ISSET_POOLID,
                                (UINT1 *) pCapwapTrgIsSetFsCawapDiscStatsEntry);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (CAPWAP_FSCAWAPDISCSTATSTABLE_POOLID,
                            (UINT1 *) pCapwapFsCawapDiscStatsEntry);
        MemReleaseMemBlock (CAPWAP_FSCAWAPDISCSTATSTABLE_POOLID,
                            (UINT1 *) pCapwapOldFsCawapDiscStatsEntry);
        MemReleaseMemBlock (CAPWAP_FSCAWAPDISCSTATSTABLE_POOLID,
                            (UINT1 *) pCapwapTrgFsCawapDiscStatsEntry);
        MemReleaseMemBlock (CAPWAP_FSCAWAPDISCSTATSTABLE_ISSET_POOLID,
                            (UINT1 *) pCapwapTrgIsSetFsCawapDiscStatsEntry);
        return OSIX_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (FsCawapDiscStatsTableFilterInputs
        (pCapwapFsCawapDiscStatsEntry, pCapwapSetFsCawapDiscStatsEntry,
         pCapwapIsSetFsCawapDiscStatsEntry) != OSIX_TRUE)
    {
        MemReleaseMemBlock (CAPWAP_FSCAWAPDISCSTATSTABLE_POOLID,
                            (UINT1 *) pCapwapOldFsCawapDiscStatsEntry);
        MemReleaseMemBlock (CAPWAP_FSCAWAPDISCSTATSTABLE_POOLID,
                            (UINT1 *) pCapwapTrgFsCawapDiscStatsEntry);
        MemReleaseMemBlock (CAPWAP_FSCAWAPDISCSTATSTABLE_ISSET_POOLID,
                            (UINT1 *) pCapwapTrgIsSetFsCawapDiscStatsEntry);
        return OSIX_SUCCESS;
    }

    /*This condtion is to make the row NOT_IN_SERVICE before modifying the values */
    if ((i4RowStatusLogic == TRUE) &&
        (pCapwapFsCawapDiscStatsEntry->MibObject.i4FsCapwapDiscStatsRowStatus ==
         ACTIVE)
        && (pCapwapSetFsCawapDiscStatsEntry->MibObject.
            i4FsCapwapDiscStatsRowStatus != NOT_IN_SERVICE))
    {
        pCapwapFsCawapDiscStatsEntry->MibObject.i4FsCapwapDiscStatsRowStatus =
            NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        pCapwapTrgFsCawapDiscStatsEntry->MibObject.
            i4FsCapwapDiscStatsRowStatus = NOT_IN_SERVICE;
        pCapwapTrgIsSetFsCawapDiscStatsEntry->bFsCapwapDiscStatsRowStatus =
            OSIX_TRUE;

        if (CapwapUtilUpdateFsCawapDiscStatsTable
            (pCapwapOldFsCawapDiscStatsEntry, pCapwapFsCawapDiscStatsEntry,
             pCapwapIsSetFsCawapDiscStatsEntry) != OSIX_SUCCESS)
        {
            /*Restore back with previous values */
            MEMCPY (pCapwapFsCawapDiscStatsEntry,
                    pCapwapOldFsCawapDiscStatsEntry,
                    sizeof (tCapwapFsCawapDiscStatsEntry));
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCawapDiscStatsTable:                 CapwapUtilUpdateFsCawapDiscStatsTable Function returns failure.\r\n");

            if (CapwapSetAllFsCawapDiscStatsTableTrigger
                (pCapwapSetFsCawapDiscStatsEntry,
                 pCapwapIsSetFsCawapDiscStatsEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCawapDiscStatsTable: CapwapSetAllFsCawapDiscStatsTableTrigger function returns failure.\r\n");
            }
            MemReleaseMemBlock (CAPWAP_FSCAWAPDISCSTATSTABLE_POOLID,
                                (UINT1 *) pCapwapOldFsCawapDiscStatsEntry);
            MemReleaseMemBlock (CAPWAP_FSCAWAPDISCSTATSTABLE_POOLID,
                                (UINT1 *) pCapwapTrgFsCawapDiscStatsEntry);
            MemReleaseMemBlock (CAPWAP_FSCAWAPDISCSTATSTABLE_ISSET_POOLID,
                                (UINT1 *) pCapwapTrgIsSetFsCawapDiscStatsEntry);
            return OSIX_FAILURE;
        }

        if (CapwapSetAllFsCawapDiscStatsTableTrigger
            (pCapwapTrgFsCawapDiscStatsEntry,
             pCapwapTrgIsSetFsCawapDiscStatsEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCawapDiscStatsTable: CapwapSetAllFsCawapDiscStatsTableTrigger function returns failure.\r\n");
        }
    }

    if (pCapwapSetFsCawapDiscStatsEntry->MibObject.
        i4FsCapwapDiscStatsRowStatus == ACTIVE)
    {
        i4RowMakeActive = TRUE;
    }

    /* Assign values for the existing node */
    if (pCapwapIsSetFsCawapDiscStatsEntry->bFsCapwapDiscStatsRowStatus !=
        OSIX_FALSE)
    {
        pCapwapFsCawapDiscStatsEntry->MibObject.i4FsCapwapDiscStatsRowStatus =
            pCapwapSetFsCawapDiscStatsEntry->MibObject.
            i4FsCapwapDiscStatsRowStatus;
    }

    /*This condtion is to make the row back to ACTIVE after modifying the values */
    if (i4RowMakeActive == TRUE)
    {
        pCapwapFsCawapDiscStatsEntry->MibObject.i4FsCapwapDiscStatsRowStatus =
            ACTIVE;
    }

    if (CapwapUtilUpdateFsCawapDiscStatsTable (pCapwapOldFsCawapDiscStatsEntry,
                                               pCapwapFsCawapDiscStatsEntry,
                                               pCapwapIsSetFsCawapDiscStatsEntry)
        != OSIX_SUCCESS)
    {

        if (CapwapSetAllFsCawapDiscStatsTableTrigger
            (pCapwapSetFsCawapDiscStatsEntry, pCapwapIsSetFsCawapDiscStatsEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)

        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCawapDiscStatsTable: CapwapSetAllFsCawapDiscStatsTableTrigger function returns failure.\r\n");

        }
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapSetAllFsCawapDiscStatsTable: CapwapUtilUpdateFsCawapDiscStatsTable function returns failure.\r\n");
        /*Restore back with previous values */
        MEMCPY (pCapwapFsCawapDiscStatsEntry, pCapwapOldFsCawapDiscStatsEntry,
                sizeof (tCapwapFsCawapDiscStatsEntry));
        MemReleaseMemBlock (CAPWAP_FSCAWAPDISCSTATSTABLE_POOLID,
                            (UINT1 *) pCapwapOldFsCawapDiscStatsEntry);
        MemReleaseMemBlock (CAPWAP_FSCAWAPDISCSTATSTABLE_POOLID,
                            (UINT1 *) pCapwapTrgFsCawapDiscStatsEntry);
        MemReleaseMemBlock (CAPWAP_FSCAWAPDISCSTATSTABLE_ISSET_POOLID,
                            (UINT1 *) pCapwapTrgIsSetFsCawapDiscStatsEntry);
        return OSIX_FAILURE;

    }
    if (CapwapSetAllFsCawapDiscStatsTableTrigger
        (pCapwapSetFsCawapDiscStatsEntry, pCapwapIsSetFsCawapDiscStatsEntry,
         SNMP_SUCCESS) != OSIX_SUCCESS)
    {

        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapSetAllFsCawapDiscStatsTable: CapwapSetAllFsCawapDiscStatsTableTrigger function returns failure.\r\n");
    }

    MemReleaseMemBlock (CAPWAP_FSCAWAPDISCSTATSTABLE_POOLID,
                        (UINT1 *) pCapwapOldFsCawapDiscStatsEntry);
    MemReleaseMemBlock (CAPWAP_FSCAWAPDISCSTATSTABLE_POOLID,
                        (UINT1 *) pCapwapTrgFsCawapDiscStatsEntry);
    MemReleaseMemBlock (CAPWAP_FSCAWAPDISCSTATSTABLE_ISSET_POOLID,
                        (UINT1 *) pCapwapTrgIsSetFsCawapDiscStatsEntry);
    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  CapwapSetAllFsCawapJoinStatsTable
 Input       :  pCapwapSetFsCawapJoinStatsEntry
                pCapwapIsSetFsCawapJoinStatsEntry
                i4RowStatusLogic
                i4RowCreateOption
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapSetAllFsCawapJoinStatsTable (tCapwapFsCawapJoinStatsEntry *
                                   pCapwapSetFsCawapJoinStatsEntry,
                                   tCapwapIsSetFsCawapJoinStatsEntry *
                                   pCapwapIsSetFsCawapJoinStatsEntry,
                                   INT4 i4RowStatusLogic,
                                   INT4 i4RowCreateOption)
{
    tCapwapFsCawapJoinStatsEntry *pCapwapFsCawapJoinStatsEntry = NULL;
    tCapwapFsCawapJoinStatsEntry *pCapwapOldFsCawapJoinStatsEntry = NULL;
    tCapwapFsCawapJoinStatsEntry *pCapwapTrgFsCawapJoinStatsEntry = NULL;
    tCapwapIsSetFsCawapJoinStatsEntry *pCapwapTrgIsSetFsCawapJoinStatsEntry =
        NULL;
    INT4                i4RowMakeActive = FALSE;

    pCapwapOldFsCawapJoinStatsEntry =
        (tCapwapFsCawapJoinStatsEntry *)
        MemAllocMemBlk (CAPWAP_FSCAWAPJOINSTATSTABLE_POOLID);
    if (pCapwapOldFsCawapJoinStatsEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    pCapwapTrgFsCawapJoinStatsEntry =
        (tCapwapFsCawapJoinStatsEntry *)
        MemAllocMemBlk (CAPWAP_FSCAWAPJOINSTATSTABLE_POOLID);
    if (pCapwapTrgFsCawapJoinStatsEntry == NULL)
    {
        MemReleaseMemBlock (CAPWAP_FSCAWAPJOINSTATSTABLE_POOLID,
                            (UINT1 *) pCapwapOldFsCawapJoinStatsEntry);
        return OSIX_FAILURE;
    }
    pCapwapTrgIsSetFsCawapJoinStatsEntry =
        (tCapwapIsSetFsCawapJoinStatsEntry *)
        MemAllocMemBlk (CAPWAP_FSCAWAPJOINSTATSTABLE_ISSET_POOLID);
    if (pCapwapTrgIsSetFsCawapJoinStatsEntry == NULL)
    {
        MemReleaseMemBlock (CAPWAP_FSCAWAPJOINSTATSTABLE_POOLID,
                            (UINT1 *) pCapwapOldFsCawapJoinStatsEntry);
        MemReleaseMemBlock (CAPWAP_FSCAWAPJOINSTATSTABLE_POOLID,
                            (UINT1 *) pCapwapTrgFsCawapJoinStatsEntry);
        return OSIX_FAILURE;
    }
    MEMSET (pCapwapOldFsCawapJoinStatsEntry, 0,
            sizeof (tCapwapFsCawapJoinStatsEntry));
    MEMSET (pCapwapTrgFsCawapJoinStatsEntry, 0,
            sizeof (tCapwapFsCawapJoinStatsEntry));
    MEMSET (pCapwapTrgIsSetFsCawapJoinStatsEntry, 0,
            sizeof (tCapwapIsSetFsCawapJoinStatsEntry));

    /* Check whether the node is already present */
    pCapwapFsCawapJoinStatsEntry =
        RBTreeGet (gCapwapGlobals.CapwapGlbMib.FsCawapJoinStatsTable,
                   (tRBElem *) pCapwapSetFsCawapJoinStatsEntry);

    if (pCapwapFsCawapJoinStatsEntry == NULL)
    {
        /* Create the node if the RowStatus given is CREATE_AND_WAIT or CREATE_AND_GO */
        if ((pCapwapSetFsCawapJoinStatsEntry->MibObject.
             i4FsCapwapJoinStatsRowStatus == CREATE_AND_WAIT)
            || (pCapwapSetFsCawapJoinStatsEntry->MibObject.
                i4FsCapwapJoinStatsRowStatus == CREATE_AND_GO)
            ||
            ((pCapwapSetFsCawapJoinStatsEntry->MibObject.
              i4FsCapwapJoinStatsRowStatus == ACTIVE)
             && (i4RowCreateOption == 1)))
        {
            /* Allocate memory for the new node */
            pCapwapFsCawapJoinStatsEntry =
                (tCapwapFsCawapJoinStatsEntry *)
                MemAllocMemBlk (CAPWAP_FSCAWAPJOINSTATSTABLE_POOLID);
            if (pCapwapFsCawapJoinStatsEntry == NULL)
            {
                if (CapwapSetAllFsCawapJoinStatsTableTrigger
                    (pCapwapSetFsCawapJoinStatsEntry,
                     pCapwapIsSetFsCawapJoinStatsEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCawapJoinStatsTable:CapwapSetAllFsCawapJoinStatsTableTrigger function fails\r\n");

                }
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCawapJoinStatsTable: Fail to Allocate Memory.\r\n");
                MemReleaseMemBlock (CAPWAP_FSCAWAPJOINSTATSTABLE_POOLID,
                                    (UINT1 *) pCapwapOldFsCawapJoinStatsEntry);
                MemReleaseMemBlock (CAPWAP_FSCAWAPJOINSTATSTABLE_POOLID,
                                    (UINT1 *) pCapwapTrgFsCawapJoinStatsEntry);
                MemReleaseMemBlock (CAPWAP_FSCAWAPJOINSTATSTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgIsSetFsCawapJoinStatsEntry);
                return OSIX_FAILURE;
            }

            MEMSET (pCapwapFsCawapJoinStatsEntry, 0,
                    sizeof (tCapwapFsCawapJoinStatsEntry));
            if ((CapwapInitializeFsCawapJoinStatsTable
                 (pCapwapFsCawapJoinStatsEntry)) == OSIX_FAILURE)
            {
                if (CapwapSetAllFsCawapJoinStatsTableTrigger
                    (pCapwapSetFsCawapJoinStatsEntry,
                     pCapwapIsSetFsCawapJoinStatsEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCawapJoinStatsTable:CapwapSetAllFsCawapJoinStatsTableTrigger function fails\r\n");

                }
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCawapJoinStatsTable: Fail to Initialize the Objects.\r\n");

                MemReleaseMemBlock (CAPWAP_FSCAWAPJOINSTATSTABLE_POOLID,
                                    (UINT1 *) pCapwapFsCawapJoinStatsEntry);
                MemReleaseMemBlock (CAPWAP_FSCAWAPJOINSTATSTABLE_POOLID,
                                    (UINT1 *) pCapwapOldFsCawapJoinStatsEntry);
                MemReleaseMemBlock (CAPWAP_FSCAWAPJOINSTATSTABLE_POOLID,
                                    (UINT1 *) pCapwapTrgFsCawapJoinStatsEntry);
                MemReleaseMemBlock (CAPWAP_FSCAWAPJOINSTATSTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgIsSetFsCawapJoinStatsEntry);
                return OSIX_FAILURE;
            }
            /* Assign values for the new node */
            if (pCapwapIsSetFsCawapJoinStatsEntry->
                bFsCapwapJoinStatsRowStatus != OSIX_FALSE)
            {
                pCapwapFsCawapJoinStatsEntry->MibObject.
                    i4FsCapwapJoinStatsRowStatus =
                    pCapwapSetFsCawapJoinStatsEntry->MibObject.
                    i4FsCapwapJoinStatsRowStatus;
            }

            if (pCapwapIsSetFsCawapJoinStatsEntry->bCapwapBaseWtpProfileId !=
                OSIX_FALSE)
            {
                pCapwapFsCawapJoinStatsEntry->MibObject.
                    u4CapwapBaseWtpProfileId =
                    pCapwapSetFsCawapJoinStatsEntry->MibObject.
                    u4CapwapBaseWtpProfileId;
            }

            if ((pCapwapSetFsCawapJoinStatsEntry->MibObject.
                 i4FsCapwapJoinStatsRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pCapwapSetFsCawapJoinStatsEntry->MibObject.
                        i4FsCapwapJoinStatsRowStatus == ACTIVE)))
            {
                pCapwapFsCawapJoinStatsEntry->MibObject.
                    i4FsCapwapJoinStatsRowStatus = ACTIVE;
            }
            else if (pCapwapSetFsCawapJoinStatsEntry->MibObject.
                     i4FsCapwapJoinStatsRowStatus == CREATE_AND_WAIT)
            {
                pCapwapFsCawapJoinStatsEntry->MibObject.
                    i4FsCapwapJoinStatsRowStatus = NOT_READY;
            }

            /* Add the new node to the database */
            if (RBTreeAdd
                (gCapwapGlobals.CapwapGlbMib.FsCawapJoinStatsTable,
                 (tRBElem *) pCapwapFsCawapJoinStatsEntry) != RB_SUCCESS)
            {
                if (CapwapSetAllFsCawapJoinStatsTableTrigger
                    (pCapwapSetFsCawapJoinStatsEntry,
                     pCapwapIsSetFsCawapJoinStatsEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCawapJoinStatsTable: CapwapSetAllFsCawapJoinStatsTableTrigger function returns failure.\r\n");
                }
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCawapJoinStatsTable: RBTreeAdd is failed.\r\n");

                MemReleaseMemBlock (CAPWAP_FSCAWAPJOINSTATSTABLE_POOLID,
                                    (UINT1 *) pCapwapFsCawapJoinStatsEntry);
                MemReleaseMemBlock (CAPWAP_FSCAWAPJOINSTATSTABLE_POOLID,
                                    (UINT1 *) pCapwapOldFsCawapJoinStatsEntry);
                MemReleaseMemBlock (CAPWAP_FSCAWAPJOINSTATSTABLE_POOLID,
                                    (UINT1 *) pCapwapTrgFsCawapJoinStatsEntry);
                MemReleaseMemBlock (CAPWAP_FSCAWAPJOINSTATSTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgIsSetFsCawapJoinStatsEntry);
                return OSIX_FAILURE;
            }
            if (CapwapUtilUpdateFsCawapJoinStatsTable
                (NULL, pCapwapFsCawapJoinStatsEntry,
                 pCapwapIsSetFsCawapJoinStatsEntry) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCawapJoinStatsTable: CapwapUtilUpdateFsCawapJoinStatsTable function returns failure.\r\n");

                if (CapwapSetAllFsCawapJoinStatsTableTrigger
                    (pCapwapSetFsCawapJoinStatsEntry,
                     pCapwapIsSetFsCawapJoinStatsEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCawapJoinStatsTable: CapwapSetAllFsCawapJoinStatsTableTrigger function returns failure.\r\n");

                }
                RBTreeRem (gCapwapGlobals.CapwapGlbMib.FsCawapJoinStatsTable,
                           pCapwapFsCawapJoinStatsEntry);
                MemReleaseMemBlock (CAPWAP_FSCAWAPJOINSTATSTABLE_POOLID,
                                    (UINT1 *) pCapwapFsCawapJoinStatsEntry);
                MemReleaseMemBlock (CAPWAP_FSCAWAPJOINSTATSTABLE_POOLID,
                                    (UINT1 *) pCapwapOldFsCawapJoinStatsEntry);
                MemReleaseMemBlock (CAPWAP_FSCAWAPJOINSTATSTABLE_POOLID,
                                    (UINT1 *) pCapwapTrgFsCawapJoinStatsEntry);
                MemReleaseMemBlock (CAPWAP_FSCAWAPJOINSTATSTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgIsSetFsCawapJoinStatsEntry);
                return OSIX_FAILURE;
            }

            if ((pCapwapSetFsCawapJoinStatsEntry->MibObject.
                 i4FsCapwapJoinStatsRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pCapwapSetFsCawapJoinStatsEntry->MibObject.
                        i4FsCapwapJoinStatsRowStatus == ACTIVE)))
            {

                /* For MSR and RM Trigger */
                pCapwapTrgFsCawapJoinStatsEntry->MibObject.
                    u4CapwapBaseWtpProfileId =
                    pCapwapSetFsCawapJoinStatsEntry->MibObject.
                    u4CapwapBaseWtpProfileId;
                pCapwapTrgFsCawapJoinStatsEntry->MibObject.
                    i4FsCapwapJoinStatsRowStatus = CREATE_AND_WAIT;
                pCapwapTrgIsSetFsCawapJoinStatsEntry->
                    bFsCapwapJoinStatsRowStatus = OSIX_TRUE;

                if (CapwapSetAllFsCawapJoinStatsTableTrigger
                    (pCapwapTrgFsCawapJoinStatsEntry,
                     pCapwapTrgIsSetFsCawapJoinStatsEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCawapJoinStatsTable: CapwapSetAllFsCawapJoinStatsTableTrigger function returns failure.\r\n");

                    MemReleaseMemBlock (CAPWAP_FSCAWAPJOINSTATSTABLE_POOLID,
                                        (UINT1 *) pCapwapFsCawapJoinStatsEntry);
                    MemReleaseMemBlock (CAPWAP_FSCAWAPJOINSTATSTABLE_POOLID,
                                        (UINT1 *)
                                        pCapwapOldFsCawapJoinStatsEntry);
                    MemReleaseMemBlock (CAPWAP_FSCAWAPJOINSTATSTABLE_POOLID,
                                        (UINT1 *)
                                        pCapwapTrgFsCawapJoinStatsEntry);
                    MemReleaseMemBlock
                        (CAPWAP_FSCAWAPJOINSTATSTABLE_ISSET_POOLID,
                         (UINT1 *) pCapwapTrgIsSetFsCawapJoinStatsEntry);
                    return OSIX_FAILURE;
                }
            }
            else if (pCapwapSetFsCawapJoinStatsEntry->MibObject.
                     i4FsCapwapJoinStatsRowStatus == CREATE_AND_WAIT)
            {

                /* For MSR and RM Trigger */
                pCapwapTrgFsCawapJoinStatsEntry->MibObject.
                    i4FsCapwapJoinStatsRowStatus = CREATE_AND_WAIT;
                pCapwapTrgIsSetFsCawapJoinStatsEntry->
                    bFsCapwapJoinStatsRowStatus = OSIX_TRUE;

                if (CapwapSetAllFsCawapJoinStatsTableTrigger
                    (pCapwapTrgFsCawapJoinStatsEntry,
                     pCapwapTrgIsSetFsCawapJoinStatsEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCawapJoinStatsTable: CapwapSetAllFsCawapJoinStatsTableTrigger function returns failure.\r\n");

                    MemReleaseMemBlock (CAPWAP_FSCAWAPJOINSTATSTABLE_POOLID,
                                        (UINT1 *) pCapwapFsCawapJoinStatsEntry);
                    MemReleaseMemBlock (CAPWAP_FSCAWAPJOINSTATSTABLE_POOLID,
                                        (UINT1 *)
                                        pCapwapOldFsCawapJoinStatsEntry);
                    MemReleaseMemBlock (CAPWAP_FSCAWAPJOINSTATSTABLE_POOLID,
                                        (UINT1 *)
                                        pCapwapTrgFsCawapJoinStatsEntry);
                    MemReleaseMemBlock
                        (CAPWAP_FSCAWAPJOINSTATSTABLE_ISSET_POOLID,
                         (UINT1 *) pCapwapTrgIsSetFsCawapJoinStatsEntry);
                    return OSIX_FAILURE;
                }
            }

            if (pCapwapSetFsCawapJoinStatsEntry->MibObject.
                i4FsCapwapJoinStatsRowStatus == CREATE_AND_GO)
            {
                pCapwapSetFsCawapJoinStatsEntry->MibObject.
                    i4FsCapwapJoinStatsRowStatus = ACTIVE;
            }

            if (CapwapSetAllFsCawapJoinStatsTableTrigger
                (pCapwapSetFsCawapJoinStatsEntry,
                 pCapwapIsSetFsCawapJoinStatsEntry,
                 SNMP_SUCCESS) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCawapJoinStatsTable:  CapwapSetAllFsCawapJoinStatsTableTrigger function returns failure.\r\n");
            }
            MemReleaseMemBlock (CAPWAP_FSCAWAPJOINSTATSTABLE_POOLID,
                                (UINT1 *) pCapwapOldFsCawapJoinStatsEntry);
            MemReleaseMemBlock (CAPWAP_FSCAWAPJOINSTATSTABLE_POOLID,
                                (UINT1 *) pCapwapTrgFsCawapJoinStatsEntry);
            MemReleaseMemBlock (CAPWAP_FSCAWAPJOINSTATSTABLE_ISSET_POOLID,
                                (UINT1 *) pCapwapTrgIsSetFsCawapJoinStatsEntry);
            return OSIX_SUCCESS;

        }
        else
        {
            if (CapwapSetAllFsCawapJoinStatsTableTrigger
                (pCapwapSetFsCawapJoinStatsEntry,
                 pCapwapIsSetFsCawapJoinStatsEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCawapJoinStatsTable: CapwapSetAllFsCawapJoinStatsTableTrigger function returns failure.\r\n");
            }
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCawapJoinStatsTable: Failure.\r\n");
            MemReleaseMemBlock (CAPWAP_FSCAWAPJOINSTATSTABLE_POOLID,
                                (UINT1 *) pCapwapOldFsCawapJoinStatsEntry);
            MemReleaseMemBlock (CAPWAP_FSCAWAPJOINSTATSTABLE_POOLID,
                                (UINT1 *) pCapwapTrgFsCawapJoinStatsEntry);
            MemReleaseMemBlock (CAPWAP_FSCAWAPJOINSTATSTABLE_ISSET_POOLID,
                                (UINT1 *) pCapwapTrgIsSetFsCawapJoinStatsEntry);
            return OSIX_FAILURE;
        }
    }
    else if ((pCapwapSetFsCawapJoinStatsEntry->MibObject.
              i4FsCapwapJoinStatsRowStatus == CREATE_AND_WAIT)
             || (pCapwapSetFsCawapJoinStatsEntry->MibObject.
                 i4FsCapwapJoinStatsRowStatus == CREATE_AND_GO))
    {
        if (CapwapSetAllFsCawapJoinStatsTableTrigger
            (pCapwapSetFsCawapJoinStatsEntry, pCapwapIsSetFsCawapJoinStatsEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)

        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCawapJoinStatsTable: CapwapSetAllFsCawapJoinStatsTableTrigger function returns failure.\r\n");
        }
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapSetAllFsCawapJoinStatsTable: The row is already present.\r\n");
        MemReleaseMemBlock (CAPWAP_FSCAWAPJOINSTATSTABLE_POOLID,
                            (UINT1 *) pCapwapOldFsCawapJoinStatsEntry);
        MemReleaseMemBlock (CAPWAP_FSCAWAPJOINSTATSTABLE_POOLID,
                            (UINT1 *) pCapwapTrgFsCawapJoinStatsEntry);
        MemReleaseMemBlock (CAPWAP_FSCAWAPJOINSTATSTABLE_ISSET_POOLID,
                            (UINT1 *) pCapwapTrgIsSetFsCawapJoinStatsEntry);
        return OSIX_FAILURE;
    }
    /* Copy the previous values before setting the new values */
    MEMCPY (pCapwapOldFsCawapJoinStatsEntry, pCapwapFsCawapJoinStatsEntry,
            sizeof (tCapwapFsCawapJoinStatsEntry));

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pCapwapSetFsCawapJoinStatsEntry->MibObject.
        i4FsCapwapJoinStatsRowStatus == DESTROY)
    {
        pCapwapFsCawapJoinStatsEntry->MibObject.i4FsCapwapJoinStatsRowStatus =
            DESTROY;

        if (CapwapUtilUpdateFsCawapJoinStatsTable
            (pCapwapOldFsCawapJoinStatsEntry, pCapwapFsCawapJoinStatsEntry,
             pCapwapIsSetFsCawapJoinStatsEntry) != OSIX_SUCCESS)
        {

            if (CapwapSetAllFsCawapJoinStatsTableTrigger
                (pCapwapSetFsCawapJoinStatsEntry,
                 pCapwapIsSetFsCawapJoinStatsEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCawapJoinStatsTable: CapwapSetAllFsCawapJoinStatsTableTrigger function returns failure.\r\n");
            }
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCawapJoinStatsTable: CapwapUtilUpdateFsCawapJoinStatsTable function returns failure.\r\n");
        }
        RBTreeRem (gCapwapGlobals.CapwapGlbMib.FsCawapJoinStatsTable,
                   pCapwapFsCawapJoinStatsEntry);
        if (CapwapSetAllFsCawapJoinStatsTableTrigger
            (pCapwapSetFsCawapJoinStatsEntry, pCapwapIsSetFsCawapJoinStatsEntry,
             SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCawapJoinStatsTable: CapwapSetAllFsCawapJoinStatsTableTrigger function returns failure.\r\n");
            MemReleaseMemBlock (CAPWAP_FSCAWAPJOINSTATSTABLE_POOLID,
                                (UINT1 *) pCapwapOldFsCawapJoinStatsEntry);
            MemReleaseMemBlock (CAPWAP_FSCAWAPJOINSTATSTABLE_POOLID,
                                (UINT1 *) pCapwapTrgFsCawapJoinStatsEntry);
            MemReleaseMemBlock (CAPWAP_FSCAWAPJOINSTATSTABLE_ISSET_POOLID,
                                (UINT1 *) pCapwapTrgIsSetFsCawapJoinStatsEntry);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (CAPWAP_FSCAWAPJOINSTATSTABLE_POOLID,
                            (UINT1 *) pCapwapFsCawapJoinStatsEntry);
        MemReleaseMemBlock (CAPWAP_FSCAWAPJOINSTATSTABLE_POOLID,
                            (UINT1 *) pCapwapOldFsCawapJoinStatsEntry);
        MemReleaseMemBlock (CAPWAP_FSCAWAPJOINSTATSTABLE_POOLID,
                            (UINT1 *) pCapwapTrgFsCawapJoinStatsEntry);
        MemReleaseMemBlock (CAPWAP_FSCAWAPJOINSTATSTABLE_ISSET_POOLID,
                            (UINT1 *) pCapwapTrgIsSetFsCawapJoinStatsEntry);
        return OSIX_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (FsCawapJoinStatsTableFilterInputs
        (pCapwapFsCawapJoinStatsEntry, pCapwapSetFsCawapJoinStatsEntry,
         pCapwapIsSetFsCawapJoinStatsEntry) != OSIX_TRUE)
    {
        MemReleaseMemBlock (CAPWAP_FSCAWAPJOINSTATSTABLE_POOLID,
                            (UINT1 *) pCapwapOldFsCawapJoinStatsEntry);
        MemReleaseMemBlock (CAPWAP_FSCAWAPJOINSTATSTABLE_POOLID,
                            (UINT1 *) pCapwapTrgFsCawapJoinStatsEntry);
        MemReleaseMemBlock (CAPWAP_FSCAWAPJOINSTATSTABLE_ISSET_POOLID,
                            (UINT1 *) pCapwapTrgIsSetFsCawapJoinStatsEntry);
        return OSIX_SUCCESS;
    }

    /*This condtion is to make the row NOT_IN_SERVICE before modifying the values */
    if ((i4RowStatusLogic == TRUE) &&
        (pCapwapFsCawapJoinStatsEntry->MibObject.i4FsCapwapJoinStatsRowStatus ==
         ACTIVE)
        && (pCapwapSetFsCawapJoinStatsEntry->MibObject.
            i4FsCapwapJoinStatsRowStatus != NOT_IN_SERVICE))
    {
        pCapwapFsCawapJoinStatsEntry->MibObject.i4FsCapwapJoinStatsRowStatus =
            NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        pCapwapTrgFsCawapJoinStatsEntry->MibObject.
            i4FsCapwapJoinStatsRowStatus = NOT_IN_SERVICE;
        pCapwapTrgIsSetFsCawapJoinStatsEntry->bFsCapwapJoinStatsRowStatus =
            OSIX_TRUE;

        if (CapwapUtilUpdateFsCawapJoinStatsTable
            (pCapwapOldFsCawapJoinStatsEntry, pCapwapFsCawapJoinStatsEntry,
             pCapwapIsSetFsCawapJoinStatsEntry) != OSIX_SUCCESS)
        {
            /*Restore back with previous values */
            MEMCPY (pCapwapFsCawapJoinStatsEntry,
                    pCapwapOldFsCawapJoinStatsEntry,
                    sizeof (tCapwapFsCawapJoinStatsEntry));
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCawapJoinStatsTable:                 CapwapUtilUpdateFsCawapJoinStatsTable Function returns failure.\r\n");

            if (CapwapSetAllFsCawapJoinStatsTableTrigger
                (pCapwapSetFsCawapJoinStatsEntry,
                 pCapwapIsSetFsCawapJoinStatsEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCawapJoinStatsTable: CapwapSetAllFsCawapJoinStatsTableTrigger function returns failure.\r\n");
            }
            MemReleaseMemBlock (CAPWAP_FSCAWAPJOINSTATSTABLE_POOLID,
                                (UINT1 *) pCapwapOldFsCawapJoinStatsEntry);
            MemReleaseMemBlock (CAPWAP_FSCAWAPJOINSTATSTABLE_POOLID,
                                (UINT1 *) pCapwapTrgFsCawapJoinStatsEntry);
            MemReleaseMemBlock (CAPWAP_FSCAWAPJOINSTATSTABLE_ISSET_POOLID,
                                (UINT1 *) pCapwapTrgIsSetFsCawapJoinStatsEntry);
            return OSIX_FAILURE;
        }

        if (CapwapSetAllFsCawapJoinStatsTableTrigger
            (pCapwapTrgFsCawapJoinStatsEntry,
             pCapwapTrgIsSetFsCawapJoinStatsEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCawapJoinStatsTable: CapwapSetAllFsCawapJoinStatsTableTrigger function returns failure.\r\n");
        }
    }

    if (pCapwapSetFsCawapJoinStatsEntry->MibObject.
        i4FsCapwapJoinStatsRowStatus == ACTIVE)
    {
        i4RowMakeActive = TRUE;
    }

    /* Assign values for the existing node */
    if (pCapwapIsSetFsCawapJoinStatsEntry->bFsCapwapJoinStatsRowStatus !=
        OSIX_FALSE)
    {
        pCapwapFsCawapJoinStatsEntry->MibObject.i4FsCapwapJoinStatsRowStatus =
            pCapwapSetFsCawapJoinStatsEntry->MibObject.
            i4FsCapwapJoinStatsRowStatus;
    }

    /*This condtion is to make the row back to ACTIVE after modifying the values */
    if (i4RowMakeActive == TRUE)
    {
        pCapwapFsCawapJoinStatsEntry->MibObject.i4FsCapwapJoinStatsRowStatus =
            ACTIVE;
    }

    if (CapwapUtilUpdateFsCawapJoinStatsTable (pCapwapOldFsCawapJoinStatsEntry,
                                               pCapwapFsCawapJoinStatsEntry,
                                               pCapwapIsSetFsCawapJoinStatsEntry)
        != OSIX_SUCCESS)
    {

        if (CapwapSetAllFsCawapJoinStatsTableTrigger
            (pCapwapSetFsCawapJoinStatsEntry, pCapwapIsSetFsCawapJoinStatsEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)

        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCawapJoinStatsTable: CapwapSetAllFsCawapJoinStatsTableTrigger function returns failure.\r\n");

        }
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapSetAllFsCawapJoinStatsTable: CapwapUtilUpdateFsCawapJoinStatsTable function returns failure.\r\n");
        /*Restore back with previous values */
        MEMCPY (pCapwapFsCawapJoinStatsEntry, pCapwapOldFsCawapJoinStatsEntry,
                sizeof (tCapwapFsCawapJoinStatsEntry));
        MemReleaseMemBlock (CAPWAP_FSCAWAPJOINSTATSTABLE_POOLID,
                            (UINT1 *) pCapwapOldFsCawapJoinStatsEntry);
        MemReleaseMemBlock (CAPWAP_FSCAWAPJOINSTATSTABLE_POOLID,
                            (UINT1 *) pCapwapTrgFsCawapJoinStatsEntry);
        MemReleaseMemBlock (CAPWAP_FSCAWAPJOINSTATSTABLE_ISSET_POOLID,
                            (UINT1 *) pCapwapTrgIsSetFsCawapJoinStatsEntry);
        return OSIX_FAILURE;

    }
    if (CapwapSetAllFsCawapJoinStatsTableTrigger
        (pCapwapSetFsCawapJoinStatsEntry, pCapwapIsSetFsCawapJoinStatsEntry,
         SNMP_SUCCESS) != OSIX_SUCCESS)
    {

        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapSetAllFsCawapJoinStatsTable: CapwapSetAllFsCawapJoinStatsTableTrigger function returns failure.\r\n");
    }

    MemReleaseMemBlock (CAPWAP_FSCAWAPJOINSTATSTABLE_POOLID,
                        (UINT1 *) pCapwapOldFsCawapJoinStatsEntry);
    MemReleaseMemBlock (CAPWAP_FSCAWAPJOINSTATSTABLE_POOLID,
                        (UINT1 *) pCapwapTrgFsCawapJoinStatsEntry);
    MemReleaseMemBlock (CAPWAP_FSCAWAPJOINSTATSTABLE_ISSET_POOLID,
                        (UINT1 *) pCapwapTrgIsSetFsCawapJoinStatsEntry);
    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  CapwapSetAllFsCawapConfigStatsTable
 Input       :  pCapwapSetFsCawapConfigStatsEntry
                pCapwapIsSetFsCawapConfigStatsEntry
                i4RowStatusLogic
                i4RowCreateOption
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapSetAllFsCawapConfigStatsTable (tCapwapFsCawapConfigStatsEntry *
                                     pCapwapSetFsCawapConfigStatsEntry,
                                     tCapwapIsSetFsCawapConfigStatsEntry *
                                     pCapwapIsSetFsCawapConfigStatsEntry,
                                     INT4 i4RowStatusLogic,
                                     INT4 i4RowCreateOption)
{
    tCapwapFsCawapConfigStatsEntry *pCapwapFsCawapConfigStatsEntry = NULL;
    tCapwapFsCawapConfigStatsEntry *pCapwapOldFsCawapConfigStatsEntry = NULL;
    tCapwapFsCawapConfigStatsEntry *pCapwapTrgFsCawapConfigStatsEntry = NULL;
    tCapwapIsSetFsCawapConfigStatsEntry *pCapwapTrgIsSetFsCawapConfigStatsEntry
        = NULL;
    INT4                i4RowMakeActive = FALSE;

    pCapwapOldFsCawapConfigStatsEntry =
        (tCapwapFsCawapConfigStatsEntry *)
        MemAllocMemBlk (CAPWAP_FSCAWAPCONFIGSTATSTABLE_POOLID);
    if (pCapwapOldFsCawapConfigStatsEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    pCapwapTrgFsCawapConfigStatsEntry =
        (tCapwapFsCawapConfigStatsEntry *)
        MemAllocMemBlk (CAPWAP_FSCAWAPCONFIGSTATSTABLE_POOLID);
    if (pCapwapTrgFsCawapConfigStatsEntry == NULL)
    {
        MemReleaseMemBlock (CAPWAP_FSCAWAPCONFIGSTATSTABLE_POOLID,
                            (UINT1 *) pCapwapOldFsCawapConfigStatsEntry);
        return OSIX_FAILURE;
    }
    pCapwapTrgIsSetFsCawapConfigStatsEntry =
        (tCapwapIsSetFsCawapConfigStatsEntry *)
        MemAllocMemBlk (CAPWAP_FSCAWAPCONFIGSTATSTABLE_ISSET_POOLID);
    if (pCapwapTrgIsSetFsCawapConfigStatsEntry == NULL)
    {
        MemReleaseMemBlock (CAPWAP_FSCAWAPCONFIGSTATSTABLE_POOLID,
                            (UINT1 *) pCapwapOldFsCawapConfigStatsEntry);
        MemReleaseMemBlock (CAPWAP_FSCAWAPCONFIGSTATSTABLE_POOLID,
                            (UINT1 *) pCapwapTrgFsCawapConfigStatsEntry);
        return OSIX_FAILURE;
    }
    MEMSET (pCapwapOldFsCawapConfigStatsEntry, 0,
            sizeof (tCapwapFsCawapConfigStatsEntry));
    MEMSET (pCapwapTrgFsCawapConfigStatsEntry, 0,
            sizeof (tCapwapFsCawapConfigStatsEntry));
    MEMSET (pCapwapTrgIsSetFsCawapConfigStatsEntry, 0,
            sizeof (tCapwapIsSetFsCawapConfigStatsEntry));

    /* Check whether the node is already present */
    pCapwapFsCawapConfigStatsEntry =
        RBTreeGet (gCapwapGlobals.CapwapGlbMib.FsCawapConfigStatsTable,
                   (tRBElem *) pCapwapSetFsCawapConfigStatsEntry);

    if (pCapwapFsCawapConfigStatsEntry == NULL)
    {
        /* Create the node if the RowStatus given is CREATE_AND_WAIT or CREATE_AND_GO */
        if ((pCapwapSetFsCawapConfigStatsEntry->MibObject.
             i4FsCapwapConfigStatsRowStatus == CREATE_AND_WAIT)
            || (pCapwapSetFsCawapConfigStatsEntry->MibObject.
                i4FsCapwapConfigStatsRowStatus == CREATE_AND_GO)
            ||
            ((pCapwapSetFsCawapConfigStatsEntry->MibObject.
              i4FsCapwapConfigStatsRowStatus == ACTIVE)
             && (i4RowCreateOption == 1)))
        {
            /* Allocate memory for the new node */
            pCapwapFsCawapConfigStatsEntry =
                (tCapwapFsCawapConfigStatsEntry *)
                MemAllocMemBlk (CAPWAP_FSCAWAPCONFIGSTATSTABLE_POOLID);
            if (pCapwapFsCawapConfigStatsEntry == NULL)
            {
                if (CapwapSetAllFsCawapConfigStatsTableTrigger
                    (pCapwapSetFsCawapConfigStatsEntry,
                     pCapwapIsSetFsCawapConfigStatsEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCawapConfigStatsTable:CapwapSetAllFsCawapConfigStatsTableTrigger function fails\r\n");

                }
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCawapConfigStatsTable: Fail to Allocate Memory.\r\n");
                MemReleaseMemBlock (CAPWAP_FSCAWAPCONFIGSTATSTABLE_POOLID,
                                    (UINT1 *)
                                    pCapwapOldFsCawapConfigStatsEntry);
                MemReleaseMemBlock (CAPWAP_FSCAWAPCONFIGSTATSTABLE_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgFsCawapConfigStatsEntry);
                MemReleaseMemBlock (CAPWAP_FSCAWAPCONFIGSTATSTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgIsSetFsCawapConfigStatsEntry);
                return OSIX_FAILURE;
            }

            MEMSET (pCapwapFsCawapConfigStatsEntry, 0,
                    sizeof (tCapwapFsCawapConfigStatsEntry));
            if ((CapwapInitializeFsCawapConfigStatsTable
                 (pCapwapFsCawapConfigStatsEntry)) == OSIX_FAILURE)
            {
                if (CapwapSetAllFsCawapConfigStatsTableTrigger
                    (pCapwapSetFsCawapConfigStatsEntry,
                     pCapwapIsSetFsCawapConfigStatsEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCawapConfigStatsTable:CapwapSetAllFsCawapConfigStatsTableTrigger function fails\r\n");

                }
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCawapConfigStatsTable: Fail to Initialize the Objects.\r\n");

                MemReleaseMemBlock (CAPWAP_FSCAWAPCONFIGSTATSTABLE_POOLID,
                                    (UINT1 *) pCapwapFsCawapConfigStatsEntry);
                MemReleaseMemBlock (CAPWAP_FSCAWAPCONFIGSTATSTABLE_POOLID,
                                    (UINT1 *)
                                    pCapwapOldFsCawapConfigStatsEntry);
                MemReleaseMemBlock (CAPWAP_FSCAWAPCONFIGSTATSTABLE_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgFsCawapConfigStatsEntry);
                MemReleaseMemBlock (CAPWAP_FSCAWAPCONFIGSTATSTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgIsSetFsCawapConfigStatsEntry);
                return OSIX_FAILURE;
            }
            /* Assign values for the new node */
            if (pCapwapIsSetFsCawapConfigStatsEntry->
                bFsCapwapConfigStatsRowStatus != OSIX_FALSE)
            {
                pCapwapFsCawapConfigStatsEntry->MibObject.
                    i4FsCapwapConfigStatsRowStatus =
                    pCapwapSetFsCawapConfigStatsEntry->MibObject.
                    i4FsCapwapConfigStatsRowStatus;
            }

            if (pCapwapIsSetFsCawapConfigStatsEntry->bCapwapBaseWtpProfileId !=
                OSIX_FALSE)
            {
                pCapwapFsCawapConfigStatsEntry->MibObject.
                    u4CapwapBaseWtpProfileId =
                    pCapwapSetFsCawapConfigStatsEntry->MibObject.
                    u4CapwapBaseWtpProfileId;
            }

            if ((pCapwapSetFsCawapConfigStatsEntry->MibObject.
                 i4FsCapwapConfigStatsRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pCapwapSetFsCawapConfigStatsEntry->MibObject.
                        i4FsCapwapConfigStatsRowStatus == ACTIVE)))
            {
                pCapwapFsCawapConfigStatsEntry->MibObject.
                    i4FsCapwapConfigStatsRowStatus = ACTIVE;
            }
            else if (pCapwapSetFsCawapConfigStatsEntry->MibObject.
                     i4FsCapwapConfigStatsRowStatus == CREATE_AND_WAIT)
            {
                pCapwapFsCawapConfigStatsEntry->MibObject.
                    i4FsCapwapConfigStatsRowStatus = NOT_READY;
            }

            /* Add the new node to the database */
            if (RBTreeAdd
                (gCapwapGlobals.CapwapGlbMib.FsCawapConfigStatsTable,
                 (tRBElem *) pCapwapFsCawapConfigStatsEntry) != RB_SUCCESS)
            {
                if (CapwapSetAllFsCawapConfigStatsTableTrigger
                    (pCapwapSetFsCawapConfigStatsEntry,
                     pCapwapIsSetFsCawapConfigStatsEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCawapConfigStatsTable: CapwapSetAllFsCawapConfigStatsTableTrigger function returns failure.\r\n");
                }
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCawapConfigStatsTable: RBTreeAdd is failed.\r\n");

                MemReleaseMemBlock (CAPWAP_FSCAWAPCONFIGSTATSTABLE_POOLID,
                                    (UINT1 *) pCapwapFsCawapConfigStatsEntry);
                MemReleaseMemBlock (CAPWAP_FSCAWAPCONFIGSTATSTABLE_POOLID,
                                    (UINT1 *)
                                    pCapwapOldFsCawapConfigStatsEntry);
                MemReleaseMemBlock (CAPWAP_FSCAWAPCONFIGSTATSTABLE_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgFsCawapConfigStatsEntry);
                MemReleaseMemBlock (CAPWAP_FSCAWAPCONFIGSTATSTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgIsSetFsCawapConfigStatsEntry);
                return OSIX_FAILURE;
            }
            if (CapwapUtilUpdateFsCawapConfigStatsTable
                (NULL, pCapwapFsCawapConfigStatsEntry,
                 pCapwapIsSetFsCawapConfigStatsEntry) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCawapConfigStatsTable: CapwapUtilUpdateFsCawapConfigStatsTable function returns failure.\r\n");

                if (CapwapSetAllFsCawapConfigStatsTableTrigger
                    (pCapwapSetFsCawapConfigStatsEntry,
                     pCapwapIsSetFsCawapConfigStatsEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCawapConfigStatsTable: CapwapSetAllFsCawapConfigStatsTableTrigger function returns failure.\r\n");

                }
                RBTreeRem (gCapwapGlobals.CapwapGlbMib.FsCawapConfigStatsTable,
                           pCapwapFsCawapConfigStatsEntry);
                MemReleaseMemBlock (CAPWAP_FSCAWAPCONFIGSTATSTABLE_POOLID,
                                    (UINT1 *) pCapwapFsCawapConfigStatsEntry);
                MemReleaseMemBlock (CAPWAP_FSCAWAPCONFIGSTATSTABLE_POOLID,
                                    (UINT1 *)
                                    pCapwapOldFsCawapConfigStatsEntry);
                MemReleaseMemBlock (CAPWAP_FSCAWAPCONFIGSTATSTABLE_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgFsCawapConfigStatsEntry);
                MemReleaseMemBlock (CAPWAP_FSCAWAPCONFIGSTATSTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgIsSetFsCawapConfigStatsEntry);
                return OSIX_FAILURE;
            }

            if ((pCapwapSetFsCawapConfigStatsEntry->MibObject.
                 i4FsCapwapConfigStatsRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pCapwapSetFsCawapConfigStatsEntry->MibObject.
                        i4FsCapwapConfigStatsRowStatus == ACTIVE)))
            {

                /* For MSR and RM Trigger */
                pCapwapTrgFsCawapConfigStatsEntry->MibObject.
                    u4CapwapBaseWtpProfileId =
                    pCapwapSetFsCawapConfigStatsEntry->MibObject.
                    u4CapwapBaseWtpProfileId;
                pCapwapTrgFsCawapConfigStatsEntry->MibObject.
                    i4FsCapwapConfigStatsRowStatus = CREATE_AND_WAIT;
                pCapwapTrgIsSetFsCawapConfigStatsEntry->
                    bFsCapwapConfigStatsRowStatus = OSIX_TRUE;

                if (CapwapSetAllFsCawapConfigStatsTableTrigger
                    (pCapwapTrgFsCawapConfigStatsEntry,
                     pCapwapTrgIsSetFsCawapConfigStatsEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCawapConfigStatsTable: CapwapSetAllFsCawapConfigStatsTableTrigger function returns failure.\r\n");

                    MemReleaseMemBlock (CAPWAP_FSCAWAPCONFIGSTATSTABLE_POOLID,
                                        (UINT1 *)
                                        pCapwapFsCawapConfigStatsEntry);
                    MemReleaseMemBlock (CAPWAP_FSCAWAPCONFIGSTATSTABLE_POOLID,
                                        (UINT1 *)
                                        pCapwapOldFsCawapConfigStatsEntry);
                    MemReleaseMemBlock (CAPWAP_FSCAWAPCONFIGSTATSTABLE_POOLID,
                                        (UINT1 *)
                                        pCapwapTrgFsCawapConfigStatsEntry);
                    MemReleaseMemBlock
                        (CAPWAP_FSCAWAPCONFIGSTATSTABLE_ISSET_POOLID,
                         (UINT1 *) pCapwapTrgIsSetFsCawapConfigStatsEntry);
                    return OSIX_FAILURE;
                }
            }
            else if (pCapwapSetFsCawapConfigStatsEntry->MibObject.
                     i4FsCapwapConfigStatsRowStatus == CREATE_AND_WAIT)
            {

                /* For MSR and RM Trigger */
                pCapwapTrgFsCawapConfigStatsEntry->MibObject.
                    i4FsCapwapConfigStatsRowStatus = CREATE_AND_WAIT;
                pCapwapTrgIsSetFsCawapConfigStatsEntry->
                    bFsCapwapConfigStatsRowStatus = OSIX_TRUE;

                if (CapwapSetAllFsCawapConfigStatsTableTrigger
                    (pCapwapTrgFsCawapConfigStatsEntry,
                     pCapwapTrgIsSetFsCawapConfigStatsEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCawapConfigStatsTable: CapwapSetAllFsCawapConfigStatsTableTrigger function returns failure.\r\n");

                    MemReleaseMemBlock (CAPWAP_FSCAWAPCONFIGSTATSTABLE_POOLID,
                                        (UINT1 *)
                                        pCapwapFsCawapConfigStatsEntry);
                    MemReleaseMemBlock (CAPWAP_FSCAWAPCONFIGSTATSTABLE_POOLID,
                                        (UINT1 *)
                                        pCapwapOldFsCawapConfigStatsEntry);
                    MemReleaseMemBlock (CAPWAP_FSCAWAPCONFIGSTATSTABLE_POOLID,
                                        (UINT1 *)
                                        pCapwapTrgFsCawapConfigStatsEntry);
                    MemReleaseMemBlock
                        (CAPWAP_FSCAWAPCONFIGSTATSTABLE_ISSET_POOLID,
                         (UINT1 *) pCapwapTrgIsSetFsCawapConfigStatsEntry);
                    return OSIX_FAILURE;
                }
            }

            if (pCapwapSetFsCawapConfigStatsEntry->MibObject.
                i4FsCapwapConfigStatsRowStatus == CREATE_AND_GO)
            {
                pCapwapSetFsCawapConfigStatsEntry->MibObject.
                    i4FsCapwapConfigStatsRowStatus = ACTIVE;
            }

            if (CapwapSetAllFsCawapConfigStatsTableTrigger
                (pCapwapSetFsCawapConfigStatsEntry,
                 pCapwapIsSetFsCawapConfigStatsEntry,
                 SNMP_SUCCESS) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCawapConfigStatsTable:  CapwapSetAllFsCawapConfigStatsTableTrigger function returns failure.\r\n");
            }
            MemReleaseMemBlock (CAPWAP_FSCAWAPCONFIGSTATSTABLE_POOLID,
                                (UINT1 *) pCapwapOldFsCawapConfigStatsEntry);
            MemReleaseMemBlock (CAPWAP_FSCAWAPCONFIGSTATSTABLE_POOLID,
                                (UINT1 *) pCapwapTrgFsCawapConfigStatsEntry);
            MemReleaseMemBlock (CAPWAP_FSCAWAPCONFIGSTATSTABLE_ISSET_POOLID,
                                (UINT1 *)
                                pCapwapTrgIsSetFsCawapConfigStatsEntry);
            return OSIX_SUCCESS;

        }
        else
        {
            if (CapwapSetAllFsCawapConfigStatsTableTrigger
                (pCapwapSetFsCawapConfigStatsEntry,
                 pCapwapIsSetFsCawapConfigStatsEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCawapConfigStatsTable: CapwapSetAllFsCawapConfigStatsTableTrigger function returns failure.\r\n");
            }
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCawapConfigStatsTable: Failure.\r\n");
            MemReleaseMemBlock (CAPWAP_FSCAWAPCONFIGSTATSTABLE_POOLID,
                                (UINT1 *) pCapwapOldFsCawapConfigStatsEntry);
            MemReleaseMemBlock (CAPWAP_FSCAWAPCONFIGSTATSTABLE_POOLID,
                                (UINT1 *) pCapwapTrgFsCawapConfigStatsEntry);
            MemReleaseMemBlock (CAPWAP_FSCAWAPCONFIGSTATSTABLE_ISSET_POOLID,
                                (UINT1 *)
                                pCapwapTrgIsSetFsCawapConfigStatsEntry);
            return OSIX_FAILURE;
        }
    }
    else if ((pCapwapSetFsCawapConfigStatsEntry->MibObject.
              i4FsCapwapConfigStatsRowStatus == CREATE_AND_WAIT)
             || (pCapwapSetFsCawapConfigStatsEntry->MibObject.
                 i4FsCapwapConfigStatsRowStatus == CREATE_AND_GO))
    {
        if (CapwapSetAllFsCawapConfigStatsTableTrigger
            (pCapwapSetFsCawapConfigStatsEntry,
             pCapwapIsSetFsCawapConfigStatsEntry, SNMP_FAILURE) != OSIX_SUCCESS)

        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCawapConfigStatsTable: CapwapSetAllFsCawapConfigStatsTableTrigger function returns failure.\r\n");
        }
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapSetAllFsCawapConfigStatsTable: The row is already present.\r\n");
        MemReleaseMemBlock (CAPWAP_FSCAWAPCONFIGSTATSTABLE_POOLID,
                            (UINT1 *) pCapwapOldFsCawapConfigStatsEntry);
        MemReleaseMemBlock (CAPWAP_FSCAWAPCONFIGSTATSTABLE_POOLID,
                            (UINT1 *) pCapwapTrgFsCawapConfigStatsEntry);
        MemReleaseMemBlock (CAPWAP_FSCAWAPCONFIGSTATSTABLE_ISSET_POOLID,
                            (UINT1 *) pCapwapTrgIsSetFsCawapConfigStatsEntry);
        return OSIX_FAILURE;
    }
    /* Copy the previous values before setting the new values */
    MEMCPY (pCapwapOldFsCawapConfigStatsEntry, pCapwapFsCawapConfigStatsEntry,
            sizeof (tCapwapFsCawapConfigStatsEntry));

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pCapwapSetFsCawapConfigStatsEntry->MibObject.
        i4FsCapwapConfigStatsRowStatus == DESTROY)
    {
        pCapwapFsCawapConfigStatsEntry->MibObject.
            i4FsCapwapConfigStatsRowStatus = DESTROY;

        if (CapwapUtilUpdateFsCawapConfigStatsTable
            (pCapwapOldFsCawapConfigStatsEntry, pCapwapFsCawapConfigStatsEntry,
             pCapwapIsSetFsCawapConfigStatsEntry) != OSIX_SUCCESS)
        {

            if (CapwapSetAllFsCawapConfigStatsTableTrigger
                (pCapwapSetFsCawapConfigStatsEntry,
                 pCapwapIsSetFsCawapConfigStatsEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCawapConfigStatsTable: CapwapSetAllFsCawapConfigStatsTableTrigger function returns failure.\r\n");
            }
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCawapConfigStatsTable: CapwapUtilUpdateFsCawapConfigStatsTable function returns failure.\r\n");
        }
        RBTreeRem (gCapwapGlobals.CapwapGlbMib.FsCawapConfigStatsTable,
                   pCapwapFsCawapConfigStatsEntry);
        if (CapwapSetAllFsCawapConfigStatsTableTrigger
            (pCapwapSetFsCawapConfigStatsEntry,
             pCapwapIsSetFsCawapConfigStatsEntry, SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCawapConfigStatsTable: CapwapSetAllFsCawapConfigStatsTableTrigger function returns failure.\r\n");
            MemReleaseMemBlock (CAPWAP_FSCAWAPCONFIGSTATSTABLE_POOLID,
                                (UINT1 *) pCapwapOldFsCawapConfigStatsEntry);
            MemReleaseMemBlock (CAPWAP_FSCAWAPCONFIGSTATSTABLE_POOLID,
                                (UINT1 *) pCapwapTrgFsCawapConfigStatsEntry);
            MemReleaseMemBlock (CAPWAP_FSCAWAPCONFIGSTATSTABLE_ISSET_POOLID,
                                (UINT1 *)
                                pCapwapTrgIsSetFsCawapConfigStatsEntry);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (CAPWAP_FSCAWAPCONFIGSTATSTABLE_POOLID,
                            (UINT1 *) pCapwapFsCawapConfigStatsEntry);
        MemReleaseMemBlock (CAPWAP_FSCAWAPCONFIGSTATSTABLE_POOLID,
                            (UINT1 *) pCapwapOldFsCawapConfigStatsEntry);
        MemReleaseMemBlock (CAPWAP_FSCAWAPCONFIGSTATSTABLE_POOLID,
                            (UINT1 *) pCapwapTrgFsCawapConfigStatsEntry);
        MemReleaseMemBlock (CAPWAP_FSCAWAPCONFIGSTATSTABLE_ISSET_POOLID,
                            (UINT1 *) pCapwapTrgIsSetFsCawapConfigStatsEntry);
        return OSIX_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (FsCawapConfigStatsTableFilterInputs
        (pCapwapFsCawapConfigStatsEntry, pCapwapSetFsCawapConfigStatsEntry,
         pCapwapIsSetFsCawapConfigStatsEntry) != OSIX_TRUE)
    {
        MemReleaseMemBlock (CAPWAP_FSCAWAPCONFIGSTATSTABLE_POOLID,
                            (UINT1 *) pCapwapOldFsCawapConfigStatsEntry);
        MemReleaseMemBlock (CAPWAP_FSCAWAPCONFIGSTATSTABLE_POOLID,
                            (UINT1 *) pCapwapTrgFsCawapConfigStatsEntry);
        MemReleaseMemBlock (CAPWAP_FSCAWAPCONFIGSTATSTABLE_ISSET_POOLID,
                            (UINT1 *) pCapwapTrgIsSetFsCawapConfigStatsEntry);
        return OSIX_SUCCESS;
    }

    /*This condtion is to make the row NOT_IN_SERVICE before modifying the values */
    if ((i4RowStatusLogic == TRUE) &&
        (pCapwapFsCawapConfigStatsEntry->MibObject.
         i4FsCapwapConfigStatsRowStatus == ACTIVE)
        && (pCapwapSetFsCawapConfigStatsEntry->MibObject.
            i4FsCapwapConfigStatsRowStatus != NOT_IN_SERVICE))
    {
        pCapwapFsCawapConfigStatsEntry->MibObject.
            i4FsCapwapConfigStatsRowStatus = NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        pCapwapTrgFsCawapConfigStatsEntry->MibObject.
            i4FsCapwapConfigStatsRowStatus = NOT_IN_SERVICE;
        pCapwapTrgIsSetFsCawapConfigStatsEntry->bFsCapwapConfigStatsRowStatus =
            OSIX_TRUE;

        if (CapwapUtilUpdateFsCawapConfigStatsTable
            (pCapwapOldFsCawapConfigStatsEntry, pCapwapFsCawapConfigStatsEntry,
             pCapwapIsSetFsCawapConfigStatsEntry) != OSIX_SUCCESS)
        {
            /*Restore back with previous values */
            MEMCPY (pCapwapFsCawapConfigStatsEntry,
                    pCapwapOldFsCawapConfigStatsEntry,
                    sizeof (tCapwapFsCawapConfigStatsEntry));
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCawapConfigStatsTable:                 CapwapUtilUpdateFsCawapConfigStatsTable Function returns failure.\r\n");

            if (CapwapSetAllFsCawapConfigStatsTableTrigger
                (pCapwapSetFsCawapConfigStatsEntry,
                 pCapwapIsSetFsCawapConfigStatsEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCawapConfigStatsTable: CapwapSetAllFsCawapConfigStatsTableTrigger function returns failure.\r\n");
            }
            MemReleaseMemBlock (CAPWAP_FSCAWAPCONFIGSTATSTABLE_POOLID,
                                (UINT1 *) pCapwapOldFsCawapConfigStatsEntry);
            MemReleaseMemBlock (CAPWAP_FSCAWAPCONFIGSTATSTABLE_POOLID,
                                (UINT1 *) pCapwapTrgFsCawapConfigStatsEntry);
            MemReleaseMemBlock (CAPWAP_FSCAWAPCONFIGSTATSTABLE_ISSET_POOLID,
                                (UINT1 *)
                                pCapwapTrgIsSetFsCawapConfigStatsEntry);
            return OSIX_FAILURE;
        }

        if (CapwapSetAllFsCawapConfigStatsTableTrigger
            (pCapwapTrgFsCawapConfigStatsEntry,
             pCapwapTrgIsSetFsCawapConfigStatsEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCawapConfigStatsTable: CapwapSetAllFsCawapConfigStatsTableTrigger function returns failure.\r\n");
        }
    }

    if (pCapwapSetFsCawapConfigStatsEntry->MibObject.
        i4FsCapwapConfigStatsRowStatus == ACTIVE)
    {
        i4RowMakeActive = TRUE;
    }

    /* Assign values for the existing node */
    if (pCapwapIsSetFsCawapConfigStatsEntry->bFsCapwapConfigStatsRowStatus !=
        OSIX_FALSE)
    {
        pCapwapFsCawapConfigStatsEntry->MibObject.
            i4FsCapwapConfigStatsRowStatus =
            pCapwapSetFsCawapConfigStatsEntry->MibObject.
            i4FsCapwapConfigStatsRowStatus;
    }

    /*This condtion is to make the row back to ACTIVE after modifying the values */
    if (i4RowMakeActive == TRUE)
    {
        pCapwapFsCawapConfigStatsEntry->MibObject.
            i4FsCapwapConfigStatsRowStatus = ACTIVE;
    }

    if (CapwapUtilUpdateFsCawapConfigStatsTable
        (pCapwapOldFsCawapConfigStatsEntry, pCapwapFsCawapConfigStatsEntry,
         pCapwapIsSetFsCawapConfigStatsEntry) != OSIX_SUCCESS)
    {

        if (CapwapSetAllFsCawapConfigStatsTableTrigger
            (pCapwapSetFsCawapConfigStatsEntry,
             pCapwapIsSetFsCawapConfigStatsEntry, SNMP_FAILURE) != OSIX_SUCCESS)

        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCawapConfigStatsTable: CapwapSetAllFsCawapConfigStatsTableTrigger function returns failure.\r\n");

        }
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapSetAllFsCawapConfigStatsTable: CapwapUtilUpdateFsCawapConfigStatsTable function returns failure.\r\n");
        /*Restore back with previous values */
        MEMCPY (pCapwapFsCawapConfigStatsEntry,
                pCapwapOldFsCawapConfigStatsEntry,
                sizeof (tCapwapFsCawapConfigStatsEntry));
        MemReleaseMemBlock (CAPWAP_FSCAWAPCONFIGSTATSTABLE_POOLID,
                            (UINT1 *) pCapwapOldFsCawapConfigStatsEntry);
        MemReleaseMemBlock (CAPWAP_FSCAWAPCONFIGSTATSTABLE_POOLID,
                            (UINT1 *) pCapwapTrgFsCawapConfigStatsEntry);
        MemReleaseMemBlock (CAPWAP_FSCAWAPCONFIGSTATSTABLE_ISSET_POOLID,
                            (UINT1 *) pCapwapTrgIsSetFsCawapConfigStatsEntry);
        return OSIX_FAILURE;

    }
    if (CapwapSetAllFsCawapConfigStatsTableTrigger
        (pCapwapSetFsCawapConfigStatsEntry, pCapwapIsSetFsCawapConfigStatsEntry,
         SNMP_SUCCESS) != OSIX_SUCCESS)
    {

        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapSetAllFsCawapConfigStatsTable: CapwapSetAllFsCawapConfigStatsTableTrigger function returns failure.\r\n");
    }

    MemReleaseMemBlock (CAPWAP_FSCAWAPCONFIGSTATSTABLE_POOLID,
                        (UINT1 *) pCapwapOldFsCawapConfigStatsEntry);
    MemReleaseMemBlock (CAPWAP_FSCAWAPCONFIGSTATSTABLE_POOLID,
                        (UINT1 *) pCapwapTrgFsCawapConfigStatsEntry);
    MemReleaseMemBlock (CAPWAP_FSCAWAPCONFIGSTATSTABLE_ISSET_POOLID,
                        (UINT1 *) pCapwapTrgIsSetFsCawapConfigStatsEntry);
    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  CapwapSetAllFsCawapRunStatsTable
 Input       :  pCapwapSetFsCawapRunStatsEntry
                pCapwapIsSetFsCawapRunStatsEntry
                i4RowStatusLogic
                i4RowCreateOption
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapSetAllFsCawapRunStatsTable (tCapwapFsCawapRunStatsEntry *
                                  pCapwapSetFsCawapRunStatsEntry,
                                  tCapwapIsSetFsCawapRunStatsEntry *
                                  pCapwapIsSetFsCawapRunStatsEntry,
                                  INT4 i4RowStatusLogic, INT4 i4RowCreateOption)
{
    tCapwapFsCawapRunStatsEntry *pCapwapFsCawapRunStatsEntry = NULL;
    tCapwapFsCawapRunStatsEntry *pCapwapOldFsCawapRunStatsEntry = NULL;
    tCapwapFsCawapRunStatsEntry *pCapwapTrgFsCawapRunStatsEntry = NULL;
    tCapwapIsSetFsCawapRunStatsEntry *pCapwapTrgIsSetFsCawapRunStatsEntry =
        NULL;
    INT4                i4RowMakeActive = FALSE;

    pCapwapOldFsCawapRunStatsEntry =
        (tCapwapFsCawapRunStatsEntry *)
        MemAllocMemBlk (CAPWAP_FSCAWAPRUNSTATSTABLE_POOLID);
    if (pCapwapOldFsCawapRunStatsEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    pCapwapTrgFsCawapRunStatsEntry =
        (tCapwapFsCawapRunStatsEntry *)
        MemAllocMemBlk (CAPWAP_FSCAWAPRUNSTATSTABLE_POOLID);
    if (pCapwapTrgFsCawapRunStatsEntry == NULL)
    {
        MemReleaseMemBlock (CAPWAP_FSCAWAPRUNSTATSTABLE_POOLID,
                            (UINT1 *) pCapwapOldFsCawapRunStatsEntry);
        return OSIX_FAILURE;
    }
    pCapwapTrgIsSetFsCawapRunStatsEntry =
        (tCapwapIsSetFsCawapRunStatsEntry *)
        MemAllocMemBlk (CAPWAP_FSCAWAPRUNSTATSTABLE_ISSET_POOLID);
    if (pCapwapTrgIsSetFsCawapRunStatsEntry == NULL)
    {
        MemReleaseMemBlock (CAPWAP_FSCAWAPRUNSTATSTABLE_POOLID,
                            (UINT1 *) pCapwapOldFsCawapRunStatsEntry);
        MemReleaseMemBlock (CAPWAP_FSCAWAPRUNSTATSTABLE_POOLID,
                            (UINT1 *) pCapwapTrgFsCawapRunStatsEntry);
        return OSIX_FAILURE;
    }
    MEMSET (pCapwapOldFsCawapRunStatsEntry, 0,
            sizeof (tCapwapFsCawapRunStatsEntry));
    MEMSET (pCapwapTrgFsCawapRunStatsEntry, 0,
            sizeof (tCapwapFsCawapRunStatsEntry));
    MEMSET (pCapwapTrgIsSetFsCawapRunStatsEntry, 0,
            sizeof (tCapwapIsSetFsCawapRunStatsEntry));

    /* Check whether the node is already present */
    pCapwapFsCawapRunStatsEntry =
        RBTreeGet (gCapwapGlobals.CapwapGlbMib.FsCawapRunStatsTable,
                   (tRBElem *) pCapwapSetFsCawapRunStatsEntry);

    if (pCapwapFsCawapRunStatsEntry == NULL)
    {
        /* Create the node if the RowStatus given is CREATE_AND_WAIT or CREATE_AND_GO */
        if ((pCapwapSetFsCawapRunStatsEntry->MibObject.
             i4FsCapwapRunStatsRowStatus == CREATE_AND_WAIT)
            || (pCapwapSetFsCawapRunStatsEntry->MibObject.
                i4FsCapwapRunStatsRowStatus == CREATE_AND_GO)
            ||
            ((pCapwapSetFsCawapRunStatsEntry->MibObject.
              i4FsCapwapRunStatsRowStatus == ACTIVE)
             && (i4RowCreateOption == 1)))
        {
            /* Allocate memory for the new node */
            pCapwapFsCawapRunStatsEntry =
                (tCapwapFsCawapRunStatsEntry *)
                MemAllocMemBlk (CAPWAP_FSCAWAPRUNSTATSTABLE_POOLID);
            if (pCapwapFsCawapRunStatsEntry == NULL)
            {
                if (CapwapSetAllFsCawapRunStatsTableTrigger
                    (pCapwapSetFsCawapRunStatsEntry,
                     pCapwapIsSetFsCawapRunStatsEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCawapRunStatsTable:CapwapSetAllFsCawapRunStatsTableTrigger function fails\r\n");

                }
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCawapRunStatsTable: Fail to Allocate Memory.\r\n");
                MemReleaseMemBlock (CAPWAP_FSCAWAPRUNSTATSTABLE_POOLID,
                                    (UINT1 *) pCapwapOldFsCawapRunStatsEntry);
                MemReleaseMemBlock (CAPWAP_FSCAWAPRUNSTATSTABLE_POOLID,
                                    (UINT1 *) pCapwapTrgFsCawapRunStatsEntry);
                MemReleaseMemBlock (CAPWAP_FSCAWAPRUNSTATSTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgIsSetFsCawapRunStatsEntry);
                return OSIX_FAILURE;
            }

            MEMSET (pCapwapFsCawapRunStatsEntry, 0,
                    sizeof (tCapwapFsCawapRunStatsEntry));
            if ((CapwapInitializeFsCawapRunStatsTable
                 (pCapwapFsCawapRunStatsEntry)) == OSIX_FAILURE)
            {
                if (CapwapSetAllFsCawapRunStatsTableTrigger
                    (pCapwapSetFsCawapRunStatsEntry,
                     pCapwapIsSetFsCawapRunStatsEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCawapRunStatsTable:CapwapSetAllFsCawapRunStatsTableTrigger function fails\r\n");

                }
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCawapRunStatsTable: Fail to Initialize the Objects.\r\n");

                MemReleaseMemBlock (CAPWAP_FSCAWAPRUNSTATSTABLE_POOLID,
                                    (UINT1 *) pCapwapFsCawapRunStatsEntry);
                MemReleaseMemBlock (CAPWAP_FSCAWAPRUNSTATSTABLE_POOLID,
                                    (UINT1 *) pCapwapOldFsCawapRunStatsEntry);
                MemReleaseMemBlock (CAPWAP_FSCAWAPRUNSTATSTABLE_POOLID,
                                    (UINT1 *) pCapwapTrgFsCawapRunStatsEntry);
                MemReleaseMemBlock (CAPWAP_FSCAWAPRUNSTATSTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgIsSetFsCawapRunStatsEntry);
                return OSIX_FAILURE;
            }
            /* Assign values for the new node */
            if (pCapwapIsSetFsCawapRunStatsEntry->bFsCapwapRunStatsRowStatus !=
                OSIX_FALSE)
            {
                pCapwapFsCawapRunStatsEntry->MibObject.
                    i4FsCapwapRunStatsRowStatus =
                    pCapwapSetFsCawapRunStatsEntry->MibObject.
                    i4FsCapwapRunStatsRowStatus;
            }

            if (pCapwapIsSetFsCawapRunStatsEntry->bCapwapBaseWtpProfileId !=
                OSIX_FALSE)
            {
                pCapwapFsCawapRunStatsEntry->MibObject.
                    u4CapwapBaseWtpProfileId =
                    pCapwapSetFsCawapRunStatsEntry->MibObject.
                    u4CapwapBaseWtpProfileId;
            }

            if ((pCapwapSetFsCawapRunStatsEntry->MibObject.
                 i4FsCapwapRunStatsRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pCapwapSetFsCawapRunStatsEntry->MibObject.
                        i4FsCapwapRunStatsRowStatus == ACTIVE)))
            {
                pCapwapFsCawapRunStatsEntry->MibObject.
                    i4FsCapwapRunStatsRowStatus = ACTIVE;
            }
            else if (pCapwapSetFsCawapRunStatsEntry->MibObject.
                     i4FsCapwapRunStatsRowStatus == CREATE_AND_WAIT)
            {
                pCapwapFsCawapRunStatsEntry->MibObject.
                    i4FsCapwapRunStatsRowStatus = NOT_READY;
            }

            /* Add the new node to the database */
            if (RBTreeAdd
                (gCapwapGlobals.CapwapGlbMib.FsCawapRunStatsTable,
                 (tRBElem *) pCapwapFsCawapRunStatsEntry) != RB_SUCCESS)
            {
                if (CapwapSetAllFsCawapRunStatsTableTrigger
                    (pCapwapSetFsCawapRunStatsEntry,
                     pCapwapIsSetFsCawapRunStatsEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCawapRunStatsTable: CapwapSetAllFsCawapRunStatsTableTrigger function returns failure.\r\n");
                }
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCawapRunStatsTable: RBTreeAdd is failed.\r\n");

                MemReleaseMemBlock (CAPWAP_FSCAWAPRUNSTATSTABLE_POOLID,
                                    (UINT1 *) pCapwapFsCawapRunStatsEntry);
                MemReleaseMemBlock (CAPWAP_FSCAWAPRUNSTATSTABLE_POOLID,
                                    (UINT1 *) pCapwapOldFsCawapRunStatsEntry);
                MemReleaseMemBlock (CAPWAP_FSCAWAPRUNSTATSTABLE_POOLID,
                                    (UINT1 *) pCapwapTrgFsCawapRunStatsEntry);
                MemReleaseMemBlock (CAPWAP_FSCAWAPRUNSTATSTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgIsSetFsCawapRunStatsEntry);
                return OSIX_FAILURE;
            }
            if (CapwapUtilUpdateFsCawapRunStatsTable
                (NULL, pCapwapFsCawapRunStatsEntry,
                 pCapwapIsSetFsCawapRunStatsEntry) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCawapRunStatsTable: CapwapUtilUpdateFsCawapRunStatsTable function returns failure.\r\n");

                if (CapwapSetAllFsCawapRunStatsTableTrigger
                    (pCapwapSetFsCawapRunStatsEntry,
                     pCapwapIsSetFsCawapRunStatsEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCawapRunStatsTable: CapwapSetAllFsCawapRunStatsTableTrigger function returns failure.\r\n");

                }
                RBTreeRem (gCapwapGlobals.CapwapGlbMib.FsCawapRunStatsTable,
                           pCapwapFsCawapRunStatsEntry);
                MemReleaseMemBlock (CAPWAP_FSCAWAPRUNSTATSTABLE_POOLID,
                                    (UINT1 *) pCapwapFsCawapRunStatsEntry);
                MemReleaseMemBlock (CAPWAP_FSCAWAPRUNSTATSTABLE_POOLID,
                                    (UINT1 *) pCapwapOldFsCawapRunStatsEntry);
                MemReleaseMemBlock (CAPWAP_FSCAWAPRUNSTATSTABLE_POOLID,
                                    (UINT1 *) pCapwapTrgFsCawapRunStatsEntry);
                MemReleaseMemBlock (CAPWAP_FSCAWAPRUNSTATSTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgIsSetFsCawapRunStatsEntry);
                return OSIX_FAILURE;
            }

            if ((pCapwapSetFsCawapRunStatsEntry->MibObject.
                 i4FsCapwapRunStatsRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pCapwapSetFsCawapRunStatsEntry->MibObject.
                        i4FsCapwapRunStatsRowStatus == ACTIVE)))
            {

                /* For MSR and RM Trigger */
                pCapwapTrgFsCawapRunStatsEntry->MibObject.
                    u4CapwapBaseWtpProfileId =
                    pCapwapSetFsCawapRunStatsEntry->MibObject.
                    u4CapwapBaseWtpProfileId;
                pCapwapTrgFsCawapRunStatsEntry->MibObject.
                    i4FsCapwapRunStatsRowStatus = CREATE_AND_WAIT;
                pCapwapTrgIsSetFsCawapRunStatsEntry->
                    bFsCapwapRunStatsRowStatus = OSIX_TRUE;

                if (CapwapSetAllFsCawapRunStatsTableTrigger
                    (pCapwapTrgFsCawapRunStatsEntry,
                     pCapwapTrgIsSetFsCawapRunStatsEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCawapRunStatsTable: CapwapSetAllFsCawapRunStatsTableTrigger function returns failure.\r\n");

                    MemReleaseMemBlock (CAPWAP_FSCAWAPRUNSTATSTABLE_POOLID,
                                        (UINT1 *) pCapwapFsCawapRunStatsEntry);
                    MemReleaseMemBlock (CAPWAP_FSCAWAPRUNSTATSTABLE_POOLID,
                                        (UINT1 *)
                                        pCapwapOldFsCawapRunStatsEntry);
                    MemReleaseMemBlock (CAPWAP_FSCAWAPRUNSTATSTABLE_POOLID,
                                        (UINT1 *)
                                        pCapwapTrgFsCawapRunStatsEntry);
                    MemReleaseMemBlock
                        (CAPWAP_FSCAWAPRUNSTATSTABLE_ISSET_POOLID,
                         (UINT1 *) pCapwapTrgIsSetFsCawapRunStatsEntry);
                    return OSIX_FAILURE;
                }
            }
            else if (pCapwapSetFsCawapRunStatsEntry->MibObject.
                     i4FsCapwapRunStatsRowStatus == CREATE_AND_WAIT)
            {

                /* For MSR and RM Trigger */
                pCapwapTrgFsCawapRunStatsEntry->MibObject.
                    i4FsCapwapRunStatsRowStatus = CREATE_AND_WAIT;
                pCapwapTrgIsSetFsCawapRunStatsEntry->
                    bFsCapwapRunStatsRowStatus = OSIX_TRUE;

                if (CapwapSetAllFsCawapRunStatsTableTrigger
                    (pCapwapTrgFsCawapRunStatsEntry,
                     pCapwapTrgIsSetFsCawapRunStatsEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCawapRunStatsTable: CapwapSetAllFsCawapRunStatsTableTrigger function returns failure.\r\n");

                    MemReleaseMemBlock (CAPWAP_FSCAWAPRUNSTATSTABLE_POOLID,
                                        (UINT1 *) pCapwapFsCawapRunStatsEntry);
                    MemReleaseMemBlock (CAPWAP_FSCAWAPRUNSTATSTABLE_POOLID,
                                        (UINT1 *)
                                        pCapwapOldFsCawapRunStatsEntry);
                    MemReleaseMemBlock (CAPWAP_FSCAWAPRUNSTATSTABLE_POOLID,
                                        (UINT1 *)
                                        pCapwapTrgFsCawapRunStatsEntry);
                    MemReleaseMemBlock
                        (CAPWAP_FSCAWAPRUNSTATSTABLE_ISSET_POOLID,
                         (UINT1 *) pCapwapTrgIsSetFsCawapRunStatsEntry);
                    return OSIX_FAILURE;
                }
            }

            if (pCapwapSetFsCawapRunStatsEntry->MibObject.
                i4FsCapwapRunStatsRowStatus == CREATE_AND_GO)
            {
                pCapwapSetFsCawapRunStatsEntry->MibObject.
                    i4FsCapwapRunStatsRowStatus = ACTIVE;
            }

            if (CapwapSetAllFsCawapRunStatsTableTrigger
                (pCapwapSetFsCawapRunStatsEntry,
                 pCapwapIsSetFsCawapRunStatsEntry,
                 SNMP_SUCCESS) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCawapRunStatsTable:  CapwapSetAllFsCawapRunStatsTableTrigger function returns failure.\r\n");
            }
            MemReleaseMemBlock (CAPWAP_FSCAWAPRUNSTATSTABLE_POOLID,
                                (UINT1 *) pCapwapOldFsCawapRunStatsEntry);
            MemReleaseMemBlock (CAPWAP_FSCAWAPRUNSTATSTABLE_POOLID,
                                (UINT1 *) pCapwapTrgFsCawapRunStatsEntry);
            MemReleaseMemBlock (CAPWAP_FSCAWAPRUNSTATSTABLE_ISSET_POOLID,
                                (UINT1 *) pCapwapTrgIsSetFsCawapRunStatsEntry);
            return OSIX_SUCCESS;

        }
        else
        {
            if (CapwapSetAllFsCawapRunStatsTableTrigger
                (pCapwapSetFsCawapRunStatsEntry,
                 pCapwapIsSetFsCawapRunStatsEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCawapRunStatsTable: CapwapSetAllFsCawapRunStatsTableTrigger function returns failure.\r\n");
            }
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCawapRunStatsTable: Failure.\r\n");
            MemReleaseMemBlock (CAPWAP_FSCAWAPRUNSTATSTABLE_POOLID,
                                (UINT1 *) pCapwapOldFsCawapRunStatsEntry);
            MemReleaseMemBlock (CAPWAP_FSCAWAPRUNSTATSTABLE_POOLID,
                                (UINT1 *) pCapwapTrgFsCawapRunStatsEntry);
            MemReleaseMemBlock (CAPWAP_FSCAWAPRUNSTATSTABLE_ISSET_POOLID,
                                (UINT1 *) pCapwapTrgIsSetFsCawapRunStatsEntry);
            return OSIX_FAILURE;
        }
    }
    else if ((pCapwapSetFsCawapRunStatsEntry->MibObject.
              i4FsCapwapRunStatsRowStatus == CREATE_AND_WAIT)
             || (pCapwapSetFsCawapRunStatsEntry->MibObject.
                 i4FsCapwapRunStatsRowStatus == CREATE_AND_GO))
    {
        if (CapwapSetAllFsCawapRunStatsTableTrigger
            (pCapwapSetFsCawapRunStatsEntry, pCapwapIsSetFsCawapRunStatsEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)

        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCawapRunStatsTable: CapwapSetAllFsCawapRunStatsTableTrigger function returns failure.\r\n");
        }
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapSetAllFsCawapRunStatsTable: The row is already present.\r\n");
        MemReleaseMemBlock (CAPWAP_FSCAWAPRUNSTATSTABLE_POOLID,
                            (UINT1 *) pCapwapOldFsCawapRunStatsEntry);
        MemReleaseMemBlock (CAPWAP_FSCAWAPRUNSTATSTABLE_POOLID,
                            (UINT1 *) pCapwapTrgFsCawapRunStatsEntry);
        MemReleaseMemBlock (CAPWAP_FSCAWAPRUNSTATSTABLE_ISSET_POOLID,
                            (UINT1 *) pCapwapTrgIsSetFsCawapRunStatsEntry);
        return OSIX_FAILURE;
    }
    /* Copy the previous values before setting the new values */
    MEMCPY (pCapwapOldFsCawapRunStatsEntry, pCapwapFsCawapRunStatsEntry,
            sizeof (tCapwapFsCawapRunStatsEntry));

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pCapwapSetFsCawapRunStatsEntry->MibObject.i4FsCapwapRunStatsRowStatus ==
        DESTROY)
    {
        pCapwapFsCawapRunStatsEntry->MibObject.i4FsCapwapRunStatsRowStatus =
            DESTROY;

        if (CapwapUtilUpdateFsCawapRunStatsTable
            (pCapwapOldFsCawapRunStatsEntry, pCapwapFsCawapRunStatsEntry,
             pCapwapIsSetFsCawapRunStatsEntry) != OSIX_SUCCESS)
        {

            if (CapwapSetAllFsCawapRunStatsTableTrigger
                (pCapwapSetFsCawapRunStatsEntry,
                 pCapwapIsSetFsCawapRunStatsEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCawapRunStatsTable: CapwapSetAllFsCawapRunStatsTableTrigger function returns failure.\r\n");
            }
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCawapRunStatsTable: CapwapUtilUpdateFsCawapRunStatsTable function returns failure.\r\n");
        }
        RBTreeRem (gCapwapGlobals.CapwapGlbMib.FsCawapRunStatsTable,
                   pCapwapFsCawapRunStatsEntry);
        if (CapwapSetAllFsCawapRunStatsTableTrigger
            (pCapwapSetFsCawapRunStatsEntry, pCapwapIsSetFsCawapRunStatsEntry,
             SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCawapRunStatsTable: CapwapSetAllFsCawapRunStatsTableTrigger function returns failure.\r\n");
            MemReleaseMemBlock (CAPWAP_FSCAWAPRUNSTATSTABLE_POOLID,
                                (UINT1 *) pCapwapOldFsCawapRunStatsEntry);
            MemReleaseMemBlock (CAPWAP_FSCAWAPRUNSTATSTABLE_POOLID,
                                (UINT1 *) pCapwapTrgFsCawapRunStatsEntry);
            MemReleaseMemBlock (CAPWAP_FSCAWAPRUNSTATSTABLE_ISSET_POOLID,
                                (UINT1 *) pCapwapTrgIsSetFsCawapRunStatsEntry);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (CAPWAP_FSCAWAPRUNSTATSTABLE_POOLID,
                            (UINT1 *) pCapwapFsCawapRunStatsEntry);
        MemReleaseMemBlock (CAPWAP_FSCAWAPRUNSTATSTABLE_POOLID,
                            (UINT1 *) pCapwapOldFsCawapRunStatsEntry);
        MemReleaseMemBlock (CAPWAP_FSCAWAPRUNSTATSTABLE_POOLID,
                            (UINT1 *) pCapwapTrgFsCawapRunStatsEntry);
        MemReleaseMemBlock (CAPWAP_FSCAWAPRUNSTATSTABLE_ISSET_POOLID,
                            (UINT1 *) pCapwapTrgIsSetFsCawapRunStatsEntry);
        return OSIX_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (FsCawapRunStatsTableFilterInputs
        (pCapwapFsCawapRunStatsEntry, pCapwapSetFsCawapRunStatsEntry,
         pCapwapIsSetFsCawapRunStatsEntry) != OSIX_TRUE)
    {
        MemReleaseMemBlock (CAPWAP_FSCAWAPRUNSTATSTABLE_POOLID,
                            (UINT1 *) pCapwapOldFsCawapRunStatsEntry);
        MemReleaseMemBlock (CAPWAP_FSCAWAPRUNSTATSTABLE_POOLID,
                            (UINT1 *) pCapwapTrgFsCawapRunStatsEntry);
        MemReleaseMemBlock (CAPWAP_FSCAWAPRUNSTATSTABLE_ISSET_POOLID,
                            (UINT1 *) pCapwapTrgIsSetFsCawapRunStatsEntry);
        return OSIX_SUCCESS;
    }

    /*This condtion is to make the row NOT_IN_SERVICE before modifying the values */
    if ((i4RowStatusLogic == TRUE) &&
        (pCapwapFsCawapRunStatsEntry->MibObject.i4FsCapwapRunStatsRowStatus ==
         ACTIVE)
        && (pCapwapSetFsCawapRunStatsEntry->MibObject.
            i4FsCapwapRunStatsRowStatus != NOT_IN_SERVICE))
    {
        pCapwapFsCawapRunStatsEntry->MibObject.i4FsCapwapRunStatsRowStatus =
            NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        pCapwapTrgFsCawapRunStatsEntry->MibObject.i4FsCapwapRunStatsRowStatus =
            NOT_IN_SERVICE;
        pCapwapTrgIsSetFsCawapRunStatsEntry->bFsCapwapRunStatsRowStatus =
            OSIX_TRUE;

        if (CapwapUtilUpdateFsCawapRunStatsTable
            (pCapwapOldFsCawapRunStatsEntry, pCapwapFsCawapRunStatsEntry,
             pCapwapIsSetFsCawapRunStatsEntry) != OSIX_SUCCESS)
        {
            /*Restore back with previous values */
            MEMCPY (pCapwapFsCawapRunStatsEntry, pCapwapOldFsCawapRunStatsEntry,
                    sizeof (tCapwapFsCawapRunStatsEntry));
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCawapRunStatsTable:                 CapwapUtilUpdateFsCawapRunStatsTable Function returns failure.\r\n");

            if (CapwapSetAllFsCawapRunStatsTableTrigger
                (pCapwapSetFsCawapRunStatsEntry,
                 pCapwapIsSetFsCawapRunStatsEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCawapRunStatsTable: CapwapSetAllFsCawapRunStatsTableTrigger function returns failure.\r\n");
            }
            MemReleaseMemBlock (CAPWAP_FSCAWAPRUNSTATSTABLE_POOLID,
                                (UINT1 *) pCapwapOldFsCawapRunStatsEntry);
            MemReleaseMemBlock (CAPWAP_FSCAWAPRUNSTATSTABLE_POOLID,
                                (UINT1 *) pCapwapTrgFsCawapRunStatsEntry);
            MemReleaseMemBlock (CAPWAP_FSCAWAPRUNSTATSTABLE_ISSET_POOLID,
                                (UINT1 *) pCapwapTrgIsSetFsCawapRunStatsEntry);
            return OSIX_FAILURE;
        }

        if (CapwapSetAllFsCawapRunStatsTableTrigger
            (pCapwapTrgFsCawapRunStatsEntry,
             pCapwapTrgIsSetFsCawapRunStatsEntry, SNMP_FAILURE) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCawapRunStatsTable: CapwapSetAllFsCawapRunStatsTableTrigger function returns failure.\r\n");
        }
    }

    if (pCapwapSetFsCawapRunStatsEntry->MibObject.i4FsCapwapRunStatsRowStatus ==
        ACTIVE)
    {
        i4RowMakeActive = TRUE;
    }

    /* Assign values for the existing node */
    if (pCapwapIsSetFsCawapRunStatsEntry->bFsCapwapRunStatsRowStatus !=
        OSIX_FALSE)
    {
        pCapwapFsCawapRunStatsEntry->MibObject.i4FsCapwapRunStatsRowStatus =
            pCapwapSetFsCawapRunStatsEntry->MibObject.
            i4FsCapwapRunStatsRowStatus;
    }

    /*This condtion is to make the row back to ACTIVE after modifying the values */
    if (i4RowMakeActive == TRUE)
    {
        pCapwapFsCawapRunStatsEntry->MibObject.i4FsCapwapRunStatsRowStatus =
            ACTIVE;
    }

    if (CapwapUtilUpdateFsCawapRunStatsTable (pCapwapOldFsCawapRunStatsEntry,
                                              pCapwapFsCawapRunStatsEntry,
                                              pCapwapIsSetFsCawapRunStatsEntry)
        != OSIX_SUCCESS)
    {

        if (CapwapSetAllFsCawapRunStatsTableTrigger
            (pCapwapSetFsCawapRunStatsEntry, pCapwapIsSetFsCawapRunStatsEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)

        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCawapRunStatsTable: CapwapSetAllFsCawapRunStatsTableTrigger function returns failure.\r\n");

        }
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapSetAllFsCawapRunStatsTable: CapwapUtilUpdateFsCawapRunStatsTable function returns failure.\r\n");
        /*Restore back with previous values */
        MEMCPY (pCapwapFsCawapRunStatsEntry, pCapwapOldFsCawapRunStatsEntry,
                sizeof (tCapwapFsCawapRunStatsEntry));
        MemReleaseMemBlock (CAPWAP_FSCAWAPRUNSTATSTABLE_POOLID,
                            (UINT1 *) pCapwapOldFsCawapRunStatsEntry);
        MemReleaseMemBlock (CAPWAP_FSCAWAPRUNSTATSTABLE_POOLID,
                            (UINT1 *) pCapwapTrgFsCawapRunStatsEntry);
        MemReleaseMemBlock (CAPWAP_FSCAWAPRUNSTATSTABLE_ISSET_POOLID,
                            (UINT1 *) pCapwapTrgIsSetFsCawapRunStatsEntry);
        return OSIX_FAILURE;

    }
    if (CapwapSetAllFsCawapRunStatsTableTrigger (pCapwapSetFsCawapRunStatsEntry,
                                                 pCapwapIsSetFsCawapRunStatsEntry,
                                                 SNMP_SUCCESS) != OSIX_SUCCESS)
    {

        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapSetAllFsCawapRunStatsTable: CapwapSetAllFsCawapRunStatsTableTrigger function returns failure.\r\n");
    }

    MemReleaseMemBlock (CAPWAP_FSCAWAPRUNSTATSTABLE_POOLID,
                        (UINT1 *) pCapwapOldFsCawapRunStatsEntry);
    MemReleaseMemBlock (CAPWAP_FSCAWAPRUNSTATSTABLE_POOLID,
                        (UINT1 *) pCapwapTrgFsCawapRunStatsEntry);
    MemReleaseMemBlock (CAPWAP_FSCAWAPRUNSTATSTABLE_ISSET_POOLID,
                        (UINT1 *) pCapwapTrgIsSetFsCawapRunStatsEntry);
    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  CapwapSetAllFsCapwapWirelessBindingTable
 Input       :  pCapwapSetFsCapwapWirelessBindingEntry
                pCapwapIsSetFsCapwapWirelessBindingEntry
                i4RowStatusLogic
                i4RowCreateOption
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapSetAllFsCapwapWirelessBindingTable (tCapwapFsCapwapWirelessBindingEntry *
                                          pCapwapSetFsCapwapWirelessBindingEntry,
                                          tCapwapIsSetFsCapwapWirelessBindingEntry
                                          *
                                          pCapwapIsSetFsCapwapWirelessBindingEntry,
                                          INT4 i4RowStatusLogic,
                                          INT4 i4RowCreateOption)
{
    tCapwapFsCapwapWirelessBindingEntry *pCapwapFsCapwapWirelessBindingEntry =
        NULL;
    tCapwapFsCapwapWirelessBindingEntry *pCapwapOldFsCapwapWirelessBindingEntry
        = NULL;
    tCapwapFsCapwapWirelessBindingEntry *pCapwapTrgFsCapwapWirelessBindingEntry
        = NULL;
    tCapwapIsSetFsCapwapWirelessBindingEntry
        * pCapwapTrgIsSetFsCapwapWirelessBindingEntry = NULL;
    INT4                i4RowMakeActive = FALSE;

    pCapwapOldFsCapwapWirelessBindingEntry =
        (tCapwapFsCapwapWirelessBindingEntry *)
        MemAllocMemBlk (CAPWAP_FSCAPWAPWIRELESSBINDINGTABLE_POOLID);
    if (pCapwapOldFsCapwapWirelessBindingEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    pCapwapTrgFsCapwapWirelessBindingEntry =
        (tCapwapFsCapwapWirelessBindingEntry *)
        MemAllocMemBlk (CAPWAP_FSCAPWAPWIRELESSBINDINGTABLE_POOLID);
    if (pCapwapTrgFsCapwapWirelessBindingEntry == NULL)
    {
        MemReleaseMemBlock (CAPWAP_FSCAPWAPWIRELESSBINDINGTABLE_POOLID,
                            (UINT1 *) pCapwapOldFsCapwapWirelessBindingEntry);
        return OSIX_FAILURE;
    }
    pCapwapTrgIsSetFsCapwapWirelessBindingEntry =
        (tCapwapIsSetFsCapwapWirelessBindingEntry *)
        MemAllocMemBlk (CAPWAP_FSCAPWAPWIRELESSBINDINGTABLE_ISSET_POOLID);
    if (pCapwapTrgIsSetFsCapwapWirelessBindingEntry == NULL)
    {
        MemReleaseMemBlock (CAPWAP_FSCAPWAPWIRELESSBINDINGTABLE_POOLID,
                            (UINT1 *) pCapwapOldFsCapwapWirelessBindingEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPWIRELESSBINDINGTABLE_POOLID,
                            (UINT1 *) pCapwapTrgFsCapwapWirelessBindingEntry);
        return OSIX_FAILURE;
    }
    MEMSET (pCapwapOldFsCapwapWirelessBindingEntry, 0,
            sizeof (tCapwapFsCapwapWirelessBindingEntry));
    MEMSET (pCapwapTrgFsCapwapWirelessBindingEntry, 0,
            sizeof (tCapwapFsCapwapWirelessBindingEntry));
    MEMSET (pCapwapTrgIsSetFsCapwapWirelessBindingEntry, 0,
            sizeof (tCapwapIsSetFsCapwapWirelessBindingEntry));

    /* Check whether the node is already present */
    pCapwapFsCapwapWirelessBindingEntry =
        RBTreeGet (gCapwapGlobals.CapwapGlbMib.FsCapwapWirelessBindingTable,
                   (tRBElem *) pCapwapSetFsCapwapWirelessBindingEntry);

    if (pCapwapFsCapwapWirelessBindingEntry == NULL)
    {
        /* Create the node if the RowStatus given is CREATE_AND_WAIT or CREATE_AND_GO */
        if ((pCapwapSetFsCapwapWirelessBindingEntry->MibObject.
             i4FsCapwapWirelessBindingRowStatus == CREATE_AND_WAIT)
            || (pCapwapSetFsCapwapWirelessBindingEntry->MibObject.
                i4FsCapwapWirelessBindingRowStatus == CREATE_AND_GO)
            ||
            ((pCapwapSetFsCapwapWirelessBindingEntry->MibObject.
              i4FsCapwapWirelessBindingRowStatus == ACTIVE)
             && (i4RowCreateOption == 1)))
        {
            /* Allocate memory for the new node */
            pCapwapFsCapwapWirelessBindingEntry =
                (tCapwapFsCapwapWirelessBindingEntry *)
                MemAllocMemBlk (CAPWAP_FSCAPWAPWIRELESSBINDINGTABLE_POOLID);
            if (pCapwapFsCapwapWirelessBindingEntry == NULL)
            {
                if (CapwapSetAllFsCapwapWirelessBindingTableTrigger
                    (pCapwapSetFsCapwapWirelessBindingEntry,
                     pCapwapIsSetFsCapwapWirelessBindingEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCapwapWirelessBindingTable:CapwapSetAllFsCapwapWirelessBindingTableTrigger function fails\r\n");

                }
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCapwapWirelessBindingTable: Fail to Allocate Memory.\r\n");
                MemReleaseMemBlock (CAPWAP_FSCAPWAPWIRELESSBINDINGTABLE_POOLID,
                                    (UINT1 *)
                                    pCapwapOldFsCapwapWirelessBindingEntry);
                MemReleaseMemBlock (CAPWAP_FSCAPWAPWIRELESSBINDINGTABLE_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgFsCapwapWirelessBindingEntry);
                MemReleaseMemBlock
                    (CAPWAP_FSCAPWAPWIRELESSBINDINGTABLE_ISSET_POOLID,
                     (UINT1 *) pCapwapTrgIsSetFsCapwapWirelessBindingEntry);
                return OSIX_FAILURE;
            }

            MEMSET (pCapwapFsCapwapWirelessBindingEntry, 0,
                    sizeof (tCapwapFsCapwapWirelessBindingEntry));
            if ((CapwapInitializeFsCapwapWirelessBindingTable
                 (pCapwapFsCapwapWirelessBindingEntry)) == OSIX_FAILURE)
            {
                if (CapwapSetAllFsCapwapWirelessBindingTableTrigger
                    (pCapwapSetFsCapwapWirelessBindingEntry,
                     pCapwapIsSetFsCapwapWirelessBindingEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCapwapWirelessBindingTable:CapwapSetAllFsCapwapWirelessBindingTableTrigger function fails\r\n");

                }
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCapwapWirelessBindingTable: Fail to Initialize the Objects.\r\n");

                MemReleaseMemBlock (CAPWAP_FSCAPWAPWIRELESSBINDINGTABLE_POOLID,
                                    (UINT1 *)
                                    pCapwapFsCapwapWirelessBindingEntry);
                MemReleaseMemBlock (CAPWAP_FSCAPWAPWIRELESSBINDINGTABLE_POOLID,
                                    (UINT1 *)
                                    pCapwapOldFsCapwapWirelessBindingEntry);
                MemReleaseMemBlock (CAPWAP_FSCAPWAPWIRELESSBINDINGTABLE_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgFsCapwapWirelessBindingEntry);
                MemReleaseMemBlock
                    (CAPWAP_FSCAPWAPWIRELESSBINDINGTABLE_ISSET_POOLID,
                     (UINT1 *) pCapwapTrgIsSetFsCapwapWirelessBindingEntry);
                return OSIX_FAILURE;
            }
            /* Assign values for the new node */
            if (pCapwapIsSetFsCapwapWirelessBindingEntry->
                bFsCapwapWirelessBindingVirtualRadioIfIndex != OSIX_FALSE)
            {
                pCapwapFsCapwapWirelessBindingEntry->MibObject.
                    i4FsCapwapWirelessBindingVirtualRadioIfIndex =
                    pCapwapSetFsCapwapWirelessBindingEntry->MibObject.
                    i4FsCapwapWirelessBindingVirtualRadioIfIndex;
            }

            if (pCapwapIsSetFsCapwapWirelessBindingEntry->
                bFsCapwapWirelessBindingType != OSIX_FALSE)
            {
                pCapwapFsCapwapWirelessBindingEntry->MibObject.
                    i4FsCapwapWirelessBindingType =
                    pCapwapSetFsCapwapWirelessBindingEntry->MibObject.
                    i4FsCapwapWirelessBindingType;
            }

            if (pCapwapIsSetFsCapwapWirelessBindingEntry->
                bFsCapwapWirelessBindingRowStatus != OSIX_FALSE)
            {
                pCapwapFsCapwapWirelessBindingEntry->MibObject.
                    i4FsCapwapWirelessBindingRowStatus =
                    pCapwapSetFsCapwapWirelessBindingEntry->MibObject.
                    i4FsCapwapWirelessBindingRowStatus;
            }

            if (pCapwapIsSetFsCapwapWirelessBindingEntry->
                bCapwapBaseWtpProfileId != OSIX_FALSE)
            {
                pCapwapFsCapwapWirelessBindingEntry->MibObject.
                    u4CapwapBaseWtpProfileId =
                    pCapwapSetFsCapwapWirelessBindingEntry->MibObject.
                    u4CapwapBaseWtpProfileId;
            }

            if (pCapwapIsSetFsCapwapWirelessBindingEntry->
                bCapwapBaseWirelessBindingRadioId != OSIX_FALSE)
            {
                pCapwapFsCapwapWirelessBindingEntry->MibObject.
                    u4CapwapBaseWirelessBindingRadioId =
                    pCapwapSetFsCapwapWirelessBindingEntry->MibObject.
                    u4CapwapBaseWirelessBindingRadioId;
            }

            if ((pCapwapSetFsCapwapWirelessBindingEntry->MibObject.
                 i4FsCapwapWirelessBindingRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pCapwapSetFsCapwapWirelessBindingEntry->MibObject.
                        i4FsCapwapWirelessBindingRowStatus == ACTIVE)))
            {
                pCapwapFsCapwapWirelessBindingEntry->MibObject.
                    i4FsCapwapWirelessBindingRowStatus = ACTIVE;
            }
            else if (pCapwapSetFsCapwapWirelessBindingEntry->MibObject.
                     i4FsCapwapWirelessBindingRowStatus == CREATE_AND_WAIT)
            {
                pCapwapFsCapwapWirelessBindingEntry->MibObject.
                    i4FsCapwapWirelessBindingRowStatus = NOT_READY;
            }

            /* Add the new node to the database */
            if (RBTreeAdd
                (gCapwapGlobals.CapwapGlbMib.FsCapwapWirelessBindingTable,
                 (tRBElem *) pCapwapFsCapwapWirelessBindingEntry) != RB_SUCCESS)
            {
                if (CapwapSetAllFsCapwapWirelessBindingTableTrigger
                    (pCapwapSetFsCapwapWirelessBindingEntry,
                     pCapwapIsSetFsCapwapWirelessBindingEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCapwapWirelessBindingTable: CapwapSetAllFsCapwapWirelessBindingTableTrigger function returns failure.\r\n");
                }
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCapwapWirelessBindingTable: RBTreeAdd is failed.\r\n");

                MemReleaseMemBlock (CAPWAP_FSCAPWAPWIRELESSBINDINGTABLE_POOLID,
                                    (UINT1 *)
                                    pCapwapFsCapwapWirelessBindingEntry);
                MemReleaseMemBlock (CAPWAP_FSCAPWAPWIRELESSBINDINGTABLE_POOLID,
                                    (UINT1 *)
                                    pCapwapOldFsCapwapWirelessBindingEntry);
                MemReleaseMemBlock (CAPWAP_FSCAPWAPWIRELESSBINDINGTABLE_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgFsCapwapWirelessBindingEntry);
                MemReleaseMemBlock
                    (CAPWAP_FSCAPWAPWIRELESSBINDINGTABLE_ISSET_POOLID,
                     (UINT1 *) pCapwapTrgIsSetFsCapwapWirelessBindingEntry);
                return OSIX_FAILURE;
            }
            if (CapwapUtilUpdateFsCapwapWirelessBindingTable
                (NULL, pCapwapFsCapwapWirelessBindingEntry,
                 pCapwapIsSetFsCapwapWirelessBindingEntry) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCapwapWirelessBindingTable: CapwapUtilUpdateFsCapwapWirelessBindingTable function returns failure.\r\n");

                if (CapwapSetAllFsCapwapWirelessBindingTableTrigger
                    (pCapwapSetFsCapwapWirelessBindingEntry,
                     pCapwapIsSetFsCapwapWirelessBindingEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCapwapWirelessBindingTable: CapwapSetAllFsCapwapWirelessBindingTableTrigger function returns failure.\r\n");

                }
                RBTreeRem (gCapwapGlobals.CapwapGlbMib.
                           FsCapwapWirelessBindingTable,
                           pCapwapFsCapwapWirelessBindingEntry);
                MemReleaseMemBlock (CAPWAP_FSCAPWAPWIRELESSBINDINGTABLE_POOLID,
                                    (UINT1 *)
                                    pCapwapFsCapwapWirelessBindingEntry);
                MemReleaseMemBlock (CAPWAP_FSCAPWAPWIRELESSBINDINGTABLE_POOLID,
                                    (UINT1 *)
                                    pCapwapOldFsCapwapWirelessBindingEntry);
                MemReleaseMemBlock (CAPWAP_FSCAPWAPWIRELESSBINDINGTABLE_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgFsCapwapWirelessBindingEntry);
                MemReleaseMemBlock
                    (CAPWAP_FSCAPWAPWIRELESSBINDINGTABLE_ISSET_POOLID,
                     (UINT1 *) pCapwapTrgIsSetFsCapwapWirelessBindingEntry);
                return OSIX_FAILURE;
            }

            if ((pCapwapSetFsCapwapWirelessBindingEntry->MibObject.
                 i4FsCapwapWirelessBindingRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pCapwapSetFsCapwapWirelessBindingEntry->MibObject.
                        i4FsCapwapWirelessBindingRowStatus == ACTIVE)))
            {

                /* For MSR and RM Trigger */
                pCapwapTrgFsCapwapWirelessBindingEntry->MibObject.
                    u4CapwapBaseWtpProfileId =
                    pCapwapSetFsCapwapWirelessBindingEntry->MibObject.
                    u4CapwapBaseWtpProfileId;
                pCapwapTrgFsCapwapWirelessBindingEntry->MibObject.
                    u4CapwapBaseWirelessBindingRadioId =
                    pCapwapSetFsCapwapWirelessBindingEntry->MibObject.
                    u4CapwapBaseWirelessBindingRadioId;
                pCapwapTrgFsCapwapWirelessBindingEntry->MibObject.
                    i4FsCapwapWirelessBindingRowStatus = CREATE_AND_WAIT;
                pCapwapTrgIsSetFsCapwapWirelessBindingEntry->
                    bFsCapwapWirelessBindingRowStatus = OSIX_TRUE;

                if (CapwapSetAllFsCapwapWirelessBindingTableTrigger
                    (pCapwapTrgFsCapwapWirelessBindingEntry,
                     pCapwapTrgIsSetFsCapwapWirelessBindingEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCapwapWirelessBindingTable: CapwapSetAllFsCapwapWirelessBindingTableTrigger function returns failure.\r\n");

                    MemReleaseMemBlock
                        (CAPWAP_FSCAPWAPWIRELESSBINDINGTABLE_POOLID,
                         (UINT1 *) pCapwapFsCapwapWirelessBindingEntry);
                    MemReleaseMemBlock
                        (CAPWAP_FSCAPWAPWIRELESSBINDINGTABLE_POOLID,
                         (UINT1 *) pCapwapOldFsCapwapWirelessBindingEntry);
                    MemReleaseMemBlock
                        (CAPWAP_FSCAPWAPWIRELESSBINDINGTABLE_POOLID,
                         (UINT1 *) pCapwapTrgFsCapwapWirelessBindingEntry);
                    MemReleaseMemBlock
                        (CAPWAP_FSCAPWAPWIRELESSBINDINGTABLE_ISSET_POOLID,
                         (UINT1 *) pCapwapTrgIsSetFsCapwapWirelessBindingEntry);
                    return OSIX_FAILURE;
                }
            }
            else if (pCapwapSetFsCapwapWirelessBindingEntry->MibObject.
                     i4FsCapwapWirelessBindingRowStatus == CREATE_AND_WAIT)
            {

                /* For MSR and RM Trigger */
                pCapwapTrgFsCapwapWirelessBindingEntry->MibObject.
                    i4FsCapwapWirelessBindingRowStatus = CREATE_AND_WAIT;
                pCapwapTrgIsSetFsCapwapWirelessBindingEntry->
                    bFsCapwapWirelessBindingRowStatus = OSIX_TRUE;

                if (CapwapSetAllFsCapwapWirelessBindingTableTrigger
                    (pCapwapTrgFsCapwapWirelessBindingEntry,
                     pCapwapTrgIsSetFsCapwapWirelessBindingEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCapwapWirelessBindingTable: CapwapSetAllFsCapwapWirelessBindingTableTrigger function returns failure.\r\n");

                    MemReleaseMemBlock
                        (CAPWAP_FSCAPWAPWIRELESSBINDINGTABLE_POOLID,
                         (UINT1 *) pCapwapFsCapwapWirelessBindingEntry);
                    MemReleaseMemBlock
                        (CAPWAP_FSCAPWAPWIRELESSBINDINGTABLE_POOLID,
                         (UINT1 *) pCapwapOldFsCapwapWirelessBindingEntry);
                    MemReleaseMemBlock
                        (CAPWAP_FSCAPWAPWIRELESSBINDINGTABLE_POOLID,
                         (UINT1 *) pCapwapTrgFsCapwapWirelessBindingEntry);
                    MemReleaseMemBlock
                        (CAPWAP_FSCAPWAPWIRELESSBINDINGTABLE_ISSET_POOLID,
                         (UINT1 *) pCapwapTrgIsSetFsCapwapWirelessBindingEntry);
                    return OSIX_FAILURE;
                }
            }

            if (pCapwapSetFsCapwapWirelessBindingEntry->MibObject.
                i4FsCapwapWirelessBindingRowStatus == CREATE_AND_GO)
            {
                pCapwapSetFsCapwapWirelessBindingEntry->MibObject.
                    i4FsCapwapWirelessBindingRowStatus = ACTIVE;
            }

            if (CapwapSetAllFsCapwapWirelessBindingTableTrigger
                (pCapwapSetFsCapwapWirelessBindingEntry,
                 pCapwapIsSetFsCapwapWirelessBindingEntry,
                 SNMP_SUCCESS) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCapwapWirelessBindingTable:  CapwapSetAllFsCapwapWirelessBindingTableTrigger function returns failure.\r\n");
            }
            MemReleaseMemBlock (CAPWAP_FSCAPWAPWIRELESSBINDINGTABLE_POOLID,
                                (UINT1 *)
                                pCapwapOldFsCapwapWirelessBindingEntry);
            MemReleaseMemBlock (CAPWAP_FSCAPWAPWIRELESSBINDINGTABLE_POOLID,
                                (UINT1 *)
                                pCapwapTrgFsCapwapWirelessBindingEntry);
            MemReleaseMemBlock
                (CAPWAP_FSCAPWAPWIRELESSBINDINGTABLE_ISSET_POOLID,
                 (UINT1 *) pCapwapTrgIsSetFsCapwapWirelessBindingEntry);
            return OSIX_SUCCESS;

        }
        else
        {
            if (CapwapSetAllFsCapwapWirelessBindingTableTrigger
                (pCapwapSetFsCapwapWirelessBindingEntry,
                 pCapwapIsSetFsCapwapWirelessBindingEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCapwapWirelessBindingTable: CapwapSetAllFsCapwapWirelessBindingTableTrigger function returns failure.\r\n");
            }
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCapwapWirelessBindingTable: Failure.\r\n");
            MemReleaseMemBlock (CAPWAP_FSCAPWAPWIRELESSBINDINGTABLE_POOLID,
                                (UINT1 *)
                                pCapwapOldFsCapwapWirelessBindingEntry);
            MemReleaseMemBlock (CAPWAP_FSCAPWAPWIRELESSBINDINGTABLE_POOLID,
                                (UINT1 *)
                                pCapwapTrgFsCapwapWirelessBindingEntry);
            MemReleaseMemBlock
                (CAPWAP_FSCAPWAPWIRELESSBINDINGTABLE_ISSET_POOLID,
                 (UINT1 *) pCapwapTrgIsSetFsCapwapWirelessBindingEntry);
            return OSIX_FAILURE;
        }
    }
    else if ((pCapwapSetFsCapwapWirelessBindingEntry->MibObject.
              i4FsCapwapWirelessBindingRowStatus == CREATE_AND_WAIT)
             || (pCapwapSetFsCapwapWirelessBindingEntry->MibObject.
                 i4FsCapwapWirelessBindingRowStatus == CREATE_AND_GO))
    {
        if (CapwapSetAllFsCapwapWirelessBindingTableTrigger
            (pCapwapSetFsCapwapWirelessBindingEntry,
             pCapwapIsSetFsCapwapWirelessBindingEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)

        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCapwapWirelessBindingTable: CapwapSetAllFsCapwapWirelessBindingTableTrigger function returns failure.\r\n");
        }
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapSetAllFsCapwapWirelessBindingTable: The row is already present.\r\n");
        MemReleaseMemBlock (CAPWAP_FSCAPWAPWIRELESSBINDINGTABLE_POOLID,
                            (UINT1 *) pCapwapOldFsCapwapWirelessBindingEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPWIRELESSBINDINGTABLE_POOLID,
                            (UINT1 *) pCapwapTrgFsCapwapWirelessBindingEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPWIRELESSBINDINGTABLE_ISSET_POOLID,
                            (UINT1 *)
                            pCapwapTrgIsSetFsCapwapWirelessBindingEntry);
        return OSIX_FAILURE;
    }
    /* Copy the previous values before setting the new values */
    MEMCPY (pCapwapOldFsCapwapWirelessBindingEntry,
            pCapwapFsCapwapWirelessBindingEntry,
            sizeof (tCapwapFsCapwapWirelessBindingEntry));

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pCapwapSetFsCapwapWirelessBindingEntry->MibObject.
        i4FsCapwapWirelessBindingRowStatus == DESTROY)
    {
        pCapwapFsCapwapWirelessBindingEntry->MibObject.
            i4FsCapwapWirelessBindingRowStatus = DESTROY;

        if (CapwapUtilUpdateFsCapwapWirelessBindingTable
            (pCapwapOldFsCapwapWirelessBindingEntry,
             pCapwapFsCapwapWirelessBindingEntry,
             pCapwapIsSetFsCapwapWirelessBindingEntry) != OSIX_SUCCESS)
        {

            if (CapwapSetAllFsCapwapWirelessBindingTableTrigger
                (pCapwapSetFsCapwapWirelessBindingEntry,
                 pCapwapIsSetFsCapwapWirelessBindingEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCapwapWirelessBindingTable: CapwapSetAllFsCapwapWirelessBindingTableTrigger function returns failure.\r\n");
            }
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCapwapWirelessBindingTable: CapwapUtilUpdateFsCapwapWirelessBindingTable function returns failure.\r\n");
        }
        RBTreeRem (gCapwapGlobals.CapwapGlbMib.FsCapwapWirelessBindingTable,
                   pCapwapFsCapwapWirelessBindingEntry);
        if (CapwapSetAllFsCapwapWirelessBindingTableTrigger
            (pCapwapSetFsCapwapWirelessBindingEntry,
             pCapwapIsSetFsCapwapWirelessBindingEntry,
             SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCapwapWirelessBindingTable: CapwapSetAllFsCapwapWirelessBindingTableTrigger function returns failure.\r\n");
            MemReleaseMemBlock (CAPWAP_FSCAPWAPWIRELESSBINDINGTABLE_POOLID,
                                (UINT1 *)
                                pCapwapOldFsCapwapWirelessBindingEntry);
            MemReleaseMemBlock (CAPWAP_FSCAPWAPWIRELESSBINDINGTABLE_POOLID,
                                (UINT1 *)
                                pCapwapTrgFsCapwapWirelessBindingEntry);
            MemReleaseMemBlock
                (CAPWAP_FSCAPWAPWIRELESSBINDINGTABLE_ISSET_POOLID,
                 (UINT1 *) pCapwapTrgIsSetFsCapwapWirelessBindingEntry);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (CAPWAP_FSCAPWAPWIRELESSBINDINGTABLE_POOLID,
                            (UINT1 *) pCapwapFsCapwapWirelessBindingEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPWIRELESSBINDINGTABLE_POOLID,
                            (UINT1 *) pCapwapOldFsCapwapWirelessBindingEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPWIRELESSBINDINGTABLE_POOLID,
                            (UINT1 *) pCapwapTrgFsCapwapWirelessBindingEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPWIRELESSBINDINGTABLE_ISSET_POOLID,
                            (UINT1 *)
                            pCapwapTrgIsSetFsCapwapWirelessBindingEntry);
        return OSIX_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (FsCapwapWirelessBindingTableFilterInputs
        (pCapwapFsCapwapWirelessBindingEntry,
         pCapwapSetFsCapwapWirelessBindingEntry,
         pCapwapIsSetFsCapwapWirelessBindingEntry) != OSIX_TRUE)
    {
        MemReleaseMemBlock (CAPWAP_FSCAPWAPWIRELESSBINDINGTABLE_POOLID,
                            (UINT1 *) pCapwapOldFsCapwapWirelessBindingEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPWIRELESSBINDINGTABLE_POOLID,
                            (UINT1 *) pCapwapTrgFsCapwapWirelessBindingEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPWIRELESSBINDINGTABLE_ISSET_POOLID,
                            (UINT1 *)
                            pCapwapTrgIsSetFsCapwapWirelessBindingEntry);
        return OSIX_SUCCESS;
    }

    /*This condtion is to make the row NOT_IN_SERVICE before modifying the values */
    if ((i4RowStatusLogic == TRUE) &&
        (pCapwapFsCapwapWirelessBindingEntry->MibObject.
         i4FsCapwapWirelessBindingRowStatus == ACTIVE)
        && (pCapwapSetFsCapwapWirelessBindingEntry->MibObject.
            i4FsCapwapWirelessBindingRowStatus != NOT_IN_SERVICE))
    {
        pCapwapFsCapwapWirelessBindingEntry->MibObject.
            i4FsCapwapWirelessBindingRowStatus = NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        pCapwapTrgFsCapwapWirelessBindingEntry->MibObject.
            i4FsCapwapWirelessBindingRowStatus = NOT_IN_SERVICE;
        pCapwapTrgIsSetFsCapwapWirelessBindingEntry->
            bFsCapwapWirelessBindingRowStatus = OSIX_TRUE;

        if (CapwapUtilUpdateFsCapwapWirelessBindingTable
            (pCapwapOldFsCapwapWirelessBindingEntry,
             pCapwapFsCapwapWirelessBindingEntry,
             pCapwapIsSetFsCapwapWirelessBindingEntry) != OSIX_SUCCESS)
        {
            /*Restore back with previous values */
            MEMCPY (pCapwapFsCapwapWirelessBindingEntry,
                    pCapwapOldFsCapwapWirelessBindingEntry,
                    sizeof (tCapwapFsCapwapWirelessBindingEntry));
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCapwapWirelessBindingTable:                 CapwapUtilUpdateFsCapwapWirelessBindingTable Function returns failure.\r\n");

            if (CapwapSetAllFsCapwapWirelessBindingTableTrigger
                (pCapwapSetFsCapwapWirelessBindingEntry,
                 pCapwapIsSetFsCapwapWirelessBindingEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCapwapWirelessBindingTable: CapwapSetAllFsCapwapWirelessBindingTableTrigger function returns failure.\r\n");
            }
            MemReleaseMemBlock (CAPWAP_FSCAPWAPWIRELESSBINDINGTABLE_POOLID,
                                (UINT1 *)
                                pCapwapOldFsCapwapWirelessBindingEntry);
            MemReleaseMemBlock (CAPWAP_FSCAPWAPWIRELESSBINDINGTABLE_POOLID,
                                (UINT1 *)
                                pCapwapTrgFsCapwapWirelessBindingEntry);
            MemReleaseMemBlock
                (CAPWAP_FSCAPWAPWIRELESSBINDINGTABLE_ISSET_POOLID,
                 (UINT1 *) pCapwapTrgIsSetFsCapwapWirelessBindingEntry);
            return OSIX_FAILURE;
        }

        if (CapwapSetAllFsCapwapWirelessBindingTableTrigger
            (pCapwapTrgFsCapwapWirelessBindingEntry,
             pCapwapTrgIsSetFsCapwapWirelessBindingEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCapwapWirelessBindingTable: CapwapSetAllFsCapwapWirelessBindingTableTrigger function returns failure.\r\n");
        }
    }

    if (pCapwapSetFsCapwapWirelessBindingEntry->MibObject.
        i4FsCapwapWirelessBindingRowStatus == ACTIVE)
    {
        i4RowMakeActive = TRUE;
    }

    /* Assign values for the existing node */
    if (pCapwapIsSetFsCapwapWirelessBindingEntry->
        bFsCapwapWirelessBindingVirtualRadioIfIndex != OSIX_FALSE)
    {
        pCapwapFsCapwapWirelessBindingEntry->MibObject.
            i4FsCapwapWirelessBindingVirtualRadioIfIndex =
            pCapwapSetFsCapwapWirelessBindingEntry->MibObject.
            i4FsCapwapWirelessBindingVirtualRadioIfIndex;
    }
    if (pCapwapIsSetFsCapwapWirelessBindingEntry->
        bFsCapwapWirelessBindingType != OSIX_FALSE)
    {
        pCapwapFsCapwapWirelessBindingEntry->MibObject.
            i4FsCapwapWirelessBindingType =
            pCapwapSetFsCapwapWirelessBindingEntry->MibObject.
            i4FsCapwapWirelessBindingType;
    }
    if (pCapwapIsSetFsCapwapWirelessBindingEntry->
        bFsCapwapWirelessBindingRowStatus != OSIX_FALSE)
    {
        pCapwapFsCapwapWirelessBindingEntry->MibObject.
            i4FsCapwapWirelessBindingRowStatus =
            pCapwapSetFsCapwapWirelessBindingEntry->MibObject.
            i4FsCapwapWirelessBindingRowStatus;
    }

    /*This condtion is to make the row back to ACTIVE after modifying the values */
    if (i4RowMakeActive == TRUE)
    {
        pCapwapFsCapwapWirelessBindingEntry->MibObject.
            i4FsCapwapWirelessBindingRowStatus = ACTIVE;
    }

    if (CapwapUtilUpdateFsCapwapWirelessBindingTable
        (pCapwapOldFsCapwapWirelessBindingEntry,
         pCapwapFsCapwapWirelessBindingEntry,
         pCapwapIsSetFsCapwapWirelessBindingEntry) != OSIX_SUCCESS)
    {

        if (CapwapSetAllFsCapwapWirelessBindingTableTrigger
            (pCapwapSetFsCapwapWirelessBindingEntry,
             pCapwapIsSetFsCapwapWirelessBindingEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)

        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCapwapWirelessBindingTable: CapwapSetAllFsCapwapWirelessBindingTableTrigger function returns failure.\r\n");

        }
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapSetAllFsCapwapWirelessBindingTable: CapwapUtilUpdateFsCapwapWirelessBindingTable function returns failure.\r\n");
        /*Restore back with previous values */
        MEMCPY (pCapwapFsCapwapWirelessBindingEntry,
                pCapwapOldFsCapwapWirelessBindingEntry,
                sizeof (tCapwapFsCapwapWirelessBindingEntry));
        MemReleaseMemBlock (CAPWAP_FSCAPWAPWIRELESSBINDINGTABLE_POOLID,
                            (UINT1 *) pCapwapOldFsCapwapWirelessBindingEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPWIRELESSBINDINGTABLE_POOLID,
                            (UINT1 *) pCapwapTrgFsCapwapWirelessBindingEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPWIRELESSBINDINGTABLE_ISSET_POOLID,
                            (UINT1 *)
                            pCapwapTrgIsSetFsCapwapWirelessBindingEntry);
        return OSIX_FAILURE;

    }
    if (CapwapSetAllFsCapwapWirelessBindingTableTrigger
        (pCapwapSetFsCapwapWirelessBindingEntry,
         pCapwapIsSetFsCapwapWirelessBindingEntry,
         SNMP_SUCCESS) != OSIX_SUCCESS)
    {

        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapSetAllFsCapwapWirelessBindingTable: CapwapSetAllFsCapwapWirelessBindingTableTrigger function returns failure.\r\n");
    }

    MemReleaseMemBlock (CAPWAP_FSCAPWAPWIRELESSBINDINGTABLE_POOLID,
                        (UINT1 *) pCapwapOldFsCapwapWirelessBindingEntry);
    MemReleaseMemBlock (CAPWAP_FSCAPWAPWIRELESSBINDINGTABLE_POOLID,
                        (UINT1 *) pCapwapTrgFsCapwapWirelessBindingEntry);
    MemReleaseMemBlock (CAPWAP_FSCAPWAPWIRELESSBINDINGTABLE_ISSET_POOLID,
                        (UINT1 *) pCapwapTrgIsSetFsCapwapWirelessBindingEntry);
    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  CapwapSetAllFsCapwapStationWhiteList
 Input       :  pCapwapSetFsCapwapStationWhiteListEntry
                pCapwapIsSetFsCapwapStationWhiteListEntry
                i4RowStatusLogic
                i4RowCreateOption
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapSetAllFsCapwapStationWhiteList (tCapwapFsCapwapStationWhiteListEntry *
                                      pCapwapSetFsCapwapStationWhiteListEntry,
                                      tCapwapIsSetFsCapwapStationWhiteListEntry
                                      *
                                      pCapwapIsSetFsCapwapStationWhiteListEntry,
                                      INT4 i4RowStatusLogic,
                                      INT4 i4RowCreateOption)
{
    tCapwapFsCapwapStationWhiteListEntry *pCapwapFsCapwapStationWhiteListEntry =
        NULL;
    tCapwapFsCapwapStationWhiteListEntry
        * pCapwapOldFsCapwapStationWhiteListEntry = NULL;
    tCapwapFsCapwapStationWhiteListEntry
        * pCapwapTrgFsCapwapStationWhiteListEntry = NULL;
    tCapwapIsSetFsCapwapStationWhiteListEntry
        * pCapwapTrgIsSetFsCapwapStationWhiteListEntry = NULL;
    INT4                i4RowMakeActive = FALSE;

    pCapwapOldFsCapwapStationWhiteListEntry =
        (tCapwapFsCapwapStationWhiteListEntry *)
        MemAllocMemBlk (CAPWAP_FSCAPWAPSTATIONWHITELIST_POOLID);
    if (pCapwapOldFsCapwapStationWhiteListEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    pCapwapTrgFsCapwapStationWhiteListEntry =
        (tCapwapFsCapwapStationWhiteListEntry *)
        MemAllocMemBlk (CAPWAP_FSCAPWAPSTATIONWHITELIST_POOLID);
    if (pCapwapTrgFsCapwapStationWhiteListEntry == NULL)
    {
        MemReleaseMemBlock (CAPWAP_FSCAPWAPSTATIONWHITELIST_POOLID,
                            (UINT1 *) pCapwapOldFsCapwapStationWhiteListEntry);
        return OSIX_FAILURE;
    }
    pCapwapTrgIsSetFsCapwapStationWhiteListEntry =
        (tCapwapIsSetFsCapwapStationWhiteListEntry *)
        MemAllocMemBlk (CAPWAP_FSCAPWAPSTATIONWHITELIST_ISSET_POOLID);
    if (pCapwapTrgIsSetFsCapwapStationWhiteListEntry == NULL)
    {
        MemReleaseMemBlock (CAPWAP_FSCAPWAPSTATIONWHITELIST_POOLID,
                            (UINT1 *) pCapwapOldFsCapwapStationWhiteListEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPSTATIONWHITELIST_POOLID,
                            (UINT1 *) pCapwapTrgFsCapwapStationWhiteListEntry);
        return OSIX_FAILURE;
    }
    MEMSET (pCapwapOldFsCapwapStationWhiteListEntry, 0,
            sizeof (tCapwapFsCapwapStationWhiteListEntry));
    MEMSET (pCapwapTrgFsCapwapStationWhiteListEntry, 0,
            sizeof (tCapwapFsCapwapStationWhiteListEntry));
    MEMSET (pCapwapTrgIsSetFsCapwapStationWhiteListEntry, 0,
            sizeof (tCapwapIsSetFsCapwapStationWhiteListEntry));

    /* Check whether the node is already present */
    pCapwapFsCapwapStationWhiteListEntry =
        RBTreeGet (gCapwapGlobals.CapwapGlbMib.FsCapwapStationWhiteList,
                   (tRBElem *) pCapwapSetFsCapwapStationWhiteListEntry);

    if (pCapwapFsCapwapStationWhiteListEntry == NULL)
    {
        /* Create the node if the RowStatus given is CREATE_AND_WAIT or CREATE_AND_GO */
        if ((pCapwapSetFsCapwapStationWhiteListEntry->MibObject.
             i4FsCapwapStationWhiteListRowStatus == CREATE_AND_WAIT)
            || (pCapwapSetFsCapwapStationWhiteListEntry->MibObject.
                i4FsCapwapStationWhiteListRowStatus == CREATE_AND_GO)
            ||
            ((pCapwapSetFsCapwapStationWhiteListEntry->MibObject.
              i4FsCapwapStationWhiteListRowStatus == ACTIVE)
             && (i4RowCreateOption == 1)))
        {
            /* Allocate memory for the new node */
            pCapwapFsCapwapStationWhiteListEntry =
                (tCapwapFsCapwapStationWhiteListEntry *)
                MemAllocMemBlk (CAPWAP_FSCAPWAPSTATIONWHITELIST_POOLID);
            if (pCapwapFsCapwapStationWhiteListEntry == NULL)
            {
                if (CapwapSetAllFsCapwapStationWhiteListTrigger
                    (pCapwapSetFsCapwapStationWhiteListEntry,
                     pCapwapIsSetFsCapwapStationWhiteListEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCapwapStationWhiteList:CapwapSetAllFsCapwapStationWhiteListTrigger function fails\r\n");

                }
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCapwapStationWhiteList: Fail to Allocate Memory.\r\n");
                MemReleaseMemBlock (CAPWAP_FSCAPWAPSTATIONWHITELIST_POOLID,
                                    (UINT1 *)
                                    pCapwapOldFsCapwapStationWhiteListEntry);
                MemReleaseMemBlock (CAPWAP_FSCAPWAPSTATIONWHITELIST_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgFsCapwapStationWhiteListEntry);
                MemReleaseMemBlock
                    (CAPWAP_FSCAPWAPSTATIONWHITELIST_ISSET_POOLID,
                     (UINT1 *) pCapwapTrgIsSetFsCapwapStationWhiteListEntry);
                return OSIX_FAILURE;
            }

            MEMSET (pCapwapFsCapwapStationWhiteListEntry, 0,
                    sizeof (tCapwapFsCapwapStationWhiteListEntry));
            if ((CapwapInitializeFsCapwapStationWhiteList
                 (pCapwapFsCapwapStationWhiteListEntry)) == OSIX_FAILURE)
            {
                if (CapwapSetAllFsCapwapStationWhiteListTrigger
                    (pCapwapSetFsCapwapStationWhiteListEntry,
                     pCapwapIsSetFsCapwapStationWhiteListEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCapwapStationWhiteList:CapwapSetAllFsCapwapStationWhiteListTrigger function fails\r\n");

                }
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCapwapStationWhiteList: Fail to Initialize the Objects.\r\n");

                MemReleaseMemBlock (CAPWAP_FSCAPWAPSTATIONWHITELIST_POOLID,
                                    (UINT1 *)
                                    pCapwapFsCapwapStationWhiteListEntry);
                MemReleaseMemBlock (CAPWAP_FSCAPWAPSTATIONWHITELIST_POOLID,
                                    (UINT1 *)
                                    pCapwapOldFsCapwapStationWhiteListEntry);
                MemReleaseMemBlock (CAPWAP_FSCAPWAPSTATIONWHITELIST_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgFsCapwapStationWhiteListEntry);
                MemReleaseMemBlock
                    (CAPWAP_FSCAPWAPSTATIONWHITELIST_ISSET_POOLID,
                     (UINT1 *) pCapwapTrgIsSetFsCapwapStationWhiteListEntry);
                return OSIX_FAILURE;
            }
            /* Assign values for the new node */
            if (pCapwapIsSetFsCapwapStationWhiteListEntry->
                bFsCapwapStationWhiteListId != OSIX_FALSE)
            {
                pCapwapFsCapwapStationWhiteListEntry->MibObject.
                    u4FsCapwapStationWhiteListId =
                    pCapwapSetFsCapwapStationWhiteListEntry->MibObject.
                    u4FsCapwapStationWhiteListId;
            }

            if (pCapwapIsSetFsCapwapStationWhiteListEntry->
                bFsCapwapStationWhiteListStationId != OSIX_FALSE)
            {
                MEMCPY (pCapwapFsCapwapStationWhiteListEntry->MibObject.
                        au1FsCapwapStationWhiteListStationId,
                        pCapwapSetFsCapwapStationWhiteListEntry->MibObject.
                        au1FsCapwapStationWhiteListStationId,
                        pCapwapSetFsCapwapStationWhiteListEntry->MibObject.
                        i4FsCapwapStationWhiteListStationIdLen);

                pCapwapFsCapwapStationWhiteListEntry->MibObject.
                    i4FsCapwapStationWhiteListStationIdLen =
                    pCapwapSetFsCapwapStationWhiteListEntry->MibObject.
                    i4FsCapwapStationWhiteListStationIdLen;
            }

            if (pCapwapIsSetFsCapwapStationWhiteListEntry->
                bFsCapwapStationWhiteListRowStatus != OSIX_FALSE)
            {
                pCapwapFsCapwapStationWhiteListEntry->MibObject.
                    i4FsCapwapStationWhiteListRowStatus =
                    pCapwapSetFsCapwapStationWhiteListEntry->MibObject.
                    i4FsCapwapStationWhiteListRowStatus;
            }

            if ((pCapwapSetFsCapwapStationWhiteListEntry->MibObject.
                 i4FsCapwapStationWhiteListRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pCapwapSetFsCapwapStationWhiteListEntry->MibObject.
                        i4FsCapwapStationWhiteListRowStatus == ACTIVE)))
            {
                pCapwapFsCapwapStationWhiteListEntry->MibObject.
                    i4FsCapwapStationWhiteListRowStatus = ACTIVE;
            }
            else if (pCapwapSetFsCapwapStationWhiteListEntry->MibObject.
                     i4FsCapwapStationWhiteListRowStatus == CREATE_AND_WAIT)
            {
                pCapwapFsCapwapStationWhiteListEntry->MibObject.
                    i4FsCapwapStationWhiteListRowStatus = NOT_READY;
            }

            /* Add the new node to the database */
            if (RBTreeAdd
                (gCapwapGlobals.CapwapGlbMib.FsCapwapStationWhiteList,
                 (tRBElem *) pCapwapFsCapwapStationWhiteListEntry) !=
                RB_SUCCESS)
            {
                if (CapwapSetAllFsCapwapStationWhiteListTrigger
                    (pCapwapSetFsCapwapStationWhiteListEntry,
                     pCapwapIsSetFsCapwapStationWhiteListEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCapwapStationWhiteList: CapwapSetAllFsCapwapStationWhiteListTrigger function returns failure.\r\n");
                }
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCapwapStationWhiteList: RBTreeAdd is failed.\r\n");

                MemReleaseMemBlock (CAPWAP_FSCAPWAPSTATIONWHITELIST_POOLID,
                                    (UINT1 *)
                                    pCapwapFsCapwapStationWhiteListEntry);
                MemReleaseMemBlock (CAPWAP_FSCAPWAPSTATIONWHITELIST_POOLID,
                                    (UINT1 *)
                                    pCapwapOldFsCapwapStationWhiteListEntry);
                MemReleaseMemBlock (CAPWAP_FSCAPWAPSTATIONWHITELIST_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgFsCapwapStationWhiteListEntry);
                MemReleaseMemBlock
                    (CAPWAP_FSCAPWAPSTATIONWHITELIST_ISSET_POOLID,
                     (UINT1 *) pCapwapTrgIsSetFsCapwapStationWhiteListEntry);
                return OSIX_FAILURE;
            }
            if (CapwapUtilUpdateFsCapwapStationWhiteList
                (NULL, pCapwapFsCapwapStationWhiteListEntry,
                 pCapwapIsSetFsCapwapStationWhiteListEntry) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCapwapStationWhiteList: CapwapUtilUpdateFsCapwapStationWhiteList function returns failure.\r\n");

                if (CapwapSetAllFsCapwapStationWhiteListTrigger
                    (pCapwapSetFsCapwapStationWhiteListEntry,
                     pCapwapIsSetFsCapwapStationWhiteListEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCapwapStationWhiteList: CapwapSetAllFsCapwapStationWhiteListTrigger function returns failure.\r\n");

                }
                RBTreeRem (gCapwapGlobals.CapwapGlbMib.FsCapwapStationWhiteList,
                           pCapwapFsCapwapStationWhiteListEntry);
                MemReleaseMemBlock (CAPWAP_FSCAPWAPSTATIONWHITELIST_POOLID,
                                    (UINT1 *)
                                    pCapwapFsCapwapStationWhiteListEntry);
                MemReleaseMemBlock (CAPWAP_FSCAPWAPSTATIONWHITELIST_POOLID,
                                    (UINT1 *)
                                    pCapwapOldFsCapwapStationWhiteListEntry);
                MemReleaseMemBlock (CAPWAP_FSCAPWAPSTATIONWHITELIST_POOLID,
                                    (UINT1 *)
                                    pCapwapTrgFsCapwapStationWhiteListEntry);
                MemReleaseMemBlock
                    (CAPWAP_FSCAPWAPSTATIONWHITELIST_ISSET_POOLID,
                     (UINT1 *) pCapwapTrgIsSetFsCapwapStationWhiteListEntry);
                return OSIX_FAILURE;
            }

            if ((pCapwapSetFsCapwapStationWhiteListEntry->MibObject.
                 i4FsCapwapStationWhiteListRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pCapwapSetFsCapwapStationWhiteListEntry->MibObject.
                        i4FsCapwapStationWhiteListRowStatus == ACTIVE)))
            {

                /* For MSR and RM Trigger */
                pCapwapTrgFsCapwapStationWhiteListEntry->MibObject.
                    u4FsCapwapStationWhiteListId =
                    pCapwapSetFsCapwapStationWhiteListEntry->MibObject.
                    u4FsCapwapStationWhiteListId;
                pCapwapTrgFsCapwapStationWhiteListEntry->MibObject.
                    i4FsCapwapStationWhiteListRowStatus = CREATE_AND_WAIT;
                pCapwapTrgIsSetFsCapwapStationWhiteListEntry->
                    bFsCapwapStationWhiteListRowStatus = OSIX_TRUE;

                if (CapwapSetAllFsCapwapStationWhiteListTrigger
                    (pCapwapTrgFsCapwapStationWhiteListEntry,
                     pCapwapTrgIsSetFsCapwapStationWhiteListEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCapwapStationWhiteList: CapwapSetAllFsCapwapStationWhiteListTrigger function returns failure.\r\n");

                    MemReleaseMemBlock (CAPWAP_FSCAPWAPSTATIONWHITELIST_POOLID,
                                        (UINT1 *)
                                        pCapwapFsCapwapStationWhiteListEntry);
                    MemReleaseMemBlock (CAPWAP_FSCAPWAPSTATIONWHITELIST_POOLID,
                                        (UINT1 *)
                                        pCapwapOldFsCapwapStationWhiteListEntry);
                    MemReleaseMemBlock (CAPWAP_FSCAPWAPSTATIONWHITELIST_POOLID,
                                        (UINT1 *)
                                        pCapwapTrgFsCapwapStationWhiteListEntry);
                    MemReleaseMemBlock
                        (CAPWAP_FSCAPWAPSTATIONWHITELIST_ISSET_POOLID,
                         (UINT1 *)
                         pCapwapTrgIsSetFsCapwapStationWhiteListEntry);
                    return OSIX_FAILURE;
                }
            }
            else if (pCapwapSetFsCapwapStationWhiteListEntry->MibObject.
                     i4FsCapwapStationWhiteListRowStatus == CREATE_AND_WAIT)
            {

                /* For MSR and RM Trigger */
                pCapwapTrgFsCapwapStationWhiteListEntry->MibObject.
                    i4FsCapwapStationWhiteListRowStatus = CREATE_AND_WAIT;
                pCapwapTrgIsSetFsCapwapStationWhiteListEntry->
                    bFsCapwapStationWhiteListRowStatus = OSIX_TRUE;

                if (CapwapSetAllFsCapwapStationWhiteListTrigger
                    (pCapwapTrgFsCapwapStationWhiteListEntry,
                     pCapwapTrgIsSetFsCapwapStationWhiteListEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCapwapStationWhiteList: CapwapSetAllFsCapwapStationWhiteListTrigger function returns failure.\r\n");

                    MemReleaseMemBlock (CAPWAP_FSCAPWAPSTATIONWHITELIST_POOLID,
                                        (UINT1 *)
                                        pCapwapFsCapwapStationWhiteListEntry);
                    MemReleaseMemBlock (CAPWAP_FSCAPWAPSTATIONWHITELIST_POOLID,
                                        (UINT1 *)
                                        pCapwapOldFsCapwapStationWhiteListEntry);
                    MemReleaseMemBlock (CAPWAP_FSCAPWAPSTATIONWHITELIST_POOLID,
                                        (UINT1 *)
                                        pCapwapTrgFsCapwapStationWhiteListEntry);
                    MemReleaseMemBlock
                        (CAPWAP_FSCAPWAPSTATIONWHITELIST_ISSET_POOLID,
                         (UINT1 *)
                         pCapwapTrgIsSetFsCapwapStationWhiteListEntry);
                    return OSIX_FAILURE;
                }
            }

            if (pCapwapSetFsCapwapStationWhiteListEntry->MibObject.
                i4FsCapwapStationWhiteListRowStatus == CREATE_AND_GO)
            {
                pCapwapSetFsCapwapStationWhiteListEntry->MibObject.
                    i4FsCapwapStationWhiteListRowStatus = ACTIVE;
            }

            if (CapwapSetAllFsCapwapStationWhiteListTrigger
                (pCapwapSetFsCapwapStationWhiteListEntry,
                 pCapwapIsSetFsCapwapStationWhiteListEntry,
                 SNMP_SUCCESS) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCapwapStationWhiteList:  CapwapSetAllFsCapwapStationWhiteListTrigger function returns failure.\r\n");
            }
            MemReleaseMemBlock (CAPWAP_FSCAPWAPSTATIONWHITELIST_POOLID,
                                (UINT1 *)
                                pCapwapOldFsCapwapStationWhiteListEntry);
            MemReleaseMemBlock (CAPWAP_FSCAPWAPSTATIONWHITELIST_POOLID,
                                (UINT1 *)
                                pCapwapTrgFsCapwapStationWhiteListEntry);
            MemReleaseMemBlock (CAPWAP_FSCAPWAPSTATIONWHITELIST_ISSET_POOLID,
                                (UINT1 *)
                                pCapwapTrgIsSetFsCapwapStationWhiteListEntry);
            return OSIX_SUCCESS;

        }
        else
        {
            if (CapwapSetAllFsCapwapStationWhiteListTrigger
                (pCapwapSetFsCapwapStationWhiteListEntry,
                 pCapwapIsSetFsCapwapStationWhiteListEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCapwapStationWhiteList: CapwapSetAllFsCapwapStationWhiteListTrigger function returns failure.\r\n");
            }
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCapwapStationWhiteList: Failure.\r\n");
            MemReleaseMemBlock (CAPWAP_FSCAPWAPSTATIONWHITELIST_POOLID,
                                (UINT1 *)
                                pCapwapOldFsCapwapStationWhiteListEntry);
            MemReleaseMemBlock (CAPWAP_FSCAPWAPSTATIONWHITELIST_POOLID,
                                (UINT1 *)
                                pCapwapTrgFsCapwapStationWhiteListEntry);
            MemReleaseMemBlock (CAPWAP_FSCAPWAPSTATIONWHITELIST_ISSET_POOLID,
                                (UINT1 *)
                                pCapwapTrgIsSetFsCapwapStationWhiteListEntry);
            return OSIX_FAILURE;
        }
    }
    else if ((pCapwapSetFsCapwapStationWhiteListEntry->MibObject.
              i4FsCapwapStationWhiteListRowStatus == CREATE_AND_WAIT)
             || (pCapwapSetFsCapwapStationWhiteListEntry->MibObject.
                 i4FsCapwapStationWhiteListRowStatus == CREATE_AND_GO))
    {
        if (CapwapSetAllFsCapwapStationWhiteListTrigger
            (pCapwapSetFsCapwapStationWhiteListEntry,
             pCapwapIsSetFsCapwapStationWhiteListEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)

        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCapwapStationWhiteList: CapwapSetAllFsCapwapStationWhiteListTrigger function returns failure.\r\n");
        }
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapSetAllFsCapwapStationWhiteList: The row is already present.\r\n");
        MemReleaseMemBlock (CAPWAP_FSCAPWAPSTATIONWHITELIST_POOLID,
                            (UINT1 *) pCapwapOldFsCapwapStationWhiteListEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPSTATIONWHITELIST_POOLID,
                            (UINT1 *) pCapwapTrgFsCapwapStationWhiteListEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPSTATIONWHITELIST_ISSET_POOLID,
                            (UINT1 *)
                            pCapwapTrgIsSetFsCapwapStationWhiteListEntry);
        return OSIX_FAILURE;
    }
    /* Copy the previous values before setting the new values */
    MEMCPY (pCapwapOldFsCapwapStationWhiteListEntry,
            pCapwapFsCapwapStationWhiteListEntry,
            sizeof (tCapwapFsCapwapStationWhiteListEntry));

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pCapwapSetFsCapwapStationWhiteListEntry->MibObject.
        i4FsCapwapStationWhiteListRowStatus == DESTROY)
    {
        pCapwapFsCapwapStationWhiteListEntry->MibObject.
            i4FsCapwapStationWhiteListRowStatus = DESTROY;

        if (CapwapUtilUpdateFsCapwapStationWhiteList
            (pCapwapOldFsCapwapStationWhiteListEntry,
             pCapwapFsCapwapStationWhiteListEntry,
             pCapwapIsSetFsCapwapStationWhiteListEntry) != OSIX_SUCCESS)
        {

            if (CapwapSetAllFsCapwapStationWhiteListTrigger
                (pCapwapSetFsCapwapStationWhiteListEntry,
                 pCapwapIsSetFsCapwapStationWhiteListEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCapwapStationWhiteList: CapwapSetAllFsCapwapStationWhiteListTrigger function returns failure.\r\n");
            }
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCapwapStationWhiteList: CapwapUtilUpdateFsCapwapStationWhiteList function returns failure.\r\n");
        }
        RBTreeRem (gCapwapGlobals.CapwapGlbMib.FsCapwapStationWhiteList,
                   pCapwapFsCapwapStationWhiteListEntry);
        if (CapwapSetAllFsCapwapStationWhiteListTrigger
            (pCapwapSetFsCapwapStationWhiteListEntry,
             pCapwapIsSetFsCapwapStationWhiteListEntry,
             SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCapwapStationWhiteList: CapwapSetAllFsCapwapStationWhiteListTrigger function returns failure.\r\n");
            MemReleaseMemBlock (CAPWAP_FSCAPWAPSTATIONWHITELIST_POOLID,
                                (UINT1 *)
                                pCapwapOldFsCapwapStationWhiteListEntry);
            MemReleaseMemBlock (CAPWAP_FSCAPWAPSTATIONWHITELIST_POOLID,
                                (UINT1 *)
                                pCapwapTrgFsCapwapStationWhiteListEntry);
            MemReleaseMemBlock (CAPWAP_FSCAPWAPSTATIONWHITELIST_ISSET_POOLID,
                                (UINT1 *)
                                pCapwapTrgIsSetFsCapwapStationWhiteListEntry);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (CAPWAP_FSCAPWAPSTATIONWHITELIST_POOLID,
                            (UINT1 *) pCapwapFsCapwapStationWhiteListEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPSTATIONWHITELIST_POOLID,
                            (UINT1 *) pCapwapOldFsCapwapStationWhiteListEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPSTATIONWHITELIST_POOLID,
                            (UINT1 *) pCapwapTrgFsCapwapStationWhiteListEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPSTATIONWHITELIST_ISSET_POOLID,
                            (UINT1 *)
                            pCapwapTrgIsSetFsCapwapStationWhiteListEntry);
        return OSIX_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (FsCapwapStationWhiteListFilterInputs
        (pCapwapFsCapwapStationWhiteListEntry,
         pCapwapSetFsCapwapStationWhiteListEntry,
         pCapwapIsSetFsCapwapStationWhiteListEntry) != OSIX_TRUE)
    {
        MemReleaseMemBlock (CAPWAP_FSCAPWAPSTATIONWHITELIST_POOLID,
                            (UINT1 *) pCapwapOldFsCapwapStationWhiteListEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPSTATIONWHITELIST_POOLID,
                            (UINT1 *) pCapwapTrgFsCapwapStationWhiteListEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPSTATIONWHITELIST_ISSET_POOLID,
                            (UINT1 *)
                            pCapwapTrgIsSetFsCapwapStationWhiteListEntry);
        return OSIX_SUCCESS;
    }

    /*This condtion is to make the row NOT_IN_SERVICE before modifying the values */
    if ((i4RowStatusLogic == TRUE) &&
        (pCapwapFsCapwapStationWhiteListEntry->MibObject.
         i4FsCapwapStationWhiteListRowStatus == ACTIVE)
        && (pCapwapSetFsCapwapStationWhiteListEntry->MibObject.
            i4FsCapwapStationWhiteListRowStatus != NOT_IN_SERVICE))
    {
        pCapwapFsCapwapStationWhiteListEntry->MibObject.
            i4FsCapwapStationWhiteListRowStatus = NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        pCapwapTrgFsCapwapStationWhiteListEntry->MibObject.
            i4FsCapwapStationWhiteListRowStatus = NOT_IN_SERVICE;
        pCapwapTrgIsSetFsCapwapStationWhiteListEntry->
            bFsCapwapStationWhiteListRowStatus = OSIX_TRUE;

        if (CapwapUtilUpdateFsCapwapStationWhiteList
            (pCapwapOldFsCapwapStationWhiteListEntry,
             pCapwapFsCapwapStationWhiteListEntry,
             pCapwapIsSetFsCapwapStationWhiteListEntry) != OSIX_SUCCESS)
        {
            /*Restore back with previous values */
            MEMCPY (pCapwapFsCapwapStationWhiteListEntry,
                    pCapwapOldFsCapwapStationWhiteListEntry,
                    sizeof (tCapwapFsCapwapStationWhiteListEntry));
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCapwapStationWhiteList:                 CapwapUtilUpdateFsCapwapStationWhiteList Function returns failure.\r\n");

            if (CapwapSetAllFsCapwapStationWhiteListTrigger
                (pCapwapSetFsCapwapStationWhiteListEntry,
                 pCapwapIsSetFsCapwapStationWhiteListEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCapwapStationWhiteList: CapwapSetAllFsCapwapStationWhiteListTrigger function returns failure.\r\n");
            }
            MemReleaseMemBlock (CAPWAP_FSCAPWAPSTATIONWHITELIST_POOLID,
                                (UINT1 *)
                                pCapwapOldFsCapwapStationWhiteListEntry);
            MemReleaseMemBlock (CAPWAP_FSCAPWAPSTATIONWHITELIST_POOLID,
                                (UINT1 *)
                                pCapwapTrgFsCapwapStationWhiteListEntry);
            MemReleaseMemBlock (CAPWAP_FSCAPWAPSTATIONWHITELIST_ISSET_POOLID,
                                (UINT1 *)
                                pCapwapTrgIsSetFsCapwapStationWhiteListEntry);
            return OSIX_FAILURE;
        }

        if (CapwapSetAllFsCapwapStationWhiteListTrigger
            (pCapwapTrgFsCapwapStationWhiteListEntry,
             pCapwapTrgIsSetFsCapwapStationWhiteListEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCapwapStationWhiteList: CapwapSetAllFsCapwapStationWhiteListTrigger function returns failure.\r\n");
        }
    }

    if (pCapwapSetFsCapwapStationWhiteListEntry->MibObject.
        i4FsCapwapStationWhiteListRowStatus == ACTIVE)
    {
        i4RowMakeActive = TRUE;
    }

    /* Assign values for the existing node */
    if (pCapwapIsSetFsCapwapStationWhiteListEntry->
        bFsCapwapStationWhiteListStationId != OSIX_FALSE)
    {
        MEMCPY (pCapwapFsCapwapStationWhiteListEntry->MibObject.
                au1FsCapwapStationWhiteListStationId,
                pCapwapSetFsCapwapStationWhiteListEntry->MibObject.
                au1FsCapwapStationWhiteListStationId,
                pCapwapSetFsCapwapStationWhiteListEntry->MibObject.
                i4FsCapwapStationWhiteListStationIdLen);

        pCapwapFsCapwapStationWhiteListEntry->MibObject.
            i4FsCapwapStationWhiteListStationIdLen =
            pCapwapSetFsCapwapStationWhiteListEntry->MibObject.
            i4FsCapwapStationWhiteListStationIdLen;
    }
    if (pCapwapIsSetFsCapwapStationWhiteListEntry->
        bFsCapwapStationWhiteListRowStatus != OSIX_FALSE)
    {
        pCapwapFsCapwapStationWhiteListEntry->MibObject.
            i4FsCapwapStationWhiteListRowStatus =
            pCapwapSetFsCapwapStationWhiteListEntry->MibObject.
            i4FsCapwapStationWhiteListRowStatus;
    }

    /*This condtion is to make the row back to ACTIVE after modifying the values */
    if (i4RowMakeActive == TRUE)
    {
        pCapwapFsCapwapStationWhiteListEntry->MibObject.
            i4FsCapwapStationWhiteListRowStatus = ACTIVE;
    }

    if (CapwapUtilUpdateFsCapwapStationWhiteList
        (pCapwapOldFsCapwapStationWhiteListEntry,
         pCapwapFsCapwapStationWhiteListEntry,
         pCapwapIsSetFsCapwapStationWhiteListEntry) != OSIX_SUCCESS)
    {

        if (CapwapSetAllFsCapwapStationWhiteListTrigger
            (pCapwapSetFsCapwapStationWhiteListEntry,
             pCapwapIsSetFsCapwapStationWhiteListEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)

        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCapwapStationWhiteList: CapwapSetAllFsCapwapStationWhiteListTrigger function returns failure.\r\n");

        }
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapSetAllFsCapwapStationWhiteList: CapwapUtilUpdateFsCapwapStationWhiteList function returns failure.\r\n");
        /*Restore back with previous values */
        MEMCPY (pCapwapFsCapwapStationWhiteListEntry,
                pCapwapOldFsCapwapStationWhiteListEntry,
                sizeof (tCapwapFsCapwapStationWhiteListEntry));
        MemReleaseMemBlock (CAPWAP_FSCAPWAPSTATIONWHITELIST_POOLID,
                            (UINT1 *) pCapwapOldFsCapwapStationWhiteListEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPSTATIONWHITELIST_POOLID,
                            (UINT1 *) pCapwapTrgFsCapwapStationWhiteListEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPSTATIONWHITELIST_ISSET_POOLID,
                            (UINT1 *)
                            pCapwapTrgIsSetFsCapwapStationWhiteListEntry);
        return OSIX_FAILURE;

    }
    if (CapwapSetAllFsCapwapStationWhiteListTrigger
        (pCapwapSetFsCapwapStationWhiteListEntry,
         pCapwapIsSetFsCapwapStationWhiteListEntry,
         SNMP_SUCCESS) != OSIX_SUCCESS)
    {

        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapSetAllFsCapwapStationWhiteList: CapwapSetAllFsCapwapStationWhiteListTrigger function returns failure.\r\n");
    }

    MemReleaseMemBlock (CAPWAP_FSCAPWAPSTATIONWHITELIST_POOLID,
                        (UINT1 *) pCapwapOldFsCapwapStationWhiteListEntry);
    MemReleaseMemBlock (CAPWAP_FSCAPWAPSTATIONWHITELIST_POOLID,
                        (UINT1 *) pCapwapTrgFsCapwapStationWhiteListEntry);
    MemReleaseMemBlock (CAPWAP_FSCAPWAPSTATIONWHITELIST_ISSET_POOLID,
                        (UINT1 *) pCapwapTrgIsSetFsCapwapStationWhiteListEntry);
    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  CapwapSetAllFsCapwapWtpRebootStatisticsTable
 Input       :  pCapwapSetFsCapwapWtpRebootStatisticsEntry
                pCapwapIsSetFsCapwapWtpRebootStatisticsEntry
                i4RowStatusLogic
                i4RowCreateOption
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    CapwapSetAllFsCapwapWtpRebootStatisticsTable
    (tCapwapFsCapwapWtpRebootStatisticsEntry *
     pCapwapSetFsCapwapWtpRebootStatisticsEntry,
     tCapwapIsSetFsCapwapWtpRebootStatisticsEntry *
     pCapwapIsSetFsCapwapWtpRebootStatisticsEntry, INT4 i4RowStatusLogic,
     INT4 i4RowCreateOption)
{
#if 0
    tCapwapFsCapwapWtpRebootStatisticsEntry
        * pCapwapFsCapwapWtpRebootStatisticsEntry = NULL;
#endif
    tCapwapFsCapwapWtpRebootStatisticsEntry
        * pCapwapOldFsCapwapWtpRebootStatisticsEntry = NULL;
    tCapwapFsCapwapWtpRebootStatisticsEntry
        * pCapwapTrgFsCapwapWtpRebootStatisticsEntry = NULL;
    tCapwapIsSetFsCapwapWtpRebootStatisticsEntry
        * pCapwapTrgIsSetFsCapwapWtpRebootStatisticsEntry = NULL;

    UNUSED_PARAM (i4RowStatusLogic);
    UNUSED_PARAM (i4RowCreateOption);

    pCapwapOldFsCapwapWtpRebootStatisticsEntry =
        (tCapwapFsCapwapWtpRebootStatisticsEntry *)
        MemAllocMemBlk (CAPWAP_FSCAPWAPWTPREBOOTSTATISTICSTABLE_POOLID);
    if (pCapwapOldFsCapwapWtpRebootStatisticsEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    pCapwapTrgFsCapwapWtpRebootStatisticsEntry =
        (tCapwapFsCapwapWtpRebootStatisticsEntry *)
        MemAllocMemBlk (CAPWAP_FSCAPWAPWTPREBOOTSTATISTICSTABLE_POOLID);
    if (pCapwapTrgFsCapwapWtpRebootStatisticsEntry == NULL)
    {
        MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPREBOOTSTATISTICSTABLE_POOLID,
                            (UINT1 *)
                            pCapwapOldFsCapwapWtpRebootStatisticsEntry);
        return OSIX_FAILURE;
    }
    pCapwapTrgIsSetFsCapwapWtpRebootStatisticsEntry =
        (tCapwapIsSetFsCapwapWtpRebootStatisticsEntry *)
        MemAllocMemBlk (CAPWAP_FSCAPWAPWTPREBOOTSTATISTICSTABLE_ISSET_POOLID);
    if (pCapwapTrgIsSetFsCapwapWtpRebootStatisticsEntry == NULL)
    {
        MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPREBOOTSTATISTICSTABLE_POOLID,
                            (UINT1 *)
                            pCapwapOldFsCapwapWtpRebootStatisticsEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPREBOOTSTATISTICSTABLE_POOLID,
                            (UINT1 *)
                            pCapwapTrgFsCapwapWtpRebootStatisticsEntry);
        return OSIX_FAILURE;
    }
    MEMSET (pCapwapOldFsCapwapWtpRebootStatisticsEntry, 0,
            sizeof (tCapwapFsCapwapWtpRebootStatisticsEntry));
    MEMSET (pCapwapTrgFsCapwapWtpRebootStatisticsEntry, 0,
            sizeof (tCapwapFsCapwapWtpRebootStatisticsEntry));
    MEMSET (pCapwapTrgIsSetFsCapwapWtpRebootStatisticsEntry, 0,
            sizeof (tCapwapIsSetFsCapwapWtpRebootStatisticsEntry));

#if 0
    /* Check whether the node is already present */
    pCapwapFsCapwapWtpRebootStatisticsEntry =
        RBTreeGet (gCapwapGlobals.CapwapGlbMib.FsCapwapWtpRebootStatisticsTable,
                   (tRBElem *) pCapwapSetFsCapwapWtpRebootStatisticsEntry);

    if (pCapwapFsCapwapWtpRebootStatisticsEntry == NULL)
    {
        /* Create the node if the RowStatus given is CREATE_AND_WAIT or CREATE_AND_GO */
        if ((pCapwapSetFsCapwapWtpRebootStatisticsEntry->MibObject.
             i4FsCapwapWtpRebootStatisticsRowStatus == CREATE_AND_WAIT)
            || (pCapwapSetFsCapwapWtpRebootStatisticsEntry->MibObject.
                i4FsCapwapWtpRebootStatisticsRowStatus == CREATE_AND_GO)
            ||
            ((pCapwapSetFsCapwapWtpRebootStatisticsEntry->MibObject.
              i4FsCapwapWtpRebootStatisticsRowStatus == ACTIVE)
             && (i4RowCreateOption == 1)))
        {
            /* Allocate memory for the new node */
            pCapwapFsCapwapWtpRebootStatisticsEntry =
                (tCapwapFsCapwapWtpRebootStatisticsEntry *)
                MemAllocMemBlk (CAPWAP_FSCAPWAPWTPREBOOTSTATISTICSTABLE_POOLID);
            if (pCapwapFsCapwapWtpRebootStatisticsEntry == NULL)
            {
                if (CapwapSetAllFsCapwapWtpRebootStatisticsTableTrigger
                    (pCapwapSetFsCapwapWtpRebootStatisticsEntry,
                     pCapwapIsSetFsCapwapWtpRebootStatisticsEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCapwapWtpRebootStatisticsTable:CapwapSetAllFsCapwapWtpRebootStatisticsTableTrigger function fails\r\n");

                }
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCapwapWtpRebootStatisticsTable: Fail to Allocate Memory.\r\n");
                MemReleaseMemBlock
                    (CAPWAP_FSCAPWAPWTPREBOOTSTATISTICSTABLE_POOLID,
                     (UINT1 *) pCapwapOldFsCapwapWtpRebootStatisticsEntry);
                MemReleaseMemBlock
                    (CAPWAP_FSCAPWAPWTPREBOOTSTATISTICSTABLE_POOLID,
                     (UINT1 *) pCapwapTrgFsCapwapWtpRebootStatisticsEntry);
                MemReleaseMemBlock
                    (CAPWAP_FSCAPWAPWTPREBOOTSTATISTICSTABLE_ISSET_POOLID,
                     (UINT1 *) pCapwapTrgIsSetFsCapwapWtpRebootStatisticsEntry);
                return OSIX_FAILURE;
            }

            MEMSET (pCapwapFsCapwapWtpRebootStatisticsEntry, 0,
                    sizeof (tCapwapFsCapwapWtpRebootStatisticsEntry));
            if ((CapwapInitializeFsCapwapWtpRebootStatisticsTable
                 (pCapwapFsCapwapWtpRebootStatisticsEntry)) == OSIX_FAILURE)
            {
                if (CapwapSetAllFsCapwapWtpRebootStatisticsTableTrigger
                    (pCapwapSetFsCapwapWtpRebootStatisticsEntry,
                     pCapwapIsSetFsCapwapWtpRebootStatisticsEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCapwapWtpRebootStatisticsTable:CapwapSetAllFsCapwapWtpRebootStatisticsTableTrigger function fails\r\n");

                }
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCapwapWtpRebootStatisticsTable: Fail to Initialize the Objects.\r\n");

                MemReleaseMemBlock
                    (CAPWAP_FSCAPWAPWTPREBOOTSTATISTICSTABLE_POOLID,
                     (UINT1 *) pCapwapFsCapwapWtpRebootStatisticsEntry);
                MemReleaseMemBlock
                    (CAPWAP_FSCAPWAPWTPREBOOTSTATISTICSTABLE_POOLID,
                     (UINT1 *) pCapwapOldFsCapwapWtpRebootStatisticsEntry);
                MemReleaseMemBlock
                    (CAPWAP_FSCAPWAPWTPREBOOTSTATISTICSTABLE_POOLID,
                     (UINT1 *) pCapwapTrgFsCapwapWtpRebootStatisticsEntry);
                MemReleaseMemBlock
                    (CAPWAP_FSCAPWAPWTPREBOOTSTATISTICSTABLE_ISSET_POOLID,
                     (UINT1 *) pCapwapTrgIsSetFsCapwapWtpRebootStatisticsEntry);
                return OSIX_FAILURE;
            }
            /* Assign values for the new node */
            if (pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
                bFsCapwapWtpRebootStatisticsRebootCount != OSIX_FALSE)
            {
                pCapwapFsCapwapWtpRebootStatisticsEntry->MibObject.
                    u4FsCapwapWtpRebootStatisticsRebootCount =
                    pCapwapSetFsCapwapWtpRebootStatisticsEntry->MibObject.
                    u4FsCapwapWtpRebootStatisticsRebootCount;
            }

            if (pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
                bFsCapwapWtpRebootStatisticsAcInitiatedCount != OSIX_FALSE)
            {
                pCapwapFsCapwapWtpRebootStatisticsEntry->MibObject.
                    u4FsCapwapWtpRebootStatisticsAcInitiatedCount =
                    pCapwapSetFsCapwapWtpRebootStatisticsEntry->MibObject.
                    u4FsCapwapWtpRebootStatisticsAcInitiatedCount;
            }

            if (pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
                bFsCapwapWtpRebootStatisticsLinkFailureCount != OSIX_FALSE)
            {
                pCapwapFsCapwapWtpRebootStatisticsEntry->MibObject.
                    u4FsCapwapWtpRebootStatisticsLinkFailureCount =
                    pCapwapSetFsCapwapWtpRebootStatisticsEntry->MibObject.
                    u4FsCapwapWtpRebootStatisticsLinkFailureCount;
            }

            if (pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
                bFsCapwapWtpRebootStatisticsSwFailureCount != OSIX_FALSE)
            {
                pCapwapFsCapwapWtpRebootStatisticsEntry->MibObject.
                    u4FsCapwapWtpRebootStatisticsSwFailureCount =
                    pCapwapSetFsCapwapWtpRebootStatisticsEntry->MibObject.
                    u4FsCapwapWtpRebootStatisticsSwFailureCount;
            }

            if (pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
                bFsCapwapWtpRebootStatisticsHwFailureCount != OSIX_FALSE)
            {
                pCapwapFsCapwapWtpRebootStatisticsEntry->MibObject.
                    u4FsCapwapWtpRebootStatisticsHwFailureCount =
                    pCapwapSetFsCapwapWtpRebootStatisticsEntry->MibObject.
                    u4FsCapwapWtpRebootStatisticsHwFailureCount;
            }

            if (pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
                bFsCapwapWtpRebootStatisticsOtherFailureCount != OSIX_FALSE)
            {
                pCapwapFsCapwapWtpRebootStatisticsEntry->MibObject.
                    u4FsCapwapWtpRebootStatisticsOtherFailureCount =
                    pCapwapSetFsCapwapWtpRebootStatisticsEntry->MibObject.
                    u4FsCapwapWtpRebootStatisticsOtherFailureCount;
            }

            if (pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
                bFsCapwapWtpRebootStatisticsUnknownFailureCount != OSIX_FALSE)
            {
                pCapwapFsCapwapWtpRebootStatisticsEntry->MibObject.
                    u4FsCapwapWtpRebootStatisticsUnknownFailureCount =
                    pCapwapSetFsCapwapWtpRebootStatisticsEntry->MibObject.
                    u4FsCapwapWtpRebootStatisticsUnknownFailureCount;
            }

            if (pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
                bFsCapwapWtpRebootStatisticsLastFailureType != OSIX_FALSE)
            {
                pCapwapFsCapwapWtpRebootStatisticsEntry->MibObject.
                    i4FsCapwapWtpRebootStatisticsLastFailureType =
                    pCapwapSetFsCapwapWtpRebootStatisticsEntry->MibObject.
                    i4FsCapwapWtpRebootStatisticsLastFailureType;
            }

            if (pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
                bFsCapwapWtpRebootStatisticsRowStatus != OSIX_FALSE)
            {
                pCapwapFsCapwapWtpRebootStatisticsEntry->MibObject.
                    i4FsCapwapWtpRebootStatisticsRowStatus =
                    pCapwapSetFsCapwapWtpRebootStatisticsEntry->MibObject.
                    i4FsCapwapWtpRebootStatisticsRowStatus;
            }

            if (pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
                bCapwapBaseWtpProfileId != OSIX_FALSE)
            {
                pCapwapFsCapwapWtpRebootStatisticsEntry->MibObject.
                    u4CapwapBaseWtpProfileId =
                    pCapwapSetFsCapwapWtpRebootStatisticsEntry->MibObject.
                    u4CapwapBaseWtpProfileId;
            }

            if ((pCapwapSetFsCapwapWtpRebootStatisticsEntry->MibObject.
                 i4FsCapwapWtpRebootStatisticsRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pCapwapSetFsCapwapWtpRebootStatisticsEntry->MibObject.
                        i4FsCapwapWtpRebootStatisticsRowStatus == ACTIVE)))
            {
                pCapwapFsCapwapWtpRebootStatisticsEntry->MibObject.
                    i4FsCapwapWtpRebootStatisticsRowStatus = ACTIVE;
            }
            else if (pCapwapSetFsCapwapWtpRebootStatisticsEntry->MibObject.
                     i4FsCapwapWtpRebootStatisticsRowStatus == CREATE_AND_WAIT)
            {
                pCapwapFsCapwapWtpRebootStatisticsEntry->MibObject.
                    i4FsCapwapWtpRebootStatisticsRowStatus = NOT_READY;
            }

            /* Add the new node to the database */
            if (RBTreeAdd
                (gCapwapGlobals.CapwapGlbMib.FsCapwapWtpRebootStatisticsTable,
                 (tRBElem *) pCapwapFsCapwapWtpRebootStatisticsEntry) !=
                RB_SUCCESS)
            {
                if (CapwapSetAllFsCapwapWtpRebootStatisticsTableTrigger
                    (pCapwapSetFsCapwapWtpRebootStatisticsEntry,
                     pCapwapIsSetFsCapwapWtpRebootStatisticsEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCapwapWtpRebootStatisticsTable: CapwapSetAllFsCapwapWtpRebootStatisticsTableTrigger function returns failure.\r\n");
                }
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCapwapWtpRebootStatisticsTable: RBTreeAdd is failed.\r\n");

                MemReleaseMemBlock
                    (CAPWAP_FSCAPWAPWTPREBOOTSTATISTICSTABLE_POOLID,
                     (UINT1 *) pCapwapFsCapwapWtpRebootStatisticsEntry);
                MemReleaseMemBlock
                    (CAPWAP_FSCAPWAPWTPREBOOTSTATISTICSTABLE_POOLID,
                     (UINT1 *) pCapwapOldFsCapwapWtpRebootStatisticsEntry);
                MemReleaseMemBlock
                    (CAPWAP_FSCAPWAPWTPREBOOTSTATISTICSTABLE_POOLID,
                     (UINT1 *) pCapwapTrgFsCapwapWtpRebootStatisticsEntry);
                MemReleaseMemBlock
                    (CAPWAP_FSCAPWAPWTPREBOOTSTATISTICSTABLE_ISSET_POOLID,
                     (UINT1 *) pCapwapTrgIsSetFsCapwapWtpRebootStatisticsEntry);
                return OSIX_FAILURE;
            }
            if (CapwapUtilUpdateFsCapwapWtpRebootStatisticsTable
                (NULL, pCapwapFsCapwapWtpRebootStatisticsEntry,
                 pCapwapIsSetFsCapwapWtpRebootStatisticsEntry) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCapwapWtpRebootStatisticsTable: CapwapUtilUpdateFsCapwapWtpRebootStatisticsTable function returns failure.\r\n");

                if (CapwapSetAllFsCapwapWtpRebootStatisticsTableTrigger
                    (pCapwapSetFsCapwapWtpRebootStatisticsEntry,
                     pCapwapIsSetFsCapwapWtpRebootStatisticsEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCapwapWtpRebootStatisticsTable: CapwapSetAllFsCapwapWtpRebootStatisticsTableTrigger function returns failure.\r\n");

                }
                RBTreeRem (gCapwapGlobals.CapwapGlbMib.
                           FsCapwapWtpRebootStatisticsTable,
                           pCapwapFsCapwapWtpRebootStatisticsEntry);
                MemReleaseMemBlock
                    (CAPWAP_FSCAPWAPWTPREBOOTSTATISTICSTABLE_POOLID,
                     (UINT1 *) pCapwapFsCapwapWtpRebootStatisticsEntry);
                MemReleaseMemBlock
                    (CAPWAP_FSCAPWAPWTPREBOOTSTATISTICSTABLE_POOLID,
                     (UINT1 *) pCapwapOldFsCapwapWtpRebootStatisticsEntry);
                MemReleaseMemBlock
                    (CAPWAP_FSCAPWAPWTPREBOOTSTATISTICSTABLE_POOLID,
                     (UINT1 *) pCapwapTrgFsCapwapWtpRebootStatisticsEntry);
                MemReleaseMemBlock
                    (CAPWAP_FSCAPWAPWTPREBOOTSTATISTICSTABLE_ISSET_POOLID,
                     (UINT1 *) pCapwapTrgIsSetFsCapwapWtpRebootStatisticsEntry);
                return OSIX_FAILURE;
            }

            if ((pCapwapSetFsCapwapWtpRebootStatisticsEntry->MibObject.
                 i4FsCapwapWtpRebootStatisticsRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pCapwapSetFsCapwapWtpRebootStatisticsEntry->MibObject.
                        i4FsCapwapWtpRebootStatisticsRowStatus == ACTIVE)))
            {

                /* For MSR and RM Trigger */
                pCapwapTrgFsCapwapWtpRebootStatisticsEntry->MibObject.
                    u4CapwapBaseWtpProfileId =
                    pCapwapSetFsCapwapWtpRebootStatisticsEntry->MibObject.
                    u4CapwapBaseWtpProfileId;
                pCapwapTrgFsCapwapWtpRebootStatisticsEntry->MibObject.
                    i4FsCapwapWtpRebootStatisticsRowStatus = CREATE_AND_WAIT;
                pCapwapTrgIsSetFsCapwapWtpRebootStatisticsEntry->
                    bFsCapwapWtpRebootStatisticsRowStatus = OSIX_TRUE;

                if (CapwapSetAllFsCapwapWtpRebootStatisticsTableTrigger
                    (pCapwapTrgFsCapwapWtpRebootStatisticsEntry,
                     pCapwapTrgIsSetFsCapwapWtpRebootStatisticsEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCapwapWtpRebootStatisticsTable: CapwapSetAllFsCapwapWtpRebootStatisticsTableTrigger function returns failure.\r\n");

                    MemReleaseMemBlock
                        (CAPWAP_FSCAPWAPWTPREBOOTSTATISTICSTABLE_POOLID,
                         (UINT1 *) pCapwapFsCapwapWtpRebootStatisticsEntry);
                    MemReleaseMemBlock
                        (CAPWAP_FSCAPWAPWTPREBOOTSTATISTICSTABLE_POOLID,
                         (UINT1 *) pCapwapOldFsCapwapWtpRebootStatisticsEntry);
                    MemReleaseMemBlock
                        (CAPWAP_FSCAPWAPWTPREBOOTSTATISTICSTABLE_POOLID,
                         (UINT1 *) pCapwapTrgFsCapwapWtpRebootStatisticsEntry);
                    MemReleaseMemBlock
                        (CAPWAP_FSCAPWAPWTPREBOOTSTATISTICSTABLE_ISSET_POOLID,
                         (UINT1 *)
                         pCapwapTrgIsSetFsCapwapWtpRebootStatisticsEntry);
                    return OSIX_FAILURE;
                }
            }
            else if (pCapwapSetFsCapwapWtpRebootStatisticsEntry->MibObject.
                     i4FsCapwapWtpRebootStatisticsRowStatus == CREATE_AND_WAIT)
            {

                /* For MSR and RM Trigger */
                pCapwapTrgFsCapwapWtpRebootStatisticsEntry->MibObject.
                    i4FsCapwapWtpRebootStatisticsRowStatus = CREATE_AND_WAIT;
                pCapwapTrgIsSetFsCapwapWtpRebootStatisticsEntry->
                    bFsCapwapWtpRebootStatisticsRowStatus = OSIX_TRUE;

                if (CapwapSetAllFsCapwapWtpRebootStatisticsTableTrigger
                    (pCapwapTrgFsCapwapWtpRebootStatisticsEntry,
                     pCapwapTrgIsSetFsCapwapWtpRebootStatisticsEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                "CapwapSetAllFsCapwapWtpRebootStatisticsTable: CapwapSetAllFsCapwapWtpRebootStatisticsTableTrigger function returns failure.\r\n");

                    MemReleaseMemBlock
                        (CAPWAP_FSCAPWAPWTPREBOOTSTATISTICSTABLE_POOLID,
                         (UINT1 *) pCapwapFsCapwapWtpRebootStatisticsEntry);
                    MemReleaseMemBlock
                        (CAPWAP_FSCAPWAPWTPREBOOTSTATISTICSTABLE_POOLID,
                         (UINT1 *) pCapwapOldFsCapwapWtpRebootStatisticsEntry);
                    MemReleaseMemBlock
                        (CAPWAP_FSCAPWAPWTPREBOOTSTATISTICSTABLE_POOLID,
                         (UINT1 *) pCapwapTrgFsCapwapWtpRebootStatisticsEntry);
                    MemReleaseMemBlock
                        (CAPWAP_FSCAPWAPWTPREBOOTSTATISTICSTABLE_ISSET_POOLID,
                         (UINT1 *)
                         pCapwapTrgIsSetFsCapwapWtpRebootStatisticsEntry);
                    return OSIX_FAILURE;
                }
            }

            if (pCapwapSetFsCapwapWtpRebootStatisticsEntry->MibObject.
                i4FsCapwapWtpRebootStatisticsRowStatus == CREATE_AND_GO)
            {
                pCapwapSetFsCapwapWtpRebootStatisticsEntry->MibObject.
                    i4FsCapwapWtpRebootStatisticsRowStatus = ACTIVE;
            }

            if (CapwapSetAllFsCapwapWtpRebootStatisticsTableTrigger
                (pCapwapSetFsCapwapWtpRebootStatisticsEntry,
                 pCapwapIsSetFsCapwapWtpRebootStatisticsEntry,
                 SNMP_SUCCESS) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCapwapWtpRebootStatisticsTable:  CapwapSetAllFsCapwapWtpRebootStatisticsTableTrigger function returns failure.\r\n");
            }
            MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPREBOOTSTATISTICSTABLE_POOLID,
                                (UINT1 *)
                                pCapwapOldFsCapwapWtpRebootStatisticsEntry);
            MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPREBOOTSTATISTICSTABLE_POOLID,
                                (UINT1 *)
                                pCapwapTrgFsCapwapWtpRebootStatisticsEntry);
            MemReleaseMemBlock
                (CAPWAP_FSCAPWAPWTPREBOOTSTATISTICSTABLE_ISSET_POOLID,
                 (UINT1 *) pCapwapTrgIsSetFsCapwapWtpRebootStatisticsEntry);
            return OSIX_SUCCESS;

        }
        else
        {
            if (CapwapSetAllFsCapwapWtpRebootStatisticsTableTrigger
                (pCapwapSetFsCapwapWtpRebootStatisticsEntry,
                 pCapwapIsSetFsCapwapWtpRebootStatisticsEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCapwapWtpRebootStatisticsTable: CapwapSetAllFsCapwapWtpRebootStatisticsTableTrigger function returns failure.\r\n");
            }
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCapwapWtpRebootStatisticsTable: Failure.\r\n");
            MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPREBOOTSTATISTICSTABLE_POOLID,
                                (UINT1 *)
                                pCapwapOldFsCapwapWtpRebootStatisticsEntry);
            MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPREBOOTSTATISTICSTABLE_POOLID,
                                (UINT1 *)
                                pCapwapTrgFsCapwapWtpRebootStatisticsEntry);
            MemReleaseMemBlock
                (CAPWAP_FSCAPWAPWTPREBOOTSTATISTICSTABLE_ISSET_POOLID,
                 (UINT1 *) pCapwapTrgIsSetFsCapwapWtpRebootStatisticsEntry);
            return OSIX_FAILURE;
        }
    }
    else if ((pCapwapSetFsCapwapWtpRebootStatisticsEntry->MibObject.
              i4FsCapwapWtpRebootStatisticsRowStatus == CREATE_AND_WAIT)
             || (pCapwapSetFsCapwapWtpRebootStatisticsEntry->MibObject.
                 i4FsCapwapWtpRebootStatisticsRowStatus == CREATE_AND_GO))
    {
        if (CapwapSetAllFsCapwapWtpRebootStatisticsTableTrigger
            (pCapwapSetFsCapwapWtpRebootStatisticsEntry,
             pCapwapIsSetFsCapwapWtpRebootStatisticsEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)

        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCapwapWtpRebootStatisticsTable: CapwapSetAllFsCapwapWtpRebootStatisticsTableTrigger function returns failure.\r\n");
        }
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapSetAllFsCapwapWtpRebootStatisticsTable: The row is already present.\r\n");
        MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPREBOOTSTATISTICSTABLE_POOLID,
                            (UINT1 *)
                            pCapwapOldFsCapwapWtpRebootStatisticsEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPREBOOTSTATISTICSTABLE_POOLID,
                            (UINT1 *)
                            pCapwapTrgFsCapwapWtpRebootStatisticsEntry);
        MemReleaseMemBlock
            (CAPWAP_FSCAPWAPWTPREBOOTSTATISTICSTABLE_ISSET_POOLID,
             (UINT1 *) pCapwapTrgIsSetFsCapwapWtpRebootStatisticsEntry);
        return OSIX_FAILURE;
    }
    /* Copy the previous values before setting the new values */
    MEMCPY (pCapwapOldFsCapwapWtpRebootStatisticsEntry,
            pCapwapFsCapwapWtpRebootStatisticsEntry,
            sizeof (tCapwapFsCapwapWtpRebootStatisticsEntry));

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pCapwapSetFsCapwapWtpRebootStatisticsEntry->MibObject.
        i4FsCapwapWtpRebootStatisticsRowStatus == DESTROY)
    {
        pCapwapFsCapwapWtpRebootStatisticsEntry->MibObject.
            i4FsCapwapWtpRebootStatisticsRowStatus = DESTROY;

        if (CapwapUtilUpdateFsCapwapWtpRebootStatisticsTable
            (pCapwapOldFsCapwapWtpRebootStatisticsEntry,
             pCapwapFsCapwapWtpRebootStatisticsEntry,
             pCapwapIsSetFsCapwapWtpRebootStatisticsEntry) != OSIX_SUCCESS)
        {

            if (CapwapSetAllFsCapwapWtpRebootStatisticsTableTrigger
                (pCapwapSetFsCapwapWtpRebootStatisticsEntry,
                 pCapwapIsSetFsCapwapWtpRebootStatisticsEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCapwapWtpRebootStatisticsTable: CapwapSetAllFsCapwapWtpRebootStatisticsTableTrigger function returns failure.\r\n");
            }
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCapwapWtpRebootStatisticsTable: CapwapUtilUpdateFsCapwapWtpRebootStatisticsTable function returns failure.\r\n");
        }
        RBTreeRem (gCapwapGlobals.CapwapGlbMib.FsCapwapWtpRebootStatisticsTable,
                   pCapwapFsCapwapWtpRebootStatisticsEntry);
        if (CapwapSetAllFsCapwapWtpRebootStatisticsTableTrigger
            (pCapwapSetFsCapwapWtpRebootStatisticsEntry,
             pCapwapIsSetFsCapwapWtpRebootStatisticsEntry,
             SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCapwapWtpRebootStatisticsTable: CapwapSetAllFsCapwapWtpRebootStatisticsTableTrigger function returns failure.\r\n");
            MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPREBOOTSTATISTICSTABLE_POOLID,
                                (UINT1 *)
                                pCapwapOldFsCapwapWtpRebootStatisticsEntry);
            MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPREBOOTSTATISTICSTABLE_POOLID,
                                (UINT1 *)
                                pCapwapTrgFsCapwapWtpRebootStatisticsEntry);
            MemReleaseMemBlock
                (CAPWAP_FSCAPWAPWTPREBOOTSTATISTICSTABLE_ISSET_POOLID,
                 (UINT1 *) pCapwapTrgIsSetFsCapwapWtpRebootStatisticsEntry);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPREBOOTSTATISTICSTABLE_POOLID,
                            (UINT1 *) pCapwapFsCapwapWtpRebootStatisticsEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPREBOOTSTATISTICSTABLE_POOLID,
                            (UINT1 *)
                            pCapwapOldFsCapwapWtpRebootStatisticsEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPREBOOTSTATISTICSTABLE_POOLID,
                            (UINT1 *)
                            pCapwapTrgFsCapwapWtpRebootStatisticsEntry);
        MemReleaseMemBlock
            (CAPWAP_FSCAPWAPWTPREBOOTSTATISTICSTABLE_ISSET_POOLID,
             (UINT1 *) pCapwapTrgIsSetFsCapwapWtpRebootStatisticsEntry);
        return OSIX_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (FsCapwapWtpRebootStatisticsTableFilterInputs
        (pCapwapFsCapwapWtpRebootStatisticsEntry,
         pCapwapSetFsCapwapWtpRebootStatisticsEntry,
         pCapwapIsSetFsCapwapWtpRebootStatisticsEntry) != OSIX_TRUE)
    {
        MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPREBOOTSTATISTICSTABLE_POOLID,
                            (UINT1 *)
                            pCapwapOldFsCapwapWtpRebootStatisticsEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPREBOOTSTATISTICSTABLE_POOLID,
                            (UINT1 *)
                            pCapwapTrgFsCapwapWtpRebootStatisticsEntry);
        MemReleaseMemBlock
            (CAPWAP_FSCAPWAPWTPREBOOTSTATISTICSTABLE_ISSET_POOLID,
             (UINT1 *) pCapwapTrgIsSetFsCapwapWtpRebootStatisticsEntry);
        return OSIX_SUCCESS;
    }

    /*This condtion is to make the row NOT_IN_SERVICE before modifying the values */
    if ((i4RowStatusLogic == TRUE) &&
        (pCapwapFsCapwapWtpRebootStatisticsEntry->MibObject.
         i4FsCapwapWtpRebootStatisticsRowStatus == ACTIVE)
        && (pCapwapSetFsCapwapWtpRebootStatisticsEntry->MibObject.
            i4FsCapwapWtpRebootStatisticsRowStatus != NOT_IN_SERVICE))
    {
        pCapwapFsCapwapWtpRebootStatisticsEntry->MibObject.
            i4FsCapwapWtpRebootStatisticsRowStatus = NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        pCapwapTrgFsCapwapWtpRebootStatisticsEntry->MibObject.
            i4FsCapwapWtpRebootStatisticsRowStatus = NOT_IN_SERVICE;
        pCapwapTrgIsSetFsCapwapWtpRebootStatisticsEntry->
            bFsCapwapWtpRebootStatisticsRowStatus = OSIX_TRUE;

        if (CapwapUtilUpdateFsCapwapWtpRebootStatisticsTable
            (pCapwapOldFsCapwapWtpRebootStatisticsEntry,
             pCapwapFsCapwapWtpRebootStatisticsEntry,
             pCapwapIsSetFsCapwapWtpRebootStatisticsEntry) != OSIX_SUCCESS)
        {
            /*Restore back with previous values */
            MEMCPY (pCapwapFsCapwapWtpRebootStatisticsEntry,
                    pCapwapOldFsCapwapWtpRebootStatisticsEntry,
                    sizeof (tCapwapFsCapwapWtpRebootStatisticsEntry));
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCapwapWtpRebootStatisticsTable:                 CapwapUtilUpdateFsCapwapWtpRebootStatisticsTable Function returns failure.\r\n");

            if (CapwapSetAllFsCapwapWtpRebootStatisticsTableTrigger
                (pCapwapSetFsCapwapWtpRebootStatisticsEntry,
                 pCapwapIsSetFsCapwapWtpRebootStatisticsEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                CAPWAP_TRC (CAPWAP_UTIL_TRC,
                            "CapwapSetAllFsCapwapWtpRebootStatisticsTable: CapwapSetAllFsCapwapWtpRebootStatisticsTableTrigger function returns failure.\r\n");
            }
            MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPREBOOTSTATISTICSTABLE_POOLID,
                                (UINT1 *)
                                pCapwapOldFsCapwapWtpRebootStatisticsEntry);
            MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPREBOOTSTATISTICSTABLE_POOLID,
                                (UINT1 *)
                                pCapwapTrgFsCapwapWtpRebootStatisticsEntry);
            MemReleaseMemBlock
                (CAPWAP_FSCAPWAPWTPREBOOTSTATISTICSTABLE_ISSET_POOLID,
                 (UINT1 *) pCapwapTrgIsSetFsCapwapWtpRebootStatisticsEntry);
            return OSIX_FAILURE;
        }

        if (CapwapSetAllFsCapwapWtpRebootStatisticsTableTrigger
            (pCapwapTrgFsCapwapWtpRebootStatisticsEntry,
             pCapwapTrgIsSetFsCapwapWtpRebootStatisticsEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCapwapWtpRebootStatisticsTable: CapwapSetAllFsCapwapWtpRebootStatisticsTableTrigger function returns failure.\r\n");
        }
    }

    if (pCapwapSetFsCapwapWtpRebootStatisticsEntry->MibObject.
        i4FsCapwapWtpRebootStatisticsRowStatus == ACTIVE)
    {
        i4RowMakeActive = TRUE;
    }

    /* Assign values for the existing node */
    if (pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
        bFsCapwapWtpRebootStatisticsRebootCount != OSIX_FALSE)
    {
        pCapwapFsCapwapWtpRebootStatisticsEntry->MibObject.
            u4FsCapwapWtpRebootStatisticsRebootCount =
            pCapwapSetFsCapwapWtpRebootStatisticsEntry->MibObject.
            u4FsCapwapWtpRebootStatisticsRebootCount;
    }
    if (pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
        bFsCapwapWtpRebootStatisticsAcInitiatedCount != OSIX_FALSE)
    {
        pCapwapFsCapwapWtpRebootStatisticsEntry->MibObject.
            u4FsCapwapWtpRebootStatisticsAcInitiatedCount =
            pCapwapSetFsCapwapWtpRebootStatisticsEntry->MibObject.
            u4FsCapwapWtpRebootStatisticsAcInitiatedCount;
    }
    if (pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
        bFsCapwapWtpRebootStatisticsLinkFailureCount != OSIX_FALSE)
    {
        pCapwapFsCapwapWtpRebootStatisticsEntry->MibObject.
            u4FsCapwapWtpRebootStatisticsLinkFailureCount =
            pCapwapSetFsCapwapWtpRebootStatisticsEntry->MibObject.
            u4FsCapwapWtpRebootStatisticsLinkFailureCount;
    }
    if (pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
        bFsCapwapWtpRebootStatisticsSwFailureCount != OSIX_FALSE)
    {
        pCapwapFsCapwapWtpRebootStatisticsEntry->MibObject.
            u4FsCapwapWtpRebootStatisticsSwFailureCount =
            pCapwapSetFsCapwapWtpRebootStatisticsEntry->MibObject.
            u4FsCapwapWtpRebootStatisticsSwFailureCount;
    }
    if (pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
        bFsCapwapWtpRebootStatisticsHwFailureCount != OSIX_FALSE)
    {
        pCapwapFsCapwapWtpRebootStatisticsEntry->MibObject.
            u4FsCapwapWtpRebootStatisticsHwFailureCount =
            pCapwapSetFsCapwapWtpRebootStatisticsEntry->MibObject.
            u4FsCapwapWtpRebootStatisticsHwFailureCount;
    }
    if (pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
        bFsCapwapWtpRebootStatisticsOtherFailureCount != OSIX_FALSE)
    {
        pCapwapFsCapwapWtpRebootStatisticsEntry->MibObject.
            u4FsCapwapWtpRebootStatisticsOtherFailureCount =
            pCapwapSetFsCapwapWtpRebootStatisticsEntry->MibObject.
            u4FsCapwapWtpRebootStatisticsOtherFailureCount;
    }
    if (pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
        bFsCapwapWtpRebootStatisticsUnknownFailureCount != OSIX_FALSE)
    {
        pCapwapFsCapwapWtpRebootStatisticsEntry->MibObject.
            u4FsCapwapWtpRebootStatisticsUnknownFailureCount =
            pCapwapSetFsCapwapWtpRebootStatisticsEntry->MibObject.
            u4FsCapwapWtpRebootStatisticsUnknownFailureCount;
    }
    if (pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
        bFsCapwapWtpRebootStatisticsLastFailureType != OSIX_FALSE)
    {
        pCapwapFsCapwapWtpRebootStatisticsEntry->MibObject.
            i4FsCapwapWtpRebootStatisticsLastFailureType =
            pCapwapSetFsCapwapWtpRebootStatisticsEntry->MibObject.
            i4FsCapwapWtpRebootStatisticsLastFailureType;
    }
    if (pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
        bFsCapwapWtpRebootStatisticsRowStatus != OSIX_FALSE)
    {
        pCapwapFsCapwapWtpRebootStatisticsEntry->MibObject.
            i4FsCapwapWtpRebootStatisticsRowStatus =
            pCapwapSetFsCapwapWtpRebootStatisticsEntry->MibObject.
            i4FsCapwapWtpRebootStatisticsRowStatus;
    }

    /*This condtion is to make the row back to ACTIVE after modifying the values */
    if (i4RowMakeActive == TRUE)
    {
        pCapwapFsCapwapWtpRebootStatisticsEntry->MibObject.
            i4FsCapwapWtpRebootStatisticsRowStatus = ACTIVE;
    }
#endif
    if (CapwapUtilUpdateFsCapwapWtpRebootStatisticsTable
        (pCapwapOldFsCapwapWtpRebootStatisticsEntry,
         pCapwapSetFsCapwapWtpRebootStatisticsEntry,
         pCapwapIsSetFsCapwapWtpRebootStatisticsEntry) != OSIX_SUCCESS)
    {
#if 0
        if (CapwapSetAllFsCapwapWtpRebootStatisticsTableTrigger
            (pCapwapSetFsCapwapWtpRebootStatisticsEntry,
             pCapwapIsSetFsCapwapWtpRebootStatisticsEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)

        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapSetAllFsCapwapWtpRebootStatisticsTable: CapwapSetAllFsCapwapWtpRebootStatisticsTableTrigger function returns failure.\r\n");

        }
        /*Restore back with previous values */
        MEMCPY (pCapwapFsCapwapWtpRebootStatisticsEntry,
                pCapwapOldFsCapwapWtpRebootStatisticsEntry,
                sizeof (tCapwapFsCapwapWtpRebootStatisticsEntry));
#endif
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapSetAllFsCapwapWtpRebootStatisticsTable: CapwapUtilUpdateFsCapwapWtpRebootStatisticsTable function returns failure.\r\n");
        MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPREBOOTSTATISTICSTABLE_POOLID,
                            (UINT1 *)
                            pCapwapOldFsCapwapWtpRebootStatisticsEntry);
        MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPREBOOTSTATISTICSTABLE_POOLID,
                            (UINT1 *)
                            pCapwapTrgFsCapwapWtpRebootStatisticsEntry);
        MemReleaseMemBlock
            (CAPWAP_FSCAPWAPWTPREBOOTSTATISTICSTABLE_ISSET_POOLID,
             (UINT1 *) pCapwapTrgIsSetFsCapwapWtpRebootStatisticsEntry);
        return OSIX_FAILURE;

    }
#if 0
    if (CapwapSetAllFsCapwapWtpRebootStatisticsTableTrigger
        (pCapwapSetFsCapwapWtpRebootStatisticsEntry,
         pCapwapIsSetFsCapwapWtpRebootStatisticsEntry,
         SNMP_SUCCESS) != OSIX_SUCCESS)
    {

        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapSetAllFsCapwapWtpRebootStatisticsTable: CapwapSetAllFsCapwapWtpRebootStatisticsTableTrigger function returns failure.\r\n");
    }
#endif

    MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPREBOOTSTATISTICSTABLE_POOLID,
                        (UINT1 *) pCapwapOldFsCapwapWtpRebootStatisticsEntry);
    MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPREBOOTSTATISTICSTABLE_POOLID,
                        (UINT1 *) pCapwapTrgFsCapwapWtpRebootStatisticsEntry);
    MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPREBOOTSTATISTICSTABLE_ISSET_POOLID,
                        (UINT1 *)
                        pCapwapTrgIsSetFsCapwapWtpRebootStatisticsEntry);
    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  CapwapSetAllFsCapwapWtpRadioStatisticsTable
 Input       :  pCapwapSetFsCapwapWtpRadioStatisticsEntry
                pCapwapIsSetFsCapwapWtpRadioStatisticsEntry
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    CapwapSetAllFsCapwapWtpRadioStatisticsTable
    (tCapwapFsCapwapWtpRadioStatisticsEntry *
     pCapwapSetFsCapwapWtpRadioStatisticsEntry,
     tCapwapIsSetFsCapwapWtpRadioStatisticsEntry *
     pCapwapIsSetFsCapwapWtpRadioStatisticsEntry)
{
#if 0
    tCapwapFsCapwapWtpRadioStatisticsEntry
        * pCapwapFsCapwapWtpRadioStatisticsEntry = NULL;
#endif
    tCapwapFsCapwapWtpRadioStatisticsEntry
        CapwapOldFsCapwapWtpRadioStatisticsEntry;

    MEMSET (&CapwapOldFsCapwapWtpRadioStatisticsEntry, 0,
            sizeof (tCapwapFsCapwapWtpRadioStatisticsEntry));

#if 0
    /* Check whether the node is already present */
    pCapwapFsCapwapWtpRadioStatisticsEntry =
        RBTreeGet (gCapwapGlobals.CapwapGlbMib.FsCapwapWtpRadioStatisticsTable,
                   (tRBElem *) pCapwapSetFsCapwapWtpRadioStatisticsEntry);

    if (pCapwapFsCapwapWtpRadioStatisticsEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapSetAllFsCapwapWtpRadioStatisticsTable: Entry doesn't exist\r\n");
        return OSIX_FAILURE;
    }

    /*Function to check whether the given input is same as there in database */
    if (FsCapwapWtpRadioStatisticsTableFilterInputs
        (pCapwapFsCapwapWtpRadioStatisticsEntry,
         pCapwapSetFsCapwapWtpRadioStatisticsEntry,
         pCapwapIsSetFsCapwapWtpRadioStatisticsEntry) != OSIX_TRUE)
    {
        return OSIX_SUCCESS;
    }

    /* Copy the previous values before setting the new values */
    MEMCPY (&CapwapOldFsCapwapWtpRadioStatisticsEntry,
            pCapwapFsCapwapWtpRadioStatisticsEntry,
            sizeof (tCapwapFsCapwapWtpRadioStatisticsEntry));

    /* Assign values for the existing node */
    if (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
        bFsCapwapWtpRadioLastFailType != OSIX_FALSE)
    {
        pCapwapFsCapwapWtpRadioStatisticsEntry->MibObject.
            i4FsCapwapWtpRadioLastFailType =
            pCapwapSetFsCapwapWtpRadioStatisticsEntry->MibObject.
            i4FsCapwapWtpRadioLastFailType;
    }
    if (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
        bFsCapwapWtpRadioResetCount != OSIX_FALSE)
    {
        pCapwapFsCapwapWtpRadioStatisticsEntry->MibObject.
            u4FsCapwapWtpRadioResetCount =
            pCapwapSetFsCapwapWtpRadioStatisticsEntry->MibObject.
            u4FsCapwapWtpRadioResetCount;
    }
    if (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
        bFsCapwapWtpRadioSwFailureCount != OSIX_FALSE)
    {
        pCapwapFsCapwapWtpRadioStatisticsEntry->MibObject.
            u4FsCapwapWtpRadioSwFailureCount =
            pCapwapSetFsCapwapWtpRadioStatisticsEntry->MibObject.
            u4FsCapwapWtpRadioSwFailureCount;
    }
    if (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
        bFsCapwapWtpRadioHwFailureCount != OSIX_FALSE)
    {
        pCapwapFsCapwapWtpRadioStatisticsEntry->MibObject.
            u4FsCapwapWtpRadioHwFailureCount =
            pCapwapSetFsCapwapWtpRadioStatisticsEntry->MibObject.
            u4FsCapwapWtpRadioHwFailureCount;
    }
    if (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
        bFsCapwapWtpRadioOtherFailureCount != OSIX_FALSE)
    {
        pCapwapFsCapwapWtpRadioStatisticsEntry->MibObject.
            u4FsCapwapWtpRadioOtherFailureCount =
            pCapwapSetFsCapwapWtpRadioStatisticsEntry->MibObject.
            u4FsCapwapWtpRadioOtherFailureCount;
    }
    if (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
        bFsCapwapWtpRadioUnknownFailureCount != OSIX_FALSE)
    {
        pCapwapFsCapwapWtpRadioStatisticsEntry->MibObject.
            u4FsCapwapWtpRadioUnknownFailureCount =
            pCapwapSetFsCapwapWtpRadioStatisticsEntry->MibObject.
            u4FsCapwapWtpRadioUnknownFailureCount;
    }
    if (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
        bFsCapwapWtpRadioConfigUpdateCount != OSIX_FALSE)
    {
        pCapwapFsCapwapWtpRadioStatisticsEntry->MibObject.
            u4FsCapwapWtpRadioConfigUpdateCount =
            pCapwapSetFsCapwapWtpRadioStatisticsEntry->MibObject.
            u4FsCapwapWtpRadioConfigUpdateCount;
    }
    if (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
        bFsCapwapWtpRadioChannelChangeCount != OSIX_FALSE)
    {
        pCapwapFsCapwapWtpRadioStatisticsEntry->MibObject.
            u4FsCapwapWtpRadioChannelChangeCount =
            pCapwapSetFsCapwapWtpRadioStatisticsEntry->MibObject.
            u4FsCapwapWtpRadioChannelChangeCount;
    }
    if (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
        bFsCapwapWtpRadioBandChangeCount != OSIX_FALSE)
    {
        pCapwapFsCapwapWtpRadioStatisticsEntry->MibObject.
            u4FsCapwapWtpRadioBandChangeCount =
            pCapwapSetFsCapwapWtpRadioStatisticsEntry->MibObject.
            u4FsCapwapWtpRadioBandChangeCount;
    }
    if (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
        bFsCapwapWtpRadioCurrentNoiseFloor != OSIX_FALSE)
    {
        pCapwapFsCapwapWtpRadioStatisticsEntry->MibObject.
            u4FsCapwapWtpRadioCurrentNoiseFloor =
            pCapwapSetFsCapwapWtpRadioStatisticsEntry->MibObject.
            u4FsCapwapWtpRadioCurrentNoiseFloor;
    }
    if (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
        bFsCapwapWtpRadioStatRowStatus != OSIX_FALSE)
    {
        pCapwapFsCapwapWtpRadioStatisticsEntry->MibObject.
            u4FsCapwapWtpRadioStatRowStatus =
            pCapwapSetFsCapwapWtpRadioStatisticsEntry->MibObject.
            u4FsCapwapWtpRadioStatRowStatus;
    }
#endif

    if (CapwapUtilUpdateFsCapwapWtpRadioStatisticsTable
        (&CapwapOldFsCapwapWtpRadioStatisticsEntry,
         pCapwapSetFsCapwapWtpRadioStatisticsEntry,
         pCapwapIsSetFsCapwapWtpRadioStatisticsEntry) != OSIX_SUCCESS)
    {
#if 0
        if (CapwapSetAllFsCapwapWtpRadioStatisticsTableTrigger
            (pCapwapSetFsCapwapWtpRadioStatisticsEntry,
             pCapwapIsSetFsCapwapWtpRadioStatisticsEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)

        {
            return OSIX_FAILURE;
        }
        /*Restore back with previous values */
        MEMCPY (pCapwapFsCapwapWtpRadioStatisticsEntry,
                &CapwapOldFsCapwapWtpRadioStatisticsEntry,
                sizeof (tCapwapFsCapwapWtpRadioStatisticsEntry));
#endif
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapSetAllFsCapwapWtpRadioStatisticsTable: CapwapUtilUpdateFsCapwapWtpRadioStatisticsTable function returns failure.\r\n");
        return OSIX_FAILURE;
    }

#if 0
    if (CapwapSetAllFsCapwapWtpRadioStatisticsTableTrigger
        (pCapwapSetFsCapwapWtpRadioStatisticsEntry,
         pCapwapIsSetFsCapwapWtpRadioStatisticsEntry,
         SNMP_SUCCESS) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
#endif
    return OSIX_SUCCESS;
}
