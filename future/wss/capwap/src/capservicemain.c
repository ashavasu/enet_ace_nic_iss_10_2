/*****************************************************************************
 *
 * $Id: capservicemain.c,v 1.6 2017/12/08 10:16:24 siva Exp $       
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved                      *
 *                                                                           *
 * Description: This file contains the service thread main loop              *
 * implementaion.                                                            *
 *                                                                           *
 *****************************************************************************/
#ifndef __CAPWAPMAIN_C__
#define __CAPWAPMAIN_C__
#define _CAPWAP_GLOBAL_VAR

#include "capwapinc.h"
#include "wssifradiodb.h"
#include "wssifpmdb.h"
#include "wtpnvram.h"
#include "capwapcli.h"
#include "aphdlrinc.h"

tSerTaskGlobals     gSerTaskGlobals;
static UINT2        gIntProfileId;

static UINT1        gau1Frame[CAPWAP_MAX_HDR_LEN];
PUBLIC UINT4        gu4CapwapEnable;

/* CPU relinquish counter */
static UINT4        gu4CPUSerRelinquishCounter = 0;
extern BOOL1        gbCapwapImageData;
/* WTP used this strucutre to store all the discovery
 * response received from the different ACs in discovery phase */
tACInfoAfterDiscovery gACInfoAfterDiscovery[MAX_DISC_RESPONSE];

/* WTP use this structure to store all the Ac-ref Ip address */
tACRefInfoAfterDiscovery gACRefInfoAfterDiscovery[10];

#ifdef WTP_WANTED
extern tCapwapStaticAcDiscList gCapwapStaticDiscInfo[MAX_STATIC_DISC];
#endif

extern eState       gCapwapState;

#ifdef WTP_WANTED
static UINT1        gu1FirstKeepAlive = 0;
#endif

INT4
CapwapCreateWtpProfile (UINT2 u2ProfildId)
{
    UINT1               u1NumRadios, u1Index;
    tRadioIfMsgStruct   WssMsgStruct;
    tWssIfCapDB        *pWssIfCapDB = NULL;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT2               u2WtpProfileId;
    INT4                i4RowStatus = TRUE;
    INT4                i4RowCreateOption = OSIX_TRUE;

    tCapwapFsCapwapWirelessBindingEntry CapwapSetFsCapwapWirelessBindingEntry;
    tCapwapIsSetFsCapwapWirelessBindingEntry
        CapwapIsSetFsCapwapWirelessBindingEntry;

    tCapwapFsCapwapWtpConfigEntry CapwapSetFsCapwapWtpConfigEntry;
    tCapwapIsSetFsCapwapWtpConfigEntry CapwapIsSetFsCapwapWtpConfigEntry;
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapDB, OSIX_FAILURE)
        WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (&WssMsgStruct, 0, sizeof (tRadioIfMsgStruct));
    MEMSET (pWssIfCapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&CapwapSetFsCapwapWtpConfigEntry, 0,
            sizeof (tCapwapFsCapwapWtpConfigEntry));
    MEMSET (&CapwapIsSetFsCapwapWtpConfigEntry, 0,
            sizeof (tCapwapIsSetFsCapwapWtpConfigEntry));

    CAPWAP_FN_ENTRY ();
    pWssIfCapDB->CapwapIsGetAllDB.bWtpProfileId = OSIX_TRUE;
    pWssIfCapDB->CapwapGetDB.u2ProfileId = u2ProfildId;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_INTERNAL_ID,
                                 pWssIfCapDB) == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to Get Internal Id from CAPWAP DB \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to Get Internal Id from CAPWAP DB \r\n"));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
        return OSIX_FAILURE;
    }
    u2WtpProfileId = pWssIfCapDB->CapwapGetDB.u2WtpInternalId;
    pWssIfCapDB->CapwapIsGetAllDB.bWtpProfileId = OSIX_FALSE;
    /* For this Internal Id Read the Model Table */
    /* MEMSET (pWssIfCapDB, 0, sizeof(tWssIfCapDB)); */
    pWssIfCapDB->CapwapIsGetAllDB.bNoOfRadio = OSIX_TRUE;
    pWssIfCapDB->CapwapIsGetAllDB.bRadioAdminStatus = OSIX_TRUE;
    pWssIfCapDB->CapwapIsGetAllDB.bWtpRadioType = OSIX_TRUE;
    pWssIfCapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                 pWssIfCapDB) == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to Get Model Table CAPWAP DB \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to Get Model Table CAPWAP DB \r\n"));

        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
        return OSIX_FAILURE;
    }
    u1NumRadios = pWssIfCapDB->CapwapGetDB.u1WtpNoofRadio;
    for (u1Index = 0; u1Index < u1NumRadios; u1Index++)
    {
        WssMsgStruct.unRadioIfMsg.RadioIfVirtualIntfCreate.u1IfType =
            CFA_CAPWAP_VIRT_RADIO;
        WssMsgStruct.unRadioIfMsg.RadioIfVirtualIntfCreate.u1AdminStatus =
            pWssIfCapDB->CapwapGetDB.au1WtpRadioAdminStatus[u1Index];
        WssMsgStruct.unRadioIfMsg.RadioIfVirtualIntfCreate.u4Dot11RadioType =
            pWssIfCapDB->CapwapGetDB.au4WtpRadioType[u1Index];
        MEMCPY (WssMsgStruct.unRadioIfMsg.RadioIfVirtualIntfCreate.MacAddr,
                pWssIfCapDB->CapwapGetDB.WtpMacAddress, MAC_ADDR_LEN);
        WssMsgStruct.unRadioIfMsg.RadioIfVirtualIntfCreate.u2WtpInternalId =
            u2WtpProfileId;
        WssMsgStruct.unRadioIfMsg.RadioIfVirtualIntfCreate.u1RadioId =
            (UINT1) (u1Index + 1);
        WssMsgStruct.unRadioIfMsg.RadioIfVirtualIntfCreate.isPresent =
            OSIX_TRUE;

        if (WssIfProcessRadioIfMsg (WSS_RADIOIF_CREATE_VIRTUAL_RADIO_MSG,
                                    &WssMsgStruct) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to create Virtual Radio Interface \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to create Virtual Radio Interface \r\n"));

            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
            return OSIX_FAILURE;
        }
        pWssIfCapwapDB->CapwapGetDB.u4IfIndex =
            WssMsgStruct.unRadioIfMsg.RadioIfVirtualIntfCreate.u4IfIndex;
        pWssIfCapwapDB->CapwapGetDB.u1RadioId = (UINT1) (u1Index + 1);
        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2WtpProfileId;
        pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_SET_DB,
                                     pWssIfCapwapDB) == OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to SET CAPWAP DB \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to SET CAPWAP DB \r\n"));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
            return OSIX_FAILURE;
        }
        CapwapSetFsCapwapWirelessBindingEntry.MibObject.
            i4FsCapwapWirelessBindingRowStatus = CREATE_AND_GO;
        CapwapSetFsCapwapWirelessBindingEntry.MibObject.
            u4CapwapBaseWtpProfileId = u2ProfildId;
        CapwapSetFsCapwapWirelessBindingEntry.MibObject.
            u4CapwapBaseWirelessBindingRadioId =
            WssMsgStruct.unRadioIfMsg.RadioIfVirtualIntfCreate.u1RadioId;
        CapwapSetFsCapwapWirelessBindingEntry.MibObject.
            i4FsCapwapWirelessBindingVirtualRadioIfIndex =
            (INT4) (WssMsgStruct.unRadioIfMsg.RadioIfVirtualIntfCreate.
                    u4IfIndex);
        CapwapSetFsCapwapWirelessBindingEntry.MibObject.
            i4FsCapwapWirelessBindingType = CAPWAP_DOT11_BIND_TYPE;
        CapwapIsSetFsCapwapWirelessBindingEntry.
            bFsCapwapWirelessBindingVirtualRadioIfIndex = OSIX_TRUE;
        CapwapIsSetFsCapwapWirelessBindingEntry.bFsCapwapWirelessBindingType =
            OSIX_TRUE;
        CapwapIsSetFsCapwapWirelessBindingEntry.
            bFsCapwapWirelessBindingRowStatus = OSIX_TRUE;
        CapwapIsSetFsCapwapWirelessBindingEntry.bCapwapBaseWtpProfileId =
            OSIX_TRUE;
        CapwapIsSetFsCapwapWirelessBindingEntry.
            bCapwapBaseWirelessBindingRadioId = OSIX_TRUE;

#ifdef CLI_WANTED
        if (CapwapGetAllFsCapwapWirelessBindingTable
            (&CapwapSetFsCapwapWirelessBindingEntry) != OSIX_SUCCESS)
        {
            CapwapCliSetFsCapwapWirelessBindingTable (0,
                                                      &CapwapSetFsCapwapWirelessBindingEntry,
                                                      &CapwapIsSetFsCapwapWirelessBindingEntry);
        }
#endif
    }

    CapwapSetFsCapwapWtpConfigEntry.MibObject.u4CapwapBaseWtpProfileId =
        u2ProfildId;
    MEMCPY (CapwapSetFsCapwapWtpConfigEntry.MibObject.au1FsWtpCountryString,
            "US", 3);
    CapwapSetFsCapwapWtpConfigEntry.MibObject.i4FsWtpCountryStringLen = 3;
    CapwapSetFsCapwapWtpConfigEntry.MibObject.i4FsCapwapWtpConfigRowStatus =
        ACTIVE;
    CapwapIsSetFsCapwapWtpConfigEntry.bFsWtpCountryString = OSIX_TRUE;

    if (CapwapSetAllFsCapwapWtpConfigTable (&CapwapSetFsCapwapWtpConfigEntry,
                                            &CapwapIsSetFsCapwapWtpConfigEntry,
                                            i4RowStatus,
                                            i4RowCreateOption) != OSIX_SUCCESS)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
        return OSIX_FAILURE;
    }

    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CapwapEnqueCtrlPkts                                  */
/*                                                                           */
/* Description        : This function is to enque CAPWAP ctrl  packets       */
/*                      to the Service task                                  */
/*                                                                           */
/* Input(s)           : pQMsg - Received queue message                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
CapwapEnqueCtrlPkts (tCapwapRxQMsg * pQMsg)
{

    INT1                i4Status = OSIX_SUCCESS;

    CAPWAP_FN_ENTRY ();
    switch (pQMsg->u4PktType)
    {
        case CAPWAP_FSM_MSG:
            if (OsixQueSend (gSerTaskGlobals.ctrlRxMsgQId,
                             (UINT1 *) &pQMsg,
                             OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Failed to send the message to CTRL Rx queue \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to send the message to CTRL Rx queue \r\n"));

                CAPWAP_RELEASE_CRU_BUF (pQMsg->pRcvBuf);
                MemReleaseMemBlock (CAPWAP_QUEMSG_POOLID, (UINT1 *) pQMsg);
                return OSIX_FAILURE;
            }
            /*post event to Service Task */
            if (OsixEvtSend (gSerTaskGlobals.capwapSerTaskId,
                             CAPWAP_CTRL_RX_MSGQ_EVENT) == OSIX_FAILURE)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "capwapServiceTaskEnqMsg: Failed to send Event \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "capwapServiceTaskEnqMsg: Failed to send Event \r\n"));

                i4Status = OSIX_FAILURE;;
            }
            break;
        case CAPWAP_CTRL_FRAG_MSG:
            if (OsixQueSend (gSerTaskGlobals.ctrlFragRxMsgQId,
                             (UINT1 *) &pQMsg,
                             OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Failed to send the message to CTRL Rx"
                            "Fragment queue \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to send the message to CTRL Rx"
                              "Fragment queue \r\n"));

                CAPWAP_RELEASE_CRU_BUF (pQMsg->pRcvBuf);
                MemReleaseMemBlock (CAPWAP_QUEMSG_POOLID, (UINT1 *) pQMsg);
                return OSIX_FAILURE;
            }
            /*post event to Service Task */
            if (OsixEvtSend (gSerTaskGlobals.capwapSerTaskId,
                             CAPWAP_FRAG_RX_MSGQ_EVENT) == OSIX_FAILURE)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "capwapServiceTaskEnqMsg: Failed to"
                            "send Event \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "capwapServiceTaskEnqMsg: Failed to"
                              "send Event \r\n"));

                i4Status = OSIX_FAILURE;
            }
            break;
        case CAPWAP_DATA_FRAG_MSG:
            if (OsixQueSend (gSerTaskGlobals.dataFragRxMsgQId,
                             (UINT1 *) &pQMsg,
                             OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Failed to send the message to CTRL Rx"
                            "Fragment queue \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to send the message to CTRL Rx"
                              "Fragment queue \r\n"));

                CAPWAP_RELEASE_CRU_BUF (pQMsg->pRcvBuf);
                MemReleaseMemBlock (CAPWAP_QUEMSG_POOLID, (UINT1 *) pQMsg);
                return OSIX_FAILURE;
            }
            /*post event to Service Task */
            if (OsixEvtSend (gSerTaskGlobals.capwapSerTaskId,
                             CAPWAP_DATA_FRAG_RX_MSGQ_EVENT) == OSIX_FAILURE)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "capwapServiceTaskEnqMsg: Failed to"
                            "send Event \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "capwapServiceTaskEnqMsg: Failed to"
                              "send Event \r\n"));

                i4Status = OSIX_FAILURE;
            }
            break;
        case CAPWAP_ECHO_ALIVE_MSG:
            if (OsixQueSend (gSerTaskGlobals.echoAliveMsgQId,
                             (UINT1 *) &pQMsg,
                             OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Failed to send the message to CAPWAP"
                            "DATA Rx queue \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to send the message to CAPWAP"
                              "DATA Rx queue \r\n"));

                CAPWAP_RELEASE_CRU_BUF (pQMsg->pRcvBuf);
                MemReleaseMemBlock (CAPWAP_QUEMSG_POOLID, (UINT1 *) pQMsg);
                return OSIX_FAILURE;
            }
            /*post event to Service Task */
            if (OsixEvtSend (gSerTaskGlobals.capwapSerTaskId,
                             CAPWAP_DATA_RX_MSGQ_EVENT) == OSIX_FAILURE)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "capwapServiceTaskEnqMsg: Failed to"
                            "send Event \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "capwapServiceTaskEnqMsg: Failed to"
                              "send Event \r\n"));

                i4Status = OSIX_FAILURE;
            }
            break;
        case CAPWAP_DATA_TX_MSG:
            if (OsixQueSend (gSerTaskGlobals.dataTxMsgQId,
                             (UINT1 *) &pQMsg,
                             OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Failed to send the message to"
                            "CAPWAP DATA Tx queue \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to send the message to"
                              "CAPWAP DATA Tx queue \r\n"));

                CAPWAP_RELEASE_CRU_BUF (pQMsg->pRcvBuf);
                MemReleaseMemBlock (CAPWAP_QUEMSG_POOLID, (UINT1 *) pQMsg);
                return OSIX_FAILURE;
            }
            /*post event to Service Task */
            if (OsixEvtSend (gSerTaskGlobals.capwapSerTaskId,
                             CAPWAP_DATA_TX_MSGQ_EVENT) == OSIX_FAILURE)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "capwapServiceTaskEnqMsg: Failed to"
                            "send Event \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "capwapServiceTaskEnqMsg: Failed to"
                              "send Event \r\n"));

                i4Status = OSIX_FAILURE;
            }
            break;
        default:
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "capwapServiceTaskEnqMsg: unknown msg queue \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "capwapServiceTaskEnqMsg: unknown msg queue \r\n"));

            CAPWAP_RELEASE_CRU_BUF (pQMsg->pRcvBuf);
            MemReleaseMemBlock (CAPWAP_QUEMSG_POOLID, (UINT1 *) pQMsg);

    }
    CAPWAP_FN_EXIT ();
    return i4Status;
}

/*****************************************************************************/
/* Function Name      : CapwapEventSend                                      */
/*                                                                           */
/* Description        : This function used to send and event to the          */
/*                      Service task.This function will be called by         */
/*                      discovery thread after discovery phase complete      */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS or OSIX_FAILURE                         */
/*****************************************************************************/
INT4
CapwapEventSend (VOID)
{
    CAPWAP_FN_ENTRY ();
    if (OsixEvtSend (gSerTaskGlobals.capwapSerTaskId,
                     CAPWAP_DISC_COMPLETE_EVENT) == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapEventSend: Failed to send"
                    "Discovery complete Event \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "CapwapEventSend: Failed to send"
                      "Discovery complete Event \r\n"));

        return OSIX_FAILURE;
    }
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : tmrRxCallback                                              *
 *                                                                           *
 * Description  : CAPWAP Receiver CPU relinquish timer function.             *
 *                                                                           *
 * Input        : tTimerListId                                               *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      :                                                            *
 *                                                                           *
 ****************************************************************************/
#if CAP_WANTED
VOID
tmrSerCallback (tTimerListId timerListId)
{
    UNUSED_PARAM (timerListId);
    if (OsixEvtSend (gSerTaskGlobals.capwapSerTaskId,
                     CAPWAP_SER_RELINQUISH_EVENT) != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Timer event send failure.\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Timer event send failure.\r\n"));
    }

    /* restart the timer */
/* coverity fix begin */
    if (TmrStart (pSerTimerListId, &tmrSerTmrBlk,
                  CAPWAP_CPU_RELINQUISH_SER_TMR, 1, 0) != TMR_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, " Timer Start FAILED.\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      " Timer Start FAILED.\r\n"));
    }
    /* coverity fix end */
}
#endif

/****************************************************************************
*                                                                           *
* Function     : CapwapServiceTaskInit                                      *
*                                                                           *
* Description  : CAPWAP Receiver task initialization routine.               *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : OSIX_SUCCESS, if initialization succeeds                   *
*                OSIX_FAILURE, otherwise                                    *
*                                                                           *
*****************************************************************************/
UINT4
CapwapServiceTaskInit (VOID)
{
    UINT4               u4Event = 0;
    UINT1               u1DiscRespCount = 0;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tACInfoAfterDiscovery *pAcInfoAfterDisc = NULL;
    UINT1               u1Index;
#ifdef WTP_WANTED
    UINT4               u4ErrorCode;
    UINT1               u1RadioId;
    UINT1              *pu1NvRamMac;
    UINT1               u1WtpMacStr[CAPWAP_WTP_DFLT_PROF_MACLEN + WTP_MAC];
    tCapwapCapwapBaseWtpProfileEntry CapwapSetCapwapBaseWtpProfileEntry;
    tCapwapIsSetCapwapBaseWtpProfileEntry CapwapIsSetCapwapBaseWtpProfileEntry;
    tCapwapFsCapwapWirelessBindingEntry CapwapSetFsCapwapWirelessBindingEntry;
    tCapwapIsSetFsCapwapWirelessBindingEntry
        CapwapIsSetFsCapwapWirelessBindingEntry;
    tRadioIfGetDB       RadioIfGetDB;
#endif /* WTP_WANTED */
    tArpRegTbl          ArpRegTbl;
#ifdef WLC_WANTED
    tVlanRegTbl         VlanRegTbl;

    MEMSET (&VlanRegTbl, 0, sizeof (tVlanRegTbl));
#endif
    MEMSET (&ArpRegTbl, 0, sizeof (tArpRegTbl));

    gu4CapwapEnable = CAPWAP_ENABLE;
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        pAcInfoAfterDisc =
        (tACInfoAfterDiscovery *) (VOID *) UtlShMemAllocAcInfoBuf ();
    if (pAcInfoAfterDisc == NULL)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapServiceTaskInit:- "
                    "UtlShMemAllocAcInfoBuf returned failure\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "CapwapServiceTaskInit:- "
                      "UtlShMemAllocAcInfoBuf returned failure\n"));
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "CapwapServiceTaskInit:- "
                      "UtlShMemAllocAcInfoBuf returned failure\n"));

        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (pAcInfoAfterDisc, 0, sizeof (tACInfoAfterDiscovery));
    if (OsixTskIdSelf (&gSerTaskGlobals.capwapSerTaskId) == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    " !!!!! CAPWAP SERVICE TASK INIT FAILURE  !!!!! \n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      " !!!!! CAPWAP SERVICE TASK INIT FAILURE  !!!!! \n"));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        UtlShMemFreeAcInfoBuf ((UINT1 *) pAcInfoAfterDisc);
        lrInitComplete (OSIX_FAILURE);
        return OSIX_FAILURE;
    }

    /* Create 'CTRL RX Q' queue */
    if (OsixQueCrt (CAPWAP_CTRL_Q_NAME, OSIX_MAX_Q_MSG_LEN, CAPWAP_Q_DEPTH,
                    &(gSerTaskGlobals.ctrlRxMsgQId)) != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "capwapServiceTaskInit:Ctrl Rx Queue Creation failed \n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "capwapServiceTaskInit:Ctrl Rx Queue Creation failed \n"));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        UtlShMemFreeAcInfoBuf ((UINT1 *) pAcInfoAfterDisc);
        lrInitComplete (OSIX_FAILURE);
        return OSIX_FAILURE;
    }

    /* Create Ctrl Fragment Rx queue */
    if (OsixQueCrt (CAPWAP_CTRLFRAG_Q_NAME, OSIX_MAX_Q_MSG_LEN, CAPWAP_Q_DEPTH,
                    &(gSerTaskGlobals.ctrlFragRxMsgQId)) != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "capwapServiceTaskInit:CTRL Fragment Rx Queue"
                    "Creation failed\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "capwapServiceTaskInit:CTRL Fragment Rx Queue"
                      "Creation failed\n"));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        UtlShMemFreeAcInfoBuf ((UINT1 *) pAcInfoAfterDisc);
        lrInitComplete (OSIX_FAILURE);
        return OSIX_FAILURE;
    }
    /* Create Data Fragment Rx queue */
    if (OsixQueCrt (CAPWAP_DATAFRAG_Q_NAME, OSIX_MAX_Q_MSG_LEN, CAPWAP_Q_DEPTH,
                    &(gSerTaskGlobals.dataFragRxMsgQId)) != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "capwapServiceTaskInit:Data Fragment Rx Queue"
                    "Creation failed\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "capwapServiceTaskInit:Data Fragment Rx Queue"
                      "Creation failed\n"));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        UtlShMemFreeAcInfoBuf ((UINT1 *) pAcInfoAfterDisc);
        lrInitComplete (OSIX_FAILURE);
        return OSIX_FAILURE;
    }

    /* Create CAPWAP Data Rx queue */
    if (OsixQueCrt (CAPWAP_DATARX_Q_NAME, OSIX_MAX_Q_MSG_LEN, CAPWAP_Q_DEPTH,
                    &(gSerTaskGlobals.echoAliveMsgQId)) != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "capwapServiceTaskInit:CAPWAP Data RX Queue Creation failed\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "capwapServiceTaskInit:CAPWAP Data RX Queue Creation failed\n"));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        UtlShMemFreeAcInfoBuf ((UINT1 *) pAcInfoAfterDisc);
        lrInitComplete (OSIX_FAILURE);
        return OSIX_FAILURE;
    }

    /* Create CAPWAP Data Tx queue */
    if (OsixQueCrt (CAPWAP_DATATX_Q_NAME, OSIX_MAX_Q_MSG_LEN, CAPWAP_Q_DEPTH,
                    &(gSerTaskGlobals.dataTxMsgQId)) != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "capwapServiceTaskInit:CAPWAP Data Tx Queue Creation failed\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "capwapServiceTaskInit:CAPWAP Data Tx Queue Creation failed\n"));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        UtlShMemFreeAcInfoBuf ((UINT1 *) pAcInfoAfterDisc);
        lrInitComplete (OSIX_FAILURE);
        return OSIX_FAILURE;
    }

    MEMCPY (gSerTaskGlobals.au1TaskSemName, CAPWAP_MUT_EXCL_SERVICE_SEM_NAME,
            OSIX_NAME_LEN);

    if (OsixCreateSem (CAPWAP_MUT_EXCL_SERVICE_SEM_NAME,
                       CAPWAP_SEM_CREATE_INIT_CNT, 0,
                       &gSerTaskGlobals.capwapServiceTaskSemId) == OSIX_FAILURE)
    {
        CAPWAP_TRC1 (CAPWAP_FAILURE_TRC,
                     "Seamphore Creation failure for %s \n",
                     CAPWAP_MUT_EXCL_SERVICE_SEM_NAME);
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Seamphore Creation failure for %s \n",
                      CAPWAP_MUT_EXCL_SERVICE_SEM_NAME));
        UtlShMemFreeAcInfoBuf ((UINT1 *) pAcInfoAfterDisc);
        return OSIX_FAILURE;
    }

    /* Create the Application timer List */
    if (CapwapTmrInit () == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "capwapServiceTaskInit:Application Timer creatiion Failed\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "capwapServiceTaskInit:Application Timer creatiion Failed\n"));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        UtlShMemFreeAcInfoBuf ((UINT1 *) pAcInfoAfterDisc);
        lrInitComplete (OSIX_FAILURE);
        return OSIX_FAILURE;
    }

    /* Create the RB Tree */
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_INIT_DB,
                                 pWssIfCapwapDB) == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "capwapServiceTaskInit:Creation of Wtp Session Profile"
                    "RB Tree Failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "capwapServiceTaskInit:Creation of Wtp Session Profile"
                      "RB Tree Failed \r\n"));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        UtlShMemFreeAcInfoBuf ((UINT1 *) pAcInfoAfterDisc);
        lrInitComplete (OSIX_FAILURE);
        return OSIX_FAILURE;
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
#ifdef WLC_WANTED
    /* CLI Main Task */
    if (CapwapMainTaskInit () == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapMainTaskInit:Creation of Capwap Main task Failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "CapwapMainTaskInit:Creation of Capwap Main task Failed \r\n"));
        CapwapMainDeInit ();
        lrInitComplete (OSIX_FAILURE);
        UtlShMemFreeAcInfoBuf ((UINT1 *) pAcInfoAfterDisc);
        return OSIX_FAILURE;
    }

    /*Notfication Globals default Disabling */
    if (CapwapSetCapwapBaseChannelUpDownNotifyEnable (CAPWAP_NOTIFY_DISABLE) !=
        OSIX_SUCCESS)
    {
    }
    if (CapwapSetCapwapBaseDecryptErrorNotifyEnable (CAPWAP_NOTIFY_ENABLE) !=
        OSIX_SUCCESS)
    {
    }
    if (CapwapSetCapwapBaseJoinFailureNotifyEnable (CAPWAP_NOTIFY_ENABLE) !=
        OSIX_SUCCESS)
    {
    }
    if (CapwapSetCapwapBaseImageUpgradeFailureNotifyEnable
        (CAPWAP_NOTIFY_ENABLE) != OSIX_SUCCESS)
    {
    }
    if (CapwapSetCapwapBaseConfigMsgErrorNotifyEnable (CAPWAP_NOTIFY_DISABLE) !=
        OSIX_SUCCESS)
    {
    }
    if (CapwapSetCapwapBaseRadioOperableStatusNotifyEnable
        (CAPWAP_NOTIFY_DISABLE) != OSIX_SUCCESS)
    {
    }
    if (CapwapSetCapwapBaseAuthenFailureNotifyEnable (CAPWAP_NOTIFY_ENABLE) !=
        OSIX_SUCCESS)
    {
    }
    VlanRegTbl.pVlanBasicInfo = CapwapRegisterVlanInfo;
    VlanRegTbl.u1ProtoId = VLAN_WSS_PROTO_ID;

    if (VlanRegDeRegProtocol (VLAN_INDICATION_REGISTER,
                              &VlanRegTbl) == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapMainTaskInit:Vlan Registration Failed\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "CapwapMainTaskInit:Vlan Registration Failed\r\n"));
        CapwapMainDeInit ();
        lrInitComplete (OSIX_FAILURE);
        UtlShMemFreeAcInfoBuf ((UINT1 *) pAcInfoAfterDisc);
        return OSIX_FAILURE;
    }

#endif
    ArpRegTbl.pArpBasicInfo = CapwapRegisterArpInfo;
    ArpRegTbl.u1ProtoId = ARP_WSS_PROTO_ID;

    if (ArpRegDeRegProtocol (ARP_INDICATION_REGISTER,
                             &ArpRegTbl) == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapMainTaskInit:Arp Registration Failed\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "CapwapMainTaskInit:Arp Registration Failed\r\n"));
        CapwapMainDeInit ();
        lrInitComplete (OSIX_FAILURE);
        UtlShMemFreeAcInfoBuf ((UINT1 *) pAcInfoAfterDisc);
        return OSIX_FAILURE;
    }
#ifdef WTP_WANTED
    /* #ifndef NPAPI_WANTED */
    /* CLI Main Task */
    if (CapwapMainTaskInit () == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapMainTaskInit:Creation of Capwap Main task Failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "CapwapMainTaskInit:Creation of Capwap Main task Failed \r\n"));
        CapwapMainDeInit ();
        lrInitComplete (OSIX_FAILURE);
        UtlShMemFreeAcInfoBuf ((UINT1 *) pAcInfoAfterDisc);
        return OSIX_FAILURE;
    }
    /* #endif */
    /* try to create the default wtp profile table for the wtp */
    MEMSET (&CapwapSetCapwapBaseWtpProfileEntry, 0,
            sizeof (tCapwapCapwapBaseWtpProfileEntry));
    MEMSET (&CapwapIsSetCapwapBaseWtpProfileEntry, 0,
            sizeof (tCapwapIsSetCapwapBaseWtpProfileEntry));

    /* fill the values */
    CapwapSetCapwapBaseWtpProfileEntry.MibObject.u4CapwapBaseWtpProfileId =
        CAPWAP_WTP_DFLT_PROF_ID;
    CapwapIsSetCapwapBaseWtpProfileEntry.bCapwapBaseWtpProfileId = OSIX_TRUE;

    MEMCPY (CapwapSetCapwapBaseWtpProfileEntry.MibObject.
            au1CapwapBaseWtpProfileName,
            CAPWAP_WTP_DFLT_PROF_NAME, STRLEN (CAPWAP_WTP_DFLT_PROF_NAME));
    CapwapSetCapwapBaseWtpProfileEntry.MibObject.i4CapwapBaseWtpProfileNameLen =
        STRLEN (CAPWAP_WTP_DFLT_PROF_NAME);
    CapwapIsSetCapwapBaseWtpProfileEntry.bCapwapBaseWtpProfileName = OSIX_TRUE;

    pu1NvRamMac = IssGetSwitchMacFromNvRam ();
    SPRINTF ((CHR1 *) u1WtpMacStr, "%02x:%02x:%02x:%02x:%02x:%02x",
             pu1NvRamMac[0], pu1NvRamMac[1], pu1NvRamMac[2],
             pu1NvRamMac[3], pu1NvRamMac[4], pu1NvRamMac[5]);
    /*     PrintMacAddress(IssGetSwitchMacFromNvRam(), u1WtpMacStr); */
    MEMCPY (CapwapSetCapwapBaseWtpProfileEntry.MibObject.
            au1CapwapBaseWtpProfileWtpMacAddress,
            u1WtpMacStr,
            STRLEN (u1WtpMacStr) /*CAPWAP_WTP_DFLT_PROF_MACLEN */ );
    CapwapSetCapwapBaseWtpProfileEntry.MibObject.
        i4CapwapBaseWtpProfileWtpMacAddressLen = STRLEN (u1WtpMacStr);
    CapwapIsSetCapwapBaseWtpProfileEntry.bCapwapBaseWtpProfileWtpMacAddress =
        OSIX_TRUE;

    MEMCPY (CapwapSetCapwapBaseWtpProfileEntry.MibObject.
            au1CapwapBaseWtpProfileWtpModelNumber,
            WssGetWtpModelNumFromWtpNvRam (),
            STRLEN (WssGetWtpModelNumFromWtpNvRam ()));
    CapwapSetCapwapBaseWtpProfileEntry.MibObject.
        i4CapwapBaseWtpProfileWtpModelNumberLen =
        STRLEN (WssGetWtpModelNumFromWtpNvRam ());
    CapwapIsSetCapwapBaseWtpProfileEntry.bCapwapBaseWtpProfileWtpModelNumber
        = OSIX_TRUE;

    MEMCPY (CapwapSetCapwapBaseWtpProfileEntry.MibObject.
            au1CapwapBaseWtpProfileWtpName,
            IssGetSwitchName (), STRLEN (IssGetSwitchName ()));
    CapwapSetCapwapBaseWtpProfileEntry.MibObject.
        i4CapwapBaseWtpProfileWtpNameLen = STRLEN (IssGetSwitchName ());
    CapwapIsSetCapwapBaseWtpProfileEntry.bCapwapBaseWtpProfileWtpName =
        OSIX_TRUE;

    MEMCPY (CapwapSetCapwapBaseWtpProfileEntry.MibObject.
            au1CapwapBaseWtpProfileWtpLocation,
            IssGetSysLocation (), STRLEN (IssGetSysLocation ()));
    CapwapSetCapwapBaseWtpProfileEntry.MibObject.
        i4CapwapBaseWtpProfileWtpLocationLen = STRLEN (IssGetSysLocation ());
    CapwapIsSetCapwapBaseWtpProfileEntry.bCapwapBaseWtpProfileWtpLocation =
        OSIX_TRUE;

    CapwapSetCapwapBaseWtpProfileEntry.MibObject.
        i4CapwapBaseWtpProfileRowStatus = CREATE_AND_GO;
    CapwapIsSetCapwapBaseWtpProfileEntry.bCapwapBaseWtpProfileRowStatus =
        OSIX_TRUE;

    if (CapwapTestAllCapwapBaseWtpProfileTable
        (&u4ErrorCode, &CapwapSetCapwapBaseWtpProfileEntry,
         &CapwapIsSetCapwapBaseWtpProfileEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapMainTaskInit: CapwapTestAllCapwapBaseWtpProfileTable"
                    "failed\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "CapwapMainTaskInit: CapwapTestAllCapwapBaseWtpProfileTable"
                      "failed\r\n"));
        CapwapMainDeInit ();
        lrInitComplete (OSIX_FAILURE);
    }

    if (CapwapSetAllCapwapBaseWtpProfileTable
        (&CapwapSetCapwapBaseWtpProfileEntry,
         &CapwapIsSetCapwapBaseWtpProfileEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapMainTaskInit: CapwapSetAllCapwapBaseWtpProfileTable"
                    "failed\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "CapwapMainTaskInit: CapwapSetAllCapwapBaseWtpProfileTable"
                      "failed\r\n"));
        CapwapMainDeInit ();
        lrInitComplete (OSIX_FAILURE);
    }

    /* create fs-capwap-wireless-binding table for the default wtp-profile */
    MEMSET (&CapwapSetFsCapwapWirelessBindingEntry, 0,
            sizeof (tCapwapFsCapwapWirelessBindingEntry));
    MEMSET (&CapwapIsSetFsCapwapWirelessBindingEntry, 0,
            sizeof (tCapwapIsSetFsCapwapWirelessBindingEntry));

    CapwapSetFsCapwapWirelessBindingEntry.MibObject.u4CapwapBaseWtpProfileId =
        CAPWAP_WTP_DFLT_PROF_ID;
    CapwapIsSetFsCapwapWirelessBindingEntry.bCapwapBaseWtpProfileId = OSIX_TRUE;

    CapwapSetFsCapwapWirelessBindingEntry.MibObject.
        i4FsCapwapWirelessBindingRowStatus = CREATE_AND_GO;
    CapwapIsSetFsCapwapWirelessBindingEntry.bFsCapwapWirelessBindingRowStatus =
        OSIX_TRUE;

    for (u1RadioId = 1; u1RadioId <= SYS_DEF_MAX_RADIO_INTERFACES; u1RadioId++)
    {
        MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

        RadioIfGetDB.RadioIfGetAllDB.u1RadioId = u1RadioId;
        RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;

        RadioIfGetDB.RadioIfIsGetAllDB.bBindingType = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bRadioIfIndex = OSIX_TRUE;

        if (WssIfProcessRadioIfDBMsg (WSS_GET_PHY_RADIO_IF_DB, &RadioIfGetDB) !=
            OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "CapwapMainTaskInit: WssIfProcessRadioIfDBMsg"
                        "WSS_GET_PHY_RADIO_IF_DB failed\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "CapwapMainTaskInit: WssIfProcessRadioIfDBMsg"
                          "WSS_GET_PHY_RADIO_IF_DB failed\r\n"));
            CapwapMainDeInit ();
            lrInitComplete (OSIX_FAILURE);
        }

        CapwapSetFsCapwapWirelessBindingEntry.MibObject.
            u4CapwapBaseWirelessBindingRadioId =
            RadioIfGetDB.RadioIfGetAllDB.u1RadioId;
        CapwapIsSetFsCapwapWirelessBindingEntry.
            bCapwapBaseWirelessBindingRadioId = OSIX_TRUE;

        CapwapSetFsCapwapWirelessBindingEntry.MibObject.
            i4FsCapwapWirelessBindingVirtualRadioIfIndex =
            RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex;
        CapwapIsSetFsCapwapWirelessBindingEntry.
            bFsCapwapWirelessBindingVirtualRadioIfIndex = OSIX_TRUE;

        CapwapSetFsCapwapWirelessBindingEntry.MibObject.
            i4FsCapwapWirelessBindingType = CAPWAP_DOT11_BIND_TYPE;
        CapwapIsSetFsCapwapWirelessBindingEntry.bFsCapwapWirelessBindingType =
            OSIX_TRUE;

        if (CapwapTestAllFsCapwapWirelessBindingTable (&u4ErrorCode,
                                                       &CapwapSetFsCapwapWirelessBindingEntry,
                                                       &CapwapIsSetFsCapwapWirelessBindingEntry,
                                                       OSIX_TRUE,
                                                       OSIX_TRUE) !=
            OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "CapwapMainTaskInit:CapwapTestAllFsCapwapWirelesBindinTable"
                        "failed\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "CapwapMainTaskInit:CapwapTestAllFsCapwapWirelesBindinTable"
                          "failed\r\n"));
            CapwapMainDeInit ();
            lrInitComplete (OSIX_FAILURE);
        }

        if (CapwapSetAllFsCapwapWirelessBindingTable
            (&CapwapSetFsCapwapWirelessBindingEntry,
             &CapwapIsSetFsCapwapWirelessBindingEntry, OSIX_TRUE,
             OSIX_TRUE) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "CapwapMainTaskInit:CapwapSetAllFsCapwapWirelesBindingTable"
                        "failed\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "CapwapMainTaskInit:CapwapSetAllFsCapwapWirelesBindingTable"
                          "failed\r\n"));
            CapwapMainDeInit ();
            lrInitComplete (OSIX_FAILURE);
        }
    }
#endif /* WTP_WANTED */
    gIntProfileId = 0;

#if CAP_WANTED
    /* CPU relinquish timer initialization */
    /* create a timer */
    if (TMR_FAILURE == TmrCreateTimerList ((CONST UINT1 *)
                                           "CPU_RELINQUISH_SER_TIMER", 0,
                                           tmrSerCallback, &pSerTimerListId))
    {
        UtlShMemFreeAcInfoBuf ((UINT1 *) pAcInfoAfterDisc);
        return OSIX_FAILURE;
    }

    /* start the timer */
    if (TmrStart (pSerTimerListId, &tmrSerTmrBlk,
                  CAPWAP_CPU_RELINQUISH_SER_TMR, 1, 0) == TMR_FAILURE)
    {
        UtlShMemFreeAcInfoBuf ((UINT1 *) pAcInfoAfterDisc);
        return OSIX_FAILURE;
    }

#endif
    /* initialize counter to zero */
    gu4CPUSerRelinquishCounter = 0;

    gSerTaskGlobals.u1IsSerTaskInitialized = CAPWAP_SUCCESS;

    CAPWAP_TRC1 (CAPWAP_INIT_TRC,
                 "capwapServiceTaskInit: WSS-AP Service Task Init Completed: %d\n",
                 gSerTaskGlobals.u1IsSerTaskInitialized);

    SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4CapwapSysLogId,
                  "capwapServiceTaskInit: WSS-AP Service Task Init Completed: %d\n",
                  gSerTaskGlobals.u1IsSerTaskInitialized));
    /* Start the PMTU update timer. */
#ifdef IMPLEMENTATION_ONGOING
    CapwapGlobalTmrStart (&(gSerTaskGlobals.PMTUUpdateTmr),
                          CAPWAP_PMTU_UPDATE_TMR, CAPWAP_PMTU_UPDATE_TIMEOUT);
#endif
    /*MEMCPY (gCapwapGlobals.au1TaskSemName, CAPWAP_MUT_EXCL_CONF_SEM_NAME,
       OSIX_NAME_LEN); */
    if (OsixCreateSem (CAPWAP_MUT_EXCL_CONF_SEM_NAME,
                       CAPWAP_SEM_CREATE_INIT_CNT, 0,
                       &gSerTaskGlobals.capwapConfSemId) == OSIX_FAILURE)
    {
        CAPWAP_TRC1 (CAPWAP_FAILURE_TRC,
                     "Seamphore Creation failure for %s \n",
                     CAPWAP_MUT_EXCL_CONF_SEM_NAME);
        SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4CapwapSysLogId,
                      "Seamphore Creation failure for %s \n",
                      CAPWAP_MUT_EXCL_CONF_SEM_NAME));
        UtlShMemFreeAcInfoBuf ((UINT1 *) pAcInfoAfterDisc);
        return OSIX_FAILURE;
    }
    if (CapwapTmrStart (NULL, CAPWAP_CPU_RELINQUISH_TMR,
                        CAPWAP_RELINQUISH_TIMER_VAL) != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, " Timer Start FAILED.\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      " Timer Start FAILED.\r\n"));
    }

    if (CapwapTmrStart (NULL, CAPWAP_CPU_RECV_RELINQUISH_TMR,
                        CAPWAP_RELINQUISH_TIMER_VAL) != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, " Timer Start FAILED.\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      " Timer Start FAILED.\r\n"));
    }
    lrInitComplete (OSIX_SUCCESS);

    while (1)
    {
        if (OsixReceiveEvent (CAPWAP_FRAG_RX_MSGQ_EVENT |
                              CAPWAP_CTRL_RX_MSGQ_EVENT |
                              CAPWAP_DATA_RX_MSGQ_EVENT |
                              CAPWAP_RUN_TMR_EXP_EVENT |
                              CAPWAP_FSM_TMR_EXP_EVENT |
                              CAPWAP_DISC_COMPLETE_EVENT |
                              CAPWAP_DATA_TX_MSGQ_EVENT |
                              CAPWAP_DATA_FRAG_RX_MSGQ_EVENT, OSIX_WAIT,
                              (UINT4) 0, (UINT4 *) &(u4Event)) == OSIX_SUCCESS)
        {

            CAPWAP_SERVICE_TASK_LOCK;
            if ((u4Event & CAPWAP_DISC_COMPLETE_EVENT) ==
                CAPWAP_DISC_COMPLETE_EVENT)
            {
                CAPWAP_TRC (CAPWAP_MGMT_TRC,
                            "Service thread received the discovery"
                            "phase complete Event \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Service thread received the discovery"
                              "phase complete Event \r\n"));
                CapwapGetDiscResponseCount (&u1DiscRespCount);
                for (u1Index = 0; u1Index < u1DiscRespCount; u1Index++)
                {
                    if (CapwapGetBestACDetails (pAcInfoAfterDisc) ==
                        OSIX_FAILURE)
                    {
                        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                    "Failed to Get Perferred AC Details \r\n");
                        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                      "Failed to Get Perferred AC Details \r\n"));
                        UtlShMemFreeAcInfoBuf ((UINT1 *) pAcInfoAfterDisc);
                        CAPWAP_SERVICE_TASK_UNLOCK;
                        return OSIX_FAILURE;
                    }
                    if (WtpEnterJoinState (pAcInfoAfterDisc) == OSIX_SUCCESS)
                    {
                        CAPWAP_TRC (CAPWAP_MGMT_TRC,
                                    "WTP Transmitted the Join Request \r\n");
                        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                      "WTP Transmitted the Join Request \r\n"));
                        break;
                    }
                }
                CapwapCheckServRelinquishCounters ();
            }
            if ((u4Event & CAPWAP_FRAG_RX_MSGQ_EVENT) ==
                CAPWAP_FRAG_RX_MSGQ_EVENT)
            {
                CAPWAP_TRC (CAPWAP_MGMT_TRC,
                            "Service Task Received the Ctrl Rx"
                            "Msg Queue Event \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Service Task Received the Ctrl Rx"
                              "Msg Queue Event \r\n"));
                CapwapProcessFragRxMsg ();
                CapwapCheckServRelinquishCounters ();
            }
            if ((u4Event & CAPWAP_DATA_FRAG_RX_MSGQ_EVENT) ==
                CAPWAP_DATA_FRAG_RX_MSGQ_EVENT)
            {
                CAPWAP_TRC (CAPWAP_MGMT_TRC,
                            "Service Task Received the Data Fragment"
                            "Rx Msg Queue Event \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Service Task Received the Data Fragment"
                              "Rx Msg Queue Event \r\n"));
                gu4CPUSerRelinquishCounter++;
                CapwapDataProcessFragRxMsg ();
            }
            if ((u4Event & CAPWAP_CTRL_RX_MSGQ_EVENT) ==
                CAPWAP_CTRL_RX_MSGQ_EVENT)
            {
                CAPWAP_TRC (CAPWAP_MGMT_TRC,
                            "Service Task Received the Capwap Ctrl"
                            "Rx Msg Queue Event \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Service Task Received the Capwap Ctrl"
                              "Rx Msg Queue Event \r\n"));
                CapwapProcessCtrlRxMsg ();
                CapwapCheckServRelinquishCounters ();
            }
            if ((u4Event & CAPWAP_DATA_RX_MSGQ_EVENT) ==
                CAPWAP_DATA_RX_MSGQ_EVENT)
            {
                /* This event process the data chennel keep alive 
                   packets and control channel echo messages */
                CAPWAP_TRC (CAPWAP_MGMT_TRC,
                            "Service Task Received the Capwap Data"
                            "Rx Msg Queue Event \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Service Task Received the Capwap Data"
                              "Rx Msg Queue Event \r\n"));
                CapwapProcessDataRxMsg ();
                CapwapCheckServRelinquishCounters ();
            }
            if ((u4Event & CAPWAP_FSM_TMR_EXP_EVENT) ==
                CAPWAP_FSM_TMR_EXP_EVENT)
            {
                CAPWAP_TRC (CAPWAP_MGMT_TRC,
                            "Service Task Received the Fsm Timer"
                            "Expiry Event \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Service Task Received the Fsm Timer"
                              "Expiry Event \r\n"));
                CapwapFsmTmrExpHandler ();
                CapwapCheckServRelinquishCounters ();
            }
            if ((u4Event & CAPWAP_RUN_TMR_EXP_EVENT) ==
                CAPWAP_RUN_TMR_EXP_EVENT)
            {
                CAPWAP_TRC (CAPWAP_MGMT_TRC,
                            "Service Task Received the Run Timer"
                            "Expiry Event \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Service Task Received the Run Timer"
                              "Expiry Event \r\n"));
                CapwapRunTmrExpHandler ();
                CapwapCheckServRelinquishCounters ();
            }
            if ((u4Event & CAPWAP_DATA_TX_MSGQ_EVENT) ==
                CAPWAP_DATA_TX_MSGQ_EVENT)
            {
                CAPWAP_TRC (CAPWAP_MGMT_TRC,
                            "Service task received the Capwap Data"
                            "Tx Msg Queue event \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Service task received the Capwap Data"
                              "Tx Msg Queue event \r\n"));
                CapwapProcessDataTxMsg ();
                CapwapCheckServRelinquishCounters ();
            }
#if CAP_WANTED
            else if ((u4Event & CAPWAP_SER_RELINQUISH_EVENT) ==
                     CAPWAP_SER_RELINQUISH_EVENT)
            {
                gu4CPUSerRelinquishCounter = 0;
            }
#endif
#if CAP_WANTED
            if (100 == gu4CPUSerRelinquishCounter)
            {
                OsixEvtRecv (gSerTaskGlobals.capwapSerTaskId,
                             CAPWAP_SER_RELINQUISH_EVENT, OSIX_WAIT, &u4Event);

                gu4CPUSerRelinquishCounter = 0;
            }
#endif

            CAPWAP_SERVICE_TASK_UNLOCK;
        }
    }
    UtlShMemFreeAcInfoBuf ((UINT1 *) pAcInfoAfterDisc);
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : CapwapServiceMainTaskLock                                  */
/*                                                                           */
/* Description  : Lock the Capwap Main Task                                  */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
CapwapServiceMainTaskLock (VOID)
{
    CAPWAP_FN_ENTRY ();
    /*** For debug *****/
    CAPWAP_TRC1 (CAPWAP_MGMT_TRC,
                 "Capwap task lock. Capwap Conf Sem Id is %d \n",
                 gSerTaskGlobals.capwapConfSemId);
    SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4CapwapSysLogId,
                  "Capwap task lock. Capwap Conf Sem Id is %d \n",
                  gSerTaskGlobals.capwapConfSemId));
    if (OsixSemTake (gSerTaskGlobals.capwapConfSemId) == OSIX_FAILURE)
    {
        CAPWAP_TRC1 (CAPWAP_FAILURE_TRC,
                     "TakeSem failure for %s \n",
                     CAPWAP_MUT_EXCL_CONF_SEM_NAME);
        SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4CapwapSysLogId,
                      "TakeSem failure for %s \n",
                      CAPWAP_MUT_EXCL_CONF_SEM_NAME));
        return OSIX_FAILURE;
    }
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : CapwapServiceMainTaskUnLock                                */
/*                                                                           */
/* Description  : UnLock the CAPWAP Task                                     */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
CapwapServiceMainTaskUnLock (VOID)
{
    CAPWAP_FN_ENTRY ();

    /*** For debug *****/
    CAPWAP_TRC1 (CAPWAP_MGMT_TRC,
                 "Capwap task unlock. Capwap Conf Sem Id is %d \n",
                 gSerTaskGlobals.capwapConfSemId);
    SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4CapwapSysLogId,
                  "Capwap task unlock. Capwap Conf Sem Id is %d \n",
                  gSerTaskGlobals.capwapConfSemId));
    /*** For debug *****/

    OsixSemGive (gSerTaskGlobals.capwapConfSemId);

    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : CapwapServiceTaskLock*/
/*                                                                           */
/* Description  : Lock the Capwap Service Task                                  */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
CapwapServiceTaskLock (VOID)
{
    CAPWAP_FN_ENTRY ();
    /*** For debug *****/
    CAPWAP_TRC1 (CAPWAP_MGMT_TRC,
                 "Capwap Service Task Sem Id is %d \n",
                 gSerTaskGlobals.capwapServiceTaskSemId);
    SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4CapwapSysLogId,
                  "Capwap Service Task Sem Id is %d \n",
                  gSerTaskGlobals.capwapServiceTaskSemId));
    if (OsixSemTake (gSerTaskGlobals.capwapServiceTaskSemId) == OSIX_FAILURE)
    {
        CAPWAP_TRC1 (CAPWAP_FAILURE_TRC,
                     "TakeSem failure for %s \n",
                     CAPWAP_MUT_EXCL_SERVICE_SEM_NAME);
        SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4CapwapSysLogId,
                      "TakeSem failure for %s \n",
                      CAPWAP_MUT_EXCL_SERVICE_SEM_NAME));
        return OSIX_FAILURE;
    }
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : CapwapServiceTaskUnLock                                    */
/*                                                                           */
/* Description  : UnLock the CAPWAP Service Task                             */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
CapwapServiceTaskUnLock (VOID)
{
    CAPWAP_FN_ENTRY ();

    /*** For debug *****/
    CAPWAP_TRC1 (CAPWAP_MGMT_TRC,
                 "Capwap Service Task Sem Id is %d \n",
                 gSerTaskGlobals.capwapServiceTaskSemId);
    SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4CapwapSysLogId,
                  "Capwap Service Task Sem Id is %d \n",
                  gSerTaskGlobals.capwapServiceTaskSemId));
    /*** For debug *****/

    OsixSemGive (gSerTaskGlobals.capwapServiceTaskSemId);

    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : CapwapDataProcessFragRxMsg
 * Description  : Process the received CAPWAP pkt in the Data Frag Rx queue
 * Input        : None
 * Output       : None
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE
 *****************************************************************************/
INT4
CapwapDataProcessFragRxMsg ()
{
    tCapwapRxQMsg      *pRxQMsg;
    tSegment           *pBuf = NULL;
    tSegment           *pReassemBuf = NULL;
    INT4                i4RetStat = 1;
    tRemoteSessionManager *pSessEntry = NULL;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT2               u2PktLen;

    CAPWAP_FN_ENTRY ();

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        while (OsixQueRecv
               (gSerTaskGlobals.dataFragRxMsgQId, (UINT1 *) (&pRxQMsg),
                OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        if (pRxQMsg->u4PktType == CAPWAP_DATA_FRAG_MSG)
        {
            MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

            pBuf = pRxQMsg->pRcvBuf;
            pWssIfCapwapDB->u4DestIp = CRU_BUF_Get_U4Reserved2 (pBuf);
            pWssIfCapwapDB->u4DestPort = CRU_BUF_Get_U4Reserved1 (pBuf);
/* coverity fix begin */
            /*WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_RSM, &WssIfCapwapDB); */
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_RSM,
                                         pWssIfCapwapDB) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "WssIfProcessCapwapDBMsg failed \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "WssIfProcessCapwapDBMsg failed \r\n"));
            }

/* coverity fix end */
            pSessEntry = pWssIfCapwapDB->pSessEntry;
            u2PktLen = CAPWAP_MAX_PKT_LEN;
            if (pSessEntry != NULL)
            {
                CAPWAP_TRC (CAPWAP_MGMT_TRC,
                            "capwapProcessCtrlRxMsg: Received packet"
                            "is fragment \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "capwapProcessCtrlRxMsg: Received packet"
                              "is fragment \r\n"));
                i4RetStat = CapwapDataFragReassemble (pBuf, pSessEntry,
                                                      u2PktLen, &pReassemBuf);
                if (i4RetStat == OSIX_SUCCESS)
                {
                    /* send the packet to the CAPWAP CTrl Rx
                     * queue for further processing */
                    CapwapProcessDataPacket (pReassemBuf);
                }
                MemReleaseMemBlock (CAPWAP_QUEMSG_POOLID, (UINT1 *) pRxQMsg);
            }
            else
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Received the unknown Fragment."
                            "Discard the packet \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Received the unknown Fragment."
                              "Discard the packet \r\n"));
                MemReleaseMemBlock (CAPWAP_QUEMSG_POOLID, (UINT1 *) pRxQMsg);
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
        }
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : CapwapProcessFragRxMsg                                     *
 *                                                                           *
 * Description  : Process the received CAPWAP pkt in the CTRL Rx queue       *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
CapwapProcessFragRxMsg ()
{
    tCapwapRxQMsg      *pRxQMsg;
    tSegment           *pBuf = NULL;
    INT4                i4RetStat = 1;
    tRemoteSessionManager *pSessEntry = NULL;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT2               u2PktLen;

    CAPWAP_FN_ENTRY ();

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        while (OsixQueRecv
               (gSerTaskGlobals.ctrlFragRxMsgQId, (UINT1 *) (&pRxQMsg),
                OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        if (pRxQMsg->u4PktType == CAPWAP_CTRL_FRAG_MSG)
        {
            pBuf = pRxQMsg->pRcvBuf;
            if (pBuf == NULL)
            {
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }

            MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

            pWssIfCapwapDB->u4DestIp = CRU_BUF_Get_U4Reserved2 (pBuf);
            pWssIfCapwapDB->u4DestPort = CRU_BUF_Get_U4Reserved1 (pBuf);
            /* coverity fix begin */
            /*WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_RSM, pWssIfCapwapDB); */
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_RSM,
                                         pWssIfCapwapDB) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "WssIfProcessCapwapDBMsg failed \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "WssIfProcessCapwapDBMsg failed \r\n"));
            }

            /* coverity fix end */
            pSessEntry = pWssIfCapwapDB->pSessEntry;
            u2PktLen = CAPWAP_MAX_PKT_LEN;
            if (pSessEntry != NULL)
            {
                CAPWAP_TRC (CAPWAP_MGMT_TRC,
                            "capwapProcessCtrlRxMsg: Received packet"
                            "is fragment \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "capwapProcessCtrlRxMsg: Received packet"
                              "is fragment \r\n"));
                i4RetStat = CapwapFragReassemble (pBuf, pSessEntry, u2PktLen,
                                                  WSS_CAPWAP_802_11_CTRL_PKT);
                if (i4RetStat != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_MGMT_TRC, "CapwapProcessFragRxMsg:Capwap"
                                "FragReassemble returned FAILURE \r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "CapwapProcessFragRxMsg:Capwap"
                                  "FragReassemble returned FAILURE \r\n"));
                }
                MemReleaseMemBlock (CAPWAP_QUEMSG_POOLID, (UINT1 *) pRxQMsg);
            }
        }
        else
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Received the unknown Fragment."
                        "Discard the packet \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Received the unknown Fragment."
                          "Discard the packet \r\n"));
            MemReleaseMemBlock (CAPWAP_QUEMSG_POOLID, (UINT1 *) pRxQMsg);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

VOID
CapwapPrintMacAddress (UINT1 *pMacAddr, UINT1 *pu1Temp)
{
    UINT1               au1MacAddr[MAC_ADDR_LEN];
    UINT1               u1MacLen;
    CAPWAP_FN_ENTRY ();
    MEMCPY (au1MacAddr, pMacAddr, MAC_ADDR_LEN);
    for (u1MacLen = 0; u1MacLen < MAC_ADDR_LEN; u1MacLen++)
    {
        if (au1MacAddr[u1MacLen] < MAC_LENGTH)
        {
/* coverity fix begin */
            /*SPRINTF ((CHR1 *) pu1Temp, "0%x", au1MacAddr[u1MacLen]); */
            SNPRINTF ((CHR1 *) pu1Temp, 1, "0%x", au1MacAddr[u1MacLen]);
/* coverity fix end */
        }
        else
        {
/* coverity fix begin */
            /*SPRINTF ((CHR1 *) pu1Temp, "%x", au1MacAddr[u1MacLen]); */
            SNPRINTF ((CHR1 *) pu1Temp, 1, "%x", au1MacAddr[u1MacLen]);
/* coverity fix end */
        }
        pu1Temp += STRLEN (pu1Temp);

        if (u1MacLen != (RADIO_MAC_LEN - 1))
        {

/* coverity fix begin */
            /*STRNCPY (pu1Temp, ":", 1); */
            STRNCPY (pu1Temp, ":", STR_LEN);
/* coverity fix end */
            pu1Temp += STRLEN (pu1Temp);

        }
    }
    CAPWAP_FN_EXIT ();
}

/*****************************************************************************
 * Function     : CapwapCleanUpSessionDetails                                *
 *                                                                           *
 * Description  : Cleans up the CAPWAP WTP Session Details in WLC.           *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           * 
 *****************************************************************************/
INT4
CapwapCleanUpSessionDetails (tRemoteSessionManager * pSessEntry)
{
    tWssIfCapDB        *pCapwapGetDB = NULL;

    CAPWAP_FN_ENTRY ();

    WSS_IF_DB_TEMP_MEM_ALLOC (pCapwapGetDB, OSIX_FAILURE)
        MEMSET (pCapwapGetDB, 0, sizeof (tWssIfCapDB));

    if (pSessEntry != NULL)
    {
        CapwapStopAllSessionTimers (pSessEntry);

        CAPWAP_TRC (CAPWAP_FSM_TRC, "Entered CAPWAP DTLSTD State \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Entered CAPWAP DTLSTD State \r\n"));
        pSessEntry->eCurrentState = CAPWAP_DTLSTD;

        if (CapwapShutDownDTLS (pSessEntry) == OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Shutdown DTLS\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to Shutdown DTLS\r\n"));
            WSS_IF_DB_TEMP_MEM_RELEASE (pCapwapGetDB);
            return OSIX_FAILURE;
        }

        /* delete the data port session-port entries also */
        pCapwapGetDB->u4DestIp = pSessEntry->remoteIpAddr.u4_addr[0];
        pCapwapGetDB->u4DestPort = pSessEntry->u4RemoteDataPort;

        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_SESSION_PORT_ENTRY,
                                     pCapwapGetDB) == OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FSM_TRC, "Failed to Retrieve SESS Port\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to Retrieve SESS Port\r\n"));
            WSS_IF_DB_TEMP_MEM_RELEASE (pCapwapGetDB);
            return OSIX_FAILURE;
        }

        if (pCapwapGetDB->pSessDestIpEntry != NULL)
        {
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_DELETE_SESSION_PORT_ENTRY,
                                         pCapwapGetDB) == OSIX_FAILURE)
            {
                CAPWAP_TRC (CAPWAP_FSM_TRC, "Failed to Delete SESS Port\r\n");
                WSS_IF_DB_TEMP_MEM_RELEASE (pCapwapGetDB);
                return OSIX_FAILURE;
            }
        }
        /* delete the session-profid entries also, index is wtp-internal-id */
        pCapwapGetDB->CapwapGetDB.u2WtpInternalId = pSessEntry->u2IntProfileId;
        pCapwapGetDB->CapwapIsGetAllDB.bWtpProfileId = OSIX_TRUE;
        if (WssIfProcessCapwapDBMsg
            (WSS_CAPWAP_GET_SESSPROFID_ENTRY, pCapwapGetDB) == OSIX_FAILURE)
        {
            CAPWAP_TRC1 (CAPWAP_FSM_TRC,
                         "Failed to Retrieve SESS Prof for WTP Prof: %d\r\n",
                         pSessEntry->u2IntProfileId);
            WSS_IF_DB_TEMP_MEM_RELEASE (pCapwapGetDB);
            return OSIX_FAILURE;
        }

        if (pCapwapGetDB->pSessProfileIdEntry != NULL)
        {
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_DELETE_SESSPROFID_ENTRY,
                                         pCapwapGetDB) == OSIX_FAILURE)
            {
                CAPWAP_TRC1 (CAPWAP_FSM_TRC,
                             "Failed to Delete SESS Prof for WTP Prof: %d\r\n",
                             pSessEntry->u2IntProfileId);
                WSS_IF_DB_TEMP_MEM_RELEASE (pCapwapGetDB);
                return OSIX_FAILURE;
            }
        }

        pCapwapGetDB->u4DestIp = pSessEntry->remoteIpAddr.u4_addr[0];
        pCapwapGetDB->u4DestPort = pSessEntry->u4RemoteCtrlPort;

        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_DELETE_RSM,
                                     pCapwapGetDB) == OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to Process CAPWAP DB Message \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to Process CAPWAP DB Message \r\n"));
            WSS_IF_DB_TEMP_MEM_RELEASE (pCapwapGetDB);
            return OSIX_FAILURE;
        }
        CAPWAP_TRC (CAPWAP_FSM_TRC, "Entered CAPWAP IDLE State \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Entered CAPWAP IDLE State \r\n"));
        pSessEntry->eCurrentState = CAPWAP_IDLE;
    }
    else
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "SessEntry is NULL \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "SessEntry is NULL \r\n"));
        WSS_IF_DB_TEMP_MEM_RELEASE (pCapwapGetDB);
        return OSIX_FAILURE;
    }

    WSS_IF_DB_TEMP_MEM_RELEASE (pCapwapGetDB);
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : CapwapProcessCtrlRxMsg                                     *
 *                                                                           *
 * Description  : Process the received CAPWAP Fragment pkt.                  *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *
 *                                                                           *
 *****************************************************************************/
INT4
CapwapProcessCtrlRxMsg ()
{

    tCapwapRxQMsg      *pCapwapRxQMsg;
    tSegment           *pBuf = NULL;
    UINT1               u1SeqNum;
    tCapwapControlPacket capwapCtrlMsg;
    UINT4               u4DestPort, u4MsgType, u4DestIpAddr;
    tRemoteSessionManager *pSessEntry = NULL;
    tMacAddr            wtpBaseMacAddress;
    UINT2               u2IntProfileId = 0;
    UINT1               au1String[20] = { 0 };
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    INT4                i4RetVal;
    tCapwapDtlsSessEntry *pDtlsEntry = NULL;
    tWssIfPMDB          WssIfPMDB;

    CAPWAP_FN_ENTRY ();
    MEMSET (&capwapCtrlMsg, 0, sizeof (tCapwapControlPacket));
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (&wtpBaseMacAddress, 0, sizeof (tMacAddr));

    while (OsixQueRecv (gSerTaskGlobals.ctrlRxMsgQId,
                        (UINT1 *) (&pCapwapRxQMsg),
                        OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        if (pCapwapRxQMsg->u4PktType == CAPWAP_FSM_MSG)
        {
            pBuf = pCapwapRxQMsg->pRcvBuf;
            if (pBuf == NULL)
            {
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
            u4DestIpAddr = CRU_BUF_Get_U4Reserved2 (pBuf);
            u4DestPort = CRU_BUF_Get_U4Reserved1 (pBuf);

            i4RetVal = CapwapParseControlPacket (pBuf, &capwapCtrlMsg);
            if ((i4RetVal == OSIX_FAILURE) ||
                (i4RetVal == MISSING_MANDATORY_MSG_ELEMENT) ||
                (i4RetVal == UNRECOGNISED_MESSAGE_ELEMENT))
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "capwapProcessCpCtrlRxMsg:Failed to parse the"
                            "received CAPWAP control packet\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "capwapProcessCpCtrlRxMsg:Failed to parse the"
                              "received CAPWAP control packet\r\n"));
                CAPWAP_RELEASE_CRU_BUF (pBuf);
                CapwapParseStructureMemrelease (&capwapCtrlMsg);
                MemReleaseMemBlock (CAPWAP_QUEMSG_POOLID,
                                    (UINT1 *) pCapwapRxQMsg);
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
            u4MsgType = capwapCtrlMsg.capwapCtrlHdr.u4MsgType;
            u1SeqNum = capwapCtrlMsg.capwapCtrlHdr.u1SeqNum;

            MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
            pWssIfCapwapDB->u4DestIp = u4DestIpAddr;
            pWssIfCapwapDB->u4DestPort = u4DestPort;
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_RSM, pWssIfCapwapDB) ==
                OSIX_FAILURE)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Retrive"
                            "CAPWAP DB \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to Retrive" "CAPWAP DB \r\n"));
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                CAPWAP_RELEASE_CRU_BUF (pBuf);
                CapwapParseStructureMemrelease (&capwapCtrlMsg);
                MemReleaseMemBlock (CAPWAP_QUEMSG_POOLID,
                                    (UINT1 *) pCapwapRxQMsg);
                return OSIX_FAILURE;
            }
            pSessEntry = pWssIfCapwapDB->pSessEntry;
            if ((u4MsgType == CAPWAP_JOIN_REQ) && (pSessEntry == NULL))
            {
                /* WTP BASE MAc is the mandatory in Join Requet message */
                CapwapGetWtpBaseMacAddr (pBuf, &capwapCtrlMsg,
                                         wtpBaseMacAddress);
                CapwapPrintMacAddress ((UINT1 *) wtpBaseMacAddress, au1String);
                pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                pWssIfCapwapDB->CapwapIsGetAllDB.bWtpMacAddress = OSIX_TRUE;
                pWssIfCapwapDB->CapwapIsGetAllDB.bDiscMode = OSIX_TRUE;
                if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                             pWssIfCapwapDB) == OSIX_FAILURE)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Discovery Mode"
                                "from CAPWAP DB \r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Failed to Discovery Mode"
                                  "from CAPWAP DB \r\n"));
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    CAPWAP_RELEASE_CRU_BUF (pBuf);
                    CapwapParseStructureMemrelease (&capwapCtrlMsg);
                    MemReleaseMemBlock (CAPWAP_QUEMSG_POOLID,
                                        (UINT1 *) pCapwapRxQMsg);
                    return OSIX_FAILURE;
                }
                if (pWssIfCapwapDB->u1DiscMode == AUTO_DISCOVERY_MODE)
                {
                    MEMCPY (pWssIfCapwapDB->CapwapGetDB.WtpMacAddress,
                            wtpBaseMacAddress, MAC_ADDR_LEN);
                    /* Based on Recv Model Number Create the Default Profile */
                    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_INDEX_FROM_MAC,
                                                 pWssIfCapwapDB) ==
                        OSIX_FAILURE)
                    {
                        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Create RSM"
                                    "Entry \r\n");
                        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                      "Failed to Create RSM" "Entry \r\n"));
                        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                        CAPWAP_RELEASE_CRU_BUF (pBuf);
                        CapwapParseStructureMemrelease (&capwapCtrlMsg);
                        MemReleaseMemBlock (CAPWAP_QUEMSG_POOLID,
                                            (UINT1 *) pCapwapRxQMsg);
                        return OSIX_FAILURE;
                    }
                    u2IntProfileId =
                        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId;
                }
                else if (pWssIfCapwapDB->u1DiscMode == MAC_DISCOVERY_MODE)
                {
                    /* Based on the MAC Addr Get WTP Prof Internal Profile Id */
                    MEMCPY (pWssIfCapwapDB->CapwapGetDB.WtpMacAddress,
                            wtpBaseMacAddress, MAC_ADDR_LEN);
                    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_INDEX_FROM_MAC,
                                                 pWssIfCapwapDB) ==
                        OSIX_FAILURE)
                    {
                        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Create RSM"
                                    "Entry \r\n");
                        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                      "Failed to Create RSM" "Entry \r\n"));
                        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                        CAPWAP_RELEASE_CRU_BUF (pBuf);
                        CapwapParseStructureMemrelease (&capwapCtrlMsg);
                        MemReleaseMemBlock (CAPWAP_QUEMSG_POOLID,
                                            (UINT1 *) pCapwapRxQMsg);
                        return OSIX_FAILURE;
                    }
                    u2IntProfileId =
                        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId;
                }
                /* updated the WTP Internal Profile Id */
                capwapCtrlMsg.u2IntProfileId = u2IntProfileId;

                if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_RSM_FROM_INDEX,
                                             pWssIfCapwapDB) == OSIX_FAILURE)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to retrive "
                                "CAPWAP DB \r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Failed to retrive " "CAPWAP DB \r\n"));
                }
                pSessEntry = pWssIfCapwapDB->pSessEntry;

                if (pSessEntry != NULL)
                {
                    if (CapwapCleanUpSessionDetails (pSessEntry) ==
                        OSIX_FAILURE)
                    {
                        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                    "CapwapCleanUpSessionDetails Failed \r\n");
                        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                      "CapwapCleanUpSessionDetails Failed \r\n"));

                        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                        CAPWAP_RELEASE_CRU_BUF (pBuf);
                        CapwapParseStructureMemrelease (&capwapCtrlMsg);
                        MemReleaseMemBlock (CAPWAP_QUEMSG_POOLID,
                                            (UINT1 *) pCapwapRxQMsg);
                        return OSIX_FAILURE;
                    }
                    else
                    {
                        pSessEntry = NULL;
                    }
                }

                if (pSessEntry == NULL)
                {
                    /* Received the Join Request Message */
                    pWssIfCapwapDB->u4DestIp = u4DestIpAddr;
                    pWssIfCapwapDB->u4DestPort = u4DestPort;
                    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_CREATE_RSM,
                                                 pWssIfCapwapDB) ==
                        OSIX_FAILURE)
                    {
                        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Create"
                                    "RSM Entry \r\n");
                        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                      "Failed to Create" "RSM Entry \r\n"));
                        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                        CAPWAP_RELEASE_CRU_BUF (pBuf);
                        CapwapParseStructureMemrelease (&capwapCtrlMsg);
                        MemReleaseMemBlock (CAPWAP_QUEMSG_POOLID,
                                            (UINT1 *) pCapwapRxQMsg);
                        return OSIX_FAILURE;
                    }
                    pSessEntry = pWssIfCapwapDB->pSessEntry;
                    if (pSessEntry == NULL)
                    {
                        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                    "Failed to create the entry in Remote"
                                    "Session Module \r\n");
                        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                      "Failed to create the entry in Remote"
                                      "Session Module \r\n"));
                        CAPWAP_RELEASE_CRU_BUF (pBuf);;
                        MemReleaseMemBlock (CAPWAP_QUEMSG_POOLID,
                                            (UINT1 *) pCapwapRxQMsg);
                        CapwapParseStructureMemrelease (&capwapCtrlMsg);
                        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                        return OSIX_FAILURE;
                    }
                    CAPWAP_TRC (CAPWAP_FSM_TRC, "Entered CAPWAP"
                                "DTLS State \r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Entered CAPWAP" "DTLS State \r\n"));
                    pSessEntry->eCurrentState = CAPWAP_DTLS;
                    pSessEntry->u2IntProfileId = u2IntProfileId;
                    /* Get the temp entry creted in the DTLS Temp RB-Tree */
                    pWssIfCapwapDB->u4DestIp = u4DestIpAddr;
                    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_TEMPDTLS_ENTRY,
                                                 pWssIfCapwapDB) ==
                        OSIX_FAILURE)
                    {
                        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                    "Failed to Get the entry"
                                    "from Temporary DTLS table \r\n");
                        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                      "Failed to Get the entry"
                                      "from Temporary DTLS table \r\n"));
                        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                        CAPWAP_RELEASE_CRU_BUF (pBuf);
                        CapwapParseStructureMemrelease (&capwapCtrlMsg);
                        MemReleaseMemBlock (CAPWAP_QUEMSG_POOLID,
                                            (UINT1 *) pCapwapRxQMsg);
                        return OSIX_FAILURE;
                    }

                    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpCtrlDTLSStatus =
                        OSIX_TRUE;
                    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpDataDTLSStatus =
                        OSIX_TRUE;
                    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
                        u2IntProfileId;
                    if (WssIfProcessCapwapDBMsg
                        (WSS_CAPWAP_GET_DB, pWssIfCapwapDB) == OSIX_FAILURE)
                    {
                        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Get entry"
                                    "from Temporary DTLS table \r\n");
                        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                      "Failed to Get entry"
                                      "from Temporary DTLS table \r\n"));
                        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                        CAPWAP_RELEASE_CRU_BUF (pBuf);
                        CapwapParseStructureMemrelease (&capwapCtrlMsg);
                        MemReleaseMemBlock (CAPWAP_QUEMSG_POOLID,
                                            (UINT1 *) pCapwapRxQMsg);
                        return OSIX_FAILURE;
                    }
                    pSessEntry->u1DtlsFlag = pWssIfCapwapDB->CapwapGetDB.
                        u1WtpCtrlDTLSStatus;
                    pSessEntry->u1DataDtlsFlag = pWssIfCapwapDB->CapwapGetDB.
                        u1WtpDataDTLSStatus;

                    CAPWAP_TRC1 (CAPWAP_FSM_TRC,
                                 "\r\n DTLS Status AP = %d \r\n",
                                 pSessEntry->u1DtlsFlag);
                    SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4CapwapSysLogId,
                                  "\r\n DTLS Status AP = %d \r\n",
                                  pSessEntry->u1DtlsFlag));

                    pDtlsEntry = pWssIfCapwapDB->pDtlsEntry;
                    if (pDtlsEntry == NULL)
                    {
                        /* Its possible CAPWAP received plain text msg -- TBD */
                        if (pSessEntry->u1DtlsFlag == DTLS_CFG_ENABLE)
                        {
                            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Wrong Configration"
                                        "Encryption Enabled in AP Hence Plain Text"
                                        "packets are not accepted\r\nn");
                            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                          "Wrong Configration"
                                          "Encryption Enabled in AP Hence Plain Text"
                                          "packets are not accepted\r\nn"));
                            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                            CAPWAP_RELEASE_CRU_BUF (pBuf);
                            CapwapParseStructureMemrelease (&capwapCtrlMsg);
                            MemReleaseMemBlock (CAPWAP_QUEMSG_POOLID,
                                                (UINT1 *) pCapwapRxQMsg);
                            return OSIX_FAILURE;
                        }
                        pSessEntry->u4DtlsSessId = 0;
                        /* return OSIX_FAILURE; */
                    }
                    else
                    {
                        pSessEntry->u4DtlsSessId = pDtlsEntry->u4DtlsId;
                        /* As the DTLS id updated in the Remote session Manager 
                         * Delete the entry from the  temporarty DTLS Tabler */
                        pWssIfCapwapDB->u4DestIp = u4DestIpAddr;
                    }
                    pWssIfCapwapDB->u2CapwapRSMId =
                        pSessEntry->u2CapwapRSMIndex;
                    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
                        pSessEntry->u2IntProfileId;
                    if (WssIfProcessCapwapDBMsg
                        (WSS_CAPWAP_CREATE_SESSPROFID_ENTRY,
                         pWssIfCapwapDB) == OSIX_FAILURE)
                    {
                        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                    "Failed to Create the Entry in CAPWAP session"
                                    "and WTP Porfiled Id MAP Table \r\n");
                        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                      "Failed to Create the Entry in CAPWAP session"
                                      "and WTP Porfiled Id MAP Table \r\n"));
                        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                        CAPWAP_RELEASE_CRU_BUF (pBuf);
                        CapwapParseStructureMemrelease (&capwapCtrlMsg);
                        MemReleaseMemBlock (CAPWAP_QUEMSG_POOLID,
                                            (UINT1 *) pCapwapRxQMsg);
                        return OSIX_FAILURE;
                    }
                    pSessEntry->remoteIpAddr.u4_addr[0] = u4DestIpAddr;
                    pSessEntry->u4RemoteCtrlPort = u4DestPort;

                    /* Increment the current serving APs in CAPWAP DB */
                    pWssIfCapwapDB->CapwapIsGetAllDB.bActiveWtps = OSIX_TRUE;
                    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_SET_DB,
                                                 pWssIfCapwapDB) ==
                        OSIX_FAILURE)
                    {
                        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                    "Failed to SET CAPWAP DB \r\n");
                        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                      "Failed to SET CAPWAP DB \r\n"));
                        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                        CAPWAP_RELEASE_CRU_BUF (pBuf);
                        CapwapParseStructureMemrelease (&capwapCtrlMsg);
                        MemReleaseMemBlock (CAPWAP_QUEMSG_POOLID,
                                            (UINT1 *) pCapwapRxQMsg);
                        return OSIX_FAILURE;
                    }
                }
            }
            else if (pSessEntry == NULL)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Received the packet from unknown source."
                            "Drop the packet  \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Received the packet from unknown source."
                              "Drop the packet  \r\n"));
                CAPWAP_RELEASE_CRU_BUF (pBuf);
                MemReleaseMemBlock (CAPWAP_QUEMSG_POOLID,
                                    (UINT1 *) pCapwapRxQMsg);
                CapwapParseStructureMemrelease (&capwapCtrlMsg);
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
            else if (pSessEntry->lastProcesseddSeqNum == u1SeqNum)
            {
                CAPWAP_TRC (CAPWAP_MGMT_TRC,
                            "capwapProcessCtrlRxMsg: Last processed seq num is"
                            "same as received seq number\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "capwapProcessCtrlRxMsg: Last processed seq num is"
                              "same as received seq number\r\n"));
                CapwapRetransmitPacket (pSessEntry);
                CapwapParseStructureMemrelease (&capwapCtrlMsg);
                CAPWAP_RELEASE_CRU_BUF (pBuf);
                MemReleaseMemBlock (CAPWAP_QUEMSG_POOLID,
                                    (UINT1 *) pCapwapRxQMsg);
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_SUCCESS;
            }
            capwapCtrlMsg.u2IntProfileId = pSessEntry->u2IntProfileId;
            if (capwapCtrlMsg.u4ReturnCode == UNRECOGNISED_MESSAGE_REQUEST)
            {
                /* Transmit a dummy message response message 
                 * with unrecognised message request return code */
                CapwapProcessUnrecognisedRequest (pBuf, &capwapCtrlMsg,
                                                  pSessEntry);

            }
            else if ((pSessEntry->eCurrentState < CAPWAP_MAX_STATE
                      && u4MsgType < CAPWAP_MAX_EVENTS)
                     &&
                     (gCapwapStateMachine[pSessEntry->eCurrentState][u4MsgType]
                      (pBuf, &capwapCtrlMsg, pSessEntry) == OSIX_FAILURE))
            {
#if 0
                CAPWAP_RELEASE_CRU_BUF (pBuf);
                MemReleaseMemBlock (CAPWAP_QUEMSG_POOLID,
                                    (UINT1 *) pCapwapRxQMsg);
                CapwapParseStructureMemrelease (&capwapCtrlMsg);
                if (u4MsgType == CAPWAP_JOIN_REQ)
                {
                    MEMSET (&WssIfPMDB, 0, sizeof (WssIfPMDB));
                    WssIfPMDB.u2ProfileId = u2IntProfileId;
                    WssIfPMDB.tWtpPmAttState.wssPmWLCStatsSetDB.
                        bWssPmWLCCapwapJoinUnsuccReqProcessed = OSIX_TRUE;
                    WssIfPMDB.tWtpPmAttState.wssPmWLCStatsSetDB.
                        bWssPmWLCCapwapJoinReasonForLastUnsuccAtt = 1;
                    WssIfPMDB.tWtpPmAttState.wssPmWLCStatsSetDB.
                        bWssPmWLCCapwapJoinLastUnsuccAttTime = OSIX_TRUE;
                    if (WssIfProcessPMDBMsg (WSS_PM_WLC_CAPWAP_STATS_SET,
                                             &WssIfPMDB) != OSIX_SUCCESS)
                    {
                        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                    "Failed to update Join Req Stats to PM \n");
                        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                      "Failed to update Join Req Stats to PM \n"));
                    }
                }
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Failed to Enter CAPWAP State Machine \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to Enter CAPWAP State Machine \r\n"));
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
#else
                if (u4MsgType == CAPWAP_JOIN_REQ)
                {
                    CapwapGetWtpBaseMacAddr (pBuf, &capwapCtrlMsg,
                                             wtpBaseMacAddress);
                    CapwapPrintMacAddress ((UINT1 *) wtpBaseMacAddress,
                                           au1String);
                    /* Based on the MAC Addr Get WTP Prof Internal Profile Id */
                    MEMCPY (pWssIfCapwapDB->CapwapGetDB.WtpMacAddress,
                            wtpBaseMacAddress, MAC_ADDR_LEN);
                    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_INDEX_FROM_MAC,
                                                 pWssIfCapwapDB) ==
                        OSIX_FAILURE)
                    {
                        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Create RSM"
                                    "Entry \r\n");
                        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                        CAPWAP_RELEASE_CRU_BUF (pBuf);
                        CapwapParseStructureMemrelease (&capwapCtrlMsg);
                        MemReleaseMemBlock (CAPWAP_QUEMSG_POOLID,
                                            (UINT1 *) pCapwapRxQMsg);
                        return OSIX_FAILURE;
                    }
                    u2IntProfileId =
                        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId;

                    MEMSET (&WssIfPMDB, 0, sizeof (WssIfPMDB));
                    WssIfPMDB.u2ProfileId = u2IntProfileId;
                    WssIfPMDB.tWtpPmAttState.wssPmWLCStatsSetDB.
                        bWssPmWLCCapwapJoinUnsuccReqProcessed = OSIX_TRUE;
                    WssIfPMDB.tWtpPmAttState.wssPmWLCStatsSetDB.
                        bWssPmWLCCapwapJoinReasonForLastUnsuccAtt = 1;
                    WssIfPMDB.tWtpPmAttState.wssPmWLCStatsSetDB.
                        bWssPmWLCCapwapJoinLastUnsuccAttTime = OSIX_TRUE;
                    if (WssIfProcessPMDBMsg (WSS_PM_WLC_CAPWAP_STATS_SET,
                                             &WssIfPMDB) != OSIX_SUCCESS)
                    {
                        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                    "Failed to update Join Req Stats to PM \n");
                    }
                }
                CAPWAP_RELEASE_CRU_BUF (pBuf);
                MemReleaseMemBlock (CAPWAP_QUEMSG_POOLID,
                                    (UINT1 *) pCapwapRxQMsg);
                CapwapParseStructureMemrelease (&capwapCtrlMsg);
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Failed to Enter CAPWAP State Machine \r\n");
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
#endif
            }
        }
        CAPWAP_RELEASE_CRU_BUF (pBuf);
        MemReleaseMemBlock (CAPWAP_QUEMSG_POOLID, (UINT1 *) pCapwapRxQMsg);
        /* coverity fix begin */
#if CAP_WANTED
        capwapCtrlMsg.numMandatoryElements = 0;
        capwapCtrlMsg.numOptionalElements = 0;
#endif
        /* coverity fix end */
        CapwapParseStructureMemrelease (&capwapCtrlMsg);
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : CapwapRetransmitPacket                                     *
 *                                                                           *
 * Description  : This functino used to retransmit the last processed packet *
 *                based on the sequence number.                              *
 *                CTRL Rx queue                                              *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
CapwapRetransmitPacket (tRemoteSessionManager * pSessEntry)
{

    CAPWAP_FN_ENTRY ();
    switch (pSessEntry->eCurrentState)
    {
        case CAPWAP_DISCOVERY:
            break;
        case CAPWAP_DTLS:
            break;
        case CAPWAP_JOIN:
            /* Stop chnage state pending timer.Configuration status response 
             * not reached the WTP. re-transmit the configuration status resp
             * message and start the timer again */
            if ((CapwapTmrStop (pSessEntry, CAPWAP_WAITJOIN_TMR)) !=
                OSIX_SUCCESS)
            {
                return OSIX_FAILURE;
            }
            if (CapwapTxMessage (pSessEntry->lastTransmittedPkt,
                                 pSessEntry,
                                 pSessEntry->lastTransmittedPktLen,
                                 WSS_CAPWAP_802_11_CTRL_PKT) == OSIX_SUCCESS)
            {
                pSessEntry->u1RetransmitCount++;
                /*Start the Wait Join Pending Timer again */
                CapwapTmrStart (pSessEntry,
                                CAPWAP_WAITJOIN_TMR, CAPWAP_WAITJOIN_TIMEOUT);
            }
            break;
        case CAPWAP_CONFIGURE:
            if ((CapwapTmrStop (pSessEntry, CAPWAP_CHANGESTATE_PENDING_TMR)) !=
                OSIX_SUCCESS)
            {
                return OSIX_FAILURE;
            }
            if (CapwapTxMessage (pSessEntry->lastTransmittedPkt,
                                 pSessEntry,
                                 pSessEntry->lastTransmittedPktLen,
                                 WSS_CAPWAP_802_11_CTRL_PKT) == OSIX_SUCCESS)
            {
                pSessEntry->u1RetransmitCount++;
                /*Start the Wait Join Pending Timer again */
                CapwapTmrStart (pSessEntry,
                                CAPWAP_CHANGESTATE_PENDING_TMR,
                                CHANGESTATE_PENDING_TIMEOUT);
            }
            break;
        case CAPWAP_DATACHECK:
            if ((CapwapTmrStop (pSessEntry, CAPWAP_DATACHECK_TMR)) !=
                OSIX_SUCCESS)
            {
                return OSIX_FAILURE;
            }
            if (CapwapTxMessage (pSessEntry->lastTransmittedPkt,
                                 pSessEntry,
                                 pSessEntry->lastTransmittedPktLen,
                                 WSS_CAPWAP_802_11_CTRL_PKT) == OSIX_SUCCESS)
            {
                pSessEntry->u1RetransmitCount++;
                /*Start the Wait Join Pending Timer again */
                CapwapTmrStart (pSessEntry,
                                CAPWAP_DATACHECK_TMR, CAPWAP_DATACHECK_TIMEOUT);
            }
            break;
            /* The following switch case label were added intentionally during coverity tool execution.
               Current these have no statements. To be added later based on the need */
        case CAPWAP_RUN:
            break;
        case CAPWAP_START:
            break;
        case CAPWAP_IDLE:
            break;
        case CAPWAP_IMAGEDATA:
            break;
        case CAPWAP_RESET:
            break;
        case CAPWAP_SULKING:
            break;
        case CAPWAP_DTLSTD:
            break;
        case CAPWAP_DEAD:
            break;
        case CAPWAP_MAX_STATE:
            break;
        default:
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "FSM is in unknown state \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "FSM is in unknown state \r\n"));

    }
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

INT4
CapwapProcessUnrecognisedRequest (tSegment * pBuf,
                                  tCapwapControlPacket * pReqMsg,
                                  tRemoteSessionManager * pSessEntry)
{
    UINT4               u4MsgLen = 0;
    INT4                i4Status;
    tUnknownRsp         UnknownRsp;
    tCRU_BUF_CHAIN_HEADER *pTxBuf;
    UNUSED_PARAM (pBuf);

    CAPWAP_FN_ENTRY ();

    if (CapwapConstructCpHeader (&UnknownRsp.capwapHdr) == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapProcessUnrecognisedRequest:Failed to"
                    "construct the capwap Header\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "CapwapProcessUnrecognisedRequest:Failed to"
                      "construct the capwap Header\n"));
        return OSIX_FAILURE;
    }
    if (CapwapUpdateResultCode (&UnknownRsp.resultCode, pReqMsg->u4ReturnCode,
                                &u4MsgLen) == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to get Mandatory Resp Message Elements \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to get Mandatory Resp Message Elements \r\n"));
        return OSIX_FAILURE;
    }

    /* update Control Header */
    UnknownRsp.u1SeqNum = pReqMsg->capwapCtrlHdr.u1SeqNum;
    UnknownRsp.u2CapwapMsgElemenLen =
        (UINT2) (u4MsgLen + CAPWAP_CMN_MSG_ELM_HEAD_LEN);
    UnknownRsp.u1NumMsgBlocks = 0;    /* flags always zero */
/* coverity fix begin */
#if CAP_WANTED
    UnknownRsp.u4MsgType = 0;    /* initialiazed to 0, need to be
                                   updated with correct value */
#endif
    UnknownRsp.u4MsgType = pReqMsg->capwapCtrlHdr.u4MsgType;
/* coverity fix end */

    pTxBuf = CAPWAP_ALLOCATE_CRU_BUF (CAPWAP_MAX_PKT_LEN, 0);
    if (pTxBuf == NULL)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapProcessDiscoveryRequest:Memory Alloc Failed\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "CapwapProcessDiscoveryRequest:Memory Alloc Failed\n"));
        return OSIX_FAILURE;
    }
    if ((CapwapAssembleUnknownRsp (&UnknownRsp, pTxBuf)) == OSIX_FAILURE)
    {
        CAPWAP_RELEASE_CRU_BUF (pTxBuf);
        return OSIX_FAILURE;
    }

    i4Status = CapwapTxMessage (pTxBuf,
                                pSessEntry,
                                (UINT4) (UnknownRsp.u2CapwapMsgElemenLen +
                                         CAPWAP_MSG_ELE_LEN),
                                WSS_CAPWAP_802_11_CTRL_PKT);
    if (i4Status == OSIX_SUCCESS)
    {
        /* Update the last transmitted sequence number */
        pSessEntry->lastTransmitedSeqNum++;
        /* store the packet buffer pointer in
         * the remote session DB copy the pointer */
        pSessEntry->lastTransmittedPkt = pTxBuf;
        pSessEntry->lastTransmittedPktLen =
            (UINT4) (UnknownRsp.u2CapwapMsgElemenLen + CAPWAP_MSG_ELE_LEN);
        /*Start the Retransmit timer */
        CapwapTmrStart (pSessEntry, CAPWAP_RETRANSMIT_INTERVAL_TMR,
                        MAX_RETRANSMIT_INTERVAL_TIMEOUT);
    }
    else
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Transmit the packet \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to Transmit the packet \r\n"));
        CAPWAP_RELEASE_CRU_BUF (pTxBuf);
        return OSIX_FAILURE;
    }
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : WtpEnterJoinState                                          *
 *                                                                           *
 * Description  : This function called by the timer module after expiry      *
 *                of DiscoveryInterval timer.                                *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
WtpEnterJoinState (tACInfoAfterDiscovery * pAcInfo)
{
    UINT1              *pu1TxBuf = NULL;
    tJoinReq           *pJoinReq = NULL;
    tRemoteSessionManager *pSessEntry = NULL;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    UINT1               u1IsNotLinear = OSIX_FALSE;
    UINT2               u2Len;
    UINT1              *pu1TempBuf = NULL;

#ifdef WTP_WANTED
    tWssIfPMDB          WssIfPMDB;
#endif

    CAPWAP_FN_ENTRY ();

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    pJoinReq = (tJoinReq *) (VOID *) UtlShMemAllocJoinReqBuf ();
    if (pJoinReq == NULL)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "WtpEnterJoinState:- "
                    "UtlShMemAllocJoinReqBuf returned failure\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "WtpEnterJoinState:- "
                      "UtlShMemAllocJoinReqBuf returned failure\n"));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    MEMSET (pJoinReq, 0, sizeof (tJoinReq));

    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_RSM, pWssIfCapwapDB) ==
        OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Retrive CAPWAP DB \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to Retrive CAPWAP DB \r\n"));
        UtlShMemFreeJoinReqBuf ((UINT1 *) pJoinReq);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    pSessEntry = pWssIfCapwapDB->pSessEntry;
    /* update the Remote session module with ACs Ip address and Port details */
    pSessEntry->remoteIpAddr.u4_addr[0] = pAcInfo->incomingAddr.u4_addr[0];
    pSessEntry->u4RemoteCtrlPort = pAcInfo->u4DestPort;

    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = pSessEntry->u2IntProfileId;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpCtrlDTLSStatus = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpDataDTLSStatus = OSIX_TRUE;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
        == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "WtpEnterJoinState:Failed to get WTP Capability \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "WtpEnterJoinState:Failed to get WTP Capability \r\n"));
        UtlShMemFreeJoinReqBuf ((UINT1 *) pJoinReq);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    pSessEntry->u1DtlsFlag = pWssIfCapwapDB->CapwapGetDB.u1WtpCtrlDTLSStatus;
    pSessEntry->u1DataDtlsFlag =
        pWssIfCapwapDB->CapwapGetDB.u1WtpDataDTLSStatus;

    /* update the AC Name */
    u2Len = (UINT2) (STRLEN (pAcInfo->wlcName));
    MEMCPY (pSessEntry->au1Name, pAcInfo->wlcName, u2Len);
    CAPWAP_TRC (CAPWAP_FSM_TRC, "Entered CAPWAP DTLS State \r\n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                  "Entered CAPWAP DTLS State \r\n"));
    pSessEntry->eCurrentState = CAPWAP_DTLS;

    if (CapwapStartDTLS (pAcInfo->incomingAddr.u4_addr[0]) == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to establish the DTLS connection \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to establish the DTLS connection \r\n"));
        /* Choose the next AC in the list to connect */
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        UtlShMemFreeJoinReqBuf ((UINT1 *) pJoinReq);
        return OSIX_FAILURE;
    }

    /* DTLS connections is successful, Change the current state to JOIN */
    CAPWAP_TRC (CAPWAP_FSM_TRC, "Entered CAPWAP JOIN State \r\n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                  "Entered CAPWAP JOIN State \r\n"));
    pSessEntry->eCurrentState = CAPWAP_JOIN;
    pSessEntry->ePrevState = CAPWAP_JOIN;
    if (CapwapConstructJoinReq (pJoinReq) == OSIX_SUCCESS)
    {
        pJoinReq->u1SeqNum = (UINT1) (pSessEntry->lastTransmitedSeqNum + 1);

        pBuf = CAPWAP_ALLOCATE_CRU_BUF (CAPWAP_MAX_PKT_LEN, 0);
        if (pBuf == NULL)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "wtpEnterJoinState:Memory Allocation Failed \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "wtpEnterJoinState:Memory Allocation Failed \r\n"));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            UtlShMemFreeJoinReqBuf ((UINT1 *) pJoinReq);
            return OSIX_FAILURE;
        }
        pu1TxBuf = CAPWAP_BUF_IF_LINEAR (pBuf, 0, CAPWAP_MAX_PKT_LEN);
        if (pu1TxBuf == NULL)
        {
            pu1TxBuf = (UINT1 *) MemAllocMemBlk (CAPWAP_PKTBUF_POOLID);
            CAPWAP_COPY_FROM_BUF (pBuf, pu1TxBuf, 0, CAPWAP_MAX_PKT_LEN);
            u1IsNotLinear = OSIX_TRUE;
        }
        if (CapwapAssembleJoinRequest (pJoinReq->u2CapwapMsgElemenLen,
                                       pJoinReq, pu1TxBuf) == OSIX_FAILURE)
        {
            CAPWAP_RELEASE_CRU_BUF (pBuf);
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "wtpEnterJoinState:Failed to Assemble the packet \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "wtpEnterJoinState:Failed to Assemble the packet \r\n"));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            UtlShMemFreeJoinReqBuf ((UINT1 *) pJoinReq);
            return OSIX_FAILURE;
        }
        pu1TempBuf = pu1TxBuf + CAPWAP_MSG_OFFSET;
        CAPWAP_GET_2BYTE (pJoinReq->u2CapwapMsgElemenLen, pu1TempBuf);
        if (u1IsNotLinear == OSIX_TRUE)
        {
            /* If the CRU buffer memory is not linear */
            CAPWAP_COPY_TO_BUF (pBuf, pu1TxBuf, 0, CAPWAP_MAX_PKT_LEN);
            MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pu1TxBuf);
        }

        /* store the packet buffer pointer in
         * the remote session DB copy the pointer */
        pSessEntry->lastTransmittedPkt = pBuf;
        pSessEntry->lastTransmittedPktLen =
            (UINT4) (pJoinReq->u2CapwapMsgElemenLen + CAPWAP_MSG_ELE_LEN);
        /*Start the Retransmit timer */
        CapwapTmrStart (pSessEntry, CAPWAP_RETRANSMIT_INTERVAL_TMR,
                        MAX_RETRANSMIT_INTERVAL_TIMEOUT);
        if (CapwapTxMessage (pBuf,
                             pSessEntry,
                             (UINT4) (pJoinReq->u2CapwapMsgElemenLen +
                                      CAPWAP_MSG_ELE_LEN),
                             WSS_CAPWAP_802_11_CTRL_PKT) == OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "wtpEnterJoinState:Failed to Transmit the packet \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "wtpEnterJoinState:Failed to Transmit the packet \r\n"));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            UtlShMemFreeJoinReqBuf ((UINT1 *) pJoinReq);
            return OSIX_FAILURE;
        }
        /* Copy the JoinRequest Session Id */
        MEMCPY (pSessEntry->JoinSessionID, pJoinReq->sessionID, MAC_LENGTH);

        /* update the seq number */
        pSessEntry->lastTransmitedSeqNum++;
        pSessEntry->u1RetransmitCount = 0;
        pSessEntry->wtpCapwapStats.joinReqSent++;
#ifdef WTP_WANTED
        MEMSET (&WssIfPMDB, 0, sizeof (tWssIfPMDB));
        WssIfPMDB.u2ProfileId = pSessEntry->u2IntProfileId;
        if (WssIfProcessPMDBMsg
            (WSS_PM_WTP_EVT_VSP_CAPWAP_STATS_GET, &WssIfPMDB) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to update Join Req Stats to PM \n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to update Join Req Stats to PM \n"));
        }

        WssIfPMDB.wtpVSPCPWPWtpStats.joinReqSent =
            pSessEntry->wtpCapwapStats.joinReqSent;
        if (WssIfProcessPMDBMsg
            (WSS_PM_WTP_EVT_VSP_CAPWAP_STATS_SET, &WssIfPMDB) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to update Join Req Stats to PM \n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to update Join Req Stats to PM \n"));

        }
#endif
    }
    else
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Fail to construct Join Request \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Fail to construct Join Request \r\n"));

        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        UtlShMemFreeJoinReqBuf ((UINT1 *) pJoinReq);
        return OSIX_FAILURE;
    }

    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    UtlShMemFreeJoinReqBuf ((UINT1 *) pJoinReq);
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : CapwapConstructJoinReq                                     *
 *                                                                           *
 * Description  : This function constructs the join request packet including *
 *                capwap header, control header and message element.         *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
CapwapConstructJoinReq (tJoinReq * pJoinRequest)
{
    UINT4               u4MsgLen = 0;

    CAPWAP_FN_ENTRY ();

    /* update CAPWAP Header */
    if (CapwapConstructCpHeader (&pJoinRequest->capwapHdr) == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "capwapConstructJoinReq:Fail to construct the capwap Header\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "capwapConstructJoinReq:Fail to construct the capwap Header\n"));

        return OSIX_FAILURE;
    }

    /* update Message elements */
    if ((CapwapGetLocationData (&pJoinRequest->wtpLocation, &u4MsgLen) ==
         OSIX_FAILURE) ||
        (CapwapGetWtpBoardData (&pJoinRequest->wtpBoardData, &u4MsgLen) ==
         OSIX_FAILURE) ||
        (CapwapGetWtpDescriptor (&pJoinRequest->wtpDescriptor, &u4MsgLen) ==
         OSIX_FAILURE) ||
        (CapwapGetWtpName (&pJoinRequest->wtpName, &u4MsgLen) ==
         OSIX_FAILURE) ||
        (CapwapGetJoinSessionId (pJoinRequest->sessionID, &u4MsgLen) ==
         OSIX_FAILURE) ||
        (CapwapGetWtpFrameTunnelMode (&pJoinRequest->wtpTunnelMode,
                                      &u4MsgLen) == OSIX_FAILURE) ||
        (CapwapGetWtpMacType (&pJoinRequest->wtpMacType, &u4MsgLen) ==
         OSIX_FAILURE) ||
        (CapwapGetEcnSupport (&pJoinRequest->ecnSupport, &u4MsgLen) ==
         OSIX_FAILURE) ||
        (CapwapGetLocalIpAddr (&pJoinRequest->incomingAddress, &u4MsgLen) ==
         OSIX_FAILURE))
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Fail to get Mandatory Join Req Message Elements \n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Fail to get Mandatory Join Req Message Elements \n"));
        return OSIX_FAILURE;
    }

    /* update Optional Message Elements */
    /*if  ((CapwapGetTransProtocol (&pJoinRequest->transProto, &u4MsgLen) ==
     * OSIX_FAILURE) ||
     (CapwapGetMaxMessageLen (&pJoinRequest->msgLength, &u4MsgLen) ==
     OSIX_FAILURE) ||
     (CapwapGetRebootStats (&pJoinRequest->rebootStats, &u4MsgLen) ==
     OSIX_FAILURE) ||
     (CapwapGetVendorPayload (&pJoinRequest->vendSpec, &u4MsgLen) ==
     OSIX_FAILURE))
     {
     CAPWAP_TRC (CAPWAP_FAILURE_TRC,
     "Failed to get Mandatory Join Request Message Elements \n");
     return OSIX_SUCCESS;
     } */
    /* update Control Header */
    pJoinRequest->u2CapwapMsgElemenLen =
        (UINT2) (u4MsgLen + CAPWAP_CMN_MSG_ELM_HEAD_LEN);
    pJoinRequest->u1NumMsgBlocks = 0;    /* flags */

    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : MakeDiscResponseEntry                             */
/*  Description     : This function used to update database of          */
/*                    Discovery response received.If the received       */
/*                    discovey responses exceeds MAX_DISC_RESPONSE      */
/*                    it will start replacing from the beginning of     */
/*                    of the array table                                */
/*  Input(s)        : None                                              */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_FAILURE or OSIX_SUCCESS                      */
/************************************************************************/
INT4
MakeDiscResponseEntry (tSegment * pBuf, tDiscRsp * pDiscResponse)
{

    UINT2               u2DiscRespNum;
    UINT2               u2FreeIndex = 0;
    UINT2               u2FreeEntryFlag = OSIX_FALSE;
    UINT1               u1Count = 0;
    static int          u2CurrIndex = 0;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT1               u1EntryFound = 0;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    CAPWAP_FN_ENTRY ();
    for (u2DiscRespNum = 0; u2DiscRespNum < MAX_DISC_RESPONSE; u2DiscRespNum++)
    {
        if (MEMCMP (gACInfoAfterDiscovery[u2DiscRespNum].wlcName,
                    pDiscResponse->acName.wlcName, CAPWAP_WLC_NAME_SIZE) == 0)
        {
            u1EntryFound = 1;
            u2FreeIndex = u2DiscRespNum;
            u2FreeEntryFlag = OSIX_TRUE;
            break;
        }
    }

    if (u1EntryFound != 1)
    {
        for (u2DiscRespNum = 0; u2DiscRespNum < MAX_DISC_RESPONSE;
             u2DiscRespNum++)
        {
            if ((gACInfoAfterDiscovery[u2DiscRespNum].u1Used == OSIX_FALSE) &&
                (u2FreeEntryFlag == OSIX_FALSE))
            {
                /*Found the free index. note it */
                u2FreeIndex = u2DiscRespNum;
                u2FreeEntryFlag = OSIX_TRUE;
            }
        }
    }

    if (u2FreeEntryFlag == OSIX_TRUE)
    {

        /* update the AC Descriptor */
        MEMCPY (&gACInfoAfterDiscovery[u2FreeIndex].acDesc,
                &pDiscResponse->acDesc, pDiscResponse->acDesc.u2MsgEleLen + 5);
        /* update the AC Name */
/* coverity 12511 fix begin */
        strncpy ((CHR1 *) gACInfoAfterDiscovery[u2FreeIndex].wlcName,
                 (CHR1 *) pDiscResponse->acName.wlcName, CAPWAP_WLC_NAME_SIZE);
/* coverity 12511 fix end */
        /* update source Ip and WTP count */
        gACInfoAfterDiscovery[u2FreeIndex].ipAddr.u4_addr[0] =
            pDiscResponse->ctrlAddr.ipAddr.u4_addr[0];
        gACInfoAfterDiscovery[u2FreeIndex].wtpCount =
            pDiscResponse->ctrlAddr.wtpCount;
        /* update Radio Info */
        MEMCPY (&gACInfoAfterDiscovery[u2FreeIndex].RadioIfInfo,
                &pDiscResponse->RadioIfInfo,
                pDiscResponse->RadioIfInfo.u2MessageLength + 5);

        gACInfoAfterDiscovery[u2FreeIndex].incomingAddr.u4_addr[0] =
            CRU_BUF_Get_U4Reserved2 (pBuf);
        gACInfoAfterDiscovery[u2FreeIndex].u4DestPort =
            CRU_BUF_Get_U4Reserved1 (pBuf);

        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = 0;
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpCtrlDTLSStatus = OSIX_TRUE;
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
            == OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to create the entry in DTLS table \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to create the entry in DTLS table \r\n"));

            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
        /* update Dtls Status */
        gACInfoAfterDiscovery[u2FreeIndex].u1CtrlDtlsStatus =
            pWssIfCapwapDB->CapwapGetDB.u1WtpCtrlDTLSStatus;
        /* Update the AC Ip list in the Global Structure */
        if (pDiscResponse->ipv4List.u2MsgEleLen != 0)
        {
            gACInfoAfterDiscovery[u2FreeIndex].acIPv4List.u2MsgEleLen =
                pDiscResponse->ipv4List.u2MsgEleLen;
            for (u1Count = 0;
                 u1Count < (pDiscResponse->ipv4List.u2MsgEleLen / 4); u1Count++)
            {
                gACInfoAfterDiscovery[u2FreeIndex].acIPv4List.
                    ipAddr[u1Count].u4_addr[0] =
                    pDiscResponse->ipv4List.ipAddr[u1Count].u4_addr[0];
            }
        }
        gACInfoAfterDiscovery[u2FreeIndex].u1Used = OSIX_TRUE;
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_SUCCESS;

    }
    if (u2DiscRespNum == MAX_DISC_RESPONSE)
    {
        u2DiscRespNum = (UINT2) u2CurrIndex;

        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = 0;
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpCtrlDTLSStatus = OSIX_TRUE;
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
            == OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to create the entry in DTLS table \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to create the entry in DTLS table \r\n"));

            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
        /* update Dtls Status */
        gACInfoAfterDiscovery[u2FreeIndex].u1CtrlDtlsStatus =
            pWssIfCapwapDB->CapwapGetDB.u1WtpCtrlDTLSStatus;

        u2CurrIndex++;
        if (u2CurrIndex == MAX_DISC_RESPONSE)
        {
            u2CurrIndex = 0;
        }

        if (u2DiscRespNum < MAX_DISC_RESPONSE)
        {
            /* update the AC Descriptor */
            MEMCPY (&gACInfoAfterDiscovery[u2FreeIndex].acDesc,
                    &pDiscResponse->acDesc,
                    pDiscResponse->acDesc.u2MsgEleLen + AC_INFO_AFTER_DISC);
            /* update the AC Name */
/* coverity fix begin */
            strncpy ((CHR1 *) gACInfoAfterDiscovery[u2FreeIndex].wlcName,
                     (CHR1 *) pDiscResponse->acName.wlcName,
                     CAPWAP_WLC_NAME_SIZE);
/* coverity fix end */
            /* update source Ip and WTP count */
            gACInfoAfterDiscovery[u2FreeIndex].ipAddr.u4_addr[0] =
                pDiscResponse->ctrlAddr.ipAddr.u4_addr[0];
            gACInfoAfterDiscovery[u2FreeIndex].wtpCount =
                pDiscResponse->ctrlAddr.wtpCount;
            /* update Radio Info */
            MEMCPY (&gACInfoAfterDiscovery[u2FreeIndex].RadioIfInfo,
                    &pDiscResponse->RadioIfInfo, pDiscResponse->RadioIfInfo.
                    u2MessageLength + AC_INFO_AFTER_DISC);
            gACInfoAfterDiscovery[u2FreeIndex].incomingAddr.u4_addr[0] =
                CRU_BUF_Get_U4Reserved2 (pBuf);
            gACInfoAfterDiscovery[u2FreeIndex].u4DestPort =
                CRU_BUF_Get_U4Reserved1 (pBuf);
            gACInfoAfterDiscovery[u2FreeIndex].u1Used = OSIX_TRUE;

        }
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

INT4
CapwapGetBestACDetails (tACInfoAfterDiscovery * pAcInfo)
{
    UINT1               u1Index, u1WtpCountTemp = 0, u1WtpCount = 0;
    UINT1               u1Primary = 0;

#ifdef WTP_WANTED
    UINT1               u1IndexCnt = 0;
#endif
    for (u1Index = 0; u1Index < MAX_DISC_RESPONSE; u1Index++)
    {
        if (gACInfoAfterDiscovery[u1Index].u1Used == OSIX_TRUE)
        {
            /* Maximum WTPs supported - WTPs currently serving */
            u1WtpCountTemp =
                (UINT1) (gACInfoAfterDiscovery[u1Index].acDesc.maxWtp -
                         gACInfoAfterDiscovery[u1Index].wtpCount);
            if (u1WtpCountTemp <= u1WtpCount)
            {
                u1WtpCount = u1WtpCountTemp;
                u1Primary = u1Index;
            }
            else if (u1WtpCount == 0)
            {
                u1WtpCount = u1WtpCountTemp;
                u1Primary = u1Index;
            }
        }
    }

#ifdef WTP_WANTED
    for (u1Index = 0; u1Index < MAX_DISC_RESPONSE; u1Index++)
    {
        if (gACInfoAfterDiscovery[u1Index].u1Used == OSIX_TRUE)
        {
            for (u1IndexCnt = 0; u1IndexCnt < MAX_STATIC_DISC; u1IndexCnt++)
            {
                if (MEMCMP (gACInfoAfterDiscovery[u1Index].wlcName,
                            gCapwapStaticDiscInfo[u1IndexCnt].wlcName,
                            CAPWAP_WLC_NAME_SIZE) == 0)
                {
                    if (u1WtpCountTemp <= u1WtpCount)
                    {
                        u1WtpCount = gCapwapStaticDiscInfo[u1IndexCnt].
                            u1Priority;
                        u1Primary = u1Index;

                    }
                    else if (u1WtpCount == 0)
                    {
                        u1WtpCount = gCapwapStaticDiscInfo[u1IndexCnt].
                            u1Priority;
                        u1Primary = u1Index;
                    }
                }
            }
        }
    }
#endif
    /* update the AC Descriptor */
    MEMCPY (&pAcInfo->acDesc, &gACInfoAfterDiscovery[u1Primary].acDesc,
            gACInfoAfterDiscovery[u1Primary].acDesc.u2MsgEleLen + 5);
    /* update the AC Name */
    /* coverity fix begin */
    strncpy ((CHR1 *) pAcInfo->wlcName,
             (CHR1 *) gACInfoAfterDiscovery[u1Primary].wlcName,
             CAPWAP_WLC_NAME_SIZE);
    /* coverity fix end */
    /* update source Ip and WTP count */
    pAcInfo->ipAddr.u4_addr[0] = gACInfoAfterDiscovery[u1Primary].
        ipAddr.u4_addr[0];
    pAcInfo->wtpCount = gACInfoAfterDiscovery[u1Primary].wtpCount;
    /* update Radio Info */
    MEMCPY (&pAcInfo->RadioIfInfo, &gACInfoAfterDiscovery[u1Primary].
            RadioIfInfo,
            gACInfoAfterDiscovery[u1Primary].RadioIfInfo.u2MessageLength + 5);

    pAcInfo->incomingAddr.u4_addr[0] = gACInfoAfterDiscovery[u1Primary].
        incomingAddr.u4_addr[0];
    pAcInfo->u4DestPort = gACInfoAfterDiscovery[u1Primary].u4DestPort;
    /* If WTP not able to connect to the given WLC it will chosse 
       the next entry in the list */
    gACInfoAfterDiscovery[u1Primary].u1Used = OSIX_FALSE;

    CAPWAP_TRC2 (CAPWAP_MGMT_TRC,
                 "CapwapGetBestACDetails:Selected the AC with Index = %d and"
                 "WtpCount = %d \r\n", u1Primary, u1WtpCount);
    SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4CapwapSysLogId,
                  "CapwapGetBestACDetails:Selected the AC with Index = %d and"
                  "WtpCount = %d \r\n", u1Primary, u1WtpCount));

    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapGetDetailsFrmBestAC                         */
/*  Description     : This function used to get the Ip address of the   */
/*                    Best Ac Selected                                  */
/*  Input(s)        : None                                              */
/*  Output(s)       : tACInfoAfterDiscovery *                           */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/

INT4
CapwapGetDetailsFrmBestAC (tACInfoAfterDiscovery * pAcInfo)
{
    UINT1               u1Index, u1WtpCountTemp = 0, u1WtpCount = 0;
    UINT1               u1Primary = 0;

    for (u1Index = 0; u1Index < MAX_DISC_RESPONSE; u1Index++)
    {
        if (gACInfoAfterDiscovery[u1Index].u1Used == OSIX_TRUE)
        {
            /* Maximum WTPs supported - WTPs currently serving */
            u1WtpCountTemp = (UINT1) (gACInfoAfterDiscovery[u1Index].
                                      acDesc.maxWtp -
                                      gACInfoAfterDiscovery[u1Index].wtpCount);
            if (u1WtpCountTemp <= u1WtpCount)
            {
                u1WtpCount = u1WtpCountTemp;
                u1Primary = u1Index;
            }
            else if (u1WtpCount == 0)
            {
                u1WtpCount = u1WtpCountTemp;
                u1Primary = u1Index;
            }
        }
    }

    pAcInfo->incomingAddr.u4_addr[0] = gACInfoAfterDiscovery[u1Primary].
        incomingAddr.u4_addr[0];
    pAcInfo->u4DestPort = gACInfoAfterDiscovery[u1Primary].u4DestPort;

    CAPWAP_TRC2 (CAPWAP_MGMT_TRC,
                 "CapwapGetDetailsFrmBestAC:Selected the AC with Index = %d and"
                 "WtpCount = %d \r\n", u1Primary, u1WtpCount);
    SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4CapwapSysLogId,
                  "CapwapGetDetailsFrmBestAC:Selected the AC with Index = %d and"
                  "WtpCount = %d \r\n", u1Primary, u1WtpCount));

    return OSIX_SUCCESS;
}

INT4
CapwapGetDiscResponseCount (UINT1 *pAcInfoCount)
{
    UINT1               u1Index, u1temp = 0;

    CAPWAP_FN_ENTRY ();
    for (u1Index = 0; u1Index < MAX_DISC_RESPONSE; u1Index++)
    {
        if (gACInfoAfterDiscovery[u1Index].u1Used == OSIX_TRUE)
        {
            CAPWAP_TRC1 (CAPWAP_MGMT_TRC,
                         "Recved Discovery Response at Index %d \r\n", u1Index);
            SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4CapwapSysLogId,
                          "Recved Discovery Response at Index %d \r\n",
                          u1Index));

            u1temp++;
        }
    }
    *pAcInfoCount = u1temp;
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapGetDiscResponseCountUpdateAcRef             */
/*  Description     : This function will analyze the Ip address got in  */
/*                    Ac referral List and will select which Ip         */
/*                    address needs we need to send a unicast           */
/*  Input(s)        : None                                              */
/*  Output(s)       : tACInfoAfterDiscovery *                           */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/

INT4
CapwapGetDiscResponseCountUpdateAcRef (UINT1 *pAcInfoCount, UINT1 *pDiscReSend)
{
    UINT1               u1Index, u1temp = 0;
    UINT1               u1Count = 0;
    UINT1               u1FreeIndex = 0;
    UINT1               u1FreeId = 0;
    UINT1               u1FreeEntryFlag = OSIX_FALSE;
    BOOL1               isExists = OSIX_FALSE;

    CAPWAP_FN_ENTRY ();

    /* Loop through all the Discovery responses */
    for (u1Index = 0; u1Index < MAX_DISC_RESPONSE; u1Index++)
    {
        /* In case if we got a Discovery response */
        if (gACInfoAfterDiscovery[u1Index].u1Used == OSIX_TRUE)
        {
            /* if it has a Valid IP list in that Discovery Response */
            if (gACInfoAfterDiscovery[u1Index].acIPv4List.u2MsgEleLen != 0)
            {
                /* Loop through that Ac Ip List */
                for (u1Count = 0; u1Count < (gACInfoAfterDiscovery[u1Index].
                                             acIPv4List.u2MsgEleLen /
                                             AC_REF_COUNT); u1Count++)
                {
                    isExists = OSIX_FALSE;

                    /* Loop through the GLobal Ac referral List to find
                     * the Free Index */
                    for (u1FreeIndex = 0; u1FreeIndex < FREE_INDEX;
                         u1FreeIndex++)
                    {
                        if ((gACRefInfoAfterDiscovery[u1FreeIndex].u1Used ==
                             OSIX_FALSE) && (u1FreeEntryFlag == OSIX_FALSE))
                        {
                            /* Free Index is found */
                            u1FreeId = u1FreeIndex;
                            u1FreeEntryFlag = OSIX_TRUE;
                        }
                    }

                    /* Check whether the Ip address is already exisitng
                     * in our data structure */
                    for (u1FreeIndex = 0; u1FreeIndex < FREE_INDEX;
                         u1FreeIndex++)
                    {
                        if (gACRefInfoAfterDiscovery[u1FreeIndex].u1Used ==
                            OSIX_TRUE)
                        {
                            if ((MEMCMP (&gACRefInfoAfterDiscovery[u1FreeIndex].
                                         ipAddr.u4_addr[0],
                                         &gACInfoAfterDiscovery[u1Index].
                                         acIPv4List.ipAddr[u1Count].
                                         u4_addr[0], sizeof (UINT4))) == 0)
                            {
                                /* Entry is alreasy present
                                 * */
                                isExists = OSIX_TRUE;
                                u1FreeEntryFlag = OSIX_FALSE;

                            }
                        }
                    }

                    /* Update the New Ip address in the Global List */
                    if ((u1FreeEntryFlag == OSIX_TRUE) && (isExists ==
                                                           OSIX_FALSE))
                    {
                        gACRefInfoAfterDiscovery[u1FreeId].u1Used = OSIX_TRUE;
                        gACRefInfoAfterDiscovery[u1FreeId].ipAddr.u4_addr[0] =
                            gACInfoAfterDiscovery[u1Index].acIPv4List.
                            ipAddr[u1Count].u4_addr[0];
                        u1FreeEntryFlag = OSIX_FALSE;
                    }

                    /* Loop through the GLobal List and find whether any
                     * new Ac Ip address is received and whether we need
                     * to send a capwap discovery */
                    for (u1FreeIndex = 0; u1FreeIndex < FREE_INDEX;
                         u1FreeIndex++)
                    {
                        if ((gACRefInfoAfterDiscovery[u1FreeIndex].u1Used ==
                             OSIX_TRUE) &&
                            (gACRefInfoAfterDiscovery[u1FreeIndex].
                             isDiscReqSent != OSIX_TRUE))
                        {
                            /* New Ip address is received and we
                             * need to send a capwap discovery
                             * */
                            (*pDiscReSend)++;
                        }
                    }
                }
            }
            CAPWAP_TRC1 (CAPWAP_MGMT_TRC,
                         "Receieved Discovery Response"
                         "at Index %d \r\n", u1Index);
            SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4CapwapSysLogId,
                          "Receieved Discovery Response"
                          "at Index %d \r\n", u1Index));

            u1temp++;
        }
    }
    *pAcInfoCount = u1temp;
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

INT4
CapwapClearDiscResponseEntry (VOID)
{
    UINT1               u1Index = 0;

    CAPWAP_FN_ENTRY ();

    for (u1Index = 0; u1Index < MAX_DISC_RESPONSE; u1Index++)
    {
        MEMSET (&gACInfoAfterDiscovery[u1Index], 0,
                sizeof (tACInfoAfterDiscovery));
    }
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*******************************Get Functions*********************************/

INT4
CapwapGetRadioAdminState (tRadioIfAdminStatus * pRadAdminState, UINT4 *pMsgLen)
{

#if CAP_WANTED
    tRadioIfGetDB       RadioIfGetDB;
    UINT2               u2NumofRadios = 1;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    SYS_DEF_MAX_ENET_INTERFACES
        RadioIfGetDB.RadioIfIsGetAllDB.bAdminStatus = OSIX_TRUE;
    RadioIfGetDB.u4RadioIfIndex =
        for (u1Index = SYS_DEF_MAX_ENET_INTERFACES + 1;
             u1Index <= u2NumofRadios; u1Index++)
    {

        RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
            UtlShMemAllocAntennaSelectionBuf ();

        if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "\r%% Memory allocation for Antenna"
                        " selection failed.\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "\r%% Memory allocation for Antenna"
                          " selection failed.\r\n"));

            return OSIX_FAILURE;
        }
        RadioIfGetDB.RadioIfIsGetAllDB.bAdminStatus = OSIX_TRUE;
        RadioIfGetDB.u4RadioIfIndex = u1Index;
        RadioIfGetDB.RadioIfIsGetAllDB.bRadioIfIndex = OSIX_TRUE;
        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, &RadioIfGetDB) ==
            OSIX_SUCCESS)
        {
            pRadAdminState->u2MessageType = RADIO_ADMIN_STATE;
            pRadAdminState->u2MessageLength = RadioIfGetDB.
                pRadAdminState->u1RadioId = RadioIfGetDB.RadioIfGetAllDB.
                u1RadioId;
            pRadAdminState->u1AdminStatus = RadioIfGetDB.RadioIfIsGetAllDB.
                u1AdminStatus;
            *pMsgLen += pRadAdminState->u2MessageLength +
                CAPWAP_MSG_ELEM_TYPE_LEN;
            pRadAdminState->isPresent = 1;
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                             pu1AntennaSelection);
            return OSIX_SUCCESS;
        }
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                         pu1AntennaSelection);
    }
#endif
    pRadAdminState->u2MessageType = RADIO_ADMIN_STATE;
    pRadAdminState->u2MessageLength = RADIO_ADMIN_LEN;
    pRadAdminState->u1RadioId = 1;
    pRadAdminState->u1AdminStatus = 1;
    *pMsgLen +=
        (UINT4) (pRadAdminState->u2MessageLength + CAPWAP_MSG_ELEM_TYPE_LEN);
    pRadAdminState->isPresent = 1;
    return OSIX_SUCCESS;
}

INT4
CapwapGetWtpRebootStats (tWtpRebootStatsElement * pWtpRebootStats,
                         UINT4 *pMsgLen)
{
    pWtpRebootStats->u2MsgEleType = WTP_REBOOT_STATISTICS;
    pWtpRebootStats->u2MsgEleLen = WTP_REBBOT_STATS_LEN;
    pWtpRebootStats->u2RebootCount = HEX_COUNT;
    pWtpRebootStats->u2AcInitiatedCount = HEX_COUNT;
    pWtpRebootStats->u2LinkFailCount = HEX_COUNT;
    pWtpRebootStats->u2SwFailCount = HEX_COUNT;
    pWtpRebootStats->u2HwFailCount = HEX_COUNT;
    pWtpRebootStats->u2OtherFailCount = HEX_COUNT;
    pWtpRebootStats->u2UnknownFailCount = HEX_COUNT;
    pWtpRebootStats->u1LastFailureType = HEX_COUNT;
    pWtpRebootStats->isOptional = 1;
    *pMsgLen += MSG_LEN;
    return OSIX_SUCCESS;
}

INT4
CapwapGetImageId (tImageId * imageId, UINT4 *pMsgLen)
{
    UNUSED_PARAM (imageId);
    UNUSED_PARAM (pMsgLen);
    return OSIX_SUCCESS;
}

INT4
CapwapGetResultCode (tResCode * pResCode, UINT4 *pMsgLen)
{
    pResCode->u2MsgEleType = RESULT_CODE;
    pResCode->u2MsgEleLen = CAPWAP_MSG_ELEM_TYPE_LEN;
    pResCode->u4Value = 0;
    *pMsgLen += (UINT4) (pResCode->u2MsgEleLen + CAPWAP_MSG_ELEM_TYPE_LEN);
    return OSIX_SUCCESS;

}

INT4
CapwapGetRadioOperationalState (tRadioIfOperStatus * pRadOperState,
                                UINT4 *pMsgLen)
{
    tRadioIfGetDB       RadioIfGetDB;
    UINT1               u1Index;
    UINT2               u2NumOfRadios = 1;

    CAPWAP_FN_ENTRY ();

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    for (u1Index = 1; u1Index <= u2NumOfRadios; u1Index++)
    {
        RadioIfGetDB.RadioIfIsGetAllDB.bFailureStatus = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bOperStatus = OSIX_TRUE;
        RadioIfGetDB.RadioIfGetAllDB.u1RadioId = u1Index;
        if (WssIfProcessRadioIfDBMsg (WSS_GET_PHY_RADIO_IF_DB,
                                      &RadioIfGetDB) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to get Radio Operationa State from RadioIf \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to get Radio Operationa State from RadioIf \r\n"));

            return OSIX_FAILURE;
        }
        pRadOperState->u2MessageType = RADIO_OPER_STATE;
        pRadOperState->u2MessageLength = RADIO_OPER_STATE_LEN;
        pRadOperState->u1RadioId = u1Index;
        pRadOperState->u1OperStatus = RadioIfGetDB.RadioIfGetAllDB.u1OperStatus;
        pRadOperState->u1FailureCause =
            RadioIfGetDB.RadioIfGetAllDB.u1FailureStatus;
        pRadOperState->isPresent = OSIX_TRUE;
        *pMsgLen +=
            (UINT4) (pRadOperState->u2MessageLength + CAPWAP_MSG_ELEM_TYPE_LEN);
    }
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

INT4
CapwapGetDecryptErrReportPeriod (tDecryptErrReportPeriod * pDecryErrPeriod,
                                 UINT4 *pMsgLen)
{
    pDecryErrPeriod->u2MsgEleType = DECRYPTION_ERROR_REPORT_PERIOD;
    pDecryErrPeriod->u2MsgEleLen = DECRYP_ERR_REPORT_LEN;
    pDecryErrPeriod->u1RadioId = 1;
    pDecryErrPeriod->u2ReportInterval = REPORT_INTERVAL;
    pDecryErrPeriod->isOptional = 1;
    *pMsgLen += DECRYP_MSG_LEN;
    return OSIX_SUCCESS;

}

/*****************************************************************************
 *                                                                           *
 * Function     : CapwapProcessErrorHandler                                  *
 *                                                                           *
 * Description  : Error handler function called when Invalid Control message *
 *                received                                                   *
 *                                                                           *
 * Input        : pBuf   - Received CRU buffer Pointer                       *
 *                pMsg   - Parsed control packet                             *
 *                                                                           *
 * Returns      : CAPWAP_FAILURE                                             *
 *                                                                           *
 *****************************************************************************/
INT4
CapwapProcessErrorHandler (tSegment * pBuf,
                           tCapwapControlPacket * pMsg,
                           tRemoteSessionManager * pSessEntry)
{
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (pSessEntry);
    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                "capwapProcessErrorHandler: Received the Invalid CAPWAP"
                "Control Message type \r\n");
    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                  "capwapProcessErrorHandler: Received the Invalid CAPWAP"
                  "Control Message type \r\n"));

    return CAPWAP_FAILURE;
}

INT4
CapwapProcessDataRxMsg ()
{
    tCapwapRxQMsg      *pCapwapRxQMsg;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tRemoteSessionManager *pSessEntry = NULL;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tCapwapControlPacket capwapCtrlMsg;
    UINT4               u4MsgType;
    tWssIfPMDB          WssIfPMDB;

    CAPWAP_FN_ENTRY ();
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    MEMSET (&WssIfPMDB, 0, sizeof (tWssIfPMDB));
    MEMSET (&capwapCtrlMsg, 0, sizeof (tCapwapControlPacket));

    while (OsixQueRecv (gSerTaskGlobals.echoAliveMsgQId,
                        (UINT1 *) (&pCapwapRxQMsg),
                        OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        if (pCapwapRxQMsg->u4PktType == CAPWAP_ECHO_ALIVE_MSG)
        {
            pBuf = pCapwapRxQMsg->pRcvBuf;
            if (CapwapDataKeepAlive (pBuf, &pSessEntry) == OSIX_SUCCESS)
            {
                if (pSessEntry == NULL)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "Received NULL pointer for Remote"
                                "session Entry \r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Received NULL pointer for Remote"
                                  "session Entry \r\n"));

                    CAPWAP_RELEASE_CRU_BUF (pBuf);
                    MemReleaseMemBlock (CAPWAP_QUEMSG_POOLID,
                                        (UINT1 *) pCapwapRxQMsg);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    return OSIX_FAILURE;
                }
#ifdef WLC_WANTED
                WssIfPMDB.u2ProfileId = pSessEntry->u2IntProfileId;
                WssIfPMDB.tWtpPmAttState.wssPmWLCStatsSetDB.
                    bWssPmWLCCapwapKeepAlivePkts = OSIX_TRUE;
                if (WssIfProcessPMDBMsg
                    (WSS_PM_WLC_CAPWAP_STATS_SET, &WssIfPMDB) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "Failed to update Keep Alive stats to PM \n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Failed to update Keep Alive stats to PM \n"));
                }
#endif
#ifdef WTP_WANTED
                MEMSET (&WssIfPMDB, 0, sizeof (tWssIfPMDB));
                WssIfPMDB.u2ProfileId = pSessEntry->u2IntProfileId;
                if (WssIfProcessPMDBMsg
                    (WSS_PM_WTP_EVT_VSP_CAPWAP_STATS_GET,
                     &WssIfPMDB) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "Failed to update Join Req Stats to PM \n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Failed to update Join Req Stats to PM \n"));
                }

                WssIfPMDB.wtpVSPCPWPWtpStats.keepAliveMsgRcvd++;
                if (WssIfProcessPMDBMsg
                    (WSS_PM_WTP_EVT_VSP_CAPWAP_STATS_SET,
                     &WssIfPMDB) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "Failed to update Join Req Stats to PM \n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Failed to update Join Req Stats to PM \n"));
                }
#endif
                CapwapProcessKeepAlivePacket (pBuf, pSessEntry);
            }
            else if ((pSessEntry != NULL) && (CapwapFragmentReceived (pBuf) ==
                                              OSIX_SUCCESS))
            {
                CAPWAP_TRC (CAPWAP_MGMT_TRC,
                            "capwapProcessDataRxMsg: Received packet"
                            "is fragment \r\n");
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                              "capwapProcessDataRxMsg: Received packet"
                              "is fragment \r\n"));
                /* capwapHandleFragment (pData,pSessEntry); */
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_SUCCESS;
            }
            else if (CapwapParseControlPacket (pBuf, &capwapCtrlMsg) ==
                     OSIX_SUCCESS)
            {
                u4MsgType = capwapCtrlMsg.capwapCtrlHdr.u4MsgType;

                pWssIfCapwapDB->u4DestIp = CRU_BUF_Get_U4Reserved2 (pBuf);
                pWssIfCapwapDB->u4DestPort = CRU_BUF_Get_U4Reserved1 (pBuf);
                if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_RSM,
                                             pWssIfCapwapDB) == OSIX_FAILURE)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "Failed to Retrive CAPWAP DB \r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Failed to Retrive CAPWAP DB \r\n"));
                    MemReleaseMemBlock (CAPWAP_QUEMSG_POOLID,
                                        (UINT1 *) pCapwapRxQMsg);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    return OSIX_FAILURE;
                }
                pSessEntry = pWssIfCapwapDB->pSessEntry;
                if ((pSessEntry != NULL) & ((u4MsgType == CAPWAP_ECHO_REQ) ||
                                            (u4MsgType == CAPWAP_ECHO_RSP)))
                {
                    if (gCapwapStateMachine[pSessEntry->eCurrentState]
                        [u4MsgType] (pBuf, &capwapCtrlMsg,
                                     pSessEntry) == OSIX_FAILURE)
                    {
                        CAPWAP_RELEASE_CRU_BUF (pBuf);
                        MemReleaseMemBlock (CAPWAP_QUEMSG_POOLID,
                                            (UINT1 *) pCapwapRxQMsg);
                        CapwapParseStructureMemrelease (&capwapCtrlMsg);
                        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                    "Faile to Enter CAPWAP State Machine \r\n");
                        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                      "Faile to Enter CAPWAP State Machine \r\n"));

                        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                        return OSIX_FAILURE;
                    }
                }
                CapwapParseStructureMemrelease (&capwapCtrlMsg);
            }
            CAPWAP_RELEASE_CRU_BUF (pBuf);
            MemReleaseMemBlock (CAPWAP_QUEMSG_POOLID, (UINT1 *) pCapwapRxQMsg);
        }
        else
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Recived the unknown"
                        "message type \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Recived the unknown" "message type \r\n"));

            CAPWAP_RELEASE_CRU_BUF (pBuf);
            MemReleaseMemBlock (CAPWAP_QUEMSG_POOLID, (UINT1 *) pCapwapRxQMsg);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

INT4
CapwapProcessDataTxMsg (void)
{
    tCapwapRxQMsg      *pCapwapRxQMsg;
    /*tSegment                       *pBuf= NULL;
       UINT1                          *pData= NULL;
       UINT4                           u4DestIpAddr;
       tRemoteSessionManager          *pSessEntry = NULL;
       UINT1                           u1NodeType; */

    CAPWAP_FN_ENTRY ();
    while (OsixQueRecv (gSerTaskGlobals.dataTxMsgQId,
                        (UINT1 *) (&pCapwapRxQMsg),
                        OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        if (pCapwapRxQMsg->u4PktType == CAPWAP_DATA_TX_MSG)
        {

        }
    }
    return OSIX_SUCCESS;
}

INT4
CapwapDataKeepAlive (tCRU_BUF_CHAIN_HEADER * pBuf,
                     tRemoteSessionManager ** pSes)
{
    UINT1               u1Val = 0;
    UINT2               u2Type, u2Len;
    UINT1              *pRcvBuf = NULL;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT1               u1Index = 0;
    UINT4               JoinSessionID[4] = { 0 };
    UINT1               au1Frame[40];
    BOOL1               isCorrect = OSIX_TRUE;
    UINT4               u4DestIp, u4DestPort;
#ifdef WTP_WANTED
    INT4                i4Status = 0;
    UINT2               u2StatsInterval = 0;
#endif
    CAPWAP_FN_ENTRY ();
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    u4DestIp = CRU_BUF_Get_U4Reserved2 (pBuf);
    u4DestPort = CRU_BUF_Get_U4Reserved1 (pBuf);
    pRcvBuf = CAPWAP_BUF_IF_LINEAR (pBuf, 0, CAPWAP_MAX_HDR_LEN);
    if (pRcvBuf == NULL)
    {
        MEMSET (au1Frame, FRAME_ADDRESS, CAPWAP_MAX_HDR_LEN);
        CAPWAP_COPY_FROM_BUF (pBuf, au1Frame, 0, CAPWAP_MAX_HDR_LEN);
        pRcvBuf = au1Frame;
    }
    /* Read 'K' bit in the CAPWAP Header */
    CAPWAP_SKIP_N_BYTES (KEEP_ALIVE_BYTE, pRcvBuf);
    CAPWAP_GET_1BYTE (u1Val, pRcvBuf);
    if ((u1Val & HEX_VAL) == HEX_VAL)
    {
        /* Validat the received Join Session Id */
        pRcvBuf += RECIVED_BUF;
        CAPWAP_GET_2BYTE (u2Type, pRcvBuf);
        CAPWAP_GET_2BYTE (u2Len, pRcvBuf);
        for (u1Index = 0; u1Index < DATA_KEEPALIVE; u1Index++)
        {
            CAPWAP_GET_4BYTE (JoinSessionID[u1Index], pRcvBuf);
        }
        pWssIfCapwapDB->u4DestIp = u4DestIp;
        pWssIfCapwapDB->u4DestPort = u4DestPort;
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_RSM,
                                     pWssIfCapwapDB) == OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to Get Remote session Entry from RSM \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to Get Remote session Entry from RSM \r\n"));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
        *pSes = pWssIfCapwapDB->pSessEntry;
        if (*pSes != NULL)
        {
            for (u1Index = 0; u1Index < DATA_KEEPALIVE; u1Index++)
            {
                if (MEMCMP (&JoinSessionID[u1Index],
                            &((*pSes)->JoinSessionID[u1Index]),
                            JOIN_SESSION_LEN) != 0)
                {
                    isCorrect = OSIX_FALSE;
                }
            }
            if (isCorrect == OSIX_TRUE)
            {
#ifdef WTP_WANTED
                /*Triggering PM Wtp Event Request Message once AP reaches run state to get
                 * multi domain capability information for the configured country */
                if ((gu1FirstKeepAlive == 0) && ((*pSes) != NULL))
                {
                    i4Status = ApHdlrTmrStart (*pSes, APHDLR_STATS_INTERVAL_TMR,
                                               u2StatsInterval);

                    if (i4Status == OSIX_FAILURE)
                    {
                        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                    "CapwapDataKeepAlive : Failed to start"
                                    "the Stats Timer \r\n");
                        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                      "CapwapDataKeepAlive : Failed to start"
                                      "the Stats Timer \r\n"));
                        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);

                        return OSIX_FAILURE;
                    }

                    gu1FirstKeepAlive++;
                }

#endif
                CAPWAP_TRC (CAPWAP_MGMT_TRC,
                            "Received packet is a correct Keep Alive Packet \r\n");
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                              "Received packet is a correct Keep Alive Packet \r\n"));

                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_SUCCESS;
            }
            else
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Received the corrupted keep alive packet"
                            "from the known Source \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Received the corrupted keep alive packet"
                              "from the known Source \r\n"));
                /* Discconnect the Session -- TBD */
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
        }
        else                    /* Received the first keep alive packet */
        {
            for (u1Index = 0; u1Index < DATA_KEEPALIVE; u1Index++)
            {
                pWssIfCapwapDB->JoinSessionID[u1Index] = JoinSessionID[u1Index];
            }
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_RSM_FROM_SESSIONID,
                                         pWssIfCapwapDB) == OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_MGMT_TRC,
                            "Received the First Keep alive packet \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Received the First Keep alive packet \r\n"));
                *pSes = pWssIfCapwapDB->pSessEntry;
                if (*pSes == NULL)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "Remote session Module returns NULL\r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Remote session Module returns NULL\r\n"));
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    return OSIX_FAILURE;
                }
                /* Update the Data UDP port */
                (*pSes)->u4RemoteDataPort = u4DestPort;
                /* Create the entry in RSM with IP address and Data UDP Port */
                pWssIfCapwapDB->u4DestIp = u4DestIp;
                pWssIfCapwapDB->u4DestPort = u4DestPort;
                pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
                    (*pSes)->u2CapwapRSMIndex;
                if (WssIfProcessCapwapDBMsg
                    (WSS_CAPWAP_CREATE_SESSION_PORT_ENTRY,
                     pWssIfCapwapDB) == OSIX_FAILURE)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "Failed to create the entry in SessionPort"
                                "Entry port table \r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Failed to create the entry in SessionPort"
                                  "Entry port table \r\n"));
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    return OSIX_SUCCESS;
                }
            }
        }
    }
    else
    {
        CAPWAP_TRC (CAPWAP_MGMT_TRC,
                    "The Received Packet is not Keep Alive \r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                      "The Received Packet is not Keep Alive \r\n"));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

INT4
CapwapUpdateResultCode (tResCode * pResCode, UINT4 u4RetCode, UINT4 *pMsgLen)
{

    pResCode->u2MsgEleType = RESULT_CODE;
    pResCode->u2MsgEleLen = RESULT_CODE_LEN;
    pResCode->u4Value = u4RetCode;
    *pMsgLen += (UINT4) (pResCode->u2MsgEleLen + CAPWAP_MSG_ELEM_TYPE_LEN);
    return OSIX_SUCCESS;
}

INT4
DbgDumpCapwapPkt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 u1PktDirection)
{

    UINT1               u1Temp;
    INT4                i4ReadOffset = 0;
    UINT2               u2Len = 0;
    tCapwapHdr          CapHdr;

    CAPWAP_FN_ENTRY ();
    MEMSET (&CapHdr, 0, sizeof (tCapwapHdr));
    if (pBuf == NULL)
    {
        return OSIX_FAILURE;
    }
    if (u1PktDirection == CAPWAP_PKT_FLOW_IN)
    {
        PRINTF ("Received Packet Dump: \n");
        u2Len = (UINT2) CAPWAP_GET_BUF_LEN (pBuf);

    }
    else
    {
        PRINTF ("Transmitted Packet Dump: \n");
        /* u2Len =  (UINT2) CAPWAP_GET_BUF_LEN (pBuf); */
        u2Len = CAPWAP_BUF_LEN;
    }
    /* CapwapExtractHdr (pBuf, &CapHdr);
       PRINTF ("CAPWAP Preamble: %d \n", CapHdr.u1Preamble);
       PRINTF ("HLEN: %d \n", (CapHdr.u2HlenRidWbidFlagT >> 11));
       PRINTF ("RID: %d \n", ((CapHdr.u2HlenRidWbidFlagT >> 6)&0x3F));
       PRINTF ("Fragment Id: %d \n", CapHdr.u2FragId);
       PRINTF ("Fragment Offset: %d \n", (CapHdr.u2FragOffsetRes3 >> 3)); */
    while (u2Len--)
    {
        GET_1_BYTE (pBuf, i4ReadOffset, u1Temp);
        PRINTF ("%02x ", u1Temp);
        i4ReadOffset++;
        if ((i4ReadOffset % CAPWAP_HEADER_LEN) == 0)
        {
            PRINTF ("\n");
        }
    }
    PRINTF ("\n");
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : CapwapGetMsgTypeFromBuf                                    *
 *                                                                           *
 * Description  : This function retrives the msg type from recieved packet   *
 *                                                                           *
 * Input        : pRcvBuf     - Pointer to the received packet buffer        *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
CapwapGetMsgTypeFromBuf (UINT1 *pRcvBuf, UINT4 *u4MsgType)
{

    UINT1               u1Val = 0;
    UINT4               u4Val;
    INT4                i4Status = OSIX_FAILURE;

    CAPWAP_FN_ENTRY ();

    /* Move the pointer 1 byte to get the HLEN */
    pRcvBuf += 1;

    CAPWAP_GET_1BYTE (u1Val, pRcvBuf);

    u1Val = (UINT1) ((u1Val >> BUF_VAL) * BUF_MSG_TYPE);

    CAPWAP_SKIP_N_BYTES ((u1Val - SKIP_BYTE), pRcvBuf);

    CAPWAP_GET_4BYTE (u4Val, pRcvBuf);

    CAPWAP_TRC1 (CAPWAP_MGMT_TRC, "Received Msg Type  = %d \r\n", u4Val);

    if ((u4Val < MIN_CAPWAP_MSG_TYPE) || (u4Val > WLAN_CONF_RSP))
    {
        CAPWAP_TRC (CAPWAP_MGMT_TRC,
                    "CapwapGetMsgType: Received the unknow MsgType\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                      "CapwapGetMsgType: Received the unknow MsgType\n"));
        *u4MsgType = CAPWAP_ERR_MSG;
        i4Status = OSIX_FAILURE;
    }
    else
    {
        *u4MsgType = u4Val;
        i4Status = OSIX_SUCCESS;
    }
    CAPWAP_FN_EXIT ();
    return i4Status;
}

/*****************************************************************************
 *                                                                           *
 * Function     : CapwapGetSeqNumFromPkt                                     *
 *                                                                           *
 * Description  : This function retrives the Sequence number from            *
 *                recieved packet                                            *
 *                                                                           *
 * Input        : pRcvBuf     - Pointer to the received packet buffer        *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
CapwapGetSeqNumFromPkt (UINT1 *pRcvBuf, UINT1 *u1SeqNum)
{
    pRcvBuf += CAPWAP_HEADER_LEN + CAPWAP_MSGTYPE_LEN;
    CAPWAP_GET_1BYTE (*u1SeqNum, pRcvBuf);
    return OSIX_SUCCESS;

}

INT4
CapwapFragmentReceived (tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UINT1               u1Val;
    UINT1              *pRcvBuf = NULL;
    UINT1               au1Frame[CAPWAP_MAX_HDR_LEN];

    pRcvBuf = CAPWAP_BUF_IF_LINEAR (pBuf, 0, CAPWAP_MAX_HDR_LEN);
    if (pRcvBuf == NULL)
    {
        MEMSET (au1Frame, FRAME_ADDRESS, CAPWAP_MAX_HDR_LEN);
        CAPWAP_COPY_FROM_BUF (pBuf, au1Frame, 0, CAPWAP_MAX_HDR_LEN);
        pRcvBuf = au1Frame;
    }
    /* Read 'F' bit in the CAPWAP Header */
    CAPWAP_SKIP_N_BYTES (KEEP_ALIVE_BYTE, pRcvBuf);
    CAPWAP_GET_1BYTE (u1Val, pRcvBuf);
    if ((u1Val & VAL_HEX) == VAL_HEX)
    {
        CAPWAP_TRC (CAPWAP_MGMT_TRC, "The received packet is fragment \r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                      "The received packet is fragment \r\n"));
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

INT4
CapwapKeepAliveReceived (tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UINT1               u1Val;
    UINT1              *pRcvBuf = NULL;
    UINT1               au1Frame[CAPWAP_MAX_HDR_LEN];

    pRcvBuf = CAPWAP_BUF_IF_LINEAR (pBuf, 0, CAPWAP_MAX_HDR_LEN);
    if (pRcvBuf == NULL)
    {
        MEMSET (au1Frame, FRAME_ADDRESS, CAPWAP_MAX_HDR_LEN);
        CAPWAP_COPY_FROM_BUF (pBuf, au1Frame, 0, CAPWAP_MAX_HDR_LEN);
        pRcvBuf = au1Frame;
    }
    /* Read 'K' bit in the CAPWAP Header */
    CAPWAP_SKIP_N_BYTES (KEEP_ALIVE_BYTE, pRcvBuf);
    CAPWAP_GET_1BYTE (u1Val, pRcvBuf);
    if ((u1Val & HEX_VAL) == HEX_VAL)
    {
        CAPWAP_TRC (CAPWAP_MGMT_TRC,
                    "The received packet is Keep Alive packet \r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                      "The received packet is Keep Alive packet \r\n"));
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

INT4
CapwapDot3FrameReceived (tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UINT2               u2Val;
    UINT1              *pRcvBuf = NULL;

    pRcvBuf = CAPWAP_BUF_IF_LINEAR (pBuf, 0, CAPWAP_MAX_HDR_LEN);
    if (pRcvBuf == NULL)
    {
        MEMSET (gau1Frame, 0x00, CAPWAP_MAX_HDR_LEN);
        CAPWAP_COPY_FROM_BUF (pBuf, gau1Frame, 0, CAPWAP_MAX_HDR_LEN);
        pRcvBuf = gau1Frame;
    }
    /* Read 'T' and 'M' bit in the CAPWAP Header */
    CAPWAP_SKIP_N_BYTES (2, pRcvBuf);
    CAPWAP_GET_2BYTE (u2Val, pRcvBuf);
    if ((u2Val & 0x0110) == 0x0010)
        /* 'T' bit should be off and 'M should be on for 802.3 frame */
    {
        CAPWAP_TRC (CAPWAP_MGMT_TRC, "The received packet is\
                     802.3 packet \r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                      "The received packet is\
                     802.3 packet \r\n"));
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

INT4
CapwapGetRadioInfoFromBuf (UINT1 *pRcvBuf,
                           UINT4 u4Offset,
                           tRadioIfInfo * pWtpRadioInfo, UINT2 u2NumMsgElems)
{
    UNUSED_PARAM (u2NumMsgElems);
    pRcvBuf += u4Offset;
    CAPWAP_GET_2BYTE (pWtpRadioInfo->u2MessageType, pRcvBuf);
    CAPWAP_GET_2BYTE (pWtpRadioInfo->u2MessageLength, pRcvBuf);
    CAPWAP_GET_1BYTE (pWtpRadioInfo->u1RadioId, pRcvBuf);
    CAPWAP_GET_4BYTE (pWtpRadioInfo->u4RadioType, pRcvBuf);
    return OSIX_SUCCESS;
}

INT4
CapwapGetIeeeAntena (tRadioIfAntenna * pRadioIfAntenna, UINT2 *pMsgLen,
                     UINT2 u2IntId)
{
    tRadioIfGetDB       RadioIfGetDB;
    UINT2               u2NumOfRadios = 1, u2Len;
    UINT1               u1Index = 0;
    UINT1               u1antennaIndex = 0;
#ifdef WLC_WANTED
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
#else
    UNUSED_PARAM (u2IntId);
#endif
    UINT4               u4Index = WSS_GET_PHY_RADIO_IF_DB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    for (u1Index = 1; u1Index <= u2NumOfRadios; u1Index++)
    {
        RadioIfGetDB.RadioIfIsGetAllDB.bDiversitySupport = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bAntennaMode = OSIX_TRUE;

        RadioIfGetDB.RadioIfIsGetAllDB.bCurrentTxAntenna = OSIX_TRUE;
        RadioIfGetDB.RadioIfGetAllDB.u1RadioId = u1Index;
#ifdef WLC_WANTED
        WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
            MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
        pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;
        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2IntId;
        pWssIfCapwapDB->CapwapGetDB.u1RadioId = u1Index;
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                     pWssIfCapwapDB) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Getting Radio index failed\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Getting Radio index failed\r\n"));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
        RadioIfGetDB.RadioIfIsGetAllDB.bRadioIfIndex = OSIX_TRUE;
        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
            pWssIfCapwapDB->CapwapGetDB.u4IfIndex;
        u4Index = WSS_GET_RADIO_IF_DB;
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
#endif
        RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
            UtlShMemAllocAntennaSelectionBuf ();

        if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "\r%% Memory allocation for Antenna"
                        " selection failed.\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "\r%% Memory allocation for Antenna"
                          " selection failed.\r\n"));
            return OSIX_FAILURE;
        }
        if (WssIfProcessRadioIfDBMsg ((UINT1) u4Index, &RadioIfGetDB) !=
            OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to Get IEEE Antena Information from Radio DB \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to Get IEEE Antena Information from Radio DB \r\n"));
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                             pu1AntennaSelection);
            return OSIX_FAILURE;
        }
        for (u1antennaIndex = 0; u1antennaIndex < RadioIfGetDB.RadioIfGetAllDB.
             u1CurrentTxAntenna; u1antennaIndex++)
        {
            RadioIfGetDB.RadioIfIsGetAllDB.
                bAntennaSelection[u1antennaIndex] = OSIX_TRUE;
        }
        if (WssIfProcessRadioIfDBMsg ((UINT1) u4Index, &RadioIfGetDB) !=
            OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to Get IEEE Antena Info from Radio DB \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to Get IEEE Antena Info from Radio DB \r\n"));
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                             pu1AntennaSelection);
            return OSIX_FAILURE;
        }

        pRadioIfAntenna->u2MessageType = IEEE_ANTENNA;
        pRadioIfAntenna->u1RadioId = u1Index;
        pRadioIfAntenna->u1ReceiveDiversity =
            RadioIfGetDB.RadioIfGetAllDB.u1DiversitySupport;
        pRadioIfAntenna->u1AntennaMode =
            RadioIfGetDB.RadioIfGetAllDB.u1AntennaMode;
        pRadioIfAntenna->u1CurrentTxAntenna =
            RadioIfGetDB.RadioIfGetAllDB.u1CurrentTxAntenna;
        u2Len =
            (UINT2) (STRLEN (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection));
        MEMCPY (pRadioIfAntenna->au1AntennaSelection,
                RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection, u2Len);
        pRadioIfAntenna->u2MessageLength =
            (UINT2) (pRadioIfAntenna->u2MessageLength +
                     (UINT2) (MIN_IEEE_ANTENNA_LEN + u2Len - 1));
        pRadioIfAntenna->isPresent = OSIX_TRUE;
        *pMsgLen = (UINT2) (*pMsgLen +
                            (UINT2) (pRadioIfAntenna->u2MessageLength +
                                     CAPWAP_MSG_ELEM_TYPE_LEN));
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                         pu1AntennaSelection);
    }
    return OSIX_SUCCESS;
}

INT4
CapwapGetIeeeDsc (tRadioIfDSSSPhy * pRadioIfDSSSPhy, UINT2 *pMsgLen,
                  UINT2 u2IntId)
{
    tRadioIfGetDB       RadioIfGetDB;
    UINT2               u2NumOfRadios = 1;
    UINT1               u1Index = 0;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT4               u4Index = 0;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    for (u1Index = 1; u1Index <= u2NumOfRadios; u1Index++)
    {
        RadioIfGetDB.RadioIfIsGetAllDB.bCurrentCCAMode = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bCurrentChannel = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bEDThreshold = OSIX_TRUE;
        RadioIfGetDB.RadioIfGetAllDB.u1RadioId = u1Index;
#ifdef WLC_WANTED
        pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;
        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2IntId;
        pWssIfCapwapDB->CapwapGetDB.u1RadioId = u1Index;
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                     pWssIfCapwapDB) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Getting Radio index failed\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Getting Radio index failed\r\n"));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
        RadioIfGetDB.RadioIfIsGetAllDB.bRadioIfIndex = OSIX_TRUE;
        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
            pWssIfCapwapDB->CapwapGetDB.u4IfIndex;
        u4Index = WSS_GET_RADIO_IF_DB;
#else
        u4Index = WSS_GET_PHY_RADIO_IF_DB;
        UNUSED_PARAM (u2IntId);

#endif
        if (WssIfProcessRadioIfDBMsg ((UINT1) u4Index, &RadioIfGetDB) !=
            OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to Get IEEE Dsc Information from Radio DB \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to Get IEEE Dsc Information from Radio DB \r\n"));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
        pRadioIfDSSSPhy->u2MessageType = IEEE_DIRECT_SEQUENCE_CONTROL;
        pRadioIfDSSSPhy->u2MessageLength = IEEE_DIRECT_SEQUENCE_CONTROL_LEN;
        pRadioIfDSSSPhy->u1RadioId = u1Index;
        pRadioIfDSSSPhy->i1CurrentCCAMode =
            (INT1) (RadioIfGetDB.RadioIfGetAllDB.u1CurrentCCAMode);
        pRadioIfDSSSPhy->i1CurrentChannel =
            (INT1) (RadioIfGetDB.RadioIfGetAllDB.u1CurrentChannel);
        pRadioIfDSSSPhy->i4EDThreshold =
            RadioIfGetDB.RadioIfGetAllDB.i4EDThreshold;
        pRadioIfDSSSPhy->isPresent = OSIX_TRUE;
        *pMsgLen = (UINT2) (*pMsgLen +
                            (pRadioIfDSSSPhy->u2MessageLength +
                             CAPWAP_MSG_ELEM_TYPE_LEN));
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}

INT4
CapwapGetIeeeInfoElem (tRadioIfInfoElement * pRadioIfInfoElem,
                       UINT2 *pMsgLen, UINT2 u2IntId, UINT1 u1ElementId)
{
    tRadioIfGetDB       RadioIfGetDB;
    UINT2               u2NumOfRadios = 1;
    UINT1               u1Index = 0;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT4               u4Index = 0;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    switch (u1ElementId)
    {
        case DOT11_INFO_ELEM_HTCAPABILITY:
            pRadioIfInfoElem->u1EleId = DOT11_INFO_ELEM_HTCAPABILITY;

            for (u1Index = 1; u1Index <= u2NumOfRadios; u1Index++)
            {
#ifdef WLC_WANTED
                pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;
                pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2IntId;
                pWssIfCapwapDB->CapwapGetDB.u1RadioId = u1Index;
                if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                             pWssIfCapwapDB) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Getting Radio"
                                "index failed\r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Getting Radio" "index failed\r\n"));
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    return OSIX_FAILURE;
                }
                RadioIfGetDB.RadioIfIsGetAllDB.bRadioIfIndex = OSIX_TRUE;
                RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
                    pWssIfCapwapDB->CapwapGetDB.u4IfIndex;
                u4Index = WSS_GET_RADIO_IF_DB;
                /*RadioIfGetDB.RadioIfIsGetAllDB.bManMCSSet = OSIX_TRUE; */
#else
                u4Index = WSS_GET_PHY_RADIO_IF_DB;
                RadioIfGetDB.RadioIfIsGetAllDB.bSuppMCSSet = OSIX_TRUE;
                UNUSED_PARAM (u2IntId);

#endif
                RadioIfGetDB.RadioIfGetAllDB.u1RadioId = u1Index;
                RadioIfGetDB.RadioIfIsGetAllDB.bHTCapInfo = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_TRUE;
                if (WssIfProcessRadioIfDBMsg ((UINT1) u4Index, &RadioIfGetDB)
                    != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "Failed to Get HTCapability"
                                " Info from Radio DB \r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Failed to Get HTCapability"
                                  " Info from Radio DB \r\n"));
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    return OSIX_FAILURE;
                }
                if ((RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                     RADIO_TYPE_BGN) ||
                    (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                     RADIO_TYPE_AN) ||
                    (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                     RADIO_TYPE_NG) ||
                    (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                     RADIO_TYPE_AC))

                {
                    pRadioIfInfoElem->u1RadioId = u1Index;
                    pRadioIfInfoElem->u1WlanId = VALUE_1;
                    pRadioIfInfoElem->u1BPFlag = 0xC0;
                    pRadioIfInfoElem->u1EleId = DOT11_INFO_ELEM_HTCAPABILITY;

                    pRadioIfInfoElem->unInfoElem.HTCapability.
                        u1ElemId = DOT11_INFO_ELEM_HTCAPABILITY;

                    pRadioIfInfoElem->unInfoElem.HTCapability.u2HTCapInfo =
                        RadioIfGetDB.RadioIfGetAllDB.u2HTCapInfo;
#ifdef WLC_WANTED
                    MEMCPY (pRadioIfInfoElem->unInfoElem.HTCapability.
                            au1SuppMCSSet,
                            RadioIfGetDB.RadioIfGetAllDB.au1ManMCSSet,
                            RADIOIF_11N_MCS_RATE_LEN_PKT);
#else
                    MEMCPY (pRadioIfInfoElem->unInfoElem.HTCapability.
                            au1SuppMCSSet,
                            RadioIfGetDB.RadioIfGetAllDB.au1SuppMCSSet,
                            RADIOIF_11N_MCS_RATE_LEN_PKT);
#endif
                    pRadioIfInfoElem->isPresent = OSIX_TRUE;
                    pRadioIfInfoElem->unInfoElem.HTCapability.isOptional =
                        OSIX_TRUE;

                    pRadioIfInfoElem->unInfoElem.HTCapability.u1ElemLen =
                        IEEE_INFO_ELEM_HT_CAPABILITY_LEN;

                    pRadioIfInfoElem->u2MessageType = IEEE_INFORMATION_ELEMENT;

                    pRadioIfInfoElem->u2MessageLength =
                        IEEE_INFORMATION_ELEMENT_LEN +
                        IEEE_INFO_ELEM_HT_CAPABILITY_LEN;
                    *pMsgLen = (UINT2) (*pMsgLen +
                                        pRadioIfInfoElem->u2MessageLength +
                                        CAPWAP_MSG_ELEM_TYPE_LEN);
                }
            }
            break;
        case DOT11AC_INFO_ELEM_VHTCAPABILITY:

            pRadioIfInfoElem->u1EleId = DOT11AC_INFO_ELEM_VHTCAPABILITY;

            for (u1Index = 1; u1Index <= u2NumOfRadios; u1Index++)
            {
#ifdef WLC_WANTED
                pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;
                pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2IntId;
                pWssIfCapwapDB->CapwapGetDB.u1RadioId = u1Index;
                if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                             pWssIfCapwapDB) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Getting Radio"
                                " index failed\r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Getting Radio" " index failed\r\n"));
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    return OSIX_FAILURE;
                }
                RadioIfGetDB.RadioIfIsGetAllDB.bRadioIfIndex = OSIX_TRUE;
                RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
                    pWssIfCapwapDB->CapwapGetDB.u4IfIndex;
                u4Index = WSS_GET_RADIO_IF_DB;
#else
                UNUSED_PARAM (u2IntId);

#endif
                RadioIfGetDB.RadioIfGetAllDB.u1RadioId = u1Index;
                RadioIfGetDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_TRUE;
                if (WssIfProcessRadioIfDBMsg ((UINT1) u4Index, &RadioIfGetDB)
                    != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "Failed to Get Radio Info from Radio DB \r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Failed to Get Radio Info from Radio DB \r\n"));
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    return OSIX_FAILURE;
                }

                u4Index = WSS_GET_RADIO_IF_DB_11AC;

                RadioIfGetDB.RadioIfIsGetAllDB.bVhtCapInfo = OSIX_TRUE;
#ifdef WLC_WANTED
                RadioIfGetDB.RadioIfIsGetAllDB.bVhtCapaMcs = OSIX_TRUE;
#else
                RadioIfGetDB.RadioIfIsGetAllDB.bSuppVhtCapaMcs = OSIX_TRUE;
#endif
                if (WssIfProcessRadioIfDBMsg ((UINT1) u4Index, &RadioIfGetDB)
                    != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "Failed to Get VHTCapability Info"
                                "from Radio DB \r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Failed to Get VHTCapability Info"
                                  "from Radio DB \r\n"));
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    return OSIX_FAILURE;
                }
                pRadioIfInfoElem->u1RadioId = u1Index;
                pRadioIfInfoElem->u1WlanId = VALUE_1;
                pRadioIfInfoElem->u1BPFlag = 0xC0;
                pRadioIfInfoElem->u1EleId = DOT11AC_INFO_ELEM_VHTCAPABILITY;

                pRadioIfInfoElem->unInfoElem.VHTCapability.
                    u1ElemId = DOT11AC_INFO_ELEM_VHTCAPABILITY;

                pRadioIfInfoElem->unInfoElem.VHTCapability.u4VhtCapInfo =
                    RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u4VhtCapInfo;
#ifdef WLC_WANTED
                MEMCPY (pRadioIfInfoElem->unInfoElem.VHTCapability.au1SuppMCS,
                        RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.
                        au1VhtCapaMcs, DOT11AC_VHT_CAP_MCS_LEN);
#else
                MEMCPY (pRadioIfInfoElem->unInfoElem.VHTCapability.au1SuppMCS,
                        RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.
                        au1SuppVhtCapaMcs, DOT11AC_VHT_CAP_MCS_LEN);
#endif
                pRadioIfInfoElem->isPresent = OSIX_TRUE;
                pRadioIfInfoElem->unInfoElem.VHTCapability.isOptional =
                    OSIX_TRUE;

                pRadioIfInfoElem->unInfoElem.VHTCapability.u1ElemLen =
                    IEEE_INFO_ELEM_VHT_CAPABILITY_LEN;

                pRadioIfInfoElem->u2MessageType = IEEE_INFORMATION_ELEMENT;

                pRadioIfInfoElem->u2MessageLength =
                    IEEE_INFORMATION_ELEMENT_LEN +
                    IEEE_INFO_ELEM_VHT_CAPABILITY_LEN;
                *pMsgLen = (UINT2) (*pMsgLen +
                                    pRadioIfInfoElem->u2MessageLength +
                                    CAPWAP_MSG_ELEM_TYPE_LEN);
            }

            break;

        case DOT11AC_INFO_ELEM_VHTOPERATION:

            pRadioIfInfoElem->u1EleId = DOT11AC_INFO_ELEM_VHTOPERATION;

            for (u1Index = 1; u1Index <= u2NumOfRadios; u1Index++)
            {
#ifdef WLC_WANTED
                pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;
                pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2IntId;
                pWssIfCapwapDB->CapwapGetDB.u1RadioId = u1Index;
                if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                             pWssIfCapwapDB) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Getting Radio"
                                "index failed\r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Getting Radio" "index failed\r\n"));
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    return OSIX_FAILURE;
                }
                RadioIfGetDB.RadioIfIsGetAllDB.bRadioIfIndex = OSIX_TRUE;
                RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
                    pWssIfCapwapDB->CapwapGetDB.u4IfIndex;
                u4Index = WSS_GET_RADIO_IF_DB;
#else
                u4Index = WSS_GET_PHY_RADIO_IF_DB;
                UNUSED_PARAM (u2IntId);

#endif
                RadioIfGetDB.RadioIfGetAllDB.u1RadioId = u1Index;
                RadioIfGetDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_TRUE;
                if (WssIfProcessRadioIfDBMsg ((UINT1) u4Index, &RadioIfGetDB)
                    != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "Failed to Get Radio Info from Radio DB \r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Failed to Get Radio Info from Radio DB \r\n"));
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    return OSIX_FAILURE;
                }

                u4Index = WSS_GET_RADIO_IF_DB_11AC;

                RadioIfGetDB.RadioIfIsGetAllDB.bVhtOperInfo = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bVhtOperMcsSet = OSIX_TRUE;

                if (WssIfProcessRadioIfDBMsg ((UINT1) u4Index, &RadioIfGetDB)
                    != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "Failed to Get VHTOperation Info"
                                "from Radio DB \r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Failed to Get VHTOperation Info"
                                  "from Radio DB \r\n"));
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    return OSIX_FAILURE;
                }

                pRadioIfInfoElem->u1RadioId = u1Index;
                pRadioIfInfoElem->u1WlanId = VALUE_1;
                pRadioIfInfoElem->u1BPFlag = 0xC0;
                pRadioIfInfoElem->u1EleId = DOT11AC_INFO_ELEM_VHTOPERATION;

                pRadioIfInfoElem->unInfoElem.VHTOperation.
                    u1ElemId = DOT11AC_INFO_ELEM_VHTOPERATION;

                MEMCPY ((pRadioIfInfoElem->unInfoElem.VHTOperation.
                         au1VhtOperInfo),
                        (RadioIfGetDB.RadioIfGetAllDB.Dot11AcOperParams.
                         au1VhtOperInfo), DOT11AC_VHT_OPER_INFO_LEN);

                pRadioIfInfoElem->unInfoElem.VHTOperation.u2BasicMCS =
                    RadioIfGetDB.RadioIfGetAllDB.Dot11AcOperParams.
                    u2VhtOperMcsSet;

                pRadioIfInfoElem->isPresent = OSIX_TRUE;
                pRadioIfInfoElem->unInfoElem.VHTOperation.isOptional =
                    OSIX_TRUE;

                pRadioIfInfoElem->unInfoElem.VHTOperation.u1ElemLen =
                    IEEE_INFO_ELEM_VHT_OPERATION_LEN;

                pRadioIfInfoElem->u2MessageType = IEEE_INFORMATION_ELEMENT;

                pRadioIfInfoElem->u2MessageLength =
                    IEEE_INFORMATION_ELEMENT_LEN +
                    IEEE_INFO_ELEM_VHT_OPERATION_LEN;
                *pMsgLen = (UINT2) (*pMsgLen +
                                    pRadioIfInfoElem->u2MessageLength +
                                    CAPWAP_MSG_ELEM_TYPE_LEN);
            }

            break;
        default:
            break;
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}

INT4
CapwapGetIeeeMacOperation (tRadioIfMacOperation * pRadioIfMacOperation,
                           UINT2 *pMsgLen, UINT2 u2IntId)
{
    tRadioIfGetDB       RadioIfGetDB;
    UINT2               u2NumOfRadios = 1;
    UINT1               u1Index = 0;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT4               u4Index = 0;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    for (u1Index = 1; u1Index <= u2NumOfRadios; u1Index++)
    {
        RadioIfGetDB.RadioIfIsGetAllDB.bRTSThreshold = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bShortRetry = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bLongRetry = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bFragmentationThreshold = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bTxMsduLifetime = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bRxMsduLifetime = OSIX_TRUE;
        RadioIfGetDB.RadioIfGetAllDB.u1RadioId = u1Index;
#ifdef WLC_WANTED
        pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;
        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2IntId;
        pWssIfCapwapDB->CapwapGetDB.u1RadioId = u1Index;
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                     pWssIfCapwapDB) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Getting Radio index failed\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Getting Radio index failed\r\n"));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
        RadioIfGetDB.RadioIfIsGetAllDB.bRadioIfIndex = OSIX_TRUE;
        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
            pWssIfCapwapDB->CapwapGetDB.u4IfIndex;
        u4Index = WSS_GET_RADIO_IF_DB;
#else
        UNUSED_PARAM (u2IntId);
        u4Index = WSS_GET_PHY_RADIO_IF_DB;

#endif
        if (WssIfProcessRadioIfDBMsg ((UINT1) u4Index, &RadioIfGetDB) !=
            OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to Get IEEE MAC Operation"
                        " Info from Radio DB \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to Get IEEE MAC Operation"
                          " Info from Radio DB \r\n"));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
        pRadioIfMacOperation->u4TxMsduLifetime =
            RadioIfGetDB.RadioIfGetAllDB.u4RxMsduLifetime;
        pRadioIfMacOperation->u4RxMsduLifetime =
            RadioIfGetDB.RadioIfGetAllDB.u4TxMsduLifetime;
        pRadioIfMacOperation->u2MessageType = IEEE_MAC_OPERATION;
        pRadioIfMacOperation->u2RTSThreshold =
            RadioIfGetDB.RadioIfGetAllDB.u2RTSThreshold;
        pRadioIfMacOperation->u2FragmentThreshold =
            RadioIfGetDB.RadioIfGetAllDB.u2FragmentationThreshold;
        pRadioIfMacOperation->u1RadioId = u1Index;
        pRadioIfMacOperation->u1ShortRetry =
            RadioIfGetDB.RadioIfGetAllDB.u1ShortRetry;
        pRadioIfMacOperation->u1LongRetry =
            RadioIfGetDB.RadioIfGetAllDB.u1LongRetry;
        pRadioIfMacOperation->isPresent = OSIX_TRUE;
        pRadioIfMacOperation->u2MessageLength = IEEE_MAC_OPERATION_LEN;
        *pMsgLen = (UINT2) (*pMsgLen +
                            (pRadioIfMacOperation->u2MessageLength +
                             CAPWAP_MSG_ELEM_TYPE_LEN));
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}

INT4
CapwapGetIeeeMultiDomainCapability (tRadioIfMultiDomainCap
                                    * pRadioIfMultiDomainCap,
                                    UINT2 *pMsgLen, UINT2 u2IntId)
{
    tRadioIfGetDB       RadioIfGetDB;
    UINT2               u2NumOfRadios = 1;
    UINT1               u1Index = 0;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT4               u4Index = 0;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    for (u1Index = 1; u1Index <= u2NumOfRadios; u1Index++)
    {
        RadioIfGetDB.RadioIfIsGetAllDB.bFirstChannelNumber = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bMaxTxPowerLevel = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bNoOfChannels = OSIX_TRUE;
        RadioIfGetDB.RadioIfGetAllDB.u1RadioId = u1Index;
#ifdef WLC_WANTED
        pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;
        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2IntId;
        pWssIfCapwapDB->CapwapGetDB.u1RadioId = u1Index;
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                     pWssIfCapwapDB) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Getting Radio index failed\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Getting Radio index failed\r\n"));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
        RadioIfGetDB.RadioIfIsGetAllDB.bRadioIfIndex = OSIX_TRUE;
        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
            pWssIfCapwapDB->CapwapGetDB.u4IfIndex;
        u4Index = WSS_GET_RADIO_IF_DB;
#else
        UNUSED_PARAM (u2IntId);
        u4Index = WSS_GET_PHY_RADIO_IF_DB;

#endif
        if (WssIfProcessRadioIfDBMsg ((UINT1) u4Index, &RadioIfGetDB) !=
            OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Fail to Get Multi Capability Info from Radio DB \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Fail to Get Multi Capability Info from Radio DB \r\n"));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
        pRadioIfMultiDomainCap->u2MessageType = IEEE_MULTIDOMAIN_CAPABILITY;
        pRadioIfMultiDomainCap->u2FirstChannel =
            RadioIfGetDB.RadioIfGetAllDB.u2FirstChannelNumber;
        pRadioIfMultiDomainCap->u2NumOfChannels =
            RadioIfGetDB.RadioIfGetAllDB.u2NoOfChannels;
        pRadioIfMultiDomainCap->u2MaxTxPowerLevel =
            RadioIfGetDB.RadioIfGetAllDB.u2MaxTxPowerLevel;
        pRadioIfMultiDomainCap->u1RadioId = u1Index;
        pRadioIfMultiDomainCap->isPresent = OSIX_TRUE;
        pRadioIfMultiDomainCap->u2MessageLength =
            IEEE_MULTIDOMAIN_CAPABILITY_LEN;
        *pMsgLen = (UINT2) (*pMsgLen +
                            (pRadioIfMultiDomainCap->u2MessageLength +
                             CAPWAP_MSG_ELEM_TYPE_LEN));
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}

INT4
CapwapGetIeeeOFDMControl (tRadioIfOFDMPhy * pRadioIfOFDMPhy, UINT2 *pMsgLen,
                          UINT2 u2IntId)
{
    tRadioIfGetDB       RadioIfGetDB;
    UINT2               u2NumOfRadios = 1;
    UINT1               u1Index = 0;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT4               u4Index = 0;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    for (u1Index = 1; u1Index <= u2NumOfRadios; u1Index++)
    {
        RadioIfGetDB.RadioIfIsGetAllDB.bSupportedBand = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bT1Threshold = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bCurrentChannel = OSIX_TRUE;
        RadioIfGetDB.RadioIfGetAllDB.u1RadioId = u1Index;
#ifdef WLC_WANTED
        pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;
        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2IntId;
        pWssIfCapwapDB->CapwapGetDB.u1RadioId = u1Index;
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                     pWssIfCapwapDB) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Getting Radio index failed\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Getting Radio index failed\r\n"));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
        RadioIfGetDB.RadioIfIsGetAllDB.bRadioIfIndex = OSIX_TRUE;
        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
            pWssIfCapwapDB->CapwapGetDB.u4IfIndex;
        u4Index = WSS_GET_RADIO_IF_DB;
#else
        UNUSED_PARAM (u2IntId);
        u4Index = WSS_GET_PHY_RADIO_IF_DB;

#endif
        if (WssIfProcessRadioIfDBMsg ((UINT1) u4Index, &RadioIfGetDB) !=
            OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to Get IEEE OFDM Information"
                        " from Radio DB \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to Get IEEE OFDM Information"
                          " from Radio DB \r\n"));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
        pRadioIfOFDMPhy->u4T1Threshold =
            RadioIfGetDB.RadioIfGetAllDB.u4T1Threshold;
        pRadioIfOFDMPhy->u2MessageType = IEEE_OFDM_CONTROL;
        pRadioIfOFDMPhy->u1RadioId = u1Index;
        pRadioIfOFDMPhy->u1CurrentChannel =
            RadioIfGetDB.RadioIfGetAllDB.u1CurrentChannel;
        pRadioIfOFDMPhy->u1SupportedBand =
            RadioIfGetDB.RadioIfGetAllDB.u1SupportedBand;
        pRadioIfOFDMPhy->isPresent = OSIX_TRUE;
        pRadioIfOFDMPhy->u2MessageLength = IEEE_OFDM_CONTROL_LEN;
        *pMsgLen = (UINT2) (*pMsgLen +
                            (pRadioIfOFDMPhy->u2MessageLength +
                             CAPWAP_MSG_ELEM_TYPE_LEN));
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}

INT4
CapwapGetIeeeRateSet (tRadioIfRateSet * pRadioIfRateSet, UINT2 *pMsgLen,
                      UINT2 u2IntId)
{
    tRadioIfGetDB       RadioIfGetDB;
    UINT2               u2NumOfRadios = 1;
    UINT1               u1Index;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT4               u4Index = 0;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    for (u1Index = 1; u1Index <= u2NumOfRadios; u1Index++)
    {
        RadioIfGetDB.RadioIfIsGetAllDB.bOperationalRate = OSIX_TRUE;
        RadioIfGetDB.RadioIfGetAllDB.u1RadioId = u1Index;
#ifdef WLC_WANTED
        pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;
        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2IntId;
        pWssIfCapwapDB->CapwapGetDB.u1RadioId = u1Index;
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                     pWssIfCapwapDB) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Getting Radio index failed\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Getting Radio index failed\r\n"));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
        RadioIfGetDB.RadioIfIsGetAllDB.bRadioIfIndex = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bOperationalRate = OSIX_TRUE;
        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
            pWssIfCapwapDB->CapwapGetDB.u4IfIndex;
        u4Index = WSS_GET_RADIO_IF_DB;
#else
        u4Index = WSS_GET_PHY_RADIO_IF_DB;
        UNUSED_PARAM (u2IntId);

#endif
        if (WssIfProcessRadioIfDBMsg ((UINT1) u4Index, &RadioIfGetDB) !=
            OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to Get IEEE Rate Set Info from Radio DB \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to Get IEEE Rate Set Info from Radio DB \r\n"));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
        pRadioIfRateSet->u2MessageType = IEEE_RATE_SET;
        pRadioIfRateSet->u2MessageLength = MIN_IEEE_RATE_SET_LEN;
        pRadioIfRateSet->isPresent = OSIX_TRUE;

        *pMsgLen = (UINT2) (*pMsgLen +
                            (pRadioIfRateSet->u2MessageLength +
                             CAPWAP_MSG_ELEM_TYPE_LEN));
        MEMCPY (pRadioIfRateSet->au1OperationalRate,
                RadioIfGetDB.RadioIfGetAllDB.au1OperationalRate,
                sizeof (RadioIfGetDB.RadioIfGetAllDB.au1OperationalRate));

        pRadioIfRateSet->u1RadioId = pWssIfCapwapDB->CapwapGetDB.u1RadioId;

    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}

INT4
CapwapGetIeeeSupportedRates (tRadioIfSupportedRate * pRadioIfSupportedRate,
                             UINT2 *pMsgLen, UINT2 u2IntId)
{
    tRadioIfGetDB       RadioIfGetDB;
    UINT2               u2NumOfRadios = 1, u2Len;
    UINT1               u1Index = 0;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT4               u4Index = 0;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    for (u1Index = 1; u1Index <= u2NumOfRadios; u1Index++)
    {
        RadioIfGetDB.RadioIfIsGetAllDB.bSupportedRate = OSIX_TRUE;
        RadioIfGetDB.RadioIfGetAllDB.u1RadioId = u1Index;
#ifdef WLC_WANTED
        pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;
        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2IntId;
        pWssIfCapwapDB->CapwapGetDB.u1RadioId = u1Index;
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                     pWssIfCapwapDB) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Getting Radio index failed\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Getting Radio index failed\r\n"));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
        RadioIfGetDB.RadioIfIsGetAllDB.bRadioIfIndex = OSIX_TRUE;
        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
            pWssIfCapwapDB->CapwapGetDB.u4IfIndex;
        u4Index = WSS_GET_RADIO_IF_DB;
#else
        u4Index = WSS_GET_PHY_RADIO_IF_DB;
        UNUSED_PARAM (u2IntId);

#endif
        if (WssIfProcessRadioIfDBMsg ((UINT1) u4Index, &RadioIfGetDB) !=
            OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Fail to Get IEEE Supported Rates"
                        " Info from Radio DB \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Fail to Get IEEE Supported Rates"
                          " Info from Radio DB \r\n"));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
        pRadioIfSupportedRate->u2MessageType = IEEE_SUPPORTED_RATES;
        u2Len = sizeof (RadioIfGetDB.RadioIfGetAllDB.au1SupportedRate);
        MEMCPY (pRadioIfSupportedRate->au1SupportedRate,
                RadioIfGetDB.RadioIfGetAllDB.au1SupportedRate, u2Len);

        pRadioIfSupportedRate->u1RadioId = u1Index;
        pRadioIfSupportedRate->isPresent = OSIX_TRUE;
        pRadioIfSupportedRate->u2MessageLength = (UINT2) (u2Len + 1);
        *pMsgLen = (UINT2) (*pMsgLen +
                            (pRadioIfSupportedRate->u2MessageLength +
                             CAPWAP_MSG_ELEM_TYPE_LEN));
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}

INT4
CapwapGetIeeeTxPower (tRadioIfTxPower * pRadioIfTxPower, UINT2 *pMsgLen,
                      UINT2 u2IntId)
{
    tRadioIfGetDB       RadioIfGetDB;
    UINT2               u2NumOfRadios = 1;
    UINT1               u1Index = 0;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT4               u4Index = 0;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    for (u1Index = 1; u1Index <= u2NumOfRadios; u1Index++)
    {
        RadioIfGetDB.RadioIfIsGetAllDB.bCurrentTxPower = OSIX_TRUE;
        RadioIfGetDB.RadioIfGetAllDB.u1RadioId = u1Index;
#ifdef WLC_WANTED
        pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;
        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2IntId;
        pWssIfCapwapDB->CapwapGetDB.u1RadioId = u1Index;
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                     pWssIfCapwapDB) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Getting Radio index failed\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Getting Radio index failed\r\n"));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
        RadioIfGetDB.RadioIfIsGetAllDB.bRadioIfIndex = OSIX_TRUE;
        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
            pWssIfCapwapDB->CapwapGetDB.u4IfIndex;
        u4Index = WSS_GET_RADIO_IF_DB;
#else
        u4Index = WSS_GET_PHY_RADIO_IF_DB;
        UNUSED_PARAM (u2IntId);

#endif
        if (WssIfProcessRadioIfDBMsg ((UINT1) u4Index, &RadioIfGetDB) !=
            OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Fail to Get IEEE Tx Power Information from Radio DB \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Fail to Get IEEE Tx Power Information from Radio DB \r\n"));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
        pRadioIfTxPower->u2MessageType = IEEE_TX_POWER;
        pRadioIfTxPower->u2TxPowerLevel =
            (UINT2) RadioIfGetDB.RadioIfGetAllDB.i2CurrentTxPower;
        pRadioIfTxPower->u1RadioId = u1Index;

        pRadioIfTxPower->isPresent = OSIX_TRUE;
        pRadioIfTxPower->u2MessageLength = IEEE_TX_POWER_LEN;
        *pMsgLen = (UINT2) (*pMsgLen + (pRadioIfTxPower->u2MessageLength +
                                        CAPWAP_MSG_ELEM_TYPE_LEN));
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}

INT4
CapwapGetIeeeTxPowerLevel (tRadioIfTxPowerLevel * pRadioIfTxPowerLevel,
                           UINT2 *pMsgLen, UINT2 u2IntId)
{
    tRadioIfGetDB       RadioIfGetDB;
    UINT2               u2NumOfRadios = 1;
    UINT1               u1Index = 0, u1Temp;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT4               u4Index = 0;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    for (u1Index = 1; u1Index <= u2NumOfRadios; u1Index++)
    {
        RadioIfGetDB.RadioIfIsGetAllDB.bNoOfSupportedPowerLevels = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bTxPowerLevel = OSIX_TRUE;
        RadioIfGetDB.RadioIfGetAllDB.u1RadioId = u1Index;
#ifdef WLC_WANTED
        pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;
        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2IntId;
        pWssIfCapwapDB->CapwapGetDB.u1RadioId = u1Index;
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                     pWssIfCapwapDB) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Getting Radio index failed\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Getting Radio index failed\r\n"));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
        RadioIfGetDB.RadioIfIsGetAllDB.bRadioIfIndex = OSIX_TRUE;
        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
            pWssIfCapwapDB->CapwapGetDB.u4IfIndex;
        u4Index = WSS_GET_RADIO_IF_DB;
#else
        u4Index = WSS_GET_PHY_RADIO_IF_DB;
        UNUSED_PARAM (u2IntId);

#endif
        if (WssIfProcessRadioIfDBMsg ((UINT1) u4Index, &RadioIfGetDB) !=
            OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Fail to Get Tx Power Level Info from Radio DB \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Fail to Get Tx Power Level Info from Radio DB \r\n"));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
        pRadioIfTxPowerLevel->u2MessageType = IEEE_TX_POWERLEVEL;
        pRadioIfTxPowerLevel->u1RadioId = u1Index;
        pRadioIfTxPowerLevel->u1NumOfLevels = RadioIfGetDB.
            RadioIfGetAllDB.u1NoOfSupportedPowerLevels;
        pRadioIfTxPowerLevel->isPresent = OSIX_TRUE;
        for (u1Temp = 0; u1Temp < RADIOIF_MAX_POWER_LEVEL; u1Temp++)
        {
            pRadioIfTxPowerLevel->au2PowerLevels[u1Temp] =
                RadioIfGetDB.RadioIfGetAllDB.au2TxPowerLevel[u1Temp];
        }
        pRadioIfTxPowerLevel->u2MessageLength = RADIO_TX_POWER_LEN;
        *pMsgLen = (UINT2) (*pMsgLen +
                            (pRadioIfTxPowerLevel->u2MessageLength +
                             CAPWAP_MSG_ELEM_TYPE_LEN));
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}

INT4
CapwapGetIeeeRadioConfiguration (tRadioIfConfig * pRadioIfConfig,
                                 UINT2 *pMsgLen, UINT2 u2IntId)
{
    tRadioIfGetDB       RadioIfGetDB;
    UINT2               u2NumOfRadios = 1, u2Len = 0;
    UINT1               u1Index = 0;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT4               u4Index = 0;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    for (u1Index = 1; u1Index <= u2NumOfRadios; u1Index++)

    {
        RadioIfGetDB.RadioIfIsGetAllDB.bShortPreamble = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bNoOfBssId = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bDTIMPeriod = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bBeaconPeriod = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bCountryString = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bMacAddr = OSIX_TRUE;
        RadioIfGetDB.RadioIfGetAllDB.u1RadioId = u1Index;
#ifdef WLC_WANTED
        pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;
        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2IntId;
        pWssIfCapwapDB->CapwapGetDB.u1RadioId = u1Index;
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                     pWssIfCapwapDB) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Getting Radio index failed\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Getting Radio index failed\r\n"));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
        RadioIfGetDB.RadioIfIsGetAllDB.bRadioIfIndex = OSIX_TRUE;
        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
            pWssIfCapwapDB->CapwapGetDB.u4IfIndex;
        u4Index = WSS_GET_RADIO_IF_DB;
#else
        u4Index = WSS_GET_PHY_RADIO_IF_DB;
        UNUSED_PARAM (u2IntId);

#endif
        if (WssIfProcessRadioIfDBMsg ((UINT1) u4Index, &RadioIfGetDB) !=
            OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to Get IEEE Configuration Information"
                        "from Radio DB \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to Get IEEE Configuration Information"
                          "from Radio DB \r\n"));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
        pRadioIfConfig->u2MessageType = IEEE_WTP_RADIO_CONFIGURATION;
        pRadioIfConfig->u2BeaconPeriod =
            RadioIfGetDB.RadioIfGetAllDB.u2BeaconPeriod;
        pRadioIfConfig->u1RadioId = u1Index;
        pRadioIfConfig->u1ShortPreamble =
            RadioIfGetDB.RadioIfGetAllDB.u1ShortPreamble;
        pRadioIfConfig->u1NoOfBssId = RadioIfGetDB.RadioIfGetAllDB.u1NoOfBssId;
        pRadioIfConfig->u1DtimPeriod =
            RadioIfGetDB.RadioIfGetAllDB.u1DTIMPeriod;

        MEMCPY (pRadioIfConfig->MacAddr,
                RadioIfGetDB.RadioIfGetAllDB.MacAddr, RADIO_MAC_LEN);
        u2Len =
            (UINT2) (STRLEN (RadioIfGetDB.RadioIfGetAllDB.au1CountryString));
        MEMCPY (pRadioIfConfig->au1CountryString,
                RadioIfGetDB.RadioIfGetAllDB.au1CountryString, u2Len);
        pRadioIfConfig->isPresent = OSIX_TRUE;
        /*if (u2Len != 4) */
        u2Len = CAPWAP_MSG_ELEM_TYPE_LEN;
        pRadioIfConfig->u2MessageLength = (UINT2) (u2Len + RADIO_CONFI_MSG_LEN);
        *pMsgLen = (UINT2) (*pMsgLen +
                            (pRadioIfConfig->u2MessageLength +
                             CAPWAP_MSG_ELEM_TYPE_LEN));
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : CapwapGetIeeeWtpQos                                        *
 *                                                                           *
 * Description  : This function gets the Ieee Wtp QoS message from Radio DB. *
 *                                                                           *
 * Input        : pRadioIfQos - Pointer to QoS Struct                        *
 *                pMsgLen - Pointer to Message Len                           *
 *                u2IntId - Wtp Internal Id                                  *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
CapwapGetIeeeWtpQos (tRadioIfQos * pRadioIfQos, UINT2 *pMsgLen, UINT2 u2IntId)
{
    tRadioIfGetDB       RadioIfGetDB;
    UINT2               u2NumOfRadios = 1;
    UINT1               u1Index = 0, u1QosIndex = 0, u1ArrIndex = 0;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;

    CAPWAP_FN_ENTRY ();
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    for (u1Index = 1; u1Index <= u2NumOfRadios; u1Index++)
    {
        RadioIfGetDB.RadioIfGetAllDB.u1RadioId = u1Index;
#ifdef WLC_WANTED
        pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;
        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2IntId;
        pWssIfCapwapDB->CapwapGetDB.u1RadioId = u1Index;
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
            != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Getting Radio index failed\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Getting Radio index failed\r\n"));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
        RadioIfGetDB.RadioIfIsGetAllDB.bRadioIfIndex = OSIX_TRUE;
        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
            pWssIfCapwapDB->CapwapGetDB.u4IfIndex;
#else
        UNUSED_PARAM (u2IntId);
#endif

        for (u1QosIndex = 1;
             u1QosIndex <= WSSIF_RADIOIF_QOS_CONFIG_LEN; u1QosIndex++)
        {
            RadioIfGetDB.RadioIfIsGetAllDB.bQosIndex = OSIX_TRUE;
            RadioIfGetDB.RadioIfIsGetAllDB.bQueueDepth = OSIX_TRUE;
            RadioIfGetDB.RadioIfIsGetAllDB.bCwMin = OSIX_TRUE;
            RadioIfGetDB.RadioIfIsGetAllDB.bCwMax = OSIX_TRUE;
            RadioIfGetDB.RadioIfIsGetAllDB.bAifsn = OSIX_TRUE;
            RadioIfGetDB.RadioIfIsGetAllDB.bQosPrio = OSIX_TRUE;
            RadioIfGetDB.RadioIfIsGetAllDB.bDscp = OSIX_TRUE;
            RadioIfGetDB.RadioIfGetAllDB.u1QosIndex = u1QosIndex;
            if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_QOS_CONFIG_DB,
                                          &RadioIfGetDB) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Failed to Get IEEE Wtp QoS from Radio DB \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to Get IEEE Wtp QoS from Radio DB \r\n"));
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }

            u1ArrIndex = (UINT1) (u1QosIndex - 1);

            pRadioIfQos->u1QueueDepth[u1ArrIndex] =
                RadioIfGetDB.RadioIfGetAllDB.au1QueueDepth[u1ArrIndex];
            pRadioIfQos->u2CwMin[u1ArrIndex] =
                RadioIfGetDB.RadioIfGetAllDB.au2CwMin[u1ArrIndex];
            pRadioIfQos->u2CwMax[u1ArrIndex] =
                RadioIfGetDB.RadioIfGetAllDB.au2CwMax[u1ArrIndex];
            pRadioIfQos->u1Aifsn[u1ArrIndex] =
                RadioIfGetDB.RadioIfGetAllDB.au1Aifsn[u1ArrIndex];
            pRadioIfQos->u1Prio[u1ArrIndex] =
                RadioIfGetDB.RadioIfGetAllDB.au1QosPrio[u1ArrIndex];
            pRadioIfQos->u1Dscp[u1ArrIndex] =
                RadioIfGetDB.RadioIfGetAllDB.au1Dscp[u1ArrIndex];
        }

        pRadioIfQos->u2MessageType = IEEE_WTP_QOS;
        pRadioIfQos->u1RadioId = u1Index;
        pRadioIfQos->u1TaggingPolicy =
            RadioIfGetDB.RadioIfGetAllDB.u1TaggingPolicy;
        pRadioIfQos->isPresent = OSIX_TRUE;
        pRadioIfQos->u2MessageLength = IEEE_WTP_QOS_LEN;
        *pMsgLen = (UINT2) (*pMsgLen + (pRadioIfQos->u2MessageLength + 4));
    }

    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : CapwapCheckServRelinquishCounters                          *
 *                                                                           *
 * Description  : Check relinquish counters and take action                  *
 *                                                                           *
 * Input        : NONE                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
VOID
CapwapCheckServRelinquishCounters (VOID)
{
#ifdef WTP_WANTED
#if 0
    UINT4               u4Event;
    gu4CPUSerRelinquishCounter++;
    {
        if (gCapwapState != CAPWAP_IMAGEDATA && gbCapwapImageData != OSIX_TRUE)
        {
            if (gu4CPUSerRelinquishCounter == CAPWAP_RELINQUISH_MSG_COUNT)
            {
                OsixEvtRecv (gSerTaskGlobals.capwapSerTaskId,
                             CAPWAP_SER_RELINQUISH_EVENT, OSIX_WAIT, &u4Event);
                gu4CPUSerRelinquishCounter = 0;
            }
        }
    }
#endif
#endif
    return;
}

/*****************************************************************************
 * Function     : CapwapCheckServRelinquishCounters                          *
 *                                                                           *
 * Description  : Check relinquish counters and take action                  *
 *                                                                           *
 * Input        : NONE                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
VOID
CapwapServiceCPURelinquishTmrExp (VOID *pArg)
{
    UNUSED_PARAM (pArg);
#if 0
    gu4CPUSerRelinquishCounter = 0;
    if (OsixEvtSend (gSerTaskGlobals.capwapSerTaskId,
                     CAPWAP_SER_RELINQUISH_EVENT) != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Timer event send failure.\r\n");
    }

    if (CapwapTmrStart (NULL, CAPWAP_CPU_RELINQUISH_TMR,
                        CAPWAP_RELINQUISH_TIMER_VAL) != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, " Timer Start FAILED.\r\n");
    }
#endif
    return;
}

#endif
