/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved                      *
 * $Id: capwaptmr.c,v 1.3 2017/11/24 10:37:03 siva Exp $       
 * Description: This file contains the CAPWAP Timer related functions        * 
 ******************************************************************************/
#ifndef __CAPWAPTMR_C__
#define __CAPWAPTMR_C__
#define _CAPWAP_GLOBAL_VAR
#include "capwapinc.h"
#include "aphdlr.h"
#include "aphdlrprot.h"

tCapwapTimerList    gCapwapTmrList;
extern tSerTaskGlobals gSerTaskGlobals;
extern INT4
        WlchdlrFreeTransmittedPacket (tRemoteSessionManager * pSessEntry);
/* timer block */
static tTmrBlk      tmrSerTmrBlk;
static tTmrBlk      tmrRecvTmrBlk;
static tTmrBlk      tmrDiscRelTmrBlk;

/*****************************************************************************
 *                                                                           *
 * Function     : CapwapTmrInit                                              *
 *                                                                           *
 * Description  :  This function creates a timer list for all the timers     *
 *                          in CAPWAP module.                                *
 *                                                                           *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
CapwapTmrInit (VOID)
{
    /* Timer List for State Machine Timers */
    if (TmrCreateTimerList ((CONST UINT1 *) CAPWAP_SERV_TASK_NAME,
                            CAPWAP_FSM_TMR_EXP_EVENT,
                            NULL,
                            (tTimerListId *) & (gCapwapTmrList.
                                                capwapFsmTmrListId)) ==
        TMR_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "capwapTmrInit: Failed to creat the Application Timer List");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "capwapTmrInit: Failed to creat the Application Timer List"));
        return OSIX_FAILURE;
    }

    /* Timer List for Run State Timers */
    if (TmrCreateTimerList ((CONST UINT1 *) CAPWAP_SERV_TASK_NAME,
                            CAPWAP_RUN_TMR_EXP_EVENT,
                            NULL,
                            (tTimerListId *) & (gCapwapTmrList.
                                                capwapRunTmrListId)) ==
        TMR_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "capwapTmrInit: Failed to creat the Application Timer List");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "capwapTmrInit: Failed to creat the Application Timer List"));
        return OSIX_FAILURE;
    }

    CapwapTmrInitTmrDesc ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : CapwapTmrInitTmrDesc                                       *
 *                                                                           *
 * Description  : This function intializes the timer desc for all            *
 *                the timers in CAPWAP module.                               *
 *                                                                           *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
VOID
CapwapTmrInitTmrDesc (VOID)
{
    /* FSM Timers */
    gCapwapTmrList.aCapwapFsmTmrDesc[CAPWAP_WAITDTLS_TMR].TmrExpFn
        = CapwapFsmTmrExp;
    gCapwapTmrList.aCapwapFsmTmrDesc[CAPWAP_WAITDTLS_TMR].i2Offset =
        (INT2) FSAP_OFFSETOF (tRemoteSessionManager, WaitDtlsTmr);

    gCapwapTmrList.aCapwapFsmTmrDesc[CAPWAP_WAITJOIN_TMR].TmrExpFn
        = CapwapFsmTmrExp;
    gCapwapTmrList.aCapwapFsmTmrDesc[CAPWAP_WAITJOIN_TMR].i2Offset =
        (INT2) FSAP_OFFSETOF (tRemoteSessionManager, WaitJoinTmr);

    /*ChangeStatePendingTimer */
    gCapwapTmrList.aCapwapFsmTmrDesc[CAPWAP_CHANGESTATE_PENDING_TMR].TmrExpFn
        = CapwapFsmTmrExp;
    gCapwapTmrList.aCapwapFsmTmrDesc[CAPWAP_CHANGESTATE_PENDING_TMR].i2Offset =
        (INT2) FSAP_OFFSETOF (tRemoteSessionManager, ChangeStatePendingTmr);

    /* Data Check Timer */
    gCapwapTmrList.aCapwapFsmTmrDesc[CAPWAP_DATACHECK_TMR].TmrExpFn
        = CapwapFsmTmrExp;
    gCapwapTmrList.aCapwapFsmTmrDesc[CAPWAP_DATACHECK_TMR].i2Offset =
        (INT2) FSAP_OFFSETOF (tRemoteSessionManager, DataCheckTmr);

    /* Image Data Start Timer */
    gCapwapTmrList.aCapwapFsmTmrDesc[CAPWAP_IMAGEDATA_START_TMR].TmrExpFn
        = CapwapImageDataStartTmrExp;
    gCapwapTmrList.aCapwapFsmTmrDesc[CAPWAP_IMAGEDATA_START_TMR].i2Offset =
        (INT2) FSAP_OFFSETOF (tRemoteSessionManager, ImageDataStartTmr);

    /* Relinquish timer */
    gCapwapTmrList.aCapwapFsmTmrDesc[CAPWAP_CPU_RELINQUISH_TMR].TmrExpFn
        = CapwapServiceCPURelinquishTmrExp;
    gCapwapTmrList.aCapwapFsmTmrDesc[CAPWAP_CPU_RELINQUISH_TMR].i2Offset = -1;

    /* Relinquish timer */
    gCapwapTmrList.aCapwapFsmTmrDesc[CAPWAP_CPU_RECV_RELINQUISH_TMR].TmrExpFn
        = CapwapRecvCPURelinquishTmrExp;
    gCapwapTmrList.aCapwapFsmTmrDesc[CAPWAP_CPU_RECV_RELINQUISH_TMR].i2Offset
        = -1;

    /* DISC Relinquish timer */
    gCapwapTmrList.aCapwapFsmTmrDesc[CAPWAP_CPU_DISC_RELINQUISH_TMR].TmrExpFn
        = CapwapDiscCPURelinquishTmrExp;
    gCapwapTmrList.aCapwapFsmTmrDesc[CAPWAP_CPU_DISC_RELINQUISH_TMR].i2Offset
        = -1;

    /* CAPWAP RUN State Timers */
    gCapwapTmrList.aCapwapRunTmrDesc[CAPWAP_RETRANSMIT_INTERVAL_TMR].TmrExpFn
        = CapwapRetransIntervalTmrExp;
    gCapwapTmrList.aCapwapRunTmrDesc[CAPWAP_RETRANSMIT_INTERVAL_TMR].i2Offset =
        (INT2) FSAP_OFFSETOF (tRemoteSessionManager, RetransIntervalTmr);

    /* Echo Interval Timer */
    gCapwapTmrList.aCapwapRunTmrDesc[CAPWAP_ECHO_INTERVAL_TMR].TmrExpFn
        = CapwapEchoIntervalTmrExp;
    gCapwapTmrList.aCapwapRunTmrDesc[CAPWAP_ECHO_INTERVAL_TMR].i2Offset =
        (INT2) FSAP_OFFSETOF (tRemoteSessionManager, EchoInterval);

    /* CAPWAP Data Dead Interval Timers */
    gCapwapTmrList.aCapwapRunTmrDesc[CAPWAP_DATACHNL_DEADINTERVAL_TMR].TmrExpFn
        = CapwapDataChnlDeadIntTmrExp;
    gCapwapTmrList.aCapwapRunTmrDesc[CAPWAP_DATACHNL_DEADINTERVAL_TMR].
        i2Offset =
        (INT2) FSAP_OFFSETOF (tRemoteSessionManager, DataChnlDeadIntTmr);

    /* CAPWAP Data Keep Alive Timer */
    gCapwapTmrList.aCapwapRunTmrDesc[CAPWAP_DATACHNL_KEEPALIVE_TMR].TmrExpFn
        = CapwapDataChnlKeepAliveTmrExp;
    gCapwapTmrList.aCapwapRunTmrDesc[CAPWAP_DATACHNL_KEEPALIVE_TMR].i2Offset =
        (INT2) FSAP_OFFSETOF (tRemoteSessionManager, DataChnlKeepAliveTmr);

    /* CAPWAP Fragmentation Reassembly Timer */
    gCapwapTmrList.aCapwapRunTmrDesc[CAPWAP_REASSEMBLE_TMR].TmrExpFn
        = CapwapReassemblyTmrExp;
    gCapwapTmrList.aCapwapRunTmrDesc[CAPWAP_REASSEMBLE_TMR].i2Offset =
        (INT2) FSAP_OFFSETOF (tCapFragment, ReassemblyTimer);

    /* CAPWAP Path MTU update Timer */
    gCapwapTmrList.aCapwapRunTmrDesc[CAPWAP_PMTU_UPDATE_TMR].TmrExpFn
        = CapwapPathMtuUpdateTmrExp;
    gCapwapTmrList.aCapwapRunTmrDesc[CAPWAP_PMTU_UPDATE_TMR].i2Offset = OFF_SET;

}

/*****************************************************************************
 *                                                                           *
 * Function     : CapwapFsmTmrExpHandler                                     *
 *                                                                           *
 * Description  :  This function is called whenever a timer expiry           *
 *                 message is received by Service task. Different timer      *
 *                 expiry handlers are called based on the timer type.       *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
VOID
CapwapFsmTmrExpHandler (VOID)
{
    tTmrAppTimer       *pExpiredTimers = NULL;
    UINT1               u1TimerId = 0;
    INT2                i2Offset;

    CAPWAP_FN_ENTRY ();

    while ((pExpiredTimers =
            TmrGetNextExpiredTimer (gCapwapTmrList.capwapFsmTmrListId)) != NULL)
    {

        u1TimerId = ((tTmrBlk *) pExpiredTimers)->u1TimerId;

        CAPWAP_TRC1 (CAPWAP_MGMT_TRC, "Timer to be processed %d\r\n",
                     u1TimerId);
        SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4CapwapSysLogId,
                      "Timer to be processed %d\r\n", u1TimerId));

        if (u1TimerId < CAPWAP_MAX_FSM_TMR)
        {
            i2Offset = gCapwapTmrList.aCapwapFsmTmrDesc[u1TimerId].i2Offset;

            if (i2Offset == OFF_SET)
            {
                /* The timer function does not take any parameter. */
                (*(gCapwapTmrList.aCapwapFsmTmrDesc[u1TimerId].
                   TmrExpFn)) (NULL);
            }
            else
            {
                (*(gCapwapTmrList.aCapwapFsmTmrDesc[u1TimerId].TmrExpFn))
                    ((UINT1 *) pExpiredTimers - i2Offset);
            }
        }
    }
    CAPWAP_FN_EXIT ();
    return;
}

/*****************************************************************************
 *                                                                           *
 * Function     :  CapwapRunTmrExpHandler                                    *
 *                                                                           *
 * Description  :  This function is called whenever a timer expiry           *
 *                 message is received by Service task. Different timer      *
 *                 expiry handlers are called based on the timer type.       *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
VOID
CapwapRunTmrExpHandler (VOID)
{
    tTmrAppTimer       *pExpiredTimers = NULL;
    UINT1               u1TimerId = 0;
    INT2                i2Offset;

    CAPWAP_FN_ENTRY ();

    while ((pExpiredTimers =
            TmrGetNextExpiredTimer (gCapwapTmrList.capwapRunTmrListId)) != NULL)
    {

        u1TimerId = ((tTmrBlk *) pExpiredTimers)->u1TimerId;

        CAPWAP_TRC1 (CAPWAP_MGMT_TRC, "Timer to be processed %d\r\n",
                     u1TimerId);
        SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4CapwapSysLogId,
                      "Timer to be processed %d\r\n", u1TimerId));

        if (u1TimerId < CAPWAP_MAX_RUN_TMR)
        {
            i2Offset = gCapwapTmrList.aCapwapRunTmrDesc[u1TimerId].i2Offset;

            if (i2Offset == OFF_SET)
            {
                /* The timer function does not take any parameter. */
                (*(gCapwapTmrList.aCapwapRunTmrDesc[u1TimerId].
                   TmrExpFn)) (NULL);
            }
            else
            {
                (*(gCapwapTmrList.aCapwapRunTmrDesc[u1TimerId].TmrExpFn))
                    ((UINT1 *) pExpiredTimers - i2Offset);
            }
        }
    }
    CAPWAP_FN_EXIT ();
    return;
}

/*****************************************************************************
 *                                                                           *
 * Function     : CapwapTmrStart                                             *
 *                                                                           *
 * Description  : This function used to start the capwap state machiune      *
 *                specified timers.                                          *
 *                                                                           *
 * Input        : pSessEntry - pointer to session entry table                *
 *            u4TmrInterval - Time interval for which timer must run     *
 *               (in seconds)                                                *
 *                u1TmrType - Indicates which timer to start.                *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
CapwapTmrStart (tRemoteSessionManager * pSessEntry, UINT1 u1TmrType,
                UINT4 u4TmrInterval)
{

    CAPWAP_FN_ENTRY ();
    switch (u1TmrType)
    {
        case CAPWAP_RETRANSMIT_INTERVAL_TMR:
            if (TmrStart (gCapwapTmrList.capwapRunTmrListId,
                          &(pSessEntry->RetransIntervalTmr),
                          u1TmrType, u4TmrInterval, 0) == TMR_FAILURE)
            {
                CAPWAP_TRC1 (CAPWAP_FAILURE_TRC,
                             "CapwapTmrStart:Failed to start the"
                             "Timer Type = %d \r\n", u1TmrType);
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "CapwapTmrStart:Failed to start the"
                              "Timer Type = %d \r\n", u1TmrType));
                return OSIX_FAILURE;
            }
            break;
        case CAPWAP_WAITDTLS_TMR:
            if (TmrStart (gCapwapTmrList.capwapFsmTmrListId,
                          &(pSessEntry->WaitDtlsTmr),
                          u1TmrType, u4TmrInterval, 0) == TMR_FAILURE)
            {
                CAPWAP_TRC1 (CAPWAP_FAILURE_TRC,
                             "CapwapTmrStart:Failed to start the"
                             "Timer Type = %d \r\n", u1TmrType);
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "CapwapTmrStart:Failed to start the"
                              "Timer Type = %d \r\n", u1TmrType));
                return OSIX_FAILURE;
            }
            break;
        case CAPWAP_WAITJOIN_TMR:
            if (TmrStart (gCapwapTmrList.capwapFsmTmrListId,
                          &(pSessEntry->WaitJoinTmr),
                          u1TmrType, u4TmrInterval, 0) == TMR_FAILURE)
            {
                CAPWAP_TRC1 (CAPWAP_FAILURE_TRC,
                             "CapwapTmrStart:Failed to start the Timer"
                             "Type = %d \r\n", u1TmrType);
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "CapwapTmrStart:Failed to start the Timer"
                              "Type = %d \r\n", u1TmrType));
                return OSIX_FAILURE;
            }
            break;
        case CAPWAP_IMAGEDATA_START_TMR:
            if (TmrStart (gCapwapTmrList.capwapFsmTmrListId,
                          &(pSessEntry->ImageDataStartTmr),
                          u1TmrType, u4TmrInterval, 0) == TMR_FAILURE)
            {
                CAPWAP_TRC1 (CAPWAP_FAILURE_TRC,
                             "CapwapTmrStart:Failed to start the Timer"
                             "Type = %d \r\n", u1TmrType);
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "CapwapTmrStart:Failed to start the Timer"
                              "Type = %d \r\n", u1TmrType));
                return OSIX_FAILURE;
            }
            break;
        case CAPWAP_CPU_RELINQUISH_TMR:
            if (TmrStart (gCapwapTmrList.capwapFsmTmrListId, &tmrSerTmrBlk,
                          u1TmrType, u4TmrInterval, 0) == TMR_FAILURE)
            {
                CAPWAP_TRC1 (CAPWAP_FAILURE_TRC,
                             "CapwapTmrStart:Failed to start the Timer"
                             "Type = %d \r\n", u1TmrType);
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "CapwapTmrStart:Failed to start the Timer"
                              "Type = %d \r\n", u1TmrType));
                return OSIX_FAILURE;
            }
            break;
        case CAPWAP_CPU_RECV_RELINQUISH_TMR:
            if (TmrStart (gCapwapTmrList.capwapFsmTmrListId, &tmrRecvTmrBlk,
                          u1TmrType, u4TmrInterval, 0) == TMR_FAILURE)
            {
                CAPWAP_TRC1 (CAPWAP_FAILURE_TRC,
                             "CapwapTmrStart:Failed to start the Timer"
                             "Type = %d \r\n", u1TmrType);
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "CapwapTmrStart:Failed to start the Timer"
                              "Type = %d \r\n", u1TmrType));
                return OSIX_FAILURE;
            }
            break;
        case CAPWAP_CPU_DISC_RELINQUISH_TMR:
            if (TmrStart (gCapwapTmrList.capwapFsmTmrListId, &tmrDiscRelTmrBlk,
                          u1TmrType, u4TmrInterval, 0) == TMR_FAILURE)
            {
                CAPWAP_TRC1 (CAPWAP_FAILURE_TRC,
                             "CapwapTmrStart:Failed to start the Timer"
                             "Type = %d \r\n", u1TmrType);
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "CapwapTmrStart:Failed to start the Timer"
                              "Type = %d \r\n", u1TmrType));
                return OSIX_FAILURE;
            }
            break;
        case CAPWAP_CHANGESTATE_PENDING_TMR:
            if (TmrStart (gCapwapTmrList.capwapFsmTmrListId,
                          &(pSessEntry->ChangeStatePendingTmr),
                          u1TmrType, u4TmrInterval, 0) == TMR_FAILURE)
            {
                CAPWAP_TRC1 (CAPWAP_FAILURE_TRC,
                             "CapwapTmrStart:Failed to start the"
                             "Timer Type = %d \r\n", u1TmrType);
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "CapwapTmrStart:Failed to start the"
                              "Timer Type = %d \r\n", u1TmrType));
                return OSIX_FAILURE;
            }
            break;
        case CAPWAP_DATACHECK_TMR:
            if (TmrStart (gCapwapTmrList.capwapFsmTmrListId,
                          &(pSessEntry->DataCheckTmr),
                          u1TmrType, u4TmrInterval, 0) == TMR_FAILURE)
            {
                CAPWAP_TRC1 (CAPWAP_FAILURE_TRC,
                             "CapwapTmrStart:Failed to start the Timer"
                             "Type = %d \r\n", u1TmrType);
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "CapwapTmrStart:Failed to start the Timer"
                              "Type = %d \r\n", u1TmrType));
                return OSIX_FAILURE;
            }
            break;
        case CAPWAP_DATACHNL_DEADINTERVAL_TMR:
            if (TmrStart (gCapwapTmrList.capwapRunTmrListId,
                          &(pSessEntry->DataChnlDeadIntTmr),
                          u1TmrType, u4TmrInterval, 0) == TMR_FAILURE)
            {
                CAPWAP_TRC1 (CAPWAP_FAILURE_TRC,
                             "CapwapTmrStart:Failed to start the Timer"
                             "Type = %d \r\n", u1TmrType);
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "CapwapTmrStart:Failed to start the Timer"
                              "Type = %d \r\n", u1TmrType));
                return OSIX_FAILURE;
            }
            break;
        case CAPWAP_DATACHNL_KEEPALIVE_TMR:
            if (TmrStart (gCapwapTmrList.capwapRunTmrListId,
                          &(pSessEntry->DataChnlKeepAliveTmr),
                          u1TmrType, u4TmrInterval, 0) == TMR_FAILURE)
            {
                CAPWAP_TRC1 (CAPWAP_FAILURE_TRC,
                             "CapwapTmrStart:Failed to start the Timer"
                             "Type = %d \r\n", u1TmrType);
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "CapwapTmrStart:Failed to start the Timer"
                              "Type = %d \r\n", u1TmrType));
                return OSIX_FAILURE;
            }
            break;
        case CAPWAP_ECHO_INTERVAL_TMR:
            if (TmrStart (gCapwapTmrList.capwapRunTmrListId,
                          &(pSessEntry->EchoInterval),
                          u1TmrType, u4TmrInterval, 0) == TMR_FAILURE)
            {
                CAPWAP_TRC1 (CAPWAP_FAILURE_TRC,
                             "CapwapTmrStart:Failed to start the Timer"
                             "Type = %d \r\n", u1TmrType);
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "CapwapTmrStart:Failed to start the Timer"
                              "Type = %d \r\n", u1TmrType));
                return OSIX_FAILURE;
            }
            break;
        case CAPWAP_REASSEMBLE_TMR:
        {
            if (TmrStart (gCapwapTmrList.capwapRunTmrListId,
                          &(pSessEntry->ReassemblyTmr),
                          u1TmrType, u4TmrInterval, 0) == TMR_FAILURE)
            {
                CAPWAP_TRC1 (CAPWAP_FAILURE_TRC,
                             "CapwapTmrStart:Failed to start the Timer"
                             "Type = %d \r\n", u1TmrType);
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "CapwapTmrStart:Failed to start the Timer"
                              "Type = %d \r\n", u1TmrType));
                return OSIX_FAILURE;
            }
            break;
        }
        default:
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "CapwapTmrStart:Invalid Timer type FAILED !!!\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "CapwapTmrStart:Invalid Timer type FAILED !!!\r\n"));

            return OSIX_FAILURE;
    }
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;

}

/*****************************************************************************
 *                                                                           *
 * Function     : CapwapGlobalTmrStart                                       *
 *                                                                           *
 * Description  : This function used to start the capwap service task        *
 *                specified timers.                                          *
 *                                                                           *
 * Input        : pTmrBlk - pointer to the Timer block                       *
 *                u4TmrInterval - Time interval for which timer must run     *
 *               (in seconds)                                                *
 *                u1TmrType - Indicates which timer to start.                *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
CapwapGlobalTmrStart (tTmrBlk * pTmrBlk, UINT1 u1TmrType, UINT4 u4TmrInterval)
{

    CAPWAP_FN_ENTRY ();
    switch (u1TmrType)
    {
        case CAPWAP_PMTU_UPDATE_TMR:
        {
            if (TmrStart (gCapwapTmrList.capwapRunTmrListId, pTmrBlk,
                          u1TmrType, u4TmrInterval, 0) == TMR_FAILURE)
            {
                CAPWAP_TRC1 (CAPWAP_FAILURE_TRC,
                             "CapwapGlobalTmrStart:Failed to start"
                             "the Timer Type = %d \r\n", u1TmrType);
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "CapwapGlobalTmrStart:Failed to start"
                              "the Timer Type = %d \r\n", u1TmrType));
                return OSIX_FAILURE;
            }
            break;
        }
        default:
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "CapwapGlobalTmrStart:Invalid Timer"
                        "type FAILED !!!\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "CapwapGlobalTmrStart:Invalid Timer"
                          "type FAILED !!!\r\n"));
            return OSIX_FAILURE;
        }
    }
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function                  : CapwapResetTmr                                *
 *                                                                           *
 * Description               : This routine reset the given timer            *
 *                                                                           *
 * Input                     : u1TmrType - Indicates which timer to reset    *
 *                             pSessEntry - pointer to session entry table   *
 *                                                                           *
 * Output                    : None                                          *
 *                                                                           *
 * Returns                   : None.                                         *
 *                                                                           *
 *****************************************************************************/
INT4
CapwapResetTmr (tRemoteSessionManager * pSessEntry, UINT1 u1TmrType,
                UINT4 u4TmrInterval)
{

    CAPWAP_FN_ENTRY ();
    switch (u1TmrType)
    {
        case CAPWAP_DATACHNL_KEEPALIVE_TMR:
            CapwapTmrStop (pSessEntry, CAPWAP_DATACHNL_KEEPALIVE_TMR);
            CapwapTmrStart (pSessEntry, CAPWAP_DATACHNL_KEEPALIVE_TMR,
                            u4TmrInterval);
            break;
        case CAPWAP_ECHO_INTERVAL_TMR:
            CapwapTmrStop (pSessEntry, CAPWAP_ECHO_INTERVAL_TMR);
            CapwapTmrStart (pSessEntry, CAPWAP_ECHO_INTERVAL_TMR,
                            u4TmrInterval);
            break;
        case CAPWAP_IMAGEDATA_START_TMR:
            CapwapTmrStop (pSessEntry, CAPWAP_IMAGEDATA_START_TMR);
            CapwapTmrStart (pSessEntry, CAPWAP_IMAGEDATA_START_TMR,
                            u4TmrInterval);
            break;
        default:
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "capwapResetTmr:Invalid Timer type FAILED !!!\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "capwapResetTmr:Invalid Timer type FAILED !!!\r\n"));
            return OSIX_FAILURE;
    }
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function                  : CapwapTmrStop                                 *
 *                                                                           *
 * Description               : This routine stops the given discovery timer  *
 *                                                                           *
 * Input                     : u1TmrType - Indicates which timer to stop     *
 *                             pSessEntry - pointer to session entry table   *
 *                                                                           *
 * Output                    : None                                          *
 *                                                                           *
 * Returns                   : None.                                         *
 *                                                                           *
 *****************************************************************************/
INT4
CapwapTmrStop (tRemoteSessionManager * pSessEntry, UINT1 u1TmrType)
{
    UINT4               u4RemainingTime = 0;
    UINT4               u4TmrRetVal = TMR_SUCCESS;
    INT4                i4RetVal = OSIX_FAILURE;

    CAPWAP_FN_ENTRY ();

    switch (u1TmrType)
    {
        case CAPWAP_RETRANSMIT_INTERVAL_TMR:
            u4TmrRetVal = TmrGetRemainingTime (gCapwapTmrList.
                                               capwapRunTmrListId,
                                               &(pSessEntry->RetransIntervalTmr.
                                                 TimerNode), &u4RemainingTime);
            if (u4TmrRetVal != TMR_FAILURE)
            {
                if (TmrStop (gCapwapTmrList.capwapRunTmrListId,
                             &(pSessEntry->RetransIntervalTmr)) != TMR_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "capwapTmrStop:Failure to stop"
                                "MaxRetransmitIntervalr\r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "capwapTmrStop:Failure to stop"
                                  "MaxRetransmitIntervalr\r\n"));
                }
                u4RemainingTime = 0;
                i4RetVal = OSIX_SUCCESS;
            }
            break;
        case CAPWAP_WAITDTLS_TMR:
            u4TmrRetVal = TmrGetRemainingTime (gCapwapTmrList.
                                               capwapFsmTmrListId,
                                               &(pSessEntry->WaitDtlsTmr.
                                                 TimerNode), &u4RemainingTime);
            if (u4TmrRetVal != TMR_FAILURE)
            {
                if (TmrStop (gCapwapTmrList.capwapFsmTmrListId,
                             &(pSessEntry->WaitDtlsTmr)) != TMR_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "capwapTmrStop:Failure to stop"
                                "MaxRetransmitIntervalr\r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "capwapTmrStop:Failure to stop"
                                  "MaxRetransmitIntervalr\r\n"));
                }
                u4RemainingTime = 0;
                i4RetVal = OSIX_SUCCESS;
            }
            break;
        case CAPWAP_WAITJOIN_TMR:
            u4TmrRetVal = TmrGetRemainingTime (gCapwapTmrList.
                                               capwapFsmTmrListId,
                                               &(pSessEntry->WaitJoinTmr.
                                                 TimerNode), &u4RemainingTime);
            if (u4TmrRetVal != TMR_FAILURE)
            {
                if (TmrStop (gCapwapTmrList.capwapFsmTmrListId,
                             &(pSessEntry->WaitJoinTmr)) != TMR_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "capwapTmrStop:Failure to stop"
                                "MaxRetransmitIntervalr\r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "capwapTmrStop:Failure to stop"
                                  "MaxRetransmitIntervalr\r\n"));
                }
                u4RemainingTime = 0;
                i4RetVal = OSIX_SUCCESS;
            }
            break;
        case CAPWAP_IMAGEDATA_START_TMR:
            u4TmrRetVal = TmrGetRemainingTime (gCapwapTmrList.
                                               capwapFsmTmrListId,
                                               &(pSessEntry->ImageDataStartTmr.
                                                 TimerNode), &u4RemainingTime);
            if (u4TmrRetVal != TMR_FAILURE)
            {
                if (TmrStop (gCapwapTmrList.capwapFsmTmrListId,
                             &(pSessEntry->ImageDataStartTmr)) != TMR_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "capwapTmrStop:Failure to stop"
                                "ImageDataStartTmr \r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "capwapTmrStop:Failure to stop"
                                  "ImageDataStartTmr \r\n"));
                }
                u4RemainingTime = 0;
                i4RetVal = OSIX_SUCCESS;
            }
            break;
        case CAPWAP_CPU_RELINQUISH_TMR:
            u4TmrRetVal = TmrGetRemainingTime (gCapwapTmrList.
                                               capwapFsmTmrListId,
                                               &(tmrSerTmrBlk.TimerNode),
                                               &u4RemainingTime);
            if (u4TmrRetVal != TMR_FAILURE)
            {
                if ((TmrStop (gCapwapTmrList.capwapFsmTmrListId, &tmrSerTmrBlk))
                    != TMR_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "capwapTmrStop:Failure to stop"
                                "cpuRelinquishTmr\r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "capwapTmrStop:Failure to stop"
                                  "cpuRelinquishTmr\r\n"));
                }
                u4RemainingTime = 0;
                i4RetVal = OSIX_SUCCESS;
            }
            break;
        case CAPWAP_CPU_RECV_RELINQUISH_TMR:
            u4TmrRetVal = TmrGetRemainingTime (gCapwapTmrList.
                                               capwapFsmTmrListId,
                                               &(tmrRecvTmrBlk.TimerNode),
                                               &u4RemainingTime);
            if (u4TmrRetVal != TMR_FAILURE)
            {
                if ((TmrStop (gCapwapTmrList.capwapFsmTmrListId,
                              &tmrRecvTmrBlk)) != TMR_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "capwapTmrStop:Failure to stop"
                                "cpuRelinquishTmr\r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "capwapTmrStop:Failure to stop"
                                  "cpuRelinquishTmr\r\n"));
                }
                u4RemainingTime = 0;
                i4RetVal = OSIX_SUCCESS;
            }
            break;

        case CAPWAP_CPU_DISC_RELINQUISH_TMR:
            u4TmrRetVal = TmrGetRemainingTime (gCapwapTmrList.
                                               capwapFsmTmrListId,
                                               &(tmrDiscRelTmrBlk.TimerNode),
                                               &u4RemainingTime);
            if (u4TmrRetVal != TMR_FAILURE)
            {
                if ((TmrStop (gCapwapTmrList.capwapFsmTmrListId,
                              &tmrDiscRelTmrBlk)) != TMR_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "capwapTmrStop:Failure to stop"
                                "cpuRelinquishTmr\r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "capwapTmrStop:Failure to stop"
                                  "cpuRelinquishTmr\r\n"));
                }
                u4RemainingTime = 0;
                i4RetVal = OSIX_SUCCESS;
            }
            break;

        case CAPWAP_CHANGESTATE_PENDING_TMR:
            u4TmrRetVal = TmrGetRemainingTime (gCapwapTmrList.
                                               capwapFsmTmrListId,
                                               &(pSessEntry->
                                                 ChangeStatePendingTmr.
                                                 TimerNode), &u4RemainingTime);
            if (u4TmrRetVal != TMR_FAILURE)
            {
                if (TmrStop (gCapwapTmrList.capwapFsmTmrListId,
                             &(pSessEntry->ChangeStatePendingTmr))
                    != TMR_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "capwapTmrStop:Failure to stop"
                                "MaxRetransmitIntervalr\r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "capwapTmrStop:Failure to stop"
                                  "MaxRetransmitIntervalr\r\n"));
                }
                u4RemainingTime = 0;
                i4RetVal = OSIX_SUCCESS;
            }
            break;
        case CAPWAP_DATACHECK_TMR:
            u4TmrRetVal = TmrGetRemainingTime (gCapwapTmrList.
                                               capwapFsmTmrListId,
                                               &(pSessEntry->DataCheckTmr.
                                                 TimerNode), &u4RemainingTime);
            if (u4TmrRetVal != TMR_FAILURE)
            {
                if (TmrStop (gCapwapTmrList.capwapFsmTmrListId,
                             &(pSessEntry->DataCheckTmr)) != TMR_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "capwapTmrStop:Failure to stop"
                                "MaxRetransmitIntervalr\r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "capwapTmrStop:Failure to stop"
                                  "MaxRetransmitIntervalr\r\n"));
                }
                u4RemainingTime = 0;
                i4RetVal = OSIX_SUCCESS;
            }
            break;
        case CAPWAP_DATACHNL_DEADINTERVAL_TMR:
            u4TmrRetVal = TmrGetRemainingTime (gCapwapTmrList.
                                               capwapRunTmrListId,
                                               &(pSessEntry->DataChnlDeadIntTmr.
                                                 TimerNode), &u4RemainingTime);
            if (u4TmrRetVal != TMR_FAILURE)
            {
                if (TmrStop (gCapwapTmrList.capwapRunTmrListId,
                             &(pSessEntry->DataChnlDeadIntTmr)) != TMR_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "capwapTmrStop:Failure to stop"
                                "MaxRetransmitIntervalr\r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "capwapTmrStop:Failure to stop"
                                  "MaxRetransmitIntervalr\r\n"));
                }
                u4RemainingTime = 0;
                i4RetVal = OSIX_SUCCESS;
            }
            break;
        case CAPWAP_DATACHNL_KEEPALIVE_TMR:
            u4TmrRetVal = TmrGetRemainingTime (gCapwapTmrList.
                                               capwapRunTmrListId,
                                               &(pSessEntry->
                                                 DataChnlKeepAliveTmr.
                                                 TimerNode), &u4RemainingTime);
            if (u4TmrRetVal != TMR_FAILURE)
            {
                if (TmrStop (gCapwapTmrList.capwapRunTmrListId,
                             &(pSessEntry->DataChnlKeepAliveTmr))
                    != TMR_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "capwapTmrStop:Failure to stop"
                                "MaxRetransmitIntervalr\r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "capwapTmrStop:Failure to stop"
                                  "MaxRetransmitIntervalr\r\n"));
                }
                u4RemainingTime = 0;
                i4RetVal = OSIX_SUCCESS;
            }

            break;
        case CAPWAP_PRIMARY_DISC_INTERVAL_TMR:
            u4TmrRetVal = TmrGetRemainingTime (gCapwapTmrList.
                                               capwapRunTmrListId,
                                               &(pSessEntry->
                                                 PrimaryDiscIntervalTmr.
                                                 TimerNode), &u4RemainingTime);
            if (u4TmrRetVal != TMR_FAILURE)
            {
                if (TmrStop (gCapwapTmrList.capwapRunTmrListId,
                             &(pSessEntry->PrimaryDiscIntervalTmr))
                    != TMR_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "capwapTmrStop:Failure to stop"
                                "PrimaryDiscIntervalTr\r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "capwapTmrStop:Failure to stop"
                                  "PrimaryDiscIntervalTr\r\n"));
                }
                u4RemainingTime = 0;
                i4RetVal = OSIX_SUCCESS;
            }

            break;
        case CAPWAP_ECHO_INTERVAL_TMR:
            u4TmrRetVal = TmrGetRemainingTime (gCapwapTmrList.
                                               capwapRunTmrListId,
                                               &(pSessEntry->EchoInterval.
                                                 TimerNode), &u4RemainingTime);
            if (u4TmrRetVal != TMR_FAILURE)
            {
                if (TmrStop (gCapwapTmrList.capwapRunTmrListId,
                             &(pSessEntry->EchoInterval)) != TMR_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "capwapTmrStop:Failure to stop"
                                "MaxRetransmitIntervalr\r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "capwapTmrStop:Failure to stop"
                                  "MaxRetransmitIntervalr\r\n"));
                }
                u4RemainingTime = 0;
                i4RetVal = OSIX_SUCCESS;
            }
            break;
        case CAPWAP_REASSEMBLE_TMR:
        {
            u4TmrRetVal = TmrGetRemainingTime (gCapwapTmrList.
                                               capwapRunTmrListId,
                                               &(pSessEntry->ReassemblyTmr.
                                                 TimerNode), &u4RemainingTime);
            if (u4TmrRetVal != TMR_FAILURE)
            {
                if (TmrStop (gCapwapTmrList.capwapRunTmrListId,
                             &(pSessEntry->ReassemblyTmr)) != TMR_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "capwapTmrStop:Failure to stop"
                                "ReAssembly timer \r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "capwapTmrStop:Failure to stop"
                                  "ReAssembly timer \r\n"));
                }
                u4RemainingTime = 0;
                i4RetVal = OSIX_SUCCESS;
            }
            break;
        }
        default:
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "CapwapTmrStop:Invalid Timer type FAILED !!!\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "CapwapTmrStop:Invalid Timer type FAILED !!!\r\n"));
            return OSIX_FAILURE;
    }
    CAPWAP_FN_EXIT ();
    return i4RetVal;
}

VOID
CapwapRetransIntervalTmrExp (VOID *pArg)
{
    tRemoteSessionManager *pSessEntry = NULL;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;

    CAPWAP_FN_ENTRY ();
    pSessEntry = (tRemoteSessionManager *) pArg;

    if (pSessEntry->u1RetransmitCount < CAPWAP_MAX_RETRANSMIT_COUNT)
    {
        if (CapwapTxMessage (pSessEntry->lastTransmittedPkt,
                             pSessEntry, pSessEntry->lastTransmittedPktLen,
                             WSS_CAPWAP_802_11_CTRL_PKT) == OSIX_SUCCESS)
        {
            pSessEntry->u1RetransmitCount++;
            /*Start the Retransmit timer.Timer is doubled every 
             *subsequent time the same Request message is retransmitted*/
            CapwapTmrStart (pSessEntry,
                            CAPWAP_RETRANSMIT_INTERVAL_TMR,
                            (UINT4) (MAX_RETRANSMIT_INTERVAL_TIMEOUT *
                                     (1 << pSessEntry->u1RetransmitCount)));
        }
        else
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Transmit the"
                        "packet \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to Transmit the" "packet \r\n"));
#ifdef WLC_WANTED
            WlchdlrFreeTransmittedPacket (pSessEntry);
#else
            AphdlrFreeTransmittedPacket (pSessEntry);
#endif
            /* Reset the current state */
            CAPWAP_TRC (CAPWAP_FSM_TRC, "Entered CAPWAP DTLSTD State \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Entered CAPWAP DTLSTD State \r\n"));
            pSessEntry->eCurrentState = CAPWAP_DTLSTD;
            if (CapwapShutDownDTLS (pSessEntry) == OSIX_FAILURE)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Disconnect"
                            "the Session \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to Disconnect" "the Session \r\n"));
            }

            WSS_IF_DB_TEMP_MEM_ALLOC_VOID_RETURN (pWssIfCapwapDB)
                MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

            pWssIfCapwapDB->u4DestIp = pSessEntry->remoteIpAddr.u4_addr[0];
            pWssIfCapwapDB->u4DestPort = pSessEntry->u4RemoteCtrlPort;
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_DELETE_RSM,
                                         pWssIfCapwapDB) == OSIX_FAILURE)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Process"
                            "CAPWAP DB Message \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to Process" "CAPWAP DB Message \r\n"));
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return;
            }
            CAPWAP_TRC (CAPWAP_FSM_TRC, "Entered CAPWAP IDLE State \r\n");
            SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                          "Entered CAPWAP IDLE State \r\n"));
            pSessEntry->eCurrentState = CAPWAP_IDLE;
#ifdef WTP_WANTED
            CapwapSendRestartDiscEvent ();
#endif
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        }
    }
    else
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Retransmit count reached maximuxm value.Reset the"
                    "current state \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Retransmit count reached maximuxm value.Reset the"
                      "current state \r\n"));
#ifdef WLC_WANTED
        WlchdlrFreeTransmittedPacket (pSessEntry);
#else
        AphdlrFreeTransmittedPacket (pSessEntry);
#endif
        /* Reset the current state */
        CAPWAP_TRC (CAPWAP_FSM_TRC, "Entered CAPWAP DTLSTD State \r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                      "Entered CAPWAP DTLSTD State \r\n"));
        pSessEntry->eCurrentState = CAPWAP_DTLSTD;
        if (CapwapShutDownDTLS (pSessEntry) == OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Disconnect the"
                        "Session \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to Disconnect the" "Session \r\n"));
        }

        WSS_IF_DB_TEMP_MEM_ALLOC_VOID_RETURN (pWssIfCapwapDB)
            MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
        pWssIfCapwapDB->u4DestIp = pSessEntry->remoteIpAddr.u4_addr[0];
        pWssIfCapwapDB->u4DestPort = pSessEntry->u4RemoteCtrlPort;
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_DELETE_RSM,
                                     pWssIfCapwapDB) == OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Process"
                        "CAPWAP DB Message \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to Process" "CAPWAP DB Message \r\n"));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return;
        }
        CAPWAP_TRC (CAPWAP_FSM_TRC, "Entered CAPWAP IDLE State \r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                      "Entered CAPWAP IDLE State \r\n"));
        pSessEntry->eCurrentState = CAPWAP_IDLE;
#ifdef WTP_WANTED
        CapwapSendRestartDiscEvent ();
#endif
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    }

    CAPWAP_FN_EXIT ();

}

VOID
CapwapFsmTmrExp (VOID *pArg)
{
    tRemoteSessionManager *pSessEntry = NULL;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;

    CAPWAP_FN_ENTRY ();

    WSS_IF_DB_TEMP_MEM_ALLOC_VOID_RETURN (pWssIfCapwapDB)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    pSessEntry = (tRemoteSessionManager *) pArg;
    /* Decleare as peer Dead and Reset the remote session Module */
    CAPWAP_TRC (CAPWAP_FSM_TRC, "Enter the CAPWAP DTLS TD State \r\n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                  "Enter the CAPWAP DTLS TD State \r\n"));
    pSessEntry->eCurrentState = CAPWAP_DTLSTD;
    if (CapwapShutDownDTLS (pSessEntry) == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Disconnect the"
                    "Session \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to Disconnect the" "Session \r\n"));
    }
#ifdef WLC_WANTED
    if (pSessEntry->lastTransmittedPkt != NULL)
    {
        WlchdlrFreeTransmittedPacket (pSessEntry);
    }
#else
    if (pSessEntry->lastTransmittedPkt != NULL)
    {
        AphdlrFreeTransmittedPacket (pSessEntry);
    }
#endif
    pWssIfCapwapDB->u4DestIp = pSessEntry->remoteIpAddr.u4_addr[0];
    pWssIfCapwapDB->u4DestPort = pSessEntry->u4RemoteCtrlPort;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_DELETE_RSM,
                                 pWssIfCapwapDB) == OSIX_FAILURE)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Process CAPWAP"
                    "DB Message \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to Process CAPWAP" "DB Message \r\n"));
        return;
    }
#ifdef WTP_WANTED
    pSessEntry->eCurrentState = CAPWAP_IDLE;
    CapwapSendRestartDiscEvent ();
#endif
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    CAPWAP_FN_EXIT ();
}

VOID
CapwapDataChnlDeadIntTmrExp (VOID *pArg)
{
    tRemoteSessionManager *pSessEntry = NULL;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;

    CAPWAP_FN_ENTRY ();

    WSS_IF_DB_TEMP_MEM_ALLOC_VOID_RETURN (pWssIfCapwapDB)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    pSessEntry = (tRemoteSessionManager *) pArg;
    if (pSessEntry->u1KeepAliveCount >= gu2MaxRetransmitCount)
    {
        pSessEntry->u1KeepAliveCount = 0;
        /* Disconnect the session */
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CAPWAP Data chennal Dead Interval Timer expired, Disconnect"
                    "the Session \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "CAPWAP Data chennal Dead Interval Timer expired, Disconnect"
                      "the Session \r\n"));
        CAPWAP_TRC (CAPWAP_FSM_TRC, "Enter the CAPWAP DTLS TD State \r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                      "Enter the CAPWAP DTLS TD State \r\n"));
        CapwapTmrStop (pSessEntry, CAPWAP_ECHO_INTERVAL_TMR);
        pSessEntry->eCurrentState = CAPWAP_DTLSTD;
        if (CapwapShutDownDTLS (pSessEntry) == OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Disconnect"
                        "the Session \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to Disconnect" "the Session \r\n"));
        }

        CAPWAP_TRC (CAPWAP_FSM_TRC, "Entered CAPWAP IDLE State \r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                      "Entered CAPWAP IDLE State \r\n"));
        pSessEntry->eCurrentState = CAPWAP_IDLE;

        pWssIfCapwapDB->u4DestIp = pSessEntry->remoteIpAddr.u4_addr[0];
        pWssIfCapwapDB->u4DestPort = pSessEntry->u4RemoteCtrlPort;
        /* coverity fix begin */
        /*WssIfProcessCapwapDBMsg (WSS_CAPWAP_DELETE_RSM, pWssIfCapwapDB); */
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_DELETE_RSM,
                                     pWssIfCapwapDB) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "WssIfProcessCapwapDBMsg"
                        "failed \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "WssIfProcessCapwapDBMsg" "failed \r\n"));
        }
        /* coverity fix end */
#ifdef WTP_WANTED
        CapwapClearWtpConfig ();
        pSessEntry->eCurrentState = CAPWAP_IDLE;
        CapwapSendRestartDiscEvent ();
#endif
    }
    else
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Dead Interval Timer expired"
                    "but not going to disconnec the session \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Dead Interval Timer expired"
                      "but not going to disconnec the session \r\n"));
        pSessEntry->u1KeepAliveCount++;
        if (CapwapTransmitKeepAlivePkt (pSessEntry) == OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to transmit the Keep Alive packet \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to transmit the Keep Alive packet \r\n"));
        }
        CapwapTmrStart (pSessEntry, CAPWAP_DATACHNL_DEADINTERVAL_TMR,
                        CAPWAP_DATACHNL_DEADINTERVAL_TIMEOUT);
    }

    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    CAPWAP_FN_EXIT ();
}

VOID
CapwapDataChnlKeepAliveTmrExp (VOID *pArg)
{
    tRemoteSessionManager *pSessEntry = NULL;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;

    CAPWAP_FN_ENTRY ();

    WSS_IF_DB_TEMP_MEM_ALLOC_VOID_RETURN (pWssIfCapwapDB)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    pSessEntry = (tRemoteSessionManager *) pArg;
    if (CapwapTransmitKeepAlivePkt (pSessEntry) == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to transmit the Keep Alive packet \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to transmit the Keep Alive packet \r\n"));
        /* Disconnect the session */
        CAPWAP_TRC (CAPWAP_FSM_TRC, "Enter the CAPWAP DTLS TD State \r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                      "Enter the CAPWAP DTLS TD State \r\n"));

        pSessEntry->eCurrentState = CAPWAP_DTLSTD;
        if (CapwapShutDownDTLS (pSessEntry) == OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Disconnect"
                        "the Session \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to Disconnect" "the Session \r\n"));
        }
        pWssIfCapwapDB->u4DestIp = pSessEntry->remoteIpAddr.u4_addr[0];
        pWssIfCapwapDB->u4DestPort = pSessEntry->u4RemoteCtrlPort;
/* coverity fix begin */
        /*WssIfProcessCapwapDBMsg (WSS_CAPWAP_DELETE_RSM, pWssIfCapwapDB); */
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_DELETE_RSM,
                                     pWssIfCapwapDB) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "WssIfProcessCapwapDBMsg"
                        "failed \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "WssIfProcessCapwapDBMsg" "failed \r\n"));
        }
/* coverity fix end */
#ifdef WTP_WANTED
        pSessEntry->eCurrentState = CAPWAP_IDLE;
        CapwapSendRestartDiscEvent ();
#endif
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return;
    }
    CapwapTmrStart (pSessEntry, CAPWAP_DATACHNL_DEADINTERVAL_TMR,
                    CAPWAP_DATACHNL_DEADINTERVAL_TIMEOUT);
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    CAPWAP_FN_EXIT ();
}

VOID
CapwapReassemblyTmrExp (VOID *pArg)
{

    tCapFragment       *pCapDataFragStream = NULL;
    tCapFragNode       *pFrag = NULL;

    CAPWAP_FN_ENTRY ();
    pCapDataFragStream = (tCapFragment *) pArg;

    printf ("\n REASSEMBLY TIMER EXPIRED AT %d for Fragment ID %d \n",
            (UINT4) OsixGetSysUpTime (), pCapDataFragStream->u2FragmentId);

    /* Free any fragments on list, starting at beginning */
    while ((pFrag = (tCapFragNode *) TMO_DLL_First
            (&(pCapDataFragStream->FragList))))
    {
        if (pFrag->pBuf != NULL)
        {
            CAPWAP_RELEASE_CRU_BUF (pFrag->pBuf);
        }
        /* Should have deleted from reassembly list before freeing it. */
        TMO_DLL_Delete (&(pCapDataFragStream->FragList), &pFrag->Link);
        MemReleaseMemBlock (CAPWAP_FRAG_POOLID, (UINT1 *) pFrag);
    }
    pCapDataFragStream->u1NumberOfFragments = 0;
    CAPWAP_FN_EXIT ();
}

VOID
CapwapPathMtuUpdateTmrExp (VOID *pArg)
{
    UNUSED_PARAM (pArg);
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    INT4                i4Status = OSIX_FAILURE;
    UINT4               u4OldPMTU;
    static UINT2        u2WtpCount = 0;
    tACInfoAfterDiscovery *pAcInfoAfterDisc = NULL;
#ifdef WLC_WANTED
#ifdef NPAPI_WANTED
    tCapwapNpParams     ptCapwapNpParams;
    MEMSET (&ptCapwapNpParams, 0, sizeof (tCapwapNpParams));
#endif
#endif

    CAPWAP_FN_ENTRY ();

    WSS_IF_DB_TEMP_MEM_ALLOC_VOID_RETURN (pWssIfCapwapDB)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    pAcInfoAfterDisc =
        (tACInfoAfterDiscovery *) (VOID *) UtlShMemAllocAcInfoBuf ();
    if (pAcInfoAfterDisc == NULL)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapPathMtuUpdateTmrExp:- "
                    "UtlShMemAllocAcInfoBuf returned failure\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "CapwapPathMtuUpdateTmrExp:- "
                      "UtlShMemAllocAcInfoBuf returned failure\n"));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return;
    }

    MEMSET (pAcInfoAfterDisc, 0, sizeof (tACInfoAfterDiscovery));

    if (u2WtpCount == MAX_WTP_SUPPORTED)
    {
        /* Reset the counter to zero */
        u2WtpCount = 0;
    }
    pWssIfCapwapDB->u2CapwapRSMId = u2WtpCount;
/* coverity fix begin */
    /*WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_RSM_FROM_CAPWAPID,
     * pWssIfCapwapDB);*/
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_RSM_FROM_CAPWAPID,
                                 pWssIfCapwapDB) != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "WssIfProcessCapwapDBMsg Failed  \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "WssIfProcessCapwapDBMsg Failed  \r\n"));
    }
/* coverity fix end */
    if ((pWssIfCapwapDB->pSessEntry != NULL) && (pWssIfCapwapDB->
                                                 pSessEntry->u1Used ==
                                                 OSIX_TRUE))
    {
/* Get the Ip Address of the Best Ac and send the Path MTU Discovery
            * to that AC */
#ifdef WTP_WANTED
        if (CapwapGetDetailsFrmBestAC (pAcInfoAfterDisc) == OSIX_FAILURE)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to Get Perferred AC Details \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to Get Perferred AC Details \r\n"));
            UtlShMemFreeAcInfoBuf ((UINT1 *) pAcInfoAfterDisc);
            return;
        }

        pWssIfCapwapDB->pSessEntry->remoteIpAddr.u4_addr[0] = pAcInfoAfterDisc->
            incomingAddr.u4_addr[0];
        pWssIfCapwapDB->pSessEntry->u4RemoteCtrlPort =
            pAcInfoAfterDisc->u4DestPort;
#endif
        /* Path MTU Discovery is valid only in RUN State */
        if (pWssIfCapwapDB->pSessEntry->eCurrentState == CAPWAP_RUN)
        {
            u4OldPMTU = pWssIfCapwapDB->pSessEntry->PMTU;
            i4Status = CapwapUpdatePathMtu (pWssIfCapwapDB->
                                            pSessEntry->remoteIpAddr.u4_addr[0],
                                            pWssIfCapwapDB->pSessEntry->
                                            u4RemoteCtrlPort,
                                            &(pWssIfCapwapDB->pSessEntry->
                                              PMTU));

            if ((i4Status == OSIX_SUCCESS) && (u4OldPMTU !=
                                               pWssIfCapwapDB->pSessEntry->
                                               PMTU))
            {
                CAPWAP_TRC2 (CAPWAP_MGMT_TRC, "The Current Path MTU changed"
                             "from %d to the new value: %d \r\n",
                             u4OldPMTU, pWssIfCapwapDB->pSessEntry->PMTU);
                SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4CapwapSysLogId,
                              "The Current Path MTU changed"
                              "from %d to the new value: %d \r\n", u4OldPMTU,
                              pWssIfCapwapDB->pSessEntry->PMTU));
                /* Call the NPAPI call to update the PMTU */

#ifdef WLC_WANTED
#ifdef NPAPI_WANTED
                ptCapwapNpParams.u4PMTU = pWssIfCapwapDB->pSessEntry->PMTU;
                capwapNpUpdateTunnelPmtu (&ptCapwapNpParams);
#endif
#endif
                /* update the DTLS Module */
            }
        }
    }
#ifdef WLC_WANTED
    u2WtpCount++;
#endif
    /* Start the timer to update the PMTU for next WTP connected */
    CapwapGlobalTmrStart (&(gSerTaskGlobals.PMTUUpdateTmr),
                          CAPWAP_PMTU_UPDATE_TMR, CAPWAP_PMTU_UPDATE_TIMEOUT);
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    UtlShMemFreeAcInfoBuf ((UINT1 *) pAcInfoAfterDisc);
    CAPWAP_FN_EXIT ();
}

VOID
CapwapImageDataStartTmrExp (VOID *pArg)
{
#ifdef WTP_WANTED
    tRemoteSessionManager *pSessEntry = NULL;
    CHR1                ac1Command[CAPWAP_IMAGE_BUFFER_SIZE];
    CAPWAP_FN_ENTRY ();

    MEMSET (&ac1Command, 0, CAPWAP_IMAGE_BUFFER_SIZE);
    pSessEntry = (tRemoteSessionManager *) pArg;
    CAPWAP_TRC (CAPWAP_FSM_TRC, "Enter Reset State \r\n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                  "Enter Reset State \r\n"));

    if (pSessEntry->eCurrentState == CAPWAP_IMAGEDATA)
    {
#ifndef DAK_WANTED
        ApHdlrEnterReset ();
#endif
    }
    else
    {
        SNPRINTF (ac1Command, sizeof (ac1Command), "%s %s%s",
                  IMAGE_DATA_REMOVE_CMD, IMAGE_DATA_FLASH_CONF_LOC,
                  pSessEntry->ImageId.data);
        system (ac1Command);
    }
#else
    UNUSED_PARAM (pArg);
#endif

    CAPWAP_FN_EXIT ();
}

/* Dummy function for WTP compilation */
#ifdef WTP_WANTED
VOID
CapwapDiscCPURelinquishTmrExp (VOID *pArg)
{
    UNUSED_PARAM (pArg);
    return;
}
#endif

VOID
CapwapStopKeepAlive (tRemoteSessionManager * pSess)
{
    if (pSess != NULL)
    {
        pSess->u1KeepAliveCount = 0;
    }
    return;
}

INT4
CapwapStopAllSessionTimers (tRemoteSessionManager * pSessEntry)
{
    UINT4               u4RemainingTime = 0;
    UINT4               u4TmrRetVal = TMR_SUCCESS;
    INT4                i4RetVal = OSIX_FAILURE;

    CAPWAP_FN_ENTRY ();
    CAPWAP_TRC1 (CAPWAP_FSM_TRC, "To Stop All the Session(%d) Timers\n\r",
                 pSessEntry->sessionId);
    SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4CapwapSysLogId,
                  "To Stop All the Session(%d) Timers\n\r",
                  pSessEntry->sessionId));

    /* RUN state timers first */
    /* CAPWAP_RETRANSMIT_INTERVAL_TMR */

    u4TmrRetVal = TmrGetRemainingTime (gCapwapTmrList.capwapRunTmrListId,
                                       &(pSessEntry->RetransIntervalTmr.
                                         TimerNode), &u4RemainingTime);
    if (u4TmrRetVal != TMR_FAILURE)
    {
        if (TmrStop (gCapwapTmrList.capwapRunTmrListId,
                     &(pSessEntry->RetransIntervalTmr)) != TMR_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "capwapTmrStop:Failure to stop RetransIntervalTmr\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "capwapTmrStop:Failure to stop RetransIntervalTmr\r\n"));
        }
        i4RetVal = OSIX_SUCCESS;
    }

    /* CAPWAP_DATACHNL_KEEPALIVE_TMR */
    u4TmrRetVal = TmrGetRemainingTime (gCapwapTmrList.capwapRunTmrListId,
                                       &(pSessEntry->DataChnlKeepAliveTmr.
                                         TimerNode), &u4RemainingTime);
    if (u4TmrRetVal != TMR_FAILURE)
    {
        if (TmrStop
            (gCapwapTmrList.capwapRunTmrListId,
             &(pSessEntry->DataChnlKeepAliveTmr)) != TMR_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "capwapTmrStop:Failure to stop DataChnlKeepAliveTmr\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "capwapTmrStop:Failure to stop DataChnlKeepAliveTmr\r\n"));
        }
        i4RetVal = OSIX_SUCCESS;
    }

    /* CAPWAP_DATACHNL_DEADINTERVAL_TMR */
    u4TmrRetVal = TmrGetRemainingTime (gCapwapTmrList.capwapRunTmrListId,
                                       &(pSessEntry->DataChnlDeadIntTmr.
                                         TimerNode), &u4RemainingTime);
    if (u4TmrRetVal != TMR_FAILURE)
    {
        if (TmrStop
            (gCapwapTmrList.capwapRunTmrListId,
             &(pSessEntry->DataChnlDeadIntTmr)) != TMR_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "capwapTmrStop:Failure to stop DataChnlDeadIntTmr\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "capwapTmrStop:Failure to stop DataChnlDeadIntTmr\r\n"));
        }
        i4RetVal = OSIX_SUCCESS;
    }

    /* CAPWAP_ECHO_INTERVAL_TMR */
    u4TmrRetVal = TmrGetRemainingTime (gCapwapTmrList.capwapRunTmrListId,
                                       &(pSessEntry->EchoInterval.TimerNode),
                                       &u4RemainingTime);
    if (u4TmrRetVal != TMR_FAILURE)
    {
        if (TmrStop (gCapwapTmrList.capwapRunTmrListId,
                     &(pSessEntry->EchoInterval)) != TMR_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "capwapTmrStop:Failure to stop EchoInterval\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "capwapTmrStop:Failure to stop EchoInterval\r\n"));
        }
        i4RetVal = OSIX_SUCCESS;
    }

    /* FSM - State Machine timers */
    /* CAPWAP_WAITDTLS_TMR */
    u4TmrRetVal = TmrGetRemainingTime (gCapwapTmrList.capwapFsmTmrListId,
                                       &(pSessEntry->WaitDtlsTmr.TimerNode),
                                       &u4RemainingTime);
    if (u4TmrRetVal != TMR_FAILURE)
    {
        if (TmrStop (gCapwapTmrList.capwapFsmTmrListId,
                     &(pSessEntry->WaitDtlsTmr)) != TMR_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "capwapTmrStop:Failure to stop WaitDtlsTmr\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "capwapTmrStop:Failure to stop WaitDtlsTmr\r\n"));
        }
        i4RetVal = OSIX_SUCCESS;
    }

    /* CAPWAP_WAITJOIN_TMR */
    u4TmrRetVal = TmrGetRemainingTime (gCapwapTmrList.capwapFsmTmrListId,
                                       &(pSessEntry->WaitJoinTmr.TimerNode),
                                       &u4RemainingTime);
    if (u4TmrRetVal != TMR_FAILURE)
    {
        if (TmrStop (gCapwapTmrList.capwapFsmTmrListId,
                     &(pSessEntry->WaitJoinTmr)) != TMR_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "capwapTmrStop:Failure to stop WaitJoinTmr\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "capwapTmrStop:Failure to stop WaitJoinTmr\r\n"));
        }
        i4RetVal = OSIX_SUCCESS;
    }

    /* CAPWAP_CHANGESTATE_PENDING_TMR */
    u4TmrRetVal = TmrGetRemainingTime (gCapwapTmrList.capwapFsmTmrListId,
                                       &(pSessEntry->ChangeStatePendingTmr.
                                         TimerNode), &u4RemainingTime);
    if (u4TmrRetVal != TMR_FAILURE)
    {
        if (TmrStop (gCapwapTmrList.capwapFsmTmrListId,
                     &(pSessEntry->ChangeStatePendingTmr)) != TMR_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "capwapTmrStop:Failure to stop ChangeStatePendingTmr\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "capwapTmrStop:Failure to stop ChangeStatePendingTmr\r\n"));
        }
        i4RetVal = OSIX_SUCCESS;
    }

    /* CAPWAP_DATACHECK_TMR */
    u4TmrRetVal = TmrGetRemainingTime (gCapwapTmrList.capwapFsmTmrListId,
                                       &(pSessEntry->DataCheckTmr.TimerNode),
                                       &u4RemainingTime);
    if (u4TmrRetVal != TMR_FAILURE)
    {
        if (TmrStop (gCapwapTmrList.capwapFsmTmrListId,
                     &(pSessEntry->DataCheckTmr)) != TMR_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "capwapTmrStop:Failure to stop DataCheckTmr\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "capwapTmrStop:Failure to stop DataCheckTmr\r\n"));
        }
        i4RetVal = OSIX_SUCCESS;
    }

    /*   CAPWAP_REASSEMBLE_TMR */
    u4TmrRetVal = TmrGetRemainingTime (gCapwapTmrList.capwapRunTmrListId,
                                       &(pSessEntry->ReassemblyTmr.TimerNode),
                                       &u4RemainingTime);
    if (u4TmrRetVal != TMR_FAILURE)
    {
        if (TmrStop
            (gCapwapTmrList.capwapRunTmrListId,
             &(pSessEntry->ReassemblyTmr)) != TMR_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "capwapTmrStop:Failure to stop ReassemblyTmr\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "capwapTmrStop:Failure to stop ReassemblyTmr\r\n"));
        }
        i4RetVal = OSIX_SUCCESS;
    }

    /* CAPWAP_IMAGEDATA_START_TMR */
    u4TmrRetVal = TmrGetRemainingTime (gCapwapTmrList.capwapFsmTmrListId,
                                       &(pSessEntry->ImageDataStartTmr.
                                         TimerNode), &u4RemainingTime);
    if (u4TmrRetVal != TMR_FAILURE)
    {
        if (TmrStop
            (gCapwapTmrList.capwapFsmTmrListId,
             &(pSessEntry->ImageDataStartTmr)) != TMR_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "capwapTmrStop:Failure to stop ImageDataStartTmr \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "capwapTmrStop:Failure to stop ImageDataStartTmr \r\n"));
        }
        i4RetVal = OSIX_SUCCESS;
    }

    CAPWAP_TRC1 (CAPWAP_FSM_TRC, "All Session(%d) Timers Stopped\r\n",
                 pSessEntry->sessionId);
    SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4CapwapSysLogId,
                  "All Session(%d) Timers Stopped\r\n", pSessEntry->sessionId));

    CAPWAP_FN_EXIT ();
    return i4RetVal;
}

INT4
CapwapStopAllGlobalTimers (VOID)
{
    UINT4               u4RemainingTime = 0;
    UINT4               u4TmrRetVal = TMR_SUCCESS;
    INT4                i4RetVal = OSIX_FAILURE;

    CAPWAP_FN_ENTRY ();
    CAPWAP_TRC (CAPWAP_FSM_TRC, "To Stop All the Global Timers\n\r");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                  "To Stop All the Global Timers\n\r"));

    /* Global timers - for FSM & RUN & DISC State */
    /* CAPWAP_PMTU_UPDATE_TMR */
    u4TmrRetVal = TmrGetRemainingTime (gCapwapTmrList.capwapRunTmrListId,
                                       &(gSerTaskGlobals.PMTUUpdateTmr.
                                         TimerNode), &u4RemainingTime);
    if (u4TmrRetVal != TMR_FAILURE)
    {
        if (TmrStop
            (gCapwapTmrList.capwapRunTmrListId,
             &(gSerTaskGlobals.PMTUUpdateTmr)) != TMR_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "capwapTmrStop:Failure to stop PMTUUpdateTmr\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "capwapTmrStop:Failure to stop PMTUUpdateTmr\r\n"));
        }
        i4RetVal = OSIX_SUCCESS;
    }

    /* CAPWAP_CPU_RELINQUISH_TMR */
    u4TmrRetVal = TmrGetRemainingTime (gCapwapTmrList.capwapFsmTmrListId,
                                       &(tmrSerTmrBlk.TimerNode),
                                       &u4RemainingTime);
    if (u4TmrRetVal != TMR_FAILURE)
    {
        if ((TmrStop (gCapwapTmrList.capwapFsmTmrListId, &tmrSerTmrBlk))
            != TMR_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "capwapTmrStop:Failure to stop cpuRelinquishTmr\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "capwapTmrStop:Failure to stop cpuRelinquishTmr\r\n"));
        }
        i4RetVal = OSIX_SUCCESS;
    }

    /* CAPWAP_CPU_RECV_RELINQUISH_TMR */
    u4TmrRetVal = TmrGetRemainingTime (gCapwapTmrList.capwapFsmTmrListId,
                                       &(tmrRecvTmrBlk.TimerNode),
                                       &u4RemainingTime);
    if (u4TmrRetVal != TMR_FAILURE)
    {
        if ((TmrStop (gCapwapTmrList.capwapFsmTmrListId, &tmrRecvTmrBlk))
            != TMR_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "capwapTmrStop:Failure to stop cpuRelinquishTmr\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "capwapTmrStop:Failure to stop cpuRelinquishTmr\r\n"));
        }
        i4RetVal = OSIX_SUCCESS;
    }

    /* CAPWAP_CPU_DISC_RELINQUISH_TMR */
    u4TmrRetVal = TmrGetRemainingTime (gCapwapTmrList.capwapFsmTmrListId,
                                       &(tmrDiscRelTmrBlk.TimerNode),
                                       &u4RemainingTime);
    if (u4TmrRetVal != TMR_FAILURE)
    {
        if ((TmrStop (gCapwapTmrList.capwapFsmTmrListId, &tmrDiscRelTmrBlk))
            != TMR_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "capwapTmrStop:Failure to stop cpuRelinquishTmr\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "capwapTmrStop:Failure to stop cpuRelinquishTmr\r\n"));
        }
        i4RetVal = OSIX_SUCCESS;
    }

    CAPWAP_TRC (CAPWAP_FSM_TRC, "All Global Timers Stopped\n\r");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                  "All Global Timers Stopped\n\r"));
    CAPWAP_FN_EXIT ();
    return i4RetVal;
}

INT4
CapwapStartAllGlobalTimers (VOID)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    CAPWAP_FN_ENTRY ();
    printf ("\nTo Start All the Global Timers\n");

    /* Global timers - for FSM & RUN & DISC State */
    /* CAPWAP_PMTU_UPDATE_TMR */
    CapwapGlobalTmrStart (&(gSerTaskGlobals.PMTUUpdateTmr),
                          CAPWAP_PMTU_UPDATE_TMR, CAPWAP_PMTU_UPDATE_TIMEOUT);

    /* CAPWAP_CPU_RELINQUISH_TMR */
    if (CapwapTmrStart
        (NULL, CAPWAP_CPU_RELINQUISH_TMR,
         CAPWAP_RELINQUISH_TIMER_VAL) != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CAPWAP_CPU_RELINQUISH_TMR Timer Start FAILED.\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "CAPWAP_CPU_RELINQUISH_TMR Timer Start FAILED.\r\n"));
        i4RetVal = OSIX_FAILURE;
    }

    /* CAPWAP_CPU_RECV_RELINQUISH_TMR */
    if (CapwapTmrStart
        (NULL, CAPWAP_CPU_RECV_RELINQUISH_TMR,
         CAPWAP_RELINQUISH_TIMER_VAL) != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CAPWAP_CPU_RECV_RELINQUISH_TMR Timer Start FAILED.\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "CAPWAP_CPU_RECV_RELINQUISH_TMR Timer Start FAILED.\r\n"));
        i4RetVal = OSIX_FAILURE;
    }

    /* CAPWAP_CPU_DISC_RELINQUISH_TMR */
    /* initialize counter to zero */
    if (CapwapTmrStart
        (NULL, CAPWAP_CPU_DISC_RELINQUISH_TMR,
         CAPWAP_RELINQUISH_TIMER_VAL) != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    " CAPWAP_CPU_DISC_RELINQUISH_TMR Timer Start FAILED.\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      " CAPWAP_CPU_DISC_RELINQUISH_TMR Timer Start FAILED.\r\n"));
        i4RetVal = OSIX_FAILURE;
    }

    printf ("\nAll Global Timers Started\n");
    CAPWAP_FN_EXIT ();
    return i4RetVal;
}

#endif /* __CAPWAPTMR_C__ */
 /*-----------------------------------------------------------------------*/
 /*                       End of the file  capwaptmr.c                    */
 /*-----------------------------------------------------------------------*/
