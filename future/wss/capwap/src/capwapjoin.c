/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 * $Id: capwapjoin.c,v 1.3 2017/11/24 10:37:03 siva Exp $       
 * Description: This file contains the CAPWAP Join state routines  
 *
 *******************************************************************/
#ifndef __CAPWAP_JOIN_C__
#define __CAPWAP_JOIN_C__
#define _CAPWAP_GLOBAL_VAR

#include "capwapinc.h"
#include "aphdlr.h"
#include "aphdlrprot.h"
#include "wssifpmdb.h"

extern eState       gCapwapState;
extern INT4         WlchdlrFreeTransmittedPacket (tRemoteSessionManager *
                                                  pSessEntry);
/************************************************************************/
/*  Function Name   : CapwapProcessJoinRequest                          */
/*  Description     : The function processes the received CAPWAP        */
/*                    join request                                      */
/*  Input(s)        : pJoinReqMsg - CAPWAP Join request packet received */
/*                    pSessEntry  - Session Db Entry                    */
/*                    pBuf        - Received queue msg from rx thread   */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
CapwapProcessJoinRequest (tSegment * pBuf,
                          tCapwapControlPacket * pJoinReqMsg,
                          tRemoteSessionManager * pSessEntry)
{
    INT4                i4Status = 1;
    UINT4               u4MsgLen = 0;
    tJoinRsp           *pJoinResponse = NULL;
    tSegment           *pTxBuf = NULL;
    UINT1              *pu1TxBuf = NULL, *pRcvdBuf = NULL;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT1               u1IsNotLinear = OSIX_FALSE;
    UINT2               u2PacketLen;
    tWssIfPMDB          WssIfPMDB;
    UINT1              *pu1TempBuf = NULL;
    UINT1               u1FsWlcDiscoveryMode = 0;

    CAPWAP_FN_ENTRY ();
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    if (pSessEntry == NULL || (pSessEntry->eCurrentState != CAPWAP_DTLS))
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Session Entry is"
                    "NULL, Return Failure \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId, "Session Entry is"
                      "NULL, Return Failure \r\n"));
        return OSIX_FAILURE;
    }
    if (CapwapTmrStop (pSessEntry, CAPWAP_WAITDTLS_TMR) == OSIX_FAILURE)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FAILURE, "Failed to stop the Wait DTLS timer \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to stop the Wait DTLS timer \r\n"));
        return OSIX_FAILURE;
    }
    pJoinResponse = (tJoinRsp *) (VOID *) UtlShMemAllocJoinRspBuf ();
    if (pJoinResponse == NULL)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapProcessJoinRequest:- "
                    "UtlShMemAllocJoinRspBuf returned failure\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "CapwapProcessJoinRequest:- "
                      "UtlShMemAllocJoinRspBuf returned failure\n"));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    MEMSET (pJoinResponse, 0, sizeof (tJoinRsp));

    u2PacketLen = (UINT2) CAPWAP_GET_BUF_LEN (pBuf);
    pRcvdBuf = CAPWAP_BUF_IF_LINEAR (pBuf, 0, u2PacketLen);
    if (pRcvdBuf == NULL)
    {
        pRcvdBuf = (UINT1 *) MemAllocMemBlk (CAPWAP_PKTBUF_POOLID);
        CAPWAP_COPY_FROM_BUF (pBuf, pRcvdBuf, 0, u2PacketLen);
        u1IsNotLinear = OSIX_TRUE;
    }

    if (CapwapValidateIpAddress (pSessEntry->remoteIpAddr.u4_addr[0]) ==
        OSIX_FAILURE)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "WTP is not suppported. Entry is in the black list \n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "WTP is not suppported. Entry is in the black list \n"));
        UtlShMemFreeJoinRspBuf ((UINT1 *) pJoinResponse);
        if (u1IsNotLinear == OSIX_TRUE)
        {
            MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pRcvdBuf);
        }
        return OSIX_FAILURE;    /* return proper error code */
    }
    CapwapGetFsWlcDiscoveryMode (&u1FsWlcDiscoveryMode);
    if (u1FsWlcDiscoveryMode != CAPWAP_DISC_MODE_AUTO)
    {
        /* Check Capwap DB whether AP's MAC is in blacklist */
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
            pJoinReqMsg->u2IntProfileId;
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpMacAddress = OSIX_TRUE;
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                     pWssIfCapwapDB) == OSIX_SUCCESS)
        {
            MEMCPY (pWssIfCapwapDB->BaseMacAddress,
                    pWssIfCapwapDB->CapwapGetDB.WtpMacAddress, MAC_ADDR_LEN);
            if (u1FsWlcDiscoveryMode == CAPWAP_DISC_MODE_MAC_BLACKLIST)
            {
                if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_BLACKLIST_ENTRY,
                                             pWssIfCapwapDB) == OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "AP Mac Address is under black list;\
                                Cannot provide service\r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "AP Mac Address is under black list;\
                                Cannot provide service\r\n"));
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    UtlShMemFreeJoinRspBuf ((UINT1 *) pJoinResponse);
                    if (u1IsNotLinear == OSIX_TRUE)
                    {
                        MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pRcvdBuf);
                    }
                    return OSIX_FAILURE;
                }
            }
            else if (u1FsWlcDiscoveryMode == CAPWAP_DISC_MODE_MAC_WHITELIST)
            {
                if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_WHITELIST_ENTRY,
                                             pWssIfCapwapDB) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "AP Mac Address is not under white list;\
                                Cannot provide service\r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "AP Mac Address is not under white list;\
                                Cannot provide service\r\n"));
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    UtlShMemFreeJoinRspBuf ((UINT1 *) pJoinResponse);
                    if (u1IsNotLinear == OSIX_TRUE)
                    {
                        MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pRcvdBuf);
                    }
                    return OSIX_FAILURE;
                }

            }
        }
        else
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "AP Mac Address is not present in DB;\
                        Cannot provide service\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "AP Mac Address is not present in DB;\
                        Cannot provide service\r\n"));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            UtlShMemFreeJoinRspBuf ((UINT1 *) pJoinResponse);
            if (u1IsNotLinear == OSIX_TRUE)
            {
                MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pRcvdBuf);
            }
            return OSIX_FAILURE;
        }

    }
    /* Validate the Join request message. if it is success
       construct the Join response */
    if ((CapwapValidateJoinRequestMsgElems (pRcvdBuf, pJoinReqMsg, pSessEntry)
         == OSIX_SUCCESS))
    {
        if (u1IsNotLinear == OSIX_TRUE)
        {
            MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pRcvdBuf);
        }
        CAPWAP_TRC (CAPWAP_FSM_TRC, "Entered CAPWAP JOIN State \r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                      "Entered CAPWAP JOIN State \r\n"));
        pSessEntry->eCurrentState = CAPWAP_JOIN;

        CapwapUpdateResultCode (&(pJoinResponse->resultCode),
                                pJoinReqMsg->u4ReturnCode, &u4MsgLen);

        MEMSET (&WssIfPMDB, 0, sizeof (WssIfPMDB));
        WssIfPMDB.u2ProfileId = pJoinReqMsg->u2IntProfileId;
        WssIfPMDB.tWtpPmAttState.wssPmWLCStatsSetDB.bWssPmWLCCapwapJoinReqRxd =
            OSIX_TRUE;
        if (WssIfProcessPMDBMsg (WSS_PM_WLC_CAPWAP_STATS_SET, &WssIfPMDB) !=
            OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to update Join Req Stats toi PM \n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to update Join Req Stats toi PM \n"));
        }

        /* Get the values to send out the Join response */
        if (CapwapGetJoinRspMsgElements (pJoinResponse, &u4MsgLen, pSessEntry,
                                         pJoinReqMsg->u2IntProfileId) ==
            OSIX_SUCCESS)
        {
            /* update the seq number */
            pJoinResponse->u1SeqNum = pJoinReqMsg->capwapCtrlHdr.u1SeqNum;
            pJoinResponse->u2CapwapMsgElemenLen = (UINT2) (u4MsgLen +
                                                           CAPWAP_CMN_MSG_ELM_HEAD_LEN);
            pTxBuf = CAPWAP_ALLOCATE_CRU_BUF (CAPWAP_MAX_PKT_LEN, 0);
            if (pTxBuf == NULL)
            {
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "capwapProcessJoinRequest:Memory"
                            "Allocation Failed \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "capwapProcessJoinRequest:Memory"
                              "Allocation Failed \r\n"));
                UtlShMemFreeJoinRspBuf ((UINT1 *) pJoinResponse);
                return OSIX_FAILURE;
            }
            pu1TxBuf = CAPWAP_BUF_IF_LINEAR (pTxBuf, 0, CAPWAP_MAX_PKT_LEN);
            if (pu1TxBuf == NULL)
            {
                pu1TxBuf = (UINT1 *) MemAllocMemBlk (CAPWAP_PKTBUF_POOLID);
                /* Kloc Fix Starts */
                if (pu1TxBuf == NULL)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "capwapProcessJoinRequest:Memory Allocction"
                                "Failed \r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "capwapProcessJoinRequest:Memory Allocction"
                                  "Failed \r\n"));
                    CAPWAP_RELEASE_CRU_BUF (pTxBuf);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    UtlShMemFreeJoinRspBuf ((UINT1 *) pJoinResponse);
                    return OSIX_FAILURE;
                }
                /* Kloc Fix Ends */
                CAPWAP_COPY_FROM_BUF (pTxBuf, pu1TxBuf, 0, CAPWAP_MAX_PKT_LEN);
                u1IsNotLinear = OSIX_TRUE;
            }
            if ((CapwapAssembleJoinResponse
                 (pJoinResponse->u2CapwapMsgElemenLen, pJoinResponse,
                  pu1TxBuf)) == OSIX_FAILURE)
            {
                if (u1IsNotLinear == OSIX_TRUE)
                {
                    MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pu1TxBuf);
                }
                CAPWAP_RELEASE_CRU_BUF (pTxBuf);
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                UtlShMemFreeJoinRspBuf ((UINT1 *) pJoinResponse);
                return OSIX_FAILURE;
            }
            pu1TempBuf = pu1TxBuf + CAPWAP_MSG_OFFSET;
            CAPWAP_GET_2BYTE (pJoinResponse->u2CapwapMsgElemenLen, pu1TempBuf);
            if (u1IsNotLinear == OSIX_TRUE)
            {
                /* If the CRU buffer memory is not linear */
                CAPWAP_COPY_TO_BUF (pTxBuf, pu1TxBuf, 0, CAPWAP_MAX_PKT_LEN);
                MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pu1TxBuf);
            }
            i4Status = CapwapTxMessage (pTxBuf,
                                        pSessEntry,
                                        (UINT4) (pJoinResponse->
                                                 u2CapwapMsgElemenLen +
                                                 CAPWAP_SOCK_HDR_LEN),
                                        WSS_CAPWAP_802_11_CTRL_PKT);
            if (i4Status == OSIX_SUCCESS)
            {
                /* Update the last transmitted sequence number */
                pSessEntry->lastProcesseddSeqNum = pJoinResponse->u1SeqNum;
                /* store the packet buffer pointer in
                 * the remote session DB copy the pointer */
                pSessEntry->lastTransmittedPkt = pTxBuf;
                pSessEntry->lastTransmittedPktLen =
                    (UINT4) (pJoinResponse->u2CapwapMsgElemenLen +
                             CAPWAP_MSG_ELE_LEN);
                pSessEntry->lastTransmitedSeqNum = pJoinResponse->u1SeqNum;
                /* Start Wait Join Timer */
                CapwapTmrStart (pSessEntry, CAPWAP_WAITJOIN_TMR,
                                CAPWAP_WAITJOIN_TIMEOUT);
                MEMSET (&WssIfPMDB, 0, sizeof (WssIfPMDB));
                WssIfPMDB.u2ProfileId = pJoinReqMsg->u2IntProfileId;
                WssIfPMDB.tWtpPmAttState.wssPmWLCStatsSetDB.
                    bWssPmWLCCapwapJoinRespSent = OSIX_TRUE;
                WssIfPMDB.tWtpPmAttState.wssPmWLCStatsSetDB.
                    bWssPmWLCCapwapJoinLastSuccAttTime = OSIX_TRUE;
                if (WssIfProcessPMDBMsg (WSS_PM_WLC_CAPWAP_STATS_SET,
                                         &WssIfPMDB) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "Failed to update Join Req Stats to PM \n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Failed to update Join Req Stats to PM \n"));
                }
            }
            else
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Failed to Transmit the packet \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to Transmit the packet \r\n"));
                CAPWAP_TRC (CAPWAP_FSM_TRC, "Enter the CAPWAP DTLS"
                            "TD State \r\n");
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                              "Enter the CAPWAP DTLS" "TD State \r\n"));
                pSessEntry->eCurrentState = CAPWAP_DTLSTD;
                if (CapwapShutDownDTLS (pSessEntry) == OSIX_FAILURE)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Disconnect"
                                "the Session \r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Failed to Disconnect" "the Session \r\n"));
                }
                /* Cleare the Entry in Remote session Module */
                pWssIfCapwapDB->u4DestIp = pSessEntry->remoteIpAddr.u4_addr[0];
                pWssIfCapwapDB->u4DestPort = pSessEntry->u4RemoteCtrlPort;
                if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_DELETE_RSM,
                                             pWssIfCapwapDB) == OSIX_FAILURE)
                {
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Delete"
                                "the RSM from CAPWAP DB \r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Failed to Delete"
                                  "the RSM from CAPWAP DB \r\n"));
                    UtlShMemFreeJoinRspBuf ((UINT1 *) pJoinResponse);
                    return OSIX_FAILURE;
                }
                CAPWAP_RELEASE_CRU_BUF (pTxBuf);
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                UtlShMemFreeJoinRspBuf ((UINT1 *) pJoinResponse);
                return OSIX_FAILURE;
            }
        }
    }
    else
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to validate the received Join Req\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to validate the received Join Req\n"));
        CAPWAP_TRC (CAPWAP_FSM_TRC, "Enter the CAPWAP DTLS TD State \r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                      "Enter the CAPWAP DTLS TD State \r\n"));
        pSessEntry->eCurrentState = CAPWAP_DTLSTD;
        if (CapwapShutDownDTLS (pSessEntry) == OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Disconnect"
                        "the Session \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to Disconnect" "the Session \r\n"));
        }
        /* Cleare the Entry in Remote session Module */
        pWssIfCapwapDB->u4DestIp = pSessEntry->remoteIpAddr.u4_addr[0];
        pWssIfCapwapDB->u4DestPort = pSessEntry->u4RemoteCtrlPort;
        /* Coverity Fix - 20/Jun/2013 */
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_DELETE_RSM,
                                     pWssIfCapwapDB) == OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, " WssIfProcessCapwapDBMsg"
                        "Failed.  \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          " WssIfProcessCapwapDBMsg" "Failed.  \r\n"));
        }
        /* Coverity Fix - 20/Jun/2013 */
        if (u1IsNotLinear == OSIX_TRUE)
        {
            MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pRcvdBuf);
        }
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        UtlShMemFreeJoinRspBuf ((UINT1 *) pJoinResponse);
        return OSIX_FAILURE;
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    UtlShMemFreeJoinRspBuf ((UINT1 *) pJoinResponse);
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapGetJoinRspMsgElements                  */
/*  Description     : The function validates the functional             */
/*                    characteristics of the received CAPWAP            */
/*                    Join response message against the configured      */
/*                    WTP profile                                       */
/*  Input(s)        : None                                              */
/*  Output(s)       :  pJoinResponse - filled structure                 */
/*  Returns         : None                                              */
/************************************************************************/
INT4
CapwapGetJoinRspMsgElements (tJoinRsp * pJoinResponse, UINT4 *pMsgLen,
                             tRemoteSessionManager * pSessEntry,
                             UINT2 u2InternalId)
{
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT2               u2ProfileId = 0;

    CAPWAP_FN_ENTRY ();
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    if (CapwapConstructCpHeader (&pJoinResponse->capwapHdr) == OSIX_FAILURE)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "capwapGetJoinRspMsgElements:Failed to construct"
                    "the capwap Header\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "capwapGetJoinRspMsgElements:Failed to construct"
                      "the capwap Header\n"));
        return OSIX_FAILURE;
    }
    /* update Mandatory Message Elements */
    if ((CapwapGetACDescriptor (&pJoinResponse->acDesc, pMsgLen) ==
         OSIX_FAILURE) ||
        (CapwapGetACName (&pJoinResponse->acName, pMsgLen) ==
         OSIX_FAILURE) ||
        (CapwapGetEcnSupport (&pJoinResponse->ecnSupport, pMsgLen) ==
         OSIX_FAILURE) ||
        (CapwapGetLocalIpAddr (&pJoinResponse->localIpAddr, pMsgLen) ==
         OSIX_FAILURE) ||
        (CapwapGetControlIpAddr (&pJoinResponse->controlIpAddr, pMsgLen) ==
         OSIX_FAILURE))
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to get Mandatory Join Response"
                    "Message Elements \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to get Mandatory Join Response"
                      "Message Elements \r\n"));
        return OSIX_FAILURE;

    }

    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2InternalId;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bImageName = OSIX_TRUE;
    CapwapGetProfileIdFromInternalProfId (u2InternalId, &u2ProfileId);
    pWssIfCapwapDB->CapwapGetDB.u2ProfileId = u2ProfileId;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB) !=
        OSIX_SUCCESS)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "WssWlanProcessConfigResponse : Getting IMAGE ID"
                    "for WLC failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "WssWlanProcessConfigResponse : Getting IMAGE ID"
                      "for WLC failed \r\n"));
        return OSIX_FAILURE;
    }
    MEMCPY (&pJoinResponse->imageId.data,
            pWssIfCapwapDB->CapwapGetDB.au1WtpImageIdentifier,
            pWssIfCapwapDB->CapwapGetDB.au1WtpImageIdLen);
    pJoinResponse->imageId.u2MsgEleLen = pWssIfCapwapDB->CapwapGetDB.
        au1WtpImageIdLen;
    pJoinResponse->u2IntProfileId = u2InternalId;
    if (CapwapGetWLCImageId (&(pJoinResponse->imageId), pMsgLen) ==
        OSIX_FAILURE)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to get Optional Join"
                    "Response Message Elements \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to get Optional Join"
                      "Response Message Elements \r\n"));
        return CAPWAP_FAILURE;
    }

    MEMCPY (&pSessEntry->ImageId.data,
            &pJoinResponse->imageId.data, pJoinResponse->imageId.u2MsgEleLen);
    pSessEntry->ImageId.u2MsgEleLen = pJoinResponse->imageId.u2MsgEleLen;

    /* update Optional msg elements */
    /*if ((CapwapGetImageId(&pJoinResponse->imageId, pMsgLen) == 
       OSIX_FAILURE) ||
       (CapwapGetTransProtocol(&pJoinResponse->transProto, pMsgLen) ==
       OSIX_FAILURE) ||
       (CapwapGetMaxMessageLen(&pJoinResponse->msgLength, pMsgLen) ==
       OSIX_FAILURE) ||
       (CapwapGetVendorPayload (&pJoinResponse->vendSpec, pMsgLen) ==
       OSIX_FAILURE))         
       {
       WSS_IF_DB_TEMP_MEM_RELEASE(pWssIfCapwapDB);
       CAPWAP_TRC (CAPWAP_FAILURE_TRC,
       "Failed to get Optional Join Response"
       "Message Elements \r\n");
       return CAPWAP_FAILURE;
       } */

    if (CapwapGetJoinRespVendorPayload (&pJoinResponse->vendSpec, pMsgLen,
                                        VENDOR_LOCAL_ROUTING_TYPE,
                                        u2InternalId) != OSIX_SUCCESS)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to get Optional Join Response"
                    "Message Elements \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to get Optional Join Response"
                      "Message Elements \r\n"));
        return CAPWAP_FAILURE;

    }
    /* update Control Header */
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpEcnSupport = OSIX_TRUE;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_SET_DB, pWssIfCapwapDB) !=
        OSIX_SUCCESS)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to set ECN support Join"
                    "Response Message Elements \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to set ECN support Join"
                      "Response Message Elements \r\n"));
        return OSIX_FAILURE;
    }
    pJoinResponse->u1NumMsgBlocks = 0;    /* flags always zero */

    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    CAPWAP_FN_EXIT ();
    return CAPWAP_SUCCESS;

}

/************************************************************************/
/*  Function Name   : CapwapValidateJoinrequestMsgElems                 */
/*  Description     : The function validates the functional             */
/*                    characteristics of the received CAPWAP            */
/*                    Join request agains the configured                */
/*                    WTP profile                                       */
/*  Input(s)        : discReqMsg - parsed CAPWAP Discovery request      */
/*                    pRcvBuf - Received discovery packet               */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
CapwapValidateJoinRequestMsgElems (UINT1 *pRcvBuf,
                                   tCapwapControlPacket * pJoinReqMsg,
                                   tRemoteSessionManager * pSess)
{
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tWtpBoardData       wtpBoardData;
    UINT4               u4Offset = 0;

    CAPWAP_FN_ENTRY ();

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&wtpBoardData, 0, sizeof (tWtpBoardData));

    pWssIfCapwapDB->CapwapIsGetAllDB.bActiveWtps = OSIX_TRUE;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB) ==
        OSIX_FAILURE)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to Retrirve the CAPWAP DB \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to Retrirve the CAPWAP DB \r\n"));
        return OSIX_FAILURE;
    }
    if (pWssIfCapwapDB->u2ActiveWtps > MAX_WTP_SUPPORTED)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "WLC current serving the"
                    "Max WTPs supported \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "WLC current serving the" "Max WTPs supported \r\n"));
        pJoinReqMsg->u4ReturnCode = JOIN_FAILURE_RES_DEPLETION;
    }

    u4Offset =
        pJoinReqMsg->capwapMsgElm[WTP_BOARD_DATA_JOIN_REQ_INDEX].pu2Offset[0];
    /* Get the WTP Base MAC Addr */
    if (CapwapGetWtpBoardDataFromBuf (pRcvBuf, u4Offset, &wtpBoardData) ==
        OSIX_SUCCESS)
    {
        MEMCPY (pWssIfCapwapDB->CapwapGetDB.WtpMacAddress,
                wtpBoardData.wtpBoardInfo[WTP_BASE_MAC_ADDRESS].boardInfoData,
                wtpBoardData.wtpBoardInfo[WTP_BASE_MAC_ADDRESS].
                u2BoardInfoLength);
    }

    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_INDEX_FROM_MAC, pWssIfCapwapDB)
        == OSIX_SUCCESS)
    {
        pJoinReqMsg->u2IntProfileId =
            pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId;
    }
    else
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "The Received Base Mac is not configured in the WTP profile \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "The Received Base Mac is not configured in the WTP profile \r\n"));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    /* Get the Join Response Result Code */
    if ((CapwapValidateLocationData (pRcvBuf, pJoinReqMsg,
                                     LOCATION_DATA_JOIN_REQ_INDEX) ==
         OSIX_FAILURE)
        ||
        (CapwapValidateFrameTunnelMode
         (pRcvBuf, pJoinReqMsg,
          WTP_FRAME_TUNNEL_MODE_JOIN_REQ_INDEX) == OSIX_FAILURE)
        ||
        (CapwapValidateWtpBoardData
         (pRcvBuf, pJoinReqMsg, WTP_BOARD_DATA_JOIN_REQ_INDEX) == OSIX_FAILURE)
        ||
        (CapwapValidateWtpName (pRcvBuf, pJoinReqMsg, WTP_NAME_JOIN_REQ_INDEX)
         == OSIX_FAILURE)
        ||
        (CapwapValidateWtpSessionId
         (pRcvBuf, pJoinReqMsg, pSess,
          SESSION_ID_JOIN_REQ_INDEX) == OSIX_FAILURE)
        ||
        (CapwapValidateMacType
         (pRcvBuf, pJoinReqMsg, WTP_MAC_TYPE_JOIN_REQ_INDEX) == OSIX_FAILURE)
        ||
        (CapwapValidateEcnSupport
         (pRcvBuf, pJoinReqMsg, ECN_SUPPORT_JOIN_REQ_INDEX) == OSIX_FAILURE)
        ||
        (CapwapValidateRadioInfo
         (pRcvBuf, pJoinReqMsg, WTP_RADIO_INFO_JOIN_REQ_INDEX) == OSIX_FAILURE))
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Validate"
                    "Join Request Message Elements \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to Validate"
                      "Join Request Message Elements \r\n"));
        return OSIX_FAILURE;
    }
    /* Validate optional elements */

    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;        /* For TEsting */
}

INT4
CapGetResultCode (UINT1 *pRcvBuf, tCapwapControlPacket * pCpParsePkt,
                  UINT4 u4Index, UINT4 *pRetCode)
{
    UINT2               u2NumMsgElems;
    UINT4               u4Offset;
    tResCode            ResCode;

    CAPWAP_FN_ENTRY ();
    if (pCpParsePkt == NULL)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Parsed packet is"
                    "NULL, return Failure \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId, "Parsed packet is"
                      "NULL, return Failure \r\n"));
        return OSIX_FAILURE;
    }
    u2NumMsgElems = pCpParsePkt->capwapMsgElm[u4Index].numMsgElemInstance;
    if (u2NumMsgElems > 1)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Received the Retrun Code More than once \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Received the Retrun Code More than once \r\n"));
        return OSIX_FAILURE;
    }
    else
    {
        u4Offset = pCpParsePkt->capwapMsgElm[u4Index].pu2Offset[0];
        pRcvBuf += u4Offset;
        CAPWAP_GET_2BYTE (ResCode.u2MsgEleType, pRcvBuf);
        CAPWAP_GET_2BYTE (ResCode.u2MsgEleLen, pRcvBuf);
        CAPWAP_GET_4BYTE (ResCode.u4Value, pRcvBuf);
        *pRetCode = ResCode.u4Value;
    }
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapProcessJoinResponse                         */
/*  Description     : The function processes the received CAPWAP        */
/*                    join response. This function handling is mainly   */
/*                    done in the AP and it validates the received join */
/*                    response and updates WSS-AP.                      */
/*  Input(s)        : JoinRespMsg- Join response packet received        */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
CapwapProcessJoinResponse (tSegment * pBuf,
                           tCapwapControlPacket * pJoinRespMsg,
                           tRemoteSessionManager * pSessEntry)
{
    UINT1              *pRcvBuf = NULL, *pu1TxBuf = NULL;
    tSegment           *pTxBuf = NULL;
    INT4                i4Status = OSIX_SUCCESS;
    UINT4               u4RetCode;
    tConfigStatusReq   *pConfigStatusReq = NULL;
    UINT1               u1IsNotLinear = OSIX_FALSE;
    UINT2               u2PacketLen;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tImageDataReq       ImageDataReq;
    UINT2               u2NumOptionalMsg;
    UINT1               u1Index = 0;
    UINT4               u4Offset = 0;
    UINT2               u2Type;
    UINT2               u2Len;
    UINT2               u2ImageIdLen = 0;
    UINT4               u4VendorId;
    UINT1               ImageId[CAPWAP_MAX_IMAGEID_DATA_LEN];
    UINT2               u2MsgType;
#ifdef WTP_WANTED
    UINT1               u1ImageIDElementPresent = OSIX_FALSE;
    UINT1               au1FlashFile[IMAGE_DATA_FLASH_FILE_NAME_LEN];
    UINT1               u1StartImageDataTransfer = OSIX_FALSE;
    UINT1               u1DestLen = 0;
    FILE               *pFlashFp = NULL;
    UINT4               u4MsgLen = 0;
    tWssIfPMDB          WssIfPMDB;
#endif
#if CAP_WANTED
    UINT2               u2IntId = 0;
    UINT2               u2EchoInterval = 0;
#endif
    UINT1              *pu1TempBuf = NULL;

    CAPWAP_FN_ENTRY ();

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        pConfigStatusReq =
        (tConfigStatusReq *) (VOID *) UtlShMemAllocConfigStatusReqBuf ();
    if (pConfigStatusReq == NULL)
    {
        CAPWAP_TRC (CAPWAP_MGMT_TRC,
                    "CapwapProcessJoinResponse:- "
                    "UtlShMemAllocConfigStatusReqBuf returned failure\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                      "CapwapProcessJoinResponse:- "
                      "UtlShMemAllocConfigStatusReqBuf returned failure\n"));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (pConfigStatusReq, 0, sizeof (tConfigStatusReq));
    MEMSET (&ImageDataReq, 0, sizeof (ImageDataReq));
    MEMSET (ImageId, 0, CAPWAP_MAX_IMAGEID_DATA_LEN);

#ifdef WTP_WANTED
    MEMSET (au1FlashFile, 0, IMAGE_DATA_FLASH_FILE_NAME_LEN);
#endif
    if (pSessEntry == NULL || (pSessEntry->eCurrentState != CAPWAP_JOIN))
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Remote session Module is NULL, return Failure \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Remote session Module is NULL, return Failure \r\n"));
        UtlShMemFreeConfigStatusReqBuf ((UINT1 *) pConfigStatusReq);
        return OSIX_FAILURE;
    }
#if CAP_WANTED
    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpEchoInterval = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2IntId;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB) ==
        OSIX_SUCCESS)
    {
        u2EchoInterval = pWssIfCapwapDB->CapwapGetDB.u2WtpEchoInterval;
    }
#endif

    /* stop the retransmit interval timer
     * and clear the packet buffer pointer */
    if (CapwapTmrStop (pSessEntry, CAPWAP_RETRANSMIT_INTERVAL_TMR) ==
        OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_MGMT_TRC, "Stopped the retransmit timer \r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                      "Stopped the retransmit timer \r\n"));
        /* clear the packet buffer pointer */
#ifdef WLC_WANTED
        WlchdlrFreeTransmittedPacket (pSessEntry);
#else
        AphdlrFreeTransmittedPacket (pSessEntry);
#endif
    }

    u2PacketLen = (UINT2) CAPWAP_GET_BUF_LEN (pBuf);
    pRcvBuf = CAPWAP_BUF_IF_LINEAR (pBuf, 0, u2PacketLen);
    if (pRcvBuf == NULL)
    {
        pRcvBuf = (UINT1 *) MemAllocMemBlk (CAPWAP_PKTBUF_POOLID);
        CAPWAP_COPY_FROM_BUF (pBuf, pRcvBuf, 0, u2PacketLen);
        u1IsNotLinear = OSIX_TRUE;
    }
    if (CapGetResultCode (pRcvBuf, pJoinRespMsg, RESULT_CODE_JOIN_RESP_INDEX,
                          &u4RetCode) != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to Get Result Code from Received Buffer\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to Get Result Code from Received Buffer\r\n"));
        if (u1IsNotLinear == OSIX_TRUE)
        {
            MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pRcvBuf);
        }
        UtlShMemFreeConfigStatusReqBuf ((UINT1 *) pConfigStatusReq);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    switch (u4RetCode)
    {
        case JOIN_FAILURE_UNSPECIFIED:
        case JOIN_FAILURE_RES_DEPLETION:
        case JOIN_FAILURE_UNKNOWN_SOURCE:
        case JOIN_FAILURE_INCORRECT_DATA:
        case JOIN_FAILURE_SESSIONID_INUSE:
        case JOIN_FAILURE_HW_NOTSUPPORTED:
        case JOIN_FAILURE_BINDING_NOT_SUPPORTED:
        {
            CAPWAP_TRC1 (CAPWAP_FAILURE_TRC, "Join Request message"
                         "is Failed with the Return Code = %d \r\n", u4RetCode);
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Join Request message"
                          "is Failed with the Return Code = %d \r\n",
                          u4RetCode));
            /* Disconnect the Session */
            if (u1IsNotLinear == OSIX_TRUE)
            {
                MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pRcvBuf);
            }
            UtlShMemFreeConfigStatusReqBuf ((UINT1 *) pConfigStatusReq);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
        case CAPWAP_SUCCESS:
        {
            CAPWAP_TRC (CAPWAP_MGMT_TRC, "The Join Request is successful \r\n");
            SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                          "The Join Request is successful \r\n"));
            break;
        }
        default:
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Received the unknow return Code \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Received the unknow return Code \r\n"));
            if (u1IsNotLinear == OSIX_TRUE)
            {
                MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pRcvBuf);
            }
            UtlShMemFreeConfigStatusReqBuf ((UINT1 *) pConfigStatusReq);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
    }

    if (CapwapValidateJoinResponseMsgElems (pRcvBuf, pJoinRespMsg, pSessEntry)
        == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to validate Join Response Message Elements \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to validate Join Response Message Elements \r\n"));
        if (u1IsNotLinear == OSIX_TRUE)
        {
            MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pRcvBuf);
        }
        CAPWAP_TRC (CAPWAP_FSM_TRC, "Enter the CAPWAP DTLS TD State \r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                      "Enter the CAPWAP DTLS TD State \r\n"));
        pSessEntry->eCurrentState = CAPWAP_DTLSTD;
        if (CapwapShutDownDTLS (pSessEntry) == OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to Disconnect the Session \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to Disconnect the Session \r\n"));

        }
        pWssIfCapwapDB->u4DestIp = pSessEntry->remoteIpAddr.u4_addr[0];
        pWssIfCapwapDB->u4DestPort = pSessEntry->u4RemoteCtrlPort;
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_DELETE_RSM, pWssIfCapwapDB) ==
            OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        " WssIfProcessCapwapDBMsg Failed.  \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          " WssIfProcessCapwapDBMsg Failed.  \r\n"));
        }
#ifdef WTP_WANTED
        pSessEntry->eCurrentState = CAPWAP_IDLE;
        CapwapSendRestartDiscEvent ();
#endif
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        UtlShMemFreeConfigStatusReqBuf ((UINT1 *) pConfigStatusReq);
        return OSIX_FAILURE;
    }
    if (u1IsNotLinear == OSIX_TRUE)
    {
        MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pRcvBuf);
    }

    pSessEntry->wtpCapwapStats.joinRespRxd++;

#ifdef WTP_WANTED
    MEMSET (&WssIfPMDB, 0, sizeof (tWssIfPMDB));
    WssIfPMDB.u2ProfileId = pJoinRespMsg->u2IntProfileId;
    if (WssIfProcessPMDBMsg (WSS_PM_WTP_EVT_VSP_CAPWAP_STATS_GET, &WssIfPMDB) !=
        OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to update Join Req Stats to PM \n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to update Join Req Stats to PM \n"));
    }

    WssIfPMDB.wtpVSPCPWPWtpStats.joinRespRxd =
        pSessEntry->wtpCapwapStats.joinRespRxd;
    if (WssIfProcessPMDBMsg (WSS_PM_WTP_EVT_VSP_CAPWAP_STATS_SET, &WssIfPMDB) !=
        OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to update Join Req Stats to PM \n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to update Join Req Stats to PM \n"));
    }
#endif
    /* Process for Optional Join Response elements */
    u2NumOptionalMsg = pJoinRespMsg->numOptionalElements;
    for (u1Index = 0; u1Index < u2NumOptionalMsg; u1Index++)
    {
        /* Get the message element type */
        u2MsgType = pJoinRespMsg->capwapMsgElm[MAX_JOIN_RES_MAND_MSG_ELEMENTS +
                                               u1Index].u2MsgType;
        if (u2MsgType == IMAGE_IDENTIFIER)
        {
            u4Offset = pJoinRespMsg->capwapMsgElm[MAX_JOIN_RES_MAND_MSG_ELEMENTS
                                                  + u1Index].pu2Offset[0];
            pRcvBuf += u4Offset;
            CAPWAP_GET_2BYTE (u2Type, pRcvBuf);
            CAPWAP_GET_2BYTE (u2Len, pRcvBuf);
            CAPWAP_GET_4BYTE (u4VendorId, pRcvBuf);
            u2ImageIdLen = (UINT2) (u2Len - 4);
            CAPWAP_GET_NBYTE (ImageId, pRcvBuf, u2ImageIdLen);
#ifdef WTP_WANTED
            u1ImageIDElementPresent = OSIX_TRUE;
#endif
            MEMCPY (&pSessEntry->ImageId.data, ImageId, u2ImageIdLen);
            break;
        }
    }

#ifdef WTP_WANTED
    if (OSIX_TRUE == u1ImageIDElementPresent)
    {
        u1DestLen = u2ImageIdLen + STRLEN (IMAGE_DATA_FLASH_CONF_LOC);
        SNPRINTF ((char *) au1FlashFile, u1DestLen + 1, "%s%s",
                  IMAGE_DATA_FLASH_CONF_LOC, ImageId);
        /* If Image is not found in Flash */
        if ((pFlashFp = FOPEN ((char *) au1FlashFile, "r")) == NULL)
        {
            CAPWAP_TRC (CAPWAP_FSM_TRC, "File not found in Flash ... \r\n");
            SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                          "File not found in Flash ... \r\n"));
            u1StartImageDataTransfer = OSIX_TRUE;
        }
        else
        {
            CAPWAP_TRC (CAPWAP_FSM_TRC,
                        "Requested Image is found in Flash so dont reset \r\n");
            SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                          "Requested Image is found in Flash so dont reset \r\n"));

        }

        if (pFlashFp != NULL)
        {
            FCLOSE (pFlashFp);
        }
    }

    /* Checking if the Image Version in AP is same as the version WLC is
     * requesting AP to upgrade */
    if (u1StartImageDataTransfer == OSIX_TRUE)
    {
        /* Received the valid Join Response so change the
         * current state in the RSM */
        CAPWAP_TRC (CAPWAP_FSM_TRC, "Entered CAPWAP IMAGE Data State \r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                      "Entered CAPWAP IMAGE Data State \r\n"));
        pSessEntry->eCurrentState = CAPWAP_IMAGEDATA;
        gCapwapState = CAPWAP_IMAGEDATA;

        MEMCPY (&(ImageDataReq.ImageId.data), ImageId, u2ImageIdLen);
        ImageDataReq.ImageId.u2MsgEleLen = u2ImageIdLen;
        if (CapwapGetImageDataReqElements (&ImageDataReq, &u4MsgLen) ==
            OSIX_SUCCESS)
        {
            /* update the seq number */
            ImageDataReq.u1SeqNum = pSessEntry->lastTransmitedSeqNum + 1;
            ImageDataReq.u2CapwapMsgElemenLen = u4MsgLen +
                CAPWAP_CMN_MSG_ELM_HEAD_LEN;

            pTxBuf = CAPWAP_ALLOCATE_CRU_BUF (CAPWAP_MAX_PKT_LEN, 0);
            if (pTxBuf == NULL)
            {
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Memory Allocation Failed \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Memory Allocation Failed \r\n"));
                UtlShMemFreeConfigStatusReqBuf ((UINT1 *) pConfigStatusReq);
                return OSIX_FAILURE;
            }
            pu1TxBuf = CAPWAP_BUF_IF_LINEAR (pBuf, 0, CAPWAP_MAX_PKT_LEN);
            if (pu1TxBuf == NULL)
            {
                pu1TxBuf = (UINT1 *) MemAllocMemBlk (CAPWAP_PKTBUF_POOLID);
                if (pu1TxBuf == NULL)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "MemAllocMemBlk Failed \r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "MemAllocMemBlk Failed \r\n"));
                    CAPWAP_RELEASE_CRU_BUF (pTxBuf);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    UtlShMemFreeConfigStatusReqBuf ((UINT1 *) pConfigStatusReq);
                    return OSIX_FAILURE;
                }
                CAPWAP_COPY_FROM_BUF (pTxBuf, pu1TxBuf, 0, CAPWAP_MAX_PKT_LEN);
                u1IsNotLinear = OSIX_TRUE;
            }

            if ((CapwapAssembleWTPImageDataReq
                 (ImageDataReq.u2CapwapMsgElemenLen, &ImageDataReq,
                  pu1TxBuf)) == OSIX_FAILURE)
            {
                CAPWAP_RELEASE_CRU_BUF (pTxBuf);
                if (u1IsNotLinear == OSIX_TRUE)
                {
                    MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pu1TxBuf);
                }
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                UtlShMemFreeConfigStatusReqBuf ((UINT1 *) pConfigStatusReq);
                return OSIX_FAILURE;
            }
            if (u1IsNotLinear == OSIX_TRUE)
            {
                /* If the CRU buffer memory is not linear */
                CAPWAP_COPY_TO_BUF (pTxBuf, pu1TxBuf, 0, CAPWAP_MAX_PKT_LEN);
                MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pu1TxBuf);
            }

#if CAP_WANTED
            /* Start echo interval timer and send Image Data Req */
            i4Status = CapwapTmrStart (pSessEntry, CAPWAP_ECHO_INTERVAL_TMR,
                                       u2EchoInterval);
            if (i4Status == OSIX_FAILURE)
            {
                /* Handle the failure */
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Failed to start the Echo Interval timer"
                            "in Image Data State \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to start the Echo Interval timer"
                              "in Image Data State \r\n"));
            }
#endif
            i4Status = CapwapTxMessage (pTxBuf,
                                        pSessEntry,
                                        ImageDataReq.u2CapwapMsgElemenLen +
                                        CAPWAP_MSG_ELE_LEN,
                                        WSS_CAPWAP_802_11_CTRL_PKT);
            if (i4Status == OSIX_SUCCESS)
            {
                /* Update the last transmitted sequence number */
                pSessEntry->lastTransmitedSeqNum++;
                pSessEntry->lastTransmittedPktLen =
                    (ImageDataReq.u2CapwapMsgElemenLen + CAPWAP_MSG_ELE_LEN);
            }
            else
            {
                /* Handle the failure */
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Failed to Transmit the packet \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to Transmit the packet \r\n"));
                if (u1IsNotLinear == OSIX_TRUE)
                {
                    MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pu1TxBuf);
                }
                CAPWAP_RELEASE_CRU_BUF (pTxBuf);
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                UtlShMemFreeConfigStatusReqBuf ((UINT1 *) pConfigStatusReq);
                return OSIX_FAILURE;
            }
            CAPWAP_RELEASE_CRU_BUF (pTxBuf);
        }
    }
    else
#endif
    {

        /* Image Version in the received Join Response 
         * is same as the one here in AP */
        /* Received the valid Join Response so change the 
         * current state in the RSM */
        CAPWAP_TRC (CAPWAP_FSM_TRC, "Entered CAPWAP CONFIGURE State \r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                      "Entered CAPWAP CONFIGURE State \r\n"));
        pSessEntry->eCurrentState = CAPWAP_CONFIGURE;

        if (CapwapGetConfigStatusRequestMsgElements (pConfigStatusReq,
                                                     pSessEntry) ==
            OSIX_SUCCESS)
        {
            /* update the seq number */
            pConfigStatusReq->u1SeqNum =
                (UINT1) (pSessEntry->lastTransmitedSeqNum + 1);

            pTxBuf = CAPWAP_ALLOCATE_CRU_BUF (CAPWAP_MAX_PKT_LEN, 0);
            if (pTxBuf == NULL)
            {
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Memory Allocation Failed \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Memory Allocation Failed \r\n"));
                UtlShMemFreeConfigStatusReqBuf ((UINT1 *) pConfigStatusReq);
                return OSIX_FAILURE;
            }
            pu1TxBuf = CAPWAP_BUF_IF_LINEAR (pBuf, 0, CAPWAP_MAX_PKT_LEN);
            if (pu1TxBuf == NULL)
            {
                pu1TxBuf = (UINT1 *) MemAllocMemBlk (CAPWAP_PKTBUF_POOLID);
                if (pu1TxBuf == NULL)
                {
                    CAPWAP_RELEASE_CRU_BUF (pTxBuf);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "MemAllocMemBlk Failed \r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "MemAllocMemBlk Failed \r\n"));
                    UtlShMemFreeConfigStatusReqBuf ((UINT1 *) pConfigStatusReq);
                    return OSIX_FAILURE;
                }
                u1IsNotLinear = OSIX_TRUE;
            }

            if ((CapwapAssembleConfigStatusRequest (pConfigStatusReq->
                                                    u2CapwapMsgElemenLen,
                                                    pConfigStatusReq,
                                                    pu1TxBuf)) == OSIX_FAILURE)
            {
                CAPWAP_RELEASE_CRU_BUF (pTxBuf);
                if (u1IsNotLinear == OSIX_TRUE)
                {
                    MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pu1TxBuf);
                }
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                UtlShMemFreeConfigStatusReqBuf ((UINT1 *) pConfigStatusReq);
                return OSIX_FAILURE;
            }
            pu1TempBuf = pu1TxBuf + CAPWAP_MSG_OFFSET;
            CAPWAP_GET_2BYTE (pConfigStatusReq->u2CapwapMsgElemenLen,
                              pu1TempBuf);
            if (u1IsNotLinear == OSIX_TRUE)
            {
                /* If the CRU buffer memory is not linear */
                CAPWAP_COPY_TO_BUF (pTxBuf, pu1TxBuf, 0,
                                    pConfigStatusReq->u2CapwapMsgElemenLen +
                                    CAPWAP_SOCK_HDR_LEN);
                MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pu1TxBuf);
            }
            i4Status =
                CapwapTxMessage (pTxBuf, pSessEntry,
                                 (UINT4) (pConfigStatusReq->
                                          u2CapwapMsgElemenLen +
                                          CAPWAP_SOCK_HDR_LEN),
                                 WSS_CAPWAP_802_11_CTRL_PKT);
            if (i4Status == OSIX_SUCCESS)
            {
                /* Update the last transmitted sequence number */
                pSessEntry->lastTransmitedSeqNum++;
                /* store the packet buffer pointer in
                 * the remote session DB copy the pointer */
                pSessEntry->lastTransmittedPkt = pTxBuf;
                pSessEntry->lastTransmittedPktLen = (UINT4) (pConfigStatusReq->
                                                             u2CapwapMsgElemenLen
                                                             +
                                                             CAPWAP_MSG_ELE_LEN);
                /*Start the Retransmit timer */
                CapwapTmrStart (pSessEntry, CAPWAP_RETRANSMIT_INTERVAL_TMR,
                                MAX_RETRANSMIT_INTERVAL_TIMEOUT);
                pSessEntry->u1RetransmitCount = 0;
#ifdef WTP_WANTED
                MEMSET (&WssIfPMDB, 0, sizeof (tWssIfPMDB));
                WssIfPMDB.u2ProfileId = pJoinRespMsg->u2IntProfileId;
                if (WssIfProcessPMDBMsg
                    (WSS_PM_WTP_EVT_VSP_CAPWAP_STATS_GET,
                     &WssIfPMDB) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "Failed to update Join Req Stats to PM \n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Failed to update Join Req Stats to PM \n"));
                }

                WssIfPMDB.wtpVSPCPWPWtpStats.cfgReqSent++;
                if (WssIfProcessPMDBMsg
                    (WSS_PM_WTP_EVT_VSP_CAPWAP_STATS_SET,
                     &WssIfPMDB) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "Failed to update Join Req Stats to PM \n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Failed to update Join Req Stats to PM \n"));
                }
#endif
            }
            else
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Failed to Transmit the packet \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to Transmit the packet \r\n"));
                CAPWAP_RELEASE_CRU_BUF (pTxBuf);
                pSessEntry->lastTransmittedPkt = NULL;
                pSessEntry->lastTransmittedPktLen = 0;
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                UtlShMemFreeConfigStatusReqBuf ((UINT1 *) pConfigStatusReq);
                return OSIX_FAILURE;
            }
        }
    }
    /*CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Transmit the packet \r\n");
       CAPWAP_RELEASE_CRU_BUF(pTxBuf); */
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    UtlShMemFreeConfigStatusReqBuf ((UINT1 *) pConfigStatusReq);
    CAPWAP_FN_EXIT ();

    return (i4Status);
}

INT4
CapwapValidateJoinResponseMsgElems (UINT1 *pRcvdBuf,
                                    tCapwapControlPacket * pJoinRespMsg,
                                    tRemoteSessionManager * pSessEntry)
{
    tVendorSpecPayload  vendSpec;
    UINT2               u2NumOptionalMsg;
    UINT2               u2MsgType;
    UINT1               u1Index;
    UNUSED_PARAM (pSessEntry);
    CAPWAP_FN_ENTRY ();

    MEMSET (&vendSpec, 0, sizeof (tVendorSpecPayload));
    u2NumOptionalMsg = pJoinRespMsg->numOptionalElements;

    /* Validate Mandatore Join Response Message Elements */
    if (CapwapValidateAcName (pRcvdBuf, pJoinRespMsg,
                              AC_NAME_JOIN_RESP_INDEX) == OSIX_FAILURE)
    {
        /* Set AC Name in WTP DB */
        if (CapwapSetAcName (pRcvdBuf, pJoinRespMsg, AC_NAME_JOIN_RESP_INDEX) ==
            OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to Set the AC Name in the CAPWAP DB \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to Set the AC Name in the CAPWAP DB \r\n"));
            return OSIX_FAILURE;
        }
    }

    if (CapwapValidateRadioInfo (pRcvdBuf, pJoinRespMsg,
                                 WTP_RADIO_INFO_JOIN_RESP_INDEX) ==
        OSIX_SUCCESS)
    {
        /* Set AC Name in WTP DB */
        if (CapwapSetRadioInfo (pRcvdBuf, pJoinRespMsg,
                                WTP_RADIO_INFO_JOIN_RESP_INDEX) == OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to Set the AC Name in the CAPWAP DB \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to Set the AC Name in the CAPWAP DB \r\n"));
            return OSIX_FAILURE;
        }
    }

    for (u1Index = 0; u1Index < u2NumOptionalMsg; u1Index++)
    {
        /* Get the message element type */
        u2MsgType =
            pJoinRespMsg->capwapMsgElm[MAX_JOIN_RES_MAND_MSG_ELEMENTS +
                                       u1Index].u2MsgType;
        switch (u2MsgType)
        {
            case VENDOR_SPECIFIC_PAYLOAD:
                if (CapwapValidateVendSpecPld (pRcvdBuf, pJoinRespMsg,
                                               &vendSpec,
                                               (UINT2)
                                               (MAX_JOIN_RES_MAND_MSG_ELEMENTS +
                                                u1Index)) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "Failed to Set Vendor Specific data in the CAPWAP DB \r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Failed to Set Vendor Specific data in the CAPWAP DB \r\n"));
                    return OSIX_FAILURE;

                }
                break;
            default:
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Received the unknow Message Type \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Received the unknow Message Type \r\n"));
                break;
        }
    }
#if CAP_WANTED
    u2NumMsgElems = pJoinRespMsg->capwapMsgElm[RESULT_CODE_JOIN_RESP_INDEX].
        numMsgElemInstance;
    u4Offset = pJoinRespMsg->capwapMsgElm[RESULT_CODE_JOIN_RESP_INDEX].
        pu2Offset[u2NumMsgElems];
    CapwapGetResultCodeFromBuf (pRcvBuf, u4Offset, &(JoinResponse.resultCode),
                                u2NumMsgElems);

    /* AC Name */
    u2NumMsgElems = pJoinRespMsg->capwapMsgElm[AC_NAME_JOIN_RESP_INDEX].
        numMsgElemInstance;
    u4Offset = pJoinRespMsg->capwapMsgElm[AC_NAME_JOIN_RESP_INDEX].
        pu2Offset[u2NumMsgElems];
    CapwapGetAcNameFromBuf (pRcvBuf, u4Offset, &(JoinResponse.acName),
                            u2NumMsgElems);

    /*AC Descriptor */
    u2NumMsgElems = pJoinRespMsg->capwapMsgElm[AC_DESCRIPTOR_JOIN_RESP_INDEX].
        numMsgElemInstance;
    u4Offset = pJoinRespMsg->capwapMsgElm[AC_DESCRIPTOR_JOIN_RESP_INDEX].
        pu2Offset[u2NumMsgElems];
    CapwapGetAcDescriptorFromBuf (pRcvBuf, u4Offset, &(JoinResponse.acDesc),
                                  u2NumMsgElems);

    /* WTP RadioInfo */
    u2NumMsgElems = pJoinRespMsg->capwapMsgElm[WTP_RADIO_INFO_JOIN_RESP_INDEX].
        numMsgElemInstance;
    u4Offset = pJoinRespMsg->capwapMsgElm[WTP_RADIO_INFO_JOIN_RESP_INDEX].
        pu2Offset[u2NumMsgElems];
    CapwapGetRadioInfoFromBuf (pRcvBuf, u4Offset, &JoinResponse.wtpRadioInfo,
                               u2NumMsgElems);

    /* ECN Support */
    u2NumMsgElems = pJoinRespMsg->capwapMsgElm[ECN_SUPPORT_JOIN_RESP_INDEX].
        numMsgElemInstance;
    u4Offset = pJoinRespMsg->capwapMsgElm[ECN_SUPPORT_JOIN_RESP_INDEX].
        pu2Offset[u2NumMsgElems];
    CapwapGetEcnSupportFromBuf (pRcvBuf, u4Offset, &(JoinResponse.ecnSupport),
                                u2NumMsgElems);

    /* Control IP Address */
    u2NumMsgElems =
        pJoinRespMsg->capwapMsgElm[CAPWAP_CONTRL_IP_ADDR_JOIN_RESP_INDEX].
        numMsgElemInstance;
    u4Offset =
        pJoinRespMsg->capwapMsgElm[CAPWAP_CONTRL_IP_ADDR_JOIN_RESP_INDEX].
        pu2Offset[u2NumMsgElems];
    CapwapGetCtrlIP4AddrFromBuf (pRcvBuf, u4Offset,
                                 &(JoinResponse.controlIpAddr), u2NumMsgElems);

    /* Local IP Address */
    u2NumMsgElems =
        pJoinRespMsg->capwapMsgElm[CAPWAP_LOCAL_IP_ADDR_JOIN_RESP_INDEX].
        numMsgElemInstance;
    u4Offset = pJoinRespMsg->capwapMsgElm[CAPWAP_LOCAL_IP_ADDR_JOIN_RESP_INDEX].
        pu2Offset[u2NumMsgElems];
    CapwapGetLocalIPAddrFromBuf (pRcvBuf, u4Offset, &(JoinResponse.localIpAddr),
                                 u2NumMsgElems);
#endif
    return OSIX_SUCCESS;
}

INT4
CapwapGetConfigStatusRequestMsgElements (tConfigStatusReq * pConfigStatusReq,
                                         tRemoteSessionManager * pSessEntry)
{
    UINT2               u2MsgLen = 0;
    UINT4               u4MsgLen = 0;
    UINT2               u1Index = 0;
    UINT4               u4VendMsgLen = 0;
    UNUSED_PARAM (pSessEntry);
    /* update CAPWAP Header */
    if (CapwapConstructCpHeader (&pConfigStatusReq->capwapHdr) == OSIX_FAILURE)
    {
        CAPWAP_TRC (ALL_FAILURE_TRC,
                    "capwapProcessJoinResponse:Failed to construct"
                    "the capwap Header \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "capwapProcessJoinResponse:Failed to construct"
                      "the capwap Header \r\n"));
        return OSIX_FAILURE;
    }

    /* update Mandatory message elements */
    /* coverity fix begin */
    /*if ((CapwapGetACName(&pConfigStatusReq->acName, &u4MsgLen) == 
       OSIX_FAILURE)|| */
    if ((CapwapGetACName (&pConfigStatusReq->acName, (UINT4 *) &u4MsgLen) ==
         OSIX_FAILURE) ||
        /* coverity fix end */
        (CapwapGetRadioAdminState (&pConfigStatusReq->radAdminState,
                                   &u4MsgLen) == OSIX_FAILURE) ||
        (CapwapGetStatsTimer (&pConfigStatusReq->statsTimer, &u4MsgLen) ==
         OSIX_FAILURE) ||
        (CapwapGetWtpRebootStats (&pConfigStatusReq->rebootStats, &u4MsgLen)
         == OSIX_FAILURE))
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to get Mandatory Configuration Request"
                    "Message Elements\r\n ");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to get Mandatory Configuration Request"
                      "Message Elements\r\n "));
        return OSIX_FAILURE;
    }
    /* update Optional Message Elements */
    /* if ((CapwapGetTransProtocol (&pConfigStatusReq->transProto, &u4MsgLen)
     * == OSIX_FAILURE) ||
     (CapwapGetAcNameWithPriority (&pConfigStatusReq->acNameWithPrio,
     &u4MsgLen) == OSIX_FAILURE) ||
     (CapwapGetWtpStaticIpInfo (&pConfigStatusReq->wtpStaticIpAddr,
     &u4MsgLen) == OSIX_FAILURE) ||
     (CapwapGetVendorPayload (&pConfigStatusReq->vendSpec, &u4MsgLen) ==
     OSIX_FAILURE))
     {
     CAPWAP_TRC (CAPWAP_FAILURE_TRC ,
     "Failed to get Optional Configuration Request"
     "Message Elements \r\n");
     return OSIX_FAILURE;
     }*/
    pConfigStatusReq->isVendorOptional = OSIX_TRUE;
#ifdef RFMGMT_WANTED
    for (u1Index = 0; u1Index <= VENDOR_CH_SWITCH_STATUS_TYPE; u1Index++)
#else
    for (u1Index = 0; u1Index < 5; u1Index++)
#endif
    {
        if (CapwapGetConfigStatVendorPayload
            (&(pConfigStatusReq->vendSpec[u1Index]), &u4VendMsgLen,
             (UINT1) u1Index) == OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to get Vendor Configuration"
                        "Request Message Elements \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to get Vendor Configuration"
                          "Request Message Elements \r\n"));
            return OSIX_FAILURE;
        }
    }

    /* update Control Header */
    pConfigStatusReq->u2CapwapMsgElemenLen = (UINT2) (u4MsgLen + u2MsgLen +
                                                      CAPWAP_CMN_MSG_ELM_HEAD_LEN);
    pConfigStatusReq->u1NumMsgBlocks = 0;    /* flags */
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}
#endif /* __CAPWAP_JOIN_C__ */
/*-----------------------------------------------------------------------*/
/*                    End of the file  capwapjoin.c                      */
/*-----------------------------------------------------------------------*/
