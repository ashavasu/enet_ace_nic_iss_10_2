/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: capwapclidsg.c,v 1.2 2017/05/23 14:16:49 siva Exp $       
*
* Description: This file contains the routines for DataStructure access for the module Capwap 
*********************************************************************/

#include "capwapinc.h"
#include "wlchdlr.h"
#include "capwapcli.h"
#include "wssifstatdfs.h"
#include "wssifstaextn.h"
#include "wsspmdb.h"
#include "wssifcapdb.h"
#include "wssifcapsz.h"

#ifdef SNTP_WANTED
extern UINT4        SntpTmToSec (tUtlTm *);
#endif
extern tRBTree      gWssPmWTPIDStatsProcessDB;
extern tRBTree      gWssPmWTPIDRadIDStatsProcessDB;
extern tRBTree      gCapProfileMacMapTable;
extern tRBTree      gWssPmCapwATPStatsProcessDB;
extern tRBTree      gWssPmWTPRunEvents;

/****************************************************************************
 Functionn    : CapwapGetFirstFsCapwATPStatsTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pWsscfgFsWlanClientStatsEntry or NULL
****************************************************************************/
INT1
CapwapGetFirstCapwATPStatsTable (UINT4 *pu4CapwapBaseWtpProfileId)
{
    tWssPmCapwATPStatsProcessRBDB *pWssPmCapwATPStatsProcessRBDB = NULL;
    UINT2               u2CapwapBaseWtpProfileId = 0;
    pWssPmCapwATPStatsProcessRBDB =
        (tWssPmCapwATPStatsProcessRBDB *)
        RBTreeGetFirst (gWssPmCapwATPStatsProcessDB);
    if (pWssPmCapwATPStatsProcessRBDB != NULL)
    {
        CapwapGetProfileIdFromInternalProfId ((UINT2)
                                              pWssPmCapwATPStatsProcessRBDB->
                                              u4CapwapBaseWtpProfileId,
                                              &u2CapwapBaseWtpProfileId);
        *pu4CapwapBaseWtpProfileId = (UINT4) u2CapwapBaseWtpProfileId;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Functionn    : CapwapGetNextFsCapwATPStatsTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pWsscfgFsWlanClientStatsEntry or NULL
****************************************************************************/

INT1
CapwapGetNextCapwATPStatsTable (UINT4 u4CapwapBaseWtpProfileId,
                                UINT4 *pu4NextCapwapBaseWtpProfileId)
{

    tWssPmCapwATPStatsProcessRBDB CapwapCapwATPStats;
    tWssPmCapwATPStatsProcessRBDB *pNextWssPmCapwATPStatsProcessRBDB = NULL;
    UINT2               u2CapwapBaseWtpProfileId = 0;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;

    CAPWAP_IF_CAP_DB_ALLOC (pWssIfCapwapDB);

    if (pWssIfCapwapDB == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    MEMSET (&CapwapCapwATPStats, 0, sizeof (tWssPmCapwATPStatsProcessRBDB));

    pWssIfCapwapDB->CapwapGetDB.u2ProfileId = (UINT2) u4CapwapBaseWtpProfileId;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpProfileId = OSIX_TRUE;

    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_INTERNAL_ID, pWssIfCapwapDB) ==
        OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FSM_TRC, "Getting Internal Id Failed\r\n");
        CAPWAP_IF_CAP_DB_RELEASE (pWssIfCapwapDB);
        return SNMP_FAILURE;
    }

    /* Assign the index */
    CapwapCapwATPStats.u4CapwapBaseWtpProfileId =
        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId;

    pNextWssPmCapwATPStatsProcessRBDB =
        (tWssPmCapwATPStatsProcessRBDB *)
        RBTreeGetNext
        (gWssPmCapwATPStatsProcessDB, (tRBElem *) & CapwapCapwATPStats, NULL);
    if (pNextWssPmCapwATPStatsProcessRBDB != NULL)
    {
        CapwapGetProfileIdFromInternalProfId
            ((UINT2) pNextWssPmCapwATPStatsProcessRBDB->
             u4CapwapBaseWtpProfileId, &u2CapwapBaseWtpProfileId);
        *pu4NextCapwapBaseWtpProfileId = (UINT4) u2CapwapBaseWtpProfileId;
        CAPWAP_IF_CAP_DB_RELEASE (pWssIfCapwapDB);
        return SNMP_SUCCESS;
    }

    CAPWAP_IF_CAP_DB_RELEASE (pWssIfCapwapDB);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  CapwapGetFsCapwapEnable
 Input       :  The Indices
 Input       :  pi4FsCapwapEnable
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetFsCapwapEnable (INT4 *pi4FsCapwapEnable)
{
    *pi4FsCapwapEnable = gCapwapGlobals.CapwapGlbMib.i4FsCapwapEnable;

    if (*pi4FsCapwapEnable == 0)
    {
        gCapwapGlobals.CapwapGlbMib.i4FsCapwapEnable = CAPWAP_ENABLE;
        *pi4FsCapwapEnable = CAPWAP_ENABLE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapSetFsCapwapEnable
 Input       :  i4FsCapwapEnable
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
CapwapSetFsCapwapEnable (INT4 i4FsCapwapEnable)
{
    INT4                i4SetOption = SNMP_SUCCESS;

    if (gCapwapGlobals.CapwapGlbMib.i4FsCapwapEnable == i4FsCapwapEnable)
    {
        return OSIX_SUCCESS;
    }

    if (i4FsCapwapEnable == CLI_FSCAPWAP_ENABLE)
    {
        if (CapwapEnable () != OSIX_SUCCESS)
        {
            return OSIX_FAILURE;
        }
    }
    else if (i4FsCapwapEnable == CLI_FSCAPWAP_DISABLE)
    {
        if (CapwapDisable () != OSIX_SUCCESS)
        {
            return OSIX_FAILURE;
        }
    }

    gCapwapGlobals.CapwapGlbMib.i4FsCapwapEnable = i4FsCapwapEnable;

    nmhSetCmnNew (FsCapwapModuleStatus, 0, CapwapMainTaskLock,
                  CapwapMainTaskUnLock, 0, 0, 0, i4SetOption, "%i",
                  gCapwapGlobals.CapwapGlbMib.i4FsCapwapEnable);

    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  CapwapGetFsCapwapShutdown
 Input       :  The Indices
 Input       :  pi4FsCapwapShutdown
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetFsCapwapShutdown (INT4 *pi4FsCapwapShutdown)
{
    *pi4FsCapwapShutdown = gCapwapGlobals.CapwapGlbMib.i4FsCapwapShutdown;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapSetFsCapwapShutdown
 Input       :  i4FsCapwapShutdown
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
CapwapSetFsCapwapShutdown (INT4 i4FsCapwapShutdown)
{
    INT4                i4SetOption = SNMP_SUCCESS;

    if (gCapwapGlobals.CapwapGlbMib.i4FsCapwapShutdown == i4FsCapwapShutdown)
    {
        return OSIX_SUCCESS;
    }
    gCapwapGlobals.CapwapGlbMib.i4FsCapwapShutdown = i4FsCapwapShutdown;

    if (i4FsCapwapShutdown == CLI_CAPWAP_SHUTDOWN)
    {
        if (CapwapShutdown () != OSIX_SUCCESS)
        {
            return OSIX_FAILURE;
        }
    }
    else if (i4FsCapwapShutdown == CLI_CAPWAP_NO_SHUTDOWN)
    {
        if (CapwapNoShutdown () != OSIX_SUCCESS)
        {
            return OSIX_FAILURE;
        }
    }

    nmhSetCmnNew (FsCapwapSystemControl, 0, CapwapMainTaskLock,
                  CapwapMainTaskUnLock, 0, 0, 0, i4SetOption, "%i",
                  gCapwapGlobals.CapwapGlbMib.i4FsCapwapShutdown);

    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  CapwapGetFsCapwapControlUdpPort
 Input       :  The Indices
 Input       :  pu4FsCapwapControlUdpPort
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetFsCapwapControlUdpPort (UINT4 *pu4FsCapwapControlUdpPort)
{
#ifdef WTP_WANTED
    tWssIfCapDB        *pWssIfCapDB = NULL;

    CAPWAP_IF_CAP_DB_ALLOC (pWssIfCapDB);
    if (pWssIfCapDB == NULL)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Memory Allocation Failed\r\n");
        return SNMP_FAILURE;
    }

    MEMSET (pWssIfCapDB, 0, sizeof (tWssIfCapDB));

    pWssIfCapDB->CapwapIsGetAllDB.bCtrlUdpPort = OSIX_TRUE;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapDB) !=
        OSIX_SUCCESS)
    {
        *pu4FsCapwapControlUdpPort = CLI_UDP_PORT_DEF;
        CAPWAP_IF_CAP_DB_RELEASE (pWssIfCapDB);
        return OSIX_FAILURE;
    }
    if (pWssIfCapDB->u4CtrludpPort == 0)
    {
        *pu4FsCapwapControlUdpPort = CLI_UDP_PORT_DEF;
    }
    else
    {
        *pu4FsCapwapControlUdpPort = pWssIfCapDB->u4CtrludpPort;
    }
    CAPWAP_IF_CAP_DB_RELEASE (pWssIfCapDB);
#else
    *pu4FsCapwapControlUdpPort =
        gCapwapGlobals.CapwapGlbMib.u4FsCapwapControlUdpPort;

    if (*pu4FsCapwapControlUdpPort == 0)
    {
        *pu4FsCapwapControlUdpPort = CLI_UDP_PORT_DEF;
    }
#endif

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapSetFsCapwapControlUdpPort
 Input       :  u4FsCapwapControlUdpPort
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
CapwapSetFsCapwapControlUdpPort (UINT4 u4FsCapwapControlUdpPort)
{
    INT4                i4SetOption = SNMP_SUCCESS;
    UINT4               u4PrevCapwapCtrlUdpPort = 0;
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;
    tWssIfCapDB        *pWssIfCapDB = NULL;
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapDB, 0, sizeof (tWssIfCapDB));
    pWlcHdlrMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pWlcHdlrMsgStruct == NULL)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
        return OSIX_FAILURE;
    }

    MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));

    if (gCapwapGlobals.CapwapGlbMib.u4FsCapwapControlUdpPort ==
        u4FsCapwapControlUdpPort)
    {
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
        return OSIX_SUCCESS;
    }

    u4PrevCapwapCtrlUdpPort =
        gCapwapGlobals.CapwapGlbMib.u4FsCapwapControlUdpPort;
    gCapwapGlobals.CapwapGlbMib.u4FsCapwapControlUdpPort =
        u4FsCapwapControlUdpPort;

    nmhSetCmnNew (FsCapwapControlUdpPort, 0, CapwapMainTaskLock,
                  CapwapMainTaskUnLock, 0, 0, 0, i4SetOption, "%u",
                  gCapwapGlobals.CapwapGlbMib.u4FsCapwapControlUdpPort);

    if (u4PrevCapwapCtrlUdpPort != 0)
    {
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.isOptional =
            OSIX_TRUE;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.u2MsgEleType =
            CAPWAP_VEND_SPECIFIC_PAYLOAD_MSG_TYPE;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.u2MsgEleLen = 12;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.elementId =
            VENDOR_UDP_SERVER_PORT_MSG;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
            VendUdpPort.isOptional = OSIX_TRUE;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
            VendUdpPort.u2MsgEleType = UDP_SERVER_PORT_VENDOR_MSG;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
            VendUdpPort.u2MsgEleLen = 8;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
            VendUdpPort.vendorId = VENDOR_ID;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
            VendUdpPort.u4Val = u4FsCapwapControlUdpPort;

#ifdef WLC_WANTED
        if (WssIfProcessWlcHdlrMsg
            (WSS_WLCHDLR_CAPWAP_CONF_UPDATE_REQ,
             pWlcHdlrMsgStruct) == NO_AP_PRESENT)
        {
            printf ("\nNo AP Present during UDP Port Updation\n");

        }
#endif
    }
    MEMSET (pWssIfCapDB, 0, sizeof (tWssIfCapDB));

    /* coverity fix Begins */
    pWssIfCapDB->CapwapIsGetAllDB.bCtrlUdpPort = OSIX_TRUE;
    /* coverity fix Ends */
    pWssIfCapDB->u4CtrludpPort = u4FsCapwapControlUdpPort;

    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_SET_DB, pWssIfCapDB) !=
        OSIX_SUCCESS)
    {
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
        return OSIX_FAILURE;
    }
    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetFsCapwapControlChannelDTLSPolicyOptions
 Input       :  The Indices
 Input       :  pFsCapwapControlChannelDTLSPolicyOptions
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetFsCapwapControlChannelDTLSPolicyOptions (UINT1
                                                  *pFsCapwapControlChannelDTLSPolicyOptions)
{
    MEMCPY (pFsCapwapControlChannelDTLSPolicyOptions,
            gCapwapGlobals.CapwapGlbMib.
            au1FsCapwapControlChannelDTLSPolicyOptions,
            gCapwapGlobals.CapwapGlbMib.
            i4FsCapwapControlChannelDTLSPolicyOptionsLen);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapSetFsCapwapControlChannelDTLSPolicyOptions
 Input       :  pFsCapwapControlChannelDTLSPolicyOptions
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
CapwapSetFsCapwapControlChannelDTLSPolicyOptions (UINT1
                                                  *pFsCapwapControlChannelDTLSPolicyOptions)
{
    INT4                i4SetOption = SNMP_SUCCESS;

    MEMCPY (gCapwapGlobals.CapwapGlbMib.
            au1FsCapwapControlChannelDTLSPolicyOptions,
            pFsCapwapControlChannelDTLSPolicyOptions,
            gCapwapGlobals.CapwapGlbMib.
            i4FsCapwapControlChannelDTLSPolicyOptionsLen);

    nmhSetCmnNew (FsCapwapControlChannelDTLSPolicyOptions, 0,
                  CapwapMainTaskLock, CapwapMainTaskUnLock, 0, 0, 0,
                  i4SetOption, "%s",
                  gCapwapGlobals.CapwapGlbMib.
                  au1FsCapwapControlChannelDTLSPolicyOptions);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetFsCapwapDataChannelDTLSPolicyOptions
 Input       :  The Indices
 Input       :  pFsCapwapDataChannelDTLSPolicyOptions
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetFsCapwapDataChannelDTLSPolicyOptions (UINT1
                                               *pFsCapwapDataChannelDTLSPolicyOptions)
{
    MEMCPY (pFsCapwapDataChannelDTLSPolicyOptions,
            gCapwapGlobals.CapwapGlbMib.au1FsCapwapDataChannelDTLSPolicyOptions,
            gCapwapGlobals.CapwapGlbMib.
            i4FsCapwapDataChannelDTLSPolicyOptionsLen);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapSetFsCapwapDataChannelDTLSPolicyOptions
 Input       :  pFsCapwapDataChannelDTLSPolicyOptions
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
CapwapSetFsCapwapDataChannelDTLSPolicyOptions (UINT1
                                               *pFsCapwapDataChannelDTLSPolicyOptions)
{
    INT4                i4SetOption = SNMP_SUCCESS;

    MEMCPY (gCapwapGlobals.CapwapGlbMib.au1FsCapwapDataChannelDTLSPolicyOptions,
            pFsCapwapDataChannelDTLSPolicyOptions,
            gCapwapGlobals.CapwapGlbMib.
            i4FsCapwapDataChannelDTLSPolicyOptionsLen);

    nmhSetCmnNew (FsCapwapDataChannelDTLSPolicyOptions, 0, CapwapMainTaskLock,
                  CapwapMainTaskUnLock, 0, 0, 0, i4SetOption, "%s",
                  gCapwapGlobals.CapwapGlbMib.
                  au1FsCapwapDataChannelDTLSPolicyOptions);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetFsWlcDiscoveryMode
 Input       :  The Indices
 Input       :  pFsWlcDiscoveryMode
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetFsWlcDiscoveryMode (UINT1 *pFsWlcDiscoveryMode)
{
    *pFsWlcDiscoveryMode = gCapwapGlobals.CapwapGlbMib.au1FsWlcDiscoveryMode[0];
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapSetFsWlcDiscoveryMode
 Input       :  pFsWlcDiscoveryMode
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
CapwapSetFsWlcDiscoveryMode (UINT1 *pFsWlcDiscoveryMode)
{
    INT4                i4SetOption = SNMP_SUCCESS;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    gCapwapGlobals.CapwapGlbMib.au1FsWlcDiscoveryMode[0] = *pFsWlcDiscoveryMode;
    if (*pFsWlcDiscoveryMode == CLI_CAPWAP_DISC_MODE_MAC_AUTO)
    {
        pWssIfCapwapDB->CapwapIsGetAllDB.bDiscMode = OSIX_TRUE;
        pWssIfCapwapDB->u1DiscMode = AUTO_DISCOVERY_MODE;
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_SET_DB,
                                     pWssIfCapwapDB) == OSIX_FAILURE)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
    }
    else
    {
        pWssIfCapwapDB->CapwapIsGetAllDB.bDiscMode = OSIX_TRUE;
        pWssIfCapwapDB->u1DiscMode = MAC_DISCOVERY_MODE;
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_SET_DB,
                                     pWssIfCapwapDB) == OSIX_FAILURE)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }

    }

    nmhSetCmnNew (FsWlcDiscoveryMode, 0, CapwapMainTaskLock,
                  CapwapMainTaskUnLock, 0, 0, 0, i4SetOption, "%s",
                  gCapwapGlobals.CapwapGlbMib.au1FsWlcDiscoveryMode);

    gCapwapGlobals.CapwapGlbMib.i4FsWlcDiscoveryModeLen =
        (INT4) STRLEN (gCapwapGlobals.CapwapGlbMib.au1FsWlcDiscoveryMode);
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetFsCapwapWtpModeIgnore
 Input       :  The Indices
 Input       :  pi4FsCapwapWtpModeIgnore
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetFsCapwapWtpModeIgnore (INT4 *pi4FsCapwapWtpModeIgnore)
{
    *pi4FsCapwapWtpModeIgnore =
        gCapwapGlobals.CapwapGlbMib.i4FsCapwapWtpModeIgnore;

    if (*pi4FsCapwapWtpModeIgnore == 0)
    {
        gCapwapGlobals.CapwapGlbMib.i4FsCapwapWtpModeIgnore = CAPWAP_ENABLE;
        *pi4FsCapwapWtpModeIgnore = CAPWAP_ENABLE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapSetFsCapwapWtpModeIgnore
 Input       :  i4FsCapwapWtpModeIgnore
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
CapwapSetFsCapwapWtpModeIgnore (INT4 i4FsCapwapWtpModeIgnore)
{
    INT4                i4SetOption = SNMP_SUCCESS;
    tWssIfCapDB        *pWssIfCapDB = NULL;

    gCapwapGlobals.CapwapGlbMib.i4FsCapwapWtpModeIgnore =
        i4FsCapwapWtpModeIgnore;

    nmhSetCmnNew (FsCapwapWtpModeIgnore, 0, CapwapMainTaskLock,
                  CapwapMainTaskUnLock, 0, 0, 0, i4SetOption, "%i",
                  gCapwapGlobals.CapwapGlbMib.i4FsCapwapWtpModeIgnore);

    /*setting module db */

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapDB, 0, sizeof (tWssIfCapDB));
    pWssIfCapDB->CapwapIsGetAllDB.bModelCheck = OSIX_TRUE;

    pWssIfCapDB->CapwapGetDB.u1ModelCheck = (UINT1) i4FsCapwapWtpModeIgnore;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_SET_DB, pWssIfCapDB) !=
        OSIX_SUCCESS)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
        return SNMP_FAILURE;
    }

    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetFsCapwapDebugMask
 Input       :  The Indices
 Input       :  pi4FsCapwapDebugMask
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetFsCapwapDebugMask (INT4 *pi4FsCapwapDebugMask)
{
    *pi4FsCapwapDebugMask = (INT4) gu4CapwapDebugMask;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapSetFsCapwapDebugMask
 Input       :  i4FsCapwapDebugMask
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
CapwapSetFsCapwapDebugMask (INT4 i4FsCapwapDebugMask)
{
    INT4                i4SetOption = SNMP_SUCCESS;

    if (gCapwapGlobals.CapwapGlbMib.i4FsCapwapDebugMask == i4FsCapwapDebugMask)
    {
        return OSIX_SUCCESS;
    }

    gCapwapGlobals.CapwapGlbMib.i4FsCapwapDebugMask = i4FsCapwapDebugMask;

    nmhSetCmnNew (FsCapwapDebugMask, 0, CapwapMainTaskLock,
                  CapwapMainTaskUnLock, 0, 0, 0, i4SetOption, "%i",
                  gCapwapGlobals.CapwapGlbMib.i4FsCapwapDebugMask);
    gu4CapwapDebugMask = (UINT4) i4FsCapwapDebugMask;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetFsDtlsDebugMask
 Input       :  The Indices
 Input       :  pi4FsDtlsDebugMask
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetFsDtlsDebugMask (INT4 *pi4FsDtlsDebugMask)
{
    *pi4FsDtlsDebugMask = gCapwapGlobals.CapwapGlbMib.i4FsDtlsDebugMask;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapSetFsDtlsDebugMask
 Input       :  i4FsDtlsDebugMask
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
CapwapSetFsDtlsDebugMask (INT4 i4FsDtlsDebugMask)
{
    INT4                i4SetOption = SNMP_SUCCESS;

    gCapwapGlobals.CapwapGlbMib.i4FsDtlsDebugMask = i4FsDtlsDebugMask;

    nmhSetCmnNew (FsDtlsDebugMask, 0, CapwapMainTaskLock, CapwapMainTaskUnLock,
                  0, 0, 0, i4SetOption, "%i",
                  gCapwapGlobals.CapwapGlbMib.i4FsDtlsDebugMask);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetFsDtlsEncryption
 Input       :  The Indices
 Input       :  pi4FsDtlsEncryption
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetFsDtlsEncryption (INT4 *pi4FsDtlsEncryption)
{
    *pi4FsDtlsEncryption = gCapwapGlobals.CapwapGlbMib.i4FsDtlsEncryption;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapSetFsDtlsEncryption
 Input       :  i4FsDtlsEncryption
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
CapwapSetFsDtlsEncryption (INT4 i4FsDtlsEncryption)
{
    INT4                i4SetOption = SNMP_SUCCESS;

    gCapwapGlobals.CapwapGlbMib.i4FsDtlsEncryption = i4FsDtlsEncryption;

    nmhSetCmnNew (FsDtlsEncryption, 0, CapwapMainTaskLock, CapwapMainTaskUnLock,
                  0, 0, 0, i4SetOption, "%i",
                  gCapwapGlobals.CapwapGlbMib.i4FsDtlsEncryption);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetFsDtlsEncryptAlogritham
 Input       :  The Indices
 Input       :  pi4FsDtlsEncryptAlogritham
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetFsDtlsEncryptAlogritham (INT4 *pi4FsDtlsEncryptAlogritham)
{
    *pi4FsDtlsEncryptAlogritham =
        gCapwapGlobals.CapwapGlbMib.i4FsDtlsEncryptAlogritham;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapSetFsDtlsEncryptAlogritham
 Input       :  i4FsDtlsEncryptAlogritham
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
CapwapSetFsDtlsEncryptAlogritham (INT4 i4FsDtlsEncryptAlogritham)
{
    INT4                i4SetOption = SNMP_SUCCESS;

    gCapwapGlobals.CapwapGlbMib.i4FsDtlsEncryptAlogritham =
        i4FsDtlsEncryptAlogritham;

    nmhSetCmnNew (FsDtlsEncryptAlgorithm, 0, CapwapMainTaskLock,
                  CapwapMainTaskUnLock, 0, 0, 0, i4SetOption, "%i",
                  gCapwapGlobals.CapwapGlbMib.i4FsDtlsEncryptAlogritham);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetFsStationType
 Input       :  The Indices
 Input       :  pi4FsStationType
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetFsStationType (INT4 *pi4FsStationType)
{
    *pi4FsStationType = gCapwapGlobals.CapwapGlbMib.i4FsStationType;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetFsAcTimestampTrigger
 Input       :  The Indices
 Input       :  pi4FsStationType
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetFsAcTimestampTrigger (INT4 *pi4RetValFsAcTimestampTrigger)
{
    *pi4RetValFsAcTimestampTrigger =
        gCapwapGlobals.CapwapGlbMib.i4FsAcTimestampTrigger;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapSetFsStationType
 Input       :  i4FsStationType
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
CapwapSetFsStationType (INT4 i4FsStationType)
{
#ifdef WLC_WANTED
    INT4                i4SetOption = SNMP_SUCCESS;
    tWssStaMsgStruct    WssStaMsgStruct;

    MEMSET (&WssStaMsgStruct, 0, sizeof (tWssStaMsgStruct));
    gCapwapGlobals.CapwapGlbMib.i4FsStationType = i4FsStationType;

    nmhSetCmnNew (FsStationType, 0, CapwapMainTaskLock, CapwapMainTaskUnLock, 0,
                  0, 0, i4SetOption, "%i",
                  gCapwapGlobals.CapwapGlbMib.i4FsStationType);
    if (i4FsStationType != CLI_CAPWAP_STA_DIS_MODE_AUTO)
    {
        WssStaMsgStruct.unAuthMsg.AuthDelStation.u1StationType =
            (UINT1) i4FsStationType;
        WssIfProcessWssStaMsg (WSS_STA_DISCOVERY_MODE, &WssStaMsgStruct);
    }
#else
    UNUSED_PARAM (i4FsStationType);
#endif
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapSetFsAcTimestampTrigger
 Input       :  i4FsAcTimestampTrigger
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
CapwapSetFsAcTimestampTrigger (INT4 i4FsAcTimestampTrigger)
{
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;
    tUtlTm              tm;
    INT4                i4SetOption = SNMP_SUCCESS;
    UINT1               u1MaxWtps = 0;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4currentProfileId = 0;
    INT4                i4retVal = 0;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&tm, 0, sizeof (tUtlTm));
    pWlcHdlrMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    UNUSED_PARAM (i4FsAcTimestampTrigger);
    if (pWlcHdlrMsgStruct == NULL)
    {
        return OSIX_FAILURE;
    }

    MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));

    gCapwapGlobals.CapwapGlbMib.i4FsAcTimestampTrigger = CLI_FSCAPWAP_ENABLE;
    nmhSetCmnNew (FsAcTimestampTrigger, 0, CapwapMainTaskLock,
                  CapwapMainTaskUnLock, 0, 0, 0, i4SetOption, "%i",
                  gCapwapGlobals.CapwapGlbMib.i4FsAcTimestampTrigger);
    pWlcHdlrMsgStruct->ClkiwfConfigUpdateReq.acTimestamp.isOptional = OSIX_TRUE;
    pWlcHdlrMsgStruct->ClkiwfConfigUpdateReq.acTimestamp.u2MsgEleType =
        CAPWAP_AC_TIMESTAMP_MSG_TYPE;
    pWlcHdlrMsgStruct->ClkiwfConfigUpdateReq.acTimestamp.u2MsgEleLen = 4;
    i4retVal = nmhGetFirstIndexCapwapBaseWtpProfileTable (&u4WtpProfileId);
    if (!i4retVal)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;
    }
    do
    {
        u4currentProfileId = u4WtpProfileId;
        pWssIfCapwapDB->CapwapGetDB.u2WtpProfileId = u4WtpProfileId;
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpProfileId = OSIX_TRUE;
        pWssIfCapwapDB->CapwapIsGetAllDB.bActiveWtps = OSIX_TRUE;
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB) !=
            OSIX_SUCCESS)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            return OSIX_FAILURE;
        }
        u1MaxWtps = (UINT1) pWssIfCapwapDB->u2ActiveWtps;

        pWlcHdlrMsgStruct->ClkiwfConfigUpdateReq.u4SessId =
            pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId;

#ifdef SNTP_WANTED
#ifdef CLKIWF_WANTED
        /*Call ClkIwf API to SetTime */
        if (ClkIwfGetClock (CLK_PROTO_SYS, (VOID *) &tm) == OSIX_FAILURE)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            return OSIX_FAILURE;
        }
#else
        UtlGetTime (&tm);
#endif
        pWlcHdlrMsgStruct->ClkiwfConfigUpdateReq.acTimestamp.u4Timestamp =
            SntpTmToSec (&tm);
#else
        UtlGetTime (&tm);
        pWlcHdlrMsgStruct->ClkiwfConfigUpdateReq.acTimestamp.u4Timestamp =
            CapwapTmToSec (&tm);
#endif

#ifdef WLCHDLR_WANTED
        if (WssIfProcessWlcHdlrMsg
            (WSS_WLCHDLR_CLKIWF_CONF_UPDATE_REQ,
             pWlcHdlrMsgStruct) == OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "\nNo AP Present during WTP timestamp Updation\n");

        }
#endif
    }
    while (nmhGetNextIndexCapwapBaseWtpProfileTable
           (u4currentProfileId, &u4WtpProfileId) == SNMP_SUCCESS);

    gCapwapGlobals.CapwapGlbMib.i4FsAcTimestampTrigger = CLI_FSCAPWAP_DISABLE;
    nmhSetCmnNew (FsAcTimestampTrigger, 0, CapwapMainTaskLock,
                  CapwapMainTaskUnLock, 0, 0, 0, i4SetOption, "%i",
                  gCapwapGlobals.CapwapGlbMib.i4FsAcTimestampTrigger);

    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);

    UNUSED_PARAM (u1MaxWtps);
    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  CapwapGetFirstFsWtpModelTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pCapwapFsWtpModelEntry or NULL
****************************************************************************/
tCapwapFsWtpModelEntry *
CapwapGetFirstFsWtpModelTable ()
{
    tCapwapFsWtpModelEntry *pCapwapFsWtpModelEntry = NULL;

    pCapwapFsWtpModelEntry =
        (tCapwapFsWtpModelEntry *) RBTreeGetFirst (gCapwapGlobals.CapwapGlbMib.
                                                   FsWtpModelTable);

    return pCapwapFsWtpModelEntry;
}

/****************************************************************************
 Function    :  CapwapGetNextFsWtpModelTable
 Input       :  pCurrentCapwapFsWtpModelEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextCapwapFsWtpModelEntry or NULL
****************************************************************************/
tCapwapFsWtpModelEntry *
CapwapGetNextFsWtpModelTable (tCapwapFsWtpModelEntry *
                              pCurrentCapwapFsWtpModelEntry)
{
    tCapwapFsWtpModelEntry *pNextCapwapFsWtpModelEntry = NULL;

    pNextCapwapFsWtpModelEntry =
        (tCapwapFsWtpModelEntry *) RBTreeGetNext (gCapwapGlobals.CapwapGlbMib.
                                                  FsWtpModelTable,
                                                  (tRBElem *)
                                                  pCurrentCapwapFsWtpModelEntry,
                                                  NULL);

    return pNextCapwapFsWtpModelEntry;
}

/****************************************************************************
 Function    :  CapwapGetFsWtpModelTable
 Input       :  pCapwapFsWtpModelEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetCapwapFsWtpModelEntry or NULL
****************************************************************************/
tCapwapFsWtpModelEntry *
CapwapGetFsWtpModelTable (tCapwapFsWtpModelEntry * pCapwapFsWtpModelEntry)
{
    tCapwapFsWtpModelEntry *pGetCapwapFsWtpModelEntry = NULL;

    pGetCapwapFsWtpModelEntry =
        (tCapwapFsWtpModelEntry *) RBTreeGet (gCapwapGlobals.CapwapGlbMib.
                                              FsWtpModelTable,
                                              (tRBElem *)
                                              pCapwapFsWtpModelEntry);

    return pGetCapwapFsWtpModelEntry;
}

/****************************************************************************
 Function    :  CapwapGetFirstFsWtpRadioTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pCapwapFsWtpRadioEntry or NULL
****************************************************************************/
tCapwapFsWtpRadioEntry *
CapwapGetFirstFsWtpRadioTable ()
{
    tCapwapFsWtpRadioEntry *pCapwapFsWtpRadioEntry = NULL;

    pCapwapFsWtpRadioEntry =
        (tCapwapFsWtpRadioEntry *) RBTreeGetFirst (gCapwapGlobals.CapwapGlbMib.
                                                   FsWtpRadioTable);

    return pCapwapFsWtpRadioEntry;
}

/****************************************************************************
 Function    :  CapwapGetNextFsWtpRadioTable
 Input       :  pCurrentCapwapFsWtpRadioEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextCapwapFsWtpRadioEntry or NULL
****************************************************************************/
tCapwapFsWtpRadioEntry *
CapwapGetNextFsWtpRadioTable (tCapwapFsWtpRadioEntry *
                              pCurrentCapwapFsWtpRadioEntry)
{
    tCapwapFsWtpRadioEntry *pNextCapwapFsWtpRadioEntry = NULL;

    pNextCapwapFsWtpRadioEntry =
        (tCapwapFsWtpRadioEntry *) RBTreeGetNext (gCapwapGlobals.CapwapGlbMib.
                                                  FsWtpRadioTable,
                                                  (tRBElem *)
                                                  pCurrentCapwapFsWtpRadioEntry,
                                                  NULL);

    return pNextCapwapFsWtpRadioEntry;
}

/****************************************************************************
 Function    :  CapwapGetFsWtpRadioTable
 Input       :  pCapwapFsWtpRadioEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetCapwapFsWtpRadioEntry or NULL
****************************************************************************/
tCapwapFsWtpRadioEntry *
CapwapGetFsWtpRadioTable (tCapwapFsWtpRadioEntry * pCapwapFsWtpRadioEntry)
{
    tCapwapFsWtpRadioEntry *pGetCapwapFsWtpRadioEntry = NULL;

    pGetCapwapFsWtpRadioEntry =
        (tCapwapFsWtpRadioEntry *) RBTreeGet (gCapwapGlobals.CapwapGlbMib.
                                              FsWtpRadioTable,
                                              (tRBElem *)
                                              pCapwapFsWtpRadioEntry);

    return pGetCapwapFsWtpRadioEntry;
}

/****************************************************************************
 Function    :  CapwapGetFirstFsCapwapWhiteListTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pCapwapFsCapwapWhiteListEntry or NULL
****************************************************************************/
tCapwapFsCapwapWhiteListEntry *
CapwapGetFirstFsCapwapWhiteListTable ()
{
    tCapwapFsCapwapWhiteListEntry *pCapwapFsCapwapWhiteListEntry = NULL;

    pCapwapFsCapwapWhiteListEntry =
        (tCapwapFsCapwapWhiteListEntry *) RBTreeGetFirst (gCapwapGlobals.
                                                          CapwapGlbMib.
                                                          FsCapwapWhiteListTable);

    return pCapwapFsCapwapWhiteListEntry;
}

/****************************************************************************
 Function    :  CapwapGetNextFsCapwapWhiteListTable
 Input       :  pCurrentCapwapFsCapwapWhiteListEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextCapwapFsCapwapWhiteListEntry or NULL
****************************************************************************/
tCapwapFsCapwapWhiteListEntry *
CapwapGetNextFsCapwapWhiteListTable (tCapwapFsCapwapWhiteListEntry *
                                     pCurrentCapwapFsCapwapWhiteListEntry)
{
    tCapwapFsCapwapWhiteListEntry *pNextCapwapFsCapwapWhiteListEntry = NULL;

    pNextCapwapFsCapwapWhiteListEntry =
        (tCapwapFsCapwapWhiteListEntry *) RBTreeGetNext (gCapwapGlobals.
                                                         CapwapGlbMib.
                                                         FsCapwapWhiteListTable,
                                                         (tRBElem *)
                                                         pCurrentCapwapFsCapwapWhiteListEntry,
                                                         NULL);

    return pNextCapwapFsCapwapWhiteListEntry;
}

/****************************************************************************
 Function    :  CapwapGetFsCapwapWhiteListTable
 Input       :  pCapwapFsCapwapWhiteListEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetCapwapFsCapwapWhiteListEntry or NULL
****************************************************************************/
tCapwapFsCapwapWhiteListEntry *
CapwapGetFsCapwapWhiteListTable (tCapwapFsCapwapWhiteListEntry *
                                 pCapwapFsCapwapWhiteListEntry)
{
    tCapwapFsCapwapWhiteListEntry *pGetCapwapFsCapwapWhiteListEntry = NULL;

    pGetCapwapFsCapwapWhiteListEntry =
        (tCapwapFsCapwapWhiteListEntry *) RBTreeGet (gCapwapGlobals.
                                                     CapwapGlbMib.
                                                     FsCapwapWhiteListTable,
                                                     (tRBElem *)
                                                     pCapwapFsCapwapWhiteListEntry);

    return pGetCapwapFsCapwapWhiteListEntry;
}

/****************************************************************************
 Function    :  CapwapGetFirstFsCapwapBlackList
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pCapwapFsCapwapBlackListEntry or NULL
****************************************************************************/
tCapwapFsCapwapBlackListEntry *
CapwapGetFirstFsCapwapBlackList ()
{
    tCapwapFsCapwapBlackListEntry *pCapwapFsCapwapBlackListEntry = NULL;

    pCapwapFsCapwapBlackListEntry =
        (tCapwapFsCapwapBlackListEntry *) RBTreeGetFirst (gCapwapGlobals.
                                                          CapwapGlbMib.
                                                          FsCapwapBlackList);

    return pCapwapFsCapwapBlackListEntry;
}

/****************************************************************************
 Function    :  CapwapGetNextFsCapwapBlackList
 Input       :  pCurrentCapwapFsCapwapBlackListEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextCapwapFsCapwapBlackListEntry or NULL
****************************************************************************/
tCapwapFsCapwapBlackListEntry *
CapwapGetNextFsCapwapBlackList (tCapwapFsCapwapBlackListEntry *
                                pCurrentCapwapFsCapwapBlackListEntry)
{
    tCapwapFsCapwapBlackListEntry *pNextCapwapFsCapwapBlackListEntry = NULL;

    pNextCapwapFsCapwapBlackListEntry =
        (tCapwapFsCapwapBlackListEntry *) RBTreeGetNext (gCapwapGlobals.
                                                         CapwapGlbMib.
                                                         FsCapwapBlackList,
                                                         (tRBElem *)
                                                         pCurrentCapwapFsCapwapBlackListEntry,
                                                         NULL);

    return pNextCapwapFsCapwapBlackListEntry;
}

/****************************************************************************
 Function    :  CapwapGetFsCapwapBlackList
 Input       :  pCapwapFsCapwapBlackListEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetCapwapFsCapwapBlackListEntry or NULL
****************************************************************************/
tCapwapFsCapwapBlackListEntry *
CapwapGetFsCapwapBlackList (tCapwapFsCapwapBlackListEntry *
                            pCapwapFsCapwapBlackListEntry)
{
    tCapwapFsCapwapBlackListEntry *pGetCapwapFsCapwapBlackListEntry = NULL;

    pGetCapwapFsCapwapBlackListEntry =
        (tCapwapFsCapwapBlackListEntry *) RBTreeGet (gCapwapGlobals.
                                                     CapwapGlbMib.
                                                     FsCapwapBlackList,
                                                     (tRBElem *)
                                                     pCapwapFsCapwapBlackListEntry);

    return pGetCapwapFsCapwapBlackListEntry;
}

/****************************************************************************
 Function    :  CapwapGetFirstFsCapwapWtpConfigTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pCapwapFsCapwapWtpConfigEntry or NULL
****************************************************************************/
tCapwapFsCapwapWtpConfigEntry *
CapwapGetFirstFsCapwapWtpConfigTable ()
{
    tCapwapFsCapwapWtpConfigEntry *pCapwapFsCapwapWtpConfigEntry = NULL;

    pCapwapFsCapwapWtpConfigEntry =
        (tCapwapFsCapwapWtpConfigEntry *) RBTreeGetFirst (gCapwapGlobals.
                                                          CapwapGlbMib.
                                                          FsCapwapWtpConfigTable);

    return pCapwapFsCapwapWtpConfigEntry;
}

/****************************************************************************
 Function    :  CapwapGetNextFsCapwapWtpConfigTable
 Input       :  pCurrentCapwapFsCapwapWtpConfigEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextCapwapFsCapwapWtpConfigEntry or NULL
****************************************************************************/
tCapwapFsCapwapWtpConfigEntry *
CapwapGetNextFsCapwapWtpConfigTable (tCapwapFsCapwapWtpConfigEntry *
                                     pCurrentCapwapFsCapwapWtpConfigEntry)
{
    tCapwapFsCapwapWtpConfigEntry *pNextCapwapFsCapwapWtpConfigEntry = NULL;

    pNextCapwapFsCapwapWtpConfigEntry =
        (tCapwapFsCapwapWtpConfigEntry *) RBTreeGetNext (gCapwapGlobals.
                                                         CapwapGlbMib.
                                                         FsCapwapWtpConfigTable,
                                                         (tRBElem *)
                                                         pCurrentCapwapFsCapwapWtpConfigEntry,
                                                         NULL);

    return pNextCapwapFsCapwapWtpConfigEntry;
}

/****************************************************************************
 Function    :  CapwapGetFsCapwapWtpConfigTable
 Input       :  pCapwapFsCapwapWtpConfigEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetCapwapFsCapwapWtpConfigEntry or NULL
****************************************************************************/
tCapwapFsCapwapWtpConfigEntry *
CapwapGetFsCapwapWtpConfigTable (tCapwapFsCapwapWtpConfigEntry *
                                 pCapwapFsCapwapWtpConfigEntry)
{
    tCapwapFsCapwapWtpConfigEntry *pGetCapwapFsCapwapWtpConfigEntry = NULL;

    pGetCapwapFsCapwapWtpConfigEntry =
        (tCapwapFsCapwapWtpConfigEntry *) RBTreeGet (gCapwapGlobals.
                                                     CapwapGlbMib.
                                                     FsCapwapWtpConfigTable,
                                                     (tRBElem *)
                                                     pCapwapFsCapwapWtpConfigEntry);

    return pGetCapwapFsCapwapWtpConfigEntry;
}

/****************************************************************************
 Function    :  CapwapGetFirstFsCapwapLinkEncryptionTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pCapwapFsCapwapLinkEncryptionEntry or NULL
****************************************************************************/
tCapwapFsCapwapLinkEncryptionEntry *
CapwapGetFirstFsCapwapLinkEncryptionTable ()
{
    tCapwapFsCapwapLinkEncryptionEntry *pCapwapFsCapwapLinkEncryptionEntry =
        NULL;

    pCapwapFsCapwapLinkEncryptionEntry =
        (tCapwapFsCapwapLinkEncryptionEntry *) RBTreeGetFirst (gCapwapGlobals.
                                                               CapwapGlbMib.
                                                               FsCapwapLinkEncryptionTable);

    return pCapwapFsCapwapLinkEncryptionEntry;
}

/****************************************************************************
 Function    :  CapwapGetNextFsCapwapLinkEncryptionTable
 Input       :  pCurrentCapwapFsCapwapLinkEncryptionEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextCapwapFsCapwapLinkEncryptionEntry or NULL
****************************************************************************/
tCapwapFsCapwapLinkEncryptionEntry *
CapwapGetNextFsCapwapLinkEncryptionTable (tCapwapFsCapwapLinkEncryptionEntry *
                                          pCurrentCapwapFsCapwapLinkEncryptionEntry)
{
    tCapwapFsCapwapLinkEncryptionEntry *pNextCapwapFsCapwapLinkEncryptionEntry =
        NULL;

    pNextCapwapFsCapwapLinkEncryptionEntry =
        (tCapwapFsCapwapLinkEncryptionEntry *) RBTreeGetNext (gCapwapGlobals.
                                                              CapwapGlbMib.
                                                              FsCapwapLinkEncryptionTable,
                                                              (tRBElem *)
                                                              pCurrentCapwapFsCapwapLinkEncryptionEntry,
                                                              NULL);

    return pNextCapwapFsCapwapLinkEncryptionEntry;
}

/****************************************************************************
 Function    :  CapwapGetFsCapwapLinkEncryptionTable
 Input       :  pCapwapFsCapwapLinkEncryptionEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetCapwapFsCapwapLinkEncryptionEntry or NULL
****************************************************************************/
tCapwapFsCapwapLinkEncryptionEntry *
CapwapGetFsCapwapLinkEncryptionTable (tCapwapFsCapwapLinkEncryptionEntry *
                                      pCapwapFsCapwapLinkEncryptionEntry)
{
    tCapwapFsCapwapLinkEncryptionEntry *pGetCapwapFsCapwapLinkEncryptionEntry =
        NULL;

    pGetCapwapFsCapwapLinkEncryptionEntry =
        (tCapwapFsCapwapLinkEncryptionEntry *) RBTreeGet (gCapwapGlobals.
                                                          CapwapGlbMib.
                                                          FsCapwapLinkEncryptionTable,
                                                          (tRBElem *)
                                                          pCapwapFsCapwapLinkEncryptionEntry);

    return pGetCapwapFsCapwapLinkEncryptionEntry;
}

/****************************************************************************
 Function    :  CapwapGetFirstFsCawapDefaultWtpProfileTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pCapwapFsCapwapDefaultWtpProfileEntry or NULL
****************************************************************************/
tCapwapFsCapwapDefaultWtpProfileEntry *
CapwapGetFirstFsCawapDefaultWtpProfileTable ()
{
    tCapwapFsCapwapDefaultWtpProfileEntry *pCapwapFsCapwapDefaultWtpProfileEntry
        = NULL;

    pCapwapFsCapwapDefaultWtpProfileEntry =
        (tCapwapFsCapwapDefaultWtpProfileEntry *)
        RBTreeGetFirst (gCapwapGlobals.CapwapGlbMib.
                        FsCawapDefaultWtpProfileTable);

    return pCapwapFsCapwapDefaultWtpProfileEntry;
}

/****************************************************************************
 Function    :  CapwapGetFirstCapwapBaseWtpEventsStatsTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  SNMP_SUCCESS 
****************************************************************************/
tWssIfProfileMacMap *
CapwapGetFirstCapwapBaseWtpEventsStatsTable (tSNMP_OCTET_STRING_TYPE
                                             * pCapwapBaseWtpCurrId)
{

    tWssIfProfileMacMap *pCapwapBaseWtpEventsStatsTable = NULL;
    pCapwapBaseWtpEventsStatsTable =
        (tWssIfProfileMacMap *) RBTreeGetFirst (gCapProfileMacMapTable);

    if (pCapwapBaseWtpEventsStatsTable == NULL)
    {
        return pCapwapBaseWtpEventsStatsTable;
    }

    /* Assign the index */
    pCapwapBaseWtpCurrId->i4_Length = MAC_ADDR_LEN;
    MEMCPY (pCapwapBaseWtpCurrId->pu1_OctetList,
            pCapwapBaseWtpEventsStatsTable->MacAddr,
            pCapwapBaseWtpCurrId->i4_Length);

    return pCapwapBaseWtpEventsStatsTable;
}

/****************************************************************************
 Function    :  CapwapGetNextCapwapBaseWtpEventsStatsTable
 Input       :  NONE
 Description :  This Routine is used to take
                Next index from a table
 Returns     :  SNMP_SUCCESS 
****************************************************************************/
tWssIfProfileMacMap *
CapwapGetNextCapwapBaseWtpEventsStatsTable (tSNMP_OCTET_STRING_TYPE
                                            * pCapwapBaseWtpCurrId,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pNextCapwapBaseWtpCurrId)
{

    tWssIfProfileMacMap *pCapwapBaseWtpEventsStatsTable = NULL;
    tWssIfProfileMacMap CapwapWtpCurrId;

    MEMSET (&CapwapWtpCurrId, 0, sizeof (tWssIfProfileMacMap));
    MEMCPY (CapwapWtpCurrId.MacAddr, pCapwapBaseWtpCurrId->pu1_OctetList,
            MAC_ADDR_LEN);

    pCapwapBaseWtpEventsStatsTable =
        ((tWssIfProfileMacMap *)
         RBTreeGetNext (gCapProfileMacMapTable, (tRBElem *) & CapwapWtpCurrId,
                        NULL));

    if (pCapwapBaseWtpEventsStatsTable == NULL)
    {
        return pCapwapBaseWtpEventsStatsTable;
    }

    pNextCapwapBaseWtpCurrId->i4_Length = MAC_ADDR_LEN;
    MEMCPY (pNextCapwapBaseWtpCurrId->pu1_OctetList,
            pCapwapBaseWtpEventsStatsTable->MacAddr,
            pNextCapwapBaseWtpCurrId->i4_Length);

    return pCapwapBaseWtpEventsStatsTable;
}

/****************************************************************************
 Function    :  CapwapGetFirstCapwapBaseRadioEventsStatsTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  SNMP_SUCCESS 
****************************************************************************/
tWssIfProfileMacMap *
CapwapGetFirstCapwapBaseRadioEventsStatsTable (tSNMP_OCTET_STRING_TYPE
                                               * pCapwapBaseWtpCurrId,
                                               UINT4
                                               *pu4CapwapBaseRadioEventsWtpRadioId)
{

    tWssIfProfileMacMap *pCapwapBaseRadioEventsStatsTable = NULL;
    pCapwapBaseRadioEventsStatsTable =
        (tWssIfProfileMacMap *) RBTreeGetFirst (gCapProfileMacMapTable);

    if (pCapwapBaseRadioEventsStatsTable == NULL)
    {
        return pCapwapBaseRadioEventsStatsTable;
    }

    /* Assign the index */
    pCapwapBaseWtpCurrId->i4_Length = MAC_ADDR_LEN;

    MEMCPY (pCapwapBaseWtpCurrId->pu1_OctetList,
            pCapwapBaseRadioEventsStatsTable->MacAddr,
            pCapwapBaseWtpCurrId->i4_Length);

    *pu4CapwapBaseRadioEventsWtpRadioId = 1;
    return pCapwapBaseRadioEventsStatsTable;
}

/****************************************************************************
 Function    :  CapwapGetNextCapwapBaseRadioEventsStatsTable
 Input       :  NONE
 Description :  This Routine is used to take
                Next index from a table
 Returns     :  SNMP_SUCCESS 
****************************************************************************/
INT1
CapwapGetNextCapwapBaseRadioEventsStatsTable (tSNMP_OCTET_STRING_TYPE
                                              * pCapwapBaseWtpCurrId,
                                              tSNMP_OCTET_STRING_TYPE *
                                              pNextCapwapBaseWtpCurrId,
                                              UINT4
                                              u4CapwapBaseRadioEventsWtpRadioId,
                                              UINT4
                                              *pu4NextCapwapBaseRadioEventsWtpRadioId)
{

    tWssIfProfileMacMap *pCapwapBaseRadioEventsStatsTable = NULL;
    tWssIfProfileMacMap CapwapWtpCurrId;
    tWssIfCapDB        *pWssIfCapDB = NULL;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapDB, SNMP_FAILURE);
    tCapwapFsCapwapWirelessBindingEntry
        * pNextCapwapFsCapwapWirelessBindingEntry = NULL;
    tCapwapFsCapwapWirelessBindingEntry CapwapFsCapwapWirelessBindingEntry;

    MEMSET (&CapwapFsCapwapWirelessBindingEntry, 0,
            sizeof (tCapwapFsCapwapWirelessBindingEntry));

    MEMSET (&CapwapWtpCurrId, 0, sizeof (tWssIfProfileMacMap));
    MEMCPY (CapwapWtpCurrId.MacAddr, pCapwapBaseWtpCurrId->pu1_OctetList,
            MAC_ADDR_LEN);
    /* To get next entry from RB Tree */
    pCapwapBaseRadioEventsStatsTable =
        ((tWssIfProfileMacMap *)
         RBTreeGet (gCapProfileMacMapTable, (tRBElem *) & CapwapWtpCurrId));

    if (pCapwapBaseRadioEventsStatsTable != NULL)
    {
        /* To get the Radio ID for next entry */
        CapwapFsCapwapWirelessBindingEntry.MibObject.u4CapwapBaseWtpProfileId =
            pCapwapBaseRadioEventsStatsTable->u2WtpProfileId;
        CapwapFsCapwapWirelessBindingEntry.MibObject.
            u4CapwapBaseWirelessBindingRadioId =
            u4CapwapBaseRadioEventsWtpRadioId;

        pNextCapwapFsCapwapWirelessBindingEntry =
            CapwapGetNextFsCapwapWirelessBindingTable
            (&CapwapFsCapwapWirelessBindingEntry);

        if (pNextCapwapFsCapwapWirelessBindingEntry == NULL)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
            return SNMP_FAILURE;
        }
        else
        {
            if (pCapwapBaseRadioEventsStatsTable->u2WtpProfileId ==
                pNextCapwapFsCapwapWirelessBindingEntry->MibObject.
                u4CapwapBaseWtpProfileId)
            {
                pNextCapwapBaseWtpCurrId->i4_Length = MAC_ADDR_LEN;
                MEMCPY (pNextCapwapBaseWtpCurrId->pu1_OctetList,
                        pCapwapBaseRadioEventsStatsTable->MacAddr,
                        pNextCapwapBaseWtpCurrId->i4_Length);
            }
            else
            {
                pWssIfCapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                pWssIfCapDB->CapwapIsGetAllDB.bWtpProfileId = OSIX_TRUE;
                pWssIfCapDB->CapwapGetDB.u2ProfileId =
                    (UINT2) pNextCapwapFsCapwapWirelessBindingEntry->MibObject.
                    u4CapwapBaseWtpProfileId;
                if (WssIfProcessCapwapDBMsg
                    (WSS_CAPWAP_GET_INTERNAL_ID, pWssIfCapDB) != OSIX_SUCCESS)
                {
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    return SNMP_FAILURE;
                }
                pWssIfCapDB->CapwapIsGetAllDB.bWtpMacAddress = OSIX_TRUE;
                pWssIfCapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapDB) !=
                    OSIX_SUCCESS)
                {
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    return SNMP_FAILURE;
                }
                pNextCapwapBaseWtpCurrId->i4_Length = MAC_ADDR_LEN;
                MEMCPY (pNextCapwapBaseWtpCurrId->pu1_OctetList,
                        pWssIfCapDB->CapwapGetDB.WtpMacAddress, MAC_ADDR_LEN);
            }
            *pu4NextCapwapBaseRadioEventsWtpRadioId =
                pNextCapwapFsCapwapWirelessBindingEntry->MibObject.
                u4CapwapBaseWirelessBindingRadioId;
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
            return SNMP_SUCCESS;
        }
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  CapwapGetNextFsCawapDefaultWtpProfileTable
 Input       :  pCurrentCapwapFsCapwapDefaultWtpProfileEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextCapwapFsCapwapDefaultWtpProfileEntry or NULL
****************************************************************************/
tCapwapFsCapwapDefaultWtpProfileEntry
    * CapwapGetNextFsCawapDefaultWtpProfileTable
    (tCapwapFsCapwapDefaultWtpProfileEntry *
     pCurrentCapwapFsCapwapDefaultWtpProfileEntry)
{
    tCapwapFsCapwapDefaultWtpProfileEntry
        * pNextCapwapFsCapwapDefaultWtpProfileEntry = NULL;

    pNextCapwapFsCapwapDefaultWtpProfileEntry =
        (tCapwapFsCapwapDefaultWtpProfileEntry *) RBTreeGetNext (gCapwapGlobals.
                                                                 CapwapGlbMib.
                                                                 FsCawapDefaultWtpProfileTable,
                                                                 (tRBElem *)
                                                                 pCurrentCapwapFsCapwapDefaultWtpProfileEntry,
                                                                 NULL);

    return pNextCapwapFsCapwapDefaultWtpProfileEntry;
}

/****************************************************************************
 Function    :  CapwapGetFsCawapDefaultWtpProfileTable
 Input       :  pCapwapFsCapwapDefaultWtpProfileEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetCapwapFsCapwapDefaultWtpProfileEntry or NULL
****************************************************************************/
tCapwapFsCapwapDefaultWtpProfileEntry *
CapwapGetFsCawapDefaultWtpProfileTable (tCapwapFsCapwapDefaultWtpProfileEntry *
                                        pCapwapFsCapwapDefaultWtpProfileEntry)
{
    tCapwapFsCapwapDefaultWtpProfileEntry
        * pGetCapwapFsCapwapDefaultWtpProfileEntry = NULL;

    pGetCapwapFsCapwapDefaultWtpProfileEntry =
        (tCapwapFsCapwapDefaultWtpProfileEntry *) RBTreeGet (gCapwapGlobals.
                                                             CapwapGlbMib.
                                                             FsCawapDefaultWtpProfileTable,
                                                             (tRBElem *)
                                                             pCapwapFsCapwapDefaultWtpProfileEntry);

    return pGetCapwapFsCapwapDefaultWtpProfileEntry;
}

/****************************************************************************
 Function    :  CapwapGetFirstFsCapwapDnsProfileTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pCapwapFsCapwapDnsProfileEntry or NULL
****************************************************************************/
tCapwapFsCapwapDnsProfileEntry *
CapwapGetFirstFsCapwapDnsProfileTable ()
{
    tCapwapFsCapwapDnsProfileEntry *pCapwapFsCapwapDnsProfileEntry = NULL;

    pCapwapFsCapwapDnsProfileEntry =
        (tCapwapFsCapwapDnsProfileEntry *) RBTreeGetFirst (gCapwapGlobals.
                                                           CapwapGlbMib.
                                                           FsCapwapDnsProfileTable);

    return pCapwapFsCapwapDnsProfileEntry;
}

/****************************************************************************
 Function    :  CapwapGetNextFsCapwapDnsProfileTable
 Input       :  pCurrentCapwapFsCapwapDnsProfileEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextCapwapFsCapwapDnsProfileEntry or NULL
****************************************************************************/
tCapwapFsCapwapDnsProfileEntry *
CapwapGetNextFsCapwapDnsProfileTable (tCapwapFsCapwapDnsProfileEntry *
                                      pCurrentCapwapFsCapwapDnsProfileEntry)
{
    tCapwapFsCapwapDnsProfileEntry *pNextCapwapFsCapwapDnsProfileEntry = NULL;

    pNextCapwapFsCapwapDnsProfileEntry =
        (tCapwapFsCapwapDnsProfileEntry *) RBTreeGetNext (gCapwapGlobals.
                                                          CapwapGlbMib.
                                                          FsCapwapDnsProfileTable,
                                                          (tRBElem *)
                                                          pCurrentCapwapFsCapwapDnsProfileEntry,
                                                          NULL);

    return pNextCapwapFsCapwapDnsProfileEntry;
}

/****************************************************************************
 Function    :  CapwapGetFsCapwapDnsProfileTable
 Input       :  pCapwapFsCapwapDnsProfileEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetCapwapFsCapwapDnsProfileEntry or NULL
****************************************************************************/
tCapwapFsCapwapDnsProfileEntry *
CapwapGetFsCapwapDnsProfileTable (tCapwapFsCapwapDnsProfileEntry *
                                  pCapwapFsCapwapDnsProfileEntry)
{
    tCapwapFsCapwapDnsProfileEntry *pGetCapwapFsCapwapDnsProfileEntry = NULL;

    pGetCapwapFsCapwapDnsProfileEntry =
        (tCapwapFsCapwapDnsProfileEntry *) RBTreeGet (gCapwapGlobals.
                                                      CapwapGlbMib.
                                                      FsCapwapDnsProfileTable,
                                                      (tRBElem *)
                                                      pCapwapFsCapwapDnsProfileEntry);

    return pGetCapwapFsCapwapDnsProfileEntry;
}

/****************************************************************************
 Function    :  CapwapGetFirstFsWtpNativeVlanIdTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pCapwapFsWtpNativeVlanIdTable or NULL
****************************************************************************/
tCapwapFsWtpNativeVlanIdTable *
CapwapGetFirstFsWtpNativeVlanIdTable ()
{
    tCapwapFsWtpNativeVlanIdTable *pCapwapFsWtpNativeVlanIdTable = NULL;

    pCapwapFsWtpNativeVlanIdTable =
        (tCapwapFsWtpNativeVlanIdTable *) RBTreeGetFirst (gCapwapGlobals.
                                                          CapwapGlbMib.
                                                          FsWtpNativeVlanIdTable);

    return pCapwapFsWtpNativeVlanIdTable;
}

/****************************************************************************
 Function    :  CapwapGetNextFsWtpNativeVlanIdTable
 Input       :  pCurrentCapwapFsWtpNativeVlanIdTable
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextCapwapFsWtpNativeVlanIdTable or NULL
****************************************************************************/
tCapwapFsWtpNativeVlanIdTable *
CapwapGetNextFsWtpNativeVlanIdTable (tCapwapFsWtpNativeVlanIdTable *
                                     pCurrentCapwapFsWtpNativeVlanIdTable)
{
    tCapwapFsWtpNativeVlanIdTable *pNextCapwapFsWtpNativeVlanIdTable = NULL;

    pNextCapwapFsWtpNativeVlanIdTable =
        (tCapwapFsWtpNativeVlanIdTable *) RBTreeGetNext (gCapwapGlobals.
                                                         CapwapGlbMib.
                                                         FsWtpNativeVlanIdTable,
                                                         (tRBElem *)
                                                         pCurrentCapwapFsWtpNativeVlanIdTable,
                                                         NULL);

    return pNextCapwapFsWtpNativeVlanIdTable;
}

/****************************************************************************
 Function    :  CapwapGetFsWtpNativeVlanIdTable
 Input       :  pCapwapFsWtpNativeVlanIdTable
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetCapwapFsWtpNativeVlanIdTable or NULL
****************************************************************************/
tCapwapFsWtpNativeVlanIdTable *
CapwapGetFsWtpNativeVlanIdTable (tCapwapFsWtpNativeVlanIdTable *
                                 pCapwapFsWtpNativeVlanIdTable)
{
    tCapwapFsWtpNativeVlanIdTable *pGetCapwapFsWtpNativeVlanIdTable = NULL;

    pGetCapwapFsWtpNativeVlanIdTable =
        (tCapwapFsWtpNativeVlanIdTable *) RBTreeGet (gCapwapGlobals.
                                                     CapwapGlbMib.
                                                     FsWtpNativeVlanIdTable,
                                                     (tRBElem *)
                                                     pCapwapFsWtpNativeVlanIdTable);

    return pGetCapwapFsWtpNativeVlanIdTable;
}

/****************************************************************************
 Function    :  CapwapGetFirstFsCawapDiscStatsTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pCapwapFsCawapDiscStatsEntry or NULL
****************************************************************************/
tCapwapFsCawapDiscStatsEntry *
CapwapGetFirstFsCawapDiscStatsTable ()
{
    tCapwapFsCawapDiscStatsEntry *pCapwapFsCawapDiscStatsEntry = NULL;

    pCapwapFsCawapDiscStatsEntry =
        (tCapwapFsCawapDiscStatsEntry *) RBTreeGetFirst (gCapwapGlobals.
                                                         CapwapGlbMib.
                                                         FsCawapDiscStatsTable);

    return pCapwapFsCawapDiscStatsEntry;
}

/****************************************************************************
 Function    :  CapwapGetFirstFsWtpLocalRoutingTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pCapwapFsWtpLocalRoutingTable or NULL
****************************************************************************/
tCapwapFsWtpLocalRoutingTable *
CapwapGetFirstFsWtpLocalRoutingTable ()
{
    tCapwapFsWtpLocalRoutingTable *pCapwapFsWtpLocalRoutingTable = NULL;

    pCapwapFsWtpLocalRoutingTable =
        (tCapwapFsWtpLocalRoutingTable *) RBTreeGetFirst (gCapwapGlobals.
                                                          CapwapGlbMib.
                                                          FsWtpLocalRoutingTable);

    return pCapwapFsWtpLocalRoutingTable;
}

/****************************************************************************
 Function    :  CapwapGetNextFsWtpLocalRoutingTable
 Input       :  pCurrentCapwapFsWtpLocalRoutingTable
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextCapwapFsWtpLocalRoutingTable or NULL
****************************************************************************/
tCapwapFsWtpLocalRoutingTable *
CapwapGetNextFsWtpLocalRoutingTable (tCapwapFsWtpLocalRoutingTable *
                                     pCurrentCapwapFsWtpLocalRoutingTable)
{
    tCapwapFsWtpLocalRoutingTable *pNextCapwapFsWtpLocalRoutingTable = NULL;

    pNextCapwapFsWtpLocalRoutingTable =
        (tCapwapFsWtpLocalRoutingTable *) RBTreeGetNext (gCapwapGlobals.
                                                         CapwapGlbMib.
                                                         FsWtpLocalRoutingTable,
                                                         (tRBElem *)
                                                         pCurrentCapwapFsWtpLocalRoutingTable,
                                                         NULL);

    return pNextCapwapFsWtpLocalRoutingTable;
}

/****************************************************************************
 Function    :  CapwapGetFsWtpLocalRoutingTable
 Input       :  pCapwapFsWtpLocalRoutingTable
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetCapwapFsWtpLocalRoutingTable or NULL
****************************************************************************/
tCapwapFsWtpLocalRoutingTable *
CapwapGetFsWtpLocalRoutingTable (tCapwapFsWtpLocalRoutingTable *
                                 pCapwapFsWtpLocalRoutingTable)
{
    tCapwapFsWtpLocalRoutingTable *pGetCapwapFsWtpLocalRoutingTable = NULL;

    pGetCapwapFsWtpLocalRoutingTable =
        (tCapwapFsWtpLocalRoutingTable *) RBTreeGet (gCapwapGlobals.
                                                     CapwapGlbMib.
                                                     FsWtpLocalRoutingTable,
                                                     (tRBElem *)
                                                     pCapwapFsWtpLocalRoutingTable);

    return pGetCapwapFsWtpLocalRoutingTable;
}

/****************************************************************************
 Function    :  CapwapGetNextFsCawapDiscStatsTable
 Input       :  pCurrentCapwapFsCawapDiscStatsEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextCapwapFsCawapDiscStatsEntry or NULL
****************************************************************************/
tCapwapFsCawapDiscStatsEntry *
CapwapGetNextFsCawapDiscStatsTable (tCapwapFsCawapDiscStatsEntry *
                                    pCurrentCapwapFsCawapDiscStatsEntry)
{
    tCapwapFsCawapDiscStatsEntry *pNextCapwapFsCawapDiscStatsEntry = NULL;

    pNextCapwapFsCawapDiscStatsEntry =
        (tCapwapFsCawapDiscStatsEntry *) RBTreeGetNext (gCapwapGlobals.
                                                        CapwapGlbMib.
                                                        FsCawapDiscStatsTable,
                                                        (tRBElem *)
                                                        pCurrentCapwapFsCawapDiscStatsEntry,
                                                        NULL);

    return pNextCapwapFsCawapDiscStatsEntry;
}

/****************************************************************************
 Function    :  CapwapGetFsCawapDiscStatsTable
 Input       :  pCapwapFsCawapDiscStatsEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetCapwapFsCawapDiscStatsEntry or NULL
****************************************************************************/
tCapwapFsCawapDiscStatsEntry *
CapwapGetFsCawapDiscStatsTable (tCapwapFsCawapDiscStatsEntry *
                                pCapwapFsCawapDiscStatsEntry)
{
    tCapwapFsCawapDiscStatsEntry *pGetCapwapFsCawapDiscStatsEntry = NULL;

    pGetCapwapFsCawapDiscStatsEntry =
        (tCapwapFsCawapDiscStatsEntry *) RBTreeGet (gCapwapGlobals.CapwapGlbMib.
                                                    FsCawapDiscStatsTable,
                                                    (tRBElem *)
                                                    pCapwapFsCawapDiscStatsEntry);

    return pGetCapwapFsCawapDiscStatsEntry;
}

/****************************************************************************
 Function    :  CapwapGetFirstFsCawapJoinStatsTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pCapwapFsCawapJoinStatsEntry or NULL
****************************************************************************/
tCapwapFsCawapJoinStatsEntry *
CapwapGetFirstFsCawapJoinStatsTable ()
{
    tCapwapFsCawapJoinStatsEntry *pCapwapFsCawapJoinStatsEntry = NULL;

    pCapwapFsCawapJoinStatsEntry =
        (tCapwapFsCawapJoinStatsEntry *) RBTreeGetFirst (gCapwapGlobals.
                                                         CapwapGlbMib.
                                                         FsCawapJoinStatsTable);

    return pCapwapFsCawapJoinStatsEntry;
}

/****************************************************************************
 Function    :  CapwapGetNextFsCawapJoinStatsTable
 Input       :  pCurrentCapwapFsCawapJoinStatsEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextCapwapFsCawapJoinStatsEntry or NULL
****************************************************************************/
tCapwapFsCawapJoinStatsEntry *
CapwapGetNextFsCawapJoinStatsTable (tCapwapFsCawapJoinStatsEntry *
                                    pCurrentCapwapFsCawapJoinStatsEntry)
{
    tCapwapFsCawapJoinStatsEntry *pNextCapwapFsCawapJoinStatsEntry = NULL;

    pNextCapwapFsCawapJoinStatsEntry =
        (tCapwapFsCawapJoinStatsEntry *) RBTreeGetNext (gCapwapGlobals.
                                                        CapwapGlbMib.
                                                        FsCawapJoinStatsTable,
                                                        (tRBElem *)
                                                        pCurrentCapwapFsCawapJoinStatsEntry,
                                                        NULL);

    return pNextCapwapFsCawapJoinStatsEntry;
}

/****************************************************************************
 Function    :  CapwapGetFsCawapJoinStatsTable
 Input       :  pCapwapFsCawapJoinStatsEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetCapwapFsCawapJoinStatsEntry or NULL
****************************************************************************/
tCapwapFsCawapJoinStatsEntry *
CapwapGetFsCawapJoinStatsTable (tCapwapFsCawapJoinStatsEntry *
                                pCapwapFsCawapJoinStatsEntry)
{
    tCapwapFsCawapJoinStatsEntry *pGetCapwapFsCawapJoinStatsEntry = NULL;

    pGetCapwapFsCawapJoinStatsEntry =
        (tCapwapFsCawapJoinStatsEntry *) RBTreeGet (gCapwapGlobals.CapwapGlbMib.
                                                    FsCawapJoinStatsTable,
                                                    (tRBElem *)
                                                    pCapwapFsCawapJoinStatsEntry);

    return pGetCapwapFsCawapJoinStatsEntry;
}

/****************************************************************************
 Function    :  CapwapGetFirstFsCawapConfigStatsTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pCapwapFsCawapConfigStatsEntry or NULL
****************************************************************************/
tCapwapFsCawapConfigStatsEntry *
CapwapGetFirstFsCawapConfigStatsTable ()
{
    tCapwapFsCawapConfigStatsEntry *pCapwapFsCawapConfigStatsEntry = NULL;

    pCapwapFsCawapConfigStatsEntry =
        (tCapwapFsCawapConfigStatsEntry *) RBTreeGetFirst (gCapwapGlobals.
                                                           CapwapGlbMib.
                                                           FsCawapConfigStatsTable);

    return pCapwapFsCawapConfigStatsEntry;
}

/****************************************************************************
 Function    :  CapwapGetNextFsCawapConfigStatsTable
 Input       :  pCurrentCapwapFsCawapConfigStatsEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextCapwapFsCawapConfigStatsEntry or NULL
****************************************************************************/
tCapwapFsCawapConfigStatsEntry *
CapwapGetNextFsCawapConfigStatsTable (tCapwapFsCawapConfigStatsEntry *
                                      pCurrentCapwapFsCawapConfigStatsEntry)
{
    tCapwapFsCawapConfigStatsEntry *pNextCapwapFsCawapConfigStatsEntry = NULL;

    pNextCapwapFsCawapConfigStatsEntry =
        (tCapwapFsCawapConfigStatsEntry *) RBTreeGetNext (gCapwapGlobals.
                                                          CapwapGlbMib.
                                                          FsCawapConfigStatsTable,
                                                          (tRBElem *)
                                                          pCurrentCapwapFsCawapConfigStatsEntry,
                                                          NULL);

    return pNextCapwapFsCawapConfigStatsEntry;
}

/****************************************************************************
 Function    :  CapwapGetFsCawapConfigStatsTable
 Input       :  pCapwapFsCawapConfigStatsEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetCapwapFsCawapConfigStatsEntry or NULL
****************************************************************************/
tCapwapFsCawapConfigStatsEntry *
CapwapGetFsCawapConfigStatsTable (tCapwapFsCawapConfigStatsEntry *
                                  pCapwapFsCawapConfigStatsEntry)
{
    tCapwapFsCawapConfigStatsEntry *pGetCapwapFsCawapConfigStatsEntry = NULL;

    pGetCapwapFsCawapConfigStatsEntry =
        (tCapwapFsCawapConfigStatsEntry *) RBTreeGet (gCapwapGlobals.
                                                      CapwapGlbMib.
                                                      FsCawapConfigStatsTable,
                                                      (tRBElem *)
                                                      pCapwapFsCawapConfigStatsEntry);

    return pGetCapwapFsCawapConfigStatsEntry;
}

/****************************************************************************
 Function    :  CapwapGetFirstFsCawapRunStatsTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pCapwapFsCawapRunStatsEntry or NULL
****************************************************************************/
INT1
CapwapGetFirstFsCawapRunStatsTable (UINT4 *pu4CapwapBaseWtpProfileId)
{
#ifdef WLC_WANTED
    tWssPmWTPRunEvents *pWssPmWTPRunEvents = NULL;
    UINT2               u2CapwapBaseWtpProfileId = 0;
    pWssPmWTPRunEvents =
        (tWssPmWTPRunEvents *) RBTreeGetFirst (gWssPmWTPRunEvents);
    if (NULL == pWssPmWTPRunEvents)
    {
        return OSIX_FAILURE;
    }

    CapwapGetProfileIdFromInternalProfId ((UINT2) pWssPmWTPRunEvents->
                                          u4CapwapWtpProfileId,
                                          &u2CapwapBaseWtpProfileId);

    *pu4CapwapBaseWtpProfileId = (UINT4) u2CapwapBaseWtpProfileId;

#else
    UNUSED_PARAM (pu4CapwapBaseWtpProfileId);
#endif /* WLC_WANTED */
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetNextFsCawapRunStatsTable
 Input       :  pCurrentCapwapFsCawapRunStatsEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextCapwapFsCawapRunStatsEntry or NULL
****************************************************************************/
INT1
CapwapGetNextFsCawapRunStatsTable (UINT4 u4CapwapBaseWtpProfileId,
                                   UINT4 *pu4CapwapBaseWtpProfileId)
{
#ifdef WLC_WANTED
    tWssPmWTPRunEvents *pWssPmWTPRunEvents = NULL;
    tWssPmWTPRunEvents  WssPmWTPRunEvents;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT2               u2CapwapBaseWtpProfileId = 0;

    CAPWAP_IF_CAP_DB_ALLOC (pWssIfCapwapDB);
    if (pWssIfCapwapDB == NULL)
    {
        return OSIX_FAILURE;
    }

    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    MEMSET (&WssPmWTPRunEvents, 0, sizeof (tWssPmWTPRunEvents));

    pWssIfCapwapDB->CapwapGetDB.u2ProfileId = (UINT2) u4CapwapBaseWtpProfileId;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpProfileId = OSIX_TRUE;

    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_INTERNAL_ID, pWssIfCapwapDB) ==
        OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FSM_TRC, "Getting Internal Id Failed\r\n");
        CAPWAP_IF_CAP_DB_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    WssPmWTPRunEvents.u4CapwapWtpProfileId =
        (UINT2) (pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId);
    pWssPmWTPRunEvents =
        (tWssPmWTPRunEvents *) RBTreeGetNext (gWssPmWTPRunEvents,
                                              (tRBElem *) & WssPmWTPRunEvents,
                                              NULL);
    if (NULL == pWssPmWTPRunEvents)
    {
        CAPWAP_IF_CAP_DB_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    CapwapGetProfileIdFromInternalProfId ((UINT2)
                                          (pWssPmWTPRunEvents->
                                           u4CapwapWtpProfileId),
                                          &u2CapwapBaseWtpProfileId);
    *pu4CapwapBaseWtpProfileId = (UINT4) u2CapwapBaseWtpProfileId;
    CAPWAP_IF_CAP_DB_RELEASE (pWssIfCapwapDB);

#else
    UNUSED_PARAM (pu4CapwapBaseWtpProfileId);
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
#endif /* WLC_WANTED */
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetFsCawapRunStatsTable
 Input       :  pCapwapFsCawapRunStatsEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetCapwapFsCawapRunStatsEntry or NULL
****************************************************************************/
tCapwapFsCawapRunStatsEntry *
CapwapGetFsCawapRunStatsTable (tCapwapFsCawapRunStatsEntry *
                               pCapwapFsCawapRunStatsEntry)
{
    tCapwapFsCawapRunStatsEntry *pGetCapwapFsCawapRunStatsEntry = NULL;

    pGetCapwapFsCawapRunStatsEntry =
        (tCapwapFsCawapRunStatsEntry *) RBTreeGet (gCapwapGlobals.CapwapGlbMib.
                                                   FsCawapRunStatsTable,
                                                   (tRBElem *)
                                                   pCapwapFsCawapRunStatsEntry);

    return pGetCapwapFsCawapRunStatsEntry;
}

/****************************************************************************
 Function    :  CapwapGetFirstFsCapwapWirelessBindingTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pCapwapFsCapwapWirelessBindingEntry or NULL
****************************************************************************/
tCapwapFsCapwapWirelessBindingEntry *
CapwapGetFirstFsCapwapWirelessBindingTable ()
{
    tCapwapFsCapwapWirelessBindingEntry *pCapwapFsCapwapWirelessBindingEntry =
        NULL;

    pCapwapFsCapwapWirelessBindingEntry =
        (tCapwapFsCapwapWirelessBindingEntry *) RBTreeGetFirst (gCapwapGlobals.
                                                                CapwapGlbMib.
                                                                FsCapwapWirelessBindingTable);

    return pCapwapFsCapwapWirelessBindingEntry;
}

/****************************************************************************
 Function    :  CapwapGetNextFsCapwapWirelessBindingTable
 Input       :  pCurrentCapwapFsCapwapWirelessBindingEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextCapwapFsCapwapWirelessBindingEntry or NULL
****************************************************************************/
tCapwapFsCapwapWirelessBindingEntry *
CapwapGetNextFsCapwapWirelessBindingTable (tCapwapFsCapwapWirelessBindingEntry *
                                           pCurrentCapwapFsCapwapWirelessBindingEntry)
{
    tCapwapFsCapwapWirelessBindingEntry *pNextCapwapFsCapwapWirelessBindingEntry
        = NULL;

    pNextCapwapFsCapwapWirelessBindingEntry =
        (tCapwapFsCapwapWirelessBindingEntry *) RBTreeGetNext (gCapwapGlobals.
                                                               CapwapGlbMib.
                                                               FsCapwapWirelessBindingTable,
                                                               (tRBElem *)
                                                               pCurrentCapwapFsCapwapWirelessBindingEntry,
                                                               NULL);

    return pNextCapwapFsCapwapWirelessBindingEntry;
}

/****************************************************************************
 Function    :  CapwapGetFsCapwapWirelessBindingTable
 Input       :  pCapwapFsCapwapWirelessBindingEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetCapwapFsCapwapWirelessBindingEntry or NULL
****************************************************************************/
tCapwapFsCapwapWirelessBindingEntry *
CapwapGetFsCapwapWirelessBindingTable (tCapwapFsCapwapWirelessBindingEntry *
                                       pCapwapFsCapwapWirelessBindingEntry)
{
    tCapwapFsCapwapWirelessBindingEntry *pGetCapwapFsCapwapWirelessBindingEntry
        = NULL;

    pGetCapwapFsCapwapWirelessBindingEntry =
        (tCapwapFsCapwapWirelessBindingEntry *) RBTreeGet (gCapwapGlobals.
                                                           CapwapGlbMib.
                                                           FsCapwapWirelessBindingTable,
                                                           (tRBElem *)
                                                           pCapwapFsCapwapWirelessBindingEntry);

    return pGetCapwapFsCapwapWirelessBindingEntry;
}

/****************************************************************************
 Function    :  CapwapGetFirstFsCapwapStationWhiteList
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pCapwapFsCapwapStationWhiteListEntry or NULL
****************************************************************************/
tCapwapFsCapwapStationWhiteListEntry *
CapwapGetFirstFsCapwapStationWhiteList ()
{
    tCapwapFsCapwapStationWhiteListEntry *pCapwapFsCapwapStationWhiteListEntry =
        NULL;

    pCapwapFsCapwapStationWhiteListEntry =
        (tCapwapFsCapwapStationWhiteListEntry *) RBTreeGetFirst (gCapwapGlobals.
                                                                 CapwapGlbMib.
                                                                 FsCapwapStationWhiteList);

    return pCapwapFsCapwapStationWhiteListEntry;
}

/****************************************************************************
 Function    :  CapwapGetNextFsCapwapStationWhiteList
 Input       :  pCurrentCapwapFsCapwapStationWhiteListEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextCapwapFsCapwapStationWhiteListEntry or NULL
****************************************************************************/
tCapwapFsCapwapStationWhiteListEntry *
CapwapGetNextFsCapwapStationWhiteList (tCapwapFsCapwapStationWhiteListEntry *
                                       pCurrentCapwapFsCapwapStationWhiteListEntry)
{
    tCapwapFsCapwapStationWhiteListEntry
        * pNextCapwapFsCapwapStationWhiteListEntry = NULL;

    pNextCapwapFsCapwapStationWhiteListEntry =
        (tCapwapFsCapwapStationWhiteListEntry *) RBTreeGetNext (gCapwapGlobals.
                                                                CapwapGlbMib.
                                                                FsCapwapStationWhiteList,
                                                                (tRBElem *)
                                                                pCurrentCapwapFsCapwapStationWhiteListEntry,
                                                                NULL);

    return pNextCapwapFsCapwapStationWhiteListEntry;
}

/****************************************************************************
 Function    :  CapwapGetFsCapwapStationWhiteList
 Input       :  pCapwapFsCapwapStationWhiteListEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetCapwapFsCapwapStationWhiteListEntry or NULL
****************************************************************************/
tCapwapFsCapwapStationWhiteListEntry *
CapwapGetFsCapwapStationWhiteList (tCapwapFsCapwapStationWhiteListEntry *
                                   pCapwapFsCapwapStationWhiteListEntry)
{
    tCapwapFsCapwapStationWhiteListEntry
        * pGetCapwapFsCapwapStationWhiteListEntry = NULL;

    pGetCapwapFsCapwapStationWhiteListEntry =
        (tCapwapFsCapwapStationWhiteListEntry *) RBTreeGet (gCapwapGlobals.
                                                            CapwapGlbMib.
                                                            FsCapwapStationWhiteList,
                                                            (tRBElem *)
                                                            pCapwapFsCapwapStationWhiteListEntry);

    return pGetCapwapFsCapwapStationWhiteListEntry;
}

/****************************************************************************
 Function    :  CapwapGetFirstFsCapwapWtpRebootStatisticsTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pCapwapFsCapwapWtpRebootStatisticsEntry or NULL
****************************************************************************/
INT1
CapwapGetFirstFsCapwapWtpRebootStatisticsTable (UINT4
                                                *pu4CapwapBaseWtpProfileId)
{
#ifdef WLC_WANTED
    UINT2               u2CapwapBaseWtpProfileId = 0;
    tWssPmWTPIDStatsProcessRBDB *pWtpIdStatsMap = NULL;

    pWtpIdStatsMap =
        (tWssPmWTPIDStatsProcessRBDB *)
        RBTreeGetFirst (gWssPmWTPIDStatsProcessDB);
    if (NULL == pWtpIdStatsMap)
    {
        return SNMP_FAILURE;
    }

    CapwapGetProfileIdFromInternalProfId (pWtpIdStatsMap->u2WtpIntProfileIndex,
                                          &u2CapwapBaseWtpProfileId);
    *pu4CapwapBaseWtpProfileId = (UINT4) u2CapwapBaseWtpProfileId;
#endif /* WLC_WANTED */

#ifdef WTP_WANTED
    /* hardcoding the profile-id to one, though there will not be any use for
     * this in getting the stats as they are globally maintained. also returning
     * always success in this case */
    *pu4CapwapBaseWtpProfileId = (UINT4) 1;
#endif /* WTP_WANTED */

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetNextFsCapwapWtpRebootStatisticsTable
 Input       :  pCurrentCapwapFsCapwapWtpRebootStatisticsEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextCapwapFsCapwapWtpRebootStatisticsEntry or NULL
****************************************************************************/
INT1
CapwapGetNextFsCapwapWtpRebootStatisticsTable (UINT4 u4CapwapBaseWtpProfileId,
                                               UINT4
                                               *pu4NextCapwapBaseWtpProfileId)
{
#ifdef WLC_WANTED
    tWssPmWTPIDStatsProcessRBDB WtpIdStatsMap;
    tWssPmWTPIDStatsProcessRBDB *pNextWtpIdStatsMap = NULL;
    UINT2               u2CapwapBaseWtpProfileId = 0;

    MEMSET (&WtpIdStatsMap, 0, sizeof (tWssPmWTPIDStatsProcessRBDB));
    WtpIdStatsMap.u2WtpIntProfileIndex = (UINT2) u4CapwapBaseWtpProfileId;

    pNextWtpIdStatsMap =
        (tWssPmWTPIDStatsProcessRBDB *)
        RBTreeGetNext (gWssPmWTPIDStatsProcessDB, (tRBElem *) & WtpIdStatsMap,
                       NULL);
    if (NULL == pNextWtpIdStatsMap)
    {
        return SNMP_FAILURE;
    }

    CapwapGetProfileIdFromInternalProfId (pNextWtpIdStatsMap->
                                          u2WtpIntProfileIndex,
                                          &u2CapwapBaseWtpProfileId);
    *pu4NextCapwapBaseWtpProfileId = (UINT4) u2CapwapBaseWtpProfileId;

    return SNMP_SUCCESS;
#endif /* WLC_WANTED */

#ifdef WTP_WANTED
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (pu4NextCapwapBaseWtpProfileId);
    /* hardcoding the return value to failure, as stats are globally maintained. 
     * and also there is only one instance of it */
    return SNMP_FAILURE;
#endif /* WTP_WANTED */

}

/****************************************************************************
 Function    :  CapwapGetFsCapwapWtpRebootStatisticsTable
 Input       :  pCapwapFsCapwapWtpRebootStatisticsEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetCapwapFsCapwapWtpRebootStatisticsEntry or NULL
****************************************************************************/
tCapwapFsCapwapWtpRebootStatisticsEntry
    * CapwapGetFsCapwapWtpRebootStatisticsTable
    (tCapwapFsCapwapWtpRebootStatisticsEntry *
     pCapwapFsCapwapWtpRebootStatisticsEntry)
{
    tCapwapFsCapwapWtpRebootStatisticsEntry
        * pGetCapwapFsCapwapWtpRebootStatisticsEntry = NULL;

    pGetCapwapFsCapwapWtpRebootStatisticsEntry =
        (tCapwapFsCapwapWtpRebootStatisticsEntry *) RBTreeGet (gCapwapGlobals.
                                                               CapwapGlbMib.
                                                               FsCapwapWtpRebootStatisticsTable,
                                                               (tRBElem *)
                                                               pCapwapFsCapwapWtpRebootStatisticsEntry);

    return pGetCapwapFsCapwapWtpRebootStatisticsEntry;
}

/****************************************************************************
 Function    :  CapwapGetFirstFsCapwapWtpRadioStatisticsTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pCapwapFsCapwapWtpRadioStatisticsEntry or NULL
****************************************************************************/
INT1
CapwapGetFirstFsCapwapWtpRadioStatisticsTable (INT4 *pi4RadioIfIndex)
{
#ifdef WLC_WANTED
    tWssPmWTPIDRadIDStatsProcessRBDB *pwtpIDRadIDStatsMap = NULL;

    pwtpIDRadIDStatsMap =
        (tWssPmWTPIDRadIDStatsProcessRBDB *)
        RBTreeGetFirst (gWssPmWTPIDRadIDStatsProcessDB);
    if (NULL == pwtpIDRadIDStatsMap)
    {
        return SNMP_FAILURE;
    }

    *pi4RadioIfIndex = (INT4) pwtpIDRadIDStatsMap->u4WtpRadioIndex;

#endif /* WLC_WANTED */

#ifdef WTP_WANTED
    /* hardcoding the radio-id to one, as there will be only one radio-id
     * in wtp. also returning always success in this case */
    *pi4RadioIfIndex = (UINT4) 1;
#endif /* WTP_WANTED */
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  CapwapGetNextFsCapwapWtpRadioStatisticsTable
 Input       :  pCurrentCapwapFsCapwapWtpRadioStatisticsEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextCapwapFsCapwapWtpRadioStatisticsEntry or NULL
****************************************************************************/
INT1
CapwapGetNextFsCapwapWtpRadioStatisticsTable (INT4 i4RadioIfIndex,
                                              INT4 *pi4NextRadioIfIndex)
{
#ifdef WLC_WANTED
    tWssPmWTPIDRadIDStatsProcessRBDB WtpIDRadIDStatsMap;
    tWssPmWTPIDRadIDStatsProcessRBDB *pNextWtpIDRadIDStatsMap = NULL;

    MEMSET (&WtpIDRadIDStatsMap, 0, sizeof (tWssPmWTPIDRadIDStatsProcessRBDB));
    WtpIDRadIDStatsMap.u4WtpRadioIndex = (UINT4) i4RadioIfIndex;

    pNextWtpIDRadIDStatsMap =
        (tWssPmWTPIDRadIDStatsProcessRBDB *)
        RBTreeGetNext (gWssPmWTPIDRadIDStatsProcessDB,
                       (tRBElem *) & WtpIDRadIDStatsMap, NULL);
    if (NULL == pNextWtpIDRadIDStatsMap)
    {
        return SNMP_FAILURE;
    }

    *pi4NextRadioIfIndex = (INT4) pNextWtpIDRadIDStatsMap->u4WtpRadioIndex;

    return SNMP_SUCCESS;
#endif /* WLC_WANTED */

#ifdef WTP_WANTED
    UNUSED_PARAM (i4RadioIfIndex);
    UNUSED_PARAM (pi4NextRadioIfIndex);
    /* hardcoding the radio-id to one, as there will be only one radio-id
     * in wtp. also returning always failure in this case
     * bcos there is only one instance of it */
    return SNMP_FAILURE;
#endif /* WTP_WANTED */

}

/****************************************************************************
 Function    :  CapwapGetFsCapwapWtpRadioStatisticsTable
 Input       :  pCapwapFsCapwapWtpRadioStatisticsEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetCapwapFsCapwapWtpRadioStatisticsEntry or NULL
****************************************************************************/
tCapwapFsCapwapWtpRadioStatisticsEntry *
CapwapGetFsCapwapWtpRadioStatisticsTable (tCapwapFsCapwapWtpRadioStatisticsEntry
                                          *
                                          pCapwapFsCapwapWtpRadioStatisticsEntry)
{
    tCapwapFsCapwapWtpRadioStatisticsEntry
        * pGetCapwapFsCapwapWtpRadioStatisticsEntry = NULL;

    pGetCapwapFsCapwapWtpRadioStatisticsEntry =
        (tCapwapFsCapwapWtpRadioStatisticsEntry *) RBTreeGet (gCapwapGlobals.
                                                              CapwapGlbMib.
                                                              FsCapwapWtpRadioStatisticsTable,
                                                              (tRBElem *)
                                                              pCapwapFsCapwapWtpRadioStatisticsEntry);

    return pGetCapwapFsCapwapWtpRadioStatisticsEntry;
}

/****************************************************************************
 Function    :  CapwapGetCapwapBaseWtpSessions
 Input       :  The Indices
 Input       :  pu4CapwapBaseWtpSessions
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetCapwapBaseWtpSessions (UINT4 *pu4CapwapBaseWtpSessions)
{
    tWssIfCapDB        *pWssIfCapDB = NULL;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapDB, 0, sizeof (tWssIfCapDB));

    pWssIfCapDB->CapwapIsGetAllDB.bActiveWtps = OSIX_TRUE;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapDB) ==
        OSIX_FAILURE)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
        return OSIX_FAILURE;
    }

    *pu4CapwapBaseWtpSessions = pWssIfCapDB->u2ActiveWtps;

    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetCapwapBaseWtpSessionsLimit
 Input       :  The Indices
 Input       :  pu4CapwapBaseWtpSessionsLimit
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetCapwapBaseWtpSessionsLimit (UINT4 *pu4CapwapBaseWtpSessionsLimit)
{
    tWssIfCapDB        *pWssIfCapDB = NULL;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapDB, 0, sizeof (tWssIfCapDB));

    pWssIfCapDB->CapwapIsGetAllDB.bMaxWtps = OSIX_TRUE;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapDB) ==
        OSIX_FAILURE)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
        return OSIX_FAILURE;
    }

    *pu4CapwapBaseWtpSessionsLimit = pWssIfCapDB->u2MaxWtps;

    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapSetCapwapBaseWtpSessionsLimit
 Input       :  u4CapwapBaseWtpSessionsLimit
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
CapwapSetCapwapBaseWtpSessionsLimit (UINT4 u4CapwapBaseWtpSessionsLimit)
{
    INT4                i4SetOption = SNMP_SUCCESS;

    if (u4CapwapBaseWtpSessionsLimit > 65535)
    {
        return OSIX_FAILURE;
    }
    gCapwapGlobals.CapwapGlbMib.u4CapwapBaseWtpSessionsLimit =
        u4CapwapBaseWtpSessionsLimit;

    nmhSetCmnNew (CapwapBaseWtpSessionsLimit, 0, CapwapMainTaskLock,
                  CapwapMainTaskUnLock, 0, 0, 0, i4SetOption, "%u",
                  gCapwapGlobals.CapwapGlbMib.u4CapwapBaseWtpSessionsLimit);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetCapwapBaseStationSessions
 Input       :  The Indices
 Input       :  pu4CapwapBaseStationSessions
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetCapwapBaseStationSessions (UINT4 *pu4CapwapBaseStationSessions)
{
    *pu4CapwapBaseStationSessions =
        gCapwapGlobals.CapwapGlbMib.u4CapwapBaseStationSessions;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetCapwapBaseStationSessionsLimit
 Input       :  The Indices
 Input       :  pu4CapwapBaseStationSessionsLimit
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetCapwapBaseStationSessionsLimit (UINT4
                                         *pu4CapwapBaseStationSessionsLimit)
{
    *pu4CapwapBaseStationSessionsLimit =
        gCapwapGlobals.CapwapGlbMib.u4CapwapBaseStationSessionsLimit;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapSetCapwapBaseStationSessionsLimit
 Input       :  u4CapwapBaseStationSessionsLimit
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
CapwapSetCapwapBaseStationSessionsLimit (UINT4 u4CapwapBaseStationSessionsLimit)
{
    INT4                i4SetOption = SNMP_SUCCESS;

    gCapwapGlobals.CapwapGlbMib.u4CapwapBaseStationSessionsLimit =
        u4CapwapBaseStationSessionsLimit;

    nmhSetCmnNew (CapwapBaseStationSessionsLimit, 0, CapwapMainTaskLock,
                  CapwapMainTaskUnLock, 0, 0, 0, i4SetOption, "%u",
                  gCapwapGlobals.CapwapGlbMib.u4CapwapBaseStationSessionsLimit);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetCapwapBaseDataChannelDTLSPolicyOptions
 Input       :  The Indices
 Input       :  pCapwapBaseDataChannelDTLSPolicyOptions
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetCapwapBaseDataChannelDTLSPolicyOptions (UINT1
                                                 *pCapwapBaseDataChannelDTLSPolicyOptions)
{
    MEMCPY (pCapwapBaseDataChannelDTLSPolicyOptions,
            gCapwapGlobals.CapwapGlbMib.
            au1CapwapBaseDataChannelDTLSPolicyOptions,
            gCapwapGlobals.CapwapGlbMib.
            i4CapwapBaseDataChannelDTLSPolicyOptionsLen);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetCapwapBaseControlChannelAuthenOptions
 Input       :  The Indices
 Input       :  pCapwapBaseControlChannelAuthenOptions
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetCapwapBaseControlChannelAuthenOptions (UINT1
                                                *pCapwapBaseControlChannelAuthenOptions)
{
    MEMCPY (pCapwapBaseControlChannelAuthenOptions,
            gCapwapGlobals.CapwapGlbMib.
            au1CapwapBaseControlChannelAuthenOptions,
            gCapwapGlobals.CapwapGlbMib.
            i4CapwapBaseControlChannelAuthenOptionsLen);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetFirstCapwapBaseAcNameListTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pCapwapCapwapBaseAcNameListEntry or NULL
****************************************************************************/
tCapwapCapwapBaseAcNameListEntry *
CapwapGetFirstCapwapBaseAcNameListTable ()
{
    tCapwapCapwapBaseAcNameListEntry *pCapwapCapwapBaseAcNameListEntry = NULL;

    pCapwapCapwapBaseAcNameListEntry =
        (tCapwapCapwapBaseAcNameListEntry *) RBTreeGetFirst (gCapwapGlobals.
                                                             CapwapGlbMib.
                                                             CapwapBaseAcNameListTable);

    return pCapwapCapwapBaseAcNameListEntry;
}

/****************************************************************************
 Function    :  CapwapGetNextCapwapBaseAcNameListTable
 Input       :  pCurrentCapwapCapwapBaseAcNameListEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextCapwapCapwapBaseAcNameListEntry or NULL
****************************************************************************/
tCapwapCapwapBaseAcNameListEntry *
CapwapGetNextCapwapBaseAcNameListTable (tCapwapCapwapBaseAcNameListEntry *
                                        pCurrentCapwapCapwapBaseAcNameListEntry)
{
    tCapwapCapwapBaseAcNameListEntry *pNextCapwapCapwapBaseAcNameListEntry =
        NULL;

    pNextCapwapCapwapBaseAcNameListEntry =
        (tCapwapCapwapBaseAcNameListEntry *) RBTreeGetNext (gCapwapGlobals.
                                                            CapwapGlbMib.
                                                            CapwapBaseAcNameListTable,
                                                            (tRBElem *)
                                                            pCurrentCapwapCapwapBaseAcNameListEntry,
                                                            NULL);

    return pNextCapwapCapwapBaseAcNameListEntry;
}

/****************************************************************************
 Function    :  CapwapGetCapwapBaseAcNameListTable
 Input       :  pCapwapCapwapBaseAcNameListEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetCapwapCapwapBaseAcNameListEntry or NULL
****************************************************************************/
tCapwapCapwapBaseAcNameListEntry *
CapwapGetCapwapBaseAcNameListTable (tCapwapCapwapBaseAcNameListEntry *
                                    pCapwapCapwapBaseAcNameListEntry)
{
    tCapwapCapwapBaseAcNameListEntry *pGetCapwapCapwapBaseAcNameListEntry =
        NULL;

    pGetCapwapCapwapBaseAcNameListEntry =
        (tCapwapCapwapBaseAcNameListEntry *) RBTreeGet (gCapwapGlobals.
                                                        CapwapGlbMib.
                                                        CapwapBaseAcNameListTable,
                                                        (tRBElem *)
                                                        pCapwapCapwapBaseAcNameListEntry);

    return pGetCapwapCapwapBaseAcNameListEntry;
}

/****************************************************************************
 Function    :  CapwapGetFirstCapwapBaseMacAclTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pCapwapCapwapBaseMacAclEntry or NULL
****************************************************************************/
tCapwapCapwapBaseMacAclEntry *
CapwapGetFirstCapwapBaseMacAclTable ()
{
    tCapwapCapwapBaseMacAclEntry *pCapwapCapwapBaseMacAclEntry = NULL;

    pCapwapCapwapBaseMacAclEntry =
        (tCapwapCapwapBaseMacAclEntry *) RBTreeGetFirst (gCapwapGlobals.
                                                         CapwapGlbMib.
                                                         CapwapBaseMacAclTable);

    return pCapwapCapwapBaseMacAclEntry;
}

/****************************************************************************
 Function    :  CapwapGetNextCapwapBaseMacAclTable
 Input       :  pCurrentCapwapCapwapBaseMacAclEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextCapwapCapwapBaseMacAclEntry or NULL
****************************************************************************/
tCapwapCapwapBaseMacAclEntry *
CapwapGetNextCapwapBaseMacAclTable (tCapwapCapwapBaseMacAclEntry *
                                    pCurrentCapwapCapwapBaseMacAclEntry)
{
    tCapwapCapwapBaseMacAclEntry *pNextCapwapCapwapBaseMacAclEntry = NULL;

    pNextCapwapCapwapBaseMacAclEntry =
        (tCapwapCapwapBaseMacAclEntry *) RBTreeGetNext (gCapwapGlobals.
                                                        CapwapGlbMib.
                                                        CapwapBaseMacAclTable,
                                                        (tRBElem *)
                                                        pCurrentCapwapCapwapBaseMacAclEntry,
                                                        NULL);

    return pNextCapwapCapwapBaseMacAclEntry;
}

/****************************************************************************
 Function    :  CapwapGetCapwapBaseMacAclTable
 Input       :  pCapwapCapwapBaseMacAclEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetCapwapCapwapBaseMacAclEntry or NULL
****************************************************************************/
tCapwapCapwapBaseMacAclEntry *
CapwapGetCapwapBaseMacAclTable (tCapwapCapwapBaseMacAclEntry *
                                pCapwapCapwapBaseMacAclEntry)
{
    tCapwapCapwapBaseMacAclEntry *pGetCapwapCapwapBaseMacAclEntry = NULL;

    pGetCapwapCapwapBaseMacAclEntry =
        (tCapwapCapwapBaseMacAclEntry *) RBTreeGet (gCapwapGlobals.CapwapGlbMib.
                                                    CapwapBaseMacAclTable,
                                                    (tRBElem *)
                                                    pCapwapCapwapBaseMacAclEntry);

    return pGetCapwapCapwapBaseMacAclEntry;
}

/****************************************************************************
 Function    :  CapwapGetFirstCapwapBaseWtpProfileTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pCapwapCapwapBaseWtpProfileEntry or NULL
****************************************************************************/
tCapwapCapwapBaseWtpProfileEntry *
CapwapGetFirstCapwapBaseWtpProfileTable ()
{
    tCapwapCapwapBaseWtpProfileEntry *pCapwapCapwapBaseWtpProfileEntry = NULL;

    pCapwapCapwapBaseWtpProfileEntry =
        (tCapwapCapwapBaseWtpProfileEntry *) RBTreeGetFirst (gCapwapGlobals.
                                                             CapwapGlbMib.
                                                             CapwapBaseWtpProfileTable);

    return pCapwapCapwapBaseWtpProfileEntry;
}

/****************************************************************************
 Function    :  CapwapGetNextCapwapBaseWtpProfileTable
 Input       :  pCurrentCapwapCapwapBaseWtpProfileEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextCapwapCapwapBaseWtpProfileEntry or NULL
****************************************************************************/
tCapwapCapwapBaseWtpProfileEntry *
CapwapGetNextCapwapBaseWtpProfileTable (tCapwapCapwapBaseWtpProfileEntry *
                                        pCurrentCapwapCapwapBaseWtpProfileEntry)
{
    tCapwapCapwapBaseWtpProfileEntry *pNextCapwapCapwapBaseWtpProfileEntry =
        NULL;

    pNextCapwapCapwapBaseWtpProfileEntry =
        (tCapwapCapwapBaseWtpProfileEntry *) RBTreeGetNext (gCapwapGlobals.
                                                            CapwapGlbMib.
                                                            CapwapBaseWtpProfileTable,
                                                            (tRBElem *)
                                                            pCurrentCapwapCapwapBaseWtpProfileEntry,
                                                            NULL);

    return pNextCapwapCapwapBaseWtpProfileEntry;
}

/****************************************************************************
 Function    :  CapwapGetCapwapBaseWtpProfileTable
 Input       :  pCapwapCapwapBaseWtpProfileEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetCapwapCapwapBaseWtpProfileEntry or NULL
****************************************************************************/
tCapwapCapwapBaseWtpProfileEntry *
CapwapGetCapwapBaseWtpProfileTable (tCapwapCapwapBaseWtpProfileEntry *
                                    pCapwapCapwapBaseWtpProfileEntry)
{
    tCapwapCapwapBaseWtpProfileEntry *pGetCapwapCapwapBaseWtpProfileEntry =
        NULL;

    pGetCapwapCapwapBaseWtpProfileEntry =
        (tCapwapCapwapBaseWtpProfileEntry *) RBTreeGet (gCapwapGlobals.
                                                        CapwapGlbMib.
                                                        CapwapBaseWtpProfileTable,
                                                        (tRBElem *)
                                                        pCapwapCapwapBaseWtpProfileEntry);

    return pGetCapwapCapwapBaseWtpProfileEntry;
}

/****************************************************************************
 Function    :  CapwapGetFirstCapwapBaseWtpStateTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pCapwapCapwapBaseWtpStateEntry or NULL
****************************************************************************/
tCapwapCapwapBaseWtpStateEntry *
CapwapGetFirstCapwapBaseWtpStateTable ()
{
    tCapwapCapwapBaseWtpStateEntry *pCapwapCapwapBaseWtpStateEntry = NULL;

    pCapwapCapwapBaseWtpStateEntry =
        (tCapwapCapwapBaseWtpStateEntry *) RBTreeGetFirst (gCapwapGlobals.
                                                           CapwapGlbMib.
                                                           CapwapBaseWtpStateTable);

    return pCapwapCapwapBaseWtpStateEntry;
}

/****************************************************************************
 Function    :  CapwapGetNextCapwapBaseWtpStateTable
 Input       :  pCurrentCapwapCapwapBaseWtpStateEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextCapwapCapwapBaseWtpStateEntry or NULL
****************************************************************************/
tCapwapCapwapBaseWtpStateEntry *
CapwapGetNextCapwapBaseWtpStateTable (tCapwapCapwapBaseWtpStateEntry *
                                      pCurrentCapwapCapwapBaseWtpStateEntry)
{
    tCapwapCapwapBaseWtpStateEntry *pNextCapwapCapwapBaseWtpStateEntry = NULL;

    pNextCapwapCapwapBaseWtpStateEntry =
        (tCapwapCapwapBaseWtpStateEntry *) RBTreeGetNext (gCapwapGlobals.
                                                          CapwapGlbMib.
                                                          CapwapBaseWtpStateTable,
                                                          (tRBElem *)
                                                          pCurrentCapwapCapwapBaseWtpStateEntry,
                                                          NULL);

    return pNextCapwapCapwapBaseWtpStateEntry;
}

/****************************************************************************
 Function    :  CapwapGetCapwapBaseAcMaxRetransmit
 Input       :  The Indices
 Input       :  pu4CapwapBaseAcMaxRetransmit
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetCapwapBaseAcMaxRetransmit (UINT4 *pu4CapwapBaseAcMaxRetransmit)
{
    *pu4CapwapBaseAcMaxRetransmit =
        gCapwapGlobals.CapwapGlbMib.u4CapwapBaseAcMaxRetransmit;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapSetCapwapBaseAcMaxRetransmit
 Input       :  u4CapwapBaseAcMaxRetransmit
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
CapwapSetCapwapBaseAcMaxRetransmit (UINT4 u4CapwapBaseAcMaxRetransmit)
{
    INT4                i4SetOption = SNMP_SUCCESS;

    gCapwapGlobals.CapwapGlbMib.u4CapwapBaseAcMaxRetransmit =
        u4CapwapBaseAcMaxRetransmit;

    nmhSetCmnNew (CapwapBaseAcMaxRetransmit, 0, CapwapMainTaskLock,
                  CapwapMainTaskUnLock, 0, 0, 0, i4SetOption, "%u",
                  gCapwapGlobals.CapwapGlbMib.u4CapwapBaseAcMaxRetransmit);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetCapwapBaseAcChangeStatePendingTimer
 Input       :  The Indices
 Input       :  pu4CapwapBaseAcChangeStatePendingTimer
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetCapwapBaseAcChangeStatePendingTimer (UINT4
                                              *pu4CapwapBaseAcChangeStatePendingTimer)
{
    *pu4CapwapBaseAcChangeStatePendingTimer =
        gCapwapGlobals.CapwapGlbMib.u4CapwapBaseAcChangeStatePendingTimer;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapSetCapwapBaseAcChangeStatePendingTimer
 Input       :  u4CapwapBaseAcChangeStatePendingTimer
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
CapwapSetCapwapBaseAcChangeStatePendingTimer (UINT4
                                              u4CapwapBaseAcChangeStatePendingTimer)
{
    INT4                i4SetOption = SNMP_SUCCESS;

    gCapwapGlobals.CapwapGlbMib.u4CapwapBaseAcChangeStatePendingTimer =
        u4CapwapBaseAcChangeStatePendingTimer;

    nmhSetCmnNew (CapwapBaseAcChangeStatePendingTimer, 0, CapwapMainTaskLock,
                  CapwapMainTaskUnLock, 0, 0, 0, i4SetOption, "%u",
                  gCapwapGlobals.CapwapGlbMib.
                  u4CapwapBaseAcChangeStatePendingTimer);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetCapwapBaseAcDataCheckTimer
 Input       :  The Indices
 Input       :  pu4CapwapBaseAcDataCheckTimer
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetCapwapBaseAcDataCheckTimer (UINT4 *pu4CapwapBaseAcDataCheckTimer)
{
    *pu4CapwapBaseAcDataCheckTimer =
        gCapwapGlobals.CapwapGlbMib.u4CapwapBaseAcDataCheckTimer;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapSetCapwapBaseAcDataCheckTimer
 Input       :  u4CapwapBaseAcDataCheckTimer
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
CapwapSetCapwapBaseAcDataCheckTimer (UINT4 u4CapwapBaseAcDataCheckTimer)
{
    INT4                i4SetOption = SNMP_SUCCESS;

    gCapwapGlobals.CapwapGlbMib.u4CapwapBaseAcDataCheckTimer =
        u4CapwapBaseAcDataCheckTimer;

    nmhSetCmnNew (CapwapBaseAcDataCheckTimer, 0, CapwapMainTaskLock,
                  CapwapMainTaskUnLock, 0, 0, 0, i4SetOption, "%u",
                  gCapwapGlobals.CapwapGlbMib.u4CapwapBaseAcDataCheckTimer);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetCapwapBaseAcDTLSSessionDeleteTimer
 Input       :  The Indices
 Input       :  pu4CapwapBaseAcDTLSSessionDeleteTimer
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetCapwapBaseAcDTLSSessionDeleteTimer (UINT4
                                             *pu4CapwapBaseAcDTLSSessionDeleteTimer)
{
    *pu4CapwapBaseAcDTLSSessionDeleteTimer =
        gCapwapGlobals.CapwapGlbMib.u4CapwapBaseAcDTLSSessionDeleteTimer;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapSetCapwapBaseAcDTLSSessionDeleteTimer
 Input       :  u4CapwapBaseAcDTLSSessionDeleteTimer
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
CapwapSetCapwapBaseAcDTLSSessionDeleteTimer (UINT4
                                             u4CapwapBaseAcDTLSSessionDeleteTimer)
{
    INT4                i4SetOption = SNMP_SUCCESS;

    gCapwapGlobals.CapwapGlbMib.u4CapwapBaseAcDTLSSessionDeleteTimer =
        u4CapwapBaseAcDTLSSessionDeleteTimer;

    nmhSetCmnNew (CapwapBaseAcDTLSSessionDeleteTimer, 0, CapwapMainTaskLock,
                  CapwapMainTaskUnLock, 0, 0, 0, i4SetOption, "%u",
                  gCapwapGlobals.CapwapGlbMib.
                  u4CapwapBaseAcDTLSSessionDeleteTimer);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetCapwapBaseAcEchoInterval
 Input       :  The Indices
 Input       :  pu4CapwapBaseAcEchoInterval
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetCapwapBaseAcEchoInterval (UINT4 *pu4CapwapBaseAcEchoInterval)
{
    *pu4CapwapBaseAcEchoInterval =
        gCapwapGlobals.CapwapGlbMib.u4CapwapBaseAcEchoInterval;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapSetCapwapBaseAcEchoInterval
 Input       :  u4CapwapBaseAcEchoInterval
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
CapwapSetCapwapBaseAcEchoInterval (UINT4 u4CapwapBaseAcEchoInterval)
{
    INT4                i4SetOption = SNMP_SUCCESS;

    gCapwapGlobals.CapwapGlbMib.u4CapwapBaseAcEchoInterval =
        u4CapwapBaseAcEchoInterval;

    nmhSetCmnNew (CapwapBaseAcEchoInterval, 0, CapwapMainTaskLock,
                  CapwapMainTaskUnLock, 0, 0, 0, i4SetOption, "%u",
                  gCapwapGlobals.CapwapGlbMib.u4CapwapBaseAcEchoInterval);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetCapwapBaseAcRetransmitInterval
 Input       :  The Indices
 Input       :  pu4CapwapBaseAcRetransmitInterval
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetCapwapBaseAcRetransmitInterval (UINT4
                                         *pu4CapwapBaseAcRetransmitInterval)
{
    *pu4CapwapBaseAcRetransmitInterval =
        gCapwapGlobals.CapwapGlbMib.u4CapwapBaseAcRetransmitInterval;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapSetCapwapBaseAcRetransmitInterval
 Input       :  u4CapwapBaseAcRetransmitInterval
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
CapwapSetCapwapBaseAcRetransmitInterval (UINT4 u4CapwapBaseAcRetransmitInterval)
{
    INT4                i4SetOption = SNMP_SUCCESS;

    gCapwapGlobals.CapwapGlbMib.u4CapwapBaseAcRetransmitInterval =
        u4CapwapBaseAcRetransmitInterval;

    nmhSetCmnNew (CapwapBaseAcRetransmitInterval, 0, CapwapMainTaskLock,
                  CapwapMainTaskUnLock, 0, 0, 0, i4SetOption, "%u",
                  gCapwapGlobals.CapwapGlbMib.u4CapwapBaseAcRetransmitInterval);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetCapwapBaseAcSilentInterval
 Input       :  The Indices
 Input       :  pu4CapwapBaseAcSilentInterval
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetCapwapBaseAcSilentInterval (UINT4 *pu4CapwapBaseAcSilentInterval)
{
    *pu4CapwapBaseAcSilentInterval =
        gCapwapGlobals.CapwapGlbMib.u4CapwapBaseAcSilentInterval;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapSetCapwapBaseAcSilentInterval
 Input       :  u4CapwapBaseAcSilentInterval
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
CapwapSetCapwapBaseAcSilentInterval (UINT4 u4CapwapBaseAcSilentInterval)
{
    INT4                i4SetOption = SNMP_SUCCESS;

    gCapwapGlobals.CapwapGlbMib.u4CapwapBaseAcSilentInterval =
        u4CapwapBaseAcSilentInterval;

    nmhSetCmnNew (CapwapBaseAcSilentInterval, 0, CapwapMainTaskLock,
                  CapwapMainTaskUnLock, 0, 0, 0, i4SetOption, "%u",
                  gCapwapGlobals.CapwapGlbMib.u4CapwapBaseAcSilentInterval);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetCapwapBaseAcWaitDTLSTimer
 Input       :  The Indices
 Input       :  pu4CapwapBaseAcWaitDTLSTimer
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetCapwapBaseAcWaitDTLSTimer (UINT4 *pu4CapwapBaseAcWaitDTLSTimer)
{
    *pu4CapwapBaseAcWaitDTLSTimer =
        gCapwapGlobals.CapwapGlbMib.u4CapwapBaseAcWaitDTLSTimer;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapSetCapwapBaseAcWaitDTLSTimer
 Input       :  u4CapwapBaseAcWaitDTLSTimer
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
CapwapSetCapwapBaseAcWaitDTLSTimer (UINT4 u4CapwapBaseAcWaitDTLSTimer)
{
    INT4                i4SetOption = SNMP_SUCCESS;

    gCapwapGlobals.CapwapGlbMib.u4CapwapBaseAcWaitDTLSTimer =
        u4CapwapBaseAcWaitDTLSTimer;

    nmhSetCmnNew (CapwapBaseAcWaitDTLSTimer, 0, CapwapMainTaskLock,
                  CapwapMainTaskUnLock, 0, 0, 0, i4SetOption, "%u",
                  gCapwapGlobals.CapwapGlbMib.u4CapwapBaseAcWaitDTLSTimer);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetCapwapBaseAcWaitJoinTimer
 Input       :  The Indices
 Input       :  pu4CapwapBaseAcWaitJoinTimer
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetCapwapBaseAcWaitJoinTimer (UINT4 *pu4CapwapBaseAcWaitJoinTimer)
{
    *pu4CapwapBaseAcWaitJoinTimer =
        gCapwapGlobals.CapwapGlbMib.u4CapwapBaseAcWaitJoinTimer;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapSetCapwapBaseAcWaitJoinTimer
 Input       :  u4CapwapBaseAcWaitJoinTimer
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
CapwapSetCapwapBaseAcWaitJoinTimer (UINT4 u4CapwapBaseAcWaitJoinTimer)
{
    INT4                i4SetOption = SNMP_SUCCESS;

    gCapwapGlobals.CapwapGlbMib.u4CapwapBaseAcWaitJoinTimer =
        u4CapwapBaseAcWaitJoinTimer;

    nmhSetCmnNew (CapwapBaseAcWaitJoinTimer, 0, CapwapMainTaskLock,
                  CapwapMainTaskUnLock, 0, 0, 0, i4SetOption, "%u",
                  gCapwapGlobals.CapwapGlbMib.u4CapwapBaseAcWaitJoinTimer);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetCapwapBaseAcEcnSupport
 Input       :  The Indices
 Input       :  pi4CapwapBaseAcEcnSupport
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetCapwapBaseAcEcnSupport (INT4 *pi4CapwapBaseAcEcnSupport)
{
    *pi4CapwapBaseAcEcnSupport =
        gCapwapGlobals.CapwapGlbMib.i4CapwapBaseAcEcnSupport;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapSetCapwapBaseAcEcnSupport
 Input       :  i4CapwapBaseAcEcnSupport
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
CapwapSetCapwapBaseAcEcnSupport (INT4 i4CapwapBaseAcEcnSupport)
{
    INT4                i4SetOption = SNMP_SUCCESS;
    tWssIfCapDB        *pWssIfCapDB = NULL;

    gCapwapGlobals.CapwapGlbMib.i4CapwapBaseAcEcnSupport =
        i4CapwapBaseAcEcnSupport;

    nmhSetCmnNew (CapwapBaseAcEcnSupport, 0, CapwapMainTaskLock,
                  CapwapMainTaskUnLock, 0, 0, 0, i4SetOption, "%i",
                  gCapwapGlobals.CapwapGlbMib.i4CapwapBaseAcEcnSupport);

    /*setting module db */
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapDB, 0, sizeof (tWssIfCapDB));
    pWssIfCapDB->CapwapIsGetAllDB.bWtpEcnSupport = OSIX_TRUE;

    pWssIfCapDB->CapwapGetDB.u1WtpEcnSupport = (UINT1) i4CapwapBaseAcEcnSupport;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_SET_DB, pWssIfCapDB) !=
        OSIX_SUCCESS)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
        return SNMP_FAILURE;
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetCapwapBaseFailedDTLSAuthFailureCount
 Input       :  The Indices
 Input       :  pu4CapwapBaseFailedDTLSAuthFailureCount
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetCapwapBaseFailedDTLSAuthFailureCount (UINT4
                                               *pu4CapwapBaseFailedDTLSAuthFailureCount)
{
    *pu4CapwapBaseFailedDTLSAuthFailureCount =
        gCapwapGlobals.CapwapGlbMib.u4CapwapBaseFailedDTLSAuthFailureCount;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetCapwapBaseFailedDTLSSessionCount
 Input       :  The Indices
 Input       :  pu4CapwapBaseFailedDTLSSessionCount
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetCapwapBaseFailedDTLSSessionCount (UINT4
                                           *pu4CapwapBaseFailedDTLSSessionCount)
{
    *pu4CapwapBaseFailedDTLSSessionCount =
        gCapwapGlobals.CapwapGlbMib.u4CapwapBaseFailedDTLSSessionCount;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetCapwapBaseNtfWtpId
 Input       :  The Indices
 Input       :  pCapwapBaseNtfWtpId
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetCapwapBaseNtfWtpId (UINT1 *pCapwapBaseNtfWtpId)
{
    MEMCPY (pCapwapBaseNtfWtpId,
            gCapwapGlobals.CapwapGlbMib.au1CapwapBaseNtfWtpId,
            gCapwapGlobals.CapwapGlbMib.i4CapwapBaseNtfWtpIdLen);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetCapwapBaseNtfRadioId
 Input       :  The Indices
 Input       :  pu4CapwapBaseNtfRadioId
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetCapwapBaseNtfRadioId (UINT4 *pu4CapwapBaseNtfRadioId)
{
    *pu4CapwapBaseNtfRadioId =
        gCapwapGlobals.CapwapGlbMib.u4CapwapBaseNtfRadioId;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetCapwapBaseNtfChannelType
 Input       :  The Indices
 Input       :  pi4CapwapBaseNtfChannelType
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetCapwapBaseNtfChannelType (INT4 *pi4CapwapBaseNtfChannelType)
{
    *pi4CapwapBaseNtfChannelType =
        gCapwapGlobals.CapwapGlbMib.i4CapwapBaseNtfChannelType;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetCapwapBaseNtfAuthenMethod
 Input       :  The Indices
 Input       :  pi4CapwapBaseNtfAuthenMethod
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetCapwapBaseNtfAuthenMethod (INT4 *pi4CapwapBaseNtfAuthenMethod)
{
    *pi4CapwapBaseNtfAuthenMethod =
        gCapwapGlobals.CapwapGlbMib.i4CapwapBaseNtfAuthenMethod;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetCapwapBaseNtfChannelDownReason
 Input       :  The Indices
 Input       :  pi4CapwapBaseNtfChannelDownReason
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetCapwapBaseNtfChannelDownReason (INT4
                                         *pi4CapwapBaseNtfChannelDownReason)
{
    *pi4CapwapBaseNtfChannelDownReason =
        gCapwapGlobals.CapwapGlbMib.i4CapwapBaseNtfChannelDownReason;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetCapwapBaseNtfStationIdList
 Input       :  The Indices
 Input       :  pCapwapBaseNtfStationIdList
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetCapwapBaseNtfStationIdList (UINT1 *pCapwapBaseNtfStationIdList)
{
    MEMCPY (pCapwapBaseNtfStationIdList,
            gCapwapGlobals.CapwapGlbMib.au1CapwapBaseNtfStationIdList,
            gCapwapGlobals.CapwapGlbMib.i4CapwapBaseNtfStationIdListLen);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetCapwapBaseNtfAuthenFailureReason
 Input       :  The Indices
 Input       :  pi4CapwapBaseNtfAuthenFailureReason
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetCapwapBaseNtfAuthenFailureReason (INT4
                                           *pi4CapwapBaseNtfAuthenFailureReason)
{
    *pi4CapwapBaseNtfAuthenFailureReason =
        gCapwapGlobals.CapwapGlbMib.i4CapwapBaseNtfAuthenFailureReason;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetCapwapBaseNtfRadioOperStatusFlag
 Input       :  The Indices
 Input       :  pi4CapwapBaseNtfRadioOperStatusFlag
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetCapwapBaseNtfRadioOperStatusFlag (INT4
                                           *pi4CapwapBaseNtfRadioOperStatusFlag)
{
    *pi4CapwapBaseNtfRadioOperStatusFlag =
        gCapwapGlobals.CapwapGlbMib.i4CapwapBaseNtfRadioOperStatusFlag;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetCapwapBaseNtfRadioStatusCause
 Input       :  The Indices
 Input       :  pi4CapwapBaseNtfRadioStatusCause
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetCapwapBaseNtfRadioStatusCause (INT4 *pi4CapwapBaseNtfRadioStatusCause)
{
    *pi4CapwapBaseNtfRadioStatusCause =
        gCapwapGlobals.CapwapGlbMib.i4CapwapBaseNtfRadioStatusCause;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetCapwapBaseNtfJoinFailureReason
 Input       :  The Indices
 Input       :  pi4CapwapBaseNtfJoinFailureReason
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetCapwapBaseNtfJoinFailureReason (INT4
                                         *pi4CapwapBaseNtfJoinFailureReason)
{
    *pi4CapwapBaseNtfJoinFailureReason =
        gCapwapGlobals.CapwapGlbMib.i4CapwapBaseNtfJoinFailureReason;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetCapwapBaseNtfImageFailureReason
 Input       :  The Indices
 Input       :  pi4CapwapBaseNtfImageFailureReason
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetCapwapBaseNtfImageFailureReason (INT4
                                          *pi4CapwapBaseNtfImageFailureReason)
{
    *pi4CapwapBaseNtfImageFailureReason =
        gCapwapGlobals.CapwapGlbMib.i4CapwapBaseNtfImageFailureReason;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetCapwapBaseNtfConfigMsgErrorType
 Input       :  The Indices
 Input       :  pi4CapwapBaseNtfConfigMsgErrorType
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetCapwapBaseNtfConfigMsgErrorType (INT4
                                          *pi4CapwapBaseNtfConfigMsgErrorType)
{
    *pi4CapwapBaseNtfConfigMsgErrorType =
        gCapwapGlobals.CapwapGlbMib.i4CapwapBaseNtfConfigMsgErrorType;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetCapwapBaseNtfMsgErrorElements
 Input       :  The Indices
 Input       :  pCapwapBaseNtfMsgErrorElements
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetCapwapBaseNtfMsgErrorElements (UINT1 *pCapwapBaseNtfMsgErrorElements)
{
    MEMCPY (pCapwapBaseNtfMsgErrorElements,
            gCapwapGlobals.CapwapGlbMib.au1CapwapBaseNtfMsgErrorElements,
            gCapwapGlobals.CapwapGlbMib.i4CapwapBaseNtfMsgErrorElementsLen);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetCapwapBaseChannelUpDownNotifyEnable
 Input       :  The Indices
 Input       :  pi4CapwapBaseChannelUpDownNotifyEnable
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetCapwapBaseChannelUpDownNotifyEnable (INT4
                                              *pi4CapwapBaseChannelUpDownNotifyEnable)
{
    *pi4CapwapBaseChannelUpDownNotifyEnable =
        gCapwapGlobals.CapwapGlbMib.i4CapwapBaseChannelUpDownNotifyEnable;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapSetCapwapBaseChannelUpDownNotifyEnable
 Input       :  i4CapwapBaseChannelUpDownNotifyEnable
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
CapwapSetCapwapBaseChannelUpDownNotifyEnable (INT4
                                              i4CapwapBaseChannelUpDownNotifyEnable)
{
    INT4                i4SetOption = SNMP_SUCCESS;

    gCapwapGlobals.CapwapGlbMib.i4CapwapBaseChannelUpDownNotifyEnable =
        i4CapwapBaseChannelUpDownNotifyEnable;

    nmhSetCmnNew (CapwapBaseChannelUpDownNotifyEnable, 0, CapwapMainTaskLock,
                  CapwapMainTaskUnLock, 0, 0, 0, i4SetOption, "%i",
                  gCapwapGlobals.CapwapGlbMib.
                  i4CapwapBaseChannelUpDownNotifyEnable);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetCapwapBaseDecryptErrorNotifyEnable
 Input       :  The Indices
 Input       :  pi4CapwapBaseDecryptErrorNotifyEnable
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetCapwapBaseDecryptErrorNotifyEnable (INT4
                                             *pi4CapwapBaseDecryptErrorNotifyEnable)
{
    *pi4CapwapBaseDecryptErrorNotifyEnable =
        gCapwapGlobals.CapwapGlbMib.i4CapwapBaseDecryptErrorNotifyEnable;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapSetCapwapBaseDecryptErrorNotifyEnable
 Input       :  i4CapwapBaseDecryptErrorNotifyEnable
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
CapwapSetCapwapBaseDecryptErrorNotifyEnable (INT4
                                             i4CapwapBaseDecryptErrorNotifyEnable)
{
    INT4                i4SetOption = SNMP_SUCCESS;

    gCapwapGlobals.CapwapGlbMib.i4CapwapBaseDecryptErrorNotifyEnable =
        i4CapwapBaseDecryptErrorNotifyEnable;

    nmhSetCmnNew (CapwapBaseDecryptErrorNotifyEnable, 0, CapwapMainTaskLock,
                  CapwapMainTaskUnLock, 0, 0, 0, i4SetOption, "%i",
                  gCapwapGlobals.CapwapGlbMib.
                  i4CapwapBaseDecryptErrorNotifyEnable);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetCapwapBaseJoinFailureNotifyEnable
 Input       :  The Indices
 Input       :  pi4CapwapBaseJoinFailureNotifyEnable
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetCapwapBaseJoinFailureNotifyEnable (INT4
                                            *pi4CapwapBaseJoinFailureNotifyEnable)
{
    *pi4CapwapBaseJoinFailureNotifyEnable =
        gCapwapGlobals.CapwapGlbMib.i4CapwapBaseJoinFailureNotifyEnable;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapSetCapwapBaseJoinFailureNotifyEnable
 Input       :  i4CapwapBaseJoinFailureNotifyEnable
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
CapwapSetCapwapBaseJoinFailureNotifyEnable (INT4
                                            i4CapwapBaseJoinFailureNotifyEnable)
{
    INT4                i4SetOption = SNMP_SUCCESS;

    gCapwapGlobals.CapwapGlbMib.i4CapwapBaseJoinFailureNotifyEnable =
        i4CapwapBaseJoinFailureNotifyEnable;

    nmhSetCmnNew (CapwapBaseJoinFailureNotifyEnable, 0, CapwapMainTaskLock,
                  CapwapMainTaskUnLock, 0, 0, 0, i4SetOption, "%i",
                  gCapwapGlobals.CapwapGlbMib.
                  i4CapwapBaseJoinFailureNotifyEnable);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetCapwapBaseImageUpgradeFailureNotifyEnable
 Input       :  The Indices
 Input       :  pi4CapwapBaseImageUpgradeFailureNotifyEnable
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetCapwapBaseImageUpgradeFailureNotifyEnable (INT4
                                                    *pi4CapwapBaseImageUpgradeFailureNotifyEnable)
{
    *pi4CapwapBaseImageUpgradeFailureNotifyEnable =
        gCapwapGlobals.CapwapGlbMib.i4CapwapBaseImageUpgradeFailureNotifyEnable;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapSetCapwapBaseImageUpgradeFailureNotifyEnable
 Input       :  i4CapwapBaseImageUpgradeFailureNotifyEnable
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
CapwapSetCapwapBaseImageUpgradeFailureNotifyEnable (INT4
                                                    i4CapwapBaseImageUpgradeFailureNotifyEnable)
{
    INT4                i4SetOption = SNMP_SUCCESS;

    gCapwapGlobals.CapwapGlbMib.i4CapwapBaseImageUpgradeFailureNotifyEnable =
        i4CapwapBaseImageUpgradeFailureNotifyEnable;

    nmhSetCmnNew (CapwapBaseImageUpgradeFailureNotifyEnable, 0,
                  CapwapMainTaskLock, CapwapMainTaskUnLock, 0, 0, 0,
                  i4SetOption, "%i",
                  gCapwapGlobals.CapwapGlbMib.
                  i4CapwapBaseImageUpgradeFailureNotifyEnable);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetCapwapBaseConfigMsgErrorNotifyEnable
 Input       :  The Indices
 Input       :  pi4CapwapBaseConfigMsgErrorNotifyEnable
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetCapwapBaseConfigMsgErrorNotifyEnable (INT4
                                               *pi4CapwapBaseConfigMsgErrorNotifyEnable)
{
    *pi4CapwapBaseConfigMsgErrorNotifyEnable =
        gCapwapGlobals.CapwapGlbMib.i4CapwapBaseConfigMsgErrorNotifyEnable;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapSetCapwapBaseConfigMsgErrorNotifyEnable
 Input       :  i4CapwapBaseConfigMsgErrorNotifyEnable
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
CapwapSetCapwapBaseConfigMsgErrorNotifyEnable (INT4
                                               i4CapwapBaseConfigMsgErrorNotifyEnable)
{
    INT4                i4SetOption = SNMP_SUCCESS;

    gCapwapGlobals.CapwapGlbMib.i4CapwapBaseConfigMsgErrorNotifyEnable =
        i4CapwapBaseConfigMsgErrorNotifyEnable;

    nmhSetCmnNew (CapwapBaseConfigMsgErrorNotifyEnable, 0, CapwapMainTaskLock,
                  CapwapMainTaskUnLock, 0, 0, 0, i4SetOption, "%i",
                  gCapwapGlobals.CapwapGlbMib.
                  i4CapwapBaseConfigMsgErrorNotifyEnable);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetCapwapBaseRadioOperableStatusNotifyEnable
 Input       :  The Indices
 Input       :  pi4CapwapBaseRadioOperableStatusNotifyEnable
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetCapwapBaseRadioOperableStatusNotifyEnable (INT4
                                                    *pi4CapwapBaseRadioOperableStatusNotifyEnable)
{
    *pi4CapwapBaseRadioOperableStatusNotifyEnable =
        gCapwapGlobals.CapwapGlbMib.i4CapwapBaseRadioOperableStatusNotifyEnable;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapSetCapwapBaseRadioOperableStatusNotifyEnable
 Input       :  i4CapwapBaseRadioOperableStatusNotifyEnable
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
CapwapSetCapwapBaseRadioOperableStatusNotifyEnable (INT4
                                                    i4CapwapBaseRadioOperableStatusNotifyEnable)
{
    INT4                i4SetOption = SNMP_SUCCESS;

    gCapwapGlobals.CapwapGlbMib.i4CapwapBaseRadioOperableStatusNotifyEnable =
        i4CapwapBaseRadioOperableStatusNotifyEnable;

    nmhSetCmnNew (CapwapBaseRadioOperableStatusNotifyEnable, 0,
                  CapwapMainTaskLock, CapwapMainTaskUnLock, 0, 0, 0,
                  i4SetOption, "%i",
                  gCapwapGlobals.CapwapGlbMib.
                  i4CapwapBaseRadioOperableStatusNotifyEnable);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetCapwapBaseAuthenFailureNotifyEnable
 Input       :  The Indices
 Input       :  pi4CapwapBaseAuthenFailureNotifyEnable
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetCapwapBaseAuthenFailureNotifyEnable (INT4
                                              *pi4CapwapBaseAuthenFailureNotifyEnable)
{
    *pi4CapwapBaseAuthenFailureNotifyEnable =
        gCapwapGlobals.CapwapGlbMib.i4CapwapBaseAuthenFailureNotifyEnable;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapSetCapwapBaseAuthenFailureNotifyEnable
 Input       :  i4CapwapBaseAuthenFailureNotifyEnable
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
CapwapSetCapwapBaseAuthenFailureNotifyEnable (INT4
                                              i4CapwapBaseAuthenFailureNotifyEnable)
{
    INT4                i4SetOption = SNMP_SUCCESS;

    gCapwapGlobals.CapwapGlbMib.i4CapwapBaseAuthenFailureNotifyEnable =
        i4CapwapBaseAuthenFailureNotifyEnable;

    nmhSetCmnNew (CapwapBaseAuthenFailureNotifyEnable, 0, CapwapMainTaskLock,
                  CapwapMainTaskUnLock, 0, 0, 0, i4SetOption, "%i",
                  gCapwapGlobals.CapwapGlbMib.
                  i4CapwapBaseAuthenFailureNotifyEnable);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetFirstCapwapBaseWtpStationTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pCapwapCapwapBaseWtpStateEntry or NULL
****************************************************************************/
INT1
CapwapGetFirstCapwapBaseStationTable (tCapwapCapwapBaseStationEntry *
                                      pCapwapCapwapBaseStationEntry)
{
#ifdef WLC_WANTED
    tWssIfAuthStateDB  *pStaDB = NULL;
    pStaDB = ((tWssIfAuthStateDB *) RBTreeGetFirst (gWssStaStateDB));

    if (pStaDB == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMCPY (pCapwapCapwapBaseStationEntry->MibObject.au1CapwapBaseStationId,
            pStaDB->stationMacAddress, MAC_ADDR_LEN);

    pCapwapCapwapBaseStationEntry->MibObject.i4CapwapBaseStationIdLen =
        MAC_ADDR_LEN;
#else
    UNUSED_PARAM (pCapwapCapwapBaseStationEntry);
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetNextCapwapBaseWtpStateTable
 Input       :  pCurrentCapwapCapwapBaseWtpStateEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextCapwapCapwapBaseWtpStateEntry or NULL
****************************************************************************/
INT1
CapwapGetNextCapwapBaseStationTable (tCapwapCapwapBaseStationEntry *
                                     pCurrentCapwapCapwapBaseStationEntry,
                                     tCapwapCapwapBaseStationEntry *
                                     pNextCapwapCapwapBaseStationEntry)
{
#ifdef WLC_WANTED
    tWssIfAuthStateDB  *pStaDB = NULL;
    tWssIfAuthStateDB   staDB;

    MEMSET (&staDB, 0, sizeof (tWssIfAuthStateDB));

    MEMCPY (staDB.stationMacAddress,
            pCurrentCapwapCapwapBaseStationEntry->MibObject.
            au1CapwapBaseStationId, MAC_ADDR_LEN);

    pStaDB = ((tWssIfAuthStateDB *) RBTreeGetNext (gWssStaStateDB,
                                                   (tRBElem *) & staDB, NULL));

    if (pStaDB == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMCPY (pNextCapwapCapwapBaseStationEntry->MibObject.au1CapwapBaseStationId,
            pStaDB->stationMacAddress, MAC_ADDR_LEN);
    pNextCapwapCapwapBaseStationEntry->MibObject.i4CapwapBaseStationIdLen =
        MAC_ADDR_LEN;
#else
    UNUSED_PARAM (pCurrentCapwapCapwapBaseStationEntry);
    UNUSED_PARAM (pNextCapwapCapwapBaseStationEntry);
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapSetCapwapBaseFailedDTLSAuthFailureCount
 Input       :  The Indices
 Input       :  NONE
 Descritpion :  This Routine will Increment
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapSetCapwapBaseFailedDTLSAuthFailureCount ()
{
    UINT4               u4GetCapwapBaseFailedDTLSAuthFailureCount = 0;
    if (CapwapGetCapwapBaseFailedDTLSAuthFailureCount
        (&u4GetCapwapBaseFailedDTLSAuthFailureCount) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    gCapwapGlobals.CapwapGlbMib.u4CapwapBaseFailedDTLSAuthFailureCount
        = u4GetCapwapBaseFailedDTLSAuthFailureCount + 1;
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapSetCapwapBaseFailedDTLSSessionCount
 Input       :  The Indices
 Input       :  NONE
 Descritpion :  This Routine will Increment
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapSetCapwapBaseFailedDTLSSessionCount ()
{
    UINT4               u4GetCapwapBaseFailedDTLSSessionCount = 0;

    if (CapwapGetCapwapBaseFailedDTLSSessionCount
        (&u4GetCapwapBaseFailedDTLSSessionCount) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    gCapwapGlobals.CapwapGlbMib.u4CapwapBaseFailedDTLSSessionCount =
        u4GetCapwapBaseFailedDTLSSessionCount + 1;
    return OSIX_SUCCESS;
}

/****************************************************************************
  * Function    :  CapwapGetFragReassembleStatus
  * Input       :  WtpProfileId
  * Descritpion :  This Routine checks gets value of Fragmentation&Reassembly
  *                enabled status for a particular WTP profile.
  * Output      :  None
  * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
***************************************************************************/
INT4
CapwapGetFragReassembleStatus (UINT4 u4CapwapBaseWtpProfileId,
                               UINT1 *pu1FragReassembleStatus)
{
    tWssIfCapDB        *pCapwapGetDB = NULL;
    WSS_IF_DB_TEMP_MEM_ALLOC (pCapwapGetDB, OSIX_FAILURE)
        MEMSET (pCapwapGetDB, 0, sizeof (tWssIfCapDB));

    pCapwapGetDB->CapwapIsGetAllDB.bWtpProfileId = OSIX_TRUE;
    pCapwapGetDB->CapwapGetDB.u2ProfileId = (UINT2) u4CapwapBaseWtpProfileId;
    pCapwapGetDB->CapwapIsGetAllDB.bFragReassembleStatus = OSIX_TRUE;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pCapwapGetDB) !=
        OSIX_SUCCESS)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pCapwapGetDB);
        return OSIX_FAILURE;
    }

    *pu1FragReassembleStatus = pCapwapGetDB->CapwapGetDB.u1FragReassembleStatus;

    WSS_IF_DB_TEMP_MEM_RELEASE (pCapwapGetDB);
    return OSIX_SUCCESS;
}

/****************************************************************************
  * Function    :  CapwapSetFragReassembleStatus
  * Input       :  WtpProfileId , FragReassembleStatus
  * Descritpion :  This Routine checks sets value of Fragmentation&Reassembly
  *                enabled status for a particular WTP profile.
  * Output      :  None
  * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
***************************************************************************/
INT4
CapwapSetFragReassembleStatus (UINT4 u4CapwapBaseWtpProfileId,
                               UINT1 u1FragReassembleStatus)
{
    tWssIfCapDB        *pCapwapGetDB = NULL;
    WSS_IF_DB_TEMP_MEM_ALLOC (pCapwapGetDB, OSIX_FAILURE)
        MEMSET (pCapwapGetDB, 0, sizeof (tWssIfCapDB));

    pCapwapGetDB->CapwapIsGetAllDB.bWtpProfileId = OSIX_TRUE;
    pCapwapGetDB->CapwapGetDB.u2ProfileId = (UINT2) u4CapwapBaseWtpProfileId;
    pCapwapGetDB->CapwapIsGetAllDB.bFragReassembleStatus = OSIX_TRUE;
    pCapwapGetDB->CapwapGetDB.u1FragReassembleStatus = u1FragReassembleStatus;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_SET_DB, pCapwapGetDB) !=
        OSIX_SUCCESS)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pCapwapGetDB);
        return OSIX_FAILURE;
    }

    WSS_IF_DB_TEMP_MEM_RELEASE (pCapwapGetDB);
    return OSIX_SUCCESS;
}

/****************************************************************************
  * Function    :  CapwapGetFirstCapwapBaseWtpTable
  * Input       :  None
  * Descritpion :  This Routine is to get the fisrt index of Capwap Wtp Table
  *                
  * Output      :  None
  * Returns     :  pCapwapBaseWtpEntry
***************************************************************************/
tWssIfProfileMacMap *
CapwapGetFirstCapwapBaseWtpTable ()
{
    tWssIfProfileMacMap *pCapwapBaseWtpEntry = NULL;

    /*we are accessing gCapProfileMacMapTable for getting the entries for this
     * mib*/
    pCapwapBaseWtpEntry = RBTreeGetFirst (gCapProfileMacMapTable);

    return pCapwapBaseWtpEntry;
}

/****************************************************************************
  * Function    :  CapwapGetCapwapBaseWtpTable
  * Input       :  pu1MacAddr
  * Descritpion :  This Routine is to get the fisrt index of Capwap Wtp Table
  *                
  * Output      :  None
  * Returns     :  CapwapBaseWtpEntry
***************************************************************************/
tWssIfProfileMacMap *
CapwapGetCapwapBaseWtpTable (UINT1 *pu1MacAddr)
{
    tWssIfProfileMacMap CapwapBaseWtpEntry;

    MEMSET (&CapwapBaseWtpEntry, 0, sizeof (tWssIfProfileMacMap));

    MEMCPY (CapwapBaseWtpEntry.MacAddr, pu1MacAddr, MAC_ADDR_LEN);
    /*we are accessing gCapProfileMacMapTable for getting the entries for this
     * mib*/
    return (RBTreeGet (gCapProfileMacMapTable, &CapwapBaseWtpEntry));
}

/****************************************************************************
  * Function    :  CapwapGetNextCapwapBaseWtpTable
  * Input       :  pwtpProfileMacEntry
  * Descritpion :  This Routine is to get the Next index of Capwap Wtp Table
  *
  * Output      :  None
  * Returns     :  pCapwapBaseWtpEntry
***************************************************************************/
tWssIfProfileMacMap *
CapwapGetNextCapwapBaseWtpTable (tWssIfProfileMacMap * pwtpProfileMacEntry)
{
    tWssIfProfileMacMap *pCapwapBaseWtpEntry = NULL;

    /*we are accessing gCapProfileMacMapTable for getting the entries for this
     * mib*/
    pCapwapBaseWtpEntry = (tWssIfProfileMacMap *) RBTreeGetNext
        (gCapProfileMacMapTable, (tRBElem *) pwtpProfileMacEntry, NULL);

    return pCapwapBaseWtpEntry;
}
