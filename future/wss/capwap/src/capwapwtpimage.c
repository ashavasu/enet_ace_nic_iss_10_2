/********************************************************************
 *
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 * $Id: capwapwtpimage.c,v 1.4 2017/11/24 10:37:03 siva Exp $       
 * Description: This file contains the CAPWAP Join state routines
 *******************************************************************/
#ifndef __CAPWAP_WTPIMAGE_C__
#define __CAPWAP_WTPIMAGE_C__
#include "capwapinc.h"
#include "aphdlr.h"
#include "aphdlrprot.h"
#include "wssifradiodb.h"
#include "msr.h"
#include "issnvram.h"
#include "wtpnvram.h"

BOOL1               gbCapwapImageData = OSIX_FALSE;
INT4                i4FileWriteCount = 0;
extern INT4         IssCustFirmwareUpgrade (CONST UINT1 *pu1SrcImagePath,
                                            UINT1 *pu1SrcImage);

/***********************************************************************
*  Function Name   : CapwapProcessImageDataRsp                         *
*  Description     : The function processes the received CAPWAP        *
*                    image data response msg.                          *
*  Input(s)        : pBuf, pImageDataRspMsg, pSessEntry                *
*  Output(s)       : None.                                             *
*  Returns         : None                                              *
************************************************************************/
INT4
CapwapProcessImageDataRsp (tSegment * pBuf,
                           tCapwapControlPacket * pImageDataRspMsg,
                           tRemoteSessionManager * pSessEntry)
{
    UINT1              *pRcvBuf = NULL;
    INT4                i4Status = OSIX_SUCCESS;
    tImageDataReq       ImageDataReq;
    UINT1               u1IsNotLinear = OSIX_FALSE;
    UINT2               u2PacketLen;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;

    CAPWAP_FN_ENTRY ();

    MEMSET (&ImageDataReq, 0, sizeof (tImageDataReq));

    if (pSessEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Remote session Module is NULL, return Failure \r\n");
        return OSIX_FAILURE;
    }

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    u2PacketLen = (UINT2) CAPWAP_GET_BUF_LEN (pBuf);
    pRcvBuf = CAPWAP_BUF_IF_LINEAR (pBuf, 0, u2PacketLen);
    if (pRcvBuf == NULL)
    {
        pRcvBuf = (UINT1 *) MemAllocMemBlk (CAPWAP_PKTBUF_POOLID);
        CAPWAP_COPY_FROM_BUF (pBuf, pRcvBuf, 0, u2PacketLen);
        u1IsNotLinear = OSIX_TRUE;
    }

    if (CapwapValidateWTPImageDataRespMsgElems (pRcvBuf, pImageDataRspMsg,
                                                (tImageDataRsp *) pSessEntry) ==
        OSIX_FAILURE)
    {
        if (pSessEntry->eCurrentState == CAPWAP_RUN)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to validate Image Data while in RUN state\r\n");

            if (u1IsNotLinear == OSIX_TRUE)
            {
                MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pRcvBuf);
            }
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to validate Join Response Message Elements \r\n");

        if (u1IsNotLinear == OSIX_TRUE)
        {
            MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pRcvBuf);
        }
        CAPWAP_TRC (CAPWAP_FSM_TRC, "Enter the CAPWAP DTLS TD State \r\n");
        pSessEntry->eCurrentState = CAPWAP_DTLSTD;

        if (CapwapShutDownDTLS (pSessEntry) == OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to Disconnect the Sesion \r\n");
        }
        pWssIfCapwapDB->u4DestIp = pSessEntry->remoteIpAddr.u4_addr[0];
        pWssIfCapwapDB->u4DestPort = pSessEntry->u4RemoteCtrlPort;
        WssIfProcessCapwapDBMsg (WSS_CAPWAP_DELETE_RSM, pWssIfCapwapDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    if (u1IsNotLinear == OSIX_TRUE)
    {
        MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pRcvBuf);
    }

    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    CAPWAP_FN_EXIT ();
    return (i4Status);

}

/************************************************************************/
/*  Function Name   : CapwapProcessImageDataReq                         */
/*  Description     : The function processes the received CAPWAP        */
/*                    join response. This function handling is mainly   */
/*                    done in the AP and it validates the received join */
/*                    response and updates WSS-AP.                      */
/*  Input(s)        : JoinRespMsg- Join response packet received        */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
CapwapProcessImageDataReq (tSegment * pBuf,
                           tCapwapControlPacket * pImageDataReqMsg,
                           tRemoteSessionManager * pSessEntry)
{

    INT4                i4Status = OSIX_FAILURE;
    UINT4               u4MsgLen = 0;
    tImageDataRsp       ImageDataRsp;
    tSegment           *pTxBuf = NULL;
    UINT1              *pu1TxBuf = NULL;
    UINT1              *pRcvdBuf = NULL;
    UINT1              *pRcvdTmpBuf = NULL;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tWssIfCapDB        *pWssIfCapwapDBTmr = NULL;
    UINT1               u1IsNotLinear = OSIX_FALSE;
    UINT2               u2PacketLen;
    UINT2               u2NumOptionalMsg = 0;
    UINT1               u1Index = 0;
    UINT2               u2MsgType = 0;
    UINT4               u4Offset = 0;
    UINT2               u2Type = 0;
    UINT2               u2Len = 0;
    UINT2               u2ReadLen = 0;
    UINT1               u1DataType = 0;
    UINT1               au1ImageData[IMAGE_READ_FLASH_SIZE];
    UINT1               u1StartEchoResponseFlag = OSIX_FALSE;
    UINT2               u2IntId = 0;
    UINT2               u2EchoInterval = 0;
    UINT1               au1Flash[IMAGE_READ_FLASH_SIZE];
    UINT1               au1File[IMAGE_READ_FLASH_SIZE];
    CHR1                ac1Command[CAPWAP_IMAGE_BUFFER_SIZE];

    CAPWAP_FN_ENTRY ();

    if (pSessEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Session Entry is NULL, Return Failure \r\n");
        return OSIX_FAILURE;
    }

    MEMSET (&ac1Command, 0, CAPWAP_IMAGE_BUFFER_SIZE);
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDBTmr, OSIX_FAILURE)
        MEMSET (&ImageDataRsp, 0, sizeof (tImageDataRsp));
    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (pWssIfCapwapDBTmr, 0, sizeof (tWssIfCapDB));
    MEMSET (au1Flash, 0, IMAGE_READ_FLASH_SIZE);
    MEMSET (au1File, 0, IMAGE_READ_FLASH_SIZE);

    pWssIfCapwapDBTmr->CapwapIsGetAllDB.bWtpEchoInterval = OSIX_TRUE;
    pWssIfCapwapDBTmr->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    pWssIfCapwapDBTmr->CapwapGetDB.u2WtpInternalId = u2IntId;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDBTmr)
        == OSIX_SUCCESS)
    {
        u2EchoInterval = pWssIfCapwapDBTmr->CapwapGetDB.u2WtpEchoInterval;
    }

    u2PacketLen = (UINT2) CAPWAP_GET_BUF_LEN (pBuf);
    pRcvdBuf = CAPWAP_BUF_IF_LINEAR (pBuf, 0, u2PacketLen);
    if (pRcvdBuf == NULL)
    {
        pRcvdBuf = (UINT1 *) MemAllocMemBlk (CAPWAP_PKTBUF_POOLID);
        CAPWAP_COPY_FROM_BUF (pBuf, pRcvdBuf, 0, u2PacketLen);
        u1IsNotLinear = OSIX_TRUE;
    }

    if (CapwapValidateIpAddress (pSessEntry->remoteIpAddr.u4_addr[0])
        == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "WTP is not suppported. Entry is in the black list \n");
        if (u1IsNotLinear == OSIX_TRUE)
        {
            MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pRcvdBuf);
        }
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDBTmr);
        return OSIX_FAILURE;    /* return proper error code */
    }

    /* Validate the Image Data request message. 
     * if it is success construct the Join response */
    if ((CapwapValidateWTPImageDataReqMsgElems (pRcvdBuf, pImageDataReqMsg,
                                                pSessEntry) == OSIX_SUCCESS))
    {
        /* Read the Image data from the buffer and write to here
         * in the file */

        pRcvdTmpBuf = pRcvdBuf;
        u2NumOptionalMsg = pImageDataReqMsg->numOptionalElements;
        for (u1Index = 0; u1Index < u2NumOptionalMsg; u1Index++)
        {
            /* Get the message element type */
            u2MsgType = pImageDataReqMsg->capwapMsgElm[u1Index].u2MsgType;

            if (u2MsgType == IMAGE_DATA)
            {
                u4Offset = pImageDataReqMsg->capwapMsgElm[u1Index].pu2Offset[0];
                pRcvdBuf += u4Offset;
                CAPWAP_GET_2BYTE (u2Type, pRcvdBuf);
                CAPWAP_GET_2BYTE (u2Len, pRcvdBuf);
                CAPWAP_GET_1BYTE (u1DataType, pRcvdBuf);
                u2ReadLen = u2Len - 1;
                MEMSET (&au1ImageData, 0, u2ReadLen);
                CAPWAP_GET_NBYTE (au1ImageData, pRcvdBuf, u2ReadLen);
                break;
            }
        }
        pRcvdBuf = pRcvdTmpBuf;

        if (u1IsNotLinear == OSIX_TRUE)
        {
            MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pRcvdBuf);
        }

        /* This check is added to prevent accepting and writing same image data
         * twice. This increases the image size and leads to corruption */
        /* To avoid repeated Data request during upgrade */
        if ((pSessEntry->u1WriteFileOpened) &&
            (pSessEntry->u1LastImgDataSeqNum ==
             pImageDataReqMsg->capwapCtrlHdr.u1SeqNum))
        {
            pImageDataReqMsg->u4ReturnCode = IMAGEDATA_FAIL_OTHER_ERROR;
        }
        else
        {
            if (u1DataType == DATA_TRANS_DATA)
            {
                gbCapwapImageData = OSIX_TRUE;
                if (pSessEntry->u1WriteFileOpened == OSIX_TRUE)
                {
                    i4Status =
                        CapwapWriteToImage (au1ImageData, pSessEntry,
                                            u2ReadLen);
                    if (i4Status != OSIX_SUCCESS)
                    {
                        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                    "capwapProcessImageDataRequest:WriteToImage Failed \r\n");
                        pImageDataReqMsg->u4ReturnCode =
                            IMAGEDATA_FAIL_OTHER_ERROR;
                    }
                }
                else
                {
                    i4Status = CapwapOpenAndWriteToImage (au1ImageData,
                                                          pSessEntry,
                                                          u2ReadLen);
                    if (i4Status != OSIX_SUCCESS)
                    {
                        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                    "capwapProcessImageDataRequest:WriteToImage Failed \r\n");
                        pImageDataReqMsg->u4ReturnCode =
                            IMAGEDATA_FAIL_OTHER_ERROR;
                    }
                    u1StartEchoResponseFlag = OSIX_TRUE;
                }
            }
            else if (u1DataType == DATA_TRANS_EOF)
            {

                gbCapwapImageData = OSIX_FALSE;
                pSessEntry->u1EndofFileReached = OSIX_TRUE;

                if (pSessEntry->u1WriteFileOpened == OSIX_TRUE)
                {
                    i4Status =
                        CapwapWriteToImage (au1ImageData, pSessEntry,
                                            u2ReadLen);
                    if (i4Status != OSIX_SUCCESS)
                    {
                        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                    "capwapProcessImageDataRequest:WriteToImage Failed \r\n");
                        /* Result Code is 16 Image Data Error (Other Error) */
                        pImageDataReqMsg->u4ReturnCode =
                            IMAGEDATA_FAIL_OTHER_ERROR;
                    }
                }
                else
                {
                    i4Status = CapwapOpenAndWriteToImage (au1ImageData,
                                                          pSessEntry,
                                                          u2ReadLen);
                    if (i4Status != OSIX_SUCCESS)
                    {
                        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                    "capwapProcessImageDataRequest:WriteToImage Failed \r\n");
                        pImageDataReqMsg->u4ReturnCode =
                            IMAGEDATA_FAIL_OTHER_ERROR;
                    }
                }
            }
        }
        CapwapUpdateResultCode (&ImageDataRsp.ResultCode,
                                pImageDataReqMsg->u4ReturnCode, &u4MsgLen);

        /* Get the values to send out the Image Data Response */
        if (CapwapGetWTPImageDataRspMsgElements (&ImageDataRsp, &u4MsgLen)
            == OSIX_SUCCESS)
        {
            /* update the seq number */
            ImageDataRsp.u1SeqNum = pImageDataReqMsg->capwapCtrlHdr.u1SeqNum;
            ImageDataRsp.u2CapwapMsgElemenLen = u4MsgLen + 3;
            pTxBuf = CAPWAP_ALLOCATE_CRU_BUF (CAPWAP_MAX_PKT_LEN, 0);
            if (pTxBuf == NULL)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "capwapProcessImageDataRequest:Memory Allocation Failed \r\n");
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDBTmr);
                return OSIX_FAILURE;
            }
            pu1TxBuf = CAPWAP_BUF_IF_LINEAR (pTxBuf, 0, CAPWAP_MAX_PKT_LEN);
            if (pu1TxBuf == NULL)
            {
                pu1TxBuf = (UINT1 *) MemAllocMemBlk (CAPWAP_PKTBUF_POOLID);
                if (pu1TxBuf == NULL)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "capwapProcessImageDataRequest: pu1TxBuf is NULL \r\n");
                    CAPWAP_RELEASE_CRU_BUF (pTxBuf);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDBTmr);
                    return OSIX_FAILURE;
                }
                u1IsNotLinear = OSIX_TRUE;
            }
            if ((CapwapAssembleImageDataRsp (ImageDataRsp.u2CapwapMsgElemenLen,
                                             &ImageDataRsp,
                                             pu1TxBuf)) == OSIX_FAILURE)
            {
                if (u1IsNotLinear == OSIX_TRUE)
                {
                    MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pu1TxBuf);
                }
                CAPWAP_RELEASE_CRU_BUF (pTxBuf);
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDBTmr);
                return OSIX_FAILURE;
            }
            if (u1IsNotLinear == OSIX_TRUE)
            {
                /* If the CRU buffer memory is not linear */
                CAPWAP_COPY_TO_BUF (pTxBuf, pu1TxBuf, 0, CAPWAP_MAX_PKT_LEN);
                MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pu1TxBuf);
            }
            i4Status = CapwapTxMessage (pTxBuf,
                                        pSessEntry,
                                        ImageDataRsp.u2CapwapMsgElemenLen +
                                        CAPWAP_MSG_ELE_LEN,
                                        WSS_CAPWAP_802_11_CTRL_PKT);
            if (i4Status == OSIX_SUCCESS)
            {
                pSessEntry->lastTransmitedSeqNum++;
                pSessEntry->u1LastImgDataSeqNum = ImageDataRsp.u1SeqNum;
            }
            else
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Failed to Transmit the packet \r\n");
                CAPWAP_RELEASE_CRU_BUF (pTxBuf);
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDBTmr);
                return OSIX_FAILURE;
            }
            CAPWAP_RELEASE_CRU_BUF (pTxBuf);
        }
        if (u1DataType == DATA_TRANS_DATA)
        {
            if (pSessEntry->u1ImageDataTimerRunning)
            {
                CapwapResetTmr (pSessEntry, CAPWAP_IMAGEDATA_START_TMR,
                                CAPWAP_IMAGE_DATA_START_TIMEOUT);
            }
            else
            {
                CapwapTmrStart (pSessEntry, CAPWAP_IMAGEDATA_START_TMR,
                                CAPWAP_IMAGE_DATA_START_TIMEOUT);
                pSessEntry->u1ImageDataTimerRunning = OSIX_TRUE;
            }
            /* Image Data transfer going to start, More data to come,
             * so initiate echo response */
            if (u1StartEchoResponseFlag == OSIX_TRUE)
            {
                /* Start echo interval timer and send Image Data Req */
                i4Status = CapwapTmrStart (pSessEntry, CAPWAP_ECHO_INTERVAL_TMR,
                                           u2EchoInterval);
                if (i4Status == OSIX_FAILURE)
                {
                    /* Handle the failure */
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "Failed to start the Echo Interval timer in Image Data State \r\n");
                }
            }
        }
        else if (u1DataType == DATA_TRANS_EOF)
        {
            CapwapTmrStop (pSessEntry, CAPWAP_IMAGEDATA_START_TMR);
            if (pSessEntry->eCurrentState == CAPWAP_IMAGEDATA)
            {

                /* Before Run State */

                CAPWAP_TRC (CAPWAP_MGMT_TRC,
                            "Image Download completed. Reset the board \r\n");
                i4Status = CapwapTmrStop (pSessEntry, CAPWAP_ECHO_INTERVAL_TMR);

                SPRINTF ((char *) au1File, "%s%s", IMAGE_DATA_FLASH_LOC,
                         pSessEntry->ImageId.data);

                PRINTF (" \n GOING TO RESTART ");
#ifndef AP200_WANTED
#ifndef DAK_WANTED
#ifdef WASP_WANTED
                /* Flash the Image */
                if (IssCustFirmwareUpgrade (au1File, pSessEntry->ImageId.data)
                    == ISS_SUCCESS)
                {
                    SNPRINTF (ac1Command, sizeof (ac1Command), "%s %s*",
                              IMAGE_DATA_REMOVE_CMD, IMAGE_DATA_FLASH_CONF_LOC);
                    system (ac1Command);
                    /*Dummy image created in flash path IMAGE_DATA_FLASH_CONF_LOC   */
                    CapwapOpenImage (pSessEntry);
                }
#endif
#endif
#endif
#ifdef AP200_WANTED
                SNPRINTF (ac1Command, sizeof (ac1Command), "%s %s*",
                          IMAGE_DATA_REMOVE_CMD, IMAGE_DATA_FLASH_CONF_LOC);
                system (ac1Command);
                MEMSET (&ac1Command, 0, CAPWAP_IMAGE_BUFFER_SIZE);
                /*Kernel Upgrade */
                if (strcmp
                    (CapwapGetFilenameExt (pSessEntry->ImageId.data),
                     "bin") == 0)
                {
                    /*Dummy image created in flash path IMAGE_DATA_FLASH_CONF_LOC   */
                    CapwapOpenImage (pSessEntry);
                    PRINTF ("\n SysUpgrade done");
                }
                else
                {
                    MEMSET (&ac1Command, 0, CAPWAP_IMAGE_BUFFER_SIZE);
                    SNPRINTF (ac1Command, sizeof (ac1Command), "%s %s%s %s",
                              IMAGE_DATA_CP_CMD, IMAGE_DATA_FLASH_LOC,
                              pSessEntry->ImageId.data,
                              IMAGE_DATA_FLASH_TEMP_CONF_LOC);
                    system (ac1Command);
                    /*Dummy image created in flash path IMAGE_DATA_FLASH_CONF_LOC   */
                    CapwapOpenImage (pSessEntry);
                }
#endif

#ifdef DAK_WANTED
                /* Upgrading the image */
                MEMSET (&ac1Command, 0, CAPWAP_IMAGE_BUFFER_SIZE);
                SNPRINTF (ac1Command, sizeof (ac1Command), "%s %s %s",
                          BOOT_LDR_SET_ENV, BOOT_IMG_VAR,
                          pSessEntry->ImageId.data);
                system (ac1Command);

                MEMSET (&ac1Command, 0, CAPWAP_IMAGE_BUFFER_SIZE);
                SNPRINTF (ac1Command, sizeof (ac1Command), "%s %s%s",
                          IMAGE_SYS_UPGRADE_CMD, IMAGE_DATA_FLASH_CONF_LOC,
                          pSessEntry->ImageId.data);
                system (ac1Command);
#endif

#ifndef DAK_WANTED
                ApHdlrEnterReset ();
#endif
            }
            else
            {

                /* After Run state */
                SPRINTF ((char *) au1File, "%s%s", IMAGE_DATA_FLASH_LOC,
                         pSessEntry->ImageId.data);

#ifndef AP200_WANTED
#ifndef DAK_WANTED
#ifdef WASP_WANTED
                /* Flash the Image */
                if (IssCustFirmwareUpgrade (au1File, pSessEntry->ImageId.data)
                    == ISS_SUCCESS)
                {
                    SNPRINTF (ac1Command, sizeof (ac1Command), "%s %s*",
                              IMAGE_DATA_REMOVE_CMD, IMAGE_DATA_FLASH_CONF_LOC);
                    system (ac1Command);

                    /*Dummy image created in flash path IMAGE_DATA_FLASH_CONF_LOC   */
                    CapwapOpenImage (pSessEntry);
                }
#endif
#endif
#endif
#ifdef AP200_WANTED
                SNPRINTF (ac1Command, sizeof (ac1Command), "%s %s*",
                          IMAGE_DATA_REMOVE_CMD, IMAGE_DATA_FLASH_CONF_LOC);
                system (ac1Command);
                MEMSET (&ac1Command, 0, CAPWAP_IMAGE_BUFFER_SIZE);
                /*Kernel Upgrade */
                if (strcmp
                    (CapwapGetFilenameExt (pSessEntry->ImageId.data),
                     "bin") == 0)
                {
                    /*Dummy image created in flash path IMAGE_DATA_FLASH_CONF_LOC   */
                    CapwapOpenImage (pSessEntry);
                    PRINTF ("\n SysUpgrade done");
                }
                else
                {
                    MEMSET (&ac1Command, 0, CAPWAP_IMAGE_BUFFER_SIZE);
                    SNPRINTF (ac1Command, sizeof (ac1Command), "%s %s%s %s",
                              IMAGE_DATA_CP_CMD, IMAGE_DATA_FLASH_LOC,
                              pSessEntry->ImageId.data,
                              IMAGE_DATA_FLASH_TEMP_CONF_LOC);
                    system (ac1Command);
                    /*Dummy image created in flash path IMAGE_DATA_FLASH_CONF_LOC   */
                    CapwapOpenImage (pSessEntry);
                }
#endif

#ifdef DAK_WANTED
                MEMSET (&ac1Command, 0, CAPWAP_IMAGE_BUFFER_SIZE);
                SNPRINTF (ac1Command, sizeof (ac1Command), "%s %s %s",
                          BOOT_LDR_SET_ENV, BOOT_IMG_VAR,
                          pSessEntry->ImageId.data);
                system (ac1Command);

                /* Upgrading the image */
                MEMSET (&ac1Command, 0, CAPWAP_IMAGE_BUFFER_SIZE);
                SNPRINTF (ac1Command, sizeof (ac1Command), "%s %s%s",
                          IMAGE_SYS_UPGRADE_CMD, IMAGE_DATA_FLASH_CONF_LOC,
                          pSessEntry->ImageId.data);
                system (ac1Command);
#endif

            }
        }
    }
    else
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Image Data Req Validation FAILED (SeqNum Mismatch) \n");
        CapwapTmrStop (pSessEntry, CAPWAP_IMAGEDATA_START_TMR);

        if (u1IsNotLinear == OSIX_TRUE)
        {
            MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pRcvdBuf);
        }
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDBTmr);
        return OSIX_FAILURE;
    }

    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDBTmr);
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/***********************************************************************
*  Function Name   : CapwapGetWTPImageDataRspMsgElements               *
*  Description     : The function processes the received CAPWAP        *
*                    image data response msg elements.                 *
*  Input(s)        : pImageDataRsp, u4MsgLen                           *
*  Output(s)       : None.                                             *
*  Returns         : None                                              *
************************************************************************/
INT4
CapwapGetWTPImageDataRspMsgElements (tImageDataRsp * pImageDataRsp,
                                     UINT4 *u4MsgLen)
{
    UNUSED_PARAM (u4MsgLen);

    CAPWAP_FN_ENTRY ();

    if (CapwapConstructCpHeader (&pImageDataRsp->capwapHdr) == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "capwapGetJoinRspMsgElements:Failed to construct the capwap Header\n");
        return OSIX_FAILURE;
    }

    /* update Control Header */
    pImageDataRsp->u1NumMsgBlocks = 0;    /* flags */

    return OSIX_SUCCESS;
}

/***********************************************************************
*  Function Name   : CapwapGetWTPImageId                               *
*  Description     : The function processes the WTP Image ID           *
*  Input(s)        : pImageId, u4MsgLen                                *
*  Output(s)       : None.                                             *
*  Returns         : None                                              *
************************************************************************/
INT4
CapwapGetWTPImageId (tImageId * pImageId, UINT4 *pMsgLen)
{
    pImageId->u2MsgEleType = IMAGE_IDENTIFIER;
    pImageId->u4VendorId = 1;
    pImageId->u2MsgEleLen = pImageId->u2MsgEleLen + CAPWAP_MSG_ELEM_TYPE_LEN;
    *pMsgLen = pImageId->u2MsgEleLen + CAPWAP_MSG_ELEM_TYPE_LEN;
    pImageId->isOptional = 1;
    return OSIX_SUCCESS;
}

/***********************************************************************
*  Function Name   : CapwapGetInitiateDownload                         *
*  Description     : The function processes to initiate download       *
*  Input(s)        : pImageInitiateDownload, u4MsgLen                  *
*  Output(s)       : None.                                             *
*  Returns         : None                                              *
************************************************************************/
INT4
CapwapGetInitiateDownload (tImageIntiateDownload * pImageInitiateDownload,
                           UINT4 *pMsgLen)
{
    pImageInitiateDownload->u2MsgEleType = INITIATE_DOWNLOAD;
    pImageInitiateDownload->u2MsgEleLen = 0;
    *pMsgLen += CAPWAP_MSG_ELEM_TYPE_LEN;

    pImageInitiateDownload->isOptional = OSIX_TRUE;

    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapGetImageDataReqElements                     */
/*  Description     : The function validates the functional             */
/*                    characteristics of the received CAPWAP            */
/*                    Join response message against the configured      */
/*                    WTP profile                                       */
/*  Input(s)        : None                                              */
/*  Output(s)       :  pJoinResponse - filled structure                 */
/*  Returns         : None                                              */
/************************************************************************/
INT4
CapwapGetImageDataReqElements (tImageDataReq * pImageDataReq, UINT4 *pMsgLen)
{

    CAPWAP_FN_ENTRY ();
    if (CapwapConstructCpHeader (&pImageDataReq->capwapHdr) == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "capwapGetJoinRspMsgElements:Failed to construct the capwap Header\n");
        return OSIX_FAILURE;
    }

    if (CapwapGetWTPImageId (&pImageDataReq->ImageId, pMsgLen) == OSIX_FAILURE)
    {
        CAPWAP_TRC (ALL_FAILURE_TRC,
                    "Failed to get Optional Message Elements \n");
        return OSIX_FAILURE;
    }

    if (CapwapGetInitiateDownload (&pImageDataReq->InitiatelDownload,
                                   pMsgLen) == OSIX_FAILURE)
    {
        CAPWAP_TRC (ALL_FAILURE_TRC,
                    "Failed to get Optional Message Elements \n");
        return OSIX_FAILURE;
    }

    /* update Control Header */
    pImageDataReq->u1NumMsgBlocks = 0;    /* flags always zero */

    CAPWAP_FN_EXIT ();
    return CAPWAP_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapValidateImageDataReqMsgElems                */
/*  Description     : The function validates the functional             */
/*                    characteristics of the received CAPWAP            */
/*                    Join request agains the configured                */
/*                    WTP profile                                       */
/*  Input(s)        : discReqMsg - parsed CAPWAP Discovery request      */
/*                    pRcvBuf - Received discovery packet               */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
CapwapOpenAndWriteToImage (UINT1 *pBuf,
                           tRemoteSessionManager * pSess, UINT2 u2WriteLen)
{

    FILE               *fp = NULL;
    UINT2               debugLen = 0;
    INT4                i4FileSize = 0;
    UINT1               au1FlashFile[128];
    UINT1              *pfile;
    UINT1               u1DestLen = 0;

    pfile = au1FlashFile;

    CAPWAP_FN_ENTRY ();

    u1DestLen =
        (STRLEN (pSess->ImageId.data) + STRLEN (IMAGE_DATA_FLASH_LOC) + 1);
    SNPRINTF ((char *) au1FlashFile, u1DestLen, "%s%s", IMAGE_DATA_FLASH_LOC,
              pSess->ImageId.data);

    fp = fopen ((char *) pfile, "wb");
    if (NULL == fp)
    {
        CAPWAP_TRC (ALL_FAILURE_TRC, "Failed to open the file \n");
        return OSIX_FAILURE;
    }

    if (fseek (fp, 0, SEEK_END) > 0)
    {
        CAPWAP_TRC (ALL_FAILURE_TRC, "Failed to seek end of file \n");
        fclose (fp);
        return OSIX_FAILURE;
    }
    i4FileSize = ftell (fp);
    if (fseek (fp, 0, SEEK_SET) > 0)
    {
        CAPWAP_TRC (ALL_FAILURE_TRC, "Failed to seek to start of file \n");
        fclose (fp);
        return OSIX_FAILURE;
    }

    pSess->u1WriteFileOpened = OSIX_TRUE;
    pSess->writeFd = fp;
    debugLen = fwrite (pBuf, 1, u2WriteLen, fp);

    i4FileWriteCount++;

    if (pSess->u1EndofFileReached == OSIX_TRUE)
    {
        fclose (pSess->writeFd);
        pSess->u1WriteFileOpened = OSIX_FALSE;
        pSess->u1EndofFileReached = OSIX_FALSE;
        pSess->writeFd = 0;
    }

    CAPWAP_FN_EXIT ();
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (u2WriteLen);
    UNUSED_PARAM (debugLen);
    UNUSED_PARAM (i4FileSize);
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapWriteToImage                                */
/*  Description     : The function write to image                       */
/*  Input(s)        : pBuf - Received packet                            */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
CapwapWriteToImage (UINT1 *pBuf,
                    tRemoteSessionManager * pSess, UINT2 u2WriteLen)
{

    UINT2               debugLen;

    CAPWAP_FN_ENTRY ();

    if (pSess->u1EndofFileReached == OSIX_TRUE)
    {
        debugLen = fwrite (pBuf, 1, u2WriteLen, pSess->writeFd);
        fclose (pSess->writeFd);
        pSess->u1EndofFileReached = OSIX_FALSE;
        pSess->u1WriteFileOpened = OSIX_FALSE;
        pSess->writeFd = 0;
    }
    else
    {
        debugLen = fwrite (pBuf, 1, u2WriteLen, pSess->writeFd);
    }

    i4FileWriteCount++;

    CAPWAP_FN_EXIT ();
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (u2WriteLen);
    UNUSED_PARAM (debugLen);
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapValidateWTPImageDataReqMsgElems             */
/*  Description     : The function validates the functional             */
/*  Input(s)        : pRcvBuf - Received discovery packet               */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
CapwapValidateWTPImageDataReqMsgElems (UINT1 *pRcvBuf,
                                       tCapwapControlPacket * pImageDataReqMsg,
                                       tRemoteSessionManager * pSessEntry)
{
    UNUSED_PARAM (pRcvBuf);
    UNUSED_PARAM (pImageDataReqMsg);
    UNUSED_PARAM (pSessEntry);
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapValidateWTPImageDataRespMsgElems            */
/*  Description     : The function validates the functional             */
/*  Input(s)        : pRcvBuf - Received discovery packet               */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
CapwapValidateWTPImageDataRespMsgElems (UINT1 *pRcvBuf,
                                        tCapwapControlPacket * pImageDataRspMsg,
                                        tImageDataRsp * pImageDataRsp)
{
    UNUSED_PARAM (pRcvBuf);
    UNUSED_PARAM (pImageDataRspMsg);
    UNUSED_PARAM (pImageDataRsp);
    CAPWAP_FN_ENTRY ();

    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapGetWLCImageId                               */
/*  Description     : The function gets the Image ID                    */
/*  Input(s)        : pImageId, pMsgLen                                 */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
CapwapGetWLCImageId (tImageId * pImageId, UINT4 *pMsgLen)
{
    pImageId->u2MsgEleType = IMAGE_IDENTIFIER;
    pImageId->u4VendorId = 0;
    pImageId->u2MsgEleLen = pImageId->u2MsgEleLen + CAPWAP_MSG_ELEM_TYPE_LEN;
    *pMsgLen += pImageId->u2MsgEleLen + CAPWAP_MSG_ELEM_TYPE_LEN;
    pImageId->isOptional = OSIX_TRUE;

    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapOpenImage                                   */
/*  Description     : The function gets the Image ID                    */
/*  Input(s)        : pSess                                             */
/*  Output(s)       : pSess                                             */
/*  Returns         : SUCCESS OR FAILURE                                */
/************************************************************************/
INT4
CapwapOpenImage (tRemoteSessionManager * pSess)
{

    FILE               *fp = NULL;
    UINT1               au1FlashFile[128];
    UINT1              *pfile;
    UINT1               u1DestLen = 0;

    pfile = au1FlashFile;

    CAPWAP_FN_ENTRY ();

    u1DestLen =
        (STRLEN (pSess->ImageId.data) + STRLEN (IMAGE_DATA_FLASH_CONF_LOC) + 1);
    SNPRINTF ((char *) au1FlashFile, u1DestLen, "%s%s",
              IMAGE_DATA_FLASH_CONF_LOC, pSess->ImageId.data);

    fp = fopen ((char *) pfile, "wb");
    if (NULL == fp)
    {
        CAPWAP_TRC (ALL_FAILURE_TRC, "Failed to open the file \n");
        return OSIX_FAILURE;
    }
    fclose (fp);
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

const char         *
CapwapGetFilenameExt (const char *filename)
{
    const char         *dot = strrchr (filename, '.');
    if (!dot || dot == filename)
        return "";
    return dot + 1;
}

#endif /* __CAPWAP_WTPIMAGE_ */
/*-----------------------------------------------------------------------*/
/*                    End of the file  capwapwlcimage.c                  */
/*-----------------------------------------------------------------------*/
