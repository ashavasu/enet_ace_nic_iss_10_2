/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved                      *
 * $Id: caprmain.c,v 1.5 2018/02/08 10:07:56 siva Exp $       
 * Description: This file contains the CAPWAP Packet RX related functions    *
 *****************************************************************************/
#ifndef __CAPRMAIN_C__
#define __CAPRMAIN_C__
#define _CAPWAP_GLOBAL_VAR

#include "capwapinc.h"
#include "capwapcli.h"
#include "aphdlr.h"
#include "aphdlrprot.h"
#ifdef NPAPI_WANTED
#include "nputil.h"
#endif
#include "fssocket.h"
#if defined (IP_WANTED)
#if defined (OS_PTHREADS) || defined (OS_CPSS_MAIN_OS)
#include <linux/if.h>
#include <linux/if_packet.h>
#include <linux/if_ether.h>
#endif
#endif
#ifdef KERNEL_CAPWAP_WANTED
#include "fcusprot.h"
#ifdef WTP_WANTED
static UINT1        gu1Flag = 0;
#endif
#endif
#define IP_MTU 14
static tRecvTaskGlobals gRecvTaskGlobals;
INT4                i4CwpCtrlSock4Id = SOCKET_ID;    /* Ipv4 Socket Id */
static INT4         i4CwpDataSock4Id = SOCKET_ID;
#ifdef WTP_WANTED
static INT4         i4CwpMulDataSock4Id = SOCKET_ID;
#endif
#ifdef IP6_WANTED
#if 0
static INT4         i4CwpCtrlSock6Id = SOCKET_ID;    /* Ipv6 Socket Id */
static INT4         i4CwpDataSock6Id = SOCKET_ID;
#endif
#endif
UINT4               gu4CapwapEnable;
UINT4               gu4CapwapShutdown;

/* CPU relinquish counter */
static UINT4        gu4CPURelinquishCounter = 0;
eState              gCapwapState = CAPWAP_START;

#ifdef WTP_WANTED
INT4                CapwapRecvMulDataIpv4Packet (void);
#endif
#ifdef LNXIP4_WANTED
extern UINT1       *CfaGddGetLnxIntfnameForPort (UINT2 u2Index);
#endif

INT4
CapwapEnable (VOID)
{
    CAPWAP_FN_ENTRY ();

    /* start the timers */
    if (CapwapNoShutdownInitAllTimers () != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CAPWAP CapwapNoShutdownInitAllTimers failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "CAPWAP CapwapNoShutdownInitAllTimers failed \r\n"));
        return OSIX_FAILURE;
    }

    /* routine initialises the timer descriptor structure */
    CapwapUtilInitGlobals ();

    /* open the capwap packet sockets */
    if (CapwapEnableSockets () != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CAPWAP CapwapEnableSockets failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "CAPWAP CapwapEnableSockets failed \r\n"));
        return OSIX_FAILURE;
    }

    gu4CapwapEnable = CAPWAP_ENABLE;

    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

INT4
CapwapDisable (VOID)
{

    CAPWAP_FN_ENTRY ();

    /* clear the timers */
    if (CapwapShutdownStopAllTimers () != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CAPWAP CapwapShutdownStopAllTimers failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "CAPWAP CapwapShutdownStopAllTimers failed \r\n"));
        return OSIX_FAILURE;
    }

    /* clear the db's */
    if (CapwapShutdownDeleteProfileAllDBs () != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CAPWAP CapwapShutdownDeleteProfileAllDBs failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "CAPWAP CapwapShutdownDeleteProfileAllDBs failed \r\n"));
        return OSIX_FAILURE;
    }

    /* close the capwap packet sockets */
    if (CapwapDisableSockets () != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CAPWAP CapwapDisableSockets failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "CAPWAP CapwapDisableSockets failed \r\n"));
        return OSIX_FAILURE;
    }

    gu4CapwapEnable = CAPWAP_DISABLE;

    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

INT4
CapwapEnableSockets (VOID)
{
    CAPWAP_FN_ENTRY ();

    /* Create the UDP socket for CAPWAP Ctrl chnl IPv4 */
    if (CapwapCtrlUdpSockInit () != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CAPWAP Ctrl chnl Socket Initialization Failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "CAPWAP Ctrl chnl Socket Initialization Failed \r\n"));
        return OSIX_FAILURE;
    }
    /* Create the UDP socket for CAPWAP Data chnl IPv4 */
    if (CapwapDataUdpSockInit () != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CAPWAP Data chnl Socket Initialization Failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "CAPWAP Data chnl Socket Initialization Failed \r\n"));
        return OSIX_FAILURE;
    }
#ifdef IP6_WANTED
#if 0

    /* Create the UDP socket for CAPWAP Ctrl Chnl IPv6 */
    if (CapwapUdpSock6Init () != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CAPWAP Ctrl chnel Sock6 Initialization failed \r\n");
        return OSIX_FAILURE;
    }
    /* Create the UDP socket for CAPWAP Data Chnl Ipv6 */
    if (CapwapDataUdpSock6Init () != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CAPWAP Data chnel Sock6 Initialization failed \r\n");
        return OSIX_FAILURE;
    }
#endif
#endif

    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

INT4
CapwapUdpSock6Init ()
{
    /* Create the UDP socket for CAPWAP Ctrl Chnl IPv6 */
    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                " IpV6 is not supported for WSS Package\r\n");
    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                  " IpV6 is not supported for WSS Package\r\n"));
    return OSIX_SUCCESS;
}

INT4
CapwapDataUdpSock6Init ()
{
    /* Create the UDP socket for CAPWAP Data Chnl Ipv6 */
    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                " IpV6 is not supported for WSS Package\r\n");
    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                  " IpV6 is not supported for WSS Package\r\n"));
    return OSIX_SUCCESS;
}

INT4
CapwapDisableSockets (VOID)
{

    CAPWAP_FN_ENTRY ();

    /* Close the Relay Socket Descriptor */
    SelRemoveFd (i4CwpCtrlSock4Id);
    if (i4CwpCtrlSock4Id != SOCKET_ID)
    {
        if (close (i4CwpCtrlSock4Id) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Socket Close failed for CAPWAP ctrl IPV4 Soket\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Socket Close failed for CAPWAP ctrl IPV4 Soket\r\n"));
        }
    }
    i4CwpCtrlSock4Id = 0;
    SelRemoveFd (i4CwpDataSock4Id);
    if (i4CwpDataSock4Id != SOCKET_ID)
    {
        if (close (i4CwpDataSock4Id) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Socket close failed for CAPWAP Data Ipv4 Socket \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Socket close failed for CAPWAP Data Ipv4 Socket \r\n"));
        }
    }
    i4CwpDataSock4Id = 0;
#ifdef IP6_WANTED
#if 0
    SelRemoveFd (i4CwpCtrlSock6Id);
    if (i4CwpCtrlSock6Id != SOCKET_ID)
    {
        if (close (i4CwpCtrlSock6Id) != OSIX_SUCCESS)
        {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Socket Close failed for CAPWAP ctrl IPV6 Soket\r\n")}
    }
    SelRemoveFd (i4CwpDataSock6Id);
    if (i4CwpDataSock6Id != SOCKET_ID)
    {
        if (close (i4CwpDataSock6Id) != OSIX_SUCCESS)
        {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Socket Close failed for CAPWAP ctrl IPV6 Soket\r\n")}
    }
#endif
#endif

    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

INT4
CapwapShutdownStopAllTimers (VOID)
{
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tRemoteSessionManager *pSessEntry = NULL;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4PrevWtpProfileId = 0;

    CAPWAP_FN_ENTRY ();

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    if (nmhGetFirstIndexCapwapBaseWtpProfileTable (&u4WtpProfileId) ==
        SNMP_SUCCESS)
    {
        do
        {
            pWssIfCapwapDB->CapwapGetDB.u2ProfileId = (UINT2) u4WtpProfileId;
            pWssIfCapwapDB->CapwapIsGetAllDB.bWtpProfileId = OSIX_TRUE;

            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_INTERNAL_ID,
                                         pWssIfCapwapDB) != OSIX_SUCCESS)
            {
                CAPWAP_TRC1 (CAPWAP_FSM_TRC,
                             "Getting Internal Id Failed for WTP Prof: %d\r\n",
                             u4WtpProfileId);
                SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4CapwapSysLogId,
                              "Getting Internal Id Failed for WTP Prof: %d\r\n",
                              u4WtpProfileId));
            }

            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_RSM_FROM_INDEX,
                                         pWssIfCapwapDB) == OSIX_FAILURE)
            {
                CAPWAP_TRC1 (CAPWAP_FSM_TRC,
                             "Failed to Retrive CAPWAP DB for WTP Prof: %d\r\n",
                             u4WtpProfileId);
                SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4CapwapSysLogId,
                              "Failed to Retrive CAPWAP DB for WTP Prof: %d\r\n",
                              u4WtpProfileId));
            }
            else
            {
                pSessEntry = pWssIfCapwapDB->pSessEntry;
                if (pSessEntry != NULL)
                {
                    /* Stop all the session timers present and runnning */
                    CapwapStopAllSessionTimers (pSessEntry);
                }
            }

            u4PrevWtpProfileId = u4WtpProfileId;
        }
        while (nmhGetNextIndexCapwapBaseWtpProfileTable (u4PrevWtpProfileId,
                                                         &u4WtpProfileId) ==
               SNMP_SUCCESS);
    }

    /* Stop all the global timers present and runnning */
    CapwapStopAllGlobalTimers ();

    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);

    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

INT4
CapwapNoShutdownInitAllTimers (VOID)
{

    CAPWAP_FN_ENTRY ();

    /* Starting of session timers will be individually done whenevr a session is
     * created. */

    /* Start all the global timers */
    CapwapStartAllGlobalTimers ();

    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

INT4
CapwapShutdownTearSessions (UINT4 u4WtpProfileId)
{
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tRemoteSessionManager *pSessEntry = NULL;

    CAPWAP_FN_ENTRY ();

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    pWssIfCapwapDB->CapwapGetDB.u2ProfileId = (UINT2) u4WtpProfileId;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpProfileId = OSIX_TRUE;

    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_INTERNAL_ID, pWssIfCapwapDB) !=
        OSIX_SUCCESS)
    {
        CAPWAP_TRC1 (CAPWAP_FSM_TRC,
                     "Getting Internal Id Failed for WTP Prof: %d\r\n",
                     u4WtpProfileId);
        SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4CapwapSysLogId,
                      "Getting Internal Id Failed for WTP Prof: %d\r\n",
                      u4WtpProfileId));
    }

    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_RSM_FROM_INDEX, pWssIfCapwapDB)
        == OSIX_FAILURE)
    {
        CAPWAP_TRC1 (CAPWAP_FSM_TRC,
                     "Failed to Retrive CAPWAP DB for WTP Prof: %d\r\n",
                     u4WtpProfileId);
        SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4CapwapSysLogId,
                      "Failed to Retrive CAPWAP DB for WTP Prof: %d\r\n",
                      u4WtpProfileId));
    }

    pSessEntry = pWssIfCapwapDB->pSessEntry;
    if (pSessEntry != NULL)
    {
        /* shutdown the DTLS for remote session */
        if (CapwapShutDownDTLS (pSessEntry) != OSIX_SUCCESS)
        {
            CAPWAP_TRC1 (CAPWAP_FSM_TRC,
                         "Failed to Shutdown DTLS for WTP Prof: %d\r\n",
                         u4WtpProfileId);
            SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4CapwapSysLogId,
                          "Failed to Shutdown DTLS for WTP Prof: %d\r\n",
                          u4WtpProfileId));
        }

        /* delete the session-profid entries also, index is wtp-internal-id */
        if (WssIfProcessCapwapDBMsg
            (WSS_CAPWAP_GET_SESSPROFID_ENTRY, pWssIfCapwapDB) == OSIX_FAILURE)
        {
            CAPWAP_TRC1 (CAPWAP_FSM_TRC,
                         "Failed to Retrieve SESS Prof for WTP Prof: %d\r\n",
                         u4WtpProfileId);
            SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4CapwapSysLogId,
                          "Failed to Retrieve SESS Prof for WTP Prof: %d\r\n",
                          u4WtpProfileId));
        }

        if (pWssIfCapwapDB->pSessProfileIdEntry != NULL)
        {
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_DELETE_SESSPROFID_ENTRY,
                                         pWssIfCapwapDB) == OSIX_FAILURE)
            {
                CAPWAP_TRC1 (CAPWAP_FSM_TRC,
                             "Failed to Delete SESS Prof for WTP Prof: %d\r\n",
                             u4WtpProfileId);
                SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4CapwapSysLogId,
                              "Failed to Delete SESS Prof for WTP Prof: %d\r\n",
                              u4WtpProfileId));

            }
        }

        /* delete the data port session-port entries also */
        pWssIfCapwapDB->u4DestIp = pSessEntry->remoteIpAddr.u4_addr[0];
        pWssIfCapwapDB->u4DestPort = pSessEntry->u4RemoteDataPort;

        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_SESSION_PORT_ENTRY,
                                     pWssIfCapwapDB) == OSIX_FAILURE)
        {
            CAPWAP_TRC1 (CAPWAP_FSM_TRC,
                         "Failed to Retrieve SESS Port for WTP Prof: %d\r\n",
                         u4WtpProfileId);
            SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4CapwapSysLogId,
                          "Failed to Retrieve SESS Port for WTP Prof: %d\r\n",
                          u4WtpProfileId));
        }

        if (pWssIfCapwapDB->pSessDestIpEntry != NULL)
        {
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_DELETE_SESSION_PORT_ENTRY,
                                         pWssIfCapwapDB) == OSIX_FAILURE)
            {
                CAPWAP_TRC1 (CAPWAP_FSM_TRC,
                             "Failed to Delete SESS Port for WTP Prof: %d\r\n",
                             u4WtpProfileId);
                SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4CapwapSysLogId,
                              "Failed to Delete SESS Port for WTP Prof: %d\r\n",
                              u4WtpProfileId));
            }
        }

        /* delete the RSM entries, thereby the control port
         * session-port entries also */
        pWssIfCapwapDB->u4DestIp = pSessEntry->remoteIpAddr.u4_addr[0];
        pWssIfCapwapDB->u4DestPort = pSessEntry->u4RemoteCtrlPort;

        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_DELETE_RSM,
                                     pWssIfCapwapDB) == OSIX_FAILURE)
        {
            CAPWAP_TRC1 (CAPWAP_FSM_TRC,
                         "Failed to Delete RSM for WTP Prof: %d\r\n",
                         u4WtpProfileId);
            SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4CapwapSysLogId,
                          "Failed to Delete RSM for WTP Prof: %d\r\n",
                          u4WtpProfileId));
        }
    }

    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);

    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

INT4
CapwapShutdownDeleteProfileAllDBs (VOID)
{
    UINT4               u4WtpProfileId = 0;
    UINT4               u4FsCapwapBlackListId = 0;
    UINT4               u4FsCapwapWhiteListId = 0;
    UINT1               i1aCapwapWtpModelNumber[512];
    tSNMP_OCTET_STRING_TYPE FsCapwapWtpModelNumber;

    CAPWAP_FN_ENTRY ();

    MEMSET (&FsCapwapWtpModelNumber, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    FsCapwapWtpModelNumber.pu1_OctetList = i1aCapwapWtpModelNumber;

    /* the function inside the while loop deletes the entry from the list of
     * nodes which we are looping through. so we always get the first entry,
     * as next entry becomes first entry after successful deletion */

    /* delete the black-list table */
    if (nmhGetFirstIndexFsCapwapBlackList (&u4FsCapwapBlackListId) ==
        SNMP_SUCCESS)
    {
        do
        {
            if (nmhSetFsCapwapBlackListRowStatus
                (u4FsCapwapBlackListId, DESTROY) != SNMP_SUCCESS)
            {
                CAPWAP_TRC1 (CAPWAP_FSM_TRC,
                             "Failed to Delete Black List Entry: %d\r\n",
                             u4FsCapwapBlackListId);
                SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4CapwapSysLogId,
                              "Failed to Delete Black List Entry: %d\r\n",
                              u4FsCapwapBlackListId));
                return OSIX_FAILURE;
            }
        }
        while (nmhGetFirstIndexFsCapwapBlackList (&u4FsCapwapBlackListId) ==
               SNMP_SUCCESS);
    }

    /* delete the white-list table */
    if (nmhGetFirstIndexFsCapwapWhiteListTable (&u4FsCapwapWhiteListId) ==
        SNMP_SUCCESS)
    {
        do
        {
            if (nmhSetFsCapwapBlackListRowStatus
                (u4FsCapwapWhiteListId, DESTROY) != SNMP_SUCCESS)
            {
                CAPWAP_TRC1 (CAPWAP_FSM_TRC,
                             "Failed to Delete White List Entry: %d\r\n",
                             u4FsCapwapWhiteListId);
                SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4CapwapSysLogId,
                              "Failed to Delete White List Entry: %d\r\n",
                              u4FsCapwapWhiteListId));
                return OSIX_FAILURE;
            }
        }
        while (nmhGetFirstIndexFsCapwapWhiteListTable (&u4FsCapwapWhiteListId)
               == SNMP_SUCCESS);
    }

    /* delete the wtp-profile table */
    if (nmhGetFirstIndexCapwapBaseWtpProfileTable (&u4WtpProfileId) ==
        SNMP_SUCCESS)
    {
        do
        {
            /* "CapwapDeleteProfileIdDB" shall delete only the
             * tables created for private mibs, which has "u4WtpProfileId" as
             * table-index, and corresponding radio-ifindex as table-index. But,
             * it is called within the row destroy of CapwapBaseWtpProfileTable
             * implicitly. so not calling it explicitly here */

            if (nmhSetCapwapBaseWtpProfileRowStatus (u4WtpProfileId, DESTROY) !=
                SNMP_SUCCESS)
            {
                CAPWAP_TRC1 (CAPWAP_FSM_TRC,
                             "Failed to Delete WTP Profile Entry: %d\r\n",
                             u4WtpProfileId);
                SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4CapwapSysLogId,
                              "Failed to Delete WTP Profile Entry: %d\r\n",
                              u4WtpProfileId);
                    )return OSIX_FAILURE;
            }

            if (CapwapShutdownTearSessions (u4WtpProfileId) != OSIX_SUCCESS)
            {
                CAPWAP_TRC1 (CAPWAP_FSM_TRC,
                             "Failed to Teardown Remote Sessions: %d\r\n",
                             u4WtpProfileId);
                SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4CapwapSysLogId,
                              "Failed to Teardown Remote Sessions: %d\r\n",
                              u4WtpProfileId));
                return OSIX_FAILURE;
            }

        }
        while (nmhGetFirstIndexCapwapBaseWtpProfileTable (&u4WtpProfileId) ==
               SNMP_SUCCESS);
    }

    /* delete the wtp-model table */
    if (nmhGetFirstIndexFsWtpModelTable (&FsCapwapWtpModelNumber) ==
        SNMP_SUCCESS)
    {
        do
        {
            if (nmhSetFsWtpModelRowStatus (&FsCapwapWtpModelNumber, DESTROY) !=
                SNMP_SUCCESS)
            {
                CAPWAP_TRC1 (CAPWAP_FSM_TRC,
                             "Failed to Delete WTP Model Entry: %s\r\n",
                             FsCapwapWtpModelNumber.pu1_OctetList);
                SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4CapwapSysLogId,
                              "Failed to Delete WTP Model Entry: %s\r\n",
                              FsCapwapWtpModelNumber.pu1_OctetList));
                return OSIX_FAILURE;
            }
        }
        while (nmhGetFirstIndexFsWtpModelTable (&FsCapwapWtpModelNumber) ==
               SNMP_SUCCESS);
    }

    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

INT4
CapwapShutdownFreeMemory (VOID)
{
    tWssIfCapDB        *pWssIfCapwapDB = NULL;

    CAPWAP_FN_ENTRY ();

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    /* no memory is allocated in CapwapDiscTaskMain */

    /* free the memory allocated in CapwapServiceTaskInit */
    /* clear/shutdown the DB's in WSSIF module */
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_SHUTDOWN_DB, pWssIfCapwapDB) ==
        OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapShutdownFreeMemory: Deletion of DB's in WSSIF module failed\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "CapwapShutdownFreeMemory: Deletion of DB's in WSSIF module failed\r\n"));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    /* remove the RBtrees created for CLI DB */
    if (CapwapUtlDeleteRBTree () == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapShutdownFreeMemory: Delettion of RBTrees Failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "CapwapShutdownFreeMemory: Delettion of RBTrees Failed \r\n"));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    /* free the memory allocated in CapwapRecvMainTaskInit */
    CapwapRecvMemClear ();

    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

INT4
CapwapNoShutdownAllocMemory (VOID)
{
    tWssIfCapDB        *pWssIfCapwapDB = NULL;

    CAPWAP_FN_ENTRY ();

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    /* no memory is allocated in CapwapDiscTaskMain */

    /* alloc the memory allocated similar to CapwapRecvMainTaskInit */
    /* memory pool inits */
    if (CapwapRecvMemInit () == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapNoShutdownAllocMemory: Memory Pool Creation Failed\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "CapwapNoShutdownAllocMemory: Memory Pool Creation Failed\n"));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    /* create the RBtrees created for CLI DB */
    if (CapwapUtlCreateRBTree () == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapNoShutdownAllocMemory: Creattion of RBTrees Failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "CapwapNoShutdownAllocMemory: Creattion of RBTrees Failed \r\n"));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    /* alloc the memory allocated similar to CapwapServiceTaskInit */
    /* init the DB's in WSSIF module */
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_INIT_DB, pWssIfCapwapDB) ==
        OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapNoShutdownAllocMemory: Allocation of DB's in WSSIF module failed\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "CapwapNoShutdownAllocMemory: Allocation of DB's in WSSIF module failed\r\n"));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

INT4
CapwapShutdown (VOID)
{
    /* Disble the Capwap - to close the socket, timers */
    INT4                i4CapwapDisable = CLI_FSCAPWAP_DISABLE;

    CAPWAP_FN_ENTRY ();

    if (CapwapSetFsCapwapEnable (i4CapwapDisable) != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Unable to Disable the capwap module\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Unable to Disable the capwap module\r\n"));
        return OSIX_FAILURE;
    }

    /* clear the memory also */
    if (CapwapShutdownFreeMemory () != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Unable to free the memory for capwap module\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Unable to free the memory for capwap module\r\n"));
        return OSIX_FAILURE;
    }

    gu4CapwapShutdown = CAPWAP_SHUTDOWN;

    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

INT4
CapwapNoShutdown (VOID)
{
    /* Enable the Capwap - to open the socket, timers */
    INT4                i4CapwapDisable = CLI_FSCAPWAP_ENABLE;

    CAPWAP_FN_ENTRY ();

    /* alloc the memory */
    if (CapwapNoShutdownAllocMemory () != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Unable to free the memory for capwap module\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Unable to free the memory for capwap module\r\n"));
        return OSIX_FAILURE;
    }

    if (CapwapSetFsCapwapEnable (i4CapwapDisable) != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Unable to Enable the capwap module\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Unable to Enable the capwap module\r\n"));
        return OSIX_FAILURE;
    }

    gu4CapwapShutdown = CAPWAP_NO_SHUTDOWN;

    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

INT4
CapwapCliCheckModuleStatus (tCliHandle CliHandle, UINT4 u4Command)
{
    if (gu4CapwapShutdown == CAPWAP_SHUTDOWN)
    {
        if (u4Command != CLI_CAPWAP_FSCAPWAPSHUTDOWN)
        {
            CliPrintf (CliHandle, "\r%% Capwap module not started \r\n");
            return CLI_FAILURE;
        }
    }

    return CLI_SUCCESS;
}

INT4
CapwapCheckModuleStatus ()
{
    if (gu4CapwapShutdown == CAPWAP_SHUTDOWN)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
CapwapCliCheckModuleStatusShow (tCliHandle CliHandle)
{
    if (gu4CapwapShutdown == CAPWAP_SHUTDOWN)
    {
        CliPrintf (CliHandle, "\r%% Capwap module not started \r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name   : CapwapPacketOnSocket
 * Description     : Call back function from SelAddFd(), when packet is
 *                   received
 * Inputs          : None
 * Output          : Sends an event to CAPWAP Receiver task
 * Returns         : None.
 ******************************************************************************/
VOID
CapwapPacketOnSocket (INT4 i4SockFd)
{

    if (i4SockFd == i4CwpCtrlSock4Id)
    {
        if (OsixEvtSend (gRecvTaskGlobals.capwapRecvTaskId,
                         CTRL_IPV4PKT_ARRIVAL_EVENT) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "CapwapPacketOnSocket:Event send Failed \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "CapwapPacketOnSocket:Event send Failed \r\n"));
        }
    }
    else if (i4SockFd == i4CwpDataSock4Id)
    {
        if (OsixEvtSend (gRecvTaskGlobals.capwapRecvTaskId,
                         DATA_IPV4PKT_ARRIVAL_EVENT) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "CapwapPacketOnSocket:Event send Failed \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "CapwapPacketOnSocket:Event send Failed \r\n"));
        }
    }
#ifdef WTP_WANTED
    else if (i4SockFd == i4CwpMulDataSock4Id)
    {
        if (OsixEvtSend (gRecvTaskGlobals.capwapRecvTaskId,
                         MULTICAST_DATA_IPV4PKT_ARRIVAL_EVENT) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "CapwapPacketOnSocket:Event send Failed \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "CapwapPacketOnSocket:Event send Failed \r\n"));
        }
    }

#endif
#ifdef IP6_WANTED
#if 0
    else if (i4SockFd == i4CwpCtrlSock6Id)
    {
        if (OsixEvtSend (gRecvTaskGlobals.capwapRecvTaskId,
                         CTRL_IPV6PKT_ARRIVAL_EVENT) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "CapwapPacketOnSocket:Event send Failed \r\n");
        }
    }
    else if (i4SockFd == i4CwpDataSock6Id)
    {
        if (OsixEvtSend (gRecvTaskGlobals.capwapRecvTaskId,
                         DATA_IPV6PKT_ARRIVAL_EVENT) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "CapwapPacketOnSocket:Event send Failed \r\n");
        }
    }
#endif
#endif
}

/*****************************************************************************
 *                                                                           *
 * Function     : CapwapRecvMemInit                                          *
 *                                                                           *
 * Description  : Memory creation and initialization required for            *
 *                CAPWAP Receiver module                                     *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
UINT4
CapwapRecvMemInit (VOID)
{
    if (CapwapSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

#if CAP_WANTED
/*****************************************************************************
 *                                                                           *
 * Function     : tmrRxCallback                                              *
 *                                                                           *
 * Description  : CAPWAP Receiver CPU relinquish timer function.             *
 *                                                                           *
 * Input        : tTimerListId                                               *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      :                                                            *
 *                                                                           *
 *****************************************************************************/
VOID
tmrRxCallback (tTimerListId timerListId)
{
    UNUSED_PARAM (timerListId);

    if (OsixEvtSend (gRecvTaskGlobals.capwapRecvTaskId,
                     CAP_RECV_RELINQUISH_EVENT) != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Timer event send failure\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Timer event send failure\n"));
    }

    /* restart the timer */
/* coverity fix begin */
    /*TmrStart(pTimerListId,&tmrRxTmrBlk,CAPWAP_CPU_RELINQUISH_RX_TMR,60,0)); */
    if ((TmrStart (pTimerListId, &tmrRxTmrBlk,
                   CAPWAP_CPU_RELINQUISH_RX_TMR, 60, 0)) != TMR_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, " Timer Start FAILED.\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      " Timer Start FAILED.\r\n"));
    }
/* coverity fix end */
}
#endif

/*****************************************************************************
 *                                                                           *
 * Function     : CapwapRecvMainTaskInit                                     *
 *                                                                           *
 * Description  : CAPWAP Receiver task initialization routine.               *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS, if initialization succeeds                   *
 *                OSIX_FAILURE, otherwise                                    *
 *                                                                           *
 *****************************************************************************/
UINT4
CapwapRecvMainTaskInit (VOID)
{
    UINT4               u4Event;
    tCfaMsgStruct       CfaMsgStruct;

    MEMSET (&CfaMsgStruct, 0, sizeof (tCfaMsgStruct));
#ifdef WLC_WANTED
#ifdef NPAPI_WANTED
    tCapwapNpParams     ptCapwapNpParams;
    tFsHwNp             FsHwNp;
    tCapwapNpModInfo   *pCapwapNpModInfo = NULL;
    tCapwapNpWrEnable  *pCapwapEnable = NULL;
    tCapwapNpWrCreateMcTunnel *pCreateMcTunnel = NULL;
    tCapwapNpWrCreateBcTunnel *pCreateBcTunnel = NULL;
    tMacAddr            wlcMacAddr = { 0x00, 0x01, 0x02, 0x03, 0x04, 0x07 };

    MEMSET (&ptCapwapNpParams, 0, sizeof (tCapwapNpParams));
#endif
#endif

#ifdef WTP_WANTED
#if 0
    /* If the address alloc method is dynamic initiate DHCP discovery for IP resolution */
    if (IP_ADDR_ALLOC_DYNAMIC == IssGetConfigModeFromNvRam ())
    {
        CfaProcessWssIfMsg (CFA_WSS_WTP_INIT, &CfaMsgStruct);
    }
#endif
#endif

    /* Create buffer pools for data structures */
    if (CapwapRecvMemInit () == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Memory Pool Creation Failed\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Memory Pool Creation Failed\n"));
        lrInitComplete (OSIX_FAILURE);
        return OSIX_FAILURE;
    }

    if (OsixTskIdSelf (&gRecvTaskGlobals.capwapRecvTaskId) == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    " !!!!! CAPWAP RECV TASK INIT FAILURE  !!!!! \n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      " !!!!! CAPWAP RECV TASK INIT FAILURE  !!!!! \n"));
        CapwapRecvMemClear ();
        lrInitComplete (OSIX_FAILURE);
        return OSIX_FAILURE;
    }

    MEMCPY (gRecvTaskGlobals.au1TaskSemName, CAPWAP_MUT_EXCL_RECEIVE_SEM_NAME,
            OSIX_NAME_LEN);

    if (OsixCreateSem (CAPWAP_MUT_EXCL_RECEIVE_SEM_NAME,
                       CAPWAP_SEM_CREATE_INIT_CNT, 0,
                       &gRecvTaskGlobals.capwapReceiveTaskSemId) ==
        OSIX_FAILURE)
    {
        CAPWAP_TRC1 (CAPWAP_FAILURE_TRC,
                     "Seamphore Creation failure for %s \n",
                     CAPWAP_MUT_EXCL_RECEIVE_SEM_NAME);
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Seamphore Creation failure for %s \n",
                      CAPWAP_MUT_EXCL_RECEIVE_SEM_NAME));

        CapwapRecvMemClear ();
        lrInitComplete (OSIX_FAILURE);
        return OSIX_FAILURE;
    }

    gCapwapGlobals.CapwapGlbMib.i4FsCapwapEnable = CLI_FSCAPWAP_ENABLE;

    if (CapwapEnableSockets () != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Unable to Enable the scokets of capwap module\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Unable to Enable the scokets of capwap module\r\n"));
        CapwapRecvMemClear ();
        lrInitComplete (OSIX_FAILURE);
        return OSIX_FAILURE;
    }

#ifdef WLC_WANTED
#ifdef NPAPI_WANTED
    /* Static values have to come from Capwap. Currently static. And needs to */
    /* be moved to capwap enable command. */

    ptCapwapNpParams.u4Ttl = IPV4_TTL;
    ptCapwapNpParams.u4LocalDataPort = DESTINATION_DATA_PORT;
    ptCapwapNpParams.u4LocalControlPort = DESTINATION_CONTROL_PORT;

#if CAP_WANTED
    if (WssGetCapwapUdpSPortFromWlcNvRam () != 0)
    {

        ptCapwapNpParams.u4LocalDataPort =
            WssGetCapwapUdpSPortFromWlcNvRam () + 1;
        ptCapwapNpParams.u4LocalControlPort =
            WssGetCapwapUdpSPortFromWlcNvRam ();
    }
    else
    {

        ptCapwapNpParams.u4LocalDataPort = CAPWAP_UDP_PORT_DATA;
        ptCapwapNpParams.u4LocalControlPort = CAPWAP_UDP_PORT_CONTROL;
    }
#endif
    ptCapwapNpParams.wlcIpAddr.ipAddr.u4_addr[0] = IPV4_WLC_IP_ADDRESS;
    MEMCPY (ptCapwapNpParams.wlcMacAddr, wlcMacAddr, sizeof (tMacAddr));

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_CAPWAP_MODULE,    /* Module ID */
                         CAPWAP_ENABLE,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pCapwapNpModInfo = &(FsHwNp.CapwapNpModInfo);
    pCapwapEnable = &pCapwapNpModInfo->CapwapNpEnable;

    pCapwapEnable->ptCapwapNpParams = &ptCapwapNpParams;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "ERROR[NP]: CAPWAP init failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "ERROR[NP]: CAPWAP init failed \r\n"));
        return OSIX_FAILURE;
    }

    MEMSET (&ptCapwapNpParams, 0, sizeof (tCapwapNpParams));

    /* IPv4 or IPv6 */
    ptCapwapNpParams.remoteIpAddr.u2AddressLen = CAPWAP_NP_ADDRESS_IPV4;
    ptCapwapNpParams.remoteIpAddr.ipAddr.u4_addr[0] = IPV4_IP_ADDRESS;
    ptCapwapNpParams.u4DestDataPort = DESTINATION_DATA_PORT;

#if CAP_WANTED
    if (WssGetCapwapUdpSPortFromWlcNvRam () != 0)
    {

        ptCapwapNpParams.u4DestDataPort =
            WssGetCapwapUdpSPortFromWlcNvRam () + 1;
    }
    else
    {

        ptCapwapNpParams.u4DestDataPort = CAPWAP_UDP_PORT_DATA;
    }
#endif
    ptCapwapNpParams.transProto = CAPWAP_UDP;    /* UDP or UDP-lite */
    ptCapwapNpParams.u4PMTU = IPV4_PMTU;
    ptCapwapNpParams.wtpTunnelMode = 1;    /* 802.3 or 802.11 */
    ptCapwapNpParams.u4RadioMacIncl = 1;

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_CAPWAP_MODULE,    /* Module ID */
                         CAPWAP_CREATE_MULTICAST_TUNNEL,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pCapwapNpModInfo = &(FsHwNp.CapwapNpModInfo);
    pCreateMcTunnel = &pCapwapNpModInfo->CapwapNpCreateMcTunnel;

    pCreateMcTunnel->ptCapwapNpParams = &ptCapwapNpParams;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "ERROR[NP]: CAPWAP multicast tunnel"
                    "creation failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "ERROR[NP]: CAPWAP multicast tunnel"
                      "creation failed \r\n"));
        return OSIX_FAILURE;
    }

    MEMSET (&ptCapwapNpParams, 0, sizeof (tCapwapNpParams));

    /* IPv4 or IPv6 */
    ptCapwapNpParams.remoteIpAddr.u2AddressLen = CAPWAP_NP_ADDRESS_IPV4;
    ptCapwapNpParams.remoteIpAddr.ipAddr.u4_addr[0] = IPV4_IP_ADD;
    ptCapwapNpParams.u4DestDataPort = DESTINATION_DATA_PORT;

#if CAP_WANTED
    if (WssGetCapwapUdpSPortFromWlcNvRam () != 0)
    {

        ptCapwapNpParams.u4DestDataPort =
            WssGetCapwapUdpSPortFromWlcNvRam () + 1;
    }
    else
    {

        ptCapwapNpParams.u4DestDataPort = CAPWAP_UDP_PORT_DATA;
    }
#endif

    ptCapwapNpParams.transProto = CAPWAP_UDP;
    ptCapwapNpParams.u4PMTU = IPV4_PMTU;
    ptCapwapNpParams.wtpTunnelMode = 1;    /* 802.3 or 802.11 */
    ptCapwapNpParams.u4RadioMacIncl = 1;

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_CAPWAP_MODULE,    /* Module ID */
                         CAPWAP_CREATE_BROADCAST_TUNNEL,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pCapwapNpModInfo = &(FsHwNp.CapwapNpModInfo);
    pCreateBcTunnel = &pCapwapNpModInfo->CapwapNpCreateBcTunnel;

    pCreateBcTunnel->ptCapwapNpParams = &ptCapwapNpParams;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "ERROR[NP]: CAPWAP broadcast tunnel"
                    "creation failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "ERROR[NP]: CAPWAP broadcast tunnel"
                      "creation failed \r\n"));
        return OSIX_FAILURE;
    }

#endif
#endif
#if CAP_WANTED
#ifdef IP6_WANTED
#if 0
    /* Create the UDP socket for CAPWAP Ctrl Chnl IPv6 */
    if (CapwapUdpSock6Init () != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CAPWAP Ctrl chnel Sock6 Initialization failed \r\n");
        CapwapRecvMemClear ();
        lrInitComplete (OSIX_FAILURE);
        return OSIX_FAILURE;
    }
    /* Create the UDP socket for CAPWAP Data Chnl Ipv6 */
    if (CapwapDataUdpSock6Init () != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CAPWAP Data chnel Sock6 Initialization failed \r\n");
        CapwapRecvMemClear ();
        lrInitComplete (OSIX_FAILURE);
        return OSIX_FAILURE;
    }
#endif
#endif
#endif
    if (CapwapEnableDTLSInit () != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Enable DTSL init failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Enable DTSL init failed \r\n"));
        CapwapRecvMemClear ();
        lrInitComplete (OSIX_FAILURE);
        return OSIX_FAILURE;
    }
    gRecvTaskGlobals.u1IsCapwapInitialized = CAPWAP_SUCCESS;
    CAPWAP_TRC (CAPWAP_INIT_TRC,
                "capwapRecvMainTaskInit: Receiver Task Init Completed \r\n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                  "capwapRecvMainTaskInit: Receiver Task Init Completed \r\n"));

#if CAP_WANTED
    /* create a timer */
    if (TMR_FAILURE ==
        TmrCreateTimerList ((CONST UINT1 *) CAPWAP_RECV_TASK_NAME, 0,
                            tmrRxCallback, &pTimerListId))
    {
        lrInitComplete (OSIX_FAILURE);
        return OSIX_FAILURE;
    }

    /* start the timer */
    if (TmrStart (pTimerListId, &tmrRxTmrBlk,
                  CAPWAP_CPU_RELINQUISH_RX_TMR, TMR, 0) == TMR_FAILURE)
    {
        lrInitComplete (OSIX_FAILURE);
        return OSIX_FAILURE;
    }
#endif

    /* initialize counter to zero */
    gu4CPURelinquishCounter = 0;

    lrInitComplete (OSIX_SUCCESS);

    while (1)
    {
        CAPWAP_TRC (CAPWAP_MGMT_TRC, "Receiver task wating for the event \r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                      "Receiver task wating for the event \r\n"));
        OsixEvtRecv (gRecvTaskGlobals.capwapRecvTaskId,
                     CTRL_IPV4PKT_ARRIVAL_EVENT | CTRL_IPV6PKT_ARRIVAL_EVENT |
                     DATA_IPV4PKT_ARRIVAL_EVENT | DATA_IPV6PKT_ARRIVAL_EVENT |
                     MULTICAST_DATA_IPV4PKT_ARRIVAL_EVENT, OSIX_WAIT, &u4Event);
        {

            CAPWAP_RCV_TASK_LOCK;

            if (u4Event & CTRL_IPV4PKT_ARRIVAL_EVENT)
            {
#ifdef MEMTRACE_WANTED
                MemPrintStats (CRU_BUF_MEM_TYPE);
#endif
                CapwapRecvCtrlIpv4Packet ();
                if (i4CwpCtrlSock4Id != SOCKET_ID)
                {
                    if (SelAddFd (i4CwpCtrlSock4Id, CapwapPacketOnSocket) !=
                        OSIX_SUCCESS)
                    {
                    }
                }

                CapwapCheckRelinquishCounters ();
            }
            if (u4Event & DATA_IPV4PKT_ARRIVAL_EVENT)
            {
#ifdef MEMTRACE_WANTED
                MemPrintStats (CRU_BUF_MEM_TYPE);
#endif
                CapwapRecvDataIpv4Packet ();
                if (i4CwpDataSock4Id != SOCKET_ID)
                {
                    if (SelAddFd (i4CwpDataSock4Id, CapwapPacketOnSocket) !=
                        OSIX_SUCCESS)
                    {
                    }
                }
                CapwapCheckRelinquishCounters ();
            }
#ifdef WTP_WANTED
            if (u4Event & MULTICAST_DATA_IPV4PKT_ARRIVAL_EVENT)
            {
                CapwapRecvMulDataIpv4Packet ();
                if (i4CwpMulDataSock4Id != SOCKET_ID)
                {
                    SelAddFd (i4CwpMulDataSock4Id, CapwapPacketOnSocket);
                }

            }
#endif
#ifdef IP6_WANTED
#if 0

            if (u4Event & CTRL_IPV6PKT_ARRIVAL_EVENT)
            {
                CapwapCtrlRevIpv6Packet ();
                if (i4CwpCtrlSock6Id != SOCKET_ID)
                {
                    SelAddFd (i4CwpCtrlSock6Id, CapwapPacketOnSocket);
                }
                CapwapCheckRelinquishCounters ();
            }
            if (u4Event & DATA_IPV6PKT_ARRIVAL_EVENT)
            {
                CapwapDataRecvIpv6Packet ();
                if (i4CwpDataSock6Id != SOCKET_ID)
                {
                    SelAddFd (i4CwpDataSock6Id, CapwapPacketOnSocket);
                }
                CapwapCheckRelinquishCounters ();
            }
#endif
#endif
#if CAP_WANTED
            else if (u4Event & CAP_RECV_RELINQUISH_EVENT)
            {
                gu4CPURelinquishCounter = 0;
            }
            if (100 == gu4CPURelinquishCounter)
            {
                OsixEvtRecv (gRecvTaskGlobals.capwapRecvTaskId,
                             CAP_RECV_RELINQUISH_EVENT, OSIX_WAIT, &u4Event);
                gu4CPURelinquishCounter = 0;
            }
#endif

            CAPWAP_RCV_TASK_UNLOCK;
        }
    }
    return OSIX_SUCCESS;
}

INT4
CapwapCtrlRevIpv6Packet ()
{
    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                "IPV6 is not supported for WSS package\r\n");
    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                  "IPV6 is not supported for WSS package\r\n"));
    return OSIX_SUCCESS;
}

INT4
CapwapDataRecvIpv6Packet ()
{
    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                "IPV6 is not supported for WSS package\r\n");
    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                  "IPV6 is not supported for WSS package\r\n"));
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : CapwapRecvMainTaskLock                                     */
/*                                                                           */
/* Description  : Lock the Capwap Recv Main Task                             */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
CapwapRecvMainTaskLock (VOID)
{
    CAPWAP_FN_ENTRY ();
    /*** For debug *****/
    CAPWAP_TRC1 (CAPWAP_MGMT_TRC,
                 "Capwap task lock. Capwap Conf Sem Id is %d \n",
                 gRecvTaskGlobals.capwapReceiveTaskSemId);
    SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4CapwapSysLogId,
                  "Capwap task lock. Capwap Conf Sem Id is %d \n",
                  gRecvTaskGlobals.capwapReceiveTaskSemId));
    if (OsixSemTake (gRecvTaskGlobals.capwapReceiveTaskSemId) == OSIX_FAILURE)
    {
        CAPWAP_TRC1 (CAPWAP_FAILURE_TRC,
                     "TakeSem failure for %s \n",
                     CAPWAP_MUT_EXCL_RECEIVE_SEM_NAME);
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "TakeSem failure for %s \n",
                      CAPWAP_MUT_EXCL_RECEIVE_SEM_NAME));
        return OSIX_FAILURE;
    }
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : CapwapRecvMainTaskUnLock                                     */
/*                                                                           */
/* Description  : Lock the Capwap Recv Main Task                             */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
CapwapRecvMainTaskUnLock (VOID)
{
    CAPWAP_FN_ENTRY ();

    /*** For debug *****/
    CAPWAP_TRC1 (CAPWAP_MGMT_TRC,
                 "Capwap Receive Task Sem Id is %d \n",
                 gRecvTaskGlobals.capwapReceiveTaskSemId);
    SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4CapwapSysLogId,
                  "Capwap Receive Task Sem Id is %d \n",
                  gRecvTaskGlobals.capwapReceiveTaskSemId));
    /*** For debug *****/

    OsixSemGive (gRecvTaskGlobals.capwapReceiveTaskSemId);

    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : CapwapRecvMemClear                                         *
 *                                                                           *
 * Description  : Deletes all the memory pools in Capwap                     *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 *                                                                           *
 *****************************************************************************/
VOID
CapwapRecvMemClear (VOID)
{
    CapwapSizingMemDeleteMemPools ();
    return;
}

/*****************************************************************************
 *                                                                           *
 * Function     : CapwapCtrlUdpSockInit                                      *
 *                                                                           *
 * Description  : This routine initialises the socket related parameters.    *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS  or OSIX_FAILURE                              *
 *                                                                           *
 *****************************************************************************/
INT4
CapwapCtrlUdpSockInit (VOID)
{
    INT4                i4Flags = 0, i4RetVal;
    struct sockaddr_in  CapwapLocalAddr;
#ifdef WLC_WANTED
    INT4                i4OptVal = OSIX_TRUE;
#endif

    CAPWAP_FN_ENTRY ();

    i4CwpCtrlSock4Id = socket (AF_INET, SOCK_DGRAM, IPPROTO_UDP);

    if (i4CwpCtrlSock4Id < 0)
    {
        perror ("capwapCtrlUdpSockInit: Unable to open stream socket");
        return OSIX_FAILURE;
    }

    CapwapLocalAddr.sin_family = AF_INET;
    CapwapLocalAddr.sin_addr.s_addr = OSIX_HTONL (INADDR_ANY);

#ifdef WLC_WANTED
    CapwapLocalAddr.sin_port = OSIX_HTONS (CAPWAP_UDP_PORT_CONTROL);

#if CAP_WANTED
    if (WssGetCapwapUdpSPortFromWlcNvRam () != 0)
    {

        UINT4               port = WssGetCapwapUdpSPortFromWlcNvRam ();
        CapwapLocalAddr.sin_port = OSIX_HTONS (port);
    }
    else
    {

        CapwapLocalAddr.sin_port = OSIX_HTONS (CAPWAP_UDP_PORT_CONTROL);
    }
#endif

#else
    CapwapLocalAddr.sin_port = 0;
#endif

    /* Bind with the socket */
    i4RetVal = bind (i4CwpCtrlSock4Id,
                     (struct sockaddr *) &CapwapLocalAddr,
                     sizeof (CapwapLocalAddr));

    if (i4RetVal < 0)
    {
        perror ("capwapCtrlUdpSockInit: Unable to bind Address");
        return (OSIX_FAILURE);
    }

#ifdef WLC_WANTED
    if (setsockopt (i4CwpCtrlSock4Id, SOL_SOCKET,
                    SO_BROADCAST, (INT4 *) &i4OptVal, sizeof (i4OptVal)) < 0)
    {
        perror ("capwapCtrlUdpSockInit: Unable to set" "SO_REUSEADDR options");
        close (i4CwpCtrlSock4Id);
        return (OSIX_FAILURE);
    }
#ifdef LNXIP4_WANTED
    /* WLC should listen on the multicast address 224.0.1.140 */
    /* Set the socket multicast option */
    if (CapwapJoinMulticastGroup (i4CwpCtrlSock4Id) == OSIX_FAILURE)
    {
        close (i4CwpCtrlSock4Id);
        return (OSIX_FAILURE);
    }
#endif
    /* Need to set the TTL ???
       if (setsockopt (i4CwpCtrlSock4Id,IPPROTO_IP,
       IP_MULTICAST_TTL,(INT4 *) &i4OptVal,
       sizeof (i4OptVal)) < 0)
       {
       close (i4CwpCtrlSock4Id);
       return (OSIX_FAILURE);
       } */
#endif
    if ((i4Flags = fcntl (i4CwpCtrlSock4Id, F_GETFL, 0)) < 0)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Acquiring the flags for the socket failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Acquiring the flags for the socket failed \r\n"));
        return (OSIX_FAILURE);
    }

    i4Flags |= O_NONBLOCK;

    if (fcntl (i4CwpCtrlSock4Id, F_SETFL, i4Flags) < 0)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Set flags for the socket failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Set flags for the socket failed \r\n"));
        return (OSIX_FAILURE);
    }

    if (SelAddFd (i4CwpCtrlSock4Id, CapwapPacketOnSocket) != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "FD add Failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "FD add Failed \r\n"));
        close (i4CwpCtrlSock4Id);
        return OSIX_FAILURE;
    }
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

INT4
CapwapUpdatePathMtu (UINT4 u2DestAddr, UINT4 u4DestPort, UINT4 *pu4PMTU)
{
    INT4                i4pmtu_len = sizeof (INT4);
    struct sockaddr_in  CapwapRemoteAddr;
    INT4                i4OpnVal = OSIX_TRUE;
    INT4                i4RetVal;
    INT4                i4tmpCtrlSock4Id = -1;    /* Ipv4 Socket Id */
    struct sockaddr_in  CapwapLocalAddr;
    INT4                i4Flags = 0;

    i4tmpCtrlSock4Id = socket (AF_INET, SOCK_DGRAM, IPPROTO_UDP);

    if (i4tmpCtrlSock4Id < 0)
    {
        perror ("CapwapUpdatePathMtu: Unable to open stream socket");
        return OSIX_FAILURE;
    }

    CapwapLocalAddr.sin_family = AF_INET;
    CapwapLocalAddr.sin_addr.s_addr = OSIX_HTONL (INADDR_ANY);
    CapwapLocalAddr.sin_port = 0;

    i4RetVal = bind (i4tmpCtrlSock4Id,
                     (struct sockaddr *) &CapwapLocalAddr,
                     sizeof (CapwapLocalAddr));

    if (i4RetVal < 0)
    {
        close (i4tmpCtrlSock4Id);
        perror ("CapwapUpdatePathMtu: Unable to bind Address");
        return (OSIX_FAILURE);
    }

    if ((i4Flags = fcntl (i4tmpCtrlSock4Id, F_GETFL, 0)) < 0)
    {
        close (i4tmpCtrlSock4Id);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Acquiring the flags for the socket failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Acquiring the flags for the socket failed \r\n"));
        return (OSIX_FAILURE);
    }

    i4Flags |= O_NONBLOCK;

    if (fcntl (i4tmpCtrlSock4Id, F_SETFL, i4Flags) < 0)
    {
        close (i4tmpCtrlSock4Id);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Set flags for the socket failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Set flags for the socket failed \r\n"));
        return (OSIX_FAILURE);
    }

    CapwapRemoteAddr.sin_family = AF_INET;
    CapwapRemoteAddr.sin_addr.s_addr = OSIX_HTONL (u2DestAddr);
    CapwapRemoteAddr.sin_port = OSIX_HTONS (u4DestPort);

    i4OpnVal = IP_PMTUDISC_PROBE;
    i4RetVal = setsockopt (i4tmpCtrlSock4Id, SOL_IP, IP_MTU_DISCOVER,
                           (INT4 *) &i4OpnVal, sizeof (i4OpnVal));
    if (i4RetVal < 0)
    {
        close (i4tmpCtrlSock4Id);
        perror ("unable to set IP_PMTUDISC_PROBE option: \r\n");
        return (OSIX_FAILURE);
    }

    i4OpnVal = IP_PMTUDISC_DO;
    i4RetVal = setsockopt (i4tmpCtrlSock4Id, SOL_IP, IP_MTU_DISCOVER,
                           (INT4 *) &i4OpnVal, sizeof (i4OpnVal));
    if (i4RetVal < 0)
    {
        close (i4tmpCtrlSock4Id);
        perror ("Failed to Set IP_MTU_DISCOVER: \r\n");
        return (OSIX_FAILURE);
    }
    if (connect (i4tmpCtrlSock4Id, (struct sockaddr *) &CapwapRemoteAddr,
                 sizeof (CapwapRemoteAddr)) == SOCKET_ID)
    {
        close (i4tmpCtrlSock4Id);
        perror ("connect");
        return (OSIX_FAILURE);
    }
    if (getsockopt (i4tmpCtrlSock4Id, IPPROTO_IP, IP_MTU, (INT4 *) pu4PMTU,
                    (socklen_t *) & i4pmtu_len) == SOCKET_ID)
    {
        close (i4tmpCtrlSock4Id);
        perror ("getsockopt");
        return (OSIX_FAILURE);
    }

#if CAP_WANTED
    INT4                i4pmtu_len = sizeof (INT4);
    struct sockaddr_in  CapwapRemoteAddr;

    CapwapRemoteAddr.sin_family = AF_INET;
    CapwapRemoteAddr.sin_addr.s_addr = OSIX_HTONL (u2DestAddr);
    CapwapRemoteAddr.sin_port = OSIX_HTONS (u4DestPort);

    if (connect (i4CwpCtrlSock4Id, (struct sockaddr *) &CapwapRemoteAddr,
                 sizeof (CapwapRemoteAddr)) == SOCKET_ID)
    {
        close (i4tmpCtrlSock4Id);
        perror ("connect");
        return (OSIX_FAILURE);
    }

    if (getsockopt (i4CwpCtrlSock4Id, IPPROTO_IP, IP_MTU, (INT4 *) pu4PMTU,
                    (socklen_t *) & i4pmtu_len) == SOCKET_ID)
    {
        close (i4tmpCtrlSock4Id);
        perror ("getsockopt");
        return (OSIX_FAILURE);
    }
#endif

    close (i4tmpCtrlSock4Id);

    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapRecvCtrlIpv4Packet                           */
/*  Description     : The function receiver the Ipv4 packet from the    */
/*                    UDP socket                                        */
/*  Input(s)        : None                                              */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
CapwapRecvCtrlIpv4Packet ()
{
    struct sockaddr_in  PeerAddr;
    UINT4               u4DestAddr, u4DestPort;
    INT4                i4PktLen = 0;
    INT4                i4AddrLen = 0;
    INT4                i4Status;
    UINT1               au1Frame[CAPWAP_MAX_PKT_LEN];
    UINT1              *pu1RxBuf = au1Frame;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;

    CAPWAP_FN_ENTRY ();

    MEMSET (&PeerAddr, 0, sizeof (struct sockaddr_in));
    MEMSET (au1Frame, FRAME_ADDRESS, CAPWAP_MAX_PKT_LEN);

    PeerAddr.sin_family = AF_INET;
    PeerAddr.sin_port = 0;
    PeerAddr.sin_addr.s_addr = OSIX_HTONL (INADDR_ANY);
    i4AddrLen = sizeof (PeerAddr);

    while ((i4PktLen = recvfrom (i4CwpCtrlSock4Id, pu1RxBuf,
                                 CAPWAP_MAX_PKT_LEN, 0,
                                 (struct sockaddr *) &PeerAddr,
                                 &i4AddrLen)) > 0)
    {

        if (gu4CapwapShutdown == CAPWAP_SHUTDOWN)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "CapwapRecvCtrlIpv4Packet:Capwap protocol have shutdown.So dropping the packets \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "CapwapRecvCtrlIpv4Packet:Capwap protocol have shutdown.So dropping the packets \r\n"));
            return OSIX_FAILURE;
        }
        u4DestAddr = OSIX_NTOHL (PeerAddr.sin_addr.s_addr);
        u4DestPort = OSIX_NTOHS (PeerAddr.sin_port);
        pBuf = CAPWAP_ALLOCATE_CRU_BUF ((UINT4) i4PktLen, 0);
        if (pBuf == NULL)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "CapwapRecvCtrlIpv4Packet:Memory Allocation Failed \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "CapwapRecvCtrlIpv4Packet:Memory Allocation Failed \r\n"));
            continue;
        }
        CAPWAP_COPY_TO_BUF (pBuf, pu1RxBuf, 0, (UINT4) i4PktLen);
        /* Copy the Destination port and source IP address
         * in to the reserved members of tMODULE_DATA
         * structure  */

        CRU_BUF_Set_U4Reserved1 (pBuf, u4DestPort);

        CRU_BUF_Set_U4Reserved2 (pBuf, u4DestAddr);
        if (gu4CapwapDebugMask & CAPWAP_PKTDUMP_TRC)
        {
            DbgDumpCapwapPkt (pBuf, CAPWAP_PKT_FLOW_IN);
        }
        /* Process the received CAPWAP packet */
        i4Status = (INT4) CapwapProcessCtrlPacket (pBuf);

        if (i4Status == OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "capwapRecvCtrlIpv4Packet:Fail to process the Recv packet \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "capwapRecvCtrlIpv4Packet:Fail to process the Recv packet \r\n"));
            continue;
        }

    }
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapRecvDataIpv4Packet                          */
/*  Description     : The function receiver the Ipv4 packet from the    */
/*                    UDP socket                                        */
/*  Input(s)        : None                                              */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
CapwapRecvDataIpv4Packet ()
{
    struct sockaddr_in  PeerAddr;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    UINT1              *pu1Frame = NULL;
    UINT1              *pu1RxBuf = NULL;
    UINT4               u4DestAddr, u4DestPort;
    INT4                i4PktLen = 0;
    INT4                i4AddrLen = 0, i4Status;
    UINT1               u1IsNotLinear = OSIX_TRUE;

    CAPWAP_FN_ENTRY ();

    MEMSET (&PeerAddr, 0, sizeof (struct sockaddr_in));
    PeerAddr.sin_family = AF_INET;
    PeerAddr.sin_port = 0;
    PeerAddr.sin_addr.s_addr = OSIX_HTONL (INADDR_ANY);
    i4AddrLen = sizeof (PeerAddr);

    pu1Frame = UtlShMemAllocCapwapPktBuf ();
    if (pu1Frame == NULL)
    {
        return OSIX_FAILURE;
    }
    pu1RxBuf = pu1Frame;
/*TO-DO Macro has to be assigned for The value 2350*/

    while ((i4PktLen = recvfrom (i4CwpDataSock4Id, pu1RxBuf, 2350,
                                 0, (struct sockaddr *) &PeerAddr,
                                 &i4AddrLen)) > 0)
    {
        if (gu4CapwapShutdown == CAPWAP_SHUTDOWN)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "CapwapRecvDataIpv4Packet:Capwap protocol have shutdown.So dropping the packets \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "CapwapRecvDataIpv4Packet:Capwap protocol have shutdown.So dropping the packets \r\n"));
            UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1Frame);
            return OSIX_FAILURE;
        }

        u4DestAddr = OSIX_NTOHL (PeerAddr.sin_addr.s_addr);
        u4DestPort = OSIX_NTOHS (PeerAddr.sin_port);

        if (u1IsNotLinear == OSIX_TRUE)
        {
            pBuf = CAPWAP_ALLOCATE_CRU_BUF ((UINT4) i4PktLen, 0);
            if (pBuf == NULL)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "capwapRecvDataIpv4Packet:Memory Alloc Failed \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "capwapRecvDataIpv4Packet:Memory Alloc Failed \r\n"));
                UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1Frame);
                return OSIX_FAILURE;
            }

            /* If the CRU buffer memory is not linear */
            CAPWAP_COPY_TO_BUF (pBuf, pu1RxBuf, 0, (UINT4) i4PktLen);
        }

        /* Copy the Destination port and source IP address
         * in to the reserved members of tMODULE_DATA
         * structure  */

        CRU_BUF_Set_U4Reserved1 (pBuf, u4DestPort);

        CRU_BUF_Set_U4Reserved2 (pBuf, u4DestAddr);

        if (gu4CapwapDebugMask & CAPWAP_PKTDUMP_TRC)
        {
            DbgDumpCapwapPkt (pBuf, CAPWAP_PKT_FLOW_IN);
        }

        /* Process the received CAPWAP packet */
        i4Status = (INT4) CapwapProcessDataPacket (pBuf);

        if (i4Status == OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "capwapRecvDataIpv4Packet:Failed to process the Recv packet\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "capwapRecvDataIpv4Packet:Failed to process the Recv packet\n"));
#ifdef WTP_WANTED
            CAPWAP_RELEASE_CRU_BUF (pBuf);
#endif
            UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1Frame);
            return OSIX_FAILURE;
        }
        MEMSET (pu1RxBuf, 0, CAPWAP_MAX_PKT_LEN);
    }
    UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1Frame);
    CAPWAP_FN_EXIT ()return OSIX_SUCCESS;
}

#ifdef WTP_WANTED
/*****************************************************************************
 *                                                                           *
 * Function     : CapwapRecvMulDataUdpSockInit                               *
 *                                                                           *
 * Description  : This routine recieved multicast/broadcast data packets.    *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS  or OSIX_FAILURE                              *
 *                                                                           *
 *****************************************************************************/

INT4
CapwapRecvMulDataIpv4Packet (void)
{
    struct sockaddr_in  PeerAddr;
    UINT1               au1Frame[CAPWAP_MAX_PKT_LEN];
    UINT1              *pu1RxBuf = au1Frame;
    UINT4               u4DestAddr, u4DestPort;
    INT4                i4PktLen = 0;
    INT4                i4AddrLen = 0, i4Status;
    UINT1               u1IsNotLinear = OSIX_TRUE;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;

    CAPWAP_FN_ENTRY ();

    MEMSET (&PeerAddr, 0, sizeof (struct sockaddr_in));
    while ((i4PktLen = recvfrom (i4CwpMulDataSock4Id, pu1RxBuf,
                                 CAPWAP_MAX_PKT_LEN, 0,
                                 (struct sockaddr *) &PeerAddr,
                                 &i4AddrLen)) > 0)
    {

        if (gu4CapwapShutdown == CAPWAP_SHUTDOWN)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "CapwapRecvMulDataIpv4Packet:Capwap protocol have shutdown.So dropping the packets \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "CapwapRecvMulDataIpv4Packet:Capwap protocol have shutdown.So dropping the packets \r\n"));
            return OSIX_FAILURE;
        }

        u4DestAddr = OSIX_NTOHL (PeerAddr.sin_addr.s_addr);
        u4DestPort = OSIX_NTOHS (PeerAddr.sin_port);

        if (u1IsNotLinear == OSIX_TRUE)
        {
            pBuf = CAPWAP_ALLOCATE_CRU_BUF (i4PktLen, 0);
            if (pBuf == NULL)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "capwapRecvDataIpv4Packet:Memory Alloc Failed \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "capwapRecvDataIpv4Packet:Memory Alloc Failed \r\n"));

                return OSIX_FAILURE;
            }

            /* If the CRU buffer memory is not linear */
            CAPWAP_COPY_TO_BUF (pBuf, pu1RxBuf, 0, i4PktLen);
        }

        /* Copy the Destination port and source IP address
         * in to the reserved members of tMODULE_DATA
         * structure  */

        CRU_BUF_Set_U4Reserved1 (pBuf, u4DestPort);

        CRU_BUF_Set_U4Reserved2 (pBuf, u4DestAddr);

        if (gu4CapwapDebugMask & CAPWAP_PKTDUMP_TRC)
        {
            DbgDumpCapwapPkt (pBuf, CAPWAP_PKT_FLOW_IN);
        }

        /* Process the received CAPWAP packet */
        i4Status = CapwapProcessDataPacket (pBuf);

        if (i4Status == OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "capwapRecvDataIpv4Packet:Failed to process the Recv packet\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "capwapRecvDataIpv4Packet:Failed to process the Recv packet\n"));
#ifdef WTP_WANTED
            CAPWAP_RELEASE_CRU_BUF (pBuf);
#endif
            return OSIX_FAILURE;
        }
        MEMSET (pu1RxBuf, 0, CAPWAP_MAX_PKT_LEN);
    }
    CAPWAP_FN_EXIT ()return OSIX_SUCCESS;
}
#endif
/*****************************************************************************
 *                                                                           *
 * Function     : CapwapDataUdpSockInit                                      *
 *                                                                           *
 * Description  : This routine initialises the socket related parameters.    *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS  or OSIX_FAILURE                              *
 *                                                                           *
 *****************************************************************************/
INT4
CapwapDataUdpSockInit (VOID)
{
    INT4                i4Flags = 0, i4RetVal;
    struct sockaddr_in  CapwapLocalAddr;
#ifdef WTP_WANTED
#ifdef BSDCOMP_SLI_WANTED
    struct sockaddr_in  CapwapMulticast;
    struct ip_mreqn     mreqn;
    UINT1               BcastGroupAddr[4] = { 224, 0, 1, 4 };
#ifndef LNXWIRELESS_WANTED
    UINT1               McastGroupAddr[4] = { 224, 0, 1, 3 };
#endif
#endif
#endif
    CAPWAP_FN_ENTRY ();

    i4CwpDataSock4Id = socket (AF_INET, SOCK_DGRAM, IPPROTO_UDP);

    if (i4CwpDataSock4Id < 0)
    {
        perror ("capwapDataUdpSockInit: Unable to open stream socket");
        return OSIX_FAILURE;
    }

    CapwapLocalAddr.sin_family = AF_INET;
    CapwapLocalAddr.sin_addr.s_addr = OSIX_HTONL (INADDR_ANY);
#ifdef WLC_WANTED

    CapwapLocalAddr.sin_port = OSIX_HTONS (DESTINATION_DATA_PORT);

#if CAP_WANTED
    if (WssGetCapwapUdpSPortFromWlcNvRam () != 0)
    {

        UINT4               port = WssGetCapwapUdpSPortFromWlcNvRam () + 1;
        CapwapLocalAddr.sin_port = OSIX_HTONS (port);
    }
    else
    {

        CapwapLocalAddr.sin_port = OSIX_HTONS (CAPWAP_UDP_PORT_DATA);
    }

#endif
#else

    CapwapLocalAddr.sin_port = 0;
#endif
    /* Bind with the socket */
    i4RetVal = bind (i4CwpDataSock4Id,
                     (struct sockaddr *) &CapwapLocalAddr,
                     sizeof (CapwapLocalAddr));

    if (i4RetVal < 0)
    {
        perror ("capwapDataUdpSockInit: Unable to bind Address");
        return (OSIX_FAILURE);
    }

    if ((i4Flags = fcntl (i4CwpDataSock4Id, F_GETFL, 0)) < 0)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Acquiring the flags for the socket failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Acquiring the flags for the socket failed \r\n"));
        return (OSIX_FAILURE);
    }

    i4Flags |= O_NONBLOCK;

    if (fcntl (i4CwpDataSock4Id, F_SETFL, i4Flags) < 0)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Set flags for the socket failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Set flags for the socket failed \r\n"));
        return (OSIX_FAILURE);
    }
#ifdef WLC_WANTED

    if (CapwapDataSelAddFd () == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to add the socket to the select utility \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to add the socket to the select utility \r\n"));

        return OSIX_FAILURE;
    }
#endif
#ifdef KERNEL_CAPWAP_WANTED
    CwpKernInit ();
#ifdef WLC_WANTED
    CapwapInitVlanDB ();
#endif
#endif
#ifdef WTP_WANTED
#ifdef BSDCOMP_SLI_WANTED
    i4CwpMulDataSock4Id = socket (AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (i4CwpMulDataSock4Id < 0)
    {
        perror ("capwapDataUdpSockInit: Unable to open multcast stream socket");
        return OSIX_FAILURE;
    }
    CapwapMulticast.sin_family = AF_INET;
    CapwapMulticast.sin_addr.s_addr = OSIX_HTONL (INADDR_ANY);
    CapwapMulticast.sin_port = OSIX_HTONS (DESTINATION_DATA_PORT);

#if CAP_WANTED
    if (WssGetCapwapUdpSPortFromWlcNvRam () != 0)
    {

        UINT4               port = WssGetCapwapUdpSPortFromWlcNvRam () + 1;
        CapwapLocalAddr.sin_port = OSIX_HTONS (port);
    }
    else
    {

        CapwapLocalAddr.sin_port = OSIX_HTONS (CAPWAP_UDP_PORT_DATA);
    }

#endif
#ifndef LNXWIRELESS_WANTED
    i4RetVal = bind (i4CwpMulDataSock4Id,
                     (struct sockaddr *) &CapwapMulticast,
                     sizeof (CapwapMulticast));
    if (i4RetVal < 0)
    {
        perror ("capwapMulDataUdpSockInit: Unable to bind Address");
        return (OSIX_FAILURE);
    }
    if ((i4Flags = fcntl (i4CwpMulDataSock4Id, F_GETFL, 0)) < 0)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Acquiring the flags for the socket failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Acquiring the flags for the socket failed \r\n"));
        return (OSIX_FAILURE);
    }

    i4Flags |= O_NONBLOCK;

    if (fcntl (i4CwpMulDataSock4Id, F_SETFL, i4Flags) < 0)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Set flags for the socket failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Set flags for the socket failed \r\n"));
        return (OSIX_FAILURE);
    }

    /* Add membership is done to receive the multicast/broadcast pkts addressed 
     * to 224.0.1.3/224.0.1.4 */
    memset (&mreqn, 0, sizeof (mreqn));
    memcpy (&mreqn.imr_multiaddr.s_addr, McastGroupAddr,
            sizeof (McastGroupAddr));

    mreqn.imr_ifindex = if_nametoindex (CAPWAP_BR_INTERFACE);
#endif
#ifndef LNXWIRELESS_WANTED
    if (setsockopt (i4CwpMulDataSock4Id, IPPROTO_IP, IP_ADD_MEMBERSHIP,
                    (void *) &mreqn, sizeof (mreqn)) < 0)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Set flags for the mcast socket failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Set flags for the mcast socket failed \r\n"));
        return (OSIX_FAILURE);
    }
#endif

    memset (&mreqn, 0, sizeof (mreqn));
    memcpy (&mreqn.imr_multiaddr.s_addr, BcastGroupAddr,
            sizeof (BcastGroupAddr));

    mreqn.imr_ifindex = if_nametoindex (CAPWAP_BR_INTERFACE);

#ifndef LNXWIRELESS_WANTED
    if (setsockopt (i4CwpMulDataSock4Id, IPPROTO_IP, IP_ADD_MEMBERSHIP,
                    (void *) &mreqn, sizeof (mreqn)) < 0)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Set flags for the bcast socket failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Set flags for the bcast socket failed \r\n"));
        return (OSIX_FAILURE);
    }
#endif

#endif
#endif
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

#ifdef IP6_WANTED
/************************************************************************/
/*  Function Name   : CapwapRecvCtrlIpv6Packet                           */
/*  Description     : The function receiver the Ipv6 packet from the    */
/*                    UDP socket                                        */
/*  Input(s)        : None                                              */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
#ifdef DEBUG_WANTED
INT4
CapwapRecvCtrlIpv6Packet ()
{
    struct sockaddr_in6 PeerAddr6;
    UINT1              *pu1RxBuf = NULL;
    UINT4               u4DestPort;
    tCapwapIpAddr       capwapIp6Addr;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    UINT1               au1Frame[CAPWAP_MAX_PKT_LEN];
    UINT1               u1IsNotLinear;

    CAPWAP_FN_ENTRY ();
    pBuf = CAPWAP_ALLOCATE_CRU_BUF (CAPWAP_MAX_PKT_LEN, 0);
    if (pBuf == NULL)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "capwapRecvDataIpv4Packet:Memory Allocation Failed \r\n");
        return OSIX_FAILURE;
    }
    pu1RxBuf = CAPWAP_BUF_IF_LINEAR (pBuf, 0, CAPWAP_MAX_PKT_LEN);
    if (pu1RxBuf == NULL)
    {
        MEMSET (au1Frame, FRAME_ADDRESS, CAPWAP_MAX_PKT_LEN);
        pu1RxBuf = au1Frame;
        u1IsNotLinear = OSIX_TRUE;
    }
    if (u4Events & CTRL_IPV4PKT_ARRIVAL_EVENT)
    {
        i4SockId = gRecvTaskGlobals.i4CwpCtrlSock6Id;
    }
    else
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapRecvCtrlIpv6Packet:Received the unknown event\n");
        return OSIX_FAILURE;
    }
    while ((i4PktLen = recvfrom (i4SockId, pu1RxBuf, CAPWAP_MAX_PKT_LEN, 0,
                                 (struct sockaddr *) &PeerAddr6,
                                 &i4AddrLen)) > 0)

    {
        /* Copying V6 Address */
        MEMCPY (capwapIp6Addr.Ip6CapwapAddr, &PeerAddr6.sin6_addr.s6_addr,
                sizeof (PeerAddr6.sin6_addr.s6_addr));

        u4DestPort = OSIX_NTOHS (PeerAddr6.sin_port);

        /* Process the received CAPWAP packet
           capwapProcessCtrlPacket (pu1RxBuf,
           capwapIp6Addr.Ip6CapwapAddr,
           u4DestAddr,
           (UINT4) i4PktLen); */
    }
    CAPWAP_RELEASE_CRU_BUF (pBuf);
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;

}
#endif
/************************************************************************/
/*  Function Name   : CapwapRecvCtrlIpv6Packet                           */
/*  Description     : The function receiver the Ipv6 packet from the    */
/*                    UDP socket                                        */
/*  Input(s)        : None                                              */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
CapwapRecvCtrlIpv6Packet ()
{
    return OSIX_SUCCESS;
}
#endif

/************************************************************************/
/*  Function Name   : CapwapDataSelAddFd                                */
/*  Description     : The function add the capwap data socket Id to the */
/*                    select utility.                                   */
/*                                                                      */
/*  Input(s)        : None                                              */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
CapwapDataSelAddFd (VOID)
{
    CAPWAP_FN_ENTRY ();
    if (SelAddFd (i4CwpDataSock4Id, CapwapPacketOnSocket) != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "FD add Failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "FD add Failed \r\n"));
        return OSIX_FAILURE;
    }
#ifdef WTP_WANTED
    if (SelAddFd (i4CwpMulDataSock4Id, CapwapPacketOnSocket) != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "FD add Failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "FD add Failed \r\n"));

        return OSIX_FAILURE;
    }
#endif

#ifdef IP6_WANTED
#ifdef DEBUG_WANTED
    if (SelAddFd (i4CwpDataSock6Id, CapwapPacketOnSocket) != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "FD add Failed \r\n");
        printf ("returned Failure in SelAddFd - IP6_WANTED\n\n");
        return OSIX_FAILURE;
    }
#endif
#endif
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : CapwapCtrlUdpTxPacket                                    *
 *                                                                           *
 * Description  : This function used to  transmits the given                 *
 *                CAPWAP packet over UDP socket                              *
 *                                                                           *
 * Input        : u4DestPort  - Destination UDP port                         *
 *                u4DestAddr  - Destination IP Address                       *
 *                pBuf        - Pointer to the linear buffer                 *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 ******************************************************************************/
INT4
CapwapCtrlUdpTxPacket (tCRU_BUF_CHAIN_HEADER * pBuf,
                       UINT4 u4DestAddr, UINT4 u4DestPort, UINT4 u4PktLen)
{
    tCapwapControlPacket CapwapCtrlmsg;
    struct sockaddr_in  PeerAddr;
    INT4                i4RetVal;
    UINT4               result;
    INT4                resultlen = 0;
    UINT1              *pu1TxBuf;
    UINT1               au1Frame[CAPWAP_MAX_PKT_LEN];
#ifdef WTP_WANTED
    INT4                i4OpnVal = OSIX_TRUE;
    INT4                i4Opn = OSIX_TRUE;
#if CAP_WANTED
    struct ifaddrs     *ifap;
    struct ifaddrs     *ifa;
#endif

    struct ifconf       ifc;
    INT4                i4ErrCount = 0;
#ifdef LNXIP4_WANTED
    INT4                i4IfCount = 0;
    char                buf[8192] = { 0 };
    struct ifreq       *ifr = NULL;
    int                 sck = 0;
    int                 nInterfaces = 0;
    int                 i = 0;
    char                ip[INET6_ADDRSTRLEN] = { 0 };
    struct ifreq       *item;
    struct sockaddr    *addr;
    void               *pVaddr_in = NULL;
    struct sockaddr_in *addr_in = NULL;
#else
    struct msghdr       PktInfo;
    struct in_pktinfo  *pIpPktInfo;
    UINT1               au1Cmsg[24];
#endif

#endif

    INT4                i4DscpCode = 0;
    CAPWAP_FN_ENTRY ();
    MEMSET (&PeerAddr, 0, sizeof (struct sockaddr_in));

#ifdef WTP_WANTED
    MEMSET (&ifc, 0, sizeof (struct ifconf));

    if ((u4DestAddr == CAPWAP_BROADCAST_ADDR) ||
        (u4DestAddr == CAPWAP_MULTICAST_ADDR))
    {
        /* Set the BROADCAST option in the socket  */
        i4RetVal = setsockopt (i4CwpCtrlSock4Id, SOL_SOCKET, SO_BROADCAST,
                               (INT4 *) &i4OpnVal, sizeof (i4OpnVal));
        if (i4RetVal < 0)
        {
            perror ("capwapCtrlUdpTxPacket: Unable  set SO_BROADCAST options ");
            return OSIX_FAILURE;
        }
    }
#endif
    PeerAddr.sin_family = AF_INET;
    PeerAddr.sin_addr.s_addr = OSIX_HTONL (u4DestAddr);
    PeerAddr.sin_port = (UINT2) OSIX_HTONS (u4DestPort);
    if (gu4CapwapDebugMask & CAPWAP_PKTDUMP_TRC)
    {
        DbgDumpCapwapPkt (pBuf, CAPWAP_PKT_FLOW_OUT);
        if (CapwapParseControlPacket (pBuf, &CapwapCtrlmsg) == OSIX_SUCCESS)
        {
            CapwapParseStructureMemrelease (&CapwapCtrlmsg);
        }
    }
    pu1TxBuf = CAPWAP_BUF_IF_LINEAR (pBuf, 0, u4PktLen);
    if (pu1TxBuf == NULL)
    {
        MEMSET (au1Frame, 0x00, u4PktLen);
        CAPWAP_COPY_FROM_BUF (pBuf, au1Frame, 0, u4PktLen);
        pu1TxBuf = au1Frame;
    }
    /* Send the packet in the socket */
    if ((u4DestAddr == CAPWAP_BROADCAST_ADDR) ||
        (u4DestAddr == CAPWAP_MULTICAST_ADDR))
    {
#ifdef WTP_WANTED
#ifdef LNXIP4_WANTED

        sck = socket (PF_INET, SOCK_DGRAM, 0);
        if (sck < 0)
        {
            perror ("socket");
            return 1;
        }

        /* Query available interfaces. */
        ifc.ifc_len = sizeof (buf);
        ifc.ifc_buf = buf;
        if (ioctl (sck, SIOCGIFCONF, &ifc) < 0)
        {
            close (sck);
            perror ("ioctl(SIOCGIFCONF)");
            return 1;
        }

        ifr = ifc.ifc_req;
        nInterfaces = ifc.ifc_len / sizeof (struct ifreq);

        for (i = 0; i < nInterfaces; i++)
        {
            item = &ifr[i];

            addr = &(item->ifr_addr);

            /* Get the IP address */
            if (ioctl (sck, SIOCGIFADDR, item) < 0)
            {
                perror ("ioctl(OSIOCGIFADDR)");
            }

            /* skip the loopback interface */
            if (!memcmp (item->ifr_name, "lo", sizeof ("lo")))
            {
                continue;
            }
            if (!memcmp (item->ifr_name, "vlan", sizeof ("vlan")))
            {
                continue;
            }
            i4IfCount++;
            i4RetVal = setsockopt (i4CwpCtrlSock4Id,
                                   SOL_SOCKET, SO_BINDTODEVICE,
                                   item->ifr_name, sizeof (item->ifr_name));
            if (i4RetVal < 0)
            {
                perror ("capwapCtrlUdpTxPacket: Unable to bind ");
                i4ErrCount++;
                continue;
            }
            i4RetVal = sendto (i4CwpCtrlSock4Id, pu1TxBuf, (UINT4) u4PktLen, 0,
                               (struct sockaddr *) &PeerAddr,
                               sizeof (struct sockaddr_in));
            if (i4RetVal < 0)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "capwapCtrlUdpTxPacket:Failed to Transmit packet : \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "capwapCtrlUdpTxPacket:Failed to Transmit packet : \r\n"));
                i4ErrCount++;
                continue;
            }
            pVaddr_in = addr;
            addr_in = pVaddr_in;

            if (inet_ntop (AF_INET, &(addr_in->sin_addr),
                           ip, sizeof (ip)) == NULL)
            {
                perror ("inet_ntop");
                continue;
            }
        }
        if (i4ErrCount == i4IfCount)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Broadcast packet sent failed \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Broadcast packet sent failed \r\n"));
            /* Reset the BROADCAST option in the socket  */
            i4Opn = OSIX_FALSE;

            if (setsockopt (i4CwpCtrlSock4Id, SOL_SOCKET, SO_BROADCAST,
                            (INT4 *) &i4Opn, sizeof (i4Opn)) < 0)
            {
                close (sck);
                perror ("capwapCtrlUdpTxPacket: Unable to reset"
                        "SO_BROADCAST options ");
                return OSIX_FAILURE;
            }
            i4RetVal = setsockopt (i4CwpCtrlSock4Id, SOL_SOCKET,
                                   SO_BINDTODEVICE, "", 0);
            if (i4RetVal < 0)
            {
                close (sck);
                perror ("capwapCtrlUdpTxPacket: Unable to remove bind ");
                return OSIX_FAILURE;
            }
            close (sck);
            return OSIX_FAILURE;
        }
        close (sck);
#else
        if (setsockopt (i4CwpCtrlSock4Id, IPPROTO_IP, IP_PKTINFO,
                        (INT4 *) &i4Opn, sizeof (i4Opn)) < 0)
        {
            perror ("capwapCtrlUdpTxPacket: Unable to reset "
                    "Setsockopt(IPPROTO_IP) options ");
            return OSIX_FAILURE;
        }
        MEMSET (&PktInfo, 0, sizeof (struct msghdr));
        MEMSET (au1Cmsg, 0, sizeof (au1Cmsg));

        PktInfo.msg_name = (void *) &PeerAddr;
        PktInfo.msg_namelen = sizeof (struct sockaddr_in);

        PktInfo.msg_control = (VOID *) au1Cmsg;
        PktInfo.msg_controllen = sizeof (au1Cmsg);

        pIpPktInfo = (struct in_pktinfo *) (VOID *)
            CMSG_DATA (CMSG_FIRSTHDR (&PktInfo));
        pIpPktInfo->ipi_ifindex = 0;
        pIpPktInfo->ipi_addr.s_addr = (u4DestAddr);
        pIpPktInfo->ipi_spec_dst.s_addr = 0;

        if (sendmsg (i4CwpCtrlSock4Id, &PktInfo, 0) < 0)
        {
            perror ("sendmsg \r\n");
        }
        PRINTF ("\n CAPWAP - sock\n");
        i4RetVal = sendto (i4CwpCtrlSock4Id, pu1TxBuf, (UINT4) u4PktLen, 0,
                           (struct sockaddr *) &PeerAddr,
                           sizeof (struct sockaddr_in));
        if (i4RetVal < 0)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "capwapCtrlUdpTxPacket:Failed to Transmit packet : \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "capwapCtrlUdpTxPacket:Failed to Transmit packet : \r\n"));
            i4ErrCount++;
            return OSIX_SUCCESS;
        }
        return OSIX_SUCCESS;

#endif
#if CAP_WANTED
        if (getifaddrs (&ifap) == SOCKET_ID)
        {
            close (sck);
            perror ("capwapCtrlUdpTxPacket: getifaddrs failed ");
            return OSIX_FAILURE;
        }
        for (ifa = ifap; ifa; ifa = ifa->ifa_next)
        {
            CAPWAP_TRC1 (CAPWAP_UTIL_TRC,
                         "getifaddrs: Interface Name is %s \r\n",
                         ifa->ifa_name);
            SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4CapwapSysLogId,
                          "getifaddrs: Interface Name is %s \r\n",
                          ifa->ifa_name));

            /* skip the loopback interface */
            if (!memcmp (ifa->ifa_name, "lo", sizeof ("lo")))
            {
                continue;
            }
            i4IfCount++;
            i4RetVal = setsockopt (i4CwpCtrlSock4Id, SOL_SOCKET,
                                   SO_BINDTODEVICE, ifa->ifa_name,
                                   sizeof (ifa->ifa_name));
            if (i4RetVal < 0)
            {
                perror ("capwapCtrlUdpTxPacket: Unable to bind ");
                i4ErrCount++;
                continue;
            }
            i4RetVal = sendto (i4CwpCtrlSock4Id, pu1TxBuf, (UINT4) u4PktLen, 0,
                               (struct sockaddr *) &PeerAddr,
                               sizeof (struct sockaddr_in));
            if (i4RetVal < 0)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "capwapCtrlUdpTxPacket:Failed to Transmit packet : \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "capwapCtrlUdpTxPacket:Failed to Transmit packet : \r\n"));
                i4ErrCount++;
                continue;
            }
        }
        if (i4ErrCount == i4IfCount)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Broadcast packet sent failed \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Broadcast packet sent failed \r\n"));
            freeifaddrs (ifap);
            /* Reset the BROADCAST option in the socket  */
            i4Opn = OSIX_FALSE;

            if (setsockopt (i4CwpCtrlSock4Id, SOL_SOCKET, SO_BROADCAST,
                            (INT4 *) &i4Opn, sizeof (i4Opn)) < 0)
            {
                close (sck);
                perror ("capwapCtrlUdpTxPacket: Unable to re-set"
                        "SO_BROADCAST options ");
                return OSIX_FAILURE;
            }
            i4RetVal = setsockopt (i4CwpCtrlSock4Id, SOL_SOCKET,
                                   SO_BINDTODEVICE, "", 0);
            if (i4RetVal < 0)
            {
                close (sck);
                perror ("capwapCtrlUdpTxPacket: Unable to remove bind ");
                return OSIX_FAILURE;
            }
            close (sck);
            return OSIX_FAILURE;
        }
        freeifaddrs (ifap);
        close (sck);
#endif
#endif
    }
    else
    {
        /* Set the DSCP Code */
        i4DscpCode = CAPWAP_CTRL_PKT_DSCP_CODE;
        if (setsockopt (i4CwpCtrlSock4Id, IPPROTO_IP, IP_TOS,
                        &i4DscpCode, sizeof (INT4)) < 0)
        {
            perror ("CapwapCtrlUdpTxPacket: Unable to set TOS-DSCP options");
            return (OSIX_FAILURE);
        }

#ifdef WTP_WANTED
#ifndef LNXIP4_WANTED
        MEMSET (&PktInfo, 0, sizeof (struct msghdr));
        MEMSET (au1Cmsg, 0, sizeof (au1Cmsg));

        PktInfo.msg_name = (void *) &PeerAddr;
        PktInfo.msg_namelen = sizeof (struct sockaddr_in);

        PktInfo.msg_control = (VOID *) au1Cmsg;
        PktInfo.msg_controllen = sizeof (au1Cmsg);

        pIpPktInfo = (struct in_pktinfo *) (VOID *)
            CMSG_DATA (CMSG_FIRSTHDR (&PktInfo));
        pIpPktInfo->ipi_ifindex = 0;
        pIpPktInfo->ipi_addr.s_addr = (u4DestAddr);
        pIpPktInfo->ipi_spec_dst.s_addr = 0;

        if (sendmsg (i4CwpCtrlSock4Id, &PktInfo, 0) < 0)
        {
            perror ("sendmsg \r\n");
        }
#endif
#endif
        /*This is a work around for avoiding the crash and error messages seen
           while sending bulk config update request and responses */
        if (getsockopt
            (i4CwpCtrlSock4Id, SOL_SOCKET, SO_RCVBUF, &result, &resultlen) < 0)
        {
            perror ("Error in getsockopt");
        }
        result = 140000;
        if (setsockopt
            (i4CwpCtrlSock4Id, SOL_SOCKET, SO_RCVBUF, &result,
             sizeof (result)) != 0)
        {
            printf ("\nSetSockOpt Failed");
        }
        if (getsockopt
            (i4CwpCtrlSock4Id, SOL_SOCKET, SO_RCVBUF, &result, &resultlen) < 0)
        {
            perror ("Error in getsockopt");
        }

        i4RetVal = sendto (i4CwpCtrlSock4Id, pu1TxBuf, (INT4) u4PktLen, 0,
                           (struct sockaddr *) &PeerAddr,
                           sizeof (struct sockaddr_in));
        if (i4RetVal < 0)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "capwapCtrlUdpTxPacket:Failed to Transmit packet : \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "capwapCtrlUdpTxPacket:Failed to Transmit packet : \r\n"));
            return OSIX_SUCCESS;
        }
    }
#ifdef WTP_WANTED
    if ((u4DestAddr == CAPWAP_BROADCAST_ADDR) ||
        (u4DestAddr == CAPWAP_MULTICAST_ADDR))
    {
        /* Reset the BROADCAST option in the socket  */
        i4OpnVal = OSIX_FALSE;

        if (setsockopt (i4CwpCtrlSock4Id, SOL_SOCKET, SO_BROADCAST,
                        (INT4 *) &i4OpnVal, sizeof (i4OpnVal)) < 0)
        {
            perror ("capwapCtrlUdpTxPacket: Unable to re-set"
                    "SO_BROADCAST options ");
            return OSIX_FAILURE;
        }
        i4RetVal = setsockopt (i4CwpCtrlSock4Id, SOL_SOCKET, SO_BINDTODEVICE,
                               "", 0);
        if (i4RetVal < 0)
        {
            perror ("capwapCtrlUdpTxPacket: Unable to remove bind ");
            return OSIX_FAILURE;
        }
    }
#endif
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : CapwapDataUdpTxPacket                                      *
 *                                                                           *
 * Description  : This function used to  transmits the given                 *
 *                CAPWAP Data packet over UDP socket                         *
 *                                                                           *
 * Input        : u4DestPort  - Destination UDP port                         *
 *                u4DestAddr  - Destination IP Address                       *
 *                pBuf        - Pointer to the linear buffer                 *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 ******************************************************************************/
INT4
CapwapDataUdpTxPacket (tCRU_BUF_CHAIN_HEADER * pBuf,
                       UINT4 u4DestAddr,
                       UINT4 u4DestPort, UINT4 u4PktLen, UINT1 u1DscpVal)
{
    struct sockaddr_in  PeerAddr;
    UINT1              *pu1TxBuf = NULL;
    UINT1              *pu1Frame = NULL;
    INT4                i4DscpCode;
    INT4                i4RetVal;
    UINT2               u2PacketLen;
#ifdef KERNEL_CAPWAP_WANTED
#ifdef WTP_WANTED
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    struct sockaddr_in  Addr;
    socklen_t           len = sizeof (Addr);
#endif
#endif

    CAPWAP_FN_ENTRY ();
    /* MEMSET (&PeerAddr, 0, sizeof (struct sockaddr_in)); */

    pu1Frame = UtlShMemAllocCapwapPktBuf ();
    if (pu1Frame == NULL)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "CapwapDataUdpTxPacket:"
                    "UtlShMemAllocCapwapPktBuf memory allocation failed\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "CapwapDataUdpTxPacket:"
                      "UtlShMemAllocCapwapPktBuf memory allocation failed\n"));
        return OSIX_FAILURE;
    }
    u2PacketLen = (UINT2) CAPWAP_GET_BUF_LEN (pBuf);
    pu1TxBuf = CAPWAP_BUF_IF_LINEAR (pBuf, 0, u2PacketLen);
    if (pu1TxBuf == NULL)
    {
        pu1TxBuf = pu1Frame;
        CAPWAP_COPY_FROM_BUF (pBuf, pu1TxBuf, 0, u2PacketLen);
    }

    PeerAddr.sin_family = AF_INET;
    PeerAddr.sin_addr.s_addr = OSIX_HTONL (u4DestAddr);
    PeerAddr.sin_port = (UINT2) OSIX_HTONS (u4DestPort);
    if (gu4CapwapDebugMask & CAPWAP_PKTDUMP_TRC)
    {
        DbgDumpCapwapPkt (pBuf, CAPWAP_PKT_FLOW_OUT);
    }

    /* Set the DSCP Code */
    i4DscpCode = (INT4) u1DscpVal;
    if (setsockopt (i4CwpDataSock4Id, IPPROTO_IP, IP_TOS,
                    &i4DscpCode, sizeof (INT4)) < 0)
    {
        perror ("CapwapDataUdpTxPacket: Unable to set TOS-DSCP options");
        UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1Frame);
        return (OSIX_FAILURE);
    }
    /* Send the packet in the socket */
    i4RetVal = sendto (i4CwpDataSock4Id, pu1TxBuf, (INT4) u4PktLen, 0,
                       (struct sockaddr *) &PeerAddr,
                       sizeof (struct sockaddr_in));
    if (i4RetVal < 0)
    {
        perror ("CapwapDataUdpTxPacket:Failed to Transmit packet : ");
        UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1Frame);
        return OSIX_SUCCESS;
    }

    UtlShMemFreeCapwapPktBuf ((UINT1 *) pu1Frame);
#ifdef KERNEL_CAPWAP_WANTED
#ifdef WTP_WANTED
    if (gu1Flag == 0)
    {
        WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
            MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

        getsockname (i4CwpDataSock4Id, (struct sockaddr *) &Addr, &len);
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpDataPort = OSIX_TRUE;
        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = 0;
        pWssIfCapwapDB->CapwapGetDB.u4WtpDataPort = OSIX_NTOHS (Addr.sin_port);

        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_SET_DB, pWssIfCapwapDB) ==
            OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Unable to set"
                        "session details to DB\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId, "Unable to set"
                          "session details to DB\r\n"));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        gu1Flag = 1;
    }
#endif
#endif
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : CapwapCheckRelinquishCounters                              *
 *                                                                           *
 * Description  : Check relinquish counters and take action                  *
 *                                                                           *
 * Input        : NONE                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
VOID
CapwapCheckRelinquishCounters (VOID)
{
#ifdef WTP_WANTED
#ifdef DEBUG_WANTED
    UINT4               u4Event;
    gu4CPURelinquishCounter++;
    {
        if (gCapwapState != CAPWAP_IMAGEDATA)
        {
            if (gu4CPURelinquishCounter == CAPWAP_RELINQUISH_MSG_COUNT)
            {
                OsixEvtRecv (gRecvTaskGlobals.capwapRecvTaskId,
                             CAP_RECV_RELINQUISH_EVENT, OSIX_WAIT, &u4Event);
                gu4CPURelinquishCounter = 0;
            }
        }
    }
#endif
#endif
    return;
}

/*****************************************************************************
 * Function     : CapwapCheckServRelinquishCounters                          *
 *                                                                           *
 * Description  : Check relinquish counters and take action                  *
 *                                                                           *
 * Input        : NONE                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
VOID
CapwapRecvCPURelinquishTmrExp (VOID *pArg)
{
    UNUSED_PARAM (pArg);
#ifdef DEBUG_WANTED
    gu4CPURelinquishCounter = 0;
    if (OsixEvtSend (gRecvTaskGlobals.capwapRecvTaskId,
                     CAP_RECV_RELINQUISH_EVENT) != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Timer event send failure\n");
    }

    if (CapwapTmrStart (NULL, CAPWAP_CPU_RECV_RELINQUISH_TMR,
                        CAPWAP_RELINQUISH_TIMER_VAL) != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, " Timer Start FAILED.\r\n");
    }
#endif
    return;
}

#ifdef LNXIP4_WANTED
/*****************************************************************************
 * Function     : CapwapCheckServRelinquishCounters                          *
 *                                                                           *
 * Description  : Check relinquish counters and take action                  *
 *                                                                           *
 * Input        : NONE                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/

INT4
CapwapJoinMulticastGroup (INT4 i4CtrlSock4Id)
{
    struct ip_mreq      MulticastGroup;
    struct sockaddr_in *saddr;
    struct ifreq        ifreq;
    UINT2               u4Index;
    UINT1              *pDevName = NULL;

    MEMSET (&MulticastGroup, 0, sizeof (struct ip_mreq));

    MulticastGroup.imr_multiaddr.s_addr = OSIX_HTONL (CAPWAP_MULTICAST_ADDR);

    for (u4Index = 1; u4Index <= SYS_DEF_MAX_PHYSICAL_INTERFACES; u4Index++)
    {
        if (CfaValidateIfMainTableIndex (u4Index) == OSIX_SUCCESS)
        {
            pDevName = CfaGddGetLnxIntfnameForPort ((UINT2) u4Index);
            if (pDevName != NULL)
            {
                MEMCPY (ifreq.ifr_name, pDevName, IFNAMSIZ);
                if (ioctl (i4CtrlSock4Id, SIOCGIFADDR, &ifreq) == 0)
                {
                    saddr = (struct sockaddr_in *) (VOID *) &ifreq.ifr_addr;
                    MulticastGroup.imr_interface.s_addr =
                        saddr->sin_addr.s_addr;

                    if (setsockopt
                        (i4CtrlSock4Id, IPPROTO_IP, IP_ADD_MEMBERSHIP,
                         (INT1 *) &MulticastGroup, sizeof (MulticastGroup)) < 0)
                    {
                        perror ("Adding multicast group error");
                        return (OSIX_FAILURE);

                    }

                }
            }
        }

    }
    return (OSIX_SUCCESS);
}
#endif
#endif /* _CAPRMAIN_C */
/*-----------------------------------------------------------------------*/
/*                       End of the file  caprmain.c                     */
/*-----------------------------------------------------------------------*/
