/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 * $Id: capwapwlcimage.c,v 1.4 2017/11/24 10:37:03 siva Exp $      *
 * Description: This file contains the CAPWAP Image transfer  routines
 *
 *******************************************************************/
#ifndef __CAPWAP_WLCIMAGE_C__
#define __CAPWAP_WLCIMAGE_C__
#include "capwapinc.h"
#include "wssifradiodb.h"
#include "arMD5_api.h"

INT4                gi4ReadCount = 0;
UINT1               gau1HashVal[HASH_DATA_SIZE] = { '\0' };

extern eState       gCapwapState;
extern INT4
        WlchdlrFreeTransmittedPacket (tRemoteSessionManager * pSessEntry);

/**********************************************************************
*  Function Name   : CapwapProcessImageDataReq                         */
/*  Description     : The function processes the received CAPWAP        */
/*                    Image data request.                               */
/*  Input(s)        : pBuf, pImageDataReqMsg, pSessEntry                */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/

INT4
CapwapProcessImageDataReq (tSegment * pBuf,
                           tCapwapControlPacket * pImageDataReqMsg,
                           tRemoteSessionManager * pSessEntry)
{

    INT4                i4Status = OSIX_FAILURE;
    INT4                i4RetValue = OSIX_FAILURE;
    UINT4               u4MsgLen = 0;
    tImageDataRsp       ImageDataRsp;
    tImageDataReq       ImageDataReq;
    tSegment           *pTxBuf = NULL;
    tSegment           *pTxBuf1 = NULL;
    UINT1              *pu1TxBuf = NULL;
    UINT1              *pu1TxBuf1 = NULL;
    UINT1              *pRcvdBuf = NULL;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT1               u1IsNotLinear = OSIX_FALSE;
    UINT2               u2PacketLen;

    CAPWAP_FN_ENTRY ();

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    if (pSessEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Session Entry is NULL, Return Failure \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Session Entry is NULL, Return Failure \r\n"));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    if (CapwapTmrStop (pSessEntry, CAPWAP_WAITJOIN_TMR) == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE, "Failed to stop the Wait Join Timer \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to stop the Wait Join Timer \r\n"));
        /* clear the packet buffer pointer */
        WlchdlrFreeTransmittedPacket (pSessEntry);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    MEMSET (&ImageDataRsp, 0, sizeof (tImageDataRsp));
    MEMSET (&ImageDataReq, 0, sizeof (tImageDataReq));

    u2PacketLen = (UINT2) CAPWAP_GET_BUF_LEN (pBuf);
    pRcvdBuf = CAPWAP_BUF_IF_LINEAR (pBuf, 0, u2PacketLen);
    if (pRcvdBuf == NULL)
    {
        pRcvdBuf = (UINT1 *) MemAllocMemBlk (CAPWAP_PKTBUF_POOLID);
        CAPWAP_COPY_FROM_BUF (pBuf, pRcvdBuf, 0, u2PacketLen);
        u1IsNotLinear = OSIX_TRUE;
    }

    if (CapwapValidateIpAddress (pSessEntry->remoteIpAddr.u4_addr[0])
        == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "WTP is not suppported. Entry is in the black list \n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "WTP is not suppported. Entry is in the black list \n"));
        if (u1IsNotLinear == OSIX_TRUE)
        {
            MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pRcvdBuf);
        }
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;    /* return proper error code */
    }

    /* Validate the Image Data request message. 
     * if it is success construct Image Data Response */
    if ((CapwapValidateImageDataReqMsgElems (pRcvdBuf, pImageDataReqMsg,
                                             (tImageDataReq *) pSessEntry) ==
         OSIX_SUCCESS))
    {
        if (u1IsNotLinear == OSIX_TRUE)
        {
            MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pRcvdBuf);
        }

        CAPWAP_TRC (CAPWAP_FSM_TRC,
                    "Entered CAPWAP IMAGE Data Request State \r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                      "Entered CAPWAP IMAGE Data Request State \r\n"));

        if (pSessEntry->eCurrentState != CAPWAP_RUN)
        {
            pSessEntry->eCurrentState = CAPWAP_IMAGEDATA;
            gCapwapState = CAPWAP_IMAGEDATA;
        }

        /* Get the values to send out the Image Data Response */
        if (CapwapGetWLCImageDataRspMsgElements (&ImageDataRsp,
                                                 &u4MsgLen,
                                                 pSessEntry) == OSIX_SUCCESS)
        {
            /* update the seq number */
            ImageDataRsp.u1SeqNum = pImageDataReqMsg->capwapCtrlHdr.u1SeqNum;
            ImageDataRsp.u2CapwapMsgElemenLen = (UINT2) (u4MsgLen + 3);

            pTxBuf = CAPWAP_ALLOCATE_CRU_BUF (CAPWAP_MAX_PKT_LEN, 0);
            if (pTxBuf == NULL)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "capwapProcessImageDataRequest:Memory Allocation Failed \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "capwapProcessImageDataRequest:Memory Allocation Failed \r\n"));
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
            pu1TxBuf = CAPWAP_BUF_IF_LINEAR (pTxBuf, 0, CAPWAP_MAX_PKT_LEN);
            if (pu1TxBuf == NULL)
            {
                pu1TxBuf = (UINT1 *) MemAllocMemBlk (CAPWAP_PKTBUF_POOLID);
                if (pu1TxBuf == NULL)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "capwapProcessImageDataRequest:Memory Allocation Failed \r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "capwapProcessImageDataRequest:Memory Allocation Failed \r\n"));
                    CAPWAP_RELEASE_CRU_BUF (pTxBuf);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    return OSIX_FAILURE;
                }
                CAPWAP_COPY_FROM_BUF (pTxBuf, pu1TxBuf, 0, CAPWAP_MAX_PKT_LEN);
                u1IsNotLinear = OSIX_TRUE;
            }
            if ((CapwapAssembleWLCImageDataRsp
                 (ImageDataRsp.u2CapwapMsgElemenLen, &ImageDataRsp,
                  pu1TxBuf)) == OSIX_FAILURE)
            {
                if (u1IsNotLinear == OSIX_TRUE)
                {
                    MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pu1TxBuf);
                }
                CAPWAP_RELEASE_CRU_BUF (pTxBuf);
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
            if (u1IsNotLinear == OSIX_TRUE)
            {
                /* If the CRU buffer memory is not linear */
                CAPWAP_COPY_TO_BUF (pTxBuf, pu1TxBuf, 0, CAPWAP_MAX_PKT_LEN);
                MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pu1TxBuf);
            }
            i4Status = CapwapTxMessage (pTxBuf,
                                        pSessEntry,
                                        (UINT4) (ImageDataRsp.
                                                 u2CapwapMsgElemenLen + 13),
                                        WSS_CAPWAP_802_11_CTRL_PKT);
            if (i4Status == OSIX_SUCCESS)
            {
                /* Update the last transmitted sequence number */
                pSessEntry->lastTransmitedSeqNum++;
                /* store the packet buffer pointer in
                 * the remote session DB copy the pointer */
                pSessEntry->lastTransmittedPkt = pTxBuf;
                pSessEntry->lastTransmittedPktLen =
                    (UINT4) (ImageDataRsp.u2CapwapMsgElemenLen + 13);
                pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                pWssIfCapwapDB->CapwapIsGetAllDB.bImageTransfer = OSIX_TRUE;
                pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
                    pSessEntry->u2IntProfileId;
                pWssIfCapwapDB->CapwapGetDB.u1ImageTransferFlag = OSIX_TRUE;
                if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_SET_DB, pWssIfCapwapDB)
                    != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "Failed to SET the value in CAPWAP DB \r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Failed to SET the value in CAPWAP DB \r\n"));
                }

            }
            else
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Failed to Transmit the packet \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to Transmit the packet \r\n"));
                CAPWAP_RELEASE_CRU_BUF (pTxBuf);
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
            CAPWAP_RELEASE_CRU_BUF (pTxBuf);
            pSessEntry->lastTransmittedPkt = NULL;
        }
        else
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Unable to get optional elements data \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Unable to get optional elements data \r\n"));
            CAPWAP_TRC (CAPWAP_FSM_TRC, "Enter the CAPWAP DTLS TD State \r\n");
            SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                          "Enter the CAPWAP DTLS TD State \r\n"));
            pSessEntry->eCurrentState = CAPWAP_DTLSTD;
            if (CapwapShutDownDTLS (pSessEntry) == OSIX_FAILURE)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Failed to Disconnect the Session \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to Disconnect the Session \r\n"));
            }
            /* Cleare the Entry in Remote session Module */
            pWssIfCapwapDB->u4DestIp = pSessEntry->remoteIpAddr.u4_addr[0];
            pWssIfCapwapDB->u4DestPort = pSessEntry->u4RemoteCtrlPort;
            i4RetValue =
                WssIfProcessCapwapDBMsg (WSS_CAPWAP_DELETE_RSM, pWssIfCapwapDB);
            if (i4RetValue == OSIX_SUCCESS || i4RetValue == OSIX_FAILURE)
            {
                /* Do nothing */
                /* Intentionaly done for coverity fixes */
            }
            /* For both the success and failure cases same code has to be executed */
            if (u1IsNotLinear == OSIX_TRUE)
            {
                MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pRcvdBuf);
            }

            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
        u4MsgLen = 0;
        /* After sending Image Data Response, Send Image Data Request to AP */
        if (CapwapGetWLCImageDataRequestMsgElements (&ImageDataReq, &u4MsgLen,
                                                     pSessEntry) ==
            OSIX_SUCCESS)
        {
            /* update the seq number */
            ImageDataReq.u1SeqNum =
                (UINT1) (pSessEntry->lastTransmitedSeqNum + 1);
            ImageDataReq.u2CapwapMsgElemenLen = (UINT2) (u4MsgLen + 3);
            pTxBuf1 = CAPWAP_ALLOCATE_CRU_BUF (CAPWAP_MAX_PKT_LEN, 0);
            if (pTxBuf1 == NULL)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "capwapProcessImageDataRequest:Memory Allocation Failed \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "capwapProcessImageDataRequest:Memory Allocation Failed \r\n"));
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
            pu1TxBuf1 = CAPWAP_BUF_IF_LINEAR (pTxBuf1, 0, CAPWAP_MAX_PKT_LEN);
            if (pu1TxBuf1 == NULL)
            {
                pu1TxBuf1 = (UINT1 *) MemAllocMemBlk (CAPWAP_PKTBUF_POOLID);
                if (pu1TxBuf1 == NULL)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "capwapProcessImageDataRequest:Memory Allocation Failed \r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "capwapProcessImageDataRequest:Memory Allocation Failed \r\n"));
                    CAPWAP_RELEASE_CRU_BUF (pTxBuf1);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    return OSIX_FAILURE;
                }
                u1IsNotLinear = OSIX_TRUE;
            }

            if ((CapwapAssembleImageDataReq (ImageDataReq.u2CapwapMsgElemenLen,
                                             &ImageDataReq,
                                             pu1TxBuf1)) == OSIX_FAILURE)
            {
                if (u1IsNotLinear == OSIX_TRUE)
                {
                    MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pu1TxBuf1);
                }
                CAPWAP_RELEASE_CRU_BUF (pTxBuf1);
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }

            if (u1IsNotLinear == OSIX_TRUE)
            {
                /* If the CRU buffer memory is not linear */
                CAPWAP_COPY_TO_BUF (pTxBuf1, pu1TxBuf1, 0, ImageDataReq.
                                    u2CapwapMsgElemenLen + CAPWAP_MSG_ELE_LEN);
                MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pu1TxBuf1);
            }
            i4Status = CapwapTxMessage (pTxBuf1,
                                        pSessEntry,
                                        (UINT4) (ImageDataReq.
                                                 u2CapwapMsgElemenLen +
                                                 CAPWAP_MSG_ELE_LEN),
                                        WSS_CAPWAP_802_11_CTRL_PKT);
            if (i4Status == OSIX_SUCCESS)
            {
                /* Update the last transmitted sequence number */
                pSessEntry->lastProcesseddSeqNum = ImageDataReq.u1SeqNum;
                /* store the packet buffer pointer in
                 * the remote session DB copy the pointer */
                pSessEntry->lastTransmittedPkt = pTxBuf1;
                pSessEntry->lastTransmittedPktLen =
                    (UINT4) (ImageDataReq.u2CapwapMsgElemenLen +
                             CAPWAP_CMN_HDR_LEN);

                CapwapTmrStart (pSessEntry, CAPWAP_RETRANSMIT_INTERVAL_TMR,
                                MAX_RETRANSMIT_INTERVAL_TIMEOUT);

                pSessEntry->lastTransmitedSeqNum++;
                pSessEntry->u1RetransmitCount = 0;
            }
            else
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Failed to Transmit the packet \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to Transmit the packet \r\n"));
                CAPWAP_RELEASE_CRU_BUF (pTxBuf1);
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
        }
        else
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Unable to get optional elements data \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Unable to get optional elements data \r\n"));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
    }
    else
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to validate the received Image Data Req\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to validate the received Image Data Req\n"));
        CAPWAP_TRC (CAPWAP_FSM_TRC, "Enter the CAPWAP DTLS TD State \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Enter the CAPWAP DTLS TD State \r\n"));
        pSessEntry->eCurrentState = CAPWAP_DTLSTD;
        if (CapwapShutDownDTLS (pSessEntry) == OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to Disconnect the Session \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to Disconnect the Session \r\n"));
        }
        /* Cleare the Entry in Remote session Module */
        pWssIfCapwapDB->u4DestIp = pSessEntry->remoteIpAddr.u4_addr[0];
        pWssIfCapwapDB->u4DestPort = pSessEntry->u4RemoteCtrlPort;
        WssIfProcessCapwapDBMsg (WSS_CAPWAP_DELETE_RSM, pWssIfCapwapDB);
        if (u1IsNotLinear == OSIX_TRUE)
        {
            MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pRcvdBuf);
        }
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapProcessImageDataRsp                         */
/*  Description     : The function validates the functional             */
/*                    characteristics of the received CAPWAP            */
/*                    Image Data response                               */
/*  Input(s)        : pBuf, pImageDataRsp, pSessEntry                   */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/

INT4
CapwapProcessImageDataRsp (tSegment * pBuf,
                           tCapwapControlPacket * pImageDataRsp,
                           tRemoteSessionManager * pSessEntry)
{
    UINT1              *pRcvBuf = NULL, *pu1TxBuf = NULL;
    tSegment           *pTxBuf = NULL;
    INT4                i4Status = OSIX_SUCCESS;
    tImageDataReq       ImageDataReq;
    UINT1               u1IsNotLinear = OSIX_FALSE;
    UINT2               u2PacketLen;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT4               u4MsgLen = 0;

    CAPWAP_FN_ENTRY ();

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&ImageDataReq, 0, sizeof (tImageDataReq));

    if (pSessEntry == NULL
        || ((pSessEntry->eCurrentState != CAPWAP_IMAGEDATA)
            && (pSessEntry->eCurrentState != CAPWAP_RUN)))
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Remote session Module is NULL, return Failure \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Remote session Module is NULL, return Failure \r\n"));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    if (CapwapTmrStop (pSessEntry, CAPWAP_RETRANSMIT_INTERVAL_TMR) ==
        OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_MGMT_TRC,
                    "ImageDataRsp - Stopped the retransmit timer \r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                      "ImageDataRsp - Stopped the retransmit timer \r\n"));
        /* clear the packet buffer pointer */
        WlchdlrFreeTransmittedPacket (pSessEntry);
    }

    u2PacketLen = (UINT2) CAPWAP_GET_BUF_LEN (pBuf);
    pRcvBuf = CAPWAP_BUF_IF_LINEAR (pBuf, 0, u2PacketLen);
    if (pRcvBuf == NULL)
    {
        pRcvBuf = (UINT1 *) MemAllocMemBlk (CAPWAP_PKTBUF_POOLID);
        CAPWAP_COPY_FROM_BUF (pBuf, pRcvBuf, 0, u2PacketLen);
        u1IsNotLinear = OSIX_TRUE;
    }

    if (CapwapValidateIpAddress (pSessEntry->remoteIpAddr.u4_addr[0])
        == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "WTP is not suppported. Entry is in the black list \n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "WTP is not suppported. Entry is in the black list \n"));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    if (CapwapValidateImageDataRespMsgElems (pRcvBuf, pImageDataRsp, pSessEntry)
        == OSIX_FAILURE)
    {
        if (pSessEntry->eCurrentState == CAPWAP_RUN)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to validate Image Data Response Message Elements \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to validate Image Data Response Message Elements \r\n"));
            if (u1IsNotLinear == OSIX_TRUE)
            {
                MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pRcvBuf);
            }
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to validate Image Data Response Message Elements \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to validate Image Data Response Message Elements \r\n"));
        if (u1IsNotLinear == OSIX_TRUE)
        {
            MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pRcvBuf);
        }
        CAPWAP_TRC (CAPWAP_FSM_TRC, "Enter the CAPWAP DTLS TD State \r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                      "Enter the CAPWAP DTLS TD State \r\n"));
        pSessEntry->eCurrentState = (UINT4) CAPWAP_DTLSTD;
        if (CapwapShutDownDTLS (pSessEntry) == OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to Disconnect the Session \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to Disconnect the Session \r\n"));
        }
        pWssIfCapwapDB->u4DestIp = pSessEntry->remoteIpAddr.u4_addr[0];
        pWssIfCapwapDB->u4DestPort = pSessEntry->u4RemoteCtrlPort;
        WssIfProcessCapwapDBMsg (WSS_CAPWAP_DELETE_RSM, pWssIfCapwapDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    if (u1IsNotLinear == OSIX_TRUE)
    {
        MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pRcvBuf);
    }

    if (pSessEntry->u1EndofFileReached)
    {
        CAPWAP_TRC (CAPWAP_MGMT_TRC,
                    "WTP Image Upload completed. Changing state to RUN \r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                      "WTP Image Upload completed. Changing state to RUN \r\n"));
        if (u1IsNotLinear == OSIX_TRUE)
        {
            MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pRcvBuf);
        }
        /*Image transfer completed. Change the state to RUN */
        pSessEntry->eCurrentState = CAPWAP_RUN;
        pSessEntry->u1EndofFileReached = OSIX_FALSE;
    }
    else
    {
        /* After sending Image Data Response, Send Image Data Request to AP */
        if (CapwapGetWLCImageDataRequestMsgElements (&ImageDataReq, &u4MsgLen,
                                                     pSessEntry) ==
            OSIX_SUCCESS)
        {
            /* update the seq number */
            ImageDataReq.u1SeqNum =
                (UINT1) (pSessEntry->lastTransmitedSeqNum + 1);
            ImageDataReq.u2CapwapMsgElemenLen = (UINT2) (u4MsgLen + 3);
            pTxBuf = CAPWAP_ALLOCATE_CRU_BUF (CAPWAP_MAX_PKT_LEN, 0);
            if (pTxBuf == NULL)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "capwapProcessImageDataRequest:Memory Allocation Failed \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "capwapProcessImageDataRequest:Memory Allocation Failed \r\n"));
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
            pu1TxBuf = CAPWAP_BUF_IF_LINEAR (pTxBuf, 0, CAPWAP_MAX_PKT_LEN);
            if (pu1TxBuf == NULL)
            {
                pu1TxBuf = (UINT1 *) MemAllocMemBlk (CAPWAP_PKTBUF_POOLID);
                if (pu1TxBuf == NULL)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "capwapProcessImageDataRequest:Memory Allocation Failed \r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "capwapProcessImageDataRequest:Memory Allocation Failed \r\n"));
                    CAPWAP_RELEASE_CRU_BUF (pTxBuf);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    return OSIX_FAILURE;
                }
                CAPWAP_COPY_FROM_BUF (pTxBuf, pu1TxBuf, 0, CAPWAP_MAX_PKT_LEN);
                u1IsNotLinear = OSIX_TRUE;
            }

            if ((CapwapAssembleImageDataReq (ImageDataReq.u2CapwapMsgElemenLen,
                                             &ImageDataReq,
                                             pu1TxBuf)) == OSIX_FAILURE)
            {
                if (u1IsNotLinear == OSIX_TRUE)
                {
                    MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pu1TxBuf);
                }
                CAPWAP_RELEASE_CRU_BUF (pTxBuf);
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }

            if (u1IsNotLinear == OSIX_TRUE)
            {
                /* If the CRU buffer memory is not linear */
                CAPWAP_COPY_TO_BUF (pTxBuf, pu1TxBuf, 0, ImageDataReq.
                                    u2CapwapMsgElemenLen + CAPWAP_MSG_ELE_LEN);
                MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pu1TxBuf);
            }
            i4Status = CapwapTxMessage (pTxBuf,
                                        pSessEntry,
                                        (UINT4) (ImageDataReq.
                                                 u2CapwapMsgElemenLen +
                                                 CAPWAP_MSG_ELE_LEN),
                                        WSS_CAPWAP_802_11_CTRL_PKT);
            if (i4Status == OSIX_SUCCESS)
            {
                /* Update the last transmitted sequence number */
                pSessEntry->lastProcesseddSeqNum = ImageDataReq.u1SeqNum;
                /* store the packet buffer pointer in
                 * the remote session DB copy the pointer */
                pSessEntry->lastTransmittedPkt = pTxBuf;
                pSessEntry->lastTransmittedPktLen =
                    (UINT4) (ImageDataReq.u2CapwapMsgElemenLen +
                             CAPWAP_CMN_HDR_LEN);

                CapwapTmrStart (pSessEntry, CAPWAP_RETRANSMIT_INTERVAL_TMR,
                                MAX_RETRANSMIT_INTERVAL_TIMEOUT);

                pSessEntry->lastTransmitedSeqNum++;
                pSessEntry->u1RetransmitCount = 0;
            }
            else
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Failed to Transmit the packet \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to Transmit the packet \r\n"));
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                CAPWAP_RELEASE_CRU_BUF (pTxBuf);
                return OSIX_FAILURE;
            }
        }
    }

    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    CAPWAP_FN_EXIT ();
    return (i4Status);
}

/************************************************************************/
/*  Function Name   : CapwapGetWLCImageDataRspMsgElements               */
/*  Description     : The function process image data response elements */
/*                    in WLC                                            */
/*  Input(s)        : pImageDataRsp, u4MsgLen, pSess                    */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
CapwapGetWLCImageDataRspMsgElements (tImageDataRsp * pImageDataRsp,
                                     UINT4 *u4MsgLen,
                                     tRemoteSessionManager * pSess)
{
    CAPWAP_FN_ENTRY ();

    if (CapwapConstructCpHeader (&pImageDataRsp->capwapHdr) == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "capwapGetJoinRspMsgElements:Failed to construct the capwap Header\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "capwapGetJoinRspMsgElements:Failed to construct the capwap Header\n"));
        return OSIX_FAILURE;
    }

    /* update Optional Messege elements */
    if (CapwapGetWLCResultCode (&pImageDataRsp->ResultCode, u4MsgLen)
        == OSIX_FAILURE)
    {
        CAPWAP_TRC (ALL_FAILURE_TRC,
                    "Failed to get Optional Message Elements \n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to get Optional Message Elements \n"));
        return OSIX_FAILURE;
    }

    if (CapwapGetImageInfo (&pImageDataRsp->ImageInfo, u4MsgLen,
                            pSess) == OSIX_FAILURE)
    {
        CAPWAP_TRC (ALL_FAILURE_TRC,
                    "Failed to get Optional Message Elements \n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to get Optional Message Elements \n"));
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapGetImageInfo                                */
/*  Description     : The function gets image information               */
/*  Input(s)        : pImageInfo, pMsgLen, pSess                        */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
CapwapGetImageInfo (tImageInfo * pImageInfo, UINT4 *pMsgLen,
                    tRemoteSessionManager * pSess)
{
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tCapwapWTPTrapInfo  sWTPTrapInfo;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&sWTPTrapInfo, 0, sizeof (tCapwapWTPTrapInfo));
    pImageInfo->u2MsgEleType = IMAGE_INFORMATION;
    pImageInfo->u2MsgEleLen = IMAGE_INFORMATION_LEN;
    pImageInfo->u4FileSize = (UINT4) (CapwapGetImageSize (pSess));
    if (pImageInfo->u4FileSize == OSIX_FAILURE)
    {
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpMacAddress = OSIX_TRUE;
        pWssIfCapwapDB->CapwapIsGetAllDB.bImageName = OSIX_TRUE;
        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = pSess->u2IntProfileId;
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB) ==
            OSIX_SUCCESS)
        {
            MEMCPY (sWTPTrapInfo.au1capwapBaseNtfWtpId,
                    pWssIfCapwapDB->CapwapGetDB.WtpMacAddress, MAC_ADDR_LEN);
            MEMCPY (sWTPTrapInfo.au1fsCapwapWtpImageName,
                    pWssIfCapwapDB->CapwapGetDB.au1WtpImageIdentifier,
                    pWssIfCapwapDB->CapwapGetDB.au1WtpImageIdLen);
            /* Calling Image Unsupported trap */
            CapwapSnmpifSendTrap (CAPWAP_WTP_IMAGE_NOT_SUPPORTED,
                                  &sWTPTrapInfo);
        }
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (ALL_FAILURE_TRC,
                    "WTP Image File is not present."
                    "Please get the file and restart\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "WTP Image File is not present."
                      "Please get the file and restart\n"));
        return OSIX_FAILURE;
    }
    CapwapGetHash (pSess);
    MEMCPY (pImageInfo->Checksum, gau1HashVal, (HASH_DATA_SIZE / 2));
    *pMsgLen += (UINT4) (pImageInfo->u2MsgEleLen + CAPWAP_MSG_ELEM_TYPE_LEN);
    pImageInfo->isOptional = OSIX_TRUE;
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapGetWLCResultCode                            */
/*  Description     : The function gets WLC result code                 */
/*  Input(s)        : pResCode, pMsgLen                                 */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
CapwapGetWLCResultCode (tResCode * pResCode, UINT4 *pMsgLen)
{
    pResCode->u2MsgEleType = RESULT_CODE;
    pResCode->u2MsgEleLen = RESULT_CODE_LEN;
    pResCode->u4Value = 0;
    *pMsgLen += (UINT4) (pResCode->u2MsgEleLen + CAPWAP_MSG_ELEM_TYPE_LEN);
    pResCode->isOptional = OSIX_TRUE;
    return OSIX_SUCCESS;

}

/************************************************************************/
/*  Function Name   : CapwapGetImageSize                                */
/*  Description     : The function the image size                       */
/*  Input(s)        : pSess                                            */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
CapwapGetImageSize (tRemoteSessionManager * pSess)
{

    FILE               *fp = NULL;
    INT4                i4Size = 0;
    UINT1               au1FlashFile[128];
    UINT1              *pfile;
    UINT1               u1DestLen = 0;

    pfile = au1FlashFile;

    CAPWAP_FN_ENTRY ();

    u1DestLen =
        (UINT1) (STRLEN (pSess->ImageId.data) +
                 STRLEN (IMAGE_DATA_FLASH_CONF_LOC) + 1);
    SNPRINTF ((CHR1 *) au1FlashFile, u1DestLen, "%s%s",
              IMAGE_DATA_FLASH_CONF_LOC, pSess->ImageId.data);

    fp = fopen ((const char *) pfile, "rb");
    if (NULL == fp)
    {
        CAPWAP_TRC (ALL_FAILURE_TRC, "Failed to open the file \n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to open the file \n"));
        return OSIX_FAILURE;
    }

    if (fseek (fp, 0, SEEK_END) > 0)
    {
        CAPWAP_TRC (ALL_FAILURE_TRC,
                    "Failed to move the file pointer to END \n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to move the file pointer to END \n"));
        fclose (fp);
        return OSIX_FAILURE;
    }
    pSess->i4FileSize = ftell (fp);
    if (fseek (fp, 0, SEEK_SET) > 0)
    {
        CAPWAP_TRC (ALL_FAILURE_TRC,
                    "Failed to move the file pointer to START \n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to move the file pointer to START \n"));
        fclose (fp);
        return OSIX_FAILURE;
    }

    pSess->readFd = fp;
    pSess->u1ReadFileOpened = OSIX_TRUE;

    CAPWAP_FN_EXIT ();
    return i4Size;
}

/************************************************************************/
/*  Function Name   : CapwapGetHash                                     */
/*  Description     : The function get hash value of the image          */
/*  Input(s)        : pSess                                             */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
CapwapGetHash (tRemoteSessionManager * pSess)
{
    FILE               *fpHashCalc = NULL;
    INT4                i4Size = 0;
    UINT1               au1FlashFile[IMAGE_FILE_FLASH_SIZE];
    UINT1              *pfile;
    UINT1               u1DestLen = 0;
    unArCryptoHash      Md5Ctx;

    pfile = au1FlashFile;

    CAPWAP_FN_ENTRY ();

    u1DestLen =
        (UINT1) (STRLEN (pSess->ImageId.data) +
                 STRLEN (IMAGE_DATA_FLASH_CONF_LOC) + 1);
    SNPRINTF ((CHR1 *) au1FlashFile, u1DestLen, "%s%s",
              IMAGE_DATA_FLASH_CONF_LOC, pSess->ImageId.data);

    fpHashCalc = fopen ((const char *) pfile, "rb");
    if (NULL == fpHashCalc)
    {
        CAPWAP_TRC (ALL_FAILURE_TRC, "Failed to open the file \n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to open the file \n"));
        return OSIX_FAILURE;
    }
    if (fseek (fpHashCalc, 0, SEEK_END) > 0)
    {
        CAPWAP_TRC (ALL_FAILURE_TRC,
                    "Failed to move the file pointer to END \n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to move the file pointer to END \n"));
        fclose (fpHashCalc);
        return OSIX_FAILURE;
    }
    i4Size = ftell (fpHashCalc);
    if (fseek (fpHashCalc, 0, SEEK_SET) > 0)
    {
        CAPWAP_TRC (ALL_FAILURE_TRC,
                    "Failed to move the file pointer to START \n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to move the file pointer to START \n"));
        fclose (fpHashCalc);
        return OSIX_FAILURE;
    }

    arMD5_start (&Md5Ctx);
    arMD5_update (&Md5Ctx, (UINT1 *) &i4Size, sizeof (i4Size));
    arMD5_finish (&Md5Ctx, gau1HashVal);

    fclose (fpHashCalc);

    CAPWAP_FN_EXIT ();
    return OSIX_FAILURE;
}

/************************************************************************/
/*  Function Name   : CapwapGetImageData                                */
/*  Description     : The function gets image data                      */
/*  Input(s)        : pImageData, pMsgLen, pSess                        */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
CapwapGetImageData (tImageData * pImageData,
                    UINT4 *pMsgLen, tRemoteSessionManager * pSess)
{
    INT4                i4ReadSize = 0;
    UINT1               au1FileReadBuf[IMAGE_READ_FLASH_SIZE];
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tCapwapWTPTrapInfo  sWTPTrapInfo;

    CAPWAP_FN_ENTRY ();

    MEMSET (au1FileReadBuf, 0, IMAGE_READ_FLASH_SIZE);
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&sWTPTrapInfo, 0, sizeof (tCapwapWTPTrapInfo));
    pImageData->u2MsgEleType = IMAGE_DATA;

    i4ReadSize = CapwapReadFromImage (au1FileReadBuf, pSess);
    if (i4ReadSize > 0)
    {
        pImageData->u2MsgEleType = IMAGE_DATA;
        MEMCPY (pImageData->au1ImageData, au1FileReadBuf, i4ReadSize);
    }
    else
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Read from Image is Completed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Read from Image is Completed \r\n"));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    if (pSess->u1EndofFileReached == OSIX_TRUE)
    {
        /*get AP mac address and image upgrade status to call trap */
        pImageData->u2MsgEleLen = (UINT2) i4ReadSize;
        pImageData->u1DataType = DATA_TRANS_EOF;
        pImageData->u2MsgEleLen = (UINT2) (pImageData->u2MsgEleLen + 1);
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpMacAddress = OSIX_TRUE;
        pWssIfCapwapDB->CapwapIsGetAllDB.bProfileName = OSIX_TRUE;
        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = pSess->u2IntProfileId;
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB) ==
            OSIX_SUCCESS)
        {
            MEMCPY (pWssIfCapwapDB->BaseMacAddress,
                    pWssIfCapwapDB->CapwapGetDB.WtpMacAddress, MAC_ADDR_LEN);
            MEMCPY (sWTPTrapInfo.au1capwapBaseNtfWtpId,
                    pWssIfCapwapDB->BaseMacAddress, MAC_ADDR_LEN);
            sWTPTrapInfo.i4fsCapwapWtpImageUpgradeStatus = OSIX_TRUE;

            /* Calling Image upgrade indication trap */
            CapwapSnmpifSendTrap (CAPWAP_WTP_IMAGE_UPGRADE_INDICATION,
                                  &sWTPTrapInfo);
        }
        mmi_printf ("\r ...Image upgrade Completed for profile : %s %%... \r\n",
                    pWssIfCapwapDB->CapwapGetDB.au1ProfileName);
        fclose (pSess->readFd);
        pSess->u1EndofFileReached = OSIX_FALSE;
        pSess->u4FileReadOffset = 0;
        pSess->u1ReadFileOpened = OSIX_FALSE;
        pSess->readFd = NULL;
        pSess->i4FileSize = 0;
        pWssIfCapwapDB->CapwapIsGetAllDB.bImageTransfer = OSIX_TRUE;
        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = pSess->u2IntProfileId;
        pWssIfCapwapDB->CapwapGetDB.u1ImageTransferFlag = OSIX_FALSE;
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_SET_DB, pWssIfCapwapDB) !=
            OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to SET the value in CAPWAP DB \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to SET the value in CAPWAP DB \r\n"));
        }

    }
    else
    {
        pImageData->u2MsgEleLen = (UINT2) i4ReadSize;
        pImageData->u1DataType = DATA_TRANS_DATA;
        pImageData->u2MsgEleLen = (UINT2) (pImageData->u2MsgEleLen + 1);
    }

    *pMsgLen += (UINT4) (pImageData->u2MsgEleLen + CAPWAP_MSG_ELEM_TYPE_LEN);
    pImageData->isOptional = OSIX_TRUE;
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapGetWLCImageDataRequestMsgElements           */
/*  Description     : The function gets WLC image data request msg      */
/*  Input(s)        : pImageDataReq, u4MsgLen, pSessEntry               */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
CapwapGetWLCImageDataRequestMsgElements (tImageDataReq * pImageDataReq,
                                         UINT4 *u4MsgLen,
                                         tRemoteSessionManager * pSessEntry)
{
    if (CapwapConstructCpHeader (&pImageDataReq->capwapHdr) == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapGetWLCImageDataRequestMsg :Failed to construct the capwap\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "CapwapGetWLCImageDataRequestMsg :Failed to construct the capwap\r\n"));
        return OSIX_FAILURE;
    }

    if (CapwapGetImageData (&pImageDataReq->ImageData,
                            u4MsgLen, pSessEntry) == OSIX_FAILURE)
    {
        CAPWAP_TRC (ALL_FAILURE_TRC,
                    "Failed to get Optional Message Elements \n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to get Optional Message Elements \n"));
        return OSIX_FAILURE;
    }

    /* update Control Header */
    pImageDataReq->u1NumMsgBlocks = 0;    /* flags */

    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapOpenAndReadFromImage                        */
/*  Description     : The function opens and reads from the image file  */
/*  Input(s)        : pSess                                             */
/*                    pBuf - Received packet                            */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
CapwapOpenAndReadFromImage (UINT1 *pBuf, tRemoteSessionManager * pSess)
{

    INT4                i4ReadSize = 0;
    INT4                i4SizeRemaining = 0;

    CAPWAP_FN_ENTRY ();

    i4SizeRemaining =
        (INT4) ((pSess->i4FileSize) - (INT4) (pSess->u4FileReadOffset));
    if (i4SizeRemaining > IMAGE_READ_FLASH_SIZE)
    {
        i4ReadSize =
            (INT4) fread (pBuf, 1, IMAGE_READ_FLASH_SIZE, pSess->readFd);
        pSess->u4FileReadOffset =
            pSess->u4FileReadOffset + IMAGE_READ_FLASH_SIZE;
    }
    else
    {
        i4ReadSize =
            (INT4) fread (pBuf, 1, (size_t) i4SizeRemaining, pSess->readFd);
        pSess->u1EndofFileReached = OSIX_TRUE;
    }

    gi4ReadCount++;

    CAPWAP_FN_EXIT ();
    return i4ReadSize;
}

/************************************************************************/
/*  Function Name   : CapwapValidateImageDataReqMsgElems                */
/*  Description     : The function read from image file                 */
/*  Input(s)        : pSess, pRcvBuf Received discovery packet          */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
CapwapReadFromImage (UINT1 *pBuf, tRemoteSessionManager * pSess)
{

    INT4                i4FileSize = 0;
    INT4                i4ReadSize = 0;
    INT4                i4SizeRemaining = 0;

    CAPWAP_FN_ENTRY ();

    i4FileSize = pSess->i4FileSize;
    /* Checking File size is zero */
    if (i4FileSize == 0)
    {
        return 0;
    }
    i4SizeRemaining = (INT4) ((i4FileSize) - (INT4) (pSess->u4FileReadOffset));
    if (i4SizeRemaining > IMAGE_READ_FLASH_SIZE)
    {
        i4ReadSize =
            (INT4) fread (pBuf, 1, IMAGE_READ_FLASH_SIZE, pSess->readFd);
        pSess->u4FileReadOffset =
            pSess->u4FileReadOffset + IMAGE_READ_FLASH_SIZE;
    }
    else
    {
        i4ReadSize =
            (INT4) fread (pBuf, 1, (size_t) i4SizeRemaining, pSess->readFd);
        pSess->u1EndofFileReached = OSIX_TRUE;
    }

    gi4ReadCount++;

    CAPWAP_FN_EXIT ();
    return i4ReadSize;
}

/************************************************************************/
/*  Function Name   : CapwapGetWLCImageId                               */
/*  Description     : The function gets WLC image ID                    */
/*  Input(s)        : pImageId,pMsgLen                                  */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/

INT4
CapwapGetWLCImageId (tImageId * pImageId, UINT4 *pMsgLen)
{
    pImageId->u2MsgEleType = IMAGE_IDENTIFIER;
    pImageId->u4VendorId = 0;
    pImageId->u2MsgEleLen =
        (UINT2) (pImageId->u2MsgEleLen + CAPWAP_MSG_ELEM_TYPE_LEN);
    *pMsgLen += (UINT4) (pImageId->u2MsgEleLen + CAPWAP_MSG_ELEM_TYPE_LEN);
    pImageId->isOptional = OSIX_TRUE;

    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapGetStatisticsTimer                          */
/*  Description     : The function gets statistics time                 */
/*  Input(s)        : pStats,pMsgLen                                    */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/

INT4
CapwapGetStatisticsTimer (tStatisticsTimer * pStats, UINT4 *pMsgLen)
{
    UINT2               u2EleLen = 0;
    pStats->u2MsgEleType = STATS_TIMER;
    pStats->u2MsgEleLen = STATS_TIMER_LEN;
    pStats->isOptional = CAPWAP_MSG_ELEM_TYPE_LEN;
    u2EleLen = (UINT2) (pStats->u2MsgEleLen + CAPWAP_MSG_ELEM_TYPE_LEN);
    *pMsgLen = *pMsgLen + u2EleLen;

    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapValidateImageDataReqMsgElems                */
/*  Description     : The function validates the functional             */
/*                    characteristics of the received CAPWAP            */
/*                    Image data request against the configured         */
/*                    WTP profile                                       */
/*  Input(s)        : discReqMsg - parsed CAPWAP Discovery request      */
/*                    pRcvBuf - Received discovery packet               */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
CapwapValidateImageDataReqMsgElems (UINT1 *pRcvBuf,
                                    tCapwapControlPacket * pImageDataReqMsg,
                                    tImageDataReq * pImageDataReq)
{

    UNUSED_PARAM (pRcvBuf);
    UNUSED_PARAM (pImageDataReqMsg);
    UNUSED_PARAM (pImageDataReq);

    CAPWAP_FN_ENTRY ();

    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapValidateImageDataRespMsgElems                */
/*  Description     : The function validates the functional             */
/*                    characteristics of the received CAPWAP            */
/*                    Join request agains the configured                */
/*                    WTP profile                                       */
/*  Input(s)        : discReqMsg - parsed CAPWAP Discovery request      */
/*                    pRcvBuf - Received discovery packet               */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
CapwapValidateImageDataRespMsgElems (UINT1 *pRcvBuf,
                                     tCapwapControlPacket * pImageDataRsp,
                                     tRemoteSessionManager * pSessEntry)
{
    CAPWAP_FN_ENTRY ();
    UNUSED_PARAM (pRcvBuf);
    UNUSED_PARAM (pImageDataRsp);
    UNUSED_PARAM (pSessEntry);

    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}
#endif /* __CAPWAP_WLCIMAGE_ */
/*-----------------------------------------------------------------------*/
/*                    End of the file  capwapwlcimage.c                  */
/*-----------------------------------------------------------------------*/
