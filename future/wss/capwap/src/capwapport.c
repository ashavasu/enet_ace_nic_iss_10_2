/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved                      *
 * $Id: capwapport.c,v 1.2 2017/05/23 14:16:49 siva Exp $       
 * Description:  This file contains porting related                          *
 *                             external interface routines.                  *
 *****************************************************************************/
#ifndef __CAPWAPPORT_C__
#define __CAPWAPPORT_C__
#define _CAPWAP_GLOBAL_VAR

#include "capwapinc.h"

INT4
CapwapProcessWssIfMsg (UINT1 u1MsgType, unCapwapMsgStruct * pWssMsgStruct)
{
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tRemoteSessionManager *pSessEntry = NULL;
    tCapwapHdr          CapwapHdr;
    UINT1               au1CapHdr[20];
    UINT2               u2PacketLen = 0;

    CAPWAP_FN_ENTRY ();

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (au1CapHdr, 0, sizeof (au1CapHdr));
    switch (u1MsgType)
    {
        case WSS_CAPWAP_PARSE_REQ:
            if (CapwapParseControlPacket (pWssMsgStruct->CapwapParseReq.pData,
                                          pWssMsgStruct->CapwapParseReq.
                                          pCapwapCtrlMsg) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "CapwapProcessWssIfMsg:Failed to parse the received"
                            "CAPWAP control packet \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "CapwapProcessWssIfMsg:Failed to parse the received"
                              "CAPWAP control packet \r\n"));
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
            break;
        case WSS_CAPWAP_PARSE_MEMREL_REQ:
            if (CapwapParseStructureMemrelease (pWssMsgStruct->CapwapParseReq.
                                                pCapwapCtrlMsg) == OSIX_FAILURE)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "CapwapProcessWssIfMsg:Failed to Free the memory"
                            "allocated in parser module \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "CapwapProcessWssIfMsg:Failed to Free the memory"
                              "allocated in parser module \r\n"));
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
            break;
        case WSS_CAPWAP_ASSEMBLE_WTP_EVENT_REQ:
            /* update CAPWAP Header */
            if (CapwapConstructCpHeader (&pWssMsgStruct->CapAssembleWtpEventReq.
                                         pCapwapCtrlMsg->capwapHdr) ==
                OSIX_FAILURE)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "WSS_CAPWAP_ASSEMBLE_WLAN_CONF_REQ:Failed to Construct"
                            "the CAPWAP Header \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "WSS_CAPWAP_ASSEMBLE_WLAN_CONF_REQ:Failed to Construct"
                              "the CAPWAP Header \r\n"));
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
            if (CapwapAssembleWtpEventRequest (pWssMsgStruct->
                                               CapAssembleWtpEventReq.u2MsgLen,
                                               pWssMsgStruct->
                                               CapAssembleWtpEventReq.
                                               pCapwapCtrlMsg,
                                               pWssMsgStruct->
                                               CapAssembleWtpEventReq.pData) ==
                OSIX_FAILURE)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Failed to Assemble WTP Event Request \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to Assemble WTP Event Request \r\n"));
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
            break;
        case WSS_CAPWAP_ASSEMBLE_EVENT_RSP:
            if (CapwapAssembleWtpEventResponse (pWssMsgStruct->
                                                CapAssembleWtpEventRsp.u2MsgLen,
                                                pWssMsgStruct->
                                                CapAssembleWtpEventRsp.
                                                pCapwapCtrlMsg,
                                                pWssMsgStruct->
                                                CapAssembleWtpEventRsp.pData) ==
                OSIX_FAILURE)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Failed to Asseemble WTP Event Response \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to Asseemble WTP Event Response \r\n"));
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
            break;
        case WSS_CAPWAP_ASSEMBLE_CHANGESTATE_EVENT_REQ:
            if (CapwapAssembleChangeStateEventRequest (pWssMsgStruct->
                                                       CapAssembleChangeStateReq.
                                                       u2MsgLen,
                                                       pWssMsgStruct->
                                                       CapAssembleChangeStateReq.
                                                       pCapwapCtrlMsg,
                                                       pWssMsgStruct->
                                                       CapAssembleChangeStateReq.
                                                       pData) == OSIX_FAILURE)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Failed to Assemble Change State Event Request \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to Assemble Change State Event Request \r\n"));
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
            break;
        case WSS_CAPWAP_ASSEMBLE_CHANGESTATE_EVENT_RSP:
            if (CapwapAssembleChangeStateEventResponse (pWssMsgStruct->
                                                        CapAssembleChangeStateRsp.
                                                        u2MsgLen,
                                                        pWssMsgStruct->
                                                        CapAssembleChangeStateRsp.
                                                        pCapwapCtrlMsg,
                                                        pWssMsgStruct->
                                                        CapAssembleChangeStateRsp.
                                                        pData) == OSIX_FAILURE)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Assemble Change"
                            "State Event Response \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to Assemble Change"
                              "State Event Response \r\n"));
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
            break;
        case WSS_CAPWAP_ASSEMBLE_PRIMARY_DISCOVERY_REQ:
            if (CapwapAssemblePrimaryDiscoveryReq (pWssMsgStruct->
                                                   CapAssemblePrimaryDiscReq.
                                                   u2MsgLen,
                                                   pWssMsgStruct->
                                                   CapAssemblePrimaryDiscReq.
                                                   pCapwapCtrlMsg,
                                                   pWssMsgStruct->
                                                   CapAssemblePrimaryDiscReq.
                                                   pData) == OSIX_FAILURE)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Assemble Primary"
                            "Discovery Request \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to Assemble Primary"
                              "Discovery Request \r\n"));
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
            break;
        case WSS_CAPWAP_ASSEMBLE_PRIMARY_DISCOVERY_RSP:
            if (CapwapAssemblePrimaryDiscoveryRsp (pWssMsgStruct->
                                                   CapAssemblePrimaryDiscRsp.
                                                   u2MsgLen,
                                                   pWssMsgStruct->
                                                   CapAssemblePrimaryDiscRsp.
                                                   pCapwapCtrlMsg,
                                                   pWssMsgStruct->
                                                   CapAssemblePrimaryDiscRsp.
                                                   pData) == OSIX_FAILURE)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Assemble Primary"
                            "Discovery Response \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to Assemble Primary"
                              "Discovery Response \r\n"));
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
            break;
        case WSS_CAPWAP_ASSEMBLE_WLAN_CONF_REQ:
            /* update CAPWAP Header */
            if (CapwapConstructCpHeader (&pWssMsgStruct->
                                         CapAssembleWlanUpdateReq.
                                         pCapwapCtrlMsg->capwapHdr) ==
                OSIX_FAILURE)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "WSS_CAPWAP_ASSEMBLE_WLAN_CONF_REQ:Failed to Construct"
                            "the CAPWAP Header \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "WSS_CAPWAP_ASSEMBLE_WLAN_CONF_REQ:Failed to Construct"
                              "the CAPWAP Header \r\n"));
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
            if (CapwapAssembIeeeWLANConfigurationReq (pWssMsgStruct->
                                                      CapAssembleWlanUpdateReq.
                                                      u2MsgLen,
                                                      pWssMsgStruct->
                                                      CapAssembleWlanUpdateReq.
                                                      pCapwapCtrlMsg,
                                                      pWssMsgStruct->
                                                      CapAssembleWlanUpdateReq.
                                                      pData) == OSIX_FAILURE)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Assemble WLAN"
                            "Configuration Request \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to Assemble WLAN"
                              "Configuration Request \r\n"));
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
            break;
        case WSS_CAPWAP_ASSEMBLE_WLAN_CONF_RSP:
            /* update CAPWAP Header */
            if (CapwapConstructCpHeader (&pWssMsgStruct->
                                         CapAssembleWlanUpdateRsp.
                                         pCapwapCtrlMsg->capwapHdr) ==
                OSIX_FAILURE)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "WSS_CAPWAP_ASSEMBLE_WLAN_CONF_RSP:Failed to"
                            "Construct the CAPWAP Header \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "WSS_CAPWAP_ASSEMBLE_WLAN_CONF_RSP:Failed to"
                              "Construct the CAPWAP Header \r\n"));
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
            if (CapwapAssembleIeeeWLANConfigurationRsp (pWssMsgStruct->
                                                        CapAssembleWlanUpdateRsp.
                                                        u2MsgLen,
                                                        pWssMsgStruct->
                                                        CapAssembleWlanUpdateRsp.
                                                        pCapwapCtrlMsg,
                                                        pWssMsgStruct->
                                                        CapAssembleWlanUpdateRsp.
                                                        pData) == OSIX_FAILURE)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Assemble WLAN"
                            "configuration Response \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to Assemble WLAN"
                              "configuration Response \r\n"));
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
            break;
        case WSS_CAPWAP_ASSEMBLE_CONF_UPDATE_REQ:
            if (CapwapConstructCpHeader (&pWssMsgStruct->
                                         CapAssembleConfUpdateReq.
                                         pCapwapCtrlMsg->capwapHdr) ==
                OSIX_FAILURE)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "WSS_CAPWAP_ASSEMBLE_CONF_UPDATE_REQ:Failed to"
                            "Construct the CAPWAP Header \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "WSS_CAPWAP_ASSEMBLE_CONF_UPDATE_REQ:Failed to"
                              "Construct the CAPWAP Header \r\n"));
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
            if (CapwapAssembleConfigUpdateReq (pWssMsgStruct->
                                               CapAssembleConfUpdateReq.
                                               u2MsgLen,
                                               pWssMsgStruct->
                                               CapAssembleConfUpdateReq.
                                               pCapwapCtrlMsg,
                                               pWssMsgStruct->
                                               CapAssembleConfUpdateReq.
                                               pData) == OSIX_FAILURE)

            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Assemble"
                            "Configuration update Request \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to Assemble"
                              "Configuration update Request \r\n"));
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
            break;
        case WSS_CAPWAP_ASSEMBLE_CONF_UPDATE_RSP:
        {
            /* update CAPWAP Header */
            if (CapwapConstructCpHeader (&pWssMsgStruct->
                                         CapAssembleConfUpdateRsp.
                                         pCapwapCtrlMsg->capwapHdr) ==
                OSIX_FAILURE)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "WSS_CAPWAP_ASSEMBLE_CONF_UPDATE_RSP:Failed to"
                            "Construct the CAPWAP Header \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "WSS_CAPWAP_ASSEMBLE_CONF_UPDATE_RSP:Failed to"
                              "Construct the CAPWAP Header \r\n"));
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
            if (CapwapAssembleConfigUpdateResp (pWssMsgStruct->
                                                CapAssembleConfUpdateRsp.
                                                u2MsgLen,
                                                pWssMsgStruct->
                                                CapAssembleConfUpdateRsp.
                                                pCapwapCtrlMsg,
                                                pWssMsgStruct->
                                                CapAssembleConfUpdateRsp.
                                                pData) == OSIX_FAILURE)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Assemble"
                            "Configuration Update Response \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to Assemble"
                              "Configuration Update Response \r\n"));
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
            break;
        }
        case WSS_CAPWAP_ASSEMBLE_STATION_CONF_REQ:
        {
            /* Upddate the CAPWAP Header */
            if (CapwapConstructCpHeader (&pWssMsgStruct->
                                         CapAssembleStaConfReq.pCapwapCtrlMsg->
                                         capwapHdr) == OSIX_FAILURE)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Construct"
                            "the CAPWAP Header \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to Construct" "the CAPWAP Header \r\n"));
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
            if (CapwapAssembleConfigStationReq (pWssMsgStruct->
                                                CapAssembleStaConfReq.u2MsgLen,
                                                pWssMsgStruct->
                                                CapAssembleStaConfReq.
                                                pCapwapCtrlMsg,
                                                pWssMsgStruct->
                                                CapAssembleStaConfReq.pData) ==
                OSIX_FAILURE)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Assemble"
                            "Configuration Station Request \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to Assemble"
                              "Configuration Station Request \r\n"));
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
            break;
        }
        case WSS_CAPWAP_ASSEMBLE_SATION_CONF_RSP:
        {
            /* Upddate the CAPWAP Header */
            if (CapwapConstructCpHeader (&pWssMsgStruct->
                                         CapAssembleStaConfRsp.pCapwapCtrlMsg->
                                         capwapHdr) == OSIX_FAILURE)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Construct"
                            "the CAPWAP Header \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to Construct" "the CAPWAP Header \r\n"));
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
            if (CapwapAssembleConfigStationResp (pWssMsgStruct->
                                                 CapAssembleStaConfRsp.u2MsgLen,
                                                 pWssMsgStruct->
                                                 CapAssembleStaConfRsp.
                                                 pCapwapCtrlMsg,
                                                 pWssMsgStruct->
                                                 CapAssembleStaConfRsp.pData) ==
                OSIX_FAILURE)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Assemble"
                            "Station Configuration Response \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to Assemble"
                              "Station Configuration Response \r\n"));
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
            break;
        }
        case WSS_CAPWAP_ASSEMBLE_RESET_REQ:
        {
            /* Upddate the CAPWAP Header */
            if (CapwapConstructCpHeader (&pWssMsgStruct->
                                         CapAssembleResetReq.pCapwapCtrlMsg->
                                         capwapHdr) == OSIX_FAILURE)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "WSS_CAPWAP_ASSEMBLE_RESET_REQ:Failed to Construct"
                            "the CAPWAP Header \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "WSS_CAPWAP_ASSEMBLE_RESET_REQ:Failed to Construct"
                              "the CAPWAP Header \r\n"));
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
            if (CapwapAssembleResetRequest (pWssMsgStruct->
                                            CapAssembleResetReq.u2MsgLen,
                                            pWssMsgStruct->CapAssembleResetReq.
                                            pCapwapCtrlMsg,
                                            pWssMsgStruct->CapAssembleResetReq.
                                            pData) == OSIX_FAILURE)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Failed to Assemble Reset Request \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to Assemble Reset Request \r\n"));
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
            break;
        }
        case WSS_CAPWAP_ASSEMBLE_RESET_RSP:
            /* Upddate the CAPWAP Header */
            if (CapwapConstructCpHeader (&pWssMsgStruct->CapAssembleResetRsp.
                                         pCapwapCtrlMsg->capwapHdr) ==
                OSIX_FAILURE)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "WSS_CAPWAP_ASSEMBLE_RESET_RSP:Failed to Construct"
                            "the CAPWAP Header \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "WSS_CAPWAP_ASSEMBLE_RESET_RSP:Failed to Construct"
                              "the CAPWAP Header \r\n"));
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
            if (CapwapAssembleResetResponse (pWssMsgStruct->CapAssembleResetRsp.
                                             u2MsgLen,
                                             pWssMsgStruct->CapAssembleResetRsp.
                                             pCapwapCtrlMsg,
                                             pWssMsgStruct->CapAssembleResetRsp.
                                             pData) == OSIX_FAILURE)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Assemble"
                            "Reset Response \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to Assemble" "Reset Response \r\n"));
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
            break;
        case WSS_CAPWAP_ASSEMBLE_CLEARCONF_REQ:
            /* Upddate the CAPWAP Header */
            if (CapwapConstructCpHeader (&pWssMsgStruct->
                                         CapAssembleClearConfReq.
                                         pCapwapCtrlMsg->capwapHdr) ==
                OSIX_FAILURE)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "WSS_CAPWAP_ASSEMBLE_CLEARCONF_REQ:Failed to"
                            "Construct the CAPWAP Header \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "WSS_CAPWAP_ASSEMBLE_CLEARCONF_REQ:Failed to"
                              "Construct the CAPWAP Header \r\n"));
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
            if (CapwapAssembleClearConfigReq (pWssMsgStruct->
                                              CapAssembleClearConfReq.u2MsgLen,
                                              pWssMsgStruct->
                                              CapAssembleClearConfReq.
                                              pCapwapCtrlMsg,
                                              pWssMsgStruct->
                                              CapAssembleClearConfReq.pData) ==
                OSIX_FAILURE)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Assemble Clear"
                            "configuration Request \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to Assemble Clear"
                              "configuration Request \r\n"));
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
            break;
        case WSS_CAPWAP_ASSEMBLE_CLEARCONF_RSP:
            /* Upddate the CAPWAP Header */
            if (CapwapConstructCpHeader (&pWssMsgStruct->
                                         CapAssembleClearConfRsp.
                                         pCapwapCtrlMsg->capwapHdr) ==
                OSIX_FAILURE)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "WSS_CAPWAP_ASSEMBLE_CLEARCONF_RSP:Failed to"
                            "Construct the CAPWAP Header \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "WSS_CAPWAP_ASSEMBLE_CLEARCONF_RSP:Failed to"
                              "Construct the CAPWAP Header \r\n"));
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
            if (CapwapAssembleClearConfigResp (pWssMsgStruct->
                                               CapAssembleClearConfRsp.u2MsgLen,
                                               pWssMsgStruct->
                                               CapAssembleClearConfRsp.
                                               pCapwapCtrlMsg,
                                               pWssMsgStruct->
                                               CapAssembleClearConfRsp.pData) ==
                OSIX_FAILURE)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Assemble"
                            "Clear configuration Response \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to Assemble"
                              "Clear configuration Response \r\n"));
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
            break;
            /*case WSS_CAPWAP_ASSEMBLE_IMAGEDATA_REQ:
               if (CapwapAssembleImageDataReq (pWssMsgStruct->
               CapAssembleImageDataReq.u2MsgLen,
               pWssMsgStruct->CapAssembleImageDataReq.pCapwapCtrlMsg,
               pWssMsgStruct->CapAssembleImageDataReq.pData) == OSIX_FAILURE)

               {
               CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Assemble"
               "Image Data Request \r\n");
               WSS_IF_DB_TEMP_MEM_RELEASE(pWssIfCapwapDB);
               return OSIX_FAILURE;
               }
               break;   
               case WSS_CAPWAP_ASSEMBLE_IMAGEDATA_RSP:
               if (CapwapAssembleImageDataRsp (pWssMsgStruct->
               CapAssembleImageDataRsp.u2MsgLen,
               pWssMsgStruct->CapAssembleImageDataRsp.pCapwapCtrlMsg,
               pWssMsgStruct->CapAssembleImageDataRsp.pData) == OSIX_FAILURE)

               {
               CAPWAP_TRC (CAPWAP_FAILURE_TRC , "Failed to Assemble"
               "Image Data Response \r\n");
               WSS_IF_DB_TEMP_MEM_RELEASE(pWssIfCapwapDB);
               return OSIX_FAILURE;
               }
               break; */
        case WSS_CAPWAP_ASSEMBLE_DATATRANSFER_REQ:
            if (CapwapConstructCpHeader (&pWssMsgStruct->
                                         CapAssembleDataTransferReq.
                                         pCapwapCtrlMsg->capwapHdr) ==
                OSIX_FAILURE)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "WSS_CAPWAP_ASSEMBLE_DATATRANSFER_REQ : Failed to"
                            "Construct the CAPWAP Header \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "WSS_CAPWAP_ASSEMBLE_DATATRANSFER_REQ : Failed to"
                              "Construct the CAPWAP Header \r\n"));
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
            if (CapwapAssembleDataTransferReq (pWssMsgStruct->
                                               CapAssembleDataTransferReq.
                                               u2MsgLen,
                                               pWssMsgStruct->
                                               CapAssembleDataTransferReq.
                                               pCapwapCtrlMsg,
                                               pWssMsgStruct->
                                               CapAssembleDataTransferReq.
                                               pData) == OSIX_FAILURE)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Assemble"
                            "Data Transfer Request \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to Assemble"
                              "Data Transfer Request \r\n"));
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
            break;
        case WSS_CAPWAP_ASSEMBLE_DATATRANSFER_RSP:
            if (CapwapAssembleDataTransferResp (pWssMsgStruct->
                                                CapAssembleDataTransferRsp.
                                                u2MsgLen,
                                                pWssMsgStruct->
                                                CapAssembleDataTransferRsp.
                                                pCapwapCtrlMsg,
                                                pWssMsgStruct->
                                                CapAssembleDataTransferRsp.
                                                pData) == OSIX_FAILURE)

            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Assemble"
                            "Data Transfer Response \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to Assemble"
                              "Data Transfer Response \r\n"));
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
            break;
        case WSS_CAPWAP_TX_CTRL_PKT:
            pWssIfCapwapDB->u4DestIp = pWssMsgStruct->CapwapTxPkt.u4IpAddr;
            pWssIfCapwapDB->u4DestPort = pWssMsgStruct->CapwapTxPkt.u4DestPort;
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_RSM, pWssIfCapwapDB) ==
                OSIX_FAILURE)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Failed to Retrive CAPWAP DB \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to Retrive CAPWAP DB \r\n"));
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
            pSessEntry = pWssIfCapwapDB->pSessEntry;
            if (CapwapTxMessage (pWssMsgStruct->CapwapTxPkt.pData,
                                 pSessEntry,
                                 pWssMsgStruct->CapwapTxPkt.u4pktLen,
                                 WSS_CAPWAP_802_11_CTRL_PKT) == OSIX_FAILURE)

            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Failed to Transmit packet to the SLL \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to Transmit packet to the SLL \r\n"));
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
            /*CAPWAP_RELEASE_CRU_BUF(pWssMsgStruct->CapwapTxPkt.pData); */
            break;
        case WSS_CAPWAP_WTP_TX_DATA_PKT:
            /* For WTP only one session Entry exists at at time */
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_RSM,
                                         pWssIfCapwapDB) == OSIX_FAILURE)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Failed to Retrive CAPWAP DB \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to Retrive CAPWAP DB \r\n"));
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
            pSessEntry = pWssIfCapwapDB->pSessEntry;
            if (pSessEntry == NULL)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "pSessEntry is NULL. Discard the packet \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "pSessEntry is NULL. Discard the packet \r\n"));
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
            if (pSessEntry->eCurrentState != CAPWAP_RUN)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "The WTP is not in Run State \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "The WTP is not in Run State \r\n"));
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
            if (pWssMsgStruct->CapwapTxPkt.pData == NULL)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "The Received packet buffer is NULL \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "The Received packet buffer is NULL \r\n"));
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
            /* Assemble CAPWAP Header and prepend to the Data pakcet */

            if (CapwapConstructCpHeader (&CapwapHdr) == OSIX_SUCCESS)
            {
                if (pWssMsgStruct->CapwapTxPkt.u4MsgType == WSS_CAPWAP_TX_CTRL_PKT)    /* DOT11 Mgmt Pkts */
                {
                    CapwapHdr.u2HlenRidWbidFlagT =
                        CAPWAP_HDR_RID_WID_FLAGS_SPLIT;
                    pWssMsgStruct->CapwapTxPkt.u1DscpValue =
                        CAPWAP_CTRL_PKT_DSCP_CODE;
                }
                else if (pWssMsgStruct->CapwapTxPkt.u4MsgType == WSS_CAPWAP_WTP_TX_DATA_PKT)    /*DOT11 or DOT3 Data Pkts */
                {
                    if (pWssMsgStruct->CapwapTxPkt.u1MacType == SPLIT_MAC)
                    {
                        CapwapHdr.u2HlenRidWbidFlagT = CAPWAP_HDR_RID_WID_FLAGS_SPLIT;    /*Overwriting it
                                                                                           for data msgs */
                    }
                    else if (pWssMsgStruct->CapwapTxPkt.u1MacType == LOCAL_MAC)    /* Local MAC Mode */
                    {
                        CapwapHdr.u2HlenRidWbidFlagT =
                            CAPWAP_HDR_RID_WID_FLAGS_LOCAL;
                        CapwapHdr.u1FlagsFLWMKResd = CAPWAP_HDR_FLWMK_FLAGS;
                        CapwapHdr.u1RadMacAddrLength = CAPWAP_HDR_RADIO_MAC;    /* sizeof tMacAddr */
                        CapwapHdr.u1WirelessInfoLength = CAPWAP_HDR_NULL_WIRELESS_INFO;    /* Optional
                                                                                           Wireless Info */

                        MEMCPY (&(CapwapHdr.RadioMacAddr),
                                &(pWssMsgStruct->CapwapTxPkt.BssIdMac),
                                sizeof (tMacAddr));

                        /* Hdr Len = 8 (default) + 1 (Radio Mac Len) +
                         * Radio Mac Addr + Wireless info Len */
                        CapwapHdr.u2HdrLen = CAPWAP_HDR_MIN_LEN +
                            CAPWAP_HDR_RADIO_MAC_LEN +
                            CAPWAP_HDR_RADIO_MAC + CAPWAP_HDR_WIRELESS_INFO_LEN;
                    }
                }
                AssembleCapwapHeader (au1CapHdr, &CapwapHdr);
                if (CRU_BUF_Prepend_BufChain (pWssMsgStruct->CapwapTxPkt.pData,
                                              (UINT1 *) au1CapHdr,
                                              CapwapHdr.u2HdrLen) ==
                    CRU_FAILURE)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Cru buffer Prepend"
                                "operation Failed to Prepend CAPWAP Header\r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Cru buffer Prepend"
                                  "operation Failed to Prepend CAPWAP Header\r\n"));
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    return OSIX_FAILURE;
                }
            }

            u2PacketLen = (UINT2) CAPWAP_GET_BUF_LEN (pWssMsgStruct->
                                                      CapwapTxPkt.pData);
            if (CapwapTxMessage (pWssMsgStruct->CapwapTxPkt.pData,
                                 pSessEntry, (UINT4) u2PacketLen,
                                 WSS_CAPWAP_802_11_DATA_PKT) != OSIX_SUCCESS)

            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Transmit packet"
                            "to the SLL \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to Transmit packet" "to the SLL \r\n"));
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
            /* CAPWAP_RELEASE_CRU_BUF(pWssMsgStruct->CapwapTxPkt.pData); */
            break;
        case WSS_CAPWAP_WLC_TX_DATA_PKT:
            pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = pWssMsgStruct->
                CapwapTxPkt.u2SessId;
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_RSM_FROM_INDEX,
                                         pWssIfCapwapDB) == OSIX_FAILURE)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Retrive"
                            "CAPWAP DB \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to Retrive" "CAPWAP DB \r\n"));
                if (pWssMsgStruct->CapwapTxPkt.u4MsgType !=
                    WSS_CAPWAP_802_11_DATA_PKT)
                {
                    CAPWAP_RELEASE_CRU_BUF (pWssMsgStruct->CapwapTxPkt.pData);
                }

                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
            pSessEntry = pWssIfCapwapDB->pSessEntry;
            if (pSessEntry == NULL)    /*FIX ME */
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "pSessEntry is NULL."
                            "Discard the packet \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "pSessEntry is NULL." "Discard the packet \r\n"));
                if (pWssMsgStruct->CapwapTxPkt.u4MsgType !=
                    WSS_CAPWAP_802_11_DATA_PKT)
                {
                    CAPWAP_RELEASE_CRU_BUF (pWssMsgStruct->CapwapTxPkt.pData);
                }
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
            if (pSessEntry->eCurrentState != CAPWAP_RUN)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "\npSessEntry is NULL."
                            "Discard the packet \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "\npSessEntry is NULL."
                              "Discard the packet \r\n"));
                if (pWssMsgStruct->CapwapTxPkt.u4MsgType !=
                    WSS_CAPWAP_802_11_DATA_PKT)
                {
                    CAPWAP_RELEASE_CRU_BUF (pWssMsgStruct->CapwapTxPkt.pData);
                }
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
            /* Assemble CAPWAP Header and prepend to the Data pakcet */

            if (CapwapConstructCpHeader (&CapwapHdr) == OSIX_SUCCESS)
            {
                if (pWssMsgStruct->CapwapTxPkt.u4MsgType ==
                    WSS_CAPWAP_802_11_MGMT_PKT)
                {
                    CapwapHdr.u2HlenRidWbidFlagT =
                        CAPWAP_HDR_RID_WID_FLAGS_SPLIT;
                    pWssMsgStruct->CapwapTxPkt.u1DscpValue =
                        CAPWAP_CTRL_PKT_DSCP_CODE;
                }
                else if (pWssMsgStruct->CapwapTxPkt.u4MsgType ==
                         WSS_CAPWAP_802_11_DATA_PKT)
                {
                    if (pWssMsgStruct->CapwapTxPkt.u1MacType == SPLIT_MAC)
                    {
                        CapwapHdr.u2HlenRidWbidFlagT =
                            CAPWAP_HDR_RID_WID_FLAGS_SPLIT;
                        /*Overwriting it for data msgs */
                    }
                    else if (pWssMsgStruct->CapwapTxPkt.u1MacType == LOCAL_MAC)    /* Local MAC Mode */
                    {
                        CapwapHdr.u2HlenRidWbidFlagT =
                            CAPWAP_HDR_RID_WID_FLAGS_LOCAL;
                        CapwapHdr.u1FlagsFLWMKResd = CAPWAP_HDR_FLWMK_FLAGS;
                        CapwapHdr.u1RadMacAddrLength = CAPWAP_HDR_RADIO_MAC;    /* sizeof tMacAddr */
                        CapwapHdr.u1WirelessInfoLength = CAPWAP_HDR_NULL_WIRELESS_INFO;    /* Optional
                                                                                           Wireless Info */

                        MEMCPY (&(CapwapHdr.RadioMacAddr),
                                &(pWssMsgStruct->CapwapTxPkt.BssIdMac),
                                sizeof (tMacAddr));

                        /* Hdr Len = 8 (default) + 1 (Radio Mac Len) +
                         * Radio Mac Addr + Wireless info Len */
                        CapwapHdr.u2HdrLen = CAPWAP_HDR_MIN_LEN +
                            CAPWAP_HDR_RADIO_MAC_LEN +
                            CAPWAP_HDR_RADIO_MAC + CAPWAP_HDR_WIRELESS_INFO_LEN;
                    }
                }

                AssembleCapwapHeader (au1CapHdr, &CapwapHdr);
                if (CRU_BUF_Prepend_BufChain (pWssMsgStruct->CapwapTxPkt.pData,
                                              (UINT1 *) au1CapHdr,
                                              CapwapHdr.u2HdrLen) ==
                    CRU_FAILURE)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Cru buffer Prepend"
                                "operation Failed to Prepend CAPWAP Header\r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Cru buffer Prepend"
                                  "operation Failed to Prepend CAPWAP Header\r\n"));
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    return OSIX_FAILURE;
                }
            }

            u2PacketLen = (UINT2) CAPWAP_GET_BUF_LEN (pWssMsgStruct->
                                                      CapwapTxPkt.pData);
            pWssMsgStruct->CapwapTxPkt.u1DscpValue = CAPWAP_CTRL_PKT_DSCP_CODE;    /* Dot11 Mgmt Pkts */
            if (CapwapTxMessage (pWssMsgStruct->CapwapTxPkt.pData,
                                 pSessEntry, (UINT4) u2PacketLen,
                                 WSS_CAPWAP_802_11_DATA_PKT) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Transmit"
                            "packet to the SLL \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to Transmit" "packet to the SLL \r\n"));
                if (pWssMsgStruct->CapwapTxPkt.u4MsgType !=
                    WSS_CAPWAP_802_11_DATA_PKT)
                {
                    CAPWAP_RELEASE_CRU_BUF (pWssMsgStruct->CapwapTxPkt.pData);
                }
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }

            if (pWssMsgStruct->CapwapTxPkt.u4MsgType !=
                WSS_CAPWAP_802_11_DATA_PKT)
            {
                CAPWAP_RELEASE_CRU_BUF (pWssMsgStruct->CapwapTxPkt.pData);
            }
            break;
        default:
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Received the unknown Event \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Received the unknown Event \r\n"));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;

}

INT4
WssIfProcessCapwapMsg (UINT1 MsgType, unCapwapMsgStruct * pWssIfMsg)
{
    if (CapwapProcessWssIfMsg (MsgType, pWssIfMsg) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

#endif /*__CAPWAPPORT_C__ */
/*-----------------------------------------------------------------------*/
/*                       End of the file  capwapport.c                   */
/*-----------------------------------------------------------------------*/
