/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: capwapclidefault.c,v 1.1.1.1 2016/09/20 10:42:39 siva Exp $       
*
* Description: This file contains the routines to initialize the
*              protocol structure for the module Capwap 
*********************************************************************/

#include "capwapinc.h"

/****************************************************************************
* Function    : CapwapInitializeCapwapBaseAcNameListTable
* Input       : pCapwapCapwapBaseAcNameListEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/

INT4
CapwapInitializeCapwapBaseAcNameListTable (tCapwapCapwapBaseAcNameListEntry *
                                           pCapwapCapwapBaseAcNameListEntry)
{
    if (pCapwapCapwapBaseAcNameListEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((CapwapInitializeMibCapwapBaseAcNameListTable
         (pCapwapCapwapBaseAcNameListEntry)) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : CapwapInitializeCapwapBaseMacAclTable
* Input       : pCapwapCapwapBaseMacAclEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/

INT4
CapwapInitializeCapwapBaseMacAclTable (tCapwapCapwapBaseMacAclEntry *
                                       pCapwapCapwapBaseMacAclEntry)
{
    if (pCapwapCapwapBaseMacAclEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((CapwapInitializeMibCapwapBaseMacAclTable
         (pCapwapCapwapBaseMacAclEntry)) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : CapwapInitializeCapwapBaseWtpProfileTable
* Input       : pCapwapCapwapBaseWtpProfileEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/

INT4
CapwapInitializeCapwapBaseWtpProfileTable (tCapwapCapwapBaseWtpProfileEntry *
                                           pCapwapCapwapBaseWtpProfileEntry)
{
    if (pCapwapCapwapBaseWtpProfileEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((CapwapInitializeMibCapwapBaseWtpProfileTable
         (pCapwapCapwapBaseWtpProfileEntry)) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : CapwapInitializeFsWtpModelTable
* Input       : pCapwapFsWtpModelEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/

INT4 CapwapInitializeFsWtpModelTable (tCapwapFsWtpModelEntry *pCapwapFsWtpModelEntry)
{
    if (pCapwapFsWtpModelEntry == NULL)
    {
        return OSIX_FAILURE;
    }
	if ((CapwapInitializeMibFsWtpModelTable(pCapwapFsWtpModelEntry)) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : CapwapInitializeFsWtpRadioTable
* Input       : pCapwapFsWtpRadioEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/

INT4 CapwapInitializeFsWtpRadioTable (tCapwapFsWtpRadioEntry *pCapwapFsWtpRadioEntry)
{
    if (pCapwapFsWtpRadioEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((CapwapInitializeMibFsWtpRadioTable (pCapwapFsWtpRadioEntry)) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}



/****************************************************************************
* Function    : CapwapInitializeFsCapwapWhiteListTable
* Input       : pCapwapFsCapwapWhiteListEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/

INT4 CapwapInitializeFsCapwapWhiteListTable (tCapwapFsCapwapWhiteListEntry *pCapwapFsCapwapWhiteListEntry)
{
	if (pCapwapFsCapwapWhiteListEntry == NULL)
	{
		return OSIX_FAILURE;
	}
	if ((CapwapInitializeMibFsCapwapWhiteListTable(pCapwapFsCapwapWhiteListEntry)) == OSIX_FAILURE)
	{
		return OSIX_FAILURE;
	}
	 return OSIX_SUCCESS;}


/****************************************************************************
* Function    : CapwapInitializeFsCapwapBlackList
* Input       : pCapwapFsCapwapBlackListEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/

INT4 CapwapInitializeFsCapwapBlackList (tCapwapFsCapwapBlackListEntry *pCapwapFsCapwapBlackListEntry)
{
	if (pCapwapFsCapwapBlackListEntry == NULL)
	{
		return OSIX_FAILURE;
	}
	if ((CapwapInitializeMibFsCapwapBlackList(pCapwapFsCapwapBlackListEntry)) == OSIX_FAILURE)
	{
		return OSIX_FAILURE;
	}
	 return OSIX_SUCCESS;}


/****************************************************************************
* Function    : CapwapInitializeFsCapwapWtpConfigTable
* Input       : pCapwapFsCapwapWtpConfigEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/

INT4 CapwapInitializeFsCapwapWtpConfigTable (tCapwapFsCapwapWtpConfigEntry *pCapwapFsCapwapWtpConfigEntry)
{
	if (pCapwapFsCapwapWtpConfigEntry == NULL)
	{
		return OSIX_FAILURE;
	}
	if ((CapwapInitializeMibFsCapwapWtpConfigTable(pCapwapFsCapwapWtpConfigEntry)) == OSIX_FAILURE)
	{
		return OSIX_FAILURE;
	}
	 return OSIX_SUCCESS;}


/****************************************************************************
* Function    : CapwapInitializeFsCapwapLinkEncryptionTable
* Input       : pCapwapFsCapwapLinkEncryptionEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/

INT4 CapwapInitializeFsCapwapLinkEncryptionTable (tCapwapFsCapwapLinkEncryptionEntry *pCapwapFsCapwapLinkEncryptionEntry)
{
	if (pCapwapFsCapwapLinkEncryptionEntry == NULL)
	{
		return OSIX_FAILURE;
	}
	if ((CapwapInitializeMibFsCapwapLinkEncryptionTable(pCapwapFsCapwapLinkEncryptionEntry)) == OSIX_FAILURE)
	{
		return OSIX_FAILURE;
	}
	 return OSIX_SUCCESS;}


/****************************************************************************
* Function    : CapwapInitializeFsCawapDefaultWtpProfileTable
* Input       : pCapwapFsCapwapDefaultWtpProfileEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/

INT4 CapwapInitializeFsCawapDefaultWtpProfileTable (tCapwapFsCapwapDefaultWtpProfileEntry *pCapwapFsCapwapDefaultWtpProfileEntry)
{
	if (pCapwapFsCapwapDefaultWtpProfileEntry == NULL)
	{
		return OSIX_FAILURE;
	}
	if ((CapwapInitializeMibFsCawapDefaultWtpProfileTable(pCapwapFsCapwapDefaultWtpProfileEntry)) == OSIX_FAILURE)
	{
		return OSIX_FAILURE;
	}
	 return OSIX_SUCCESS;}


/****************************************************************************
* Function    : CapwapInitializeFsCapwapDnsProfileTable
* Input       : pCapwapFsCapwapDnsProfileEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/

INT4 CapwapInitializeFsCapwapDnsProfileTable (tCapwapFsCapwapDnsProfileEntry *pCapwapFsCapwapDnsProfileEntry)
{
	if (pCapwapFsCapwapDnsProfileEntry == NULL)
	{
		return OSIX_FAILURE;
	}
	if ((CapwapInitializeMibFsCapwapDnsProfileTable(pCapwapFsCapwapDnsProfileEntry)) == OSIX_FAILURE)
	{
		return OSIX_FAILURE;
	}
	 return OSIX_SUCCESS;}


/****************************************************************************
* Function    : CapwapInitializeFsWtpNativeVlanIdTable
* Input       : pCapwapFsWtpNativeVlanIdTable
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/

INT4 CapwapInitializeFsWtpNativeVlanIdTable (tCapwapFsWtpNativeVlanIdTable *pCapwapFsWtpNativeVlanIdTable)
{
	if (pCapwapFsWtpNativeVlanIdTable == NULL)
	{
		return OSIX_FAILURE;
	}
	if ((CapwapInitializeMibFsWtpNativeVlanIdTable(pCapwapFsWtpNativeVlanIdTable)) == OSIX_FAILURE)
	{
		return OSIX_FAILURE;
	}
	 return OSIX_SUCCESS;}

/****************************************************************************
* Function    : CapwapInitializeFsWtpLocalRoutingTable
* Input       : pCapwapFsWtpLocalRoutingTable
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/

INT4 CapwapInitializeFsWtpLocalRoutingTable (tCapwapFsWtpLocalRoutingTable *pCapwapFsWtpLocalRoutingTable)
{
	if (pCapwapFsWtpLocalRoutingTable == NULL)
	{
		return OSIX_FAILURE;
	}
	if ((CapwapInitializeMibFsWtpLocalRoutingTable(pCapwapFsWtpLocalRoutingTable)) == OSIX_FAILURE)
	{
		return OSIX_FAILURE;
	}
	 return OSIX_SUCCESS;}


/****************************************************************************
* Function    : CapwapInitializeFsCawapDiscStatsTable
* Input       : pCapwapFsCawapDiscStatsEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/

INT4 CapwapInitializeFsCawapDiscStatsTable (tCapwapFsCawapDiscStatsEntry *pCapwapFsCawapDiscStatsEntry)
{
	if (pCapwapFsCawapDiscStatsEntry == NULL)
	{
		return OSIX_FAILURE;
	}
	if ((CapwapInitializeMibFsCawapDiscStatsTable(pCapwapFsCawapDiscStatsEntry)) == OSIX_FAILURE)
	{
		return OSIX_FAILURE;
	}
	 return OSIX_SUCCESS;}


/****************************************************************************
* Function    : CapwapInitializeFsCawapJoinStatsTable
* Input       : pCapwapFsCawapJoinStatsEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/

INT4 CapwapInitializeFsCawapJoinStatsTable (tCapwapFsCawapJoinStatsEntry *pCapwapFsCawapJoinStatsEntry)
{
	if (pCapwapFsCawapJoinStatsEntry == NULL)
	{
		return OSIX_FAILURE;
	}
	if ((CapwapInitializeMibFsCawapJoinStatsTable(pCapwapFsCawapJoinStatsEntry)) == OSIX_FAILURE)
	{
		return OSIX_FAILURE;
	}
	 return OSIX_SUCCESS;}


/****************************************************************************
* Function    : CapwapInitializeFsCawapConfigStatsTable
* Input       : pCapwapFsCawapConfigStatsEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/

INT4 CapwapInitializeFsCawapConfigStatsTable (tCapwapFsCawapConfigStatsEntry *pCapwapFsCawapConfigStatsEntry)
{
	if (pCapwapFsCawapConfigStatsEntry == NULL)
	{
		return OSIX_FAILURE;
	}
	if ((CapwapInitializeMibFsCawapConfigStatsTable(pCapwapFsCawapConfigStatsEntry)) == OSIX_FAILURE)
	{
		return OSIX_FAILURE;
	}
	 return OSIX_SUCCESS;}


/****************************************************************************
* Function    : CapwapInitializeFsCawapRunStatsTable
* Input       : pCapwapFsCawapRunStatsEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/

INT4 CapwapInitializeFsCawapRunStatsTable (tCapwapFsCawapRunStatsEntry *pCapwapFsCawapRunStatsEntry)
{
	if (pCapwapFsCawapRunStatsEntry == NULL)
	{
		return OSIX_FAILURE;
	}
	if ((CapwapInitializeMibFsCawapRunStatsTable(pCapwapFsCawapRunStatsEntry)) == OSIX_FAILURE)
	{
		return OSIX_FAILURE;
	}
	 return OSIX_SUCCESS;}


/****************************************************************************
* Function    : CapwapInitializeFsCapwapWirelessBindingTable
* Input       : pCapwapFsCapwapWirelessBindingEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/

INT4 CapwapInitializeFsCapwapWirelessBindingTable (tCapwapFsCapwapWirelessBindingEntry *pCapwapFsCapwapWirelessBindingEntry)
{
	if (pCapwapFsCapwapWirelessBindingEntry == NULL)
	{
		return OSIX_FAILURE;
	}
	if ((CapwapInitializeMibFsCapwapWirelessBindingTable(pCapwapFsCapwapWirelessBindingEntry)) == OSIX_FAILURE)
	{
		return OSIX_FAILURE;
	}
	 return OSIX_SUCCESS;}


/****************************************************************************
* Function    : CapwapInitializeFsCapwapStationWhiteList
* Input       : pCapwapFsCapwapStationWhiteListEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/

INT4 CapwapInitializeFsCapwapStationWhiteList (tCapwapFsCapwapStationWhiteListEntry *pCapwapFsCapwapStationWhiteListEntry)
{
	if (pCapwapFsCapwapStationWhiteListEntry == NULL)
	{
		return OSIX_FAILURE;
	}
	if ((CapwapInitializeMibFsCapwapStationWhiteList(pCapwapFsCapwapStationWhiteListEntry)) == OSIX_FAILURE)
	{
		return OSIX_FAILURE;
	}
	 return OSIX_SUCCESS;}


/****************************************************************************
* Function    : CapwapInitializeFsCapwapWtpRebootStatisticsTable
* Input       : pCapwapFsCapwapWtpRebootStatisticsEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/

INT4 CapwapInitializeFsCapwapWtpRebootStatisticsTable (tCapwapFsCapwapWtpRebootStatisticsEntry *pCapwapFsCapwapWtpRebootStatisticsEntry)
{
	if (pCapwapFsCapwapWtpRebootStatisticsEntry == NULL)
	{
		return OSIX_FAILURE;
	}
	if ((CapwapInitializeMibFsCapwapWtpRebootStatisticsTable(pCapwapFsCapwapWtpRebootStatisticsEntry)) == OSIX_FAILURE)
	{
		return OSIX_FAILURE;
	}
	 return OSIX_SUCCESS;}
