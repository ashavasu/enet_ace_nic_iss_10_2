/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: capwapclicfg.c,v 1.6 2018/01/04 09:54:32 siva Exp $       
 *
 * Description: This file contains the routines to update the Capwap DB.
 *********************************************************************/

#ifndef _WSSCAPWAPCFG_C_
#define _WSSCAPWAPCFG_C_

#include "wlchdlr.h"
#include "capwapinc.h"
#include "wsspm.h"
#include "wssifpmdb.h"
#include "wssifinc.h"
#include "wssifstatdfs.h"
#include "capwapcli.h"
#include "wsscfgtdfsg.h"
#ifdef SNTP_WANTED
extern UINT4        SntpTmToSec (tUtlTm *);
#endif
static UINT4        gu1IsConfigResponseReceived;

INT4                UtilGetIntfNameFromMac (tMacAddr, UINT1 *);
extern UINT1       *CfaGddGetLnxIntfnameForPort (UINT2 u2Index);
extern INT1         nmhGetFsDot1qTpFdbPort (INT4, UINT4, tMacAddr, INT4 *);

/****************************************************************************
 * Function    :  WssCapwapSetWtpProfile
 * Input       :   pWsscfgCapwapDot11WlanEntry
 *                 pWsscfgIsSetCapwapDot11WlanEntry
 * Descritpion :  This Routine checks set value and invoke corresponding
 *                module
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 ***************************************************************************/
INT4
CapwapCliSetWtpProfile (tCapwapCapwapBaseWtpProfileEntry *
                        pCapwapCapwapBaseWtpProfileEntry,
                        tCapwapIsSetCapwapBaseWtpProfileEntry *
                        pCapwapIsSetCapwapBaseWtpProfileEntry)
{
    tWssIfCapDB        *pWssIfCapDB = NULL;
    tWssIfCapDB        *pCapGetDB = NULL;
    INT4                i4RowStatus = 0;
    UINT1               au1ModelNumber[256];
    UINT4               u4NoOfRadio = 0;
    UINT1               u1Flag = 0;
#ifdef SNTP_WANTED
    tUtlTm              tm;
#endif

#ifdef WLC_WANTED
    INT4                i4RetVal = 0;
    UINT1               u1RadioIndex = 0;
    UINT1               au1IntfName[CAPWAP_INTERFACE_LEN];
    UINT1               au1IntName[CAPWAP_INTERFACE_LEN];

    MEMSET (au1IntfName, 0, CAPWAP_INTERFACE_LEN);
    MEMSET (au1IntName, 0, CAPWAP_INTERFACE_LEN);
#endif
#ifdef RFMGMT_WANTED
    tRfMgmtMsgStruct    RfMgmtMsgStruct;
    UINT1               u1Index = 0;
#endif
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;
    tSNMP_OCTET_STRING_TYPE WtpModelNumber;
    tMacAddr            MacAddrTmp;
    tRemoteSessionManager *pSessEntry = NULL;
    tRadioIfMsgStruct   WssMsgStruct;

    tSNMP_OCTET_STRING_TYPE ModelName;
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapDB, OSIX_FAILURE)
        WSS_IF_DB_TEMP_MEM_ALLOC (pCapGetDB, OSIX_FAILURE)
        MEMSET (pWssIfCapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (pCapGetDB, 0, sizeof (tWssIfCapDB));
    pWlcHdlrMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pWlcHdlrMsgStruct == NULL)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapCliSetWtpProfile:- "
                    "UtlShMemAllocWlcBuf returned failure\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
        return OSIX_FAILURE;
    }
    MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));
    MEMSET (&WtpModelNumber, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&MacAddrTmp, 0, sizeof (tMacAddr));
    MEMSET (&WssMsgStruct, 0, sizeof (tRadioIfMsgStruct));
    MEMSET (&ModelName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1ModelNumber, 0, 256);
#ifdef RFMGMT_WANTED
    MEMSET (&RfMgmtMsgStruct, 0, sizeof (tRfMgmtMsgStruct));
#endif

    if ((pCapwapCapwapBaseWtpProfileEntry == NULL) ||
        (pCapwapIsSetCapwapBaseWtpProfileEntry == NULL))
    {
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
        return OSIX_FAILURE;
    }

    pWssIfCapDB->CapwapIsGetAllDB.bWtpProfileId = OSIX_TRUE;
    pWssIfCapDB->CapwapGetDB.u2ProfileId =
        (UINT2) (pCapwapCapwapBaseWtpProfileEntry->MibObject.
                 u4CapwapBaseWtpProfileId);

    i4RowStatus = pCapwapCapwapBaseWtpProfileEntry->MibObject.
        i4CapwapBaseWtpProfileRowStatus;

    if (pCapwapIsSetCapwapBaseWtpProfileEntry->bCapwapBaseWtpProfileRowStatus
        != OSIX_FALSE)
    {
        switch (i4RowStatus)
        {
            case NOT_READY:
                /* Check for create */
                if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_CREATE_PROFILE_ENTRY,
                                             pWssIfCapDB) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "CapwapCliSetWtpProfile :CAPWAP DB access "
                                "failed.\r\n");
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
                    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                    return OSIX_FAILURE;
                }

                if (CapwapCreateProfileIdDB
                    (pCapwapCapwapBaseWtpProfileEntry->MibObject.
                     u4CapwapBaseWtpProfileId) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "CapwapCreateProfileIdDB Failed \r\n");
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
                    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                    return OSIX_FAILURE;
                }
                break;

            case NOT_IN_SERVICE:
                if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_INTERNAL_ID,
                                             pWssIfCapDB) != OSIX_SUCCESS)
                {
                    break;
                }
                /* break; */
            case DESTROY:
                MEMCPY (pWssIfCapDB->CapwapGetDB.au1ProfileName,
                        pCapwapCapwapBaseWtpProfileEntry->MibObject.
                        au1CapwapBaseWtpProfileName,
                        STRLEN (pCapwapCapwapBaseWtpProfileEntry->MibObject.
                                au1CapwapBaseWtpProfileName));

                pWssIfCapDB->CapwapGetDB.u2ProfileId =
                    (UINT2) (pCapwapCapwapBaseWtpProfileEntry->MibObject.
                             u4CapwapBaseWtpProfileId);
                pWssIfCapDB->CapwapIsGetAllDB.bWtpProfileId = OSIX_TRUE;
                pWssIfCapDB->CapwapIsGetAllDB.bRowStatus = OSIX_TRUE;
                pWssIfCapDB->CapwapGetDB.u1RowStatus = (UINT1) i4RowStatus;

                if (i4RowStatus == DESTROY)
                {
                    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_INTERNAL_ID,
                                                 pWssIfCapDB) == OSIX_FAILURE)
                    {
                        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                    "Getting Internal Id Failed\n");
                    }

                    if (CapwapDeleteProfileIdDB
                        (pCapwapCapwapBaseWtpProfileEntry->MibObject.
                         u4CapwapBaseWtpProfileId) != OSIX_SUCCESS)
                    {
                        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                    "CapwapDeleteProfileIdDB Failed \r\n");
                    }
                }
#ifdef RFMGMT_WANTED
                else
                {
                    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_INTERNAL_ID,
                                                 pWssIfCapDB) == OSIX_SUCCESS)
                    {

                        pWssIfCapDB->CapwapIsGetAllDB.bWtpInternalId =
                            OSIX_TRUE;
                        pWssIfCapDB->CapwapIsGetAllDB.bNoOfRadio = OSIX_TRUE;
                        /*Fetch the number of radios from capwap DB */
                        if (WssIfProcessCapwapDBMsg
                            (WSS_CAPWAP_GET_DB, pWssIfCapDB) == OSIX_SUCCESS)
                        {

                            for (u1Index = 1;
                                 u1Index <=
                                 pWssIfCapDB->CapwapGetDB.u1WtpNoofRadio;
                                 u1Index++)
                            {
                                pWssIfCapDB->CapwapIsGetAllDB.bRadioIfDB =
                                    OSIX_TRUE;
                                pWssIfCapDB->CapwapGetDB.u1RadioId = u1Index;
                                if (WssIfProcessCapwapDBMsg
                                    (WSS_CAPWAP_GET_DB,
                                     pWssIfCapDB) == OSIX_SUCCESS)
                                {
                                    RfMgmtMsgStruct.unRfMgmtMsg.
                                        RfMgmtIntfConfigReq.u4RadioIfIndex =
                                        pWssIfCapDB->CapwapGetDB.u4IfIndex;

                                    if (WssIfProcessRfMgmtMsg
                                        (RFMGMT_PROFILE_DISABLE_MSG,
                                         &RfMgmtMsgStruct) != OSIX_SUCCESS)
                                    {
                                        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                                    "Profile disable notification to RF module failed\n");

                                    }
                                }

                            }
                        }
                        else
                        {

                            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                        "Failed to Retrieve the Number of Radios \r\n");
                        }
                    }
                    else
                    {

                        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                    "Getting Internal Id Failed\n");
                    }
                }
#endif
                if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_RSM_FROM_INDEX,
                                             pWssIfCapDB) == OSIX_FAILURE)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to retrive "
                                "CAPWAP DB \r\n");
                }
                pSessEntry = pWssIfCapDB->pSessEntry;
                if ((pSessEntry != NULL) &&
                    (pSessEntry->eCurrentState == CAPWAP_RUN))
                {
                    CapwapStopAllSessionTimers (pSessEntry);

                    CAPWAP_TRC (CAPWAP_FSM_TRC,
                                "Entered CAPWAP DTLSTD State \r\n");
                    pSessEntry->eCurrentState = CAPWAP_DTLSTD;

                    if (CapwapShutDownDTLS (pSessEntry) == OSIX_FAILURE)
                    {
                        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Disconnect "
                                    "the Session \r\n");
                    }

                    MEMCPY (&pWssIfCapDB->JoinSessionID,
                            &pSessEntry->JoinSessionID, MAC_LENGTH);

                    if (WssIfProcessCapwapDBMsg
                        (WSS_CAPWAP_DELETE_RSM_FROM_SESSIONID,
                         pWssIfCapDB) == OSIX_FAILURE)
                    {
                        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                    "Failed to Process CAPWAP DB Message \r\n");
                    }

                    if (WssIfProcessCapwapDBMsg
                        (WSS_CAPWAP_DELETE_SESSPROFID_ENTRY,
                         pWssIfCapDB) == OSIX_FAILURE)
                    {
                        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                    "Failed to Process CAPWAP DB Message \r\n");
                    }

                    /* delete the data port session-port entries also */
                    pWssIfCapDB->u4DestIp = pSessEntry->remoteIpAddr.u4_addr[0];
                    pWssIfCapDB->u4DestPort = pSessEntry->u4RemoteDataPort;

                    if (WssIfProcessCapwapDBMsg
                        (WSS_CAPWAP_GET_SESSION_PORT_ENTRY,
                         pWssIfCapDB) == OSIX_FAILURE)
                    {
                        CAPWAP_TRC (CAPWAP_FSM_TRC,
                                    "Failed to Retrieve SESS Port\r\n");
                    }

                    if (pWssIfCapDB->pSessDestIpEntry != NULL)
                    {
                        if (WssIfProcessCapwapDBMsg
                            (WSS_CAPWAP_DELETE_SESSION_PORT_ENTRY,
                             pWssIfCapDB) == OSIX_FAILURE)
                        {
                            CAPWAP_TRC (CAPWAP_FSM_TRC,
                                        "Failed to Delete SESS Port\r\n");
                        }
                    }

                    pWssIfCapDB->u4DestIp = pSessEntry->remoteIpAddr.u4_addr[0];
                    pWssIfCapDB->u4DestPort = pSessEntry->u4RemoteCtrlPort;

                    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_DELETE_RSM,
                                                 pWssIfCapDB) == OSIX_FAILURE)
                    {
                        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                    "Failed to Process CAPWAP DB Message \r\n");
                    }
                    CAPWAP_TRC (CAPWAP_FSM_TRC,
                                "Entered CAPWAP IDLE State \r\n");
                    pSessEntry->eCurrentState = CAPWAP_IDLE;
                }

                if (i4RowStatus == DESTROY)
                {
                    if (WssIfProcessCapwapDBMsg
                        (WSS_CAPWAP_DELETE_PROFILE_MAPPING_ENTRY,
                         pWssIfCapDB) != OSIX_SUCCESS)
                    {
                        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                    "CapwapSetWtpProfile :CAPWAP DB access "
                                    "failed. \r\n");
                    }

                    StrToMac (pCapwapCapwapBaseWtpProfileEntry->MibObject.
                              au1CapwapBaseWtpProfileWtpMacAddress,
                              pWssIfCapDB->CapwapGetDB.WtpMacAddress);
                    if (WssIfProcessCapwapDBMsg
                        (WSS_CAPWAP_DELETE_MACADDR_MAPPING_ENTRY,
                         pWssIfCapDB) != OSIX_SUCCESS)
                    {
                        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                    "CapwapSetWtpProfile :CAPWAP DB access "
                                    "failed. \r\n");
                    }

                    if (WssIfProcessCapwapDBMsg
                        (WSS_CAPWAP_DESTROY_PROFILE_ENTRY,
                         pWssIfCapDB) != OSIX_SUCCESS)
                    {
                        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                    "CapwapSetWtpProfile :CAPWAP "
                                    "DB access failed. \r\n");
                    }
                }
                else
                {
                    break;
                }
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
                UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                return OSIX_SUCCESS;

            case ACTIVE:
#ifdef WTP_WANTED
                /* Check for create */
                if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_CREATE_PROFILE_ENTRY,
                                             pWssIfCapDB) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "CapwapCliSetWtpProfile :CAPWAP DB access "
                                "failed.\r\n");
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
                    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                    return OSIX_FAILURE;
                }

                if (CapwapCreateProfileIdDB
                    (pCapwapCapwapBaseWtpProfileEntry->MibObject.
                     u4CapwapBaseWtpProfileId) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "CapwapCreateProfileIdDB Failed \r\n");
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
                    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                    return OSIX_FAILURE;
                }
#endif
                if ((pCapwapCapwapBaseWtpProfileEntry->MibObject.
                     au1CapwapBaseWtpProfileWtpMacAddress[0] == '\0')
                    || (pCapwapCapwapBaseWtpProfileEntry->MibObject.
                        au1CapwapBaseWtpProfileName[0] == '\0')
                    || (pCapwapCapwapBaseWtpProfileEntry->MibObject.
                        au1CapwapBaseWtpProfileWtpName[0] == '\0')
                    || (pCapwapCapwapBaseWtpProfileEntry->MibObject.
                        au1CapwapBaseWtpProfileWtpLocation[0] == '\0')
                    || (pCapwapCapwapBaseWtpProfileEntry->MibObject.
                        au1CapwapBaseWtpProfileWtpModelNumber[0] == '\0'))
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "CapwapSetWtpProfile : Mandatory arguments "
                                "not present \r\n");
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
                    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                    return OSIX_FAILURE;
                }
                break;
            default:
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Default case \r\n");
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
                UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                return OSIX_FAILURE;
                break;

        }
    }
    /* WTP Profile Name */
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->bCapwapBaseWtpProfileName !=
        OSIX_FALSE)
    {
        pWssIfCapDB->CapwapIsGetAllDB.bProfileName = OSIX_TRUE;
        MEMCPY (pWssIfCapDB->CapwapGetDB.au1ProfileName,
                pCapwapCapwapBaseWtpProfileEntry->MibObject.
                au1CapwapBaseWtpProfileName,
                STRLEN (pCapwapCapwapBaseWtpProfileEntry->MibObject.
                        au1CapwapBaseWtpProfileName));

        /* Create Profile name to Profile Id mapping entry */
        if (WssIfProcessCapwapDBMsg
            (WSS_CAPWAP_CREATE_PROFILE_MAPPING_ENTRY,
             pWssIfCapDB) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "CapwapCliSetWtpProfile : Profile Name mapping creation failed. \r\n");

            pWssIfCapDB->CapwapIsGetAllDB.bWtpProfileId = OSIX_TRUE;
            pWssIfCapDB->CapwapGetDB.u2ProfileId =
                (UINT2) (pCapwapCapwapBaseWtpProfileEntry->MibObject.
                         u4CapwapBaseWtpProfileId);

            StrToMac (pCapwapCapwapBaseWtpProfileEntry->MibObject.
                      au1CapwapBaseWtpProfileWtpMacAddress,
                      pWssIfCapDB->CapwapGetDB.WtpMacAddress);
            pWssIfCapDB->CapwapIsGetAllDB.bWtpMacAddress = OSIX_TRUE;

            if (WssIfProcessCapwapDBMsg
                (WSS_CAPWAP_DELETE_MACADDR_MAPPING_ENTRY,
                 pWssIfCapDB) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "CapwapSetWtpProfile :MAC Address mapping deletion failed. \r\n");
            }
            if (CapwapDeleteProfileIdDB (pCapwapCapwapBaseWtpProfileEntry->
                                         MibObject.u4CapwapBaseWtpProfileId) !=
                OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "CapwapDeleteProfileIdDB Failed \r\n");
            }
            /* Revert the DB created for WTP Profile Id */
            if (WssIfProcessCapwapDBMsg
                (WSS_CAPWAP_DESTROY_PROFILE_ENTRY, pWssIfCapDB) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "CapwapSetWtpProfile :CAPWAP DB access failed. \r\n");
            }

            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            return OSIX_FAILURE;
        }
    }

    /* WTP Mac Address */
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpMacAddress != OSIX_FALSE)
    {
        pWssIfCapDB->CapwapGetDB.u2WtpProfileId =
            (UINT2) (pCapwapCapwapBaseWtpProfileEntry->MibObject.
                     u4CapwapBaseWtpProfileId);
        pWssIfCapDB->CapwapIsGetAllDB.bCapwapState = OSIX_TRUE;
        pWssIfCapDB->CapwapIsGetAllDB.bWtpMacAddress = OSIX_TRUE;

        /*Since WSS_CAPWAP_GET_DB is common to all the boolean for pWssIfCapDB,
         * hence disabling bProfileName and resetting it to original value after
         * WSS_CAPWAP_GET_DB function ends*/
        u1Flag = pWssIfCapDB->CapwapIsGetAllDB.bProfileName;
        pWssIfCapDB->CapwapIsGetAllDB.bProfileName = OSIX_FALSE;
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapDB) ==
            OSIX_SUCCESS)
        {
#ifdef WTP_WANTED
            StrToMac (pCapwapCapwapBaseWtpProfileEntry->MibObject.
                      au1CapwapBaseWtpProfileWtpMacAddress, MacAddrTmp);
#endif
            if (MEMCMP (pWssIfCapDB->CapwapGetDB.WtpMacAddress, MacAddrTmp, 6)
                != 0)
            {
                if (WssIfProcessCapwapDBMsg
                    (WSS_CAPWAP_SHOW_WTPSTATE_TABLE,
                     pWssIfCapDB) == OSIX_SUCCESS)
                {
                    if (pWssIfCapDB->CapwapGetDB.u4CapwapState == CAPWAP_RUN)
                    {
                        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                    "CapwapCliSetWtpProfile : Wtp in Run State, Configuration failed \r\n");
                        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                        WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
                        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                        return OSIX_FAILURE;
                    }
                }
                if (WssIfProcessCapwapDBMsg
                    (WSS_CAPWAP_DELETE_MACADDR_MAPPING_ENTRY,
                     pWssIfCapDB) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "CapwapCliSetWtpProfile : MAC Address mapping deletion(Previous mac) failed. \r\n");
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
                    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                    return OSIX_FAILURE;
                }
            }
        }

        StrToMac (pCapwapCapwapBaseWtpProfileEntry->MibObject.
                  au1CapwapBaseWtpProfileWtpMacAddress,
                  pWssIfCapDB->CapwapGetDB.WtpMacAddress);

        /* Create MAC Address to Profile Id mapping entry */
        if (WssIfProcessCapwapDBMsg
            (WSS_CAPWAP_CREATE_MACADDR_MAPPING_ENTRY,
             pWssIfCapDB) != OSIX_SUCCESS)
        {
            pWssIfCapDB->CapwapIsGetAllDB.bWtpProfileId = OSIX_TRUE;
            pWssIfCapDB->CapwapGetDB.u2ProfileId =
                (UINT2) (pCapwapCapwapBaseWtpProfileEntry->MibObject.
                         u4CapwapBaseWtpProfileId);
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "CapwapCliSetWtpProfile : MAC Address mapping creation failed. \r\n");

            MEMCPY (pWssIfCapDB->CapwapGetDB.au1ProfileName,
                    pCapwapCapwapBaseWtpProfileEntry->MibObject.
                    au1CapwapBaseWtpProfileName,
                    STRLEN (pCapwapCapwapBaseWtpProfileEntry->MibObject.
                            au1CapwapBaseWtpProfileName));

            if (WssIfProcessCapwapDBMsg
                (WSS_CAPWAP_DELETE_PROFILE_MAPPING_ENTRY,
                 pWssIfCapDB) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "CapwapSetWtpProfile :Profile name mapping deletion failed. \r\n");
            }

            if (CapwapDeleteProfileIdDB (pCapwapCapwapBaseWtpProfileEntry->
                                         MibObject.u4CapwapBaseWtpProfileId) !=
                OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "CapwapDeleteProfileIdDB Failed \r\n");
            }
            /* Revert the DB created for WTP Profile Id */
            if (WssIfProcessCapwapDBMsg
                (WSS_CAPWAP_DESTROY_PROFILE_ENTRY, pWssIfCapDB) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "CapwapSetWtpProfile :CAPWAP DB access failed. \r\n");
            }
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            return OSIX_FAILURE;
        }
#ifdef WLC_WANTED
        if (UtilGetIntfNameFromMac (pWssIfCapDB->CapwapGetDB.WtpMacAddress,
                                    au1IntfName) == OSIX_SUCCESS)
        {
            if (MEMCMP (au1IntfName, au1IntName, CAPWAP_INTERFACE_LEN) != 0)
            {
                MEMCPY (pWssIfCapDB->CapwapGetDB.au1IntfName,
                        au1IntfName, CAPWAP_INTERFACE_LEN);
                pWssIfCapDB->CapwapIsGetAllDB.bIntfName = OSIX_TRUE;
            }
        }
#endif
        pWssIfCapDB->CapwapIsGetAllDB.bProfileName = u1Flag;
    }
#ifdef WLC_WANTED
    /* WTP Model Number */
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpModelNumber != OSIX_FALSE)
    {
        WtpModelNumber.pu1_OctetList =
            pCapwapCapwapBaseWtpProfileEntry->MibObject.
            au1CapwapBaseWtpProfileWtpModelNumber;
        WtpModelNumber.i4_Length =
            (INT4) (STRLEN (pCapwapCapwapBaseWtpProfileEntry->MibObject.
                            au1CapwapBaseWtpProfileWtpModelNumber));

        if (nmhGetFsWtpModelRowStatus (&WtpModelNumber, &i4RowStatus) !=
            SNMP_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "CapwapCliSetWtpProfile : Unknown Model number received. \r\n");
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            return OSIX_FAILURE;
        }
        if (i4RowStatus != ACTIVE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "CapwapCliSetWtpProfile : Model number inactive. \r\n");
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            return OSIX_FAILURE;
        }

        pWssIfCapDB->CapwapIsGetAllDB.bWtpModelNumber = OSIX_TRUE;
        MEMCPY (pWssIfCapDB->CapwapGetDB.au1WtpModelNumber,
                pCapwapCapwapBaseWtpProfileEntry->MibObject.
                au1CapwapBaseWtpProfileWtpModelNumber,
                STRLEN (pCapwapCapwapBaseWtpProfileEntry->MibObject.
                        au1CapwapBaseWtpProfileWtpModelNumber));
    }
#endif
    /* WTP Name */
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->bCapwapBaseWtpProfileWtpName !=
        OSIX_FALSE)
    {
        pWssIfCapDB->CapwapIsGetAllDB.bWtpName = OSIX_TRUE;
        MEMCPY (pWssIfCapDB->CapwapGetDB.au1WtpName,
                pCapwapCapwapBaseWtpProfileEntry->MibObject.
                au1CapwapBaseWtpProfileWtpName,
                STRLEN (pCapwapCapwapBaseWtpProfileEntry->MibObject.
                        au1CapwapBaseWtpProfileWtpName));

#ifdef WLC_WANTED
        /* Construct Config Update request message to be send to WTP when there
         * is change in WTP Name */
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.u2InternalId =
            pWssIfCapDB->CapwapGetDB.u2WtpInternalId;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.wtpName.isOptional = OSIX_TRUE;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.wtpName.u2MsgEleType =
            CAPWAP_WTP_NAME_MSG_TYPE;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.wtpName.u2MsgEleLen =
            (UINT2) (4 + STRLEN (pWssIfCapDB->CapwapGetDB.au1WtpName));

        MEMCPY (pWlcHdlrMsgStruct->CapwapConfigUpdateReq.wtpName.wtpName,
                pWssIfCapDB->CapwapGetDB.au1WtpName,
                STRLEN (pWssIfCapDB->CapwapGetDB.au1WtpName));

        /* Config Update Request message should be sent only when the row status
         * is ACTIVE. Otherwise update DB */
        if (pCapwapCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileRowStatus == ACTIVE)
        {
            i4RetVal = CapwapServiceMainTaskLock ();
            if (i4RetVal == OSIX_FAILURE)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "CapwapServiceMainTaskLock: Failed to obtain lock \r\n");
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
                UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                return OSIX_FAILURE;
            }
            i4RetVal =
                WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_CAPWAP_CONF_UPDATE_REQ,
                                        pWlcHdlrMsgStruct);
            if (i4RetVal == CAPWAP_NO_AP_PRESENT)
            {
                gu1IsConfigResponseReceived = CAPWAP_NO_AP_PRESENT;
            }
            else if (i4RetVal != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "CapwapCliSetWtpProfile : Sending Request to WLC Handler failed \r\n");
                i4RetVal = CapwapServiceMainTaskUnLock ();
                if (i4RetVal == OSIX_FAILURE)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "CapwapServiceMainTaskLock: Failed to obtain lock \r\n");
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
                    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                    return OSIX_FAILURE;
                }
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
                UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                return OSIX_FAILURE;
            }

            while (1)
            {
                if ((gu1IsConfigResponseReceived == OSIX_SUCCESS)
                    || (gu1IsConfigResponseReceived == CAPWAP_NO_AP_PRESENT))
                {
                    i4RetVal = CapwapServiceMainTaskUnLock ();
                    if (i4RetVal == OSIX_FAILURE)
                    {
                        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                    "CapwapServiceMainTaskLock: Failed to obtain lock \r\n");
                        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                        WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
                        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                        return OSIX_FAILURE;
                    }
                    break;

                }
                else if ((gu1IsConfigResponseReceived ==
                          CAPWAP_RESPONSE_TIMEOUT)
                         || (gu1IsConfigResponseReceived == OSIX_FAILURE))
                {
                    i4RetVal = CapwapServiceMainTaskUnLock ();
                    if (i4RetVal == OSIX_FAILURE)
                    {
                        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                    "CapwapServiceMainTaskLock: Failed to obtain lock \r\n");
                        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                        WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
                        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                        return OSIX_FAILURE;
                    }
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "CapwapCliSetWtpProfile : Negative response received\r\n");
                    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
                    return OSIX_FAILURE;
                }
            }
        }
#endif
    }
    /* WTP Location */
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpLocation != OSIX_FALSE)
    {
        if (STRLEN (pCapwapCapwapBaseWtpProfileEntry->MibObject.
                    au1CapwapBaseWtpProfileWtpLocation) > CAPWAP_WTP_NAME_SIZE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Length of LOCATION is greater than 512\r\n");
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            return OSIX_FAILURE;
        }

        pWssIfCapDB->CapwapIsGetAllDB.bWtpLocation = OSIX_TRUE;
        MEMSET (pWssIfCapDB->CapwapGetDB.au1WtpLocation, 0,
                CAPWAP_WTP_NAME_SIZE);

        MEMCPY (pWssIfCapDB->CapwapGetDB.au1WtpLocation,
                pCapwapCapwapBaseWtpProfileEntry->MibObject.
                au1CapwapBaseWtpProfileWtpLocation,
                /*                STRLEN(pCapwapCapwapBaseWtpProfileEntry->\
                   MibObject.au1CapwapBaseWtpProfileWtpLocation)); */
                /* Coverity Fix - 20/Jun/2013 */
                /* Added range check above to fix the coverity issue */
                STRLEN (pCapwapCapwapBaseWtpProfileEntry->MibObject.
                        au1CapwapBaseWtpProfileWtpLocation));
        /* Coverity Fix - 20/Jun/2013 */

#ifdef WLC_WANTED
        /* Construct Config Update request message to be send to WTP when there
         * is change in WTP Location */
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.u2InternalId =
            pWssIfCapDB->CapwapGetDB.u2WtpInternalId;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.wtpLocation.isOptional =
            OSIX_TRUE;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.wtpLocation.u2MsgEleType =
            CAPWAP_WTP_LOCATION_MSG_TYPE;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.wtpLocation.u2MsgEleLen =
            (UINT2) (STRLEN (pWssIfCapDB->CapwapGetDB.au1WtpLocation));

        MEMCPY (pWlcHdlrMsgStruct->CapwapConfigUpdateReq.wtpLocation.value,
                pWssIfCapDB->CapwapGetDB.au1WtpLocation,
                STRLEN (pWssIfCapDB->CapwapGetDB.au1WtpLocation));

        if (pCapwapCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileRowStatus == ACTIVE)
        {
            i4RetVal = CapwapServiceMainTaskLock ();
            if (i4RetVal == OSIX_FAILURE)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "CapwapServiceMainTaskLock: Failed to obtain lock \r\n");
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
                UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                return OSIX_FAILURE;
            }
            i4RetVal =
                WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_CAPWAP_CONF_UPDATE_REQ,
                                        pWlcHdlrMsgStruct);
            if (i4RetVal == CAPWAP_NO_AP_PRESENT)
            {
                gu1IsConfigResponseReceived = CAPWAP_NO_AP_PRESENT;
            }
            else if (i4RetVal != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "CapwapCliSetWtpProfile : Sending Request to WLC Handler failed \r\n");
                i4RetVal = CapwapServiceMainTaskUnLock ();
                if (i4RetVal == OSIX_FAILURE)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "CapwapServiceMainTaskLock: Failed to obtain lock \r\n");
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
                    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                    return OSIX_FAILURE;
                }
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
                UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                return OSIX_FAILURE;
            }

            while (1)
            {
                if ((gu1IsConfigResponseReceived == OSIX_SUCCESS)
                    || (gu1IsConfigResponseReceived == CAPWAP_NO_AP_PRESENT))
                {
                    i4RetVal = CapwapServiceMainTaskUnLock ();
                    if (i4RetVal == OSIX_FAILURE)
                    {
                        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                    "CapwapServiceMainTaskLock: Failed to obtain lock \r\n");
                        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                        WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
                        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                        return OSIX_FAILURE;
                    }
                    break;

                }
                else if ((gu1IsConfigResponseReceived ==
                          CAPWAP_RESPONSE_TIMEOUT)
                         || (gu1IsConfigResponseReceived == OSIX_FAILURE))
                {
                    i4RetVal = CapwapServiceMainTaskUnLock ();
                    if (i4RetVal == OSIX_FAILURE)
                    {
                        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                    "CapwapServiceMainTaskLock: Failed to obtain lock \r\n");
                        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                        WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
                        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                        return OSIX_FAILURE;
                    }
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "CapwapCliSetWtpProfile : Negative response received\r\n");
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
                    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                    return OSIX_FAILURE;
                }
            }
        }
#endif
    }
    /* WTP StaticIp Info */
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpStaticIpEnable != OSIX_FALSE
        || pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpStaticIpType != OSIX_FALSE
        || pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpStaticIpAddress != OSIX_FALSE
        || pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpNetmask != OSIX_FALSE
        || pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpGateway != OSIX_FALSE)
    {
        /* Static IP Enable */
        pWssIfCapDB->CapwapIsGetAllDB.bWtpStaticIpEnable = OSIX_TRUE;
        pWssIfCapDB->CapwapGetDB.u1WtpStaticIpEnable =
            (UINT1) pCapwapCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileWtpStaticIpEnable;

        /* Static Type */
        pWssIfCapDB->CapwapIsGetAllDB.bWtpStaticIpType = OSIX_TRUE;
        pWssIfCapDB->CapwapGetDB.u1WtpStaticIpType =
            (UINT1) pCapwapCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileWtpStaticIpType;
        pWssIfCapDB->CapwapIsGetAllDB.bWtpStaticIpAddress = OSIX_TRUE;
        MEMCPY (pWssIfCapDB->CapwapGetDB.WtpStaticIpAddress.u4_addr,
                pCapwapCapwapBaseWtpProfileEntry->MibObject.
                au1CapwapBaseWtpProfileWtpStaticIpAddress, 4);
        pWssIfCapDB->CapwapIsGetAllDB.bWtpNetmask = OSIX_TRUE;
        MEMCPY (pWssIfCapDB->CapwapGetDB.WtpNetmask.u4_addr,
                pCapwapCapwapBaseWtpProfileEntry->MibObject.
                au1CapwapBaseWtpProfileWtpNetmask, 4);
        pWssIfCapDB->CapwapIsGetAllDB.bWtpGateway = OSIX_TRUE;
        MEMCPY (&pWssIfCapDB->CapwapGetDB.WtpGateway.u4_addr,
                pCapwapCapwapBaseWtpProfileEntry->MibObject.
                au1CapwapBaseWtpProfileWtpGateway, 4);
#ifdef WLC_WANTED
        /* Construct Config Update request message to be send to WTP when there
         * is change in WTP Name */
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.u2InternalId =
            pWssIfCapDB->CapwapGetDB.u2WtpInternalId;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.wtpStaticIpAddr.isOptional =
            OSIX_TRUE;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.wtpStaticIpAddr.u2MsgEleType =
            CAPWAP_WTP_STATIC_IP_INFO_MSG_TYPE;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.wtpStaticIpAddr.u2MsgEleLen =
            17;

        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.wtpStaticIpAddr.bStaticIpBool =
            (BOOL1) pWssIfCapDB->CapwapGetDB.u1WtpStaticIpEnable;

        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.wtpStaticIpAddr.u4IpAddr =
            pWssIfCapDB->CapwapGetDB.WtpStaticIpAddress.u4_addr[0];

        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.wtpStaticIpAddr.u4IpNetMask =
            pWssIfCapDB->CapwapGetDB.WtpNetmask.u4_addr[0];

        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.wtpStaticIpAddr.u4IpGateway =
            pWssIfCapDB->CapwapGetDB.WtpGateway.u4_addr[0];

        if (pCapwapCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileRowStatus == ACTIVE)
        {
            i4RetVal = CapwapServiceMainTaskLock ();
            if (i4RetVal == OSIX_FAILURE)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "CapwapServiceMainTaskLock: Failed to obtain lock \r\n");
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
                UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                return OSIX_FAILURE;
            }

            i4RetVal =
                WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_CAPWAP_CONF_UPDATE_REQ,
                                        pWlcHdlrMsgStruct);
            if (i4RetVal == CAPWAP_NO_AP_PRESENT)
            {
                gu1IsConfigResponseReceived = CAPWAP_NO_AP_PRESENT;
            }
            else if (i4RetVal != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "CapwapCliSetWtpProfile : Sending Request to WLC Handler failed \r\n");

                i4RetVal = CapwapServiceMainTaskUnLock ();
                if (i4RetVal == OSIX_FAILURE)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "CapwapServiceMainTaskLock: Failed to obtain lock \r\n");
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
                    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                    return OSIX_FAILURE;
                }
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
                UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                return OSIX_FAILURE;
            }

            while (1)
            {
                if ((gu1IsConfigResponseReceived == OSIX_SUCCESS)
                    || (gu1IsConfigResponseReceived == CAPWAP_NO_AP_PRESENT))
                {
                    i4RetVal = CapwapServiceMainTaskUnLock ();
                    if (i4RetVal == OSIX_FAILURE)
                    {
                        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                    "CapwapServiceMainTaskLock: Failed to obtain lock \r\n");
                        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                        WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
                        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                        return OSIX_FAILURE;
                    }
                    break;

                }
                else if ((gu1IsConfigResponseReceived ==
                          CAPWAP_RESPONSE_TIMEOUT)
                         || (gu1IsConfigResponseReceived == OSIX_FAILURE))
                {
                    i4RetVal = CapwapServiceMainTaskUnLock ();
                    if (i4RetVal == OSIX_FAILURE)
                    {
                        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                    "CapwapServiceMainTaskLock: Failed to obtain lock \r\n");
                        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                        WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
                        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                        return OSIX_FAILURE;
                    }
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "CapwapCliSetWtpProfile : Negative response received\r\n");
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
                    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                    return OSIX_FAILURE;
                }
            }
        }
#endif
    }
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->bFsCapwapFsAcTimestampTrigger !=
        OSIX_FALSE)
    {
        if (pCapwapCapwapBaseWtpProfileEntry->MibObject.
            i4FsCapwapFsAcTimestampTrigger == CLI_FSCAPWAP_ENABLE)
        {
            /* Get Internal Id */
            pWssIfCapDB->CapwapIsGetAllDB.bRowStatus = OSIX_FALSE;
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_INTERNAL_ID,
                                         pWssIfCapDB) == OSIX_FAILURE)
            {
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
                UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                return OSIX_FAILURE;
            }
            pWlcHdlrMsgStruct->ClkiwfConfigUpdateReq.
                acTimestamp.isOptional = OSIX_TRUE;
            pWlcHdlrMsgStruct->ClkiwfConfigUpdateReq.acTimestamp.
                u2MsgEleType = CAPWAP_AC_TIMESTAMP_MSG_TYPE;
            pWlcHdlrMsgStruct->ClkiwfConfigUpdateReq.acTimestamp.
                u2MsgEleLen = 4;
            pWlcHdlrMsgStruct->ClkiwfConfigUpdateReq.u4SessId =
                pWssIfCapDB->CapwapGetDB.u2WtpInternalId;
#ifdef SNTP_WANTED
#ifdef CLKIWF_WANTED
            /*Call ClkIwf API to SetTime */
            if (ClkIwfGetClock (CLK_PROTO_SYS, (VOID *) &tm) == OSIX_FAILURE)
            {

                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
                UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);

                return OSIX_FAILURE;
            }
#else
            UtlGetTime (&tm);
#endif
            pWlcHdlrMsgStruct->ClkiwfConfigUpdateReq.acTimestamp.u4Timestamp =
                SntpTmToSec (&tm);
#endif

            if (nmhSetFsCapwapFsAcTimestampTrigger
                (pCapwapCapwapBaseWtpProfileEntry->MibObject.
                 u4CapwapBaseWtpProfileId, CLI_FSCAPWAP_ENABLE) != SNMP_SUCCESS)
            {

                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "\nTimestamp trigerred successfully."
                            "Unable to RESET AcTimeStamp Trigger\n");

                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
                UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                return OSIX_FAILURE;
            }
#ifdef WLCHDLR_WANTED
            if (WssIfProcessWlcHdlrMsg
                (WSS_WLCHDLR_CLKIWF_CONF_UPDATE_REQ,
                 pWlcHdlrMsgStruct) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "\nNo AP Present during WTP timestamp Updation\n");

                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
                UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                return OSIX_FAILURE;

            }
#endif

        }
    }

    /* WTP Static Ip Address Type */
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpStaticIpType != OSIX_FALSE)
    {
        pWssIfCapDB->CapwapIsGetAllDB.bWtpStaticIpType = OSIX_TRUE;
        pWssIfCapDB->CapwapGetDB.u1WtpStaticIpType =
            (UINT1) pCapwapCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileWtpStaticIpType;
    }
    /* WTP Static Ip Address */
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpStaticIpAddress != OSIX_FALSE)
    {
        pWssIfCapDB->CapwapIsGetAllDB.bWtpStaticIpAddress = OSIX_TRUE;
        MEMCPY (pWssIfCapDB->CapwapGetDB.WtpStaticIpAddress.u4_addr,
                pCapwapCapwapBaseWtpProfileEntry->MibObject.
                au1CapwapBaseWtpProfileWtpStaticIpAddress, 4);
    }

    /* WTP Netmask */
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpNetmask != OSIX_FALSE)
    {
        pWssIfCapDB->CapwapIsGetAllDB.bWtpNetmask = OSIX_TRUE;
        MEMCPY (pWssIfCapDB->CapwapGetDB.WtpNetmask.u4_addr,
                pCapwapCapwapBaseWtpProfileEntry->MibObject.
                au1CapwapBaseWtpProfileWtpNetmask, 4);
    }

    /* WTP Gateway */
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpGateway != OSIX_FALSE)
    {
        pWssIfCapDB->CapwapIsGetAllDB.bWtpGateway = OSIX_TRUE;
        MEMCPY (&pWssIfCapDB->CapwapGetDB.WtpGateway.u4_addr,
                pCapwapCapwapBaseWtpProfileEntry->MibObject.
                au1CapwapBaseWtpProfileWtpGateway, 4);
    }

    /* WTP Fallback Enable */
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpFallbackEnable != OSIX_FALSE)
    {
        pWssIfCapDB->CapwapIsGetAllDB.bWtpFallbackEnable = OSIX_TRUE;
        pWssIfCapDB->CapwapGetDB.u1WtpFallbackEnable =
            (UINT1) pCapwapCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileWtpFallbackEnable;

#ifdef WLC_WANTED
        /* Construct Config Update request message to be send to WTP when there
         * is change in WTP Name */
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.u2InternalId =
            pWssIfCapDB->CapwapGetDB.u2WtpInternalId;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.wtpFallback.isOptional =
            OSIX_TRUE;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.wtpFallback.u2MsgEleType =
            CAPWAP_WTP_FALLBACK_MSG_TYPE;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.wtpFallback.u2MsgEleLen = 5;

        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.wtpFallback.mode =
            pWssIfCapDB->CapwapGetDB.u1WtpFallbackEnable;

        if (pCapwapCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileRowStatus == ACTIVE)
        {
            i4RetVal = CapwapServiceMainTaskLock ();
            if (i4RetVal == OSIX_FAILURE)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "CapwapServiceMainTaskLock: Failed to obtain lock \r\n");
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
                UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                return OSIX_FAILURE;
            }

            i4RetVal =
                WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_CAPWAP_CONF_UPDATE_REQ,
                                        pWlcHdlrMsgStruct);
            if (i4RetVal == CAPWAP_NO_AP_PRESENT)
            {
                gu1IsConfigResponseReceived = CAPWAP_NO_AP_PRESENT;
            }
            else if (i4RetVal != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "CapwapCliSetWtpProfile : Sending Request to WLC Handler failed \r\n");
                i4RetVal = CapwapServiceMainTaskUnLock ();
                if (i4RetVal == OSIX_FAILURE)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "CapwapServiceMainTaskLock: Failed to obtain lock \r\n");
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
                    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                    return OSIX_FAILURE;
                }
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
                UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                return OSIX_FAILURE;
            }

            while (1)
            {
                if ((gu1IsConfigResponseReceived == OSIX_SUCCESS)
                    || (gu1IsConfigResponseReceived == CAPWAP_NO_AP_PRESENT))
                {
                    i4RetVal = CapwapServiceMainTaskUnLock ();
                    if (i4RetVal == OSIX_FAILURE)
                    {
                        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                    "CapwapServiceMainTaskLock: Failed to obtain lock \r\n");
                        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                        WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
                        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                        return OSIX_FAILURE;
                    }
                    break;

                }
                else if ((gu1IsConfigResponseReceived ==
                          CAPWAP_RESPONSE_TIMEOUT)
                         || (gu1IsConfigResponseReceived == OSIX_FAILURE))
                {
                    i4RetVal = CapwapServiceMainTaskUnLock ();
                    if (i4RetVal == OSIX_FAILURE)
                    {
                        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                    "CapwapServiceMainTaskLock: Failed to obtain lock \r\n");
                        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                        WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
                        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                        return OSIX_FAILURE;
                    }
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "CapwapCliSetWtpProfile : Negative response received\r\n");
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
                    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                    return OSIX_FAILURE;
                }
            }
        }
#endif
    }

    /* WTP Echo Interval and WTP Max Discovery Interval */
    if ((pCapwapIsSetCapwapBaseWtpProfileEntry->
         bCapwapBaseWtpProfileWtpEchoInterval != OSIX_FALSE) ||
        (pCapwapIsSetCapwapBaseWtpProfileEntry->
         bCapwapBaseWtpProfileWtpMaxDiscoveryInterval != OSIX_FALSE))
    {
        pWssIfCapDB->CapwapIsGetAllDB.bWtpEchoInterval = OSIX_TRUE;
        pWssIfCapDB->CapwapGetDB.u2WtpEchoInterval =
            (UINT1) pCapwapCapwapBaseWtpProfileEntry->MibObject.
            u4CapwapBaseWtpProfileWtpEchoInterval;

        pWssIfCapDB->CapwapIsGetAllDB.bWtpMaxDiscoveryInterval = OSIX_TRUE;
        pWssIfCapDB->CapwapGetDB.u2WtpMaxDiscoveryInterval =
            (UINT2) pCapwapCapwapBaseWtpProfileEntry->MibObject.
            u4CapwapBaseWtpProfileWtpMaxDiscoveryInterval;
#ifdef WLC_WANTED
        /* Construct Config Update request message to be send to WTP when there
         * is change in WTP Name */
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.u2InternalId =
            pWssIfCapDB->CapwapGetDB.u2WtpInternalId;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.capwapTimer.isOptional = 1;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.capwapTimer.u2MsgEleType =
            CAPWAP_TIMERS;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.capwapTimer.u2MsgEleLen =
            CAPWAP_TIMERS_LEN;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.capwapTimer.u1Discovery =
            (UINT1) pWssIfCapDB->CapwapGetDB.u2WtpMaxDiscoveryInterval;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.capwapTimer.u1EchoRequest =
            (UINT1) pWssIfCapDB->CapwapGetDB.u2WtpEchoInterval;

        if (pCapwapCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileRowStatus == ACTIVE)
        {
            i4RetVal = CapwapServiceMainTaskLock ();
            if (i4RetVal == OSIX_FAILURE)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "CapwapServiceMainTaskLock: Failed to obtain "
                            "lock \r\n");
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
                UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                return OSIX_FAILURE;
            }

            i4RetVal = WssIfProcessWlcHdlrMsg
                (WSS_WLCHDLR_CAPWAP_CONF_UPDATE_REQ, pWlcHdlrMsgStruct);
            if (i4RetVal == CAPWAP_NO_AP_PRESENT)
            {
                gu1IsConfigResponseReceived = CAPWAP_NO_AP_PRESENT;
            }
            else if (i4RetVal != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "CapwapCliSetWtpProfile : Sending Request to WLC "
                            "Handler failed \r\n");
                i4RetVal = CapwapServiceMainTaskUnLock ();
                if (i4RetVal == OSIX_FAILURE)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "CapwapServiceMainTaskLock: Failed to "
                                "obtain lock \r\n");
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
                    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                    return OSIX_FAILURE;
                }
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
                UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                return OSIX_FAILURE;
            }
            while (1)
            {
                if ((gu1IsConfigResponseReceived == OSIX_SUCCESS)
                    || (gu1IsConfigResponseReceived == CAPWAP_NO_AP_PRESENT))
                {
                    i4RetVal = CapwapServiceMainTaskUnLock ();
                    if (i4RetVal == OSIX_FAILURE)
                    {
                        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                    "CapwapServiceMainTaskLock: Failed to "
                                    "obtain lock \r\n");
                        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                        WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
                        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                        return OSIX_FAILURE;
                    }
                    break;

                }
                else if ((gu1IsConfigResponseReceived ==
                          CAPWAP_RESPONSE_TIMEOUT)
                         || (gu1IsConfigResponseReceived == OSIX_FAILURE))
                {
                    i4RetVal = CapwapServiceMainTaskUnLock ();
                    if (i4RetVal == OSIX_FAILURE)
                    {
                        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                    "CapwapServiceMainTaskLock: Failed to "
                                    "obtain lock \r\n");
                        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                        WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
                        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                        return OSIX_FAILURE;
                    }
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "CapwapCliSetWtpProfile : Negative response "
                                "received\r\n");
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
                    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                    return OSIX_FAILURE;
                }
            }
        }
#endif
    }
    /* WTP Idle Timeout */
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpIdleTimeout != OSIX_FALSE)
    {
        pWssIfCapDB->CapwapIsGetAllDB.bWtpIdleTimeout = OSIX_TRUE;
        pWssIfCapDB->CapwapGetDB.u4WtpIdleTimeout =
            pCapwapCapwapBaseWtpProfileEntry->MibObject.
            u4CapwapBaseWtpProfileWtpIdleTimeout;

#ifdef WLC_WANTED
        /* Construct Config Update request message to be send to WTP when there
         * is change in WTP Name */
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.u2InternalId =
            pWssIfCapDB->CapwapGetDB.u2WtpInternalId;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.idleTimeout.isOptional = 1;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.idleTimeout.u2MsgEleType =
            CAPWAP_IDLE_TIMEOUT_MSG_TYPE;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.idleTimeout.u2MsgEleLen = 4;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.idleTimeout.u4Timeout =
            pWssIfCapDB->CapwapGetDB.u4WtpIdleTimeout;

        if (pCapwapCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileRowStatus == ACTIVE)
        {
            i4RetVal = CapwapServiceMainTaskLock ();
            if (i4RetVal == OSIX_FAILURE)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "CapwapServiceMainTaskLock: Failed to obtain lock \r\n");
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
                UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                return OSIX_FAILURE;
            }

            i4RetVal =
                WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_CAPWAP_CONF_UPDATE_REQ,
                                        pWlcHdlrMsgStruct);
            if (i4RetVal == CAPWAP_NO_AP_PRESENT)
            {
                gu1IsConfigResponseReceived = CAPWAP_NO_AP_PRESENT;
            }
            else if (i4RetVal != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "CapwapCliSetWtpProfile : Sending Request to WLC Handler failed \r\n");
                i4RetVal = CapwapServiceMainTaskUnLock ();
                if (i4RetVal == OSIX_FAILURE)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "CapwapServiceMainTaskLock: Failed to obtain lock \r\n");
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
                    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                    return OSIX_FAILURE;
                }
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
                UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                return OSIX_FAILURE;
            }

            while (1)
            {
                if ((gu1IsConfigResponseReceived == OSIX_SUCCESS)
                    || (gu1IsConfigResponseReceived == CAPWAP_NO_AP_PRESENT))
                {
                    i4RetVal = CapwapServiceMainTaskUnLock ();
                    if (i4RetVal == OSIX_FAILURE)
                    {
                        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                    "CapwapServiceMainTaskLock: Failed to obtain lock \r\n");
                        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                        WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
                        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                        return OSIX_FAILURE;
                    }
                    break;

                }
                else if ((gu1IsConfigResponseReceived ==
                          CAPWAP_RESPONSE_TIMEOUT)
                         || (gu1IsConfigResponseReceived == OSIX_FAILURE))
                {
                    i4RetVal = CapwapServiceMainTaskUnLock ();
                    if (i4RetVal == OSIX_FAILURE)
                    {
                        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                    "CapwapServiceMainTaskLock: Failed to obtain lock \r\n");
                        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                        WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
                        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                        return OSIX_FAILURE;
                    }
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "CapwapCliSetWtpProfile : Negative response received\r\n");
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
                    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                    return OSIX_FAILURE;
                }
            }
        }
#endif
    }
    /* WTP Report Interval */
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpReportInterval != OSIX_FALSE)
    {
        ModelName.pu1_OctetList = au1ModelNumber;

        pWssIfCapDB->CapwapIsGetAllDB.bWtpReportInterval = OSIX_TRUE;
        pWssIfCapDB->CapwapGetDB.u2WtpReportInterval =
            (UINT2) pCapwapCapwapBaseWtpProfileEntry->MibObject.
            u4CapwapBaseWtpProfileWtpReportInterval;

        if (pCapwapCapwapBaseWtpProfileEntry->u4RadioIndex == 0)
        {
            if (nmhGetCapwapBaseWtpProfileWtpModelNumber
                (pCapwapCapwapBaseWtpProfileEntry->MibObject.
                 u4CapwapBaseWtpProfileId, &ModelName) != SNMP_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Model Num not successfully obtained \r\n");
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
                UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);

                return OSIX_FAILURE;
            }

            if (nmhGetFsNoOfRadio (&ModelName, &u4NoOfRadio) != OSIX_FAILURE)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "No.of Radios present not successfully retrived \r\n");
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
                UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);

                return OSIX_FAILURE;
            }
        }

#ifdef WLC_WANTED
        /* Construct Config Update request message to be send to WTP when there
         * is change in WTP Name */
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.u2InternalId =
            pWssIfCapDB->CapwapGetDB.u2WtpInternalId;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.decryErrPeriod.isOptional =
            OSIX_TRUE;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.decryErrPeriod.
            u2ReportInterval = pWssIfCapDB->CapwapGetDB.u2WtpReportInterval;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.decryErrPeriod.u2MsgEleType =
            DECRYPTION_ERROR_REPORT_PERIOD;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.decryErrPeriod.u2MsgEleLen =
            DECRYPTION_ERROR_REPORT_PERIOD_LEN;

        if (pCapwapCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileRowStatus == ACTIVE)
        {
            i4RetVal = CapwapServiceMainTaskLock ();
            if (i4RetVal == OSIX_FAILURE)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "CapwapServiceMainTaskLock: Failed to obtain lock \n");
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
                UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                return OSIX_FAILURE;
            }

            for (u1RadioIndex = 1; u1RadioIndex <= u4NoOfRadio; u1RadioIndex++)
            {
                pWlcHdlrMsgStruct->CapwapConfigUpdateReq.decryErrPeriod.
                    u1RadioId = u1RadioIndex;
                i4RetVal =
                    WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_CAPWAP_CONF_UPDATE_REQ,
                                            pWlcHdlrMsgStruct);
                if (i4RetVal == CAPWAP_NO_AP_PRESENT)
                {
                    gu1IsConfigResponseReceived = CAPWAP_NO_AP_PRESENT;
                }
                else if (i4RetVal != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "CapwapCliSetWtpProfile : Sending Request to WLC "
                                "Handler failed \r\n");
                    i4RetVal = CapwapServiceMainTaskUnLock ();
                    if (i4RetVal == OSIX_FAILURE)
                    {
                        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                    "CapwapServiceMainTaskLock: Failed to "
                                    "obtain lock \n");
                        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                        WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
                        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                        return OSIX_FAILURE;
                    }
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
                    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                    return OSIX_FAILURE;
                }
            }
            while (1)
            {
                if ((gu1IsConfigResponseReceived == OSIX_SUCCESS)
                    || (gu1IsConfigResponseReceived == CAPWAP_NO_AP_PRESENT))
                {
                    i4RetVal = CapwapServiceMainTaskUnLock ();
                    if (i4RetVal == OSIX_FAILURE)
                    {
                        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                    "CapwapServiceMainTaskLock: Failed to obtain "
                                    "lock \r\n");
                        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                        WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
                        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                        return OSIX_FAILURE;
                    }

                    WssMsgStruct.unRadioIfMsg.DecryptReportTimer.
                        u2WtpInternalId =
                        pWssIfCapDB->CapwapGetDB.u2WtpInternalId;
                    WssMsgStruct.unRadioIfMsg.DecryptReportTimer.
                        u2ReportInterval =
                        (UINT2) (pCapwapCapwapBaseWtpProfileEntry->MibObject.
                                 u4CapwapBaseWtpProfileWtpReportInterval);
                    for (u1RadioIndex = 1; u1RadioIndex <= u4NoOfRadio;
                         u1RadioIndex++)
                    {
                        WssMsgStruct.unRadioIfMsg.DecryptReportTimer.u1RadioId =
                            u1RadioIndex;
                        if (WssIfProcessRadioIfMsg
                            (WSS_RADIOIF_DECRYPT_REPORT,
                             &WssMsgStruct) == OSIX_FAILURE)
                        {
                            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                        "CapwapCliSetWtpProfile : Triggering Radio Module Failed\r\n");
                            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                            WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
                            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                            return OSIX_FAILURE;
                        }
                    }
                    break;

                }
                else if ((gu1IsConfigResponseReceived ==
                          CAPWAP_RESPONSE_TIMEOUT)
                         || (gu1IsConfigResponseReceived == OSIX_FAILURE))
                {
                    i4RetVal = CapwapServiceMainTaskUnLock ();
                    if (i4RetVal == OSIX_FAILURE)
                    {
                        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                    "CapwapServiceMainTaskLock: Failed to "
                                    "obtain lock \r\n");
                        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                        WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
                        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                        return OSIX_FAILURE;
                    }
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "CapwapCliSetWtpProfile : Negative response "
                                "received\r\n");
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
                    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                    return OSIX_FAILURE;
                }
            }
        }
#endif
    }

    /* WTP Statistics Timer */
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpStatisticsTimer != OSIX_FALSE)
    {
        if (pCapwapCapwapBaseWtpProfileEntry->MibObject.
            u4CapwapBaseWtpProfileWtpStatisticsTimer != 0)
        {
            pWssIfCapDB->CapwapIsGetAllDB.bWtpStatisticsTimer = OSIX_TRUE;
            pWssIfCapDB->CapwapGetDB.u2WtpStatisticsTimer =
                (UINT2) pCapwapCapwapBaseWtpProfileEntry->MibObject.
                u4CapwapBaseWtpProfileWtpStatisticsTimer;

        }

#ifdef WLC_WANTED
        /* Construct Config Update request message to be send to WTP when there
         * is change in WTP Name */
        pWssIfCapDB->CapwapIsGetAllDB.bRowStatus = OSIX_FALSE;
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_INTERNAL_ID,
                                     pWssIfCapDB) == OSIX_FAILURE)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            return OSIX_FAILURE;
        }

        pWlcHdlrMsgStruct->PmConfigUpdateReq.u4SessId =
            pWssIfCapDB->CapwapGetDB.u2WtpInternalId;
        pWlcHdlrMsgStruct->PmConfigUpdateReq.statsTimer.isOptional = OSIX_TRUE;
        pWlcHdlrMsgStruct->PmConfigUpdateReq.statsTimer.u2MsgEleType =
            CAPWAP_STATS_TIMER_MSG_TYPE;
        pWlcHdlrMsgStruct->PmConfigUpdateReq.statsTimer.u2MsgEleLen = 4;
        pWlcHdlrMsgStruct->PmConfigUpdateReq.statsTimer.u2StatsTimer =
            pWssIfCapDB->CapwapGetDB.u2WtpStatisticsTimer;

        if (pCapwapCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileRowStatus == ACTIVE)
        {
            i4RetVal = CapwapServiceMainTaskLock ();
            if (i4RetVal == OSIX_FAILURE)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "CapwapServiceMainTaskLock: Failed to obtain lock \n");
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
                UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                return OSIX_FAILURE;
            }

            i4RetVal = WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_PM_CONF_UPDATE_REQ,
                                               pWlcHdlrMsgStruct);
            if (i4RetVal == CAPWAP_NO_AP_PRESENT)
            {
                gu1IsConfigResponseReceived = CAPWAP_NO_AP_PRESENT;
            }
            else if (i4RetVal != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "CapwapCliSetWtpProfile : Sending Request to WLC "
                            "Handler failed \r\n");
                i4RetVal = CapwapServiceMainTaskUnLock ();
                if (i4RetVal == OSIX_FAILURE)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "CapwapServiceMainTaskLock: Failed to "
                                "obtain lock \n");
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
                    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                    return OSIX_FAILURE;
                }
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
                UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                return OSIX_FAILURE;
            }

            while (1)
            {
                if ((gu1IsConfigResponseReceived == OSIX_SUCCESS)
                    || (gu1IsConfigResponseReceived == CAPWAP_NO_AP_PRESENT))
                {
                    i4RetVal = CapwapServiceMainTaskUnLock ();
                    if (i4RetVal == OSIX_FAILURE)
                    {
                        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                    "CapwapServiceMainTaskLock: Failed to obtain "
                                    "lock \r\n");
                        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                        WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
                        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                        return OSIX_FAILURE;
                    }
                    break;

                }
                else if ((gu1IsConfigResponseReceived ==
                          CAPWAP_RESPONSE_TIMEOUT)
                         || (gu1IsConfigResponseReceived == OSIX_FAILURE))
                {
                    i4RetVal = CapwapServiceMainTaskUnLock ();
                    if (i4RetVal == OSIX_FAILURE)
                    {
                        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                    "CapwapServiceMainTaskLock: Failed to "
                                    "obtain lock \r\n");
                        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                        WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
                        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                        return OSIX_FAILURE;
                    }
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "CapwapCliSetWtpProfile : Negative response "
                                "received\r\n");
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
                    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                    return OSIX_FAILURE;
                }
            }
        }
#endif
    }

    /* WTP ECN Support */
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpEcnSupport != OSIX_FALSE)
    {
        pWssIfCapDB->CapwapIsGetAllDB.bWtpEcnSupport = OSIX_TRUE;
        pWssIfCapDB->CapwapGetDB.u1WtpEcnSupport =
            (UINT1) pCapwapCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileWtpEcnSupport;
    }

    pWssIfCapDB->CapwapGetDB.u1RowStatus =
        (UINT1) (pCapwapCapwapBaseWtpProfileEntry->MibObject.
                 i4CapwapBaseWtpProfileRowStatus);
    pWssIfCapDB->CapwapIsGetAllDB.bRowStatus = OSIX_TRUE;

    MEMSET (pCapGetDB, 0, sizeof (tWssIfCapDB));
    pCapGetDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    pCapGetDB->CapwapIsGetAllDB.bWtpProfileId = OSIX_TRUE;
    pCapGetDB->CapwapGetDB.u2ProfileId =
        (UINT2) (pCapwapCapwapBaseWtpProfileEntry->MibObject.
                 u4CapwapBaseWtpProfileId);

    pCapGetDB->CapwapIsGetAllDB.bRowStatus = OSIX_TRUE;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pCapGetDB) != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapSetWtpProfile :CAPWAP DB access failed. \r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;
    }
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_SET_DB, pWssIfCapDB) !=
        OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapSetWtpProfile :CAPWAP DB access failed. \r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;
    }
#ifdef WLC_WANTED
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpModelNumber != OSIX_FALSE)
    {
        /* Create all the tables with WTP Profile Id as the key */
        CapwapCreateWtpProfile ((UINT2)
                                (pCapwapCapwapBaseWtpProfileEntry->MibObject.
                                 u4CapwapBaseWtpProfileId));
    }
#endif
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
    WSS_IF_DB_TEMP_MEM_RELEASE (pCapGetDB);
    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WssCapwapSetWtpProfile
 * Input       :   pWsscfgCapwapDot11WlanEntry
 *                 pWsscfgIsSetCapwapDot11WlanEntry
 * Descritpion :  This Routine checks set value and invoke corresponding
 *                module
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 ***************************************************************************/
INT4
CapwapCliSetWtpDefaultProfile (tCapwapFsCapwapDefaultWtpProfileEntry *
                               pCapwapFsCapwapDefaultWtpProfileEntry,
                               tCapwapIsSetFsCapwapDefaultWtpProfileEntry *
                               pCapwapIsSetFsCapwapDefaultWtpProfileEntry)
{
    tWssIfCapDB        *pWssIfCapDB = NULL;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapDB, OSIX_FAILURE)
/* WTP Echo Interval */
        if (pCapwapIsSetFsCapwapDefaultWtpProfileEntry->
            bFsCapwapDefaultWtpProfileWtpEchoInterval != OSIX_FALSE)
    {
        pWssIfCapDB->CapwapIsGetAllDB.bWtpEchoInterval = OSIX_TRUE;
        pWssIfCapDB->CapwapIsGetAllDB.bWtpDefaultProfile = OSIX_TRUE;
        pWssIfCapDB->CapwapGetDB.u2WtpEchoInterval = (UINT2)
            pCapwapFsCapwapDefaultWtpProfileEntry->MibObject.
            u4FsCapwapDefaultWtpProfileWtpEchoInterval;
    }
    /* WTP Idle Timeout */
    if (pCapwapIsSetFsCapwapDefaultWtpProfileEntry->
        bFsCapwapDefaultWtpProfileWtpIdleTimeout != OSIX_FALSE)
    {
        pWssIfCapDB->CapwapIsGetAllDB.bWtpIdleTimeout = OSIX_TRUE;
        pWssIfCapDB->CapwapIsGetAllDB.bWtpDefaultProfile = OSIX_TRUE;
        pWssIfCapDB->CapwapGetDB.u4WtpIdleTimeout = (UINT2)
            pCapwapFsCapwapDefaultWtpProfileEntry->MibObject.
            u4FsCapwapDefaultWtpProfileWtpIdleTimeout;
    }
    /* WTP Max Discovery Interval */
    if (pCapwapIsSetFsCapwapDefaultWtpProfileEntry->
        bFsCapwapDefaultWtpProfileWtpMaxDiscoveryInterval != OSIX_FALSE)
    {
        pWssIfCapDB->CapwapIsGetAllDB.bWtpMaxDiscoveryInterval = OSIX_TRUE;
        pWssIfCapDB->CapwapIsGetAllDB.bWtpDefaultProfile = OSIX_TRUE;
        pWssIfCapDB->CapwapGetDB.u2WtpMaxDiscoveryInterval = (UINT2)
            pCapwapFsCapwapDefaultWtpProfileEntry->MibObject.
            u4FsCapwapDefaultWtpProfileWtpMaxDiscoveryInterval;
    }
    /* WTP Report Interval */
    if (pCapwapIsSetFsCapwapDefaultWtpProfileEntry->
        bFsCapwapDefaultWtpProfileWtpReportInterval != OSIX_FALSE)
    {
        pWssIfCapDB->CapwapIsGetAllDB.bWtpReportInterval = OSIX_TRUE;
        pWssIfCapDB->CapwapIsGetAllDB.bWtpDefaultProfile = OSIX_TRUE;
        pWssIfCapDB->CapwapGetDB.u2WtpReportInterval = (UINT2)
            pCapwapFsCapwapDefaultWtpProfileEntry->MibObject.
            u4FsCapwapDefaultWtpProfileWtpReportInterval;
    }
    /* WTP Statistics Timer */
    if (pCapwapIsSetFsCapwapDefaultWtpProfileEntry->
        bFsCapwapDefaultWtpProfileWtpStatisticsTimer != OSIX_FALSE)
    {
        pWssIfCapDB->CapwapIsGetAllDB.bWtpStatisticsTimer = OSIX_TRUE;
        pWssIfCapDB->CapwapIsGetAllDB.bWtpDefaultProfile = OSIX_TRUE;
        pWssIfCapDB->CapwapGetDB.u2WtpStatisticsTimer = (UINT2)
            pCapwapFsCapwapDefaultWtpProfileEntry->MibObject.
            u4FsCapwapDefaultWtpProfileWtpStatisticsTimer;
    }
    /* WTP Fallback Enable */
    if (pCapwapIsSetFsCapwapDefaultWtpProfileEntry->
        bFsCapwapDefaultWtpProfileWtpFallbackEnable != OSIX_FALSE)
    {
        pWssIfCapDB->CapwapIsGetAllDB.bWtpFallbackEnable = OSIX_TRUE;
        pWssIfCapDB->CapwapIsGetAllDB.bWtpDefaultProfile = OSIX_TRUE;
        pWssIfCapDB->CapwapGetDB.u1WtpFallbackEnable = (UINT1)
            (pCapwapFsCapwapDefaultWtpProfileEntry->MibObject.
             i4FsCapwapDefaultWtpProfileWtpFallbackEnable);
    }
    /* WTP ECN Support */
    if (pCapwapIsSetFsCapwapDefaultWtpProfileEntry->
        bFsCapwapDefaultWtpProfileWtpEcnSupport != OSIX_FALSE)
    {
        pWssIfCapDB->CapwapIsGetAllDB.bWtpEcnSupport = OSIX_TRUE;
        pWssIfCapDB->CapwapIsGetAllDB.bWtpDefaultProfile = OSIX_TRUE;
        pWssIfCapDB->CapwapGetDB.u1WtpEcnSupport = (UINT1)
            (pCapwapFsCapwapDefaultWtpProfileEntry->MibObject.
             i4FsCapwapDefaultWtpProfileWtpEcnSupport);
    }
    if (pCapwapIsSetFsCapwapDefaultWtpProfileEntry->
        bFsCapwapDefaultWtpProfileRowStatus != OSIX_FALSE)
    {
        pWssIfCapDB->CapwapIsGetAllDB.bRowStatus = OSIX_TRUE;
        pWssIfCapDB->CapwapIsGetAllDB.bWtpDefaultProfile = OSIX_TRUE;
        pWssIfCapDB->CapwapGetDB.u1RowStatus =
            (UINT1) (pCapwapFsCapwapDefaultWtpProfileEntry->MibObject.
                     i4FsCapwapDefaultWtpProfileRowStatus);
    }

    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_SET_PROFILE_ENTRY, pWssIfCapDB) !=
        OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapSetWtpProfile :CAPWAP DB access failed. \r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
        return OSIX_FAILURE;
    }

    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  CapwapGetWtpProfileIdFromProfileMac
 * Input       :   pu1WtpProfileMac
 *                 pu4WtpProfileId
 * Descritpion :  This Routine checks set value and invoke corresponding
 *                module
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 ***************************************************************************/
INT4
CapwapGetWtpProfileIdFromProfileMac (tMacAddr WtpMacAddr,
                                     UINT4 *pu4WtpProfileId)
{
    tWssIfCapDB        *pWssIfCapDB = NULL;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapDB, 0, sizeof (tWssIfCapDB));

    pWssIfCapDB->CapwapIsGetAllDB.bWtpProfileId = OSIX_TRUE;
    MEMCPY (pWssIfCapDB->CapwapGetDB.WtpMacAddress, WtpMacAddr, MAC_ADDR_LEN);

    if (WssIfProcessCapwapDBMsg
        (WSS_CAPWAP_GET_MACADDR_MAPPING_ENTRY, pWssIfCapDB) != OSIX_SUCCESS)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
        return OSIX_FAILURE;
    }
    *pu4WtpProfileId = (UINT4) pWssIfCapDB->CapwapGetDB.u2WtpInternalId;

    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  CapwapGetBssifIndexFromStationMac 
 * Input       :   StaMacAddr
 *                 pu4BssIfIndex
 * Descritpion :  This Routine checks set value and invoke corresponding
 *                module
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 ***************************************************************************/
INT4
CapwapGetBssifIndexFromStationMac (tMacAddr StaMacAddr, UINT4 *pu4BssIfIndex)
{
    tWssifauthDBMsgStruct *pWssifauthDBMsgStruct = NULL;
    pWssifauthDBMsgStruct =
        (tWssifauthDBMsgStruct *) (VOID *) UtlShMemAllocWssIfAuthDbBuf ();
    if (pWssifauthDBMsgStruct == NULL)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapGetBssifIndexFromStationMac:- "
                    "UtlShMemAllocWssIfAuthDbBuf returned failure\n");
        return OSIX_FAILURE;
    }

    MEMSET (pWssifauthDBMsgStruct, 0, sizeof (tWssifauthDBMsgStruct));

    /*pWssifauthDBMsgStruct->WssIfAuthStateDB.stationMacAddress = StaMacAddr; */

    MEMCPY (pWssifauthDBMsgStruct->WssIfAuthStateDB.stationMacAddress,
            StaMacAddr, sizeof (tMacAddr));
#ifdef WLC_WANTED
    if (WssIfProcessWssAuthDBMsg
        (WSS_AUTH_GET_STATION_DB, pWssifauthDBMsgStruct) != OSIX_SUCCESS)
    {
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssifauthDBMsgStruct);
        return OSIX_FAILURE;
    }
    *pu4BssIfIndex =
        pWssifauthDBMsgStruct->WssIfAuthStateDB.aWssStaBSS[0].u4BssIfIndex;
#else
    UNUSED_PARAM (pu4BssIfIndex);
#endif
    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssifauthDBMsgStruct);
    return OSIX_SUCCESS;

}

/****************************************************************************
 * Function    :  CapwapGetRadioIfIndexFromBssIfIndex 
 * Input       :   u4BssIfIndex
 *            pu4RadioIfIndex
 * Descritpion :  This Routine checks set value and invoke corresponding
 *                module
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 ***************************************************************************/
INT4
CapwapGetRadioIfIndexFromBssIfIndex (UINT4 u4BssIfIndex, UINT4 *pu4RadioIfIndex)
{
    tWssWlanDB          wssWlanDB;
    MEMSET (&wssWlanDB, 0, sizeof (tWssWlanDB));
    wssWlanDB.WssWlanAttributeDB.u4BssIfIndex = u4BssIfIndex;
    wssWlanDB.WssWlanIsPresentDB.bRadioIfIndex = OSIX_TRUE;
    wssWlanDB.WssWlanIsPresentDB.bBssIfIndex = OSIX_TRUE;
#ifdef WLC_WANTED
/* Once vlan id is added to wlan module, uncomment the line below  */
/* WssWlanDB.WssWlanIsPresentDB.bVlanId = OSIX_TRUE;  */
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_IFINDEX_ENTRY, &wssWlanDB) !=
        OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    *pu4RadioIfIndex = wssWlanDB.WssWlanAttributeDB.u4RadioIfIndex;
#else
    UNUSED_PARAM (pu4RadioIfIndex);
#endif
    /*Get the type of vlan id and show it here  */
    return OSIX_SUCCESS;

}

/****************************************************************************
 * Function    :  CapwapGetRadioIdFromRadioIfIndex 
 * Input       :  u4RadioIfIndex,
 *           pu4RadioId,
 *           pu4WtpInternalId
 * Descritpion :  This Routine checks set value and invoke corresponding
 *                module
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 ***************************************************************************/
INT4
CapwapGetRadioIdFromRadioIfIndex (UINT4 u4RadioIfIndex, UINT4 *pu4RadioId,
                                  UINT4 *pu4WtpInternalId)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
        UtlShMemAllocAntennaSelectionBuf ();

    if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "\r%% Memory allocation for Antenna"
                    " selection failed.\r\n");
        return OSIX_FAILURE;
    }

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = u4RadioIfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioIfIndex = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;

    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, &RadioIfGetDB) !=
        OSIX_SUCCESS)
    {
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                         pu1AntennaSelection);
        return OSIX_FAILURE;
    }
    *pu4RadioId = RadioIfGetDB.RadioIfGetAllDB.u1RadioId;
    *pu4WtpInternalId = RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;
    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                     pu1AntennaSelection);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :   CapwapGetWtpMacFromWtpInternalId
 * Input       :  u4WtpInternalId
 *           pWtpMacAddr 
 * Descritpion :  This Routine checks set value and invoke corresponding
 *                module
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 ***************************************************************************/
INT4
CapwapGetWtpMacFromWtpInternalId (UINT4 u4WtpInternalId, tMacAddr * pWtpMacAddr)
{
    tWssIfCapDB        *pWssIfCapDB = NULL;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapDB, OSIX_FAILURE)
        pWssIfCapDB->CapwapGetDB.u2WtpInternalId = (UINT2) u4WtpInternalId;
    pWssIfCapDB->CapwapIsGetAllDB.bWtpMacAddress = OSIX_TRUE;
    pWssIfCapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapDB) !=
        OSIX_SUCCESS)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
        return OSIX_FAILURE;
    }

    MEMCPY (pWtpMacAddr, pWssIfCapDB->CapwapGetDB.WtpMacAddress, MAC_ADDR_LEN);
/*    MEMCPY(WssifauthDBMsgStruct.WssIfAuthStateDB.stationMacAddress,
           , sizeof(tMacAddr)); 
    *pWtpMacAddr = pWssIfCapDB->CapwapGetDB.WtpMacAddress;*/
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
    return OSIX_SUCCESS;
}

/****************************************************************************
  * Function    :   CapwapGetWtpDiscStatsEntry
  * Input       :   pWssIfCapDB
  * Descritpion :  This Routine checks set value and invoke corresponding
  *                module
  * Output      :  None
  * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
***************************************************************************/

INT4
CapwapGetWtpDiscStatsEntry (tWssIfPMDB * pWssIfPMDB)
{
#ifdef WLC_WANTED
    if (WssIfProcessPMDBMsg (WSS_PM_CLI_CPWP_DISC_STATS_GET, pWssIfPMDB) !=
        OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
#else
    if (WssIfProcessPMDBMsg (WSS_PM_WTP_EVT_VSP_CAPWAP_STATS_GET, pWssIfPMDB) !=
        OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
#endif

    return OSIX_SUCCESS;
}

/****************************************************************************
  * Function    :   CapwapGetCapwATPStatsEntry
  * Input       :   pWssIfCapDB
  * Descritpion :  This Routine checks set value and invoke corresponding
  *                module
  * Output      :  None
  * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
***************************************************************************/

INT4
CapwapGetCapwATPStatsEntry (tWssIfPMDB * pWssIfPMDB)
{
#ifdef WLC_WANTED
    if (WssIfProcessPMDBMsg (WSS_PM_CLI_CAPW_ATP_STATS_GET, pWssIfPMDB) !=
        OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
#else
    if (WssIfProcessPMDBMsg (WSS_PM_WTP_EVT_VSP_CAPWAP_STATS_GET, pWssIfPMDB) !=
        OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
#endif

    return OSIX_SUCCESS;
}

/****************************************************************************
  * Function    :   CapwapGetApTrafficStatsEntry
  * Input       :   pWssIfCapDB
  * Descritpion :  This Routine checks set value and invoke corresponding
  *                module
  * Output      :  None
  * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
***************************************************************************/
/*
INT4
CapwapGetApTrafficStatsEntry (tWssIfPMDB * pWssIfPMDB)
{
#ifdef WLC_WANTED
    if (WssIfProcessPMDBMsg (WSS_PM_CLI_CPWP_ATP_STATS_GET, pWssIfPMDB) !=
        OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
#else
    if (WssIfProcessPMDBMsg (WSS_PM_WTP_EVT_VSP_CAPWAP_STATS_GET, pWssIfPMDB) !=
        OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
#endif

    return OSIX_SUCCESS;

}*/
/****************************************************************************
  * Function    :  CapwapGetWtpJoinStatsEntry
  * Input       :   pWssIfCapDB
  * Descritpion :  This Routine checks set value and invoke corresponding
  *                module
  * Output      :  None
  * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
***************************************************************************/
INT4
CapwapGetWtpJoinStatsEntry (tWssIfPMDB * pWssIfPMDB)
{
#ifdef WLC_WANTED
    if (WssIfProcessPMDBMsg (WSS_PM_CLI_CPWP_JOIN_STATS_GET, pWssIfPMDB) !=
        OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
#else
    if (WssIfProcessPMDBMsg (WSS_PM_WTP_EVT_VSP_CAPWAP_STATS_GET, pWssIfPMDB) !=
        OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
#endif

    return OSIX_SUCCESS;
}

/****************************************************************************
  * Function    :  CapwapGetWtpConfigStatsEntry
  * Input       :   pWssIfCapDB
  * Descritpion :  This Routine checks set value and invoke corresponding
  *                module
  * Output      :  None
  * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
***************************************************************************/
INT4
CapwapGetWtpConfigStatsEntry (tWssIfPMDB * pWssIfPMDB)
{
#ifdef WLC_WANTED
    if (WssIfProcessPMDBMsg (WSS_PM_CLI_CAPWAP_CONFIG_STATS_GET, pWssIfPMDB) !=
        OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
#else
    if (WssIfProcessPMDBMsg (WSS_PM_WTP_EVT_VSP_CAPWAP_STATS_GET, pWssIfPMDB) !=
        OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
#endif

    return OSIX_SUCCESS;
}

/****************************************************************************
  * Function    :  CapwapGetWtpEvtRebootStatsEntry
  * Input       :   pWssIfCapDB
  * Descritpion :  This Routine checks set value and invoke corresponding
  *                module
  * Output      :  None
  * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
***************************************************************************/
INT4
CapwapGetWtpEvtRebootStatsEntry (tWssIfPMDB * pWssIfPMDB)
{

    if (WssIfProcessPMDBMsg (WSS_PM_CLI_WTP_REBOOT_STATS_GET, pWssIfPMDB) !=
        OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  CapwapGetWtpEvtRebootStatsEntry
 * Input       :   pWssIfCapDB
 * Descritpion :  This Routine checks set value and invoke corresponding
 *                module
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 ***************************************************************************/
INT4
CapwapGetWtpEvtRadioStatsEntry (tWssIfPMDB * pWssIfPMDB)
{

    if (WssIfProcessPMDBMsg (WSS_PM_CLI_CAPWAP_AP_RADIO_STATS_GET, pWssIfPMDB)
        != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
  * Function    :  CapwapGetWtpRunStatsEntry
  * Input       :   pWssIfCapDB
  * Descritpion :  This Routine checks set value and invoke corresponding
  *                module
  * Output      :  None
  * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
***************************************************************************/
INT4
CapwapGetWtpRunStatsEntry (tWssIfPMDB * pWssIfPMDB)
{
#ifdef WLC_WANTED
    if (WssIfProcessPMDBMsg (WSS_PM_CLI_CAPWAP_RUN_STATS_GET, pWssIfPMDB) !=
        OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
#else
    if (WssIfProcessPMDBMsg (WSS_PM_WTP_EVT_VSP_CAPWAP_STATS_GET, pWssIfPMDB) !=
        OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
#endif
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  CapwapGetWtpProfileIdFromProfileName
 * Input       :   pu1WtpProfileName
 *                 pu4WtpProfileId
 * Descritpion :  This Routine checks set value and invoke corresponding
 *                module
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 ***************************************************************************/
INT4
CapwapGetWtpProfileIdFromProfileName (UINT1 *pu1WtpProfileName,
                                      UINT4 *pu4WtpProfileId)
{
    tWssIfCapDB        *pWssIfCapDB = NULL;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapDB, 0, sizeof (tWssIfCapDB));

    pWssIfCapDB->CapwapIsGetAllDB.bWtpProfileId = OSIX_TRUE;
    if (STRLEN (pu1WtpProfileName) > WSSIF_CAPWAP_PROFILE_NAME_LEN)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
        return OSIX_FAILURE;
    }
    MEMCPY (pWssIfCapDB->CapwapGetDB.au1ProfileName, pu1WtpProfileName,
            STRLEN (pu1WtpProfileName));

    if (WssIfProcessCapwapDBMsg
        (WSS_CAPWAP_GET_PROFILE_MAPPING_ENTRY, pWssIfCapDB) != OSIX_SUCCESS)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
        return OSIX_FAILURE;
    }

    *pu4WtpProfileId = pWssIfCapDB->CapwapGetDB.u2WtpInternalId;

    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  CapwapGetWtpProfileNameFromProfileId
 * Input       :   pu1WtpProfileName
 *                 pu4WtpProfileId
 * Descritpion :  This Routine checks set value and invoke corresponding
 *                module
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 ***************************************************************************/
INT4
CapwapGetWtpProfileNameFromProfileId (UINT1 *pu1WtpProfileName,
                                      UINT4 u4WtpProfileId)
{
    tWssIfCapDB        *pWssIfCapDB = NULL;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapDB, 0, sizeof (tWssIfCapDB));

    pWssIfCapDB->CapwapIsGetAllDB.bProfileName = OSIX_TRUE;
    pWssIfCapDB->CapwapGetDB.u2ProfileId = (UINT2) u4WtpProfileId;
    if (WssIfProcessCapwapDBMsg
        (WSS_CAPWAP_GET_PROFILEID_MAPPING_ENTRY, pWssIfCapDB) != OSIX_SUCCESS)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
        return OSIX_FAILURE;
    }

    MEMCPY (pu1WtpProfileName, pWssIfCapDB->CapwapGetDB.au1ProfileName,
            STRLEN (pWssIfCapDB->CapwapGetDB.au1ProfileName));
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  CapwapGetFsCapwapEncryptChannelRxPackets
 * Input       :  The Indices
 *               CapwapBaseWtpProfileId
 *               FsCapwapEncryptChannel
 *
 *               The Object
 *               retValFsCapwapEncryptChannelRxPackets
 * Output      :  The Get Routine Take the Indices &
 *               store the Value requested in the Return val.
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetFsCapwapEncryptChannelRxPackets (UINT4 u4CapwapBaseWtpProfileId,
                                          INT4 i4FsCapwapEncryptChannel,
                                          UINT4
                                          *pu4RetValFsCapwapEncryptChannelRxPackets)
{
    INT4                i4RetVal = OSIX_SUCCESS;
    tWssIfCapDB        *pCapwapGetDB = NULL;

    WSS_IF_DB_TEMP_MEM_ALLOC (pCapwapGetDB, OSIX_FAILURE)
        MEMSET (pCapwapGetDB, 0, sizeof (tWssIfCapDB));

    pCapwapGetDB->CapwapIsGetAllDB.bWtpProfileId = OSIX_TRUE;
    pCapwapGetDB->CapwapGetDB.u2ProfileId = (UINT2) u4CapwapBaseWtpProfileId;

    if (i4FsCapwapEncryptChannel == CLI_FSCAPWAP_ENCRYPTION_CONTROL)
    {
        if (CapwapGetInternalProfildId (&pCapwapGetDB->CapwapGetDB.u2ProfileId,
                                        &pCapwapGetDB->CapwapGetDB.
                                        u2WtpInternalId) == OSIX_SUCCESS)
        {
            pCapwapGetDB->CapwapIsGetAllDB.bWtpCtrlDTLSCount = OSIX_TRUE;
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pCapwapGetDB) ==
                OSIX_SUCCESS)
            {
                *pu4RetValFsCapwapEncryptChannelRxPackets =
                    pCapwapGetDB->CapwapGetDB.u4WtpCtrlDTLSCount;
            }
        }
        else
        {
            PRINTF ("Entry does not exist\n");
            i4RetVal = OSIX_FAILURE;
        }
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pCapwapGetDB);
    return i4RetVal;

}

/****************************************************************************
  * Function    :  CapwapSetBaseAcNameListTable
  * Input       :   pWssIfCapDB
  * Descritpion :  This Routine checks set value and invoke corresponding
  *                module
  * Output      :  None
  * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
***************************************************************************/
INT4
CapwapSetBaseAcNameListTable (tWssIfCapDB * pWssIfCapDB)
{

    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_SET_DB, pWssIfCapDB) !=
        OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  CapwapSetDnsProfileTable
 * Input       :   pWssIfCapDB
 * Descritpion :  This Routine  set value and invoke corresponding
 *                module
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 ***************************************************************************/
INT4
CapwapSetDnsProfileTable (tWssIfCapDB * pWssIfCapDB)
{
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_SET_DB, pWssIfCapDB) !=
        OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  CapwapCreateDnsProfileTable
 * Input       :   pWssIfCapDB
 * Descritpion :  This Routine set value and invoke corresponding
 *                module
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 ***************************************************************************/
INT4
CapwapCreateDnsProfileTable (tWssIfCapDB * pWssIfCapDB)
{
    if (WssIfProcessCapwapDBMsg
        (WSS_CAPWAP_CREATE_DNS_PROFILE_ENTRY, pWssIfCapDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
  * Function    : CapwapDestroyBaseAcNameListTable 
  * Input       :   pWssIfCapDB
  * Descritpion :  This Routine checks set value and invoke corresponding
  *                module
  * Output      :  None
  * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 ***************************************************************************/
INT4
CapwapDestroyBaseAcNameListTable (tWssIfCapDB * pWssIfCapDB)
{
    if (WssIfProcessCapwapDBMsg
        (WSS_CAPWAP_DESTROY_BASEACNAMELIST_ENTRY, pWssIfCapDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  CapwapGetWtpStatsEntry
 * Input       :   pWssIfCapDB
 * Descritpion :  This Routine checks set value and invoke corresponding
 *                module
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 ***************************************************************************/
INT4
CapwapGetWtpStatsEntry (tWssIfCapDB * pWssIfCapDB)
{

    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_SHOW_WTPSTATE_TABLE, pWssIfCapDB) !=
        OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :   CapwapCliSetWtpRadio
 * Input       :   pCapwapFsWtpRadioEntry
 *                 pCapwapIsSetFsWtpRadioEntry
 * Descritpion :  This Routine checks set value and invoke corresponding
 *                module
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 ***************************************************************************/
INT4
CapwapCliSetWtpRadio (tCapwapFsWtpRadioEntry *
                      pCapwapFsWtpRadioEntry,
                      tCapwapIsSetFsWtpRadioEntry * pCapwapIsSetFsWtpRadioEntry)
{
    tWssIfCapDB        *pWssIfCapDB = NULL;

    if ((pCapwapFsWtpRadioEntry == NULL) ||
        (pCapwapIsSetFsWtpRadioEntry == NULL))
    {
        return OSIX_FAILURE;
    }

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapDB, 0, sizeof (tWssIfCapDB));

    if ((STRLEN (pCapwapFsWtpRadioEntry->MibObject.au1FsCapwapWtpModelNumber) !=
         0) && (pCapwapFsWtpRadioEntry->MibObject.u4FsNumOfRadio != 0))
    {
        /* WTP Radio: No of Radios */
        pWssIfCapDB->CapwapIsGetAllDB.bRadioCount = OSIX_TRUE;
        pWssIfCapDB->CapwapGetDB.WtpModelEntry.u1NumOfRadio =
            (UINT1) pCapwapFsWtpRadioEntry->MibObject.u4FsNumOfRadio;

        pWssIfCapDB->CapwapIsGetAllDB.bWtpModelNumber = OSIX_TRUE;
        MEMCPY (pWssIfCapDB->CapwapGetDB.WtpModelEntry.au1ModelNumber,
                pCapwapFsWtpRadioEntry->MibObject.au1FsCapwapWtpModelNumber,
                STRLEN (pCapwapFsWtpRadioEntry->MibObject.
                        au1FsCapwapWtpModelNumber));

        pWssIfCapDB->CapwapIsGetAllDB.bRowStatus = OSIX_TRUE;
        pWssIfCapDB->CapwapGetDB.WtpModelEntry.u1RowStatus =
            (UINT1) pCapwapFsWtpRadioEntry->MibObject.i4FsWtpRadioRowStatus;
    }
    else
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapSetWtpRadio : Index missing. \r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
        return OSIX_FAILURE;
    }
    /*WTP Radio Type */
    if (pCapwapFsWtpRadioEntry->MibObject.u4FsWtpRadioType != 0)
    {
        pCapwapIsSetFsWtpRadioEntry->bFsWtpRadioType = OSIX_TRUE;
        pWssIfCapDB->CapwapIsGetAllDB.bWtpRadioType = OSIX_TRUE;
        pWssIfCapDB->CapwapGetDB.WtpModelEntry.au4WtpRadioType[pWssIfCapDB->
                                                               CapwapGetDB.
                                                               WtpModelEntry.
                                                               u1NumOfRadio -
                                                               1] =
            pCapwapFsWtpRadioEntry->MibObject.u4FsWtpRadioType;
    }
    /*WTP Radio Admin Status */
    if (pCapwapFsWtpRadioEntry->MibObject.i4FsRadioAdminStatus != 0)
    {
        pCapwapIsSetFsWtpRadioEntry->bFsRadioAdminStatus = OSIX_TRUE;
        pWssIfCapDB->CapwapIsGetAllDB.bRadioAdminStatus = OSIX_TRUE;
        pWssIfCapDB->CapwapGetDB.WtpModelEntry.
            au1WtpRadioAdminStatus[pWssIfCapDB->CapwapGetDB.WtpModelEntry.
                                   u1NumOfRadio - 1] =
            (UINT1) pCapwapFsWtpRadioEntry->MibObject.i4FsRadioAdminStatus;
    }
    /*WTP Max SSID Supported */
    if (pCapwapIsSetFsWtpRadioEntry->bFsMaxSSIDSupported != OSIX_FALSE)
    {
        pWssIfCapDB->CapwapIsGetAllDB.bMaxSSIDSupported = OSIX_TRUE;
        pWssIfCapDB->CapwapGetDB.WtpModelEntry.
            au1WtpMaxSSIDSupported[pWssIfCapDB->CapwapGetDB.WtpModelEntry.
                                   u1NumOfRadio - 1] =
            (UINT1) pCapwapFsWtpRadioEntry->MibObject.i4FsMaxSSIDSupported;
    }

    pWssIfCapDB->CapwapIsGetAllDB.bRowStatus = OSIX_TRUE;
    pWssIfCapDB->CapwapGetDB.WtpModelEntry.u1RowStatus =
        (UINT1) pCapwapFsWtpRadioEntry->MibObject.i4FsWtpRadioRowStatus;

    if ((pCapwapIsSetFsWtpRadioEntry->bFsWtpRadioType != OSIX_FALSE) ||
        (pCapwapIsSetFsWtpRadioEntry->bFsRadioAdminStatus != OSIX_FALSE) ||
        (pCapwapIsSetFsWtpRadioEntry->bFsMaxSSIDSupported != OSIX_FALSE))
    {
        if (WssIfProcessCapwapDBMsg
            (WSS_CAPWAP_SET_WTP_MODEL_ENTRY, pWssIfCapDB) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "CapwapSetWtpRadio :CAPWAP DB access failed. \r\n");
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
            return OSIX_FAILURE;
        }
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
    return OSIX_SUCCESS;

}

/****************************************************************************
 * Function    :   CapwapCliSetWtpModel
 * Input       :   pCapwapFsWtpModelEntry
 *                 pCapwapIsSetFsWtpModelEntry
 * Descritpion :  This Routine checks set value and invoke corresponding
 *                module
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 ***************************************************************************/
INT4
CapwapCliSetWtpModel (tCapwapFsWtpModelEntry *
                      pCapwapFsWtpModelEntry,
                      tCapwapIsSetFsWtpModelEntry * pCapwapIsSetFsWtpModelEntry)
{
    tWssIfCapDB        *pWssIfCapDB = NULL;
    tWssIfCapDB        *pWssIfCapGetOldDB = NULL;
    UINT1               u1RadioCount = 0;
    INT4                i4FsWtpRadioRowStatus = 0;
    UINT4               u4ErrCode = 0;
    tSNMP_OCTET_STRING_TYPE ModelNumber;

    MEMSET (&ModelNumber, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    if ((pCapwapFsWtpModelEntry == NULL) ||
        (pCapwapIsSetFsWtpModelEntry == NULL))
    {
        return OSIX_FAILURE;
    }

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapDB, OSIX_FAILURE)
        WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapGetOldDB, OSIX_FAILURE)
        MEMSET (pWssIfCapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (pWssIfCapGetOldDB, 0, sizeof (tWssIfCapDB));
    ModelNumber.pu1_OctetList =
        pCapwapFsWtpModelEntry->MibObject.au1FsCapwapWtpModelNumber;
    ModelNumber.i4_Length =
        (INT4) (STRLEN
                (pCapwapFsWtpModelEntry->MibObject.au1FsCapwapWtpModelNumber));

    /* WTP Model Number */
    if (STRLEN (pCapwapFsWtpModelEntry->MibObject.au1FsCapwapWtpModelNumber) !=
        0)
    {
        pWssIfCapDB->CapwapIsGetAllDB.bWtpModelNumber = OSIX_TRUE;
        MEMCPY (pWssIfCapDB->CapwapGetDB.WtpModelEntry.au1ModelNumber,
                pCapwapFsWtpModelEntry->MibObject.au1FsCapwapWtpModelNumber,
                STRLEN (pCapwapFsWtpModelEntry->MibObject.
                        au1FsCapwapWtpModelNumber));

        pWssIfCapDB->CapwapIsGetAllDB.bRowStatus = OSIX_TRUE;
        pWssIfCapDB->CapwapGetDB.WtpModelEntry.u1RowStatus =
            (UINT1) pCapwapFsWtpModelEntry->MibObject.i4FsWtpModelRowStatus;

        if (pCapwapIsSetFsWtpModelEntry->bFsWtpModelRowStatus != OSIX_FALSE)
        {
            if (pCapwapFsWtpModelEntry->MibObject.i4FsWtpModelRowStatus ==
                DESTROY)
            {
                for (u1RadioCount = 1; u1RadioCount <=
                     pCapwapFsWtpModelEntry->MibObject.u4FsNoOfRadio;
                     u1RadioCount++)
                {
                    /* Check if the profile is already present */
                    if ((nmhGetFsWtpRadioRowStatus (&ModelNumber, u1RadioCount,
                                                    &i4FsWtpRadioRowStatus)) ==
                        SNMP_SUCCESS)
                    {
                        /* Profile NOT present - CREATE */

                        if (nmhTestv2FsWtpRadioRowStatus
                            (&u4ErrCode, &ModelNumber, u1RadioCount,
                             DESTROY) == SNMP_SUCCESS)
                        {
                            /* DELETE Profile */
                            if (nmhSetFsWtpRadioRowStatus
                                (&ModelNumber, u1RadioCount,
                                 DESTROY) == SNMP_FAILURE)
                            {
                                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                            "CapwapCliSetWtpModel : Radio DB Access failed. \r\n");
                            }
                        }
                    }
                    else
                    {
                        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                    "CapwapCliSetWtpModel : Radio Entry doesnot exists. \r\n");
                    }
                }

                if (WssIfProcessCapwapDBMsg
                    (WSS_CAPWAP_DESTROY_WTP_MODEL_ENTRY,
                     pWssIfCapDB) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "CapwapCliSetWtpModel :CAPWAP DB access failed to delete entry. \r\n");
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapGetOldDB);
                    return OSIX_FAILURE;
                }
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapGetOldDB);
                return OSIX_SUCCESS;
            }

            else if (WssIfProcessCapwapDBMsg
                     (WSS_CAPWAP_GET_WTP_MODEL_ENTRY,
                      pWssIfCapDB) != OSIX_SUCCESS)
            {
                if (WssIfProcessCapwapDBMsg
                    (WSS_CAPWAP_CREATE_WTP_MODEL_ENTRY,
                     pWssIfCapDB) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "CapwapCliSetWtpModel :CAPWAP DB access failed to create an entry. \r\n");
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapGetOldDB);
                    return OSIX_FAILURE;
                }
            }
        }
    }
    else
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapCliSetWtpModel : Model number missing. \r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapGetOldDB);
        return OSIX_FAILURE;
    }

    /* WTP Model No of Radios */
    if (pCapwapIsSetFsWtpModelEntry->bFsNoOfRadio != OSIX_FALSE)
    {
        pWssIfCapDB->CapwapIsGetAllDB.bNoOfRadio = OSIX_TRUE;
        pWssIfCapDB->CapwapGetDB.WtpModelEntry.u1NumOfRadio =
            (UINT1) pCapwapFsWtpModelEntry->MibObject.u4FsNoOfRadio;

        MEMCPY (pWssIfCapGetOldDB->CapwapGetDB.WtpModelEntry.au1ModelNumber,
                pCapwapFsWtpModelEntry->MibObject.au1FsCapwapWtpModelNumber,
                STRLEN (pCapwapFsWtpModelEntry->MibObject.
                        au1FsCapwapWtpModelNumber));
        pWssIfCapGetOldDB->CapwapIsGetAllDB.bNoOfRadio = OSIX_TRUE;

        if (WssIfProcessCapwapDBMsg
            (WSS_CAPWAP_GET_WTP_MODEL_ENTRY, pWssIfCapGetOldDB) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "CapwapCliSetWtpModel :CAPWAP DB access failed. \r\n");
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapGetOldDB);
            return OSIX_FAILURE;
        }
        pCapwapFsWtpModelEntry->MibObject.u4FsNoOfRadio =
            pWssIfCapGetOldDB->CapwapGetDB.WtpModelEntry.u1NumOfRadio;
        for (u1RadioCount = 1; u1RadioCount <=
             pWssIfCapGetOldDB->CapwapGetDB.WtpModelEntry.u1NumOfRadio;
             u1RadioCount++)
        {
            /* Check if the profile is already present */
            if ((nmhGetFsWtpRadioRowStatus (&ModelNumber, u1RadioCount,
                                            &i4FsWtpRadioRowStatus)) ==
                SNMP_SUCCESS)
            {
                /* Profile NOT present - CREATE */

                if (nmhTestv2FsWtpRadioRowStatus
                    (&u4ErrCode, &ModelNumber, u1RadioCount,
                     DESTROY) == SNMP_FAILURE)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "CapwapCliSetWtpModel : Delete existing entry failed \r\n");
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapGetOldDB);
                    return OSIX_FAILURE;
                }
                /* CREATE Profile */
                if (nmhSetFsWtpRadioRowStatus
                    (&ModelNumber, u1RadioCount, DESTROY) == SNMP_FAILURE)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "CapwapCliSetWtpModel : Delete existing entry failed. \r\n");
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapGetOldDB);
                    return OSIX_FAILURE;
                }
            }
        }

        pCapwapFsWtpModelEntry->MibObject.u4FsNoOfRadio =
            pWssIfCapDB->CapwapGetDB.WtpModelEntry.u1NumOfRadio;
        for (u1RadioCount = 1;
             u1RadioCount <=
             pWssIfCapDB->CapwapGetDB.WtpModelEntry.u1NumOfRadio;
             u1RadioCount++)
        {
            /* Check if the profile is already present */
            if ((nmhGetFsWtpRadioRowStatus (&ModelNumber, u1RadioCount,
                                            &i4FsWtpRadioRowStatus)) !=
                SNMP_SUCCESS)
            {
                /* Profile NOT present - CREATE */

                if (nmhTestv2FsWtpRadioRowStatus
                    (&u4ErrCode, &ModelNumber, u1RadioCount,
                     CREATE_AND_GO) == SNMP_FAILURE)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "CapwapCliSetWtpModel : Capwap DB Access failed1. \r\n");
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapGetOldDB);
                    return OSIX_FAILURE;
                }
                /* CREATE Profile */
                if (nmhSetFsWtpRadioRowStatus
                    (&ModelNumber, u1RadioCount, CREATE_AND_GO) == SNMP_FAILURE)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "CapwapCliSetWtpModel : Capwap DB Access failed2. \r\n");
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapGetOldDB);
                    return OSIX_FAILURE;
                }
            }
            else
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "CapwapCliSetWtpModel : Radio Entry exists. \r\n");
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapGetOldDB);
                return OSIX_FAILURE;
            }

        }
    }
    /*Wtp Model Mac Type */
    if (pCapwapIsSetFsWtpModelEntry->bFsCapwapWtpMacType != OSIX_FALSE)
    {
        pWssIfCapDB->CapwapIsGetAllDB.bMacType = OSIX_TRUE;
        pWssIfCapDB->CapwapGetDB.WtpModelEntry.u1MacType =
            (UINT1) pCapwapFsWtpModelEntry->MibObject.i4FsCapwapWtpMacType;
    }

    /*Wtp Model Tunnel Mode */
    if (pCapwapIsSetFsWtpModelEntry->bFsCapwapWtpTunnelMode != OSIX_FALSE)
    {
        pWssIfCapDB->CapwapIsGetAllDB.bWtpTunnelMode = OSIX_TRUE;
        pWssIfCapDB->CapwapIsGetAllDB.bTunnelMode = OSIX_TRUE;
        pWssIfCapDB->CapwapGetDB.WtpModelEntry.u1TunnelMode =
            pCapwapFsWtpModelEntry->MibObject.au1FsCapwapWtpTunnelMode[0];
    }

    /* Wtp Model Version */
    if (pCapwapIsSetFsWtpModelEntry->bFsCapwapImageName != OSIX_FALSE)
    {
        pWssIfCapDB->CapwapIsGetAllDB.bImageName = OSIX_TRUE;
        MEMCPY (&pWssIfCapDB->CapwapGetDB.WtpModelEntry.au1WtpImageName,
                &pCapwapFsWtpModelEntry->MibObject.au1FsCapwapImageName,
                pCapwapFsWtpModelEntry->MibObject.i4FsCapwapImageNameLen);
    }

    pWssIfCapDB->CapwapIsGetAllDB.bRowStatus = OSIX_TRUE;
    pWssIfCapDB->CapwapGetDB.u1RowStatus =
        (UINT1) pCapwapFsWtpModelEntry->MibObject.i4FsWtpModelRowStatus;
    if ((pCapwapIsSetFsWtpModelEntry->bFsCapwapWtpTunnelMode != OSIX_FALSE) ||
        (pCapwapIsSetFsWtpModelEntry->bFsCapwapWtpMacType != OSIX_FALSE) ||
        (pCapwapIsSetFsWtpModelEntry->bFsCapwapImageName != OSIX_FALSE) ||
        (pCapwapIsSetFsWtpModelEntry->bFsWtpModelRowStatus != OSIX_FALSE) ||
        (pCapwapIsSetFsWtpModelEntry->bFsNoOfRadio != OSIX_FALSE))
    {
        if (WssIfProcessCapwapDBMsg
            (WSS_CAPWAP_SET_WTP_MODEL_ENTRY, pWssIfCapDB) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "CapwapCliSetWtpModel :CAPWAP DB access failed. \r\n");
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapGetOldDB);
            return OSIX_FAILURE;
        }

    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapGetOldDB);
    return OSIX_SUCCESS;

}

INT4
CapwapCliCfgUpdateConfigRespRecv (UINT4 u4ResCode)
{
    gu1IsConfigResponseReceived = u4ResCode;

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  CapwapCreateProfileIdDB
 * Input       :  u4CapwapBaseWtpProfileId      
 * Descritpion :  This Routine will delete all the AP Profile dependency tables 
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 ***************************************************************************/
INT4
CapwapCreateProfileIdDB (UINT4 u4CapwapBaseWtpProfileId)
{
    INT4                i4RowStatus = TRUE;
    INT4                i4RowCreateOption = 1;
    tWssIfWtpLRConfigDB ConfigEntry;
    tCapwapFsCapwapWtpConfigEntry CapwapSetFsCapwapWtpConfigEntry;
    tCapwapIsSetFsCapwapWtpConfigEntry CapwapIsSetFsCapwapWtpConfigEntry;

    tCapwapFsCapwapLinkEncryptionEntry CapwapSetFsCapwapLinkEncryptionEntry;
    tCapwapIsSetFsCapwapLinkEncryptionEntry
        CapwapIsSetFsCapwapLinkEncryptionEntry;

    tCapwapFsCapwapDnsProfileEntry CapwapSetFsCapwapDnsProfileEntry;
    tCapwapIsSetFsCapwapDnsProfileEntry CapwapIsSetFsCapwapDnsProfileEntry;

    tCapwapFsWtpNativeVlanIdTable CapwapSetFsWtpNativeVlanIdTable;
    tCapwapIsSetFsWtpNativeVlanIdTable CapwapIsSetFsWtpNativeVlanIdTable;

    tCapwapFsWtpLocalRoutingTable CapwapSetFsWtpLocalRoutingTable;
    tCapwapIsSetFsWtpLocalRoutingTable CapwapIsSetFsWtpLocalRoutingTable;

    tCapwapFsCawapDiscStatsEntry CapwapSetFsCawapDiscStatsEntry;
    tCapwapIsSetFsCawapDiscStatsEntry CapwapIsSetFsCawapDiscStatsEntry;

    tCapwapFsCawapJoinStatsEntry CapwapSetFsCawapJoinStatsEntry;
    tCapwapIsSetFsCawapJoinStatsEntry CapwapIsSetFsCawapJoinStatsEntry;

    tCapwapFsCawapConfigStatsEntry CapwapSetFsCawapConfigStatsEntry;
    tCapwapIsSetFsCawapConfigStatsEntry CapwapIsSetFsCawapConfigStatsEntry;

    tCapwapFsCawapRunStatsEntry CapwapSetFsCawapRunStatsEntry;
    tCapwapIsSetFsCawapRunStatsEntry CapwapIsSetFsCawapRunStatsEntry;

    tCapwapFsCapwapWtpRebootStatisticsEntry
        CapwapSetFsCapwapWtpRebootStatisticsEntry;
    tCapwapIsSetFsCapwapWtpRebootStatisticsEntry
        CapwapIsSetFsCapwapWtpRebootStatisticsEntry;

    tCapwapFsCapwapWirelessBindingEntry CapwapSetFsCapwapWirelessBindingEntry;
    tCapwapIsSetFsCapwapWirelessBindingEntry
        CapwapIsSetFsCapwapWirelessBindingEntry;
    tWssIfWtpDhcpConfigDB WssIfWtpDhcpConfigDB;

    MEMSET (&ConfigEntry, 0, sizeof (ConfigEntry));
    MEMSET (&CapwapSetFsCapwapWirelessBindingEntry, 0,
            sizeof (tCapwapFsCapwapWirelessBindingEntry));
    MEMSET (&CapwapIsSetFsCapwapWirelessBindingEntry, 0,
            sizeof (tCapwapIsSetFsCapwapWirelessBindingEntry));
    MEMSET (&CapwapSetFsCapwapWtpRebootStatisticsEntry, 0,
            sizeof (tCapwapFsCapwapWtpRebootStatisticsEntry));
    MEMSET (&CapwapIsSetFsCapwapWtpRebootStatisticsEntry, 0,
            sizeof (tCapwapIsSetFsCapwapWtpRebootStatisticsEntry));
    MEMSET (&CapwapSetFsCawapRunStatsEntry, 0,
            sizeof (tCapwapFsCawapRunStatsEntry));
    MEMSET (&CapwapIsSetFsCawapRunStatsEntry, 0,
            sizeof (tCapwapIsSetFsCawapRunStatsEntry));
    MEMSET (&CapwapSetFsCapwapWtpConfigEntry, 0,
            sizeof (tCapwapFsCapwapWtpConfigEntry));
    MEMSET (&CapwapIsSetFsCapwapWtpConfigEntry, 0,
            sizeof (tCapwapIsSetFsCapwapWtpConfigEntry));
    MEMSET (&CapwapSetFsCapwapLinkEncryptionEntry, 0,
            sizeof (tCapwapFsCapwapLinkEncryptionEntry));
    MEMSET (&CapwapIsSetFsCapwapLinkEncryptionEntry, 0,
            sizeof (tCapwapIsSetFsCapwapLinkEncryptionEntry));
    MEMSET (&CapwapSetFsCapwapDnsProfileEntry, 0,
            sizeof (tCapwapFsCapwapDnsProfileEntry));
    MEMSET (&CapwapIsSetFsCapwapDnsProfileEntry, 0,
            sizeof (tCapwapIsSetFsCapwapDnsProfileEntry));
    MEMSET (&CapwapSetFsWtpNativeVlanIdTable, 0,
            sizeof (tCapwapFsWtpNativeVlanIdTable));
    MEMSET (&CapwapIsSetFsWtpNativeVlanIdTable, 0,
            sizeof (tCapwapIsSetFsWtpNativeVlanIdTable));
    MEMSET (&CapwapSetFsWtpLocalRoutingTable, 0,
            sizeof (tCapwapFsWtpLocalRoutingTable));
    MEMSET (&CapwapIsSetFsWtpLocalRoutingTable, 0,
            sizeof (tCapwapIsSetFsWtpLocalRoutingTable));
    MEMSET (&CapwapSetFsCawapDiscStatsEntry, 0,
            sizeof (tCapwapFsCawapDiscStatsEntry));
    MEMSET (&CapwapIsSetFsCawapDiscStatsEntry, 0,
            sizeof (tCapwapIsSetFsCawapDiscStatsEntry));
    MEMSET (&CapwapSetFsCawapJoinStatsEntry, 0,
            sizeof (tCapwapFsCawapJoinStatsEntry));
    MEMSET (&CapwapIsSetFsCawapJoinStatsEntry, 0,
            sizeof (tCapwapIsSetFsCawapJoinStatsEntry));
    MEMSET (&CapwapIsSetFsCawapConfigStatsEntry, 0,
            sizeof (tCapwapIsSetFsCawapConfigStatsEntry));
    MEMSET (&CapwapSetFsCawapConfigStatsEntry, 0,
            sizeof (tCapwapFsCawapConfigStatsEntry));
    MEMSET (&WssIfWtpDhcpConfigDB, 0, sizeof (tWssIfWtpDhcpConfigDB));

    CapwapSetFsCapwapWtpConfigEntry.MibObject.u4CapwapBaseWtpProfileId =
        u4CapwapBaseWtpProfileId;
    CapwapSetFsCapwapWtpConfigEntry.MibObject.i4FsCapwapWtpConfigRowStatus =
        CREATE_AND_WAIT;
    CapwapIsSetFsCapwapWtpConfigEntry.bCapwapBaseWtpProfileId = OSIX_TRUE;

    if (CapwapSetAllFsCapwapWtpConfigTable (&CapwapSetFsCapwapWtpConfigEntry,
                                            &CapwapIsSetFsCapwapWtpConfigEntry,
                                            i4RowStatus,
                                            i4RowCreateOption) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    CapwapSetFsCapwapLinkEncryptionEntry.MibObject.u4CapwapBaseWtpProfileId =
        u4CapwapBaseWtpProfileId;
    CapwapSetFsCapwapLinkEncryptionEntry.MibObject.i4FsCapwapEncryptChannel =
        CLI_FSCAPWAP_ENCRYPTION_CONTROL;
    CapwapSetFsCapwapLinkEncryptionEntry.MibObject.
        i4FsCapwapEncryptChannelRowStatus = CREATE_AND_WAIT;
    CapwapIsSetFsCapwapLinkEncryptionEntry.bCapwapBaseWtpProfileId = OSIX_TRUE;
    CapwapIsSetFsCapwapLinkEncryptionEntry.bFsCapwapEncryptChannel = OSIX_TRUE;

    if (CapwapSetAllFsCapwapLinkEncryptionTable
        (&CapwapSetFsCapwapLinkEncryptionEntry,
         &CapwapIsSetFsCapwapLinkEncryptionEntry, i4RowStatus,
         i4RowCreateOption) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    CapwapSetFsCapwapLinkEncryptionEntry.MibObject.i4FsCapwapEncryptChannel =
        CLI_FSCAPWAP_ENCRYPTION_DATA;
    CapwapSetFsCapwapLinkEncryptionEntry.MibObject.
        i4FsCapwapEncryptChannelStatus = DTLS_CFG_DISABLE;
    CapwapIsSetFsCapwapLinkEncryptionEntry.bFsCapwapEncryptChannelStatus =
        OSIX_TRUE;
    if (CapwapSetAllFsCapwapLinkEncryptionTable
        (&CapwapSetFsCapwapLinkEncryptionEntry,
         &CapwapIsSetFsCapwapLinkEncryptionEntry, i4RowStatus,
         i4RowCreateOption) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    CapwapSetFsCapwapDnsProfileEntry.MibObject.u4CapwapBaseWtpProfileId =
        u4CapwapBaseWtpProfileId;
    CapwapSetFsCapwapDnsProfileEntry.MibObject.i4FsCapwapDnsProfileRowStatus =
        CREATE_AND_WAIT;
    CapwapIsSetFsCapwapDnsProfileEntry.bCapwapBaseWtpProfileId = OSIX_TRUE;

    if (CapwapSetAllFsCapwapDnsProfileTable (&CapwapSetFsCapwapDnsProfileEntry,
                                             &CapwapIsSetFsCapwapDnsProfileEntry,
                                             i4RowStatus,
                                             i4RowCreateOption) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    CapwapSetFsWtpNativeVlanIdTable.MibObject.u4CapwapBaseWtpProfileId =
        u4CapwapBaseWtpProfileId;
    CapwapSetFsWtpNativeVlanIdTable.MibObject.i4FsWtpNativeVlanIdRowStatus =
        CREATE_AND_GO;
    CapwapIsSetFsWtpNativeVlanIdTable.bCapwapBaseWtpProfileId = OSIX_TRUE;

    if (CapwapSetAllFsWtpNativeVlanIdTable (&CapwapSetFsWtpNativeVlanIdTable,
                                            &CapwapIsSetFsWtpNativeVlanIdTable,
                                            i4RowStatus,
                                            i4RowCreateOption) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    CapwapSetFsWtpLocalRoutingTable.MibObject.u4CapwapBaseWtpProfileId =
        u4CapwapBaseWtpProfileId;
    CapwapSetFsWtpLocalRoutingTable.MibObject.i4FsWtpLocalRoutingRowStatus =
        CREATE_AND_GO;
    CapwapIsSetFsWtpLocalRoutingTable.bCapwapBaseWtpProfileId = OSIX_TRUE;
    CapwapSetFsWtpLocalRoutingTable.MibObject.i4FsWtpLocalRouting =
        CLI_FSCAPWAP_DISABLE;
    CapwapIsSetFsWtpLocalRoutingTable.bFsWtpLocalRouting = OSIX_TRUE;
    if (CapwapSetAllFsWtpLocalRoutingTable (&CapwapSetFsWtpLocalRoutingTable,
                                            &CapwapIsSetFsWtpLocalRoutingTable,
                                            i4RowStatus,
                                            i4RowCreateOption) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    CapwapSetFsCawapDiscStatsEntry.MibObject.u4CapwapBaseWtpProfileId =
        u4CapwapBaseWtpProfileId;
    CapwapSetFsCawapDiscStatsEntry.MibObject.i4FsCapwapDiscStatsRowStatus
        = CREATE_AND_GO;
    CapwapIsSetFsCawapDiscStatsEntry.bCapwapBaseWtpProfileId = OSIX_TRUE;

    if (CapwapSetAllFsCawapDiscStatsTable (&CapwapSetFsCawapDiscStatsEntry,
                                           &CapwapIsSetFsCawapDiscStatsEntry,
                                           i4RowStatus,
                                           i4RowCreateOption) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    CapwapSetFsCawapJoinStatsEntry.MibObject.u4CapwapBaseWtpProfileId =
        u4CapwapBaseWtpProfileId;
    CapwapSetFsCawapJoinStatsEntry.MibObject.i4FsCapwapJoinStatsRowStatus =
        CREATE_AND_GO;
    CapwapIsSetFsCawapJoinStatsEntry.bCapwapBaseWtpProfileId = OSIX_TRUE;
    if (CapwapSetAllFsCawapJoinStatsTable (&CapwapSetFsCawapJoinStatsEntry,
                                           &CapwapIsSetFsCawapJoinStatsEntry,
                                           i4RowStatus,
                                           i4RowCreateOption) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    CapwapSetFsCawapConfigStatsEntry.MibObject.u4CapwapBaseWtpProfileId =
        u4CapwapBaseWtpProfileId;
    CapwapSetFsCawapConfigStatsEntry.MibObject.i4FsCapwapConfigStatsRowStatus =
        CREATE_AND_GO;
    CapwapIsSetFsCawapConfigStatsEntry.bCapwapBaseWtpProfileId = OSIX_TRUE;

    if (CapwapSetAllFsCawapConfigStatsTable (&CapwapSetFsCawapConfigStatsEntry,
                                             &CapwapIsSetFsCawapConfigStatsEntry,
                                             i4RowStatus,
                                             i4RowCreateOption) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    CapwapSetFsCawapRunStatsEntry.MibObject.u4CapwapBaseWtpProfileId =
        u4CapwapBaseWtpProfileId;
    CapwapSetFsCawapRunStatsEntry.MibObject.i4FsCapwapRunStatsRowStatus =
        CREATE_AND_GO;
    CapwapIsSetFsCawapRunStatsEntry.bCapwapBaseWtpProfileId = OSIX_TRUE;

    if (CapwapSetAllFsCawapRunStatsTable (&CapwapSetFsCawapRunStatsEntry,
                                          &CapwapIsSetFsCawapRunStatsEntry,
                                          i4RowStatus,
                                          i4RowCreateOption) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

#ifdef WLC_WANTED
    WssIfWtpDhcpConfigDB.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    WssIfWtpDhcpConfigDB.i4WtpDhcpServerStatus = gDhcpServerStatus;
    WssIfWtpDhcpConfigDB.i4WtpDhcpRelayStatus = gDhcpRelayStatus;
    ConfigEntry.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    ConfigEntry.i4WtpFirewallStatus = 0;
    ConfigEntry.i4RowStatus = CREATE_AND_GO;
    if (WssIfWtpDhcpConfigAddEntry (&WssIfWtpDhcpConfigDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (WssIfWtpLRConfigAddEntry (&ConfigEntry) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
#endif

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  CapwapDeleteProfileIdDB
 * Input       :  u4CapwapBaseWtpProfileId      
 * Descritpion :  This Routine will delete all the AP Profile dependency tables 
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 ***************************************************************************/
INT4
CapwapDeleteProfileIdDB (UINT4 u4CapwapBaseWtpProfileId)
{
    UINT4               u4ErrorCode = 0;
    UINT4               u4RadioCount = 0, u4RadioId = 0;
    tWssIfCapDB        *pWssIfCapDB = NULL;
    INT4                i4RowStatus = OSIX_TRUE;
    INT4                i4RowCreateOption = OSIX_TRUE;

    tCapwapFsCapwapWtpConfigEntry CapwapSetFsCapwapWtpConfigEntry;
    tCapwapIsSetFsCapwapWtpConfigEntry CapwapIsSetFsCapwapWtpConfigEntry;

    tCapwapFsCapwapLinkEncryptionEntry CapwapSetFsCapwapLinkEncryptionEntry;

    tCapwapIsSetFsCapwapLinkEncryptionEntry
        CapwapIsSetFsCapwapLinkEncryptionEntry;

    tCapwapFsCapwapDnsProfileEntry CapwapSetFsCapwapDnsProfileEntry;
    tCapwapIsSetFsCapwapDnsProfileEntry CapwapIsSetFsCapwapDnsProfileEntry;

    tCapwapFsWtpNativeVlanIdTable CapwapSetFsWtpNativeVlanIdTable;
    tCapwapIsSetFsWtpNativeVlanIdTable CapwapIsSetFsWtpNativeVlanIdTable;

    tCapwapFsCawapDiscStatsEntry CapwapSetFsCawapDiscStatsEntry;
    tCapwapIsSetFsCawapDiscStatsEntry CapwapIsSetFsCawapDiscStatsEntry;

    tCapwapFsCawapJoinStatsEntry CapwapSetFsCawapJoinStatsEntry;
    tCapwapIsSetFsCawapJoinStatsEntry CapwapIsSetFsCawapJoinStatsEntry;

    tCapwapFsCawapConfigStatsEntry CapwapSetFsCawapConfigStatsEntry;
    tCapwapIsSetFsCawapConfigStatsEntry CapwapIsSetFsCawapConfigStatsEntry;

    tCapwapFsCawapRunStatsEntry CapwapSetFsCawapRunStatsEntry;
    tCapwapIsSetFsCawapRunStatsEntry CapwapIsSetFsCawapRunStatsEntry;

    tCapwapFsWtpLocalRoutingTable CapwapSetFsWtpLocalRoutingTable;
    tCapwapIsSetFsWtpLocalRoutingTable CapwapIsSetFsWtpLocalRoutingTable;

    tCapwapFsCapwapWtpRebootStatisticsEntry
        CapwapSetFsCapwapWtpRebootStatisticsEntry;
    tCapwapIsSetFsCapwapWtpRebootStatisticsEntry
        CapwapIsSetFsCapwapWtpRebootStatisticsEntry;

    tCapwapFsCapwapWirelessBindingEntry CapwapGetFsCapwapWirelessBindingEntry;
    tCapwapFsCapwapWirelessBindingEntry CapwapSetFsCapwapWirelessBindingEntry;
    tCapwapIsSetFsCapwapWirelessBindingEntry
        CapwapIsSetFsCapwapWirelessBindingEntry;
    tWssIfWtpDhcpConfigDB WssIfWtpDhcpConfigDB;
    tWssIfWtpLRConfigDB ConfigEntry;

    MEMSET (&CapwapGetFsCapwapWirelessBindingEntry, 0,
            sizeof (tCapwapFsCapwapWirelessBindingEntry));
    MEMSET (&CapwapSetFsCapwapWirelessBindingEntry, 0,
            sizeof (tCapwapFsCapwapWirelessBindingEntry));
    MEMSET (&CapwapIsSetFsCapwapWirelessBindingEntry, 0,
            sizeof (tCapwapIsSetFsCapwapWirelessBindingEntry));
    MEMSET (&CapwapSetFsCapwapWtpRebootStatisticsEntry, 0,
            sizeof (tCapwapFsCapwapWtpRebootStatisticsEntry));
    MEMSET (&CapwapIsSetFsCapwapWtpRebootStatisticsEntry, 0,
            sizeof (tCapwapIsSetFsCapwapWtpRebootStatisticsEntry));
    MEMSET (&CapwapSetFsCawapRunStatsEntry, 0,
            sizeof (tCapwapFsCawapRunStatsEntry));
    MEMSET (&CapwapIsSetFsCawapRunStatsEntry, 0,
            sizeof (tCapwapIsSetFsCawapRunStatsEntry));
    MEMSET (&CapwapSetFsCapwapWtpConfigEntry, 0,
            sizeof (tCapwapFsCapwapWtpConfigEntry));
    MEMSET (&CapwapIsSetFsCapwapWtpConfigEntry, 0,
            sizeof (tCapwapIsSetFsCapwapWtpConfigEntry));
    MEMSET (&CapwapSetFsCapwapLinkEncryptionEntry, 0,
            sizeof (tCapwapFsCapwapLinkEncryptionEntry));
    MEMSET (&CapwapIsSetFsCapwapLinkEncryptionEntry, 0,
            sizeof (tCapwapIsSetFsCapwapLinkEncryptionEntry));
    MEMSET (&CapwapSetFsCapwapDnsProfileEntry, 0,
            sizeof (tCapwapFsCapwapDnsProfileEntry));
    MEMSET (&CapwapIsSetFsCapwapDnsProfileEntry, 0,
            sizeof (tCapwapIsSetFsCapwapDnsProfileEntry));
    MEMSET (&CapwapSetFsWtpNativeVlanIdTable, 0,
            sizeof (tCapwapFsWtpNativeVlanIdTable));
    MEMSET (&CapwapIsSetFsWtpNativeVlanIdTable, 0,
            sizeof (tCapwapIsSetFsWtpNativeVlanIdTable));
    MEMSET (&CapwapSetFsCawapDiscStatsEntry, 0,
            sizeof (tCapwapFsCawapDiscStatsEntry));
    MEMSET (&CapwapIsSetFsCawapDiscStatsEntry, 0,
            sizeof (tCapwapIsSetFsCawapDiscStatsEntry));
    MEMSET (&CapwapSetFsCawapJoinStatsEntry, 0,
            sizeof (tCapwapFsCawapJoinStatsEntry));
    MEMSET (&CapwapIsSetFsCawapJoinStatsEntry, 0,
            sizeof (tCapwapIsSetFsCawapJoinStatsEntry));
    MEMSET (&CapwapIsSetFsCawapConfigStatsEntry, 0,
            sizeof (tCapwapIsSetFsCawapConfigStatsEntry));
    MEMSET (&CapwapSetFsCawapConfigStatsEntry, 0,
            sizeof (tCapwapFsCawapConfigStatsEntry));
    MEMSET (&CapwapIsSetFsWtpLocalRoutingTable, 0,
            sizeof (tCapwapIsSetFsWtpLocalRoutingTable));
    MEMSET (&WssIfWtpDhcpConfigDB, 0, sizeof (tWssIfWtpDhcpConfigDB));
    MEMSET (&ConfigEntry, 0, sizeof (tWssIfWtpLRConfigDB));

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapDB, 0, sizeof (tWssIfCapDB));

    CapwapSetFsCapwapWtpConfigEntry.MibObject.u4CapwapBaseWtpProfileId =
        u4CapwapBaseWtpProfileId;
    CapwapSetFsCapwapWtpConfigEntry.MibObject.i4FsCapwapWtpConfigRowStatus =
        DESTROY;
    CapwapIsSetFsCapwapWtpConfigEntry.bCapwapBaseWtpProfileId = OSIX_TRUE;

    if (CapwapSetAllFsCapwapWtpConfigTable (&CapwapSetFsCapwapWtpConfigEntry,
                                            &CapwapIsSetFsCapwapWtpConfigEntry,
                                            i4RowStatus,
                                            i4RowCreateOption) != OSIX_SUCCESS)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
        return OSIX_FAILURE;
    }

    CapwapSetFsCapwapLinkEncryptionEntry.MibObject.u4CapwapBaseWtpProfileId =
        u4CapwapBaseWtpProfileId;
    CapwapSetFsCapwapLinkEncryptionEntry.MibObject.i4FsCapwapEncryptChannel =
        CLI_FSCAPWAP_ENCRYPTION_CONTROL;
    CapwapSetFsCapwapLinkEncryptionEntry.MibObject.
        i4FsCapwapEncryptChannelRowStatus = DESTROY;
    CapwapIsSetFsCapwapLinkEncryptionEntry.bCapwapBaseWtpProfileId = OSIX_TRUE;
    CapwapIsSetFsCapwapLinkEncryptionEntry.bFsCapwapEncryptChannel = OSIX_TRUE;

    if (CapwapSetAllFsCapwapLinkEncryptionTable
        (&CapwapSetFsCapwapLinkEncryptionEntry,
         &CapwapIsSetFsCapwapLinkEncryptionEntry, i4RowStatus,
         i4RowCreateOption) != OSIX_SUCCESS)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
        return OSIX_FAILURE;
    }

    CapwapSetFsCapwapLinkEncryptionEntry.MibObject.i4FsCapwapEncryptChannel =
        CLI_FSCAPWAP_ENCRYPTION_DATA;
    if (CapwapSetAllFsCapwapLinkEncryptionTable
        (&CapwapSetFsCapwapLinkEncryptionEntry,
         &CapwapIsSetFsCapwapLinkEncryptionEntry, i4RowStatus,
         i4RowCreateOption) != OSIX_SUCCESS)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
        return OSIX_FAILURE;
    }

    CapwapSetFsCapwapDnsProfileEntry.MibObject.u4CapwapBaseWtpProfileId =
        u4CapwapBaseWtpProfileId;
    CapwapSetFsCapwapDnsProfileEntry.MibObject.i4FsCapwapDnsProfileRowStatus =
        DESTROY;
    CapwapIsSetFsCapwapDnsProfileEntry.bCapwapBaseWtpProfileId = OSIX_TRUE;

    if (CapwapSetAllFsCapwapDnsProfileTable (&CapwapSetFsCapwapDnsProfileEntry,
                                             &CapwapIsSetFsCapwapDnsProfileEntry,
                                             i4RowStatus,
                                             i4RowCreateOption) != OSIX_SUCCESS)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
        return OSIX_FAILURE;
    }

    CapwapSetFsWtpNativeVlanIdTable.MibObject.u4CapwapBaseWtpProfileId =
        u4CapwapBaseWtpProfileId;
    CapwapSetFsWtpNativeVlanIdTable.MibObject.i4FsWtpNativeVlanIdRowStatus =
        DESTROY;
    CapwapIsSetFsWtpNativeVlanIdTable.bCapwapBaseWtpProfileId = OSIX_TRUE;
    if (CapwapSetAllFsWtpNativeVlanIdTable (&CapwapSetFsWtpNativeVlanIdTable,
                                            &CapwapIsSetFsWtpNativeVlanIdTable,
                                            i4RowStatus,
                                            i4RowCreateOption) != OSIX_SUCCESS)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
        return OSIX_FAILURE;
    }

    CapwapSetFsWtpLocalRoutingTable.MibObject.u4CapwapBaseWtpProfileId =
        u4CapwapBaseWtpProfileId;
    CapwapSetFsWtpLocalRoutingTable.MibObject.i4FsWtpLocalRoutingRowStatus =
        DESTROY;
    CapwapIsSetFsWtpLocalRoutingTable.bCapwapBaseWtpProfileId = OSIX_TRUE;
    if (CapwapSetAllFsWtpLocalRoutingTable (&CapwapSetFsWtpLocalRoutingTable,
                                            &CapwapIsSetFsWtpLocalRoutingTable,
                                            i4RowStatus,
                                            i4RowCreateOption) != OSIX_SUCCESS)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
        return OSIX_FAILURE;
    }

    CapwapSetFsCawapDiscStatsEntry.MibObject.u4CapwapBaseWtpProfileId
        = u4CapwapBaseWtpProfileId;
    CapwapSetFsCawapDiscStatsEntry.MibObject.i4FsCapwapDiscStatsRowStatus
        = DESTROY;
    CapwapIsSetFsCawapDiscStatsEntry.bCapwapBaseWtpProfileId = OSIX_TRUE;

    if (CapwapSetAllFsCawapDiscStatsTable (&CapwapSetFsCawapDiscStatsEntry,
                                           &CapwapIsSetFsCawapDiscStatsEntry,
                                           i4RowStatus,
                                           i4RowCreateOption) != OSIX_SUCCESS)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
        return OSIX_FAILURE;
    }

    CapwapSetFsCawapJoinStatsEntry.MibObject.u4CapwapBaseWtpProfileId =
        u4CapwapBaseWtpProfileId;
    CapwapSetFsCawapJoinStatsEntry.MibObject.i4FsCapwapJoinStatsRowStatus =
        DESTROY;
    CapwapIsSetFsCawapJoinStatsEntry.bCapwapBaseWtpProfileId = OSIX_TRUE;
    if (CapwapSetAllFsCawapJoinStatsTable (&CapwapSetFsCawapJoinStatsEntry,
                                           &CapwapIsSetFsCawapJoinStatsEntry,
                                           i4RowStatus,
                                           i4RowCreateOption) != OSIX_SUCCESS)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
        return OSIX_FAILURE;
    }

    CapwapSetFsCawapConfigStatsEntry.MibObject.u4CapwapBaseWtpProfileId =
        u4CapwapBaseWtpProfileId;
    CapwapSetFsCawapConfigStatsEntry.MibObject.i4FsCapwapConfigStatsRowStatus =
        DESTROY;
    CapwapIsSetFsCawapConfigStatsEntry.bCapwapBaseWtpProfileId = OSIX_TRUE;

    if (CapwapSetAllFsCawapConfigStatsTable (&CapwapSetFsCawapConfigStatsEntry,
                                             &CapwapIsSetFsCawapConfigStatsEntry,
                                             i4RowStatus,
                                             i4RowCreateOption) != OSIX_SUCCESS)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
        return OSIX_FAILURE;
    }

    CapwapSetFsCawapRunStatsEntry.MibObject.u4CapwapBaseWtpProfileId =
        u4CapwapBaseWtpProfileId;
    CapwapSetFsCawapRunStatsEntry.MibObject.i4FsCapwapRunStatsRowStatus =
        DESTROY;
    CapwapIsSetFsCawapRunStatsEntry.bCapwapBaseWtpProfileId = OSIX_TRUE;

    if (CapwapSetAllFsCawapRunStatsTable (&CapwapSetFsCawapRunStatsEntry,
                                          &CapwapIsSetFsCawapRunStatsEntry,
                                          i4RowStatus,
                                          i4RowCreateOption) != OSIX_SUCCESS)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
        return OSIX_FAILURE;
    }

    CapwapSetFsCapwapWirelessBindingEntry.MibObject.u4CapwapBaseWtpProfileId =
        u4CapwapBaseWtpProfileId;
    CapwapSetFsCapwapWirelessBindingEntry.MibObject.
        i4FsCapwapWirelessBindingRowStatus = DESTROY;

    pWssIfCapDB->CapwapIsGetAllDB.bWtpProfileId = OSIX_TRUE;
    pWssIfCapDB->CapwapGetDB.u2ProfileId = (UINT2) u4CapwapBaseWtpProfileId;
    pWssIfCapDB->CapwapIsGetAllDB.bRowStatus = OSIX_TRUE;

    if (pWssIfCapDB->CapwapGetDB.u1RowStatus == NOT_READY)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
        return OSIX_SUCCESS;
    }

    pWssIfCapDB->CapwapGetDB.u1RowStatus = DESTROY;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_INTERNAL_ID,
                                 pWssIfCapDB) == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Get Internal Id "
                    "from CAPWAP DB \r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
        return OSIX_FAILURE;
    }

    /* For this Internal Id read the radio count */
    pWssIfCapDB->CapwapGetDB.u2WtpProfileId = (UINT2) u4CapwapBaseWtpProfileId;
    pWssIfCapDB->CapwapIsGetAllDB.bNoOfRadio = OSIX_TRUE;
    pWssIfCapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;

    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                 pWssIfCapDB) == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Get ProfileDB\r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
        return OSIX_FAILURE;
    }

    u4RadioCount = pWssIfCapDB->CapwapGetDB.u1WtpNoofRadio;

    for (u4RadioId = 1; u4RadioId <= u4RadioCount; u4RadioId++)
    {
        CapwapGetFsCapwapWirelessBindingEntry.MibObject.
            u4CapwapBaseWtpProfileId = u4CapwapBaseWtpProfileId;
        CapwapGetFsCapwapWirelessBindingEntry.MibObject.
            u4CapwapBaseWirelessBindingRadioId = u4RadioId;
        if (CapwapGetAllFsCapwapWirelessBindingTable
            (&CapwapGetFsCapwapWirelessBindingEntry) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapGetAllCapwapBaseWirelessBindingTable: Entry doesn't exist\r\n");
            return OSIX_FAILURE;
        }

        CapwapGetFsCapwapWirelessBindingEntry.MibObject.
            i4FsCapwapWirelessBindingVirtualRadioIfIndex =
            CapwapSetFsCapwapWirelessBindingEntry.MibObject.
            i4FsCapwapWirelessBindingVirtualRadioIfIndex;

        CapwapSetFsCapwapWirelessBindingEntry.MibObject.
            u4CapwapBaseWtpProfileId = u4CapwapBaseWtpProfileId;
        CapwapSetFsCapwapWirelessBindingEntry.MibObject.
            i4FsCapwapWirelessBindingRowStatus = DESTROY;
        CapwapSetFsCapwapWirelessBindingEntry.MibObject.
            u4CapwapBaseWirelessBindingRadioId = u4RadioId;
        CapwapIsSetFsCapwapWirelessBindingEntry.
            bCapwapBaseWtpProfileId = OSIX_TRUE;
        CapwapIsSetFsCapwapWirelessBindingEntry.
            bCapwapBaseWirelessBindingRadioId = OSIX_TRUE;
        CapwapIsSetFsCapwapWirelessBindingEntry.
            bFsCapwapWirelessBindingRowStatus = OSIX_TRUE;

        if (CapwapTestAllFsCapwapWirelessBindingTable (&u4ErrorCode,
                                                       &CapwapSetFsCapwapWirelessBindingEntry,
                                                       &CapwapIsSetFsCapwapWirelessBindingEntry,
                                                       i4RowStatus,
                                                       i4RowCreateOption) !=
            OSIX_SUCCESS)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
            return OSIX_FAILURE;
        }

        if (CapwapSetAllFsCapwapWirelessBindingTable
            (&CapwapSetFsCapwapWirelessBindingEntry,
             &CapwapIsSetFsCapwapWirelessBindingEntry, i4RowStatus,
             i4RowCreateOption) != OSIX_SUCCESS)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
            return OSIX_FAILURE;
        }
    }
#ifdef WLC_WANTED
    WssIfWtpDhcpConfigDB.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    if (WssIfWtpDhcpConfigDeleteEntry (&WssIfWtpDhcpConfigDB) != OSIX_SUCCESS)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
        return OSIX_FAILURE;
    }
    ConfigEntry.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    if (WssIfWtpLRConfigDeleteEntry (&ConfigEntry) != OSIX_SUCCESS)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
        return OSIX_FAILURE;
    }

    /* Delete L3 SUB IF entry created with ths profile */
    if ((WssIfWtpL3SubIfDeleteEntryOnProfile (u4CapwapBaseWtpProfileId)) !=
        OSIX_SUCCESS)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
        return OSIX_FAILURE;
    }
#endif
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  CapwapCreateAutoProfile
 * Input       :  u4CapwapBaseWtpProfileId      
 * Descritpion :  This Routine will create a AP profile dynamically in Auto mode
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 ***************************************************************************/
UINT1
CapwapCreateAutoProfile (tMacAddr MacAddr, UINT1 *pu1WtpModelName)
{
    tCapwapCapwapBaseWtpProfileEntry CapwapSetCapwapBaseWtpProfileEntry;
    tCapwapIsSetCapwapBaseWtpProfileEntry CapwapIsSetCapwapBaseWtpProfileEntry;
    tCapwapFsWtpLocalRoutingTable CapwapSetFsWtpLocalRoutingTable;
    tCapwapIsSetFsWtpLocalRoutingTable CapwapIsSetFsWtpLocalRoutingTable;
    INT4                i4RowCreateOption = 1;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT1               au1ProfileName[CAPWAP_MAX_PROFILE_NAME];
    UINT1               au1MacStr[18];
    UINT4               u4ErrorCode = 0;
    UINT4               u4ProfileId = 0;
    INT4                i4RowStatus = 0;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE);
    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&CapwapSetFsWtpLocalRoutingTable, 0,
            sizeof (tCapwapFsWtpLocalRoutingTable));
    MEMSET (&CapwapIsSetFsWtpLocalRoutingTable, 0,
            sizeof (tCapwapIsSetFsWtpLocalRoutingTable));
    MEMSET (&CapwapSetCapwapBaseWtpProfileEntry, 0,
            sizeof (tCapwapCapwapBaseWtpProfileEntry));
    MEMSET (&CapwapIsSetCapwapBaseWtpProfileEntry, 0,
            sizeof (tCapwapIsSetCapwapBaseWtpProfileEntry));
    MEMSET (au1ProfileName, 0, CAPWAP_MAX_PROFILE_NAME);
    MEMSET (au1MacStr, 0, 18);

    for (u4ProfileId = 1; u4ProfileId <= CAPWAP_MAX_WTP_PROFILE; u4ProfileId++)
    {
        if (u4ProfileId == CAPWAP_MAX_WTP_PROFILE)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
        if (nmhGetCapwapBaseWtpProfileRowStatus (u4ProfileId, &i4RowStatus)
            != SNMP_SUCCESS)
        {
            CapwapSetCapwapBaseWtpProfileEntry.MibObject.
                i4CapwapBaseWtpProfileRowStatus = CREATE_AND_WAIT;
            i4RowStatus = 0;
            break;
        }
    }

    CapwapSetCapwapBaseWtpProfileEntry.MibObject.u4CapwapBaseWtpProfileId =
        u4ProfileId;

    SPRINTF ((CHR1 *) au1ProfileName, "pro%d", u4ProfileId);
    CapwapSetCapwapBaseWtpProfileEntry.MibObject.i4CapwapBaseWtpProfileNameLen =
        (INT4) STRLEN (au1ProfileName);

    MEMCPY (CapwapSetCapwapBaseWtpProfileEntry.MibObject.
            au1CapwapBaseWtpProfileName, au1ProfileName,
            STRLEN (au1ProfileName));
    SPRINTF ((CHR1 *) CapwapSetCapwapBaseWtpProfileEntry.MibObject.
             au1CapwapBaseWtpProfileWtpName, "wtp%d", u4ProfileId);
    CapwapSetCapwapBaseWtpProfileEntry.MibObject.
        i4CapwapBaseWtpProfileWtpNameLen =
        (INT4) STRLEN (CapwapSetCapwapBaseWtpProfileEntry.MibObject.
                       au1CapwapBaseWtpProfileWtpName);

    MEMCPY (CapwapSetCapwapBaseWtpProfileEntry.MibObject.
            au1CapwapBaseWtpProfileWtpModelNumber, pu1WtpModelName,
            CAPWAP_MAX_PROFILE_NAME);
    CapwapSetCapwapBaseWtpProfileEntry.MibObject.
        i4CapwapBaseWtpProfileWtpModelNumberLen =
        (INT4) STRLEN (pu1WtpModelName);

    SPRINTF ((CHR1 *) CapwapSetCapwapBaseWtpProfileEntry.MibObject.
             au1CapwapBaseWtpProfileWtpLocation, "indoor%d", u4ProfileId);
    CapwapSetCapwapBaseWtpProfileEntry.MibObject.
        i4CapwapBaseWtpProfileWtpLocationLen =
        (INT4) STRLEN (CapwapSetCapwapBaseWtpProfileEntry.MibObject.
                       au1CapwapBaseWtpProfileWtpLocation);

    PrintMacAddress (MacAddr, au1MacStr);
    CapwapSetCapwapBaseWtpProfileEntry.MibObject.
        i4CapwapBaseWtpProfileWtpMacAddressLen = 17;

    MEMCPY (CapwapSetCapwapBaseWtpProfileEntry.MibObject.
            au1CapwapBaseWtpProfileWtpMacAddress, au1MacStr,
            CapwapSetCapwapBaseWtpProfileEntry.MibObject.
            i4CapwapBaseWtpProfileWtpMacAddressLen);

    CapwapIsSetCapwapBaseWtpProfileEntry.bCapwapBaseWtpProfileId = OSIX_TRUE;
    CapwapIsSetCapwapBaseWtpProfileEntry.bCapwapBaseWtpProfileName = OSIX_TRUE;
    CapwapIsSetCapwapBaseWtpProfileEntry.bCapwapBaseWtpProfileWtpMacAddress =
        OSIX_TRUE;
    CapwapIsSetCapwapBaseWtpProfileEntry.bCapwapBaseWtpProfileWtpModelNumber =
        OSIX_TRUE;
    CapwapIsSetCapwapBaseWtpProfileEntry.bCapwapBaseWtpProfileWtpName =
        OSIX_TRUE;
    CapwapIsSetCapwapBaseWtpProfileEntry.bCapwapBaseWtpProfileWtpLocation =
        OSIX_TRUE;

    CapwapSetCapwapBaseWtpProfileEntry.u4SystemUpTime = OsixGetSysUpTime ();
    CapwapSetCapwapBaseWtpProfileEntry.MibObject.
        i4CapwapBaseWtpProfileRowStatus = CREATE_AND_WAIT;
    CapwapIsSetCapwapBaseWtpProfileEntry.bCapwapBaseWtpProfileRowStatus =
        OSIX_TRUE;
    if (CapwapTestAllCapwapBaseWtpProfileTable
        (&u4ErrorCode, &CapwapSetCapwapBaseWtpProfileEntry,
         &CapwapIsSetCapwapBaseWtpProfileEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    if (CapwapSetAllCapwapBaseWtpProfileTable
        (&CapwapSetCapwapBaseWtpProfileEntry,
         &CapwapIsSetCapwapBaseWtpProfileEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    MEMSET (&CapwapIsSetCapwapBaseWtpProfileEntry, 0,
            sizeof (tCapwapIsSetCapwapBaseWtpProfileEntry));
    CapwapSetCapwapBaseWtpProfileEntry.MibObject.u4CapwapBaseWtpProfileId =
        u4ProfileId;
    CapwapSetCapwapBaseWtpProfileEntry.MibObject.
        i4CapwapBaseWtpProfileRowStatus = ACTIVE;
    CapwapIsSetCapwapBaseWtpProfileEntry.bCapwapBaseWtpProfileRowStatus =
        OSIX_TRUE;
    if (CapwapTestAllCapwapBaseWtpProfileTable
        (&u4ErrorCode, &CapwapSetCapwapBaseWtpProfileEntry,
         &CapwapIsSetCapwapBaseWtpProfileEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    if (CapwapSetAllCapwapBaseWtpProfileTable
        (&CapwapSetCapwapBaseWtpProfileEntry,
         &CapwapIsSetCapwapBaseWtpProfileEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    MEMCPY (pWssIfCapwapDB->CapwapGetDB.WtpModelEntry.au1ModelNumber,
            pu1WtpModelName, STRLEN (pu1WtpModelName));
    pWssIfCapwapDB->CapwapIsGetAllDB.bLocalRouting = OSIX_TRUE;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_WTP_MODEL_ENTRY, pWssIfCapwapDB)
        == OSIX_FAILURE)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    CapwapSetFsWtpLocalRoutingTable.MibObject.u4CapwapBaseWtpProfileId =
        u4ProfileId;
    CapwapSetFsWtpLocalRoutingTable.MibObject.i4FsWtpLocalRoutingRowStatus =
        ACTIVE;
    CapwapIsSetFsWtpLocalRoutingTable.bCapwapBaseWtpProfileId = OSIX_TRUE;
    CapwapIsSetFsWtpLocalRoutingTable.bFsWtpLocalRoutingRowStatus = OSIX_TRUE;
    CapwapSetFsWtpLocalRoutingTable.MibObject.i4FsWtpLocalRouting =
        (INT4) pWssIfCapwapDB->CapwapGetDB.WtpModelEntry.u1LocalRouting;
    CapwapIsSetFsWtpLocalRoutingTable.bFsWtpLocalRouting = OSIX_TRUE;
    if (CapwapSetAllFsWtpLocalRoutingTable (&CapwapSetFsWtpLocalRoutingTable,
                                            &CapwapIsSetFsWtpLocalRoutingTable,
                                            i4RowStatus,
                                            i4RowCreateOption) != OSIX_SUCCESS)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    CapwapCreateBinding (pu1WtpModelName, u4ProfileId);
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  CapwapCreateBinding
 * Descritpion :  This routine will start the binding creation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 ***************************************************************************/

UINT1
CapwapCreateBinding (UINT1 *pu1WtpModelName, UINT4 u4ProfileId)
{
    tWssIfCapDB        *pWssIfCapDB = NULL;
    UINT4               u4ErrorCode = 0;
    UINT2               u2WtpProfileId = 0;
    UINT2               u2ProfileId = 0;
    UINT2               u2Index = 0;
    UINT4               u4RadioCount = 0;
    INT4                i4RowStatus = 0;
    tCapwapBindingTable *pCapwapBindingTable = NULL;
    tCapwapBindingTable CapwapBindingTable;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapDB, OSIX_FAILURE);
    MEMSET (pWssIfCapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&CapwapBindingTable, 0, sizeof (tCapwapBindingTable));

    u2ProfileId = (UINT2) u4ProfileId;
    if (CapwapGetInternalProfildId ((UINT2 *) &u2ProfileId, &u2WtpProfileId)
        != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Get ProfileDB\r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
        return OSIX_FAILURE;
    }

    u4RadioCount = pWssIfCapDB->CapwapGetDB.u1WtpNoofRadio;

    MEMCPY (CapwapBindingTable.au1ModelNumber, pu1WtpModelName,
            STRLEN (pu1WtpModelName - 1));

    pCapwapBindingTable = CapwapGetFirstFsWlanBindingTable ();
    if (pCapwapBindingTable == NULL)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
        return OSIX_SUCCESS;
    }
    do
    {
        pWssIfCapDB->CapwapGetDB.u2WtpInternalId = u2WtpProfileId;
        pWssIfCapDB->CapwapGetDB.u1RadioId = pCapwapBindingTable->u1RadioId;
        pWssIfCapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
        pWssIfCapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;

        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                     pWssIfCapDB) == OSIX_FAILURE)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
            return OSIX_SUCCESS;
        }
        if (MEMCMP (pu1WtpModelName, pCapwapBindingTable->au1ModelNumber,
                    MAX_WTP_MODELNUMBER_LEN) == 0)
        {
            if (WssifStartBinding (pCapwapBindingTable, pWssIfCapDB) !=
                OSIX_SUCCESS)
            {
            }
        }
        MEMSET (&CapwapBindingTable, 0, sizeof (tCapwapBindingTable));
        MEMCPY (&CapwapBindingTable, pCapwapBindingTable,
                sizeof (tCapwapBindingTable));
        pCapwapBindingTable =
            CapwapGetNextFsWlanBindingTable (&CapwapBindingTable);
    }
    while (pCapwapBindingTable != NULL);
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
    UNUSED_PARAM (i4RowStatus);
    UNUSED_PARAM (u4RadioCount);
    UNUSED_PARAM (u2Index);
    UNUSED_PARAM (u4ErrorCode);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  UtilGetIntfNameFromMac
 * Descritpion :  This routine give the interface name from which the given 
 *           mac was learnt
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 ****************************************************************************/
INT4
UtilGetIntfNameFromMac (tMacAddr Mac, UINT1 *pu1IntfName)
{
    UINT4               u4ContextId = 0;
    UINT4               u4FdbId = 1;
    UINT4               u4FdbPort = 0;
#ifdef LNXIP4_WANTED
    UINT1              *pu1PortName = NULL;
#endif
    if (nmhGetFsDot1qTpFdbPort (u4ContextId, u4FdbId,
                                Mac, (INT4 *) &u4FdbPort) == SNMP_SUCCESS)
    {
        if (u4FdbPort != 0)
        {
#ifdef LNXIP4_WANTED
            pu1PortName = CfaGddGetLnxIntfnameForPort ((UINT2) u4FdbPort);
            if (pu1PortName != NULL)
            {
                MEMCPY (pu1IntfName, pu1PortName, CAPWAP_INTERFACE_LEN);
                return OSIX_SUCCESS;
            }
#endif
        }
    }
    UNUSED_PARAM (pu1IntfName);
    return OSIX_FAILURE;
}

#endif
