/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 * $Id: capwapecho.c,v 1.3 2017/11/24 10:37:03 siva Exp $       
 * Description: This file contains the CAPWAP Join state routines
 *
 *******************************************************************/
#ifndef __CAPWAP_ECHO_C__
#define __CAPWAP_ECHO_C__
#include "capwapinc.h"
static UINT4        gu4EchoReqReceived;
static UINT4        gu4EchoRspReceived;
/*****************************************************************************
 *                                                                           *
 * Function     : CapwapAssembleEchoResponse                                 *
 *                                                                           *
 * Description  : Assemble the capwap Control packet structure to            *
 *                transmittable  format.                                     *
 *                                                                           *
 * Input        : Capwap Control message structure                           *
 *                                                                           *
 * Output       : Capwap control message to be transmitted out               *
 *                                                                           *
 * Returns      : SUCCESS/FAILURE                                            *
 *****************************************************************************/
INT4
CapwapAssembleEchoResponse (tEchoRsp * pEchoResp, UINT1 *pMesgOut)
{
    UINT1              *pu1Buf = NULL;

    CAPWAP_FN_ENTRY ();
    pu1Buf = pMesgOut;
    CAPWAP_MEMSET (pu1Buf, CAPWAP_INIT_VAL, CAPWAP_MAX_PKT_LEN);
    /* Assemble capwap Header */
    AssembleCapwapHeader (pu1Buf, &(pEchoResp->capwapHdr));
    /* Move the buffer pointer */
    pu1Buf += (pEchoResp->capwapHdr.u2HdrLen);

    /* Assemble Control header */
    AssembleCapwapControlHeader (pu1Buf,
                                 CAPWAP_ECHO_RSP,
                                 pEchoResp->u1SeqNum,
                                 pEchoResp->u2CapwapMsgElemenLen,
                                 pEchoResp->u1NumMsgBlocks);
    pu1Buf += CAPWAP_CTRL_HEADER_SIZE;
    /* Mandatory elements */
    /* None */
    /* Assemble the Optional Message elements */
    if (pEchoResp->vendSpec.isOptional)
    {
        CapwapAssembleVendorSpecificPayload (pu1Buf, &(pEchoResp->vendSpec));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN + (pEchoResp->vendSpec.u2MsgEleLen));
    }
    CAPWAP_FN_EXIT ();
    return CAPWAP_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapProcessEchoRequest                          */
/*  Description     : The function processes the received CAPWAP        */
/*                    Echo request                                      */
/*  Input(s)        : joinReqMsg - CAPWAP Join request packet received  */
/*                    pSessEntry - Pointer to the session data base     */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
CapwapProcessEchoRequest (tCRU_BUF_CHAIN_HEADER * pBuf,
                          tCapwapControlPacket * pEchoReqMsg,
                          tRemoteSessionManager * pSessEntry)
{
    INT4                i4Status;
    tEchoRsp            EchoResp;
    tWssIfPMDB          WssIfPMDB;
    UINT1              *pu1TxBuf = NULL, *pRcvdBuf = NULL;
    UINT4               u4MsgLen = 0;
    tCRU_BUF_CHAIN_HEADER *pTxBuf = NULL;
    UINT1               u1IsNotLinear = OSIX_FALSE;
    UINT2               u2PacketLen;

    CAPWAP_FN_ENTRY ();
    /* Counter */
    gu4EchoReqReceived++;
    CAPWAP_TRC1 (CAPWAP_STATS_TRC, "EchoRequest Received = %d \r\n",
                 gu4EchoReqReceived);
    SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4CapwapSysLogId,
                  "EchoRequest Received = %d \r\n", gu4EchoReqReceived));
    if (pSessEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "CAPWAP Session Entry"
                    "is NULL, retrun FAILURE \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "CAPWAP Session Entry" "is NULL, retrun FAILURE \r\n"));
        return OSIX_FAILURE;
    }
    MEMSET (&WssIfPMDB, 0, sizeof (tWssIfPMDB));
    WssIfPMDB.u2ProfileId = pSessEntry->u2IntProfileId;
    WssIfPMDB.WssPmWLCCapwapStatsSetFlag.bEchoStats = OSIX_TRUE;
    WssIfPMDB.WssPmWLCCapwapStatsSetFlag.bRunUpdateReqReceived = OSIX_TRUE;

    if (WssIfProcessPMDBMsg (WSS_PM_WLC_CAPWAP_RUN_STATS_SET,
                             &WssIfPMDB) != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to update PM db\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to update PM db\r\n"));
    }

    pSessEntry->u1EchoCount = 0;
    MEMSET (&EchoResp, 0, sizeof (tEchoRsp));

    u2PacketLen = (UINT2) CAPWAP_GET_BUF_LEN (pBuf);
    pRcvdBuf = CAPWAP_BUF_IF_LINEAR (pBuf, 0, u2PacketLen);
    if (pRcvdBuf == NULL)
    {
        pRcvdBuf = (UINT1 *) MemAllocMemBlk (CAPWAP_PKTBUF_POOLID);
        if (pRcvdBuf == NULL)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "CAPWAP Session Entry"
                        "is NULL, retrun FAILURE \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "CAPWAP Session Entry"
                          "is NULL, retrun FAILURE \r\n"));
            return OSIX_FAILURE;
        }
        CAPWAP_COPY_FROM_BUF (pBuf, pRcvdBuf, 0, u2PacketLen);
        u1IsNotLinear = OSIX_TRUE;
    }
    /* Recieved sequence number */

    /* Validate the Echo request message. if it
       is success construct the Config Status Response */
    if ((CapwapValidateEchoRequestMsgElems (pRcvdBuf, pEchoReqMsg) ==
         OSIX_SUCCESS))
    {
        if (u1IsNotLinear == OSIX_TRUE)
        {
            MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pRcvdBuf);
        }
        if (CapwapConstructCpHeader (&EchoResp.capwapHdr) == OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "capwapProcessConfigStationRequest:Failed to"
                        "construct the capwap Header\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "capwapProcessConfigStationRequest:Failed to"
                          "construct the capwap Header\n"));
            return OSIX_FAILURE;
        }
        /* get message elements to construct the config status response */
        if (CapwapGetVendorPayload (&EchoResp.vendSpec, &u4MsgLen) ==
            OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to get Optional Echo Response"
                        "Message Elements \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to get Optional Echo Response"
                          "Message Elements \r\n"));
            return OSIX_FAILURE;

        }
        /* update Control Header */
        EchoResp.u1SeqNum = pEchoReqMsg->capwapCtrlHdr.u1SeqNum;
        EchoResp.u2CapwapMsgElemenLen = (UINT2) (u4MsgLen + 3);
        EchoResp.u1NumMsgBlocks = 0;    /* flags always zero */

        pTxBuf = CAPWAP_ALLOCATE_CRU_BUF (CAPWAP_MAX_PKT_LEN, 0);
        if (pTxBuf == NULL)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Memory Allocation Failed \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Memory Allocation Failed \r\n"));
            return OSIX_FAILURE;
        }
        pu1TxBuf = CAPWAP_BUF_IF_LINEAR (pTxBuf, 0, CAPWAP_MAX_PKT_LEN);
        if (pu1TxBuf == NULL)
        {
            pu1TxBuf = (UINT1 *) MemAllocMemBlk (CAPWAP_PKTBUF_POOLID);
            /* Kloc Fix Start */
            if (pu1TxBuf == NULL)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Memory Allocation Failed \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Memory Allocation Failed \r\n"));
                CAPWAP_RELEASE_CRU_BUF (pTxBuf);
                return OSIX_FAILURE;
            }
            /* Kloc Fix Ends */
            CAPWAP_COPY_FROM_BUF (pTxBuf, pu1TxBuf, 0, CAPWAP_MAX_PKT_LEN);
            u1IsNotLinear = OSIX_TRUE;
        }

        if ((CapwapAssembleEchoResponse (&EchoResp, pu1TxBuf)) == OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to assemble the Echo Response Message \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to assemble the Echo Response Message \r\n"));
            if (u1IsNotLinear == OSIX_TRUE)
            {
                MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pu1TxBuf);
            }
            CAPWAP_RELEASE_CRU_BUF (pTxBuf);
            return OSIX_FAILURE;
        }
        if (u1IsNotLinear == OSIX_TRUE)
        {
            /* If the CRU buffer memory is not linear */
            CAPWAP_COPY_TO_BUF (pTxBuf, pu1TxBuf, 0, CAPWAP_MAX_PKT_LEN);
            MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pu1TxBuf);
        }

        i4Status = CapwapTxMessage (pTxBuf,
                                    pSessEntry,
                                    (UINT4) (EchoResp.u2CapwapMsgElemenLen +
                                             CAPWAP_SOCK_HDR_LEN),
                                    WSS_CAPWAP_802_11_CTRL_PKT);

        if (OSIX_SUCCESS == i4Status)
        {
            MEMSET (&WssIfPMDB, 0, sizeof (tWssIfPMDB));
            WssIfPMDB.u2ProfileId = pSessEntry->u2IntProfileId;
            WssIfPMDB.WssPmWLCCapwapStatsSetFlag.bEchoStats = OSIX_TRUE;
            WssIfPMDB.WssPmWLCCapwapStatsSetFlag.bRunUpdateRspTransmitted =
                OSIX_TRUE;
            if (WssIfProcessPMDBMsg
                (WSS_PM_WLC_CAPWAP_RUN_STATS_SET, &WssIfPMDB) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to update PM db\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to update PM db\r\n"));
            }

            /* Update the last transmitted sequence number */
            /* store the packet buffer pointer in
             * the remote session DB copy the pointer */

            /* pSessEntry->lastTransmittedPkt is not used anywhere for
               ECHO response.
               * So deleting these 2 lines and freeing the CRU BUF */

            /*pSessEntry->lastTransmittedPkt = pBuf;
               pSessEntry->lastTransmittedPktLen =
               (EchoResp.u2CapwapMsgElemenLen + 13); */
            CAPWAP_RELEASE_CRU_BUF (pTxBuf);

            /* Start Wait Join Timer */
            CapwapResetTmr (pSessEntry, CAPWAP_ECHO_INTERVAL_TMR,
                            CAPWAP_WLC_ECHO_INTERVAL_TIMEOUT);
        }
        else
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to transmit"
                        "Echo Response \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to transmit" "Echo Response \r\n"));
            MEMSET (&WssIfPMDB, 0, sizeof (tWssIfPMDB));
            WssIfPMDB.u2ProfileId = pSessEntry->u2IntProfileId;
            WssIfPMDB.WssPmWLCCapwapStatsSetFlag.bEchoStats = OSIX_TRUE;
            WssIfPMDB.WssPmWLCCapwapStatsSetFlag.
                bRunUpdateunsuccessfulProcessed = OSIX_TRUE;
            if (WssIfProcessPMDBMsg
                (WSS_PM_WLC_CAPWAP_RUN_STATS_SET, &WssIfPMDB) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to update PM db\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to update PM db\r\n"));
            }

            CAPWAP_RELEASE_CRU_BUF (pTxBuf);
            pSessEntry->lastTransmittedPkt = NULL;
            pSessEntry->lastTransmittedPktLen = 0;
            return OSIX_FAILURE;
        }
    }
    else
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to validate Echo Req \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to validate Echo Req \r\n"));
        return OSIX_FAILURE;
    }

    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapValidateEchoRequestMsgElems         */
/*  Description     : The function validates the functional             */
/*                    characteristics of the received CAPWAP            */
/*                    Join request agains the configured                */
/*                    WTP profile                                       */
/*  Input(s)        : discReqMsg - parsed CAPWAP Discovery request      */
/*                    pRcvBuf - Received discovery packet               */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
CapwapValidateEchoRequestMsgElems (UINT1 *pRcvBuf,
                                   tCapwapControlPacket * pEchoReqMsg)
{

    tVendorSpecPayload  vendSpec;

    CAPWAP_FN_ENTRY ();
    MEMSET (&vendSpec, 0, sizeof (tVendorSpecPayload));

    /* Validate the mandatory message elements */

    if (CapwapValidateVendSpecPld (pRcvBuf, pEchoReqMsg, &vendSpec, 5) ==
        OSIX_SUCCESS)
    {
        return OSIX_SUCCESS;
    }
    /* Validate optional elements */

        /****************************TBD ***************************/

    /* Profile not configured by the operator
     * ncrement the error count*/
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;

}

/************************************************************************/
/*  Function Name   : CapwapProcessEchoResponse                         */
/*  Description     : The function processes the received CAPWAP        */
/*                    join response. This function handling is mainly   */
/*                    done in the AP and it validates the received join */
/*                    response and updates WSS-AP.                      */
/*  Input(s)        : JoinRespMsg- Join response packet received        */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
CapwapProcessEchoResponse (tSegment * pBuf,
                           tCapwapControlPacket * pEchoRespMsg,
                           tRemoteSessionManager * pSessEntry)
{
    UINT1              *pRcvBuf = NULL;
    UINT1               u1IsNotLinear = OSIX_FALSE;
    UINT2               u2PacketLen, u2InternalProfileId;
    UINT2               u2IntId = 0;
    UINT2               u2EchoInterval = 0;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tWssIfPMDB          WssIfPMDB;

    CAPWAP_FN_ENTRY ();

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpEchoInterval = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2IntId;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB) ==
        OSIX_SUCCESS)
    {
        u2EchoInterval = pWssIfCapwapDB->CapwapGetDB.u2WtpEchoInterval;
    }

    /* Counters */
    gu4EchoRspReceived++;
    CAPWAP_TRC1 (CAPWAP_STATS_TRC, "Echo Responses Received = %d \r\n ",
                 gu4EchoRspReceived);
    SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4CapwapSysLogId,
                  "Echo Responses Received = %d \r\n ", gu4EchoRspReceived));

    if (pSessEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Session Entry is NULL, Return"
                    "Failure \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Session Entry is NULL, Return" "Failure \r\n"));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    /* reset the count */
    pSessEntry->u1EchoCount = 0;

    u2PacketLen = (UINT2) CAPWAP_GET_BUF_LEN (pBuf);
    pRcvBuf = CAPWAP_BUF_IF_LINEAR (pBuf, 0, u2PacketLen);
    if (pRcvBuf == NULL)
    {
        pRcvBuf = (UINT1 *) MemAllocMemBlk (CAPWAP_PKTBUF_POOLID);
        if (pRcvBuf == NULL)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
        CAPWAP_COPY_FROM_BUF (pBuf, pRcvBuf, 0, u2PacketLen);
        u1IsNotLinear = OSIX_TRUE;
    }

    /* Validate the Echo response message */
    if ((CapwapValidateEchoResponseMsgElems (pRcvBuf, pEchoRespMsg,
                                             &u2InternalProfileId) ==
         OSIX_SUCCESS))
    {
        /* renew the echo interval timer */
        CapwapTmrStart (pSessEntry, CAPWAP_ECHO_INTERVAL_TMR, u2EchoInterval);
        MEMSET (&WssIfPMDB, 0, sizeof (tWssIfPMDB));
        WssIfPMDB.u2ProfileId = pSessEntry->u2IntProfileId;
        WssIfPMDB.WssPmWLCCapwapStatsSetFlag.bEchoStats = OSIX_TRUE;
        WssIfPMDB.WssPmWLCCapwapStatsSetFlag.bRunUpdateRspReceived = OSIX_TRUE;
        if (WssIfProcessPMDBMsg (WSS_PM_WLC_CAPWAP_RUN_STATS_SET,
                                 &WssIfPMDB) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to update PM db\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to update PM db\r\n"));
        }
    }
    else
    {
        MEMSET (&WssIfPMDB, 0, sizeof (tWssIfPMDB));
        WssIfPMDB.u2ProfileId = pSessEntry->u2IntProfileId;
        WssIfPMDB.WssPmWLCCapwapStatsSetFlag.bEchoStats = OSIX_TRUE;
        WssIfPMDB.WssPmWLCCapwapStatsSetFlag.bRunUpdateunsuccessfulProcessed =
            OSIX_TRUE;

        if (WssIfProcessPMDBMsg (WSS_PM_WLC_CAPWAP_RUN_STATS_SET,
                                 &WssIfPMDB) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to update PM db\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to update PM db\r\n"));
        }

    }
    /* Kloc Fix Start */
    if (u1IsNotLinear == OSIX_TRUE)
    {
        MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pRcvBuf);
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);

    /* Kloc Fix Ends */
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapValidateEchoResponseMsgElems         */
/*  Description     : The function validates the functional             */
/*                    characteristics of the received CAPWAP            */
/*                    Join request agains the configured                */
/*                    WTP profile                                       */
/*  Input(s)        : discReqMsg - parsed CAPWAP Discovery request      */
/*                    pRcvBuf - Received discovery packet               */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
CapwapValidateEchoResponseMsgElems (UINT1 *pRcvBuf,
                                    tCapwapControlPacket * echoRespMsg,
                                    UINT2 *u2ProfileId)
{

    tVendorSpecPayload  vendSpec;

    CAPWAP_FN_ENTRY ();
    MEMSET (&vendSpec, 0, sizeof (tVendorSpecPayload));

    /* Validate the mandatory message elements */
    UNUSED_PARAM (pRcvBuf);
    UNUSED_PARAM (echoRespMsg);
#if CAPWAP_UNUSED_CODE
    if (CapwapValidateVendSpecPld (pRcvBuf, echoRespMsg, &vendSpec, 5) ==
        OSIX_SUCCESS)

    {
        return OSIX_SUCCESS;
    }
#endif
    /* Validate optional elements */

        /****************************TBD ***************************/

    /* Profile not configured by the operator
     * ncrement the error count*/
    CAPWAP_FN_EXIT ();
    UNUSED_PARAM (u2ProfileId);
    return OSIX_FAILURE;

}

/*****************************************************************************
 *                                                                           *
 * Function     : CapwapAssembleEchoRequest                          *
 *                                                                           *
 * Description  : Assemble the capwap Control packet structure to            *
 *                transmittable  format.                                     *
 *                                                                           *
 * Input        : Capwap Control message structure                           *
 *                                                                           *
 * Output       : Capwap control message to be transmitted out               *
 *                                                                           *
 * Returns      : SUCCESS/FAILURE                                            *
 *****************************************************************************/
INT4
CapwapAssembleEchoRequest (tEchoReq * pEchoReq, UINT1 *pMesgOut)
{
    UINT1              *pu1Buf = NULL;

    pu1Buf = pMesgOut;
    CAPWAP_MEMSET (pu1Buf, CAPWAP_INIT_VAL, CAPWAP_MAX_PKT_LEN);
    /* Assemble capwap Header */
    AssembleCapwapHeader (pu1Buf, &(pEchoReq->capwapHdr));
    /* Move the buffer pointer */
    pu1Buf += (pEchoReq->capwapHdr.u2HdrLen);

    /* Assemble Control header */
    AssembleCapwapControlHeader (pu1Buf,
                                 CAPWAP_ECHO_REQ,
                                 pEchoReq->u1SeqNum,
                                 pEchoReq->u2CapwapMsgElemenLen,
                                 pEchoReq->u1NumMsgBlocks);
    pu1Buf += CAPWAP_CTRL_HEADER_SIZE;

    /* Assemble the Vendor Specific Payload */
    if (pEchoReq->vendSpec.isOptional)
    {
        CapwapAssembleVendorSpecificPayload (pu1Buf, &(pEchoReq->vendSpec));
        pu1Buf += (CAPWAP_MSG_ELEM_TYPE_LEN + (pEchoReq->vendSpec.u2MsgEleLen));
    }
    return CAPWAP_SUCCESS;
}

#endif
