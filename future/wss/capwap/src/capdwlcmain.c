/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 * $Id: capdwlcmain.c,v 1.3 2017/11/24 10:37:02 siva Exp $ *
 * Description: This file contains the Discovery task main loop
 *              and the initialization routines.
 *
 *******************************************************************/
#ifndef __CAPWAP_DISCOVERY_C__
#define __CAPWAP_DISCOVERY_C__
#define _CAPWAP_GLOBAL_VAR
#include "capwapinc.h"
#include "wssifpmdb.h"

static tDiscTaskGloabls gDiscTaskGlobals;

/* CPU relinquish counter */
static UINT4        gu4CPUDiscRelinquishCounter = 0;

extern eState       gCapwapState;

/*****************************************************************************/
/* Function Name      : CapwapEnqueDiscPkts                                  */
/*                                                                           */
/* Description        : This function is to enque discovery packets to the   */
/*                      discovery task                                       */
/*                                                                           */
/* Input(s)           : pCapwapRxQMsg - Received queue message               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
CapwapEnqueDiscPkts (tCapwapRxQMsg * pDiscQMsg)
{

    CAPWAP_FN_ENTRY ();
    if (OsixQueSend (gDiscTaskGlobals.discMsgQId,
                     (UINT1 *) &pDiscQMsg, OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapEnqueDiscPkts:"
                    "Sending the packet to Disc queue failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "CapwapEnqueDiscPkts:"
                      "Sending the packet to Disc queue failed \r\n"));
        CAPWAP_RELEASE_CRU_BUF (pDiscQMsg->pRcvBuf);
        MemReleaseMemBlock (CAPWAP_QUEMSG_POOLID, (UINT1 *) pDiscQMsg);
    }
    if (OsixEvtSend (gDiscTaskGlobals.capwapDiscTaskId,
                     CAPWAP_DISC_RX_MSGQ_EVENT) == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapEnqueDiscPkts:Sending the event failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "CapwapEnqueDiscPkts:Sending the event failed \r\n"));
        return;
    }
    CAPWAP_FN_EXIT ();
    return;
}

/****************************************************************************
*                                                                           *
* Function     : CapwapDiscTaskMain                                         *
*                                                                           *
* Description  : Main function of CAPWAP Discovery task.                    *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : VOID                                                       *
*                                                                           *
*****************************************************************************/
UINT4
CapwapDiscTaskMain ()
{

    UINT4               u4Event = 0;
    tCapwapRxQMsg      *pDiscQMsg = NULL;

    /* Create Discovery queue */
    if (OsixQueCrt (CAPWAP_DISC_Q_NAME, OSIX_MAX_Q_MSG_LEN, CAPWAP_DISC_Q_LEN,
                    &(gDiscTaskGlobals.discMsgQId)) != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapDiscTaskMain:Discovery Queue Creation failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "CapwapDiscTaskMain:Discovery Queue Creation failed \r\n"));
        lrInitComplete (OSIX_FAILURE);
        return OSIX_FAILURE;
    }

    if (OsixTskIdSelf (&gDiscTaskGlobals.capwapDiscTaskId) == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    " !!!!! CAPWAP DISC TASK INIT FAILURE  !!!!! \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      " !!!!! CAPWAP DISC TASK INIT FAILURE  !!!!! \r\n"));
        lrInitComplete (OSIX_FAILURE);
        return OSIX_FAILURE;
    }

    /* This thread dedicated to handle the received discovery message */
    gDiscTaskGlobals.u1IsDiscTaskInitialized = CAPWAP_SUCCESS;

    /* initialize counter to zero */
    gu4CPUDiscRelinquishCounter = 0;

    if (CapwapTmrStart (NULL, CAPWAP_CPU_DISC_RELINQUISH_TMR,
                        CAPWAP_RELINQUISH_TIMER_VAL) != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CAPWAP_CPU_DISC_RELINQUISH_TMR Timer Start FAILED.\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "CAPWAP_CPU_DISC_RELINQUISH_TMR Timer Start FAILED.\r\n"));
    }

    CAPWAP_TRC (CAPWAP_INIT_TRC,
                "capwapDiscoveryTask: Disc Task Init Completed \r\n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                  "capwapDiscoveryTask: Disc Task Init Completed \r\n"));
    lrInitComplete (OSIX_SUCCESS);

    gu4CapwapSysLogId =
        (UINT4) SYS_LOG_REGISTER (CAPWAP_MUT_EXCL_RECEIVE_SEM_NAME,
                                  SYSLOG_CRITICAL_LEVEL);
    while (1)
    {
        if (OsixReceiveEvent (CAPWAP_DISC_RX_MSGQ_EVENT |
                              CAPWAP_DISC_TMR_EXP_EVENT,
                              OSIX_WAIT, (UINT4) 0,
                              (UINT4 *) &(u4Event)) == OSIX_SUCCESS)
        {
            if (u4Event & CAPWAP_DISC_RX_MSGQ_EVENT)
            {
                while (OsixQueRecv (gDiscTaskGlobals.discMsgQId,
                                    (UINT1 *) (&pDiscQMsg),
                                    OSIX_DEF_MSG_LEN,
                                    OSIX_NO_WAIT) == OSIX_SUCCESS)
                {
                    if (pDiscQMsg->u4PktType == CAPWAP_DISC_MSG)
                    {
                        if (CapwapDiscProcessPkt ((tSegment *) pDiscQMsg->
                                                  pRcvBuf) == OSIX_FAILURE)
                        {
                            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                        "Failed to process the received discovery packet\n");
                            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                          "Failed to process the received discovery packet\n"));
                        }
                    }
                    CAPWAP_RELEASE_CRU_BUF (pDiscQMsg->pRcvBuf);
                    MemReleaseMemBlock (CAPWAP_QUEMSG_POOLID,
                                        (UINT1 *) pDiscQMsg);
                }
                CapwapCheckDiscRelinquishCounters ();
            }
        }

    }
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapValidateDiscMsgElems                        */
/*  Description     : The function validates the functional             */
/*                    characteristics of the received CAPWAP            */
/*                    discovery request agains the configured           */
/*                    WTP profile                                       */
/*  Input(s)        : discReqMsg - parsed CAPWAP Discovery request      */
/*                    pRcvBuf - Received discovery packet               */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS/ OSIX_FAILURE                        */
/************************************************************************/
INT4
CapwapValidateDiscReqMsgElems (UINT1 *pRcvBuf,
                               tCapwapControlPacket * pDiscReqMsg)
{
    tWtpBoardData       wtpBoardData;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT4               u1Index = 0;
    UINT4               u4Offset = 0;
    UINT2               u2NumOptionalMsg = 0;
    UINT2               u2MsgType = 0;
    tVendorSpecPayload  vendDesc;

    CAPWAP_FN_ENTRY ();

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&wtpBoardData, 0, sizeof (tWtpBoardData));
    MEMSET (&vendDesc, 0, sizeof (tVendorSpecPayload));

    u2NumOptionalMsg = pDiscReqMsg->numOptionalElements;
    u4Offset =
        pDiscReqMsg->capwapMsgElm[WTP_BOARD_DATA_DISC_REQ_INDEX].pu2Offset[0];
    /* Get the WTP Base MAC Addr */
    if (CapwapGetWtpBoardDataFromBuf (pRcvBuf, u4Offset, &wtpBoardData)
        == OSIX_SUCCESS)
    {
        MEMCPY (pWssIfCapwapDB->CapwapGetDB.WtpMacAddress,
                wtpBoardData.wtpBoardInfo[WTP_BASE_MAC_ADDRESS].boardInfoData,
                wtpBoardData.wtpBoardInfo[WTP_BASE_MAC_ADDRESS].
                u2BoardInfoLength);
    }

    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_INDEX_FROM_MAC, pWssIfCapwapDB)
        == OSIX_SUCCESS)
    {
        pDiscReqMsg->u2IntProfileId =
            pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId;
        /* Validate Message elements */
        if ((CapwapValidateDiscType
             (pRcvBuf, pDiscReqMsg, DISCOVERY_TYPE_DISC_REQ_INDEX)
             == OSIX_SUCCESS) &&
            (CapwapValidateWtpBoardData
             (pRcvBuf, pDiscReqMsg, WTP_BOARD_DATA_DISC_REQ_INDEX)
             == OSIX_SUCCESS) &&
            (CapwapValidateFrameTunnelMode
             (pRcvBuf, pDiscReqMsg, WTP_FRAME_TUNNEL_MODE_DISC_REQ_INDEX)
             == OSIX_SUCCESS) &&
            (CapwapValidateMacType
             (pRcvBuf, pDiscReqMsg, WTP_MAC_TYPE_DISC_REQ_INDEX)
             == OSIX_SUCCESS) &&
            (CapwapValidateRadioInfo
             (pRcvBuf, pDiscReqMsg, WTP_RADIO_INFO_DISC_REQ_INDEX)
             == OSIX_SUCCESS))
        {
            for (u1Index = 0; u1Index < u2NumOptionalMsg; u1Index++)
            {
                /* Get the message element type */
                u2MsgType =
                    pDiscReqMsg->capwapMsgElm[DISCOVERY_REQ_MAND_MSG_ELEMENTS +
                                              u1Index].u2MsgType;

                switch (u2MsgType)
                {
                    case MTU_DISC_PADDING:
                    {
                        if (CapwapValidateMtuDiscPad (pRcvBuf, pDiscReqMsg,
                                                      (UINT2)
                                                      (DISCOVERY_REQ_MAND_MSG_ELEMENTS
                                                       + u1Index)) !=
                            OSIX_SUCCESS)
/* Multi Radio Changes */
                        {
                            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                            return OSIX_FAILURE;
                        }
                        break;
                    }

                    case VENDOR_SPECIFIC_PAYLOAD:
                    {
                        if (CapwapValidateVendSpecPld (pRcvBuf, pDiscReqMsg,
                                                       &vendDesc,
                                                       (UINT2)
                                                       (DISCOVERY_REQ_MAND_MSG_ELEMENTS
                                                        + u1Index)) !=
                            OSIX_SUCCESS)
/* Multi Radio Changes */
                        {
                            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                            return OSIX_FAILURE;
                        }
                        break;
                    }

                    default:
                    {
                        CAPWAP_TRC1 (CAPWAP_FAILURE_TRC,
                                     "Received the unknown Message Type "
                                     "u2MsgType = %d \r\n", u2MsgType);
                        SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4CapwapSysLogId,
                                      "Received the unknown Message Type "
                                      "u2MsgType = %d \r\n", u2MsgType));
                        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                        return OSIX_FAILURE;
                        break;
                    }
                }
            }
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_SUCCESS;
        }
    }
    else
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "The Received Base Mac is not configured in the WTP profile \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "The Received Base Mac is not configured in the WTP profile \r\n"));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    CAPWAP_FN_EXIT ();
    return OSIX_FAILURE;
}

/************************************************************************/
/*  Function Name   : CapwapProcessDiscoveryResponse                    */
/*  Description     : The function processes the received CAPWAP        */
/*                    discovery response. This function validates the   */
/*                    received discovery response and updates WSS-AP.   */
/*  Input(s)        : discReqMsg - CAPWAP Discovery packet received     */
/*                    u4DestPort - Received UDP port                    */
/*                    u4DestIp - Dest IP Address                        */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS / OSIX_FAILURE                       */
/************************************************************************/
INT4
CapwapProcessDiscoveryResponse (tCRU_BUF_CHAIN_HEADER * pBuf,
                                tCapwapControlPacket * pDiscRespMsg,
                                tRemoteSessionManager * pSessEntry)
{
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pDiscRespMsg);
    UNUSED_PARAM (pSessEntry);
    return OSIX_FAILURE;
}

/************************************************************************/
/*  Function Name   : CapwapProcessDiscoveryRequest                     */
/*  Description     : The function processes the received CAPWAP        */
/*                    discovery request                                 */
/*  Input(s)        : discReqMsg - CAPWAP Discovery packet received     */
/*                    u4DestPort - Received UDP port                    */
/*                    u4DestIp - Dest IP Address                        */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
CapwapProcessDiscoveryRequest (tSegment * pBuf,
                               tCapwapControlPacket * pDiscReqMsg,
                               tRemoteSessionManager * pSessEntry)
{
    UINT4               u4MsgLen = 0;
    UINT4               u4WtpProfileId = 0;
    INT4                i4Status = OSIX_SUCCESS;
    tDiscRsp           *pDiscResponse = NULL;
    UINT1              *pu1TxBuf = NULL;
    UINT1              *pRcvdBuf = NULL;
    UINT1              *pTempBuf = NULL;
    tIp6Addr            destIpAddr;
    UINT4               u4DestPort = 0;
    UINT1               u1IsNotLinear = OSIX_FALSE;
    UINT2               u2PacketLen = 0;
    UINT2               u2WtpInternalId = 0;
    tSegment           *pTxBuf = NULL;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tWssIfPMDB          WssIfPMDB;
    UINT1               u1FsWlcDiscoveryMode = 0;
    UINT2               u2TempLength = 0;
    UINT1              *pu1TempBuf = NULL;
    tCapwapWTPTrapInfo  sWTPTrapInfo;
    tWtpBoardData       wtpBoardData;
    UINT4               u4Offset = 0;
    tMacAddr            MacAddr;
    tMacAddr            WtpMacAddr;
    UNUSED_PARAM (pSessEntry);
    MEMSET (&wtpBoardData, 0, sizeof (tWtpBoardData));

    CAPWAP_FN_ENTRY ();

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        pDiscResponse = (tDiscRsp *) (VOID *) UtlShMemAllocDiscRspBuf ();
    if (pDiscResponse == NULL)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapProcessDiscoveryRequest:- "
                    "UtlShMemAllocDiscRspBuf returned failure\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "CapwapProcessDiscoveryRequest:- "
                      "UtlShMemAllocDiscRspBuf returned failure\n"));

        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    MEMSET (pDiscResponse, 0, sizeof (tDiscRsp));
    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&WssIfPMDB, 0, sizeof (WssIfPMDB));
    MEMSET (&sWTPTrapInfo, 0, sizeof (tCapwapWTPTrapInfo));

    u4DestPort = CRU_BUF_Get_U4Reserved1 (pBuf);
    destIpAddr.u4_addr[0] = CRU_BUF_Get_U4Reserved2 (pBuf);

    u2PacketLen = (UINT2) CAPWAP_GET_BUF_LEN (pBuf);
    pRcvdBuf = CAPWAP_BUF_IF_LINEAR (pBuf, 0, u2PacketLen);

    if (NULL == pRcvdBuf)
    {
        pRcvdBuf = (UINT1 *) MemAllocMemBlk (CAPWAP_PKTBUF_POOLID);
        CAPWAP_COPY_FROM_BUF (pBuf, pRcvdBuf, 0, u2PacketLen);
        u1IsNotLinear = OSIX_TRUE;
    }
    pTempBuf = pRcvdBuf;
    pWssIfCapwapDB->CapwapIsGetAllDB.bDiscMode = OSIX_TRUE;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                 pWssIfCapwapDB) == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to Discovery Mode from CAPWAP DB \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to Discovery Mode from CAPWAP DB \r\n"));

        MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pRcvdBuf);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        UtlShMemFreeDiscRspBuf ((UINT1 *) pDiscResponse);
        return OSIX_FAILURE;
    }

    if (CapwapValidateMacType (pRcvdBuf, pDiscReqMsg,
                               WTP_MAC_TYPE_DISC_REQ_INDEX) != OSIX_SUCCESS)
    {
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2WtpInternalId;
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpMacAddress = OSIX_TRUE;
        pWssIfCapwapDB->CapwapIsGetAllDB.bMacType = OSIX_TRUE;
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                     pWssIfCapwapDB) == OSIX_SUCCESS)
        {
            MEMCPY (sWTPTrapInfo.au1capwapBaseNtfWtpId,
                    pWssIfCapwapDB->CapwapGetDB.WtpMacAddress, MAC_ADDR_LEN);
            sWTPTrapInfo.i4capwapBaseWtpMacTypeOptions =
                pWssIfCapwapDB->CapwapGetDB.u1WtpMacType;
            CapwapSnmpifSendTrap (CAPWAP_WTP_SYNC_TRAP, &sWTPTrapInfo);
        }
    }

    if (CapwapValidateFrameTunnelMode (pRcvdBuf, pDiscReqMsg,
                                       WTP_FRAME_TUNNEL_MODE_DISC_REQ_INDEX) !=
        OSIX_SUCCESS)
    {
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2WtpInternalId;
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpMacAddress = OSIX_TRUE;
        pWssIfCapwapDB->CapwapIsGetAllDB.bTunnelMode = OSIX_TRUE;
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                     pWssIfCapwapDB) == OSIX_SUCCESS)
        {
            MEMCPY (sWTPTrapInfo.au1capwapBaseNtfWtpId,
                    pWssIfCapwapDB->CapwapGetDB.WtpMacAddress, MAC_ADDR_LEN);
            sWTPTrapInfo.i4capwapBaseWtpTunnelModeOptions =
                pWssIfCapwapDB->CapwapGetDB.u1WtpTunnelMode;
            CapwapSnmpifSendTrap (CAPWAP_WTP_SYNC_TRAP, &sWTPTrapInfo);
        }
    }
    if ((pWssIfCapwapDB->u1DiscMode == AUTO_DISCOVERY_MODE))
    {
        u4Offset =
            pDiscReqMsg->capwapMsgElm[WTP_BOARD_DATA_DISC_REQ_INDEX].
            pu2Offset[0];
        if (CapwapGetWtpBoardDataFromBuf (pTempBuf, u4Offset, &wtpBoardData) !=
            OSIX_SUCCESS)
        {
            if (u1IsNotLinear == OSIX_TRUE)
            {
                MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pRcvdBuf);
            }
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            UtlShMemFreeDiscRspBuf ((UINT1 *) pDiscResponse);
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Get WTP data from Buf failed \r\n");
            return OSIX_FAILURE;
        }
        if (wtpBoardData.wtpBoardInfo[WTP_MODEL_NUMBER].isOptional)
        {
            pWssIfCapwapDB->CapwapIsGetAllDB.bWtpModelNumber = OSIX_TRUE;
            pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
            MEMCPY (pWssIfCapwapDB->CapwapGetDB.WtpModelEntry.au1ModelNumber,
                    &wtpBoardData.wtpBoardInfo[WTP_MODEL_NUMBER].boardInfoData,
                    STRLEN (wtpBoardData.wtpBoardInfo[WTP_MODEL_NUMBER].
                            boardInfoData));
            if (WssIfProcessCapwapDBMsg
                (WSS_CAPWAP_GET_WTP_MODEL_ENTRY,
                 pWssIfCapwapDB) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "entry does not exists\r\n");
                if (u1IsNotLinear == OSIX_TRUE)
                {
                    MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pRcvdBuf);
                }
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                UtlShMemFreeDiscRspBuf ((UINT1 *) pDiscResponse);
                return OSIX_FAILURE;
            }
        }
        if (wtpBoardData.wtpBoardInfo[WTP_BASE_MAC_ADDRESS].isOptional)
        {
            MEMCPY (MacAddr,
                    &wtpBoardData.wtpBoardInfo[WTP_BASE_MAC_ADDRESS].
                    boardInfoData, sizeof (tMacAddr));
            MEMCPY (pWssIfCapwapDB->CapwapGetDB.WtpMacAddress, &MacAddr,
                    sizeof (tMacAddr));
        }
        MEMCPY (WtpMacAddr, MacAddr, sizeof (tMacAddr));
        if (CapwapGetWtpProfileIdFromProfileMac (WtpMacAddr, &u4WtpProfileId)
            != OSIX_SUCCESS)
        {
            if (CapwapCreateAutoProfile
                (MacAddr,
                 pWssIfCapwapDB->CapwapGetDB.WtpModelEntry.au1ModelNumber) !=
                OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Profile Creation Failed\r\n");
                if (u1IsNotLinear == OSIX_TRUE)
                {
                    MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pRcvdBuf);
                }
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                UtlShMemFreeDiscRspBuf ((UINT1 *) pDiscResponse);
                return OSIX_FAILURE;
            }
        }
        MEMCPY (pWssIfCapwapDB->CapwapGetDB.WtpMacAddress,
                WtpMacAddr, MAC_ADDR_LEN);
        /*Finding the profil id and storing it in u4WtpProfileId */
        if (CapwapGetWtpProfileIdFromProfileMac (WtpMacAddr, &u4WtpProfileId) ==
            OSIX_SUCCESS)
        {
            pWssIfCapwapDB->CapwapGetDB.u2ProfileId = u4WtpProfileId;
            pWssIfCapwapDB->CapwapIsGetAllDB.bWtpProfileId = OSIX_TRUE;
            if (WssIfProcessCapwapDBMsg
                (WSS_CAPWAP_GET_INTERNAL_ID, pWssIfCapwapDB) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Failed to get the internal profile id of Auto Created Profile\r\n");
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                UtlShMemFreeDiscRspBuf ((UINT1 *) pDiscResponse);
                return OSIX_FAILURE;
            }
            /*Changing the value of u4WtpProfileId with internal profile id */
            u4WtpProfileId = pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId;
        }
        else
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to get the wtp profile id of Auto Created Profile\r\n");
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            UtlShMemFreeDiscRspBuf ((UINT1 *) pDiscResponse);
            return OSIX_FAILURE;
        }

        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpModelNumber = OSIX_FALSE;
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpMacAddress = OSIX_TRUE;
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_SET_DB, pWssIfCapwapDB) ==
            OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "failed to set mac address\r\n");
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            UtlShMemFreeDiscRspBuf ((UINT1 *) pDiscResponse);
            return OSIX_FAILURE;
        }
    }

    /* In Auto discovery mode WLC send the
     *discovery response with out any validation */
    if (((pWssIfCapwapDB->u1DiscMode == MAC_DISCOVERY_MODE) &&
         (CapwapValidateDiscReqMsgElems (pRcvdBuf, pDiscReqMsg) ==
          OSIX_SUCCESS)) || (pWssIfCapwapDB->u1DiscMode == AUTO_DISCOVERY_MODE))
    {
        if (u1IsNotLinear == OSIX_TRUE)
        {
            MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pRcvdBuf);
        }

        /* Start assembling the discovery response */
        /* update CAPWAP Header */
        if (CapwapConstructCpHeader (&(pDiscResponse->capwapHdr)) ==
            OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "capwapProcessDiscoveryRequest:"
                        "Failed to construct the capwap Header\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "capwapProcessDiscoveryRequest:"
                          "Failed to construct the capwap Header\n"));

            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            UtlShMemFreeDiscRspBuf ((UINT1 *) pDiscResponse);
            return OSIX_FAILURE;
        }

        /* Get the internal Id from the CAPWAP get DB */
        if (CapwapGetInternalId (pRcvdBuf, pDiscReqMsg, &u2WtpInternalId)
            != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "capwapProcessDiscoveryRequest:Failed to get the internal id\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "capwapProcessDiscoveryRequest:Failed to get the internal id\n"));

            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            UtlShMemFreeDiscRspBuf ((UINT1 *) pDiscResponse);
            return OSIX_FAILURE;
        }
        pDiscResponse->u2WtpInternalId = u2WtpInternalId;

        CapwapGetFsWlcDiscoveryMode (&u1FsWlcDiscoveryMode);
        if (u1FsWlcDiscoveryMode != CAPWAP_WTP_DISC_MODE_AUTO)
        {
            /* Check Capwap DB whether AP's MAC is in blacklist */
            pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
            pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2WtpInternalId;
            pWssIfCapwapDB->CapwapIsGetAllDB.bWtpMacAddress = OSIX_TRUE;
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                         pWssIfCapwapDB) == OSIX_SUCCESS)
            {
                MEMCPY (pWssIfCapwapDB->BaseMacAddress,
                        pWssIfCapwapDB->CapwapGetDB.WtpMacAddress,
                        MAC_ADDR_LEN);
                if (u1FsWlcDiscoveryMode == CAPWAP_DISC_MODE_MAC_BLACKLIST)
                {
                    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_BLACKLIST_ENTRY,
                                                 pWssIfCapwapDB) ==
                        OSIX_SUCCESS)
                    {
                        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                    "AP Mac Address is under black list;\
                                Cannot provide service\r\n");
                        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                      "AP Mac Address is under black list;\
                                Cannot provide service\r\n"));

                        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                        UtlShMemFreeDiscRspBuf ((UINT1 *) pDiscResponse);
                        return OSIX_FAILURE;
                    }
                }
                else if (u1FsWlcDiscoveryMode == CAPWAP_DISC_MODE_MAC_WHITELIST)
                {
                    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_WHITELIST_ENTRY,
                                                 pWssIfCapwapDB) !=
                        OSIX_SUCCESS)
                    {
                        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                    "AP Mac Address is not under white list;\
                                Cannot provide service\r\n");
                        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                      "AP Mac Address is not under white list;\
                                Cannot provide service\r\n"));

                        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                        UtlShMemFreeDiscRspBuf ((UINT1 *) pDiscResponse);
                        return OSIX_FAILURE;
                    }

                }
            }
            else
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "AP Mac Address is present in DB;\
                        Cannot provide service\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "AP Mac Address is present in DB;\
                        Cannot provide service\r\n"));

                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                UtlShMemFreeDiscRspBuf ((UINT1 *) pDiscResponse);
                return OSIX_FAILURE;

            }

        }

        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2WtpInternalId;
        if (WssIfProcessCapwapDBMsg
            (WSS_CAPWAP_GET_RSM_FROM_INDEX, pWssIfCapwapDB) == OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "\r\nDFailed to access the DB\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "\r\nDFailed to access the DB\r\n"));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            UtlShMemFreeDiscRspBuf ((UINT1 *) pDiscResponse);
            return OSIX_FAILURE;

        }
        if (pWssIfCapwapDB->pSessEntry != NULL)
        {
            /*get AP mac address and state to call trap when AP restart */
            if ((pWssIfCapwapDB->pSessEntry->eCurrentState == CAPWAP_RUN) ||
                (pWssIfCapwapDB->pSessEntry->eCurrentState == CAPWAP_IDLE))
            {
                pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2WtpInternalId;
                pWssIfCapwapDB->CapwapIsGetAllDB.bWtpMacAddress = OSIX_TRUE;
                if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                             pWssIfCapwapDB) == OSIX_SUCCESS)
                {
                    MEMCPY (pWssIfCapwapDB->BaseMacAddress,
                            pWssIfCapwapDB->CapwapGetDB.WtpMacAddress,
                            MAC_ADDR_LEN);
                    MEMCPY (sWTPTrapInfo.au1capwapBaseNtfWtpId,
                            pWssIfCapwapDB->BaseMacAddress, MAC_ADDR_LEN);
                    sWTPTrapInfo.i4capwapBaseWtpState =
                        (INT4) pWssIfCapwapDB->pSessEntry->eCurrentState;

                    CapwapSnmpifSendTrap (CAPWAP_WTP_RESTART, &sWTPTrapInfo);
                }
                pWssIfCapwapDB->CapwapIsGetAllDB.bWtpMacAddress = OSIX_FALSE;
            }
        }
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpModelNumber = OSIX_FALSE;
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2WtpInternalId;
        pWssIfCapwapDB->CapwapGetDB.u4PathMTU = (UINT4) u2PacketLen;
        pWssIfCapwapDB->CapwapIsGetAllDB.bPathMTU = OSIX_TRUE;
        pWssIfCapwapDB->CapwapIsGetAllDB.bImageTransfer = OSIX_TRUE;
        pWssIfCapwapDB->CapwapGetDB.u1ImageTransferFlag = OSIX_FALSE;
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_SET_DB,
                                     pWssIfCapwapDB) == OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "\r\nCannot update Path MTU\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "\r\nCannot update Path MTU\r\n"));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            UtlShMemFreeDiscRspBuf ((UINT1 *) pDiscResponse);
            return OSIX_FAILURE;
        }

        WssIfPMDB.u2ProfileId = u2WtpInternalId;
        WssIfPMDB.tWtpPmAttState.wssPmWLCStatsSetDB.bWssPmWLCCapwapDiscReqRxd
            = OSIX_TRUE;
        if (WssIfProcessPMDBMsg (WSS_PM_WLC_CAPWAP_STATS_SET, &WssIfPMDB)
            != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to update Discovery stats to PM \n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to update Discovery stats to PM \n"));

        }

        /* update Mandatory Message Elements */
        if ((CapwapGetACDescriptor (&(pDiscResponse->acDesc),
                                    &u4MsgLen) == OSIX_FAILURE) ||
            (CapwapGetACName (&(pDiscResponse->acName),
                              &u4MsgLen) == OSIX_FAILURE) ||
            (CapwapGetControlIpAddr (&(pDiscResponse->ctrlAddr),
                                     &u4MsgLen) == OSIX_FAILURE))
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to get Mandatory Discovery Response Message Elements \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to get Mandatory Discovery Response Message Elements \r\n"));

            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            UtlShMemFreeDiscRspBuf ((UINT1 *) pDiscResponse);
            return OSIX_FAILURE;
        }

        /* update Optional Message Elements */
        if (CapwapGetDiscRespVendorPayload
            (&(pDiscResponse->vendSpec), &u4MsgLen,
             VENDOR_CONTROL_POLICY_DTLS_MSG, u2WtpInternalId) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to get Optional Discovery Response Message Elements \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to get Optional Discovery Response Message Elements \r\n"));

            /* with out optional message 
             * elements session should 
             * establish continue */
            /* return OSIX_FAILURE; */
        }

        /* Update the Option Ac Ip List */
        if (CapwapGetAcIpList (&(pDiscResponse->ipv4List), &u4MsgLen)
            == OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to get Optional Discovery Response Message Elements \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to get Optional Discovery Response Message Elements \r\n"));
        }
        /* update Control Header */
        pDiscResponse->u1SeqNum = pDiscReqMsg->capwapCtrlHdr.u1SeqNum;
        pDiscResponse->u2CapwapMsgElemenLen =
            (UINT2) (u4MsgLen + CAPWAP_CMN_MSG_ELM_HEAD_LEN);
        pDiscResponse->u1NumMsgBlocks = (UINT1) 0;    /* flags always zero */

        pTxBuf = CAPWAP_ALLOCATE_CRU_BUF (CAPWAP_MAX_PKT_LEN, 0);
        if (NULL == pTxBuf)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "CapwapProcessDiscoveryRequest:Memory Allocation Failed\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "CapwapProcessDiscoveryRequest:Memory Allocation Failed\n"));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            UtlShMemFreeDiscRspBuf ((UINT1 *) pDiscResponse);
            return OSIX_FAILURE;
        }
        pu1TxBuf = CAPWAP_BUF_IF_LINEAR (pTxBuf, 0, CAPWAP_MAX_PKT_LEN);
        if (NULL == pu1TxBuf)
        {
            pu1TxBuf = (UINT1 *) MemAllocMemBlk (CAPWAP_PKTBUF_POOLID);
            CAPWAP_COPY_FROM_BUF (pBuf, pu1TxBuf, 0, CAPWAP_MAX_PKT_LEN);
            u1IsNotLinear = OSIX_TRUE;
        }

        if ((CapwapAssembleDiscoveryRsp ((UINT2) u4MsgLen,
                                         pDiscResponse,
                                         pu1TxBuf)) == OSIX_FAILURE)
        {
            CAPWAP_RELEASE_CRU_BUF (pTxBuf);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            UtlShMemFreeDiscRspBuf ((UINT1 *) pDiscResponse);
            return OSIX_FAILURE;
        }

        pu1TempBuf = pu1TxBuf + CAPWAP_MSG_OFFSET;
        CAPWAP_GET_2BYTE (u2TempLength, pu1TempBuf);
        if (u1IsNotLinear == OSIX_TRUE)
        {
            /* If the CRU buffer memory is not linear */
            CAPWAP_COPY_TO_BUF (pTxBuf, pu1TxBuf, 0, CAPWAP_MAX_PKT_LEN);
            MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pu1TxBuf);
        }
        pDiscResponse->u2CapwapMsgElemenLen = u2TempLength;
        i4Status = CapwapCtrlUdpTxPacket (pTxBuf,
                                          destIpAddr.u4_addr[0],
                                          u4DestPort,
                                          (UINT4) (pDiscResponse->
                                                   u2CapwapMsgElemenLen +
                                                   CAPWAP_CMN_HDR_LEN));
        if (OSIX_FAILURE == i4Status)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to Transmit the packet \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to Transmit the packet \r\n"));
        }

        CAPWAP_RELEASE_CRU_BUF (pTxBuf);

        MEMSET (&WssIfPMDB, 0, sizeof (WssIfPMDB));
        WssIfPMDB.u2ProfileId = u2WtpInternalId;
        WssIfPMDB.tWtpPmAttState.wssPmWLCStatsSetDB.bWssPmWLCCapwapDiscRespSent
            = OSIX_TRUE;
        WssIfPMDB.tWtpPmAttState.wssPmWLCStatsSetDB.
            bWssPmWLCCapwapDiscLastSuccAttTime = OSIX_TRUE;
        if (WssIfProcessPMDBMsg (WSS_PM_WLC_CAPWAP_STATS_SET, &WssIfPMDB) !=
            OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to update Discovery stats to PM \n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to update Discovery stats to PM \n"));
        }

        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        UtlShMemFreeDiscRspBuf ((UINT1 *) pDiscResponse);
        return (i4Status);
    }
    else
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to validate the received Disc Req\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to validate the received Disc Req\n"));

        if (u1IsNotLinear == OSIX_TRUE)
        {
            MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pRcvdBuf);
        }
        UtlShMemFreeDiscRspBuf ((UINT1 *) pDiscResponse);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    CAPWAP_FN_EXIT ();
}

/************************************************************************/
/*  Function Name   : CapwapDiscProcessPkt                              */
/*  Description     : The function validates the header and processes   */
/*                    message based on the message type                 */
/*  Input(s)        : pRcvBuf - CAPWAP Discovery packet received       */
/*                    u1DestPort - Received UDP port                    */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
CapwapDiscProcessPkt (tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UINT4               u4MsgType = 0;
    INT4                i4Status = 0;
    tCapwapControlPacket capwapDiscMsg;
    tRemoteSessionManager *pSessEntry = NULL;
    tWssIfPMDB          WssIfPMDB;
    UINT2               u2PacketLen = 0;
    UINT1              *pRcvdBuf = NULL;
    UINT2               u2WtpInternalId = 0;
    UINT1               u1IsNotLinear = OSIX_FALSE;
    CAPWAP_FN_ENTRY ();

    /* Update the interface index and
     * Source IP address from the tMODULE_DATA_PTR
     * structure
     */

    if (CapwapParseControlPacket (pBuf, &capwapDiscMsg) == OSIX_SUCCESS)
    {
        u4MsgType = capwapDiscMsg.capwapCtrlHdr.u4MsgType;
        if (u4MsgType < CAPWAP_MAX_EVENTS)
        {
            i4Status = gCapwapStateMachine[CAPWAP_DISCOVERY][u4MsgType]
                (pBuf, &capwapDiscMsg, pSessEntry);
        }
        else
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "CapwapDiscProcessPkt: u4MsgType is invalid \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "CapwapDiscProcessPkt: Failed to parse the Received discovery packet \r\n"));

            return OSIX_FAILURE;
        }
    }
    else
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapDiscProcessPkt: Failed to parse the Received discovery packet \r\n");
        return OSIX_FAILURE;
    }

    if (i4Status == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapDiscProcessPkt:Failed to "
                    "process the Received discovery packet \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "CapwapDiscProcessPkt:Failed to "
                      "process the Received discovery packet \r\n"));

        u2PacketLen = (UINT2) CAPWAP_GET_BUF_LEN (pBuf);
        pRcvdBuf = CAPWAP_BUF_IF_LINEAR (pBuf, 0, u2PacketLen);

        if (NULL == pRcvdBuf)
        {
            pRcvdBuf = (UINT1 *) MemAllocMemBlk (CAPWAP_PKTBUF_POOLID);
            CAPWAP_COPY_FROM_BUF (pBuf, pRcvdBuf, 0, u2PacketLen);
            u1IsNotLinear = OSIX_TRUE;
        }

        /* Get the internal Id from the CAPWAP get DB */
        if (CapwapGetInternalId (pRcvdBuf, &capwapDiscMsg, &u2WtpInternalId)
            == OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "The Received Base Mac is not configured in the WTP profile \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "The Received Base Mac is not configured in the WTP profile \r\n"));

            if (u1IsNotLinear == OSIX_TRUE)
            {
                MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pRcvdBuf);
            }
            CapwapParseStructureMemrelease (&capwapDiscMsg);
            return OSIX_FAILURE;
        }

        MEMSET (&WssIfPMDB, 0, sizeof (WssIfPMDB));
        WssIfPMDB.u2ProfileId = u2WtpInternalId;
        WssIfPMDB.tWtpPmAttState.wssPmWLCStatsSetDB.
            bWssPmWLCCapwapDiscUnsuccReqProcessed = OSIX_TRUE;
        WssIfPMDB.tWtpPmAttState.wssPmWLCStatsSetDB.
            bWssPmWLCCapwapDiscLastUnsuccAttTime = OSIX_TRUE;
        if (WssIfProcessPMDBMsg (WSS_PM_WLC_CAPWAP_STATS_SET, &WssIfPMDB) !=
            OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to update Discovery stats to PM \n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to update Discovery stats to PM \n"));

        }

        if (u1IsNotLinear == OSIX_TRUE)
        {
            MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pRcvdBuf);
        }

        /* Increment the error count */
    }
    CapwapParseStructureMemrelease (&capwapDiscMsg);
    CAPWAP_FN_EXIT ();
    return i4Status;
}

/*****************************************************************************
 * Function     : CapwapCheckDiscRelinquishCounters                          *
 *                                                                           *
 * Description  : Check relinquish counters and take action                  *
 *                                                                           *
 * Input        : NONE                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
VOID
CapwapDiscCPURelinquishTmrExp (VOID *pArg)
{
    UNUSED_PARAM (pArg);
#if 0
    gu4CPUDiscRelinquishCounter = 0;
    if (OsixEvtSend (gDiscTaskGlobals.capwapDiscTaskId,
                     CAPWAP_DISC_RELINQUISH_EVENT) != OSIX_SUCCESS)
    {
        printf ("Timer event send failure\n");
    }

    if (CapwapTmrStart (NULL, CAPWAP_CPU_DISC_RELINQUISH_TMR,
                        CAPWAP_RELINQUISH_TIMER_VAL) != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, " Timer Start FAILED.\r\n");
    }
#endif
    return;
}

/*****************************************************************************
 * Function     : CapwapCheckServRelinquishCounters                          *
 *                                                                           *
 * Description  : Check relinquish counters and take action                  *
 *                                                                           *
 * Input        : NONE                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
VOID
CapwapCheckDiscRelinquishCounters (VOID)
{
#if 0
#ifdef WTP_WANTED
    UINT4               u4Event 0;
    gu4CPUDiscRelinquishCounter++;
    {
        if (gCapwapState != CAPWAP_IMAGEDATA)
        {
            if (gu4CPUDiscRelinquishCounter == CAPWAP_RELINQUISH_MSG_COUNT)
            {
                OsixEvtRecv (gDiscTaskGlobals.capwapDiscTaskId,
                             CAPWAP_DISC_RELINQUISH_EVENT, OSIX_WAIT, &u4Event);
                gu4CPUDiscRelinquishCounter = 0;
            }
        }
    }
#endif
#endif
    return;
}

#endif /* _CAPWAPDISCOVERY_C_ */
/*-----------------------------------------------------------------------*/
/*                    End of the file  capwapdiscovery.c                 */
/*-----------------------------------------------------------------------*/
