/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: capwapcliutlg.c,v 1.2 2017/05/23 14:16:49 siva Exp $       
*
* Description: This file contains utility functions used by protocol Capwap
*********************************************************************/

#include "capwapinc.h"

/****************************************************************************
 Function    :  CapwapUtlCreateRBTree
 Input       :  None
 Description :  This function creates all 
                the RBTree required.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
PUBLIC INT4
CapwapUtlCreateRBTree ()
{
    if (CapwapCapwapBaseAcNameListTableCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }

    if (CapwapCapwapBaseMacAclTableCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }
    if (CapwapCapwapBaseWtpProfileTableCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }

    if (CapwapCapwapBaseWtpStateTableCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }

    if (CapwapCapwapBaseWtpTableCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }

    if (CapwapCapwapBaseWirelessBindingTableCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }

    if (CapwapCapwapBaseStationTableCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }

    if (CapwapCapwapBaseWtpEventsStatsTableCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }

    if (CapwapCapwapBaseRadioEventsStatsTableCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }

    if (CapwapFsWtpModelTableCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }

    if (CapwapFsWtpRadioTableCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }

    if (CapwapFsCapwapWhiteListTableCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }

    if (CapwapFsCapwapBlackListCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }

    if (CapwapFsCapwapWtpConfigTableCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }

    if (CapwapFsCapwapLinkEncryptionTableCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }

    if (CapwapFsCawapDefaultWtpProfileTableCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }

    if (CapwapFsCapwapDnsProfileTableCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }

    if (CapwapFsWtpNativeVlanIdTableCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }
    if (CapwapFsWtpLocalRoutingTableCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }

    if (CapwapFsCawapDiscStatsTableCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }

    if (CapwapFsCawapJoinStatsTableCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }

    if (CapwapFsCawapConfigStatsTableCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }

    if (CapwapFsCawapRunStatsTableCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }

    if (CapwapFsCapwapWirelessBindingTableCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }

    if (CapwapFsCapwapStationWhiteListCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }

    if (CapwapFsCapwapWtpRebootStatisticsTableCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }

    if (CapwapFsCapwapWtpRadioStatisticsTableCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapUtlDeleteRBTree
 Input       :  None
 Description :  This function creates all 
                the RBTree required.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
PUBLIC INT4
CapwapUtlDeleteRBTree ()
{

    if (gCapwapGlobals.CapwapGlbMib.CapwapBaseAcNameListTable != NULL)
    {
        RBTreeDelete (gCapwapGlobals.CapwapGlbMib.CapwapBaseAcNameListTable);
        gCapwapGlobals.CapwapGlbMib.CapwapBaseAcNameListTable = NULL;
    }

    if (gCapwapGlobals.CapwapGlbMib.CapwapBaseMacAclTable != NULL)
    {
        RBTreeDelete (gCapwapGlobals.CapwapGlbMib.CapwapBaseMacAclTable);
        gCapwapGlobals.CapwapGlbMib.CapwapBaseMacAclTable = NULL;
    }

    if (gCapwapGlobals.CapwapGlbMib.CapwapBaseWtpProfileTable != NULL)
    {
        RBTreeDelete (gCapwapGlobals.CapwapGlbMib.CapwapBaseWtpProfileTable);
        gCapwapGlobals.CapwapGlbMib.CapwapBaseWtpProfileTable = NULL;
    }

    if (gCapwapGlobals.CapwapGlbMib.CapwapBaseWtpStateTable != NULL)
    {
        RBTreeDelete (gCapwapGlobals.CapwapGlbMib.CapwapBaseWtpStateTable);
        gCapwapGlobals.CapwapGlbMib.CapwapBaseWtpStateTable = NULL;
    }

    if (gCapwapGlobals.CapwapGlbMib.CapwapBaseWtpTable != NULL)
    {
        RBTreeDelete (gCapwapGlobals.CapwapGlbMib.CapwapBaseWtpTable);
        gCapwapGlobals.CapwapGlbMib.CapwapBaseWtpTable = NULL;
    }

    if (gCapwapGlobals.CapwapGlbMib.CapwapBaseWirelessBindingTable != NULL)
    {
        RBTreeDelete (gCapwapGlobals.CapwapGlbMib.
                      CapwapBaseWirelessBindingTable);
        gCapwapGlobals.CapwapGlbMib.CapwapBaseWirelessBindingTable = NULL;
    }

    if (gCapwapGlobals.CapwapGlbMib.CapwapBaseStationTable != NULL)
    {
        RBTreeDelete (gCapwapGlobals.CapwapGlbMib.CapwapBaseStationTable);
        gCapwapGlobals.CapwapGlbMib.CapwapBaseStationTable = NULL;
    }

    if (gCapwapGlobals.CapwapGlbMib.CapwapBaseWtpEventsStatsTable != NULL)
    {
        RBTreeDelete (gCapwapGlobals.CapwapGlbMib.
                      CapwapBaseWtpEventsStatsTable);
        gCapwapGlobals.CapwapGlbMib.CapwapBaseWtpEventsStatsTable = NULL;
    }

    if (gCapwapGlobals.CapwapGlbMib.CapwapBaseRadioEventsStatsTable != NULL)
    {
        RBTreeDelete (gCapwapGlobals.CapwapGlbMib.
                      CapwapBaseRadioEventsStatsTable);
        gCapwapGlobals.CapwapGlbMib.CapwapBaseRadioEventsStatsTable = NULL;
    }

    if (gCapwapGlobals.CapwapGlbMib.FsWtpModelTable != NULL)
    {
        RBTreeDelete (gCapwapGlobals.CapwapGlbMib.FsWtpModelTable);
        gCapwapGlobals.CapwapGlbMib.FsWtpModelTable = NULL;
    }

    if (gCapwapGlobals.CapwapGlbMib.FsWtpRadioTable != NULL)
    {
        RBTreeDelete (gCapwapGlobals.CapwapGlbMib.FsWtpRadioTable);
        gCapwapGlobals.CapwapGlbMib.FsWtpRadioTable = NULL;
    }

    if (gCapwapGlobals.CapwapGlbMib.FsCapwapWhiteListTable != NULL)
    {
        RBTreeDelete (gCapwapGlobals.CapwapGlbMib.FsCapwapWhiteListTable);
        gCapwapGlobals.CapwapGlbMib.FsCapwapWhiteListTable = NULL;
    }

    if (gCapwapGlobals.CapwapGlbMib.FsCapwapBlackList != NULL)
    {
        RBTreeDelete (gCapwapGlobals.CapwapGlbMib.FsCapwapBlackList);
        gCapwapGlobals.CapwapGlbMib.FsCapwapBlackList = NULL;
    }

    if (gCapwapGlobals.CapwapGlbMib.FsCapwapWtpConfigTable != NULL)
    {
        RBTreeDelete (gCapwapGlobals.CapwapGlbMib.FsCapwapWtpConfigTable);
        gCapwapGlobals.CapwapGlbMib.FsCapwapWtpConfigTable = NULL;
    }

    if (gCapwapGlobals.CapwapGlbMib.FsCapwapLinkEncryptionTable != NULL)
    {
        RBTreeDelete (gCapwapGlobals.CapwapGlbMib.FsCapwapLinkEncryptionTable);
        gCapwapGlobals.CapwapGlbMib.FsCapwapLinkEncryptionTable = NULL;
    }

    if (gCapwapGlobals.CapwapGlbMib.FsCawapDefaultWtpProfileTable != NULL)
    {
        RBTreeDelete (gCapwapGlobals.CapwapGlbMib.
                      FsCawapDefaultWtpProfileTable);
        gCapwapGlobals.CapwapGlbMib.FsCawapDefaultWtpProfileTable = NULL;
    }

    if (gCapwapGlobals.CapwapGlbMib.FsCapwapDnsProfileTable != NULL)
    {
        RBTreeDelete (gCapwapGlobals.CapwapGlbMib.FsCapwapDnsProfileTable);
        gCapwapGlobals.CapwapGlbMib.FsCapwapDnsProfileTable = NULL;
    }

    if (gCapwapGlobals.CapwapGlbMib.FsWtpNativeVlanIdTable != NULL)
    {
        RBTreeDelete (gCapwapGlobals.CapwapGlbMib.FsWtpNativeVlanIdTable);
        gCapwapGlobals.CapwapGlbMib.FsWtpNativeVlanIdTable = NULL;
    }
    if (gCapwapGlobals.CapwapGlbMib.FsWtpLocalRoutingTable != NULL)
    {
        RBTreeDelete (gCapwapGlobals.CapwapGlbMib.FsWtpLocalRoutingTable);
        gCapwapGlobals.CapwapGlbMib.FsWtpLocalRoutingTable = NULL;
    }

    if (gCapwapGlobals.CapwapGlbMib.FsCawapDiscStatsTable != NULL)
    {
        RBTreeDelete (gCapwapGlobals.CapwapGlbMib.FsCawapDiscStatsTable);
        gCapwapGlobals.CapwapGlbMib.FsCawapDiscStatsTable = NULL;
    }

    if (gCapwapGlobals.CapwapGlbMib.FsCawapJoinStatsTable != NULL)
    {
        RBTreeDelete (gCapwapGlobals.CapwapGlbMib.FsCawapJoinStatsTable);
        gCapwapGlobals.CapwapGlbMib.FsCawapJoinStatsTable = NULL;
    }

    if (gCapwapGlobals.CapwapGlbMib.FsCawapConfigStatsTable != NULL)
    {
        RBTreeDelete (gCapwapGlobals.CapwapGlbMib.FsCawapConfigStatsTable);
        gCapwapGlobals.CapwapGlbMib.FsCawapConfigStatsTable = NULL;
    }

    if (gCapwapGlobals.CapwapGlbMib.FsCawapRunStatsTable != NULL)
    {
        RBTreeDelete (gCapwapGlobals.CapwapGlbMib.FsCawapRunStatsTable);
        gCapwapGlobals.CapwapGlbMib.FsCawapRunStatsTable = NULL;
    }

    if (gCapwapGlobals.CapwapGlbMib.FsCapwapWirelessBindingTable != NULL)
    {
        RBTreeDelete (gCapwapGlobals.CapwapGlbMib.FsCapwapWirelessBindingTable);
        gCapwapGlobals.CapwapGlbMib.FsCapwapWirelessBindingTable = NULL;
    }

    if (gCapwapGlobals.CapwapGlbMib.FsCapwapStationWhiteList != NULL)
    {
        RBTreeDelete (gCapwapGlobals.CapwapGlbMib.FsCapwapStationWhiteList);
        gCapwapGlobals.CapwapGlbMib.FsCapwapStationWhiteList = NULL;
    }

    if (gCapwapGlobals.CapwapGlbMib.FsCapwapWtpRebootStatisticsTable != NULL)
    {
        RBTreeDelete (gCapwapGlobals.CapwapGlbMib.
                      FsCapwapWtpRebootStatisticsTable);
        gCapwapGlobals.CapwapGlbMib.FsCapwapWtpRebootStatisticsTable = NULL;
    }

    if (gCapwapGlobals.CapwapGlbMib.FsCapwapWtpRadioStatisticsTable != NULL)
    {
        RBTreeDelete (gCapwapGlobals.CapwapGlbMib.
                      FsCapwapWtpRadioStatisticsTable);
        gCapwapGlobals.CapwapGlbMib.FsCapwapWtpRadioStatisticsTable = NULL;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapCapwapBaseAcNameListTableCreate
 Input       :  None
 Description :  This function creates the CapwapBaseAcNameListTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
CapwapCapwapBaseAcNameListTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tCapwapCapwapBaseAcNameListEntry,
                       MibObject.CapwapBaseAcNameListTableNode);

    if ((gCapwapGlobals.CapwapGlbMib.CapwapBaseAcNameListTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               CapwapBaseAcNameListTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapCapwapBaseMacAclTableCreate
 Input       :  None
 Description :  This function creates the CapwapBaseMacAclTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
CapwapCapwapBaseMacAclTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tCapwapCapwapBaseMacAclEntry,
                       MibObject.CapwapBaseMacAclTableNode);

    if ((gCapwapGlobals.CapwapGlbMib.CapwapBaseMacAclTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               CapwapBaseMacAclTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapCapwapBaseWtpProfileTableCreate
 Input       :  None
 Description :  This function creates the CapwapBaseWtpProfileTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
CapwapCapwapBaseWtpProfileTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tCapwapCapwapBaseWtpProfileEntry,
                       MibObject.CapwapBaseWtpProfileTableNode);

    if ((gCapwapGlobals.CapwapGlbMib.CapwapBaseWtpProfileTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               CapwapBaseWtpProfileTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapCapwapBaseWtpStateTableCreate
 Input       :  None
 Description :  This function creates the CapwapBaseWtpStateTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
CapwapCapwapBaseWtpStateTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tCapwapCapwapBaseWtpStateEntry,
                       MibObject.CapwapBaseWtpStateTableNode);

    if ((gCapwapGlobals.CapwapGlbMib.CapwapBaseWtpStateTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               CapwapBaseWtpStateTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapCapwapBaseWtpTableCreate
 Input       :  None
 Description :  This function creates the CapwapBaseWtpTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
CapwapCapwapBaseWtpTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tCapwapCapwapBaseWtpEntry,
                       MibObject.CapwapBaseWtpTableNode);

    if ((gCapwapGlobals.CapwapGlbMib.CapwapBaseWtpTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               CapwapBaseWtpTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapCapwapBaseWirelessBindingTableCreate
 Input       :  None
 Description :  This function creates the CapwapBaseWirelessBindingTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
CapwapCapwapBaseWirelessBindingTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tCapwapCapwapBaseWirelessBindingEntry,
                       MibObject.CapwapBaseWirelessBindingTableNode);

    if ((gCapwapGlobals.CapwapGlbMib.CapwapBaseWirelessBindingTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               CapwapBaseWirelessBindingTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapCapwapBaseStationTableCreate
 Input       :  None
 Description :  This function creates the CapwapBaseStationTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
CapwapCapwapBaseStationTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tCapwapCapwapBaseStationEntry,
                       MibObject.CapwapBaseStationTableNode);

    if ((gCapwapGlobals.CapwapGlbMib.CapwapBaseStationTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               CapwapBaseStationTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapCapwapBaseWtpEventsStatsTableCreate
 Input       :  None
 Description :  This function creates the CapwapBaseWtpEventsStatsTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
CapwapCapwapBaseWtpEventsStatsTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tCapwapCapwapBaseWtpEventsStatsEntry,
                       MibObject.CapwapBaseWtpEventsStatsTableNode);

    if ((gCapwapGlobals.CapwapGlbMib.CapwapBaseWtpEventsStatsTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               CapwapBaseWtpEventsStatsTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapCapwapBaseRadioEventsStatsTableCreate
 Input       :  None
 Description :  This function creates the CapwapBaseRadioEventsStatsTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
CapwapCapwapBaseRadioEventsStatsTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tCapwapCapwapBaseRadioEventsStatsEntry,
                       MibObject.CapwapBaseRadioEventsStatsTableNode);

    if ((gCapwapGlobals.CapwapGlbMib.CapwapBaseRadioEventsStatsTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               CapwapBaseRadioEventsStatsTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapFsWtpModelTableCreate
 Input       :  None
 Description :  This function creates the FsWtpModelTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
CapwapFsWtpModelTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tCapwapFsWtpModelEntry, MibObject.FsWtpModelTableNode);

    if ((gCapwapGlobals.CapwapGlbMib.FsWtpModelTable =
         RBTreeCreateEmbedded (u4RBNodeOffset, FsWtpModelTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapFsWtpRadioTableCreate
 Input       :  None
 Description :  This function creates the FsWtpRadioTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
CapwapFsWtpRadioTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tCapwapFsWtpRadioEntry, MibObject.FsWtpRadioTableNode);

    if ((gCapwapGlobals.CapwapGlbMib.FsWtpRadioTable =
         RBTreeCreateEmbedded (u4RBNodeOffset, FsWtpRadioTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapFsCapwapWhiteListTableCreate
 Input       :  None
 Description :  This function creates the FsCapwapWhiteListTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
CapwapFsCapwapWhiteListTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tCapwapFsCapwapWhiteListEntry,
                       MibObject.FsCapwapWhiteListTableNode);

    if ((gCapwapGlobals.CapwapGlbMib.FsCapwapWhiteListTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               FsCapwapWhiteListTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapFsCapwapBlackListCreate
 Input       :  None
 Description :  This function creates the FsCapwapBlackList
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
CapwapFsCapwapBlackListCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tCapwapFsCapwapBlackListEntry,
                       MibObject.FsCapwapBlackListNode);

    if ((gCapwapGlobals.CapwapGlbMib.FsCapwapBlackList =
         RBTreeCreateEmbedded (u4RBNodeOffset, FsCapwapBlackListRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapFsCapwapWtpConfigTableCreate
 Input       :  None
 Description :  This function creates the FsCapwapWtpConfigTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
CapwapFsCapwapWtpConfigTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tCapwapFsCapwapWtpConfigEntry,
                       MibObject.FsCapwapWtpConfigTableNode);

    if ((gCapwapGlobals.CapwapGlbMib.FsCapwapWtpConfigTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               FsCapwapWtpConfigTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapFsCapwapLinkEncryptionTableCreate
 Input       :  None
 Description :  This function creates the FsCapwapLinkEncryptionTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
CapwapFsCapwapLinkEncryptionTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tCapwapFsCapwapLinkEncryptionEntry,
                       MibObject.FsCapwapLinkEncryptionTableNode);

    if ((gCapwapGlobals.CapwapGlbMib.FsCapwapLinkEncryptionTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               FsCapwapLinkEncryptionTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapFsCawapDefaultWtpProfileTableCreate
 Input       :  None
 Description :  This function creates the FsCawapDefaultWtpProfileTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
CapwapFsCawapDefaultWtpProfileTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tCapwapFsCapwapDefaultWtpProfileEntry,
                       MibObject.FsCawapDefaultWtpProfileTableNode);

    if ((gCapwapGlobals.CapwapGlbMib.FsCawapDefaultWtpProfileTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               FsCawapDefaultWtpProfileTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapFsCapwapDnsProfileTableCreate
 Input       :  None
 Description :  This function creates the FsCapwapDnsProfileTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
CapwapFsCapwapDnsProfileTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tCapwapFsCapwapDnsProfileEntry,
                       MibObject.FsCapwapDnsProfileTableNode);

    if ((gCapwapGlobals.CapwapGlbMib.FsCapwapDnsProfileTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               FsCapwapDnsProfileTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapFsWtpNativeVlanIdTableCreate
 Input       :  None
 Description :  This function creates the FsWtpNativeVlanIdTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
CapwapFsWtpNativeVlanIdTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tCapwapFsWtpNativeVlanIdTable,
                       MibObject.FsWtpNativeVlanIdTableNode);

    if ((gCapwapGlobals.CapwapGlbMib.FsWtpNativeVlanIdTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               FsWtpNativeVlanIdTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapFsWtpLocalRoutingTableCreate
 Input       :  None
 Description :  This function creates the FsWtpLocalRoutingTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
CapwapFsWtpLocalRoutingTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tCapwapFsWtpLocalRoutingTable,
                       MibObject.FsWtpLocalRoutingTableNode);

    if ((gCapwapGlobals.CapwapGlbMib.FsWtpLocalRoutingTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               FsWtpLocalRoutingTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapFsCawapDiscStatsTableCreate
 Input       :  None
 Description :  This function creates the FsCawapDiscStatsTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
CapwapFsCawapDiscStatsTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tCapwapFsCawapDiscStatsEntry,
                       MibObject.FsCawapDiscStatsTableNode);

    if ((gCapwapGlobals.CapwapGlbMib.FsCawapDiscStatsTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               FsCawapDiscStatsTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapFsCawapJoinStatsTableCreate
 Input       :  None
 Description :  This function creates the FsCawapJoinStatsTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
CapwapFsCawapJoinStatsTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tCapwapFsCawapJoinStatsEntry,
                       MibObject.FsCawapJoinStatsTableNode);

    if ((gCapwapGlobals.CapwapGlbMib.FsCawapJoinStatsTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               FsCawapJoinStatsTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapFsCawapConfigStatsTableCreate
 Input       :  None
 Description :  This function creates the FsCawapConfigStatsTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
CapwapFsCawapConfigStatsTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tCapwapFsCawapConfigStatsEntry,
                       MibObject.FsCawapConfigStatsTableNode);

    if ((gCapwapGlobals.CapwapGlbMib.FsCawapConfigStatsTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               FsCawapConfigStatsTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapFsCawapRunStatsTableCreate
 Input       :  None
 Description :  This function creates the FsCawapRunStatsTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
CapwapFsCawapRunStatsTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tCapwapFsCawapRunStatsEntry,
                       MibObject.FsCawapRunStatsTableNode);

    if ((gCapwapGlobals.CapwapGlbMib.FsCawapRunStatsTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               FsCawapRunStatsTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapFsCapwapWirelessBindingTableCreate
 Input       :  None
 Description :  This function creates the FsCapwapWirelessBindingTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
CapwapFsCapwapWirelessBindingTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tCapwapFsCapwapWirelessBindingEntry,
                       MibObject.FsCapwapWirelessBindingTableNode);

    if ((gCapwapGlobals.CapwapGlbMib.FsCapwapWirelessBindingTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               FsCapwapWirelessBindingTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapFsCapwapStationWhiteListCreate
 Input       :  None
 Description :  This function creates the FsCapwapStationWhiteList
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
CapwapFsCapwapStationWhiteListCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tCapwapFsCapwapStationWhiteListEntry,
                       MibObject.FsCapwapStationWhiteListNode);

    if ((gCapwapGlobals.CapwapGlbMib.FsCapwapStationWhiteList =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               FsCapwapStationWhiteListRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapFsCapwapWtpRebootStatisticsTableCreate
 Input       :  None
 Description :  This function creates the FsCapwapWtpRebootStatisticsTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
CapwapFsCapwapWtpRebootStatisticsTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tCapwapFsCapwapWtpRebootStatisticsEntry,
                       MibObject.FsCapwapWtpRebootStatisticsTableNode);

    if ((gCapwapGlobals.CapwapGlbMib.FsCapwapWtpRebootStatisticsTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               FsCapwapWtpRebootStatisticsTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapFsCapwapWtpRadioStatisticsTableCreate
 Input       :  None
 Description :  This function creates the FsCapwapWtpRadioStatisticsTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
CapwapFsCapwapWtpRadioStatisticsTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tCapwapFsCapwapWtpRadioStatisticsEntry,
                       MibObject.FsCapwapWtpRadioStatisticsTableNode);

    if ((gCapwapGlobals.CapwapGlbMib.FsCapwapWtpRadioStatisticsTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               FsCapwapWtpRadioStatisticsTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapBaseAcNameListTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                CapwapBaseAcNameListTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
CapwapBaseAcNameListTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tCapwapCapwapBaseAcNameListEntry *pCapwapBaseAcNameListEntry1 =
        (tCapwapCapwapBaseAcNameListEntry *) pRBElem1;
    tCapwapCapwapBaseAcNameListEntry *pCapwapBaseAcNameListEntry2 =
        (tCapwapCapwapBaseAcNameListEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pCapwapBaseAcNameListEntry1->MibObject.u4CapwapBaseAcNameListId >
        pCapwapBaseAcNameListEntry2->MibObject.u4CapwapBaseAcNameListId)
    {
        return 1;
    }
    else if (pCapwapBaseAcNameListEntry1->MibObject.u4CapwapBaseAcNameListId <
             pCapwapBaseAcNameListEntry2->MibObject.u4CapwapBaseAcNameListId)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  CapwapBaseMacAclTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                CapwapBaseMacAclTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
CapwapBaseMacAclTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tCapwapCapwapBaseMacAclEntry *pCapwapBaseMacAclEntry1 =
        (tCapwapCapwapBaseMacAclEntry *) pRBElem1;
    tCapwapCapwapBaseMacAclEntry *pCapwapBaseMacAclEntry2 =
        (tCapwapCapwapBaseMacAclEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pCapwapBaseMacAclEntry1->MibObject.u4CapwapBaseMacAclId >
        pCapwapBaseMacAclEntry2->MibObject.u4CapwapBaseMacAclId)
    {
        return 1;
    }
    else if (pCapwapBaseMacAclEntry1->MibObject.u4CapwapBaseMacAclId <
             pCapwapBaseMacAclEntry2->MibObject.u4CapwapBaseMacAclId)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  CapwapBaseWtpProfileTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                CapwapBaseWtpProfileTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
CapwapBaseWtpProfileTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tCapwapCapwapBaseWtpProfileEntry *pCapwapBaseWtpProfileEntry1 =
        (tCapwapCapwapBaseWtpProfileEntry *) pRBElem1;
    tCapwapCapwapBaseWtpProfileEntry *pCapwapBaseWtpProfileEntry2 =
        (tCapwapCapwapBaseWtpProfileEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pCapwapBaseWtpProfileEntry1->MibObject.u4CapwapBaseWtpProfileId >
        pCapwapBaseWtpProfileEntry2->MibObject.u4CapwapBaseWtpProfileId)
    {
        return 1;
    }
    else if (pCapwapBaseWtpProfileEntry1->MibObject.u4CapwapBaseWtpProfileId <
             pCapwapBaseWtpProfileEntry2->MibObject.u4CapwapBaseWtpProfileId)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  CapwapBaseWtpStateTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                CapwapBaseWtpStateTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
CapwapBaseWtpStateTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tCapwapCapwapBaseWtpStateEntry *pCapwapBaseWtpStateEntry1 =
        (tCapwapCapwapBaseWtpStateEntry *) pRBElem1;
    tCapwapCapwapBaseWtpStateEntry *pCapwapBaseWtpStateEntry2 =
        (tCapwapCapwapBaseWtpStateEntry *) pRBElem2;
    INT4                i4MaxLength = 0;
    i4MaxLength = 0;
    if (pCapwapBaseWtpStateEntry1->MibObject.i4CapwapBaseWtpStateWtpIdLen >
        pCapwapBaseWtpStateEntry2->MibObject.i4CapwapBaseWtpStateWtpIdLen)
    {
        i4MaxLength =
            pCapwapBaseWtpStateEntry1->MibObject.i4CapwapBaseWtpStateWtpIdLen;
    }
    else
    {
        i4MaxLength =
            pCapwapBaseWtpStateEntry2->MibObject.i4CapwapBaseWtpStateWtpIdLen;
    }

    if (MEMCMP
        (pCapwapBaseWtpStateEntry1->MibObject.au1CapwapBaseWtpStateWtpId,
         pCapwapBaseWtpStateEntry2->MibObject.au1CapwapBaseWtpStateWtpId,
         i4MaxLength) > 0)
    {
        return 1;
    }
    else if (MEMCMP
             (pCapwapBaseWtpStateEntry1->MibObject.au1CapwapBaseWtpStateWtpId,
              pCapwapBaseWtpStateEntry2->MibObject.au1CapwapBaseWtpStateWtpId,
              i4MaxLength) < 0)
    {
        return -1;
    }

    if (pCapwapBaseWtpStateEntry1->MibObject.i4CapwapBaseWtpStateWtpIdLen >
        pCapwapBaseWtpStateEntry2->MibObject.i4CapwapBaseWtpStateWtpIdLen)
    {
        return 1;
    }
    else if (pCapwapBaseWtpStateEntry1->MibObject.i4CapwapBaseWtpStateWtpIdLen <
             pCapwapBaseWtpStateEntry2->MibObject.i4CapwapBaseWtpStateWtpIdLen)
    {
        return -1;
    }
    return 0;
}

/****************************************************************************
 Function    :  CapwapBaseWtpTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                CapwapBaseWtpTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
CapwapBaseWtpTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tCapwapCapwapBaseWtpEntry *pCapwapBaseWtpEntry1 =
        (tCapwapCapwapBaseWtpEntry *) pRBElem1;
    tCapwapCapwapBaseWtpEntry *pCapwapBaseWtpEntry2 =
        (tCapwapCapwapBaseWtpEntry *) pRBElem2;
    INT4                i4MaxLength = 0;
    i4MaxLength = 0;
    if (pCapwapBaseWtpEntry1->MibObject.i4CapwapBaseWtpCurrIdLen >
        pCapwapBaseWtpEntry2->MibObject.i4CapwapBaseWtpCurrIdLen)
    {
        i4MaxLength = pCapwapBaseWtpEntry1->MibObject.i4CapwapBaseWtpCurrIdLen;
    }
    else
    {
        i4MaxLength = pCapwapBaseWtpEntry2->MibObject.i4CapwapBaseWtpCurrIdLen;
    }

    if (MEMCMP
        (pCapwapBaseWtpEntry1->MibObject.au1CapwapBaseWtpCurrId,
         pCapwapBaseWtpEntry2->MibObject.au1CapwapBaseWtpCurrId,
         i4MaxLength) > 0)
    {
        return 1;
    }
    else if (MEMCMP
             (pCapwapBaseWtpEntry1->MibObject.au1CapwapBaseWtpCurrId,
              pCapwapBaseWtpEntry2->MibObject.au1CapwapBaseWtpCurrId,
              i4MaxLength) < 0)
    {
        return -1;
    }

    if (pCapwapBaseWtpEntry1->MibObject.i4CapwapBaseWtpCurrIdLen >
        pCapwapBaseWtpEntry2->MibObject.i4CapwapBaseWtpCurrIdLen)
    {
        return 1;
    }
    else if (pCapwapBaseWtpEntry1->MibObject.i4CapwapBaseWtpCurrIdLen <
             pCapwapBaseWtpEntry2->MibObject.i4CapwapBaseWtpCurrIdLen)
    {
        return -1;
    }
    return 0;
}

/****************************************************************************
 Function    :  CapwapBaseWirelessBindingTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                CapwapBaseWirelessBindingTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
CapwapBaseWirelessBindingTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tCapwapCapwapBaseWirelessBindingEntry *pCapwapBaseWirelessBindingEntry1 =
        (tCapwapCapwapBaseWirelessBindingEntry *) pRBElem1;
    tCapwapCapwapBaseWirelessBindingEntry *pCapwapBaseWirelessBindingEntry2 =
        (tCapwapCapwapBaseWirelessBindingEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pCapwapBaseWirelessBindingEntry1->MibObject.
        u4CapwapBaseWirelessBindingRadioId >
        pCapwapBaseWirelessBindingEntry2->MibObject.
        u4CapwapBaseWirelessBindingRadioId)
    {
        return 1;
    }
    else if (pCapwapBaseWirelessBindingEntry1->MibObject.
             u4CapwapBaseWirelessBindingRadioId <
             pCapwapBaseWirelessBindingEntry2->MibObject.
             u4CapwapBaseWirelessBindingRadioId)
    {
        return -1;
    }

    if (pCapwapBaseWirelessBindingEntry1->MibObject.u4CapwapBaseWtpProfileId >
        pCapwapBaseWirelessBindingEntry2->MibObject.u4CapwapBaseWtpProfileId)
    {
        return 1;
    }
    else if (pCapwapBaseWirelessBindingEntry1->MibObject.
             u4CapwapBaseWtpProfileId <
             pCapwapBaseWirelessBindingEntry2->MibObject.
             u4CapwapBaseWtpProfileId)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  CapwapBaseStationTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                CapwapBaseStationTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
CapwapBaseStationTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tCapwapCapwapBaseStationEntry *pCapwapBaseStationEntry1 =
        (tCapwapCapwapBaseStationEntry *) pRBElem1;
    tCapwapCapwapBaseStationEntry *pCapwapBaseStationEntry2 =
        (tCapwapCapwapBaseStationEntry *) pRBElem2;
    INT4                i4MaxLength = 0;
    i4MaxLength = 0;
    if (pCapwapBaseStationEntry1->MibObject.i4CapwapBaseStationIdLen >
        pCapwapBaseStationEntry2->MibObject.i4CapwapBaseStationIdLen)
    {
        i4MaxLength =
            pCapwapBaseStationEntry1->MibObject.i4CapwapBaseStationIdLen;
    }
    else
    {
        i4MaxLength =
            pCapwapBaseStationEntry2->MibObject.i4CapwapBaseStationIdLen;
    }

    if (MEMCMP
        (pCapwapBaseStationEntry1->MibObject.au1CapwapBaseStationId,
         pCapwapBaseStationEntry2->MibObject.au1CapwapBaseStationId,
         i4MaxLength) > 0)
    {
        return 1;
    }
    else if (MEMCMP
             (pCapwapBaseStationEntry1->MibObject.au1CapwapBaseStationId,
              pCapwapBaseStationEntry2->MibObject.au1CapwapBaseStationId,
              i4MaxLength) < 0)
    {
        return -1;
    }

    if (pCapwapBaseStationEntry1->MibObject.i4CapwapBaseStationIdLen >
        pCapwapBaseStationEntry2->MibObject.i4CapwapBaseStationIdLen)
    {
        return 1;
    }
    else if (pCapwapBaseStationEntry1->MibObject.i4CapwapBaseStationIdLen <
             pCapwapBaseStationEntry2->MibObject.i4CapwapBaseStationIdLen)
    {
        return -1;
    }
    return 0;
}

/****************************************************************************
 Function    :  CapwapBaseWtpEventsStatsTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                CapwapBaseWtpEventsStatsTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
CapwapBaseWtpEventsStatsTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tCapwapCapwapBaseWtpEventsStatsEntry *pCapwapBaseWtpEventsStatsEntry1 =
        (tCapwapCapwapBaseWtpEventsStatsEntry *) pRBElem1;
    tCapwapCapwapBaseWtpEventsStatsEntry *pCapwapBaseWtpEventsStatsEntry2 =
        (tCapwapCapwapBaseWtpEventsStatsEntry *) pRBElem2;
    INT4                i4MaxLength = 0;
    i4MaxLength = 0;
    if (pCapwapBaseWtpEventsStatsEntry1->MibObject.i4CapwapBaseWtpCurrIdLen >
        pCapwapBaseWtpEventsStatsEntry2->MibObject.i4CapwapBaseWtpCurrIdLen)
    {
        i4MaxLength =
            pCapwapBaseWtpEventsStatsEntry1->MibObject.i4CapwapBaseWtpCurrIdLen;
    }
    else
    {
        i4MaxLength =
            pCapwapBaseWtpEventsStatsEntry2->MibObject.i4CapwapBaseWtpCurrIdLen;
    }

    if (MEMCMP
        (pCapwapBaseWtpEventsStatsEntry1->MibObject.au1CapwapBaseWtpCurrId,
         pCapwapBaseWtpEventsStatsEntry2->MibObject.au1CapwapBaseWtpCurrId,
         i4MaxLength) > 0)
    {
        return 1;
    }
    else if (MEMCMP
             (pCapwapBaseWtpEventsStatsEntry1->MibObject.au1CapwapBaseWtpCurrId,
              pCapwapBaseWtpEventsStatsEntry2->MibObject.au1CapwapBaseWtpCurrId,
              i4MaxLength) < 0)
    {
        return -1;
    }

    if (pCapwapBaseWtpEventsStatsEntry1->MibObject.i4CapwapBaseWtpCurrIdLen >
        pCapwapBaseWtpEventsStatsEntry2->MibObject.i4CapwapBaseWtpCurrIdLen)
    {
        return 1;
    }
    else if (pCapwapBaseWtpEventsStatsEntry1->MibObject.
             i4CapwapBaseWtpCurrIdLen <
             pCapwapBaseWtpEventsStatsEntry2->MibObject.
             i4CapwapBaseWtpCurrIdLen)
    {
        return -1;
    }
    return 0;
}

/****************************************************************************
 Function    :  CapwapBaseRadioEventsStatsTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                CapwapBaseRadioEventsStatsTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
CapwapBaseRadioEventsStatsTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tCapwapCapwapBaseRadioEventsStatsEntry *pCapwapBaseRadioEventsStatsEntry1 =
        (tCapwapCapwapBaseRadioEventsStatsEntry *) pRBElem1;
    tCapwapCapwapBaseRadioEventsStatsEntry *pCapwapBaseRadioEventsStatsEntry2 =
        (tCapwapCapwapBaseRadioEventsStatsEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pCapwapBaseRadioEventsStatsEntry1->MibObject.
        u4CapwapBaseRadioEventsWtpRadioId >
        pCapwapBaseRadioEventsStatsEntry2->MibObject.
        u4CapwapBaseRadioEventsWtpRadioId)
    {
        return 1;
    }
    else if (pCapwapBaseRadioEventsStatsEntry1->MibObject.
             u4CapwapBaseRadioEventsWtpRadioId <
             pCapwapBaseRadioEventsStatsEntry2->MibObject.
             u4CapwapBaseRadioEventsWtpRadioId)
    {
        return -1;
    }

    i4MaxLength = 0;
    if (pCapwapBaseRadioEventsStatsEntry1->MibObject.i4CapwapBaseWtpCurrIdLen >
        pCapwapBaseRadioEventsStatsEntry2->MibObject.i4CapwapBaseWtpCurrIdLen)
    {
        i4MaxLength =
            pCapwapBaseRadioEventsStatsEntry1->MibObject.
            i4CapwapBaseWtpCurrIdLen;
    }
    else
    {
        i4MaxLength =
            pCapwapBaseRadioEventsStatsEntry2->MibObject.
            i4CapwapBaseWtpCurrIdLen;
    }

    if (MEMCMP
        (pCapwapBaseRadioEventsStatsEntry1->MibObject.au1CapwapBaseWtpCurrId,
         pCapwapBaseRadioEventsStatsEntry2->MibObject.au1CapwapBaseWtpCurrId,
         i4MaxLength) > 0)
    {
        return 1;
    }
    else if (MEMCMP
             (pCapwapBaseRadioEventsStatsEntry1->MibObject.
              au1CapwapBaseWtpCurrId,
              pCapwapBaseRadioEventsStatsEntry2->MibObject.
              au1CapwapBaseWtpCurrId, i4MaxLength) < 0)
    {
        return -1;
    }

    if (pCapwapBaseRadioEventsStatsEntry1->MibObject.i4CapwapBaseWtpCurrIdLen >
        pCapwapBaseRadioEventsStatsEntry2->MibObject.i4CapwapBaseWtpCurrIdLen)
    {
        return 1;
    }
    else if (pCapwapBaseRadioEventsStatsEntry1->MibObject.
             i4CapwapBaseWtpCurrIdLen <
             pCapwapBaseRadioEventsStatsEntry2->MibObject.
             i4CapwapBaseWtpCurrIdLen)
    {
        return -1;
    }
    return 0;
}

/****************************************************************************
 Function    :  FsWtpModelTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsWtpModelTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsWtpModelTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tCapwapFsWtpModelEntry *pFsWtpModelEntry1 =
        (tCapwapFsWtpModelEntry *) pRBElem1;
    tCapwapFsWtpModelEntry *pFsWtpModelEntry2 =
        (tCapwapFsWtpModelEntry *) pRBElem2;
    INT4                i4MaxLength = 0;
    i4MaxLength = 0;
    if (pFsWtpModelEntry1->MibObject.i4FsCapwapWtpModelNumberLen >
        pFsWtpModelEntry2->MibObject.i4FsCapwapWtpModelNumberLen)
    {
        i4MaxLength = pFsWtpModelEntry1->MibObject.i4FsCapwapWtpModelNumberLen;
    }
    else
    {
        i4MaxLength = pFsWtpModelEntry2->MibObject.i4FsCapwapWtpModelNumberLen;
    }

    if (MEMCMP
        (pFsWtpModelEntry1->MibObject.au1FsCapwapWtpModelNumber,
         pFsWtpModelEntry2->MibObject.au1FsCapwapWtpModelNumber,
         i4MaxLength) > 0)
    {
        return 1;
    }
    else if (MEMCMP
             (pFsWtpModelEntry1->MibObject.au1FsCapwapWtpModelNumber,
              pFsWtpModelEntry2->MibObject.au1FsCapwapWtpModelNumber,
              i4MaxLength) < 0)
    {
        return -1;
    }

    if (pFsWtpModelEntry1->MibObject.i4FsCapwapWtpModelNumberLen >
        pFsWtpModelEntry2->MibObject.i4FsCapwapWtpModelNumberLen)
    {
        return 1;
    }
    else if (pFsWtpModelEntry1->MibObject.i4FsCapwapWtpModelNumberLen <
             pFsWtpModelEntry2->MibObject.i4FsCapwapWtpModelNumberLen)
    {
        return -1;
    }
    return 0;
}

/****************************************************************************
 Function    :  FsWtpRadioTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsWtpRadioTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsWtpRadioTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tCapwapFsWtpRadioEntry *pFsWtpRadioEntry1 =
        (tCapwapFsWtpRadioEntry *) pRBElem1;
    tCapwapFsWtpRadioEntry *pFsWtpRadioEntry2 =
        (tCapwapFsWtpRadioEntry *) pRBElem2;
    INT4                i4MaxLength = 0;
    i4MaxLength = 0;
    if (pFsWtpRadioEntry1->MibObject.i4FsCapwapWtpModelNumberLen >
        pFsWtpRadioEntry2->MibObject.i4FsCapwapWtpModelNumberLen)
    {
        i4MaxLength = pFsWtpRadioEntry1->MibObject.i4FsCapwapWtpModelNumberLen;
    }
    else
    {
        i4MaxLength = pFsWtpRadioEntry2->MibObject.i4FsCapwapWtpModelNumberLen;
    }

    if (MEMCMP
        (pFsWtpRadioEntry1->MibObject.au1FsCapwapWtpModelNumber,
         pFsWtpRadioEntry2->MibObject.au1FsCapwapWtpModelNumber,
         i4MaxLength) > 0)
    {
        return 1;
    }
    else if (MEMCMP
             (pFsWtpRadioEntry1->MibObject.au1FsCapwapWtpModelNumber,
              pFsWtpRadioEntry2->MibObject.au1FsCapwapWtpModelNumber,
              i4MaxLength) < 0)
    {
        return -1;
    }

    if (pFsWtpRadioEntry1->MibObject.i4FsCapwapWtpModelNumberLen >
        pFsWtpRadioEntry2->MibObject.i4FsCapwapWtpModelNumberLen)
    {
        return 1;
    }
    else if (pFsWtpRadioEntry1->MibObject.i4FsCapwapWtpModelNumberLen <
             pFsWtpRadioEntry2->MibObject.i4FsCapwapWtpModelNumberLen)
    {
        return -1;
    }

    if (pFsWtpRadioEntry1->MibObject.u4FsNumOfRadio >
        pFsWtpRadioEntry2->MibObject.u4FsNumOfRadio)
    {
        return 1;
    }
    else if (pFsWtpRadioEntry1->MibObject.u4FsNumOfRadio <
             pFsWtpRadioEntry2->MibObject.u4FsNumOfRadio)
    {
        return -1;
    }

    return 0;
}

/****************************************************************************
 Function    :  FsCapwapWhiteListTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsCapwapWhiteListTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsCapwapWhiteListTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tCapwapFsCapwapWhiteListEntry *pFsCapwapWhiteListEntry1 =
        (tCapwapFsCapwapWhiteListEntry *) pRBElem1;
    tCapwapFsCapwapWhiteListEntry *pFsCapwapWhiteListEntry2 =
        (tCapwapFsCapwapWhiteListEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsCapwapWhiteListEntry1->MibObject.u4FsCapwapWhiteListId >
        pFsCapwapWhiteListEntry2->MibObject.u4FsCapwapWhiteListId)
    {
        return 1;
    }
    else if (pFsCapwapWhiteListEntry1->MibObject.u4FsCapwapWhiteListId <
             pFsCapwapWhiteListEntry2->MibObject.u4FsCapwapWhiteListId)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  FsCapwapBlackListRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsCapwapBlackList table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsCapwapBlackListRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tCapwapFsCapwapBlackListEntry *pFsCapwapBlackListEntry1 =
        (tCapwapFsCapwapBlackListEntry *) pRBElem1;
    tCapwapFsCapwapBlackListEntry *pFsCapwapBlackListEntry2 =
        (tCapwapFsCapwapBlackListEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsCapwapBlackListEntry1->MibObject.u4FsCapwapBlackListId >
        pFsCapwapBlackListEntry2->MibObject.u4FsCapwapBlackListId)
    {
        return 1;
    }
    else if (pFsCapwapBlackListEntry1->MibObject.u4FsCapwapBlackListId <
             pFsCapwapBlackListEntry2->MibObject.u4FsCapwapBlackListId)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  FsCapwapWtpConfigTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsCapwapWtpConfigTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsCapwapWtpConfigTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tCapwapFsCapwapWtpConfigEntry *pFsCapwapWtpConfigEntry1 =
        (tCapwapFsCapwapWtpConfigEntry *) pRBElem1;
    tCapwapFsCapwapWtpConfigEntry *pFsCapwapWtpConfigEntry2 =
        (tCapwapFsCapwapWtpConfigEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsCapwapWtpConfigEntry1->MibObject.u4CapwapBaseWtpProfileId >
        pFsCapwapWtpConfigEntry2->MibObject.u4CapwapBaseWtpProfileId)
    {
        return 1;
    }
    else if (pFsCapwapWtpConfigEntry1->MibObject.u4CapwapBaseWtpProfileId <
             pFsCapwapWtpConfigEntry2->MibObject.u4CapwapBaseWtpProfileId)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  FsCapwapLinkEncryptionTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsCapwapLinkEncryptionTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsCapwapLinkEncryptionTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tCapwapFsCapwapLinkEncryptionEntry *pFsCapwapLinkEncryptionEntry1 =
        (tCapwapFsCapwapLinkEncryptionEntry *) pRBElem1;
    tCapwapFsCapwapLinkEncryptionEntry *pFsCapwapLinkEncryptionEntry2 =
        (tCapwapFsCapwapLinkEncryptionEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsCapwapLinkEncryptionEntry1->MibObject.u4CapwapBaseWtpProfileId >
        pFsCapwapLinkEncryptionEntry2->MibObject.u4CapwapBaseWtpProfileId)
    {
        return 1;
    }
    else if (pFsCapwapLinkEncryptionEntry1->MibObject.u4CapwapBaseWtpProfileId <
             pFsCapwapLinkEncryptionEntry2->MibObject.u4CapwapBaseWtpProfileId)
    {
        return -1;
    }

    if (pFsCapwapLinkEncryptionEntry1->MibObject.i4FsCapwapEncryptChannel >
        pFsCapwapLinkEncryptionEntry2->MibObject.i4FsCapwapEncryptChannel)
    {
        return 1;
    }
    else if (pFsCapwapLinkEncryptionEntry1->MibObject.i4FsCapwapEncryptChannel <
             pFsCapwapLinkEncryptionEntry2->MibObject.i4FsCapwapEncryptChannel)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  FsCawapDefaultWtpProfileTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsCawapDefaultWtpProfileTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsCawapDefaultWtpProfileTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tCapwapFsCapwapDefaultWtpProfileEntry *pFsCapwapDefaultWtpProfileEntry1 =
        (tCapwapFsCapwapDefaultWtpProfileEntry *) pRBElem1;
    tCapwapFsCapwapDefaultWtpProfileEntry *pFsCapwapDefaultWtpProfileEntry2 =
        (tCapwapFsCapwapDefaultWtpProfileEntry *) pRBElem2;
    INT4                i4MaxLength = 0;
    i4MaxLength = 0;
    if (pFsCapwapDefaultWtpProfileEntry1->MibObject.
        i4FsCapwapDefaultWtpProfileModelNumberLen >
        pFsCapwapDefaultWtpProfileEntry2->MibObject.
        i4FsCapwapDefaultWtpProfileModelNumberLen)
    {
        i4MaxLength =
            pFsCapwapDefaultWtpProfileEntry1->MibObject.
            i4FsCapwapDefaultWtpProfileModelNumberLen;
    }
    else
    {
        i4MaxLength =
            pFsCapwapDefaultWtpProfileEntry2->MibObject.
            i4FsCapwapDefaultWtpProfileModelNumberLen;
    }

    if (MEMCMP
        (pFsCapwapDefaultWtpProfileEntry1->MibObject.
         au1FsCapwapDefaultWtpProfileModelNumber,
         pFsCapwapDefaultWtpProfileEntry2->MibObject.
         au1FsCapwapDefaultWtpProfileModelNumber, i4MaxLength) > 0)
    {
        return 1;
    }
    else if (MEMCMP
             (pFsCapwapDefaultWtpProfileEntry1->MibObject.
              au1FsCapwapDefaultWtpProfileModelNumber,
              pFsCapwapDefaultWtpProfileEntry2->MibObject.
              au1FsCapwapDefaultWtpProfileModelNumber, i4MaxLength) < 0)
    {
        return -1;
    }

    if (pFsCapwapDefaultWtpProfileEntry1->MibObject.
        i4FsCapwapDefaultWtpProfileModelNumberLen >
        pFsCapwapDefaultWtpProfileEntry2->MibObject.
        i4FsCapwapDefaultWtpProfileModelNumberLen)
    {
        return 1;
    }
    else if (pFsCapwapDefaultWtpProfileEntry1->MibObject.
             i4FsCapwapDefaultWtpProfileModelNumberLen <
             pFsCapwapDefaultWtpProfileEntry2->MibObject.
             i4FsCapwapDefaultWtpProfileModelNumberLen)
    {
        return -1;
    }
    return 0;
}

/****************************************************************************
 Function    :  FsCapwapDnsProfileTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsCapwapDnsProfileTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsCapwapDnsProfileTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tCapwapFsCapwapDnsProfileEntry *pFsCapwapDnsProfileEntry1 =
        (tCapwapFsCapwapDnsProfileEntry *) pRBElem1;
    tCapwapFsCapwapDnsProfileEntry *pFsCapwapDnsProfileEntry2 =
        (tCapwapFsCapwapDnsProfileEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsCapwapDnsProfileEntry1->MibObject.u4CapwapBaseWtpProfileId >
        pFsCapwapDnsProfileEntry2->MibObject.u4CapwapBaseWtpProfileId)
    {
        return 1;
    }
    else if (pFsCapwapDnsProfileEntry1->MibObject.u4CapwapBaseWtpProfileId <
             pFsCapwapDnsProfileEntry2->MibObject.u4CapwapBaseWtpProfileId)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  FsWtpNativeVlanIdTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsWtpNativeVlanIdTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsWtpNativeVlanIdTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tCapwapFsWtpNativeVlanIdTable *pFsWtpNativeVlanIdTable1 =
        (tCapwapFsWtpNativeVlanIdTable *) pRBElem1;
    tCapwapFsWtpNativeVlanIdTable *pFsWtpNativeVlanIdTable2 =
        (tCapwapFsWtpNativeVlanIdTable *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsWtpNativeVlanIdTable1->MibObject.u4CapwapBaseWtpProfileId >
        pFsWtpNativeVlanIdTable2->MibObject.u4CapwapBaseWtpProfileId)
    {
        return 1;
    }
    else if (pFsWtpNativeVlanIdTable1->MibObject.u4CapwapBaseWtpProfileId <
             pFsWtpNativeVlanIdTable2->MibObject.u4CapwapBaseWtpProfileId)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  FsWtpLocalRoutingTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsWtpLocalRoutingTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsWtpLocalRoutingTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tCapwapFsWtpLocalRoutingTable *pFsWtpLocalRoutingTable1 =
        (tCapwapFsWtpLocalRoutingTable *) pRBElem1;
    tCapwapFsWtpLocalRoutingTable *pFsWtpLocalRoutingTable2 =
        (tCapwapFsWtpLocalRoutingTable *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsWtpLocalRoutingTable1->MibObject.u4CapwapBaseWtpProfileId >
        pFsWtpLocalRoutingTable2->MibObject.u4CapwapBaseWtpProfileId)
    {
        return 1;
    }
    else if (pFsWtpLocalRoutingTable1->MibObject.u4CapwapBaseWtpProfileId <
             pFsWtpLocalRoutingTable2->MibObject.u4CapwapBaseWtpProfileId)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  FsCawapDiscStatsTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsCawapDiscStatsTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsCawapDiscStatsTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tCapwapFsCawapDiscStatsEntry *pFsCawapDiscStatsEntry1 =
        (tCapwapFsCawapDiscStatsEntry *) pRBElem1;
    tCapwapFsCawapDiscStatsEntry *pFsCawapDiscStatsEntry2 =
        (tCapwapFsCawapDiscStatsEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsCawapDiscStatsEntry1->MibObject.u4CapwapBaseWtpProfileId >
        pFsCawapDiscStatsEntry2->MibObject.u4CapwapBaseWtpProfileId)
    {
        return 1;
    }
    else if (pFsCawapDiscStatsEntry1->MibObject.u4CapwapBaseWtpProfileId <
             pFsCawapDiscStatsEntry2->MibObject.u4CapwapBaseWtpProfileId)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  FsCawapJoinStatsTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsCawapJoinStatsTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsCawapJoinStatsTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tCapwapFsCawapJoinStatsEntry *pFsCawapJoinStatsEntry1 =
        (tCapwapFsCawapJoinStatsEntry *) pRBElem1;
    tCapwapFsCawapJoinStatsEntry *pFsCawapJoinStatsEntry2 =
        (tCapwapFsCawapJoinStatsEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsCawapJoinStatsEntry1->MibObject.u4CapwapBaseWtpProfileId >
        pFsCawapJoinStatsEntry2->MibObject.u4CapwapBaseWtpProfileId)
    {
        return 1;
    }
    else if (pFsCawapJoinStatsEntry1->MibObject.u4CapwapBaseWtpProfileId <
             pFsCawapJoinStatsEntry2->MibObject.u4CapwapBaseWtpProfileId)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  FsCawapConfigStatsTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsCawapConfigStatsTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsCawapConfigStatsTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tCapwapFsCawapConfigStatsEntry *pFsCawapConfigStatsEntry1 =
        (tCapwapFsCawapConfigStatsEntry *) pRBElem1;
    tCapwapFsCawapConfigStatsEntry *pFsCawapConfigStatsEntry2 =
        (tCapwapFsCawapConfigStatsEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsCawapConfigStatsEntry1->MibObject.u4CapwapBaseWtpProfileId >
        pFsCawapConfigStatsEntry2->MibObject.u4CapwapBaseWtpProfileId)
    {
        return 1;
    }
    else if (pFsCawapConfigStatsEntry1->MibObject.u4CapwapBaseWtpProfileId <
             pFsCawapConfigStatsEntry2->MibObject.u4CapwapBaseWtpProfileId)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  FsCawapRunStatsTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsCawapRunStatsTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsCawapRunStatsTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tCapwapFsCawapRunStatsEntry *pFsCawapRunStatsEntry1 =
        (tCapwapFsCawapRunStatsEntry *) pRBElem1;
    tCapwapFsCawapRunStatsEntry *pFsCawapRunStatsEntry2 =
        (tCapwapFsCawapRunStatsEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsCawapRunStatsEntry1->MibObject.u4CapwapBaseWtpProfileId >
        pFsCawapRunStatsEntry2->MibObject.u4CapwapBaseWtpProfileId)
    {
        return 1;
    }
    else if (pFsCawapRunStatsEntry1->MibObject.u4CapwapBaseWtpProfileId <
             pFsCawapRunStatsEntry2->MibObject.u4CapwapBaseWtpProfileId)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  FsCapwapWirelessBindingTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsCapwapWirelessBindingTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsCapwapWirelessBindingTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tCapwapFsCapwapWirelessBindingEntry *pFsCapwapWirelessBindingEntry1 =
        (tCapwapFsCapwapWirelessBindingEntry *) pRBElem1;
    tCapwapFsCapwapWirelessBindingEntry *pFsCapwapWirelessBindingEntry2 =
        (tCapwapFsCapwapWirelessBindingEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsCapwapWirelessBindingEntry1->MibObject.u4CapwapBaseWtpProfileId >
        pFsCapwapWirelessBindingEntry2->MibObject.u4CapwapBaseWtpProfileId)
    {
        return 1;
    }
    else if (pFsCapwapWirelessBindingEntry1->MibObject.
             u4CapwapBaseWtpProfileId <
             pFsCapwapWirelessBindingEntry2->MibObject.u4CapwapBaseWtpProfileId)
    {
        return -1;
    }

    if (pFsCapwapWirelessBindingEntry1->MibObject.
        u4CapwapBaseWirelessBindingRadioId >
        pFsCapwapWirelessBindingEntry2->MibObject.
        u4CapwapBaseWirelessBindingRadioId)
    {
        return 1;
    }
    else if (pFsCapwapWirelessBindingEntry1->MibObject.
             u4CapwapBaseWirelessBindingRadioId <
             pFsCapwapWirelessBindingEntry2->MibObject.
             u4CapwapBaseWirelessBindingRadioId)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  FsCapwapStationWhiteListRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsCapwapStationWhiteList table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsCapwapStationWhiteListRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tCapwapFsCapwapStationWhiteListEntry *pFsCapwapStationWhiteListEntry1 =
        (tCapwapFsCapwapStationWhiteListEntry *) pRBElem1;
    tCapwapFsCapwapStationWhiteListEntry *pFsCapwapStationWhiteListEntry2 =
        (tCapwapFsCapwapStationWhiteListEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsCapwapStationWhiteListEntry1->MibObject.
        u4FsCapwapStationWhiteListId >
        pFsCapwapStationWhiteListEntry2->MibObject.u4FsCapwapStationWhiteListId)
    {
        return 1;
    }
    else if (pFsCapwapStationWhiteListEntry1->MibObject.
             u4FsCapwapStationWhiteListId <
             pFsCapwapStationWhiteListEntry2->MibObject.
             u4FsCapwapStationWhiteListId)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  FsCapwapWtpRebootStatisticsTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsCapwapWtpRebootStatisticsTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsCapwapWtpRebootStatisticsTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tCapwapFsCapwapWtpRebootStatisticsEntry *pFsCapwapWtpRebootStatisticsEntry1
        = (tCapwapFsCapwapWtpRebootStatisticsEntry *) pRBElem1;
    tCapwapFsCapwapWtpRebootStatisticsEntry *pFsCapwapWtpRebootStatisticsEntry2
        = (tCapwapFsCapwapWtpRebootStatisticsEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsCapwapWtpRebootStatisticsEntry1->MibObject.u4CapwapBaseWtpProfileId >
        pFsCapwapWtpRebootStatisticsEntry2->MibObject.u4CapwapBaseWtpProfileId)
    {
        return 1;
    }
    else if (pFsCapwapWtpRebootStatisticsEntry1->MibObject.
             u4CapwapBaseWtpProfileId <
             pFsCapwapWtpRebootStatisticsEntry2->MibObject.
             u4CapwapBaseWtpProfileId)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  FsCapwapWtpRadioStatisticsTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsCapwapWtpRadioStatisticsTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsCapwapWtpRadioStatisticsTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tCapwapFsCapwapWtpRadioStatisticsEntry *pFsCapwapWtpRadioStatisticsEntry1 =
        (tCapwapFsCapwapWtpRadioStatisticsEntry *) pRBElem1;
    tCapwapFsCapwapWtpRadioStatisticsEntry *pFsCapwapWtpRadioStatisticsEntry2 =
        (tCapwapFsCapwapWtpRadioStatisticsEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsCapwapWtpRadioStatisticsEntry1->MibObject.i4RadioIfIndex >
        pFsCapwapWtpRadioStatisticsEntry2->MibObject.i4RadioIfIndex)
    {
        return 1;
    }
    else if (pFsCapwapWtpRadioStatisticsEntry1->MibObject.i4RadioIfIndex <
             pFsCapwapWtpRadioStatisticsEntry2->MibObject.i4RadioIfIndex)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  CapwapBaseAcNameListTableFilterInputs
 Input       :  The Indices
                pCapwapCapwapBaseAcNameListEntry
                pCapwapSetCapwapBaseAcNameListEntry
                pCapwapIsSetCapwapBaseAcNameListEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
CapwapBaseAcNameListTableFilterInputs (tCapwapCapwapBaseAcNameListEntry *
                                       pCapwapCapwapBaseAcNameListEntry,
                                       tCapwapCapwapBaseAcNameListEntry *
                                       pCapwapSetCapwapBaseAcNameListEntry,
                                       tCapwapIsSetCapwapBaseAcNameListEntry *
                                       pCapwapIsSetCapwapBaseAcNameListEntry)
{
    if (pCapwapIsSetCapwapBaseAcNameListEntry->bCapwapBaseAcNameListId ==
        OSIX_TRUE)
    {
        if (pCapwapCapwapBaseAcNameListEntry->MibObject.
            u4CapwapBaseAcNameListId ==
            pCapwapSetCapwapBaseAcNameListEntry->MibObject.
            u4CapwapBaseAcNameListId)
            pCapwapIsSetCapwapBaseAcNameListEntry->bCapwapBaseAcNameListId =
                OSIX_FALSE;
    }
    if (pCapwapIsSetCapwapBaseAcNameListEntry->bCapwapBaseAcNameListName ==
        OSIX_TRUE)
    {
        if ((MEMCMP
             (pCapwapCapwapBaseAcNameListEntry->MibObject.
              au1CapwapBaseAcNameListName,
              pCapwapSetCapwapBaseAcNameListEntry->MibObject.
              au1CapwapBaseAcNameListName,
              pCapwapSetCapwapBaseAcNameListEntry->MibObject.
              i4CapwapBaseAcNameListNameLen) == 0)
            && (pCapwapCapwapBaseAcNameListEntry->MibObject.
                i4CapwapBaseAcNameListNameLen ==
                pCapwapSetCapwapBaseAcNameListEntry->MibObject.
                i4CapwapBaseAcNameListNameLen))
            pCapwapIsSetCapwapBaseAcNameListEntry->bCapwapBaseAcNameListName =
                OSIX_FALSE;
    }
    if (pCapwapIsSetCapwapBaseAcNameListEntry->bCapwapBaseAcNameListPriority ==
        OSIX_TRUE)
    {
        if (pCapwapCapwapBaseAcNameListEntry->MibObject.
            u4CapwapBaseAcNameListPriority ==
            pCapwapSetCapwapBaseAcNameListEntry->MibObject.
            u4CapwapBaseAcNameListPriority)
            pCapwapIsSetCapwapBaseAcNameListEntry->
                bCapwapBaseAcNameListPriority = OSIX_FALSE;
    }
    if (pCapwapIsSetCapwapBaseAcNameListEntry->bCapwapBaseAcNameListRowStatus ==
        OSIX_TRUE)
    {
        if (pCapwapCapwapBaseAcNameListEntry->MibObject.
            i4CapwapBaseAcNameListRowStatus ==
            pCapwapSetCapwapBaseAcNameListEntry->MibObject.
            i4CapwapBaseAcNameListRowStatus)
            pCapwapIsSetCapwapBaseAcNameListEntry->
                bCapwapBaseAcNameListRowStatus = OSIX_FALSE;
    }
    if ((pCapwapIsSetCapwapBaseAcNameListEntry->bCapwapBaseAcNameListId ==
         OSIX_FALSE)
        && (pCapwapIsSetCapwapBaseAcNameListEntry->bCapwapBaseAcNameListName ==
            OSIX_FALSE)
        && (pCapwapIsSetCapwapBaseAcNameListEntry->
            bCapwapBaseAcNameListPriority == OSIX_FALSE)
        && (pCapwapIsSetCapwapBaseAcNameListEntry->
            bCapwapBaseAcNameListRowStatus == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  CapwapBaseMacAclTableFilterInputs
 Input       :  The Indices
                pCapwapCapwapBaseMacAclEntry
                pCapwapSetCapwapBaseMacAclEntry
                pCapwapIsSetCapwapBaseMacAclEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
CapwapBaseMacAclTableFilterInputs (tCapwapCapwapBaseMacAclEntry *
                                   pCapwapCapwapBaseMacAclEntry,
                                   tCapwapCapwapBaseMacAclEntry *
                                   pCapwapSetCapwapBaseMacAclEntry,
                                   tCapwapIsSetCapwapBaseMacAclEntry *
                                   pCapwapIsSetCapwapBaseMacAclEntry)
{
    if (pCapwapIsSetCapwapBaseMacAclEntry->bCapwapBaseMacAclId == OSIX_TRUE)
    {
        if (pCapwapCapwapBaseMacAclEntry->MibObject.u4CapwapBaseMacAclId ==
            pCapwapSetCapwapBaseMacAclEntry->MibObject.u4CapwapBaseMacAclId)
            pCapwapIsSetCapwapBaseMacAclEntry->bCapwapBaseMacAclId = OSIX_FALSE;
    }
    if (pCapwapIsSetCapwapBaseMacAclEntry->bCapwapBaseMacAclStationId ==
        OSIX_TRUE)
    {
        if ((MEMCMP
             (pCapwapCapwapBaseMacAclEntry->MibObject.
              au1CapwapBaseMacAclStationId,
              pCapwapSetCapwapBaseMacAclEntry->MibObject.
              au1CapwapBaseMacAclStationId,
              pCapwapSetCapwapBaseMacAclEntry->MibObject.
              i4CapwapBaseMacAclStationIdLen) == 0)
            && (pCapwapCapwapBaseMacAclEntry->MibObject.
                i4CapwapBaseMacAclStationIdLen ==
                pCapwapSetCapwapBaseMacAclEntry->MibObject.
                i4CapwapBaseMacAclStationIdLen))
            pCapwapIsSetCapwapBaseMacAclEntry->bCapwapBaseMacAclStationId =
                OSIX_FALSE;
    }
    if (pCapwapIsSetCapwapBaseMacAclEntry->bCapwapBaseMacAclRowStatus ==
        OSIX_TRUE)
    {
        if (pCapwapCapwapBaseMacAclEntry->MibObject.
            i4CapwapBaseMacAclRowStatus ==
            pCapwapSetCapwapBaseMacAclEntry->MibObject.
            i4CapwapBaseMacAclRowStatus)
            pCapwapIsSetCapwapBaseMacAclEntry->bCapwapBaseMacAclRowStatus =
                OSIX_FALSE;
    }
    if ((pCapwapIsSetCapwapBaseMacAclEntry->bCapwapBaseMacAclId == OSIX_FALSE)
        && (pCapwapIsSetCapwapBaseMacAclEntry->bCapwapBaseMacAclStationId ==
            OSIX_FALSE)
        && (pCapwapIsSetCapwapBaseMacAclEntry->bCapwapBaseMacAclRowStatus ==
            OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  CapwapBaseWtpProfileTableFilterInputs
 Input       :  The Indices
                pCapwapCapwapBaseWtpProfileEntry
                pCapwapSetCapwapBaseWtpProfileEntry
                pCapwapIsSetCapwapBaseWtpProfileEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
CapwapBaseWtpProfileTableFilterInputs (tCapwapCapwapBaseWtpProfileEntry *
                                       pCapwapCapwapBaseWtpProfileEntry,
                                       tCapwapCapwapBaseWtpProfileEntry *
                                       pCapwapSetCapwapBaseWtpProfileEntry,
                                       tCapwapIsSetCapwapBaseWtpProfileEntry *
                                       pCapwapIsSetCapwapBaseWtpProfileEntry)
{
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->bCapwapBaseWtpProfileId ==
        OSIX_TRUE)
    {
        if (pCapwapCapwapBaseWtpProfileEntry->MibObject.
            u4CapwapBaseWtpProfileId ==
            pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
            u4CapwapBaseWtpProfileId)
            pCapwapIsSetCapwapBaseWtpProfileEntry->bCapwapBaseWtpProfileId =
                OSIX_FALSE;
    }
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->bCapwapBaseWtpProfileName ==
        OSIX_TRUE)
    {
        if ((MEMCMP
             (pCapwapCapwapBaseWtpProfileEntry->MibObject.
              au1CapwapBaseWtpProfileName,
              pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
              au1CapwapBaseWtpProfileName,
              pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
              i4CapwapBaseWtpProfileNameLen) == 0)
            && (pCapwapCapwapBaseWtpProfileEntry->MibObject.
                i4CapwapBaseWtpProfileNameLen ==
                pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                i4CapwapBaseWtpProfileNameLen))
            pCapwapIsSetCapwapBaseWtpProfileEntry->bCapwapBaseWtpProfileName =
                OSIX_FALSE;
    }
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpMacAddress == OSIX_TRUE)
    {
        if ((MEMCMP
             (pCapwapCapwapBaseWtpProfileEntry->MibObject.
              au1CapwapBaseWtpProfileWtpMacAddress,
              pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
              au1CapwapBaseWtpProfileWtpMacAddress,
              pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
              i4CapwapBaseWtpProfileWtpMacAddressLen) == 0)
            && (pCapwapCapwapBaseWtpProfileEntry->MibObject.
                i4CapwapBaseWtpProfileWtpMacAddressLen ==
                pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                i4CapwapBaseWtpProfileWtpMacAddressLen))
            pCapwapIsSetCapwapBaseWtpProfileEntry->
                bCapwapBaseWtpProfileWtpMacAddress = OSIX_FALSE;
    }
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpModelNumber == OSIX_TRUE)
    {
        if ((MEMCMP
             (pCapwapCapwapBaseWtpProfileEntry->MibObject.
              au1CapwapBaseWtpProfileWtpModelNumber,
              pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
              au1CapwapBaseWtpProfileWtpModelNumber,
              pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
              i4CapwapBaseWtpProfileWtpModelNumberLen) == 0)
            && (pCapwapCapwapBaseWtpProfileEntry->MibObject.
                i4CapwapBaseWtpProfileWtpModelNumberLen ==
                pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                i4CapwapBaseWtpProfileWtpModelNumberLen))
            pCapwapIsSetCapwapBaseWtpProfileEntry->
                bCapwapBaseWtpProfileWtpModelNumber = OSIX_FALSE;
    }
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->bCapwapBaseWtpProfileWtpName ==
        OSIX_TRUE)
    {
        if ((MEMCMP
             (pCapwapCapwapBaseWtpProfileEntry->MibObject.
              au1CapwapBaseWtpProfileWtpName,
              pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
              au1CapwapBaseWtpProfileWtpName,
              pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
              i4CapwapBaseWtpProfileWtpNameLen) == 0)
            && (pCapwapCapwapBaseWtpProfileEntry->MibObject.
                i4CapwapBaseWtpProfileWtpNameLen ==
                pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                i4CapwapBaseWtpProfileWtpNameLen))
            pCapwapIsSetCapwapBaseWtpProfileEntry->
                bCapwapBaseWtpProfileWtpName = OSIX_FALSE;
    }
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpLocation == OSIX_TRUE)
    {
        if ((MEMCMP
             (pCapwapCapwapBaseWtpProfileEntry->MibObject.
              au1CapwapBaseWtpProfileWtpLocation,
              pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
              au1CapwapBaseWtpProfileWtpLocation,
              pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
              i4CapwapBaseWtpProfileWtpLocationLen) == 0)
            && (pCapwapCapwapBaseWtpProfileEntry->MibObject.
                i4CapwapBaseWtpProfileWtpLocationLen ==
                pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                i4CapwapBaseWtpProfileWtpLocationLen))
            pCapwapIsSetCapwapBaseWtpProfileEntry->
                bCapwapBaseWtpProfileWtpLocation = OSIX_FALSE;
    }
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpStaticIpEnable == OSIX_TRUE)
    {
        if (pCapwapCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileWtpStaticIpEnable ==
            pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileWtpStaticIpEnable)
            pCapwapIsSetCapwapBaseWtpProfileEntry->
                bCapwapBaseWtpProfileWtpStaticIpEnable = OSIX_FALSE;
    }
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bFsCapwapFsAcTimestampTrigger == OSIX_TRUE)
    {
        if (pCapwapCapwapBaseWtpProfileEntry->MibObject.
            i4FsCapwapFsAcTimestampTrigger ==
            pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
            i4FsCapwapFsAcTimestampTrigger)
            pCapwapIsSetCapwapBaseWtpProfileEntry->
                bFsCapwapFsAcTimestampTrigger = OSIX_FALSE;
    }

    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpStaticIpType == OSIX_TRUE)
    {
        if (pCapwapCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileWtpStaticIpType ==
            pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileWtpStaticIpType)
            pCapwapIsSetCapwapBaseWtpProfileEntry->
                bCapwapBaseWtpProfileWtpStaticIpType = OSIX_FALSE;
    }
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpStaticIpAddress == OSIX_TRUE)
    {
        if ((MEMCMP
             (pCapwapCapwapBaseWtpProfileEntry->MibObject.
              au1CapwapBaseWtpProfileWtpStaticIpAddress,
              pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
              au1CapwapBaseWtpProfileWtpStaticIpAddress,
              pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
              i4CapwapBaseWtpProfileWtpStaticIpAddressLen) == 0)
            && (pCapwapCapwapBaseWtpProfileEntry->MibObject.
                i4CapwapBaseWtpProfileWtpStaticIpAddressLen ==
                pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                i4CapwapBaseWtpProfileWtpStaticIpAddressLen))
            pCapwapIsSetCapwapBaseWtpProfileEntry->
                bCapwapBaseWtpProfileWtpStaticIpAddress = OSIX_FALSE;
    }
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpNetmask == OSIX_TRUE)
    {
        if ((MEMCMP
             (pCapwapCapwapBaseWtpProfileEntry->MibObject.
              au1CapwapBaseWtpProfileWtpNetmask,
              pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
              au1CapwapBaseWtpProfileWtpNetmask,
              pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
              i4CapwapBaseWtpProfileWtpNetmaskLen) == 0)
            && (pCapwapCapwapBaseWtpProfileEntry->MibObject.
                i4CapwapBaseWtpProfileWtpNetmaskLen ==
                pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                i4CapwapBaseWtpProfileWtpNetmaskLen))
            pCapwapIsSetCapwapBaseWtpProfileEntry->
                bCapwapBaseWtpProfileWtpNetmask = OSIX_FALSE;
    }
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpGateway == OSIX_TRUE)
    {
        if ((MEMCMP
             (pCapwapCapwapBaseWtpProfileEntry->MibObject.
              au1CapwapBaseWtpProfileWtpGateway,
              pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
              au1CapwapBaseWtpProfileWtpGateway,
              pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
              i4CapwapBaseWtpProfileWtpGatewayLen) == 0)
            && (pCapwapCapwapBaseWtpProfileEntry->MibObject.
                i4CapwapBaseWtpProfileWtpGatewayLen ==
                pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                i4CapwapBaseWtpProfileWtpGatewayLen))
            pCapwapIsSetCapwapBaseWtpProfileEntry->
                bCapwapBaseWtpProfileWtpGateway = OSIX_FALSE;
    }
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpFallbackEnable == OSIX_TRUE)
    {
        if (pCapwapCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileWtpFallbackEnable ==
            pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileWtpFallbackEnable)
            pCapwapIsSetCapwapBaseWtpProfileEntry->
                bCapwapBaseWtpProfileWtpFallbackEnable = OSIX_FALSE;
    }
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpEchoInterval == OSIX_TRUE)
    {
        if (pCapwapCapwapBaseWtpProfileEntry->MibObject.
            u4CapwapBaseWtpProfileWtpEchoInterval ==
            pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
            u4CapwapBaseWtpProfileWtpEchoInterval)
            pCapwapIsSetCapwapBaseWtpProfileEntry->
                bCapwapBaseWtpProfileWtpEchoInterval = OSIX_FALSE;
    }
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpIdleTimeout == OSIX_TRUE)
    {
        if (pCapwapCapwapBaseWtpProfileEntry->MibObject.
            u4CapwapBaseWtpProfileWtpIdleTimeout ==
            pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
            u4CapwapBaseWtpProfileWtpIdleTimeout)
            pCapwapIsSetCapwapBaseWtpProfileEntry->
                bCapwapBaseWtpProfileWtpIdleTimeout = OSIX_FALSE;
    }
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpMaxDiscoveryInterval == OSIX_TRUE)
    {
        if (pCapwapCapwapBaseWtpProfileEntry->MibObject.
            u4CapwapBaseWtpProfileWtpMaxDiscoveryInterval ==
            pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
            u4CapwapBaseWtpProfileWtpMaxDiscoveryInterval)
            pCapwapIsSetCapwapBaseWtpProfileEntry->
                bCapwapBaseWtpProfileWtpMaxDiscoveryInterval = OSIX_FALSE;
    }
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpReportInterval == OSIX_TRUE)
    {
        if (pCapwapCapwapBaseWtpProfileEntry->MibObject.
            u4CapwapBaseWtpProfileWtpReportInterval ==
            pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
            u4CapwapBaseWtpProfileWtpReportInterval)
            pCapwapIsSetCapwapBaseWtpProfileEntry->
                bCapwapBaseWtpProfileWtpReportInterval = OSIX_FALSE;
    }
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpStatisticsTimer == OSIX_TRUE)
    {
        if (pCapwapCapwapBaseWtpProfileEntry->MibObject.
            u4CapwapBaseWtpProfileWtpStatisticsTimer ==
            pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
            u4CapwapBaseWtpProfileWtpStatisticsTimer)
            pCapwapIsSetCapwapBaseWtpProfileEntry->
                bCapwapBaseWtpProfileWtpStatisticsTimer = OSIX_FALSE;
    }
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpEcnSupport == OSIX_TRUE)
    {
        if (pCapwapCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileWtpEcnSupport ==
            pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileWtpEcnSupport)
            pCapwapIsSetCapwapBaseWtpProfileEntry->
                bCapwapBaseWtpProfileWtpEcnSupport = OSIX_FALSE;
    }
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->bCapwapBaseWtpProfileRowStatus ==
        OSIX_TRUE)
    {
        if (pCapwapCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileRowStatus ==
            pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileRowStatus)
            pCapwapIsSetCapwapBaseWtpProfileEntry->
                bCapwapBaseWtpProfileRowStatus = OSIX_FALSE;
    }
    if ((pCapwapIsSetCapwapBaseWtpProfileEntry->bCapwapBaseWtpProfileId ==
         OSIX_FALSE)
        && (pCapwapIsSetCapwapBaseWtpProfileEntry->bCapwapBaseWtpProfileName ==
            OSIX_FALSE)
        && (pCapwapIsSetCapwapBaseWtpProfileEntry->
            bCapwapBaseWtpProfileWtpMacAddress == OSIX_FALSE)
        && (pCapwapIsSetCapwapBaseWtpProfileEntry->
            bCapwapBaseWtpProfileWtpModelNumber == OSIX_FALSE)
        && (pCapwapIsSetCapwapBaseWtpProfileEntry->
            bCapwapBaseWtpProfileWtpName == OSIX_FALSE)
        && (pCapwapIsSetCapwapBaseWtpProfileEntry->
            bCapwapBaseWtpProfileWtpLocation == OSIX_FALSE)
        && (pCapwapIsSetCapwapBaseWtpProfileEntry->
            bCapwapBaseWtpProfileWtpStaticIpEnable == OSIX_FALSE)
        && (pCapwapIsSetCapwapBaseWtpProfileEntry->
            bFsCapwapFsAcTimestampTrigger == OSIX_FALSE)
        && (pCapwapIsSetCapwapBaseWtpProfileEntry->
            bCapwapBaseWtpProfileWtpStaticIpType == OSIX_FALSE)
        && (pCapwapIsSetCapwapBaseWtpProfileEntry->
            bCapwapBaseWtpProfileWtpStaticIpAddress == OSIX_FALSE)
        && (pCapwapIsSetCapwapBaseWtpProfileEntry->
            bCapwapBaseWtpProfileWtpNetmask == OSIX_FALSE)
        && (pCapwapIsSetCapwapBaseWtpProfileEntry->
            bCapwapBaseWtpProfileWtpGateway == OSIX_FALSE)
        && (pCapwapIsSetCapwapBaseWtpProfileEntry->
            bCapwapBaseWtpProfileWtpFallbackEnable == OSIX_FALSE)
        && (pCapwapIsSetCapwapBaseWtpProfileEntry->
            bCapwapBaseWtpProfileWtpEchoInterval == OSIX_FALSE)
        && (pCapwapIsSetCapwapBaseWtpProfileEntry->
            bCapwapBaseWtpProfileWtpIdleTimeout == OSIX_FALSE)
        && (pCapwapIsSetCapwapBaseWtpProfileEntry->
            bCapwapBaseWtpProfileWtpMaxDiscoveryInterval == OSIX_FALSE)
        && (pCapwapIsSetCapwapBaseWtpProfileEntry->
            bCapwapBaseWtpProfileWtpReportInterval == OSIX_FALSE)
        && (pCapwapIsSetCapwapBaseWtpProfileEntry->
            bCapwapBaseWtpProfileWtpStatisticsTimer == OSIX_FALSE)
        && (pCapwapIsSetCapwapBaseWtpProfileEntry->
            bCapwapBaseWtpProfileWtpEcnSupport == OSIX_FALSE)
        && (pCapwapIsSetCapwapBaseWtpProfileEntry->
            bCapwapBaseWtpProfileRowStatus == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  FsWtpModelTableFilterInputs
 Input       :  The Indices
                pCapwapFsWtpModelEntry
                pCapwapSetFsWtpModelEntry
                pCapwapIsSetFsWtpModelEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
FsWtpModelTableFilterInputs (tCapwapFsWtpModelEntry * pCapwapFsWtpModelEntry,
                             tCapwapFsWtpModelEntry * pCapwapSetFsWtpModelEntry,
                             tCapwapIsSetFsWtpModelEntry *
                             pCapwapIsSetFsWtpModelEntry)
{
    if (pCapwapIsSetFsWtpModelEntry->bFsCapwapWtpModelNumber == OSIX_TRUE)
    {
        if ((MEMCMP
             (pCapwapFsWtpModelEntry->MibObject.au1FsCapwapWtpModelNumber,
              pCapwapSetFsWtpModelEntry->MibObject.au1FsCapwapWtpModelNumber,
              pCapwapSetFsWtpModelEntry->MibObject.
              i4FsCapwapWtpModelNumberLen) == 0)
            && (pCapwapFsWtpModelEntry->MibObject.i4FsCapwapWtpModelNumberLen ==
                pCapwapSetFsWtpModelEntry->MibObject.
                i4FsCapwapWtpModelNumberLen))
            pCapwapIsSetFsWtpModelEntry->bFsCapwapWtpModelNumber = OSIX_FALSE;
    }
    if (pCapwapIsSetFsWtpModelEntry->bFsNoOfRadio == OSIX_TRUE)
    {
        if (pCapwapFsWtpModelEntry->MibObject.u4FsNoOfRadio ==
            pCapwapSetFsWtpModelEntry->MibObject.u4FsNoOfRadio)
            pCapwapIsSetFsWtpModelEntry->bFsNoOfRadio = OSIX_FALSE;
    }
    if (pCapwapIsSetFsWtpModelEntry->bFsCapwapWtpMacType == OSIX_TRUE)
    {
        if (pCapwapFsWtpModelEntry->MibObject.i4FsCapwapWtpMacType ==
            pCapwapSetFsWtpModelEntry->MibObject.i4FsCapwapWtpMacType)
            pCapwapIsSetFsWtpModelEntry->bFsCapwapWtpMacType = OSIX_FALSE;
    }
    if (pCapwapIsSetFsWtpModelEntry->bFsCapwapWtpTunnelMode == OSIX_TRUE)
    {
        if ((MEMCMP (pCapwapFsWtpModelEntry->MibObject.au1FsCapwapWtpTunnelMode,
                     pCapwapSetFsWtpModelEntry->MibObject.
                     au1FsCapwapWtpTunnelMode,
                     pCapwapSetFsWtpModelEntry->MibObject.
                     i4FsCapwapWtpTunnelModeLen) == 0)
            && (pCapwapFsWtpModelEntry->MibObject.i4FsCapwapWtpTunnelModeLen ==
                pCapwapSetFsWtpModelEntry->MibObject.
                i4FsCapwapWtpTunnelModeLen))
            pCapwapIsSetFsWtpModelEntry->bFsCapwapWtpTunnelMode = OSIX_FALSE;
    }
    if (pCapwapIsSetFsWtpModelEntry->bFsCapwapImageName == OSIX_TRUE)
    {
        if ((MEMCMP (pCapwapFsWtpModelEntry->MibObject.au1FsCapwapImageName,
                     pCapwapSetFsWtpModelEntry->MibObject.au1FsCapwapImageName,
                     pCapwapSetFsWtpModelEntry->MibObject.
                     i4FsCapwapImageNameLen) == 0)
            && (pCapwapFsWtpModelEntry->MibObject.i4FsCapwapImageNameLen ==
                pCapwapSetFsWtpModelEntry->MibObject.i4FsCapwapImageNameLen))
            pCapwapIsSetFsWtpModelEntry->bFsCapwapImageName = OSIX_FALSE;
    }
    if (pCapwapIsSetFsWtpModelEntry->bFsCapwapQosProfileName == OSIX_TRUE)
    {
        if ((MEMCMP
             (pCapwapFsWtpModelEntry->MibObject.au1FsCapwapQosProfileName,
              pCapwapSetFsWtpModelEntry->MibObject.au1FsCapwapQosProfileName,
              pCapwapSetFsWtpModelEntry->MibObject.
              i4FsCapwapQosProfileNameLen) == 0)
            && (pCapwapFsWtpModelEntry->MibObject.i4FsCapwapQosProfileNameLen ==
                pCapwapSetFsWtpModelEntry->MibObject.
                i4FsCapwapQosProfileNameLen))
            pCapwapIsSetFsWtpModelEntry->bFsCapwapQosProfileName = OSIX_FALSE;
    }
    if (pCapwapIsSetFsWtpModelEntry->bFsMaxStations == OSIX_TRUE)
    {
        if (pCapwapFsWtpModelEntry->MibObject.i4FsMaxStations ==
            pCapwapSetFsWtpModelEntry->MibObject.i4FsMaxStations)
            pCapwapIsSetFsWtpModelEntry->bFsMaxStations = OSIX_FALSE;
    }
    if (pCapwapIsSetFsWtpModelEntry->bFsWtpModelRowStatus == OSIX_TRUE)
    {
        if (pCapwapFsWtpModelEntry->MibObject.i4FsWtpModelRowStatus ==
            pCapwapSetFsWtpModelEntry->MibObject.i4FsWtpModelRowStatus)
            pCapwapIsSetFsWtpModelEntry->bFsWtpModelRowStatus = OSIX_FALSE;
    }
    if ((pCapwapIsSetFsWtpModelEntry->bFsCapwapWtpModelNumber == OSIX_FALSE)
        && (pCapwapIsSetFsWtpModelEntry->bFsNoOfRadio == OSIX_FALSE)
        && (pCapwapIsSetFsWtpModelEntry->bFsCapwapWtpMacType == OSIX_FALSE)
        && (pCapwapIsSetFsWtpModelEntry->bFsCapwapWtpTunnelMode == OSIX_FALSE)
        && (pCapwapIsSetFsWtpModelEntry->bFsCapwapImageName == OSIX_FALSE)
        && (pCapwapIsSetFsWtpModelEntry->bFsCapwapQosProfileName == OSIX_FALSE)
        && (pCapwapIsSetFsWtpModelEntry->bFsMaxStations == OSIX_FALSE)
        && (pCapwapIsSetFsWtpModelEntry->bFsWtpModelRowStatus == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  FsWtpRadioTableFilterInputs
 Input       :  The Indices
                pCapwapFsWtpRadioEntry
                pCapwapSetFsWtpRadioEntry
                pCapwapIsSetFsWtpRadioEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
FsWtpRadioTableFilterInputs (tCapwapFsWtpRadioEntry * pCapwapFsWtpRadioEntry,
                             tCapwapFsWtpRadioEntry * pCapwapSetFsWtpRadioEntry,
                             tCapwapIsSetFsWtpRadioEntry *
                             pCapwapIsSetFsWtpRadioEntry)
{
    if (pCapwapIsSetFsWtpRadioEntry->bFsCapwapWtpModelNumber == OSIX_TRUE)
    {
        if ((MEMCMP
             (pCapwapFsWtpRadioEntry->MibObject.au1FsCapwapWtpModelNumber,
              pCapwapSetFsWtpRadioEntry->MibObject.au1FsCapwapWtpModelNumber,
              pCapwapSetFsWtpRadioEntry->MibObject.
              i4FsCapwapWtpModelNumberLen) == 0)
            && (pCapwapFsWtpRadioEntry->MibObject.i4FsCapwapWtpModelNumberLen ==
                pCapwapSetFsWtpRadioEntry->MibObject.
                i4FsCapwapWtpModelNumberLen))
            pCapwapIsSetFsWtpRadioEntry->bFsCapwapWtpModelNumber = OSIX_FALSE;
    }
    if (pCapwapIsSetFsWtpRadioEntry->bFsWtpRadioType == OSIX_TRUE)
    {
        if (pCapwapFsWtpRadioEntry->MibObject.u4FsWtpRadioType ==
            pCapwapSetFsWtpRadioEntry->MibObject.u4FsWtpRadioType)
            pCapwapIsSetFsWtpRadioEntry->bFsWtpRadioType = OSIX_FALSE;
    }
    if (pCapwapIsSetFsWtpRadioEntry->bFsRadioAdminStatus == OSIX_TRUE)
    {
        if (pCapwapFsWtpRadioEntry->MibObject.i4FsRadioAdminStatus ==
            pCapwapSetFsWtpRadioEntry->MibObject.i4FsRadioAdminStatus)
            pCapwapIsSetFsWtpRadioEntry->bFsRadioAdminStatus = OSIX_FALSE;
    }
    if (pCapwapIsSetFsWtpRadioEntry->bFsMaxSSIDSupported == OSIX_TRUE)
    {
        if (pCapwapFsWtpRadioEntry->MibObject.i4FsMaxSSIDSupported ==
            pCapwapSetFsWtpRadioEntry->MibObject.i4FsMaxSSIDSupported)
            pCapwapIsSetFsWtpRadioEntry->bFsMaxSSIDSupported = OSIX_FALSE;
    }
    if (pCapwapIsSetFsWtpRadioEntry->bFsWtpRadioRowStatus == OSIX_TRUE)
    {
        if (pCapwapFsWtpRadioEntry->MibObject.i4FsWtpRadioRowStatus ==
            pCapwapSetFsWtpRadioEntry->MibObject.i4FsWtpRadioRowStatus)
            pCapwapIsSetFsWtpRadioEntry->bFsWtpRadioRowStatus = OSIX_FALSE;
    }
    if (pCapwapIsSetFsWtpRadioEntry->bFsNumOfRadio == OSIX_TRUE)
    {
        if (pCapwapFsWtpRadioEntry->MibObject.u4FsNumOfRadio ==
            pCapwapSetFsWtpRadioEntry->MibObject.u4FsNumOfRadio)
            pCapwapIsSetFsWtpRadioEntry->bFsNumOfRadio = OSIX_FALSE;
    }
    if ((pCapwapIsSetFsWtpRadioEntry->bFsCapwapWtpModelNumber == OSIX_FALSE)
        && (pCapwapIsSetFsWtpRadioEntry->bFsWtpRadioType == OSIX_FALSE)
        && (pCapwapIsSetFsWtpRadioEntry->bFsRadioAdminStatus == OSIX_FALSE)
        && (pCapwapIsSetFsWtpRadioEntry->bFsMaxSSIDSupported == OSIX_FALSE)
        && (pCapwapIsSetFsWtpRadioEntry->bFsWtpRadioRowStatus == OSIX_FALSE)
        && (pCapwapIsSetFsWtpRadioEntry->bFsNumOfRadio == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  FsCapwapWhiteListTableFilterInputs
 Input       :  The Indices
                pCapwapFsCapwapWhiteListEntry
                pCapwapSetFsCapwapWhiteListEntry
                pCapwapIsSetFsCapwapWhiteListEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
FsCapwapWhiteListTableFilterInputs (tCapwapFsCapwapWhiteListEntry *
                                    pCapwapFsCapwapWhiteListEntry,
                                    tCapwapFsCapwapWhiteListEntry *
                                    pCapwapSetFsCapwapWhiteListEntry,
                                    tCapwapIsSetFsCapwapWhiteListEntry *
                                    pCapwapIsSetFsCapwapWhiteListEntry)
{
    if (pCapwapIsSetFsCapwapWhiteListEntry->bFsCapwapWhiteListId == OSIX_TRUE)
    {
        if (pCapwapFsCapwapWhiteListEntry->MibObject.u4FsCapwapWhiteListId ==
            pCapwapSetFsCapwapWhiteListEntry->MibObject.u4FsCapwapWhiteListId)
            pCapwapIsSetFsCapwapWhiteListEntry->bFsCapwapWhiteListId =
                OSIX_FALSE;
    }
    if (pCapwapIsSetFsCapwapWhiteListEntry->bFsCapwapWhiteListWtpBaseMac ==
        OSIX_TRUE)
    {
        if ((MEMCMP
             (pCapwapFsCapwapWhiteListEntry->MibObject.
              au1FsCapwapWhiteListWtpBaseMac,
              pCapwapSetFsCapwapWhiteListEntry->MibObject.
              au1FsCapwapWhiteListWtpBaseMac,
              pCapwapSetFsCapwapWhiteListEntry->MibObject.
              i4FsCapwapWhiteListWtpBaseMacLen) == 0)
            && (pCapwapFsCapwapWhiteListEntry->MibObject.
                i4FsCapwapWhiteListWtpBaseMacLen ==
                pCapwapSetFsCapwapWhiteListEntry->MibObject.
                i4FsCapwapWhiteListWtpBaseMacLen))
            pCapwapIsSetFsCapwapWhiteListEntry->bFsCapwapWhiteListWtpBaseMac =
                OSIX_FALSE;
    }
    if (pCapwapIsSetFsCapwapWhiteListEntry->bFsCapwapWhiteListRowStatus ==
        OSIX_TRUE)
    {
        if (pCapwapFsCapwapWhiteListEntry->MibObject.
            i4FsCapwapWhiteListRowStatus ==
            pCapwapSetFsCapwapWhiteListEntry->MibObject.
            i4FsCapwapWhiteListRowStatus)
            pCapwapIsSetFsCapwapWhiteListEntry->bFsCapwapWhiteListRowStatus =
                OSIX_FALSE;
    }
    if ((pCapwapIsSetFsCapwapWhiteListEntry->bFsCapwapWhiteListId == OSIX_FALSE)
        && (pCapwapIsSetFsCapwapWhiteListEntry->bFsCapwapWhiteListWtpBaseMac ==
            OSIX_FALSE)
        && (pCapwapIsSetFsCapwapWhiteListEntry->bFsCapwapWhiteListRowStatus ==
            OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  FsCapwapBlackListFilterInputs
 Input       :  The Indices
                pCapwapFsCapwapBlackListEntry
                pCapwapSetFsCapwapBlackListEntry
                pCapwapIsSetFsCapwapBlackListEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
FsCapwapBlackListFilterInputs (tCapwapFsCapwapBlackListEntry *
                               pCapwapFsCapwapBlackListEntry,
                               tCapwapFsCapwapBlackListEntry *
                               pCapwapSetFsCapwapBlackListEntry,
                               tCapwapIsSetFsCapwapBlackListEntry *
                               pCapwapIsSetFsCapwapBlackListEntry)
{
    if (pCapwapIsSetFsCapwapBlackListEntry->bFsCapwapBlackListId == OSIX_TRUE)
    {
        if (pCapwapFsCapwapBlackListEntry->MibObject.u4FsCapwapBlackListId ==
            pCapwapSetFsCapwapBlackListEntry->MibObject.u4FsCapwapBlackListId)
            pCapwapIsSetFsCapwapBlackListEntry->bFsCapwapBlackListId =
                OSIX_FALSE;
    }
    if (pCapwapIsSetFsCapwapBlackListEntry->bFsCapwapBlackListWtpBaseMac ==
        OSIX_TRUE)
    {
        if ((MEMCMP
             (pCapwapFsCapwapBlackListEntry->MibObject.
              au1FsCapwapBlackListWtpBaseMac,
              pCapwapSetFsCapwapBlackListEntry->MibObject.
              au1FsCapwapBlackListWtpBaseMac,
              pCapwapSetFsCapwapBlackListEntry->MibObject.
              i4FsCapwapBlackListWtpBaseMacLen) == 0)
            && (pCapwapFsCapwapBlackListEntry->MibObject.
                i4FsCapwapBlackListWtpBaseMacLen ==
                pCapwapSetFsCapwapBlackListEntry->MibObject.
                i4FsCapwapBlackListWtpBaseMacLen))
            pCapwapIsSetFsCapwapBlackListEntry->bFsCapwapBlackListWtpBaseMac =
                OSIX_FALSE;
    }
    if (pCapwapIsSetFsCapwapBlackListEntry->bFsCapwapBlackListRowStatus ==
        OSIX_TRUE)
    {
        if (pCapwapFsCapwapBlackListEntry->MibObject.
            i4FsCapwapBlackListRowStatus ==
            pCapwapSetFsCapwapBlackListEntry->MibObject.
            i4FsCapwapBlackListRowStatus)
            pCapwapIsSetFsCapwapBlackListEntry->bFsCapwapBlackListRowStatus =
                OSIX_FALSE;
    }
    if ((pCapwapIsSetFsCapwapBlackListEntry->bFsCapwapBlackListId == OSIX_FALSE)
        && (pCapwapIsSetFsCapwapBlackListEntry->bFsCapwapBlackListWtpBaseMac ==
            OSIX_FALSE)
        && (pCapwapIsSetFsCapwapBlackListEntry->bFsCapwapBlackListRowStatus ==
            OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  FsCapwapWtpConfigTableFilterInputs
 Input       :  The Indices
                pCapwapFsCapwapWtpConfigEntry
                pCapwapSetFsCapwapWtpConfigEntry
                pCapwapIsSetFsCapwapWtpConfigEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
FsCapwapWtpConfigTableFilterInputs (tCapwapFsCapwapWtpConfigEntry *
                                    pCapwapFsCapwapWtpConfigEntry,
                                    tCapwapFsCapwapWtpConfigEntry *
                                    pCapwapSetFsCapwapWtpConfigEntry,
                                    tCapwapIsSetFsCapwapWtpConfigEntry *
                                    pCapwapIsSetFsCapwapWtpConfigEntry)
{
    if (pCapwapIsSetFsCapwapWtpConfigEntry->bFsCapwapClearApStats == OSIX_TRUE)
    {
        return OSIX_TRUE;
    }
    if (pCapwapIsSetFsCapwapWtpConfigEntry->bFsCapwapWtpReset == OSIX_TRUE)
    {
        if (pCapwapFsCapwapWtpConfigEntry->MibObject.i4FsCapwapWtpReset ==
            pCapwapSetFsCapwapWtpConfigEntry->MibObject.i4FsCapwapWtpReset)
            pCapwapIsSetFsCapwapWtpConfigEntry->bFsCapwapWtpReset = OSIX_FALSE;
    }
    if (pCapwapIsSetFsCapwapWtpConfigEntry->bFsCapwapClearConfig == OSIX_TRUE)
    {
        if (pCapwapFsCapwapWtpConfigEntry->MibObject.i4FsCapwapClearConfig ==
            pCapwapSetFsCapwapWtpConfigEntry->MibObject.i4FsCapwapClearConfig)
            pCapwapIsSetFsCapwapWtpConfigEntry->bFsCapwapClearConfig =
                OSIX_FALSE;
    }
    if (pCapwapIsSetFsCapwapWtpConfigEntry->bFsWtpDiscoveryType == OSIX_TRUE)
    {
        if (pCapwapFsCapwapWtpConfigEntry->MibObject.i4FsWtpDiscoveryType ==
            pCapwapSetFsCapwapWtpConfigEntry->MibObject.i4FsWtpDiscoveryType)
            pCapwapIsSetFsCapwapWtpConfigEntry->bFsWtpDiscoveryType =
                OSIX_FALSE;
    }
    if (pCapwapIsSetFsCapwapWtpConfigEntry->bFsWlcStaticIpAddress == OSIX_TRUE)
    {
        if (pCapwapFsCapwapWtpConfigEntry->MibObject.u4FsWlcStaticIpAddress ==
            pCapwapSetFsCapwapWtpConfigEntry->MibObject.u4FsWlcStaticIpAddress)
            pCapwapIsSetFsCapwapWtpConfigEntry->bFsWlcStaticIpAddress =
                OSIX_FALSE;
    }

    if (pCapwapIsSetFsCapwapWtpConfigEntry->bFsWtpCountryString == OSIX_TRUE)
    {
        if ((MEMCMP
             (pCapwapFsCapwapWtpConfigEntry->MibObject.au1FsWtpCountryString,
              pCapwapSetFsCapwapWtpConfigEntry->MibObject.au1FsWtpCountryString,
              pCapwapSetFsCapwapWtpConfigEntry->MibObject.
              i4FsWtpCountryStringLen) == 0)
            && (pCapwapFsCapwapWtpConfigEntry->MibObject.
                i4FsWtpCountryStringLen ==
                pCapwapSetFsCapwapWtpConfigEntry->MibObject.
                i4FsWtpCountryStringLen))
            pCapwapIsSetFsCapwapWtpConfigEntry->bFsWtpCountryString =
                OSIX_FALSE;
    }
    if (pCapwapIsSetFsCapwapWtpConfigEntry->bFsWtpCrashDumpFileName ==
        OSIX_TRUE)
    {
        if ((MEMCMP
             (pCapwapFsCapwapWtpConfigEntry->MibObject.
              au1FsWtpCrashDumpFileName,
              pCapwapSetFsCapwapWtpConfigEntry->MibObject.
              au1FsWtpCrashDumpFileName,
              pCapwapSetFsCapwapWtpConfigEntry->MibObject.
              i4FsWtpCrashDumpFileNameLen) == 0)
            && (pCapwapFsCapwapWtpConfigEntry->MibObject.
                i4FsWtpCrashDumpFileNameLen ==
                pCapwapSetFsCapwapWtpConfigEntry->MibObject.
                i4FsWtpCrashDumpFileNameLen))
        {
            pCapwapIsSetFsCapwapWtpConfigEntry->bFsWtpCrashDumpFileName =
                OSIX_FALSE;
        }
    }
    if (pCapwapIsSetFsCapwapWtpConfigEntry->bFsWtpMemoryDumpFileName ==
        OSIX_TRUE)
    {
        if ((MEMCMP
             (pCapwapFsCapwapWtpConfigEntry->MibObject.
              au1FsWtpMemoryDumpFileName,
              pCapwapSetFsCapwapWtpConfigEntry->MibObject.
              au1FsWtpMemoryDumpFileName,
              pCapwapSetFsCapwapWtpConfigEntry->MibObject.
              i4FsWtpMemoryDumpFileNameLen) == 0)
            && (pCapwapFsCapwapWtpConfigEntry->MibObject.
                i4FsWtpMemoryDumpFileNameLen ==
                pCapwapSetFsCapwapWtpConfigEntry->MibObject.
                i4FsWtpMemoryDumpFileNameLen))
            pCapwapIsSetFsCapwapWtpConfigEntry->bFsWtpMemoryDumpFileName =
                OSIX_FALSE;
    }
    if (pCapwapIsSetFsCapwapWtpConfigEntry->bFsWtpDeleteOperation == OSIX_TRUE)
    {
        if (pCapwapFsCapwapWtpConfigEntry->MibObject.u4FsWtpDeleteOperation ==
            pCapwapSetFsCapwapWtpConfigEntry->MibObject.u4FsWtpDeleteOperation)
            pCapwapIsSetFsCapwapWtpConfigEntry->bFsWtpDeleteOperation =
                OSIX_FALSE;

        if (pCapwapIsSetFsCapwapWtpConfigEntry->bFsWtpCrashDumpFileName ==
            OSIX_FALSE
            && pCapwapSetFsCapwapWtpConfigEntry->MibObject.
            u4FsWtpDeleteOperation != NO_CRASHMEMORY)
        {
            pCapwapIsSetFsCapwapWtpConfigEntry->bFsWtpCrashDumpFileName =
                OSIX_TRUE;
        }
    }
    if (pCapwapIsSetFsCapwapWtpConfigEntry->bFsCapwapWtpConfigRowStatus ==
        OSIX_TRUE)
    {
        if (pCapwapFsCapwapWtpConfigEntry->MibObject.
            i4FsCapwapWtpConfigRowStatus ==
            pCapwapSetFsCapwapWtpConfigEntry->MibObject.
            i4FsCapwapWtpConfigRowStatus)
            pCapwapIsSetFsCapwapWtpConfigEntry->bFsCapwapWtpConfigRowStatus =
                OSIX_FALSE;
    }
    if (pCapwapIsSetFsCapwapWtpConfigEntry->bCapwapBaseWtpProfileId ==
        OSIX_TRUE)
    {
        if (pCapwapFsCapwapWtpConfigEntry->MibObject.u4CapwapBaseWtpProfileId ==
            pCapwapSetFsCapwapWtpConfigEntry->MibObject.
            u4CapwapBaseWtpProfileId)
            pCapwapIsSetFsCapwapWtpConfigEntry->bCapwapBaseWtpProfileId =
                OSIX_FALSE;
    }
    if ((pCapwapIsSetFsCapwapWtpConfigEntry->bFsCapwapWtpReset == OSIX_FALSE)
        && (pCapwapIsSetFsCapwapWtpConfigEntry->bFsCapwapClearConfig ==
            OSIX_FALSE)
        && (pCapwapIsSetFsCapwapWtpConfigEntry->bFsWtpDiscoveryType ==
            OSIX_FALSE)
        && (pCapwapIsSetFsCapwapWtpConfigEntry->bFsWtpCountryString ==
            OSIX_FALSE)
        && (pCapwapIsSetFsCapwapWtpConfigEntry->bFsCapwapWtpConfigRowStatus ==
            OSIX_FALSE)
        && (pCapwapIsSetFsCapwapWtpConfigEntry->bCapwapBaseWtpProfileId ==
            OSIX_FALSE)
        && (pCapwapIsSetFsCapwapWtpConfigEntry->bFsWtpCrashDumpFileName ==
            OSIX_FALSE)
        && (pCapwapIsSetFsCapwapWtpConfigEntry->bFsWlcStaticIpAddress ==
            OSIX_FALSE)
        && (pCapwapIsSetFsCapwapWtpConfigEntry->bFsWtpMemoryDumpFileName ==
            OSIX_FALSE)
        && (pCapwapIsSetFsCapwapWtpConfigEntry->bFsWtpDeleteOperation ==
            OSIX_FALSE)
        && (pCapwapIsSetFsCapwapWtpConfigEntry->bFsWlcStaticIpAddress ==
            OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  FsCapwapLinkEncryptionTableFilterInputs
 Input       :  The Indices
                pCapwapFsCapwapLinkEncryptionEntry
                pCapwapSetFsCapwapLinkEncryptionEntry
                pCapwapIsSetFsCapwapLinkEncryptionEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
FsCapwapLinkEncryptionTableFilterInputs (tCapwapFsCapwapLinkEncryptionEntry *
                                         pCapwapFsCapwapLinkEncryptionEntry,
                                         tCapwapFsCapwapLinkEncryptionEntry *
                                         pCapwapSetFsCapwapLinkEncryptionEntry,
                                         tCapwapIsSetFsCapwapLinkEncryptionEntry
                                         *
                                         pCapwapIsSetFsCapwapLinkEncryptionEntry)
{
    if (pCapwapIsSetFsCapwapLinkEncryptionEntry->bFsCapwapEncryptChannel ==
        OSIX_TRUE)
    {
        if (pCapwapFsCapwapLinkEncryptionEntry->MibObject.
            i4FsCapwapEncryptChannel ==
            pCapwapSetFsCapwapLinkEncryptionEntry->MibObject.
            i4FsCapwapEncryptChannel)
            pCapwapIsSetFsCapwapLinkEncryptionEntry->bFsCapwapEncryptChannel =
                OSIX_FALSE;
    }
    if (pCapwapIsSetFsCapwapLinkEncryptionEntry->
        bFsCapwapEncryptChannelStatus == OSIX_TRUE)
    {
        if (pCapwapFsCapwapLinkEncryptionEntry->MibObject.
            i4FsCapwapEncryptChannelStatus ==
            pCapwapSetFsCapwapLinkEncryptionEntry->MibObject.
            i4FsCapwapEncryptChannelStatus)
            pCapwapIsSetFsCapwapLinkEncryptionEntry->
                bFsCapwapEncryptChannelStatus = OSIX_FALSE;
    }
    if (pCapwapIsSetFsCapwapLinkEncryptionEntry->
        bFsCapwapEncryptChannelRowStatus == OSIX_TRUE)
    {
        if (pCapwapFsCapwapLinkEncryptionEntry->MibObject.
            i4FsCapwapEncryptChannelRowStatus ==
            pCapwapSetFsCapwapLinkEncryptionEntry->MibObject.
            i4FsCapwapEncryptChannelRowStatus)
            pCapwapIsSetFsCapwapLinkEncryptionEntry->
                bFsCapwapEncryptChannelRowStatus = OSIX_FALSE;
    }
    if (pCapwapIsSetFsCapwapLinkEncryptionEntry->bCapwapBaseWtpProfileId ==
        OSIX_TRUE)
    {
        if (pCapwapFsCapwapLinkEncryptionEntry->MibObject.
            u4CapwapBaseWtpProfileId ==
            pCapwapSetFsCapwapLinkEncryptionEntry->MibObject.
            u4CapwapBaseWtpProfileId)
            pCapwapIsSetFsCapwapLinkEncryptionEntry->bCapwapBaseWtpProfileId =
                OSIX_FALSE;
    }
    if ((pCapwapIsSetFsCapwapLinkEncryptionEntry->bFsCapwapEncryptChannel ==
         OSIX_FALSE)
        && (pCapwapIsSetFsCapwapLinkEncryptionEntry->
            bFsCapwapEncryptChannelStatus == OSIX_FALSE)
        && (pCapwapIsSetFsCapwapLinkEncryptionEntry->
            bFsCapwapEncryptChannelRowStatus == OSIX_FALSE)
        && (pCapwapIsSetFsCapwapLinkEncryptionEntry->bCapwapBaseWtpProfileId ==
            OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  FsCawapDefaultWtpProfileTableFilterInputs
 Input       :  The Indices
                pCapwapFsCapwapDefaultWtpProfileEntry
                pCapwapSetFsCapwapDefaultWtpProfileEntry
                pCapwapIsSetFsCapwapDefaultWtpProfileEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
FsCawapDefaultWtpProfileTableFilterInputs (tCapwapFsCapwapDefaultWtpProfileEntry
                                           *
                                           pCapwapFsCapwapDefaultWtpProfileEntry,
                                           tCapwapFsCapwapDefaultWtpProfileEntry
                                           *
                                           pCapwapSetFsCapwapDefaultWtpProfileEntry,
                                           tCapwapIsSetFsCapwapDefaultWtpProfileEntry
                                           *
                                           pCapwapIsSetFsCapwapDefaultWtpProfileEntry)
{
    if (pCapwapIsSetFsCapwapDefaultWtpProfileEntry->
        bFsCapwapDefaultWtpProfileModelNumber == OSIX_TRUE)
    {
        if ((MEMCMP
             (pCapwapFsCapwapDefaultWtpProfileEntry->MibObject.
              au1FsCapwapDefaultWtpProfileModelNumber,
              pCapwapSetFsCapwapDefaultWtpProfileEntry->MibObject.
              au1FsCapwapDefaultWtpProfileModelNumber,
              pCapwapSetFsCapwapDefaultWtpProfileEntry->MibObject.
              i4FsCapwapDefaultWtpProfileModelNumberLen) == 0)
            && (pCapwapFsCapwapDefaultWtpProfileEntry->MibObject.
                i4FsCapwapDefaultWtpProfileModelNumberLen ==
                pCapwapSetFsCapwapDefaultWtpProfileEntry->MibObject.
                i4FsCapwapDefaultWtpProfileModelNumberLen))
            pCapwapIsSetFsCapwapDefaultWtpProfileEntry->
                bFsCapwapDefaultWtpProfileModelNumber = OSIX_FALSE;
    }
    if (pCapwapIsSetFsCapwapDefaultWtpProfileEntry->
        bFsCapwapDefaultWtpProfileWtpFallbackEnable == OSIX_TRUE)
    {
        if (pCapwapFsCapwapDefaultWtpProfileEntry->MibObject.
            i4FsCapwapDefaultWtpProfileWtpFallbackEnable ==
            pCapwapSetFsCapwapDefaultWtpProfileEntry->MibObject.
            i4FsCapwapDefaultWtpProfileWtpFallbackEnable)
            pCapwapIsSetFsCapwapDefaultWtpProfileEntry->
                bFsCapwapDefaultWtpProfileWtpFallbackEnable = OSIX_FALSE;
    }
    if (pCapwapIsSetFsCapwapDefaultWtpProfileEntry->
        bFsCapwapDefaultWtpProfileWtpEchoInterval == OSIX_TRUE)
    {
        if (pCapwapFsCapwapDefaultWtpProfileEntry->MibObject.
            u4FsCapwapDefaultWtpProfileWtpEchoInterval ==
            pCapwapSetFsCapwapDefaultWtpProfileEntry->MibObject.
            u4FsCapwapDefaultWtpProfileWtpEchoInterval)
            pCapwapIsSetFsCapwapDefaultWtpProfileEntry->
                bFsCapwapDefaultWtpProfileWtpEchoInterval = OSIX_FALSE;
    }
    if (pCapwapIsSetFsCapwapDefaultWtpProfileEntry->
        bFsCapwapDefaultWtpProfileWtpIdleTimeout == OSIX_TRUE)
    {
        if (pCapwapFsCapwapDefaultWtpProfileEntry->MibObject.
            u4FsCapwapDefaultWtpProfileWtpIdleTimeout ==
            pCapwapSetFsCapwapDefaultWtpProfileEntry->MibObject.
            u4FsCapwapDefaultWtpProfileWtpIdleTimeout)
            pCapwapIsSetFsCapwapDefaultWtpProfileEntry->
                bFsCapwapDefaultWtpProfileWtpIdleTimeout = OSIX_FALSE;
    }
    if (pCapwapIsSetFsCapwapDefaultWtpProfileEntry->
        bFsCapwapDefaultWtpProfileWtpMaxDiscoveryInterval == OSIX_TRUE)
    {
        if (pCapwapFsCapwapDefaultWtpProfileEntry->MibObject.
            u4FsCapwapDefaultWtpProfileWtpMaxDiscoveryInterval ==
            pCapwapSetFsCapwapDefaultWtpProfileEntry->MibObject.
            u4FsCapwapDefaultWtpProfileWtpMaxDiscoveryInterval)
            pCapwapIsSetFsCapwapDefaultWtpProfileEntry->
                bFsCapwapDefaultWtpProfileWtpMaxDiscoveryInterval = OSIX_FALSE;
    }
    if (pCapwapIsSetFsCapwapDefaultWtpProfileEntry->
        bFsCapwapDefaultWtpProfileWtpReportInterval == OSIX_TRUE)
    {
        if (pCapwapFsCapwapDefaultWtpProfileEntry->MibObject.
            u4FsCapwapDefaultWtpProfileWtpReportInterval ==
            pCapwapSetFsCapwapDefaultWtpProfileEntry->MibObject.
            u4FsCapwapDefaultWtpProfileWtpReportInterval)
            pCapwapIsSetFsCapwapDefaultWtpProfileEntry->
                bFsCapwapDefaultWtpProfileWtpReportInterval = OSIX_FALSE;
    }
    if (pCapwapIsSetFsCapwapDefaultWtpProfileEntry->
        bFsCapwapDefaultWtpProfileWtpStatisticsTimer == OSIX_TRUE)
    {
        if (pCapwapFsCapwapDefaultWtpProfileEntry->MibObject.
            u4FsCapwapDefaultWtpProfileWtpStatisticsTimer ==
            pCapwapSetFsCapwapDefaultWtpProfileEntry->MibObject.
            u4FsCapwapDefaultWtpProfileWtpStatisticsTimer)
            pCapwapIsSetFsCapwapDefaultWtpProfileEntry->
                bFsCapwapDefaultWtpProfileWtpStatisticsTimer = OSIX_FALSE;
    }
    if (pCapwapIsSetFsCapwapDefaultWtpProfileEntry->
        bFsCapwapDefaultWtpProfileWtpEcnSupport == OSIX_TRUE)
    {
        if (pCapwapFsCapwapDefaultWtpProfileEntry->MibObject.
            i4FsCapwapDefaultWtpProfileWtpEcnSupport ==
            pCapwapSetFsCapwapDefaultWtpProfileEntry->MibObject.
            i4FsCapwapDefaultWtpProfileWtpEcnSupport)
            pCapwapIsSetFsCapwapDefaultWtpProfileEntry->
                bFsCapwapDefaultWtpProfileWtpEcnSupport = OSIX_FALSE;
    }
    if (pCapwapIsSetFsCapwapDefaultWtpProfileEntry->
        bFsCapwapDefaultWtpProfileRowStatus == OSIX_TRUE)
    {
        if (pCapwapFsCapwapDefaultWtpProfileEntry->MibObject.
            i4FsCapwapDefaultWtpProfileRowStatus ==
            pCapwapSetFsCapwapDefaultWtpProfileEntry->MibObject.
            i4FsCapwapDefaultWtpProfileRowStatus)
            pCapwapIsSetFsCapwapDefaultWtpProfileEntry->
                bFsCapwapDefaultWtpProfileRowStatus = OSIX_FALSE;
    }
    if ((pCapwapIsSetFsCapwapDefaultWtpProfileEntry->
         bFsCapwapDefaultWtpProfileModelNumber == OSIX_FALSE)
        && (pCapwapIsSetFsCapwapDefaultWtpProfileEntry->
            bFsCapwapDefaultWtpProfileWtpFallbackEnable == OSIX_FALSE)
        && (pCapwapIsSetFsCapwapDefaultWtpProfileEntry->
            bFsCapwapDefaultWtpProfileWtpEchoInterval == OSIX_FALSE)
        && (pCapwapIsSetFsCapwapDefaultWtpProfileEntry->
            bFsCapwapDefaultWtpProfileWtpIdleTimeout == OSIX_FALSE)
        && (pCapwapIsSetFsCapwapDefaultWtpProfileEntry->
            bFsCapwapDefaultWtpProfileWtpMaxDiscoveryInterval == OSIX_FALSE)
        && (pCapwapIsSetFsCapwapDefaultWtpProfileEntry->
            bFsCapwapDefaultWtpProfileWtpReportInterval == OSIX_FALSE)
        && (pCapwapIsSetFsCapwapDefaultWtpProfileEntry->
            bFsCapwapDefaultWtpProfileWtpStatisticsTimer == OSIX_FALSE)
        && (pCapwapIsSetFsCapwapDefaultWtpProfileEntry->
            bFsCapwapDefaultWtpProfileWtpEcnSupport == OSIX_FALSE)
        && (pCapwapIsSetFsCapwapDefaultWtpProfileEntry->
            bFsCapwapDefaultWtpProfileRowStatus == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  FsCapwapDnsProfileTableFilterInputs
 Input       :  The Indices
                pCapwapFsCapwapDnsProfileEntry
                pCapwapSetFsCapwapDnsProfileEntry
                pCapwapIsSetFsCapwapDnsProfileEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
FsCapwapDnsProfileTableFilterInputs (tCapwapFsCapwapDnsProfileEntry *
                                     pCapwapFsCapwapDnsProfileEntry,
                                     tCapwapFsCapwapDnsProfileEntry *
                                     pCapwapSetFsCapwapDnsProfileEntry,
                                     tCapwapIsSetFsCapwapDnsProfileEntry *
                                     pCapwapIsSetFsCapwapDnsProfileEntry)
{
    if (pCapwapIsSetFsCapwapDnsProfileEntry->bFsCapwapDnsServerIp == OSIX_TRUE)
    {
        if ((MEMCMP
             (pCapwapFsCapwapDnsProfileEntry->MibObject.au1FsCapwapDnsServerIp,
              pCapwapSetFsCapwapDnsProfileEntry->MibObject.
              au1FsCapwapDnsServerIp,
              pCapwapSetFsCapwapDnsProfileEntry->MibObject.
              i4FsCapwapDnsServerIpLen) == 0)
            && (pCapwapFsCapwapDnsProfileEntry->MibObject.
                i4FsCapwapDnsServerIpLen ==
                pCapwapSetFsCapwapDnsProfileEntry->MibObject.
                i4FsCapwapDnsServerIpLen))
            pCapwapIsSetFsCapwapDnsProfileEntry->bFsCapwapDnsServerIp =
                OSIX_FALSE;
    }
    if (pCapwapIsSetFsCapwapDnsProfileEntry->bFsCapwapDnsDomainName ==
        OSIX_TRUE)
    {
        if ((MEMCMP
             (pCapwapFsCapwapDnsProfileEntry->MibObject.
              au1FsCapwapDnsDomainName,
              pCapwapSetFsCapwapDnsProfileEntry->MibObject.
              au1FsCapwapDnsDomainName,
              pCapwapSetFsCapwapDnsProfileEntry->MibObject.
              i4FsCapwapDnsDomainNameLen) == 0)
            && (pCapwapFsCapwapDnsProfileEntry->MibObject.
                i4FsCapwapDnsDomainNameLen ==
                pCapwapSetFsCapwapDnsProfileEntry->MibObject.
                i4FsCapwapDnsDomainNameLen))
            pCapwapIsSetFsCapwapDnsProfileEntry->bFsCapwapDnsDomainName =
                OSIX_FALSE;
    }
    if (pCapwapIsSetFsCapwapDnsProfileEntry->bFsCapwapDnsProfileRowStatus ==
        OSIX_TRUE)
    {
        if (pCapwapFsCapwapDnsProfileEntry->MibObject.
            i4FsCapwapDnsProfileRowStatus ==
            pCapwapSetFsCapwapDnsProfileEntry->MibObject.
            i4FsCapwapDnsProfileRowStatus)
            pCapwapIsSetFsCapwapDnsProfileEntry->bFsCapwapDnsProfileRowStatus =
                OSIX_FALSE;
    }
    if (pCapwapIsSetFsCapwapDnsProfileEntry->bCapwapBaseWtpProfileId ==
        OSIX_TRUE)
    {
        if (pCapwapFsCapwapDnsProfileEntry->MibObject.
            u4CapwapBaseWtpProfileId ==
            pCapwapSetFsCapwapDnsProfileEntry->MibObject.
            u4CapwapBaseWtpProfileId)
            pCapwapIsSetFsCapwapDnsProfileEntry->bCapwapBaseWtpProfileId =
                OSIX_FALSE;
    }
    if ((pCapwapIsSetFsCapwapDnsProfileEntry->bFsCapwapDnsServerIp ==
         OSIX_FALSE)
        && (pCapwapIsSetFsCapwapDnsProfileEntry->bFsCapwapDnsDomainName ==
            OSIX_FALSE)
        && (pCapwapIsSetFsCapwapDnsProfileEntry->bFsCapwapDnsProfileRowStatus ==
            OSIX_FALSE)
        && (pCapwapIsSetFsCapwapDnsProfileEntry->bCapwapBaseWtpProfileId ==
            OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  FsWtpNativeVlanIdTableFilterInputs
 Input       :  The Indices
                pCapwapFsWtpNativeVlanIdTable
                pCapwapSetFsWtpNativeVlanIdTable
                pCapwapIsSetFsWtpNativeVlanIdTable
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
FsWtpNativeVlanIdTableFilterInputs (tCapwapFsWtpNativeVlanIdTable *
                                    pCapwapFsWtpNativeVlanIdTable,
                                    tCapwapFsWtpNativeVlanIdTable *
                                    pCapwapSetFsWtpNativeVlanIdTable,
                                    tCapwapIsSetFsWtpNativeVlanIdTable *
                                    pCapwapIsSetFsWtpNativeVlanIdTable)
{
    if (pCapwapIsSetFsWtpNativeVlanIdTable->bFsWtpNativeVlanId == OSIX_TRUE)
    {
        if (pCapwapFsWtpNativeVlanIdTable->MibObject.i4FsWtpNativeVlanId ==
            pCapwapSetFsWtpNativeVlanIdTable->MibObject.i4FsWtpNativeVlanId)
            pCapwapIsSetFsWtpNativeVlanIdTable->bFsWtpNativeVlanId = OSIX_FALSE;
    }
    if (pCapwapIsSetFsWtpNativeVlanIdTable->bFsWtpNativeVlanIdRowStatus ==
        OSIX_TRUE)
    {
        if (pCapwapFsWtpNativeVlanIdTable->MibObject.
            i4FsWtpNativeVlanIdRowStatus ==
            pCapwapSetFsWtpNativeVlanIdTable->MibObject.
            i4FsWtpNativeVlanIdRowStatus)
            pCapwapIsSetFsWtpNativeVlanIdTable->bFsWtpNativeVlanIdRowStatus =
                OSIX_FALSE;
    }
    if (pCapwapIsSetFsWtpNativeVlanIdTable->bCapwapBaseWtpProfileId ==
        OSIX_TRUE)
    {
        if (pCapwapFsWtpNativeVlanIdTable->MibObject.u4CapwapBaseWtpProfileId ==
            pCapwapSetFsWtpNativeVlanIdTable->MibObject.
            u4CapwapBaseWtpProfileId)
            pCapwapIsSetFsWtpNativeVlanIdTable->bCapwapBaseWtpProfileId =
                OSIX_FALSE;
    }
    if ((pCapwapIsSetFsWtpNativeVlanIdTable->bFsWtpNativeVlanId == OSIX_FALSE)
        && (pCapwapIsSetFsWtpNativeVlanIdTable->bFsWtpNativeVlanIdRowStatus ==
            OSIX_FALSE)
        && (pCapwapIsSetFsWtpNativeVlanIdTable->bCapwapBaseWtpProfileId ==
            OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  FsWtpLocalRoutingTableFilterInputs
 Input       :  The Indices
                pCapwapFsWtpLocalRoutingTable
                pCapwapSetFsWtpLocalRoutingTable
                pCapwapIsSetFsWtpLocalRoutingTable
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
FsWtpLocalRoutingTableFilterInputs (tCapwapFsWtpLocalRoutingTable *
                                    pCapwapFsWtpLocalRoutingTable,
                                    tCapwapFsWtpLocalRoutingTable *
                                    pCapwapSetFsWtpLocalRoutingTable,
                                    tCapwapIsSetFsWtpLocalRoutingTable *
                                    pCapwapIsSetFsWtpLocalRoutingTable)
{
    if (pCapwapIsSetFsWtpLocalRoutingTable->bFsWtpLocalRouting == OSIX_TRUE)
    {
        if (pCapwapFsWtpLocalRoutingTable->MibObject.i4FsWtpLocalRouting ==
            pCapwapSetFsWtpLocalRoutingTable->MibObject.i4FsWtpLocalRouting)
            pCapwapIsSetFsWtpLocalRoutingTable->bFsWtpLocalRouting = OSIX_FALSE;
    }
    if (pCapwapIsSetFsWtpLocalRoutingTable->bFsWtpLocalRoutingRowStatus ==
        OSIX_TRUE)
    {
        if (pCapwapFsWtpLocalRoutingTable->MibObject.
            i4FsWtpLocalRoutingRowStatus ==
            pCapwapSetFsWtpLocalRoutingTable->MibObject.
            i4FsWtpLocalRoutingRowStatus)
            pCapwapIsSetFsWtpLocalRoutingTable->bFsWtpLocalRoutingRowStatus =
                OSIX_FALSE;
    }
    if (pCapwapIsSetFsWtpLocalRoutingTable->bCapwapBaseWtpProfileId ==
        OSIX_TRUE)
    {
        if (pCapwapFsWtpLocalRoutingTable->MibObject.u4CapwapBaseWtpProfileId ==
            pCapwapSetFsWtpLocalRoutingTable->MibObject.
            u4CapwapBaseWtpProfileId)
            pCapwapIsSetFsWtpLocalRoutingTable->bCapwapBaseWtpProfileId =
                OSIX_FALSE;
    }
    if ((pCapwapIsSetFsWtpLocalRoutingTable->bFsWtpLocalRouting == OSIX_FALSE)
        && (pCapwapIsSetFsWtpLocalRoutingTable->bFsWtpLocalRoutingRowStatus ==
            OSIX_FALSE)
        && (pCapwapIsSetFsWtpLocalRoutingTable->bCapwapBaseWtpProfileId ==
            OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  FsCawapDiscStatsTableFilterInputs
 Input       :  The Indices
                pCapwapFsCawapDiscStatsEntry
                pCapwapSetFsCawapDiscStatsEntry
                pCapwapIsSetFsCawapDiscStatsEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
FsCawapDiscStatsTableFilterInputs (tCapwapFsCawapDiscStatsEntry *
                                   pCapwapFsCawapDiscStatsEntry,
                                   tCapwapFsCawapDiscStatsEntry *
                                   pCapwapSetFsCawapDiscStatsEntry,
                                   tCapwapIsSetFsCawapDiscStatsEntry *
                                   pCapwapIsSetFsCawapDiscStatsEntry)
{
    if (pCapwapIsSetFsCawapDiscStatsEntry->bFsCapwapDiscStatsRowStatus ==
        OSIX_TRUE)
    {
        if (pCapwapFsCawapDiscStatsEntry->MibObject.
            i4FsCapwapDiscStatsRowStatus ==
            pCapwapSetFsCawapDiscStatsEntry->MibObject.
            i4FsCapwapDiscStatsRowStatus)
            pCapwapIsSetFsCawapDiscStatsEntry->bFsCapwapDiscStatsRowStatus =
                OSIX_FALSE;
    }
    if (pCapwapIsSetFsCawapDiscStatsEntry->bCapwapBaseWtpProfileId == OSIX_TRUE)
    {
        if (pCapwapFsCawapDiscStatsEntry->MibObject.u4CapwapBaseWtpProfileId ==
            pCapwapSetFsCawapDiscStatsEntry->MibObject.u4CapwapBaseWtpProfileId)
            pCapwapIsSetFsCawapDiscStatsEntry->bCapwapBaseWtpProfileId =
                OSIX_FALSE;
    }
    if ((pCapwapIsSetFsCawapDiscStatsEntry->bFsCapwapDiscStatsRowStatus ==
         OSIX_FALSE)
        && (pCapwapIsSetFsCawapDiscStatsEntry->bCapwapBaseWtpProfileId ==
            OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  FsCawapJoinStatsTableFilterInputs
 Input       :  The Indices
                pCapwapFsCawapJoinStatsEntry
                pCapwapSetFsCawapJoinStatsEntry
                pCapwapIsSetFsCawapJoinStatsEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
FsCawapJoinStatsTableFilterInputs (tCapwapFsCawapJoinStatsEntry *
                                   pCapwapFsCawapJoinStatsEntry,
                                   tCapwapFsCawapJoinStatsEntry *
                                   pCapwapSetFsCawapJoinStatsEntry,
                                   tCapwapIsSetFsCawapJoinStatsEntry *
                                   pCapwapIsSetFsCawapJoinStatsEntry)
{
    if (pCapwapIsSetFsCawapJoinStatsEntry->bFsCapwapJoinStatsRowStatus ==
        OSIX_TRUE)
    {
        if (pCapwapFsCawapJoinStatsEntry->MibObject.
            i4FsCapwapJoinStatsRowStatus ==
            pCapwapSetFsCawapJoinStatsEntry->MibObject.
            i4FsCapwapJoinStatsRowStatus)
            pCapwapIsSetFsCawapJoinStatsEntry->bFsCapwapJoinStatsRowStatus =
                OSIX_FALSE;
    }
    if (pCapwapIsSetFsCawapJoinStatsEntry->bCapwapBaseWtpProfileId == OSIX_TRUE)
    {
        if (pCapwapFsCawapJoinStatsEntry->MibObject.u4CapwapBaseWtpProfileId ==
            pCapwapSetFsCawapJoinStatsEntry->MibObject.u4CapwapBaseWtpProfileId)
            pCapwapIsSetFsCawapJoinStatsEntry->bCapwapBaseWtpProfileId =
                OSIX_FALSE;
    }
    if ((pCapwapIsSetFsCawapJoinStatsEntry->bFsCapwapJoinStatsRowStatus ==
         OSIX_FALSE)
        && (pCapwapIsSetFsCawapJoinStatsEntry->bCapwapBaseWtpProfileId ==
            OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  FsCawapConfigStatsTableFilterInputs
 Input       :  The Indices
                pCapwapFsCawapConfigStatsEntry
                pCapwapSetFsCawapConfigStatsEntry
                pCapwapIsSetFsCawapConfigStatsEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
FsCawapConfigStatsTableFilterInputs (tCapwapFsCawapConfigStatsEntry *
                                     pCapwapFsCawapConfigStatsEntry,
                                     tCapwapFsCawapConfigStatsEntry *
                                     pCapwapSetFsCawapConfigStatsEntry,
                                     tCapwapIsSetFsCawapConfigStatsEntry *
                                     pCapwapIsSetFsCawapConfigStatsEntry)
{
    if (pCapwapIsSetFsCawapConfigStatsEntry->bFsCapwapConfigStatsRowStatus ==
        OSIX_TRUE)
    {
        if (pCapwapFsCawapConfigStatsEntry->MibObject.
            i4FsCapwapConfigStatsRowStatus ==
            pCapwapSetFsCawapConfigStatsEntry->MibObject.
            i4FsCapwapConfigStatsRowStatus)
            pCapwapIsSetFsCawapConfigStatsEntry->bFsCapwapConfigStatsRowStatus =
                OSIX_FALSE;
    }
    if (pCapwapIsSetFsCawapConfigStatsEntry->bCapwapBaseWtpProfileId ==
        OSIX_TRUE)
    {
        if (pCapwapFsCawapConfigStatsEntry->MibObject.
            u4CapwapBaseWtpProfileId ==
            pCapwapSetFsCawapConfigStatsEntry->MibObject.
            u4CapwapBaseWtpProfileId)
            pCapwapIsSetFsCawapConfigStatsEntry->bCapwapBaseWtpProfileId =
                OSIX_FALSE;
    }
    if ((pCapwapIsSetFsCawapConfigStatsEntry->bFsCapwapConfigStatsRowStatus ==
         OSIX_FALSE)
        && (pCapwapIsSetFsCawapConfigStatsEntry->bCapwapBaseWtpProfileId ==
            OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  FsCawapRunStatsTableFilterInputs
 Input       :  The Indices
                pCapwapFsCawapRunStatsEntry
                pCapwapSetFsCawapRunStatsEntry
                pCapwapIsSetFsCawapRunStatsEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
FsCawapRunStatsTableFilterInputs (tCapwapFsCawapRunStatsEntry *
                                  pCapwapFsCawapRunStatsEntry,
                                  tCapwapFsCawapRunStatsEntry *
                                  pCapwapSetFsCawapRunStatsEntry,
                                  tCapwapIsSetFsCawapRunStatsEntry *
                                  pCapwapIsSetFsCawapRunStatsEntry)
{
    if (pCapwapIsSetFsCawapRunStatsEntry->bFsCapwapRunStatsRowStatus ==
        OSIX_TRUE)
    {
        if (pCapwapFsCawapRunStatsEntry->MibObject.
            i4FsCapwapRunStatsRowStatus ==
            pCapwapSetFsCawapRunStatsEntry->MibObject.
            i4FsCapwapRunStatsRowStatus)
            pCapwapIsSetFsCawapRunStatsEntry->bFsCapwapRunStatsRowStatus =
                OSIX_FALSE;
    }
    if (pCapwapIsSetFsCawapRunStatsEntry->bCapwapBaseWtpProfileId == OSIX_TRUE)
    {
        if (pCapwapFsCawapRunStatsEntry->MibObject.u4CapwapBaseWtpProfileId ==
            pCapwapSetFsCawapRunStatsEntry->MibObject.u4CapwapBaseWtpProfileId)
            pCapwapIsSetFsCawapRunStatsEntry->bCapwapBaseWtpProfileId =
                OSIX_FALSE;
    }
    if ((pCapwapIsSetFsCawapRunStatsEntry->bFsCapwapRunStatsRowStatus ==
         OSIX_FALSE)
        && (pCapwapIsSetFsCawapRunStatsEntry->bCapwapBaseWtpProfileId ==
            OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  FsCapwapWirelessBindingTableFilterInputs
 Input       :  The Indices
                pCapwapFsCapwapWirelessBindingEntry
                pCapwapSetFsCapwapWirelessBindingEntry
                pCapwapIsSetFsCapwapWirelessBindingEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
FsCapwapWirelessBindingTableFilterInputs (tCapwapFsCapwapWirelessBindingEntry *
                                          pCapwapFsCapwapWirelessBindingEntry,
                                          tCapwapFsCapwapWirelessBindingEntry *
                                          pCapwapSetFsCapwapWirelessBindingEntry,
                                          tCapwapIsSetFsCapwapWirelessBindingEntry
                                          *
                                          pCapwapIsSetFsCapwapWirelessBindingEntry)
{
    if (pCapwapIsSetFsCapwapWirelessBindingEntry->
        bFsCapwapWirelessBindingVirtualRadioIfIndex == OSIX_TRUE)
    {
        if (pCapwapFsCapwapWirelessBindingEntry->MibObject.
            i4FsCapwapWirelessBindingVirtualRadioIfIndex ==
            pCapwapSetFsCapwapWirelessBindingEntry->MibObject.
            i4FsCapwapWirelessBindingVirtualRadioIfIndex)
            pCapwapIsSetFsCapwapWirelessBindingEntry->
                bFsCapwapWirelessBindingVirtualRadioIfIndex = OSIX_FALSE;
    }
    if (pCapwapIsSetFsCapwapWirelessBindingEntry->
        bFsCapwapWirelessBindingType == OSIX_TRUE)
    {
        if (pCapwapFsCapwapWirelessBindingEntry->MibObject.
            i4FsCapwapWirelessBindingType ==
            pCapwapSetFsCapwapWirelessBindingEntry->MibObject.
            i4FsCapwapWirelessBindingType)
            pCapwapIsSetFsCapwapWirelessBindingEntry->
                bFsCapwapWirelessBindingType = OSIX_FALSE;
    }
    if (pCapwapIsSetFsCapwapWirelessBindingEntry->
        bFsCapwapWirelessBindingRowStatus == OSIX_TRUE)
    {
        if (pCapwapFsCapwapWirelessBindingEntry->MibObject.
            i4FsCapwapWirelessBindingRowStatus ==
            pCapwapSetFsCapwapWirelessBindingEntry->MibObject.
            i4FsCapwapWirelessBindingRowStatus)
            pCapwapIsSetFsCapwapWirelessBindingEntry->
                bFsCapwapWirelessBindingRowStatus = OSIX_FALSE;
    }
    if (pCapwapIsSetFsCapwapWirelessBindingEntry->bCapwapBaseWtpProfileId ==
        OSIX_TRUE)
    {
        if (pCapwapFsCapwapWirelessBindingEntry->MibObject.
            u4CapwapBaseWtpProfileId ==
            pCapwapSetFsCapwapWirelessBindingEntry->MibObject.
            u4CapwapBaseWtpProfileId)
            pCapwapIsSetFsCapwapWirelessBindingEntry->bCapwapBaseWtpProfileId =
                OSIX_FALSE;
    }
    if (pCapwapIsSetFsCapwapWirelessBindingEntry->
        bCapwapBaseWirelessBindingRadioId == OSIX_TRUE)
    {
        if (pCapwapFsCapwapWirelessBindingEntry->MibObject.
            u4CapwapBaseWirelessBindingRadioId ==
            pCapwapSetFsCapwapWirelessBindingEntry->MibObject.
            u4CapwapBaseWirelessBindingRadioId)
            pCapwapIsSetFsCapwapWirelessBindingEntry->
                bCapwapBaseWirelessBindingRadioId = OSIX_FALSE;
    }
    if ((pCapwapIsSetFsCapwapWirelessBindingEntry->
         bFsCapwapWirelessBindingVirtualRadioIfIndex == OSIX_FALSE)
        && (pCapwapIsSetFsCapwapWirelessBindingEntry->
            bFsCapwapWirelessBindingType == OSIX_FALSE)
        && (pCapwapIsSetFsCapwapWirelessBindingEntry->
            bFsCapwapWirelessBindingRowStatus == OSIX_FALSE)
        && (pCapwapIsSetFsCapwapWirelessBindingEntry->bCapwapBaseWtpProfileId ==
            OSIX_FALSE)
        && (pCapwapIsSetFsCapwapWirelessBindingEntry->
            bCapwapBaseWirelessBindingRadioId == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  FsCapwapStationWhiteListFilterInputs
 Input       :  The Indices
                pCapwapFsCapwapStationWhiteListEntry
                pCapwapSetFsCapwapStationWhiteListEntry
                pCapwapIsSetFsCapwapStationWhiteListEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
FsCapwapStationWhiteListFilterInputs (tCapwapFsCapwapStationWhiteListEntry *
                                      pCapwapFsCapwapStationWhiteListEntry,
                                      tCapwapFsCapwapStationWhiteListEntry *
                                      pCapwapSetFsCapwapStationWhiteListEntry,
                                      tCapwapIsSetFsCapwapStationWhiteListEntry
                                      *
                                      pCapwapIsSetFsCapwapStationWhiteListEntry)
{
    if (pCapwapIsSetFsCapwapStationWhiteListEntry->
        bFsCapwapStationWhiteListId == OSIX_TRUE)
    {
        if (pCapwapFsCapwapStationWhiteListEntry->MibObject.
            u4FsCapwapStationWhiteListId ==
            pCapwapSetFsCapwapStationWhiteListEntry->MibObject.
            u4FsCapwapStationWhiteListId)
            pCapwapIsSetFsCapwapStationWhiteListEntry->
                bFsCapwapStationWhiteListId = OSIX_FALSE;
    }
    if (pCapwapIsSetFsCapwapStationWhiteListEntry->
        bFsCapwapStationWhiteListStationId == OSIX_TRUE)
    {
        if ((MEMCMP
             (pCapwapFsCapwapStationWhiteListEntry->MibObject.
              au1FsCapwapStationWhiteListStationId,
              pCapwapSetFsCapwapStationWhiteListEntry->MibObject.
              au1FsCapwapStationWhiteListStationId,
              pCapwapSetFsCapwapStationWhiteListEntry->MibObject.
              i4FsCapwapStationWhiteListStationIdLen) == 0)
            && (pCapwapFsCapwapStationWhiteListEntry->MibObject.
                i4FsCapwapStationWhiteListStationIdLen ==
                pCapwapSetFsCapwapStationWhiteListEntry->MibObject.
                i4FsCapwapStationWhiteListStationIdLen))
            pCapwapIsSetFsCapwapStationWhiteListEntry->
                bFsCapwapStationWhiteListStationId = OSIX_FALSE;
    }
    if (pCapwapIsSetFsCapwapStationWhiteListEntry->
        bFsCapwapStationWhiteListRowStatus == OSIX_TRUE)
    {
        if (pCapwapFsCapwapStationWhiteListEntry->MibObject.
            i4FsCapwapStationWhiteListRowStatus ==
            pCapwapSetFsCapwapStationWhiteListEntry->MibObject.
            i4FsCapwapStationWhiteListRowStatus)
            pCapwapIsSetFsCapwapStationWhiteListEntry->
                bFsCapwapStationWhiteListRowStatus = OSIX_FALSE;
    }
    if ((pCapwapIsSetFsCapwapStationWhiteListEntry->
         bFsCapwapStationWhiteListId == OSIX_FALSE)
        && (pCapwapIsSetFsCapwapStationWhiteListEntry->
            bFsCapwapStationWhiteListStationId == OSIX_FALSE)
        && (pCapwapIsSetFsCapwapStationWhiteListEntry->
            bFsCapwapStationWhiteListRowStatus == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  FsCapwapWtpRebootStatisticsTableFilterInputs
 Input       :  The Indices
                pCapwapFsCapwapWtpRebootStatisticsEntry
                pCapwapSetFsCapwapWtpRebootStatisticsEntry
                pCapwapIsSetFsCapwapWtpRebootStatisticsEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    FsCapwapWtpRebootStatisticsTableFilterInputs
    (tCapwapFsCapwapWtpRebootStatisticsEntry *
     pCapwapFsCapwapWtpRebootStatisticsEntry,
     tCapwapFsCapwapWtpRebootStatisticsEntry *
     pCapwapSetFsCapwapWtpRebootStatisticsEntry,
     tCapwapIsSetFsCapwapWtpRebootStatisticsEntry *
     pCapwapIsSetFsCapwapWtpRebootStatisticsEntry)
{
    if (pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
        bFsCapwapWtpRebootStatisticsRebootCount == OSIX_TRUE)
    {
        if (pCapwapFsCapwapWtpRebootStatisticsEntry->MibObject.
            u4FsCapwapWtpRebootStatisticsRebootCount ==
            pCapwapSetFsCapwapWtpRebootStatisticsEntry->MibObject.
            u4FsCapwapWtpRebootStatisticsRebootCount)
            pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
                bFsCapwapWtpRebootStatisticsRebootCount = OSIX_FALSE;
    }
    if (pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
        bFsCapwapWtpRebootStatisticsAcInitiatedCount == OSIX_TRUE)
    {
        if (pCapwapFsCapwapWtpRebootStatisticsEntry->MibObject.
            u4FsCapwapWtpRebootStatisticsAcInitiatedCount ==
            pCapwapSetFsCapwapWtpRebootStatisticsEntry->MibObject.
            u4FsCapwapWtpRebootStatisticsAcInitiatedCount)
            pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
                bFsCapwapWtpRebootStatisticsAcInitiatedCount = OSIX_FALSE;
    }
    if (pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
        bFsCapwapWtpRebootStatisticsLinkFailureCount == OSIX_TRUE)
    {
        if (pCapwapFsCapwapWtpRebootStatisticsEntry->MibObject.
            u4FsCapwapWtpRebootStatisticsLinkFailureCount ==
            pCapwapSetFsCapwapWtpRebootStatisticsEntry->MibObject.
            u4FsCapwapWtpRebootStatisticsLinkFailureCount)
            pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
                bFsCapwapWtpRebootStatisticsLinkFailureCount = OSIX_FALSE;
    }
    if (pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
        bFsCapwapWtpRebootStatisticsSwFailureCount == OSIX_TRUE)
    {
        if (pCapwapFsCapwapWtpRebootStatisticsEntry->MibObject.
            u4FsCapwapWtpRebootStatisticsSwFailureCount ==
            pCapwapSetFsCapwapWtpRebootStatisticsEntry->MibObject.
            u4FsCapwapWtpRebootStatisticsSwFailureCount)
            pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
                bFsCapwapWtpRebootStatisticsSwFailureCount = OSIX_FALSE;
    }
    if (pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
        bFsCapwapWtpRebootStatisticsHwFailureCount == OSIX_TRUE)
    {
        if (pCapwapFsCapwapWtpRebootStatisticsEntry->MibObject.
            u4FsCapwapWtpRebootStatisticsHwFailureCount ==
            pCapwapSetFsCapwapWtpRebootStatisticsEntry->MibObject.
            u4FsCapwapWtpRebootStatisticsHwFailureCount)
            pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
                bFsCapwapWtpRebootStatisticsHwFailureCount = OSIX_FALSE;
    }
    if (pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
        bFsCapwapWtpRebootStatisticsOtherFailureCount == OSIX_TRUE)
    {
        if (pCapwapFsCapwapWtpRebootStatisticsEntry->MibObject.
            u4FsCapwapWtpRebootStatisticsOtherFailureCount ==
            pCapwapSetFsCapwapWtpRebootStatisticsEntry->MibObject.
            u4FsCapwapWtpRebootStatisticsOtherFailureCount)
            pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
                bFsCapwapWtpRebootStatisticsOtherFailureCount = OSIX_FALSE;
    }
    if (pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
        bFsCapwapWtpRebootStatisticsUnknownFailureCount == OSIX_TRUE)
    {
        if (pCapwapFsCapwapWtpRebootStatisticsEntry->MibObject.
            u4FsCapwapWtpRebootStatisticsUnknownFailureCount ==
            pCapwapSetFsCapwapWtpRebootStatisticsEntry->MibObject.
            u4FsCapwapWtpRebootStatisticsUnknownFailureCount)
            pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
                bFsCapwapWtpRebootStatisticsUnknownFailureCount = OSIX_FALSE;
    }
    if (pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
        bFsCapwapWtpRebootStatisticsLastFailureType == OSIX_TRUE)
    {
        if (pCapwapFsCapwapWtpRebootStatisticsEntry->MibObject.
            i4FsCapwapWtpRebootStatisticsLastFailureType ==
            pCapwapSetFsCapwapWtpRebootStatisticsEntry->MibObject.
            i4FsCapwapWtpRebootStatisticsLastFailureType)
            pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
                bFsCapwapWtpRebootStatisticsLastFailureType = OSIX_FALSE;
    }
    if (pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
        bFsCapwapWtpRebootStatisticsRowStatus == OSIX_TRUE)
    {
        if (pCapwapFsCapwapWtpRebootStatisticsEntry->MibObject.
            i4FsCapwapWtpRebootStatisticsRowStatus ==
            pCapwapSetFsCapwapWtpRebootStatisticsEntry->MibObject.
            i4FsCapwapWtpRebootStatisticsRowStatus)
            pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
                bFsCapwapWtpRebootStatisticsRowStatus = OSIX_FALSE;
    }
    if (pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->bCapwapBaseWtpProfileId ==
        OSIX_TRUE)
    {
        if (pCapwapFsCapwapWtpRebootStatisticsEntry->MibObject.
            u4CapwapBaseWtpProfileId ==
            pCapwapSetFsCapwapWtpRebootStatisticsEntry->MibObject.
            u4CapwapBaseWtpProfileId)
            pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
                bCapwapBaseWtpProfileId = OSIX_FALSE;
    }
    if ((pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
         bFsCapwapWtpRebootStatisticsRebootCount == OSIX_FALSE)
        && (pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
            bFsCapwapWtpRebootStatisticsAcInitiatedCount == OSIX_FALSE)
        && (pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
            bFsCapwapWtpRebootStatisticsLinkFailureCount == OSIX_FALSE)
        && (pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
            bFsCapwapWtpRebootStatisticsSwFailureCount == OSIX_FALSE)
        && (pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
            bFsCapwapWtpRebootStatisticsHwFailureCount == OSIX_FALSE)
        && (pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
            bFsCapwapWtpRebootStatisticsOtherFailureCount == OSIX_FALSE)
        && (pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
            bFsCapwapWtpRebootStatisticsUnknownFailureCount == OSIX_FALSE)
        && (pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
            bFsCapwapWtpRebootStatisticsLastFailureType == OSIX_FALSE)
        && (pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
            bFsCapwapWtpRebootStatisticsRowStatus == OSIX_FALSE)
        && (pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
            bCapwapBaseWtpProfileId == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  FsCapwapWtpRadioStatisticsTableFilterInputs
 Input       :  The Indices
                pCapwapFsCapwapWtpRadioStatisticsEntry
                pCapwapSetFsCapwapWtpRadioStatisticsEntry
                pCapwapIsSetFsCapwapWtpRadioStatisticsEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    FsCapwapWtpRadioStatisticsTableFilterInputs
    (tCapwapFsCapwapWtpRadioStatisticsEntry *
     pCapwapFsCapwapWtpRadioStatisticsEntry,
     tCapwapFsCapwapWtpRadioStatisticsEntry *
     pCapwapSetFsCapwapWtpRadioStatisticsEntry,
     tCapwapIsSetFsCapwapWtpRadioStatisticsEntry *
     pCapwapIsSetFsCapwapWtpRadioStatisticsEntry)
{
    if (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
        bFsCapwapWtpRadioLastFailType == OSIX_TRUE)
    {
        if (pCapwapFsCapwapWtpRadioStatisticsEntry->MibObject.
            i4FsCapwapWtpRadioLastFailType ==
            pCapwapSetFsCapwapWtpRadioStatisticsEntry->MibObject.
            i4FsCapwapWtpRadioLastFailType)
            pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
                bFsCapwapWtpRadioLastFailType = OSIX_FALSE;
    }
    if (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
        bFsCapwapWtpRadioResetCount == OSIX_TRUE)
    {
        if (pCapwapFsCapwapWtpRadioStatisticsEntry->MibObject.
            u4FsCapwapWtpRadioResetCount ==
            pCapwapSetFsCapwapWtpRadioStatisticsEntry->MibObject.
            u4FsCapwapWtpRadioResetCount)
            pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
                bFsCapwapWtpRadioResetCount = OSIX_FALSE;
    }
    if (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
        bFsCapwapWtpRadioSwFailureCount == OSIX_TRUE)
    {
        if (pCapwapFsCapwapWtpRadioStatisticsEntry->MibObject.
            u4FsCapwapWtpRadioSwFailureCount ==
            pCapwapSetFsCapwapWtpRadioStatisticsEntry->MibObject.
            u4FsCapwapWtpRadioSwFailureCount)
            pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
                bFsCapwapWtpRadioSwFailureCount = OSIX_FALSE;
    }
    if (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
        bFsCapwapWtpRadioHwFailureCount == OSIX_TRUE)
    {
        if (pCapwapFsCapwapWtpRadioStatisticsEntry->MibObject.
            u4FsCapwapWtpRadioHwFailureCount ==
            pCapwapSetFsCapwapWtpRadioStatisticsEntry->MibObject.
            u4FsCapwapWtpRadioHwFailureCount)
            pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
                bFsCapwapWtpRadioHwFailureCount = OSIX_FALSE;
    }
    if (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
        bFsCapwapWtpRadioOtherFailureCount == OSIX_TRUE)
    {
        if (pCapwapFsCapwapWtpRadioStatisticsEntry->MibObject.
            u4FsCapwapWtpRadioOtherFailureCount ==
            pCapwapSetFsCapwapWtpRadioStatisticsEntry->MibObject.
            u4FsCapwapWtpRadioOtherFailureCount)
            pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
                bFsCapwapWtpRadioOtherFailureCount = OSIX_FALSE;
    }
    if (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
        bFsCapwapWtpRadioUnknownFailureCount == OSIX_TRUE)
    {
        if (pCapwapFsCapwapWtpRadioStatisticsEntry->MibObject.
            u4FsCapwapWtpRadioUnknownFailureCount ==
            pCapwapSetFsCapwapWtpRadioStatisticsEntry->MibObject.
            u4FsCapwapWtpRadioUnknownFailureCount)
            pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
                bFsCapwapWtpRadioUnknownFailureCount = OSIX_FALSE;
    }
    if (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
        bFsCapwapWtpRadioConfigUpdateCount == OSIX_TRUE)
    {
        if (pCapwapFsCapwapWtpRadioStatisticsEntry->MibObject.
            u4FsCapwapWtpRadioConfigUpdateCount ==
            pCapwapSetFsCapwapWtpRadioStatisticsEntry->MibObject.
            u4FsCapwapWtpRadioConfigUpdateCount)
            pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
                bFsCapwapWtpRadioConfigUpdateCount = OSIX_FALSE;
    }
    if (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
        bFsCapwapWtpRadioChannelChangeCount == OSIX_TRUE)
    {
        if (pCapwapFsCapwapWtpRadioStatisticsEntry->MibObject.
            u4FsCapwapWtpRadioChannelChangeCount ==
            pCapwapSetFsCapwapWtpRadioStatisticsEntry->MibObject.
            u4FsCapwapWtpRadioChannelChangeCount)
            pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
                bFsCapwapWtpRadioChannelChangeCount = OSIX_FALSE;
    }
    if (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
        bFsCapwapWtpRadioBandChangeCount == OSIX_TRUE)
    {
        if (pCapwapFsCapwapWtpRadioStatisticsEntry->MibObject.
            u4FsCapwapWtpRadioBandChangeCount ==
            pCapwapSetFsCapwapWtpRadioStatisticsEntry->MibObject.
            u4FsCapwapWtpRadioBandChangeCount)
            pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
                bFsCapwapWtpRadioBandChangeCount = OSIX_FALSE;
    }
    if (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
        bFsCapwapWtpRadioCurrentNoiseFloor == OSIX_TRUE)
    {
        if (pCapwapFsCapwapWtpRadioStatisticsEntry->MibObject.
            u4FsCapwapWtpRadioCurrentNoiseFloor ==
            pCapwapSetFsCapwapWtpRadioStatisticsEntry->MibObject.
            u4FsCapwapWtpRadioCurrentNoiseFloor)
            pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
                bFsCapwapWtpRadioCurrentNoiseFloor = OSIX_FALSE;
    }
    if (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
        bFsCapwapWtpRadioStatRowStatus == OSIX_TRUE)
    {
        if (pCapwapFsCapwapWtpRadioStatisticsEntry->MibObject.
            u4FsCapwapWtpRadioStatRowStatus ==
            pCapwapSetFsCapwapWtpRadioStatisticsEntry->MibObject.
            u4FsCapwapWtpRadioStatRowStatus)
            pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
                bFsCapwapWtpRadioStatRowStatus = OSIX_FALSE;
    }
    if (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->bRadioIfIndex == OSIX_TRUE)
    {
        if (pCapwapFsCapwapWtpRadioStatisticsEntry->MibObject.i4RadioIfIndex ==
            pCapwapSetFsCapwapWtpRadioStatisticsEntry->MibObject.i4RadioIfIndex)
            pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->bRadioIfIndex =
                OSIX_FALSE;
    }
    if ((pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
         bFsCapwapWtpRadioLastFailType == OSIX_FALSE)
        && (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
            bFsCapwapWtpRadioResetCount == OSIX_FALSE)
        && (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
            bFsCapwapWtpRadioSwFailureCount == OSIX_FALSE)
        && (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
            bFsCapwapWtpRadioHwFailureCount == OSIX_FALSE)
        && (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
            bFsCapwapWtpRadioOtherFailureCount == OSIX_FALSE)
        && (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
            bFsCapwapWtpRadioUnknownFailureCount == OSIX_FALSE)
        && (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
            bFsCapwapWtpRadioConfigUpdateCount == OSIX_FALSE)
        && (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
            bFsCapwapWtpRadioChannelChangeCount == OSIX_FALSE)
        && (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
            bFsCapwapWtpRadioBandChangeCount == OSIX_FALSE)
        && (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
            bFsCapwapWtpRadioCurrentNoiseFloor == OSIX_FALSE)
        && (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
            bFsCapwapWtpRadioStatRowStatus == OSIX_FALSE)
        && (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->bRadioIfIndex ==
            OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  CapwapSetAllCapwapBaseAcNameListTableTrigger
 Input       :  The Indices
                pCapwapSetCapwapBaseAcNameListEntry
                pCapwapIsSetCapwapBaseAcNameListEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
CapwapSetAllCapwapBaseAcNameListTableTrigger (tCapwapCapwapBaseAcNameListEntry *
                                              pCapwapSetCapwapBaseAcNameListEntry,
                                              tCapwapIsSetCapwapBaseAcNameListEntry
                                              *
                                              pCapwapIsSetCapwapBaseAcNameListEntry,
                                              INT4 i4SetOption)
{
    tSNMP_OCTET_STRING_TYPE CapwapBaseAcNameListNameVal;
    UINT1               au1CapwapBaseAcNameListNameVal[256];

    MEMSET (au1CapwapBaseAcNameListNameVal, 0,
            sizeof (au1CapwapBaseAcNameListNameVal));
    CapwapBaseAcNameListNameVal.pu1_OctetList = au1CapwapBaseAcNameListNameVal;
    CapwapBaseAcNameListNameVal.i4_Length = 0;

    if (pCapwapIsSetCapwapBaseAcNameListEntry->bCapwapBaseAcNameListId ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (CapwapBaseAcNameListId, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %u",
                      pCapwapSetCapwapBaseAcNameListEntry->MibObject.
                      u4CapwapBaseAcNameListId,
                      pCapwapSetCapwapBaseAcNameListEntry->MibObject.
                      u4CapwapBaseAcNameListId);
    }
    if (pCapwapIsSetCapwapBaseAcNameListEntry->bCapwapBaseAcNameListName ==
        OSIX_TRUE)
    {
        MEMCPY (CapwapBaseAcNameListNameVal.pu1_OctetList,
                pCapwapSetCapwapBaseAcNameListEntry->MibObject.
                au1CapwapBaseAcNameListName,
                pCapwapSetCapwapBaseAcNameListEntry->MibObject.
                i4CapwapBaseAcNameListNameLen);
        CapwapBaseAcNameListNameVal.i4_Length =
            pCapwapSetCapwapBaseAcNameListEntry->MibObject.
            i4CapwapBaseAcNameListNameLen;

        nmhSetCmnNew (CapwapBaseAcNameListName, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %s",
                      pCapwapSetCapwapBaseAcNameListEntry->MibObject.
                      u4CapwapBaseAcNameListId, &CapwapBaseAcNameListNameVal);
    }
    if (pCapwapIsSetCapwapBaseAcNameListEntry->bCapwapBaseAcNameListPriority ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (CapwapBaseAcNameListPriority, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %u",
                      pCapwapSetCapwapBaseAcNameListEntry->MibObject.
                      u4CapwapBaseAcNameListId,
                      pCapwapSetCapwapBaseAcNameListEntry->MibObject.
                      u4CapwapBaseAcNameListPriority);
    }
    if (pCapwapIsSetCapwapBaseAcNameListEntry->bCapwapBaseAcNameListRowStatus ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (CapwapBaseAcNameListRowStatus, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 1, 1, i4SetOption, "%u %i",
                      pCapwapSetCapwapBaseAcNameListEntry->MibObject.
                      u4CapwapBaseAcNameListId,
                      pCapwapSetCapwapBaseAcNameListEntry->MibObject.
                      i4CapwapBaseAcNameListRowStatus);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapSetAllCapwapBaseMacAclTableTrigger
 Input       :  The Indices
                pCapwapSetCapwapBaseMacAclEntry
                pCapwapIsSetCapwapBaseMacAclEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
CapwapSetAllCapwapBaseMacAclTableTrigger (tCapwapCapwapBaseMacAclEntry *
                                          pCapwapSetCapwapBaseMacAclEntry,
                                          tCapwapIsSetCapwapBaseMacAclEntry *
                                          pCapwapIsSetCapwapBaseMacAclEntry,
                                          INT4 i4SetOption)
{
    tSNMP_OCTET_STRING_TYPE CapwapBaseMacAclStationIdVal;
    UINT1               au1CapwapBaseMacAclStationIdVal[256];

    MEMSET (au1CapwapBaseMacAclStationIdVal, 0,
            sizeof (au1CapwapBaseMacAclStationIdVal));
    CapwapBaseMacAclStationIdVal.pu1_OctetList =
        au1CapwapBaseMacAclStationIdVal;
    CapwapBaseMacAclStationIdVal.i4_Length = 0;

    if (pCapwapIsSetCapwapBaseMacAclEntry->bCapwapBaseMacAclId == OSIX_TRUE)
    {
        nmhSetCmnNew (CapwapBaseMacAclId, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %u",
                      pCapwapSetCapwapBaseMacAclEntry->MibObject.
                      u4CapwapBaseMacAclId,
                      pCapwapSetCapwapBaseMacAclEntry->MibObject.
                      u4CapwapBaseMacAclId);
    }
    if (pCapwapIsSetCapwapBaseMacAclEntry->bCapwapBaseMacAclStationId ==
        OSIX_TRUE)
    {
        MEMCPY (CapwapBaseMacAclStationIdVal.pu1_OctetList,
                pCapwapSetCapwapBaseMacAclEntry->MibObject.
                au1CapwapBaseMacAclStationId,
                pCapwapSetCapwapBaseMacAclEntry->MibObject.
                i4CapwapBaseMacAclStationIdLen);
        CapwapBaseMacAclStationIdVal.i4_Length =
            pCapwapSetCapwapBaseMacAclEntry->MibObject.
            i4CapwapBaseMacAclStationIdLen;

        nmhSetCmnNew (CapwapBaseMacAclStationId, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %s",
                      pCapwapSetCapwapBaseMacAclEntry->MibObject.
                      u4CapwapBaseMacAclId, &CapwapBaseMacAclStationIdVal);
    }
    if (pCapwapIsSetCapwapBaseMacAclEntry->bCapwapBaseMacAclRowStatus ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (CapwapBaseMacAclRowStatus, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 1, 1, i4SetOption, "%u %i",
                      pCapwapSetCapwapBaseMacAclEntry->MibObject.
                      u4CapwapBaseMacAclId,
                      pCapwapSetCapwapBaseMacAclEntry->MibObject.
                      i4CapwapBaseMacAclRowStatus);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapSetAllCapwapBaseWtpProfileTableTrigger
 Input       :  The Indices
                pCapwapSetCapwapBaseWtpProfileEntry
                pCapwapIsSetCapwapBaseWtpProfileEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
CapwapSetAllCapwapBaseWtpProfileTableTrigger (tCapwapCapwapBaseWtpProfileEntry *
                                              pCapwapSetCapwapBaseWtpProfileEntry,
                                              tCapwapIsSetCapwapBaseWtpProfileEntry
                                              *
                                              pCapwapIsSetCapwapBaseWtpProfileEntry,
                                              INT4 i4SetOption)
{
    tSNMP_OCTET_STRING_TYPE CapwapBaseWtpProfileNameVal;
    UINT1               au1CapwapBaseWtpProfileNameVal[256];
    tSNMP_OCTET_STRING_TYPE CapwapBaseWtpProfileWtpMacAddressVal;
    UINT1               au1CapwapBaseWtpProfileWtpMacAddressVal[256];
    tSNMP_OCTET_STRING_TYPE CapwapBaseWtpProfileWtpModelNumberVal;
    UINT1               au1CapwapBaseWtpProfileWtpModelNumberVal[256];
    tSNMP_OCTET_STRING_TYPE CapwapBaseWtpProfileWtpNameVal;
    UINT1               au1CapwapBaseWtpProfileWtpNameVal[256];
    tSNMP_OCTET_STRING_TYPE CapwapBaseWtpProfileWtpLocationVal;
    UINT1               au1CapwapBaseWtpProfileWtpLocationVal[256];
    tSNMP_OCTET_STRING_TYPE CapwapBaseWtpProfileWtpStaticIpAddressVal;
    UINT1               au1CapwapBaseWtpProfileWtpStaticIpAddressVal[256];
    tSNMP_OCTET_STRING_TYPE CapwapBaseWtpProfileWtpNetmaskVal;
    UINT1               au1CapwapBaseWtpProfileWtpNetmaskVal[256];
    tSNMP_OCTET_STRING_TYPE CapwapBaseWtpProfileWtpGatewayVal;
    UINT1               au1CapwapBaseWtpProfileWtpGatewayVal[256];

    MEMSET (au1CapwapBaseWtpProfileNameVal, 0,
            sizeof (au1CapwapBaseWtpProfileNameVal));
    CapwapBaseWtpProfileNameVal.pu1_OctetList = au1CapwapBaseWtpProfileNameVal;
    CapwapBaseWtpProfileNameVal.i4_Length = 0;

    MEMSET (au1CapwapBaseWtpProfileWtpMacAddressVal, 0,
            sizeof (au1CapwapBaseWtpProfileWtpMacAddressVal));
    CapwapBaseWtpProfileWtpMacAddressVal.pu1_OctetList =
        au1CapwapBaseWtpProfileWtpMacAddressVal;
    CapwapBaseWtpProfileWtpMacAddressVal.i4_Length = 0;

    MEMSET (au1CapwapBaseWtpProfileWtpModelNumberVal, 0,
            sizeof (au1CapwapBaseWtpProfileWtpModelNumberVal));
    CapwapBaseWtpProfileWtpModelNumberVal.pu1_OctetList =
        au1CapwapBaseWtpProfileWtpModelNumberVal;
    CapwapBaseWtpProfileWtpModelNumberVal.i4_Length = 0;

    MEMSET (au1CapwapBaseWtpProfileWtpNameVal, 0,
            sizeof (au1CapwapBaseWtpProfileWtpNameVal));
    CapwapBaseWtpProfileWtpNameVal.pu1_OctetList =
        au1CapwapBaseWtpProfileWtpNameVal;
    CapwapBaseWtpProfileWtpNameVal.i4_Length = 0;

    MEMSET (au1CapwapBaseWtpProfileWtpLocationVal, 0,
            sizeof (au1CapwapBaseWtpProfileWtpLocationVal));
    CapwapBaseWtpProfileWtpLocationVal.pu1_OctetList =
        au1CapwapBaseWtpProfileWtpLocationVal;
    CapwapBaseWtpProfileWtpLocationVal.i4_Length = 0;

    MEMSET (au1CapwapBaseWtpProfileWtpStaticIpAddressVal, 0,
            sizeof (au1CapwapBaseWtpProfileWtpStaticIpAddressVal));
    CapwapBaseWtpProfileWtpStaticIpAddressVal.pu1_OctetList =
        au1CapwapBaseWtpProfileWtpStaticIpAddressVal;
    CapwapBaseWtpProfileWtpStaticIpAddressVal.i4_Length = 0;

    MEMSET (au1CapwapBaseWtpProfileWtpNetmaskVal, 0,
            sizeof (au1CapwapBaseWtpProfileWtpNetmaskVal));
    CapwapBaseWtpProfileWtpNetmaskVal.pu1_OctetList =
        au1CapwapBaseWtpProfileWtpNetmaskVal;
    CapwapBaseWtpProfileWtpNetmaskVal.i4_Length = 0;

    MEMSET (au1CapwapBaseWtpProfileWtpGatewayVal, 0,
            sizeof (au1CapwapBaseWtpProfileWtpGatewayVal));
    CapwapBaseWtpProfileWtpGatewayVal.pu1_OctetList =
        au1CapwapBaseWtpProfileWtpGatewayVal;
    CapwapBaseWtpProfileWtpGatewayVal.i4_Length = 0;

    if (pCapwapIsSetCapwapBaseWtpProfileEntry->bCapwapBaseWtpProfileId ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (CapwapBaseWtpProfileId, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %u",
                      pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                      u4CapwapBaseWtpProfileId,
                      pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                      u4CapwapBaseWtpProfileId);
    }
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->bCapwapBaseWtpProfileName ==
        OSIX_TRUE)
    {
        MEMCPY (CapwapBaseWtpProfileNameVal.pu1_OctetList,
                pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                au1CapwapBaseWtpProfileName,
                pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                i4CapwapBaseWtpProfileNameLen);
        CapwapBaseWtpProfileNameVal.i4_Length =
            pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileNameLen;

        nmhSetCmnNew (CapwapBaseWtpProfileName, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %s",
                      pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                      u4CapwapBaseWtpProfileId, &CapwapBaseWtpProfileNameVal);
    }
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpMacAddress == OSIX_TRUE)
    {
        MEMCPY (CapwapBaseWtpProfileWtpMacAddressVal.pu1_OctetList,
                pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                au1CapwapBaseWtpProfileWtpMacAddress,
                pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                i4CapwapBaseWtpProfileWtpMacAddressLen);
        CapwapBaseWtpProfileWtpMacAddressVal.i4_Length =
            pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileWtpMacAddressLen;

        nmhSetCmnNew (CapwapBaseWtpProfileWtpMacAddress, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %s",
                      pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                      u4CapwapBaseWtpProfileId,
                      &CapwapBaseWtpProfileWtpMacAddressVal);
    }
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpModelNumber == OSIX_TRUE)
    {
        MEMCPY (CapwapBaseWtpProfileWtpModelNumberVal.pu1_OctetList,
                pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                au1CapwapBaseWtpProfileWtpModelNumber,
                pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                i4CapwapBaseWtpProfileWtpModelNumberLen);
        CapwapBaseWtpProfileWtpModelNumberVal.i4_Length =
            pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileWtpModelNumberLen;

        nmhSetCmnNew (CapwapBaseWtpProfileWtpModelNumber, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %s",
                      pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                      u4CapwapBaseWtpProfileId,
                      &CapwapBaseWtpProfileWtpModelNumberVal);
    }
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->bCapwapBaseWtpProfileWtpName ==
        OSIX_TRUE)
    {
        MEMCPY (CapwapBaseWtpProfileWtpNameVal.pu1_OctetList,
                pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                au1CapwapBaseWtpProfileWtpName,
                pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                i4CapwapBaseWtpProfileWtpNameLen);
        CapwapBaseWtpProfileWtpNameVal.i4_Length =
            pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileWtpNameLen;

        nmhSetCmnNew (CapwapBaseWtpProfileWtpName, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %s",
                      pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                      u4CapwapBaseWtpProfileId,
                      &CapwapBaseWtpProfileWtpNameVal);
    }
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpLocation == OSIX_TRUE)
    {
        MEMCPY (CapwapBaseWtpProfileWtpLocationVal.pu1_OctetList,
                pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                au1CapwapBaseWtpProfileWtpLocation,
                pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                i4CapwapBaseWtpProfileWtpLocationLen);
        CapwapBaseWtpProfileWtpLocationVal.i4_Length =
            pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileWtpLocationLen;

        nmhSetCmnNew (CapwapBaseWtpProfileWtpLocation, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %s",
                      pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                      u4CapwapBaseWtpProfileId,
                      &CapwapBaseWtpProfileWtpLocationVal);
    }
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpStaticIpEnable == OSIX_TRUE)
    {
        nmhSetCmnNew (CapwapBaseWtpProfileWtpStaticIpEnable, 0,
                      CapwapMainTaskLock, CapwapMainTaskUnLock, 0, 0, 1,
                      i4SetOption, "%u %i",
                      pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                      u4CapwapBaseWtpProfileId,
                      pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                      i4CapwapBaseWtpProfileWtpStaticIpEnable);
    }
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bFsCapwapFsAcTimestampTrigger == OSIX_TRUE)
    {
        nmhSetCmnNew (FsCapwapFsAcTimestampTrigger, 0,
                      CapwapMainTaskLock, CapwapMainTaskUnLock, 0, 0, 1,
                      i4SetOption, "%u %i",
                      pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                      u4CapwapBaseWtpProfileId,
                      pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                      i4FsCapwapFsAcTimestampTrigger);
    }

    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpStaticIpType == OSIX_TRUE)
    {
        nmhSetCmnNew (CapwapBaseWtpProfileWtpStaticIpType, 0,
                      CapwapMainTaskLock, CapwapMainTaskUnLock, 0, 0, 1,
                      i4SetOption, "%u %i",
                      pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                      u4CapwapBaseWtpProfileId,
                      pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                      i4CapwapBaseWtpProfileWtpStaticIpType);
    }
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpStaticIpAddress == OSIX_TRUE)
    {
        MEMCPY (CapwapBaseWtpProfileWtpStaticIpAddressVal.pu1_OctetList,
                pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                au1CapwapBaseWtpProfileWtpStaticIpAddress,
                pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                i4CapwapBaseWtpProfileWtpStaticIpAddressLen);
        CapwapBaseWtpProfileWtpStaticIpAddressVal.i4_Length =
            pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileWtpStaticIpAddressLen;

        nmhSetCmnNew (CapwapBaseWtpProfileWtpStaticIpAddress, 0,
                      CapwapMainTaskLock, CapwapMainTaskUnLock, 0, 0, 1,
                      i4SetOption, "%u %s",
                      pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                      u4CapwapBaseWtpProfileId,
                      &CapwapBaseWtpProfileWtpStaticIpAddressVal);
    }
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpNetmask == OSIX_TRUE)
    {
        MEMCPY (CapwapBaseWtpProfileWtpNetmaskVal.pu1_OctetList,
                pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                au1CapwapBaseWtpProfileWtpNetmask,
                pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                i4CapwapBaseWtpProfileWtpNetmaskLen);
        CapwapBaseWtpProfileWtpNetmaskVal.i4_Length =
            pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileWtpNetmaskLen;

        nmhSetCmnNew (CapwapBaseWtpProfileWtpNetmask, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %s",
                      pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                      u4CapwapBaseWtpProfileId,
                      &CapwapBaseWtpProfileWtpNetmaskVal);
    }
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpGateway == OSIX_TRUE)
    {
        MEMCPY (CapwapBaseWtpProfileWtpGatewayVal.pu1_OctetList,
                pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                au1CapwapBaseWtpProfileWtpGateway,
                pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                i4CapwapBaseWtpProfileWtpGatewayLen);
        CapwapBaseWtpProfileWtpGatewayVal.i4_Length =
            pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileWtpGatewayLen;

        nmhSetCmnNew (CapwapBaseWtpProfileWtpGateway, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %s",
                      pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                      u4CapwapBaseWtpProfileId,
                      &CapwapBaseWtpProfileWtpGatewayVal);
    }
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpFallbackEnable == OSIX_TRUE)
    {
        nmhSetCmnNew (CapwapBaseWtpProfileWtpFallbackEnable, 0,
                      CapwapMainTaskLock, CapwapMainTaskUnLock, 0, 0, 1,
                      i4SetOption, "%u %i",
                      pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                      u4CapwapBaseWtpProfileId,
                      pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                      i4CapwapBaseWtpProfileWtpFallbackEnable);
    }
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpEchoInterval == OSIX_TRUE)
    {
        nmhSetCmnNew (CapwapBaseWtpProfileWtpEchoInterval, 0,
                      CapwapMainTaskLock, CapwapMainTaskUnLock, 0, 0, 1,
                      i4SetOption, "%u %u",
                      pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                      u4CapwapBaseWtpProfileId,
                      pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                      u4CapwapBaseWtpProfileWtpEchoInterval);
    }
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpIdleTimeout == OSIX_TRUE)
    {
        nmhSetCmnNew (CapwapBaseWtpProfileWtpIdleTimeout, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %u",
                      pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                      u4CapwapBaseWtpProfileId,
                      pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                      u4CapwapBaseWtpProfileWtpIdleTimeout);
    }
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpMaxDiscoveryInterval == OSIX_TRUE)
    {
        nmhSetCmnNew (CapwapBaseWtpProfileWtpMaxDiscoveryInterval, 0,
                      CapwapMainTaskLock, CapwapMainTaskUnLock, 0, 0, 1,
                      i4SetOption, "%u %u",
                      pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                      u4CapwapBaseWtpProfileId,
                      pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                      u4CapwapBaseWtpProfileWtpMaxDiscoveryInterval);
    }
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpReportInterval == OSIX_TRUE)
    {
        nmhSetCmnNew (CapwapBaseWtpProfileWtpReportInterval, 0,
                      CapwapMainTaskLock, CapwapMainTaskUnLock, 0, 0, 1,
                      i4SetOption, "%u %u",
                      pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                      u4CapwapBaseWtpProfileId,
                      pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                      u4CapwapBaseWtpProfileWtpReportInterval);
    }
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpStatisticsTimer == OSIX_TRUE)
    {
        nmhSetCmnNew (CapwapBaseWtpProfileWtpStatisticsTimer, 0,
                      CapwapMainTaskLock, CapwapMainTaskUnLock, 0, 0, 1,
                      i4SetOption, "%u %u",
                      pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                      u4CapwapBaseWtpProfileId,
                      pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                      u4CapwapBaseWtpProfileWtpStatisticsTimer);
    }
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpEcnSupport == OSIX_TRUE)
    {
        nmhSetCmnNew (CapwapBaseWtpProfileWtpEcnSupport, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %i",
                      pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                      u4CapwapBaseWtpProfileId,
                      pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                      i4CapwapBaseWtpProfileWtpEcnSupport);
    }
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->bCapwapBaseWtpProfileRowStatus ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (CapwapBaseWtpProfileRowStatus, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 1, 1, i4SetOption, "%u %i",
                      pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                      u4CapwapBaseWtpProfileId,
                      pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                      i4CapwapBaseWtpProfileRowStatus);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapSetAllFsWtpModelTableTrigger
 Input       :  The Indices
                pCapwapSetFsWtpModelEntry
                pCapwapIsSetFsWtpModelEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
CapwapSetAllFsWtpModelTableTrigger (tCapwapFsWtpModelEntry *
                                    pCapwapSetFsWtpModelEntry,
                                    tCapwapIsSetFsWtpModelEntry *
                                    pCapwapIsSetFsWtpModelEntry,
                                    INT4 i4SetOption)
{
    tSNMP_OCTET_STRING_TYPE FsCapwapWtpModelNumberVal;
    UINT1               au1FsCapwapWtpModelNumberVal[256];
    tSNMP_OCTET_STRING_TYPE FsCapwapWtpTunnelModeVal;
    UINT1               au1FsCapwapWtpTunnelModeVal[256];
    tSNMP_OCTET_STRING_TYPE FsCapwapImageNameVal;
    UINT1               au1FsCapwapImageNameVal[256];
    tSNMP_OCTET_STRING_TYPE FsCapwapQosProfileNameVal;
    UINT1               au1FsCapwapQosProfileNameVal[256];

    MEMSET (au1FsCapwapWtpModelNumberVal, 0,
            sizeof (au1FsCapwapWtpModelNumberVal));
    FsCapwapWtpModelNumberVal.pu1_OctetList = au1FsCapwapWtpModelNumberVal;
    FsCapwapWtpModelNumberVal.i4_Length = 0;

    MEMSET (au1FsCapwapWtpTunnelModeVal, 0,
            sizeof (au1FsCapwapWtpTunnelModeVal));
    FsCapwapWtpTunnelModeVal.pu1_OctetList = au1FsCapwapWtpTunnelModeVal;
    FsCapwapWtpTunnelModeVal.i4_Length = 0;

    MEMSET (au1FsCapwapImageNameVal, 0, sizeof (au1FsCapwapImageNameVal));
    FsCapwapImageNameVal.pu1_OctetList = au1FsCapwapImageNameVal;
    FsCapwapImageNameVal.i4_Length = 0;

    MEMSET (au1FsCapwapQosProfileNameVal, 0,
            sizeof (au1FsCapwapQosProfileNameVal));
    FsCapwapQosProfileNameVal.pu1_OctetList = au1FsCapwapQosProfileNameVal;
    FsCapwapQosProfileNameVal.i4_Length = 0;

    if (pCapwapIsSetFsWtpModelEntry->bFsCapwapWtpModelNumber == OSIX_TRUE)
    {
        MEMCPY (FsCapwapWtpModelNumberVal.pu1_OctetList,
                pCapwapSetFsWtpModelEntry->MibObject.au1FsCapwapWtpModelNumber,
                pCapwapSetFsWtpModelEntry->MibObject.
                i4FsCapwapWtpModelNumberLen);
        FsCapwapWtpModelNumberVal.i4_Length =
            pCapwapSetFsWtpModelEntry->MibObject.i4FsCapwapWtpModelNumberLen;

        nmhSetCmnNew (FsCapwapWtpModelNumber, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 0, 1, i4SetOption, "%s %s",
                      &FsCapwapWtpModelNumberVal, &FsCapwapWtpModelNumberVal);
    }
    if (pCapwapIsSetFsWtpModelEntry->bFsNoOfRadio == OSIX_TRUE)
    {
        nmhSetCmnNew (FsNoOfRadio, 0, CapwapMainTaskLock, CapwapMainTaskUnLock,
                      0, 0, 1, i4SetOption, "%s %u", &FsCapwapWtpModelNumberVal,
                      pCapwapSetFsWtpModelEntry->MibObject.u4FsNoOfRadio);
    }
    if (pCapwapIsSetFsWtpModelEntry->bFsCapwapWtpMacType == OSIX_TRUE)
    {
        nmhSetCmnNew (FsCapwapWtpMacType, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 0, 1, i4SetOption, "%s %i",
                      &FsCapwapWtpModelNumberVal,
                      pCapwapSetFsWtpModelEntry->MibObject.
                      i4FsCapwapWtpMacType);
    }
    if (pCapwapIsSetFsWtpModelEntry->bFsCapwapWtpTunnelMode == OSIX_TRUE)
    {
        MEMCPY (FsCapwapWtpTunnelModeVal.pu1_OctetList,
                pCapwapSetFsWtpModelEntry->MibObject.au1FsCapwapWtpTunnelMode,
                pCapwapSetFsWtpModelEntry->MibObject.
                i4FsCapwapWtpTunnelModeLen);
        FsCapwapWtpTunnelModeVal.i4_Length =
            pCapwapSetFsWtpModelEntry->MibObject.i4FsCapwapWtpTunnelModeLen;

        nmhSetCmnNew (FsCapwapWtpTunnelMode, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 0, 1, i4SetOption, "%s %s",
                      &FsCapwapWtpModelNumberVal, &FsCapwapWtpTunnelModeVal);
    }
    if (pCapwapIsSetFsWtpModelEntry->bFsCapwapImageName == OSIX_TRUE)
    {
        MEMCPY (FsCapwapImageNameVal.pu1_OctetList,
                pCapwapSetFsWtpModelEntry->MibObject.au1FsCapwapImageName,
                pCapwapSetFsWtpModelEntry->MibObject.i4FsCapwapImageNameLen);
        FsCapwapImageNameVal.i4_Length =
            pCapwapSetFsWtpModelEntry->MibObject.i4FsCapwapImageNameLen;

        nmhSetCmnNew (FsCapwapImageName, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 0, 1, i4SetOption, "%s %s",
                      &FsCapwapWtpModelNumberVal, &FsCapwapImageNameVal);
    }
    if (pCapwapIsSetFsWtpModelEntry->bFsCapwapQosProfileName == OSIX_TRUE)
    {
        MEMCPY (FsCapwapQosProfileNameVal.pu1_OctetList,
                pCapwapSetFsWtpModelEntry->MibObject.au1FsCapwapQosProfileName,
                pCapwapSetFsWtpModelEntry->MibObject.
                i4FsCapwapQosProfileNameLen);
        FsCapwapQosProfileNameVal.i4_Length =
            pCapwapSetFsWtpModelEntry->MibObject.i4FsCapwapQosProfileNameLen;

        nmhSetCmnNew (FsCapwapQosProfileName, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 0, 1, i4SetOption, "%s %s",
                      &FsCapwapWtpModelNumberVal, &FsCapwapQosProfileNameVal);
    }
    if (pCapwapIsSetFsWtpModelEntry->bFsMaxStations == OSIX_TRUE)
    {
        nmhSetCmnNew (FsMaxStations, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 0, 1, i4SetOption, "%s %i",
                      &FsCapwapWtpModelNumberVal,
                      pCapwapSetFsWtpModelEntry->MibObject.i4FsMaxStations);
    }
    if (pCapwapIsSetFsWtpModelEntry->bFsWtpModelRowStatus == OSIX_TRUE)
    {
        nmhSetCmnNew (FsWtpModelRowStatus, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 1, 1, i4SetOption, "%s %i",
                      &FsCapwapWtpModelNumberVal,
                      pCapwapSetFsWtpModelEntry->MibObject.
                      i4FsWtpModelRowStatus);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapSetAllFsWtpRadioTableTrigger
 Input       :  The Indices
                pCapwapSetFsWtpRadioEntry
                pCapwapIsSetFsWtpRadioEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
CapwapSetAllFsWtpRadioTableTrigger (tCapwapFsWtpRadioEntry *
                                    pCapwapSetFsWtpRadioEntry,
                                    tCapwapIsSetFsWtpRadioEntry *
                                    pCapwapIsSetFsWtpRadioEntry,
                                    INT4 i4SetOption)
{
    tSNMP_OCTET_STRING_TYPE FsCapwapWtpModelNumberVal;
    UINT1               au1FsCapwapWtpModelNumberVal[256];

    MEMSET (au1FsCapwapWtpModelNumberVal, 0,
            sizeof (au1FsCapwapWtpModelNumberVal));
    FsCapwapWtpModelNumberVal.pu1_OctetList = au1FsCapwapWtpModelNumberVal;
    FsCapwapWtpModelNumberVal.i4_Length = 0;

    if (pCapwapIsSetFsWtpRadioEntry->bFsNumOfRadio == OSIX_TRUE)
    {
        nmhSetCmnNew (FsNoOfRadio, 0, CapwapMainTaskLock, CapwapMainTaskUnLock,
                      0, 0, 2, i4SetOption, "%s %u %u",
                      &FsCapwapWtpModelNumberVal,
                      pCapwapSetFsWtpRadioEntry->MibObject.u4FsNumOfRadio,
                      pCapwapSetFsWtpRadioEntry->MibObject.u4FsNumOfRadio);
    }
    if (pCapwapIsSetFsWtpRadioEntry->bFsWtpRadioType == OSIX_TRUE)
    {
        nmhSetCmnNew (FsWtpRadioType, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 0, 2, i4SetOption, "%s %u %i",
                      &FsCapwapWtpModelNumberVal,
                      pCapwapSetFsWtpRadioEntry->MibObject.u4FsNumOfRadio,
                      pCapwapSetFsWtpRadioEntry->MibObject.u4FsWtpRadioType);
    }
    if (pCapwapIsSetFsWtpRadioEntry->bFsRadioAdminStatus == OSIX_TRUE)
    {
        nmhSetCmnNew (FsRadioAdminStatus, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 0, 2, i4SetOption, "%s %u %i",
                      &FsCapwapWtpModelNumberVal,
                      pCapwapSetFsWtpRadioEntry->MibObject.u4FsNumOfRadio,
                      pCapwapSetFsWtpRadioEntry->MibObject.
                      i4FsRadioAdminStatus);
    }
    if (pCapwapIsSetFsWtpRadioEntry->bFsWtpRadioRowStatus == OSIX_TRUE)
    {
        nmhSetCmnNew (FsWtpRadioRowStatus, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 1, 2, i4SetOption, "%s %u %i",
                      &FsCapwapWtpModelNumberVal,
                      pCapwapSetFsWtpRadioEntry->MibObject.u4FsNumOfRadio,
                      pCapwapSetFsWtpRadioEntry->MibObject.
                      i4FsWtpRadioRowStatus);
    }
    if (pCapwapIsSetFsWtpRadioEntry->bFsCapwapWtpModelNumber == OSIX_TRUE)
    {
        MEMCPY (FsCapwapWtpModelNumberVal.pu1_OctetList,
                pCapwapSetFsWtpRadioEntry->MibObject.au1FsCapwapWtpModelNumber,
                pCapwapSetFsWtpRadioEntry->MibObject.
                i4FsCapwapWtpModelNumberLen);
        FsCapwapWtpModelNumberVal.i4_Length =
            pCapwapSetFsWtpRadioEntry->MibObject.i4FsCapwapWtpModelNumberLen;

        nmhSetCmnNew (FsCapwapWtpModelNumber, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 0, 2, i4SetOption, "%s %u %s",
                      &FsCapwapWtpModelNumberVal,
                      pCapwapSetFsWtpRadioEntry->MibObject.u4FsNumOfRadio,
                      &FsCapwapWtpModelNumberVal);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapSetAllFsCapwapWhiteListTableTrigger
 Input       :  The Indices
                pCapwapSetFsCapwapWhiteListEntry
                pCapwapIsSetFsCapwapWhiteListEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
CapwapSetAllFsCapwapWhiteListTableTrigger (tCapwapFsCapwapWhiteListEntry *
                                           pCapwapSetFsCapwapWhiteListEntry,
                                           tCapwapIsSetFsCapwapWhiteListEntry *
                                           pCapwapIsSetFsCapwapWhiteListEntry,
                                           INT4 i4SetOption)
{
    tSNMP_OCTET_STRING_TYPE FsCapwapWhiteListWtpBaseMacVal;
    UINT1               au1FsCapwapWhiteListWtpBaseMacVal[256];

    MEMSET (au1FsCapwapWhiteListWtpBaseMacVal, 0,
            sizeof (au1FsCapwapWhiteListWtpBaseMacVal));
    FsCapwapWhiteListWtpBaseMacVal.pu1_OctetList =
        au1FsCapwapWhiteListWtpBaseMacVal;
    FsCapwapWhiteListWtpBaseMacVal.i4_Length = 0;

    if (pCapwapIsSetFsCapwapWhiteListEntry->bFsCapwapWhiteListId == OSIX_TRUE)
    {
        nmhSetCmnNew (FsCapwapWhiteListId, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %u",
                      pCapwapSetFsCapwapWhiteListEntry->MibObject.
                      u4FsCapwapWhiteListId,
                      pCapwapSetFsCapwapWhiteListEntry->MibObject.
                      u4FsCapwapWhiteListId);
    }
    if (pCapwapIsSetFsCapwapWhiteListEntry->bFsCapwapWhiteListWtpBaseMac ==
        OSIX_TRUE)
    {
        MEMCPY (FsCapwapWhiteListWtpBaseMacVal.pu1_OctetList,
                pCapwapSetFsCapwapWhiteListEntry->MibObject.
                au1FsCapwapWhiteListWtpBaseMac,
                pCapwapSetFsCapwapWhiteListEntry->MibObject.
                i4FsCapwapWhiteListWtpBaseMacLen);
        FsCapwapWhiteListWtpBaseMacVal.i4_Length =
            pCapwapSetFsCapwapWhiteListEntry->MibObject.
            i4FsCapwapWhiteListWtpBaseMacLen;

        nmhSetCmnNew (FsCapwapWhiteListWtpBaseMac, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %s",
                      pCapwapSetFsCapwapWhiteListEntry->MibObject.
                      u4FsCapwapWhiteListId, &FsCapwapWhiteListWtpBaseMacVal);
    }
    if (pCapwapIsSetFsCapwapWhiteListEntry->bFsCapwapWhiteListRowStatus ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsCapwapWhiteListRowStatus, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 1, 1, i4SetOption, "%u %i",
                      pCapwapSetFsCapwapWhiteListEntry->MibObject.
                      u4FsCapwapWhiteListId,
                      pCapwapSetFsCapwapWhiteListEntry->MibObject.
                      i4FsCapwapWhiteListRowStatus);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapSetAllFsCapwapBlackListTrigger
 Input       :  The Indices
                pCapwapSetFsCapwapBlackListEntry
                pCapwapIsSetFsCapwapBlackListEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
CapwapSetAllFsCapwapBlackListTrigger (tCapwapFsCapwapBlackListEntry *
                                      pCapwapSetFsCapwapBlackListEntry,
                                      tCapwapIsSetFsCapwapBlackListEntry *
                                      pCapwapIsSetFsCapwapBlackListEntry,
                                      INT4 i4SetOption)
{
    tSNMP_OCTET_STRING_TYPE FsCapwapBlackListWtpBaseMacVal;
    UINT1               au1FsCapwapBlackListWtpBaseMacVal[256];

    MEMSET (au1FsCapwapBlackListWtpBaseMacVal, 0,
            sizeof (au1FsCapwapBlackListWtpBaseMacVal));
    FsCapwapBlackListWtpBaseMacVal.pu1_OctetList =
        au1FsCapwapBlackListWtpBaseMacVal;
    FsCapwapBlackListWtpBaseMacVal.i4_Length = 0;

    if (pCapwapIsSetFsCapwapBlackListEntry->bFsCapwapBlackListId == OSIX_TRUE)
    {
        nmhSetCmnNew (FsCapwapBlackListId, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %u",
                      pCapwapSetFsCapwapBlackListEntry->MibObject.
                      u4FsCapwapBlackListId,
                      pCapwapSetFsCapwapBlackListEntry->MibObject.
                      u4FsCapwapBlackListId);
    }
    if (pCapwapIsSetFsCapwapBlackListEntry->bFsCapwapBlackListWtpBaseMac ==
        OSIX_TRUE)
    {
        MEMCPY (FsCapwapBlackListWtpBaseMacVal.pu1_OctetList,
                pCapwapSetFsCapwapBlackListEntry->MibObject.
                au1FsCapwapBlackListWtpBaseMac,
                pCapwapSetFsCapwapBlackListEntry->MibObject.
                i4FsCapwapBlackListWtpBaseMacLen);
        FsCapwapBlackListWtpBaseMacVal.i4_Length =
            pCapwapSetFsCapwapBlackListEntry->MibObject.
            i4FsCapwapBlackListWtpBaseMacLen;

        nmhSetCmnNew (FsCapwapBlackListWtpBaseMac, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %s",
                      pCapwapSetFsCapwapBlackListEntry->MibObject.
                      u4FsCapwapBlackListId, &FsCapwapBlackListWtpBaseMacVal);
    }
    if (pCapwapIsSetFsCapwapBlackListEntry->bFsCapwapBlackListRowStatus ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsCapwapBlackListRowStatus, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 1, 1, i4SetOption, "%u %i",
                      pCapwapSetFsCapwapBlackListEntry->MibObject.
                      u4FsCapwapBlackListId,
                      pCapwapSetFsCapwapBlackListEntry->MibObject.
                      i4FsCapwapBlackListRowStatus);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapSetAllFsCapwapWtpConfigTableTrigger
 Input       :  The Indices
                pCapwapSetFsCapwapWtpConfigEntry
                pCapwapIsSetFsCapwapWtpConfigEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
CapwapSetAllFsCapwapWtpConfigTableTrigger (tCapwapFsCapwapWtpConfigEntry *
                                           pCapwapSetFsCapwapWtpConfigEntry,
                                           tCapwapIsSetFsCapwapWtpConfigEntry *
                                           pCapwapIsSetFsCapwapWtpConfigEntry,
                                           INT4 i4SetOption)
{
    tSNMP_OCTET_STRING_TYPE FsWtpCountryStringVal;
    UINT1               au1FsWtpCountryStringVal[256];
    tSNMP_OCTET_STRING_TYPE FsWtpCrashDumpFileNameVal;
    UINT1               au1FsWtpCrashDumpFileNameVal[256];
    tSNMP_OCTET_STRING_TYPE FsWtpMemoryDumpFileNameVal;
    UINT1               au1FsWtpMemoryDumpFileNameVal[256];

    MEMSET (au1FsWtpCountryStringVal, 0, sizeof (au1FsWtpCountryStringVal));
    FsWtpCountryStringVal.pu1_OctetList = au1FsWtpCountryStringVal;
    FsWtpCountryStringVal.i4_Length = 0;

    MEMSET (au1FsWtpCrashDumpFileNameVal, 0,
            sizeof (au1FsWtpCrashDumpFileNameVal));
    FsWtpCrashDumpFileNameVal.pu1_OctetList = au1FsWtpCrashDumpFileNameVal;
    FsWtpCrashDumpFileNameVal.i4_Length = 0;

    MEMSET (au1FsWtpMemoryDumpFileNameVal, 0,
            sizeof (au1FsWtpMemoryDumpFileNameVal));
    FsWtpMemoryDumpFileNameVal.pu1_OctetList = au1FsWtpMemoryDumpFileNameVal;
    FsWtpMemoryDumpFileNameVal.i4_Length = 0;

    if (pCapwapIsSetFsCapwapWtpConfigEntry->bFsCapwapWtpReset == OSIX_TRUE)
    {
        nmhSetCmnNew (FsCapwapWtpReset, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %i",
                      pCapwapSetFsCapwapWtpConfigEntry->MibObject.
                      u4CapwapBaseWtpProfileId,
                      pCapwapSetFsCapwapWtpConfigEntry->MibObject.
                      i4FsCapwapWtpReset);
    }
    if (pCapwapIsSetFsCapwapWtpConfigEntry->bFsCapwapClearConfig == OSIX_TRUE)
    {
        nmhSetCmnNew (FsCapwapClearConfig, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %i",
                      pCapwapSetFsCapwapWtpConfigEntry->MibObject.
                      u4CapwapBaseWtpProfileId,
                      pCapwapSetFsCapwapWtpConfigEntry->MibObject.
                      i4FsCapwapClearConfig);
    }
    if (pCapwapIsSetFsCapwapWtpConfigEntry->bFsWtpDiscoveryType == OSIX_TRUE)
    {
        nmhSetCmnNew (FsWtpDiscoveryType, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %i",
                      pCapwapSetFsCapwapWtpConfigEntry->MibObject.
                      u4CapwapBaseWtpProfileId,
                      pCapwapSetFsCapwapWtpConfigEntry->MibObject.
                      i4FsWtpDiscoveryType);
    }
    if (pCapwapIsSetFsCapwapWtpConfigEntry->bFsWtpCountryString == OSIX_TRUE)
    {
        MEMCPY (FsWtpCountryStringVal.pu1_OctetList,
                pCapwapSetFsCapwapWtpConfigEntry->MibObject.
                au1FsWtpCountryString,
                pCapwapSetFsCapwapWtpConfigEntry->MibObject.
                i4FsWtpCountryStringLen);
        FsWtpCountryStringVal.i4_Length =
            pCapwapSetFsCapwapWtpConfigEntry->MibObject.i4FsWtpCountryStringLen;

        nmhSetCmnNew (FsWtpCountryString, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %s",
                      pCapwapSetFsCapwapWtpConfigEntry->MibObject.
                      u4CapwapBaseWtpProfileId, &FsWtpCountryStringVal);
    }
    if (pCapwapIsSetFsCapwapWtpConfigEntry->bFsWtpCrashDumpFileName ==
        OSIX_TRUE)
    {
        MEMCPY (FsWtpCrashDumpFileNameVal.pu1_OctetList,
                pCapwapSetFsCapwapWtpConfigEntry->MibObject.
                au1FsWtpCrashDumpFileName,
                pCapwapSetFsCapwapWtpConfigEntry->MibObject.
                i4FsWtpCrashDumpFileNameLen);
        FsWtpCrashDumpFileNameVal.i4_Length =
            pCapwapSetFsCapwapWtpConfigEntry->MibObject.
            i4FsWtpCrashDumpFileNameLen;

        nmhSetCmnNew (FsWtpCrashDumpFileName, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %s",
                      pCapwapSetFsCapwapWtpConfigEntry->MibObject.
                      u4CapwapBaseWtpProfileId, &FsWtpCrashDumpFileNameVal);
    }
    if (pCapwapIsSetFsCapwapWtpConfigEntry->bFsWtpMemoryDumpFileName ==
        OSIX_TRUE)
    {
        MEMCPY (FsWtpMemoryDumpFileNameVal.pu1_OctetList,
                pCapwapSetFsCapwapWtpConfigEntry->MibObject.
                au1FsWtpMemoryDumpFileName,
                pCapwapSetFsCapwapWtpConfigEntry->MibObject.
                i4FsWtpMemoryDumpFileNameLen);
        FsWtpMemoryDumpFileNameVal.i4_Length =
            pCapwapSetFsCapwapWtpConfigEntry->MibObject.
            i4FsWtpMemoryDumpFileNameLen;

        nmhSetCmnNew (FsWtpMemoryDumpFileName, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %s",
                      pCapwapSetFsCapwapWtpConfigEntry->MibObject.
                      u4CapwapBaseWtpProfileId, &FsWtpMemoryDumpFileNameVal);
    }
    if (pCapwapIsSetFsCapwapWtpConfigEntry->bFsWtpDeleteOperation == OSIX_TRUE)
    {
        nmhSetCmnNew (FsWtpDeleteOperation, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %u",
                      pCapwapSetFsCapwapWtpConfigEntry->MibObject.
                      u4FsWtpDeleteOperation,
                      pCapwapSetFsCapwapWtpConfigEntry->MibObject.
                      u4FsWtpDeleteOperation);
    }
    if (pCapwapIsSetFsCapwapWtpConfigEntry->bFsCapwapClearApStats == OSIX_TRUE)
    {
        nmhSetCmnNew (FsCapwapClearApStats, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %i",
                      pCapwapSetFsCapwapWtpConfigEntry->MibObject.
                      i4FsCapwapClearApStats,
                      pCapwapSetFsCapwapWtpConfigEntry->MibObject.
                      i4FsCapwapClearApStats);
    }
    if (pCapwapIsSetFsCapwapWtpConfigEntry->bFsCapwapWtpConfigRowStatus ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsCapwapWtpConfigRowStatus, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 1, 1, i4SetOption, "%u %i",
                      pCapwapSetFsCapwapWtpConfigEntry->MibObject.
                      u4CapwapBaseWtpProfileId,
                      pCapwapSetFsCapwapWtpConfigEntry->MibObject.
                      i4FsCapwapWtpConfigRowStatus);
    }
    if (pCapwapIsSetFsCapwapWtpConfigEntry->bCapwapBaseWtpProfileId ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (CapwapBaseWtpProfileId, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %u",
                      pCapwapSetFsCapwapWtpConfigEntry->MibObject.
                      u4CapwapBaseWtpProfileId,
                      pCapwapSetFsCapwapWtpConfigEntry->MibObject.
                      u4CapwapBaseWtpProfileId);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapSetAllFsCapwapLinkEncryptionTableTrigger
 Input       :  The Indices
                pCapwapSetFsCapwapLinkEncryptionEntry
                pCapwapIsSetFsCapwapLinkEncryptionEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    CapwapSetAllFsCapwapLinkEncryptionTableTrigger
    (tCapwapFsCapwapLinkEncryptionEntry * pCapwapSetFsCapwapLinkEncryptionEntry,
     tCapwapIsSetFsCapwapLinkEncryptionEntry *
     pCapwapIsSetFsCapwapLinkEncryptionEntry, INT4 i4SetOption)
{

    if (pCapwapIsSetFsCapwapLinkEncryptionEntry->bFsCapwapEncryptChannel ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsCapwapEncryptChannel, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %i %i",
                      pCapwapSetFsCapwapLinkEncryptionEntry->MibObject.
                      u4CapwapBaseWtpProfileId,
                      pCapwapSetFsCapwapLinkEncryptionEntry->MibObject.
                      i4FsCapwapEncryptChannel,
                      pCapwapSetFsCapwapLinkEncryptionEntry->MibObject.
                      i4FsCapwapEncryptChannel);
    }
    if (pCapwapIsSetFsCapwapLinkEncryptionEntry->
        bFsCapwapEncryptChannelStatus == OSIX_TRUE)
    {
        nmhSetCmnNew (FsCapwapEncryptChannelStatus, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %i %i",
                      pCapwapSetFsCapwapLinkEncryptionEntry->MibObject.
                      u4CapwapBaseWtpProfileId,
                      pCapwapSetFsCapwapLinkEncryptionEntry->MibObject.
                      i4FsCapwapEncryptChannel,
                      pCapwapSetFsCapwapLinkEncryptionEntry->MibObject.
                      i4FsCapwapEncryptChannelStatus);
    }
    if (pCapwapIsSetFsCapwapLinkEncryptionEntry->
        bFsCapwapEncryptChannelRowStatus == OSIX_TRUE)
    {
        nmhSetCmnNew (FsCapwapEncryptChannelRowStatus, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 1, 2, i4SetOption, "%u %i %i",
                      pCapwapSetFsCapwapLinkEncryptionEntry->MibObject.
                      u4CapwapBaseWtpProfileId,
                      pCapwapSetFsCapwapLinkEncryptionEntry->MibObject.
                      i4FsCapwapEncryptChannel,
                      pCapwapSetFsCapwapLinkEncryptionEntry->MibObject.
                      i4FsCapwapEncryptChannelRowStatus);
    }
    if (pCapwapIsSetFsCapwapLinkEncryptionEntry->bCapwapBaseWtpProfileId ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (CapwapBaseWtpProfileId, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %i %u",
                      pCapwapSetFsCapwapLinkEncryptionEntry->MibObject.
                      u4CapwapBaseWtpProfileId,
                      pCapwapSetFsCapwapLinkEncryptionEntry->MibObject.
                      i4FsCapwapEncryptChannel,
                      pCapwapSetFsCapwapLinkEncryptionEntry->MibObject.
                      u4CapwapBaseWtpProfileId);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapSetAllFsCawapDefaultWtpProfileTableTrigger
 Input       :  The Indices
                pCapwapSetFsCapwapDefaultWtpProfileEntry
                pCapwapIsSetFsCapwapDefaultWtpProfileEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    CapwapSetAllFsCawapDefaultWtpProfileTableTrigger
    (tCapwapFsCapwapDefaultWtpProfileEntry *
     pCapwapSetFsCapwapDefaultWtpProfileEntry,
     tCapwapIsSetFsCapwapDefaultWtpProfileEntry *
     pCapwapIsSetFsCapwapDefaultWtpProfileEntry, INT4 i4SetOption)
{
    tSNMP_OCTET_STRING_TYPE FsCapwapDefaultWtpProfileModelNumberVal;
    UINT1               au1FsCapwapDefaultWtpProfileModelNumberVal[256];

    MEMSET (au1FsCapwapDefaultWtpProfileModelNumberVal, 0,
            sizeof (au1FsCapwapDefaultWtpProfileModelNumberVal));
    FsCapwapDefaultWtpProfileModelNumberVal.pu1_OctetList =
        au1FsCapwapDefaultWtpProfileModelNumberVal;
    FsCapwapDefaultWtpProfileModelNumberVal.i4_Length = 0;

    if (pCapwapIsSetFsCapwapDefaultWtpProfileEntry->
        bFsCapwapDefaultWtpProfileRowStatus == OSIX_TRUE)
    {
        nmhSetCmnNew (FsCapwapDefaultWtpProfileRowStatus, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 1, 1, i4SetOption, "%s %i",
                      &FsCapwapDefaultWtpProfileModelNumberVal,
                      pCapwapSetFsCapwapDefaultWtpProfileEntry->MibObject.
                      i4FsCapwapDefaultWtpProfileRowStatus);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapSetAllFsCapwapDnsProfileTableTrigger
 Input       :  The Indices
                pCapwapSetFsCapwapDnsProfileEntry
                pCapwapIsSetFsCapwapDnsProfileEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
CapwapSetAllFsCapwapDnsProfileTableTrigger (tCapwapFsCapwapDnsProfileEntry *
                                            pCapwapSetFsCapwapDnsProfileEntry,
                                            tCapwapIsSetFsCapwapDnsProfileEntry
                                            *
                                            pCapwapIsSetFsCapwapDnsProfileEntry,
                                            INT4 i4SetOption)
{
    tSNMP_OCTET_STRING_TYPE FsCapwapDnsServerIpVal;
    UINT1               au1FsCapwapDnsServerIpVal[256];
    tSNMP_OCTET_STRING_TYPE FsCapwapDnsDomainNameVal;
    UINT1               au1FsCapwapDnsDomainNameVal[256];

    MEMSET (au1FsCapwapDnsServerIpVal, 0, sizeof (au1FsCapwapDnsServerIpVal));
    FsCapwapDnsServerIpVal.pu1_OctetList = au1FsCapwapDnsServerIpVal;
    FsCapwapDnsServerIpVal.i4_Length = 0;

    MEMSET (au1FsCapwapDnsDomainNameVal, 0,
            sizeof (au1FsCapwapDnsDomainNameVal));
    FsCapwapDnsDomainNameVal.pu1_OctetList = au1FsCapwapDnsDomainNameVal;
    FsCapwapDnsDomainNameVal.i4_Length = 0;

    if (pCapwapIsSetFsCapwapDnsProfileEntry->bFsCapwapDnsServerIp == OSIX_TRUE)
    {
        MEMCPY (FsCapwapDnsServerIpVal.pu1_OctetList,
                pCapwapSetFsCapwapDnsProfileEntry->MibObject.
                au1FsCapwapDnsServerIp,
                pCapwapSetFsCapwapDnsProfileEntry->MibObject.
                i4FsCapwapDnsServerIpLen);
        FsCapwapDnsServerIpVal.i4_Length =
            pCapwapSetFsCapwapDnsProfileEntry->MibObject.
            i4FsCapwapDnsServerIpLen;

        nmhSetCmnNew (FsCapwapDnsServerIp, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %s",
                      pCapwapSetFsCapwapDnsProfileEntry->MibObject.
                      u4CapwapBaseWtpProfileId, &FsCapwapDnsServerIpVal);
    }
    if (pCapwapIsSetFsCapwapDnsProfileEntry->bFsCapwapDnsDomainName ==
        OSIX_TRUE)
    {
        MEMCPY (FsCapwapDnsDomainNameVal.pu1_OctetList,
                pCapwapSetFsCapwapDnsProfileEntry->MibObject.
                au1FsCapwapDnsDomainName,
                pCapwapSetFsCapwapDnsProfileEntry->MibObject.
                i4FsCapwapDnsDomainNameLen);
        FsCapwapDnsDomainNameVal.i4_Length =
            pCapwapSetFsCapwapDnsProfileEntry->MibObject.
            i4FsCapwapDnsDomainNameLen;

        nmhSetCmnNew (FsCapwapDnsDomainName, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %s",
                      pCapwapSetFsCapwapDnsProfileEntry->MibObject.
                      u4CapwapBaseWtpProfileId, &FsCapwapDnsDomainNameVal);
    }
    if (pCapwapIsSetFsCapwapDnsProfileEntry->bFsCapwapDnsProfileRowStatus ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsCapwapDnsProfileRowStatus, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 1, 1, i4SetOption, "%u %i",
                      pCapwapSetFsCapwapDnsProfileEntry->MibObject.
                      u4CapwapBaseWtpProfileId,
                      pCapwapSetFsCapwapDnsProfileEntry->MibObject.
                      i4FsCapwapDnsProfileRowStatus);
    }
    if (pCapwapIsSetFsCapwapDnsProfileEntry->bCapwapBaseWtpProfileId ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (CapwapBaseWtpProfileId, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %u",
                      pCapwapSetFsCapwapDnsProfileEntry->MibObject.
                      u4CapwapBaseWtpProfileId,
                      pCapwapSetFsCapwapDnsProfileEntry->MibObject.
                      u4CapwapBaseWtpProfileId);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapSetAllFsWtpNativeVlanIdTableTrigger
 Input       :  The Indices
                pCapwapSetFsWtpNativeVlanIdTable
                pCapwapIsSetFsWtpNativeVlanIdTable
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
CapwapSetAllFsWtpNativeVlanIdTableTrigger (tCapwapFsWtpNativeVlanIdTable *
                                           pCapwapSetFsWtpNativeVlanIdTable,
                                           tCapwapIsSetFsWtpNativeVlanIdTable *
                                           pCapwapIsSetFsWtpNativeVlanIdTable,
                                           INT4 i4SetOption)
{

    if (pCapwapIsSetFsWtpNativeVlanIdTable->bFsWtpNativeVlanId == OSIX_TRUE)
    {
        nmhSetCmnNew (FsWtpNativeVlanId, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %i",
                      pCapwapSetFsWtpNativeVlanIdTable->MibObject.
                      u4CapwapBaseWtpProfileId,
                      pCapwapSetFsWtpNativeVlanIdTable->MibObject.
                      i4FsWtpNativeVlanId);
    }
    if (pCapwapIsSetFsWtpNativeVlanIdTable->bFsWtpNativeVlanIdRowStatus ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsWtpNativeVlanIdRowStatus, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 1, 1, i4SetOption, "%u %i",
                      pCapwapSetFsWtpNativeVlanIdTable->MibObject.
                      u4CapwapBaseWtpProfileId,
                      pCapwapSetFsWtpNativeVlanIdTable->MibObject.
                      i4FsWtpNativeVlanIdRowStatus);
    }
    if (pCapwapIsSetFsWtpNativeVlanIdTable->bCapwapBaseWtpProfileId ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (CapwapBaseWtpProfileId, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %u",
                      pCapwapSetFsWtpNativeVlanIdTable->MibObject.
                      u4CapwapBaseWtpProfileId,
                      pCapwapSetFsWtpNativeVlanIdTable->MibObject.
                      u4CapwapBaseWtpProfileId);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapSetAllFsWtpLocalRoutingTableTrigger
 Input       :  The Indices
                pCapwapSetFsWtpLocalRoutingTable
                pCapwapIsSetFsWtpLocalRoutingTable
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
CapwapSetAllFsWtpLocalRoutingTableTrigger (tCapwapFsWtpLocalRoutingTable *
                                           pCapwapSetFsWtpLocalRoutingTable,
                                           tCapwapIsSetFsWtpLocalRoutingTable *
                                           pCapwapIsSetFsWtpLocalRoutingTable,
                                           INT4 i4SetOption)
{

    if (pCapwapIsSetFsWtpLocalRoutingTable->bFsWtpLocalRouting == OSIX_TRUE)
    {
        nmhSetCmnNew (FsWtpLocalRouting, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %i",
                      pCapwapSetFsWtpLocalRoutingTable->MibObject.
                      u4CapwapBaseWtpProfileId,
                      pCapwapSetFsWtpLocalRoutingTable->MibObject.
                      i4FsWtpLocalRouting);
    }
    if (pCapwapIsSetFsWtpLocalRoutingTable->bCapwapBaseWtpProfileId ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (CapwapBaseWtpProfileId, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %u",
                      pCapwapSetFsWtpLocalRoutingTable->MibObject.
                      u4CapwapBaseWtpProfileId,
                      pCapwapSetFsWtpLocalRoutingTable->MibObject.
                      u4CapwapBaseWtpProfileId);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapSetAllFsCawapDiscStatsTableTrigger
 Input       :  The Indices
                pCapwapSetFsCawapDiscStatsEntry
                pCapwapIsSetFsCawapDiscStatsEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
CapwapSetAllFsCawapDiscStatsTableTrigger (tCapwapFsCawapDiscStatsEntry *
                                          pCapwapSetFsCawapDiscStatsEntry,
                                          tCapwapIsSetFsCawapDiscStatsEntry *
                                          pCapwapIsSetFsCawapDiscStatsEntry,
                                          INT4 i4SetOption)
{

    if (pCapwapIsSetFsCawapDiscStatsEntry->bFsCapwapDiscStatsRowStatus ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsCapwapDiscStatsRowStatus, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 1, 1, i4SetOption, "%u %i",
                      pCapwapSetFsCawapDiscStatsEntry->MibObject.
                      u4CapwapBaseWtpProfileId,
                      pCapwapSetFsCawapDiscStatsEntry->MibObject.
                      i4FsCapwapDiscStatsRowStatus);
    }
    if (pCapwapIsSetFsCawapDiscStatsEntry->bCapwapBaseWtpProfileId == OSIX_TRUE)
    {
        nmhSetCmnNew (CapwapBaseWtpProfileId, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %u",
                      pCapwapSetFsCawapDiscStatsEntry->MibObject.
                      u4CapwapBaseWtpProfileId,
                      pCapwapSetFsCawapDiscStatsEntry->MibObject.
                      u4CapwapBaseWtpProfileId);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapSetAllFsCawapJoinStatsTableTrigger
 Input       :  The Indices
                pCapwapSetFsCawapJoinStatsEntry
                pCapwapIsSetFsCawapJoinStatsEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
CapwapSetAllFsCawapJoinStatsTableTrigger (tCapwapFsCawapJoinStatsEntry *
                                          pCapwapSetFsCawapJoinStatsEntry,
                                          tCapwapIsSetFsCawapJoinStatsEntry *
                                          pCapwapIsSetFsCawapJoinStatsEntry,
                                          INT4 i4SetOption)
{

    if (pCapwapIsSetFsCawapJoinStatsEntry->bFsCapwapJoinStatsRowStatus ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsCapwapJoinStatsRowStatus, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 1, 1, i4SetOption, "%u %i",
                      pCapwapSetFsCawapJoinStatsEntry->MibObject.
                      u4CapwapBaseWtpProfileId,
                      pCapwapSetFsCawapJoinStatsEntry->MibObject.
                      i4FsCapwapJoinStatsRowStatus);
    }
    if (pCapwapIsSetFsCawapJoinStatsEntry->bCapwapBaseWtpProfileId == OSIX_TRUE)
    {
        nmhSetCmnNew (CapwapBaseWtpProfileId, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %u",
                      pCapwapSetFsCawapJoinStatsEntry->MibObject.
                      u4CapwapBaseWtpProfileId,
                      pCapwapSetFsCawapJoinStatsEntry->MibObject.
                      u4CapwapBaseWtpProfileId);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapSetAllFsCawapConfigStatsTableTrigger
 Input       :  The Indices
                pCapwapSetFsCawapConfigStatsEntry
                pCapwapIsSetFsCawapConfigStatsEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
CapwapSetAllFsCawapConfigStatsTableTrigger (tCapwapFsCawapConfigStatsEntry *
                                            pCapwapSetFsCawapConfigStatsEntry,
                                            tCapwapIsSetFsCawapConfigStatsEntry
                                            *
                                            pCapwapIsSetFsCawapConfigStatsEntry,
                                            INT4 i4SetOption)
{

    if (pCapwapIsSetFsCawapConfigStatsEntry->bFsCapwapConfigStatsRowStatus ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsCapwapConfigStatsRowStatus, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 1, 1, i4SetOption, "%u %i",
                      pCapwapSetFsCawapConfigStatsEntry->MibObject.
                      u4CapwapBaseWtpProfileId,
                      pCapwapSetFsCawapConfigStatsEntry->MibObject.
                      i4FsCapwapConfigStatsRowStatus);
    }
    if (pCapwapIsSetFsCawapConfigStatsEntry->bCapwapBaseWtpProfileId ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (CapwapBaseWtpProfileId, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %u",
                      pCapwapSetFsCawapConfigStatsEntry->MibObject.
                      u4CapwapBaseWtpProfileId,
                      pCapwapSetFsCawapConfigStatsEntry->MibObject.
                      u4CapwapBaseWtpProfileId);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapSetAllFsCawapRunStatsTableTrigger
 Input       :  The Indices
                pCapwapSetFsCawapRunStatsEntry
                pCapwapIsSetFsCawapRunStatsEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
CapwapSetAllFsCawapRunStatsTableTrigger (tCapwapFsCawapRunStatsEntry *
                                         pCapwapSetFsCawapRunStatsEntry,
                                         tCapwapIsSetFsCawapRunStatsEntry *
                                         pCapwapIsSetFsCawapRunStatsEntry,
                                         INT4 i4SetOption)
{

    if (pCapwapIsSetFsCawapRunStatsEntry->bFsCapwapRunStatsRowStatus ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsCapwapRunStatsRowStatus, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 1, 1, i4SetOption, "%u %i",
                      pCapwapSetFsCawapRunStatsEntry->MibObject.
                      u4CapwapBaseWtpProfileId,
                      pCapwapSetFsCawapRunStatsEntry->MibObject.
                      i4FsCapwapRunStatsRowStatus);
    }
    if (pCapwapIsSetFsCawapRunStatsEntry->bCapwapBaseWtpProfileId == OSIX_TRUE)
    {
        nmhSetCmnNew (CapwapBaseWtpProfileId, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %u",
                      pCapwapSetFsCawapRunStatsEntry->MibObject.
                      u4CapwapBaseWtpProfileId,
                      pCapwapSetFsCawapRunStatsEntry->MibObject.
                      u4CapwapBaseWtpProfileId);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapSetAllFsCapwapWirelessBindingTableTrigger
 Input       :  The Indices
                pCapwapSetFsCapwapWirelessBindingEntry
                pCapwapIsSetFsCapwapWirelessBindingEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    CapwapSetAllFsCapwapWirelessBindingTableTrigger
    (tCapwapFsCapwapWirelessBindingEntry *
     pCapwapSetFsCapwapWirelessBindingEntry,
     tCapwapIsSetFsCapwapWirelessBindingEntry *
     pCapwapIsSetFsCapwapWirelessBindingEntry, INT4 i4SetOption)
{

    if (pCapwapIsSetFsCapwapWirelessBindingEntry->
        bFsCapwapWirelessBindingVirtualRadioIfIndex == OSIX_TRUE)
    {
        nmhSetCmnNew (FsCapwapWirelessBindingVirtualRadioIfIndex, 0,
                      CapwapMainTaskLock, CapwapMainTaskUnLock, 0, 0, 2,
                      i4SetOption, "%u %u %i",
                      pCapwapSetFsCapwapWirelessBindingEntry->MibObject.
                      u4CapwapBaseWtpProfileId,
                      pCapwapSetFsCapwapWirelessBindingEntry->MibObject.
                      u4CapwapBaseWirelessBindingRadioId,
                      pCapwapSetFsCapwapWirelessBindingEntry->MibObject.
                      i4FsCapwapWirelessBindingVirtualRadioIfIndex);
    }
    if (pCapwapIsSetFsCapwapWirelessBindingEntry->
        bFsCapwapWirelessBindingType == OSIX_TRUE)
    {
        nmhSetCmnNew (FsCapwapWirelessBindingType, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %u %i",
                      pCapwapSetFsCapwapWirelessBindingEntry->MibObject.
                      u4CapwapBaseWtpProfileId,
                      pCapwapSetFsCapwapWirelessBindingEntry->MibObject.
                      u4CapwapBaseWirelessBindingRadioId,
                      pCapwapSetFsCapwapWirelessBindingEntry->MibObject.
                      i4FsCapwapWirelessBindingType);
    }
    if (pCapwapIsSetFsCapwapWirelessBindingEntry->
        bFsCapwapWirelessBindingRowStatus == OSIX_TRUE)
    {
        nmhSetCmnNew (FsCapwapWirelessBindingRowStatus, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 1, 2, i4SetOption, "%u %u %i",
                      pCapwapSetFsCapwapWirelessBindingEntry->MibObject.
                      u4CapwapBaseWtpProfileId,
                      pCapwapSetFsCapwapWirelessBindingEntry->MibObject.
                      u4CapwapBaseWirelessBindingRadioId,
                      pCapwapSetFsCapwapWirelessBindingEntry->MibObject.
                      i4FsCapwapWirelessBindingRowStatus);
    }
    if (pCapwapIsSetFsCapwapWirelessBindingEntry->bCapwapBaseWtpProfileId ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (CapwapBaseWtpProfileId, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %u %u",
                      pCapwapSetFsCapwapWirelessBindingEntry->MibObject.
                      u4CapwapBaseWtpProfileId,
                      pCapwapSetFsCapwapWirelessBindingEntry->MibObject.
                      u4CapwapBaseWirelessBindingRadioId,
                      pCapwapSetFsCapwapWirelessBindingEntry->MibObject.
                      u4CapwapBaseWtpProfileId);
    }
    if (pCapwapIsSetFsCapwapWirelessBindingEntry->
        bCapwapBaseWirelessBindingRadioId == OSIX_TRUE)
    {
        nmhSetCmnNew (CapwapBaseWirelessBindingRadioId, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %u %u",
                      pCapwapSetFsCapwapWirelessBindingEntry->MibObject.
                      u4CapwapBaseWtpProfileId,
                      pCapwapSetFsCapwapWirelessBindingEntry->MibObject.
                      u4CapwapBaseWirelessBindingRadioId,
                      pCapwapSetFsCapwapWirelessBindingEntry->MibObject.
                      u4CapwapBaseWirelessBindingRadioId);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapSetAllFsCapwapStationWhiteListTrigger
 Input       :  The Indices
                pCapwapSetFsCapwapStationWhiteListEntry
                pCapwapIsSetFsCapwapStationWhiteListEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    CapwapSetAllFsCapwapStationWhiteListTrigger
    (tCapwapFsCapwapStationWhiteListEntry *
     pCapwapSetFsCapwapStationWhiteListEntry,
     tCapwapIsSetFsCapwapStationWhiteListEntry *
     pCapwapIsSetFsCapwapStationWhiteListEntry, INT4 i4SetOption)
{
    tSNMP_OCTET_STRING_TYPE FsCapwapStationWhiteListStationIdVal;
    UINT1               au1FsCapwapStationWhiteListStationIdVal[256];

    MEMSET (au1FsCapwapStationWhiteListStationIdVal, 0,
            sizeof (au1FsCapwapStationWhiteListStationIdVal));
    FsCapwapStationWhiteListStationIdVal.pu1_OctetList =
        au1FsCapwapStationWhiteListStationIdVal;
    FsCapwapStationWhiteListStationIdVal.i4_Length = 0;

    if (pCapwapIsSetFsCapwapStationWhiteListEntry->
        bFsCapwapStationWhiteListStationId == OSIX_TRUE)
    {
        MEMCPY (FsCapwapStationWhiteListStationIdVal.pu1_OctetList,
                pCapwapSetFsCapwapStationWhiteListEntry->MibObject.
                au1FsCapwapStationWhiteListStationId,
                pCapwapSetFsCapwapStationWhiteListEntry->MibObject.
                i4FsCapwapStationWhiteListStationIdLen);
        FsCapwapStationWhiteListStationIdVal.i4_Length =
            pCapwapSetFsCapwapStationWhiteListEntry->MibObject.
            i4FsCapwapStationWhiteListStationIdLen;

        nmhSetCmnNew (FsCapwapStationWhiteListStationId, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %s",
                      pCapwapSetFsCapwapStationWhiteListEntry->MibObject.
                      u4FsCapwapStationWhiteListId,
                      &FsCapwapStationWhiteListStationIdVal);
    }
    if (pCapwapIsSetFsCapwapStationWhiteListEntry->
        bFsCapwapStationWhiteListRowStatus == OSIX_TRUE)
    {
        nmhSetCmnNew (FsCapwapStationWhiteListRowStatus, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 1, 1, i4SetOption, "%u %i",
                      pCapwapSetFsCapwapStationWhiteListEntry->MibObject.
                      u4FsCapwapStationWhiteListId,
                      pCapwapSetFsCapwapStationWhiteListEntry->MibObject.
                      i4FsCapwapStationWhiteListRowStatus);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapSetAllFsCapwapWtpRebootStatisticsTableTrigger
 Input       :  The Indices
                pCapwapSetFsCapwapWtpRebootStatisticsEntry
                pCapwapIsSetFsCapwapWtpRebootStatisticsEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    CapwapSetAllFsCapwapWtpRebootStatisticsTableTrigger
    (tCapwapFsCapwapWtpRebootStatisticsEntry *
     pCapwapSetFsCapwapWtpRebootStatisticsEntry,
     tCapwapIsSetFsCapwapWtpRebootStatisticsEntry *
     pCapwapIsSetFsCapwapWtpRebootStatisticsEntry, INT4 i4SetOption)
{

    if (pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
        bFsCapwapWtpRebootStatisticsRebootCount == OSIX_TRUE)
    {
        nmhSetCmnNew (FsCapwapWtpRebootStatisticsRebootCount, 0,
                      CapwapMainTaskLock, CapwapMainTaskUnLock, 0, 0, 1,
                      i4SetOption, "%u %u",
                      pCapwapSetFsCapwapWtpRebootStatisticsEntry->MibObject.
                      u4CapwapBaseWtpProfileId,
                      pCapwapSetFsCapwapWtpRebootStatisticsEntry->MibObject.
                      u4FsCapwapWtpRebootStatisticsRebootCount);
    }
    if (pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
        bFsCapwapWtpRebootStatisticsAcInitiatedCount == OSIX_TRUE)
    {
        nmhSetCmnNew (FsCapwapWtpRebootStatisticsAcInitiatedCount, 0,
                      CapwapMainTaskLock, CapwapMainTaskUnLock, 0, 0, 1,
                      i4SetOption, "%u %u",
                      pCapwapSetFsCapwapWtpRebootStatisticsEntry->MibObject.
                      u4CapwapBaseWtpProfileId,
                      pCapwapSetFsCapwapWtpRebootStatisticsEntry->MibObject.
                      u4FsCapwapWtpRebootStatisticsAcInitiatedCount);
    }
    if (pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
        bFsCapwapWtpRebootStatisticsLinkFailureCount == OSIX_TRUE)
    {
        nmhSetCmnNew (FsCapwapWtpRebootStatisticsLinkFailureCount, 0,
                      CapwapMainTaskLock, CapwapMainTaskUnLock, 0, 0, 1,
                      i4SetOption, "%u %u",
                      pCapwapSetFsCapwapWtpRebootStatisticsEntry->MibObject.
                      u4CapwapBaseWtpProfileId,
                      pCapwapSetFsCapwapWtpRebootStatisticsEntry->MibObject.
                      u4FsCapwapWtpRebootStatisticsLinkFailureCount);
    }
    if (pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
        bFsCapwapWtpRebootStatisticsSwFailureCount == OSIX_TRUE)
    {
        nmhSetCmnNew (FsCapwapWtpRebootStatisticsSwFailureCount, 0,
                      CapwapMainTaskLock, CapwapMainTaskUnLock, 0, 0, 1,
                      i4SetOption, "%u %u",
                      pCapwapSetFsCapwapWtpRebootStatisticsEntry->MibObject.
                      u4CapwapBaseWtpProfileId,
                      pCapwapSetFsCapwapWtpRebootStatisticsEntry->MibObject.
                      u4FsCapwapWtpRebootStatisticsSwFailureCount);
    }
    if (pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
        bFsCapwapWtpRebootStatisticsHwFailureCount == OSIX_TRUE)
    {
        nmhSetCmnNew (FsCapwapWtpRebootStatisticsHwFailureCount, 0,
                      CapwapMainTaskLock, CapwapMainTaskUnLock, 0, 0, 1,
                      i4SetOption, "%u %u",
                      pCapwapSetFsCapwapWtpRebootStatisticsEntry->MibObject.
                      u4CapwapBaseWtpProfileId,
                      pCapwapSetFsCapwapWtpRebootStatisticsEntry->MibObject.
                      u4FsCapwapWtpRebootStatisticsHwFailureCount);
    }
    if (pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
        bFsCapwapWtpRebootStatisticsOtherFailureCount == OSIX_TRUE)
    {
        nmhSetCmnNew (FsCapwapWtpRebootStatisticsOtherFailureCount, 0,
                      CapwapMainTaskLock, CapwapMainTaskUnLock, 0, 0, 1,
                      i4SetOption, "%u %u",
                      pCapwapSetFsCapwapWtpRebootStatisticsEntry->MibObject.
                      u4CapwapBaseWtpProfileId,
                      pCapwapSetFsCapwapWtpRebootStatisticsEntry->MibObject.
                      u4FsCapwapWtpRebootStatisticsOtherFailureCount);
    }
    if (pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
        bFsCapwapWtpRebootStatisticsUnknownFailureCount == OSIX_TRUE)
    {
        nmhSetCmnNew (FsCapwapWtpRebootStatisticsUnknownFailureCount, 0,
                      CapwapMainTaskLock, CapwapMainTaskUnLock, 0, 0, 1,
                      i4SetOption, "%u %u",
                      pCapwapSetFsCapwapWtpRebootStatisticsEntry->MibObject.
                      u4CapwapBaseWtpProfileId,
                      pCapwapSetFsCapwapWtpRebootStatisticsEntry->MibObject.
                      u4FsCapwapWtpRebootStatisticsUnknownFailureCount);
    }
    if (pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
        bFsCapwapWtpRebootStatisticsLastFailureType == OSIX_TRUE)
    {
        nmhSetCmnNew (FsCapwapWtpRebootStatisticsLastFailureType, 0,
                      CapwapMainTaskLock, CapwapMainTaskUnLock, 0, 0, 1,
                      i4SetOption, "%u %i",
                      pCapwapSetFsCapwapWtpRebootStatisticsEntry->MibObject.
                      u4CapwapBaseWtpProfileId,
                      pCapwapSetFsCapwapWtpRebootStatisticsEntry->MibObject.
                      i4FsCapwapWtpRebootStatisticsLastFailureType);
    }
    if (pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
        bFsCapwapWtpRebootStatisticsRowStatus == OSIX_TRUE)
    {
        nmhSetCmnNew (FsCapwapWtpRebootStatisticsRowStatus, 0,
                      CapwapMainTaskLock, CapwapMainTaskUnLock, 0, 1, 1,
                      i4SetOption, "%u %i",
                      pCapwapSetFsCapwapWtpRebootStatisticsEntry->MibObject.
                      u4CapwapBaseWtpProfileId,
                      pCapwapSetFsCapwapWtpRebootStatisticsEntry->MibObject.
                      i4FsCapwapWtpRebootStatisticsRowStatus);
    }
    if (pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->bCapwapBaseWtpProfileId ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (CapwapBaseWtpProfileId, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %u",
                      pCapwapSetFsCapwapWtpRebootStatisticsEntry->MibObject.
                      u4CapwapBaseWtpProfileId,
                      pCapwapSetFsCapwapWtpRebootStatisticsEntry->MibObject.
                      u4CapwapBaseWtpProfileId);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapSetAllFsCapwapWtpRadioStatisticsTableTrigger
 Input       :  The Indices
                pCapwapSetFsCapwapWtpRadioStatisticsEntry
                pCapwapIsSetFsCapwapWtpRadioStatisticsEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    CapwapSetAllFsCapwapWtpRadioStatisticsTableTrigger
    (tCapwapFsCapwapWtpRadioStatisticsEntry *
     pCapwapSetFsCapwapWtpRadioStatisticsEntry,
     tCapwapIsSetFsCapwapWtpRadioStatisticsEntry *
     pCapwapIsSetFsCapwapWtpRadioStatisticsEntry, INT4 i4SetOption)
{

    if (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
        bFsCapwapWtpRadioLastFailType == OSIX_TRUE)
    {
        nmhSetCmnNew (FsCapwapWtpRadioLastFailType, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %i",
                      pCapwapSetFsCapwapWtpRadioStatisticsEntry->MibObject.
                      i4RadioIfIndex,
                      pCapwapSetFsCapwapWtpRadioStatisticsEntry->MibObject.
                      i4FsCapwapWtpRadioLastFailType);
    }
    if (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
        bFsCapwapWtpRadioResetCount == OSIX_TRUE)
    {
        nmhSetCmnNew (FsCapwapWtpRadioResetCount, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %u",
                      pCapwapSetFsCapwapWtpRadioStatisticsEntry->MibObject.
                      i4RadioIfIndex,
                      pCapwapSetFsCapwapWtpRadioStatisticsEntry->MibObject.
                      u4FsCapwapWtpRadioResetCount);
    }
    if (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
        bFsCapwapWtpRadioSwFailureCount == OSIX_TRUE)
    {
        nmhSetCmnNew (FsCapwapWtpRadioSwFailureCount, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %u",
                      pCapwapSetFsCapwapWtpRadioStatisticsEntry->MibObject.
                      i4RadioIfIndex,
                      pCapwapSetFsCapwapWtpRadioStatisticsEntry->MibObject.
                      u4FsCapwapWtpRadioSwFailureCount);
    }
    if (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
        bFsCapwapWtpRadioHwFailureCount == OSIX_TRUE)
    {
        nmhSetCmnNew (FsCapwapWtpRadioHwFailureCount, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %u",
                      pCapwapSetFsCapwapWtpRadioStatisticsEntry->MibObject.
                      i4RadioIfIndex,
                      pCapwapSetFsCapwapWtpRadioStatisticsEntry->MibObject.
                      u4FsCapwapWtpRadioHwFailureCount);
    }
    if (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
        bFsCapwapWtpRadioOtherFailureCount == OSIX_TRUE)
    {
        nmhSetCmnNew (FsCapwapWtpRadioOtherFailureCount, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %u",
                      pCapwapSetFsCapwapWtpRadioStatisticsEntry->MibObject.
                      i4RadioIfIndex,
                      pCapwapSetFsCapwapWtpRadioStatisticsEntry->MibObject.
                      u4FsCapwapWtpRadioOtherFailureCount);
    }
    if (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
        bFsCapwapWtpRadioUnknownFailureCount == OSIX_TRUE)
    {
        nmhSetCmnNew (FsCapwapWtpRadioUnknownFailureCount, 0,
                      CapwapMainTaskLock, CapwapMainTaskUnLock, 0, 0, 1,
                      i4SetOption, "%u %u",
                      pCapwapSetFsCapwapWtpRadioStatisticsEntry->MibObject.
                      i4RadioIfIndex,
                      pCapwapSetFsCapwapWtpRadioStatisticsEntry->MibObject.
                      u4FsCapwapWtpRadioUnknownFailureCount);
    }
    if (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
        bFsCapwapWtpRadioConfigUpdateCount == OSIX_TRUE)
    {
        nmhSetCmnNew (FsCapwapWtpRadioConfigUpdateCount, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %u",
                      pCapwapSetFsCapwapWtpRadioStatisticsEntry->MibObject.
                      i4RadioIfIndex,
                      pCapwapSetFsCapwapWtpRadioStatisticsEntry->MibObject.
                      u4FsCapwapWtpRadioConfigUpdateCount);
    }
    if (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
        bFsCapwapWtpRadioChannelChangeCount == OSIX_TRUE)
    {
        nmhSetCmnNew (FsCapwapWtpRadioChannelChangeCount, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %u",
                      pCapwapSetFsCapwapWtpRadioStatisticsEntry->MibObject.
                      i4RadioIfIndex,
                      pCapwapSetFsCapwapWtpRadioStatisticsEntry->MibObject.
                      u4FsCapwapWtpRadioChannelChangeCount);
    }
    if (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
        bFsCapwapWtpRadioBandChangeCount == OSIX_TRUE)
    {
        nmhSetCmnNew (FsCapwapWtpRadioBandChangeCount, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %u",
                      pCapwapSetFsCapwapWtpRadioStatisticsEntry->MibObject.
                      i4RadioIfIndex,
                      pCapwapSetFsCapwapWtpRadioStatisticsEntry->MibObject.
                      u4FsCapwapWtpRadioBandChangeCount);
    }
    if (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
        bFsCapwapWtpRadioCurrentNoiseFloor == OSIX_TRUE)
    {
        nmhSetCmnNew (FsCapwapWtpRadioCurrentNoiseFloor, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %u",
                      pCapwapSetFsCapwapWtpRadioStatisticsEntry->MibObject.
                      i4RadioIfIndex,
                      pCapwapSetFsCapwapWtpRadioStatisticsEntry->MibObject.
                      u4FsCapwapWtpRadioCurrentNoiseFloor);
    }
    if (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
        bFsCapwapWtpRadioStatRowStatus == OSIX_TRUE)
    {
        nmhSetCmnNew (FsCapwapWtpRadioStatRowStatus, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %u",
                      pCapwapSetFsCapwapWtpRadioStatisticsEntry->MibObject.
                      i4RadioIfIndex,
                      pCapwapSetFsCapwapWtpRadioStatisticsEntry->MibObject.
                      u4FsCapwapWtpRadioStatRowStatus);
    }
    if (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->bRadioIfIndex == OSIX_TRUE)
    {
        nmhSetCmnNew (CapwapBaseWtpProfileId, 0, CapwapMainTaskLock,
                      CapwapMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %u",
                      pCapwapSetFsCapwapWtpRadioStatisticsEntry->MibObject.
                      i4RadioIfIndex,
                      pCapwapSetFsCapwapWtpRadioStatisticsEntry->MibObject.
                      i4RadioIfIndex);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapCapwapBaseAcNameListTableCreateApi
 Input       :  pCapwapCapwapBaseAcNameListEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tCapwapCapwapBaseAcNameListEntry *
CapwapCapwapBaseAcNameListTableCreateApi (tCapwapCapwapBaseAcNameListEntry *
                                          pSetCapwapCapwapBaseAcNameListEntry)
{
    tCapwapCapwapBaseAcNameListEntry *pCapwapCapwapBaseAcNameListEntry = NULL;

    if (pSetCapwapCapwapBaseAcNameListEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapCapwapBaseAcNameListTableCreatApi: pSetCapwapCapwapBaseAcNameListEntry is NULL.\r\n");
        return NULL;
    }
    /* Allocate memory for the new node */
    pCapwapCapwapBaseAcNameListEntry =
        (tCapwapCapwapBaseAcNameListEntry *)
        MemAllocMemBlk (CAPWAP_CAPWAPBASEACNAMELISTTABLE_POOLID);
    if (pCapwapCapwapBaseAcNameListEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapCapwapBaseAcNameListTableCreatApi: Fail to Allocate Memory.\r\n");
        return NULL;
    }
    else
    {
        MEMCPY (pCapwapCapwapBaseAcNameListEntry,
                pSetCapwapCapwapBaseAcNameListEntry,
                sizeof (tCapwapCapwapBaseAcNameListEntry));
        if (RBTreeAdd
            (gCapwapGlobals.CapwapGlbMib.CapwapBaseAcNameListTable,
             (tRBElem *) pCapwapCapwapBaseAcNameListEntry) != RB_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapCapwapBaseAcNameListTableCreatApi: Fail to Add the node.\r\n");
            MemReleaseMemBlock (CAPWAP_CAPWAPBASEACNAMELISTTABLE_POOLID,
                                (UINT1 *) pCapwapCapwapBaseAcNameListEntry);
            return NULL;
        }
        return pCapwapCapwapBaseAcNameListEntry;
    }
}

/****************************************************************************
 Function    :  CapwapCapwapBaseMacAclTableCreateApi
 Input       :  pCapwapCapwapBaseMacAclEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tCapwapCapwapBaseMacAclEntry *
CapwapCapwapBaseMacAclTableCreateApi (tCapwapCapwapBaseMacAclEntry *
                                      pSetCapwapCapwapBaseMacAclEntry)
{
    tCapwapCapwapBaseMacAclEntry *pCapwapCapwapBaseMacAclEntry = NULL;

    if (pSetCapwapCapwapBaseMacAclEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapCapwapBaseMacAclTableCreatApi: pSetCapwapCapwapBaseMacAclEntry is NULL.\r\n");
        return NULL;
    }
    /* Allocate memory for the new node */
    pCapwapCapwapBaseMacAclEntry =
        (tCapwapCapwapBaseMacAclEntry *)
        MemAllocMemBlk (CAPWAP_CAPWAPBASEMACACLTABLE_POOLID);
    if (pCapwapCapwapBaseMacAclEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapCapwapBaseMacAclTableCreatApi: Fail to Allocate Memory.\r\n");
        return NULL;
    }
    else
    {
        MEMCPY (pCapwapCapwapBaseMacAclEntry, pSetCapwapCapwapBaseMacAclEntry,
                sizeof (tCapwapCapwapBaseMacAclEntry));
        if (RBTreeAdd
            (gCapwapGlobals.CapwapGlbMib.CapwapBaseMacAclTable,
             (tRBElem *) pCapwapCapwapBaseMacAclEntry) != RB_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapCapwapBaseMacAclTableCreatApi: Fail to Add the node.\r\n");
            MemReleaseMemBlock (CAPWAP_CAPWAPBASEMACACLTABLE_POOLID,
                                (UINT1 *) pCapwapCapwapBaseMacAclEntry);
            return NULL;
        }
        return pCapwapCapwapBaseMacAclEntry;
    }
}

/****************************************************************************
 Function    :  CapwapCapwapBaseWtpProfileTableCreateApi
 Input       :  pCapwapCapwapBaseWtpProfileEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tCapwapCapwapBaseWtpProfileEntry *
CapwapCapwapBaseWtpProfileTableCreateApi (tCapwapCapwapBaseWtpProfileEntry *
                                          pSetCapwapCapwapBaseWtpProfileEntry)
{
    tCapwapCapwapBaseWtpProfileEntry *pCapwapCapwapBaseWtpProfileEntry = NULL;

    if (pSetCapwapCapwapBaseWtpProfileEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapCapwapBaseWtpProfileTableCreatApi: pSetCapwapCapwapBaseWtpProfileEntry is NULL.\r\n");
        return NULL;
    }
    /* Allocate memory for the new node */
    pCapwapCapwapBaseWtpProfileEntry =
        (tCapwapCapwapBaseWtpProfileEntry *)
        MemAllocMemBlk (CAPWAP_CAPWAPBASEWTPPROFILETABLE_POOLID);
    if (pCapwapCapwapBaseWtpProfileEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapCapwapBaseWtpProfileTableCreatApi: Fail to Allocate Memory.\r\n");
        return NULL;
    }
    else
    {
        MEMCPY (pCapwapCapwapBaseWtpProfileEntry,
                pSetCapwapCapwapBaseWtpProfileEntry,
                sizeof (tCapwapCapwapBaseWtpProfileEntry));
        if (RBTreeAdd
            (gCapwapGlobals.CapwapGlbMib.CapwapBaseWtpProfileTable,
             (tRBElem *) pCapwapCapwapBaseWtpProfileEntry) != RB_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapCapwapBaseWtpProfileTableCreatApi: Fail to Add the node.\r\n");
            MemReleaseMemBlock (CAPWAP_CAPWAPBASEWTPPROFILETABLE_POOLID,
                                (UINT1 *) pCapwapCapwapBaseWtpProfileEntry);
            return NULL;
        }
        return pCapwapCapwapBaseWtpProfileEntry;
    }
}

/****************************************************************************
 Function    :  CapwapCapwapBaseWtpStateTableCreateApi
 Input       :  pCapwapCapwapBaseWtpStateEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tCapwapCapwapBaseWtpStateEntry *
CapwapCapwapBaseWtpStateTableCreateApi (tCapwapCapwapBaseWtpStateEntry *
                                        pSetCapwapCapwapBaseWtpStateEntry)
{
    tCapwapCapwapBaseWtpStateEntry *pCapwapCapwapBaseWtpStateEntry = NULL;

    if (pSetCapwapCapwapBaseWtpStateEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapCapwapBaseWtpStateTableCreatApi: pSetCapwapCapwapBaseWtpStateEntry is NULL.\r\n");
        return NULL;
    }
    /* Allocate memory for the new node */
    pCapwapCapwapBaseWtpStateEntry =
        (tCapwapCapwapBaseWtpStateEntry *)
        MemAllocMemBlk (CAPWAP_CAPWAPBASEWTPSTATETABLE_POOLID);
    if (pCapwapCapwapBaseWtpStateEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapCapwapBaseWtpStateTableCreatApi: Fail to Allocate Memory.\r\n");
        return NULL;
    }
    else
    {
        MEMCPY (pCapwapCapwapBaseWtpStateEntry,
                pSetCapwapCapwapBaseWtpStateEntry,
                sizeof (tCapwapCapwapBaseWtpStateEntry));
        if (RBTreeAdd
            (gCapwapGlobals.CapwapGlbMib.CapwapBaseWtpStateTable,
             (tRBElem *) pCapwapCapwapBaseWtpStateEntry) != RB_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapCapwapBaseWtpStateTableCreatApi: Fail to Add the node.\r\n");
            MemReleaseMemBlock (CAPWAP_CAPWAPBASEWTPSTATETABLE_POOLID,
                                (UINT1 *) pCapwapCapwapBaseWtpStateEntry);
            return NULL;
        }
        return pCapwapCapwapBaseWtpStateEntry;
    }
}

/****************************************************************************
 Function    :  CapwapCapwapBaseWtpTableCreateApi
 Input       :  pCapwapCapwapBaseWtpEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tCapwapCapwapBaseWtpEntry *
CapwapCapwapBaseWtpTableCreateApi (tCapwapCapwapBaseWtpEntry *
                                   pSetCapwapCapwapBaseWtpEntry)
{
    tCapwapCapwapBaseWtpEntry *pCapwapCapwapBaseWtpEntry = NULL;

    if (pSetCapwapCapwapBaseWtpEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapCapwapBaseWtpTableCreatApi: pSetCapwapCapwapBaseWtpEntry is NULL.\r\n");
        return NULL;
    }
    /* Allocate memory for the new node */
    pCapwapCapwapBaseWtpEntry =
        (tCapwapCapwapBaseWtpEntry *)
        MemAllocMemBlk (CAPWAP_CAPWAPBASEWTPTABLE_POOLID);
    if (pCapwapCapwapBaseWtpEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapCapwapBaseWtpTableCreatApi: Fail to Allocate Memory.\r\n");
        return NULL;
    }
    else
    {
        MEMCPY (pCapwapCapwapBaseWtpEntry, pSetCapwapCapwapBaseWtpEntry,
                sizeof (tCapwapCapwapBaseWtpEntry));
        if (RBTreeAdd
            (gCapwapGlobals.CapwapGlbMib.CapwapBaseWtpTable,
             (tRBElem *) pCapwapCapwapBaseWtpEntry) != RB_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapCapwapBaseWtpTableCreatApi: Fail to Add the node.\r\n");
            MemReleaseMemBlock (CAPWAP_CAPWAPBASEWTPTABLE_POOLID,
                                (UINT1 *) pCapwapCapwapBaseWtpEntry);
            return NULL;
        }
        return pCapwapCapwapBaseWtpEntry;
    }
}

/****************************************************************************
 Function    :  CapwapCapwapBaseWirelessBindingTableCreateApi
 Input       :  pCapwapCapwapBaseWirelessBindingEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tCapwapCapwapBaseWirelessBindingEntry
    * CapwapCapwapBaseWirelessBindingTableCreateApi
    (tCapwapCapwapBaseWirelessBindingEntry *
     pSetCapwapCapwapBaseWirelessBindingEntry)
{
    tCapwapCapwapBaseWirelessBindingEntry *pCapwapCapwapBaseWirelessBindingEntry
        = NULL;

    if (pSetCapwapCapwapBaseWirelessBindingEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapCapwapBaseWirelessBindingTableCreatApi: pSetCapwapCapwapBaseWirelessBindingEntry is NULL.\r\n");
        return NULL;
    }
    /* Allocate memory for the new node */
    pCapwapCapwapBaseWirelessBindingEntry =
        (tCapwapCapwapBaseWirelessBindingEntry *)
        MemAllocMemBlk (CAPWAP_CAPWAPBASEWIRELESSBINDINGTABLE_POOLID);
    if (pCapwapCapwapBaseWirelessBindingEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapCapwapBaseWirelessBindingTableCreatApi: Fail to Allocate Memory.\r\n");
        return NULL;
    }
    else
    {
        MEMCPY (pCapwapCapwapBaseWirelessBindingEntry,
                pSetCapwapCapwapBaseWirelessBindingEntry,
                sizeof (tCapwapCapwapBaseWirelessBindingEntry));
        if (RBTreeAdd
            (gCapwapGlobals.CapwapGlbMib.CapwapBaseWirelessBindingTable,
             (tRBElem *) pCapwapCapwapBaseWirelessBindingEntry) != RB_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapCapwapBaseWirelessBindingTableCreatApi: Fail to Add the node.\r\n");
            MemReleaseMemBlock (CAPWAP_CAPWAPBASEWIRELESSBINDINGTABLE_POOLID,
                                (UINT1 *)
                                pCapwapCapwapBaseWirelessBindingEntry);
            return NULL;
        }
        return pCapwapCapwapBaseWirelessBindingEntry;
    }
}

/****************************************************************************
 Function    :  CapwapCapwapBaseStationTableCreateApi
 Input       :  pCapwapCapwapBaseStationEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tCapwapCapwapBaseStationEntry *
CapwapCapwapBaseStationTableCreateApi (tCapwapCapwapBaseStationEntry *
                                       pSetCapwapCapwapBaseStationEntry)
{
    tCapwapCapwapBaseStationEntry *pCapwapCapwapBaseStationEntry = NULL;

    if (pSetCapwapCapwapBaseStationEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapCapwapBaseStationTableCreatApi: pSetCapwapCapwapBaseStationEntry is NULL.\r\n");
        return NULL;
    }
    /* Allocate memory for the new node */
    pCapwapCapwapBaseStationEntry =
        (tCapwapCapwapBaseStationEntry *)
        MemAllocMemBlk (CAPWAP_CAPWAPBASESTATIONTABLE_POOLID);
    if (pCapwapCapwapBaseStationEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapCapwapBaseStationTableCreatApi: Fail to Allocate Memory.\r\n");
        return NULL;
    }
    else
    {
        MEMCPY (pCapwapCapwapBaseStationEntry, pSetCapwapCapwapBaseStationEntry,
                sizeof (tCapwapCapwapBaseStationEntry));
        if (RBTreeAdd
            (gCapwapGlobals.CapwapGlbMib.CapwapBaseStationTable,
             (tRBElem *) pCapwapCapwapBaseStationEntry) != RB_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapCapwapBaseStationTableCreatApi: Fail to Add the node.\r\n");
            MemReleaseMemBlock (CAPWAP_CAPWAPBASESTATIONTABLE_POOLID,
                                (UINT1 *) pCapwapCapwapBaseStationEntry);
            return NULL;
        }
        return pCapwapCapwapBaseStationEntry;
    }
}

/****************************************************************************
 Function    :  CapwapCapwapBaseWtpEventsStatsTableCreateApi
 Input       :  pCapwapCapwapBaseWtpEventsStatsEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tCapwapCapwapBaseWtpEventsStatsEntry
    * CapwapCapwapBaseWtpEventsStatsTableCreateApi
    (tCapwapCapwapBaseWtpEventsStatsEntry *
     pSetCapwapCapwapBaseWtpEventsStatsEntry)
{
    tCapwapCapwapBaseWtpEventsStatsEntry *pCapwapCapwapBaseWtpEventsStatsEntry =
        NULL;

    if (pSetCapwapCapwapBaseWtpEventsStatsEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapCapwapBaseWtpEventsStatsTableCreatApi: pSetCapwapCapwapBaseWtpEventsStatsEntry is NULL.\r\n");
        return NULL;
    }
    /* Allocate memory for the new node */
    pCapwapCapwapBaseWtpEventsStatsEntry =
        (tCapwapCapwapBaseWtpEventsStatsEntry *)
        MemAllocMemBlk (CAPWAP_CAPWAPBASEWTPEVENTSSTATSTABLE_POOLID);
    if (pCapwapCapwapBaseWtpEventsStatsEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapCapwapBaseWtpEventsStatsTableCreatApi: Fail to Allocate Memory.\r\n");
        return NULL;
    }
    else
    {
        MEMCPY (pCapwapCapwapBaseWtpEventsStatsEntry,
                pSetCapwapCapwapBaseWtpEventsStatsEntry,
                sizeof (tCapwapCapwapBaseWtpEventsStatsEntry));
        if (RBTreeAdd
            (gCapwapGlobals.CapwapGlbMib.CapwapBaseWtpEventsStatsTable,
             (tRBElem *) pCapwapCapwapBaseWtpEventsStatsEntry) != RB_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapCapwapBaseWtpEventsStatsTableCreatApi: Fail to Add the node.\r\n");
            MemReleaseMemBlock (CAPWAP_CAPWAPBASEWTPEVENTSSTATSTABLE_POOLID,
                                (UINT1 *) pCapwapCapwapBaseWtpEventsStatsEntry);
            return NULL;
        }
        return pCapwapCapwapBaseWtpEventsStatsEntry;
    }
}

/****************************************************************************
 Function    :  CapwapCapwapBaseRadioEventsStatsTableCreateApi
 Input       :  pCapwapCapwapBaseRadioEventsStatsEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tCapwapCapwapBaseRadioEventsStatsEntry
    * CapwapCapwapBaseRadioEventsStatsTableCreateApi
    (tCapwapCapwapBaseRadioEventsStatsEntry *
     pSetCapwapCapwapBaseRadioEventsStatsEntry)
{
    tCapwapCapwapBaseRadioEventsStatsEntry
        * pCapwapCapwapBaseRadioEventsStatsEntry = NULL;

    if (pSetCapwapCapwapBaseRadioEventsStatsEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapCapwapBaseRadioEventsStatsTableCreatApi: pSetCapwapCapwapBaseRadioEventsStatsEntry is NULL.\r\n");
        return NULL;
    }
    /* Allocate memory for the new node */
    pCapwapCapwapBaseRadioEventsStatsEntry =
        (tCapwapCapwapBaseRadioEventsStatsEntry *)
        MemAllocMemBlk (CAPWAP_CAPWAPBASERADIOEVENTSSTATSTABLE_POOLID);
    if (pCapwapCapwapBaseRadioEventsStatsEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapCapwapBaseRadioEventsStatsTableCreatApi: Fail to Allocate Memory.\r\n");
        return NULL;
    }
    else
    {
        MEMCPY (pCapwapCapwapBaseRadioEventsStatsEntry,
                pSetCapwapCapwapBaseRadioEventsStatsEntry,
                sizeof (tCapwapCapwapBaseRadioEventsStatsEntry));
        if (RBTreeAdd
            (gCapwapGlobals.CapwapGlbMib.CapwapBaseRadioEventsStatsTable,
             (tRBElem *) pCapwapCapwapBaseRadioEventsStatsEntry) != RB_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapCapwapBaseRadioEventsStatsTableCreatApi: Fail to Add the node.\r\n");
            MemReleaseMemBlock (CAPWAP_CAPWAPBASERADIOEVENTSSTATSTABLE_POOLID,
                                (UINT1 *)
                                pCapwapCapwapBaseRadioEventsStatsEntry);
            return NULL;
        }
        return pCapwapCapwapBaseRadioEventsStatsEntry;
    }
}

/****************************************************************************
 Function    :  CapwapFsWtpModelTableCreateApi
 Input       :  pCapwapFsWtpModelEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tCapwapFsWtpModelEntry *
CapwapFsWtpModelTableCreateApi (tCapwapFsWtpModelEntry *
                                pSetCapwapFsWtpModelEntry)
{
    tCapwapFsWtpModelEntry *pCapwapFsWtpModelEntry = NULL;

    if (pSetCapwapFsWtpModelEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapFsWtpModelTableCreatApi: pSetCapwapFsWtpModelEntry is NULL.\r\n");
        return NULL;
    }
    /* Allocate memory for the new node */
    pCapwapFsWtpModelEntry =
        (tCapwapFsWtpModelEntry *)
        MemAllocMemBlk (CAPWAP_FSWTPMODELTABLE_POOLID);
    if (pCapwapFsWtpModelEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapFsWtpModelTableCreatApi: Fail to Allocate Memory.\r\n");
        return NULL;
    }
    else
    {
        MEMCPY (pCapwapFsWtpModelEntry, pSetCapwapFsWtpModelEntry,
                sizeof (tCapwapFsWtpModelEntry));
        if (RBTreeAdd
            (gCapwapGlobals.CapwapGlbMib.FsWtpModelTable,
             (tRBElem *) pCapwapFsWtpModelEntry) != RB_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapFsWtpModelTableCreatApi: Fail to Add the node.\r\n");
            MemReleaseMemBlock (CAPWAP_FSWTPMODELTABLE_POOLID,
                                (UINT1 *) pCapwapFsWtpModelEntry);
            return NULL;
        }
        return pCapwapFsWtpModelEntry;
    }
}

/****************************************************************************
 Function    :  CapwapFsWtpRadioTableCreateApi
 Input       :  pCapwapFsWtpRadioEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tCapwapFsWtpRadioEntry *
CapwapFsWtpRadioTableCreateApi (tCapwapFsWtpRadioEntry *
                                pSetCapwapFsWtpRadioEntry)
{
    tCapwapFsWtpRadioEntry *pCapwapFsWtpRadioEntry = NULL;

    if (pSetCapwapFsWtpRadioEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapFsWtpRadioTableCreatApi: pSetCapwapFsWtpRadioEntry is NULL.\r\n");
        return NULL;
    }
    /* Allocate memory for the new node */
    pCapwapFsWtpRadioEntry =
        (tCapwapFsWtpRadioEntry *)
        MemAllocMemBlk (CAPWAP_FSWTPRADIOTABLE_POOLID);
    if (pCapwapFsWtpRadioEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapFsWtpRadioTableCreatApi: Fail to Allocate Memory.\r\n");
        return NULL;
    }
    else
    {
        MEMCPY (pCapwapFsWtpRadioEntry, pSetCapwapFsWtpRadioEntry,
                sizeof (tCapwapFsWtpRadioEntry));
        if (RBTreeAdd
            (gCapwapGlobals.CapwapGlbMib.FsWtpRadioTable,
             (tRBElem *) pCapwapFsWtpRadioEntry) != RB_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapFsWtpRadioTableCreatApi: Fail to Add the node.\r\n");
            MemReleaseMemBlock (CAPWAP_FSWTPRADIOTABLE_POOLID,
                                (UINT1 *) pCapwapFsWtpRadioEntry);
            return NULL;
        }
        return pCapwapFsWtpRadioEntry;
    }
}

/****************************************************************************
 Function    :  CapwapFsCapwapWhiteListTableCreateApi
 Input       :  pCapwapFsCapwapWhiteListEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tCapwapFsCapwapWhiteListEntry *
CapwapFsCapwapWhiteListTableCreateApi (tCapwapFsCapwapWhiteListEntry *
                                       pSetCapwapFsCapwapWhiteListEntry)
{
    tCapwapFsCapwapWhiteListEntry *pCapwapFsCapwapWhiteListEntry = NULL;

    if (pSetCapwapFsCapwapWhiteListEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapFsCapwapWhiteListTableCreatApi: pSetCapwapFsCapwapWhiteListEntry is NULL.\r\n");
        return NULL;
    }
    /* Allocate memory for the new node */
    pCapwapFsCapwapWhiteListEntry =
        (tCapwapFsCapwapWhiteListEntry *)
        MemAllocMemBlk (CAPWAP_FSCAPWAPWHITELISTTABLE_POOLID);
    if (pCapwapFsCapwapWhiteListEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapFsCapwapWhiteListTableCreatApi: Fail to Allocate Memory.\r\n");
        return NULL;
    }
    else
    {
        MEMCPY (pCapwapFsCapwapWhiteListEntry, pSetCapwapFsCapwapWhiteListEntry,
                sizeof (tCapwapFsCapwapWhiteListEntry));
        if (RBTreeAdd
            (gCapwapGlobals.CapwapGlbMib.FsCapwapWhiteListTable,
             (tRBElem *) pCapwapFsCapwapWhiteListEntry) != RB_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapFsCapwapWhiteListTableCreatApi: Fail to Add the node.\r\n");
            MemReleaseMemBlock (CAPWAP_FSCAPWAPWHITELISTTABLE_POOLID,
                                (UINT1 *) pCapwapFsCapwapWhiteListEntry);
            return NULL;
        }
        return pCapwapFsCapwapWhiteListEntry;
    }
}

/****************************************************************************
 Function    :  CapwapFsCapwapBlackListCreateApi
 Input       :  pCapwapFsCapwapBlackListEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tCapwapFsCapwapBlackListEntry *
CapwapFsCapwapBlackListCreateApi (tCapwapFsCapwapBlackListEntry *
                                  pSetCapwapFsCapwapBlackListEntry)
{
    tCapwapFsCapwapBlackListEntry *pCapwapFsCapwapBlackListEntry = NULL;

    if (pSetCapwapFsCapwapBlackListEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapFsCapwapBlackListCreatApi: pSetCapwapFsCapwapBlackListEntry is NULL.\r\n");
        return NULL;
    }
    /* Allocate memory for the new node */
    pCapwapFsCapwapBlackListEntry =
        (tCapwapFsCapwapBlackListEntry *)
        MemAllocMemBlk (CAPWAP_FSCAPWAPBLACKLIST_POOLID);
    if (pCapwapFsCapwapBlackListEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapFsCapwapBlackListCreatApi: Fail to Allocate Memory.\r\n");
        return NULL;
    }
    else
    {
        MEMCPY (pCapwapFsCapwapBlackListEntry, pSetCapwapFsCapwapBlackListEntry,
                sizeof (tCapwapFsCapwapBlackListEntry));
        if (RBTreeAdd
            (gCapwapGlobals.CapwapGlbMib.FsCapwapBlackList,
             (tRBElem *) pCapwapFsCapwapBlackListEntry) != RB_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapFsCapwapBlackListCreatApi: Fail to Add the node.\r\n");
            MemReleaseMemBlock (CAPWAP_FSCAPWAPBLACKLIST_POOLID,
                                (UINT1 *) pCapwapFsCapwapBlackListEntry);
            return NULL;
        }
        return pCapwapFsCapwapBlackListEntry;
    }
}

/****************************************************************************
 Function    :  CapwapFsCapwapWtpConfigTableCreateApi
 Input       :  pCapwapFsCapwapWtpConfigEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tCapwapFsCapwapWtpConfigEntry *
CapwapFsCapwapWtpConfigTableCreateApi (tCapwapFsCapwapWtpConfigEntry *
                                       pSetCapwapFsCapwapWtpConfigEntry)
{
    tCapwapFsCapwapWtpConfigEntry *pCapwapFsCapwapWtpConfigEntry = NULL;

    if (pSetCapwapFsCapwapWtpConfigEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapFsCapwapWtpConfigTableCreatApi: pSetCapwapFsCapwapWtpConfigEntry is NULL.\r\n");
        return NULL;
    }
    /* Allocate memory for the new node */
    pCapwapFsCapwapWtpConfigEntry =
        (tCapwapFsCapwapWtpConfigEntry *)
        MemAllocMemBlk (CAPWAP_FSCAPWAPWTPCONFIGTABLE_POOLID);
    if (pCapwapFsCapwapWtpConfigEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapFsCapwapWtpConfigTableCreatApi: Fail to Allocate Memory.\r\n");
        return NULL;
    }
    else
    {
        MEMCPY (pCapwapFsCapwapWtpConfigEntry, pSetCapwapFsCapwapWtpConfigEntry,
                sizeof (tCapwapFsCapwapWtpConfigEntry));
        if (RBTreeAdd
            (gCapwapGlobals.CapwapGlbMib.FsCapwapWtpConfigTable,
             (tRBElem *) pCapwapFsCapwapWtpConfigEntry) != RB_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapFsCapwapWtpConfigTableCreatApi: Fail to Add the node.\r\n");
            MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPCONFIGTABLE_POOLID,
                                (UINT1 *) pCapwapFsCapwapWtpConfigEntry);
            return NULL;
        }
        return pCapwapFsCapwapWtpConfigEntry;
    }
}

/****************************************************************************
 Function    :  CapwapFsCapwapLinkEncryptionTableCreateApi
 Input       :  pCapwapFsCapwapLinkEncryptionEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tCapwapFsCapwapLinkEncryptionEntry *
CapwapFsCapwapLinkEncryptionTableCreateApi (tCapwapFsCapwapLinkEncryptionEntry *
                                            pSetCapwapFsCapwapLinkEncryptionEntry)
{
    tCapwapFsCapwapLinkEncryptionEntry *pCapwapFsCapwapLinkEncryptionEntry =
        NULL;

    if (pSetCapwapFsCapwapLinkEncryptionEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapFsCapwapLinkEncryptionTableCreatApi: pSetCapwapFsCapwapLinkEncryptionEntry is NULL.\r\n");
        return NULL;
    }
    /* Allocate memory for the new node */
    pCapwapFsCapwapLinkEncryptionEntry =
        (tCapwapFsCapwapLinkEncryptionEntry *)
        MemAllocMemBlk (CAPWAP_FSCAPWAPLINKENCRYPTIONTABLE_POOLID);
    if (pCapwapFsCapwapLinkEncryptionEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapFsCapwapLinkEncryptionTableCreatApi: Fail to Allocate Memory.\r\n");
        return NULL;
    }
    else
    {
        MEMCPY (pCapwapFsCapwapLinkEncryptionEntry,
                pSetCapwapFsCapwapLinkEncryptionEntry,
                sizeof (tCapwapFsCapwapLinkEncryptionEntry));
        if (RBTreeAdd
            (gCapwapGlobals.CapwapGlbMib.FsCapwapLinkEncryptionTable,
             (tRBElem *) pCapwapFsCapwapLinkEncryptionEntry) != RB_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapFsCapwapLinkEncryptionTableCreatApi: Fail to Add the node.\r\n");
            MemReleaseMemBlock (CAPWAP_FSCAPWAPLINKENCRYPTIONTABLE_POOLID,
                                (UINT1 *) pCapwapFsCapwapLinkEncryptionEntry);
            return NULL;
        }
        return pCapwapFsCapwapLinkEncryptionEntry;
    }
}

/****************************************************************************
 Function    :  CapwapFsCawapDefaultWtpProfileTableCreateApi
 Input       :  pCapwapFsCapwapDefaultWtpProfileEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tCapwapFsCapwapDefaultWtpProfileEntry
    *CapwapFsCawapDefaultWtpProfileTableCreateApi
    (tCapwapFsCapwapDefaultWtpProfileEntry *
     pSetCapwapFsCapwapDefaultWtpProfileEntry)
{
    tCapwapFsCapwapDefaultWtpProfileEntry *pCapwapFsCapwapDefaultWtpProfileEntry
        = NULL;

    if (pSetCapwapFsCapwapDefaultWtpProfileEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapFsCawapDefaultWtpProfileTableCreatApi: pSetCapwapFsCapwapDefaultWtpProfileEntry is NULL.\r\n");
        return NULL;
    }
    /* Allocate memory for the new node */
    pCapwapFsCapwapDefaultWtpProfileEntry =
        (tCapwapFsCapwapDefaultWtpProfileEntry *)
        MemAllocMemBlk (CAPWAP_FSCAWAPDEFAULTWTPPROFILETABLE_POOLID);
    if (pCapwapFsCapwapDefaultWtpProfileEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapFsCawapDefaultWtpProfileTableCreatApi: Fail to Allocate Memory.\r\n");
        return NULL;
    }
    else
    {
        MEMCPY (pCapwapFsCapwapDefaultWtpProfileEntry,
                pSetCapwapFsCapwapDefaultWtpProfileEntry,
                sizeof (tCapwapFsCapwapDefaultWtpProfileEntry));
        if (RBTreeAdd
            (gCapwapGlobals.CapwapGlbMib.FsCawapDefaultWtpProfileTable,
             (tRBElem *) pCapwapFsCapwapDefaultWtpProfileEntry) != RB_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapFsCawapDefaultWtpProfileTableCreatApi: Fail to Add the node.\r\n");
            MemReleaseMemBlock (CAPWAP_FSCAWAPDEFAULTWTPPROFILETABLE_POOLID,
                                (UINT1 *)
                                pCapwapFsCapwapDefaultWtpProfileEntry);
            return NULL;
        }
        return pCapwapFsCapwapDefaultWtpProfileEntry;
    }
}

/****************************************************************************
 Function    :  CapwapFsCapwapDnsProfileTableCreateApi
 Input       :  pCapwapFsCapwapDnsProfileEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tCapwapFsCapwapDnsProfileEntry *
CapwapFsCapwapDnsProfileTableCreateApi (tCapwapFsCapwapDnsProfileEntry *
                                        pSetCapwapFsCapwapDnsProfileEntry)
{
    tCapwapFsCapwapDnsProfileEntry *pCapwapFsCapwapDnsProfileEntry = NULL;

    if (pSetCapwapFsCapwapDnsProfileEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapFsCapwapDnsProfileTableCreatApi: pSetCapwapFsCapwapDnsProfileEntry is NULL.\r\n");
        return NULL;
    }
    /* Allocate memory for the new node */
    pCapwapFsCapwapDnsProfileEntry =
        (tCapwapFsCapwapDnsProfileEntry *)
        MemAllocMemBlk (CAPWAP_FSCAPWAPDNSPROFILETABLE_POOLID);
    if (pCapwapFsCapwapDnsProfileEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapFsCapwapDnsProfileTableCreatApi: Fail to Allocate Memory.\r\n");
        return NULL;
    }
    else
    {
        MEMCPY (pCapwapFsCapwapDnsProfileEntry,
                pSetCapwapFsCapwapDnsProfileEntry,
                sizeof (tCapwapFsCapwapDnsProfileEntry));
        if (RBTreeAdd
            (gCapwapGlobals.CapwapGlbMib.FsCapwapDnsProfileTable,
             (tRBElem *) pCapwapFsCapwapDnsProfileEntry) != RB_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapFsCapwapDnsProfileTableCreatApi: Fail to Add the node.\r\n");
            MemReleaseMemBlock (CAPWAP_FSCAPWAPDNSPROFILETABLE_POOLID,
                                (UINT1 *) pCapwapFsCapwapDnsProfileEntry);
            return NULL;
        }
        return pCapwapFsCapwapDnsProfileEntry;
    }
}

/****************************************************************************
 Function    :  CapwapFsWtpNativeVlanIdTableCreateApi
 Input       :  pCapwapFsWtpNativeVlanIdTable
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tCapwapFsWtpNativeVlanIdTable *
CapwapFsWtpNativeVlanIdTableCreateApi (tCapwapFsWtpNativeVlanIdTable *
                                       pSetCapwapFsWtpNativeVlanIdTable)
{
    tCapwapFsWtpNativeVlanIdTable *pCapwapFsWtpNativeVlanIdTable = NULL;

    if (pSetCapwapFsWtpNativeVlanIdTable == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapFsWtpNativeVlanIdTableCreatApi: pSetCapwapFsWtpNativeVlanIdTable is NULL.\r\n");
        return NULL;
    }
    /* Allocate memory for the new node */
    pCapwapFsWtpNativeVlanIdTable =
        (tCapwapFsWtpNativeVlanIdTable *)
        MemAllocMemBlk (CAPWAP_FSWTPNATIVEVLANIDTABLE_POOLID);
    if (pCapwapFsWtpNativeVlanIdTable == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapFsWtpNativeVlanIdTableCreatApi: Fail to Allocate Memory.\r\n");
        return NULL;
    }
    else
    {
        MEMCPY (pCapwapFsWtpNativeVlanIdTable, pSetCapwapFsWtpNativeVlanIdTable,
                sizeof (tCapwapFsWtpNativeVlanIdTable));
        if (RBTreeAdd
            (gCapwapGlobals.CapwapGlbMib.FsWtpNativeVlanIdTable,
             (tRBElem *) pCapwapFsWtpNativeVlanIdTable) != RB_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapFsWtpNativeVlanIdTableCreatApi: Fail to Add the node.\r\n");
            MemReleaseMemBlock (CAPWAP_FSWTPNATIVEVLANIDTABLE_POOLID,
                                (UINT1 *) pCapwapFsWtpNativeVlanIdTable);
            return NULL;
        }
        return pCapwapFsWtpNativeVlanIdTable;
    }
}

/****************************************************************************
 Function    :  CapwapFsWtpLocalRoutingTableCreateApi
 Input       :  pCapwapFsWtpLocalRoutingTable
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tCapwapFsWtpLocalRoutingTable *
CapwapFsWtpLocalRoutingTableCreateApi (tCapwapFsWtpLocalRoutingTable *
                                       pSetCapwapFsWtpLocalRoutingTable)
{
    tCapwapFsWtpLocalRoutingTable *pCapwapFsWtpLocalRoutingTable = NULL;

    if (pSetCapwapFsWtpLocalRoutingTable == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapFsWtpLocalRoutingTableCreatApi: pSetCapwapFsWtpLocalRoutingTable is NULL.\r\n");
        return NULL;
    }
    /* Allocate memory for the new node */
    pCapwapFsWtpLocalRoutingTable =
        (tCapwapFsWtpLocalRoutingTable *)
        MemAllocMemBlk (CAPWAP_FSWTPLOCALROUTINGTABLE_POOLID);
    if (pCapwapFsWtpLocalRoutingTable == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapFsWtpLocalRoutingTableCreatApi: Fail to Allocate Memory.\r\n");
        return NULL;
    }
    else
    {
        MEMCPY (pCapwapFsWtpLocalRoutingTable, pSetCapwapFsWtpLocalRoutingTable,
                sizeof (tCapwapFsWtpLocalRoutingTable));
        if (RBTreeAdd
            (gCapwapGlobals.CapwapGlbMib.FsWtpLocalRoutingTable,
             (tRBElem *) pCapwapFsWtpLocalRoutingTable) != RB_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapFsWtpLocalRoutingTableCreatApi: Fail to Add the node.\r\n");
            MemReleaseMemBlock (CAPWAP_FSWTPLOCALROUTINGTABLE_POOLID,
                                (UINT1 *) pCapwapFsWtpLocalRoutingTable);
            return NULL;
        }
        return pCapwapFsWtpLocalRoutingTable;
    }
}

/****************************************************************************
 Function    :  CapwapFsCawapDiscStatsTableCreateApi
 Input       :  pCapwapFsCawapDiscStatsEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tCapwapFsCawapDiscStatsEntry *
CapwapFsCawapDiscStatsTableCreateApi (tCapwapFsCawapDiscStatsEntry *
                                      pSetCapwapFsCawapDiscStatsEntry)
{
    tCapwapFsCawapDiscStatsEntry *pCapwapFsCawapDiscStatsEntry = NULL;

    if (pSetCapwapFsCawapDiscStatsEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapFsCawapDiscStatsTableCreatApi: pSetCapwapFsCawapDiscStatsEntry is NULL.\r\n");
        return NULL;
    }
    /* Allocate memory for the new node */
    pCapwapFsCawapDiscStatsEntry =
        (tCapwapFsCawapDiscStatsEntry *)
        MemAllocMemBlk (CAPWAP_FSCAWAPDISCSTATSTABLE_POOLID);
    if (pCapwapFsCawapDiscStatsEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapFsCawapDiscStatsTableCreatApi: Fail to Allocate Memory.\r\n");
        return NULL;
    }
    else
    {
        MEMCPY (pCapwapFsCawapDiscStatsEntry, pSetCapwapFsCawapDiscStatsEntry,
                sizeof (tCapwapFsCawapDiscStatsEntry));
        if (RBTreeAdd
            (gCapwapGlobals.CapwapGlbMib.FsCawapDiscStatsTable,
             (tRBElem *) pCapwapFsCawapDiscStatsEntry) != RB_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapFsCawapDiscStatsTableCreatApi: Fail to Add the node.\r\n");
            MemReleaseMemBlock (CAPWAP_FSCAWAPDISCSTATSTABLE_POOLID,
                                (UINT1 *) pCapwapFsCawapDiscStatsEntry);
            return NULL;
        }
        return pCapwapFsCawapDiscStatsEntry;
    }
}

/****************************************************************************
 Function    :  CapwapFsCawapJoinStatsTableCreateApi
 Input       :  pCapwapFsCawapJoinStatsEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tCapwapFsCawapJoinStatsEntry *
CapwapFsCawapJoinStatsTableCreateApi (tCapwapFsCawapJoinStatsEntry *
                                      pSetCapwapFsCawapJoinStatsEntry)
{
    tCapwapFsCawapJoinStatsEntry *pCapwapFsCawapJoinStatsEntry = NULL;

    if (pSetCapwapFsCawapJoinStatsEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapFsCawapJoinStatsTableCreatApi: pSetCapwapFsCawapJoinStatsEntry is NULL.\r\n");
        return NULL;
    }
    /* Allocate memory for the new node */
    pCapwapFsCawapJoinStatsEntry =
        (tCapwapFsCawapJoinStatsEntry *)
        MemAllocMemBlk (CAPWAP_FSCAWAPJOINSTATSTABLE_POOLID);
    if (pCapwapFsCawapJoinStatsEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapFsCawapJoinStatsTableCreatApi: Fail to Allocate Memory.\r\n");
        return NULL;
    }
    else
    {
        MEMCPY (pCapwapFsCawapJoinStatsEntry, pSetCapwapFsCawapJoinStatsEntry,
                sizeof (tCapwapFsCawapJoinStatsEntry));
        if (RBTreeAdd
            (gCapwapGlobals.CapwapGlbMib.FsCawapJoinStatsTable,
             (tRBElem *) pCapwapFsCawapJoinStatsEntry) != RB_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapFsCawapJoinStatsTableCreatApi: Fail to Add the node.\r\n");
            MemReleaseMemBlock (CAPWAP_FSCAWAPJOINSTATSTABLE_POOLID,
                                (UINT1 *) pCapwapFsCawapJoinStatsEntry);
            return NULL;
        }
        return pCapwapFsCawapJoinStatsEntry;
    }
}

/****************************************************************************
 Function    :  CapwapFsCawapConfigStatsTableCreateApi
 Input       :  pCapwapFsCawapConfigStatsEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tCapwapFsCawapConfigStatsEntry *
CapwapFsCawapConfigStatsTableCreateApi (tCapwapFsCawapConfigStatsEntry *
                                        pSetCapwapFsCawapConfigStatsEntry)
{
    tCapwapFsCawapConfigStatsEntry *pCapwapFsCawapConfigStatsEntry = NULL;

    if (pSetCapwapFsCawapConfigStatsEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapFsCawapConfigStatsTableCreatApi: pSetCapwapFsCawapConfigStatsEntry is NULL.\r\n");
        return NULL;
    }
    /* Allocate memory for the new node */
    pCapwapFsCawapConfigStatsEntry =
        (tCapwapFsCawapConfigStatsEntry *)
        MemAllocMemBlk (CAPWAP_FSCAWAPCONFIGSTATSTABLE_POOLID);
    if (pCapwapFsCawapConfigStatsEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapFsCawapConfigStatsTableCreatApi: Fail to Allocate Memory.\r\n");
        return NULL;
    }
    else
    {
        MEMCPY (pCapwapFsCawapConfigStatsEntry,
                pSetCapwapFsCawapConfigStatsEntry,
                sizeof (tCapwapFsCawapConfigStatsEntry));
        if (RBTreeAdd
            (gCapwapGlobals.CapwapGlbMib.FsCawapConfigStatsTable,
             (tRBElem *) pCapwapFsCawapConfigStatsEntry) != RB_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapFsCawapConfigStatsTableCreatApi: Fail to Add the node.\r\n");
            MemReleaseMemBlock (CAPWAP_FSCAWAPCONFIGSTATSTABLE_POOLID,
                                (UINT1 *) pCapwapFsCawapConfigStatsEntry);
            return NULL;
        }
        return pCapwapFsCawapConfigStatsEntry;
    }
}

/****************************************************************************
 Function    :  CapwapFsCawapRunStatsTableCreateApi
 Input       :  pCapwapFsCawapRunStatsEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tCapwapFsCawapRunStatsEntry *
CapwapFsCawapRunStatsTableCreateApi (tCapwapFsCawapRunStatsEntry *
                                     pSetCapwapFsCawapRunStatsEntry)
{
    tCapwapFsCawapRunStatsEntry *pCapwapFsCawapRunStatsEntry = NULL;

    if (pSetCapwapFsCawapRunStatsEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapFsCawapRunStatsTableCreatApi: pSetCapwapFsCawapRunStatsEntry is NULL.\r\n");
        return NULL;
    }
    /* Allocate memory for the new node */
    pCapwapFsCawapRunStatsEntry =
        (tCapwapFsCawapRunStatsEntry *)
        MemAllocMemBlk (CAPWAP_FSCAWAPRUNSTATSTABLE_POOLID);
    if (pCapwapFsCawapRunStatsEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapFsCawapRunStatsTableCreatApi: Fail to Allocate Memory.\r\n");
        return NULL;
    }
    else
    {
        MEMCPY (pCapwapFsCawapRunStatsEntry, pSetCapwapFsCawapRunStatsEntry,
                sizeof (tCapwapFsCawapRunStatsEntry));
        if (RBTreeAdd
            (gCapwapGlobals.CapwapGlbMib.FsCawapRunStatsTable,
             (tRBElem *) pCapwapFsCawapRunStatsEntry) != RB_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapFsCawapRunStatsTableCreatApi: Fail to Add the node.\r\n");
            MemReleaseMemBlock (CAPWAP_FSCAWAPRUNSTATSTABLE_POOLID,
                                (UINT1 *) pCapwapFsCawapRunStatsEntry);
            return NULL;
        }
        return pCapwapFsCawapRunStatsEntry;
    }
}

/****************************************************************************
 Function    :  CapwapFsCapwapWirelessBindingTableCreateApi
 Input       :  pCapwapFsCapwapWirelessBindingEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tCapwapFsCapwapWirelessBindingEntry *
CapwapFsCapwapWirelessBindingTableCreateApi (tCapwapFsCapwapWirelessBindingEntry
                                             *
                                             pSetCapwapFsCapwapWirelessBindingEntry)
{
    tCapwapFsCapwapWirelessBindingEntry *pCapwapFsCapwapWirelessBindingEntry =
        NULL;

    if (pSetCapwapFsCapwapWirelessBindingEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapFsCapwapWirelessBindingTableCreatApi: pSetCapwapFsCapwapWirelessBindingEntry is NULL.\r\n");
        return NULL;
    }
    /* Allocate memory for the new node */
    pCapwapFsCapwapWirelessBindingEntry =
        (tCapwapFsCapwapWirelessBindingEntry *)
        MemAllocMemBlk (CAPWAP_FSCAPWAPWIRELESSBINDINGTABLE_POOLID);
    if (pCapwapFsCapwapWirelessBindingEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapFsCapwapWirelessBindingTableCreatApi: Fail to Allocate Memory.\r\n");
        return NULL;
    }
    else
    {
        MEMCPY (pCapwapFsCapwapWirelessBindingEntry,
                pSetCapwapFsCapwapWirelessBindingEntry,
                sizeof (tCapwapFsCapwapWirelessBindingEntry));
        if (RBTreeAdd
            (gCapwapGlobals.CapwapGlbMib.FsCapwapWirelessBindingTable,
             (tRBElem *) pCapwapFsCapwapWirelessBindingEntry) != RB_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapFsCapwapWirelessBindingTableCreatApi: Fail to Add the node.\r\n");
            MemReleaseMemBlock (CAPWAP_FSCAPWAPWIRELESSBINDINGTABLE_POOLID,
                                (UINT1 *) pCapwapFsCapwapWirelessBindingEntry);
            return NULL;
        }
        return pCapwapFsCapwapWirelessBindingEntry;
    }
}

/****************************************************************************
 Function    :  CapwapFsCapwapStationWhiteListCreateApi
 Input       :  pCapwapFsCapwapStationWhiteListEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tCapwapFsCapwapStationWhiteListEntry *
CapwapFsCapwapStationWhiteListCreateApi (tCapwapFsCapwapStationWhiteListEntry *
                                         pSetCapwapFsCapwapStationWhiteListEntry)
{
    tCapwapFsCapwapStationWhiteListEntry *pCapwapFsCapwapStationWhiteListEntry =
        NULL;

    if (pSetCapwapFsCapwapStationWhiteListEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapFsCapwapStationWhiteListCreatApi: pSetCapwapFsCapwapStationWhiteListEntry is NULL.\r\n");
        return NULL;
    }
    /* Allocate memory for the new node */
    pCapwapFsCapwapStationWhiteListEntry =
        (tCapwapFsCapwapStationWhiteListEntry *)
        MemAllocMemBlk (CAPWAP_FSCAPWAPSTATIONWHITELIST_POOLID);
    if (pCapwapFsCapwapStationWhiteListEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapFsCapwapStationWhiteListCreatApi: Fail to Allocate Memory.\r\n");
        return NULL;
    }
    else
    {
        MEMCPY (pCapwapFsCapwapStationWhiteListEntry,
                pSetCapwapFsCapwapStationWhiteListEntry,
                sizeof (tCapwapFsCapwapStationWhiteListEntry));
        if (RBTreeAdd
            (gCapwapGlobals.CapwapGlbMib.FsCapwapStationWhiteList,
             (tRBElem *) pCapwapFsCapwapStationWhiteListEntry) != RB_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapFsCapwapStationWhiteListCreatApi: Fail to Add the node.\r\n");
            MemReleaseMemBlock (CAPWAP_FSCAPWAPSTATIONWHITELIST_POOLID,
                                (UINT1 *) pCapwapFsCapwapStationWhiteListEntry);
            return NULL;
        }
        return pCapwapFsCapwapStationWhiteListEntry;
    }
}/****************************************************************************

 Function    :  CapwapFsCapwapWtpRebootStatisticsTableCreateApi
 Input       :  pCapwapFsCapwapWtpRebootStatisticsEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tCapwapFsCapwapWtpRebootStatisticsEntry
    *CapwapFsCapwapWtpRebootStatisticsTableCreateApi
    (tCapwapFsCapwapWtpRebootStatisticsEntry *
     pSetCapwapFsCapwapWtpRebootStatisticsEntry)
{
    tCapwapFsCapwapWtpRebootStatisticsEntry
        * pCapwapFsCapwapWtpRebootStatisticsEntry = NULL;

    if (pSetCapwapFsCapwapWtpRebootStatisticsEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapFsCapwapWtpRebootStatisticsTableCreatApi: pSetCapwapFsCapwapWtpRebootStatisticsEntry is NULL.\r\n");
        return NULL;
    }
    /* Allocate memory for the new node */
    pCapwapFsCapwapWtpRebootStatisticsEntry =
        (tCapwapFsCapwapWtpRebootStatisticsEntry *)
        MemAllocMemBlk (CAPWAP_FSCAPWAPWTPREBOOTSTATISTICSTABLE_POOLID);
    if (pCapwapFsCapwapWtpRebootStatisticsEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapFsCapwapWtpRebootStatisticsTableCreatApi: Fail to Allocate Memory.\r\n");
        return NULL;
    }
    else
    {
        MEMCPY (pCapwapFsCapwapWtpRebootStatisticsEntry,
                pSetCapwapFsCapwapWtpRebootStatisticsEntry,
                sizeof (tCapwapFsCapwapWtpRebootStatisticsEntry));
        if (RBTreeAdd
            (gCapwapGlobals.CapwapGlbMib.FsCapwapWtpRebootStatisticsTable,
             (tRBElem *) pCapwapFsCapwapWtpRebootStatisticsEntry) != RB_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapFsCapwapWtpRebootStatisticsTableCreatApi: Fail to Add the node.\r\n");
            MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPREBOOTSTATISTICSTABLE_POOLID,
                                (UINT1 *)
                                pCapwapFsCapwapWtpRebootStatisticsEntry);
            return NULL;
        }
        return pCapwapFsCapwapWtpRebootStatisticsEntry;
    }
}

/****************************************************************************
 Function    :  CapwapFsCapwapWtpRadioStatisticsTableCreateApi
 Input       :  pCapwapFsCapwapWtpRadioStatisticsEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tCapwapFsCapwapWtpRadioStatisticsEntry
    *CapwapFsCapwapWtpRadioStatisticsTableCreateApi
    (tCapwapFsCapwapWtpRadioStatisticsEntry *
     pSetCapwapFsCapwapWtpRadioStatisticsEntry)
{
    tCapwapFsCapwapWtpRadioStatisticsEntry
        * pCapwapFsCapwapWtpRadioStatisticsEntry = NULL;

    if (pSetCapwapFsCapwapWtpRadioStatisticsEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapFsCapwapWtpRadioStatisticsTableCreatApi: pSetCapwapFsCapwapWtpRadioStatisticsEntry is NULL.\r\n");
        return NULL;
    }
    /* Allocate memory for the new node */
    pCapwapFsCapwapWtpRadioStatisticsEntry =
        (tCapwapFsCapwapWtpRadioStatisticsEntry *)
        MemAllocMemBlk (CAPWAP_FSCAPWAPWTPRADIOSTATISTICSTABLE_POOLID);
    if (pCapwapFsCapwapWtpRadioStatisticsEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_UTIL_TRC,
                    "CapwapFsCapwapWtpRadioStatisticsTableCreatApi: Fail to Allocate Memory.\r\n");
        return NULL;
    }
    else
    {
        MEMCPY (pCapwapFsCapwapWtpRadioStatisticsEntry,
                pSetCapwapFsCapwapWtpRadioStatisticsEntry,
                sizeof (tCapwapFsCapwapWtpRadioStatisticsEntry));
        if (RBTreeAdd
            (gCapwapGlobals.CapwapGlbMib.FsCapwapWtpRadioStatisticsTable,
             (tRBElem *) pCapwapFsCapwapWtpRadioStatisticsEntry) != RB_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                        "CapwapFsCapwapWtpRadioStatisticsTableCreatApi: Fail to Add the node.\r\n");
            MemReleaseMemBlock (CAPWAP_FSCAPWAPWTPRADIOSTATISTICSTABLE_POOLID,
                                (UINT1 *)
                                pCapwapFsCapwapWtpRadioStatisticsEntry);
            return NULL;
        }
        return pCapwapFsCapwapWtpRadioStatisticsEntry;
    }
}
