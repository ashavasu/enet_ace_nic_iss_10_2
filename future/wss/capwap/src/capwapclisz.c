/* $Id: capwapclisz.c,v 1.1.1.1 2016/09/20 10:42:39 siva Exp $ */
#if 0                            /* This file is not longer needed.Because content in this file is already present in capwapsz.h */

#define _CAPWAPCLISZ_C
#include "capwapcliinc.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
CapwapcliSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < CAPWAPCLI_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            MemCreateMemPool (FsCAPWAPCLISizingParams[i4SizingId].u4StructSize,
                              FsCAPWAPCLISizingParams[i4SizingId].
                              u4PreAllocatedUnits, MEM_DEFAULT_MEMORY_TYPE,
                              &(CAPWAPCLIMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            CapwapcliSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
CapwapcliSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsCAPWAPCLISizingParams);
    IssSzRegisterModulePoolId (pu1ModName, CAPWAPCLIMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
CapwapcliSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < CAPWAPCLI_MAX_SIZING_ID; i4SizingId++)
    {
        if (CAPWAPCLIMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (CAPWAPCLIMemPoolIds[i4SizingId]);
            CAPWAPCLIMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
#endif
