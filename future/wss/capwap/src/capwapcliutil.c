/******************************************************************************
 * Copyright (C) Future Software Limited,2005
 * $Id: capwapcliutil.c,v 1.3 2017/12/05 09:36:53 siva Exp $  
 * Description : This file contains the utility 
 *               functions of the CAPWAP module
 *****************************************************************************/

#ifndef __CAPWAPCLIUTIL_C__
#define __CAPWAPCLIUTIL_C__
#include "capwapinc.h"
#include "capwapcli.h"
#include "fswssllw.h"
#include "wsscfgcli.h"
#include "ifmmacs.h"

/******************************************************************************
 * Function   : CapwapUtilFindCapwapStruct
 * Description: 
 * Input      :
 * Output     : 
 * Returns    : Pointer to the tCapwapStruct ,if exists
 *              NULL otherwise
 *****************************************************************************/
/*
PUBLIC tCapwapStruct    *
CapwapUtilFindCapwapStruct ( strcutIndex )
{
}
*/

/****************************************************************************
* Function    :  CapwapCheckForAlphaNumeric
* Description :  Validate profile name, only character and numbers check
* Input       :  pu1WtpProfileName
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
CapwapCheckForAlphaNumeric (UINT1 *pu1WtpProfileName)
{
    UINT2               u2Temp = 0, u2Len = 0;
    tCliHandle          CliHandle;

    MEMSET (&CliHandle, 0, sizeof (tCliHandle));

    u2Len = (UINT2) (STRLEN (pu1WtpProfileName));

    for (u2Temp = 0; u2Temp < u2Len; u2Temp++)
    {
        if (((*(pu1WtpProfileName + u2Temp) >= CLI_CAPWAP_NUM_ASCII_START)
             && (*(pu1WtpProfileName + u2Temp) <= CLI_CAPWAP_NUM_ASCII_END)) ||
            ((*(pu1WtpProfileName + u2Temp) >= CLI_CAPWAP_CALPHA_ASCII_START)
             && (*(pu1WtpProfileName + u2Temp) <= CLI_CAPWAP_CALPHA_ASCII_END))
            || ((*(pu1WtpProfileName + u2Temp) >= CLI_CAPWAP_SALPHA_ASCII_START)
                && (*(pu1WtpProfileName + u2Temp) <=
                    CLI_CAPWAP_SALPHA_ASCII_END)))
        {
            continue;
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nOnly characters and numbers are accepted!\r\n");
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : CapwapShowApRunCount
* Description :
* Input       :  CliHandle 
*            
*            
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/

INT4
CapwapShowApRunCount ()
{
    UINT4               u4WtpProfileId = 0;
    UINT4               u4NextProfileId = 0;
    INT4                i4OutCome = 0;
    UINT1               Macaddr[20];
    INT4                i4Idle = 0;
    INT4                i4Run = 0;
    INT4                i4WtpState = 0;
    tSNMP_OCTET_STRING_TYPE MacAddress;
    MEMSET (&MacAddress, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (Macaddr, 0, sizeof (Macaddr));
    MacAddress.pu1_OctetList = Macaddr;
    i4OutCome = nmhGetFirstIndexCapwapBaseWtpProfileTable (&u4NextProfileId);
    if (i4OutCome == SNMP_FAILURE)
    {
        return CLI_SUCCESS;
    }
    do
    {
        u4WtpProfileId = u4NextProfileId;
        tWssIfCapDB        *pWssIfCapDB = NULL;
        WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapDB, OSIX_FAILURE)
            MEMSET (pWssIfCapDB, 0, sizeof (tWssIfCapDB));
        pWssIfCapDB->CapwapGetDB.u2WtpProfileId = (UINT2) u4WtpProfileId;
        pWssIfCapDB->CapwapIsGetAllDB.bCapwapState = OSIX_TRUE;
        if (CapwapGetWtpStatsEntry (pWssIfCapDB) != OSIX_SUCCESS)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
            continue;
        }

        i4WtpState = (INT4) (pWssIfCapDB->CapwapGetDB.u4CapwapState);
        if (i4WtpState == CAPWAP_RUN)
        {
            i4Run++;
        }
        if (i4WtpState == CAPWAP_IDLE)
        {
            i4Idle++;
        }
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);

    }
    while (nmhGetNextIndexCapwapBaseWtpProfileTable (u4WtpProfileId,
                                                     &u4NextProfileId) ==
           SNMP_SUCCESS);
    gCapwapGlobals.i4CapwApRunStateCount = i4Run;
    gCapwapGlobals.i4CapwApIdleStateCount = i4Idle;
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : FsWssCliValidateIfIpSubnet                         */
/*                                                                           */
/*     DESCRIPTION      : This function validates whether the given IP is    */
/*              is already assigned to any other L3SUB IF          */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*              pu1WtpProfileName - WTP PRofile Name              */
/*              u4IfIndex, IP Address, Subnet Mask                 */
/*     OUTPUT           : NONE                                               */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
FsWssCliValidateIfIpSubnet (UINT4 u4TestValWtpProfileId, UINT4 u4TestValIfIndex,
                            UINT4 u4TestValIpAddr, UINT4 u4TestValSubnetMask)
{
    INT4                i4RetVal = CLI_SUCCESS;
#ifdef WLC_WANTED
    UINT4               u4WtpNextProfileId = 0;
    UINT4               u4NextIfIndex = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4IpAddr = 0;
    UINT4               u4SubnetMask = 0;

    UNUSED_PARAM (u4TestValWtpProfileId);

    if ((nmhGetFirstIndexFsWtpIfMainTable (&u4WtpNextProfileId,
                                           (INT4 *) &u4NextIfIndex)) ==
        SNMP_SUCCESS)
    {
        do
        {
            nmhGetFsWtpIfIpAddr (u4WtpNextProfileId, u4NextIfIndex, &u4IpAddr);
            nmhGetFsWtpIfIpSubnetMask (u4WtpNextProfileId, u4NextIfIndex,
                                       &u4SubnetMask);

            if (u4IpAddr != 0)
            {
                if (u4TestValIfIndex != u4NextIfIndex)
                {
                    if ((u4TestValIpAddr == u4IpAddr) ||
                        ((u4TestValIpAddr & u4TestValSubnetMask) ==
                         (u4IpAddr & u4SubnetMask)))
                    {
                        i4RetVal = CLI_FAILURE;
                        break;
                    }
                }
            }
            u4WtpProfileId = u4WtpNextProfileId;
            u4IfIndex = u4NextIfIndex;
        }
        while ((nmhGetNextIndexFsWtpIfMainTable (u4WtpProfileId,
                                                 &u4WtpNextProfileId,
                                                 (INT4) u4IfIndex,
                                                 (INT4 *) &u4NextIfIndex)) ==
               SNMP_SUCCESS);
    }
#else
    UNUSED_PARAM (u4TestValIfIndex);
    UNUSED_PARAM (u4TestValIpAddr);
    UNUSED_PARAM (u4TestValSubnetMask);

#endif
    UNUSED_PARAM (u4TestValWtpProfileId);
    return i4RetVal;
}

/****************************************************************************
* Function    : CapwapShowAdvancedTimers
* Description :
* Input       : NONE
* Output      :  None
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
nmhTestv2MacAddress (UINT4 *pu4ErrorCode, tMacAddr TestValIfMainExtMacAddress)
{
    tMacAddr            zeroAddr;
    tMacAddr            BcastMacAddr;

    /* Check the mac address to be set is not a complete zero address
       00:00:00:00:00:00 or Multicast or Broadcast Address. */
    MEMSET (zeroAddr, 0, CFA_ENET_ADDR_LEN);
    MEMSET (BcastMacAddr, 0xff, CFA_ENET_ADDR_LEN);

    if ((MEMCMP (TestValIfMainExtMacAddress, zeroAddr,
                 CFA_ENET_ADDR_LEN) == 0) ||
        (CFA_IS_MAC_MULTICAST (TestValIfMainExtMacAddress)) ||
        (MEMCMP (TestValIfMainExtMacAddress, BcastMacAddr,
                 CFA_ENET_ADDR_LEN) == 0))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  capwaputil.c                     */
/*-----------------------------------------------------------------------*/
#endif
