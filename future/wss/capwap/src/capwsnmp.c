/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: capwsnmp.c,v 1.2 2017/05/23 14:16:49 siva Exp $ rfmsnmp.c,v 1.0
 *
 * Description: This file contains the CAPWAP trap generation
 *              related code.
 *                            
 ********************************************************************/

#ifndef __CAPWAPSNMP_C__
#define __CAPWAPSNMP_C__

#include "snmputil.h"
#include "capwapinc.h"
#include "capwapconst.h"
#include "capwapcli.h"

/****************************************************************************
 *                                                                          *
 * Function     : CapwapSnmpifSendTrap                                    *
 *                                                                          *
 * Description  : Routine to send trap to SNMP Agent.                       *
 *                                                                          *
 * Input        : u1TrapId: Trap identifier                                 *
 *                pTrapInfo : Pointer to structure having the trap info.  *
 *                                                                          *
 * Output       : None                                                      *
 *                                                                          *
 * Returns      : VOID                                                      *
 *                                                                          *
 ****************************************************************************/
PUBLIC VOID
CapwapSnmpifSendTrap (UINT1 u1TrapId, VOID *pTrapInfo)
/*(UINT1 u1TrapId, UNIT1 *pcapwapBaseNtfWtpId, INT4 *pi4capwapBaseWtpState, INT4 *pi4fsCapwapImageUpgradeIndi,INT4 *pi4capwapBaseWtpProfileWtpStaticIpAddress, INT4 *pi4fsCapwapBaseSyncFailueStatus )*/
{
#if ((defined(FUTURE_SNMP_WANTED))||(defined(SNMP_3_WANTED))||\
                 (defined(SNMPV3_WANTED)))
    tSNMP_VAR_BIND     *pVbList = NULL, *pStartVb = NULL;
    tSNMP_OID_TYPE     *pEnterpriseOid = NULL;
    tSNMP_COUNTER64_TYPE u8CounterVal = { 0, 0 };
    UINT4               au4snmpTrapOid[] = { 1, 3, 6, 1, 6, 3, 1, 1, 4, 1, 0 };
    UINT4               au4CapwapTrapOid[] =
        { 1, 3, 6, 1, 4, 1, 29601, 2, 82, 5, 0 };
    UINT4               au4capwapBaseNtfWtpId[] =
        { 1, 3, 6, 1, 2, 1, 196, 1, 5, 1 };
    UINT4               au4capwapBaseWtpState[] =
        { 1, 3, 6, 1, 2, 1, 196, 1, 2, 2, 1, 7 };
    UINT4               au4fsCapwapWtpImageUpgradeStatus[] =
        { 1, 3, 6, 1, 4, 1, 29601, 2, 82, 4, 1, 1 };
    UINT4               au4capwapBaseWtpStateWtpIpAddress[] =
        { 1, 3, 6, 1, 2, 1, 196, 1, 2, 2, 1, 3 };
    UINT4               au4fsCapwapWtpSyncStatus[] =
        { 1, 3, 6, 1, 4, 1, 29601, 2, 82, 4, 1, 2 };
    UINT4               au4capwapBaseWtpMacTypeOptions[] =
        { 1, 3, 6, 1, 2, 1, 196, 1, 2, 3, 1, 5 };
    UINT4               au4capwapBaseWtpTunnelModeOptions[] =
        { 1, 3, 6, 1, 2, 1, 196, 1, 2, 3, 1, 4 };
    UINT4               au4fsCapwapWtpImageName[] =
        { 1, 3, 6, 1, 4, 1, 29601, 2, 82, 3, 20, 1, 3 };
    UINT1               au1MacBuf[MAC_ADDR_LENGTH];
    UINT1               au1WtpImageNameBuf[1024];
    tSNMP_OID_TYPE     *pSnmpTrapOid = NULL;
    tCapwapWTPTrapInfo *pWTPTrapInfo = NULL;
    tSNMP_OCTET_STRING_TYPE *pOMacString = NULL;
    tSNMP_OCTET_STRING_TYPE *pOWtpImageNameString = NULL;
    UNUSED_PARAM (au4fsCapwapWtpSyncStatus);

    pWTPTrapInfo = (tCapwapWTPTrapInfo *) pTrapInfo;
    if (gCapwapGlobals.u4CapwTrapStatus != CAPWAP_WTP_TRAP_ENABLE)
    {
        return;
    }

    /* The format of SNMP Trap varbinding which is being send using
     * SNMP_AGT_RIF_Notify_v2Trap() is as given below:
     *
     *
     ********************************************************************************
     *                   *                   *    Varbind     *     Varbind    *
     * sysUpTime = Time  * snmpTrapOID = OID * OID# 1 = Value * OID# 2 = Value *
     *                   *                   *                *                *
     ********************************************************************************
     * 
     * First Var binding sysUpTime = Time - is filled by the SNMP_AGT_RIF_Notify_v2Trap().
     * Second Var binding snmpTrapOID - OID should be first Varbind in our list to the api SNMP_AGT_RIF_Notify_v2Trap().
     * The remaining Varbind are updated based on the type of the trap, which
     * are going to be generated.
     *
     */

    /* Allocate the memory for the Varbind for snmpTrapOID MIB object OID and 
     * the OID of the MIB object for those trap is going to be generated.
     */

    /* Allocate a memory for storing snmpTrapOID OID and assign it */
    pSnmpTrapOid = alloc_oid (CAPWAP_SNMP_V2_TRAP_OID_LEN);
    if (pSnmpTrapOid == NULL)
    {
        return;
    }

    /* Assigning the OID value and its length of snmpTrapOid to psnmpTrapOid */
    MEMCPY (pSnmpTrapOid->pu4_OidList, au4snmpTrapOid, sizeof (au4snmpTrapOid));
    pSnmpTrapOid->u4_Length = sizeof (au4snmpTrapOid) / sizeof (UINT4);

    /* Allocate a memory for storing OID Value which is the OID of the
     * Notification MIB object and assign it */
    pEnterpriseOid = alloc_oid (CAPWAP_SNMP_V2_TRAP_OID_LEN);
    if (pEnterpriseOid == NULL)
    {
        SNMP_FreeOid (pSnmpTrapOid);
        return;
    }

    /* Assigning TrapOid to EnterpriseOid and binding it to packet. */
    MEMCPY (pEnterpriseOid->pu4_OidList, au4CapwapTrapOid,
            sizeof (au4CapwapTrapOid));
    pEnterpriseOid->u4_Length = sizeof (au4CapwapTrapOid) / sizeof (UINT4);

    pEnterpriseOid->pu4_OidList[pEnterpriseOid->u4_Length++] = u1TrapId;
    /* Form a VarBind for snmpTrapOID OID and its value */

    pVbList = (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pSnmpTrapOid,
                                                       SNMP_DATA_TYPE_OBJECT_ID,
                                                       0L, 0, NULL,
                                                       pEnterpriseOid,
                                                       u8CounterVal);

    /* If the pVbList is empty or values are binded then free the memory
       allocated to pEnterpriseOid and pSnmpTrapOid. */
    if (pVbList == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        return;
    }
    /* Assigning pVbList to pStartVb */
    pStartVb = pVbList;
    switch (u1TrapId)
    {
        case CAPWAP_WTP_RESTART:

            /* Checking whether Trap status is enabled or not */
            if (gCapwapGlobals.u4CapwTrapStatus != CAPWAP_WTP_TRAP_ENABLE)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                return;
            }

            /* Allocating memory for storing OID of capwapBaseNtfWtpId object
             * and its length*/
            pSnmpTrapOid = alloc_oid (CAPWAP_SNMP_V2_TRAP_OID_LEN);

            /* Checking for error in memory allocation for pSnmpTrapOid and
             * freeing the memory allocated to pEnterpriseOid if there is an
             * error */
            if (pSnmpTrapOid == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            /* Assigning the oid and oid length of capwapBaseNtfWtpId to pSnmpTrapOid */
            MEMCPY (pSnmpTrapOid->pu4_OidList, au4capwapBaseNtfWtpId,
                    sizeof (au4capwapBaseNtfWtpId));
            pSnmpTrapOid->u4_Length =
                sizeof (au4capwapBaseNtfWtpId) / sizeof (UINT4);

            MEMSET (au1MacBuf, 0, MAC_ADDR_LENGTH);

            /* Assigning MacAddress to MacBuf */
            MEMCPY (au1MacBuf, pWTPTrapInfo->au1capwapBaseNtfWtpId,
                    MAC_ADDR_LENGTH);

            /* Converting the normal string to OctetString using SNMP_AGT_FormOctetString function */
            pOMacString =
                SNMP_AGT_FormOctetString ((UINT1 *) au1MacBuf,
                                          (INT4) (MAC_ADDR_LENGTH));

            /* Forming a VarBind for NtfWtpId, its value and storing it in pVbList */

            pVbList->pNextVarBind = SNMP_AGT_FormVarBind (pSnmpTrapOid,
                                                          SNMP_DATA_TYPE_OCTET_PRIM,
                                                          0, 0, pOMacString,
                                                          NULL, u8CounterVal);

            /* Checking if the pVbList is empty and freeing the memory allocated
               to pSnmpTrapOid and pEnterpriseOid if it is NULL */

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;

            /* Allocating the memory for pSnmpTrapOid to store OID of
             * capwapBaseWtpState object and length of the OID. */
            pSnmpTrapOid = alloc_oid (CAPWAP_SNMP_V2_TRAP_OID_LEN);

            /* Checking for error in memory allocation for pSnmpTrapOid and
             * freeing the memory allocated to pEnterpriseOid if there is an
             * error */

            if (pSnmpTrapOid == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            /* Assigning the OID value and its length of capwapBaseWtpState to pSnmpTrapOid */
            MEMCPY (pSnmpTrapOid->pu4_OidList, au4capwapBaseWtpState,
                    sizeof (au4capwapBaseWtpState));
            pSnmpTrapOid->u4_Length =
                sizeof (au4capwapBaseWtpState) / sizeof (UINT4);

            /* Forming a VarBind for OID of capwapBaseWtpState and its value */
            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pSnmpTrapOid,
                                                         SNMP_DATA_TYPE_INTEGER,
                                                         0L,
                                                         pWTPTrapInfo->
                                                         i4capwapBaseWtpState,
                                                         NULL, NULL,
                                                         u8CounterVal);

            /* Checking if the pVbList is empty and freeing the memory allocated
               to pSnmpTrapOid and pEnterpriseOid if it is NULL */

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                return;
            }
            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;
            break;

        case CAPWAP_WTP_IMAGE_UPGRADE_INDICATION:
            /* Checking whether Trap status is enabled or not */

            if (gCapwapGlobals.u4CapwTrapStatus != CAPWAP_WTP_TRAP_ENABLE)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                return;
            }

            /* Allocating memory for storing OID of capwapBaseNtfWtpId object
             * and its length*/
            pSnmpTrapOid = alloc_oid (CAPWAP_SNMP_V2_TRAP_OID_LEN);

            /* Checking for error in memory allocation for pSnmpTrapOid and
             * freeing the memory allocated to pEnterpriseOid if there is an
             * error */
            if (pSnmpTrapOid == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            /* Assigning the oid and oid length of capwapBaseNtfWtpId to pSnmpTrapOid */
            MEMCPY (pSnmpTrapOid->pu4_OidList, au4capwapBaseNtfWtpId,
                    sizeof (au4capwapBaseNtfWtpId));
            pSnmpTrapOid->u4_Length =
                sizeof (au4capwapBaseNtfWtpId) / sizeof (UINT4);

            MEMSET (au1MacBuf, 0, MAC_ADDR_LENGTH);

            /* Assigning MacAddress to MacBuf */
            MEMCPY (au1MacBuf, pWTPTrapInfo->au1capwapBaseNtfWtpId,
                    MAC_ADDR_LENGTH);

            /* Converting the normal string to OctetString using SNMP_AGT_FormOctetString 
             * function*/
            pOMacString =
                SNMP_AGT_FormOctetString ((UINT1 *) au1MacBuf,
                                          (INT4) (MAC_ADDR_LENGTH));

            /* Forming a VarBind for NtfWtpId, its value and storing it in
             * pVbList */

            pVbList->pNextVarBind = SNMP_AGT_FormVarBind (pSnmpTrapOid,
                                                          SNMP_DATA_TYPE_OCTET_PRIM,
                                                          0, 0, pOMacString,
                                                          NULL, u8CounterVal);

            /* Checking if the pVbList is empty and freeing the memory allocated
               to pSnmpTrapOid and pEnterpriseOid if it is NULL */
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;

            /* Allocating the memory for pSnmpTrapOid to store OID of
             * fsCapwapImageUpgradeIndi object and length of the OID */
            pSnmpTrapOid = alloc_oid (CAPWAP_SNMP_V2_TRAP_OID_LEN);

            /* Checking for error in memory allocation for pSnmpTrapOid and
             * freeing the memory allocated to pEnterpriseOid if there is an
             * error */

            if (pSnmpTrapOid == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            /* Assigning the OID value and its length of CapwapImageUpgradeIndi
             * to pSnmpTrapOid */
            MEMCPY (pSnmpTrapOid->pu4_OidList, au4fsCapwapWtpImageUpgradeStatus,
                    sizeof (au4fsCapwapWtpImageUpgradeStatus));
            pSnmpTrapOid->u4_Length =
                sizeof (au4fsCapwapWtpImageUpgradeStatus) / sizeof (UINT4);
            /* Forming a VarBind for OID of fsCapwapImageUpgradeIndi and its
             * value */
            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pSnmpTrapOid,
                                                         SNMP_DATA_TYPE_INTEGER,
                                                         0L,
                                                         pWTPTrapInfo->
                                                         i4fsCapwapWtpImageUpgradeStatus,
                                                         NULL, NULL,
                                                         u8CounterVal);

            /* Checking if the pVbList is empty and freeing the memory allocated
             * to pSnmpTrapOid and pEnterpriseOid if it is NULL */
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                return;
            }
            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;
            break;

        case CAPWAP_WTP_IP_ADDR_CHANGE:

            /* Checking whether Trap status is enabled or not */
            if (gCapwapGlobals.u4CapwTrapStatus != CAPWAP_WTP_TRAP_ENABLE)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                return;
            }

            /* Allocating memory for storing OID of capwapBaseNtfWtpId object
             * and its length*/

            pSnmpTrapOid = alloc_oid (CAPWAP_SNMP_V2_TRAP_OID_LEN);

            /* Checking for error in memory allocation for pSnmpTrapOid and
             * freeing the memory allocated to pEnterpriseOid if there is an
             * error*/
            if (pSnmpTrapOid == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            /* Assigning the oid and oid length of capwapBaseNtfWtpId to
             * pSnmpTrapOid */
            MEMCPY (pSnmpTrapOid->pu4_OidList, au4capwapBaseNtfWtpId,
                    sizeof (au4capwapBaseNtfWtpId));
            pSnmpTrapOid->u4_Length =
                sizeof (au4capwapBaseNtfWtpId) / sizeof (UINT4);

            MEMSET (au1MacBuf, 0, MAC_ADDR_LENGTH);

            /* Assigning MacAddress to MacBuf */
            MEMCPY (au1MacBuf, pWTPTrapInfo->au1capwapBaseNtfWtpId,
                    MAC_ADDR_LENGTH);

            /* Converting the normal string to OctetString using
             * SNMP_AGT_FormOctetString function */

            pOMacString =
                SNMP_AGT_FormOctetString ((UINT1 *) au1MacBuf,
                                          (INT4) (MAC_ADDR_LENGTH));

            /* Forming a VarBind for NtfWtpId, its value and storing it in
             * pVbList */

            pVbList->pNextVarBind = SNMP_AGT_FormVarBind (pSnmpTrapOid,
                                                          SNMP_DATA_TYPE_OCTET_PRIM,
                                                          0, 0, pOMacString,
                                                          NULL, u8CounterVal);

            /* Checking if the pVbList is empty and freeing the memory allocated
             * to pSnmpTrapOid and pEnterpriseOid if it is NULL */

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;

            /* Allocating the memory for pSnmpTrapOid to store OID of
             * capwapBaseWtpStateWtpIpAddress object and length of the OID */
            pSnmpTrapOid = alloc_oid (CAPWAP_SNMP_V2_TRAP_OID_LEN);

            /* Checking for error in memory allocation for pSnmpTrapOid and
             * freeing the memory allocated to pEnterpriseOid if there is an
             * error */
            if (pSnmpTrapOid == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            /* Assigning the OID value and its length of
             * capwapBaseWtpStateWtpIpAddress to pSnmpTrapOid */

            MEMCPY (pSnmpTrapOid->pu4_OidList,
                    au4capwapBaseWtpStateWtpIpAddress,
                    sizeof (au4capwapBaseWtpStateWtpIpAddress));
            pSnmpTrapOid->u4_Length =
                sizeof (au4capwapBaseWtpStateWtpIpAddress) / sizeof (UINT4);

            /* Forming a VarBind for OID of capwapBaseWtpStateWtpIpAddress and
             * its value */
            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pSnmpTrapOid,
                                                         SNMP_DATA_TYPE_INTEGER,
                                                         0L,
                                                         pWTPTrapInfo->
                                                         i4capwapBaseWtpStateWtpIpAddress,
                                                         NULL, NULL,
                                                         u8CounterVal);

            /* Checking if the pVbList is empty and freeing the memory allocated
             * to pSnmpTrapOid and pEnterpriseOid if it is NULL */
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                return;
            }
            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;
            break;

        case CAPWAP_WTP_STATE_CHANGE:

            /* Checking whether Trap status is enabled or not */
            if (gCapwapGlobals.u4CapwTrapStatus != CAPWAP_WTP_TRAP_ENABLE)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                return;
            }

            /* Allocating memory for storing OID of capwapBaseNtfWtpId object
             * and its length */

            pSnmpTrapOid = alloc_oid (CAPWAP_SNMP_V2_TRAP_OID_LEN);

            /* Checking for error in memory allocation for pSnmpTrapOid and
             * freeing the memory allocated to pEnterpriseOid if there is an
             * error */
            if (pSnmpTrapOid == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            /* Assigning the oid and oid length of capwapBaseNtfWtpId to
             * pSnmpTrapOid */
            MEMCPY (pSnmpTrapOid->pu4_OidList, au4capwapBaseNtfWtpId,
                    sizeof (au4capwapBaseNtfWtpId));
            pSnmpTrapOid->u4_Length =
                sizeof (au4capwapBaseNtfWtpId) / sizeof (UINT4);

            MEMSET (au1MacBuf, 0, MAC_ADDR_LENGTH);

            /* Assigning MacAddress to MacBuf */
            MEMCPY (au1MacBuf, pWTPTrapInfo->au1capwapBaseNtfWtpId,
                    MAC_ADDR_LENGTH);

            /* Converting the normal string to OctetString using
             * SNMP_AGT_FormOctetString function */

            pOMacString =
                SNMP_AGT_FormOctetString ((UINT1 *) au1MacBuf,
                                          (INT4) (MAC_ADDR_LENGTH));

            /* Forming a VarBind for NtfWtpId, its value and storing it in
             * pVbList */

            pVbList->pNextVarBind = SNMP_AGT_FormVarBind (pSnmpTrapOid,
                                                          SNMP_DATA_TYPE_OCTET_PRIM,
                                                          0, 0, pOMacString,
                                                          NULL, u8CounterVal);

            /* Checking if the pVbList is empty and freeing the memory allocated
             * to pSnmpTrapOid and pEnterpriseOid if it is NULL */

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;

            /* Allocating the memory for pSnmpTrapOid to store OID of
             * capwapBaseWtpState object and length of the OID */
            pSnmpTrapOid = alloc_oid (CAPWAP_SNMP_V2_TRAP_OID_LEN);

            /* Checking for error in memory allocation for pSnmpTrapOid and
             * freeing the memory allocated to pEnterpriseOid if there is an
             * error */
            if (pSnmpTrapOid == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            /* Assigning the OID value and its length of capwapBaseWtpState to
             * pSnmpTrapOid */

            MEMCPY (pSnmpTrapOid->pu4_OidList, au4capwapBaseWtpState,
                    sizeof (au4capwapBaseWtpState));
            pSnmpTrapOid->u4_Length =
                sizeof (au4capwapBaseWtpState) / sizeof (UINT4);
            /* Forming a VarBind for OID of capwapBaseWtpState and its value */
            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pSnmpTrapOid,
                                                         SNMP_DATA_TYPE_INTEGER,
                                                         0L,
                                                         pWTPTrapInfo->
                                                         i4capwapBaseWtpState,
                                                         NULL, NULL,
                                                         u8CounterVal);

            /* Checking if the pVbList is empty and freeing the memory allocated
             * to pSnmpTrapOid and pEnterpriseOid if it is NULL */
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                return;
            }
            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;
            break;

        case CAPWAP_WTP_SYNC_TRAP:

            /* Checking whether Trap status is enabled or not */
            if (gCapwapGlobals.u4CapwTrapStatus != CAPWAP_WTP_TRAP_ENABLE)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                return;
            }

            /* Allocating memory for storing OID of capwapBaseNtfWtpId object
             * and its length */
            pSnmpTrapOid = alloc_oid (CAPWAP_SNMP_V2_TRAP_OID_LEN);

            /* Checking for error in memory allocation for pSnmpTrapOid and
             * freeing the memory allocated to pEnterpriseOid if there is an
             * error */
            if (pSnmpTrapOid == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            /* Assigning the oid and oid length of capwapBaseNtfWtpId to
             * pSnmpTrapOid */
            MEMCPY (pSnmpTrapOid->pu4_OidList, au4capwapBaseNtfWtpId,
                    sizeof (au4capwapBaseNtfWtpId));
            pSnmpTrapOid->u4_Length =
                sizeof (au4capwapBaseNtfWtpId) / sizeof (UINT4);

            MEMSET (au1MacBuf, 0, MAC_ADDR_LENGTH);

            /* Assigning MacAddress to MacBuf */
            MEMCPY (au1MacBuf, pWTPTrapInfo->au1capwapBaseNtfWtpId,
                    MAC_ADDR_LENGTH);

            /* Converting the normal string to OctetString using
             * SNMP_AGT_FormOctetString function */
            pOMacString =
                SNMP_AGT_FormOctetString ((UINT1 *) au1MacBuf,
                                          (INT4) (MAC_ADDR_LENGTH));

            /* Forming a VarBind for NtfWtpId, its value and storing it in
             * pVbList */

            pVbList->pNextVarBind = SNMP_AGT_FormVarBind (pSnmpTrapOid,
                                                          SNMP_DATA_TYPE_OCTET_PRIM,
                                                          0, 0, pOMacString,
                                                          NULL, u8CounterVal);

            /* Checking if the pVbList is empty and freeing the memory allocated
             * to pSnmpTrapOid and pEnterpriseOid if it is NULL */

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;

            /* Allocating memory for storing OID of capwapBaseWtpMacTypeOptions
             * object and its length */

            pSnmpTrapOid = alloc_oid (CAPWAP_SNMP_V2_TRAP_OID_LEN);

            /* Checking for error in memory allocation for pSnmpTrapOid and
             * freeing the memory allocated to pEnterpriseOid if there is an
             * error */

            if (pSnmpTrapOid == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            /* Assigning the OID value and its length of
             * fsCapwapBaseSyncFailueStatus to pSnmpTrapOid */

            MEMCPY (pSnmpTrapOid->pu4_OidList, au4capwapBaseWtpMacTypeOptions,
                    sizeof (au4capwapBaseWtpMacTypeOptions));
            pSnmpTrapOid->u4_Length =
                sizeof (au4capwapBaseWtpMacTypeOptions) / sizeof (UINT4);

            /* Forming a VarBind for OID of capwapBaseWtpMacTypeOptions and its
             * value */
            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pSnmpTrapOid,
                                                         SNMP_DATA_TYPE_INTEGER,
                                                         0L,
                                                         pWTPTrapInfo->
                                                         i4capwapBaseWtpMacTypeOptions,
                                                         NULL, NULL,
                                                         u8CounterVal);

            /* Checking if the pVbList is empty and freeing the memory allocated
             * to pSnmpTrapOid and pEnterpriseOid if it is NULL */

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                return;
            }
            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;

            /* Allocating memory for storing OID of capwapBaseWtpTunnelModeOptions
             * object and its length */

            pSnmpTrapOid = alloc_oid (CAPWAP_SNMP_V2_TRAP_OID_LEN);

            /* Checking for error in memory allocation for pSnmpTrapOid and
             * freeing the memory allocated to pEnterpriseOid if there is an
             * error */

            if (pSnmpTrapOid == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            /* Assigning the OID value and its length of
             * fsCapwapBaseSyncFailueStatus to pSnmpTrapOid */

            MEMCPY (pSnmpTrapOid->pu4_OidList,
                    au4capwapBaseWtpTunnelModeOptions,
                    sizeof (au4capwapBaseWtpTunnelModeOptions));
            pSnmpTrapOid->u4_Length =
                sizeof (au4capwapBaseWtpTunnelModeOptions) / sizeof (UINT4);

            /* Forming a VarBind for OID of capwapBaseWtpTunnelModeOptions and its
             * value */
            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pSnmpTrapOid,
                                                         SNMP_DATA_TYPE_INTEGER,
                                                         0L,
                                                         pWTPTrapInfo->
                                                         i4capwapBaseWtpTunnelModeOptions,
                                                         NULL, NULL,
                                                         u8CounterVal);

            /* Checking if the pVbList is empty and freeing the memory allocated
             * to pSnmpTrapOid and pEnterpriseOid if it is NULL */

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                return;
            }
            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;
            break;

        case CAPWAP_WTP_IMAGE_NOT_SUPPORTED:

            /* Checking whether Trap status is enabled or not */
            if (gCapwapGlobals.u4CapwTrapStatus != CAPWAP_WTP_TRAP_ENABLE)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                return;
            }

            /* Allocating memory for storing OID of capwapBaseNtfWtpId object
             * and its length */

            pSnmpTrapOid = alloc_oid (CAPWAP_SNMP_V2_TRAP_OID_LEN);

            /* Checking for error in memory allocation for pSnmpTrapOid and
             * freeing the memory allocated to pEnterpriseOid if there is an
             * error */
            if (pSnmpTrapOid == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            /* Assigning the oid and oid length of capwapBaseNtfWtpId to
             * pSnmpTrapOid */
            MEMCPY (pSnmpTrapOid->pu4_OidList, au4capwapBaseNtfWtpId,
                    sizeof (au4capwapBaseNtfWtpId));
            pSnmpTrapOid->u4_Length =
                sizeof (au4capwapBaseNtfWtpId) / sizeof (UINT4);

            MEMSET (au1MacBuf, 0, MAC_ADDR_LENGTH);

            /* Assigning MacAddress to MacBuf */
            MEMCPY (au1MacBuf, pWTPTrapInfo->au1capwapBaseNtfWtpId,
                    MAC_ADDR_LENGTH);

            /* Converting the normal string to OctetString using
             * SNMP_AGT_FormOctetString function */

            pOMacString =
                SNMP_AGT_FormOctetString ((UINT1 *) au1MacBuf,
                                          (INT4) (MAC_ADDR_LENGTH));

            /* Forming a VarBind for NtfWtpId, its value and storing it in
             * pVbList */

            pVbList->pNextVarBind = SNMP_AGT_FormVarBind (pSnmpTrapOid,
                                                          SNMP_DATA_TYPE_OCTET_PRIM,
                                                          0, 0, pOMacString,
                                                          NULL, u8CounterVal);

            /* Checking if the pVbList is empty and freeing the memory allocated
             * to pSnmpTrapOid and pEnterpriseOid if it is NULL */

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;

            /* Allocating the memory for pSnmpTrapOid to store OID of
             * fsCapwapImageName object and length of the OID */
            pSnmpTrapOid = alloc_oid (CAPWAP_SNMP_V2_TRAP_OID_LEN);

            /* Checking for error in memory allocation for pSnmpTrapOid and
             * freeing the memory allocated to pEnterpriseOid if there is an
             * error */
            if (pSnmpTrapOid == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            MEMSET (au1WtpImageNameBuf, 0, 1024);

            /* Assigning WtpImageName to WtpImageNameBuf */
            MEMCPY (au1WtpImageNameBuf, pWTPTrapInfo->au1fsCapwapWtpImageName,
                    sizeof (pWTPTrapInfo->au1fsCapwapWtpImageName));
            /* Converting the normal string to OctetString using
             * SNMP_AGT_FormOctetString function */

            pOWtpImageNameString =
                SNMP_AGT_FormOctetString ((UINT1 *) au1WtpImageNameBuf,
                                          (INT4) (STRLEN
                                                  (pWTPTrapInfo->
                                                   au1fsCapwapWtpImageName)));

            /* Assigning the OID value and its length of fsCapwapWtpImageName to
             * pSnmpTrapOid */

            MEMCPY (pSnmpTrapOid->pu4_OidList, au4fsCapwapWtpImageName,
                    sizeof (au4fsCapwapWtpImageName));
            pSnmpTrapOid->u4_Length =
                sizeof (au4fsCapwapWtpImageName) / sizeof (UINT4);
            /* Forming a VarBind for OID of fsCapwapWtpImageName and its value */
            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pSnmpTrapOid,
                                                         SNMP_DATA_TYPE_OCTET_PRIM,
                                                         0, 0,
                                                         pOWtpImageNameString,
                                                         NULL, u8CounterVal);

            /* Checking if the pVbList is empty and freeing the memory allocated
             * to pSnmpTrapOid and pEnterpriseOid if it is NULL */
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                return;
            }
            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;
            break;

        default:
            break;

    }
#ifdef SNMP_2_WANTED
    /* The following API sends the Trap info to the FutureSNMP Agent. */
    SNMP_AGT_RIF_Notify_v2Trap (pStartVb);

    /* Replace this function with the Corresponding Function provided by 
     * SNMP Agent for Notifying Traps.
     */
#endif
#else

    UNUSED_PARAM (u1TrapId);
    UNUSED_PARAM (pTrapInfo);

#endif /* FUTURE_SNMP_WANTED */

#ifdef DEBUG_WANTED
    DbgNotifyTrap (u1TrapId, pTrapInfo);
#endif
}
#endif
