/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 * $Id: capwaprun.c,v 1.3 2017/11/24 10:37:03 siva Exp $       
 * Description: This file contains the CAPWAP Join state routines
 *
 *******************************************************************/
#ifndef __CAPWAP_RUN_C__
#define __CAPWAP_RUN_C__
#include "capwapinc.h"
#include "wssifpmdb.h"
#include "aphdlrinc.h"

#ifdef KERNEL_CAPWAP_WANTED
#include "fcusprot.h"
#endif
#ifdef HOSTAPD_WANTED
extern UINT1        gu1HostApdReset;
#endif

#ifdef WTP_WANTED
#if defined (LNXIP4_WANTED)
#include "wsssta.h"
#endif
#endif

#if defined (LNXIP4_WANTED)
extern UINT1       *CfaGddGetLnxIntfnameForPort (UINT2 u2Index);
#endif
extern INT4
        WlchdlrFreeTransmittedPacket (tRemoteSessionManager * pSessEntry);
static UINT4        gu4KeepAliveSent;

/************************************************************************/
/*  Function Name   : CapwapProcessChangeStateEvntRequest               */
/*  Description     : The function processes the received CAPWAP        */
/*                    Config status request                             */
/*  Input(s)        : pStateEventReqMsg - Parsed CAPWAP chnage state    */
/*                    event request message.                            */
/*                    pSessEntry - Remote session entry                 */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/

INT4
CapwapProcessChangeStateEvntRequest (tCRU_BUF_CHAIN_HEADER * pBuf,
                                     tCapwapControlPacket * pStateEventReqMsg,
                                     tRemoteSessionManager * pSessEntry)
{
    INT4                i4Status;
    UINT1              *pu1TxBuf = NULL, *pRcvdBuf = NULL;
    tCRU_BUF_CHAIN_HEADER *pTxBuf;
    tChangeStateEvtRsp  StateEvntResp;
    UINT1               u1IsNotLinear = OSIX_FALSE;
    UINT2               u2PacketLen;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;

    CAPWAP_FN_ENTRY ();

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    if (pSessEntry == NULL || (pSessEntry->eCurrentState != CAPWAP_CONFIGURE))
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Session Entry is NULL, Return Failure \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Session Entry is NULL, Return Failure \r\n"));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    /* stop the change State pending  interval timer
     * and clear the packet buffer pointer */
    if (CapwapTmrStop (pSessEntry, CAPWAP_CHANGESTATE_PENDING_TMR) ==
        OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_MGMT_TRC,
                    "Stopped the Change State Pending timer \r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                      "Stopped the Change State Pending timer \r\n"));
        /* clear the packet buffer pointer */
#ifdef WLC_WANTED
        WlchdlrFreeTransmittedPacket (pSessEntry);
#else
        AphdlrFreeTransmittedPacket (pSessEntry);
#endif
    }

    u2PacketLen = (UINT2) CAPWAP_GET_BUF_LEN (pBuf);
    pRcvdBuf = CAPWAP_BUF_IF_LINEAR (pBuf, 0, u2PacketLen);
    if (pRcvdBuf == NULL)
    {
        pRcvdBuf = (UINT1 *) MemAllocMemBlk (CAPWAP_PKTBUF_POOLID);
        CAPWAP_COPY_FROM_BUF (pBuf, pRcvdBuf, 0, u2PacketLen);
        u1IsNotLinear = OSIX_TRUE;
    }
    if ((CapwapValidateChangeStateEventReqMsg (pRcvdBuf, pStateEventReqMsg,
                                               pSessEntry) == OSIX_SUCCESS))
    {
        if (u1IsNotLinear == OSIX_TRUE)
        {
            MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pRcvdBuf);
        }

        /* Update the Remote session state */
        pSessEntry->eCurrentState = CAPWAP_DATACHECK;
        CAPWAP_TRC (CAPWAP_FSM_TRC, "Entered CAPWAP DATACHECK State \r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                      "Entered CAPWAP DATACHECK State \r\n"));
        MEMSET (&StateEvntResp, 0, sizeof (tChangeStateEvtRsp));
        if (CapwapGetStateEventResponseMsgElements (&StateEvntResp) ==
            CAPWAP_SUCCESS)
        {
            /* update the seq number */
            StateEvntResp.u1SeqNum = pStateEventReqMsg->capwapCtrlHdr.u1SeqNum;

            pTxBuf = CAPWAP_ALLOCATE_CRU_BUF (CAPWAP_MAX_PKT_LEN, 0);
            if (pTxBuf == NULL)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Memory Allocation Failed\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Memory Allocation Failed\n"));
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
            pu1TxBuf = CAPWAP_BUF_IF_LINEAR (pTxBuf, 0, CAPWAP_MAX_PKT_LEN);
            if (pu1TxBuf == NULL)
            {
                pu1TxBuf = (UINT1 *) MemAllocMemBlk (CAPWAP_PKTBUF_POOLID);
                CAPWAP_COPY_FROM_BUF (pTxBuf, pu1TxBuf, 0, CAPWAP_MAX_PKT_LEN);
                u1IsNotLinear = OSIX_TRUE;
            }
            if ((CapwapAssembleChangeStateEventResponse (StateEvntResp.
                                                         u2CapwapMsgElemenLen,
                                                         &StateEvntResp,
                                                         pu1TxBuf)) ==
                OSIX_FAILURE)
            {
                if (u1IsNotLinear == OSIX_TRUE)
                {
                    MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pu1TxBuf);
                }
                CAPWAP_RELEASE_CRU_BUF (pTxBuf);
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
            if (u1IsNotLinear == OSIX_TRUE)
            {
                /* If the CRU buffer memory is not linear */
                CAPWAP_COPY_TO_BUF (pTxBuf, pu1TxBuf, 0, CAPWAP_MAX_PKT_LEN);
                MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pu1TxBuf);

            }

            i4Status = CapwapTxMessage (pTxBuf,
                                        pSessEntry,
                                        (UINT4) (StateEvntResp.
                                                 u2CapwapMsgElemenLen +
                                                 CAPWAP_SOCK_HDR_LEN),
                                        WSS_CAPWAP_802_11_CTRL_PKT);
            if (i4Status == OSIX_SUCCESS)
            {
                /* Update the last transmitted sequence number */
                pSessEntry->lastProcesseddSeqNum = StateEvntResp.u1SeqNum;
                /* store the packet buffer pointer in
                 *                  * the remote session DB copy the pointer */
                pSessEntry->lastTransmittedPkt = pTxBuf;
                pSessEntry->lastTransmittedPktLen = (UINT4) (StateEvntResp.
                                                             u2CapwapMsgElemenLen
                                                             + 13);
                pSessEntry->lastTransmitedSeqNum++;
                /* Start Wait Join Timer */
                CapwapTmrStart (pSessEntry, CAPWAP_DATACHECK_TMR,
                                CAPWAP_WAITJOIN_TIMEOUT);
            }
            else
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Failed to Transmit the packet \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to Transmit the packet \r\n"));
                CAPWAP_RELEASE_CRU_BUF (pTxBuf);
                pSessEntry->lastTransmittedPkt = NULL;
                pSessEntry->lastTransmittedPktLen = 0;
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
        }
    }
    else
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to validate the received State Event request \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to validate the received State Event request \r\n"));
        CAPWAP_TRC (CAPWAP_FSM_TRC, "Enter the CAPWAP DTLS TD State \r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                      "Enter the CAPWAP DTLS TD State \r\n"));
        pSessEntry->eCurrentState = CAPWAP_DTLSTD;
        if (CapwapShutDownDTLS (pSessEntry) == OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Disconnect"
                        "the Session \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to Disconnect" "the Session \r\n"));
        }
        pWssIfCapwapDB->u4DestIp = pSessEntry->remoteIpAddr.u4_addr[0];
        pWssIfCapwapDB->u4DestPort = pSessEntry->u4RemoteCtrlPort;
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_DELETE_RSM, pWssIfCapwapDB) ==
            OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Delete the RSM"
                        "from CAPWAP DB \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to Delete the RSM" "from CAPWAP DB \r\n"));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
        if (u1IsNotLinear == OSIX_TRUE)
        {
            MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pRcvdBuf);
        }
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapValidateChangeStateEventReqMsg                    */
/*  Description     : The function validates the functional             */
/*                    characteristics of the received CAPWAP            */
/*                    Join request agains the configured                */
/*                    WTP profile                                       */
/*  Input(s)        : discReqMsg - parsed CAPWAP Discovery request      */
/*                    pRcvBuf - Received discovery packet               */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
CapwapValidateChangeStateEventReqMsg (UINT1 *pRcvBuf,
                                      tCapwapControlPacket * stateEvntReqMsg,
                                      tRemoteSessionManager * pSeeEntry)
{
    UNUSED_PARAM (pSeeEntry);
    CAPWAP_FN_ENTRY ();
    tRadioIfOperStatus  radioOper;

    /* Validate the mandatory message elements */
    if (CapwapValidateRadioOperState (pRcvBuf, stateEvntReqMsg, &radioOper,
                                      RADIO_OPER_STATE_EVENT_REQ_INDEX) ==
        OSIX_SUCCESS)
    {
        return OSIX_SUCCESS;
    }

    if (CapwapSetRadioOperState (pRcvBuf, stateEvntReqMsg, &radioOper,
                                 RADIO_OPER_STATE_EVENT_REQ_INDEX) ==
        OSIX_SUCCESS)
    {
        return OSIX_SUCCESS;
    }

    /* Validate optional elements */
    /****************************TBD ***************************/

    CAPWAP_FN_EXIT ();
    return OSIX_FAILURE;

}

/************************************************************************/
/*  Function Name   : CapwapProcessChangeStateEvntResponse              */
/*  Description     : The function processes the received CAPWAP        */
/*                    join response. This function handling is mainly   */
/*                    done in the AP and it validates the received join */
/*                    response and updates WSS-AP.                      */
/*  Input(s)        : JoinRespMsg- Join response packet received        */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
CapwapProcessChangeStateEvntResponse (tSegment * pBuf,
                                      tCapwapControlPacket * stateEvntRespMsg,
                                      tRemoteSessionManager * pSessEntry)
{
    UINT1              *pRcvBuf = NULL;
    INT4                i4Status = OSIX_SUCCESS;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tWssIfCapDB        *pWssIfCapwapDBTmr = NULL;
    UINT2               u2PacketLen;
    UINT1               u1IsNotLinear = OSIX_FALSE;
#ifdef DAK_WANTED
    UINT2               u2IfIndex = 0;
#endif
#ifdef WTP_WANTED
    UINT1               u1WtpLocalRouting = 0;
#if defined (LNXIP4_WANTED)
    UINT4               u4Index;
    UINT1              *pDevName = NULL;
#endif
#endif
    UINT2               u2IntId = 0;
    UINT2               u2EchoInterval = 0;

    CAPWAP_FN_ENTRY ();

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDBTmr, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (pWssIfCapwapDBTmr, 0, sizeof (tWssIfCapDB));

    if (pSessEntry == NULL || pSessEntry->eCurrentState != CAPWAP_DATACHECK)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "CAPWAP session Entry is"
                    "Null, Return Failure \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "CAPWAP session Entry is" "Null, Return Failure \r\n"));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDBTmr);
        return OSIX_FAILURE;
    }

    pWssIfCapwapDBTmr->CapwapIsGetAllDB.bWtpEchoInterval = OSIX_TRUE;
    pWssIfCapwapDBTmr->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    pWssIfCapwapDBTmr->CapwapGetDB.u2WtpInternalId = u2IntId;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDBTmr) ==
        OSIX_SUCCESS)
    {
        u2EchoInterval = pWssIfCapwapDBTmr->CapwapGetDB.u2WtpEchoInterval;
    }

    /* stop the retransmit interval timer
     * and clear the packet buffer pointer */
    if (CapwapTmrStop (pSessEntry, CAPWAP_RETRANSMIT_INTERVAL_TMR) ==
        OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_MGMT_TRC, "Stopped the retransmit timer \r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                      "Stopped the retransmit timer \r\n"));
        /* clear the packet buffer pointer */
#ifdef WLC_WANTED
        WlchdlrFreeTransmittedPacket (pSessEntry);
#else
        AphdlrFreeTransmittedPacket (pSessEntry);
#endif
    }
    u2PacketLen = (UINT2) CAPWAP_GET_BUF_LEN (pBuf);
    pRcvBuf = CAPWAP_BUF_IF_LINEAR (pBuf, 0, u2PacketLen);
    if (pRcvBuf == NULL)
    {
        pRcvBuf = (UINT1 *) MemAllocMemBlk (CAPWAP_PKTBUF_POOLID);
        CAPWAP_COPY_FROM_BUF (pBuf, pRcvBuf, 0, u2PacketLen);
        u1IsNotLinear = OSIX_TRUE;
    }

    /* Validate the state event response message. if it is success
       construct the Config Status Response */
    if ((CapwapValidateStateEventRespMsg (pRcvBuf, stateEvntRespMsg) ==
         OSIX_SUCCESS))
    {
        if (u1IsNotLinear == OSIX_TRUE)
        {
            MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pRcvBuf);
        }

        /* Update the Remote session state */
        pSessEntry->eCurrentState = CAPWAP_RUN;
        pSessEntry->ePrevState = CAPWAP_RUN;
        CAPWAP_TRC (CAPWAP_FSM_TRC, "Entered CAPWAP RUN State \r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                      "Entered CAPWAP RUN State \r\n"));

        CAPWAP_TRC1 (CAPWAP_FSM_TRC, "\r\n Entered CAPWAP RUN State with the"
                     "WLC Ip address = %x ",
                     pSessEntry->remoteIpAddr.u4_addr[0]);
        SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4CapwapSysLogId,
                      "\r\n Entered CAPWAP RUN State with the"
                      "WLC Ip address = %x ",
                      pSessEntry->remoteIpAddr.u4_addr[0]));

#ifdef WTP_WANTED
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpStatisticsTimer = OSIX_TRUE;
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = stateEvntRespMsg->
            u2IntProfileId;
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpLocalRouting = OSIX_TRUE;
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB) ==
            OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Unable to get statistics"
                        "timer from DB \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Unable to get statistics" "timer from DB \r\n"));
        }

        u1WtpLocalRouting = pWssIfCapwapDB->CapwapGetDB.u1WtpLocalRouting;

        /*Here the ifindex is the physical interface which is the next interface 
         * to the br-lan in the Mapping table*/

#if defined (LNXIP4_WANTED)
        u4Index = 2;
        if (CfaValidateIfMainTableIndex (u4Index) == OSIX_SUCCESS)
        {
            pDevName = CfaGddGetLnxIntfnameForPort ((UINT2) u4Index);
            if (pDevName != NULL)
            {
#ifdef KERNEL_CAPWAP_WANTED
#ifdef DAK_WANTED
                for (u2IfIndex = SYS_DEF_TGT_MAX_ENET_INTERFACES; u2IfIndex <=
                     SYS_DEF_TGT_MAX_ENET_INTERFACES + 1; u2IfIndex++)
                {
                    pDevName = CfaGddGetLnxIntfnameForPort (u2IfIndex);
                    WssIfWmmQueueCreation (pDevName);
                }
#else
                WssIfWmmQueueCreation (pDevName);
#endif
#endif
            }
        }
#endif

#ifdef HOSTAPD_WANTED
        /* CAPWAP RUN state is reached. This indicates that 
         * hostapd has to be reset. The timer is also started here,once the timer
         * expires the  NPAPI call to restart the hostapd 
         * process will be called */
        gu1HostApdReset = OSIX_TRUE;
        if (ApHdlrTmrStart (NULL, APHDLR_HOSTAPD_INTERVAL_TMR,
                            APHDLR_HOSTAPD_INTERVAL_TMR_VAL) != OSIX_SUCCESS)
        {
            APHDLR_TRC (APHDLR_FAILURE_TRC, "%s::%d "
                        "APHDLR_Hostapd_Timer Start" "failure\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId, "%s::%d "
                          "APHDLR_Hostapd_Timer Start" "failure\n"));
        }
#endif

#ifdef KERNEL_CAPWAP_WANTED
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
        pWssIfCapwapDB->CapwapIsGetAllDB.bWlcIpAddress = OSIX_TRUE;
        pWssIfCapwapDB->CapwapIsGetAllDB.bWlcCtrlPort = OSIX_TRUE;
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;

        pWssIfCapwapDB->CapwapGetDB.u4WlcIpAddress =
            pSessEntry->remoteIpAddr.u4_addr[0];
        pWssIfCapwapDB->CapwapGetDB.u4WlcCtrlPort =
            (UINT2) pSessEntry->u4RemoteCtrlPort;
        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
            stateEvntRespMsg->u2IntProfileId;

        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_SET_DB, pWssIfCapwapDB) ==
            OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Unable to set"
                        "session details to DB\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId, "Unable to set"
                          "session details to DB\r\n"));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDBTmr);
            return OSIX_FAILURE;
        }
#endif
#endif

        /* Initiate the Data Channel to the WLC */
        if (CapwapInitiateDataChannel (pSessEntry) == OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Initiate"
                        "DataChannel \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to Initiate" "DataChannel \r\n"));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDBTmr);
            return OSIX_FAILURE;
        }
#ifdef WTP_WANTED
#ifdef KERNEL_CAPWAP_WANTED
        if (CapwapUpdateWtpKernelDB () == OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to update"
                        "kernel DBDB\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to update" "kernel DBDB\r\n"));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDBTmr);
            return OSIX_FAILURE;
        }
#ifdef NPAPI_WANTED
        if (RadioIfSetLocalRoutingStatus (u1WtpLocalRouting) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "RadioIfSetLocalRoutingStatus failed \n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "RadioIfSetLocalRoutingStatus failed \n"));
        }
#endif
#endif
#endif
        CapwapTmrStart (pSessEntry, CAPWAP_DATACHNL_DEADINTERVAL_TMR,
                        CAPWAP_DATACHNL_DEADINTERVAL_TIMEOUT);
        CapwapTmrStart (pSessEntry, CAPWAP_ECHO_INTERVAL_TMR, u2EchoInterval);
#ifdef WTP_WANTED
        CapwapDiscTmrStart (CAPWAP_PRIMARY_DISC_INTERVAL_TMR,
                            PRIMARY_DISC_INTERVAL_TIMEOUT);
#endif

        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDBTmr);
        return i4Status;
    }
    else
    {
        CAPWAP_TRC (CAPWAP_FSM_TRC, "Enter the CAPWAP DTLS TD State \r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                      "Enter the CAPWAP DTLS TD State \r\n"));
        pSessEntry->eCurrentState = CAPWAP_DTLSTD;
        if (CapwapShutDownDTLS (pSessEntry) == OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Disconnect"
                        "the Session \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to Disconnect" "the Session \r\n"));
        }
        /* Cleare the Entry in Remote session Module */
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
        pWssIfCapwapDB->u4DestIp = pSessEntry->remoteIpAddr.u4_addr[0];
        pWssIfCapwapDB->u4DestPort = pSessEntry->u4RemoteCtrlPort;
        WssIfProcessCapwapDBMsg (WSS_CAPWAP_DELETE_RSM, pWssIfCapwapDB);
#ifdef WTP_WANTED
        pSessEntry->eCurrentState = CAPWAP_IDLE;
        CapwapSendRestartDiscEvent ();
#endif
        if (u1IsNotLinear == OSIX_TRUE)
        {
            MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pRcvBuf);
        }
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to validate the received State"
                    "Event response \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to validate the received State"
                      "Event response \r\n"));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDBTmr);
        return OSIX_FAILURE;
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDBTmr);
#ifdef WTP_WANTED
    UNUSED_PARAM (u1WtpLocalRouting);
#endif
    /* TBD - Updatet he sequence number */
    CAPWAP_FN_EXIT ()return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapGetStateEventResponseMsgElements            */
/*  Description     : The function validates the functional             */
/*                    characteristics of the received CAPWAP            */
/*                    Config Status response against the configured     */
/*                    WTP profile                                       */
/*  Input(s)        : discReqMsg - parsed CAPWAP Discovery request      */
/*                    pRcvBuf - Received discovery packet               */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
CapwapGetStateEventResponseMsgElements (tChangeStateEvtRsp * pStateEvntResp)
{
    UINT4               u4MsgLen = 0;
    /* update capwap header */
    if (CapwapConstructCpHeader (&pStateEvntResp->capwapHdr) == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "capwapGetStateEventResponseMsgElements:Failed to"
                    "construct the capwap Header\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "capwapGetStateEventResponseMsgElements:Failed to"
                      "construct the capwap Header\n"));
        return OSIX_FAILURE;
    }

    /* Mandatory elements - None */
    /* update Optional elements  */
    if (CapwapGetVendorPayload (&pStateEvntResp->vendSpec, &u4MsgLen) ==
        OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to get Optional Event Response Message"
                    "Elements \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to get Optional Event Response Message"
                      "Elements \r\n"));
        return CAPWAP_FAILURE;
    }

    pStateEvntResp->u2CapwapMsgElemenLen = (UINT2) (u4MsgLen + 3);
    pStateEvntResp->u1NumMsgBlocks = 0;    /* flags always zero */

    return CAPWAP_SUCCESS;

}

/************************************************************************/
/*  Function Name   : CapwapValidateStateEventRespMsg                   */
/*  Description     : The function validates the functional             */
/*                    characteristics of the received CAPWAP            */
/*                    Join request agains the configured                */
/*                    WTP profile                                       */
/*  Input(s)        : discReqMsg - parsed CAPWAP Discovery request      */
/*                    pRcvBuf - Received discovery packet               */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
CapwapValidateStateEventRespMsg (UINT1 *pRcvBuf,
                                 tCapwapControlPacket * stateEvntRespMsg)
{

    CAPWAP_FN_ENTRY ();

    /* No mandatory message elements */

    /* Validate optional elements */
    /****************************TBD ***************************/
    UNUSED_PARAM (pRcvBuf);
    UNUSED_PARAM (stateEvntRespMsg);
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;

}

INT4
CapwapInitiateDataChannel (tRemoteSessionManager * pSessEntry)
{

    /* Inform the CAPWAP Data Hanlder to initiate the CAPWAP Tunnel */
    /* Add the capwap Data channel to the select utility */
    CapwapDataSelAddFd ();
    if (CapwapTransmitKeepAlivePkt (pSessEntry) == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to Transmit the keep alive packet on"
                    "data channel \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to Transmit the keep alive packet on"
                      "data channel \r\n"));
        return OSIX_FAILURE;

    }
    return OSIX_SUCCESS;
}

INT4
CapwapTransmitKeepAlivePkt (tRemoteSessionManager * pSessEntry)
{
    tKeepAlivePkt       KeepAlivePkt;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tQueData            QueData;
#ifdef WLC_WANTED
    tWssIfPMDB          WssIfPMDB;
#endif
    /* Counter */
    gu4KeepAliveSent++;
    CAPWAP_TRC1 (CAPWAP_STATS_TRC, "Keep Alive packets"
                 "Transmitted = %d \r\n", gu4KeepAliveSent);
    SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4CapwapSysLogId, "Keep Alive packets"
                  "Transmitted = %d \r\n", gu4KeepAliveSent));
    CAPWAP_FN_ENTRY ();
    if (pSessEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Session Entry is"
                    "NULL, Return Failure \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId, "Session Entry is"
                      "NULL, Return Failure \r\n"));
        return OSIX_FAILURE;
    }
    /* Construct CAPWAP Header */
    KeepAlivePkt.capwapHdr.u1Preamble = 0;
    KeepAlivePkt.capwapHdr.u2HlenRidWbidFlagT = 0x1000;
    KeepAlivePkt.capwapHdr.u1FlagsFLWMKResd = 0x08;    /* set the K-Flag for
                                                       Keep Alive packet */
    KeepAlivePkt.capwapHdr.u2FragId = 0;
    KeepAlivePkt.capwapHdr.u2FragOffsetRes3 = 0;
    KeepAlivePkt.capwapHdr.u1RadMacAddrLength = 0;
    KeepAlivePkt.capwapHdr.u1WirelessInfoLength = 0;
    KeepAlivePkt.capwapHdr.u2HdrLen = 8;

    /* update the Join Session Id */
    MEMCPY (&KeepAlivePkt.sessionID, pSessEntry->JoinSessionID, 16);
    KeepAlivePkt.u2CapwapMsgElemenLen = 22;
    pBuf = CAPWAP_ALLOCATE_CRU_BUF (CAPWAP_MAX_PKT_LEN, 0);
    if (pBuf == NULL)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Memory Allocation Failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Memory Allocation Failed \r\n"));
        return OSIX_FAILURE;
    }
    if (CapwapAssembleKeepAlivePkt (&KeepAlivePkt, pBuf) == OSIX_FAILURE)
    {
        CAPWAP_RELEASE_CRU_BUF (pBuf);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "capwapTransmitKeepAlivePkt:Failed to Assemble"
                    "the packet \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "capwapTransmitKeepAlivePkt:Failed to Assemble"
                      "the packet \r\n"));
        return OSIX_FAILURE;
    }
    /* CapwapGetDataUdpPort (&u4DataPort) */

    /*muthu */
    /* pSessEntry->u4RemoteDataPort = WssGetCapwapUdpSPortFromWlcNvRam() + 1; */
    pSessEntry->u4RemoteDataPort = 5247;
    if ((pSessEntry->u1DataDtlsFlag) == DTLS_CFG_DISABLE)
    {
        CAPWAP_TRC (CAPWAP_MGMT_TRC,
                    "DTLS is disabled for data pkts"
                    "send the plain text packet \r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                      "DTLS is disabled for data pkts"
                      "send the plain text packet \r\n"));
        if (CapwapTxMessage (pBuf, pSessEntry,
                             (UINT4) (KeepAlivePkt.u2CapwapMsgElemenLen +
                                      CAPWAP_MSG_ELE_LEN),
                             WSS_CAPWAP_802_11_DATA_PKT) != OSIX_SUCCESS)
        {
            CAPWAP_RELEASE_CRU_BUF (pBuf);
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "capwapTransmitKeepAlivePkt:Failed to Transmit"
                        "the packet \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "capwapTransmitKeepAlivePkt:Failed to Transmit"
                          "the packet \r\n"));
            return OSIX_FAILURE;
        }
    }
    else
    {
        MEMSET (&QueData, 0, sizeof (tQueData));
        QueData.pBuffer = pBuf;
        QueData.eQueDataType = DATA_ENCRYPT;
        QueData.u4BufSize = (UINT4) (KeepAlivePkt.u2CapwapMsgElemenLen + 13);
        QueData.u4IpAddr = pSessEntry->remoteIpAddr.u4_addr[0];
        QueData.u4PortNo = pSessEntry->u4RemoteDataPort;
        QueData.u4Id = pSessEntry->u4DtlsSessId;
        QueData.u4Channel = DTLS_DATA_CHANNEL;

        if (CapwapDtlsDecryptNotify (&QueData) == OSIX_FAILURE)
        {
            CAPWAP_RELEASE_CRU_BUF (pBuf);
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "DTLS decryption Failed for data packets\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "DTLS decryption Failed for data packets\r\n"));
            return OSIX_FAILURE;
        }
    }

#ifdef WTP_WANTED
    pSessEntry->wtpKeepAlivePktsSent++;
#else
    MEMSET (&WssIfPMDB, 0, sizeof (WssIfPMDB));
    WssIfPMDB.u2ProfileId = pSessEntry->u2IntProfileId;
    WssIfPMDB.tWtpPmAttState.wssPmWLCStatsSetDB.bWssPmWLCCapwapJoinReqRxd =
        OSIX_TRUE;
    if (WssIfProcessPMDBMsg (WSS_PM_WLC_CAPWAP_STATS_SET, &WssIfPMDB) !=
        OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to update Join Req Stats to PM \n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to update Join Req Stats to PM \n"));
    }
#endif

    CAPWAP_RELEASE_CRU_BUF (pBuf);
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

INT4
CapwapTransmitEchoRequest (tRemoteSessionManager * pSess)
{
    UINT4               u4MsgLen = 0;
    tEchoReq            EchoReq;
    UINT1              *pu1TxBuf = NULL;
    tCRU_BUF_CHAIN_HEADER *pBuf;
    UINT1               u1IsNotLinear = OSIX_FALSE;

    CAPWAP_FN_ENTRY ();
    MEMSET (&EchoReq, 0, sizeof (tEchoReq));
    /* update CAPWAP Header */
    if (CapwapConstructCpHeader (&EchoReq.capwapHdr) == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to construct"
                    "the capwap Header\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to construct" "the capwap Header\n"));
        return OSIX_FAILURE;
    }

    /* update Message elements - None */
    /* update Optional Message Elements */
    if (CapwapGetVendorPayload (&EchoReq.vendSpec, &u4MsgLen) == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to get Optional Message Elements \n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to get Optional Message Elements \n"));
        return OSIX_FAILURE;
    }
    /* update Control Header */
    EchoReq.u1SeqNum = (UINT1) (pSess->lastTransmitedSeqNum + 1);
    EchoReq.u2CapwapMsgElemenLen = (UINT2) (u4MsgLen + 3);
    EchoReq.u1NumMsgBlocks = 0;    /* flags */

    pBuf = CAPWAP_ALLOCATE_CRU_BUF (CAPWAP_MAX_PKT_LEN, 0);
    if (pBuf == NULL)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Memory Allocation Failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Memory Allocation Failed \r\n"));
        return OSIX_FAILURE;
    }

    pu1TxBuf = CAPWAP_BUF_IF_LINEAR (pBuf, 0, CAPWAP_MAX_PKT_LEN);
    if (pu1TxBuf == NULL)
    {
        pu1TxBuf = (UINT1 *) MemAllocMemBlk (CAPWAP_PKTBUF_POOLID);
        /* Kloc Fix Start */
        if (pu1TxBuf == NULL)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "pu1TxBuf Memory Allocation"
                        "Failed \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "pu1TxBuf Memory Allocation" "Failed \r\n"));
            CAPWAP_RELEASE_CRU_BUF (pBuf);
            return OSIX_FAILURE;
        }
        /* Kloc Fix Ends */
        CAPWAP_COPY_FROM_BUF (pBuf, pu1TxBuf, 0, CAPWAP_MAX_PKT_LEN);
        u1IsNotLinear = OSIX_TRUE;
    }
    if (CapwapAssembleEchoRequest (&EchoReq, pu1TxBuf) == OSIX_FAILURE)
    {
        if (u1IsNotLinear == OSIX_TRUE)
        {
            MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pu1TxBuf);
        }
        CAPWAP_RELEASE_CRU_BUF (pBuf);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "capwapTransmitEchoRequest:Failed to Assemble"
                    "the packet \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "capwapTransmitEchoRequest:Failed to Assemble"
                      "the packet \r\n"));
        return OSIX_FAILURE;
    }

    if (u1IsNotLinear == OSIX_TRUE)
    {
        /* If the CRU buffer memory is not linear */
        CAPWAP_COPY_TO_BUF (pBuf, pu1TxBuf, 0, CAPWAP_MAX_PKT_LEN);
        MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pu1TxBuf);
    }
    if (CapwapTxMessage (pBuf,
                         pSess,
                         (UINT4) (EchoReq.u2CapwapMsgElemenLen +
                                  CAPWAP_SOCK_HDR_LEN),
                         WSS_CAPWAP_802_11_CTRL_PKT) == OSIX_FAILURE)
    {
        CAPWAP_RELEASE_CRU_BUF (pBuf);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "capwapTransmitEchoRequest:Failed to Transmit"
                    "the packet \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "capwapTransmitEchoRequest:Failed to Transmit"
                      "the packet \r\n"));
        return OSIX_FAILURE;
    }
    else
    {
        /* Update the last transmitted sequence number */
        pSess->lastTransmitedSeqNum++;
        CAPWAP_RELEASE_CRU_BUF (pBuf);
    }
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

#endif /* __CAPWAP_CONFIG_ */
/*-----------------------------------------------------------------------*/
/*                    End of the file  capwapconfig.c                      */
/*-----------------------------------------------------------------------*/
