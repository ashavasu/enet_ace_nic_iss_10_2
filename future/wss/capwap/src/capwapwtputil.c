/*********************************************************
 *
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 * $Id: capwapwtputil.c,v 1.4 2017/11/24 10:37:03 siva Exp $       
 * Description : This file contains the utility
 *               functions of the CAPWAP  module
 *********************************************************/
#ifndef _CAPWAPWTPUTIL_C_
#define _CAPWAPWTPUTIL_C_
#define _CAPWAP_GLOBAL_VAR

#include "capwapinc.h"
#include "aphdlrinc.h"
#include "radioif.h"
#include "radioifextn.h"
#include "capwapcli.h"
#include "wsswlanwtpproto.h"
#include "wssifstawtpprot.h"
#include "issnvram.h"
#include "wtpnvram.h"

#ifdef RFMGMT_WANTED
#include "rfminc.h"
#endif

#ifdef SNTP_WANTED
#include "sntp.h"
#endif
#ifdef KERNEL_CAPWAP_WANTED
#ifdef WTP_WANTED
#include "fcusglob.h"
#include "fcusprot.h"
#endif
#endif

#define CAPWAP_UDP_HDR_LEN 8
UINT1               gu1EchoCount = 0;
extern tCapwapDhcpAcDiscList gCapwapAcDiscInfo;
extern INT1         nmhGetIfMtu (INT4, INT4 *);

#ifdef BAND_SELECT_WANTED
extern UINT1        gu1GlobalBandSelectStatus;
#endif

/*****************************************************************************
 * Function     : CapwapGetLocationData                                      *
 * Description  : Get location data from DB                                  *
 * Input        : pWtpLocation, pMsgLen                                      *
 * Output       : None                                                       *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *****************************************************************************/
INT4
CapwapGetLocationData (tLocationData * pWtpLocation, UINT4 *pMsgLen)
{
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT2               u2IntId = 0;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpLocation = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2IntId;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
        == OSIX_SUCCESS)
    {
        pWtpLocation->u2MsgEleType = LOCATION_DATA;
        pWtpLocation->u2MsgEleLen
            = STRLEN (pWssIfCapwapDB->CapwapGetDB.au1WtpLocation);
        MEMCPY (pWtpLocation->value, pWssIfCapwapDB->CapwapGetDB.au1WtpLocation,
                pWtpLocation->u2MsgEleLen);
        *pMsgLen += (pWtpLocation->u2MsgEleLen) + CAPWAP_MSG_ELEM_TYPE_LEN;
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_SUCCESS;
    }
    else
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to get Location Data\r\n");
        return OSIX_FAILURE;
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : CapwapGetWtpBoardData                                      *
 * Description  : Get wtp board data from DB                                 *
 * Input        : pWtpBoardData, pMsgLen                                     *
 * Output       : None                                                       *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *****************************************************************************/
INT4
CapwapGetWtpBoardData (tWtpBoardData * pWtpBoardData, UINT4 *pMsgLen)
{

    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT2               u2Len;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpModelNumber = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpMacAddress = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = 0;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpSerialNumber = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpBoardId = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpBoardRivision = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bVendorSMI = OSIX_TRUE;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB) ==
        OSIX_SUCCESS)
    {
        pWtpBoardData->u4VendorSMI = pWssIfCapwapDB->CapwapGetDB.u4VendorSMI;
        u2Len = STRLEN (pWssIfCapwapDB->CapwapGetDB.au1WtpModelNumber);
        if (u2Len > 0)
        {
            pWtpBoardData->wtpBoardInfo[WTP_MODEL_NUMBER].u2BoardInfoType
                = WTP_MODEL_NUMBER;
            pWtpBoardData->wtpBoardInfo[WTP_MODEL_NUMBER].u2BoardInfoLength =
                u2Len;
            CAPWAP_MEMSET (pWtpBoardData->wtpBoardInfo[WTP_MODEL_NUMBER].
                           boardInfoData, 0, CAPWAP_MAX_DATA_LENGTH);
            MEMCPY (pWtpBoardData->wtpBoardInfo[WTP_MODEL_NUMBER].boardInfoData,
                    pWssIfCapwapDB->CapwapGetDB.au1WtpModelNumber, u2Len);
            pWtpBoardData->u2MsgEleLen =
                (pWtpBoardData->u2MsgEleLen + u2Len + CAPWAP_MSG_ELEM_TYPE_LEN);
            pWtpBoardData->u1DataInfoCount++;
        }
        u2Len = STRLEN (pWssIfCapwapDB->CapwapGetDB.au1WtpSerialNumber);
        if (u2Len > 0)
        {
            pWtpBoardData->wtpBoardInfo[WTP_SERIAL_NUMBER].u2BoardInfoType
                = WTP_SERIAL_NUMBER;
            pWtpBoardData->wtpBoardInfo[WTP_SERIAL_NUMBER].u2BoardInfoLength =
                u2Len;
            CAPWAP_MEMSET (pWtpBoardData->wtpBoardInfo[WTP_SERIAL_NUMBER].
                           boardInfoData, 0, CAPWAP_MAX_DATA_LENGTH);
            MEMCPY (pWtpBoardData->wtpBoardInfo[WTP_SERIAL_NUMBER].
                    boardInfoData,
                    pWssIfCapwapDB->CapwapGetDB.au1WtpSerialNumber, u2Len);
            pWtpBoardData->u2MsgEleLen =
                (pWtpBoardData->u2MsgEleLen + u2Len + CAPWAP_MSG_ELEM_TYPE_LEN);
            pWtpBoardData->u1DataInfoCount++;
        }
        u2Len = STRLEN (pWssIfCapwapDB->CapwapGetDB.au1WtpBoardId);
        if (u2Len > 0)
        {
            pWtpBoardData->wtpBoardInfo[WTP_BOARD_ID].u2BoardInfoType =
                WTP_BOARD_ID;
            pWtpBoardData->wtpBoardInfo[WTP_BOARD_ID].u2BoardInfoLength = u2Len;
            CAPWAP_MEMSET (pWtpBoardData->wtpBoardInfo[WTP_BOARD_ID].
                           boardInfoData, 0, CAPWAP_MAX_DATA_LENGTH);
            MEMCPY (pWtpBoardData->wtpBoardInfo[WTP_BOARD_ID].boardInfoData,
                    pWssIfCapwapDB->CapwapGetDB.au1WtpBoardId, u2Len);
            pWtpBoardData->u2MsgEleLen =
                (pWtpBoardData->u2MsgEleLen + u2Len + CAPWAP_MSG_ELEM_TYPE_LEN);
            pWtpBoardData->u1DataInfoCount++;
        }
        u2Len = STRLEN (pWssIfCapwapDB->CapwapGetDB.au1WtpBoardRivision);
        if (u2Len > 0)
        {
            pWtpBoardData->wtpBoardInfo[WTP_BOARD_RIVISION].u2BoardInfoType
                = WTP_BOARD_RIVISION;
            pWtpBoardData->wtpBoardInfo[WTP_BOARD_RIVISION].u2BoardInfoLength
                = u2Len;
            CAPWAP_MEMSET (pWtpBoardData->wtpBoardInfo[WTP_BOARD_RIVISION].
                           boardInfoData, 0, CAPWAP_MAX_DATA_LENGTH);
            MEMCPY (pWtpBoardData->wtpBoardInfo[WTP_BOARD_RIVISION].
                    boardInfoData,
                    pWssIfCapwapDB->CapwapGetDB.au1WtpBoardRivision, u2Len);
            pWtpBoardData->u2MsgEleLen =
                (pWtpBoardData->u2MsgEleLen + u2Len + CAPWAP_MSG_ELEM_TYPE_LEN);
            pWtpBoardData->u1DataInfoCount++;
        }
        u2Len = MAC_ADDR_LEN;
        if (u2Len > 0)
        {
            pWtpBoardData->wtpBoardInfo[WTP_BASE_MAC_ADDRESS].u2BoardInfoType
                = WTP_BASE_MAC_ADDRESS;
            pWtpBoardData->wtpBoardInfo[WTP_BASE_MAC_ADDRESS].u2BoardInfoLength
                = u2Len;
            CAPWAP_MEMSET (pWtpBoardData->wtpBoardInfo[WTP_BASE_MAC_ADDRESS].
                           boardInfoData, 0, CAPWAP_MAX_DATA_LENGTH);
            MEMCPY (pWtpBoardData->wtpBoardInfo[WTP_BASE_MAC_ADDRESS].
                    boardInfoData, pWssIfCapwapDB->CapwapGetDB.WtpMacAddress,
                    u2Len);
            pWtpBoardData->u2MsgEleLen =
                (pWtpBoardData->u2MsgEleLen + u2Len + CAPWAP_MSG_ELEM_TYPE_LEN);
            pWtpBoardData->u1DataInfoCount++;
        }
        pWtpBoardData->u2MsgEleType = WTP_BOARD_DATA;
        pWtpBoardData->u2MsgEleLen =
            (pWtpBoardData->u2MsgEleLen + CAPWAP_MSG_ELEM_TYPE_LEN);
        *pMsgLen += pWtpBoardData->u2MsgEleLen + CAPWAP_MSG_ELEM_TYPE_LEN;
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_SUCCESS;
    }
    else
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to get Location Data from AP HDLR DB \r\n");
        return OSIX_FAILURE;
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : CapwapGetWtpDescriptor                                     *
 * Description  : Get wtp descriptor from DB                                 *
 * Input        : pWtpDescriptor, pMsgLen                                    *
 * Output       : None                                                       *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *****************************************************************************/
INT4
CapwapGetWtpDescriptor (tWtpDescriptor * pWtpDescriptor, UINT4 *pMsgLen)
{
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT2               u2Len;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = 0;
    pWssIfCapwapDB->CapwapGetDB.u1WtpNoofRadio = SYS_DEF_MAX_RADIO_INTERFACES;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpDescriptor = OSIX_TRUE;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB) ==
        OSIX_SUCCESS)
    {
        pWtpDescriptor->u2MsgEleType = WTP_DESCRIPTOR;
        pWtpDescriptor->u1MaxRadios =
            pWssIfCapwapDB->CapwapGetDB.u1WtpNoofRadio;
        pWtpDescriptor->u1RadiosInUse =
            pWssIfCapwapDB->CapwapGetDB.u1WtpNoofRadio;
        pWtpDescriptor->u1NumEncrypt = 1;
        pWtpDescriptor->wtpEncrptCap.u1WBID = 1;
        pWtpDescriptor->wtpEncrptCap.encryptCapability = AUTH_CIPHER_CCMP;
        pWtpDescriptor->u4VendorSMI = pWssIfCapwapDB->CapwapGetDB.u4VendorSMI;
        u2Len = STRLEN (pWssIfCapwapDB->CapwapGetDB.au1WtpHWversion);
        if (u2Len > 0)
        {
            pWtpDescriptor->VendDesc[WTP_HW_VERSION].vendorInfoType =
                WTP_HW_VERSION;
            pWtpDescriptor->VendDesc[WTP_HW_VERSION].u2VendorInfoLength = u2Len;
            CAPWAP_MEMSET (pWtpDescriptor->VendDesc[WTP_HW_VERSION].
                           vendorInfoData, 0, CAPWAP_MAX_DATA_LENGTH);
            MEMCPY (pWtpDescriptor->VendDesc[WTP_HW_VERSION].vendorInfoData,
                    pWssIfCapwapDB->CapwapGetDB.au1WtpHWversion, u2Len);
            pWtpDescriptor->u2MsgEleLen = pWtpDescriptor->u2MsgEleLen +
                u2Len + CAPWAP_VENDOR_ID_LEN + CAPWAP_MSG_ELEM_TYPE_LEN;
            pWtpDescriptor->u4VendorInfoCount++;
        }
        u2Len = STRLEN (pWssIfCapwapDB->CapwapGetDB.au1WtpSWversion);
        if (u2Len > 0)
        {
            /* Fill the Active Software version */
            pWtpDescriptor->VendDesc[WTP_SW_VERSION].vendorInfoType =
                WTP_SW_VERSION;
            pWtpDescriptor->VendDesc[WTP_SW_VERSION].u2VendorInfoLength = u2Len;
            CAPWAP_MEMSET (pWtpDescriptor->VendDesc[WTP_SW_VERSION].
                           vendorInfoData, 0, CAPWAP_MAX_DATA_LENGTH);
            MEMCPY (pWtpDescriptor->VendDesc[WTP_SW_VERSION].vendorInfoData,
                    pWssIfCapwapDB->CapwapGetDB.au1WtpSWversion, u2Len);
            pWtpDescriptor->u2MsgEleLen = pWtpDescriptor->u2MsgEleLen +
                u2Len + CAPWAP_VENDOR_ID_LEN + CAPWAP_MSG_ELEM_TYPE_LEN;
            pWtpDescriptor->u4VendorInfoCount++;
        }
        u2Len = STRLEN (pWssIfCapwapDB->CapwapGetDB.au1WtpBootversion);
        if (u2Len > 0)
        {
            /* update boot version */
            pWtpDescriptor->VendDesc[WTP_BOOT_VERSION].vendorInfoType
                = WTP_BOOT_VERSION;
            pWtpDescriptor->VendDesc[WTP_BOOT_VERSION].u2VendorInfoLength =
                u2Len;
            CAPWAP_MEMSET (pWtpDescriptor->VendDesc[WTP_BOOT_VERSION].
                           vendorInfoData, 0, CAPWAP_MAX_DATA_LENGTH);
            MEMCPY (pWtpDescriptor->VendDesc[WTP_BOOT_VERSION].vendorInfoData,
                    pWssIfCapwapDB->CapwapGetDB.au1WtpBootversion, u2Len);
            pWtpDescriptor->u2MsgEleLen = pWtpDescriptor->u2MsgEleLen +
                u2Len + CAPWAP_VENDOR_ID_LEN + CAPWAP_MSG_ELEM_TYPE_LEN;
            pWtpDescriptor->u4VendorInfoCount++;
        }
        pWtpDescriptor->u2MsgEleLen = pWtpDescriptor->u2MsgEleLen + 6;
        pWtpDescriptor->u2MsgEleType = WTP_DESCRIPTOR;
        *pMsgLen += pWtpDescriptor->u2MsgEleLen + CAPWAP_MSG_ELEM_TYPE_LEN;
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_SUCCESS;
    }
    else
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to get WTP Descriptor from CAPWAP DB \r\n");
        return OSIX_FAILURE;
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : CapwapGetWtpName                                           *
 * Description  : Get wtp name from DB                                       *
 * Input        : pWtpName, pMsgLen                                          *
 * Output       : None                                                       *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *****************************************************************************/
INT4
CapwapGetWtpName (tWtpName * pWtpName, UINT4 *pMsgLen)
{
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT2               u2IntId = 0;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpName = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2IntId;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
        == OSIX_SUCCESS)
    {
        pWtpName->u2MsgEleType = WTP_NAME;
        CAPWAP_MEMSET (pWtpName->wtpName, 0, CAPWAP_WTP_NAME_SIZE);
        pWtpName->u2MsgEleLen = STRLEN (pWssIfCapwapDB->CapwapGetDB.au1WtpName);
        MEMCPY (pWtpName->wtpName, pWssIfCapwapDB->CapwapGetDB.au1WtpName,
                pWtpName->u2MsgEleLen);
        *pMsgLen += pWtpName->u2MsgEleLen + CAPWAP_MSG_ELEM_TYPE_LEN;
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_SUCCESS;
    }
    else
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to get Location Data\r\n");
        return OSIX_FAILURE;
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : CapwapGetACName                                            *
 * Description  : Get AC name from DB                                        *
 * Input        : pWlcName, pMsgLen                                          *
 * Output       : None                                                       *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *****************************************************************************/
INT4
CapwapGetACName (tACName * pWlcName, UINT4 *pMsgLen)
{
    tWssIfCapDB        *pWssIfCapwapDB = NULL;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    pWssIfCapwapDB->CapwapIsGetAllDB.bAcName = OSIX_TRUE;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
        == OSIX_SUCCESS)
    {
        pWlcName->u2MsgEleType = AC_NAME;
        pWlcName->u2MsgEleLen = STRLEN (pWssIfCapwapDB->au1WlcName);
        MEMCPY (pWlcName->wlcName, pWssIfCapwapDB->au1WlcName,
                pWlcName->u2MsgEleLen);
        *pMsgLen += pWlcName->u2MsgEleLen + CAPWAP_MSG_ELEM_TYPE_LEN;
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_SUCCESS;
    }
    else
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Get AC Name\r\n");
        return OSIX_FAILURE;
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : CapwapGetCapwapTimers                                      *
 * Description  : Get capwap timers from DB                                  *
 * Input        : pWlcName, pMsgLen                                          *
 * Output       : None                                                       *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *****************************************************************************/
INT4
CapwapGetCapwapTimers (tCapwapTimer * pCapwapTimer, UINT4 *pMsgLen,
                       UINT2 u2IntProfileId)
{

    tWssIfCapDB        *pWssIfCapwapDB = NULL;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpMaxDiscoveryInterval = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpEchoInterval = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2IntProfileId;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
        == OSIX_SUCCESS)
    {
        pCapwapTimer->u2MsgEleType = CAPWAP_TIMERS;
        pCapwapTimer->u1Discovery =
            pWssIfCapwapDB->CapwapGetDB.u2WtpEchoInterval;
        pCapwapTimer->u1EchoRequest =
            pWssIfCapwapDB->CapwapGetDB.u2WtpEchoInterval;
        pCapwapTimer->u2MsgEleLen = CAPWAP_TIMERS_LEN;
        *pMsgLen += pCapwapTimer->u2MsgEleLen + CAPWAP_MSG_ELEM_TYPE_LEN;
        pCapwapTimer->isOptional = 1;
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_SUCCESS;
    }
    else
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Get Capwap Timers \r\n");
        return OSIX_FAILURE;
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : CapwapGetJoinSessionId                                     *
 * Description  : Get joins session ID from DB                               *
 * Input        : pJoinSessionID, pMsgLen                                    *
 * Output       : None                                                       *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *****************************************************************************/
INT4
CapwapGetJoinSessionId (UINT4 *pJoinSessionID, UINT4 *pMsgLen)
{

    INT4                i4Index = 0, i4RandNo = 0;
    UINT4               u4Seed;

    OSIX_SEED (u4Seed);
    OSIX_SRAND (u4Seed);
    for (i4Index = 0; i4Index < 4; i4Index++)
    {
        i4RandNo = (OSIX_RAND (0ul, OSIX_RAND_MAX));
        pJoinSessionID[i4Index] = i4RandNo;
    }
    *pMsgLen += (SESSION_ID_LEN + CAPWAP_MSG_ELEM_TYPE_LEN);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : CapwapGetWtpFrameTunnelMode                                *
 * Description  : Get WTP Frame tunnel mode                                  *
 * Input        : pWtpFrameTunnel, pMsgLen                                   *
 * Output       : None                                                       *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *****************************************************************************/
INT4
CapwapGetWtpFrameTunnelMode (tWtpFrameTunnel * pWtpFrameTunnel, UINT4 *pMsgLen)
{
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT2               u2IntId = 0;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    pWssIfCapwapDB->CapwapIsGetAllDB.bTunnelMode = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2IntId;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
        == OSIX_SUCCESS)
    {
        pWtpFrameTunnel->u2MsgEleType = WTP_FRAME_TUNNEL_MODE;
        pWtpFrameTunnel->resvd4NELU =
            pWssIfCapwapDB->CapwapGetDB.u1WtpTunnelMode;
        pWtpFrameTunnel->u2MsgEleLen = WTP_FRAME_TUNNEL_MODE_LEN;
        *pMsgLen += pWtpFrameTunnel->u2MsgEleLen + CAPWAP_MSG_ELEM_TYPE_LEN;
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_SUCCESS;
    }
    else
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to get Location Data from AP HDLR DB \r\n");
        return OSIX_FAILURE;
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : CapwapGetWtpMacType                                        *
 * Description  : Get WTP MAC type from DB                                   *
 * Input        : pWtpMacType, pMsgLen                                       *
 * Output       : None                                                       *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *****************************************************************************/
INT4
CapwapGetWtpMacType (tWtpMacType * pWtpMacType, UINT4 *pMsgLen)
{

    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT2               u2IntId = 0;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    pWssIfCapwapDB->CapwapIsGetAllDB.bMacType = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2IntId;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
        == OSIX_SUCCESS)
    {
        pWtpMacType->u2MsgEleType = WTP_MAC_TYPE;
        pWtpMacType->macValue = pWssIfCapwapDB->CapwapGetDB.u1WtpMacType;
        pWtpMacType->u2MsgEleLen = WTP_MAC_TYPE_LEN;
        *pMsgLen += pWtpMacType->u2MsgEleLen + CAPWAP_MSG_ELEM_TYPE_LEN;
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_SUCCESS;
    }
    else
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to get Location Data from AP HDLR DB \r\n");
        return OSIX_FAILURE;
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;

}

/*****************************************************************************
 * Function     : CapwapGetWtpDiscPadding                                    *
 * Description  : Get WTP Discriptor padding                                 *
 * Input        : pMtuDiscPad, pMsgLen                                       *
 * Output       : None                                                       *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *****************************************************************************/
INT4
CapwapGetWtpDiscPadding (tMtuDiscPad * pMtuDiscPad, UINT4 *pMsgLen)
{

    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT2               u2IntId = 0, u2Len;
    UINT4               u4Mtu = 0;
    INT1                i1RetVal = SNMP_SUCCESS;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    pWssIfCapwapDB->CapwapIsGetAllDB.bDiscPadding = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2IntId;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
        == OSIX_SUCCESS)
    {
        pMtuDiscPad->u2MsgEleType = MTU_DISC_PADDING;
        u2Len = STRLEN (pWssIfCapwapDB->CapwapGetDB.au1DiscPadding);
        if (u2Len > CAPWAP_DISCOVERY_PADDING_LEN)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "au1DiscPadding length is more than 1024 \r\n");
            return OSIX_FAILURE;
        }
        MEMCPY (pMtuDiscPad->paddingVal,
                pWssIfCapwapDB->CapwapGetDB.au1DiscPadding, u2Len);

        i1RetVal = CfaGetIfMtu (1, &u4Mtu);
        if (i1RetVal == CFA_FAILURE)
        {
            u4Mtu = CAPWAP_MTU_SIZE;
        }
        u2Len = u4Mtu - (*pMsgLen + CAPWAP_CMN_MSG_ELM_HEAD_LEN + CAPWAP_SOCK_HDR_LEN) - CAPWAP_MSG_ELEM_TYPE_LEN - CAPWAP_UDP_HDR_LEN - IP_HDR_LEN - VLAN_HDR_LEN;    /* Excluding VLAN header to not 
                                                                                                                                                                       drop the packet due to MTU oversize
                                                                                                                                                                       for tagged packets */

        pMtuDiscPad->u2MsgEleLen = u2Len;
        pMtuDiscPad->isOptional = OSIX_TRUE;
        *pMsgLen += pMtuDiscPad->u2MsgEleLen + CAPWAP_MSG_ELEM_TYPE_LEN;
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_SUCCESS;
    }
    else
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to get Location Data from AP HDLR DB \r\n");
        return OSIX_FAILURE;
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : CapwapGetDiscoveryType                                     *
 * Description  : Get Discovery Type from DB                                 *
 * Input        : pMtuDiscPad, pMsgLen                                       *
 * Output       : None                                                       *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *****************************************************************************/
INT4
CapwapGetDiscoveryType (tWtpDiscType * pWtpDiscType, UINT4 *pMsgLen)
{
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT2               u2IntId = 0;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpDiscType = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2IntId;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
        == OSIX_SUCCESS)
    {
        pWtpDiscType->u2MsgEleType = DISCOVERY_TYPE;
        pWtpDiscType->u1DiscType = gCapwapAcDiscInfo.uCurAcDiscMode;
        pWtpDiscType->u2MsgEleLen = DISCOVERY_TYPE_LEN;
        *pMsgLen += pWtpDiscType->u2MsgEleLen + CAPWAP_MSG_ELEM_TYPE_LEN;
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_SUCCESS;
    }
    else
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to get Location Data from AP HDLR DB \r\n");
        return OSIX_FAILURE;
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : CapwapGetLocalIpAddr                                       *
 * Description  : Get Local IP address from DB                               *
 * Input        : pIncomingAddress, pMsgLen                                  *
 * Output       : None                                                       *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *****************************************************************************/
INT4
CapwapGetLocalIpAddr (tLocalIpAddr * pIncomingAddress, UINT4 *pMsgLen)
{
    tWssIfApHdlrDB      WssIfApHdlrDB;
    UINT1               au1IfName[CAPWAP_WTP_IF_INTERFACE_LEN];
    MEMSET (&WssIfApHdlrDB, 0, sizeof (tWssIfApHdlrDB));

    STRNCPY ((CHR1 *) au1IfName, CAPWAP_WTP_IF_INTERFACE_NAME,
             sizeof (CAPWAP_WTP_IF_INTERFACE_NAME));

    if (WssIfProcessApHdlrDBMsg (WSS_APHDLR_GET_LOCAL_IPV4, &WssIfApHdlrDB)
        == OSIX_SUCCESS)
    {
        pIncomingAddress->u2MsgEleType = CAPWAP_LOCAL_IPV4_ADDR;
        WssIfGetIfIpInfo ((UINT1 *) au1IfName,
                          &pIncomingAddress->ipAddr.u4_addr[0]);

        pIncomingAddress->u2MsgEleLen = CAPWAP_LOCAL_IPV4_ADDR_LEN;
        *pMsgLen += pIncomingAddress->u2MsgEleLen + CAPWAP_MSG_ELEM_TYPE_LEN;
        return OSIX_SUCCESS;
    }
    else
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to get Location Data from AP HDLR DB \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : CapwapGetTransProtocol                                     *
 * Description  : Get Transport protocol from DB                             *
 * Input        : pTransProto, pMsgLen, u2IntId                              *
 * Output       : None                                                       *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *****************************************************************************/
INT4
CapwapGetTransProtocol (tCapwapTransprotocol * pTransProto, UINT4 *pMsgLen,
                        UINT2 u2IntId)
{

    tWssIfCapDB        *pWssIfCapwapDB = NULL;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    pWssIfCapwapDB->CapwapIsGetAllDB.bTransportProtocol = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2IntId;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
        == OSIX_SUCCESS)
    {
        pTransProto->u2MsgEleType = CAPWAP_TRANSPORT_PROTOCOL;
        pTransProto->transportType = pWssIfCapwapDB->CapwapGetDB.u1ProtocolType;
        pTransProto->u2MsgEleLen = CAPWAP_TRANSPORT_PROTOCOL_LEN;
        *pMsgLen += pTransProto->u2MsgEleLen + CAPWAP_MSG_ELEM_TYPE_LEN;
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_SUCCESS;
    }
    else
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to get Transport Protocol \r\n");
        return OSIX_FAILURE;
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : CapwapGetMaxMessageLen                                     *
 * Description  : Get Max message length from DB                             *
 * Input        : pMsgLength, pMsgLen                                        *
 * Output       : None                                                       *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *****************************************************************************/
INT4
CapwapGetMaxMessageLen (tMaxMsgLen * pMsgLength, UINT4 *pMsgLen)
{
    tWssIfCapDB        *pWssIfCapwapDB = NULL;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    pWssIfCapwapDB->CapwapIsGetAllDB.bMaxMsgLen = OSIX_TRUE;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB) ==
        OSIX_SUCCESS)
    {
        pMsgLength->u2MsgEleType = MAX_MESG_LEN;
        pMsgLength->u2MaxMsglen = pWssIfCapwapDB->CapwapGetDB.u2MaxMsgLen;
        pMsgLength->u2MsgEleLen = MAX_MESGLEN_LEN;
        *pMsgLen += pMsgLength->u2MsgEleLen + CAPWAP_MSG_ELEM_TYPE_LEN;
        pMsgLength->isOptional = OSIX_TRUE;
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_SUCCESS;
    }
    else
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to Get Max Message Length \r\n");
        return OSIX_FAILURE;
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;

}

/*****************************************************************************
 * Function     : CapwapGetRebootStats                                       *
 * Description  : Get reboot statistics length from DB                       *
 * Input        : pRebootStats, pMsgLen                                      *
 * Output       : None                                                       *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *****************************************************************************/
INT4
CapwapGetRebootStats (tWtpRebootStatsElement * pRebootStats, UINT4 *pMsgLen)
{
    tWssIfApHdlrDB      WssIfApHdlrDB;

    MEMSET (&WssIfApHdlrDB, 0, sizeof (tWssIfApHdlrDB));

    if (WssIfProcessApHdlrDBMsg (WSS_APHDLR_GET_WTPREBOOT_STATS, &WssIfApHdlrDB)
        == OSIX_SUCCESS)
    {
        pRebootStats->u2MsgEleType = WTP_REBOOT_STATISTICS;
        pRebootStats->u2RebootCount = WssIfApHdlrDB.u2RebootCount;
        pRebootStats->u2AcInitiatedCount = WssIfApHdlrDB.u2AcInitiatedCount;
        pRebootStats->u2LinkFailCount = WssIfApHdlrDB.u2LinkFailCount;
        pRebootStats->u2SwFailCount = WssIfApHdlrDB.u2SwFailCount;
        pRebootStats->u2HwFailCount = WssIfApHdlrDB.u2HwFailCount;
        pRebootStats->u2OtherFailCount = WssIfApHdlrDB.u2OtherFailCount;
        pRebootStats->u2UnknownFailCount = WssIfApHdlrDB.u2UnknownFailCount;
        pRebootStats->u1LastFailureType = WssIfApHdlrDB.u1LastFailureType;
        pRebootStats->u2MsgEleLen = WTP_REBOOT_STATISTICS_MSG_LEN;
        *pMsgLen += pRebootStats->u2MsgEleLen + CAPWAP_MSG_ELEM_TYPE_LEN;
        pRebootStats->isOptional = OSIX_TRUE;
        return OSIX_SUCCESS;
    }
    else
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Get WTP Reboot Stats \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;

}

/*****************************************************************************
 * Function     : CapwapGetStatsTimer                                        *
 * Description  : Get statistics timer from DB                               *
 * Input        : pStatsTimer, pMsgLen                                       *
 * Output       : None                                                       *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *****************************************************************************/
INT4
CapwapGetStatsTimer (tStatisticsTimer * pStatsTimer, UINT4 *pMsgLen)
{

    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT2               u2IntId = 0;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpStatisticsTimer = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2IntId;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
        == OSIX_SUCCESS)
    {
        pStatsTimer->u2MsgEleType = STATS_TIMER;
        pStatsTimer->u2StatsTimer
            = pWssIfCapwapDB->CapwapGetDB.u2WtpStatisticsTimer;
        pStatsTimer->u2MsgEleLen = STATS_TIMER_LEN;
        pStatsTimer->isOptional = OSIX_TRUE;
        *pMsgLen += pStatsTimer->u2MsgEleLen + CAPWAP_MSG_ELEM_TYPE_LEN;
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_SUCCESS;
    }
    else
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Get Stats Timer  \r\n");
        return OSIX_FAILURE;
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : CapwapGetAcNameWithPriority                                *
 * Description  : Get AC name with priority from DB                          *
 * Input        : pAcNameWithPri, pMsgLen                                    *
 * Output       : None                                                       *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *****************************************************************************/
INT4
CapwapGetAcNameWithPriority (tACNameWithPrio * pAcNameWithPri, UINT4 *pMsgLen)
{

    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT2               u2IntId = 0, u2Len;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    pWssIfCapwapDB->CapwapIsGetAllDB.bAcNamewithPriority = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2IntId;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
        == OSIX_SUCCESS)
    {
        pAcNameWithPri->u2MsgEleType = AC_NAME_WITH_PRIO;
        u2Len = STRLEN (pWssIfCapwapDB->CapwapGetDB.AcNameWithPri[0].wlcName);
        MEMCPY (pAcNameWithPri->wlcName,
                pWssIfCapwapDB->CapwapGetDB.AcNameWithPri[0].wlcName,
                CAPWAP_WLC_NAME_SIZE);
        pAcNameWithPri->u1Priority =
            pWssIfCapwapDB->CapwapGetDB.AcNameWithPri[0].u1Priority;
        pAcNameWithPri->u2MsgEleLen = u2Len;
        *pMsgLen += pAcNameWithPri->u2MsgEleLen + CAPWAP_MSG_ELEM_TYPE_LEN;
        pAcNameWithPri->isOptional = OSIX_TRUE;
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_SUCCESS;
    }
    else
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to Get Ac name with Priority  \r\n");
        return OSIX_FAILURE;
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : CapwapGetWtpStaticIpInfo                                   *
 * Description  : Get Wtp static IP from DB                                  *
 * Input        : pWtpStaticIp, pMsgLen                                      *
 * Output       : None                                                       *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *****************************************************************************/
INT4
CapwapGetWtpStaticIpInfo (tWtpStaticIpAddr * pWtpStaticIp, UINT4 *pMsgLen)
{

    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT2               u2IntId = 0;

    UNUSED_PARAM (pMsgLen);

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpStaticIpEnable = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpStaticIpType = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpStaticIpAddress = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpNetmask = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpGateway = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2IntId;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
        == OSIX_SUCCESS)
    {
        pWtpStaticIp->u2MsgEleType = AC_NAME_WITH_PRIO;
        pWtpStaticIp->u2MsgEleLen = AC_NAME_WITH_PRIO_MIN_LEN;
        pWtpStaticIp->isOptional = OSIX_FALSE;
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_SUCCESS;
    }
    else
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to Get Static IP Info State \r\n");
        return OSIX_FAILURE;
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : CapwapGetVendorPayload                                     *
 * Description  : Get vendor payloadfrom DB                                  *
 * Input        : pVendorPayload, pMsgLen                                    *
 * Output       : None                                                       *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *****************************************************************************/
INT4
CapwapGetVendorPayload (tVendorSpecPayload * pVendorPayload, UINT4 *pMsgLen)
{
#ifdef PMF_DEBUG_WANTED
#ifdef WTP_WANTED
    if (pVendorPayload->elementId == VENDOR_PMF_TYPE)
    {
        /*PMF_WANTED */

        tRadioIfGetDB       RadioIfGetDB;
        UINT1               au1RadioName[RADIO_INTF_NAME_LEN] = { 0 };

        CAPWAP_FN_ENTRY ();

        MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

        RadioIfGetDB.RadioIfGetAllDB.u1RadioId =
            pVendorPayload->unVendorSpec.VendPMFPktNo.u1RadioId;
        RadioIfGetDB.RadioIfGetAllDB.u1WlanId =
            pVendorPayload->unVendorSpec.VendPMFPktNo.u1WlanId;
        RadioIfGetDB.RadioIfIsGetAllDB.bRadioIfIndex = OSIX_TRUE;

        if (WssIfProcessRadioIfDBMsg (WSS_GET_PHY_RADIO_IF_DB, &RadioIfGetDB)
            != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to get Wlan Id and Radio IfIndex \r\n");
            return OSIX_FAILURE;
        }

        pVendorPayload->u2MsgEleLen = 22;
        pVendorPayload->unVendorSpec.VendPMFPktNo.u2MsgEleType = VENDOR_PMF_MSG;
        pVendorPayload->unVendorSpec.VendPMFPktNo.isOptional = OSIX_TRUE;
        pVendorPayload->unVendorSpec.VendPMFPktNo.u2MsgEleLen = 14;
        pVendorPayload->unVendorSpec.VendPMFPktNo.u4VendorId = VENDOR_ID;
        pVendorPayload->unVendorSpec.VendPMFPktNo.RsnaIGTKSeqInfo[0].
            u1KeyIndex = 4;
        pVendorPayload->unVendorSpec.VendPMFPktNo.RsnaIGTKSeqInfo[1].
            u1KeyIndex = 5;

        SPRINTF ((CHR1 *) au1RadioName, "%s%d", RADIO_INTF_NAME,
                 pVendorPayload->unVendorSpec.VendPMFPktNo.u1RadioId - 1);

#ifdef NPAPI_WANTED
        if (RadioIfGetSeqNum
            (&RadioIfGetDB,
             pVendorPayload->unVendorSpec.VendPMFPktNo.RsnaIGTKSeqInfo[0].
             u1KeyIndex,
             pVendorPayload->unVendorSpec.VendPMFPktNo.RsnaIGTKSeqInfo[0].
             au1IGTKPktNo, au1RadioName) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "RadioIfGetSeqNum:- Failure to get the Packet Number \n");
            return OSIX_FAILURE;
        }

        if (RadioIfGetSeqNum
            (&RadioIfGetDB,
             pVendorPayload->unVendorSpec.VendPMFPktNo.RsnaIGTKSeqInfo[1].
             u1KeyIndex,
             pVendorPayload->unVendorSpec.VendPMFPktNo.RsnaIGTKSeqInfo[1].
             au1IGTKPktNo, au1RadioName) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "RadioIfGetSeqNum:- Failure to get the Packet Number \n");
            return OSIX_FAILURE;
        }
#endif

        *pMsgLen = pVendorPayload->u2MsgEleLen + CAPWAP_MSG_ELEM_TYPE_LEN;
    }

#endif
#endif
    UNUSED_PARAM (pVendorPayload);
    UNUSED_PARAM (pMsgLen);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : CapwapGetDiscRespVendorPayload                             *
 * Description  : Receives the CAPWAP packet and get the Disc Resp vendor    *
 *                payload                                                    *
 * Input        : pVendorPayload, pMsgLen,u1Index                            *
 * Output       : None                                                       *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *****************************************************************************/
INT4
CapwapGetDiscRespVendorPayload (tVendorSpecPayload * pVendorPayload,
                                UINT4 *pMsgLen, UINT1 u1Index,
                                UINT2 u2InternalId)
{
    UNUSED_PARAM (pMsgLen);
    UNUSED_PARAM (pVendorPayload);
    UNUSED_PARAM (u2InternalId);
    UNUSED_PARAM (u1Index);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : CapwapGetJoinRespVendorPayload                             *
 * Description  : Receives the CAPWAP packet and get the config stat vendor  *
 *                payload                                                    *
 * Input        : pVendorPayload, pMsgLen,u1Index                            *
 * Output       : None                                                       *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *****************************************************************************/
INT4
CapwapGetJoinRespVendorPayload (tVendorSpecPayload * pVendorPayload,
                                UINT4 *pMsgLen, UINT1 u1Index,
                                UINT2 u2InternalId)
{
    tWssIfCapDB        *pWssIfCapwapDB = NULL;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    CAPWAP_FN_ENTRY ();
    switch (u1Index)
    {
        case VENDOR_LOCAL_ROUTING_TYPE:
            pWssIfCapwapDB->CapwapIsGetAllDB.bWtpLocalRouting = OSIX_TRUE;
            pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2InternalId;
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB) !=
                OSIX_SUCCESS)
            {
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Failed to get WtpLocalRouting Join"
                            "Response Message Elements \r\n");
                return OSIX_FAILURE;
            }

            pVendorPayload->isOptional = OSIX_TRUE;
            pVendorPayload->elementId = VENDOR_LOCAL_ROUTING_TYPE;
            pVendorPayload->u2MsgEleType =
                CAPWAP_VEND_SPECIFIC_PAYLOAD_MSG_TYPE;
            pVendorPayload->u2MsgEleLen = 9;
            pVendorPayload->unVendorSpec.VendLocalRouting.isOptional =
                OSIX_TRUE;
            pVendorPayload->unVendorSpec.VendLocalRouting.u2MsgEleType =
                LOCAL_ROUTING_VENDOR_MSG;
            pVendorPayload->unVendorSpec.VendLocalRouting.u2MsgEleLen = 5;
            pVendorPayload->unVendorSpec.VendLocalRouting.vendorId = VENDOR_ID;
            pVendorPayload->unVendorSpec.VendLocalRouting.u1WtpLocalRouting =
                pWssIfCapwapDB->CapwapGetDB.u1WtpLocalRouting;
            *pMsgLen += pVendorPayload->u2MsgEleLen + CAPWAP_MSG_ELEM_TYPE_LEN;

            break;
        default:
            break;
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : CapwapProcessCtrlPacket                                    *
 * Description  : Receives the CAPWAP packet and process the messag          *
 *                if Discovery Request, Discovery response                   *
 * Input        : pBuf - received CAPWAP packet                              *
 * Output       : None                                                       *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *****************************************************************************/
UINT4
CapwapProcessCtrlPacket (tCRU_BUF_CHAIN_HEADER * pBuf)
{

    UINT1               u1Val;
    UINT1              *pRcvBuf = NULL;
    UINT4               u4MsgType, u4DestPort, u4DestIp;
    tCapwapRxQMsg      *pCapwapRxQMsg = NULL;
    unApHdlrMsgStruct   ApHdlrMsg;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT1               au1Frame[CAPWAP_HDR_LEN + CAPWAP_CONTROL_HDR_LEN];
    UINT1               u1HdrLen = CAPWAP_HDR_LEN + CAPWAP_CONTROL_HDR_LEN;
    UINT2               u2PacketLen;
    tQueData            QueData;

    CAPWAP_FN_ENTRY ();

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&QueData, 0, sizeof (tQueData));

    pRcvBuf = CAPWAP_BUF_IF_LINEAR (pBuf, 0, u1HdrLen);
    if (pRcvBuf == NULL)
    {
        /* Header is not linear in CRU buffer */
        MEMSET (au1Frame, 0x00, u1HdrLen);
        CAPWAP_COPY_FROM_BUF (pBuf, au1Frame, 0, u1HdrLen);
        pRcvBuf = au1Frame;
    }

    u4DestPort = CRU_BUF_Get_U4Reserved1 (pBuf);

    u4DestIp = CRU_BUF_Get_U4Reserved2 (pBuf);

    /* Parse the received packet to check the packet type */
    CAPWAP_GET_1BYTE (u1Val, pRcvBuf);
    /* Move the buffer pointer to the original position */
    pRcvBuf -= 1;
    if ((u1Val & 0x0F) == CAPWAP_PREAMBLE_DTLS_TYPE)
    {
        /* Remove the CAPWAP DTLS header Move the buffer offset by 
         * 4 Bytes to Remove CAPWAP DTLS Header */
        CRU_BUF_Move_ValidOffset (pBuf, CAPWAP_DTLS_HDR_LEN);
        u2PacketLen = (UINT2) CAPWAP_GET_BUF_LEN (pBuf);
        QueData.pBuffer = pBuf;
        QueData.u4Id = 0;
        QueData.u4IpAddr = u4DestIp;
        QueData.u4PortNo = u4DestPort;
        QueData.u4BufSize = u2PacketLen;
        QueData.eQueDataType = DATA_DECRYPT;
        QueData.u4Channel = DTLS_CTRL_CHANNEL;

        if (CapwapDtlsDecryptNotify (&QueData) == OSIX_FAILURE)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to decrypt the packet \r\n");
            CAPWAP_RELEASE_CRU_BUF (pBuf);
            return OSIX_FAILURE;
        }
        CAPWAP_RELEASE_CRU_BUF (pBuf);
    }
    else if ((u1Val & 0x0F) == CAPWAP_PREAMBLE_PLAINTEXT_TYPE)
    {
        pCapwapRxQMsg = (tCapwapRxQMsg *) MemAllocMemBlk (CAPWAP_QUEMSG_POOLID);

        if (pCapwapRxQMsg == NULL)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "CapwapProcessCtrlPacket:Memory Allocation Failed \r\n");
            CAPWAP_RELEASE_CRU_BUF (pBuf);
            return OSIX_FAILURE;
        }
        MEMSET (pCapwapRxQMsg, 0, sizeof (tCapwapRxQMsg));

        /* If the discovery message received as fragment first it will be
         * send to CAPWAP module. The CAPWAP module will reassemble the 
         * packet and sends to discovery thread for furhter processing */
        /* If the CAPWAP control message (ex WLAN Configuration request,
         * configuration udpated requests etc)received as fragment it will
         * be send to CAPWAP module for reassembly. CAPWAP module reassemble
         * the packet and sends to AP HDLR or WLC HDLR for for further 
         * processing. To Limit the reassembly logic to one module its decided 
         * to reassemble only at CAPWAP module */
        if (CapwapFragmentReceived (pBuf) == OSIX_SUCCESS)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            pCapwapRxQMsg->u4PktType = CAPWAP_CTRL_FRAG_MSG;
            pCapwapRxQMsg->pRcvBuf = pBuf;
            pCapwapRxQMsg->capwapSessionId.u4_addr[0] = u4DestIp;
            CapwapEnqueCtrlPkts (pCapwapRxQMsg);
            return OSIX_SUCCESS;
        }
        CapwapGetMsgTypeFromBuf (pRcvBuf, &u4MsgType);
        if ((u4MsgType < MIN_CAPWAP_MSG_TYPE) || (u4MsgType > WLAN_CONF_RSP))
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "capwapProcessCtrlPacket: Received unknown MsgType \r\n");
            MemReleaseMemBlock (CAPWAP_QUEMSG_POOLID, (UINT1 *) pCapwapRxQMsg);
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            return OSIX_FAILURE;
        }
        else if ((u4MsgType == CAPWAP_DISC_REQ)
                 || (u4MsgType == CAPWAP_DISC_RSP))
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            pCapwapRxQMsg->u4PktType = CAPWAP_DISC_MSG;
            pCapwapRxQMsg->pRcvBuf = pBuf;
            pCapwapRxQMsg->capwapSessionId.u4_addr[0] = u4DestIp;
            CapwapEnqueDiscPkts (pCapwapRxQMsg);
            return OSIX_SUCCESS;
        }
        else if ((u4MsgType == CAPWAP_JOIN_REQ) ||
                 (u4MsgType == CAPWAP_JOIN_RSP) ||
                 (u4MsgType == CAPWAP_CONF_STAT_REQ) ||
                 (u4MsgType == CAPWAP_CONF_STAT_RSP) ||
                 (u4MsgType == CAPWAP_CHANGE_STAT_REQ) ||
                 (u4MsgType == CAPWAP_CHANGE_STAT_RSP))
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            pCapwapRxQMsg->u4PktType = CAPWAP_FSM_MSG;
            pCapwapRxQMsg->pRcvBuf = pBuf;
            pCapwapRxQMsg->capwapSessionId.u4_addr[0] = u4DestIp;
            CapwapEnqueCtrlPkts (pCapwapRxQMsg);
            return OSIX_SUCCESS;
        }
        else if ((u4MsgType == CAPWAP_ECHO_REQ)
                 || (u4MsgType == CAPWAP_ECHO_RSP))
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            pCapwapRxQMsg->u4PktType = CAPWAP_ECHO_ALIVE_MSG;
            pCapwapRxQMsg->pRcvBuf = pBuf;
            pCapwapRxQMsg->capwapSessionId.u4_addr[0] = u4DestIp;
            CapwapEnqueCtrlPkts (pCapwapRxQMsg);
            return OSIX_SUCCESS;
        }
        else if ((u4MsgType == CAPWAP_CONF_UPD_RSP) ||
                 (u4MsgType == CAPWAP_WTP_EVENT_RSP) ||
                 (u4MsgType == CAPWAP_IMAGE_DATA_REQ) ||
                 (u4MsgType == CAPWAP_IMAGE_DATA_RSP) ||
                 (u4MsgType == CAPWAP_RESET_REQ) ||
                 (u4MsgType == CAPWAP_DATA_TRANS_REQ) ||
                 (u4MsgType == CAPWAP_DATA_TRANS_RSP) ||
                 (u4MsgType == CAPWAP_CLEAR_CONF_RSP) ||
                 (u4MsgType == CAPWAP_STATION_CONF_REQ) ||
                 (u4MsgType == CAPWAP_CONF_UPD_REQ) ||
                 (u4MsgType == WLAN_CONF_REQ) ||
                 (u4MsgType == CAPWAP_PRIMARY_DISC_RSP))
        {
            MemReleaseMemBlock (CAPWAP_QUEMSG_POOLID, (UINT1 *) pCapwapRxQMsg);
            /* These packets should go to AP HDLR module */
            ApHdlrMsg.ApHdlrQueueReq.u4MsgType = APHDLR_CTRL_MSG;
            ApHdlrMsg.ApHdlrQueueReq.pRcvBuf = pBuf;
            if (WssIfProcessApHdlrMsg (WSS_APHDLR_CAPWAP_QUEUE_REQ, &ApHdlrMsg)
                == OSIX_FAILURE)
            {
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Failed to send the packet to the Ap Hdlr Module \r\n");
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                return OSIX_FAILURE;
            }
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_SUCCESS;
        }
        else if ((u4MsgType % 2) == 1)
        {
            /* If the Received Message Type is odd number send the response
             * with unrecognized request return code*/
            pCapwapRxQMsg->u4PktType = CAPWAP_FSM_MSG;
            pCapwapRxQMsg->pRcvBuf = pBuf;
            pCapwapRxQMsg->capwapSessionId.u4_addr[0] = u4DestIp;
            CapwapEnqueCtrlPkts (pCapwapRxQMsg);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_SUCCESS;
        }
        else if ((u4MsgType % 2) == 0)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Received the unknown message type \r\n");

            MemReleaseMemBlock (CAPWAP_QUEMSG_POOLID, (UINT1 *) pCapwapRxQMsg);
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : CapwapProcessReassemCtrlPacket
 * Description  : Receives the CAPWAP packet and process the messag
 *                if Discovery Request, Discovery response
 * Input        : pBuf - received CAPWAP packet
 * Output       : None
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE
 *****************************************************************************/
UINT4
CapwapProcessReassemCtrlPacket (tCRU_BUF_CHAIN_HEADER * pBuf)
{

    UINT1               u1Val;
    UINT1              *pRcvBuf = NULL;
    UINT4               u4MsgType, u4DestPort, u4DestIp;
    tCapwapRxQMsg      *pCapwapRxQMsg = NULL;
    unApHdlrMsgStruct   ApHdlrMsg;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT1               au1Frame[CAPWAP_HDR_LEN + CAPWAP_CONTROL_HDR_LEN];
    UINT1               u1HdrLen = CAPWAP_HDR_LEN + CAPWAP_CONTROL_HDR_LEN;
    UINT2               u2PacketLen;
    tQueData            QueData;

    CAPWAP_FN_ENTRY ();
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&QueData, 0, sizeof (tQueData));

    pRcvBuf = CAPWAP_BUF_IF_LINEAR (pBuf, 0, u1HdrLen);
    if (pRcvBuf == NULL)
    {
        /* Header is not linear in CRU buffer */
        MEMSET (au1Frame, 0x00, u1HdrLen);
        CAPWAP_COPY_FROM_BUF (pBuf, au1Frame, 0, u1HdrLen);
        pRcvBuf = au1Frame;
    }

    u4DestPort = CRU_BUF_Get_U4Reserved1 (pBuf);
    u4DestIp = CRU_BUF_Get_U4Reserved2 (pBuf);
    /* Parse the received packet to check the packet type */
    CAPWAP_GET_1BYTE (u1Val, pRcvBuf);
    /* Move the buffer pointer to the original position */
    pRcvBuf -= 1;
    if ((u1Val & 0x0F) == CAPWAP_PREAMBLE_DTLS_TYPE)
    {
        /* Remove the CAPWAP DTLS header Move the buffer offset by 4 Bytes to Remove
         * CAPWAP DTLS Header */
        CRU_BUF_Move_ValidOffset (pBuf, CAPWAP_DTLS_HDR_LEN);
        u2PacketLen = (UINT2) CAPWAP_GET_BUF_LEN (pBuf);
        QueData.pBuffer = pBuf;
        QueData.u4Id = 0;
        QueData.u4IpAddr = u4DestIp;
        QueData.u4PortNo = u4DestPort;
        QueData.u4BufSize = u2PacketLen;
        QueData.eQueDataType = DATA_DECRYPT;
        QueData.u4Channel = DTLS_CTRL_CHANNEL;

        if (CapwapDtlsDecryptNotify (&QueData) == OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to decrypt the packet \r\n");
            CAPWAP_RELEASE_CRU_BUF (pBuf);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
        CAPWAP_RELEASE_CRU_BUF (pBuf);
    }
    else if ((u1Val & 0x0F) == CAPWAP_PREAMBLE_PLAINTEXT_TYPE)
    {
        pCapwapRxQMsg = (tCapwapRxQMsg *) MemAllocMemBlk (CAPWAP_QUEMSG_POOLID);

        if (pCapwapRxQMsg == NULL)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "CapwapProcessCtrlPacket:Memory Allocation Failed \r\n");
            CAPWAP_RELEASE_CRU_BUF (pBuf);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
        MEMSET (pCapwapRxQMsg, 0, sizeof (tCapwapRxQMsg));

        /* If the discovery message received as fragment first it will be
         * send to CAPWAP module. The CAPWAP module will reassemble the packet
         * and sends to discovery thread for furhter processing. If the CAPWAP 
         * control message (ex WLAN Configuration request, configuration udpated
         * requests etc)received as fragment it will be send to CAPWAP module 
         * for reassembly. CAPWAP module reassemble the packet and sends to AP 
         * HDLR or WLC HDLR for for further processing. To Limit the reassembly 
         * logic to one module its decided to reassemble only at CAPWAP module */

        CapwapGetMsgTypeFromBuf (pRcvBuf, &u4MsgType);
        if ((u4MsgType < MIN_CAPWAP_MSG_TYPE) || (u4MsgType > WLAN_CONF_RSP))
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "capwapProcessCtrlPacket: Received unknown MsgType \r\n");
            MemReleaseMemBlock (CAPWAP_QUEMSG_POOLID, (UINT1 *) pCapwapRxQMsg);
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
        else if ((u4MsgType == CAPWAP_DISC_REQ)
                 || (u4MsgType == CAPWAP_DISC_RSP))
        {
            pCapwapRxQMsg->u4PktType = CAPWAP_DISC_MSG;
            pCapwapRxQMsg->pRcvBuf = pBuf;
            pCapwapRxQMsg->capwapSessionId.u4_addr[0] = u4DestIp;
            CapwapEnqueDiscPkts (pCapwapRxQMsg);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_SUCCESS;
        }
        else if ((u4MsgType == CAPWAP_JOIN_REQ) ||
                 (u4MsgType == CAPWAP_JOIN_RSP) ||
                 (u4MsgType == CAPWAP_CONF_STAT_REQ) ||
                 (u4MsgType == CAPWAP_CONF_STAT_RSP) ||
                 (u4MsgType == CAPWAP_CHANGE_STAT_REQ) ||
                 (u4MsgType == CAPWAP_CHANGE_STAT_RSP))
        {
            pCapwapRxQMsg->u4PktType = CAPWAP_FSM_MSG;
            pCapwapRxQMsg->pRcvBuf = pBuf;
            pCapwapRxQMsg->capwapSessionId.u4_addr[0] = u4DestIp;
            CapwapEnqueCtrlPkts (pCapwapRxQMsg);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_SUCCESS;
        }
        else if ((u4MsgType == CAPWAP_ECHO_REQ)
                 || (u4MsgType == CAPWAP_ECHO_RSP))
        {
            pCapwapRxQMsg->u4PktType = CAPWAP_ECHO_ALIVE_MSG;
            pCapwapRxQMsg->pRcvBuf = pBuf;
            pCapwapRxQMsg->capwapSessionId.u4_addr[0] = u4DestIp;
            CapwapEnqueCtrlPkts (pCapwapRxQMsg);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_SUCCESS;
        }
        else if ((u4MsgType == CAPWAP_CONF_UPD_RSP) ||
                 (u4MsgType == CAPWAP_WTP_EVENT_RSP) ||
                 (u4MsgType == CAPWAP_IMAGE_DATA_REQ) ||
                 (u4MsgType == CAPWAP_IMAGE_DATA_RSP) ||
                 (u4MsgType == CAPWAP_RESET_REQ) ||
                 (u4MsgType == CAPWAP_DATA_TRANS_REQ) ||
                 (u4MsgType == CAPWAP_DATA_TRANS_RSP) ||
                 (u4MsgType == CAPWAP_CLEAR_CONF_RSP) ||
                 (u4MsgType == CAPWAP_STATION_CONF_REQ) ||
                 (u4MsgType == CAPWAP_CONF_UPD_REQ) ||
                 (u4MsgType == WLAN_CONF_REQ) ||
                 (u4MsgType == CAPWAP_PRIMARY_DISC_RSP))
        {
            MemReleaseMemBlock (CAPWAP_QUEMSG_POOLID, (UINT1 *) pCapwapRxQMsg);
            /* These packets should go to AP HDLR module */
            ApHdlrMsg.ApHdlrQueueReq.u4MsgType = APHDLR_CTRL_MSG;
            ApHdlrMsg.ApHdlrQueueReq.pRcvBuf = pBuf;
            if (WssIfProcessApHdlrMsg (WSS_APHDLR_CAPWAP_QUEUE_REQ, &ApHdlrMsg)
                == OSIX_FAILURE)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Failed to send the packet to the Ap Hdlr Module \r\n");
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_SUCCESS;
        }
        else if ((u4MsgType % 2) == 1)
        {
            /* If the Received Message Type is odd number send the response with
             * unrecognized request return code*/
            pCapwapRxQMsg->u4PktType = CAPWAP_FSM_MSG;
            pCapwapRxQMsg->pRcvBuf = pBuf;
            pCapwapRxQMsg->capwapSessionId.u4_addr[0] = u4DestIp;
            CapwapEnqueCtrlPkts (pCapwapRxQMsg);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_SUCCESS;
        }
        else if ((u4MsgType % 2) == 0)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Received the unknown message type \r\n");
            MemReleaseMemBlock (CAPWAP_QUEMSG_POOLID, (UINT1 *) pCapwapRxQMsg);
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : CapwapProcessDataPacket                                    *
 * Description  : Receives the CAPWAP packet and process the msg             *
 * Input        : pRcvBuf - received CAPWAP packet                           *
 * Output       : None                                                       *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *****************************************************************************/
UINT4
CapwapProcessDataPacket (tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UINT1               u1Val, *pRcvBuf = NULL;
    UINT1               u1HdrVal = 0, u1Type = 0;
    tCapwapRxQMsg      *pCapwapRxQMsg = NULL;
    unApHdlrMsgStruct   ApHdlrMsg;
    UINT4               u4DestPort = 0;
    UINT4               u4DestIp = 0;
    UINT1               u1HdrLen = CAPWAP_HDR_LEN + CAPWAP_CONTROL_HDR_LEN;
    UINT2               u2PacketLen = 0;
    tQueData            QueData;

    CAPWAP_FN_ENTRY ();

    MEMSET (&ApHdlrMsg, 0, sizeof (unApHdlrMsgStruct));

    pRcvBuf = CRU_BUF_GetDataPtr (pBuf->pFirstValidDataDesc);
    u4DestPort = CRU_BUF_Get_U4Reserved1 (pBuf);
    u4DestIp = CRU_BUF_Get_U4Reserved2 (pBuf);

    CAPWAP_GET_1BYTE (u1Val, pRcvBuf);
    pRcvBuf -= 1;

    if ((u1Val & 0x0f) == CAPWAP_PREAMBLE_DTLS_TYPE)
    {
        MEMSET (&QueData, 0, sizeof (tQueData));
        CRU_BUF_Move_ValidOffset (pBuf, CAPWAP_DTLS_HDR_LEN);
        u2PacketLen = (UINT2) CAPWAP_GET_BUF_LEN (pBuf);
        QueData.pBuffer = pBuf;
        QueData.u4Id = 0;
        QueData.u4IpAddr = u4DestIp;
        QueData.u4PortNo = u4DestPort;
        QueData.u4BufSize = u2PacketLen;
        QueData.eQueDataType = DATA_DECRYPT;
        QueData.u4Channel = DTLS_DATA_CHANNEL;

        if (CapwapDtlsDecryptNotify (&QueData) == OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to decrypt the packet \r\n");
            CAPWAP_RELEASE_CRU_BUF (pBuf);
            return OSIX_FAILURE;
        }
        CAPWAP_RELEASE_CRU_BUF (pBuf);

    }
    else if ((u1Val & 0x0f) == CAPWAP_PREAMBLE_PLAINTEXT_TYPE)
    {
        pCapwapRxQMsg = (tCapwapRxQMsg *) MemAllocMemBlk (CAPWAP_QUEMSG_POOLID);

        if (pCapwapRxQMsg == NULL)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "CapwapProcessDataPacket:Memory Allocation Failed\n");
            return OSIX_FAILURE;
        }
        MEMSET (pCapwapRxQMsg, 0, sizeof (tCapwapRxQMsg));

        if ((CapwapKeepAliveReceived (pBuf) == OSIX_SUCCESS))
        {
            /* Data Packet keep alive packets and fragments are handled at CAPWAP 
             * service thread. CAPWAP Module reassembles the received fragements 
             * and send to APHDLR or WLCHDLR for further processing */
            pCapwapRxQMsg->u4PktType = CAPWAP_ECHO_ALIVE_MSG;
            pCapwapRxQMsg->pRcvBuf = pBuf;
            pCapwapRxQMsg->capwapSessionId.u4_addr[0]
                = CRU_BUF_Get_U4Reserved2 (pBuf);
            CAPWAP_TRC (CAPWAP_MGMT_TRC,
                        "Send the received packet to the 'DATA Rx Q' to process by"
                        "service thread \r\n");
            CapwapEnqueCtrlPkts (pCapwapRxQMsg);
        }
        else if ((CapwapFragmentReceived (pBuf) == OSIX_SUCCESS))
        {
            /* Data Packet keep alive packets and fragments are handled at CAPWAP
             * service thread. CAPWAP Module reassembles the received fragements 
             * and send to APHDLR or WLCHDLR for further processing */
            pCapwapRxQMsg->u4PktType = CAPWAP_DATA_FRAG_MSG;
            pCapwapRxQMsg->pRcvBuf = pBuf;
            pCapwapRxQMsg->capwapSessionId.u4_addr[0]
                = CRU_BUF_Get_U4Reserved2 (pBuf);
            CAPWAP_TRC (CAPWAP_MGMT_TRC,
                        "Send the received packet to the 'DATA Rx Q' to process by"
                        " service thread \r\n");
            CapwapEnqueCtrlPkts (pCapwapRxQMsg);
        }
        else
        {
            /* The Received packet is the Data packet. In this CAPWAP Header not
             * required by the APHDLR module. Strip of the CAPWAP header and send 
             * the packet to the APHDLR Module */

            MemReleaseMemBlock (CAPWAP_QUEMSG_POOLID, (UINT1 *) pCapwapRxQMsg);

            /* Get CAPWAP Header Length */
            CAPWAP_COPY_FROM_BUF (pBuf, &u1HdrLen, CAPWAP_HDRLEN_OFFSET,
                                  CAPWAP_HDRLEN_FIELD_LEN);
            CAPWAP_COPY_FROM_BUF (pBuf, &u1Type, 2, CAPWAP_HDRLEN_FIELD_LEN);

            /* Last bit of second byte to check split or mac mode */
            u1HdrVal = (u1Type & 1);
            if (u1HdrVal == SPLIT_MAC)
            {
                ApHdlrMsg.ApHdlrQueueReq.u1MacType = SPLIT_MAC;
            }
            else if (u1HdrVal == LOCAL_MAC)
            {
                ApHdlrMsg.ApHdlrQueueReq.u1MacType = LOCAL_MAC;
                CAPWAP_COPY_FROM_BUF (pBuf,
                                      &(ApHdlrMsg.ApHdlrQueueReq.BssIdMac),
                                      CAPWAP_RADIO_MAC_OFFSET,
                                      sizeof (tMacAddr));
            }

            /* Strip off the CAPWAP Hdr */
            u1HdrLen = (UINT1) ((u1HdrLen & CAPWAP_HDRLEN_MASK) >> 3);
            u1HdrLen = (UINT1) (u1HdrLen << CAPWAP_4BYTE_WORD_TO_NUMOF_BYTES);
            CRU_BUF_Move_ValidOffset (pBuf, u1HdrLen);

            ApHdlrMsg.ApHdlrQueueReq.u4MsgType = APHDLR_DATA_RX_MSG;
            ApHdlrMsg.ApHdlrQueueReq.pRcvBuf = pBuf;
            if (WssIfProcessApHdlrMsg (WSS_APHDLR_CAPWAP_QUEUE_REQ, &ApHdlrMsg)
                == OSIX_FAILURE)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Failed to send the packet to the Ap Hdlr Module \r\n");
                return OSIX_FAILURE;
            }
        }
    }
    else
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "capwapProcessDataPacket:Received unknown Msg \r\n");
        MemReleaseMemBlock (CAPWAP_QUEMSG_POOLID, (UINT1 *) pCapwapRxQMsg);
        return OSIX_FAILURE;
    }
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : CapwapEchoIntervalTmrExp                                   *
 * Description  : Processed Echo interval timer expiry                       *
 * Input        : pArg - received CAPWAP packet                           *
 * Output       : None                                                       *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *****************************************************************************/
VOID
CapwapEchoIntervalTmrExp (VOID *pArg)
{
    tRemoteSessionManager *pSessEntry = NULL;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT2               u2IntId = 0;
    UINT2               u2EchoInterval = 0;

    CAPWAP_FN_ENTRY ();

    WSS_IF_DB_TEMP_MEM_ALLOC_VOID_RETURN (pWssIfCapwapDB)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpEchoInterval = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2IntId;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
        == OSIX_SUCCESS)
    {
        u2EchoInterval = pWssIfCapwapDB->CapwapGetDB.u2WtpEchoInterval;
    }

    pSessEntry = (tRemoteSessionManager *) pArg;
    /* Construct Echo Request */
    CapwapTransmitEchoRequest (pSessEntry);

    CapwapTmrStart (pSessEntry, CAPWAP_ECHO_INTERVAL_TMR, u2EchoInterval);

    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    CAPWAP_FN_EXIT ();
}

/************************************************************************/
/*  Function Name   : CapwapValidateRadioInfo                           */
/*  Description     : The function validates the WTP Radio information  */
/*                    received in CAPWAP discovery request. Following   */
/*                    two validations done in this function,            */
/*                 1. Number of radios received in discovery request    */
/*                    against the virtual radio interfaces created in   */
/*                    WLC (based on WTP Model Number)                   */
/*                 2  Received Radio Type received in discovery request */
/*                    against the WLC supported radio type              */
/*                                                                      */
/*  Input(s)        : discReqMsg - parsed CAPWAP Discovery request      */
/*                    pRcvBuf - Received discovery packet               */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
CapwapValidateRadioInfo (UINT1 *pRcvBuf,
                         tCapwapControlPacket * pCapMsg, UINT2 u2Index)
{
    UINT4               u4Offset, u4RadioType;
    UINT2               u2NumMsgElems;
    UINT1               u1Index, u1RadioId;

    CAPWAP_FN_ENTRY ();

    u2NumMsgElems = pCapMsg->capwapMsgElm[u2Index].numMsgElemInstance;
    for (u1Index = 0; u1Index < u2NumMsgElems; u1Index++)
    {
        if (u1Index == 0)
        {
            u4Offset = pCapMsg->capwapMsgElm[u2Index].pu2Offset[u1Index];
            pRcvBuf += u4Offset + CAPWAP_MSG_ELEM_TYPE_LEN;
        }
        else
        {
            pRcvBuf += CAPWAP_MSG_ELEM_TYPE_LEN;
        }
        CAPWAP_GET_1BYTE (u1RadioId, pRcvBuf);
        CAPWAP_GET_4BYTE (u4RadioType, pRcvBuf);
        if ((u4RadioType != CLI_RADIO_TYPEB) && (u4RadioType != CLI_RADIO_TYPEA)
            && (u4RadioType != CLI_RADIO_TYPEG)
            && (u4RadioType != CLI_RADIO_TYPEBG)
            && (u4RadioType != CLI_RADIO_TYPEAN)
            && (u4RadioType != CLI_RADIO_TYPEBGN)
            && (u4RadioType != CLI_RADIO_TYPEAC))
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Radio information validation Failed \r\n");
            return OSIX_FAILURE;
        }
    }
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapSetRadioInfo                                */
/*  Description     : This function invoked the Radio module to update  */
/*                    the radio type in WTP                             */
/*  Input(s)        : pRcvBuf - received CAPWAP Join response           */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
CapwapSetRadioInfo (UINT1 *pRcvBuf,
                    tCapwapControlPacket * pCwMsg, UINT2 u2MsgIndex)
{
    tRadioIfGetDB       RadioIfGetDB;
    UINT4               u4Offset = 0;
    UINT2               u2NumMsgElems;
    UINT1               u1RadioId = 0;
    UINT4               u4RadioType = 0;
    UINT1               u1Index = 0;
    /*
       UINT1                     au1OperationalRate_Radio_A[] = {12,18,24,36,48,72,96,108};
       UINT1                     au1OperationalRate_Radio_B[] = {2,4,11,22};
     */
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    u2NumMsgElems = pCwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;
    for (u1Index = 0; u1Index < u2NumMsgElems; u1Index++)
    {
        if (u1Index == 0)
        {
            u4Offset = pCwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[0];
            pRcvBuf += u4Offset + CAPWAP_MSG_ELEM_TYPE_LEN;
        }
        else
        {
            pRcvBuf += CAPWAP_MSG_ELEM_TYPE_LEN;
        }

        CAPWAP_GET_1BYTE (u1RadioId, pRcvBuf);
        CAPWAP_GET_4BYTE (u4RadioType, pRcvBuf);

        RadioIfGetDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_TRUE;
        RadioIfGetDB.RadioIfGetAllDB.u1RadioId = u1RadioId;
        RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType = u4RadioType;

        if ((RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType == CLI_RADIO_TYPEB)
            || (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                CLI_RADIO_TYPEBG)
            || (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                CLI_RADIO_TYPEBGN))
        {
            MEMCPY (RadioIfGetDB.RadioIfGetAllDB.au1SupportedRate,
                    gau1OperationalRate_RadioB,
                    sizeof (gau1OperationalRate_RadioB));
        }

        else if ((RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                  CLI_RADIO_TYPEA)
                 || (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                     CLI_RADIO_TYPEG)
                 || (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                     CLI_RADIO_TYPEAN)
                 || (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                     CLI_RADIO_TYPEAC))
        {
            MEMCPY (RadioIfGetDB.RadioIfGetAllDB.au1SupportedRate,
                    gau1OperationalRate_RadioA,
                    sizeof (gau1OperationalRate_RadioA));
        }

        RadioIfGetDB.RadioIfIsGetAllDB.bSupportedRate = OSIX_TRUE;

        if (WssIfProcessRadioIfDBMsg (WSS_SET_PHY_RADIO_IF_DB, &RadioIfGetDB)
            == OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to set Radio Type in DB \r\n");
            return OSIX_FAILURE;
        }
    }
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapValidateIpAddress                           */
/*  Description     : This function validates the received source IP    */
/*                    with the user configured while list.              */
/*  Input(s)        : discReqMsg - parsed CAPWAP Discovery request      */
/*                    pRcvBuf - Received discovery packet               */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
CapwapValidateIpAddress (UINT4 u4SrtIp)
{
    /* Get user configured black list from WSS-WLC module
       validate the u4SrcIP with the user configured black list */

    /*Get user configured white list from WSS-WLC module
       Validate white list also */
    UNUSED_PARAM (u4SrtIp);
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : WssApGetUdpPort                                   */
/*  Description     : This function CAPWAP control port number          */
/*  Input(s)        : None                                              */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
WssApGetUdpPort (UINT4 *u4CtrlPort)
{

    *u4CtrlPort = CAPWAP_CONTROL_PORT;
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function     : CapwapGetEcnSupport                                        *
* Description  : Gets ECN Support information                               *
* Input        : pEcnSupport, pMsgLen                                       *
* Output       : None                                                       *
* Returns      : None                                                       *
*****************************************************************************/
INT4
CapwapGetEcnSupport (tEcnSupport * pEcnSupport, UINT4 *pMsgLen)
{
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT2               u2IntId = 0;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpEcnSupport = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2IntId;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
        == OSIX_SUCCESS)
    {
        pEcnSupport->u2MsgEleType = ECN_SUPPORT;
        pEcnSupport->u1EcnSupport = pWssIfCapwapDB->CapwapGetDB.u1WtpEcnSupport;
        pEcnSupport->u2MsgEleLen = ECN_SUPPORT_LEN;
        *pMsgLen += pEcnSupport->u2MsgEleLen + CAPWAP_MSG_ELEM_TYPE_LEN;
        pEcnSupport->isOptional = OSIX_TRUE;
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_SUCCESS;
    }
    else
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to get ECN Support Data\r\n");
        return OSIX_FAILURE;
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}

/***********************************************************************
*  Function Name   : CapwapGetWtpBaseMacAddr                           *
*  Description     : Gets MAC base address                             *
*  Input(s)        : pBuf, pJoinReqMsg, wtpBaseMacAddr                 *
*  Output(s)       : None                                              *
*  Returns         : None                                              *
************************************************************************/
INT4
CapwapGetWtpBaseMacAddr (tCRU_BUF_CHAIN_HEADER * pBuf,
                         tCapwapControlPacket * pJoinReqMsg,
                         tMacAddr wtpBaseMacAddr)
{
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pJoinReqMsg);
    UNUSED_PARAM (wtpBaseMacAddr);
    return OSIX_FAILURE;
}

/*****************************************************************************
 * Function     : CapwapProcessKeepAlivePacket                               *
 * Description  : Process keep alive pkts                                    *
 * Input        : pBuf, pSess                                                *
 * Output       : None                                                       *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *****************************************************************************/
INT4
CapwapProcessKeepAlivePacket (tCRU_BUF_CHAIN_HEADER * pBuf,
                              tRemoteSessionManager * pSess)
{
    UNUSED_PARAM (pBuf);
    CAPWAP_FN_ENTRY ();
    CapwapTmrStop (pSess, CAPWAP_DATACHNL_DEADINTERVAL_TMR);
    CapwapTmrStop (pSess, CAPWAP_DATACHNL_KEEPALIVE_TMR);
    CapwapTmrStart (pSess, CAPWAP_DATACHNL_KEEPALIVE_TMR,
                    CAPWAP_DATACHNL_KEEPALIVE_TIMEOUT);
    pSess->wtpCapwapStats.keepAliveMsgRcvd++;
    CapwapStopKeepAlive (pSess);
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/***********************************************************************
*  Function Name   : CapwapGetWtpBoardDataFromBuf                      *
*  Description     : Gets WTP data from buf                            *
*  Input(s)        : pRcvBuf, u4Offset, pWtpBoardData                  *
*  Output(s)       : None                                              *
*  Returns         : None                                              *
************************************************************************/
INT4
CapwapGetWtpBoardDataFromBuf (UINT1 *pRcvBuf,
                              UINT4 u4Offset, tWtpBoardData * pWtpBoardData)
{
    UNUSED_PARAM (pRcvBuf);
    UNUSED_PARAM (u4Offset);
    UNUSED_PARAM (pWtpBoardData);
    return OSIX_FAILURE;
}

/***********************************************************************
*  Function Name   : CapwapGetAcDescriptorFromBuf                      *
*  Description     : Gets WTP data from buf                            *
*  Input(s)        : pRcvBuf, u4Offset, pAcDescriptor, u2NumMsgElems   *
*  Output(s)       : None                                              *
*  Returns         : None                                              *
************************************************************************/
INT4
CapwapGetAcDescriptorFromBuf (UINT1 *pRcvBuf,
                              UINT4 u4Offset,
                              tAcDescriptor * pAcDescriptor,
                              UINT2 u2NumMsgElems)
{
    UINT2               u2Type = 0, u2Len = 0, u2MsgElemLen = 0;
    pRcvBuf += u4Offset;
    CAPWAP_GET_2BYTE (pAcDescriptor->u2MsgEleType, pRcvBuf);
    CAPWAP_GET_2BYTE (pAcDescriptor->u2MsgEleLen, pRcvBuf);
    CAPWAP_GET_2BYTE (pAcDescriptor->stations, pRcvBuf);
    CAPWAP_GET_2BYTE (pAcDescriptor->stationLimit, pRcvBuf);
    CAPWAP_GET_2BYTE (pAcDescriptor->activeWtp, pRcvBuf);
    CAPWAP_GET_2BYTE (pAcDescriptor->maxWtp, pRcvBuf);
    CAPWAP_GET_1BYTE (pAcDescriptor->security, pRcvBuf);
    CAPWAP_GET_1BYTE (pAcDescriptor->radioMacField, pRcvBuf);
    CAPWAP_GET_1BYTE (pAcDescriptor->resvd, pRcvBuf);
    CAPWAP_GET_1BYTE (pAcDescriptor->dtlsPolicy, pRcvBuf);
    u2MsgElemLen = (pAcDescriptor->u2MsgEleLen - 12);
    while (u2MsgElemLen > 0)
    {
        CAPWAP_GET_4BYTE (pAcDescriptor->ACVendorId, pRcvBuf);
        CAPWAP_GET_2BYTE (u2Type, pRcvBuf);
        CAPWAP_GET_2BYTE (u2Len, pRcvBuf);
        pAcDescriptor->VendDesc[u2Type].vendorInfoType = u2Type;
        pAcDescriptor->VendDesc[u2Type].u2VendorInfoLength = u2Len;
        CAPWAP_GET_NBYTE (pAcDescriptor->VendDesc[u2Type].vendorInfoData,
                          pRcvBuf, u2Len);
        u2MsgElemLen -= u2Len + CAPWAP_MSG_ELEM_TYPE_LEN + CAPWAP_VENDOR_ID_LEN;
    }
    UNUSED_PARAM (u2NumMsgElems);
    return OSIX_SUCCESS;
}

/***********************************************************************
*  Function Name   : CapwapGetAcNameFromBuf                            *
*  Description     : Gets AC name from buf                             *
*  Input(s)        : pRcvBuf, u4Offset, pAcName, u2NumMsgElems         *
*  Output(s)       : None                                              *
*  Returns         : None                                              *
************************************************************************/
INT4
CapwapGetAcNameFromBuf (UINT1 *pRcvBuf,
                        UINT4 u4Offset, tACName * pAcName, UINT2 u2NumMsgElems)
{
    pRcvBuf += u4Offset;
    CAPWAP_GET_2BYTE (pAcName->u2MsgEleType, pRcvBuf);
    CAPWAP_GET_2BYTE (pAcName->u2MsgEleLen, pRcvBuf);
    CAPWAP_GET_NBYTE (pAcName->wlcName, pRcvBuf, pAcName->u2MsgEleLen);
    UNUSED_PARAM (u2NumMsgElems);
    return OSIX_SUCCESS;
}

/***********************************************************************
*  Function Name   : CapwapGetCtrlIP4AddrFromBuf                       *
*  Description     : Gets contrl IPv4 address from buf                 *
*  Input(s)        : pRcvBuf, u4Offset, pControlIP4Addr, u2NumMsgElems *
*  Output(s)       : None                                              *
*  Returns         : None                                              *
************************************************************************/
INT4
CapwapGetCtrlIP4AddrFromBuf (UINT1 *pRcvBuf,
                             UINT4 u4Offset,
                             tCtrlIpAddr * pControlIP4Addr, UINT2 u2NumMsgElems)
{
    pRcvBuf += u4Offset;
    CAPWAP_GET_2BYTE (pControlIP4Addr->u2MsgEleType, pRcvBuf);
    CAPWAP_GET_2BYTE (pControlIP4Addr->u2MsgEleLen, pRcvBuf);
    if (pControlIP4Addr->u2MsgEleType == CAPWAP_CTRL_IPV4_ADDR)
    {
        CAPWAP_GET_4BYTE (pControlIP4Addr->ipAddr.u4_addr[0], pRcvBuf);
    }
    CAPWAP_GET_2BYTE (pControlIP4Addr->wtpCount, pRcvBuf);
    UNUSED_PARAM (u2NumMsgElems);
    return OSIX_SUCCESS;
}

/***********************************************************************
*  Function Name   : CapwapGetAcIp4ListFromBuf                         *
*  Description     : Gets AC IPv4 List from buf                        *
*  Input(s)        : pRcvBuf, u4Offset,pAcIpv4List, u2NumMsgElems      * 
*  Output(s)       : None                                              *
*  Returns         : None                                              *
************************************************************************/
INT4
CapwapGetAcIp4ListFromBuf (UINT1 *pRcvBuf,
                           UINT4 u4Offset,
                           tACIplist * pAcIpv4List, UINT2 u2NumMsgElems)
{
    UINT1               u1Index = 0;
    pRcvBuf += u4Offset;
    CAPWAP_GET_2BYTE (pAcIpv4List->u2MsgEleType, pRcvBuf);
    CAPWAP_GET_2BYTE (pAcIpv4List->u2MsgEleLen, pRcvBuf);
    if (pAcIpv4List->u2MsgEleType == AC_IPv4_LIST)
    {
        for (u1Index = 0; u1Index < ((pAcIpv4List->u2MsgEleLen) / 4); u1Index++)
        {
            CAPWAP_GET_4BYTE (pAcIpv4List->ipAddr[u1Index].u4_addr[0], pRcvBuf);
        }
    }
    UNUSED_PARAM (u2NumMsgElems);

    return OSIX_SUCCESS;
}

/***********************************************************************
*  Function Name   : CapwapGetVendorInfoFromBuf                        *
*  Description     : Gets vendor information from buf                  *
*  Input(s)        : pRcvBuf, u4Offset,pVendorPayload, u2NumMsgElems   * 
*  Output(s)       : None                                              *
*  Returns         : None                                              *
************************************************************************/
INT4
CapwapGetVendorInfoFromBuf (UINT1 *pRcvBuf,
                            UINT4 u4Offset,
                            tVendorSpecPayload * pVendorPayload,
                            UINT2 u2NumMsgElems)
{
    pRcvBuf += u4Offset;
    CAPWAP_GET_2BYTE (pVendorPayload->u2MsgEleType, pRcvBuf);
    CAPWAP_GET_2BYTE (pVendorPayload->u2MsgEleLen, pRcvBuf);
    UNUSED_PARAM (u2NumMsgElems);

    return OSIX_SUCCESS;
}

/****************************************************************************
* Function     : CapwapConstructCpHeader                                    *
* Description  : Construct CAPWAP Hdr                                       *
* Input        : pCapwapHdr                                                 *
* Output       : None                                                       *
* Returns      : None                                                       *
*****************************************************************************/
INT4
CapwapConstructCpHeader (tCapwapHdr * pCapwapHdr)
{

    pCapwapHdr->u1Preamble = 0;
    pCapwapHdr->u2HlenRidWbidFlagT = CAPWAP_WTP_HLEN_RID_WBID_FLAGT;
    pCapwapHdr->u1FlagsFLWMKResd = 0;
    pCapwapHdr->u2FragId = 0;
    pCapwapHdr->u2FragOffsetRes3 = 0;
    pCapwapHdr->u1RadMacAddrLength = 0;
    pCapwapHdr->u1WirelessInfoLength = 0;
    pCapwapHdr->u2HdrLen = CAPWAP_HDR_LEN;
    return OSIX_SUCCESS;
}

/***********************************************************************
*  Function Name   : CapwapGetResultCode                               *
*  Description     : Gets Result code local buffer                     *
*  Input(s)        : pRcvBuf, u4Offset, pResCode, u2NumMsgElems        *
*  Output(s)       : None                                              *
*  Returns         : None                                              *
************************************************************************/
INT4
CapwapGetResultCodeFromBuf (UINT1 *pRcvBuf,
                            UINT4 u4Offset,
                            tResCode * pResCode, UINT2 u2NumMsgElems)
{
    pRcvBuf += u4Offset;
    CAPWAP_GET_2BYTE (pResCode->u2MsgEleType, pRcvBuf);
    CAPWAP_GET_2BYTE (pResCode->u2MsgEleLen, pRcvBuf);
    CAPWAP_GET_4BYTE (pResCode->u4Value, pRcvBuf);
    UNUSED_PARAM (u2NumMsgElems);
    return OSIX_SUCCESS;
}

/***********************************************************************
*  Function Name   : CapwapGetEcnSupportFromBuf                        *
*  Description     : Gets ECN support local buffer                     *
*  Input(s)        : pRcvBuf, u4Offset, ecnSupport, u2NumMsgElems      *
*  Output(s)       : None                                              *
*  Returns         : None                                              *
************************************************************************/
INT4
CapwapGetEcnSupportFromBuf (UINT1 *pRcvBuf,
                            UINT4 u4Offset,
                            tEcnSupport * ecnSupport, UINT2 u2NumMsgElems)
{
    pRcvBuf += u4Offset;
    CAPWAP_GET_2BYTE (ecnSupport->u2MsgEleType, pRcvBuf);
    CAPWAP_GET_2BYTE (ecnSupport->u2MsgEleLen, pRcvBuf);
    CAPWAP_GET_1BYTE (ecnSupport->u1EcnSupport, pRcvBuf);
    UNUSED_PARAM (u2NumMsgElems);
    return OSIX_SUCCESS;

}

/***********************************************************************
*  Function Name   : CapwapGetLocalIPAddrFromBuf                       *
*  Description     : Gets IP from locall buffer                        *
*  Input(s)        : pRcvBuf, u4Offset, pLocalIpAddr, u2NumMsgElems    *
*  Output(s)       : None                                              *
*  Returns         : None                                              *
************************************************************************/
INT4
CapwapGetLocalIPAddrFromBuf (UINT1 *pRcvBuf,
                             UINT4 u4Offset,
                             tLocalIpAddr * pLocalIpAddr, UINT2 u2NumMsgElems)
{
    pRcvBuf += u4Offset;
    CAPWAP_GET_2BYTE (pLocalIpAddr->u2MsgEleType, pRcvBuf);
    CAPWAP_GET_2BYTE (pLocalIpAddr->u2MsgEleLen, pRcvBuf);
    if (pLocalIpAddr->u2MsgEleType == CAPWAP_LOCAL_IPV4_ADDR)
    {
        CAPWAP_GET_4BYTE (pLocalIpAddr->ipAddr.u4_addr[0], pRcvBuf);
    }

    UNUSED_PARAM (u2NumMsgElems);
    return OSIX_SUCCESS;
}

/***********************************************************************
*  Function Name   : CapwapGetTransportProtFromBuf                     *
*  Description     : Gets transport protocol inform                    *
*  Input(s)        : pRcvBuf, u4Offset, transProto, u2NumMsgElems      *
*  Output(s)       : None                                              *
*  Returns         : None                                              *
************************************************************************/
INT4
CapwapGetTransportProtFromBuf (UINT1 *pRcvBuf,
                               UINT4 u4Offset,
                               tCapwapTransprotocol * transProto,
                               UINT2 u2NumMsgElems)
{
    pRcvBuf += u4Offset;
    CAPWAP_GET_2BYTE (transProto->u2MsgEleType, pRcvBuf);
    CAPWAP_GET_2BYTE (transProto->u2MsgEleLen, pRcvBuf);
    CAPWAP_GET_1BYTE (transProto->transportType, pRcvBuf);
    UNUSED_PARAM (u2NumMsgElems);
    return OSIX_SUCCESS;

}

/***********************************************************************
*  Function Name   : CapwapGetImageIdFromBuf                           *
*  Description     : Gets image ID from bug                            *
*  Input(s)        : pRcvBuf, u4Offset, imageId, u2NumMsgElems         *
*  Output(s)       : None                                              *
*  Returns         : None                                              *
************************************************************************/
INT4
CapwapGetImageIdFromBuf (UINT1 *pRcvBuf,
                         UINT4 u4Offset,
                         tImageId * imageId, UINT2 u2NumMsgElems)
{
    pRcvBuf += u4Offset;
    CAPWAP_GET_2BYTE (imageId->u2MsgEleType, pRcvBuf);
    CAPWAP_GET_2BYTE (imageId->u2MsgEleLen, pRcvBuf);
    CAPWAP_GET_4BYTE (imageId->u4VendorId, pRcvBuf);
    CAPWAP_GET_NBYTE (imageId->data, pRcvBuf, imageId->u2MsgEleLen);
    UNUSED_PARAM (u2NumMsgElems);
    return OSIX_SUCCESS;

}

/************************************************************************/
/*  Function Name   : CapwapValidateLocationData                        */
/*  Description     : The function validates the WTP Location Data      */
/*                    mode of the received CAPWAP control packets       */
/*  Input(s)        : tCapwapControlPacket -received CW control packet  */
/*                    pRcvBuf - Received discovery packet               */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
CapwapValidateLocationData (UINT1 *pRcvBuf,
                            tCapwapControlPacket * pCwMsg, UINT2 u2MsgIndex)
{
    UINT2               u2NumMsgElems;
    UINT4               u4Offset = 0;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT2               u2Type, u2Len;

    CAPWAP_FN_ENTRY ();
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    u2NumMsgElems = pCwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;
    if (u2NumMsgElems > 1)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "The Message element Location Data Received More than once \r\n");
        return OSIX_FAILURE;
    }
    else
    {
        u4Offset = pCwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[0];
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpLocation = OSIX_TRUE;
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = pCwMsg->u2IntProfileId;
        pRcvBuf += u4Offset;
        CAPWAP_GET_2BYTE (u2Type, pRcvBuf);
        CAPWAP_GET_2BYTE (u2Len, pRcvBuf);
        CAPWAP_GET_NBYTE (pWssIfCapwapDB->CapwapGetDB.au1WtpLocation, pRcvBuf,
                          u2Len);
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_VALIDATE_DB, pWssIfCapwapDB)
            == OSIX_FAILURE)
        {
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_SET_DB, pWssIfCapwapDB)
                == OSIX_FAILURE)
            {
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Failed to Set the Received Location Data \r\n");
                return OSIX_FAILURE;
            }
        }
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/***********************************************************************
*  Function Name   : CapwapValidateRadioStats                          *
*  Description     : Validates Radio Statistics                        *
*  Input(s)        : pRcvBuf, cwMsg, u2MsgIndex                        *
*  Output(s)       : None                                              *
*  Returns         : None                                              *
************************************************************************/
INT4
CapwapValidateRadioStats (UINT1 *pRcvBuf,
                          tCapwapControlPacket * cwMsg, UINT2 u2MsgIndex)
{
    UINT4               u4Offset = 0;
    UINT2               u2NumMsgElems;
    tWtpProfile         WtpProfile;
    tRadioIfAdminStatus RadioAdmin;

    MEMSET (&WtpProfile, 0, sizeof (tWtpProfile));
    MEMSET (&RadioAdmin, 0, sizeof (tRadioIfAdminStatus));

    u2NumMsgElems = cwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;
    if (u2NumMsgElems > 0)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "The Message element Radio Statistics Received More than once \r\n");
        return OSIX_FAILURE;
    }
    else
    {
        u4Offset = cwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[0];
        pRcvBuf += u4Offset;
        CAPWAP_GET_2BYTE (RadioAdmin.u2MessageType, pRcvBuf);
        CAPWAP_GET_2BYTE (RadioAdmin.u2MessageLength, pRcvBuf);
        CAPWAP_GET_1BYTE (RadioAdmin.u1RadioId, pRcvBuf);
        CAPWAP_GET_1BYTE (RadioAdmin.u1AdminStatus, pRcvBuf);
    }
    return OSIX_SUCCESS;
}

/***********************************************************************
*  Function Name   : CapwapValidateAcTimeStamp                         *
*  Description     : Validates AC time stamp                           *
*  Input(s)        : pRcvBuf, cwMsg, u2MsgIndex                        *
*  Output(s)       : None                                              *
*  Returns         : None                                              *
************************************************************************/
INT4
CapwapValidateAcTimeStamp (UINT1 *pRcvBuf,
                           tCapwapControlPacket * cwMsg, UINT2 u2MsgIndex)
{
    UINT4               u4Offset = 0;
    UINT2               u2NumMsgElems;
    tWtpProfile         WtpProfile;
    tACtimestamp        ACtime;

#ifdef CLKIWF_WANTED
    tClkSysTimeInfo     ClkSysTimeInfo;
    tUtlTm              RecvdWlcTime;

    MEMSET (&ClkSysTimeInfo, 0, sizeof (tClkSysTimeInfo));
    MEMSET (&RecvdWlcTime, 0, sizeof (tUtlTm));
#endif

    MEMSET (&WtpProfile, 0, sizeof (tWtpProfile));
    MEMSET (&ACtime, 0, sizeof (tACtimestamp));

    u2NumMsgElems = cwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;
    if (u2NumMsgElems > 1)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "The Message element AC-Timestamp Received More than once \r\n");
        return OSIX_FAILURE;
    }
    else
    {
        u4Offset = cwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[0];
        pRcvBuf += u4Offset;
        CAPWAP_GET_2BYTE (ACtime.u2MsgEleType, pRcvBuf);
        CAPWAP_GET_2BYTE (ACtime.u2MsgEleLen, pRcvBuf);
        CAPWAP_GET_4BYTE (ACtime.u4Timestamp, pRcvBuf);
    }

#ifdef SNTP_WANTED
    /* Convert the seconds to Tm */
    SntpSecToTm (ACtime.u4Timestamp, &RecvdWlcTime);

#ifdef CLKIWF_WANTED
    /*Call ClkIwf API to SetTime */
    ClkSysTimeInfo.FsClkTimeVal.UtlTmVal = RecvdWlcTime;
    if (ClkIwfSetClock (&ClkSysTimeInfo, CLK_HANDSET_CLK) == OSIX_FAILURE)
    {
        return ISS_FAILURE;
    }
#else
    UtlSetTime (&RecvdWlcTime);
#endif

#endif
    return OSIX_SUCCESS;
}

/***********************************************************************
*  Function Name   : CapwapValidateAcNamePriority                      *
*  Description     : Validates AC name priority                        *
*  Input(s)        : pRcvBuf, cwMsg, u2MsgIndex                        *
*  Output(s)       : None                                              *
*  Returns         : None                                              *
************************************************************************/

INT4
CapwapValidateAcNamePriority (UINT1 *pRcvBuf,
                              tCapwapControlPacket * pCwMsg, UINT2 u2MsgIndex)
{
    UINT2               u2NumMsgElems;
    UINT4               u4Offset = 0;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tWssIfCapDB        *ptmpWssIfCapwapDB = NULL;
    UINT2               u2IntId = 0, u2Type, u2Len;
    UINT1               u1Index = 0;
    UINT1               au1Array[TEMP_BUF_SIZE];

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        WSS_IF_DB_TEMP_MEM_ALLOC (ptmpWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (ptmpWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    u2NumMsgElems = pCwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;
    if (u2NumMsgElems > 1)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (ptmpWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "The Message element Ac Name with Priority Received More than once \r\n");
        return OSIX_FAILURE;
    }
    else
    {
        pWssIfCapwapDB->CapwapIsGetAllDB.bAcNamewithPriority = OSIX_TRUE;
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2IntId;
        ptmpWssIfCapwapDB->CapwapIsGetAllDB.bAcNamewithPriority = OSIX_TRUE;
        ptmpWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
        ptmpWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2IntId;

        u4Offset = pCwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[0];
        pRcvBuf += u4Offset;
        CAPWAP_GET_2BYTE (u2Type, pRcvBuf);
        CAPWAP_GET_2BYTE (u2Len, pRcvBuf);
        CAPWAP_GET_1BYTE (pWssIfCapwapDB->CapwapGetDB.AcNameWithPri[0].
                          u1Priority, pRcvBuf);
        /* Getting (N-1) bytes from the packet as AC Name, since the first
         * byte indicates the priority field in value block and the rest of
         * the bytes (N-1) indicate the AC name. */
        CAPWAP_GET_NBYTE (pWssIfCapwapDB->CapwapGetDB.AcNameWithPri[0].wlcName,
                          pRcvBuf, (u2Len - 1));
        if (pWssIfCapwapDB->CapwapGetDB.AcNameWithPri[0].u1Priority !=
            OSIX_FALSE)
        {
            memset (&au1Array[0], 0, sizeof (au1Array));
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, ptmpWssIfCapwapDB)
                == OSIX_FAILURE)
            {
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                WSS_IF_DB_TEMP_MEM_RELEASE (ptmpWssIfCapwapDB);
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Failed to Validate the AC Name List \r\n");
                return OSIX_FAILURE;
            }
            u1Index =
                pWssIfCapwapDB->CapwapGetDB.AcNameWithPri[0].u1Priority - 1;
            ptmpWssIfCapwapDB->CapwapGetDB.AcNameWithPri[u1Index].u1Priority =
                pWssIfCapwapDB->CapwapGetDB.AcNameWithPri[0].u1Priority;

            MEMCPY (ptmpWssIfCapwapDB->CapwapGetDB.AcNameWithPri[u1Index].
                    wlcName,
                    pWssIfCapwapDB->CapwapGetDB.AcNameWithPri[0].wlcName,
                    STRLEN (pWssIfCapwapDB->CapwapGetDB.AcNameWithPri[0].
                            wlcName) + 1);

            if (MEMCMP (pWssIfCapwapDB->CapwapGetDB.AcNameWithPri[0].wlcName,
                        &au1Array[0], sizeof (au1Array)) == 0)
            {
                MEMSET (&
                        (ptmpWssIfCapwapDB->CapwapGetDB.AcNameWithPri[u1Index].
                         wlcName[0]), 0, TEMP_BUF_SIZE);
            }
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_SET_DB, ptmpWssIfCapwapDB)
                == OSIX_FAILURE)
            {
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                WSS_IF_DB_TEMP_MEM_RELEASE (ptmpWssIfCapwapDB);
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Failed to Validate the AC Name List \r\n");
                return OSIX_FAILURE;
            }
            CapwapDiscTmrStart (CAPWAP_PRIMARY_DISC_INTERVAL_TMR,
                                PRIMARY_DISC_INTERVAL_TIMEOUT);
        }
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    WSS_IF_DB_TEMP_MEM_RELEASE (ptmpWssIfCapwapDB);
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/***********************************************************************
*  Function Name   : CapwapValidateAddMacEntry                         *
*  Description     : Validates Added MAC entry                         *
*  Input(s)        : pRcvBuf, cwMsg, u2MsgIndex                        *
*  Output(s)       : None                                              *
*  Returns         : None                                              *
************************************************************************/
INT4
CapwapValidateAddMacEntry (UINT1 *pRcvBuf,
                           tCapwapControlPacket * cwMsg, UINT2 u2MsgIndex)
{
    UINT4               u4Offset = 0;
    UINT2               u2NumMsgElems;
    tWtpProfile         WtpProfile;
    tAddMacAclEntry     AddMac;

    MEMSET (&WtpProfile, 0, sizeof (tWtpProfile));
    MEMSET (&AddMac, 0, sizeof (tAddMacAclEntry));

    u2NumMsgElems = cwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;
    if (u2NumMsgElems > 0)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "The Message element Capwap Timer Received More than once \r\n");
        return OSIX_FAILURE;
    }
    else
    {
        u4Offset = cwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[0];
        pRcvBuf += u4Offset;
        CAPWAP_GET_2BYTE (AddMac.u2MsgEleType, pRcvBuf);
        CAPWAP_GET_2BYTE (AddMac.u2MsgEleLen, pRcvBuf);
        CAPWAP_GET_1BYTE (AddMac.u1NumEntries, pRcvBuf);
        CAPWAP_GET_1BYTE (AddMac.u1Length, pRcvBuf);
        CAPWAP_GET_2BYTE (AddMac.u2MacAddr, pRcvBuf);

    }
    return OSIX_SUCCESS;
}

/***********************************************************************
*  Function Name   : CapwapValidateDeleteMacEntry                      *
*  Description     : Validates Deleted MAC entry                       *
*  Input(s)        : pRcvBuf, cwMsg, u2MsgIndex                        *
*  Output(s)       : None                                              *
*  Returns         : None                                              *
************************************************************************/
INT4
CapwapValidateDeleteMacEntry (UINT1 *pRcvBuf,
                              tCapwapControlPacket * cwMsg, UINT2 u2MsgIndex)
{
    UINT4               u4Offset = 0;
    UINT2               u2NumMsgElems;
    tWtpProfile         WtpProfile;
    tDeleteMacAclEntry  DeleteMac;

    MEMSET (&WtpProfile, 0, sizeof (tWtpProfile));
    MEMSET (&DeleteMac, 0, sizeof (tDeleteMacAclEntry));

    u2NumMsgElems = cwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;
    if (u2NumMsgElems > 0)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "The Message element Delete Mac Entry Received More than once \r\n");
        return OSIX_FAILURE;
    }
    else
    {
        u4Offset = cwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[0];
        pRcvBuf += u4Offset;
        CAPWAP_GET_2BYTE (DeleteMac.u2MsgEleType, pRcvBuf);
        CAPWAP_GET_2BYTE (DeleteMac.u2MsgEleLen, pRcvBuf);
        CAPWAP_GET_1BYTE (DeleteMac.u1NumEntries, pRcvBuf);
        CAPWAP_GET_1BYTE (DeleteMac.u1Length, pRcvBuf);
        CAPWAP_GET_2BYTE (DeleteMac.u2MacAddr, pRcvBuf);

    }
    return OSIX_SUCCESS;
}

/***********************************************************************
*  Function Name   : CapwapValidateDupIPV4Addr                         *
*  Description     : Validates Duplicate IPv4 address                  *
*  Input(s)        : pRcvBuf, cwMsg, u2MsgIndex                        *
*  Output(s)       : None                                              *
*  Returns         : None                                              *
************************************************************************/
INT4
CapwapValidateDupIPV4Addr (UINT1 *pRcvBuf,
                           tCapwapControlPacket * cwMsg, UINT2 u2MsgIndex)
{
    UINT4               u4Offset = 0;
    UINT2               u2NumMsgElems;
    tWtpProfile         WtpProfile;
    tDupIPV4Addr        Dupv4;

    MEMSET (&WtpProfile, 0, sizeof (tWtpProfile));
    MEMSET (&Dupv4, 0, sizeof (tDupIPV4Addr));

    u2NumMsgElems = cwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;
    if (u2NumMsgElems > 0)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "The Message element Statistics Timer Received More than once \r\n");
        return OSIX_FAILURE;
    }
    else
    {
        u4Offset = cwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[0];
        pRcvBuf += u4Offset;
        CAPWAP_GET_2BYTE (Dupv4.u2MsgEleType, pRcvBuf);
        CAPWAP_GET_2BYTE (Dupv4.u2MsgEleLen, pRcvBuf);
        CAPWAP_GET_4BYTE (Dupv4.u4IpAddress, pRcvBuf);
        CAPWAP_GET_1BYTE (Dupv4.u1Status, pRcvBuf);
        CAPWAP_GET_1BYTE (Dupv4.u1LenMacAddr, pRcvBuf);
        CAPWAP_GET_2BYTE (Dupv4.u2MacAddr, pRcvBuf);
    }
    return OSIX_SUCCESS;
}

/***********************************************************************
*  Function Name   : CapwapValidateDupIPV6Addr                         *
*  Description     : Validates Duplicate IPv6 address                  *
*  Input(s)        : pRcvBuf, cwMsg, u2MsgIndex                        *
*  Output(s)       : None                                              *
*  Returns         : None                                              *
************************************************************************/
INT4
CapwapValidateDupIPV6Addr (UINT1 *pRcvBuf,
                           tCapwapControlPacket * cwMsg, UINT2 u2MsgIndex)
{
    UINT4               u4Offset = 0;
    UINT2               u2NumMsgElems;
    tWtpProfile         WtpProfile;

    MEMSET (&WtpProfile, 0, sizeof (tWtpProfile));

    u2NumMsgElems = cwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;
    if (u2NumMsgElems > 0)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "The Message element Statistics Timer Received More than once \r\n");
        return OSIX_FAILURE;
    }
    else
    {
        u4Offset = cwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[0];
        pRcvBuf += u4Offset;
    }
    return OSIX_SUCCESS;
}

/***********************************************************************
*  Function Name   : CapwapValidateAddStation                          *
*  Description     : Validates Add station                             *
*  Input(s)        : pRcvBuf, cwMsg, u2MsgIndex,pAddStation            *
*  Output(s)       : None                                              *
*  Returns         : None                                              *
************************************************************************/
INT4
CapwapValidateAddStation (UINT1 *pRcvBuf,
                          tCapwapControlPacket * cwMsg,
                          tAddstation * pAddStation, UINT2 u2MsgIndex)
{
    UINT4               u4Offset = 0;
    UINT2               u2NumMsgElems;
    tWtpProfile         WtpProfile;

    MEMSET (&WtpProfile, 0, sizeof (tWtpProfile));

    u2NumMsgElems = cwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;
    if (u2NumMsgElems > 0)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "The Message element Add Station Received More than once \r\n");
        return OSIX_FAILURE;
    }
    else
    {
        u4Offset = cwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[0];
        pRcvBuf += u4Offset;
        CAPWAP_GET_2BYTE (pAddStation->u2MsgEleType, pRcvBuf);
        CAPWAP_GET_2BYTE (pAddStation->u2MsgEleLen, pRcvBuf);
        CAPWAP_GET_1BYTE (pAddStation->u1RadioId, pRcvBuf);
        CAPWAP_GET_2BYTE (pAddStation->u2LenMacAddr, pRcvBuf);
        CAPWAP_GET_2BYTE (pAddStation->u2RadMacAddr, pRcvBuf);
        CAPWAP_GET_NBYTE (pAddStation->u2Name, pRcvBuf,
                          pAddStation->u2MsgEleLen -
                          CAPWAP_ADD_STATION_REDUCED_LEN);

    }
    return OSIX_SUCCESS;
}

/***********************************************************************
*  Function Name   : CapwapValidateDeleteStation                       *
*  Description     : Validates Delete station                          *
*  Input(s)        : pRcvBuf, cwMsg, u2MsgIndex,pDeleteStation         *
*  Output(s)       : None                                              *
*  Returns         : None                                              *
************************************************************************/
INT4
CapwapValidateDeleteStation (UINT1 *pRcvBuf,
                             tCapwapControlPacket * cwMsg,
                             tDeletestation * pDeleteStation, UINT2 u2MsgIndex)
{
    UINT4               u4Offset = 0;
    UINT2               u2NumMsgElems;
    tWtpProfile         WtpProfile;

    MEMSET (&WtpProfile, 0, sizeof (tWtpProfile));

    u2NumMsgElems = cwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;
    if (u2NumMsgElems > 0)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "The Message element Delete Station Received More than once \r\n");
        return OSIX_FAILURE;
    }
    else
    {
        u4Offset = cwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[0];
        pRcvBuf += u4Offset;
        CAPWAP_GET_2BYTE (pDeleteStation->u2MsgEleType, pRcvBuf);
        CAPWAP_GET_2BYTE (pDeleteStation->u2MsgEleLen, pRcvBuf);
        CAPWAP_GET_1BYTE (pDeleteStation->u1RadioId, pRcvBuf);
        CAPWAP_GET_2BYTE (pDeleteStation->u2LenMacAddr, pRcvBuf);
        CAPWAP_GET_2BYTE (pDeleteStation->u2RadMacAddr, pRcvBuf);

    }
    return OSIX_SUCCESS;
}

/***********************************************************************
*  Function Name   : CapwapValidateImageId                             *
*  Description     : Validates image ID                                *
*  Input(s)        : pRcvBuf, cwMsg, u2MsgIndex                        *
*  Output(s)       : None                                              *
*  Returns         : None                                              *
************************************************************************/
INT4
CapwapValidateImageId (UINT1 *pRcvBuf,
                       tCapwapControlPacket * cwMsg, UINT2 u2MsgIndex)
{
    UINT4               u4Offset = 0;
    UINT2               u2NumMsgElems;
    tWtpProfile         WtpProfile;

    MEMSET (&WtpProfile, 0, sizeof (tWtpProfile));

    u2NumMsgElems = cwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;
    if (u2NumMsgElems > 0)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "The Message element Image Identifier Received More than once \r\n");
        /*return OSIX_FAILURE; */
    }
    else
    {
        u4Offset = cwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[0];
        pRcvBuf += u4Offset;
        /* pImageId is NULL */
        /*
           CAPWAP_GET_2BYTE (pImageId->u2MsgEleType,pRcvBuf);
           CAPWAP_GET_2BYTE (pImageId->u2MsgEleLen, pRcvBuf);
           CAPWAP_GET_4BYTE (pImageId->u4VendorId, pRcvBuf);
           CAPWAP_GET_NBYTE (pImageId->data, pRcvBuf,1024);
         */
    }
    return OSIX_SUCCESS;
}

/***********************************************************************
*  Function Name   : CapwapValidateImageId                             *
*  Description     : Validates image ID                                *
*  Input(s)        : pRcvBuf, cwMsg, u2MsgIndex                        *
*  Output(s)       : None                                              *
*  Returns         : None                                              *
************************************************************************/
INT4
CapwapValidateDataTransferMode (UINT1 *pRcvBuf,
                                tCapwapControlPacket * cwMsg, UINT2 u2MsgIndex)
{
    UINT4               u4Offset = 0;
    UINT2               u2NumMsgElems;
    tWtpProfile         WtpProfile;
    tDataTransferMode   DataTransfer;

    MEMSET (&WtpProfile, 0, sizeof (tWtpProfile));
    MEMSET (&DataTransfer, 0, sizeof (tDataTransferMode));

    u2NumMsgElems = cwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;
    if (u2NumMsgElems > 0)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "The Message element Data Transfer Mode Received More than once \r\n");
        return OSIX_FAILURE;
    }
    else
    {
        u4Offset = cwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[0];
        pRcvBuf += u4Offset;
        CAPWAP_GET_2BYTE (DataTransfer.u2MsgEleType, pRcvBuf);
        CAPWAP_GET_2BYTE (DataTransfer.u2MsgEleLen, pRcvBuf);
        CAPWAP_GET_1BYTE (DataTransfer.DataMode, pRcvBuf);

    }
    return OSIX_SUCCESS;
}

/***********************************************************************
*  Function Name   : CapwapValidateVendSpecPld                         *
*  Description     : Validates Vendor spec Id                          *
*  Input(s)        : pRcvBuf, pcwMsg, u2MsgIndex,pVendor               *
*  Output(s)       : None                                              *
*  Returns         : None                                              *
************************************************************************/
INT4
CapwapValidateVendSpecPld (UINT1 *pRcvBuf,
                           tCapwapControlPacket * cwMsg,
                           tVendorSpecPayload * pVendor, UINT2 u2MsgIndex)
{
    UNUSED_PARAM (pRcvBuf);
    UNUSED_PARAM (pVendor);

    UINT4               u4Offset = 0;
    UINT2               u2NumMsgElems;
    tWtpProfile         WtpProfile;
    UINT2               u2Type = 0;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (&WtpProfile, 0, sizeof (tWtpProfile));
    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    u2NumMsgElems = cwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;
    if (u2NumMsgElems > 1)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "The Message element Vendor specific"
                    " payload Received More than once \r\n");
        return OSIX_FAILURE;
    }
    else
    {
        u4Offset = cwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[0];

        pRcvBuf += u4Offset;
        CAPWAP_GET_2BYTE (pVendor->u2MsgEleType, pRcvBuf);
        CAPWAP_GET_2BYTE (pVendor->u2MsgEleLen, pRcvBuf);
        CAPWAP_GET_2BYTE (u2Type, pRcvBuf);
        switch (u2Type)
        {
            case LOCAL_ROUTING_VENDOR_MSG:
            {

                pVendor->unVendorSpec.VendLocalRouting.u2MsgEleType = u2Type;
                CAPWAP_GET_2BYTE (pVendor->unVendorSpec.VendLocalRouting.
                                  u2MsgEleLen, pRcvBuf);
                CAPWAP_GET_4BYTE (pVendor->unVendorSpec.VendLocalRouting.
                                  vendorId, pRcvBuf);
                CAPWAP_GET_1BYTE (pVendor->unVendorSpec.VendLocalRouting.
                                  u1WtpLocalRouting, pRcvBuf);
                pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                pWssIfCapwapDB->CapwapIsGetAllDB.bWtpLocalRouting = OSIX_TRUE;
                pWssIfCapwapDB->CapwapGetDB.u1WtpLocalRouting =
                    pVendor->unVendorSpec.VendLocalRouting.u1WtpLocalRouting;
                if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_SET_DB, pWssIfCapwapDB)
                    != OSIX_SUCCESS)
                {
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    return OSIX_FAILURE;
                }

                break;
            }

            default:
                break;

        }

    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}

/***********************************************************************
*  Function Name   : CapwapValidateConfigUpdateVendSpecPld             *
*  Description     : Validates Vendor spec Id                          *
*  Input(s)        : pRcvBuf, pcwMsg, u2MsgIndex,pVendor               *
*  Output(s)       : None                                              *
*  Returns         : None                                              *
************************************************************************/
INT4
CapwapValidateConfigUpdateVendSpecPld (UINT1 *pRcvBuf,
                                       tCapwapControlPacket * cwMsg,
                                       tVendorSpecPayload * pVendor,
                                       UINT2 u2MsgIndex)
{
    UINT4               u4Offset = 0;
    UINT2               u2NumMsgElems;
    tWtpProfile         WtpProfile;
    UINT2               u2Type = 0;
    INT2                i2Index = 0;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tRadioIfMsgStruct   pWssMsgStruct;
    UINT2               u2CapwapBaseWtpProfileId = 0;
    INT4                i4FsCapwapEncryptChannelRowStatus = 0;
    tWssWlanMsgStruct   WssWlanMsgStruct;
    tWTPNVRAM_DATA      WTPNVRAM_DATA;
    tNVRAM_DATA         NvRamData;
#ifdef RFMGMT_WANTED
    UINT1               u1WlanId = 0;
    tRfMgmtMsgStruct    RfMgmtMsgStruct;

    MEMSET (&RfMgmtMsgStruct, 0, sizeof (tRfMgmtMsgStruct));
#endif
#ifdef ROGUEAP_WANTED
    tRadioIfGetDB       RadioIfGetDB;
    UINT2               i = 0;
    UINT1               u1Index = 0;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
#endif

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (&WtpProfile, 0, sizeof (tWtpProfile));
    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&pWssMsgStruct, 0, sizeof (tRadioIfMsgStruct));
    MEMSET (&WssWlanMsgStruct, 0, sizeof (tWssWlanMsgStruct));
    MEMSET (&WTPNVRAM_DATA, 0, sizeof (WTPNVRAM_DATA));
    MEMSET (&NvRamData, 0, sizeof (NvRamData));
    u2NumMsgElems = cwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;
    if (u2NumMsgElems > 1)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "The Message element Vendor specific"
                    " payload Received More than once \r\n");
        return OSIX_FAILURE;
    }
    else
    {
        u4Offset = cwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[0];
        pRcvBuf += u4Offset;
        CAPWAP_GET_2BYTE (pVendor->u2MsgEleType, pRcvBuf);
        CAPWAP_GET_2BYTE (pVendor->u2MsgEleLen, pRcvBuf);
        CAPWAP_GET_2BYTE (u2Type, pRcvBuf);
        switch (u2Type)
        {
            case DISCOVERY_TYPE_VENDOR_MSG:
            {
                pVendor->unVendorSpec.VendDiscType.u2MsgEleType = u2Type;
                CAPWAP_GET_2BYTE (pVendor->unVendorSpec.VendDiscType.
                                  u2MsgEleLen, pRcvBuf);
                CAPWAP_GET_4BYTE (pVendor->unVendorSpec.VendDiscType.vendorId,
                                  pRcvBuf);
                CAPWAP_GET_1BYTE (pVendor->unVendorSpec.VendDiscType.u1Val,
                                  pRcvBuf);
                CAPWAP_GET_4BYTE (pVendor->unVendorSpec.VendDiscType.
                                  u4WlcIpAddress, pRcvBuf);

                pWssIfCapwapDB->CapwapIsGetAllDB.bWtpDiscType = OSIX_TRUE;
                pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
                    cwMsg->u2IntProfileId;

                pWssIfCapwapDB->CapwapGetDB.u1WtpDiscType =
                    pVendor->unVendorSpec.VendDiscType.u1Val;
                if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_SET_DB, pWssIfCapwapDB)
                    != OSIX_SUCCESS)
                {
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    return OSIX_FAILURE;
                }
                WtpNvRamRead (&WTPNVRAM_DATA);
                WTPNVRAM_DATA.u1DiscoveryType =
                    pVendor->unVendorSpec.VendDiscType.u1Val;
                if (WTPNVRAM_DATA.u1DiscoveryType == WSS_STATIC_CONF_DISC_TYPE)
                {
                    WTPNVRAM_DATA.u4WlcIpAddr =
                        pVendor->unVendorSpec.VendDiscType.u4WlcIpAddress;
                }
                WtpNvRamWrite (&WTPNVRAM_DATA);
                NvRamRead (&NvRamData);
                if (pVendor->unVendorSpec.VendDiscType.u1Val ==
                    WSS_DHCP_DISC_TYPE
                    || pVendor->unVendorSpec.VendDiscType.u1Val ==
                    WSS_DNS_DISC_TYPE)
                {
                    NvRamData.u4CfgMode = 2;
#ifdef CLI_WANTED
                    MGMT_UNLOCK ();
                    CliTakeAppContext ();
                    CliExecuteAppCmd ("end\n");
                    CliExecuteAppCmd ("erase startup-config\n");
                    CliGiveAppContext ();
                    MGMT_LOCK ();
#endif
                }
                else
                {
                    NvRamData.u4CfgMode = 1;
                }
                NvRamWrite (&NvRamData);
                break;
            }
            case SSID_RATE_VENDOR_MSG:
            {
                pVendor->unVendorSpec.VendSsidRate.u2MsgEleType = u2Type;
                CAPWAP_GET_2BYTE (pVendor->unVendorSpec.VendSsidRate.
                                  u2MsgEleLen, pRcvBuf);
                CAPWAP_GET_4BYTE (pVendor->unVendorSpec.VendSsidRate.vendorId,
                                  pRcvBuf);

                CAPWAP_GET_1BYTE (pVendor->unVendorSpec.VendSsidRate.
                                  u1QosRateLimit, pRcvBuf);
                CAPWAP_GET_4BYTE (pVendor->unVendorSpec.VendSsidRate.
                                  u4QosUpStreamCIR, pRcvBuf);
                CAPWAP_GET_4BYTE (pVendor->unVendorSpec.VendSsidRate.
                                  u4QosUpStreamCBS, pRcvBuf);
                CAPWAP_GET_4BYTE (pVendor->unVendorSpec.VendSsidRate.
                                  u4QosUpStreamEIR, pRcvBuf);
                CAPWAP_GET_4BYTE (pVendor->unVendorSpec.VendSsidRate.
                                  u4QosUpStreamEBS, pRcvBuf);
                CAPWAP_GET_4BYTE (pVendor->unVendorSpec.VendSsidRate.
                                  u4QosDownStreamCIR, pRcvBuf);
                CAPWAP_GET_4BYTE (pVendor->unVendorSpec.VendSsidRate.
                                  u4QosDownStreamCBS, pRcvBuf);
                CAPWAP_GET_4BYTE (pVendor->unVendorSpec.VendSsidRate.
                                  u4QosDownStreamEIR, pRcvBuf);
                CAPWAP_GET_4BYTE (pVendor->unVendorSpec.VendSsidRate.
                                  u4QosDownStreamEBS, pRcvBuf);
                break;
            }
            case UDP_SERVER_PORT_VENDOR_MSG:
            {
                pVendor->unVendorSpec.VendUdpPort.u2MsgEleType = u2Type;
                CAPWAP_GET_2BYTE (pVendor->unVendorSpec.VendUdpPort.u2MsgEleLen,
                                  pRcvBuf);
                CAPWAP_GET_4BYTE (pVendor->unVendorSpec.VendUdpPort.vendorId,
                                  pRcvBuf);
                CAPWAP_GET_4BYTE (pVendor->unVendorSpec.VendUdpPort.u4Val,
                                  pRcvBuf);

                pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                pWssIfCapwapDB->CapwapIsGetAllDB.bCtrlUdpPort = OSIX_TRUE;
                pWssIfCapwapDB->u4CtrludpPort =
                    pVendor->unVendorSpec.VendUdpPort.u4Val;
                if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_SET_DB, pWssIfCapwapDB)
                    != OSIX_SUCCESS)
                {
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    return OSIX_FAILURE;
                }
                break;
            }
            case CONTROL_POLICY_DTLS_VENDOR_MSG:
            {
                pVendor->unVendorSpec.VendorContDTLS.u2MsgEleType = u2Type;
                CAPWAP_GET_2BYTE (pVendor->unVendorSpec.VendorContDTLS.
                                  u2MsgEleLen, pRcvBuf);
                CAPWAP_GET_4BYTE (pVendor->unVendorSpec.VendorContDTLS.vendorId,
                                  pRcvBuf);
                CAPWAP_GET_1BYTE (pVendor->unVendorSpec.VendorContDTLS.u1Val,
                                  pRcvBuf);

                pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                pWssIfCapwapDB->CapwapIsGetAllDB.bWtpCtrlDTLSStatus = OSIX_TRUE;
                pWssIfCapwapDB->CapwapGetDB.u1WtpCtrlDTLSStatus =
                    pVendor->unVendorSpec.VendorContDTLS.u1Val;
                if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_SET_DB, pWssIfCapwapDB)
                    == OSIX_FAILURE)
                {
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "\n Failed to Set the DTLS info in DB");
                    return OSIX_FAILURE;
                }

                CapwapGetProfileIdFromInternalProfId (0,
                                                      &u2CapwapBaseWtpProfileId);
                if (nmhGetFsCapwapEncryptChannelRowStatus
                    ((UINT4) u2CapwapBaseWtpProfileId,
                     CLI_FSCAPWAP_ENCRYPTION_CONTROL,
                     &i4FsCapwapEncryptChannelRowStatus) != SNMP_SUCCESS)
                {
                    if (nmhSetFsCapwapEncryptChannelRowStatus
                        ((UINT4) u2CapwapBaseWtpProfileId,
                         CLI_FSCAPWAP_ENCRYPTION_CONTROL,
                         CREATE_AND_GO) != SNMP_SUCCESS)
                    {
                        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                    "nmhSetFsCapwapEncryptChannel"
                                    "RowStatus row creation failed.\r\n");
                        return OSIX_FAILURE;
                    }
                }

                if (nmhSetFsCapwapEncryptChannelStatus
                    ((UINT4) u2CapwapBaseWtpProfileId,
                     CLI_FSCAPWAP_ENCRYPTION_CONTROL,
                     pVendor->unVendorSpec.VendorContDTLS.u1Val) !=
                    SNMP_SUCCESS)
                {
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "nmhSetFsCapwapEncryptChannelStatus set failed..\r\n");
                    return OSIX_FAILURE;
                }
                break;
            }
            case DOMAIN_NAME_VENDOR_MSG:
            {
                pVendor->unVendorSpec.VendDns.u2MsgEleType = u2Type;
                CAPWAP_GET_2BYTE (pVendor->unVendorSpec.VendDns.u2MsgEleLen,
                                  pRcvBuf);
                CAPWAP_GET_4BYTE (pVendor->unVendorSpec.VendDns.vendorId,
                                  pRcvBuf);
                CAPWAP_GET_NBYTE (pVendor->unVendorSpec.VendDns.au1ServerIp,
                                  pRcvBuf, 4);
                CAPWAP_GET_NBYTE (pVendor->unVendorSpec.VendDns.au1DomainName,
                                  pRcvBuf, 32);
                pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                pWssIfCapwapDB->CapwapIsGetAllDB.bCapwapDnsServerIp = OSIX_TRUE;
                pWssIfCapwapDB->CapwapIsGetAllDB.bCapwapDnsDomainName =
                    OSIX_TRUE;
                MEMCPY (pWssIfCapwapDB->DnsProfileEntry.au1FsCapwapDnsServerIp,
                        pVendor->unVendorSpec.VendDns.au1ServerIp,
                        sizeof ((char *)(pVendor->unVendorSpec.VendDns.
                                         au1ServerIp)));

                MEMCPY (pWssIfCapwapDB->DnsProfileEntry.
                        au1FsCapwapDnsDomainName,
                        pVendor->unVendorSpec.VendDns.au1DomainName,
                        sizeof ((char *)(pVendor->unVendorSpec.VendDns.
                                         au1DomainName)));

                if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_SET_DB, pWssIfCapwapDB)
                    != OSIX_SUCCESS)
                {
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    return OSIX_FAILURE;
                }
                break;
            }
            case NATIVE_VLAN_VENDOR_MSG:
            {
                pVendor->unVendorSpec.VendVlan.u2MsgEleType = u2Type;
                CAPWAP_GET_2BYTE (pVendor->unVendorSpec.VendVlan.u2MsgEleLen,
                                  pRcvBuf);
                CAPWAP_GET_4BYTE (pVendor->unVendorSpec.VendVlan.vendorId,
                                  pRcvBuf);
                CAPWAP_GET_2BYTE (pVendor->unVendorSpec.VendVlan.u2VlanId,
                                  pRcvBuf);
                pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                pWssIfCapwapDB->CapwapIsGetAllDB.bNativeVlan = OSIX_TRUE;
                pWssIfCapwapDB->CapwapGetDB.u4NativeVlan =
                    pVendor->unVendorSpec.VendVlan.u2VlanId;
                if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_SET_DB, pWssIfCapwapDB)
                    != OSIX_SUCCESS)
                {
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    return OSIX_FAILURE;
                }
                break;
            }
            case DOT11N_VENDOR_MSG:
            {
                pVendor->unVendorSpec.Dot11nVendor.u2MsgEleType = u2Type;

                CAPWAP_GET_2BYTE (pVendor->unVendorSpec.Dot11nVendor.
                                  u2MsgEleLen, pRcvBuf);
                CAPWAP_GET_4BYTE (pVendor->unVendorSpec.Dot11nVendor.u4VendorId,
                                  pRcvBuf);
                CAPWAP_GET_1BYTE (pVendor->unVendorSpec.Dot11nVendor.u1RadioId,
                                  pRcvBuf);
                CAPWAP_GET_1BYTE (pVendor->unVendorSpec.Dot11nVendor.u1HtFlag,
                                  pRcvBuf);
                CAPWAP_GET_1BYTE (pVendor->unVendorSpec.Dot11nVendor.
                                  u1MaxSuppMCS, pRcvBuf);
                CAPWAP_GET_1BYTE (pVendor->unVendorSpec.Dot11nVendor.
                                  u1MaxManMCS, pRcvBuf);
                CAPWAP_GET_1BYTE (pVendor->unVendorSpec.Dot11nVendor.
                                  u1TxAntenna, pRcvBuf);
                CAPWAP_GET_1BYTE (pVendor->unVendorSpec.Dot11nVendor.
                                  u1RxAntenna, pRcvBuf);
                pWssMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                    RadioIfDot11nParam.u2MessageType =
                    pVendor->unVendorSpec.Dot11nVendor.u2MsgEleType;
                pWssMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                    RadioIfDot11nParam.u2MessageLength =
                    pVendor->unVendorSpec.Dot11nVendor.u2MsgEleLen;
                pWssMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                    RadioIfDot11nParam.u4VendorId =
                    pVendor->unVendorSpec.Dot11nVendor.u4VendorId;
                pWssMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                    RadioIfDot11nParam.u1RadioId =
                    pVendor->unVendorSpec.Dot11nVendor.u1RadioId;
                pWssMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                    RadioIfDot11nParam.u1HtFlag =
                    pVendor->unVendorSpec.Dot11nVendor.u1HtFlag;
                pWssMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                    RadioIfDot11nParam.u1MaxSuppMCS =
                    pVendor->unVendorSpec.Dot11nVendor.u1MaxSuppMCS;
                pWssMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                    RadioIfDot11nParam.u1MaxManMCS =
                    pVendor->unVendorSpec.Dot11nVendor.u1MaxManMCS;
                pWssMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                    RadioIfDot11nParam.u1TxAntenna =
                    pVendor->unVendorSpec.Dot11nVendor.u1TxAntenna;
                pWssMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                    RadioIfDot11nParam.u1RxAntenna =
                    pVendor->unVendorSpec.Dot11nVendor.u1RxAntenna;
                pWssMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                    RadioIfDot11nParam.isPresent = OSIX_TRUE;

                if (u2MsgIndex ==
                    VENDOR_SPECIFIC_PAYLOAD_CONFIG_UPDATE_REQ_INDEX)
                {
                    if (WssIfProcessRadioIfMsg
                        (WSS_RADIOIF_CONFIG_UPDATE_REQ_VALIDATE,
                         &pWssMsgStruct) != OSIX_SUCCESS)
                    {
                        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                        return OSIX_FAILURE;
                    }
                    if (WssIfProcessRadioIfMsg (WSS_RADIOIF_CONFIG_UPDATE_REQ,
                                                &pWssMsgStruct) != OSIX_SUCCESS)
                    {
                        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                        return OSIX_FAILURE;
                    }
                }
                break;
            }
            case MGMT_SSID_VENDOR_MSG:
            {
                pVendor->unVendorSpec.VendSsid.u2MsgEleType = u2Type;
                CAPWAP_GET_2BYTE (pVendor->unVendorSpec.VendSsid.u2MsgEleLen,
                                  pRcvBuf);
                CAPWAP_GET_4BYTE (pVendor->unVendorSpec.VendSsid.vendorId,
                                  pRcvBuf);
                CAPWAP_GET_NBYTE (pVendor->unVendorSpec.VendSsid.
                                  au1ManagmentSSID, pRcvBuf,
                                  WSSWLAN_SSID_NAME_LEN);

                MEMCPY (WssWlanMsgStruct.unWssWlanMsg.au1ManagmentSSID,
                        pVendor->unVendorSpec.VendSsid.au1ManagmentSSID,
                        WSSWLAN_SSID_NAME_LEN);
                if (WssWlanProcessWssIfMsg (WSS_WLAN_SET_MANAGMENT_SSID,
                                            &WssWlanMsgStruct) != OSIX_SUCCESS)
                {
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    return OSIX_FAILURE;
                }
            }
                break;
            case LOCAL_ROUTING_VENDOR_MSG:
            {

                pVendor->unVendorSpec.VendLocalRouting.u2MsgEleType = u2Type;
                CAPWAP_GET_2BYTE (pVendor->unVendorSpec.VendLocalRouting.
                                  u2MsgEleLen, pRcvBuf);
                CAPWAP_GET_4BYTE (pVendor->unVendorSpec.VendLocalRouting.
                                  vendorId, pRcvBuf);
                CAPWAP_GET_1BYTE (pVendor->unVendorSpec.VendLocalRouting.
                                  u1WtpLocalRouting, pRcvBuf);
                pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                pWssIfCapwapDB->CapwapIsGetAllDB.bWtpLocalRouting = OSIX_TRUE;
                pWssIfCapwapDB->CapwapGetDB.u1WtpLocalRouting =
                    pVendor->unVendorSpec.VendLocalRouting.u1WtpLocalRouting;
                if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_SET_DB, pWssIfCapwapDB)
                    != OSIX_SUCCESS)
                {
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    return OSIX_FAILURE;
                }
#ifdef KERNEL_CAPWAP_WANTED
                if (CapwapUpdateWtpKernelDB () == OSIX_FAILURE)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to update"
                                "kernel DBDB\r\n");
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    return OSIX_FAILURE;
                }
#endif
#ifdef NPAPI_WANTED
                if (RadioIfUpdateLocalRoutingStatus
                    (pWssIfCapwapDB->CapwapGetDB.u1WtpLocalRouting) !=
                    OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "RadioIfUpdateLocalRoutingStatus failed \n");
                }
#endif
                /* Local Routing Changes */

                pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = 0;
                pWssIfCapwapDB->CapwapIsGetAllDB.bWtpLocalRouting = OSIX_TRUE;
                if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
                    != OSIX_SUCCESS)
                {
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    return OSIX_FAILURE;
                }

                break;
            }
#ifdef RFMGMT_WANTED
            case NEIGH_CONFIG_VENDOR_MSG:
            {
                RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtConfigUpdateReq.
                    NeighApTableConfig.u2MsgEleType = u2Type;

                CAPWAP_GET_2BYTE (RfMgmtMsgStruct.unRfMgmtMsg.
                                  RfMgmtConfigUpdateReq.NeighApTableConfig.
                                  u2MsgEleLen, pRcvBuf);
                CAPWAP_GET_4BYTE (RfMgmtMsgStruct.unRfMgmtMsg.
                                  RfMgmtConfigUpdateReq.NeighApTableConfig.
                                  u4VendorId, pRcvBuf);
                CAPWAP_GET_1BYTE (RfMgmtMsgStruct.unRfMgmtMsg.
                                  RfMgmtConfigUpdateReq.NeighApTableConfig.
                                  u1RadioId, pRcvBuf);
                CAPWAP_GET_1BYTE (RfMgmtMsgStruct.unRfMgmtMsg.
                                  RfMgmtConfigUpdateReq.NeighApTableConfig.
                                  u1AutoScanStatus, pRcvBuf);
                CAPWAP_GET_2BYTE (RfMgmtMsgStruct.unRfMgmtMsg.
                                  RfMgmtConfigUpdateReq.NeighApTableConfig.
                                  u2NeighborMsgPeriod, pRcvBuf);
                CAPWAP_GET_2BYTE (RfMgmtMsgStruct.unRfMgmtMsg.
                                  RfMgmtConfigUpdateReq.NeighApTableConfig.
                                  u2ChannelScanDuration, pRcvBuf);
                CAPWAP_GET_2BYTE (RfMgmtMsgStruct.unRfMgmtMsg.
                                  RfMgmtConfigUpdateReq.NeighApTableConfig.
                                  u2NeighborAgingPeriod, pRcvBuf);
                CAPWAP_GET_2BYTE (RfMgmtMsgStruct.unRfMgmtMsg.
                                  RfMgmtConfigUpdateReq.NeighApTableConfig.
                                  i2RssiThreshold, pRcvBuf);

                RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtConfigUpdateReq.
                    u2WtpInternalId = cwMsg->u2IntProfileId;

                RfMgmtMsgStruct.u1Opcode = NEIGH_CONFIG_VENDOR_MSG;

                if (WssIfProcessRfMgmtMsg (RFMGMT_CONFIG_UPDATE_REQ,
                                           &RfMgmtMsgStruct) != OSIX_SUCCESS)
                {
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Configuration update"
                                "Request failure:CLIENT_CONFIG Vendor Type\r\n");
                    return OSIX_FAILURE;
                }
            }
                break;

            case CLIENT_CONFIG_VENDOR_MSG:
            {
                RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtConfigUpdateReq.
                    ClientTableConfig.u2MsgEleType = u2Type;
                CAPWAP_GET_2BYTE (RfMgmtMsgStruct.unRfMgmtMsg.
                                  RfMgmtConfigUpdateReq.ClientTableConfig.
                                  u2MsgEleLen, pRcvBuf);
                CAPWAP_GET_4BYTE (RfMgmtMsgStruct.unRfMgmtMsg.
                                  RfMgmtConfigUpdateReq.ClientTableConfig.
                                  u4VendorId, pRcvBuf);
                CAPWAP_GET_1BYTE (RfMgmtMsgStruct.unRfMgmtMsg.
                                  RfMgmtConfigUpdateReq.ClientTableConfig.
                                  u1RadioId, pRcvBuf);
                CAPWAP_GET_1BYTE (RfMgmtMsgStruct.unRfMgmtMsg.
                                  RfMgmtConfigUpdateReq.ClientTableConfig.
                                  u1SNRScanStatus, pRcvBuf);
                CAPWAP_GET_2BYTE (RfMgmtMsgStruct.unRfMgmtMsg.
                                  RfMgmtConfigUpdateReq.ClientTableConfig.
                                  u2SNRScanPeriod, pRcvBuf);
                CAPWAP_GET_2BYTE (RfMgmtMsgStruct.unRfMgmtMsg.
                                  RfMgmtConfigUpdateReq.ClientTableConfig.
                                  i2SNRThreshold, pRcvBuf);
                CAPWAP_GET_1BYTE (RfMgmtMsgStruct.unRfMgmtMsg.
                                  RfMgmtConfigUpdateReq.ClientTableConfig.
                                  u1WlanId, pRcvBuf);
                u1WlanId = RfMgmtMsgStruct.unRfMgmtMsg.
                    RfMgmtConfigUpdateReq.ClientTableConfig.u1WlanId;
                CAPWAP_GET_1BYTE (RfMgmtMsgStruct.unRfMgmtMsg.
                                  RfMgmtConfigUpdateReq.ClientTableConfig.
                                  u1BssidScanStatus[u1WlanId - 1], pRcvBuf);

                RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtConfigUpdateReq.
                    u2WtpInternalId = cwMsg->u2IntProfileId;
                RfMgmtMsgStruct.u1Opcode = CLIENT_CONFIG_VENDOR_MSG;
                if (WssIfProcessRfMgmtMsg (RFMGMT_CONFIG_UPDATE_REQ,
                                           &RfMgmtMsgStruct) != OSIX_SUCCESS)
                {
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Configuration update"
                                "Request failure:CLIENT_CONFIG Vendor Type\r\n");
                    return OSIX_FAILURE;
                }
            }
                break;
            case CH_SWITCH_STATUS_VENDOR_MSG:
            {
                RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtConfigUpdateReq.
                    ChSwitchStatusTable.u2MsgEleType = u2Type;
                CAPWAP_GET_2BYTE (RfMgmtMsgStruct.unRfMgmtMsg.
                                  RfMgmtConfigUpdateReq.ChSwitchStatusTable.
                                  u2MsgEleLen, pRcvBuf);
                CAPWAP_GET_4BYTE (RfMgmtMsgStruct.unRfMgmtMsg.
                                  RfMgmtConfigUpdateReq.ChSwitchStatusTable.
                                  u4VendorId, pRcvBuf);
                CAPWAP_GET_1BYTE (RfMgmtMsgStruct.unRfMgmtMsg.
                                  RfMgmtConfigUpdateReq.ChSwitchStatusTable.
                                  u1RadioId, pRcvBuf);
                CAPWAP_GET_1BYTE (RfMgmtMsgStruct.unRfMgmtMsg.
                                  RfMgmtConfigUpdateReq.ChSwitchStatusTable.
                                  u1ChSwitchStatus, pRcvBuf);

                RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtConfigUpdateReq.
                    u2WtpInternalId = cwMsg->u2IntProfileId;

                RfMgmtMsgStruct.u1Opcode = CH_SWITCH_STATUS_VENDOR_MSG;

                if (WssIfProcessRfMgmtMsg (RFMGMT_CONFIG_UPDATE_REQ,
                                           &RfMgmtMsgStruct) != OSIX_SUCCESS)
                {
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Configuration update"
                                "Request failure:CH_SWITCH_STATUS Vendor Type\r\n");
                    return OSIX_FAILURE;
                }
            }
                break;
            case VENDOR_SPECT_MGMT_TPC_MSG:
            {
                RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtConfigUpdateReq.
                    TpcSpectMgmtTable.u2MsgEleType = u2Type;
                CAPWAP_GET_2BYTE (RfMgmtMsgStruct.unRfMgmtMsg.
                                  RfMgmtConfigUpdateReq.TpcSpectMgmtTable.
                                  u2MsgEleLen, pRcvBuf);
                CAPWAP_GET_4BYTE (RfMgmtMsgStruct.unRfMgmtMsg.
                                  RfMgmtConfigUpdateReq.TpcSpectMgmtTable.
                                  u4VendorId, pRcvBuf);
                CAPWAP_GET_1BYTE (RfMgmtMsgStruct.unRfMgmtMsg.
                                  RfMgmtConfigUpdateReq.TpcSpectMgmtTable.
                                  u1RadioId, pRcvBuf);
                CAPWAP_GET_2BYTE (RfMgmtMsgStruct.unRfMgmtMsg.
                                  RfMgmtConfigUpdateReq.TpcSpectMgmtTable.
                                  u2TpcRequestInterval, pRcvBuf);
                CAPWAP_GET_1BYTE (RfMgmtMsgStruct.unRfMgmtMsg.
                                  RfMgmtConfigUpdateReq.TpcSpectMgmtTable.
                                  u111hTpcStatus, pRcvBuf);

                RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtConfigUpdateReq.
                    u2WtpInternalId = cwMsg->u2IntProfileId;

                RfMgmtMsgStruct.u1Opcode = VENDOR_SPECT_MGMT_TPC_MSG;

                if (WssIfProcessRfMgmtMsg (RFMGMT_CONFIG_UPDATE_REQ,
                                           &RfMgmtMsgStruct) != OSIX_SUCCESS)
                {
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Configuration update"
                                "Request failure:CH_SWITCH_STATUS Vendor Type\r\n");
                    return OSIX_FAILURE;
                }
            }
                break;
#ifdef ROGUEAP_WANTED
            case ROGUE_CONFIG_VENDOR_MSG:
            {
                RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtConfigUpdateReq.
                    RogueMgmt.u2MsgEleType = u2Type;
                CAPWAP_GET_2BYTE (RfMgmtMsgStruct.unRfMgmtMsg.
                                  RfMgmtConfigUpdateReq.RogueMgmt.
                                  u2MsgEleLen, pRcvBuf);
                CAPWAP_GET_4BYTE (RfMgmtMsgStruct.unRfMgmtMsg.
                                  RfMgmtConfigUpdateReq.RogueMgmt.
                                  u4VendorId, pRcvBuf);
                CAPWAP_GET_NBYTE (RfMgmtMsgStruct.unRfMgmtMsg.
                                  RfMgmtConfigUpdateReq.RogueMgmt.
                                  u1RfGroupName, pRcvBuf,
                                  CAPWAP_RF_GROUP_NAME_SIZE);
                CAPWAP_GET_1BYTE (RfMgmtMsgStruct.unRfMgmtMsg.
                                  RfMgmtConfigUpdateReq.RogueMgmt.u1RfDetection,
                                  pRcvBuf);

                RfMgmtMsgStruct.u1Opcode = ROGUE_CONFIG_VENDOR_MSG;

                if (WssIfProcessRfMgmtMsg (RFMGMT_CONFIG_UPDATE_REQ,
                                           &RfMgmtMsgStruct) != OSIX_SUCCESS)
                {
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Configuration update"
                                "Request failure:ROGUE_CONFIG Vendor Type\r\n");
                    return OSIX_FAILURE;
                }

                /* Get the radio info */
                for (i = RADIOIF_START_RADIOID;
                     i <= SYS_DEF_MAX_RADIO_INTERFACES; i++)
                {
                    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
                        SYS_DEF_MAX_ENET_INTERFACES + i;
                    /* Check for the available Wlans and pass 
                     *           each Radio Id + Wlan ID */
                    for (u1Index = WSSWLAN_START_WLANID_PER_RADIO;
                         u1Index <= WSSWLAN_END_WLANID_PER_RADIO; u1Index++)
                    {
                        RadioIfGetDB.RadioIfGetAllDB.u1WlanId = u1Index;
                        if (WssIfProcessRadioIfDBMsg
                            (WSS_GET_WLAN_BSS_IFINDEX_DB,
                             &RadioIfGetDB) != OSIX_SUCCESS)
                        {
                            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                        "CapwapClearWtpConfig:Get WlanIfIndex failed\r\n");
                            continue;
                        }
                        WssWlanMsgStruct.unWssWlanMsg.WssWlanConfigReq.
                            unWlanConfReq.WssWlanAddReq.u1RadioId = i;

                        WssWlanMsgStruct.unWssWlanMsg.WssWlanConfigReq.
                            unWlanConfReq.WssWlanAddReq.u1WlanId = u1Index;

                        if (WssWlanProcessWssIfMsg (WSS_WLAN_SET_RF_GROUP_ID,
                                                    &WssWlanMsgStruct) !=
                            OSIX_SUCCESS)
                        {
                            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                            return OSIX_FAILURE;
                        }
                    }
                }
            }
                break;
#endif
            case VENDOR_SPECTRUM_MGMT_DFS_MSG:
            {
                RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtConfigUpdateReq.
                    DfsParamsTable.u2MsgEleType = u2Type;
                CAPWAP_GET_2BYTE (RfMgmtMsgStruct.unRfMgmtMsg.
                                  RfMgmtConfigUpdateReq.DfsParamsTable.
                                  u2MsgEleLen, pRcvBuf);
                CAPWAP_GET_4BYTE (RfMgmtMsgStruct.unRfMgmtMsg.
                                  RfMgmtConfigUpdateReq.DfsParamsTable.
                                  u4VendorId, pRcvBuf);
                CAPWAP_GET_1BYTE (RfMgmtMsgStruct.unRfMgmtMsg.
                                  RfMgmtConfigUpdateReq.DfsParamsTable.
                                  u1RadioId, pRcvBuf);
                CAPWAP_GET_2BYTE (RfMgmtMsgStruct.unRfMgmtMsg.
                                  RfMgmtConfigUpdateReq.DfsParamsTable.
                                  u2DfsQuietInterval, pRcvBuf);
                CAPWAP_GET_2BYTE (RfMgmtMsgStruct.unRfMgmtMsg.
                                  RfMgmtConfigUpdateReq.DfsParamsTable.
                                  u2DfsQuietPeriod, pRcvBuf);
                CAPWAP_GET_2BYTE (RfMgmtMsgStruct.unRfMgmtMsg.
                                  RfMgmtConfigUpdateReq.DfsParamsTable.
                                  u2DfsMeasurementInterval, pRcvBuf);
                CAPWAP_GET_2BYTE (RfMgmtMsgStruct.unRfMgmtMsg.
                                  RfMgmtConfigUpdateReq.DfsParamsTable.
                                  u2DfsChannelSwitchStatus, pRcvBuf);
                CAPWAP_GET_1BYTE (RfMgmtMsgStruct.unRfMgmtMsg.
                                  RfMgmtConfigUpdateReq.DfsParamsTable.
                                  u111hDfsStatus, pRcvBuf);
                RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtConfigUpdateReq.
                    u2WtpInternalId = cwMsg->u2IntProfileId;
                RfMgmtMsgStruct.u1Opcode = VENDOR_SPECTRUM_MGMT_DFS_MSG;
                if (WssIfProcessRfMgmtMsg (RFMGMT_CONFIG_UPDATE_REQ,
                                           &RfMgmtMsgStruct) != OSIX_SUCCESS)
                {
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Configuration update"
                                "Request failure:CH_SWITCH_STATUS Vendor Type\r\n");
                    return OSIX_FAILURE;
                }
            }
                break;
#endif

            case WEBAUTH_COMPLETE_MSG:
            {
                pVendor->unVendorSpec.VendorWebAuthStatus.u2MsgEleType = u2Type;
                CAPWAP_GET_2BYTE (pVendor->unVendorSpec.
                                  VendorWebAuthStatus.u2MsgEleLen, pRcvBuf);
                CAPWAP_GET_4BYTE (pVendor->unVendorSpec.
                                  VendorWebAuthStatus.vendorId, pRcvBuf);
                CAPWAP_GET_NBYTE (pVendor->unVendorSpec.
                                  VendorWebAuthStatus.StationMac, pRcvBuf,
                                  sizeof (tMacAddr));
                CAPWAP_GET_1BYTE (pVendor->unVendorSpec.
                                  VendorWebAuthStatus.u1WebAuthCompleteStatus,
                                  pRcvBuf);
                pVendor->unVendorSpec.VendorWebAuthStatus.isOptional =
                    OSIX_TRUE;
                if (CapwapSendWebAuthStatusToSta (pVendor) != OSIX_SUCCESS)
                {
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    return OSIX_FAILURE;
                }
            }
                break;
            case DHCP_POOL:
                pVendor->unVendorSpec.DhcpPoolConfigUpdateReq.u2MsgEleType =
                    u2Type;
                CAPWAP_GET_2BYTE (pVendor->unVendorSpec.DhcpPoolConfigUpdateReq.
                                  u2MsgEleLen, pRcvBuf);
                CAPWAP_GET_4BYTE (pVendor->unVendorSpec.DhcpPoolConfigUpdateReq.
                                  u4VendorId, pRcvBuf);
                CAPWAP_GET_1BYTE (pVendor->unVendorSpec.DhcpPoolConfigUpdateReq.
                                  u1ServerStatus, pRcvBuf);
                CAPWAP_GET_2BYTE (pVendor->unVendorSpec.DhcpPoolConfigUpdateReq.
                                  u2NoofPools, pRcvBuf);
                for (i2Index = 0;
                     i2Index <
                     pVendor->unVendorSpec.DhcpPoolConfigUpdateReq.u2NoofPools;
                     i2Index++)
                {
                    CAPWAP_GET_4BYTE (pVendor->unVendorSpec.
                                      DhcpPoolConfigUpdateReq.
                                      u4DhcpPoolId[i2Index], pRcvBuf);
                    CAPWAP_GET_2BYTE (pVendor->unVendorSpec.
                                      DhcpPoolConfigUpdateReq.
                                      u2PoolStatus[i2Index], pRcvBuf);
                    CAPWAP_GET_4BYTE (pVendor->unVendorSpec.
                                      DhcpPoolConfigUpdateReq.u4Subnet[i2Index],
                                      pRcvBuf);
                    CAPWAP_GET_4BYTE (pVendor->unVendorSpec.
                                      DhcpPoolConfigUpdateReq.u4Mask[i2Index],
                                      pRcvBuf);
                    CAPWAP_GET_4BYTE (pVendor->unVendorSpec.
                                      DhcpPoolConfigUpdateReq.
                                      u4StartIp[i2Index], pRcvBuf);
                    CAPWAP_GET_4BYTE (pVendor->unVendorSpec.
                                      DhcpPoolConfigUpdateReq.u4EndIp[i2Index],
                                      pRcvBuf);
                    CAPWAP_GET_4BYTE (pVendor->unVendorSpec.
                                      DhcpPoolConfigUpdateReq.
                                      u4LeaseExprTime[i2Index], pRcvBuf);
                    CAPWAP_GET_4BYTE (pVendor->unVendorSpec.
                                      DhcpPoolConfigUpdateReq.
                                      u4DefaultIp[i2Index], pRcvBuf);
                    CAPWAP_GET_4BYTE (pVendor->unVendorSpec.
                                      DhcpPoolConfigUpdateReq.
                                      u4DnsServerIp[i2Index], pRcvBuf);
#ifdef DHCP_SRV_WANTED
                    CapSetDhcpSrv (&
                                   (pVendor->unVendorSpec.
                                    DhcpPoolConfigUpdateReq), i2Index);
#endif
                }

                if (pVendor->unVendorSpec.DhcpPoolConfigUpdateReq.u2NoofPools ==
                    0)
                {
#ifdef DHCP_SRV_WANTED
                    CapSetDhcpSrv (&
                                   (pVendor->unVendorSpec.
                                    DhcpPoolConfigUpdateReq), 0);
#endif
                }
                break;
            case DHCP_RELAY:
                pVendor->unVendorSpec.DhcpRelayConfigUpdateReq.u2MsgEleType =
                    u2Type;
                CAPWAP_GET_2BYTE (pVendor->unVendorSpec.
                                  DhcpRelayConfigUpdateReq.u2MsgEleLen,
                                  pRcvBuf);
                CAPWAP_GET_4BYTE (pVendor->unVendorSpec.
                                  DhcpRelayConfigUpdateReq.u4VendorId, pRcvBuf);
                CAPWAP_GET_1BYTE (pVendor->unVendorSpec.
                                  DhcpRelayConfigUpdateReq.u1RelayStatus,
                                  pRcvBuf);
                CAPWAP_GET_4BYTE (pVendor->unVendorSpec.
                                  DhcpRelayConfigUpdateReq.u4NextIpAddress,
                                  pRcvBuf);
#ifdef DHCP_SRV_WANTED
#if 0
                CapSetDhcpRelay (&
                                 (pVendor->unVendorSpec.
                                  DhcpRelayConfigUpdateReq));
#endif
#endif
                break;
            case FIREWALL_STATUS:
                pVendor->unVendorSpec.FireWallUpdateReq.u2MsgEleType = u2Type;
                CAPWAP_GET_2BYTE (pVendor->unVendorSpec.FireWallUpdateReq.
                                  u2MsgEleLen, pRcvBuf);
                CAPWAP_GET_4BYTE (pVendor->unVendorSpec.FireWallUpdateReq.
                                  u4VendorId, pRcvBuf);
                CAPWAP_GET_1BYTE (pVendor->unVendorSpec.FireWallUpdateReq.
                                  u1FirewallStatus, pRcvBuf);
                CapSetFirewallStatus (&
                                      (pVendor->unVendorSpec.
                                       FireWallUpdateReq));
                break;
            case NAT_STATUS:
                pVendor->unVendorSpec.NatUpdateReq.u2MsgEleType = u2Type;
                CAPWAP_GET_2BYTE (pVendor->unVendorSpec.NatUpdateReq.
                                  u2MsgEleLen, pRcvBuf);
                CAPWAP_GET_4BYTE (pVendor->unVendorSpec.NatUpdateReq.u4VendorId,
                                  pRcvBuf);
                CAPWAP_GET_4BYTE (pVendor->unVendorSpec.NatUpdateReq.
                                  u4NATConfigIndex, pRcvBuf);
                CAPWAP_GET_2BYTE (pVendor->unVendorSpec.NatUpdateReq.i2WanType,
                                  pRcvBuf);
                CapSetNatStatus (&(pVendor->unVendorSpec.NatUpdateReq));
                break;
            case ROUTE_TABLE:
                pVendor->unVendorSpec.RouteTableUpdateReq.u2MsgEleType = u2Type;
                CAPWAP_GET_2BYTE (pVendor->unVendorSpec.RouteTableUpdateReq.
                                  u2MsgEleLen, pRcvBuf);
                CAPWAP_GET_4BYTE (pVendor->unVendorSpec.RouteTableUpdateReq.
                                  u4VendorId, pRcvBuf);
                CAPWAP_GET_1BYTE (pVendor->unVendorSpec.RouteTableUpdateReq.
                                  u1EntryStatus, pRcvBuf);
                CAPWAP_GET_2BYTE (pVendor->unVendorSpec.RouteTableUpdateReq.
                                  u2NoofRoutes, pRcvBuf);
                for (i2Index = 0;
                     i2Index <
                     pVendor->unVendorSpec.RouteTableUpdateReq.u2NoofRoutes;
                     i2Index++)
                {
                    CAPWAP_GET_4BYTE (pVendor->unVendorSpec.RouteTableUpdateReq.
                                      u4Subnet[i2Index], pRcvBuf);
                    CAPWAP_GET_4BYTE (pVendor->unVendorSpec.RouteTableUpdateReq.
                                      u4NetMask[i2Index], pRcvBuf);
                    CAPWAP_GET_4BYTE (pVendor->unVendorSpec.RouteTableUpdateReq.
                                      u4Gateway[i2Index], pRcvBuf);
                    CapSetIpRoute (&(pVendor->unVendorSpec.RouteTableUpdateReq),
                                   i2Index);
                }
                break;
            case FIREWALL_FILTER_TABLE:
                pVendor->unVendorSpec.FirewallFilterUpdateReq.u2MsgEleType =
                    u2Type;
                CAPWAP_GET_2BYTE (pVendor->unVendorSpec.FirewallFilterUpdateReq.
                                  u2MsgEleLen, pRcvBuf);
                CAPWAP_GET_4BYTE (pVendor->unVendorSpec.FirewallFilterUpdateReq.
                                  u4VendorId, pRcvBuf);
                CAPWAP_GET_1BYTE (pVendor->unVendorSpec.FirewallFilterUpdateReq.
                                  u1EntryStatus, pRcvBuf);
                CAPWAP_GET_2BYTE (pVendor->unVendorSpec.FirewallFilterUpdateReq.
                                  u2NoOfFilters, pRcvBuf);
                CAPWAP_GET_NBYTE (pVendor->unVendorSpec.FirewallFilterUpdateReq.
                                  au1FilterName, pRcvBuf, 36);
                CAPWAP_GET_NBYTE (pVendor->unVendorSpec.FirewallFilterUpdateReq.
                                  au1SrcPort, pRcvBuf, 12);
                CAPWAP_GET_NBYTE (pVendor->unVendorSpec.FirewallFilterUpdateReq.
                                  au1DestPort, pRcvBuf, 12);
                CAPWAP_GET_NBYTE (pVendor->unVendorSpec.FirewallFilterUpdateReq.
                                  au1SrcAddr, pRcvBuf, 30);
                CAPWAP_GET_NBYTE (pVendor->unVendorSpec.FirewallFilterUpdateReq.
                                  au1DestAddr, pRcvBuf, 30);
                CAPWAP_GET_1BYTE (pVendor->unVendorSpec.FirewallFilterUpdateReq.
                                  u1Proto, pRcvBuf);
                CAPWAP_GET_2BYTE (pVendor->unVendorSpec.FirewallFilterUpdateReq.
                                  u2AddrType, pRcvBuf);
                CapSetFirewallFilter (&
                                      (pVendor->unVendorSpec.
                                       FirewallFilterUpdateReq));
                break;
            case FIREWALL_ACL_TABLE:
                pVendor->unVendorSpec.FirewallAclUpdateReq.u2MsgEleType =
                    u2Type;
                CAPWAP_GET_2BYTE (pVendor->unVendorSpec.FirewallAclUpdateReq.
                                  u2MsgEleLen, pRcvBuf);
                CAPWAP_GET_4BYTE (pVendor->unVendorSpec.FirewallAclUpdateReq.
                                  u4VendorId, pRcvBuf);
                CAPWAP_GET_1BYTE (pVendor->unVendorSpec.FirewallAclUpdateReq.
                                  u1EntryStatus, pRcvBuf);
                CAPWAP_GET_2BYTE (pVendor->unVendorSpec.FirewallAclUpdateReq.
                                  u2NoOfAcl, pRcvBuf);
                CAPWAP_GET_NBYTE (pVendor->unVendorSpec.FirewallAclUpdateReq.
                                  au1AclName, pRcvBuf, 36);
                CAPWAP_GET_NBYTE (pVendor->unVendorSpec.FirewallAclUpdateReq.
                                  au1FilterSet, pRcvBuf, 36);
                CAPWAP_GET_4BYTE (pVendor->unVendorSpec.FirewallAclUpdateReq.
                                  i4AclIfIndex, pRcvBuf);
                CAPWAP_GET_1BYTE (pVendor->unVendorSpec.FirewallAclUpdateReq.
                                  u1AclDirection, pRcvBuf);
                CAPWAP_GET_2BYTE (pVendor->unVendorSpec.FirewallAclUpdateReq.
                                  u2SeqNum, pRcvBuf);
                CAPWAP_GET_1BYTE (pVendor->unVendorSpec.FirewallAclUpdateReq.
                                  u1Action, pRcvBuf);
                CapSetFirewallAcl (&
                                   (pVendor->unVendorSpec.
                                    FirewallAclUpdateReq));
                break;
            case VENDOR_CAPW_DIFF_SERV_MSG:
                pVendor->unVendorSpec.VendorCapwSpecDiffServ.u2MsgEleType =
                    u2Type;
                CAPWAP_GET_2BYTE (pVendor->unVendorSpec.VendorCapwSpecDiffServ.
                                  u2MsgEleLen, pRcvBuf);
                CAPWAP_GET_4BYTE (pVendor->unVendorSpec.VendorCapwSpecDiffServ.
                                  u4VendorId, pRcvBuf);
                CAPWAP_GET_1BYTE (pVendor->unVendorSpec.VendorCapwSpecDiffServ.
                                  u1RadioId, pRcvBuf);
                CAPWAP_GET_1BYTE (pVendor->unVendorSpec.VendorCapwSpecDiffServ.
                                  u1EntryStatus, pRcvBuf);
                CAPWAP_GET_1BYTE (pVendor->unVendorSpec.VendorCapwSpecDiffServ.
                                  u1InPriority, pRcvBuf);
                CAPWAP_GET_1BYTE (pVendor->unVendorSpec.VendorCapwSpecDiffServ.
                                  u1OutDscp, pRcvBuf);
                CapSetCapwDiffServ (&
                                    (pVendor->unVendorSpec.
                                     VendorCapwSpecDiffServ));
                break;
            case L3SUBIF_TABLE:
                pVendor->unVendorSpec.L3SubIfUpdateReq.u2MsgEleType = u2Type;
                CAPWAP_GET_2BYTE (pVendor->unVendorSpec.L3SubIfUpdateReq.
                                  u2MsgEleLen, pRcvBuf);
                CAPWAP_GET_4BYTE (pVendor->unVendorSpec.L3SubIfUpdateReq.
                                  u4VendorId, pRcvBuf);
                CAPWAP_GET_1BYTE (pVendor->unVendorSpec.L3SubIfUpdateReq.
                                  u1EntryStatus, pRcvBuf);
                CAPWAP_GET_2BYTE (pVendor->unVendorSpec.L3SubIfUpdateReq.
                                  u2NoOfIfaces, pRcvBuf);
                for (i2Index = 0;
                     i2Index <
                     pVendor->unVendorSpec.L3SubIfUpdateReq.u2NoOfIfaces;
                     i2Index++)
                {
                    CAPWAP_GET_4BYTE (pVendor->unVendorSpec.L3SubIfUpdateReq.
                                      u4PhyPort[i2Index], pRcvBuf);
                    CAPWAP_GET_4BYTE (pVendor->unVendorSpec.L3SubIfUpdateReq.
                                      u4IpAddr[i2Index], pRcvBuf);
                    CAPWAP_GET_4BYTE (pVendor->unVendorSpec.L3SubIfUpdateReq.
                                      u4SubnetMask[i2Index], pRcvBuf);
                    CAPWAP_GET_2BYTE (pVendor->unVendorSpec.L3SubIfUpdateReq.
                                      u2VlanId[i2Index], pRcvBuf);
                    CAPWAP_GET_1BYTE (pVendor->unVendorSpec.L3SubIfUpdateReq.
                                      u1IfNwType[i2Index], pRcvBuf);
                    CAPWAP_GET_1BYTE (pVendor->unVendorSpec.L3SubIfUpdateReq.
                                      u1IfAdminStatus[i2Index], pRcvBuf);

                    CapCreateL3SubIf (&(pVendor->unVendorSpec.L3SubIfUpdateReq),
                                      i2Index);
                }
                break;
            case VENDOR_DOT11N_CONFIG_MSG:
            {
                pVendor->unVendorSpec.VendorDot11nCfg.u2MsgEleType = u2Type;

                CAPWAP_GET_2BYTE (pVendor->unVendorSpec.VendorDot11nCfg.
                                  u2MsgEleLen, pRcvBuf);
                CAPWAP_GET_4BYTE (pVendor->unVendorSpec.VendorDot11nCfg.
                                  u4VendorId, pRcvBuf);
                CAPWAP_GET_1BYTE (pVendor->unVendorSpec.VendorDot11nCfg.
                                  u1RadioId, pRcvBuf);
                CAPWAP_GET_1BYTE (pVendor->unVendorSpec.VendorDot11nCfg.
                                  u1AMPDUStatus, pRcvBuf);
                CAPWAP_GET_1BYTE (pVendor->unVendorSpec.VendorDot11nCfg.
                                  u1AMPDUSubFrame, pRcvBuf);
                CAPWAP_GET_2BYTE (pVendor->unVendorSpec.VendorDot11nCfg.
                                  u2AMPDULimit, pRcvBuf);
                CAPWAP_GET_1BYTE (pVendor->unVendorSpec.VendorDot11nCfg.
                                  u1AMSDUStatus, pRcvBuf);
                CAPWAP_GET_2BYTE (pVendor->unVendorSpec.VendorDot11nCfg.
                                  u2AMSDULimit, pRcvBuf);
                pWssMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                    RadioIfDot11nCfg.u2MessageType =
                    pVendor->unVendorSpec.VendorDot11nCfg.u2MsgEleType;
                pWssMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                    RadioIfDot11nCfg.u2MessageLength =
                    pVendor->unVendorSpec.VendorDot11nCfg.u2MsgEleLen;
                pWssMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                    RadioIfDot11nCfg.u4VendorId =
                    pVendor->unVendorSpec.VendorDot11nCfg.u4VendorId;
                pWssMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                    RadioIfDot11nCfg.u1RadioId =
                    pVendor->unVendorSpec.VendorDot11nCfg.u1RadioId;
                pWssMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                    RadioIfDot11nCfg.u1AMPDUStatus =
                    pVendor->unVendorSpec.VendorDot11nCfg.u1AMPDUStatus;
                pWssMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                    RadioIfDot11nCfg.u1AMPDUSubFrame =
                    pVendor->unVendorSpec.VendorDot11nCfg.u1AMPDUSubFrame;
                pWssMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                    RadioIfDot11nCfg.u2AMPDULimit =
                    pVendor->unVendorSpec.VendorDot11nCfg.u2AMPDULimit;
                pWssMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                    RadioIfDot11nCfg.u1AMSDUStatus =
                    pVendor->unVendorSpec.VendorDot11nCfg.u1AMSDUStatus;
                pWssMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                    RadioIfDot11nCfg.u2AMSDULimit =
                    pVendor->unVendorSpec.VendorDot11nCfg.u2AMSDULimit;
                pWssMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.
                    RadioIfDot11nCfg.isPresent = OSIX_TRUE;

                if (u2MsgIndex ==
                    VENDOR_SPECIFIC_PAYLOAD_CONFIG_UPDATE_REQ_INDEX)
                {
                    if (WssIfProcessRadioIfMsg (WSS_RADIOIF_CONFIG_UPDATE_REQ,
                                                &pWssMsgStruct) != OSIX_SUCCESS)
                    {
                        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                    "Processing DOT11N Config message failed \r\n");
                    }
                }
                break;
            }

#ifdef BAND_SELECT_WANTED
            case VENDOR_BANDSELECT_MSG:
            {
                pVendor->unVendorSpec.VendorBandSelect.u2MsgEleType = u2Type;
                CAPWAP_GET_2BYTE (pVendor->unVendorSpec.
                                  VendorBandSelect.u2MsgEleLen, pRcvBuf);
                CAPWAP_GET_4BYTE (pVendor->unVendorSpec.
                                  VendorBandSelect.u4VendorId, pRcvBuf);
                CAPWAP_GET_1BYTE (pVendor->unVendorSpec.
                                  VendorBandSelect.u1RadioId, pRcvBuf);
                CAPWAP_GET_1BYTE (pVendor->unVendorSpec.
                                  VendorBandSelect.u1WlanId, pRcvBuf);
                CAPWAP_GET_1BYTE (pVendor->unVendorSpec.
                                  VendorBandSelect.u1BandSelectGlobalStatus,
                                  pRcvBuf);
                CAPWAP_GET_1BYTE (pVendor->unVendorSpec.VendorBandSelect.
                                  u1BandSelectStatus, pRcvBuf);
                CAPWAP_GET_1BYTE (pVendor->unVendorSpec.VendorBandSelect.
                                  u1AgeOutSuppression, pRcvBuf);
                pVendor->unVendorSpec.VendorBandSelect.isOptional = OSIX_TRUE;
                if (CapwapUpdateBandSelectStatus (pVendor) != OSIX_SUCCESS)
                {
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    return OSIX_FAILURE;
                }
            }
                break;
#endif
            default:
                break;

        }
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}

/***********************************************************************
*  Function Name   : CapwapValidateResultCode                          *
*  Description     : Validates Result code                             *
*  Input(s)        : pRcvBuf, pcwMsg, u2MsgIndex,pResult               *
*  Output(s)       : None                                              *
*  Returns         : None                                              *
************************************************************************/
INT4
CapwapValidateResultCode (UINT1 *pRcvBuf,
                          tCapwapControlPacket * cwMsg,
                          tResCode * pResult, UINT2 u2MsgIndex)
{
    UINT4               u4Offset = 0;
    UINT2               u2NumMsgElems;
    tWtpProfile         WtpProfile;

    MEMSET (&WtpProfile, 0, sizeof (tWtpProfile));

    u2NumMsgElems = cwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;
    if (u2NumMsgElems > 1)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "The Message element result Code Received More than once \r\n");
        return OSIX_FAILURE;
    }
    else
    {
        u4Offset = cwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[0];
        pRcvBuf += u4Offset;
        CAPWAP_GET_2BYTE (pResult->u2MsgEleType, pRcvBuf);
        CAPWAP_GET_2BYTE (pResult->u2MsgEleLen, pRcvBuf);
        CAPWAP_GET_4BYTE (pResult->u4Value, pRcvBuf);
    }
    return OSIX_SUCCESS;
}

/***********************************************************************
*  Function Name   : CapwapValidateWtpName                             *
*  Description     : Validates WTP name                                *
*  Input(s)        : pRcvBuf, pcwMsg, u2MsgIndex                       *
*  Output(s)       : None                                              *
*  Returns         : None                                              *
************************************************************************/
INT4
CapwapValidateWtpName (UINT1 *pRcvBuf,
                       tCapwapControlPacket * cwMsg, UINT2 u2MsgIndex)
{
    UINT4               u4Offset = 0;
    UINT2               u2NumMsgElems = 0;
    tWtpName            WtpName;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;

    CAPWAP_FN_ENTRY ();
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&WtpName, 0, sizeof (tWtpName));
    if (u2MsgIndex < 200)
    {
        u2NumMsgElems = cwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;
    }
    if (u2NumMsgElems > 0)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "The Message element WTP Name Received More than once \r\n");
        return OSIX_FAILURE;
    }
    else
    {
        u4Offset = cwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[0];
        pRcvBuf += u4Offset;
        CAPWAP_GET_2BYTE (WtpName.u2MsgEleType, pRcvBuf);
        CAPWAP_GET_2BYTE (WtpName.u2MsgEleLen, pRcvBuf);
        CAPWAP_GET_NBYTE (WtpName.wtpName, pRcvBuf, WtpName.u2MsgEleLen);

        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpName = OSIX_TRUE;

        STRNCPY (pWssIfCapwapDB->CapwapGetDB.au1WtpName, WtpName.wtpName,
                 CAPWAP_WTP_NAME_SIZE);
        pWssIfCapwapDB->CapwapGetDB.au1WtpName[CAPWAP_WTP_NAME_SIZE - 1] = '\0';

        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_VALIDATE_DB, pWssIfCapwapDB)
            == OSIX_FAILURE)
        {
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_SET_DB, pWssIfCapwapDB)
                == OSIX_FAILURE)
            {
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Failed to Set the Received WtpName \r\n");
                return OSIX_FAILURE;
            }
        }
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/***********************************************************************
*  Function Name   : CapwapValidateMtuDiscPad                          *
*  Description     : Validates MTU disc pad                            *
*  Input(s)        : pRcvBuf, pcwMsg, u2MsgIndex                       *
*  Output(s)       : None                                              *
*  Returns         : None                                              *
************************************************************************/
INT4
CapwapValidateMtuDiscPad (UINT1 *pRcvBuf,
                          tCapwapControlPacket * cwMsg, UINT2 u2MsgIndex)
{
#ifdef WLC_WANTED
    UINT4               u4Offset = 0;
#endif
    UINT2               u2NumMsgElems;

    CAPWAP_FN_ENTRY ();
    u2NumMsgElems = cwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;
    if (u2NumMsgElems > 0)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "The Message element MTU discovery "
                    "padding Received More than once \r\n");
        return OSIX_FAILURE;
    }
    else
    {
#ifdef WLC_WANTED
        u4Offset = cwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[0];
#endif
        /*capwapGetWtpSessionIdFromBuf(pRcvBuf, u4Offset,wtpSessId, u2NumMsgElems); */
        UNUSED_PARAM (pRcvBuf);
    }
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/***********************************************************************
*  Function Name   : CapwapValidateWtpSessionId                        *
*  Description     : Validates WTP session ID                          *
*  Input(s)        : pRcvBuf, pcwMsg, u2MsgIndex                       *
*  Output(s)       : None                                              *
*  Returns         : None                                              *
************************************************************************/
INT4
CapwapValidateWtpSessionId (UINT1 *pRcvBuf,
                            tCapwapControlPacket * cwMsg,
                            tRemoteSessionManager * pSess, UINT2 u2MsgIndex)
{
    UNUSED_PARAM (pRcvBuf);
    UNUSED_PARAM (cwMsg);
    UNUSED_PARAM (pSess);
    UNUSED_PARAM (u2MsgIndex);
    return OSIX_FAILURE;
}

/***********************************************************************
*  Function Name   : CapwapValidateEcnSupport                           *
*  Description     : Validates ECN support                             *
*  Input(s)        : pRcvBuf, pcwMsg, u2MsgIndex                       *
*  Output(s)       : None                                              *
*  Returns         : None                                              *
************************************************************************/
INT4
CapwapValidateEcnSupport (UINT1 *pRcvBuf,
                          tCapwapControlPacket * pCwMsg, UINT2 u2MsgIndex)
{
    UINT2               u2NumMsgElems;
    UINT4               u4Offset = 0;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    u2NumMsgElems = pCwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;
    u4Offset = pCwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[0];
    if (u2NumMsgElems > 1)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "The Message element Ecn Support Received More than once \r\n");
        return OSIX_FAILURE;
    }
    else
    {
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpEcnSupport = OSIX_TRUE;
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = pCwMsg->u2IntProfileId;
        pRcvBuf += u4Offset + CAPWAP_MSG_ELEM_TYPE_LEN;
        CAPWAP_GET_1BYTE (pWssIfCapwapDB->CapwapGetDB.u1WtpEcnSupport, pRcvBuf);
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_VALIDATE_DB, pWssIfCapwapDB)
            == OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to Validate the Received ECN support \r\n");
            /* If ECN support does not match, dont return failure */
            /* return OSIX_FAILURE; */
        }
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/***********************************************************************
*  Function Name   : CapwapValidateAcName                              *
*  Description     : Validates AC name                                 *
*  Input(s)        : pRcvBuf, pcwMsg, u2MsgIndex                       *
*  Output(s)       : None                                              *
*  Returns         : None                                              *
************************************************************************/
INT4
CapwapValidateAcName (UINT1 *pRcvBuf,
                      tCapwapControlPacket * pCwMsg, UINT2 u2MsgIndex)
{
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT4               u4Offset = 0;
    UINT2               u2NumMsgElems = 0;
    tACName             ACName;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&ACName, 0, sizeof (tACName));

    u2NumMsgElems = pCwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;
    if (u2NumMsgElems > 1)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "AC Name Received More than once \r\n");
        return OSIX_FAILURE;
    }
    else
    {
        u4Offset = pCwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[0];
        pRcvBuf += u4Offset;
        CAPWAP_GET_2BYTE (ACName.u2MsgEleType, pRcvBuf);
        CAPWAP_GET_2BYTE (ACName.u2MsgEleLen, pRcvBuf);
        CAPWAP_GET_NBYTE (pWssIfCapwapDB->au1WlcName, pRcvBuf,
                          ACName.u2MsgEleLen);
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
        pWssIfCapwapDB->CapwapIsGetAllDB.bAcName = OSIX_TRUE;
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_VALIDATE_DB, pWssIfCapwapDB)
            == OSIX_FAILURE)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "AC Name Validation Failed \r\n");
            return OSIX_FAILURE;
        }
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/***********************************************************************
*  Function Name   : CapwapValidatePriAcNameWithPrio                   *
*  Description     : Validates AC name with priority                   *
*  Input(s)        : pRcvBuf, pcwMsg, u2MsgIndex                       *
*  Output(s)       : None                                              *
*  Returns         : None                                              *
************************************************************************/
INT4
CapwapValidatePriAcNameWithPrio (UINT1 *pRcvBuf,
                                 tCapwapControlPacket * pCwMsg,
                                 UINT2 u2MsgIndex)
{
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT4               u4Offset = 0;
    UINT2               u2NumMsgElems;
    tACNameWithPrio     AcNameWithPri;

    CAPWAP_FN_ENTRY ();

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&AcNameWithPri, 0, sizeof (tACNameWithPrio));

    u2NumMsgElems = pCwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;
    if (u2NumMsgElems > 1)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CAPWAP Primary Ac Name Received More than Once \r\n");
        return OSIX_FAILURE;
    }
    else
    {
        u4Offset = pCwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[0];
        pRcvBuf += u4Offset;
        CAPWAP_GET_2BYTE (AcNameWithPri.u2MsgEleType, pRcvBuf);
        CAPWAP_GET_2BYTE (AcNameWithPri.u2MsgEleLen, pRcvBuf);
        CAPWAP_GET_1BYTE (AcNameWithPri.u1Priority, pRcvBuf);
        CAPWAP_GET_NBYTE ((pWssIfCapwapDB->CapwapGetDB.AcNameWithPri[0].
                           wlcName), pRcvBuf, (AcNameWithPri.u2MsgEleLen - 1));
    }

    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/***********************************************************************
*  Function Name   : CapwapSetAcName                                   *
*  Description     : Sets AC name                                      *
*  Input(s)        : pRcvBuf, pcwMsg, u2MsgIndex                       *
*  Output(s)       : None                                              *
*  Returns         : None                                              *
************************************************************************/
INT4
CapwapSetAcName (UINT1 *pRcvBuf,
                 tCapwapControlPacket * pCwMsg, UINT2 u2MsgIndex)
{
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT4               u4Offset = 0;
    UINT2               u2NumMsgElems;
    tACName             ACName;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&ACName, 0, sizeof (tACName));

    u2NumMsgElems = pCwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;
    if (u2NumMsgElems > 1)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "AC Name Received More than once \r\n");
        return OSIX_FAILURE;
    }
    else
    {
        u4Offset = pCwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[0];
        pRcvBuf += u4Offset;
        CAPWAP_GET_2BYTE (ACName.u2MsgEleType, pRcvBuf);
        CAPWAP_GET_2BYTE (ACName.u2MsgEleLen, pRcvBuf);
        CAPWAP_GET_NBYTE (pWssIfCapwapDB->au1WlcName, pRcvBuf,
                          ACName.u2MsgEleLen);
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
        pWssIfCapwapDB->CapwapIsGetAllDB.bAcName = OSIX_TRUE;
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_SET_DB, pWssIfCapwapDB)
            == OSIX_FAILURE)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to set AC Name in WTP DB \r\n");
            return OSIX_FAILURE;
        }
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/***********************************************************************
*  Function Name   : CapwapValidateRadioAdminState                     *
*  Description     : Validates Radio Admin state                       *
*  Input(s)        : pRcvBuf, pcwMsg, pRadioAdmin, u2MsgIndex          *
*  Output(s)       : None                                              *
*  Returns         : None                                              *
************************************************************************/
INT4
CapwapValidateRadioAdminState (UINT1 *pRcvBuf, tCapwapControlPacket * pcwMsg,
                               tRadioIfAdminStatus * pRadioAdmin,
                               UINT2 u2MsgIndex)
{

    UINT1               u1Index;
    UINT1              *pBuf = NULL;
    UINT2               u2NumMsgElems;
    UINT4               u4Offset = 0;

    CAPWAP_FN_ENTRY ();
    tRadioIfMsgStruct   pWssMsgStruct;
    pBuf = pRcvBuf;

    CAPWAP_FN_ENTRY ();
    MEMSET (&pWssMsgStruct, 0, sizeof (tRadioIfMsgStruct));

    u2NumMsgElems = pcwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;

    for (u1Index = 0; u1Index < u2NumMsgElems; u1Index++)
    {
        u4Offset = pcwMsg->capwapMsgElm[u2MsgIndex + u1Index].pu2Offset[0];
        pRcvBuf = pBuf;
        pRcvBuf += u4Offset;

        CAPWAP_GET_2BYTE (pRadioAdmin->u2MessageType, pRcvBuf);
        CAPWAP_GET_2BYTE (pRadioAdmin->u2MessageLength, pRcvBuf);
        CAPWAP_GET_1BYTE (pRadioAdmin->u1RadioId, pRcvBuf);
        CAPWAP_GET_1BYTE (pRadioAdmin->u1AdminStatus, pRcvBuf);

        pWssMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfAdminStatus.
            u2MessageType = pRadioAdmin->u2MessageType;
        pWssMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfAdminStatus.
            u2MessageLength = pRadioAdmin->u2MessageLength;
        pWssMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfAdminStatus.
            u1RadioId = pRadioAdmin->u1RadioId;
        pWssMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfAdminStatus.
            u1AdminStatus = pRadioAdmin->u1AdminStatus;
        pWssMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.u2WtpInternalId =
            pcwMsg->u2IntProfileId;
        pWssMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfAdminStatus.
            isPresent = OSIX_TRUE;

        if (u2MsgIndex == RADIO_ADMIN_STATE_CONFIG_UPDATE_REQ_INDEX)
        {
            if (WssIfProcessRadioIfMsg (WSS_RADIOIF_CONFIG_UPDATE_REQ_VALIDATE,
                                        &pWssMsgStruct) == OSIX_SUCCESS)
            {
                return OSIX_SUCCESS;
            }
        }

    }
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/***********************************************************************
*  Function Name   : CapwapSetRadioAdminState                          *
*  Description     : Sets Radio Admin state                            *
*  Input(s)        : pRcvBuf, pcwMsg, pRadioAdmin, u2MsgIndex          *
*  Output(s)       : None                                              *
*  Returns         : None                                              *
************************************************************************/
INT4
CapwapSetRadioAdminState (UINT1 *pRcvBuf, tCapwapControlPacket * pcwMsg,
                          tRadioIfAdminStatus * pRadioAdmin, UINT2 u2MsgIndex)
{
    UINT1               u1Index;
    UINT1              *pBuf = NULL;
    UINT2               u2NumMsgElems;
    UINT4               u4Offset = 0;

    CAPWAP_FN_ENTRY ();
    tRadioIfMsgStruct   pWssMsgStruct;
    pBuf = pRcvBuf;

    MEMSET (&pWssMsgStruct, 0, sizeof (tRadioIfMsgStruct));

    u2NumMsgElems = pcwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;

    for (u1Index = 0; u1Index < u2NumMsgElems; u1Index++)
    {
        u4Offset = pcwMsg->capwapMsgElm[u2MsgIndex + u1Index].pu2Offset[0];
        pRcvBuf = pBuf;
        pRcvBuf += u4Offset;

        CAPWAP_GET_2BYTE (pRadioAdmin->u2MessageType, pRcvBuf);
        CAPWAP_GET_2BYTE (pRadioAdmin->u2MessageLength, pRcvBuf);
        CAPWAP_GET_1BYTE (pRadioAdmin->u1RadioId, pRcvBuf);
        CAPWAP_GET_1BYTE (pRadioAdmin->u1AdminStatus, pRcvBuf);
        CAPWAP_GET_2BYTE (pcwMsg->u2IntProfileId, pRcvBuf);

        pWssMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfAdminStatus.
            u2MessageType = pRadioAdmin->u2MessageType;
        pWssMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfAdminStatus.
            u2MessageLength = pRadioAdmin->u2MessageLength;
        pWssMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfAdminStatus.
            u1RadioId = pRadioAdmin->u1RadioId;
        pWssMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfAdminStatus.
            u1AdminStatus = pRadioAdmin->u1AdminStatus;
        pWssMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.u2WtpInternalId =
            pcwMsg->u2IntProfileId;
        pWssMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfAdminStatus.
            isPresent = OSIX_TRUE;

        if (u2MsgIndex == RADIO_ADMIN_STATE_CONFIG_UPDATE_REQ_INDEX)
        {
            if (WssIfProcessRadioIfMsg (WSS_RADIOIF_CONFIG_UPDATE_REQ_VALIDATE,
                                        &pWssMsgStruct) == OSIX_SUCCESS)
            {
                return OSIX_SUCCESS;
            }
        }
        else if (u2MsgIndex == RADIO_ADMIN_STATE_CONF_STATUS_REQ_INDEX)
        {
            if (WssIfProcessRadioIfMsg (WSS_RADIOIF_CONFIG_STATUS_REQ_VALIDATE,
                                        &pWssMsgStruct) == OSIX_SUCCESS)
            {
                return OSIX_SUCCESS;
            }
        }

    }
    return OSIX_SUCCESS;
}

/***********************************************************************
*  Function Name   : CapwapValidateStatsTimer                          *
*  Description     : Validate Statistics times                         *
*  Input(s)        : pRcvBuf, pcwMsg, u2MsgIndex                       *
*  Output(s)       : None                                              *
*  Returns         : None                                              *
************************************************************************/
INT4
CapwapValidateStatsTimer (UINT1 *pRcvBuf,
                          tCapwapControlPacket * pcwMsg, UINT2 u2MsgIndex)
{
    UINT4               u4Offset = 0;
    UINT2               u2NumMsgElems = 0;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tStatisticsTimer    statsTimer;

    CAPWAP_FN_ENTRY ();
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&statsTimer, 0, sizeof (tStatisticsTimer));

    u2NumMsgElems = pcwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;
    if (u2NumMsgElems > 1)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "The Message element stats Timer Received More than once \r\n");
        return OSIX_FAILURE;

    }
    u4Offset = pcwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[0];
    pRcvBuf += u4Offset;

    CAPWAP_GET_2BYTE (statsTimer.u2MsgEleType, pRcvBuf);
    CAPWAP_GET_2BYTE (statsTimer.u2MsgEleLen, pRcvBuf);
    CAPWAP_GET_2BYTE (pWssIfCapwapDB->CapwapGetDB.u2WtpStatisticsTimer,
                      pRcvBuf);
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpStatisticsTimer = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;

    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = pcwMsg->u2IntProfileId;

    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_VALIDATE_DB, pWssIfCapwapDB)
        == OSIX_SUCCESS)
    {
        if (pWssIfCapwapDB->CapwapGetDB.u2WtpStatisticsTimer != 0)
        {

            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_SET_DB, pWssIfCapwapDB)
                == OSIX_FAILURE)
            {
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Failed to Set the Received Stats Timeout \r\n");
                return OSIX_FAILURE;
            }
        }
    }
    else
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to Validate the Received Stats Timeout \r\n");
        return OSIX_FAILURE;
    }
    UNUSED_PARAM (pRcvBuf);
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;

}

/***********************************************************************
*  Function Name   : CapwapValidateCapwapTimer                         *
*  Description     : Validates CAPWAP timers                           *
*  Input(s)        : pRcvBuf, pcwMsg, u2MsgIndex                       *
*  Output(s)       : None                                              *
*  Returns         : None                                              *
************************************************************************/
INT4
CapwapValidateCapwapTimer (UINT1 *pRcvBuf,
                           tCapwapControlPacket * pCwMsg, UINT2 u2MsgIndex)
{
    UINT4               u4Offset = 0;
    UINT2               u2NumMsgElems;
    UINT1               u1DiscIntTime = 0, u1EchoIntTime = 0;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;

    CAPWAP_FN_ENTRY ();
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    u2NumMsgElems = pCwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;
    if (u2NumMsgElems > 1)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CAPWAP Timers Received More than Once \r\n");
        return OSIX_FAILURE;
    }
    else
    {
        u4Offset = pCwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[0];
        pRcvBuf += u4Offset + 4;
        CAPWAP_GET_1BYTE (u1DiscIntTime, pRcvBuf);
        CAPWAP_GET_1BYTE (u1EchoIntTime, pRcvBuf);
        if (u1DiscIntTime != 0)
        {
            pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
            pWssIfCapwapDB->CapwapIsGetAllDB.bWtpMaxDiscoveryInterval =
                OSIX_TRUE;
            pWssIfCapwapDB->CapwapGetDB.u2WtpMaxDiscoveryInterval =
                u1DiscIntTime;
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_VALIDATE_DB, pWssIfCapwapDB)
                == OSIX_FAILURE)
            {
                if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_SET_DB, pWssIfCapwapDB)
                    == OSIX_FAILURE)
                {
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "Failed to Set the Discovery Interval Timeout \r\n");
                    return OSIX_SUCCESS;
                }
            }
        }

        if (u1EchoIntTime != 0)
        {
            pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
            pWssIfCapwapDB->CapwapIsGetAllDB.bWtpEchoInterval = OSIX_TRUE;
            pWssIfCapwapDB->CapwapGetDB.u2WtpEchoInterval = u1EchoIntTime;
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_VALIDATE_DB, pWssIfCapwapDB)
                == OSIX_FAILURE)
            {
                if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_SET_DB, pWssIfCapwapDB)
                    == OSIX_FAILURE)
                {
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "Failed to Set the Echo Interval Timeout \r\n");
                    return OSIX_SUCCESS;
                }
            }
        }
    }
    UNUSED_PARAM (pRcvBuf);
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/***********************************************************************
*  Function Name   : CapwapValidateDecryptErrReportPeriod              *
*  Description     : Validates Decrypt Err report period               *
*  Input(s)        : pRcvBuf, pcwMsg, u2MsgIndex                       *
*  Output(s)       : None                                              *
*  Returns         : None                                              *
************************************************************************/
INT4
CapwapValidateDecryptErrReportPeriod (UINT1 *pRcvBuf,
                                      tCapwapControlPacket * cwMsg,
                                      UINT2 u2MsgIndex)
{
    UINT4               u4Offset = 0;
    UINT2               u2NumMsgElems;
    tRadioIfMsgStruct   WssMsgStruct;
    tDecryptErrReportPeriod DecryptErrReportPeriod;

    CAPWAP_FN_ENTRY ();

    MEMSET (&WssMsgStruct, 0, sizeof (tRadioIfMsgStruct));
    MEMSET (&DecryptErrReportPeriod, 0, sizeof (tDecryptErrReportPeriod));

    u2NumMsgElems = cwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;
    if (u2NumMsgElems > 1)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "The Message element Decryption Error"
                    " Report Period Received More than once\r\n");
        return OSIX_FAILURE;
    }
    u4Offset = cwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[0];
    pRcvBuf += u4Offset;
    CAPWAP_GET_2BYTE (DecryptErrReportPeriod.u2MsgEleType, pRcvBuf);
    CAPWAP_GET_2BYTE (DecryptErrReportPeriod.u2MsgEleLen, pRcvBuf);
    CAPWAP_GET_1BYTE (DecryptErrReportPeriod.u1RadioId, pRcvBuf);
    CAPWAP_GET_2BYTE (DecryptErrReportPeriod.u2ReportInterval, pRcvBuf);

    WssMsgStruct.unRadioIfMsg.DecryptReportTimer.u2ReportInterval
        = DecryptErrReportPeriod.u2ReportInterval;
    WssMsgStruct.unRadioIfMsg.DecryptReportTimer.u1RadioId =
        DecryptErrReportPeriod.u1RadioId;
    WssMsgStruct.unRadioIfMsg.DecryptReportTimer.u2WtpInternalId =
        cwMsg->u2IntProfileId;

    if (WssIfProcessRadioIfMsg (WSS_RADIOIF_DECRYPT_REPORT, &WssMsgStruct)
        == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to Set the Received Decrypt Report Timeout \r\n");
        return OSIX_SUCCESS;
    }

    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/***********************************************************************
*  Function Name   : CapwapValidateDecryptErrReport                    *
*  Description     : Validates Decrypt Err report                      *
*  Input(s)        : pRcvBuf, pcwMsg, u2MsgIndex                       *
*  Output(s)       : None                                              *
*  Returns         : None                                              *
************************************************************************/
INT4
CapwapValidateDecryptErrReport (UINT1 *pRcvBuf,
                                tCapwapControlPacket * cwMsg, UINT2 u2MsgIndex)
{
    UINT4               u4Offset = 0;
    UINT2               u2NumMsgElems;
    tWtpProfile         WtpProfile;
    tDecryptErrReport   Decrypt;

    MEMSET (&WtpProfile, 0, sizeof (tWtpProfile));
    MEMSET (&Decrypt, 0, sizeof (tDecryptErrReport));

    u2NumMsgElems = cwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;
    if (u2NumMsgElems > 0)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "The Message element Decryp Error Received More than once \r\n");
        return OSIX_FAILURE;
    }
    else
    {
        u4Offset = cwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[0];
        pRcvBuf += u4Offset;
        CAPWAP_GET_2BYTE (Decrypt.u2MsgEleType, pRcvBuf);
        CAPWAP_GET_2BYTE (Decrypt.u2MsgEleLen, pRcvBuf);
        CAPWAP_GET_1BYTE (Decrypt.u1RadioId, pRcvBuf);
        CAPWAP_GET_2BYTE (Decrypt.u2NumEntries, pRcvBuf);
        CAPWAP_GET_2BYTE (Decrypt.u2LenMacAddr, pRcvBuf);
        CAPWAP_GET_2BYTE (Decrypt.u2RadMacAddr, pRcvBuf);

    }
    return OSIX_SUCCESS;
}

/***********************************************************************
*  Function Name   : CapwapValidateWtpStaticIpAddress                  *
*  Description     : Validates WTP static IP address                   *
*  Input(s)        : pRcvBuf, pcwMsg, u2MsgIndex                       *
*  Output(s)       : None                                              *
*  Returns         : None                                              *
************************************************************************/
INT4
CapwapValidateWtpStaticIpAddress (UINT1 *pRcvBuf,
                                  tCapwapControlPacket * cwMsg,
                                  UINT2 u2MsgIndex)
{
    UINT4               u4Offset = 0;
    UINT2               u2NumMsgElems;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tWtpStaticIpAddr    WtpStaticIpAddr;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&WtpStaticIpAddr, 0, sizeof (tWtpStaticIpAddr));

    u2NumMsgElems = cwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;
    if (u2NumMsgElems > 1)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "The Message element WTP static IP Rcvd More than once \r\n");
        return OSIX_FAILURE;
    }
    else
    {
        u4Offset = cwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[0];
        pRcvBuf += u4Offset;
        CAPWAP_GET_2BYTE (WtpStaticIpAddr.u2MsgEleType, pRcvBuf);
        CAPWAP_GET_2BYTE (WtpStaticIpAddr.u2MsgEleLen, pRcvBuf);
        CAPWAP_GET_4BYTE (pWssIfCapwapDB->CapwapGetDB.WtpStaticIpAddress.
                          u4_addr[0], pRcvBuf);
        CAPWAP_GET_4BYTE (pWssIfCapwapDB->CapwapGetDB.WtpNetmask.u4_addr[0],
                          pRcvBuf);
        CAPWAP_GET_4BYTE (pWssIfCapwapDB->CapwapGetDB.WtpGateway.u4_addr[0],
                          pRcvBuf);
        CAPWAP_GET_1BYTE (pWssIfCapwapDB->CapwapGetDB.u1WtpStaticIpEnable,
                          pRcvBuf);

        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpStaticIpAddress = OSIX_TRUE;
        /* static ip change */
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpNetmask = OSIX_TRUE;
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpGateway = OSIX_TRUE;
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpStaticIpEnable = OSIX_TRUE;

        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_VALIDATE_DB, pWssIfCapwapDB)
            == OSIX_FAILURE)
        {
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_SET_DB, pWssIfCapwapDB)
                == OSIX_FAILURE)
            {
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Failed to Set the Received Wtp static IP \r\n");
                return OSIX_SUCCESS;
            }
        }

    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/***********************************************************************
*  Function Name   : CapwapValidateIdleTimeout                         *
*  Description     : Validates Idle timeout                            *
*  Input(s)        : pRcvBuf, pcwMsg, u2MsgIndex                       *
*  Output(s)       : None                                              *
*  Returns         : None                                              *
************************************************************************/
INT4
CapwapValidateIdleTimeout (UINT1 *pRcvBuf,
                           tCapwapControlPacket * pCwMsg, UINT2 u2MsgIndex)
{
    UINT4               u4Offset = 0;
    UINT2               u2NumMsgElems;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tIdleTimeout        IdleTimeout;

    CAPWAP_FN_ENTRY ();
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&IdleTimeout, 0, sizeof (tIdleTimeout));

    u2NumMsgElems = pCwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;
    if (u2NumMsgElems > 1)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "The Message element Idle Timeout Received More than once \r\n");
        return OSIX_FAILURE;
    }
    u4Offset = pCwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[0];
    pRcvBuf += u4Offset;

    CAPWAP_GET_2BYTE (IdleTimeout.u2MsgEleType, pRcvBuf);
    CAPWAP_GET_2BYTE (IdleTimeout.u2MsgEleLen, pRcvBuf);
    CAPWAP_GET_4BYTE (pWssIfCapwapDB->CapwapGetDB.u4WtpIdleTimeout, pRcvBuf);
    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = pCwMsg->u2IntProfileId;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpIdleTimeout = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_VALIDATE_DB, pWssIfCapwapDB)
        == OSIX_FAILURE)
    {
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_SET_DB, pWssIfCapwapDB)
            == OSIX_FAILURE)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to Set the Received Idle Timeout \r\n");
            return OSIX_SUCCESS;
        }
    }
    UNUSED_PARAM (pRcvBuf);
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/***********************************************************************
*  Function Name   : CapwapValidateWtpFallback                         *
*  Description     : Validates WTP fallback                            *
*  Input(s)        : pRcvBuf, pcwMsg, u2MsgIndex                       *
*  Output(s)       : None                                              *
*  Returns         : None                                              *
************************************************************************/
INT4
CapwapValidateWtpFallback (UINT1 *pRcvBuf,
                           tCapwapControlPacket * cwMsg, UINT2 u2MsgIndex)
{
    UINT4               u4Offset = 0;
    UINT2               u2NumMsgElems;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tWtpFallback        WtpFallback;

    CAPWAP_FN_ENTRY ();
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&WtpFallback, 0, sizeof (tWtpFallback));
    u2NumMsgElems = cwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;
    if (u2NumMsgElems > 1)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "The Message element Wtp Fallback Received More than once \r\n");
        return OSIX_FAILURE;
    }
    u4Offset = cwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[0];
    pRcvBuf += u4Offset;

    CAPWAP_GET_2BYTE (WtpFallback.u2MsgEleType, pRcvBuf);
    CAPWAP_GET_2BYTE (WtpFallback.u2MsgEleLen, pRcvBuf);
    CAPWAP_GET_1BYTE (WtpFallback.mode, pRcvBuf);

    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpFallbackEnable = OSIX_TRUE;

    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = cwMsg->u2IntProfileId;
    pWssIfCapwapDB->CapwapGetDB.u1WtpFallbackEnable = WtpFallback.mode;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_VALIDATE_DB, pWssIfCapwapDB)
        == OSIX_FAILURE)
    {
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_SET_DB, pWssIfCapwapDB)
            == OSIX_FAILURE)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to Set the Received WtpFallback \r\n");
            return OSIX_SUCCESS;
        }
    }

    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/***********************************************************************
*  Function Name   : CapwapValidateIpv4List                            *
*  Description     : Validate IPv4 List                                *
*  Input(s)        : pRcvBuf, pcwMsg, u2MsgIndex                       *
*  Output(s)       : None                                              *
*  Returns         : None                                              *
************************************************************************/
INT4
CapwapValidateIpv4List (UINT1 *pRcvBuf,
                        tCapwapControlPacket * cwMsg, UINT2 u2MsgIndex)
{
#ifdef WLC_WANTED
    UINT4               u4Offset;
#endif
    UINT2               u2NumMsgElems;

    CAPWAP_FN_ENTRY ();
    u2NumMsgElems = cwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;
    if (u2NumMsgElems > 0)
    {
        /* Allocate the dynamic memory */
    }
    else
    {
#ifdef WLC_WANTED
        u4Offset = cwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[0];
#endif
    }
    UNUSED_PARAM (pRcvBuf);
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/***********************************************************************
*  Function Name   : CapwapValidateRadioOperState                      *
*  Description     : Validates radio operational state                 *
*  Input(s)        : pRcvBuf, pcwMsg, pradioOper, u2MsgIndex           *
*  Output(s)       : None                                              *
*  Returns         : None                                              *
************************************************************************/
INT4
CapwapValidateRadioOperState (UINT1 *pRcvBuf, tCapwapControlPacket * pcwMsg,
                              tRadioIfOperStatus * pradioOper, UINT2 u2MsgIndex)
{

    UINT1               u1Index;
    UINT1              *pBuf = NULL;
    UINT4               u4Offset;
    UINT2               u2NumMsgElems;

    CAPWAP_FN_ENTRY ();

    tRadioIfMsgStruct   pWssMsgStruct;
    pBuf = pRcvBuf;
    MEMSET (&pWssMsgStruct, 0, sizeof (tRadioIfMsgStruct));
    u2NumMsgElems = pcwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;

    for (u1Index = 0; u1Index < u2NumMsgElems; u1Index++)
    {
        u4Offset = pcwMsg->capwapMsgElm[u2MsgIndex + u1Index].pu2Offset[0];
        pRcvBuf = pBuf;
        pRcvBuf += u4Offset;

        CAPWAP_GET_2BYTE (pradioOper->u2MessageType, pRcvBuf);
        CAPWAP_GET_2BYTE (pradioOper->u2MessageLength, pRcvBuf);
        CAPWAP_GET_1BYTE (pradioOper->u1RadioId, pRcvBuf);
        CAPWAP_GET_1BYTE (pradioOper->u1OperStatus, pRcvBuf);
        CAPWAP_GET_1BYTE (pradioOper->u1FailureCause, pRcvBuf);
        CAPWAP_GET_2BYTE (pcwMsg->u2IntProfileId, pRcvBuf);

        pWssMsgStruct.unRadioIfMsg.RadioIfChangeStateEventReq.RadioIfOperStatus.
            u2MessageType = pradioOper->u2MessageType;
        pWssMsgStruct.unRadioIfMsg.RadioIfChangeStateEventReq.RadioIfOperStatus.
            u2MessageLength = pradioOper->u2MessageLength;
        pWssMsgStruct.unRadioIfMsg.RadioIfChangeStateEventReq.RadioIfOperStatus.
            u1RadioId = pradioOper->u1RadioId;
        pWssMsgStruct.unRadioIfMsg.RadioIfChangeStateEventReq.RadioIfOperStatus.
            u1OperStatus = pradioOper->u1OperStatus;
        pWssMsgStruct.unRadioIfMsg.RadioIfChangeStateEventReq.RadioIfOperStatus.
            u1FailureCause = pradioOper->u1FailureCause;
        pWssMsgStruct.unRadioIfMsg.RadioIfChangeStateEventReq.u2WtpInternalId =
            pcwMsg->u2IntProfileId;
        pWssMsgStruct.unRadioIfMsg.RadioIfChangeStateEventReq.RadioIfOperStatus.
            isPresent = OSIX_TRUE;

        if (u2MsgIndex == RADIO_OPER_STATE_EVENT_REQ_INDEX)
        {
            if (WssIfProcessRadioIfMsg
                (WSS_RADIOIF_CHANGE_STATE_EVT_REQ_VALIDATE,
                 &pWssMsgStruct) == OSIX_SUCCESS)
            {
                return OSIX_SUCCESS;
            }
        }
        else if (u2MsgIndex == RADIO_OPER_STATE_CONF_UPDATE_RESP_INDEX)
        {
            if (WssIfProcessRadioIfMsg (WSS_RADIOIF_CONFIG_UPDATE_RSP_VALIDATE,
                                        &pWssMsgStruct) == OSIX_SUCCESS)
            {
                return OSIX_SUCCESS;
            }
        }

    }
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/***********************************************************************
*  Function Name   : CapwapSetRadioOperState                           *
*  Description     : Sets operational State                            *
*  Input(s)        : pRcvBuf, pcwMsg, pradioOper, u2MsgIndex           *
*  Output(s)       : None                                              *
*  Returns         : None                                              *
************************************************************************/
INT4
CapwapSetRadioOperState (UINT1 *pRcvBuf, tCapwapControlPacket * pcwMsg,
                         tRadioIfOperStatus * pradioOper, UINT2 u2MsgIndex)
{

    UINT1               u1Index;
    UINT1              *pBuf = NULL;
    UINT4               u4Offset;
    UINT2               u2NumMsgElems;

    CAPWAP_FN_ENTRY ();

    tRadioIfMsgStruct   pWssMsgStruct;
    pBuf = pRcvBuf;
    MEMSET (&pWssMsgStruct, 0, sizeof (tRadioIfMsgStruct));
    u2NumMsgElems = pcwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;

    for (u1Index = 0; u1Index < u2NumMsgElems; u1Index++)
    {
        u4Offset = pcwMsg->capwapMsgElm[u2MsgIndex + u1Index].pu2Offset[0];
        pRcvBuf = pBuf;
        pRcvBuf += u4Offset;

        CAPWAP_GET_2BYTE (pradioOper->u2MessageType, pRcvBuf);
        CAPWAP_GET_2BYTE (pradioOper->u2MessageLength, pRcvBuf);
        CAPWAP_GET_1BYTE (pradioOper->u1RadioId, pRcvBuf);
        CAPWAP_GET_1BYTE (pradioOper->u1OperStatus, pRcvBuf);
        CAPWAP_GET_1BYTE (pradioOper->u1FailureCause, pRcvBuf);
        CAPWAP_GET_2BYTE (pcwMsg->u2IntProfileId, pRcvBuf);

        pWssMsgStruct.unRadioIfMsg.RadioIfChangeStateEventReq.RadioIfOperStatus.
            u2MessageType = pradioOper->u2MessageType;
        pWssMsgStruct.unRadioIfMsg.RadioIfChangeStateEventReq.RadioIfOperStatus.
            u2MessageLength = pradioOper->u2MessageLength;
        pWssMsgStruct.unRadioIfMsg.RadioIfChangeStateEventReq.RadioIfOperStatus.
            u1RadioId = pradioOper->u1RadioId;
        pWssMsgStruct.unRadioIfMsg.RadioIfChangeStateEventReq.RadioIfOperStatus.
            u1OperStatus = pradioOper->u1OperStatus;
        pWssMsgStruct.unRadioIfMsg.RadioIfChangeStateEventReq.RadioIfOperStatus.
            u1FailureCause = pradioOper->u1FailureCause;
        pWssMsgStruct.unRadioIfMsg.RadioIfChangeStateEventReq.u2WtpInternalId =
            pcwMsg->u2IntProfileId;
        pWssMsgStruct.unRadioIfMsg.RadioIfChangeStateEventReq.RadioIfOperStatus.
            isPresent = OSIX_TRUE;

        if (u2MsgIndex == RADIO_OPER_STATE_EVENT_REQ_INDEX)
        {
            if (WssIfProcessRadioIfMsg
                (WSS_RADIOIF_CHANGE_STATE_EVT_REQ_VALIDATE,
                 &pWssMsgStruct) == OSIX_SUCCESS)
            {
                return OSIX_SUCCESS;
            }
        }
        else if (u2MsgIndex == RADIO_OPER_STATE_CONF_UPDATE_RESP_INDEX)
        {
            if (WssIfProcessRadioIfMsg (WSS_RADIOIF_CONFIG_UPDATE_RSP_VALIDATE,
                                        &pWssMsgStruct) == OSIX_SUCCESS)
            {
                return OSIX_SUCCESS;
            }
        }
    }
    return OSIX_SUCCESS;
}

/***********************************************************************
*  Function Name   : CapwapGetIeeeRadioInformation                     *
*  Description     : Gets IEEE Radio information                       *
*  Input(s)        : pRadioIfInfo, pMsgLen, u2IntProfileId             *
*  Output(s)       : None                                              *
*  Returns         : None                                              *
************************************************************************/
INT4
CapwapGetIeeeRadioInformation (tRadioIfInfo * pRadioIfInfo, UINT4 *pMsgLen,
                               UINT2 u2WtpProfileId)
{

    UINT1               u1Index = 0;
    tRadioIfGetDB       RadioIfGetDB;

    CAPWAP_FN_ENTRY ();

    UNUSED_PARAM (u2WtpProfileId);

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    for (u1Index = 1; u1Index <= SYS_DEF_MAX_RADIO_INTERFACES; u1Index++)
    {
        RadioIfGetDB.RadioIfGetAllDB.u1RadioId = u1Index;

        RadioIfGetDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_TRUE;

        if (WssIfProcessRadioIfDBMsg (WSS_GET_PHY_RADIO_IF_DB, &RadioIfGetDB)
            != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to get Radio Type\r\n");
            return OSIX_FAILURE;
        }

        pRadioIfInfo->u2MessageType = WTP_RADIO_INFO;
        pRadioIfInfo->u2MessageLength = IEEE_RADIO_INFO_LEN;
        pRadioIfInfo->u1RadioId = u1Index;
        pRadioIfInfo->u4RadioType
            = RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType;
        pRadioIfInfo->isPresent = OSIX_TRUE;
    }
    *pMsgLen += pRadioIfInfo->u2MessageLength + 4;
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : CapwapGetConfigStatVendorPayload                           *
 * Description  : Receives the CAPWAP packet and get the config stat vendor  *
 *                payload                                                    *
 * Input        : pVendorPayload, pMsgLen,u1Index                            *
 * Output       : None                                                       *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *****************************************************************************/
INT4
CapwapGetConfigStatVendorPayload (tVendorSpecPayload * pVendorPayload,
                                  UINT4 *pMsgLen, UINT1 u1Index)
{
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tWssWlanMsgStruct  *pWssWlanMsgStruct = NULL;
    tWssWlanDB          wssWlanDB;
    tRadioIfGetDB       RadioIfGetDB;
    UINT2               u2NumOfRadios = 1;
    UINT1               u1LoopIndex = 0;
    UINT1               u1Idx = 0;
#ifdef RFMGMT_WANTED
    UINT1               u1RadioId = 0;
    UINT1               u1WlanId = 0;
    tRfMgmtDB           RfMgmtDB;
    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
#endif
    pWssWlanMsgStruct =
        (tWssWlanMsgStruct *) (VOID *) UtlShMemAllocWssWlanBuf ();
    if (pWssWlanMsgStruct == NULL)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapGetConfigStatVendorPayload:- "
                    "UtlShMemAllocWssWlanBuf returned failure\n");
        return OSIX_FAILURE;
    }

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (&wssWlanDB, 0, sizeof (tWssWlanDB));
    MEMSET (pWssWlanMsgStruct, 0, sizeof (tWssWlanMsgStruct));
    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&wssWlanDB, 0, sizeof (tWssWlanDB));
    switch (u1Index)
    {
        case VENDOR_DISC_TYPE:
            pVendorPayload->u2MsgEleType = VENDOR_SPECIFIC_PAYLOAD;
            pVendorPayload->u2MsgEleLen = DISCOVERY_TYPE_VENDOR_MSG_LEN;
            pVendorPayload->elementId = VENDOR_DISC_TYPE;
            pVendorPayload->isOptional = OSIX_TRUE;

            pWssIfCapwapDB->CapwapIsGetAllDB.bWtpDiscType = OSIX_TRUE;

            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
                == OSIX_SUCCESS)
            {
                pVendorPayload->unVendorSpec.VendDiscType.u2MsgEleType
                    = DISCOVERY_TYPE_VENDOR_MSG;
                pVendorPayload->unVendorSpec.VendDiscType.u2MsgEleLen
                    = CAPWAP_VENDOR_DISC_TYPE_MSG_ELM_LEN;
                pVendorPayload->unVendorSpec.VendDiscType.vendorId = VENDOR_ID;
                pVendorPayload->unVendorSpec.VendDiscType.u1Val
                    = pWssIfCapwapDB->CapwapGetDB.u1WtpDiscType;
                pVendorPayload->unVendorSpec.VendDiscType.isOptional = TRUE;
                *pMsgLen +=
                    pVendorPayload->u2MsgEleLen + CAPWAP_MSG_ELEM_TYPE_LEN;
            }
            break;
        case VENDOR_DOT11N_TYPE:
            for (u1LoopIndex = 1; u1LoopIndex <= u2NumOfRadios; u1LoopIndex++)
            {
                RadioIfGetDB.RadioIfGetAllDB.u1RadioId = u1LoopIndex;

                RadioIfGetDB.RadioIfIsGetAllDB.bHtFlag = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bMaxSuppMCS = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bMaxManMCS = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bTxAntenna = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bRxAntenna = OSIX_TRUE;

                RadioIfGetDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bChannelWidth = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bShortGi20 = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bShortGi40 = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bShortGi40 = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bMCSRateSet = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bSuppHtExtCap = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bSuppTxBeamCapParam = OSIX_TRUE;

                if (WssIfProcessRadioIfDBMsg (WSS_GET_PHY_RADIO_IF_DB,
                                              &RadioIfGetDB) != OSIX_SUCCESS)
                {
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "Failed to get Radio Interface Index\r\n");
                    UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
                    return OSIX_FAILURE;
                }
                if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N,
                                              &RadioIfGetDB) != OSIX_SUCCESS)
                {
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "Failed to fetch 11n capabilities\r\n");
                    UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
                    return OSIX_FAILURE;
                }

                if (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                    RADIO_TYPE_BGN
                    || RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                    RADIO_TYPE_AN)
                {
                    pVendorPayload->isOptional = OSIX_TRUE;

                    pVendorPayload->unVendorSpec.Dot11nVendor.isOptional =
                        OSIX_TRUE;

                    pVendorPayload->unVendorSpec.Dot11nVendor.u2MsgEleType
                        = DOT11N_VENDOR_MSG;
                    pVendorPayload->unVendorSpec.Dot11nVendor.u2MsgEleLen
                        = DOT11N_VENDOR_MSG_LEN;
                    pVendorPayload->unVendorSpec.Dot11nVendor.u4VendorId
                        = VENDOR_ID;
                    pVendorPayload->unVendorSpec.Dot11nVendor.u1RadioId
                        = RadioIfGetDB.RadioIfGetAllDB.u1RadioId;
                    pVendorPayload->unVendorSpec.Dot11nVendor.u1HtFlag
                        = RadioIfGetDB.RadioIfGetAllDB.u1HtFlag;

                    pVendorPayload->unVendorSpec.Dot11nVendor.u1MaxSuppMCS
                        = RadioIfGetDB.RadioIfGetAllDB.u1MaxSuppMCS;

                    pVendorPayload->unVendorSpec.Dot11nVendor.u1MaxManMCS
                        = RadioIfGetDB.RadioIfGetAllDB.u1MaxManMCS;

                    pVendorPayload->unVendorSpec.Dot11nVendor.u1TxAntenna
                        = RadioIfGetDB.RadioIfGetAllDB.u1TxAntenna;

                    pVendorPayload->unVendorSpec.Dot11nVendor.u1RxAntenna
                        = RadioIfGetDB.RadioIfGetAllDB.u1RxAntenna;

                    pVendorPayload->unVendorSpec.Dot11nVendor.
                        u4SuppTranBeamformCap = RadioIfGetDB.RadioIfGetAllDB.
                        Dot11NhtTxBeamCapParams.u4SuppTxBeamCapParam;

                    pVendorPayload->unVendorSpec.Dot11nVendor.
                        u2SuppExtCap = RadioIfGetDB.RadioIfGetAllDB.
                        Dot11NhtExtCapParams.u2SuppHtExtCap;

                    pVendorPayload->u2MsgEleType = VENDOR_SPECIFIC_PAYLOAD;
                    pVendorPayload->u2MsgEleLen = DOT11N_VENDOR_MSG_LEN +
                        CAPWAP_MSG_ELEM_TYPE_LEN;
                    *pMsgLen +=
                        pVendorPayload->u2MsgEleLen + CAPWAP_MSG_ELEM_TYPE_LEN;
                }
            }
            pVendorPayload->elementId = VENDOR_DOT11N_TYPE;
            break;

        case VENDOR_DOMAIN_NAME_TYPE:

            pVendorPayload->u2MsgEleType = VENDOR_SPECIFIC_PAYLOAD;
            pVendorPayload->u2MsgEleLen = VENDOR_DOMAIN_NAME_LENGTH;
            pVendorPayload->isOptional = OSIX_TRUE;
            pVendorPayload->elementId = VENDOR_DOMAIN_NAME_TYPE;

            pWssIfCapwapDB->CapwapIsGetAllDB.bCapwapDnsServerIp = OSIX_TRUE;
            pWssIfCapwapDB->CapwapIsGetAllDB.bCapwapDnsDomainName = OSIX_TRUE;

            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
                == OSIX_SUCCESS)
            {
                pVendorPayload->unVendorSpec.VendDns.u2MsgEleType
                    = DOMAIN_NAME_VENDOR_MSG;
                pVendorPayload->unVendorSpec.VendDns.u2MsgEleLen
                    = (DOMAIN_NAME_VEND_MSG_ELEM_LENGTH
                       + DNS_SERVER_VEND_MSG_ELEM_LENGTH);
                pVendorPayload->unVendorSpec.VendDns.vendorId = VENDOR_ID;
                MEMCPY (pVendorPayload->unVendorSpec.VendDns.au1DomainName,
                        pWssIfCapwapDB->DnsProfileEntry.
                        au1FsCapwapDnsDomainName,
                        DOMAIN_NAME_VEND_MSG_ELEM_LENGTH);
                pVendorPayload->unVendorSpec.VendDns.isOptional = TRUE;
                *pMsgLen +=
                    pVendorPayload->u2MsgEleLen + CAPWAP_MSG_ELEM_TYPE_LEN;
            }
            break;

        case VENDOR_NATIVE_VLAN:
            pVendorPayload->u2MsgEleType = VENDOR_SPECIFIC_PAYLOAD;
            pVendorPayload->u2MsgEleLen = VENDOR_NATIVE_VLAN_LENGTH;
            pVendorPayload->isOptional = TRUE;
            pVendorPayload->elementId = VENDOR_NATIVE_VLAN;

            pWssIfCapwapDB->CapwapIsGetAllDB.bNativeVlan = OSIX_TRUE;
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
                == OSIX_SUCCESS)
            {
                pVendorPayload->unVendorSpec.VendVlan.u2MsgEleType
                    = NATIVE_VLAN_VENDOR_MSG;
                pVendorPayload->unVendorSpec.VendVlan.u2MsgEleLen
                    = NATIVE_VLAN_VEND_MSG_ELEM_LENGTH;
                pVendorPayload->unVendorSpec.VendVlan.vendorId = VENDOR_ID;
                pVendorPayload->unVendorSpec.VendVlan.u2VlanId
                    = pWssIfCapwapDB->CapwapGetDB.u4NativeVlan;
                pVendorPayload->unVendorSpec.VendVlan.isOptional = OSIX_TRUE;
                *pMsgLen +=
                    pVendorPayload->u2MsgEleLen + CAPWAP_MSG_ELEM_TYPE_LEN;
            }
            break;

        case VENDOR_MGMT_SSID_TYPE:
            pVendorPayload->u2MsgEleType = VENDOR_SPECIFIC_PAYLOAD;
            pVendorPayload->u2MsgEleLen = MGMT_SSID_VENDOR_MSG_LEN;
            pVendorPayload->elementId = VENDOR_MGMT_SSID_TYPE;
            pVendorPayload->isOptional = TRUE;
            if (WssWlanProcessWssIfMsg (WSS_WLAN_GET_MGMT_SSID,
                                        pWssWlanMsgStruct) == OSIX_SUCCESS)
            {
                pVendorPayload->unVendorSpec.VendSsid.u2MsgEleType
                    = MGMT_SSID_VENDOR_MSG;
                pVendorPayload->unVendorSpec.VendSsid.u2MsgEleLen
                    = WTP_MGMT_SSID_VENDOR_MSG_ELM_LEN;
                pVendorPayload->unVendorSpec.VendSsid.vendorId = VENDOR_ID;
                pVendorPayload->unVendorSpec.VendSsid.isOptional = OSIX_TRUE;
                MEMCPY (pVendorPayload->unVendorSpec.VendSsid.au1ManagmentSSID,
                        pWssWlanMsgStruct->unWssWlanMsg.au1ManagmentSSID,
                        WSSWLAN_SSID_NAME_LEN);
            }
            *pMsgLen += pVendorPayload->u2MsgEleLen + CAPWAP_MSG_ELEM_TYPE_LEN;
            break;
        case VENDOR_LOCAL_ROUTING_TYPE:
            pVendorPayload->u2MsgEleType = VENDOR_SPECIFIC_PAYLOAD;
            pVendorPayload->u2MsgEleLen = VENDOR_LOCAL_ROUTING_TYPE_LENGTH;
            pVendorPayload->isOptional = TRUE;
            pVendorPayload->elementId = VENDOR_LOCAL_ROUTING_TYPE;

            pWssIfCapwapDB->CapwapIsGetAllDB.bWtpLocalRouting = OSIX_TRUE;
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
                == OSIX_SUCCESS)
            {

                pVendorPayload->unVendorSpec.VendLocalRouting.u2MsgEleType
                    = LOCAL_ROUTING_VENDOR_MSG;
                pVendorPayload->unVendorSpec.VendLocalRouting.u2MsgEleLen
                    = NATIVE_VLAN_VEND_MSG_ELEM_LENGTH;
                pVendorPayload->unVendorSpec.VendLocalRouting.vendorId =
                    VENDOR_ID;
                pVendorPayload->unVendorSpec.VendLocalRouting.
                    u1WtpLocalRouting =
                    pWssIfCapwapDB->CapwapGetDB.u1WtpLocalRouting;
                pVendorPayload->unVendorSpec.VendLocalRouting.isOptional =
                    OSIX_TRUE;
                *pMsgLen +=
                    pVendorPayload->u2MsgEleLen + CAPWAP_MSG_ELEM_TYPE_LEN;
            }
            break;
#ifdef RFMGMT_WANTED
        case VENDOR_NEIGH_CONFIG_TYPE:
        {
            pVendorPayload->u2MsgEleType = VENDOR_SPECIFIC_PAYLOAD;
            pVendorPayload->u2MsgEleLen = NEIGH_CONFIG_VENDOR_MSG_LEN;
            pVendorPayload->elementId = VENDOR_NEIGH_CONFIG_TYPE;
            pVendorPayload->isOptional = OSIX_TRUE;

            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
                bNeighborMsgPeriod = OSIX_TRUE;
            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
                bChannelScanDuration = OSIX_TRUE;
            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
                bAutoScanStatus = OSIX_TRUE;
            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
                bNeighborAgingPeriod = OSIX_TRUE;
            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
                bRssiThreshold = OSIX_TRUE;

            pVendorPayload->unVendorSpec.VendorNeighConfig.u2MsgEleType
                = NEIGH_CONFIG_VENDOR_MSG;
            pVendorPayload->unVendorSpec.VendorNeighConfig.u2MsgEleLen
                = RF_NEIGH_CONFIG_VENDOR_MSG_ELM_LEN;
            pVendorPayload->unVendorSpec.VendorNeighConfig.u4VendorId
                = VENDOR_ID;
            pVendorPayload->unVendorSpec.VendorNeighConfig.isOptional
                = OSIX_TRUE;
            for (u1RadioId = 1; u1RadioId <= SYS_DEF_MAX_RADIO_INTERFACES;
                 u1RadioId++)
            {
                RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
                    u4RadioIfIndex = u1RadioId + SYS_DEF_MAX_ENET_INTERFACES;

                if (WssIfProcessRfMgmtDBMsg
                    (RFMGMT_GET_AP_CONFIG_ENTRY, &RfMgmtDB) == RFMGMT_SUCCESS)
                {
                    pVendorPayload->unVendorSpec.VendorNeighConfig.
                        VendorNeighParams[u1RadioId - 1].u2NeighborMsgPeriod
                        = RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                        RfMgmtAPConfigDB.u2NeighborMsgPeriod;
                    pVendorPayload->unVendorSpec.VendorNeighConfig.
                        VendorNeighParams[u1RadioId - 1].u2NeighborAgingPeriod
                        = RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                        RfMgmtAPConfigDB.u2NeighborAgingPeriod;
                    pVendorPayload->unVendorSpec.VendorNeighConfig.
                        VendorNeighParams[u1RadioId - 1].u2ChannelScanDuration
                        = RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                        RfMgmtAPConfigDB.u2ChannelScanDuration;
                    pVendorPayload->unVendorSpec.VendorNeighConfig.
                        VendorNeighParams[u1RadioId - 1].u1AutoScanStatus
                        = RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                        RfMgmtAPConfigDB.u1AutoScanStatus;
                    pVendorPayload->unVendorSpec.VendorNeighConfig.
                        VendorNeighParams[u1RadioId - 1].i2RssiThreshold
                        = RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                        RfMgmtAPConfigDB.i2RssiThreshold;
                    pVendorPayload->unVendorSpec.VendorNeighConfig.
                        VendorNeighParams[u1RadioId - 1].u1RadioId = u1RadioId;

                }
            }
            *pMsgLen += pVendorPayload->u2MsgEleLen + CAPWAP_MSG_ELEM_TYPE_LEN;
        }
            break;
        case VENDOR_CLIENT_CONFIG_TYPE:
        {
            pVendorPayload->u2MsgEleType = VENDOR_SPECIFIC_PAYLOAD;
            pVendorPayload->u2MsgEleLen = CLIENT_CONFIG_VENDOR_MSG_LEN;
            pVendorPayload->elementId = VENDOR_CLIENT_CONFIG_TYPE;
            pVendorPayload->isOptional = OSIX_TRUE;

            RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
                RfMgmtClientConfigIsSetDB.bSNRScanPeriod = OSIX_TRUE;
            RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
                RfMgmtClientConfigIsSetDB.bSNRScanStatus = OSIX_TRUE;
            RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
                RfMgmtClientConfigIsSetDB.bSNRThreshold = OSIX_TRUE;

            pVendorPayload->unVendorSpec.VendorClientConfig.u2MsgEleType
                = CLIENT_CONFIG_VENDOR_MSG;
            pVendorPayload->unVendorSpec.VendorClientConfig.u2MsgEleLen
                = RF_CLIENT_CONFIG_VENDOR_MSG_ELM_LEN;
            pVendorPayload->unVendorSpec.VendorClientConfig.u4VendorId
                = VENDOR_ID;
            pVendorPayload->unVendorSpec.VendorClientConfig.isOptional
                = OSIX_TRUE;
            for (u1RadioId = 1; u1RadioId <= SYS_DEF_MAX_RADIO_INTERFACES;
                 u1RadioId++)
            {
                RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
                    RfMgmtClientConfigDB.u4RadioIfIndex =
                    u1RadioId + SYS_DEF_MAX_ENET_INTERFACES;

                if (WssIfProcessRfMgmtDBMsg
                    (RFMGMT_GET_CLIENT_CONFIG_ENTRY,
                     &RfMgmtDB) == RFMGMT_SUCCESS)
                {
                    pVendorPayload->unVendorSpec.VendorClientConfig.
                        VendorClientParams[u1RadioId - 1].u2SNRScanPeriod
                        = RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
                        RfMgmtClientConfigDB.u2SNRScanPeriod;
                    pVendorPayload->unVendorSpec.VendorClientConfig.
                        VendorClientParams[u1RadioId - 1].u1SNRScanStatus
                        = RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
                        RfMgmtClientConfigDB.u1SNRScanStatus;
                    pVendorPayload->unVendorSpec.VendorClientConfig.
                        VendorClientParams[u1RadioId - 1].u1WlanId
                        = RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
                        RfMgmtClientConfigDB.u1WlanId;
                    pVendorPayload->unVendorSpec.VendorClientConfig.
                        VendorClientParams[u1RadioId - 1].
                        u1BssidScanStatus[u1WlanId]
                        = RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
                        RfMgmtClientConfigDB.u1BssidScanStatus[u1WlanId];
                    pVendorPayload->unVendorSpec.VendorClientConfig.
                        VendorClientParams[u1RadioId - 1].i2SNRThreshold
                        = RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
                        RfMgmtClientConfigDB.i2SNRThreshold;
                    pVendorPayload->unVendorSpec.VendorClientConfig.
                        VendorClientParams[u1RadioId - 1].u1RadioId = u1RadioId;
                }
            }
            *pMsgLen += pVendorPayload->u2MsgEleLen + CAPWAP_MSG_ELEM_TYPE_LEN;
        }
            break;
        case VENDOR_CH_SWITCH_STATUS_TYPE:
        {
            pVendorPayload->u2MsgEleType = VENDOR_SPECIFIC_PAYLOAD;
            pVendorPayload->u2MsgEleLen = CH_SWITCH_STATUS_VENDOR_MSG_LEN;
            pVendorPayload->elementId = VENDOR_CH_SWITCH_STATUS_TYPE;
            pVendorPayload->isOptional = OSIX_TRUE;
            pVendorPayload->unVendorSpec.VendorChSwitchStatus.u2MsgEleType
                = CH_SWITCH_STATUS_VENDOR_MSG;
            pVendorPayload->unVendorSpec.VendorChSwitchStatus.u2MsgEleLen
                = RF_CH_SWITCH_STATUS_VENDOR_MSG_ELM_LEN;
            pVendorPayload->unVendorSpec.VendorChSwitchStatus.u4VendorId
                = VENDOR_ID;
            pVendorPayload->unVendorSpec.VendorChSwitchStatus.isOptional
                = OSIX_TRUE;

            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
                bChSwitchStatus = OSIX_TRUE;

            for (u1RadioId = 1; u1RadioId <= SYS_DEF_MAX_RADIO_INTERFACES;
                 u1RadioId++)
            {
                RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
                    u4RadioIfIndex = u1RadioId + SYS_DEF_MAX_ENET_INTERFACES;

                if (WssIfProcessRfMgmtDBMsg
                    (RFMGMT_GET_AP_CONFIG_ENTRY, &RfMgmtDB) == RFMGMT_SUCCESS)
                {
                    pVendorPayload->unVendorSpec.VendorChSwitchStatus.
                        VendorChSwitchParams[u1RadioId - 1].u1ChSwitchStatus
                        = RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                        RfMgmtAPConfigDB.u1ChSwitchStatus;
                    pVendorPayload->unVendorSpec.VendorChSwitchStatus.
                        VendorChSwitchParams[u1RadioId - 1].u1RadioId =
                        u1RadioId;
                }
            }
            *pMsgLen += pVendorPayload->u2MsgEleLen + CAPWAP_MSG_ELEM_TYPE_LEN;
        }
            break;
#endif
        case VENDOR_DFS_CHANNEL_STATS:
        {
            for (u1RadioId = 1; u1RadioId <= SYS_DEF_MAX_RADIO_INTERFACES;
                 u1RadioId++)
            {
                RadioIfGetDB.RadioIfGetAllDB.u1RadioId = u1RadioId;
                RadioIfGetDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bCapability = OSIX_TRUE;
                if (WssIfProcessRadioIfDBMsg (WSS_GET_PHY_RADIO_IF_DB,
                                              &RadioIfGetDB) != OSIX_SUCCESS)
                {
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "Failed to get Radio Interface Index\r\n");
                    UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
                    return OSIX_FAILURE;
                }
                if ((RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                     RADIO_TYPE_A) ||
                    (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                     RFMGMT_RADIO_TYPEAN) ||
                    (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                     RFMGMT_RADIO_TYPEAC))
                {
                    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                        RfMgmtAPConfigIsSetDB.bDFSChannelStatus = OSIX_TRUE;
                    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
                        u4RadioIfIndex =
                        u1RadioId + SYS_DEF_MAX_ENET_INTERFACES;

                    if (WssIfProcessRfMgmtDBMsg (RFMGMT_GET_AP_CONFIG_ENTRY,
                                                 &RfMgmtDB) == RFMGMT_SUCCESS)
                    {
                        pVendorPayload->isOptional = OSIX_TRUE;
                        while (u1Idx < RADIO_MAX_DFS_CHANNEL)
                        {
                            pVendorPayload->unVendorSpec.DFSChannelInfo.
                                DFSInfo[u1Idx].u4ChannelNum =
                                RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                                RfMgmtAPConfigDB.DFSChannelStatus[u1Idx].
                                u4ChannelNum;
                            pVendorPayload->unVendorSpec.DFSChannelInfo.
                                DFSInfo[u1Idx].u4DFSFlag =
                                RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                                RfMgmtAPConfigDB.DFSChannelStatus[u1Idx].
                                u4DFSFlag;
                            u1Idx++;
                        }
                        pVendorPayload->unVendorSpec.DFSChannelInfo.u1RadioId
                            = u1RadioId;
                        pVendorPayload->unVendorSpec.DFSChannelInfo.isOptional
                            = OSIX_TRUE;
                        pVendorPayload->unVendorSpec.DFSChannelInfo.
                            u2MsgEleType = VENDOR_DFS_CHANNEL_MSG;
                        pVendorPayload->unVendorSpec.DFSChannelInfo.
                            u2MsgEleLen = VENDOR_DFS_CHANNEL_MSG_LEN;
                        pVendorPayload->unVendorSpec.DFSChannelInfo.
                            u4VendorId = VENDOR_ID;

                        pVendorPayload->u2MsgEleType = VENDOR_SPECIFIC_PAYLOAD;
                        pVendorPayload->u2MsgEleLen =
                            VENDOR_DFS_CHANNEL_MSG_LEN +
                            CAPWAP_MSG_ELEM_TYPE_LEN;
                        *pMsgLen +=
                            pVendorPayload->u2MsgEleLen +
                            CAPWAP_MSG_ELEM_TYPE_LEN;
                    }
                }
            }
            pVendorPayload->elementId = VENDOR_DFS_CHANNEL_STATS;
        }
            break;
        default:
            break;
    }
    UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;

}

/*****************************************************************************
 * Function     : CapwapGetWtpFallback                                       *
 * Description  : Get wtp fallback from from DB                              *
 * Input        : pWtpFallback, pMsgLen, u2InternalIndex                     *
 * Output       : None                                                       *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *****************************************************************************/
INT4
CapwapGetWtpFallback (tWtpFallback * pWtpFallback, UINT4 *pMsgLen,
                      UINT2 u2InternalIndex)
{
    UNUSED_PARAM (pWtpFallback);
    UNUSED_PARAM (pMsgLen);
    UNUSED_PARAM (u2InternalIndex);
    return OSIX_FAILURE;
}

INT4
CapwapGetIdleTimeout (tIdleTimeout * pIdleTimeout, UINT4 *pMsgLen,
                      UINT2 u2InternalIndex)
{
    UNUSED_PARAM (pIdleTimeout);
    UNUSED_PARAM (pMsgLen);
    UNUSED_PARAM (u2InternalIndex);
    return OSIX_FAILURE;
}

INT4
CapwapGetAcIpList (tACIplist * pIpList, UINT4 *pMsgLen)
{
    UNUSED_PARAM (pIpList);
    UNUSED_PARAM (pMsgLen);
    return OSIX_FAILURE;
}

INT4
CapwapGetPriAcNameWithPrioList (tACNameWithPrio * pAcNameWithPriolist,
                                UINT4 *pMsgLen, UINT2 u2IntProfileId)
{
    UNUSED_PARAM (pAcNameWithPriolist);
    UNUSED_PARAM (pMsgLen);
    UNUSED_PARAM (u2IntProfileId);
    return OSIX_FAILURE;
}

INT4
CapwapValidateFrameTunnelMode (UINT1 *pRcvBuf,
                               tCapwapControlPacket * pCapMsg, UINT2 u2Index)
{
    UNUSED_PARAM (pRcvBuf);
    UNUSED_PARAM (pCapMsg);
    UNUSED_PARAM (u2Index);
    return OSIX_FAILURE;
}

INT4
CapwapValidateWtpBoardData (UINT1 *pRcvBuf,
                            tCapwapControlPacket * pCapMsg, UINT2 u2Index)
{
    UNUSED_PARAM (pRcvBuf);
    UNUSED_PARAM (pCapMsg);
    UNUSED_PARAM (u2Index);
    return OSIX_FAILURE;
}

INT4
CapwapValidateMacType (UINT1 *pRcvBuf,
                       tCapwapControlPacket * pCapMsg, UINT2 u2Index)
{
    UNUSED_PARAM (pRcvBuf);
    UNUSED_PARAM (pCapMsg);
    UNUSED_PARAM (u2Index);
    return OSIX_FAILURE;
}

INT4
CapwapValidateDiscType (UINT1 *pRcvBuf,
                        tCapwapControlPacket * pCapMsg, UINT2 u2Index)
{
    UNUSED_PARAM (pRcvBuf);
    UNUSED_PARAM (pCapMsg);
    UNUSED_PARAM (u2Index);
    return OSIX_FAILURE;
}

INT4
CapwapGetACDescriptor (tAcDescriptor * pAcDescriptor, UINT4 *pMsgLen)
{
    UNUSED_PARAM (pAcDescriptor);
    UNUSED_PARAM (pMsgLen);
    return OSIX_FAILURE;
}

INT4
CapwapGetControlIpAddr (tCtrlIpAddr * pControlIP4Addr, UINT4 *pMsgLen)
{
    UNUSED_PARAM (pControlIP4Addr);
    UNUSED_PARAM (pMsgLen);
    return OSIX_FAILURE;
}

INT4
CapwapValidateIpv6List (UINT1 *pRcvBuf,
                        tCapwapControlPacket * cwMsg, UINT2 u2MsgIndex)
{
#ifdef WLC_WANTED
    UINT4               u4Offset;
#endif
    UINT2               u2NumMsgElems;

    CAPWAP_FN_ENTRY ();
    u2NumMsgElems = cwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;
    if (u2NumMsgElems > 0)
    {
        /* Allocate the dynamic memory */
    }
    else
    {
#ifdef WLC_WANTED
        u4Offset = cwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[0];
#endif
    }
    UNUSED_PARAM (pRcvBuf);
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

INT4
CapwapValidateRebootStats (UINT1 *pRcvBuf,
                           tCapwapControlPacket * cwMsg, UINT2 u2MsgIndex)
{
#ifdef WLC_WANTED
    UINT4               u4Offset;
#endif
    UINT2               u2NumMsgElems;
    CAPWAP_FN_ENTRY ();
    u2NumMsgElems = cwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;
    if (u2NumMsgElems > 0)
    {
        /* Allocate the dynamic memory */
    }
    else
    {
#ifdef WLC_WANTED
        u4Offset = cwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[0];
#endif
    }
    UNUSED_PARAM (pRcvBuf);
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;

}

#ifdef KERNEL_CAPWAP_WANTED
/*****************************************************************************
 * Function     : CapwapUpdateWtpKernelDB                                    *
 * Description  : This function updates the Ap session details to            *
 *                kernel module                                              *
 * Input        : None                                                       *
 * Output       : None                                                       *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *****************************************************************************/
INT4
CapwapUpdateWtpKernelDB ()
{
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tWtpKernelDB        WssKernelDB;
    UINT1               u1Module = 0;
    UINT2               u2Field = 0;
    UINT4               u4WlcIpAddress = 0;
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (&WssKernelDB, 0, sizeof (tWtpKernelDB));
    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    pWssIfCapwapDB->CapwapIsGetAllDB.bWlcIpAddress = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWlcCtrlPort = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpDataPort = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpLocalRouting = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bMacType = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = 0;

    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB) ==
        OSIX_FAILURE)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Unable to get"
                    "session details from DB\r\n");
        return OSIX_FAILURE;
    }

    u4WlcIpAddress = WssGetWlcIpAddrFromWtpNvRam ();
    WssKernelDB.rbData.u2WlcDataPort = pWssIfCapwapDB->CapwapGetDB.u4WlcCtrlPort + 1;    /*Passing the Data Port */
    WssKernelDB.rbData.u2WtpDataPort =
        pWssIfCapwapDB->CapwapGetDB.u4WtpDataPort;
    if (u4WlcIpAddress == 0)
    {
        WssKernelDB.rbData.u4WlcIpAddr = 0;
    }
    else
    {
        WssKernelDB.rbData.u4WlcIpAddr =
            pWssIfCapwapDB->CapwapGetDB.u4WlcIpAddress;
    }
    WssKernelDB.rbData.u1MacType = pWssIfCapwapDB->CapwapGetDB.u1WtpMacType;
    WssKernelDB.rbData.u1LocalRouting =
        pWssIfCapwapDB->CapwapGetDB.u1WtpLocalRouting;
    WssKernelDB.rbData.au1Interface[0] = 1;
    nmhGetIfMtu (1, &(WssKernelDB.rbData.i4CapwapPortMtu));

    u1Module = TX_MODULE;
    u2Field = AP_GLOBALS;

    if (capwapNpUpdateKernel (u1Module, u2Field, &WssKernelDB) == OSIX_FAILURE)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Kernel Notification Failed \r\n");
        return OSIX_FAILURE;
    }

    u1Module = RX_MODULE;
    u2Field = AP_GLOBALS;

    if (capwapNpUpdateKernel (u1Module, u2Field, &WssKernelDB) == OSIX_FAILURE)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Kernel Notification Failed \r\n");
        return OSIX_FAILURE;
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : CapwapRemoveWtpKernelDB                                    *
 * Description  : This function updates the Ap session details to            *
 *                kernel module                                              *
 * Input        : None                                                       *
 * Output       : None                                                       *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *****************************************************************************/
INT4
CapwapRemoveWtpKernelDB ()
{
    tWtpKernelDB        WssKernelDB;
    UINT1               u1Module = 0;
    UINT2               u2Field = 0;

    MEMSET (&WssKernelDB, 0, sizeof (tWtpKernelDB));
    u1Module = TX_MODULE;
    u2Field = AP_GLOBALS;

    if (capwapNpKernelRemove (u1Module, u2Field, &WssKernelDB) == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Kernel Notification Failed \r\n");
        return OSIX_FAILURE;
    }

    u1Module = RX_MODULE;
    u2Field = AP_GLOBALS;

    if (capwapNpKernelRemove (u1Module, u2Field, &WssKernelDB) == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Kernel Notification Failed \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : CapwapAddDeviceInKernel                                    *
 * Description  : This function adds the device in the kernel                *
 * Input        : None                                                       *
 * Output       : None                                                       *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *****************************************************************************/
INT4
CapwapAddDeviceInKernel (UINT1 *pu1DeviceName)
{
    tWtpKernelDB        WssKernelDB;
    UINT1               u1Module = 0;
    UINT2               u2Field = 0;

    MEMSET (&WssKernelDB, 0, sizeof (tWtpKernelDB));

    MEMCPY (WssKernelDB.rbData.au1DeviceName,
            pu1DeviceName, STRLEN (pu1DeviceName));

    u1Module = TX_MODULE;
    u2Field = AP_ADD_DEVICE;

    if (capwapNpUpdateKernel (u1Module, u2Field, &WssKernelDB) == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Kernel add device failed \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : CapwapRemoveDeviceInKernel                                 *
 * Description  : This function removes teh device in kernel                 *
 * Input        : None                                                       *
 * Output       : None                                                       *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *****************************************************************************/
INT4
CapwapRemoveDeviceInKernel (UINT1 *pu1DeviceName)
{
    tWtpKernelDB        WssKernelDB;
    UINT1               u1Module = 0;
    UINT2               u2Field = 0;

    MEMSET (&WssKernelDB, 0, sizeof (tWtpKernelDB));

    MEMCPY (WssKernelDB.rbData.au1DeviceName,
            pu1DeviceName, STRLEN (pu1DeviceName));
    u1Module = TX_MODULE;
    u2Field = AP_REMOVE_DEVICE;

    if (capwapNpKernelRemove (u1Module, u2Field, &WssKernelDB) == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Kernel remove device Failed \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}
#endif

/*****************************************************************************
 * Function     : CapwapRemoveDeviceInKernel                                 *
 * Description  : This function removes teh device in kernel                 *
 * Input        : None                                                       *
 * Output       : None                                                       *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *****************************************************************************/
UINT1
CapwapSendWebAuthStatusToSta (tVendorSpecPayload * pVendor)
{
    unWssMsgStructs     WssMsgStructs;

    MEMSET (&WssMsgStructs, 0, sizeof (unWssMsgStructs));
    /* Fill the station DB structure and invoke the interface structure */
    WssMsgStructs.StaInfoAp.WebAuthStatus.isPresent =
        pVendor->unVendorSpec.VendorWebAuthStatus.isOptional;
    MEMCPY (WssMsgStructs.StaInfoAp.WebAuthStatus.stationMacAddress,
            pVendor->unVendorSpec.VendorWebAuthStatus.StationMac,
            sizeof (tMacAddr));
    WssMsgStructs.StaInfoAp.WebAuthStatus.u1WebAuthCompStatus =
        pVendor->unVendorSpec.VendorWebAuthStatus.u1WebAuthCompleteStatus;

    if (WssStaWtpProcessWssIfMsg (WSS_WTP_STA_CONFIG_REQUEST_AP,
                                  &WssMsgStructs) != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "WSS_WTP_STA_CONFIG_REQUEST_AP "
                    "Failed \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : CapwapRegisterArpInfo                                      *
 * Description  : This function is registered with ARP module to get the     *
 *                basic arp information for CAPWAP module.                   *
 * Input        : pArpBasicInfo   - arp information                          *
 * Output       : None                                                       *
 * Returns      : None                                                       *
 *****************************************************************************/
VOID
CapwapRegisterArpInfo (tArpBasicInfo * pArpBasicInfo)
{
#ifdef KERNEL_CAPWAP_WANTED
    switch (pArpBasicInfo->u1Action)
    {
        case ARP_CB_ADD_ENTRY:
            CapwapUpdateArpTable (pArpBasicInfo->u4IpAddr,
                                  (UINT1 *) pArpBasicInfo->MacAddr);
            break;

        case ARP_CB_REMOVE_ENTRY:
            CapwapRemoveArpTable (pArpBasicInfo->u4IpAddr,
                                  (UINT1 *) pArpBasicInfo->MacAddr);
            break;

        default:
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "WSS - ARP module - Invalid indication from ARP\r\n");
            break;
    }
#else
    UNUSED_PARAM (pArpBasicInfo);
#endif
    return;
}

#ifdef KERNEL_CAPWAP_WANTED
/*****************************************************************************
 * Function     : CapwapUpdateArpTable                                       *
 * Description  : This function is used to update the kernel module          *
 * Input        : None                                                       *
 * Output       : None                                                       *
 * Returns      : None                                                       *
 *****************************************************************************/
VOID
CapwapUpdateArpTable (UINT4 u4IpAddr, UINT1 *pMacAddr)
{
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT1               u1Module = 0;
    UINT2               u2Field = 0;
    tWtpKernelDB        WssKernelDB;
    WSS_IF_DB_TEMP_MEM_ALLOC_VOID_RETURN (pWssIfCapwapDB)
        MEMSET (&WssKernelDB, 0, sizeof (tWtpKernelDB));
    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    pWssIfCapwapDB->CapwapIsGetAllDB.bWlcIpAddress = OSIX_TRUE;
    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = 0;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWlcCtrlPort = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpDataPort = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpLocalRouting = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bMacType = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;

    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB) ==
        OSIX_FAILURE)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Unable to get"
                    "session details from DB\r\n");
        return;
    }
    if (u4IpAddr == pWssIfCapwapDB->CapwapGetDB.u4WlcIpAddress)
    {
        MEMCPY (WssKernelDB.rbData.WlcMacAddr, pMacAddr, sizeof (tMacAddr));
        WssKernelDB.rbData.u4WlcIpAddr = u4IpAddr;

        u1Module = TX_MODULE;
        u2Field = AP_GLOBAL_WLC_MAC;

        if (capwapNpUpdateKernel (u1Module, u2Field, &WssKernelDB) ==
            OSIX_FAILURE)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Kernel Notification Failed in TX_MODULE for AP_GLOBAL_WLC_MAC\r\n");
            return;
        }
        u1Module = RX_MODULE;
        u2Field = AP_GLOBAL_WLC_MAC;

        if (capwapNpUpdateKernel (u1Module, u2Field, &WssKernelDB) ==
            OSIX_FAILURE)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Kernel Notification Failed  in RX_MODULE for AP_GLOBAL_WLC_MAC\r\n");
            return;
        }
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    WssIfStaUpdateIpAddress (u4IpAddr, pMacAddr);
    return;
}

/*****************************************************************************
 * Function     : CapwapRemoveArpTable                                       *
 * Description  : This function is used to update the kernel module          *
 * Input        : None                                                       *
 * Output       : None                                                       *
 * Returns      : None                                                       *
 *****************************************************************************/
VOID
CapwapRemoveArpTable (UINT4 u4IpAddr, UINT1 *pMacAddr)
{
#if 0
    tWssStaWepProcessDB *pWssStaWepProcessDB = NULL;

    pWssStaWepProcessDB = WssStaProcessEntryGet (pMacAddr);

    if (pWssStaWepProcessDB != NULL)
    {
        pWssStaWepProcessDB->u4StationIpAddress = 0;
        CapwapRemoveKernelStationIPTable (u4IpAddr);
        CapwapReplaceKernelStaTable (pMacAddr);
    }
#endif
    WssIfStaUpdateIpAddressDelete (u4IpAddr, pMacAddr);
    return;
}

#endif

#ifdef BAND_SELECT_WANTED
/*****************************************************************************
 * Function     : CapwapUpdateBandSelectStatus                               *
 * Description  : This function removes teh device in kernel                 *
 * Input        : None                                                       *
 * Output       : None                                                       *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *****************************************************************************/
UINT1
CapwapUpdateBandSelectStatus (tVendorSpecPayload * pVendor)
{
    tWssWlanMsgStruct  *pWssWlanMsgStruct = NULL;

    pWssWlanMsgStruct =
        (tWssWlanMsgStruct *) (VOID *) UtlShMemAllocWssWlanBuf ();
    if (pWssWlanMsgStruct == NULL)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapGetConfigStatVendorPayload:- "
                    "UtlShMemAllocWssWlanBuf returned failure\n");
        return OSIX_FAILURE;
    }

    MEMSET (pWssWlanMsgStruct, 0, sizeof (tWssWlanMsgStruct));

    /* Fill the station DB structure and invoke the interface structure */
    pWssWlanMsgStruct->unWssWlanMsg.WssWlanBandSelect.isPresent =
        pVendor->unVendorSpec.VendorBandSelect.isOptional;
    pWssWlanMsgStruct->unWssWlanMsg.WssWlanBandSelect.u1RadioId =
        pVendor->unVendorSpec.VendorBandSelect.u1RadioId;
    pWssWlanMsgStruct->unWssWlanMsg.WssWlanBandSelect.u1WlanId =
        pVendor->unVendorSpec.VendorBandSelect.u1WlanId;
    pWssWlanMsgStruct->unWssWlanMsg.WssWlanBandSelect.u1BandSelectStatus =
        pVendor->unVendorSpec.VendorBandSelect.u1BandSelectStatus;
    pWssWlanMsgStruct->unWssWlanMsg.WssWlanBandSelect.u1AgeOutSuppression =
        pVendor->unVendorSpec.VendorBandSelect.u1AgeOutSuppression;

    /* Update the Global Status */
    gu1GlobalBandSelectStatus = pVendor->unVendorSpec.
        VendorBandSelect.u1BandSelectGlobalStatus;

    if (WssWlanProcessWssIfMsg (WSS_WLAN_SET_BANDSELECT,
                                pWssWlanMsgStruct) != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "WSS_WLAN_SET_BANDSELECT "
                    "Failed \r\n");
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        return OSIX_FAILURE;
    }
    UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
    return OSIX_SUCCESS;
}

#endif
#endif
