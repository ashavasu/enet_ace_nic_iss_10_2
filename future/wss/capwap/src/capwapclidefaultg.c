/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: capwapclidefaultg.c,v 1.1.1.1 2016/09/20 10:42:38 siva Exp $       
*
* Description: This file contains the routines to initialize the
*              mib objects for the module Capwap 
*********************************************************************/

#include "capwapinc.h"
#include "capwapcli.h"
#include "wsscfgcli.h"

/****************************************************************************
* Function    : CapwapInitializeMibCapwapBaseAcNameListTable
* Input       : pCapwapCapwapBaseAcNameListEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
CapwapInitializeMibCapwapBaseAcNameListTable (tCapwapCapwapBaseAcNameListEntry *
                                              pCapwapCapwapBaseAcNameListEntry)
{
    UNUSED_PARAM (pCapwapCapwapBaseAcNameListEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : CapwapInitializeMibCapwapBaseMacAclTable
* Input       : pCapwapCapwapBaseMacAclEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
CapwapInitializeMibCapwapBaseMacAclTable (tCapwapCapwapBaseMacAclEntry *
                                          pCapwapCapwapBaseMacAclEntry)
{
    UNUSED_PARAM (pCapwapCapwapBaseMacAclEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : CapwapInitializeMibCapwapBaseWtpProfileTable
* Input       : pCapwapCapwapBaseWtpProfileEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
CapwapInitializeMibCapwapBaseWtpProfileTable (tCapwapCapwapBaseWtpProfileEntry *
                                              pCapwapCapwapBaseWtpProfileEntry)
{

    pCapwapCapwapBaseWtpProfileEntry->MibObject.
        i4CapwapBaseWtpProfileWtpFallbackEnable = 1;

    pCapwapCapwapBaseWtpProfileEntry->MibObject.
        u4CapwapBaseWtpProfileWtpEchoInterval = 30;

    pCapwapCapwapBaseWtpProfileEntry->MibObject.
        u4CapwapBaseWtpProfileWtpIdleTimeout = 300;

    pCapwapCapwapBaseWtpProfileEntry->MibObject.
        u4CapwapBaseWtpProfileWtpMaxDiscoveryInterval = 20;

    pCapwapCapwapBaseWtpProfileEntry->MibObject.
        u4CapwapBaseWtpProfileWtpReportInterval = 120;

    pCapwapCapwapBaseWtpProfileEntry->MibObject.
        u4CapwapBaseWtpProfileWtpStatisticsTimer = 120;

    pCapwapCapwapBaseWtpProfileEntry->MibObject.
        i4FsCapwapFsAcTimestampTrigger = CLI_FSCAPWAP_DISABLE;
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : CapwapInitializeMibFsWtpModelTable
* Input       : pCapwapFsWtpModelEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
CapwapInitializeMibFsWtpModelTable (tCapwapFsWtpModelEntry *
                                    pCapwapFsWtpModelEntry)
{
    pCapwapFsWtpModelEntry->MibObject.i4FsCapwapWtpMacType = CLI_MAC_TYPE_LOCAL;
    pCapwapFsWtpModelEntry->MibObject.au1FsCapwapWtpTunnelMode[0] =
        CLI_TUNNEL_MODE_DOT3;
    pCapwapFsWtpModelEntry->MibObject.i4FsCapwapWtpTunnelModeLen = 1;
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : CapwapInitializeMibFsWtpRadioTable
* Input       : pCapwapFsWtpRadioEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
CapwapInitializeMibFsWtpRadioTable (tCapwapFsWtpRadioEntry *
                                    pCapwapFsWtpRadioEntry)
{
    pCapwapFsWtpRadioEntry->MibObject.u4FsWtpRadioType = CLI_RADIO_TYPEB;
    pCapwapFsWtpRadioEntry->MibObject.i4FsRadioAdminStatus = CLI_NETWORK_ENABLE;
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : CapwapInitializeMibFsCapwapWhiteListTable
* Input       : pCapwapFsCapwapWhiteListEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
CapwapInitializeMibFsCapwapWhiteListTable (tCapwapFsCapwapWhiteListEntry *
                                           pCapwapFsCapwapWhiteListEntry)
{
    UNUSED_PARAM (pCapwapFsCapwapWhiteListEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : CapwapInitializeMibFsCapwapBlackList
* Input       : pCapwapFsCapwapBlackListEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
CapwapInitializeMibFsCapwapBlackList (tCapwapFsCapwapBlackListEntry *
                                      pCapwapFsCapwapBlackListEntry)
{
    UNUSED_PARAM (pCapwapFsCapwapBlackListEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : CapwapInitializeMibFsCapwapWtpConfigTable
* Input       : pCapwapFsCapwapWtpConfigEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
CapwapInitializeMibFsCapwapWtpConfigTable (tCapwapFsCapwapWtpConfigEntry *
                                           pCapwapFsCapwapWtpConfigEntry)
{
    pCapwapFsCapwapWtpConfigEntry->MibObject.i4FsWtpDiscoveryType =
        CLI_WTP_DISC_TYPE_REFERRAL;
    pCapwapFsCapwapWtpConfigEntry->MibObject.i4FsCapwapClearConfig = FALSE;
    pCapwapFsCapwapWtpConfigEntry->MibObject.i4FsCapwapWtpReset = FALSE;
    pCapwapFsCapwapWtpConfigEntry->MibObject.u4FsWtpDeleteOperation =
        CLI_NO_CRASHMEMORY;
    pCapwapFsCapwapWtpConfigEntry->MibObject.i4FsCapwapClearApStats = CLI_NETWORK_DISABLE;
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : CapwapInitializeMibFsCapwapLinkEncryptionTable
* Input       : pCapwapFsCapwapLinkEncryptionEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
CapwapInitializeMibFsCapwapLinkEncryptionTable
    (tCapwapFsCapwapLinkEncryptionEntry * pCapwapFsCapwapLinkEncryptionEntry)
{
    pCapwapFsCapwapLinkEncryptionEntry->MibObject.
        i4FsCapwapEncryptChannelStatus = CLI_FSCAPWAP_ENCRYPTION_DISABLE;
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : CapwapInitializeMibFsCawapDefaultWtpProfileTable
* Input       : pCapwapFsCapwapDefaultWtpProfileEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
CapwapInitializeMibFsCawapDefaultWtpProfileTable
    (tCapwapFsCapwapDefaultWtpProfileEntry *
     pCapwapFsCapwapDefaultWtpProfileEntry)
{

    pCapwapFsCapwapDefaultWtpProfileEntry->MibObject.
        i4FsCapwapDefaultWtpProfileWtpFallbackEnable = 1;

    pCapwapFsCapwapDefaultWtpProfileEntry->MibObject.
        u4FsCapwapDefaultWtpProfileWtpEchoInterval = 30;

    pCapwapFsCapwapDefaultWtpProfileEntry->MibObject.
        u4FsCapwapDefaultWtpProfileWtpIdleTimeout = 300;

    pCapwapFsCapwapDefaultWtpProfileEntry->MibObject.
        u4FsCapwapDefaultWtpProfileWtpMaxDiscoveryInterval = 20;

    pCapwapFsCapwapDefaultWtpProfileEntry->MibObject.
        u4FsCapwapDefaultWtpProfileWtpReportInterval = 120;

    pCapwapFsCapwapDefaultWtpProfileEntry->MibObject.
        u4FsCapwapDefaultWtpProfileWtpStatisticsTimer = 120;

    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : CapwapInitializeMibFsCapwapDnsProfileTable
* Input       : pCapwapFsCapwapDnsProfileEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
CapwapInitializeMibFsCapwapDnsProfileTable (tCapwapFsCapwapDnsProfileEntry *
                                            pCapwapFsCapwapDnsProfileEntry)
{
    UNUSED_PARAM (pCapwapFsCapwapDnsProfileEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : CapwapInitializeMibFsWtpNativeVlanIdTable
* Input       : pCapwapFsWtpNativeVlanIdTable
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
CapwapInitializeMibFsWtpNativeVlanIdTable (tCapwapFsWtpNativeVlanIdTable *
                                           pCapwapFsWtpNativeVlanIdTable)
{
    UNUSED_PARAM (pCapwapFsWtpNativeVlanIdTable);
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : CapwapInitializeMibFsWtpLocalRoutingTable
* Input       : pCapwapFsWtpLocalRoutingTable
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
CapwapInitializeMibFsWtpLocalRoutingTable (tCapwapFsWtpLocalRoutingTable *
                                           pCapwapFsWtpLocalRoutingTable)
{
    pCapwapFsWtpLocalRoutingTable->MibObject.i4FsWtpLocalRouting =
        CLI_FSCAPWAP_DISABLE;
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : CapwapInitializeMibFsCawapDiscStatsTable
* Input       : pCapwapFsCawapDiscStatsEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
CapwapInitializeMibFsCawapDiscStatsTable (tCapwapFsCawapDiscStatsEntry *
                                          pCapwapFsCawapDiscStatsEntry)
{
    UNUSED_PARAM (pCapwapFsCawapDiscStatsEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : CapwapInitializeMibFsCawapJoinStatsTable
* Input       : pCapwapFsCawapJoinStatsEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
CapwapInitializeMibFsCawapJoinStatsTable (tCapwapFsCawapJoinStatsEntry *
                                          pCapwapFsCawapJoinStatsEntry)
{
    UNUSED_PARAM (pCapwapFsCawapJoinStatsEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : CapwapInitializeMibFsCawapConfigStatsTable
* Input       : pCapwapFsCawapConfigStatsEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
CapwapInitializeMibFsCawapConfigStatsTable (tCapwapFsCawapConfigStatsEntry *
                                            pCapwapFsCawapConfigStatsEntry)
{
    UNUSED_PARAM (pCapwapFsCawapConfigStatsEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : CapwapInitializeMibFsCawapRunStatsTable
* Input       : pCapwapFsCawapRunStatsEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
CapwapInitializeMibFsCawapRunStatsTable (tCapwapFsCawapRunStatsEntry *
                                         pCapwapFsCawapRunStatsEntry)
{
    UNUSED_PARAM (pCapwapFsCawapRunStatsEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : CapwapInitializeMibFsCapwapWirelessBindingTable
* Input       : pCapwapFsCapwapWirelessBindingEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
CapwapInitializeMibFsCapwapWirelessBindingTable
    (tCapwapFsCapwapWirelessBindingEntry * pCapwapFsCapwapWirelessBindingEntry)
{
    UNUSED_PARAM (pCapwapFsCapwapWirelessBindingEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : CapwapInitializeMibFsCapwapStationWhiteList
* Input       : pCapwapFsCapwapStationWhiteListEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
CapwapInitializeMibFsCapwapStationWhiteList
    (tCapwapFsCapwapStationWhiteListEntry *
     pCapwapFsCapwapStationWhiteListEntry)
{
    UNUSED_PARAM (pCapwapFsCapwapStationWhiteListEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : CapwapInitializeMibFsCapwapWtpRebootStatisticsTable
* Input       : pCapwapFsCapwapWtpRebootStatisticsEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
CapwapInitializeMibFsCapwapWtpRebootStatisticsTable
    (tCapwapFsCapwapWtpRebootStatisticsEntry *
     pCapwapFsCapwapWtpRebootStatisticsEntry)
{
    UNUSED_PARAM (pCapwapFsCapwapWtpRebootStatisticsEntry);
    return OSIX_SUCCESS;
}
