/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: capwapclitask.c,v 1.1.1.1 2016/09/20 10:42:39 siva Exp $
 * Description:This file contains procedures related to
 *             CAPWAP - Task Initialization
 *******************************************************************/

#include "capwapinc.h"
#if 0
/*****************************************************************************/
/*                                                                           */
/* Function     : CapwapTaskSpawnCapwapTask()                                    */
/*                                                                           */
/* Description  : This procedure is provided to Spawn Capwap Task              */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS, if CAPWAP Task is successfully spawned         */
/*                OSIX_FAILURE, otherwise                                    */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
CapwapTaskSpawnCapwapTask (INT1 *pi1Arg)
{

    UNUSED_PARAM (pi1Arg);
    CAPWAP_FN_ENTRY();

    /* task initializations */
    if (CapwapMainTaskInit () == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, " !!!!! CAPWAP TASK INIT FAILURE  !!!!! \n");
        CapwapMainDeInit ();
        lrInitComplete (OSIX_FAILURE);
        return;
    }

    /* spawn capwap task */

    /*if (OsixCreateTask ((const UINT1 *) CAPWAP_TASK_NAME, CAPWAP_TASK_PRIORITY,
       OSIX_DEFAULT_STACK_SIZE,
       CapwapMainTask, NULL, OSIX_DEFAULT_TASK_MODE,
       (tOsixTaskId *) & (gCapwapGlobals.capwapTaskId))
       == OSIX_FAILURE)
       {
       return OSIX_FAILURE;
       } */

    CapwapTaskRegisterCapwapMibs ();
    lrInitComplete (OSIX_SUCCESS);
    CapwapMainTask ();
    CAPWAP_FN_EXIT();
    return;
}
#endif
/*
PUBLIC INT4 CapwapTaskRegisterCapwapMibs ()
{
}
*/

/*------------------------------------------------------------------------*/
/*                        End of the file  capwaptask.c                     */
/*------------------------------------------------------------------------*/
