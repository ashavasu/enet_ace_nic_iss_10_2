/*********************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 * $Id: capwapwlcutil.c,v 1.4 2018/01/04 09:54:32 siva Exp $       
 * Description : This file contains the utility
 *               functions of the CAPWAP  module
 *********************************************************/
#ifndef _CAPWAPWLCUTIL_C_
#define _CAPWAPWLCUTIL_C_
#define _CAPWAP_GLOBAL_VAR

#include "capwapinc.h"
#include "wsssta.h"
#ifdef NPAPI_WANTED
#include "nputil.h"
#endif
#include "wlchdlrinc.h"
#include "radioif.h"
#include "wsswlanwlcproto.h"

#include "wssmacinc.h"
#include "wsscfgprot.h"
#ifdef KERNEL_CAPWAP_WANTED
#include "fcusprot.h"
#include "fcusglob.h"
#if defined (WLC_WANTED) && defined (LNXIP4_WANTED)
extern UINT1       *CfaGddGetLnxIntfnameForPort (UINT2 u2Index);
extern UINT4        gu4CapwapEnable;
extern VOID         VlanConvertLocalPortListToArray (tLocalPortList
                                                     LocalPortList,
                                                     UINT4 *pu4IfPortArray,
                                                     UINT2 *u2NumPorts);
#endif
#endif
extern INT4         WssCfgSetLegacyRateStatus (UINT4 *pu4ProfileId);
extern INT4         WsscfgGetApGroupEnable (UINT4 *pi4ApGroupEnable);

static UINT4        gu1IsConfigResponseReceived;
/*****************************************************************************
 * Function     : CapwapGetACDescriptor                                      *
 * Description  : Receives the CAPWAP packet and get the AC descriptor       *
 * Input        : pAcDescriptor, pMsgLen                                     *
 * Output       : None                                                       *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *****************************************************************************/
INT4
CapwapGetACDescriptor (tAcDescriptor * pAcDescriptor, UINT4 *pMsgLen)
{
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT2               u2Len;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    pWssIfCapwapDB->CapwapIsGetAllDB.bWlcDescriptor = OSIX_TRUE;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB) ==
        OSIX_SUCCESS)
    {
        pAcDescriptor->u2MsgEleType = AC_DESCRIPTOR;
        pAcDescriptor->stations = pWssIfCapwapDB->u2ActiveStation;
        pAcDescriptor->stationLimit = pWssIfCapwapDB->u2MaxStationLimit;
        pAcDescriptor->activeWtp = pWssIfCapwapDB->u2ActiveWtps;
        pAcDescriptor->maxWtp = pWssIfCapwapDB->u2MaxWtps;
        pAcDescriptor->security = pWssIfCapwapDB->u1Security;
        pAcDescriptor->radioMacField = pWssIfCapwapDB->u1RMAC;
        pAcDescriptor->dtlsPolicy = pWssIfCapwapDB->u1DataDTLSStatus;
        pAcDescriptor->ACVendorId = pWssIfCapwapDB->u4VendorSMI;
        u2Len = (UINT2) (STRLEN (pWssIfCapwapDB->au1WlcHWversion));
        if (u2Len > 0)
        {
            pAcDescriptor->VendDesc[WTP_HW_VERSION].vendorInfoType
                = WLC_HW_VERSION;
            pAcDescriptor->VendDesc[WTP_HW_VERSION].u2VendorInfoLength = u2Len;
            CAPWAP_MEMSET (pAcDescriptor->VendDesc[WTP_HW_VERSION].
                           vendorInfoData, 0, CAPWAP_MAX_DATA_LENGTH);
            MEMCPY (pAcDescriptor->VendDesc[WTP_HW_VERSION].vendorInfoData,
                    pWssIfCapwapDB->au1WlcHWversion, u2Len);
            pAcDescriptor->u2MsgEleLen = (UINT2) (pAcDescriptor->u2MsgEleLen +
                                                  (u2Len +
                                                   CAPWAP_VENDOR_ID_LEN +
                                                   CAPWAP_MSG_ELEM_TYPE_LEN));
            pAcDescriptor->u4VendorInfoCount++;
        }
        u2Len = (UINT2) (STRLEN (pWssIfCapwapDB->au1WlcSWversion));
        if (u2Len > 0)
        {
            pAcDescriptor->VendDesc[WTP_SW_VERSION].vendorInfoType
                = WLC_SW_VERSION;
            pAcDescriptor->VendDesc[WTP_SW_VERSION].u2VendorInfoLength = u2Len;
            CAPWAP_MEMSET (pAcDescriptor->VendDesc[WTP_SW_VERSION].
                           vendorInfoData, 0, CAPWAP_MAX_DATA_LENGTH);
            MEMCPY (pAcDescriptor->VendDesc[WTP_SW_VERSION].vendorInfoData,
                    pWssIfCapwapDB->au1WlcSWversion, u2Len);
            pAcDescriptor->u2MsgEleLen = (UINT2) (pAcDescriptor->u2MsgEleLen +
                                                  (u2Len +
                                                   CAPWAP_VENDOR_ID_LEN +
                                                   CAPWAP_MSG_ELEM_TYPE_LEN));
            pAcDescriptor->u4VendorInfoCount++;
        }
        pAcDescriptor->u2MsgEleLen =
            (UINT2) (pAcDescriptor->u2MsgEleLen + AC_DESCRIPTOR_MIN_LEN);
        *pMsgLen +=
            (UINT4) (pAcDescriptor->u2MsgEleLen + CAPWAP_MSG_ELEM_TYPE_LEN);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_SUCCESS;
    }
    else
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to get Location Data from AP HDLR DB \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to get Location Data from AP HDLR DB \r\n"));
        return OSIX_FAILURE;
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : CapwapGetControlIpAddr                                     *
 * Description  : Receives the CAPWAP packet and get the control IP addr     *
 * Input        : pControlIP4Addr, pMsgLen                                   *
 * Output       : None                                                       *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *****************************************************************************/
INT4
CapwapGetControlIpAddr (tCtrlIpAddr * pControlIP4Addr, UINT4 *pMsgLen)
{
    tWssIfCapDB        *pWssIfCapwapDB = NULL;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    pWssIfCapwapDB->CapwapIsGetAllDB.bActiveWtps = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bLocalIpAddr = OSIX_TRUE;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB) ==
        OSIX_SUCCESS)
    {
        pControlIP4Addr->u2MsgEleType = CAPWAP_CTRL_IPV4_ADDR;
        pControlIP4Addr->ipAddr.u4_addr[0] =
            pWssIfCapwapDB->LocalIpAddr.u4_addr[0];
        pControlIP4Addr->wtpCount = pWssIfCapwapDB->u2ActiveWtps;
        pControlIP4Addr->u2MsgEleLen = CAPWAP_CTRL_IPV4_ADDR_LEN;
        *pMsgLen +=
            (UINT4) (pControlIP4Addr->u2MsgEleLen + CAPWAP_MSG_ELEM_TYPE_LEN);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_SUCCESS;
    }
    else
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to Get Control Ipv4 Address \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to Get Control Ipv4 Address \r\n"));
        return OSIX_FAILURE;
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : CapwapGetVendorPayload                                     *
 * Description  : Receives the CAPWAP packet and get the vendor payload      *
 * Input        : pVendorPayload, pMsgLen                                    *
 * Output       : None                                                       *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *****************************************************************************/
INT4
CapwapGetVendorPayload (tVendorSpecPayload * pVendorPayload, UINT4 *pMsgLen)
{
    UNUSED_PARAM (pVendorPayload);
    UNUSED_PARAM (pMsgLen);

    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : CapwapGetDiscRespVendorPayload                             *
 * Description  : Receives the CAPWAP packet and get the Disc response vendor*
 *                payload                                                    *
 * Input        : pVendorPayload, pMsgLen,u1Index                            *
 * Output       : None                                                       *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *****************************************************************************/
INT4
CapwapGetDiscRespVendorPayload (tVendorSpecPayload * pVendorPayload,
                                UINT4 *pMsgLen, UINT1 u1Index,
                                UINT2 u2InternalId)
{
    tWssIfCapDB        *pWssIfCapwapDB = NULL;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    CAPWAP_FN_ENTRY ();
    switch (u1Index)
    {
        case VENDOR_CONTROL_POLICY_DTLS_MSG:
            pWssIfCapwapDB->CapwapIsGetAllDB.bWtpCtrlDTLSStatus = OSIX_TRUE;
            pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
            pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2InternalId;
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB) !=
                OSIX_SUCCESS)
            {
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Failed to get WtpCtrlDTLSStatus Disc"
                            "Response Message Elements \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to get WtpCtrlDTLSStatus Disc"
                              "Response Message Elements \r\n"));
                return OSIX_FAILURE;
            }
            if (pWssIfCapwapDB->CapwapGetDB.u1WtpCtrlDTLSStatus == 0)
            {
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Failed to get valid WtpCtrlDTLSStatus value"
                            "for Response Message Elements \n"
                            "May be due to invalid ApProfileId\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to get valid WtpCtrlDTLSStatus value"
                              "for Response Message Elements \n"
                              "May be due to invalid ApProfileId\r\n"));
                return OSIX_FAILURE;
            }

            pVendorPayload->isOptional = OSIX_TRUE;
            pVendorPayload->elementId = VENDOR_CONTROL_POLICY_DTLS_MSG;
            pVendorPayload->u2MsgEleType =
                CAPWAP_VEND_SPECIFIC_PAYLOAD_MSG_TYPE;
            pVendorPayload->u2MsgEleLen = 9;
            pVendorPayload->unVendorSpec.VendorContDTLS.isOptional = OSIX_TRUE;
            pVendorPayload->unVendorSpec.VendorContDTLS.u2MsgEleType =
                CONTROL_POLICY_DTLS_VENDOR_MSG;
            pVendorPayload->unVendorSpec.VendorContDTLS.u2MsgEleLen = 5;
            pVendorPayload->unVendorSpec.VendorContDTLS.vendorId = VENDOR_ID;
            pVendorPayload->unVendorSpec.VendorContDTLS.u1Val =
                pWssIfCapwapDB->CapwapGetDB.u1WtpCtrlDTLSStatus;
            *pMsgLen +=
                (UINT4) (pVendorPayload->u2MsgEleLen +
                         CAPWAP_MSG_ELEM_TYPE_LEN);

            break;
        default:
            break;
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : CapwapGetJoinRespVendorPayload                             *
 * Description  : Receives the CAPWAP packet and get the config stat vendor  *
 *                payload                                                    *
 * Input        : pVendorPayload, pMsgLen,u1Index                            *
 * Output       : None                                                       *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *****************************************************************************/
INT4
CapwapGetJoinRespVendorPayload (tVendorSpecPayload * pVendorPayload,
                                UINT4 *pMsgLen, UINT1 u1Index,
                                UINT2 u2InternalId)
{
    tWssIfCapDB        *pWssIfCapwapDB = NULL;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    CAPWAP_FN_ENTRY ();
    switch (u1Index)
    {
        case VENDOR_LOCAL_ROUTING_TYPE:
            pWssIfCapwapDB->CapwapIsGetAllDB.bWtpLocalRouting = OSIX_TRUE;
            pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
            pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2InternalId;
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB) !=
                OSIX_SUCCESS)
            {
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Failed to get WtpLocalRouting Join"
                            "Response Message Elements \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to get WtpLocalRouting Join"
                              "Response Message Elements \r\n"));
                return OSIX_FAILURE;
            }
            if (pWssIfCapwapDB->CapwapGetDB.u1WtpLocalRouting == 0)
            {
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Failed to get valid WtpLocalRouting value"
                            "for Response Message Elements \n"
                            "May be due to invalid ApProfileId\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to get valid WtpLocalRouting value"
                              "for Response Message Elements \n"
                              "May be due to invalid ApProfileId\r\n"));
                return OSIX_FAILURE;
            }

            pVendorPayload->isOptional = OSIX_TRUE;
            pVendorPayload->elementId = VENDOR_LOCAL_ROUTING_TYPE;
            pVendorPayload->u2MsgEleType =
                CAPWAP_VEND_SPECIFIC_PAYLOAD_MSG_TYPE;
            pVendorPayload->u2MsgEleLen = 9;
            pVendorPayload->unVendorSpec.VendLocalRouting.isOptional =
                OSIX_TRUE;
            pVendorPayload->unVendorSpec.VendLocalRouting.u2MsgEleType =
                LOCAL_ROUTING_VENDOR_MSG;
            pVendorPayload->unVendorSpec.VendLocalRouting.u2MsgEleLen = 5;
            pVendorPayload->unVendorSpec.VendLocalRouting.vendorId = VENDOR_ID;
            pVendorPayload->unVendorSpec.VendLocalRouting.u1WtpLocalRouting =
                pWssIfCapwapDB->CapwapGetDB.u1WtpLocalRouting;

            *pMsgLen +=
                (UINT4) (pVendorPayload->u2MsgEleLen +
                         CAPWAP_MSG_ELEM_TYPE_LEN);

            break;
        default:
            break;
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : CapwapGetConfigStatVendorPayload                           *
 * Description  : Receives the CAPWAP packet and get the config stat vendor  *
 *                payload                                                    *
 * Input        : pVendorPayload, pMsgLen,u1Index                            *
 * Output       : None                                                       *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *****************************************************************************/
INT4
CapwapGetConfigStatVendorPayload (tVendorSpecPayload * pVendorPayload,
                                  UINT4 *pMsgLen, UINT1 u1Index)
{
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tWssWlanMsgStruct  *pWssWlanMsgStruct = NULL;
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    pWssWlanMsgStruct =
        (tWssWlanMsgStruct *) (VOID *) UtlShMemAllocWssWlanBuf ();
    if (pWssWlanMsgStruct == NULL)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapGetConfigStatVendorPayload:- "
                    "UtlShMemAllocWssWlanBuf returned failure\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "CapwapGetConfigStatVendorPayload:- "
                      "UtlShMemAllocWssWlanBuf returned failure\n"));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    MEMSET (pWssWlanMsgStruct, 0, sizeof (tWssWlanMsgStruct));

    UNUSED_PARAM (pMsgLen);

    CAPWAP_FN_ENTRY ();

    switch (u1Index)
    {
        case VENDOR_DISC_TYPE:
            pVendorPayload->u2MsgEleType = VENDOR_SPECIFIC_PAYLOAD;
            pVendorPayload->u2MsgEleLen = VENDOR_DISC_TYPE_MSG_ELM_LEN;
            pVendorPayload->isOptional = OSIX_TRUE;

            pWssIfCapwapDB->CapwapIsGetAllDB.bWtpDiscType = OSIX_TRUE;

            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
                == OSIX_SUCCESS)
            {
                pVendorPayload->unVendorSpec.VendDiscType.u2MsgEleType
                    = DISCOVERY_TYPE_VENDOR_MSG;
                pVendorPayload->unVendorSpec.VendDiscType.u2MsgEleLen
                    = DISCOVERY_TYPE_VENDOR_MSG_LEN;
                pVendorPayload->unVendorSpec.VendDiscType.vendorId = VENDOR_ID;
                pVendorPayload->unVendorSpec.VendDiscType.u1Val
                    = pWssIfCapwapDB->CapwapGetDB.u1WtpDiscType;
                pVendorPayload->unVendorSpec.VendDiscType.isOptional =
                    OSIX_TRUE;
            }
            break;
        case VENDOR_MGMT_SSID_TYPE:
            pVendorPayload->u2MsgEleType = VENDOR_SPECIFIC_PAYLOAD;
            pVendorPayload->u2MsgEleLen = VENDOR_MGMT_SSID_TYPE_MSG_ELM_LEN;
            pVendorPayload->isOptional = OSIX_TRUE;

            if (WssWlanProcessWssIfMsg (WSS_WLAN_GET_MGMT_SSID,
                                        pWssWlanMsgStruct) == OSIX_SUCCESS)
            {
                pVendorPayload->unVendorSpec.VendSsid.u2MsgEleType
                    = MGMT_SSID_VENDOR_MSG;
                pVendorPayload->unVendorSpec.VendSsid.u2MsgEleLen
                    = MGMT_SSID_VENDOR_MSG_LEN;
                pVendorPayload->unVendorSpec.VendSsid.vendorId = VENDOR_ID;
                MEMCPY (pVendorPayload->unVendorSpec.VendSsid.au1ManagmentSSID,
                        pWssWlanMsgStruct->unWssWlanMsg.au1ManagmentSSID,
                        WSSWLAN_SSID_NAME_LEN);
                pVendorPayload->unVendorSpec.VendSsid.isOptional = OSIX_TRUE;

            }
            break;
        default:
            break;
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;

}

/*****************************************************************************
 * Function     : CapwapGetACName                                            *
 * Description  : Receives the CAPWAP packet and get the AC name             *
 * Input        : pWlcName, pMsgLen                                          *
 * Output       : None                                                       *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *****************************************************************************/
INT4
CapwapGetACName (tACName * pWlcName, UINT4 *pMsgLen)
{
    tWssIfCapDB        *pWssIfCapwapDB = NULL;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    pWssIfCapwapDB->CapwapIsGetAllDB.bAcName = OSIX_TRUE;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
        == OSIX_SUCCESS)
    {
        pWlcName->u2MsgEleType = AC_NAME;
        pWlcName->u2MsgEleLen = (UINT2) (STRLEN (pWssIfCapwapDB->au1WlcName));
        MEMCPY (pWlcName->wlcName, pWssIfCapwapDB->au1WlcName,
                pWlcName->u2MsgEleLen);
        *pMsgLen += (UINT4) (pWlcName->u2MsgEleLen + CAPWAP_MSG_ELEM_TYPE_LEN);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_SUCCESS;
    }
    else
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to Get AC Name from CAPWAP HDLR module \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to Get AC Name from CAPWAP HDLR module \r\n"));
        return OSIX_FAILURE;
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : CapwapGetCapwapTimers                                      *
 * Description  : Receives the CAPWAP packet and get the CAPWAP timers       *
 * Input        : pWlcName, pMsgLen                                          *
 * Output       : None                                                       *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *****************************************************************************/
INT4
CapwapGetCapwapTimers (tCapwapTimer * pCapwapTimer, UINT4 *pMsgLen,
                       UINT2 u2IntProfileId)
{
    tWssIfCapDB        *pWssIfCapwapDB = NULL;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpMaxDiscoveryInterval = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpEchoInterval = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2IntProfileId;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
        == OSIX_SUCCESS)
    {
        pCapwapTimer->u2MsgEleType = CAPWAP_TIMERS;
        pCapwapTimer->u1Discovery =
            (UINT1) (pWssIfCapwapDB->CapwapGetDB.u2WtpMaxDiscoveryInterval);
        pCapwapTimer->u1EchoRequest
            = (UINT1) (pWssIfCapwapDB->CapwapGetDB.u2WtpEchoInterval);
        pCapwapTimer->u2MsgEleLen = CAPWAP_TIMERS_LEN;
        *pMsgLen +=
            (UINT4) (pCapwapTimer->u2MsgEleLen + CAPWAP_MSG_ELEM_TYPE_LEN);
        pCapwapTimer->isOptional = OSIX_TRUE;
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_SUCCESS;
    }
    else
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Get Capwap Timers \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to Get Capwap Timers \r\n"));
        return OSIX_FAILURE;
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : CapwapGetWtpStaticIpInfo                                   *
 * Description  : Receives the CAPWAP packet and get WTP static IP           *
 * Input        : pWtpStaticIp, pMsgLen                                      *
 * Output       : None                                                       *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *****************************************************************************/
INT4
CapwapGetWtpStaticIpInfo (tWtpStaticIpAddr * pWtpStaticIp, UINT4 *pMsgLen)
{

    UNUSED_PARAM (pMsgLen);
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT2               u2IntId = 0;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpStaticIpEnable = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpStaticIpType = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpStaticIpAddress = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpNetmask = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpGateway = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2IntId;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
        == OSIX_SUCCESS)
    {
        pWtpStaticIp->u2MsgEleType = AC_NAME_WITH_PRIO;
        pWtpStaticIp->u2MsgEleLen = AC_NAME_WITH_PRIO_MIN_LEN;
        pWtpStaticIp->isOptional = OSIX_FALSE;
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_SUCCESS;
    }
    else
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to Get Static IP Info State \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to Get Static IP Info State \r\n"));
        return OSIX_FAILURE;
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : CapwapGetWtpFallback                                       *
 * Description  : Receives the CAPWAP packet and get WTP fallback            *
 * Input        : pWtpStaticIp, pMsgLen                                      *
 * Output       : None                                                       *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *****************************************************************************/
INT4
CapwapGetWtpFallback (tWtpFallback * pWtpFallback, UINT4 *pMsgLen,
                      UINT2 u2IntProfileId)
{
    tWssIfCapDB        *pWssIfCapwapDB = NULL;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpFallbackEnable = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2IntProfileId;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
        == OSIX_SUCCESS)
    {
        pWtpFallback->u2MsgEleType = WTP_FALLBACK;
        pWtpFallback->mode = pWssIfCapwapDB->CapwapGetDB.u1WtpFallbackEnable;
        pWtpFallback->u2MsgEleLen = WTP_FALLBACK_LEN;
        *pMsgLen +=
            (UINT4) (pWtpFallback->u2MsgEleLen + CAPWAP_MSG_ELEM_TYPE_LEN);
        pWtpFallback->isOptional = OSIX_TRUE;
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_SUCCESS;
    }
    else
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Get Wtp Fallback \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to Get Wtp Fallback \r\n"));
        return OSIX_FAILURE;
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : CapwapGetIdleTimeout                                       *
 * Description  : Receives the CAPWAP packet and get idle timeout val        *
 * Input        : pIdleTimeout, pMsgLen, u2IntProfileId                      *
 * Output       : None                                                       *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *****************************************************************************/
INT4
CapwapGetIdleTimeout (tIdleTimeout * pIdleTimeout, UINT4 *pMsgLen,
                      UINT2 u2IntProfileId)
{
    tWssIfCapDB        *pWssIfCapwapDB = NULL;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpIdleTimeout = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;

    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2IntProfileId;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
        == OSIX_SUCCESS)
    {
        pIdleTimeout->u2MsgEleType = IDLE_TIMEOUT;
        pIdleTimeout->u4Timeout = pWssIfCapwapDB->CapwapGetDB.u4WtpIdleTimeout;
        pIdleTimeout->u2MsgEleLen = IDLE_TIMEOUT_LEN;
        *pMsgLen +=
            (UINT4) (pIdleTimeout->u2MsgEleLen + CAPWAP_MSG_ELEM_TYPE_LEN);
        pIdleTimeout->isOptional = OSIX_TRUE;
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_SUCCESS;
    }
    else
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Get Idle Timer out \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to Get Idle Timer out \r\n"));
        return OSIX_FAILURE;
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : CapwapGetPriAcNameWithPrioList                             *
 * Description  : Receives the CAPWAP packet and get AC name with priority   *
 * Input        : pAcNameWithPriolist, pMsgLen, u2IntProfileId               *
 * Output       : None                                                       *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *****************************************************************************/
INT4
CapwapGetPriAcNameWithPrioList (tACNameWithPrio * pAcNameWithPriolist,
                                UINT4 *pMsgLen, UINT2 u2IntProfileId)
{

    UNUSED_PARAM (u2IntProfileId);
    tCapwapCapwapBaseAcNameListEntry *pCapwapCapwapBaseAcNameListEntry = NULL;

    pCapwapCapwapBaseAcNameListEntry =
        (tCapwapCapwapBaseAcNameListEntry *) RBTreeGetFirst (gCapwapGlobals.
                                                             CapwapGlbMib.
                                                             CapwapBaseAcNameListTable);

    while (pCapwapCapwapBaseAcNameListEntry != NULL)
    {
        if (pCapwapCapwapBaseAcNameListEntry->MibObject.
            u4CapwapBaseAcNameListPriority != OSIX_FALSE)
        {
            pAcNameWithPriolist->isOptional = OSIX_TRUE;
            pAcNameWithPriolist->u2MsgEleType = AC_NAME_WITH_PRIO;
            pAcNameWithPriolist->u2MsgEleLen
                =
                (UINT2) (STRLEN
                         (pCapwapCapwapBaseAcNameListEntry->MibObject.
                          au1CapwapBaseAcNameListName) + 1);
            pAcNameWithPriolist->u1Priority =
                pCapwapCapwapBaseAcNameListEntry->MibObject.
                u4CapwapBaseAcNameListPriority;
            memcpy (&pAcNameWithPriolist->wlcName[0],
                    pCapwapCapwapBaseAcNameListEntry->MibObject.
                    au1CapwapBaseAcNameListName,
                    STRLEN (pCapwapCapwapBaseAcNameListEntry->MibObject.
                            au1CapwapBaseAcNameListName));
            *pMsgLen +=
                (UINT4) (pAcNameWithPriolist->u2MsgEleLen +
                         CAPWAP_MSG_ELEM_TYPE_LEN);

        }
        pAcNameWithPriolist++;

        pCapwapCapwapBaseAcNameListEntry =
            (tCapwapCapwapBaseAcNameListEntry *) RBTreeGetNext (gCapwapGlobals.
                                                                CapwapGlbMib.
                                                                CapwapBaseAcNameListTable,
                                                                (tRBElem *)
                                                                pCapwapCapwapBaseAcNameListEntry,
                                                                NULL);
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : CapwapGetAcIpList                                          *
 * Description  : Receives the CAPWAP packet and get AC IP list              *
 * Input        : pAcNameWithPriolist, pMsgLen, u2IntProfileId               *
 * Output       : None                                                       *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *****************************************************************************/
INT4
CapwapGetAcIpList (tACIplist * pIpList, UINT4 *pMsgLen)
{
    tWssIfWlcHdlrDB     WssIfWlcHdlrDB;
    UINT1               u1NumOfEntries;
    UINT1               u1Index;

    MEMSET (&WssIfWlcHdlrDB, 0, sizeof (tWssIfWlcHdlrDB));

    if (WssIfProcessWlcHdlrDBMsg (WSS_WLCHDLR_GET_AC_IPV6_LIST, &WssIfWlcHdlrDB)
        == OSIX_SUCCESS)
    {
        pIpList->u2MsgEleType = AC_IPv4_LIST;
        u1NumOfEntries = WssIfWlcHdlrDB.u1NumOfIPv6List;
        for (u1Index = 0; u1Index < u1NumOfEntries; u1Index++)
        {
            pIpList->ipAddr[u1Index].u1_addr[0]
                = WssIfWlcHdlrDB.ACIpv6list.ipAddr[u1Index].u1_addr[0];
            pIpList->u2MsgEleLen = (UINT2) (pIpList->u2MsgEleLen + 16);
        }
        *pMsgLen += (UINT4) (pIpList->u2MsgEleLen + 4);
        pIpList->u2Afi = CAPWAP_INET_AFI_IPV6;
        pIpList->isOptional = OSIX_TRUE;
        return OSIX_SUCCESS;
    }
    else if (WssIfProcessWlcHdlrDBMsg (WSS_WLCHDLR_GET_AC_IPV4_LIST,
                                       &WssIfWlcHdlrDB) == OSIX_SUCCESS)
    {
        pIpList->u2MsgEleType = AC_IPv4_LIST;
        u1NumOfEntries = WssIfWlcHdlrDB.u1NumOfIPv4List;
        /* If IP address is present */
        if (u1NumOfEntries != 0)
        {
            /* Copy the Ip address */
            for (u1Index = 0; u1Index < u1NumOfEntries; u1Index++)
            {
                pIpList->ipAddr[u1Index].u4_addr[0]
                    = WssIfWlcHdlrDB.ACIpv4list.ipAddr[u1Index];
                pIpList->u2MsgEleLen =
                    (UINT2) (pIpList->u2MsgEleLen + AC_IPv4_LIST_MIN_LEN);
            }
            *pMsgLen += (UINT4) (pIpList->u2MsgEleLen + AC_IPv4_LIST_MIN_LEN);
            pIpList->u2Afi = CAPWAP_INET_AFI_IPV4;
            pIpList->isOptional = OSIX_TRUE;
        }
        return OSIX_SUCCESS;
    }
    else
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Get Ipv6 List \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to Get Ipv6 List \r\n"));
        return OSIX_SUCCESS;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : CapwapProcessCtrlPacket                                    *
 * Description  : Receives the CAPWAP packet and process the messag          *
 *                if Discovery Request, Discovery response                   *
 * Input        : pBuf - received CAPWAP packet                              *
 * Output       : None                                                       *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *****************************************************************************/
UINT4
CapwapProcessCtrlPacket (tCRU_BUF_CHAIN_HEADER * pBuf)
{

    UINT1               u1Val;
    UINT1              *pRcvBuf = NULL;
    UINT4               u4MsgType, u4DestPort, u4DestIp;
    tCapwapRxQMsg      *pCapwapRxQMsg = NULL;
    unWlcHdlrMsgStruct *pWlcHdlrMsg = NULL;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tRemoteSessionManager *pSessEntry = NULL;
    UINT1               au1Frame[CAPWAP_HDR_LEN + CAPWAP_CONTROL_HDR_LEN];
    UINT1               u1HdrLen = CAPWAP_HDR_LEN + CAPWAP_CONTROL_HDR_LEN;
    tQueData            QueData;
    UINT2               u2PacketLen;
    UINT4               u4DtlsSessId = 0;
    tCapwapDtlsSessEntry *pDtlsEntry = NULL;

    CAPWAP_FN_ENTRY ();
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    pWlcHdlrMsg = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pWlcHdlrMsg == NULL)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapProcessCtrlPacket:- "
                    "UtlShMemAllocWlcBuf returned failure\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "CapwapProcessCtrlPacket:- "
                      "UtlShMemAllocWlcBuf returned failure\n"));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_RELEASE_CRU_BUF (pBuf);
        return OSIX_FAILURE;
    }

    MEMSET (&QueData, 0, sizeof (tQueData));

    pRcvBuf = CAPWAP_BUF_IF_LINEAR (pBuf, 0, u1HdrLen);
    if (pRcvBuf == NULL)
    {
        /* Header is not linear in CRU buffer */
        MEMSET (au1Frame, 0x00, u1HdrLen);
        CAPWAP_COPY_FROM_BUF (pBuf, au1Frame, 0, u1HdrLen);
        pRcvBuf = au1Frame;
    }

    u4DestPort = CRU_BUF_Get_U4Reserved1 (pBuf);

    u4DestIp = CRU_BUF_Get_U4Reserved2 (pBuf);

    /* Parse the received packet to check the packet type */
    CAPWAP_GET_1BYTE (u1Val, pRcvBuf);
    /* Move the buffer pointer to the original position */
    pRcvBuf -= 1;
    if ((u1Val & 0x0F) == CAPWAP_PREAMBLE_DTLS_TYPE)
    {
        pWssIfCapwapDB->u4DestIp = u4DestIp;
        pWssIfCapwapDB->u4DestPort = u4DestPort;
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_RSM, pWssIfCapwapDB)
            == OSIX_FAILURE)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Retrive CAPWAP DB \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to Retrive CAPWAP DB \r\n"));
            CAPWAP_RELEASE_CRU_BUF (pBuf);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsg);
            return OSIX_FAILURE;
        }
        pSessEntry = pWssIfCapwapDB->pSessEntry;
        if (pSessEntry == NULL)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Entry not present in remote session table Try to Get from DTLS Temp Table \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Entry not present in remote session table Try to Get from DTLS Temp Table \r\n"));
            pWssIfCapwapDB->u4DestIp = u4DestIp;
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_TEMPDTLS_ENTRY,
                                         pWssIfCapwapDB) == OSIX_FAILURE)
            {
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Failed to Get the entry from Temporary DTLS table \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to Get the entry from Temporary DTLS table \r\n"));
                CAPWAP_RELEASE_CRU_BUF (pBuf);
                UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsg);
                return OSIX_FAILURE;
            }
            pDtlsEntry = pWssIfCapwapDB->pDtlsEntry;
            if (pDtlsEntry != NULL)
            {
                u4DtlsSessId = pDtlsEntry->u4DtlsId;
            }
        }
        else
        {
            u4DtlsSessId = pSessEntry->u4DtlsSessId;
        }
        /* Remove the CAPWAP DTLS header */
        /* Move the buffer offset by 4 Bytes */
        CRU_BUF_Move_ValidOffset (pBuf, CAPWAP_DTLS_HDR_LEN);
        u2PacketLen = (UINT2) CAPWAP_GET_BUF_LEN (pBuf);
        QueData.eQueDataType = DATA_DECRYPT;
        QueData.pBuffer = pBuf;
        QueData.u4Id = u4DtlsSessId;
        QueData.u4IpAddr = u4DestIp;
        QueData.u4PortNo = u4DestPort;
        QueData.u4BufSize = u2PacketLen;
        QueData.u4Channel = DTLS_CTRL_CHANNEL;

        if (CapwapDtlsDecryptNotify (&QueData) == OSIX_FAILURE)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to Decrypt the Received packet \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to Decrypt the Received packet \r\n"));
            CAPWAP_RELEASE_CRU_BUF (pBuf);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsg);
            return OSIX_FAILURE;
        }
        if (pSessEntry != NULL)
        {
            pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
                pSessEntry->u2IntProfileId;
            pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
            pWssIfCapwapDB->CapwapIsGetAllDB.bWtpCtrlDTLSCount = OSIX_TRUE;
            WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB);
            (pWssIfCapwapDB->CapwapGetDB.u4WtpCtrlDTLSCount)++;
            WssIfProcessCapwapDBMsg (WSS_CAPWAP_SET_DB, pWssIfCapwapDB);
        }
        CAPWAP_RELEASE_CRU_BUF (pBuf);
    }
    else if ((u1Val & 0x0F) == CAPWAP_PREAMBLE_PLAINTEXT_TYPE)
    {
        pCapwapRxQMsg = (tCapwapRxQMsg *) MemAllocMemBlk (CAPWAP_QUEMSG_POOLID);

        if (pCapwapRxQMsg == NULL)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "CapwapProcessCtrlPacket:Memory Allocation Failed \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "CapwapProcessCtrlPacket:Memory Allocation Failed \r\n"));
            CAPWAP_RELEASE_CRU_BUF (pBuf);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsg);
            return OSIX_FAILURE;
        }
        MEMSET (pCapwapRxQMsg, 0, sizeof (tCapwapRxQMsg));

        /* If the discovery message received as fragment first it will be
           send to CAPWAP module. The CAPWAP module will reassemble the packet
           and sends to discovery thread for furhter processing */
        /* If the CAPWAP control message (ex WLAN Configuration request,
           configuration udpated requests etc)received as fragment it will
           be send to CAPWAP module for reassembly. CAPWAP module reassemble
           the packet and sends to AP HDLR or WLC HDLR for for further processing */
        /* To Limit the reassembly logic to one module its decided to reassemble only
           at CAPWAP module */
        if (CapwapFragmentReceived (pBuf) == OSIX_SUCCESS)
        {
            pCapwapRxQMsg->u4PktType = CAPWAP_CTRL_FRAG_MSG;
            pCapwapRxQMsg->pRcvBuf = pBuf;
            pCapwapRxQMsg->capwapSessionId.u4_addr[0] = u4DestIp;
            CapwapEnqueCtrlPkts (pCapwapRxQMsg);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsg);
            return OSIX_SUCCESS;
        }
        CapwapGetMsgTypeFromBuf (pRcvBuf, &u4MsgType);
        if ((u4MsgType < MIN_CAPWAP_MSG_TYPE) || (u4MsgType > WLAN_CONF_RSP))
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "capwapProcessCtrlPacket: Received unknown MsgType \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "capwapProcessCtrlPacket: Received unknown MsgType \r\n"));
            MemReleaseMemBlock (CAPWAP_QUEMSG_POOLID, (UINT1 *) pCapwapRxQMsg);
            CAPWAP_RELEASE_CRU_BUF (pBuf);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsg);
            return OSIX_FAILURE;
        }
        else if ((u4MsgType == CAPWAP_DISC_REQ)
                 || (u4MsgType == CAPWAP_DISC_RSP)
                 || (u4MsgType == CAPWAP_PRIMARY_DISC_REQ))
        {
            pCapwapRxQMsg->u4PktType = CAPWAP_DISC_MSG;
            pCapwapRxQMsg->pRcvBuf = pBuf;
            pCapwapRxQMsg->capwapSessionId.u4_addr[0] = u4DestIp;
            CapwapEnqueDiscPkts (pCapwapRxQMsg);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsg);
            return OSIX_SUCCESS;
        }
        else if ((u4MsgType == CAPWAP_JOIN_REQ)
                 || (u4MsgType == CAPWAP_JOIN_RSP)
                 || (u4MsgType == CAPWAP_CONF_STAT_REQ)
                 || (u4MsgType == CAPWAP_CONF_STAT_RSP)
                 || (u4MsgType == CAPWAP_CHANGE_STAT_REQ)
                 || (u4MsgType == CAPWAP_CHANGE_STAT_RSP))
        {
            pCapwapRxQMsg->u4PktType = CAPWAP_FSM_MSG;
            pCapwapRxQMsg->pRcvBuf = pBuf;
            pCapwapRxQMsg->capwapSessionId.u4_addr[0] = u4DestIp;
            CapwapEnqueCtrlPkts (pCapwapRxQMsg);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsg);
            return OSIX_SUCCESS;
        }
        else if ((u4MsgType == CAPWAP_ECHO_REQ)
                 || (u4MsgType == CAPWAP_ECHO_RSP))
        {
            pCapwapRxQMsg->u4PktType = CAPWAP_ECHO_ALIVE_MSG;
            pCapwapRxQMsg->pRcvBuf = pBuf;
            pCapwapRxQMsg->capwapSessionId.u4_addr[0] = u4DestIp;
            CapwapEnqueCtrlPkts (pCapwapRxQMsg);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsg);
            return OSIX_SUCCESS;
        }
        else if ((u4MsgType == CAPWAP_CONF_UPD_REQ) ||
                 (u4MsgType == CAPWAP_WTP_EVENT_REQ) ||
                 (u4MsgType == CAPWAP_IMAGE_DATA_REQ) ||
                 (u4MsgType == CAPWAP_IMAGE_DATA_RSP) ||
                 (u4MsgType == CAPWAP_RESET_RSP) ||
                 (u4MsgType == CAPWAP_DATA_TRANS_REQ) ||
                 (u4MsgType == CAPWAP_DATA_TRANS_RSP) ||
                 (u4MsgType == CAPWAP_CLEAR_CONF_RSP) ||
                 (u4MsgType == CAPWAP_STATION_CONF_RSP) ||
                 (u4MsgType == CAPWAP_CONF_UPD_RSP) ||
                 (u4MsgType == WLAN_CONF_RSP) ||
                 (u4MsgType == CAPWAP_PRIMARY_DISC_REQ))
        {
            /* These packets should go to WLC HDLR module */
            /* pCapwapRxQMsg->u4PktType = WLCHDLR_CTRL_MSG;
               pCapwapRxQMsg->pRcvBuf = pBuf;
               pCapwapRxQMsg->capwapSessionId.u4_addr[0] = u4DestIp; */
            pWlcHdlrMsg->WlcHdlrQueueReq.pRcvBuf = pBuf;
            pWlcHdlrMsg->WlcHdlrQueueReq.u4MsgType = WLCHDLR_CTRL_MSG;
            if (WssIfProcessWlcHdlrMsg
                (WSS_WLCHDLR_CAPWAP_QUEUE_REQ, pWlcHdlrMsg) == OSIX_FAILURE)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Failed to send the packet to the WLC HDLR \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to send the packet to the WLC HDLR \r\n"));
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsg);
                return OSIX_FAILURE;
            }
            MemReleaseMemBlock (CAPWAP_QUEMSG_POOLID, (UINT1 *) pCapwapRxQMsg);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsg);
            return OSIX_SUCCESS;
        }
        else if ((u4MsgType % 2) == 1)
        {
            /* If the Received Message Type is odd number send the response with 
             * unrecognized request return code*/
            pCapwapRxQMsg->u4PktType = CAPWAP_FSM_MSG;
            pCapwapRxQMsg->pRcvBuf = pBuf;
            pCapwapRxQMsg->capwapSessionId.u4_addr[0] = u4DestIp;
            CapwapEnqueCtrlPkts (pCapwapRxQMsg);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsg);
            return OSIX_SUCCESS;
        }
        else if ((u4MsgType % 2) == 0)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Received the unknown message type \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Received the unknown message type \r\n"));
            MemReleaseMemBlock (CAPWAP_QUEMSG_POOLID, (UINT1 *) pCapwapRxQMsg);
            CAPWAP_RELEASE_CRU_BUF (pBuf);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsg);
            return OSIX_FAILURE;
        }
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    CAPWAP_FN_EXIT ();
    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsg);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : CapwapProcessReassemCtrlPacket                             *
 * Description  : Receives the CAPWAP packet and process the messag          *
 *                if Discovery Request, Discovery response                   *
 * Input        : pBuf - received CAPWAP packet                              *
 * Output       : None                                                       *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *****************************************************************************/
UINT4
CapwapProcessReassemCtrlPacket (tCRU_BUF_CHAIN_HEADER * pBuf)
{

    UINT1               u1Val;
    UINT1              *pRcvBuf = NULL;
    UINT4               u4MsgType, u4DestPort, u4DestIp;
    tCapwapRxQMsg      *pCapwapRxQMsg = NULL;
    unWlcHdlrMsgStruct *pWlcHdlrMsg = NULL;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tRemoteSessionManager *pSessEntry = NULL;
    UINT1               au1Frame[CAPWAP_HDR_LEN + CAPWAP_CONTROL_HDR_LEN];
    UINT1               u1HdrLen = CAPWAP_HDR_LEN + CAPWAP_CONTROL_HDR_LEN;
    tQueData            QueData;
    UINT2               u2PacketLen;
    UINT4               u4DtlsSessId = 0;
    tCapwapDtlsSessEntry *pDtlsEntry = NULL;

    CAPWAP_FN_ENTRY ();
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    pWlcHdlrMsg = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pWlcHdlrMsg == NULL)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapProcessReassemCtrlPacket:- "
                    "UtlShMemAllocWlcBuf returned failure\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "CapwapProcessReassemCtrlPacket:- "
                      "UtlShMemAllocWlcBuf returned failure\n"));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    MEMSET (&QueData, 0, sizeof (tQueData));

    pRcvBuf = CAPWAP_BUF_IF_LINEAR (pBuf, 0, u1HdrLen);
    if (pRcvBuf == NULL)
    {
        /* Header is not linear in CRU buffer */
        MEMSET (au1Frame, 0x00, u1HdrLen);
        CAPWAP_COPY_FROM_BUF (pBuf, au1Frame, 0, u1HdrLen);
        pRcvBuf = au1Frame;
    }

    u4DestPort = CRU_BUF_Get_U4Reserved1 (pBuf);

    u4DestIp = CRU_BUF_Get_U4Reserved2 (pBuf);

    /* Parse the received packet to check the packet type */
    CAPWAP_GET_1BYTE (u1Val, pRcvBuf);
    /* Move the buffer pointer to the original position */
    pRcvBuf -= 1;
    if ((u1Val & 0x0F) == CAPWAP_PREAMBLE_DTLS_TYPE)
    {
        pWssIfCapwapDB->u4DestIp = u4DestIp;
        pWssIfCapwapDB->u4DestPort = u4DestPort;
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_RSM, pWssIfCapwapDB) ==
            OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Retrive CAPWAP DB \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to Retrive CAPWAP DB \r\n"));
            CAPWAP_RELEASE_CRU_BUF (pBuf);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsg);
            return OSIX_FAILURE;
        }
        pSessEntry = pWssIfCapwapDB->pSessEntry;
        if (pSessEntry == NULL)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Entry not present in remote session table Try to Get from DTLS Temp Table \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Entry not present in remote session table Try to Get from DTLS Temp Table \r\n"));
            pWssIfCapwapDB->u4DestIp = u4DestIp;
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_TEMPDTLS_ENTRY,
                                         pWssIfCapwapDB) == OSIX_FAILURE)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Failed to Get the entry from Temporary DTLS table \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to Get the entry from Temporary DTLS table \r\n"));
                CAPWAP_RELEASE_CRU_BUF (pBuf);
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsg);
                return OSIX_FAILURE;
            }
            pDtlsEntry = pWssIfCapwapDB->pDtlsEntry;
            if (pDtlsEntry != NULL)
            {
                u4DtlsSessId = pDtlsEntry->u4DtlsId;
            }
        }
        else
        {
            u4DtlsSessId = pSessEntry->u4DtlsSessId;
        }
        /* Remove the CAPWAP DTLS header */
        /* Move the buffer offset by 4 Bytes */
        CRU_BUF_Move_ValidOffset (pBuf, CAPWAP_DTLS_HDR_LEN);
        u2PacketLen = (UINT2) CAPWAP_GET_BUF_LEN (pBuf);
        QueData.eQueDataType = DATA_DECRYPT;
        QueData.pBuffer = pBuf;
        QueData.u4Id = u4DtlsSessId;
        QueData.u4IpAddr = u4DestIp;
        QueData.u4PortNo = u4DestPort;
        QueData.u4BufSize = u2PacketLen;
        QueData.u4Channel = DTLS_CTRL_CHANNEL;

        if (CapwapDtlsDecryptNotify (&QueData) == OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to Decrypt the Received packet \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to Decrypt the Received packet \r\n"));
            CAPWAP_RELEASE_CRU_BUF (pBuf);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsg);
            return OSIX_FAILURE;
        }
        CAPWAP_RELEASE_CRU_BUF (pBuf);
    }
    else if ((u1Val & 0x0F) == CAPWAP_PREAMBLE_PLAINTEXT_TYPE)
    {
        pCapwapRxQMsg = (tCapwapRxQMsg *) MemAllocMemBlk (CAPWAP_QUEMSG_POOLID);

        if (pCapwapRxQMsg == NULL)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "CapwapProcessCtrlPacket:Memory Allocation Failed \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "CapwapProcessCtrlPacket:Memory Allocation Failed \r\n"));
            CAPWAP_RELEASE_CRU_BUF (pBuf);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsg);
            return OSIX_FAILURE;
        }
        MEMSET (pCapwapRxQMsg, 0, sizeof (tCapwapRxQMsg));

        /* If the discovery message received as fragment first it will be
           send to CAPWAP module. The CAPWAP module will reassemble the packet
           and sends to discovery thread for furhter processing */
        /* If the CAPWAP control message (ex WLAN Configuration request,
           configuration udpated requests etc)received as fragment it will
           be send to CAPWAP module for reassembly. CAPWAP module reassemble
           the packet and sends to AP HDLR or WLC HDLR for for further processing */
        /* To Limit the reassembly logic to one module its decided to reassemble only
           at CAPWAP module */
        CapwapGetMsgTypeFromBuf (pRcvBuf, &u4MsgType);
        if ((u4MsgType < MIN_CAPWAP_MSG_TYPE) || (u4MsgType > WLAN_CONF_RSP))
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "capwapProcessCtrlPacket: Received unknown MsgType \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "capwapProcessCtrlPacket: Received unknown MsgType \r\n"));
            MemReleaseMemBlock (CAPWAP_QUEMSG_POOLID, (UINT1 *) pCapwapRxQMsg);
            CAPWAP_RELEASE_CRU_BUF (pBuf);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsg);
            return OSIX_FAILURE;
        }
        else if ((u4MsgType == CAPWAP_DISC_REQ)
                 || (u4MsgType == CAPWAP_DISC_RSP))
        {
            pCapwapRxQMsg->u4PktType = CAPWAP_DISC_MSG;
            pCapwapRxQMsg->pRcvBuf = pBuf;
            pCapwapRxQMsg->capwapSessionId.u4_addr[0] = u4DestIp;
            CapwapEnqueDiscPkts (pCapwapRxQMsg);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsg);
            return OSIX_SUCCESS;
        }
        else if ((u4MsgType == CAPWAP_JOIN_REQ)
                 || (u4MsgType == CAPWAP_JOIN_RSP)
                 || (u4MsgType == CAPWAP_CONF_STAT_REQ)
                 || (u4MsgType == CAPWAP_CONF_STAT_RSP)
                 || (u4MsgType == CAPWAP_CHANGE_STAT_REQ)
                 || (u4MsgType == CAPWAP_CHANGE_STAT_RSP))
        {
            pCapwapRxQMsg->u4PktType = CAPWAP_FSM_MSG;
            pCapwapRxQMsg->pRcvBuf = pBuf;
            pCapwapRxQMsg->capwapSessionId.u4_addr[0] = u4DestIp;
            CapwapEnqueCtrlPkts (pCapwapRxQMsg);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsg);
            return OSIX_SUCCESS;
        }
        else if ((u4MsgType == CAPWAP_ECHO_REQ)
                 || (u4MsgType == CAPWAP_ECHO_RSP))
        {
            pCapwapRxQMsg->u4PktType = CAPWAP_ECHO_ALIVE_MSG;
            pCapwapRxQMsg->pRcvBuf = pBuf;
            pCapwapRxQMsg->capwapSessionId.u4_addr[0] = u4DestIp;
            CapwapEnqueCtrlPkts (pCapwapRxQMsg);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsg);
            return OSIX_SUCCESS;
        }
        else if ((u4MsgType == CAPWAP_CONF_UPD_REQ) ||
                 (u4MsgType == CAPWAP_WTP_EVENT_REQ) ||
                 (u4MsgType == CAPWAP_IMAGE_DATA_REQ) ||
                 (u4MsgType == CAPWAP_IMAGE_DATA_RSP) ||
                 (u4MsgType == CAPWAP_RESET_RSP) ||
                 (u4MsgType == CAPWAP_DATA_TRANS_REQ) ||
                 (u4MsgType == CAPWAP_DATA_TRANS_RSP) ||
                 (u4MsgType == CAPWAP_CLEAR_CONF_RSP) ||
                 (u4MsgType == CAPWAP_STATION_CONF_RSP) ||
                 (u4MsgType == CAPWAP_CONF_UPD_RSP) ||
                 (u4MsgType == WLAN_CONF_RSP) ||
                 (u4MsgType == CAPWAP_PRIMARY_DISC_REQ))
        {
            /* These packets should go to WLC HDLR module */
            /* pCapwapRxQMsg->u4PktType = WLCHDLR_CTRL_MSG;
               pCapwapRxQMsg->pRcvBuf = pBuf;
               pCapwapRxQMsg->capwapSessionId.u4_addr[0] = u4DestIp; */
            pWlcHdlrMsg->WlcHdlrQueueReq.pRcvBuf = pBuf;
            pWlcHdlrMsg->WlcHdlrQueueReq.u4MsgType = WLCHDLR_CTRL_MSG;
            if (WssIfProcessWlcHdlrMsg
                (WSS_WLCHDLR_CAPWAP_QUEUE_REQ, pWlcHdlrMsg) == OSIX_FAILURE)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Failed to send the packet to the WLC HDLR \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to send the packet to the WLC HDLR \r\n"));
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsg);
                return OSIX_FAILURE;
            }
            MemReleaseMemBlock (CAPWAP_QUEMSG_POOLID, (UINT1 *) pCapwapRxQMsg);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsg);
            return OSIX_SUCCESS;
        }
        else if ((u4MsgType % 2) == 1)
        {
            /* If the Received Message Type is odd number send the response with 
             * unrecognized request return code*/
            pCapwapRxQMsg->u4PktType = CAPWAP_FSM_MSG;
            pCapwapRxQMsg->pRcvBuf = pBuf;
            pCapwapRxQMsg->capwapSessionId.u4_addr[0] = u4DestIp;
            CapwapEnqueCtrlPkts (pCapwapRxQMsg);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsg);
            return OSIX_SUCCESS;
        }
        else if ((u4MsgType % 2) == 0)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Received the unknown message type \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Received the unknown message type \r\n"));
            MemReleaseMemBlock (CAPWAP_QUEMSG_POOLID, (UINT1 *) pCapwapRxQMsg);
            CAPWAP_RELEASE_CRU_BUF (pBuf);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsg);
            return OSIX_FAILURE;
        }
    }
    CAPWAP_FN_EXIT ();
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsg);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : CapwapProcessDataPacket                                    *
 * Description  : Receives the CAPWAP packet and process the msg             *
 * Input        : pRcvBuf - received CAPWAP packet                           *
 * Output       : None                                                       *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *****************************************************************************/
UINT4
CapwapProcessDataPacket (tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UINT1               u1Val, *pRcvBuf = NULL;
    UINT1               au1Frame[CAPWAP_HDR_LEN + CAPWAP_CONTROL_HDR_LEN];
    UINT1               u1HdrLen = CAPWAP_HDR_LEN + CAPWAP_CONTROL_HDR_LEN;
    UINT2               u2PacketLen;
    UINT4               u4DtlsSessId = 0;
    tMacAddr            McastBssId = { 0xff, 0xff, 0xff, 0xff, 0xff, 0xff };
    UINT4               u4DestPort = 0;
    UINT4               u4DestIp = 0;
    tQueData            QueData;
    tCapwapDtlsSessEntry *pDtlsEntry = NULL;
    tMacAddr            BssId;
    tCapwapRxQMsg      *pCapwapRxQMsg = NULL;
    unWlcHdlrMsgStruct *pWlcHdlrMsg = NULL;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tRemoteSessionManager *pSessEntry = NULL;
    tCapwapSessDestIpPortMapTable *pSessDestIpEntry = NULL;
    tWssWlanDB          wssWlanDB;
    UINT4               u4ApGroupEnabled = 0;

    CAPWAP_FN_ENTRY ();
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    pWlcHdlrMsg = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pWlcHdlrMsg == NULL)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapProcessDataPacket:- "
                    "UtlShMemAllocWlcBuf returned failure\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "CapwapProcessDataPacket:- "
                      "UtlShMemAllocWlcBuf returned failure\n"));
        CAPWAP_RELEASE_CRU_BUF (pBuf);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    MEMSET (pWlcHdlrMsg, 0, sizeof (unWlcHdlrMsgStruct));

    pRcvBuf = CAPWAP_BUF_IF_LINEAR (pBuf, 0, u1HdrLen);
    if (pRcvBuf == NULL)
    {
        /* Header is not linear in CRU buffer */
        MEMSET (au1Frame, 0x00, u1HdrLen);
        CAPWAP_COPY_FROM_BUF (pBuf, au1Frame, 0, u1HdrLen);
        pRcvBuf = au1Frame;
    }

    u4DestPort = CRU_BUF_Get_U4Reserved1 (pBuf);
    u4DestIp = CRU_BUF_Get_U4Reserved2 (pBuf);

    CAPWAP_GET_1BYTE (u1Val, pRcvBuf);
    pRcvBuf -= 1;

    if ((u1Val & 0x0f) == CAPWAP_PREAMBLE_DTLS_TYPE)
    {
        pWssIfCapwapDB->u4DestIp = u4DestIp;
        pWssIfCapwapDB->u4DestPort = u4DestPort;
        MEMSET (&QueData, 0, sizeof (tQueData));

        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_RSM, pWssIfCapwapDB)
            == OSIX_FAILURE)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "CapwapProcessDataPacket"
                        "Failed to Retrive CAPWAP DB \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "CapwapProcessDataPacket"
                          "Failed to Retrive CAPWAP DB \r\n"));
            CAPWAP_RELEASE_CRU_BUF (pBuf);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsg);
            return OSIX_FAILURE;
        }
        pSessEntry = pWssIfCapwapDB->pSessEntry;
        if (pSessEntry == NULL)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "CapwapProcessDataPacket"
                        "Entry not present in remote session"
                        "table Try to Get from DTLS Temp Table \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "CapwapProcessDataPacket"
                          "Entry not present in remote session"
                          "table Try to Get from DTLS Temp Table \r\n"));
            pWssIfCapwapDB->u4DestIp = u4DestIp;
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_TEMPDTLS_ENTRY,
                                         pWssIfCapwapDB) == OSIX_FAILURE)
            {
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Failed to Get the entry from Temporary DTLS table \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to Get the entry from Temporary DTLS table \r\n"));
                CAPWAP_RELEASE_CRU_BUF (pBuf);
                UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsg);
                return OSIX_FAILURE;
            }
            pDtlsEntry = pWssIfCapwapDB->pDtlsEntry;
            if (pDtlsEntry != NULL)
            {
                u4DtlsSessId = pDtlsEntry->u4DtlsId;
            }
        }
        else
        {
            u4DtlsSessId = pSessEntry->u4DtlsSessId;
        }

        /* Remove the CAPWAP DTLS header */
        /* Move the buffer offset by 4 Bytes */
        CRU_BUF_Move_ValidOffset (pBuf, CAPWAP_DTLS_HDR_LEN);
        u2PacketLen = (UINT2) CAPWAP_GET_BUF_LEN (pBuf);
        QueData.eQueDataType = DATA_DECRYPT;
        QueData.pBuffer = pBuf;
        QueData.u4Id = u4DtlsSessId;
        QueData.u4IpAddr = u4DestIp;
        QueData.u4PortNo = u4DestPort;
        QueData.u4BufSize = u2PacketLen;
        QueData.u4Channel = DTLS_DATA_CHANNEL;

        if (CapwapDtlsDecryptNotify (&QueData) == OSIX_FAILURE)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to Decrypt the Received packet \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to Decrypt the Received packet \r\n"));
            CAPWAP_RELEASE_CRU_BUF (pBuf);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsg);
            return OSIX_FAILURE;
        }
        CAPWAP_RELEASE_CRU_BUF (pBuf);
    }
    else if ((u1Val & 0x0f) == CAPWAP_PREAMBLE_PLAINTEXT_TYPE)
    {
        pCapwapRxQMsg = (tCapwapRxQMsg *) MemAllocMemBlk (CAPWAP_QUEMSG_POOLID);

        if (pCapwapRxQMsg == NULL)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "CapwapProcessDataPacket:Memory Allocation Failed\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "CapwapProcessDataPacket:Memory Allocation Failed\n"));
            CAPWAP_RELEASE_CRU_BUF (pBuf);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsg);
            return OSIX_FAILURE;
        }
        MEMSET (pCapwapRxQMsg, 0, sizeof (tCapwapRxQMsg));

        if ((CapwapKeepAliveReceived (pBuf) == OSIX_SUCCESS))
        {
            /* Data Packet keep alive packets and fragments are handled at CAPWAP service thread.
               CAPWAP Module reassembles the received fragements and send to APHDLR or WLCHDLR
               for further processing */
            pCapwapRxQMsg->u4PktType = CAPWAP_ECHO_ALIVE_MSG;
            pCapwapRxQMsg->pRcvBuf = pBuf;
            pCapwapRxQMsg->capwapSessionId.u4_addr[0]
                = CRU_BUF_Get_U4Reserved2 (pBuf);
            CAPWAP_TRC (CAPWAP_MGMT_TRC,
                        "Send the received packet to the 'DATA Rx Q' to process by service thread \r\n");
            SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                          "Send the received packet to the 'DATA Rx Q' to process by service thread \r\n"));
            CapwapEnqueCtrlPkts (pCapwapRxQMsg);
        }
        else if ((CapwapFragmentReceived (pBuf) == OSIX_SUCCESS))
        {
            /* Data Packet keep alive packets and fragments are handled at CAPWAP
             * service thread.
             * CAPWAP Module reassembles the received fragements and send to
             * APHDLR or WLCHDLR
             * for further processing */
            pCapwapRxQMsg->u4PktType = CAPWAP_DATA_FRAG_MSG;
            pCapwapRxQMsg->pRcvBuf = pBuf;
            pCapwapRxQMsg->capwapSessionId.u4_addr[0]
                = CRU_BUF_Get_U4Reserved2 (pBuf);
            CAPWAP_TRC (CAPWAP_MGMT_TRC,
                        "Send the received packet to the 'DATA Rx Q' to process by service thread \r\n");
            SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                          "Send the received packet to the 'DATA Rx Q' to process by service thread \r\n"));
            CapwapEnqueCtrlPkts (pCapwapRxQMsg);
        }
        else if (CapwapDot3FrameReceived (pBuf) == OSIX_SUCCESS)
        {
            /*The frame is of type 802.3. So the mode should be local 
             * Parse the radio MAC address field, and fetch the WLAN
             * information.
             * Send the packet to the interface based on the VLAN */
            MemReleaseMemBlock (CAPWAP_QUEMSG_POOLID, (UINT1 *) pCapwapRxQMsg);
            CAPWAP_COPY_FROM_BUF (pBuf, BssId, CAPWAP_OPT_MAC_OFFSET,
                                  CAPWAP_OPT_MAC_FIELD_LEN);
            CAPWAP_COPY_FROM_BUF (pBuf, &u1HdrLen, CAPWAP_HDRLEN_OFFSET,
                                  CAPWAP_HDRLEN_FIELD_LEN);
            u1HdrLen = (UINT1) ((u1HdrLen & CAPWAP_HDRLEN_MASK) >> 3);

            u1HdrLen = (UINT1) (u1HdrLen * 4);

            CRU_BUF_Move_ValidOffset (pBuf, u1HdrLen);

            MEMSET (&wssWlanDB, 0, sizeof (tWssWlanDB));
            MEMCPY (wssWlanDB.WssWlanAttributeDB.BssId, BssId,
                    sizeof (tMacAddr));
            wssWlanDB.WssWlanIsPresentDB.bBssIfIndex = OSIX_TRUE;
            wssWlanDB.WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;

            if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSSID_MAPPING_ENTRY,
                                          &wssWlanDB) != OSIX_SUCCESS)
            {
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "WssIfProcessWssWlanDBMsg Failed BssID\n\r");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "WssIfProcessWssWlanDBMsg Failed BssID\n\r"));
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "WssIfProcessWssWlanDBMsg\
                        WlanIfIndex: " "Failed data from wssWlanDB\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "WssIfProcessWssWlanDBMsg\
                        WlanIfIndex: " "Failed data from wssWlanDB\r\n"));
                CAPWAP_RELEASE_CRU_BUF (pBuf);
                UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsg);
                return OSIX_FAILURE;
            }

            wssWlanDB.WssWlanIsPresentDB.bVlanId = OSIX_TRUE;
            if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY,
                                          &wssWlanDB) != OSIX_SUCCESS)
            {
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "WssIfProcessWssWlanDBMsg\
                        VlanId : " "Failed data from wssWlanDB\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "WssIfProcessWssWlanDBMsg\
                        VlanId : " "Failed data from wssWlanDB\r\n"));
                CAPWAP_RELEASE_CRU_BUF (pBuf);
                UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsg);
                return OSIX_FAILURE;
            }
            pWssIfCapwapDB->u4DestIp = CRU_BUF_Get_U4Reserved2 (pBuf);
            pWssIfCapwapDB->u4DestPort = CRU_BUF_Get_U4Reserved1 (pBuf);
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_SESSION_PORT_ENTRY,
                                         pWssIfCapwapDB) != OSIX_SUCCESS)
            {
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Failed to Get the entry from SessionPort\
                        Entry port table \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to Get the entry from SessionPort\
                        Entry port table \r\n"));
                CAPWAP_RELEASE_CRU_BUF (pBuf);
                UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsg);
                return OSIX_FAILURE;
            }
            pSessDestIpEntry = pWssIfCapwapDB->pSessDestIpEntry;
            if (pSessDestIpEntry == NULL)
            {
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Received the data\
                        packet from unknown source \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Received the data\
                        packet from unknown source \r\n"));
                CAPWAP_RELEASE_CRU_BUF (pBuf);
                UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsg);
                return OSIX_FAILURE;
            }
            pWssIfCapwapDB->u2CapwapRSMId = pSessDestIpEntry->u2CapwapRSMIndex;
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_RSM_FROM_SESS_IDX,
                                         pWssIfCapwapDB) != OSIX_SUCCESS)
            {
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Get the \
                        entry from SessionPort Entry port table \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to Get the \
                        entry from SessionPort Entry port table \r\n"));
                CAPWAP_RELEASE_CRU_BUF (pBuf);
                UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsg);
                return OSIX_FAILURE;
            }
            pWssIfCapwapDB->CapwapIsGetAllDB.bMacType = OSIX_TRUE;
            pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
                pWssIfCapwapDB->pSessEntry->u2IntProfileId;
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                         pWssIfCapwapDB) != OSIX_SUCCESS)
            {
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Get \
                        the entry from SessionPort Entry port table \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to Get \
                        the entry from SessionPort Entry port table \r\n"));
                CAPWAP_RELEASE_CRU_BUF (pBuf);
                UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsg);
                return OSIX_FAILURE;
            }
            memcpy (pWlcHdlrMsg->WlcHdlrQueueReq.BssId, BssId,
                    sizeof (tMacAddr));
            pWlcHdlrMsg->WlcHdlrQueueReq.u1MacType = LOCAL_MAC;

            WsscfgGetApGroupEnable (&u4ApGroupEnabled);
            if (u4ApGroupEnabled == APGROUP_ENABLE)
            {
                wssWlanDB.WssWlanIsPresentDB.bVlanId = OSIX_TRUE;
                if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_IFINDEX_ENTRY,
                                              &wssWlanDB) != OSIX_SUCCESS)
                {
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC, "WssIfProcessWssWlanDBMsg\
                                  VlanId : " "Failed data from wssWlanDB\r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "WssIfProcessWssWlanDBMsg\
                                VlanId : " "Failed data from wssWlanDB\r\n"));
                    CAPWAP_RELEASE_CRU_BUF (pBuf);
                    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsg);
                    return OSIX_FAILURE;
                }
                pWlcHdlrMsg->WlcHdlrQueueReq.u2VlanId =
                    wssWlanDB.WssWlanAttributeDB.u2VlanId;
            }
            else
            {
                pWlcHdlrMsg->WlcHdlrQueueReq.u2VlanId =
                    wssWlanDB.WssWlanAttributeDB.u2VlanId;
            }

            pWlcHdlrMsg->WlcHdlrQueueReq.u4MsgType = WLCHDLR_DATA_RX_MSG;
            pWlcHdlrMsg->WlcHdlrQueueReq.pRcvBuf = pBuf;
            pWlcHdlrMsg->WlcHdlrQueueReq.u2SessId =
                pWssIfCapwapDB->pSessEntry->u2IntProfileId;

            if (WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_CAPWAP_QUEUE_REQ,
                                        pWlcHdlrMsg) == OSIX_FAILURE)
            {
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to send the\
                        packet to the WLC Hdlr Module \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to send the\
                        packet to the WLC Hdlr Module \r\n"));
                UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsg);
                return OSIX_FAILURE;
            }
        }
        else
        {
            MemReleaseMemBlock (CAPWAP_QUEMSG_POOLID, (UINT1 *) pCapwapRxQMsg);
            /* The Received packet is the Data packet. In this CAPWAP Header not \
             * required by the APHDLR module.
             Strip of the CAPWAP header and send the packet to the APHDLR Module*/
            /* Get CAPWAP Header Length */
            CAPWAP_COPY_FROM_BUF (pBuf, &u1HdrLen, CAPWAP_HDRLEN_OFFSET,
                                  CAPWAP_HDRLEN_FIELD_LEN);
            u1HdrLen = (UINT1) ((u1HdrLen & CAPWAP_HDRLEN_MASK) >> 3);
            u1HdrLen = (UINT1) (u1HdrLen << CAPWAP_4BYTE_WORD_TO_NUMOF_BYTES);

            CRU_BUF_Move_ValidOffset (pBuf, u1HdrLen);
            pWssIfCapwapDB->u4DestIp = CRU_BUF_Get_U4Reserved2 (pBuf);
            pWssIfCapwapDB->u4DestPort = CRU_BUF_Get_U4Reserved1 (pBuf);
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_SESSION_PORT_ENTRY,
                                         pWssIfCapwapDB) == OSIX_FAILURE)
            {
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Failed to Get the entry from SessionPort Entry port table \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to Get the entry from SessionPort Entry port table \r\n"));
                CAPWAP_RELEASE_CRU_BUF (pBuf);
                UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsg);
                return OSIX_FAILURE;
            }
            pSessDestIpEntry = pWssIfCapwapDB->pSessDestIpEntry;
            if (pSessDestIpEntry == NULL)
            {
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Received the data packet from unknown source \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Received the data packet from unknown source \r\n"));
                CAPWAP_RELEASE_CRU_BUF (pBuf);
                UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsg);
                return OSIX_FAILURE;
            }
            pWssIfCapwapDB->u2CapwapRSMId = pSessDestIpEntry->u2CapwapRSMIndex;
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_RSM_FROM_SESS_IDX,
                                         pWssIfCapwapDB) == OSIX_FAILURE)
            {
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Failed to Get the entry from SessionPort Entry port table \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to Get the entry from SessionPort Entry port table \r\n"));
                CAPWAP_RELEASE_CRU_BUF (pBuf);
                UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsg);
                return OSIX_FAILURE;
            }
            pWssIfCapwapDB->CapwapIsGetAllDB.bMacType = OSIX_TRUE;
            pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
                pWssIfCapwapDB->pSessEntry->u2IntProfileId;
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                         pWssIfCapwapDB) != OSIX_SUCCESS)
            {
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Get the \
                        entry from SessionPort Entry port table \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to Get the \
                        entry from SessionPort Entry port table \r\n"));
                CAPWAP_RELEASE_CRU_BUF (pBuf);
                UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsg);
                return OSIX_FAILURE;
            }

            CAPWAP_COPY_FROM_BUF (pBuf, BssId,
                                  CAPWAP_OFF_80211_MGMT_PKT,
                                  CAPWAP_OPT_MAC_FIELD_LEN);

            MEMSET (&wssWlanDB, 0, sizeof (tWssWlanDB));
            MEMCPY (wssWlanDB.WssWlanAttributeDB.BssId, BssId,
                    sizeof (tMacAddr));
            wssWlanDB.WssWlanIsPresentDB.bBssIfIndex = OSIX_TRUE;
            wssWlanDB.WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
            if (MEMCMP (McastBssId, BssId, sizeof (tMacAddr)) != 0)
            {
                if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSSID_MAPPING_ENTRY,
                                              &wssWlanDB) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC, "this could be  \
            probe req so proceed to process \r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "this could be  \
                        probe req so proceed to process \r\n"));

                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);

                    CAPWAP_TRC (CAPWAP_FAILURE_TRC, "WssIfProcessWssWlanDBMsg\
                WlanIfIndex: " "Failed data from wssWlanDB\r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "WssIfProcessWssWlanDBMsg\
                            WlanIfIndex: " "Failed data from wssWlanDB\r\n"));
                    CAPWAP_RELEASE_CRU_BUF (pBuf);
                    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsg);
                    return OSIX_FAILURE;
                }

                wssWlanDB.WssWlanIsPresentDB.bVlanId = OSIX_TRUE;
                if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY,
                                              &wssWlanDB) != OSIX_SUCCESS)
                {
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC, "WssIfProcessWssWlanDBMsg\
                        VlanId : " "Failed data from wssWlanDB\r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "WssIfProcessWssWlanDBMsg\
                        VlanId : " "Failed data from wssWlanDB\r\n"));
                    CAPWAP_RELEASE_CRU_BUF (pBuf);
                    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsg);
                    return OSIX_FAILURE;
                }
            }
            memcpy (pWlcHdlrMsg->WlcHdlrQueueReq.BssId, &BssId,
                    sizeof (tMacAddr));
            pWlcHdlrMsg->WlcHdlrQueueReq.u1MacType = SPLIT_MAC;
            pWlcHdlrMsg->WlcHdlrQueueReq.u2VlanId =
                wssWlanDB.WssWlanAttributeDB.u2VlanId;
            pWlcHdlrMsg->WlcHdlrQueueReq.u4MsgType = WLCHDLR_DATA_RX_MSG;
            pWlcHdlrMsg->WlcHdlrQueueReq.pRcvBuf = pBuf;
            pWlcHdlrMsg->WlcHdlrQueueReq.u2SessId
                = pWssIfCapwapDB->pSessEntry->u2IntProfileId;
            if (WssIfProcessWlcHdlrMsg
                (WSS_WLCHDLR_CAPWAP_QUEUE_REQ, pWlcHdlrMsg) == OSIX_FAILURE)
            {
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsg);
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Failed to send the packet to the WLC Hdlr Module \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to send the packet to the WLC Hdlr Module \r\n"));
                return OSIX_FAILURE;
            }
        }
    }
    else
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "capwapProcessDataPacket:Received unknown Msg \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "capwapProcessDataPacket:Received unknown Msg \r\n"));
        CAPWAP_RELEASE_CRU_BUF (pBuf);
        MemReleaseMemBlock (CAPWAP_QUEMSG_POOLID, (UINT1 *) pCapwapRxQMsg);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsg);
        return OSIX_FAILURE;
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsg);
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : CapwapEchoIntervalTmrExp                                   *
 * Description  : Processed Echo interval timer expiry                       *
 * Input        : pArg - received CAPWAP packet                           *
 * Output       : None                                                       *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *****************************************************************************/
VOID
CapwapEchoIntervalTmrExp (VOID *pArg)
{
    tRemoteSessionManager *pSessEntry = NULL;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tRadioIfMsgStruct   WssMsgStruct;

    MEMSET (&WssMsgStruct, 0, sizeof (tRadioIfMsgStruct));

#ifdef RFMGMT_WANTED
    tRfMgmtMsgStruct    RfMgmtMsgStruct;
    UINT1               u1RadioId = 0;
#endif

    CAPWAP_FN_ENTRY ();

    WSS_IF_DB_TEMP_MEM_ALLOC_VOID_RETURN (pWssIfCapwapDB)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
#ifdef RFMGMT_WANTED
    MEMSET (&RfMgmtMsgStruct, 0, sizeof (tRfMgmtMsgStruct));
#endif

    pSessEntry = (tRemoteSessionManager *) pArg;
    if (pSessEntry->u1EchoCount >= gu2MaxRetransmitCount)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Echo Interval Timer expired, Disconnect the session \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Echo Interval Timer expired, Disconnect the session \r\n"));
        CAPWAP_TRC (CAPWAP_FSM_TRC, "Enter the CAPWAP DTLS TD State \r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                      "Enter the CAPWAP DTLS TD State \r\n"));
        pSessEntry->eCurrentState = CAPWAP_DTLSTD;

        pWssIfCapwapDB->u4DestIp = pSessEntry->remoteIpAddr.u4_addr[0];
        pWssIfCapwapDB->u4DestPort = pSessEntry->u4RemoteCtrlPort;
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_DELETE_RSM, pWssIfCapwapDB)
            != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "WssIfProcessCapwapDBMsg failed \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "WssIfProcessCapwapDBMsg failed \r\n"));
        }
        /*reset the counter to zero */

        PRINTF ("\n Session expired for WTP Internal Id:%d\n",
                pSessEntry->u2IntProfileId);
        /* Call WSS-WLAN Module to notify the run State */
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
        pWssIfCapwapDB->CapwapIsGetAllDB.bNoOfRadio = OSIX_TRUE;
        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
            pSessEntry->u2IntProfileId;
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
            == OSIX_SUCCESS)
        {
            /* Whenever an AP reaches run state, send an inidcation to RFMGMT
             * module. In case DCA algorithm is running, a channel wil be 
             * assigned for a new AP */
            for (u1RadioId = 1;
                 u1RadioId <= pWssIfCapwapDB->CapwapGetDB.u1WtpNoofRadio;
                 u1RadioId++)
            {
                pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;
                pWssIfCapwapDB->CapwapGetDB.u1RadioId = u1RadioId;

                if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
                    == OSIX_SUCCESS)
                {
                    WssMsgStruct.unRadioIfMsg.RadioIfVirtualIntfCreate.
                        u2WtpInternalId = pSessEntry->u2IntProfileId;
                    WssMsgStruct.unRadioIfMsg.RadioIfVirtualIntfCreate.
                        u4IfIndex = pWssIfCapwapDB->CapwapGetDB.u4IfIndex;
                    WssMsgStruct.unRadioIfMsg.RadioIfVirtualIntfCreate.
                        isPresent = OSIX_TRUE;
                    if (WssIfProcessRadioIfMsg
                        (WSS_RADIOIF_TEARDOWN_NOTIFICATION_MSG,
                         &WssMsgStruct) != OSIX_SUCCESS)
                    {
                        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                    "Failed in removing entries for Teardown AP \r\n");
                        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                      "Failed in removing entries for Teardown AP \r\n"));
                    }

#ifdef RFMGMT_WANTED
                    RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtIntfConfigReq.
                        u4RadioIfIndex = pWssIfCapwapDB->CapwapGetDB.u4IfIndex;

                    if (WssIfProcessRfMgmtMsg (RFMGMT_DELETE_NEIGHBOR_INFO,
                                               &RfMgmtMsgStruct) !=
                        OSIX_SUCCESS)
                    {
                        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                    "Error deleting neighbor "
                                    "scan information \r\n");
                        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                      "Error deleting neighbor "
                                      "scan information \r\n"));
                    }
#endif
                }
            }
        }
        if (CapwapCleanUpSessionDetails (pSessEntry) == OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "CapwapCleanUpSessionDetails Failed after echo time out\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "CapwapCleanUpSessionDetails Failed after echo time out\r\n"));
        }
    }
    else
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Echo Timer expired but not going to disconnect the session \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Echo Timer expired but not going to disconnect the session \r\n"));
        pSessEntry->u1EchoCount++;
        CapwapTmrStart (pSessEntry, CAPWAP_ECHO_INTERVAL_TMR,
                        CAPWAP_ECHO_INTERVAL_TIMEOUT);
    }

    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    CAPWAP_FN_EXIT ();
}

/*****************************************************************************
 * Function     : CapwapNotifyRunStateToModules                              *
 * Description  : Notify other modules when CAPWAP reaches RUN state         *
 * Input        : u2IntProfileId                                             *
 * Output       : None                                                       *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *****************************************************************************/
INT4
CapwapNotifyRunStateToModules (UINT2 u2IntProfileId)
{
    tWssWlanMsgStruct  *pWssWlanMsgStruct = NULL;
    tRadioIfMsgStruct   RadioIfMsgStruct;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tRadioIfGetDB       RadioIfGetDB;
#ifdef KERNEL_CAPWAP_WANTED
    static UINT2        u2FilterFlag = 1;
#endif
#ifdef RFMGMT_WANTED
    tRfMgmtMsgStruct    RfMgmtMsgStruct;
    UINT1               u1RadioId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4NextIpAddress = 0;
    INT4                i4RadioIfIndex = 0;
#endif
    tWssIfWtpDhcpConfigDB DhcpConfigEntry;
    tWssIfWtpL3SubIfDB  WssIfWtpL3SubIfDB;
    UINT1               u1LegacyRate = 0;

    MEMSET (&DhcpConfigEntry, 0, sizeof (tWssIfWtpDhcpConfigDB));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    MEMSET (&RadioIfMsgStruct, 0, sizeof (tRadioIfMsgStruct));
    MEMSET (&WssIfWtpL3SubIfDB, 0, sizeof (tWssIfWtpL3SubIfDB));

    CAPWAP_FN_ENTRY ();
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    pWssWlanMsgStruct =
        (tWssWlanMsgStruct *) (VOID *) UtlShMemAllocWssWlanBuf ();
    if (pWssWlanMsgStruct == NULL)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapNotifyRunStateToModules:- "
                    "UtlShMemAllocWssWlanBuf returned failure\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "CapwapNotifyRunStateToModules:- "
                      "UtlShMemAllocWssWlanBuf returned failure\n"));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    MEMSET (pWssWlanMsgStruct, 0, sizeof (tWssWlanMsgStruct));
#ifdef RFMGMT_WANTED
    MEMSET (&RfMgmtMsgStruct, 0, sizeof (tRfMgmtMsgStruct));
#endif

    /* Call WSS-WLAN Module to notify the run State */
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bNoOfRadio = OSIX_TRUE;
    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2IntProfileId;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpMacAddress = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpLocalRouting = OSIX_TRUE;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
        != OSIX_SUCCESS)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to Retrive the Number of Radios \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to Retrive the Number of Radios \r\n"));
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        return OSIX_FAILURE;
    }

#ifdef RFMGMT_WANTED
    /* Whenever an AP reaches run state, send an inidcation to RFMGMT
     * module. In case DCA algorithm is running, a channel wil be assigned
     * for a new AP */
    for (u1RadioId = 1; u1RadioId <= pWssIfCapwapDB->CapwapGetDB.u1WtpNoofRadio;
         u1RadioId++)
    {
        pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;
        pWssIfCapwapDB->CapwapGetDB.u1RadioId = u1RadioId;
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
            == OSIX_SUCCESS)
        {
            RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtIntfConfigReq.
                u4RadioIfIndex = pWssIfCapwapDB->CapwapGetDB.u4IfIndex;

            if (u1RadioId == 1)
            {
                i4RadioIfIndex = (INT4) pWssIfCapwapDB->CapwapGetDB.u4IfIndex;
            }
            if (WssIfProcessRfMgmtMsg (RFMGMT_WLC_RUN_STATE_INDI_MSG,
                                       &RfMgmtMsgStruct) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Error triggering DCA "
                            "Algorithm \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Error triggering DCA " "Algorithm \r\n"));
            }
        }
    }
#endif

    pWssWlanMsgStruct->unWssWlanMsg.WssWlanPendingConfReq.u1NoOfRadios =
        pWssIfCapwapDB->CapwapGetDB.u1WtpNoofRadio;

    pWssWlanMsgStruct->unWssWlanMsg.WssWlanPendingConfReq.u2WtpInternalId =
        u2IntProfileId;

    if (WssIfProcessWssWlanMsg
        (WSS_WLAN_PROCESS_ADD_BIND_REQ, pWssWlanMsgStruct) != OSIX_SUCCESS)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to Notify the WSS-WLAN Module \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to Notify the WSS-WLAN Module \r\n"));
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        return OSIX_FAILURE;
    }
#ifdef KERNEL_CAPWAP_WANTED
    pWssIfCapwapDB->CapwapIsGetAllDB.bIntfName = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
        == OSIX_SUCCESS)
    {
        if ((u2FilterFlag == 1)
            && (pWssIfCapwapDB->CapwapGetDB.au1IntfName[0] != 0))
        {
            WssIfWmmQueueCreation (pWssIfCapwapDB->CapwapGetDB.au1IntfName);
            u2FilterFlag = 0;
        }
    }
#endif

    WssIfGetLegacyRateStatus (&u1LegacyRate);
    if (u1LegacyRate == LEGACY_RATE_ENABLED)
    {
        if ((CapwapGetWtpProfileIdFromProfileMac
             (pWssIfCapwapDB->CapwapGetDB.WtpMacAddress,
              &u4WtpProfileId) != OSIX_SUCCESS))
        {
            UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
            return OSIX_FAILURE;
        }
        WssCfgSetLegacyRateStatus (&u4WtpProfileId);
    }

    /* If local routing disable do nothing */
    if (pWssIfCapwapDB->CapwapGetDB.u1WtpLocalRouting == 1)
    {
        if ((CapwapGetWtpProfileIdFromProfileMac
             (pWssIfCapwapDB->CapwapGetDB.WtpMacAddress,
              &u4WtpProfileId) != OSIX_SUCCESS))
        {
            UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
            return OSIX_FAILURE;
        }
        DhcpConfigEntry.u4WtpProfileId = u4WtpProfileId;
        if (WssIfWtpDhcpConfigGetEntry (&DhcpConfigEntry) == OSIX_SUCCESS)
        {
            if (DhcpConfigEntry.i4WtpDhcpServerStatus == 1)
            {
                WssifStartDhcpPoolUpdateReq (u4WtpProfileId,
                                             DhcpConfigEntry.
                                             i4WtpDhcpServerStatus, 0, ACTIVE);
            }
            else
            {
                u4NextIpAddress = gNextDhcpSrvAddress;
                WssifStartDhcpRelayUpdateReq (u4WtpProfileId,
                                              DhcpConfigEntry.
                                              i4WtpDhcpRelayStatus,
                                              u4NextIpAddress);
            }
        }
        WssifFireWallUpdateReq (u4WtpProfileId, 0);
        WssifNatUpdateReq (u4WtpProfileId, 0, 0);
        WssifRouteTable (u4WtpProfileId, 0, 0, 0, 0);
        WssifFirewallFilterTable (u4WtpProfileId, NULL, NULL,
                                  NULL, 0, NULL, NULL, 0, 0);
        WssifFirewallAclTable (u4WtpProfileId, NULL, 0, 0, 0, 0, 0);
        WssIfRadioUpdateDiffServ (i4RadioIfIndex);

        /* configure L3 SUB IFACES */
        WssIfWtpL3SubIfDB.u4WtpProfileId = u4WtpProfileId;
        WssIfL3SubIfUpdateReq (&WssIfWtpL3SubIfDB);
    }
#ifdef RFMGMT_WANTED
    /* When the bindings are complete, send an indication to RFMGMT
     * module. RF module will send a config update request if auto scan is 
     * enabled  */
    for (u1RadioId = 1; u1RadioId <= pWssIfCapwapDB->CapwapGetDB.u1WtpNoofRadio;
         u1RadioId++)
    {
        pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;
        pWssIfCapwapDB->CapwapGetDB.u1RadioId = u1RadioId;
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
            == OSIX_SUCCESS)
        {
            RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtIntfConfigReq.
                u4RadioIfIndex = pWssIfCapwapDB->CapwapGetDB.u4IfIndex;

            if (WssIfProcessRfMgmtMsg (RFMGMT_NOTIFY_BINDING_COMPLETE,
                                       &RfMgmtMsgStruct) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Error triggering DCA "
                            "Algorithm \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Error triggering DCA " "Algorithm \r\n"));
            }
        }
    }
#endif

    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : CapwapProcessKeepAlivePacket                               *
 * Description  : Process keep alive pkts                                    *
 * Input        : pBuf, pSess                                                *
 * Output       : None                                                       *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *****************************************************************************/
INT4
CapwapProcessKeepAlivePacket (tCRU_BUF_CHAIN_HEADER * pBuf,
                              tRemoteSessionManager * pSess)
{
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
#ifdef NPAPI_WANTED
    tFsHwNp             FsHwNp;
    tCapwapNpParams     ptCapwapNpParams;
    tCapwapNpModInfo   *pCapwapNpModInfo = NULL;
    tCapwapNpWrEnable  *pCreateUcTunnel = NULL;
    MEMSET (&ptCapwapNpParams, 0, sizeof (tCapwapNpParams));
#endif
    INT4                i4RetVal = 0;
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;
    tQueData            QueData;
    tCapwapWTPTrapInfo  sWTPTrapInfo;
    MEMSET (&sWTPTrapInfo, 0, sizeof (tCapwapWTPTrapInfo));
    CAPWAP_FN_ENTRY ();
    if (pSess == NULL)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Remote session entry is NULL, return Failur \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Remote session entry is NULL, return Failur \r\n"));
        return OSIX_FAILURE;
    }

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    pWlcHdlrMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pWlcHdlrMsgStruct == NULL)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapCliSetWtpProfile:- "
                    "UtlShMemAllocWlcBuf returned failure\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "CapwapCliSetWtpProfile:- "
                      "UtlShMemAllocWlcBuf returned failure\n"));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));

    if (pSess->eCurrentState == CAPWAP_DATACHECK)
    {
#ifdef NPAPI_WANTED

        /* Static values need to be filled from Capwap */
        /* Call the tunnel creation NPAPI */
        ptCapwapNpParams.u1TunnelMode = CAPWAP_TUNNEL_TYPE_WLAN;
        ptCapwapNpParams.remoteIpAddr.u2AddressLen = CAPWAP_NP_ADDRESS_IPV4;
        ptCapwapNpParams.remoteIpAddr.ipAddr.u4_addr[0] =
            pSess->remoteIpAddr.u4_addr[0];
        ptCapwapNpParams.u4DestDataPort = pSess->u4RemoteDataPort;
        ptCapwapNpParams.transProto = CAPWAP_UDP;
        ptCapwapNpParams.u4PMTU = pSess->PMTU;

        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
        pWssIfCapwapDB->CapwapIsGetAllDB.bMacType = OSIX_TRUE;
        pWssIfCapwapDB->CapwapIsGetAllDB.bTunnelMode = OSIX_TRUE;
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpTunnelMode = OSIX_TRUE;
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpModelNumber = OSIX_TRUE;
        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = pSess->u2IntProfileId;
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
            == OSIX_SUCCESS)
        {
            /* 802.11 or 802.3  - Split MAC or Local MAC */
            ptCapwapNpParams.wtpTunnelMode =
                pWssIfCapwapDB->CapwapGetDB.u1WtpMacType;

        }
        else
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to Get Capwap DB Tunnel parameters \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to Get Capwap DB Tunnel parameters \r\n"));
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            return OSIX_FAILURE;
        }

        ptCapwapNpParams.u4RadioMacIncl = 1;

        NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                             NP_CAPWAP_MODULE,    /* Module ID */
                             CAPWAP_CREATE_UNICAST_TUNNEL,    /* Function/OpCode */
                             0,    /* IfIndex value if applicable */
                             0,    /* No. of Port Params */
                             0);    /* No. of PortList Parms */
        pCapwapNpModInfo = &(FsHwNp.CapwapNpModInfo);
        pCreateUcTunnel
            = (tCapwapNpWrEnable *) & pCapwapNpModInfo->CapwapNpCreateUcTunnel;

        pCreateUcTunnel->ptCapwapNpParams = &ptCapwapNpParams;

        if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "\n Creating unicast tunnel failed"
                        "\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "\n Creating unicast tunnel failed" "\r\n"));
            return (OSIX_FAILURE);
        }
        pSess->u4TunnelId = pCreateUcTunnel->ptCapwapNpParams->u4TunnelId;
#endif
        /* Received the First Keep Alive Packet */
        pSess->u4RemoteDataPort = CRU_BUF_Get_U4Reserved1 (pBuf);
        CAPWAP_TRC (CAPWAP_MGMT_TRC, "WLC Moved to Run State \r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                      "WLC Moved to Run State \r\n"));
        CapwapTmrStop (pSess, CAPWAP_DATACHECK_TMR);
        if (pSess->lastTransmittedPkt != NULL)
        {
            WlchdlrFreeTransmittedPacket (pSess);
        }
        CAPWAP_TRC (CAPWAP_FSM_TRC, "Entered CAPWAP RUN State \r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                      "Entered CAPWAP RUN State \r\n"));
        pSess->eCurrentState = CAPWAP_RUN;
        pSess->ePrevState = CAPWAP_RUN;

        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpStatisticsTimer = OSIX_TRUE;
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpMacAddress = OSIX_TRUE;
        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = pSess->u2IntProfileId;
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB) ==
            OSIX_SUCCESS)
        {
            /*get AP mac address and state to call trap when AP comes to run state */
            MEMCPY (pWssIfCapwapDB->BaseMacAddress,
                    pWssIfCapwapDB->CapwapGetDB.WtpMacAddress, MAC_ADDR_LEN);
            MEMCPY (sWTPTrapInfo.au1capwapBaseNtfWtpId,
                    pWssIfCapwapDB->BaseMacAddress, MAC_ADDR_LEN);
            sWTPTrapInfo.i4capwapBaseWtpState = CAPWAP_RUN;
            /* Calling WTP state change trap when Access Point comes to run state */
            CapwapSnmpifSendTrap (CAPWAP_WTP_STATE_CHANGE, &sWTPTrapInfo);
        }
        else
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Unable to get statistics"
                        "timer from DB \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Unable to get statistics" "timer from DB \r\n"));
        }
        if ((pWssIfCapwapDB->CapwapGetDB.u2WtpStatisticsTimer != 120) &&
            (pWssIfCapwapDB->CapwapGetDB.u2WtpStatisticsTimer != 0))
        {
            pWlcHdlrMsgStruct->PmConfigUpdateReq.u4SessId =
                pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId;
            pWlcHdlrMsgStruct->PmConfigUpdateReq.statsTimer.isOptional =
                OSIX_TRUE;
            pWlcHdlrMsgStruct->PmConfigUpdateReq.statsTimer.u2MsgEleType =
                CAPWAP_STATS_TIMER_MSG_TYPE;
            pWlcHdlrMsgStruct->PmConfigUpdateReq.statsTimer.u2MsgEleLen = 4;
            pWlcHdlrMsgStruct->PmConfigUpdateReq.statsTimer.u2StatsTimer =
                pWssIfCapwapDB->CapwapGetDB.u2WtpStatisticsTimer;
            i4RetVal = WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_PM_CONF_UPDATE_REQ,
                                               pWlcHdlrMsgStruct);
            if (i4RetVal == CAPWAP_NO_AP_PRESENT)
            {
                gu1IsConfigResponseReceived = CAPWAP_NO_AP_PRESENT;
            }
            else if (i4RetVal != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "CapwapCliSetWtpProfile : Sending Request to WLC "
                            "Handler failed \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "CapwapCliSetWtpProfile : Sending Request to WLC "
                              "Handler failed \r\n"));
                i4RetVal = CapwapServiceMainTaskUnLock ();
                if (i4RetVal == OSIX_FAILURE)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "CapwapServiceMainTaskLock: Failed to "
                                "obtain lock \n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "CapwapServiceMainTaskLock: Failed to "
                                  "obtain lock \n"));
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                    return OSIX_FAILURE;
                }
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                return OSIX_FAILURE;
            }

        }

        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpUpTime = OSIX_TRUE;
        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = pSess->u2IntProfileId;
        pWssIfCapwapDB->CapwapGetDB.u4WtpUpTime = OsixGetSysUpTime ();
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_SET_DB, pWssIfCapwapDB)
            == OSIX_FAILURE)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to SET CAPWAP DB \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to SET CAPWAP DB \r\n"));
            return OSIX_FAILURE;
        }

        /* Notify Other Modules on moving to RUN State */
        CapwapNotifyRunStateToModules (pSess->u2IntProfileId);

#ifdef KERNEL_CAPWAP_WANTED
#ifdef WLC_WANTED
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
        pWssIfCapwapDB->CapwapIsGetAllDB.bMacType = OSIX_TRUE;
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpMacAddress = OSIX_TRUE;
        pWssIfCapwapDB->CapwapIsGetAllDB.bCtrlUdpPort = OSIX_TRUE;
        pWssIfCapwapDB->CapwapIsGetAllDB.bFragReassembleStatus = OSIX_TRUE;
        pWssIfCapwapDB->CapwapIsGetAllDB.bPathMTU = OSIX_TRUE;
        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = pSess->u2IntProfileId;
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
            != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed from WssIfProcessCapwapDBMsg \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed from WssIfProcessCapwapDBMsg \r\n"));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            return OSIX_FAILURE;
        }

        if (CapwapUpdateKernelIpAndMacTable
            (pSess->remoteIpAddr.u4_addr[0], pSess->u4RemoteDataPort,
             pWssIfCapwapDB->CapwapGetDB.u1WtpMacType,
             pWssIfCapwapDB->CapwapGetDB.WtpMacAddress,
             (INT4) pWssIfCapwapDB->u4CtrludpPort,
             pWssIfCapwapDB->CapwapGetDB.u1FragReassembleStatus,
             pWssIfCapwapDB->CapwapGetDB.u4PathMTU) == OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to update KERNEL DB \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to update KERNEL DB \r\n"));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            return OSIX_FAILURE;
        }
#endif
#endif

        /* Start Echo Interval Timer for WLC */
        CapwapTmrStart (pSess, CAPWAP_ECHO_INTERVAL_TMR,
                        CAPWAP_ECHO_INTERVAL_TIMEOUT);
    }
    /* Transmit the same received keep alive packet received */
    if ((pSess->u1DataDtlsFlag) == DTLS_CFG_DISABLE)
    {
        CAPWAP_TRC (CAPWAP_MGMT_TRC,
                    "DTLS is disabled for data pkts"
                    "send the plain text packet \r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                      "DTLS is disabled for data pkts"
                      "send the plain text packet \r\n"));
        if (CapwapTxMessage (pBuf,
                             pSess, CAPWAP_MAX_KEEPALIVE_LEN + 5,
                             WSS_CAPWAP_802_11_DATA_PKT) != OSIX_SUCCESS)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to transmit keep alive packet \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to transmit keep alive packet \r\n"));
            return OSIX_FAILURE;
        }
    }
    else
    {
        MEMSET (&QueData, 0, sizeof (tQueData));
        QueData.pBuffer = pBuf;
        QueData.eQueDataType = DATA_ENCRYPT;
        QueData.u4BufSize = (UINT4) (CAPWAP_MAX_KEEPALIVE_LEN + 5);
        QueData.u4IpAddr = pSess->remoteIpAddr.u4_addr[0];
        QueData.u4PortNo = pSess->u4RemoteDataPort;
        QueData.u4Id = pSess->u4DtlsSessId;
        QueData.u4Channel = DTLS_DATA_CHANNEL;

        if (CapwapDtlsDecryptNotify (&QueData) == OSIX_FAILURE)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "DTLS decryption Failed for data packets\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "DTLS decryption Failed for data packets\r\n"));
            return OSIX_FAILURE;
        }
    }

    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : CapwapValidateDiscType                                     *
 * Description  : Validates Disc type pkt                                    *
 * Input        : pRcvBuf, pCapMsg, u2Index                                  *
 * Output       : None                                                       *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *****************************************************************************/
INT4
CapwapValidateDiscType (UINT1 *pRcvBuf,
                        tCapwapControlPacket * pCapMsg, UINT2 u2Index)
{
    UINT4               u4Offset;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT2               u2NumMsgElems = 0;

    CAPWAP_FN_ENTRY ();

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    u2NumMsgElems = pCapMsg->capwapMsgElm[u2Index].numMsgElemInstance;

    if (u2NumMsgElems > 1)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "The Message element Disvovery Type Received More than once \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "The Message element Disvovery Type Received More than once \r\n"));

        return OSIX_FAILURE;
    }
    else
    {
        u4Offset = pCapMsg->capwapMsgElm[u2Index].pu2Offset[0];
        pRcvBuf += u4Offset + CAPWAP_MSG_ELEM_TYPE_LEN;
        CAPWAP_GET_1BYTE (pWssIfCapwapDB->CapwapGetDB.u1WtpDiscType, pRcvBuf);
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpDiscType = OSIX_TRUE;
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = pCapMsg->u2IntProfileId;
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_VALIDATE_DB, pWssIfCapwapDB)
            == OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to Validate the Received Discovery Type \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to Validate the Received Discovery Type \r\n"));
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_SET_DB, pWssIfCapwapDB)
                == OSIX_FAILURE)
            {
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_SUCCESS;
        }
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : CapwapValidateAcNamePriority                               *
 * Description  : Validates AC name with priority                            *
 * Input        : pRcvBuf, pCapMsg, u2MsgIndex                               *
 * Output       : None                                                       *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *****************************************************************************/
INT4
CapwapValidateAcNamePriority (UINT1 *pRcvBuf,
                              tCapwapControlPacket * pCwMsg, UINT2 u2MsgIndex)
{
    UINT2               u2NumMsgElems;
    UINT4               u4Offset;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT2               u2IntId = 0, u2Type, u2Len;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    u2NumMsgElems = pCwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;
    u4Offset = pCwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[0];
    if (u2NumMsgElems > 1)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "The Message element Ac Name with Priority Received More than once \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "The Message element Ac Name with Priority Received More than once \r\n"));
        return OSIX_FAILURE;
    }
    else
    {
        pWssIfCapwapDB->CapwapIsGetAllDB.bAcNamewithPriority = OSIX_TRUE;
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2IntId;
        pRcvBuf += u4Offset;
        CAPWAP_GET_2BYTE (u2Type, pRcvBuf);
        CAPWAP_GET_2BYTE (u2Len, pRcvBuf);
        CAPWAP_GET_1BYTE (pWssIfCapwapDB->CapwapGetDB.AcNameWithPri[0].
                          u1Priority, pRcvBuf);
        CAPWAP_GET_NBYTE (pWssIfCapwapDB->CapwapGetDB.AcNameWithPri[0].wlcName,
                          pRcvBuf, (UINT4) (u2Len - 1));
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_VALIDATE_DB, pWssIfCapwapDB) ==
            OSIX_FAILURE)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to Validate the AC Name List \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to Validate the AC Name List \r\n"));
            return OSIX_FAILURE;
        }
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function     : CapwapValidateWtpBoardData                                 *
* Description  : This routine used to validate the received wtp board data  *
* Input        : None                                                       *
* Output       : None                                                       *
* Returns      : None                                                       *
*****************************************************************************/
INT4
CapwapValidateWtpBoardData (UINT1 *pRcvBuf,
                            tCapwapControlPacket * pCapMsg, UINT2 u2Index)
{
    UINT4               u4Offset;
    UINT2               u2NumMsgElems;
    tWtpBoardData       wtpBoardData;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;

    CAPWAP_FN_ENTRY ();

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&wtpBoardData, 0, sizeof (tWtpBoardData));

    u2NumMsgElems = pCapMsg->capwapMsgElm[u2Index].numMsgElemInstance;

    if (u2NumMsgElems > 1)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "The Message element Board Data Received More than once \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "The Message element Board Data Received More than once \r\n"));
        return OSIX_FAILURE;
    }
    else
    {
        u4Offset = pCapMsg->capwapMsgElm[u2Index].pu2Offset[0];
        if (CapwapGetWtpBoardDataFromBuf (pRcvBuf, u4Offset, &wtpBoardData) !=
            OSIX_SUCCESS)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Get WTP data from Buf failed \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Get WTP data from Buf failed \r\n"));
            return OSIX_FAILURE;
        }
        pWssIfCapwapDB->CapwapIsGetAllDB.bVendorSMI = OSIX_TRUE;
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = pCapMsg->u2IntProfileId;
        pWssIfCapwapDB->CapwapGetDB.u4VendorSMI = wtpBoardData.u4VendorSMI;
        /* Validate WTP Model Number */
        if (wtpBoardData.wtpBoardInfo[WTP_MODEL_NUMBER].isOptional)
        {
            pWssIfCapwapDB->CapwapIsGetAllDB.bWtpModelNumber = OSIX_TRUE;
            MEMCPY (pWssIfCapwapDB->CapwapGetDB.au1WtpModelNumber,
                    &wtpBoardData.wtpBoardInfo[WTP_MODEL_NUMBER].boardInfoData,
                    wtpBoardData.wtpBoardInfo[WTP_MODEL_NUMBER].
                    u2BoardInfoLength);
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_VALIDATE_DB, pWssIfCapwapDB)
                == OSIX_FAILURE)
            {
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "WTP Model Number validation Failed \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "WTP Model Number validation Failed \r\n"));
                return OSIX_FAILURE;
            }
        }

        /* Validate WTP Serial Number */
        if (wtpBoardData.wtpBoardInfo[WTP_SERIAL_NUMBER].isOptional)
        {
            pWssIfCapwapDB->CapwapIsGetAllDB.bWtpSerialNumber = OSIX_TRUE;
            MEMCPY (pWssIfCapwapDB->CapwapGetDB.au1WtpSerialNumber,
                    &wtpBoardData.wtpBoardInfo[WTP_SERIAL_NUMBER].boardInfoData,
                    wtpBoardData.wtpBoardInfo[WTP_SERIAL_NUMBER].
                    u2BoardInfoLength);
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_VALIDATE_DB, pWssIfCapwapDB)
                == OSIX_FAILURE)
            {
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "WTP Model Board Serial Number validation Failed \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "WTP Model Board Serial Number validation Failed \r\n"));
                return OSIX_FAILURE;
            }
        }
        /* Validate WTP Board Id */
        if (wtpBoardData.wtpBoardInfo[WTP_BOARD_ID].isOptional)
        {
            pWssIfCapwapDB->CapwapIsGetAllDB.bWtpBoardId = OSIX_TRUE;
            MEMCPY (pWssIfCapwapDB->CapwapGetDB.au1WtpBoardId,
                    &wtpBoardData.wtpBoardInfo[WTP_BOARD_ID].boardInfoData,
                    wtpBoardData.wtpBoardInfo[WTP_BOARD_ID].u2BoardInfoLength);
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_VALIDATE_DB, pWssIfCapwapDB)
                == OSIX_FAILURE)
            {
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "WTP Board Id validation Failed \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "WTP Board Id validation Failed \r\n"));
                return OSIX_FAILURE;
            }
        }
        /* Validate WTP Board Rivision */
        if (wtpBoardData.wtpBoardInfo[WTP_BOARD_RIVISION].isOptional)
        {
            pWssIfCapwapDB->CapwapIsGetAllDB.bWtpBoardRivision = OSIX_TRUE;
            MEMCPY (pWssIfCapwapDB->CapwapGetDB.au1WtpBoardRivision,
                    &wtpBoardData.wtpBoardInfo[WTP_BOARD_RIVISION].
                    boardInfoData,
                    wtpBoardData.wtpBoardInfo[WTP_BOARD_RIVISION].
                    u2BoardInfoLength);
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_VALIDATE_DB, pWssIfCapwapDB)
                == OSIX_FAILURE)
            {
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "WTP Board Rivision validation Failed \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "WTP Board Rivision validation Failed \r\n"));
                return OSIX_FAILURE;
            }
        }
        /* Validate WTP Base Mac Address */
        if (wtpBoardData.wtpBoardInfo[WTP_BASE_MAC_ADDRESS].isOptional)
        {
            pWssIfCapwapDB->CapwapIsGetAllDB.bWtpMacAddress = OSIX_TRUE;
            MEMCPY (pWssIfCapwapDB->CapwapGetDB.WtpMacAddress,
                    &wtpBoardData.wtpBoardInfo[WTP_BASE_MAC_ADDRESS].
                    boardInfoData,
                    wtpBoardData.wtpBoardInfo[WTP_BASE_MAC_ADDRESS].
                    u2BoardInfoLength);
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_VALIDATE_DB, pWssIfCapwapDB)
                == OSIX_FAILURE)
            {
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "WTP MAC Address validation Failed \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "WTP MAC Address validation Failed \r\n"));
                return OSIX_FAILURE;
            }
        }
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapValidateMacType                             */
/*  Description     : The function validates the Mac type of the        */
/*                    received CAPWAP discovery request                 */
/*  Input(s)        : discReqMsg - parsed CAPWAP Discovery request      */
/*                    pRcvBuf - Received discovery packet               */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
CapwapValidateMacType (UINT1 *pRcvBuf,
                       tCapwapControlPacket * pCwMsg, UINT2 u2Index)
{
    UINT2               u2NumMsgElems;
    UINT4               u4Offset;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    u2NumMsgElems = pCwMsg->capwapMsgElm[u2Index].numMsgElemInstance;
    u4Offset = pCwMsg->capwapMsgElm[u2Index].pu2Offset[0];
    if (u2NumMsgElems > 1)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "The Message element Mac Type Received More than once \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "The Message element Mac Type Received More than once \r\n"));
        return OSIX_FAILURE;
    }
    else
    {
        pWssIfCapwapDB->CapwapIsGetAllDB.bMacType = OSIX_TRUE;
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = pCwMsg->u2IntProfileId;
        pRcvBuf += u4Offset + CAPWAP_MSG_ELEM_TYPE_LEN;
        CAPWAP_GET_1BYTE (pWssIfCapwapDB->CapwapGetDB.u1WtpMacType, pRcvBuf);
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_VALIDATE_DB, pWssIfCapwapDB)
            == OSIX_FAILURE)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to Validate the received Mac Type \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to Validate the received Mac Type \r\n"));
            return OSIX_FAILURE;
        }
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapValidateFrameTunnelMode                     */
/*  Description     : The function validates the WTP frame tunnel       */
/*                    mode of the received CAPWAP discovery request     */
/*  Input(s)        : discReqMsg - parsed CAPWAP Discovery request      */
/*                    pRcvBuf - Received discovery packet               */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
CapwapValidateFrameTunnelMode (UINT1 *pRcvBuf,
                               tCapwapControlPacket * pCwMsg, UINT2 u2Index)
{
    UINT2               u2NumMsgElems;
    UINT4               u4Offset;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;

    CAPWAP_FN_ENTRY ();
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    u2NumMsgElems = pCwMsg->capwapMsgElm[u2Index].numMsgElemInstance;
    u4Offset = pCwMsg->capwapMsgElm[u2Index].pu2Offset[0];
    if (u2NumMsgElems > 1)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "The Message element Frame Tunnel Mode Received More than once \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "The Message element Frame Tunnel Mode Received More than once \r\n"));
        return OSIX_FAILURE;
    }
    else
    {
        pWssIfCapwapDB->CapwapIsGetAllDB.bTunnelMode = OSIX_TRUE;
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = pCwMsg->u2IntProfileId;
        pRcvBuf += u4Offset + CAPWAP_MSG_ELEM_TYPE_LEN;
        CAPWAP_GET_1BYTE (pWssIfCapwapDB->CapwapGetDB.u1WtpTunnelMode, pRcvBuf);
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_VALIDATE_DB, pWssIfCapwapDB)
            == OSIX_FAILURE)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to Validate the Frame Tunnel Mode \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to Validate the Frame Tunnel Mode \r\n"));
            return OSIX_FAILURE;
        }
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapValidateRadioInfo                           */
/*  Description     : The function validates the WTP Radio information  */
/*                    received in CAPWAP discovery request. Following   */
/*                    two validations done in this function,            */
/*                 1. Number of radios received in discovery request    */
/*                    against the virtual radio interfaces created in   */
/*                    WLC (based on WTP Model Number)                   */
/*                 2  Received Radio Type received in discovery request */
/*                    against the WLC supported radio type              */
/*                                                                      */
/*  Input(s)        : discReqMsg - parsed CAPWAP Discovery request      */
/*                    pRcvBuf - Received discovery packet               */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
CapwapValidateRadioInfo (UINT1 *pRcvBuf,
                         tCapwapControlPacket * pCapMsg, UINT2 u2Index)
{
    UNUSED_PARAM (pRcvBuf);
    UINT4               u4RadioCount = 0;
    UINT2               u2NumMsgElems = 0;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tRadioIfGetDB       RadioIfGetDB;
    UINT1               u1RadioId = 0;
    UINT4               u4Offset = 0;

    CAPWAP_FN_ENTRY ();
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    u2NumMsgElems = pCapMsg->capwapMsgElm[u2Index].numMsgElemInstance;

    pWssIfCapwapDB->CapwapIsGetAllDB.bNoOfRadio = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = pCapMsg->u2IntProfileId;

    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                 pWssIfCapwapDB) == OSIX_FAILURE)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        WSSIF_TRC (WSSIF_FAILURE_TRC, "Failed to Get ProfileDB\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to Get ProfileDB\r\n"));
        return OSIX_FAILURE;
    }

    u4RadioCount = pWssIfCapwapDB->CapwapGetDB.u1WtpNoofRadio;
    if (u4RadioCount != u2NumMsgElems)
    {
        WSSIF_TRC2 (WSSIF_FAILURE_TRC,
                    "The Received Radio Element is %d \nConfigured Radio element %d is not supported \r\n",
                    u2NumMsgElems, u4RadioCount);
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "The Received Radio Element is %d \nConfigured Radio element %d is not supported \r\n",
                      u2NumMsgElems, u4RadioCount));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;
    pWssIfCapwapDB->CapwapGetDB.u1RadioId = u1RadioId;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
        == OSIX_SUCCESS)
    {

        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
            pWssIfCapwapDB->CapwapGetDB.u4IfIndex;
        RadioIfGetDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_TRUE;

        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                      &RadioIfGetDB) != OSIX_SUCCESS)
        {
            WSSIF_TRC (WSSIF_FAILURE_TRC, "Failed to Get from RadioDB\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to Get from RadioDB\r\n"));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;

        }
    }
    u4Offset = pCapMsg->capwapMsgElm[u2Index].pu2Offset[0];

    if ((RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType == RADIO_TYPE_AC) &&
        ((*(pRcvBuf + u4Offset + RADIOIF_11AC_OFFSET)) != RADIOIF_11AC_TYPE))
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "The Configured Radio type is not supported by wtp \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "The Configured Radio type is not supported by wtp \r\n"));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;

    }

    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function     : CapwapConstructCpHeader                                    *
* Description  : Construct CAPWAP Hdr                                       *
* Input        : pCapwapHdr                                                 *
* Output       : None                                                       *
* Returns      : None                                                       *
*****************************************************************************/
INT4
CapwapConstructCpHeader (tCapwapHdr * pCapwapHdr)
{

    pCapwapHdr->u1Preamble = 0;
    pCapwapHdr->u2HlenRidWbidFlagT = CAPWAP_WLC_HLEN_RID_WBID_FLAGT;
    pCapwapHdr->u1FlagsFLWMKResd = 0;
    pCapwapHdr->u2FragId = 0;
    pCapwapHdr->u2FragOffsetRes3 = 0;
    pCapwapHdr->u1RadMacAddrLength = 0;
    pCapwapHdr->u1WirelessInfoLength = 0;
    pCapwapHdr->u2HdrLen = CAPWAP_HDR_LEN;
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function     : CapwapGetEcnSupport                                        *
* Description  : Gets ECN Support information                               *
* Input        : pEcnSupport, pMsgLen                                       *
* Output       : None                                                       *
* Returns      : None                                                       *
*****************************************************************************/
INT4
CapwapGetEcnSupport (tEcnSupport * pEcnSupport, UINT4 *pMsgLen)
{
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT2               u2IntId = 0;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpEcnSupport = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2IntId;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
        == OSIX_SUCCESS)
    {
        pEcnSupport->u2MsgEleType = ECN_SUPPORT;
        pEcnSupport->u1EcnSupport = pWssIfCapwapDB->CapwapGetDB.u1WtpEcnSupport;
        pEcnSupport->u2MsgEleLen = ECN_SUPPORT_LEN;
        *pMsgLen +=
            (UINT4) (pEcnSupport->u2MsgEleLen + CAPWAP_MSG_ELEM_TYPE_LEN);
        pEcnSupport->isOptional = OSIX_TRUE;
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_SUCCESS;
    }
    else
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to get ECN Support Data\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to get ECN Support Data\r\n"));
        return OSIX_FAILURE;
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapSetRadioInfo                                */
/*  Description     : This function invoked the Radio module to update  */
/*                    the radio type in WTP                             */
/*  Input(s)        : pRcvBuf - received CAPWAP Join response           */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
CapwapSetRadioInfo (UINT1 *pRcvBuf,
                    tCapwapControlPacket * pCwMsg, UINT2 u2MsgIndex)
{
    tRadioIfGetDB       RadioIfGetDB;
    UINT4               u4Offset = 0;
    UINT1               u1RadioId = 0;
    UINT4               u4RadioType = 0;
    UINT2               u2NumMsgElems;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    u2NumMsgElems = pCwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;
    if (u2NumMsgElems > 1)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Radio Info element received more than once \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Radio Info element received more than once \r\n"));
        return OSIX_FAILURE;
    }
    else
    {
        u4Offset = pCwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[0];
        pRcvBuf += u4Offset + CAPWAP_MSG_ELEM_TYPE_LEN;
        CAPWAP_GET_1BYTE (u1RadioId, pRcvBuf);
        CAPWAP_GET_4BYTE (u4RadioType, pRcvBuf);

        RadioIfGetDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_TRUE;
        RadioIfGetDB.RadioIfGetAllDB.u1RadioId = u1RadioId;
        RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType = u4RadioType;

        if (WssIfProcessRadioIfDBMsg (WSS_SET_PHY_RADIO_IF_DB, &RadioIfGetDB)
            == OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to set Radio Type in DB \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to set Radio Type in DB \r\n"));
            return OSIX_FAILURE;
        }
    }
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/***********************************************************************
*  Function Name   : CapwapGetLocalIpAddr                              *
*  Description     : Gets local IP address                             *
*  Input(s)        : pIncomingAddress, pMsgLen                         *
*  Output(s)       : None                                              *
*  Returns         : None                                              *
************************************************************************/
INT4
CapwapGetLocalIpAddr (tLocalIpAddr * pIncomingAddress, UINT4 *pMsgLen)
{
    tWssIfWlcHdlrDB     WssIfWlcHdlrDB;
    UINT1               au1IfName[CAPWAP_WLC_IF_INTERFACE_LEN];

    MEMSET (&WssIfWlcHdlrDB, 0, sizeof (tWssIfWlcHdlrDB));

    STRNCPY ((CHR1 *) au1IfName, CAPWAP_WLC_IF_INTERFACE_NAME,
             sizeof (CAPWAP_WLC_IF_INTERFACE_NAME));

    if (WssIfProcessWlcHdlrDBMsg (WSS_WLCHDLR_GET_LOCAL_IPV4, &WssIfWlcHdlrDB)
        == OSIX_SUCCESS)
    {
        pIncomingAddress->u2MsgEleType = CAPWAP_LOCAL_IPV4_ADDR;
        WssIfGetIfIpInfo ((UINT1 *) au1IfName,
                          &pIncomingAddress->ipAddr.u4_addr[0]);
        pIncomingAddress->u2MsgEleLen = CAPWAP_MSG_ELEM_TYPE_LEN;
        *pMsgLen +=
            (UINT4) (pIncomingAddress->u2MsgEleLen + CAPWAP_MSG_ELEM_TYPE_LEN);
        return OSIX_SUCCESS;
    }
    else
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to get Location Data from AP HDLR DB \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to get Location Data from AP HDLR DB \r\n"));
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/***********************************************************************
*  Function Name   : CapwapGetTransProtocol                            *
*  Description     : Gets transport protocol inform                    *
*  Input(s)        : pTransProto pMsgLen, u2IntId                      *
*  Output(s)       : None                                              *
*  Returns         : None                                              *
************************************************************************/
INT4
CapwapGetTransProtocol (tCapwapTransprotocol * pTransProto, UINT4 *pMsgLen,
                        UINT2 u2IntId)
{
    tWssIfCapDB        *pWssIfCapwapDB = NULL;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    pWssIfCapwapDB->CapwapIsGetAllDB.bTransportProtocol = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2IntId;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB) ==
        OSIX_SUCCESS)
    {
        pTransProto->u2MsgEleType = CAPWAP_TRANSPORT_PROTOCOL;
        pTransProto->transportType = pWssIfCapwapDB->CapwapGetDB.u1ProtocolType;
        pTransProto->u2MsgEleLen = CAPWAP_TRANSPORT_PROTOCOL_LEN;
        *pMsgLen +=
            (UINT4) (pTransProto->u2MsgEleLen + CAPWAP_MSG_ELEM_TYPE_LEN);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_SUCCESS;
    }
    else
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to get Transport Protocol \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to get Transport Protocol \r\n"));
        return OSIX_FAILURE;
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}

/***********************************************************************
*  Function Name   : CapwapGetMaxMessageLen                            *
*  Description     : Gets max length                                   *
*  Input(s)        : pMsgLength, pMsgLen                               *
*  Output(s)       : None                                              *
*  Returns         : None                                              *
************************************************************************/
INT4
CapwapGetMaxMessageLen (tMaxMsgLen * pMsgLength, UINT4 *pMsgLen)
{

    tWssIfWlcHdlrDB     WssIfWlcHdlrDB;
    UNUSED_PARAM (pMsgLength);
    UNUSED_PARAM (pMsgLen);

    MEMSET (&WssIfWlcHdlrDB, 0, sizeof (tWssIfWlcHdlrDB));

    return OSIX_SUCCESS;
}

/***********************************************************************
*  Function Name   : CapwapGetStatsTimer                               *
*  Description     : Gets stats timer val                              *
*  Input(s)        : pStatsTimer, pMsgLen                              *
*  Output(s)       : None                                              *
*  Returns         : None                                              *
************************************************************************/
INT4
CapwapGetStatsTimer (tStatisticsTimer * pStatsTimer, UINT4 *pMsgLen)
{
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT2               u2IntId = 0;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpStatisticsTimer = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2IntId;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
        == OSIX_SUCCESS)
    {
        pStatsTimer->u2MsgEleType = STATS_TIMER;
        pStatsTimer->u2StatsTimer =
            pWssIfCapwapDB->CapwapGetDB.u2WtpStatisticsTimer;
        pStatsTimer->u2MsgEleLen = STATS_TIMER_LEN;
        pStatsTimer->isOptional = OSIX_TRUE;
        *pMsgLen +=
            (UINT4) (pStatsTimer->u2MsgEleLen + CAPWAP_MSG_ELEM_TYPE_LEN);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_SUCCESS;
    }
    else
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to get Location Data from AP HDLR DB \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to get Location Data from AP HDLR DB \r\n"));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}

/***********************************************************************
*  Function Name   : CapwapGetWtpBoardDataFromBuf                      *
*  Description     : Gets WTP data from buf                            *
*  Input(s)        : pRcvBuf, u4Offset, pWtpBoardData                  *
*  Output(s)       : None                                              *
*  Returns         : None                                              *
************************************************************************/
INT4
CapwapGetWtpBoardDataFromBuf (UINT1 *pRcvBuf,
                              UINT4 u4Offset, tWtpBoardData * pWtpBoardData)
{
    UINT2               u2Len = 0;
    UINT2               u2Type = 0;

    CAPWAP_FN_ENTRY ();
    pRcvBuf += u4Offset;
    CAPWAP_GET_2BYTE (pWtpBoardData->u2MsgEleType, pRcvBuf);
    CAPWAP_GET_2BYTE (pWtpBoardData->u2MsgEleLen, pRcvBuf);
    /* get the 4 byte vendor identifier */
    CAPWAP_GET_4BYTE (pWtpBoardData->u4VendorSMI, pRcvBuf);
    u2Len = pWtpBoardData->u2MsgEleLen;
    u2Len = (UINT2) (u2Len - CAPWAP_VENDOR_ID_LEN);    /* Subtract the Vendor identifier length */
    /* Get the board data sub element */
    while (u2Len > 0)
    {
        CAPWAP_GET_2BYTE (u2Type, pRcvBuf);
        switch (u2Type)
        {
            case WTP_MODEL_NUMBER:
                pWtpBoardData->wtpBoardInfo[WTP_MODEL_NUMBER].u2BoardInfoType =
                    WTP_MODEL_NUMBER;
                CAPWAP_GET_2BYTE (pWtpBoardData->wtpBoardInfo[WTP_MODEL_NUMBER].
                                  u2BoardInfoLength, pRcvBuf);
                CAPWAP_GET_NBYTE (pWtpBoardData->wtpBoardInfo[WTP_MODEL_NUMBER].
                                  boardInfoData, pRcvBuf,
                                  pWtpBoardData->wtpBoardInfo[WTP_MODEL_NUMBER].
                                  u2BoardInfoLength);
                pWtpBoardData->wtpBoardInfo[WTP_MODEL_NUMBER].isOptional =
                    OSIX_TRUE;
                u2Len =
                    (UINT2) (u2Len -
                             (pWtpBoardData->wtpBoardInfo[WTP_MODEL_NUMBER].
                              u2BoardInfoLength + CAPWAP_WTPBOARDDATA_LEN));
                break;
            case WTP_SERIAL_NUMBER:
                pWtpBoardData->wtpBoardInfo[WTP_SERIAL_NUMBER].u2BoardInfoType =
                    WTP_SERIAL_NUMBER;
                CAPWAP_GET_2BYTE (pWtpBoardData->
                                  wtpBoardInfo[WTP_SERIAL_NUMBER].
                                  u2BoardInfoLength, pRcvBuf);
                CAPWAP_GET_NBYTE (pWtpBoardData->
                                  wtpBoardInfo[WTP_SERIAL_NUMBER].boardInfoData,
                                  pRcvBuf,
                                  pWtpBoardData->
                                  wtpBoardInfo[WTP_SERIAL_NUMBER].
                                  u2BoardInfoLength);
                u2Len =
                    (UINT2) (u2Len -
                             (pWtpBoardData->wtpBoardInfo[WTP_SERIAL_NUMBER].
                              u2BoardInfoLength + CAPWAP_WTPBOARDDATA_LEN));
                break;
            case WTP_BOARD_ID:
                pWtpBoardData->wtpBoardInfo[WTP_BOARD_ID].u2BoardInfoType =
                    WTP_BOARD_ID;
                CAPWAP_GET_2BYTE (pWtpBoardData->wtpBoardInfo[WTP_BOARD_ID].
                                  u2BoardInfoLength, pRcvBuf);
                CAPWAP_GET_NBYTE (pWtpBoardData->wtpBoardInfo[WTP_BOARD_ID].
                                  boardInfoData, pRcvBuf,
                                  pWtpBoardData->wtpBoardInfo[WTP_BOARD_ID].
                                  u2BoardInfoLength);
                u2Len =
                    (UINT2) (u2Len - (pWtpBoardData->wtpBoardInfo[WTP_BOARD_ID].
                                      u2BoardInfoLength +
                                      CAPWAP_WTPBOARDDATA_LEN));
                break;
            case WTP_BOARD_RIVISION:
                pWtpBoardData->wtpBoardInfo[WTP_BOARD_RIVISION].
                    u2BoardInfoType = WTP_BOARD_RIVISION;
                CAPWAP_GET_2BYTE (pWtpBoardData->
                                  wtpBoardInfo[WTP_BOARD_RIVISION].
                                  u2BoardInfoLength, pRcvBuf);
                CAPWAP_GET_NBYTE (pWtpBoardData->
                                  wtpBoardInfo[WTP_BOARD_RIVISION].
                                  boardInfoData, pRcvBuf,
                                  pWtpBoardData->
                                  wtpBoardInfo[WTP_BOARD_RIVISION].
                                  u2BoardInfoLength);
                u2Len =
                    (UINT2) (u2Len -
                             (pWtpBoardData->wtpBoardInfo[WTP_BOARD_RIVISION].
                              u2BoardInfoLength + CAPWAP_WTPBOARDDATA_LEN));
                break;
            case WTP_BASE_MAC_ADDRESS:
                pWtpBoardData->wtpBoardInfo[WTP_BASE_MAC_ADDRESS].
                    u2BoardInfoType = WTP_BASE_MAC_ADDRESS;
                CAPWAP_GET_2BYTE (pWtpBoardData->
                                  wtpBoardInfo[WTP_BASE_MAC_ADDRESS].
                                  u2BoardInfoLength, pRcvBuf);
                CAPWAP_GET_NBYTE (pWtpBoardData->
                                  wtpBoardInfo[WTP_BASE_MAC_ADDRESS].
                                  boardInfoData, pRcvBuf,
                                  pWtpBoardData->
                                  wtpBoardInfo[WTP_BASE_MAC_ADDRESS].
                                  u2BoardInfoLength);
                pWtpBoardData->wtpBoardInfo[WTP_BASE_MAC_ADDRESS].isOptional =
                    OSIX_TRUE;
                u2Len =
                    (UINT2) (u2Len -
                             (pWtpBoardData->wtpBoardInfo[WTP_BASE_MAC_ADDRESS].
                              u2BoardInfoLength + 4));
                break;
            default:
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "CapwapGetWtpBoardDataFromBuf: Received unknow Board Info type \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "CapwapGetWtpBoardDataFromBuf: Received unknow Board Info type \r\n"));
                return OSIX_FAILURE;

        }
    }

    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/***********************************************************************
*  Function Name   : CapwapGetResultCode                               *
*  Description     : Gets Result code local buffer                     *
*  Input(s)        : pRcvBuf, u4Offset, pResCode, u2NumMsgElems        *
*  Output(s)       : None                                              *
*  Returns         : None                                              *
************************************************************************/
INT4
CapwapGetResultCodeFromBuf (UINT1 *pRcvBuf,
                            UINT4 u4Offset,
                            tResCode * pResCode, UINT2 u2NumMsgElems)
{
    pRcvBuf += u4Offset;
    CAPWAP_GET_2BYTE (pResCode->u2MsgEleType, pRcvBuf);
    CAPWAP_GET_2BYTE (pResCode->u2MsgEleLen, pRcvBuf);
    CAPWAP_GET_4BYTE (pResCode->u4Value, pRcvBuf);
    UNUSED_PARAM (u2NumMsgElems);
    return OSIX_SUCCESS;
}

/***********************************************************************
*  Function Name   : CapwapGetEcnSupportFromBuf                        *
*  Description     : Gets ECN support local buffer                     *
*  Input(s)        : pRcvBuf, u4Offset, ecnSupport, u2NumMsgElems      *
*  Output(s)       : None                                              *
*  Returns         : None                                              *
************************************************************************/
INT4
CapwapGetEcnSupportFromBuf (UINT1 *pRcvBuf,
                            UINT4 u4Offset,
                            tEcnSupport * ecnSupport, UINT2 u2NumMsgElems)
{
    pRcvBuf += u4Offset;
    CAPWAP_GET_2BYTE (ecnSupport->u2MsgEleType, pRcvBuf);
    CAPWAP_GET_2BYTE (ecnSupport->u2MsgEleLen, pRcvBuf);
    CAPWAP_GET_1BYTE (ecnSupport->u1EcnSupport, pRcvBuf);
    UNUSED_PARAM (u2NumMsgElems);
    return OSIX_SUCCESS;

}

/***********************************************************************
*  Function Name   : CapwapGetLocalIPAddrFromBuf                       *
*  Description     : Gets IP from locall buffer                        *
*  Input(s)        : pRcvBuf, u4Offset, pLocalIpAddr, u2NumMsgElems    *
*  Output(s)       : None                                              *
*  Returns         : None                                              *
************************************************************************/
INT4
CapwapGetLocalIPAddrFromBuf (UINT1 *pRcvBuf,
                             UINT4 u4Offset,
                             tLocalIpAddr * pLocalIpAddr, UINT2 u2NumMsgElems)
{
    pRcvBuf += u4Offset;
    CAPWAP_GET_2BYTE (pLocalIpAddr->u2MsgEleType, pRcvBuf);
    CAPWAP_GET_2BYTE (pLocalIpAddr->u2MsgEleLen, pRcvBuf);
    if (pLocalIpAddr->u2MsgEleType == CAPWAP_LOCAL_IPV4_ADDR)
    {
        CAPWAP_GET_4BYTE (pLocalIpAddr->ipAddr.u4_addr[0], pRcvBuf);
    }

    UNUSED_PARAM (u2NumMsgElems);
    return OSIX_SUCCESS;
}

/***********************************************************************
*  Function Name   : CapwapValidateLocationData                        *
*  Description     : Validates location data                           *
*  Input(s)        : pRcvBuf, pcwMsg, u2MsgIndex                       *
*  Output(s)       : None                                              *
*  Returns         : None                                              *
************************************************************************/
INT4
CapwapValidateLocationData (UINT1 *pRcvBuf,
                            tCapwapControlPacket * pCwMsg, UINT2 u2MsgIndex)
{
    UINT2               u2NumMsgElems;
    UINT4               u4Offset;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT2               u2Type, u2Len;

    CAPWAP_FN_ENTRY ();
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    u2NumMsgElems = pCwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;
    u4Offset = pCwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[0];
    if (u2NumMsgElems > 1)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "The Message element Location Data Received More than once \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "The Message element Location Data Received More than once \r\n"));
        return OSIX_FAILURE;
    }
    else
    {
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpLocation = OSIX_TRUE;
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = pCwMsg->u2IntProfileId;
        pRcvBuf += u4Offset;
        CAPWAP_GET_2BYTE (u2Type, pRcvBuf);
        CAPWAP_GET_2BYTE (u2Len, pRcvBuf);
        CAPWAP_GET_NBYTE (pWssIfCapwapDB->CapwapGetDB.au1WtpLocation,
                          pRcvBuf, u2Len);
#ifdef IMPLEMENTATION_REQ
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_VALIDATE_DB, pWssIfCapwapDB)
            == OSIX_FAILURE)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to Validate the Location Data \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to Validate the Location Data \r\n"));
            return OSIX_FAILURE;
        }
#endif
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/***********************************************************************
*  Function Name   : CapwapValidateImageId                             *
*  Description     : Validates image ID                                *
*  Input(s)        : pRcvBuf, pcwMsg, u2MsgIndex                       *
*  Output(s)       : None                                              *
*  Returns         : None                                              *
************************************************************************/
INT4
CapwapValidateImageId (UINT1 *pRcvBuf,
                       tCapwapControlPacket * cwMsg, UINT2 u2MsgIndex)
{
    UINT4               u4Offset;
    UINT2               u2NumMsgElems;
    tWtpProfile         WtpProfile;

    MEMSET (&WtpProfile, 0, sizeof (tWtpProfile));

    u2NumMsgElems = cwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;
    if (u2NumMsgElems > 1)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "The Message element Image Identifier Received More than once \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "The Message element Image Identifier Received More than once \r\n"));
        return OSIX_FAILURE;
    }
    else
    {
        u4Offset = cwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[0];
        pRcvBuf += u4Offset;
        /* pImageId is NULL */
    }
    return OSIX_SUCCESS;
}

/***********************************************************************
*  Function Name   : CapwapValidateDataTransferMode                    *
*  Description     : Validates Vendor spec Id                          *
*  Input(s)        : pRcvBuf, pcwMsg, u2MsgIndex                       *
*  Output(s)       : None                                              *
*  Returns         : None                                              *
************************************************************************/
INT4
CapwapValidateDataTransferMode (UINT1 *pRcvBuf,
                                tCapwapControlPacket * cwMsg, UINT2 u2MsgIndex)
{
    UINT4               u4Offset;
    UINT2               u2NumMsgElems;
    tWtpProfile         WtpProfile;
    tDataTransferMode   DataTransfer;

    MEMSET (&WtpProfile, 0, sizeof (tWtpProfile));
    MEMSET (&DataTransfer, 0, sizeof (tDataTransferMode));

    u2NumMsgElems = cwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;
    if (u2NumMsgElems > 1)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "The Message element Data Transfer Mode Received More than once \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "The Message element Data Transfer Mode Received More than once \r\n"));
        return OSIX_FAILURE;
    }
    else
    {
        u4Offset = cwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[0];
        pRcvBuf += u4Offset;
        CAPWAP_GET_2BYTE (DataTransfer.u2MsgEleType, pRcvBuf);
        CAPWAP_GET_2BYTE (DataTransfer.u2MsgEleLen, pRcvBuf);
        CAPWAP_GET_1BYTE (DataTransfer.DataMode, pRcvBuf);

    }
    return OSIX_SUCCESS;
}

/***********************************************************************
*  Function Name   : CapwapValidateVendSpecPld                         *
*  Description     : Validates Vendor spec Id                          *
*  Input(s)        : pRcvBuf, pcwMsg, u2MsgIndex,pVendor               *
*  Output(s)       : None                                              *
*  Returns         : None                                              *
************************************************************************/
INT4
CapwapValidateVendSpecPld (UINT1 *pRcvBuf,
                           tCapwapControlPacket * cwMsg,
                           tVendorSpecPayload * pVendor, UINT2 u2MsgIndex)
{
    UINT2               u2NumMsgElems;
    tWtpProfile         WtpProfile;
    UNUSED_PARAM (pRcvBuf);
    UNUSED_PARAM (pVendor);
    MEMSET (&WtpProfile, 0, sizeof (tWtpProfile));

    u2NumMsgElems = cwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;
    if (u2NumMsgElems > 1)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "The Message element Vendor specific payload Received More than once \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "The Message element Vendor specific payload Received More than once \r\n"));
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/***********************************************************************
*  Function Name   : CapwapValidateResultCode                          *
*  Description     : Validates Result code                             *
*  Input(s)        : pRcvBuf, pcwMsg, u2MsgIndex                       *
*  Output(s)       : None                                              *
*  Returns         : None                                              *
************************************************************************/
INT4
CapwapValidateResultCode (UINT1 *pRcvBuf,
                          tCapwapControlPacket * cwMsg,
                          tResCode * pResult, UINT2 u2MsgIndex)
{
    UINT4               u4Offset;
    UINT2               u2NumMsgElems;
    tWtpProfile         WtpProfile;

    MEMSET (&WtpProfile, 0, sizeof (tWtpProfile));

    u2NumMsgElems = cwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;
    if (u2NumMsgElems > 1)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "The Message element result Code Received More than once \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "The Message element result Code Received More than once \r\n"));
        return OSIX_FAILURE;
    }
    else
    {
        u4Offset = cwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[0];
        pRcvBuf += u4Offset;
        CAPWAP_GET_2BYTE (pResult->u2MsgEleType, pRcvBuf);
        CAPWAP_GET_2BYTE (pResult->u2MsgEleLen, pRcvBuf);
        CAPWAP_GET_4BYTE (pResult->u4Value, pRcvBuf);
    }
    return OSIX_SUCCESS;
}

/***********************************************************************
*  Function Name   : CapwapValidateWtpName                             *
*  Description     : Validates WTP name                                *
*  Input(s)        : pRcvBuf, pcwMsg, u2MsgIndex                       *
*  Output(s)       : None                                              *
*  Returns         : None                                              *
************************************************************************/
INT4
CapwapValidateWtpName (UINT1 *pRcvBuf,
                       tCapwapControlPacket * cwMsg, UINT2 u2MsgIndex)
{
    UINT2               u2NumMsgElems;
    tWtpName            WtpName;

    CAPWAP_FN_ENTRY ();
    MEMSET (&WtpName, 0, sizeof (tWtpName));
    u2NumMsgElems = cwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;
    if (u2NumMsgElems > 1)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "The Message element WTP Name Received More than once \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "The Message element WTP Name Received More than once \r\n"));
        return OSIX_FAILURE;
    }
    else
    {
        CAPWAP_GET_2BYTE (WtpName.u2MsgEleType, pRcvBuf);
        CAPWAP_GET_2BYTE (WtpName.u2MsgEleLen, pRcvBuf);
        CAPWAP_GET_NBYTE (WtpName.wtpName, pRcvBuf, WtpName.u2MsgEleLen);
    }
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/***********************************************************************
*  Function Name   : CapwapValidateMtuDiscPad                          *
*  Description     : Validates MTU disc pad                            *
*  Input(s)        : pRcvBuf, pcwMsg, u2MsgIndex                       *
*  Output(s)       : None                                              *
*  Returns         : None                                              *
************************************************************************/
INT4
CapwapValidateMtuDiscPad (UINT1 *pRcvBuf,
                          tCapwapControlPacket * cwMsg, UINT2 u2MsgIndex)
{

    UINT2               u2NumMsgElems;

    CAPWAP_FN_ENTRY ();
    u2NumMsgElems = cwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;
    if (u2NumMsgElems > 1)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "The Message element MTU discovery padding Received More than once \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "The Message element MTU discovery padding Received More than once \r\n"));
        return OSIX_FAILURE;
    }
    else
    {
        UNUSED_PARAM (pRcvBuf);
    }
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;

}

/***********************************************************************
*  Function Name   : CapwapValidateWtpSessionId                        *
*  Description     : Validates WTP session ID                          *
*  Input(s)        : pRcvBuf, pcwMsg, u2MsgIndex                       *
*  Output(s)       : None                                              *
*  Returns         : None                                              *
************************************************************************/
INT4
CapwapValidateWtpSessionId (UINT1 *pRcvBuf,
                            tCapwapControlPacket * pCwMsg,
                            tRemoteSessionManager * pSess, UINT2 u2MsgIndex)
{

    UINT4               u4Offset;
    UINT2               u2Type, u2Len;
    UINT1               u1Index;
    UINT1              *pTmpRcvBuf = NULL;
    UINT2               u2NumMsgElems;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;

    CAPWAP_FN_ENTRY ();

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    u2NumMsgElems = pCwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;
    if (u2NumMsgElems > 1)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "The Message element Sess Id Received More than once \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "The Message element Sess Id Received More than once \r\n"));
        return OSIX_FAILURE;
    }
    else
    {
        u4Offset = pCwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[0];
        pRcvBuf += u4Offset;
        CAPWAP_GET_2BYTE (u2Type, pRcvBuf);
        CAPWAP_GET_2BYTE (u2Len, pRcvBuf);
        pTmpRcvBuf = pRcvBuf;
        for (u1Index = 0; u1Index < 4; u1Index++)
        {
            CAPWAP_GET_4BYTE (pWssIfCapwapDB->JoinSessionID[u1Index], pRcvBuf);
        }
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_RSM_FROM_SESSIONID,
                                     pWssIfCapwapDB) == OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "The Received Join Session Id Already in use \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "The Received Join Session Id Already in use \r\n"));
            pCwMsg->u4ReturnCode = JOIN_FAILURE_SESSIONID_INUSE;
        }
        else
        {
            for (u1Index = 0; u1Index < 4; u1Index++)
            {
                /* covert from network byte order to host byte order */
                CAPWAP_GET_4BYTE (pSess->JoinSessionID[u1Index], pTmpRcvBuf);
            }
        }
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/***********************************************************************
*  Function Name   : CapwapValidateEcnSupport                           *
*  Description     : Validates ECN support                             *
*  Input(s)        : pRcvBuf, pcwMsg, u2MsgIndex                       *
*  Output(s)       : None                                              *
*  Returns         : None                                              *
************************************************************************/
INT4
CapwapValidateEcnSupport (UINT1 *pRcvBuf,
                          tCapwapControlPacket * pCwMsg, UINT2 u2MsgIndex)
{
    UINT2               u2NumMsgElems;
    UINT4               u4Offset;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    u2NumMsgElems = pCwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;
    u4Offset = pCwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[0];
    if (u2NumMsgElems > 1)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "The Message element Ecn Support Received More than once \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "The Message element Ecn Support Received More than once \r\n"));
        return OSIX_FAILURE;
    }
    else
    {
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpEcnSupport = OSIX_TRUE;
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = pCwMsg->u2IntProfileId;
        pRcvBuf += u4Offset + CAPWAP_MSG_ELEM_TYPE_LEN;
        CAPWAP_GET_1BYTE (pWssIfCapwapDB->CapwapGetDB.u1WtpEcnSupport, pRcvBuf);
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_VALIDATE_DB, pWssIfCapwapDB)
            == OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to Validate the Received ECN support \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to Validate the Received ECN support \r\n"));
            /* If ECN Support does not match don't return failure */
            /* return OSIX_FAILURE; */
        }
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/***********************************************************************
*  Function Name   : CapwapValidateAcName                              *
*  Description     : Validates AC name                                 *
*  Input(s)        : pRcvBuf, pcwMsg, u2MsgIndex                       *
*  Output(s)       : None                                              *
*  Returns         : None                                              *
************************************************************************/
INT4
CapwapValidateAcName (UINT1 *pRcvBuf,
                      tCapwapControlPacket * pCwMsg, UINT2 u2MsgIndex)
{

    UINT2               u2NumMsgElems;
    tACName             ACName;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;

    CAPWAP_FN_ENTRY ();
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (&ACName, 0, sizeof (tACName));
    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    u2NumMsgElems = pCwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;
    if (u2NumMsgElems > 1)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "The Message element AC Name Received More than once \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "The Message element AC Name Received More than once \r\n"));
        return OSIX_FAILURE;
    }
    else
    {
        CAPWAP_GET_2BYTE (ACName.u2MsgEleType, pRcvBuf);
        CAPWAP_GET_2BYTE (ACName.u2MsgEleLen, pRcvBuf);
        CAPWAP_GET_NBYTE (ACName.wlcName, pRcvBuf, ACName.u2MsgEleLen);
        MEMCPY (pWssIfCapwapDB->au1WlcName, ACName.wlcName, ACName.u2MsgEleLen);
        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = pCwMsg->u2IntProfileId;
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
        pWssIfCapwapDB->CapwapIsGetAllDB.bAcName = OSIX_TRUE;
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_VALIDATE_DB, pWssIfCapwapDB)
            == OSIX_FAILURE)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "The Received WLC Name from WTP is not maching with the configured Name \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "The Received WLC Name from WTP is not maching with the configured Name \r\n"));
            return OSIX_SUCCESS;
        }
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/***********************************************************************
*  Function Name   : CapwapValidateRadioAdminState                     *
*  Description     : Validates Radio Admin state                       *
*  Input(s)        : pRcvBuf, pcwMsg, pRadioAdmin, u2MsgIndex          *
*  Output(s)       : None                                              *
*  Returns         : None                                              *
************************************************************************/
INT4
CapwapValidateRadioAdminState (UINT1 *pRcvBuf, tCapwapControlPacket * pcwMsg,
                               tRadioIfAdminStatus * pRadioAdmin,
                               UINT2 u2MsgIndex)
{

    UINT1               u1Index;
    UINT1              *pBuf = NULL;
    UINT2               u2NumMsgElems;
    UINT4               u4Offset;

    CAPWAP_FN_ENTRY ();
    tRadioIfMsgStruct   pWssMsgStruct;
    pBuf = pRcvBuf;

    MEMSET (&pWssMsgStruct, 0, sizeof (tRadioIfMsgStruct));

    u2NumMsgElems = pcwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;

    for (u1Index = 1; u1Index <= u2NumMsgElems; u1Index++)
    {
        u4Offset = pcwMsg->capwapMsgElm[u2MsgIndex + u1Index].pu2Offset[0];
        pRcvBuf = pBuf;
        pRcvBuf += u4Offset;

        CAPWAP_GET_2BYTE (pRadioAdmin->u2MessageType, pRcvBuf);
        CAPWAP_GET_2BYTE (pRadioAdmin->u2MessageLength, pRcvBuf);
        CAPWAP_GET_1BYTE (pRadioAdmin->u1RadioId, pRcvBuf);
        CAPWAP_GET_1BYTE (pRadioAdmin->u1AdminStatus, pRcvBuf);

        pWssMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfAdminStatus.
            u2MessageType = pRadioAdmin->u2MessageType;
        pWssMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfAdminStatus.
            u2MessageLength = pRadioAdmin->u2MessageLength;
        pWssMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfAdminStatus.
            u1RadioId = pRadioAdmin->u1RadioId;
        pWssMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfAdminStatus.
            u1AdminStatus = pRadioAdmin->u1AdminStatus;
        pWssMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.u2WtpInternalId =
            pcwMsg->u2IntProfileId;
        pWssMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfAdminStatus.
            isPresent = OSIX_TRUE;
        if (u2MsgIndex == RADIO_ADMIN_STATE_CONFIG_UPDATE_REQ_INDEX)
        {
            if (WssIfProcessRadioIfMsg (WSS_RADIOIF_CONFIG_UPDATE_REQ_VALIDATE,
                                        &pWssMsgStruct) == OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Config update request validation failed for Radio Admin State \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Config update request validation failed for Radio Admin State \r\n"));
                return OSIX_SUCCESS;
            }
        }
    }
    return OSIX_SUCCESS;
}

/***********************************************************************
*  Function Name   : CapwapSetRadioAdminState                          *
*  Description     : Sets Radio Admin state                            *
*  Input(s)        : pRcvBuf, pcwMsg, pRadioAdmin, u2MsgIndex          *
*  Output(s)       : None                                              *
*  Returns         : None                                              *
************************************************************************/
INT4
CapwapSetRadioAdminState (UINT1 *pRcvBuf, tCapwapControlPacket * pcwMsg,
                          tRadioIfAdminStatus * pRadioAdmin, UINT2 u2MsgIndex)
{

    UINT1               u1Index;
    UINT1              *pBuf = NULL;
    UINT2               u2NumMsgElems;
    UINT4               u4Offset;

    CAPWAP_FN_ENTRY ();
    tRadioIfMsgStruct   pWssMsgStruct;
    pBuf = pRcvBuf;

    MEMSET (&pWssMsgStruct, 0, sizeof (tRadioIfMsgStruct));

    u2NumMsgElems = pcwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;

    for (u1Index = 0; u1Index < u2NumMsgElems; u1Index++)
    {
        u4Offset = pcwMsg->capwapMsgElm[u2MsgIndex + u1Index].pu2Offset[0];
        pRcvBuf = pBuf;
        pRcvBuf += u4Offset;

        CAPWAP_GET_2BYTE (pRadioAdmin->u2MessageType, pRcvBuf);
        CAPWAP_GET_2BYTE (pRadioAdmin->u2MessageLength, pRcvBuf);
        CAPWAP_GET_1BYTE (pRadioAdmin->u1RadioId, pRcvBuf);
        CAPWAP_GET_1BYTE (pRadioAdmin->u1AdminStatus, pRcvBuf);

        pWssMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfAdminStatus.
            u2MessageType = pRadioAdmin->u2MessageType;
        pWssMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfAdminStatus.
            u2MessageLength = pRadioAdmin->u2MessageLength;
        pWssMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfAdminStatus.
            u1RadioId = pRadioAdmin->u1RadioId;
        pWssMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfAdminStatus.
            u1AdminStatus = pRadioAdmin->u1AdminStatus;
        pWssMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.u2WtpInternalId =
            pcwMsg->u2IntProfileId;
        pWssMsgStruct.unRadioIfMsg.RadioIfConfigUpdateReq.RadioIfAdminStatus.
            isPresent = OSIX_TRUE;

        if (u2MsgIndex == RADIO_ADMIN_STATE_CONFIG_UPDATE_REQ_INDEX)
        {
            if (WssIfProcessRadioIfMsg (WSS_RADIOIF_CONFIG_UPDATE_REQ_VALIDATE,
                                        &pWssMsgStruct) == OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Failed to set Radio admin state in Radio DB \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to set Radio admin state in Radio DB \r\n"));
                return OSIX_SUCCESS;
            }
        }

    }
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapValidateIpAddress                           */
/*  Description     : This function validates the received source IP    */
/*                    with the user configured while list.              */
/*  Input(s)        : discReqMsg - parsed CAPWAP Discovery request      */
/*                    pRcvBuf - Received discovery packet               */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
CapwapValidateIpAddress (UINT4 u4SrtIp)
{
    /*Get user configured black list from WSS-WLC module
       validate the u4SrcIP with the user configured black list */

    /*Get user configured white list from WSS-WLC module
       Validate white list also ?? */
    UNUSED_PARAM (u4SrtIp);
    return OSIX_SUCCESS;
}

/***********************************************************************
*  Function Name   : CapwapGetWtpBaseMacAddr                           *
*  Description     : Gets MAC base address                             *
*  Input(s)        : pBuf, pJoinReqMsg, wtpBaseMacAddr                 *
*  Output(s)       : None                                              *
*  Returns         : None                                              *
************************************************************************/
INT4
CapwapGetWtpBaseMacAddr (tCRU_BUF_CHAIN_HEADER * pBuf,
                         tCapwapControlPacket * pJoinReqMsg,
                         tMacAddr wtpBaseMacAddr)
{
    UINT4               u4Offset;
    UINT2               u2NumMsgElems = 0, u2Length;
    tWtpBoardData       wtpBoardData;
    UINT1              *pRcvBuf = NULL;
    UINT1               au1Frame[CAPWAP_MAX_PKT_LEN];

    CAPWAP_FN_ENTRY ();
    MEMSET (&wtpBoardData, 0, sizeof (tWtpBoardData));

    /* Get the WTP Board Data */
    u2NumMsgElems =
        pJoinReqMsg->capwapMsgElm[WTP_BOARD_DATA_JOIN_REQ_INDEX].
        numMsgElemInstance;
    u2Length =
        pJoinReqMsg->capwapMsgElm[WTP_BOARD_DATA_JOIN_REQ_INDEX].u2Length;
    if (u2NumMsgElems > 1)
    {
        CAPWAP_TRC (CAPWAP_MGMT_TRC,
                    "Wtp Board Data is repeated in Join Request Message \r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                      "Wtp Board Data is repeated in Join Request Message \r\n"));
        return OSIX_FAILURE;
    }
    else
    {
        u4Offset =
            pJoinReqMsg->capwapMsgElm[WTP_BOARD_DATA_JOIN_REQ_INDEX].
            pu2Offset[u2NumMsgElems - 1];
        pRcvBuf = CAPWAP_BUF_IF_LINEAR (pBuf, 0, u2Length);
        if (pRcvBuf == NULL)
        {
            MEMSET (au1Frame, 0x00, u2Length);
            CAPWAP_COPY_FROM_BUF (pBuf, au1Frame, 0, u2Length);
            pRcvBuf = au1Frame;
        }
        if (CapwapGetWtpBoardDataFromBuf (pRcvBuf, u4Offset, &wtpBoardData)
            == OSIX_SUCCESS)
        {
            MEMCPY (wtpBaseMacAddr,
                    wtpBoardData.wtpBoardInfo[WTP_BASE_MAC_ADDRESS].
                    boardInfoData,
                    wtpBoardData.wtpBoardInfo[WTP_BASE_MAC_ADDRESS].
                    u2BoardInfoLength);
        }
    }
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/***********************************************************************
*  Function Name   : CapwapValidateRadioOperState                      *
*  Description     : Validates radio operational state                 *
*  Input(s)        : pRcvBuf, pcwMsg, pradioOper, u2MsgIndex           *
*  Output(s)       : None                                              *
*  Returns         : None                                              *
************************************************************************/
INT4
CapwapValidateRadioOperState (UINT1 *pRcvBuf, tCapwapControlPacket * pcwMsg,
                              tRadioIfOperStatus * pradioOper, UINT2 u2MsgIndex)
{

    UINT1               u1Index;
    UINT1              *pBuf = NULL;
    UINT4               u4Offset;
    UINT2               u2NumMsgElems;

    CAPWAP_FN_ENTRY ();

    tRadioIfMsgStruct   pWssMsgStruct;
    pBuf = pRcvBuf;
    MEMSET (&pWssMsgStruct, 0, sizeof (tRadioIfMsgStruct));
    u2NumMsgElems = pcwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;

    for (u1Index = 0; u1Index < u2NumMsgElems; u1Index++)
    {
        u4Offset = pcwMsg->capwapMsgElm[u2MsgIndex + u1Index].pu2Offset[0];
        pRcvBuf = pBuf;
        pRcvBuf += u4Offset;

        CAPWAP_GET_2BYTE (pradioOper->u2MessageType, pRcvBuf);
        CAPWAP_GET_2BYTE (pradioOper->u2MessageLength, pRcvBuf);
        CAPWAP_GET_1BYTE (pradioOper->u1RadioId, pRcvBuf);
        CAPWAP_GET_1BYTE (pradioOper->u1OperStatus, pRcvBuf);
        CAPWAP_GET_1BYTE (pradioOper->u1FailureCause, pRcvBuf);
        CAPWAP_GET_2BYTE (pcwMsg->u2IntProfileId, pRcvBuf);

        pWssMsgStruct.unRadioIfMsg.RadioIfChangeStateEventReq.RadioIfOperStatus.
            u2MessageType = pradioOper->u2MessageType;
        pWssMsgStruct.unRadioIfMsg.RadioIfChangeStateEventReq.RadioIfOperStatus.
            u2MessageLength = pradioOper->u2MessageLength;
        pWssMsgStruct.unRadioIfMsg.RadioIfChangeStateEventReq.RadioIfOperStatus.
            u1RadioId = pradioOper->u1RadioId;
        pWssMsgStruct.unRadioIfMsg.RadioIfChangeStateEventReq.RadioIfOperStatus.
            u1OperStatus = pradioOper->u1OperStatus;
        pWssMsgStruct.unRadioIfMsg.RadioIfChangeStateEventReq.RadioIfOperStatus.
            u1FailureCause = pradioOper->u1FailureCause;
        pWssMsgStruct.unRadioIfMsg.RadioIfChangeStateEventReq.u2WtpInternalId =
            pcwMsg->u2IntProfileId;
        pWssMsgStruct.unRadioIfMsg.RadioIfChangeStateEventReq.RadioIfOperStatus.
            isPresent = OSIX_TRUE;

        if (u2MsgIndex == RADIO_OPER_STATE_EVENT_REQ_INDEX)
        {
            if (WssIfProcessRadioIfMsg
                (WSS_RADIOIF_CHANGE_STATE_EVT_REQ_VALIDATE,
                 &pWssMsgStruct) == OSIX_FAILURE)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Failed to Process Receieved State Event Request\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to Process Receieved State Event Request\r\n"));
                return OSIX_FAILURE;
            }
        }
        else if (u2MsgIndex == RADIO_OPER_STATE_CONF_UPDATE_RESP_INDEX)
        {
            if (WssIfProcessRadioIfMsg (WSS_RADIOIF_CONFIG_UPDATE_RSP_VALIDATE,
                                        &pWssMsgStruct) == OSIX_FAILURE)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Failed to Process Receieved Configuration Update Response \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to Process Receieved Configuration Update Response \r\n"));
                return OSIX_FAILURE;
            }
        }
    }
    return OSIX_SUCCESS;
}

/***********************************************************************
*  Function Name   : CapwapSetRadioOperState                           *
*  Description     : Sets operational State                            *
*  Input(s)        : pRcvBuf, pcwMsg, pradioOper, u2MsgIndex           *
*  Output(s)       : None                                              *
*  Returns         : None                                              *
************************************************************************/
INT4
CapwapSetRadioOperState (UINT1 *pRcvBuf, tCapwapControlPacket * pcwMsg,
                         tRadioIfOperStatus * pradioOper, UINT2 u2MsgIndex)
{

    UINT1               u1Index;
    UINT1              *pBuf = NULL;
    UINT4               u4Offset;
    UINT2               u2NumMsgElems;

    CAPWAP_FN_ENTRY ();

    tRadioIfMsgStruct   pWssMsgStruct;
    pBuf = pRcvBuf;
    MEMSET (&pWssMsgStruct, 0, sizeof (tRadioIfMsgStruct));
    u2NumMsgElems = pcwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;

    for (u1Index = 0; u1Index < u2NumMsgElems; u1Index++)
    {
        u4Offset = pcwMsg->capwapMsgElm[u2MsgIndex + u1Index].pu2Offset[0];
        pRcvBuf = pBuf;
        pRcvBuf += u4Offset;

        CAPWAP_GET_2BYTE (pradioOper->u2MessageType, pRcvBuf);
        CAPWAP_GET_2BYTE (pradioOper->u2MessageLength, pRcvBuf);
        CAPWAP_GET_1BYTE (pradioOper->u1RadioId, pRcvBuf);
        CAPWAP_GET_1BYTE (pradioOper->u1OperStatus, pRcvBuf);
        CAPWAP_GET_1BYTE (pradioOper->u1FailureCause, pRcvBuf);
        CAPWAP_GET_2BYTE (pcwMsg->u2IntProfileId, pRcvBuf);

        pWssMsgStruct.unRadioIfMsg.RadioIfChangeStateEventReq.RadioIfOperStatus.
            u2MessageType = pradioOper->u2MessageType;
        pWssMsgStruct.unRadioIfMsg.RadioIfChangeStateEventReq.RadioIfOperStatus.
            u2MessageLength = pradioOper->u2MessageLength;
        pWssMsgStruct.unRadioIfMsg.RadioIfChangeStateEventReq.RadioIfOperStatus.
            u1RadioId = pradioOper->u1RadioId;
        pWssMsgStruct.unRadioIfMsg.RadioIfChangeStateEventReq.RadioIfOperStatus.
            u1OperStatus = pradioOper->u1OperStatus;
        pWssMsgStruct.unRadioIfMsg.RadioIfChangeStateEventReq.RadioIfOperStatus.
            u1FailureCause = pradioOper->u1FailureCause;
        pWssMsgStruct.unRadioIfMsg.RadioIfChangeStateEventReq.u2WtpInternalId =
            pcwMsg->u2IntProfileId;
        pWssMsgStruct.unRadioIfMsg.RadioIfChangeStateEventReq.RadioIfOperStatus.
            isPresent = OSIX_TRUE;

        if (u2MsgIndex == RADIO_OPER_STATE_EVENT_REQ_INDEX)
        {
            if (WssIfProcessRadioIfMsg
                (WSS_RADIOIF_CHANGE_STATE_EVT_REQ_VALIDATE,
                 &pWssMsgStruct) == OSIX_SUCCESS)
            {
                return OSIX_SUCCESS;
            }
        }
        else if (u2MsgIndex == RADIO_OPER_STATE_CONF_UPDATE_RESP_INDEX)
        {
            if (WssIfProcessRadioIfMsg (WSS_RADIOIF_CONFIG_UPDATE_RSP_VALIDATE,
                                        &pWssMsgStruct) == OSIX_SUCCESS)
            {
                return OSIX_SUCCESS;
            }
        }

    }
    return OSIX_SUCCESS;
}

/***********************************************************************
*  Function Name   : CapwapGetIeeeRadioInformation                     *
*  Description     : Gets IEEE Radio information                       *
*  Input(s)        : pRadioIfInfo, pMsgLen, u2IntProfileId             *
*  Output(s)       : None                                              *
*  Returns         : None                                              *
************************************************************************/

INT4
CapwapGetIeeeRadioInformation (tRadioIfInfo * pRadioIfInfo, UINT4 *pMsgLen,
                               UINT2 u2IntProfileId)
{
    UINT2               u2NumOfRadios = 0;
    UINT1               u1Index = 0;
    tRadioIfGetDB       RadioIfGetDB;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;

    CAPWAP_FN_ENTRY ();

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2IntProfileId;
    pWssIfCapwapDB->CapwapIsGetAllDB.bNoOfRadio = OSIX_TRUE;

    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB) !=
        OSIX_SUCCESS)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to get IEEE Radio Information element\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to get IEEE Radio Information element\r\n"));
        return OSIX_FAILURE;
    }
    u2NumOfRadios = pWssIfCapwapDB->CapwapGetDB.u1WtpNoofRadio;

    pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;

    u2NumOfRadios = RADIOIF_START_RADIOID;
    for (u1Index = 1; u1Index <= u2NumOfRadios; u1Index++)
    {
        pWssIfCapwapDB->CapwapGetDB.u1RadioId = u1Index;

        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
            != OSIX_SUCCESS)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to get IEEE Radio Information element\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to get IEEE Radio Information element\r\n"));
            return OSIX_FAILURE;
        }

        RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
            UtlShMemAllocAntennaSelectionBuf ();

        if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "\r%% Memory allocation for Antenna"
                        " selection failed.\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "\r%% Memory allocation for Antenna"
                          " selection failed.\r\n"));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
        /* Get the radio type for the Radio DB */
        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
            pWssIfCapwapDB->CapwapGetDB.u4IfIndex;

        RadioIfGetDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_TRUE;

        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, &RadioIfGetDB)
            != OSIX_SUCCESS)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to get Radio Type\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to get Radio Type\r\n"));
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                             pu1AntennaSelection);
            return OSIX_FAILURE;
        }

        pRadioIfInfo->u2MessageType = WTP_RADIO_INFO;
        pRadioIfInfo->u2MessageLength = IEEE_RADIO_INFO_LEN;
        pRadioIfInfo->u1RadioId = u1Index;
        pRadioIfInfo->u4RadioType =
            RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType;
        pRadioIfInfo->isPresent = OSIX_TRUE;
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                         pu1AntennaSelection);
    }
    *pMsgLen += (UINT4) (pRadioIfInfo->u2MessageLength + 4);
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapGetInternalId                               */
/*  Description     : The function will parse the packet and get the    */
/*                    MAC address.                                      */
/*  Input(s)        : discReqMsg - parsed CAPWAP Discovery request      */
/*                    pRcvBuf - Received discovery packet               */
/*  Output(s)       : pu2WtpInternalId - WTP Internal ID                */
/*  Returns         : None                                              */
/************************************************************************/
INT4
CapwapGetInternalId (UINT1 *pRcvBuf, tCapwapControlPacket * pDiscReqMsg,
                     UINT2 *pu2WtpInternalId)
{
    UINT4               u4Offset;
    UINT4               u4WtpProfileId;
    tWtpBoardData       wtpBoardData;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;

    CAPWAP_FN_ENTRY ();

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&wtpBoardData, 0, sizeof (tWtpBoardData));

    u4Offset =
        pDiscReqMsg->capwapMsgElm[WTP_BOARD_DATA_DISC_REQ_INDEX].pu2Offset[0];
    /* Get the WTP Base MAC Addr */
    if (CapwapGetWtpBoardDataFromBuf (pRcvBuf, u4Offset, &wtpBoardData) ==
        OSIX_SUCCESS)
    {
        MEMCPY (pWssIfCapwapDB->CapwapGetDB.WtpMacAddress,
                wtpBoardData.wtpBoardInfo[WTP_BASE_MAC_ADDRESS].boardInfoData,
                wtpBoardData.wtpBoardInfo[WTP_BASE_MAC_ADDRESS].
                u2BoardInfoLength);
    }

    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_INDEX_FROM_MAC, pWssIfCapwapDB)
        == OSIX_SUCCESS)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        *pu2WtpInternalId = pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId;
        return OSIX_SUCCESS;
    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileMac
            (pWssIfCapwapDB->CapwapGetDB.WtpMacAddress,
             &u4WtpProfileId) == OSIX_SUCCESS)
        {
            pWssIfCapwapDB->CapwapGetDB.u2ProfileId = (UINT2) u4WtpProfileId;
            pWssIfCapwapDB->CapwapIsGetAllDB.bWtpProfileId = OSIX_TRUE;
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_INTERNAL_ID,
                                         pWssIfCapwapDB) == NOT_IN_SERVICE)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "The WTP profile for received Base Mac is inactive\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "The WTP profile for received Base Mac is inactive\r\n"));
                *pu2WtpInternalId = pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId;
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return NOT_IN_SERVICE;
            }
        }
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    CAPWAP_FN_EXIT ();
    return OSIX_FAILURE;
}

INT4
CapwapGetLocationData (tLocationData * wtpLocation, UINT4 *pMsgLen)
{
    UNUSED_PARAM (wtpLocation);
    UNUSED_PARAM (pMsgLen);
    return OSIX_SUCCESS;
}

INT4
CapwapGetWtpBoardData (tWtpBoardData * pWtpBoardData, UINT4 *pMsgLen)
{
    UNUSED_PARAM (pWtpBoardData);
    UNUSED_PARAM (pMsgLen);
    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                "This message element not used in WLC \r\n");
    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                  "This message element not used in WLC \r\n"));
    return OSIX_FAILURE;
}

INT4
CapwapGetWtpDescriptor (tWtpDescriptor * pWtpDescriptor, UINT4 *pMsgLen)
{
    UNUSED_PARAM (pWtpDescriptor);
    UNUSED_PARAM (pMsgLen);
    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                "This message element not used in WLC \r\n");
    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                  "This message element not used in WLC \r\n"));
    return OSIX_FAILURE;
}

INT4
CapwapGetWtpName (tWtpName * pWtpName, UINT4 *pMsgLen)
{
    UNUSED_PARAM (pWtpName);
    UNUSED_PARAM (pMsgLen);
    return OSIX_SUCCESS;
}

INT4
CapwapGetJoinSessionId (UINT4 *pJoinSessionID, UINT4 *pMsgLen)
{
    UNUSED_PARAM (pJoinSessionID);
    UNUSED_PARAM (pMsgLen);
    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                "This message element not used in WLC \r\n");
    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                  "This message element not used in WLC \r\n"));
    return OSIX_FAILURE;
}

INT4
CapwapGetWtpFrameTunnelMode (tWtpFrameTunnel * pWtpFrameTunnel, UINT4 *pMsgLen)
{
    UNUSED_PARAM (pWtpFrameTunnel);
    UNUSED_PARAM (pMsgLen);
    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                "This message element not used in WLC \r\n");
    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                  "This message element not used in WLC \r\n"));
    return OSIX_FAILURE;
}

INT4
CapwapGetWtpMacType (tWtpMacType * pWtpMacType, UINT4 *pMsgLen)
{
    UNUSED_PARAM (pWtpMacType);
    UNUSED_PARAM (pMsgLen);
    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                "This message element not used in WLC \r\n");
    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                  "This message element not used in WLC \r\n"));
    return OSIX_FAILURE;
}

INT4
CapwapGetWtpDiscPadding (tMtuDiscPad * pMtuDiscPad, UINT4 *pMsgLen)
{
    UNUSED_PARAM (pMtuDiscPad);
    UNUSED_PARAM (pMsgLen);
    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                "This message element not used in WLC \r\n");
    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                  "This message element not used in WLC \r\n"));
    return OSIX_FAILURE;
}

INT4
CapwapGetDiscoveryType (tWtpDiscType * pWtpDiscType, UINT4 *pMsgLen)
{
    UNUSED_PARAM (pWtpDiscType);
    UNUSED_PARAM (pMsgLen);
    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                "This message element not used in WLC \r\n");
    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                  "This message element not used in WLC \r\n"));
    return OSIX_FAILURE;
}

INT4
CapwapValidateCapwapTimer (UINT1 *pRcvBuf,
                           tCapwapControlPacket * cwMsg, UINT2 u2MsgIndex)
{
    UINT2               u2NumMsgElems;

    CAPWAP_FN_ENTRY ();
    u2NumMsgElems = cwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;
    if (u2NumMsgElems > 1)
    {
        /* Allocate the dynamic memory */
    }
    UNUSED_PARAM (pRcvBuf);
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

INT4
CapwapValidateIdleTimeout (UINT1 *pRcvBuf,
                           tCapwapControlPacket * cwMsg, UINT2 u2MsgIndex)
{
    UINT2               u2NumMsgElems;

    CAPWAP_FN_ENTRY ();
    u2NumMsgElems = cwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;
    if (u2NumMsgElems > 1)
    {
        /* Allocate the dynamic memory */
    }
    UNUSED_PARAM (pRcvBuf);
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

INT4
CapwapValidateIpv4List (UINT1 *pRcvBuf,
                        tCapwapControlPacket * cwMsg, UINT2 u2MsgIndex)
{
    UINT2               u2NumMsgElems;

    CAPWAP_FN_ENTRY ();
    u2NumMsgElems = cwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;
    if (u2NumMsgElems > 1)
    {
        /* Allocate the dynamic memory */
    }
    UNUSED_PARAM (pRcvBuf);
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

INT4
CapwapValidateDecryptErrReportPeriod (UINT1 *pRcvBuf,
                                      tCapwapControlPacket * cwMsg,
                                      UINT2 u2MsgIndex)
{
    UINT2               u2NumMsgElems;

    CAPWAP_FN_ENTRY ();
    u2NumMsgElems = cwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;
    if (u2NumMsgElems > 1)
    {
        /* Allocate the dynamic memory */
    }
    UNUSED_PARAM (pRcvBuf);
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

INT4
CapwapValidateWtpFallback (UINT1 *pRcvBuf,
                           tCapwapControlPacket * cwMsg, UINT2 u2MsgIndex)
{
    UINT2               u2NumMsgElems;

    CAPWAP_FN_ENTRY ();
    u2NumMsgElems = cwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;
    if (u2NumMsgElems > 1)
    {
        /* Allocate the dynamic memory */
    }
    UNUSED_PARAM (pRcvBuf);
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

INT4
CapwapValidateStatsTimer (UINT1 *pRcvBuf,
                          tCapwapControlPacket * cwMsg, UINT2 u2MsgIndex)
{
    UINT2               u2NumMsgElems;

    CAPWAP_FN_ENTRY ();
    u2NumMsgElems = cwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;
    if (u2NumMsgElems > 1)
    {
        /* Allocate the dynamic memory */
    }
    UNUSED_PARAM (pRcvBuf);
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;

}

INT4
CapwapSetAcName (UINT1 *pRcvBuf,
                 tCapwapControlPacket * pCwMsg, UINT2 u2MsgIndex)
{
    UNUSED_PARAM (pRcvBuf);
    UNUSED_PARAM (pCwMsg);
    UNUSED_PARAM (u2MsgIndex);
    return OSIX_FAILURE;
}

INT4
CapwapGetAcDescriptorFromBuf (UINT1 *pRcvBuf,
                              UINT4 u4Offset,
                              tAcDescriptor * pAcDescriptor,
                              UINT2 u2NumMsgElems)
{
    UNUSED_PARAM (pRcvBuf);
    UNUSED_PARAM (u4Offset);
    UNUSED_PARAM (pAcDescriptor);
    UNUSED_PARAM (u2NumMsgElems);
    return OSIX_FAILURE;
}

INT4
CapwapGetAcNameFromBuf (UINT1 *pRcvBuf,
                        UINT4 u4Offset, tACName * pAcName, UINT2 u2NumMsgElems)
{
    UNUSED_PARAM (pRcvBuf);
    UNUSED_PARAM (u4Offset);
    UNUSED_PARAM (pAcName);
    UNUSED_PARAM (u2NumMsgElems);
    return OSIX_FAILURE;
}

INT4
CapwapGetCtrlIP4AddrFromBuf (UINT1 *pRcvBuf,
                             UINT4 u4Offset,
                             tCtrlIpAddr * pControlIP4Addr, UINT2 u2NumMsgElems)
{
    UNUSED_PARAM (pRcvBuf);
    UNUSED_PARAM (u4Offset);
    UNUSED_PARAM (pControlIP4Addr);
    UNUSED_PARAM (u2NumMsgElems);
    return OSIX_FAILURE;
}

INT4
CapwapGetRebootStats (tWtpRebootStatsElement * pRebootStats, UINT4 *pMsgLen)
{
    UNUSED_PARAM (pRebootStats);
    UNUSED_PARAM (pMsgLen);
    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                "This message element not used in WLC \r\n");
    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                  "This message element not used in WLC \r\n"));
    return OSIX_FAILURE;
}

INT4
CapwapUpdateKernelWlcIpaddr (UINT4 u4WlcIpAddr)
{
#ifdef KERNEL_CAPWAP_WANTED
    UINT1               u1Module;
    UINT2               u2Field;
    tHandleRbWlc        WssKernelDB;
    MEMSET (&WssKernelDB, 0, sizeof (tHandleRbWlc));
    WssKernelDB.rbData.u4WlcIpAddr = u4WlcIpAddr;
    u1Module = TX_MODULE;
    u2Field = WLC_IP_ADDR;
    if (capwapNpUpdateKernelDBWlc (u1Module, u2Field, &WssKernelDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    u1Module = RX_MODULE;
    if (capwapNpUpdateKernelDBWlc (u1Module, u2Field, &WssKernelDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

#endif
    UNUSED_PARAM (u4WlcIpAddr);
    return OSIX_SUCCESS;
}

#ifdef KERNEL_CAPWAP_WANTED
/*****************************************************************************
 * Function     : CapwapUpdateKernelIpAndMacTable                            *
 * Description  : This function updates the Ap session details to            *
 *                kernel module                                              *
 * Input        : None                                                       *
 * Output       : None                                                       *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *****************************************************************************/
INT4
CapwapUpdateKernelIpAndMacTable (UINT4 u4ipAddr, UINT4 u4udpPort,
                                 UINT1 u1MacType, tMacAddr WtpMacAddr,
                                 INT4 i4CtrlUdpPort,
                                 UINT1 u1FragReassembleStatus, UINT4 u4PathMTU)
{
    UINT1               u1Module;
    UINT2               u2Field;
    tHandleRbWlc        WssKernelDB;

    MEMSET (&WssKernelDB, 0, sizeof (tHandleRbWlc));
    WssKernelDB.rbData.unTable.apIpData.u4ipAddr = u4ipAddr;
    WssKernelDB.rbData.unTable.apIpData.u4UdpPort = u4udpPort;
    WssKernelDB.rbData.unTable.apIpData.u1macType = u1MacType;
    WssKernelDB.rbData.unTable.apIpData.u1FragReassembleStatus =
        u1FragReassembleStatus;
    WssKernelDB.rbData.unTable.apIpData.u4PathMTU = u4PathMTU;

    u1Module = TX_MODULE;
    u2Field = AP_IP_TABLE;
    if (capwapNpUpdateKernelDBWlc (u1Module, u2Field, &WssKernelDB) ==
        OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to KERNEL DB \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to KERNEL DB \r\n"));
        return OSIX_FAILURE;
    }

    MEMSET (&WssKernelDB, 0, sizeof (tHandleRbWlc));
    WssKernelDB.rbData.unTable.apMacData.u4ipAddr = u4ipAddr;
    WssKernelDB.rbData.unTable.apMacData.u4UdpPort = u4udpPort;
    WssKernelDB.rbData.unTable.apMacData.u1macType = u1MacType;
    WssKernelDB.rbData.unTable.apMacData.u1FragReassembleStatus =
        u1FragReassembleStatus;
    WssKernelDB.rbData.unTable.apMacData.u4PathMTU = u4PathMTU;
    MEMCPY (WssKernelDB.rbData.unTable.apMacData.apMacAddr, WtpMacAddr,
            sizeof (tMacAddr));

    u1Module = RX_MODULE;
    u2Field = AP_MAC_TABLE;
    if (capwapNpUpdateKernelDBWlc (u1Module, u2Field, &WssKernelDB) ==
        OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to KERNEL DB \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to KERNEL DB \r\n"));
        return OSIX_FAILURE;
    }

    u1Module = TX_MODULE;
    u2Field = AP_MAC_TABLE;
    if (capwapNpUpdateKernelDBWlc (u1Module, u2Field, &WssKernelDB) ==
        OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to KERNEL DB \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to KERNEL DB \r\n"));
        return OSIX_FAILURE;
    }

    MEMSET (&WssKernelDB, 0, sizeof (tHandleRbWlc));
    WssKernelDB.rbData.i4CtrlUdpPort = i4CtrlUdpPort;

    u1Module = TX_MODULE;
    u2Field = SOCKET_INFO;
    if (capwapNpUpdateKernelDBWlc (u1Module, u2Field, &WssKernelDB) ==
        OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to KERNEL DB \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to KERNEL DB \r\n"));
        return OSIX_FAILURE;
    }

    u1Module = RX_MODULE;
    u2Field = SOCKET_INFO;
    if (capwapNpUpdateKernelDBWlc (u1Module, u2Field, &WssKernelDB) ==
        OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to KERNEL DB \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to KERNEL DB \r\n"));
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : CapwapRemoveKernelIpAndMacTable                            *
 * Description  : This function updates the Ap session details to            *
 *                kernel module                                              *
 * Input        : None                                                       *
 * Output       : None                                                       *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *****************************************************************************/
INT4
CapwapRemoveKernelIpAndMacTable (UINT4 u4ipAddr)
{
    UINT1               u1Module;
    UINT2               u2Field;
    tHandleRbWlc        WssKernelDB;

    MEMSET (&WssKernelDB, 0, sizeof (tHandleRbWlc));
    WssKernelDB.rbData.unTable.apIpData.u4ipAddr = u4ipAddr;

    u1Module = TX_MODULE;
    u2Field = AP_IP_TABLE;
    if (capwapNpRemoveKernelDBWlc (u1Module, u2Field, &WssKernelDB) ==
        OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to KERNEL DB \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to KERNEL DB \r\n"));
        /* return OSIX_FAILURE; */
    }

    MEMSET (&WssKernelDB, 0, sizeof (tHandleRbWlc));
    WssKernelDB.rbData.unTable.apMacData.u4ipAddr = u4ipAddr;

    u1Module = RX_MODULE;
    u2Field = AP_MAC_TABLE;
    if (capwapNpRemoveKernelDBWlc (u1Module, u2Field, &WssKernelDB) ==
        OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to KERNEL DB \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to KERNEL DB \r\n"));
        /* return OSIX_FAILURE; */
    }
    return OSIX_SUCCESS;

}

/*****************************************************************************
 * Function     : CapwapKernelUpdateVlanTable                                *
 * Description  : This function updates the kernel module                    *
 * Input        : None                                                       *
 * Output       : None                                                       *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *****************************************************************************/
INT4
CapwapKernelUpdateVlanTable (UINT2 u2VlanId, UINT1 *pPhysicalPort,
                             UINT1 u1Tagged)
{
    UINT1               u1Module;
    UINT2               u2Field;
    tHandleRbWlc        WssKernelDB;

    MEMSET (&WssKernelDB, 0, sizeof (tHandleRbWlc));
    WssKernelDB.rbData.unTable.VlanData.u2VlanId = u2VlanId;
    MEMCPY (WssKernelDB.rbData.unTable.VlanData.au1PhysicalPort, pPhysicalPort,
            INTERFACE_LEN);
    WssKernelDB.rbData.unTable.VlanData.u1Tagged = u1Tagged;

    u1Module = TX_MODULE;
    u2Field = VLAN_TABLE;
    if (capwapNpUpdateKernelDBWlc (u1Module, u2Field, &WssKernelDB) ==
        OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to KERNEL DB \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to KERNEL DB \r\n"));
        /* return OSIX_FAILURE; */
    }

    u1Module = RX_MODULE;
    u2Field = VLAN_TABLE;
    if (capwapNpUpdateKernelDBWlc (u1Module, u2Field, &WssKernelDB) ==
        OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to KERNEL DB \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to KERNEL DB \r\n"));
        /* return OSIX_FAILURE; */
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : CapwapKernelReplaceVlanTable                               *
 * Description  : This function is used to replace the kernel module         *
 * Input        : None                                                       *
 * Output       : None                                                       *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *****************************************************************************/
INT4
CapwapKernelReplaceVlanTable (UINT2 u2VlanId, UINT1 *pPhysicalPort,
                              UINT1 u1Tagged)
{
    UINT1               u1Module;
    UINT2               u2Field;
    tHandleRbWlc        WssKernelDB;

    MEMSET (&WssKernelDB, 0, sizeof (tHandleRbWlc));
    WssKernelDB.rbData.unTable.VlanData.u2VlanId = u2VlanId;
    MEMCPY (WssKernelDB.rbData.unTable.VlanData.au1PhysicalPort, pPhysicalPort,
            INTERFACE_LEN);
    WssKernelDB.rbData.unTable.VlanData.u1Tagged = u1Tagged;

    u1Module = TX_MODULE;
    u2Field = VLAN_TABLE;
    if (capwapNpReplaceKernelDBWlc (u1Module, u2Field, &WssKernelDB) ==
        OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to KERNEL DB \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to KERNEL DB \r\n"));
        /* return OSIX_FAILURE; */
    }

    u1Module = RX_MODULE;
    u2Field = VLAN_TABLE;
    if (capwapNpReplaceKernelDBWlc (u1Module, u2Field, &WssKernelDB) ==
        OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to KERNEL DB \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to KERNEL DB \r\n"));
        /* return OSIX_FAILURE; */
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : CapwapKernelRemoveVlanTable                                *
 * Description  : This function is used to remove the kernel module          *
 * Input        : None                                                       *
 * Output       : None                                                       *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *****************************************************************************/
INT4
CapwapKernelRemoveVlanTable (UINT2 u2VlanId, UINT1 *pPhysicalPort)
{
    UINT1               u1Module;
    UINT2               u2Field;
    tHandleRbWlc        WssKernelDB;

    MEMSET (&WssKernelDB, 0, sizeof (tHandleRbWlc));
    WssKernelDB.rbData.unTable.VlanData.u2VlanId = u2VlanId;
    MEMCPY (WssKernelDB.rbData.unTable.VlanData.au1PhysicalPort, pPhysicalPort,
            INTERFACE_LEN);

    u1Module = TX_MODULE;
    u2Field = VLAN_TABLE;
    if (capwapNpRemoveKernelDBWlc (u1Module, u2Field, &WssKernelDB) ==
        OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to KERNEL DB \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to KERNEL DB \r\n"));
        /* return OSIX_FAILURE; */
    }

    u1Module = RX_MODULE;
    u2Field = VLAN_TABLE;
    if (capwapNpRemoveKernelDBWlc (u1Module, u2Field, &WssKernelDB) ==
        OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to KERNEL DB \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to KERNEL DB \r\n"));
        /* return OSIX_FAILURE; */
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : CapwapKernelUpdateMacTable                                 *
 * Description  : This function is used to update the kernel module          *
 * Input        : None                                                       *
 * Output       : None                                                       *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *****************************************************************************/
INT4
CapwapKernelUpdateMacTable (tMacAddr MacAddress, UINT1 *PhysicalPort,
                            UINT1 u1WlanFlag, UINT2 u2VlanId)
{
    UINT1               u1Module;
    UINT2               u2Field;
    tHandleRbWlc        WssKernelDB;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;

    CAPWAP_IF_CAP_DB_ALLOC (pWssIfCapwapDB);

    MEMSET (&WssKernelDB, 0, sizeof (tHandleRbWlc));
    MEMCPY (WssKernelDB.rbData.unTable.MacData.au1MacAddress, MacAddress,
            sizeof (tMacAddr));
    MEMCPY (WssKernelDB.rbData.unTable.MacData.au1PhysicalInterface,
            PhysicalPort, INTERFACE_LEN);
    WssKernelDB.rbData.unTable.MacData.u1WlanFlag = u1WlanFlag;
    WssKernelDB.rbData.unTable.MacData.u2VlanId = u2VlanId;

    u1Module = TX_MODULE;
    u2Field = MAC_TABLE;
    if (capwapNpUpdateKernelDBWlc (u1Module, u2Field, &WssKernelDB) ==
        OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to KERNEL DB \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to KERNEL DB \r\n"));
        /* return OSIX_FAILURE; */
    }

    u1Module = RX_MODULE;
    u2Field = MAC_TABLE;
    if (capwapNpUpdateKernelDBWlc (u1Module, u2Field, &WssKernelDB) ==
        OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to KERNEL DB \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to KERNEL DB \r\n"));
        /* return OSIX_FAILURE; */
    }
    if (pWssIfCapwapDB != NULL)
    {

        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

        MEMCPY (pWssIfCapwapDB->CapwapGetDB.WtpMacAddress,
                WssKernelDB.rbData.unTable.MacData.au1MacAddress, MAC_ADDR_LEN);

        if (WssIfProcessCapwapDBMsg
            (WSS_CAPWAP_GET_MACADDR_MAPPING_ENTRY,
             pWssIfCapwapDB) == OSIX_SUCCESS)
        {
            pWssIfCapwapDB->CapwapIsGetAllDB.bWtpProfileId = OSIX_TRUE;
            pWssIfCapwapDB->CapwapGetDB.u2ProfileId =
                pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId;

            if (WssIfProcessCapwapDBMsg
                (WSS_CAPWAP_GET_INTERNAL_ID, pWssIfCapwapDB) == OSIX_SUCCESS)
            {
                pWssIfCapwapDB->CapwapIsGetAllDB.bIntfName = OSIX_TRUE;
                pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;

                MEMCPY (pWssIfCapwapDB->CapwapGetDB.au1IntfName,
                        PhysicalPort, INTERFACE_LEN);
                WssIfProcessCapwapDBMsg (WSS_CAPWAP_SET_DB, pWssIfCapwapDB);
            }
        }
    }
    CAPWAP_IF_CAP_DB_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : CapwapKernelRemoveMacTable                                 *
 * Description  : This function is used to remove the kernel module          *
 * Input        : None                                                       *
 * Output       : None                                                       *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *****************************************************************************/
INT4
CapwapKernelRemoveMacTable (tMacAddr MacAddress)
{
    UINT1               u1Module;
    UINT2               u2Field;
    tHandleRbWlc        WssKernelDB;

    MEMSET (&WssKernelDB, 0, sizeof (tHandleRbWlc));
    MEMCPY (WssKernelDB.rbData.unTable.MacData.au1MacAddress, MacAddress,
            sizeof (tMacAddr));

    u1Module = TX_MODULE;
    u2Field = MAC_TABLE;
    if (capwapNpRemoveKernelDBWlc (u1Module, u2Field, &WssKernelDB) ==
        OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to KERNEL DB \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to KERNEL DB \r\n"));
        /* return OSIX_FAILURE; */
    }

    u1Module = RX_MODULE;
    u2Field = MAC_TABLE;
    if (capwapNpRemoveKernelDBWlc (u1Module, u2Field, &WssKernelDB) ==
        OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to KERNEL DB \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to KERNEL DB \r\n"));
        /* return OSIX_FAILURE; */
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : CapwapUpdateMacDB                                          *
 * Description  : This function is used to update the kernel module          *
 * Input        : None                                                       *
 * Output       : None                                                       *
 * Returns      : None                                                       *
 *****************************************************************************/
VOID
CapwapUpdateMacDB (tMacAddr MacAddr, UINT2 u2Port, UINT2 u2VlanId)
{
    UINT1               u1WlanFlag = 0;
    /* tVlanPortEntry     *pVlanPortEntry = NULL; */
    tWssStaWepProcessDB *pWssStaWepProcessDB = NULL;
    UINT1               au1IfName[INTERFACE_LEN] = { 0 };

    /*
       if( (u2Port >= CFA_MIN_IVR_IF_INDEX) && (u2Port <= CFA_MAX_IVR_IF_INDEX))
       {
       pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);
       u2Port = pVlanPortEntry->u2Port;
       }
     */
    if (u2Port < SYS_DEF_MAX_PHYSICAL_INTERFACES)
    {
        STRNCPY (au1IfName, CfaGddGetLnxIntfnameForPort (u2Port),
                 STRLEN (CfaGddGetLnxIntfnameForPort (u2Port)));

        pWssStaWepProcessDB = WssStaProcessEntryGet (MacAddr);
        if (pWssStaWepProcessDB != NULL)
        {
            u1WlanFlag = 1;
        }

        if (CapwapKernelUpdateMacTable
            (MacAddr, au1IfName, u1WlanFlag, u2VlanId) == OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to KERNEL DB \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to KERNEL DB \r\n"));
            /* return OSIX_FAILURE; */
        }
    }
    else
    {
        CAPWAP_TRC1 (CAPWAP_FAILURE_TRC, "Invalid Interface - %d \r\n", u2Port);
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Invalid Interface - %d \r\n", u2Port));
    }
    return;
}

/*****************************************************************************
 * Function     : CapwapInitVlanDB                                           *
 * Description  : This function is used to init the kernel module            *
 * Input        : None                                                       *
 * Output       : None                                                       *
 * Returns      : None                                                       *
 *****************************************************************************/
VOID
CapwapInitVlanDB ()
{
#if 0
    tVlanId             ScanVlanId;
    tVlanCurrEntry     *pScanVlanEntry = NULL;
    UINT1              *pu1EgressLocalPortList = NULL;
    UINT1              *pu1UntaggedLocalPortList = NULL;
    UINT1              *pu1TaggedLocalPortList = NULL;

    pu1EgressLocalPortList =
        UtilPlstAllocLocalPortList (sizeof (tLocalPortList));
    if (pu1EgressLocalPortList == NULL)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to allocate memory for port list \r\n");
        return OSIX_FAILURE;
    }
    pu1UntaggedLocalPortList =
        UtilPlstAllocLocalPortList (sizeof (tLocalPortList));
    if (pu1UntaggedLocalPortList == NULL)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to allocate memory for port list \r\n");
        return OSIX_FAILURE;
    }
    pu1TaggedLocalPortList =
        UtilPlstAllocLocalPortList (sizeof (tLocalPortList));
    if (pu1TaggedLocalPortList == NULL)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to allocate memory for port list \r\n");
        return OSIX_FAILURE;
    }
    VLAN_SCAN_VLAN_TABLE (ScanVlanId)
    {
        pScanVlanEntry = VLAN_GET_CURR_ENTRY (ScanVlanId);
        if (pScanVlanEntry != NULL)
        {
            VLAN_GET_EGRESS_PORTS (pScanVlanEntry, pu1EgressLocalPortList);
            VLAN_GET_UNTAGGED_PORTS (pScanVlanEntry, pu1UntaggedLocalPortList);
            pu1TaggedLocalPortList =
                (pu1EgressLocalPortList) & (~pu1UntaggedLocalPortList);
        CapwapUpdateVlanDB (ScanVlanId, pu1TaggedLocalPortList,
                                pu1UntaggedLocalPortList)}
    }
    UtilPlstReleaseLocalPortList (pu1EgressLocalPortList);
    UtilPlstReleaseLocalPortList (pu1UntaggedLocalPortList);
    UtilPlstReleaseLocalPortList (pu1TaggedLocalPortList);
#endif
    return;
}

/*****************************************************************************
 * Function     : CapwapUpdateVlanEgressPorts                                *
 * Description  : This function is used to update the kernel module          *
 * Input        : None                                                       *
 * Output       : None                                                       *
 * Returns      : None                                                       *
 *****************************************************************************/
VOID
CapwapUpdateVlanEgressPorts (UINT4 u4VlanId, UINT1 *pu1TaggedMemberPorts,
                             UINT1 u1Action)
{
    UINT4               portArray[CONTEXT_PORT_LIST_SIZE];
    UINT2               u2NumPorts = 0, u2Iter = 0;
    UINT1               au1IfName[INTERFACE_LEN] = { 0 };
    if (gu4CapwapEnable == CAPWAP_ENABLE)
    {
        VlanConvertLocalPortListToArray (pu1TaggedMemberPorts, portArray,
                                         &u2NumPorts);
        for (u2Iter = 0; u2Iter < u2NumPorts; u2Iter++)
        {
            MEMSET (&au1IfName, 0, INTERFACE_LEN);
            STRNCPY (au1IfName,
                     CfaGddGetLnxIntfnameForPort ((UINT2) portArray[u2Iter]),
                     (STRLEN
                      (CfaGddGetLnxIntfnameForPort
                       ((UINT2) portArray[u2Iter]))));
            if (u1Action == ADD_VLAN)
            {
                CapwapKernelUpdateVlanTable ((UINT2) u4VlanId, au1IfName, 1);
            }
            else if (u1Action == DELETE_VLAN)
            {
                CapwapKernelRemoveVlanTable ((UINT2) u4VlanId, au1IfName);
            }
            else
            {
                printf ("\nInvalid Action\n");
            }
        }
    }
    return;
}

/*****************************************************************************
 * Function     : CapwapUpdateVlanUnTaggedPorts                              *
 * Description  : This function is used to update the kernel module          *
 * Input        : None                                                       *
 * Output       : None                                                       *
 * Returns      : None                                                       *
 *****************************************************************************/
VOID
CapwapUpdateVlanUnTaggedPorts (UINT4 u4VlanId, UINT1 *pu1UntaggedPorts,
                               UINT1 u1Action)
{
    UINT4               portArray[CONTEXT_PORT_LIST_SIZE];
    UINT2               u2NumPorts = 0, u2Iter = 0;
    UINT1               au1IfName[INTERFACE_LEN] = { 0 };
    if (gu4CapwapEnable == CAPWAP_ENABLE)
    {
        VlanConvertLocalPortListToArray (pu1UntaggedPorts, portArray,
                                         &u2NumPorts);
        for (u2Iter = 0; u2Iter < u2NumPorts; u2Iter++)
        {
            MEMSET (&au1IfName, 0, INTERFACE_LEN);
            STRNCPY (au1IfName,
                     CfaGddGetLnxIntfnameForPort ((UINT2) portArray[u2Iter]),
                     STRLEN (CfaGddGetLnxIntfnameForPort
                             ((UINT2) portArray[u2Iter])));
            if (u1Action == ADD_VLAN)
            {
                CapwapKernelReplaceVlanTable ((UINT2) u4VlanId, au1IfName, 0);
            }
            else if (u1Action == DELETE_VLAN)
            {
                CapwapKernelRemoveVlanTable ((UINT2) u4VlanId, au1IfName);
            }
            else
            {
                printf ("\nInvalid Action\n");
            }

        }
    }
    return;
}

/*****************************************************************************
 * Function     : CapwapUpdateArpTable                                       *
 * Description  : This function is used to update the kernel module          *
 * Input        : None                                                       *
 * Output       : None                                                       *
 * Returns      : None                                                       *
 *****************************************************************************/
VOID
CapwapUpdateArpTable (UINT4 u4IpAddr, UINT1 *pMacAddr)
{
    tWssStaWepProcessDB *pWssStaWepProcessDB = NULL;
    tWssIfPMDB          wssIfPMDB;
    MEMSET (&wssIfPMDB, 0, sizeof (tWssIfPMDB));
    pWssStaWepProcessDB = WssStaProcessEntryGet (pMacAddr);
    if (pWssStaWepProcessDB != NULL)
    {
        if (pWssStaWepProcessDB->u4StationIpAddress == 0)
        {
            pWssStaWepProcessDB->u4StationIpAddress = u4IpAddr;
            if (CapwapUpdateKernelStationIPTable (pMacAddr, u4IpAddr) !=
                OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Failed to update kernel station DB \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to update kernel station DB \r\n"));
                return;
            }
            if (CapwapReplaceKernelStaTable (pMacAddr) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Failed to update kernel station DB \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to update kernel station DB \r\n"));
                return;
            }
            MEMCPY (wssIfPMDB.FsWlanClientStatsMACAddress,
                    (tMacAddr *) pMacAddr, MAC_ADDR_LEN);
            if (WssIfProcessPMDBMsg
                (WSS_PM_WTP_EVT_VSP_CLIENT_STATS_GET,
                 &wssIfPMDB) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Failed to update Station IP Address in PM DB \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to update Station IP Address in PM DB \r\n"));
            }
            wssIfPMDB.clientStats.u4StaIpAddr = u4IpAddr;
            wssIfPMDB.tWtpPmAttState.bWssPmVSPClientStatsSet = OSIX_TRUE;
            if (WssIfProcessPMDBMsg
                (WSS_PM_WTP_EVT_VSP_CLIENT_STATS_SET,
                 &wssIfPMDB) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Failed to update Station IP Address in PM DB \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to update Station IP Address in PM DB \r\n"));
                return;
            }

        }
        else if (pWssStaWepProcessDB->u4StationIpAddress != u4IpAddr)
        {
            if (CapwapRemoveKernelStationIPTable
                (pWssStaWepProcessDB->u4StationIpAddress) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Failed to update kernel station DB \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to update kernel station DB \r\n"));
                return;
            }

            pWssStaWepProcessDB->u4StationIpAddress = u4IpAddr;
            if (CapwapUpdateKernelStationIPTable (pMacAddr, u4IpAddr) !=
                OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Failed to update kernel station DB \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to update kernel station DB \r\n"));
                return;
            }
            if (CapwapReplaceKernelStaTable (pMacAddr) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Failed to update kernel station DB \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to update kernel station DB \r\n"));
                return;
            }
        }
    }
    return;
}

/*****************************************************************************
 * Function     : CapwapRemoveArpTable                                       *
 * Description  : This function is used to update the kernel module          *
 * Input        : None                                                       *
 * Output       : None                                                       *
 * Returns      : None                                                       *
 *****************************************************************************/
VOID
CapwapRemoveArpTable (UINT4 u4IpAddr, UINT1 *pMacAddr)
{
    tWssStaWepProcessDB *pWssStaWepProcessDB = NULL;

    pWssStaWepProcessDB = WssStaProcessEntryGet (pMacAddr);

    if (pWssStaWepProcessDB != NULL)
    {
        pWssStaWepProcessDB->u4StationIpAddress = 0;
        CapwapRemoveKernelStationIPTable (u4IpAddr);
        CapwapReplaceKernelStaTable (pMacAddr);
    }
    return;
}

/*****************************************************************************
 * Function     : CapwapUpdateKernelDebugInfo                                *
 * Description  : This function updates the debug information to             *
 *                kernel module                                              *
 * Input        : None                                                       *
 * Output       : None                                                       *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *****************************************************************************/
INT4
CapwapUpdateKernelDebugInfo ()
{
    UINT1               u1Module;
    UINT2               u2Field;
    tHandleRbWlc        WssKernelDB;

    MEMSET (&WssKernelDB, 0, sizeof (tHandleRbWlc));

    WssKernelDB.rbData.i4WssDataDebugMask = gCapwapGlobals.CapwapGlbMib.
        i4FsCapwapWssDataDebugMask;
    u1Module = DSCP_MODULE;
    u2Field = DEBUG_INFO;
    if (capwapNpUpdateKernelDBWlc (u1Module, u2Field, &WssKernelDB) ==
        OSIX_FAILURE)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC, "Failed to update KERNEL DB \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to update KERNEL DB \r\n"));
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

#endif

/*****************************************************************************
 * Function     : CapwapRegisterVlanInfo                                     *
 * Description  : This function is registered with VLAN module to get the    *
 *                basic vlan information for CAPWAP module.                  *
 * Input        : pVlanBasicInfo   - vlan information                        *
 * Output       : None                                                       *
 * Returns      : None                                                       *
 *****************************************************************************/
VOID
CapwapRegisterVlanInfo (tVlanBasicInfo * pVlanBasicInfo)
{
#ifdef KERNEL_CAPWAP_WANTED
    UINT4               u4IfIndex = 0;

    switch (pVlanBasicInfo->u1Action)
    {
        case VLAN_CB_ADD_EGRESS_PORTS:
            CapwapUpdateVlanEgressPorts (pVlanBasicInfo->u2VlanId,
                                         pVlanBasicInfo->LocalPortList,
                                         ADD_VLAN);
            break;

        case VLAN_CB_DELETE_EGRESS_PORTS:
            CapwapUpdateVlanEgressPorts (pVlanBasicInfo->u2VlanId,
                                         pVlanBasicInfo->LocalPortList,
                                         DELETE_VLAN);
            break;

        case VLAN_CB_ADD_UNTAGGED_PORTS:
            CapwapUpdateVlanUnTaggedPorts (pVlanBasicInfo->u2VlanId,
                                           pVlanBasicInfo->LocalPortList,
                                           ADD_VLAN);
            break;

        case VLAN_CB_DELETE_UNTAGGED_PORTS:
            CapwapUpdateVlanUnTaggedPorts (pVlanBasicInfo->u2VlanId,
                                           pVlanBasicInfo->LocalPortList,
                                           DELETE_VLAN);
            break;

        case VLAN_CB_UPDATE_MAC:
            if (VcmGetIfIndexFromLocalPort (0, pVlanBasicInfo->u2Port,
                                            &u4IfIndex) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "WSS - VCM module - u4IfIndex not found\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "WSS - VCM module - u4IfIndex not found\r\n"));
                return;
            }
            CapwapUpdateMacDB (pVlanBasicInfo->MacAddr, (UINT2) u4IfIndex,
                               pVlanBasicInfo->u2VlanId);
            break;

        case VLAN_CB_REMOVE_MAC:
            CapwapKernelRemoveMacTable (pVlanBasicInfo->MacAddr);
            break;

        default:
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "WSS - VLAN module - Invalid indication from VLAN\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "WSS - VLAN module - Invalid indication from VLAN\r\n"));
            break;
    }
#else
    UNUSED_PARAM (pVlanBasicInfo);
#endif
    return;
}

/*****************************************************************************
 * Function     : CapwapRegisterArpInfo                                      *
 * Description  : This function is registered with ARP module to get the     *
 *                basic arp information for CAPWAP module.                   *
 * Input        : pArpBasicInfo   - arp information                          *
 * Output       : None                                                       *
 * Returns      : None                                                       *
 *****************************************************************************/
VOID
CapwapRegisterArpInfo (tArpBasicInfo * pArpBasicInfo)
{
#ifdef KERNEL_CAPWAP_WANTED
    switch (pArpBasicInfo->u1Action)
    {
        case ARP_CB_ADD_ENTRY:
            CapwapUpdateArpTable (pArpBasicInfo->u4IpAddr,
                                  (UINT1 *) pArpBasicInfo->MacAddr);
            break;

        case ARP_CB_REMOVE_ENTRY:
            CapwapRemoveArpTable (pArpBasicInfo->u4IpAddr,
                                  (UINT1 *) pArpBasicInfo->MacAddr);
            break;

        default:
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "WSS - ARP module - Invalid indication from ARP\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "WSS - ARP module - Invalid indication from ARP\r\n"));
            break;
    }
#else
    UNUSED_PARAM (pArpBasicInfo);
#endif
    return;
}

/*****************************************************************************
 * Function     : CapwapSendWebAuthStatus                                    *
 * Description  : This function is used to construct the webauth status for  *
 *                a WLAN when a station gets associated.                     *
 * Input        :                                                            *
 * Output       : None                                                       *
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 *****************************************************************************/
UINT1
CapwapSendWebAuthStatus (tMacAddr StationMacAddr, UINT1 u1Status,
                         UINT2 u2WtpInternalId)
{
    INT4                i4RetVal = 0;
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;

    /* Allocate mempool for the WLCHDLR message structure */
    pWlcHdlrMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pWlcHdlrMsgStruct == NULL)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapSendWebAuthStatus:- "
                    "UtlShMemAllocWlcBuf returned failure\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "CapwapSendWebAuthStatus:- "
                      "UtlShMemAllocWlcBuf returned failure\n"));
        return OSIX_FAILURE;
    }
    MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));

    /* Contruct the vendor message structure */
    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.
        u2MsgEleType = VENDOR_SPECIFIC_PAYLOAD;
    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.u2MsgEleLen = 15;
    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.
        elementId = VENDOR_WEBAUTH_COMPLETE_MSG;
    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.vendorId = VENDOR_ID;
    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
        VendorWebAuthStatus.u2MsgEleType = WEBAUTH_COMPLETE_MSG;
    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
        VendorWebAuthStatus.u2MsgEleLen = CAPWAP_WEB_AUTH_COMPLETE_MSG_LEN +
        CAPWAP_MSG_ELEM_TYPE_LEN;

    MEMCPY (pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
            VendorWebAuthStatus.StationMac, StationMacAddr, sizeof (tMacAddr));
    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
        VendorWebAuthStatus.u1WebAuthCompleteStatus = u1Status;
    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.isOptional = OSIX_TRUE;
    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
        VendorWebAuthStatus.isOptional = OSIX_TRUE;
    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.u2InternalId = u2WtpInternalId;

    i4RetVal =
        WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_CAPWAP_CONF_UPDATE_REQ,
                                pWlcHdlrMsgStruct);

    if (i4RetVal == OSIX_SUCCESS)
    {
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_SUCCESS;
    }
    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
    return OSIX_FAILURE;
}
#endif
