/******************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * $Id: capwapclitmr.c,v 1.1.1.1 2016/09/20 10:42:39 siva Exp $
 * Description : This file contains procedures containing Capwap Timer
 *               related operations
 *****************************************************************************/
#include "capwapinc.h"

PRIVATE tCapwapTmrDesc gaCapwapTmrDesc[CAPWAP_MAX_TMRS];

/****************************************************************************
*                                                                           *
* Function     : CapwapTmrInitTmrDesc                                         *
*                                                                           *
* Description  : Initialize Capwap Timer Decriptors                           *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*                                                                           *
*****************************************************************************/

PUBLIC VOID
CapwapTmrInitTmrDesc ()
{

    CAPWAP_TRC_FUNC ((CAPWAP_FN_ENTRY, "FUNC:CapwapTmrInitTmrDesc\n"));

/*
    gaCapwapTmrDesc[CAPWAP_PMR_TMR].pTmrExpFunc = CapwapTmrPmr;
    gaCapwapTmrDesc[CAPWAP_PMR_TMR].i2Offset =
        (INT2) (CAPWAP_GET_OFFSET (tCapwapPmr, pmrTmr));
*/

    CAPWAP_TRC_FUNC ((CAPWAP_FN_EXIT, "FUNC:EXIT CapwapTmrInitTmrDesc\n"));

}

/****************************************************************************
*                                                                           *
* Function     : CapwapTmrStartTmr                                            *
*                                                                           *
* Description  : Starts Capwap Timer                                          *
*                                                                           *
* Input        : pCapwapTmr - pointer to CapwapTmr structure                    *
*                eCapwapTmrId - CAPWAP timer ID                                 *
*                u4Secs     - ticks in seconds                              *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
*                                                                           *
*                                                                           *
*****************************************************************************/

PUBLIC VOID
CapwapTmrStartTmr (tCapwapTmr * pCapwapTmr, enCapwapTmrId eCapwapTmrId, UINT4 u4Secs)
{

    CAPWAP_TRC_FUNC ((CAPWAP_FN_ENTRY, "FUNC:CapwapTmrStartTmr\n"));

    pCapwapTmr->eCapwapTmrId = eCapwapTmrId;

    if (TmrStartTimer (gCapwapGlobals.capwapTmrLst, &(pCapwapTmr->tmrNode),
                       (UINT4) NO_OF_TICKS_PER_SEC * u4Secs) == TMR_FAILURE)
    {
        CAPWAP_TRC ((CAPWAP_TMR_TRC, "Tmr Start Failure\n"));
    }

    CAPWAP_TRC_FUNC ((CAPWAP_FN_EXIT, "FUNC: Exit CapwapTmrStartTmr\n"));
    return;
}

/****************************************************************************
*                                                                           *
* Function     : CapwapTmrRestartTmr                                          *
*                                                                           *
* Description  :  ReStarts Capwap Timer                                       *
*                                                                           *
* Input        : pCapwapTmr - pointer to CapwapTmr structure                    *
*                eCapwapTmrId - CAPWAP timer ID                                 *
*                u4Secs     - ticks in seconds                              *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*                                                                           *
*****************************************************************************/

PUBLIC VOID
CapwapTmrRestartTmr (tCapwapTmr * pCapwapTmr, enCapwapTmrId eCapwapTmrId, UINT4 u4Secs)
{

    CAPWAP_TRC_FUNC ((CAPWAP_FN_ENTRY, "FUNC:CapwapTmrRestartTmr\n"));

    TmrStopTimer (gCapwapGlobals.capwapTmrLst, &(pCapwapTmr->tmrNode));
    CapwapTmrStartTmr (pCapwapTmr, eCapwapTmrId, u4Secs);

    CAPWAP_TRC_FUNC ((CAPWAP_FN_EXIT, "FUNC: Exit CapwapTmrStartTmr\n"));
    return;
}

/****************************************************************************
*                                                                           *
* Function     : CapwapTmrStopTmr                                             *
*                                                                           *
* Description  : Restarts Capwap Timer                                        *
*                                                                           *
* Input        : pCapwapTmr - pointer to CapwapTmr structure                    *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*                                                                           *
*****************************************************************************/

PUBLIC VOID
CapwapTmrStopTmr (tCapwapTmr * pCapwapTmr)
{

    CAPWAP_TRC_FUNC ((CAPWAP_FN_ENTRY, "FUNC:CapwapTmrStopTmr\n"));

    TmrStopTimer (gCapwapGlobals.capwapTmrLst, &(pCapwapTmr->tmrNode));

    CAPWAP_TRC_FUNC ((CAPWAP_FN_EXIT, "FUNC: Exit CapwapTmrStopTmr\n"));
    return;
}

/****************************************************************************
*                                                                           *
* Function     : CapwapTmrHandleExpiry                                        *
*                                                                           *
* Description  : This procedure is invoked when the event indicating a time *
*                expiry occurs. This procedure finds the expired timers and *
*                invokes the corresponding timer routines.                  *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*                                                                           *
*****************************************************************************/
PUBLIC VOID
CapwapTmrHandleExpiry (VOID)
{
    tTmrAppTimer       *pExpiredTimers = NULL;
    enCapwapTmrId         eCapwapTmrId = CAPWAP_TMR1; 
    INT2                i2Offset = 0;

    CAPWAP_TRC_FUNC ((CAPWAP_FN_ENTRY, "FUNC: CapwapTmrHandleExpiry\n"));
    while ((pExpiredTimers = TmrGetNextExpiredTimer (gCapwapGlobals.capwapTmrLst))
           != NULL)
    {

        eCapwapTmrId = ((tCapwapTmr *) pExpiredTimers)->eCapwapTmrId;
        i2Offset = gaCapwapTmrDesc[eCapwapTmrId].i2Offset;

        if (i2Offset < 0)
        {

            /* The timer function does not take any parameter */

            (*(gaCapwapTmrDesc[eCapwapTmrId].pTmrExpFunc)) (NULL);

        }
        else
        {

            /* The timer function requires a parameter */

            (*(gaCapwapTmrDesc[eCapwapTmrId].pTmrExpFunc))
                ((UINT1 *) pExpiredTimers - i2Offset);
        }
    }
    CAPWAP_TRC_FUNC ((CAPWAP_FN_EXIT, "FUNC: Exit CapwapTmrHandleExpiry\n"));
    return;

}


/*-----------------------------------------------------------------------*/
/*                       End of the file  capwaptmr.c                      */
/*-----------------------------------------------------------------------*/
