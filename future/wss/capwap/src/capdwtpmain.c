/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 * $Id: capdwtpmain.c,v 1.3 2017/11/24 10:37:02 siva Exp $       
 * Description: This file contains the Discovery task main loop
 *              and the initialization routines.
 *
 *******************************************************************/
#ifndef __CAPDWTPMAIN_C__
#define __CAPDWTPMAIN_C__
#define _CAPWAP_GLOBAL_VAR
#include "capwapinc.h"
#include "iss.h"
#include "dhcp.h"
#include "wtpnvram.h"
#include "aphdlr.h"
#include "dnsinc.h"
static tDiscTaskGloabls gDiscTaskGlobals;
tCapwapDhcpAcDiscList gCapwapAcDiscInfo;
tCapwapDnsAcDiscList gCapwapDnsAcDiscInfo;
extern              tACRefInfoAfterDiscovery
    gACRefInfoAfterDiscovery[MAX_AC_DISCOVERY_SUPPORTED];
tCapwapStaticAcDiscList gCapwapStaticDiscInfo[MAX_STATIC_DISC];
extern tACInfoAfterDiscovery gACInfoAfterDiscovery[MAX_DISC_RESPONSE];

/*****************************************************************************/
/* Function Name      : CapwapEnqueDiscPkts                                  */
/*                                                                           */
/* Description        : This function is to enque discovery packets to the   */
/*                      discovery task                                       */
/*                                                                           */
/* Input(s)           : pCapwapRxQMsg - Received queue message               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
CapwapEnqueDiscPkts (tCapwapRxQMsg * pDiscQMsg)
{

    CAPWAP_FN_ENTRY ();
    if (OsixQueSend (gDiscTaskGlobals.discMsgQId,
                     (UINT1 *) &pDiscQMsg, OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapEnqueDiscPkts:Sending the packet to Disc queue failed \r\n");
        CRU_BUF_Release_MsgBufChain (pDiscQMsg->pRcvBuf, FALSE);
        MemReleaseMemBlock (CAPWAP_QUEMSG_POOLID, (UINT1 *) pDiscQMsg);
    }

    if (OsixEvtSend (gDiscTaskGlobals.capwapDiscTaskId,
                     CAPWAP_DISC_RX_MSGQ_EVENT) == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapEnqueDiscPkts:Sending the event failed \r\n");
        return;
    }
    CAPWAP_FN_EXIT ();
    return;
}

/****************************************************************************
*                                                                           *
* Function     : CapwapDiscTaskMain                                         *
*                                                                           *
* Description  : Main function of CAPWAP Discovery task.                    *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                *
*                                                                           *
*****************************************************************************/
UINT4
CapwapDiscTaskMain ()
{
    UINT4               u4Event = 0;
    tCapwapRxQMsg      *pDiscQMsg = NULL;
    tRemoteSessionManager *pSessEntry = NULL;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tDhcpCDiscCbInfo    pCbInfoV4;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    /* Create Discovery queue */
    if (OsixQueCrt (CAPWAP_DISC_Q_NAME, OSIX_MAX_Q_MSG_LEN, CAPWAP_DISC_Q_LEN,
                    &(gDiscTaskGlobals.discMsgQId)) != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapDiscTaskMain:Discovery Queue Creation failed \r\n");
        lrInitComplete (OSIX_FAILURE);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    if (OsixTskIdSelf (&gDiscTaskGlobals.capwapDiscTaskId) == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "!!!!! CAPWAP DISC TASK INIT FAILURE  !!!!! \r\n");
        lrInitComplete (OSIX_FAILURE);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    MEMCPY (gDiscTaskGlobals.au1TaskSemName, CAPWAP_MUT_EXCL_DISC_SEM_NAME,
            OSIX_NAME_LEN);

    if (OsixCreateSem (CAPWAP_MUT_EXCL_DISC_SEM_NAME,
                       CAPWAP_SEM_CREATE_INIT_CNT, 0,
                       &gDiscTaskGlobals.capwapDiscSemId) == OSIX_FAILURE)
    {
        CAPWAP_TRC1 (CAPWAP_FAILURE_TRC,
                     "Seamphore Creation failure for %s \n",
                     CAPWAP_MUT_EXCL_DISC_SEM_NAME);
        lrInitComplete (OSIX_FAILURE);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    /* Create the Discovery Timer List */
    if (CapDiscTmrInit () == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapDiscTaskMain:Application Timer creatiion Failed \r\n");
        lrInitComplete (OSIX_FAILURE);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    /* Memset the DHCP AC Discovery global Structure */
    memset (&gCapwapAcDiscInfo, 0, sizeof (tCapwapDhcpAcDiscList));

    /* Memset the structure used to regiter the callback function  
     * with DHCP*/
    memset (&pCbInfoV4, 0, sizeof (pCbInfoV4));

#ifdef DHCPC_WANTED
    /* Fill the callback information */
    DhcpClientUtilFillDhcpOptionBitMask (pCbInfoV4.aiOptionBitMask,
                                         AC_DISCTYPE_DHCP_V4);
    pCbInfoV4.iTaskId = gDiscTaskGlobals.capwapDiscTaskId;
    pCbInfoV4.pDhcDiscoveryCbFn = &CapwapAcquireV4ACAddress;
    DhcpClientUtilCallBackRegister (&pCbInfoV4);
#endif

    /* This thread dedicated to handle the received discovery message */
    gDiscTaskGlobals.u1IsDiscTaskInitialized = CAPWAP_SUCCESS;

    CAPWAP_TRC (CAPWAP_INIT_TRC,
                "capwapDiscoveryTask: Disc Task Init Completed \r\n");

    lrInitComplete (OSIX_SUCCESS);

    gu4CapwapSysLogId =
        (UINT4) SYS_LOG_REGISTER (CAPWAP_MUT_EXCL_RECEIVE_SEM_NAME,
                                  SYSLOG_CRITICAL_LEVEL);
    while (1)
    {
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_RSM, pWssIfCapwapDB)
            != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "WssIfProcessCapwapDBMsg failed\r\n");
        }
        pSessEntry = pWssIfCapwapDB->pSessEntry;
        if (pSessEntry->eCurrentState == CAPWAP_IDLE)
        {
            if (WtpEnterDiscoveryState (pSessEntry) == OSIX_FAILURE)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "CapwapDiscTaskMain: WTP Failed to Enter discovery state \r\n");

                /* Start silenet interval timer 
                 * i.e move to sulking state -- TBD */
                CAPWAP_TRC (CAPWAP_FSM_TRC,
                            "Entered CAPWAP SULKING State \r\n");
                pSessEntry->eCurrentState = CAPWAP_SULKING;
                CapwapDiscTmrStart (CAPWAP_SILENT_INTERVAL_TMR,
                                    SILENT_INTERVAL_TIMEOUT);
                continue;
            }
        }
        if (OsixReceiveEvent (CAPWAP_DISC_RX_MSGQ_EVENT |
                              CAPWAP_DISC_TMR_EXP_EVENT |
                              CAPWAP_DHCP_OFFER_RCV_EVENT |
                              CAPWAP_DISC_RESTART_EVENT,
                              OSIX_WAIT, (UINT4) 0,
                              (UINT4 *) &(u4Event)) == OSIX_SUCCESS)
        {

            CAPWAP_DISCOVERY_TASK_LOCK;
            if (u4Event & CAPWAP_DISC_TMR_EXP_EVENT)
            {
                CapDiscTmrExpHandler ();
            }

            if (u4Event & CAPWAP_DISC_RX_MSGQ_EVENT)
            {
                while (OsixQueRecv (gDiscTaskGlobals.discMsgQId,
                                    (UINT1 *) (&pDiscQMsg),
                                    OSIX_DEF_MSG_LEN,
                                    OSIX_NO_WAIT) == OSIX_SUCCESS)
                {
                    if (pDiscQMsg->u4PktType == CAPWAP_DISC_MSG)
                    {
                        if (CapwapDiscProcessPkt
                            ((tSegment *) pDiscQMsg->pRcvBuf) == OSIX_FAILURE)
                        {
                            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                        "Failed to process the received discovery packet\n");
                        }
                    }
                    CRU_BUF_Release_MsgBufChain (pDiscQMsg->pRcvBuf, FALSE);
                    MemReleaseMemBlock (CAPWAP_QUEMSG_POOLID,
                                        (UINT1 *) pDiscQMsg);
                }
            }

            if (u4Event & CAPWAP_DISC_RESTART_EVENT)
            {
                if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_RSM, pWssIfCapwapDB)
                    != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "WssIfProcessCapwapDBMsg failed\r\n");
                }
                /* coverity fix end */
                pSessEntry = pWssIfCapwapDB->pSessEntry;
                if (CAPWAP_IDLE == pSessEntry->eCurrentState)
                {
                    gCapwapAcDiscInfo.uCurAcDiscMode = UNKNOWN_DISC_TYPE;
                    if (WtpEnterDiscoveryState (pSessEntry) == OSIX_FAILURE)
                    {
                        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                    "CapwapDiscTaskMain: "
                                    "WTP Failed to Enter discovery state \r\n");
                        /* Start silenet interval timer 
                         * i.e move to sulking state  */
                        CAPWAP_TRC (CAPWAP_FSM_TRC,
                                    "Entered CAPWAP SULKING State \r\n");
                        pSessEntry->eCurrentState = CAPWAP_SULKING;
                        CapwapDiscTmrStart (CAPWAP_SILENT_INTERVAL_TMR,
                                            SILENT_INTERVAL_TIMEOUT);

                        CAPWAP_DISCOVERY_TASK_UNLOCK;
                        continue;
                    }
                }
            }
            CAPWAP_DISCOVERY_TASK_UNLOCK;
        }
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapValidateDiscMsgElems                        */
/*  Description     : The function validates the functional             */
/*                    characteristics of the received CAPWAP            */
/*                    discovery request agains the configured           */
/*                    WTP profile                                       */
/*  Input(s)        : discReqMsg - parsed CAPWAP Discovery request      */
/*                    pRcvBuf - Received discovery packet               */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
CapwapValidateDiscReqMsgElems (UINT1 *pRcvBuf,
                               tCapwapControlPacket * pDiscReqMsg)
{
    UNUSED_PARAM (pRcvBuf);
    UNUSED_PARAM (pDiscReqMsg);
    return OSIX_FAILURE;
}

/************************************************************************/
/*  Function Name   : CapwapProcessDiscoveryResponse                    */
/*  Description     : The function processes the received CAPWAP        */
/*                    discovery response. This function validates the   */
/*                    received discovery response and updates WSS-AP.   */
/*  Input(s)        : discReqMsg - CAPWAP Discovery packet received     */
/*                    u4DestPort - Received UDP port                    */
/*                    u4DestIp - Dest IP Address                        */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
CapwapProcessDiscoveryResponse (tSegment * pBuf,
                                tCapwapControlPacket * pDiscRespMsg,
                                tRemoteSessionManager * pSessEntry)
{
    UINT4               u4Offset = 0;
    UINT2               u2NumMsgElems = 0;
    UINT1              *pRcvBuf = NULL;
    INT4                i4Status = OSIX_SUCCESS;
    tDiscRsp            DiscResponse;
    tWssIfPMDB          WssIfPMDB;
    UINT1               u1Index = 0;
    UINT2               u2NumOptionalMsg;
    tVendorSpecPayload  vendSpec;
    UINT2               u2MsgType;

    u2NumOptionalMsg = pDiscRespMsg->numOptionalElements;
    MEMSET (&vendSpec, 0, sizeof (tVendorSpecPayload));

    CAPWAP_FN_ENTRY ();

    if (pSessEntry == NULL || pSessEntry->eCurrentState != CAPWAP_DISCOVERY)
    {
        /* The FSM may be in sulking state. In sulking state 
           WTP should not process received discovery responses */
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "FSM is in wrong State, Return Failure \r\n");
        return OSIX_FAILURE;
    }
    MEMSET (&DiscResponse, 0, sizeof (tDiscRsp));

    pRcvBuf = CRU_BUF_GetDataPtr (pBuf->pFirstValidDataDesc);

    /* Get AC Descriptor from Buffer */
    u2NumMsgElems =
        pDiscRespMsg->capwapMsgElm[AC_DESCRIPTOR_DISC_RESP_INDEX].
        numMsgElemInstance;
    u4Offset =
        pDiscRespMsg->capwapMsgElm[AC_DESCRIPTOR_DISC_RESP_INDEX].
        pu2Offset[u2NumMsgElems - 1];
    CapwapGetAcDescriptorFromBuf (pRcvBuf, u4Offset, &(DiscResponse.acDesc),
                                  u2NumMsgElems);

    /* Get AC Name from Buffer */
    u2NumMsgElems =
        pDiscRespMsg->capwapMsgElm[AC_NAME_DISC_RESP_INDEX].numMsgElemInstance;
    u4Offset =
        pDiscRespMsg->capwapMsgElm[AC_NAME_DISC_RESP_INDEX].
        pu2Offset[u2NumMsgElems - 1];
    CapwapGetAcNameFromBuf (pRcvBuf, u4Offset, &(DiscResponse.acName),
                            u2NumMsgElems);

    /* Get WTP Radio Information */
    u2NumMsgElems =
        pDiscRespMsg->capwapMsgElm[WTP_RADIO_INFO_DISC_RESP_INDEX].
        numMsgElemInstance;
    u4Offset =
        pDiscRespMsg->capwapMsgElm[WTP_RADIO_INFO_DISC_RESP_INDEX].
        pu2Offset[u2NumMsgElems - 1];
    CapwapGetRadioInfoFromBuf (pRcvBuf, u4Offset, &(DiscResponse.RadioIfInfo),
                               u2NumMsgElems);

    /* Get Control IPv4 Address */
    u2NumMsgElems =
        pDiscRespMsg->capwapMsgElm[CAPWAP_CTRL_IP_ADDR_DISC_RESP_INDEX].
        numMsgElemInstance;
    u4Offset =
        pDiscRespMsg->capwapMsgElm[CAPWAP_CTRL_IP_ADDR_DISC_RESP_INDEX].
        pu2Offset[u2NumMsgElems - 1];
    CapwapGetCtrlIP4AddrFromBuf (pRcvBuf, u4Offset, &(DiscResponse.ctrlAddr),
                                 u2NumMsgElems);

    /* Get AC IP List */
    /* This message element is optonal in discovery response it will be 
     * sent by the WLC only if discovery type is ACReferral */
    u2NumMsgElems =
        pDiscRespMsg->capwapMsgElm[CAPWAP_AC_IPLIST_INDEX].numMsgElemInstance;
    /* Fill the Ipv4 Ac List only if we have valid elements */
    if (u2NumMsgElems == OSIX_TRUE)
    {
        u4Offset =
            pDiscRespMsg->capwapMsgElm[CAPWAP_AC_IPLIST_INDEX].
            pu2Offset[u2NumMsgElems - 1];
        CapwapGetAcIp4ListFromBuf (pRcvBuf, u4Offset, &(DiscResponse.ipv4List),
                                   u2NumMsgElems);
    }

    for (u1Index = 0; u1Index < u2NumOptionalMsg; u1Index++)
    {
        /* Get the message element type */
        u2MsgType =
            pDiscRespMsg->capwapMsgElm[MAX_DISCOVERY_RES_MAND_MSG_ELEMENTS +
                                       u1Index].u2MsgType;
        switch (u2MsgType)
        {
            case VENDOR_SPECIFIC_PAYLOAD:
                if (CapwapValidateConfigUpdateVendSpecPld
                    (pRcvBuf, pDiscRespMsg, &vendSpec,
                     (MAX_DISCOVERY_RES_MAND_MSG_ELEMENTS + u1Index)) !=
                    OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "Failed to Set Vendor Specific data in the CAPWAP DB \r\n");
                    return OSIX_FAILURE;

                }
        }
    }

    i4Status = MakeDiscResponseEntry (pBuf, &DiscResponse);
    if (i4Status == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to make the discovery response entry \r\n");
        i4Status = OSIX_FAILURE;
    }

#ifdef WTP_WANTED
    MEMSET (&WssIfPMDB, 0, sizeof (tWssIfPMDB));
    WssIfPMDB.u2ProfileId = pDiscRespMsg->u2IntProfileId;

    if (WssIfProcessPMDBMsg (WSS_PM_WTP_EVT_VSP_CAPWAP_STATS_GET, &WssIfPMDB) !=
        OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to update Join Req Stats to PM \n");
    }

    WssIfPMDB.wtpVSPCPWPWtpStats.discRespRxd++;
    if (WssIfProcessPMDBMsg (WSS_PM_WTP_EVT_VSP_CAPWAP_STATS_SET, &WssIfPMDB) !=
        OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to update Join Req Stats to PM \n");
    }
#endif
    CAPWAP_FN_EXIT ();
    return i4Status;
}

/************************************************************************/
/*  Function Name   : CapwapProcessDiscoveryRequest                     */
/*  Description     : The function processes the received CAPWAP        */
/*                    discovery request                                 */
/*  Input(s)        : discReqMsg - CAPWAP Discovery packet received     */
/*                    u4DestPort - Received UDP port                    */
/*                    u4DestIp - Dest IP Address                        */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS / OSIX_FAILURE                       */
/************************************************************************/
INT4
CapwapProcessDiscoveryRequest (tSegment * pBuf,
                               tCapwapControlPacket * pDiscReqMsg,
                               tRemoteSessionManager * pSessEntry)
{
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pDiscReqMsg);
    UNUSED_PARAM (pSessEntry);
    return OSIX_FAILURE;
}

/************************************************************************/
/*  Function Name   : CapwapDiscProcessPkt                              */
/*  Description     : The function validates the header and processes   */
/*                    message based on the message type                 */
/*  Input(s)        : pRcvBuf - CAPWAP Discovery packet received        */
/*                    u1DestPort - Received UDP port                    */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_FAILURE/OSIX_SUCCESS                         */
/************************************************************************/
INT4
CapwapDiscProcessPkt (tSegment * pBuf)
{
    UINT4               u4MsgType = 0;
    INT4                i4Status = OSIX_SUCCESS;
    tCapwapControlPacket capwapDiscMsg;
    tRemoteSessionManager *pSessEntry = NULL;
#ifdef WLC_WANTED
    UINT4               u4DestIp = 0;
    UINT4               u4DestPort = 0;
#endif
    tWssIfCapDB        *pWssIfCapwapDB = NULL;

    CAPWAP_FN_ENTRY ();
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    /* Update the interface index and
     * Source IP address from the tMODULE_DATA_PTR
     * structure
     */

#ifdef WLC_WANTED
    u4DestPort = CRU_BUF_Get_U4Reserved1 (pBuf);
    u4DestIp = CRU_BUF_Get_U4Reserved2 (pBuf);
#endif

    if (CapwapParseControlPacket (pBuf, &capwapDiscMsg) == OSIX_SUCCESS)
    {
        u4MsgType = capwapDiscMsg.capwapCtrlHdr.u4MsgType;
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_RSM, pWssIfCapwapDB)
            == OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Retrive CAPWAP DB \r\n");
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
        pSessEntry = pWssIfCapwapDB->pSessEntry;
        i4Status = gCapwapStateMachine[CAPWAP_DISCOVERY][u4MsgType]
            (pBuf, &capwapDiscMsg, pSessEntry);
    }
    else
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapDiscProcessPkt: "
                    "Failed to parse the Received discovery packet \r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    if (i4Status == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapDiscProcessPkt:"
                    "Failed to process the Received discovery packet \r\n");
        /* Increment the error count */
    }

    CapwapParseStructureMemrelease (&capwapDiscMsg);
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    CAPWAP_FN_EXIT ();

    return i4Status;
}

/*****************************************************************************
 * Function     : WtpEnterDiscoveryState                                     *
 *                                                                           *
 * Description  : This function called by the WTP discovery thread after     *
 *                system initialization.This function constructs the         *
 *                discovery request and transmits the message.               *
 *                                                                           *
 * Input        : Pointer to the RemoteSessionManager                        *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
WtpEnterDiscoveryState (tRemoteSessionManager * pSessEntry)
{
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT2               u2Timeout = 0;
    UINT2               u2IntId = 0;
    UINT4               uConfigMode = 0;
    UINT4               u4WlcAddr = 0x0F;
    UINT1               icount = 0;
    UINT1               u1LoopCount = 0;
    BOOL1               isDiscReqSent = 0;
    UINT1               pQueryName[CAPWAP_DNS_QUERY_SIZE];
    UINT1               u1FreeId = 0;
    BOOL1               bFreeIdFound = OSIX_FALSE;
    UINT4               u4IfIndex = 1;
    UINT4               u4IpAddress = 0;
    UINT1               u1count = 0;

    CAPWAP_FN_ENTRY ();
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    if (pSessEntry == NULL)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Session Entry is NULL, Return Failure \r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpDiscType = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bAcNamewithPriority = OSIX_TRUE;
    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2IntId;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpCtrlDTLSStatus = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpDataDTLSStatus = OSIX_TRUE;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
        == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "WtpEnterDiscoveryState:Failed to get WTP Capability \r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_CTRLUDP_PORT, pWssIfCapwapDB)
        == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to Get Destination UDP port \r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    pSessEntry->u1DtlsFlag = pWssIfCapwapDB->CapwapGetDB.u1WtpCtrlDTLSStatus;
    pSessEntry->u1DataDtlsFlag =
        pWssIfCapwapDB->CapwapGetDB.u1WtpDataDTLSStatus;
    pSessEntry->u4RemoteCtrlPort = pWssIfCapwapDB->u4CtrludpPort;

    /* Fetch the IP Address Allocation Scheme */
    uConfigMode = IssGetConfigModeFromNvRam ();

    /* Copy the Current Mode to Global Value */
    gCapwapAcDiscInfo.uCfgAcDiscMode =
        pWssIfCapwapDB->CapwapGetDB.u1WtpDiscType;

    CAPWAP_TRC2 (CAPWAP_FSM_TRC, " WTP DTLS State %d Discovery Mode = %d \r\n",
                 pSessEntry->u1DtlsFlag, gCapwapAcDiscInfo.uCfgAcDiscMode);

    /* if the Current Mode is Auto Mode */
    if (gCapwapAcDiscInfo.uCfgAcDiscMode == AUTO_DISC_TYPE)
    {
        /* Increment the Mode when AC is not discovered in previous Mode
         * and do this only when if DHCP offer is not received and in 
         * the mode is DHCP AC Disc */
        if ((gCapwapAcDiscInfo.uDhcpOfferTmrExp) == 0)
        {
            gCapwapAcDiscInfo.uPreAcDiscMode = gCapwapAcDiscInfo.uCurAcDiscMode;
#if 0
            gCapwapAcDiscInfo.uCurAcDiscMode =
                ((++gCapwapAcDiscInfo.uCurAcDiscMode) % AUTO_DISC_TYPE);
#else
            ++gCapwapAcDiscInfo.uCurAcDiscMode;
            gCapwapAcDiscInfo.uCurAcDiscMode =
                (gCapwapAcDiscInfo.uCurAcDiscMode % AUTO_DISC_TYPE);
#endif
            if (gCapwapAcDiscInfo.uCurAcDiscMode == 0)
            {
                gCapwapAcDiscInfo.uCurAcDiscMode++;
            }
        }
    }
    else
    {
        /* routing mentioned in wtpnvram is
         * static --> always remain in static */
        if (gCapwapAcDiscInfo.uCfgAcDiscMode == STATIC_CONF_DISC_TYPE)
        {
            /* Check if WLC address is in valid range */
            u4WlcAddr = WssGetWlcIpAddrFromWtpNvRam ();
            if ((ISS_IS_ADDR_CLASS_A (u4WlcAddr)) ||
                (ISS_IS_ADDR_CLASS_B (u4WlcAddr)) ||
                (ISS_IS_ADDR_CLASS_C (u4WlcAddr)))
            {
                gCapwapAcDiscInfo.uCurAcDiscMode = STATIC_CONF_DISC_TYPE;
            }
            else
            {
                gCapwapAcDiscInfo.uCurAcDiscMode = AC_REFERRAL_DISC_TYPE;
            }
        }
        else
        {
            /* Increment the Mode when AC is not discovered in previous Mode
             * and do this only when if DNS response/DHCP offer is not received and in 
             * the mode is DNS AC DISC/DHCP AC DISC respectively */
            if (gCapwapAcDiscInfo.uCfgAcDiscMode == DNS_DISC_TYPE)
            {
                if ((gCapwapDnsAcDiscInfo.u4DnsMaxRetries >
                     CAPWAP_DNS_AC_DISC_RETRY)
                    || (gCapwapDnsAcDiscInfo.u4DnsDiscCount >
                        CAPWAP_DNS_AC_DISC_RETRY))
                {
                    gCapwapAcDiscInfo.uCurAcDiscMode = AC_REFERRAL_DISC_TYPE;
                    gCapwapDnsAcDiscInfo.u4DnsMaxRetries = 0;
                    gCapwapDnsAcDiscInfo.u4DnsDiscCount = 0;
                }
                else
                {
                    gCapwapAcDiscInfo.uCurAcDiscMode =
                        gCapwapAcDiscInfo.uCfgAcDiscMode;
                }
            }
            else if ((gCapwapAcDiscInfo.uDhcpOfferTmrExp) == 0)
            {
                gCapwapAcDiscInfo.uPreAcDiscMode =
                    gCapwapAcDiscInfo.uCurAcDiscMode;
                if (gCapwapAcDiscInfo.uPreAcDiscMode !=
                    gCapwapAcDiscInfo.uCfgAcDiscMode)
                {
                    gCapwapAcDiscInfo.uCurAcDiscMode =
                        gCapwapAcDiscInfo.uCfgAcDiscMode;
                }
                else
                {
                    gCapwapAcDiscInfo.uCurAcDiscMode = AC_REFERRAL_DISC_TYPE;
                }
            }
        }
    }

    /* Check whether the Mode is DHCP_DISC_MODE */
    if (gCapwapAcDiscInfo.uCurAcDiscMode == DHCP_DISC_TYPE)
    {
        /* Check whether the Ip Address Allocation is dynamic otherwise
         * increment the mode to discover the AC*/
        if (uConfigMode != IP_ADDR_ALLOC_DYNAMIC)
        {
            if (gCapwapAcDiscInfo.uCfgAcDiscMode == AUTO_DISC_TYPE)
            {
                gCapwapAcDiscInfo.uCurAcDiscMode++;
            }
            else
            {
                gCapwapAcDiscInfo.uCurAcDiscMode = AC_REFERRAL_DISC_TYPE;
            }
        }
        else if (uConfigMode == IP_ADDR_ALLOC_DYNAMIC)
        {
            /* If the mode is dynamic check whether we recevied DHCP
             * offer from the server */
            if (gCapwapAcDiscInfo.uOfferRcvd)
            {
                gCapwapAcDiscInfo.uDhcpOfferTmrExp = 0;
                /* in the received check whether we received any IP address */
                if (!gCapwapAcDiscInfo.CapwapV4AcList.iLength)
                {
                    /* Call the timer in case if no ip address is recevied */
                    CapwapDiscTmrStart (CAPWAP_DHCP_OFFER_RCV_INTERVAL_TMR,
                                        DHCP_OFFER_INTERVAL_TIMEOUT);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    return OSIX_SUCCESS;
                }

            }
            else
            {
                /* No DHCP offer */
                gCapwapAcDiscInfo.uDhcpOfferTmrExp++;
                /* Start the timer */
                CapwapDiscTmrStart (CAPWAP_DHCP_OFFER_RCV_INTERVAL_TMR,
                                    DHCP_OFFER_INTERVAL_TIMEOUT);
                if (gCapwapAcDiscInfo.uDhcpOfferTmrExp >
                    CAPWAP_DHCP_AC_DISC_RETRY)
                {
                    /* After timer expiry move to the next mode */
                    gCapwapAcDiscInfo.uDhcpOfferTmrExp = 0;
                }
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_SUCCESS;
            }
        }
    }

    /* Check whether the mode is DNS */
    if (gCapwapAcDiscInfo.uCurAcDiscMode == DNS_DISC_TYPE)
    {
        /* If the mode is dynamic check whether we recevied DHCP
         * offer from the server */
        if (gCapwapDnsAcDiscInfo.u4DnsRespRcvd)
        {
            gCapwapDnsAcDiscInfo.u4DnsMaxRetries = 0;
            /* in the received check whether we received any IP address */
            if (!gCapwapDnsAcDiscInfo.u4Length)
            {
                /* Start the DNS query in case no IP address */
                strncpy ((char *) pQueryName, "_capwap-control._udp.",
                         sizeof ("_capwap-control._udp."));
                strcat ((char *) pQueryName,
                        (char *) gCapwapDnsAcDiscInfo.au1DnsDomainName);

                /* Send it to DNS Server for Resloving the Ip address */
#if 0
                DNSResolveIPFromNameServer (pQueryName,
                                            pWssIfCapwapDB->DnsProfileEntry.
                                            au1FsCapwapDnsServerIp);
#endif
                CapwapDiscTmrStart (CAPWAP_DHCP_OFFER_RCV_INTERVAL_TMR,
                                    DHCP_OFFER_INTERVAL_TIMEOUT);

                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_SUCCESS;
            }
        }
        else
        {
            /* No DNS response received */
            gCapwapDnsAcDiscInfo.u4DnsMaxRetries++;

            /* Start the DNS query in case no IP address */
            strncpy ((char *) pQueryName, "_capwap-control._udp.",
                     sizeof ("_capwap-control._udp."));
            strcat ((char *) pQueryName,
                    (char *) gCapwapDnsAcDiscInfo.au1DnsDomainName);

#if 0
            /* Send it to DNS Server for Resloving the Ip address */
            DNSResolveIPFromNameServer (pQueryName,
                                        pWssIfCapwapDB->DnsProfileEntry.
                                        au1FsCapwapDnsServerIp);
#endif
            CapwapDiscTmrStart (CAPWAP_DHCP_OFFER_RCV_INTERVAL_TMR,
                                DHCP_OFFER_INTERVAL_TIMEOUT);

            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_SUCCESS;
        }
    }

    switch (gCapwapAcDiscInfo.uCurAcDiscMode)
    {
        case STATIC_CONF_DISC_TYPE:
        {
            CAPWAP_TRC (CAPWAP_FSM_TRC,
                        " WTPEntered STATIC_CONF_DISC_TYPE \r\n");
            for (icount = 0; icount < MAX_STATIC_DISC; icount++)
            {
                if ((gCapwapStaticDiscInfo[icount].u1Used
                     == OSIX_FALSE) && (bFreeIdFound != OSIX_TRUE))
                {
                    u1FreeId = icount;
                    bFreeIdFound = OSIX_TRUE;
                }
            }
            if (bFreeIdFound != OSIX_FALSE)
            {
                gCapwapStaticDiscInfo[u1FreeId].u1Used = OSIX_TRUE;
                pSessEntry->remoteIpAddr.u4_addr[0] =
                    WssGetWlcIpAddrFromWtpNvRam ();
                CapwapConstructDiscAndSend (pSessEntry);
            }
            else
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "FreeId not present \r\n");
                pSessEntry->remoteIpAddr.u4_addr[0] =
                    WssGetWlcIpAddrFromWtpNvRam ();
                CapwapConstructDiscAndSend (pSessEntry);
            }
            break;
        }
        case DHCP_DISC_TYPE:
        {
            CAPWAP_TRC (CAPWAP_FSM_TRC, "WTPEntered DHCP_DISC_TYPE \r\n");

            /* check whether the ip address allocation is dynamic */
            if (uConfigMode == IP_ADDR_ALLOC_DYNAMIC)
            {
                /* check whether Offer is recevied */
                if (gCapwapAcDiscInfo.uOfferRcvd)
                {
                    do
                    {
                        CfaIfGetIpAddress (u4IfIndex, &u4IpAddress);
                        OsixTskDelay (1);
                        u1count++;
                        if (u1count == 10)
                        {
                            break;
                        }
                    }
                    while (u4IpAddress == 0);
                    for (icount = 0;
                         icount <
                         ((gCapwapAcDiscInfo.CapwapV4AcList.iLength) / 4);
                         icount++)
                    {
                        /* Send the Cap wap discovery messages to all the ip
                         * address */
                        pSessEntry->remoteIpAddr.u4_addr[0] =
                            OSIX_NTOHL (gCapwapAcDiscInfo.CapwapV4AcList.
                                        iV4AcIpAddress[icount]);
                        CapwapConstructDiscAndSend (pSessEntry);
                        CAPWAP_TRC1 (CAPWAP_FSM_TRC,
                                     " CapWap Discovery sent with Ip address = %x "
                                     "usign DHCP DISC Type\r\n",
                                     pSessEntry->remoteIpAddr.u4_addr[0]);
                    }
                }
            }
            break;
        }
        case DNS_DISC_TYPE:
            CAPWAP_TRC (CAPWAP_FSM_TRC, " WTPEntered DNS_DISC_TYPE \r\n");
            if (gCapwapDnsAcDiscInfo.u4DnsRespRcvd)
            {
                for (icount = 0; icount < ((gCapwapDnsAcDiscInfo.u4Length) / 4);
                     icount++)
                {
                    /* Send the Capwap discovery messages to all the ip
                     * address */
                    pSessEntry->remoteIpAddr.u4_addr[0] =
                        OSIX_NTOHL (gCapwapDnsAcDiscInfo.
                                    iV4AcIpAddress[icount]);
                    CapwapConstructDiscAndSend (pSessEntry);
                    gCapwapDnsAcDiscInfo.u4DnsDiscCount++;

                    CAPWAP_TRC1 (CAPWAP_FSM_TRC,
                                 " CapWap Discovery sent with Ip address = %x "
                                 "usign DHCP DISC Type\r\n",
                                 pSessEntry->remoteIpAddr.u4_addr[0]);
                }
            }
            break;
        case AC_REFERRAL_DISC_TYPE:
            CAPWAP_TRC (CAPWAP_FSM_TRC,
                        " WTPEntered AC_REFERRAL_DISC_TYPE \r\n");
            /* Loop the Global Data structure to 
             * find whether Any AC is referred */
            for (u1LoopCount = 0; u1LoopCount < MAX_AC_DISCOVERY_SUPPORTED;
                 u1LoopCount++)
            {
                /* Check whether the Index is used and whether the Discovery
                 * request is already sent */
                if ((gACRefInfoAfterDiscovery[u1LoopCount].u1Used == OSIX_TRUE)
                    && (gACRefInfoAfterDiscovery[u1LoopCount].isDiscReqSent
                        == OSIX_FALSE))
                {

                    CAPWAP_TRC1 (CAPWAP_UTIL_TRC,
                                 "\r\nNow the session is in Ac ref the Ip address = %x ",
                                 gACRefInfoAfterDiscovery[u1LoopCount].ipAddr.
                                 u4_addr[0]);
                    /* Fill the Ip address and update the global variables
                     * */
                    pSessEntry->remoteIpAddr.u4_addr[0] =
                        gACRefInfoAfterDiscovery[u1LoopCount].ipAddr.u4_addr[0];
                    CapwapConstructDiscAndSend (pSessEntry);
                    gACRefInfoAfterDiscovery[u1LoopCount].isDiscReqSent
                        = OSIX_TRUE;
                    isDiscReqSent = OSIX_TRUE;
                }
            }

            /* Incase the AC referral list is empty fall back to the Multicast /
             * Braodcast based on the configuration */
            if (!isDiscReqSent)
            {
                if (pWssIfCapwapDB->u1AcRefOption == CAPWAP_BROADCAST_ENABLE)
                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                " \r\n Fallback to Broadcast \r\n");
                    pSessEntry->remoteIpAddr.u4_addr[0] = CAPWAP_BROADCAST_ADDR;
                }
                else if (pWssIfCapwapDB->u1AcRefOption ==
                         CAPWAP_MULTICAST_ENABLE)
                {
                    CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                " \r\n Fallback to Multicast \r\n");
                    pSessEntry->remoteIpAddr.u4_addr[0] = CAPWAP_MULTICAST_ADDR;
                }
                CapwapConstructDiscAndSend (pSessEntry);
            }
            break;
        case UNKNOWN_DISC_TYPE:
        default:
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "WtpEnterDiscoveryState:Failed to Enter discovery State \r\n");
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
    }
    /* Enable the entry in RSM */
    pSessEntry->u1Used = OSIX_TRUE;
    pSessEntry->lastTransmitedSeqNum++;
    /* MaxDiscInterval Timer: The maximum time allowed
     * between sending Discovery Request messages,in seconds */
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpMaxDiscoveryInterval = OSIX_TRUE;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
        == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to get Timer value from CAPWAP DB \r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    u2Timeout = pWssIfCapwapDB->CapwapGetDB.u2WtpMaxDiscoveryInterval;
    CapwapDiscTmrStart (CAPWAP_MAX_DISC_INTERVAL_TMR, u2Timeout);
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    CAPWAP_FN_EXIT ();

    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : CapwapConstructDiscAndSend                                 *
 *                                                                           *
 * Description  : This function will consruct the Capwap Discovery and       *
 *                transmits the message.                                     *
 *                                                                           *
 * Input        : Pointer to the RemoteSessionManager                        *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/

INT4
CapwapConstructDiscAndSend (tRemoteSessionManager * pSessEntry)
{
    tDiscReq            DiscRequest;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    UINT1              *pu1TxBuf = NULL;
    UINT1               u1IsNotLinear = OSIX_FALSE;
    tWssIfPMDB          WssIfPMDB;

    MEMSET (&DiscRequest, 0, sizeof (tDiscReq));

    if (CapwapConstructDiscReq (&DiscRequest) == OSIX_SUCCESS)
    {
        /* update CAPWAP Header */
        if (CapwapConstructCpHeader (&DiscRequest.capwapHdr) == OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "capwapConstructDiscReq:Failed to construct capwap Header \r\n");
            return OSIX_FAILURE;
        }

        DiscRequest.u1SeqNum = pSessEntry->lastTransmitedSeqNum + 1;

        pBuf = CAPWAP_ALLOCATE_CRU_BUF (CAPWAP_MAX_PKT_LEN, 0);
        if (pBuf == NULL)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "WtpEnterDiscoveryState:Memory Allocation Failed \r\n");
            return OSIX_FAILURE;
        }

        pu1TxBuf = CAPWAP_BUF_IF_LINEAR (pBuf, 0, CAPWAP_MAX_PKT_LEN);
        if (pu1TxBuf == NULL)
        {
            pu1TxBuf = (UINT1 *) MemAllocMemBlk (CAPWAP_PKTBUF_POOLID);
            if (pu1TxBuf == NULL)
            {
                CAPWAP_RELEASE_CRU_BUF (pBuf);
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "WtpEnterDiscoveryState:Failed to Assemble the packet \n");
                return OSIX_FAILURE;
            }
            CAPWAP_COPY_FROM_BUF (pBuf, pu1TxBuf, 0, CAPWAP_MAX_PKT_LEN);
            u1IsNotLinear = OSIX_TRUE;
        }

        if (CapwapAssembleDiscoveryReq (&DiscRequest, pu1TxBuf) == OSIX_FAILURE)
        {
            if (u1IsNotLinear == OSIX_TRUE)
            {
                MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pu1TxBuf);
            }
            else
            {
                CAPWAP_RELEASE_CRU_BUF (pBuf);
            }
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "WtpEnterDiscoveryState:Failed to Assemble the packet \n");
            return OSIX_FAILURE;
        }

        if (u1IsNotLinear == OSIX_TRUE)
        {
            /* If the CRU buffer memory is not linear */
            CAPWAP_COPY_TO_BUF (pBuf, pu1TxBuf, 0, CAPWAP_MAX_PKT_LEN);
            MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pu1TxBuf);
        }
        /* Change the state to Discovery */
        CAPWAP_TRC (CAPWAP_FSM_TRC, "Entered CAPWAP_DISCOVERY State \r\n");
        pSessEntry->eCurrentState = CAPWAP_DISCOVERY;
        if (CapwapTxMessage (pBuf,
                             pSessEntry,
                             DiscRequest.u2CapwapMsgElemenLen +
                             CAPWAP_SOCK_HDR_LEN,
                             WSS_CAPWAP_802_11_CTRL_PKT) == OSIX_FAILURE)
        {
            CAPWAP_RELEASE_CRU_BUF (pBuf);
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "WtpEnterDiscoveryState:Failed to Transmit the packet \r\n");
            return OSIX_FAILURE;
        }
        /* Increase the discovery count */
        gDiscTaskGlobals.u2DiscCount++;

#ifdef WTP_WANTED
        MEMSET (&WssIfPMDB, 0, sizeof (tWssIfPMDB));
        WssIfPMDB.u2ProfileId = pSessEntry->u2IntProfileId;
        if (WssIfProcessPMDBMsg
            (WSS_PM_WTP_EVT_VSP_CAPWAP_STATS_GET, &WssIfPMDB) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to update Join Req Stats to PM \n");
        }

        WssIfPMDB.wtpVSPCPWPWtpStats.discReqSent = gDiscTaskGlobals.u2DiscCount;
        if (WssIfProcessPMDBMsg
            (WSS_PM_WTP_EVT_VSP_CAPWAP_STATS_SET, &WssIfPMDB) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to update Join Req Stats to PM \n");
        }
#endif

    }

    CAPWAP_RELEASE_CRU_BUF (pBuf);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : CapwapConstructDiscReq                                  *
 *                                                                           *
 * Description  : This function constructs the discovery  request            *
 *                Capability Parameters                                      *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
CapwapConstructDiscReq (tDiscReq * pDiscRequest)
{

    UINT4               u4MsgLen = 0;

#ifdef WTP_WANTED
    tRadioIfGetDB       RadioIfGetDB;
    UINT1               u1RadioId = 0;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
#endif

    CAPWAP_FN_ENTRY ();

    /* update CAPWAP Header */
    if (CapwapConstructCpHeader (&pDiscRequest->capwapHdr) == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "capwapConstructDiscReq:Failed to construct capwap Header \r\n");
        return OSIX_FAILURE;
    }
#ifdef WTP_WANTED
    /* Set the supported radio types as N,G,A,B in the DB */
    for (u1RadioId = 1; u1RadioId <= SYS_DEF_MAX_RADIO_INTERFACES; u1RadioId++)
    {
        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = u1RadioId +
            SYS_DEF_MAX_ENET_INTERFACES;
        RadioIfGetDB.RadioIfIsGetAllDB.bSuppRadioType = OSIX_TRUE;
        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, &RadioIfGetDB)
            == OSIX_FAILURE)
        {
            CAPWAP_TRC1 (CAPWAP_UTIL_TRC, "Failed to set Radio Type in DB for "
                         "Radio Id: %d\r\n", u1RadioId);
        }
        RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType =
            RadioIfGetDB.RadioIfGetAllDB.u4SuppRadioType;
        RadioIfGetDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_TRUE;
        RadioIfGetDB.RadioIfGetAllDB.u1RadioId = u1RadioId;
        if (WssIfProcessRadioIfDBMsg (WSS_SET_PHY_RADIO_IF_DB, &RadioIfGetDB)
            == OSIX_FAILURE)
        {
            CAPWAP_TRC1 (CAPWAP_UTIL_TRC, "Failed to set Radio Type in DB for "
                         "Radio Id: %d\r\n", u1RadioId);
        }
    }
#endif
    /* update Mandatory Message Elements */
    if ((CapwapGetDiscoveryType (&pDiscRequest->discType, &u4MsgLen)
         == OSIX_FAILURE) ||
        (CapwapGetWtpFrameTunnelMode (&pDiscRequest->wtpTunnelMode,
                                      &u4MsgLen)
         == OSIX_FAILURE) ||
        (CapwapGetWtpMacType (&pDiscRequest->wtpMacType, &u4MsgLen)
         == OSIX_FAILURE) ||
        (CapwapGetWtpBoardData (&pDiscRequest->wtpBoardData, &u4MsgLen)
         == OSIX_FAILURE) ||
        (CapwapGetWtpDescriptor (&pDiscRequest->wtpDescriptor, &u4MsgLen)
         == OSIX_FAILURE))
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to get Mandatory Discovery Request Message Elements \n");
        return OSIX_FAILURE;
    }
    /* update Optional Message Elements */
    if ((CapwapGetWtpDiscPadding (&pDiscRequest->mtuDiscPad, &u4MsgLen)
         == OSIX_FAILURE) ||
        (CapwapGetVendorPayload (&pDiscRequest->vendSpec, &u4MsgLen)
         == OSIX_FAILURE))
    {
        /* With out optinal message elements 
         * It should continue */
        return OSIX_SUCCESS;
    }
    /* update Control Header */
    pDiscRequest->u2CapwapMsgElemenLen = u4MsgLen + CAPWAP_CMN_MSG_ELM_HEAD_LEN;
    pDiscRequest->u1NumMsgBlocks = 0;    /* flags */

    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************capdwtptmr.c***********************************/
/*****************************************************************************
 *                                                                           *
 * Function     : CapDiscTmrInit                                             *
 *                                                                           *
 * Description  :  This function creates a timer list for all the discovery  *
 *                 timers in CAPWAP module.                                  *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
CapDiscTmrInit (VOID)
{
    /* Timer List for State Machine Timers */
    if (TmrCreateTimerList ((CONST UINT1 *) CAPWAP_DISC_TASK_NAME,
                            CAPWAP_DISC_TMR_EXP_EVENT,
                            NULL,
                            (tTimerListId *) & (gDiscTaskGlobals.
                                                capDiscTmrListId)) ==
        TMR_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "capDiscTmrInit: Failed to create the Discvoery"
                    " Application Timer List \r\n");
        return OSIX_FAILURE;
    }

    CapDiscInitTmrDesc ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : CapDiscInitTmrDesc                                         *
 *                                                                           *
 * Description  : This function intializes the timer desc for all            *
 *                the Discovery timers in CAPWAP module.                     *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
VOID
CapDiscInitTmrDesc (VOID)
{

    /* Discovery Interval Timer */
    gDiscTaskGlobals.aCapDiscTmrDesc[CAPWAP_DISC_INTERVAL_TMR].TmrExpFn
        = CapwapDiscIntervalTmrExp;
    gDiscTaskGlobals.aCapDiscTmrDesc[CAPWAP_DISC_INTERVAL_TMR].i2Offset =
        (INT2) FSAP_OFFSETOF (tDiscTaskGloabls, DiscIntervalTmr);

    /* Max Discovery Interval Timer */
    gDiscTaskGlobals.aCapDiscTmrDesc[CAPWAP_MAX_DISC_INTERVAL_TMR].TmrExpFn
        = CapwapMaxIntervalDiscTmrExp;
    gDiscTaskGlobals.aCapDiscTmrDesc[CAPWAP_MAX_DISC_INTERVAL_TMR].i2Offset =
        (INT2) FSAP_OFFSETOF (tDiscTaskGloabls, MaxDiscIntervalTmr);

    /* Silent Interval Timer */
    gDiscTaskGlobals.aCapDiscTmrDesc[CAPWAP_SILENT_INTERVAL_TMR].TmrExpFn
        = CapwapSilentIntervalTmrExp;
    gDiscTaskGlobals.aCapDiscTmrDesc[CAPWAP_SILENT_INTERVAL_TMR].i2Offset =
        (INT2) FSAP_OFFSETOF (tDiscTaskGloabls, SilentIntervalTmr);

    /* Primary Discovery Interval timer */
    gDiscTaskGlobals.aCapDiscTmrDesc[CAPWAP_PRIMARY_DISC_INTERVAL_TMR].TmrExpFn
        = CapwapPrimaryDiscIntervalTmrExp;
    gDiscTaskGlobals.aCapDiscTmrDesc[CAPWAP_PRIMARY_DISC_INTERVAL_TMR].i2Offset
        = (INT2) FSAP_OFFSETOF (tDiscTaskGloabls, PrimaryDiscTmr);

    gDiscTaskGlobals.aCapDiscTmrDesc[CAPWAP_DHCP_OFFER_RCV_INTERVAL_TMR].
        TmrExpFn = CapwapDhcpOfferIntervalTmrExp;
    gDiscTaskGlobals.aCapDiscTmrDesc[CAPWAP_DHCP_OFFER_RCV_INTERVAL_TMR].
        i2Offset = (INT2) FSAP_OFFSETOF (tDiscTaskGloabls, DhcpOfferTmr);

}

/*****************************************************************************
 *                                                                           *
 * Function     : CapDiscTmrExpHandler                                       *
 *                                                                           *
 * Description  :  This function is called whenever a timer expiry           *
 *                 message is received by discovery thread. Different timer  *
 *                 expiry handlers are called based on the timer type.       *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
VOID
CapDiscTmrExpHandler (VOID)
{
    tTmrAppTimer       *pExpiredTimers = NULL;
    UINT1               u1TimerId = 0;
    INT2                i2Offset = 0;

    CAPWAP_FN_ENTRY ();

    while ((pExpiredTimers =
            TmrGetNextExpiredTimer (gDiscTaskGlobals.capDiscTmrListId)) != NULL)
    {
        u1TimerId = ((tTmrBlk *) pExpiredTimers)->u1TimerId;

        CAPWAP_TRC1 (CAPWAP_MGMT_TRC, "Timer to be processed %d\r\n",
                     u1TimerId);

        if (u1TimerId < CAPWAP_MAX_DISC_TMR)
        {
            i2Offset = gDiscTaskGlobals.aCapDiscTmrDesc[u1TimerId].i2Offset;

            if (i2Offset == -1)
            {
                /* The timer function does not take any parameter. */
                (*(gDiscTaskGlobals.aCapDiscTmrDesc[u1TimerId].TmrExpFn))
                    (NULL);
            }
            else
            {
                (*(gDiscTaskGlobals.aCapDiscTmrDesc[u1TimerId].TmrExpFn))
                    ((UINT1 *) pExpiredTimers - i2Offset);
            }
        }
    }
    CAPWAP_FN_EXIT ();
    return;
}

/*****************************************************************************
 *                                                                           *
 * Function     : CapwapDiscTmrStart                                         *
 *                                                                           *
 * Description  : This function starts the Discovery specific timer.        *
 *                                                                           *
 * Input        : u4TmrInterval - Time interval for which timer must run     *
 *               (in seconds)                                                *
 *                u1TmrType - Indicates which timer to start.                *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
CapwapDiscTmrStart (UINT1 u1TmrType, UINT4 u4TmrInterval)
{

    CAPWAP_FN_ENTRY ();
    switch (u1TmrType)
    {
        case CAPWAP_DISC_INTERVAL_TMR:
            if (TmrStart (gDiscTaskGlobals.capDiscTmrListId,
                          &(gDiscTaskGlobals.DiscIntervalTmr),
                          u1TmrType, u4TmrInterval, 0) == TMR_FAILURE)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "CapwapDiscTmrStart:DiscIntervalTmr Timer Start FAILED.\r\n");
                return OSIX_FAILURE;
            }
            break;
        case CAPWAP_MAX_DISC_INTERVAL_TMR:
            if (TmrStart (gDiscTaskGlobals.capDiscTmrListId,
                          &(gDiscTaskGlobals.MaxDiscIntervalTmr),
                          u1TmrType, u4TmrInterval, 0) == TMR_FAILURE)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "CapwapDiscTmrStart: "
                            "MaxDiscIntervalTmr Timer Start FAILED.\r\n");
                return OSIX_FAILURE;
            }
            break;
        case CAPWAP_SILENT_INTERVAL_TMR:
            if (TmrStart (gDiscTaskGlobals.capDiscTmrListId,
                          &(gDiscTaskGlobals.SilentIntervalTmr),
                          u1TmrType, u4TmrInterval, 0) == TMR_FAILURE)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "CapwapDiscTmrStart:SilentIntervalTmr Timer Start FAILED.\r\n");
                return OSIX_FAILURE;
            }
            break;
        case CAPWAP_PRIMARY_DISC_INTERVAL_TMR:
            if (TmrStart (gDiscTaskGlobals.capDiscTmrListId,
                          &(gDiscTaskGlobals.PrimaryDiscTmr),
                          u1TmrType, u4TmrInterval, 0) == TMR_FAILURE)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "CapwapDiscTmrStart:PrimaryDiscTmr Timer Start FAILED.\r\n");
                return OSIX_FAILURE;
            }
            /* coverity fix begin */
            break;                /* added missing break */
            /* coverity fix end */
        case CAPWAP_DHCP_OFFER_RCV_INTERVAL_TMR:
            if (TmrStart (gDiscTaskGlobals.capDiscTmrListId,
                          &(gDiscTaskGlobals.DhcpOfferTmr),
                          u1TmrType, u4TmrInterval, 0) == TMR_FAILURE)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "CapwapDiscTmrStart:DhcpOfferTmr Timer Start FAILED.\r\n");
                return OSIX_FAILURE;
            }
            break;

        default:
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "CapwapDiscTmrStart: Invalid Timer type FAILED !!!\r\n");
            return OSIX_FAILURE;
    }

    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : CapwapDiscIntervalTmrExp                                   *
 *                                                                           *
 * Description  : This function handles Discovery Interval Timer Expiry      *
 *                                                                           *
 * Input        : VOID* Discovery Information                                *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
VOID
CapwapDiscIntervalTmrExp (VOID *pArg)
{
    UNUSED_PARAM (pArg);
    CAPWAP_FN_ENTRY ();
    /* Discovery Phase is completed, send an event to Service thread */
    if (CapwapEventSend () == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "capwapDiscIntervalTmrExp: Failed to send the Event to Service Thread \r\n");
        /* Move the state to sulking state ?? */
    }
    CAPWAP_FN_EXIT ();

}

/*****************************************************************************
 *                                                                           *
 * Function     : CapwapMaxIntervalDiscTmrExp                                *
 *                                                                           *
 * Description  : This function process discovery response messages          *
 *                in case no new AC is learnt else send discover rquest      *
 *                to the new known AC IP.                                    *
 *                                                                           *
 * Input        : VOID* Discovery Information                                *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
VOID
CapwapMaxIntervalDiscTmrExp (VOID *pArg)
{
    tRemoteSessionManager *pSessEntry = NULL;
    tDiscTaskGloabls   *pDiscInfo = NULL;
    UINT1               u1DiscRespCount = 0;
    UINT1               u1DiscReSend = 0;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    BOOL1               isFlag = OSIX_FALSE;
    UINT2               u2IntId = 0;
    UINT2               u2DiscInterval = 0;
    tWssIfCapDB        *pWssIfCapwapDBTmr = NULL;

    CAPWAP_FN_ENTRY ();
    WSS_IF_DB_TEMP_MEM_ALLOC_VOID_RETURN (pWssIfCapwapDB)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    WSS_IF_DB_TEMP_MEM_ALLOC_VOID_RETURN (pWssIfCapwapDBTmr)
        MEMSET (pWssIfCapwapDBTmr, 0, sizeof (tWssIfCapDB));
    pWssIfCapwapDBTmr->CapwapIsGetAllDB.bWtpMaxDiscoveryInterval = OSIX_TRUE;
    pWssIfCapwapDBTmr->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    pWssIfCapwapDBTmr->CapwapGetDB.u2WtpInternalId = u2IntId;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDBTmr)
        == OSIX_SUCCESS)
    {
        u2DiscInterval =
            pWssIfCapwapDBTmr->CapwapGetDB.u2WtpMaxDiscoveryInterval;
    }

    pDiscInfo = (tDiscTaskGloabls *) pArg;

    /*CapwapGetDiscResponseCount (&u1DiscRespCount); */
    /* Find any Ip address is referred in the Discovery response 
     * and update the global Datastructres */
    CapwapGetDiscResponseCountUpdateAcRef (&u1DiscRespCount, &u1DiscReSend);

    CAPWAP_TRC1 (CAPWAP_MGMT_TRC,
                 "capwapMaxIntervalDiscTmrExp: Received Disc Responses: %d \r\n",
                 u1DiscRespCount);

    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpDiscType = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = 0;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB) ==
        OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapMaxIntervalDiscTmrExp:Failed to get WTP Capability \r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDBTmr);
        return;
    }

    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_RSM, pWssIfCapwapDB) !=
        OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "WssIfProcessCapwapDBMsg failed \r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDBTmr);
        return;
    }
    pSessEntry = pWssIfCapwapDB->pSessEntry;
    if (NULL == pSessEntry)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "WssIfProcessCapwapDBMsg Session Entry"
                    "Retrieval failed \r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDBTmr);
        return;
    }
    /* In case if any New AC is refereed again we need to send a unicast 
     * Discovery request before processing with the next state */
    if (u1DiscReSend != 0)
    {
        /* Move to Discovery only if the TYPE is Ac Referral */
        if (AC_REFERRAL_DISC_TYPE == pWssIfCapwapDB->CapwapGetDB.u1WtpDiscType)
        {
            if (WtpEnterDiscoveryState (pSessEntry) == OSIX_FAILURE)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "capwapMaxIntervalDiscTmrExp: "
                            "Failed to Enter Discovery State \r\n");
            }
            isFlag = OSIX_TRUE;
        }
    }

    /* Wait for the response for the Discovery sent to the AC ref Ip address */
    if (!isFlag)
    {
        if (0 == u1DiscRespCount)
        {
            if ((pDiscInfo->u2DiscCount) < CAPWAP_MAX_DISCOVERY_COUNT)
            {
                if (WtpEnterDiscoveryState (pSessEntry) == OSIX_FAILURE)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "capwapMaxIntervalDiscTmrExp:"
                                "Failed to Enter Discovery State \r\n");
                }
            }
            else
            {
                /* Enter sulking state */
                /* start silentInterval timer */
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "capwapMaxIntervalDiscTmrExp:"
                            " Started Silent Interval Timer \r\n");

                CAPWAP_TRC (CAPWAP_FSM_TRC,
                            "Entered CAPWAP SULKING State \r\n");
                pSessEntry->eCurrentState = CAPWAP_SULKING;
                CapwapDiscTmrStart (CAPWAP_SILENT_INTERVAL_TMR,
                                    SILENT_INTERVAL_TIMEOUT);
            }
        }
        else
        {
            CAPWAP_TRC (CAPWAP_MGMT_TRC,
                        "capwapMaxIntervalDiscTmrExp:"
                        " Start DiscoveryInterval Timer \r\n");
            CapwapDiscTmrStart (CAPWAP_DISC_INTERVAL_TMR, u2DiscInterval);
        }
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDBTmr);
    CAPWAP_FN_EXIT ();
}

/*****************************************************************************
 *                                                                           *
 * Function     : CapwapSilentIntervalTmrExp                                 *
 *                                                                           *
 * Description  : This function handles Silent timer expiry                  *
 *                                                                           *
 * Input        : VOID* Discovery Information                                *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
VOID
CapwapSilentIntervalTmrExp (VOID *pArg)
{
    tRemoteSessionManager *pSessEntry = NULL;
    tDiscTaskGloabls   *pDiscInfo = NULL;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;

    WSS_IF_DB_TEMP_MEM_ALLOC_VOID_RETURN (pWssIfCapwapDB)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    CAPWAP_FN_ENTRY ();
    pDiscInfo = (tDiscTaskGloabls *) pArg;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_RSM, pWssIfCapwapDB)
        == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Retrive CAPWAP DB \r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return;
    }
    pSessEntry = pWssIfCapwapDB->pSessEntry;
    /* Clear discovery count */
    pDiscInfo->u2DiscCount = 0;
    /* Change the current State to Idle */
    CAPWAP_TRC (CAPWAP_FSM_TRC, "Entered CAPWAP IDLE State \r\n");
    pSessEntry->eCurrentState = CAPWAP_IDLE;
    /* Clear Discovery response received earlier */
    CapwapClearDiscResponseEntry ();

#ifdef WTP_WANTED
    CapwapSendRestartDiscEvent ();
#endif
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    CAPWAP_FN_EXIT ();
}

/*****************************************************************************/
/* Function Name      : CapwapSendRestartDiscEvent                           */
/*                                                                           */
/* Description        : This function is to send restart discovery process   */
/*                      event to discovery task                              */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
CapwapSendRestartDiscEvent (VOID)
{
    CAPWAP_FN_ENTRY ();
    gDiscTaskGlobals.u2DiscCount = 0;
    /* Clear Discovery response received earlier */
    CapwapClearDiscResponseEntry ();
    if (OsixEvtSend (gDiscTaskGlobals.capwapDiscTaskId,
                     CAPWAP_DISC_RESTART_EVENT) == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapSendRestartDiscEvent :Sending the event failed \r\n");
        return;
    }
    CAPWAP_FN_EXIT ();
    return;
}

/*****************************************************************************/
/* Function Name      : CapwapPrimaryDiscIntervalTmrExp                      */
/*                                                                           */
/* Description        : This function handles the Primary Discovery timer    */
/*                      event to discovery task                              */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
CapwapPrimaryDiscIntervalTmrExp (VOID *pArg)
{
    tRemoteSessionManager *pSessEntry = NULL;
#ifdef WLC_WANTED
    tDiscTaskGloabls   *pDiscInfo = NULL;
#endif
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    unApHdlrMsgStruct  *pApHdlrMsgStruct = NULL;
    UINT1               u1WlcIndex = MAX_NUM_AC_FALLBACK + 1;
    UINT1               u1Index = 0;
    UINT1               u1Primary = 0;

    CAPWAP_FN_ENTRY ();
    WSS_IF_DB_TEMP_MEM_ALLOC_VOID_RETURN (pWssIfCapwapDB)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    WSS_IF_DB_TEMP_MEM_ALLOC_VOID_RETURN (pApHdlrMsgStruct);
    MEMSET (pApHdlrMsgStruct, 0, sizeof (unApHdlrMsgStruct));
#ifdef WLC_WANTED
    pDiscInfo = (tDiscTaskGloabls *) pArg;
#endif
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_RSM, pWssIfCapwapDB)
        == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Retrive CAPWAP DB \r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pApHdlrMsgStruct);
        return;
    }
    pSessEntry = pWssIfCapwapDB->pSessEntry;

    pWssIfCapwapDB->CapwapIsGetAllDB.bAcNamewithPriority = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpFallbackEnable = OSIX_TRUE;
    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = 0;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
        == OSIX_FAILURE)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pApHdlrMsgStruct);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to Validate the AC Name List \r\n");
        return;
    }

    for (u1Index = 0; u1Index < MAX_NUM_AC_FALLBACK; u1Index++)
    {
        if (pWssIfCapwapDB->CapwapGetDB.AcNameWithPri[u1Index].u1Priority == 1)
        {
            u1Primary = u1Index;
            break;
        }
    }
    for (u1Index = 0; u1Index < MAX_NUM_AC_FALLBACK; u1Index++)
    {
        if (MEMCMP
            (pWssIfCapwapDB->CapwapGetDB.AcNameWithPri[u1Primary].wlcName,
             gACInfoAfterDiscovery[u1Index].wlcName, CAPWAP_WLC_NAME_SIZE) == 0)
        {
            u1WlcIndex = u1Index;
            break;
        }
    }

    if (u1WlcIndex >= MAX_NUM_AC_FALLBACK)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Primary WLC IP Address NOT KNOWN...Restarting Primary Discovery Timer\r\n");
        CapwapDiscTmrStart (CAPWAP_PRIMARY_DISC_INTERVAL_TMR,
                            PRIMARY_DISC_INTERVAL_TIMEOUT);
    }
    else
    {

        /* gACInfoAfterDiscovery[0].incomingAddr.u4_addr[0] = 0xC0A80105; */

        /* Transmit Primary Discovery Request */
        if ((pWssIfCapwapDB->CapwapGetDB.u1WtpFallbackEnable == 1) &&
            (gACInfoAfterDiscovery[u1WlcIndex].incomingAddr.u4_addr[0] != 0))
        {
            if (pSessEntry->remoteIpAddr.u4_addr[0] !=
                gACInfoAfterDiscovery[u1WlcIndex].incomingAddr.u4_addr[0])
            {
                if (CapwapConstructPrimaryDiscReq
                    (&(pApHdlrMsgStruct->PriDiscReq)) == OSIX_SUCCESS)
                {
                    pApHdlrMsgStruct->PriDiscReq.u1AcRefOption = u1WlcIndex;
                    if (ApHdlrProcessWssIfMsg
                        (WSS_APHDLR_PRIMARY_DISC_REQ,
                         pApHdlrMsgStruct) == OSIX_FAILURE)
                    {
                        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Initiate"
                                    "primary discovery \r\n");
                    }
                }
            }
            CapwapDiscTmrStart (CAPWAP_PRIMARY_DISC_INTERVAL_TMR,
                                PRIMARY_DISC_INTERVAL_TIMEOUT);
        }
        else
        {
            CapwapDiscTmrStart (CAPWAP_PRIMARY_DISC_INTERVAL_TMR,
                                PRIMARY_DISC_INTERVAL_TIMEOUT);
        }
    }

    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    WSS_IF_DB_TEMP_MEM_RELEASE (pApHdlrMsgStruct);
    CAPWAP_FN_EXIT ();
    UNUSED_PARAM (pArg);
    return;
}

/*****************************************************************************/
/* Function Name      : CapwapDhcpOfferIntervalTmrExp                        */
/*                                                                           */
/* Description        : This function handles the DHCP Offer for AC IPs      */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
CapwapDhcpOfferIntervalTmrExp (VOID *pArg)
{
    tRemoteSessionManager *pSessEntry = NULL;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UNUSED_PARAM (pArg);
    WSS_IF_DB_TEMP_MEM_ALLOC_VOID_RETURN (pWssIfCapwapDB)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_RSM, pWssIfCapwapDB)
        != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapDhcpOfferIntervalTmrExp: "
                    "WssIfProcessCapwapDBMsg failed\r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return;
    }

    pSessEntry = pWssIfCapwapDB->pSessEntry;
    if (NULL == pSessEntry)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapDhcpOfferIntervalTmrExp: "
                    "Session entry Not available\r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return;

    }
    /* Discovery state needs to reenter              */
    /* 1) when if an offer is not received           */
    /* 2) when ip address not came in DHCP offer     */
    /* 3) in Both 1 & 2                              */
    if ((!gCapwapAcDiscInfo.uOfferRcvd) ||
        (!gCapwapAcDiscInfo.CapwapV4AcList.iLength) ||
        (!gCapwapDnsAcDiscInfo.u4Length))
    {
        if (WtpEnterDiscoveryState (pSessEntry) == OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "capwapMaxIntervalDiscTmrExp:Failed "
                        " to Enter Discovery State \r\n");
        }
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
}

/*****************************************************************************/
/* Function Name      : CapwapAcquireV4ACAddress                             */
/*                                                                           */
/* Description        : This is the callback function to update AC IP List   */
/*                      from DHCP Clients                                    */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            */
/*****************************************************************************/
INT4
CapwapAcquireV4ACAddress (UINT1 u1Len, UINT1 *u4AcIpList, UINT1 u1Flag)
{
    INT4                i4Ret = OSIX_SUCCESS;
    CAPWAP_FN_ENTRY ();
    if (OFFER_RECEVIED_WITH_OPTION == u1Flag)
    {
        gCapwapAcDiscInfo.CapwapV4AcList.iLength = u1Len;
        memcpy (&(gCapwapAcDiscInfo.CapwapV4AcList.iV4AcIpAddress),
                u4AcIpList, u1Len);
    }
    else if (u1Flag == OFFER_COMPLETED)
    {
        gCapwapAcDiscInfo.uOfferRcvd = 1;
    }

    CAPWAP_FN_EXIT ();
    return i4Ret;
}

/*****************************************************************************/
/*     FUNCTION NAME    : CapwapDnsSrvParamsFromDHCP                 */
/*                                                                           */
/*     DESCRIPTION      : This function is used to resolve the IP Address    */
/*                        from the English name                              */
/*                                                                           */
/*     INPUT            : pu1QueryName - Name to be resolved                 */
/*                                                                           */
/*     OUTPUT           : pResolvedIP - Resolved IP addresses                */
/*                        pu4NoOfIps - Number of Ip address                  */
/*                        pu4Error - Error Code                              */
/*                                                                           */
/*     RETURNS          : DNS_SUCCESS / DNS_FAILURE                          */
/*****************************************************************************/
INT4
CapwapDnsSrvParamsFromDHCP (UINT4 u4OptionType, UINT1 *pu1DhcpVal,
                            UINT4 u4DhcpLen)
{

    UINT4               u4TempIpAddr = 0;

    if (u4OptionType == DHCP_OPT_DNS_NS)
    {
        MEMCPY (&u4TempIpAddr, pu1DhcpVal, u4DhcpLen);
        u4TempIpAddr = OSIX_NTOHL (u4TempIpAddr);
        /* Update the Name server for in the global DNS List */
#if 0
        DnsCreateNameServer (0, IPVX_ADDR_FMLY_IPV4, pu1DhcpVal);
#endif
    }
    else if (u4OptionType == DHCP_OPT_DNS_NAME)
    {
        MEMCPY (gCapwapDnsAcDiscInfo.au1DnsDomainName, pu1DhcpVal, u4DhcpLen);
        gCapwapDnsAcDiscInfo.u4DnsDomainLen = u4DhcpLen;
    }

    return DNS_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : CapwapSetAcIPAddressFromDns            */
/*                                                                           */
/*     DESCRIPTION      : This function is used to resolve the IP Address    */
/*                        from the English name                              */
/*                                                                           */
/*     INPUT            : pu1QueryName - Name to be resolved                 */
/*                                                                           */
/*     OUTPUT           : pResolvedIP - Resolved IP addresses                */
/*                        pu4NoOfIps - Number of Ip address                  */
/*                        pu4Error - Error Code                              */
/*                                                                           */
/*     RETURNS          : DNS_SUCCESS / DNS_FAILURE                          */
/*****************************************************************************/
INT4
CapwapSetAcIPAddressFromDns (UINT4 u4Len, UINT1 *u4AcIpList, UINT4 u4Port)
{
    gCapwapDnsAcDiscInfo.u4Length = u4Len;
    memcpy (&(gCapwapDnsAcDiscInfo.iV4AcIpAddress), u4AcIpList, u4Len);
    gCapwapDnsAcDiscInfo.u2AcDestPort = (UINT2) u4Port;
    gCapwapDnsAcDiscInfo.u4DnsRespRcvd = TRUE;

    return DNS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CapwapGetDiscReqSentCount                            */
/*                                                                           */
/* Description        : This function returns Discovery Request transmitted  */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            */
/*****************************************************************************/
INT4
CapwapGetDiscReqSentCount (UINT2 *pDiscReqSentCount)
{
    CAPWAP_FN_ENTRY ();
    *pDiscReqSentCount = gDiscTaskGlobals.u2DiscCount;
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : CapwapDiscoveryTaskLock                                    */
/*                                                                           */
/* Description  : Lock the Capwap Discovery Task                             */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
CapwapDiscoveryTaskLock (VOID)
{
    CAPWAP_FN_ENTRY ();
    /*** For debug *****/
    CAPWAP_TRC1 (CAPWAP_MGMT_TRC,
                 "Capwap task lock. Capwap Discovery Sem Id is %d \n",
                 gDiscTaskGlobals.capwapDiscSemId);
    if (OsixSemTake (gDiscTaskGlobals.capwapDiscSemId) == OSIX_FAILURE)
    {
        CAPWAP_TRC1 (CAPWAP_FAILURE_TRC,
                     "TakeSem failure for %s \n",
                     CAPWAP_MUT_EXCL_DISC_SEM_NAME);
        return OSIX_FAILURE;
    }
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : CapwapDiscoveryTaskUnLock                                    */
/*                                                                           */
/* Description  : UnLock the Capwap Discovery Task                             */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
CapwapDiscoveryTaskUnLock (VOID)
{
    CAPWAP_FN_ENTRY ();
    /*** For debug *****/
    CAPWAP_TRC1 (CAPWAP_MGMT_TRC,
                 "Discovery task Unlock. Capwap Discovery Sem Id is %d \n",
                 gDiscTaskGlobals.capwapDiscSemId);
    OsixSemGive (gDiscTaskGlobals.capwapDiscSemId);

    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

#endif /* _CAPWAPDISCOVERY_C_ */
/*-----------------------------------------------------------------------*/
/*                    End of the file  capwapdiscovery.c                 */
/*-----------------------------------------------------------------------*/
