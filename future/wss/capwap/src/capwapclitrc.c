/* $Id: capwapclitrc.c,v 1.1.1.1 2016/09/20 10:42:39 siva Exp $ */
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include "capwapinc.h"
/***************************************************************************
*                                                                           *
* Function     : CapwapTrcPrint                                               *
*                                                                           *
* Description  :  prints the trace - with filename and line no              *
*                                                                           *
* Input        : fname   - File name                                        *
*                u4Line  - Line no                                          *
*                s       - strong to be printed                             *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : Returns string                                             *
*                                                                           *
*****************************************************************************/

VOID
CapwapTrcPrint (const char *fname, UINT4 u4Line, const char *s)
{

    tOsixSysTime        sysTime = 0;
    const char         *pc1Fname = fname;

    if (s == NULL)
    {
        return;
    }
    while (*fname != '\0')
    {
        if (*fname == '/')
            pc1Fname = (fname + 1);
        fname++;
    }
    OsixGetSysTime (&sysTime);
    printf ("CAPWAP: %d:%s:%d:   %s", sysTime, pc1Fname, u4Line, s);
}

/****************************************************************************
*                                                                           *
* Function     : CapwapTrcWrite                                               *
*                                                                           *
* Description  :  prints the trace - without filename and line no ,         *
*                 Useful for dumping packets                                *
*                                                                           *
* Input        : s - string to be printed                                   *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : Returns string                                             *
*                                                                           *
*****************************************************************************/

VOID
CapwapTrcWrite (CHR1 * s)
{
    if (s != NULL)
    {
        printf ("%s", s);
    }
}

/****************************************************************************
*                                                                           *
* Function     : CapwapTrc                                                    *
*                                                                           *
* Description  : converts variable argument in to string depending on flag  *
*                                                                           *
* Input        : u4Flags  - Trace flag                                      *
*                fmt  - format strong, variable argument
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : Returns string                                             *
*                                                                           *
*****************************************************************************/

CHR1               *
CapwapTrc (UINT4 u4Flags, const char *fmt, ...)
{
    va_list             ap;
#define CAPWAP_TRC_BUF_SIZE    2000
    static CHR1         buf[CAPWAP_TRC_BUF_SIZE];

    if ((u4Flags & CAPWAP_TRC_FLAG) > 0)
    {
        return (NULL);
    }
    va_start (ap, fmt);
    vsprintf (&buf[0], fmt, ap);
    va_end (ap);

    return (&buf[0]);
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  capwaptrc.c                      */
/*-----------------------------------------------------------------------*/
