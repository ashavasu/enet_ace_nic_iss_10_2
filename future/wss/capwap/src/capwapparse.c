/********************************************************************

 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 * $Id: capwapparse.c,v 1.3 2017/11/24 10:37:03 siva Exp $       
 *
 * DESCRIPTION    : Contains functions for CAPWAP Protocol message parsing
 ********************************************************************/
#ifndef __CAPWAP_PARSE_C__
#define __CAPWAP_PARSE_C__

#define _CAPWAP_GLOBAL_VAR

#include "capwapinc.h"
/*****************************************************************************
 *                                                                           *
 * Function     : CapwapParseControlPacket                                   *
 *                                                                           *
 * Description  : Parse the capwap Control packet                            *
 *                                                                           *
 * Input        : pktBuf   - Pointer to the buffer                           *
 *                                                                           *
 *                                                                           *
 * Output       : Capwap control message structure                           *
 *                                                                           *
 * Returns      : CAPWAP_SUCCESS / CAPWAP_FAILURE                            *
 *                                                                           *
 *****************************************************************************/
INT4
CapwapParseControlPacket (tCRU_BUF_CHAIN_HEADER * pBuf,
                          tCapwapControlPacket * pCapwapCtrlmsg)
{
    UINT4               pktOffset = 0;
    UINT4               u4MsgType = 0;
    UINT4               u4RetVal = 0;
    UINT1              *pu1Buf = NULL;
    UINT2               u2PacketLen = 0;
    UINT1               u1IsNotLinear = OSIX_FALSE;
    tMemPoolId          MemPoolId = 0;
    UINT2               u2Len = 0;

    CAPWAP_FN_ENTRY ();
    MEMSET (pCapwapCtrlmsg, 0, sizeof (tCapwapControlPacket));

    u2PacketLen = (UINT2) CAPWAP_GET_BUF_LEN (pBuf);
    pu1Buf = CAPWAP_BUF_IF_LINEAR (pBuf, 0, u2PacketLen);

    if (u2PacketLen <= CAPWAP_MAX_PKT_LEN)
    {
        if (pu1Buf == NULL)
        {
            pu1Buf = (UINT1 *) MemAllocMemBlk (CAPWAP_PKTBUF_POOLID);
            if (pu1Buf == NULL)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "CapwapParseControlPacket: "
                            "Memory Allocation FAILURE\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "CapwapParseControlPacket: "
                              "Memory Allocation FAILURE\r\n"));
                return OSIX_FAILURE;
            }
            CAPWAP_COPY_FROM_BUF (pBuf, pu1Buf, 0, u2PacketLen);
            u1IsNotLinear = OSIX_TRUE;
            MemPoolId = CAPWAP_PKTBUF_POOLID;
        }
    }
    else
    {
        if (pu1Buf == NULL)
        {
            pu1Buf = (UINT1 *) MemAllocMemBlk (CAPWAP_DATA_TRANSFER_POOLID);
            if (pu1Buf == NULL)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "CapwapParseControlPacket: "
                            "Memory Allocation FAILURE\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "CapwapParseControlPacket: Memory Allocation FAILURE\r\n"));
                return OSIX_FAILURE;
            }
            CAPWAP_COPY_FROM_BUF (pBuf, pu1Buf, 0, u2PacketLen);
            u1IsNotLinear = OSIX_TRUE;
            MemPoolId = CAPWAP_DATA_TRANSFER_POOLID;
        }
    }

    /* Parse the capwap header and populate it to a structure */
    CapwapProcessCapwapHdr (pu1Buf,
                            &pCapwapCtrlmsg->capwapHdr,
                            &pCapwapCtrlmsg->capwapCtrlHdr, &pktOffset);

    if ((pCapwapCtrlmsg->capwapCtrlHdr.u4MsgType > CAPWAP_PROT_MSG_ELEM_MAX) &&
        (pCapwapCtrlmsg->capwapCtrlHdr.u4MsgType < WLAN_CONF_REQ))
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Received the unrecognised message request \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Received the unrecognised message request \r\n"));
        pCapwapCtrlmsg->u4ReturnCode = UNRECOGNISED_MESSAGE_REQUEST;
        if (u1IsNotLinear == OSIX_TRUE)
        {
            MemReleaseMemBlock (MemPoolId, pu1Buf);
        }
        return UNRECOGNISED_MESSAGE_REQUEST;
    }
    if ((pCapwapCtrlmsg->capwapCtrlHdr.u4MsgType >= WLAN_CONF_REQ) &&
        (pCapwapCtrlmsg->capwapCtrlHdr.u4MsgType <= WLAN_CONF_RSP))
    {
        /* Get the IEEE Msg Index */
        u4MsgType = pCapwapCtrlmsg->capwapCtrlHdr.u4MsgType -
            CAPWAP_IEEE_MSG_INDEX_OFFSET;

        /* Add the IEEE Msg Index to CAPWAP last message index */
        u4MsgType += (CAPWAP_MAX_MSG - 1);
    }
    else
    {
        if (pCapwapCtrlmsg->capwapCtrlHdr.u4MsgType > CAPWAP_MAX_MSG)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "CapwapParseControlPacket:Capwap Ctrl MsgType INVALID\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "CapwapParseControlPacket:Capwap Ctrl MsgType INVALID\r\n"));
            if (u1IsNotLinear == OSIX_TRUE)
            {
                MemReleaseMemBlock (MemPoolId, pu1Buf);
            }
            return OSIX_FAILURE;
        }

        u4MsgType = pCapwapCtrlmsg->capwapCtrlHdr.u4MsgType;
    }

    u4RetVal = (UINT4) CapwapParseCtrlMsgBlocks (pu1Buf,
                                                 pCapwapCtrlmsg,
                                                 (UINT2) pktOffset, u4MsgType);
    if (u4RetVal != CAPWAP_SUCCESS)
    {
        CAPWAP_TRC2 (CAPWAP_FAILURE_TRC,
                     "capwapParseControlPacket:Error in parsing the CAPWAP "
                     "packet u4RetVal = %d  %d \r\n",
                     u4RetVal, pCapwapCtrlmsg->capwapCtrlHdr.u4MsgType);
        pCapwapCtrlmsg->u4ReturnCode = u4RetVal;
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "capwapParseControlPacket:Error in parsing the CAPWAP "
                      "packet u4RetVal = %d  %d \r\n",
                      u4RetVal, pCapwapCtrlmsg->capwapCtrlHdr.u4MsgType));
        pCapwapCtrlmsg->u4ReturnCode = u4RetVal;

        /* MISSING_MANDATORY_MSG_ELEMENT or UNRECOGNISED_MESSAGE_ELEMENT */
        if (u1IsNotLinear == OSIX_TRUE)
        {
            MemReleaseMemBlock (MemPoolId, pu1Buf);
        }
        return (INT4) u4RetVal;
    }

    if (u1IsNotLinear == OSIX_TRUE)
    {
        MemReleaseMemBlock (MemPoolId, pu1Buf);
    }
    if (gu4CapwapDebugMask & CAPWAP_PKTDUMP_TRC)
    {
        DumpParsedData (pCapwapCtrlmsg);
        u2Len = (UINT2) CAPWAP_GET_BUF_LEN (pBuf);
        CAPWAP_PKT_DUMP (DUMP_TRC, pBuf, u2Len,
                         "DUMPING CAPWAP CONTROL PACKET.....\n");
    }
    CAPWAP_FN_EXIT ();
    return CAPWAP_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : CapwapProcessCapwapHdr                                     *
 *                                                                           *
 * Description  : Parse the capwap header and Control Header                 *
 *                                                                           *
 * Input        : pBuf   - Pointer to the buffer                             *
 *                                                                           *
 * Output       : cwHdr   - pointer to capwap header                         *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
VOID
CapwapProcessCapwapHdr (UINT1 *pBuf,
                        tCapwapHdr * cwhdr,
                        tCapwapCtrlHdr * cwCtrlHdr, UINT4 *pktOffset)
{
    UINT1              *pu1ReadPtr = NULL;

    CAPWAP_FN_ENTRY ();
    /* Copy the pointer locally and use it */
    pu1ReadPtr = pBuf;
    /* Get the preamble field */
    CAPWAP_GET_1BYTE (cwhdr->u1Preamble, pu1ReadPtr);
    /* Get the Hlen, RID, WBID, and flag T field */
    CAPWAP_GET_2BYTE (cwhdr->u2HlenRidWbidFlagT, pu1ReadPtr);
    /* Get the Flags F,L,W,M,K, 3bits reserved flags */
    CAPWAP_GET_1BYTE (cwhdr->u1FlagsFLWMKResd, pu1ReadPtr);
    /* Get the 16 bit fragment id field */
    CAPWAP_GET_2BYTE (cwhdr->u2FragId, pu1ReadPtr);
    /* Get the frag offset and 3 bits reserved field */
    CAPWAP_GET_2BYTE (cwhdr->u2FragOffsetRes3, pu1ReadPtr);
    /* get the hlen value and if it is greater than 2,
     * fetch the optional MAC address and wireless specific info*/
    *pktOffset += CAPWAP_HDR_LEN;

    if ((cwhdr->u1FlagsFLWMKResd) & CAPWAP_HEADER_RMAC_MASK)
    {                            /* Optional Radio MAC address is present in the packet */
        CAPWAP_GET_1BYTE (cwhdr->u1RadMacAddrLength, pu1ReadPtr);
        CAPWAP_GET_NBYTE (cwhdr->RadioMacAddr, pu1ReadPtr, sizeof (tMacAddr));
        pu1ReadPtr += CAPWAP_HEADER_RMAC_PAD;
        *pktOffset += CAPWAP_HEADER_RADIO_MAC_LEN + CAPWAP_HEADER_RMAC_PAD + 1;
    }
    if ((cwhdr->u1FlagsFLWMKResd) & CAPWAP_HEADER_WIFI_INFO_MASK)
    {                            /* Optional wireless info is present in the packet */
        CAPWAP_GET_1BYTE (cwhdr->u1WirelessInfoLength, pu1ReadPtr);
        CAPWAP_GET_4BYTE (cwhdr->u4WirelessRadioInfo, pu1ReadPtr);
        pu1ReadPtr += CAPWAP_HEADER_WIFI_INFO_PAD;
        *pktOffset +=
            CAPWAP_HEADER_WIRELESS_INFO_LEN + CAPWAP_HEADER_WIFI_INFO_PAD + 1;
    }
    /* Get the control header */
    /* Parse the CAPWAP control header and store the values in the structure */
    CAPWAP_GET_4BYTE (cwCtrlHdr->u4MsgType, pu1ReadPtr);
    CAPWAP_GET_1BYTE (cwCtrlHdr->u1SeqNum, pu1ReadPtr);
    CAPWAP_GET_2BYTE (cwCtrlHdr->u2MsgElementLen, pu1ReadPtr);
    /* Skip one byte reserved flags field */
    *pktOffset += CAPWAP_CONTROL_HDR_LEN;    /* For flags */
    CAPWAP_FN_EXIT ();

    return;
}

/*****************************************************************************
 *                                                                           *
 * Function     : CapwapParseCtrlMsgBlocks                                   *
 *                                                                           *
 * Description  : Parse the Capwap Msg Elements in the Capwap Msg            *
 *                                                                           *
 * Input        : pMesg   - Pointer to the packet buffer                     *
 *                pktOffset - Packet offset from where to start the parsing  *
 *                u4MsgType - MessageType
 *                                                                           *
 * Output       : capwapCtrlMsg - The message elements of capwap control Msg *
 *                                                                           *
 * Returns      : OSIX_SUCCESS | OSIX_FAILURE                                *
 *                                                                           *
 *****************************************************************************/
INT4
CapwapParseCtrlMsgBlocks (UINT1 *pMesg,
                          tCapwapControlPacket * capwapCtrlMsg,
                          UINT2 pktOffset, UINT4 u4MsgType)
{
    UINT1              *pu1ReadPtr = NULL;
    UINT2               u2OptionalElementIndex = 0;
    UINT2               u2Type = 0;
    UINT2               u2LenParsed = 0;
    UINT2               u2ElemIndex = 0;
    INT4                i4ParsingState = 0;
    UINT2               u2MaxMandMsgElem = 0;
    UINT2               u2NumOptionalMsgElem = 0;
    UINT2               u2MesgElemLen = 0;
    UINT2               u2NumRadioElems = 0;
    UINT1               u1IsMultiElem = 0;
    UINT2               u2ExpMandMsgElemCnt = 0;
    UINT1               u1IsMandOrOpt = 3;
    tCapwapMsgElement   capwapMsg;

    CAPWAP_FN_ENTRY ();

    if (u4MsgType < CAPWAP_MAX_EVENTS)
    {
        u2ExpMandMsgElemCnt = gu2CapwapMandMsgElemCnt[u4MsgType];
        u2OptionalElementIndex = u2ExpMandMsgElemCnt;
    }
    else
    {
        CAPWAP_TRC1 (CAPWAP_FAILURE_TRC,
                     "Invalid CAPWAP Message Type = %d\r\n", u4MsgType);
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Invalid CAPWAP Message Type = %d\r\n", u4MsgType));
        return OSIX_FAILURE;
    }

    /* Parse the packet till the message element len.
     * CAPWAP_CMN_MSG_ELM_HEAD_LEN includes the Mesg element length 
     * itself and Flags length in the control header */

    u2MesgElemLen = (UINT2) (capwapCtrlMsg->capwapCtrlHdr.u2MsgElementLen -
                             CAPWAP_CMN_MSG_ELM_HEAD_LEN);

    while ((i4ParsingState == OSIX_SUCCESS) && (u2MesgElemLen > 0))
    {
        /* Copy the pointer locally and use it */
        pu1ReadPtr = pMesg;

        /* Add the length parsed to the pkt offset */
        pktOffset = (UINT2) (pktOffset + u2LenParsed);

        CAPWAP_MEMSET (&capwapMsg, CAPWAP_INIT_VAL, sizeof (tCapwapMsgElement));

        /* Point to the offset from where the message element to be parsed */
        pu1ReadPtr += pktOffset;

        /* Capwap message element */
        /***********************************************************************
         *       0                   1                   2                   3 *
         *    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1  *
         *   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ *
         *   |              Type             |             Length            | *
         *   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ *
         *   |   Value ...   |                                                 *
         *   +-+-+-+-+-+-+-+-+                                                 *
         **********************************************************************/

        /* Get the 2 byte message element type */
        CAPWAP_GET_2BYTE (u2Type, pu1ReadPtr);

        /* CAPWAP protocol Message Elements parsing */
        u1IsMultiElem = OSIX_FALSE;

        if (u2Type != VENDOR_SPECIFIC_PAYLOAD)
        {
            /* Check if the Message Element is of Single Instance Type
             * or Multiple Instance Type */
            u1IsMultiElem = CapwapIsMultiElemOfSameType (u2Type);

            if (((u2Type >= CAPWAP_PROT_MSG_ELEM_MIN) &&
                 (u2Type <= CAPWAP_PROT_MSG_ELEM_MAX))
                ||
                ((u2Type >= CAPWAP_IEEE_802_11_MSG_ELEM_MIN) &&
                 (u2Type <= CAPWAP_IEEE_802_11_MSG_ELEM_MAX)))
            {
                i4ParsingState = CapwapParseMsgElement (pu1ReadPtr,
                                                        pktOffset,
                                                        &capwapMsg,
                                                        &u2LenParsed,
                                                        u2Type, u1IsMultiElem);
                if (i4ParsingState != OSIX_SUCCESS)
                {
                    CAPWAP_TRC1 (CAPWAP_FAILURE_TRC,
                                 "Failed to parse the received MsgElem, "
                                 "Return Code = %d \r\n", i4ParsingState);
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Failed to parse the received MsgElem, "
                                  "Return Code = %d \r\n", i4ParsingState));
                    i4ParsingState = OSIX_FAILURE;
                    break;
                }
            }
            else                /* Unknown message element */
            {
                CAPWAP_TRC1 (CAPWAP_FAILURE_TRC,
                             "Unknown CAPWAP Message element type = %d\r\n",
                             u2Type);
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Unknown CAPWAP Message element type = %d\r\n",
                              u2Type));
                i4ParsingState = OSIX_FAILURE;
                break;
            }
        }
        else
        {
            i4ParsingState = CapwapParseVendorSpecificPayload (pu1ReadPtr,
                                                               pktOffset,
                                                               &capwapMsg,
                                                               &u2LenParsed);
            if (i4ParsingState != OSIX_SUCCESS)
            {
                CAPWAP_TRC1 (CAPWAP_FAILURE_TRC,
                             "Failed to parse Vendor specific payload, "
                             "Element type = %d \r\n", u2Type);
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to parse Vendor specific payload, "
                              "Element type = %d \r\n", u2Type));
                MemReleaseMemBlock (CAPWAP_BULKOFFSET_POOLID,
                                    (UINT1 *) capwapMsg.pu2Offset);

                i4ParsingState = OSIX_FAILURE;
                break;
            }
        }

        /* Subtract the length parsed from the total message element length */
        u2MesgElemLen = (UINT2) (u2MesgElemLen - u2LenParsed);

        /* Check if the MsgElem is mandatory or optional */
        u1IsMandOrOpt = CapwapIsMsgElemMandOrOptional (u4MsgType,
                                                       u2Type, &u2ElemIndex);

        if (u1IsMandOrOpt == CAPWAP_MSG_ELEM_OPTIONAL)
        {
            u2ElemIndex =
                (UINT2) (u2OptionalElementIndex + u2NumOptionalMsgElem);
        }

        /* Copy the message elements into Capwap Msg Structure */

        if ((u1IsMandOrOpt == CAPWAP_MSG_ELEM_MANDATORY) ||
            (u1IsMandOrOpt == CAPWAP_MSG_ELEM_OPTIONAL))
        {
            /* Message Element is present only once in this msgtype */
            if (!u1IsMultiElem)
            {
                if (capwapCtrlMsg->capwapMsgElm[u2ElemIndex].pu2Offset == NULL)
                {
                    CAPWAP_MEMCPY (&capwapCtrlMsg->
                                   capwapMsgElm[u2ElemIndex],
                                   &capwapMsg, sizeof (tCapwapMsgElement));

                    if (u1IsMandOrOpt == CAPWAP_MSG_ELEM_MANDATORY)
                    {
                        u2MaxMandMsgElem++;
                    }
                }
                else
                {
                    i4ParsingState = OSIX_FAILURE;
                    CAPWAP_TRC2 (CAPWAP_FAILURE_TRC,
                                 "Message element %d available in MsgType"
                                 " %d more than once\r\n", u2Type, u4MsgType);
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Message element %d available in MsgType"
                                  " %d more than once\r\n", u2Type, u4MsgType));
                }
            }
            else
            {
                /* More than one Message element of same type is possible */

                u2NumRadioElems = capwapCtrlMsg->capwapMsgElm[u2ElemIndex].
                    numMsgElemInstance;

                if (u2NumRadioElems == 0)
                {
                    capwapCtrlMsg->capwapMsgElm[u2ElemIndex].pu2Offset =
                        (UINT2 *) MemAllocMemBlk (CAPWAP_BULKOFFSET_POOLID);

                    if (capwapCtrlMsg->capwapMsgElm[u2ElemIndex].pu2Offset
                        == NULL)
                    {
                        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "MemAlloc failed "
                                    "in CapwapParseCtrlMsgBlocks\n");
                        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                      "MemAlloc failed "
                                      "in CapwapParseCtrlMsgBlocks\n"));
                        i4ParsingState = OSIX_FAILURE;
                        break;
                    }

                    if (u1IsMandOrOpt == CAPWAP_MSG_ELEM_MANDATORY)
                    {
                        u2MaxMandMsgElem++;
                    }
                }

                if (u2NumRadioElems <= CAPWAP_MAX_RADIO_ID)
                {
                    capwapCtrlMsg->capwapMsgElm[u2ElemIndex].u2MsgType = u2Type;
                    capwapCtrlMsg->capwapMsgElm[u2ElemIndex].
                        pu2Offset[u2NumRadioElems] = pktOffset;
                    u2NumRadioElems++;
                    capwapCtrlMsg->capwapMsgElm[u2ElemIndex].numMsgElemInstance
                        = u2NumRadioElems;
                }
                else
                {
                    i4ParsingState = OSIX_FAILURE;
                    CAPWAP_TRC2 (CAPWAP_FAILURE_TRC,
                                 "Message element %d available in MsgType %d"
                                 " more than MAX Radio\r\n", u2Type, u4MsgType);
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Message element %d available in MsgType %d"
                                  " more than MAX Radio\r\n", u2Type,
                                  u4MsgType));
                }
            }

            if ((i4ParsingState != OSIX_FAILURE) &&
                (u1IsMandOrOpt == CAPWAP_MSG_ELEM_OPTIONAL))
            {
                u2NumOptionalMsgElem++;
            }
        }
        else
        {
            CAPWAP_TRC2 (CAPWAP_FAILURE_TRC,
                         "Received Unrecognised message element %d"
                         " for MsgType %d\n", u2Type, u4MsgType);
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Received Unrecognised message element %d"
                          " for MsgType %d\n", u2Type, u4MsgType));
            i4ParsingState = UNRECOGNISED_MESSAGE_ELEMENT;
        }
    }

    /* Check for the number of mandatory elements */
    if ((u2MaxMandMsgElem != u2ExpMandMsgElemCnt) &&
        (i4ParsingState == OSIX_SUCCESS))
    {
        CAPWAP_TRC1 (CAPWAP_FAILURE_TRC,
                     "The mandatory number of message elements are"
                     "missing in the type %d packet\n", u4MsgType);
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "The mandatory number of message elements are"
                      "missing in the type %d packet\n", u4MsgType));
        i4ParsingState = MISSING_MANDATORY_MSG_ELEMENT;
    }

    /* Update the number of optional and mandatory message elements 
     * present in the packet */
    capwapCtrlMsg->numOptionalElements = u2NumOptionalMsgElem;
    capwapCtrlMsg->numMandatoryElements = u2MaxMandMsgElem;

    /* Release Message Element allocated Memory */
    if (i4ParsingState != OSIX_SUCCESS)
    {
        CapwapParseStructureMemrelease (capwapCtrlMsg);
    }

    CAPWAP_FN_EXIT ();

    return i4ParsingState;
}

/*****************************************************************************
 *                                                                           *
 * Function     : CapwapIsMsgElemMandOrOptional                              *
 *                                                                           *
 * Description  : This funtion checks if the message element for a message   *
 *                type is mandatory or optional                              *
 *                                                                           *
 * Input        : u4MsgType - CAPWAP Message Type                            *
 *                u2ElemType - Message Element Type                          *
 *                                                                           *
 * Output       : u2ElemIndex   - Message Element Index                      *
 *                                                                           *
 * Returns      : MANDATORY | OPTIONAL | NOT_PRESENT                         *
 *                                                                           *
 *****************************************************************************/
UINT1
CapwapIsMsgElemMandOrOptional (UINT4 u4MsgType,
                               UINT2 u2ElemType, UINT2 *u2ElemIndex)
{
    UINT1               u1RetVal = CAPWAP_MSG_ELEM_NOT_PRESENT;
    UINT1               u1Index = 0;
    UINT2               u2MandIndex = 0;
    UINT2               u2OptIndex = 1;
    UINT2               u2MsgType = 0;
    UINT2               u2MandElemVal = 0;
    UINT2               u2OptElemVal = 0;

    CAPWAP_FN_ENTRY ();

    u2MsgType = (UINT2) u4MsgType;

    if (u2MsgType >= CAPWAP_MAX_EVENTS)
    {
        CAPWAP_TRC1 (CAPWAP_FAILURE_TRC,
                     "Invalid CAPWAP Message Type = %d\r\n", u4MsgType);
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Invalid CAPWAP Message Type = %d\r\n", u4MsgType));
        return OSIX_FAILURE;
    }

    for (u1Index = 0; u1Index < CAPWAP_MAX_MSG_ELEMS; u1Index++)
    {
        u2MandElemVal = gu2CapwapMsgTypeToElemMap[u2MsgType]
            [u2MandIndex][u1Index];

        if (u2MandElemVal == 0)
        {
            break;
        }

        if (u2MandElemVal == u2ElemType)
        {
            u1RetVal = CAPWAP_MSG_ELEM_MANDATORY;
            *u2ElemIndex = u1Index;
            break;
        }
    }

    if (u1RetVal == CAPWAP_MSG_ELEM_NOT_PRESENT)
    {
        for (u1Index = 0; u1Index < CAPWAP_MAX_MSG_ELEMS; u1Index++)
        {
            u2OptElemVal = gu2CapwapMsgTypeToElemMap[u2MsgType]
                [u2OptIndex][u1Index];
            if (u2OptElemVal == 0)
            {
                break;
            }

            if (u2OptElemVal == u2ElemType)
            {
                u1RetVal = CAPWAP_MSG_ELEM_OPTIONAL;
                break;
            }
        }
    }

    if (u1RetVal == CAPWAP_MSG_ELEM_NOT_PRESENT)
    {
        for (u1Index = 0; u1Index < CAPWAP_MAX_MSG_ELEMS; u1Index++)
        {
            u2MandElemVal = gu2CapwapMsgTypeToElemMap[u2MsgType]
                [u2MandIndex][u1Index];

            if (u2MandElemVal == 0)
            {
                break;
            }

            if ((u2MandElemVal & u2ElemType) == u2ElemType)
            {
                u1RetVal = CAPWAP_MSG_ELEM_MANDATORY;
                *u2ElemIndex = u1Index;
                break;
            }
        }
    }

    CAPWAP_FN_EXIT ();
    return u1RetVal;
}

/*****************************************************************************
 *                                                                           *
 * Function     : CapwapIsMultiElemOfSameType                                *
 *                                                                           *
 * Description  : This function checks if the message element occurs only    *
 *                once or multiple times in a given message type             *
 *                                                                           *
 * Input        : u2MsgType - Message Type                                   *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_TRUE | OSIX_FALSE                                     *
 *                                                                           *
 *****************************************************************************/
UINT1
CapwapIsMultiElemOfSameType (UINT2 u2MsgType)
{
    UINT1               u1RetVal = OSIX_FALSE;

    CAPWAP_FN_ENTRY ();

    switch (u2MsgType)
    {
        case AC_DESCRIPTOR:
        case AC_IPv4_LIST:
        case AC_IPv6_LIST:
        case AC_NAME:
        case AC_NAME_WITH_PRIO:
        case AC_TIMESTAMP:
        case ADD_MAC_ACL_ENTRY:
        case CAPWAP_CTRL_IPV4_ADDR:
        case CAPWAP_CTRL_IPV6_ADDR:
        case CAPWAP_TIMERS:
        case DATA_TRANSFER_DATA:
        case DATA_TRANSFER_MODE:
        case DECRYPTION_ERROR_REPORT_PERIOD:
        case DELETE_MAC_ACL_ENTRY:
        case DISCOVERY_TYPE:
        case IDLE_TIMEOUT:
        case IMAGE_DATA:
        case IMAGE_IDENTIFIER:
        case IMAGE_INFORMATION:
        case INITIATE_DOWNLOAD:
        case MTU_DISC_PADDING:
        case LOCATION_DATA:
        case MAX_MESG_LEN:
        case CAPWAP_LOCAL_IPV4_ADDR:
        case RESULT_CODE:
        case SESSION_ID:
        case STATS_TIMER:
        case WTP_BOARD_DATA:
        case WTP_DESCRIPTOR:
        case WTP_FALLBACK:
        case WTP_FRAME_TUNNEL_MODE:
        case WTP_MAC_TYPE:
        case WTP_NAME:
        case WTP_STATIC_IP_ADDR_INFO:
        case CAPWAP_LOCAL_IPV6_ADDR:
        case CAPWAP_TRANSPORT_PROTOCOL:
        case ECN_SUPPORT:
        case IEEE_ADD_WLAN:
        case IEEE_UPDATE_WLAN:
        case IEEE_DELETE_WLAN:
        case IEEE_ASSIGNED_WTPBSSID:
        case IEEE_WTP_RADIO_FAIL_ALARAM:
            u1RetVal = OSIX_FALSE;
            break;

        case ADD_STATION:
        case RADIO_OPER_STATE:
        case DELETE_STATION:
        case DECRYPTION_ERROR_REPORT:
        case DUPLICATE_IPV4_ADDR:
        case DUPLICATE_IPV6_ADDR:
        case WTP_RADIO_STATISTICS:
        case WTP_REBOOT_STATISTICS:
        case RADIO_ADMIN_STATE:
        case IEEE_ANTENNA:
        case IEEE_DIRECT_SEQUENCE_CONTROL:
        case IEEE_INFORMATION_ELEMENT:
        case IEEE_MAC_OPERATION:
        case IEEE_MIC_COUNTERMEASURES:
        case IEEE_MULTIDOMAIN_CAPABILITY:
        case IEEE_OFDM_CONTROL:
        case IEEE_RATE_SET:
        case IEEE_RSNA_ERROR_REPORT:
        case IEEE_STATION:
        case IEEE_STATION_QOS_PROFILE:
        case IEEE_STATION_SESSION_KEY:
        case IEEE_STATISTICS:
        case IEEE_SUPPORTED_RATES:
        case IEEE_TX_POWER:
        case IEEE_TX_POWERLEVEL:
        case IEEE_UPDATE_STATION_QOS:
        case IEEE_WTP_QOS:
        case IEEE_WTP_RADIO_CONFIGURATION:
        case WTP_RADIO_INFO:
            u1RetVal = OSIX_TRUE;
            break;

        default:
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Received the unknown message type \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Received the unknown message type \r\n"));
            return OSIX_FALSE;
        }
            break;
    }

    CAPWAP_FN_EXIT ();
    return u1RetVal;
}

/*****************************************************************************
 *                                                                           *
 * Function     : CapwapParseMsgElement                                      *
 *                                                                           *
 * Description  : Parse the capwap message element len and offset            *
 *                                                                           *
 * Input        : pMesg   - Pointer to the buffer                            *
 *                pktOffset - Packet Offset of this message element          *
 *                u2MsgType - Message Type                                   *
 *                u1IsMultiElem - Is Message Element present multiple times  *
 *                                                                           *
 * Output       : capwapMsg   - pointer to capwap message                    *
 *                u2LenParsed - Length parsed                                * 
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
INT4
CapwapParseMsgElement (UINT1 *pMesg,
                       UINT2 pktOffset,
                       tCapwapMsgElement * capwapMsg,
                       UINT2 *u2LenParsed, UINT2 u2MsgType, UINT1 u1IsMultiElem)
{
    UINT2               u2Len = 0;

    CAPWAP_FN_ENTRY ();

    /* Get the length field */
    CAPWAP_GET_2BYTE (u2Len, pMesg);

    /* Validate the length field */
    if (CapwapValidateMsgElemLen (u2MsgType, u2Len) != OSIX_SUCCESS)
    {
        CAPWAP_TRC1 (CAPWAP_FAILURE_TRC,
                     "Invalid Len field for MsgType %d \r\n", u2MsgType);
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Invalid Len field for MsgType %d \r\n", u2MsgType));
        return OSIX_FAILURE;
    }

    /* Validate the message elem value */
    if (CapwapValidateMsgElemVal (u2MsgType, u2Len, pMesg) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    /* Fill the capwap message structure with TLO format */
    capwapMsg->u2MsgType = u2MsgType;

    if (!u1IsMultiElem)
    {
        capwapMsg->numMsgElemInstance = CAPWAP_DEFAULT_MESG_ELEM_INSTANCE;

        capwapMsg->pu2Offset = (UINT2 *) MemAllocMemBlk (CAPWAP_OFFSET_POOLID);
        if (capwapMsg->pu2Offset == NULL)
        {
            CAPWAP_TRC1 (CAPWAP_FAILURE_TRC,
                         "Mem alloc failed for message element %d \r\n",
                         u2MsgType);
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Mem alloc failed for message element %d \r\n",
                          u2MsgType));
            return OSIX_FAILURE;
        }

        capwapMsg->pu2Offset[0] = pktOffset;
    }

    *u2LenParsed = (UINT2) (u2Len + CAPWAP_MSG_ELEM_TYPE_LEN);

    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : CapwapValidateMsgElemLen                                   *
 *                                                                           *
 * Description  : Validates the message element length                       *
 *                                                                           *
 * Input        : u2MsgType - Message Type                                   *
 *                u2Len - Message Element length                             *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS | OSIX_FAILURE                                *
 *                                                                           *
 *****************************************************************************/
INT4
CapwapValidateMsgElemLen (UINT2 u2MsgType, UINT2 u2Len)
{
    INT4                i4RetVal = OSIX_SUCCESS;
    UINT2               u2ExpectedLen = 0;
    UINT2               u2ExpectedMinLen = 0;
    UINT2               u2ExpectedMaxLen = 0;
    UINT1               u1Operation = OPERATION_NO_CHECK;

    CAPWAP_FN_ENTRY ();

    switch (u2MsgType)
    {
        case AC_DESCRIPTOR:
        {
            u2ExpectedLen = AC_DESCRIPTOR_MIN_LEN;
            u1Operation = OPERATION_LESS_THAN;
        }
            break;

        case AC_IPv4_LIST:
        {
            u2ExpectedLen = AC_IPv4_LIST_MIN_LEN;
            u1Operation = OPERATION_LESS_THAN;
        }
            break;

        case AC_IPv6_LIST:
        {
            u2ExpectedLen = AC_IPv6_LIST_MIN_LEN;
            u1Operation = OPERATION_LESS_THAN;
        }
            break;

        case AC_NAME:
        {
            u2ExpectedLen = AC_NAME_MIN_LEN;
            u1Operation = OPERATION_LESS_THAN;
        }
            break;

        case AC_NAME_WITH_PRIO:
        {
            u2ExpectedLen = AC_NAME_WITH_PRIO_MIN_LEN;
            u1Operation = OPERATION_LESS_THAN;
        }
            break;

        case AC_TIMESTAMP:
        {
            u2ExpectedLen = AC_TIMESTAMP_LEN;
            u1Operation = OPERATION_NOT_EQUALTO;
        }
            break;

        case ADD_MAC_ACL_ENTRY:
        {
            u2ExpectedLen = ADD_MAC_ACL_ENTRY_MIN_LEN;
            u1Operation = OPERATION_LESS_THAN;
        }
            break;

        case ADD_STATION:
        {
            u2ExpectedLen = ADD_STATION_MIN_LEN;
            u1Operation = OPERATION_LESS_THAN;
        }
            break;

        case CAPWAP_CTRL_IPV4_ADDR:
        {
            u2ExpectedLen = CAPWAP_CTRL_IPV4_ADDR_LEN;
            u1Operation = OPERATION_NOT_EQUALTO;
        }
            break;

        case CAPWAP_CTRL_IPV6_ADDR:
        {
            u2ExpectedLen = CAPWAP_CTRL_IPV6_ADDR_LEN;
            u1Operation = OPERATION_NOT_EQUALTO;
        }
            break;

        case CAPWAP_TIMERS:
        {
            u2ExpectedLen = CAPWAP_TIMERS_LEN;
            u1Operation = OPERATION_LESS_THAN;
        }
            break;

        case DATA_TRANSFER_DATA:
        {
            u2ExpectedLen = DATA_TRANSFER_DATA_MIN_LEN;
            u1Operation = OPERATION_LESS_THAN;
        }
            break;

        case DATA_TRANSFER_MODE:
        {
            u2ExpectedLen = DATA_TRANSFER_MODE_LEN;
            u1Operation = OPERATION_LESS_THAN;
        }
            break;

        case DECRYPTION_ERROR_REPORT:
        {
            u2ExpectedLen = DECRYPTION_ERROR_REPORT_PERIOD_LEN;
            u1Operation = OPERATION_NOT_EQUALTO;
        }
            break;

        case DECRYPTION_ERROR_REPORT_PERIOD:
        {
            u2ExpectedLen = DECRYPTION_ERROR_REPORT_PERIOD_LEN;
            u1Operation = OPERATION_NOT_EQUALTO;
        }
            break;

        case DELETE_MAC_ACL_ENTRY:
        {
            u2ExpectedLen = DELETE_MAC_ACL_ENTRY_MIN_LEN;
            u1Operation = OPERATION_LESS_THAN;
        }
            break;

        case DELETE_STATION:
        {
            u2ExpectedLen = DELETE_STATION_MIN_LEN;
            u1Operation = OPERATION_LESS_THAN;
        }
            break;

        case DISCOVERY_TYPE:
        {
            u2ExpectedLen = DISC_TYPE_LEN;
            u1Operation = OPERATION_NOT_EQUALTO;
        }
            break;

        case DUPLICATE_IPV4_ADDR:
        {
            u2ExpectedLen = DUPLICATE_IPV4_ADDR_MIN_LEN;
            u1Operation = OPERATION_NOT_EQUALTO;
        }
            break;

        case DUPLICATE_IPV6_ADDR:
        {
            u2ExpectedLen = DUPLICATE_IPV6_ADDR_MIN_LEN;
            u1Operation = OPERATION_NOT_EQUALTO;
        }
            break;

        case IDLE_TIMEOUT:
        {
            u2ExpectedLen = IDLE_TIMEOUT_LEN;
            u1Operation = OPERATION_LESS_THAN;
        }
            break;

        case IMAGE_DATA:
        {
            u2ExpectedLen = IMAGE_DATA_MIN_LEN;
            u1Operation = OPERATION_LESS_THAN;
        }
            break;

        case LOCATION_DATA:
        {
            u2ExpectedMinLen = LOCATION_DATA_MIN_LEN;
            u2ExpectedMaxLen = LOCATION_DATA_MAX_LEN;
            u1Operation = OPERATION_BETWEEN;
        }
            break;

        case MAX_MESG_LEN:
        {
            u2ExpectedLen = MAX_MESGLEN_LEN;
            u1Operation = OPERATION_NOT_EQUALTO;
        }
            break;

        case CAPWAP_LOCAL_IPV4_ADDR:
        {
            u2ExpectedLen = CAPWAP_LOCAL_IPV4_ADDR_LEN;
            u1Operation = OPERATION_NOT_EQUALTO;
        }
            break;

        case RADIO_ADMIN_STATE:
        {
            u2ExpectedLen = RADIO_ADMIN_STATE_LEN;
            u1Operation = OPERATION_NOT_EQUALTO;
        }
            break;

        case RADIO_OPER_STATE:
        {
            u2ExpectedLen = RADIO_OPER_STATE_LEN;
            u1Operation = OPERATION_NOT_EQUALTO;
        }
            break;

        case RESULT_CODE:
        {
            u2ExpectedLen = RESULT_CODE_LEN;
            u1Operation = OPERATION_NOT_EQUALTO;
        }
            break;

        case SESSION_ID:
        {
            u2ExpectedLen = SESSION_ID_LEN;
            u1Operation = OPERATION_NOT_EQUALTO;
        }
            break;

        case STATS_TIMER:
        {
            u2ExpectedLen = STATS_TIMER_LEN;
            u1Operation = OPERATION_LESS_THAN;
        }
            break;

        case WTP_BOARD_DATA:
        {
            u2ExpectedLen = MIN_WTP_BOARD_DATA_LEN;
            u1Operation = OPERATION_LESS_THAN;
        }
            break;

        case WTP_DESCRIPTOR:
        {
            u2ExpectedLen = MIN_WTP_DESCRIPTOR_LEN;
            u1Operation = OPERATION_LESS_THAN;
        }
            break;

        case WTP_FALLBACK:
        {
            u2ExpectedLen = WTP_FALLBACK_LEN;
            u1Operation = OPERATION_LESS_THAN;
        }
            break;

        case WTP_FRAME_TUNNEL_MODE:
        {
            u2ExpectedLen = WTP_FRAME_TUNN_MODE_LEN;
            u1Operation = OPERATION_NOT_EQUALTO;
        }
            break;

        case WTP_MAC_TYPE:
        {
            u2ExpectedLen = WTP_MAC_TYPE_LEN;
            u1Operation = OPERATION_NOT_EQUALTO;
        }
            break;

        case WTP_NAME:
        {
            u2ExpectedMinLen = WTP_NAME_MIN_NAME;
            u2ExpectedMaxLen = WTP_NAME_MAX_NAME;
            u1Operation = OPERATION_BETWEEN;
        }
            break;

        case WTP_RADIO_STATISTICS:
        {
            u2ExpectedLen = WTP_RADIO_STATISTICS_MIN_LEN;
            u1Operation = OPERATION_LESS_THAN;
        }
            break;

        case WTP_REBOOT_STATISTICS:
        {
            u2ExpectedLen = WTP_REBOOT_STATISTICS_MIN_LEN;
            u1Operation = OPERATION_LESS_THAN;
        }
            break;

        case WTP_STATIC_IP_ADDR_INFO:
        {
            u2ExpectedLen = WTP_STATIC_IP_ADDR_INFO_MIN_LEN;
            u1Operation = OPERATION_LESS_THAN;
        }
            break;

        case CAPWAP_LOCAL_IPV6_ADDR:
        {
            u2ExpectedLen = CAPWAP_LOCAL_IPV6_ADDR_LEN;
            u1Operation = OPERATION_NOT_EQUALTO;
        }
            break;

        case CAPWAP_TRANSPORT_PROTOCOL:
        {
            u2ExpectedLen = CAPWAP_TRANSPORT_PROTOCOL_LEN;
            u1Operation = OPERATION_NOT_EQUALTO;
        }
            break;

        case ECN_SUPPORT:
        {
            u2ExpectedLen = ECN_SUPPORT_LEN;
            u1Operation = OPERATION_NOT_EQUALTO;
        }
            break;

        case IEEE_ADD_WLAN:
        {
            u2ExpectedLen = MIN_IEEE_ADD_WLAN_LEN;
            u1Operation = OPERATION_LESS_THAN;
        }
            break;

        case IEEE_ANTENNA:
        {
            u2ExpectedLen = MIN_IEEE_ANTENNA_LEN;
            u1Operation = OPERATION_LESS_THAN;
        }
            break;

        case IEEE_ASSIGNED_WTPBSSID:
        {
            u2ExpectedLen = IEEE_ASSIGNED_WTPBSSID_LEN;
            u1Operation = OPERATION_NOT_EQUALTO;
        }
            break;

        case IEEE_DELETE_WLAN:
        {
            u2ExpectedLen = IEEE_DELETE_WLAN_LEN;
            u1Operation = OPERATION_NOT_EQUALTO;
        }
            break;

        case IEEE_DIRECT_SEQUENCE_CONTROL:
        {
            u2ExpectedLen = IEEE_DIRECT_SEQUENCE_CONTROL_LEN;
            u1Operation = OPERATION_NOT_EQUALTO;
        }
            break;

        case IEEE_INFORMATION_ELEMENT:
        {
            u2ExpectedLen = MIN_IEEE_INFORMATION_ELEMENT_LEN;
            u1Operation = OPERATION_LESS_THAN;
        }
            break;

        case IEEE_MAC_OPERATION:
        {
            u2ExpectedLen = IEEE_MAC_OPERATION_LEN;
            u1Operation = OPERATION_LESS_THAN;
        }
            break;

        case IEEE_MIC_COUNTERMEASURES:
        {
            u2ExpectedLen = IEEE_MIC_COUNTERMEASURES_LEN;
            u1Operation = OPERATION_LESS_THAN;
        }
            break;

        case IEEE_MULTIDOMAIN_CAPABILITY:
        {
            u2ExpectedLen = IEEE_MULTIDOMAIN_CAPABILITY_LEN;
            u1Operation = OPERATION_LESS_THAN;
        }
            break;

        case IEEE_OFDM_CONTROL:
        {
            u2ExpectedLen = IEEE_OFDM_CONTROL_LEN;
            u1Operation = OPERATION_LESS_THAN;
        }
            break;

        case IEEE_RATE_SET:
        {
            u2ExpectedLen = MIN_IEEE_RATE_SET_LEN;
            u1Operation = OPERATION_LESS_THAN;
        }
            break;

        case IEEE_RSNA_ERROR_REPORT:
        {
            u2ExpectedLen = IEEE_RSNA_ERROR_REPORT_LEN;
            u1Operation = OPERATION_LESS_THAN;
        }
            break;

        case IEEE_STATION:
        {
            u2ExpectedLen = MIN_IEEE_STATION_LEN;
            u1Operation = OPERATION_LESS_THAN;
        }
            break;

        case IEEE_STATION_QOS_PROFILE:
        {
            u2ExpectedLen = IEEE_STATION_QOS_PROFILE_LEN;
            u1Operation = OPERATION_LESS_THAN;
        }
            break;

        case IEEE_STATION_SESSION_KEY:
        {
            u2ExpectedLen = MIN_IEEE_STATION_SESSION_KEY_LEN;
            u1Operation = OPERATION_LESS_THAN;
        }
            break;

        case IEEE_STATISTICS:
        {
            u2ExpectedLen = IEEE_STATISTICS_LEN;
            u1Operation = OPERATION_LESS_THAN;
        }
            break;

        case IEEE_SUPPORTED_RATES:
        {
            u2ExpectedLen = MIN_IEEE_SUPPORTED_RATES_LEN;
            u1Operation = OPERATION_LESS_THAN;
        }
            break;

        case IEEE_TX_POWER:
        {
            u2ExpectedLen = IEEE_TX_POWER_LEN;
            u1Operation = OPERATION_LESS_THAN;
        }
            break;

        case IEEE_TX_POWERLEVEL:
        {
            u2ExpectedLen = MIN_IEEE_TX_POWERLEVEL_LEN;
            u1Operation = OPERATION_LESS_THAN;
        }
            break;

        case IEEE_UPDATE_STATION_QOS:
        {
            u2ExpectedLen = IEEE_UPDATE_STATION_QOS_LEN;
            u1Operation = OPERATION_LESS_THAN;
        }
            break;

        case IEEE_UPDATE_WLAN:
        {
            u2ExpectedLen = MIN_IEEE_UPDATE_WLAN_LEN;
            u1Operation = OPERATION_LESS_THAN;
        }
            break;

        case IEEE_WTP_QOS:
        {
            u2ExpectedLen = IEEE_WTP_QOS_LEN;
            u1Operation = OPERATION_LESS_THAN;
        }
            break;

        case IEEE_WTP_RADIO_CONFIGURATION:
        {
            u2ExpectedLen = IEEE_WTP_RADIO_CONFIGURATION_LEN;
            u1Operation = OPERATION_LESS_THAN;
        }
            break;

        case IEEE_WTP_RADIO_FAIL_ALARAM:
        {
            u2ExpectedLen = IEEE_WTP_RADIO_FAIL_ALARAM_LEN;
            u1Operation = OPERATION_LESS_THAN;
        }
            break;

        case WTP_RADIO_INFO:
        {
            u2ExpectedLen = IEEE_802_11_RADIO_INFO_LEN;
            u1Operation = OPERATION_NOT_EQUALTO;
        }
            break;

        case VENDOR_SPECIFIC_PAYLOAD:
        {
            /* To do */
        }
            break;

        case IMAGE_IDENTIFIER:
        case IMAGE_INFORMATION:
        case INITIATE_DOWNLOAD:
        case MTU_DISC_PADDING:
        {
            u1Operation = OPERATION_NO_CHECK;
        }
            break;

        default:
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Received the unknown message type \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Received the unknown message type \r\n"));
            return OSIX_FAILURE;
        }
            break;
    }

    /* Perform the Length Check */
    if (u1Operation == OPERATION_NOT_EQUALTO)
    {
        if (u2Len != u2ExpectedLen)
        {
            i4RetVal = OSIX_FAILURE;
        }
    }
    else if (u1Operation == OPERATION_LESS_THAN)
    {
        if (u2Len < u2ExpectedLen)
        {
            i4RetVal = OSIX_FAILURE;
        }
    }
    else if (u1Operation == OPERATION_BETWEEN)
    {
        if ((u2Len > u2ExpectedMaxLen) || (u2Len < u2ExpectedMinLen))
        {
            i4RetVal = OSIX_FAILURE;
        }
    }
    else if (u1Operation == OPERATION_NO_CHECK)
    {
        i4RetVal = OSIX_SUCCESS;
    }

    CAPWAP_FN_EXIT ();

    return i4RetVal;
}

/*****************************************************************************
 *                                                                           *
 * Function     : CapwapValidateMsgElemVal                                   *
 *                                                                           *
 * Description  : Validates the message element value                        *
 *                                                                           *
 * Input        : u2MsgType - Message Type                                   *
 *                u2Len - Message Length                                     *
 *                pMesg - Pointer to packet buffer                           * 
 *                                                                           *
 * Output       : cwHdr   - pointer to capwap header                         *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
INT4
CapwapValidateMsgElemVal (UINT2 u2MsgType, UINT2 u2Len, UINT1 *pMesg)
{
    INT4                i4RetVal = OSIX_SUCCESS;
    UINT1               u1Val = 0;
    UINT2               u2Val = 0;
    UINT4               u4VendorID = 0;
    UINT2               u2DescDataLen = 0;
    UINT4               u4DescElemLen = 0;
    UINT2               u2BoardDataLen = 0;
    UINT4               u4MsgElemLen = 0;
    UINT1               u1MandFields = 0;
    UINT4               u4VendorId = 0;

    CAPWAP_FN_ENTRY ();

    switch (u2MsgType)
    {
        case DISCOVERY_TYPE:
        {

            CAPWAP_GET_1BYTE (u1Val, pMesg);
            /* Validate the one byte discovery type value */
            if (u1Val > AC_REFERRAL_DISC_TYPE)
            {
                CAPWAP_TRC (CAPWAP_MGMT_TRC,
                            " Invalid Discovery type value \n ");
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                              " Invalid Discovery type value \n "));
                return OSIX_FAILURE;
            }
        }
            break;

        case WTP_MAC_TYPE:
        {
            CAPWAP_GET_1BYTE (u1Val, pMesg);
            if (u1Val > LOCAL_SPLIT_MODE)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Invalid WTP Mac type \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Invalid WTP Mac type \r\n"));
                return OSIX_FAILURE;
            }
        }
            break;

        case CAPWAP_TRANSPORT_PROTOCOL:
        {
            /* Get the protocol value and validate  */
            CAPWAP_GET_1BYTE (u1Val, pMesg);
            if ((u1Val <= TRANS_PROT_MIN) || (u1Val >= TRANS_PROT_MAX))
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Invalid transport protocol value \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Invalid transport protocol value \r\n"));
                return OSIX_FAILURE;
            }
        }
            break;

        case WTP_RADIO_INFO:
        {
            CAPWAP_GET_1BYTE (u1Val, pMesg);
            if (u1Val > 31)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Invalid Wtp Radio Type \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Invalid Wtp Radio Type \r\n"));
                return OSIX_FAILURE;
            }
        }
            break;

        case AC_DESCRIPTOR:
        {
            /* Skip and point to the Descriptor type of 
             * Descriptor sub element */
            CAPWAP_SKIP_N_BYTES (AC_DESCRIPTOR_MIN_LEN, pMesg);

            u4DescElemLen = (UINT4) (u2Len - AC_DESCRIPTOR_MIN_LEN);
            while (u4DescElemLen > 0)
            {
                /* Get Vendor Identifier */
                CAPWAP_GET_4BYTE (u4VendorID, pMesg);

                /* Get the Desc type */
                CAPWAP_GET_2BYTE (u2Val, pMesg);

                if ((u2Val == WLC_HW_VERSION) || (u2Val == WLC_SW_VERSION))
                {
                    u1MandFields++;
                }

                /* Get the Descriptor length */
                CAPWAP_GET_2BYTE (u2DescDataLen, pMesg);
                if (u2DescDataLen > MAX_WLC_DESCRIPTOR_DATA_LEN)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "The WLC descriptor value length exceeds "
                                "the max value \r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "The WLC descriptor value length exceeds "
                                  "the max value \r\n"));
                    return OSIX_FAILURE;
                }
                CAPWAP_SKIP_N_BYTES (u2DescDataLen, pMesg);
                u4DescElemLen = (u4DescElemLen - (UINT4) (u2DescDataLen + 8));
            }

            if (u1MandFields != 2)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "The mandatory fields are mising in "
                            "WLC Descriptor \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "The mandatory fields are mising in "
                              "WLC Descriptor \r\n"));
                return MISSING_MANDATORY_MSG_ELEMENT;
            }
        }
            break;

        case WTP_BOARD_DATA:
        {
            u4MsgElemLen = u2Len;

            /* Get the 4 byte Vendor identifier */
            CAPWAP_GET_4BYTE (u4VendorID, pMesg);

            u4MsgElemLen -= 4;
            /* The vendor id should be non-zero. Otherwise return error */
            if (u4VendorID == 0)
            {
                CAPWAP_TRC (CAPWAP_MGMT_TRC, " The Vendor id field is zero\n");
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                              " The Vendor id field is zero\n"));
                return OSIX_FAILURE;
            }

            while (u4MsgElemLen > 0)
            {
                /* Get the Board data type */
                CAPWAP_GET_2BYTE (u2Val, pMesg);
                if ((u2Val == WTP_MODEL_NUMBER) || (u2Val == WTP_SERIAL_NUMBER))
                    u1MandFields++;

                /* Get the board data length */
                CAPWAP_GET_2BYTE (u2BoardDataLen, pMesg);
                CAPWAP_SKIP_N_BYTES (u2BoardDataLen, pMesg);
                u4MsgElemLen =
                    (UINT4) (u4MsgElemLen - (UINT4) (u2BoardDataLen + 4));
            }

            if (u1MandFields != MANDATORY_BOARD_DATA_ELEMENTS)
            {
                /*No of Mandatory board data elements */
                CAPWAP_TRC (CAPWAP_MGMT_TRC,
                            " The mandatory fields are missing in "
                            "WTP board data \n");
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                              " The mandatory fields are missing in "
                              "WTP board data \n"));
                return MISSING_MANDATORY_MSG_ELEMENT;
            }
        }
            break;

        case WTP_DESCRIPTOR:
        {
            /* Skip and point to the Descriptor type of 
             * Descriptor sub element */
            CAPWAP_SKIP_N_BYTES (6, pMesg);
            u4DescElemLen = (UINT4) (u2Len - 6);
            while (u4DescElemLen > 0)
            {
                /* Get Vendor Identifier */
                CAPWAP_GET_4BYTE (u4VendorId, pMesg);

                /* Get the Desc type */
                CAPWAP_GET_2BYTE (u2Val, pMesg);
                if (u2Val <= BOOT_VER)
                {
                    u1MandFields++;
                }

                /* Get the Descriptor length */
                CAPWAP_GET_2BYTE (u2DescDataLen, pMesg);
                if (u2DescDataLen > MAX_WTP_DESCRIPTOR_DATA_LEN)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "The WTP descriptor value length exceeds"
                                " the max value\r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "The WTP descriptor value length exceeds"
                                  " the max value\r\n"));
                    return OSIX_FAILURE;
                }
                CAPWAP_SKIP_N_BYTES (u2DescDataLen, pMesg);
                u4DescElemLen =
                    (UINT4) (u4DescElemLen - (UINT4) (u2DescDataLen + 8));
            }
            if (u1MandFields != 3)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "The mandatory fields are mising in "
                            "WTP Descriptor \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "The mandatory fields are mising in "
                              "WTP Descriptor \r\n"));
                return MISSING_MANDATORY_MSG_ELEMENT;
            }
        }
            break;

        default:
        {
            i4RetVal = OSIX_SUCCESS;
        }
            break;
    }

    CAPWAP_FN_EXIT ();

    return i4RetVal;
}

INT4
CapwapParseVendorSpecificPayload (UINT1 *pMesg,
                                  UINT2 pktOffset,
                                  tCapwapMsgElement * capwapMsg,
                                  UINT2 *u2LenParsed)
{
    UINT2               u2Len = 0;
    UINT2               u2Type = 0;
    UINT2               u2MsgElemLen;
    UINT4               u4VendorId = 0;

    CAPWAP_FN_ENTRY ();
    /* Get the length field */
    CAPWAP_GET_2BYTE (u2Len, pMesg);

    /* The vendor specific data should not exceed 2048.
     * the check is done including Vendor id-4B, Element id - 2 bytes*/

    /* The below change is a work around. Vendor message should not exceed 2048 
     * bytes. WTP event request needs to be splitted into multiple request to 
     * handle this scenario. Once done this code needs to be reverted */
#ifdef DEBUG_WANTED
    /* The vendor specific data should not exceed 2048.
     * the check is done including Vendor id-4B, Element id - 2 bytes*/
    if ((u2Len > MAX_VENDOR_SPECIFIC_DATA_LEN)
        || (u2Len < MIN_VENDOR_SPECIFIC_DATA_LEN))
        if ((u2Len > 2500) || (u2Len < MIN_VENDOR_SPECIFIC_DATA_LEN))
        {
            CAPWAP_TRC (CAPWAP_MGMT_TRC,
                        "Invalid Vendor specific Payload length field \n");
            SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                          "Invalid Vendor specific Payload length field \n"));
            return (CAPWAP_FAILURE);
        }
#endif
    capwapMsg->pu2Offset = (UINT2 *) MemAllocMemBlk (CAPWAP_BULKOFFSET_POOLID);
    if (capwapMsg->pu2Offset == NULL)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Memory allocation Failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Memory allocation Failed \r\n"));
        return OSIX_FAILURE;
    }

    /* Fill the capwap message structure with TLO format */
    CAPWAP_GET_2BYTE (u2Type, pMesg);
    if (u2Type < VENDOR_SPECIFIC_MSG)
    {
        /* Call third party func */
    }
    u2Type = (UINT2) (u2Type - VENDOR_SPECIFIC_MSG);
    switch (u2Type)
    {
        case VENDOR_DISC_TYPE:
        {
            CAPWAP_GET_2BYTE (u2MsgElemLen, pMesg);
            if (u2MsgElemLen < 5)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Invalid Discovery Vendor "
                            "specific Payload length field \n");
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                              "Invalid Discovery Vendor "
                              "specific Payload length field \n"));
                return (CAPWAP_FAILURE);
            }
            CAPWAP_SKIP_N_BYTES (u2Len, pMesg);
            capwapMsg->pu2Offset[0] = pktOffset;
            capwapMsg->numMsgElemInstance = CAPWAP_DEFAULT_MESG_ELEM_INSTANCE;
            break;
        }
        case VENDOR_UDP_SERVER_PORT_MSG:
        {
            CAPWAP_GET_2BYTE (u2MsgElemLen, pMesg);
            if (u2MsgElemLen < 8)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Invalid UDP Server port "
                            "Vendor specific Payload length field \n");
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                              "Invalid UDP Server port "
                              "Vendor specific Payload length field \n"));
                return (CAPWAP_FAILURE);
            }
            CAPWAP_SKIP_N_BYTES (u2Len, pMesg);
            capwapMsg->pu2Offset[0] = pktOffset;
            capwapMsg->numMsgElemInstance = CAPWAP_DEFAULT_MESG_ELEM_INSTANCE;
            break;
        }
        case VENDOR_CONTROL_POLICY_DTLS_MSG:
        {
            CAPWAP_GET_2BYTE (u2MsgElemLen, pMesg);
            if (u2MsgElemLen < 1)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Invalid CONTROL DTLS "
                            "Vendor specific Payload length field \n");
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                              "Invalid CONTROL DTLS "
                              "Vendor specific Payload length field \n"));
                return (CAPWAP_FAILURE);
            }
            CAPWAP_SKIP_N_BYTES (u2Len, pMesg);
            capwapMsg->pu2Offset[0] = pktOffset;
            capwapMsg->numMsgElemInstance = CAPWAP_DEFAULT_MESG_ELEM_INSTANCE;
            break;
        }
        case VENDOR_DOT11N_TYPE:
        {
            CAPWAP_GET_2BYTE (u2MsgElemLen, pMesg);
            if (u2MsgElemLen < 5)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Invalid DOT11N "
                            "Vendor specific Payload length field \n");
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                              "Invalid DOT11N "
                              "Vendor specific Payload length field \n"));
                return (CAPWAP_FAILURE);
            }
            CAPWAP_SKIP_N_BYTES (u2Len, pMesg);
            capwapMsg->pu2Offset[0] = pktOffset;
            capwapMsg->numMsgElemInstance = CAPWAP_DEFAULT_MESG_ELEM_INSTANCE;
            break;
        }
        case VENDOR_SSID_ISOLATION_MSG:
        {
            CAPWAP_GET_2BYTE (u2MsgElemLen, pMesg);
            if (u2MsgElemLen < 5)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Invalid SSID ISOLATION "
                            "Vendor specific Payload length field \n");
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                              "Invalid SSID ISOLATION "
                              "Vendor specific Payload length field \n"));
                return (CAPWAP_FAILURE);
            }
            capwapMsg->pu2Offset[0] = pktOffset;
            CAPWAP_SKIP_N_BYTES (u2Len, pMesg);
            break;
        }
        case VENDOR_SSID_RATE_MSG:
        {
            CAPWAP_GET_2BYTE (u2MsgElemLen, pMesg);
            if (u2MsgElemLen < 39)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Invalid UDP SSID RATE "
                            "Vendor specific Payload length field \n");
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                              "Invalid UDP SSID RATE "
                              "Vendor specific Payload length field \n"));
                return (CAPWAP_FAILURE);
            }
            capwapMsg->pu2Offset[0] = pktOffset;
            CAPWAP_SKIP_N_BYTES (u2Len, pMesg);
            break;
        }
        case VENDOR_MULTICAST_MSG:
        {
            CAPWAP_GET_2BYTE (u2MsgElemLen, pMesg);
            if (u2MsgElemLen < 15)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Invalid UDP MULTICAST "
                            "Vendor specific Payload length field \n");
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                              "Invalid UDP MULTICAST "
                              "Vendor specific Payload length field \n"));
                return (CAPWAP_FAILURE);
            }
            capwapMsg->pu2Offset[0] = pktOffset;
            CAPWAP_SKIP_N_BYTES (u2Len, pMesg);
            capwapMsg->numMsgElemInstance = CAPWAP_DEFAULT_MESG_ELEM_INSTANCE;
            break;
        }
        case VENDOR_DOMAIN_NAME_TYPE:
        {
            CAPWAP_GET_2BYTE (u2MsgElemLen, pMesg);
            if (u2MsgElemLen < 4)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Invalid Domain Name "
                            "Vendor specific Payload length field \n");
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                              "Invalid Domain Name "
                              "Vendor specific Payload length field \n"));
                return (CAPWAP_FAILURE);
            }
            CAPWAP_SKIP_N_BYTES (u2Len, pMesg);
            capwapMsg->pu2Offset[0] = pktOffset;
            capwapMsg->numMsgElemInstance = CAPWAP_DEFAULT_MESG_ELEM_INSTANCE;
            break;
        }
        case VENDOR_NATIVE_VLAN:
        {
            CAPWAP_GET_2BYTE (u2MsgElemLen, pMesg);
            if (u2MsgElemLen < 4)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Invalid Native Vlan "
                            "Vendor specific Payload length field \n");
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                              "Invalid Native Vlan "
                              "Vendor specific Payload length field \n"));
                return (CAPWAP_FAILURE);
            }
            CAPWAP_SKIP_N_BYTES (u2Len, pMesg);
            capwapMsg->pu2Offset[0] = pktOffset;
            capwapMsg->numMsgElemInstance = CAPWAP_DEFAULT_MESG_ELEM_INSTANCE;
            break;
        }
        case VENDOR_LOCAL_ROUTING_TYPE:
        {
            CAPWAP_GET_2BYTE (u2MsgElemLen, pMesg);
            if (u2MsgElemLen < 5)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Invalid Ap Local Routing "
                            "Vendor specific Payload length field \n");
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                              "Invalid Ap Local Routing "
                              "Vendor specific Payload length field \n"));
                return (CAPWAP_FAILURE);
            }
            CAPWAP_SKIP_N_BYTES (u2Len, pMesg);
            capwapMsg->pu2Offset[0] = pktOffset;
            capwapMsg->numMsgElemInstance = CAPWAP_DEFAULT_MESG_ELEM_INSTANCE;
            break;
        }
        case VENDOR_MGMT_SSID_TYPE:
        {
            CAPWAP_GET_2BYTE (u2MsgElemLen, pMesg);
            if (u2MsgElemLen < 6)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Invalid Management SSID "
                            "Vendor specific Payload length field \n");
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                              "Invalid Management SSID "
                              "Vendor specific Payload length field \n"));
                return (CAPWAP_FAILURE);
            }
            CAPWAP_SKIP_N_BYTES (u2Len, pMesg);
            capwapMsg->pu2Offset[0] = pktOffset;
            capwapMsg->numMsgElemInstance = CAPWAP_DEFAULT_MESG_ELEM_INSTANCE;
            break;
        }
        case VENDOR_BSSID_STATS_TYPE:
        {
            CAPWAP_GET_2BYTE (u2MsgElemLen, pMesg);
            CAPWAP_GET_2BYTE (u2MsgElemLen, pMesg);
            /* As we have already skipped vendor id and element id */
            CAPWAP_SKIP_N_BYTES (u2Len - 6, pMesg);
            capwapMsg->pu2Offset[0] = pktOffset;
            capwapMsg->numMsgElemInstance = CAPWAP_DEFAULT_MESG_ELEM_INSTANCE;
            break;
        }
#ifdef RFMGMT_WANTED
        case VENDOR_NEIGH_CONFIG_TYPE:
        {
            CAPWAP_GET_2BYTE (u2MsgElemLen, pMesg);
            if (u2MsgElemLen < 14)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Invalid Neigh config Vendor "
                            "specific Payload length field \n");
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                              "Invalid Neigh config Vendor "
                              "specific Payload length field \n"));
                return (CAPWAP_FAILURE);
            }
            CAPWAP_SKIP_N_BYTES (u2Len, pMesg);
            capwapMsg->pu2Offset[0] = pktOffset;
            capwapMsg->numMsgElemInstance = CAPWAP_DEFAULT_MESG_ELEM_INSTANCE;
            break;
        }
        case VENDOR_CLIENT_CONFIG_TYPE:
        {
            CAPWAP_GET_2BYTE (u2MsgElemLen, pMesg);
            if (u2MsgElemLen < 12)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Invalid client config Vendor "
                            "specific Payload length field \n");
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                              "Invalid client config Vendor "
                              "specific Payload length field \n"));
                return (CAPWAP_FAILURE);
            }
            CAPWAP_SKIP_N_BYTES (u2Len, pMesg);
            capwapMsg->pu2Offset[0] = pktOffset;
            capwapMsg->numMsgElemInstance = CAPWAP_DEFAULT_MESG_ELEM_INSTANCE;
            break;
        }
        case VENDOR_CH_SWITCH_STATUS_TYPE:
        {
            CAPWAP_GET_2BYTE (u2MsgElemLen, pMesg);
            if (u2MsgElemLen < 6)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Invalid ch switch status Vendor "
                            "specific Payload length field \n");
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                              "Invalid ch switch status Vendor "
                              "specific Payload length field \n"));
                return (CAPWAP_FAILURE);
            }
            CAPWAP_SKIP_N_BYTES (u2Len, pMesg);
            capwapMsg->pu2Offset[0] = pktOffset;
            capwapMsg->numMsgElemInstance = CAPWAP_DEFAULT_MESG_ELEM_INSTANCE;
            break;
        }
        case VENDOR_SPECT_MGMT_TPC_TYPE:
        {
            CAPWAP_GET_2BYTE (u2MsgElemLen, pMesg);
            if (u2MsgElemLen < 7)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Invalid vendor spectrum"
                            "management Payload length field \n");
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                              "Invalid vendor spectrum"
                              "management Payload length field \n"));
                return (CAPWAP_FAILURE);
            }
            CAPWAP_SKIP_N_BYTES (u2Len, pMesg);
            capwapMsg->pu2Offset[0] = pktOffset;
            capwapMsg->numMsgElemInstance = CAPWAP_DEFAULT_MESG_ELEM_INSTANCE;
            break;
        }
        case VENDOR_SPECTRUM_MGMT_DFS_TYPE:
        {
            CAPWAP_GET_2BYTE (u2MsgElemLen, pMesg);
            if (u2MsgElemLen < 14)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Invalid vendor spectrum"
                            "management Payload length field \n");
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                              "Invalid vendor spectrum"
                              "management Payload length field \n"));
                return (CAPWAP_FAILURE);
            }
            CAPWAP_SKIP_N_BYTES (u2Len, pMesg);
            capwapMsg->pu2Offset[0] = pktOffset;
            capwapMsg->numMsgElemInstance = CAPWAP_DEFAULT_MESG_ELEM_INSTANCE;
            break;
        }

        case VENDOR_NEIGHBOR_AP_TYPE:
        {
            CAPWAP_GET_2BYTE (u2MsgElemLen, pMesg);
            CAPWAP_GET_2BYTE (u2MsgElemLen, pMesg);
            /* As we have already skipped vendor id and element id */
            CAPWAP_SKIP_N_BYTES (u2Len - 6, pMesg);
            capwapMsg->pu2Offset[0] = pktOffset;
            capwapMsg->numMsgElemInstance = CAPWAP_DEFAULT_MESG_ELEM_INSTANCE;
            break;
        }
        case VENDOR_CLIENT_SCAN_TYPE:
            CAPWAP_GET_2BYTE (u2MsgElemLen, pMesg);
            CAPWAP_GET_2BYTE (u2MsgElemLen, pMesg);
            /* As we have already skipped vendor id and element id */
            CAPWAP_SKIP_N_BYTES (u2Len - 6, pMesg);
            capwapMsg->pu2Offset[0] = pktOffset;
            capwapMsg->numMsgElemInstance = CAPWAP_DEFAULT_MESG_ELEM_INSTANCE;
            break;

        case VENDOR_CH_SCANNED_LIST:
            CAPWAP_GET_2BYTE (u2MsgElemLen, pMesg);
            CAPWAP_GET_2BYTE (u2MsgElemLen, pMesg);
            /* As we have already skipped vendor id and element id */
            CAPWAP_SKIP_N_BYTES (u2Len - 6, pMesg);
            capwapMsg->pu2Offset[0] = pktOffset;
            capwapMsg->numMsgElemInstance = CAPWAP_DEFAULT_MESG_ELEM_INSTANCE;
            break;

#endif
        case VENDOR_DOT11N_STA_TYPE:
        {
            CAPWAP_GET_2BYTE (u2MsgElemLen, pMesg);
            if (u2MsgElemLen < DOT11N_STA_VENDOR_MSG_LEN)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Invalid Station DOT11N "
                            "Vendor specific Payload length field \n");
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                              "Invalid Station DOT11N "
                              "Vendor specific Payload length field \n"));
                return (CAPWAP_FAILURE);
            }
            CAPWAP_SKIP_N_BYTES (u2Len, pMesg);
            capwapMsg->pu2Offset[0] = pktOffset;
            capwapMsg->numMsgElemInstance = CAPWAP_DEFAULT_MESG_ELEM_INSTANCE;
            break;
        }

        case VENDOR_WEBAUTH_COMPLETE_MSG:
            CAPWAP_GET_2BYTE (u2MsgElemLen, pMesg);
            CAPWAP_GET_2BYTE (u2MsgElemLen, pMesg);
            /* As we have already skipped vendor id and element id */
            CAPWAP_SKIP_N_BYTES (u2Len - 6, pMesg);
            capwapMsg->pu2Offset[0] = pktOffset;
            capwapMsg->numMsgElemInstance = CAPWAP_DEFAULT_MESG_ELEM_INSTANCE;
            break;

        case VENDOR_USERROLE_TYPE:
            CAPWAP_GET_2BYTE (u2MsgElemLen, pMesg);
            CAPWAP_GET_4BYTE (u4VendorId, pMesg);
            CAPWAP_SKIP_N_BYTES (u2Len, pMesg);
            capwapMsg->pu2Offset[0] = pktOffset;
            capwapMsg->numMsgElemInstance = CAPWAP_DEFAULT_MESG_ELEM_INSTANCE;
            break;
        case VENDOR_WMM_TYPE:
            CAPWAP_GET_2BYTE (u2MsgElemLen, pMesg);
            CAPWAP_GET_4BYTE (u4VendorId, pMesg);
            CAPWAP_SKIP_N_BYTES (u2Len, pMesg);
            capwapMsg->pu2Offset[0] = pktOffset;
            capwapMsg->numMsgElemInstance = CAPWAP_DEFAULT_MESG_ELEM_INSTANCE;
            break;

        case VENDOR_MULTIDOMAIN_INFO_TYPE:
            CAPWAP_GET_2BYTE (u2MsgElemLen, pMesg);
            CAPWAP_GET_4BYTE (u4VendorId, pMesg);
            CAPWAP_SKIP_N_BYTES (u2Len, pMesg);
            capwapMsg->pu2Offset[0] = pktOffset;
            capwapMsg->numMsgElemInstance = CAPWAP_DEFAULT_MESG_ELEM_INSTANCE;
            break;
        case VENDOR_DFS_CHANNEL_STATS:
            CAPWAP_GET_2BYTE (u2MsgElemLen, pMesg);
            CAPWAP_GET_4BYTE (u4VendorId, pMesg);
            CAPWAP_SKIP_N_BYTES (u2Len, pMesg);
            capwapMsg->pu2Offset[0] = pktOffset;
            capwapMsg->numMsgElemInstance = CAPWAP_DEFAULT_MESG_ELEM_INSTANCE;
            break;

        case VENDOR_DFS_RADAR_STATS:
            CAPWAP_GET_2BYTE (u2MsgElemLen, pMesg);
            CAPWAP_GET_4BYTE (u4VendorId, pMesg);
            CAPWAP_SKIP_N_BYTES (u2Len, pMesg);
            capwapMsg->pu2Offset[0] = pktOffset;
            capwapMsg->numMsgElemInstance = CAPWAP_DEFAULT_MESG_ELEM_INSTANCE;
            break;

#ifdef RFMGMT_WANTED
#ifdef ROGUEAP_WANTED
        case VENDOR_ROUGE_AP:
            CAPWAP_GET_2BYTE (u2MsgElemLen, pMesg);
            CAPWAP_GET_4BYTE (u4VendorId, pMesg);
            CAPWAP_SKIP_N_BYTES (u2Len, pMesg);
            capwapMsg->pu2Offset[0] = pktOffset;
            capwapMsg->numMsgElemInstance = CAPWAP_DEFAULT_MESG_ELEM_INSTANCE;
            break;
#endif
#endif

#ifdef BAND_SELECT_WANTED
        case VENDOR_BANDSELECT_TYPE:
            CAPWAP_GET_2BYTE (u2MsgElemLen, pMesg);
            CAPWAP_GET_4BYTE (u4VendorId, pMesg);
            CAPWAP_SKIP_N_BYTES (u2Len, pMesg);
            capwapMsg->pu2Offset[0] = pktOffset;
            capwapMsg->numMsgElemInstance = CAPWAP_DEFAULT_MESG_ELEM_INSTANCE;
            break;
#endif
        case VENDOR_WLAN_INTERFACE_IP:
            CAPWAP_GET_2BYTE (u2MsgElemLen, pMesg);
            CAPWAP_SKIP_N_BYTES (u2Len, pMesg);
            capwapMsg->pu2Offset[0] = pktOffset;
            capwapMsg->numMsgElemInstance = CAPWAP_DEFAULT_MESG_ELEM_INSTANCE;
            break;
        case VENDOR_DHCP_POOL:
            CAPWAP_GET_2BYTE (u2MsgElemLen, pMesg);
            CAPWAP_SKIP_N_BYTES (u2Len, pMesg);
            capwapMsg->pu2Offset[0] = pktOffset;
            capwapMsg->numMsgElemInstance = CAPWAP_DEFAULT_MESG_ELEM_INSTANCE;
            break;
        case VENDOR_DHCP_RELAY:
            CAPWAP_GET_2BYTE (u2MsgElemLen, pMesg);
            CAPWAP_SKIP_N_BYTES (u2Len, pMesg);
            capwapMsg->pu2Offset[0] = pktOffset;
            capwapMsg->numMsgElemInstance = CAPWAP_DEFAULT_MESG_ELEM_INSTANCE;
            break;
        case VENDOR_FIREWALL_STATUS:
            CAPWAP_GET_2BYTE (u2MsgElemLen, pMesg);
            CAPWAP_SKIP_N_BYTES (u2Len, pMesg);
            capwapMsg->pu2Offset[0] = pktOffset;
            capwapMsg->numMsgElemInstance = CAPWAP_DEFAULT_MESG_ELEM_INSTANCE;
            break;
        case VENDOR_NAT_STATUS:
            CAPWAP_GET_2BYTE (u2MsgElemLen, pMesg);
            CAPWAP_SKIP_N_BYTES (u2Len, pMesg);
            capwapMsg->pu2Offset[0] = pktOffset;
            capwapMsg->numMsgElemInstance = CAPWAP_DEFAULT_MESG_ELEM_INSTANCE;
            break;
        case VENDOR_ROUTE_TABLE:
            CAPWAP_GET_2BYTE (u2MsgElemLen, pMesg);
            CAPWAP_SKIP_N_BYTES (u2Len, pMesg);
            capwapMsg->pu2Offset[0] = pktOffset;
            capwapMsg->numMsgElemInstance = CAPWAP_DEFAULT_MESG_ELEM_INSTANCE;
            break;
        case VENDOR_FIREWALL_FILTER_TABLE:
            CAPWAP_GET_2BYTE (u2MsgElemLen, pMesg);
            CAPWAP_SKIP_N_BYTES (u2Len, pMesg);
            capwapMsg->pu2Offset[0] = pktOffset;
            capwapMsg->numMsgElemInstance = CAPWAP_DEFAULT_MESG_ELEM_INSTANCE;
            break;
        case VENDOR_FIREWALL_ACL_TABLE:
            CAPWAP_GET_2BYTE (u2MsgElemLen, pMesg);
            CAPWAP_SKIP_N_BYTES (u2Len, pMesg);
            capwapMsg->pu2Offset[0] = pktOffset;
            capwapMsg->numMsgElemInstance = CAPWAP_DEFAULT_MESG_ELEM_INSTANCE;
            break;

        case VENDOR_DIFF_SERV_TYPE:
            CAPWAP_GET_2BYTE (u2MsgElemLen, pMesg);
            CAPWAP_SKIP_N_BYTES (u2Len, pMesg);
            capwapMsg->pu2Offset[0] = pktOffset;
            capwapMsg->numMsgElemInstance = CAPWAP_DEFAULT_MESG_ELEM_INSTANCE;
            break;
        case VENDOR_CAPW_DIFF_SERV_TYPE:
            CAPWAP_GET_2BYTE (u2MsgElemLen, pMesg);
            CAPWAP_SKIP_N_BYTES (u2Len, pMesg);
            capwapMsg->pu2Offset[0] = pktOffset;
            capwapMsg->numMsgElemInstance = CAPWAP_DEFAULT_MESG_ELEM_INSTANCE;
            break;

#ifdef PMF_DEBUG_WANTED
        case VENDOR_PMF_TYPE:
            CAPWAP_GET_2BYTE (u2MsgElemLen, pMesg);
            CAPWAP_GET_4BYTE (u4VendorId, pMesg);
            CAPWAP_SKIP_N_BYTES (u2Len, pMesg);
            capwapMsg->pu2Offset[0] = pktOffset;
            capwapMsg->numMsgElemInstance = CAPWAP_DEFAULT_MESG_ELEM_INSTANCE;
            break;
#endif
        case VENDOR_L3SUBIF_TABLE:
            CAPWAP_GET_2BYTE (u2MsgElemLen, pMesg);
            CAPWAP_SKIP_N_BYTES (u2Len, pMesg);
            capwapMsg->pu2Offset[0] = pktOffset;
            capwapMsg->numMsgElemInstance = CAPWAP_DEFAULT_MESG_ELEM_INSTANCE;
            break;

        case VENDOR_DOT11N_CONFIG_TYPE:
            CAPWAP_GET_2BYTE (u2MsgElemLen, pMesg);
            CAPWAP_SKIP_N_BYTES (u2Len, pMesg);
            capwapMsg->pu2Offset[0] = pktOffset;
            capwapMsg->numMsgElemInstance = CAPWAP_DEFAULT_MESG_ELEM_INSTANCE;
            break;
        case VENDOR_WLAN_VLAN_TYPE:
            CAPWAP_GET_2BYTE (u2MsgElemLen, pMesg);
            CAPWAP_GET_4BYTE (u4VendorId, pMesg);
            CAPWAP_SKIP_N_BYTES (u2Len, pMesg);
            capwapMsg->pu2Offset[0] = pktOffset;
            capwapMsg->numMsgElemInstance = CAPWAP_DEFAULT_MESG_ELEM_INSTANCE;
            break;
        case VENDOR_WPA_TYPE:
            CAPWAP_GET_2BYTE (u2MsgElemLen, pMesg);
            CAPWAP_GET_4BYTE (u4VendorId, pMesg);
            CAPWAP_SKIP_N_BYTES (u2Len, pMesg);
            capwapMsg->pu2Offset[0] = pktOffset;
            capwapMsg->numMsgElemInstance = CAPWAP_DEFAULT_MESG_ELEM_INSTANCE;
            break;

        default:
            break;
    }

    capwapMsg->u2MsgType = VENDOR_SPECIFIC_PAYLOAD;
    /*capwapMsg->u2Length = u2Len;
       capwapMsg->pu2Offset[0] = pktOffset; */
    *u2LenParsed = (UINT2) (u2Len + CAPWAP_MSG_ELEM_TYPE_LEN);

    CAPWAP_FN_EXIT ();
    return CAPWAP_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : CapwapParseStructureMemrelease                             *
 *                                                                           *
 * Description  : Parse the Structure memory release                         *
 *                                                                           *
 * Input        : pMesg   - Pointer to the packet buffer                     *
 *                u2PktOffset - Packet offset from where to start the parsing*
 *                                                                           *
 * Output       : capwapCtrlMsg - the message elements of capwap control Msg *
 *                pu2LenParsed  - Number of Bytes parsed
 *                                                                           *
 * Returns      : CAPWAP_SUCCESS / CAPWAP_FAILURE                            *
 *                                                                           *
 *****************************************************************************/
INT4
CapwapParseStructureMemrelease (tCapwapControlPacket * pCapwapCtrlMsg)
{
    /* Free the allocated memory to storet the offset */
    UINT2               u2Index = 0;
    UINT2               u2MsgType = 0;
    UINT2               u2MsgElems = 0;

    if (pCapwapCtrlMsg != NULL)
    {
        CAPWAP_FN_ENTRY ();
        u2MsgElems =
            (UINT2) (pCapwapCtrlMsg->numOptionalElements +
                     pCapwapCtrlMsg->numMandatoryElements);
        for (u2Index = 0; u2Index < u2MsgElems; u2Index++)
        {
            u2MsgType = pCapwapCtrlMsg->capwapMsgElm[u2Index].u2MsgType;
            switch (u2MsgType)
            {
                case ADD_STATION:
                case DELETE_STATION:
                case RADIO_OPER_STATE:
                case DECRYPTION_ERROR_REPORT:
                case DUPLICATE_IPV4_ADDR:
                case DUPLICATE_IPV6_ADDR:
                case WTP_RADIO_STATISTICS:
                case WTP_REBOOT_STATISTICS:
                case RADIO_ADMIN_STATE:
                case IEEE_ANTENNA:
                case IEEE_DIRECT_SEQUENCE_CONTROL:
                case IEEE_INFORMATION_ELEMENT:
                case IEEE_MAC_OPERATION:
                case IEEE_MIC_COUNTERMEASURES:
                case IEEE_MULTIDOMAIN_CAPABILITY:
                case IEEE_OFDM_CONTROL:
                case IEEE_RATE_SET:
                case IEEE_RSNA_ERROR_REPORT:
                case IEEE_STATION:
                case IEEE_STATION_SESSION_KEY:
                case IEEE_STATION_QOS_PROFILE:
                case IEEE_STATISTICS:
                case IEEE_SUPPORTED_RATES:
                case IEEE_TX_POWER:
                case IEEE_TX_POWERLEVEL:
                case IEEE_UPDATE_STATION_QOS:
                case IEEE_WTP_QOS:
                case IEEE_WTP_RADIO_CONFIGURATION:
                case WTP_RADIO_INFO:
                case VENDOR_SPECIFIC_PAYLOAD:
                    if ((UINT1 *) pCapwapCtrlMsg->capwapMsgElm[u2Index].
                        pu2Offset != NULL)
                    {
                        MemReleaseMemBlock (CAPWAP_BULKOFFSET_POOLID,
                                            (UINT1 *) pCapwapCtrlMsg->
                                            capwapMsgElm[u2Index].pu2Offset);
                    }
                    /* u2MsgElems += 32; */
                    break;
                default:
                    if ((UINT1 *) pCapwapCtrlMsg->capwapMsgElm[u2Index].
                        pu2Offset != NULL)
                    {
                        MemReleaseMemBlock (CAPWAP_OFFSET_POOLID,
                                            (UINT1 *) pCapwapCtrlMsg->
                                            capwapMsgElm[u2Index].pu2Offset);
                    }
                    break;
            }
        }
    }
    CAPWAP_FN_EXIT ();
    return CAPWAP_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : DumpParsedData                                             *
 *                                                                           *
 * Description  : Parse the Dump Data Message Element                        *
 *                                                                           *
 * Input        : pMesg   - Pointer to the packet buffer                     *
 *                u2PktOffset - Packet offset from where to start the parsing*
 *                                                                           *
 * Output       : capwapCtrlMsg - the message elements of capwap control Msg *
 *                pu2LenParsed  - Number of Bytes parsed
 *                                                                           *
 * Returns      : CAPWAP_SUCCESS / CAPWAP_FAILURE                            *
 *                                                                           *
 *****************************************************************************/
INT4
DumpParsedData (tCapwapControlPacket * pCapwapCtrlMsg)
{
    UINT1               u1Index = 0;

    if (pCapwapCtrlMsg->numMandatoryElements != 0
        || pCapwapCtrlMsg->numOptionalElements != 0)
    {

        CAPWAP_TRC1 (CAPWAP_PKTDUMP_TRC, "Mandatory Message elements  %d \r\n",
                     pCapwapCtrlMsg->numMandatoryElements);
        SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4CapwapSysLogId,
                      "Mandatory Message elements  %d \r\n",
                      pCapwapCtrlMsg->numMandatoryElements));
        CAPWAP_TRC1 (CAPWAP_PKTDUMP_TRC, "Optional Message elements   %d \r\n",
                     pCapwapCtrlMsg->numOptionalElements);
        SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4CapwapSysLogId,
                      "Optional Message elements   %d \r\n",
                      pCapwapCtrlMsg->numOptionalElements));
        CAPWAP_TRC1 (CAPWAP_PKTDUMP_TRC, "Message Type - %d\r\n",
                     pCapwapCtrlMsg->capwapCtrlHdr.u4MsgType);
        SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4CapwapSysLogId,
                      "Message Type - %d\r\n",
                      pCapwapCtrlMsg->capwapCtrlHdr.u4MsgType));
        CAPWAP_TRC (CAPWAP_PKTDUMP_TRC,
                    "--------------------------------------------\r\n");
        SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4CapwapSysLogId,
                      "--------------------------------------------\r\n"));
        CAPWAP_TRC (CAPWAP_PKTDUMP_TRC,
                    "Sl No | Type | Length | Offset  |NoOfElems |\r\n");
        SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4CapwapSysLogId,
                      "Sl No | Type | Length | Offset  |NoOfElems |\r\n"));
        CAPWAP_TRC (CAPWAP_PKTDUMP_TRC,
                    "-------------------------------------------|\r\n");
        SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4CapwapSysLogId,
                      "-------------------------------------------|\r\n"));

        for (u1Index = 0;
             u1Index <
             (pCapwapCtrlMsg->numMandatoryElements +
              pCapwapCtrlMsg->numOptionalElements); u1Index++)
        {

            CAPWAP_TRC5 (CAPWAP_PKTDUMP_TRC,
                         " %4d | %4d | %4d   | %4d    | %4d     |\r\n", u1Index,
                         pCapwapCtrlMsg->capwapMsgElm[u1Index].u2MsgType,
                         pCapwapCtrlMsg->capwapMsgElm[u1Index].u2Length,
                         pCapwapCtrlMsg->capwapMsgElm[u1Index].pu2Offset[0],
                         pCapwapCtrlMsg->capwapMsgElm[u1Index].
                         numMsgElemInstance);
            SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4CapwapSysLogId,
                          " %4d | %4d | %4d   | %4d    | %4d     |\r\n",
                          u1Index,
                          pCapwapCtrlMsg->capwapMsgElm[u1Index].u2MsgType,
                          pCapwapCtrlMsg->capwapMsgElm[u1Index].u2Length,
                          pCapwapCtrlMsg->capwapMsgElm[u1Index].pu2Offset[0],
                          pCapwapCtrlMsg->capwapMsgElm[u1Index].
                          numMsgElemInstance));

        }
    }
    return OSIX_SUCCESS;
}

VOID
CapwapReleaseMemBlock (UINT2 u2PacketLen, UINT1 *pu1Buf, UINT1 u1IsNotLinear)
{
    CAPWAP_FN_ENTRY ();
    /* Release the respective MemBlocks if allocated */
    if (u2PacketLen <= CAPWAP_MAX_PKT_LEN)
    {
        if (u1IsNotLinear == OSIX_TRUE)
        {
            MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pu1Buf);
        }
    }
    else
    {
        if (u1IsNotLinear == OSIX_TRUE)
        {
            MemReleaseMemBlock (CAPWAP_DATA_TRANSFER_POOLID, pu1Buf);
        }
    }
    CAPWAP_FN_EXIT ();
}

#endif
