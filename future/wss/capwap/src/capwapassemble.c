/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: capwapassemble.c,v 1.6 2017/12/08 10:16:24 siva Exp $       
 * DESCRIPTION    : Contains Capwap Packet assembling functions
 ********************************************************************/
#ifndef _CAPWAP_ASSEMBLE_C
#define _CAPWAP_ASSEMBLE_C

#ifdef WLC_WANTED
#include "wsswlanwlcproto.h"
#endif
#ifdef WTP_WANTED
#include "wsswlanwtpproto.h"
#include "wssifstatdfs.h"
#include "wssifstawtpprot.h"
#endif
#ifdef NPAPI_WANTED
#include "wssifstawtpprot.h"
#include "nputil.h"
#endif
#include "capwapinc.h"
#include "aphdlr.h"
#ifdef RFMGMT_WANTED
#include "rfminc.h"
#endif
UINT1               gu1MultiDomainSent;
PUBLIC UINT1        gau1IfName[MAX_IFNAME_SIZE];
PUBLIC tWssStaWtpDBWalk gaWssStaWtpDBWalk[MAX_STA_SUPP_PER_AP];
PUBLIC UINT1        gu1WtpClientWalkIndex;

#ifdef WTP_WANTED
PRIVATE UINT1       CapwapUtilSetConflictList (UINT1 u1Channel,
                                               UINT1 u1Counter,
                                               UINT1 *pu1ConflictList);
#endif
/*****************************************************************************
 *                                                                           *
 * Function     : AssembleCapwapHeader                                       *
 *                                                                           *
 * Description  : Assemble the Capwap Header                                 *
 *                                                                           *
 * Input        : tCapwapHdr Capwap Header Information                       *
 *                                                                           *
 * Output       : Packet Buffer with the CAPWAP Header                       *
 *                                                                           *
 * Returns      : None                                                       *
 *****************************************************************************/
VOID
AssembleCapwapHeader (UINT1 *pTxPkt, tCapwapHdr * pCapwapHdr)
{
    UINT4               h_sz = 0;

    CAPWAP_PUT_1BYTE (pTxPkt, pCapwapHdr->u1Preamble);
    CAPWAP_PUT_2BYTE (pTxPkt, pCapwapHdr->u2HlenRidWbidFlagT);
    CAPWAP_PUT_1BYTE (pTxPkt, pCapwapHdr->u1FlagsFLWMKResd);
    CAPWAP_PUT_2BYTE (pTxPkt, pCapwapHdr->u2FragId);
    CAPWAP_PUT_2BYTE (pTxPkt, pCapwapHdr->u2FragOffsetRes3);
    h_sz = 8;

    if (pCapwapHdr->u1RadMacAddrLength != 0)
    {
        CAPWAP_PUT_1BYTE (pTxPkt, pCapwapHdr->u1RadMacAddrLength);
        CAPWAP_PUT_NBYTE (pTxPkt, pCapwapHdr->RadioMacAddr, sizeof (tMacAddr));
        /* 1Byte Padding for 4 Byte alignment */
        CAPWAP_PUT_1BYTE (pTxPkt, 0);
        h_sz = (UINT4) (h_sz + (2 + sizeof (tMacAddr)));
    }

    if (pCapwapHdr->u1WirelessInfoLength != 0)
    {
        CAPWAP_PUT_1BYTE (pTxPkt, pCapwapHdr->u1WirelessInfoLength);
        CAPWAP_PUT_4BYTE (pTxPkt, pCapwapHdr->u4WirelessRadioInfo);
        /* 3 Bytes Padding for 4 Byte alignment */
        CAPWAP_PUT_1BYTE (pTxPkt, 0);
        CAPWAP_PUT_1BYTE (pTxPkt, 0);
        CAPWAP_PUT_1BYTE (pTxPkt, 0);
        h_sz += 8;
    }
    return;
}

/*****************************************************************************
 *                                                                           *
 * Function     : AssembleCapwapControlHeader                                *
 *                                                                           *
 * Description  : Assemble the capwap Control Header structure to            *
 *                transmittable  format.                                     *
 *                                                                           *
 * Input        : msgType:  CAPWAP Message Type                              *
 *                SeqNum : CAPWAP message sequence number                    *
 *                capwapMsgElemenLen : Message Element Length                *
 *                u1Flags : Flags                                            *
 * Output       : Packet with Capwap control Header                          *
 *                                                                           *
 * Returns      : NONE                                                       *
 *****************************************************************************/
VOID
AssembleCapwapControlHeader (UINT1 *pTxPkt,
                             UINT4 mesgType,
                             UINT1 seqNum,
                             UINT2 capwapMsgElemenLen, UINT1 u1Flags)
{
    CAPWAP_PUT_4BYTE (pTxPkt, mesgType);
    CAPWAP_PUT_1BYTE (pTxPkt, seqNum);
    CAPWAP_PUT_2BYTE (pTxPkt, capwapMsgElemenLen);
    CAPWAP_PUT_1BYTE (pTxPkt, u1Flags);
    return;
}

/*****************************************************************************
 *                                                                           *
 * Function     : CapwapAssembleLocationData                                 *
 *                                                                           *
 * Description  : Assemble the capwap Location Data Message Element in the   *
 *                transmittable  format.                                     *
 *                                                                           *
 * Input        : tLocationData : Location Data information                  *
 *                                                                           *
 * Output       : Capwap Message element updated in Packet                   *
 *                                                                           *
 * Returns      : NONE                                                       *
 *****************************************************************************/
VOID
CapwapAssembleLocationData (UINT1 *pTxPkt, tLocationData * pWtpLocation)
{
    UINT2               u2Len = 0;
    CAPWAP_PUT_2BYTE (pTxPkt, pWtpLocation->u2MsgEleType);
    CAPWAP_PUT_2BYTE (pTxPkt, pWtpLocation->u2MsgEleLen);
    u2Len = (UINT2) (STRLEN (pWtpLocation->value));
    CAPWAP_PUT_NBYTE (pTxPkt, pWtpLocation->value, u2Len);
    return;
}

/*****************************************************************************
 *                                                                           *
 * Function     : CapwapAssembleWtpBoardData                                 *
 *                                                                           *
 * Description  : Assemble capwap WTP Board Data message element in the      *
 *                transmittable  format.                                     *
 *                                                                           *
 * Input        : tWtpBoardData : WTP Board Data information                 *
 *                                                                           *
 * Output       : Capwap Message element updated in Packet                   *
 *                                                                           *
 * Returns      : NONE                                                       *
 *****************************************************************************/
VOID
CapwapAssembleWtpBoardData (UINT1 *pTxPkt, tWtpBoardData * wtpBoardData)
{
    UINT1               u1Index = 0;

    /* Copy the message type and length */
    CAPWAP_PUT_2BYTE (pTxPkt, wtpBoardData->u2MsgEleType);
    CAPWAP_PUT_2BYTE (pTxPkt, wtpBoardData->u2MsgEleLen);
    /* Copy the Vendor SMi */
    CAPWAP_PUT_4BYTE (pTxPkt, wtpBoardData->u4VendorSMI);
    while ((wtpBoardData->u1DataInfoCount > 0)
           && (u1Index < CAPWAP_MAX_BOARD_DATA_TYPE))
    {
        if (wtpBoardData->wtpBoardInfo[u1Index].u2BoardInfoLength != 0)
        {
            CAPWAP_PUT_2BYTE (pTxPkt,
                              wtpBoardData->
                              wtpBoardInfo[u1Index].u2BoardInfoType);
            CAPWAP_PUT_2BYTE (pTxPkt,
                              wtpBoardData->
                              wtpBoardInfo[u1Index].u2BoardInfoLength);
            CAPWAP_PUT_NBYTE (pTxPkt,
                              wtpBoardData->wtpBoardInfo[u1Index].boardInfoData,
                              wtpBoardData->
                              wtpBoardInfo[u1Index].u2BoardInfoLength);
            wtpBoardData->u1DataInfoCount--;
        }
        u1Index++;
    }
    return;
}

/*****************************************************************************
 *                                                                           *
 * Function     : capwapAssembleWtpDescriptor                                *
 *                                                                           *
 * Description  : Assemble WTP Descriptor Message Element to the passed      *
 *                buffer.                                                    *
 *                                                                           *
 * Input        : tWtpDescriptor : WTP Descriptor information                *
 *                                                                           *
 * Output       : Capwap Message element updated in Packet                   *
 *                                                                           *
 * Returns      : NONE                                                       *
 *****************************************************************************/
VOID
capwapAssembleWtpDescriptor (UINT1 *pTxPkt, tWtpDescriptor * wtpDescriptor)
{
    UINT4               u1Index = 0;
    /* Copy the message type and length */
    CAPWAP_PUT_2BYTE (pTxPkt, wtpDescriptor->u2MsgEleType);
    CAPWAP_PUT_2BYTE (pTxPkt, wtpDescriptor->u2MsgEleLen);
    CAPWAP_PUT_1BYTE (pTxPkt, wtpDescriptor->u1MaxRadios);
    CAPWAP_PUT_1BYTE (pTxPkt, wtpDescriptor->u1RadiosInUse);
    CAPWAP_PUT_1BYTE (pTxPkt, wtpDescriptor->u1NumEncrypt);
    CAPWAP_PUT_1BYTE (pTxPkt, wtpDescriptor->wtpEncrptCap.u1WBID);
    CAPWAP_PUT_2BYTE (pTxPkt, wtpDescriptor->wtpEncrptCap.encryptCapability);
    /* CAPWAP_PUT_4BYTE(pTxPkt, wtpDescriptor->u4VendorSMI); */
    /* Copy the vendor info in the following loop */

    while ((wtpDescriptor->u4VendorInfoCount > 0)
           && (u1Index < CAPWAP_MAX_WTP_DESCRIPTOR_TYPE))
    {
        if (wtpDescriptor->VendDesc[u1Index].u2VendorInfoLength != 0)
        {
            CAPWAP_PUT_4BYTE (pTxPkt, wtpDescriptor->u4VendorSMI);
            CAPWAP_PUT_2BYTE (pTxPkt,
                              wtpDescriptor->VendDesc[u1Index].vendorInfoType);
            CAPWAP_PUT_2BYTE (pTxPkt,
                              wtpDescriptor->
                              VendDesc[u1Index].u2VendorInfoLength);
            CAPWAP_PUT_NBYTE (pTxPkt,
                              wtpDescriptor->VendDesc[u1Index].vendorInfoData,
                              wtpDescriptor->
                              VendDesc[u1Index].u2VendorInfoLength);
            wtpDescriptor->u4VendorInfoCount--;
        }
        u1Index++;
    }

    return;
}

/*****************************************************************************
 *                                                                           *
 * Function     : capwapAssembleWtpName                                      *
 *                                                                           *
 * Description  : Assemble WTP Name message element in th passed buffer      *
 *                                                                           *
 * Input        : tWtpName : WTP Name information                            *
 *                                                                           *
 * Output       : Capwap message element updated in passed buffer            *
 *                                                                           *
 * Returns      : None                                                       *
 *****************************************************************************/
VOID
capwapAssembleWtpName (UINT1 *pTxPkt, tWtpName * wtpName)
{
    UINT2               u2Len;

    /* Copy the message type and length */
    CAPWAP_PUT_2BYTE (pTxPkt, wtpName->u2MsgEleType);
    CAPWAP_PUT_2BYTE (pTxPkt, wtpName->u2MsgEleLen);
    /* Copy the Wtp name */
    u2Len = (UINT2) (STRLEN (wtpName->wtpName));
    CAPWAP_PUT_NBYTE (pTxPkt, wtpName->wtpName, u2Len);
    return;
}

/*****************************************************************************
 *                                                                           *
 * Function     : capwapAssembleSessionId                                    *
 *                                                                           *
 * Description  : Assemble the capwap SessionID message element              *
 *                                                                           *
 * Input        : Session ID                                                 *
 *                                                                           *
 * Output       : Capwap message element updated in the passed buffer        *
 *                                                                           *
 * Returns      : None                                                       *
 *****************************************************************************/
VOID
capwapAssembleSessionId (UINT1 *pTxPkt, UINT4 *pSessionID)
{
    UINT2               mesType = SESSION_ID;
    UINT2               meslen = SESSION_ID_LEN;
    UINT1               u1Index = 0;
    UINT1               u1NumOfBlocks = SESSION_ID_LEN / sizeof (UINT4);

    /* Copy the message type and length */
    CAPWAP_PUT_2BYTE (pTxPkt, mesType);
    CAPWAP_PUT_2BYTE (pTxPkt, meslen);

    for (u1Index = 0; u1Index < u1NumOfBlocks; u1Index++)
    {
        CAPWAP_PUT_4BYTE (pTxPkt, pSessionID[u1Index]);
    }

    return;
}

/*****************************************************************************
 *                                                                           *
 * Function     : capwapAssembleWtpFrameTunnelMode                           *
 *                                                                           *
 * Description  : Assemble the WtpFrameTunnelMode message element in the     *
 *                transmittable  format.                                     *
 *                                                                           *
 * Input        : tWtpFrameTunnel : WTP Frame Tunnel information             *
 *                                                                           *
 * Output       : Capwap WTP Frame Tunnel updated in buffer                  *
 *                                                                           *
 * Returns      : None                                                       *
 *****************************************************************************/
VOID
capwapAssembleWtpFrameTunnelMode (UINT1 *pTxPkt,
                                  tWtpFrameTunnel * wtpFrameTunnelMode)
{
    /* Copy the message type and length */
    CAPWAP_PUT_2BYTE (pTxPkt, wtpFrameTunnelMode->u2MsgEleType);
    CAPWAP_PUT_2BYTE (pTxPkt, wtpFrameTunnelMode->u2MsgEleLen);
    CAPWAP_PUT_1BYTE (pTxPkt, wtpFrameTunnelMode->resvd4NELU);
    return;
}

/*****************************************************************************
 *                                                                           *
 * Function     : capwapAssembleWtpMacType                                   *
 *                                                                           *
 * Description  : Assemble the WTP MAC Type message elemente to              *
 *                transmittable  format.                                     *
 *                                                                           *
 * Input        : tWtpMacType : WTP MAC Type information                     *
 *                                                                           *
 * Output       : Capwap WTP MAC Type message element updated in buffer      *
 *                                                                           *
 * Returns      : NONE                                                       *
 *****************************************************************************/
VOID
capwapAssembleWtpMacType (UINT1 *pTxPkt, tWtpMacType * wtpMacType)
{
    /* Copy the message type and length */
    CAPWAP_PUT_2BYTE (pTxPkt, wtpMacType->u2MsgEleType);
    CAPWAP_PUT_2BYTE (pTxPkt, wtpMacType->u2MsgEleLen);
    CAPWAP_PUT_1BYTE (pTxPkt, wtpMacType->macValue);
    return;
}

/*****************************************************************************
 *                                                                           *
 * Function     : capwapAssembleIeeeWtpRadioInformation                      *
 *                                                                           *
 * Description  : Assemble the IeeeWtpRadioInformation message element       *
 *                                                                           *
 * Input        : tRadioIfInfo : Radio information                           *
 *                                                                           *
 * Output       : Capwap messagei element to be transmitted out              *
 *                                                                           *
 * Returns      : NONE                                                       *
 *****************************************************************************/
VOID
capwapAssembleIeeeWtpRadioInformation (UINT1 *pTxPkt,
                                       tRadioIfInfo * wtpRadioInfo)
{
    /* Copy the message type and length */
    CAPWAP_PUT_2BYTE (pTxPkt, wtpRadioInfo->u2MessageType);
    CAPWAP_PUT_2BYTE (pTxPkt, wtpRadioInfo->u2MessageLength);
    CAPWAP_PUT_1BYTE (pTxPkt, wtpRadioInfo->u1RadioId);
    CAPWAP_PUT_4BYTE (pTxPkt, wtpRadioInfo->u4RadioType);
    return;
}

/*****************************************************************************
 *                                                                           *
 * Function     : capwapAssembleEcnSupport                                   *
 *                                                                           *
 * Description  : Assemble the ECN Support message element                   *
 *                                                                           *
 * Input        : tEcnSupport ECN Support information                        *
 *                                                                           *
 * Output       : Capwap message element to be transmitted out               *
 *                                                                           *
 * Returns      : NONE                                                       *
 *****************************************************************************/
VOID
capwapAssembleEcnSupport (UINT1 *pTxPkt, tEcnSupport * ecnSuppport)
{
    /* Copy the message type and length */
    CAPWAP_PUT_2BYTE (pTxPkt, ecnSuppport->u2MsgEleType);
    CAPWAP_PUT_2BYTE (pTxPkt, ecnSuppport->u2MsgEleLen);
    /* Copy the 1 byte ECN support value */
    CAPWAP_PUT_1BYTE (pTxPkt, ecnSuppport->u1EcnSupport);

    return;
}

/*****************************************************************************
 *                                                                           *
 * Function     : capwapAssembleLocalIPAddress                               *
 *                                                                           *
 * Description  : Assemble the LocalIPAddress message element                *
 *                                                                           *
 * Input        : tLocalIpAddr : Local IP Address                            *
 *                                                                           *
 * Output       : Capwap control message to be transmitted out               *
 *                                                                           *
 * Returns      : SUCCESS/FAILURE                                            *
 *****************************************************************************/
VOID
capwapAssembleLocalIPAddress (UINT1 *pTxPkt, tLocalIpAddr * ipAddr)
{
    /* Copy the message type and length */
    CAPWAP_PUT_2BYTE (pTxPkt, ipAddr->u2MsgEleType);
    CAPWAP_PUT_2BYTE (pTxPkt, ipAddr->u2MsgEleLen);
    if (ipAddr->u2MsgEleType == CAPWAP_LOCAL_IPV4_ADDR)
    {
        CAPWAP_PUT_4BYTE (pTxPkt, ipAddr->ipAddr.u4_addr[0]);
    }
    else
    {
        CAPWAP_PUT_NBYTE (pTxPkt, ipAddr->ipAddr.u1_addr[0],
                          ipAddr->u2MsgEleLen);
    }
    return;
}

/*****************************************************************************
 *                                                                           *
 * Function     : capwapAssembleTransportProtocol                            *
 *                                                                           *
 * Description  : Assemble the capwap iTransportProtocol message element     *
 *                                                                           *
 * Input        : tCapwapTransprotocol : Transport protocol information      *
 *                                                                           *
 * Output       : Capwap message element to be transmitted out               *
 *                                                                           *
 * Returns      : NONE                                                       *
 *****************************************************************************/
VOID
capwapAssembleTrasnportProtocol (UINT1 *pTxPkt,
                                 tCapwapTransprotocol * transProtocol)
{
    CAPWAP_FN_ENTRY ();
    /* Copy the message type and length */
    CAPWAP_PUT_2BYTE (pTxPkt, transProtocol->u2MsgEleType);
    CAPWAP_PUT_2BYTE (pTxPkt, transProtocol->u2MsgEleLen);
    CAPWAP_PUT_1BYTE (pTxPkt, transProtocol->transportType);
    CAPWAP_FN_EXIT ();
    return;
}

/*****************************************************************************
 *                                                                           *
 * Function     : capwapAssembleMaxMessageLength                             *
 *                                                                           *
 * Description  : Assemble the capwap MAX Message Length message element     *
 *                                                                           *
 * Input        : tMaxMsgLen : message length information                    *
 *                                                                           *
 * Output       : Capwap messagei element to be transmitted out              *
 *                                                                           *
 * Returns      : NONE                                                       *
 *****************************************************************************/
VOID
capwapAssembleMaxMessageLength (UINT1 *pTxPkt, tMaxMsgLen * maxMsgLen)
{
    /* Copy the message type and length */
    CAPWAP_PUT_2BYTE (pTxPkt, maxMsgLen->u2MsgEleType);
    CAPWAP_PUT_2BYTE (pTxPkt, maxMsgLen->u2MsgEleLen);
    CAPWAP_PUT_2BYTE (pTxPkt, maxMsgLen->u2MaxMsglen);
    return;
}

/*****************************************************************************
 *                                                                           *
 * Function     : capwapAssembleWtpDot11Statistics                           *
 *                                                                           *
 * Description  : Assemble the WtpDot11Statistics message element            *
 *                                                                           *
 * Input        : tWtpdot11StatsElement : WTP dot11 Stats information        *
 *                                                                           *
 * Output       : Capwap control message to be transmitted out               *
 *                                                                           *
 * Returns      : NONE                                                       *
 *****************************************************************************/
VOID
capwapAssembleWtpDot11Statistics (UINT1 *pTxPkt,
                                  tWtpdot11StatsElement * dot11StatsElement)
{
    /* Copy the message type and length */
    CAPWAP_PUT_2BYTE (pTxPkt, dot11StatsElement->u2MsgEleType);
    CAPWAP_PUT_2BYTE (pTxPkt, dot11StatsElement->u2MsgEleLen);
    /* Copy the reboot statistics */
    CAPWAP_PUT_1BYTE (pTxPkt, dot11StatsElement->dot11Stats.u1RadioId);
    /* Reserved 3 bytes as zero, Refer RFC 5416 */
    CAPWAP_PUT_2BYTE (pTxPkt, 0);
    CAPWAP_PUT_1BYTE (pTxPkt, 0);
    CAPWAP_PUT_4BYTE (pTxPkt, dot11StatsElement->dot11Stats.u4TxFragmentCnt);
    CAPWAP_PUT_4BYTE (pTxPkt, dot11StatsElement->dot11Stats.u4MulticastTxCnt);
    CAPWAP_PUT_4BYTE (pTxPkt, dot11StatsElement->dot11Stats.u4FailedCount);
    CAPWAP_PUT_4BYTE (pTxPkt, dot11StatsElement->dot11Stats.u4RetryCount);
    CAPWAP_PUT_4BYTE (pTxPkt,
                      dot11StatsElement->dot11Stats.u4MultipleRetryCount);
    CAPWAP_PUT_4BYTE (pTxPkt, dot11StatsElement->dot11Stats.u4FrameDupCount);
    CAPWAP_PUT_4BYTE (pTxPkt, dot11StatsElement->dot11Stats.u4RTSSuccessCount);
    CAPWAP_PUT_4BYTE (pTxPkt, dot11StatsElement->dot11Stats.u4RTSFailCount);
    CAPWAP_PUT_4BYTE (pTxPkt, dot11StatsElement->dot11Stats.u4ACKFailCount);
    CAPWAP_PUT_4BYTE (pTxPkt, dot11StatsElement->dot11Stats.u4RxFragmentCount);
    CAPWAP_PUT_4BYTE (pTxPkt, dot11StatsElement->dot11Stats.u4MulticastRxCount);
    CAPWAP_PUT_4BYTE (pTxPkt, dot11StatsElement->dot11Stats.u4FCSErrCount);
    CAPWAP_PUT_4BYTE (pTxPkt, dot11StatsElement->dot11Stats.u4TxFrameCount);
    CAPWAP_PUT_4BYTE (pTxPkt, dot11StatsElement->dot11Stats.u4DecryptionErr);
    CAPWAP_PUT_4BYTE (pTxPkt,
                      dot11StatsElement->dot11Stats.u4DiscardQosFragmentCnt);
    CAPWAP_PUT_4BYTE (pTxPkt,
                      dot11StatsElement->dot11Stats.u4AssociatedStaCount);
    CAPWAP_PUT_4BYTE (pTxPkt,
                      dot11StatsElement->dot11Stats.u4QosCFPollsRecvdCount);
    CAPWAP_PUT_4BYTE (pTxPkt,
                      dot11StatsElement->dot11Stats.u4QosCFPollsUnusedCount);
    CAPWAP_PUT_4BYTE (pTxPkt,
                      dot11StatsElement->dot11Stats.u4QosCFPollsUnusableCount);
    return;
}

/*****************************************************************************
 *                                                                           *
 * Function     : capwapAssembleWtpRebootStatistics                          *
 *                                                                           *
 * Description  : Assemble the WTP Reboot Statistics message element         *
 *                                                                           *
 * Input        : tWtpRebootStatsElement : WTP reboot statistics information *
 *                                                                           *
 * Output       : Capwap message element to be transmitted out               *
 *                                                                           *
 * Returns      : NONE                                                       *
 *****************************************************************************/
VOID
capwapAssembleWtpRebootStatistics (UINT1 *pTxPkt,
                                   tWtpRebootStatsElement * rebootStats)
{
    /* Copy the message type and length */
    CAPWAP_PUT_2BYTE (pTxPkt, rebootStats->u2MsgEleType);
    CAPWAP_PUT_2BYTE (pTxPkt, rebootStats->u2MsgEleLen);
    /* Copy the reboot statistics */
    CAPWAP_PUT_2BYTE (pTxPkt, rebootStats->u2RebootCount);
    CAPWAP_PUT_2BYTE (pTxPkt, rebootStats->u2AcInitiatedCount);
    CAPWAP_PUT_2BYTE (pTxPkt, rebootStats->u2LinkFailCount);
    CAPWAP_PUT_2BYTE (pTxPkt, rebootStats->u2SwFailCount);
    CAPWAP_PUT_2BYTE (pTxPkt, rebootStats->u2HwFailCount);
    CAPWAP_PUT_2BYTE (pTxPkt, rebootStats->u2OtherFailCount);
    CAPWAP_PUT_2BYTE (pTxPkt, rebootStats->u2UnknownFailCount);
    CAPWAP_PUT_1BYTE (pTxPkt, rebootStats->u1LastFailureType);
    return;

}

/*****************************************************************************
 *                                                                           *
 * Function     : CapwapAssembleVendorSpecificPayload                        *
 *                                                                           *
 * Description  : Assemble the VendorSpecificPayload in the buffer           *
 *                                                                           *
 * Input        : tVendorSpecPayload : Vendor Specific message element       *
 *                information                                                *
 *                                                                           *
 * Output       : Capwap message element to be transmitted out               *
 *                                                                           *
 * Returns      : NONE                                                       *
 *****************************************************************************/
VOID
CapwapAssembleVendorSpecificPayload (UINT1 *pTxPkt,
                                     tVendorSpecPayload * vendPld)
{
#ifdef RFMGMT_WANTED
    UINT1               u1RadioId = 0;
    UINT1               u1WlanId = 0;
    INT2                i2Index = 0;
#endif
#ifdef WPA_WANTED
    UINT1               u1Index = 0;
#endif
    CAPWAP_FN_ENTRY ();

    /* Copy the message type and length */
    CAPWAP_PUT_2BYTE (pTxPkt, vendPld->u2MsgEleType);
    CAPWAP_PUT_2BYTE (pTxPkt, vendPld->u2MsgEleLen);

    switch (vendPld->elementId)
    {
        case VENDOR_DISC_TYPE:
            if (vendPld->unVendorSpec.VendDiscType.isOptional == OSIX_TRUE)
            {
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendDiscType.u2MsgEleType);
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendDiscType.u2MsgEleLen);
                CAPWAP_PUT_4BYTE (pTxPkt,
                                  vendPld->unVendorSpec.VendDiscType.vendorId);
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  vendPld->unVendorSpec.VendDiscType.u1Val);
                CAPWAP_PUT_4BYTE (pTxPkt,
                                  vendPld->unVendorSpec.VendDiscType.
                                  u4WlcIpAddress);
            }
            break;
        case VENDOR_UDP_SERVER_PORT_MSG:
            if (vendPld->unVendorSpec.VendUdpPort.isOptional == OSIX_TRUE)
            {
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendUdpPort.u2MsgEleType);
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendUdpPort.u2MsgEleLen);
                CAPWAP_PUT_4BYTE (pTxPkt,
                                  vendPld->unVendorSpec.VendUdpPort.vendorId);
                CAPWAP_PUT_4BYTE (pTxPkt,
                                  vendPld->unVendorSpec.VendUdpPort.u4Val);
            }
            break;
        case VENDOR_CONTROL_POLICY_DTLS_MSG:
            if (vendPld->unVendorSpec.VendorContDTLS.isOptional == OSIX_TRUE)
            {
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendorContDTLS.u2MsgEleType);
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendorContDTLS.u2MsgEleLen);
                CAPWAP_PUT_4BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendorContDTLS.vendorId);
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  vendPld->unVendorSpec.VendorContDTLS.u1Val);
            }
            break;
        case VENDOR_DOT11N_TYPE:
            if (vendPld->unVendorSpec.Dot11nVendor.isOptional == OSIX_TRUE)
            {
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  Dot11nVendor.u2MsgEleType);
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  Dot11nVendor.u2MsgEleLen);
                CAPWAP_PUT_4BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  Dot11nVendor.u4VendorId);
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  vendPld->unVendorSpec.Dot11nVendor.u1RadioId);
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  vendPld->unVendorSpec.Dot11nVendor.u1HtFlag);
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  Dot11nVendor.u1MaxSuppMCS);
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  Dot11nVendor.u1MaxManMCS);
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  Dot11nVendor.u1TxAntenna);
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  Dot11nVendor.u1RxAntenna);
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  Dot11nVendor.u2Reserved);
            }
            break;

        case VENDOR_DOT11N_STA_TYPE:
            if (vendPld->unVendorSpec.Dot11nStaVendor.isOptional == OSIX_TRUE)
            {
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  Dot11nStaVendor.u2MsgEleType);
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  Dot11nStaVendor.u2MsgEleLen);
                CAPWAP_PUT_4BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  Dot11nStaVendor.u4VendorId);
                CAPWAP_PUT_NBYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  Dot11nStaVendor.stationMacAddr, 6);
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  Dot11nStaVendor.u1HtFlag);
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  Dot11nStaVendor.u1MaxRxFactor);
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  Dot11nStaVendor.u1MinStaSpacing);
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  Dot11nStaVendor.u2HiSuppDataRate);
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  Dot11nStaVendor.u2AMPDUBuffSize);
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  Dot11nStaVendor.u1HTCSupp);
                CAPWAP_PUT_NBYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  Dot11nStaVendor.au1ManMCSSet,
                                  CAPWAP_11N_MCS_RATE_LEN);
            }
            break;

        case VENDOR_SSID_ISOLATION_MSG:
            if (vendPld->unVendorSpec.VendSsidIsolation.isOptional == OSIX_TRUE)
            {
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendSsidIsolation.u2MsgEleType);
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendSsidIsolation.u2MsgEleLen);
                CAPWAP_PUT_4BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendSsidIsolation.vendorId);
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendSsidIsolation.u1Val);
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendSsidIsolation.u1RadioId);
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendSsidIsolation.u1WlanId);
            }
            break;
#ifdef WLC_WANTED
        case VENDOR_MULTICAST_MSG:
            if (vendPld->unVendorSpec.VendorMulticast.isOptional == OSIX_TRUE)
            {
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendorMulticast.u2MsgEleType);
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendorMulticast.u2MsgEleLen);
                CAPWAP_PUT_4BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendorMulticast.vendorId);
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendorMulticast.u1RadioId);
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendorMulticast.u1WlanId);
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendorMulticast.u2WlanMulticastMode);
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendorMulticast.u2WlanSnoopTableLength);
                CAPWAP_PUT_4BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendorMulticast.u4WlanSnoopTimer);
                CAPWAP_PUT_4BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendorMulticast.u4WlanSnoopTimeout);
            }
            break;
#endif
        case VENDOR_SSID_RATE_MSG:
            if (vendPld->unVendorSpec.VendSsidRate.isOptional == OSIX_TRUE)
            {
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendSsidRate.u2MsgEleType);
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendSsidRate.u2MsgEleLen);
                CAPWAP_PUT_4BYTE (pTxPkt,
                                  vendPld->unVendorSpec.VendSsidRate.vendorId);
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendSsidRate.u1QosRateLimit);
                CAPWAP_PUT_4BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendSsidRate.u4QosUpStreamCIR);
                CAPWAP_PUT_4BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendSsidRate.u4QosUpStreamCBS);
                CAPWAP_PUT_4BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendSsidRate.u4QosUpStreamEIR);
                CAPWAP_PUT_4BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendSsidRate.u4QosUpStreamEBS);
                CAPWAP_PUT_4BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendSsidRate.u4QosDownStreamCIR);
                CAPWAP_PUT_4BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendSsidRate.u4QosDownStreamCBS);
                CAPWAP_PUT_4BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendSsidRate.u4QosDownStreamEIR);
                CAPWAP_PUT_4BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendSsidRate.u4QosDownStreamEBS);
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendSsidRate.u1PassengerTrustMode);
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  vendPld->unVendorSpec.VendSsidRate.u1RadioId);
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  vendPld->unVendorSpec.VendSsidRate.u1WlanId);
            }
            break;
        case VENDOR_DOMAIN_NAME_TYPE:
            if (vendPld->unVendorSpec.VendDns.isOptional == OSIX_TRUE)
            {
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.VendDns.u2MsgEleType);
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.VendDns.u2MsgEleLen);
                CAPWAP_PUT_4BYTE (pTxPkt,
                                  vendPld->unVendorSpec.VendDns.vendorId);
                CAPWAP_PUT_NBYTE (pTxPkt,
                                  vendPld->unVendorSpec.VendDns.au1DomainName,
                                  32);
                /*   CAPWAP_PUT_NBYTE(pTxPkt, vendPld->unVendorSpec.VendDns.au1ServerIp, 4); */
            }
            break;
        case VENDOR_NATIVE_VLAN:
            if (vendPld->unVendorSpec.VendVlan.isOptional == OSIX_TRUE)
            {
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.VendVlan.u2MsgEleType);
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.VendVlan.u2MsgEleLen);
                CAPWAP_PUT_4BYTE (pTxPkt,
                                  vendPld->unVendorSpec.VendVlan.vendorId);
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.VendVlan.u2VlanId);
            }
            break;
        case VENDOR_MGMT_SSID_TYPE:
            if (vendPld->unVendorSpec.VendSsid.isOptional == OSIX_TRUE)
            {
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.VendSsid.u2MsgEleType);
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.VendSsid.u2MsgEleLen);
                CAPWAP_PUT_4BYTE (pTxPkt,
                                  vendPld->unVendorSpec.VendSsid.vendorId);
                CAPWAP_PUT_NBYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendSsid.au1ManagmentSSID,
                                  /* strlen(vendPld->unVendorSpec.VendSsid.au1ManagmentSSID)); */
                                  WSSWLAN_SSID_NAME_LEN);
            }
            break;
        case VENDOR_LOCAL_ROUTING_TYPE:

            if (vendPld->unVendorSpec.VendLocalRouting.isOptional == OSIX_TRUE)
            {
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendLocalRouting.u2MsgEleType);
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendLocalRouting.u2MsgEleLen);
                CAPWAP_PUT_4BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendLocalRouting.vendorId);
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendLocalRouting.u1WtpLocalRouting);
            }
            break;
#ifdef RFMGMT_WANTED
        case VENDOR_NEIGH_CONFIG_TYPE:
        {
            if (vendPld->unVendorSpec.VendorNeighConfig.isOptional == OSIX_TRUE)
            {
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendorNeighConfig.u2MsgEleType);
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendorNeighConfig.u2MsgEleLen);
                CAPWAP_PUT_4BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendorNeighConfig.u4VendorId);
#ifdef WTP_WANTED
                for (u1RadioId = 1; u1RadioId <= SYS_DEF_MAX_RADIO_INTERFACES;
                     u1RadioId++)
                {
#else
                for (u1RadioId = 1; u1RadioId <= MAX_NUM_OF_RADIO_PER_AP;
                     u1RadioId++)
                {
                    if (vendPld->unVendorSpec.
                        VendorNeighConfig.VendorNeighParams[u1RadioId -
                                                            1].u1RadioId != 0)
#endif
                    {
                        CAPWAP_PUT_1BYTE (pTxPkt,
                                          vendPld->
                                          unVendorSpec.VendorNeighConfig.
                                          VendorNeighParams[u1RadioId -
                                                            1].u1RadioId);
                        CAPWAP_PUT_1BYTE (pTxPkt,
                                          vendPld->unVendorSpec.
                                          VendorNeighConfig.
                                          VendorNeighParams[u1RadioId -
                                                            1].
                                          u1AutoScanStatus);
                        CAPWAP_PUT_2BYTE (pTxPkt,
                                          vendPld->unVendorSpec.
                                          VendorNeighConfig.
                                          VendorNeighParams[u1RadioId -
                                                            1].
                                          u2NeighborMsgPeriod);
                        CAPWAP_PUT_2BYTE (pTxPkt,
                                          vendPld->unVendorSpec.
                                          VendorNeighConfig.
                                          VendorNeighParams[u1RadioId -
                                                            1].
                                          u2ChannelScanDuration);
                        CAPWAP_PUT_2BYTE (pTxPkt,
                                          vendPld->unVendorSpec.
                                          VendorNeighConfig.
                                          VendorNeighParams[u1RadioId -
                                                            1].
                                          u2NeighborAgingPeriod);
                        CAPWAP_PUT_2BYTE (pTxPkt,
                                          vendPld->unVendorSpec.
                                          VendorNeighConfig.
                                          VendorNeighParams[u1RadioId -
                                                            1].i2RssiThreshold);
                    }
                }
            }
        }
            break;
        case VENDOR_CLIENT_CONFIG_TYPE:
        {
            if (vendPld->unVendorSpec.VendorClientConfig.isOptional
                == OSIX_TRUE)
            {
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendorClientConfig.u2MsgEleType);
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendorClientConfig.u2MsgEleLen);
                CAPWAP_PUT_4BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendorClientConfig.u4VendorId);
#ifdef WTP_WANTED
                for (u1RadioId = 1; u1RadioId <= SYS_DEF_MAX_RADIO_INTERFACES;
                     u1RadioId++)
                {
#else
                for (u1RadioId = 1; u1RadioId <= MAX_NUM_OF_RADIO_PER_AP;
                     u1RadioId++)
                {
                    if (vendPld->unVendorSpec.
                        VendorClientConfig.VendorClientParams[u1RadioId -
                                                              1].u1RadioId != 0)
#endif
                    {
                        CAPWAP_PUT_1BYTE (pTxPkt,
                                          vendPld->
                                          unVendorSpec.VendorClientConfig.
                                          VendorClientParams[u1RadioId -
                                                             1].u1RadioId);
                        CAPWAP_PUT_1BYTE (pTxPkt,
                                          vendPld->unVendorSpec.
                                          VendorClientConfig.
                                          VendorClientParams[u1RadioId -
                                                             1].
                                          u1SNRScanStatus);
                        CAPWAP_PUT_2BYTE (pTxPkt,
                                          vendPld->unVendorSpec.
                                          VendorClientConfig.
                                          VendorClientParams[u1RadioId -
                                                             1].
                                          u2SNRScanPeriod);
                        CAPWAP_PUT_2BYTE (pTxPkt,
                                          vendPld->unVendorSpec.
                                          VendorClientConfig.
                                          VendorClientParams[u1RadioId -
                                                             1].i2SNRThreshold);
                        CAPWAP_PUT_1BYTE (pTxPkt,
                                          vendPld->unVendorSpec.
                                          VendorClientConfig.
                                          VendorClientParams[u1RadioId -
                                                             1].
                                          u1BssidScanStatus[u1WlanId]);
                        CAPWAP_PUT_1BYTE (pTxPkt,
                                          vendPld->unVendorSpec.
                                          VendorClientConfig.
                                          VendorClientParams[u1RadioId -
                                                             1].u1WlanId);
                    }
                }
            }
        }
            break;
        case VENDOR_CH_SWITCH_STATUS_TYPE:
        {
            if (vendPld->unVendorSpec.VendorChSwitchStatus.isOptional
                == OSIX_TRUE)
            {
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendorChSwitchStatus.u2MsgEleType);
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendorChSwitchStatus.u2MsgEleLen);
                CAPWAP_PUT_4BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendorChSwitchStatus.u4VendorId);
#ifdef WTP_WANTED
                for (u1RadioId = 1; u1RadioId <=
                     SYS_DEF_MAX_RADIO_INTERFACES; u1RadioId++)
                {
#else
                for (u1RadioId = 1; u1RadioId <= MAX_NUM_OF_RADIO_PER_AP;
                     u1RadioId++)
                {
                    if (vendPld->unVendorSpec.
                        VendorChSwitchStatus.VendorChSwitchParams[u1RadioId -
                                                                  1].
                        u1RadioId != 0)
#endif
                    {
                        CAPWAP_PUT_1BYTE (pTxPkt,
                                          vendPld->
                                          unVendorSpec.VendorChSwitchStatus.
                                          VendorChSwitchParams[u1RadioId -
                                                               1].u1RadioId);
                        CAPWAP_PUT_1BYTE (pTxPkt,
                                          vendPld->unVendorSpec.
                                          VendorChSwitchStatus.
                                          VendorChSwitchParams[u1RadioId -
                                                               1].
                                          u1ChSwitchStatus);
                    }
                }
            }
        }
            break;
        case VENDOR_SPECT_MGMT_TPC_TYPE:
        {
            if (vendPld->unVendorSpec.VendorTpcSpectMgmt.isOptional
                == OSIX_TRUE)
            {
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendorTpcSpectMgmt.u2MsgEleType);
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendorTpcSpectMgmt.u2MsgEleLen);
                CAPWAP_PUT_4BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendorTpcSpectMgmt.u4VendorId);
#ifdef WTP_WANTED
                for (u1RadioId = 1; u1RadioId <=
                     SYS_DEF_MAX_RADIO_INTERFACES; u1RadioId++)
                {
#else
                for (u1RadioId = 1; u1RadioId <= MAX_NUM_OF_RADIO_PER_AP;
                     u1RadioId++)
                {
                    if (vendPld->unVendorSpec.
                        VendorTpcSpectMgmt.VendorTpcSpectMgmtParams[u1RadioId -
                                                                    1].
                        u1RadioId != 0)
#endif
                    {
                        CAPWAP_PUT_1BYTE (pTxPkt,
                                          vendPld->
                                          unVendorSpec.VendorTpcSpectMgmt.
                                          VendorTpcSpectMgmtParams[u1RadioId -
                                                                   1].
                                          u1RadioId);
                        CAPWAP_PUT_2BYTE (pTxPkt,
                                          vendPld->unVendorSpec.
                                          VendorTpcSpectMgmt.
                                          VendorTpcSpectMgmtParams[u1RadioId -
                                                                   1].
                                          u2TpcRequestInterval);
                        CAPWAP_PUT_1BYTE (pTxPkt,
                                          vendPld->unVendorSpec.
                                          VendorTpcSpectMgmt.
                                          VendorTpcSpectMgmtParams[u1RadioId -
                                                                   1].
                                          u111hTpcStatus);
                    }
                }
            }
        }
            break;
        case VENDOR_SPECTRUM_MGMT_DFS_TYPE:
        {
            if (vendPld->unVendorSpec.VendorDfsParams.isOptional == OSIX_TRUE)
            {
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendorDfsParams.u2MsgEleType);
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendorDfsParams.u2MsgEleLen);
                CAPWAP_PUT_4BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendorDfsParams.u4VendorId);
#ifdef WTP_WANTED
                for (u1RadioId = 1; u1RadioId <=
                     SYS_DEF_MAX_RADIO_INTERFACES; u1RadioId++)
                {
#else
                for (u1RadioId = 1; u1RadioId <= MAX_NUM_OF_RADIO_PER_AP;
                     u1RadioId++)
                {
                    if (vendPld->unVendorSpec.
                        VendorDfsParams.VendorDfsParamsParams[u1RadioId -
                                                              1].u1RadioId != 0)
#endif
                    {
                        CAPWAP_PUT_1BYTE (pTxPkt,
                                          vendPld->
                                          unVendorSpec.VendorDfsParams.
                                          VendorDfsParamsParams[u1RadioId -
                                                                1].u1RadioId);
                        CAPWAP_PUT_2BYTE (pTxPkt,
                                          vendPld->unVendorSpec.VendorDfsParams.
                                          VendorDfsParamsParams[u1RadioId -
                                                                1].
                                          u2DfsQuietInterval);
                        CAPWAP_PUT_2BYTE (pTxPkt,
                                          vendPld->unVendorSpec.VendorDfsParams.
                                          VendorDfsParamsParams[u1RadioId -
                                                                1].
                                          u2DfsQuietPeriod);
                        CAPWAP_PUT_2BYTE (pTxPkt,
                                          vendPld->unVendorSpec.VendorDfsParams.
                                          VendorDfsParamsParams[u1RadioId -
                                                                1].
                                          u2DfsMeasurementInterval);
                        CAPWAP_PUT_2BYTE (pTxPkt,
                                          vendPld->unVendorSpec.VendorDfsParams.
                                          VendorDfsParamsParams[u1RadioId -
                                                                1].
                                          u2DfsChannelSwitchStatus);
                        CAPWAP_PUT_1BYTE (pTxPkt,
                                          vendPld->unVendorSpec.VendorDfsParams.
                                          VendorDfsParamsParams[u1RadioId -
                                                                1].
                                          u111hDfsStatus);
                    }
                }
            }
        }
            break;

#endif
        case VENDOR_WEBAUTH_COMPLETE_MSG:
            if (vendPld->unVendorSpec.VendorWebAuthStatus.isOptional ==
                OSIX_TRUE)
            {
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendorWebAuthStatus.u2MsgEleType);
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendorWebAuthStatus.u2MsgEleLen);
                CAPWAP_PUT_4BYTE (pTxPkt,
                                  vendPld->unVendorSpec.VendorWebAuthStatus.
                                  vendorId);
                CAPWAP_PUT_NBYTE (pTxPkt,
                                  vendPld->unVendorSpec.VendorWebAuthStatus.
                                  StationMac, sizeof (tMacAddr));
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  vendPld->unVendorSpec.VendorWebAuthStatus.
                                  u1WebAuthCompleteStatus);
            }
            break;

        case VENDOR_USERROLE_TYPE:
            if (vendPld->unVendorSpec.WssStaVendSpecPayload.isOptional ==
                OSIX_TRUE)
            {
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  WssStaVendSpecPayload.u2MsgEleType);
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  WssStaVendSpecPayload.u2MsgEleLen);
                CAPWAP_PUT_4BYTE (pTxPkt,
                                  vendPld->unVendorSpec.WssStaVendSpecPayload.
                                  u4VendorId);
                CAPWAP_PUT_NBYTE (pTxPkt,
                                  vendPld->unVendorSpec.WssStaVendSpecPayload.
                                  StaMacAddr, sizeof (tMacAddr));
                CAPWAP_PUT_4BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  WssStaVendSpecPayload.u4BandWidth);
                CAPWAP_PUT_4BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  WssStaVendSpecPayload.u4DLBandWidth);
                CAPWAP_PUT_4BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  WssStaVendSpecPayload.u4ULBandWidth);
            }
            break;
        case VENDOR_WMM_TYPE:
            if (vendPld->unVendorSpec.WMMStaVendor.isOptional == OSIX_TRUE)
            {
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  WMMStaVendor.u2MsgEleType);
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  WMMStaVendor.u2MsgEleLen);
                CAPWAP_PUT_4BYTE (pTxPkt,
                                  vendPld->unVendorSpec.WMMStaVendor.
                                  u4VendorId);
                CAPWAP_PUT_NBYTE (pTxPkt,
                                  vendPld->unVendorSpec.WMMStaVendor.
                                  StaMacAddr, sizeof (tMacAddr));
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  WMMStaVendor.u1WmmStaFlag);
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.WMMStaVendor.u2TcpPort);
                CAPWAP_PUT_4BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  WMMStaVendor.u4BandwidthThresh);
            }
            break;
#ifdef WPA_WANTED
        case VENDOR_WPA_TYPE:
        {
            if (vendPld->unVendorSpec.WpaIEElements.isOptional == OSIX_TRUE)
            {
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  WpaIEElements.u2MsgEleType);
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  WpaIEElements.u2MsgEleLen);
                CAPWAP_PUT_4BYTE (pTxPkt,
                                  vendPld->unVendorSpec.WpaIEElements.
                                  u4VendorId);
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.WpaIEElements.u2Ver);
                CAPWAP_PUT_NBYTE (pTxPkt,
                                  vendPld->unVendorSpec.WpaIEElements.
                                  au1GroupCipherSuite, 4);
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.WpaIEElements.
                                  u2PairwiseCipherSuiteCount);
                for (u1Index = 0;
                     u1Index <
                     vendPld->unVendorSpec.WpaIEElements.
                     u2PairwiseCipherSuiteCount
                     && u1Index < WPA_PAIRWISE_CIPHER_COUNT; u1Index++)
                {
                    CAPWAP_PUT_NBYTE (pTxPkt,
                                      vendPld->unVendorSpec.WpaIEElements.
                                      aWpaPwCipherDB[u1Index].au1PairwiseCipher,
                                      4);
                }
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.WpaIEElements.
                                  u2AKMSuiteCount);

                for (u1Index = 0;
                     u1Index <
                     vendPld->unVendorSpec.WpaIEElements.u2AKMSuiteCount;
                     u1Index++)
                {
                    CAPWAP_PUT_NBYTE (pTxPkt,
                                      vendPld->unVendorSpec.WpaIEElements.
                                      aWpaAkmDB[u1Index].au1AKMSuite, 4);
                }
            }
        }
            break;
#endif

        case VENDOR_WLAN_VLAN_TYPE:
            if (vendPld->unVendorSpec.VendWlanVlan.isOptional == OSIX_TRUE)
            {
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendWlanVlan.u2MsgEleType);
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendWlanVlan.u2MsgEleLen);
                CAPWAP_PUT_4BYTE (pTxPkt,
                                  vendPld->unVendorSpec.VendWlanVlan.vendorId);
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.VendWlanVlan.u2VlanId);
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  vendPld->unVendorSpec.VendWlanVlan.u1RadioId);
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  vendPld->unVendorSpec.VendWlanVlan.u1WlanId);
            }
            break;

        case VENDOR_DFS_RADAR_STATS:
        {
            if (OSIX_TRUE == vendPld->unVendorSpec.DFSRadarEventInfo.isOptional)
            {

                CAPWAP_PUT_4BYTE (pTxPkt,
                                  vendPld->unVendorSpec.DFSRadarEventInfo.
                                  u4VendorId);
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  DFSRadarEventInfo.u2MsgEleType);
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  DFSRadarEventInfo.u2MsgEleLen);
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.DFSRadarEventInfo.
                                  u2ChannelNum);
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  (UINT1) vendPld->unVendorSpec.
                                  DFSRadarEventInfo.isRadarFound);

            }

        }
            break;
#ifdef WPS_WTP_WANTED
        case VENDOR_WPS_TYPE:
            if (vendPld->unVendorSpec.VendorWpsConfig.isOptional == OSIX_TRUE)
            {
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendorWpsConfig.u2MsgEleType);
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendorWpsConfig.u2MsgEleLen);
                CAPWAP_PUT_4BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendorWpsConfig.u4vendorId);
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendorWpsConfig.u1WlanId);
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendorWpsConfig.u2WtpInternalId);
            }
            break;
#endif
#ifdef WLC_WANTED
        case VENDOR_DIFF_SERV_TYPE:
        {
            if (vendPld->unVendorSpec.VendorSpecDiffServ.isOptional ==
                OSIX_TRUE)
            {
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendorSpecDiffServ.u2MsgEleType);
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendorSpecDiffServ.u2MsgEleLen);
                CAPWAP_PUT_4BYTE (pTxPkt,
                                  vendPld->unVendorSpec.VendorSpecDiffServ.
                                  u4VendorId);
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  vendPld->unVendorSpec.VendorSpecDiffServ.
                                  u1EntryStatus);
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  vendPld->unVendorSpec.VendorSpecDiffServ.
                                  u1RadioId);
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  vendPld->unVendorSpec.VendorSpecDiffServ.
                                  u1InPriority);
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  vendPld->unVendorSpec.VendorSpecDiffServ.
                                  u1OutDscp);
            }
        }
            break;
        case VENDOR_WLAN_INTERFACE_IP:
            if (vendPld->unVendorSpec.VendorWlanInterfaceIp.isOptional ==
                OSIX_TRUE)
            {
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendorWlanInterfaceIp.u2MsgEleType);
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendorWlanInterfaceIp.u2MsgEleLen);
                CAPWAP_PUT_4BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendorWlanInterfaceIp.u4vendorId);
                CAPWAP_PUT_4BYTE (pTxPkt,
                                  vendPld->unVendorSpec.VendorWlanInterfaceIp.
                                  u4IpAddr);
                CAPWAP_PUT_4BYTE (pTxPkt,
                                  vendPld->unVendorSpec.VendorWlanInterfaceIp.
                                  u4SubMask);
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  vendPld->unVendorSpec.VendorWlanInterfaceIp.
                                  u1AdminStatus);
            }
            break;
#endif
        case VENDOR_DHCP_POOL:
        {
            CAPWAP_PUT_2BYTE (pTxPkt,
                              vendPld->unVendorSpec.DhcpPoolConfigUpdateReq.
                              u2MsgEleType);
            CAPWAP_PUT_2BYTE (pTxPkt,
                              vendPld->unVendorSpec.DhcpPoolConfigUpdateReq.
                              u2MsgEleLen);
            CAPWAP_PUT_4BYTE (pTxPkt,
                              vendPld->unVendorSpec.DhcpPoolConfigUpdateReq.
                              u4VendorId);
            CAPWAP_PUT_1BYTE (pTxPkt,
                              vendPld->unVendorSpec.DhcpPoolConfigUpdateReq.
                              u1ServerStatus);
            CAPWAP_PUT_2BYTE (pTxPkt,
                              vendPld->unVendorSpec.DhcpPoolConfigUpdateReq.
                              u2NoofPools);
            for (i2Index = 0;
                 i2Index <
                 vendPld->unVendorSpec.DhcpPoolConfigUpdateReq.u2NoofPools;
                 i2Index++)
            {
                CAPWAP_PUT_4BYTE (pTxPkt,
                                  vendPld->unVendorSpec.DhcpPoolConfigUpdateReq.
                                  u4DhcpPoolId[i2Index]);
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.DhcpPoolConfigUpdateReq.
                                  u2PoolStatus[i2Index]);
                CAPWAP_PUT_4BYTE (pTxPkt,
                                  vendPld->unVendorSpec.DhcpPoolConfigUpdateReq.
                                  u4Subnet[i2Index]);
                CAPWAP_PUT_4BYTE (pTxPkt,
                                  vendPld->unVendorSpec.DhcpPoolConfigUpdateReq.
                                  u4Mask[i2Index]);
                CAPWAP_PUT_4BYTE (pTxPkt,
                                  vendPld->unVendorSpec.DhcpPoolConfigUpdateReq.
                                  u4StartIp[i2Index]);
                CAPWAP_PUT_4BYTE (pTxPkt,
                                  vendPld->unVendorSpec.DhcpPoolConfigUpdateReq.
                                  u4EndIp[i2Index]);
                CAPWAP_PUT_4BYTE (pTxPkt,
                                  vendPld->unVendorSpec.DhcpPoolConfigUpdateReq.
                                  u4LeaseExprTime[i2Index]);
                CAPWAP_PUT_4BYTE (pTxPkt,
                                  vendPld->unVendorSpec.DhcpPoolConfigUpdateReq.
                                  u4DefaultIp[i2Index]);
                CAPWAP_PUT_4BYTE (pTxPkt,
                                  vendPld->unVendorSpec.DhcpPoolConfigUpdateReq.
                                  u4DnsServerIp[i2Index]);
            }
        }
            break;
        case VENDOR_DHCP_RELAY:
        {
            CAPWAP_PUT_2BYTE (pTxPkt,
                              vendPld->unVendorSpec.DhcpRelayConfigUpdateReq.
                              u2MsgEleType);
            CAPWAP_PUT_2BYTE (pTxPkt,
                              vendPld->unVendorSpec.DhcpRelayConfigUpdateReq.
                              u2MsgEleLen);
            CAPWAP_PUT_4BYTE (pTxPkt,
                              vendPld->unVendorSpec.DhcpRelayConfigUpdateReq.
                              u4VendorId);
            CAPWAP_PUT_1BYTE (pTxPkt,
                              vendPld->unVendorSpec.DhcpRelayConfigUpdateReq.
                              u1RelayStatus);
            CAPWAP_PUT_4BYTE (pTxPkt,
                              vendPld->unVendorSpec.DhcpRelayConfigUpdateReq.
                              u4NextIpAddress);
        }
            break;
        case VENDOR_FIREWALL_STATUS:
            if (vendPld->unVendorSpec.FireWallUpdateReq.b1IsOptional ==
                OSIX_TRUE)
            {
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.FireWallUpdateReq.
                                  u2MsgEleType);
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.FireWallUpdateReq.
                                  u2MsgEleLen);
                CAPWAP_PUT_4BYTE (pTxPkt,
                                  vendPld->unVendorSpec.FireWallUpdateReq.
                                  u4VendorId);
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  vendPld->unVendorSpec.FireWallUpdateReq.
                                  u1FirewallStatus);
            }
            break;
        case VENDOR_NAT_STATUS:
            if (vendPld->unVendorSpec.NatUpdateReq.b1IsOptional == OSIX_TRUE)
            {
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.NatUpdateReq.
                                  u2MsgEleType);
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.NatUpdateReq.
                                  u2MsgEleLen);
                CAPWAP_PUT_4BYTE (pTxPkt,
                                  vendPld->unVendorSpec.NatUpdateReq.
                                  u4VendorId);
                CAPWAP_PUT_4BYTE (pTxPkt,
                                  vendPld->unVendorSpec.NatUpdateReq.
                                  u4NATConfigIndex);
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.NatUpdateReq.i2WanType);
            }
            break;
        case VENDOR_ROUTE_TABLE:
            if (vendPld->unVendorSpec.RouteTableUpdateReq.b1IsOptional
                == OSIX_TRUE)
            {
                CAPWAP_PUT_2BYTE (pTxPkt, vendPld->
                                  unVendorSpec.RouteTableUpdateReq.
                                  u2MsgEleType);
                CAPWAP_PUT_2BYTE (pTxPkt, vendPld->
                                  unVendorSpec.RouteTableUpdateReq.u2MsgEleLen);
                CAPWAP_PUT_4BYTE (pTxPkt, vendPld->
                                  unVendorSpec.RouteTableUpdateReq.u4VendorId);
                CAPWAP_PUT_1BYTE (pTxPkt, vendPld->
                                  unVendorSpec.RouteTableUpdateReq.
                                  u1EntryStatus);
                CAPWAP_PUT_2BYTE (pTxPkt, vendPld->
                                  unVendorSpec.RouteTableUpdateReq.
                                  u2NoofRoutes);
                for (i2Index = 0; i2Index < vendPld->
                     unVendorSpec.RouteTableUpdateReq.u2NoofRoutes; i2Index++)
                {
                    CAPWAP_PUT_4BYTE (pTxPkt,
                                      vendPld->unVendorSpec.RouteTableUpdateReq.
                                      u4Subnet[i2Index]);
                    CAPWAP_PUT_4BYTE (pTxPkt,
                                      vendPld->unVendorSpec.RouteTableUpdateReq.
                                      u4NetMask[i2Index]);
                    CAPWAP_PUT_4BYTE (pTxPkt,
                                      vendPld->unVendorSpec.RouteTableUpdateReq.
                                      u4Gateway[i2Index]);
                }
            }
            break;
        case VENDOR_FIREWALL_FILTER_TABLE:
            if (vendPld->unVendorSpec.FirewallFilterUpdateReq.b1IsOptional ==
                OSIX_TRUE)
            {
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.FirewallFilterUpdateReq.
                                  u2MsgEleType);
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.FirewallFilterUpdateReq.
                                  u2MsgEleLen);
                CAPWAP_PUT_4BYTE (pTxPkt,
                                  vendPld->unVendorSpec.FirewallFilterUpdateReq.
                                  u4VendorId);
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  vendPld->unVendorSpec.FirewallFilterUpdateReq.
                                  u1EntryStatus);
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.FirewallFilterUpdateReq.
                                  u2NoOfFilters);
                CAPWAP_PUT_NBYTE (pTxPkt,
                                  vendPld->unVendorSpec.FirewallFilterUpdateReq.
                                  au1FilterName, 36);
                CAPWAP_PUT_NBYTE (pTxPkt,
                                  vendPld->unVendorSpec.FirewallFilterUpdateReq.
                                  au1SrcPort, 12);
                CAPWAP_PUT_NBYTE (pTxPkt,
                                  vendPld->unVendorSpec.FirewallFilterUpdateReq.
                                  au1DestPort, 12);
                CAPWAP_PUT_NBYTE (pTxPkt,
                                  vendPld->unVendorSpec.FirewallFilterUpdateReq.
                                  au1SrcAddr, 30);
                CAPWAP_PUT_NBYTE (pTxPkt,
                                  vendPld->unVendorSpec.FirewallFilterUpdateReq.
                                  au1DestAddr, 30);
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  vendPld->unVendorSpec.FirewallFilterUpdateReq.
                                  u1Proto);
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.FirewallFilterUpdateReq.
                                  u2AddrType);

            }
            break;
        case VENDOR_FIREWALL_ACL_TABLE:
            if (vendPld->unVendorSpec.FirewallAclUpdateReq.b1IsOptional ==
                OSIX_TRUE)
            {
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.FirewallAclUpdateReq.
                                  u2MsgEleType);
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.FirewallAclUpdateReq.
                                  u2MsgEleLen);
                CAPWAP_PUT_4BYTE (pTxPkt,
                                  vendPld->unVendorSpec.FirewallAclUpdateReq.
                                  u4VendorId);
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  vendPld->unVendorSpec.FirewallAclUpdateReq.
                                  u1EntryStatus);
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.FirewallAclUpdateReq.
                                  u2NoOfAcl);
                CAPWAP_PUT_NBYTE (pTxPkt,
                                  vendPld->unVendorSpec.FirewallAclUpdateReq.
                                  au1AclName, 36);
                CAPWAP_PUT_NBYTE (pTxPkt,
                                  vendPld->unVendorSpec.FirewallAclUpdateReq.
                                  au1FilterSet, 36);
                CAPWAP_PUT_4BYTE (pTxPkt,
                                  vendPld->unVendorSpec.FirewallAclUpdateReq.
                                  i4AclIfIndex);
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  vendPld->unVendorSpec.FirewallAclUpdateReq.
                                  u1AclDirection);
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.FirewallAclUpdateReq.
                                  u2SeqNum);
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  vendPld->unVendorSpec.FirewallAclUpdateReq.
                                  u1Action);

            }
            break;
#ifdef WLC_WANTED
        case VENDOR_CAPW_DIFF_SERV_TYPE:
            if (vendPld->unVendorSpec.VendorSpecDiffServ.isOptional ==
                OSIX_TRUE)
            {
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.VendorSpecDiffServ.
                                  u2MsgEleType);
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.VendorSpecDiffServ.
                                  u2MsgEleLen);
                CAPWAP_PUT_4BYTE (pTxPkt,
                                  vendPld->unVendorSpec.VendorSpecDiffServ.
                                  u4VendorId);
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  vendPld->unVendorSpec.VendorSpecDiffServ.
                                  u1RadioId);
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  vendPld->unVendorSpec.VendorSpecDiffServ.
                                  u1EntryStatus);
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  vendPld->unVendorSpec.VendorSpecDiffServ.
                                  u1InPriority);
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  vendPld->unVendorSpec.VendorSpecDiffServ.
                                  u1OutDscp);
            }
            break;
#endif
#ifdef PMF_DEBUG_WANTED
#ifdef WTP_WANTED
        case VENDOR_PMF_TYPE:
            if (vendPld->unVendorSpec.VendPMFPktNo.isOptional == OSIX_TRUE)
            {
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendPMFPktNo.u2MsgEleType);
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendPMFPktNo.u2MsgEleLen);
                CAPWAP_PUT_4BYTE (pTxPkt,
                                  vendPld->unVendorSpec.VendPMFPktNo.
                                  u4VendorId);
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  vendPld->unVendorSpec.VendPMFPktNo.
                                  RsnaIGTKSeqInfo[0].u1KeyIndex);
                CAPWAP_PUT_NBYTE (pTxPkt,
                                  vendPld->unVendorSpec.VendPMFPktNo.
                                  RsnaIGTKSeqInfo[0].au1IGTKPktNo, 6);
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  vendPld->unVendorSpec.VendPMFPktNo.
                                  RsnaIGTKSeqInfo[1].u1KeyIndex);
                CAPWAP_PUT_NBYTE (pTxPkt,
                                  vendPld->unVendorSpec.VendPMFPktNo.
                                  RsnaIGTKSeqInfo[1].au1IGTKPktNo, 6);

            }

            break;
#endif
#endif
        case VENDOR_L3SUBIF_TABLE:
            if (vendPld->unVendorSpec.L3SubIfUpdateReq.b1IsOptional
                == OSIX_TRUE)
            {
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.L3SubIfUpdateReq.
                                  u2MsgEleType);
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.L3SubIfUpdateReq.
                                  u2MsgEleLen);
                CAPWAP_PUT_4BYTE (pTxPkt,
                                  vendPld->unVendorSpec.L3SubIfUpdateReq.
                                  u4VendorId);
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  vendPld->unVendorSpec.L3SubIfUpdateReq.
                                  u1EntryStatus);
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.L3SubIfUpdateReq.
                                  u2NoOfIfaces);
                for (i2Index = 0;
                     i2Index <
                     vendPld->unVendorSpec.L3SubIfUpdateReq.u2NoOfIfaces;
                     i2Index++)
                {
                    CAPWAP_PUT_4BYTE (pTxPkt,
                                      vendPld->unVendorSpec.L3SubIfUpdateReq.
                                      u4PhyPort[i2Index]);
                    CAPWAP_PUT_4BYTE (pTxPkt,
                                      vendPld->unVendorSpec.L3SubIfUpdateReq.
                                      u4IpAddr[i2Index]);
                    CAPWAP_PUT_4BYTE (pTxPkt,
                                      vendPld->unVendorSpec.L3SubIfUpdateReq.
                                      u4SubnetMask[i2Index]);
                    CAPWAP_PUT_2BYTE (pTxPkt,
                                      vendPld->unVendorSpec.L3SubIfUpdateReq.
                                      u2VlanId[i2Index]);
                    CAPWAP_PUT_1BYTE (pTxPkt,
                                      vendPld->unVendorSpec.L3SubIfUpdateReq.
                                      u1IfNwType[i2Index]);
                    CAPWAP_PUT_1BYTE (pTxPkt,
                                      vendPld->unVendorSpec.L3SubIfUpdateReq.
                                      u1IfAdminStatus[i2Index]);
                }
            }
            break;

        case VENDOR_DOT11N_CONFIG_TYPE:
            if (vendPld->unVendorSpec.VendorDot11nCfg.isOptional == OSIX_TRUE)
            {
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendorDot11nCfg.u2MsgEleType);
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendorDot11nCfg.u2MsgEleLen);
                CAPWAP_PUT_4BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendorDot11nCfg.u4VendorId);
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  vendPld->unVendorSpec.VendorDot11nCfg.
                                  u1RadioId);
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  vendPld->unVendorSpec.VendorDot11nCfg.
                                  u1AMPDUStatus);
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  vendPld->unVendorSpec.VendorDot11nCfg.
                                  u1AMPDUSubFrame);
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.VendorDot11nCfg.
                                  u2AMPDULimit);
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  vendPld->unVendorSpec.VendorDot11nCfg.
                                  u1AMSDUStatus);
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.VendorDot11nCfg.
                                  u2AMSDULimit);
            }
            break;

#ifdef BAND_SELECT_WANTED
        case VENDOR_BANDSELECT_TYPE:

            if (vendPld->unVendorSpec.VendorBandSelect.isOptional == OSIX_TRUE)
            {
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendorBandSelect.u2MsgEleType);
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendorBandSelect.u2MsgEleLen);
                CAPWAP_PUT_4BYTE (pTxPkt,
                                  vendPld->unVendorSpec.VendorBandSelect.
                                  u4VendorId);
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  vendPld->unVendorSpec.VendorBandSelect.
                                  u1RadioId);
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  vendPld->unVendorSpec.VendorBandSelect.
                                  u1WlanId);
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  vendPld->unVendorSpec.VendorBandSelect.
                                  u1BandSelectGlobalStatus);
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  vendPld->unVendorSpec.VendorBandSelect.
                                  u1BandSelectStatus);
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  vendPld->unVendorSpec.
                                  VendorBandSelect.u1AgeOutSuppression);
            }
            break;

#endif
        default:
            CAPWAP_TRC (CAPWAP_MGMT_TRC, "Invalid Msg Type\r\n");
            SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                          "Invalid Msg Type\r\n"));

            break;

    }
    CAPWAP_FN_EXIT ();
    return;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : CapwapAssembleVendorSpecificPayloadWTPEventRequest         *
         *                                                                           *
         * Description  : Assemble the capwap Control packet structure to            *
         *                transmittable  format.                                     *
         *                                                                           *
         * Input        : Capwap Control message structure                           *
         *                                                                           *
         * Output       : Capwap control message to be transmitted out               *
         *                                                                           *
         * Returns      : SUCCESS/FAILURE                                            *
         *****************************************************************************/
VOID
CapwapAssembleVendorSpecificPayloadWTPEventRequest (UINT1 *pTxPkt,
                                                    tVendorSpecPayload *
                                                    vendPld,
                                                    tWtpCapwapSessStatsElement *
                                                    capwapStatsElement,
                                                    tBssIDMacHdlrStatsElement *
                                                    MacHdlrStatsElement,
                                                    tRadioClientStats *
                                                    RadioClientStatsElement,
                                                    tApParamsStats *
                                                    ApParamsElement)
{
    UINT1               u1Index = 0;
    UINT1               u1Index1 = 0;
    UINT1               u1Index2 = 0;
    tMacAddr            BssId;
    /* Copy the message type and length */
    vendPld->u2MsgEleType = 37;
    CAPWAP_PUT_2BYTE (pTxPkt, vendPld->u2MsgEleType);
    vendPld->u2MsgEleLen =
        (UINT2) (vendPld->u2MsgEleLen + capwapStatsElement->u2MsgEleLen);
    if (MacHdlrStatsElement->isOptional)
    {
        vendPld->u2MsgEleLen = (UINT2) (vendPld->u2MsgEleLen +
                                        MacHdlrStatsElement->u2MsgEleLen);
    }
    CAPWAP_PUT_2BYTE (pTxPkt, vendPld->u2MsgEleLen);

    /* Vendor id */
    vendPld->vendorId = 0x006e006e;
    vendPld->elementId = VENDOR_SPECIFIC_MSG + VENDOR_BSSID_STATS_TYPE;

    CAPWAP_PUT_2BYTE (pTxPkt, vendPld->elementId);
    CAPWAP_PUT_4BYTE (pTxPkt, vendPld->vendorId);

    if (capwapStatsElement->isOptional)
    {
        CAPWAP_PUT_NBYTE (pTxPkt, capwapStatsElement->capwapStats,
                          sizeof (tWtpCPWPWtpStats));
    }
    if (MacHdlrStatsElement->isOptional)
    {
        CAPWAP_PUT_1BYTE (pTxPkt, MacHdlrStatsElement->numBssCount);
        for (u1Index = 0; u1Index < (MacHdlrStatsElement->numBssCount);
             u1Index++)
        {
            CAPWAP_PUT_NBYTE (pTxPkt, MacHdlrStatsElement->BssId[u1Index],
                              MAC_ADDR_LEN);
            MEMCPY (&BssId, &MacHdlrStatsElement->BssId[u1Index], MAC_ADDR_LEN);
            CAPWAP_PUT_4BYTE (pTxPkt,
                              MacHdlrStatsElement->BSSIDMacHndlrStats
                              [u1Index].wlanBSSIDBeaconsSentCount);
            CAPWAP_PUT_4BYTE (pTxPkt,
                              MacHdlrStatsElement->
                              BSSIDMacHndlrStats
                              [u1Index].wlanBSSIDProbeReqRcvdCount);
            CAPWAP_PUT_4BYTE (pTxPkt,
                              MacHdlrStatsElement->
                              BSSIDMacHndlrStats
                              [u1Index].wlanBSSIDProbeRespSentCount);
            CAPWAP_PUT_4BYTE (pTxPkt,
                              MacHdlrStatsElement->
                              BSSIDMacHndlrStats
                              [u1Index].wlanBSSIDDataPktRcvdCount);
            CAPWAP_PUT_4BYTE (pTxPkt,
                              MacHdlrStatsElement->
                              BSSIDMacHndlrStats
                              [u1Index].wlanBSSIDDataPktSentCount);
        }
    }
    if (ApParamsElement->isOptional)
    {
        CAPWAP_PUT_4BYTE (pTxPkt, ApParamsElement->ApElement.u4GatewayIp);
        CAPWAP_PUT_4BYTE (pTxPkt, ApParamsElement->ApElement.u4Subnetmask);
        CAPWAP_PUT_4BYTE (pTxPkt, ApParamsElement->ApElement.u4SentBytes);
        CAPWAP_PUT_4BYTE (pTxPkt, ApParamsElement->ApElement.u4RcvdBytes);
        CAPWAP_PUT_4BYTE (pTxPkt, ApParamsElement->ApElement.u4SentTrafficRate);
        CAPWAP_PUT_4BYTE (pTxPkt, ApParamsElement->ApElement.u4RcvdTrafficRate);
        CAPWAP_PUT_1BYTE (pTxPkt, ApParamsElement->ApElement.u1TotalInterfaces);
    }
    if (RadioClientStatsElement->isOptional)
    {
        CAPWAP_PUT_1BYTE (pTxPkt, RadioClientStatsElement->numRadioCount);
        for (u1Index1 = 0; u1Index1 < (RadioClientStatsElement->numRadioCount);
             u1Index1++)
        {
            CAPWAP_PUT_4BYTE (pTxPkt,
                              RadioClientStatsElement->radioClientStats
                              [u1Index1].u4IfInOctets);
            CAPWAP_PUT_4BYTE (pTxPkt,
                              RadioClientStatsElement->radioClientStats
                              [u1Index1].u4IfOutOctets);
        }

        CAPWAP_PUT_1BYTE (pTxPkt, RadioClientStatsElement->numStaCount);
        for (u1Index2 = 0; u1Index2 < (RadioClientStatsElement->numStaCount);
             u1Index2++)
        {

            CAPWAP_PUT_NBYTE (pTxPkt,
                              RadioClientStatsElement->
                              clientStats[u1Index2].StaMacAddr, MAC_ADDR_LEN);
            CAPWAP_PUT_4BYTE (pTxPkt,
                              RadioClientStatsElement->
                              clientStats
                              [u1Index2].u4ClientStatsBytesSentCount);
            CAPWAP_PUT_4BYTE (pTxPkt,
                              RadioClientStatsElement->
                              clientStats
                              [u1Index2].u4ClientStatsBytesRecvdCount);
        }
    }
    return;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : capwapAssembleDiscoveryType                                *
         *                                                                           *
         * Description  : Assemble the capwap Discovery Type message element         *
         *                                                                           *
         * Input        : tWtpDiscType : Discovery Type information                  *
         *                                                                           *
         * Output       : Capwap message element to be transmitted out               *
         *                                                                           *
         * Returns      : NONE                                                       *
         *****************************************************************************/
VOID
capwapAssembleDiscoveryType (UINT1 *pTxPkt, tWtpDiscType * discType)
{

    /* Copy the message type and length */
    CAPWAP_PUT_2BYTE (pTxPkt, discType->u2MsgEleType);
    CAPWAP_PUT_2BYTE (pTxPkt, discType->u2MsgEleLen);
    CAPWAP_PUT_1BYTE (pTxPkt, discType->u1DiscType);
    return;

}

        /*****************************************************************************
         *                                                                           *
         * Function     : capwapAssembleMTUDiscoveryPadding                          *
         *                                                                           *
         * Description  : Assemble the MTU Discovery Padding message element         *
         *                                                                           *
         * Input        : tMtuDiscPad : MTU Dicovery Padding message element         *
         *                                                                           *
         * Output       : Capwap MTU Dicovery Padding message element                *
         *                                                                           *
         * Returns      : NONE                                                       *
         *****************************************************************************/
VOID
capwapAssembleMTUDiscoveryPadding (UINT1 *pTxPkt, tMtuDiscPad * discPadding)
{
    INT2                i2Index = 0;
    /* Copy the message type and length */
    CAPWAP_PUT_2BYTE (pTxPkt, discPadding->u2MsgEleType);
    CAPWAP_PUT_2BYTE (pTxPkt, discPadding->u2MsgEleLen);
    for (i2Index = 0; i2Index < discPadding->u2MsgEleLen; i2Index++)
    {
        CAPWAP_PUT_1BYTE (pTxPkt, 0xFF);
    }

    return;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : capwapAssembleAcDescriptor                                 *
         *                                                                           *
         * Description  : Assemble the AC Descriptor message element                 *
         *                                                                           *
         * Input        : tAcDescriptor : AC Descriptor message element              *
         *                                                                           *
         * Output       : Capwap message element to be transmitted out               *
         *                                                                           *
         * Returns      : NONE                                                       *
         *****************************************************************************/
VOID
capwapAssembleAcDescriptor (UINT1 *pTxPkt, tAcDescriptor * acDesc)
{
    UINT1               u1Index = 0;

    /* Copy the message type and length */
    CAPWAP_PUT_2BYTE (pTxPkt, acDesc->u2MsgEleType);
    CAPWAP_PUT_2BYTE (pTxPkt, acDesc->u2MsgEleLen);
    CAPWAP_PUT_2BYTE (pTxPkt, acDesc->stations);
    CAPWAP_PUT_2BYTE (pTxPkt, acDesc->stationLimit);
    CAPWAP_PUT_2BYTE (pTxPkt, acDesc->activeWtp);
    CAPWAP_PUT_2BYTE (pTxPkt, acDesc->maxWtp);
    CAPWAP_PUT_1BYTE (pTxPkt, acDesc->security);
    CAPWAP_PUT_1BYTE (pTxPkt, acDesc->radioMacField);
    CAPWAP_PUT_1BYTE (pTxPkt, acDesc->resvd);
    CAPWAP_PUT_1BYTE (pTxPkt, acDesc->dtlsPolicy);
    /* CAPWAP_PUT_4BYTE(pTxPkt, acDesc->ACVendorId); */

    /* Copy the vendor info in the following loop */
    while ((acDesc->u4VendorInfoCount > 0)
           && (u1Index < CAPWAP_MAX_WLC_DESCRIPTOR_TYPE))
    {
        if (acDesc->VendDesc[u1Index].u2VendorInfoLength != 0)
        {
            CAPWAP_PUT_4BYTE (pTxPkt, acDesc->ACVendorId);
            CAPWAP_PUT_2BYTE (pTxPkt, acDesc->VendDesc[u1Index].vendorInfoType);
            CAPWAP_PUT_2BYTE (pTxPkt,
                              acDesc->VendDesc[u1Index].u2VendorInfoLength);
            CAPWAP_PUT_NBYTE (pTxPkt,
                              acDesc->VendDesc[u1Index].vendorInfoData,
                              acDesc->VendDesc[u1Index].u2VendorInfoLength);
            acDesc->u4VendorInfoCount--;
        }
        u1Index++;
    }

    return;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : CapwapAssembleAcName                                       *
         *                                                                           *
         * Description  : Assemble the AC Name message element                       *
         *                                                                           *
         * Input        : tACName  : AC Name information                             *
         *                                                                           *
         * Output       : Capwap message element to be transmitted out               *
         *                                                                           *
         * Returns      : NONE                                                       *
         *****************************************************************************/
VOID
CapwapAssembleAcName (UINT1 *pTxPkt, tACName * acName)
{
    /* Copy the message type and length */
    UINT2               u2Len;
    CAPWAP_PUT_2BYTE (pTxPkt, acName->u2MsgEleType);
    CAPWAP_PUT_2BYTE (pTxPkt, acName->u2MsgEleLen);
    u2Len = (UINT2) (STRLEN (acName->wlcName));
    CAPWAP_PUT_NBYTE (pTxPkt, acName->wlcName, u2Len);
    return;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : capwapAssembleControlIPAddress                             *
         *                                                                           *
         * Description  : Assemble the Control IP Address message element            *
         *                                                                           *
         * Input        : tCtrlIpAddr : Control IP Address                           *
         *                                                                           *
         * Output       : Capwap message element to be transmitted out               *
         *                                                                           *
         * Returns      : NONE                                                       *
         *****************************************************************************/
VOID
capwapAssembleControlIPAddress (UINT1 *pTxPkt, tCtrlIpAddr * ipAddr)
{
    /* Copy the message type and length */
    CAPWAP_PUT_2BYTE (pTxPkt, ipAddr->u2MsgEleType);
    CAPWAP_PUT_2BYTE (pTxPkt, ipAddr->u2MsgEleLen);
    if (ipAddr->u2MsgEleType == CAPWAP_CTRL_IPV4_ADDR)
    {
        CAPWAP_PUT_4BYTE (pTxPkt, ipAddr->ipAddr.u4_addr[0]);
    }
    else
    {                            /* Copying ipv6 address should be verified */
        CAPWAP_PUT_NBYTE (pTxPkt, ipAddr->ipAddr.u1_addr[0],
                          ipAddr->u2MsgEleLen);
    }
    CAPWAP_PUT_2BYTE (pTxPkt, ipAddr->wtpCount);
    return;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : capwapAssembleResultCode                                   *
         *                                                                           *
         * Description  : Assemble the capwap Result Code message element            *
         *                                                                           *
         * Input        : tResCode Result Code information                           *
         *                                                                           *
         * Output       : Capwap message element to be transmitted out               *
         *                                                                           *
         * Returns      : NONE                                                       *
         *****************************************************************************/
VOID
capwapAssembleResultCode (UINT1 *pTxPkt, tResCode * resultCode)
{
    CAPWAP_FN_ENTRY ();
    /* Copy the message type and length */
    CAPWAP_PUT_2BYTE (pTxPkt, resultCode->u2MsgEleType);
    CAPWAP_PUT_2BYTE (pTxPkt, resultCode->u2MsgEleLen);
    CAPWAP_PUT_4BYTE (pTxPkt, resultCode->u4Value);
    CAPWAP_FN_EXIT ();
    return;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : capwapAssembleIpv4List                                     *
         *                                                                           *
         * Description  : Assemble the IPv4 List message element                     *
         *                                                                           *
         * Input        : tACIplist IPv4 List                                        *
         *                                                                           *
         * Output       : Capwap message element to be transmitted out               *
         *                                                                           *
         * Returns      : NONE                                                       *
         *****************************************************************************/
VOID
capwapAssembleIpv4List (UINT1 *pTxPkt, tACIplist * pIpv4List)
{
    UINT1               u1Index = 0;
    UINT1               u1NumOfIP =
        (UINT1) ((pIpv4List->u2MsgEleLen) / sizeof (UINT4));
    /* Copy the message type and length */
    CAPWAP_PUT_2BYTE (pTxPkt, pIpv4List->u2MsgEleType);
    CAPWAP_PUT_2BYTE (pTxPkt, pIpv4List->u2MsgEleLen);

    for (u1Index = 0; u1Index < u1NumOfIP; u1Index++)
    {
        CAPWAP_PUT_4BYTE (pTxPkt, pIpv4List->ipAddr[u1Index].u4_addr[0]);
    }
    return;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : capwapAssembleIpv6List                                     *
         *                                                                           *
         * Description  : Assemble the capwap IPV6 IP List message element           *
         *                                                                           *
         * Input        : Capwap Control message structure                           *
         *                                                                           *
         * Output       : Capwap control message to be transmitted out               *
         *                                                                           *
         * Returns      : NONE                                                       *
         *****************************************************************************/
VOID
capwapAssembleIpv6List (UINT1 *pTxPkt, tACIplist * ipv6List)
{
    UINT1               u1Index = 0;
    UINT1               u1NumOfIP =
        (UINT1) ((ipv6List->u2MsgEleLen) / sizeof (ipv6List_LEN));

    CAPWAP_PUT_2BYTE (pTxPkt, ipv6List->u2MsgEleType);
    CAPWAP_PUT_2BYTE (pTxPkt, ipv6List->u2MsgEleLen);

    for (u1Index = 0; u1Index < u1NumOfIP; u1Index++)
    {
        CAPWAP_PUT_NBYTE (pTxPkt,
                          ipv6List->ipAddr[u1Index].u4_addr[0],
                          sizeof (ipv6List_LEN));
    }
    return;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : capwapAssembleImageIdentifier                              *
         *                                                                           *
         * Description  : Assemble the ImageIdentifier message element               *
         *                                                                           *
         * Input        : Capwap Control message structure                           *
         *                                                                           *
         * Output       : Capwap control message to be transmitted out               *
         *                                                                           *
         * Returns      : NONE                                                       *
         *****************************************************************************/
VOID
capwapAssembleImageIdentifier (UINT1 *pTxPkt, tImageId * pImageId)
{
    UINT2               dataLen = 0;
    /* Copy the message type and length */
    CAPWAP_PUT_2BYTE (pTxPkt, pImageId->u2MsgEleType);
    CAPWAP_PUT_2BYTE (pTxPkt, pImageId->u2MsgEleLen);
    CAPWAP_PUT_4BYTE (pTxPkt, pImageId->u4VendorId);

    dataLen = (UINT2) ((pImageId->u2MsgEleLen) - CAPWAP_MSG_ELEM_TYPE_LEN);

    CAPWAP_PUT_NBYTE (pTxPkt, pImageId->data, dataLen);
    return;

}

        /*****************************************************************************
         *                                                                           *
         * Function     : CapwapAssembleImageInformation                             *
         *                                                                           *
         * Description  : Assemble the Image information message element             *
         *                transmittable  format.                                     *
         *                                                                           *
         * Input        : Capwap Control message structure                           *
         *                                                                           *
         * Output       : Capwap control message to be transmitted out               *
         *                                                                           *
         * Returns      : NONE                                                       *
         *****************************************************************************/
VOID
CapwapAssembleImageInformation (UINT1 *pTxPkt, tImageInfo * pImageInfo)
{
    CAPWAP_PUT_2BYTE (pTxPkt, pImageInfo->u2MsgEleType);
    CAPWAP_PUT_2BYTE (pTxPkt, pImageInfo->u2MsgEleLen);
    CAPWAP_PUT_4BYTE (pTxPkt, pImageInfo->u4FileSize);
    CAPWAP_PUT_NBYTE (pTxPkt, pImageInfo->Checksum, IMAGE_DATA_CHECKSUM_LEN);

    return;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : capwapAssembleImageData                                    *
         *                                                                           *
         * Description  : Assemble the capwap Control packet structure to            *
         *                transmittable  format.                                     *
         *                                                                           *
         * Input        : Capwap Control message structure                           *
         *                                                                           *
         * Output       : Capwap control message to be transmitted out               *
         *                                                                           *
         * Returns      : NONE                                                       *  
         *****************************************************************************/
VOID
capwapAssembleImageData (UINT1 *pTxPkt, tImageData * pImageData)
{
    CAPWAP_PUT_2BYTE (pTxPkt, pImageData->u2MsgEleType);
    CAPWAP_PUT_2BYTE (pTxPkt, pImageData->u2MsgEleLen);
    CAPWAP_PUT_1BYTE (pTxPkt, pImageData->u1DataType);
    CAPWAP_PUT_NBYTE (pTxPkt, pImageData->au1ImageData,
                      pImageData->u2MsgEleLen);
    return;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : capwapAssembleInitiateDownload                             *
         *                                                                           *
         * Description  : Assemble the capwap InitiateDownload message element       *
         *                                                                           *
         * Input        : tImageIntiateDownload : Initiate Download information      *
         *                                                                           *
         * Output       : Capwap control message to be transmitted out               *
         *                                                                           *
         * Returns      : NONE                                                       *
         *****************************************************************************/
VOID
capwapAssembleInitiateDownload (UINT1 *pTxPkt,
                                tImageIntiateDownload * imageInitiateDownload)
{
    /* Copy the message type and length */
    CAPWAP_PUT_2BYTE (pTxPkt, imageInitiateDownload->u2MsgEleType);
    CAPWAP_PUT_2BYTE (pTxPkt, imageInitiateDownload->u2MsgEleLen);
    return;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : capwapAssembleRadioAdminState                              *
         *                                                                           *
         * Description  : Assemble the capwap Radio Admin State message element      *
         *                                                                           *
         * Input        : tRadioIfAdminStatus : Radio Admin information              *
         *                                                                           *
         * Output       : Capwap message element to be transmitted out               *
         *                                                                           *
         * Returns      : NONE                                                       *
         *****************************************************************************/
VOID
capwapAssembleRadioAdminState (UINT1 *pTxPkt,
                               tRadioIfAdminStatus * radioAdminState)
{
    /* Copy the message type and length */
    CAPWAP_PUT_2BYTE (pTxPkt, radioAdminState->u2MessageType);
    CAPWAP_PUT_2BYTE (pTxPkt, radioAdminState->u2MessageLength);
    CAPWAP_PUT_1BYTE (pTxPkt, radioAdminState->u1RadioId);
    CAPWAP_PUT_1BYTE (pTxPkt, radioAdminState->u1AdminStatus);
    return;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : capwapAssembleDot11nParams                                 *
         *                                                                           *
         * Description  : Assemble the DOT11n Vendor Specific message element        *
         *                                                                           *
         * Input        : tRadioIfDot11nParam : DOT11n parameters                    *
         *                                                                           *
         * Output       : Capwap message element to be transmitted out               *
         *                                                                           *
         * Returns      : NONE                                                       *
         *****************************************************************************/
VOID
capwapAssembleDot11nParams (UINT1 *pTxPkt,
                            tRadioIfDot11nParam * radioIfDot11nParam)
{
    tVendorSpecPayload  vendorSpecificPayload;
    tDot11nVendorType   dot11nVendor;
    CAPWAP_MEMSET (&vendorSpecificPayload, CAPWAP_INIT_VAL,
                   sizeof (tVendorSpecPayload));
    CAPWAP_MEMSET (&dot11nVendor, CAPWAP_INIT_VAL, sizeof (tDot11nVendorType));

    vendorSpecificPayload.u2MsgEleType = VENDOR_SPECIFIC_PAYLOAD;
    vendorSpecificPayload.u2MsgEleLen =
        DOT11N_VENDOR_MSG_LEN + CAPWAP_MSG_ELEM_TYPE_LEN;
    dot11nVendor.isOptional = OSIX_TRUE;
    dot11nVendor.u2MsgEleType = radioIfDot11nParam->u2MessageType;
    dot11nVendor.u2MsgEleLen = DOT11N_VENDOR_MSG_LEN;
    dot11nVendor.u4VendorId = radioIfDot11nParam->u4VendorId;
    dot11nVendor.u1RadioId = radioIfDot11nParam->u1RadioId;
    dot11nVendor.u1HtFlag = radioIfDot11nParam->u1HtFlag;
    dot11nVendor.u1MaxSuppMCS = radioIfDot11nParam->u1MaxSuppMCS;
    dot11nVendor.u1MaxManMCS = radioIfDot11nParam->u1MaxManMCS;
    dot11nVendor.u1TxAntenna = radioIfDot11nParam->u1TxAntenna;
    dot11nVendor.u1RxAntenna = radioIfDot11nParam->u1RxAntenna;
    dot11nVendor.u2Reserved = radioIfDot11nParam->u2Reserved;

    /* The element id needs to be set in order to identify the 
     * specific type of vendor message */
    vendorSpecificPayload.elementId = VENDOR_DOT11N_TYPE;
    vendorSpecificPayload.isOptional = OSIX_TRUE;
    CAPWAP_MEMCPY (&(vendorSpecificPayload.unVendorSpec.Dot11nVendor),
                   &dot11nVendor, sizeof (tDot11nVendorType));

    /*Invoke vendor specific payload assemble function */
    CapwapAssembleVendorSpecificPayload (pTxPkt, &vendorSpecificPayload);

    return;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : CapwapAssembleStatisticsTimer                              *
         *                                                                           *
         * Description  : Assemble the capwap Statistics Timer message element       *
         *                                                                           *
         * Input        : tStatisticsTimer : Statistics timer information            *
         *                                                                           *
         * Output       : Capwap message element to be transmitted out               *
         *                                                                           *
         * Returns      : NONE                                                       *
         *****************************************************************************/
VOID
CapwapAssembleStatisticsTimer (UINT1 *pTxPkt, tStatisticsTimer * statsTimer)
{
    /* Copy the message type and length */
    CAPWAP_PUT_2BYTE (pTxPkt, statsTimer->u2MsgEleType);
    CAPWAP_PUT_2BYTE (pTxPkt, statsTimer->u2MsgEleLen);
    CAPWAP_PUT_2BYTE (pTxPkt, statsTimer->u2StatsTimer);
    return;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : capwapAssembleAcnameWithPriority                           *
         *                                                                           *
         * Description  : Assemble the capwap AC Name with Priority message element  *
         *                                                                           *
         * Input        : tACNameWithPrio  : AC Name and priority information        *
         *                                                                           *
         * Output       : Capwap message element to be transmitted out               *
         *                                                                           *
         * Returns      : NONE                                                       *
         *****************************************************************************/
VOID
capwapAssembleAcnameWithPriority (UINT1 *pTxPkt,
                                  tACNameWithPrio * acNameWithPrio)
{
    /* Copy the message type and length */
    CAPWAP_PUT_2BYTE (pTxPkt, acNameWithPrio->u2MsgEleType);
    CAPWAP_PUT_2BYTE (pTxPkt, acNameWithPrio->u2MsgEleLen);
    CAPWAP_PUT_1BYTE (pTxPkt, acNameWithPrio->u1Priority);
    CAPWAP_PUT_NBYTE (pTxPkt, acNameWithPrio->wlcName,
                      acNameWithPrio->u2MsgEleLen);
    return;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : icapwapAssembleWtpStaticIpAddress                          *
         *                                                                           *
         * Description  : Assemble the capwap WTP Static IP address message element  *
         *                                                                           *
         * Input        : tWtpStaticIpAddr : WTP IP address information              *
         *                                                                           *
         * Output       : Capwap message element to be transmitted out               *
         *                                                                           *
         * Returns      : NONE                                                       *
         *****************************************************************************/
VOID
capwapAssembleWtpStaticIpAddress (UINT1 *pTxPkt,
                                  tWtpStaticIpAddr * wtpStaticIpAddr)
{
    /* Copy the message type and length */
    CAPWAP_PUT_2BYTE (pTxPkt, wtpStaticIpAddr->u2MsgEleType);
    CAPWAP_PUT_2BYTE (pTxPkt, wtpStaticIpAddr->u2MsgEleLen);
    CAPWAP_PUT_4BYTE (pTxPkt, wtpStaticIpAddr->u4IpAddr);
    CAPWAP_PUT_4BYTE (pTxPkt, wtpStaticIpAddr->u4IpNetMask);
    CAPWAP_PUT_4BYTE (pTxPkt, wtpStaticIpAddr->u4IpGateway);
    CAPWAP_PUT_1BYTE (pTxPkt, (UINT1) wtpStaticIpAddr->bStaticIpBool);
    return;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : capwapAssembleCapwapTimer                                  *
         *                                                                           *
         * Description  : Assemble the capwap timer message element                  *
         *                                                                           *
         * Input        : tCapwapTimer : CAPWAP Timer information                    *
         *                                                                           *
         * Output       : Capwap message element to be transmitted out               *
         *                                                                           *
         * Returns      : NONE                                                       *
         *****************************************************************************/
VOID
capwapAssembleCapwapTimer (UINT1 *pTxPkt, tCapwapTimer * capwapTimer)
{
    /* Copy the message type and length */
    CAPWAP_PUT_2BYTE (pTxPkt, capwapTimer->u2MsgEleType);
    CAPWAP_PUT_2BYTE (pTxPkt, capwapTimer->u2MsgEleLen);
    CAPWAP_PUT_1BYTE (pTxPkt, capwapTimer->u1Discovery);
    CAPWAP_PUT_1BYTE (pTxPkt, capwapTimer->u1EchoRequest);
    return;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : capwapAssembleDecryptionErrorReport                        *
         *                                                                           *
         * Description  : Assemble the capwap DecryptionErrorReport                  *
         *                message element                                            *
         *                                                                           *
         * Input        : tDecryptErrReportPeriod Capwap message element information *
         *                                                                           *
         * Output       : Capwap message element to be transmitted out               *
         *                                                                           *
         * Returns      : NONE                                                       *
         *****************************************************************************/
VOID
capwapAssembleDecryptionErrorReport (UINT1 *pTxPkt,
                                     tDecryptErrReportPeriod * decryptPeriod)
{
    /* Copy the message type and length */
    CAPWAP_PUT_2BYTE (pTxPkt, decryptPeriod->u2MsgEleType);
    CAPWAP_PUT_2BYTE (pTxPkt, decryptPeriod->u2MsgEleLen);
    CAPWAP_PUT_1BYTE (pTxPkt, decryptPeriod->u1RadioId);
    CAPWAP_PUT_2BYTE (pTxPkt, decryptPeriod->u2ReportInterval);
    return;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : capwapAssembleIdleTimeout                                  *
         *                                                                           *
         * Description  : Assemble the capwap IdleTimeout message element            *
         *                                                                           *
         * Input        : tIdleTimeout : idle timeout information                    *
         *                                                                           *
         * Output       : Capwap messagei element to be transmitted out              *
         *                                                                           *
         * Returns      : NONE                                                       *
         *****************************************************************************/
VOID
capwapAssembleIdleTimeout (UINT1 *pTxPkt, tIdleTimeout * idleTimeout)
{
    /* Copy the message type and length */
    CAPWAP_PUT_2BYTE (pTxPkt, idleTimeout->u2MsgEleType);
    CAPWAP_PUT_2BYTE (pTxPkt, idleTimeout->u2MsgEleLen);
    CAPWAP_PUT_4BYTE (pTxPkt, idleTimeout->u4Timeout);
    return;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : capwapAssembleWtpFallback                                  *
         *                                                                           *
         * Description  : Assemble the capwap WtpFallback message element            *
         *                                                                           *
         * Input        : tWtpFallback : WTP Fallback configuration                  *
         *                                                                           *
         * Output       : Capwap message element to be transmitted out               *
         *                                                                           *
         * Returns      : NONE                                                       *
         *****************************************************************************/
VOID
capwapAssembleWtpFallback (UINT1 *pTxPkt, tWtpFallback * wtpFallback)
{
    /* Copy the message type and length */
    CAPWAP_PUT_2BYTE (pTxPkt, wtpFallback->u2MsgEleType);
    CAPWAP_PUT_2BYTE (pTxPkt, wtpFallback->u2MsgEleLen);
    CAPWAP_PUT_1BYTE (pTxPkt, wtpFallback->mode);
    return;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : capwapAssembleRadioOperState                               *
         *                                                                           *
         * Description  : Assemble the capwap Radio OperState message element        *
         *                                                                           *
         * Input        : tRadioIfOperStatus : Radio OperStatus information          *
         *                                                                           *
         * Output       : Capwap message element to be transmitted out               *
         *                                                                           *
         * Returns      : NONE                                                       *
         *****************************************************************************/
VOID
capwapAssembleRadioOperState (UINT1 *pTxPkt, tRadioIfOperStatus * radOperState)
{
    /* Get the number of radios and update accordingly */
    CAPWAP_PUT_2BYTE (pTxPkt, radOperState->u2MessageType);
    CAPWAP_PUT_2BYTE (pTxPkt, radOperState->u2MessageLength);
    CAPWAP_PUT_1BYTE (pTxPkt, radOperState->u1RadioId);
    CAPWAP_PUT_1BYTE (pTxPkt, radOperState->u1OperStatus);
    CAPWAP_PUT_1BYTE (pTxPkt, radOperState->u1FailureCause);
    return;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : capwapAssembleDeleteMacEntry                               *
         *                                                                           *
         * Description  : Assemble the capwap DeleteMacEntry message element         *
         *                transmittable  format.                                     *
         *                                                                           *
         * Input        : Capwap Control message structure                           *
         *                                                                           *
         * Output       : Capwap control message to be transmitted out               *
         *                                                                           *
         * Returns      : NONE                                                       *
         *****************************************************************************/
VOID
capwapAssembleDeleteMacEntry (UINT1 *txPkt, tDeleteMacAclEntry * pDeleteMac)
{
    /* Copy the message type and length */
    CAPWAP_PUT_2BYTE (txPkt, pDeleteMac->u2MsgEleType);
    CAPWAP_PUT_2BYTE (txPkt, pDeleteMac->u2MsgEleLen);
    CAPWAP_PUT_1BYTE (txPkt, pDeleteMac->u1NumEntries);
    CAPWAP_PUT_1BYTE (txPkt, pDeleteMac->u1Length);
    CAPWAP_PUT_2BYTE (txPkt, pDeleteMac->u2MacAddr);
    return;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : capwapAssembleAddMacEntry                                  *
         *                                                                           *
         * Description  : Assemble the capwap AddMacEntry message element            *
         *                                                                           *
         * Input        : Capwap Control message structure                           *
         *                                                                           *
         * Output       : Capwap control message to be transmitted out               *
         *                                                                           *
         * Returns      : NONE                                                       *
         *****************************************************************************/
VOID
capwapAssembleAddMacEntry (UINT1 *txPkt, tAddMacAclEntry * pAddMac)
{
    /* Copy the message type and length */
    CAPWAP_PUT_2BYTE (txPkt, pAddMac->u2MsgEleType);
    CAPWAP_PUT_2BYTE (txPkt, pAddMac->u2MsgEleLen);
    CAPWAP_PUT_1BYTE (txPkt, pAddMac->u1NumEntries);
    CAPWAP_PUT_1BYTE (txPkt, pAddMac->u1Length);
    CAPWAP_PUT_2BYTE (txPkt, pAddMac->u2MacAddr);
    return;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : capwapAssembleAcTimestamp                                  *
         *                                                                           *
         * Description  : Assemble the capwap AcTimeStamp Message Element            *
         *                                                                           *
         * Input        : Capwap Control message structure                           *
         *                                                                           *
         * Output       : Capwap control message to be transmitted out               *
         *                                                                           *
         * Returns      : NONE                                                       *
         *****************************************************************************/
VOID
capwapAssembleAcTimestamp (UINT1 *txPkt, tACtimestamp * pAcTime)
{
    /* Copy the message type and length */
    CAPWAP_PUT_2BYTE (txPkt, pAcTime->u2MsgEleType);
    CAPWAP_PUT_2BYTE (txPkt, pAcTime->u2MsgEleLen);
    CAPWAP_PUT_4BYTE (txPkt, pAcTime->u4Timestamp);
    return;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : capwapAssembleAcNamePriority                               *
         *                                                                           *
         * Description  : Assemble the capwap AcNamePriority Message Element         *
         *                transmittable  format.                                     *
         *                                                                           *
         * Input        : Capwap Control message structure                           *
         *                                                                           *
         * Output       : Capwap control message to be transmitted out               *
         *                                                                           *
         * Returns      : NONE                                                       *
         *****************************************************************************/
VOID
capwapAssembleAcNamePriority (UINT1 *txPkt, tACNameWithPrio * pAcName)
{
    /* Copy the message type and length */
    UINT2               u2Len = 0;
    CAPWAP_PUT_2BYTE (txPkt, pAcName->u2MsgEleType);
    CAPWAP_PUT_2BYTE (txPkt, pAcName->u2MsgEleLen);
    CAPWAP_PUT_1BYTE (txPkt, pAcName->u1Priority);
    u2Len = (UINT2) (STRLEN (pAcName->wlcName));
    CAPWAP_PUT_NBYTE (txPkt, pAcName->wlcName, u2Len);
    return;
}

        /**************************IEEE 802.11 Message Elements **************************************************************/
        /*****************************************************************************
         *                                                                           *
         * Function     : CapwapAssembleIeeeAddWlan                                  *
         *                                                                           *
         * Description  : Assemble the capwap IEEE AddWlan Message Element           *
         *                transmittable  format.                                     *
         *                                                                           *
         * Input        : Capwap Control message structure                           *
         *                                                                           *
         * Output       : Capwap control message to be transmitted out               *
         *                                                                           *
         * Returns      : NONE                                                       *
         *****************************************************************************/
VOID
CapwapAssembleIeeeAddWlan (UINT1 *pTxPkt,
                           tWssWlanAddReq * pWssWlanAddReq, UINT2 *pOffset)
{
    CAPWAP_FN_ENTRY ();
#ifdef RSNA_WANTED
    UINT1               u1Index = 0;
#endif

    pTxPkt += *pOffset;
    CAPWAP_PUT_2BYTE (pTxPkt, pWssWlanAddReq->u2MessageType);
    CAPWAP_PUT_2BYTE (pTxPkt, pWssWlanAddReq->u2MessageLength);
    CAPWAP_PUT_1BYTE (pTxPkt, pWssWlanAddReq->u1RadioId);
    CAPWAP_PUT_1BYTE (pTxPkt, pWssWlanAddReq->u1WlanId);
    CAPWAP_PUT_2BYTE (pTxPkt, pWssWlanAddReq->u2Capability);
    CAPWAP_PUT_1BYTE (pTxPkt, pWssWlanAddReq->u1KeyIndex);
    CAPWAP_PUT_1BYTE (pTxPkt, pWssWlanAddReq->u1KeyStatus);
    CAPWAP_PUT_2BYTE (pTxPkt, pWssWlanAddReq->u2KeyLength);
    CAPWAP_PUT_NBYTE (pTxPkt, pWssWlanAddReq->au1Key,
                      pWssWlanAddReq->u2KeyLength);
    CAPWAP_PUT_NBYTE (pTxPkt, pWssWlanAddReq->au1GroupTsc, 6);
    CAPWAP_PUT_1BYTE (pTxPkt, pWssWlanAddReq->u1Qos);
    CAPWAP_PUT_1BYTE (pTxPkt, pWssWlanAddReq->u1AuthType);
    CAPWAP_PUT_1BYTE (pTxPkt, pWssWlanAddReq->u1MacMode);
    CAPWAP_PUT_1BYTE (pTxPkt, pWssWlanAddReq->u1TunnelMode);
    CAPWAP_PUT_1BYTE (pTxPkt, pWssWlanAddReq->u1SupressSsid);
    CAPWAP_PUT_NBYTE (pTxPkt, pWssWlanAddReq->au1Ssid, 32);

    CAPWAP_PUT_1BYTE (pTxPkt,
                      pWssWlanAddReq->WssWlanPowerConstraint.u1ElementId);
    CAPWAP_PUT_1BYTE (pTxPkt, pWssWlanAddReq->WssWlanPowerConstraint.u1Length);
    CAPWAP_PUT_1BYTE (pTxPkt,
                      pWssWlanAddReq->
                      WssWlanPowerConstraint.u1LocalPowerConstraint);

    CAPWAP_PUT_1BYTE (pTxPkt, pWssWlanAddReq->WssWlanQosCapab.u1ElementId);
    CAPWAP_PUT_1BYTE (pTxPkt, pWssWlanAddReq->WssWlanQosCapab.u1Length);
    CAPWAP_PUT_1BYTE (pTxPkt, pWssWlanAddReq->WssWlanQosCapab.u1QosInfo);

    CAPWAP_PUT_1BYTE (pTxPkt, pWssWlanAddReq->WssWlanEdcaParam.u1ElementId);
    CAPWAP_PUT_1BYTE (pTxPkt, pWssWlanAddReq->WssWlanEdcaParam.u1Length);
    CAPWAP_PUT_1BYTE (pTxPkt, pWssWlanAddReq->WssWlanEdcaParam.u1QosInfo);
    CAPWAP_PUT_2BYTE (pTxPkt,
                      pWssWlanAddReq->WssWlanEdcaParam.AC_BE_ParameterRecord.
                      u2TxOpLimit);
    CAPWAP_PUT_1BYTE (pTxPkt,
                      pWssWlanAddReq->WssWlanEdcaParam.AC_BE_ParameterRecord.
                      u1ACI_AIFSN);
    CAPWAP_PUT_1BYTE (pTxPkt,
                      pWssWlanAddReq->WssWlanEdcaParam.AC_BE_ParameterRecord.
                      u1ECWmin_ECWmax);
    CAPWAP_PUT_2BYTE (pTxPkt,
                      pWssWlanAddReq->WssWlanEdcaParam.AC_BK_ParameterRecord.
                      u2TxOpLimit);
    CAPWAP_PUT_1BYTE (pTxPkt,
                      pWssWlanAddReq->WssWlanEdcaParam.AC_BK_ParameterRecord.
                      u1ACI_AIFSN);
    CAPWAP_PUT_1BYTE (pTxPkt,
                      pWssWlanAddReq->WssWlanEdcaParam.AC_BK_ParameterRecord.
                      u1ECWmin_ECWmax);
    CAPWAP_PUT_2BYTE (pTxPkt,
                      pWssWlanAddReq->WssWlanEdcaParam.AC_VI_ParameterRecord.
                      u2TxOpLimit);
    CAPWAP_PUT_1BYTE (pTxPkt,
                      pWssWlanAddReq->WssWlanEdcaParam.AC_VI_ParameterRecord.
                      u1ACI_AIFSN);
    CAPWAP_PUT_1BYTE (pTxPkt,
                      pWssWlanAddReq->WssWlanEdcaParam.AC_VI_ParameterRecord.
                      u1ECWmin_ECWmax);
    CAPWAP_PUT_2BYTE (pTxPkt,
                      pWssWlanAddReq->WssWlanEdcaParam.AC_VO_ParameterRecord.
                      u2TxOpLimit);
    CAPWAP_PUT_1BYTE (pTxPkt,
                      pWssWlanAddReq->WssWlanEdcaParam.AC_VO_ParameterRecord.
                      u1ACI_AIFSN);
    CAPWAP_PUT_1BYTE (pTxPkt,
                      pWssWlanAddReq->WssWlanEdcaParam.AC_VO_ParameterRecord.
                      u1ECWmin_ECWmax);

#ifdef RSNA_WANTED
    /* Key Status equal to zero indicates that RSN elements 
     * are present */
    if ((pWssWlanAddReq->u1KeyStatus == 0)
        && (pWssWlanAddReq->RsnaIEElements.u1ElemId == WSSMAC_RSN_ELEMENT_ID))
    {
        CAPWAP_PUT_1BYTE (pTxPkt, pWssWlanAddReq->RsnaIEElements.u1ElemId);
        CAPWAP_PUT_1BYTE (pTxPkt, pWssWlanAddReq->RsnaIEElements.u1Len);
        CAPWAP_PUT_2BYTE (pTxPkt, pWssWlanAddReq->RsnaIEElements.u2Ver);
        CAPWAP_PUT_NBYTE (pTxPkt,
                          pWssWlanAddReq->RsnaIEElements.au1GroupCipherSuite,
                          RSNA_CIPHER_SUITE_LEN);
        CAPWAP_PUT_2BYTE (pTxPkt,
                          pWssWlanAddReq->
                          RsnaIEElements.u2PairwiseCipherSuiteCount);

        for (u1Index = 0;
             u1Index <
             pWssWlanAddReq->RsnaIEElements.u2PairwiseCipherSuiteCount;
             u1Index++)
        {
            CAPWAP_PUT_NBYTE (pTxPkt,
                              pWssWlanAddReq->
                              RsnaIEElements.aRsnaPwCipherDB[u1Index].
                              au1PairwiseCipher, RSNA_CIPHER_SUITE_LEN);
        }
        CAPWAP_PUT_2BYTE (pTxPkt,
                          pWssWlanAddReq->RsnaIEElements.u2AKMSuiteCount);

        for (u1Index = 0;
             u1Index < pWssWlanAddReq->RsnaIEElements.u2AKMSuiteCount;
             u1Index++)
        {
            CAPWAP_PUT_NBYTE (pTxPkt,
                              pWssWlanAddReq->
                              RsnaIEElements.aRsnaAkmDB[u1Index].au1AKMSuite,
                              RSNA_AKM_SELECTOR_LEN);
        }
        CAPWAP_PUT_2BYTE (pTxPkt, pWssWlanAddReq->RsnaIEElements.u2RsnCapab);

        if (pWssWlanAddReq->RsnaIEElements.u2PmkidCount != 0)
        {
            CAPWAP_PUT_2BYTE (pTxPkt,
                              pWssWlanAddReq->RsnaIEElements.u2PmkidCount);
            CAPWAP_PUT_NBYTE (pTxPkt, pWssWlanAddReq->RsnaIEElements.aPmkidList,
                              RSNA_PMKID_LEN);
        }
#ifdef PMF_WANTED
        if (((pWssWlanAddReq->RsnaIEElements.u2RsnCapab & RSNA_CAPABILITY_MFPR)
             == RSNA_CAPABILITY_MFPR)
            ||
            ((pWssWlanAddReq->RsnaIEElements.
              u2RsnCapab & RSNA_CAPABILITY_MFPC) == RSNA_CAPABILITY_MFPC))
        {
            CAPWAP_PUT_NBYTE (pTxPkt,
                              pWssWlanAddReq->RsnaIEElements.
                              au1GroupMgmtCipherSuite, RSNA_CIPHER_SUITE_LEN);

        }
#endif
    }
#endif

    *pOffset =
        (UINT2) (CAPWAP_MSG_ELEM_TYPE_LEN + pWssWlanAddReq->u2MessageLength);
    CAPWAP_FN_EXIT ();
    return;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : CapwapAssembleIeeeAntena                                   *
         *                                                                           *
         * Description  : Assemble the capwap IEEE Antenna packet structure to       *
         *                transmittable  format.                                     *
         *                                                                           *
         * Input        : Capwap Control message structure                           *
         *                                                                           *
         * Output       : Capwap control message to be transmitted out               *
         *                                                                           *
         * Returns      : NONE                                                       *
         *****************************************************************************/
VOID
CapwapAssembleIeeeAntena (UINT1 *pTxPkt, tRadioIfAntenna * pRadioIfAntenna)
{
    CAPWAP_PUT_2BYTE (pTxPkt, pRadioIfAntenna->u2MessageType);
    CAPWAP_PUT_2BYTE (pTxPkt, pRadioIfAntenna->u2MessageLength);
    CAPWAP_PUT_1BYTE (pTxPkt, pRadioIfAntenna->u1RadioId);
    CAPWAP_PUT_1BYTE (pTxPkt, pRadioIfAntenna->u1ReceiveDiversity);
    CAPWAP_PUT_1BYTE (pTxPkt, pRadioIfAntenna->u1AntennaMode);
    CAPWAP_PUT_1BYTE (pTxPkt, pRadioIfAntenna->u1CurrentTxAntenna);
    CAPWAP_PUT_NBYTE (pTxPkt, pRadioIfAntenna->au1AntennaSelection,
                      pRadioIfAntenna->u1CurrentTxAntenna);
    return;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : CapwapAssembleIeeeDeleteWlan                               *
         *                                                                           *
         * Description  : Assemble the capwap IEEE Delete Wlan packet structure to   *
         *                transmittable  format.                                     *
         *                                                                           *
         * Input        : Capwap Control message structure                           *
         *                                                                           *
         * Output       : Capwap control message to be transmitted out               *
         *                                                                           *
         * Returns      : NONE                                                       *
         *****************************************************************************/
VOID
CapwapAssembleIeeeDeleteWlan (UINT1 *pTxPkt,
                              tWssWlanDeleteReq * pWssWlanDeleteReq,
                              UINT2 *pOffset)
{
    pTxPkt += *pOffset;
    CAPWAP_PUT_2BYTE (pTxPkt, pWssWlanDeleteReq->u2MessageType);
    CAPWAP_PUT_2BYTE (pTxPkt, pWssWlanDeleteReq->u2MessageLength);
    CAPWAP_PUT_1BYTE (pTxPkt, pWssWlanDeleteReq->u1RadioId);
    CAPWAP_PUT_1BYTE (pTxPkt, pWssWlanDeleteReq->u1WlanId);
    *pOffset =
        (UINT2) (CAPWAP_MSG_ELEM_TYPE_LEN + pWssWlanDeleteReq->u2MessageLength);
    return;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : CapwapAssembleIeeeAssignedBssId                            *
         *                                                                           *
         * Description  : Assemble the capwap IEEE Assigned BSS Id packet structure  *
         *                to transmittable  format.                                  *
         *                                                                           *
         * Input        : Capwap Control message structure                           *
         *                                                                           *
         * Output       : Capwap control message to be transmitted out               *
         *                                                                           *
         * Returns      : NONE                                                       *
         *****************************************************************************/
VOID
CapwapAssembleIeeeAssignedBssId (UINT1 *pTxPkt, tWssWlanRsp * pWssWlanRsp)
{
    CAPWAP_FN_ENTRY ();
    CAPWAP_PUT_2BYTE (pTxPkt, pWssWlanRsp->u2MessageType);
    CAPWAP_PUT_2BYTE (pTxPkt, pWssWlanRsp->u2Length);
    CAPWAP_PUT_1BYTE (pTxPkt, pWssWlanRsp->u1RadioId);
    CAPWAP_PUT_1BYTE (pTxPkt, pWssWlanRsp->u1WlanId);
    CAPWAP_PUT_NBYTE (pTxPkt, pWssWlanRsp->BssId, 6);
    CAPWAP_FN_EXIT ();
    return;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : CapwapAssembleIeeeDirectSequeceControl                     *
         *                                                                           *
         * Description  : Assemble the capwap IEEE Direct Seq Ctrl packet structure  *
         *                to transmittable  format.                                  *
         *                                                                           *
         * Input        : Capwap Control message structure                           *
         *                                                                           *
         * Output       : Capwap control message to be transmitted out               *
         *                                                                           *
         * Returns      : NONE                                                       *
         *****************************************************************************/
VOID
CapwapAssembleIeeeDirectSequeceControl (UINT1 *pTxPkt,
                                        tRadioIfDSSSPhy * pRadioIfDSSSPhy)
{
    CAPWAP_PUT_2BYTE (pTxPkt, pRadioIfDSSSPhy->u2MessageType);
    CAPWAP_PUT_2BYTE (pTxPkt, pRadioIfDSSSPhy->u2MessageLength);
    CAPWAP_PUT_1BYTE (pTxPkt, pRadioIfDSSSPhy->u1RadioId);
    CAPWAP_PUT_1BYTE (pTxPkt, pRadioIfDSSSPhy->u1Reserved);
    CAPWAP_PUT_1BYTE (pTxPkt, (UINT1) pRadioIfDSSSPhy->i1CurrentChannel);
    CAPWAP_PUT_1BYTE (pTxPkt, (UINT1) pRadioIfDSSSPhy->i1CurrentCCAMode);
    CAPWAP_PUT_4BYTE (pTxPkt, (UINT4) pRadioIfDSSSPhy->i4EDThreshold);
    return;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : CapwapAssembleIeeeInfoElem                                 *
         *                                                                           *
         * Description  : Assemble the Information Element in the buffer             *
         *                                                                           *
         * Input        : tRadioIfInfoElement : Information Message Element          *
         *                                                                           *
         * Output       : Capwap message element to be transmitted out               *
         *                                                                           *
         * Returns      : NONE                                                       *
         *****************************************************************************/
VOID
CapwapAssembleIeeeInfoElem (UINT1 *pTxPkt, tRadioIfInfoElement * pInfoElem)
{
#ifdef WPS_WANTED
    UINT1               u1WpsIndex = 0;
#endif
    CAPWAP_FN_ENTRY ();

    /* Copy the message type and length */
    CAPWAP_PUT_2BYTE (pTxPkt, pInfoElem->u2MessageType);
    CAPWAP_PUT_2BYTE (pTxPkt, pInfoElem->u2MessageLength);
    CAPWAP_PUT_1BYTE (pTxPkt, pInfoElem->u1RadioId);
    CAPWAP_PUT_1BYTE (pTxPkt, pInfoElem->u1WlanId);
    CAPWAP_PUT_1BYTE (pTxPkt, pInfoElem->u1BPFlag);

    switch (pInfoElem->u1EleId)
    {
        case DOT11_INFO_ELEM_HTCAPABILITY:
            if (pInfoElem->unInfoElem.HTCapability.isOptional == OSIX_TRUE)
            {
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  pInfoElem->unInfoElem.HTCapability.u1ElemId);
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  pInfoElem->unInfoElem.HTCapability.u1ElemLen);
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  pInfoElem->unInfoElem.
                                  HTCapability.u2HTCapInfo);
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  pInfoElem->unInfoElem.
                                  HTCapability.u1AMPDUParam);
                CAPWAP_PUT_NBYTE (pTxPkt,
                                  pInfoElem->unInfoElem.
                                  HTCapability.au1SuppMCSSet,
                                  CAPWAP_11N_MCS_RATE_LEN);
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  pInfoElem->unInfoElem.
                                  HTCapability.u2HTExtCap);
                CAPWAP_PUT_4BYTE (pTxPkt,
                                  pInfoElem->unInfoElem.
                                  HTCapability.u4TranBeamformCap);
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  pInfoElem->unInfoElem.HTCapability.u1ASELCap);
            }
            break;

        case DOT11_INFO_ELEM_HTOPERATION:
            if (pInfoElem->unInfoElem.HTOperation.isOptional == OSIX_TRUE)
            {
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  pInfoElem->unInfoElem.HTOperation.u1ElemId);
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  pInfoElem->unInfoElem.HTOperation.u1ElemLen);
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  pInfoElem->unInfoElem.HTOperation.
                                  u1PrimaryCh);
                CAPWAP_PUT_NBYTE (pTxPkt,
                                  pInfoElem->unInfoElem.HTOperation.
                                  au1HTOpeInfo, WSSMAC_HTOPE_INFO);
                CAPWAP_PUT_NBYTE (pTxPkt,
                                  pInfoElem->unInfoElem.HTOperation.
                                  au1BasicMCSSet, WSSMAC_BASIC_MCS_SET);
            }

            break;

        case DOT11AC_INFO_ELEM_VHTCAPABILITY:
            if (pInfoElem->unInfoElem.VHTCapability.isOptional == OSIX_TRUE)
            {
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  pInfoElem->unInfoElem.VHTCapability.u1ElemId);
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  pInfoElem->unInfoElem.
                                  VHTCapability.u1ElemLen);
                CAPWAP_PUT_4BYTE (pTxPkt,
                                  pInfoElem->unInfoElem.
                                  VHTCapability.u4VhtCapInfo);
                CAPWAP_PUT_NBYTE (pTxPkt,
                                  pInfoElem->unInfoElem.
                                  VHTCapability.au1SuppMCS,
                                  DOT11AC_VHT_CAP_MCS_LEN);
            }

            break;

        case DOT11AC_INFO_ELEM_VHTOPERATION:
            if (pInfoElem->unInfoElem.VHTOperation.isOptional == OSIX_TRUE)
            {
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  pInfoElem->unInfoElem.VHTOperation.u1ElemId);
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  pInfoElem->unInfoElem.VHTOperation.u1ElemLen);
                CAPWAP_PUT_NBYTE (pTxPkt,
                                  pInfoElem->unInfoElem.
                                  VHTOperation.au1VhtOperInfo,
                                  DOT11AC_VHT_OPER_INFO_LEN);
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  pInfoElem->unInfoElem.
                                  VHTOperation.u2BasicMCS);
            }

            break;
#ifdef WPS_WANTED
        case WPS_VENDOR_ELEMENT_ID:
            if (pInfoElem->unInfoElem.WpsIEElements.isOptional == OSIX_TRUE)
            {
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  pInfoElem->unInfoElem.WpsIEElements.u1ElemId);
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  pInfoElem->unInfoElem.WpsIEElements.u1Len);
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  (UINT1) pInfoElem->unInfoElem.WpsIEElements.
                                  bWpsEnabled);
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  pInfoElem->unInfoElem.WpsIEElements.wscState);
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  (UINT1) pInfoElem->unInfoElem.WpsIEElements.
                                  bWpsAdditionalIE);
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  pInfoElem->unInfoElem.WpsIEElements.
                                  noOfAuthMac);

                for (u1WpsIndex = 0;
                     u1WpsIndex <
                     pInfoElem->unInfoElem.WpsIEElements.noOfAuthMac;
                     u1WpsIndex++)
                {
                    CAPWAP_PUT_NBYTE (pTxPkt,
                                      pInfoElem->
                                      unInfoElem.WpsIEElements.
                                      authorized_macs[u1WpsIndex],
                                      WPS_ETH_ALEN);
                }

                CAPWAP_PUT_NBYTE (pTxPkt,
                                  pInfoElem->
                                  unInfoElem.WpsIEElements.uuid_e,
                                  WPS_UUID_LEN);
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  pInfoElem->unInfoElem.WpsIEElements.
                                  configMethods);
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  pInfoElem->unInfoElem.WpsIEElements.
                                  statusOrAddIE);
            }
            break;
#endif
        default:
            break;
    }
    CAPWAP_FN_EXIT ();
    return;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : CapwapAssembleIeeeMacOperation                             *
         *                                                                           *
         * Description  : Assemble the capwap IEEE MAC Operation packet structure    *
         *                to transmittable  format.                                  *
         *                                                                           *
         * Input        : Capwap Control message structure                           *
         *                                                                           *
         * Output       : Capwap control message to be transmitted out               *
         *                                                                           *
         * Returns      : NONE                                                       *
         *****************************************************************************/
VOID
CapwapAssembleIeeeMacOperation (UINT1 *pTxPkt,
                                tRadioIfMacOperation * pRadioIfMacOperation)
{
    CAPWAP_PUT_2BYTE (pTxPkt, pRadioIfMacOperation->u2MessageType);
    CAPWAP_PUT_2BYTE (pTxPkt, pRadioIfMacOperation->u2MessageLength);
    CAPWAP_PUT_1BYTE (pTxPkt, pRadioIfMacOperation->u1RadioId);
    CAPWAP_PUT_1BYTE (pTxPkt, pRadioIfMacOperation->u1Reserved);
    CAPWAP_PUT_2BYTE (pTxPkt, pRadioIfMacOperation->u2RTSThreshold);
    CAPWAP_PUT_1BYTE (pTxPkt, pRadioIfMacOperation->u1ShortRetry);
    CAPWAP_PUT_1BYTE (pTxPkt, pRadioIfMacOperation->u1LongRetry);
    CAPWAP_PUT_2BYTE (pTxPkt, pRadioIfMacOperation->u2FragmentThreshold);
    CAPWAP_PUT_4BYTE (pTxPkt, pRadioIfMacOperation->u4TxMsduLifetime);
    CAPWAP_PUT_4BYTE (pTxPkt, pRadioIfMacOperation->u4RxMsduLifetime);
    return;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : CapwapAssembleIeeeMultiDomainCapability                    *
         *                                                                           *
         * Description  : Assemble the capwap IEEE MultiDomainCap. packet structure  *
         *                to transmittable  format.                                  *
         *                                                                           *
         * Input        : Capwap Control message structure                           *
         *                                                                           *
         * Output       : Capwap control message to be transmitted out               *
         *                                                                           *
         * Returns      : NONE                                                       *
         *****************************************************************************/
VOID
CapwapAssembleIeeeMultiDomainCapability (UINT1 *pTxPkt,
                                         tRadioIfMultiDomainCap *
                                         pRadioIfMultiDomainCap)
{
    CAPWAP_PUT_2BYTE (pTxPkt, pRadioIfMultiDomainCap->u2MessageType);
    CAPWAP_PUT_2BYTE (pTxPkt, pRadioIfMultiDomainCap->u2MessageLength);
    CAPWAP_PUT_1BYTE (pTxPkt, pRadioIfMultiDomainCap->u1RadioId);
    CAPWAP_PUT_1BYTE (pTxPkt, pRadioIfMultiDomainCap->u1Reserved);
    CAPWAP_PUT_2BYTE (pTxPkt, pRadioIfMultiDomainCap->u2FirstChannel);
    CAPWAP_PUT_2BYTE (pTxPkt, pRadioIfMultiDomainCap->u2NumOfChannels);
    CAPWAP_PUT_2BYTE (pTxPkt, pRadioIfMultiDomainCap->u2MaxTxPowerLevel);
    return;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : CapwapAssembleIeeeOFDMControl                              *
         *                                                                           *
         * Description  : Assemble the capwap IEEE OFDM.Control packet structure     *
         *                to transmittable  format.                                  *
         *                                                                           *
         * Input        : Capwap Control message structure                           *
         *                                                                           *
         * Output       : Capwap control message to be transmitted out               *
         *                                                                           *
         * Returns      : NONE                                                       *
         *****************************************************************************/
VOID
CapwapAssembleIeeeOFDMControl (UINT1 *pTxPkt, tRadioIfOFDMPhy * pRadioIfOFDMPhy)
{
    CAPWAP_PUT_2BYTE (pTxPkt, pRadioIfOFDMPhy->u2MessageType);
    CAPWAP_PUT_2BYTE (pTxPkt, pRadioIfOFDMPhy->u2MessageLength);
    CAPWAP_PUT_1BYTE (pTxPkt, pRadioIfOFDMPhy->u1RadioId);
    CAPWAP_PUT_1BYTE (pTxPkt, pRadioIfOFDMPhy->u1Reserved);
    CAPWAP_PUT_1BYTE (pTxPkt, pRadioIfOFDMPhy->u1CurrentChannel);
    CAPWAP_PUT_1BYTE (pTxPkt, pRadioIfOFDMPhy->u1SupportedBand);
    CAPWAP_PUT_4BYTE (pTxPkt, pRadioIfOFDMPhy->u4T1Threshold);
    return;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : CapwapAssembleIeeeRateSet                                  *
         *                                                                           *
         * Description  : Assemble the capwap IEEE Rate.Control packet structure     *
         *                to transmittable  format.                                  *
         *                                                                           *
         * Input        : Capwap Control message structure                           *
         *                                                                           *
         * Output       : Capwap control message to be transmitted out               *
         *                                                                           *
         * Returns      : NONE                                                       *
         *****************************************************************************/
VOID
CapwapAssembleIeeeRateSet (UINT1 *pTxPkt, tRadioIfRateSet * pRadioIfRateSet)
{
    CAPWAP_PUT_2BYTE (pTxPkt, pRadioIfRateSet->u2MessageType);
    CAPWAP_PUT_2BYTE (pTxPkt, pRadioIfRateSet->u2MessageLength);
    CAPWAP_PUT_1BYTE (pTxPkt, pRadioIfRateSet->u1RadioId);
    CAPWAP_PUT_NBYTE (pTxPkt, pRadioIfRateSet->au1OperationalRate, 8);
    return;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : CapwapAssembleIeeeStation                                  *
         *                                                                           *
         * Description  : Assemble the capwap IEEE Station packet structure to       *
         *                transmittable  format.                                     *
         *                                                                           *
         * Input        : Capwap Control message structure                           *
         *                                                                           *
         * Output       : Capwap control message to be transmitted out               *
         *                                                                           *
         * Returns      : NONE                                                       *
         *****************************************************************************/
VOID
CapwapAssembleIeeeStation (UINT1 *pTxPkt,
                           tWssStaDot11Station * pWssAuthDot11Station)
{
    UINT2               u2Len;
    CAPWAP_PUT_2BYTE (pTxPkt, pWssAuthDot11Station->u2MessageType);
    CAPWAP_PUT_2BYTE (pTxPkt, pWssAuthDot11Station->u2MessageLength);
    CAPWAP_PUT_1BYTE (pTxPkt, pWssAuthDot11Station->u1RadioId);
    CAPWAP_PUT_2BYTE (pTxPkt, pWssAuthDot11Station->u2AssocId);
    CAPWAP_PUT_1BYTE (pTxPkt, pWssAuthDot11Station->u1Flags);
    CAPWAP_PUT_NBYTE (pTxPkt, pWssAuthDot11Station->stationMacAddress, 6);
    CAPWAP_PUT_2BYTE (pTxPkt, pWssAuthDot11Station->u2Capab);
    CAPWAP_PUT_1BYTE (pTxPkt, pWssAuthDot11Station->u1WlanId);
    u2Len = (UINT2) (STRLEN (pWssAuthDot11Station->au1SuppRates));
    CAPWAP_PUT_NBYTE (pTxPkt, pWssAuthDot11Station->au1SuppRates, u2Len);
    return;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : CapwapAssembleIeeeStationQosProfile                        *
         *                                                                           *
         * Description  : Assemble the capwap IEEE Station QoS Profile packet        *
         *                structure to transmittable  format.                        *
         *                                                                           *
         * Input        : Capwap Control message structure                           *
         *                                                                           *
         * Output       : Capwap control message to be transmitted out               *
         *                                                                           *
         * Returns      : NONE                                                       *
         *****************************************************************************/
VOID
CapwapAssembleIeeeStationQosProfile (UINT1 *pTxPkt,
                                     tWssStaDot11QoSProfile *
                                     pWssAuthDot11QoSProfile)
{
    CAPWAP_PUT_2BYTE (pTxPkt, pWssAuthDot11QoSProfile->u2MessageType);
    CAPWAP_PUT_2BYTE (pTxPkt, pWssAuthDot11QoSProfile->u2MessageLength);
    CAPWAP_PUT_NBYTE (pTxPkt, pWssAuthDot11QoSProfile->stationMacAddress, 6);
    CAPWAP_PUT_1BYTE (pTxPkt, pWssAuthDot11QoSProfile->u180211Priority);
    return;
}

VOID
CapwapAssembleStaDot11nParams (UINT1 *pTxPkt,
                               tWssStaDot11nConfigInfo *
                               pWssStaDot11nConfigInfo)
{
    tVendorSpecPayload  vendorSpecificPayload;
    tDot11nStaVendorType dot11nStaVendor;

    CAPWAP_MEMSET (&vendorSpecificPayload, CAPWAP_INIT_VAL,
                   sizeof (tVendorSpecPayload));
    CAPWAP_MEMSET (&dot11nStaVendor, CAPWAP_INIT_VAL,
                   sizeof (tDot11nStaVendorType));

    vendorSpecificPayload.u2MsgEleType = VENDOR_SPECIFIC_PAYLOAD;
    vendorSpecificPayload.u2MsgEleLen =
        DOT11N_STA_VENDOR_MSG_LEN + CAPWAP_MSG_ELEM_TYPE_LEN;
    dot11nStaVendor.isOptional = OSIX_TRUE;
    dot11nStaVendor.u2MsgEleType = pWssStaDot11nConfigInfo->u2MessageType;
    dot11nStaVendor.u2MsgEleLen = DOT11N_STA_VENDOR_MSG_LEN;
    dot11nStaVendor.u4VendorId = pWssStaDot11nConfigInfo->u4VendorId;
    MEMCPY (dot11nStaVendor.stationMacAddr,
            pWssStaDot11nConfigInfo->stationMacAddr,
            sizeof (pWssStaDot11nConfigInfo->stationMacAddr));
    dot11nStaVendor.u2HiSuppDataRate =
        pWssStaDot11nConfigInfo->u2HiSuppDataRate;
    dot11nStaVendor.u2AMPDUBuffSize = pWssStaDot11nConfigInfo->u2AMPDUBuffSize;
    dot11nStaVendor.u1HtFlag = pWssStaDot11nConfigInfo->u1HtFlag;
    dot11nStaVendor.u1MaxRxFactor = pWssStaDot11nConfigInfo->u1MaxRxFactor;
    dot11nStaVendor.u1MinStaSpacing = pWssStaDot11nConfigInfo->u1MinStaSpacing;
    dot11nStaVendor.u1HTCSupp = pWssStaDot11nConfigInfo->u1HTCSupp;
    MEMCPY (dot11nStaVendor.au1ManMCSSet,
            pWssStaDot11nConfigInfo->au1ManMCSSet, CAPWAP_11N_MCS_RATE_LEN);

    /* The element id needs to be set in order to identify the 
     * specific type of vendor message */
    vendorSpecificPayload.elementId = VENDOR_DOT11N_STA_TYPE;
    vendorSpecificPayload.isOptional = OSIX_TRUE;
    CAPWAP_MEMCPY (&(vendorSpecificPayload.unVendorSpec.Dot11nStaVendor),
                   &dot11nStaVendor, sizeof (tDot11nStaVendorType));

    /*Invoke vendor specific payload assemble function */
    CapwapAssembleVendorSpecificPayload (pTxPkt, &vendorSpecificPayload);

    return;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : CapwapAssembleIeeeStationSessionKey                        *
         *                                                                           *
         * Description  : Assemble the capwap IEEE Station Session Key packet        *
         *                structure to transmittable  format.                        *
         *                                                                           *
         * Input        : Capwap Control message structure                           *
         *                                                                           *
         * Output       : Capwap control message to be transmitted out               *
         *                                                                           *
         * Returns      : NONE                                                       *
         *****************************************************************************/
VOID
CapwapAssembleIeeeStationSessionKey (UINT1 *pTxPkt,
                                     tWssStaDot11Sesskey * pWssAuthDot11Sesskey)
{
#ifdef RSNA_WANTED
    UINT1               u1Index = 0;
    UINT1               au1TkipCipher[] = { 0x00, 0x50, 0xF2, 2 };
    UINT1               au1CcmpCipher[] = { 0x00, 0x50, 0xF2, 4 };
#endif
    CAPWAP_PUT_2BYTE (pTxPkt, pWssAuthDot11Sesskey->u2MessageType);
    CAPWAP_PUT_2BYTE (pTxPkt, pWssAuthDot11Sesskey->u2MessageLength);
    CAPWAP_PUT_NBYTE (pTxPkt, pWssAuthDot11Sesskey->stationMacAddress, 6);
    CAPWAP_PUT_2BYTE (pTxPkt, pWssAuthDot11Sesskey->u2Flags);
    CAPWAP_PUT_NBYTE (pTxPkt, pWssAuthDot11Sesskey->au1PairwiseTSC, 6);
    CAPWAP_PUT_NBYTE (pTxPkt, pWssAuthDot11Sesskey->au1PairwiseRSC, 6);
    if ((pWssAuthDot11Sesskey->u2MessageLength == (20 + CAPWAP_WEP_40_KEY_LEN))
        || (pWssAuthDot11Sesskey->u2MessageLength ==
            (20 + CAPWAP_WEP_104_KEY_LEN)))
    {
        CAPWAP_PUT_NBYTE (pTxPkt, pWssAuthDot11Sesskey->au1Key,
                          (size_t) ((pWssAuthDot11Sesskey->u2MessageLength) -
                                    20));
    }
#ifdef RSNA_WANTED
    else if ((MEMCMP
              (pWssAuthDot11Sesskey->RsnaIEElements.
               aRsnaPwCipherDB[u1Index].au1PairwiseCipher,
               gau1RsnaCipherSuiteTKIP, RSNA_CIPHER_SUITE_LEN)) == 0 ||
             MEMCMP
             (pWssAuthDot11Sesskey->RsnaIEElements.aRsnaPwCipherDB[u1Index].
              au1PairwiseCipher, au1TkipCipher, RSNA_CIPHER_SUITE_LEN) == 0)
    {
        CAPWAP_PUT_NBYTE (pTxPkt, pWssAuthDot11Sesskey->au1Key,
                          RSNA_TKIP_KEY_LEN);

    }
    else if ((MEMCMP
              (pWssAuthDot11Sesskey->RsnaIEElements.
               aRsnaPwCipherDB[u1Index].au1PairwiseCipher,
               gau1RsnaCipherSuiteCCMP, RSNA_CIPHER_SUITE_LEN)) == 0 ||
             (MEMCMP (pWssAuthDot11Sesskey->RsnaIEElements.
                      aRsnaPwCipherDB[u1Index].au1PairwiseCipher, au1CcmpCipher,
                      RSNA_CIPHER_SUITE_LEN)) == 0)
    {
        CAPWAP_PUT_NBYTE (pTxPkt, pWssAuthDot11Sesskey->au1Key,
                          RSNA_CCMP_KEY_LEN);

    }
    /* Element id equal to RSN element id indicates that RSN elements 
     * are present */
    if (pWssAuthDot11Sesskey->RsnaIEElements.u1ElemId == WSSMAC_RSN_ELEMENT_ID
        || pWssAuthDot11Sesskey->RsnaIEElements.u1ElemId == WPA_IE_ID)
    {
        CAPWAP_PUT_1BYTE (pTxPkt,
                          pWssAuthDot11Sesskey->RsnaIEElements.u1ElemId);
        CAPWAP_PUT_1BYTE (pTxPkt, pWssAuthDot11Sesskey->RsnaIEElements.u1Len);
        CAPWAP_PUT_2BYTE (pTxPkt, pWssAuthDot11Sesskey->RsnaIEElements.u2Ver);
        CAPWAP_PUT_NBYTE (pTxPkt,
                          pWssAuthDot11Sesskey->
                          RsnaIEElements.au1GroupCipherSuite,
                          RSNA_CIPHER_SUITE_LEN);
        CAPWAP_PUT_2BYTE (pTxPkt,
                          pWssAuthDot11Sesskey->
                          RsnaIEElements.u2PairwiseCipherSuiteCount);

        CAPWAP_PUT_NBYTE (pTxPkt,
                          pWssAuthDot11Sesskey->
                          RsnaIEElements.aRsnaPwCipherDB[u1Index].
                          au1PairwiseCipher, RSNA_CIPHER_SUITE_LEN);

        CAPWAP_PUT_2BYTE (pTxPkt,
                          pWssAuthDot11Sesskey->RsnaIEElements.u2AKMSuiteCount);

        CAPWAP_PUT_NBYTE (pTxPkt,
                          pWssAuthDot11Sesskey->
                          RsnaIEElements.aRsnaAkmDB[u1Index].au1AKMSuite,
                          RSNA_AKM_SELECTOR_LEN);

        if (pWssAuthDot11Sesskey->RsnaIEElements.u1ElemId ==
            WSSMAC_RSN_ELEMENT_ID)
        {
            CAPWAP_PUT_2BYTE (pTxPkt,
                              pWssAuthDot11Sesskey->RsnaIEElements.u2RsnCapab);
            if (pWssAuthDot11Sesskey->RsnaIEElements.u2PmkidCount != 0)
            {
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  pWssAuthDot11Sesskey->
                                  RsnaIEElements.u2PmkidCount);
                CAPWAP_PUT_NBYTE (pTxPkt,
                                  pWssAuthDot11Sesskey->RsnaIEElements.
                                  aPmkidList, RSNA_PMKID_LEN);
            }
        }
    }
#endif
    return;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : CapwapAssembleIeeeSupportedRates                           *
         *                                                                           *
         * Description  : Assemble the capwap IEEE Supported Rates  packet structure *
         *                to transmittable  format.                                  *
         *                                                                           *
         * Input        : Capwap Control message structure                           *
         *                                                                           *
         * Output       : Capwap control message to be transmitted out               *
         *                                                                           *
         * Returns      : NONE                                                       *
         *****************************************************************************/
VOID
CapwapAssembleIeeeSupportedRates (UINT1 *pTxPkt,
                                  tRadioIfSupportedRate * pRadioIfSupportedRate)
{
    CAPWAP_PUT_2BYTE (pTxPkt, pRadioIfSupportedRate->u2MessageType);
    CAPWAP_PUT_2BYTE (pTxPkt, pRadioIfSupportedRate->u2MessageLength);
    CAPWAP_PUT_1BYTE (pTxPkt, pRadioIfSupportedRate->u1RadioId);
    CAPWAP_PUT_NBYTE (pTxPkt, pRadioIfSupportedRate->au1SupportedRate,
                      (size_t) (pRadioIfSupportedRate->u2MessageLength - 1));
    return;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : CapwapAssembleIeeeTxPower                                  *
         *                                                                           *
         * Description  : Assemble the capwap IEEE Tx Power  packet structure        *
         *                to transmittable  format.                                  *
         *                                                                           *
         * Input        : Capwap Control message structure                           *
         *                                                                           *
         * Output       : Capwap control message to be transmitted out               *
         *                                                                           *
         * Returns      : NONE                                                       *
         *****************************************************************************/
VOID
CapwapAssembleIeeeTxPower (UINT1 *pTxPkt, tRadioIfTxPower * pRadioIfTxPower)
{
    CAPWAP_PUT_2BYTE (pTxPkt, pRadioIfTxPower->u2MessageType);
    CAPWAP_PUT_2BYTE (pTxPkt, pRadioIfTxPower->u2MessageLength);
    CAPWAP_PUT_1BYTE (pTxPkt, pRadioIfTxPower->u1RadioId);
    CAPWAP_PUT_1BYTE (pTxPkt, pRadioIfTxPower->u1Reserved);
    CAPWAP_PUT_2BYTE (pTxPkt, pRadioIfTxPower->u2TxPowerLevel);
    return;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : CapwapAssembleIeeeTxPowerLevel                             *
         *                                                                           *
         * Description  : Assemble the capwap IEEE Tx Power level packet structure   *
         *                to transmittable  format.                                  *
         *                                                                           *
         * Input        : Capwap Control message structure                           *
         *                                                                           *
         * Output       : Capwap control message to be transmitted out               *
         *                                                                           *
         * Returns      : NONE                                                       *
         *****************************************************************************/
VOID
CapwapAssembleIeeeTxPowerLevel (UINT1 *pTxPkt,
                                tRadioIfTxPowerLevel * pRadioIfTxPowerLevel)
{
    CAPWAP_PUT_2BYTE (pTxPkt, pRadioIfTxPowerLevel->u2MessageType);
    CAPWAP_PUT_2BYTE (pTxPkt, pRadioIfTxPowerLevel->u2MessageLength);
    CAPWAP_PUT_1BYTE (pTxPkt, pRadioIfTxPowerLevel->u1RadioId);
    CAPWAP_PUT_1BYTE (pTxPkt, pRadioIfTxPowerLevel->u1NumOfLevels);
    CAPWAP_PUT_NBYTE (pTxPkt, pRadioIfTxPowerLevel->au2PowerLevels, 16);
    return;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : CapwapAssembleIeeeUpdateStatonQos                          *
         *                                                                           *
         * Description  : Assemble the capwap IEEE Update Station QoS packet         *
         *                structure to transmittable  format.                        *
         *                                                                           *
         * Input        : Capwap Control message structure                           *
         *                                                                           *
         * Output       : Capwap control message to be transmitted out               *
         *                                                                           *
         * Returns      : SUCCESS/FAILURE                                            *
         *****************************************************************************/
VOID
CapwapAssembleIeeeUpdateStatonQos (UINT1 *pTxPkt,
                                   tWssStaDot11UpdateStaQoS *
                                   pWssAuthDot11UpdateStaQoS)
{
    CAPWAP_PUT_2BYTE (pTxPkt, pWssAuthDot11UpdateStaQoS->u2MessageType);
    CAPWAP_PUT_2BYTE (pTxPkt, pWssAuthDot11UpdateStaQoS->u2MessageLength);
    CAPWAP_PUT_1BYTE (pTxPkt, pWssAuthDot11UpdateStaQoS->u1RadioId);
    CAPWAP_PUT_NBYTE (pTxPkt, pWssAuthDot11UpdateStaQoS->stationMacAddress, 6);
    CAPWAP_PUT_2BYTE (pTxPkt, pWssAuthDot11UpdateStaQoS->u2QoSElement);
    return;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : CapwapAssembleIeeeUpdateWlan                               *
         *                                                                           *
         * Description  : Assemble the capwap IEEE Update WLAN packet structure to   *
         *                transmittable  format.                                     *
         *                                                                           *
         * Input        : Capwap Control message structure                           *
         *                                                                           *
         * Output       : Capwap control message to be transmitted out               *
         *                                                                           *
         * Returns      : SUCCESS/FAILURE                                            *
         *****************************************************************************/
VOID
CapwapAssembleIeeeUpdateWlan (UINT1 *pTxPkt,
                              tWssWlanUpdateReq * pWssWlanUpdateReq,
                              UINT2 *pOffset)
{
#ifdef RSNA_WANTED
    UINT1               u1Index = 0;
#endif
    pTxPkt += *pOffset;
    CAPWAP_PUT_2BYTE (pTxPkt, pWssWlanUpdateReq->u2MessageType);
    CAPWAP_PUT_2BYTE (pTxPkt, pWssWlanUpdateReq->u2MessageLength);
    CAPWAP_PUT_1BYTE (pTxPkt, pWssWlanUpdateReq->u1RadioId);
    CAPWAP_PUT_1BYTE (pTxPkt, pWssWlanUpdateReq->u1WlanId);
    CAPWAP_PUT_2BYTE (pTxPkt, pWssWlanUpdateReq->u2Capability);
    CAPWAP_PUT_1BYTE (pTxPkt, pWssWlanUpdateReq->u1KeyIndex);
    CAPWAP_PUT_1BYTE (pTxPkt, pWssWlanUpdateReq->u1KeyStatus);
    CAPWAP_PUT_2BYTE (pTxPkt, pWssWlanUpdateReq->u2KeyLength);
    CAPWAP_PUT_NBYTE (pTxPkt, pWssWlanUpdateReq->au1Key,
                      pWssWlanUpdateReq->u2KeyLength);
    CAPWAP_PUT_1BYTE (pTxPkt, pWssWlanUpdateReq->u1Qos);

    CAPWAP_PUT_1BYTE (pTxPkt,
                      pWssWlanUpdateReq->WssWlanPowerConstraint.u1ElementId);
    CAPWAP_PUT_1BYTE (pTxPkt,
                      pWssWlanUpdateReq->WssWlanPowerConstraint.u1Length);
    CAPWAP_PUT_1BYTE (pTxPkt,
                      pWssWlanUpdateReq->
                      WssWlanPowerConstraint.u1LocalPowerConstraint);

    CAPWAP_PUT_1BYTE (pTxPkt, pWssWlanUpdateReq->WssWlanQosCapab.u1ElementId);
    CAPWAP_PUT_1BYTE (pTxPkt, pWssWlanUpdateReq->WssWlanQosCapab.u1Length);
    CAPWAP_PUT_1BYTE (pTxPkt, pWssWlanUpdateReq->WssWlanQosCapab.u1QosInfo);

    CAPWAP_PUT_1BYTE (pTxPkt, pWssWlanUpdateReq->WssWlanEdcaParam.u1ElementId);
    CAPWAP_PUT_1BYTE (pTxPkt, pWssWlanUpdateReq->WssWlanEdcaParam.u1Length);
    CAPWAP_PUT_1BYTE (pTxPkt, pWssWlanUpdateReq->WssWlanEdcaParam.u1QosInfo);
    CAPWAP_PUT_2BYTE (pTxPkt,
                      pWssWlanUpdateReq->WssWlanEdcaParam.AC_BE_ParameterRecord.
                      u2TxOpLimit);
    CAPWAP_PUT_1BYTE (pTxPkt,
                      pWssWlanUpdateReq->WssWlanEdcaParam.AC_BE_ParameterRecord.
                      u1ACI_AIFSN);
    CAPWAP_PUT_1BYTE (pTxPkt,
                      pWssWlanUpdateReq->WssWlanEdcaParam.AC_BE_ParameterRecord.
                      u1ECWmin_ECWmax);
    CAPWAP_PUT_2BYTE (pTxPkt,
                      pWssWlanUpdateReq->WssWlanEdcaParam.AC_BK_ParameterRecord.
                      u2TxOpLimit);
    CAPWAP_PUT_1BYTE (pTxPkt,
                      pWssWlanUpdateReq->WssWlanEdcaParam.AC_BK_ParameterRecord.
                      u1ACI_AIFSN);
    CAPWAP_PUT_1BYTE (pTxPkt,
                      pWssWlanUpdateReq->WssWlanEdcaParam.AC_BK_ParameterRecord.
                      u1ECWmin_ECWmax);
    CAPWAP_PUT_2BYTE (pTxPkt,
                      pWssWlanUpdateReq->WssWlanEdcaParam.AC_VI_ParameterRecord.
                      u2TxOpLimit);
    CAPWAP_PUT_1BYTE (pTxPkt,
                      pWssWlanUpdateReq->WssWlanEdcaParam.AC_VI_ParameterRecord.
                      u1ACI_AIFSN);
    CAPWAP_PUT_1BYTE (pTxPkt,
                      pWssWlanUpdateReq->WssWlanEdcaParam.AC_VI_ParameterRecord.
                      u1ECWmin_ECWmax);
    CAPWAP_PUT_2BYTE (pTxPkt,
                      pWssWlanUpdateReq->WssWlanEdcaParam.AC_VO_ParameterRecord.
                      u2TxOpLimit);
    CAPWAP_PUT_1BYTE (pTxPkt,
                      pWssWlanUpdateReq->WssWlanEdcaParam.AC_VO_ParameterRecord.
                      u1ACI_AIFSN);
    CAPWAP_PUT_1BYTE (pTxPkt,
                      pWssWlanUpdateReq->WssWlanEdcaParam.AC_VO_ParameterRecord.
                      u1ECWmin_ECWmax);

#ifdef RSNA_WANTED
    /* Key Status equal to zero indicates that RSN elements 
     * are present */
    if ((pWssWlanUpdateReq->u1KeyStatus == 0)
        && (pWssWlanUpdateReq->RsnaIEElements.u1ElemId
            == WSSMAC_RSN_ELEMENT_ID))
    {
        CAPWAP_PUT_1BYTE (pTxPkt, pWssWlanUpdateReq->RsnaIEElements.u1ElemId);
        CAPWAP_PUT_1BYTE (pTxPkt, pWssWlanUpdateReq->RsnaIEElements.u1Len);
        CAPWAP_PUT_2BYTE (pTxPkt, pWssWlanUpdateReq->RsnaIEElements.u2Ver);
        CAPWAP_PUT_NBYTE (pTxPkt,
                          pWssWlanUpdateReq->RsnaIEElements.au1GroupCipherSuite,
                          RSNA_CIPHER_SUITE_LEN);
        CAPWAP_PUT_2BYTE (pTxPkt,
                          pWssWlanUpdateReq->
                          RsnaIEElements.u2PairwiseCipherSuiteCount);

        for (u1Index = 0;
             u1Index <
             pWssWlanUpdateReq->RsnaIEElements.u2PairwiseCipherSuiteCount;
             u1Index++)
        {
            CAPWAP_PUT_NBYTE (pTxPkt,
                              pWssWlanUpdateReq->
                              RsnaIEElements.aRsnaPwCipherDB[u1Index].
                              au1PairwiseCipher, RSNA_CIPHER_SUITE_LEN);
        }
        CAPWAP_PUT_2BYTE (pTxPkt,
                          pWssWlanUpdateReq->RsnaIEElements.u2AKMSuiteCount);

        for (u1Index = 0;
             u1Index < pWssWlanUpdateReq->RsnaIEElements.u2AKMSuiteCount;
             u1Index++)
        {
            CAPWAP_PUT_NBYTE (pTxPkt,
                              pWssWlanUpdateReq->
                              RsnaIEElements.aRsnaAkmDB[u1Index].au1AKMSuite,
                              RSNA_AKM_SELECTOR_LEN);
        }
        CAPWAP_PUT_2BYTE (pTxPkt, pWssWlanUpdateReq->RsnaIEElements.u2RsnCapab);

        if (pWssWlanUpdateReq->RsnaIEElements.u2PmkidCount != 0)
        {
            CAPWAP_PUT_2BYTE (pTxPkt,
                              pWssWlanUpdateReq->RsnaIEElements.u2PmkidCount);
            CAPWAP_PUT_NBYTE (pTxPkt,
                              pWssWlanUpdateReq->RsnaIEElements.aPmkidList,
                              RSNA_PMKID_LEN);
        }

#ifdef PMF_WANTED
        if (((pWssWlanUpdateReq->RsnaIEElements.
              u2RsnCapab & RSNA_CAPABILITY_MFPR) == RSNA_CAPABILITY_MFPR)
            ||
            ((pWssWlanUpdateReq->RsnaIEElements.
              u2RsnCapab & RSNA_CAPABILITY_MFPC) == RSNA_CAPABILITY_MFPC))
        {
            CAPWAP_PUT_NBYTE (pTxPkt,
                              pWssWlanUpdateReq->RsnaIEElements.
                              au1GroupMgmtCipherSuite, RSNA_CIPHER_SUITE_LEN);

        }
#endif
    }
#endif

    *pOffset =
        (UINT2) (CAPWAP_MSG_ELEM_TYPE_LEN + pWssWlanUpdateReq->u2MessageLength);
    return;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : CapwapAssembleIeeeWtpQos                                   *
         *                                                                           *
         * Description  : Assemble the capwap IEEE WTP QoS packet structure to       *
         *                transmittable  format.                                     *
         *                                                                           *
         * Input        : Capwap Control message structure                           *
         *                                                                           *
         * Output       : Capwap control message to be transmitted out               *
         *                                                                           *
         * Returns      : NONE                                                       *
         *****************************************************************************/
VOID
CapwapAssembleIeeeWtpQos (UINT1 *pTxPkt, tRadioIfQos * pRadioIfQos)
{
    UINT1               u1Index = 0;

    CAPWAP_PUT_2BYTE (pTxPkt, pRadioIfQos->u2MessageType);
    CAPWAP_PUT_2BYTE (pTxPkt, pRadioIfQos->u2MessageLength);
    CAPWAP_PUT_1BYTE (pTxPkt, pRadioIfQos->u1RadioId);
    CAPWAP_PUT_1BYTE (pTxPkt, pRadioIfQos->u1TaggingPolicy);
    for (u1Index = 0; u1Index < 4; u1Index++)
    {
        CAPWAP_PUT_1BYTE (pTxPkt, pRadioIfQos->u1QueueDepth[u1Index]);
        CAPWAP_PUT_2BYTE (pTxPkt, pRadioIfQos->u2CwMin[u1Index]);
        CAPWAP_PUT_2BYTE (pTxPkt, pRadioIfQos->u2CwMax[u1Index]);
        CAPWAP_PUT_1BYTE (pTxPkt, pRadioIfQos->u1Aifsn[u1Index]);
        CAPWAP_PUT_1BYTE (pTxPkt, pRadioIfQos->u1Prio[u1Index]);
        CAPWAP_PUT_1BYTE (pTxPkt, pRadioIfQos->u1Dscp[u1Index]);
    }
    return;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : CapwapAssembleIeeeWtpRadioConfiguration                    *
         *                                                                           *
         * Description  : Assemble the capwap IEEE WTP Radio config packet structure *
         *                to transmittable  format.                                  *
         *                                                                           *
         * Input        : Capwap Control message structure                           *
         *                                                                           *
         * Output       : Capwap control message to be transmitted out               *
         *                                                                           *
         * Returns      : SUCCESS/FAILURE                                            *
         *****************************************************************************/
VOID
CapwapAssembleIeeeWtpRadioConfiguration (UINT1 *pTxPkt,
                                         tRadioIfConfig * pRadioIfConfig)
{
    CAPWAP_PUT_2BYTE (pTxPkt, pRadioIfConfig->u2MessageType);
    CAPWAP_PUT_2BYTE (pTxPkt, pRadioIfConfig->u2MessageLength);
    CAPWAP_PUT_1BYTE (pTxPkt, pRadioIfConfig->u1RadioId);
    CAPWAP_PUT_1BYTE (pTxPkt, pRadioIfConfig->u1ShortPreamble);
    CAPWAP_PUT_1BYTE (pTxPkt, pRadioIfConfig->u1NoOfBssId);
    CAPWAP_PUT_1BYTE (pTxPkt, pRadioIfConfig->u1DtimPeriod);
    CAPWAP_PUT_NBYTE (pTxPkt, pRadioIfConfig->MacAddr, 6);
    CAPWAP_PUT_2BYTE (pTxPkt, pRadioIfConfig->u2BeaconPeriod);
    CAPWAP_PUT_NBYTE (pTxPkt, pRadioIfConfig->au1CountryString, 4);
    return;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : CapwapAssembleIeeeWTPRadioFailAlarm                        *
         *                                                                           *
         * Description  : Assemble the capwap IEEE WTP RadioFailAlaram packet        *
         *                structure to transmittable  format.                        *
         *                                                                           *
         * Input        : Capwap Control message structure                           *
         *                                                                           *
         * Output       : Capwap control message to be transmitted out               *
         *                                                                           *
         * Returns      : NONE                                                       *
         *****************************************************************************/
VOID
CapwapAssembleIeeeWTPRadioFailAlarm (UINT1 *pTxPkt,
                                     tRadioIfFailAlarm * pRadioIfFailAlarm)
{
    CAPWAP_PUT_2BYTE (pTxPkt, pRadioIfFailAlarm->u2MessageType);
    CAPWAP_PUT_2BYTE (pTxPkt, pRadioIfFailAlarm->u2MessageLength);
    CAPWAP_PUT_1BYTE (pTxPkt, pRadioIfFailAlarm->u1RadioId);
    CAPWAP_PUT_1BYTE (pTxPkt, pRadioIfFailAlarm->u1Type);
    CAPWAP_PUT_1BYTE (pTxPkt, pRadioIfFailAlarm->u1Status);
    return;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : CapwapAssembIeeeWLANConfigurationReq                       *
         *                                                                           *
         * Description  : Assemble the capwap IEEE WLAN Configuration Req  Message   *
         *                Element.                                                   *
         *                                                                           *
         * Input        : Capwap Control message structure                           *
         *                                                                           *
         * Output       : Capwap control message to be transmitted out               *
         *                                                                           *
         * Returns      : SUCCESS/FAILURE                                            *
         *****************************************************************************/
INT4
CapwapAssembIeeeWLANConfigurationReq (UINT2 u2MsgLen,
                                      tWssWlanConfigReq * pWlanConfReq,
                                      UINT1 *pMesgOut)
{
    UINT1              *pu1Buf = NULL;
    UINT2               u2Offset = 0;
    UINT1               u1Index = 0;

    CAPWAP_FN_ENTRY ();
    pu1Buf = pMesgOut;
    MEMSET (pu1Buf, CAPWAP_INIT_VAL, u2MsgLen);
    /* Assemble capwap Header */
    AssembleCapwapHeader (pu1Buf, &pWlanConfReq->capwapHdr);
    pu1Buf += (pWlanConfReq->capwapHdr.u2HdrLen);
    /* Assemble Control header */
    AssembleCapwapControlHeader (pu1Buf,
                                 WLAN_CONF_REQ,
                                 pWlanConfReq->u1SeqNum,
                                 pWlanConfReq->u2CapwapMsgElemenLen,
                                 pWlanConfReq->u1NumMsgBlocks);
    pu1Buf += CAPWAP_CTRL_HEADER_SIZE;
    /* Assemble the Message Element */
    if (pWlanConfReq->u1WlanOption == WSSWLAN_ADD_REQ)
    {
        CapwapAssembleIeeeAddWlan (pu1Buf,
                                   &pWlanConfReq->unWlanConfReq.WssWlanAddReq,
                                   &u2Offset);
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN +
             (pWlanConfReq->unWlanConfReq.WssWlanAddReq.u2MessageLength));
    }
    else if (pWlanConfReq->u1WlanOption == WSSWLAN_DEL_REQ)
    {
        CapwapAssembleIeeeDeleteWlan (pu1Buf,
                                      &pWlanConfReq->
                                      unWlanConfReq.WssWlanDeleteReq,
                                      &u2Offset);
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN +
             (pWlanConfReq->unWlanConfReq.WssWlanDeleteReq.u2MessageLength));
    }
    else if (pWlanConfReq->u1WlanOption == WSSWLAN_UPDATE_REQ)
    {
        CapwapAssembleIeeeUpdateWlan (pu1Buf,
                                      &pWlanConfReq->
                                      unWlanConfReq.WssWlanUpdateReq,
                                      &u2Offset);
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN +
             (pWlanConfReq->unWlanConfReq.WssWlanUpdateReq.u2MessageLength));
    }
#ifdef WPS_WANTED
    if (pWlanConfReq->RadioIfInfoElement.isPresent)
    {
        CapwapAssembleIeeeInfoElem (pu1Buf,
                                    &(pWlanConfReq->RadioIfInfoElement));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN +
             (pWlanConfReq->RadioIfInfoElement.u2MessageLength));
    }
#endif
    for (u1Index = 0; u1Index < VEND_CONF_MAX; u1Index++)
    {
        if (pWlanConfReq->vendSpec[u1Index].isOptional)
        {
            CapwapAssembleVendorSpecificPayload (pu1Buf,
                                                 &(pWlanConfReq->vendSpec
                                                   [u1Index]));
            pu1Buf +=
                (CAPWAP_MSG_ELEM_TYPE_LEN +
                 (pWlanConfReq->vendSpec[u1Index].u2MsgEleLen));
        }
    }
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : CapwapAssembleIeeeWLANConfigurationRsp                     *
         *                                                                           *
         * Description  : Assemble the capwap IEEE WLAN Configuration Resp packet    *
         *                structure to transmittable  format.                        *
         *                                                                           *
         * Input        : Capwap Control message structure                           *
         *                                                                           *
         * Output       : Capwap control message to be transmitted out               *
         *                                                                           *
         * Returns      : SUCCESS/FAILURE                                            *
         *****************************************************************************/
INT4
CapwapAssembleIeeeWLANConfigurationRsp (UINT2 u2MsgLen,
                                        tWssWlanConfigRsp * pWlanConfRsp,
                                        UINT1 *pu1Buf)
{

    MEMSET (pu1Buf, CAPWAP_INIT_VAL, u2MsgLen);
    /* Assemble capwap Header */
    AssembleCapwapHeader (pu1Buf, &pWlanConfRsp->capwapHdr);
    pu1Buf += (pWlanConfRsp->capwapHdr.u2HdrLen);
    /* Assemble Control header */
    AssembleCapwapControlHeader (pu1Buf,
                                 WLAN_CONF_RSP,
                                 pWlanConfRsp->u1SeqNum,
                                 pWlanConfRsp->u2CapwapMsgElemenLen,
                                 pWlanConfRsp->u1NumMsgBlocks);
    pu1Buf += CAPWAP_CTRL_HEADER_SIZE;

    capwapAssembleResultCode (pu1Buf, &(pWlanConfRsp->resultCode));
    pu1Buf +=
        (CAPWAP_MSG_ELEM_TYPE_LEN + (pWlanConfRsp->resultCode.u2MsgEleLen));
    if (pWlanConfRsp->WssWlanConfRsp.WssWlanRsp.u1IsPresent)
    {
        CapwapAssembleIeeeAssignedBssId (pu1Buf,
                                         &(pWlanConfRsp->
                                           WssWlanConfRsp.WssWlanRsp));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN +
             (pWlanConfRsp->WssWlanConfRsp.WssWlanRsp.u2Length));
    }

    if (pWlanConfRsp->vendSpec.isOptional)
    {
        CapwapAssembleVendorSpecificPayload (pu1Buf, &(pWlanConfRsp->vendSpec));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN + (pWlanConfRsp->vendSpec.u2MsgEleLen));
    }

    return OSIX_SUCCESS;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : CapwapAssembleDiscoveryReq                                 *
         *                                                                           *
         * Description  : Assemble the capwap Control packet structure to            *
         *                transmittable  format.                                     *
         *                                                                           *
         * Input        : Capwap Control message structure                           *
         *                                                                           *
         * Output       : Capwap control message to be transmitted out               *
         *                                                                           *
         * Returns      : SUCCESS/FAILURE                                            *
         *****************************************************************************/

INT4
CapwapAssembleDiscoveryReq (tDiscReq * pDiscReq, UINT1 *pu1Buf)
{
    UINT2               u2MsgElemLength = 0;

    CAPWAP_FN_ENTRY ();
    CAPWAP_MEMSET (pu1Buf, CAPWAP_INIT_VAL, CAPWAP_MAX_PKT_LEN);
    /* Assemble capwap Header */
    AssembleCapwapHeader (pu1Buf, &(pDiscReq->capwapHdr));
    pu1Buf += (pDiscReq->capwapHdr.u2HdrLen);
    /* Assemble Control header */
    AssembleCapwapControlHeader (pu1Buf,
                                 CAPWAP_DISC_REQ,
                                 pDiscReq->u1SeqNum,
                                 pDiscReq->u2CapwapMsgElemenLen,
                                 pDiscReq->u1NumMsgBlocks);
    pu1Buf += CAPWAP_CTRL_HEADER_SIZE;
    /* Assemble the location data */
    capwapAssembleDiscoveryType (pu1Buf, &pDiscReq->discType);
    pu1Buf += (CAPWAP_MSG_ELEM_TYPE_LEN + (pDiscReq->discType.u2MsgEleLen));

    capwapAssembleWtpFrameTunnelMode (pu1Buf, &(pDiscReq->wtpTunnelMode));
    pu1Buf +=
        (CAPWAP_MSG_ELEM_TYPE_LEN + (pDiscReq->wtpTunnelMode.u2MsgEleLen));

    capwapAssembleWtpMacType (pu1Buf, &(pDiscReq->wtpMacType));
    pu1Buf += (CAPWAP_MSG_ELEM_TYPE_LEN + (pDiscReq->wtpMacType.u2MsgEleLen));

    CapwapAssembleGenRadioParam (WTP_RADIO_INFO, 0, &pu1Buf, &u2MsgElemLength,
                                 0);

    CapwapAssembleWtpBoardData (pu1Buf, &(pDiscReq->wtpBoardData));
    pu1Buf += (CAPWAP_MSG_ELEM_TYPE_LEN + (pDiscReq->wtpBoardData.u2MsgEleLen));

    capwapAssembleWtpDescriptor (pu1Buf, &(pDiscReq->wtpDescriptor));
    pu1Buf +=
        (CAPWAP_MSG_ELEM_TYPE_LEN + (pDiscReq->wtpDescriptor.u2MsgEleLen));

    if (pDiscReq->vendSpec.isOptional)
    {
        CapwapAssembleVendorSpecificPayload (pu1Buf, &pDiscReq->vendSpec);
        pu1Buf += (CAPWAP_MSG_ELEM_TYPE_LEN + (pDiscReq->vendSpec.u2MsgEleLen));
    }
    if (pDiscReq->mtuDiscPad.isOptional)
    {
        pDiscReq->mtuDiscPad.u2MsgEleLen =
            (UINT2) (pDiscReq->mtuDiscPad.u2MsgEleLen - u2MsgElemLength);

        capwapAssembleMTUDiscoveryPadding (pu1Buf, &pDiscReq->mtuDiscPad);
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN + (pDiscReq->mtuDiscPad.u2MsgEleLen));
    }
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : CapwapAssembleDiscoveryRsp                                 *
         *                                                                           *
         * Description  : Assemble the capwap Control packet structure to            *
         *                transmittable  format.                                     *
         *                                                                           *
         * Input        : Capwap Control message structure                           *
         *                                                                           *
         * Output       : Capwap control message to be transmitted out               *
         *                                                                           *
         * Returns      : SUCCESS/FAILURE                                            *
         *****************************************************************************/

INT4
CapwapAssembleDiscoveryRsp (UINT2 mesgLen, tDiscRsp * pDiscRsp, UINT1 *pu1Buf)
{
    UINT2               u2MsgElemLength = 0;
    UINT1              *pu1TempBuf = NULL;
    UINT2               u2TempLength = 0;

    CAPWAP_MEMSET (pu1Buf, CAPWAP_INIT_VAL, mesgLen);
    /* Assemble capwap Header */
    pu1TempBuf = pu1Buf;
    AssembleCapwapHeader (pu1Buf, &(pDiscRsp->capwapHdr));
    pu1Buf += (pDiscRsp->capwapHdr.u2HdrLen);
    /* Assemble Control header */
    AssembleCapwapControlHeader (pu1Buf,
                                 CAPWAP_DISC_RSP,
                                 pDiscRsp->u1SeqNum,
                                 pDiscRsp->u2CapwapMsgElemenLen,
                                 pDiscRsp->u1NumMsgBlocks);
    pu1Buf += CAPWAP_CTRL_HEADER_SIZE;

    CapwapAssembleAcName (pu1Buf, &pDiscRsp->acName);
    pu1Buf += (CAPWAP_MSG_ELEM_TYPE_LEN + (pDiscRsp->acName.u2MsgEleLen));
    CapwapAssembleGenRadioParam (WTP_RADIO_INFO, 0, &pu1Buf, &u2MsgElemLength,
                                 pDiscRsp->u2WtpInternalId);
    pu1TempBuf += CAPWAP_MSG_OFFSET;
    CAPWAP_GET_2BYTE (u2TempLength, pu1TempBuf);
    u2TempLength = (UINT2) (u2TempLength + u2MsgElemLength);
    pu1TempBuf -= sizeof (UINT2);
    CAPWAP_PUT_2BYTE (pu1TempBuf, u2TempLength);

    capwapAssembleControlIPAddress (pu1Buf, &pDiscRsp->ctrlAddr);
    pu1Buf += (CAPWAP_MSG_ELEM_TYPE_LEN + (pDiscRsp->ctrlAddr.u2MsgEleLen));
    capwapAssembleAcDescriptor (pu1Buf, &pDiscRsp->acDesc);
    pu1Buf += (CAPWAP_MSG_ELEM_TYPE_LEN + (pDiscRsp->acDesc.u2MsgEleLen));

    /* Optional */
    if (pDiscRsp->acDesc.isOptional)
    {
        CapwapAssembleVendorSpecificPayload (pu1Buf, &pDiscRsp->vendSpec);
        pu1Buf += (CAPWAP_MSG_ELEM_TYPE_LEN + (pDiscRsp->vendSpec.u2MsgEleLen));
    }

    if (pDiscRsp->ipv4List.isOptional)
    {
        capwapAssembleIpv4List (pu1Buf, &pDiscRsp->ipv4List);
        pu1Buf += (CAPWAP_MSG_ELEM_TYPE_LEN + (pDiscRsp->ipv4List.u2MsgEleLen));
    }

    if (pDiscRsp->vendSpec.isOptional)
    {

        CapwapAssembleVendorSpecificPayload (pu1Buf, &(pDiscRsp->vendSpec));
        pu1Buf += (CAPWAP_MSG_ELEM_TYPE_LEN + (pDiscRsp->vendSpec.u2MsgEleLen));
    }

    return OSIX_SUCCESS;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : CapwapAssemblePrimaryDiscoveryReq                          *
         *                                                                           *
         * Description  : Assemble the capwap Control packet structure to            *
         *                transmittable  format.                                     *
         *                                                                           *
         * Input        : Capwap Control message structure                           *
         *                                                                           *
         * Output       : Capwap control message to be transmitted out               *
         *                                                                           *
         * Returns      : SUCCESS/FAILURE                                            *
         *****************************************************************************/
INT4
CapwapAssemblePrimaryDiscoveryReq (UINT2 mesgLen,
                                   tPriDiscReq * pPriDiscReq, UINT1 *pu1Buf)
{

    CAPWAP_FN_ENTRY ();
    UINT2               u2MsgElemLength = 0;
    CAPWAP_MEMSET (pu1Buf, CAPWAP_INIT_VAL, mesgLen);
    /* Assemble capwap Header */
    AssembleCapwapHeader (pu1Buf, &(pPriDiscReq->capwapHdr));
    pu1Buf += (pPriDiscReq->capwapHdr.u2HdrLen);
    /* Assemble Control header */
    AssembleCapwapControlHeader (pu1Buf,
                                 CAPWAP_PRIMARY_DISC_REQ,
                                 pPriDiscReq->u1SeqNum,
                                 pPriDiscReq->u2CapwapMsgElemenLen,
                                 pPriDiscReq->u1NumMsgBlocks);
    pu1Buf += CAPWAP_CTRL_HEADER_SIZE;
    /* Assemble the location data */
    capwapAssembleDiscoveryType (pu1Buf, &pPriDiscReq->discType);
    pu1Buf += (CAPWAP_MSG_ELEM_TYPE_LEN + (pPriDiscReq->discType.u2MsgEleLen));

    capwapAssembleWtpFrameTunnelMode (pu1Buf, &(pPriDiscReq->wtpTunnelMode));
    pu1Buf +=
        (CAPWAP_MSG_ELEM_TYPE_LEN + (pPriDiscReq->wtpTunnelMode.u2MsgEleLen));

    capwapAssembleWtpMacType (pu1Buf, &(pPriDiscReq->wtpMacType));
    pu1Buf +=
        (CAPWAP_MSG_ELEM_TYPE_LEN + (pPriDiscReq->wtpMacType.u2MsgEleLen));

    CapwapAssembleGenRadioParam (WTP_RADIO_INFO, 0, &pu1Buf, &u2MsgElemLength,
                                 0);

    CapwapAssembleWtpBoardData (pu1Buf, &(pPriDiscReq->wtpBoardData));
    pu1Buf +=
        (CAPWAP_MSG_ELEM_TYPE_LEN + (pPriDiscReq->wtpBoardData.u2MsgEleLen));

    capwapAssembleWtpDescriptor (pu1Buf, &(pPriDiscReq->wtpDescriptor));
    pu1Buf +=
        (CAPWAP_MSG_ELEM_TYPE_LEN + (pPriDiscReq->wtpDescriptor.u2MsgEleLen));
    if (pPriDiscReq->mtuDiscPad.isOptional)
    {
        pPriDiscReq->mtuDiscPad.u2MsgEleLen =
            (UINT2) (pPriDiscReq->mtuDiscPad.u2MsgEleLen - u2MsgElemLength);
        capwapAssembleMTUDiscoveryPadding (pu1Buf, &pPriDiscReq->mtuDiscPad);
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN + (pPriDiscReq->mtuDiscPad.u2MsgEleLen));
    }
    if (pPriDiscReq->vendSpec.isOptional)
    {
        pPriDiscReq->vendSpec.u2MsgEleLen =
            (UINT2) (pPriDiscReq->vendSpec.u2MsgEleLen - u2MsgElemLength);
        CapwapAssembleVendorSpecificPayload (pu1Buf, &pPriDiscReq->vendSpec);
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN + (pPriDiscReq->vendSpec.u2MsgEleLen));
    }
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : CapwapAssemblePrimaryDiscoveryRsp                                 *
         *                                                                           *
         * Description  : Assemble the capwap Control packet structure to            *
         *                transmittable  format.                                     *
         *                                                                           *
         * Input        : Capwap Control message structure                           *
         *                                                                           *
         * Output       : Capwap control message to be transmitted out               *
         *                                                                           *
         * Returns      : SUCCESS/FAILURE                                            *
         *****************************************************************************/

INT4
CapwapAssemblePrimaryDiscoveryRsp (UINT2 mesgLen,
                                   tPriDiscRsp * pPriDiscRsp, UINT1 *pu1Buf)
{

    CAPWAP_MEMSET (pu1Buf, CAPWAP_INIT_VAL, mesgLen);
    UINT2               u2MsgElemLength = 0;
    UINT1              *pu1TempBuf = NULL;
    UINT2               u2TempLength = 0;

    /* Assemble capwap Header */
    pu1TempBuf = pu1Buf;
    AssembleCapwapHeader (pu1Buf, &(pPriDiscRsp->capwapHdr));
    pu1Buf += (pPriDiscRsp->capwapHdr.u2HdrLen);
    /* Assemble Control header */
    AssembleCapwapControlHeader (pu1Buf,
                                 CAPWAP_PRIMARY_DISC_RSP,
                                 pPriDiscRsp->u1SeqNum,
                                 pPriDiscRsp->u2CapwapMsgElemenLen,
                                 pPriDiscRsp->u1NumMsgBlocks);
    pu1Buf += CAPWAP_CTRL_HEADER_SIZE;

    CapwapAssembleAcName (pu1Buf, &pPriDiscRsp->acName);
    pu1Buf += (CAPWAP_MSG_ELEM_TYPE_LEN + (pPriDiscRsp->acName.u2MsgEleLen));

    CapwapAssembleGenRadioParam (WTP_RADIO_INFO, 0, &pu1Buf, &u2MsgElemLength,
                                 pPriDiscRsp->u2WtpInternalId);
    pu1TempBuf += CAPWAP_MSG_OFFSET;
    CAPWAP_GET_2BYTE (u2TempLength, pu1TempBuf);
    u2TempLength = (UINT2) (u2TempLength + u2MsgElemLength);
    pu1TempBuf -= sizeof (UINT2);
    CAPWAP_PUT_2BYTE (pu1TempBuf, u2TempLength);

    capwapAssembleControlIPAddress (pu1Buf, &pPriDiscRsp->ctrlAddr);
    pu1Buf += (CAPWAP_MSG_ELEM_TYPE_LEN + (pPriDiscRsp->ctrlAddr.u2MsgEleLen));
    capwapAssembleAcDescriptor (pu1Buf, &pPriDiscRsp->acDesc);
    pu1Buf += (CAPWAP_MSG_ELEM_TYPE_LEN + (pPriDiscRsp->acDesc.u2MsgEleLen));

    /* Optional */
    if (pPriDiscRsp->acDesc.isOptional)
    {
        CapwapAssembleVendorSpecificPayload (pu1Buf, &pPriDiscRsp->vendSpec);
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN + (pPriDiscRsp->vendSpec.u2MsgEleLen));
    }

    if (pPriDiscRsp->ipv4List.isOptional)
    {
        capwapAssembleIpv4List (pu1Buf, &pPriDiscRsp->ipv4List);
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN + (pPriDiscRsp->ipv4List.u2MsgEleLen));
    }
    return OSIX_SUCCESS;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : CapwapAssembleJoinRequest                                  *
         *                                                                           *
         * Description  : Assemble the capwap Control packet structure to            *
         *                transmittable  format.                                     *
         *                                                                           *
         * Input        : Capwap Control message structure                           *
         *                                                                           *
         * Output       : Capwap control message to be transmitted out               *
         *                                                                           *
         * Returns      : SUCCESS/FAILURE                                            *
         *****************************************************************************/

INT4
CapwapAssembleJoinRequest (UINT2 mesgLen, tJoinReq * joinReq, UINT1 *pu1Buf)
{
    UINT2               u2MsgElemLength = 0;
    UINT1              *pu1TempBuf = NULL;
    UINT2               u2TempLength = 0;
    CAPWAP_FN_ENTRY ();
    CAPWAP_MEMSET (pu1Buf, CAPWAP_INIT_VAL, mesgLen);
    pu1TempBuf = pu1Buf;
    /* Assemble capwap Header */
    AssembleCapwapHeader (pu1Buf, &(joinReq->capwapHdr));
    /* Move the buffer pointer */
    pu1Buf += (joinReq->capwapHdr.u2HdrLen);

    /* Assemble Control header */
    AssembleCapwapControlHeader (pu1Buf,
                                 CAPWAP_JOIN_REQ,
                                 joinReq->u1SeqNum,
                                 joinReq->u2CapwapMsgElemenLen,
                                 joinReq->u1NumMsgBlocks);
    pu1Buf += CAPWAP_CTRL_HEADER_SIZE;
    /* Assemble the location data */
    CapwapAssembleLocationData (pu1Buf, &(joinReq->wtpLocation));
    pu1Buf += (CAPWAP_MSG_ELEM_TYPE_LEN + (joinReq->wtpLocation.u2MsgEleLen));
    capwapAssembleWtpName (pu1Buf, &(joinReq->wtpName));
    pu1Buf += (CAPWAP_MSG_ELEM_TYPE_LEN + (joinReq->wtpName.u2MsgEleLen));
    capwapAssembleSessionId (pu1Buf, joinReq->sessionID);
    pu1Buf += (CAPWAP_MSG_ELEM_TYPE_LEN + 16);
    capwapAssembleWtpFrameTunnelMode (pu1Buf, &(joinReq->wtpTunnelMode));
    pu1Buf += (CAPWAP_MSG_ELEM_TYPE_LEN + (joinReq->wtpTunnelMode.u2MsgEleLen));
    capwapAssembleWtpMacType (pu1Buf, &(joinReq->wtpMacType));
    pu1Buf += (CAPWAP_MSG_ELEM_TYPE_LEN + (joinReq->wtpMacType.u2MsgEleLen));
    CapwapAssembleGenRadioParam (WTP_RADIO_INFO, 0, &pu1Buf, &u2MsgElemLength,
                                 0);
    pu1TempBuf += CAPWAP_MSG_OFFSET;
    CAPWAP_GET_2BYTE (u2TempLength, pu1TempBuf);
    u2TempLength = (UINT2) (u2TempLength + u2MsgElemLength);
    pu1TempBuf -= sizeof (UINT2);
    CAPWAP_PUT_2BYTE (pu1TempBuf, u2TempLength);

    capwapAssembleEcnSupport (pu1Buf, &(joinReq->ecnSupport));
    pu1Buf += (CAPWAP_MSG_ELEM_TYPE_LEN + (joinReq->ecnSupport.u2MsgEleLen));
    capwapAssembleLocalIPAddress (pu1Buf, &(joinReq->incomingAddress));
    pu1Buf +=
        (CAPWAP_MSG_ELEM_TYPE_LEN + (joinReq->incomingAddress.u2MsgEleLen));
    CapwapAssembleWtpBoardData (pu1Buf, &(joinReq->wtpBoardData));
    pu1Buf += (CAPWAP_MSG_ELEM_TYPE_LEN + (joinReq->wtpBoardData.u2MsgEleLen));
    capwapAssembleWtpDescriptor (pu1Buf, &(joinReq->wtpDescriptor));
    pu1Buf += (CAPWAP_MSG_ELEM_TYPE_LEN + (joinReq->wtpDescriptor.u2MsgEleLen));

    /* Optional message elements */
    /* Transport protocol */
    if (joinReq->transProto.isOptional)
    {
        capwapAssembleTrasnportProtocol (pu1Buf, &(joinReq->transProto));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN + (joinReq->transProto.u2MsgEleLen));
    }
    /* Max message Element */
    if (joinReq->msgLength.isOptional)
    {
        capwapAssembleMaxMessageLength (pu1Buf, &(joinReq->msgLength));
        pu1Buf += (CAPWAP_MSG_ELEM_TYPE_LEN + (joinReq->msgLength.u2MsgEleLen));
    }

    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : CapwapAssembleJoinResponse                                 *
         *                                                                           *
         * Description  : Assemble the capwap Control packet structure to            *
         *                transmittable  format.                                     *
         *                                                                           *
         * Input        : Capwap Control message structure                           *
         *                                                                           *
         * Output       : Capwap control message to be transmitted out               *
         *                                                                           *
         * Returns      : SUCCESS/FAILURE                                            *
         *****************************************************************************/

INT4
CapwapAssembleJoinResponse (UINT2 mesgLen, tJoinRsp * joinResp, UINT1 *pMesgOut)
{
    UINT1              *pu1Buf = NULL;
    UINT2               u2MsgElemLength = 0;
    UINT1              *pu1TempBuf = NULL;
    UINT2               u2TempLength = 0;

    pu1Buf = pMesgOut;

    CAPWAP_FN_ENTRY ();
    CAPWAP_MEMSET (pu1Buf, CAPWAP_INIT_VAL, mesgLen);
    pu1TempBuf = pu1Buf;
    /* Assemble capwap Header */
    AssembleCapwapHeader (pu1Buf, &(joinResp->capwapHdr));
    /* Move the buffer pointer */
    pu1Buf += joinResp->capwapHdr.u2HdrLen;

    /* Assemble Control header */
    AssembleCapwapControlHeader (pu1Buf,
                                 CAPWAP_JOIN_RSP,
                                 joinResp->u1SeqNum,
                                 joinResp->u2CapwapMsgElemenLen,
                                 joinResp->u1NumMsgBlocks);
    pu1Buf += CAPWAP_CTRL_HEADER_SIZE;

    /* Assemble the location data */
    capwapAssembleResultCode (pu1Buf, &(joinResp->resultCode));
    pu1Buf += (CAPWAP_MSG_ELEM_TYPE_LEN + (joinResp->resultCode.u2MsgEleLen));
    CapwapAssembleAcName (pu1Buf, &(joinResp->acName));
    pu1Buf += (CAPWAP_MSG_ELEM_TYPE_LEN + (joinResp->acName.u2MsgEleLen));
    capwapAssembleEcnSupport (pu1Buf, &(joinResp->ecnSupport));
    pu1Buf += (CAPWAP_MSG_ELEM_TYPE_LEN + (joinResp->ecnSupport.u2MsgEleLen));
    CapwapAssembleGenRadioParam (WTP_RADIO_INFO, 0, &pu1Buf, &u2MsgElemLength,
                                 joinResp->u2IntProfileId);
    pu1TempBuf += CAPWAP_MSG_OFFSET;
    CAPWAP_GET_2BYTE (u2TempLength, pu1TempBuf);
    u2TempLength = (UINT2) (u2TempLength + u2MsgElemLength);
    pu1TempBuf -= sizeof (UINT2);
    CAPWAP_PUT_2BYTE (pu1TempBuf, u2TempLength);

    capwapAssembleControlIPAddress (pu1Buf, &(joinResp->controlIpAddr));
    pu1Buf +=
        (CAPWAP_MSG_ELEM_TYPE_LEN + (joinResp->controlIpAddr.u2MsgEleLen));
    capwapAssembleLocalIPAddress (pu1Buf, &(joinResp->localIpAddr));
    pu1Buf += (CAPWAP_MSG_ELEM_TYPE_LEN + (joinResp->localIpAddr.u2MsgEleLen));
    capwapAssembleAcDescriptor (pu1Buf, &(joinResp->acDesc));
    pu1Buf += (CAPWAP_MSG_ELEM_TYPE_LEN + (joinResp->acDesc.u2MsgEleLen));
    if (joinResp->imageId.isOptional)
    {
        capwapAssembleImageIdentifier (pu1Buf, &(joinResp->imageId));
        pu1Buf += (CAPWAP_MSG_ELEM_TYPE_LEN + (joinResp->imageId.u2MsgEleLen));
    }

    /* Optional message elements */
    /* Transport protocol */
    if (joinResp->transProto.isOptional)
    {
        capwapAssembleTrasnportProtocol (pu1Buf, &(joinResp->transProto));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN + (joinResp->transProto.u2MsgEleLen));
    }
    /* Vendor Specific Payload */
    if (joinResp->vendSpec.isOptional)
    {

        CapwapAssembleVendorSpecificPayload (pu1Buf, &(joinResp->vendSpec));
        pu1Buf += (CAPWAP_MSG_ELEM_TYPE_LEN + (joinResp->vendSpec.u2MsgEleLen));
    }
    /* Max message Element */
    if (joinResp->msgLength.isOptional)
    {
        capwapAssembleMaxMessageLength (pu1Buf, &(joinResp->msgLength));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN + (joinResp->msgLength.u2MsgEleLen));
    }

    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : CapwapAssembleConfigStatusRequest                          *
         *                                                                           *
         * Description  : Assemble the capwap Control packet structure to            *
         *                transmittable  format.                                     *
         *                                                                           *
         * Input        : Capwap Control message structure                           *
         *                                                                           *
         * Output       : Capwap control message to be transmitted out               *
         *                                                                           *
         * Returns      : SUCCESS/FAILURE                                            *
         *****************************************************************************/

INT4
CapwapAssembleConfigStatusRequest (UINT2 mesgLen,
                                   tConfigStatusReq * confStatusReq,
                                   UINT1 *pMesgOut)
{
    UINT1              *pu1Buf = NULL;
    UINT1               u1Index = 0;
    UINT2               u2MsgElemLength = 0;
    UINT1              *pu1TempBuf = NULL;
    UINT2               u2TotalMsgElemLength = 0;
    UINT2               u2TempLength = 0;

    CAPWAP_FN_ENTRY ();
    pu1Buf = pMesgOut;
    CAPWAP_MEMSET (pu1Buf, CAPWAP_INIT_VAL, mesgLen);
    pu1TempBuf = pu1Buf;
    /* Assemble capwap Header */
    AssembleCapwapHeader (pu1Buf, &(confStatusReq->capwapHdr));
    /* Move the buffer pointer */
    pu1Buf += (confStatusReq->capwapHdr.u2HdrLen);

    CAPWAP_TRC1 (CAPWAP_MGMT_TRC, "Status Req Msg Len = %d\r\n",
                 confStatusReq->u2CapwapMsgElemenLen);
    SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4CapwapSysLogId,
                  "Status Req Msg Len = %d\r\n",
                  confStatusReq->u2CapwapMsgElemenLen));

    /* Assemble Control header */
    AssembleCapwapControlHeader (pu1Buf,
                                 CAPWAP_CONF_STAT_REQ,
                                 confStatusReq->u1SeqNum,
                                 confStatusReq->u2CapwapMsgElemenLen,
                                 confStatusReq->u1NumMsgBlocks);
    pu1Buf += CAPWAP_CTRL_HEADER_SIZE;
    /* Mandatory elements */
    /* Assemble the AC Name */
    CapwapAssembleAcName (pu1Buf, &(confStatusReq->acName));
    pu1Buf += (CAPWAP_MSG_ELEM_TYPE_LEN + (confStatusReq->acName.u2MsgEleLen));
    /* Assemble Radio Admin State */
    capwapAssembleRadioAdminState (pu1Buf, &(confStatusReq->radAdminState));
    pu1Buf +=
        (CAPWAP_MSG_ELEM_TYPE_LEN +
         (confStatusReq->radAdminState.u2MessageLength));
    /* Assemble Statistics timer */
    CapwapAssembleStatisticsTimer (pu1Buf, &(confStatusReq->statsTimer));
    pu1Buf +=
        (CAPWAP_MSG_ELEM_TYPE_LEN + (confStatusReq->statsTimer.u2MsgEleLen));
    /* Assemble WTP Reboot Statistics */
    capwapAssembleWtpRebootStatistics (pu1Buf, &(confStatusReq->rebootStats));
    pu1Buf +=
        (CAPWAP_MSG_ELEM_TYPE_LEN + (confStatusReq->rebootStats.u2MsgEleLen));

    /* Optional message elements */
    /* Transport protocol */
    if (confStatusReq->transProto.isOptional)
    {
        capwapAssembleTrasnportProtocol (pu1Buf, &(confStatusReq->transProto));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN +
             (confStatusReq->transProto.u2MsgEleLen));
    }

    CapwapAssembleGenRadioParam (IEEE_ANTENNA, 0, &pu1Buf, &u2MsgElemLength, 0);
    u2TotalMsgElemLength = (UINT2) (u2TotalMsgElemLength + u2MsgElemLength);
    CapwapAssembleGenRadioParam (IEEE_DIRECT_SEQUENCE_CONTROL, 0, &pu1Buf,
                                 &u2MsgElemLength, 0);
    u2TotalMsgElemLength = (UINT2) (u2TotalMsgElemLength + u2MsgElemLength);
    CapwapAssembleGenRadioParam (IEEE_MAC_OPERATION, 0, &pu1Buf,
                                 &u2MsgElemLength, 0);
    u2TotalMsgElemLength = (UINT2) (u2TotalMsgElemLength + u2MsgElemLength);
    CapwapAssembleGenRadioParam (IEEE_MULTIDOMAIN_CAPABILITY, 0, &pu1Buf,
                                 &u2MsgElemLength, 0);
    u2TotalMsgElemLength = (UINT2) (u2TotalMsgElemLength + u2MsgElemLength);
    CapwapAssembleGenRadioParam (IEEE_OFDM_CONTROL, 0, &pu1Buf,
                                 &u2MsgElemLength, 0);
    u2TotalMsgElemLength = (UINT2) (u2TotalMsgElemLength + u2MsgElemLength);
    CapwapAssembleGenRadioParam (IEEE_SUPPORTED_RATES, 0, &pu1Buf,
                                 &u2MsgElemLength, 0);
    u2TotalMsgElemLength = (UINT2) (u2TotalMsgElemLength + u2MsgElemLength);
    CapwapAssembleGenRadioParam (IEEE_TX_POWER, 0, &pu1Buf, &u2MsgElemLength,
                                 0);
    u2TotalMsgElemLength = (UINT2) (u2TotalMsgElemLength + u2MsgElemLength);
    CapwapAssembleGenRadioParam (IEEE_TX_POWERLEVEL, 0, &pu1Buf,
                                 &u2MsgElemLength, 0);
    u2TotalMsgElemLength = (UINT2) (u2TotalMsgElemLength + u2MsgElemLength);
    CapwapAssembleGenRadioParam (IEEE_WTP_RADIO_CONFIGURATION, 0, &pu1Buf,
                                 &u2MsgElemLength, 0);
    u2TotalMsgElemLength = (UINT2) (u2TotalMsgElemLength + u2MsgElemLength);
    CapwapAssembleGenRadioParam (WTP_RADIO_INFO, 0, &pu1Buf,
                                 &u2MsgElemLength, 0);
    u2TotalMsgElemLength = (UINT2) (u2TotalMsgElemLength + u2MsgElemLength);
    CapwapAssembleGenRadioParam (IEEE_INFORMATION_ELEMENT,
                                 DOT11_INFO_ELEM_HTCAPABILITY,
                                 &pu1Buf, &u2MsgElemLength, 0);
    u2TotalMsgElemLength = (UINT2) (u2TotalMsgElemLength + u2MsgElemLength);
    CapwapAssembleGenRadioParam (IEEE_INFORMATION_ELEMENT,
                                 DOT11_INFO_ELEM_HTOPERATION,
                                 &pu1Buf, &u2MsgElemLength, 0);
    u2TotalMsgElemLength = (UINT2) (u2TotalMsgElemLength + u2MsgElemLength);
    CapwapAssembleGenRadioParam (IEEE_INFORMATION_ELEMENT,
                                 DOT11AC_INFO_ELEM_VHTCAPABILITY,
                                 &pu1Buf, &u2MsgElemLength, 0);
    u2TotalMsgElemLength = (UINT2) (u2TotalMsgElemLength + u2MsgElemLength);
    CapwapAssembleGenRadioParam (IEEE_INFORMATION_ELEMENT,
                                 DOT11AC_INFO_ELEM_VHTOPERATION,
                                 &pu1Buf, &u2MsgElemLength, 0);
    u2TotalMsgElemLength = (UINT2) (u2TotalMsgElemLength + u2MsgElemLength);

    if (confStatusReq->isVendorOptional)
    {
#ifdef RFMGMT_WANTED
        for (u1Index = 0; u1Index < VENDOR_CH_SWITCH_STATUS_TYPE; u1Index++)
#else
        for (u1Index = 0; u1Index < 5; u1Index++)
#endif
        {
            CapwapAssembleGenRadioParam (VENDOR_SPECIFIC_PAYLOAD,
                                         u1Index, &pu1Buf, &u2MsgElemLength, 0);
            if (u2MsgElemLength != 0)
            {
                u2TotalMsgElemLength = (UINT2) (u2TotalMsgElemLength +
                                                u2MsgElemLength);
            }

        }
#ifdef RFMGMT_WANTED
        CapwapAssembleGenRadioParam (VENDOR_SPECIFIC_PAYLOAD,
                                     VENDOR_CH_SCANNED_LIST, &pu1Buf,
                                     &u2MsgElemLength, 0);
        if (u2MsgElemLength != 0)
        {
            u2TotalMsgElemLength = (UINT2) (u2TotalMsgElemLength +
                                            u2MsgElemLength);
        }
        CapwapAssembleGenRadioParam (VENDOR_SPECIFIC_PAYLOAD,
                                     VENDOR_SPECT_MGMT_TPC_TYPE, &pu1Buf,
                                     &u2MsgElemLength, 0);
        if (u2MsgElemLength != 0)
        {
            u2TotalMsgElemLength = (UINT2) (u2TotalMsgElemLength +
                                            u2MsgElemLength);
        }
        CapwapAssembleGenRadioParam (VENDOR_SPECIFIC_PAYLOAD,
                                     VENDOR_SPECTRUM_MGMT_DFS_TYPE, &pu1Buf,
                                     &u2MsgElemLength, 0);
        if (u2MsgElemLength != 0)
        {
            u2TotalMsgElemLength = (UINT2) (u2TotalMsgElemLength +
                                            u2MsgElemLength);
        }
#endif
        CapwapAssembleGenRadioParam (VENDOR_SPECIFIC_PAYLOAD,
                                     (UINT1) VENDOR_DFS_CHANNEL_STATS, &pu1Buf,
                                     &u2MsgElemLength, 0);
        if (u2MsgElemLength != 0)
        {
            u2TotalMsgElemLength = (UINT2) (u2TotalMsgElemLength +
                                            u2MsgElemLength);
        }

    }

    pu1TempBuf += CAPWAP_MSG_OFFSET;
    CAPWAP_GET_2BYTE (u2TempLength, pu1TempBuf);
    u2TempLength = (UINT2) (u2TempLength + u2TotalMsgElemLength);
    pu1TempBuf -= sizeof (UINT2);
    CAPWAP_PUT_2BYTE (pu1TempBuf, u2TempLength);

    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : CapwapAssembleConfigStatusResponse                          *
         *                                                                           *
         * Description  : Assemble the capwap Control packet structure to            *
         *                transmittable  format.                                     *
         *                                                                           *
         * Input        : Capwap Control message structure                           *
         *                                                                           *
         * Output       : Capwap control message to be transmitted out               *
         *                                                                           *
         * Returns      : SUCCESS/FAILURE                                            *
         *****************************************************************************/

INT4
CapwapAssembleConfigStatusResponse (UINT2 mesgLen,
                                    tConfigStatusRsp * confStatusResp,
                                    UINT1 *pMesgOut)
{
    UINT1              *pu1Buf = NULL;
    UINT1               u1LoopCnt = 0;
    UINT2               u2MsgElemLength = 0;
    UINT1              *pu1TempBuf = NULL;
    UINT1              *pu1TempBuf1 = NULL;
    UINT2               u2TotalMsgElemLength = 0;
    UINT2               u2TempLength = 0;

    pu1Buf = pMesgOut;
    CAPWAP_FN_ENTRY ();
    CAPWAP_MEMSET (pu1Buf, CAPWAP_INIT_VAL, mesgLen);
    pu1TempBuf = pu1Buf;
    /* Assemble capwap Header */
    AssembleCapwapHeader (pu1Buf, &(confStatusResp->capwapHdr));
    /* Move the buffer pointer */
    pu1Buf += (confStatusResp->capwapHdr.u2HdrLen);

    /* Assemble Control header */
    AssembleCapwapControlHeader (pu1Buf,
                                 CAPWAP_CONF_STAT_RSP,
                                 confStatusResp->u1SeqNum,
                                 confStatusResp->u2CapwapMsgElemenLen,
                                 confStatusResp->u1NumMsgBlocks);
    pu1Buf += CAPWAP_CTRL_HEADER_SIZE;

    pu1TempBuf1 = pu1Buf;

    /* Mandatory elements */
    /* Assemble the Capwap timer */
    capwapAssembleCapwapTimer (pu1Buf, &(confStatusResp->capwapTimer));
    pu1Buf +=
        (CAPWAP_MSG_ELEM_TYPE_LEN + (confStatusResp->capwapTimer.u2MsgEleLen));
    /* Assemble Decryption error report */
    capwapAssembleDecryptionErrorReport (pu1Buf,
                                         &(confStatusResp->decryErrPeriod));
    pu1Buf +=
        (CAPWAP_MSG_ELEM_TYPE_LEN +
         (confStatusResp->decryErrPeriod.u2MsgEleLen));
    /* Assemble Idle timeout */
    capwapAssembleIdleTimeout (pu1Buf, &(confStatusResp->idleTimeout));
    pu1Buf +=
        (CAPWAP_MSG_ELEM_TYPE_LEN + (confStatusResp->idleTimeout.u2MsgEleLen));
    /* Assemble WTP Fallback */
    capwapAssembleWtpFallback (pu1Buf, &(confStatusResp->wtpFallback));
    pu1Buf +=
        (CAPWAP_MSG_ELEM_TYPE_LEN + (confStatusResp->wtpFallback.u2MsgEleLen));
    /* Assemble IPv4 list */
    if (confStatusResp->ipList.u2Afi == CAPWAP_INET_AFI_IPV4)
    {
        capwapAssembleIpv4List (pu1Buf, &(confStatusResp->ipList));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN + (confStatusResp->ipList.u2MsgEleLen));
    }
    else if (confStatusResp->ipList.u2Afi == CAPWAP_INET_AFI_IPV6)
    {
        /* Assemble Ipv6 list */
        capwapAssembleIpv6List (pu1Buf, &(confStatusResp->ipList));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN + (confStatusResp->ipList.u2MsgEleLen));
    }
    else
    {
        /* Donot assmeble AC IP list */
    }

    /* Optional Message elements */
    /* Ac Name With Priority */
    for (u1LoopCnt = 0; u1LoopCnt < 3; u1LoopCnt++)
    {
        if (confStatusResp->acNameWithPrio[u1LoopCnt].isOptional)
        {
            capwapAssembleAcnameWithPriority (pu1Buf,
                                              &(confStatusResp->acNameWithPrio
                                                [u1LoopCnt]));
            pu1Buf +=
                (CAPWAP_MSG_ELEM_TYPE_LEN +
                 (confStatusResp->acNameWithPrio[u1LoopCnt].u2MsgEleLen));
        }
    }

    if (confStatusResp->RadioIfAntenna.isPresent)
    {
        CapwapAssembleGenRadioParam (IEEE_ANTENNA, 0, &pu1Buf, &u2MsgElemLength,
                                     confStatusResp->u2IntProfileId);
        u2TotalMsgElemLength = (UINT2) (u2TotalMsgElemLength + u2MsgElemLength);
    }

    if (confStatusResp->RadioIfDSSPhy.isPresent)
    {
        CapwapAssembleGenRadioParam (IEEE_DIRECT_SEQUENCE_CONTROL, 0, &pu1Buf,
                                     &u2MsgElemLength,
                                     confStatusResp->u2IntProfileId);
        u2TotalMsgElemLength = (UINT2) (u2TotalMsgElemLength + u2MsgElemLength);

    }
    if (confStatusResp->RadioIfMacOperation.isPresent)
    {
        CapwapAssembleGenRadioParam (IEEE_MAC_OPERATION, 0, &pu1Buf,
                                     &u2MsgElemLength,
                                     confStatusResp->u2IntProfileId);
        u2TotalMsgElemLength = (UINT2) (u2TotalMsgElemLength + u2MsgElemLength);
    }
    if (confStatusResp->RadioIfMultiDomainCap.isPresent)
    {
        CapwapAssembleGenRadioParam (IEEE_MULTIDOMAIN_CAPABILITY, 0, &pu1Buf,
                                     &u2MsgElemLength,
                                     confStatusResp->u2IntProfileId);
        u2TotalMsgElemLength = (UINT2) (u2TotalMsgElemLength + u2MsgElemLength);
    }
    if (confStatusResp->RadioIfOFDMPhy.isPresent)
    {
        CapwapAssembleGenRadioParam (IEEE_OFDM_CONTROL, 0, &pu1Buf,
                                     &u2MsgElemLength,
                                     confStatusResp->u2IntProfileId);
        u2TotalMsgElemLength = (UINT2) (u2TotalMsgElemLength + u2MsgElemLength);
    }
    if (confStatusResp->RadioIfRateSet.isPresent)
    {
        CapwapAssembleGenRadioParam (IEEE_RATE_SET, 0, &pu1Buf,
                                     &u2MsgElemLength,
                                     confStatusResp->u2IntProfileId);
        u2TotalMsgElemLength = (UINT2) (u2TotalMsgElemLength + u2MsgElemLength);

    }
    if (confStatusResp->RadioIfSupportedRate.isPresent)
    {
        CapwapAssembleGenRadioParam (IEEE_SUPPORTED_RATES, 0, &pu1Buf,
                                     &u2MsgElemLength,
                                     confStatusResp->u2IntProfileId);
        u2TotalMsgElemLength = (UINT2) (u2TotalMsgElemLength + u2MsgElemLength);
    }
    if (confStatusResp->RadioIfTxPower.isPresent)
    {
        CapwapAssembleGenRadioParam (IEEE_TX_POWER, 0, &pu1Buf,
                                     &u2MsgElemLength,
                                     confStatusResp->u2IntProfileId);
        u2TotalMsgElemLength = (UINT2) (u2TotalMsgElemLength + u2MsgElemLength);
    }
    if (confStatusResp->RadioIfQos.isPresent)
    {
        CapwapAssembleGenRadioParam (IEEE_WTP_QOS, 0, &pu1Buf, &u2MsgElemLength,
                                     confStatusResp->u2IntProfileId);
        u2TotalMsgElemLength = (UINT2) (u2TotalMsgElemLength + u2MsgElemLength);
    }
    if (confStatusResp->RadioIfConfig.isPresent)
    {
        CapwapAssembleGenRadioParam (IEEE_WTP_RADIO_CONFIGURATION, 0, &pu1Buf,
                                     &u2MsgElemLength,
                                     confStatusResp->u2IntProfileId);
        u2TotalMsgElemLength = (UINT2) (u2TotalMsgElemLength + u2MsgElemLength);
    }

    if ((confStatusResp->RadioIfInfoElem.isPresent) &&
        (confStatusResp->RadioIfInfoElem.unInfoElem.HTCapability.u1ElemId ==
         DOT11_INFO_ELEM_HTCAPABILITY))
    {
        CapwapAssembleGenRadioParam (IEEE_INFORMATION_ELEMENT,
                                     DOT11_INFO_ELEM_HTCAPABILITY,
                                     &pu1Buf, &u2MsgElemLength,
                                     confStatusResp->u2IntProfileId);

        u2TotalMsgElemLength = (UINT2) (u2TotalMsgElemLength + u2MsgElemLength);
    }
    if ((confStatusResp->RadioIfInfoElem.isPresent) &&
        (confStatusResp->RadioIfInfoElem.unInfoElem.HTOperation.u1ElemId ==
         DOT11_INFO_ELEM_HTOPERATION))
    {
        CapwapAssembleGenRadioParam (IEEE_INFORMATION_ELEMENT,
                                     DOT11_INFO_ELEM_HTOPERATION,
                                     &pu1Buf, &u2MsgElemLength,
                                     confStatusResp->u2IntProfileId);

        u2TotalMsgElemLength = (UINT2) (u2TotalMsgElemLength + u2MsgElemLength);
    }

    if ((confStatusResp->RadioIfInfoElem.isPresent) &&
        (confStatusResp->RadioIfInfoElem.unInfoElem.VHTCapability.u1ElemId ==
         DOT11AC_INFO_ELEM_VHTCAPABILITY))
    {
        CapwapAssembleGenRadioParam (IEEE_INFORMATION_ELEMENT,
                                     DOT11AC_INFO_ELEM_VHTCAPABILITY,
                                     &pu1Buf, &u2MsgElemLength,
                                     confStatusResp->u2IntProfileId);

        u2TotalMsgElemLength = (UINT2) (u2TotalMsgElemLength + u2MsgElemLength);
    }

    if ((confStatusResp->RadioIfInfoElem.isPresent) &&
        (confStatusResp->RadioIfInfoElem.unInfoElem.VHTOperation.u1ElemId ==
         DOT11AC_INFO_ELEM_VHTOPERATION))
    {
        CapwapAssembleGenRadioParam (IEEE_INFORMATION_ELEMENT,
                                     DOT11AC_INFO_ELEM_VHTOPERATION,
                                     &pu1Buf, &u2MsgElemLength,
                                     confStatusResp->u2IntProfileId);

        u2TotalMsgElemLength = (UINT2) (u2TotalMsgElemLength + u2MsgElemLength);
    }
#ifdef RFMGMT_WANTED
    for (u1LoopCnt = 0; u1LoopCnt <= VENDOR_CH_SWITCH_STATUS_TYPE; u1LoopCnt++)
#else
    for (u1LoopCnt = 0; u1LoopCnt < 5; u1LoopCnt++)
#endif
    {
        CapwapAssembleGenRadioParam (VENDOR_SPECIFIC_PAYLOAD,
                                     u1LoopCnt, &pu1Buf, &u2MsgElemLength,
                                     confStatusResp->u2IntProfileId);

    }
#ifdef RFMGMT_WANTED
    CapwapAssembleGenRadioParam (VENDOR_SPECIFIC_PAYLOAD,
                                 VENDOR_SPECT_MGMT_TPC_TYPE, &pu1Buf,
                                 &u2MsgElemLength,
                                 confStatusResp->u2IntProfileId);

    CapwapAssembleGenRadioParam (VENDOR_SPECIFIC_PAYLOAD,
                                 VENDOR_SPECTRUM_MGMT_DFS_TYPE, &pu1Buf,
                                 &u2MsgElemLength,
                                 confStatusResp->u2IntProfileId);
#ifdef ROGUEAP_WANTED
    CapwapAssembleGenRadioParam (VENDOR_SPECIFIC_PAYLOAD,
                                 VENDOR_ROUGE_AP, &pu1Buf,
                                 &u2MsgElemLength,
                                 confStatusResp->u2IntProfileId);
#endif
#endif
    CapwapAssembleGenRadioParam (VENDOR_SPECIFIC_PAYLOAD,
                                 VENDOR_DOT11N_CONFIG_TYPE, &pu1Buf,
                                 &u2MsgElemLength,
                                 confStatusResp->u2IntProfileId);

    u2TempLength = (UINT2) ((pu1Buf - pu1TempBuf1) + CAPWAP_CONFIG_RESP_OFFSET);
    pu1TempBuf += CAPWAP_MSG_OFFSET;
    CAPWAP_PUT_2BYTE (pu1TempBuf, u2TempLength);

    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : CapwapAssembleUnknownRsp                                   *
         *                                                                           *
         * Description  : Assemble the capwap Unknown Resp packet structure to       *
         *                transmittable  format.                                     *
         *                                                                           *
         * Input        : Capwap Control message structure                           *
         *                                                                           *
         * Output       : Capwap control message to be transmitted out               *
         *                                                                           *
         * Returns      : SUCCESS/FAILURE                                            *
         *****************************************************************************/
INT4
CapwapAssembleUnknownRsp (tUnknownRsp * pUnknownRsp,
                          tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UINT1              *pu1Buf = NULL;
    UINT1               u1IsNotLinear = OSIX_FALSE;
    UINT1               au1Frame[CAPWAP_MAX_PKT_LEN];

    CAPWAP_FN_ENTRY ();

    pu1Buf = CAPWAP_BUF_IF_LINEAR (pBuf, 0, CAPWAP_MAX_PKT_LEN);
    if (pu1Buf == NULL)
    {
        MEMSET (au1Frame, 0x00, CAPWAP_MAX_PKT_LEN);
        CAPWAP_COPY_FROM_BUF (pBuf, au1Frame, 0, CAPWAP_MAX_PKT_LEN);
        pu1Buf = au1Frame;
        u1IsNotLinear = OSIX_TRUE;
    }

    /* Assemble capwap Header */
    AssembleCapwapHeader (pu1Buf, &(pUnknownRsp->capwapHdr));
    /* Move the buffer pointer */
    pu1Buf += (pUnknownRsp->capwapHdr.u2HdrLen);

    /* Assemble Control header */
    AssembleCapwapControlHeader (pu1Buf,
                                 pUnknownRsp->u4MsgType + 1,
                                 pUnknownRsp->u1SeqNum,
                                 pUnknownRsp->u2CapwapMsgElemenLen,
                                 pUnknownRsp->u1NumMsgBlocks);
    pu1Buf += CAPWAP_CTRL_HEADER_SIZE;

    if (u1IsNotLinear == OSIX_TRUE)
    {
        CAPWAP_COPY_TO_BUF (pBuf, au1Frame, 0, CAPWAP_MAX_PKT_LEN);
    }

    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;

}

        /*****************************************************************************
         *                                                                           *
         * Function     : capwapAssembleAddstation                                   *
         *                                                                           *
         * Description  : Assemble the capwap AddStation message element             *
         *                                                                           *
         * Input        : Capwap Control message structure                           *
         *                                                                           *
         * Output       : Capwap control message to be transmitted out               *
         *                                                                           *
         * Returns      : NONE                                                       *
         *****************************************************************************/
VOID
capwapAssembleAddstation (UINT1 *pTxPkt, tWssStaAddStation * pAddSta)
{

    /* Copy the message type and length */
    CAPWAP_PUT_2BYTE (pTxPkt, pAddSta->u2MessageType);
    CAPWAP_PUT_2BYTE (pTxPkt, pAddSta->u2MessageLength);
    CAPWAP_PUT_1BYTE (pTxPkt, pAddSta->u1RadioId);
    CAPWAP_PUT_1BYTE (pTxPkt, pAddSta->u1MacAddrLen);
    CAPWAP_PUT_NBYTE (pTxPkt, pAddSta->StaMacAddress, 6);
    CAPWAP_PUT_2BYTE (pTxPkt, pAddSta->u2VlanId);
    /*
       u2Len = STRLEN (pAddSta->au1VlanName);
       CAPWAP_PUT_NBYTE(pTxPkt, pAddSta->au1VlanName, u2Len);
     */

    return;
}

VOID
CapwapAssembleRadarEventInfo (UINT1 *pTxPkt, tDFSRadarEventVendorInfo * pDelSta)
    /* Copy the message element to the tx buffer */
{
    UINT2               u2MsgType = VENDOR_SPECIFIC_PAYLOAD;
    UINT2               u2MsgLen =
        (UINT2) (pDelSta->u2MsgEleLen + CAPWAP_MSG_ELEM_TYPE_LEN);

    /* Copy the message type and length */
    CAPWAP_PUT_2BYTE (pTxPkt, u2MsgType);
    CAPWAP_PUT_2BYTE (pTxPkt, u2MsgLen);
    CAPWAP_PUT_2BYTE (pTxPkt, pDelSta->u2MsgEleType);
    CAPWAP_PUT_2BYTE (pTxPkt, pDelSta->u2MsgEleLen);
    CAPWAP_PUT_2BYTE (pTxPkt, pDelSta->u2ChannelNum);
    CAPWAP_PUT_1BYTE (pTxPkt, pDelSta->u1RadioId);
    CAPWAP_PUT_1BYTE (pTxPkt, ((UINT1) pDelSta->isRadarFound));
    return;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : capwapAssembleDeletestation                                *
         *                                                                           *
         * Description  : Assemble the capwap DeleteStation Message Element           *
         *                                                                           *
         * Input        : Capwap Control message structure                           *
         *                                                                           *
         * Output       : Capwap control message to be transmitted out               *
         *                                                                           *
         * Returns      : NONE                                                       *
         *****************************************************************************/
VOID
capwapAssembleDeletestation (UINT1 *pTxPkt, tWssStaDeleteStation * pDelSta)
{
    /* Copy the message type and length */
    CAPWAP_PUT_2BYTE (pTxPkt, pDelSta->u2MessageType);
    CAPWAP_PUT_2BYTE (pTxPkt, pDelSta->u2MessageLength);
    CAPWAP_PUT_1BYTE (pTxPkt, pDelSta->u1RadioId);
    CAPWAP_PUT_1BYTE (pTxPkt, pDelSta->u1MacAddrLen);
    CAPWAP_PUT_NBYTE (pTxPkt, pDelSta->StaMacAddr, 6);
    return;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : capwapAssembleDataTransferMode                             *
         *                                                                           *
         * Description  : Assemble the capwap DataTransferMode Message Element        *
         *                                                                           *
         * Input        : Capwap Control message structure                           *
         *                                                                           *
         * Output       : Capwap control message to be transmitted out               *
         *                                                                           *
         * Returns      : NONE                                                       *
         *****************************************************************************/
VOID
capwapAssembleDataTransferMode (UINT1 *pTxPkt,
                                tDataTransferMode * datatransfermode)
{
    /* Copy the message type and length */
    CAPWAP_PUT_2BYTE (pTxPkt, datatransfermode->u2MsgEleType);
    CAPWAP_PUT_2BYTE (pTxPkt, datatransfermode->u2MsgEleLen);
    CAPWAP_PUT_1BYTE (pTxPkt, datatransfermode->DataMode);
    return;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : capwapAssembleDataTransferData                             *
         *                                                                           *
         * Description  : Assemble the capwap DataTransferData Message Element       *
         *                                                                           *
         * Input        : Capwap Control message structure                           *
         *                                                                           *
         * Output       : Capwap control message to be transmitted out               *
         *                                                                           *
         * Returns      : SUCCESS/FAILURE                                            *
         *****************************************************************************/
VOID
capwapAssembleDataTransferData (UINT1 *pTxPkt,
                                tDataTransferData * datatransferdata)
{
    /* Copy the message type and length */
    UINT2               u2Len;

    CAPWAP_PUT_2BYTE (pTxPkt, datatransferdata->u2MsgEleType);
    CAPWAP_PUT_2BYTE (pTxPkt, datatransferdata->u2MsgEleLen);
    CAPWAP_PUT_1BYTE (pTxPkt, datatransferdata->DataType);
    CAPWAP_PUT_1BYTE (pTxPkt, datatransferdata->DataMode);
    CAPWAP_PUT_2BYTE (pTxPkt, datatransferdata->DataLength);
    u2Len = (UINT2) (STRLEN (datatransferdata->Data));
    CAPWAP_PUT_NBYTE (pTxPkt, datatransferdata->Data, u2Len);

    return;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : capwapAssembleRadioOperationalState                        *
         *                                                                           *
         * Description  : Assemble the capwap RadioOperationalState Message Element  *
         *                                                                           *
         * Input        : Capwap Control message structure                           *
         *                                                                           *
         * Output       : Capwap control message to be transmitted out               *
         *                                                                           *
         * Returns      : NONE                                                       *
         *****************************************************************************/
VOID
capwapAssembleRadioOperationalState (UINT1 *pTxPkt,
                                     tRadioIfOperStatus * radiostate)
{
    /* Copy the message type and length */
    CAPWAP_PUT_2BYTE (pTxPkt, radiostate->u2MessageType);
    CAPWAP_PUT_2BYTE (pTxPkt, radiostate->u2MessageLength);
    CAPWAP_PUT_1BYTE (pTxPkt, radiostate->u1RadioId);
    CAPWAP_PUT_1BYTE (pTxPkt, radiostate->u1OperStatus);
    CAPWAP_PUT_1BYTE (pTxPkt, radiostate->u1FailureCause);
    return;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : capwapAssembleDecryptionErrReport                          *
         *                                                                           *
         * Description  : Assemble the capwap DecryptionErReport Message Elemen      *
         *                                                                           *
         * Input        : Capwap Control message structure                           *
         *                                                                           *
         * Output       : Capwap control message to be transmitted out               *
         *                                                                           *
         * Returns      : NONE                                                       *
         *****************************************************************************/
VOID
capwapAssembleDecryptionErrReport (UINT1 *pTxPkt,
                                   tDecryptErrReport * decryptErr)
{
    /* Copy the message type and length */
    CAPWAP_PUT_2BYTE (pTxPkt, decryptErr->u2MsgEleType);
    CAPWAP_PUT_2BYTE (pTxPkt, decryptErr->u2MsgEleLen);
    CAPWAP_PUT_1BYTE (pTxPkt, decryptErr->u1RadioId);
    CAPWAP_PUT_2BYTE (pTxPkt, decryptErr->u2NumEntries);
    CAPWAP_PUT_2BYTE (pTxPkt, decryptErr->u2LenMacAddr);
    CAPWAP_PUT_2BYTE (pTxPkt, decryptErr->u2RadMacAddr);
    return;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : capwapAssembleWtpRadioStatistics                           *
         *                                                                           *
         * Description  : Assemble the capwap WtpRadioStatistics                     *
         *                                                                           *
         * Input        : Capwap Control message structure                           *
         *                                                                           *
         * Output       : Capwap control message to be transmitted out               *
         *                                                                           *
         * Returns      : NONE                                                       *
         *****************************************************************************/
VOID
capwapAssembleWtpRadioStatistics (UINT1 *pTxPkt,
                                  tWtpRadioStatsElement * wtpRadioStatsElement)
{
    /* Copy the message type and length */
    CAPWAP_PUT_2BYTE (pTxPkt, wtpRadioStatsElement->u2MsgEleType);
    CAPWAP_PUT_2BYTE (pTxPkt, wtpRadioStatsElement->u2MsgEleLen);
    /* Copy the reboot statistics */
    CAPWAP_PUT_1BYTE (pTxPkt, wtpRadioStatsElement->wtpRadioStats.u1RadioId);
    CAPWAP_PUT_1BYTE (pTxPkt,
                      wtpRadioStatsElement->wtpRadioStats.u1LastFailureType);
    CAPWAP_PUT_2BYTE (pTxPkt, wtpRadioStatsElement->wtpRadioStats.u2ResetCount);
    CAPWAP_PUT_2BYTE (pTxPkt,
                      wtpRadioStatsElement->wtpRadioStats.u2SwFailCount);
    CAPWAP_PUT_2BYTE (pTxPkt,
                      wtpRadioStatsElement->wtpRadioStats.u2HwFailCount);
    CAPWAP_PUT_2BYTE (pTxPkt,
                      wtpRadioStatsElement->wtpRadioStats.u2OtherFailureCount);
    CAPWAP_PUT_2BYTE (pTxPkt,
                      wtpRadioStatsElement->
                      wtpRadioStats.u2UnknownFailureCount);
    CAPWAP_PUT_2BYTE (pTxPkt,
                      wtpRadioStatsElement->wtpRadioStats.u2ConfigUpdateCount);
    CAPWAP_PUT_2BYTE (pTxPkt,
                      wtpRadioStatsElement->wtpRadioStats.u2ChannelChangeCount);
    CAPWAP_PUT_2BYTE (pTxPkt,
                      wtpRadioStatsElement->wtpRadioStats.u2BandChangeCount);
    CAPWAP_PUT_2BYTE (pTxPkt,
                      wtpRadioStatsElement->wtpRadioStats.u2CurrentNoiseFloor);
    return;

}

        /*****************************************************************************
         *                                                                           *
         * Function     : capwapAssembleDuplicateIpv4Address                         *
         *                                                                           *
         * Description  : Assemble the capwap DuplicateIpv4Address Message Element   *
         *                                                                           *
         * Input        : Capwap Control message structure                           *
         *                                                                           *
         * Output       : Capwap control message to be transmitted out               *
         *                                                                           *
         * Returns      : NONE                                                       *
         *****************************************************************************/
VOID
capwapAssembleDuplicateIpv4Address (UINT1 *pTxPkt, tDupIPV4Addr * ipv4addr)
{
    /* Copy the message type and length */
    CAPWAP_PUT_2BYTE (pTxPkt, ipv4addr->u2MsgEleType);
    CAPWAP_PUT_2BYTE (pTxPkt, ipv4addr->u2MsgEleLen);
    /* TBD- copying the IPv4 address */
    return;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : capwapAssembleDuplicateIpv6Address                         *
         *                                                                           *
         * Description  : Assemble the capwap DuplicateIpv6Address Message Element   *
         *                                                                           *
         * Input        : Capwap Control message structure                           *
         *                                                                           *
         * Output       : Capwap control message to be transmitted out               *
         *                                                                           *
         * Returns      : NONE                                                       *
         *****************************************************************************/
VOID
capwapAssembleDuplicateIpv6Address (UINT1 *pTxPkt, tDupIPV6Addr * ipv6addr)
{
    /* Copy the message type and length */
    CAPWAP_PUT_2BYTE (pTxPkt, ipv6addr->u2MsgEleType);
    CAPWAP_PUT_2BYTE (pTxPkt, ipv6addr->u2MsgEleLen);
    /* TBD- copying the IPv6 address */
    return;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : capwapAssembleDot11MicCountermeasures                      *
         *                                                                           *
         * Description  : Assemble the capwap Dot11 MicCounter measures msg element  *
         *                                                                           *
         * Input        : Capwap Control message structure                           *
         *                                                                           *
         * Output       : Capwap control message to be transmitted out               *
         *                                                                           *
         * Returns      : NONE                                                       *
         *****************************************************************************/
VOID
capwapAssembleDot11MicCountermeasures (UINT1 *pTxPkt,
                                       tDot11MicCountermeasures * Dot11MicCount)
{

    CAPWAP_PUT_2BYTE (pTxPkt, Dot11MicCount->u2MessageType);
    CAPWAP_PUT_2BYTE (pTxPkt, Dot11MicCount->u2MessageLength);
    CAPWAP_PUT_1BYTE (pTxPkt, Dot11MicCount->u1RadioId);
    CAPWAP_PUT_1BYTE (pTxPkt, Dot11MicCount->u1WlanId);
    CAPWAP_PUT_NBYTE (pTxPkt, Dot11MicCount->StaMacAddr, sizeof (tMacAddr));
    return;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : capwapAssembleDot11RSNAErrReport                           *
         *                                                                           *
         * Description  : Assemble the capwap Dot11 RSNA Error Report Msg Element    *
         *                                                                           *
         * Input        : Capwap Control message structure                           *
         *                                                                           *
         * Output       : Capwap control message to be transmitted out               *
         *                                                                           *
         * Returns      : NONE                                                       *
         *****************************************************************************/
VOID
capwapAssembleDot11RSNAErrReport (UINT1 *pTxPkt,
                                  tDot11RSNAErrReport * RSNAErrReport)
{

    CAPWAP_PUT_2BYTE (pTxPkt, RSNAErrReport->u2MessageType);
    CAPWAP_PUT_2BYTE (pTxPkt, RSNAErrReport->u2MessageLength);
    CAPWAP_PUT_NBYTE (pTxPkt, RSNAErrReport->ClientAddr, sizeof (tMacAddr));
    CAPWAP_PUT_2BYTE (pTxPkt, RSNAErrReport->u2Reserved);
    CAPWAP_PUT_NBYTE (pTxPkt, RSNAErrReport->BssId, sizeof (tMacAddr));
    CAPWAP_PUT_1BYTE (pTxPkt, RSNAErrReport->u1RadioId);
    CAPWAP_PUT_1BYTE (pTxPkt, RSNAErrReport->u1WlanId);
    CAPWAP_PUT_4BYTE (pTxPkt, RSNAErrReport->u4TkipICVErr);
    CAPWAP_PUT_4BYTE (pTxPkt, RSNAErrReport->u4TkipLocalMicFail);
    CAPWAP_PUT_4BYTE (pTxPkt, RSNAErrReport->u4TkipRemoteMicFail);
    CAPWAP_PUT_4BYTE (pTxPkt, RSNAErrReport->u4CCMPReplays);
    CAPWAP_PUT_4BYTE (pTxPkt, RSNAErrReport->u4CCMPDecryptErr);
    CAPWAP_PUT_4BYTE (pTxPkt, RSNAErrReport->u4TkipReplays);
}

#if 0
VOID
capwapAssembleDot11Statistics (UINT1 *pTxPkt,
                               tWtpdot11StatsElement * Dot11Statistics)
{

    CAPWAP_PUT_2BYTE (pTxPkt, Dot11Statistics->u2MessageType);
    CAPWAP_PUT_2BYTE (pTxPkt, Dot11Statistics->u2MessageLength);
    CAPWAP_PUT_4BYTE (pTxPkt, Dot11Statistics->u4RadioId);
    CAPWAP_PUT_4BYTE (pTxPkt, Dot11Statistics->u4TxFragmentCnt);
    CAPWAP_PUT_4BYTE (pTxPkt, Dot11Statistics->u4MulticastTxCnt);
    CAPWAP_PUT_4BYTE (pTxPkt, Dot11Statistics->u4FailedCount);
    CAPWAP_PUT_4BYTE (pTxPkt, Dot11Statistics->u4RetryCount);
    CAPWAP_PUT_4BYTE (pTxPkt, Dot11Statistics->u4MultipleRetryCount);
    CAPWAP_PUT_4BYTE (pTxPkt, Dot11Statistics->u4FrameDupCount);
    CAPWAP_PUT_4BYTE (pTxPkt, Dot11Statistics->u4RTSSuccessCount);
    CAPWAP_PUT_4BYTE (pTxPkt, Dot11Statistics->u4RTSFailCount);
    CAPWAP_PUT_4BYTE (pTxPkt, Dot11Statistics->u4ACKFailCount);
    CAPWAP_PUT_4BYTE (pTxPkt, Dot11Statistics->u4RxFragmentCount);
    CAPWAP_PUT_4BYTE (pTxPkt, Dot11Statistics->u4MulticastRxCount);
    CAPWAP_PUT_4BYTE (pTxPkt, Dot11Statistics->u4FCSErrCount);
    CAPWAP_PUT_4BYTE (pTxPkt, Dot11Statistics->u4TxFrameCount);
    CAPWAP_PUT_4BYTE (pTxPkt, Dot11Statistics->u4DecryptionErr);
    CAPWAP_PUT_4BYTE (pTxPkt, Dot11Statistics->u4DiscardQosFragmentCnt);
    CAPWAP_PUT_4BYTE (pTxPkt, Dot11Statistics->u4AssociatedStaCount);
    CAPWAP_PUT_4BYTE (pTxPkt, Dot11Statistics->u4QosCFPollsRecvdCount);
    CAPWAP_PUT_4BYTE (pTxPkt, Dot11Statistics->u4QosCFPollsUnusedCount);
    CAPWAP_PUT_4BYTE (pTxPkt, Dot11Statistics->u4QosCFPollsUnusableCount);
}
#endif

        /*****************************************************************************
         *                                                                           *
         * Function     : CapwapAssembleConfigStationReq                             *
         *                                                                           *
         * Description  : Assemble the capwap Control packet structure to            *
         *                transmittable  format.                                     *
         *                                                                           *
         * Input        : Capwap Control message structure                           *
         *                                                                           *
         * Output       : Capwap control message to be transmitted out               *
         *                                                                           *
         * Returns      : SUCCESS/FAILURE                                            *
         *****************************************************************************/

INT4
CapwapAssembleConfigStationReq (UINT2 mesgLen,
                                tStationConfReq * pconfStationReq,
                                UINT1 *pMesgOut)
{
    UINT1              *pu1Buf = NULL;

    pu1Buf = pMesgOut;
    CAPWAP_MEMSET (pu1Buf, CAPWAP_INIT_VAL, mesgLen);
    /* Assemble capwap Header */
    AssembleCapwapHeader (pu1Buf, &(pconfStationReq->capwapHdr));
    /* Move the buffer pointer */
    pu1Buf += (pconfStationReq->capwapHdr.u2HdrLen);

    /* Assemble Control header */
    AssembleCapwapControlHeader (pu1Buf,
                                 CAPWAP_STATION_CONF_REQ,
                                 pconfStationReq->u1SeqNum,
                                 pconfStationReq->u2CapwapMsgElemenLen,
                                 pconfStationReq->u1NumMsgBlocks);
    pu1Buf += CAPWAP_CTRL_HEADER_SIZE;

    /* Optional message elements */

    /* Assemble the Add Station */
    if (pconfStationReq->wssStaConfig.AddSta.isPresent)
    {
        capwapAssembleAddstation (pu1Buf,
                                  &(pconfStationReq->wssStaConfig.AddSta));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN +
             (pconfStationReq->wssStaConfig.AddSta.u2MessageLength));
    }

    /* Assemble the Delete Station */
    if (pconfStationReq->wssStaConfig.DelSta.isPresent)
    {
        capwapAssembleDeletestation (pu1Buf,
                                     &(pconfStationReq->wssStaConfig.DelSta));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN +
             (pconfStationReq->wssStaConfig.DelSta.u2MessageLength));
    }

    /* Assemble the Ieee Anteena */
    if (pconfStationReq->wssStaConfig.StaMsg.isPresent)
    {
        CapwapAssembleIeeeStation (pu1Buf,
                                   &(pconfStationReq->wssStaConfig.StaMsg));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN +
             (pconfStationReq->wssStaConfig.StaMsg.u2MessageLength));
    }

    /* Assemble the Station Dot11n params */
    if (pconfStationReq->Dot11nConfig.isPresent)
    {
        CapwapAssembleStaDot11nParams (pu1Buf,
                                       &(pconfStationReq->Dot11nConfig));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN +
             (pconfStationReq->Dot11nConfig.u2MessageLength));
    }

    /* Assemble the Ieee Station Session Key */
    if (pconfStationReq->wssStaConfig.StaSessKey.isPresent)
    {
        CapwapAssembleIeeeStationSessionKey (pu1Buf,
                                             &(pconfStationReq->wssStaConfig.
                                               StaSessKey));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN +
             (pconfStationReq->wssStaConfig.StaSessKey.u2MessageLength));
    }

    /* Assemble the Vendor Specific Payload */
    if (pconfStationReq->vendSpec.isOptional)
    {
        CapwapAssembleVendorSpecificPayload (pu1Buf,
                                             &(pconfStationReq->vendSpec));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN +
             (pconfStationReq->vendSpec.u2MsgEleLen));
    }

#if 0
    /* Assemble the Ieee Station Qos Profile */
    if (pconfStationReq->wssStaConfig.StaQosProfile.isPresent)
    {
        CapwapAssembleIeeeStationQosProfile (pu1Buf,
                                             &(pconfStationReq->wssStaConfig.
                                               StaQosProfile));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN +
             (pconfStationReq->wssStaConfig.StaQosProfile.u2MessageLength));
    }

    /* Assemble the Ieee Update Station Qos */
    if (pconfStationReq->wssStaConfig.StaQosUpdate.isPresent)
    {
        CapwapAssembleIeeeUpdateStatonQos (pu1Buf,
                                           &(pconfStationReq->
                                             wssStaConfig.StaQosUpdate));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN +
             (pconfStationReq->wssStaConfig.StaQosUpdate.u2MessageLength));
    }
#endif

    return OSIX_SUCCESS;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : CapwapAssembleConfigStationResp                            *
         *                                                                           *
         * Description  : Assemble the capwap Control packet structure to            *
         *                transmittable  format.                                     *
         *                                                                           *
         * Input        : Capwap Control message structure                           *
         *                                                                           *
         * Output       : Capwap control message to be transmitted out               *
         *                                                                           *
         * Returns      : SUCCESS/FAILURE                                            *
         *****************************************************************************/
INT4
CapwapAssembleConfigStationResp (UINT2 mesgLen,
                                 tStationConfRsp * pconfStationResp,
                                 UINT1 *pMesgOut)
{
    UINT1              *pu1Buf = NULL;

    pu1Buf = pMesgOut;
    CAPWAP_MEMSET (pu1Buf, CAPWAP_INIT_VAL, mesgLen);
    /* Assemble capwap Header */
    AssembleCapwapHeader (pu1Buf, &(pconfStationResp->capwapHdr));
    /* Move the buffer pointer */

    pu1Buf += (pconfStationResp->capwapHdr.u2HdrLen);

    /* Assemble Control header */
    AssembleCapwapControlHeader (pu1Buf,
                                 CAPWAP_STATION_CONF_RSP,
                                 pconfStationResp->u1SeqNum,
                                 pconfStationResp->u2CapwapMsgElemenLen,
                                 pconfStationResp->u1NumMsgBlocks);
    pu1Buf += CAPWAP_CTRL_HEADER_SIZE;
    /* Mandatory elements */

    /* Assemble the Result code */
    capwapAssembleResultCode (pu1Buf, &(pconfStationResp->resultCode));
    pu1Buf +=
        (CAPWAP_MSG_ELEM_TYPE_LEN + (pconfStationResp->resultCode.u2MsgEleLen));

    /* Optional message elements */

    /* Assemble the Vendor Specific Payload */
    if (pconfStationResp->vendSpec.isOptional)
    {
        CapwapAssembleVendorSpecificPayload (pu1Buf,
                                             &(pconfStationResp->vendSpec));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN +
             (pconfStationResp->vendSpec.u2MsgEleLen));
    }

    return OSIX_SUCCESS;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : CapwapAssembleClearConfigReq                               *
         *                                                                           *
         * Description  : Assemble the capwap Control packet structure to            *
         *                transmittable  format.                                     *
         *                                                                           *
         * Input        : Capwap Control message structure                           *
         *                                                                           *
         * Output       : Capwap control message to be transmitted out               *
         *                                                                           *
         * Returns      : SUCCESS/FAILURE                                            *
         *****************************************************************************/
INT4
CapwapAssembleClearConfigReq (UINT2 mesgLen,
                              tClearconfigReq * pclearconfigReq,
                              UINT1 *pMesgOut)
{
    UINT1              *pu1Buf = NULL;

    pu1Buf = pMesgOut;
    CAPWAP_MEMSET (pu1Buf, CAPWAP_INIT_VAL, mesgLen);
    /* Assemble capwap Header */
    AssembleCapwapHeader (pu1Buf, &(pclearconfigReq->capwapHdr));
    /* Move the buffer pointer */
    pu1Buf += (pclearconfigReq->capwapHdr.u2HdrLen);

    /* Assemble Control header */
    AssembleCapwapControlHeader (pu1Buf,
                                 CAPWAP_CLEAR_CONF_REQ,
                                 pclearconfigReq->u1SeqNum,
                                 pclearconfigReq->u2CapwapMsgElemenLen,
                                 pclearconfigReq->u1NumMsgBlocks);
    pu1Buf += CAPWAP_CTRL_HEADER_SIZE;
    /* Optional elements */
    /* Assemble the Vendor Specific Payload */
    if (pclearconfigReq->vendSpec.isOptional)
    {
        CapwapAssembleVendorSpecificPayload (pu1Buf,
                                             &(pclearconfigReq->vendSpec));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN +
             (pclearconfigReq->vendSpec.u2MsgEleLen));
    }

    return OSIX_SUCCESS;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : CapwapAssembleClearConfigResp                              *
         *                                                                           *
         * Description  : Assemble the capwap Control packet structure to            *
         *                transmittable  format.                                     *
         *                                                                           *
         * Input        : Capwap Control message structure                           *
         *                                                                           *
         * Output       : Capwap control message to be transmitted out               *
         *                                                                           *
         * Returns      : SUCCESS/FAILURE                                            *
         *****************************************************************************/
INT4
CapwapAssembleClearConfigResp (UINT2 mesgLen,
                               tClearconfigRsp * pclearconfigResp,
                               UINT1 *pMesgOut)
{
    UINT1              *pu1Buf = NULL;

    pu1Buf = pMesgOut;
    CAPWAP_MEMSET (pu1Buf, CAPWAP_INIT_VAL, mesgLen);
    /* Assemble capwap Header */
    AssembleCapwapHeader (pu1Buf, &(pclearconfigResp->capwapHdr));
    /* Move the buffer pointer */
    pu1Buf += (pclearconfigResp->capwapHdr.u2HdrLen);

    /* Assemble Control header */
    AssembleCapwapControlHeader (pu1Buf,
                                 CAPWAP_CLEAR_CONF_RSP,
                                 pclearconfigResp->u1SeqNum,
                                 pclearconfigResp->u2CapwapMsgElemenLen,
                                 pclearconfigResp->u1NumMsgBlocks);
    pu1Buf += CAPWAP_CTRL_HEADER_SIZE;

    /* Mandatory Messege elements */

    /* Assemble the Result code */
    capwapAssembleResultCode (pu1Buf, &(pclearconfigResp->resultCode));
    pu1Buf +=
        (CAPWAP_MSG_ELEM_TYPE_LEN + (pclearconfigResp->resultCode.u2MsgEleLen));

    /* Optional Messege elements */

    /* Assemble the Vendor Specific Payload */
    if (pclearconfigResp->vendSpec.isOptional)
    {
        CapwapAssembleVendorSpecificPayload (pu1Buf,
                                             &(pclearconfigResp->vendSpec));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN +
             (pclearconfigResp->vendSpec.u2MsgEleLen));
    }

    return OSIX_SUCCESS;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : CapwapAssembleResetRequest                                 *
         *                                                                           *
         * Description  : Assemble the capwap Control packet structure to            *
         *                transmittable  format.                                     *
         *                                                                           *
         * Input        : Capwap Control message structure                           *
         *                                                                           *
         * Output       : Capwap control message to be transmitted out               *
         *                                                                           *
         * Returns      : SUCCESS/FAILURE                                            *
         *****************************************************************************/
INT4
CapwapAssembleResetRequest (UINT2 mesgLen,
                            tResetReq * presetReq, UINT1 *pMesgOut)
{
    UINT1              *pu1Buf = NULL;

    pu1Buf = pMesgOut;
    CAPWAP_MEMSET (pu1Buf, CAPWAP_INIT_VAL, mesgLen);
    /* Assemble capwap Header */
    AssembleCapwapHeader (pu1Buf, &(presetReq->capwapHdr));
    /* Move the buffer pointer */
    pu1Buf += (presetReq->capwapHdr.u2HdrLen);

    /* Assemble Control header */
    AssembleCapwapControlHeader (pu1Buf,
                                 CAPWAP_RESET_REQ,
                                 presetReq->u1SeqNum,
                                 presetReq->u2CapwapMsgElemenLen,
                                 presetReq->u1NumMsgBlocks);
    pu1Buf += CAPWAP_CTRL_HEADER_SIZE;
    /* Mandatory elements */
    /* Assemble the Image Identifier */
    capwapAssembleImageIdentifier (pu1Buf, &(presetReq->imageId));
    pu1Buf += (CAPWAP_MSG_ELEM_TYPE_LEN + (presetReq->imageId.u2MsgEleLen));

    /* Optional message elements */
#if 0
    /* Assemble the Vendor Specific Payload */
    if (presetReq->vendSpec.isOptional)
    {
        CapwapAssembleVendorSpecificPayload (pu1Buf, &(presetReq->vendSpec));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN + (presetReq->vendSpec.u2MsgEleLen));
    }
#endif
    return OSIX_SUCCESS;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : CapwapAssembleResetResponse                                *
         *                                                                           *
         * Description  : Assemble the capwap Control packet structure to            *
         *                transmittable  format.                                     *
         *                                                                           *
         * Input        : Capwap Control message structure                           *
         *                                                                           *
         * Output       : Capwap control message to be transmitted out               *
         *                                                                           *
         * Returns      : SUCCESS/FAILURE                                            *
         *****************************************************************************/
INT4
CapwapAssembleResetResponse (UINT2 mesgLen,
                             tResetRsp * presetResp, UINT1 *pMesgOut)
{
    UINT1              *pu1Buf = NULL;

    pu1Buf = pMesgOut;
    CAPWAP_MEMSET (pu1Buf, CAPWAP_INIT_VAL, mesgLen);
    /* Assemble capwap Header */
    AssembleCapwapHeader (pu1Buf, &(presetResp->capwapHdr));
    /* Move the buffer pointer */
    pu1Buf += (presetResp->capwapHdr.u2HdrLen);

    /* Assemble Control header */
    AssembleCapwapControlHeader (pu1Buf,
                                 CAPWAP_RESET_RSP,
                                 presetResp->u1SeqNum,
                                 presetResp->u2CapwapMsgElemenLen,
                                 presetResp->u1NumMsgBlocks);
    pu1Buf += CAPWAP_CTRL_HEADER_SIZE;

    /* Optional message elements */

    /* Assemble the Result code */
    if (presetResp->resultCode.isOptional)
    {
        capwapAssembleResultCode (pu1Buf, &(presetResp->resultCode));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN + (presetResp->resultCode.u2MsgEleLen));
    }
    /* Assemble the Vendor Specific Payload */
    if (presetResp->vendSpec.isOptional)
    {
        CapwapAssembleVendorSpecificPayload (pu1Buf, &(presetResp->vendSpec));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN + (presetResp->vendSpec.u2MsgEleLen));
    }
    return OSIX_SUCCESS;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : CapwapAssembleDataTransferReq                              *
         *                                                                           *
         * Description  : Assemble the capwap Control packet structure to            *
         *                transmittable  format.                                     *
         *                                                                           *
         * Input        : Capwap Control message structure                           *
         *                                                                           *
         * Output       : Capwap control message to be transmitted out               *
         *                                                                           *
         * Returns      : SUCCESS/FAILURE                                            *
         *****************************************************************************/
INT4
CapwapAssembleDataTransferReq (UINT2 mesgLen,
                               tDataReq * pDataReq, UINT1 *pMesgOut)
{
    UINT1              *pu1Buf = NULL;

    pu1Buf = pMesgOut;
    CAPWAP_MEMSET (pu1Buf, CAPWAP_INIT_VAL, mesgLen);
    /* Assemble capwap Header */
    AssembleCapwapHeader (pu1Buf, &(pDataReq->capwapHdr));
    /* Move the buffer pointer */
    pu1Buf += (pDataReq->capwapHdr.u2HdrLen);

    /* Assemble Control header */
    AssembleCapwapControlHeader (pu1Buf,
                                 CAPWAP_DATA_TRANS_REQ,
                                 pDataReq->u1SeqNum,
                                 pDataReq->u2CapwapMsgElemenLen,
                                 pDataReq->u1NumMsgBlocks);
    pu1Buf += CAPWAP_CTRL_HEADER_SIZE;
    if (pDataReq->datatransfermode.isPresent == OSIX_TRUE
        && pDataReq->datatransferdata.isPresent == OSIX_TRUE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Both Mode and Data Should not be present in the Data Transfer Request\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Both Mode and Data Should not be present in the Data Transfer Request\r\n"));
        return OSIX_FAILURE;
    }

    if (pDataReq->datatransfermode.isPresent == OSIX_FALSE
        && pDataReq->datatransferdata.isPresent == OSIX_FALSE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Atleast one of data transfer mode or data transfer data is mandatory\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Atleast one of data transfer mode or data transfer data is mandatory\r\n"));
        return OSIX_FAILURE;
    }
    /* Mandatory elements */
    /* Assemble Data Transfer Mode */
    if (pDataReq->datatransfermode.isPresent)
    {
        capwapAssembleDataTransferMode (pu1Buf, &(pDataReq->datatransfermode));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN +
             (pDataReq->datatransfermode.u2MsgEleLen));
    }
    /* Assemble Data Transfer Data */

    /* Assemble Data Transfer Data */
    if (pDataReq->datatransferdata.isPresent)
    {
        capwapAssembleDataTransferData (pu1Buf, &(pDataReq->datatransferdata));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN +
             (pDataReq->datatransferdata.u2MsgEleLen));
    }
    /* Optional Messege elements */

    /* Assemble the Vendor Specific Payload */
    if (pDataReq->vendSpec.isOptional)
    {
        CapwapAssembleVendorSpecificPayload (pu1Buf, &(pDataReq->vendSpec));
        pu1Buf += (CAPWAP_MSG_ELEM_TYPE_LEN + (pDataReq->vendSpec.u2MsgEleLen));
    }

    return OSIX_SUCCESS;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : capwapAssembleDataTransferResponse                         *
         *                                                                           *
         * Description  : Assemble the capwap Control packet structure to            *
         *                transmittable  format.                                     *
         *                                                                           *
         * Input        : Capwap Control message structure                           *
         *                                                                           *
         * Output       : Capwap control message to be transmitted out               *
         *                                                                           *
         * Returns      : SUCCESS/FAILURE                                            *
         *****************************************************************************/
INT4
CapwapAssembleDataTransferResp (UINT2 mesgLen,
                                tDataRsp * pDataResp, UINT1 *pMesgOut)
{
    UINT1              *pu1Buf = NULL;

    pu1Buf = pMesgOut;
    CAPWAP_MEMSET (pu1Buf, CAPWAP_INIT_VAL, mesgLen);
    /* Assemble capwap Header */
    AssembleCapwapHeader (pu1Buf, &(pDataResp->capwapHdr));
    /* Move the buffer pointer */
    pu1Buf += (pDataResp->capwapHdr.u2HdrLen);

    /* Assemble Control header */
    AssembleCapwapControlHeader (pu1Buf,
                                 CAPWAP_DATA_TRANS_RSP,
                                 pDataResp->u1SeqNum,
                                 pDataResp->u2CapwapMsgElemenLen,
                                 pDataResp->u1NumMsgBlocks);
    pu1Buf += CAPWAP_CTRL_HEADER_SIZE;
    /* Mandatory elements */
    /* Assemble Result Code */
    capwapAssembleResultCode (pu1Buf, &(pDataResp->resultCode));
    pu1Buf += (CAPWAP_MSG_ELEM_TYPE_LEN + (pDataResp->resultCode.u2MsgEleLen));

    /* Optional Messege elements */

#if 0
    /* Assemble the Vendor Specific Payload */
    if (pDataResp->vendSpec.isOptional)
    {
        CapwapAssembleVendorSpecificPayload (pu1Buf, &(pDataResp->vendSpec));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN + (pDataResp->vendSpec.u2MsgEleLen));
    }
#endif
    return OSIX_SUCCESS;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : CapwapAssembleConfigUpdateReq                              *
         *                                                                           *
         * Description  : Assemble the capwap Control packet structure to            *
         *                transmittable  format.                                     *
         *                                                                           *
         * Input        : Capwap Control message structure                           *
         *                                                                           *
         * Output       : Capwap control message to be transmitted out               *
         *                                                                           *
         * Returns      : SUCCESS/FAILURE                                            *
         *****************************************************************************/
INT4
CapwapAssembleConfigUpdateReq (UINT2 mesgLen,
                               tConfigUpdateReq * pConfigUpdateReq,
                               UINT1 *pMesgOut)
{
    UINT1              *pu1Buf = NULL;

    UNUSED_PARAM (mesgLen);

    pu1Buf = pMesgOut;
    CAPWAP_MEMSET (pu1Buf, CAPWAP_INIT_VAL, CAPWAP_MAX_PKT_LEN);
    /* Assemble capwap Header */
    AssembleCapwapHeader (pu1Buf, &(pConfigUpdateReq->capwapHdr));
    /* Move the buffer pointer */
    pu1Buf += (pConfigUpdateReq->capwapHdr.u2HdrLen);

    /* Assemble Control header */
    AssembleCapwapControlHeader (pu1Buf,
                                 CAPWAP_CONF_UPD_REQ,
                                 pConfigUpdateReq->u1SeqNum,
                                 pConfigUpdateReq->u2CapwapMsgElemenLen,
                                 pConfigUpdateReq->u1NumMsgBlocks);
    pu1Buf += CAPWAP_CTRL_HEADER_SIZE;
    /* Optional message elements */
    /* Assemble the Capwap timer */
    if (pConfigUpdateReq->CapwapConfigUpdateReq.capwapTimer.isOptional)
    {
        capwapAssembleCapwapTimer (pu1Buf,
                                   &(pConfigUpdateReq->CapwapConfigUpdateReq.
                                     capwapTimer));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN +
             (pConfigUpdateReq->CapwapConfigUpdateReq.capwapTimer.u2MsgEleLen));
    }

    /* Assemble Idle timeout */
    if (pConfigUpdateReq->CapwapConfigUpdateReq.idleTimeout.isOptional)
    {
        capwapAssembleIdleTimeout (pu1Buf,
                                   &(pConfigUpdateReq->CapwapConfigUpdateReq.
                                     idleTimeout));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN +
             (pConfigUpdateReq->CapwapConfigUpdateReq.idleTimeout.u2MsgEleLen));
    }

    /* Assemble WTP Fallback */
    if (pConfigUpdateReq->CapwapConfigUpdateReq.wtpFallback.isOptional)
    {
        capwapAssembleWtpFallback (pu1Buf,
                                   &(pConfigUpdateReq->CapwapConfigUpdateReq.
                                     wtpFallback));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN +
             (pConfigUpdateReq->CapwapConfigUpdateReq.wtpFallback.u2MsgEleLen));
    }

    /* Assemble the location data */
    if (pConfigUpdateReq->CapwapConfigUpdateReq.wtpLocation.isOptional)
    {
        CapwapAssembleLocationData (pu1Buf,
                                    &(pConfigUpdateReq->CapwapConfigUpdateReq.
                                      wtpLocation));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN +
             (pConfigUpdateReq->CapwapConfigUpdateReq.wtpLocation.u2MsgEleLen));
    }

    /* Assemble the WTP Name */
    if (pConfigUpdateReq->CapwapConfigUpdateReq.wtpName.isOptional)
    {
        capwapAssembleWtpName (pu1Buf,
                               &(pConfigUpdateReq->
                                 CapwapConfigUpdateReq.wtpName));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN +
             (pConfigUpdateReq->CapwapConfigUpdateReq.wtpName.u2MsgEleLen));
    }

    /* Assemble Decryption error report Period */
    if (pConfigUpdateReq->CapwapConfigUpdateReq.decryErrPeriod.isOptional)
    {
        capwapAssembleDecryptionErrorReport (pu1Buf,
                                             &
                                             (pConfigUpdateReq->
                                              CapwapConfigUpdateReq.
                                              decryErrPeriod));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN +
             (pConfigUpdateReq->CapwapConfigUpdateReq.decryErrPeriod.
              u2MsgEleLen));
    }

    /* Assemble Radio Admin State */
    if (pConfigUpdateReq->RadioIfConfigUpdateReq.RadioIfAdminStatus.isPresent)
    {
        capwapAssembleRadioAdminState (pu1Buf,
                                       &
                                       (pConfigUpdateReq->
                                        RadioIfConfigUpdateReq.
                                        RadioIfAdminStatus));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN +
             (pConfigUpdateReq->RadioIfConfigUpdateReq.RadioIfAdminStatus.
              u2MessageLength));
    }
    /* Assemble the Ieee Information Element */
    if (pConfigUpdateReq->RadioIfConfigUpdateReq.RadioIfInfoElem.isPresent)
    {
        CapwapAssembleIeeeInfoElem (pu1Buf,
                                    &
                                    (pConfigUpdateReq->RadioIfConfigUpdateReq.
                                     RadioIfInfoElem));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN +
             (pConfigUpdateReq->RadioIfConfigUpdateReq.RadioIfInfoElem.
              u2MessageLength));
    }

    /* Assemble 802.11n parameters */
    if (pConfigUpdateReq->RadioIfConfigUpdateReq.RadioIfDot11nParam.isPresent)
    {
        capwapAssembleDot11nParams (pu1Buf,
                                    &
                                    (pConfigUpdateReq->RadioIfConfigUpdateReq.
                                     RadioIfDot11nParam));
        pu1Buf += (CAPWAP_MSG_ELEM_TYPE_LEN + DOT11N_VENDOR_MSG_LEN);
    }

    /* Assemble Statistics timer */
    if (pConfigUpdateReq->PmConfigUpdateReq.statsTimer.isOptional)
    {
        CapwapAssembleStatisticsTimer (pu1Buf,
                                       &(pConfigUpdateReq->PmConfigUpdateReq.
                                         statsTimer));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN +
             (pConfigUpdateReq->PmConfigUpdateReq.statsTimer.u2MsgEleLen));
    }

    /* Assemble the Image Identifier */
    if (pConfigUpdateReq->CapwapConfigUpdateReq.imageId.isOptional)
    {
        capwapAssembleImageIdentifier (pu1Buf,
                                       &
                                       (pConfigUpdateReq->CapwapConfigUpdateReq.
                                        imageId));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN +
             (pConfigUpdateReq->CapwapConfigUpdateReq.imageId.u2MsgEleLen));
    }

    /* Assemble the WTP Static IP Address */
    if (pConfigUpdateReq->CapwapConfigUpdateReq.wtpStaticIpAddr.isOptional)
    {
        capwapAssembleWtpStaticIpAddress (pu1Buf,
                                          &
                                          (pConfigUpdateReq->
                                           CapwapConfigUpdateReq.
                                           wtpStaticIpAddr));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN +
             (pConfigUpdateReq->CapwapConfigUpdateReq.wtpStaticIpAddr.
              u2MsgEleLen));
    }
    /* Assemble the Acname With Priority */
    if (pConfigUpdateReq->CapwapConfigUpdateReq.acNameWithPrio[0].isOptional)
    {
        capwapAssembleAcnameWithPriority (pu1Buf,
                                          &
                                          (pConfigUpdateReq->
                                           CapwapConfigUpdateReq.
                                           acNameWithPrio[0]));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN +
             (pConfigUpdateReq->CapwapConfigUpdateReq.acNameWithPrio[0].
              u2MsgEleLen));
    }

    if (pConfigUpdateReq->CapwapConfigUpdateReq.acNameWithPrio[1].isOptional)
    {
        capwapAssembleAcnameWithPriority (pu1Buf,
                                          &
                                          (pConfigUpdateReq->
                                           CapwapConfigUpdateReq.
                                           acNameWithPrio[1]));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN +
             (pConfigUpdateReq->CapwapConfigUpdateReq.acNameWithPrio[1].
              u2MsgEleLen));
    }

    if (pConfigUpdateReq->CapwapConfigUpdateReq.acNameWithPrio[2].isOptional)
    {
        capwapAssembleAcnameWithPriority (pu1Buf,
                                          &
                                          (pConfigUpdateReq->
                                           CapwapConfigUpdateReq.
                                           acNameWithPrio[2]));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN +
             (pConfigUpdateReq->CapwapConfigUpdateReq.acNameWithPrio[2].
              u2MsgEleLen));
    }

    /* Assemble the AddMac Entry */
    if (pConfigUpdateReq->CapwapConfigUpdateReq.addMacEntry.isOptional)
    {
        capwapAssembleAddMacEntry (pu1Buf,
                                   &(pConfigUpdateReq->CapwapConfigUpdateReq.
                                     addMacEntry));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN +
             (pConfigUpdateReq->CapwapConfigUpdateReq.addMacEntry.u2MsgEleLen));
    }

    /* Assemble the Delete Mac Entry */
    if (pConfigUpdateReq->CapwapConfigUpdateReq.delMacEntry.isOptional)
    {
        capwapAssembleDeleteMacEntry (pu1Buf,
                                      &(pConfigUpdateReq->CapwapConfigUpdateReq.
                                        delMacEntry));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN +
             (pConfigUpdateReq->CapwapConfigUpdateReq.delMacEntry.u2MsgEleLen));
    }

    /* Assemble the Ac Time stamp */
    if (pConfigUpdateReq->ClkiwfConfigUpdateReq.acTimestamp.isOptional)
    {
        capwapAssembleAcTimestamp (pu1Buf,
                                   &(pConfigUpdateReq->ClkiwfConfigUpdateReq.
                                     acTimestamp));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN +
             (pConfigUpdateReq->ClkiwfConfigUpdateReq.acTimestamp.u2MsgEleLen));
    }

    /* Assemble the Vendor Specific Payload */
    if (pConfigUpdateReq->CapwapConfigUpdateReq.vendSpec.isOptional)
    {
        CapwapAssembleVendorSpecificPayload (pu1Buf,
                                             &
                                             (pConfigUpdateReq->
                                              CapwapConfigUpdateReq.vendSpec));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN +
             (pConfigUpdateReq->CapwapConfigUpdateReq.vendSpec.u2MsgEleLen));
    }
    /* IEEE Message Elements */

    /* Assemble the Ieee Anteena */
    if (pConfigUpdateReq->RadioIfConfigUpdateReq.RadioIfAntenna.isPresent)
    {
        CapwapAssembleIeeeAntena (pu1Buf,
                                  &(pConfigUpdateReq->RadioIfConfigUpdateReq.
                                    RadioIfAntenna));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN +
             (pConfigUpdateReq->RadioIfConfigUpdateReq.
              RadioIfAntenna.u2MessageLength));
    }

    /* Assemble the Ieee Direct Sequece Control */
    if (pConfigUpdateReq->RadioIfConfigUpdateReq.RadioIfDSSSPhy.isPresent)
    {
        CapwapAssembleIeeeDirectSequeceControl (pu1Buf,
                                                &
                                                (pConfigUpdateReq->
                                                 RadioIfConfigUpdateReq.
                                                 RadioIfDSSSPhy));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN +
             (pConfigUpdateReq->RadioIfConfigUpdateReq.RadioIfDSSSPhy.
              u2MessageLength));
    }

    /* Assemble the Ieee Mac Operation */
    if (pConfigUpdateReq->RadioIfConfigUpdateReq.RadioIfMacOperation.isPresent)
    {
        CapwapAssembleIeeeMacOperation (pu1Buf,
                                        &
                                        (pConfigUpdateReq->
                                         RadioIfConfigUpdateReq.
                                         RadioIfMacOperation));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN +
             (pConfigUpdateReq->RadioIfConfigUpdateReq.RadioIfMacOperation.
              u2MessageLength));
    }

    /* Assemble the Ieee Multi Domain Capability */
    if (pConfigUpdateReq->RadioIfConfigUpdateReq.
        RadioIfMultiDomainCap.isPresent)
    {
        CapwapAssembleIeeeMultiDomainCapability (pu1Buf,
                                                 &
                                                 (pConfigUpdateReq->
                                                  RadioIfConfigUpdateReq.
                                                  RadioIfMultiDomainCap));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN +
             (pConfigUpdateReq->RadioIfConfigUpdateReq.RadioIfMultiDomainCap.
              u2MessageLength));
    }

    /* Assemble the Ieee OFDM Control */
    if (pConfigUpdateReq->RadioIfConfigUpdateReq.RadioIfOFDMPhy.isPresent)
    {
        CapwapAssembleIeeeOFDMControl (pu1Buf,
                                       &
                                       (pConfigUpdateReq->
                                        RadioIfConfigUpdateReq.RadioIfOFDMPhy));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN +
             (pConfigUpdateReq->RadioIfConfigUpdateReq.RadioIfOFDMPhy.
              u2MessageLength));
    }

    /* Assemble the Ieee Rate Set */
    if (pConfigUpdateReq->RadioIfConfigUpdateReq.RadioIfRateSet.isPresent)
    {
        CapwapAssembleIeeeRateSet (pu1Buf,
                                   &(pConfigUpdateReq->RadioIfConfigUpdateReq.
                                     RadioIfRateSet));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN +
             (pConfigUpdateReq->RadioIfConfigUpdateReq.
              RadioIfRateSet.u2MessageLength));
    }

    /* Assemble the Ieee Tx Power */
    if (pConfigUpdateReq->RadioIfConfigUpdateReq.RadioIfTxPower.isPresent)
    {
        CapwapAssembleIeeeTxPower (pu1Buf,
                                   &(pConfigUpdateReq->RadioIfConfigUpdateReq.
                                     RadioIfTxPower));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN +
             (pConfigUpdateReq->RadioIfConfigUpdateReq.
              RadioIfTxPower.u2MessageLength));
    }

    /* Assemble the Ieee Wtp Qos */
    if (pConfigUpdateReq->RadioIfConfigUpdateReq.RadioIfQos.isPresent)
    {
        CapwapAssembleIeeeWtpQos (pu1Buf,
                                  &(pConfigUpdateReq->RadioIfConfigUpdateReq.
                                    RadioIfQos));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN +
             (pConfigUpdateReq->RadioIfConfigUpdateReq.
              RadioIfQos.u2MessageLength));
    }

    /* Assemble the Ieee Wtp Radio Configuration */
    if (pConfigUpdateReq->RadioIfConfigUpdateReq.RadioIfConfig.isPresent)
    {
        CapwapAssembleIeeeWtpRadioConfiguration (pu1Buf,
                                                 &
                                                 (pConfigUpdateReq->
                                                  RadioIfConfigUpdateReq.
                                                  RadioIfConfig));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN +
             (pConfigUpdateReq->RadioIfConfigUpdateReq.RadioIfConfig.
              u2MessageLength));
    }

    /* Assemble the Ieee Wtp Radio Information */
    if (pConfigUpdateReq->RadioIfConfigUpdateReq.RadioIfInfo.isPresent)
    {
        capwapAssembleIeeeWtpRadioInformation (pu1Buf,
                                               &
                                               (pConfigUpdateReq->
                                                RadioIfConfigUpdateReq.
                                                RadioIfInfo));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN +
             (pConfigUpdateReq->RadioIfConfigUpdateReq.RadioIfInfo.
              u2MessageLength));
    }
#ifdef RFMGMT_WANTED
    /* Assemble Neighbor message parameters */
    if (pConfigUpdateReq->RfMgmtConfigUpdateReq.NeighApTableConfig.isOptional)
    {
        CapwapAssembleNeighborAPConfig (pu1Buf,
                                        &
                                        (pConfigUpdateReq->
                                         RfMgmtConfigUpdateReq.
                                         NeighApTableConfig));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN + RFMGMT_NEIGH_CONF_MSG_LEN +
             CAPWAP_MSG_ELEM_TYPE_LEN);
    }

    /* Assemble Client message parameters */
    if (pConfigUpdateReq->RfMgmtConfigUpdateReq.ClientTableConfig.isOptional)
    {
        CapwapAssembleClientSNRConfig (pu1Buf,
                                       &
                                       (pConfigUpdateReq->RfMgmtConfigUpdateReq.
                                        ClientTableConfig));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN + RFMGMT_CLIENT_CONF_MSG_LEN +
             CAPWAP_MSG_ELEM_TYPE_LEN);
    }

    /* Assemble Channel switch  message parameters */
    if (pConfigUpdateReq->RfMgmtConfigUpdateReq.ChSwitchStatusTable.isOptional)
    {
        CapwapAssembleChSwitchStatus (pu1Buf,
                                      &
                                      (pConfigUpdateReq->RfMgmtConfigUpdateReq.
                                       ChSwitchStatusTable));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN + RFMGMT_CH_SWITCH_STATUS_MSG_LEN +
             CAPWAP_MSG_ELEM_TYPE_LEN);
    }
    /* Assemble Vendor Spectrum Mgmt  message parameters */
    if (pConfigUpdateReq->RfMgmtConfigUpdateReq.TpcSpectMgmtTable.isOptional)
    {
        CapwapAssembleVendorTpcSpectMgmt (pu1Buf,
                                          &(pConfigUpdateReq->
                                            RfMgmtConfigUpdateReq.
                                            TpcSpectMgmtTable));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN + RFMGMT_SPECT_MGMT_MSG_LEN +
             CAPWAP_MSG_ELEM_TYPE_LEN);
    }
    /* Assemble Vendor Dfs Params  message parameters */
    if ((pConfigUpdateReq->RfMgmtConfigUpdateReq.DfsParamsTable.isOptional) !=
        OSIX_SUCCESS)
    {
        CapwapAssembleVendorDfsParams (pu1Buf,
                                       &
                                       (pConfigUpdateReq->RfMgmtConfigUpdateReq.
                                        DfsParamsTable));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN + RFMGMT_SPECT_MGMT_DFS_MSG_LEN +
             CAPWAP_MSG_ELEM_TYPE_LEN);
    }

#ifdef ROGUEAP_WANTED
    /* Assemble Vendor Rogue Mgmt message parameters */
    if (pConfigUpdateReq->RfMgmtConfigUpdateReq.RogueMgmt.isOptional)
    {
        CapwapAssembleVendorRogueMgmt (pu1Buf,
                                       &(pConfigUpdateReq->
                                         RfMgmtConfigUpdateReq.RogueMgmt));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN + RFMGMT_ROGUE_CONF_MSG_LEN +
             CAPWAP_MSG_ELEM_TYPE_LEN);
    }
#endif
#endif

    /* Assemble Vendor Dot11n Params  message parameters */
    if ((pConfigUpdateReq->RadioIfConfigUpdateReq.RadioIfDot11nCfg.isPresent) !=
        OSIX_SUCCESS)
    {
        CapwapAssembleVendorDot11nCfg (pu1Buf,
                                       &
                                       (pConfigUpdateReq->
                                        RadioIfConfigUpdateReq.
                                        RadioIfDot11nCfg));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN + RADIO_DOT11N_EXT_MSG_LEN +
             CAPWAP_MSG_ELEM_TYPE_LEN);
    }

    return OSIX_SUCCESS;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : CapwapAssembleConfigUpdateResp                             *
         *                                                                           *
         * Description  : Assemble the capwap Control packet structure to            *
         *                transmittable  format.                                     *
         *                                                                           *
         * Input        : Capwap Control message structure                           *
         *                                                                           *
         * Output       : Capwap control message to be transmitted out               *
         *                                                                           *
         * Returns      : SUCCESS/FAILURE                                            *
         *****************************************************************************/
INT4
CapwapAssembleConfigUpdateResp (UINT2 mesgLen,
                                tConfigUpdateRsp * pConfigUpdateResp,
                                UINT1 *pMesgOut)
{
    UINT1              *pu1Buf = NULL;

    pu1Buf = pMesgOut;
    CAPWAP_MEMSET (pu1Buf, CAPWAP_INIT_VAL, mesgLen);
    /* Assemble capwap Header */
    AssembleCapwapHeader (pu1Buf, &(pConfigUpdateResp->capwapHdr));
    /* Move the buffer pointer */
    pu1Buf += (pConfigUpdateResp->capwapHdr.u2HdrLen);

    /* Assemble Control header */
    AssembleCapwapControlHeader (pu1Buf,
                                 CAPWAP_CONF_UPD_RSP,
                                 pConfigUpdateResp->u1SeqNum,
                                 pConfigUpdateResp->u2CapwapMsgElemenLen,
                                 pConfigUpdateResp->u1NumMsgBlocks);
    pu1Buf += CAPWAP_CTRL_HEADER_SIZE;
    /* Mandatory elements */
    /* Assemble Result Code */
    capwapAssembleResultCode (pu1Buf, &(pConfigUpdateResp->resultCode));
    pu1Buf +=
        (CAPWAP_MSG_ELEM_TYPE_LEN +
         (pConfigUpdateResp->resultCode.u2MsgEleLen));

    /* Optional message elements */

    /* Assemble the Radio Operational State */
    if (pConfigUpdateResp->radOperState.isPresent)
    {
        capwapAssembleRadioOperationalState (pu1Buf,
                                             &
                                             (pConfigUpdateResp->radOperState));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN +
             (pConfigUpdateResp->radOperState.u2MessageLength));
    }

    /* Assemble the Vendor Specific Payload */
    if (pConfigUpdateResp->vendSpec.isOptional)
    {
        CapwapAssembleVendorSpecificPayload (pu1Buf,
                                             &(pConfigUpdateResp->vendSpec));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN +
             (pConfigUpdateResp->vendSpec.u2MsgEleLen));
    }

    return OSIX_SUCCESS;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : CapwapAssembleWtpEventRequest                              *
         *                                                                           *
         * Description  : Assemble the capwap Control packet structure to            *
         *                transmittable  format.                                     *
         *                                                                           *
         * Input        : Capwap Control message structure                           *
         *                                                                           *
         * Output       : Capwap control message to be transmitted out               *
         *                                                                           *
         * Returns      : SUCCESS/FAILURE                                            *
         *****************************************************************************/
INT4
CapwapAssembleWtpEventRequest (UINT2 mesgLen,
                               tWtpEveReq * pWtpEveReq, UINT1 *pMesgOut)
{
    UINT1              *pu1Buf = NULL;
    UNUSED_PARAM (mesgLen);
    pu1Buf = pMesgOut;
    UINT1              *pu1TempBuf = NULL;
    UINT2               u2MsgElemLength = 0;
    UINT2               u2TotalMsgElemLength = 0;

    CAPWAP_MEMSET (pu1Buf, CAPWAP_INIT_VAL, CAPWAP_MAX_PKT_LEN);
    pu1TempBuf = pu1Buf;
    /* Assemble capwap Header */
    AssembleCapwapHeader (pu1Buf, &(pWtpEveReq->capwapHdr));
    /* Move the buffer pointer */
    pu1Buf += (pWtpEveReq->capwapHdr.u2HdrLen);

    /* Assemble Control header */
    AssembleCapwapControlHeader (pu1Buf,
                                 CAPWAP_WTP_EVENT_REQ,
                                 pWtpEveReq->u1SeqNum,
                                 pWtpEveReq->u2CapwapMsgElemenLen,
                                 pWtpEveReq->u1NumMsgBlocks);
    pu1Buf += CAPWAP_CTRL_HEADER_SIZE;
    /* Mandatory elements */

    /* Assemble the Vendor Specific Payload */
    if (pWtpEveReq->vendSpec.isOptional)
    {
        CapwapAssembleVendorSpecificPayload (pu1Buf, &(pWtpEveReq->vendSpec));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN + (pWtpEveReq->vendSpec.u2MsgEleLen));
        u2TotalMsgElemLength = (UINT2) (u2TotalMsgElemLength +
                                        (CAPWAP_MSG_ELEM_TYPE_LEN +
                                         (pWtpEveReq->vendSpec.u2MsgEleLen)));

    }
    /* Assemble the Descryption Error Report */
    if (pWtpEveReq->MacHdlrWtpEventReq.decrypreport.isOptional)
    {
        capwapAssembleDecryptionErrReport (pu1Buf,
                                           &(pWtpEveReq->
                                             MacHdlrWtpEventReq.decrypreport));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN +
             (pWtpEveReq->MacHdlrWtpEventReq.decrypreport.u2MsgEleLen));
        u2TotalMsgElemLength =
            (UINT2) (u2TotalMsgElemLength +
                     (CAPWAP_MSG_ELEM_TYPE_LEN +
                      (pWtpEveReq->MacHdlrWtpEventReq.decrypreport.
                       u2MsgEleLen)));

    }

    /* Assemble the Delete Station Status */
    if (pWtpEveReq->StationWtpEventReq.deletestation.isPresent)
    {
        capwapAssembleDeletestation (pu1Buf,
                                     &(pWtpEveReq->
                                       StationWtpEventReq.deletestation));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN +
             (pWtpEveReq->StationWtpEventReq.deletestation.u2MessageLength));
        u2TotalMsgElemLength =
            (UINT2) (u2TotalMsgElemLength +
                     (CAPWAP_MSG_ELEM_TYPE_LEN +
                      (pWtpEveReq->StationWtpEventReq.deletestation.
                       u2MessageLength)));

    }

    /* Assemble the Duplicate IPV4 Address */
    if (pWtpEveReq->ArpWtpEventReq.ipv4addr.isOptional)
    {
        capwapAssembleDuplicateIpv4Address (pu1Buf,
                                            &(pWtpEveReq->
                                              ArpWtpEventReq.ipv4addr));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN +
             (pWtpEveReq->ArpWtpEventReq.ipv4addr.u2MsgEleLen));
        u2TotalMsgElemLength =
            (UINT2) (u2TotalMsgElemLength +
                     (CAPWAP_MSG_ELEM_TYPE_LEN +
                      (pWtpEveReq->ArpWtpEventReq.ipv4addr.u2MsgEleLen)));

    }

    /* Assemble the Duplicate IPV6 Address */
    if (pWtpEveReq->ArpWtpEventReq.ipv6addr.isOptional)
    {
        capwapAssembleDuplicateIpv6Address (pu1Buf,
                                            &(pWtpEveReq->
                                              ArpWtpEventReq.ipv6addr));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN +
             (pWtpEveReq->ArpWtpEventReq.ipv6addr.u2MsgEleLen));
        u2TotalMsgElemLength =
            (UINT2) (u2TotalMsgElemLength +
                     (CAPWAP_MSG_ELEM_TYPE_LEN +
                      (pWtpEveReq->ArpWtpEventReq.ipv6addr.u2MsgEleLen)));

    }

    /* Assemble the Wtp Radio Statistics */
    if (pWtpEveReq->PmWtpEventReq.wtpRadiostatsElement.isOptional)
    {
        CapwapAssembleGenRadioParam (WTP_RADIO_STATISTICS, 0, &pu1Buf,
                                     &u2MsgElemLength, 0);
        pWtpEveReq->PmWtpEventReq.wtpRadiostatsElement.u2MsgEleLen =
            u2MsgElemLength;
        u2TotalMsgElemLength = (UINT2) (u2TotalMsgElemLength + u2MsgElemLength);

    }

    /* Assemble the Wtp Reboot Statistics */
    if (pWtpEveReq->PmWtpEventReq.wtpRebootstatsElement.isOptional)
    {
        CapwapAssembleGenRadioParam (WTP_REBOOT_STATISTICS, 0, &pu1Buf,
                                     &u2MsgElemLength, 0);
        pWtpEveReq->PmWtpEventReq.wtpRebootstatsElement.u2MsgEleLen =
            u2MsgElemLength;
        u2TotalMsgElemLength = (UINT2) (u2TotalMsgElemLength +
                                        pWtpEveReq->PmWtpEventReq.
                                        wtpRebootstatsElement.u2MsgEleLen);
    }

    /* Assemble the Wtp 802.11 Statistics */
    if (pWtpEveReq->PmWtpEventReq.dot11StatsElement.isOptional)
    {
        CapwapAssembleGenRadioParam (IEEE_STATISTICS, 0, &pu1Buf,
                                     &u2MsgElemLength, 0);
        pWtpEveReq->PmWtpEventReq.dot11StatsElement.u2MsgEleLen =
            u2MsgElemLength;

        u2TotalMsgElemLength = (UINT2) (u2TotalMsgElemLength + u2MsgElemLength);
    }

    /* Assemble the Vendor Specific Payload */
    if ((pWtpEveReq->PmWtpEventReq.capwapStatsElement.isOptional) ||
        (pWtpEveReq->PmWtpEventReq.MacHdlrStatsElement.isOptional) ||
        (pWtpEveReq->PmWtpEventReq.RadioClientStatsElement.isOptional) ||
        (pWtpEveReq->PmWtpEventReq.ApParamsElement.isOptional))
    {

        CapwapAssembleGenRadioParam (VENDOR_SPECIFIC_PAYLOAD, 0, &pu1Buf,
                                     &u2MsgElemLength, 0);

        pWtpEveReq->PmWtpEventReq.vendSpec.u2MsgEleLen = u2MsgElemLength;

        u2TotalMsgElemLength = (UINT2) (u2TotalMsgElemLength +
                                        pWtpEveReq->PmWtpEventReq.vendSpec.
                                        u2MsgEleLen);

    }

#ifdef RFMGMT_WANTED
    if ((pWtpEveReq->PmWtpEventReq.vendSpec.u1RFVendorMsgEleType ==
         VENDOR_NEIGHBOR_AP_TYPE)
        || (pWtpEveReq->PmWtpEventReq.vendSpec.u1RFVendorMsgEleType ==
            VENDOR_NEIGHBOR_RADIO2_AP_TYPE))
    {

        if ((CapwapAssembleGenRadioParam (VENDOR_SPECIFIC_PAYLOAD,
                                          pWtpEveReq->PmWtpEventReq.
                                          vendSpec.u1RFVendorMsgEleType,
                                          &pu1Buf, &u2MsgElemLength,
                                          0)) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed  to Assemble WTP Event Request for Neighbour AP Scan\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed  to Assemble WTP Event Request for Neighbour AP Scan\n"));

            return OSIX_FAILURE;
        }
        pWtpEveReq->vendSpec.unVendorSpec.VendNeighAP.u2MsgEleLen =
            (UINT2) (pWtpEveReq->vendSpec.unVendorSpec.VendNeighAP.u2MsgEleLen +
                     u2MsgElemLength);
        u2TotalMsgElemLength =
            (UINT2) (u2TotalMsgElemLength +
                     pWtpEveReq->vendSpec.unVendorSpec.VendNeighAP.u2MsgEleLen);

    }

    if (pWtpEveReq->PmWtpEventReq.vendSpec.u1RFVendorMsgEleType ==
        VENDOR_CLIENT_SCAN_TYPE)
    {

        if ((CapwapAssembleGenRadioParam (VENDOR_SPECIFIC_PAYLOAD,
                                          VENDOR_CLIENT_SCAN_TYPE,
                                          &pu1Buf, &u2MsgElemLength,
                                          0)) != OSIX_SUCCESS)
        {

            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed  to Assemble WTP Event Request for Vendor Client Scan\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed  to Assemble WTP Event Request for Vendor Client Scan\n"));
            return OSIX_FAILURE;
        }
        pWtpEveReq->vendSpec.unVendorSpec.VendClientScan.u2MsgEleLen =
            (UINT2) (pWtpEveReq->vendSpec.unVendorSpec.VendClientScan.
                     u2MsgEleLen + u2MsgElemLength);

        u2TotalMsgElemLength = (UINT2) (u2TotalMsgElemLength +
                                        pWtpEveReq->vendSpec.unVendorSpec.
                                        VendClientScan.u2MsgEleLen);

    }
#endif

    if (pWtpEveReq->PmWtpEventReq.vendSpec.unVendorSpec.VendMultiDomain.
        isOptional == OSIX_TRUE)
    {
        if ((CapwapAssembleGenRadioParam
             (VENDOR_SPECIFIC_PAYLOAD, VENDOR_MULTIDOMAIN_INFO_TYPE, &pu1Buf,
              &u2MsgElemLength, 0)) != OSIX_SUCCESS)
        {
        }
        pWtpEveReq->vendSpec.unVendorSpec.VendMultiDomain.u2MsgEleLen =
            u2MsgElemLength;
        u2TotalMsgElemLength = (UINT2) (u2TotalMsgElemLength + u2MsgElemLength);
    }
    if (pWtpEveReq->PmWtpEventReq.vendSpec.unVendorSpec.DFSRadarEventInfo.
        isOptional)
    {
        CapwapAssembleRadarEventInfo (pu1Buf,
                                      &(pWtpEveReq->PmWtpEventReq.vendSpec.
                                        unVendorSpec.DFSRadarEventInfo));

        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN + 4 +
             (pWtpEveReq->PmWtpEventReq.vendSpec.unVendorSpec.DFSRadarEventInfo.
              u2MsgEleLen));
        u2TotalMsgElemLength =
            (UINT2) (u2TotalMsgElemLength +
                     (CAPWAP_MSG_ELEM_TYPE_LEN + 4 +
                      (pWtpEveReq->PmWtpEventReq.vendSpec.unVendorSpec.
                       DFSRadarEventInfo.u2MsgEleLen)));
    }

    u2TotalMsgElemLength =
        (UINT2) (u2TotalMsgElemLength + CAPWAP_CMN_MSG_ELM_HEAD_LEN);
    pu1TempBuf += CAPWAP_MSG_OFFSET;
    pWtpEveReq->u2CapwapMsgElemenLen = u2TotalMsgElemLength;
    CAPWAP_PUT_2BYTE (pu1TempBuf, u2TotalMsgElemLength);

    return OSIX_SUCCESS;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : CapwapAssembleWtpEventResponse                             *
         *                                                                           *
         * Description  : Assemble the capwap Control packet structure to            *
         *                transmittable  format.                                     *
         *                                                                           *
         * Input        : Capwap Control message structure                           *
         *                                                                           *
         * Output       : Capwap control message to be transmitted out               *
         *                                                                           *
         * Returns      : SUCCESS/FAILURE                                            *
         *****************************************************************************/
INT4
CapwapAssembleWtpEventResponse (UINT2 mesgLen,
                                tWtpEveRsp * pWtpEveRsp, UINT1 *pMesgOut)
{
    UINT1              *pu1Buf = NULL;

    pu1Buf = pMesgOut;
    CAPWAP_MEMSET (pu1Buf, CAPWAP_INIT_VAL, mesgLen);
    /* Assemble capwap Header */
    AssembleCapwapHeader (pu1Buf, &(pWtpEveRsp->capwapHdr));
    /* Move the buffer pointer */
    pu1Buf += (pWtpEveRsp->capwapHdr.u2HdrLen);

    /* Assemble Control header */
    AssembleCapwapControlHeader (pu1Buf,
                                 CAPWAP_WTP_EVENT_RSP,
                                 pWtpEveRsp->u1SeqNum,
                                 pWtpEveRsp->u2CapwapMsgElemenLen,
                                 pWtpEveRsp->u1NumMsgBlocks);
    pu1Buf += CAPWAP_CTRL_HEADER_SIZE;
    /* Optional Messege elements */
    /* Assemble the Vendor Specific Payload */
    if (pWtpEveRsp->vendSpec.isOptional)
    {
        CapwapAssembleVendorSpecificPayload (pu1Buf, &(pWtpEveRsp->vendSpec));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN + (pWtpEveRsp->vendSpec.u2MsgEleLen));
    }

    return OSIX_SUCCESS;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : CapwapAssembleChangeStateEventRequest                      *
         *                                                                           *
         * Description  : Assemble the capwap Control packet structure to            *
         *                transmittable  format.                                     *
         *                                                                           *
         * Input        : Capwap Control message structure                           *
         *                                                                           *
         * Output       : Capwap control message to be transmitted out               *
         *                                                                           *
         * Returns      : SUCCESS/FAILURE                                            *
         *****************************************************************************/

INT4
CapwapAssembleChangeStateEventRequest (UINT2 mesgLen,
                                       tChangeStateEvtReq * pStateEventReq,
                                       UINT1 *pMesgOut)
{
    UINT1              *pu1Buf = NULL;
    UINT1              *pu1TempBuf = NULL;
    UINT2               u2MsgElemLength = 0;
    UINT2               u2TempLength = 0;

    CAPWAP_FN_ENTRY ();
    pu1Buf = pMesgOut;
    CAPWAP_MEMSET (pu1Buf, CAPWAP_INIT_VAL, mesgLen);
    pu1TempBuf = pu1Buf;
    /* Assemble capwap Header */
    AssembleCapwapHeader (pu1Buf, &(pStateEventReq->capwapHdr));
    /* Move the buffer pointer */
    pu1Buf += (pStateEventReq->capwapHdr.u2HdrLen);

    /* Assemble Control header */
    AssembleCapwapControlHeader (pu1Buf,
                                 CAPWAP_CHANGE_STAT_REQ,
                                 pStateEventReq->u1SeqNum,
                                 pStateEventReq->u2CapwapMsgElemenLen,
                                 pStateEventReq->u1NumMsgBlocks);
    pu1Buf += CAPWAP_CTRL_HEADER_SIZE;
    /* Mandatory elements */
    /* Assemble the Radio Operational State */

    CapwapAssembleGenRadioParam (RADIO_OPER_STATE, 0, &pu1Buf, &u2MsgElemLength,
                                 0);
    pu1TempBuf += CAPWAP_MSG_OFFSET;
    CAPWAP_GET_2BYTE (u2TempLength, pu1TempBuf);
    u2TempLength = (UINT2) (u2TempLength + u2MsgElemLength);
    pu1TempBuf -= sizeof (UINT2);
    CAPWAP_PUT_2BYTE (pu1TempBuf, u2TempLength);

    /* Assemble Result Code */
    capwapAssembleResultCode (pu1Buf, &(pStateEventReq->resultCode));
    pu1Buf +=
        (CAPWAP_MSG_ELEM_TYPE_LEN + (pStateEventReq->resultCode.u2MsgEleLen));

    /* Optional message elements */
    /* TBD */
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : CapwapAssembleChangeStateEventResponse                     *
         *                                                                           *
         * Description  : Assemble the capwap Control packet structure to            *
         *                transmittable  format.                                     *
         *                                                                           *
         * Input        : Capwap Control message structure                           *
         *                                                                           *
         * Output       : Capwap control message to be transmitted out               *
         *                                                                           *
         * Returns      : SUCCESS/FAILURE                                            *
         ****************************************************************************/
INT4
CapwapAssembleChangeStateEventResponse (UINT2 mesgLen,
                                        tChangeStateEvtRsp * stateEventResp,
                                        UINT1 *pMesgOut)
{
    UINT1              *pu1Buf = NULL;

    CAPWAP_FN_ENTRY ();
    pu1Buf = pMesgOut;
    CAPWAP_MEMSET (pu1Buf, CAPWAP_INIT_VAL, mesgLen);
    /* Assemble capwap Header */
    AssembleCapwapHeader (pu1Buf, &(stateEventResp->capwapHdr));
    /* Move the buffer pointer */
    pu1Buf += (stateEventResp->capwapHdr.u2HdrLen);

    /* Assemble Control header */
    AssembleCapwapControlHeader (pu1Buf,
                                 CAPWAP_CHANGE_STAT_RSP,
                                 stateEventResp->u1SeqNum,
                                 stateEventResp->u2CapwapMsgElemenLen,
                                 stateEventResp->u1NumMsgBlocks);
    pu1Buf += CAPWAP_CTRL_HEADER_SIZE;
    /* No Mandatory elements */

    /* Only Optional message elements - Vendor Specific payload */
    /* TBD */
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;

}

INT4
CapwapAssembleKeepAlivePkt (tKeepAlivePkt * pKeepAlivePkt,
                            tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UINT1              *pTxMsg = NULL;
    UINT1               u1IsNotLinear = OSIX_FALSE;
    UINT1              *pTemp = NULL;

    CAPWAP_FN_ENTRY ();
    /* Kloc Fix Start */
    if (pBuf == NULL)
    {
        return OSIX_FAILURE;
    }
    /* Kloc Fix Ends */
    pTxMsg = CAPWAP_BUF_IF_LINEAR (pBuf, 0, CAPWAP_MAX_PKT_LEN);
    if (pTxMsg == NULL)
    {
        pTxMsg = (UINT1 *) MemAllocMemBlk (CAPWAP_PKTBUF_POOLID);
        /* Kloc Fix Start */
        if (pTxMsg == NULL)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Memory Allocation Failed \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Memory Allocation Failed \r\n"));
            return OSIX_FAILURE;
        }
        /* Kloc Fix Ends */
        pTemp = pTxMsg;
        u1IsNotLinear = OSIX_TRUE;
    }
    /* Assemble capwap Header */
    AssembleCapwapHeader (pTxMsg, &(pKeepAlivePkt->capwapHdr));
    /* Move the buffer pointer */
    pTxMsg += (pKeepAlivePkt->capwapHdr.u2HdrLen);
    CAPWAP_PUT_2BYTE (pTxMsg, pKeepAlivePkt->u2CapwapMsgElemenLen);
    capwapAssembleSessionId (pTxMsg, pKeepAlivePkt->sessionID);
    pTxMsg += (CAPWAP_MSG_ELEM_TYPE_LEN + 16);

    if (u1IsNotLinear == OSIX_TRUE)
    {
        CAPWAP_COPY_TO_BUF (pBuf, pTemp, 0, CAPWAP_MAX_PKT_LEN);
        MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pTemp);
    }

    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : CapwapAssembleImageDataReq                                 *
         *                                                                           *
         * Description  : Assemble the capwap Control packet structure to            *
         *                transmittable  format.                                     *
         *                                                                           *
         * Input        : Capwap Control message structure                           *
         *                                                                           *
         * Output       : Capwap control message to be transmitted out               *
         *                                                                           *
         * Returns      : SUCCESS/FAILURE                                            *
         *****************************************************************************/
INT4
CapwapAssembleImageDataReq (UINT2 mesgLen,
                            tImageDataReq * pImageDataReq, UINT1 *pMesgOut)
{
    UINT1              *pu1Buf = NULL;

    pu1Buf = pMesgOut;
    CAPWAP_MEMSET (pu1Buf, CAPWAP_INIT_VAL, mesgLen);
    /* Assemble capwap Header */
    AssembleCapwapHeader (pu1Buf, &(pImageDataReq->capwapHdr));
    /* Move the buffer pointer */
    pu1Buf += (pImageDataReq->capwapHdr.u2HdrLen);

    /* Assemble Control header */
    AssembleCapwapControlHeader (pu1Buf,
                                 CAPWAP_IMAGE_DATA_REQ,
                                 pImageDataReq->u1SeqNum,
                                 pImageDataReq->u2CapwapMsgElemenLen,
                                 pImageDataReq->u1NumMsgBlocks);
    pu1Buf += CAPWAP_CTRL_HEADER_SIZE;
    /* Optional message elements */
    /* Assemble the Capwap transfer protocol */
    if (pImageDataReq->TransportProtocol.isOptional)
    {
        capwapAssembleTrasnportProtocol (pu1Buf,
                                         &(pImageDataReq->TransportProtocol));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN +
             (pImageDataReq->TransportProtocol.u2MsgEleLen));
    }

    /* Assemble the Image Data */
    if (pImageDataReq->ImageData.isOptional)
    {
        capwapAssembleImageData (pu1Buf, &(pImageDataReq->ImageData));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN + (pImageDataReq->ImageData.u2MsgEleLen));
    }

    /* Assemble the Vendor Specific Payload */
    if (pImageDataReq->vendSpec.isOptional)
    {
        CapwapAssembleVendorSpecificPayload (pu1Buf,
                                             &(pImageDataReq->vendSpec));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN + (pImageDataReq->vendSpec.u2MsgEleLen));
    }

    /* Assemble the Image Identifier */
    if (pImageDataReq->ImageId.isOptional)
    {
        capwapAssembleImageIdentifier (pu1Buf, &(pImageDataReq->ImageId));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN + (pImageDataReq->ImageId.u2MsgEleLen));
    }

    /* Assemble the Initiate download */
    if (pImageDataReq->InitiatelDownload.isOptional)
    {
        capwapAssembleInitiateDownload (pu1Buf,
                                        &(pImageDataReq->InitiatelDownload));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN +
             (pImageDataReq->InitiatelDownload.u2MsgEleLen));
    }

    return OSIX_SUCCESS;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : CapwapAssembleImageDataRsp 
         *                                                                           *
         * Description  : Assemble the capwap Control packet structure to            *
         *                transmittable  format.                                     *
         *                                                                           *
         * Input        : Capwap Control message structure                           *
         *                                                                           *
         * Output       : Capwap control message to be transmitted out               *
         *                                                                           *
         * Returns      : SUCCESS/FAILURE                                            *
         *****************************************************************************/
INT4
CapwapAssembleImageDataRsp (UINT2 mesgLen,
                            tImageDataRsp * pImageDataRsp, UINT1 *pMesgOut)
{
    UINT1              *pu1Buf = NULL;

    pu1Buf = pMesgOut;
    CAPWAP_MEMSET (pu1Buf, CAPWAP_INIT_VAL, mesgLen);
    /* Assemble capwap Header */
    AssembleCapwapHeader (pu1Buf, &(pImageDataRsp->capwapHdr));
    /* Move the buffer pointer */
    pu1Buf += (pImageDataRsp->capwapHdr.u2HdrLen);

    /* Assemble Control header */
    AssembleCapwapControlHeader (pu1Buf,
                                 CAPWAP_IMAGE_DATA_RSP,
                                 pImageDataRsp->u1SeqNum,
                                 pImageDataRsp->u2CapwapMsgElemenLen,
                                 pImageDataRsp->u1NumMsgBlocks);
    pu1Buf += CAPWAP_CTRL_HEADER_SIZE;
    /* Optional message elements */
    /* Assemble the Result Code */
    capwapAssembleResultCode (pu1Buf, &(pImageDataRsp->ResultCode));
    pu1Buf +=
        (CAPWAP_MSG_ELEM_TYPE_LEN + (pImageDataRsp->ResultCode.u2MsgEleLen));

    /* Assemble the Vendor Specific Payload */
    if (pImageDataRsp->vendSpec.isOptional)
    {
        CapwapAssembleVendorSpecificPayload (pu1Buf,
                                             &(pImageDataRsp->vendSpec));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN + (pImageDataRsp->vendSpec.u2MsgEleLen));
    }

    /* Assemble the Image Information */
    if (pImageDataRsp->ImageInfo.isOptional)
    {
        CapwapAssembleImageInformation (pu1Buf, &(pImageDataRsp->ImageInfo));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN + (pImageDataRsp->ImageInfo.u2MsgEleLen));
    }

    return OSIX_SUCCESS;
}

        /*****************************************************************************
         *                                                                           *
         * Function    : CapwapAssembleWTPImageDataReq                               *
         *                                                                           *
         * Description : Assemble the capwap Control packet structure to             *
         *                transmittable  format.                                     *
         *                                                                           *
         * Input       : Capwap Control message structure                            *
         *                                                                           *
         * Output      : Capwap control message to be transmitted out                *
         *                                                                           *
         * Returns     : SUCCESS/FAILURE                                             *
         *****************************************************************************/
INT4
CapwapAssembleWTPImageDataReq (UINT2 mesgLen,
                               tImageDataReq * pImageDataReq, UINT1 *pMesgOut)
{
    UINT1              *pu1Buf = NULL;

    CAPWAP_FN_ENTRY ();
    pu1Buf = pMesgOut;
    CAPWAP_MEMSET (pu1Buf, CAPWAP_INIT_VAL, mesgLen);
    /* Assemble capwap Header */
    AssembleCapwapHeader (pu1Buf, &(pImageDataReq->capwapHdr));
    /* Move the buffer pointer */
    pu1Buf += (pImageDataReq->capwapHdr.u2HdrLen);

    /* Assemble Control header */
    AssembleCapwapControlHeader (pu1Buf,
                                 CAPWAP_IMAGE_DATA_REQ,
                                 pImageDataReq->u1SeqNum,
                                 pImageDataReq->u2CapwapMsgElemenLen,
                                 pImageDataReq->u1NumMsgBlocks);
    pu1Buf += CAPWAP_CTRL_HEADER_SIZE;

    /* Optional message elements */
    /* Assemble the Capwap transfer protocol */
    if (pImageDataReq->TransportProtocol.isOptional)
    {
        capwapAssembleTrasnportProtocol (pu1Buf,
                                         &(pImageDataReq->TransportProtocol));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN +
             (pImageDataReq->TransportProtocol.u2MsgEleLen));
    }

    /* Assemble the Image Identifier */
    if (pImageDataReq->ImageId.isOptional)
    {
        capwapAssembleImageIdentifier (pu1Buf, &(pImageDataReq->ImageId));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN + (pImageDataReq->ImageId.u2MsgEleLen));
    }
    /* Assemble the Initiate download */
    if (pImageDataReq->InitiatelDownload.isOptional)
    {
        capwapAssembleInitiateDownload (pu1Buf,
                                        &(pImageDataReq->InitiatelDownload));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN +
             (pImageDataReq->InitiatelDownload.u2MsgEleLen));
    }

    return OSIX_SUCCESS;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : CapwapAssembleWLCImageDataRsp 
         *                                                                           *
         * Description  : Assemble the capwap Control packet structure to            *
         *                transmittable  format.                                     *
         *                                                                           *
         * Input        : Capwap Control message structure                           *
         *                                                                           *
         * Output       : Capwap control message to be transmitted out               *
         *                                                                           *
         * Returns      : SUCCESS/FAILURE                                            *
         *****************************************************************************/
INT4
CapwapAssembleWLCImageDataRsp (UINT2 mesgLen,
                               tImageDataRsp * pImageDataRsp, UINT1 *pu1Buf)
{

    CAPWAP_MEMSET (pu1Buf, CAPWAP_INIT_VAL, mesgLen);
    /* Assemble capwap Header */
    AssembleCapwapHeader (pu1Buf, &(pImageDataRsp->capwapHdr));
    /* Move the buffer pointer */
    pu1Buf += (pImageDataRsp->capwapHdr.u2HdrLen);

    /* Assemble Control header */
    AssembleCapwapControlHeader (pu1Buf,
                                 CAPWAP_IMAGE_DATA_RSP,
                                 pImageDataRsp->u1SeqNum,
                                 pImageDataRsp->u2CapwapMsgElemenLen,
                                 pImageDataRsp->u1NumMsgBlocks);

    pu1Buf += CAPWAP_CTRL_HEADER_SIZE;

    /* Optional message elements */
    /* Assemble the Result Code */
    if (pImageDataRsp->ResultCode.isOptional)
    {
        capwapAssembleResultCode (pu1Buf, &(pImageDataRsp->ResultCode));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN +
             (pImageDataRsp->ResultCode.u2MsgEleLen));
    }
    /* Assemble the Vendor Specific Payload */
    if (pImageDataRsp->vendSpec.isOptional)
    {
        CapwapAssembleVendorSpecificPayload (pu1Buf,
                                             &(pImageDataRsp->vendSpec));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN + (pImageDataRsp->vendSpec.u2MsgEleLen));
    }

    /* Assemble the Image Information */
    if (pImageDataRsp->ImageInfo.isOptional)
    {
        CapwapAssembleImageInformation (pu1Buf, &(pImageDataRsp->ImageInfo));
        pu1Buf +=
            (CAPWAP_MSG_ELEM_TYPE_LEN + (pImageDataRsp->ImageInfo.u2MsgEleLen));
    }

    return OSIX_SUCCESS;
}

#ifdef RFMGMT_WANTED
        /*****************************************************************************/
        /* Function     : CapwapAssembleVendSpecNeighAP                              */
        /*                                                                           */
        /* Description  : This function will assemble the Neighbor AP information in */
        /*                WTP Event request vendor message                           */
        /*                                                                           */
        /* Input        : pvendPld - Neighbor message related info                   */
        /*                                                                           */
        /* Output       : pTxPkt - Packet to be transmitted                          */
        /*                                                                           */
        /* Returns      : RFMGMT_SUCCESS, is processing succeeds                     */
        /*                RFMGMT_FAILURE, otherwise                                  */
        /*****************************************************************************/
VOID
CapwapAssembleVendSpecNeighAP (UINT1 *pTxPkt, tVendorSpecPayload * pvendPld)
{
    UINT2               u2ScannedChannel = 0;
    UINT1               u1NeighborCount = 0;
    UINT4               u4MaxChannel = 0;
    tRfMgmtScanChannel *pTempScanChannel;

    /* Copy the message element to the tx buffer */
    if (pvendPld->unVendorSpec.VendNeighAP.isOptional == OSIX_TRUE)
    {
        CAPWAP_PUT_2BYTE (pTxPkt,
                          pvendPld->unVendorSpec.VendNeighAP.u2MsgEleType);
        CAPWAP_PUT_2BYTE (pTxPkt,
                          pvendPld->unVendorSpec.VendNeighAP.u2MsgEleLen);
        CAPWAP_PUT_4BYTE (pTxPkt,
                          pvendPld->unVendorSpec.VendNeighAP.u4VendorId);
        CAPWAP_PUT_1BYTE (pTxPkt, pvendPld->unVendorSpec.VendNeighAP.u1RadioId);
        CAPWAP_PUT_4BYTE (pTxPkt,
                          pvendPld->unVendorSpec.VendNeighAP.u4Dot11RadioType);

        if ((pvendPld->unVendorSpec.VendNeighAP.u4Dot11RadioType ==
             RFMGMT_RADIO_TYPEA)
            || (pvendPld->unVendorSpec.VendNeighAP.u4Dot11RadioType ==
                RFMGMT_RADIO_TYPEAN)
            || (pvendPld->unVendorSpec.VendNeighAP.u4Dot11RadioType ==
                RFMGMT_RADIO_TYPEAC))
        {
            u4MaxChannel = RFMGMT_MAX_CHANNELA;
        }
        else
        {
            u4MaxChannel = RFMGMT_MAX_CHANNELB;
        }
        for (u2ScannedChannel = 0;
             u2ScannedChannel < u4MaxChannel; u2ScannedChannel++)
        {
            pTempScanChannel = &pvendPld->unVendorSpec.VendNeighAP.
                ScanChannel[u2ScannedChannel];
            for (u1NeighborCount = 0;
                 u1NeighborCount < pTempScanChannel->u1NeighborApCount;
                 u1NeighborCount++)
            {

                CAPWAP_PUT_2BYTE (pTxPkt, pTempScanChannel->u2ScannedChannel);
                CAPWAP_PUT_1BYTE (pTxPkt, pTempScanChannel->u1NeighborApCount);
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  pTempScanChannel->
                                  u1EntryStatus[u1NeighborCount]);
                CAPWAP_PUT_NBYTE (pTxPkt,
                                  pTempScanChannel->
                                  NeighborMacAddr[u1NeighborCount],
                                  MAC_ADDR_LEN);
                CAPWAP_PUT_2BYTE (pTxPkt,
                                  pTempScanChannel->i2Rssi[u1NeighborCount]);
                CAPWAP_PUT_1BYTE (pTxPkt, pTempScanChannel->u1NonGFPresent);
                CAPWAP_PUT_1BYTE (pTxPkt, pTempScanChannel->u1OBSSNonGFPresent);
#ifdef ROGUEAP_WANTED
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  pTempScanChannel->
                                  isRogueApProcetionType[u1NeighborCount]);
                CAPWAP_PUT_1BYTE (pTxPkt,
                                  pTempScanChannel->
                                  isRogueApStatus[u1NeighborCount]);
                CAPWAP_PUT_4BYTE (pTxPkt,
                                  pTempScanChannel->
                                  u4RogueApLastReportedTime[u1NeighborCount]);
                CAPWAP_PUT_NBYTE (pTxPkt,
                                  pTempScanChannel->au1ssid[u1NeighborCount],
                                  WLAN_MAX_SSID_LEN);
                CAPWAP_PUT_NBYTE (pTxPkt,
                                  pTempScanChannel->
                                  BSSIDRogueApLearntFrom[u1NeighborCount],
                                  MAC_ADDR_LEN);

                CAPWAP_PUT_1BYTE (pTxPkt,
                                  pTempScanChannel->
                                  u1StationCount[u1NeighborCount]);
                CAPWAP_PUT_NBYTE (pTxPkt,
                                  pTempScanChannel->
                                  au1RfGroupID[u1NeighborCount],
                                  RF_GROUP_ID_MAX_LEN);
#endif
            }
        }
    }

    return;
}

        /*****************************************************************************/
        /* Function     : CapwapAssembleVendSpecClientScan                           */
        /*                                                                           */
        /* Description  : This function will assemble the Client SCAN information in */
        /*                WTP event request vendor message                           */
        /*                                                                           */
        /* Input        : pvendPld - Client Scan message related info                */
        /*                                                                           */
        /* Output       : pTxPkt - Packet to be transmitted                          */
        /*                                                                           */
        /* Returns      : RFMGMT_SUCCESS, is processing succeeds                     */
        /*                RFMGMT_FAILURE, otherwise                                  */
        /*****************************************************************************/
VOID
CapwapAssembleVendSpecClientScan (UINT1 *pTxPkt, tVendorSpecPayload * pvendPld)
{
    UINT1               u1ClientCount = 0;

    /* Copy the message element to the tx buffer */
    if (pvendPld->unVendorSpec.VendClientScan.isOptional == OSIX_TRUE)
    {
        CAPWAP_PUT_2BYTE (pTxPkt,
                          pvendPld->unVendorSpec.VendClientScan.u2MsgEleType);
        CAPWAP_PUT_2BYTE (pTxPkt,
                          pvendPld->unVendorSpec.VendClientScan.u2MsgEleLen);
        CAPWAP_PUT_4BYTE (pTxPkt,
                          pvendPld->unVendorSpec.VendClientScan.u4VendorId);
        CAPWAP_PUT_1BYTE (pTxPkt,
                          pvendPld->unVendorSpec.VendClientScan.u1RadioId);
        CAPWAP_PUT_1BYTE (pTxPkt,
                          pvendPld->unVendorSpec.VendClientScan.u1ClientCount);

        for (u1ClientCount = 0;
             u1ClientCount <
             pvendPld->unVendorSpec.VendClientScan.u1ClientCount;
             u1ClientCount++)
        {
            CAPWAP_PUT_1BYTE (pTxPkt,
                              pvendPld->unVendorSpec.
                              VendClientScan.u1EntryStatus[u1ClientCount]);
            CAPWAP_PUT_NBYTE (pTxPkt,
                              pvendPld->unVendorSpec.
                              VendClientScan.ClientMacAddress[u1ClientCount],
                              MAC_ADDR_LEN);
            CAPWAP_PUT_2BYTE (pTxPkt,
                              pvendPld->unVendorSpec.
                              VendClientScan.i2ClientSNR[u1ClientCount]);
        }
    }

    return;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : CapwapAssembleNeighborAPConfig                             *
         *                                                                           *
         * Description  : Assemble the configuration params to scan Neighbor AP      *
         *                                                                           *
         * Input        : tVendorNeighConfig  : Neighbor AP Config                   *
         *                                                                           *
         * Output       : Capwap message element to be transmitted out               *
         *                                                                           *
         * Returns      : NONE                                                       *
         *****************************************************************************/
VOID
CapwapAssembleNeighborAPConfig (UINT1 *pTxPkt, tNeighApTableConfig * pvendPld)
{
    UINT2               u2MsgType = VENDOR_SPECIFIC_PAYLOAD;
    UINT2               u2MsgLen =
        (UINT2) (pvendPld->u2MsgEleLen + CAPWAP_MSG_ELEM_TYPE_LEN);

    /* Copy the message type and length */
    CAPWAP_PUT_2BYTE (pTxPkt, u2MsgType);
    CAPWAP_PUT_2BYTE (pTxPkt, u2MsgLen);

    /* Copy the message element to the tx buffer */
    if (pvendPld->isOptional == OSIX_TRUE)
    {
        CAPWAP_PUT_2BYTE (pTxPkt, pvendPld->u2MsgEleType);
        CAPWAP_PUT_2BYTE (pTxPkt, pvendPld->u2MsgEleLen);
        CAPWAP_PUT_4BYTE (pTxPkt, pvendPld->u4VendorId);
        CAPWAP_PUT_1BYTE (pTxPkt, pvendPld->u1RadioId);
        CAPWAP_PUT_1BYTE (pTxPkt, pvendPld->u1AutoScanStatus);
        CAPWAP_PUT_2BYTE (pTxPkt, pvendPld->u2NeighborMsgPeriod);
        CAPWAP_PUT_2BYTE (pTxPkt, pvendPld->u2ChannelScanDuration);
        CAPWAP_PUT_2BYTE (pTxPkt, pvendPld->u2NeighborAgingPeriod);
        CAPWAP_PUT_2BYTE (pTxPkt, pvendPld->i2RssiThreshold);
    }
    return;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : CapwapAssembleClientSNRConfig                              *
         *                                                                           *
         * Description  : Assemble the configuration params to scan Client info      *
         *                                                                           *
         * Input        : tVendorClientConfig : Client scan config                   *
         *                                                                           *
         * Output       : Capwap message element to be transmitted out               *
         *                                                                           *
         * Returns      : NONE                                                       *
         *****************************************************************************/
VOID
CapwapAssembleClientSNRConfig (UINT1 *pTxPkt, tClientTableConfig * pvendPld)
{
    UINT2               u2MsgType = VENDOR_SPECIFIC_PAYLOAD;
    UINT2               u2MsgLen =
        (UINT2) (pvendPld->u2MsgEleLen + CAPWAP_MSG_ELEM_TYPE_LEN);

    /* Copy the message type and length */
    CAPWAP_PUT_2BYTE (pTxPkt, u2MsgType);
    CAPWAP_PUT_2BYTE (pTxPkt, u2MsgLen);

    /* Copy the message element to the tx buffer */
    if (pvendPld->isOptional == OSIX_TRUE)
    {
        CAPWAP_PUT_2BYTE (pTxPkt, pvendPld->u2MsgEleType);
        CAPWAP_PUT_2BYTE (pTxPkt, pvendPld->u2MsgEleLen);
        CAPWAP_PUT_4BYTE (pTxPkt, pvendPld->u4VendorId);
        CAPWAP_PUT_1BYTE (pTxPkt, pvendPld->u1RadioId);
        CAPWAP_PUT_1BYTE (pTxPkt, pvendPld->u1SNRScanStatus);
        CAPWAP_PUT_2BYTE (pTxPkt, pvendPld->u2SNRScanPeriod);
        CAPWAP_PUT_2BYTE (pTxPkt, pvendPld->i2SNRThreshold);
        CAPWAP_PUT_1BYTE (pTxPkt, pvendPld->u1WlanId);
        CAPWAP_PUT_1BYTE (pTxPkt,
                          pvendPld->u1BssidScanStatus[pvendPld->u1WlanId - 1]);
    }
    return;
}

        /*****************************************************************************
         *                                                                           *
         * Function     : CapwapAssembleChSwitchStatus                               *
         *                                                                           *
         * Description  : Assemble the configuration params to channel switch  info  *
         *                                                                           *
         * Input        : tVendorChSwitchStatus : Client scan config                 *
         *                                                                           *
         * Output       : Capwap message element to be transmitted out               *
         *                                                                           *
         * Returns      : NONE                                                       *
         *****************************************************************************/
VOID
CapwapAssembleChSwitchStatus (UINT1 *pTxPkt, tChSwitchStatusTable * pvendPld)
{
    UINT2               u2MsgType = VENDOR_SPECIFIC_PAYLOAD;
    UINT2               u2MsgLen =
        (UINT2) (pvendPld->u2MsgEleLen + CAPWAP_MSG_ELEM_TYPE_LEN);

    /* Copy the message type and length */
    CAPWAP_PUT_2BYTE (pTxPkt, u2MsgType);
    CAPWAP_PUT_2BYTE (pTxPkt, u2MsgLen);

    /* Copy the message element to the tx buffer */
    if (pvendPld->isOptional == OSIX_TRUE)
    {
        CAPWAP_PUT_2BYTE (pTxPkt, pvendPld->u2MsgEleType);
        CAPWAP_PUT_2BYTE (pTxPkt, pvendPld->u2MsgEleLen);
        CAPWAP_PUT_4BYTE (pTxPkt, pvendPld->u4VendorId);
        CAPWAP_PUT_1BYTE (pTxPkt, pvendPld->u1RadioId);
        CAPWAP_PUT_1BYTE (pTxPkt, pvendPld->u1ChSwitchStatus);
    }
    return;
}

/*****************************************************************************
 *                                                                           *
 * Function     : CapwapAssembleVendorTpcSpectMgmt                           *
 *                                                                           *
 * Description  : Assemble the configuration params to TPC request interval  *
 *                                                                           *
 * Input        : tTpcSpectMgmtTable                                         *
 *                                                                           *
 * Output       : Capwap message element to be transmitted out               *
 *                                                                           *
 * Returns      : NONE                                                       *
 *****************************************************************************/
VOID
CapwapAssembleVendorTpcSpectMgmt (UINT1 *pTxPkt, tTpcSpectMgmtTable * pvendPld)
{
    UINT2               u2MsgType = VENDOR_SPECIFIC_PAYLOAD;
    UINT2               u2MsgLen =
        (UINT2) (pvendPld->u2MsgEleLen + CAPWAP_MSG_ELEM_TYPE_LEN);

    /* Copy the message type and length */
    CAPWAP_PUT_2BYTE (pTxPkt, u2MsgType);
    CAPWAP_PUT_2BYTE (pTxPkt, u2MsgLen);

    /* Copy the message element to the tx buffer */
    if (pvendPld->isOptional == OSIX_TRUE)
    {
        CAPWAP_PUT_2BYTE (pTxPkt, pvendPld->u2MsgEleType);
        CAPWAP_PUT_2BYTE (pTxPkt, pvendPld->u2MsgEleLen);
        CAPWAP_PUT_4BYTE (pTxPkt, pvendPld->u4VendorId);
        CAPWAP_PUT_1BYTE (pTxPkt, pvendPld->u1RadioId);
        CAPWAP_PUT_2BYTE (pTxPkt, pvendPld->u2TpcRequestInterval);
        CAPWAP_PUT_1BYTE (pTxPkt, pvendPld->u111hTpcStatus);
    }
    return;
}

/****************************************************************************
*                                                                           *
* Function     : CapwapAssembleVendorDfsParams                              *
*                                                                           *
* Description  : Assemble the configuration params to DFS  info             *
*                                                                           *
* Input        : tVendorDfsParams : DFS Params config                       *
*                                                                           *
* Output       : Capwap message element to be transmitted out               *
*                                                                           *
* Returns      : NONE                                                       *
*****************************************************************************/
VOID
CapwapAssembleVendorDfsParams (UINT1 *pTxPkt, tDfsParamsTable * pvendPld)
{
    UINT2               u2MsgType = VENDOR_SPECIFIC_PAYLOAD;
    UINT2               u2MsgLen =
        (UINT2) (pvendPld->u2MsgEleLen + CAPWAP_MSG_ELEM_TYPE_LEN);

    /* Copy the message type and length */
    CAPWAP_PUT_2BYTE (pTxPkt, u2MsgType);
    CAPWAP_PUT_2BYTE (pTxPkt, u2MsgLen);

    /* Copy the message element to the tx buffer */
    if (pvendPld->isOptional == OSIX_TRUE)
    {
        CAPWAP_PUT_2BYTE (pTxPkt, pvendPld->u2MsgEleType);
        CAPWAP_PUT_2BYTE (pTxPkt, pvendPld->u2MsgEleLen);
        CAPWAP_PUT_4BYTE (pTxPkt, pvendPld->u4VendorId);
        CAPWAP_PUT_1BYTE (pTxPkt, pvendPld->u1RadioId);
        CAPWAP_PUT_2BYTE (pTxPkt, pvendPld->u2DfsQuietInterval);
        CAPWAP_PUT_2BYTE (pTxPkt, pvendPld->u2DfsQuietPeriod);
        CAPWAP_PUT_2BYTE (pTxPkt, pvendPld->u2DfsMeasurementInterval);
        CAPWAP_PUT_2BYTE (pTxPkt, pvendPld->u2DfsChannelSwitchStatus);
        CAPWAP_PUT_1BYTE (pTxPkt, pvendPld->u111hDfsStatus);
    }
    return;
}

#ifdef ROGUEAP_WANTED

/*****************************************************************************
 *                                                                           *
 * Function     : CapwapAssembleVendorRogueMgmt                              *
 *                                                                           *
 * Description  : Assemble the configuration params to TPC request interval  *
 *                                                                           *
 * Input        : tTpcSpectMgmtTable                                         *
 *                                                                           *
 * Output       : Capwap message element to be transmitted out               *
 *                                                                           *
 * Returns      : NONE                                                       *
 *****************************************************************************/
VOID
CapwapAssembleVendorRogueMgmt (UINT1 *pTxPkt, tRogueMgmt * pvendPld)
{
    UINT2               u2MsgType = VENDOR_SPECIFIC_PAYLOAD;
    UINT2               u2MsgLen =
        (UINT2) (pvendPld->u2MsgEleLen + CAPWAP_MSG_ELEM_TYPE_LEN);

    /* Copy the message type and length */
    CAPWAP_PUT_2BYTE (pTxPkt, u2MsgType);
    CAPWAP_PUT_2BYTE (pTxPkt, u2MsgLen);

    /* Copy the message element to the tx buffer */
    if (pvendPld->isOptional == OSIX_TRUE)
    {
        CAPWAP_PUT_2BYTE (pTxPkt, pvendPld->u2MsgEleType);
        CAPWAP_PUT_2BYTE (pTxPkt, pvendPld->u2MsgEleLen);
        CAPWAP_PUT_4BYTE (pTxPkt, pvendPld->u4VendorId);
        CAPWAP_PUT_NBYTE (pTxPkt, pvendPld->u1RfGroupName,
                          CAPWAP_RF_GROUP_NAME_SIZE);
        CAPWAP_PUT_1BYTE (pTxPkt, pvendPld->u1RfDetection);
    }
    return;
}
#endif

#endif

/****************************************************************************
*                                                                           *
* Function     : CapwapAssembleVendorDot11nCfg                              *
*                                                                           *
* Description  : Assemble the configuration params for 11n extension table  *
*                                                                           *
* Input        : RadioIfDot11nCfg : 11n Configuration extension             *
*                                                                           *
* Output       : Capwap message element to be transmitted out               *
*                                                                           *
* Returns      : NONE                                                       *
*****************************************************************************/
VOID
CapwapAssembleVendorDot11nCfg (UINT1 *pTxPkt, tRadioIfDot11nCfg * pvendPld)
{
    UINT2               u2MsgType = VENDOR_SPECIFIC_PAYLOAD;
    UINT2               u2MsgLen =
        (UINT2) (pvendPld->u2MessageLength + CAPWAP_MSG_ELEM_TYPE_LEN);

    /* Copy the message type and length */
    CAPWAP_PUT_2BYTE (pTxPkt, u2MsgType);
    CAPWAP_PUT_2BYTE (pTxPkt, u2MsgLen);

    /* Copy the message element to the tx buffer */
    if (pvendPld->isPresent == OSIX_TRUE)
    {
        CAPWAP_PUT_2BYTE (pTxPkt, pvendPld->u2MessageType);
        CAPWAP_PUT_2BYTE (pTxPkt, pvendPld->u2MessageLength);
        CAPWAP_PUT_4BYTE (pTxPkt, pvendPld->u4VendorId);
        CAPWAP_PUT_1BYTE (pTxPkt, pvendPld->u1RadioId);
        CAPWAP_PUT_1BYTE (pTxPkt, pvendPld->u1AMPDUStatus);
        CAPWAP_PUT_1BYTE (pTxPkt, pvendPld->u1AMPDUSubFrame);
        CAPWAP_PUT_2BYTE (pTxPkt, pvendPld->u2AMPDULimit);
        CAPWAP_PUT_1BYTE (pTxPkt, pvendPld->u1AMSDUStatus);
        CAPWAP_PUT_2BYTE (pTxPkt, pvendPld->u2AMSDULimit);
    }
    return;
}

/*****************************************************************************/
/* Function     : CapwapAssembleVendSpecMultiDomainInfo                      */
/*                                                                           */
/* Description  : This function will assemble the Multi domain information in*/
/*                WTP Event request vendor message                           */
/*                                                                           */
/* Input        : pRadioIfDB - Radio related info                            */
/*                                                                           */
/* Output       : pTxPkt - Packet to be transmitted                          */
/*                                                                           */
/* Returns      : OSIX_SUCCESS, is processing succeeds                       */
/*                OSIX_FAILURE, otherwise                                    */
/*****************************************************************************/
VOID
CapwapAssembleVendSpecMultiDomainInfo (UINT1 *pTxPkt, UINT2 u2NumRadioElems,
                                       UINT2 *pu2MsgElemLen,
                                       tRadioIfGetDB * pRadioIfGetDB)
{
    UINT1               u1Radioid = 0;
    UINT1               u1MultiDomainIndex = 0;
    UINT2               u2ElemLength = 0;
    UINT2               u2MsgElemLen = 0;
    UINT2               u2Length = 0;
    UINT2               u2Len = 0;
    UINT1              *pu1MsgTmpElemValue = NULL;
    UINT1              *pu1MsgElemValue = NULL;
    UINT1              *pu1MsgTmpValue = NULL;
    UINT1              *pu1MsgValue = NULL;

    pu1MsgElemValue = (UINT1 *) MemAllocMemBlk (CAPWAP_GENASSEMBLERADIO_POOLID);

    if (pu1MsgElemValue == NULL)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapAssembleGenRadioParam : Memory Allocation Failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "CapwapAssembleGenRadioParam : Memory Allocation Failed \r\n"));
        return;
    }

    pu1MsgValue = (UINT1 *) MemAllocMemBlk (CAPWAP_GENASSEMBLERADIO_POOLID);
    if (pu1MsgValue == NULL)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapAssembleGenRadioParam : Memory Allocation Failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "CapwapAssembleGenRadioParam : Memory Allocation Failed \r\n"));
        MemReleaseMemBlock (CAPWAP_GENASSEMBLERADIO_POOLID, pu1MsgElemValue);
        return;
    }

    MEMSET (pu1MsgElemValue, 0, MAX_CAPWAP_GENASSEMBLERADIO_LEN);
    MEMSET (pu1MsgValue, 0, MAX_CAPWAP_GENASSEMBLERADIO_LEN);

    pu1MsgTmpElemValue = pu1MsgElemValue;
    pu1MsgTmpValue = pu1MsgValue;

    for (u1Radioid = 1; u1Radioid <= u2NumRadioElems; u1Radioid++)
    {
        CAPWAP_PUT_1BYTE (pu1MsgElemValue, u1Radioid);
        u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT1));

        pRadioIfGetDB->RadioIfIsGetAllDB.bRadioIfIndex = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bDot11RadioType = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bMultiDomainInfo = OSIX_TRUE;
        pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex =
            (UINT4) (SYS_DEF_MAX_ENET_INTERFACES + u1Radioid);
        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                      pRadioIfGetDB) == OSIX_SUCCESS)
        {
            if ((pRadioIfGetDB->RadioIfGetAllDB.u4Dot11RadioType ==
                 RADIO_TYPE_B) || (pRadioIfGetDB->RadioIfGetAllDB.
                                   u4Dot11RadioType == RADIO_TYPE_BGN)
                || (pRadioIfGetDB->RadioIfGetAllDB.u4Dot11RadioType ==
                    RADIO_TYPE_BG))
            {
                for (u1MultiDomainIndex = 0; ((pRadioIfGetDB->RadioIfGetAllDB.
                                               MultiDomainInfo
                                               [u1MultiDomainIndex].
                                               u2FirstChannelNo) <=
                                              RADIOIF_MAX_CHANNEL_B);
                     u1MultiDomainIndex++)
                {
                    if (pRadioIfGetDB->RadioIfGetAllDB.
                        MultiDomainInfo[u1MultiDomainIndex].u2FirstChannelNo ==
                        0)
                    {
                        break;
                    }
                    CAPWAP_PUT_2BYTE (pu1MsgValue,
                                      pRadioIfGetDB->
                                      RadioIfGetAllDB.
                                      MultiDomainInfo[u1MultiDomainIndex].
                                      u2FirstChannelNo);
                    u2Length = (UINT2) (u2Length + sizeof (UINT2));
                    CAPWAP_PUT_2BYTE (pu1MsgValue,
                                      pRadioIfGetDB->RadioIfGetAllDB.
                                      MultiDomainInfo[u1MultiDomainIndex].
                                      u2NoOfChannels);
                    u2Length = (UINT2) (u2Length + sizeof (UINT2));
                    CAPWAP_PUT_2BYTE (pu1MsgValue,
                                      pRadioIfGetDB->RadioIfGetAllDB.
                                      MultiDomainInfo[u1MultiDomainIndex].
                                      u2MaxTxPowerLevel);
                    u2Length = (UINT2) (u2Length + sizeof (UINT2));
                }
            }
            if (pRadioIfGetDB->RadioIfGetAllDB.u4Dot11RadioType == RADIO_TYPE_G)
            {
                for (u1MultiDomainIndex = 0; ((pRadioIfGetDB->RadioIfGetAllDB.
                                               MultiDomainInfo
                                               [u1MultiDomainIndex].
                                               u2FirstChannelNo) <=
                                              RADIOIF_MAX_CHANNEL_G);
                     u1MultiDomainIndex++)
                {
                    if (pRadioIfGetDB->RadioIfGetAllDB.
                        MultiDomainInfo[u1MultiDomainIndex].u2FirstChannelNo ==
                        0)
                    {
                        break;
                    }
                    CAPWAP_PUT_2BYTE (pu1MsgValue,
                                      pRadioIfGetDB->
                                      RadioIfGetAllDB.
                                      MultiDomainInfo[u1MultiDomainIndex].
                                      u2FirstChannelNo);
                    u2Length = (UINT2) (u2Length + sizeof (UINT2));
                    CAPWAP_PUT_2BYTE (pu1MsgValue,
                                      pRadioIfGetDB->RadioIfGetAllDB.
                                      MultiDomainInfo[u1MultiDomainIndex].
                                      u2NoOfChannels);
                    u2Length = (UINT2) (u2Length + sizeof (UINT2));
                    CAPWAP_PUT_2BYTE (pu1MsgValue,
                                      pRadioIfGetDB->RadioIfGetAllDB.
                                      MultiDomainInfo[u1MultiDomainIndex].
                                      u2MaxTxPowerLevel);
                    u2Length = (UINT2) (u2Length + sizeof (UINT2));
                }
            }
            else if ((pRadioIfGetDB->RadioIfGetAllDB.u4Dot11RadioType ==
                      RADIO_TYPE_A) || (pRadioIfGetDB->RadioIfGetAllDB.
                                        u4Dot11RadioType == RADIO_TYPE_AN)
                     || (pRadioIfGetDB->RadioIfGetAllDB.u4Dot11RadioType ==
                         RADIO_TYPE_AC))

            {
                for (u1MultiDomainIndex = 0;
                     u1MultiDomainIndex < MAX_MULTIDOMAIN_INFO_ELEMENT;
                     u1MultiDomainIndex++)
                {
                    if (pRadioIfGetDB->RadioIfGetAllDB.
                        MultiDomainInfo[u1MultiDomainIndex].u2FirstChannelNo ==
                        0)
                    {
                        break;
                    }
                    else if (pRadioIfGetDB->RadioIfGetAllDB.
                             MultiDomainInfo[u1MultiDomainIndex].
                             u2FirstChannelNo > RADIOIF_MAX_CHANNEL_B)
                    {
                        CAPWAP_PUT_2BYTE (pu1MsgValue,
                                          pRadioIfGetDB->
                                          RadioIfGetAllDB.
                                          MultiDomainInfo[u1MultiDomainIndex].
                                          u2FirstChannelNo);
                        u2Length = (UINT2) (u2Length + sizeof (UINT2));
                        CAPWAP_PUT_2BYTE (pu1MsgValue,
                                          pRadioIfGetDB->RadioIfGetAllDB.
                                          MultiDomainInfo[u1MultiDomainIndex].
                                          u2NoOfChannels);
                        u2Length = (UINT2) (u2Length + sizeof (UINT2));
                        CAPWAP_PUT_2BYTE (pu1MsgValue,
                                          pRadioIfGetDB->RadioIfGetAllDB.
                                          MultiDomainInfo[u1MultiDomainIndex].
                                          u2MaxTxPowerLevel);
                        u2Length = (UINT2) (u2Length + sizeof (UINT2));
                    }
                }
            }
        }
        CAPWAP_PUT_2BYTE (pu1MsgElemValue, u2Length);
        u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT2));
        CAPWAP_PUT_NBYTE (pu1MsgElemValue, *(pu1MsgValue - u2Length), u2Length);
        u2ElemLength = (UINT2) (u2ElemLength + u2Length);
        u2Length = 0;
    }

    u2ElemLength = (UINT2) (u2ElemLength + CAPWAP_MSG_ELEM_TYPE_LEN);
    CAPWAP_PUT_2BYTE (pTxPkt, VENDOR_MULTIDOMAIN_INFO_MSG);
    u2MsgElemLen = (UINT2) (u2MsgElemLen + sizeof (UINT2));

    CAPWAP_PUT_2BYTE (pTxPkt, u2ElemLength);
    u2MsgElemLen = (UINT2) (u2MsgElemLen + sizeof (UINT2));
    CAPWAP_PUT_4BYTE (pTxPkt, VENDOR_ID);
    u2MsgElemLen = (UINT2) (u2MsgElemLen + sizeof (UINT4));

    u2Len = (UINT2) (u2ElemLength - CAPWAP_MSG_ELEM_TYPE_LEN);
    CAPWAP_PUT_NBYTE (pTxPkt, *(pu1MsgElemValue - u2Len), u2Len);
    u2MsgElemLen = (UINT2) (u2MsgElemLen + u2ElemLength);
    *pu2MsgElemLen = u2MsgElemLen;

    MemReleaseMemBlock (CAPWAP_GENASSEMBLERADIO_POOLID, pu1MsgTmpElemValue);
    MemReleaseMemBlock (CAPWAP_GENASSEMBLERADIO_POOLID, pu1MsgTmpValue);
    return;
}

        /*Multi Radio */

        /*****************************************************************************
         * Function     : CapwapAssembleGenRadioParam                                *
         *                                                                           *
         * Description  : General function to store a particular element's           *
         *                type, length and value                                     *
         *                                                                           *
         * Input        : Element type and Buffer address                            *
         *                                                                           *
         * Output       : Assembled buffer and the respective length                 *
         *                                                                           *
         * Returns      : SUCCESS/FAILURE                                            *
         *****************************************************************************/

INT4
CapwapAssembleGenRadioParam (UINT2 u2MsgElemType, UINT1 u1MsgSubType,
                             UINT1 **pu1TxBuf, UINT2 *pu2MsgElemLength,
                             UINT2 u2GenIntId)
{
    CAPWAP_FN_ENTRY ();
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tRadioIfGetDB       RadioIfGetDB;
    UINT1              *pu1MsgTempWtpQos = NULL;
    UINT1              *pu1MsgElemValue = NULL;
    UINT1              *pu1MsgWtpQosElemValue = NULL;
    UINT4               u4RadioIfIndex = 0;
    UINT2               u2MsgElemLen = 0;
    UINT2               u2NumRadioElems = 0;
    UINT2               u2IntId = 0;
    UINT2               u2MsgWtpQosElemLength = 0;
    UINT2               u2MultiDomainIndex = 0;
    UINT1               u1RadioIndex = 0;
    UINT1               u1Index = 0;
    UINT1               u1IsValuePresent = OSIX_FALSE;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        pu1MsgElemValue =
        (UINT1 *) MemAllocMemBlk (CAPWAP_GENASSEMBLERADIO_POOLID);

    if (pu1MsgElemValue == NULL)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapAssembleGenRadioParam : Memory Allocation Failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "CapwapAssembleGenRadioParam : Memory Allocation Failed \r\n"));
        return OSIX_FAILURE;
    }

    pu1MsgWtpQosElemValue =
        (UINT1 *) MemAllocMemBlk (CAPWAP_GENASSEMBLERADIO_POOLID);

    if (pu1MsgWtpQosElemValue == NULL)
    {
        MemReleaseMemBlock (CAPWAP_GENASSEMBLERADIO_POOLID, pu1MsgElemValue);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapAssembleGenRadioParam : Memory Allocation Failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "CapwapAssembleGenRadioParam : Memory Allocation Failed \r\n"));
        return OSIX_FAILURE;
    }

    *pu2MsgElemLength = 0;

    u2IntId = u2GenIntId;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    if (CapwapConfigNoOfRadio (u2IntId, &u2NumRadioElems) != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Getting No of Radios failed\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Getting No of Radios failed\r\n"));
        MemReleaseMemBlock (CAPWAP_GENASSEMBLERADIO_POOLID, pu1MsgElemValue);
        MemReleaseMemBlock (CAPWAP_GENASSEMBLERADIO_POOLID,
                            pu1MsgWtpQosElemValue);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    if (((u2MsgElemType == VENDOR_SPECIFIC_PAYLOAD)
         || (u2MsgElemType == WTP_REBOOT_STATISTICS))
        && ((u1MsgSubType != VENDOR_DOT11N_TYPE)
            && (u1MsgSubType != VENDOR_MULTIDOMAIN_INFO_TYPE)
            && (u1MsgSubType != VENDOR_DFS_CHANNEL_STATS)
#ifdef RFMGMT_WANTED
            && (u1MsgSubType != VENDOR_NEIGHBOR_AP_TYPE)
            && (u1MsgSubType != VENDOR_NEIGHBOR_RADIO2_AP_TYPE)
            && (u1MsgSubType != VENDOR_CLIENT_SCAN_TYPE)
            && (u1MsgSubType != VENDOR_NEIGH_CONFIG_TYPE)
            && (u1MsgSubType != VENDOR_CLIENT_CONFIG_TYPE)
            && (u1MsgSubType != VENDOR_CH_SWITCH_STATUS_TYPE)
            && (u1MsgSubType != VENDOR_CH_SCANNED_LIST)
            && (u1MsgSubType != VENDOR_SPECT_MGMT_TPC_TYPE)
            && (u1MsgSubType != VENDOR_SPECTRUM_MGMT_DFS_TYPE)
#endif
            && (u1MsgSubType != VENDOR_DOT11N_CONFIG_TYPE)))
    {
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
        MEMSET (pu1MsgElemValue, 0, MAX_CAPWAP_GENASSEMBLERADIO_LEN);
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2IntId;
        pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;
        u1RadioIndex = 1;
        pWssIfCapwapDB->CapwapGetDB.u1RadioId = u1RadioIndex;
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                     pWssIfCapwapDB) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Getting Radio index failed\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Getting Radio index failed\r\n"));
            MemReleaseMemBlock (CAPWAP_GENASSEMBLERADIO_POOLID,
                                pu1MsgElemValue);
            MemReleaseMemBlock (CAPWAP_GENASSEMBLERADIO_POOLID,
                                pu1MsgWtpQosElemValue);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
        u4RadioIfIndex = pWssIfCapwapDB->CapwapGetDB.u4IfIndex;
        CapwapFetchGenElem (u2MsgElemType, u1MsgSubType, pu1MsgElemValue,
                            &u2MsgElemLen, u4RadioIfIndex, u1RadioIndex,
                            u1Index);
        if (u2MsgElemLen != 0)
        {
            CAPWAP_PUT_2BYTE (*pu1TxBuf, u2MsgElemType);
            *pu2MsgElemLength = (UINT2) (*pu2MsgElemLength + sizeof (UINT2));

            CAPWAP_PUT_2BYTE (*pu1TxBuf, u2MsgElemLen);
            *pu2MsgElemLength = (UINT2) (*pu2MsgElemLength + sizeof (UINT2));
            CAPWAP_PUT_NBYTE (*pu1TxBuf, *pu1MsgElemValue, u2MsgElemLen);
            *pu2MsgElemLength = (UINT2) (*pu2MsgElemLength + u2MsgElemLen);
        }

    }
    else
    {
        for (u1RadioIndex = 1; u1RadioIndex <= u2NumRadioElems; u1RadioIndex++)
        {
            MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
            MEMSET (pu1MsgElemValue, 0, MAX_CAPWAP_GENASSEMBLERADIO_LEN);
            pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
            pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2IntId;
            pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;
            pWssIfCapwapDB->CapwapGetDB.u1RadioId = u1RadioIndex;
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                         pWssIfCapwapDB) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Getting Radio index failed\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Getting Radio index failed\r\n"));
                MemReleaseMemBlock (CAPWAP_GENASSEMBLERADIO_POOLID,
                                    pu1MsgElemValue);
                MemReleaseMemBlock (CAPWAP_GENASSEMBLERADIO_POOLID,
                                    pu1MsgWtpQosElemValue);
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
            u4RadioIfIndex = pWssIfCapwapDB->CapwapGetDB.u4IfIndex;

            if (u2MsgElemType == IEEE_WTP_QOS)
            {
                MEMSET (pu1MsgWtpQosElemValue, 0,
                        MAX_CAPWAP_GENASSEMBLERADIO_LEN);
                pu1MsgTempWtpQos = pu1MsgWtpQosElemValue;
                u2MsgWtpQosElemLength = 0;
                for (u1Index = 1; u1Index <= WSSIF_RADIOIF_QOS_CONFIG_LEN;
                     u1Index++)
                {
                    CapwapFetchGenElem (u2MsgElemType, u1MsgSubType,
                                        pu1MsgElemValue, &u2MsgElemLen,
                                        u4RadioIfIndex, u1RadioIndex, u1Index);
                    CAPWAP_PUT_NBYTE (pu1MsgTempWtpQos, *pu1MsgElemValue,
                                      u2MsgElemLen);
                    u2MsgWtpQosElemLength =
                        (UINT2) (u2MsgWtpQosElemLength + u2MsgElemLen);
                }
            }
            else if (u2MsgElemType == IEEE_MULTIDOMAIN_CAPABILITY)
            {
                RadioIfGetDB.RadioIfIsGetAllDB.bRadioIfIndex = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bMultiDomainInfo = OSIX_TRUE;
                RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
                    (UINT4) u1RadioIndex + SYS_DEF_MAX_ENET_INTERFACES;

                if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                              &RadioIfGetDB) == OSIX_SUCCESS)
                {
                    u2MsgWtpQosElemLength = 0;
                    if ((RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                         RADIO_TYPE_B) || (RadioIfGetDB.RadioIfGetAllDB.
                                           u4Dot11RadioType == RADIO_TYPE_BG) ||
                        (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                         RADIO_TYPE_BGN))
                    {
                        for (u2MultiDomainIndex = 0;
                             ((RadioIfGetDB.RadioIfGetAllDB.
                               MultiDomainInfo[u2MultiDomainIndex].
                               u2FirstChannelNo) <= RADIOIF_MAX_CHANNEL_B);
                             u2MultiDomainIndex++)
                        {
                            if ((RadioIfGetDB.RadioIfGetAllDB.
                                 MultiDomainInfo[u2MultiDomainIndex].
                                 u2FirstChannelNo) == 0)
                            {
                                continue;
                            }

                            MEMSET (pu1MsgWtpQosElemValue, 0,
                                    MAX_CAPWAP_GENASSEMBLERADIO_LEN);
                            pu1MsgTempWtpQos = pu1MsgWtpQosElemValue;

                            CapwapFetchGenElem (u2MsgElemType, u1MsgSubType,
                                                pu1MsgElemValue, &u2MsgElemLen,
                                                u4RadioIfIndex, u1RadioIndex,
                                                (UINT1) u2MultiDomainIndex);
                            CAPWAP_PUT_NBYTE (pu1MsgTempWtpQos,
                                              *pu1MsgElemValue, u2MsgElemLen);

                            u2MsgElemType = IEEE_MULTIDOMAIN_CAPABILITY;
                            u2MsgElemLen = 8;

                            CAPWAP_PUT_2BYTE (*pu1TxBuf, u2MsgElemType);
                            *pu2MsgElemLength =
                                (UINT2) (*pu2MsgElemLength + sizeof (UINT2));

                            CAPWAP_PUT_2BYTE (*pu1TxBuf, u2MsgElemLen);
                            *pu2MsgElemLength =
                                (UINT2) (*pu2MsgElemLength + sizeof (UINT2));

                            CAPWAP_PUT_NBYTE (*pu1TxBuf, *pu1MsgElemValue,
                                              u2MsgElemLen);

                            *pu2MsgElemLength =
                                (UINT2) (*pu2MsgElemLength + u2MsgElemLen);
                        }
                        u2MsgElemLen = 0;
                        u1IsValuePresent = OSIX_TRUE;
                    }
                    if (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                        RADIO_TYPE_G)
                    {
                        for (u2MultiDomainIndex = 0;
                             ((RadioIfGetDB.RadioIfGetAllDB.
                               MultiDomainInfo[u2MultiDomainIndex].
                               u2FirstChannelNo) <= RADIOIF_MAX_CHANNEL_G);
                             u2MultiDomainIndex++)
                        {
                            if ((RadioIfGetDB.RadioIfGetAllDB.
                                 MultiDomainInfo[u2MultiDomainIndex].
                                 u2FirstChannelNo) == 0)
                            {
                                continue;
                            }

                            MEMSET (pu1MsgWtpQosElemValue, 0,
                                    MAX_CAPWAP_GENASSEMBLERADIO_LEN);
                            pu1MsgTempWtpQos = pu1MsgWtpQosElemValue;

                            CapwapFetchGenElem (u2MsgElemType, u1MsgSubType,
                                                pu1MsgElemValue, &u2MsgElemLen,
                                                u4RadioIfIndex, u1RadioIndex,
                                                (UINT1) u2MultiDomainIndex);
                            CAPWAP_PUT_NBYTE (pu1MsgTempWtpQos,
                                              *pu1MsgElemValue, u2MsgElemLen);

                            u2MsgElemType = IEEE_MULTIDOMAIN_CAPABILITY;
                            u2MsgElemLen = 8;

                            CAPWAP_PUT_2BYTE (*pu1TxBuf, u2MsgElemType);
                            *pu2MsgElemLength =
                                (UINT2) (*pu2MsgElemLength + sizeof (UINT2));

                            CAPWAP_PUT_2BYTE (*pu1TxBuf, u2MsgElemLen);
                            *pu2MsgElemLength =
                                (UINT2) (*pu2MsgElemLength + sizeof (UINT2));

                            CAPWAP_PUT_NBYTE (*pu1TxBuf, *pu1MsgElemValue,
                                              u2MsgElemLen);

                            *pu2MsgElemLength =
                                (UINT2) (*pu2MsgElemLength + u2MsgElemLen);
                        }
                        u2MsgElemLen = 0;
                        u1IsValuePresent = OSIX_TRUE;
                    }
                    else if ((RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                              RADIO_TYPE_A) || (RadioIfGetDB.RadioIfGetAllDB.
                                                u4Dot11RadioType ==
                                                RADIO_TYPE_AN)
                             || (RadioIfGetDB.RadioIfGetAllDB.
                                 u4Dot11RadioType == RADIO_TYPE_AC))
                    {
                        for (u2MultiDomainIndex = 0;
                             u2MultiDomainIndex < MAX_MULTIDOMAIN_INFO_ELEMENT;
                             u2MultiDomainIndex++)
                        {
                            if (RadioIfGetDB.RadioIfGetAllDB.
                                MultiDomainInfo[u2MultiDomainIndex].
                                u2FirstChannelNo == 0)
                            {
                                continue;
                            }
                            else
                            {
                                MEMSET (pu1MsgWtpQosElemValue, 0,
                                        MAX_CAPWAP_GENASSEMBLERADIO_LEN);
                                pu1MsgTempWtpQos = pu1MsgWtpQosElemValue;

                                if (RadioIfGetDB.RadioIfGetAllDB.
                                    MultiDomainInfo[u2MultiDomainIndex].
                                    u2FirstChannelNo > RADIOIF_MAX_CHANNEL_B)
                                {
                                    CapwapFetchGenElem (u2MsgElemType,
                                                        u1MsgSubType,
                                                        pu1MsgElemValue,
                                                        &u2MsgElemLen,
                                                        u4RadioIfIndex,
                                                        u1RadioIndex,
                                                        (UINT1)
                                                        u2MultiDomainIndex);

                                    CAPWAP_PUT_NBYTE (pu1MsgTempWtpQos,
                                                      *pu1MsgElemValue,
                                                      u2MsgElemLen);

                                    u2MsgElemType = IEEE_MULTIDOMAIN_CAPABILITY;
                                    u2MsgElemLen = 8;

                                    CAPWAP_PUT_2BYTE (*pu1TxBuf, u2MsgElemType);
                                    *pu2MsgElemLength =
                                        (UINT2) (*pu2MsgElemLength +
                                                 sizeof (UINT2));

                                    CAPWAP_PUT_2BYTE (*pu1TxBuf, u2MsgElemLen);
                                    *pu2MsgElemLength =
                                        (UINT2) (*pu2MsgElemLength +
                                                 sizeof (UINT2));

                                    CAPWAP_PUT_NBYTE (*pu1TxBuf,
                                                      *pu1MsgElemValue,
                                                      u2MsgElemLen);

                                    *pu2MsgElemLength =
                                        (UINT2) (*pu2MsgElemLength +
                                                 u2MsgElemLen);
                                }
                            }
                        }
                        u2MsgElemLen = 0;
                        u1IsValuePresent = OSIX_TRUE;
                    }
                }
            }
            else
            {
                if ((u1MsgSubType == VENDOR_NEIGHBOR_RADIO2_AP_TYPE)
                    || (u1MsgSubType == VENDOR_NEIGHBOR_AP_TYPE))
                {
                    if ((u1MsgSubType == VENDOR_NEIGHBOR_RADIO2_AP_TYPE)
                        && (u1RadioIndex == 1))
                    {
                        continue;
                    }
                    u1MsgSubType = VENDOR_NEIGHBOR_AP_TYPE;
                }
                u2MsgElemLen = 0;
                if ((CapwapFetchGenElem (u2MsgElemType, u1MsgSubType,
                                         pu1MsgElemValue, &u2MsgElemLen,
                                         u4RadioIfIndex, u1RadioIndex,
                                         u1Index)) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "Failed to fetch Message Element for  given RadioId\r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Failed to fetch Message Element for  given RadioId\r\n"));

                    MemReleaseMemBlock (CAPWAP_GENASSEMBLERADIO_POOLID,
                                        pu1MsgElemValue);
                    MemReleaseMemBlock (CAPWAP_GENASSEMBLERADIO_POOLID,
                                        pu1MsgWtpQosElemValue);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);

                    return OSIX_FAILURE;

                }
            }
            if (u2MsgElemLen != 0)
            {
                u1IsValuePresent = OSIX_TRUE;
                CAPWAP_PUT_2BYTE (*pu1TxBuf, u2MsgElemType);
                *pu2MsgElemLength =
                    (UINT2) (*pu2MsgElemLength + sizeof (UINT2));
                if (u2MsgElemType == IEEE_WTP_QOS)
                {
                    CAPWAP_PUT_2BYTE (*pu1TxBuf, u2MsgWtpQosElemLength);
                    *pu2MsgElemLength =
                        (UINT2) (*pu2MsgElemLength + sizeof (UINT2));
                    CAPWAP_PUT_NBYTE (*pu1TxBuf, *pu1MsgWtpQosElemValue,
                                      u2MsgWtpQosElemLength);
                    *pu2MsgElemLength =
                        (UINT2) (*pu2MsgElemLength + u2MsgWtpQosElemLength);
                }
                else
                {
                    CAPWAP_PUT_2BYTE (*pu1TxBuf, u2MsgElemLen);
                    *pu2MsgElemLength =
                        (UINT2) (*pu2MsgElemLength + sizeof (UINT2));
                    CAPWAP_PUT_NBYTE (*pu1TxBuf, *pu1MsgElemValue,
                                      u2MsgElemLen);
                    *pu2MsgElemLength =
                        (UINT2) (*pu2MsgElemLength + u2MsgElemLen);
                }
            }
            if ((u1RadioIndex == u2NumRadioElems)
                && (u1IsValuePresent == OSIX_FALSE))
            {
                MemReleaseMemBlock (CAPWAP_GENASSEMBLERADIO_POOLID,
                                    pu1MsgElemValue);
                MemReleaseMemBlock (CAPWAP_GENASSEMBLERADIO_POOLID,
                                    pu1MsgWtpQosElemValue);
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
            if (u1MsgSubType == VENDOR_NEIGHBOR_AP_TYPE)
            {
                MemReleaseMemBlock (CAPWAP_GENASSEMBLERADIO_POOLID,
                                    pu1MsgElemValue);
                MemReleaseMemBlock (CAPWAP_GENASSEMBLERADIO_POOLID,
                                    pu1MsgWtpQosElemValue);
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                if (u1IsValuePresent == OSIX_FALSE)
                {
                    return OSIX_FAILURE;
                }
                else
                {
                    return OSIX_SUCCESS;
                }
            }
        }
    }
    MemReleaseMemBlock (CAPWAP_GENASSEMBLERADIO_POOLID, pu1MsgElemValue);
    MemReleaseMemBlock (CAPWAP_GENASSEMBLERADIO_POOLID, pu1MsgWtpQosElemValue);
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

        /*****************************************************************************
         * Function     : CapwapFetchGenElem                                         *
         *                                                                           *
         * Description  : Fetch the values of the parameters whose                   *
         *                boolen flags are set from corresponding DB                 *
         *                                                                           *
         * Input        : Internal ID, Element Type, Multiple element flag,          *
         *                IfIndex, RadioID and QOS Index                             *
         *                                                                           *
         * Output       : Assembled buffer and the respective length                 *
         *                                                                           *
         * Returns      : SUCCESS/FAILURE                                            *
         *****************************************************************************/

INT4
CapwapFetchGenElem (UINT2 u2MsgElemType, UINT1 u1MsgSubType,
                    UINT1 *pu1MsgElemValue, UINT2 *pu2MsgElemLen,
                    UINT4 u4RadioIfIndex, UINT1 u1RadioID, UINT1 u1Index)
{
    tRadioIfGetDB       RadioIfGetDB;
    UINT4               u4DBFetchMacro = 0;
    UINT1               u1RetVal = OSIX_FAILURE;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    if (u4RadioIfIndex == 0)    /* For WTP case */
    {
        u4RadioIfIndex = (UINT4) (SYS_DEF_MAX_ENET_INTERFACES + u1RadioID);
    }

    if (u2MsgElemType == IEEE_WTP_QOS)
    {
        u4DBFetchMacro = WSS_GET_RADIO_QOS_CONFIG_DB;
    }
    else
    {
        u4DBFetchMacro = WSS_GET_RADIO_IF_DB;
    }

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = u4RadioIfIndex;

    CapwapSetMsgElemBoolRadioValue (u2MsgElemType, u1MsgSubType, &RadioIfGetDB);

    if (u2MsgElemType == IEEE_WTP_QOS)
    {
        RadioIfGetDB.RadioIfGetAllDB.u1QosIndex = u1Index;
    }

    if (u2MsgElemType == VENDOR_SPECIFIC_PAYLOAD)
    {
        if ((CapwapFetchMsgElemRadioValue (u2MsgElemType, u1MsgSubType,
                                           &RadioIfGetDB, pu1MsgElemValue,
                                           pu2MsgElemLen, u1RadioID,
                                           u1Index)) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to Fetch Message Element for the  given Radio ID\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to Fetch Message Element for the  given Radio ID\r\n"));
            return OSIX_FAILURE;

        }
        u1RetVal = OSIX_SUCCESS;
    }
    else
    {
        RadioIfGetDB.RadioIfIsGetAllDB.bRadioIfIndex = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bSuppVhtCapInfo = OSIX_TRUE;
        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = u4RadioIfIndex;
        if (WssIfProcessRadioIfDBMsg ((UINT1) u4DBFetchMacro,
                                      &RadioIfGetDB) == OSIX_SUCCESS)
        {
            if ((RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                 RADIO_TYPE_AC) && (u2MsgElemType ==
                                    IEEE_INFORMATION_ELEMENT)
                && (u1MsgSubType != DOT11_INFO_ELEM_HTCAPABILITY))
            {
                if ((WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11AC,
                                               &RadioIfGetDB) != OSIX_SUCCESS))
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "Failed to Manipulate 11AC Message"
                                " Element for the  given Radio ID\r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Failed to Manipulate 11AC Message"
                                  " Element for the  given Radio ID\r\n"));

                    return OSIX_FAILURE;

                }

                if (u1MsgSubType == DOT11AC_INFO_ELEM_VHTOPERATION)
                {
#ifdef WLC_WANTED
                    RadioIfGetDB.RadioIfGetAllDB.Dot11AcOperParams.
                        u1SuppVhtChannelWidth =
                        RadioIfGetDB.RadioIfGetAllDB.Dot11AcOperParams.
                        u1VhtChannelWidth;
                    RadioIfGetDB.RadioIfGetAllDB.Dot11AcOperParams.
                        u1SuppCenterFcy0 =
                        RadioIfGetDB.RadioIfGetAllDB.Dot11AcOperParams.
                        u1CenterFcy0;
#endif
                    if ((WssIfProcessRadioIfDBMsg (WSS_ASSEMBLE_11AC_OPER_INFO,
                                                   &RadioIfGetDB) !=
                         OSIX_SUCCESS))
                    {
                        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                    "Failed to  assemble 11AC"
                                    " operation params\r\n");
                        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                      "Failed to  assemble 11AC"
                                      " operation params\r\n"));
                        return OSIX_FAILURE;

                    }
                }

            }
            if ((RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType == RADIO_TYPE_AN)
                || (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                    RADIO_TYPE_BGN) || (RadioIfGetDB.RadioIfGetAllDB.
                                        u4Dot11RadioType == RADIO_TYPE_AC))
            {
                if (u1MsgSubType == DOT11_INFO_ELEM_HTCAPABILITY)
                {
                    if ((WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N,
                                                   &RadioIfGetDB) !=
                         OSIX_SUCCESS))
                    {
                        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                    "Failed to Manipulate 11n Message"
                                    " Element for the  given Radio ID\r\n");
                        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                      "Failed to Manipulate 11n Message"
                                      " Element for the  given Radio ID\r\n"));
                        return OSIX_FAILURE;
                    }
#ifdef WLC_WANTED
                    if ((WssIfProcessRadioIfDBMsg (WSS_ASSEMBLE_11N_EXTCAP_INFO,
                                                   &RadioIfGetDB) !=
                         OSIX_SUCCESS))
                    {
                        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                    "Failed to  assemble 11AC"
                                    " operation params\r\n");
                        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                      "Failed to  assemble 11AC"
                                      " operation params\r\n"));
                        return OSIX_FAILURE;
                    }
                    if ((WssIfProcessRadioIfDBMsg
                         (WSS_ASSEMBLE_11N_BEAMFORMING_INFO,
                          &RadioIfGetDB) != OSIX_SUCCESS))
                    {
                        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                    "Failed to  assemble 11AC"
                                    " operation params\r\n");
                        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                      "Failed to  assemble 11AC"
                                      " operation params\r\n"));
                        return OSIX_FAILURE;
                    }
#endif
                }

                if (u1MsgSubType == DOT11_INFO_ELEM_HTOPERATION)
                {
                    if ((WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N,
                                                   &RadioIfGetDB) !=
                         OSIX_SUCCESS))
                    {
                        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                    "Failed to Manipulate 11n Message"
                                    " Element for the  given Radio ID\r\n");
                        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                      "Failed to Manipulate 11n Message"
                                      " Element for the  given Radio ID\r\n"));
                        return OSIX_FAILURE;
                    }

#ifdef WLC_WANTED
                    if ((WssIfProcessRadioIfDBMsg (WSS_ASSEMBLE_11N_OPER_INFO,
                                                   &RadioIfGetDB) !=
                         OSIX_SUCCESS))
                    {
                        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                    "Failed to  assemble 11AC"
                                    " operation params\r\n");
                        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                      "Failed to  assemble 11AC"
                                      " operation params\r\n"));
                        return OSIX_FAILURE;
                    }
#endif
                }
            }

            CapwapFetchMsgElemRadioValue (u2MsgElemType, u1MsgSubType,
                                          &RadioIfGetDB,
                                          pu1MsgElemValue, pu2MsgElemLen,
                                          u1RadioID, u1Index);
            u1RetVal = OSIX_SUCCESS;
        }

        else
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to fetch from RADIO DB using IfIndex");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to fetch from RADIO DB using IfIndex"));
            u1RetVal = OSIX_FAILURE;
        }
    }
    return u1RetVal;
}

        /*****************************************************************************
         * Function     : CapwapSetDot11nHTOperBooleanEnable                          *
         *                                                                           *
         * Description  : Set the 802.11n boolen flags of the parameters             *
         *                based on the message element type                          *
         *                                                                           *
         * Input        : Message element type and respective DB                     *
         *                                                                           *
         * Output       : DB with the populated boolean flag values                  *
         *                                                                           *
         * Returns      : NONE                                                       *
         *****************************************************************************/

VOID
CapwapSetDot11nHTOperBooleanEnable (tRadioIfGetDB * pRadioIfGetDB)
{
#ifdef WLC_WANTED
    pRadioIfGetDB->RadioIfIsGetAllDB.bPrimaryChannel = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bBasicMCSSet = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bSecondaryChannel = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bStaChanWidth = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bRifsMode = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bHtProtection = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bNongfHtStasPresent = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bObssNonHtStasPresent = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bDualBeacon = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bDualCtsProtection = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bStbcBeacon = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bLsigTxopProtFulSup = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bPcoActive = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bPcoPhase = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bHTOpeInfo = OSIX_TRUE;
#else
    pRadioIfGetDB->RadioIfIsGetAllDB.bSuppPrimaryCh = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bSuppHTOpeInfo = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bBasicMCSSet = OSIX_TRUE;
#endif

}

        /*****************************************************************************
         * Function     : CapwapSetDot11nHTCapBooleanEnable                          *
         *                                                                           *
         * Description  : Set the 802.11n boolen flags of the parameters             *
         *                based on the message element type                          *
         *                                                                           *
         * Input        : Message element type and respective DB                     *
         *                                                                           *
         * Output       : DB with the populated boolean flag values                  *
         *                                                                           *
         * Returns      : NONE                                                       *
         *****************************************************************************/

VOID
CapwapSetDot11nHTCapBooleanEnable (tRadioIfGetDB * pRadioIfGetDB)
{
#ifdef WLC_WANTED
    pRadioIfGetDB->RadioIfIsGetAllDB.bHtCapInfo = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bAmpduParam = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bHtCapaMcs = OSIX_TRUE;
    /*HT Ext Capabilities */
    pRadioIfGetDB->RadioIfIsGetAllDB.bPco = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bPcoTransTime = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bMcsFeedback = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bHtcSupp = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bRdResponder = OSIX_TRUE;
    /*Beam Forming */
    pRadioIfGetDB->RadioIfIsGetAllDB.bTxBeamCapParam = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bImpTranBeamRecCapable = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bRecStagSoundCapable = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bTransStagSoundCapable = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bRecNdpCapable = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bTransNdpCapable = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bImpTranBeamCapable = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bCalibration = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bExplCsiTranBeamCapable = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bExplNoncompSteCapable = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bExplCompSteCapable = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bExplTranBeamCsiFeedback = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bExplNoncompBeamFbCapable = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bExplCompBeamFbCapable = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bMinGrouping = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bCsiNoofBeamAntSupported = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bNoncompSteNoBeamformer = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bCompSteNoBeamformer = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bCSIMaxNoRowsBeamformer = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bChannelEstCapability = OSIX_TRUE;
#else
    pRadioIfGetDB->RadioIfIsGetAllDB.bSuppHtCapInfo = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bSuppAmpduParam = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bAmpduParam = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bSuppHtCapaMcs = OSIX_TRUE;
#endif

}

        /*****************************************************************************
         * Function     : CapwapSetMsgElemBoolRadioValue                             *
         *                                                                           *
         * Description  : Set the corresponding boolen flags of the parameters       *
         *                based on the message element type                          *
         *                                                                           *
         * Input        : Message element type and respective DB                     *
         *                                                                           *
         * Output       : DB with the populated boolean flag values                  *
         *                                                                           *
         * Returns      : NONE                                                       *
         *****************************************************************************/

VOID
CapwapSetMsgElemBoolRadioValue (UINT2 u2MsgElemType,
                                UINT1 u1SubType, tRadioIfGetDB * pRadioIfGetDB)
{
    UINT1               u1antennaIndex = 0;

    switch (u2MsgElemType)
    {
        case WTP_RADIO_INFO:
            pRadioIfGetDB->RadioIfIsGetAllDB.bDot11RadioType = OSIX_TRUE;
            break;
        case IEEE_ANTENNA:
            pRadioIfGetDB->RadioIfGetAllDB.pu1AntennaSelection =
                UtlShMemAllocAntennaSelectionBuf ();
            if (pRadioIfGetDB->RadioIfGetAllDB.pu1AntennaSelection == NULL)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "\r%% Memory allocation for Antenna"
                            " selection failed.\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "\r%% Memory allocation for Antenna"
                              " selection failed.\r\n"));
                return;
            }
            pRadioIfGetDB->RadioIfIsGetAllDB.bCurrentTxAntenna = OSIX_TRUE;
            if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, pRadioIfGetDB) !=
                OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Failed to Get IEEE Antena Information from Radio DB \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to Get IEEE Antena Information from Radio DB \r\n"));
                UtlShMemFreeAntennaSelectionBuf (pRadioIfGetDB->
                                                 RadioIfGetAllDB.
                                                 pu1AntennaSelection);
                return;
            }
            pRadioIfGetDB->RadioIfIsGetAllDB.bDiversitySupport = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bAntennaMode = OSIX_TRUE;
            for (u1antennaIndex = 0;
                 u1antennaIndex <
                 pRadioIfGetDB->RadioIfGetAllDB.u1CurrentTxAntenna;
                 u1antennaIndex++)
            {
                pRadioIfGetDB->
                    RadioIfIsGetAllDB.bAntennaSelection[u1antennaIndex] =
                    OSIX_TRUE;
            }
            break;
        case IEEE_DIRECT_SEQUENCE_CONTROL:
            pRadioIfGetDB->RadioIfIsGetAllDB.bCurrentCCAMode = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bCurrentChannel = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bEDThreshold = OSIX_TRUE;
            break;
        case IEEE_INFORMATION_ELEMENT:
            pRadioIfGetDB->RadioIfIsGetAllDB.bBPFlag = OSIX_TRUE;
            switch (u1SubType)
            {
                case DOT11_INFO_ELEM_HTCAPABILITY:
                    CapwapSetDot11nHTCapBooleanEnable (pRadioIfGetDB);
                    break;

                case DOT11_INFO_ELEM_HTOPERATION:
                    CapwapSetDot11nHTOperBooleanEnable (pRadioIfGetDB);
                    break;

                case DOT11AC_INFO_ELEM_VHTCAPABILITY:

                    pRadioIfGetDB->RadioIfIsGetAllDB.bVhtCapInfo = OSIX_TRUE;
                    pRadioIfGetDB->RadioIfIsGetAllDB.bSuppChanWidth = OSIX_TRUE;
                    pRadioIfGetDB->RadioIfIsGetAllDB.bShortGi160 = OSIX_TRUE;
                    pRadioIfGetDB->RadioIfIsGetAllDB.bSuBeamFormer = OSIX_TRUE;
                    pRadioIfGetDB->RadioIfIsGetAllDB.bSuBeamFormee = OSIX_TRUE;
                    pRadioIfGetDB->RadioIfIsGetAllDB.bSuppBeamFormAnt =
                        OSIX_TRUE;
                    pRadioIfGetDB->RadioIfIsGetAllDB.bSoundDimension =
                        OSIX_TRUE;
                    pRadioIfGetDB->RadioIfIsGetAllDB.bMuBeamFormer = OSIX_TRUE;
                    pRadioIfGetDB->RadioIfIsGetAllDB.bMuBeamFormee = OSIX_TRUE;
                    pRadioIfGetDB->RadioIfIsGetAllDB.bVhtTxOpPs = OSIX_TRUE;
                    pRadioIfGetDB->RadioIfIsGetAllDB.bHtcVhtCapable = OSIX_TRUE;
                    pRadioIfGetDB->RadioIfIsGetAllDB.bVhtLinkAdaption =
                        OSIX_TRUE;
                    pRadioIfGetDB->RadioIfIsGetAllDB.bVhtRxAntenna = OSIX_TRUE;
                    pRadioIfGetDB->RadioIfIsGetAllDB.bVhtTxAntenna = OSIX_TRUE;

#ifdef WLC_WANTED
                    pRadioIfGetDB->RadioIfIsGetAllDB.bMaxMPDULen = OSIX_TRUE;
                    pRadioIfGetDB->RadioIfIsGetAllDB.bRxLpdc = OSIX_TRUE;
                    pRadioIfGetDB->RadioIfIsGetAllDB.bShortGi80 = OSIX_TRUE;
                    pRadioIfGetDB->RadioIfIsGetAllDB.bVhtMaxAMPDU = OSIX_TRUE;
                    pRadioIfGetDB->RadioIfIsGetAllDB.bVhtCapaMcs = OSIX_TRUE;
                    pRadioIfGetDB->RadioIfIsGetAllDB.bTxStbc = OSIX_TRUE;
                    pRadioIfGetDB->RadioIfIsGetAllDB.bRxStbc = OSIX_TRUE;
#else
                    pRadioIfGetDB->RadioIfIsGetAllDB.bSuppMaxMPDULen =
                        OSIX_TRUE;
                    pRadioIfGetDB->RadioIfIsGetAllDB.bSuppRxLpdc = OSIX_TRUE;
                    pRadioIfGetDB->RadioIfIsGetAllDB.bSuppShortGi80 = OSIX_TRUE;
                    pRadioIfGetDB->RadioIfIsGetAllDB.bSuppVhtMaxAMPDU =
                        OSIX_TRUE;
                    pRadioIfGetDB->RadioIfIsGetAllDB.bSuppVhtCapaMcs =
                        OSIX_TRUE;
                    pRadioIfGetDB->RadioIfIsGetAllDB.bSuppVhtTxStbc = OSIX_TRUE;
                    pRadioIfGetDB->RadioIfIsGetAllDB.bSuppVhtRxStbc = OSIX_TRUE;
#endif
                    break;

                case DOT11AC_INFO_ELEM_VHTOPERATION:

                    pRadioIfGetDB->RadioIfIsGetAllDB.bVhtOperInfo = OSIX_TRUE;
                    pRadioIfGetDB->RadioIfIsGetAllDB.bVhtOperMcsSet = OSIX_TRUE;
#ifdef WLC_WANTED
                    pRadioIfGetDB->RadioIfIsGetAllDB.bVhtChannelWidth =
                        OSIX_TRUE;
                    pRadioIfGetDB->RadioIfIsGetAllDB.bCenterFcy0 = OSIX_TRUE;
#else
                    pRadioIfGetDB->RadioIfIsGetAllDB.bSuppVhtChannelWidth =
                        OSIX_TRUE;
                    pRadioIfGetDB->RadioIfIsGetAllDB.bSuppCenterFcy0 =
                        OSIX_TRUE;
#endif
                    break;

                default:
                    break;
            }
            break;
        case IEEE_MAC_OPERATION:
            pRadioIfGetDB->RadioIfIsGetAllDB.bRTSThreshold = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bShortRetry = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bLongRetry = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bFragmentationThreshold =
                OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bTxMsduLifetime = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bRxMsduLifetime = OSIX_TRUE;
            break;
        case IEEE_MULTIDOMAIN_CAPABILITY:
            pRadioIfGetDB->RadioIfIsGetAllDB.bMultiDomainInfo = OSIX_TRUE;
            break;
        case IEEE_OFDM_CONTROL:
            pRadioIfGetDB->RadioIfIsGetAllDB.bSupportedBand = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bT1Threshold = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bCurrentChannel = OSIX_TRUE;
            break;
        case IEEE_SUPPORTED_RATES:
            pRadioIfGetDB->RadioIfIsGetAllDB.bSupportedRate = OSIX_TRUE;
            break;
        case IEEE_TX_POWER:
            pRadioIfGetDB->RadioIfIsGetAllDB.bCurrentTxPower = OSIX_TRUE;
            break;
        case IEEE_TX_POWERLEVEL:
            pRadioIfGetDB->RadioIfIsGetAllDB.bNoOfSupportedPowerLevels =
                OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bTxPowerLevel = OSIX_TRUE;
            break;
        case IEEE_WTP_RADIO_CONFIGURATION:
            pRadioIfGetDB->RadioIfIsGetAllDB.bShortPreamble = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bNoOfBssId = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bDTIMPeriod = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bBeaconPeriod = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bCountryString = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bMacAddr = OSIX_TRUE;
            break;
        case IEEE_RATE_SET:
            pRadioIfGetDB->RadioIfIsGetAllDB.bOperationalRate = OSIX_TRUE;
            break;
        case IEEE_WTP_QOS:
            pRadioIfGetDB->RadioIfIsGetAllDB.bQosIndex = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bTaggingPolicy = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bQueueDepth = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bCwMin = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bCwMax = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bAifsn = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bQosPrio = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bDscp = OSIX_TRUE;
            break;
        case RADIO_OPER_STATE:
            pRadioIfGetDB->RadioIfIsGetAllDB.bFailureStatus = OSIX_TRUE;
            pRadioIfGetDB->RadioIfIsGetAllDB.bOperStatus = OSIX_TRUE;
            break;
        case WTP_RADIO_STATISTICS:
            pRadioIfGetDB->RadioIfIsGetAllDB.bRadioStats = OSIX_TRUE;
            break;
        case IEEE_STATISTICS:
            pRadioIfGetDB->RadioIfIsGetAllDB.bIeee80211Stats = OSIX_TRUE;
            break;
        default:
            break;
    }

}

#ifdef WTP_WANTED
/*****************************************************************************
 * Function     : CapwapUtilSetConflictList                                  *
 *                                                                           *
 * Description  : It will set the input channel into the identifed           *
 *                conflict list.                                             *
 *                                                                           *
 * Input        : input channel.                                             *
 *                                                                           *
 *                                                                           *
 * Output       : NONE                                                       *
 *                                                                           *
 * Returns      : NONE                                                       *
 *****************************************************************************/
PRIVATE UINT1
CapwapUtilSetConflictList (UINT1 u1Channel,
                           UINT1 u1Counter, UINT1 *pu1ConflictList)
{
    if (u1Counter > 1)
    {
        if (pu1ConflictList[u1Counter - 1] == u1Channel)
        {
            return OSIX_FAILURE;
        }
    }
    if (pu1ConflictList[u1Counter] != u1Channel)
    {
        pu1ConflictList[u1Counter] = u1Channel;
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}
#endif

        /*****************************************************************************
         * Function     : CapwapFetchMsgElemRadioValue                               *
         *                                                                           *
         * Description  : Populate the fetched DB parameters on to a buffer          *
         *                based on the corresponding message type.                   *
         *                                                                           *
         * Input        : Message element type, Radio ID, QOS Index and              *
         *                respective DB                                              *
         *                                                                           *
         * Output       : Populated buffer and the corresponding length              *
         *                                                                           *
         * Returns      : NONE                                                       *
         *****************************************************************************/

INT1
CapwapFetchMsgElemRadioValue (UINT2 u2MsgElemType, UINT1 u1MsgSubType,
                              tRadioIfGetDB * pRadioIfGetDB,
                              UINT1 *pu1MsgElemValue, UINT2 *pu2MsgElemLen,
                              UINT1 u1RadioID, UINT1 u1QosIndex)
{
    tRadioIfGetDB       RadioIfGetDB;
#ifdef WTP_WANTED
    unApHdlrMsgStruct   ApHdlrMsg;
#endif
    tVendorSpecPayload  vendSpec;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tWssWlanMsgStruct  *pWssWlanMsgStruct = NULL;
    tWssWlanDB          WssWlanDB;
    tWssIfApHdlrDB      WssIfApHdlrDB;

    UINT2               u2NumRadioElems = 0;
    UINT1               u1Radioid = 0;
    UINT1               u1Wlanid = 0;
    UINT1               u1Index = 0;
    UINT1               u1NoofBss = 0;
    UINT1               u1InfoFlag = 0;

#ifdef RFMGMT_WANTED
    tRfMgmtDB           RfMgmtDB;
    tRfMgmtMsgStruct    WssMsgStruct;

#ifdef WLC_WANTED
#ifdef ROGUEAP_WANTED
    tSNMP_OCTET_STRING_TYPE RfName;
    UINT1               au1RogueRfName[WSSMAC_MAX_SSID_LEN];
#endif
#endif

#endif
    UINT1               u1WlanId = 0;
    UINT1              *pu1MsgTempElemValue = pu1MsgElemValue;
    UINT2               u2ElemLength = 0;
    UINT1               u1Temp = 0;
    UINT1              *pu1TempPrint = NULL;
    UINT1               u1ArrIndex = 0;
#ifdef WTP_WANTED
    tRemoteSessionManager *pSessEntry = NULL;
    UINT1               u1Index1 = 0;
    UINT1               u1Index2 = 0;
    UINT1               u1Index3 = 0;
    UINT1               u1ConflictList[RFMGMT_MAX_CHANNELA];
    UINT1               u1PrimaryChannel;
    UINT1               u1SecondaryChannel;
    UINT1               u1PrimaryOffset;
    UINT1               u1SecondaryOffset;
    UINT1               u1ChannIndex;
    UINT1               u1ConflictCounter = 0;
    UINT2               u2DiscReqSentCount = 0;
    UINT1               u1DiscRespCount = 0;
    UINT4               u4CurrSentBytes = 0;
    UINT4               u4CurrRcvdBytes = 0;
    UINT4               u4RadioOutOctets = 0;
    UINT4               u4RadioInOctets = 0;
    UINT4               u4Ipaddr = 0;
    UINT1               u1WlanPerRadio[WSSWLAN_END_WLANID_PER_RADIO] = { 0 };
#endif
#ifdef WTP_WANTED
#ifdef NPAPI_WANTED
    UINT4               u4HwCount = 0;
    UINT1               u1Count = 0;
    UINT4               u4HwCount1 = 0;
    UINT1               u1NumStaCount = 0;
    tFsHwNp             FsHwNp;
    UINT1               au1WlanName[6] = { 0 };
    unWssMsgStructs     WssMsgStructs;
#endif
#endif

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE);

    pWssWlanMsgStruct =
        (tWssWlanMsgStruct *) (VOID *) UtlShMemAllocWssWlanBuf ();
    if (pWssWlanMsgStruct == NULL)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapGetConfigStatVendorPayload:- "
                    "UtlShMemAllocWssWlanBuf returned failure\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "CapwapGetConfigStatVendorPayload:- "
                      "UtlShMemAllocWssWlanBuf returned failure\n"));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
#ifdef RFMGMT_WANTED
    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
    MEMSET (&WssMsgStruct, 0, sizeof (tRfMgmtMsgStruct));
#endif
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
#ifdef WTP_WANTED
    MEMSET (&ApHdlrMsg, 0, sizeof (unApHdlrMsgStruct));
#endif
    MEMSET (&vendSpec, 0, sizeof (tVendorSpecPayload));
    MEMSET (&WssWlanDB, 0, sizeof (tWssWlanDB));
    MEMSET (&WssIfApHdlrDB, 0, sizeof (tWssIfApHdlrDB));

    switch (u2MsgElemType)
    {
        case WTP_RADIO_INFO:
            CAPWAP_PUT_1BYTE (pu1MsgElemValue, u1RadioID);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT1));
            CAPWAP_PUT_4BYTE (pu1MsgElemValue,
                              pRadioIfGetDB->RadioIfGetAllDB.u4Dot11RadioType);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT4));

            CAPWAP_TRC2 (CAPWAP_RADIO_PARM_TRC,
                         "\n* WTP_RADIO_INFO : \nu1RadioID %d\n RadioType %d\n",
                         u1RadioID,
                         pRadioIfGetDB->RadioIfGetAllDB.u4Dot11RadioType);
            SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4CapwapSysLogId,
                          "\n* WTP_RADIO_INFO : \nu1RadioID %d\n RadioType %d\n",
                          u1RadioID,
                          pRadioIfGetDB->RadioIfGetAllDB.u4Dot11RadioType));

            *pu2MsgElemLen = u2ElemLength;
            break;
        case IEEE_ANTENNA:
            CAPWAP_PUT_1BYTE (pu1MsgElemValue, u1RadioID);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT1));
            CAPWAP_PUT_1BYTE (pu1MsgElemValue,
                              pRadioIfGetDB->
                              RadioIfGetAllDB.u1DiversitySupport);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT1));
            CAPWAP_PUT_1BYTE (pu1MsgElemValue,
                              pRadioIfGetDB->RadioIfGetAllDB.u1AntennaMode);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT1));
            CAPWAP_PUT_1BYTE (pu1MsgElemValue,
                              pRadioIfGetDB->
                              RadioIfGetAllDB.u1CurrentTxAntenna);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT1));
            CAPWAP_PUT_NBYTE (pu1MsgElemValue,
                              pRadioIfGetDB->
                              RadioIfGetAllDB.pu1AntennaSelection,
                              pRadioIfGetDB->
                              RadioIfGetAllDB.u1CurrentTxAntenna);
            u2ElemLength =
                (UINT2) (u2ElemLength +
                         pRadioIfGetDB->RadioIfGetAllDB.u1CurrentTxAntenna);

            CAPWAP_TRC4 (CAPWAP_RADIO_PARM_TRC,
                         "\n* IEEE_ANTENNA : \nu1RadioID %d\n DiversitySupport %d\n AntennaMode %d\n CurrentTxAntenna %d\n",
                         u1RadioID,
                         pRadioIfGetDB->RadioIfGetAllDB.u1DiversitySupport,
                         pRadioIfGetDB->RadioIfGetAllDB.u1AntennaMode,
                         pRadioIfGetDB->RadioIfGetAllDB.u1CurrentTxAntenna);
            SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4CapwapSysLogId,
                          "\n* IEEE_ANTENNA : \nu1RadioID %d\n DiversitySupport %d\n AntennaMode %d\n CurrentTxAntenna %d\n",
                          u1RadioID,
                          pRadioIfGetDB->RadioIfGetAllDB.u1DiversitySupport,
                          pRadioIfGetDB->RadioIfGetAllDB.u1AntennaMode,
                          pRadioIfGetDB->RadioIfGetAllDB.u1CurrentTxAntenna));
            CAPWAP_TRC (CAPWAP_RADIO_PARM_TRC, "AntennaSelection\n");
            SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4CapwapSysLogId,
                          "AntennaSelection\n"));
            pu1TempPrint = pRadioIfGetDB->RadioIfGetAllDB.pu1AntennaSelection;
            for (u1Index = 1;
                 u1Index <= pRadioIfGetDB->RadioIfGetAllDB.u1CurrentTxAntenna;
                 u1Index++)
            {
                CAPWAP_TRC1 (CAPWAP_RADIO_PARM_TRC, "%d\t", pu1TempPrint);
                SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4CapwapSysLogId, "%d\t",
                              pu1TempPrint));
                pu1TempPrint++;
            }
            *pu2MsgElemLen = u2ElemLength;
            UtlShMemFreeAntennaSelectionBuf (pRadioIfGetDB->
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            break;
        case IEEE_DIRECT_SEQUENCE_CONTROL:
            CAPWAP_PUT_1BYTE (pu1MsgElemValue, u1RadioID);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT1));
            CAPWAP_PUT_1BYTE (pu1MsgElemValue, 0);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT1));
            CAPWAP_PUT_1BYTE (pu1MsgElemValue,
                              pRadioIfGetDB->RadioIfGetAllDB.u1CurrentChannel);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT1));
            CAPWAP_PUT_1BYTE (pu1MsgElemValue,
                              pRadioIfGetDB->RadioIfGetAllDB.u1CurrentCCAMode);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT1));
            CAPWAP_PUT_4BYTE (pu1MsgElemValue,
                              (UINT4) pRadioIfGetDB->RadioIfGetAllDB.
                              i4EDThreshold);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT4));

            CAPWAP_TRC4 (CAPWAP_RADIO_PARM_TRC,
                         "\n* IEEE_DIRECT_SEQUENCE_CONTROL : \nu1RadioID %d\n CurrentChannel %d\n CurrentCCAMode %d\n EDThreshold %d\n",
                         u1RadioID,
                         pRadioIfGetDB->RadioIfGetAllDB.u1CurrentChannel,
                         pRadioIfGetDB->RadioIfGetAllDB.u1CurrentCCAMode,
                         pRadioIfGetDB->RadioIfGetAllDB.i4EDThreshold);
            SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4CapwapSysLogId,
                          "\n* IEEE_DIRECT_SEQUENCE_CONTROL : \nu1RadioID %d\n CurrentChannel %d\n CurrentCCAMode %d\n EDThreshold %d\n",
                          u1RadioID,
                          pRadioIfGetDB->RadioIfGetAllDB.u1CurrentChannel,
                          pRadioIfGetDB->RadioIfGetAllDB.u1CurrentCCAMode,
                          pRadioIfGetDB->RadioIfGetAllDB.i4EDThreshold));
            *pu2MsgElemLen = u2ElemLength;
            break;
        case IEEE_INFORMATION_ELEMENT:
            u1WlanId = RADIO_VALUE_1;
            CAPWAP_PUT_1BYTE (pu1MsgElemValue, u1RadioID);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT1));
            CAPWAP_PUT_1BYTE (pu1MsgElemValue, u1WlanId);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT1));
            CAPWAP_PUT_1BYTE (pu1MsgElemValue,
                              pRadioIfGetDB->RadioIfGetAllDB.u1BPFlag);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT1));
            CAPWAP_TRC3 (CAPWAP_RADIO_PARM_TRC,
                         "\n* IEEE_INFORMATION_ELEMENT : \nu1RadioID "
                         " %d\nu1WlanID %d\nu1BPFlag  %d\n",
                         u1RadioID,
                         pRadioIfGetDB->RadioIfGetAllDB.u1WlanId,
                         pRadioIfGetDB->RadioIfGetAllDB.u1BPFlag);
            SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4CapwapSysLogId,
                          "\n* IEEE_INFORMATION_ELEMENT : \nu1RadioID "
                          " %d\nu1WlanID %d\nu1BPFlag  %d\n",
                          u1RadioID,
                          pRadioIfGetDB->RadioIfGetAllDB.u1WlanId,
                          pRadioIfGetDB->RadioIfGetAllDB.u1BPFlag));

            switch (u1MsgSubType)
            {
                case DOT11_INFO_ELEM_HTCAPABILITY:

                    if (((pRadioIfGetDB->RadioIfGetAllDB.u4Dot11RadioType) ==
                         RADIO_TYPE_BGN)
                        || ((pRadioIfGetDB->RadioIfGetAllDB.u4Dot11RadioType) ==
                            RADIO_TYPE_AN)
                        || ((pRadioIfGetDB->RadioIfGetAllDB.u4Dot11RadioType) ==
                            RADIO_TYPE_AC))
                    {
                        u1InfoFlag = RADIO_VALUE_1;
                        CAPWAP_PUT_1BYTE (pu1MsgElemValue,
                                          DOT11_INFO_ELEM_HTCAPABILITY);
                        u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT1));
                        CAPWAP_PUT_1BYTE (pu1MsgElemValue,
                                          DOT11_INFO_HT_CAP_MSG_LEN);
                        u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT1));
#ifdef WLC_WANTED
                        CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                                          pRadioIfGetDB->RadioIfGetAllDB.
                                          Dot11NCapaParams.u2HtCapInfo);

                        u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT2));
                        CAPWAP_PUT_1BYTE (pu1MsgElemValue, pRadioIfGetDB->RadioIfGetAllDB.Dot11NampduParams.u1AmpduParam);    /*AMPUParam */
                        u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT1));
                        CAPWAP_PUT_NBYTE (pu1MsgElemValue,
                                          pRadioIfGetDB->RadioIfGetAllDB.
                                          Dot11NsuppMcsParams.au1HtCapaMcs,
                                          CAPWAP_11N_MCS_RATE_LEN);
                        u2ElemLength =
                            (UINT2) (u2ElemLength + CAPWAP_11N_MCS_RATE_LEN);
                        CAPWAP_PUT_2BYTE (pu1MsgElemValue, pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.u2HtExtCap);    /*HTExtCap */
                        u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT2));
                        CAPWAP_PUT_4BYTE (pu1MsgElemValue, pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.u4TxBeamCapParam);    /*TranBeamformCap */
                        u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT4));
                        CAPWAP_PUT_1BYTE (pu1MsgElemValue, 0);    /*ASELCap */
                        u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT1));
#else
                        CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                                          pRadioIfGetDB->RadioIfGetAllDB.
                                          Dot11NCapaParams.u2SuppHtCapInfo);
                        u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT2));
                        CAPWAP_PUT_1BYTE (pu1MsgElemValue, pRadioIfGetDB->RadioIfGetAllDB.Dot11NampduParams.u1AmpduParam);    /*AMPUParam */
                        u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT1));
                        CAPWAP_PUT_NBYTE (pu1MsgElemValue,
                                          pRadioIfGetDB->RadioIfGetAllDB.
                                          Dot11NsuppMcsParams.au1SuppHtCapaMcs,
                                          CAPWAP_11N_MCS_RATE_LEN);
                        u2ElemLength =
                            (UINT2) (u2ElemLength + CAPWAP_11N_MCS_RATE_LEN);

                        CAPWAP_PUT_NBYTE (pu1MsgElemValue,
                                          pRadioIfGetDB->RadioIfGetAllDB.
                                          Dot11NsuppMcsParams.au1SuppHtCapaMcs,
                                          CAPWAP_11N_MCS_RATE_LEN);
                        u2ElemLength =
                            (UINT2) (u2ElemLength + CAPWAP_11N_MCS_RATE_LEN);
                        CAPWAP_PUT_2BYTE (pu1MsgElemValue, 0);    /*HTExtCap */
                        u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT2));
                        CAPWAP_PUT_4BYTE (pu1MsgElemValue, 0);    /*TranBeamformCap */
                        u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT4));
                        CAPWAP_PUT_1BYTE (pu1MsgElemValue, 0);    /*ASELCap */
                        u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT1));

#endif
                        CAPWAP_TRC1 (CAPWAP_RADIO_PARM_TRC,
                                     "\n* DOT11_INFO_ELEM_HTCAPABILITY:\nu2HTCapInfo %d\n",
                                     pRadioIfGetDB->RadioIfGetAllDB.
                                     u2HTCapInfo);
                        SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4CapwapSysLogId,
                                      "\n* DOT11_INFO_ELEM_HTCAPABILITY:\nu2HTCapInfo %d\n",
                                      pRadioIfGetDB->RadioIfGetAllDB.
                                      u2HTCapInfo));

                        *pu2MsgElemLen = u2ElemLength;
                    }
                    break;
                case DOT11_INFO_ELEM_HTOPERATION:

                    if (((pRadioIfGetDB->RadioIfGetAllDB.u4Dot11RadioType) ==
                         RADIO_TYPE_BGN)
                        || ((pRadioIfGetDB->RadioIfGetAllDB.u4Dot11RadioType) ==
                            RADIO_TYPE_AN)
                        || ((pRadioIfGetDB->RadioIfGetAllDB.u4Dot11RadioType) ==
                            RADIO_TYPE_AC))
                    {
                        u1InfoFlag = RADIO_VALUE_1;
                        CAPWAP_PUT_1BYTE (pu1MsgElemValue,
                                          DOT11_INFO_ELEM_HTOPERATION);
                        u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT1));
                        CAPWAP_PUT_1BYTE (pu1MsgElemValue,
                                          DOT11_INFO_HT_OPER_MSG_LEN);
                        u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT1));
#ifdef WLC_WANTED
                        CAPWAP_PUT_1BYTE (pu1MsgElemValue, pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.u1PrimaryChannel);    /*Primary Channel */
                        u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT1));

                        CAPWAP_PUT_NBYTE (pu1MsgElemValue,
                                          pRadioIfGetDB->RadioIfGetAllDB.
                                          Dot11NhtOperation.au1HTOpeInfo,
                                          WSSMAC_HTOPE_INFO);
                        u2ElemLength =
                            (UINT2) (u2ElemLength + WSSMAC_HTOPE_INFO);

                        CAPWAP_PUT_NBYTE (pu1MsgElemValue,
                                          pRadioIfGetDB->RadioIfGetAllDB.
                                          Dot11NhtOperation.au1BasicMCSSet,
                                          WSSMAC_BASIC_MCS_SET);
                        u2ElemLength =
                            (UINT2) (u2ElemLength + WSSMAC_BASIC_MCS_SET);
#else

                        CAPWAP_PUT_1BYTE (pu1MsgElemValue, pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.u1PrimaryChannel);    /*Primary Channel */
                        u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT1));
                        CAPWAP_PUT_NBYTE (pu1MsgElemValue, pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.au1HTOpeInfo, WSSMAC_HTOPE_INFO);    /* HT Operation */
                        u2ElemLength =
                            (UINT2) (u2ElemLength + WSSMAC_HTOPE_INFO);
                        CAPWAP_PUT_NBYTE (pu1MsgElemValue, pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.au1BasicMCSSet, WSSMAC_BASIC_MCS_SET);    /*Basic MCS Set */
                        u2ElemLength =
                            (UINT2) (u2ElemLength + WSSMAC_BASIC_MCS_SET);

#endif
                        CAPWAP_TRC1 (CAPWAP_RADIO_PARM_TRC,
                                     "\n* DOT11_INFO_ELEM_HTOPERATION:\nu1PrimaryCh %d\n",
                                     pRadioIfGetDB->RadioIfGetAllDB.
                                     Dot11NhtOperation.u1PrimaryChannel);
                        SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4CapwapSysLogId,
                                      "\n* DOT11_INFO_ELEM_HTOPERATION:\nu1PrimaryCh %d\n",
                                      pRadioIfGetDB->RadioIfGetAllDB.
                                      Dot11NhtOperation.u1PrimaryChannel));

                        *pu2MsgElemLen = u2ElemLength;
                    }
                    break;

                case DOT11AC_INFO_ELEM_VHTCAPABILITY:

                    if ((pRadioIfGetDB->RadioIfGetAllDB.u4Dot11RadioType) ==
                        RADIO_TYPE_AC)
                    {
                        u1InfoFlag = RADIO_VALUE_1;
                        CAPWAP_PUT_1BYTE (pu1MsgElemValue,
                                          DOT11AC_INFO_ELEM_VHTCAPABILITY);
                        u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT1));
                        CAPWAP_PUT_1BYTE (pu1MsgElemValue,
                                          DOT11AC_INFO_VHT_CAP_MSG_LEN);
                        u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT1));
#ifdef WLC_WANTED
                        CAPWAP_PUT_4BYTE (pu1MsgElemValue,
                                          pRadioIfGetDB->RadioIfGetAllDB.
                                          Dot11AcCapaParams.u4VhtCapInfo);
                        u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT4));

                        CAPWAP_PUT_NBYTE (pu1MsgElemValue,
                                          pRadioIfGetDB->RadioIfGetAllDB.
                                          Dot11AcCapaParams.au1VhtCapaMcs,
                                          DOT11AC_VHT_CAP_MCS_LEN);
#else
                        CAPWAP_PUT_4BYTE (pu1MsgElemValue,
                                          pRadioIfGetDB->RadioIfGetAllDB.
                                          Dot11AcCapaParams.u4SuppVhtCapInfo);
                        u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT4));
                        CAPWAP_PUT_NBYTE (pu1MsgElemValue,
                                          pRadioIfGetDB->RadioIfGetAllDB.
                                          Dot11AcCapaParams.au1SuppVhtCapaMcs,
                                          DOT11AC_VHT_CAP_MCS_LEN);
#endif
                        u2ElemLength = (UINT2) (u2ElemLength +
                                                DOT11AC_VHT_CAP_MCS_LEN);

                        *pu2MsgElemLen = u2ElemLength;
                    }
                    break;

                case DOT11AC_INFO_ELEM_VHTOPERATION:

                    if ((pRadioIfGetDB->RadioIfGetAllDB.u4Dot11RadioType) ==
                        RADIO_TYPE_AC)
                    {
                        u1InfoFlag = RADIO_VALUE_1;
                        CAPWAP_PUT_1BYTE (pu1MsgElemValue,
                                          DOT11AC_INFO_ELEM_VHTOPERATION);
                        u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT1));
                        CAPWAP_PUT_1BYTE (pu1MsgElemValue,
                                          DOT11AC_INFO_VHT_OPER_MSG_LEN);
                        u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT1));
                        CAPWAP_PUT_NBYTE (pu1MsgElemValue,
                                          pRadioIfGetDB->RadioIfGetAllDB.
                                          Dot11AcOperParams.au1VhtOperInfo,
                                          DOT11AC_VHT_OPER_INFO_LEN);
                        u2ElemLength =
                            (UINT2) (u2ElemLength + DOT11AC_VHT_OPER_INFO_LEN);
                        CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                                          pRadioIfGetDB->RadioIfGetAllDB.
                                          Dot11AcOperParams.u2VhtOperMcsSet);
                        u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT2));

                        *pu2MsgElemLen = u2ElemLength;
                    }
                    break;

                default:
                    break;
            }
            if (u1InfoFlag == RADIO_VALUE_0)
            {
                u2ElemLength = (UINT2) (u2ElemLength - RADIO_VALUE_3);
                *pu2MsgElemLen = u2ElemLength;
            }
            break;

        case IEEE_MAC_OPERATION:
            CAPWAP_PUT_1BYTE (pu1MsgElemValue, u1RadioID);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT1));
            CAPWAP_PUT_1BYTE (pu1MsgElemValue, 0);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT1));
            CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                              pRadioIfGetDB->RadioIfGetAllDB.u2RTSThreshold);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT2));
            CAPWAP_PUT_1BYTE (pu1MsgElemValue,
                              pRadioIfGetDB->RadioIfGetAllDB.u1ShortRetry);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT1));
            CAPWAP_PUT_1BYTE (pu1MsgElemValue,
                              pRadioIfGetDB->RadioIfGetAllDB.u1LongRetry);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT1));
            CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                              pRadioIfGetDB->
                              RadioIfGetAllDB.u2FragmentationThreshold);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT2));
            CAPWAP_PUT_4BYTE (pu1MsgElemValue,
                              pRadioIfGetDB->RadioIfGetAllDB.u4RxMsduLifetime);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT4));
            CAPWAP_PUT_4BYTE (pu1MsgElemValue,
                              pRadioIfGetDB->RadioIfGetAllDB.u4TxMsduLifetime);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT4));

            CAPWAP_TRC5 (CAPWAP_RADIO_PARM_TRC,
                         "\n* IEEE_MAC_OPERATION : \nu1RadioID %d\n RTSThreshold %d\n ShortRetry %d\n LongRetry %d\n FragmentationThreshold %d\n",
                         u1RadioID,
                         pRadioIfGetDB->RadioIfGetAllDB.u2RTSThreshold,
                         pRadioIfGetDB->RadioIfGetAllDB.u1ShortRetry,
                         pRadioIfGetDB->RadioIfGetAllDB.u1LongRetry,
                         pRadioIfGetDB->
                         RadioIfGetAllDB.u2FragmentationThreshold);

            CAPWAP_TRC2 (CAPWAP_RADIO_PARM_TRC,
                         "RxMsduLifetime %d\n TxMsduLifetime %d\n",
                         pRadioIfGetDB->RadioIfGetAllDB.u4RxMsduLifetime,
                         pRadioIfGetDB->RadioIfGetAllDB.u4TxMsduLifetime);

            *pu2MsgElemLen = u2ElemLength;
            break;
        case IEEE_MULTIDOMAIN_CAPABILITY:
            CAPWAP_PUT_1BYTE (pu1MsgElemValue, u1RadioID);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT1));
            CAPWAP_PUT_1BYTE (pu1MsgElemValue, 0);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT1));
            CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                              pRadioIfGetDB->
                              RadioIfGetAllDB.
                              MultiDomainInfo[u1QosIndex].u2FirstChannelNo);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT2));
            CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                              pRadioIfGetDB->RadioIfGetAllDB.
                              MultiDomainInfo[u1QosIndex].u2NoOfChannels);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT2));
            CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                              pRadioIfGetDB->RadioIfGetAllDB.
                              MultiDomainInfo[u1QosIndex].u2MaxTxPowerLevel);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT2));

            CAPWAP_TRC4 (CAPWAP_RADIO_PARM_TRC,
                         "\n* IEEE_MULTIDOMAIN_CAPABILITY : \nu1RadioID %d\n \
                    FirstChannelNumber %d\n NoOfChannels %d\n \
                    MaxTxPowerLevel %d\n", u1RadioID, pRadioIfGetDB->RadioIfGetAllDB.MultiDomainInfo[u1QosIndex].u2FirstChannelNo, pRadioIfGetDB->RadioIfGetAllDB.MultiDomainInfo[u1QosIndex].u2NoOfChannels, pRadioIfGetDB->RadioIfGetAllDB.MultiDomainInfo[u1QosIndex].u2MaxTxPowerLevel);
            *pu2MsgElemLen = u2ElemLength;
            break;

        case IEEE_OFDM_CONTROL:
            CAPWAP_PUT_1BYTE (pu1MsgElemValue, u1RadioID);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT1));
            CAPWAP_PUT_1BYTE (pu1MsgElemValue, 0);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT1));
            CAPWAP_PUT_1BYTE (pu1MsgElemValue,
                              pRadioIfGetDB->RadioIfGetAllDB.u1CurrentChannel);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT1));
            CAPWAP_PUT_1BYTE (pu1MsgElemValue,
                              pRadioIfGetDB->RadioIfGetAllDB.u1SupportedBand);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT1));
            CAPWAP_PUT_4BYTE (pu1MsgElemValue,
                              pRadioIfGetDB->RadioIfGetAllDB.u4T1Threshold);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT4));

            CAPWAP_TRC4 (CAPWAP_RADIO_PARM_TRC,
                         "\n* IEEE_OFDM_CONTROL : \nu1RadioID %d\n CurrentChannel %d\n SupportedBand %d\n T1Threshold %d\n",
                         u1RadioID,
                         pRadioIfGetDB->RadioIfGetAllDB.u1CurrentChannel,
                         pRadioIfGetDB->RadioIfGetAllDB.u1SupportedBand,
                         pRadioIfGetDB->RadioIfGetAllDB.u4T1Threshold);

            *pu2MsgElemLen = u2ElemLength;
            break;

        case IEEE_SUPPORTED_RATES:
            CAPWAP_PUT_1BYTE (pu1MsgElemValue, u1RadioID);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT1));
            CAPWAP_PUT_NBYTE (pu1MsgElemValue,
                              pRadioIfGetDB->RadioIfGetAllDB.au1SupportedRate,
                              sizeof (pRadioIfGetDB->
                                      RadioIfGetAllDB.au1SupportedRate));
            u2ElemLength =
                (UINT2) (u2ElemLength +
                         sizeof (pRadioIfGetDB->RadioIfGetAllDB.
                                 au1SupportedRate));
            CAPWAP_TRC1 (CAPWAP_RADIO_PARM_TRC,
                         "\n* IEEE_SUPPORTED_RATES : \nu1RadioID %d\n",
                         u1RadioID);
            SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4CapwapSysLogId,
                          "\n* IEEE_SUPPORTED_RATES : \nu1RadioID %d\n",
                          u1RadioID));
            pu1TempPrint = pRadioIfGetDB->RadioIfGetAllDB.au1SupportedRate;
            CAPWAP_TRC (CAPWAP_RADIO_PARM_TRC, "SupportedRate\n");
            for (u1Index = 1;
                 u1Index <=
                 sizeof (pRadioIfGetDB->RadioIfGetAllDB.au1SupportedRate);
                 u1Index++)
            {
                CAPWAP_TRC1 (CAPWAP_RADIO_PARM_TRC, "%d ", *pu1TempPrint);
                SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4CapwapSysLogId, "%d ",
                              *pu1TempPrint));
                pu1TempPrint++;
            }

            *pu2MsgElemLen = u2ElemLength;
            break;

        case IEEE_TX_POWER:
            CAPWAP_PUT_1BYTE (pu1MsgElemValue, u1RadioID);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT1));
            CAPWAP_PUT_1BYTE (pu1MsgElemValue, 0);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT1));
            CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                              pRadioIfGetDB->RadioIfGetAllDB.i2CurrentTxPower);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT2));
            CAPWAP_TRC2 (CAPWAP_RADIO_PARM_TRC,
                         "\n* IEEE_TX_POWER : \nu1RadioID %d\n CurrentTxPowerLevel %d\n",
                         u1RadioID,
                         pRadioIfGetDB->RadioIfGetAllDB.i2CurrentTxPower);
            *pu2MsgElemLen = u2ElemLength;
            break;

        case IEEE_TX_POWERLEVEL:
            CAPWAP_PUT_1BYTE (pu1MsgElemValue, u1RadioID);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT1));
            CAPWAP_PUT_1BYTE (pu1MsgElemValue,
                              pRadioIfGetDB->
                              RadioIfGetAllDB.u1NoOfSupportedPowerLevels);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT1));
            for (u1Temp = 0; u1Temp < RADIOIF_MAX_POWER_LEVEL; u1Temp++)
            {
                CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                                  pRadioIfGetDB->
                                  RadioIfGetAllDB.au2TxPowerLevel[u1Temp]);
                CAPWAP_TRC2 (CAPWAP_RADIO_PARM_TRC,
                             "\n* IEEE_TX_POWERLEVEL : \n TxPowerLevel %d %d\n",
                             u1Temp,
                             pRadioIfGetDB->
                             RadioIfGetAllDB.au2TxPowerLevel[u1Temp]);
            }
            u2ElemLength = (UINT2) (u2ElemLength +
                                    RADIOIF_MAX_POWER_LEVEL * sizeof (UINT2));
            CAPWAP_TRC2 (CAPWAP_RADIO_PARM_TRC,
                         "u1RadioID %d\n NoOfSupportedPowerLevels %d\n",
                         u1RadioID,
                         pRadioIfGetDB->
                         RadioIfGetAllDB.u1NoOfSupportedPowerLevels);

            *pu2MsgElemLen = u2ElemLength;
            break;

        case IEEE_WTP_RADIO_CONFIGURATION:
            CAPWAP_PUT_1BYTE (pu1MsgElemValue, u1RadioID);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT1));
            CAPWAP_PUT_1BYTE (pu1MsgElemValue,
                              pRadioIfGetDB->RadioIfGetAllDB.u1ShortPreamble);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT1));
            CAPWAP_PUT_1BYTE (pu1MsgElemValue,
                              pRadioIfGetDB->RadioIfGetAllDB.u1NoOfBssId);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT1));
            CAPWAP_PUT_1BYTE (pu1MsgElemValue,
                              pRadioIfGetDB->RadioIfGetAllDB.u1DTIMPeriod);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT1));
            CAPWAP_PUT_NBYTE (pu1MsgElemValue,
                              pRadioIfGetDB->RadioIfGetAllDB.MacAddr,
                              RADIO_MAC_LEN);
            u2ElemLength = (UINT2) (u2ElemLength + RADIO_MAC_LEN);

            CAPWAP_TRC (CAPWAP_RADIO_PARM_TRC, "MacAddr\n");
            pu1TempPrint = pRadioIfGetDB->RadioIfGetAllDB.MacAddr;
            for (u1Index = 1; u1Index <= 6; u1Index++)
            {
                CAPWAP_TRC1 (CAPWAP_RADIO_PARM_TRC, "%d ", *pu1TempPrint);
                SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4CapwapSysLogId, "%d ",
                              *pu1TempPrint));
                pu1TempPrint++;
            }

            CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                              pRadioIfGetDB->RadioIfGetAllDB.u2BeaconPeriod);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT2));
            CAPWAP_PUT_NBYTE (pu1MsgElemValue,
                              pRadioIfGetDB->RadioIfGetAllDB.au1CountryString,
                              RADIO_COUNTRY_LEN);
            u2ElemLength = (UINT2) (u2ElemLength + RADIO_COUNTRY_LEN);
            CAPWAP_PUT_1BYTE (pu1MsgElemValue, 0);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT1));

            CAPWAP_TRC5 (CAPWAP_RADIO_PARM_TRC,
                         "\n* IEEE_WTP_RADIO_CONFIGURATION : \nu1RadioID %d\n ShortPreamble %d\n NoOfBssId %d\n DTIMPeriod %d\n BeaconPeriod %d\n",
                         u1RadioID,
                         pRadioIfGetDB->RadioIfGetAllDB.u1ShortPreamble,
                         pRadioIfGetDB->RadioIfGetAllDB.u1NoOfBssId,
                         pRadioIfGetDB->RadioIfGetAllDB.u1DTIMPeriod,
                         pRadioIfGetDB->RadioIfGetAllDB.u2BeaconPeriod);

            CAPWAP_TRC1 (CAPWAP_RADIO_PARM_TRC, "CountryString %s\n",
                         pRadioIfGetDB->RadioIfGetAllDB.au1CountryString);
            SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4CapwapSysLogId,
                          "CountryString %s\n",
                          pRadioIfGetDB->RadioIfGetAllDB.au1CountryString));

            *pu2MsgElemLen = u2ElemLength;
            break;

        case IEEE_RATE_SET:
            CAPWAP_PUT_1BYTE (pu1MsgElemValue, u1RadioID);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT1));
            CAPWAP_PUT_NBYTE (pu1MsgElemValue,
                              pRadioIfGetDB->RadioIfGetAllDB.au1OperationalRate,
                              RADIO_OPER_RATE_LEN);
            u2ElemLength = (UINT2) (u2ElemLength + RADIO_OPER_RATE_LEN);

            CAPWAP_TRC (CAPWAP_RADIO_PARM_TRC, "Oper Rate\n");
            SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4CapwapSysLogId,
                          "Oper Rate\n"));
            pu1TempPrint = pRadioIfGetDB->RadioIfGetAllDB.au1OperationalRate;
            for (u1Index = 1; u1Index <= 8; u1Index++)
            {
                CAPWAP_TRC1 (CAPWAP_RADIO_PARM_TRC, "%d ", *pu1TempPrint);
                SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4CapwapSysLogId, "%d ",
                              *pu1TempPrint));

                pu1TempPrint++;
            }

            CAPWAP_TRC1 (CAPWAP_RADIO_PARM_TRC,
                         "\n* IEEE_RATE_SET : \nu1RadioID %d\n", u1RadioID);
            SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4CapwapSysLogId, "%d ",
                          "\n* IEEE_RATE_SET : \nu1RadioID %d\n", u1RadioID));

            *pu2MsgElemLen = u2ElemLength;
            break;

        case IEEE_WTP_QOS:
            u1ArrIndex = (UINT1) (u1QosIndex - 1);
            if (u1ArrIndex == 0)
            {
                CAPWAP_PUT_1BYTE (pu1MsgElemValue, u1RadioID);
                u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT1));
                CAPWAP_PUT_1BYTE (pu1MsgElemValue,
                                  pRadioIfGetDB->
                                  RadioIfGetAllDB.u1TaggingPolicy);
                u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT1));
                CAPWAP_TRC2 (CAPWAP_RADIO_PARM_TRC,
                             "\n* IEEE_WTP_QOS : \nu1RadioID %d\n TaggingPolicy %d\n",
                             u1RadioID,
                             pRadioIfGetDB->RadioIfGetAllDB.u1TaggingPolicy);
                SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4CapwapSysLogId,
                              "\n* IEEE_WTP_QOS : \nu1RadioID %d\n TaggingPolicy %d\n",
                              u1RadioID,
                              pRadioIfGetDB->RadioIfGetAllDB.u1TaggingPolicy));
            }
            CAPWAP_PUT_1BYTE (pu1MsgElemValue,
                              pRadioIfGetDB->
                              RadioIfGetAllDB.au1QueueDepth[u1ArrIndex]);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT1));
            CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                              pRadioIfGetDB->
                              RadioIfGetAllDB.au2CwMin[u1ArrIndex]);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT2));
            CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                              pRadioIfGetDB->
                              RadioIfGetAllDB.au2CwMax[u1ArrIndex]);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT2));
            CAPWAP_PUT_1BYTE (pu1MsgElemValue,
                              pRadioIfGetDB->
                              RadioIfGetAllDB.au1Aifsn[u1ArrIndex]);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT1));
            CAPWAP_PUT_1BYTE (pu1MsgElemValue,
                              pRadioIfGetDB->
                              RadioIfGetAllDB.au1QosPrio[u1ArrIndex]);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT1));
            CAPWAP_PUT_1BYTE (pu1MsgElemValue,
                              pRadioIfGetDB->
                              RadioIfGetAllDB.au1Dscp[u1ArrIndex]);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT1));

            CAPWAP_TRC5 (CAPWAP_RADIO_PARM_TRC,
                         "u1RadioID %d\n QueueDepth %d\n CwMin %d\n CwMax %d\n Aifsn %d\n",
                         u1RadioID,
                         pRadioIfGetDB->
                         RadioIfGetAllDB.au1QueueDepth[u1ArrIndex],
                         pRadioIfGetDB->RadioIfGetAllDB.au2CwMin[u1ArrIndex],
                         pRadioIfGetDB->RadioIfGetAllDB.au2CwMax[u1ArrIndex],
                         pRadioIfGetDB->RadioIfGetAllDB.au1Aifsn[u1ArrIndex]);
            SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4CapwapSysLogId,
                          "u1RadioID %d\n QueueDepth %d\n CwMin %d\n CwMax %d\n Aifsn %d\n",
                          u1RadioID,
                          pRadioIfGetDB->
                          RadioIfGetAllDB.au1QueueDepth[u1ArrIndex],
                          pRadioIfGetDB->RadioIfGetAllDB.au2CwMin[u1ArrIndex],
                          pRadioIfGetDB->RadioIfGetAllDB.au2CwMax[u1ArrIndex],
                          pRadioIfGetDB->RadioIfGetAllDB.au1Aifsn[u1ArrIndex]));

            CAPWAP_TRC2 (CAPWAP_RADIO_PARM_TRC, "QosPrio %d\n Dscp %d\n",
                         pRadioIfGetDB->RadioIfGetAllDB.au1QosPrio[u1ArrIndex],
                         pRadioIfGetDB->RadioIfGetAllDB.au1Dscp[u1ArrIndex]);
            SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4CapwapSysLogId,
                          "QosPrio %d\n Dscp %d\n",
                          pRadioIfGetDB->RadioIfGetAllDB.au1QosPrio[u1ArrIndex],
                          pRadioIfGetDB->RadioIfGetAllDB.au1Dscp[u1ArrIndex]));

            *pu2MsgElemLen = u2ElemLength;
            break;

        case RADIO_OPER_STATE:
            CAPWAP_PUT_1BYTE (pu1MsgElemValue, u1RadioID);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT1));
            CAPWAP_PUT_1BYTE (pu1MsgElemValue,
                              pRadioIfGetDB->RadioIfGetAllDB.u1OperStatus);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT1));
            CAPWAP_PUT_1BYTE (pu1MsgElemValue,
                              pRadioIfGetDB->RadioIfGetAllDB.u1FailureStatus);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT1));
            CAPWAP_TRC3 (CAPWAP_RADIO_PARM_TRC,
                         "\n* RADIO_OPER_STATE : \nu1RadioID %d\n OperStatus %d\n FailureStatus %d\n",
                         u1RadioID, pRadioIfGetDB->RadioIfGetAllDB.u1OperStatus,
                         pRadioIfGetDB->RadioIfGetAllDB.u1FailureStatus);
            SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4CapwapSysLogId,
                          "\n* RADIO_OPER_STATE : \nu1RadioID %d\n OperStatus %d\n FailureStatus %d\n",
                          u1RadioID,
                          pRadioIfGetDB->RadioIfGetAllDB.u1OperStatus,
                          pRadioIfGetDB->RadioIfGetAllDB.u1FailureStatus));
            *pu2MsgElemLen = u2ElemLength;
            break;

        case WTP_RADIO_STATISTICS:
            CAPWAP_PUT_1BYTE (pu1MsgElemValue, u1RadioID);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT1));
            CAPWAP_PUT_1BYTE (pu1MsgElemValue,
                              pRadioIfGetDB->RadioIfGetAllDB.
                              radioStats.u1LastFailureType);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT1));
            CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                              pRadioIfGetDB->RadioIfGetAllDB.
                              radioStats.u2ResetCount);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT2));
            CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                              pRadioIfGetDB->RadioIfGetAllDB.
                              radioStats.u2SwFailCount);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT2));
            CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                              pRadioIfGetDB->RadioIfGetAllDB.
                              radioStats.u2HwFailCount);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT2));
            CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                              pRadioIfGetDB->RadioIfGetAllDB.
                              radioStats.u2OtherFailureCount);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT2));
            CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                              pRadioIfGetDB->RadioIfGetAllDB.
                              radioStats.u2UnknownFailureCount);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT2));
            CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                              pRadioIfGetDB->RadioIfGetAllDB.
                              radioStats.u2ConfigUpdateCount);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT2));
            CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                              pRadioIfGetDB->RadioIfGetAllDB.
                              radioStats.u2ChannelChangeCount);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT2));
            CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                              pRadioIfGetDB->RadioIfGetAllDB.
                              radioStats.u2BandChangeCount);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT2));
            CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                              pRadioIfGetDB->RadioIfGetAllDB.
                              radioStats.u2CurrentNoiseFloor);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT2));
            *pu2MsgElemLen = u2ElemLength;
            break;
        case IEEE_STATISTICS:
            CAPWAP_PUT_1BYTE (pu1MsgElemValue, u1RadioID);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT1));
            /* Reserved 3 bytes as zero, Refer RFC 5416 */
            CAPWAP_PUT_2BYTE (pu1MsgElemValue, 0);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT2));
            CAPWAP_PUT_1BYTE (pu1MsgElemValue, 0);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT1));
            CAPWAP_PUT_4BYTE (pu1MsgElemValue,
                              pRadioIfGetDB->RadioIfGetAllDB.
                              ieee80211Stats.u4TxFragmentCnt);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT4));
            CAPWAP_PUT_4BYTE (pu1MsgElemValue,
                              pRadioIfGetDB->RadioIfGetAllDB.
                              ieee80211Stats.u4MulticastTxCnt);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT4));
            CAPWAP_PUT_4BYTE (pu1MsgElemValue,
                              pRadioIfGetDB->RadioIfGetAllDB.
                              ieee80211Stats.u4FailedCount);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT4));
            CAPWAP_PUT_4BYTE (pu1MsgElemValue,
                              pRadioIfGetDB->RadioIfGetAllDB.
                              ieee80211Stats.u4RetryCount);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT4));
            CAPWAP_PUT_4BYTE (pu1MsgElemValue,
                              pRadioIfGetDB->RadioIfGetAllDB.
                              ieee80211Stats.u4MultipleRetryCount);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT4));
            CAPWAP_PUT_4BYTE (pu1MsgElemValue,
                              pRadioIfGetDB->RadioIfGetAllDB.
                              ieee80211Stats.u4FrameDupCount);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT4));
            CAPWAP_PUT_4BYTE (pu1MsgElemValue,
                              pRadioIfGetDB->RadioIfGetAllDB.
                              ieee80211Stats.u4RTSSuccessCount);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT4));
            CAPWAP_PUT_4BYTE (pu1MsgElemValue,
                              pRadioIfGetDB->RadioIfGetAllDB.
                              ieee80211Stats.u4RTSFailCount);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT4));
            CAPWAP_PUT_4BYTE (pu1MsgElemValue,
                              pRadioIfGetDB->RadioIfGetAllDB.
                              ieee80211Stats.u4ACKFailCount);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT4));
            CAPWAP_PUT_4BYTE (pu1MsgElemValue,
                              pRadioIfGetDB->RadioIfGetAllDB.
                              ieee80211Stats.u4RxFragmentCount);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT4));
            CAPWAP_PUT_4BYTE (pu1MsgElemValue,
                              pRadioIfGetDB->RadioIfGetAllDB.
                              ieee80211Stats.u4MulticastRxCount);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT4));
            CAPWAP_PUT_4BYTE (pu1MsgElemValue,
                              pRadioIfGetDB->RadioIfGetAllDB.
                              ieee80211Stats.u4FCSErrCount);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT4));
            CAPWAP_PUT_4BYTE (pu1MsgElemValue,
                              pRadioIfGetDB->RadioIfGetAllDB.
                              ieee80211Stats.u4TxFrameCount);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT4));
            CAPWAP_PUT_4BYTE (pu1MsgElemValue,
                              pRadioIfGetDB->RadioIfGetAllDB.
                              ieee80211Stats.u4DecryptionErr);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT4));
            CAPWAP_PUT_4BYTE (pu1MsgElemValue,
                              pRadioIfGetDB->RadioIfGetAllDB.
                              ieee80211Stats.u4DiscardQosFragmentCnt);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT4));
            CAPWAP_PUT_4BYTE (pu1MsgElemValue,
                              pRadioIfGetDB->RadioIfGetAllDB.
                              ieee80211Stats.u4AssociatedStaCount);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT4));
            CAPWAP_PUT_4BYTE (pu1MsgElemValue,
                              pRadioIfGetDB->RadioIfGetAllDB.
                              ieee80211Stats.u4QosCFPollsRecvdCount);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT4));
            CAPWAP_PUT_4BYTE (pu1MsgElemValue,
                              pRadioIfGetDB->RadioIfGetAllDB.
                              ieee80211Stats.u4QosCFPollsUnusedCount);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT4));
            CAPWAP_PUT_4BYTE (pu1MsgElemValue,
                              pRadioIfGetDB->RadioIfGetAllDB.
                              ieee80211Stats.u4QosCFPollsUnusableCount);
            u2ElemLength = (UINT2) (u2ElemLength + sizeof (UINT4));
            *pu2MsgElemLen = u2ElemLength;
            break;

#ifdef WTP_WANTED
        case WTP_REBOOT_STATISTICS:

            /* Get WTP Reboot Stats from APHdlr */
            MEMSET (&WssIfApHdlrDB, 0, sizeof (tWssIfApHdlrDB));
            if (WssIfProcessApHdlrDBMsg (WSS_APHDLR_GET_WTPREBOOT_STATS,
                                         &WssIfApHdlrDB) == OSIX_SUCCESS)
            {
                ApHdlrMsg.PmWtpEventReq.wtpRebootstatsElement.u2MsgEleType =
                    WTP_REBOOT_STATISTICS;
                ApHdlrMsg.PmWtpEventReq.wtpRebootstatsElement.u2RebootCount =
                    WssIfApHdlrDB.u2RebootCount;
                ApHdlrMsg.PmWtpEventReq.wtpRebootstatsElement.u2AcInitiatedCount
                    = WssIfApHdlrDB.u2AcInitiatedCount;
                ApHdlrMsg.PmWtpEventReq.wtpRebootstatsElement.u2LinkFailCount
                    = WssIfApHdlrDB.u2LinkFailCount;
                ApHdlrMsg.PmWtpEventReq.wtpRebootstatsElement.u2SwFailCount
                    = WssIfApHdlrDB.u2SwFailCount;
                ApHdlrMsg.PmWtpEventReq.wtpRebootstatsElement.u2HwFailCount
                    = WssIfApHdlrDB.u2HwFailCount;
                ApHdlrMsg.PmWtpEventReq.wtpRebootstatsElement.u2OtherFailCount
                    = WssIfApHdlrDB.u2OtherFailCount;
                ApHdlrMsg.PmWtpEventReq.wtpRebootstatsElement.u2UnknownFailCount
                    = WssIfApHdlrDB.u2UnknownFailCount;
                ApHdlrMsg.PmWtpEventReq.wtpRebootstatsElement.u1LastFailureType
                    = (UINT1) WssIfApHdlrDB.u1LastFailureType;
                ApHdlrMsg.PmWtpEventReq.wtpRebootstatsElement.u2MsgEleType
                    = CAPWAP_WTP_REBOOT_STATS_MSG_TYPE;
                ApHdlrMsg.PmWtpEventReq.wtpRebootstatsElement.u2MsgEleLen
                    = CAPWAP_WTP_REBOOT_STATS_MSG_LEN;
                ApHdlrMsg.PmWtpEventReq.wtpRebootstatsElement.isOptional
                    = OSIX_TRUE;

                CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                                  ApHdlrMsg.PmWtpEventReq.wtpRebootstatsElement.
                                  u2RebootCount);
                CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                                  ApHdlrMsg.PmWtpEventReq.wtpRebootstatsElement.
                                  u2AcInitiatedCount);
                CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                                  ApHdlrMsg.PmWtpEventReq.wtpRebootstatsElement.
                                  u2LinkFailCount);
                CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                                  ApHdlrMsg.PmWtpEventReq.wtpRebootstatsElement.
                                  u2SwFailCount);
                CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                                  ApHdlrMsg.PmWtpEventReq.wtpRebootstatsElement.
                                  u2HwFailCount);
                CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                                  ApHdlrMsg.PmWtpEventReq.wtpRebootstatsElement.
                                  u2OtherFailCount);
                CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                                  ApHdlrMsg.PmWtpEventReq.wtpRebootstatsElement.
                                  u2UnknownFailCount);
                CAPWAP_PUT_1BYTE (pu1MsgElemValue,
                                  ApHdlrMsg.PmWtpEventReq.wtpRebootstatsElement.
                                  u1LastFailureType);
                *pu2MsgElemLen =
                    ApHdlrMsg.PmWtpEventReq.wtpRebootstatsElement.u2MsgEleLen;

            }
            else
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "CapwapFetchMsgElemRadioValue:-Failed to Get WTP \
                        Reboot Stats \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "CapwapFetchMsgElemRadioValue:-Failed to Get WTP \
                        Reboot Stats \r\n"));
            }

            break;
#endif
        case VENDOR_SPECIFIC_PAYLOAD:

            CapwapConfigNoOfRadio (pRadioIfGetDB->RadioIfGetAllDB.
                                   u2WtpInternalId, &u2NumRadioElems);
#ifdef WTP_WANTED
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_RSM, pWssIfCapwapDB) ==
                OSIX_FAILURE)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Failed to Get RSM Entry for WTP \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to Get RSM Entry for WTP \r\n"));
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
                return OSIX_FAILURE;
            }

            /*CAPWAP STATS MESSAGE ELEMENT */
            pSessEntry = pWssIfCapwapDB->pSessEntry;

            if (pWssIfCapwapDB->pSessEntry->eCurrentState != CAPWAP_CONFIGURE)
            {
#endif
                for (u1Radioid = u1RadioID; u1Radioid <= u2NumRadioElems;
                     u1Radioid++)
                {
                    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
                        (UINT4) (SYS_DEF_MAX_ENET_INTERFACES + u1Radioid);
                    RadioIfGetDB.RadioIfIsGetAllDB.bBssIdCount = OSIX_TRUE;

                    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                                  &RadioIfGetDB) !=
                        OSIX_SUCCESS)
                    {
                        /* If no BSSID is found for this RadioId then fetch the BSSID
                         * for the next RadioID*/
                        continue;
                    }

                    if (RadioIfGetDB.RadioIfGetAllDB.u1BssIdCount != 0)
                    {
                        for (u1Wlanid = WSSWLAN_START_WLANID_PER_RADIO;
                             u1Wlanid <= WSSWLAN_END_WLANID_PER_RADIO;
                             u1Wlanid++)
                        {
                            RadioIfGetDB.RadioIfGetAllDB.u1WlanId = u1Wlanid;

                            if (WssIfProcessRadioIfDBMsg
                                (WSS_GET_WLAN_BSS_IFINDEX_DB,
                                 &RadioIfGetDB) == OSIX_SUCCESS)
                            {
#ifdef WTP_WANTED

                                WssWlanDB.WssWlanAttributeDB.u1WlanId =
                                    u1Wlanid;
                                WssWlanDB.WssWlanAttributeDB.u1RadioId =
                                    u1Radioid;
                                WssWlanDB.WssWlanIsPresentDB.bWlanIfIndex =
                                    OSIX_TRUE;
                                WssWlanDB.WssWlanIsPresentDB.bBssId = OSIX_TRUE;
                                WssWlanDB.WssWlanAttributeDB.u4WlanIfIndex =
                                    RadioIfGetDB.RadioIfGetAllDB.u4BssIfIndex;
                                if (WssIfProcessWssWlanDBMsg
                                    (WSS_WLAN_GET_INTERFACE_ENTRY,
                                     &WssWlanDB) != OSIX_SUCCESS)
                                {
                                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                                "WTPEventRequest:Get WlanIfIndex Failed\r\n");
                                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL,
                                                  gu4CapwapSysLogId,
                                                  "WTPEventRequest:Get WlanIfIndex Failed\r\n"));
                                    continue;
                                }

                                MEMCPY (&ApHdlrMsg.PmWtpEventReq.
                                        MacHdlrStatsElement.BssId[u1NoofBss],
                                        &WssWlanDB.WssWlanAttributeDB.BssId,
                                        MAC_ADDR_LEN);

                                u1WlanPerRadio[u1Radioid - 1] += 1;
#endif
                                /* Increment the BSS count. This count will be used for
                                 * scanning of neighbor message */
                                u1NoofBss++;

                                if (RadioIfGetDB.RadioIfGetAllDB.u1BssIdCount ==
                                    u1NoofBss)
                                {
                                    break;
                                }
                            }
                        }
                    }
                }
#ifdef WTP_WANTED
            }
#endif
            if (u1NoofBss != 0)
            {

#ifdef WTP_WANTED
                switch (u1MsgSubType)
                {
#ifdef RFMGMT_WANTED
                    case VENDOR_NEIGHBOR_AP_TYPE:

                        WssMsgStruct.unRfMgmtMsg.RfMgmtIntfConfigReq.
                            u4RadioIfIndex =
                            (UINT4) (SYS_DEF_MAX_ENET_INTERFACES + u1RadioID);
                        WSS_IF_DB_NEIGHBOR_AP_SCAN_ALLOC (WssMsgStruct.
                                                          unRfMgmtMsg.
                                                          RfMgmtIntfConfigReq.
                                                          VendorNeighborAp.
                                                          ScanChannel);

                        if (WssMsgStruct.unRfMgmtMsg.RfMgmtIntfConfigReq.
                            VendorNeighborAp.ScanChannel == NULL)
                        {
                            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                            UtlShMemFreeWssWlanBuf ((UINT1 *)
                                                    pWssWlanMsgStruct);
                            *pu2MsgElemLen = 0;
                            return OSIX_FAILURE;
                        }

                        if (WssIfProcessRfMgmtMsg
                            (RFMGMT_VENDOR_NEIGHBOR_AP_TYPE,
                             &WssMsgStruct) != RFMGMT_SUCCESS)
                        {
                            *pu2MsgElemLen = 0;
                            WSS_IF_DB_NEIGHBOR_AP_SCAN_RELEASE (WssMsgStruct.
                                                                unRfMgmtMsg.
                                                                RfMgmtIntfConfigReq.
                                                                VendorNeighborAp.
                                                                ScanChannel);
                            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                            UtlShMemFreeWssWlanBuf ((UINT1 *)
                                                    pWssWlanMsgStruct);

                            return OSIX_SUCCESS;
                        }
                        else
                        {
                            ApHdlrMsg.PmWtpEventReq.vendSpec.isOptional =
                                OSIX_TRUE;
                            ApHdlrMsg.PmWtpEventReq.vendSpec.u2MsgEleType =
                                VENDOR_SPECIFIC_PAYLOAD;
                            ApHdlrMsg.PmWtpEventReq.vendSpec.elementId =
                                VENDOR_NEIGHBOR_AP_TYPE;

                            /* Copy the neighbor AP info to the AP HDLR
                             * structure */
                            MEMCPY (&ApHdlrMsg.PmWtpEventReq.
                                    vendSpec.unVendorSpec.VendNeighAP,
                                    &WssMsgStruct.
                                    unRfMgmtMsg.RfMgmtIntfConfigReq.
                                    VendorNeighborAp,
                                    sizeof (tVendorNeighborAp));
                            ApHdlrMsg.PmWtpEventReq.vendSpec.u2MsgEleLen =
                                (UINT2) (ApHdlrMsg.PmWtpEventReq.vendSpec.
                                         unVendorSpec.VendNeighAP.u2MsgEleLen +
                                         CAPWAP_MSG_ELEM_TYPE_LEN);
                            ApHdlrMsg.PmWtpEventReq.vendSpec.unVendorSpec.
                                VendNeighAP.isOptional = OSIX_TRUE;

                            CapwapAssembleVendSpecNeighAP (pu1MsgElemValue,
                                                           &(ApHdlrMsg.
                                                             PmWtpEventReq.
                                                             vendSpec));

                            *pu2MsgElemLen =
                                ApHdlrMsg.PmWtpEventReq.vendSpec.u2MsgEleLen;

                            WSS_IF_DB_NEIGHBOR_AP_SCAN_RELEASE (WssMsgStruct.
                                                                unRfMgmtMsg.
                                                                RfMgmtIntfConfigReq.
                                                                VendorNeighborAp.
                                                                ScanChannel);

                        }
                        break;
                    case VENDOR_CLIENT_SCAN_TYPE:

                        WssMsgStruct.unRfMgmtMsg.RfMgmtIntfConfigReq.
                            u4RadioIfIndex =
                            (UINT4) (SYS_DEF_MAX_ENET_INTERFACES + u1RadioID);
                        if (WssIfProcessRfMgmtMsg
                            (RFMGMT_VENDOR_CLIENT_SCAN_TYPE,
                             &WssMsgStruct) != RFMGMT_SUCCESS)

                        {
                            UtlShMemFreeWssWlanBuf ((UINT1 *)
                                                    pWssWlanMsgStruct);
                            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);

                            *pu2MsgElemLen = 0;
                            return OSIX_SUCCESS;
                        }
                        else
                        {
                            MEMSET (&ApHdlrMsg, 0, sizeof (unApHdlrMsgStruct));
                            ApHdlrMsg.PmWtpEventReq.vendSpec.isOptional
                                = OSIX_TRUE;
                            ApHdlrMsg.PmWtpEventReq.vendSpec.u2MsgEleType =
                                VENDOR_SPECIFIC_PAYLOAD;
                            ApHdlrMsg.PmWtpEventReq.vendSpec.elementId =
                                VENDOR_CLIENT_SCAN_TYPE;
                            ApHdlrMsg.PmWtpEventReq.vendSpec.unVendorSpec.
                                VendClientScan.u4VendorId = VENDOR_ID;
                            ApHdlrMsg.PmWtpEventReq.vendSpec.unVendorSpec.
                                VendClientScan.isOptional = OSIX_TRUE;

                            /* Copy the neighbor AP info to the AP HDLR
                             * structure */
                            ApHdlrMsg.PmWtpEventReq.vendSpec.unVendorSpec.
                                VendClientScan.u1RadioId = u1RadioID;
                            MEMCPY (&ApHdlrMsg.PmWtpEventReq.
                                    vendSpec.unVendorSpec.VendClientScan,
                                    &WssMsgStruct.
                                    unRfMgmtMsg.RfMgmtIntfConfigReq.
                                    VendorClientScan,
                                    sizeof (tVendorClientScan));
                            ApHdlrMsg.PmWtpEventReq.vendSpec.u2MsgEleLen =
                                (UINT2) (ApHdlrMsg.PmWtpEventReq.vendSpec.
                                         unVendorSpec.VendClientScan.
                                         u2MsgEleLen +
                                         CAPWAP_MSG_ELEM_TYPE_LEN);

                            ApHdlrMsg.PmWtpEventReq.vendSpec.
                                unVendorSpec.VendClientScan.isOptional =
                                OSIX_TRUE;
                            CapwapAssembleVendSpecClientScan (pu1MsgElemValue,
                                                              &(ApHdlrMsg.
                                                                PmWtpEventReq.
                                                                vendSpec));
                            *pu2MsgElemLen =
                                (UINT2) (ApHdlrMsg.PmWtpEventReq.vendSpec.
                                         unVendorSpec.VendClientScan.
                                         u2MsgEleLen +
                                         CAPWAP_MSG_ELEM_TYPE_LEN);
                        }

                        break;
#endif
                    case VENDOR_MULTIDOMAIN_INFO_TYPE:
                        break;
                    default:
                        /*Code to be implemented for WTP Event Request */
                        ApHdlrMsg.PmWtpEventReq.vendSpec.vendorId = 0x006e006e;
                        ApHdlrMsg.PmWtpEventReq.vendSpec.elementId =
                            VENDOR_SPECIFIC_MSG + VENDOR_BSSID_STATS_TYPE;
                        CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                                          ApHdlrMsg.PmWtpEventReq.vendSpec.
                                          elementId);
                        ApHdlrMsg.PmWtpEventReq.vendSpec.u2MsgEleLen =
                            (UINT2) (ApHdlrMsg.PmWtpEventReq.vendSpec.
                                     u2MsgEleLen + sizeof (UINT2));
                        CAPWAP_PUT_4BYTE (pu1MsgElemValue,
                                          ApHdlrMsg.PmWtpEventReq.vendSpec.
                                          vendorId);
                        ApHdlrMsg.PmWtpEventReq.vendSpec.u2MsgEleLen =
                            (UINT2) (ApHdlrMsg.PmWtpEventReq.vendSpec.
                                     u2MsgEleLen + sizeof (UINT4));

                        if (WssIfProcessCapwapDBMsg
                            (WSS_CAPWAP_GET_RSM,
                             pWssIfCapwapDB) == OSIX_FAILURE)
                        {
                            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                        "Failed to Get RSM Entry for WTP \r\n");
                            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                          "Failed to Get RSM Entry for WTP \r\n"));
                            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                            UtlShMemFreeWssWlanBuf ((UINT1 *)
                                                    pWssWlanMsgStruct);
                            return OSIX_FAILURE;
                        }

                        /*CAPWAP STATS MESSAGE ELEMENT */
                        pSessEntry = pWssIfCapwapDB->pSessEntry;
                        MEMCPY (&ApHdlrMsg.PmWtpEventReq.capwapStatsElement.
                                capwapStats, &pSessEntry->wtpCapwapStats,
                                sizeof (tWtpCPWPWtpStats));
                        CapwapGetDiscReqSentCount (&u2DiscReqSentCount);
                        CapwapGetDiscResponseCount (&u1DiscRespCount);
                        ApHdlrMsg.PmWtpEventReq.capwapStatsElement.capwapStats.
                            discReqSent = u2DiscReqSentCount;
                        ApHdlrMsg.PmWtpEventReq.capwapStatsElement.capwapStats.
                            discRespRxd = u1DiscRespCount;
                        ApHdlrMsg.PmWtpEventReq.capwapStatsElement.isOptional =
                            OSIX_TRUE;
                        ApHdlrMsg.PmWtpEventReq.capwapStatsElement.u2MsgEleLen =
                            CAPWAP_STATS_MSG_LEN;
                        CAPWAP_PUT_NBYTE (pu1MsgElemValue,
                                          ApHdlrMsg.
                                          PmWtpEventReq.capwapStatsElement.
                                          capwapStats,
                                          ApHdlrMsg.
                                          PmWtpEventReq.capwapStatsElement.
                                          u2MsgEleLen);
                        ApHdlrMsg.PmWtpEventReq.vendSpec.u2MsgEleLen =
                            (UINT2) (ApHdlrMsg.PmWtpEventReq.vendSpec.
                                     u2MsgEleLen +
                                     ApHdlrMsg.PmWtpEventReq.capwapStatsElement.
                                     u2MsgEleLen);

                        /*MAC HANDLER STATS MESSAGE ELEMENT */
                        ApHdlrMsg.PmWtpEventReq.MacHdlrStatsElement.
                            numBssCount = u1NoofBss;
                        CAPWAP_PUT_1BYTE (pu1MsgElemValue,
                                          ApHdlrMsg.
                                          PmWtpEventReq.MacHdlrStatsElement.
                                          numBssCount);
                        ApHdlrMsg.PmWtpEventReq.MacHdlrStatsElement.
                            u2MsgEleLen += sizeof (UINT1);
#ifdef NPAPI_WANTED

                        for (u1Radioid = u1RadioID;
                             u1Radioid <= SYS_DEF_MAX_RADIO_INTERFACES;
                             u1Radioid++)
                        {
                            RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
                                (UINT4) (SYS_DEF_MAX_ENET_INTERFACES +
                                         u1Radioid);
                            RadioIfGetDB.RadioIfIsGetAllDB.bBssIdCount =
                                OSIX_TRUE;

                            if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                                          &RadioIfGetDB) !=
                                OSIX_SUCCESS)
                            {
                                /* If no BSSID is found for this RadioId then fetch the BSSID
                                 * for the next RadioID*/
                                continue;
                            }

                            /* MAC HANDLER STATS MESSAGE ELEMENT */
                            for (u1Wlanid = WSSWLAN_START_WLANID_PER_RADIO;
                                 u1Wlanid <= WSSWLAN_END_WLANID_PER_RADIO;
                                 u1Wlanid++)
                            {
                                RadioIfGetDB.RadioIfGetAllDB.u1WlanId =
                                    u1Wlanid;

                                if (WssIfProcessRadioIfDBMsg
                                    (WSS_GET_WLAN_BSS_IFINDEX_DB,
                                     &RadioIfGetDB) == OSIX_SUCCESS)
                                {
                                    RadioIfGetDB.RadioIfGetAllDB.u1RadioId =
                                        u1Radioid;
                                    RadioIfGetDB.RadioIfIsGetAllDB.
                                        bRadioIfIndex = OSIX_TRUE;
                                    if ((WssIfProcessRadioIfDBMsg
                                         (WSS_GET_PHY_RADIO_IF_DB,
                                          &RadioIfGetDB)) != OSIX_SUCCESS)
                                    {
                                        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                                    "Failed to get Radio Operation State from"
                                                    "RadioIf \r\n");
                                        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL,
                                                      gu4CapwapSysLogId,
                                                      "Failed to get Radio Operation State from"
                                                      "RadioIf \r\n"));

                                        WSS_IF_DB_TEMP_MEM_RELEASE
                                            (pWssIfCapwapDB);
                                        UtlShMemFreeWssWlanBuf ((UINT1 *)
                                                                pWssWlanMsgStruct);
                                        return OSIX_FAILURE;
                                    }
                                    u1Count++;
                                    FsHwNp.RadioIfNpModInfo.unOpCode.
                                        sConfigGet80211Stats.RadInfo.
                                        u4RadioIfIndex =
                                        RadioIfGetDB.RadioIfGetAllDB.
                                        u4RadioIfIndex;
                                    SPRINTF ((CHR1 *) FsHwNp.RadioIfNpModInfo.
                                             unOpCode.sConfigGet80211Stats.
                                             RadInfo.au1RadioIfName, "%s%d",
                                             RADIO_INTF_NAME, u1Radioid - 1);
                                    FsHwNp.RadioIfNpModInfo.unOpCode.
                                        sConfigGet80211Stats.RadInfo.
                                        au2WlanIfIndex[0] = u1Wlanid;
                                    WssWlanGetWlanIntfName (u1Radioid, u1Wlanid,
                                                            au1WlanName,
                                                            &u1Wlanid);
                                    STRCPY (FsHwNp.RadioIfNpModInfo.unOpCode.
                                            sConfigGet80211Stats.RadInfo.
                                            au1WlanIfname[0], au1WlanName);
                                    FsHwNp.u4Opcode = FS_RADIO_HW_GET_RAD_STATS;
                                    RadioNpWrHwProgram (&FsHwNp);

                                    ApHdlrMsg.PmWtpEventReq.
                                        MacHdlrStatsElement.BSSIDMacHndlrStats
                                        [u1Index].wlanBSSIDBeaconsSentCount =
                                        FsHwNp.RadioIfNpModInfo.unOpCode.
                                        sConfigGet80211Stats.Stats.rx_beacon.
                                        u4Lo;
                                    ApHdlrMsg.PmWtpEventReq.
                                        MacHdlrStatsElement.BSSIDMacHndlrStats
                                        [u1Index].wlanBSSIDProbeReqRcvdCount =
                                        FsHwNp.RadioIfNpModInfo.unOpCode.
                                        sConfigGet80211Stats.Stats.u4ProbeReqRx;
                                    ApHdlrMsg.PmWtpEventReq.
                                        MacHdlrStatsElement.BSSIDMacHndlrStats
                                        [u1Index].wlanBSSIDProbeRespSentCount =
                                        FsHwNp.RadioIfNpModInfo.unOpCode.
                                        sConfigGet80211Stats.Stats.
                                        u4ProbeRespTx;
                                    ApHdlrMsg.PmWtpEventReq.
                                        MacHdlrStatsElement.BSSIDMacHndlrStats
                                        [u1Index].wlanBSSIDDataPktRcvdCount =
                                        FsHwNp.RadioIfNpModInfo.unOpCode.
                                        sConfigGet80211Stats.MacStats.
                                        rx_packets.u4Lo;
                                    ApHdlrMsg.PmWtpEventReq.
                                        MacHdlrStatsElement.BSSIDMacHndlrStats
                                        [u1Index].wlanBSSIDDataPktSentCount =
                                        FsHwNp.RadioIfNpModInfo.unOpCode.
                                        sConfigGet80211Stats.MacStats.
                                        tx_packets.u4Lo;
                                    ApHdlrMsg.PmWtpEventReq.MacHdlrStatsElement.
                                        isOptional = OSIX_TRUE;
                                    u1Index++;
                                    if (u1Count ==
                                        RadioIfGetDB.RadioIfGetAllDB.
                                        u1BssIdCount)
                                    {
                                        break;
                                    }
                                }
                            }
                        }

#endif
                        for (u1Index = 0;
                             u1Index <
                             ApHdlrMsg.PmWtpEventReq.MacHdlrStatsElement.
                             numBssCount; u1Index++)
                        {

                            CAPWAP_PUT_NBYTE (pu1MsgElemValue,
                                              ApHdlrMsg.
                                              PmWtpEventReq.MacHdlrStatsElement.
                                              BssId[u1Index], MAC_ADDR_LEN);
                            CAPWAP_PUT_4BYTE (pu1MsgElemValue,
                                              ApHdlrMsg.
                                              PmWtpEventReq.MacHdlrStatsElement.
                                              BSSIDMacHndlrStats
                                              [u1Index].
                                              wlanBSSIDBeaconsSentCount);

                            CAPWAP_PUT_4BYTE (pu1MsgElemValue,
                                              ApHdlrMsg.
                                              PmWtpEventReq.MacHdlrStatsElement.
                                              BSSIDMacHndlrStats
                                              [u1Index].
                                              wlanBSSIDProbeReqRcvdCount);

                            CAPWAP_PUT_4BYTE (pu1MsgElemValue,
                                              ApHdlrMsg.
                                              PmWtpEventReq.MacHdlrStatsElement.
                                              BSSIDMacHndlrStats
                                              [u1Index].
                                              wlanBSSIDProbeRespSentCount);

                            CAPWAP_PUT_4BYTE (pu1MsgElemValue,
                                              ApHdlrMsg.
                                              PmWtpEventReq.MacHdlrStatsElement.
                                              BSSIDMacHndlrStats
                                              [u1Index].
                                              wlanBSSIDDataPktRcvdCount);

                            CAPWAP_PUT_4BYTE (pu1MsgElemValue,
                                              ApHdlrMsg.
                                              PmWtpEventReq.MacHdlrStatsElement.
                                              BSSIDMacHndlrStats
                                              [u1Index].
                                              wlanBSSIDDataPktSentCount);

                            ApHdlrMsg.PmWtpEventReq.MacHdlrStatsElement.
                                u2MsgEleLen +=
                                CAPWAP_MAC_STATS_MSG_LEN + sizeof (tMacAddr);
                        }
                        ApHdlrMsg.PmWtpEventReq.vendSpec.u2MsgEleLen +=
                            ApHdlrMsg.PmWtpEventReq.MacHdlrStatsElement.
                            u2MsgEleLen;

                        /*AP PARAMS ELEMENT */

                        ApHdlrMsg.PmWtpEventReq.ApParamsElement.ApElement.
                            u4GatewayIp = ApHdlrGetApGatewayIpAddr ();
                        ApHdlrMsg.PmWtpEventReq.ApParamsElement.ApElement.
                            u4Subnetmask = ApHdlrGetApNetMask ();

                        u4CurrSentBytes =
                            ApHdlrGetApSentBytes (gau1IfName) +
                            u4RadioOutOctets;

                        if (WssIfProcessApHdlrDBMsg (WSS_APHDLR_GET_AP_STATS,
                                                     &WssIfApHdlrDB) !=
                            OSIX_SUCCESS)
                        {
                            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                        "WTPEventRequest:-Failed to Get the AP STATS \r\n");
                            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                          "WTPEventRequest:-Failed to Get the AP STATS \r\n"));

                            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                            UtlShMemFreeWssWlanBuf ((UINT1 *)
                                                    pWssWlanMsgStruct);
                            return OSIX_FAILURE;
                        }

                        ApHdlrMsg.PmWtpEventReq.ApParamsElement.ApElement.
                            u4SentBytes =
                            (u4CurrSentBytes -
                             WssIfApHdlrDB.ApStats.u4SentBytes);

                        u4CurrRcvdBytes =
                            ApHdlrGetApRcvdBytes (gau1IfName) + u4RadioInOctets;

                        if (WssIfProcessApHdlrDBMsg (WSS_APHDLR_GET_AP_STATS,
                                                     &WssIfApHdlrDB) !=
                            OSIX_SUCCESS)
                        {
                            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                        "WTPEventRequest:-Failed to Get the AP STATS\r\n");
                            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                          "WTPEventRequest:-Failed to Get the AP STATS\r\n"));

                            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                            UtlShMemFreeWssWlanBuf ((UINT1 *)
                                                    pWssWlanMsgStruct);
                            return OSIX_FAILURE;
                        }
                        ApHdlrMsg.PmWtpEventReq.ApParamsElement.ApElement.
                            u4RcvdBytes =
                            (u4CurrRcvdBytes -
                             WssIfApHdlrDB.ApStats.u4RcvdBytes);

                        WssIfApHdlrDB.ApStats.u4SentBytes = u4CurrSentBytes;
                        WssIfApHdlrDB.ApStats.u4RcvdBytes = u4CurrRcvdBytes;

                        if (WssIfProcessApHdlrDBMsg (WSS_APHDLR_SET_AP_STATS,
                                                     &WssIfApHdlrDB) !=
                            OSIX_SUCCESS)
                        {
                            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                        "WTPEventRequest:-Failed to Get the AP STATS \r\n");
                            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                          "WTPEventRequest:-Failed to Get the AP STATS \r\n"));

                            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                            UtlShMemFreeWssWlanBuf ((UINT1 *)
                                                    pWssWlanMsgStruct);
                            return OSIX_FAILURE;
                        }

                        ApHdlrMsg.PmWtpEventReq.ApParamsElement.ApElement.
                            u1TotalInterfaces =
                            ApHdlrStatsGetNumOfInterfaces () +
                            SYS_DEF_MAX_RADIO_INTERFACES;
                        ApHdlrMsg.PmWtpEventReq.ApParamsElement.isOptional =
                            OSIX_TRUE;
                        ApHdlrMsg.PmWtpEventReq.ApParamsElement.u2MsgEleLen =
                            AP_STATS_MSG_LEN;

                        CAPWAP_PUT_4BYTE (pu1MsgElemValue,
                                          ApHdlrMsg.
                                          PmWtpEventReq.ApParamsElement.
                                          ApElement.u4GatewayIp);
                        CAPWAP_PUT_4BYTE (pu1MsgElemValue,
                                          ApHdlrMsg.
                                          PmWtpEventReq.ApParamsElement.
                                          ApElement.u4Subnetmask);
                        CAPWAP_PUT_4BYTE (pu1MsgElemValue,
                                          ApHdlrMsg.
                                          PmWtpEventReq.ApParamsElement.
                                          ApElement.u4SentBytes);
                        CAPWAP_PUT_4BYTE (pu1MsgElemValue,
                                          ApHdlrMsg.
                                          PmWtpEventReq.ApParamsElement.
                                          ApElement.u4RcvdBytes);
                        CAPWAP_PUT_4BYTE (pu1MsgElemValue,
                                          ApHdlrMsg.
                                          PmWtpEventReq.ApParamsElement.
                                          ApElement.u4SentTrafficRate);
                        CAPWAP_PUT_4BYTE (pu1MsgElemValue,
                                          ApHdlrMsg.
                                          PmWtpEventReq.ApParamsElement.
                                          ApElement.u4RcvdTrafficRate);
                        CAPWAP_PUT_1BYTE (pu1MsgElemValue,
                                          ApHdlrMsg.
                                          PmWtpEventReq.ApParamsElement.
                                          ApElement.u1TotalInterfaces);

                        ApHdlrMsg.PmWtpEventReq.vendSpec.u2MsgEleLen +=
                            ApHdlrMsg.PmWtpEventReq.ApParamsElement.u2MsgEleLen;

                        /* RADIO CLIENT STATS MESSAGE ELEMENT */

                        ApHdlrMsg.PmWtpEventReq.RadioClientStatsElement.
                            isOptional = OSIX_TRUE;
#ifdef NPAPI_WANTED
                        ApHdlrMsg.PmWtpEventReq.
                            RadioClientStatsElement.numRadioCount =
                            SYS_DEF_MAX_RADIO_INTERFACES;
                        u4RadioInOctets = u4RadioOutOctets = 0;
                        u4HwCount = u4HwCount1 = 0;
                        for (u1Radioid = 1;
                             u1Radioid <=
                             ApHdlrMsg.PmWtpEventReq.RadioClientStatsElement.
                             numRadioCount; u1Radioid++)
                        {
                            SPRINTF ((CHR1 *) FsHwNp.RadioIfNpModInfo.
                                     unOpCode.sConfigGet80211Stats.RadInfo.
                                     au1RadioIfName, "%s%d", RADIO_INTF_NAME,
                                     u1Radioid - 1);
                            FsHwNp.RadioIfNpModInfo.
                                unOpCode.sConfigGetInterfaceInOctets.
                                u4IfInOctets = 0;
                            FsHwNp.RadioIfNpModInfo.
                                unOpCode.sConfigGetInterfaceOutOctets.
                                u4IfOutOctets = 0;
                            FsHwNp.u4Opcode =
                                FS_RADIO_HW_GET_RADIO_CLIENT_STATS;
                            /*Get the number of octets received on radio interface
                             * identified by u1Radioid*/
                            RadioNpWrHwProgram (&FsHwNp);

                            u4HwCount =
                                FsHwNp.RadioIfNpModInfo.
                                unOpCode.sConfigGetInterfaceInOctets.
                                u4IfInOctets;

                            RadioIfGetDB.RadioIfGetAllDB.u1RadioId = u1Radioid;
                            RadioIfGetDB.RadioIfIsGetAllDB.bRadioIfIndex =
                                OSIX_TRUE;
                            RadioIfGetDB.RadioIfIsGetAllDB.bRadioCounters =
                                OSIX_TRUE;

                            /*Get the radio counters from the RadioIf DB */
                            if (WssIfProcessRadioIfDBMsg
                                (WSS_GET_PHY_RADIO_IF_DB,
                                 &RadioIfGetDB) != OSIX_SUCCESS)
                            {
                                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                            "Failed to get Radio Operationa State from RadioIf \r\n");
                                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL,
                                              gu4CapwapSysLogId,
                                              "Failed to get Radio Operationa State from RadioIf \r\n"));
                                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                                UtlShMemFreeWssWlanBuf ((UINT1 *)
                                                        pWssWlanMsgStruct);
                                return OSIX_FAILURE;
                            }

                            ApHdlrMsg.PmWtpEventReq.
                                RadioClientStatsElement.radioClientStats
                                [u1Radioid - 1].u4IfInOctets =
                                (u4HwCount -
                                 RadioIfGetDB.RadioIfGetAllDB.u4RadioInOctets);

                            u4RadioInOctets +=
                                FsHwNp.RadioIfNpModInfo.
                                unOpCode.sConfigGetInterfaceInOctets.
                                u4IfInOctets;

                            FsHwNp.u4Opcode = FS_RADIO_HW_GET_INT_OUT_OCTETS;
                            RadioNpWrHwProgram (&FsHwNp);

                            u4HwCount1 =
                                FsHwNp.RadioIfNpModInfo.
                                unOpCode.sConfigGetInterfaceOutOctets.
                                u4IfOutOctets;

                            ApHdlrMsg.PmWtpEventReq.
                                RadioClientStatsElement.radioClientStats
                                [u1Radioid - 1].u4IfOutOctets =
                                (u4HwCount1 -
                                 RadioIfGetDB.RadioIfGetAllDB.u4RadioOutOctets);

                            RadioIfGetDB.RadioIfGetAllDB.u1RadioId = u1Radioid;
                            RadioIfGetDB.RadioIfIsGetAllDB.bRadioCounters =
                                OSIX_TRUE;
                            RadioIfGetDB.RadioIfIsGetAllDB.bRadioIfIndex =
                                OSIX_TRUE;

                            RadioIfGetDB.RadioIfGetAllDB.u4RadioInOctets =
                                u4HwCount;
                            RadioIfGetDB.RadioIfGetAllDB.u4RadioOutOctets =
                                u4HwCount1;
                            if (WssIfProcessRadioIfDBMsg
                                (WSS_SET_PHY_RADIO_IF_DB,
                                 &RadioIfGetDB) != OSIX_SUCCESS)
                            {
                                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                            "Failed to get Radio Operationa State from RadioIf \r\n");
                                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL,
                                              gu4CapwapSysLogId,
                                              "Failed to get Radio Operationa State from RadioIf \r\n"));

                                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                                UtlShMemFreeWssWlanBuf ((UINT1 *)
                                                        pWssWlanMsgStruct);
                                return OSIX_FAILURE;
                            }
                            u4RadioOutOctets +=
                                FsHwNp.RadioIfNpModInfo.
                                unOpCode.sConfigGetInterfaceOutOctets.
                                u4IfOutOctets;
                        }

                        /*Get the Client Stats from Station DB */
                        WssStaWtpShowClient ();

                        for (u1Index2 = 0; u1Index2 < gu1WtpClientWalkIndex;
                             u1Index2++)
                        {

                            MEMCPY (ApHdlrMsg.PmWtpEventReq.
                                    RadioClientStatsElement.clientStats
                                    [u1Index2].StaMacAddr,
                                    gaWssStaWtpDBWalk[u1Index2].staMacAddr,
                                    sizeof (tMacAddr));

                            MEMSET (&WssMsgStructs, 0,
                                    sizeof (unWssMsgStructs));
                            MEMCPY (&WssMsgStructs.WssStaStateDB.
                                    stationMacAddress,
                                    gaWssStaWtpDBWalk[u1Index2].staMacAddr,
                                    sizeof (tMacAddr));

                            if (WssStaWtpProcessWssIfMsg
                                (WSS_STA_GET_DB,
                                 &WssMsgStructs) != OSIX_SUCCESS)
                            {
                                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                            "WSS_STA_GET_DB Failed \r\n\n");
                                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL,
                                              gu4CapwapSysLogId,
                                              "WSS_STA_GET_DB Failed \r\n\n"));
                                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                                UtlShMemFreeWssWlanBuf ((UINT1 *)
                                                        pWssWlanMsgStruct);
                                return OSIX_FAILURE;
                            }

                            u1Radioid = WssMsgStructs.WssStaStateDB.u1RadioId;
                            u1Wlanid = WssMsgStructs.WssStaStateDB.u1WlanId;
                            MEMSET (au1WlanName, 0, 6);
                            WssWlanGetWlanIntfName (u1Radioid, u1Wlanid,
                                                    au1WlanName, &u1Wlanid);
                            STRCPY (FsHwNp.RadioIfNpModInfo.unOpCode.
                                    sConfigGetClientStatsBytesRecvdCount.
                                    RadInfo.au1WlanIfname[0], au1WlanName);
                            FsHwNp.RadioIfNpModInfo.unOpCode.
                                sConfigGetClientStatsBytesRecvdCount.
                                u4ClientStatsBytesRecvdCount = 0;
                            FsHwNp.u4Opcode =
                                FS_RADIO_HW_GET_CLIENT_RECVD_COUNT;

                            MEMCPY (FsHwNp.RadioIfNpModInfo.
                                    unOpCode.
                                    sConfigGetClientStatsBytesRecvdCount.
                                    stationMacAddress,
                                    gaWssStaWtpDBWalk[u1Index2].staMacAddr,
                                    sizeof (tMacAddr));

                            RadioNpWrHwProgram (&FsHwNp);
                            ApHdlrMsg.PmWtpEventReq.
                                RadioClientStatsElement.clientStats[u1Index2].
                                u4ClientStatsBytesRecvdCount =
                                FsHwNp.RadioIfNpModInfo.
                                unOpCode.sConfigGetClientStatsBytesRecvdCount.
                                u4ClientStatsBytesRecvdCount;

                            FsHwNp.RadioIfNpModInfo.
                                unOpCode.sConfigGetClientStatsBytesSentCount.
                                u4ClientStatsBytesSentCount = 0;
                            FsHwNp.u4Opcode = FS_RADIO_HW_GET_CLIENT_SENT_COUNT;
                            MEMCPY (FsHwNp.RadioIfNpModInfo.
                                    unOpCode.
                                    sConfigGetClientStatsBytesSentCount.
                                    stationMacAddress,
                                    gaWssStaWtpDBWalk[u1Index2].staMacAddr,
                                    sizeof (tMacAddr));
                            RadioNpWrHwProgram (&FsHwNp);
                            ApHdlrMsg.PmWtpEventReq.
                                RadioClientStatsElement.clientStats[u1Index2].
                                u4ClientStatsBytesSentCount =
                                FsHwNp.RadioIfNpModInfo.
                                unOpCode.sConfigGetClientStatsBytesSentCount.
                                u4ClientStatsBytesSentCount;

                            u1NumStaCount++;
                        }
                        ApHdlrMsg.PmWtpEventReq.
                            RadioClientStatsElement.numStaCount = u1NumStaCount;
                        ApHdlrMsg.PmWtpEventReq.RadioClientStatsElement.
                            isOptional = OSIX_TRUE;

#endif

                        CAPWAP_PUT_1BYTE (pu1MsgElemValue,
                                          ApHdlrMsg.
                                          PmWtpEventReq.RadioClientStatsElement.
                                          numRadioCount);
                        ApHdlrMsg.PmWtpEventReq.RadioClientStatsElement.
                            u2MsgEleLen += sizeof (UINT1);

                        for (u1Index1 = 0;
                             u1Index1 <
                             (ApHdlrMsg.PmWtpEventReq.RadioClientStatsElement.
                              numRadioCount); u1Index1++)
                        {
                            CAPWAP_PUT_4BYTE (pu1MsgElemValue,
                                              ApHdlrMsg.PmWtpEventReq.
                                              RadioClientStatsElement.
                                              radioClientStats[u1Index1].
                                              u4IfInOctets);
                            ApHdlrMsg.PmWtpEventReq.RadioClientStatsElement.
                                u2MsgEleLen += sizeof (UINT4);
                            CAPWAP_PUT_4BYTE (pu1MsgElemValue,
                                              ApHdlrMsg.PmWtpEventReq.
                                              RadioClientStatsElement.
                                              radioClientStats[u1Index1].
                                              u4IfOutOctets);
                            ApHdlrMsg.PmWtpEventReq.RadioClientStatsElement.
                                u2MsgEleLen += sizeof (UINT4);
                        }

                        CAPWAP_PUT_1BYTE (pu1MsgElemValue,
                                          ApHdlrMsg.
                                          PmWtpEventReq.RadioClientStatsElement.
                                          numStaCount);
                        ApHdlrMsg.PmWtpEventReq.RadioClientStatsElement.
                            u2MsgEleLen += sizeof (UINT1);
                        for (u1Index2 = 0;
                             u1Index2 <
                             (ApHdlrMsg.PmWtpEventReq.RadioClientStatsElement.
                              numStaCount); u1Index2++)
                        {
                            WssIfStaGetIpAddr (&u4Ipaddr,
                                               ApHdlrMsg.PmWtpEventReq.
                                               RadioClientStatsElement.
                                               clientStats[u1Index2].
                                               StaMacAddr);
                            CAPWAP_PUT_NBYTE (pu1MsgElemValue,
                                              ApHdlrMsg.PmWtpEventReq.
                                              RadioClientStatsElement.
                                              clientStats[u1Index2].StaMacAddr,
                                              MAC_ADDR_LEN);
                            ApHdlrMsg.PmWtpEventReq.RadioClientStatsElement.
                                u2MsgEleLen += MAC_ADDR_LEN;
                            CAPWAP_PUT_4BYTE (pu1MsgElemValue,
                                              ApHdlrMsg.PmWtpEventReq.
                                              RadioClientStatsElement.
                                              clientStats[u1Index2].
                                              u4ClientStatsBytesSentCount);
                            ApHdlrMsg.PmWtpEventReq.RadioClientStatsElement.
                                u2MsgEleLen += sizeof (UINT4);

                            CAPWAP_PUT_4BYTE (pu1MsgElemValue,
                                              ApHdlrMsg.PmWtpEventReq.
                                              RadioClientStatsElement.
                                              clientStats[u1Index2].
                                              u4ClientStatsBytesRecvdCount);
                            ApHdlrMsg.PmWtpEventReq.RadioClientStatsElement.
                                u2MsgEleLen += sizeof (UINT4);
                            CAPWAP_PUT_4BYTE (pu1MsgElemValue, u4Ipaddr);

                            ApHdlrMsg.PmWtpEventReq.RadioClientStatsElement.
                                u2MsgEleLen += sizeof (UINT4);
                        }

                        ApHdlrMsg.PmWtpEventReq.vendSpec.u2MsgEleLen +=
                            ApHdlrMsg.PmWtpEventReq.RadioClientStatsElement.
                            u2MsgEleLen;

                        *pu2MsgElemLen =
                            ApHdlrMsg.PmWtpEventReq.vendSpec.u2MsgEleLen;
                        break;
                    case VENDOR_DFS_RADAR_STATS:
                    {
                        if (OSIX_TRUE ==
                            ApHdlrMsg.PmWtpEventReq.vendSpec.unVendorSpec.
                            DFSRadarEventInfo.isOptional)
                        {
                            CAPWAP_PUT_4BYTE (pu1MsgElemValue,
                                              ApHdlrMsg.PmWtpEventReq.vendSpec.
                                              unVendorSpec.DFSRadarEventInfo.
                                              u4VendorId);
                            ApHdlrMsg.PmWtpEventReq.vendSpec.u2MsgEleLen +=
                                sizeof (UINT4);
                            CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                                              ApHdlrMsg.PmWtpEventReq.vendSpec.
                                              unVendorSpec.DFSRadarEventInfo.
                                              u2MsgEleType);
                            ApHdlrMsg.PmWtpEventReq.vendSpec.u2MsgEleLen +=
                                sizeof (UINT2);
                            CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                                              ApHdlrMsg.PmWtpEventReq.vendSpec.
                                              unVendorSpec.DFSRadarEventInfo.
                                              u2MsgEleLen);
                            ApHdlrMsg.PmWtpEventReq.vendSpec.u2MsgEleLen +=
                                sizeof (UINT2);

                            CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                                              ApHdlrMsg.PmWtpEventReq.vendSpec.
                                              unVendorSpec.DFSRadarEventInfo.
                                              u2ChannelNum);

                            CAPWAP_PUT_1BYTE (pu1MsgElemValue,
                                              ApHdlrMsg.PmWtpEventReq.vendSpec.
                                              unVendorSpec.DFSRadarEventInfo.
                                              isRadarFound);
                            ApHdlrMsg.PmWtpEventReq.vendSpec.u2MsgEleLen +=
                                ApHdlrMsg.PmWtpEventReq.vendSpec.unVendorSpec.
                                DFSRadarEventInfo.u2MsgEleLen;

                            *pu2MsgElemLen =
                                ApHdlrMsg.PmWtpEventReq.vendSpec.u2MsgEleLen;

                        }

                    }
                        break;

                }
#endif
            }
            else
            {

                switch (u1MsgSubType)
                {
                    case VENDOR_DISC_TYPE:
#ifdef WLC_WANTED
                        vendSpec.u2MsgEleType = VENDOR_SPECIFIC_PAYLOAD;
                        vendSpec.u2MsgEleLen = VENDOR_DISC_TYPE_MSG_ELM_LEN;
                        vendSpec.isOptional = OSIX_TRUE;

                        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpDiscType =
                            OSIX_TRUE;

                        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                                     pWssIfCapwapDB) ==
                            OSIX_SUCCESS)
                        {
                            vendSpec.unVendorSpec.VendDiscType.u2MsgEleType
                                = DISCOVERY_TYPE_VENDOR_MSG;
                            vendSpec.unVendorSpec.VendDiscType.u2MsgEleLen
                                = DISCOVERY_TYPE_VENDOR_MSG_LEN;
                            vendSpec.unVendorSpec.VendDiscType.vendorId
                                = VENDOR_ID;
                            vendSpec.unVendorSpec.VendDiscType.u1Val
                                = pWssIfCapwapDB->CapwapGetDB.u1WtpDiscType;
                            vendSpec.unVendorSpec.VendDiscType.u4WlcIpAddress
                                = pWssIfCapwapDB->CapwapGetDB.u4WlcStaticIp;
                            vendSpec.unVendorSpec.VendDiscType.isOptional =
                                OSIX_TRUE;
                        }
#else
                        vendSpec.u2MsgEleType = VENDOR_SPECIFIC_PAYLOAD;
                        vendSpec.u2MsgEleLen = DISCOVERY_TYPE_VENDOR_MSG_LEN;
                        vendSpec.elementId = VENDOR_DISC_TYPE;
                        vendSpec.isOptional = OSIX_TRUE;

                        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpDiscType
                            = OSIX_TRUE;

                        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                                     pWssIfCapwapDB) ==
                            OSIX_SUCCESS)
                        {
                            vendSpec.unVendorSpec.VendDiscType.u2MsgEleType
                                = DISCOVERY_TYPE_VENDOR_MSG;
                            vendSpec.unVendorSpec.VendDiscType.u2MsgEleLen
                                = CAPWAP_VENDOR_DISC_TYPE_MSG_ELM_LEN;
                            vendSpec.unVendorSpec.VendDiscType.vendorId
                                = VENDOR_ID;
                            vendSpec.unVendorSpec.VendDiscType.u1Val
                                = pWssIfCapwapDB->CapwapGetDB.u1WtpDiscType;
                            vendSpec.unVendorSpec.VendDiscType.isOptional
                                = TRUE;
                        }
#endif
                        /*Assemble */
                        if (vendSpec.unVendorSpec.VendDiscType.isOptional
                            == OSIX_TRUE)
                        {
                            CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                                              vendSpec.unVendorSpec.
                                              VendDiscType.u2MsgEleType);
                            u2ElemLength =
                                (UINT2) (u2ElemLength + sizeof (UINT2));
                            CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                                              vendSpec.unVendorSpec.
                                              VendDiscType.u2MsgEleLen);
                            u2ElemLength =
                                (UINT2) (u2ElemLength + sizeof (UINT2));
                            CAPWAP_PUT_4BYTE (pu1MsgElemValue,
                                              vendSpec.unVendorSpec.
                                              VendDiscType.vendorId);
                            u2ElemLength =
                                (UINT2) (u2ElemLength + sizeof (UINT4));
                            CAPWAP_PUT_1BYTE (pu1MsgElemValue,
                                              vendSpec.unVendorSpec.
                                              VendDiscType.u1Val);
                            u2ElemLength =
                                (UINT2) (u2ElemLength + sizeof (UINT1));
                        }
                        *pu2MsgElemLen = u2ElemLength;
                        break;

                    case VENDOR_MGMT_SSID_TYPE:
#ifdef WLC_WANTED
                        vendSpec.u2MsgEleType = VENDOR_SPECIFIC_PAYLOAD;
                        vendSpec.u2MsgEleLen =
                            VENDOR_MGMT_SSID_TYPE_MSG_ELM_LEN;
                        vendSpec.isOptional = OSIX_TRUE;

                        vendSpec.unVendorSpec.VendSsid.u2MsgEleType
                            = MGMT_SSID_VENDOR_MSG;
                        vendSpec.unVendorSpec.VendSsid.u2MsgEleLen
                            = MGMT_SSID_VENDOR_MSG_LEN;
                        vendSpec.unVendorSpec.VendSsid.vendorId = VENDOR_ID;
                        vendSpec.unVendorSpec.VendSsid.isOptional = OSIX_TRUE;

#else
                        vendSpec.u2MsgEleType = VENDOR_SPECIFIC_PAYLOAD;
                        vendSpec.u2MsgEleLen = MGMT_SSID_VENDOR_MSG_LEN;
                        vendSpec.elementId = VENDOR_MGMT_SSID_TYPE;
                        vendSpec.isOptional = TRUE;
                        vendSpec.unVendorSpec.VendSsid.u2MsgEleType
                            = MGMT_SSID_VENDOR_MSG;
                        vendSpec.unVendorSpec.VendSsid.u2MsgEleLen
                            = WTP_MGMT_SSID_VENDOR_MSG_ELM_LEN;
                        vendSpec.unVendorSpec.VendSsid.vendorId = VENDOR_ID;
                        vendSpec.unVendorSpec.VendSsid.isOptional = OSIX_TRUE;
#endif
                        if (vendSpec.unVendorSpec.VendSsid.isOptional
                            == OSIX_TRUE)
                        {
                            CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                                              vendSpec.unVendorSpec.VendSsid.
                                              u2MsgEleType);
                            u2ElemLength =
                                (UINT2) (u2ElemLength + sizeof (UINT2));
                            CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                                              vendSpec.unVendorSpec.VendSsid.
                                              u2MsgEleLen);
                            u2ElemLength =
                                (UINT2) (u2ElemLength + sizeof (UINT2));
                            CAPWAP_PUT_4BYTE (pu1MsgElemValue,
                                              vendSpec.unVendorSpec.VendSsid.
                                              vendorId);
                            u2ElemLength =
                                (UINT2) (u2ElemLength + sizeof (UINT4));
                            CAPWAP_PUT_NBYTE (pu1MsgElemValue,
                                              vendSpec.unVendorSpec.
                                              VendSsid.au1ManagmentSSID,
                                              /* strlen(vendPld->unVendorSpec.VendSsid.au1ManagmentSSID)); */
                                              WSSWLAN_SSID_NAME_LEN);
                            u2ElemLength =
                                (UINT2) (u2ElemLength + WSSWLAN_SSID_NAME_LEN);
                        }
                        *pu2MsgElemLen = u2ElemLength;
                        break;

                    case VENDOR_DOT11N_TYPE:
#ifdef WTP_WANTED
                        RadioIfGetDB.RadioIfGetAllDB.u1RadioId = u1RadioID;

                        RadioIfGetDB.RadioIfIsGetAllDB.bHtFlag = OSIX_TRUE;
                        RadioIfGetDB.RadioIfIsGetAllDB.bMaxSuppMCS = OSIX_TRUE;
                        RadioIfGetDB.RadioIfIsGetAllDB.bMaxManMCS = OSIX_TRUE;
                        RadioIfGetDB.RadioIfIsGetAllDB.bTxAntenna = OSIX_TRUE;
                        RadioIfGetDB.RadioIfIsGetAllDB.bRxAntenna = OSIX_TRUE;

                        RadioIfGetDB.RadioIfIsGetAllDB.bDot11RadioType
                            = OSIX_TRUE;
                        RadioIfGetDB.RadioIfIsGetAllDB.bChannelWidth
                            = OSIX_TRUE;
                        RadioIfGetDB.RadioIfIsGetAllDB.bShortGi20 = OSIX_TRUE;
                        RadioIfGetDB.RadioIfIsGetAllDB.bShortGi40 = OSIX_TRUE;
                        RadioIfGetDB.RadioIfIsGetAllDB.bShortGi40 = OSIX_TRUE;
                        RadioIfGetDB.RadioIfIsGetAllDB.bMCSRateSet = OSIX_TRUE;
                        if (WssIfProcessRadioIfDBMsg (WSS_GET_PHY_RADIO_IF_DB,
                                                      &RadioIfGetDB) !=
                            OSIX_SUCCESS)
                        {
                            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                        "Failed to get Radio Interface Index\r\n");
                            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                          "Failed to get Radio Interface Index\r\n"));
                            UtlShMemFreeWssWlanBuf ((UINT1 *)
                                                    pWssWlanMsgStruct);
                            return OSIX_FAILURE;
                        }
                        if (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                            RADIO_TYPE_BGN
                            || RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType
                            == RADIO_TYPE_AN)
                        {
                            vendSpec.isOptional = OSIX_TRUE;

                            vendSpec.unVendorSpec.Dot11nVendor.isOptional =
                                OSIX_TRUE;

                            vendSpec.unVendorSpec.Dot11nVendor.u2MsgEleType
                                = DOT11N_VENDOR_MSG;
                            vendSpec.unVendorSpec.Dot11nVendor.u2MsgEleLen
                                = DOT11N_VENDOR_MSG_LEN;
                            vendSpec.unVendorSpec.Dot11nVendor.u4VendorId
                                = VENDOR_ID;
                            vendSpec.unVendorSpec.Dot11nVendor.u1RadioId
                                = RadioIfGetDB.RadioIfGetAllDB.u1RadioId;
                            vendSpec.unVendorSpec.Dot11nVendor.u1HtFlag
                                = RadioIfGetDB.RadioIfGetAllDB.u1HtFlag;

                            vendSpec.unVendorSpec.Dot11nVendor.u1MaxSuppMCS
                                = RadioIfGetDB.RadioIfGetAllDB.u1MaxSuppMCS;
                            vendSpec.unVendorSpec.Dot11nVendor.u1MaxManMCS
                                = RadioIfGetDB.RadioIfGetAllDB.u1MaxManMCS;

                            vendSpec.unVendorSpec.Dot11nVendor.u1TxAntenna
                                = RadioIfGetDB.RadioIfGetAllDB.u1TxAntenna;

                            vendSpec.unVendorSpec.Dot11nVendor.u1RxAntenna
                                = RadioIfGetDB.RadioIfGetAllDB.u1RxAntenna;

                            vendSpec.u2MsgEleType = VENDOR_SPECIFIC_PAYLOAD;
                            vendSpec.u2MsgEleLen = DOT11N_VENDOR_MSG_LEN +
                                CAPWAP_MSG_ELEM_TYPE_LEN;
                        }
                        vendSpec.elementId = VENDOR_DOT11N_TYPE;
#else
                        vendSpec.u2MsgEleType = VENDOR_SPECIFIC_PAYLOAD;
                        vendSpec.u2MsgEleLen = DOT11N_VENDOR_MSG_LEN +
                            CAPWAP_MSG_ELEM_TYPE_LEN;
                        vendSpec.elementId = VENDOR_DOT11N_TYPE;

                        vendSpec.unVendorSpec.Dot11nVendor.u2MsgEleType
                            = DOT11N_VENDOR_MSG;
                        vendSpec.unVendorSpec.Dot11nVendor.u2MsgEleLen
                            = DOT11N_VENDOR_MSG_LEN;
                        vendSpec.unVendorSpec.Dot11nVendor.u4VendorId
                            = VENDOR_ID;
                        vendSpec.unVendorSpec.Dot11nVendor.isOptional =
                            OSIX_TRUE;

#endif
                        /*Assemble */
                        if (vendSpec.unVendorSpec.Dot11nVendor.isOptional
                            == OSIX_TRUE)
                        {
                            CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                                              vendSpec.unVendorSpec.
                                              Dot11nVendor.u2MsgEleType);
                            u2ElemLength =
                                (UINT2) (u2ElemLength + sizeof (UINT2));
                            CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                                              vendSpec.unVendorSpec.
                                              Dot11nVendor.u2MsgEleLen);
                            u2ElemLength =
                                (UINT2) (u2ElemLength + sizeof (UINT2));
                            CAPWAP_PUT_4BYTE (pu1MsgElemValue,
                                              vendSpec.unVendorSpec.
                                              Dot11nVendor.u4VendorId);
                            u2ElemLength =
                                (UINT2) (u2ElemLength + sizeof (UINT4));
                            CAPWAP_PUT_1BYTE (pu1MsgElemValue,
                                              vendSpec.unVendorSpec.
                                              Dot11nVendor.u1RadioId);
                            u2ElemLength =
                                (UINT2) (u2ElemLength + sizeof (UINT1));
                            CAPWAP_PUT_1BYTE (pu1MsgElemValue,
                                              vendSpec.unVendorSpec.
                                              Dot11nVendor.u1HtFlag);
                            u2ElemLength =
                                (UINT2) (u2ElemLength + sizeof (UINT1));
                            CAPWAP_PUT_1BYTE (pu1MsgElemValue,
                                              vendSpec.unVendorSpec.
                                              Dot11nVendor.u1MaxSuppMCS);
                            u2ElemLength =
                                (UINT2) (u2ElemLength + sizeof (UINT1));
                            CAPWAP_PUT_1BYTE (pu1MsgElemValue,
                                              vendSpec.unVendorSpec.
                                              Dot11nVendor.u1MaxManMCS);
                            u2ElemLength =
                                (UINT2) (u2ElemLength + sizeof (UINT1));
                            CAPWAP_PUT_1BYTE (pu1MsgElemValue,
                                              vendSpec.unVendorSpec.
                                              Dot11nVendor.u1TxAntenna);
                            u2ElemLength =
                                (UINT2) (u2ElemLength + sizeof (UINT1));
                            CAPWAP_PUT_1BYTE (pu1MsgElemValue,
                                              vendSpec.unVendorSpec.
                                              Dot11nVendor.u1RxAntenna);
                            u2ElemLength =
                                (UINT2) (u2ElemLength + sizeof (UINT1));
                            CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                                              vendSpec.unVendorSpec.
                                              Dot11nVendor.u2Reserved);
                            u2ElemLength =
                                (UINT2) (u2ElemLength + sizeof (UINT2));
                            /* FIX to add , additional length */
                            CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                                              vendSpec.unVendorSpec.
                                              Dot11nVendor.u2Reserved);
                            u2ElemLength =
                                (UINT2) (u2ElemLength + sizeof (UINT2));
                            CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                                              vendSpec.unVendorSpec.
                                              Dot11nVendor.u2Reserved);
                            u2ElemLength =
                                (UINT2) (u2ElemLength + sizeof (UINT2));
                        }
                        *pu2MsgElemLen = u2ElemLength;
                        break;
                    case VENDOR_DOMAIN_NAME_TYPE:
#ifdef WTP_WANTED
                        vendSpec.u2MsgEleType = VENDOR_SPECIFIC_PAYLOAD;
                        vendSpec.u2MsgEleLen = VENDOR_DOMAIN_NAME_LENGTH;
                        vendSpec.isOptional = OSIX_TRUE;
                        vendSpec.elementId = VENDOR_DOMAIN_NAME_TYPE;

                        pWssIfCapwapDB->CapwapIsGetAllDB.bCapwapDnsServerIp
                            = OSIX_TRUE;
                        pWssIfCapwapDB->CapwapIsGetAllDB.bCapwapDnsDomainName
                            = OSIX_TRUE;

                        if (WssIfProcessCapwapDBMsg
                            (WSS_CAPWAP_GET_DB, pWssIfCapwapDB) == OSIX_SUCCESS)
                        {
                            vendSpec.unVendorSpec.VendDns.u2MsgEleType
                                = DOMAIN_NAME_VENDOR_MSG;
                            vendSpec.unVendorSpec.VendDns.u2MsgEleLen
                                = (DOMAIN_NAME_VEND_MSG_ELEM_LENGTH
                                   + DNS_SERVER_VEND_MSG_ELEM_LENGTH);
                            vendSpec.unVendorSpec.VendDns.vendorId = VENDOR_ID;
                            MEMCPY (vendSpec.unVendorSpec.VendDns.au1DomainName,
                                    pWssIfCapwapDB->
                                    DnsProfileEntry.au1FsCapwapDnsDomainName,
                                    DOMAIN_NAME_VEND_MSG_ELEM_LENGTH);
                            vendSpec.unVendorSpec.VendDns.isOptional = TRUE;
                        }
#else
                        vendSpec.u2MsgEleType = VENDOR_SPECIFIC_PAYLOAD;
                        vendSpec.u2MsgEleLen = VENDOR_DOMAIN_NAME_LENGTH;
                        vendSpec.isOptional = OSIX_TRUE;
                        vendSpec.elementId = VENDOR_DOMAIN_NAME_TYPE;

                        vendSpec.unVendorSpec.VendDns.u2MsgEleType
                            = DOMAIN_NAME_VENDOR_MSG;
                        vendSpec.unVendorSpec.VendDns.u2MsgEleLen
                            = (DOMAIN_NAME_VEND_MSG_ELEM_LENGTH
                               + DNS_SERVER_VEND_MSG_ELEM_LENGTH);
                        vendSpec.unVendorSpec.VendDns.vendorId = VENDOR_ID;
                        vendSpec.unVendorSpec.VendDns.isOptional = TRUE;

#endif
                        /*Assemble */
                        if (vendSpec.unVendorSpec.VendDns.isOptional
                            == OSIX_TRUE)
                        {
                            CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                                              vendSpec.unVendorSpec.VendDns.
                                              u2MsgEleType);
                            u2ElemLength =
                                (UINT2) (u2ElemLength + sizeof (UINT2));
                            CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                                              vendSpec.unVendorSpec.VendDns.
                                              u2MsgEleLen);
                            u2ElemLength =
                                (UINT2) (u2ElemLength + sizeof (UINT2));
                            CAPWAP_PUT_4BYTE (pu1MsgElemValue,
                                              vendSpec.unVendorSpec.VendDns.
                                              vendorId);
                            u2ElemLength =
                                (UINT2) (u2ElemLength + sizeof (UINT4));
                            CAPWAP_PUT_NBYTE (pu1MsgElemValue,
                                              vendSpec.unVendorSpec.VendDns.
                                              au1DomainName, 32);
                            u2ElemLength = (UINT2) (u2ElemLength + 32);
                        }
                        *pu2MsgElemLen = u2ElemLength;
                        break;
                    case VENDOR_NATIVE_VLAN:
#ifdef WTP_WANTED
                        vendSpec.u2MsgEleType = VENDOR_SPECIFIC_PAYLOAD;
                        vendSpec.u2MsgEleLen = VENDOR_NATIVE_VLAN_LENGTH;
                        vendSpec.isOptional = TRUE;
                        vendSpec.elementId = VENDOR_NATIVE_VLAN;

                        pWssIfCapwapDB->CapwapIsGetAllDB.bNativeVlan
                            = OSIX_TRUE;
                        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                                     pWssIfCapwapDB) ==
                            OSIX_SUCCESS)
                        {
                            vendSpec.unVendorSpec.VendVlan.u2MsgEleType
                                = NATIVE_VLAN_VENDOR_MSG;
                            vendSpec.unVendorSpec.VendVlan.u2MsgEleLen
                                = NATIVE_VLAN_VEND_MSG_ELEM_LENGTH;
                            vendSpec.unVendorSpec.VendVlan.vendorId = VENDOR_ID;
                            vendSpec.unVendorSpec.VendVlan.u2VlanId
                                = pWssIfCapwapDB->CapwapGetDB.u4NativeVlan;
                            vendSpec.unVendorSpec.VendVlan.isOptional
                                = OSIX_TRUE;
                        }
#else

                        vendSpec.u2MsgEleType = VENDOR_SPECIFIC_PAYLOAD;
                        vendSpec.u2MsgEleLen = VENDOR_NATIVE_VLAN_LENGTH;
                        vendSpec.isOptional = TRUE;
                        vendSpec.elementId = VENDOR_NATIVE_VLAN;

                        vendSpec.unVendorSpec.VendVlan.u2MsgEleType
                            = NATIVE_VLAN_VENDOR_MSG;
                        vendSpec.unVendorSpec.VendVlan.u2MsgEleLen
                            = NATIVE_VLAN_VEND_MSG_ELEM_LENGTH;
                        vendSpec.unVendorSpec.VendVlan.vendorId = VENDOR_ID;
                        vendSpec.unVendorSpec.VendVlan.isOptional = OSIX_TRUE;
#endif
                        if (vendSpec.unVendorSpec.VendVlan.isOptional
                            == OSIX_TRUE)
                        {
                            CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                                              vendSpec.unVendorSpec.VendVlan.
                                              u2MsgEleType);
                            u2ElemLength =
                                (UINT2) (u2ElemLength + sizeof (UINT2));
                            CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                                              vendSpec.unVendorSpec.VendVlan.
                                              u2MsgEleLen);
                            u2ElemLength =
                                (UINT2) (u2ElemLength + sizeof (UINT2));
                            CAPWAP_PUT_4BYTE (pu1MsgElemValue,
                                              vendSpec.unVendorSpec.VendVlan.
                                              vendorId);
                            u2ElemLength =
                                (UINT2) (u2ElemLength + sizeof (UINT4));
                            CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                                              vendSpec.unVendorSpec.VendVlan.
                                              u2VlanId);
                            u2ElemLength =
                                (UINT2) (u2ElemLength + sizeof (UINT2));
                        }
                        *pu2MsgElemLen = u2ElemLength;

                        break;

                    case VENDOR_LOCAL_ROUTING_TYPE:
#ifdef WTP_WANTED
                        vendSpec.u2MsgEleType = VENDOR_SPECIFIC_PAYLOAD;
                        vendSpec.u2MsgEleLen = VENDOR_LOCAL_ROUTING_TYPE_LENGTH;
                        vendSpec.isOptional = TRUE;
                        vendSpec.elementId = VENDOR_LOCAL_ROUTING_TYPE;

                        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpLocalRouting
                            = OSIX_TRUE;
                        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                                     pWssIfCapwapDB) ==
                            OSIX_SUCCESS)
                        {

                            vendSpec.unVendorSpec.VendLocalRouting.u2MsgEleType
                                = LOCAL_ROUTING_VENDOR_MSG;
                            vendSpec.unVendorSpec.VendLocalRouting.u2MsgEleLen
                                = NATIVE_VLAN_VEND_MSG_ELEM_LENGTH;
                            vendSpec.unVendorSpec.VendLocalRouting.vendorId =
                                VENDOR_ID;
                            vendSpec.unVendorSpec.
                                VendLocalRouting.u1WtpLocalRouting =
                                pWssIfCapwapDB->CapwapGetDB.u1WtpLocalRouting;
                            vendSpec.unVendorSpec.VendLocalRouting.isOptional =
                                OSIX_TRUE;
                        }
#else
                        vendSpec.u2MsgEleType = VENDOR_SPECIFIC_PAYLOAD;
                        vendSpec.u2MsgEleLen = VENDOR_LOCAL_ROUTING_TYPE_LENGTH;
                        vendSpec.isOptional = TRUE;
                        vendSpec.elementId = VENDOR_LOCAL_ROUTING_TYPE;

                        vendSpec.unVendorSpec.VendLocalRouting.u2MsgEleType
                            = LOCAL_ROUTING_VENDOR_MSG;
                        vendSpec.unVendorSpec.VendLocalRouting.u2MsgEleLen
                            = NATIVE_VLAN_VEND_MSG_ELEM_LENGTH;
                        vendSpec.unVendorSpec.VendLocalRouting.vendorId =
                            VENDOR_ID;
                        vendSpec.unVendorSpec.VendLocalRouting.isOptional =
                            OSIX_TRUE;

#endif
                        if (vendSpec.unVendorSpec.VendLocalRouting.isOptional
                            == OSIX_TRUE)
                        {
                            CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                                              vendSpec.unVendorSpec.
                                              VendLocalRouting.u2MsgEleType);
                            u2ElemLength =
                                (UINT2) (u2ElemLength + sizeof (UINT2));
                            CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                                              vendSpec.unVendorSpec.
                                              VendLocalRouting.u2MsgEleLen);
                            u2ElemLength =
                                (UINT2) (u2ElemLength + sizeof (UINT2));
                            CAPWAP_PUT_4BYTE (pu1MsgElemValue,
                                              vendSpec.unVendorSpec.
                                              VendLocalRouting.vendorId);
                            u2ElemLength =
                                (UINT2) (u2ElemLength + sizeof (UINT4));
                            CAPWAP_PUT_1BYTE (pu1MsgElemValue,
                                              vendSpec.unVendorSpec.
                                              VendLocalRouting.
                                              u1WtpLocalRouting);
                            u2ElemLength =
                                (UINT2) (u2ElemLength + sizeof (UINT1));
                        }
                        *pu2MsgElemLen = u2ElemLength;

                        break;
#ifdef RFMGMT_WANTED
                    case VENDOR_NEIGH_CONFIG_TYPE:

                        vendSpec.u2MsgEleType = VENDOR_SPECIFIC_PAYLOAD;
                        vendSpec.u2MsgEleLen = NEIGH_CONFIG_VENDOR_MSG_LEN;
                        vendSpec.elementId = VENDOR_NEIGH_CONFIG_TYPE;
                        vendSpec.isOptional = OSIX_TRUE;

                        RfMgmtDB.unRfMgmtDB.
                            RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
                            bNeighborMsgPeriod = OSIX_TRUE;
                        RfMgmtDB.unRfMgmtDB.
                            RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
                            bChannelScanDuration = OSIX_TRUE;
                        RfMgmtDB.unRfMgmtDB.
                            RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
                            bAutoScanStatus = OSIX_TRUE;
                        RfMgmtDB.unRfMgmtDB.
                            RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
                            bNeighborAgingPeriod = OSIX_TRUE;
                        vendSpec.unVendorSpec.VendorNeighConfig.u2MsgEleType =
                            NEIGH_CONFIG_VENDOR_MSG;
                        vendSpec.unVendorSpec.VendorNeighConfig.u2MsgEleLen =
                            RF_NEIGH_CONFIG_VENDOR_MSG_ELM_LEN;
                        vendSpec.unVendorSpec.VendorNeighConfig.u4VendorId =
                            VENDOR_ID;
                        vendSpec.unVendorSpec.VendorNeighConfig.isOptional =
                            OSIX_TRUE;
#ifdef WTP_WANTED
                        RfMgmtDB.unRfMgmtDB.
                            RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
                            bRssiThreshold = OSIX_TRUE;
                        RfMgmtDB.unRfMgmtDB.
                            RfMgmtApConfigTable.RfMgmtAPConfigDB.
                            u4RadioIfIndex =
                            u1RadioID + SYS_DEF_MAX_ENET_INTERFACES;
#else

                        pRadioIfGetDB->RadioIfIsGetAllDB.bDot11RadioType =
                            OSIX_TRUE;
                        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                                      pRadioIfGetDB) !=
                            OSIX_SUCCESS)
                        {
                            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                        "Unable to get Radio Type\n");
                            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                          "Unable to get Radio Type\n"));
                            UtlShMemFreeWssWlanBuf ((UINT1 *)
                                                    pWssWlanMsgStruct);
                            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                            return OSIX_FAILURE;
                        }
                        /* To get Snr threshold */
                        RfMgmtDB.unRfMgmtDB.
                            RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
                            u4Dot11RadioType =
                            pRadioIfGetDB->RadioIfGetAllDB.u4Dot11RadioType;
                        RfMgmtDB.unRfMgmtDB.
                            RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                            bRssiThreshold = OSIX_TRUE;
                        if (WssIfProcessRfMgmtDBMsg
                            (RFMGMT_GET_AUTO_RF_ENTRY,
                             &RfMgmtDB) != RFMGMT_SUCCESS)
                        {
                            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                        "Getting RSSI Threshold failed\r\n");
                            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                          "Getting RSSI Threshold failed\r\n"));
                        }

                        RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                            RfMgmtAPConfigDB.u4RadioIfIndex =
                            pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
#endif

                        if (WssIfProcessRfMgmtDBMsg
                            (RFMGMT_GET_AP_CONFIG_ENTRY,
                             &RfMgmtDB) == RFMGMT_SUCCESS)
                        {
                            vendSpec.unVendorSpec.
                                VendorNeighConfig.VendorNeighParams[u1RadioID -
                                                                    1].
                                u2NeighborMsgPeriod =
                                RfMgmtDB.unRfMgmtDB.
                                RfMgmtApConfigTable.RfMgmtAPConfigDB.
                                u2NeighborMsgPeriod;
                            vendSpec.unVendorSpec.
                                VendorNeighConfig.VendorNeighParams[u1RadioID -
                                                                    1].
                                u2NeighborAgingPeriod =
                                RfMgmtDB.unRfMgmtDB.
                                RfMgmtApConfigTable.RfMgmtAPConfigDB.
                                u2NeighborAgingPeriod;
                            vendSpec.unVendorSpec.
                                VendorNeighConfig.VendorNeighParams[u1RadioID -
                                                                    1].
                                u2ChannelScanDuration =
                                RfMgmtDB.unRfMgmtDB.
                                RfMgmtApConfigTable.RfMgmtAPConfigDB.
                                u2ChannelScanDuration;
                            /*Since scanning should happen after all binding are done,
                             * we are sending auto scan send as disable irrespective of what is there in DB  */
                            vendSpec.unVendorSpec.
                                VendorNeighConfig.VendorNeighParams[u1RadioID -
                                                                    1].
                                u1AutoScanStatus = RFMGMT_AUTO_SCAN_DISABLE;
#ifdef WTP_WANTED
                            vendSpec.unVendorSpec.
                                VendorNeighConfig.VendorNeighParams[u1RadioID -
                                                                    1].
                                i2RssiThreshold =
                                RfMgmtDB.unRfMgmtDB.
                                RfMgmtApConfigTable.RfMgmtAPConfigDB.
                                i2RssiThreshold;
#else
                            vendSpec.unVendorSpec.
                                VendorNeighConfig.VendorNeighParams[u1RadioID -
                                                                    1].
                                i2RssiThreshold =
                                RfMgmtDB.unRfMgmtDB.
                                RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
                                i2RssiThreshold;
#endif
                            vendSpec.unVendorSpec.
                                VendorNeighConfig.VendorNeighParams[u1RadioID -
                                                                    1].
                                u1RadioId = u1RadioID;

                        }
                        if (vendSpec.unVendorSpec.VendorNeighConfig.
                            isOptional == OSIX_TRUE)
                        {
                            CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                                              vendSpec.unVendorSpec.
                                              VendorNeighConfig.u2MsgEleType);
                            u2ElemLength =
                                (UINT2) (u2ElemLength + sizeof (UINT2));
                            CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                                              vendSpec.unVendorSpec.
                                              VendorNeighConfig.u2MsgEleLen);
                            u2ElemLength =
                                (UINT2) (u2ElemLength + sizeof (UINT2));
                            CAPWAP_PUT_4BYTE (pu1MsgElemValue,
                                              vendSpec.unVendorSpec.
                                              VendorNeighConfig.u4VendorId);
                            u2ElemLength =
                                (UINT2) (u2ElemLength + sizeof (UINT4));

                            if (vendSpec.unVendorSpec.
                                VendorNeighConfig.VendorNeighParams[u1RadioID -
                                                                    1].
                                u1RadioId != 0)
                            {
                                CAPWAP_PUT_1BYTE (pu1MsgElemValue,
                                                  vendSpec.
                                                  unVendorSpec.
                                                  VendorNeighConfig.
                                                  VendorNeighParams[u1RadioID -
                                                                    1].
                                                  u1RadioId);
                                u2ElemLength =
                                    (UINT2) (u2ElemLength + sizeof (UINT1));
                                CAPWAP_PUT_1BYTE (pu1MsgElemValue,
                                                  vendSpec.unVendorSpec.
                                                  VendorNeighConfig.
                                                  VendorNeighParams[u1RadioID -
                                                                    1].
                                                  u1AutoScanStatus);
                                u2ElemLength =
                                    (UINT2) (u2ElemLength + sizeof (UINT1));
                                CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                                                  vendSpec.unVendorSpec.
                                                  VendorNeighConfig.
                                                  VendorNeighParams[u1RadioID -
                                                                    1].
                                                  u2NeighborMsgPeriod);
                                u2ElemLength =
                                    (UINT2) (u2ElemLength + sizeof (UINT2));
                                CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                                                  vendSpec.unVendorSpec.
                                                  VendorNeighConfig.
                                                  VendorNeighParams[u1RadioID -
                                                                    1].
                                                  u2ChannelScanDuration);
                                u2ElemLength =
                                    (UINT2) (u2ElemLength + sizeof (UINT2));
                                CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                                                  vendSpec.unVendorSpec.
                                                  VendorNeighConfig.
                                                  VendorNeighParams[u1RadioID -
                                                                    1].
                                                  u2NeighborAgingPeriod);
                                u2ElemLength =
                                    (UINT2) (u2ElemLength + sizeof (UINT2));
                                CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                                                  vendSpec.unVendorSpec.
                                                  VendorNeighConfig.
                                                  VendorNeighParams[u1RadioID -
                                                                    1].
                                                  i2RssiThreshold);
                                u2ElemLength =
                                    (UINT2) (u2ElemLength + sizeof (UINT2));
                            }
                        }
                        *pu2MsgElemLen = u2ElemLength;
                        break;
                    case VENDOR_CLIENT_CONFIG_TYPE:
                        vendSpec.u2MsgEleType = VENDOR_SPECIFIC_PAYLOAD;
                        vendSpec.u2MsgEleLen = CLIENT_CONFIG_VENDOR_MSG_LEN;
                        vendSpec.elementId = VENDOR_CLIENT_CONFIG_TYPE;
                        vendSpec.isOptional = OSIX_TRUE;

                        RfMgmtDB.unRfMgmtDB.
                            RfMgmtClientConfigTable.RfMgmtClientConfigIsSetDB.
                            bSNRScanPeriod = OSIX_TRUE;
                        RfMgmtDB.unRfMgmtDB.
                            RfMgmtClientConfigTable.RfMgmtClientConfigIsSetDB.
                            bSNRScanStatus = OSIX_TRUE;

                        vendSpec.unVendorSpec.VendorClientConfig.u2MsgEleType
                            = CLIENT_CONFIG_VENDOR_MSG;
                        vendSpec.unVendorSpec.VendorClientConfig.u2MsgEleLen
                            = RF_CLIENT_CONFIG_VENDOR_MSG_ELM_LEN;
                        vendSpec.unVendorSpec.VendorClientConfig.u4VendorId
                            = VENDOR_ID;
                        vendSpec.unVendorSpec.VendorClientConfig.isOptional
                            = OSIX_TRUE;
#ifdef WTP_WANTED
                        RfMgmtDB.unRfMgmtDB.
                            RfMgmtClientConfigTable.RfMgmtClientConfigIsSetDB.
                            bSNRThreshold = OSIX_TRUE;
                        RfMgmtDB.unRfMgmtDB.
                            RfMgmtClientConfigTable.RfMgmtClientConfigDB.
                            u4RadioIfIndex =
                            u1RadioID + SYS_DEF_MAX_ENET_INTERFACES;
#else
                        pRadioIfGetDB->RadioIfIsGetAllDB.bDot11RadioType =
                            OSIX_TRUE;
                        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                                      pRadioIfGetDB) !=
                            OSIX_SUCCESS)
                        {
                            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                        "Unable to get Radio Type\n");
                            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                          "Unable to get Radio Type\n"));

                            UtlShMemFreeWssWlanBuf ((UINT1 *)
                                                    pWssWlanMsgStruct);
                            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                            return OSIX_FAILURE;
                        }
                        RfMgmtDB.unRfMgmtDB.
                            RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
                            u4Dot11RadioType =
                            pRadioIfGetDB->RadioIfGetAllDB.u4Dot11RadioType;
                        RfMgmtDB.unRfMgmtDB.
                            RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                            bSNRThreshold = OSIX_TRUE;
                        if (WssIfProcessRfMgmtDBMsg
                            (RFMGMT_GET_AUTO_RF_ENTRY,
                             &RfMgmtDB) != RFMGMT_SUCCESS)
                        {
                            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                        "Getting SNR Threshold failed\r\n");
                            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                          "Getting SNR Threshold failed\r\n"));
                        }

                        RfMgmtDB.unRfMgmtDB.
                            RfMgmtClientConfigTable.RfMgmtClientConfigDB.
                            u4RadioIfIndex =
                            pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
#endif

                        if (WssIfProcessRfMgmtDBMsg
                            (RFMGMT_GET_CLIENT_CONFIG_ENTRY,
                             &RfMgmtDB) == RFMGMT_SUCCESS)
                        {
                            vendSpec.unVendorSpec.
                                VendorClientConfig.VendorClientParams[u1RadioID
                                                                      -
                                                                      1].
                                u2SNRScanPeriod =
                                RfMgmtDB.unRfMgmtDB.
                                RfMgmtClientConfigTable.RfMgmtClientConfigDB.
                                u2SNRScanPeriod;
                            vendSpec.unVendorSpec.
                                VendorClientConfig.VendorClientParams[u1RadioID
                                                                      -
                                                                      1].
                                u1SNRScanStatus =
                                RfMgmtDB.unRfMgmtDB.
                                RfMgmtClientConfigTable.RfMgmtClientConfigDB.
                                u1SNRScanStatus;
#ifdef WTP_WANTED
                            vendSpec.unVendorSpec.
                                VendorClientConfig.VendorClientParams[u1RadioID
                                                                      -
                                                                      1].
                                i2SNRThreshold =
                                RfMgmtDB.unRfMgmtDB.
                                RfMgmtClientConfigTable.RfMgmtClientConfigDB.
                                i2SNRThreshold;
                            vendSpec.unVendorSpec.
                                VendorClientConfig.VendorClientParams[u1RadioID
                                                                      -
                                                                      1].
                                u1WlanId =
                                RfMgmtDB.unRfMgmtDB.
                                RfMgmtClientConfigTable.RfMgmtClientConfigDB.
                                u1WlanId;
                            vendSpec.unVendorSpec.
                                VendorClientConfig.VendorClientParams[u1RadioID
                                                                      -
                                                                      1].
                                u1BssidScanStatus[u1WlanId] =
                                RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
                                RfMgmtClientConfigDB.
                                u1BssidScanStatus[u1WlanId];
#else
                            vendSpec.unVendorSpec.
                                VendorClientConfig.VendorClientParams[u1RadioID
                                                                      -
                                                                      1].
                                i2SNRThreshold =
                                RfMgmtDB.unRfMgmtDB.
                                RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
                                i2SNRThreshold;

                            vendSpec.unVendorSpec.
                                VendorClientConfig.VendorClientParams[u1RadioID
                                                                      -
                                                                      1].
                                u1BssidScanStatus[u1WlanId] =
                                RfMgmtDB.unRfMgmtDB.RfMgmtBssidScanTable.
                                RfMgmtBssidScanDB.u1AutoScanStatus;
                            vendSpec.unVendorSpec.VendorClientConfig.
                                VendorClientParams[u1RadioID - 1].u1WlanId =
                                RfMgmtDB.unRfMgmtDB.RfMgmtBssidScanTable.
                                RfMgmtBssidScanDB.u1WlanID;
#endif
                            vendSpec.unVendorSpec.
                                VendorClientConfig.VendorClientParams[u1RadioID
                                                                      -
                                                                      1].
                                u1RadioId = u1RadioID;
                        }
                        if (vendSpec.unVendorSpec.VendorClientConfig.isOptional
                            == OSIX_TRUE)
                        {
                            CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                                              vendSpec.unVendorSpec.
                                              VendorClientConfig.u2MsgEleType);
                            u2ElemLength =
                                (UINT2) (u2ElemLength + sizeof (UINT2));
                            CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                                              vendSpec.unVendorSpec.
                                              VendorClientConfig.u2MsgEleLen);
                            u2ElemLength =
                                (UINT2) (u2ElemLength + sizeof (UINT2));
                            CAPWAP_PUT_4BYTE (pu1MsgElemValue,
                                              vendSpec.unVendorSpec.
                                              VendorClientConfig.u4VendorId);
                            u2ElemLength =
                                (UINT2) (u2ElemLength + sizeof (UINT4));

                            if (vendSpec.unVendorSpec.
                                VendorClientConfig.VendorClientParams[u1RadioID
                                                                      -
                                                                      1].
                                u1RadioId != 0)
                            {
                                CAPWAP_PUT_1BYTE (pu1MsgElemValue,
                                                  vendSpec.
                                                  unVendorSpec.
                                                  VendorClientConfig.
                                                  VendorClientParams[u1RadioID -
                                                                     1].
                                                  u1RadioId);
                                u2ElemLength =
                                    (UINT2) (u2ElemLength + sizeof (UINT1));
                                CAPWAP_PUT_1BYTE (pu1MsgElemValue,
                                                  vendSpec.unVendorSpec.
                                                  VendorClientConfig.
                                                  VendorClientParams[u1RadioID -
                                                                     1].
                                                  u1SNRScanStatus);
                                u2ElemLength =
                                    (UINT2) (u2ElemLength + sizeof (UINT1));
                                CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                                                  vendSpec.unVendorSpec.
                                                  VendorClientConfig.
                                                  VendorClientParams[u1RadioID -
                                                                     1].
                                                  u2SNRScanPeriod);
                                u2ElemLength =
                                    (UINT2) (u2ElemLength + sizeof (UINT2));
                                CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                                                  vendSpec.unVendorSpec.
                                                  VendorClientConfig.
                                                  VendorClientParams[u1RadioID -
                                                                     1].
                                                  i2SNRThreshold);
                                u2ElemLength =
                                    (UINT2) (u2ElemLength + sizeof (UINT2));
                                CAPWAP_PUT_1BYTE (pu1MsgElemValue,
                                                  vendSpec.unVendorSpec.
                                                  VendorClientConfig.
                                                  VendorClientParams[u1RadioID -
                                                                     1].
                                                  u1BssidScanStatus[u1WlanId]);
                                u2ElemLength =
                                    (UINT2) (u2ElemLength + sizeof (UINT1));
                                CAPWAP_PUT_1BYTE (pu1MsgElemValue,
                                                  vendSpec.unVendorSpec.
                                                  VendorClientConfig.
                                                  VendorClientParams[u1RadioID -
                                                                     1].
                                                  u1WlanId);
                                u2ElemLength =
                                    (UINT2) (u2ElemLength + sizeof (UINT1));
                            }
                        }
                        *pu2MsgElemLen = u2ElemLength;

                        break;

                    case VENDOR_CH_SWITCH_STATUS_TYPE:
                        vendSpec.u2MsgEleType = VENDOR_SPECIFIC_PAYLOAD;
                        vendSpec.u2MsgEleLen = CH_SWITCH_STATUS_VENDOR_MSG_LEN;
                        vendSpec.elementId = VENDOR_CH_SWITCH_STATUS_TYPE;
                        vendSpec.isOptional = OSIX_TRUE;
                        vendSpec.unVendorSpec.VendorChSwitchStatus.u2MsgEleType
                            = CH_SWITCH_STATUS_VENDOR_MSG;
                        vendSpec.unVendorSpec.VendorChSwitchStatus.u2MsgEleLen
                            = RF_CH_SWITCH_STATUS_VENDOR_MSG_ELM_LEN;
                        vendSpec.unVendorSpec.VendorChSwitchStatus.u4VendorId
                            = VENDOR_ID;
                        vendSpec.unVendorSpec.VendorChSwitchStatus.isOptional
                            = OSIX_TRUE;

#ifdef WTP_WANTED
                        RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                            RfMgmtAPConfigIsSetDB.bChSwitchStatus = OSIX_TRUE;

                        RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                            RfMgmtAPConfigDB.u4RadioIfIndex =
                            u1RadioID + SYS_DEF_MAX_ENET_INTERFACES;

                        if (WssIfProcessRfMgmtDBMsg
                            (RFMGMT_GET_AP_CONFIG_ENTRY,
                             &RfMgmtDB) == RFMGMT_SUCCESS)
                        {
                            vendSpec.unVendorSpec.
                                VendorChSwitchStatus.VendorChSwitchParams
                                [u1RadioID - 1].u1ChSwitchStatus =
                                RfMgmtDB.unRfMgmtDB.
                                RfMgmtApConfigTable.RfMgmtAPConfigDB.
                                u1ChSwitchStatus;
                            vendSpec.unVendorSpec.
                                VendorChSwitchStatus.VendorChSwitchParams
                                [u1RadioID - 1].u1RadioId = u1RadioID;
                        }
#else
                        RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                            RfMgmtAPConfigDB.u4RadioIfIndex =
                            pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
                        vendSpec.unVendorSpec.
                            VendorChSwitchStatus.VendorChSwitchParams[u1RadioID
                                                                      -
                                                                      1].
                            u1ChSwitchStatus =
                            (UINT1) gu4ChannelSwitchMsgStatus;
                        vendSpec.unVendorSpec.
                            VendorChSwitchStatus.VendorChSwitchParams[u1RadioID
                                                                      -
                                                                      1].
                            u1RadioId = u1RadioID;

#endif
                        /*Assemble */
                        if (vendSpec.unVendorSpec.VendorChSwitchStatus.
                            isOptional == OSIX_TRUE)
                        {
                            CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                                              vendSpec.unVendorSpec.
                                              VendorChSwitchStatus.
                                              u2MsgEleType);
                            u2ElemLength =
                                (UINT2) (u2ElemLength + sizeof (UINT2));
                            CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                                              vendSpec.unVendorSpec.
                                              VendorChSwitchStatus.u2MsgEleLen);
                            u2ElemLength =
                                (UINT2) (u2ElemLength + sizeof (UINT2));
                            CAPWAP_PUT_4BYTE (pu1MsgElemValue,
                                              vendSpec.unVendorSpec.
                                              VendorChSwitchStatus.u4VendorId);
                            u2ElemLength =
                                (UINT2) (u2ElemLength + sizeof (UINT4));
                            if (vendSpec.unVendorSpec.VendorChSwitchStatus.
                                VendorChSwitchParams[u1RadioID - 1].u1RadioId !=
                                0)
                            {
                                CAPWAP_PUT_1BYTE (pu1MsgElemValue,
                                                  vendSpec.
                                                  unVendorSpec.
                                                  VendorChSwitchStatus.
                                                  VendorChSwitchParams[u1RadioID
                                                                       -
                                                                       1].
                                                  u1RadioId);
                                u2ElemLength =
                                    (UINT2) (u2ElemLength + sizeof (UINT1));
                                CAPWAP_PUT_1BYTE (pu1MsgElemValue,
                                                  vendSpec.unVendorSpec.
                                                  VendorChSwitchStatus.
                                                  VendorChSwitchParams[u1RadioID
                                                                       -
                                                                       1].
                                                  u1ChSwitchStatus);
                                u2ElemLength =
                                    (UINT2) (u2ElemLength + sizeof (UINT1));
                            }
                        }
                        *pu2MsgElemLen = u2ElemLength;
                        break;
                    case VENDOR_CH_SCANNED_LIST:
#ifdef WTP_WANTED
                        RfMgmtDB.unRfMgmtDB.RfMgmtChannelAllowedScanTable.
                            RfMgmtChannelAllowedScanDB.u4RadioIfIndex =
                            u1RadioID + SYS_DEF_MAX_ENET_INTERFACES;
                        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
                            u1RadioID + SYS_DEF_MAX_ENET_INTERFACES;
                        /* GETTING CONFIGURED RADIO TYPE for the particular Radio from RADIO db */
                        RadioIfGetDB.RadioIfIsGetAllDB.bDot11RadioType =
                            OSIX_TRUE;
                        if (WssIfProcessRadioIfDBMsg
                            (WSS_GET_RADIO_IF_DB,
                             (&RadioIfGetDB)) != OSIX_SUCCESS)
                        {
                            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                        "RBTREE get of RadioIf DB failed \r\n");
                            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                          "RBTREE get of RadioIf DB failed \r\n"));
                            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                             RadioIfGetAllDB.
                                                             pu1AntennaSelection);
                            return OSIX_FAILURE;
                        }
                        if ((RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                             RADIO_TYPE_A)
                            || (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                                RADIO_TYPE_AN)
                            || (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                                RADIO_TYPE_AC))
                        {

                            RfMgmtDB.unRfMgmtDB.RfMgmtChannelAllowedScanTable.
                                RfMgmtChannelAllowedScanDB.u4RadioType =
                                RADIO_TYPE_A;
                        }
                        else
                        {
                            RfMgmtDB.unRfMgmtDB.RfMgmtChannelAllowedScanTable.
                                RfMgmtChannelAllowedScanDB.u4RadioType =
                                RADIO_TYPE_B;
                        }
                        /*Collecting the scanned results of both the lists from RF db */
/*get_next entry*/
#if 1
                        UINT4               u4RadioType =
                            RfMgmtDB.unRfMgmtDB.RfMgmtChannelAllowedScanTable.
                            RfMgmtChannelAllowedScanDB.u4RadioType;
                        UINT4               u4RadioIfIndex =
                            RfMgmtDB.unRfMgmtDB.RfMgmtChannelAllowedScanTable.
                            RfMgmtChannelAllowedScanDB.u4RadioIfIndex;
#endif
                        while (WssIfProcessRfMgmtDBMsg
                               (RFMGMT_GET_NEXT_SCAN_ALLOWED_ENTRY,
                                &RfMgmtDB) == RFMGMT_SUCCESS)
                        {
                            if ((u4RadioType ==
                                 RfMgmtDB.unRfMgmtDB.
                                 RfMgmtChannelAllowedScanTable.
                                 RfMgmtChannelAllowedScanDB.u4RadioType)
                                && (u4RadioIfIndex ==
                                    RfMgmtDB.unRfMgmtDB.
                                    RfMgmtChannelAllowedScanTable.
                                    RfMgmtChannelAllowedScanDB.u4RadioIfIndex))
                            {
                                vendSpec.unVendorSpec.
                                    VendorChAllowedList.
                                    au1ChannelList[u1Index3++] =
                                    RfMgmtDB.unRfMgmtDB.
                                    RfMgmtChannelAllowedScanTable.
                                    RfMgmtChannelAllowedScanDB.u1Channel;
                                vendSpec.unVendorSpec.VendorChAllowedList.
                                    au1ChannelList[u1Index3++] =
                                    RfMgmtDB.unRfMgmtDB.
                                    RfMgmtChannelAllowedScanTable.
                                    RfMgmtChannelAllowedScanDB.
                                    u1SecChannelOffset;
                            }
                        }
              /*************************************************************/
                        /*      IDENTIFYING CONFLICT using HT20/40 Rule              */
                        /*                                                           */
                        /* For each iteration ,It takes two channels and offset      */
                        /*  for comparison from scanned list                         */
                        /*                                                           */
                        /* 1) If offset is above                                     */
                        /*                                                           */
                        /*     check if u1PrimaryChannel + 4== u1SecondaryChannel    */
                        /*              then add both channels to conflictlist       */
                        /*                                                           */
                        /* 2) If offset is below                                     */
                        /*                                                           */
                        /*     check if u1PrimaryChannel - 4== u1SecondaryChannel    */
                        /*              then add both channels to conflictlist       */
                        /*                                                           */
                        /* At some cases                                             */
                        /*     check if u1PrimaryChannel == u1SecondaryChannel       */
                        /*              then add only channel to conflictlist        */
                        /*                                                           */
              /*************************************************************/

                        for (u1ChannIndex = 0; u1ChannIndex < u1Index3;
                             u1ChannIndex = u1ChannIndex + 4)
                        {
                            u1PrimaryChannel =
                                vendSpec.unVendorSpec.VendorChAllowedList.
                                au1ChannelList[u1ChannIndex];
                            if (u1PrimaryChannel == 0)
                            {
                                break;
                            }
                            u1PrimaryOffset =
                                vendSpec.unVendorSpec.VendorChAllowedList.
                                au1ChannelList[u1ChannIndex + 1];
                            if (u1PrimaryOffset == 1)
                            {
                                /*above */
                                /*Arrayout of bound */
                                if (u1ChannIndex + 2 <= u1Index3)
                                {
                                    u1SecondaryChannel =
                                        vendSpec.unVendorSpec.
                                        VendorChAllowedList.
                                        au1ChannelList[u1ChannIndex + 2];
                                    u1SecondaryOffset =
                                        vendSpec.unVendorSpec.
                                        VendorChAllowedList.
                                        au1ChannelList[u1ChannIndex + 3];
                                    if (u1PrimaryChannel != u1SecondaryChannel)
                                    {
                                        if ((u1PrimaryChannel + 4) ==
                                            u1SecondaryChannel)
                                        {
                                            if (u1SecondaryOffset !=
                                                u1PrimaryOffset)
                                            {
                                                if (OSIX_FAILURE ==
                                                    CapwapUtilSetConflictList
                                                    (u1PrimaryChannel,
                                                     u1ConflictCounter++,
                                                     u1ConflictList))
                                                {
                                                    u1ConflictCounter--;
                                                }
                                                if (OSIX_FAILURE ==
                                                    CapwapUtilSetConflictList
                                                    (u1SecondaryChannel,
                                                     u1ConflictCounter++,
                                                     u1ConflictList))
                                                {
                                                    u1ConflictCounter--;
                                                }
                                            }
                                        }
                                        if (u1ChannIndex + 4 <= u1Index3)
                                        {
                                            if (u1SecondaryChannel ==
                                                vendSpec.unVendorSpec.
                                                VendorChAllowedList.
                                                au1ChannelList[u1ChannIndex +
                                                               4])
                                            {
                                                if (OSIX_FAILURE ==
                                                    CapwapUtilSetConflictList
                                                    (u1SecondaryChannel,
                                                     u1ConflictCounter++,
                                                     u1ConflictList))
                                                {
                                                    u1ConflictCounter--;
                                                }

                                            }
                                        }
                                    }
                                    else
                                    {

                                        if (OSIX_FAILURE ==
                                            CapwapUtilSetConflictList
                                            (u1PrimaryChannel,
                                             u1ConflictCounter++,
                                             u1ConflictList))
                                        {
                                            u1ConflictCounter--;
                                        }

                                    }
                                }
                            }
                            else if (u1PrimaryOffset == 3)
                            {
                                /*below */
                                /*Arrayout of bound */
                                if (u1ChannIndex - 2 >= 0)
                                {
                                    u1SecondaryChannel =
                                        vendSpec.unVendorSpec.
                                        VendorChAllowedList.
                                        au1ChannelList[u1ChannIndex - 2];
                                    u1SecondaryOffset =
                                        vendSpec.unVendorSpec.
                                        VendorChAllowedList.
                                        au1ChannelList[u1ChannIndex - 1];
                                    if ((u1PrimaryChannel - 4) ==
                                        u1SecondaryChannel)
                                    {
                                        if (u1SecondaryOffset !=
                                            u1PrimaryOffset)
                                        {
                                            if (OSIX_FAILURE ==
                                                CapwapUtilSetConflictList
                                                (u1PrimaryChannel,
                                                 u1ConflictCounter++,
                                                 u1ConflictList))
                                            {
                                                u1ConflictCounter--;
                                            }
                                            if (OSIX_FAILURE ==
                                                CapwapUtilSetConflictList
                                                (u1SecondaryChannel,
                                                 u1ConflictCounter++,
                                                 u1ConflictList))
                                            {
                                                u1ConflictCounter--;
                                            }

                                        }
                                    }

                                }
                            }
                        }
                        vendSpec.u2MsgEleType = VENDOR_SPECIFIC_PAYLOAD;
                        vendSpec.u2MsgEleLen = (UINT2) (13 + u1ConflictCounter);
                        vendSpec.elementId = VENDOR_CH_SCANNED_LIST;
                        vendSpec.isOptional = OSIX_TRUE;
                        vendSpec.unVendorSpec.VendorChAllowedList.u2MsgEleType
                            = VENDOR_CH_SCANNED_LIST_MSG;
                        vendSpec.unVendorSpec.VendorChAllowedList.u2MsgEleLen
                            = (UINT2) (9 + u1ConflictCounter);
                        vendSpec.unVendorSpec.VendorChAllowedList.u4VendorId
                            = VENDOR_ID;
                        vendSpec.unVendorSpec.VendorChAllowedList.isOptional
                            = OSIX_TRUE;
                        vendSpec.unVendorSpec.
                            VendorChAllowedList.u1RadioId = u1RadioID;
                        /*Assemble */
                        if (vendSpec.unVendorSpec.VendorChAllowedList.
                            isOptional == OSIX_TRUE)
                        {
                            CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                                              vendSpec.unVendorSpec.
                                              VendorChAllowedList.u2MsgEleType);
                            u2ElemLength =
                                (UINT2) (u2ElemLength + sizeof (UINT2));
                            CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                                              vendSpec.unVendorSpec.
                                              VendorChAllowedList.u2MsgEleLen);
                            u2ElemLength =
                                (UINT2) (u2ElemLength + sizeof (UINT2));
                            CAPWAP_PUT_4BYTE (pu1MsgElemValue,
                                              vendSpec.unVendorSpec.
                                              VendorChAllowedList.u4VendorId);
                            u2ElemLength =
                                (UINT2) (u2ElemLength + sizeof (UINT4));

                            CAPWAP_PUT_1BYTE (pu1MsgElemValue,
                                              vendSpec.unVendorSpec.
                                              VendorChAllowedList.u1RadioId);
                            u2ElemLength =
                                (UINT2) (u2ElemLength + sizeof (UINT1));

                            u1Index = 0;
                            for (u1Index = 0; u1Index < u1ConflictCounter;
                                 u1Index++)
                            {
                                CAPWAP_PUT_1BYTE (pu1MsgElemValue,
                                                  u1ConflictList[u1Index]);
                                u2ElemLength =
                                    (UINT2) (u2ElemLength + sizeof (UINT1));
                            }
                        }
                        *pu2MsgElemLen = u2ElemLength;
#endif
                        break;
#ifdef ROGUEAP_WANTED
                    case VENDOR_ROUGE_AP:
#ifdef WLC_WANTED
                        vendSpec.u2MsgEleType = VENDOR_SPECIFIC_PAYLOAD;
                        vendSpec.u2MsgEleLen = VENDOR_ROUGE_AP_MSG_LEN;
                        vendSpec.elementId = VENDOR_ID;
                        vendSpec.isOptional = OSIX_TRUE;

                        vendSpec.unVendorSpec.RougeAp.u2MsgEleType
                            = ROGUE_CONFIG_VENDOR_MSG;
                        vendSpec.unVendorSpec.RougeAp.u2MsgEleLen = 23;
                        vendSpec.unVendorSpec.RougeAp.u4VendorId = VENDOR_ID;
                        vendSpec.unVendorSpec.RougeAp.isOptional = OSIX_TRUE;
                        /*Assemble */
                        if (vendSpec.unVendorSpec.RougeAp.
                            isOptional == OSIX_TRUE)
                        {
                            CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                                              vendSpec.unVendorSpec.
                                              RougeAp.u2MsgEleType);
                            u2ElemLength =
                                (UINT2) (u2ElemLength + sizeof (UINT2));
                            CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                                              vendSpec.unVendorSpec.
                                              RougeAp.u2MsgEleLen);
                            u2ElemLength =
                                (UINT2) (u2ElemLength + sizeof (UINT2));
                            CAPWAP_PUT_4BYTE (pu1MsgElemValue,
                                              vendSpec.unVendorSpec.
                                              RougeAp.u4VendorId);
                            u2ElemLength =
                                (UINT2) (u2ElemLength + sizeof (UINT4));

                            MEMSET (&RfName, 0,
                                    sizeof (tSNMP_OCTET_STRING_TYPE));
                            RfName.pu1_OctetList = au1RogueRfName;

                            if (nmhGetFsRfGroupName (&RfName) != SNMP_FAILURE)
                            {
                                if (strlen ((const char *) au1RogueRfName) != 0)
                                {
                                    MEMCPY (vendSpec.unVendorSpec.RougeAp.
                                            au1RfGroupName, au1RogueRfName,
                                            CAPWAP_RF_GROUP_NAME_SIZE);
                                }
                                else
                                {
                                    MEMCPY (vendSpec.unVendorSpec.RougeAp.
                                            au1RfGroupName, "Aricent123",
                                            strlen ("Aricent123"));
                                }
                            }
                            CAPWAP_PUT_NBYTE (pu1MsgElemValue,
                                              vendSpec.unVendorSpec.
                                              RougeAp.au1RfGroupName,
                                              CAPWAP_RF_GROUP_NAME_SIZE);
                            u2ElemLength =
                                (UINT2) (u2ElemLength +
                                         CAPWAP_RF_GROUP_NAME_SIZE);

                        }
                        *pu2MsgElemLen = u2ElemLength;
#endif
                        break;

#endif

                    case VENDOR_SPECT_MGMT_TPC_TYPE:
                        vendSpec.u2MsgEleType = VENDOR_SPECIFIC_PAYLOAD;
                        vendSpec.u2MsgEleLen = TPC_SPEC_MGMT_VENDOR_MSG_LEN;
                        vendSpec.elementId = VENDOR_SPECT_MGMT_TPC_TYPE;
                        vendSpec.isOptional = OSIX_TRUE;

                        vendSpec.unVendorSpec.VendorTpcSpectMgmt.u2MsgEleType
                            = VENDOR_SPECT_MGMT_TPC_MSG;
                        vendSpec.unVendorSpec.VendorTpcSpectMgmt.u2MsgEleLen
                            = TPC_SPECT_MGMT_VENDOR_MSG_ELM_LEN;
                        vendSpec.unVendorSpec.VendorTpcSpectMgmt.u4VendorId
                            = VENDOR_ID;
                        vendSpec.unVendorSpec.VendorTpcSpectMgmt.isOptional
                            = OSIX_TRUE;

                        RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                            RfMgmtAPConfigIsSetDB.bRfMgmt11hTpcRequestInterval
                            = OSIX_TRUE;

#ifdef WTP_WANTED
                        RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                            RfMgmtAPConfigDB.u4RadioIfIndex =
                            u1RadioID + SYS_DEF_MAX_ENET_INTERFACES;

                        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
                            u1RadioID + SYS_DEF_MAX_ENET_INTERFACES;

                        RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                            RfMgmtAPConfigIsSetDB.bRfMgmt11hTpcStatus
                            = OSIX_TRUE;
#else
                        RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                            RfMgmtAPConfigDB.u4RadioIfIndex =
                            pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
                        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
                            pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
#endif
                        RadioIfGetDB.RadioIfIsGetAllDB.bDot11RadioType =
                            OSIX_TRUE;

                        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                                      &RadioIfGetDB) !=
                            OSIX_SUCCESS)
                        {
                            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                            UtlShMemFreeWssWlanBuf ((UINT1 *)
                                                    pWssWlanMsgStruct);
                            return OSIX_FAILURE;
                        }

                        if ((RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType
                             != RADIO_TYPE_A) &&
                            (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType
                             != RADIO_TYPE_AN) &&
                            (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType
                             != RADIO_TYPE_AC))
                        {
                            break;
                        }

                        if (WssIfProcessRfMgmtDBMsg
                            (RFMGMT_GET_AP_CONFIG_ENTRY,
                             &RfMgmtDB) == RFMGMT_SUCCESS)
                        {
                            vendSpec.unVendorSpec.
                                VendorTpcSpectMgmt.VendorTpcSpectMgmtParams
                                [u1RadioID - 1].u2TpcRequestInterval =
                                RfMgmtDB.unRfMgmtDB.
                                RfMgmtApConfigTable.RfMgmtAPConfigDB.
                                u2RfMgmt11hTpcRequestInterval;
                            vendSpec.unVendorSpec.
                                VendorTpcSpectMgmt.VendorTpcSpectMgmtParams
                                [u1RadioID - 1].u1RadioId = u1RadioID;
#ifdef WTP_WANTED
                            vendSpec.unVendorSpec.
                                VendorTpcSpectMgmt.VendorTpcSpectMgmtParams
                                [u1RadioID - 1].u111hTpcStatus =
                                RfMgmtDB.unRfMgmtDB.
                                RfMgmtApConfigTable.RfMgmtAPConfigDB.
                                u1RfMgmt11hTpcStatus;
#endif
                        }

#ifdef WLC_WANTED
                        MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
                        RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
                            RfMgmtAutoRfIsSetDB.bRfMgmt11hTpcStatus = OSIX_TRUE;
                        RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
                            RfMgmtAutoRfProfileDB.u4Dot11RadioType =
                            RADIO_TYPE_A;

                        if (WssIfProcessRfMgmtDBMsg
                            (RFMGMT_GET_AUTO_RF_ENTRY,
                             &RfMgmtDB) == RFMGMT_SUCCESS)
                        {
                            vendSpec.unVendorSpec.
                                VendorTpcSpectMgmt.VendorTpcSpectMgmtParams
                                [u1RadioID - 1].u111hTpcStatus =
                                RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
                                RfMgmtAutoRfProfileDB.u1RfMgmt11hTpcStatus;
                        }

#endif
                        /*Assemble */
                        if (vendSpec.unVendorSpec.VendorTpcSpectMgmt.
                            isOptional == OSIX_TRUE)
                        {
                            CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                                              vendSpec.unVendorSpec.
                                              VendorTpcSpectMgmt.u2MsgEleType);
                            u2ElemLength =
                                (UINT2) (u2ElemLength + sizeof (UINT2));
                            CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                                              vendSpec.unVendorSpec.
                                              VendorTpcSpectMgmt.u2MsgEleLen);
                            u2ElemLength =
                                (UINT2) (u2ElemLength + sizeof (UINT2));
                            CAPWAP_PUT_4BYTE (pu1MsgElemValue,
                                              vendSpec.unVendorSpec.
                                              VendorTpcSpectMgmt.u4VendorId);
                            u2ElemLength =
                                (UINT2) (u2ElemLength + sizeof (UINT4));
                            if (vendSpec.unVendorSpec.
                                VendorTpcSpectMgmt.VendorTpcSpectMgmtParams
                                [u1RadioID - 1].u1RadioId != 0)
                            {
                                CAPWAP_PUT_1BYTE (pu1MsgElemValue,
                                                  vendSpec.
                                                  unVendorSpec.
                                                  VendorTpcSpectMgmt.
                                                  VendorTpcSpectMgmtParams
                                                  [u1RadioID - 1].u1RadioId);
                                u2ElemLength =
                                    (UINT2) (u2ElemLength + sizeof (UINT1));
                                CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                                                  vendSpec.unVendorSpec.
                                                  VendorTpcSpectMgmt.
                                                  VendorTpcSpectMgmtParams
                                                  [u1RadioID -
                                                   1].u2TpcRequestInterval);
                                u2ElemLength =
                                    (UINT2) (u2ElemLength + sizeof (UINT2));
                                CAPWAP_PUT_1BYTE (pu1MsgElemValue,
                                                  vendSpec.unVendorSpec.
                                                  VendorTpcSpectMgmt.
                                                  VendorTpcSpectMgmtParams
                                                  [u1RadioID -
                                                   1].u111hTpcStatus);
                                u2ElemLength =
                                    (UINT2) (u2ElemLength + sizeof (UINT1));

                            }
                        }
                        *pu2MsgElemLen = u2ElemLength;
                        break;

                    case VENDOR_SPECTRUM_MGMT_DFS_TYPE:
                        vendSpec.u2MsgEleType = VENDOR_SPECIFIC_PAYLOAD;
                        vendSpec.u2MsgEleLen = SPEC_MGMT_VENDOR_DFS_MSG_LEN;
                        vendSpec.elementId = VENDOR_SPECTRUM_MGMT_DFS_TYPE;
                        vendSpec.isOptional = OSIX_TRUE;

                        vendSpec.unVendorSpec.VendorDfsParams.u2MsgEleType
                            = VENDOR_SPECTRUM_MGMT_DFS_MSG;
                        vendSpec.unVendorSpec.VendorDfsParams.u2MsgEleLen
                            = SPECT_MGMT_VENDOR_DFS_MSG_ELM_LEN;
                        vendSpec.unVendorSpec.VendorDfsParams.u4VendorId
                            = VENDOR_ID;
                        vendSpec.unVendorSpec.VendorDfsParams.isOptional
                            = OSIX_TRUE;

                        RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                            RfMgmtAPConfigIsSetDB.bRfMgmt11hDfsQuietInterval
                            = OSIX_TRUE;
                        RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                            RfMgmtAPConfigIsSetDB.bRfMgmt11hDfsQuietPeriod
                            = OSIX_TRUE;
                        RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                            RfMgmtAPConfigIsSetDB.
                            bRfMgmt11hDfsMeasurementInterval = OSIX_TRUE;
                        RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                            RfMgmtAPConfigIsSetDB.
                            bRfMgmt11hDfsChannelSwitchStatus = OSIX_TRUE;
#ifdef WTP_WANTED
                        RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                            RfMgmtAPConfigDB.u4RadioIfIndex =
                            u1RadioID + SYS_DEF_MAX_ENET_INTERFACES;

                        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
                            u1RadioID + SYS_DEF_MAX_ENET_INTERFACES;

                        RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                            RfMgmtAPConfigIsSetDB.bRfMgmt11hDfsStatus
                            = OSIX_TRUE;
#else
                        RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                            RfMgmtAPConfigDB.u4RadioIfIndex =
                            pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
                        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
                            pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
#endif
                        RadioIfGetDB.RadioIfIsGetAllDB.bDot11RadioType =
                            OSIX_TRUE;

                        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                                      &RadioIfGetDB) !=
                            OSIX_SUCCESS)
                        {
                            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                            UtlShMemFreeWssWlanBuf ((UINT1 *)
                                                    pWssWlanMsgStruct);
                            return OSIX_FAILURE;
                        }

                        if ((RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType
                             != RADIO_TYPE_A) &&
                            (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType
                             != RADIO_TYPE_AN) &&
                            (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType
                             != RADIO_TYPE_AC))
                        {
                            break;
                        }

                        if (WssIfProcessRfMgmtDBMsg
                            (RFMGMT_GET_AP_CONFIG_ENTRY,
                             &RfMgmtDB) == RFMGMT_SUCCESS)
                        {
                            vendSpec.unVendorSpec.
                                VendorDfsParams.VendorDfsParamsParams
                                [u1RadioID - 1].u2DfsQuietInterval =
                                RfMgmtDB.unRfMgmtDB.
                                RfMgmtApConfigTable.RfMgmtAPConfigDB.
                                u2RfMgmt11hDfsQuietInterval;
                            vendSpec.unVendorSpec.
                                VendorDfsParams.VendorDfsParamsParams
                                [u1RadioID - 1].u2DfsQuietPeriod =
                                RfMgmtDB.unRfMgmtDB.
                                RfMgmtApConfigTable.RfMgmtAPConfigDB.
                                u2RfMgmt11hDfsQuietPeriod;
                            vendSpec.unVendorSpec.
                                VendorDfsParams.VendorDfsParamsParams
                                [u1RadioID - 1].u2DfsMeasurementInterval =
                                RfMgmtDB.unRfMgmtDB.
                                RfMgmtApConfigTable.RfMgmtAPConfigDB.
                                u2RfMgmt11hDfsMeasurementInterval;
                            vendSpec.unVendorSpec.
                                VendorDfsParams.VendorDfsParamsParams
                                [u1RadioID - 1].u2DfsChannelSwitchStatus =
                                RfMgmtDB.unRfMgmtDB.
                                RfMgmtApConfigTable.RfMgmtAPConfigDB.
                                u2RfMgmt11hDfsChannelSwitchStatus;
                            vendSpec.unVendorSpec.
                                VendorDfsParams.VendorDfsParamsParams
                                [u1RadioID - 1].u1RadioId = u1RadioID;
#ifdef WTP_WANTED
                            vendSpec.unVendorSpec.
                                VendorDfsParams.VendorDfsParamsParams
                                [u1RadioID - 1].u111hDfsStatus =
                                RfMgmtDB.unRfMgmtDB.
                                RfMgmtApConfigTable.RfMgmtAPConfigDB.
                                u1RfMgmt11hDfsStatus;
#endif
                        }
#ifdef WLC_WANTED
                        MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
                        RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
                            RfMgmtAutoRfIsSetDB.bRfMgmt11hDfsStatus = OSIX_TRUE;
                        RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
                            RfMgmtAutoRfProfileDB.u4Dot11RadioType =
                            RADIO_TYPE_A;

                        if (WssIfProcessRfMgmtDBMsg
                            (RFMGMT_GET_AUTO_RF_ENTRY,
                             &RfMgmtDB) == RFMGMT_SUCCESS)
                        {
                            vendSpec.unVendorSpec.
                                VendorDfsParams.VendorDfsParamsParams
                                [u1RadioID - 1].u111hDfsStatus =
                                RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
                                RfMgmtAutoRfProfileDB.u1RfMgmt11hDfsStatus;
                        }

#endif
                        /*Assemble */
                        if (vendSpec.unVendorSpec.VendorDfsParams.
                            isOptional == OSIX_TRUE)
                        {
                            CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                                              vendSpec.unVendorSpec.
                                              VendorDfsParams.u2MsgEleType);
                            u2ElemLength =
                                (UINT2) (u2ElemLength + sizeof (UINT2));
                            CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                                              vendSpec.unVendorSpec.
                                              VendorDfsParams.u2MsgEleLen);
                            u2ElemLength =
                                (UINT2) (u2ElemLength + sizeof (UINT2));
                            CAPWAP_PUT_4BYTE (pu1MsgElemValue,
                                              vendSpec.unVendorSpec.
                                              VendorDfsParams.u4VendorId);
                            u2ElemLength =
                                (UINT2) (u2ElemLength + sizeof (UINT4));
                            if (vendSpec.unVendorSpec.
                                VendorDfsParams.VendorDfsParamsParams
                                [u1RadioID - 1].u1RadioId != 0)
                            {
                                CAPWAP_PUT_1BYTE (pu1MsgElemValue,
                                                  vendSpec.
                                                  unVendorSpec.VendorDfsParams.
                                                  VendorDfsParamsParams
                                                  [u1RadioID - 1].u1RadioId);
                                u2ElemLength =
                                    (UINT2) (u2ElemLength + sizeof (UINT1));
                                CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                                                  vendSpec.
                                                  unVendorSpec.VendorDfsParams.
                                                  VendorDfsParamsParams
                                                  [u1RadioID -
                                                   1].u2DfsQuietInterval);
                                u2ElemLength =
                                    (UINT2) (u2ElemLength + sizeof (UINT2));
                                CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                                                  vendSpec.
                                                  unVendorSpec.VendorDfsParams.
                                                  VendorDfsParamsParams
                                                  [u1RadioID -
                                                   1].u2DfsQuietPeriod);
                                u2ElemLength =
                                    (UINT2) (u2ElemLength + sizeof (UINT2));
                                CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                                                  vendSpec.
                                                  unVendorSpec.VendorDfsParams.
                                                  VendorDfsParamsParams
                                                  [u1RadioID -
                                                   1].u2DfsMeasurementInterval);
                                u2ElemLength =
                                    (UINT2) (u2ElemLength + sizeof (UINT2));
                                CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                                                  vendSpec.
                                                  unVendorSpec.VendorDfsParams.
                                                  VendorDfsParamsParams
                                                  [u1RadioID -
                                                   1].u2DfsChannelSwitchStatus);
                                u2ElemLength =
                                    (UINT2) (u2ElemLength + sizeof (UINT2));
                                CAPWAP_PUT_1BYTE (pu1MsgElemValue,
                                                  vendSpec.
                                                  unVendorSpec.VendorDfsParams.
                                                  VendorDfsParamsParams
                                                  [u1RadioID -
                                                   1].u111hDfsStatus);
                                u2ElemLength =
                                    (UINT2) (u2ElemLength + sizeof (UINT2));

                            }
                        }
                        *pu2MsgElemLen = u2ElemLength;
                        break;
#endif
                    case VENDOR_MULTIDOMAIN_INFO_TYPE:
                        break;
                    case VENDOR_DFS_CHANNEL_STATS:
#ifdef WTP_WANTED
                        vendSpec.isOptional = OSIX_TRUE;
                        vendSpec.u2MsgEleType = VENDOR_SPECIFIC_PAYLOAD;
                        vendSpec.unVendorSpec.DFSChannelInfo.isOptional =
                            OSIX_TRUE;
                        vendSpec.unVendorSpec.DFSChannelInfo.u2MsgEleType =
                            VENDOR_DFS_CHANNEL_MSG;
                        vendSpec.unVendorSpec.DFSChannelInfo.u2MsgEleLen =
                            VENDOR_DFS_CHANNEL_MSG_LEN;
                        vendSpec.unVendorSpec.DFSChannelInfo.u4VendorId =
                            VENDOR_ID;
                        vendSpec.elementId = VENDOR_DFS_CHANNEL_STATS;

                        RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                            RfMgmtAPConfigIsSetDB.bDFSChannelStatus = OSIX_TRUE;

                        RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                            RfMgmtAPConfigDB.u4RadioIfIndex =
                            u1RadioID + SYS_DEF_MAX_ENET_INTERFACES;
                        if (WssIfProcessRfMgmtDBMsg
                            (RFMGMT_GET_AP_CONFIG_ENTRY,
                             &RfMgmtDB) == RFMGMT_SUCCESS)
                        {
                            unsigned int        idx = 0;
                            while (idx < RADIO_MAX_DFS_CHANNEL)
                            {

                                vendSpec.unVendorSpec.DFSChannelInfo.
                                    DFSInfo[idx].u4ChannelNum =
                                    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                                    RfMgmtAPConfigDB.DFSChannelStatus[idx].
                                    u4ChannelNum;

                                vendSpec.unVendorSpec.DFSChannelInfo.
                                    DFSInfo[idx].u4DFSFlag =
                                    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                                    RfMgmtAPConfigDB.DFSChannelStatus[idx].
                                    u4DFSFlag;
                                idx++;

                            }
                            vendSpec.unVendorSpec.DFSChannelInfo.u1RadioId =
                                u1RadioID;
                            vendSpec.u2MsgEleType = VENDOR_SPECIFIC_PAYLOAD;
                            vendSpec.u2MsgEleLen = VENDOR_DFS_CHANNEL_MSG_LEN +
                                CAPWAP_MSG_ELEM_TYPE_LEN;

                        }
#endif
                        /*Assemble */
                        if (vendSpec.unVendorSpec.DFSChannelInfo.isOptional
                            == OSIX_TRUE)
                        {
                            CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                                              vendSpec.unVendorSpec.
                                              DFSChannelInfo.u2MsgEleType);
                            u2ElemLength =
                                (UINT2) (u2ElemLength + sizeof (UINT2));
                            CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                                              vendSpec.unVendorSpec.
                                              DFSChannelInfo.u2MsgEleLen);
                            u2ElemLength =
                                (UINT2) (u2ElemLength + sizeof (UINT2));
                            CAPWAP_PUT_4BYTE (pu1MsgElemValue,
                                              vendSpec.unVendorSpec.
                                              DFSChannelInfo.u4VendorId);
                            u2ElemLength =
                                (UINT2) (u2ElemLength + sizeof (UINT4));
                            CAPWAP_PUT_1BYTE (pu1MsgElemValue,
                                              vendSpec.unVendorSpec.
                                              DFSChannelInfo.u1RadioId);
                            u2ElemLength =
                                (UINT2) (u2ElemLength + sizeof (UINT1));

                            unsigned int        idx = 0;
                            while (idx < RADIO_MAX_DFS_CHANNEL)
                            {
                                CAPWAP_PUT_4BYTE (pu1MsgElemValue,
                                                  vendSpec.unVendorSpec.
                                                  DFSChannelInfo.DFSInfo[idx].
                                                  u4ChannelNum);
                                u2ElemLength =
                                    (UINT2) (u2ElemLength + sizeof (UINT4));
                                CAPWAP_PUT_4BYTE (pu1MsgElemValue,
                                                  vendSpec.unVendorSpec.
                                                  DFSChannelInfo.DFSInfo[idx].
                                                  u4DFSFlag);
                                u2ElemLength =
                                    (UINT2) (u2ElemLength + sizeof (UINT4));

                                idx++;
                            }
                        }
                        *pu2MsgElemLen = u2ElemLength;
                        break;

                    case VENDOR_DOT11N_CONFIG_TYPE:
                        vendSpec.u2MsgEleType = VENDOR_SPECIFIC_PAYLOAD;
                        vendSpec.u2MsgEleLen = RADIO_DOT11N_EXT_MSG_LEN +
                            CAPWAP_MSG_ELEM_TYPE_LEN;
                        vendSpec.elementId = VENDOR_DOT11N_CONFIG_TYPE;

                        vendSpec.unVendorSpec.VendorDot11nCfg.u2MsgEleType
                            = VENDOR_DOT11N_CONFIG_MSG;
                        vendSpec.unVendorSpec.VendorDot11nCfg.u2MsgEleLen
                            = RADIO_DOT11N_EXT_MSG_LEN;
                        vendSpec.unVendorSpec.VendorDot11nCfg.u4VendorId
                            = VENDOR_ID;
                        vendSpec.unVendorSpec.VendorDot11nCfg.isOptional =
                            OSIX_TRUE;

                        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
                            pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;

                        RadioIfGetDB.RadioIfIsGetAllDB.bAMPDUStatus = OSIX_TRUE;
                        RadioIfGetDB.RadioIfIsGetAllDB.bAMPDUSubFrame =
                            OSIX_TRUE;
                        RadioIfGetDB.RadioIfIsGetAllDB.bAMPDULimit = OSIX_TRUE;
                        RadioIfGetDB.RadioIfIsGetAllDB.bAMSDUStatus = OSIX_TRUE;
                        RadioIfGetDB.RadioIfIsGetAllDB.bAMSDULimit = OSIX_TRUE;

                        RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
                        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                                      &RadioIfGetDB) !=
                            OSIX_SUCCESS)
                        {

                        }
                        /* Get the Radio IF DB info to fill the structure  */
                        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N,
                                                      &RadioIfGetDB) !=
                            OSIX_SUCCESS)
                        {

                        }
                        /*Assemble */
                        if (vendSpec.unVendorSpec.VendorDot11nCfg.isOptional
                            == OSIX_TRUE)
                        {
                            CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                                              vendSpec.unVendorSpec.
                                              VendorDot11nCfg.u2MsgEleType);
                            u2ElemLength =
                                (UINT2) (u2ElemLength + sizeof (UINT2));
                            CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                                              vendSpec.unVendorSpec.
                                              VendorDot11nCfg.u2MsgEleLen);
                            u2ElemLength =
                                (UINT2) (u2ElemLength + sizeof (UINT2));
                            CAPWAP_PUT_4BYTE (pu1MsgElemValue,
                                              vendSpec.unVendorSpec.
                                              VendorDot11nCfg.u4VendorId);
                            u2ElemLength =
                                (UINT2) (u2ElemLength + sizeof (UINT4));
                            CAPWAP_PUT_1BYTE (pu1MsgElemValue,
                                              RadioIfGetDB.RadioIfGetAllDB.
                                              u1RadioId);
                            u2ElemLength =
                                (UINT2) (u2ElemLength + sizeof (UINT1));
                            CAPWAP_PUT_1BYTE (pu1MsgElemValue,
                                              RadioIfGetDB.RadioIfGetAllDB.
                                              u1AMPDUStatus);
                            u2ElemLength =
                                (UINT2) (u2ElemLength + sizeof (UINT1));
                            CAPWAP_PUT_1BYTE (pu1MsgElemValue,
                                              RadioIfGetDB.RadioIfGetAllDB.
                                              u1AMPDUSubFrame);
                            u2ElemLength =
                                (UINT2) (u2ElemLength + sizeof (UINT1));
                            CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                                              RadioIfGetDB.RadioIfGetAllDB.
                                              u2AMPDULimit);
                            u2ElemLength =
                                (UINT2) (u2ElemLength + sizeof (UINT2));
                            CAPWAP_PUT_1BYTE (pu1MsgElemValue,
                                              RadioIfGetDB.RadioIfGetAllDB.
                                              u1AMSDUStatus);
                            u2ElemLength =
                                (UINT2) (u2ElemLength + sizeof (UINT1));
                            CAPWAP_PUT_2BYTE (pu1MsgElemValue,
                                              RadioIfGetDB.RadioIfGetAllDB.
                                              u2AMSDULimit);
                            u2ElemLength =
                                (UINT2) (u2ElemLength + sizeof (UINT2));
                        }
                        *pu2MsgElemLen = u2ElemLength;
                        break;
                    default:
                        break;
                }                /* End of switch for u1MsgSubType for Config status req */
            }                    /* End of else part */
            switch (u1MsgSubType)
            {
                case VENDOR_MULTIDOMAIN_INFO_TYPE:
                    if ((u1RadioID == 1) && (gu1MultiDomainSent == 0))
                    {
#ifdef WTP_WANTED
                        ApHdlrMsg.PmWtpEventReq.vendSpec.isOptional = OSIX_TRUE;
                        ApHdlrMsg.PmWtpEventReq.vendSpec.u2MsgEleType =
                            VENDOR_SPECIFIC_PAYLOAD;
                        ApHdlrMsg.PmWtpEventReq.vendSpec.elementId =
                            VENDOR_MULTIDOMAIN_INFO_TYPE;
                        ApHdlrMsg.PmWtpEventReq.vendSpec.
                            unVendorSpec.VendMultiDomain.isOptional = OSIX_TRUE;
#endif
                        CapwapAssembleVendSpecMultiDomainInfo (pu1MsgElemValue,
                                                               u2NumRadioElems,
                                                               pu2MsgElemLen,
                                                               &RadioIfGetDB);
                        gu1MultiDomainSent = 1;

                    }
                    break;
                default:
                    break;
            }
            break;                /* Break for case VENDOR_SPECIFIC_PAYLOAD */

        default:
            /* default case for u2MsgElemType */
            break;
    }
    pu1MsgElemValue = pu1MsgTempElemValue;    /*To give back the same starting point address */
    UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}

        /*****************************************************************************
         * Function     : CapwapConfigNoOfRadio                                      *
         *                                                                           *
         * Description  : Find the number of radios using Internal ID                *
         *                                                                           *
         * Input        : Internal ID                                                *
         *                                                                           *
         * Output       : Number of Radios                                           *
         *                                                                           *
         * Returns      : SUCCESS/FAILURE                                            *
         *****************************************************************************/

INT4
CapwapConfigNoOfRadio (UINT2 u2IntId, UINT2 *u2NumOfRadios)
{
#ifdef WLC_WANTED
    tWssIfCapDB        *pWssIfCapDB = NULL;
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapDB, 0, sizeof (tWssIfCapDB));

    pWssIfCapDB->CapwapIsGetAllDB.bNoOfRadio = OSIX_TRUE;
    pWssIfCapDB->CapwapGetDB.u2WtpInternalId = u2IntId;
    pWssIfCapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                 pWssIfCapDB) == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to Get Model Table CAPWAP DB \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to Get Model Table CAPWAP DB \r\n"));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
        return OSIX_FAILURE;
    }
    *u2NumOfRadios = pWssIfCapDB->CapwapGetDB.u1WtpNoofRadio;

    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
#else
    UNUSED_PARAM (u2IntId);
    *u2NumOfRadios = SYS_DEF_MAX_RADIO_INTERFACES;
#endif
    return OSIX_SUCCESS;
}

        /*****************************************************************************
         * Function     : CapwapDispGenRadioParam                                    *
         *                                                                           *
         * Description  : Display the Radios parameters stored in DB                 *
         *                                                                           *
         * Input        : NONE                                                       *
         *                                                                           *
         * Output       : NONE                                                       *
         *                                                                           *
         * Returns      : SUCCESS/FAILURE                                            *
         *****************************************************************************/

INT4
CapwapDispGenRadioParam ()
{
    CAPWAP_FN_ENTRY ();
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT1              *pu1MsgTempWtpQos = NULL;
    UINT1              *pu1MsgElemValue = NULL;
    UINT1              *pu1MsgWtpQosElemValue = NULL;
    UINT4               u4RadioIfIndex = 0;
    UINT2               u2MsgElemLen = 0;
    UINT2               u2NumRadioElems = 0;
    UINT2               u2MsgWtpQosElemLength = 0;
    UINT2               u2MsgElemType = 0;
    UINT1               u1MsgSubType = 0;
    UINT2               u2IntId = 0;
    UINT1               u1RadioIndex = 0;
    UINT1               u1QosIndex = 0;
    UINT1               u1MsgElemIndex = 0;

    /* this static variable is used in finding the presence
     * of at least one client info available for radio. */

    for (u1MsgElemIndex = 1; u1MsgElemIndex <= 14; u1MsgElemIndex++)    /*Increase count when new members are added */
    {
        WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
            pu1MsgElemValue =
            (UINT1 *) MemAllocMemBlk (CAPWAP_GENASSEMBLERADIO_POOLID);

        pu1MsgWtpQosElemValue =
            (UINT1 *) MemAllocMemBlk (CAPWAP_GENASSEMBLERADIO_POOLID);

        switch (u1MsgElemIndex)
        {
            case 1:
                u2MsgElemType = IEEE_ANTENNA;
                break;
            case 2:
                u2MsgElemType = IEEE_DIRECT_SEQUENCE_CONTROL;
                break;
            case 3:
                u2MsgElemType = IEEE_INFORMATION_ELEMENT;
                break;
            case 4:
                u2MsgElemType = IEEE_MAC_OPERATION;
                break;
            case 5:
                u2MsgElemType = IEEE_MULTIDOMAIN_CAPABILITY;
                break;
            case 6:
                u2MsgElemType = IEEE_OFDM_CONTROL;
                break;
            case 7:
                u2MsgElemType = IEEE_SUPPORTED_RATES;
                break;
            case 8:
                u2MsgElemType = IEEE_TX_POWER;
                break;
            case 9:
                u2MsgElemType = IEEE_TX_POWERLEVEL;
                break;
            case 10:
                u2MsgElemType = IEEE_WTP_RADIO_CONFIGURATION;
                break;
            case 11:
                u2MsgElemType = WTP_RADIO_INFO;
                break;
            case 12:
                u2MsgElemType = IEEE_RATE_SET;
                break;
            case 13:
                u2MsgElemType = IEEE_WTP_QOS;
                break;
            case 14:
                u2MsgElemType = RADIO_OPER_STATE;
                break;
            default:
                break;
        }
        CapwapConfigNoOfRadio (u2IntId, &u2NumRadioElems);
        for (u1RadioIndex = 1; u1RadioIndex <= u2NumRadioElems; u1RadioIndex++)
        {
            MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
            MEMSET (pu1MsgElemValue, 0, MAX_CAPWAP_GENASSEMBLERADIO);
            pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
            pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2IntId;
            pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;
            pWssIfCapwapDB->CapwapGetDB.u1RadioId = u1RadioIndex;
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                         pWssIfCapwapDB) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Getting Radio index failed\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Getting Radio index failed\r\n"));
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                MemReleaseMemBlock (CAPWAP_GENASSEMBLERADIO_POOLID,
                                    pu1MsgElemValue);
                MemReleaseMemBlock (CAPWAP_GENASSEMBLERADIO_POOLID,
                                    pu1MsgWtpQosElemValue);
                return OSIX_FAILURE;
            }
            u4RadioIfIndex = pWssIfCapwapDB->CapwapGetDB.u4IfIndex;
            if (u2MsgElemType == IEEE_WTP_QOS)
            {
                MEMSET (pu1MsgWtpQosElemValue, 0, MAX_CAPWAP_GENASSEMBLERADIO);
                pu1MsgTempWtpQos = pu1MsgWtpQosElemValue;
                u2MsgWtpQosElemLength = 0;
                for (u1QosIndex = 1; u1QosIndex <= WSSIF_RADIOIF_QOS_CONFIG_LEN;
                     u1QosIndex++)
                {
                    CapwapFetchGenElem (u2MsgElemType, u1MsgSubType,
                                        pu1MsgElemValue, &u2MsgElemLen,
                                        u4RadioIfIndex, u1RadioIndex,
                                        u1QosIndex);
                    CAPWAP_PUT_NBYTE (pu1MsgTempWtpQos, *pu1MsgElemValue,
                                      (INT4) u2MsgElemLen);
                    u2MsgWtpQosElemLength =
                        (UINT2) (u2MsgWtpQosElemLength + u2MsgElemLen);
                }
            }
            else
            {
                CapwapFetchGenElem (u2MsgElemType, u1MsgSubType,
                                    pu1MsgElemValue, &u2MsgElemLen,
                                    u4RadioIfIndex, u1RadioIndex, u1QosIndex);
            }
            if (u2MsgElemType == IEEE_WTP_QOS)
            {                    /* Include while testing QOS */
                /*CAPWAP_PUT_NBYTE (*pu1TxBuf, *pu1MsgWtpQosElemValue, u2MsgWtpQosElemLength); */
            }
            else
            {
                /* Include while testing QOS */
                /*CAPWAP_PUT_NBYTE (*pu1TxBuf, *pu1MsgElemValue, u2MsgElemLen); */
            }
        }
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        MemReleaseMemBlock (CAPWAP_GENASSEMBLERADIO_POOLID, pu1MsgElemValue);
        MemReleaseMemBlock (CAPWAP_GENASSEMBLERADIO_POOLID,
                            pu1MsgWtpQosElemValue);
    }
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

#endif
