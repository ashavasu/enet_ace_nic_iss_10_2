/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved                      *
 * $Id: capwapwlcdtls.c,v 1.2 2017/05/23 14:16:49 siva Exp $      *
 * Description: This file contains the implementation for CAPWAP to DTLS     *
 * interface.                                                                *
 *                                                                           *
 *****************************************************************************/
#ifndef __CAPWAPDTLS_C__
#define __CAPWAPDTLS_C__

#include "capwapinc.h"
#include "dtls.h"
#ifdef NPAPI_WANTED
#include "nputil.h"
#endif

#include "dtlsProtWLC.h"
/* The Gloabl variable to store the DTLS fd */
UINT4               gu4DtlsFd;
#define CAPWAP_DTLS_UDP_PORT          9999

/****************************************************************************
*                                                                           *
* Function     : CapwapEnableDTLSInit                                       *
*                                                                           *
* Description  : This function used to enable the DTLS interface            *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : OSIX_SUCCESS, if initialization succeeds                   *
*                OSIX_FAILURE, otherwise                                    *
*                                                                           *
*****************************************************************************/
INT4
CapwapEnableDTLSInit ()
{
    tDtlsParams         DtlsParams;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
#ifdef DTLS_WANTED
    UINT4               u4DtlsFd;
#endif
    MEMSET (&DtlsParams, 0, sizeof (tDtlsParams));

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    pWssIfCapwapDB->CapwapIsGetAllDB.bCtrlDTLSStatus = OSIX_TRUE;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
        == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to create the entry in Temporary DTLS table \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to create the entry in Temporary DTLS table \r\n"));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
#ifdef DTLS_WANTED
    DtlsInit ();

    DtlsParams.SeesionEstdNotify = CapwapDtlsSeesionEstdNotify;
    DtlsParams.PeerDisconnectNotify = CapwapDtlsPeerDisconnectNotify;
    DtlsParams.DataExchangeNotify = CapwapDtlsDataNotify;

    u4DtlsFd = DtlsFnSelect (DTLS_OPEN, &DtlsParams);
    if (u4DtlsFd == 0)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Faild to open DTLS \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Faild to open DTLS \r\n"));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    else
    {
        /* Updated the DTLS Fd in CPWAP module */
        gu4DtlsFd = u4DtlsFd;
        DtlsParams.u4Fd = gu4DtlsFd;
        DtlsParams.u4Port = DTLS_PORT_NO;
        if (DtlsFnSelect (DTLS_ACCEPT, &DtlsParams) == OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Accept DTLS \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to Accept DTLS \r\n"));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
    }
#endif
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}

/****************************************************************************
*                                                                           *
* Function     : CapwapEnableDTLS                                           *
*                                                                           *
* Description  : This function used to disable the DTLS interface           *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : OSIX_SUCCESS, if initialization succeeds                   *
*                OSIX_FAILURE, otherwise                                    *
*                                                                           *
*****************************************************************************/
INT4
CapwapEnableDTLS (VOID)
{
#ifdef DTLS_WANTED
    tDtlsParams         DtlsParams;

    DtlsParams.u4Fd = gu4DtlsFd;
    if (DtlsFnSelect (DTLS_ENABLE, &DtlsParams) == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Eanble DTLS \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to Eanble DTLS \r\n"));
        return OSIX_FAILURE;
    }
#endif
    return OSIX_SUCCESS;
}

/****************************************************************************
*                                                                           *
* Function     : CapwapDisableDTLS                                          *
*                                                                           *
* Description  : This function used to disable the DTLS interface           *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : OSIX_SUCCESS, if initialization succeeds                   *
*                OSIX_FAILURE, otherwise                                    *
*                                                                           *
*****************************************************************************/
INT4
CapwapDisableDTLS (VOID)
{
#ifdef DTLS_WANTED
    tDtlsParams         DtlsParams;

    DtlsParams.u4Fd = gu4DtlsFd;
    if (DtlsFnSelect (DTLS_DISABLE, &DtlsParams) == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Eanble DTLS \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to Eanble DTLS \r\n"));
        return OSIX_FAILURE;
    }
#endif
    return OSIX_SUCCESS;
}

/******************************************************************************
 *                                                                            *
 *  Function     : CapwapShutDownDTLS                                         *
 *                                                                            *
 *  Description  : This function used to disable the DTLS interface           *
 *                                                                            *
 *  Input        : None                                                       *
 *                                                                            *
 *  Output       : None                                                       *
 *                                                                            *
 *  Returns      : OSIX_SUCCESS, if initialization succeeds                   *
 *                 OSIX_FAILURE, otherwise                                    *
 *                                                                            *
 * ****************************************************************************/
INT4
CapwapShutDownDTLS (tRemoteSessionManager * pSessEntry)
{
#ifdef DTLS_WANTED
    tDtlsParams         DtlsParams;
#endif
#ifdef NPAPI_WANTED
    tFsHwNp             FsHwNp;
    tCapwapNpParams     ptCapwapNpParams;
    tCapwapNpModInfo   *pCapwapNpModInfo = NULL;
    tCapwapNpWrEnable  *pDelUcTunnel = NULL;
    MEMSET (&ptCapwapNpParams, 0, sizeof (tCapwapNpParams));
#endif

#ifdef NPAPI_WANTED
    /* Static values need to be filled from Capwap */
    /* Call the tunnel deletion NPAPI */
    ptCapwapNpParams.remoteIpAddr.u2AddressLen = CAPWAP_NP_ADDRESS_IPV4;
    ptCapwapNpParams.remoteIpAddr.ipAddr.u4_addr[0]
        = pSessEntry->remoteIpAddr.u4_addr[0];
    ptCapwapNpParams.u4DestDataPort = pSessEntry->u4RemoteDataPort;
    ptCapwapNpParams.u4TunnelId = pSessEntry->u4TunnelId;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_CAPWAP_MODULE,    /* Module ID */
                         CAPWAP_DELETE_UNICAST_TUNNEL,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pCapwapNpModInfo = &(FsHwNp.CapwapNpModInfo);
    pDelUcTunnel
        = (tCapwapNpWrEnable *) & pCapwapNpModInfo->CapwapNpDeleteUcTunnel;

    pDelUcTunnel->ptCapwapNpParams = &ptCapwapNpParams;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "\n Deleting unicast tunnel failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "\n Deleting unicast tunnel failed \r\n"));
        return (OSIX_FAILURE);
    }
#endif
    if (pSessEntry->u1DtlsFlag == DTLS_CFG_DISABLE)
    {
        CAPWAP_TRC (CAPWAP_MGMT_TRC, "DTLS disabled \r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                      "DTLS disabled \r\n"));
        return OSIX_SUCCESS;
    }
    else
    {
#ifdef DTLS_WANTED
        DtlsParams.u4Id = pSessEntry->u4DtlsSessId;
        if (DtlsFnSelect (DTLS_SESSION_SHUTDOWN, &DtlsParams) == OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Shutdown DTLS \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to Shutdown DTLS \r\n"));
            return OSIX_FAILURE;
        }
#endif
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
*                                                                           *
* Function     : CapwapDtlsSeesionEstdNotify                                *
*                                                                           *
* Description  : This function will be called by the DTLS module if it      *
*                detects the new peer                                       *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*****************************************************************************/
VOID
CapwapDtlsSeesionEstdNotify (UINT4 u4DtlsId, UINT4 u4IpAddr, UINT4 u4DestPort)
{
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UNUSED_PARAM (u4DestPort);

    WSS_IF_DB_TEMP_MEM_ALLOC_VOID_RETURN (pWssIfCapwapDB)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    pWssIfCapwapDB->u4DestIp = u4IpAddr;
    pWssIfCapwapDB->u4DtlsId = u4DtlsId;
    /* Create Entry in the Temporary RB Tree */
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_CREATE_TEMPDTLS_ENTRY,
                                 pWssIfCapwapDB) == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to create the entry in Temporary DTLS table \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to create the entry in Temporary DTLS table \r\n"));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return;
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return;
}

/****************************************************************************
*                                                                           *
* Function     : CapwapDtlsPeerDisconnectNotify                             *
*                                                                           *
* Description  : This function will be called by the DTLS module to         *
*                disconnect the peer                                        *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*****************************************************************************/
VOID
CapwapDtlsPeerDisconnectNotify (UINT4 u4IpAddr, UINT4 u4DestPort,
                                UINT4 u4DtlsId)
{
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UNUSED_PARAM (u4DtlsId);

    WSS_IF_DB_TEMP_MEM_ALLOC_VOID_RETURN (pWssIfCapwapDB)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    pWssIfCapwapDB->u4DestIp = u4IpAddr;
    pWssIfCapwapDB->u4DestPort = u4DestPort;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_DELETE_RSM, pWssIfCapwapDB)
        == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to Delete the Enry in Remote session Module \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to Delete the Enry in Remote session Module \r\n"));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return;
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return;
}

/****************************************************************************
*                                                                           *
* Function     : CapwapDtlsDataNotify                                       *
*                                                                           *
* Description  : This function will be called by the DTLS module after      *
*                encrypt or decrypt the given packet.                       *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*****************************************************************************/
VOID
CapwapDtlsDataNotify (tQueData * pDtlsData)
{
    UINT1               au1DtlsHdr[DTLS_HDR_SIZE] = DTLS_HDR_FORMAT;
    tCRU_BUF_CHAIN_HEADER *pTxBuf = NULL;
    INT4                i4RetVal = OSIX_FAILURE;

    CAPWAP_FN_ENTRY ();
    if (pDtlsData->eQueDataType == DATA_ENCRYPT)
    {
        pTxBuf = pDtlsData->pBuffer;
        if (CRU_BUF_Prepend_BufChain (pTxBuf, (UINT1 *) au1DtlsHdr,
                                      CAPWAP_DTLS_HDR_LEN) == CRU_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Cru buffer Prepend operation Failed \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Cru buffer Prepend operation Failed \r\n"));
            return;
        }
        if (pDtlsData->u4Channel == DTLS_CTRL_CHANNEL)
        {
            i4RetVal = CapwapCtrlUdpTxPacket (pTxBuf,
                                              pDtlsData->u4IpAddr,
                                              pDtlsData->u4PortNo,
                                              (pDtlsData->u4BufSize +
                                               CAPWAP_DTLS_HDR_LEN));
            if (i4RetVal == OSIX_FAILURE)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Failed to Transmit the encrypted packet \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to Transmit the encrypted packet \r\n"));
            }
            CAPWAP_RELEASE_CRU_BUF (pTxBuf);
        }
        else if (pDtlsData->u4Channel == DTLS_DATA_CHANNEL)
        {
            if (CapwapDataUdpTxPacket (pTxBuf,
                                       pDtlsData->u4IpAddr,
                                       pDtlsData->u4PortNo,
                                       (pDtlsData->u4BufSize +
                                        CAPWAP_DTLS_HDR_LEN),
                                       CAPWAP_CTRL_PKT_DSCP_CODE) ==
                OSIX_FAILURE)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Failed to Transmit the encrypted packet \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to Transmit the encrypted packet \r\n"));
            }
        }
        else
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Invalid Channel type received \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Invalid Channel type received \r\n"));
        }
    }
    else if (pDtlsData->eQueDataType == DATA_DECRYPT)
    {
        pTxBuf = pDtlsData->pBuffer;
        CRU_BUF_Set_U4Reserved2 (pTxBuf, pDtlsData->u4IpAddr);
        CRU_BUF_Set_U4Reserved1 (pTxBuf, pDtlsData->u4PortNo);
        if (pDtlsData->u4Channel == DTLS_CTRL_CHANNEL)
        {
            if (CapwapProcessCtrlPacket (pTxBuf) == OSIX_FAILURE)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Failed to process the Received Control packet \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to process the Received Control packet \r\n"));
                return;
            }
        }
        else if (pDtlsData->u4Channel == DTLS_DATA_CHANNEL)
        {
            if (CapwapProcessDataPacket (pTxBuf) == OSIX_FAILURE)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Failed to process the Received Control packet \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to process the Received Control packet \r\n"));
                return;
            }
        }
        else
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Invalid Channel type received \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Invalid Channel type received \r\n"));
        }
    }
    CAPWAP_FN_EXIT ();
    return;
}

/****************************************************************************
*                                                                           *
* Function     : CapwapDtlsDecryptNotify                                    *
*                                                                           *
* Description  : This function will be called by the DTLS module for        *
*                decrypting the given packet.                               *
*                                                                           *
* Input        : pDtlsData                                                  *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*****************************************************************************/
INT4
CapwapDtlsDecryptNotify (tQueData * pDtlsData)
{
#ifdef DTLS_WANTED
    if (DtlsDataEncryptDecryptNotify (gu4DtlsFd, pDtlsData) == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapDtlsDecryptNotify Retrurnted Failure \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "CapwapDtlsDecryptNotify Retrurnted Failure \r\n"));
        return OSIX_FAILURE;
    }
#else
    UNUSED_PARAM (pDtlsData);
#endif
    return OSIX_SUCCESS;
}

INT4
CapwapStartDTLS (UINT4 u4IpAddr)
{
    UNUSED_PARAM (u4IpAddr);
    return OSIX_FAILURE;
}

#endif
