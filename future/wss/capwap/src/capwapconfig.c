/********************************************************************
 *
 * $Id: capwapconfig.c,v 1.3 2017/11/24 10:37:03 siva Exp $       
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * Description: This file contains the CAPWAP Join state routines
 *
 *******************************************************************/
#ifndef __CAPWAP_CONFIG_C__
#define __CAPWAP_CONFIG_C__
#include "capwapinc.h"
#include "wssifradiodb.h"
#ifdef WLC_WANTED
#include "wsswlanwlcproto.h"
#else
#include "wsswlanwtpproto.h"
#endif
#ifdef RFMGMT_WANTED
#include "rfminc.h"
#include "rfmextn.h"
#endif
#include "radioifextn.h"
extern INT4
        WlchdlrFreeTransmittedPacket (tRemoteSessionManager * pSessEntry);
extern INT4         AphdlrFreeTransmittedPacket (tRemoteSessionManager *
                                                 pSessEntry);

/************************************************************************/
/*  Function Name   : CapwapProcessConfigStatusRequest                  */
/*  Description     : The function processes the received CAPWAP        */
/*                    Config status request                             */
/*  Input(s)        : joinReqMsg - CAPWAP Join request packet received  */
/*                    pSessEntry - Pointer to the session data base     */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/

INT4
CapwapProcessConfigStatusRequest (tSegment * pBuf,
                                  tCapwapControlPacket * pConfigStatusReqMsg,
                                  tRemoteSessionManager * pSessEntry)
{
    UINT4               u4MsgLen = 0;
    INT4                i4Status;
    UINT1              *pu1TxBuf = NULL, *pRcvdBuf = NULL;
    tConfigStatusRsp   *pConfigStatusResp = NULL;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tSegment           *pTxBuf = NULL;
    UINT1               u1IsNotLinear = OSIX_FALSE;
    UINT2               u2IntProfileId = 0, u2PacketLen;
    tWssIfPMDB          WssIfPMDB;

    UINT1              *pu1TempBuf = NULL;

    CAPWAP_FN_ENTRY ();

    gu1IsConfigResponseReceived = OSIX_SUCCESS;
#ifdef RFMGMT_WANTED
    gu1IsRfMgmtResponseReceived = OSIX_SUCCESS;
#endif
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        pConfigStatusResp =
        (tConfigStatusRsp *) (VOID *) UtlShMemAllocConfigStatusRspBuf ();
    if (pConfigStatusResp == NULL)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapProcessConfigStatusRequest:- "
                    "UtlShMemAllocConfigStatusRspBuf returned failure\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "CapwapProcessConfigStatusRequest:- "
                      "UtlShMemAllocConfigStatusRspBuf returned failure\n"));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    if (pSessEntry == NULL || (pSessEntry->eCurrentState != CAPWAP_JOIN))
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "CAPWAP Session Entry is NULL, retrun"
                    "FAILURE \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "CAPWAP Session Entry is NULL, retrun" "FAILURE \r\n"));
        UtlShMemFreeConfigStatusRspBuf ((UINT1 *) pConfigStatusResp);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    /* Kloc Fix Start */
    if (pBuf == NULL)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "pBuf Entry is NULL, retrun"
                    "FAILURE \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "pBuf Entry is NULL, retrun" "FAILURE \r\n"));
        UtlShMemFreeConfigStatusRspBuf ((UINT1 *) pConfigStatusResp);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    /* Kloc Fix Ends */
    /* stop the wait Join timer and 
     * clear the packet buffer pointer */
    if (CapwapTmrStop (pSessEntry, CAPWAP_WAITJOIN_TMR) == OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_MGMT_TRC, "Stopped the WaitJoin timer \r\n");
        SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4CapwapSysLogId,
                      "Stopped the WaitJoin timer \r\n"));
        /* clear the packet buffer pointer */
#ifdef WLC_WANTED
        WlchdlrFreeTransmittedPacket (pSessEntry);
#else
        AphdlrFreeTransmittedPacket (pSessEntry);
#endif
    }

    MEMSET (pConfigStatusResp, 0, sizeof (tConfigStatusRsp));

    u2PacketLen = (UINT2) CAPWAP_GET_BUF_LEN (pBuf);
    pRcvdBuf = CAPWAP_BUF_IF_LINEAR (pBuf, 0, u2PacketLen);
    if (pRcvdBuf == NULL)
    {
        pRcvdBuf = (UINT1 *) MemAllocMemBlk (CAPWAP_PKTBUF_POOLID);
        CAPWAP_COPY_FROM_BUF (pBuf, pRcvdBuf, 0, u2PacketLen);
        u1IsNotLinear = OSIX_TRUE;
    }

    u2IntProfileId = pConfigStatusReqMsg->u2IntProfileId;
    pConfigStatusResp->u2IntProfileId = pConfigStatusReqMsg->u2IntProfileId;
    if (pConfigStatusReqMsg->capwapCtrlHdr.u4MsgType !=
        WSS_RADIOIF_CONFIG_STATUS_REQ)
    {
        pConfigStatusReqMsg->capwapCtrlHdr.u4MsgType =
            WSS_RADIOIF_CONFIG_STATUS_REQ;
    }

    /* Validate the Conf Status request message. if it is success
       construct the Config Status Response */
    if ((CapwapValidateConfigStatusRequestMsgElems
         (pRcvdBuf, pConfigStatusReqMsg, pConfigStatusResp) == OSIX_SUCCESS))
    {

        if (u1IsNotLinear == OSIX_TRUE)
        {
            MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pRcvdBuf);
        }
        /* Update the Remote session state to CAPWAP_JOIN */
        CAPWAP_TRC (CAPWAP_FSM_TRC, "Entered CAPWAP CONFIGURE State \r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                      "Entered CAPWAP CONFIGURE State \r\n"));
        pSessEntry->eCurrentState = CAPWAP_CONFIGURE;
        pSessEntry->ePrevState = CAPWAP_CONFIGURE;

        if (CapwapConstructCpHeader (&(pConfigStatusResp->capwapHdr)) ==
            OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "capwapProcessConfigStatusRequest:Failed to"
                        "construct the capwap Header\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "capwapProcessConfigStatusRequest:Failed to"
                          "construct the capwap Header\n"));
            UtlShMemFreeConfigStatusRspBuf ((UINT1 *) pConfigStatusResp);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
        /* get message elements to construct the config status response */
        if ((CapwapGetCapwapTimers
             (&(pConfigStatusResp->capwapTimer), &u4MsgLen,
              u2IntProfileId) == OSIX_FAILURE)
            ||
            (CapwapGetDecryptErrReportPeriod
             (&(pConfigStatusResp->decryErrPeriod), &u4MsgLen) == OSIX_FAILURE)
            ||
            (CapwapGetIdleTimeout
             (&(pConfigStatusResp->idleTimeout), &u4MsgLen,
              u2IntProfileId) == OSIX_FAILURE)
            ||
            (CapwapGetWtpFallback
             (&(pConfigStatusResp->wtpFallback), &u4MsgLen,
              u2IntProfileId) == OSIX_FAILURE)
            || (CapwapGetAcIpList (&(pConfigStatusResp->ipList), &u4MsgLen) ==
                OSIX_FAILURE))
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to get Mandatory Configuration Response"
                        "Message Elements \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to get Mandatory Configuration Response"
                          "Message Elements \r\n"));
            UtlShMemFreeConfigStatusRspBuf ((UINT1 *) pConfigStatusResp);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }

        if (CapwapGetPriAcNameWithPrioList
            (&(pConfigStatusResp->acNameWithPrio[0]), &u4MsgLen,
             u2IntProfileId) == OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to get Optional Message Ac Name"
                        "With Priority \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to get Optional Message Ac Name"
                          "With Priority \r\n"));
            UtlShMemFreeConfigStatusRspBuf ((UINT1 *) pConfigStatusResp);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
        /* update optional Message Elements */
        /* if ((CapwapGetWtpStaticIpInfo (pConfigStatusResp->wtpStaticIpAddr,
           &u4MsgLen) == OSIX_FAILURE ) ||
           (CapwapGetVendorPayload  (pConfigStatusResp->vendSpec, &u4MsgLen) ==
           OSIX_FAILURE ))
           {
           CAPWAP_TRC (CAPWAP_FAILURE_TRC,
           "Failed to get Optional Configuration"
           "Response Message Elements \r\n");
           UtlShMemFreeConfigStatusRspBuf ((UINT1 *) pConfStatRsp);
           return CAPWAP_FAILURE;

           } */
        /* update Control Header */
        pConfigStatusResp->u1SeqNum =
            pConfigStatusReqMsg->capwapCtrlHdr.u1SeqNum;
        pConfigStatusResp->u2CapwapMsgElemenLen =
            (UINT2) (pConfigStatusResp->u2CapwapMsgElemenLen + (u4MsgLen + 3));
        pConfigStatusResp->u1NumMsgBlocks = 0;    /* flags always zero */
        pTxBuf = CAPWAP_ALLOCATE_CRU_BUF (CAPWAP_MAX_PKT_LEN, 0);
        if (pTxBuf == NULL)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Memory Allocation Failed\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Memory Allocation Failed\n"));
            UtlShMemFreeConfigStatusRspBuf ((UINT1 *) pConfigStatusResp);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
        pu1TxBuf = CAPWAP_BUF_IF_LINEAR (pTxBuf, 0, CAPWAP_MAX_PKT_LEN);
        if (pu1TxBuf == NULL)
        {
            pu1TxBuf = (UINT1 *) MemAllocMemBlk (CAPWAP_PKTBUF_POOLID);
            /* Kloc Fix Start */
            if (pu1TxBuf == NULL)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Memory Allocation Failed \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Memory Allocation Failed \r\n"));
                CAPWAP_RELEASE_CRU_BUF (pTxBuf);
                UtlShMemFreeConfigStatusRspBuf ((UINT1 *) pConfigStatusResp);
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
            /* Kloc Fix Ends */
            CAPWAP_COPY_FROM_BUF (pBuf, pu1TxBuf, 0, CAPWAP_MAX_PKT_LEN);
            u1IsNotLinear = OSIX_TRUE;
        }
        /*NOTE :- This part of code has been added for multi radio purpose,
         * because RF implementation is not done. 
         * so once the RF implementation for multi radio is completed, this
         * part of code will be removed*/
        MEMSET (&WssIfPMDB, 0, sizeof (WssIfPMDB));
        WssIfPMDB.u2ProfileId = pConfigStatusReqMsg->u2IntProfileId;
        pConfigStatusResp->u2IntProfileId = pConfigStatusReqMsg->u2IntProfileId;
        WssIfPMDB.tWtpPmAttState.wssPmWLCStatsSetDB.bWssPmWLCCapwapCfgReqSent =
            OSIX_TRUE;
        WssIfPMDB.tWtpPmAttState.
            wssPmWLCStatsSetDB.bWssPmWLCCapwapCfgLastSuccAttTime = OSIX_TRUE;
        if (WssIfProcessPMDBMsg (WSS_PM_WLC_CAPWAP_STATS_SET, &WssIfPMDB) !=
            OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to update Config Status Req Stats to PM \n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to update Config Status Req Stats to PM \n"));
        }
        if ((CapwapAssembleConfigStatusResponse
             (pConfigStatusResp->u2CapwapMsgElemenLen, pConfigStatusResp,
              pu1TxBuf)) == OSIX_FAILURE)
        {
            if (u1IsNotLinear == OSIX_TRUE)
            {
                MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pu1TxBuf);
            }
            CAPWAP_RELEASE_CRU_BUF (pTxBuf);
            UtlShMemFreeConfigStatusRspBuf ((UINT1 *) pConfigStatusResp);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }

        pu1TempBuf = pu1TxBuf + CAPWAP_MSG_OFFSET;
        CAPWAP_GET_2BYTE (pConfigStatusResp->u2CapwapMsgElemenLen, pu1TempBuf);

        if (u1IsNotLinear == OSIX_TRUE)
        {
            /* If the CRU buffer memory is not linear */
            CAPWAP_COPY_TO_BUF (pTxBuf, pu1TxBuf, 0,
                                (UINT4) (pConfigStatusResp->
                                         u2CapwapMsgElemenLen) +
                                CAPWAP_MSG_ELE_LEN);
            MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pu1TxBuf);
        }

        /* Transmit Configuration Status Response Message */
        i4Status = CapwapTxMessage (pTxBuf,
                                    pSessEntry,
                                    (UINT4)
                                    (pConfigStatusResp->u2CapwapMsgElemenLen +
                                     CAPWAP_MSG_ELE_LEN),
                                    WSS_CAPWAP_802_11_CTRL_PKT);
        if (i4Status == OSIX_SUCCESS)
        {
            /* Update the last transmitted sequence number */
            pSessEntry->lastProcesseddSeqNum = pConfigStatusResp->u1SeqNum;
            /* store the packet buffer pointer in
             * the remote session DB copy the pointer */
            pSessEntry->lastTransmittedPkt = pTxBuf;
            pSessEntry->lastTransmittedPktLen =
                (UINT4) (pConfigStatusResp->u2CapwapMsgElemenLen +
                         CAPWAP_MSG_ELE_LEN);
            pSessEntry->lastTransmitedSeqNum++;
            /* Start ChangeStatePendingTimer  Timer */
            CapwapTmrStart (pSessEntry, CAPWAP_CHANGESTATE_PENDING_TMR,
                            CHANGESTATE_PENDING_TIMEOUT);
            UtlShMemFreeConfigStatusRspBuf ((UINT1 *) pConfigStatusResp);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_SUCCESS;
        }
        else
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to transmit the packet \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to transmit the packet \r\n"));
            CAPWAP_RELEASE_CRU_BUF (pTxBuf);
            pSessEntry->lastTransmittedPkt = NULL;
            pSessEntry->lastTransmittedPktLen = 0;
            UtlShMemFreeConfigStatusRspBuf ((UINT1 *) pConfigStatusResp);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
    }
    else
    {

        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to validate Configuration Status  Req \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to validate Configuration Status  Req \r\n"));
        CAPWAP_TRC (CAPWAP_FSM_TRC, "Enter the CAPWAP DTLS TD State \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Enter the CAPWAP DTLS TD State \r\n"));
        pSessEntry->eCurrentState = CAPWAP_DTLSTD;
        if (CapwapShutDownDTLS (pSessEntry) == OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Disconnect"
                        "the Session \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to Disconnect" "the Session \r\n"));
        }

        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
        pWssIfCapwapDB->u4DestIp = pSessEntry->remoteIpAddr.u4_addr[0];
        pWssIfCapwapDB->u4DestPort = pSessEntry->u4RemoteCtrlPort;
        /* coverity fix begin */
        /*WssIfProcessCapwapDBMsg (WSS_CAPWAP_DELETE_RSM, pWssIfCapwapDB); */
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_DELETE_RSM, pWssIfCapwapDB) !=
            OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "WssIfProcessCapwapDBMsg"
                        "failed  \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "WssIfProcessCapwapDBMsg" "failed  \r\n"));
        }

        /* coverity fix end */
        if (u1IsNotLinear == OSIX_TRUE)
        {
            /* Kloc Fix Start */
            MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pRcvdBuf);
            CAPWAP_RELEASE_CRU_BUF (pBuf);
            /* Kloc Fix Ends */
        }
        UtlShMemFreeConfigStatusRspBuf ((UINT1 *) pConfigStatusResp);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

INT4
CapwapSetIeeeAntna (UINT1 *pRcvBuf,
                    tCapwapControlPacket * pCwMsg,
                    tConfigStatusRsp * pConfStatRsp, UINT2 u2MsgIndex)
{
    tRadioIfMsgStruct   RadioIfMsgStruct;
    UINT1              *pu1Buf = NULL;
    UINT2               u2NumOfRadios = 0;
    UINT1               u1Index = 0;
    UINT4               u4Offset;

    CAPWAP_FN_ENTRY ();
    MEMSET (&RadioIfMsgStruct, 0, sizeof (tRadioIfMsgStruct));
    pu1Buf = pRcvBuf;
    u2NumOfRadios = pCwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;

    CAPWAP_TRC1 (CAPWAP_MGMT_TRC, "CapwapSetIeeeAntna: Number of Radios"
                 "Received is %d \r\n", u2NumOfRadios);
    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                  "CapwapSetIeeeAntna: Number of Radios" "Received is %d \r\n",
                  u2NumOfRadios));

    for (u1Index = 1; u1Index <= u2NumOfRadios; u1Index++)
    {
        u4Offset = pCwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[u1Index - 1];
        pRcvBuf = pu1Buf;
        pRcvBuf += u4Offset;
        if (pCwMsg->capwapCtrlHdr.u4MsgType == WSS_RADIOIF_CONFIG_STATUS_REQ)
        {
            CAPWAP_GET_2BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusReq.
                              RadioIfAntenna.u2MessageType, pRcvBuf);
            CAPWAP_GET_2BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusReq.
                              RadioIfAntenna.u2MessageLength, pRcvBuf);
            CAPWAP_GET_1BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusReq.
                              RadioIfAntenna.u1RadioId, pRcvBuf);
            CAPWAP_GET_1BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusReq.
                              RadioIfAntenna.u1ReceiveDiversity, pRcvBuf);
            CAPWAP_GET_1BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusReq.
                              RadioIfAntenna.u1AntennaMode, pRcvBuf);
            CAPWAP_GET_1BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusReq.
                              RadioIfAntenna.u1CurrentTxAntenna, pRcvBuf);
            CAPWAP_GET_NBYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusReq.
                              RadioIfAntenna.au1AntennaSelection, pRcvBuf,
                              RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusReq.
                              RadioIfAntenna.u1CurrentTxAntenna);
            RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusReq.
                RadioIfAntenna.isPresent = OSIX_TRUE;
            RadioIfMsgStruct.unRadioIfMsg.
                RadioIfConfigStatusReq.u2WtpInternalId = pCwMsg->u2IntProfileId;
            if (WssIfProcessRadioIfMsg
                (WSS_RADIOIF_CONFIG_STATUS_REQ,
                 &RadioIfMsgStruct) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Set the IEEE"
                            "Antena in Radio IF \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to Set the IEEE"
                              "Antena in Radio IF \r\n"));
                return OSIX_FAILURE;
            }

            if (RadioIfMsgStruct.unRadioIfMsg.
                RadioIfConfigStatusReq.RadioIfAntenna.isPresent == OSIX_TRUE)
            {
                pConfStatRsp->RadioIfAntenna.isPresent = OSIX_TRUE;
                if (CapwapGetIeeeAntena (&pConfStatRsp->RadioIfAntenna,
                                         &pConfStatRsp->u2CapwapMsgElemenLen,
                                         pCwMsg->u2IntProfileId) ==
                    OSIX_FAILURE)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Get IEEE"
                                "Antenna Info \r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Failed to Get IEEE" "Antenna Info \r\n"));
                    return OSIX_FAILURE;
                }
            }
        }
        else
        {
            CAPWAP_GET_2BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusRsp.
                              RadioIfAntenna.u2MessageType, pRcvBuf);
            CAPWAP_GET_2BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusRsp.
                              RadioIfAntenna.u2MessageLength, pRcvBuf);
            CAPWAP_GET_1BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusRsp.
                              RadioIfAntenna.u1RadioId, pRcvBuf);
            CAPWAP_GET_1BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusRsp.
                              RadioIfAntenna.u1ReceiveDiversity, pRcvBuf);
            CAPWAP_GET_1BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusRsp.
                              RadioIfAntenna.u1AntennaMode, pRcvBuf);
            CAPWAP_GET_NBYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusRsp.
                              RadioIfAntenna.au1AntennaSelection, pRcvBuf,
                              RADIOIF_ANTENNA_LIST_INDEX);
            RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusRsp.
                RadioIfAntenna.isPresent = OSIX_TRUE;
            RadioIfMsgStruct.unRadioIfMsg.
                RadioIfConfigStatusRsp.u2WtpInternalId = pCwMsg->u2IntProfileId;
            if (WssIfProcessRadioIfMsg
                (WSS_RADIOIF_CONFIG_STATUS_RSP,
                 &RadioIfMsgStruct) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Set the IEEE"
                            "Antena in Radio IF \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to Set the IEEE"
                              "Antena in Radio IF \r\n"));
                return OSIX_FAILURE;
            }
        }
    }
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

INT4
CapwapSetIeeeDSC (UINT1 *pRcvBuf,
                  tCapwapControlPacket * pCwMsg,
                  tConfigStatusRsp * pConfStatRsp, UINT2 u2MsgIndex)
{
    tRadioIfMsgStruct   RadioIfMsgStruct;
    UINT1              *pu1Buf = NULL;
    UINT2               u2NumOfRadios = 0;
    UINT1               u1Index = 0;
    UINT4               u4Offset;

    CAPWAP_FN_ENTRY ();
    MEMSET (&RadioIfMsgStruct, 0, sizeof (tRadioIfMsgStruct));
    pu1Buf = pRcvBuf;
    u2NumOfRadios = pCwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;

    for (u1Index = 1; u1Index <= u2NumOfRadios; u1Index++)
    {
        u4Offset = pCwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[u1Index - 1];
        pRcvBuf = pu1Buf;
        pRcvBuf += u4Offset;

        if (pCwMsg->capwapCtrlHdr.u4MsgType == WSS_RADIOIF_CONFIG_STATUS_REQ)
        {
            CAPWAP_GET_2BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusReq.
                              RadioIfDSSPhy.u2MessageType, pRcvBuf);
            CAPWAP_GET_2BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusReq.
                              RadioIfDSSPhy.u2MessageLength, pRcvBuf);
            CAPWAP_GET_1BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusReq.RadioIfDSSPhy.
                              u1RadioId, pRcvBuf);
            CAPWAP_GET_1BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusRsp.RadioIfDSSPhy.
                              u1Reserved, pRcvBuf);
            CAPWAP_GET_1BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusReq.
                              RadioIfDSSPhy.i1CurrentChannel, pRcvBuf);
            CAPWAP_GET_1BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusReq.
                              RadioIfDSSPhy.i1CurrentCCAMode, pRcvBuf);
            CAPWAP_GET_INT_4BYTE ((RadioIfMsgStruct.
                                   unRadioIfMsg.RadioIfConfigStatusReq.
                                   RadioIfDSSPhy.i4EDThreshold), pRcvBuf);

            RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusReq.
                RadioIfDSSPhy.isPresent = OSIX_TRUE;
            RadioIfMsgStruct.unRadioIfMsg.
                RadioIfConfigStatusReq.u2WtpInternalId = pCwMsg->u2IntProfileId;
            if (WssIfProcessRadioIfMsg
                (WSS_RADIOIF_CONFIG_STATUS_REQ,
                 &RadioIfMsgStruct) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Set IEEE"
                            "DSC in Radio IF \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to Set IEEE" "DSC in Radio IF \r\n"));
                return OSIX_FAILURE;
            }
            if (RadioIfMsgStruct.unRadioIfMsg.
                RadioIfConfigStatusReq.RadioIfDSSPhy.isPresent == OSIX_TRUE)
            {
                pConfStatRsp->RadioIfDSSPhy.isPresent = OSIX_TRUE;
                if (CapwapGetIeeeDsc (&pConfStatRsp->RadioIfDSSPhy,
                                      &pConfStatRsp->u2CapwapMsgElemenLen,
                                      pCwMsg->u2IntProfileId) == OSIX_FAILURE)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Get Radio"
                                "DSS Phy information \r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Failed to Get Radio"
                                  "DSS Phy information \r\n"));
                    return OSIX_FAILURE;
                }

            }
        }
        else
        {
            CAPWAP_GET_2BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusRsp.
                              RadioIfDSSPhy.u2MessageType, pRcvBuf);
            CAPWAP_GET_2BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusRsp.
                              RadioIfDSSPhy.u2MessageLength, pRcvBuf);
            CAPWAP_GET_1BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusRsp.RadioIfDSSPhy.
                              u1RadioId, pRcvBuf);
            CAPWAP_GET_1BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusRsp.RadioIfDSSPhy.
                              u1Reserved, pRcvBuf);
            CAPWAP_GET_1BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusRsp.
                              RadioIfDSSPhy.i1CurrentChannel, pRcvBuf);
            CAPWAP_GET_1BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusRsp.
                              RadioIfDSSPhy.i1CurrentCCAMode, pRcvBuf);
            CAPWAP_GET_INT_4BYTE (RadioIfMsgStruct.
                                  unRadioIfMsg.RadioIfConfigStatusRsp.
                                  RadioIfDSSPhy.i4EDThreshold, pRcvBuf);
            RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusRsp.
                RadioIfDSSPhy.isPresent = OSIX_TRUE;
            RadioIfMsgStruct.unRadioIfMsg.
                RadioIfConfigStatusRsp.u2WtpInternalId = pCwMsg->u2IntProfileId;
            if (WssIfProcessRadioIfMsg
                (WSS_RADIOIF_CONFIG_STATUS_RSP,
                 &RadioIfMsgStruct) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Set IEEE DSC"
                            "in Radio IF \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to Set IEEE DSC" "in Radio IF \r\n"));

                return OSIX_FAILURE;
            }

        }
    }
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

INT4
CapwapSetIeeeInfoElem (UINT1 *pRcvBuf,
                       tCapwapControlPacket * pCwMsg,
                       tConfigStatusRsp * pConfStatRsp, UINT2 u2MsgIndex)
{
    tRadioIfMsgStruct   RadioIfMsgStruct;
    UINT1              *pu1Buf = NULL;
    UINT2               u2NumOfRadios = 0;
    UINT1               u1Index = 0;
    UINT4               u4Offset;

    CAPWAP_FN_ENTRY ();
    MEMSET (&RadioIfMsgStruct, 0, sizeof (tRadioIfMsgStruct));
    pu1Buf = pRcvBuf;
    u2NumOfRadios = pCwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;

    for (u1Index = 1; u1Index <= u2NumOfRadios; u1Index++)
    {
        u4Offset = pCwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[u1Index - 1];
        pRcvBuf = pu1Buf;
        pRcvBuf += u4Offset;

        CAPWAP_GET_2BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusReq.
                          RadioIfInfoElem.u2MessageType, pRcvBuf);
        CAPWAP_GET_2BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusReq.
                          RadioIfInfoElem.u2MessageLength, pRcvBuf);

        CAPWAP_GET_1BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusReq.
                          RadioIfInfoElem.u1RadioId, pRcvBuf);
        CAPWAP_GET_1BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusReq.
                          RadioIfInfoElem.u1WlanId, pRcvBuf);
        CAPWAP_GET_1BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusReq.
                          RadioIfInfoElem.u1BPFlag, pRcvBuf);
        CAPWAP_GET_1BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusReq.
                          RadioIfInfoElem.u1EleId, pRcvBuf);
        switch (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusReq.
                RadioIfInfoElem.u1EleId)
        {
            case DOT11_INFO_ELEM_HTCAPABILITY:

                if (pCwMsg->capwapCtrlHdr.u4MsgType ==
                    WSS_RADIOIF_CONFIG_STATUS_REQ)
                {
                    RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusReq.
                        RadioIfInfoElem.unInfoElem.HTCapability.u1ElemId =
                        RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusReq.
                        RadioIfInfoElem.u1EleId;
                    CAPWAP_GET_1BYTE (RadioIfMsgStruct.
                                      unRadioIfMsg.RadioIfConfigStatusReq.
                                      RadioIfInfoElem.unInfoElem.HTCapability.
                                      u1ElemLen, pRcvBuf);
                    CAPWAP_GET_2BYTE (RadioIfMsgStruct.
                                      unRadioIfMsg.RadioIfConfigStatusReq.
                                      RadioIfInfoElem.unInfoElem.HTCapability.
                                      u2HTCapInfo, pRcvBuf);
                    CAPWAP_GET_1BYTE (RadioIfMsgStruct.
                                      unRadioIfMsg.RadioIfConfigStatusReq.
                                      RadioIfInfoElem.unInfoElem.HTCapability.
                                      u1AMPDUParam, pRcvBuf);
                    CAPWAP_GET_NBYTE (RadioIfMsgStruct.
                                      unRadioIfMsg.RadioIfConfigStatusReq.
                                      RadioIfInfoElem.unInfoElem.HTCapability.
                                      au1SuppMCSSet, pRcvBuf, 16);
                    CAPWAP_GET_2BYTE (RadioIfMsgStruct.
                                      unRadioIfMsg.RadioIfConfigStatusReq.
                                      RadioIfInfoElem.unInfoElem.HTCapability.
                                      u2HTExtCap, pRcvBuf);
                    CAPWAP_GET_4BYTE (RadioIfMsgStruct.
                                      unRadioIfMsg.RadioIfConfigStatusReq.
                                      RadioIfInfoElem.unInfoElem.HTCapability.
                                      u4TranBeamformCap, pRcvBuf);
                    CAPWAP_GET_1BYTE (RadioIfMsgStruct.
                                      unRadioIfMsg.RadioIfConfigStatusReq.
                                      RadioIfInfoElem.unInfoElem.HTCapability.
                                      u1ASELCap, pRcvBuf);
                    RadioIfMsgStruct.unRadioIfMsg.
                        RadioIfConfigStatusReq.RadioIfInfoElem.isPresent =
                        OSIX_TRUE;
                    RadioIfMsgStruct.unRadioIfMsg.
                        RadioIfConfigStatusReq.RadioIfInfoElem.unInfoElem.
                        HTCapability.isOptional = OSIX_TRUE;
                    RadioIfMsgStruct.unRadioIfMsg.
                        RadioIfConfigStatusReq.u2WtpInternalId =
                        pCwMsg->u2IntProfileId;
                    if (WssIfProcessRadioIfMsg
                        (WSS_RADIOIF_CONFIG_STATUS_REQ,
                         &RadioIfMsgStruct) != OSIX_SUCCESS)
                    {
                        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Set IEEE "
                                    "Info in Radio IF \r\n");
                        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                      "Failed to Set IEEE "
                                      "Info in Radio IF \r\n"));
                        return OSIX_FAILURE;
                    }
                    if (RadioIfMsgStruct.unRadioIfMsg.
                        RadioIfConfigStatusReq.RadioIfInfoElem.isPresent ==
                        OSIX_TRUE)
                    {
                        pConfStatRsp->RadioIfInfoElem.isPresent = OSIX_TRUE;
                        pConfStatRsp->RadioIfInfoElem.unInfoElem.
                            HTCapability.u1ElemId =
                            DOT11_INFO_ELEM_HTCAPABILITY;
                    }
                }
                else
                {
                    RadioIfMsgStruct.unRadioIfMsg.
                        RadioIfConfigStatusRsp.RadioIfInfoElem.u2MessageType =
                        RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusReq.
                        RadioIfInfoElem.u2MessageType;
                    RadioIfMsgStruct.unRadioIfMsg.
                        RadioIfConfigStatusRsp.RadioIfInfoElem.u2MessageLength =
                        RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusReq.
                        RadioIfInfoElem.u2MessageLength;
                    RadioIfMsgStruct.unRadioIfMsg.
                        RadioIfConfigStatusRsp.RadioIfInfoElem.u1RadioId =
                        RadioIfMsgStruct.
                        unRadioIfMsg.RadioIfConfigStatusReq.RadioIfInfoElem.
                        u1RadioId;
                    RadioIfMsgStruct.unRadioIfMsg.
                        RadioIfConfigStatusRsp.RadioIfInfoElem.u1WlanId =
                        RadioIfMsgStruct.
                        unRadioIfMsg.RadioIfConfigStatusReq.RadioIfInfoElem.
                        u1WlanId;
                    RadioIfMsgStruct.unRadioIfMsg.
                        RadioIfConfigStatusRsp.RadioIfInfoElem.u1BPFlag =
                        RadioIfMsgStruct.
                        unRadioIfMsg.RadioIfConfigStatusReq.RadioIfInfoElem.
                        u1BPFlag;
                    RadioIfMsgStruct.unRadioIfMsg.
                        RadioIfConfigStatusRsp.RadioIfInfoElem.u1EleId =
                        RadioIfMsgStruct.
                        unRadioIfMsg.RadioIfConfigStatusReq.RadioIfInfoElem.
                        u1EleId;
                    RadioIfMsgStruct.unRadioIfMsg.
                        RadioIfConfigStatusRsp.RadioIfInfoElem.unInfoElem.
                        HTCapability.u1ElemId =
                        RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusReq.
                        RadioIfInfoElem.u1EleId;
                    CAPWAP_GET_1BYTE (RadioIfMsgStruct.
                                      unRadioIfMsg.RadioIfConfigStatusRsp.
                                      RadioIfInfoElem.unInfoElem.HTCapability.
                                      u1ElemLen, pRcvBuf);
                    CAPWAP_GET_2BYTE (RadioIfMsgStruct.
                                      unRadioIfMsg.RadioIfConfigStatusRsp.
                                      RadioIfInfoElem.unInfoElem.HTCapability.
                                      u2HTCapInfo, pRcvBuf);
                    CAPWAP_GET_1BYTE (RadioIfMsgStruct.
                                      unRadioIfMsg.RadioIfConfigStatusRsp.
                                      RadioIfInfoElem.unInfoElem.HTCapability.
                                      u1AMPDUParam, pRcvBuf);
                    CAPWAP_GET_NBYTE (RadioIfMsgStruct.
                                      unRadioIfMsg.RadioIfConfigStatusRsp.
                                      RadioIfInfoElem.unInfoElem.HTCapability.
                                      au1SuppMCSSet, pRcvBuf, 16);
                    CAPWAP_GET_2BYTE (RadioIfMsgStruct.
                                      unRadioIfMsg.RadioIfConfigStatusRsp.
                                      RadioIfInfoElem.unInfoElem.HTCapability.
                                      u2HTExtCap, pRcvBuf);
                    CAPWAP_GET_4BYTE (RadioIfMsgStruct.
                                      unRadioIfMsg.RadioIfConfigStatusRsp.
                                      RadioIfInfoElem.unInfoElem.HTCapability.
                                      u4TranBeamformCap, pRcvBuf);
                    CAPWAP_GET_1BYTE (RadioIfMsgStruct.
                                      unRadioIfMsg.RadioIfConfigStatusRsp.
                                      RadioIfInfoElem.unInfoElem.HTCapability.
                                      u1ASELCap, pRcvBuf);

                    RadioIfMsgStruct.unRadioIfMsg.
                        RadioIfConfigStatusRsp.RadioIfInfoElem.isPresent =
                        OSIX_TRUE;
                    RadioIfMsgStruct.unRadioIfMsg.
                        RadioIfConfigStatusRsp.u2WtpInternalId =
                        pCwMsg->u2IntProfileId;
                    if (WssIfProcessRadioIfMsg
                        (WSS_RADIOIF_CONFIG_STATUS_RSP,
                         &RadioIfMsgStruct) != OSIX_SUCCESS)
                    {
                        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Set "
                                    "IEEE Info in Radio IF \r\n");
                        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                      "Failed to Set "
                                      "IEEE Info in Radio IF \r\n"));
                        return OSIX_FAILURE;
                    }

                }
                break;
            case DOT11_INFO_ELEM_HTOPERATION:

                if (pCwMsg->capwapCtrlHdr.u4MsgType ==
                    WSS_RADIOIF_CONFIG_STATUS_REQ)
                {
                    RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusReq.
                        RadioIfInfoElem.unInfoElem.HTOperation.u1ElemId =
                        RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusReq.
                        RadioIfInfoElem.u1EleId;
                    CAPWAP_GET_1BYTE (RadioIfMsgStruct.
                                      unRadioIfMsg.RadioIfConfigStatusReq.
                                      RadioIfInfoElem.unInfoElem.HTOperation.
                                      u1ElemLen, pRcvBuf);
                    CAPWAP_GET_1BYTE (RadioIfMsgStruct.
                                      unRadioIfMsg.RadioIfConfigStatusReq.
                                      RadioIfInfoElem.unInfoElem.HTOperation.
                                      u1PrimaryCh, pRcvBuf);
                    CAPWAP_GET_NBYTE (RadioIfMsgStruct.
                                      unRadioIfMsg.RadioIfConfigStatusReq.
                                      RadioIfInfoElem.unInfoElem.HTOperation.
                                      au1HTOpeInfo, pRcvBuf, WSSMAC_HTOPE_INFO);
                    CAPWAP_GET_NBYTE (RadioIfMsgStruct.
                                      unRadioIfMsg.RadioIfConfigStatusReq.
                                      RadioIfInfoElem.unInfoElem.HTOperation.
                                      au1BasicMCSSet, pRcvBuf,
                                      WSSMAC_BASIC_MCS_SET);
                    RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusReq.
                        RadioIfInfoElem.isPresent = OSIX_TRUE;
                    RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusReq.
                        RadioIfInfoElem.unInfoElem.HTOperation.isOptional =
                        OSIX_TRUE;
                    RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusReq.
                        u2WtpInternalId = pCwMsg->u2IntProfileId;
                    if (WssIfProcessRadioIfMsg
                        (WSS_RADIOIF_CONFIG_STATUS_REQ,
                         &RadioIfMsgStruct) != OSIX_SUCCESS)
                    {
                        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Set IEEE "
                                    "Info in Radio IF \r\n");
                        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                      "Failed to Set IEEE "
                                      "Info in Radio IF \r\n"));
                        return OSIX_FAILURE;
                    }
                    if (RadioIfMsgStruct.unRadioIfMsg.
                        RadioIfConfigStatusReq.RadioIfInfoElem.isPresent ==
                        OSIX_TRUE)
                    {
                        pConfStatRsp->RadioIfInfoElem.isPresent = OSIX_TRUE;
                        pConfStatRsp->RadioIfInfoElem.unInfoElem.
                            HTOperation.u1ElemId = DOT11_INFO_ELEM_HTOPERATION;
                    }
                }
                else
                {
                    RadioIfMsgStruct.unRadioIfMsg.
                        RadioIfConfigStatusRsp.RadioIfInfoElem.u2MessageType =
                        RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusReq.
                        RadioIfInfoElem.u2MessageType;
                    RadioIfMsgStruct.unRadioIfMsg.
                        RadioIfConfigStatusRsp.RadioIfInfoElem.u2MessageLength =
                        RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusReq.
                        RadioIfInfoElem.u2MessageLength;
                    RadioIfMsgStruct.unRadioIfMsg.
                        RadioIfConfigStatusRsp.RadioIfInfoElem.u1RadioId =
                        RadioIfMsgStruct.
                        unRadioIfMsg.RadioIfConfigStatusReq.RadioIfInfoElem.
                        u1RadioId;
                    RadioIfMsgStruct.unRadioIfMsg.
                        RadioIfConfigStatusRsp.RadioIfInfoElem.u1WlanId =
                        RadioIfMsgStruct.
                        unRadioIfMsg.RadioIfConfigStatusReq.RadioIfInfoElem.
                        u1WlanId;
                    RadioIfMsgStruct.unRadioIfMsg.
                        RadioIfConfigStatusRsp.RadioIfInfoElem.u1BPFlag =
                        RadioIfMsgStruct.
                        unRadioIfMsg.RadioIfConfigStatusReq.RadioIfInfoElem.
                        u1BPFlag;
                    RadioIfMsgStruct.unRadioIfMsg.
                        RadioIfConfigStatusRsp.RadioIfInfoElem.u1EleId =
                        RadioIfMsgStruct.
                        unRadioIfMsg.RadioIfConfigStatusReq.RadioIfInfoElem.
                        u1EleId;
                    RadioIfMsgStruct.unRadioIfMsg.
                        RadioIfConfigStatusRsp.RadioIfInfoElem.unInfoElem.
                        HTOperation.u1ElemId =
                        RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusReq.
                        RadioIfInfoElem.u1EleId;
                    CAPWAP_GET_1BYTE (RadioIfMsgStruct.
                                      unRadioIfMsg.RadioIfConfigStatusRsp.
                                      RadioIfInfoElem.unInfoElem.HTOperation.
                                      u1ElemLen, pRcvBuf);
                    CAPWAP_GET_1BYTE (RadioIfMsgStruct.
                                      unRadioIfMsg.RadioIfConfigStatusRsp.
                                      RadioIfInfoElem.unInfoElem.HTOperation.
                                      u1PrimaryCh, pRcvBuf);
                    CAPWAP_GET_NBYTE (RadioIfMsgStruct.
                                      unRadioIfMsg.RadioIfConfigStatusRsp.
                                      RadioIfInfoElem.unInfoElem.HTOperation.
                                      au1HTOpeInfo, pRcvBuf, RADIO_HTOPE_INFO);
                    CAPWAP_GET_NBYTE (RadioIfMsgStruct.
                                      unRadioIfMsg.RadioIfConfigStatusRsp.
                                      RadioIfInfoElem.unInfoElem.HTOperation.
                                      au1BasicMCSSet, pRcvBuf,
                                      RADIO_BASIC_MCS_SET);
                    RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusRsp.
                        RadioIfInfoElem.isPresent = OSIX_TRUE;
                    RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusRsp.
                        u2WtpInternalId = pCwMsg->u2IntProfileId;
                    if (WssIfProcessRadioIfMsg
                        (WSS_RADIOIF_CONFIG_STATUS_RSP,
                         &RadioIfMsgStruct) != OSIX_SUCCESS)
                    {
                        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Set "
                                    "IEEE Info in Radio IF \r\n");
                        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                      "Failed to Set "
                                      "IEEE Info in Radio IF \r\n"));

                        return OSIX_FAILURE;
                    }
                }
                break;

            case DOT11AC_INFO_ELEM_VHTCAPABILITY:

                if (pCwMsg->capwapCtrlHdr.u4MsgType ==
                    WSS_RADIOIF_CONFIG_STATUS_REQ)
                {
                    RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusReq.
                        RadioIfInfoElem.unInfoElem.VHTCapability.u1ElemId =
                        RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusReq.
                        RadioIfInfoElem.u1EleId;
                    CAPWAP_GET_1BYTE (RadioIfMsgStruct.
                                      unRadioIfMsg.RadioIfConfigStatusReq.
                                      RadioIfInfoElem.unInfoElem.VHTCapability.
                                      u1ElemLen, pRcvBuf);
                    CAPWAP_GET_4BYTE (RadioIfMsgStruct.
                                      unRadioIfMsg.RadioIfConfigStatusReq.
                                      RadioIfInfoElem.unInfoElem.VHTCapability.
                                      u4VhtCapInfo, pRcvBuf);
                    CAPWAP_GET_NBYTE (RadioIfMsgStruct.
                                      unRadioIfMsg.RadioIfConfigStatusReq.
                                      RadioIfInfoElem.unInfoElem.VHTCapability.
                                      au1SuppMCS, pRcvBuf,
                                      DOT11AC_VHT_CAP_MCS_LEN);

                    RadioIfMsgStruct.unRadioIfMsg.
                        RadioIfConfigStatusReq.RadioIfInfoElem.isPresent =
                        OSIX_TRUE;
                    RadioIfMsgStruct.unRadioIfMsg.
                        RadioIfConfigStatusReq.RadioIfInfoElem.unInfoElem.
                        VHTCapability.isOptional = OSIX_TRUE;
                    RadioIfMsgStruct.unRadioIfMsg.
                        RadioIfConfigStatusReq.u2WtpInternalId =
                        pCwMsg->u2IntProfileId;
                    if (WssIfProcessRadioIfMsg
                        (WSS_RADIOIF_CONFIG_STATUS_REQ,
                         &RadioIfMsgStruct) != OSIX_SUCCESS)
                    {
                        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Set IEEE "
                                    "Info VHT Capapbility in Radio IF \r\n");
                        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                      "Failed to Set IEEE "
                                      "Info VHT Capapbility in Radio IF \r\n"));
                        return OSIX_FAILURE;
                    }
                    if (RadioIfMsgStruct.unRadioIfMsg.
                        RadioIfConfigStatusReq.RadioIfInfoElem.isPresent ==
                        OSIX_TRUE)
                    {
                        pConfStatRsp->RadioIfInfoElem.isPresent = OSIX_TRUE;
                        pConfStatRsp->RadioIfInfoElem.unInfoElem.
                            VHTCapability.u1ElemId =
                            DOT11AC_INFO_ELEM_VHTCAPABILITY;
                    }

                }
                else
                {
                    RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusRsp.
                        RadioIfInfoElem.u2MessageType =
                        RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusReq.
                        RadioIfInfoElem.u2MessageType;
                    RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusRsp.
                        RadioIfInfoElem.u2MessageLength =
                        RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusReq.
                        RadioIfInfoElem.u2MessageLength;
                    RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusRsp.
                        RadioIfInfoElem.u1RadioId =
                        RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusReq.
                        RadioIfInfoElem.u1RadioId;
                    RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusRsp.
                        RadioIfInfoElem.u1WlanId =
                        RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusReq.
                        RadioIfInfoElem.u1WlanId;
                    RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusRsp.
                        RadioIfInfoElem.u1BPFlag =
                        RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusReq.
                        RadioIfInfoElem.u1BPFlag;
                    RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusRsp.
                        RadioIfInfoElem.u1EleId =
                        RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusReq.
                        RadioIfInfoElem.u1EleId;
                    RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusRsp.
                        RadioIfInfoElem.unInfoElem.VHTCapability.u1ElemId =
                        RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusReq.
                        RadioIfInfoElem.u1EleId;
                    CAPWAP_GET_1BYTE (RadioIfMsgStruct.
                                      unRadioIfMsg.RadioIfConfigStatusRsp.
                                      RadioIfInfoElem.unInfoElem.VHTCapability.
                                      u1ElemLen, pRcvBuf);
                    CAPWAP_GET_4BYTE (RadioIfMsgStruct.
                                      unRadioIfMsg.RadioIfConfigStatusRsp.
                                      RadioIfInfoElem.unInfoElem.VHTCapability.
                                      u4VhtCapInfo, pRcvBuf);
                    CAPWAP_GET_NBYTE (RadioIfMsgStruct.
                                      unRadioIfMsg.RadioIfConfigStatusRsp.
                                      RadioIfInfoElem.unInfoElem.VHTCapability.
                                      au1SuppMCS, pRcvBuf,
                                      DOT11AC_VHT_CAP_MCS_LEN);

                    RadioIfMsgStruct.unRadioIfMsg.
                        RadioIfConfigStatusRsp.RadioIfInfoElem.isPresent =
                        OSIX_TRUE;
                    RadioIfMsgStruct.unRadioIfMsg.
                        RadioIfConfigStatusRsp.u2WtpInternalId =
                        pCwMsg->u2IntProfileId;
                    if (WssIfProcessRadioIfMsg
                        (WSS_RADIOIF_CONFIG_STATUS_RSP,
                         &RadioIfMsgStruct) != OSIX_SUCCESS)
                    {
                        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                    "Failed to Set IEEE Info"
                                    "in Radio IF \r\n");
                        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                      "Failed to Set IEEE Info"
                                      "in Radio IF \r\n"));
                        return OSIX_FAILURE;
                    }

                }

                break;

            case DOT11AC_INFO_ELEM_VHTOPERATION:

                if (pCwMsg->capwapCtrlHdr.u4MsgType ==
                    WSS_RADIOIF_CONFIG_STATUS_REQ)
                {
                    RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusReq.
                        RadioIfInfoElem.unInfoElem.VHTCapability.u1ElemId =
                        RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusReq.
                        RadioIfInfoElem.u1EleId;
                    CAPWAP_GET_1BYTE (RadioIfMsgStruct.
                                      unRadioIfMsg.RadioIfConfigStatusReq.
                                      RadioIfInfoElem.unInfoElem.VHTOperation.
                                      u1ElemLen, pRcvBuf);
                    CAPWAP_GET_NBYTE (RadioIfMsgStruct.
                                      unRadioIfMsg.RadioIfConfigStatusReq.
                                      RadioIfInfoElem.unInfoElem.VHTOperation.
                                      au1VhtOperInfo, pRcvBuf,
                                      DOT11AC_VHT_OPER_INFO_LEN);
                    CAPWAP_GET_2BYTE (RadioIfMsgStruct.
                                      unRadioIfMsg.RadioIfConfigStatusReq.
                                      RadioIfInfoElem.unInfoElem.VHTOperation.
                                      u2BasicMCS, pRcvBuf);

                    RadioIfMsgStruct.unRadioIfMsg.
                        RadioIfConfigStatusReq.RadioIfInfoElem.isPresent =
                        OSIX_TRUE;
                    RadioIfMsgStruct.unRadioIfMsg.
                        RadioIfConfigStatusReq.RadioIfInfoElem.u1EleId =
                        DOT11AC_INFO_ELEM_VHTOPERATION;
                    RadioIfMsgStruct.unRadioIfMsg.
                        RadioIfConfigStatusReq.RadioIfInfoElem.unInfoElem.
                        VHTOperation.isOptional = OSIX_TRUE;
                    RadioIfMsgStruct.unRadioIfMsg.
                        RadioIfConfigStatusReq.u2WtpInternalId =
                        pCwMsg->u2IntProfileId;
                    if (WssIfProcessRadioIfMsg
                        (WSS_RADIOIF_CONFIG_STATUS_REQ,
                         &RadioIfMsgStruct) != OSIX_SUCCESS)
                    {
                        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Set IEEE "
                                    "VHT Operation Info in Radio IF \r\n");
                        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                      "Failed to Set IEEE "
                                      "VHT Operation Info in Radio IF \r\n"));
                        return OSIX_FAILURE;
                    }
                    if (RadioIfMsgStruct.unRadioIfMsg.
                        RadioIfConfigStatusReq.RadioIfInfoElem.isPresent ==
                        OSIX_TRUE)
                    {
                        pConfStatRsp->RadioIfInfoElem.isPresent = OSIX_TRUE;
                        pConfStatRsp->RadioIfInfoElem.unInfoElem.
                            VHTOperation.u1ElemId =
                            DOT11AC_INFO_ELEM_VHTOPERATION;
                    }
                }
                else
                {
                    RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusRsp.
                        RadioIfInfoElem.u2MessageType =
                        RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusReq.
                        RadioIfInfoElem.u2MessageType;
                    RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusRsp.
                        RadioIfInfoElem.u2MessageLength =
                        RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusReq.
                        RadioIfInfoElem.u2MessageLength;
                    RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusRsp.
                        RadioIfInfoElem.u1RadioId =
                        RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusReq.
                        RadioIfInfoElem.u1RadioId;
                    RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusRsp.
                        RadioIfInfoElem.u1WlanId =
                        RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusReq.
                        RadioIfInfoElem.u1WlanId;
                    RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusRsp.
                        RadioIfInfoElem.u1BPFlag =
                        RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusReq.
                        RadioIfInfoElem.u1BPFlag;
                    RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusRsp.
                        RadioIfInfoElem.u1EleId =
                        RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusReq.
                        RadioIfInfoElem.u1EleId;
                    RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusRsp.
                        RadioIfInfoElem.unInfoElem.VHTOperation.u1ElemId =
                        RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusReq.
                        RadioIfInfoElem.u1EleId;
                    CAPWAP_GET_1BYTE (RadioIfMsgStruct.
                                      unRadioIfMsg.RadioIfConfigStatusRsp.
                                      RadioIfInfoElem.unInfoElem.VHTOperation.
                                      u1ElemLen, pRcvBuf);
                    CAPWAP_GET_NBYTE (RadioIfMsgStruct.
                                      unRadioIfMsg.RadioIfConfigStatusRsp.
                                      RadioIfInfoElem.unInfoElem.VHTOperation.
                                      au1VhtOperInfo, pRcvBuf,
                                      DOT11AC_VHT_OPER_INFO_LEN);
                    CAPWAP_GET_2BYTE (RadioIfMsgStruct.
                                      unRadioIfMsg.RadioIfConfigStatusRsp.
                                      RadioIfInfoElem.unInfoElem.VHTOperation.
                                      u2BasicMCS, pRcvBuf);

                    RadioIfMsgStruct.unRadioIfMsg.
                        RadioIfConfigStatusRsp.RadioIfInfoElem.isPresent =
                        OSIX_TRUE;
                    RadioIfMsgStruct.unRadioIfMsg.
                        RadioIfConfigStatusRsp.u2WtpInternalId =
                        pCwMsg->u2IntProfileId;
                    if (WssIfProcessRadioIfMsg
                        (WSS_RADIOIF_CONFIG_STATUS_RSP,
                         &RadioIfMsgStruct) != OSIX_SUCCESS)
                    {
                        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                    "Failed to Set IEEE Info "
                                    "in Radio IF \r\n");
                        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                      "Failed to Set IEEE Info "
                                      "in Radio IF \r\n"));
                        return OSIX_FAILURE;
                    }

                }

                break;

            default:
                break;
        }
    }
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

INT4
CapwapSetMacOperation (UINT1 *pRcvBuf,
                       tCapwapControlPacket * pCwMsg,
                       tConfigStatusRsp * pConfStatRsp, UINT2 u2MsgIndex)
{
    tRadioIfMsgStruct   RadioIfMsgStruct;
    UINT1              *pu1Buf = NULL;
    UINT2               u2NumOfRadios = 0;
    UINT1               u1Index = 0;
    UINT4               u4Offset;

    CAPWAP_FN_ENTRY ();
    MEMSET (&RadioIfMsgStruct, 0, sizeof (tRadioIfMsgStruct));
    pu1Buf = pRcvBuf;
    u2NumOfRadios = pCwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;

    for (u1Index = 1; u1Index <= u2NumOfRadios; u1Index++)
    {
        u4Offset = pCwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[u1Index - 1];
        pRcvBuf = pu1Buf;
        pRcvBuf += u4Offset;

        if (pCwMsg->capwapCtrlHdr.u4MsgType == WSS_RADIOIF_CONFIG_STATUS_REQ)
        {
            CAPWAP_GET_2BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusReq.
                              RadioIfMacOperation.u2MessageType, pRcvBuf);
            CAPWAP_GET_2BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusReq.
                              RadioIfMacOperation.u2MessageLength, pRcvBuf);
            CAPWAP_GET_1BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusReq.
                              RadioIfMacOperation.u1RadioId, pRcvBuf);
            CAPWAP_GET_1BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusReq.
                              RadioIfMacOperation.u1Reserved, pRcvBuf);
            CAPWAP_GET_2BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusReq.
                              RadioIfMacOperation.u2RTSThreshold, pRcvBuf);
            CAPWAP_GET_1BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusReq.
                              RadioIfMacOperation.u1ShortRetry, pRcvBuf);
            CAPWAP_GET_1BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusReq.
                              RadioIfMacOperation.u1LongRetry, pRcvBuf);
            CAPWAP_GET_2BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusReq.
                              RadioIfMacOperation.u2FragmentThreshold, pRcvBuf);
            CAPWAP_GET_4BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusReq.
                              RadioIfMacOperation.u4TxMsduLifetime, pRcvBuf);
            CAPWAP_GET_4BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusReq.
                              RadioIfMacOperation.u4RxMsduLifetime, pRcvBuf);
            RadioIfMsgStruct.unRadioIfMsg.
                RadioIfConfigStatusReq.RadioIfMacOperation.isPresent =
                OSIX_TRUE;
            RadioIfMsgStruct.unRadioIfMsg.
                RadioIfConfigStatusReq.u2WtpInternalId = pCwMsg->u2IntProfileId;
            if (WssIfProcessRadioIfMsg
                (WSS_RADIOIF_CONFIG_STATUS_REQ,
                 &RadioIfMsgStruct) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Set the IEEE"
                            "Radio MAC in Radio IF \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to Set the IEEE"
                              "Radio MAC in Radio IF \r\n"));
                return OSIX_FAILURE;
            }
            if (RadioIfMsgStruct.unRadioIfMsg.
                RadioIfConfigStatusReq.RadioIfMacOperation.isPresent ==
                OSIX_TRUE)
            {
                pConfStatRsp->RadioIfMacOperation.isPresent = OSIX_TRUE;
                if (CapwapGetIeeeMacOperation
                    (&pConfStatRsp->RadioIfMacOperation,
                     &pConfStatRsp->u2CapwapMsgElemenLen,
                     pCwMsg->u2IntProfileId) == OSIX_FAILURE)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Get"
                                "Mac Operation \r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Failed to Get" "Mac Operation \r\n"));
                    return OSIX_FAILURE;
                }

            }
        }
        else
        {
            CAPWAP_GET_2BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusRsp.
                              RadioIfMacOperation.u2MessageType, pRcvBuf);
            CAPWAP_GET_2BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusRsp.
                              RadioIfMacOperation.u2MessageLength, pRcvBuf);
            CAPWAP_GET_1BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusRsp.
                              RadioIfMacOperation.u1RadioId, pRcvBuf);
            CAPWAP_GET_1BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusRsp.
                              RadioIfMacOperation.u1Reserved, pRcvBuf);
            CAPWAP_GET_2BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusRsp.
                              RadioIfMacOperation.u2RTSThreshold, pRcvBuf);
            CAPWAP_GET_1BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusRsp.
                              RadioIfMacOperation.u1ShortRetry, pRcvBuf);
            CAPWAP_GET_1BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusRsp.
                              RadioIfMacOperation.u1LongRetry, pRcvBuf);
            CAPWAP_GET_2BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusRsp.
                              RadioIfMacOperation.u2FragmentThreshold, pRcvBuf);
            CAPWAP_GET_4BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusRsp.
                              RadioIfMacOperation.u4TxMsduLifetime, pRcvBuf);
            CAPWAP_GET_4BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusRsp.
                              RadioIfMacOperation.u4RxMsduLifetime, pRcvBuf);
            RadioIfMsgStruct.unRadioIfMsg.
                RadioIfConfigStatusRsp.RadioIfMacOperation.isPresent =
                OSIX_TRUE;
            RadioIfMsgStruct.unRadioIfMsg.
                RadioIfConfigStatusRsp.u2WtpInternalId = pCwMsg->u2IntProfileId;
            if (WssIfProcessRadioIfMsg
                (WSS_RADIOIF_CONFIG_STATUS_RSP,
                 &RadioIfMsgStruct) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Set the"
                            "IEEE Radio MAC in Radio IF \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to Set the"
                              "IEEE Radio MAC in Radio IF \r\n"));
                return OSIX_FAILURE;
            }

        }
    }
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

INT4
CapwapSetIeeeMultiDomainCapability (UINT1 *pRcvBuf,
                                    tCapwapControlPacket * pCwMsg,
                                    tConfigStatusRsp * pConfStatRsp,
                                    UINT2 u2MsgIndex)
{
    tRadioIfMsgStruct   RadioIfMsgStruct;
    UINT1              *pu1Buf = NULL;
    UINT2               u2NumOfRadios = 0;
    UINT1               u1Index = 0;
    UINT4               u4Offset;

    CAPWAP_FN_ENTRY ();
    MEMSET (&RadioIfMsgStruct, 0, sizeof (tRadioIfMsgStruct));
    pu1Buf = pRcvBuf;
    u2NumOfRadios = pCwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;

    for (u1Index = 1; u1Index <= u2NumOfRadios; u1Index++)
    {
        u4Offset = pCwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[u1Index - 1];
        pRcvBuf = pu1Buf;
        pRcvBuf += u4Offset;

        if (pCwMsg->capwapCtrlHdr.u4MsgType == WSS_RADIOIF_CONFIG_STATUS_REQ)
        {
            CAPWAP_GET_2BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusReq.
                              RadioIfMultiDomainCap.u2MessageType, pRcvBuf);
            CAPWAP_GET_2BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusReq.
                              RadioIfMultiDomainCap.u2MessageLength, pRcvBuf);
            CAPWAP_GET_1BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusReq.
                              RadioIfMultiDomainCap.u1RadioId, pRcvBuf);
            CAPWAP_GET_1BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusReq.
                              RadioIfMultiDomainCap.u1Reserved, pRcvBuf);
            CAPWAP_GET_2BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusReq.
                              RadioIfMultiDomainCap.u2FirstChannel, pRcvBuf);
            CAPWAP_GET_2BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusReq.
                              RadioIfMultiDomainCap.u2NumOfChannels, pRcvBuf);
            CAPWAP_GET_2BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusReq.
                              RadioIfMultiDomainCap.u2MaxTxPowerLevel, pRcvBuf);
            RadioIfMsgStruct.unRadioIfMsg.
                RadioIfConfigStatusReq.u2WtpInternalId = pCwMsg->u2IntProfileId;
            RadioIfMsgStruct.unRadioIfMsg.
                RadioIfConfigStatusReq.RadioIfMultiDomainCap.isPresent =
                OSIX_TRUE;
            if (WssIfProcessRadioIfMsg
                (WSS_RADIOIF_CONFIG_STATUS_REQ,
                 &RadioIfMsgStruct) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Set Multi"
                            "Domain Capability in Radio IF \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to Set Multi"
                              "Domain Capability in Radio IF \r\n"));
                return OSIX_FAILURE;
            }
            if (RadioIfMsgStruct.unRadioIfMsg.
                RadioIfConfigStatusReq.RadioIfMultiDomainCap.isPresent ==
                OSIX_TRUE)
            {
                pConfStatRsp->RadioIfMultiDomainCap.isPresent = OSIX_TRUE;
                if (CapwapGetIeeeMultiDomainCapability
                    (&pConfStatRsp->RadioIfMultiDomainCap,
                     &pConfStatRsp->u2CapwapMsgElemenLen,
                     pCwMsg->u2IntProfileId) == OSIX_FAILURE)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Get"
                                "Multi Domain Capability \r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Failed to Get"
                                  "Multi Domain Capability \r\n"));
                    return OSIX_FAILURE;
                }
            }
        }
        else
        {
            CAPWAP_GET_2BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusRsp.
                              RadioIfMultiDomainCap.u2MessageType, pRcvBuf);
            CAPWAP_GET_2BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusRsp.
                              RadioIfMultiDomainCap.u2MessageLength, pRcvBuf);
            CAPWAP_GET_1BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusRsp.
                              RadioIfMultiDomainCap.u1RadioId, pRcvBuf);
            CAPWAP_GET_1BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusRsp.
                              RadioIfMultiDomainCap.u1Reserved, pRcvBuf);
            CAPWAP_GET_2BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusRsp.
                              RadioIfMultiDomainCap.u2FirstChannel, pRcvBuf);
            CAPWAP_GET_2BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusRsp.
                              RadioIfMultiDomainCap.u2NumOfChannels, pRcvBuf);
            CAPWAP_GET_2BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusRsp.
                              RadioIfMultiDomainCap.u2MaxTxPowerLevel, pRcvBuf);
            RadioIfMsgStruct.unRadioIfMsg.
                RadioIfConfigStatusRsp.u2WtpInternalId = pCwMsg->u2IntProfileId;
            RadioIfMsgStruct.unRadioIfMsg.
                RadioIfConfigStatusRsp.RadioIfMultiDomainCap.isPresent =
                OSIX_TRUE;
            if (WssIfProcessRadioIfMsg
                (WSS_RADIOIF_CONFIG_STATUS_RSP,
                 &RadioIfMsgStruct) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Set Multi"
                            "Domain Capability in Radio IF \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to Set Multi"
                              "Domain Capability in Radio IF \r\n"));
                return OSIX_FAILURE;
            }

        }
    }
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

INT4
CapwapSetIeeeOFDMControl (UINT1 *pRcvBuf,
                          tCapwapControlPacket * pCwMsg,
                          tConfigStatusRsp * pConfStatRsp, UINT2 u2MsgIndex)
{
    tRadioIfMsgStruct   RadioIfMsgStruct;
    UINT1              *pu1Buf = NULL;
    UINT2               u2NumOfRadios = 0;
    UINT1               u1Index = 0;
    UINT4               u4Offset;

    CAPWAP_FN_ENTRY ();
    MEMSET (&RadioIfMsgStruct, 0, sizeof (tRadioIfMsgStruct));
    pu1Buf = pRcvBuf;
    u2NumOfRadios = pCwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;

    for (u1Index = 1; u1Index <= u2NumOfRadios; u1Index++)
    {
        u4Offset = pCwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[u1Index - 1];
        pRcvBuf = pu1Buf;
        pRcvBuf += u4Offset;
        if (pCwMsg->capwapCtrlHdr.u4MsgType == WSS_RADIOIF_CONFIG_STATUS_REQ)
        {
            CAPWAP_GET_2BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusReq.
                              RadioIfOFDMPhy.u2MessageType, pRcvBuf);
            CAPWAP_GET_2BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusReq.
                              RadioIfOFDMPhy.u2MessageLength, pRcvBuf);
            CAPWAP_GET_1BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusReq.
                              RadioIfOFDMPhy.u1RadioId, pRcvBuf);
            CAPWAP_GET_1BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusReq.
                              RadioIfOFDMPhy.u1Reserved, pRcvBuf);
            CAPWAP_GET_1BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusReq.
                              RadioIfOFDMPhy.u1CurrentChannel, pRcvBuf);
            CAPWAP_GET_1BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusReq.
                              RadioIfOFDMPhy.u1SupportedBand, pRcvBuf);
            CAPWAP_GET_4BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusReq.
                              RadioIfOFDMPhy.u4T1Threshold, pRcvBuf);
            RadioIfMsgStruct.unRadioIfMsg.
                RadioIfConfigStatusReq.u2WtpInternalId = pCwMsg->u2IntProfileId;
            RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusReq.
                RadioIfOFDMPhy.isPresent = OSIX_TRUE;
            if (WssIfProcessRadioIfMsg
                (WSS_RADIOIF_CONFIG_STATUS_REQ,
                 &RadioIfMsgStruct) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Set the"
                            "IEEE OFDM Control in Radio IF \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to Set the"
                              "IEEE OFDM Control in Radio IF \r\n"));
                return OSIX_FAILURE;
            }
            if (RadioIfMsgStruct.unRadioIfMsg.
                RadioIfConfigStatusReq.RadioIfOFDMPhy.isPresent == OSIX_TRUE)
            {
                pConfStatRsp->RadioIfOFDMPhy.isPresent = OSIX_TRUE;
                if (CapwapGetIeeeOFDMControl (&pConfStatRsp->RadioIfOFDMPhy,
                                              &pConfStatRsp->
                                              u2CapwapMsgElemenLen,
                                              pCwMsg->u2IntProfileId) ==
                    OSIX_FAILURE)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Get"
                                "Radio OFDM \r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Failed to Get" "Radio OFDM \r\n"));
                    return OSIX_FAILURE;
                }
            }
        }
        else
        {
            CAPWAP_GET_2BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusRsp.
                              RadioIfOFDMPhy.u2MessageType, pRcvBuf);
            CAPWAP_GET_2BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusRsp.
                              RadioIfOFDMPhy.u2MessageLength, pRcvBuf);
            CAPWAP_GET_1BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusRsp.
                              RadioIfOFDMPhy.u1RadioId, pRcvBuf);
            CAPWAP_GET_1BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusRsp.
                              RadioIfOFDMPhy.u1Reserved, pRcvBuf);
            CAPWAP_GET_1BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusRsp.
                              RadioIfOFDMPhy.u1CurrentChannel, pRcvBuf);
            CAPWAP_GET_1BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusRsp.
                              RadioIfOFDMPhy.u1SupportedBand, pRcvBuf);
            CAPWAP_GET_4BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusRsp.
                              RadioIfOFDMPhy.u4T1Threshold, pRcvBuf);
            RadioIfMsgStruct.unRadioIfMsg.
                RadioIfConfigStatusRsp.u2WtpInternalId = pCwMsg->u2IntProfileId;
            RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusRsp.
                RadioIfOFDMPhy.isPresent = OSIX_TRUE;
            if (WssIfProcessRadioIfMsg
                (WSS_RADIOIF_CONFIG_STATUS_RSP,
                 &RadioIfMsgStruct) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Set the"
                            "IEEE OFDM Control in Radio IF \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to Set the"
                              "IEEE OFDM Control in Radio IF \r\n"));
                return OSIX_FAILURE;
            }
        }

    }
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

INT4
CapwapSetIeeeRateSet (UINT1 *pRcvBuf,
                      tCapwapControlPacket * pCwMsg,
                      tConfigStatusRsp * pConfStatRsp, UINT2 u2MsgIndex)
{
    tRadioIfMsgStruct   RadioIfMsgStruct;
    UINT1              *pu1Buf = NULL;
    UINT2               u2NumOfRadios = 0;
    UINT1               u1Index = 0;
    UINT4               u4Offset;

    UNUSED_PARAM (pConfStatRsp);
    CAPWAP_FN_ENTRY ();
    MEMSET (&RadioIfMsgStruct, 0, sizeof (tRadioIfMsgStruct));
    pu1Buf = pRcvBuf;
    u2NumOfRadios = pCwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;

    for (u1Index = 1; u1Index <= u2NumOfRadios; u1Index++)
    {
        u4Offset = pCwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[u1Index - 1];
        pRcvBuf = pu1Buf;
        pRcvBuf += u4Offset;

        CAPWAP_GET_2BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusRsp.
                          RadioIfRateSet.u2MessageType, pRcvBuf);
        CAPWAP_GET_2BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusRsp.
                          RadioIfRateSet.u2MessageLength, pRcvBuf);
        CAPWAP_GET_1BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusRsp.
                          RadioIfRateSet.u1RadioId, pRcvBuf);
        CAPWAP_GET_NBYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusRsp.
                          RadioIfRateSet.au1OperationalRate, pRcvBuf,
                          RADIOIF_OPER_RATE);
        RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusRsp.
            RadioIfRateSet.isPresent = OSIX_TRUE;
        if (WssIfProcessRadioIfMsg
            (WSS_RADIOIF_CONFIG_STATUS_RSP, &RadioIfMsgStruct) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Set"
                        "the IEEE Supported Rates in Radio IF \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId, "Failed to Set"
                          "the IEEE Supported Rates in Radio IF \r\n"));
            return OSIX_FAILURE;
        }

    }
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

INT4
CapwapSetIeeeSupportedRates (UINT1 *pRcvBuf,
                             tCapwapControlPacket * pCwMsg,
                             tConfigStatusRsp * pConfStatRsp, UINT2 u2MsgIndex)
{
    tRadioIfMsgStruct   RadioIfMsgStruct;
    UINT1              *pu1Buf = NULL;
    UINT2               u2NumOfRadios = 0;
    UINT1               u1Index = 0;
    UINT4               u4Offset;

    CAPWAP_FN_ENTRY ();
    MEMSET (&RadioIfMsgStruct, 0, sizeof (tRadioIfMsgStruct));
    pu1Buf = pRcvBuf;
    u2NumOfRadios = pCwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;

    for (u1Index = 1; u1Index <= u2NumOfRadios; u1Index++)
    {
        u4Offset = pCwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[u1Index - 1];
        pRcvBuf = pu1Buf;
        pRcvBuf += u4Offset;

        if (pCwMsg->capwapCtrlHdr.u4MsgType == WSS_RADIOIF_CONFIG_STATUS_REQ)
        {
            CAPWAP_GET_2BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusReq.
                              RadioIfSupportedRate.u2MessageType, pRcvBuf);
            CAPWAP_GET_2BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusReq.
                              RadioIfSupportedRate.u2MessageLength, pRcvBuf);
            CAPWAP_GET_1BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusReq.
                              RadioIfSupportedRate.u1RadioId, pRcvBuf);
            CAPWAP_GET_NBYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusReq.
                              RadioIfSupportedRate.au1SupportedRate, pRcvBuf,
                              RADIOIF_OPER_RATE);
            RadioIfMsgStruct.unRadioIfMsg.
                RadioIfConfigStatusReq.RadioIfSupportedRate.isPresent =
                OSIX_TRUE;
            RadioIfMsgStruct.unRadioIfMsg.
                RadioIfConfigStatusReq.u2WtpInternalId = pCwMsg->u2IntProfileId;
            if (WssIfProcessRadioIfMsg
                (WSS_RADIOIF_CONFIG_STATUS_REQ,
                 &RadioIfMsgStruct) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Set"
                            "the IEEE Supported Rates in Radio IF \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to Set"
                              "the IEEE Supported Rates in Radio IF \r\n"));
                return OSIX_FAILURE;
            }
            if (RadioIfMsgStruct.unRadioIfMsg.
                RadioIfConfigStatusReq.RadioIfSupportedRate.isPresent ==
                OSIX_TRUE)
            {
        /** addedd **/
                pConfStatRsp->RadioIfRateSet.isPresent = OSIX_TRUE;
                if (CapwapGetIeeeRateSet (&pConfStatRsp->RadioIfRateSet,
                                          &pConfStatRsp->u2CapwapMsgElemenLen,
                                          pCwMsg->u2IntProfileId) ==
                    OSIX_FAILURE)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Get"
                                "Radio Rate set in DB \r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Failed to Get" "Radio Rate set in DB \r\n"));
                    return OSIX_FAILURE;

                }
        /** addedd **/
                pConfStatRsp->RadioIfSupportedRate.isPresent = OSIX_TRUE;
                if (CapwapGetIeeeSupportedRates
                    (&pConfStatRsp->RadioIfSupportedRate,
                     &pConfStatRsp->u2CapwapMsgElemenLen,
                     pCwMsg->u2IntProfileId) == OSIX_FAILURE)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Get"
                                "Radio Supported Rates \r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Failed to Get"
                                  "Radio Supported Rates \r\n"));
                    return OSIX_FAILURE;
                }
            }
        }
        else
        {
            CAPWAP_GET_2BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusRsp.
                              RadioIfSupportedRate.u2MessageType, pRcvBuf);
            CAPWAP_GET_2BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusRsp.
                              RadioIfSupportedRate.u2MessageLength, pRcvBuf);
            CAPWAP_GET_1BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusRsp.
                              RadioIfSupportedRate.u1RadioId, pRcvBuf);
            CAPWAP_GET_NBYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusRsp.
                              RadioIfSupportedRate.au1SupportedRate, pRcvBuf,
                              RADIOIF_OPER_RATE);
            RadioIfMsgStruct.unRadioIfMsg.
                RadioIfConfigStatusRsp.RadioIfSupportedRate.isPresent =
                OSIX_TRUE;
            RadioIfMsgStruct.unRadioIfMsg.
                RadioIfConfigStatusRsp.u2WtpInternalId = pCwMsg->u2IntProfileId;
            if (WssIfProcessRadioIfMsg
                (WSS_RADIOIF_CONFIG_STATUS_RSP,
                 &RadioIfMsgStruct) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Set"
                            "the IEEE Supported Rates in Radio IF \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to Set"
                              "the IEEE Supported Rates in Radio IF \r\n"));
                return OSIX_FAILURE;
            }

        }

    }
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

INT4
CapwapSetIeeeTxPower (UINT1 *pRcvBuf,
                      tCapwapControlPacket * pCwMsg,
                      tConfigStatusRsp * pConfStatRsp, UINT2 u2MsgIndex)
{
    tRadioIfMsgStruct   RadioIfMsgStruct;
    UINT1              *pu1Buf = NULL;
    UINT2               u2NumOfRadios = 0;
    UINT1               u1Index = 0;
    UINT4               u4Offset;

    CAPWAP_FN_ENTRY ();
    MEMSET (&RadioIfMsgStruct, 0, sizeof (tRadioIfMsgStruct));
    pu1Buf = pRcvBuf;
    u2NumOfRadios = pCwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;

    for (u1Index = 1; u1Index <= u2NumOfRadios; u1Index++)
    {
        u4Offset = pCwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[u1Index - 1];
        pRcvBuf = pu1Buf;
        pRcvBuf += u4Offset;
        if (pCwMsg->capwapCtrlHdr.u4MsgType == WSS_RADIOIF_CONFIG_STATUS_REQ)
        {

            CAPWAP_GET_2BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusReq.
                              RadioIfTxPower.u2MessageType, pRcvBuf);
            CAPWAP_GET_2BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusReq.
                              RadioIfTxPower.u2MessageLength, pRcvBuf);
            CAPWAP_GET_1BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusReq.
                              RadioIfTxPower.u1RadioId, pRcvBuf);
            CAPWAP_GET_1BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusReq.
                              RadioIfTxPower.u1Reserved, pRcvBuf);
            CAPWAP_GET_2BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusReq.
                              RadioIfTxPower.u2TxPowerLevel, pRcvBuf);
            RadioIfMsgStruct.unRadioIfMsg.
                RadioIfConfigStatusReq.u2WtpInternalId = pCwMsg->u2IntProfileId;
            RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusReq.
                RadioIfTxPower.isPresent = OSIX_TRUE;
            if (WssIfProcessRadioIfMsg
                (WSS_RADIOIF_CONFIG_STATUS_REQ,
                 &RadioIfMsgStruct) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Set the"
                            "IEEE Tx Power in Radio IF \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to Set the"
                              "IEEE Tx Power in Radio IF \r\n"));
                return OSIX_FAILURE;
            }
            if (RadioIfMsgStruct.unRadioIfMsg.
                RadioIfConfigStatusReq.RadioIfTxPower.isPresent == OSIX_TRUE)
            {
                pConfStatRsp->RadioIfTxPower.isPresent = OSIX_TRUE;
                if (CapwapGetIeeeTxPower (&pConfStatRsp->RadioIfTxPower,
                                          &pConfStatRsp->u2CapwapMsgElemenLen,
                                          pCwMsg->u2IntProfileId) ==
                    OSIX_FAILURE)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Get"
                                "Radio Tx Power  \r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Failed to Get" "Radio Tx Power  \r\n"));
                    return OSIX_FAILURE;
                }
            }
        }
        else
        {
            CAPWAP_GET_2BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusRsp.
                              RadioIfTxPower.u2MessageType, pRcvBuf);
            CAPWAP_GET_2BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusRsp.
                              RadioIfTxPower.u2MessageLength, pRcvBuf);
            CAPWAP_GET_1BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusRsp.
                              RadioIfTxPower.u1RadioId, pRcvBuf);
            CAPWAP_GET_1BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusRsp.
                              RadioIfTxPower.u1Reserved, pRcvBuf);
            CAPWAP_GET_2BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusRsp.
                              RadioIfTxPower.u2TxPowerLevel, pRcvBuf);
            RadioIfMsgStruct.unRadioIfMsg.
                RadioIfConfigStatusRsp.u2WtpInternalId = pCwMsg->u2IntProfileId;
            RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusRsp.
                RadioIfTxPower.isPresent = OSIX_TRUE;
            if (WssIfProcessRadioIfMsg
                (WSS_RADIOIF_CONFIG_STATUS_RSP,
                 &RadioIfMsgStruct) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Set"
                            "the IEEE Tx Power in Radio IF \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to Set"
                              "the IEEE Tx Power in Radio IF \r\n"));
                return OSIX_FAILURE;
            }
        }
    }
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

INT4
CapwapSetIeeeTxPowerLevel (UINT1 *pRcvBuf,
                           tCapwapControlPacket * pCwMsg,
                           tConfigStatusRsp * pConfStatRsp, UINT2 u2MsgIndex)
{
    tRadioIfMsgStruct   RadioIfMsgStruct;
    UINT1              *pu1Buf = NULL;
    UINT2               u2NumOfRadios = 0;
    UINT1               u1Index = 0;
    UINT4               u4Offset;
    UNUSED_PARAM (pConfStatRsp);

    CAPWAP_FN_ENTRY ();
    MEMSET (&RadioIfMsgStruct, 0, sizeof (tRadioIfMsgStruct));
    pu1Buf = pRcvBuf;
    u2NumOfRadios = pCwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;
    for (u1Index = 1; u1Index <= u2NumOfRadios; u1Index++)
    {
        u4Offset = pCwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[u1Index - 1];
        pRcvBuf = pu1Buf;
        pRcvBuf += u4Offset;

        CAPWAP_GET_2BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusReq.
                          RadioIfTxPowerLevel.u2MessageType, pRcvBuf);
        CAPWAP_GET_2BYTE (RadioIfMsgStruct.unRadioIfMsg.
                          RadioIfConfigStatusReq.RadioIfTxPowerLevel.
                          u2MessageLength, pRcvBuf);
        CAPWAP_GET_1BYTE (RadioIfMsgStruct.unRadioIfMsg.
                          RadioIfConfigStatusReq.RadioIfTxPowerLevel.u1RadioId,
                          pRcvBuf);
        CAPWAP_GET_1BYTE (RadioIfMsgStruct.unRadioIfMsg.
                          RadioIfConfigStatusReq.RadioIfTxPowerLevel.
                          u1NumOfLevels, pRcvBuf);
        CAPWAP_GET_NBYTE (RadioIfMsgStruct.unRadioIfMsg.
                          RadioIfConfigStatusReq.RadioIfTxPowerLevel.
                          au2PowerLevels, pRcvBuf, RADIOIF_MAX_POWER_LEVEL);
        RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusReq.u2WtpInternalId =
            pCwMsg->u2IntProfileId;
        RadioIfMsgStruct.unRadioIfMsg.
            RadioIfConfigStatusReq.RadioIfTxPowerLevel.isPresent = OSIX_TRUE;
        /*if (WssIfProcessRadioIfMsg( WSS_RADIOIF_CONFIG_STATUS_REQ,
           &RadioIfMsgStruct) != OSIX_SUCCESS)
           {
           CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Set the IEEE"
           "Tx Power Leverl in Radio IF \r\n");
           return OSIX_FAILURE;
           } */
    }
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

INT4
CapwapSetIeeeWtpRadioConfig (UINT1 *pRcvBuf,
                             tCapwapControlPacket * pCwMsg,
                             tConfigStatusRsp * pConfStatRsp, UINT2 u2MsgIndex)
{
    tRadioIfMsgStruct   RadioIfMsgStruct;
    UINT1              *pu1Buf = NULL;
    UINT2               u2NumOfRadios = 0;
    UINT1               u1Index = 0;
    UINT4               u4Offset;

    CAPWAP_FN_ENTRY ();
    MEMSET (&RadioIfMsgStruct, 0, sizeof (tRadioIfMsgStruct));
    pu1Buf = pRcvBuf;
    u2NumOfRadios = pCwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;

    for (u1Index = 1; u1Index <= u2NumOfRadios; u1Index++)
    {
        u4Offset = pCwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[u1Index - 1];
        pRcvBuf = pu1Buf;
        pRcvBuf += u4Offset;

        if (pCwMsg->capwapCtrlHdr.u4MsgType == WSS_RADIOIF_CONFIG_STATUS_REQ)
        {

            CAPWAP_GET_2BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusReq.
                              RadioIfConfig.u2MessageType, pRcvBuf);
            CAPWAP_GET_2BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusReq.
                              RadioIfConfig.u2MessageLength, pRcvBuf);
            CAPWAP_GET_1BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusReq.RadioIfConfig.
                              u1RadioId, pRcvBuf);
            CAPWAP_GET_1BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusReq.
                              RadioIfConfig.u1ShortPreamble, pRcvBuf);
            CAPWAP_GET_1BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusReq.RadioIfConfig.
                              u1NoOfBssId, pRcvBuf);
            CAPWAP_GET_1BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusReq.RadioIfConfig.
                              u1DtimPeriod, pRcvBuf);
            CAPWAP_GET_NBYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusReq.RadioIfConfig.
                              MacAddr, pRcvBuf, RADIO_MAC_LEN);
            CAPWAP_GET_2BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusReq.
                              RadioIfConfig.u2BeaconPeriod, pRcvBuf);
            CAPWAP_GET_NBYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusReq.
                              RadioIfConfig.au1CountryString, pRcvBuf,
                              RADIOIF_COUNTRY_STR_LEN);
            RadioIfMsgStruct.unRadioIfMsg.
                RadioIfConfigStatusReq.u2WtpInternalId = pCwMsg->u2IntProfileId;
            RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusReq.
                RadioIfConfig.isPresent = OSIX_TRUE;
            if (WssIfProcessRadioIfMsg
                (WSS_RADIOIF_CONFIG_STATUS_REQ,
                 &RadioIfMsgStruct) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Set"
                            "the IEEE Radio Configuration in Radio IF \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to Set"
                              "the IEEE Radio Configuration in Radio IF \r\n"));
                return OSIX_FAILURE;
            }
            if (RadioIfMsgStruct.unRadioIfMsg.
                RadioIfConfigStatusReq.RadioIfConfig.isPresent == OSIX_TRUE)
            {
                pConfStatRsp->RadioIfConfig.isPresent = OSIX_TRUE;
                if (CapwapGetIeeeRadioConfiguration
                    (&pConfStatRsp->RadioIfConfig,
                     &pConfStatRsp->u2CapwapMsgElemenLen,
                     pCwMsg->u2IntProfileId) == OSIX_FAILURE)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Get"
                                "Radio Configuration  \r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Failed to Get" "Radio Configuration  \r\n"));
                    return OSIX_FAILURE;
                }
            }
        }
        else
        {
            CAPWAP_GET_2BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusRsp.
                              RadioIfConfig.u2MessageType, pRcvBuf);
            CAPWAP_GET_2BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusRsp.
                              RadioIfConfig.u2MessageLength, pRcvBuf);
            CAPWAP_GET_1BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusRsp.RadioIfConfig.
                              u1RadioId, pRcvBuf);
            CAPWAP_GET_1BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusRsp.
                              RadioIfConfig.u1ShortPreamble, pRcvBuf);
            CAPWAP_GET_1BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusRsp.RadioIfConfig.
                              u1NoOfBssId, pRcvBuf);
            CAPWAP_GET_1BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusRsp.RadioIfConfig.
                              u1DtimPeriod, pRcvBuf);
            CAPWAP_GET_NBYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusRsp.RadioIfConfig.
                              MacAddr, pRcvBuf, RADIO_MAC_LEN);
            CAPWAP_GET_2BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusRsp.
                              RadioIfConfig.u2BeaconPeriod, pRcvBuf);
            CAPWAP_GET_NBYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusRsp.
                              RadioIfConfig.au1CountryString, pRcvBuf,
                              RADIOIF_COUNTRY_STR_LEN);
            RadioIfMsgStruct.unRadioIfMsg.
                RadioIfConfigStatusRsp.u2WtpInternalId = pCwMsg->u2IntProfileId;
            RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusRsp.
                RadioIfConfig.isPresent = OSIX_TRUE;
            if (WssIfProcessRadioIfMsg
                (WSS_RADIOIF_CONFIG_STATUS_RSP,
                 &RadioIfMsgStruct) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Set the"
                            "IEEE Radio Configuration in Radio IF \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to Set the"
                              "IEEE Radio Configuration in Radio IF \r\n"));

                return OSIX_FAILURE;
            }

        }

    }
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

INT4
CapwapSetIeeeRadioInfo (UINT1 *pRcvBuf,
                        tCapwapControlPacket * pCwMsg,
                        tConfigStatusRsp * pConfStatRsp, UINT2 u2MsgIndex)
{
    tRadioIfMsgStruct   RadioIfMsgStruct;
    UINT1              *pu1Buf = NULL;
    UINT2               u2NumOfRadios = 0;
    UINT1               u1Index = 0;
    UINT4               u4Offset;
    UNUSED_PARAM (pConfStatRsp);

    CAPWAP_FN_ENTRY ();
    MEMSET (&RadioIfMsgStruct, 0, sizeof (tRadioIfMsgStruct));
    pu1Buf = pRcvBuf;

    u2NumOfRadios = pCwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;

    for (u1Index = 1; u1Index <= u2NumOfRadios; u1Index++)
    {
        u4Offset = pCwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[u1Index - 1];
        pRcvBuf = pu1Buf;
        pRcvBuf += u4Offset;

        CAPWAP_GET_2BYTE (RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusReq.
                          RadioIfInfo.u2MessageType, pRcvBuf);
        CAPWAP_GET_2BYTE (RadioIfMsgStruct.unRadioIfMsg.
                          RadioIfConfigStatusReq.RadioIfInfo.u2MessageLength,
                          pRcvBuf);
        CAPWAP_GET_1BYTE (RadioIfMsgStruct.unRadioIfMsg.
                          RadioIfConfigStatusReq.RadioIfInfo.u1RadioId,
                          pRcvBuf);
        CAPWAP_GET_4BYTE (RadioIfMsgStruct.unRadioIfMsg.
                          RadioIfConfigStatusReq.RadioIfInfo.u4RadioType,
                          pRcvBuf);
        RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusReq.u2WtpInternalId =
            pCwMsg->u2IntProfileId;
        RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusReq.
            RadioIfInfo.isPresent = OSIX_TRUE;
        if (WssIfProcessRadioIfMsg
            (WSS_RADIOIF_CONFIG_STATUS_REQ, &RadioIfMsgStruct) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Set"
                        "the IEEE Radio Information in Radio IF \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId, "Failed to Set"
                          "the IEEE Radio Information in Radio IF \r\n"));
            return OSIX_FAILURE;
        }
    }
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapSetIeeeWtpQos                               */
/*  Description     : The function sets the Ieee Quality Update         */
/*  Input(s)        : pRcvBuf - Received Buf                            */
/*                    pCwMsg - Pointer to Capwap Ctrl Msg               */
/*                    u2MsgIndex - Message Index                        */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS | OSIX_FAILURE                       */
/************************************************************************/
INT4
CapwapSetIeeeWtpQos (UINT1 *pRcvBuf,
                     tCapwapControlPacket * pCwMsg,
                     tConfigStatusRsp * pConfStatRsp, UINT2 u2MsgIndex)
{
    tRadioIfMsgStruct   RadioIfMsgStruct;
    UINT1              *pu1Buf = NULL;
    UINT2               u2NumRadioQosProfile = 0;
    UINT1               u1Index = 0;
    UINT1               u1QosIndex = 0;
    UINT4               u4Offset = 0;
    UNUSED_PARAM (pConfStatRsp);

    CAPWAP_FN_ENTRY ();
    MEMSET (&RadioIfMsgStruct, 0, sizeof (tRadioIfMsgStruct));

    pu1Buf = pRcvBuf;
    u2NumRadioQosProfile = pCwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;

    for (u1Index = 1; u1Index <= u2NumRadioQosProfile; u1Index++)
    {
        u4Offset = pCwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[u1Index - 1];
        pRcvBuf = pu1Buf;
        pRcvBuf += u4Offset;

        CAPWAP_GET_2BYTE (RadioIfMsgStruct.unRadioIfMsg.
                          RadioIfConfigStatusRsp.RadioIfQos.u2MessageType,
                          pRcvBuf);
        CAPWAP_GET_2BYTE (RadioIfMsgStruct.unRadioIfMsg.
                          RadioIfConfigStatusRsp.RadioIfQos.u2MessageLength,
                          pRcvBuf);
        CAPWAP_GET_1BYTE (RadioIfMsgStruct.unRadioIfMsg.
                          RadioIfConfigStatusRsp.RadioIfQos.u1RadioId, pRcvBuf);
        CAPWAP_GET_1BYTE (RadioIfMsgStruct.unRadioIfMsg.
                          RadioIfConfigStatusRsp.RadioIfQos.u1TaggingPolicy,
                          pRcvBuf);
        for (u1QosIndex = 0; u1QosIndex < RADIOIF_MAX_QOS_PROFILE_IND;
             u1QosIndex++)
        {
            CAPWAP_GET_1BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusRsp.
                              RadioIfQos.u1QueueDepth[u1QosIndex], pRcvBuf);
            CAPWAP_GET_2BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusRsp.
                              RadioIfQos.u2CwMin[u1QosIndex], pRcvBuf);
            CAPWAP_GET_2BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusRsp.
                              RadioIfQos.u2CwMax[u1QosIndex], pRcvBuf);
            CAPWAP_GET_1BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusRsp.
                              RadioIfQos.u1Aifsn[u1QosIndex], pRcvBuf);
            CAPWAP_GET_1BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusRsp.
                              RadioIfQos.u1Prio[u1QosIndex], pRcvBuf);
            CAPWAP_GET_1BYTE (RadioIfMsgStruct.
                              unRadioIfMsg.RadioIfConfigStatusRsp.
                              RadioIfQos.u1Dscp[u1QosIndex], pRcvBuf);
        }
        RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusRsp.u2WtpInternalId
            = pCwMsg->u2IntProfileId;
        RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusRsp.RadioIfQos.
            isPresent = OSIX_TRUE;

        if (WssIfProcessRadioIfMsg (WSS_RADIOIF_CONFIG_STATUS_RSP,
                                    &RadioIfMsgStruct) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to Set the IEEE Wtp Qos in Radio IF \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to Set the IEEE Wtp Qos in Radio IF \r\n"));
            return OSIX_FAILURE;
        }
    }
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapSetVendSpecInfo                             */
/*  Description     : The function processes the received CAPWAP        */
/*                    Vendor info in Config status request              */
/*  Input(s)        : pRcvBuf - Received buffer                         */
/*                    pCwMsg  - capwapcontrolpacket structure           */
/*                    pConfStatRsp - Pointer to the config status resp  */
/*                    u2MsgIndex   - message index                      */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS | OSIX_FAILURE                       */
/************************************************************************/
INT4
CapwapSetVendSpecInfo (UINT1 *pRcvBuf,
                       tCapwapControlPacket * pCwMsg,
                       tConfigStatusRsp * pConfStatRsp, UINT2 u2MsgIndex)
{
    tVendorSpecPayload  VendorSpecPayload;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tRadioIfMsgStruct   RadioIfMsgStruct;
    UINT1              *pu1Buf = NULL;
#ifdef WTP_WANTED
    UINT2               u2NumRadios = 0;
#endif
    UINT4               u4Offset;
    UINT2               elementType;
    tWssWlanMsgStruct  *pWssWlanMsgStruct = NULL;
#ifdef RFMGMT_WANTED
    tRfMgmtMsgStruct    RfMgmtMsgStruct;
#ifdef WLC_WANTED
    tRadioIfGetDB       RadioIfGetDB;
    tRfMgmtDB           RfMgmtThresDB;
    UINT4               u4Idx = 0;
#endif
    tRfMgmtDB           RfMgmtDB;
    UINT1               u1WlanId = 0;
    UINT1               u1RadioId = 0;
#endif

    UNUSED_PARAM (pCwMsg);

    CAPWAP_FN_ENTRY ();

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE) pu1Buf = pRcvBuf;
    pWssWlanMsgStruct =
        (tWssWlanMsgStruct *) (VOID *) UtlShMemAllocWssWlanBuf ();
    if (pWssWlanMsgStruct == NULL)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapSetVendSpecInfo:- "
                    "UtlShMemAllocWssWlanBuf returned failure\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "CapwapSetVendSpecInfo:- "
                      "UtlShMemAllocWssWlanBuf returned failure\n"));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    MEMSET (&VendorSpecPayload, 0, sizeof (tVendorSpecPayload));
    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&RadioIfMsgStruct, 0, sizeof (tRadioIfMsgStruct));
    MEMSET (pWssWlanMsgStruct, 0, sizeof (tWssWlanMsgStruct));
#ifdef RFMGMT_WANTED
    MEMSET (&RfMgmtMsgStruct, 0, sizeof (tRfMgmtMsgStruct));
    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
#endif

#ifdef WTP_WANTED
    u2NumRadios = pCwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;
#endif
    u4Offset = pCwMsg->capwapMsgElm[u2MsgIndex].pu2Offset[0];
    pRcvBuf = pu1Buf;
    pRcvBuf += u4Offset;
    CAPWAP_GET_2BYTE (VendorSpecPayload.u2MsgEleType, pRcvBuf);
    CAPWAP_GET_2BYTE (VendorSpecPayload.u2MsgEleLen, pRcvBuf);
    CAPWAP_GET_2BYTE (elementType, pRcvBuf);
    VendorSpecPayload.elementId = (UINT2) (elementType - VENDOR_SPECIFIC_MSG);

    switch (VendorSpecPayload.elementId)
    {
        case VENDOR_UDP_SERVER_PORT_MSG:
            VendorSpecPayload.unVendorSpec.VendDiscType.u2MsgEleType =
                elementType;
            CAPWAP_GET_2BYTE (VendorSpecPayload.unVendorSpec.
                              VendUdpPort.u2MsgEleLen, pRcvBuf);
            CAPWAP_GET_4BYTE (VendorSpecPayload.unVendorSpec.
                              VendUdpPort.vendorId, pRcvBuf);
            CAPWAP_GET_4BYTE (VendorSpecPayload.unVendorSpec.VendUdpPort.u4Val,
                              pRcvBuf);
            if (WssIfProcessCapwapDBMsg
                (WSS_CAPWAP_GET_CTRLUDP_PORT, pWssIfCapwapDB) == OSIX_SUCCESS)
            {
                if (VendorSpecPayload.unVendorSpec.VendUdpPort.u4Val !=
                    pWssIfCapwapDB->u4CtrludpPort)
                {
                    pConfStatRsp->
                        vendSpec[VENDOR_UDP_SERVER_PORT_MSG].isOptional = TRUE;
                    pConfStatRsp->
                        vendSpec[VENDOR_UDP_SERVER_PORT_MSG].u2MsgEleType =
                        VENDOR_SPECIFIC_PAYLOAD;
                    pConfStatRsp->
                        vendSpec[VENDOR_UDP_SERVER_PORT_MSG].u2MsgEleLen =
                        CAPWAP_MSG_ELE_LEN;
                    pConfStatRsp->
                        vendSpec[VENDOR_UDP_SERVER_PORT_MSG].elementId =
                        VENDOR_UDP_SERVER_PORT_MSG;
                    pConfStatRsp->
                        vendSpec[VENDOR_UDP_SERVER_PORT_MSG].unVendorSpec.
                        VendUdpPort.isOptional = TRUE;
                    pConfStatRsp->
                        vendSpec[VENDOR_UDP_SERVER_PORT_MSG].unVendorSpec.
                        VendUdpPort.u2MsgEleType = UDP_SERVER_PORT_VENDOR_MSG;
                    pConfStatRsp->
                        vendSpec[VENDOR_UDP_SERVER_PORT_MSG].unVendorSpec.
                        VendUdpPort.u2MsgEleLen = 8;
                    pConfStatRsp->
                        vendSpec[VENDOR_UDP_SERVER_PORT_MSG].unVendorSpec.
                        VendUdpPort.vendorId = VENDOR_ID;
                    pConfStatRsp->
                        vendSpec[VENDOR_UDP_SERVER_PORT_MSG].unVendorSpec.
                        VendUdpPort.u4Val = pWssIfCapwapDB->u4CtrludpPort;

                    pConfStatRsp->u2CapwapMsgElemenLen =
                        (UINT2) (pConfStatRsp->u2CapwapMsgElemenLen +
                                 (pConfStatRsp->
                                  vendSpec[VENDOR_UDP_SERVER_PORT_MSG].
                                  u2MsgEleLen + 4));
                }
            }
            break;
        case VENDOR_DISC_TYPE:
            VendorSpecPayload.unVendorSpec.VendDiscType.u2MsgEleType =
                elementType;
            CAPWAP_GET_2BYTE (VendorSpecPayload.unVendorSpec.
                              VendDiscType.u2MsgEleLen, pRcvBuf);
            CAPWAP_GET_4BYTE (VendorSpecPayload.unVendorSpec.
                              VendDiscType.vendorId, pRcvBuf);
            CAPWAP_GET_1BYTE (VendorSpecPayload.unVendorSpec.VendDiscType.u1Val,
                              pRcvBuf);

            pWssIfCapwapDB->CapwapIsGetAllDB.bWtpDiscType = OSIX_TRUE;
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB) ==
                OSIX_SUCCESS)
            {
                if (VendorSpecPayload.unVendorSpec.VendDiscType.u1Val !=
                    pWssIfCapwapDB->CapwapGetDB.u1WtpDiscType)
                {
                    pConfStatRsp->vendSpec[VENDOR_DISC_TYPE].isOptional = TRUE;
                    pConfStatRsp->vendSpec[VENDOR_DISC_TYPE].u2MsgEleType =
                        VENDOR_SPECIFIC_PAYLOAD;
                    pConfStatRsp->vendSpec[VENDOR_DISC_TYPE].u2MsgEleLen =
                        CAPWAP_MSG_ELE_LEN;
                    pConfStatRsp->vendSpec[VENDOR_DISC_TYPE].elementId =
                        VENDOR_DISC_TYPE;
                    pConfStatRsp->vendSpec[VENDOR_DISC_TYPE].
                        unVendorSpec.VendDiscType.isOptional = TRUE;
                    pConfStatRsp->vendSpec[VENDOR_DISC_TYPE].
                        unVendorSpec.VendDiscType.u2MsgEleType = VEND_DISC_TYPE;
                    pConfStatRsp->vendSpec[VENDOR_DISC_TYPE].
                        unVendorSpec.VendDiscType.u2MsgEleLen = VEND_DISC_LEN;
                    pConfStatRsp->vendSpec[VENDOR_DISC_TYPE].
                        unVendorSpec.VendDiscType.vendorId = VEND_DISC_ID;
                    pConfStatRsp->vendSpec[VENDOR_DISC_TYPE].
                        unVendorSpec.VendDiscType.u1Val =
                        pWssIfCapwapDB->CapwapGetDB.u1WtpDiscType;
                    pConfStatRsp->u2CapwapMsgElemenLen =
                        (UINT2) (pConfStatRsp->u2CapwapMsgElemenLen +
                                 (pConfStatRsp->vendSpec[VENDOR_DISC_TYPE].
                                  u2MsgEleLen + 4));
                }
            }
            break;
        case VENDOR_DOT11N_TYPE:
            break;
        case VENDOR_DOMAIN_NAME_TYPE:
            VendorSpecPayload.unVendorSpec.VendDns.u2MsgEleType = elementType;
            CAPWAP_GET_2BYTE (VendorSpecPayload.unVendorSpec.
                              VendDns.u2MsgEleLen, pRcvBuf);
            CAPWAP_GET_4BYTE (VendorSpecPayload.unVendorSpec.VendDns.vendorId,
                              pRcvBuf);
            CAPWAP_GET_NBYTE (VendorSpecPayload.unVendorSpec.
                              VendDns.au1DomainName, pRcvBuf, VEND_DOMAIN_LEN);
            pWssIfCapwapDB->CapwapIsGetAllDB.bCapwapDnsServerIp = TRUE;
            pWssIfCapwapDB->CapwapIsGetAllDB.bCapwapDnsDomainName = TRUE;

            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB) ==
                OSIX_SUCCESS)
            {
                if (MEMCMP
                    (VendorSpecPayload.unVendorSpec.VendDns.au1DomainName,
                     pWssIfCapwapDB->DnsProfileEntry.au1FsCapwapDnsDomainName,
                     VEND_DOMAIN_LEN) != OSIX_SUCCESS)
                {
                    pConfStatRsp->vendSpec[VENDOR_DOMAIN_NAME_TYPE].isOptional =
                        TRUE;
                    pConfStatRsp->
                        vendSpec[VENDOR_DOMAIN_NAME_TYPE].u2MsgEleType =
                        VENDOR_SPECIFIC_PAYLOAD;
                    pConfStatRsp->
                        vendSpec[VENDOR_DOMAIN_NAME_TYPE].u2MsgEleLen =
                        VEND_DOMAIN_NAME_LEN;
                    pConfStatRsp->
                        vendSpec[VENDOR_DOMAIN_NAME_TYPE].unVendorSpec.VendDns.
                        isOptional = TRUE;
                    pConfStatRsp->
                        vendSpec[VENDOR_DOMAIN_NAME_TYPE].unVendorSpec.VendDns.
                        u2MsgEleType = DOMAIN_NAME_VENDOR_MSG;
                    pConfStatRsp->
                        vendSpec[VENDOR_DOMAIN_NAME_TYPE].unVendorSpec.VendDns.
                        u2MsgEleLen = VEND_DNS_LEN;
                    pConfStatRsp->
                        vendSpec[VENDOR_DOMAIN_NAME_TYPE].unVendorSpec.VendDns.
                        vendorId = VENDOR_ID;
                    MEMCPY (pConfStatRsp->
                            vendSpec[VENDOR_DOMAIN_NAME_TYPE].unVendorSpec.
                            VendDns.au1DomainName,
                            pWssIfCapwapDB->
                            DnsProfileEntry.au1FsCapwapDnsDomainName,
                            VEND_DOMAIN_LEN);
                    pConfStatRsp->u2CapwapMsgElemenLen =
                        (UINT2) (pConfStatRsp->u2CapwapMsgElemenLen +
                                 (pConfStatRsp->
                                  vendSpec[VENDOR_DOMAIN_NAME_TYPE].u2MsgEleLen
                                  + CAPWAP_MSG_ELEM_TYPE_LEN));
                }
            }
            break;
        case VENDOR_NATIVE_VLAN:
            VendorSpecPayload.unVendorSpec.VendVlan.u2MsgEleType = elementType;
            CAPWAP_GET_2BYTE (VendorSpecPayload.unVendorSpec.
                              VendVlan.u2MsgEleLen, pRcvBuf);
            CAPWAP_GET_4BYTE (VendorSpecPayload.unVendorSpec.VendVlan.vendorId,
                              pRcvBuf);
            CAPWAP_GET_2BYTE (VendorSpecPayload.unVendorSpec.VendVlan.u2VlanId,
                              pRcvBuf);

            pWssIfCapwapDB->CapwapIsGetAllDB.bNativeVlan = TRUE;
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB) ==
                OSIX_SUCCESS)
            {
                if (VendorSpecPayload.unVendorSpec.VendVlan.u2VlanId !=
                    pWssIfCapwapDB->CapwapGetDB.u4NativeVlan)
                {
                    pConfStatRsp->vendSpec[VENDOR_NATIVE_VLAN].isOptional =
                        TRUE;
                    pConfStatRsp->vendSpec[VENDOR_NATIVE_VLAN].u2MsgEleType =
                        VENDOR_SPECIFIC_PAYLOAD;
                    pConfStatRsp->vendSpec[VENDOR_NATIVE_VLAN].u2MsgEleLen =
                        VEND_NATIVE_LEN;
                    pConfStatRsp->vendSpec[VENDOR_NATIVE_VLAN].
                        unVendorSpec.VendVlan.isOptional = TRUE;
                    pConfStatRsp->vendSpec[VENDOR_NATIVE_VLAN].
                        unVendorSpec.VendVlan.u2MsgEleType =
                        NATIVE_VLAN_VENDOR_MSG;
                    pConfStatRsp->vendSpec[VENDOR_NATIVE_VLAN].
                        unVendorSpec.VendVlan.u2MsgEleLen =
                        VEND_SPEC_NATIVE_LEN;
                    pConfStatRsp->vendSpec[VENDOR_NATIVE_VLAN].
                        unVendorSpec.VendVlan.vendorId = VENDOR_ID;
                    pConfStatRsp->vendSpec[VENDOR_NATIVE_VLAN].
                        unVendorSpec.VendVlan.u2VlanId =
                        (UINT2) pWssIfCapwapDB->CapwapGetDB.u4NativeVlan;
                    pConfStatRsp->u2CapwapMsgElemenLen =
                        (UINT2) (pConfStatRsp->u2CapwapMsgElemenLen +
                                 (pConfStatRsp->
                                  vendSpec[VENDOR_NATIVE_VLAN].u2MsgEleLen +
                                  CAPWAP_MSG_ELEM_TYPE_LEN));
                }
            }
            break;
        case VENDOR_MAC_WHITE_LIST_TYPE:
            break;
        case VENDOR_MGMT_SSID_TYPE:
            CAPWAP_GET_2BYTE (VendorSpecPayload.u2MsgEleType, pRcvBuf);
            CAPWAP_GET_2BYTE (VendorSpecPayload.u2MsgEleLen, pRcvBuf);
            CAPWAP_GET_2BYTE (VendorSpecPayload.unVendorSpec.
                              VendSsid.u2MsgEleType, pRcvBuf);
            CAPWAP_GET_2BYTE (VendorSpecPayload.unVendorSpec.
                              VendSsid.u2MsgEleLen, pRcvBuf);
            CAPWAP_GET_4BYTE (VendorSpecPayload.unVendorSpec.VendSsid.vendorId,
                              pRcvBuf);
            CAPWAP_GET_NBYTE (VendorSpecPayload.unVendorSpec.
                              VendSsid.au1ManagmentSSID, pRcvBuf,
                              WSSWLAN_SSID_NAME_LEN);
            if (WssWlanProcessWssIfMsg
                (WSS_WLAN_GET_MGMT_SSID, pWssWlanMsgStruct) == OSIX_SUCCESS)
            {
                if (MEMCMP
                    (VendorSpecPayload.unVendorSpec.VendSsid.au1ManagmentSSID,
                     pWssWlanMsgStruct->unWssWlanMsg.au1ManagmentSSID,
                     WSSWLAN_SSID_NAME_LEN) == 0)
                {
                    pConfStatRsp->vendSpec[VENDOR_MGMT_SSID_TYPE].isOptional =
                        TRUE;
                    pConfStatRsp->vendSpec[VENDOR_MGMT_SSID_TYPE].u2MsgEleType =
                        VENDOR_SPECIFIC_PAYLOAD;
                    pConfStatRsp->vendSpec[VENDOR_MGMT_SSID_TYPE].u2MsgEleLen =
                        VEND_DOMAIN_NAME_LEN;
                    pConfStatRsp->vendSpec[VENDOR_MGMT_SSID_TYPE].unVendorSpec.
                        VendSsid.isOptional = TRUE;
                    pConfStatRsp->vendSpec[VENDOR_MGMT_SSID_TYPE].unVendorSpec.
                        VendSsid.u2MsgEleType = MGMT_SSID_VENDOR_MSG;
                    pConfStatRsp->vendSpec[VENDOR_MGMT_SSID_TYPE].unVendorSpec.
                        VendSsid.u2MsgEleLen = VEND_DNS_LEN;
                    pConfStatRsp->vendSpec[VENDOR_MGMT_SSID_TYPE].
                        unVendorSpec.VendSsid.vendorId = VENDOR_ID;
                    MEMCPY (pConfStatRsp->
                            vendSpec[VENDOR_MGMT_SSID_TYPE].unVendorSpec.
                            VendSsid.au1ManagmentSSID,
                            pWssWlanMsgStruct->unWssWlanMsg.au1ManagmentSSID,
                            WSSWLAN_SSID_NAME_LEN);

                }
            }
            break;
#ifdef RFMGMT_WANTED
        case VENDOR_NEIGH_CONFIG_TYPE:
        {
#ifdef WLC_WANTED
            pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
            pWssIfCapwapDB->CapwapIsGetAllDB.bNoOfRadio = OSIX_TRUE;
            pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
                pCwMsg->u2IntProfileId;
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                         pWssIfCapwapDB) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Validation failure : Unable "
                            "to fetch the no of radios\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Validation failure : Unable "
                              "to fetch the no of radios\r\n"));
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
                return OSIX_FAILURE;
            }

            for (u1RadioId = 1;
                 u1RadioId <=
                 pCwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;
                 u1RadioId++)
            {
                RfMgmtMsgStruct.unRfMgmtMsg.
                    RfMgmtConfigStatusReq.NeighApTableConfig.u2MsgEleType =
                    elementType;

                CAPWAP_GET_2BYTE (RfMgmtMsgStruct.
                                  unRfMgmtMsg.RfMgmtConfigStatusReq.
                                  NeighApTableConfig.u2MsgEleLen, pRcvBuf);
                CAPWAP_GET_4BYTE (RfMgmtMsgStruct.
                                  unRfMgmtMsg.RfMgmtConfigStatusReq.
                                  NeighApTableConfig.u4VendorId, pRcvBuf);
                CAPWAP_GET_1BYTE (RfMgmtMsgStruct.
                                  unRfMgmtMsg.RfMgmtConfigStatusReq.
                                  NeighApTableConfig.u1RadioId, pRcvBuf);
                CAPWAP_GET_1BYTE (RfMgmtMsgStruct.
                                  unRfMgmtMsg.RfMgmtConfigStatusReq.
                                  NeighApTableConfig.u1AutoScanStatus, pRcvBuf);
                CAPWAP_GET_2BYTE (RfMgmtMsgStruct.
                                  unRfMgmtMsg.RfMgmtConfigStatusReq.
                                  NeighApTableConfig.u2NeighborMsgPeriod,
                                  pRcvBuf);
                CAPWAP_GET_2BYTE (RfMgmtMsgStruct.
                                  unRfMgmtMsg.RfMgmtConfigStatusReq.
                                  NeighApTableConfig.u2ChannelScanDuration,
                                  pRcvBuf);
                CAPWAP_GET_2BYTE (RfMgmtMsgStruct.
                                  unRfMgmtMsg.RfMgmtConfigStatusReq.
                                  NeighApTableConfig.u2NeighborAgingPeriod,
                                  pRcvBuf);
                CAPWAP_GET_INT_2BYTE (RfMgmtMsgStruct.
                                      unRfMgmtMsg.RfMgmtConfigStatusReq.
                                      NeighApTableConfig.i2RssiThreshold,
                                      pRcvBuf);

                RfMgmtMsgStruct.unRfMgmtMsg.
                    RfMgmtConfigStatusReq.u2WtpInternalId =
                    pCwMsg->u2IntProfileId;

                RfMgmtMsgStruct.unRfMgmtMsg.
                    RfMgmtConfigStatusReq.NeighApTableConfig.isOptional =
                    OSIX_TRUE;

                RfMgmtMsgStruct.u1Opcode = VENDOR_NEIGH_CONFIG_TYPE;

                if (WssIfProcessRfMgmtMsg (RFMGMT_VALIDATE_CONFIG_STATUS_REQ,
                                           &RfMgmtMsgStruct) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "Validation failure : NEIGH_CONFIG"
                                "Vendor Type in Config Status Request\r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Validation failure : NEIGH_CONFIG"
                                  "Vendor Type in Config Status Request\r\n"));
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
                    return OSIX_FAILURE;
                }
                if (WssIfProcessRfMgmtMsg (RFMGMT_CONFIG_STATUS_REQ,
                                           &RfMgmtMsgStruct) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Configuration Status"
                                "Request failure:NEIGH_CONFIG Vendor Type\r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Configuration Status"
                                  "Request failure:NEIGH_CONFIG Vendor Type\r\n"));
                }
                else
                {
                    if (RfMgmtMsgStruct.unRfMgmtMsg.
                        RfMgmtConfigStatusReq.NeighApTableConfig.isOptional ==
                        OSIX_TRUE)
                    {
                        pConfStatRsp->
                            vendSpec[VENDOR_NEIGH_CONFIG_TYPE].isOptional =
                            OSIX_TRUE;
                        pConfStatRsp->
                            vendSpec[VENDOR_NEIGH_CONFIG_TYPE].u2MsgEleType =
                            VENDOR_SPECIFIC_PAYLOAD;
                        pConfStatRsp->
                            vendSpec[VENDOR_NEIGH_CONFIG_TYPE].u2MsgEleLen =
                            CAPWAP_MSG_ELEM_TYPE_LEN +
                            RF_NEIGH_CONFIG_VENDOR_MSG_ELM_LEN;
                        pConfStatRsp->
                            vendSpec[VENDOR_NEIGH_CONFIG_TYPE].elementId =
                            VENDOR_NEIGH_CONFIG_TYPE;

                        pConfStatRsp->
                            vendSpec[VENDOR_NEIGH_CONFIG_TYPE].unVendorSpec.
                            VendorNeighConfig.isOptional = OSIX_TRUE;
                        pConfStatRsp->
                            vendSpec[VENDOR_NEIGH_CONFIG_TYPE].unVendorSpec.
                            VendorNeighConfig.u2MsgEleType =
                            NEIGH_CONFIG_VENDOR_MSG;
                        pConfStatRsp->
                            vendSpec[VENDOR_NEIGH_CONFIG_TYPE].unVendorSpec.
                            VendorNeighConfig.u2MsgEleLen =
                            RF_NEIGH_CONFIG_VENDOR_MSG_ELM_LEN;
                        pConfStatRsp->
                            vendSpec[VENDOR_NEIGH_CONFIG_TYPE].unVendorSpec.
                            VendorNeighConfig.u4VendorId = VENDOR_ID;

                        /* To get radio if index */
                        pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;
                        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
                            RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtConfigStatusReq.
                            u2WtpInternalId;
                        pWssIfCapwapDB->CapwapGetDB.u1RadioId =
                            RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtConfigStatusReq.
                            NeighApTableConfig.u1RadioId;
                        if (WssIfProcessCapwapDBMsg
                            (WSS_CAPWAP_GET_DB, pWssIfCapwapDB) != OSIX_SUCCESS)
                        {
                            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                        "CapwapSetVendSpecInfo: "
                                        "Getting Radio index failed\r\n");
                            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                          "CapwapSetVendSpecInfo: "
                                          "Getting Radio index failed\r\n"));
                            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                            UtlShMemFreeWssWlanBuf ((UINT1 *)
                                                    pWssWlanMsgStruct);
                            return OSIX_FAILURE;
                        }
                        RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
                            UtlShMemAllocAntennaSelectionBuf ();

                        if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection ==
                            NULL)
                        {
                            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                        "\r%% Memory allocation for Antenna"
                                        " selection failed.\r\n");
                            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                          "\r%% Memory allocation for Antenna"
                                          " selection failed.\r\n"));
                            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                            UtlShMemFreeWssWlanBuf ((UINT1 *)
                                                    pWssWlanMsgStruct);
                            return OSIX_FAILURE;
                        }

                        /* To get radio type */
                        RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
                        RadioIfGetDB.RadioIfIsGetAllDB.bDot11RadioType =
                            OSIX_TRUE;
                        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
                            pWssIfCapwapDB->CapwapGetDB.u4IfIndex;
                        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                                      &RadioIfGetDB) !=
                            OSIX_SUCCESS)
                        {
                            RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                                        "RfMgmtProcessConfigStatusReq: Getting "
                                        "Radio type failed\r\n");
                            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                          "RfMgmtProcessConfigStatusReq: Getting "
                                          "Radio type failed\r\n"));
                            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                            UtlShMemFreeAntennaSelectionBuf
                                (RadioIfGetDB.RadioIfGetAllDB.
                                 pu1AntennaSelection);
                            UtlShMemFreeWssWlanBuf ((UINT1 *)
                                                    pWssWlanMsgStruct);
                            return OSIX_FAILURE;
                        }

                        UtlShMemFreeAntennaSelectionBuf
                            (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
                        /* To get neigh config params */
                        RfMgmtDB.unRfMgmtDB.
                            RfMgmtApConfigTable.RfMgmtAPConfigDB.
                            u4RadioIfIndex =
                            pWssIfCapwapDB->CapwapGetDB.u4IfIndex;
                        RfMgmtDB.unRfMgmtDB.
                            RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
                            bNeighborMsgPeriod = OSIX_TRUE;
                        RfMgmtDB.unRfMgmtDB.
                            RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
                            bChannelScanDuration = OSIX_TRUE;
                        RfMgmtDB.unRfMgmtDB.
                            RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
                            bAutoScanStatus = OSIX_TRUE;
                        RfMgmtDB.unRfMgmtDB.
                            RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
                            bNeighborAgingPeriod = OSIX_TRUE;
                        if (WssIfProcessRfMgmtDBMsg
                            (RFMGMT_GET_AP_CONFIG_ENTRY,
                             &RfMgmtDB) != RFMGMT_SUCCESS)
                        {
                            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                        "Getting Neigh config parm failed\r\n");
                            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                          "Getting Neigh config parm failed\r\n"));
                            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                            UtlShMemFreeWssWlanBuf ((UINT1 *)
                                                    pWssWlanMsgStruct);
                            return OSIX_FAILURE;
                        }

                        /* To get rssi threshold */
                        RfMgmtThresDB.unRfMgmtDB.
                            RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
                            u4Dot11RadioType =
                            RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType;
                        RfMgmtThresDB.unRfMgmtDB.
                            RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                            bRssiThreshold = OSIX_TRUE;
                        if (WssIfProcessRfMgmtDBMsg
                            (RFMGMT_GET_AUTO_RF_ENTRY,
                             &RfMgmtThresDB) != RFMGMT_SUCCESS)
                        {
                            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                        "Getting Rssi Threshold failed\r\n");
                            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                          "Getting Rssi Threshold failed\r\n"));
                        }
                        pConfStatRsp->
                            vendSpec[VENDOR_NEIGH_CONFIG_TYPE].unVendorSpec.
                            VendorNeighConfig.VendorNeighParams[u1RadioId -
                                                                1].
                            u2NeighborMsgPeriod =
                            RfMgmtDB.unRfMgmtDB.
                            RfMgmtApConfigTable.RfMgmtAPConfigDB.
                            u2NeighborMsgPeriod;
                        pConfStatRsp->
                            vendSpec[VENDOR_NEIGH_CONFIG_TYPE].unVendorSpec.
                            VendorNeighConfig.VendorNeighParams[u1RadioId -
                                                                1].
                            u2NeighborAgingPeriod =
                            RfMgmtDB.unRfMgmtDB.
                            RfMgmtApConfigTable.RfMgmtAPConfigDB.
                            u2NeighborAgingPeriod;
                        pConfStatRsp->
                            vendSpec[VENDOR_NEIGH_CONFIG_TYPE].unVendorSpec.
                            VendorNeighConfig.VendorNeighParams[u1RadioId -
                                                                1].
                            u2ChannelScanDuration =
                            RfMgmtDB.unRfMgmtDB.
                            RfMgmtApConfigTable.RfMgmtAPConfigDB.
                            u2ChannelScanDuration;
                        pConfStatRsp->
                            vendSpec[VENDOR_NEIGH_CONFIG_TYPE].unVendorSpec.
                            VendorNeighConfig.VendorNeighParams[u1RadioId -
                                                                1].
                            i2RssiThreshold =
                            RfMgmtThresDB.unRfMgmtDB.
                            RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
                            i2RssiThreshold;
                        pConfStatRsp->
                            vendSpec[VENDOR_NEIGH_CONFIG_TYPE].unVendorSpec.
                            VendorNeighConfig.VendorNeighParams[u1RadioId -
                                                                1].u1RadioId =
                            u1RadioId;
                        pConfStatRsp->
                            vendSpec[VENDOR_NEIGH_CONFIG_TYPE].unVendorSpec.
                            VendorNeighConfig.VendorNeighParams[u1RadioId -
                                                                1].
                            u1AutoScanStatus =
                            RfMgmtDB.unRfMgmtDB.
                            RfMgmtApConfigTable.RfMgmtAPConfigDB.
                            u1AutoScanStatus;
                        pConfStatRsp->u2CapwapMsgElemenLen =
                            (UINT2) (pConfStatRsp->u2CapwapMsgElemenLen +
                                     (pConfStatRsp->
                                      vendSpec
                                      [VENDOR_NEIGH_CONFIG_TYPE].u2MsgEleLen +
                                      CAPWAP_MSG_ELEM_TYPE_LEN));
                    }
                }
            }
#endif
#ifdef WTP_WANTED
            for (u1RadioId = 1; u1RadioId <= u2NumRadios; u1RadioId++)
            {
                RfMgmtMsgStruct.unRfMgmtMsg.
                    RfMgmtConfigStatusRsp.NeighApTableConfig.u2MsgEleType =
                    elementType;

                CAPWAP_GET_2BYTE (RfMgmtMsgStruct.
                                  unRfMgmtMsg.RfMgmtConfigStatusRsp.
                                  NeighApTableConfig.u2MsgEleLen, pRcvBuf);
                CAPWAP_GET_4BYTE (RfMgmtMsgStruct.
                                  unRfMgmtMsg.RfMgmtConfigStatusRsp.
                                  NeighApTableConfig.u4VendorId, pRcvBuf);
                CAPWAP_GET_1BYTE (RfMgmtMsgStruct.
                                  unRfMgmtMsg.RfMgmtConfigStatusRsp.
                                  NeighApTableConfig.u1RadioId, pRcvBuf);
                CAPWAP_GET_1BYTE (RfMgmtMsgStruct.
                                  unRfMgmtMsg.RfMgmtConfigStatusRsp.
                                  NeighApTableConfig.u1AutoScanStatus, pRcvBuf);
                CAPWAP_GET_2BYTE (RfMgmtMsgStruct.
                                  unRfMgmtMsg.RfMgmtConfigStatusRsp.
                                  NeighApTableConfig.u2NeighborMsgPeriod,
                                  pRcvBuf);
                CAPWAP_GET_2BYTE (RfMgmtMsgStruct.
                                  unRfMgmtMsg.RfMgmtConfigStatusRsp.
                                  NeighApTableConfig.u2ChannelScanDuration,
                                  pRcvBuf);
                CAPWAP_GET_2BYTE (RfMgmtMsgStruct.
                                  unRfMgmtMsg.RfMgmtConfigStatusRsp.
                                  NeighApTableConfig.u2NeighborAgingPeriod,
                                  pRcvBuf);
                CAPWAP_GET_2BYTE (RfMgmtMsgStruct.
                                  unRfMgmtMsg.RfMgmtConfigStatusRsp.
                                  NeighApTableConfig.i2RssiThreshold, pRcvBuf);

                RfMgmtMsgStruct.unRfMgmtMsg.
                    RfMgmtConfigStatusRsp.u2WtpInternalId =
                    pCwMsg->u2IntProfileId;

                RfMgmtMsgStruct.unRfMgmtMsg.
                    RfMgmtConfigStatusRsp.NeighApTableConfig.isOptional =
                    OSIX_TRUE;

                RfMgmtMsgStruct.u1Opcode = VENDOR_NEIGH_CONFIG_TYPE;
                if (WssIfProcessRfMgmtMsg (RFMGMT_CONFIG_STATUS_RSP,
                                           &RfMgmtMsgStruct) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Configuration Status"
                                "Request failure:NEIGH_CONFIG Vendor Type\r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Configuration Status"
                                  "Request failure:NEIGH_CONFIG Vendor Type\r\n"));

                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
                    return OSIX_FAILURE;
                }
            }
#endif
        }
            break;
        case VENDOR_CLIENT_CONFIG_TYPE:
        {
#ifdef WLC_WANTED
            pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
            pWssIfCapwapDB->CapwapIsGetAllDB.bNoOfRadio = OSIX_TRUE;
            pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
                pCwMsg->u2IntProfileId;
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                         pWssIfCapwapDB) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Validation failure : Unable "
                            "to fetch the no of radios\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Validation failure : Unable "
                              "to fetch the no of radios\r\n"));
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
                return OSIX_FAILURE;
            }

            for (u1RadioId = 1;
                 u1RadioId <=
                 pCwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;
                 u1RadioId++)
            {
                RfMgmtMsgStruct.unRfMgmtMsg.
                    RfMgmtConfigStatusReq.ClientTableConfig.u2MsgEleType =
                    elementType;

                CAPWAP_GET_2BYTE (RfMgmtMsgStruct.
                                  unRfMgmtMsg.RfMgmtConfigStatusReq.
                                  ClientTableConfig.u2MsgEleLen, pRcvBuf);
                CAPWAP_GET_4BYTE (RfMgmtMsgStruct.
                                  unRfMgmtMsg.RfMgmtConfigStatusReq.
                                  ClientTableConfig.u4VendorId, pRcvBuf);
                CAPWAP_GET_1BYTE (RfMgmtMsgStruct.
                                  unRfMgmtMsg.RfMgmtConfigStatusReq.
                                  ClientTableConfig.u1RadioId, pRcvBuf);
                CAPWAP_GET_1BYTE (RfMgmtMsgStruct.
                                  unRfMgmtMsg.RfMgmtConfigStatusReq.
                                  ClientTableConfig.u1SNRScanStatus, pRcvBuf);
                CAPWAP_GET_2BYTE (RfMgmtMsgStruct.
                                  unRfMgmtMsg.RfMgmtConfigStatusReq.
                                  ClientTableConfig.u2SNRScanPeriod, pRcvBuf);
                CAPWAP_GET_INT_2BYTE (RfMgmtMsgStruct.
                                      unRfMgmtMsg.RfMgmtConfigStatusReq.
                                      ClientTableConfig.i2SNRThreshold,
                                      pRcvBuf);
                CAPWAP_GET_1BYTE (RfMgmtMsgStruct.
                                  unRfMgmtMsg.RfMgmtConfigStatusReq.
                                  ClientTableConfig.u1BssidScanStatus[u1WlanId],
                                  pRcvBuf);
                CAPWAP_GET_1BYTE (RfMgmtMsgStruct.
                                  unRfMgmtMsg.RfMgmtConfigStatusReq.
                                  ClientTableConfig.u1WlanId, pRcvBuf);

                RfMgmtMsgStruct.unRfMgmtMsg.
                    RfMgmtConfigStatusReq.u2WtpInternalId =
                    pCwMsg->u2IntProfileId;

                RfMgmtMsgStruct.unRfMgmtMsg.
                    RfMgmtConfigStatusReq.ClientTableConfig.isOptional =
                    OSIX_TRUE;

                RfMgmtMsgStruct.u1Opcode = VENDOR_CLIENT_CONFIG_TYPE;

                if (WssIfProcessRfMgmtMsg (RFMGMT_VALIDATE_CONFIG_STATUS_REQ,
                                           &RfMgmtMsgStruct) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "Validation failure : CLIENT_CONFIG"
                                "Vendor Type in Config Status Request\r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Validation failure : CLIENT_CONFIG"
                                  "Vendor Type in Config Status Request\r\n"));
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
                    return OSIX_FAILURE;
                }
                if (WssIfProcessRfMgmtMsg (RFMGMT_CONFIG_STATUS_REQ,
                                           &RfMgmtMsgStruct) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Configuration Status"
                                "Request failure:CLIENT_CONFIG Vendor Type\r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Configuration Status"
                                  "Request failure:CLIENT_CONFIG Vendor Type\r\n"));
                }
                else
                {
                    if (RfMgmtMsgStruct.unRfMgmtMsg.
                        RfMgmtConfigStatusReq.ClientTableConfig.isOptional ==
                        OSIX_TRUE)
                    {
                        pConfStatRsp->
                            vendSpec[VENDOR_CLIENT_CONFIG_TYPE].isOptional =
                            OSIX_TRUE;
                        pConfStatRsp->
                            vendSpec[VENDOR_CLIENT_CONFIG_TYPE].u2MsgEleType =
                            VENDOR_SPECIFIC_PAYLOAD;
                        pConfStatRsp->
                            vendSpec[VENDOR_CLIENT_CONFIG_TYPE].u2MsgEleLen =
                            CAPWAP_MSG_ELEM_TYPE_LEN +
                            RF_CLIENT_CONFIG_VENDOR_MSG_ELM_LEN;
                        pConfStatRsp->
                            vendSpec[VENDOR_CLIENT_CONFIG_TYPE].elementId =
                            VENDOR_CLIENT_CONFIG_TYPE;
                        pConfStatRsp->
                            vendSpec[VENDOR_CLIENT_CONFIG_TYPE].unVendorSpec.
                            VendorClientConfig.isOptional = OSIX_TRUE;
                        pConfStatRsp->
                            vendSpec[VENDOR_CLIENT_CONFIG_TYPE].unVendorSpec.
                            VendorClientConfig.u2MsgEleType =
                            CLIENT_CONFIG_VENDOR_MSG;
                        pConfStatRsp->
                            vendSpec[VENDOR_CLIENT_CONFIG_TYPE].unVendorSpec.
                            VendorClientConfig.u2MsgEleLen =
                            RF_CLIENT_CONFIG_VENDOR_MSG_ELM_LEN;
                        pConfStatRsp->
                            vendSpec[VENDOR_CLIENT_CONFIG_TYPE].unVendorSpec.
                            VendorClientConfig.u4VendorId = VENDOR_ID;

                        /* To get Radio if index */
                        pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;
                        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
                            RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtConfigStatusReq.
                            u2WtpInternalId;
                        pWssIfCapwapDB->CapwapGetDB.u1RadioId =
                            RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtConfigStatusReq.
                            ClientTableConfig.u1RadioId;
                        if (WssIfProcessCapwapDBMsg
                            (WSS_CAPWAP_GET_DB, pWssIfCapwapDB) != OSIX_SUCCESS)
                        {
                            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                        "CapwapSetVendSpecInfo: "
                                        "Getting Radio index failed\r\n");
                            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                          "CapwapSetVendSpecInfo: "
                                          "Getting Radio index failed\r\n"));
                            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                            UtlShMemFreeWssWlanBuf ((UINT1 *)
                                                    pWssWlanMsgStruct);
                            return OSIX_FAILURE;
                        }
                        RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
                            UtlShMemAllocAntennaSelectionBuf ();

                        if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection ==
                            NULL)
                        {
                            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                        "\r%% Memory allocation for Antenna"
                                        " selection failed.\r\n");
                            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                          "\r%% Memory allocation for Antenna"
                                          " selection failed.\r\n"));
                            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                            UtlShMemFreeWssWlanBuf ((UINT1 *)
                                                    pWssWlanMsgStruct);
                            return OSIX_FAILURE;
                        }

                        /* To get radio type */
                        RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
                        RadioIfGetDB.RadioIfIsGetAllDB.bDot11RadioType =
                            OSIX_TRUE;
                        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
                            pWssIfCapwapDB->CapwapGetDB.u4IfIndex;

                        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                                      &RadioIfGetDB) !=
                            OSIX_SUCCESS)
                        {
                            RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                                        "RfMgmtProcessConfigStatusReq : Getting "
                                        "Radio type failed\r\n");
                            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                          "RfMgmtProcessConfigStatusReq : Getting "
                                          "Radio type failed\r\n"));
                            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                            UtlShMemFreeAntennaSelectionBuf
                                (RadioIfGetDB.RadioIfGetAllDB.
                                 pu1AntennaSelection);
                            UtlShMemFreeWssWlanBuf ((UINT1 *)
                                                    pWssWlanMsgStruct);
                            return OSIX_FAILURE;

                        }
                        UtlShMemFreeAntennaSelectionBuf
                            (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);

                        /* To get client config params */
                        RfMgmtDB.unRfMgmtDB.
                            RfMgmtClientConfigTable.RfMgmtClientConfigDB.
                            u4RadioIfIndex =
                            pWssIfCapwapDB->CapwapGetDB.u4IfIndex;
                        RfMgmtDB.unRfMgmtDB.
                            RfMgmtClientConfigTable.RfMgmtClientConfigIsSetDB.
                            bSNRScanPeriod = OSIX_TRUE;
                        RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
                            RfMgmtClientConfigIsSetDB.bSNRScanStatus =
                            OSIX_TRUE;
                        if (WssIfProcessRfMgmtDBMsg
                            (RFMGMT_GET_CLIENT_CONFIG_ENTRY,
                             &RfMgmtDB) != RFMGMT_SUCCESS)
                        {
                            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                        "Getting client config params "
                                        "failed\r\n");
                            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                          "Getting client config params "
                                          "failed\r\n"));
                            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                            UtlShMemFreeWssWlanBuf ((UINT1 *)
                                                    pWssWlanMsgStruct);
                            return OSIX_FAILURE;
                        }

                        /* To get Snr threshold */
                        RfMgmtThresDB.unRfMgmtDB.
                            RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
                            u4Dot11RadioType =
                            RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType;
                        RfMgmtThresDB.unRfMgmtDB.
                            RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                            bSNRThreshold = OSIX_TRUE;
                        if (WssIfProcessRfMgmtDBMsg
                            (RFMGMT_GET_AUTO_RF_ENTRY,
                             &RfMgmtThresDB) != RFMGMT_SUCCESS)
                        {
                            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                        "Getting SNR Threshold failed\r\n");
                            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                          "Getting SNR Threshold failed\r\n"));

                        }
                        pConfStatRsp->
                            vendSpec[VENDOR_CLIENT_CONFIG_TYPE].unVendorSpec.
                            VendorClientConfig.VendorClientParams[u1RadioId -
                                                                  1].
                            u2SNRScanPeriod =
                            RfMgmtDB.unRfMgmtDB.
                            RfMgmtClientConfigTable.RfMgmtClientConfigDB.
                            u2SNRScanPeriod;
                        pConfStatRsp->
                            vendSpec[VENDOR_CLIENT_CONFIG_TYPE].unVendorSpec.
                            VendorClientConfig.VendorClientParams[u1RadioId -
                                                                  1].
                            i2SNRThreshold =
                            RfMgmtThresDB.unRfMgmtDB.
                            RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
                            i2SNRThreshold;
                        pConfStatRsp->
                            vendSpec[VENDOR_CLIENT_CONFIG_TYPE].unVendorSpec.
                            VendorClientConfig.VendorClientParams[u1RadioId -
                                                                  1].u1RadioId =
                            u1RadioId;
                        pConfStatRsp->
                            vendSpec[VENDOR_CLIENT_CONFIG_TYPE].unVendorSpec.
                            VendorClientConfig.VendorClientParams[u1RadioId -
                                                                  1].
                            u1SNRScanStatus =
                            RfMgmtDB.unRfMgmtDB.
                            RfMgmtClientConfigTable.RfMgmtClientConfigDB.
                            u1SNRScanStatus;
                        pConfStatRsp->
                            vendSpec[VENDOR_CLIENT_CONFIG_TYPE].unVendorSpec.
                            VendorClientConfig.VendorClientParams[u1RadioId -
                                                                  1].
                            u1BssidScanStatus[u1WlanId] =
                            RfMgmtDB.unRfMgmtDB.RfMgmtBssidScanTable.
                            RfMgmtBssidScanDB.u1AutoScanStatus;
                        pConfStatRsp->vendSpec[VENDOR_CLIENT_CONFIG_TYPE].
                            unVendorSpec.VendorClientConfig.
                            VendorClientParams[u1RadioId - 1].u1WlanId =
                            RfMgmtDB.unRfMgmtDB.RfMgmtBssidScanTable.
                            RfMgmtBssidScanDB.u1WlanID;

                        pConfStatRsp->u2CapwapMsgElemenLen = (UINT2)
                            (pConfStatRsp->u2CapwapMsgElemenLen +
                             (pConfStatRsp->
                              vendSpec[VENDOR_CLIENT_CONFIG_TYPE].u2MsgEleLen +
                              CAPWAP_MSG_ELEM_TYPE_LEN));
                    }
                }
            }
#endif
#ifdef WTP_WANTED
            for (u1RadioId = 1; u1RadioId <= u2NumRadios; u1RadioId++)
            {
                RfMgmtMsgStruct.unRfMgmtMsg.
                    RfMgmtConfigStatusRsp.ClientTableConfig.u2MsgEleType =
                    elementType;

                CAPWAP_GET_2BYTE (RfMgmtMsgStruct.
                                  unRfMgmtMsg.RfMgmtConfigStatusRsp.
                                  ClientTableConfig.u2MsgEleLen, pRcvBuf);
                CAPWAP_GET_4BYTE (RfMgmtMsgStruct.
                                  unRfMgmtMsg.RfMgmtConfigStatusRsp.
                                  ClientTableConfig.u4VendorId, pRcvBuf);
                CAPWAP_GET_1BYTE (RfMgmtMsgStruct.
                                  unRfMgmtMsg.RfMgmtConfigStatusRsp.
                                  ClientTableConfig.u1RadioId, pRcvBuf);
                CAPWAP_GET_1BYTE (RfMgmtMsgStruct.
                                  unRfMgmtMsg.RfMgmtConfigStatusRsp.
                                  ClientTableConfig.u1SNRScanStatus, pRcvBuf);
                CAPWAP_GET_2BYTE (RfMgmtMsgStruct.
                                  unRfMgmtMsg.RfMgmtConfigStatusRsp.
                                  ClientTableConfig.u2SNRScanPeriod, pRcvBuf);
                CAPWAP_GET_2BYTE (RfMgmtMsgStruct.
                                  unRfMgmtMsg.RfMgmtConfigStatusRsp.
                                  ClientTableConfig.i2SNRThreshold, pRcvBuf);
                CAPWAP_GET_1BYTE (RfMgmtMsgStruct.
                                  unRfMgmtMsg.RfMgmtConfigStatusRsp.
                                  ClientTableConfig.u1BssidScanStatus[u1WlanId],
                                  pRcvBuf);
                CAPWAP_GET_1BYTE (RfMgmtMsgStruct.
                                  unRfMgmtMsg.RfMgmtConfigStatusRsp.
                                  ClientTableConfig.u1WlanId, pRcvBuf);

                RfMgmtMsgStruct.unRfMgmtMsg.
                    RfMgmtConfigStatusRsp.u2WtpInternalId =
                    pCwMsg->u2IntProfileId;

                RfMgmtMsgStruct.unRfMgmtMsg.
                    RfMgmtConfigStatusRsp.ClientTableConfig.isOptional =
                    OSIX_TRUE;

                RfMgmtMsgStruct.u1Opcode = VENDOR_CLIENT_CONFIG_TYPE;

                if (WssIfProcessRfMgmtMsg (RFMGMT_CONFIG_STATUS_RSP,
                                           &RfMgmtMsgStruct) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Configuration Status"
                                "Request failure:CLIENT_CONFIG Vendor Type\r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Configuration Status"
                                  "Request failure:CLIENT_CONFIG Vendor Type\r\n"));
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
                    return OSIX_FAILURE;
                }
            }
#endif
        }
            break;
        case VENDOR_CH_SWITCH_STATUS_TYPE:
        {
#ifdef WLC_WANTED
            pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
            pWssIfCapwapDB->CapwapIsGetAllDB.bNoOfRadio = OSIX_TRUE;
            pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
                pCwMsg->u2IntProfileId;
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                         pWssIfCapwapDB) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Validation failure : Unable "
                            "to fetch the no of radios\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Validation failure : Unable "
                              "to fetch the no of radios\r\n"));
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
                return OSIX_FAILURE;
            }
            for (u1RadioId = 1;
                 u1RadioId <=
                 pCwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;
                 u1RadioId++)
            {
                RfMgmtMsgStruct.unRfMgmtMsg.
                    RfMgmtConfigStatusReq.ChSwitchStatusTable.u2MsgEleType =
                    elementType;

                CAPWAP_GET_2BYTE (RfMgmtMsgStruct.
                                  unRfMgmtMsg.RfMgmtConfigStatusReq.
                                  ChSwitchStatusTable.u2MsgEleLen, pRcvBuf);
                CAPWAP_GET_4BYTE (RfMgmtMsgStruct.
                                  unRfMgmtMsg.RfMgmtConfigStatusReq.
                                  ChSwitchStatusTable.u4VendorId, pRcvBuf);
                CAPWAP_GET_1BYTE (RfMgmtMsgStruct.
                                  unRfMgmtMsg.RfMgmtConfigStatusReq.
                                  ChSwitchStatusTable.u1RadioId, pRcvBuf);
                CAPWAP_GET_1BYTE (RfMgmtMsgStruct.
                                  unRfMgmtMsg.RfMgmtConfigStatusReq.
                                  ChSwitchStatusTable.u1ChSwitchStatus,
                                  pRcvBuf);

                RfMgmtMsgStruct.unRfMgmtMsg.
                    RfMgmtConfigStatusReq.u2WtpInternalId =
                    pCwMsg->u2IntProfileId;

                RfMgmtMsgStruct.unRfMgmtMsg.
                    RfMgmtConfigStatusReq.ChSwitchStatusTable.isOptional =
                    OSIX_TRUE;

                RfMgmtMsgStruct.u1Opcode = VENDOR_CH_SWITCH_STATUS_TYPE;

                if (WssIfProcessRfMgmtMsg (RFMGMT_VALIDATE_CONFIG_STATUS_REQ,
                                           &RfMgmtMsgStruct) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "Validation failure : CH_SWITCH_MSG"
                                "Vendor Type in Config Status Request\r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Validation failure : CH_SWITCH_MSG"
                                  "Vendor Type in Config Status Request\r\n"));
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
                    return OSIX_FAILURE;
                }
                if (WssIfProcessRfMgmtMsg (RFMGMT_CONFIG_STATUS_REQ,
                                           &RfMgmtMsgStruct) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Configuration Status"
                                "Request failure:CH_SWITCH_MSG Vendor Type\r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Configuration Status"
                                  "Request failure:CH_SWITCH_MSG Vendor Type\r\n"));

                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
                    return OSIX_FAILURE;
                }
                else
                {
                    if (RfMgmtMsgStruct.unRfMgmtMsg.
                        RfMgmtConfigStatusReq.ChSwitchStatusTable.isOptional ==
                        OSIX_TRUE)
                    {
                        pConfStatRsp->
                            vendSpec[VENDOR_CH_SWITCH_STATUS_TYPE].isOptional =
                            OSIX_TRUE;
                        pConfStatRsp->
                            vendSpec[VENDOR_CH_SWITCH_STATUS_TYPE].u2MsgEleType
                            = VENDOR_SPECIFIC_PAYLOAD;
                        pConfStatRsp->
                            vendSpec[VENDOR_CH_SWITCH_STATUS_TYPE].u2MsgEleLen =
                            CAPWAP_MSG_ELEM_TYPE_LEN +
                            RF_CH_SWITCH_STATUS_VENDOR_MSG_ELM_LEN;
                        pConfStatRsp->
                            vendSpec[VENDOR_CH_SWITCH_STATUS_TYPE].elementId =
                            VENDOR_CH_SWITCH_STATUS_TYPE;
                        pConfStatRsp->
                            vendSpec[VENDOR_CH_SWITCH_STATUS_TYPE].unVendorSpec.
                            VendorChSwitchStatus.isOptional = OSIX_TRUE;
                        pConfStatRsp->
                            vendSpec[VENDOR_CH_SWITCH_STATUS_TYPE].unVendorSpec.
                            VendorChSwitchStatus.u2MsgEleType =
                            CH_SWITCH_STATUS_VENDOR_MSG;
                        pConfStatRsp->
                            vendSpec[VENDOR_CH_SWITCH_STATUS_TYPE].unVendorSpec.
                            VendorChSwitchStatus.u2MsgEleLen =
                            RF_CH_SWITCH_STATUS_VENDOR_MSG_ELM_LEN;
                        pConfStatRsp->
                            vendSpec[VENDOR_CH_SWITCH_STATUS_TYPE].unVendorSpec.
                            VendorChSwitchStatus.u4VendorId = VENDOR_ID;

                        /* To get radio if index */
                        pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;
                        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
                            RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtConfigStatusReq.
                            u2WtpInternalId;
                        pWssIfCapwapDB->CapwapGetDB.u1RadioId =
                            RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtConfigStatusReq.
                            ChSwitchStatusTable.u1RadioId;
                        if (WssIfProcessCapwapDBMsg
                            (WSS_CAPWAP_GET_DB, pWssIfCapwapDB) != OSIX_SUCCESS)
                        {
                            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                        "CapwapSetVendSpecInfo: "
                                        "Getting Radio index failed\r\n");
                            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                          "CapwapSetVendSpecInfo: "
                                          "Getting Radio index failed\r\n"));
                            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                            UtlShMemFreeWssWlanBuf ((UINT1 *)
                                                    pWssWlanMsgStruct);
                            return OSIX_FAILURE;
                        }

                        /* To get channel switch msg */
                        RfMgmtDB.unRfMgmtDB.
                            RfMgmtApConfigTable.RfMgmtAPConfigDB.
                            u4RadioIfIndex =
                            pWssIfCapwapDB->CapwapGetDB.u4IfIndex;

                        pConfStatRsp->vendSpec
                            [VENDOR_CH_SWITCH_STATUS_TYPE].unVendorSpec.
                            VendorChSwitchStatus.VendorChSwitchParams[u1RadioId
                                                                      -
                                                                      1].
                            u1RadioId = u1RadioId;
                        pConfStatRsp->
                            vendSpec[VENDOR_CH_SWITCH_STATUS_TYPE].unVendorSpec.
                            VendorChSwitchStatus.VendorChSwitchParams[u1RadioId
                                                                      -
                                                                      1].
                            u1ChSwitchStatus =
                            (UINT1) gu4ChannelSwitchMsgStatus;

                        pConfStatRsp->u2CapwapMsgElemenLen = (UINT2)
                            (pConfStatRsp->u2CapwapMsgElemenLen +
                             (pConfStatRsp->vendSpec
                              [VENDOR_CH_SWITCH_STATUS_TYPE].u2MsgEleLen +
                              CAPWAP_MSG_ELEM_TYPE_LEN));
                    }
                }
            }
#endif
#ifdef WTP_WANTED
            for (u1RadioId = 1; u1RadioId <= u2NumRadios; u1RadioId++)
            {
                RfMgmtMsgStruct.unRfMgmtMsg.
                    RfMgmtConfigStatusRsp.ChSwitchStatusTable.u2MsgEleType =
                    elementType;

                CAPWAP_GET_2BYTE (RfMgmtMsgStruct.
                                  unRfMgmtMsg.RfMgmtConfigStatusRsp.
                                  ChSwitchStatusTable.u2MsgEleLen, pRcvBuf);
                CAPWAP_GET_4BYTE (RfMgmtMsgStruct.
                                  unRfMgmtMsg.RfMgmtConfigStatusRsp.
                                  ChSwitchStatusTable.u4VendorId, pRcvBuf);
                CAPWAP_GET_1BYTE (RfMgmtMsgStruct.
                                  unRfMgmtMsg.RfMgmtConfigStatusRsp.
                                  ChSwitchStatusTable.u1RadioId, pRcvBuf);
                CAPWAP_GET_1BYTE (RfMgmtMsgStruct.
                                  unRfMgmtMsg.RfMgmtConfigStatusRsp.
                                  ChSwitchStatusTable.u1ChSwitchStatus,
                                  pRcvBuf);

                RfMgmtMsgStruct.unRfMgmtMsg.
                    RfMgmtConfigStatusRsp.u2WtpInternalId =
                    pCwMsg->u2IntProfileId;

                RfMgmtMsgStruct.unRfMgmtMsg.
                    RfMgmtConfigStatusRsp.ChSwitchStatusTable.isOptional =
                    OSIX_TRUE;

                RfMgmtMsgStruct.u1Opcode = VENDOR_CH_SWITCH_STATUS_TYPE;

                if (WssIfProcessRfMgmtMsg (RFMGMT_CONFIG_STATUS_RSP,
                                           &RfMgmtMsgStruct) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "Configuration Status Request "
                                "failure: CH_SWITCH_MSG Vendor Type\r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Configuration Status Request "
                                  "failure: CH_SWITCH_MSG Vendor Type\r\n"));
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
                    return OSIX_FAILURE;
                }
            }
#endif
        }
            break;
        case VENDOR_CH_SCANNED_LIST:
        {
#ifdef WLC_WANTED
            pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
            pWssIfCapwapDB->CapwapIsGetAllDB.bNoOfRadio = OSIX_TRUE;
            pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
                pCwMsg->u2IntProfileId;
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                         pWssIfCapwapDB) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Validation failure : Unable "
                            "to fetch the no of radios\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Validation failure : Unable "
                              "to fetch the no of radios\r\n"));
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
                return OSIX_FAILURE;
            }
            for (u1RadioId = 1;
                 u1RadioId <=
                 pCwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;
                 u1RadioId++)
            {
                CAPWAP_SKIP_N_BYTES (6, pRcvBuf);
                CAPWAP_GET_1BYTE (RfMgmtMsgStruct.
                                  unRfMgmtMsg.RfMgmtScanChannelList.
                                  u1RadioId, pRcvBuf);

                CAPWAP_GET_NBYTE (RfMgmtMsgStruct.
                                  unRfMgmtMsg.RfMgmtScanChannelList.
                                  au1ScannedList, pRcvBuf,
                                  (UINT4) (VendorSpecPayload.u2MsgEleLen - 9));

                RfMgmtMsgStruct.unRfMgmtMsg.
                    RfMgmtScanChannelList.u2WtpInternalId =
                    pCwMsg->u2IntProfileId;

                if (WssIfProcessRfMgmtMsg (RFMGMT_VALIDATE_SCANNED_CHANNEL,
                                           &RfMgmtMsgStruct) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "Validation failure : NEIGH_CONFIG"
                                "Vendor Type in Config Status Request\r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Validation failure : NEIGH_CONFIG"
                                  "Vendor Type in Config Status Request\r\n"));
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
                    return OSIX_FAILURE;
                }

            }

#endif
        }
            break;
        case VENDOR_SPECT_MGMT_TPC_TYPE:
        {
#ifdef WLC_WANTED
            pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
            pWssIfCapwapDB->CapwapIsGetAllDB.bNoOfRadio = OSIX_TRUE;
            pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
                pCwMsg->u2IntProfileId;
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                         pWssIfCapwapDB) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Validation failure : Unable "
                            "to fetch the no of radios\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Validation failure : Unable "
                              "to fetch the no of radios\r\n"));
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
                return OSIX_FAILURE;
            }
            for (u1RadioId = 1;
                 u1RadioId <=
                 pCwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;
                 u1RadioId++)
            {
                RfMgmtMsgStruct.unRfMgmtMsg.
                    RfMgmtConfigStatusReq.TpcSpectMgmtTable.u2MsgEleType =
                    elementType;

                CAPWAP_GET_2BYTE (RfMgmtMsgStruct.
                                  unRfMgmtMsg.RfMgmtConfigStatusReq.
                                  TpcSpectMgmtTable.u2MsgEleLen, pRcvBuf);
                CAPWAP_GET_4BYTE (RfMgmtMsgStruct.
                                  unRfMgmtMsg.RfMgmtConfigStatusReq.
                                  TpcSpectMgmtTable.u4VendorId, pRcvBuf);
                CAPWAP_GET_1BYTE (RfMgmtMsgStruct.
                                  unRfMgmtMsg.RfMgmtConfigStatusReq.
                                  TpcSpectMgmtTable.u1RadioId, pRcvBuf);
                CAPWAP_GET_2BYTE (RfMgmtMsgStruct.
                                  unRfMgmtMsg.RfMgmtConfigStatusReq.
                                  TpcSpectMgmtTable.u2TpcRequestInterval,
                                  pRcvBuf);
                CAPWAP_GET_1BYTE (RfMgmtMsgStruct.
                                  unRfMgmtMsg.RfMgmtConfigStatusReq.
                                  TpcSpectMgmtTable.u111hTpcStatus, pRcvBuf);

                RfMgmtMsgStruct.unRfMgmtMsg.
                    RfMgmtConfigStatusReq.u2WtpInternalId =
                    pCwMsg->u2IntProfileId;

                RfMgmtMsgStruct.unRfMgmtMsg.
                    RfMgmtConfigStatusReq.TpcSpectMgmtTable.isOptional =
                    OSIX_TRUE;

                RfMgmtMsgStruct.u1Opcode = VENDOR_SPECT_MGMT_TPC_TYPE;

                if (WssIfProcessRfMgmtMsg (RFMGMT_VALIDATE_CONFIG_STATUS_REQ,
                                           &RfMgmtMsgStruct) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "Validation failure : Vendor Spect Mgmt"
                                "Vendor Type in Config Status Request\r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Validation failure : Vendor Spect Mgmt"
                                  "Vendor Type in Config Status Request\r\n"));
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
                    return OSIX_FAILURE;
                }
                if (WssIfProcessRfMgmtMsg (RFMGMT_CONFIG_STATUS_REQ,
                                           &RfMgmtMsgStruct) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Configuration Status"
                                "Request failure:Vendor Spect Mgmt Vendor "
                                " Type\r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Configuration Status"
                                  "Request failure:Vendor Spect Mgmt Vendor "
                                  " Type\r\n"));
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
                    return OSIX_FAILURE;
                }
                else
                {
                    if (RfMgmtMsgStruct.unRfMgmtMsg.
                        RfMgmtConfigStatusReq.TpcSpectMgmtTable.isOptional ==
                        OSIX_TRUE)
                    {
                        pConfStatRsp->
                            vendSpec[VENDOR_SPECT_MGMT_TPC_TYPE].isOptional =
                            OSIX_TRUE;
                        pConfStatRsp->
                            vendSpec[VENDOR_SPECT_MGMT_TPC_TYPE].u2MsgEleType
                            = VENDOR_SPECIFIC_PAYLOAD;
                        pConfStatRsp->
                            vendSpec[VENDOR_SPECT_MGMT_TPC_TYPE].u2MsgEleLen =
                            CAPWAP_MSG_ELEM_TYPE_LEN +
                            TPC_SPECT_MGMT_VENDOR_MSG_ELM_LEN;
                        pConfStatRsp->
                            vendSpec[VENDOR_SPECT_MGMT_TPC_TYPE].elementId =
                            VENDOR_SPECT_MGMT_TPC_TYPE;
                        pConfStatRsp->
                            vendSpec[VENDOR_SPECT_MGMT_TPC_TYPE].unVendorSpec.
                            VendorTpcSpectMgmt.isOptional = OSIX_TRUE;
                        pConfStatRsp->
                            vendSpec[VENDOR_SPECT_MGMT_TPC_TYPE].unVendorSpec.
                            VendorTpcSpectMgmt.u2MsgEleType =
                            VENDOR_SPECT_MGMT_TPC_MSG;
                        pConfStatRsp->
                            vendSpec[VENDOR_SPECT_MGMT_TPC_TYPE].unVendorSpec.
                            VendorTpcSpectMgmt.u2MsgEleLen =
                            TPC_SPECT_MGMT_VENDOR_MSG_ELM_LEN;
                        pConfStatRsp->
                            vendSpec[VENDOR_SPECT_MGMT_TPC_TYPE].unVendorSpec.
                            VendorTpcSpectMgmt.u4VendorId = VENDOR_ID;

                        /* To get radio if index */
                        pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;
                        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
                            RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtConfigStatusReq.
                            u2WtpInternalId;
                        pWssIfCapwapDB->CapwapGetDB.u1RadioId =
                            RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtConfigStatusReq.
                            TpcSpectMgmtTable.u1RadioId;
                        if (WssIfProcessCapwapDBMsg
                            (WSS_CAPWAP_GET_DB, pWssIfCapwapDB) != OSIX_SUCCESS)
                        {
                            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                        "CapwapSetVendSpecInfo: "
                                        "Getting Radio index failed\r\n");
                            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                          "CapwapSetVendSpecInfo: "
                                          "Getting Radio index failed\r\n"));
                            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                            UtlShMemFreeWssWlanBuf ((UINT1 *)
                                                    pWssWlanMsgStruct);
                            return OSIX_FAILURE;
                        }

                        /* To get Tpc request interval */
                        RfMgmtDB.unRfMgmtDB.
                            RfMgmtApConfigTable.RfMgmtAPConfigDB.
                            u4RadioIfIndex =
                            pWssIfCapwapDB->CapwapGetDB.u4IfIndex;

                        RfMgmtDB.unRfMgmtDB.
                            RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
                            bRfMgmt11hTpcRequestInterval = OSIX_TRUE;

                        if (WssIfProcessRfMgmtDBMsg
                            (RFMGMT_GET_AP_CONFIG_ENTRY,
                             &RfMgmtDB) != RFMGMT_SUCCESS)
                        {
                            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                        "Getting TPC request interval "
                                        " failed\r\n");
                            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                          "Getting TPC request interval "
                                          " failed\r\n"));
                            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                            UtlShMemFreeWssWlanBuf ((UINT1 *)
                                                    pWssWlanMsgStruct);
                            return OSIX_FAILURE;
                        }

                        pConfStatRsp->vendSpec
                            [VENDOR_SPECT_MGMT_TPC_TYPE].unVendorSpec.
                            VendorTpcSpectMgmt.VendorTpcSpectMgmtParams
                            [u1RadioId - 1].u1RadioId =
                            RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtConfigStatusReq.
                            TpcSpectMgmtTable.u1RadioId;
                        pConfStatRsp->vendSpec[VENDOR_SPECT_MGMT_TPC_TYPE].
                            unVendorSpec.VendorTpcSpectMgmt.
                            VendorTpcSpectMgmtParams[u1RadioId -
                                                     1].u2TpcRequestInterval =
                            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                            RfMgmtAPConfigDB.u2RfMgmt11hTpcRequestInterval;

                        MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
                        RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
                            RfMgmtAutoRfProfileDB.
                            u4Dot11RadioType = RFMGMT_RADIO_TYPEA;
                        RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
                            RfMgmtAutoRfIsSetDB.bRfMgmt11hTpcStatus = OSIX_TRUE;

                        if (WssIfProcessRfMgmtDBMsg
                            (RFMGMT_GET_AUTO_RF_ENTRY,
                             &RfMgmtDB) != RFMGMT_SUCCESS)
                        {
                            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                        "Getting TPC Status failed\r\n");
                            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                          "Getting TPC Status failed\r\n"));
                            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                            UtlShMemFreeWssWlanBuf ((UINT1 *)
                                                    pWssWlanMsgStruct);
                            return OSIX_FAILURE;
                        }

                        pConfStatRsp->
                            vendSpec[VENDOR_SPECT_MGMT_TPC_TYPE].unVendorSpec.
                            VendorTpcSpectMgmt.VendorTpcSpectMgmtParams
                            [u1RadioId - 1].u111hTpcStatus
                            = RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
                            RfMgmtAutoRfProfileDB.u1RfMgmt11hTpcStatus;

                        pConfStatRsp->u2CapwapMsgElemenLen = (UINT2)
                            (pConfStatRsp->u2CapwapMsgElemenLen +
                             (pConfStatRsp->vendSpec
                              [VENDOR_SPECT_MGMT_TPC_TYPE].u2MsgEleLen +
                              CAPWAP_MSG_ELEM_TYPE_LEN));
                    }
                }
            }
#endif
#ifdef WTP_WANTED
            for (u1RadioId = 1; u1RadioId <= u2NumRadios; u1RadioId++)
            {
                RfMgmtMsgStruct.unRfMgmtMsg.
                    RfMgmtConfigStatusRsp.TpcSpectMgmtTable.u2MsgEleType =
                    elementType;

                CAPWAP_GET_2BYTE (RfMgmtMsgStruct.
                                  unRfMgmtMsg.RfMgmtConfigStatusRsp.
                                  TpcSpectMgmtTable.u2MsgEleLen, pRcvBuf);
                CAPWAP_GET_4BYTE (RfMgmtMsgStruct.
                                  unRfMgmtMsg.RfMgmtConfigStatusRsp.
                                  TpcSpectMgmtTable.u4VendorId, pRcvBuf);
                CAPWAP_GET_1BYTE (RfMgmtMsgStruct.
                                  unRfMgmtMsg.RfMgmtConfigStatusRsp.
                                  TpcSpectMgmtTable.u1RadioId, pRcvBuf);
                CAPWAP_GET_2BYTE (RfMgmtMsgStruct.
                                  unRfMgmtMsg.RfMgmtConfigStatusRsp.
                                  TpcSpectMgmtTable.u2TpcRequestInterval,
                                  pRcvBuf);
                CAPWAP_GET_1BYTE (RfMgmtMsgStruct.
                                  unRfMgmtMsg.RfMgmtConfigStatusRsp.
                                  TpcSpectMgmtTable.u111hTpcStatus, pRcvBuf);

                RfMgmtMsgStruct.unRfMgmtMsg.
                    RfMgmtConfigStatusRsp.u2WtpInternalId =
                    pCwMsg->u2IntProfileId;

                RfMgmtMsgStruct.unRfMgmtMsg.
                    RfMgmtConfigStatusRsp.TpcSpectMgmtTable.isOptional =
                    OSIX_TRUE;

                RfMgmtMsgStruct.u1Opcode = VENDOR_SPECT_MGMT_TPC_TYPE;

                if (WssIfProcessRfMgmtMsg (RFMGMT_CONFIG_STATUS_RSP,
                                           &RfMgmtMsgStruct) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "Configuration Status Request "
                                "failure: Vendor Spec Mgmt Vendor Type\r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Configuration Status Request "
                                  "failure: Vendor Spec Mgmt Vendor Type\r\n"));
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
                    return OSIX_SUCCESS;
                }
            }
#endif

        }
            break;
#ifdef ROGUEAP_WANTED
        case VENDOR_ROUGE_AP:
        {
#ifdef WTP_WANTED
            RfMgmtMsgStruct.unRfMgmtMsg.
                RfMgmtConfigStatusRsp.RougeTable.u2MsgEleType = elementType;

            CAPWAP_GET_2BYTE (RfMgmtMsgStruct.
                              unRfMgmtMsg.RfMgmtConfigStatusRsp.
                              RougeTable.u2MsgEleLen, pRcvBuf);
            CAPWAP_GET_4BYTE (RfMgmtMsgStruct.
                              unRfMgmtMsg.RfMgmtConfigStatusRsp.
                              RougeTable.u4VendorId, pRcvBuf);
            CAPWAP_GET_NBYTE (RfMgmtMsgStruct.
                              unRfMgmtMsg.RfMgmtConfigStatusRsp.
                              RougeTable.au1RfGroupName,
                              pRcvBuf, CAPWAP_RF_GROUP_NAME_SIZE);

            RfMgmtMsgStruct.unRfMgmtMsg.
                RfMgmtConfigStatusRsp.u2WtpInternalId = pCwMsg->u2IntProfileId;

            RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtConfigStatusRsp.
                RougeTable.u1RadioId = u1RadioId;

            RfMgmtMsgStruct.u1Opcode = VENDOR_ROUGE_AP;
            if (WssIfProcessRfMgmtMsg (RFMGMT_CONFIG_STATUS_RSP,
                                       &RfMgmtMsgStruct) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Configuration Status Request "
                            "failure: Vendor Spec Mgmt Vendor Type\r\n");
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
                return OSIX_FAILURE;
            }
#endif
        }
            break;
#endif
#if 0
        case VENDOR_SPECTRUM_MGMT_DFS_TYPE:
        {
#ifdef WLC_WANTED
            pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
            pWssIfCapwapDB->CapwapIsGetAllDB.bNoOfRadio = OSIX_TRUE;
            pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
                pCwMsg->u2IntProfileId;
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                         pWssIfCapwapDB) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Validation failure : Unable "
                            "to fetch the no of radios\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Validation failure : Unable "
                              "to fetch the no of radios\r\n"));
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
                return OSIX_SUCCESS;
            }
            for (u1RadioId = 1;
                 u1RadioId <=
                 pCwMsg->capwapMsgElm[u2MsgIndex].numMsgElemInstance;
                 u1RadioId++)
            {
                RfMgmtMsgStruct.unRfMgmtMsg.
                    RfMgmtConfigStatusReq.DfsParamsTable.u2MsgEleType =
                    elementType;

                CAPWAP_GET_2BYTE (RfMgmtMsgStruct.
                                  unRfMgmtMsg.RfMgmtConfigStatusReq.
                                  DfsParamsTable.u2MsgEleLen, pRcvBuf);
                CAPWAP_GET_4BYTE (RfMgmtMsgStruct.
                                  unRfMgmtMsg.RfMgmtConfigStatusReq.
                                  DfsParamsTable.u4VendorId, pRcvBuf);
                CAPWAP_GET_1BYTE (RfMgmtMsgStruct.
                                  unRfMgmtMsg.RfMgmtConfigStatusReq.
                                  DfsParamsTable.u1RadioId, pRcvBuf);
                CAPWAP_GET_2BYTE (RfMgmtMsgStruct.
                                  unRfMgmtMsg.RfMgmtConfigStatusReq.
                                  DfsParamsTable.u2DfsQuietInterval, pRcvBuf);
                CAPWAP_GET_2BYTE (RfMgmtMsgStruct.
                                  unRfMgmtMsg.RfMgmtConfigStatusReq.
                                  DfsParamsTable.u2DfsQuietPeriod, pRcvBuf);
                CAPWAP_GET_2BYTE (RfMgmtMsgStruct.
                                  unRfMgmtMsg.RfMgmtConfigStatusReq.
                                  DfsParamsTable.u2DfsMeasurementInterval,
                                  pRcvBuf);
                CAPWAP_GET_2BYTE (RfMgmtMsgStruct.
                                  unRfMgmtMsg.RfMgmtConfigStatusReq.
                                  DfsParamsTable.u2DfsChannelSwitchStatus,
                                  pRcvBuf);
                CAPWAP_GET_1BYTE (RfMgmtMsgStruct.
                                  unRfMgmtMsg.RfMgmtConfigStatusReq.
                                  DfsParamsTable.u111hDfsStatus, pRcvBuf);

                RfMgmtMsgStruct.unRfMgmtMsg.
                    RfMgmtConfigStatusReq.u2WtpInternalId =
                    pCwMsg->u2IntProfileId;

                RfMgmtMsgStruct.unRfMgmtMsg.
                    RfMgmtConfigStatusReq.DfsParamsTable.isOptional = OSIX_TRUE;

                RfMgmtMsgStruct.u1Opcode = VENDOR_SPECTRUM_MGMT_DFS_TYPE;
                if (WssIfProcessRfMgmtMsg (RFMGMT_VALIDATE_CONFIG_STATUS_REQ,
                                           &RfMgmtMsgStruct) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "Validation failure : Vendor Spect Mgmt DFS"
                                "Vendor Type in Config Status Request\r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Validation failure : Vendor Spect Mgmt DFS"
                                  "Vendor Type in Config Status Request\r\n"));
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
                    return OSIX_FAILURE;
                }
                if (WssIfProcessRfMgmtMsg (RFMGMT_CONFIG_STATUS_REQ,
                                           &RfMgmtMsgStruct) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Configuration Status"
                                "Request failure:Vendor Spect Mgmt Vendor DFS"
                                " Type\r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Configuration Status"
                                  "Request failure:Vendor Spect Mgmt Vendor DFS"
                                  " Type\r\n"));
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
                    return OSIX_FAILURE;
                }
                else
                {
                    if (RfMgmtMsgStruct.unRfMgmtMsg.
                        RfMgmtConfigStatusReq.DfsParamsTable.isOptional ==
                        OSIX_TRUE)
                    {
                        pConfStatRsp->
                            vendSpec[VENDOR_SPECTRUM_MGMT_DFS_TYPE].isOptional =
                            OSIX_TRUE;
                        pConfStatRsp->
                            vendSpec[VENDOR_SPECTRUM_MGMT_DFS_TYPE].u2MsgEleType
                            = VENDOR_SPECIFIC_PAYLOAD;
                        pConfStatRsp->
                            vendSpec[VENDOR_SPECTRUM_MGMT_DFS_TYPE].
                            u2MsgEleLen =
                            CAPWAP_MSG_ELEM_TYPE_LEN +
                            SPECT_MGMT_VENDOR_DFS_MSG_ELM_LEN;
                        pConfStatRsp->vendSpec[VENDOR_SPECTRUM_MGMT_DFS_TYPE].
                            elementId = VENDOR_SPECTRUM_MGMT_DFS_TYPE;
                        pConfStatRsp->vendSpec[VENDOR_SPECTRUM_MGMT_DFS_TYPE].
                            unVendorSpec.VendorDfsParams.isOptional = OSIX_TRUE;
                        pConfStatRsp->vendSpec[VENDOR_SPECTRUM_MGMT_DFS_TYPE].
                            unVendorSpec.VendorDfsParams.u2MsgEleType =
                            VENDOR_SPECTRUM_MGMT_DFS_MSG;
                        pConfStatRsp->vendSpec[VENDOR_SPECTRUM_MGMT_DFS_TYPE].
                            unVendorSpec.VendorDfsParams.u2MsgEleLen =
                            SPECT_MGMT_VENDOR_DFS_MSG_ELM_LEN;
                        pConfStatRsp->vendSpec[VENDOR_SPECTRUM_MGMT_DFS_TYPE].
                            unVendorSpec.VendorDfsParams.u4VendorId = VENDOR_ID;

                        /* To get radio if index */
                        pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;
                        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
                            RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtConfigStatusReq.
                            u2WtpInternalId;
                        pWssIfCapwapDB->CapwapGetDB.u1RadioId =
                            RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtConfigStatusReq.
                            DfsParamsTable.u1RadioId;
                        if (WssIfProcessCapwapDBMsg
                            (WSS_CAPWAP_GET_DB, pWssIfCapwapDB) != OSIX_SUCCESS)
                        {
                            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                        "CapwapSetVendSpecInfo: "
                                        "Getting Radio index failed\r\n");
                            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                          "CapwapSetVendSpecInfo: "
                                          "Getting Radio index failed\r\n"));
                            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                            UtlShMemFreeWssWlanBuf ((UINT1 *)
                                                    pWssWlanMsgStruct);
                            return OSIX_FAILURE;
                        }

                        /*To get DFS Configuration Parameters */
                        RfMgmtDB.unRfMgmtDB.
                            RfMgmtApConfigTable.RfMgmtAPConfigDB.
                            u4RadioIfIndex =
                            pWssIfCapwapDB->CapwapGetDB.u4IfIndex;

                        RfMgmtDB.unRfMgmtDB.
                            RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
                            bRfMgmt11hDfsQuietInterval = OSIX_TRUE;
                        RfMgmtDB.unRfMgmtDB.
                            RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
                            bRfMgmt11hDfsQuietPeriod = OSIX_TRUE;
                        RfMgmtDB.unRfMgmtDB.
                            RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
                            bRfMgmt11hDfsMeasurementInterval = OSIX_TRUE;
                        RfMgmtDB.unRfMgmtDB.
                            RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
                            bRfMgmt11hDfsChannelSwitchStatus = OSIX_TRUE;

                        if (WssIfProcessRfMgmtDBMsg
                            (RFMGMT_GET_AP_CONFIG_ENTRY,
                             &RfMgmtDB) != RFMGMT_SUCCESS)
                        {
                            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                        "Getting DFS Configuration Parameters"
                                        " failed\r\n");
                            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                          "Getting DFS Configuration Parameters"
                                          " failed\r\n"));
                            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                            UtlShMemFreeWssWlanBuf ((UINT1 *)
                                                    pWssWlanMsgStruct);
                            return OSIX_FAILURE;
                        }
                        pConfStatRsp->vendSpec
                            [VENDOR_SPECTRUM_MGMT_DFS_TYPE].unVendorSpec.
                            VendorDfsParams.VendorDfsParamsParams
                            [u1RadioId - 1].u1RadioId =
                            RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtConfigStatusReq.
                            DfsParamsTable.u1RadioId;
                        pConfStatRsp->vendSpec[VENDOR_SPECTRUM_MGMT_DFS_TYPE].
                            unVendorSpec.VendorDfsParams.
                            VendorDfsParamsParams[u1RadioId -
                                                  1].u2DfsQuietInterval =
                            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                            RfMgmtAPConfigDB.u2RfMgmt11hDfsQuietInterval;
                        pConfStatRsp->vendSpec[VENDOR_SPECTRUM_MGMT_DFS_TYPE].
                            unVendorSpec.VendorDfsParams.
                            VendorDfsParamsParams[u1RadioId -
                                                  1].u2DfsQuietPeriod =
                            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                            RfMgmtAPConfigDB.u2RfMgmt11hDfsQuietPeriod;
                        pConfStatRsp->vendSpec[VENDOR_SPECTRUM_MGMT_DFS_TYPE].
                            unVendorSpec.VendorDfsParams.
                            VendorDfsParamsParams[u1RadioId -
                                                  1].u2DfsMeasurementInterval =
                            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                            RfMgmtAPConfigDB.u2RfMgmt11hDfsMeasurementInterval;
                        pConfStatRsp->vendSpec[VENDOR_SPECTRUM_MGMT_DFS_TYPE].
                            unVendorSpec.VendorDfsParams.
                            VendorDfsParamsParams[u1RadioId -
                                                  1].u2DfsChannelSwitchStatus =
                            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                            RfMgmtAPConfigDB.u2RfMgmt11hDfsChannelSwitchStatus;

                        MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
                        RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
                            RfMgmtAutoRfProfileDB.
                            u4Dot11RadioType = RFMGMT_RADIO_TYPEA;
                        RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
                            RfMgmtAutoRfIsSetDB.bRfMgmt11hDfsStatus = OSIX_TRUE;

                        if (WssIfProcessRfMgmtDBMsg
                            (RFMGMT_GET_AUTO_RF_ENTRY,
                             &RfMgmtDB) != RFMGMT_SUCCESS)
                        {
                            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                        "Getting DFS Status failed\r\n");
                            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                          "Getting DFS Status failed\r\n"));
                            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                            UtlShMemFreeWssWlanBuf ((UINT1 *)
                                                    pWssWlanMsgStruct);
                            return OSIX_FAILURE;
                        }
                        pConfStatRsp->
                            vendSpec[VENDOR_SPECTRUM_MGMT_DFS_TYPE].
                            unVendorSpec.VendorDfsParams.
                            VendorDfsParamsParams[u1RadioId -
                                                  1].u111hDfsStatus =
                            RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
                            RfMgmtAutoRfProfileDB.u1RfMgmt11hDfsStatus;

                        pConfStatRsp->u2CapwapMsgElemenLen = (UINT2)
                            (pConfStatRsp->u2CapwapMsgElemenLen +
                             (pConfStatRsp->vendSpec
                              [VENDOR_SPECTRUM_MGMT_DFS_TYPE].u2MsgEleLen +
                              CAPWAP_MSG_ELEM_TYPE_LEN));
                    }
                }
            }
#endif
#ifdef WTP_WANTED
            for (u1RadioId = 1; u1RadioId <= u2NumRadios; u1RadioId++)
            {
                RfMgmtMsgStruct.unRfMgmtMsg.
                    RfMgmtConfigStatusRsp.DfsParamsTable.u2MsgEleType =
                    elementType;

                CAPWAP_GET_2BYTE (RfMgmtMsgStruct.
                                  unRfMgmtMsg.RfMgmtConfigStatusRsp.
                                  DfsParamsTable.u2MsgEleLen, pRcvBuf);
                CAPWAP_GET_4BYTE (RfMgmtMsgStruct.
                                  unRfMgmtMsg.RfMgmtConfigStatusRsp.
                                  DfsParamsTable.u4VendorId, pRcvBuf);
                CAPWAP_GET_1BYTE (RfMgmtMsgStruct.
                                  unRfMgmtMsg.RfMgmtConfigStatusRsp.
                                  DfsParamsTable.u1RadioId, pRcvBuf);
                CAPWAP_GET_2BYTE (RfMgmtMsgStruct.
                                  unRfMgmtMsg.RfMgmtConfigStatusRsp.
                                  DfsParamsTable.u2DfsQuietInterval, pRcvBuf);
                CAPWAP_GET_2BYTE (RfMgmtMsgStruct.
                                  unRfMgmtMsg.RfMgmtConfigStatusRsp.
                                  DfsParamsTable.u2DfsQuietPeriod, pRcvBuf);
                CAPWAP_GET_2BYTE (RfMgmtMsgStruct.
                                  unRfMgmtMsg.RfMgmtConfigStatusRsp.
                                  DfsParamsTable.u2DfsMeasurementInterval,
                                  pRcvBuf);
                CAPWAP_GET_2BYTE (RfMgmtMsgStruct.
                                  unRfMgmtMsg.RfMgmtConfigStatusRsp.
                                  DfsParamsTable.u2DfsChannelSwitchStatus,
                                  pRcvBuf);
                CAPWAP_GET_1BYTE (RfMgmtMsgStruct.
                                  unRfMgmtMsg.RfMgmtConfigStatusRsp.
                                  DfsParamsTable.u111hDfsStatus, pRcvBuf);

                RfMgmtMsgStruct.unRfMgmtMsg.
                    RfMgmtConfigStatusRsp.u2WtpInternalId =
                    pCwMsg->u2IntProfileId;

                RfMgmtMsgStruct.unRfMgmtMsg.
                    RfMgmtConfigStatusRsp.DfsParamsTable.isOptional = OSIX_TRUE;

                RfMgmtMsgStruct.u1Opcode = VENDOR_SPECTRUM_MGMT_DFS_TYPE;

                if (WssIfProcessRfMgmtMsg (RFMGMT_CONFIG_STATUS_RSP,
                                           &RfMgmtMsgStruct) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "Configuration Status Request "
                                "failure: Vendor Spec Mgmt Vendor Type\r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Configuration Status Request "
                                  "failure: Vendor Spec Mgmt Vendor Type\r\n"));
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
                    return OSIX_SUCCESS;
                }
            }
#endif

        }
            break;

#endif
        case VENDOR_DFS_CHANNEL_STATS:
        {
#ifdef WLC_WANTED
            RfMgmtMsgStruct.unRfMgmtMsg.
                RfMgmtConfigStatusReq.DFSChannelTable.u2MsgEleType =
                elementType;

            CAPWAP_GET_2BYTE (RfMgmtMsgStruct.
                              unRfMgmtMsg.RfMgmtConfigStatusReq.
                              DFSChannelTable.u2MsgEleLen, pRcvBuf);
            CAPWAP_GET_4BYTE (RfMgmtMsgStruct.
                              unRfMgmtMsg.RfMgmtConfigStatusReq.
                              DFSChannelTable.u4VendorId, pRcvBuf);
            CAPWAP_GET_1BYTE (RfMgmtMsgStruct.
                              unRfMgmtMsg.RfMgmtConfigStatusReq.
                              DFSChannelTable.u1RadioId, pRcvBuf);
            while (u4Idx < RADIO_MAX_DFS_CHANNEL)
            {
                CAPWAP_GET_4BYTE (RfMgmtMsgStruct.unRfMgmtMsg.
                                  RfMgmtConfigStatusReq.DFSChannelTable.
                                  DFSChStats[u4Idx].u4ChannelNum, pRcvBuf);
                CAPWAP_GET_4BYTE (RfMgmtMsgStruct.unRfMgmtMsg.
                                  RfMgmtConfigStatusReq.DFSChannelTable.
                                  DFSChStats[u4Idx].u4DFSFlag, pRcvBuf);
                u4Idx++;

            }
            RfMgmtMsgStruct.unRfMgmtMsg.
                RfMgmtConfigStatusReq.u2WtpInternalId = pCwMsg->u2IntProfileId;

            RfMgmtMsgStruct.unRfMgmtMsg.
                RfMgmtConfigStatusReq.DFSChannelTable.isOptional = OSIX_TRUE;

            RfMgmtMsgStruct.u1Opcode = VENDOR_DFS_CHANNEL_STATS;

            if (WssIfProcessRfMgmtMsg (RFMGMT_DFS_INFO_UPDATE,
                                       &RfMgmtMsgStruct) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Configuration Status"
                            "Request failure:DFS Vendor Type\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Configuration Status"
                              "Request failure:DFS Vendor Type\r\n"));
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
                return OSIX_FAILURE;
            }
#endif
        }
            break;
#endif
        case VENDOR_DOT11N_CONFIG_TYPE:
        {
            VendorSpecPayload.unVendorSpec.VendorDot11nCfg.u2MsgEleType =
                VendorSpecPayload.elementId;

            CAPWAP_GET_2BYTE (VendorSpecPayload.unVendorSpec.VendorDot11nCfg.
                              u2MsgEleLen, pRcvBuf);
            CAPWAP_GET_4BYTE (VendorSpecPayload.unVendorSpec.VendorDot11nCfg.
                              u4VendorId, pRcvBuf);
            CAPWAP_GET_1BYTE (VendorSpecPayload.unVendorSpec.VendorDot11nCfg.
                              u1RadioId, pRcvBuf);
            CAPWAP_GET_1BYTE (VendorSpecPayload.unVendorSpec.VendorDot11nCfg.
                              u1AMPDUStatus, pRcvBuf);
            CAPWAP_GET_1BYTE (VendorSpecPayload.unVendorSpec.VendorDot11nCfg.
                              u1AMPDUSubFrame, pRcvBuf);
            CAPWAP_GET_2BYTE (VendorSpecPayload.unVendorSpec.VendorDot11nCfg.
                              u2AMPDULimit, pRcvBuf);
            CAPWAP_GET_1BYTE (VendorSpecPayload.unVendorSpec.VendorDot11nCfg.
                              u1AMSDUStatus, pRcvBuf);
            CAPWAP_GET_2BYTE (VendorSpecPayload.unVendorSpec.VendorDot11nCfg.
                              u2AMSDULimit, pRcvBuf);
            RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusRsp.
                RadioIfDot11nCfg.u2MessageType =
                VendorSpecPayload.unVendorSpec.VendorDot11nCfg.u2MsgEleType;
            RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusRsp.
                RadioIfDot11nCfg.u2MessageLength =
                VendorSpecPayload.unVendorSpec.VendorDot11nCfg.u2MsgEleLen;
            RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusRsp.
                RadioIfDot11nCfg.u4VendorId =
                VendorSpecPayload.unVendorSpec.VendorDot11nCfg.u4VendorId;
            RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusRsp.
                RadioIfDot11nCfg.u1RadioId =
                VendorSpecPayload.unVendorSpec.VendorDot11nCfg.u1RadioId;
            RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusRsp.
                RadioIfDot11nCfg.u1AMPDUStatus =
                VendorSpecPayload.unVendorSpec.VendorDot11nCfg.u1AMPDUStatus;
            RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusRsp.
                RadioIfDot11nCfg.u1AMPDUSubFrame =
                VendorSpecPayload.unVendorSpec.VendorDot11nCfg.u1AMPDUSubFrame;
            RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusRsp.
                RadioIfDot11nCfg.u2AMPDULimit =
                VendorSpecPayload.unVendorSpec.VendorDot11nCfg.u2AMPDULimit;
            RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusRsp.
                RadioIfDot11nCfg.u1AMSDUStatus =
                VendorSpecPayload.unVendorSpec.VendorDot11nCfg.u1AMSDUStatus;
            RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusRsp.
                RadioIfDot11nCfg.u2AMSDULimit =
                VendorSpecPayload.unVendorSpec.VendorDot11nCfg.u2AMSDULimit;
            RadioIfMsgStruct.unRadioIfMsg.RadioIfConfigStatusRsp.
                RadioIfDot11nCfg.isPresent = OSIX_TRUE;

            if (WssIfProcessRadioIfMsg (WSS_RADIOIF_CONFIG_STATUS_RSP,
                                        &RadioIfMsgStruct) != OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "Processing DOT11N Config message failed \r\n");
            }
            break;
        }

        default:
            break;
    }

    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapValidateConfigStatusRequestMsgElems         */
/*  Description     : The function validates the functional             */
/*                    characteristics of the received CAPWAP            */
/*                    Join request agains the configured                */
/*                    WTP profile                                       */
/*  Input(s)        : discReqMsg - parsed CAPWAP Discovery request      */
/*                    pRcvBuf - Received discovery packet               */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
CapwapValidateConfigStatusRequestMsgElems (UINT1 *pRcvBuf,
                                           tCapwapControlPacket *
                                           pConfigStatusReqMsg,
                                           tConfigStatusRsp * pConfStatRsp)
{
    tACName             acName;
    tRadioIfAdminStatus radAdminState;
    tStatisticsTimer    statsTimer;
    tWtpRebootStatsElement rebootStats;
    UINT2               u2NumOptionalMsg = 0, u2MsgType = 0;
    UINT1               u1Index = 0;
    tRadioIfMsgStruct   RadioIfMsgStruct;
    UINT2               u2MsgBaseIndex = 0;

    CAPWAP_FN_ENTRY ();
    MEMSET (&RadioIfMsgStruct, 0, sizeof (tRadioIfMsgStruct));
    MEMSET (&acName, 0, sizeof (tACName));
    MEMSET (&radAdminState, 0, sizeof (tRadioIfAdminStatus));
    MEMSET (&statsTimer, 0, sizeof (tStatisticsTimer));
    MEMSET (&rebootStats, 0, sizeof (tWtpRebootStatsElement));

    u2NumOptionalMsg = pConfigStatusReqMsg->numOptionalElements;
    /* Validate the mandatory message elements */
    if ((CapwapValidateAcName (pRcvBuf, pConfigStatusReqMsg,
                               AC_NAME_CONF_STATUS_REQ_INDEX) == OSIX_FAILURE)
        &&
        (CapwapValidateRadioAdminState
         (pRcvBuf, pConfigStatusReqMsg, &radAdminState,
          RADIO_ADMIN_STATE_CONF_STATUS_REQ_INDEX) == OSIX_FAILURE)
        &&
        (CapwapValidateStatsTimer
         (pRcvBuf, pConfigStatusReqMsg,
          STATS_TIMER_CONF_STATUS_REQ_INDEX) == OSIX_FAILURE))
#if CAP_WANTED
        (CapwapValidateRebootStats (pRcvBuf, pConfigStatusReqMsg,
                                    WTP_REBOOT_STATS_CONF_STATUS_REQ_INDEX) ==
         OSIX_FAILURE)
#endif
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Validated the received"
                    "Config status request message elements \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to Validated the received"
                      "Config status request message elements \r\n"));
        return OSIX_FAILURE;
    }

    /*Multidomain Information gets appended when we restart the AP alone. So resetting the
       radio DB */
    RadioIfMsgStruct.unRadioIfMsg.
        RadioIfConfigStatusReq.u2WtpInternalId =
        pConfigStatusReqMsg->u2IntProfileId;
    if (WssIfProcessRadioIfMsg (WSS_RADIOIF_RADIODB_RESET, &RadioIfMsgStruct) !=
        OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Reset the Multi"
                    "Domain Capability in Radio IF \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to Reset the Multi"
                      "Domain Capability in Radio IF \r\n"));
        return OSIX_FAILURE;
    }

    /* Validate IEEE radio optional elements */
    for (u1Index = 0; u1Index < u2NumOptionalMsg; u1Index++)
    {
        u2MsgBaseIndex =
            (UINT2) (MAX_CONF_STATUS_REQ_MAND_MSG_ELEMENTS + u1Index);
        /* Get the message element type */
        u2MsgType =
            pConfigStatusReqMsg->capwapMsgElm
            [MAX_CONF_STATUS_REQ_MAND_MSG_ELEMENTS + u1Index].u2MsgType;
        switch (u2MsgType)
        {
            case IEEE_ANTENNA:
                if (CapwapSetIeeeAntna (pRcvBuf, pConfigStatusReqMsg,
                                        pConfStatRsp,
                                        u2MsgBaseIndex) == OSIX_FAILURE)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Set"
                                "IEEE Antenna \r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Failed to Set" "IEEE Antenna \r\n"));
                    return OSIX_FAILURE;
                }
                break;
            case IEEE_DIRECT_SEQUENCE_CONTROL:
                if (CapwapSetIeeeDSC (pRcvBuf, pConfigStatusReqMsg,
                                      pConfStatRsp,
                                      u2MsgBaseIndex) == OSIX_FAILURE)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Set"
                                "Direct Sequnece control \r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Failed to Set"
                                  "Direct Sequnece control \r\n"));
                    return OSIX_FAILURE;
                }
                break;
            case IEEE_INFORMATION_ELEMENT:
                if (CapwapSetIeeeInfoElem (pRcvBuf, pConfigStatusReqMsg,
                                           pConfStatRsp,
                                           u2MsgBaseIndex) == OSIX_FAILURE)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Set "
                                "Information Element \r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Failed to Set " "Information Element \r\n"));
                    return OSIX_FAILURE;
                }
                break;
            case IEEE_MAC_OPERATION:
                if (CapwapSetMacOperation (pRcvBuf, pConfigStatusReqMsg,
                                           pConfStatRsp,
                                           u2MsgBaseIndex) == OSIX_FAILURE)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Set"
                                "Mac Operation \r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Failed to Set" "Mac Operation \r\n"));
                    return OSIX_FAILURE;
                }
                break;
            case IEEE_MULTIDOMAIN_CAPABILITY:
                if (CapwapSetIeeeMultiDomainCapability (pRcvBuf,
                                                        pConfigStatusReqMsg,
                                                        pConfStatRsp,
                                                        u2MsgBaseIndex)
                    == OSIX_FAILURE)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Faild to set Mac"
                                "Operation \r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Faild to set Mac" "Operation \r\n"));
                    return OSIX_FAILURE;
                }
                break;
            case IEEE_OFDM_CONTROL:
                if (CapwapSetIeeeOFDMControl (pRcvBuf, pConfigStatusReqMsg,
                                              pConfStatRsp,
                                              u2MsgBaseIndex) == OSIX_FAILURE)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Set"
                                "OFDM Control \r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Failed to Set" "OFDM Control \r\n"));

                    return OSIX_FAILURE;
                }
                break;
            case IEEE_SUPPORTED_RATES:
                if (CapwapSetIeeeSupportedRates (pRcvBuf, pConfigStatusReqMsg,
                                                 pConfStatRsp,
                                                 u2MsgBaseIndex)
                    == OSIX_FAILURE)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Set"
                                "IEEE Supported Rates \r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Failed to Set" "IEEE Supported Rates \r\n"));
                    return OSIX_FAILURE;
                }
                break;
            case IEEE_TX_POWER:
                if (CapwapSetIeeeTxPower (pRcvBuf, pConfigStatusReqMsg,
                                          pConfStatRsp,
                                          u2MsgBaseIndex) == OSIX_FAILURE)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Set"
                                "Tx Power \r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Failed to Set" "Tx Power \r\n"));
                    return OSIX_FAILURE;

                }
                break;
            case IEEE_TX_POWERLEVEL:
                if (CapwapSetIeeeTxPowerLevel (pRcvBuf, pConfigStatusReqMsg,
                                               pConfStatRsp,
                                               u2MsgBaseIndex) == OSIX_FAILURE)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to set"
                                "IEEE Tx Power Level \r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Failed to set" "IEEE Tx Power Level \r\n"));
                    return OSIX_FAILURE;
                }
                break;
            case IEEE_WTP_RADIO_CONFIGURATION:
                if (CapwapSetIeeeWtpRadioConfig (pRcvBuf, pConfigStatusReqMsg,
                                                 pConfStatRsp,
                                                 u2MsgBaseIndex)
                    == OSIX_FAILURE)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to set"
                                "Wtp Radio configuration \r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Failed to set"
                                  "Wtp Radio configuration \r\n"));
                    return OSIX_FAILURE;
                }
                break;
            case WTP_RADIO_INFO:
                if (CapwapSetIeeeRadioInfo (pRcvBuf, pConfigStatusReqMsg,
                                            pConfStatRsp,
                                            u2MsgBaseIndex) == OSIX_FAILURE)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Faild to set"
                                "IEEE Radio information \r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Faild to set"
                                  "IEEE Radio information \r\n"));
                    return OSIX_FAILURE;
                }
                break;
            case VENDOR_SPECIFIC_PAYLOAD:
                if (CapwapSetVendSpecInfo (pRcvBuf, pConfigStatusReqMsg,
                                           pConfStatRsp,
                                           u2MsgBaseIndex) == OSIX_FAILURE)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Faild to set"
                                "VENDOR specific info \r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Faild to set" "VENDOR specific info \r\n"));
                    /*For Vendor Specific Payload there is no need to return Failure */
                    return OSIX_SUCCESS;
                }
                break;
            default:
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Unknown Message Element \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Unknown Message Element \r\n"));
                return OSIX_FAILURE;
                break;

        }
    }

    pConfStatRsp->RadioIfQos.isPresent = OSIX_TRUE;
    if (CapwapGetIeeeWtpQos (&pConfStatRsp->RadioIfQos,
                             &pConfStatRsp->u2CapwapMsgElemenLen,
                             pConfigStatusReqMsg->u2IntProfileId)
        == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Get Radio WTP QoS \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to Get Radio WTP QoS \r\n"));
        return OSIX_FAILURE;
    }

    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapProcessConfigStatusResponse                 */
/*  Description     : The function processes the received CAPWAP        */
/*                    join response. This function handling is mainly   */
/*                    done in the AP and it validates the received join */
/*                    response and updates WSS-AP.                      */
/*  Input(s)        : JoinRespMsg- Join response packet received        */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
CapwapProcessConfigStatusResponse (tSegment * pBuf,
                                   tCapwapControlPacket *
                                   pConfStatusRespMsg,
                                   tRemoteSessionManager * pSessEntry)
{
    UINT1              *pRcvBuf = NULL, *pu1TxBuf = NULL;
    INT4                i4Status = 1;
    tChangeStateEvtReq  StateEvntReq;
    tSegment           *pTxBuf = NULL;
    UINT1               u1IsNotLinear = OSIX_FALSE;
    UINT2               u2PacketLen = 0;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT1              *pu1TempBuf = NULL;
#ifdef WTP_WANTED
    tWssIfPMDB          WssIfPMDB;
#endif

    CAPWAP_FN_ENTRY ();

    if (pSessEntry == NULL || pSessEntry->eCurrentState != CAPWAP_CONFIGURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "CAPWAP Session Entry is"
                    "NULL, Retrun NULL \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "CAPWAP Session Entry is" "NULL, Retrun NULL \r\n"));
        return OSIX_FAILURE;
    }
    /* Kloc Fix Start */
    MEMSET (&StateEvntReq, 0, sizeof (tChangeStateEvtReq));
    /* Kloc Fix Ends */

    /* stop the retransmit interval timer
     * and clear the packet buffer pointer */
    if (CapwapTmrStop (pSessEntry, CAPWAP_RETRANSMIT_INTERVAL_TMR) ==
        OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_MGMT_TRC, "Stopped the retransmit timer \r\n");
        SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4CapwapSysLogId,
                      "Stopped the retransmit timer \r\n"));
        /* clear the packet buffer pointer */
#ifdef WLC_WANTED
        WlchdlrFreeTransmittedPacket (pSessEntry);
#else
        AphdlrFreeTransmittedPacket (pSessEntry);
#endif
    }

    u2PacketLen = (UINT2) CAPWAP_GET_BUF_LEN (pBuf);
    pRcvBuf = CAPWAP_BUF_IF_LINEAR (pBuf, 0, u2PacketLen);
    if (pRcvBuf == NULL)
    {
        pRcvBuf = (UINT1 *) MemAllocMemBlk (CAPWAP_PKTBUF_POOLID);
        /* Kloc Fix Start */
        if (pRcvBuf == NULL)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, " Memory Error \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          " Memory Error \r\n"));
            return OSIX_FAILURE;
        }
        /* Kloc Fix Ends */
        CAPWAP_COPY_FROM_BUF (pBuf, pRcvBuf, 0, u2PacketLen);
        u1IsNotLinear = OSIX_TRUE;
    }

    /* Validate the Conf Status response message.
       if it is success construct the State Event Request */
    if ((CapwapValidateConfigStatusResponseMsgElems
         (pRcvBuf, pConfStatusRespMsg, pSessEntry) == OSIX_SUCCESS))
    {

        MEMSET (&StateEvntReq, 0, sizeof (tChangeStateEvtReq));

        /* As Validtion is successful set the result code to SUCCESS */
        StateEvntReq.resultCode.u4Value = CAPWAP_SUCCESS;
        StateEvntReq.resultCode.u2MsgEleType = RESULT_CODE;
        StateEvntReq.resultCode.u2MsgEleLen = RESULT_CODE_LEN;
        StateEvntReq.u2CapwapMsgElemenLen = (UINT2)
            (StateEvntReq.u2CapwapMsgElemenLen +
             (StateEvntReq.resultCode.u2MsgEleLen + CAPWAP_MSG_ELEM_TYPE_LEN));
#ifdef WTP_WANTED
        MEMSET (&WssIfPMDB, 0, sizeof (tWssIfPMDB));
        WssIfPMDB.u2ProfileId = pConfStatusRespMsg->u2IntProfileId;
        if (WssIfProcessPMDBMsg
            (WSS_PM_WTP_EVT_VSP_CAPWAP_STATS_GET, &WssIfPMDB) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to update Join Req Stats to PM \n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to update Join Req Stats to PM \n"));

        }

        WssIfPMDB.wtpVSPCPWPWtpStats.cfgRespRxd++;
        if (WssIfProcessPMDBMsg
            (WSS_PM_WTP_EVT_VSP_CAPWAP_STATS_SET, &WssIfPMDB) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "Failed to update Join Req Stats to PM \n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to update Join Req Stats to PM \n"));
        }
#endif

    }
    else
    {
        MEMSET (&StateEvntReq, 0, sizeof (tChangeStateEvtReq));
        /* As Validtion is not successful set the result code to FAILURE */
        StateEvntReq.resultCode.u4Value = CAPWAP_FAILURE;
        StateEvntReq.resultCode.u2MsgEleType = RESULT_CODE;
        StateEvntReq.resultCode.u2MsgEleLen = RESULT_CODE_LEN;
        StateEvntReq.u2CapwapMsgElemenLen = (UINT2)
            (StateEvntReq.u2CapwapMsgElemenLen +
             (StateEvntReq.resultCode.u2MsgEleLen + CAPWAP_MSG_ELEM_TYPE_LEN));

    }

    if (u1IsNotLinear == OSIX_TRUE)
    {
        MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pRcvBuf);
    }
    /* Update the Remote session state to CAPWAP_CONFIGURE */
    CAPWAP_TRC (CAPWAP_FSM_TRC, "Entered CAPWAP DATACHECK State \r\n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                  "Entered CAPWAP DATACHECK State \r\n"));
    pSessEntry->eCurrentState = CAPWAP_DATACHECK;

    /* get message elements to construct the config status response */
    if (CapwapGetStateEventRequestMsgElements (&StateEvntReq, pSessEntry) ==
        OSIX_SUCCESS)
    {
        /* update the seq number */
        StateEvntReq.u1SeqNum = (UINT1) (pSessEntry->lastTransmitedSeqNum + 1);
        pTxBuf = CAPWAP_ALLOCATE_CRU_BUF (CAPWAP_MAX_PKT_LEN, 0);
        if (pTxBuf == NULL)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "capwapProcessConfigStatusResponse:"
                        "Memory Allocation Failed \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "capwapProcessConfigStatusResponse:"
                          "Memory Allocation Failed \r\n"));
            return OSIX_FAILURE;
        }
        pu1TxBuf = CAPWAP_BUF_IF_LINEAR (pTxBuf, 0, CAPWAP_MAX_PKT_LEN);
        if (pu1TxBuf == NULL)
        {
            pu1TxBuf = (UINT1 *) MemAllocMemBlk (CAPWAP_PKTBUF_POOLID);
            if (pu1TxBuf == NULL)
            {
                CAPWAP_RELEASE_CRU_BUF (pTxBuf);
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "capwapProcessConfigStatus"
                            "Response:Memory Allocation Failed \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "capwapProcessConfigStatus"
                              "Response:Memory Allocation Failed \r\n"));
                return OSIX_FAILURE;
            }
            CAPWAP_COPY_FROM_BUF (pTxBuf, pu1TxBuf, 0, CAPWAP_MAX_PKT_LEN);
            u1IsNotLinear = OSIX_TRUE;
        }
        if ((CapwapAssembleChangeStateEventRequest
             (StateEvntReq.u2CapwapMsgElemenLen, &StateEvntReq,
              pu1TxBuf)) == OSIX_FAILURE)
        {
            if (u1IsNotLinear == OSIX_TRUE)
            {
                MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pu1TxBuf);
            }
            CAPWAP_RELEASE_CRU_BUF (pTxBuf);
            return OSIX_FAILURE;
        }

        pu1TempBuf = pu1TxBuf + CAPWAP_MSG_OFFSET;
        CAPWAP_GET_2BYTE (StateEvntReq.u2CapwapMsgElemenLen, pu1TempBuf);
        if (u1IsNotLinear == OSIX_TRUE)
        {
            /* If the CRU buffer memory is not linear */
            CAPWAP_COPY_TO_BUF (pTxBuf, pu1TxBuf, 0, CAPWAP_MAX_PKT_LEN);
            MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pu1TxBuf);
        }
        i4Status = CapwapTxMessage (pTxBuf,
                                    pSessEntry,
                                    (UINT4) (StateEvntReq.u2CapwapMsgElemenLen +
                                             CAPWAP_MSG_ELE_LEN),
                                    WSS_CAPWAP_802_11_CTRL_PKT);
        if (i4Status == OSIX_SUCCESS)
        {
            /* update the seq number */
            pSessEntry->lastTransmitedSeqNum++;

            /* store the packet buffer pointer in 
             * the remote session DB copy the pointer */
            pSessEntry->lastTransmittedPkt = pTxBuf;
            pSessEntry->lastTransmittedPktLen =
                (UINT4) (StateEvntReq.u2CapwapMsgElemenLen +
                         CAPWAP_MSG_ELE_LEN);
/* below line was added for config status request looping issue in dakota board*/
            pSessEntry->u1RetransmitCount = 0;

            /*Start the Retransmit timer */
            CapwapTmrStart (pSessEntry, CAPWAP_RETRANSMIT_INTERVAL_TMR,
                            MAX_RETRANSMIT_INTERVAL_TIMEOUT);
            return OSIX_SUCCESS;
        }
        else
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "capwapProcessConfigStatusResponse:Failed to"
                        "Transmit the packet \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "capwapProcessConfigStatusResponse:Failed to"
                          "Transmit the packet \r\n"));

            CAPWAP_RELEASE_CRU_BUF (pTxBuf);
            pSessEntry->lastTransmittedPkt = NULL;
            pSessEntry->lastTransmittedPktLen = 0;
            if (u1IsNotLinear == OSIX_TRUE)
            {
                MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pu1TxBuf);
            }
            return OSIX_FAILURE;
        }
    }
    else
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to validate the Configuration status response \n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Failed to validate the Configuration status response \n"));

        CAPWAP_TRC (CAPWAP_FSM_TRC, "Enter the CAPWAP DTLS TD State \r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                      "Enter the CAPWAP DTLS TD State \r\n"));

        pSessEntry->eCurrentState = CAPWAP_DTLSTD;
        if (CapwapShutDownDTLS (pSessEntry) == OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Disconnect"
                        "the Session \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Failed to Disconnect" "the Session \r\n"));

        }

        WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
            MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

        /* Cleare the Entry in Remote session Module */
        pWssIfCapwapDB->u4DestIp = pSessEntry->remoteIpAddr.u4_addr[0];
        pWssIfCapwapDB->u4DestPort = pSessEntry->u4RemoteCtrlPort;
        /* coverity fix begin */
        /*WssIfProcessCapwapDBMsg (WSS_CAPWAP_DELETE_RSM, pWssIfCapwapDB); */
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_DELETE_RSM, pWssIfCapwapDB) !=
            OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "WssIfProcessCapwapDBMsg"
                        "failed  \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "WssIfProcessCapwapDBMsg" "failed  \r\n"));
        }
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);

        /* coverity fix end */
#ifdef WTP_WANTED
        pSessEntry->eCurrentState = CAPWAP_IDLE;
        CapwapSendRestartDiscEvent ();
#endif
        /*if ( u1IsNotLinear == OSIX_TRUE)
           {
           MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pu1TxBuf);
           }
           CAPWAP_RELEASE_CRU_BUF (pTxBuf); */
        return OSIX_FAILURE;
    }
    /*if ( u1IsNotLinear == OSIX_TRUE)
       {
       MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pu1TxBuf);
       }
       CAPWAP_RELEASE_CRU_BUF (pTxBuf); */
    return (i4Status);
}

/************************************************************************/
/*  Function Name   : CapwapGetStateEventRequestMsgElements            */
/*  Description     : The function validates the functional             */
/*                    characteristics of the received CAPWAP            */
/*                    Config Status response against the configured     */
/*                    WTP profile                                       */
/*  Input(s)        : discReqMsg - parsed CAPWAP Discovery request      */
/*                    pRcvBuf - Received discovery packet               */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
CapwapGetStateEventRequestMsgElements (tChangeStateEvtReq * pStateEvntReq,
                                       tRemoteSessionManager * pSessEntry)
{
    UINT4               u4MsgLen = 0;
    UNUSED_PARAM (pSessEntry);

    CAPWAP_FN_ENTRY ();
    /* updated the capwap header */
    if (CapwapConstructCpHeader (&pStateEvntReq->capwapHdr) == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapGetStateEventRequestMsgElements:Failed to"
                    "construct the capwap Header\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "CapwapGetStateEventRequestMsgElements:Failed to"
                      "construct the capwap Header\n"));
        return OSIX_FAILURE;
    }

    /* updated Mandatory Message elements */

    pStateEvntReq->u2CapwapMsgElemenLen = (UINT2)
        (pStateEvntReq->u2CapwapMsgElemenLen +
         (u4MsgLen + CAPWAP_CMN_MSG_ELM_HEAD_LEN));
    pStateEvntReq->u1NumMsgBlocks = 0;    /* flags always zero */
    CAPWAP_FN_EXIT ();
    return CAPWAP_SUCCESS;

}

/************************************************************************/
/*  Function Name   : CapwapValidateConfigStatusResponseMsgElems        */
/*  Description     : The function validates the functional             */
/*                    characteristics of the received CAPWAP            */
/*                    Config Status response against the configured     */
/*                    WTP profile                                       */
/*  Input(s)        : discReqMsg - parsed CAPWAP Discovery request      */
/*                    pRcvBuf - Received discovery packet               */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
CapwapValidateConfigStatusResponseMsgElems (UINT1 *pRcvBuf,
                                            tCapwapControlPacket
                                            * pConfigStatusRespMsg,
                                            tRemoteSessionManager * pSessEntry)
{

    tCapwapTimer        capwapTimer;
    tDecryptErrReportPeriod decryErrPeriod;
    tIdleTimeout        idleTimeout;
    tWtpFallback        wtpFallback;
    tConfigStatusRsp   *pConfStatRsp = NULL;
    UINT2               u2NumOptionalMsg = 0, u2MsgType = 0;
    UINT2               u2OptionalElementIndex =
        (MAX_CONF_STATUS_RES_MAND_MSG_ELEMENTS);
    UINT1               u1Index = 0;

    UNUSED_PARAM (pSessEntry);
    CAPWAP_FN_ENTRY ();

    pConfStatRsp =
        (tConfigStatusRsp *) (VOID *) UtlShMemAllocConfigStatusRspBuf ();
    if (pConfStatRsp == NULL)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapValidateConfigStatusResponseMsgElems:- "
                    "UtlShMemAllocConfigStatusRspBuf returned failure\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "CapwapValidateConfigStatusResponseMsgElems:- "
                      "UtlShMemAllocConfigStatusRspBuf returned failure\n"));
        return OSIX_FAILURE;
    }

    MEMSET (&capwapTimer, 0, sizeof (tCapwapTimer));
    MEMSET (&decryErrPeriod, 0, sizeof (tDecryptErrReportPeriod));
    MEMSET (&idleTimeout, 0, sizeof (tIdleTimeout));
    MEMSET (&wtpFallback, 0, sizeof (tWtpFallback));
    MEMSET (pConfStatRsp, 0, sizeof (tConfigStatusRsp));

    u2NumOptionalMsg = pConfigStatusRespMsg->numOptionalElements;
    /* Validate the mandatory message elements */
    if ((CapwapValidateCapwapTimer (pRcvBuf, pConfigStatusRespMsg,
                                    CAPWAP_TIMERS_CONF_STATUS_RESP_INDEX) ==
         OSIX_SUCCESS)
        &&
        (CapwapValidateDecryptErrReportPeriod
         (pRcvBuf, pConfigStatusRespMsg,
          DECRYPT_ERR_REPORT_PERIOD_CONF_STATUS_RESP_INDEX) == OSIX_SUCCESS)
        &&
        (CapwapValidateIdleTimeout
         (pRcvBuf, pConfigStatusRespMsg,
          IDLE_TIMEOUT_CONF_STATUS_RESP_INDEX) == OSIX_SUCCESS)
        &&
        (CapwapValidateWtpFallback
         (pRcvBuf, pConfigStatusRespMsg,
          WTP_FALLBACK_CONF_STATUS_RESP_INDEX) == OSIX_SUCCESS)
        &&
        (CapwapValidateIpv4List
         (pRcvBuf, pConfigStatusRespMsg,
          AC_IPV4_LIST_CONF_STATUS_RESP_INDEX) == OSIX_SUCCESS))
    {
        /*return OSIX_SUCCESS; */
    }
    /* Validate optional elements */

    /****************************TBD ***************************/
    /* Validate IEEE radio optional elements */
    for (u1Index = 0; u1Index < u2NumOptionalMsg; u1Index++)
    {
        /* Get the message element type */
        u2MsgType = pConfigStatusRespMsg->capwapMsgElm[u2OptionalElementIndex +
                                                       u1Index].u2MsgType;

        switch (u2MsgType)
        {
            case AC_IPv4_LIST:
                if (CapwapValidateIpv4List (pRcvBuf, pConfigStatusRespMsg,
                                            (UINT2) (u2OptionalElementIndex +
                                                     u1Index)) == OSIX_FAILURE)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Validate"
                                "AC IPv4 List \r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Failed to Validate" "AC IPv4 List \r\n"));
                    UtlShMemFreeConfigStatusRspBuf ((UINT1 *) pConfStatRsp);
                    return OSIX_FAILURE;
                }
                break;
            case AC_NAME_WITH_PRIO:
                if (CapwapValidateAcNamePriority (pRcvBuf, pConfigStatusRespMsg,
                                                  (UINT2)
                                                  (u2OptionalElementIndex +
                                                   u1Index)) == OSIX_FAILURE)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Validate"
                                "AC Name With Prio \r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Failed to Validate"
                                  "AC Name With Prio \r\n"));
                    UtlShMemFreeConfigStatusRspBuf ((UINT1 *) pConfStatRsp);
                    return OSIX_FAILURE;
                }
                break;
            case IEEE_ANTENNA:
                if (CapwapSetIeeeAntna (pRcvBuf, pConfigStatusRespMsg,
                                        pConfStatRsp,
                                        (UINT2) (u2OptionalElementIndex +
                                                 u1Index)) == OSIX_FAILURE)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Set"
                                "IEEE Antenna \r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Failed to Set" "IEEE Antenna \r\n"));
                    UtlShMemFreeConfigStatusRspBuf ((UINT1 *) pConfStatRsp);
                    return OSIX_FAILURE;
                }
                break;
            case IEEE_DIRECT_SEQUENCE_CONTROL:
                if (CapwapSetIeeeDSC (pRcvBuf, pConfigStatusRespMsg,
                                      pConfStatRsp,
                                      (UINT2) (u2OptionalElementIndex +
                                               u1Index)) == OSIX_FAILURE)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Set Direct"
                                "Sequnece control \r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Failed to Set Direct"
                                  "Sequnece control \r\n"));
                    UtlShMemFreeConfigStatusRspBuf ((UINT1 *) pConfStatRsp);
                    return OSIX_FAILURE;
                }
                break;
            case IEEE_INFORMATION_ELEMENT:
                if (CapwapSetIeeeInfoElem (pRcvBuf, pConfigStatusRespMsg,
                                           pConfStatRsp,
                                           (UINT2) (u2OptionalElementIndex +
                                                    u1Index)) == OSIX_FAILURE)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Set Information "
                                "Element \r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Failed to Set Information " "Element \r\n"));
                    UtlShMemFreeConfigStatusRspBuf ((UINT1 *) pConfStatRsp);
                    return OSIX_FAILURE;
                }
                break;
            case IEEE_MAC_OPERATION:
                if (CapwapSetMacOperation (pRcvBuf, pConfigStatusRespMsg,
                                           pConfStatRsp,
                                           (UINT2) (u2OptionalElementIndex +
                                                    u1Index)) == OSIX_FAILURE)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Set"
                                "Mac Operation \r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Failed to Set" "Mac Operation \r\n"));

                    UtlShMemFreeConfigStatusRspBuf ((UINT1 *) pConfStatRsp);
                    return OSIX_FAILURE;
                }
                break;
            case IEEE_MULTIDOMAIN_CAPABILITY:
                if (CapwapSetIeeeMultiDomainCapability (pRcvBuf,
                                                        pConfigStatusRespMsg,
                                                        pConfStatRsp,
                                                        (UINT2)
                                                        (u2OptionalElementIndex
                                                         + u1Index)) ==
                    OSIX_FAILURE)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Faild to set"
                                "Mac Operation \r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Faild to set" "Mac Operation \r\n"));
                    UtlShMemFreeConfigStatusRspBuf ((UINT1 *) pConfStatRsp);
                    return OSIX_FAILURE;
                }
                break;
            case IEEE_OFDM_CONTROL:
                if (CapwapSetIeeeOFDMControl (pRcvBuf, pConfigStatusRespMsg,
                                              pConfStatRsp,
                                              (UINT2) (u2OptionalElementIndex +
                                                       u1Index)) ==
                    OSIX_FAILURE)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Set"
                                "OFDM Control \r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Failed to Set" "OFDM Control \r\n"));
                    UtlShMemFreeConfigStatusRspBuf ((UINT1 *) pConfStatRsp);
                    return OSIX_FAILURE;
                }
                break;
            case IEEE_RATE_SET:
                if (CapwapSetIeeeRateSet (pRcvBuf, pConfigStatusRespMsg,
                                          pConfStatRsp,
                                          (UINT2) (u2OptionalElementIndex +
                                                   u1Index)) == OSIX_FAILURE)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Set"
                                "IEEE Supported Rates \r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Failed to Set" "IEEE Supported Rates \r\n"));
                    UtlShMemFreeConfigStatusRspBuf ((UINT1 *) pConfStatRsp);
                    return OSIX_FAILURE;
                }
                break;
            case IEEE_SUPPORTED_RATES:
                if (CapwapSetIeeeSupportedRates (pRcvBuf, pConfigStatusRespMsg,
                                                 pConfStatRsp,
                                                 (UINT2) (u2OptionalElementIndex
                                                          + u1Index)) ==
                    OSIX_FAILURE)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Set"
                                "IEEE Supported Rates \r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Failed to Set" "IEEE Supported Rates \r\n"));
                    UtlShMemFreeConfigStatusRspBuf ((UINT1 *) pConfStatRsp);
                    return OSIX_FAILURE;
                }
                break;
            case IEEE_TX_POWER:
                if (CapwapSetIeeeTxPower (pRcvBuf, pConfigStatusRespMsg,
                                          pConfStatRsp,
                                          (UINT2) (u2OptionalElementIndex +
                                                   u1Index)) == OSIX_FAILURE)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Set"
                                "Tx Power \r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Failed to Set" "Tx Power \r\n"));
                    UtlShMemFreeConfigStatusRspBuf ((UINT1 *) pConfStatRsp);
                    return OSIX_FAILURE;
                }
                break;
            case IEEE_WTP_RADIO_CONFIGURATION:
                if (CapwapSetIeeeWtpRadioConfig (pRcvBuf, pConfigStatusRespMsg,
                                                 pConfStatRsp,
                                                 (UINT2) (u2OptionalElementIndex
                                                          + u1Index)) ==
                    OSIX_FAILURE)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to set"
                                "Wtp Radio configuration \r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Failed to set"
                                  "Wtp Radio configuration \r\n"));
                    UtlShMemFreeConfigStatusRspBuf ((UINT1 *) pConfStatRsp);
                    return OSIX_FAILURE;
                }
                break;
            case IEEE_WTP_QOS:
                if (CapwapSetIeeeWtpQos (pRcvBuf, pConfigStatusRespMsg,
                                         pConfStatRsp,
                                         (UINT2) (u2OptionalElementIndex +
                                                  u1Index)) == OSIX_FAILURE)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "Failed to Set WTP QoS \r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Failed to Set WTP QoS \r\n"));
                    UtlShMemFreeConfigStatusRspBuf ((UINT1 *) pConfStatRsp);
                    return OSIX_FAILURE;
                }
                break;
            case VENDOR_SPECIFIC_PAYLOAD:
                if (CapwapSetVendSpecInfo (pRcvBuf, pConfigStatusRespMsg,
                                           pConfStatRsp,
                                           (UINT2) (u2OptionalElementIndex +
                                                    u1Index)) == OSIX_FAILURE)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Faild to set"
                                "VENDOR specific info \r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                                  "Faild to set" "VENDOR specific info \r\n"));
                    UtlShMemFreeConfigStatusRspBuf ((UINT1 *) pConfStatRsp);
                    return OSIX_FAILURE;
                }
                break;
            default:
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Unknown Message Element \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Unknown Message Element \r\n"));
                UtlShMemFreeConfigStatusRspBuf ((UINT1 *) pConfStatRsp);
                return OSIX_FAILURE;
                break;
        }
    }

    /* Profile not configured by the operator
     * increment the error count*/
    UtlShMemFreeConfigStatusRspBuf ((UINT1 *) pConfStatRsp);
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;

}

INT4
CapProcessConfigUpdateReq (tCapwapConfigUpdateReq * pCapUpdteReq,
                           tRemoteSessionManager * pSessEntry)
{
    tConfigUpdateReq    ConfigUpdateReq;
    UINT1              *pu1TxBuf = NULL;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    UINT4               u4MsgLen = 0;
    UINT1               u1IsNotLinear = OSIX_FALSE;

    UNUSED_PARAM (pCapUpdteReq);

    MEMSET (&ConfigUpdateReq, 0, sizeof (tConfigUpdateReq));

    /* Get Remote Session Module */
    /* update CAPWAP Header */
    if (CapwapConstructCpHeader (&ConfigUpdateReq.capwapHdr) == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "capwapConstructJoinReq:Failed to"
                    "construct the capwap Header\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "capwapConstructJoinReq:Failed to"
                      "construct the capwap Header\n"));
        return OSIX_FAILURE;
    }
    /* update Control Header */
    ConfigUpdateReq.u2CapwapMsgElemenLen = (UINT2) (u4MsgLen +
                                                    CAPWAP_CMN_MSG_ELM_HEAD_LEN);
    ConfigUpdateReq.u1NumMsgBlocks = 0;    /* flags */
    if (pSessEntry != NULL)
    {
        ConfigUpdateReq.u1SeqNum =
            (UINT1) (pSessEntry->lastTransmitedSeqNum + 1);
    }

    /* Allocate memory for packet linear buffer */
    pBuf = CAPWAP_ALLOCATE_CRU_BUF (CAPWAP_MAX_PKT_LEN, 0);
    if (pBuf == NULL)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Memory Allocation Failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Memory Allocation Failed \r\n"));
        return OSIX_FAILURE;
    }
    pu1TxBuf = CAPWAP_BUF_IF_LINEAR (pBuf, 0, CAPWAP_MAX_PKT_LEN);
    if (pu1TxBuf == NULL)
    {
        pu1TxBuf = (UINT1 *) MemAllocMemBlk (CAPWAP_PKTBUF_POOLID);
        /* Kloc Fix Start */
        if (pu1TxBuf == NULL)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "MemAllocMemBlk Failed \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "MemAllocMemBlk Failed \r\n"));
            CAPWAP_RELEASE_CRU_BUF (pBuf);
            return OSIX_FAILURE;
        }
        /* Kloc Fix Ends */
        u1IsNotLinear = OSIX_TRUE;
    }
    if (CapwapAssembleConfigUpdateReq (ConfigUpdateReq.u2CapwapMsgElemenLen,
                                       &ConfigUpdateReq,
                                       pu1TxBuf) == OSIX_FAILURE)
    {
        CAPWAP_RELEASE_CRU_BUF (pBuf);
        if (u1IsNotLinear == OSIX_TRUE)
        {
            MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pu1TxBuf);
        }
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapProcessConfigUpdateReq:Failed to Assemble"
                    "the packet \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "CapProcessConfigUpdateReq:Failed to Assemble"
                      "the packet \r\n"));
        return OSIX_FAILURE;
    }

    if (u1IsNotLinear == OSIX_TRUE)
    {
        /* If the CRU buffer memory is not linear */
        CAPWAP_COPY_TO_BUF (pBuf, pu1TxBuf, 0, CAPWAP_PKTBUF_POOLID);
        MemReleaseMemBlock (CAPWAP_PKTBUF_POOLID, pu1TxBuf);
    }
    if (pSessEntry == NULL)
    {
        CAPWAP_RELEASE_CRU_BUF (pBuf);
        CAPWAP_TRC (ALL_FAILURE_TRC,
                    "CapProcessConfigUpdateReq: pSessionEntry is NULL \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "CapProcessConfigUpdateReq: pSessionEntry is NULL \r\n"));
        return OSIX_FAILURE;
    }
    if (CapwapTxMessage (pBuf,
                         pSessEntry,
                         (UINT4) (ConfigUpdateReq.u2CapwapMsgElemenLen +
                                  CAPWAP_MSG_ELE_LEN),
                         WSS_CAPWAP_802_11_CTRL_PKT) == OSIX_FAILURE)
    {
        CAPWAP_RELEASE_CRU_BUF (pBuf);
        CAPWAP_TRC (ALL_FAILURE_TRC,
                    "CapProcessConfigUpdateReq:Failed to Transmit the packet \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "CapProcessConfigUpdateReq:Failed to Transmit the packet \r\n"));
        return OSIX_FAILURE;
    }
    /* store the packet buffer pointer in
     * the remote session DB copy the pointer */
    pSessEntry->lastTransmittedPkt = pBuf;
    /*Start the Retransmit timer */
    CapwapTmrStart (pSessEntry, CAPWAP_RETRANSMIT_INTERVAL_TMR,
                    MAX_RETRANSMIT_INTERVAL_TIMEOUT);

    /* update the seq number */
    pSessEntry->lastTransmitedSeqNum++;

    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

#endif /* __CAPWAP_CONFIG_ */
/*-----------------------------------------------------------------------*/
/*                    End of the file  capwapconfig.c                      */
/*-----------------------------------------------------------------------*/
