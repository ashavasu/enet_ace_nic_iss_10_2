/**************************************************************************** 
 *
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved 
 * $Id: capwapfrag.c,v 1.5 2018/01/29 09:34:35 siva Exp $       
 * Description:Consists of  fragment reassembly functions for CAPWAP Module 
 *
 *****************************************************************************/
#ifndef __CAPWAPFRAG_C__
#define __CAPWAPFRAG_C__
#include "capwapinc.h"

PUBLIC tCapwapTimerList gCapwapTmrList;

/*****************************************************************************
 * Function     : CapwapCreateFrag                                           *
 *                                                                           *
 * Description  : creates a Capwap Fragment and initialises it               *
 *                                                                           *
 * Input        : u2StartOffset - start offset of the fragment received      *
 *                u2EndOffset - End offset of the fragment received          *
 *                pBuf - Buffer to be created as a node.                     *      
 *                u2FragId - Fragmed Id of the received fragment.            *  
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/

tCapFragNode       *
CapwapCreateFrag (UINT2 u2StartOffset, UINT2 u2EndOffset,
                  tCRU_BUF_CHAIN_HEADER * pBuf, UINT2 u2FragId)
{
    tCapFragNode       *pFrag = NULL;

    if (pBuf == NULL)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CRU Buffer passed in is a NULL pointer, Return NULL \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "CRU Buffer passed in is a NULL pointer, Return NULL \r\n"));
        return NULL;
    }
    pFrag = (tCapFragNode *) MemAllocMemBlk (CAPWAP_FRAG_POOLID);
    if (pFrag == NULL)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapCreateFrag:Memory allocation failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "CapwapCreateFrag:Memory allocation failed \r\n"));
        return NULL;
    }
    pFrag->pBuf = pBuf;
    pFrag->u2StartOffset = u2StartOffset;
    pFrag->u2EndOffset = u2EndOffset;
    pFrag->u2FragId = u2FragId;

    TMO_DLL_Init_Node (&pFrag->Link);
    return pFrag;
}

/*****************************************************************************
 * Function     : CapwapDeleteFrag                                           *
 *                                                                           *
 * Description  : Deletes a Capwap Fragment and releases the memory          *
 *                                                                           *
 * Input        : pFrag - Fragment to be deleted.                            *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
VOID
CapwapDeleteFrag (tCapFragNode * pFrag)
{
    if (pFrag == NULL)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Fragment structure passed in is a NULL pointer \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Fragment structure passed in is a NULL pointer \r\n"));
        return;
    }
    if (pFrag->pBuf != NULL)
    {
        CAPWAP_RELEASE_CRU_BUF (pFrag->pBuf);
    }
    MemReleaseMemBlock (CAPWAP_FRAG_POOLID, (UINT1 *) pFrag);
}

/*****************************************************************************
 * Function     : CapwapDataFragReassemble                                   *
 *                                                                           *
 * Description  : Reassembling of fragmented CAPWAP Control packet           *
 *                                                                           *
 * Input        : pBuf  - Fragment of the packet                             *
 *                pSess - Session Entry of AP and AC                         *
 *                u2PktLen - Fragment Length                                 *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/

INT4
CapwapDataFragReassemble (tCRU_BUF_CHAIN_HEADER * pBuf,
                          tRemoteSessionManager * pSess, UINT2 u2PktLen,
                          tCRU_BUF_CHAIN_HEADER ** ppReassembleBuf)
{
    tCapFragNode       *pPrevFrag = NULL;
    tCapFragNode       *pNextFrag = NULL;
    tCapFragNode       *pTempFrag = NULL;
    UINT2               u2Place = CAPWAP_FRAG_INSERT;
    UINT1               u1FlgOffs = 0, u1IndexFound = 0;
    UINT1               u1HdrLen = 0;
    INT1                i1LastFlag;    /* 1 if not last fragment, 0 otherwise */
    UINT2               u2StartOffset = 0;    /* Index of first byte in fragment  */
    UINT2               u2FragId = 0;
    UINT2               u2EndOffset;
    UINT2               u2Index = 0;
    UINT2               u2FragIndex = 0;
    UINT1               u1ResourceTracker = 0;

    CAPWAP_FN_ENTRY ();

    if (pBuf == NULL)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CRU Buffer passed in is a NULL pointer\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "CRU Buffer passed in is a NULL pointer\r\n"));
        return OSIX_FAILURE;
    }
    if (pSess == NULL)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Session Entry passed is a NULL Pointer \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Session Entry passed is a NULL Pointer \r\n"));
        CAPWAP_RELEASE_CRU_BUF (pBuf);
        return OSIX_FAILURE;

    }

    u2PktLen = (UINT2) CAPWAP_GET_BUF_LEN (pBuf);

    /* Get CAPWAP Header Length */
    CAPWAP_COPY_FROM_BUF (pBuf, &u1HdrLen,
                          CAPWAP_HDRLEN_OFFSET, CAPWAP_HDRLEN_FIELD_LEN);
    /* Header length is 32 bit word */
    u1HdrLen = (UINT1) ((u1HdrLen & CAPWAP_HDRLEN_MASK) >> 3);
    u1HdrLen = (UINT1) (u1HdrLen << CAPWAP_4BYTE_WORD_TO_NUMOF_BYTES);

    /* Get CAPWAP Header Flags */
    CAPWAP_COPY_FROM_BUF (pBuf, &u1FlgOffs,
                          CAPWAP_FLAGS_BIT_OFFSET, CAPWAP_FLAGS_OFFSET_LEN);

    /* Get CAPWAP Fragment ID */
    CAPWAP_COPY_FROM_BUF (pBuf, &u2FragId, CAPWAP_FRAGID_OFFSET,
                          CAPWAP_FRAGID_OFFSET_LEN);
    u2FragId = OSIX_HTONS (u2FragId);

    /* Get CAPWAP Fragment Offset */
    CAPWAP_COPY_FROM_BUF (pBuf, &u2StartOffset, CAPWAP_FRAG_OFFSET,
                          CAPWAP_FRAG_OFFSET_LEN);
    u2StartOffset = OSIX_HTONS (u2StartOffset);
    u2StartOffset = (UINT2) ((u2StartOffset & CAPWAP_FRAG_OFFSET_MASK) >> 3);
    u2EndOffset = (UINT2) (u2StartOffset + u2PktLen - u1HdrLen);
    i1LastFlag = (u1FlgOffs & CAPWAP_LAST_FRAG_BIT) ? TRUE : FALSE;

    /* Remove CAPWAP  header from all fragments except first */
    if (u2StartOffset != CAPWAP_ZERO_OFFSET)
    {
        CRU_BUF_Move_ValidOffset (pBuf, u1HdrLen);
    }
    /* Get the fragment id and the data frag index */
    for (u2Index = 0; u2Index < CAPWAP_MAX_DATA_FRAG_INSTANCE; u2Index++)
    {
        if (pSess->CapDataFragStream[u2Index].u1NumberOfFragments != 0)
        {
            u1ResourceTracker++;

            if (pSess->CapDataFragStream[u2Index].u2FragmentId == u2FragId)
            {
                u2FragIndex = u2Index;
                u1IndexFound++;
                break;
            }
        }
    }

    if (u1ResourceTracker == CAPWAP_MAX_DATA_FRAG_INSTANCE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "RESOURCE UNAVAILABLE"
                    "TO STORE THE FRAGMENTS\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "RESOURCE UNAVAILABLE" "TO STORE THE FRAGMENTS\r\n"));
        CAPWAP_RELEASE_CRU_BUF (pBuf);
        return OSIX_FAILURE;
    }
    if (u1IndexFound == 1)
    {
        /* Already received some fragments */
        if (i1LastFlag == TRUE)
        {
            /* Last Fragment Received, update the entire datagram size */
            pSess->CapDataFragStream[u2FragIndex].u2TotalLen = u2EndOffset;
        }
        if (pSess->CapDataFragStream[u2FragIndex].u1NumberOfFragments ==
            CAPWAP_MAX_NO_OF_FRAGMENTS_PER_FRAME)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Exceeding max no"
                        "fragments, Cleaning the resources \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Exceeding max no"
                          "fragments, Cleaning the resources \r\n"));
            CAPWAP_RELEASE_CRU_BUF (pBuf);

            CapwapDataFreeReAssemble (&(pSess->CapDataFragStream[u2FragIndex]),
                                      u2FragIndex);
            return OSIX_FAILURE;
        }
        pSess->CapDataFragStream[u2FragIndex].u1NumberOfFragments++;
    }
    else
    {
        /* First fragment received, Find the free index and  Start Reassembly
         * Timer */
        for (u2Index = 0; u2Index < CAPWAP_MAX_DATA_FRAG_INSTANCE; u2Index++)
        {
            if (pSess->CapDataFragStream[u2Index].u1NumberOfFragments == 0)
            {
                u2FragIndex = u2Index;
                pSess->CapDataFragStream[u2Index].u2FragmentId = u2FragId;
                break;
            }
        }
        /* First fragment received,  Start Reassembly Timer */
        if (TmrStart (gCapwapTmrList.capwapRunTmrListId,
                      &(pSess->CapDataFragStream[u2FragIndex].ReassemblyTimer),
                      CAPWAP_REASSEMBLE_TMR,
                      CAPWAP_DATA_REASSENBLY_TIMEOUT, 0) == OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Capwap Tmr start Failed \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Capwap Tmr start Failed \r\n"));
            CAPWAP_RELEASE_CRU_BUF (pBuf);
            return OSIX_FAILURE;
        }

        /* Initialise the frag list */
        TMO_DLL_Init (&(pSess->CapDataFragStream[u2FragIndex].FragList));
        pSess->CapDataFragStream[u2FragIndex].u1NumberOfFragments++;

    }

    /*Set pNextFrag to the first fragment which begins after us,
       and pLastFrag to the last fragment which begins before us.
     */
    pPrevFrag = NULL;
    TMO_DLL_Scan (&(pSess->CapDataFragStream[u2FragIndex].FragList),
                  pNextFrag, tCapFragNode *)
    {
        if (pNextFrag->u2StartOffset > u2StartOffset)
        {
            break;
        }
        else if (pNextFrag->u2StartOffset == u2StartOffset)
        {
            /* Received the duplicate Fragment, drop the fragement here */
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Received the duplicate"
                        "fragment, drop here \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Received the duplicate" "fragment, drop here \r\n"));
            CAPWAP_RELEASE_CRU_BUF (pBuf);
            return OSIX_FAILURE;
        }
        pPrevFrag = pNextFrag;
        /* Previous Frag exists */
    }

    /* pPrevfrag now points, as before, to the fragment before us;
     * pNextFrag points at the next fragment. Check to see if we can
     * join to either or both fragments.
     **/

    if ((pPrevFrag != NULL) && (pPrevFrag->u2EndOffset == u2StartOffset))
    {
        u2Place |= CAPWAP_FRAG_APPEND;
    }

    if ((pNextFrag != NULL) && (pNextFrag->u2StartOffset == u2EndOffset))
    {
        u2Place |= CAPWAP_FRAG_PREPEND;
    }

    switch (u2Place)
    {
        case CAPWAP_FRAG_APPEND:    /* Append the Received Fragment */
        {
            CRU_BUF_Concat_MsgBufChains (pPrevFrag->pBuf, pBuf);
            pPrevFrag->u2EndOffset = u2EndOffset;
            break;
        }
        case CAPWAP_FRAG_PREPEND:    /* Prepend the Received Fragment */
        {
            CRU_BUF_Concat_MsgBufChains (pBuf, pNextFrag->pBuf);
            pNextFrag->pBuf = pBuf;
            pNextFrag->u2StartOffset = u2StartOffset;    /* Extend backward */
            break;
        }
            /* If fragments are received out of order then the received fragment
             * may be need to be placed in between the fragments.*/
        case (CAPWAP_FRAG_APPEND | CAPWAP_FRAG_PREPEND):
        {
            CRU_BUF_Concat_MsgBufChains (pPrevFrag->pBuf, pBuf);
            CRU_BUF_Concat_MsgBufChains (pPrevFrag->pBuf, pNextFrag->pBuf);
            pPrevFrag->u2EndOffset = pNextFrag->u2EndOffset;
            break;
        }

        default:                /* Insert new desc between pLastFrag and
                                   pNextFrag */
        {
            pTempFrag =
                CapwapCreateFrag (u2StartOffset, u2EndOffset, pBuf, u2FragId);
            if (pTempFrag == NULL)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to create"
                            "the CapwapCreateFrag Entry .\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to create"
                              "the CapwapCreateFrag Entry .\r\n"));
                CAPWAP_RELEASE_CRU_BUF (pBuf);
                /*pBuf = NULL; */
                return OSIX_FAILURE;
            }
            TMO_DLL_Insert (&pSess->CapDataFragStream[u2FragIndex].FragList,
                            &pPrevFrag->Link, &pTempFrag->Link);
            break;
        }
/* coverity fix end */
    }
    pTempFrag =
        (tCapFragNode *)
        TMO_DLL_First (&(pSess->CapDataFragStream[u2FragIndex].FragList));
    if (pTempFrag == NULL)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "First Fragment in the list"
                    "is NULL, return FAILURE\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "First Fragment in the list"
                      "is NULL, return FAILURE\r\n"));
        CAPWAP_RELEASE_CRU_BUF (pBuf);
        return OSIX_FAILURE;
    }
    /* Kloc Fix Start */
    if (pTempFrag->pBuf == NULL)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "First Fragment in the list is NULL, return FAILURE\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "First Fragment in the list is NULL, return FAILURE\r\n"));
        CAPWAP_RELEASE_CRU_BUF (pBuf);
        return OSIX_FAILURE;
    }
    /* Kloc Fix Ends */

    if ((pTempFrag->u2StartOffset == CAPWAP_ZERO_OFFSET) &&
        (pSess->CapDataFragStream[u2FragIndex].u2TotalLen !=
         CAPWAP_ZERO_LEN) &&
        (pTempFrag->u2EndOffset ==
         pSess->CapDataFragStream[u2FragIndex].u2TotalLen))
    {
        /* We 've got a complete datagram, so extract it from the
         * reassembly buffer and pass it on.
         * */
        *ppReassembleBuf = CRU_BUF_Duplicate_BufChain (pTempFrag->pBuf);

        if (*ppReassembleBuf == NULL)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "CapwapDataFragReassemble:Memory Allocation Failed \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "CapwapDataFragReassemble:Memory Allocation Failed \r\n"));
            /* Kloc Fix Start */
            CAPWAP_RELEASE_CRU_BUF (pBuf);
            /* Kloc Fix Ends */
            /*pBuf = NULL; */
            return OSIX_FAILURE;
        }
        /* Get CAPWAP Header Flags */
        CAPWAP_COPY_FROM_BUF (*ppReassembleBuf, &u1FlgOffs,
                              CAPWAP_FLAGS_BIT_OFFSET, CAPWAP_FLAGS_OFFSET_LEN);

        /* Clear the F and L Bit in CAPWAP Header */
        u1FlgOffs = (u1FlgOffs & CAPWAP_FRAG_BIT_CLEAR_OFFSET);

        /* Update Flag bits in CAPWAP Header */
        CAPWAP_COPY_TO_BUF (*ppReassembleBuf, &u1FlgOffs,
                            CAPWAP_FLAGS_BIT_OFFSET, CAPWAP_FLAGS_OFFSET_LEN);

        /* Clear Fragment Id in CAPWAP Header */
        u2FragId = 0;
        CAPWAP_COPY_TO_BUF (*ppReassembleBuf, &u2FragId, CAPWAP_FRAGID_OFFSET,
                            CAPWAP_FRAGID_OFFSET_LEN);
        CapwapDataFreeReAssemble (&(pSess->CapDataFragStream[u2FragIndex]),
                                  u2FragIndex);
        return OSIX_SUCCESS;
    }
    else
    {
        /* Not received the complete packet, Return Failure */
        CAPWAP_TRC (CAPWAP_MGMT_TRC, "CapwapFragReassemble:: Not received the "
                    "complete packet \r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                      "CapwapFragReassemble:: Not received the "
                      "complete packet \r\n"));

        return OSIX_FAILURE;
    }
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : CapwapFragReassemble                                       *
 *                                                                           *
 * Description  : Reassembling of fragmented CAPWAP Control packet           *
 *                                                                           *
 * Input        : pBuf  - Fragment of the packet                             *
 *                pSess - Session Entry of AP and AC                         *
 *                u2PktLen - Fragment Length                                 *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
CapwapFragReassemble (tCRU_BUF_CHAIN_HEADER * pBuf,
                      tRemoteSessionManager * pSess, UINT2 u2PktLen,
                      UINT1 u1PktType)
{
    tCapFragNode       *pPrevFrag = NULL;
    tCapFragNode       *pNextFrag = NULL;
    tCapFragNode       *pTempFrag = NULL;
    tCRU_BUF_CHAIN_HEADER *pReassemBuf = NULL;
    UINT2               u2Place = CAPWAP_FRAG_INSERT;
    UINT1               u1FlgOffs = 0;
    UINT1               u1HdrLen = 0;
    INT1                i1LastFlag;    /*1 if not last fragment, 0 otherwise */
    UINT2               u2StartOffset = 0;    /* Index of first byte in fragment  */
    UINT2               u2FragId = 0;
    UINT2               u2EndOffset;
    UINT2               u2MsgElemLen = 0;

    CAPWAP_FN_ENTRY ();

    if (pBuf == NULL)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "CRU Buffer passed in is a "
                    "NULL pointer\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "CRU Buffer passed in is a " "NULL pointer\r\n"));
        return OSIX_FAILURE;
    }
    if (pSess == NULL)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Session Entry passed is a "
                    "NULL Pointer \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "Session Entry passed is a " "NULL Pointer \r\n"));
        return OSIX_FAILURE;
    }

    u2PktLen = (UINT2) CAPWAP_GET_BUF_LEN (pBuf);

    /* Get CAPWAP Header Length */
    CAPWAP_COPY_FROM_BUF (pBuf, &u1HdrLen,
                          CAPWAP_HDRLEN_OFFSET, CAPWAP_HDRLEN_FIELD_LEN);
    /* Header length is 32 bit word */
    u1HdrLen = (UINT1) ((u1HdrLen & CAPWAP_HDRLEN_MASK) >> 3);
    u1HdrLen = (UINT1) (u1HdrLen << CAPWAP_4BYTE_WORD_TO_NUMOF_BYTES);

    /* Get CAPWAP Header Flags */
    CAPWAP_COPY_FROM_BUF (pBuf, &u1FlgOffs,
                          CAPWAP_FLAGS_BIT_OFFSET, CAPWAP_FLAGS_OFFSET_LEN);

    /* Get CAPWAP Fragment ID */
    CAPWAP_COPY_FROM_BUF (pBuf, &u2FragId,
                          CAPWAP_FRAGID_OFFSET, CAPWAP_FRAGID_OFFSET_LEN);
    u2FragId = OSIX_HTONS (u2FragId);

    /* Get CAPWAP Fragment Offset */
    CAPWAP_COPY_FROM_BUF (pBuf, &u2StartOffset,
                          CAPWAP_FRAG_OFFSET, CAPWAP_FRAG_OFFSET_LEN);
    u2StartOffset = OSIX_HTONS (u2StartOffset);
    u2StartOffset = (UINT2) ((u2StartOffset & CAPWAP_FRAG_OFFSET_MASK) >> 3);
    if (u1PktType == WSS_CAPWAP_802_11_CTRL_PKT)
    {
        u2EndOffset = (UINT2) (u2StartOffset + u2PktLen);
    }
    else
    {
        u2EndOffset = (UINT2) (u2StartOffset + u2PktLen - u1HdrLen);
    }

    i1LastFlag = (u1FlgOffs & CAPWAP_LAST_FRAG_BIT) ? TRUE : FALSE;

    /* Remove CAPWAP  header from all fragments except first */
    if (u2StartOffset != CAPWAP_ZERO_OFFSET)
    {
        if (u1PktType == WSS_CAPWAP_802_11_CTRL_PKT)
        {
            CRU_BUF_Move_ValidOffset (pBuf, CAPWAP_HEADER_LEN +
                                      CAPWAP_CONTROL_HDR_LEN);
            u2EndOffset =
                u2EndOffset - (CAPWAP_HEADER_LEN + CAPWAP_CONTROL_HDR_LEN);
        }
        else
        {
            CRU_BUF_Move_ValidOffset (pBuf, u1HdrLen);
        }
    }

    if (i1LastFlag == TRUE)
    {
        /* Last Fragment Received, update the entire datagram size */
        pSess->CapFragStream.u2TotalLen = u2EndOffset;
    }
    if (pSess->CapFragStream.u1NumberOfFragments == 0 && u2StartOffset == 0)
    {
#if 0
        /* First fragment received,  Start Reassembly Timer */
        if (CapwapTmrStart (pSess, CAPWAP_REASSEMBLE_TMR,
                            CAPWAP_REASSENBLY_TIMEOUT) == OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Capwap Tmr start Failed \r\n");
            CAPWAP_RELEASE_CRU_BUF (pBuf);
            pBuf = NULL;
            return OSIX_FAILURE;
        }
#else

        /* First fragment received,  Start Reassembly Timer */
        if (TmrStart (gCapwapTmrList.capwapRunTmrListId,
                      &(pSess->CapFragStream.ReassemblyTimer),
                      CAPWAP_REASSEMBLE_TMR,
                      CAPWAP_REASSENBLY_TIMEOUT, 0) == OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Capwap Tmr start Failed \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Capwap Tmr start Failed \r\n"));
            CAPWAP_RELEASE_CRU_BUF (pBuf);
            return OSIX_FAILURE;
        }
#endif
        TMO_DLL_Init (&(pSess->CapFragStream.FragList));
        pSess->CapFragStream.u1NumberOfFragments++;

    }
    else
    {
        if (u2StartOffset > 0 && pSess->CapFragStream.u1NumberOfFragments == 0)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Fragment"
                        "is received with a delay so dropping it\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId, "Fragment"
                          "is received with a delay so dropping it\r\n"));
            CAPWAP_RELEASE_CRU_BUF (pBuf);
            /*pBuf = NULL; */
            return OSIX_FAILURE;

        }
        if (pSess->CapFragStream.u1NumberOfFragments ==
            CAPWAP_MAX_NO_OF_FRAGMENTS_PER_FRAME)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Exceeding max no fragments, "
                        "Cleaning the resources \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Exceeding max no fragments, "
                          "Cleaning the resources \r\n"));
            CAPWAP_RELEASE_CRU_BUF (pBuf);
            /* pBuf = NULL; */
            CapwapFreeReAssemble (&(pSess->CapFragStream));
            return OSIX_FAILURE;
        }
        pSess->CapFragStream.u1NumberOfFragments++;
    }

    /*
     * Set pNextFrag to the first fragment which begins after us,
     * and pLastFrag to the last fragment which begins before us.
     */
    pPrevFrag = NULL;

    TMO_DLL_Scan (&(pSess->CapFragStream.FragList), pNextFrag, tCapFragNode *)
    {
        if (pNextFrag->u2StartOffset > u2StartOffset)
        {
            break;
        }
        else if (pNextFrag->u2StartOffset == u2StartOffset)
        {
            /* Received the duplicate Fragment, drop the fragement here */
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Received the duplicate "
                        "fragment, drop here \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Received the duplicate "
                          "fragment, drop here \r\n"));
            CAPWAP_RELEASE_CRU_BUF (pBuf);
            /*pBuf = NULL; */
            return OSIX_FAILURE;
        }
        pPrevFrag = pNextFrag;
        /* Previous Frag exists */
    }

    /* pPrevfrag now points, as before, to the fragment before us;
     * pNextFrag points at the next fragment. Check to see if we can
     * join to either or both fragments.*/

    if ((pPrevFrag != NULL) && (pPrevFrag->u2EndOffset == u2StartOffset))
    {
        u2Place = CAPWAP_FRAG_APPEND;
    }

    if ((pNextFrag != NULL) && (pNextFrag->u2StartOffset == u2EndOffset))
    {
        u2Place = CAPWAP_FRAG_PREPEND;
    }

    switch (u2Place)
    {
            /* Insert new desc between pLastFrag and pNextFrag */
        case CAPWAP_FRAG_APPEND:    /* Append the Received Fragment */
        {
            CRU_BUF_Concat_MsgBufChains (pPrevFrag->pBuf, pBuf);
            pPrevFrag->u2EndOffset = u2EndOffset;
            break;
        }
        case CAPWAP_FRAG_PREPEND:    /* Prepend the Received Fragment */
        {
            CRU_BUF_Concat_MsgBufChains (pBuf, pNextFrag->pBuf);
            pNextFrag->pBuf = pBuf;
            pNextFrag->u2StartOffset = u2StartOffset;    /* Extend backward */
            break;
        }
        default:                /* Insert new desc between pLastFrag and pNextFrag */
        {
            pTempFrag = CapwapCreateFrag (u2StartOffset, u2EndOffset, pBuf,
                                          u2FragId);
            if (pTempFrag == NULL)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to create the "
                            "CapwapCreateFrag Entry .\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to create the "
                              "CapwapCreateFrag Entry .\r\n"));
                CAPWAP_RELEASE_CRU_BUF (pBuf);
                /*pBuf = NULL; */
                return OSIX_FAILURE;
            }
            TMO_DLL_Insert (&pSess->CapFragStream.FragList,
                            &pPrevFrag->Link, &pTempFrag->Link);
            break;
        }
    }

    pTempFrag =
        (tCapFragNode *) TMO_DLL_First (&(pSess->CapFragStream.FragList));
    if (pTempFrag == NULL)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "First Fragment in the list "
                    "is NULL, return FAILURE\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "First Fragment in the list "
                      "is NULL, return FAILURE\r\n"));
        return OSIX_FAILURE;
    }

    if (pTempFrag->pBuf == NULL)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "First Fragment in the list is "
                    "NULL, return FAILURE\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "First Fragment in the list is "
                      "NULL, return FAILURE\r\n"));
        return OSIX_FAILURE;
    }

    if ((pTempFrag->u2StartOffset == CAPWAP_ZERO_OFFSET) &&
        (pSess->CapFragStream.u2TotalLen != CAPWAP_ZERO_LEN) &&
        (pTempFrag->u2EndOffset == pSess->CapFragStream.u2TotalLen))
    {
        /* We've got a complete datagram, so extract it from the
         * reassembly buffer and pass it on.*/

        pReassemBuf = CRU_BUF_Duplicate_BufChain (pTempFrag->pBuf);

        if (pReassemBuf == NULL)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Unable to Duplicate the packet "
                        ",return FAILURE\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "Unable to Duplicate the packet "
                          ",return FAILURE\r\n"));
            return OSIX_FAILURE;
        }

        /* Get CAPWAP Header Flags */
        CAPWAP_COPY_FROM_BUF (pReassemBuf, &u1FlgOffs,
                              CAPWAP_FLAGS_BIT_OFFSET, CAPWAP_FLAGS_OFFSET_LEN);

        /* Clear the F and L Bit in CAPWAP Header */
        u1FlgOffs = (u1FlgOffs & CAPWAP_FRAG_BIT_CLEAR_OFFSET);

        /* Update Flag bits in CAPWAP Header */
        CAPWAP_COPY_TO_BUF (pReassemBuf, &u1FlgOffs,
                            CAPWAP_FLAGS_BIT_OFFSET, CAPWAP_FLAGS_OFFSET_LEN);
        /* Clear Fragment Id in CAPWAP Header */
        u2FragId = 0;
        CAPWAP_COPY_TO_BUF (pReassemBuf, &u2FragId,
                            CAPWAP_FRAGID_OFFSET, CAPWAP_FRAGID_OFFSET_LEN);
        if (u1PktType == WSS_CAPWAP_802_11_CTRL_PKT)
        {
            u2MsgElemLen =
                pTempFrag->u2EndOffset + CAPWAP_CMN_MSG_ELM_HEAD_LEN -
                (CAPWAP_HEADER_LEN + CAPWAP_CONTROL_HDR_LEN);
            u2MsgElemLen = OSIX_HTONS (u2MsgElemLen);

            CAPWAP_COPY_TO_BUF (pReassemBuf, &(u2MsgElemLen), 13, 2);
        }

        /* No need to update Fragment offset because 
           always for first fragment offset will be zero */
        CapwapProcessReassemCtrlPacket (pReassemBuf);
        CapwapFreeReAssemble (&(pSess->CapFragStream));
    }
    else
    {
        /* Not received the complete packet, Return Failure */
        CAPWAP_TRC (CAPWAP_MGMT_TRC,
                    "CapwapFragReassemble: "
                    "Not received the complete packet \r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CapwapSysLogId,
                      "CapwapFragReassemble: "
                      "Not received the complete packet \r\n"));
    }
    CAPWAP_FN_EXIT ();
    return OSIX_FAILURE;
}

/*****************************************************************************
 * Function     : CapwapFreeReAssemble                                       *
 *                                                                           *
 * Description  : Deletes the fragments of fragmented CAPWAP Control packet  *
 *                and releases the memory associated with it.                *
 *                                                                           *
 * Input        : pCapFragment - Fragment List to store the  CAPWAP control  *
 *                               packet.                                     *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
CapwapFreeReAssemble (tCapFragment * pCapFragment)
{
    tCapFragNode       *pFrag = NULL;
    UINT4               u4TmrRetVal = 0;
    UINT4               u4RemainingTime = 0;

    CAPWAP_FN_ENTRY ();

    if (pCapFragment == NULL)
    {
        return OSIX_FAILURE;
    }
    /* Stop the Reassembly Timer */

    u4TmrRetVal = TmrGetRemainingTime (gCapwapTmrList.capwapRunTmrListId,
                                       &(pCapFragment->ReassemblyTimer.
                                         TimerNode), &u4RemainingTime);

    if (u4TmrRetVal != TMR_FAILURE)
    {
        if (TmrStop (gCapwapTmrList.capwapRunTmrListId,
                     &(pCapFragment->ReassemblyTimer)) != TMR_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "capwapTmrStop:Failure to stop"
                        "ReAssembly timer \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "capwapTmrStop:Failure to stop"
                          "ReAssembly timer \r\n"));
            return OSIX_FAILURE;
        }
    }

    /* Free any fragments on list, starting at beginning */
    while ((pFrag = (tCapFragNode *) TMO_DLL_First (&(pCapFragment->FragList))))
    {
        /* Should have deleted from reassembly list before freeing it. */
        if (pFrag->pBuf != NULL)
        {
            CAPWAP_RELEASE_CRU_BUF (pFrag->pBuf);
            pFrag->pBuf = NULL;
        }
        TMO_DLL_Delete (&(pCapFragment->FragList), &pFrag->Link);
        MemReleaseMemBlock (CAPWAP_FRAG_POOLID, (UINT1 *) pFrag);
        pFrag = NULL;
    }
    pCapFragment->u1NumberOfFragments = 0;
    pCapFragment->u2FragmentId = 0;
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : CapwapDataFreeReAssemble                                   *
 *                                                                           *
 * Description  : Deletes the fragments of fragmented CAPWAP data packet     *
 *                and releases the memory associated with it.                *
 *                                                                           *
 * Input        : pCapFragment - Array of Fragment List to store the  CAPWAP *
 *                               data packet.                                *
 *                u2FragIndex  - Index of the Fragment List                  *       
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/

INT4
CapwapDataFreeReAssemble (tCapFragment * pCapDataFragStream, UINT2 u2FragIndex)
{
    tCapFragNode       *pFrag = NULL;
    UINT4               u4TmrRetVal = 0;
    UINT4               u4RemainingTime = 0;

    UNUSED_PARAM (u2FragIndex);

    CAPWAP_FN_ENTRY ();

    if (pCapDataFragStream == NULL)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapDataFreeReAssemble : pCapDataFragStream is "
                    "NULL \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "CapwapDataFreeReAssemble : pCapDataFragStream is "
                      "NULL \r\n"));
        return OSIX_FAILURE;
    }
    /* Stop the Reassembly Timer */

    u4TmrRetVal = TmrGetRemainingTime (gCapwapTmrList.capwapRunTmrListId,
                                       &(pCapDataFragStream->ReassemblyTimer.
                                         TimerNode), &u4RemainingTime);

    if (u4TmrRetVal != TMR_FAILURE)
    {
        if (TmrStop (gCapwapTmrList.capwapRunTmrListId,
                     &(pCapDataFragStream->ReassemblyTimer)) != TMR_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "capwapTmrStop:Failure to stop"
                        "ReAssembly timer \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          "capwapTmrStop:Failure to stop"
                          "ReAssembly timer \r\n"));
            return OSIX_FAILURE;
        }
    }

    /* Free any fragments on list, starting at beginning */
    while ((pFrag = (tCapFragNode *) TMO_DLL_First
            (&(pCapDataFragStream->FragList))))
    {
        if (pFrag->pBuf != NULL)
        {
            CAPWAP_RELEASE_CRU_BUF (pFrag->pBuf);
            pFrag->pBuf = NULL;
        }
        /* Should have deleted from reassembly list before freeing it. */
        TMO_DLL_Delete (&(pCapDataFragStream->FragList), &pFrag->Link);
        MemReleaseMemBlock (CAPWAP_FRAG_POOLID, (UINT1 *) pFrag);
    }
    pCapDataFragStream->u2FragmentId = 0;
    pCapDataFragStream->u1NumberOfFragments = 0;

    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : CapwapPutHdr                                               *
 *                                                                           *
 * Description  : API to form the CAPWAP Header to a packet                  *
 *                                                                           *
 * Input        : pBuf - Packet to which CAPWAP Header is to be added        *
 *                pHdr - CAPWAP Header that has to be prepended              *       
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
VOID
CapwapPutHdr (tCRU_BUF_CHAIN_HEADER * pBuf, tCapwapHdr * pHdr)
{
    UINT1               au1CapHdr[CAPWAP_MAX_HDR_LEN];

    CAPWAP_FN_ENTRY ();

    AssembleCapwapHeader (au1CapHdr, pHdr);
    if (CRU_BUF_Prepend_BufChain (pBuf, (UINT1 *) au1CapHdr, pHdr->u2HdrLen) !=
        CRU_SUCCESS)
    {
    }

    CAPWAP_FN_EXIT ();
}

/*****************************************************************************
 * Function     : CapwapPutCtrlHdr                                           *
 *                                                                           *
 * Description  : API to form the CAPWAP COntrol Header to a packet          *
 *                                                                           *
 * Input        : pBuf - Packet to which CAPWAP COntrol Header is to be      * 
 *                       added                                               *
 *                pCtrlHdr -CAPWAP ControlHeader that has to be prepended    *       
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/

VOID
CapwapPutCtrlHdr (tCRU_BUF_CHAIN_HEADER * pBuf, tCapwapCtrlHdr * pCtrlHdr)
{
    UINT1               au1CapCtrlHdr[CAPWAP_CTRL_HEADER_SIZE];

    CAPWAP_FN_ENTRY ();

    AssembleCapwapControlHeader (au1CapCtrlHdr,
                                 pCtrlHdr->u4MsgType,
                                 pCtrlHdr->u1SeqNum,
                                 pCtrlHdr->u2MsgElementLen, pCtrlHdr->u1Flags);
    if (CRU_BUF_Prepend_BufChain (pBuf, (UINT1 *) au1CapCtrlHdr,
                                  CAPWAP_CTRL_HEADER_SIZE) != CRU_SUCCESS)
    {
    }
    CAPWAP_FN_EXIT ();
}

/*****************************************************************************
 * Function     : CapwapExtractHdr                                           *
 *                                                                           *
 * Description  : API to Extract the CAPWAP Header from a packet             *
 *                                                                           *
 * Input        : pBuf - Packet from which CAPWAP Header is to be extracted  *
 *                pHdr - Buffer to store the CAPWAP Header                   *       
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
CapwapExtractHdr (tCRU_BUF_CHAIN_HEADER * pBuf, tCapwapHdr * pHdr)
{
    UINT1               au1CapHdr[CAPWAP_MAX_HDR_LEN];
    UINT1               u1HdrLen = 0;
    UINT1              *pCapHdr = NULL;

    /* Get CAPWAP Header Length */
    CAPWAP_COPY_FROM_BUF (pBuf, &u1HdrLen,
                          CAPWAP_HDRLEN_OFFSET, CAPWAP_HDRLEN_FIELD_LEN);

    /* Header length is 32 bit word */
    u1HdrLen = (UINT1) ((u1HdrLen & CAPWAP_HDRLEN_MASK) >> 3);
    u1HdrLen = (UINT1) (u1HdrLen << CAPWAP_4BYTE_WORD_TO_NUMOF_BYTES);
    CAPWAP_COPY_FROM_BUF (pBuf, au1CapHdr, 0, u1HdrLen);
    pCapHdr = au1CapHdr;
    CAPWAP_GET_1BYTE (pHdr->u1Preamble, pCapHdr);
    CAPWAP_GET_2BYTE (pHdr->u2HlenRidWbidFlagT, pCapHdr);
    CAPWAP_GET_1BYTE (pHdr->u1FlagsFLWMKResd, pCapHdr);
    CAPWAP_GET_2BYTE (pHdr->u2FragId, pCapHdr);
    CAPWAP_GET_2BYTE (pHdr->u2FragOffsetRes3, pCapHdr);
    if (u1HdrLen > 8)
    {
        CAPWAP_GET_1BYTE (pHdr->u1RadMacAddrLength, pCapHdr);
        CAPWAP_GET_NBYTE (pHdr->RadioMacAddr, pCapHdr,
                          pHdr->u1RadMacAddrLength);
    }
    pHdr->u2HdrLen = u1HdrLen;
    return CAPWAP_SUCCESS;
}

/*****************************************************************************
 * Function     : CapwapExtractCtrlHdr                                       *
 *                                                                           *
 * Description  : API to Extract the CAPWAP Control Header from a packet     *
 *                                                                           *
 * Input        : pBuf - Packet from which CAPWAP Control Header is to be    *
 *                       extracted                                           *
 *                pHdr - Buffer to store the CAPWAP Control Header           *       
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/

INT4
CapwapExtractCtrlHdr (tCRU_BUF_CHAIN_HEADER * pBuf, tCapwapCtrlHdr * pCtrlHdr)
{
    UINT1               au1CapCtrlHdr[CAPWAP_CTRL_HEADER_SIZE];
    UINT1              *pCtrlCapHdr = NULL;

    /* Get CAPWAP Header Length */
    CAPWAP_COPY_FROM_BUF (pBuf, au1CapCtrlHdr,
                          CAPWAP_HDR_LEN, CAPWAP_CTRL_HEADER_SIZE);
    pCtrlCapHdr = au1CapCtrlHdr;
    CAPWAP_GET_4BYTE (pCtrlHdr->u4MsgType, pCtrlCapHdr);
    CAPWAP_GET_1BYTE (pCtrlHdr->u1SeqNum, pCtrlCapHdr);
    CAPWAP_GET_2BYTE (pCtrlHdr->u2MsgElementLen, pCtrlCapHdr);
    CAPWAP_GET_1BYTE (pCtrlHdr->u1Flags, pCtrlCapHdr);

    return CAPWAP_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapFragmentAndSend                             */
/*                                                                      */
/*  Description     : The function fragments  the received CAPWAP       */
/*                    Control/Data Packets based on the MTU size        */
/*                                                                      */
/*  Input(s)        : pBuf  - Packet to be Fragmented                   */
/*                    u4MTU - MTU Size                                  */
/*                    pSessEntry - Session Entry for the AP/AC          */
/*                    i2PktLen - Packet Length                          */
/*                    u1PktType - CAPWAP Control/Data Packet            */
/*                                                                      */
/*  Output(s)       : None.                                             */
/*                                                                      */
/*  Returns         : SUCCESS/FAILURE                                   */
/************************************************************************/

INT4
CapwapFragmentAndSend (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4MTU,
                       tRemoteSessionManager * pSessEntry, INT2 i2PktLen,
                       UINT1 u1PktType)
{
    UINT2               u2FragDataSize = 0;
    UINT2               u2Len = 0;
    UINT2               u2Offset = 0;
    UINT2               u2FragId = 0;
    tCRU_BUF_CHAIN_HEADER *pTmpBuf = NULL;
    tCRU_BUF_CHAIN_HEADER *pPktBuf = NULL;
    UINT1               u1FlgOffs = 0;
    UINT1               u1HdrLen = 0;
    INT1                i1OriLastFlag;
    tCapwapHdr          CapHdr;
    tCapwapCtrlHdr      CapCtrlHdr;
    UINT1               u1Type[2];
    UINT1               u1Length[2];
    UINT2               u2PacketLen = 0;
    UINT1               u1LastFrag = OSIX_FALSE;
    UINT1               u1FirstFrag = 1;

    CAPWAP_FN_ENTRY ();
    MEMSET (&CapHdr, 0, sizeof (tCapwapHdr));
    MEMSET (&u1Type, 0, sizeof (UINT2));
    MEMSET (&u1Length, 0, sizeof (UINT2));
    MEMSET (&CapCtrlHdr, 0, sizeof (tCapwapCtrlHdr));

    pPktBuf = CAPWAP_ALLOCATE_CRU_BUF (i2PktLen, 0);

    if (pPktBuf == NULL)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "CRU Buffer Allocation Failed "
                    "NULL pointer \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "CRU Buffer Allocation Failed " "NULL pointer \r\n"));
        return CAPWAP_FAILURE;
    }

    if ((CRU_BUF_Copy_BufChains (pPktBuf, pBuf, 0, 0, i2PktLen)) != CRU_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "CRU Buffer Copy " "Failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId, "CRU Buffer Copy "
                      "Failed \r\n"));
        CAPWAP_RELEASE_CRU_BUF (pPktBuf);
        return CAPWAP_FAILURE;

    }

    u2PacketLen = CAPWAP_GET_BUF_LEN (pPktBuf);

    /* Get CAPWAP Header Length */
    CAPWAP_COPY_FROM_BUF (pPktBuf, &u1HdrLen,
                          CAPWAP_HDRLEN_OFFSET, CAPWAP_HDRLEN_FIELD_LEN);
    /* Header length is 32 bit word */
    u1HdrLen = (UINT1) ((u1HdrLen & CAPWAP_HDRLEN_MASK) >> 3);
    u1HdrLen = (UINT1) (u1HdrLen << CAPWAP_4BYTE_WORD_TO_NUMOF_BYTES);

    /* Get CAPWAP Header Flags */
    CAPWAP_COPY_FROM_BUF (pPktBuf, &u1FlgOffs,
                          CAPWAP_FLAGS_BIT_OFFSET, CAPWAP_FLAGS_OFFSET_LEN);

    i1OriLastFlag = (u1FlgOffs & CAPWAP_LAST_FRAG_BIT) ? TRUE : FALSE;

    /* Get Capwap Header */
    CapwapExtractHdr (pPktBuf, &CapHdr);
    /* Move the cru buffer */

    if (u1PktType == WSS_CAPWAP_802_11_CTRL_PKT)
    {
        CapwapExtractCtrlHdr (pPktBuf, &CapCtrlHdr);

        CRU_BUF_Move_ValidOffset (pPktBuf, (CAPWAP_HEADER_LEN +
                                            CAPWAP_CONTROL_HDR_LEN));

    }
    else
    {
        u2PacketLen = (INT2) (u2PacketLen - u1HdrLen);
        CRU_BUF_Move_ValidOffset (pPktBuf, u1HdrLen);
    }

    u2FragId = pSessEntry->u2CtrllastfragID;

    /*  Fragments should be in  multpIple of 8 bytes */
    if (u4MTU < u1HdrLen)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "MTU is less than capwap "
                    "header length\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                      "MTU is less than capwap " "header length\r\n"));
        CAPWAP_RELEASE_CRU_BUF (pPktBuf);
        return OSIX_FAILURE;
    }

    if (u1PktType == WSS_CAPWAP_802_11_CTRL_PKT)
    {
        if (pSessEntry->u1DtlsFlag == DTLS_CFG_DISABLE)
        {
#ifdef WLC_WANTED
            u4MTU = (UINT4) (u4MTU - (CAPWAP_HEADER_LEN +
                                      CAPWAP_CONTROL_HDR_LEN));

            /*SUM of Length of CAPWAP Header = 8,
               Length of CAPWAP Control Header = 8, */
#else
            u4MTU =
                (UINT4) (u4MTU - (CAPWAP_HEADER_LEN + CAPWAP_CONTROL_HDR_LEN) -
                         IP_UDP_HEADER_LEN);
            /*IP Header Length = 20
               UDP Header Length = 8 */
#endif
        }
        else
        {
#ifdef WLC_WANTED
            u4MTU = (UINT4) (u4MTU - (CAPWAP_HEADER_LEN +
                                      CAPWAP_CONTROL_HDR_LEN) - 45);

            /*SUM of Length of CAPWAP Header = 8,
               Length of CAPWAP Control Header = 8, */
#else
            u4MTU =
                (UINT4) (u4MTU - (CAPWAP_HEADER_LEN + CAPWAP_CONTROL_HDR_LEN) -
                         IP_UDP_HEADER_LEN - 45);
            /*IP Header Length = 20
               UDP Header Length = 8 */
#endif
        }
    }
    else
    {
#ifdef WLC_WANTED
        u4MTU = (UINT4) (u4MTU - u1HdrLen);
#else
        u4MTU = (UINT4) (u4MTU - u1HdrLen - IP_UDP_HEADER_LEN);    /*IP Header Length = 20
                                                                   UDP Header Length = 8 */

#endif
    }

    pTmpBuf = pPktBuf;

    while (u2PacketLen > 0)
    {
        u2FragId = pSessEntry->u2CtrllastfragID;

        if ((UINT2) (u2PacketLen) <= u4MTU)
        {
            /* Last fragment; send all that remains */
            u2FragDataSize = (UINT2) u2PacketLen;
            CapHdr.u1FlagsFLWMKResd |= CAPWAP_LAST_FRAG_BIT;
            CapHdr.u1FlagsFLWMKResd |= CAPWAP_MORE_FRAG_BIT;
            CapHdr.u2FragId = u2FragId;
            CapHdr.u2FragOffsetRes3 = (UINT2) (u2Offset << 3);
            pPktBuf = pTmpBuf;
            u1LastFrag = OSIX_TRUE;
        }
        else
        {
            u2FragDataSize = (UINT2) u4MTU;
            CapHdr.u1FlagsFLWMKResd |= CAPWAP_MORE_FRAG_BIT;
            CapHdr.u1FlagsFLWMKResd |= i1OriLastFlag;
            CapHdr.u2FragId = u2FragId;
            CapHdr.u2FragOffsetRes3 = (UINT2) (u2Offset << 3);
            pPktBuf = pTmpBuf;

            if (CAPWAP_FRAGMENT_BUF (pPktBuf, u2FragDataSize, &pTmpBuf) !=
                CRU_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Fragment the "
                            "Packet \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Failed to Fragment the " "Packet \r\n"));
                CAPWAP_RELEASE_CRU_BUF (pPktBuf);
                return OSIX_FAILURE;
            }
            if (pTmpBuf == NULL)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Error in Fragmentation"
                            ". Fragmented Buff is NULL\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                              "Error in Fragmentation"
                              ". Fragmented Buff is NULL\r\n"));
                CAPWAP_RELEASE_CRU_BUF (pPktBuf);
                return OSIX_FAILURE;
            }

        }

        if (u1PktType == WSS_CAPWAP_802_11_CTRL_PKT)
        {

            CapCtrlHdr.u2MsgElementLen = u2FragDataSize;
            CapwapPutCtrlHdr (pPktBuf, &CapCtrlHdr);
            CapwapPutHdr (pPktBuf, &CapHdr);
            u2Len = (UINT2) (CRU_BUF_Get_ChainValidByteCount (pPktBuf));
        }
        else
        {
            CapwapPutHdr (pPktBuf, &CapHdr);

            u2Len = (UINT2) (CRU_BUF_Get_ChainValidByteCount (pPktBuf));
        }

        if (u1FirstFrag == 1)
        {
            u2Offset =
                (UINT2) (u2FragDataSize +
                         (CAPWAP_HEADER_LEN + CAPWAP_CONTROL_HDR_LEN));
            u2PacketLen -= u2Offset;
            u1FirstFrag = 0;
        }
        else
        {
            /* Now the datagram to send is in pPktBuf; Add frag specific info */
            u2Offset += u2FragDataSize;    /* Each time offset gets updated */
            u2PacketLen -= u2FragDataSize;
        }
        if (CapwapTxPktAfterFrag (pPktBuf, pSessEntry, u2Len, u1PktType)
            == OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, " CapwapTxMessage Failed.  \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4CapwapSysLogId,
                          " CapwapTxMessage Failed.  \r\n"));
        }
        CAPWAP_RELEASE_CRU_BUF (pPktBuf);
        pPktBuf = NULL;
    }

    /* Increment the Fragment Id */
    pSessEntry->u2CtrllastfragID++;
    UNUSED_PARAM (u1LastFrag);
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}
#endif
