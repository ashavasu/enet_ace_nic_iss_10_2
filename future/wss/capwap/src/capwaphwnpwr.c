/* $Id: capwaphwnpwr.c,v 1.1.1.1 2016/09/20 10:42:39 siva Exp $ */
#include "nputil.h"
/***************************************************************************
 *                                                                          
 *    Function Name       : CapwapNpWrHwProgram                             
 *                                                                          
 *    Description         : This function takes care of calling appropriate 
 *                          Np call using the tCapwapNpModInfo
 *                                                                          
 *    Input(s)            : FsHwNpParam of type tfsHwNp                     
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/
#ifdef WLC_WANTED
PUBLIC UINT1 CapwapNpWrHwProgram (tFsHwNp * pFsHwNp)
{
    UINT1               u1RetVal = FNP_FAILURE;
    UINT4               u4Opcode = 0;
    tCapwapNpModInfo    *pCapwapNpModInfo = NULL;

    if (NULL == pFsHwNp)
    {
        return FNP_FAILURE;
    }

    u4Opcode = pFsHwNp->u4Opcode;
    pCapwapNpModInfo = &(pFsHwNp->CapwapNpModInfo);

    if (NULL == pCapwapNpModInfo)
    {
        return FNP_FAILURE;
    }

    switch (u4Opcode)
    {
        case CAPWAP_ENABLE:
        {
            tCapwapNpWrEnable *pEntry = NULL;
            pEntry = &pCapwapNpModInfo->CapwapNpEnable;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = capwapNpEnable (pEntry->ptCapwapNpParams);
            break;
        }
        case CAPWAP_DISABLE:
        {
            u1RetVal = capwapNpDisable ();
            break;
        }

        case CAPWAP_CREATE_UNICAST_TUNNEL:
        {
            tCapwapNpWrCreateUcTunnel *pEntry = NULL;
            pEntry = &pCapwapNpModInfo->CapwapNpCreateUcTunnel;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = capwapNpCreateUcTunnel (pEntry->ptCapwapNpParams);
            break;
        }

        case CAPWAP_CREATE_MULTICAST_TUNNEL:
        {
            tCapwapNpWrCreateMcTunnel *pEntry = NULL;
            pEntry = &pCapwapNpModInfo->CapwapNpCreateMcTunnel;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = capwapNpCreateMcTunnel (pEntry->ptCapwapNpParams);
            break;
        }

        case CAPWAP_CREATE_BROADCAST_TUNNEL:
        {
            tCapwapNpWrCreateBcTunnel *pEntry = NULL;
            pEntry = &pCapwapNpModInfo->CapwapNpCreateBcTunnel;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = capwapNpCreateBcTunnel (pEntry->ptCapwapNpParams);
            break;
        }

        case CAPWAP_DELETE_UNICAST_TUNNEL:
        {
            tCapwapNpWrDeleteUcTunnel *pEntry = NULL;
            pEntry = &pCapwapNpModInfo->CapwapNpDeleteUcTunnel;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = capwapNpDeleteUcTunnel (pEntry->ptCapwapNpParams);
            break;
        }

        case CAPWAP_DELETE_MULTICAST_TUNNEL:
        {
            u1RetVal = capwapNpDeleteMcTunnel ();
            break;
        }

        case CAPWAP_DELETE_BROADCAST_TUNNEL:
        {
            u1RetVal = capwapNpDeleteBcTunnel ();
            break;
        }

        case CAPWAP_DELETE_ALL_TUNNEL:
        {
            u1RetVal = capwapNpDeleteAllTunnel ();
            break;
        }

        case CAPWAP_GET_UNICAST_TUNNEL_INFO:
        {
            tCapwapNpWrGetUcTunnelInfo *pEntry = NULL;
            pEntry = &pCapwapNpModInfo->CapwapNpGetUcTunnelInfo;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
                u1RetVal = 
                    capwapNpGetUnicastTunnelInfo (pEntry->ptCapwapNpParams);
            break;
        }

        case CAPWAP_GET_MULTICAST_TUNNEL_INFO:
        {
            tCapwapNpWrGetMcTunnelInfo *pEntry = NULL;
            pEntry = &pCapwapNpModInfo->CapwapNpGetMcTunnelInfo;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
                u1RetVal = 
                    capwapNpGetMulticastTunnelInfo (pEntry->ptCapwapNpParams);
            break;
        }

        case CAPWAP_GET_BROADCAST_TUNNEL_INFO:
        {
            tCapwapNpWrGetBcTunnelInfo *pEntry = NULL;
            pEntry = &pCapwapNpModInfo->CapwapNpGetBcTunnelInfo;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
                u1RetVal = 
                    capwapNpGetBroadcastTunnelInfo (pEntry->ptCapwapNpParams);
            break;
        }

        default:
            break;

    }                            /* switch */

    return (u1RetVal);
}
#endif
