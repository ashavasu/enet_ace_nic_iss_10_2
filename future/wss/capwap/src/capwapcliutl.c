/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: capwapcliutl.c,v 1.6 2017/11/24 10:37:03 siva Exp $            
*
* Description: This file contains utility functions used by protocol Capwap
*********************************************************************/

#include "capwapinc.h"
#include "capwapcli.h"

#ifdef WLC_WANTED
#include "wsscfgtdfsg.h"
#include "wsscfgtdfs.h"
#include "wlchdlr.h"
#include "wlchdlr.h"
#else
#include "wtpnvram.h"
#include "wsscfgtdfsg.h"
#include "wsscfgtdfs.h"
#endif
#include "wsscfgprotg.h"
#include "wsscfgwlanproto.h"
#include "wsspmdb.h"
#include "wsspmprot.h"
#include "wssifinc.h"

extern UINT4        gu1IsConfigResponseReceived;
extern UINT4        gu4Stups;
extern INT4         CapwapClearWtpConfig (VOID);

/****************************************************************************
 Function    :  CapwapGetAllUtlCapwapBaseAcNameListTable
 Input       :  pCapwapGetCapwapBaseAcNameListEntry
                pCapwapdsCapwapBaseAcNameListEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetAllUtlCapwapBaseAcNameListTable (tCapwapCapwapBaseAcNameListEntry *
                                          pCapwapGetCapwapBaseAcNameListEntry,
                                          tCapwapCapwapBaseAcNameListEntry *
                                          pCapwapdsCapwapBaseAcNameListEntry)
{
    UNUSED_PARAM (pCapwapGetCapwapBaseAcNameListEntry);
    UNUSED_PARAM (pCapwapdsCapwapBaseAcNameListEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetAllUtlCapwapBaseMacAclTable
 Input       :  pCapwapGetCapwapBaseMacAclEntry
                pCapwapdsCapwapBaseMacAclEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetAllUtlCapwapBaseMacAclTable (tCapwapCapwapBaseMacAclEntry *
                                      pCapwapGetCapwapBaseMacAclEntry,
                                      tCapwapCapwapBaseMacAclEntry *
                                      pCapwapdsCapwapBaseMacAclEntry)
{
    UNUSED_PARAM (pCapwapGetCapwapBaseMacAclEntry);
    UNUSED_PARAM (pCapwapdsCapwapBaseMacAclEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetAllUtlCapwapBaseWtpProfileTable
 Input       :  pCapwapGetCapwapBaseWtpProfileEntry
                pCapwapdsCapwapBaseWtpProfileEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetAllUtlCapwapBaseWtpProfileTable (tCapwapCapwapBaseWtpProfileEntry *
                                          pCapwapGetCapwapBaseWtpProfileEntry,
                                          tCapwapCapwapBaseWtpProfileEntry *
                                          pCapwapdsCapwapBaseWtpProfileEntry)
{
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
#ifdef WLC_WANTED
    UINT4               u4WtpIpaddr = 0;
#endif

    CAPWAP_IF_CAP_DB_ALLOC (pWssIfCapwapDB);
    if (pWssIfCapwapDB == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
#ifdef WTP_WANTED
    pWssIfCapwapDB->CapwapGetDB.u2ProfileId =
        pCapwapGetCapwapBaseWtpProfileEntry->MibObject.u4CapwapBaseWtpProfileId;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpFallbackEnable = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpIdleTimeout = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpMaxDiscoveryInterval = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpEchoInterval = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpLocation = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpStatisticsTimer = OSIX_TRUE;

    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
        == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to Set the Received WtpFallback \r\n");
    }

    pCapwapGetCapwapBaseWtpProfileEntry->MibObject.
        i4CapwapBaseWtpProfileWtpFallbackEnable =
        pWssIfCapwapDB->CapwapGetDB.u1WtpFallbackEnable;

    pCapwapGetCapwapBaseWtpProfileEntry->MibObject.
        u4CapwapBaseWtpProfileWtpIdleTimeout =
        pWssIfCapwapDB->CapwapGetDB.u4WtpIdleTimeout;

    pCapwapGetCapwapBaseWtpProfileEntry->MibObject.
        u4CapwapBaseWtpProfileWtpMaxDiscoveryInterval =
        pWssIfCapwapDB->CapwapGetDB.u2WtpMaxDiscoveryInterval;

    pCapwapGetCapwapBaseWtpProfileEntry->MibObject.
        u4CapwapBaseWtpProfileWtpEchoInterval =
        pWssIfCapwapDB->CapwapGetDB.u2WtpEchoInterval;

    pCapwapGetCapwapBaseWtpProfileEntry->MibObject.
        u4CapwapBaseWtpProfileWtpStatisticsTimer =
        pWssIfCapwapDB->CapwapGetDB.u2WtpStatisticsTimer;

    MEMCPY (pCapwapGetCapwapBaseWtpProfileEntry->MibObject.
            au1CapwapBaseWtpProfileWtpLocation,
            pWssIfCapwapDB->CapwapGetDB.au1WtpLocation,
            sizeof (pWssIfCapwapDB->CapwapGetDB.au1WtpLocation));

    pWssIfCapwapDB->CapwapGetDB.u2WtpProfileId =
        pCapwapGetCapwapBaseWtpProfileEntry->MibObject.u4CapwapBaseWtpProfileId;
    pWssIfCapwapDB->CapwapIsGetAllDB.bIpAddressType = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bIpAddress = OSIX_TRUE;
    if (WssIfProcessCapwapDBMsg
        (WSS_CAPWAP_SHOW_WTPSTATE_TABLE, pWssIfCapwapDB) == OSIX_SUCCESS)
    {
        MEMCPY (pCapwapGetCapwapBaseWtpProfileEntry->MibObject.
                au1CapwapBaseWtpProfileWtpStaticIpAddress,
                pWssIfCapwapDB->CapwapGetDB.WtpStaticIpAddress.u4_addr[0],
                sizeof (pWssIfCapwapDB->CapwapGetDB.
                        WtpStaticIpAddress.u4_addr[0]));
    }

#else

    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpProfileId = OSIX_TRUE;
    pWssIfCapwapDB->CapwapGetDB.u2WtpProfileId =
        (UINT2) (pCapwapGetCapwapBaseWtpProfileEntry->MibObject.
                 u4CapwapBaseWtpProfileId);
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpUpTime = OSIX_TRUE;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
        != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapCliSetWtpProfile : Sending Request to WLC Handler failed \r\n");
        CAPWAP_IF_CAP_DB_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    pWssIfCapwapDB->CapwapIsGetAllDB.bIpAddressType = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bIpAddress = OSIX_TRUE;
    if (WssIfProcessCapwapDBMsg
        (WSS_CAPWAP_SHOW_WTPSTATE_TABLE, pWssIfCapwapDB) == OSIX_SUCCESS)
    {
        MEMCPY (&u4WtpIpaddr,
                &pWssIfCapwapDB->CapwapGetDB.WtpStaticIpAddress.u4_addr[0],
                sizeof (pWssIfCapwapDB->CapwapGetDB.
                        WtpStaticIpAddress.u4_addr[0]));

        u4WtpIpaddr = OSIX_HTONL (u4WtpIpaddr);

        MEMCPY (pCapwapGetCapwapBaseWtpProfileEntry->MibObject.
                au1CapwapBaseWtpProfileWtpStaticIpAddress,
                &u4WtpIpaddr, sizeof (u4WtpIpaddr));

        pCapwapGetCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileWtpStaticIpAddressLen = sizeof (u4WtpIpaddr);
    }

    pCapwapGetCapwapBaseWtpProfileEntry->u4SystemUpTime =
        pWssIfCapwapDB->CapwapGetDB.u4WtpUpTime;
#endif
    UNUSED_PARAM (pCapwapdsCapwapBaseWtpProfileEntry);
    CAPWAP_IF_CAP_DB_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetAllUtlCapwapBaseWtpStateTable
 Input       :  pCapwapGetCapwapBaseWtpStateEntry
                pCapwapdsCapwapBaseWtpStateEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetAllUtlCapwapBaseWtpStateTable (tCapwapCapwapBaseWtpStateEntry *
                                        pCapwapGetCapwapBaseWtpStateEntry,
                                        tCapwapCapwapBaseWtpStateEntry *
                                        pCapwapdsCapwapBaseWtpStateEntry)
{
    UNUSED_PARAM (pCapwapGetCapwapBaseWtpStateEntry);
    UNUSED_PARAM (pCapwapdsCapwapBaseWtpStateEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetAllUtlCapwapBaseWtpTable
 Input       :  pCapwapGetCapwapBaseWtpEntry
                pCapwapdsCapwapBaseWtpEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetAllUtlCapwapBaseWtpTable (tCapwapCapwapBaseWtpEntry *
                                   pCapwapGetCapwapBaseWtpEntry,
                                   tCapwapCapwapBaseWtpEntry *
                                   pCapwapdsCapwapBaseWtpEntry)
{
    UNUSED_PARAM (pCapwapGetCapwapBaseWtpEntry);
    UNUSED_PARAM (pCapwapdsCapwapBaseWtpEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetAllUtlCapwapBaseWirelessBindingTable
 Input       :  pCapwapGetCapwapBaseWirelessBindingEntry
                pCapwapdsCapwapBaseWirelessBindingEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    CapwapGetAllUtlCapwapBaseWirelessBindingTable
    (tCapwapCapwapBaseWirelessBindingEntry *
     pCapwapGetCapwapBaseWirelessBindingEntry,
     tCapwapCapwapBaseWirelessBindingEntry *
     pCapwapdsCapwapBaseWirelessBindingEntry)
{
    UNUSED_PARAM (pCapwapGetCapwapBaseWirelessBindingEntry);
    UNUSED_PARAM (pCapwapdsCapwapBaseWirelessBindingEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetAllUtlCapwapBaseStationTable
 Input       :  pCapwapGetCapwapBaseStationEntry
                pCapwapdsCapwapBaseStationEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetAllUtlCapwapBaseStationTable (tCapwapCapwapBaseStationEntry *
                                       pCapwapGetCapwapBaseStationEntry,
                                       tCapwapCapwapBaseStationEntry *
                                       pCapwapdsCapwapBaseStationEntry)
{
    UNUSED_PARAM (pCapwapGetCapwapBaseStationEntry);
    UNUSED_PARAM (pCapwapdsCapwapBaseStationEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetAllUtlCapwapBaseWtpEventsStatsTable
 Input       :  pCapwapGetCapwapBaseWtpEventsStatsEntry
                pCapwapdsCapwapBaseWtpEventsStatsEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    CapwapGetAllUtlCapwapBaseWtpEventsStatsTable
    (tCapwapCapwapBaseWtpEventsStatsEntry *
     pCapwapGetCapwapBaseWtpEventsStatsEntry,
     tCapwapCapwapBaseWtpEventsStatsEntry *
     pCapwapdsCapwapBaseWtpEventsStatsEntry)
{
    UNUSED_PARAM (pCapwapGetCapwapBaseWtpEventsStatsEntry);
    UNUSED_PARAM (pCapwapdsCapwapBaseWtpEventsStatsEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetAllUtlCapwapBaseRadioEventsStatsTable
 Input       :  pCapwapGetCapwapBaseRadioEventsStatsEntry
                pCapwapdsCapwapBaseRadioEventsStatsEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    CapwapGetAllUtlCapwapBaseRadioEventsStatsTable
    (tCapwapCapwapBaseRadioEventsStatsEntry *
     pCapwapGetCapwapBaseRadioEventsStatsEntry,
     tCapwapCapwapBaseRadioEventsStatsEntry *
     pCapwapdsCapwapBaseRadioEventsStatsEntry)
{
    UNUSED_PARAM (pCapwapGetCapwapBaseRadioEventsStatsEntry);
    UNUSED_PARAM (pCapwapdsCapwapBaseRadioEventsStatsEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  CapwapUtilUpdateCapwapBaseAcNameListTable
 * Input       :   pCapwapOldCapwapBaseAcNameListEntry
                   pCapwapCapwapBaseAcNameListEntry
                   pCapwapIsSetCapwapBaseAcNameListEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapUtilUpdateCapwapBaseAcNameListTable (tCapwapCapwapBaseAcNameListEntry *
                                           pCapwapOldCapwapBaseAcNameListEntry,
                                           tCapwapCapwapBaseAcNameListEntry *
                                           pCapwapCapwapBaseAcNameListEntry,
                                           tCapwapIsSetCapwapBaseAcNameListEntry
                                           *
                                           pCapwapIsSetCapwapBaseAcNameListEntry)
{
    /*setting module DB */
    UNUSED_PARAM (pCapwapOldCapwapBaseAcNameListEntry);
    UNUSED_PARAM (pCapwapIsSetCapwapBaseAcNameListEntry);
    tWssIfCapDB        *pWssIfCapDB = NULL;
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapDB, 0, sizeof (tWssIfCapDB));
#ifdef WLC_WANTED
    UINT1               u1Index = 0;
    INT4                i4RetVal = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4CurrentProfileId = 0;
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;
    pWlcHdlrMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pWlcHdlrMsgStruct == NULL)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapUtilUpdateCapwapBaseAcNameListTable:- "
                    "UtlShMemAllocWlcBuf returned failure\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
        return OSIX_FAILURE;
    }

    MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));
#endif

    pWssIfCapDB->CapwapIsGetAllDB.bAcNamewithPriority = OSIX_TRUE;
    pWssIfCapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;

    /* Check whether the Rowstatus is Create or Destroy */
    /* In the case of Create, copy the Name to the DB, 
     * In the case of Destroy , we need to memset the varaible to 
     * zero, to differentiate between create and destroy in WTP */
    if (pCapwapCapwapBaseAcNameListEntry->MibObject.
        i4CapwapBaseAcNameListRowStatus == 1)
    {
        /* AC Name with Priority */
        MEMCPY (pWssIfCapDB->CapwapGetDB.
                AcNameWithPri[pCapwapCapwapBaseAcNameListEntry->MibObject.
                              u4CapwapBaseAcNameListPriority - 1].wlcName,
                pCapwapCapwapBaseAcNameListEntry->MibObject.
                au1CapwapBaseAcNameListName,
                STRLEN (pCapwapCapwapBaseAcNameListEntry->MibObject.
                        au1CapwapBaseAcNameListName));
    }
    else if (pCapwapCapwapBaseAcNameListEntry->MibObject.
             i4CapwapBaseAcNameListRowStatus == DESTROY)
    {
        MEMSET (&pWssIfCapDB->CapwapGetDB.
                AcNameWithPri[pCapwapCapwapBaseAcNameListEntry->MibObject.
                              u4CapwapBaseAcNameListPriority - 1].wlcName[0], 0,
                512);
    }

    pWssIfCapDB->CapwapGetDB.AcNameWithPri[pCapwapCapwapBaseAcNameListEntry->
                                           MibObject.
                                           u4CapwapBaseAcNameListPriority -
                                           1].u1Priority =
        (UINT1) (pCapwapCapwapBaseAcNameListEntry->MibObject.
                 u4CapwapBaseAcNameListPriority);

#ifdef WLC_WANTED
    /* Pass the information to WTP */
    if ((pCapwapCapwapBaseAcNameListEntry->MibObject.
         i4CapwapBaseAcNameListRowStatus == ACTIVE) ||
        (pCapwapCapwapBaseAcNameListEntry->MibObject.
         i4CapwapBaseAcNameListRowStatus == DESTROY))
    {

        for (u1Index = 0; u1Index < 3; u1Index++)
        {
            if (pWssIfCapDB->CapwapGetDB.AcNameWithPri[u1Index].u1Priority !=
                OSIX_FALSE)
            {
                pWlcHdlrMsgStruct->CapwapConfigUpdateReq.
                    acNameWithPrio[u1Index].isOptional = OSIX_TRUE;
                pWlcHdlrMsgStruct->CapwapConfigUpdateReq.
                    acNameWithPrio[u1Index].u2MsgEleType =
                    CAPWAP_AC_NAME_WITH_PRIORITTY_MSG_TYPE;
                pWlcHdlrMsgStruct->CapwapConfigUpdateReq.
                    acNameWithPrio[u1Index].u2MsgEleLen =
                    (UINT2) (STRLEN
                             (pWssIfCapDB->CapwapGetDB.AcNameWithPri[u1Index].
                              wlcName) + 2);

                pWlcHdlrMsgStruct->CapwapConfigUpdateReq.
                    acNameWithPrio[u1Index].u1Priority =
                    pWssIfCapDB->CapwapGetDB.AcNameWithPri[u1Index].u1Priority;
                MEMCPY (pWlcHdlrMsgStruct->CapwapConfigUpdateReq.
                        acNameWithPrio[u1Index].wlcName,
                        pWssIfCapDB->CapwapGetDB.AcNameWithPri[u1Index].wlcName,
                        STRLEN (pWssIfCapDB->CapwapGetDB.AcNameWithPri[u1Index].
                                wlcName));
            }
        }

        if (nmhGetFirstIndexCapwapBaseWtpProfileTable (&u4WtpProfileId)
            == SNMP_SUCCESS)
        {
            do
            {
                u4CurrentProfileId = u4WtpProfileId;

                pWssIfCapDB->CapwapGetDB.u2ProfileId =
                    (UINT2) u4CurrentProfileId;
                pWssIfCapDB->CapwapIsGetAllDB.bWtpProfileId = OSIX_TRUE;

                if (WssIfProcessCapwapDBMsg
                    (WSS_CAPWAP_GET_INTERNAL_ID, pWssIfCapDB) != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "CapwapCliSetWtpProfile : Failed to get the Internal Id\r\n");

                    continue;
                }

                pWlcHdlrMsgStruct->CapwapConfigUpdateReq.u2InternalId =
                    pWssIfCapDB->CapwapGetDB.u2WtpInternalId;

                i4RetVal =
                    WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_CAPWAP_CONF_UPDATE_REQ,
                                            pWlcHdlrMsgStruct);
                if (i4RetVal == CAPWAP_NO_AP_PRESENT)
                {
                    gu1IsConfigResponseReceived = CAPWAP_NO_AP_PRESENT;
                }
                else if (i4RetVal != OSIX_SUCCESS)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "CapwapCliSetWtpProfile : Sending Request to WLC Handler failed \r\n");
                    continue;

                }
                if ((gu1IsConfigResponseReceived == CAPWAP_RESPONSE_TIMEOUT)
                    || (gu1IsConfigResponseReceived == OSIX_FAILURE))
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "CapwapCliSetWtpProfile : Negative response received\r\n");
                    continue;
                }

            }
            while (nmhGetNextIndexCapwapBaseWtpProfileTable (u4CurrentProfileId,
                                                             &u4WtpProfileId));
        }
    }

    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_SET_DB, pWssIfCapDB) !=
        OSIX_SUCCESS)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
#ifdef WLCHDLR_WANTED
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
#endif
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapSetWtpProfile :CAPWAP DB access failed. \r\n");
        return OSIX_FAILURE;
    }

    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
#endif
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  CapwapUtilUpdateCapwapBaseMacAclTable
 * Input       :   pCapwapOldCapwapBaseMacAclEntry
                   pCapwapCapwapBaseMacAclEntry
                   pCapwapIsSetCapwapBaseMacAclEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapUtilUpdateCapwapBaseMacAclTable (tCapwapCapwapBaseMacAclEntry *
                                       pCapwapOldCapwapBaseMacAclEntry,
                                       tCapwapCapwapBaseMacAclEntry *
                                       pCapwapCapwapBaseMacAclEntry,
                                       tCapwapIsSetCapwapBaseMacAclEntry *
                                       pCapwapIsSetCapwapBaseMacAclEntry)
{

    UNUSED_PARAM (pCapwapOldCapwapBaseMacAclEntry);
    UNUSED_PARAM (pCapwapCapwapBaseMacAclEntry);
    UNUSED_PARAM (pCapwapIsSetCapwapBaseMacAclEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  CapwapUtilUpdateCapwapBaseWtpProfileTable
 * Input       :   pCapwapOldCapwapBaseWtpProfileEntry
                   pCapwapCapwapBaseWtpProfileEntry
                   pCapwapIsSetCapwapBaseWtpProfileEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapUtilUpdateCapwapBaseWtpProfileTable (tCapwapCapwapBaseWtpProfileEntry *
                                           pCapwapOldCapwapBaseWtpProfileEntry,
                                           tCapwapCapwapBaseWtpProfileEntry *
                                           pCapwapCapwapBaseWtpProfileEntry,
                                           tCapwapIsSetCapwapBaseWtpProfileEntry
                                           *
                                           pCapwapIsSetCapwapBaseWtpProfileEntry)
{

    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpMacAddress != OSIX_FALSE)
    {
        if (pCapwapCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileWtpMacAddressLen < 15
            || pCapwapCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileWtpMacAddressLen > 18)
        {
            return OSIX_FAILURE;
        }
    }

    if ((pCapwapCapwapBaseWtpProfileEntry->MibObject.
         i4CapwapBaseWtpProfileRowStatus == NOT_READY))
    {
        pCapwapIsSetCapwapBaseWtpProfileEntry->
            bCapwapBaseWtpProfileWtpIdleTimeout = OSIX_TRUE;
    }

    if (CapwapCliSetWtpProfile (pCapwapCapwapBaseWtpProfileEntry,
                                pCapwapIsSetCapwapBaseWtpProfileEntry) !=
        OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    UNUSED_PARAM (pCapwapOldCapwapBaseWtpProfileEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetAllUtlFsWtpModelTable
 Input       :  pCapwapGetFsWtpModelEntry
                pCapwapdsFsWtpModelEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetAllUtlFsWtpModelTable (tCapwapFsWtpModelEntry *
                                pCapwapGetFsWtpModelEntry,
                                tCapwapFsWtpModelEntry *
                                pCapwapdsFsWtpModelEntry)
{
    UNUSED_PARAM (pCapwapGetFsWtpModelEntry);
    UNUSED_PARAM (pCapwapdsFsWtpModelEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetAllUtlFsWtpRadioTable
 Input       :  pCapwapGetFsWtpRadioEntry
                pCapwapdsFsWtpRadioEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetAllUtlFsWtpRadioTable (tCapwapFsWtpRadioEntry *
                                pCapwapGetFsWtpRadioEntry,
                                tCapwapFsWtpRadioEntry *
                                pCapwapdsFsWtpRadioEntry)
{
    UNUSED_PARAM (pCapwapGetFsWtpRadioEntry);
    UNUSED_PARAM (pCapwapdsFsWtpRadioEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetAllUtlFsCapwapWhiteListTable
 Input       :  pCapwapGetFsCapwapWhiteListEntry
                pCapwapdsFsCapwapWhiteListEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetAllUtlFsCapwapWhiteListTable (tCapwapFsCapwapWhiteListEntry *
                                       pCapwapGetFsCapwapWhiteListEntry,
                                       tCapwapFsCapwapWhiteListEntry *
                                       pCapwapdsFsCapwapWhiteListEntry)
{
    UNUSED_PARAM (pCapwapGetFsCapwapWhiteListEntry);
    UNUSED_PARAM (pCapwapdsFsCapwapWhiteListEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetAllUtlFsCapwapBlackList
 Input       :  pCapwapGetFsCapwapBlackListEntry
                pCapwapdsFsCapwapBlackListEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetAllUtlFsCapwapBlackList (tCapwapFsCapwapBlackListEntry *
                                  pCapwapGetFsCapwapBlackListEntry,
                                  tCapwapFsCapwapBlackListEntry *
                                  pCapwapdsFsCapwapBlackListEntry)
{
    UNUSED_PARAM (pCapwapGetFsCapwapBlackListEntry);
    UNUSED_PARAM (pCapwapdsFsCapwapBlackListEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetAllUtlFsCapwapWtpConfigTable
 Input       :  pCapwapGetFsCapwapWtpConfigEntry
                pCapwapdsFsCapwapWtpConfigEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetAllUtlFsCapwapWtpConfigTable (tCapwapFsCapwapWtpConfigEntry *
                                       pCapwapGetFsCapwapWtpConfigEntry,
                                       tCapwapFsCapwapWtpConfigEntry *
                                       pCapwapdsFsCapwapWtpConfigEntry)
{
    tWssIfCapDB        *pWssIfCapDB = NULL;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapDB, 0, sizeof (tWssIfCapDB));
#ifdef WTP_WANTED
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    RadioIfGetDB.RadioIfGetAllDB.u1RadioId = CAPWAP_DEFAULT_RADIO_ID;
    RadioIfGetDB.RadioIfIsGetAllDB.bCountryString = OSIX_TRUE;

    if (WssIfProcessRadioIfDBMsg (WSS_GET_PHY_RADIO_IF_DB, &RadioIfGetDB)
        != OSIX_SUCCESS)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
        return OSIX_FAILURE;
    }
    MEMCPY (pCapwapGetFsCapwapWtpConfigEntry->MibObject.au1FsWtpCountryString,
            RadioIfGetDB.RadioIfGetAllDB.au1CountryString,
            sizeof (RadioIfGetDB.RadioIfGetAllDB.au1CountryString));
    pCapwapGetFsCapwapWtpConfigEntry->MibObject.i4FsWtpCountryStringLen =
        sizeof (RadioIfGetDB.RadioIfGetAllDB.au1CountryString);

    pWssIfCapDB->CapwapIsGetAllDB.bWtpDiscType = OSIX_TRUE;
    pWssIfCapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;

    pWssIfCapDB->CapwapGetDB.u2WtpInternalId = 0;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapDB)
        != OSIX_SUCCESS)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
        return OSIX_FAILURE;
    }

    pCapwapGetFsCapwapWtpConfigEntry->MibObject.i4FsWtpDiscoveryType =
        pWssIfCapDB->CapwapGetDB.u1WtpDiscType;
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
#else
    pWssIfCapDB->CapwapGetDB.u2ProfileId =
        (UINT2) (pCapwapGetFsCapwapWtpConfigEntry->MibObject.
                 u4CapwapBaseWtpProfileId);
    pWssIfCapDB->CapwapIsGetAllDB.bWtpDiscType = OSIX_TRUE;
    pWssIfCapDB->CapwapIsGetAllDB.bWtpProfileId = OSIX_TRUE;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapDB)
        != OSIX_SUCCESS)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
        return OSIX_FAILURE;
    }

    pCapwapGetFsCapwapWtpConfigEntry->MibObject.i4FsWtpDiscoveryType =
        pWssIfCapDB->CapwapGetDB.u1WtpDiscType;
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
#endif
    UNUSED_PARAM (pCapwapdsFsCapwapWtpConfigEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetAllUtlFsCapwapLinkEncryptionTable
 Input       :  pCapwapGetFsCapwapLinkEncryptionEntry
                pCapwapdsFsCapwapLinkEncryptionEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetAllUtlFsCapwapLinkEncryptionTable (tCapwapFsCapwapLinkEncryptionEntry *
                                            pCapwapGetFsCapwapLinkEncryptionEntry,
                                            tCapwapFsCapwapLinkEncryptionEntry *
                                            pCapwapdsFsCapwapLinkEncryptionEntry)
{
    tWssIfCapDB        *pCapwapGetDB = NULL;

    UNUSED_PARAM (pCapwapdsFsCapwapLinkEncryptionEntry);

    WSS_IF_DB_TEMP_MEM_ALLOC (pCapwapGetDB, OSIX_FAILURE)
        MEMSET (pCapwapGetDB, 0, sizeof (tWssIfCapDB));

    pCapwapGetDB->CapwapIsGetAllDB.bWtpProfileId = OSIX_TRUE;
    pCapwapGetDB->CapwapGetDB.u2WtpProfileId =
        (UINT2) (pCapwapGetFsCapwapLinkEncryptionEntry->MibObject.
                 u4CapwapBaseWtpProfileId);
    pCapwapGetDB->CapwapGetDB.u2ProfileId =
        (UINT2) (pCapwapGetFsCapwapLinkEncryptionEntry->MibObject.
                 u4CapwapBaseWtpProfileId);

    if (pCapwapGetFsCapwapLinkEncryptionEntry->MibObject.
        i4FsCapwapEncryptChannel == CLI_FSCAPWAP_ENCRYPTION_CONTROL)
    {
        pCapwapGetDB->CapwapIsGetAllDB.bWtpCtrlDTLSStatus = OSIX_TRUE;
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pCapwapGetDB) ==
            OSIX_SUCCESS)
        {
            if (pCapwapGetDB->CapwapGetDB.u1WtpCtrlDTLSStatus != 0)
            {
                pCapwapGetFsCapwapLinkEncryptionEntry->MibObject.
                    i4FsCapwapEncryptChannelStatus =
                    pCapwapGetDB->CapwapGetDB.u1WtpCtrlDTLSStatus;
            }
        }
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pCapwapGetDB);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetAllUtlFsCawapDefaultWtpProfileTable
 Input       :  pCapwapGetFsCapwapDefaultWtpProfileEntry
                pCapwapdsFsCapwapDefaultWtpProfileEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    CapwapGetAllUtlFsCawapDefaultWtpProfileTable
    (tCapwapFsCapwapDefaultWtpProfileEntry *
     pCapwapGetFsCapwapDefaultWtpProfileEntry,
     tCapwapFsCapwapDefaultWtpProfileEntry *
     pCapwapdsFsCapwapDefaultWtpProfileEntry)
{
    UNUSED_PARAM (pCapwapGetFsCapwapDefaultWtpProfileEntry);
    UNUSED_PARAM (pCapwapdsFsCapwapDefaultWtpProfileEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetAllUtlFsCapwapDnsProfileTable
 Input       :  pCapwapGetFsCapwapDnsProfileEntry
                pCapwapdsFsCapwapDnsProfileEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetAllUtlFsCapwapDnsProfileTable (tCapwapFsCapwapDnsProfileEntry *
                                        pCapwapGetFsCapwapDnsProfileEntry,
                                        tCapwapFsCapwapDnsProfileEntry *
                                        pCapwapdsFsCapwapDnsProfileEntry)
{
    UNUSED_PARAM (pCapwapGetFsCapwapDnsProfileEntry);
    UNUSED_PARAM (pCapwapdsFsCapwapDnsProfileEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetAllUtlFsWtpNativeVlanIdTable
 Input       :  pCapwapGetFsWtpNativeVlanIdTable
                pCapwapdsFsWtpNativeVlanIdTable
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetAllUtlFsWtpNativeVlanIdTable (tCapwapFsWtpNativeVlanIdTable *
                                       pCapwapGetFsWtpNativeVlanIdTable,
                                       tCapwapFsWtpNativeVlanIdTable *
                                       pCapwapdsFsWtpNativeVlanIdTable)
{
    UNUSED_PARAM (pCapwapGetFsWtpNativeVlanIdTable);
    UNUSED_PARAM (pCapwapdsFsWtpNativeVlanIdTable);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetAllUtlFsWtpLocalRoutingTable
 Input       :  pCapwapGetFsWtpLocalRoutingTable
                pCapwapdsFsWtpLocalRoutingTable
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetAllUtlFsWtpLocalRoutingTable (tCapwapFsWtpLocalRoutingTable *
                                       pCapwapGetFsWtpLocalRoutingTable,
                                       tCapwapFsWtpLocalRoutingTable *
                                       pCapwapdsFsWtpLocalRoutingTable)
{
    UNUSED_PARAM (pCapwapGetFsWtpLocalRoutingTable);
    UNUSED_PARAM (pCapwapdsFsWtpLocalRoutingTable);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetAllUtlFsCawapDiscStatsTable
 Input       :  pCapwapGetFsCawapDiscStatsEntry
                pCapwapdsFsCawapDiscStatsEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetAllUtlFsCawapDiscStatsTable (tCapwapFsCawapDiscStatsEntry *
                                      pCapwapGetFsCawapDiscStatsEntry,
                                      tCapwapFsCawapDiscStatsEntry *
                                      pCapwapdsFsCawapDiscStatsEntry)
{
    UNUSED_PARAM (pCapwapGetFsCawapDiscStatsEntry);
    UNUSED_PARAM (pCapwapdsFsCawapDiscStatsEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetAllUtlFsCawapJoinStatsTable
 Input       :  pCapwapGetFsCawapJoinStatsEntry
                pCapwapdsFsCawapJoinStatsEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetAllUtlFsCawapJoinStatsTable (tCapwapFsCawapJoinStatsEntry *
                                      pCapwapGetFsCawapJoinStatsEntry,
                                      tCapwapFsCawapJoinStatsEntry *
                                      pCapwapdsFsCawapJoinStatsEntry)
{
    UNUSED_PARAM (pCapwapGetFsCawapJoinStatsEntry);
    UNUSED_PARAM (pCapwapdsFsCawapJoinStatsEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetAllUtlFsCawapConfigStatsTable
 Input       :  pCapwapGetFsCawapConfigStatsEntry
                pCapwapdsFsCawapConfigStatsEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetAllUtlFsCawapConfigStatsTable (tCapwapFsCawapConfigStatsEntry *
                                        pCapwapGetFsCawapConfigStatsEntry,
                                        tCapwapFsCawapConfigStatsEntry *
                                        pCapwapdsFsCawapConfigStatsEntry)
{
    UNUSED_PARAM (pCapwapGetFsCawapConfigStatsEntry);
    UNUSED_PARAM (pCapwapdsFsCawapConfigStatsEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetAllUtlFsCawapRunStatsTable
 Input       :  pCapwapGetFsCawapRunStatsEntry
                pCapwapdsFsCawapRunStatsEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetAllUtlFsCawapRunStatsTable (tCapwapFsCawapRunStatsEntry *
                                     pCapwapGetFsCawapRunStatsEntry,
                                     tCapwapFsCawapRunStatsEntry *
                                     pCapwapdsFsCawapRunStatsEntry)
{
    tWssPmWTPRunEvents *pWssPmWTPRunEvents = NULL;
    tWssIfCapDB        *pWssIfCapDB = NULL;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapDB, 0, sizeof (tWssIfCapDB));

    pWssIfCapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    pWssIfCapDB->CapwapIsGetAllDB.bWtpProfileId = OSIX_TRUE;
    pWssIfCapDB->CapwapGetDB.u2ProfileId =
        (UINT2) (pCapwapGetFsCawapRunStatsEntry->MibObject.
                 u4CapwapBaseWtpProfileId);
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_INTERNAL_ID, pWssIfCapDB) !=
        OSIX_SUCCESS)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
        return OSIX_FAILURE;
    }

    pWssPmWTPRunEvents =
        WssPmWTPRunEventsEntryGet (pWssIfCapDB->CapwapGetDB.u2WtpInternalId);

    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);

    if (pWssPmWTPRunEvents == NULL)
    {
        return OSIX_FAILURE;
    }

    pCapwapGetFsCawapRunStatsEntry->MibObject.
        u4FsCapwapRunConfigUpdateReqReceived =
        pWssPmWTPRunEvents->ConfigUpdateStats.u4RunUpdateReqReceived;

    pCapwapGetFsCawapRunStatsEntry->MibObject.
        u4FsCapwapRunConfigUpdateRspReceived =
        pWssPmWTPRunEvents->ConfigUpdateStats.u4RunUpdateRspReceived;

    pCapwapGetFsCawapRunStatsEntry->MibObject.
        u4FsCapwapRunConfigUpdateReqTransmitted =
        pWssPmWTPRunEvents->ConfigUpdateStats.u4RunUpdateReqTransmitted;

    pCapwapGetFsCawapRunStatsEntry->MibObject.
        u4FsCapwapRunConfigUpdateRspTransmitted =
        pWssPmWTPRunEvents->ConfigUpdateStats.u4RunUpdateRspTransmitted;

    pCapwapGetFsCawapRunStatsEntry->MibObject.
        u4FsCapwapRunConfigUpdateunsuccessfulProcessed =
        pWssPmWTPRunEvents->ConfigUpdateStats.u4RunUpdateunsuccessfulProcessed;

    pCapwapGetFsCawapRunStatsEntry->MibObject.
        u4FsCapwapRunStationConfigReqReceived =
        pWssPmWTPRunEvents->StaConfigStats.u4RunUpdateReqReceived;
    pCapwapGetFsCawapRunStatsEntry->MibObject.
        u4FsCapwapRunStationConfigRspReceived =
        pWssPmWTPRunEvents->StaConfigStats.u4RunUpdateRspReceived;
    pCapwapGetFsCawapRunStatsEntry->MibObject.
        u4FsCapwapRunStationConfigReqTransmitted =
        pWssPmWTPRunEvents->StaConfigStats.u4RunUpdateReqTransmitted;
    pCapwapGetFsCawapRunStatsEntry->MibObject.
        u4FsCapwapRunStationConfigRspTransmitted =
        pWssPmWTPRunEvents->StaConfigStats.u4RunUpdateRspTransmitted;
    pCapwapGetFsCawapRunStatsEntry->MibObject.
        u4FsCapwapRunStationConfigunsuccessfulProcessed =
        pWssPmWTPRunEvents->StaConfigStats.u4RunUpdateunsuccessfulProcessed;

    pCapwapGetFsCawapRunStatsEntry->MibObject.
        u4FsCapwapRunClearConfigReqReceived =
        pWssPmWTPRunEvents->ClearConfigStats.u4RunUpdateReqReceived;
    pCapwapGetFsCawapRunStatsEntry->MibObject.
        u4FsCapwapRunClearConfigRspReceived =
        pWssPmWTPRunEvents->ClearConfigStats.u4RunUpdateRspReceived;
    pCapwapGetFsCawapRunStatsEntry->MibObject.
        u4FsCapwapRunClearConfigReqTransmitted =
        pWssPmWTPRunEvents->ClearConfigStats.u4RunUpdateReqTransmitted;
    pCapwapGetFsCawapRunStatsEntry->MibObject.
        u4FsCapwapRunClearConfigRspTransmitted =
        pWssPmWTPRunEvents->ClearConfigStats.u4RunUpdateRspTransmitted;
    pCapwapGetFsCawapRunStatsEntry->MibObject.
        u4FsCapwapRunClearConfigunsuccessfulProcessed =
        pWssPmWTPRunEvents->ClearConfigStats.u4RunUpdateunsuccessfulProcessed;

    pCapwapGetFsCawapRunStatsEntry->MibObject.
        u4FsCapwapRunDataTransferReqReceived =
        pWssPmWTPRunEvents->DataTransferStats.u4RunUpdateReqReceived;
    pCapwapGetFsCawapRunStatsEntry->MibObject.
        u4FsCapwapRunDataTransferRspReceived =
        pWssPmWTPRunEvents->DataTransferStats.u4RunUpdateRspReceived;
    pCapwapGetFsCawapRunStatsEntry->MibObject.
        u4FsCapwapRunDataTransferReqTransmitted =
        pWssPmWTPRunEvents->DataTransferStats.u4RunUpdateReqTransmitted;
    pCapwapGetFsCawapRunStatsEntry->MibObject.
        u4FsCapwapRunDataTransferRspTransmitted =
        pWssPmWTPRunEvents->DataTransferStats.u4RunUpdateRspTransmitted;
    pCapwapGetFsCawapRunStatsEntry->MibObject.
        u4FsCapwapRunDataTransferunsuccessfulProcessed =
        pWssPmWTPRunEvents->DataTransferStats.u4RunUpdateunsuccessfulProcessed;

    pCapwapGetFsCawapRunStatsEntry->MibObject.u4FsCapwapRunResetReqReceived =
        pWssPmWTPRunEvents->ResetStats.u4RunUpdateReqReceived;
    pCapwapGetFsCawapRunStatsEntry->MibObject.u4FsCapwapRunResetRspReceived =
        pWssPmWTPRunEvents->ResetStats.u4RunUpdateRspReceived;
    pCapwapGetFsCawapRunStatsEntry->MibObject.u4FsCapwapRunResetReqTransmitted =
        pWssPmWTPRunEvents->ResetStats.u4RunUpdateReqTransmitted;
    pCapwapGetFsCawapRunStatsEntry->MibObject.u4FsCapwapRunResetRspTransmitted =
        pWssPmWTPRunEvents->ResetStats.u4RunUpdateRspTransmitted;
    pCapwapGetFsCawapRunStatsEntry->MibObject.
        u4FsCapwapRunResetunsuccessfulProcessed =
        pWssPmWTPRunEvents->ResetStats.u4RunUpdateunsuccessfulProcessed;

    pCapwapGetFsCawapRunStatsEntry->MibObject.u4FsCapwapRunPriDiscReqReceived =
        pWssPmWTPRunEvents->PrimaryDiscStats.u4RunUpdateReqReceived;
    pCapwapGetFsCawapRunStatsEntry->MibObject.u4FsCapwapRunPriDiscRspReceived =
        pWssPmWTPRunEvents->PrimaryDiscStats.u4RunUpdateRspReceived;
    pCapwapGetFsCawapRunStatsEntry->MibObject.
        u4FsCapwapRunPriDiscReqTransmitted =
        pWssPmWTPRunEvents->PrimaryDiscStats.u4RunUpdateReqTransmitted;
    pCapwapGetFsCawapRunStatsEntry->MibObject.
        u4FsCapwapRunPriDiscRspTransmitted =
        pWssPmWTPRunEvents->PrimaryDiscStats.u4RunUpdateRspTransmitted;
    pCapwapGetFsCawapRunStatsEntry->MibObject.
        u4FsCapwapRunPriDiscunsuccessfulProcessed =
        pWssPmWTPRunEvents->PrimaryDiscStats.u4RunUpdateunsuccessfulProcessed;

    pCapwapGetFsCawapRunStatsEntry->MibObject.u4FsCapwapRunEchoReqReceived =
        pWssPmWTPRunEvents->EchoStats.u4RunUpdateReqReceived;
    pCapwapGetFsCawapRunStatsEntry->MibObject.u4FsCapwapRunEchoRspReceived =
        pWssPmWTPRunEvents->EchoStats.u4RunUpdateRspReceived;
    pCapwapGetFsCawapRunStatsEntry->MibObject.u4FsCapwapRunEchoReqTransmitted =
        pWssPmWTPRunEvents->EchoStats.u4RunUpdateReqTransmitted;
    pCapwapGetFsCawapRunStatsEntry->MibObject.u4FsCapwapRunEchoRspTransmitted =
        pWssPmWTPRunEvents->EchoStats.u4RunUpdateRspTransmitted;
    pCapwapGetFsCawapRunStatsEntry->MibObject.
        u4FsCapwapRunEchounsuccessfulProcessed =
        pWssPmWTPRunEvents->EchoStats.u4RunUpdateunsuccessfulProcessed;

    pCapwapGetFsCawapRunStatsEntry->MibObject.i4FsCapwapRunStatsRowStatus =
        pWssPmWTPRunEvents->u1RowStatus;

    pCapwapGetFsCawapRunStatsEntry->MibObject.
        i4FsCapwapRunConfigUpdateReasonLastUnsuccAttemptLen =
        STRLEN (pWssPmWTPRunEvents->ConfigUpdateStats.
                au1RunUpdateReasonLastUnsuccAttempt);
    pCapwapGetFsCawapRunStatsEntry->MibObject.
        i4FsCapwapRunConfigUpdateLastSuccAttemptTimeLen =
        STRLEN (pWssPmWTPRunEvents->ConfigUpdateStats.
                au1RunUpdateLastSuccAttemptTime);
    pCapwapGetFsCawapRunStatsEntry->MibObject.
        i4FsCapwapRunConfigUpdateLastUnsuccessfulAttemptTimeLen =
        STRLEN (pWssPmWTPRunEvents->ConfigUpdateStats.
                au1RunUpdateLastUnsuccessfulAttemptTime);

    pCapwapGetFsCawapRunStatsEntry->MibObject.
        i4FsCapwapRunStationConfigReasonLastUnsuccAttemptLen =
        STRLEN (pWssPmWTPRunEvents->StaConfigStats.
                au1RunUpdateReasonLastUnsuccAttempt);
    pCapwapGetFsCawapRunStatsEntry->MibObject.
        i4FsCapwapRunStationConfigLastSuccAttemptTimeLen =
        STRLEN (pWssPmWTPRunEvents->StaConfigStats.
                au1RunUpdateLastSuccAttemptTime);
    pCapwapGetFsCawapRunStatsEntry->MibObject.
        i4FsCapwapRunStationConfigLastUnsuccessfulAttemptTimeLen =
        STRLEN (pWssPmWTPRunEvents->StaConfigStats.
                au1RunUpdateLastUnsuccessfulAttemptTime);

    pCapwapGetFsCawapRunStatsEntry->MibObject.
        i4FsCapwapRunClearConfigReasonLastUnsuccAttemptLen =
        STRLEN (pWssPmWTPRunEvents->ClearConfigStats.
                au1RunUpdateReasonLastUnsuccAttempt);
    pCapwapGetFsCawapRunStatsEntry->MibObject.
        i4FsCapwapRunClearConfigLastSuccAttemptTimeLen =
        STRLEN (pWssPmWTPRunEvents->ClearConfigStats.
                au1RunUpdateLastSuccAttemptTime);
    pCapwapGetFsCawapRunStatsEntry->MibObject.
        i4FsCapwapRunClearConfigLastUnsuccessfulAttemptTimeLen =
        STRLEN (pWssPmWTPRunEvents->ClearConfigStats.
                au1RunUpdateLastUnsuccessfulAttemptTime);

    pCapwapGetFsCawapRunStatsEntry->MibObject.
        i4FsCapwapRunDataTransferReasonLastUnsuccAttemptLen =
        STRLEN (pWssPmWTPRunEvents->DataTransferStats.
                au1RunUpdateReasonLastUnsuccAttempt);
    pCapwapGetFsCawapRunStatsEntry->MibObject.
        i4FsCapwapRunDataTransferLastSuccAttemptTimeLen =
        STRLEN (pWssPmWTPRunEvents->DataTransferStats.
                au1RunUpdateLastSuccAttemptTime);
    pCapwapGetFsCawapRunStatsEntry->MibObject.
        i4FsCapwapRunDataTransferLastUnsuccessfulAttemptTimeLen =
        STRLEN (pWssPmWTPRunEvents->DataTransferStats.
                au1RunUpdateLastUnsuccessfulAttemptTime);

    pCapwapGetFsCawapRunStatsEntry->MibObject.
        i4FsCapwapRunResetReasonLastUnsuccAttemptLen =
        STRLEN (pWssPmWTPRunEvents->ResetStats.
                au1RunUpdateReasonLastUnsuccAttempt);
    pCapwapGetFsCawapRunStatsEntry->MibObject.
        i4FsCapwapRunResetLastSuccAttemptTimeLen =
        STRLEN (pWssPmWTPRunEvents->ResetStats.au1RunUpdateLastSuccAttemptTime);
    pCapwapGetFsCawapRunStatsEntry->MibObject.
        i4FsCapwapRunResetLastUnsuccessfulAttemptTimeLen =
        STRLEN (pWssPmWTPRunEvents->ResetStats.
                au1RunUpdateLastUnsuccessfulAttemptTime);

    pCapwapGetFsCawapRunStatsEntry->MibObject.
        i4FsCapwapRunPriDiscReasonLastUnsuccAttemptLen =
        STRLEN (pWssPmWTPRunEvents->PrimaryDiscStats.
                au1RunUpdateReasonLastUnsuccAttempt);
    pCapwapGetFsCawapRunStatsEntry->MibObject.
        i4FsCapwapRunPriDiscLastSuccAttemptTimeLen =
        STRLEN (pWssPmWTPRunEvents->PrimaryDiscStats.
                au1RunUpdateLastSuccAttemptTime);
    pCapwapGetFsCawapRunStatsEntry->MibObject.
        i4FsCapwapRunPriDiscLastUnsuccessfulAttemptTimeLen =
        STRLEN (pWssPmWTPRunEvents->PrimaryDiscStats.
                au1RunUpdateLastUnsuccessfulAttemptTime);

    pCapwapGetFsCawapRunStatsEntry->MibObject.
        i4FsCapwapRunEchoReasonLastUnsuccAttemptLen =
        STRLEN (pWssPmWTPRunEvents->EchoStats.
                au1RunUpdateReasonLastUnsuccAttempt);
    pCapwapGetFsCawapRunStatsEntry->MibObject.
        i4FsCapwapRunEchoLastSuccAttemptTimeLen =
        STRLEN (pWssPmWTPRunEvents->EchoStats.au1RunUpdateLastSuccAttemptTime);
    pCapwapGetFsCawapRunStatsEntry->MibObject.
        i4FsCapwapRunEchoLastUnsuccessfulAttemptTimeLen =
        STRLEN (pWssPmWTPRunEvents->EchoStats.
                au1RunUpdateLastUnsuccessfulAttemptTime);

    MEMCPY (pCapwapGetFsCawapRunStatsEntry->MibObject.
            au1FsCapwapRunConfigUpdateReasonLastUnsuccAttempt,
            pWssPmWTPRunEvents->ConfigUpdateStats.
            au1RunUpdateReasonLastUnsuccAttempt,
            pCapwapGetFsCawapRunStatsEntry->MibObject.
            i4FsCapwapRunConfigUpdateReasonLastUnsuccAttemptLen);

    MEMCPY (pCapwapGetFsCawapRunStatsEntry->MibObject.
            au1FsCapwapRunConfigUpdateLastSuccAttemptTime,
            pWssPmWTPRunEvents->ConfigUpdateStats.
            au1RunUpdateLastSuccAttemptTime,
            pCapwapGetFsCawapRunStatsEntry->MibObject.
            i4FsCapwapRunConfigUpdateLastSuccAttemptTimeLen);

    MEMCPY (pCapwapGetFsCawapRunStatsEntry->MibObject.
            au1FsCapwapRunConfigUpdateLastUnsuccessfulAttemptTime,
            pWssPmWTPRunEvents->ConfigUpdateStats.
            au1RunUpdateLastUnsuccessfulAttemptTime,
            pCapwapGetFsCawapRunStatsEntry->MibObject.
            i4FsCapwapRunConfigUpdateLastUnsuccessfulAttemptTimeLen);

    MEMCPY (pCapwapGetFsCawapRunStatsEntry->MibObject.
            au1FsCapwapRunStationConfigReasonLastUnsuccAttempt,
            pWssPmWTPRunEvents->StaConfigStats.
            au1RunUpdateReasonLastUnsuccAttempt,
            pCapwapGetFsCawapRunStatsEntry->MibObject.
            i4FsCapwapRunStationConfigReasonLastUnsuccAttemptLen);

    MEMCPY (pCapwapGetFsCawapRunStatsEntry->MibObject.
            au1FsCapwapRunStationConfigLastSuccAttemptTime,
            pWssPmWTPRunEvents->StaConfigStats.au1RunUpdateLastSuccAttemptTime,
            pCapwapGetFsCawapRunStatsEntry->MibObject.
            i4FsCapwapRunStationConfigLastSuccAttemptTimeLen);

    MEMCPY (pCapwapGetFsCawapRunStatsEntry->MibObject.
            au1FsCapwapRunStationConfigLastUnsuccessfulAttemptTime,
            pWssPmWTPRunEvents->StaConfigStats.
            au1RunUpdateLastUnsuccessfulAttemptTime,
            pCapwapGetFsCawapRunStatsEntry->MibObject.
            i4FsCapwapRunStationConfigLastUnsuccessfulAttemptTimeLen);

    MEMCPY (pCapwapGetFsCawapRunStatsEntry->MibObject.
            au1FsCapwapRunClearConfigReasonLastUnsuccAttempt,
            pWssPmWTPRunEvents->ClearConfigStats.
            au1RunUpdateReasonLastUnsuccAttempt,
            pCapwapGetFsCawapRunStatsEntry->MibObject.
            i4FsCapwapRunClearConfigReasonLastUnsuccAttemptLen);

    MEMCPY (pCapwapGetFsCawapRunStatsEntry->MibObject.
            au1FsCapwapRunClearConfigLastSuccAttemptTime,
            pWssPmWTPRunEvents->ClearConfigStats.
            au1RunUpdateLastSuccAttemptTime,
            pCapwapGetFsCawapRunStatsEntry->MibObject.
            i4FsCapwapRunClearConfigLastSuccAttemptTimeLen);

    MEMCPY (pCapwapGetFsCawapRunStatsEntry->MibObject.
            au1FsCapwapRunClearConfigLastUnsuccessfulAttemptTime,
            pWssPmWTPRunEvents->ClearConfigStats.
            au1RunUpdateLastUnsuccessfulAttemptTime,
            pCapwapGetFsCawapRunStatsEntry->MibObject.
            i4FsCapwapRunClearConfigLastUnsuccessfulAttemptTimeLen);

    MEMCPY (pCapwapGetFsCawapRunStatsEntry->MibObject.
            au1FsCapwapRunDataTransferReasonLastUnsuccAttempt,
            pWssPmWTPRunEvents->DataTransferStats.
            au1RunUpdateReasonLastUnsuccAttempt,
            pCapwapGetFsCawapRunStatsEntry->MibObject.
            i4FsCapwapRunDataTransferReasonLastUnsuccAttemptLen);

    MEMCPY (pCapwapGetFsCawapRunStatsEntry->MibObject.
            au1FsCapwapRunDataTransferLastSuccAttemptTime,
            pWssPmWTPRunEvents->DataTransferStats.
            au1RunUpdateLastSuccAttemptTime,
            pCapwapGetFsCawapRunStatsEntry->MibObject.
            i4FsCapwapRunDataTransferLastSuccAttemptTimeLen);

    MEMCPY (pCapwapGetFsCawapRunStatsEntry->MibObject.
            au1FsCapwapRunDataTransferLastUnsuccessfulAttemptTime,
            pWssPmWTPRunEvents->DataTransferStats.
            au1RunUpdateLastUnsuccessfulAttemptTime,
            pCapwapGetFsCawapRunStatsEntry->MibObject.
            i4FsCapwapRunDataTransferLastUnsuccessfulAttemptTimeLen);

    MEMCPY (pCapwapGetFsCawapRunStatsEntry->MibObject.
            au1FsCapwapRunResetReasonLastUnsuccAttempt,
            pWssPmWTPRunEvents->ResetStats.au1RunUpdateReasonLastUnsuccAttempt,
            pCapwapGetFsCawapRunStatsEntry->MibObject.
            i4FsCapwapRunResetReasonLastUnsuccAttemptLen);

    MEMCPY (pCapwapGetFsCawapRunStatsEntry->MibObject.
            au1FsCapwapRunResetLastSuccAttemptTime,
            pWssPmWTPRunEvents->ResetStats.au1RunUpdateLastSuccAttemptTime,
            pCapwapGetFsCawapRunStatsEntry->MibObject.
            i4FsCapwapRunResetLastSuccAttemptTimeLen);

    MEMCPY (pCapwapGetFsCawapRunStatsEntry->MibObject.
            au1FsCapwapRunResetLastUnsuccessfulAttemptTime,
            pWssPmWTPRunEvents->ResetStats.
            au1RunUpdateLastUnsuccessfulAttemptTime,
            pCapwapGetFsCawapRunStatsEntry->MibObject.
            i4FsCapwapRunResetLastUnsuccessfulAttemptTimeLen);

    MEMCPY (pCapwapGetFsCawapRunStatsEntry->MibObject.
            au1FsCapwapRunPriDiscReasonLastUnsuccAttempt,
            pWssPmWTPRunEvents->PrimaryDiscStats.
            au1RunUpdateReasonLastUnsuccAttempt,
            pCapwapGetFsCawapRunStatsEntry->MibObject.
            i4FsCapwapRunPriDiscReasonLastUnsuccAttemptLen);

    MEMCPY (pCapwapGetFsCawapRunStatsEntry->MibObject.
            au1FsCapwapRunPriDiscLastSuccAttemptTime,
            pWssPmWTPRunEvents->PrimaryDiscStats.
            au1RunUpdateLastSuccAttemptTime,
            pCapwapGetFsCawapRunStatsEntry->MibObject.
            i4FsCapwapRunPriDiscLastSuccAttemptTimeLen);

    MEMCPY (pCapwapGetFsCawapRunStatsEntry->MibObject.
            au1FsCapwapRunPriDiscLastUnsuccessfulAttemptTime,
            pWssPmWTPRunEvents->PrimaryDiscStats.
            au1RunUpdateLastUnsuccessfulAttemptTime,
            pCapwapGetFsCawapRunStatsEntry->MibObject.
            i4FsCapwapRunPriDiscLastUnsuccessfulAttemptTimeLen);

    MEMCPY (pCapwapGetFsCawapRunStatsEntry->MibObject.
            au1FsCapwapRunEchoReasonLastUnsuccAttempt,
            pWssPmWTPRunEvents->EchoStats.au1RunUpdateReasonLastUnsuccAttempt,
            pCapwapGetFsCawapRunStatsEntry->MibObject.
            i4FsCapwapRunEchoReasonLastUnsuccAttemptLen);

    MEMCPY (pCapwapGetFsCawapRunStatsEntry->MibObject.
            au1FsCapwapRunEchoLastSuccAttemptTime,
            pWssPmWTPRunEvents->EchoStats.au1RunUpdateLastSuccAttemptTime,
            pCapwapGetFsCawapRunStatsEntry->MibObject.
            i4FsCapwapRunEchoLastSuccAttemptTimeLen);

    MEMCPY (pCapwapGetFsCawapRunStatsEntry->MibObject.
            au1FsCapwapRunEchoLastUnsuccessfulAttemptTime,
            pWssPmWTPRunEvents->EchoStats.
            au1RunUpdateLastUnsuccessfulAttemptTime,
            pCapwapGetFsCawapRunStatsEntry->MibObject.
            i4FsCapwapRunEchoLastUnsuccessfulAttemptTimeLen);

    UNUSED_PARAM (pCapwapdsFsCawapRunStatsEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetAllUtlFsCapwapWirelessBindingTable
 Input       :  pCapwapGetFsCapwapWirelessBindingEntry
                pCapwapdsFsCapwapWirelessBindingEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetAllUtlFsCapwapWirelessBindingTable (tCapwapFsCapwapWirelessBindingEntry
                                             *
                                             pCapwapGetFsCapwapWirelessBindingEntry,
                                             tCapwapFsCapwapWirelessBindingEntry
                                             *
                                             pCapwapdsFsCapwapWirelessBindingEntry)
{
    UNUSED_PARAM (pCapwapGetFsCapwapWirelessBindingEntry);
    UNUSED_PARAM (pCapwapdsFsCapwapWirelessBindingEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetAllUtlFsCapwapStationWhiteList
 Input       :  pCapwapGetFsCapwapStationWhiteListEntry
                pCapwapdsFsCapwapStationWhiteListEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapGetAllUtlFsCapwapStationWhiteList (tCapwapFsCapwapStationWhiteListEntry *
                                         pCapwapGetFsCapwapStationWhiteListEntry,
                                         tCapwapFsCapwapStationWhiteListEntry *
                                         pCapwapdsFsCapwapStationWhiteListEntry)
{
    UNUSED_PARAM (pCapwapGetFsCapwapStationWhiteListEntry);
    UNUSED_PARAM (pCapwapdsFsCapwapStationWhiteListEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetAllUtlFsCapwapWtpRebootStatisticsTable
 Input       :  pCapwapGetFsCapwapWtpRebootStatisticsEntry
                pCapwapdsFsCapwapWtpRebootStatisticsEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    CapwapGetAllUtlFsCapwapWtpRebootStatisticsTable
    (tCapwapFsCapwapWtpRebootStatisticsEntry *
     pCapwapGetFsCapwapWtpRebootStatisticsEntry,
     tCapwapFsCapwapWtpRebootStatisticsEntry *
     pCapwapdsFsCapwapWtpRebootStatisticsEntry)
{
    UNUSED_PARAM (pCapwapdsFsCapwapWtpRebootStatisticsEntry);

#ifdef WLC_WANTED
    tWssPmWTPIDStatsProcessRBDB *pWtpIdStatsMap = NULL;
    UINT2               u2TmpWtpProfiledId, u2WtpIntProfileIndex = 0;
#endif /* WLC_WANTED */

#ifdef WTP_WANTED
    tWssIfApHdlrDB      WssIfApHdlrDB;
#endif /* WTP_WANTED */

#ifdef WLC_WANTED
    u2TmpWtpProfiledId =
        (UINT2) (pCapwapGetFsCapwapWtpRebootStatisticsEntry->MibObject.
                 u4CapwapBaseWtpProfileId);
    u2WtpIntProfileIndex =
        (UINT2) (CapwapGetInternalProfildId
                 (&u2TmpWtpProfiledId, &u2WtpIntProfileIndex));
    pWtpIdStatsMap =
        (tWssPmWTPIDStatsProcessRBDB *)
        WssIfWtpIDStatsEntryGet (u2WtpIntProfileIndex);
    if (NULL != pWtpIdStatsMap)
    {
        /* fill the values got from db */
        pCapwapGetFsCapwapWtpRebootStatisticsEntry->MibObject.
            u4FsCapwapWtpRebootStatisticsRebootCount =
            pWtpIdStatsMap->pWtpRebootStat.u2RebootCount;

        pCapwapGetFsCapwapWtpRebootStatisticsEntry->MibObject.
            u4FsCapwapWtpRebootStatisticsAcInitiatedCount =
            pWtpIdStatsMap->pWtpRebootStat.u2AcInitiatedCount;

        pCapwapGetFsCapwapWtpRebootStatisticsEntry->MibObject.
            u4FsCapwapWtpRebootStatisticsLinkFailureCount =
            pWtpIdStatsMap->pWtpRebootStat.u2LinkFailCount;

        pCapwapGetFsCapwapWtpRebootStatisticsEntry->MibObject.
            u4FsCapwapWtpRebootStatisticsSwFailureCount =
            pWtpIdStatsMap->pWtpRebootStat.u2SwFailCount;

        pCapwapGetFsCapwapWtpRebootStatisticsEntry->MibObject.
            u4FsCapwapWtpRebootStatisticsHwFailureCount =
            pWtpIdStatsMap->pWtpRebootStat.u2HwFailCount;

        pCapwapGetFsCapwapWtpRebootStatisticsEntry->MibObject.
            u4FsCapwapWtpRebootStatisticsOtherFailureCount =
            pWtpIdStatsMap->pWtpRebootStat.u2OtherFailCount;

        pCapwapGetFsCapwapWtpRebootStatisticsEntry->MibObject.
            u4FsCapwapWtpRebootStatisticsUnknownFailureCount =
            pWtpIdStatsMap->pWtpRebootStat.u2UnknownFailCount;

        pCapwapGetFsCapwapWtpRebootStatisticsEntry->MibObject.
            i4FsCapwapWtpRebootStatisticsLastFailureType =
            pWtpIdStatsMap->pWtpRebootStat.u1LastFailureType;
    }
#endif /* WLC_WANTED */

#ifdef WTP_WANTED
    MEMSET (&WssIfApHdlrDB, 0, sizeof (tWssIfApHdlrDB));
    if (WssIfProcessApHdlrDBMsg (WSS_APHDLR_GET_WTPREBOOT_STATS, &WssIfApHdlrDB)
        == OSIX_SUCCESS)
    {
        /* fill the values got from db */
        pCapwapGetFsCapwapWtpRebootStatisticsEntry->MibObject.
            u4FsCapwapWtpRebootStatisticsRebootCount =
            WssIfApHdlrDB.u2RebootCount;

        pCapwapGetFsCapwapWtpRebootStatisticsEntry->MibObject.
            u4FsCapwapWtpRebootStatisticsAcInitiatedCount =
            WssIfApHdlrDB.u2AcInitiatedCount;

        pCapwapGetFsCapwapWtpRebootStatisticsEntry->MibObject.
            u4FsCapwapWtpRebootStatisticsLinkFailureCount =
            WssIfApHdlrDB.u2LinkFailCount;

        pCapwapGetFsCapwapWtpRebootStatisticsEntry->MibObject.
            u4FsCapwapWtpRebootStatisticsSwFailureCount =
            WssIfApHdlrDB.u2SwFailCount;

        pCapwapGetFsCapwapWtpRebootStatisticsEntry->MibObject.
            u4FsCapwapWtpRebootStatisticsHwFailureCount =
            WssIfApHdlrDB.u2HwFailCount;

        pCapwapGetFsCapwapWtpRebootStatisticsEntry->MibObject.
            u4FsCapwapWtpRebootStatisticsOtherFailureCount =
            WssIfApHdlrDB.u2OtherFailCount;

        pCapwapGetFsCapwapWtpRebootStatisticsEntry->MibObject.
            u4FsCapwapWtpRebootStatisticsUnknownFailureCount =
            WssIfApHdlrDB.u2UnknownFailCount;

        pCapwapGetFsCapwapWtpRebootStatisticsEntry->MibObject.
            i4FsCapwapWtpRebootStatisticsLastFailureType =
            WssIfApHdlrDB.u1LastFailureType;
    }
#endif /* WTP_WANTED */

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetAllUtlFsCapwapWtpRadioStatisticsTable
 Input       :  pCapwapGetFsCapwapWtpRadioStatisticsEntry
                pCapwapdsFsCapwapWtpRadioStatisticsEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    CapwapGetAllUtlFsCapwapWtpRadioStatisticsTable
    (tCapwapFsCapwapWtpRadioStatisticsEntry *
     pCapwapGetFsCapwapWtpRadioStatisticsEntry,
     tCapwapFsCapwapWtpRadioStatisticsEntry *
     pCapwapdsFsCapwapWtpRadioStatisticsEntry)
{
    UNUSED_PARAM (pCapwapdsFsCapwapWtpRadioStatisticsEntry);

#ifdef WLC_WANTED
    tWssPmWTPIDRadIDStatsProcessRBDB *pwtpIDRadIDStatsMap = NULL;
    UINT4               u4RadioIfIndex = 0;
#endif /* WLC_WANTED */

#ifdef WTP_WANTED
    tRadioIfGetDB       RadioIfGetDB;
#endif /* WTP_WANTED */

#ifdef WLC_WANTED
    u4RadioIfIndex =
        (UINT4) pCapwapGetFsCapwapWtpRadioStatisticsEntry->MibObject.
        i4RadioIfIndex;
    pwtpIDRadIDStatsMap =
        (tWssPmWTPIDRadIDStatsProcessRBDB *)
        WssIfWtpIDRadIDStatsEntryGet ((UINT2) u4RadioIfIndex);
    if (NULL != pwtpIDRadIDStatsMap)
    {
        /* fill the values got from db */
        pCapwapGetFsCapwapWtpRadioStatisticsEntry->MibObject.
            i4FsCapwapWtpRadioLastFailType =
            pwtpIDRadIDStatsMap->pWtpRadioStats.u1LastFailureType;

        pCapwapGetFsCapwapWtpRadioStatisticsEntry->MibObject.
            u4FsCapwapWtpRadioResetCount =
            pwtpIDRadIDStatsMap->pWtpRadioStats.u2ResetCount;

        pCapwapGetFsCapwapWtpRadioStatisticsEntry->MibObject.
            u4FsCapwapWtpRadioSwFailureCount =
            pwtpIDRadIDStatsMap->pWtpRadioStats.u2SwFailCount;

        pCapwapGetFsCapwapWtpRadioStatisticsEntry->MibObject.
            u4FsCapwapWtpRadioHwFailureCount =
            pwtpIDRadIDStatsMap->pWtpRadioStats.u2HwFailCount;

        pCapwapGetFsCapwapWtpRadioStatisticsEntry->MibObject.
            u4FsCapwapWtpRadioOtherFailureCount =
            pwtpIDRadIDStatsMap->pWtpRadioStats.u2OtherFailureCount;

        pCapwapGetFsCapwapWtpRadioStatisticsEntry->MibObject.
            u4FsCapwapWtpRadioUnknownFailureCount =
            pwtpIDRadIDStatsMap->pWtpRadioStats.u2UnknownFailureCount;

        pCapwapGetFsCapwapWtpRadioStatisticsEntry->MibObject.
            u4FsCapwapWtpRadioConfigUpdateCount =
            pwtpIDRadIDStatsMap->pWtpRadioStats.u2ConfigUpdateCount;

        pCapwapGetFsCapwapWtpRadioStatisticsEntry->MibObject.
            u4FsCapwapWtpRadioChannelChangeCount =
            pwtpIDRadIDStatsMap->pWtpRadioStats.u2ChannelChangeCount;

        pCapwapGetFsCapwapWtpRadioStatisticsEntry->MibObject.
            u4FsCapwapWtpRadioBandChangeCount =
            pwtpIDRadIDStatsMap->pWtpRadioStats.u2BandChangeCount;

        pCapwapGetFsCapwapWtpRadioStatisticsEntry->MibObject.
            u4FsCapwapWtpRadioCurrentNoiseFloor =
            pwtpIDRadIDStatsMap->pWtpRadioStats.u2CurrentNoiseFloor;
    }
#endif /* WLC_WANTED */

#ifdef WTP_WANTED
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    /* radio-id is passed in case of wtp in the mib-object */
    RadioIfGetDB.RadioIfGetAllDB.u1RadioId =
        pCapwapGetFsCapwapWtpRadioStatisticsEntry->MibObject.i4RadioIfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioStats = OSIX_TRUE;

    if (WssIfProcessRadioIfDBMsg (WSS_GET_PHY_RADIO_IF_DB, &RadioIfGetDB) ==
        OSIX_SUCCESS)
    {
        /* fill the values got from db */
        pCapwapGetFsCapwapWtpRadioStatisticsEntry->MibObject.
            i4FsCapwapWtpRadioLastFailType =
            RadioIfGetDB.RadioIfGetAllDB.radioStats.u1LastFailureType;

        pCapwapGetFsCapwapWtpRadioStatisticsEntry->MibObject.
            u4FsCapwapWtpRadioResetCount =
            RadioIfGetDB.RadioIfGetAllDB.radioStats.u2ResetCount;

        pCapwapGetFsCapwapWtpRadioStatisticsEntry->MibObject.
            u4FsCapwapWtpRadioSwFailureCount =
            RadioIfGetDB.RadioIfGetAllDB.radioStats.u2SwFailCount;

        pCapwapGetFsCapwapWtpRadioStatisticsEntry->MibObject.
            u4FsCapwapWtpRadioHwFailureCount =
            RadioIfGetDB.RadioIfGetAllDB.radioStats.u2HwFailCount;

        pCapwapGetFsCapwapWtpRadioStatisticsEntry->MibObject.
            u4FsCapwapWtpRadioOtherFailureCount =
            RadioIfGetDB.RadioIfGetAllDB.radioStats.u2OtherFailureCount;

        pCapwapGetFsCapwapWtpRadioStatisticsEntry->MibObject.
            u4FsCapwapWtpRadioUnknownFailureCount =
            RadioIfGetDB.RadioIfGetAllDB.radioStats.u2UnknownFailureCount;

        pCapwapGetFsCapwapWtpRadioStatisticsEntry->MibObject.
            u4FsCapwapWtpRadioConfigUpdateCount =
            RadioIfGetDB.RadioIfGetAllDB.radioStats.u2ConfigUpdateCount;

        pCapwapGetFsCapwapWtpRadioStatisticsEntry->MibObject.
            u4FsCapwapWtpRadioChannelChangeCount =
            RadioIfGetDB.RadioIfGetAllDB.radioStats.u2ChannelChangeCount;

        pCapwapGetFsCapwapWtpRadioStatisticsEntry->MibObject.
            u4FsCapwapWtpRadioBandChangeCount =
            RadioIfGetDB.RadioIfGetAllDB.radioStats.u2BandChangeCount;

        pCapwapGetFsCapwapWtpRadioStatisticsEntry->MibObject.
            u4FsCapwapWtpRadioCurrentNoiseFloor =
            RadioIfGetDB.RadioIfGetAllDB.radioStats.u2CurrentNoiseFloor;
    }
#endif /* WTP_WANTED */

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapGetAllUtlFsCapwapDot11StatisticsTable
 Input       :  pCapwapFsCapwapWtpDot11StatisticsEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    CapwapGetAllUtlFsCapwapDot11StatisticsTable
    (tCapwapFsCapwapWtpDot11StatisticsEntry *
     pCapwapFsCapwapWtpDot11StatisticsEntry)
{
#ifdef WTP_WANTED
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u1RadioId =
        pCapwapFsCapwapWtpDot11StatisticsEntry->MibObject.u1Dot11RadioId;
    RadioIfGetDB.RadioIfIsGetAllDB.bIeee80211Stats = OSIX_TRUE;

    if (WssIfProcessRadioIfDBMsg (WSS_GET_PHY_RADIO_IF_DB, &RadioIfGetDB) ==
        OSIX_SUCCESS)
    {
        pCapwapFsCapwapWtpDot11StatisticsEntry->MibObject.
            u4Dot11TransmittedFragmentCount =
            RadioIfGetDB.RadioIfGetAllDB.ieee80211Stats.u4TxFragmentCnt;
        pCapwapFsCapwapWtpDot11StatisticsEntry->MibObject.
            u4Dot11MulticastTransmittedFrameCount =
            RadioIfGetDB.RadioIfGetAllDB.ieee80211Stats.u4MulticastTxCnt;
        pCapwapFsCapwapWtpDot11StatisticsEntry->MibObject.u4Dot11FailedCount =
            RadioIfGetDB.RadioIfGetAllDB.ieee80211Stats.u4FailedCount;
        pCapwapFsCapwapWtpDot11StatisticsEntry->MibObject.u4Dot11RetryCount =
            RadioIfGetDB.RadioIfGetAllDB.ieee80211Stats.u4RetryCount;
        pCapwapFsCapwapWtpDot11StatisticsEntry->MibObject.
            u4Dot11MultipleRetryCount =
            RadioIfGetDB.RadioIfGetAllDB.ieee80211Stats.u4MultipleRetryCount;
        pCapwapFsCapwapWtpDot11StatisticsEntry->MibObject.
            u4Dot11FrameDuplicateCount =
            RadioIfGetDB.RadioIfGetAllDB.ieee80211Stats.u4FrameDupCount;
        pCapwapFsCapwapWtpDot11StatisticsEntry->MibObject.
            u4Dot11RTSSuccessCount =
            RadioIfGetDB.RadioIfGetAllDB.ieee80211Stats.u4RTSSuccessCount;
        pCapwapFsCapwapWtpDot11StatisticsEntry->MibObject.
            u4Dot11RTSFailureCount =
            RadioIfGetDB.RadioIfGetAllDB.ieee80211Stats.u4RTSFailCount;
        pCapwapFsCapwapWtpDot11StatisticsEntry->MibObject.
            u4Dot11ACKFailureCount =
            RadioIfGetDB.RadioIfGetAllDB.ieee80211Stats.u4ACKFailCount;
        pCapwapFsCapwapWtpDot11StatisticsEntry->MibObject.
            u4Dot11ReceivedFragmentCount =
            RadioIfGetDB.RadioIfGetAllDB.ieee80211Stats.u4RxFragmentCount;
        pCapwapFsCapwapWtpDot11StatisticsEntry->MibObject.
            u4Dot11MulticastReceivedFrameCount =
            RadioIfGetDB.RadioIfGetAllDB.ieee80211Stats.u4MulticastRxCount;
        pCapwapFsCapwapWtpDot11StatisticsEntry->MibObject.u4Dot11FCSErrorCount =
            RadioIfGetDB.RadioIfGetAllDB.ieee80211Stats.u4FCSErrCount;
        pCapwapFsCapwapWtpDot11StatisticsEntry->MibObject.
            u4Dot11TransmittedFrameCount =
            RadioIfGetDB.RadioIfGetAllDB.ieee80211Stats.u4TxFrameCount;
        pCapwapFsCapwapWtpDot11StatisticsEntry->MibObject.
            u4Dot11WEPUndecryptableCount =
            RadioIfGetDB.RadioIfGetAllDB.ieee80211Stats.u4DecryptionErr;
        pCapwapFsCapwapWtpDot11StatisticsEntry->MibObject.
            u4Dot11QosDiscardedFragmentCount =
            RadioIfGetDB.RadioIfGetAllDB.ieee80211Stats.u4DiscardQosFragmentCnt;
        pCapwapFsCapwapWtpDot11StatisticsEntry->MibObject.
            u4Dot11AssociatedStationCount =
            RadioIfGetDB.RadioIfGetAllDB.ieee80211Stats.u4AssociatedStaCount;
        pCapwapFsCapwapWtpDot11StatisticsEntry->MibObject.
            u4Dot11QosCFPollsReceivedCount =
            RadioIfGetDB.RadioIfGetAllDB.ieee80211Stats.u4QosCFPollsRecvdCount;
        pCapwapFsCapwapWtpDot11StatisticsEntry->MibObject.
            u4Dot11QosCFPollsUnusedCount =
            RadioIfGetDB.RadioIfGetAllDB.ieee80211Stats.u4QosCFPollsUnusedCount;
        pCapwapFsCapwapWtpDot11StatisticsEntry->MibObject.
            u4Dot11QosCFPollsUnusableCount =
            RadioIfGetDB.RadioIfGetAllDB.ieee80211Stats.
            u4QosCFPollsUnusableCount;
    }
#else
    UNUSED_PARAM (pCapwapFsCapwapWtpDot11StatisticsEntry);
#endif /* WTP_WANTED */
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  CapwapUtilUpdateFsWtpModelTable
 * Input       :   pCapwapOldFsWtpModelEntry
                   pCapwapFsWtpModelEntry
                   pCapwapIsSetFsWtpModelEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapUtilUpdateFsWtpModelTable (tCapwapFsWtpModelEntry *
                                 pCapwapOldFsWtpModelEntry,
                                 tCapwapFsWtpModelEntry *
                                 pCapwapFsWtpModelEntry,
                                 tCapwapIsSetFsWtpModelEntry *
                                 pCapwapIsSetFsWtpModelEntry)
{
    tWsscfgFsWtpImageUpgradeEntry wsscfgFsWtpImageUpgradeEntry;
    tWsscfgIsSetFsWtpImageUpgradeEntry wsscfgIsSetFsWtpImageUpgradeEntry;

    MEMSET (&wsscfgFsWtpImageUpgradeEntry, 0,
            sizeof (tWsscfgFsWtpImageUpgradeEntry));
    MEMSET (&wsscfgIsSetFsWtpImageUpgradeEntry, 0,
            sizeof (tWsscfgIsSetFsWtpImageUpgradeEntry));
    if (CapwapCliSetWtpModel
        (pCapwapFsWtpModelEntry, pCapwapIsSetFsWtpModelEntry) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    UNUSED_PARAM (pCapwapOldFsWtpModelEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  CapwapUtilUpdateFsWtpRadioTable
 * Input       :   pCapwapOldFsWtpRadioEntry
                   pCapwapFsWtpRadioEntry
                   pCapwapIsSetFsWtpRadioEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapUtilUpdateFsWtpRadioTable (tCapwapFsWtpRadioEntry *
                                 pCapwapOldFsWtpRadioEntry,
                                 tCapwapFsWtpRadioEntry *
                                 pCapwapFsWtpRadioEntry,
                                 tCapwapIsSetFsWtpRadioEntry *
                                 pCapwapIsSetFsWtpRadioEntry)
{
    if (CapwapCliSetWtpRadio
        (pCapwapFsWtpRadioEntry, pCapwapIsSetFsWtpRadioEntry) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    UNUSED_PARAM (pCapwapOldFsWtpRadioEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  CapwapUtilUpdateFsCapwapWhiteListTable
 * Input       :   pCapwapOldFsCapwapWhiteListEntry
                   pCapwapFsCapwapWhiteListEntry
                   pCapwapIsSetFsCapwapWhiteListEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapUtilUpdateFsCapwapWhiteListTable (tCapwapFsCapwapWhiteListEntry *
                                        pCapwapOldFsCapwapWhiteListEntry,
                                        tCapwapFsCapwapWhiteListEntry *
                                        pCapwapFsCapwapWhiteListEntry,
                                        tCapwapIsSetFsCapwapWhiteListEntry *
                                        pCapwapIsSetFsCapwapWhiteListEntry)
{

    tWssIfCapDB        *pWssIfCapDB = NULL;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapDB, 0, sizeof (tWssIfCapDB));

    MEMCPY (pWssIfCapDB->BaseMacAddress,
            pCapwapFsCapwapWhiteListEntry->MibObject.
            au1FsCapwapWhiteListWtpBaseMac,
            pCapwapFsCapwapWhiteListEntry->MibObject.
            i4FsCapwapWhiteListWtpBaseMacLen);

    /*    StrToMac(pCapwapFsCapwapWhiteListEntry->MibObject.au1FsCapwapWhiteListWtpBaseMac,
       pWssIfCapDB->BaseMacAddress); */

    if ((pCapwapFsCapwapWhiteListEntry->MibObject.
         i4FsCapwapWhiteListRowStatus == CREATE_AND_GO)
        || (pCapwapFsCapwapWhiteListEntry->MibObject.
            i4FsCapwapWhiteListRowStatus == ACTIVE))
    {
        if (WssIfProcessCapwapDBMsg
            (WSS_CAPWAP_CREATE_WHITELIST_ENTRY, pWssIfCapDB) != OSIX_SUCCESS)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "CapwapUtilUpdateFsCapwapWhiteList : WhiteList addition\failed\r\n");
            return OSIX_FAILURE;
        }
    }
    else if (pCapwapFsCapwapWhiteListEntry->MibObject.
             i4FsCapwapWhiteListRowStatus == DESTROY)
    {
        if (WssIfProcessCapwapDBMsg
            (WSS_CAPWAP_DELETE_WHITELIST_ENTRY, pWssIfCapDB) != OSIX_SUCCESS)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "CapwapUtilUpdateFsCapwapWhiteList : WhiteList Deletion\failed\r\n");
            return OSIX_FAILURE;
        }

    }

    UNUSED_PARAM (pCapwapOldFsCapwapWhiteListEntry);
    UNUSED_PARAM (pCapwapIsSetFsCapwapWhiteListEntry);
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  CapwapUtilUpdateFsCapwapBlackList
 * Input       :   pCapwapOldFsCapwapBlackListEntry
                   pCapwapFsCapwapBlackListEntry
                   pCapwapIsSetFsCapwapBlackListEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapUtilUpdateFsCapwapBlackList (tCapwapFsCapwapBlackListEntry *
                                   pCapwapOldFsCapwapBlackListEntry,
                                   tCapwapFsCapwapBlackListEntry *
                                   pCapwapFsCapwapBlackListEntry,
                                   tCapwapIsSetFsCapwapBlackListEntry *
                                   pCapwapIsSetFsCapwapBlackListEntry)
{

    tWssIfCapDB        *pWssIfCapDB = NULL;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapDB, 0, sizeof (tWssIfCapDB));

    MEMCPY (pWssIfCapDB->BaseMacAddress,
            pCapwapFsCapwapBlackListEntry->MibObject.
            au1FsCapwapBlackListWtpBaseMac,
            pCapwapFsCapwapBlackListEntry->MibObject.
            i4FsCapwapBlackListWtpBaseMacLen);

    /*    StrToMac(pCapwapFsCapwapBlackListEntry->MibObject.au1FsCapwapBlackListWtpBaseMac,
       pWssIfCapDB->BaseMacAddress); */

    if ((pCapwapFsCapwapBlackListEntry->MibObject.
         i4FsCapwapBlackListRowStatus == CREATE_AND_GO)
        || (pCapwapFsCapwapBlackListEntry->MibObject.
            i4FsCapwapBlackListRowStatus == ACTIVE))
    {
        if (WssIfProcessCapwapDBMsg
            (WSS_CAPWAP_CREATE_BLACKLIST_ENTRY, pWssIfCapDB) != OSIX_SUCCESS)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "CapwapUtilUpdateFsCapwapBlackList : BlackList addition failed\r\n");
            return OSIX_FAILURE;
        }
    }
    else if (pCapwapFsCapwapBlackListEntry->MibObject.
             i4FsCapwapBlackListRowStatus == DESTROY)
    {
        if (WssIfProcessCapwapDBMsg
            (WSS_CAPWAP_DELETE_BLACKLIST_ENTRY, pWssIfCapDB) != OSIX_SUCCESS)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "CapwapUtilUpdateFsCapwapBlackList : BlackList Deletion\
                    failed\r\n");
            return OSIX_FAILURE;
        }

    }

    UNUSED_PARAM (pCapwapOldFsCapwapBlackListEntry);
    UNUSED_PARAM (pCapwapIsSetFsCapwapBlackListEntry);
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  CapwapUtilUpdateFsCapwapWtpConfigTable
 * Input       :   pCapwapOldFsCapwapWtpConfigEntry
                   pCapwapFsCapwapWtpConfigEntry
                   pCapwapIsSetFsCapwapWtpConfigEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapUtilUpdateFsCapwapWtpConfigTable (tCapwapFsCapwapWtpConfigEntry *
                                        pCapwapOldFsCapwapWtpConfigEntry,
                                        tCapwapFsCapwapWtpConfigEntry *
                                        pCapwapFsCapwapWtpConfigEntry,
                                        tCapwapIsSetFsCapwapWtpConfigEntry *
                                        pCapwapIsSetFsCapwapWtpConfigEntry)
{
    tWssIfCapDB        *pWssIfCapDB = NULL;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tWssIfPMDB          WssIfPMDB;

    tWsscfgDot11StationConfigEntry WsscfgDot11StationConfigEntry;
    tWsscfgIsSetDot11StationConfigEntry WsscfgIsSetDot11StationConfigEntry;
    UINT4               u4RadioId = 1;
    UINT4               u4WtpProfileId = 0;
    INT4                i4Index = 0;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapDB, OSIX_FAILURE)
        WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
#ifdef WLCHDLR_WANTED
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;
    pWlcHdlrMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pWlcHdlrMsgStruct == NULL)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapUtilUpdateFsCapwapWtpConfigTable:- "
                    "UtlShMemAllocWlcBuf returned failure\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));
#endif
    MEMSET (pWssIfCapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&WsscfgDot11StationConfigEntry, 0,
            sizeof (tWsscfgDot11StationConfigEntry));
    MEMSET (&WsscfgIsSetDot11StationConfigEntry, 0,
            sizeof (tWsscfgIsSetDot11StationConfigEntry));

    UNUSED_PARAM (pCapwapOldFsCapwapWtpConfigEntry);

    if (pCapwapIsSetFsCapwapWtpConfigEntry->bFsCapwapClearApStats == OSIX_TRUE)
    {

        pWssIfCapwapDB->CapwapGetDB.u2ProfileId = (UINT2)
            pCapwapFsCapwapWtpConfigEntry->MibObject.u4CapwapBaseWtpProfileId;
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpProfileId = OSIX_TRUE;
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_INTERNAL_ID, pWssIfCapwapDB)
            == OSIX_FAILURE)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
#ifdef WLCHDLR_WANTED
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
#endif
            return SNMP_FAILURE;
        }
        if (WssIfProcessCapwapDBMsg
            (WSS_CAPWAP_GET_RSM_FROM_INDEX, pWssIfCapwapDB) == OSIX_FAILURE)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
#ifdef WLCHDLR_WANTED
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
#endif
            return SNMP_FAILURE;
        }
        if (pWssIfCapwapDB->pSessEntry == NULL)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "AP not in Run State\n");
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
#ifdef WLCHDLR_WANTED
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
#endif
            return SNMP_FAILURE;
        }
        if (nmhSetCapwapBaseWtpProfileWtpStatisticsTimer
            (pCapwapFsCapwapWtpConfigEntry->MibObject.u4CapwapBaseWtpProfileId,
             0) != SNMP_SUCCESS)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }

        WssIfPMDB.u2ProfileId = pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId;
        WssIfPMDB.tWtpPmAttState.wssPmWLCStatsSetDB.
            bWssPmWLCCapwapClearApStats = OSIX_TRUE;
        if (WssIfProcessPMDBMsg (WSS_PM_CLI_WTP_ID_STATS_CLEAR, &WssIfPMDB) !=
            OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to clear the Ap stats \n");
        }

        pCapwapFsCapwapWtpConfigEntry->MibObject.i4FsCapwapClearApStats =
            CAPWAP_DISABLE;
    }

    if (pCapwapIsSetFsCapwapWtpConfigEntry->bFsWtpDiscoveryType == OSIX_TRUE)
    {
        pWssIfCapDB->CapwapIsGetAllDB.bWtpProfileId = OSIX_TRUE;
        pWssIfCapDB->CapwapGetDB.u2WtpProfileId =
            (UINT2) (pCapwapFsCapwapWtpConfigEntry->MibObject.
                     u4CapwapBaseWtpProfileId);

        pWssIfCapDB->CapwapIsGetAllDB.bWtpDiscType = OSIX_TRUE;
        pWssIfCapDB->CapwapGetDB.u1WtpDiscType =
            (UINT1) (pCapwapFsCapwapWtpConfigEntry->MibObject.
                     i4FsWtpDiscoveryType);
        pWssIfCapDB->CapwapIsGetAllDB.bWlcStaticIp = OSIX_TRUE;
        pWssIfCapDB->CapwapGetDB.u4WlcStaticIp =
            pCapwapFsCapwapWtpConfigEntry->MibObject.u4FsWlcStaticIpAddress;
#ifdef WLCHDLR_WANTED
        if (pCapwapFsCapwapWtpConfigEntry->MibObject.
            i4FsCapwapWtpConfigRowStatus == ACTIVE)
        {
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.isOptional =
                OSIX_TRUE;
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.u2MsgEleType = 37;
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.u2MsgEleLen =
                DISCOVERY_TYPE_MESSAGE_LEN;
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.elementId =
                VENDOR_DISC_TYPE;
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
                VendDiscType.isOptional = OSIX_TRUE;
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
                VendDiscType.u2MsgEleType = DISCOVERY_TYPE_VENDOR_MSG;
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
                VendDiscType.u2MsgEleLen = DISCOVERY_TYPE_MESSAGE_LEN - 4;
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
                VendDiscType.vendorId = VENDOR_ID;
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
                VendDiscType.u1Val =
                (UINT1) (pCapwapFsCapwapWtpConfigEntry->MibObject.
                         i4FsWtpDiscoveryType);
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
                VendDiscType.u4WlcIpAddress =
                pCapwapFsCapwapWtpConfigEntry->MibObject.u4FsWlcStaticIpAddress;

            pWssIfCapDB->CapwapGetDB.u2ProfileId =
                pWssIfCapDB->CapwapGetDB.u2WtpProfileId;
            pWssIfCapDB->CapwapIsGetAllDB.bWtpProfileId = OSIX_TRUE;

            if (WssIfProcessCapwapDBMsg
                (WSS_CAPWAP_GET_INTERNAL_ID, pWssIfCapDB) != OSIX_SUCCESS)
            {
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                return OSIX_FAILURE;
            }

            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.u2InternalId =
                pWssIfCapDB->CapwapGetDB.u2WtpInternalId;
            if (WssIfProcessWlcHdlrMsg
                (WSS_WLCHDLR_CAPWAP_CONF_UPDATE_REQ,
                 pWlcHdlrMsgStruct) == OSIX_FAILURE)
            {
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                return OSIX_FAILURE;
            }
        }
#endif
#ifdef WTP_WANTED
        WssSetDiscoveryTypeToWtpNvRam (pWssIfCapDB->CapwapGetDB.u1WtpDiscType);
        WssSetWlcIpAddrToWtpNvRam (pWssIfCapDB->CapwapGetDB.u4WlcStaticIp);
#endif
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_SET_DB, pWssIfCapDB) !=
            OSIX_SUCCESS)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
#ifdef WLCHDLR_WANTED
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
#endif
            return OSIX_FAILURE;
        }

    }

    if (pCapwapIsSetFsCapwapWtpConfigEntry->bFsWtpCountryString == OSIX_TRUE)
    {
        /*Country string same for all the Radios in WTP, hence trigerred for */
        /*all u4RadioId */

        u4WtpProfileId =
            pCapwapFsCapwapWtpConfigEntry->MibObject.u4CapwapBaseWtpProfileId;

        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpProfileId = OSIX_TRUE;
        pWssIfCapwapDB->CapwapGetDB.u2ProfileId = (UINT2) u4WtpProfileId;

        pWssIfCapwapDB->CapwapIsGetAllDB.bNoOfRadio = OSIX_TRUE;
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;

        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
            == OSIX_FAILURE)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
#ifdef WLCHDLR_WANTED
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
#endif
            return OSIX_FAILURE;
        }
        /* FIX for 3440 */
        for (u4RadioId = 1; u4RadioId <= pWssIfCapwapDB->CapwapGetDB.
             u1WtpNoofRadio; u4RadioId++)
        {
            if (nmhGetFsCapwapWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4Index) != SNMP_SUCCESS)
            {
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
#ifdef WLCHDLR_WANTED
                UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
#endif
                return OSIX_FAILURE;
            }
            MEMCPY (WsscfgDot11StationConfigEntry.MibObject.
                    au1Dot11CountryString,
                    pCapwapFsCapwapWtpConfigEntry->MibObject.
                    au1FsWtpCountryString,
                    pCapwapFsCapwapWtpConfigEntry->MibObject.
                    i4FsWtpCountryStringLen);
            WsscfgDot11StationConfigEntry.MibObject.i4Dot11CountryStringLen =
                pCapwapFsCapwapWtpConfigEntry->MibObject.
                i4FsWtpCountryStringLen;
            WsscfgDot11StationConfigEntry.MibObject.i4IfIndex = i4Index;
#ifdef WLC_WANTED
            if (WssCfgSetStationConfigTable (&WsscfgDot11StationConfigEntry,
                                             &WsscfgIsSetDot11StationConfigEntry)
                == OSIX_FAILURE)
            {
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                return OSIX_FAILURE;
            }
#endif
        }
    }

    if (pCapwapIsSetFsCapwapWtpConfigEntry->bFsWtpCrashDumpFileName == OSIX_TRUE
        && pCapwapFsCapwapWtpConfigEntry->MibObject.
        i4FsCapwapWtpConfigRowStatus == ACTIVE
        && pCapwapFsCapwapWtpConfigEntry->MibObject.u4FsWtpDeleteOperation ==
        CRASHFILE_GET)
    {
#ifdef WLCHDLR_WANTED
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
        tRemoteSessionManager *pSessEntry = NULL;
        UINT1               au1CrashFile[280];
        MEMSET (au1CrashFile, 0, sizeof (au1CrashFile));

        pWssIfCapwapDB->CapwapGetDB.u2ProfildId =
            (UINT2) (pCapwapFsCapwapWtpConfigEntry->MibObject.
                     u4CapwapBaseWtpProfileId);
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB) ==
            OSIX_FAILURE)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            return SNMP_FAILURE;
        }

        if (WssIfProcessCapwapDBMsg
            (WSS_CAPWAP_GET_RSM_FROM_INDEX, pWssIfCapwapDB) == OSIX_FAILURE)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            return SNMP_FAILURE;
        }
        pSessEntry = pWssIfCapwapDB->pSessEntry;

        if (pSessEntry == NULL)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            CAPWAP_TRC (ALL_FAILURE_TRC, "Session pointer is NULL\n");
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            return OSIX_FAILURE;
        }
        if (pSessEntry->eCurrentState != CAPWAP_RUN)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            CAPWAP_TRC (ALL_FAILURE_TRC, "Session Not in RUN State\n");
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            return OSIX_FAILURE;
        }

        /* Set the Crash File Name */
        SPRINTF ((char *) au1CrashFile, "%s%s", "/tmp/crashfiles/",
                 pCapwapFsCapwapWtpConfigEntry->MibObject.
                 au1FsWtpCrashDumpFileName);
        STRCPY (pSessEntry->au1CrashFileName, au1CrashFile);
        pSessEntry->u1CrashFileNameSet = OSIX_TRUE;

        pWlcHdlrMsgStruct->DataReq.isPresent = OSIX_TRUE;
        pWlcHdlrMsgStruct->DataReq.u2SessId =
            pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId;
        pWlcHdlrMsgStruct->DataReq.datatransfermode.u2MsgEleType =
            DATA_TRANSFER_MODE;
        pWlcHdlrMsgStruct->DataReq.datatransfermode.u2MsgEleLen =
            DATA_TRANSFER_MODE_LEN;
        pWlcHdlrMsgStruct->DataReq.datatransfermode.isPresent = OSIX_TRUE;
        pWlcHdlrMsgStruct->DataReq.datatransfermode.DataMode =
            DATA_TRANS_MODE_WTP_CRASH;

        if (WssIfProcessWlcHdlrMsg
            (WSS_WLCHDLR_DATA_TRANSFER_REQ, pWlcHdlrMsgStruct) == OSIX_FAILURE)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            return OSIX_FAILURE;
        }

        pCapwapFsCapwapWtpConfigEntry->MibObject.u4FsWtpDeleteOperation =
            NO_CRASHMEMORY;
#endif
    }
    if (pCapwapIsSetFsCapwapWtpConfigEntry->bFsWtpCrashDumpFileName == OSIX_TRUE
        && pCapwapFsCapwapWtpConfigEntry->MibObject.
        i4FsCapwapWtpConfigRowStatus == ACTIVE
        && pCapwapFsCapwapWtpConfigEntry->MibObject.u4FsWtpDeleteOperation ==
        CRASHFILE_DELETE)
    {
#ifdef WLCHDLR_WANTED
        FILE               *fp = NULL;
        UINT1               au1CrashFileName[280];
        MEMSET (au1CrashFileName, 0, sizeof (au1CrashFileName));

        pCapwapFsCapwapWtpConfigEntry->MibObject.u4FsWtpDeleteOperation =
            NO_CRASHMEMORY;

        SPRINTF ((CHR1 *) au1CrashFileName, "%s/%s", CRASH_FILES_PATH,
                 pCapwapFsCapwapWtpConfigEntry->MibObject.
                 au1FsWtpCrashDumpFileName);

        if ((fp = fopen ((char *) au1CrashFileName, "r")) != NULL)
        {
            fclose (fp);
            if (FileDelete (au1CrashFileName) != OSIX_SUCCESS)
            {
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                            "CapwapUtilUpdateFsCapwapWtpConfigTable : Error in deleting the Crash File\r\n");
                UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                return OSIX_FAILURE;
            }
        }
        else
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "The Crash File is Not Found\r\n");
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            return OSIX_SUCCESS;
        }
#endif
    }
#ifdef WLCHDLR_WANTED
    if (pCapwapIsSetFsCapwapWtpConfigEntry->bFsCapwapWtpReset == OSIX_TRUE &&
        pCapwapFsCapwapWtpConfigEntry->MibObject.
        i4FsCapwapWtpConfigRowStatus == ACTIVE)
    {
        pWssIfCapwapDB->CapwapGetDB.u2ProfileId =
            (UINT2) (pCapwapFsCapwapWtpConfigEntry->MibObject.
                     u4CapwapBaseWtpProfileId);
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpProfileId = OSIX_TRUE;
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_INTERNAL_ID, pWssIfCapwapDB)
            != OSIX_SUCCESS)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            return OSIX_FAILURE;
        }
        pWlcHdlrMsgStruct->ResetReq.u2SessId =
            pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId;
        if (WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_RESET_REQ,
                                    pWlcHdlrMsgStruct) != OSIX_SUCCESS)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            return OSIX_FAILURE;
        }

        pCapwapFsCapwapWtpConfigEntry->MibObject.i4FsCapwapWtpReset
            = CAPWAP_DISABLE;
    }
    if (pCapwapIsSetFsCapwapWtpConfigEntry->bFsCapwapClearConfig == OSIX_TRUE &&
        pCapwapFsCapwapWtpConfigEntry->MibObject.
        i4FsCapwapWtpConfigRowStatus == ACTIVE)
    {
        pWssIfCapwapDB->CapwapGetDB.u2ProfileId =
            (UINT2) (pCapwapFsCapwapWtpConfigEntry->MibObject.
                     u4CapwapBaseWtpProfileId);
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpProfileId = OSIX_TRUE;
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_INTERNAL_ID, pWssIfCapwapDB)
            != OSIX_SUCCESS)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            return OSIX_FAILURE;
        }
        pWlcHdlrMsgStruct->ClearConfigReq.u2SessId =
            pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId;
        if (WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_CLEAR_CONF_REQ,
                                    pWlcHdlrMsgStruct) != OSIX_SUCCESS)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            return OSIX_FAILURE;
        }

        pCapwapFsCapwapWtpConfigEntry->MibObject.i4FsCapwapClearConfig
            = CAPWAP_DISABLE;
    }
    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
#endif
#ifdef WTP_WANTED
    if (pCapwapIsSetFsCapwapWtpConfigEntry->bFsCapwapClearConfig == OSIX_TRUE &&
        pCapwapFsCapwapWtpConfigEntry->MibObject.
        i4FsCapwapWtpConfigRowStatus == ACTIVE)
    {
        if (CapwapClearWtpConfig () != OSIX_SUCCESS)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
    }
#endif

    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  CapwapUtilUpdateFsCapwapLinkEncryptionTable
 * Input       :   pCapwapOldFsCapwapLinkEncryptionEntry
                   pCapwapFsCapwapLinkEncryptionEntry
                   pCapwapIsSetFsCapwapLinkEncryptionEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapUtilUpdateFsCapwapLinkEncryptionTable (tCapwapFsCapwapLinkEncryptionEntry
                                             *
                                             pCapwapOldFsCapwapLinkEncryptionEntry,
                                             tCapwapFsCapwapLinkEncryptionEntry
                                             *
                                             pCapwapFsCapwapLinkEncryptionEntry,
                                             tCapwapIsSetFsCapwapLinkEncryptionEntry
                                             *
                                             pCapwapIsSetFsCapwapLinkEncryptionEntry)
{

    tWssIfCapDB        *pWssIfCapDB = NULL;
#ifdef WLCHDLR_WANTED
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;
#endif

    CAPWAP_FN_ENTRY ();

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapDB, 0, sizeof (tWssIfCapDB));

    if (((pCapwapFsCapwapLinkEncryptionEntry->MibObject.
          i4FsCapwapEncryptChannelStatus == 1) &&
         (pCapwapFsCapwapLinkEncryptionEntry->MibObject.
          i4FsCapwapEncryptChannel == 1)) ||
        ((pCapwapFsCapwapLinkEncryptionEntry->MibObject.
          i4FsCapwapEncryptChannelStatus == 2) &&
         (pCapwapFsCapwapLinkEncryptionEntry->MibObject.
          i4FsCapwapEncryptChannel == 1)))
    {
        pWssIfCapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
        pWssIfCapDB->CapwapIsGetAllDB.bWtpProfileId = OSIX_TRUE;
        pWssIfCapDB->CapwapIsGetAllDB.bWtpCtrlDTLSStatus = OSIX_TRUE;
        pWssIfCapDB->CapwapGetDB.u1WtpCtrlDTLSStatus =
            (UINT1) (pCapwapFsCapwapLinkEncryptionEntry->MibObject.
                     i4FsCapwapEncryptChannelStatus);
        pWssIfCapDB->CapwapGetDB.u2WtpProfileId =
            (UINT2) (pCapwapFsCapwapLinkEncryptionEntry->MibObject.
                     u4CapwapBaseWtpProfileId);
        pWssIfCapDB->CapwapGetDB.u2ProfileId =
            (UINT2) (pCapwapFsCapwapLinkEncryptionEntry->MibObject.
                     u4CapwapBaseWtpProfileId);

        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_INTERNAL_ID,
                                     pWssIfCapDB) != OSIX_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "CapwapUtilUpdateFsCapwapLinkEncryptionTable:- "
                        "WssIfProcessCapwapDBMsg returned failure\n");
        }

#ifdef WLCHDLR_WANTED
        pWlcHdlrMsgStruct =
            (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
        if (pWlcHdlrMsgStruct == NULL)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "CapwapUtilUpdateFsCapwapLinkEncryptionTable:- "
                        "UtlShMemAllocWlcBuf returned failure\n");
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
            return OSIX_FAILURE;
        }

        MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));
        if (pCapwapFsCapwapLinkEncryptionEntry->MibObject.
            i4FsCapwapEncryptChannelRowStatus == ACTIVE)
        {
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.u2InternalId =
                pWssIfCapDB->CapwapGetDB.u2WtpInternalId;
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.isOptional =
                OSIX_TRUE;
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.u2MsgEleType = 37;
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.u2MsgEleLen = 9;
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.elementId =
                VENDOR_CONTROL_POLICY_DTLS_MSG;
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
                VendorContDTLS.isOptional = OSIX_TRUE;
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
                VendorContDTLS.u2MsgEleType = CONTROL_POLICY_DTLS_VENDOR_MSG;
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
                VendorContDTLS.u2MsgEleLen = 5;
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
                VendorContDTLS.vendorId = VENDOR_ID;
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
                VendorContDTLS.u1Val =
                (UINT1) pCapwapFsCapwapLinkEncryptionEntry->MibObject.
                i4FsCapwapEncryptChannelStatus;

            if (WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_CAPWAP_CONF_UPDATE_REQ,
                                        pWlcHdlrMsgStruct) == OSIX_FAILURE)
            {
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                return OSIX_FAILURE;
            }
        }
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
#endif

        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_SET_DB, pWssIfCapDB)
            != OSIX_SUCCESS)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
            return OSIX_FAILURE;
        }
    }

    if (((pCapwapFsCapwapLinkEncryptionEntry->MibObject.
          i4FsCapwapEncryptChannelStatus == 1) &&
         (pCapwapFsCapwapLinkEncryptionEntry->MibObject.
          i4FsCapwapEncryptChannel == 2)) ||
        ((pCapwapFsCapwapLinkEncryptionEntry->MibObject.
          i4FsCapwapEncryptChannelStatus == 2) &&
         (pCapwapFsCapwapLinkEncryptionEntry->MibObject.
          i4FsCapwapEncryptChannel == 2)))
    {
        pWssIfCapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
        pWssIfCapDB->CapwapIsGetAllDB.bWtpProfileId = OSIX_TRUE;
        pWssIfCapDB->CapwapIsGetAllDB.bWtpDataDTLSStatus = OSIX_TRUE;
        pWssIfCapDB->CapwapGetDB.u1WtpDataDTLSStatus =
            (UINT1) (pCapwapFsCapwapLinkEncryptionEntry->MibObject.
                     i4FsCapwapEncryptChannelStatus);
        pWssIfCapDB->CapwapGetDB.u2WtpProfileId =
            (UINT2) (pCapwapFsCapwapLinkEncryptionEntry->MibObject.
                     u4CapwapBaseWtpProfileId);
        pWssIfCapDB->CapwapGetDB.u2ProfileId =
            (UINT2) (pCapwapFsCapwapLinkEncryptionEntry->MibObject.
                     u4CapwapBaseWtpProfileId);

        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_SET_DB, pWssIfCapDB)
            != OSIX_SUCCESS)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
            return OSIX_FAILURE;
        }
    }

    UNUSED_PARAM (pCapwapOldFsCapwapLinkEncryptionEntry);
    UNUSED_PARAM (pCapwapIsSetFsCapwapLinkEncryptionEntry);
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  CapwapUtilUpdateFsCawapDefaultWtpProfileTable
 * Input       :   pCapwapOldFsCapwapDefaultWtpProfileEntry
                   pCapwapFsCapwapDefaultWtpProfileEntry
                   pCapwapIsSetFsCapwapDefaultWtpProfileEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    CapwapUtilUpdateFsCawapDefaultWtpProfileTable
    (tCapwapFsCapwapDefaultWtpProfileEntry *
     pCapwapOldFsCapwapDefaultWtpProfileEntry,
     tCapwapFsCapwapDefaultWtpProfileEntry *
     pCapwapFsCapwapDefaultWtpProfileEntry,
     tCapwapIsSetFsCapwapDefaultWtpProfileEntry *
     pCapwapIsSetFsCapwapDefaultWtpProfileEntry)
{

    UNUSED_PARAM (pCapwapOldFsCapwapDefaultWtpProfileEntry);
    if (CapwapCliSetWtpDefaultProfile
        (pCapwapFsCapwapDefaultWtpProfileEntry,
         pCapwapIsSetFsCapwapDefaultWtpProfileEntry) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    UNUSED_PARAM (pCapwapFsCapwapDefaultWtpProfileEntry);
    UNUSED_PARAM (pCapwapIsSetFsCapwapDefaultWtpProfileEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  CapwapUtilUpdateFsCapwapDnsProfileTable
 * Input       :   pCapwapOldFsCapwapDnsProfileEntry
                   pCapwapFsCapwapDnsProfileEntry
                   pCapwapIsSetFsCapwapDnsProfileEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapUtilUpdateFsCapwapDnsProfileTable (tCapwapFsCapwapDnsProfileEntry *
                                         pCapwapOldFsCapwapDnsProfileEntry,
                                         tCapwapFsCapwapDnsProfileEntry *
                                         pCapwapFsCapwapDnsProfileEntry,
                                         tCapwapIsSetFsCapwapDnsProfileEntry *
                                         pCapwapIsSetFsCapwapDnsProfileEntry)
{

    tWssIfCapDB        *pWssIfCapDB = NULL;
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapDB, 0, sizeof (tWssIfCapDB));
#ifdef WLC_WANTED
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;
    pWlcHdlrMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pWlcHdlrMsgStruct == NULL)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapUtilUpdateFsCapwapDnsProfileTable:- "
                    "UtlShMemAllocWlcBuf returned failure\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
        return OSIX_FAILURE;
    }
    MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));
#endif

    UNUSED_PARAM (pCapwapOldFsCapwapDnsProfileEntry);
    if (pCapwapFsCapwapDnsProfileEntry->MibObject.
        i4FsCapwapDnsProfileRowStatus == CREATE_AND_GO)
    {
        pWssIfCapDB->DnsProfileEntry.au1CapwapBaseWtpProfileId =
            pCapwapFsCapwapDnsProfileEntry->MibObject.u4CapwapBaseWtpProfileId;

        if (CapwapCreateDnsProfileTable (pWssIfCapDB) != OSIX_SUCCESS)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
#ifdef WLC_WANTED
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
#endif
            return SNMP_FAILURE;
        }
    }

    if (pCapwapFsCapwapDnsProfileEntry->MibObject.
        i4FsCapwapDnsProfileRowStatus == ACTIVE)
    {
        pWssIfCapDB->DnsProfileEntry.au1CapwapBaseWtpProfileId =
            pCapwapFsCapwapDnsProfileEntry->MibObject.u4CapwapBaseWtpProfileId;

        if (pCapwapIsSetFsCapwapDnsProfileEntry->bFsCapwapDnsServerIp ==
            OSIX_TRUE)
        {
            pWssIfCapDB->CapwapIsGetAllDB.bCapwapDnsServerIp = OSIX_TRUE;

            MEMCPY (pWssIfCapDB->DnsProfileEntry.au1FsCapwapDnsServerIp,
                    pCapwapFsCapwapDnsProfileEntry->MibObject.
                    au1FsCapwapDnsServerIp,
                    pCapwapFsCapwapDnsProfileEntry->MibObject.
                    i4FsCapwapDnsServerIpLen);

            pWssIfCapDB->DnsProfileEntry.i4FsCapwapDnsServerIpLen =
                pCapwapFsCapwapDnsProfileEntry->MibObject.
                i4FsCapwapDnsServerIpLen;
#ifdef WLC_WANTED
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.isOptional =
                OSIX_TRUE;
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.u2MsgEleType = 37;
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.u2MsgEleLen = 44;
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.elementId =
                VENDOR_DOMAIN_NAME_TYPE;
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
                VendDns.isOptional = OSIX_TRUE;
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
                VendDns.u2MsgEleType = DOMAIN_NAME_VENDOR_MSG;
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
                VendDns.u2MsgEleLen = 40;
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
                VendDns.vendorId = VENDOR_ID;
            MEMCPY (pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.
                    unVendorSpec.VendDns.au1ServerIp,
                    pCapwapFsCapwapDnsProfileEntry->MibObject.
                    au1FsCapwapDnsServerIp,
                    pCapwapFsCapwapDnsProfileEntry->MibObject.
                    i4FsCapwapDnsServerIpLen);
#endif
        }

        if (pCapwapIsSetFsCapwapDnsProfileEntry->bFsCapwapDnsDomainName ==
            OSIX_TRUE)
        {
            pWssIfCapDB->CapwapIsGetAllDB.bCapwapDnsDomainName = OSIX_TRUE;

            MEMCPY (pWssIfCapDB->DnsProfileEntry.au1FsCapwapDnsDomainName,
                    pCapwapFsCapwapDnsProfileEntry->MibObject.
                    au1FsCapwapDnsDomainName,
                    pCapwapFsCapwapDnsProfileEntry->MibObject.
                    i4FsCapwapDnsDomainNameLen);

            pWssIfCapDB->DnsProfileEntry.i4FsCapwapDnsDomainNameLen =
                pCapwapFsCapwapDnsProfileEntry->MibObject.
                i4FsCapwapDnsDomainNameLen;
#ifdef WLC_WANTED
            MEMCPY (pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.
                    unVendorSpec.VendDns.au1DomainName,
                    pCapwapFsCapwapDnsProfileEntry->MibObject.
                    au1FsCapwapDnsDomainName,
                    pCapwapFsCapwapDnsProfileEntry->MibObject.
                    i4FsCapwapDnsDomainNameLen);
#endif

        }
#ifdef WLC_WANTED
        if (WssIfProcessWlcHdlrMsg
            (WSS_WLCHDLR_CAPWAP_CONF_UPDATE_REQ,
             pWlcHdlrMsgStruct) == OSIX_FAILURE)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            return OSIX_FAILURE;
        }
#endif

        if (CapwapSetDnsProfileTable (pWssIfCapDB) != OSIX_SUCCESS)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
#ifdef WLC_WANTED
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
#endif
            return OSIX_FAILURE;
        }
    }
#ifdef WLC_WANTED
    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
#endif
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  CapwapUtilUpdateFsWtpNativeVlanIdTable
 * Input       :   pCapwapOldFsWtpNativeVlanIdTable
                   pCapwapFsWtpNativeVlanIdTable
                   pCapwapIsSetFsWtpNativeVlanIdTable
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapUtilUpdateFsWtpNativeVlanIdTable (tCapwapFsWtpNativeVlanIdTable *
                                        pCapwapOldFsWtpNativeVlanIdTable,
                                        tCapwapFsWtpNativeVlanIdTable *
                                        pCapwapFsWtpNativeVlanIdTable,
                                        tCapwapIsSetFsWtpNativeVlanIdTable *
                                        pCapwapIsSetFsWtpNativeVlanIdTable)
{
    tWssIfCapDB        *pCapwapGetDB = NULL;
    WSS_IF_DB_TEMP_MEM_ALLOC (pCapwapGetDB, OSIX_FAILURE)
        MEMSET (pCapwapGetDB, 0, sizeof (tWssIfCapDB));
#ifdef WLCHDLR_WANTED
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;
    pWlcHdlrMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pWlcHdlrMsgStruct == NULL)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapUtilUpdateFsWtpNativeVlanIdTable:- "
                    "UtlShMemAllocWlcBuf returned failure\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pCapwapGetDB);
        return OSIX_FAILURE;
    }

    MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));
#endif
    UNUSED_PARAM (pCapwapOldFsWtpNativeVlanIdTable);

    /* WTP Native Vlan ID */
    if (pCapwapIsSetFsWtpNativeVlanIdTable->bFsWtpNativeVlanId != OSIX_FALSE)
    {
        pCapwapGetDB->CapwapIsGetAllDB.bNativeVlan = OSIX_TRUE;
        pCapwapGetDB->CapwapIsGetAllDB.bWtpProfileId = OSIX_TRUE;
        pCapwapGetDB->CapwapGetDB.u4NativeVlan = (UINT4)
            pCapwapFsWtpNativeVlanIdTable->MibObject.i4FsWtpNativeVlanId;
        pCapwapGetDB->CapwapGetDB.u2ProfileId = (UINT2)
            pCapwapFsWtpNativeVlanIdTable->MibObject.u4CapwapBaseWtpProfileId;
#ifdef WLCHDLR_WANTED
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.isOptional =
            OSIX_TRUE;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.u2MsgEleType = 37;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.u2MsgEleLen = 10;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.elementId =
            VENDOR_NATIVE_VLAN;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.VendVlan.
            isOptional = OSIX_TRUE;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.VendVlan.
            u2MsgEleType = NATIVE_VLAN_VENDOR_MSG;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.VendVlan.
            u2MsgEleLen = 6;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.VendVlan.
            vendorId = VENDOR_ID;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.VendVlan.
            u2VlanId =
            (UINT2) pCapwapFsWtpNativeVlanIdTable->MibObject.
            i4FsWtpNativeVlanId;

        if (WssIfProcessWlcHdlrMsg
            (WSS_WLCHDLR_CAPWAP_CONF_UPDATE_REQ,
             pWlcHdlrMsgStruct) == OSIX_FAILURE)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pCapwapGetDB);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            return OSIX_FAILURE;
        }
#endif
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_SET_DB, pCapwapGetDB) !=
            OSIX_SUCCESS)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pCapwapGetDB);
#ifdef WLCHDLR_WANTED
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
#endif
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "CapwapSetWtpProfile :CAPWAP DB access failed. \r\n");
            return OSIX_FAILURE;
        }
    }
#ifdef WLCHDLR_WANTED
    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
#endif
    WSS_IF_DB_TEMP_MEM_RELEASE (pCapwapGetDB);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  CapwapUtilUpdateFsWtpLocalRoutingTable
 * Input       :   pCapwapOldFsWtpLocalRoutingTable
                   pCapwapFsWtpLocalRoutingTable
                   pCapwapIsSetFsWtpLocalRoutingTable
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapUtilUpdateFsWtpLocalRoutingTable (tCapwapFsWtpLocalRoutingTable *
                                        pCapwapOldFsWtpLocalRoutingTable,
                                        tCapwapFsWtpLocalRoutingTable *
                                        pCapwapFsWtpLocalRoutingTable,
                                        tCapwapIsSetFsWtpLocalRoutingTable *
                                        pCapwapIsSetFsWtpLocalRoutingTable)
{
    tWssIfCapDB        *pCapwapGetDB = NULL;
    WSS_IF_DB_TEMP_MEM_ALLOC (pCapwapGetDB, OSIX_FAILURE)
        MEMSET (pCapwapGetDB, 0, sizeof (tWssIfCapDB));
#ifdef WLCHDLR_WANTED
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;
    pWlcHdlrMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pWlcHdlrMsgStruct == NULL)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapUtilUpdateFsWtpLocalRoutingTable:- "
                    "UtlShMemAllocWlcBuf returned failure\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pCapwapGetDB);
        return OSIX_FAILURE;
    }

    MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));
#endif
#ifdef WLC_WANTED
    INT4                i4RetVal = 0;
#endif
    UNUSED_PARAM (pCapwapOldFsWtpLocalRoutingTable);

    if (pCapwapIsSetFsWtpLocalRoutingTable->bFsWtpLocalRouting != OSIX_FALSE)
    {
        pCapwapGetDB->CapwapIsGetAllDB.bWtpLocalRouting = OSIX_TRUE;
        pCapwapGetDB->CapwapIsGetAllDB.bWtpProfileId = OSIX_TRUE;
        pCapwapGetDB->CapwapGetDB.u1WtpLocalRouting = (UINT1)
            pCapwapFsWtpLocalRoutingTable->MibObject.i4FsWtpLocalRouting;
        pCapwapGetDB->CapwapGetDB.u2ProfileId = (UINT2)
            pCapwapFsWtpLocalRoutingTable->MibObject.u4CapwapBaseWtpProfileId;

#ifdef WLC_WANTED
        pCapwapGetDB->CapwapGetDB.u2ProfileId = (UINT2)
            pCapwapFsWtpLocalRoutingTable->MibObject.u4CapwapBaseWtpProfileId;
        pCapwapGetDB->CapwapIsGetAllDB.bWtpProfileId = OSIX_TRUE;

        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_INTERNAL_ID, pCapwapGetDB)
            == OSIX_SUCCESS)
        {
            /* Construct Config Update request message to be send to WTP when there
             * is change in WTP Local Routing */
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.isOptional =
                OSIX_TRUE;
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.elementId =
                VENDOR_LOCAL_ROUTING_TYPE;
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.u2MsgEleType =
                CAPWAP_VEND_SPECIFIC_PAYLOAD_MSG_TYPE;
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.u2MsgEleLen = 9;
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
                VendLocalRouting.isOptional = OSIX_TRUE;
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
                VendLocalRouting.u2MsgEleType = LOCAL_ROUTING_VENDOR_MSG;
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
                VendLocalRouting.u2MsgEleLen = 5;
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
                VendLocalRouting.vendorId = VENDOR_ID;
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
                VendLocalRouting.u1WtpLocalRouting =
                pCapwapGetDB->CapwapGetDB.u1WtpLocalRouting;
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.u2InternalId =
                pCapwapGetDB->CapwapGetDB.u2WtpInternalId;

            if (pCapwapFsWtpLocalRoutingTable->MibObject.
                i4FsWtpLocalRoutingRowStatus == ACTIVE)
            {

                i4RetVal =
                    WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_CAPWAP_CONF_UPDATE_REQ,
                                            pWlcHdlrMsgStruct);
                if (i4RetVal == OSIX_FAILURE)
                {
                    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                                "LocalRouting: Sending Request to WLC Handler failed \r\n");
                    WSS_IF_DB_TEMP_MEM_RELEASE (pCapwapGetDB);
                    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                    return OSIX_FAILURE;
                }

            }
        }
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_SET_DB, pCapwapGetDB) !=
            OSIX_SUCCESS)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pCapwapGetDB);
#ifdef WLCHDLR_WANTED
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
#endif
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "CapwapSetWtpProfile :CAPWAP DB access failed. \r\n");
            return OSIX_FAILURE;
        }
#endif
    }
#ifdef WSSUSER_WANTED
#if defined (WLC_WANTED) && defined (LNXIP4_WANTED)
    if (pCapwapFsWtpLocalRoutingTable->MibObject.
        i4FsWtpLocalRoutingRowStatus == ACTIVE)
    {

        if ((pCapwapGetDB->CapwapGetDB.u1WtpLocalRouting ==
             LOCAL_ROUTING_ENABLED)
            || (pCapwapGetDB->CapwapGetDB.u1WtpLocalRouting ==
                LOCAL_BRIDGING_ENABLED))
        {
            WssifAddRemoveTcFilter (pCapwapGetDB->CapwapGetDB.u2ProfileId,
                                    OSIX_TRUE);
        }
        else if ((pCapwapGetDB->CapwapGetDB.u1WtpLocalRouting ==
                  LOCAL_ROUTING_DISABLED)
                 || (pCapwapGetDB->CapwapGetDB.u1WtpLocalRouting ==
                     LOCAL_BRIDGING_DISABLED))
        {
            WssifAddRemoveTcFilter (pCapwapGetDB->CapwapGetDB.u2ProfileId,
                                    OSIX_FALSE);
        }

    }
#endif
#endif
#ifdef WLCHDLR_WANTED
    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
#endif
    WSS_IF_DB_TEMP_MEM_RELEASE (pCapwapGetDB);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  CapwapUtilUpdateFsCawapDiscStatsTable
 * Input       :   pCapwapOldFsCawapDiscStatsEntry
                   pCapwapFsCawapDiscStatsEntry
                   pCapwapIsSetFsCawapDiscStatsEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapUtilUpdateFsCawapDiscStatsTable (tCapwapFsCawapDiscStatsEntry *
                                       pCapwapOldFsCawapDiscStatsEntry,
                                       tCapwapFsCawapDiscStatsEntry *
                                       pCapwapFsCawapDiscStatsEntry,
                                       tCapwapIsSetFsCawapDiscStatsEntry *
                                       pCapwapIsSetFsCawapDiscStatsEntry)
{

    UNUSED_PARAM (pCapwapOldFsCawapDiscStatsEntry);
    UNUSED_PARAM (pCapwapFsCawapDiscStatsEntry);
    UNUSED_PARAM (pCapwapIsSetFsCawapDiscStatsEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  CapwapUtilUpdateFsCawapJoinStatsTable
 * Input       :   pCapwapOldFsCawapJoinStatsEntry
                   pCapwapFsCawapJoinStatsEntry
                   pCapwapIsSetFsCawapJoinStatsEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapUtilUpdateFsCawapJoinStatsTable (tCapwapFsCawapJoinStatsEntry *
                                       pCapwapOldFsCawapJoinStatsEntry,
                                       tCapwapFsCawapJoinStatsEntry *
                                       pCapwapFsCawapJoinStatsEntry,
                                       tCapwapIsSetFsCawapJoinStatsEntry *
                                       pCapwapIsSetFsCawapJoinStatsEntry)
{

    UNUSED_PARAM (pCapwapOldFsCawapJoinStatsEntry);
    UNUSED_PARAM (pCapwapFsCawapJoinStatsEntry);
    UNUSED_PARAM (pCapwapIsSetFsCawapJoinStatsEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  CapwapUtilUpdateFsCawapConfigStatsTable
 * Input       :   pCapwapOldFsCawapConfigStatsEntry
                   pCapwapFsCawapConfigStatsEntry
                   pCapwapIsSetFsCawapConfigStatsEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapUtilUpdateFsCawapConfigStatsTable (tCapwapFsCawapConfigStatsEntry *
                                         pCapwapOldFsCawapConfigStatsEntry,
                                         tCapwapFsCawapConfigStatsEntry *
                                         pCapwapFsCawapConfigStatsEntry,
                                         tCapwapIsSetFsCawapConfigStatsEntry *
                                         pCapwapIsSetFsCawapConfigStatsEntry)
{

    UNUSED_PARAM (pCapwapOldFsCawapConfigStatsEntry);
    UNUSED_PARAM (pCapwapFsCawapConfigStatsEntry);
    UNUSED_PARAM (pCapwapIsSetFsCawapConfigStatsEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  CapwapUtilUpdateFsCawapRunStatsTable
 * Input       :   pCapwapOldFsCawapRunStatsEntry
                   pCapwapFsCawapRunStatsEntry
                   pCapwapIsSetFsCawapRunStatsEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapUtilUpdateFsCawapRunStatsTable (tCapwapFsCawapRunStatsEntry *
                                      pCapwapOldFsCawapRunStatsEntry,
                                      tCapwapFsCawapRunStatsEntry *
                                      pCapwapFsCawapRunStatsEntry,
                                      tCapwapIsSetFsCawapRunStatsEntry *
                                      pCapwapIsSetFsCawapRunStatsEntry)
{

    UNUSED_PARAM (pCapwapOldFsCawapRunStatsEntry);
    UNUSED_PARAM (pCapwapFsCawapRunStatsEntry);
    UNUSED_PARAM (pCapwapIsSetFsCawapRunStatsEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  CapwapUtilUpdateFsCapwapWirelessBindingTable
 * Input       :   pCapwapOldFsCapwapWirelessBindingEntry
                   pCapwapFsCapwapWirelessBindingEntry
                   pCapwapIsSetFsCapwapWirelessBindingEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    CapwapUtilUpdateFsCapwapWirelessBindingTable
    (tCapwapFsCapwapWirelessBindingEntry *
     pCapwapOldFsCapwapWirelessBindingEntry,
     tCapwapFsCapwapWirelessBindingEntry * pCapwapFsCapwapWirelessBindingEntry,
     tCapwapIsSetFsCapwapWirelessBindingEntry *
     pCapwapIsSetFsCapwapWirelessBindingEntry)
{
    tRadioIfMsgStruct   RadioIfMsgStruct;

    MEMSET (&RadioIfMsgStruct, 0, sizeof (tRadioIfMsgStruct));

    UNUSED_PARAM (pCapwapOldFsCapwapWirelessBindingEntry);

    if (pCapwapIsSetFsCapwapWirelessBindingEntry->
        bFsCapwapWirelessBindingRowStatus != OSIX_FALSE)
    {
        if (pCapwapFsCapwapWirelessBindingEntry->MibObject.
            i4FsCapwapWirelessBindingRowStatus == DESTROY)
        {
            RadioIfMsgStruct.unRadioIfMsg.RadioIfVirtualIntfCreate.isPresent =
                OSIX_TRUE;
            RadioIfMsgStruct.unRadioIfMsg.RadioIfVirtualIntfCreate.u4IfIndex =
                (UINT4) (pCapwapFsCapwapWirelessBindingEntry->MibObject.
                         i4FsCapwapWirelessBindingVirtualRadioIfIndex);

            if (WssIfProcessRadioIfMsg
                (WSS_RADIOIF_DELETE_VIRTUAL_RADIO_MSG,
                 &RadioIfMsgStruct) != OSIX_SUCCESS)
            {
                return OSIX_FAILURE;
            }
        }
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  CapwapUtilUpdateFsCapwapStationWhiteList
 * Input       :   pCapwapOldFsCapwapStationWhiteListEntry
                   pCapwapFsCapwapStationWhiteListEntry
                   pCapwapIsSetFsCapwapStationWhiteListEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapUtilUpdateFsCapwapStationWhiteList (tCapwapFsCapwapStationWhiteListEntry
                                          *
                                          pCapwapOldFsCapwapStationWhiteListEntry,
                                          tCapwapFsCapwapStationWhiteListEntry *
                                          pCapwapFsCapwapStationWhiteListEntry,
                                          tCapwapIsSetFsCapwapStationWhiteListEntry
                                          *
                                          pCapwapIsSetFsCapwapStationWhiteListEntry)
{
    tWssIfCapDB        *pWssIfCapDB = NULL;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapDB, 0, sizeof (tWssIfCapDB));

    MEMCPY (pWssIfCapDB->BaseMacAddress,
            pCapwapFsCapwapStationWhiteListEntry->MibObject.
            au1FsCapwapStationWhiteListStationId,
            pCapwapFsCapwapStationWhiteListEntry->MibObject.
            i4FsCapwapStationWhiteListStationIdLen);

    if ((pCapwapFsCapwapStationWhiteListEntry->MibObject.
         i4FsCapwapStationWhiteListRowStatus == CREATE_AND_GO) ||
        (pCapwapFsCapwapStationWhiteListEntry->MibObject.
         i4FsCapwapStationWhiteListRowStatus == ACTIVE))
    {
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_CREATE_STA_WHITELIST_ENTRY,
                                     pWssIfCapDB) != OSIX_SUCCESS)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "CapwapUtilUpdateFsCapwapStationWhiteList "
                        ": Sta WhiteList Addition failed\r\n");
            return OSIX_FAILURE;
        }
    }
    else if (pCapwapFsCapwapStationWhiteListEntry->MibObject.
             i4FsCapwapStationWhiteListRowStatus == DESTROY)
    {
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_DELETE_STA_WHITELIST_ENTRY,
                                     pWssIfCapDB) != OSIX_SUCCESS)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "CapwapUtilUpdateFsCapwapStationWhiteList "
                        ": Sta WhiteList Deletion failed\r\n");
            return OSIX_FAILURE;
        }
    }

    UNUSED_PARAM (pCapwapOldFsCapwapStationWhiteListEntry);
    UNUSED_PARAM (pCapwapIsSetFsCapwapStationWhiteListEntry);
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  CapwapUtilUpdateFsCapwapWtpRebootStatisticsTable
 * Input       :   pCapwapOldFsCapwapWtpRebootStatisticsEntry
                   pCapwapFsCapwapWtpRebootStatisticsEntry
                   pCapwapIsSetFsCapwapWtpRebootStatisticsEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    CapwapUtilUpdateFsCapwapWtpRebootStatisticsTable
    (tCapwapFsCapwapWtpRebootStatisticsEntry *
     pCapwapOldFsCapwapWtpRebootStatisticsEntry,
     tCapwapFsCapwapWtpRebootStatisticsEntry *
     pCapwapFsCapwapWtpRebootStatisticsEntry,
     tCapwapIsSetFsCapwapWtpRebootStatisticsEntry *
     pCapwapIsSetFsCapwapWtpRebootStatisticsEntry)
{

    UNUSED_PARAM (pCapwapOldFsCapwapWtpRebootStatisticsEntry);

#ifdef WLC_WANTED
    tWtpRebootStats     WssPmRebootStats;
    tWssPmWTPIDStatsProcessRBDB *pWtpIdStatsMap = NULL;
    UINT2               u2TmpWtpProfiledId, u2WtpIntProfileIndex = 0;
#endif /* WLC_WANTED */

#ifdef WTP_WANTED
    tWssIfApHdlrDB      WssIfApHdlrDB;
#endif /* WTP_WANTED */

#ifdef WLC_WANTED
    /* assuming, wtp-profile id is always set, as its index */
    u2TmpWtpProfiledId =
        (UINT2) (pCapwapFsCapwapWtpRebootStatisticsEntry->MibObject.
                 u4CapwapBaseWtpProfileId);
    u2WtpIntProfileIndex =
        (UINT2) (CapwapGetInternalProfildId
                 (&u2TmpWtpProfiledId, &u2WtpIntProfileIndex));

    MEMSET (&WssPmRebootStats, 0, sizeof (tWtpRebootStats));
    /* get, existing stats if-any to not overwrite it */
    pWtpIdStatsMap =
        (tWssPmWTPIDStatsProcessRBDB *)
        WssIfWtpIDStatsEntryGet (u2WtpIntProfileIndex);
    if (NULL != pWtpIdStatsMap)
    {
        /* copy from the previous stats */
        MEMCPY (&WssPmRebootStats, &pWtpIdStatsMap->pWtpRebootStat,
                sizeof (tWtpRebootStats));
    }

    if (pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
        bFsCapwapWtpRebootStatisticsRebootCount != OSIX_FALSE)
    {
        WssPmRebootStats.u2RebootCount =
            (UINT2) (pCapwapFsCapwapWtpRebootStatisticsEntry->MibObject.
                     u4FsCapwapWtpRebootStatisticsRebootCount);
    }
    if (pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
        bFsCapwapWtpRebootStatisticsAcInitiatedCount != OSIX_FALSE)
    {
        WssPmRebootStats.u2AcInitiatedCount =
            (UINT2) (pCapwapFsCapwapWtpRebootStatisticsEntry->MibObject.
                     u4FsCapwapWtpRebootStatisticsAcInitiatedCount);
    }
    if (pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
        bFsCapwapWtpRebootStatisticsLinkFailureCount != OSIX_FALSE)
    {
        WssPmRebootStats.u2LinkFailCount =
            (UINT2) (pCapwapFsCapwapWtpRebootStatisticsEntry->MibObject.
                     u4FsCapwapWtpRebootStatisticsLinkFailureCount);
    }
    if (pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
        bFsCapwapWtpRebootStatisticsSwFailureCount != OSIX_FALSE)
    {
        WssPmRebootStats.u2SwFailCount =
            (UINT2) (pCapwapFsCapwapWtpRebootStatisticsEntry->MibObject.
                     u4FsCapwapWtpRebootStatisticsSwFailureCount);
    }
    if (pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
        bFsCapwapWtpRebootStatisticsHwFailureCount != OSIX_FALSE)
    {
        WssPmRebootStats.u2HwFailCount =
            (UINT2) (pCapwapFsCapwapWtpRebootStatisticsEntry->MibObject.
                     u4FsCapwapWtpRebootStatisticsHwFailureCount);
    }
    if (pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
        bFsCapwapWtpRebootStatisticsOtherFailureCount != OSIX_FALSE)
    {
        WssPmRebootStats.u2OtherFailCount =
            (UINT2) (pCapwapFsCapwapWtpRebootStatisticsEntry->MibObject.
                     u4FsCapwapWtpRebootStatisticsOtherFailureCount);
    }
    if (pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
        bFsCapwapWtpRebootStatisticsUnknownFailureCount != OSIX_FALSE)
    {
        WssPmRebootStats.u2UnknownFailCount =
            (UINT2) (pCapwapFsCapwapWtpRebootStatisticsEntry->MibObject.
                     u4FsCapwapWtpRebootStatisticsUnknownFailureCount);
    }
    if (pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
        bFsCapwapWtpRebootStatisticsLastFailureType != OSIX_FALSE)
    {
        WssPmRebootStats.u1LastFailureType =
            (UINT1) (pCapwapFsCapwapWtpRebootStatisticsEntry->MibObject.
                     i4FsCapwapWtpRebootStatisticsLastFailureType);
    }

    if (WssPmRbtStatsEntrySet (u2WtpIntProfileIndex, &WssPmRebootStats) !=
        OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapUtilUpdateFsCapwapWtpRebootStatisticsTable: Failed to update DB \r\n");
        return OSIX_FAILURE;
    }
#endif /* WLC_WANTED */

#ifdef WTP_WANTED
    MEMSET (&WssIfApHdlrDB, 0, sizeof (tWssIfApHdlrDB));
    /* get, existing stats if-any to not overwrite it */
    WssIfProcessApHdlrDBMsg (WSS_APHDLR_GET_WTPREBOOT_STATS, &WssIfApHdlrDB);

    if (pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
        bFsCapwapWtpRebootStatisticsRebootCount != OSIX_FALSE)
    {
        WssIfApHdlrDB.u2RebootCount =
            pCapwapFsCapwapWtpRebootStatisticsEntry->MibObject.
            u4FsCapwapWtpRebootStatisticsRebootCount;
    }
    if (pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
        bFsCapwapWtpRebootStatisticsAcInitiatedCount != OSIX_FALSE)
    {
        WssIfApHdlrDB.u2AcInitiatedCount =
            pCapwapFsCapwapWtpRebootStatisticsEntry->MibObject.
            u4FsCapwapWtpRebootStatisticsAcInitiatedCount;
    }
    if (pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
        bFsCapwapWtpRebootStatisticsLinkFailureCount != OSIX_FALSE)
    {
        WssIfApHdlrDB.u2LinkFailCount =
            pCapwapFsCapwapWtpRebootStatisticsEntry->MibObject.
            u4FsCapwapWtpRebootStatisticsLinkFailureCount;
    }
    if (pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
        bFsCapwapWtpRebootStatisticsSwFailureCount != OSIX_FALSE)
    {
        WssIfApHdlrDB.u2SwFailCount =
            pCapwapFsCapwapWtpRebootStatisticsEntry->MibObject.
            u4FsCapwapWtpRebootStatisticsSwFailureCount;
    }
    if (pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
        bFsCapwapWtpRebootStatisticsHwFailureCount != OSIX_FALSE)
    {
        WssIfApHdlrDB.u2HwFailCount =
            pCapwapFsCapwapWtpRebootStatisticsEntry->MibObject.
            u4FsCapwapWtpRebootStatisticsHwFailureCount;
    }
    if (pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
        bFsCapwapWtpRebootStatisticsOtherFailureCount != OSIX_FALSE)
    {
        WssIfApHdlrDB.u2OtherFailCount =
            pCapwapFsCapwapWtpRebootStatisticsEntry->MibObject.
            u4FsCapwapWtpRebootStatisticsOtherFailureCount;
    }
    if (pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
        bFsCapwapWtpRebootStatisticsUnknownFailureCount != OSIX_FALSE)
    {
        WssIfApHdlrDB.u2UnknownFailCount =
            pCapwapFsCapwapWtpRebootStatisticsEntry->MibObject.
            u4FsCapwapWtpRebootStatisticsUnknownFailureCount;
    }
    if (pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
        bFsCapwapWtpRebootStatisticsLastFailureType != OSIX_FALSE)
    {
        WssIfApHdlrDB.u1LastFailureType =
            pCapwapFsCapwapWtpRebootStatisticsEntry->MibObject.
            i4FsCapwapWtpRebootStatisticsLastFailureType;
    }

    if (WssIfProcessApHdlrDBMsg (WSS_APHDLR_SET_WTPREBOOT_STATS, &WssIfApHdlrDB)
        != OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapUtilUpdateFsCapwapWtpRebootStatisticsTable: Failed to update DB \r\n");
        return OSIX_FAILURE;
    }
#endif /* WTP_WANTED */

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  CapwapUtilUpdateFsCapwapWtpRadioStatisticsTable
 * Input       :   pCapwapOldFsCapwapWtpRadioStatisticsEntry
                   pCapwapFsCapwapWtpRadioStatisticsEntry
                   pCapwapIsSetFsCapwapWtpRadioStatisticsEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    CapwapUtilUpdateFsCapwapWtpRadioStatisticsTable
    (tCapwapFsCapwapWtpRadioStatisticsEntry *
     pCapwapOldFsCapwapWtpRadioStatisticsEntry,
     tCapwapFsCapwapWtpRadioStatisticsEntry *
     pCapwapFsCapwapWtpRadioStatisticsEntry,
     tCapwapIsSetFsCapwapWtpRadioStatisticsEntry *
     pCapwapIsSetFsCapwapWtpRadioStatisticsEntry)
{

    UNUSED_PARAM (pCapwapOldFsCapwapWtpRadioStatisticsEntry);
#ifdef WLC_WANTED
    tWtpRadioStats      WssPmRadioStats;
    tWssPmWTPIDRadIDStatsProcessRBDB *pwtpIDRadIDStatsMap = NULL;
    UINT4               u4RadioIfIndex = 0;
#endif /* WLC_WANTED */

#ifdef WTP_WANTED
    tRadioIfGetDB       RadioIfGetDB;
#endif /* WTP_WANTED */

#ifdef WLC_WANTED
    u4RadioIfIndex = (UINT4)
        pCapwapFsCapwapWtpRadioStatisticsEntry->MibObject.i4RadioIfIndex;
    MEMSET (&WssPmRadioStats, 0, sizeof (tWtpRadioStats));
    /* get, existing stats if-any to not overwrite it */
    pwtpIDRadIDStatsMap =
        (tWssPmWTPIDRadIDStatsProcessRBDB *)
        WssIfWtpIDRadIDStatsEntryGet ((UINT2) u4RadioIfIndex);
    if (NULL != pwtpIDRadIDStatsMap)
    {
        /* copy from the previous stats */
        MEMCPY (&WssPmRadioStats, &pwtpIDRadIDStatsMap->pWtpRadioStats,
                sizeof (tWtpRadioStats));
    }

    if (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
        bFsCapwapWtpRadioLastFailType != OSIX_FALSE)
    {
        WssPmRadioStats.u1LastFailureType =
            (UINT1) (pCapwapFsCapwapWtpRadioStatisticsEntry->MibObject.
                     i4FsCapwapWtpRadioLastFailType);
    }
    if (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
        bFsCapwapWtpRadioResetCount != OSIX_FALSE)
    {
        WssPmRadioStats.u2ResetCount =
            (UINT2) (pCapwapFsCapwapWtpRadioStatisticsEntry->MibObject.
                     u4FsCapwapWtpRadioResetCount);
    }
    if (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
        bFsCapwapWtpRadioSwFailureCount != OSIX_FALSE)
    {
        WssPmRadioStats.u2SwFailCount =
            (UINT2) (pCapwapFsCapwapWtpRadioStatisticsEntry->MibObject.
                     u4FsCapwapWtpRadioSwFailureCount);
    }
    if (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
        bFsCapwapWtpRadioHwFailureCount != OSIX_FALSE)
    {
        WssPmRadioStats.u2HwFailCount =
            (UINT2) (pCapwapFsCapwapWtpRadioStatisticsEntry->MibObject.
                     u4FsCapwapWtpRadioHwFailureCount);
    }
    if (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
        bFsCapwapWtpRadioOtherFailureCount != OSIX_FALSE)
    {
        WssPmRadioStats.u2OtherFailureCount =
            (UINT2) (pCapwapFsCapwapWtpRadioStatisticsEntry->MibObject.
                     u4FsCapwapWtpRadioOtherFailureCount);
    }
    if (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
        bFsCapwapWtpRadioUnknownFailureCount != OSIX_FALSE)
    {
        WssPmRadioStats.u2UnknownFailureCount =
            (UINT2) (pCapwapFsCapwapWtpRadioStatisticsEntry->MibObject.
                     u4FsCapwapWtpRadioUnknownFailureCount);
    }
    if (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
        bFsCapwapWtpRadioConfigUpdateCount != OSIX_FALSE)
    {
        WssPmRadioStats.u2ConfigUpdateCount =
            (UINT2) (pCapwapFsCapwapWtpRadioStatisticsEntry->MibObject.
                     u4FsCapwapWtpRadioConfigUpdateCount);
    }
    if (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
        bFsCapwapWtpRadioChannelChangeCount != OSIX_FALSE)
    {
        WssPmRadioStats.u2ChannelChangeCount =
            (UINT2) (pCapwapFsCapwapWtpRadioStatisticsEntry->MibObject.
                     u4FsCapwapWtpRadioChannelChangeCount);
    }
    if (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
        bFsCapwapWtpRadioBandChangeCount != OSIX_FALSE)
    {
        WssPmRadioStats.u2BandChangeCount =
            (UINT2) (pCapwapFsCapwapWtpRadioStatisticsEntry->MibObject.
                     u4FsCapwapWtpRadioBandChangeCount);
    }
    if (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
        bFsCapwapWtpRadioCurrentNoiseFloor != OSIX_FALSE)
    {
        WssPmRadioStats.u2CurrentNoiseFloor =
            (UINT2) (pCapwapFsCapwapWtpRadioStatisticsEntry->MibObject.
                     u4FsCapwapWtpRadioCurrentNoiseFloor);
    }

    if (WssIfWtpIDRadIDStatsEntrySet (u4RadioIfIndex, &WssPmRadioStats) !=
        OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapUtilUpdateFsCapwapWtpRadioStatisticsTable: Failed to update DB \r\n");
        return OSIX_FAILURE;
    }
#endif /* WLC_WANTED */

#ifdef WTP_WANTED
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    /* radio-id is passed in case of wtp in the mib-object */
    RadioIfGetDB.RadioIfGetAllDB.u1RadioId =
        pCapwapFsCapwapWtpRadioStatisticsEntry->MibObject.i4RadioIfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioStats = OSIX_TRUE;
    /* get, existing stats if-any to not overwrite it */
    if (WssIfProcessRadioIfDBMsg (WSS_GET_PHY_RADIO_IF_DB, &RadioIfGetDB) !=
        OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    " WssIfProcessRadioIfDBMsg : Failed \r\n");
        return OSIX_FAILURE;
    }

    if (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
        bFsCapwapWtpRadioLastFailType != OSIX_FALSE)
    {
        RadioIfGetDB.RadioIfGetAllDB.radioStats.u1LastFailureType =
            pCapwapFsCapwapWtpRadioStatisticsEntry->MibObject.
            i4FsCapwapWtpRadioLastFailType;
    }
    if (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
        bFsCapwapWtpRadioResetCount != OSIX_FALSE)
    {
        RadioIfGetDB.RadioIfGetAllDB.radioStats.u2ResetCount =
            pCapwapFsCapwapWtpRadioStatisticsEntry->MibObject.
            u4FsCapwapWtpRadioResetCount;
    }
    if (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
        bFsCapwapWtpRadioSwFailureCount != OSIX_FALSE)
    {
        RadioIfGetDB.RadioIfGetAllDB.radioStats.u2SwFailCount =
            pCapwapFsCapwapWtpRadioStatisticsEntry->MibObject.
            u4FsCapwapWtpRadioSwFailureCount;
    }
    if (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
        bFsCapwapWtpRadioHwFailureCount != OSIX_FALSE)
    {
        RadioIfGetDB.RadioIfGetAllDB.radioStats.u2HwFailCount =
            pCapwapFsCapwapWtpRadioStatisticsEntry->MibObject.
            u4FsCapwapWtpRadioHwFailureCount;
    }
    if (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
        bFsCapwapWtpRadioOtherFailureCount != OSIX_FALSE)
    {
        RadioIfGetDB.RadioIfGetAllDB.radioStats.u2OtherFailureCount =
            pCapwapFsCapwapWtpRadioStatisticsEntry->MibObject.
            u4FsCapwapWtpRadioOtherFailureCount;
    }
    if (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
        bFsCapwapWtpRadioUnknownFailureCount != OSIX_FALSE)
    {
        RadioIfGetDB.RadioIfGetAllDB.radioStats.u2UnknownFailureCount =
            pCapwapFsCapwapWtpRadioStatisticsEntry->MibObject.
            u4FsCapwapWtpRadioUnknownFailureCount;
    }
    if (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
        bFsCapwapWtpRadioConfigUpdateCount != OSIX_FALSE)
    {
        RadioIfGetDB.RadioIfGetAllDB.radioStats.u2ConfigUpdateCount =
            pCapwapFsCapwapWtpRadioStatisticsEntry->MibObject.
            u4FsCapwapWtpRadioConfigUpdateCount;
    }
    if (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
        bFsCapwapWtpRadioChannelChangeCount != OSIX_FALSE)
    {
        RadioIfGetDB.RadioIfGetAllDB.radioStats.u2ChannelChangeCount =
            pCapwapFsCapwapWtpRadioStatisticsEntry->MibObject.
            u4FsCapwapWtpRadioChannelChangeCount;
    }
    if (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
        bFsCapwapWtpRadioBandChangeCount != OSIX_FALSE)
    {
        RadioIfGetDB.RadioIfGetAllDB.radioStats.u2BandChangeCount =
            pCapwapFsCapwapWtpRadioStatisticsEntry->MibObject.
            u4FsCapwapWtpRadioBandChangeCount;
    }
    if (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
        bFsCapwapWtpRadioCurrentNoiseFloor != OSIX_FALSE)
    {
        RadioIfGetDB.RadioIfGetAllDB.radioStats.u2CurrentNoiseFloor =
            pCapwapFsCapwapWtpRadioStatisticsEntry->MibObject.
            u4FsCapwapWtpRadioCurrentNoiseFloor;
    }

    if (WssIfProcessRadioIfDBMsg (WSS_SET_PHY_RADIO_IF_DB, &RadioIfGetDB) !=
        OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapUtilUpdateFsCapwapWtpRadioStatisticsTable: Failed to update DB \r\n");
        return OSIX_FAILURE;
    }
#endif /* WTP_WANTED */

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  CapwapUtilGetBaseWtpProfileName
 * Input       :  u4CapwapBaseWtpProfileId
 * Descritpion :  This Routine checks gets the WTP Profile name
 * Output      :  pRetValCapwapBaseWtpProfileName
 *                ModelNumber
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapUtilGetBaseWtpProfileName (UINT4 u4CapwapBaseWtpProfileId,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pRetValCapwapBaseWtpProfileName,
                                 UINT1 *ModelNumber)
{
    tCapwapCapwapBaseWtpProfileEntry *pCapwapCapwapBaseWtpProfileEntry = NULL;

    pCapwapCapwapBaseWtpProfileEntry =
        (tCapwapCapwapBaseWtpProfileEntry *)
        MemAllocMemBlk (CAPWAP_CAPWAPBASEWTPPROFILETABLE_POOLID);

    if (pCapwapCapwapBaseWtpProfileEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pCapwapCapwapBaseWtpProfileEntry, 0,
            sizeof (tCapwapCapwapBaseWtpProfileEntry));

    /* Assign the index */
    pCapwapCapwapBaseWtpProfileEntry->MibObject.u4CapwapBaseWtpProfileId =
        u4CapwapBaseWtpProfileId;
    if (CapwapGetAllCapwapBaseWtpProfileTable (pCapwapCapwapBaseWtpProfileEntry)
        != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (CAPWAP_CAPWAPBASEWTPPROFILETABLE_POOLID,
                            (UINT1 *) pCapwapCapwapBaseWtpProfileEntry);
        return SNMP_FAILURE;
    }
    if (STRCMP
        (pCapwapCapwapBaseWtpProfileEntry->MibObject.
         au1CapwapBaseWtpProfileWtpModelNumber, ModelNumber) == 0)
    {
        /* Assign the value */
        MEMCPY (pRetValCapwapBaseWtpProfileName->pu1_OctetList,
                pCapwapCapwapBaseWtpProfileEntry->MibObject.
                au1CapwapBaseWtpProfileName,
                pCapwapCapwapBaseWtpProfileEntry->MibObject.
                i4CapwapBaseWtpProfileNameLen);
        pRetValCapwapBaseWtpProfileName->i4_Length =
            pCapwapCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileNameLen;
    }
    else
    {
        return SNMP_FAILURE;
    }
    MemReleaseMemBlock (CAPWAP_CAPWAPBASEWTPPROFILETABLE_POOLID,
                        (UINT1 *) pCapwapCapwapBaseWtpProfileEntry);

    return SNMP_SUCCESS;
}

/****************************************************************************
 * Function    :  CapwapShowApConfiguration
 * Input       :  
 * Descritpion :  This Routine displays the ap configuration
 * Output      :  
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
CapwapShowApConfiguration (tCliHandle CliHandle,
                           UINT4 u4FsRadioType,
                           UINT1 *pu1CapwapBaseWtpProfileName, UINT4 u4RadioId)
{
#if defined(WSSCFG_WANTED) && defined(CLI_WANTED)
    if (WssCfgShowApConfiguration
        (CliHandle, u4FsRadioType, pu1CapwapBaseWtpProfileName,
         u4RadioId) != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }
#else
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (u4FsRadioType);
    UNUSED_PARAM (pu1CapwapBaseWtpProfileName);
    UNUSED_PARAM (u4RadioId);
#endif
    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    :  CapwapShowApTraffic
 * Input       :
 * Descritpion :  This Routine displays the ap configuration
 * Output      :
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
CapwapShowApTraffic (tCliHandle CliHandle, UINT4 *pu4CapwapBaseWtpProfileId)
{
    UINT4               u4WtpProfileId = 0;
    UINT4               u4SentBytes = 0;
    UINT4               u4RcvdBytes = 0;
    UINT4               u4SentTrafficRate = 0;
    UINT4               u4RcvdTrafficRate = 0;
    UINT4               u4TotalClient = 0;

    u4WtpProfileId = *pu4CapwapBaseWtpProfileId;

    if (nmhGetFsCapwATPStatsSentBytes
        (u4WtpProfileId, &u4SentBytes) == SNMP_SUCCESS)
    {
    }
    if (nmhGetFsCapwATPStatsRcvdBytes
        (u4WtpProfileId, &u4RcvdBytes) == SNMP_SUCCESS)
    {
    }
    if (nmhGetFsCapwATPStatsSentTrafficRate
        (u4WtpProfileId, &u4SentTrafficRate) == SNMP_SUCCESS)
    {
    }
    if (nmhGetFsCapwATPStatsRcvdTrafficRate
        (u4WtpProfileId, &u4RcvdTrafficRate) == SNMP_SUCCESS)
    {
    }
    if (nmhGetFsCapwATPStatsTotalClient
        (u4WtpProfileId, &u4TotalClient) == SNMP_SUCCESS)
    {
    }

    CliPrintf (CliHandle, "\r\nSentBytes                  : %u bytes",
               u4SentBytes);
    CliPrintf (CliHandle, "\r\nRcvdBytes                  : %u bytes",
               u4RcvdBytes);
    CliPrintf (CliHandle, "\r\nSentTrafficRate            : %u bps",
               u4SentTrafficRate);
    CliPrintf (CliHandle, "\r\nRcvdTrafficRate            : %u bps",
               u4RcvdTrafficRate);
    CliPrintf (CliHandle, "\r\nTotalClient                : %d", u4TotalClient);
    CliPrintf (CliHandle, "\r\n");
    return CLI_SUCCESS;

}

/****************************************************************************
 * Function    :  CapwapUtilGetReportInterval
 * Input       :  u4RadioId - Radio Id
 * Descritpion :  This Routine gets the report Interval
 * Output      :  pu2ReportInterval - Report Interval
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapUtilGetReportInterval (UINT4 u4RadioId, UINT2 *pu2ReportInterval)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
        UtlShMemAllocAntennaSelectionBuf ();

    if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC, "\r%% Memory allocation for Antenna"
                    " selection failed.\r\n");
        return OSIX_FAILURE;
    }

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = u4RadioId;
    RadioIfGetDB.RadioIfIsGetAllDB.bReportInterval = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, (&RadioIfGetDB))
        != OSIX_SUCCESS)
    {
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                         pu1AntennaSelection);
        return OSIX_FAILURE;
    }
    *pu2ReportInterval = RadioIfGetDB.RadioIfGetAllDB.u2ReportInterval;
    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                     pu1AntennaSelection);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  CapwapUtilGetTimeForTicks
 * Input       :   u4CapwapTicks - Time in Ticks
 * Descritpion :  This Routine converts the time in ticks to string
 * Output      :  CapwapTime - Time is string format
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapUtilGetTimeForTicks (UINT4 u4CapwapTicks, UINT1 *CapwapTime)
{
    tUtlTm              tm;
    UINT1               au1Hrs[3], au1Min[3], au1Sec[3], au1Day[3], au1Mon[3],
        au1Year[5], au1Month[4];
    MEMSET (&tm, 0, sizeof (tUtlTm));

    UtlGetTimeForTicks (u4CapwapTicks, &tm);
    tm.tm_mon++;
    if (tm.tm_mon <= 9)
    {
        SNPRINTF ((CHR1 *) au1Mon, 3, "0%u", tm.tm_mon);
    }
    else
    {
        SNPRINTF ((CHR1 *) au1Mon, 3, "%u", tm.tm_mon);
    }
    if (tm.tm_mday <= 9)
    {
        SNPRINTF ((CHR1 *) au1Day, 3, "0%u", tm.tm_mday);
    }
    else
    {
        SNPRINTF ((CHR1 *) au1Day, 3, "%u", tm.tm_mday);
    }
    if (tm.tm_hour <= 9)
    {
        SNPRINTF ((CHR1 *) au1Hrs, 3, "0%u", tm.tm_hour);
    }
    else
    {
        SNPRINTF ((CHR1 *) au1Hrs, 3, "%u", tm.tm_hour);
    }

    if (tm.tm_min <= 9)
    {
        SNPRINTF ((CHR1 *) au1Min, 3, "0%u", tm.tm_min);
    }
    else
    {
        SNPRINTF ((CHR1 *) au1Min, 3, "%u", tm.tm_min);
    }

    if (tm.tm_sec <= 9)
    {
        SNPRINTF ((CHR1 *) au1Sec, 3, "0%u", tm.tm_sec);
    }
    else
    {
        SNPRINTF ((CHR1 *) au1Sec, 3, "%u", tm.tm_sec);
    }
    SNPRINTF ((CHR1 *) au1Year, 5, "%u", tm.tm_year);
    switch (tm.tm_mon)
    {
        case 1:
            STRCPY (au1Month, "Jan");
            break;
        case 2:
            STRCPY (au1Month, "Feb");
            break;
        case 3:
            STRCPY (au1Month, "Mar");
            break;
        case 4:
            STRCPY (au1Month, "Apr");
            break;
        case 5:
            STRCPY (au1Month, "May");
            break;
        case 6:
            STRCPY (au1Month, "Jun");
            break;
        case 7:
            STRCPY (au1Month, "Jul");
            break;
        case 8:
            STRCPY (au1Month, "Aug");
            break;
        case 9:
            STRCPY (au1Month, "Sep");
            break;
        case 10:
            STRCPY (au1Month, "Oct");
            break;
        case 11:
            STRCPY (au1Month, "Nov");
            break;
        case 12:
            STRCPY (au1Month, "Dec");
            break;
        default:
            break;
    }
    SPRINTF ((CHR1 *) CapwapTime, "%s %s %s %s:%s:%s",
             au1Day, au1Month, au1Year, au1Hrs, au1Min, au1Sec);

    return OSIX_SUCCESS;
}

/***************************************************************************/
/* Function Name     : CapwapTmToSec                                       */
/* Description       : This procedure converts the time in tUtlTm struct  */
/*                     to seconds since Capwap Base Year(1900)             */
/* Input(s)          : tm - Time in tUtlTm structure                     */
/* Output(s)         : None                                              */
/* Returns           : u4Secs -Time in seconds since Capwap Base Year(1900)*/
/*************************************************************************/
UINT4
CapwapTmToSec (tUtlTm * tm)
{
    UINT4               u4Year = CAP_SNTP_REF_BASE_YEAR;
    UINT4               u4Secs = 0;

    while (u4Year < tm->tm_year)
    {
        u4Secs += CAP_SECS_IN_YEAR (u4Year);
        ++u4Year;
    }

    /*For SECS_IN_DAY, seconds should be calculated from the previous
     * day for current day*/
    u4Secs += ((tm->tm_yday - 1) * CAP_SECS_IN_DAY);
    u4Secs += (tm->tm_hour * CAP_SECS_IN_HOUR);
    u4Secs += (tm->tm_min * CAP_SECS_IN_MINUTE);
    u4Secs += (tm->tm_sec);
    return u4Secs;
}
