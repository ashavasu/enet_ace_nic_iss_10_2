/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: capwapsz.c,v 1.1.1.1 2016/09/20 10:42:38 siva Exp $
 *
 * Description: This file contains the CAPWAP MemPool Init routines.
 *
 *****************************************************************************/

#define _CAPWAPSZ_C
#include "capwapinc.h"
extern INT4  IssSzRegisterModuleSizingParams( CHR1 *pu1ModName, tFsModSizingParams *pModSizingParams); 
extern INT4  IssSzRegisterModulePoolId( CHR1 *pu1ModName, tMemPoolId *pModPoolId); 
INT4  CapwapSizingMemCreateMemPools()
{
    INT4 i4RetVal;
    INT4 i4SizingId;

    for( i4SizingId=0; i4SizingId < CAPWAP_MAX_SIZING_ID; i4SizingId++) {
        i4RetVal = (INT4)MemCreateMemPool( 
                          FsCAPWAPSizingParams[i4SizingId].u4StructSize,
                          FsCAPWAPSizingParams[i4SizingId].u4PreAllocatedUnits,
                          MEM_DEFAULT_MEMORY_TYPE,
                          &(CAPWAPMemPoolIds[ i4SizingId]));
        if( i4RetVal == (INT4) MEM_FAILURE) {
            CapwapSizingMemDeleteMemPools();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}


INT4   CapwapSzRegisterModuleSizingParams( CHR1 *pu1ModName)
{
      /* Copy the Module Name */ 
       IssSzRegisterModuleSizingParams( pu1ModName, FsCAPWAPSizingParams); 
      IssSzRegisterModulePoolId( pu1ModName, CAPWAPMemPoolIds); 
      return OSIX_SUCCESS; 
}


VOID  CapwapSizingMemDeleteMemPools()
{
    INT4 i4SizingId;

    for( i4SizingId=0; i4SizingId < CAPWAP_MAX_SIZING_ID; i4SizingId++) {
        if(CAPWAPMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool( CAPWAPMemPoolIds[ i4SizingId] );
            CAPWAPMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
