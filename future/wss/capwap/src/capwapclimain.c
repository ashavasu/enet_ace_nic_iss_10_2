/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: capwapclimain.c,v 1.2 2017/12/08 10:16:24 siva Exp $
* Description: This file contains the capwap task main loop
*              and the initialization routines.
*
*******************************************************************/
#define __CAPWAPMAIN_C__
#include "capwapinc.h"
#include "wsscfgtdfsg.h"
#include "wsscfgtdfs.h"
#include "wsscfgprotg.h"

/****************************************************************************
*                                                                           *
* Function     : CapwapMainTaskInit                                           *
*                                                                           *
* Description  : CAPWAP initialization routine.                               *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : OSIX_SUCCESS, if initialization succeeds                   *
*                OSIX_FAILURE, otherwise                                    *
*                                                                           *
*****************************************************************************/

PUBLIC UINT4
CapwapMainTaskInit (VOID)
{
    CAPWAP_FN_ENTRY ();
    UINT4               u4FsCapwapControlUdpPort = 0;
    MEMSET (&gCapwapGlobals, 0, sizeof (gCapwapGlobals));
    MEMCPY (gCapwapGlobals.au1TaskSemName, CAPWAP_MUT_EXCL_SEM_NAME,
            OSIX_NAME_LEN);

    /* Initializing default value for CAPWAP  Trap Status Object as per MIB defintion */
    gCapwapGlobals.u4CapwTrapStatus = CAPWAP_WTP_DEFAULT_TRAP_STATUS;

    /* Initializing default value for CAPWAP  Critical Trap Status Object as per MIB defintion */
    gCapwapGlobals.u4CapwCriticalTrapStatus =
        CAPWAP_WTP_DEFAULT_CRITICAL_TRAP_STATUS;

    if (OsixCreateSem
        (CAPWAP_MUT_EXCL_SEM_NAME, CAPWAP_SEM_CREATE_INIT_CNT, 0,
         &gCapwapGlobals.capwapTaskSemId) == OSIX_FAILURE)
    {
        CAPWAP_TRC1 (CAPWAP_FAILURE_TRC,
                     "Seamphore Creation failure for %s \n",
                     CAPWAP_MUT_EXCL_SEM_NAME);
        return OSIX_FAILURE;
    }
/*****DEBUG */
    CAPWAP_TRC1 (CAPWAP_MGMT_TRC,
                 "Capwap Task Semid successfully created %d \n",
                 gCapwapGlobals.capwapTaskSemId);
/*****DEBUG */
    if (CapwapUtlCreateRBTree () == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    RegisterSTDWTP ();
    RegisterFSCAPW ();

    /* The following routine initialises the timer descriptor structure */
    CapwapUtilInitGlobals ();

/* Update WLC UDP Port number to default Value  */
    u4FsCapwapControlUdpPort = CAPWAP_UDP_PORT_CONTROL;

    if (CapwapSetFsCapwapControlUdpPort (u4FsCapwapControlUdpPort) !=
        OSIX_SUCCESS)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapSetFsCapwapControlUdpPort Returned Failure \r\n");
        return OSIX_FAILURE;
    }

    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : CapwapMainDeInit                                             */
/*                                                                           */
/* Description  : Deleting the resources when task init fails.               */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
CapwapMainDeInit (VOID)
{
    tVlanRegTbl         VlanRegTbl;
    tArpRegTbl          ArpRegTbl;

    MEMSET (&VlanRegTbl, 0, sizeof (tVlanRegTbl));
    MEMSET (&ArpRegTbl, 0, sizeof (tArpRegTbl));

    VlanRegTbl.u1ProtoId = VLAN_WSS_PROTO_ID;
    VlanRegDeRegProtocol (VLAN_INDICATION_DEREGISTER, &VlanRegTbl);

    ArpRegTbl.u1ProtoId = ARP_WSS_PROTO_ID;
    ArpRegDeRegProtocol (ARP_INDICATION_DEREGISTER, &ArpRegTbl);

/*    CapwapMainMemClear (); */
    if (gCapwapGlobals.capwapTaskSemId)
    {
        OsixSemDel (gCapwapGlobals.capwapTaskSemId);
    }

}

/*****************************************************************************/
/*                                                                           */
/* Function     : CapwapMainTaskLock                                           */
/*                                                                           */
/* Description  : Lock the Capwap Main Task                                    */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SNMP_SUCCESS or SNMP_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
CapwapMainTaskLock (VOID)
{
    CAPWAP_FN_ENTRY ();
/*** For debug *****/
    CAPWAP_TRC1 (CAPWAP_MGMT_TRC,
                 "Capwap task lock. Capwap Task Sem Id is %d \n",
                 gCapwapGlobals.capwapTaskSemId);
/*** For debug *****/

    if (OsixSemTake (gCapwapGlobals.capwapTaskSemId) == OSIX_FAILURE)
    {
        CAPWAP_TRC1 (CAPWAP_FAILURE_TRC,
                     "TakeSem failure for %s \n", CAPWAP_MUT_EXCL_SEM_NAME);
        return SNMP_FAILURE;
    }
    CAPWAP_FN_EXIT ();
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : CapwapMainTaskUnLock                                         */
/*                                                                           */
/* Description  : UnLock the CAPWAP Task                                       */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SNMP_SUCCESS or SNMP_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
CapwapMainTaskUnLock (VOID)
{
    CAPWAP_FN_ENTRY ();

/*** For debug *****/
    CAPWAP_TRC1 (CAPWAP_MGMT_TRC,
                 "Capwap task unlock. Capwap Task Sem Id is %d \n",
                 gCapwapGlobals.capwapTaskSemId);
/*** For debug *****/

    OsixSemGive (gCapwapGlobals.capwapTaskSemId);

    CAPWAP_FN_EXIT ();
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : CapwapUtilInitGlobals                                      */
/*                                                                           */
/* Description  : Initializes Capwap timer global variables                  */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SNMP_SUCCESS or SNMP_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
INT4
CapwapUtilInitGlobals (VOID)
{
    UINT1               u1DiscoveryMode = 0;
    if (CapwapSetCapwapBaseAcMaxRetransmit (CAPWAP_MAX_RETRANSMIT_COUNT) !=
        OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    if (CapwapSetCapwapBaseAcChangeStatePendingTimer
        (CHANGESTATE_PENDING_TIMEOUT) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    if (CapwapSetCapwapBaseAcDataCheckTimer (CAPWAP_DATACHECK_TIMEOUT) !=
        OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    if (CapwapSetCapwapBaseAcDTLSSessionDeleteTimer
        (CAPWAP_DTLS_SESSION_DELETE_TIMEOUT) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    if (CapwapSetCapwapBaseAcEchoInterval (CAPWAP_AP_ECHO_INTERVAL_TIMEOUT) !=
        OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    if (CapwapSetCapwapBaseAcRetransmitInterval (CAPWAP_MAX_RETRANSMIT_INTERVAL)
        != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    if (CapwapSetCapwapBaseAcSilentInterval (SILENT_INTERVAL_TIMEOUT) !=
        OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    if (CapwapSetCapwapBaseAcWaitDTLSTimer (CAPWAP_WAITDTLS_TIMEOUT) !=
        OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    if (CapwapSetCapwapBaseAcWaitJoinTimer (CAPWAP_WAITJOIN_TIMEOUT) !=
        OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    u1DiscoveryMode = CAPWAP_DISC_MODE_AUTO;
    if (CapwapSetFsWlcDiscoveryMode (&u1DiscoveryMode) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
#ifdef WSSCFG_WANTED
    if (WsscfgSetFsDot11CountryString (DEFAULT_COUNTRY) != OSIX_SUCCESS)
    {
    }
#endif
    return OSIX_SUCCESS;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  capwapmain.c                     */
/*-----------------------------------------------------------------------*/
