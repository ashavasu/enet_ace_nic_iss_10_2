/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved                      *
 * $Id: capwapwtpdtls.c,v 1.1.1.1 2016/09/20 10:42:39 siva Exp $       
 * Description: This file contains the implementation for CAPWAP to DTLS     *
 * interface.                                                                *
 *                                                                           *
 *****************************************************************************/
#ifndef __CAPWAPWTPDTLS_C__
#define __CAPWAPWTPDTLS_C__

#include "dtls.h"
#include "capwapinc.h"
#include "dtlsProtWTP.h"
#define DTLS_MAX_RETIRES 10

/* The Gloabl variable to store the DTLS fd */
#ifdef DTLS_WANTED
static UINT4        gu4DtlsFd;
#endif
#ifdef KERNEL_CAPWAP_WANTED
#include "fcusprot.h"
#endif

/****************************************************************************
* Function     : CapwapDTLSInit                                             *
* Description  : This function used to enable the DTLS interface            *
* Input        : None                                                       *
* Output       : None                                                       *
* Returns      : OSIX_SUCCESS, if initialization succeeds                   *
*                OSIX_FAILURE, otherwise                                    *
*****************************************************************************/
INT4
CapwapDTLSInit (VOID)
{
    return OSIX_FAILURE;
}

/****************************************************************************
* Function     : CapwapEnableDTLSInit                                       *
* Description  : This function used to enable the DTLS interface            *
* Input        : None                                                       *
* Output       : None                                                       *
* Returns      : OSIX_SUCCESS, if initialization succeeds                   *
*                OSIX_FAILURE, otherwise                                    *
*****************************************************************************/
INT4
CapwapEnableDTLSInit ()
{
    return OSIX_SUCCESS;

}

/****************************************************************************
* Function     : CapwapStartDTLS                                            *
* Description  : This function used to start the DTLS session               *
* Input        : None                                                       *
* Output       : None                                                       *
* Returns      : OSIX_SUCCESS, if initialization succeeds                   *
*                OSIX_FAILURE, otherwise                                    *
*****************************************************************************/
INT4
CapwapStartDTLS (UINT4 u4IpAddr)
{
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tDtlsParams         DtlsParams;
#ifdef DTLS_WANTED
    UINT4               u4Port;
    UINT4               u4Count;
    UINT4               u4DtlsFd;

#endif

    CAPWAP_FN_ENTRY ();
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (&DtlsParams, 0, sizeof (tDtlsParams));
    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    DtlsParams.DataExchangeNotify = CapwapDtlsDataNotify;

    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = 0;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpCtrlDTLSStatus = OSIX_TRUE;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
        == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to create the entry in Temporary DTLS table \r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    if (pWssIfCapwapDB->CapwapGetDB.u1WtpCtrlDTLSStatus == DTLS_CFG_DISABLE)
    {
        CAPWAP_TRC (CAPWAP_MGMT_TRC, "DTLS interface is disabled\r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_SUCCESS;
    }
    else
    {
#ifdef DTLS_WANTED
        u4DtlsFd = DtlsFnSelect (DTLS_OPEN, &DtlsParams);
        if (u4DtlsFd == 0)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Faild to open DTLS \r\n");
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
        u4Port = DTLS_PORT_NO;
        gu4DtlsFd = u4DtlsFd;
        DtlsParams.u4Fd = gu4DtlsFd;
        DtlsParams.u4Port = u4Port;
        DtlsParams.u4IpAddress = u4IpAddr;

        for (u4Count = 0; u4Count < DTLS_MAX_RETIRES; u4Count++)
        {
            if (DtlsFnSelect (DTLS_CONNECT, &DtlsParams) == OSIX_SUCCESS)
            {
                CAPWAP_TRC (CAPWAP_FAILURE_TRC, " Accept DTLS success \r\n");
                break;
            }
        }
        if (u4Count == DTLS_MAX_RETIRES)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Accept DTLS \r\n");
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
#else
        UNUSED_PARAM (u4IpAddr);
#endif
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function     : CapwapEnableDTLS                                           *
* Description  : This function used to disable the DTLS interface           *
* Input        : None                                                       *
* Output       : None                                                       *
* Returns      : OSIX_SUCCESS, if initialization succeeds                   *
*                OSIX_FAILURE, otherwise                                    *
*****************************************************************************/
INT4
CapwapEnableDTLS ()
{
    return OSIX_FAILURE;
}

/****************************************************************************
* Function     : CapwapDisableDTLS                                          *
* Description  : This function used to disable the DTLS interface           *
* Input        : None                                                       *
* Output       : None                                                       *
* Returns      : OSIX_SUCCESS, if initialization succeeds                   *
*                OSIX_FAILURE, otherwise                                    *
*****************************************************************************/
INT4
CapwapDisableDTLS ()
{
    return OSIX_FAILURE;
}

/*******************************************************************************
 * * Function     : CapwapShutDownDTLS                                         *
 * * Description  : This function used to disable the DTLS interface           *
 * * Input        : None                                                       *
 * * Output       : None                                                       *
 * * Returns      : OSIX_SUCCESS, if initialization succeeds                   *
 * *                OSIX_FAILURE, otherwise                                    *
 * *****************************************************************************/
INT4
CapwapShutDownDTLS (tRemoteSessionManager * pSessEntry)
{
#ifdef DTLS_WANTED
    tDtlsParams         DtlsParams;
#endif
    tWssIfCapDB        *pWssIfCapwapDB = NULL;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = 0;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpCtrlDTLSStatus = OSIX_TRUE;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
        == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "Failed to create the entry in Temporary DTLS table \r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    if (pWssIfCapwapDB->CapwapGetDB.u1WtpCtrlDTLSStatus == DTLS_CFG_DISABLE)
    {
        CAPWAP_TRC (CAPWAP_MGMT_TRC,
                    "DTLS interface is disabled, send the packet as plain text \r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_SUCCESS;
    }
    else
    {
#ifdef DTLS_WANTED
        DtlsParams.u4Id = pSessEntry->u4DtlsSessId;
        if (DtlsFnSelect (DTLS_SESSION_SHUTDOWN, &DtlsParams) == OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Failed to Shutdown DTLS \r\n");
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
#else
        UNUSED_PARAM (pSessEntry);
#endif
    }
#ifdef KERNEL_CAPWAP_WANTED
    CapwapRemoveWtpKernelDB ();
    WssStaRemoveAllKernelDB ();
#endif
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function     : CapwapDtlsSeesionEstdNotify                                *
* Description  : This function will be called by the DTLS module if it      *
*                detects the new peer                                       *
* Input        : None                                                       *
* Output       : None                                                       *
* Returns      : None                                                       *
*****************************************************************************/
VOID
CapwapDtlsSeesionEstdNotify (UINT4 u4IpAddr, UINT4 u4DestPort, UINT4 u4DtlsId)
{
    UNUSED_PARAM (u4IpAddr);
    UNUSED_PARAM (u4DestPort);
    UNUSED_PARAM (u4DtlsId);
    return;
}

/****************************************************************************
* Function     : CapwapDtlsPeerDisconnectNotify                             *
* Description  : This function will be called by the DTLS module to         *
*                disconnect the peer                                        *
* Input        : None                                                       *
* Output       : None                                                       *
* Returns      : None                                                       * 
*****************************************************************************/
VOID
CapwapDtlsPeerDisconnectNotify (UINT4 u4IpAddr, UINT4 u4DestPort,
                                UINT4 u4DtlsId)
{
    UNUSED_PARAM (u4IpAddr);
    UNUSED_PARAM (u4DestPort);
    UNUSED_PARAM (u4DtlsId);
    return;
}

/****************************************************************************
* Function     : CapwapDtlsDataNotify                                       *
* Description  : This function will be called by the DTLS module after      *
*                encrypt or decrypt the given packet.                       *
* Input        : None                                                       *
* Output       : None                                                       *
* Returns      : None
*****************************************************************************/
VOID
CapwapDtlsDataNotify (tQueData * pDtlsData)
{
    UINT1               au1DtlsHdr[DTLS_HDR_SIZE] = DTLS_HDR_FORMAT;
    tCRU_BUF_CHAIN_HEADER *pTxBuf = NULL;

    CAPWAP_FN_ENTRY ();
    if (pDtlsData->eQueDataType == DATA_ENCRYPT)
    {
	pTxBuf = pDtlsData->pBuffer;
	if (CRU_BUF_Prepend_BufChain (pTxBuf, (UINT1 *) au1DtlsHdr,
		    CAPWAP_DTLS_HDR_LEN) == CRU_FAILURE)
	{
	    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
		    "CRU buffer Prepend operation for CAPWAP DTLS Hdr Failed \r\n");
	    return;
	}
	if (pDtlsData->u4Channel == DTLS_CTRL_CHANNEL)
	{
	    if (CapwapCtrlUdpTxPacket (pTxBuf,
			pDtlsData->u4IpAddr,
			pDtlsData->u4PortNo,
			(pDtlsData->u4BufSize +
			 CAPWAP_DTLS_HDR_LEN)) == OSIX_FAILURE)
	    {
		CAPWAP_TRC (CAPWAP_FAILURE_TRC,
			"Failed to Transmit the encrypted packet \r\n");
	    }
	}
	else if(pDtlsData->u4Channel == DTLS_DATA_CHANNEL)
	{
	    if (CapwapDataUdpTxPacket (pTxBuf,
			pDtlsData->u4IpAddr,
			pDtlsData->u4PortNo,
			(pDtlsData->u4BufSize +
			 CAPWAP_DTLS_HDR_LEN),
			CAPWAP_CTRL_PKT_DSCP_CODE) == OSIX_FAILURE)
	    {
		CAPWAP_TRC (CAPWAP_FAILURE_TRC,
			"Failed to Transmit the encrypted Data packet \r\n");
	    }
	}
	else
	{
	    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
		    "Invalid Channel type received \r\n");
	}
	CAPWAP_RELEASE_CRU_BUF (pTxBuf);
    }
    else if (pDtlsData->eQueDataType == DATA_DECRYPT)
    {
	pTxBuf = pDtlsData->pBuffer;
	CRU_BUF_Set_U4Reserved2 (pTxBuf, pDtlsData->u4IpAddr);
	CRU_BUF_Set_U4Reserved1 (pTxBuf, pDtlsData->u4PortNo);

	if (pDtlsData->u4Channel == DTLS_CTRL_CHANNEL)
	{
	    if (CapwapProcessCtrlPacket (pTxBuf) == OSIX_FAILURE)
	    {
		CAPWAP_TRC (CAPWAP_FAILURE_TRC,
			"Failed to process the Received Control packet \r\n");
		return;
	    }
	}
	else if(pDtlsData->u4Channel == DTLS_DATA_CHANNEL)
	{
	    if (CapwapProcessDataPacket (pTxBuf) == OSIX_FAILURE)
	    {
		CAPWAP_TRC (CAPWAP_FAILURE_TRC,
			"Failed to process the Received Data packet \r\n");
		return;
	    }
	}
	else
	{
	    CAPWAP_TRC (CAPWAP_FAILURE_TRC,
		    "Invalid Channel type received\r\n");
	    return;
	}
    }
    CAPWAP_FN_EXIT ();
    return;
}

/****************************************************************************
* Function     : CapwapDtlsDecryptNotify                                    *
* Description  : This function will be called by the DTLS module to         *
*                decrypt the given packet.                                  *
* Input        : None                                                       *
* Output       : None                                                       *
* Returns      : None                                                       *
*****************************************************************************/
INT4
CapwapDtlsDecryptNotify (tQueData * pDtlsData)
{
#ifdef DTLS_WANTED
    if (DtlsDataEncryptDecryptNotify (pDtlsData) == OSIX_FAILURE)
    {
        CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                    "CapwapDtlsDecryptNotify Retrurnted Failure \r\n");
        return OSIX_FAILURE;
    }
#else
    UNUSED_PARAM (pDtlsData);
#endif
    return OSIX_SUCCESS;
}
#endif
