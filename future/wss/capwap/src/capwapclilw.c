/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: capwapclilw.c,v 1.2 2017/05/23 14:16:49 siva Exp $       
*
* Description: This file contains some of low level functions used by protocol in Capwap module
*********************************************************************/

#include "capwapcliinc.h"

/****************************************************************************
 Function    :  nmhValidateIndexInstanceCapwapBaseAcNameListTable
 Input       :  The Indices
                CapwapBaseAcNameListId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceCapwapBaseAcNameListTable (UINT4
                                                   u4CapwapBaseAcNameListId)
{
    UNUSED_PARAM (u4CapwapBaseAcNameListId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceCapwapBaseMacAclTable
 Input       :  The Indices
                CapwapBaseMacAclId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceCapwapBaseMacAclTable (UINT4 u4CapwapBaseMacAclId)
{
    UNUSED_PARAM (u4CapwapBaseMacAclId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceCapwapBaseWtpProfileTable
 Input       :  The Indices
                CapwapBaseWtpProfileId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceCapwapBaseWtpProfileTable (UINT4
                                                   u4CapwapBaseWtpProfileId)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceCapwapBaseWtpStateTable
 Input       :  The Indices
                CapwapBaseWtpStateWtpId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceCapwapBaseWtpStateTable (tSNMP_OCTET_STRING_TYPE *
                                                 pCapwapBaseWtpStateWtpId)
{
    UNUSED_PARAM (pCapwapBaseWtpStateWtpId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceCapwapBaseWtpTable
 Input       :  The Indices
                CapwapBaseWtpCurrId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceCapwapBaseWtpTable (tSNMP_OCTET_STRING_TYPE *
                                            pCapwapBaseWtpCurrId)
{
    if (CapwapGetCapwapBaseWtpTable (pCapwapBaseWtpCurrId->pu1_OctetList)
        == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceCapwapBaseWirelessBindingTable
 Input       :  The Indices
                CapwapBaseWtpProfileId
                CapwapBaseWirelessBindingRadioId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceCapwapBaseWirelessBindingTable (UINT4
                                                        u4CapwapBaseWtpProfileId,
                                                        UINT4
                                                        u4CapwapBaseWirelessBindingRadioId)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (u4CapwapBaseWirelessBindingRadioId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceCapwapBaseStationTable
 Input       :  The Indices
                CapwapBaseStationId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceCapwapBaseStationTable (tSNMP_OCTET_STRING_TYPE *
                                                pCapwapBaseStationId)
{
    UNUSED_PARAM (pCapwapBaseStationId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceCapwapBaseWtpEventsStatsTable
 Input       :  The Indices
                CapwapBaseWtpCurrId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceCapwapBaseWtpEventsStatsTable (tSNMP_OCTET_STRING_TYPE *
                                                       pCapwapBaseWtpCurrId)
{
    UNUSED_PARAM (pCapwapBaseWtpCurrId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceCapwapBaseRadioEventsStatsTable
 Input       :  The Indices
                CapwapBaseWtpCurrId
                CapwapBaseRadioEventsWtpRadioId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceCapwapBaseRadioEventsStatsTable (tSNMP_OCTET_STRING_TYPE
                                                         * pCapwapBaseWtpCurrId,
                                                         UINT4
                                                         u4CapwapBaseRadioEventsWtpRadioId)
{
    UNUSED_PARAM (pCapwapBaseWtpCurrId);
    UNUSED_PARAM (u4CapwapBaseRadioEventsWtpRadioId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsWtpModelTable
 Input       :  The Indices
                FsCapwapWtpModelNumber
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsWtpModelTable (tSNMP_OCTET_STRING_TYPE *
                                         pFsCapwapWtpModelNumber)
{
    UNUSED_PARAM (pFsCapwapWtpModelNumber);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsWtpRadioTable
 Input       :  The Indices
                FsCapwapWtpModelNumber
                FsNumOfRadio
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsWtpRadioTable (tSNMP_OCTET_STRING_TYPE *
                                         pFsCapwapWtpModelNumber,
                                         UINT4 u4FsNumOfRadio)
{
    UNUSED_PARAM (pFsCapwapWtpModelNumber);
    UNUSED_PARAM (u4FsNumOfRadio);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsWtpExtModelTable
 Input       :  The Indices
                FsCapwapWtpModelNumber
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsWtpExtModelTable (tSNMP_OCTET_STRING_TYPE *
                                            pFsCapwapWtpModelNumber)
{
    UNUSED_PARAM (pFsCapwapWtpModelNumber);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsWtpModelWlanBindingTable
 Input       :  The Indices
                FsCapwapWtpModelNumber
                FsRadioNumber
                CapwapDot11WlanProfileId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsWtpModelWlanBindingTable (tSNMP_OCTET_STRING_TYPE *
                                                    pFsCapwapWtpModelNumber,
                                                    UINT4 u4FsRadioNumber,
                                                    UINT4
                                                    u4CapwapDot11WlanProfileId)
{
    UNUSED_PARAM (pFsCapwapWtpModelNumber);
    UNUSED_PARAM (u4FsRadioNumber);
    UNUSED_PARAM (u4CapwapDot11WlanProfileId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsCapwapWhiteListTable
 Input       :  The Indices
                FsCapwapWhiteListId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsCapwapWhiteListTable (UINT4 u4FsCapwapWhiteListId)
{
    UNUSED_PARAM (u4FsCapwapWhiteListId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsCapwapBlackList
 Input       :  The Indices
                FsCapwapBlackListId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsCapwapBlackList (UINT4 u4FsCapwapBlackListId)
{
    UNUSED_PARAM (u4FsCapwapBlackListId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsCapwapWtpConfigTable
 Input       :  The Indices
                CapwapBaseWtpProfileId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsCapwapWtpConfigTable (UINT4 u4CapwapBaseWtpProfileId)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsCapwapExtWtpConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsCapwapExtWtpConfigTable
 Input       :  The Indices
                CapwapBaseWtpProfileId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsCapwapExtWtpConfigTable (UINT4
                                                   u4CapwapBaseWtpProfileId)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsCapwapLinkEncryptionTable
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsCapwapEncryptChannel
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsCapwapLinkEncryptionTable (UINT4
                                                     u4CapwapBaseWtpProfileId,
                                                     INT4
                                                     i4FsCapwapEncryptChannel)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsCapwapEncryptChannel);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsCawapDefaultWtpProfileTable
 Input       :  The Indices
                FsCapwapDefaultWtpProfileModelNumber
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsCawapDefaultWtpProfileTable (tSNMP_OCTET_STRING_TYPE *
                                                       pFsCapwapDefaultWtpProfileModelNumber)
{
    UNUSED_PARAM (pFsCapwapDefaultWtpProfileModelNumber);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsCapwapDnsProfileTable
 Input       :  The Indices
                CapwapBaseWtpProfileId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsCapwapDnsProfileTable (UINT4 u4CapwapBaseWtpProfileId)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsWtpNativeVlanIdTable
 Input       :  The Indices
                CapwapBaseWtpProfileId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsWtpNativeVlanIdTable (UINT4 u4CapwapBaseWtpProfileId)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsWtpLocalRoutingTable
 Input       :  The Indices
                CapwapBaseWtpProfileId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsWtpLocalRoutingTable (UINT4 u4CapwapBaseWtpProfileId)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsCapwapFsAcTimestampTriggerTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsCapwapFsAcTimestampTriggerTable
 Input       :  The Indices
                CapwapBaseWtpProfileId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsCapwapFsAcTimestampTriggerTable (UINT4
                                                           u4CapwapBaseWtpProfileId)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsCawapDiscStatsTable
 Input       :  The Indices
                CapwapBaseWtpProfileId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsCawapDiscStatsTable (UINT4 u4CapwapBaseWtpProfileId)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsCawapJoinStatsTable
 Input       :  The Indices
                CapwapBaseWtpProfileId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsCawapJoinStatsTable (UINT4 u4CapwapBaseWtpProfileId)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsCawapConfigStatsTable
 Input       :  The Indices
                CapwapBaseWtpProfileId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsCawapConfigStatsTable (UINT4 u4CapwapBaseWtpProfileId)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsCawapRunStatsTable
 Input       :  The Indices
                CapwapBaseWtpProfileId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsCawapRunStatsTable (UINT4 u4CapwapBaseWtpProfileId)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsCapwapWirelessBindingTable
 Input       :  The Indices
                CapwapBaseWtpProfileId
                CapwapBaseWirelessBindingRadioId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsCapwapWirelessBindingTable (UINT4
                                                      u4CapwapBaseWtpProfileId,
                                                      UINT4
                                                      u4CapwapBaseWirelessBindingRadioId)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (u4CapwapBaseWirelessBindingRadioId);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsCapwapStationWhiteListTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsCapwapStationWhiteListTable
 Input       :  The Indices
                FsCapwapStationWhiteListStationId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsCapwapStationWhiteListTable (tSNMP_OCTET_STRING_TYPE *
                                                       pFsCapwapStationWhiteListStationId)
{
    UNUSED_PARAM (pFsCapwapStationWhiteListStationId);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsCapwapStationBlackListTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsCapwapStationBlackListTable
 Input       :  The Indices
                FsCapwapStationBlackListStationId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsCapwapStationBlackListTable (tSNMP_OCTET_STRING_TYPE *
                                                       pFsCapwapStationBlackListStationId)
{
    UNUSED_PARAM (pFsCapwapStationBlackListStationId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsCapwapWtpRebootStatisticsTable
 Input       :  The Indices
                CapwapBaseWtpProfileId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsCapwapWtpRebootStatisticsTable (UINT4
                                                          u4CapwapBaseWtpProfileId)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsCapwapWtpRadioStatisticsTable
 Input       :  The Indices
                CapwapBaseWtpProfileId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsCapwapWtpRadioStatisticsTable (INT4 i4IfIndex)
{
    UNUSED_PARAM (i4IfIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsCapwapDot11StatisticsTable
 Input       :  The Indices
                IfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsCapwapDot11StatisticsTable (INT4 i4IfIndex)
{
    UNUSED_PARAM (i4IfIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsCapwATPStatsTable
 Input       :  The Indices
                CapwapBaseWtpProfileId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsCapwATPStatsTable (UINT4 u4CapwapBaseWtpProfileId)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    return SNMP_SUCCESS;

}
