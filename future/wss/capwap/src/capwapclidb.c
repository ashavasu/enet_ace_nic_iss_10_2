/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: capwapclidb.c,v 1.5 2017/12/08 10:16:24 siva Exp $       
*
* Description: This file contains the routines for the protocol Database Access for the module Capwap 
*********************************************************************/

#include "capwapinc.h"
#include "capwapcli.h"
#include "wsscfgcli.h"
#include "radioifextn.h"
#include "radioifconst.h"

tCountryList        CountryListA[] = {
    {"US", '\0'}
    ,
    {"GB", '\0'}
    ,
    {"CN", '\0'}
    ,
    {"IN", '\0'}
    ,
    {"JP", '\0'}
    ,
    {"SG", '\0'}
};

/****************************************************************************
 Function    :  CapwapTestAllCapwapBaseAcNameListTable
 Input       :  pu4ErrorCode
                pCapwapSetCapwapBaseAcNameListEntry
                pCapwapIsSetCapwapBaseAcNameListEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
CapwapTestAllCapwapBaseAcNameListTable (UINT4 *pu4ErrorCode,
                                        tCapwapCapwapBaseAcNameListEntry *
                                        pCapwapSetCapwapBaseAcNameListEntry,
                                        tCapwapIsSetCapwapBaseAcNameListEntry *
                                        pCapwapIsSetCapwapBaseAcNameListEntry,
                                        INT4 i4RowStatusLogic,
                                        INT4 i4RowCreateOption)
{
    INT4                i4OutCome = 0;
    INT4                i4RowStatus = 0;
    UINT4               currentProfileId = 0, nextProfileId = 0;
    UINT4               u4Priority = 0;
    tSNMP_OCTET_STRING_TYPE CapwapBaseAcName;
    UINT1               au1CapwapBaseAcName[CAPWAP_ACL_MAX_SIZE];
    MEMSET (&CapwapBaseAcName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    CapwapBaseAcName.pu1_OctetList = au1CapwapBaseAcName;

    if (CapwapCheckModuleStatus () == OSIX_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_RESOURCE_UNAVAILABLE;
        return OSIX_FAILURE;
    }

    if ((pCapwapIsSetCapwapBaseAcNameListEntry->
         bCapwapBaseAcNameListRowStatus == OSIX_FALSE)
        &&
        ((pCapwapIsSetCapwapBaseAcNameListEntry->bCapwapBaseAcNameListName ==
          OSIX_TRUE)
         || (pCapwapIsSetCapwapBaseAcNameListEntry->
             bCapwapBaseAcNameListPriority == OSIX_TRUE)))
    {
        if (nmhGetCapwapBaseAcNameListRowStatus
            (pCapwapSetCapwapBaseAcNameListEntry->MibObject.
             u4CapwapBaseAcNameListId, &i4RowStatus) != SNMP_SUCCESS)
        {
            if ((pCapwapSetCapwapBaseAcNameListEntry->MibObject.
                 i4CapwapBaseAcNameListRowStatus != CREATE_AND_WAIT)
                && (pCapwapSetCapwapBaseAcNameListEntry->MibObject.
                    i4CapwapBaseAcNameListRowStatus != CREATE_AND_GO))
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return OSIX_FAILURE;
            }
        }
        else
        {
            if ((i4RowStatus == CREATE_AND_GO)
                || (i4RowStatus == CREATE_AND_WAIT))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return OSIX_FAILURE;
            }
        }
    }

    if (pCapwapIsSetCapwapBaseAcNameListEntry->bCapwapBaseAcNameListRowStatus !=
        OSIX_FALSE)
    {
        if (pCapwapSetCapwapBaseAcNameListEntry->MibObject.
            i4CapwapBaseAcNameListRowStatus == NOT_READY)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }

        if (pCapwapSetCapwapBaseAcNameListEntry->MibObject.
            i4CapwapBaseAcNameListRowStatus == ACTIVE)
        {
            if ((nmhGetCapwapBaseAcNameListName
                 (pCapwapSetCapwapBaseAcNameListEntry->MibObject.
                  u4CapwapBaseAcNameListId, &CapwapBaseAcName) == SNMP_SUCCESS)
                || (pCapwapIsSetCapwapBaseAcNameListEntry->
                    bCapwapBaseAcNameListName == OSIX_FALSE))
            {
                if (CapwapBaseAcName.pu1_OctetList == NULL)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return OSIX_FAILURE;
                }
            }
            if ((nmhGetCapwapBaseAcNameListPriority
                 (pCapwapSetCapwapBaseAcNameListEntry->MibObject.
                  u4CapwapBaseAcNameListId, &u4Priority) == SNMP_SUCCESS)
                || (pCapwapIsSetCapwapBaseAcNameListEntry->
                    bCapwapBaseAcNameListPriority == OSIX_FALSE))
            {
                if ((u4Priority < CLI_CAPWAP_AC_NAME_PRIO_MIN) ||
                    (u4Priority > CLI_CAPWAP_AC_NAME_PRIO_MAX))
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return OSIX_FAILURE;
                }
            }
            if (pCapwapIsSetCapwapBaseAcNameListEntry->
                bCapwapBaseAcNameListName != OSIX_FALSE)
            {
                if (pCapwapSetCapwapBaseAcNameListEntry->MibObject.
                    au1CapwapBaseAcNameListName[0] == '\0')
                {
                    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                    return OSIX_FAILURE;
                }
                i4OutCome =
                    nmhGetFirstIndexCapwapBaseAcNameListTable (&nextProfileId);
                if (i4OutCome == SNMP_FAILURE)
                {
                    return OSIX_SUCCESS;
                }

                do
                {
                    currentProfileId = nextProfileId;
                    if (nmhGetCapwapBaseAcNameListName (currentProfileId,
                                                        &CapwapBaseAcName) ==
                        SNMP_SUCCESS)
                    {

                        if ((STRCMP
                             (pCapwapSetCapwapBaseAcNameListEntry->MibObject.
                              au1CapwapBaseAcNameListName,
                              CapwapBaseAcName.pu1_OctetList) == 0)
                            && (currentProfileId !=
                                pCapwapSetCapwapBaseAcNameListEntry->MibObject.
                                u4CapwapBaseAcNameListId))
                        {
                            CAPWAP_TRC (CAPWAP_UTIL_TRC,
                                        "CapwapTestAllCapwapBaseAcNameListTable: "
                                        "Inconsistent Entry\r\n");
                            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                            return OSIX_FAILURE;
                        }
                    }
                }
                while (nmhGetNextIndexCapwapBaseAcNameListTable
                       (currentProfileId, &nextProfileId) == SNMP_SUCCESS);
            }

            if (pCapwapIsSetCapwapBaseAcNameListEntry->
                bCapwapBaseAcNameListPriority != OSIX_FALSE)
            {
                if ((pCapwapSetCapwapBaseAcNameListEntry->MibObject.
                     u4CapwapBaseAcNameListPriority <
                     CLI_CAPWAP_AC_NAME_PRIO_MIN)
                    || (pCapwapSetCapwapBaseAcNameListEntry->MibObject.
                        u4CapwapBaseAcNameListPriority >
                        CLI_CAPWAP_AC_NAME_PRIO_MAX))
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return OSIX_FAILURE;
                }
            }
        }
    }
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pCapwapSetCapwapBaseAcNameListEntry);
    UNUSED_PARAM (i4RowStatusLogic);
    UNUSED_PARAM (i4RowCreateOption);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapTestAllCapwapBaseMacAclTable
 Input       :  pu4ErrorCode
                pCapwapSetCapwapBaseMacAclEntry
                pCapwapIsSetCapwapBaseMacAclEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
CapwapTestAllCapwapBaseMacAclTable (UINT4 *pu4ErrorCode,
                                    tCapwapCapwapBaseMacAclEntry *
                                    pCapwapSetCapwapBaseMacAclEntry,
                                    tCapwapIsSetCapwapBaseMacAclEntry *
                                    pCapwapIsSetCapwapBaseMacAclEntry,
                                    INT4 i4RowStatusLogic,
                                    INT4 i4RowCreateOption)
{
    if (pCapwapIsSetCapwapBaseMacAclEntry->bCapwapBaseMacAclStationId !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pCapwapIsSetCapwapBaseMacAclEntry->bCapwapBaseMacAclRowStatus !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pCapwapSetCapwapBaseMacAclEntry);
    UNUSED_PARAM (i4RowStatusLogic);
    UNUSED_PARAM (i4RowCreateOption);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapTestAllCapwapBaseWtpProfileTable
 Input       :  pu4ErrorCode
                pCapwapSetCapwapBaseWtpProfileEntry
                pCapwapIsSetCapwapBaseWtpProfileEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
CapwapTestAllCapwapBaseWtpProfileTable (UINT4 *pu4ErrorCode,
                                        tCapwapCapwapBaseWtpProfileEntry *
                                        pCapwapSetCapwapBaseWtpProfileEntry,
                                        tCapwapIsSetCapwapBaseWtpProfileEntry *
                                        pCapwapIsSetCapwapBaseWtpProfileEntry,
                                        INT4 i4RowStatusLogic,
                                        INT4 i4RowCreateOption)
{
    CAPWAP_FN_ENTRY ();
    tSNMP_OCTET_STRING_TYPE ProfileName, MacAddr, ModelNumber;
    tSNMP_OCTET_STRING_TYPE WtpName, Location;
    tMacAddr            WtpMacAddr;
    UINT1               au1ProfileName[256];
    UINT1               au1MacAddr[256];
    UINT1               au1ModelNumber[256];
    UINT1               au1WtpName[512];
    UINT1               au1Location[1024];
    /*Coverity Change Begins */
    UINT4               u4WtpProfileCount = 0;
    UINT1               au1IpAddr[4];
    UINT1               au1NetMask[4];
    UINT1               au1GateWay[4];
    UINT4               u4Count = 0;
    UINT4               u4IpAddr = 0;
    UINT4               u4NetMask = 0;
    UINT4               u4GateWay = 0;
    UINT4               u4CurrentProfileId = 0;
    UINT4               u4WtpProfileId = 0;
    INT4                i4RowStatus = 0;
    UINT4               u4BroadCastAddr;
    UINT4               u4NetworkIpAddr;
    /*Coverity Change Ends */

    /* Initialize the variables */
    MEMSET (&WtpMacAddr, 0, sizeof (tMacAddr));
    MEMSET (&ProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1ProfileName, 0, sizeof (au1ProfileName));
    MEMSET (&MacAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1MacAddr, 0, sizeof (au1MacAddr));
    MEMSET (&ModelNumber, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1ModelNumber, 0, sizeof (au1ModelNumber));
    MEMSET (&WtpName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1WtpName, 0, sizeof (au1WtpName));
    MEMSET (&Location, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1Location, 0, sizeof (au1Location));
    MEMSET (au1IpAddr, 0, sizeof (au1IpAddr));
    MEMSET (au1NetMask, 0, sizeof (au1NetMask));
    MEMSET (au1GateWay, 0, sizeof (au1GateWay));

    ProfileName.pu1_OctetList = au1ProfileName;
    ProfileName.i4_Length = 0;
    MacAddr.pu1_OctetList = au1MacAddr;
    MacAddr.i4_Length = 0;
    ModelNumber.pu1_OctetList = au1ModelNumber;
    ModelNumber.i4_Length = 0;
    WtpName.pu1_OctetList = au1WtpName;
    WtpName.i4_Length = 0;
    Location.pu1_OctetList = au1Location;
    Location.i4_Length = 0;

    if (pCapwapIsSetCapwapBaseWtpProfileEntry->bCapwapBaseWtpProfileId !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
        if ((pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
             u4CapwapBaseWtpProfileId) > 4096)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CAPW_CLI_INVALID_PROF_ID);
            return OSIX_FAILURE;
        }
    }
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->bCapwapBaseWtpProfileName !=
        OSIX_FALSE)
    {
        if (CapwapCheckForAlphaNumeric (pCapwapSetCapwapBaseWtpProfileEntry->
                                        MibObject.
                                        au1CapwapBaseWtpProfileName) ==
            OSIX_FAILURE)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CAPW_CLI_NON_PRINTABLE_CHAR);
            return OSIX_FAILURE;
        }
        /*Fill your check condition */
        if ((pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
             i4CapwapBaseWtpProfileNameLen > 256) ||
            (pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
             i4CapwapBaseWtpProfileNameLen < 1))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CAPW_CLI_INVALID_PROF_NAME);
            return OSIX_FAILURE;
        }
#ifdef WLC_WANTED
        if (CapwapGetWtpProfileIdFromProfileName
            (pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
             au1CapwapBaseWtpProfileName, &u4WtpProfileId) == OSIX_SUCCESS)
        {
            if (u4WtpProfileId != pCapwapSetCapwapBaseWtpProfileEntry->
                MibObject.u4CapwapBaseWtpProfileId)
            {
                CLI_SET_ERR (CAPW_CLI_ENTRY_EXISTS);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                return OSIX_FAILURE;
            }
        }
#endif
    }
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpMacAddress != OSIX_FALSE)
    {
        if ((pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
             i4CapwapBaseWtpProfileWtpMacAddressLen > 256)
            || (pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                i4CapwapBaseWtpProfileWtpMacAddressLen < 1))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
#ifdef WLC_WANTED
        CliDotStrToMac (pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                        au1CapwapBaseWtpProfileWtpMacAddress, WtpMacAddr);
        if (CapwapGetWtpProfileIdFromProfileMac (WtpMacAddr, &u4WtpProfileId)
            == OSIX_SUCCESS)
        {
            if (u4WtpProfileId != pCapwapSetCapwapBaseWtpProfileEntry->
                MibObject.u4CapwapBaseWtpProfileId)
            {
                CLI_SET_ERR (CAPW_CLI_ENTRY_EXISTS);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                return OSIX_FAILURE;
            }
        }
#endif
    }
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpModelNumber != OSIX_FALSE)
    {
        if ((pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
             i4CapwapBaseWtpProfileWtpModelNumberLen > 256)
            || (pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                i4CapwapBaseWtpProfileWtpModelNumberLen < 1))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
#ifdef WLC_WANTED
        ModelNumber.pu1_OctetList =
            pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
            au1CapwapBaseWtpProfileWtpModelNumber;

        ModelNumber.i4_Length = (INT4) (STRLEN (ModelNumber.pu1_OctetList));
        if (nmhGetFsWtpModelRowStatus (&ModelNumber, &i4RowStatus)
            != SNMP_SUCCESS)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return OSIX_FAILURE;
        }
        if (i4RowStatus != ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return OSIX_FAILURE;
        }
#endif
        /*Fill your check condition */
    }
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->bCapwapBaseWtpProfileWtpName !=
        OSIX_FALSE)
    {
        if (CapwapCheckForAlphaNumeric (pCapwapSetCapwapBaseWtpProfileEntry->
                                        MibObject.
                                        au1CapwapBaseWtpProfileWtpName) ==
            OSIX_FAILURE)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CAPW_CLI_NON_PRINTABLE_CHAR);
            return OSIX_FAILURE;
        }
        if ((pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
             i4CapwapBaseWtpProfileWtpNameLen > 512)
            || (pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                i4CapwapBaseWtpProfileWtpNameLen < 1))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
        /*Fill your check condition */
    }
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpLocation != OSIX_FALSE)
    {
        if ((pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
             i4CapwapBaseWtpProfileWtpLocationLen > 1024)
            || (pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                i4CapwapBaseWtpProfileWtpLocationLen < 1))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
        /*Fill your check condition */
    }
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpStaticIpEnable != OSIX_FALSE)
    {
        if ((pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
             i4CapwapBaseWtpProfileWtpStaticIpEnable != CLI_CAPWAP_IP_ENABLE)
            && (pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                i4CapwapBaseWtpProfileWtpStaticIpEnable !=
                CLI_CAPWAP_IP_DISABLE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bFsCapwapFsAcTimestampTrigger != OSIX_FALSE)
    {
        if ((pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
             i4FsCapwapFsAcTimestampTrigger != CLI_FSCAPWAP_ENABLE)
            && (pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                i4FsCapwapFsAcTimestampTrigger != CLI_FSCAPWAP_DISABLE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }

    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpStaticIpType != OSIX_FALSE)
    {
        if (pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileWtpStaticIpType != CLI_CAPWAP_AP_TYPE_IPV4)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpStaticIpAddress != OSIX_FALSE)
    {
        if (MEMCMP
            (&pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
             au1CapwapBaseWtpProfileWtpStaticIpAddress, au1IpAddr,
             sizeof (au1IpAddr)) == 0)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
        MEMCPY (&u4IpAddr,
                pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                au1CapwapBaseWtpProfileWtpStaticIpAddress,
                pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                i4CapwapBaseWtpProfileWtpStaticIpAddressLen);

        if (u4IpAddr != 0)
        {
            if (CAPWAP_IS_LOOPBACK (u4IpAddr))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return OSIX_FAILURE;
            }
            if (!((CAPWAP_IS_ADDR_CLASS_A (u4IpAddr) ?
                   (CAPWAP_IP_IS_ZERO_NETWORK (u4IpAddr) ? 0 : 1) : 0) ||
                  (CAPWAP_IS_ADDR_CLASS_B (u4IpAddr)) ||
                  (CAPWAP_IS_ADDR_CLASS_C (u4IpAddr))))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return OSIX_FAILURE;
            }
            if (CAPWAP_IS_BROADCAST (u4IpAddr))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return OSIX_FAILURE;
            }
            if (CAPWAP_IS_ZERO_ADDR (u4IpAddr))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return OSIX_FAILURE;
            }
        }

    }
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpNetmask != OSIX_FALSE)
    {
        if (MEMCMP
            (&pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
             au1CapwapBaseWtpProfileWtpNetmask, au1NetMask,
             sizeof (au1NetMask)) == 0)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
        MEMCPY (&u4NetMask,
                pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                au1CapwapBaseWtpProfileWtpNetmask,
                pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                i4CapwapBaseWtpProfileWtpNetmaskLen);
        /*when netmask is 31 bit mask we need NOT to check ip address with
         * broadcast address */
        if ((u4NetMask != 0xffffffff)
            && (u4NetMask != CAPWAP_IP_ADDR_31BIT_MASK))
        {
            /* ipaddress should not be Broadcast Address */
            u4BroadCastAddr = u4IpAddr | (~(u4NetMask));
            if (u4IpAddr == u4BroadCastAddr)
            {
                return OSIX_FAILURE;
            }

            /* ipaddress should not be
             * Network Address */
            u4NetworkIpAddr = u4IpAddr & (~(u4NetMask));
            if (u4NetworkIpAddr == 0)
            {
                return CLI_FAILURE;
            }
        }

    }
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpGateway != OSIX_FALSE)
    {
        if (MEMCMP
            (&pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
             au1CapwapBaseWtpProfileWtpGateway, au1GateWay,
             sizeof (au1GateWay)) == 0)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
        MEMCPY (&u4GateWay,
                pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                au1CapwapBaseWtpProfileWtpGateway,
                pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                i4CapwapBaseWtpProfileWtpGatewayLen);
        if (u4GateWay != 0)
        {
            if (CAPWAP_IS_LOOPBACK (u4GateWay))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return OSIX_FAILURE;
            }

            if (CAPWAP_IS_BROADCAST (u4GateWay))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return OSIX_FAILURE;
            }
            if (CAPWAP_IS_ZERO_ADDR (u4GateWay))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return OSIX_FAILURE;
            }

        }
    }
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpFallbackEnable != OSIX_FALSE)
    {
        if ((pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
             i4CapwapBaseWtpProfileWtpFallbackEnable !=
             CLI_CAPWAP_FALLBACK_ENABLE)
            && (pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                i4CapwapBaseWtpProfileWtpFallbackEnable !=
                CLI_CAPWAP_FALLBACK_DISABLE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }

    }
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpEchoInterval != OSIX_FALSE)
    {
        /*Fill your check condition */
        if ((pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
             u4CapwapBaseWtpProfileWtpEchoInterval < ECHO_INTERVAL_LOWER_LIMIT)
            || (pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                u4CapwapBaseWtpProfileWtpEchoInterval >
                ECHO_INTERVAL_UPPER_LIMIT))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpIdleTimeout != OSIX_FALSE)
    {
        /*Fill your check condition */
        /* Coverity Fix Start 11-7-13 *//* u4CapwapBaseWtpProfileWtpIdleTimeout declared as UINT, so it never be <0 */
        if ((pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
             u4CapwapBaseWtpProfileWtpIdleTimeout < IDLE_TIMEOUT_LOWER_LIMIT)
            || (pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                u4CapwapBaseWtpProfileWtpIdleTimeout >
                IDLE_TIMEOUT_UPPER_LIMIT))
            /* Coverity Fix End 11-7-13 */
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpMaxDiscoveryInterval != OSIX_FALSE)
    {
        if (((pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
              u4CapwapBaseWtpProfileWtpMaxDiscoveryInterval) >
             MAX_DISCOVERY_INTERVAL_UPPER_LIMIT)
            ||
            ((pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
              u4CapwapBaseWtpProfileWtpMaxDiscoveryInterval) <
             MAX_DISCOVERY_INTERVAL_LOWER_LIMIT))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
        /*Fill your check condition */
    }
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpReportInterval != OSIX_FALSE)
    {
        /*Fill your check condition */
        if ((pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
             u4CapwapBaseWtpProfileWtpReportInterval <
             REPORT_INTERVAL_LOWER_LIMIT)
            || (pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                u4CapwapBaseWtpProfileWtpReportInterval >
                REPORT_INTERVAL_UPPER_LIMIT))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }

    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpStatisticsTimer != OSIX_FALSE)
    {
        /*Fill your check condition */
        if ((pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
             u4CapwapBaseWtpProfileWtpStatisticsTimer <
             STATISTICS_TIMER_LOWER_LIMIT)
            || (pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                u4CapwapBaseWtpProfileWtpStatisticsTimer >
                STATISTICS_TIMER_UPPER_LIMIT))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pCapwapIsSetCapwapBaseWtpProfileEntry->
        bCapwapBaseWtpProfileWtpEcnSupport != OSIX_FALSE)
    {
        if ((pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
             i4CapwapBaseWtpProfileWtpEcnSupport != CLI_CAPWAP_ECN_LIMITED)
            && (pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                i4CapwapBaseWtpProfileWtpEcnSupport != CLI_CAPWAP_ECN_FULL))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }

    if (pCapwapIsSetCapwapBaseWtpProfileEntry->bCapwapBaseWtpProfileRowStatus !=
        OSIX_FALSE)
    {
        /* If the command action is to enable the row status, then the WTP profile name,
         * WTP Mac Addr, WTP model number, WTP name and Location should have been assigned 
         * valid values 
         */
        if (pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
            i4CapwapBaseWtpProfileRowStatus == ACTIVE)
        {
            /* Get the WTP profile Name and Validate */
            if ((nmhGetCapwapBaseWtpProfileName
                 (pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                  u4CapwapBaseWtpProfileId, &ProfileName) != SNMP_SUCCESS)
                && (pCapwapIsSetCapwapBaseWtpProfileEntry->
                    bCapwapBaseWtpProfileName == OSIX_FALSE))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }
            if ((ProfileName.pu1_OctetList == NULL) &&
                (pCapwapIsSetCapwapBaseWtpProfileEntry->
                 bCapwapBaseWtpProfileName != OSIX_TRUE))
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return OSIX_FAILURE;
            }
            /* Get the WTP Mac Address and Validate */
            if ((nmhGetCapwapBaseWtpProfileWtpMacAddress
                 (pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                  u4CapwapBaseWtpProfileId, &MacAddr) != SNMP_SUCCESS)
                && (pCapwapIsSetCapwapBaseWtpProfileEntry->
                    bCapwapBaseWtpProfileWtpMacAddress == OSIX_FALSE))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }

            if ((MacAddr.i4_Length == 0) &&
                (pCapwapIsSetCapwapBaseWtpProfileEntry->
                 bCapwapBaseWtpProfileWtpMacAddress != OSIX_TRUE))
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return OSIX_FAILURE;
            }

            /* Get the WTP model number and Validate */
            if ((nmhGetCapwapBaseWtpProfileWtpModelNumber
                 (pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                  u4CapwapBaseWtpProfileId, &ModelNumber) != SNMP_SUCCESS)
                && (pCapwapIsSetCapwapBaseWtpProfileEntry->
                    bCapwapBaseWtpProfileWtpModelNumber == OSIX_FALSE))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                return OSIX_FAILURE;
            }
            if ((ModelNumber.pu1_OctetList == NULL) &&
                (pCapwapIsSetCapwapBaseWtpProfileEntry->
                 bCapwapBaseWtpProfileWtpModelNumber != OSIX_TRUE))
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return OSIX_FAILURE;
            }

            /* Get the WTP name and Validate */
            if ((nmhGetCapwapBaseWtpProfileWtpName
                 (pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                  u4CapwapBaseWtpProfileId, &WtpName) != SNMP_SUCCESS)
                && (pCapwapIsSetCapwapBaseWtpProfileEntry->
                    bCapwapBaseWtpProfileWtpName == OSIX_FALSE))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                return OSIX_FAILURE;
            }
            if ((WtpName.pu1_OctetList == NULL) &&
                (pCapwapIsSetCapwapBaseWtpProfileEntry->
                 bCapwapBaseWtpProfileWtpName != OSIX_TRUE))
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return OSIX_FAILURE;
            }

            /* Get the Location  and Validate */
            if ((nmhGetCapwapBaseWtpProfileWtpLocation
                 (pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                  u4CapwapBaseWtpProfileId, &Location) != SNMP_SUCCESS)
                && (pCapwapIsSetCapwapBaseWtpProfileEntry->
                    bCapwapBaseWtpProfileWtpLocation == OSIX_FALSE))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                return OSIX_FAILURE;
            }
            if ((Location.pu1_OctetList == NULL) &&
                (pCapwapIsSetCapwapBaseWtpProfileEntry->
                 bCapwapBaseWtpProfileWtpLocation != OSIX_TRUE))
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return OSIX_FAILURE;
            }
        }

        if (nmhGetFirstIndexCapwapBaseWtpProfileTable (&u4WtpProfileId)
            != SNMP_SUCCESS)
        {
            return OSIX_SUCCESS;
        }

        do
        {
            if (u4WtpProfileId ==
                pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                u4CapwapBaseWtpProfileId)
            {
                return OSIX_SUCCESS;
            }
            u4WtpProfileCount++;
            if (u4WtpProfileCount == CLI_MAX_AP)
            {
                *pu4ErrorCode = SNMP_ERR_RESOURCE_UNAVAILABLE;
                return OSIX_FAILURE;
            }
            if (nmhGetCapwapBaseWtpProfileRowStatus (u4WtpProfileId,
                                                     &i4RowStatus) !=
                SNMP_SUCCESS)
            {
                return OSIX_SUCCESS;
            }
            if (i4RowStatus == ACTIVE)
            {
                u4Count++;
            }
            if ((u4Count == NUM_OF_AP_SUPPORTED) &&
                (pCapwapSetCapwapBaseWtpProfileEntry->MibObject.
                 i4CapwapBaseWtpProfileRowStatus == ACTIVE))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                return OSIX_FAILURE;
            }
            u4CurrentProfileId = u4WtpProfileId;
        }
        while (nmhGetNextIndexCapwapBaseWtpProfileTable (u4CurrentProfileId,
                                                         &u4WtpProfileId));

    }
    UNUSED_PARAM (i4RowStatusLogic);
    UNUSED_PARAM (i4RowCreateOption);
    CAPWAP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapTestCapwapBaseWtpSessionsLimit
 Input       :  pu4ErrorCode
                u4CapwapBaseWtpSessionsLimit
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapTestCapwapBaseWtpSessionsLimit (UINT4 *pu4ErrorCode,
                                      UINT4 u4CapwapBaseWtpSessionsLimit)
{
    if (u4CapwapBaseWtpSessionsLimit > 65535)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4CapwapBaseWtpSessionsLimit);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapTestCapwapBaseStationSessionsLimit
 Input       :  pu4ErrorCode
                u4CapwapBaseStationSessionsLimit
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapTestCapwapBaseStationSessionsLimit (UINT4 *pu4ErrorCode,
                                          UINT4
                                          u4CapwapBaseStationSessionsLimit)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4CapwapBaseStationSessionsLimit);
    if (u4CapwapBaseStationSessionsLimit > 65535)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
Function     : CapwapTestFsdtlsencrption
Input       :  pu4ErrorCode
               u4Fsdtlsencrption
Description : Set the encryption type for DTLS.
Output      :  None
Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapTestFsDtlsEncrption (UINT4 *pu4ErrorCode, UINT4 u4FsDtlsEncrption)
{
    if ((u4FsDtlsEncrption != CLI_CAPWAP_FSDTLSENCRPTION_PSK) &&
        (u4FsDtlsEncrption != CLI_CAPWAP_FSDTLSENCRPTION_CER))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;

}

/****************************************************************************
Function     : CapwapTestFsDtlsEncAuthEncryptAlgorithm
Input       : pu4ErrorCode
              u4FsDtlsEncAuthEncryptAlgorithm
Description : Set the encryption Algorithm.
 Output      :  None
Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapTestFsDtlsEncAuthEncryptAlgorithm (UINT4 *pu4ErrorCode, UINT4
                                         u4FsDtlsEncAuthEncryptAlgorithm)
{
    if ((u4FsDtlsEncAuthEncryptAlgorithm !=
         CLI_CAPWAP_FSDTLSENCAUTHENCRYPTALGORITHM_AES)
        && (u4FsDtlsEncAuthEncryptAlgorithm !=
            CLI_CAPWAP_FSDTLSENCAUTHENCRYPTALGORITHM_DHE_AES)
        && (u4FsDtlsEncAuthEncryptAlgorithm !=
            CLI_CAPWAP_FSDTLSENCAUTHENCRYPTALGORITHM_AES256)
        && (u4FsDtlsEncAuthEncryptAlgorithm !=
            CLI_CAPWAP_FSDTLSENCAUTHENCRYPTALGORITHM_DHE_AES256))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapTestCapwapBaseAcMaxRetransmit
 Input       :  pu4ErrorCode
                u4CapwapBaseAcMaxRetransmit
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapTestCapwapBaseAcMaxRetransmit (UINT4 *pu4ErrorCode,
                                     UINT4 u4CapwapBaseAcMaxRetransmit)
{
    if (u4CapwapBaseAcMaxRetransmit > RETRANSMIT_TIMER_UPPER_LIMIT)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapTestCapwapBaseAcChangeStatePendingTimer
 Input       :  pu4ErrorCode
                u4CapwapBaseAcChangeStatePendingTimer
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapTestCapwapBaseAcChangeStatePendingTimer (UINT4 *pu4ErrorCode,
                                               UINT4
                                               u4CapwapBaseAcChangeStatePendingTimer)
{
    if ((u4CapwapBaseAcChangeStatePendingTimer < PENDING_TIMER_LOWER_LIMIT) ||
        (u4CapwapBaseAcChangeStatePendingTimer > PENDING_TIMER_UPPER_LIMIT))
    {
        printf ("\n Fn : %s Line: %d", __FUNCTION__, __LINE__);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapTestCapwapBaseAcDataCheckTimer
 Input       :  pu4ErrorCode
                u4CapwapBaseAcDataCheckTimer
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapTestCapwapBaseAcDataCheckTimer (UINT4 *pu4ErrorCode,
                                      UINT4 u4CapwapBaseAcDataCheckTimer)
{
    if ((u4CapwapBaseAcDataCheckTimer < DATA_CHECK_TIMER_LOWER_LIMIT)
        || (u4CapwapBaseAcDataCheckTimer > DATA_CHECK_TIMER_UPPER_LIMIT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapTestCapwapBaseAcDTLSSessionDeleteTimer
 Input       :  pu4ErrorCode
                u4CapwapBaseAcDTLSSessionDeleteTimer
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapTestCapwapBaseAcDTLSSessionDeleteTimer (UINT4 *pu4ErrorCode,
                                              UINT4
                                              u4CapwapBaseAcDTLSSessionDeleteTimer)
{

    if ((u4CapwapBaseAcDTLSSessionDeleteTimer <
         DTLS_SESSION_DEL_TIMER_LOWER_LIMIT)
        || (u4CapwapBaseAcDTLSSessionDeleteTimer >
            DTLS_SESSION_DEL_TIMER_UPPER_LIMIT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapTestCapwapBaseAcEchoInterval
 Input       :  pu4ErrorCode
                u4CapwapBaseAcEchoInterval
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapTestCapwapBaseAcEchoInterval (UINT4 *pu4ErrorCode,
                                    UINT4 u4CapwapBaseAcEchoInterval)
{

    if ((u4CapwapBaseAcEchoInterval < ECHO_INTERVAL_LOWER_LIMIT) ||
        (u4CapwapBaseAcEchoInterval > ECHO_INTERVAL_UPPER_LIMIT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapTestCapwapBaseAcRetransmitInterval
 Input       :  pu4ErrorCode
                u4CapwapBaseAcRetransmitInterval
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapTestCapwapBaseAcRetransmitInterval (UINT4 *pu4ErrorCode,
                                          UINT4
                                          u4CapwapBaseAcRetransmitInterval)
{
    if ((u4CapwapBaseAcRetransmitInterval < RETRANSMIT_INTERVAL_LOWER_LIMIT) ||
        (u4CapwapBaseAcRetransmitInterval > RETRANSMIT_INTERVAL_UPPER_LIMIT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapTestCapwapBaseAcSilentInterval
 Input       :  pu4ErrorCode
                u4CapwapBaseAcSilentInterval
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapTestCapwapBaseAcSilentInterval (UINT4 *pu4ErrorCode,
                                      UINT4 u4CapwapBaseAcSilentInterval)
{
    if ((u4CapwapBaseAcSilentInterval < SILENT_INTERVAL_LOWER_LIMIT) ||
        (u4CapwapBaseAcSilentInterval > SILENT_INTERVAL_UPPER_LIMIT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapTestCapwapBaseAcWaitDTLSTimer
 Input       :  pu4ErrorCode
                u4CapwapBaseAcWaitDTLSTimer
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapTestCapwapBaseAcWaitDTLSTimer (UINT4 *pu4ErrorCode,
                                     UINT4 u4CapwapBaseAcWaitDTLSTimer)
{
    if ((u4CapwapBaseAcWaitDTLSTimer < WAIT_DTLS_TIMER_LOWER_LIMIT) ||
        (u4CapwapBaseAcWaitDTLSTimer > WAIT_DTLS_TIMER_UPPER_LIMIT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapTestCapwapBaseAcWaitJoinTimer
 Input       :  pu4ErrorCode
                u4CapwapBaseAcWaitJoinTimer
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapTestCapwapBaseAcWaitJoinTimer (UINT4 *pu4ErrorCode,
                                     UINT4 u4CapwapBaseAcWaitJoinTimer)
{
    if ((u4CapwapBaseAcWaitJoinTimer < WAIT_JOIN_TIMER_LOWER_LIMIT) ||
        (u4CapwapBaseAcWaitJoinTimer > WAIT_JOIN_TIMER_UPPER_LIMIT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapTestCapwapBaseAcEcnSupport
 Input       :  pu4ErrorCode
                i4CapwapBaseAcEcnSupport
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapTestCapwapBaseAcEcnSupport (UINT4 *pu4ErrorCode,
                                  INT4 i4CapwapBaseAcEcnSupport)
{
    if ((i4CapwapBaseAcEcnSupport != CLI_CAPWAP_ECN_LIMITED) &&
        (i4CapwapBaseAcEcnSupport != CLI_CAPWAP_ECN_FULL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapTestCapwapBaseChannelUpDownNotifyEnable
 Input       :  pu4ErrorCode
                i4CapwapBaseChannelUpDownNotifyEnable
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapTestCapwapBaseChannelUpDownNotifyEnable (UINT4 *pu4ErrorCode,
                                               INT4
                                               i4CapwapBaseChannelUpDownNotifyEnable)
{
    if ((i4CapwapBaseChannelUpDownNotifyEnable != CLI_CAPWAP_NOTIFY_ENABLE) &&
        (i4CapwapBaseChannelUpDownNotifyEnable != CLI_CAPWAP_NOTIFY_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapTestCapwapBaseDecryptErrorNotifyEnable
 Input       :  pu4ErrorCode
                i4CapwapBaseDecryptErrorNotifyEnable
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapTestCapwapBaseDecryptErrorNotifyEnable (UINT4 *pu4ErrorCode,
                                              INT4
                                              i4CapwapBaseDecryptErrorNotifyEnable)
{
    if ((i4CapwapBaseDecryptErrorNotifyEnable != CLI_CAPWAP_NOTIFY_ENABLE) &&
        (i4CapwapBaseDecryptErrorNotifyEnable != CLI_CAPWAP_NOTIFY_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapTestCapwapBaseJoinFailureNotifyEnable
 Input       :  pu4ErrorCode
                i4CapwapBaseJoinFailureNotifyEnable
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapTestCapwapBaseJoinFailureNotifyEnable (UINT4 *pu4ErrorCode,
                                             INT4
                                             i4CapwapBaseJoinFailureNotifyEnable)
{
    if ((i4CapwapBaseJoinFailureNotifyEnable != CLI_CAPWAP_NOTIFY_ENABLE) &&
        (i4CapwapBaseJoinFailureNotifyEnable != CLI_CAPWAP_NOTIFY_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapTestCapwapBaseImageUpgradeFailureNotifyEnable
 Input       :  pu4ErrorCode
                i4CapwapBaseImageUpgradeFailureNotifyEnable
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapTestCapwapBaseImageUpgradeFailureNotifyEnable (UINT4 *pu4ErrorCode,
                                                     INT4
                                                     i4CapwapBaseImageUpgradeFailureNotifyEnable)
{
    if ((i4CapwapBaseImageUpgradeFailureNotifyEnable !=
         CLI_CAPWAP_NOTIFY_ENABLE)
        && (i4CapwapBaseImageUpgradeFailureNotifyEnable !=
            CLI_CAPWAP_NOTIFY_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapTestCapwapBaseConfigMsgErrorNotifyEnable
 Input       :  pu4ErrorCode
                i4CapwapBaseConfigMsgErrorNotifyEnable
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapTestCapwapBaseConfigMsgErrorNotifyEnable (UINT4 *pu4ErrorCode,
                                                INT4
                                                i4CapwapBaseConfigMsgErrorNotifyEnable)
{

    if ((i4CapwapBaseConfigMsgErrorNotifyEnable != CLI_CAPWAP_NOTIFY_ENABLE) &&
        (i4CapwapBaseConfigMsgErrorNotifyEnable != CLI_CAPWAP_NOTIFY_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapTestCapwapBaseRadioOperableStatusNotifyEnable
 Input       :  pu4ErrorCode
                i4CapwapBaseRadioOperableStatusNotifyEnable
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapTestCapwapBaseRadioOperableStatusNotifyEnable (UINT4 *pu4ErrorCode,
                                                     INT4
                                                     i4CapwapBaseRadioOperableStatusNotifyEnable)
{
    if ((i4CapwapBaseRadioOperableStatusNotifyEnable !=
         CLI_CAPWAP_NOTIFY_ENABLE)
        && (i4CapwapBaseRadioOperableStatusNotifyEnable !=
            CLI_CAPWAP_NOTIFY_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapTestCapwapBaseAuthenFailureNotifyEnable
 Input       :  pu4ErrorCode
                i4CapwapBaseAuthenFailureNotifyEnable
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapTestCapwapBaseAuthenFailureNotifyEnable (UINT4 *pu4ErrorCode,
                                               INT4
                                               i4CapwapBaseAuthenFailureNotifyEnable)
{
    if ((i4CapwapBaseAuthenFailureNotifyEnable != CLI_CAPWAP_NOTIFY_ENABLE) &&
        (i4CapwapBaseAuthenFailureNotifyEnable != CLI_CAPWAP_NOTIFY_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapTestAllFsWtpModelTable
 Input       :  pu4ErrorCode
                pCapwapSetFsWtpModelEntry
                pCapwapIsSetFsWtpModelEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
CapwapTestAllFsWtpModelTable (UINT4 *pu4ErrorCode,
                              tCapwapFsWtpModelEntry *
                              pCapwapSetFsWtpModelEntry,
                              tCapwapIsSetFsWtpModelEntry *
                              pCapwapIsSetFsWtpModelEntry,
                              INT4 i4RowStatusLogic, INT4 i4RowCreateOption)
{
    UINT4               u4CurrentProfileId = 0;
    UINT4               u4NextProfileId = 0;
    INT4                i4OutCome = 0;
    INT4                i4RowStatus = 0;
    tCapwapFsWtpModelEntry CapwapGetFsWtpModelEntry;
    tSNMP_OCTET_STRING_TYPE ModelName;
    tWssIfCapDB        *pCapwapGetDB = NULL;
    UINT1               au1ModelName[CLI_MODEL_NAME_LEN];

    MEMSET (&ModelName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1ModelName, 0, CLI_MODEL_NAME_LEN);
    MEMSET (&CapwapGetFsWtpModelEntry, 0, sizeof (CapwapGetFsWtpModelEntry));

    ModelName.pu1_OctetList = au1ModelName;

    if (CapwapCheckForAlphaNumeric (pCapwapSetFsWtpModelEntry->
                                    MibObject.au1FsCapwapWtpModelNumber) !=
        OSIX_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }
    if (pCapwapIsSetFsWtpModelEntry->bFsNoOfRadio == OSIX_TRUE)
    {
        if ((pCapwapSetFsWtpModelEntry->MibObject.u4FsNoOfRadio < 1)
            || (pCapwapSetFsWtpModelEntry->MibObject.u4FsNoOfRadio > 31))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }

        /* If a profile is already bounded to this model then dont allow to
         * change the entries */
        i4OutCome =
            nmhGetFirstIndexCapwapBaseWtpProfileTable (&u4NextProfileId);

        if (i4OutCome != SNMP_FAILURE)
        {
            do
            {
                u4CurrentProfileId = u4NextProfileId;

                if (nmhGetCapwapBaseWtpProfileWtpModelNumber
                    (u4CurrentProfileId, &ModelName) == SNMP_SUCCESS)
                {
                    if (MEMCMP (ModelName.pu1_OctetList,
                                pCapwapSetFsWtpModelEntry->MibObject.
                                au1FsCapwapWtpModelNumber,
                                CLI_MODEL_NAME_LEN) == 0)
                    {
                        if (nmhGetCapwapBaseWtpProfileRowStatus
                            (u4CurrentProfileId, &i4RowStatus) == SNMP_SUCCESS)
                        {
                            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                            return OSIX_FAILURE;
                        }
                    }
                    MEMSET (ModelName.pu1_OctetList, 0,
                            pCapwapSetFsWtpModelEntry->MibObject.
                            i4FsCapwapWtpModelNumberLen);
                }
            }
            while (nmhGetNextIndexCapwapBaseWtpProfileTable
                   (u4CurrentProfileId, &u4NextProfileId) == SNMP_SUCCESS);
        }
    }
    if (pCapwapIsSetFsWtpModelEntry->bFsCapwapWtpMacType == OSIX_TRUE)
    {
        if ((pCapwapSetFsWtpModelEntry->MibObject.i4FsCapwapWtpMacType !=
             CLI_MAC_TYPE_SPLIT)
            && (pCapwapSetFsWtpModelEntry->MibObject.i4FsCapwapWtpMacType !=
                CLI_MAC_TYPE_LOCAL))

        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }

        /* If a profile is already bounded to this model then dont allow to
         * change the entries */
        i4OutCome =
            nmhGetFirstIndexCapwapBaseWtpProfileTable (&u4NextProfileId);

        if (i4OutCome != SNMP_FAILURE)
        {
            do
            {
                u4CurrentProfileId = u4NextProfileId;

                if (nmhGetCapwapBaseWtpProfileWtpModelNumber
                    (u4CurrentProfileId, &ModelName) == SNMP_SUCCESS)
                {
                    if (MEMCMP (ModelName.pu1_OctetList,
                                pCapwapSetFsWtpModelEntry->MibObject.
                                au1FsCapwapWtpModelNumber,
                                CLI_MODEL_NAME_LEN) == 0)
                    {
                        if (nmhGetCapwapBaseWtpProfileRowStatus
                            (u4CurrentProfileId, &i4RowStatus) == SNMP_SUCCESS)
                        {
                            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                            return OSIX_FAILURE;
                        }
                    }
                    MEMSET (ModelName.pu1_OctetList, 0,
                            pCapwapSetFsWtpModelEntry->MibObject.
                            i4FsCapwapWtpModelNumberLen);
                }
            }
            while (nmhGetNextIndexCapwapBaseWtpProfileTable
                   (u4CurrentProfileId, &u4NextProfileId) == SNMP_SUCCESS);
        }
    }
    if (pCapwapIsSetFsWtpModelEntry->bFsCapwapWtpTunnelMode == OSIX_TRUE)
    {
        if ((pCapwapSetFsWtpModelEntry->MibObject.au1FsCapwapWtpTunnelMode[0] !=
             CLI_TUNNEL_MODE_BRIDGE)
            && (pCapwapSetFsWtpModelEntry->MibObject.
                au1FsCapwapWtpTunnelMode[0] != CLI_TUNNEL_MODE_DOT3)
            && (pCapwapSetFsWtpModelEntry->MibObject.
                au1FsCapwapWtpTunnelMode[0] != CLI_TUNNEL_MODE_NATIVE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }

        /* If a profile is already bounded to this model then dont allow to
         * change the entries */
        i4OutCome =
            nmhGetFirstIndexCapwapBaseWtpProfileTable (&u4NextProfileId);

        if (i4OutCome != SNMP_FAILURE)
        {
            do
            {
                u4CurrentProfileId = u4NextProfileId;

                if (nmhGetCapwapBaseWtpProfileWtpModelNumber
                    (u4CurrentProfileId, &ModelName) == SNMP_SUCCESS)
                {
                    if (MEMCMP (ModelName.pu1_OctetList,
                                pCapwapSetFsWtpModelEntry->MibObject.
                                au1FsCapwapWtpModelNumber,
                                CLI_MODEL_NAME_LEN) == 0)
                    {
                        if (nmhGetCapwapBaseWtpProfileRowStatus
                            (u4CurrentProfileId, &i4RowStatus) == SNMP_SUCCESS)
                        {
                            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                            return OSIX_FAILURE;
                        }
                    }
                    MEMSET (ModelName.pu1_OctetList, 0,
                            pCapwapSetFsWtpModelEntry->MibObject.
                            i4FsCapwapWtpModelNumberLen);
                }
            }
            while (nmhGetNextIndexCapwapBaseWtpProfileTable
                   (u4CurrentProfileId, &u4NextProfileId) == SNMP_SUCCESS);
        }
    }
    if (pCapwapIsSetFsWtpModelEntry->bFsCapwapImageName != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pCapwapIsSetFsWtpModelEntry->bFsCapwapQosProfileName != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pCapwapIsSetFsWtpModelEntry->bFsMaxStations != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pCapwapIsSetFsWtpModelEntry->bFsWtpModelRowStatus == OSIX_TRUE)
    {
        if (pCapwapSetFsWtpModelEntry->MibObject.i4FsWtpModelRowStatus !=
            DESTROY)
        {
            WSS_IF_DB_TEMP_MEM_ALLOC (pCapwapGetDB, OSIX_FAILURE)
                MEMSET (pCapwapGetDB, 0, sizeof (tWssIfCapDB));

            pCapwapGetDB->CapwapIsGetAllDB.bModelCount = OSIX_TRUE;
            if (WssIfProcessCapwapDBMsg
                (WSS_CAPWAP_GET_MODEL_COUNT, pCapwapGetDB) == OSIX_SUCCESS)
            {
                if (pCapwapGetDB->CapwapGetDB.WtpModelEntry.u1ModelCount >
                    WTP_MODEL_COUNT)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    WSS_IF_DB_TEMP_MEM_RELEASE (pCapwapGetDB);
                    return OSIX_FAILURE;
                }
            }
            WSS_IF_DB_TEMP_MEM_RELEASE (pCapwapGetDB);
        }
        /*Fill your check condition */
        if (pCapwapSetFsWtpModelEntry->MibObject.i4FsWtpModelRowStatus ==
            ACTIVE)
        {
            MEMCPY (CapwapGetFsWtpModelEntry.MibObject.
                    au1FsCapwapWtpModelNumber,
                    pCapwapSetFsWtpModelEntry->MibObject.
                    au1FsCapwapWtpModelNumber,
                    pCapwapSetFsWtpModelEntry->MibObject.
                    i4FsCapwapWtpModelNumberLen);

            CapwapGetFsWtpModelEntry.MibObject.i4FsCapwapWtpModelNumberLen =
                pCapwapSetFsWtpModelEntry->MibObject.
                i4FsCapwapWtpModelNumberLen;

            /* Get the table entry to check whether the mandatory arguments are 
             * already configured. If not return error */
            if (CapwapGetAllFsWtpModelTable (&CapwapGetFsWtpModelEntry) !=
                OSIX_SUCCESS)
            {
                return OSIX_SUCCESS;
            }

            if (pCapwapSetFsWtpModelEntry->MibObject.i4FsWtpModelRowStatus !=
                CapwapGetFsWtpModelEntry.MibObject.i4FsWtpModelRowStatus)
            {
                if (((CapwapGetFsWtpModelEntry.MibObject.u4FsNoOfRadio == 0) &&
                     (pCapwapSetFsWtpModelEntry->MibObject.u4FsNoOfRadio == 0))
                    ||
                    ((CapwapGetFsWtpModelEntry.MibObject.
                      au1FsCapwapWtpTunnelMode[0] == 0)
                     && (pCapwapSetFsWtpModelEntry->MibObject.
                         au1FsCapwapWtpTunnelMode[0] == 0)))
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return OSIX_FAILURE;
                }
            }
        }
        else if (pCapwapSetFsWtpModelEntry->MibObject.i4FsWtpModelRowStatus ==
                 DESTROY)
        {
            i4OutCome =
                nmhGetFirstIndexCapwapBaseWtpProfileTable (&u4NextProfileId);

            if (i4OutCome == SNMP_FAILURE)
            {
                return OSIX_SUCCESS;
            }
            else
            {
                do
                {
                    u4CurrentProfileId = u4NextProfileId;

                    if (nmhGetCapwapBaseWtpProfileWtpModelNumber
                        (u4CurrentProfileId, &ModelName) == SNMP_SUCCESS)
                    {
                        if (MEMCMP (ModelName.pu1_OctetList,
                                    pCapwapSetFsWtpModelEntry->MibObject.
                                    au1FsCapwapWtpModelNumber,
                                    CLI_MODEL_NAME_LEN) == 0)
                        {
                            if (nmhGetCapwapBaseWtpProfileRowStatus
                                (u4CurrentProfileId,
                                 &i4RowStatus) == SNMP_SUCCESS)
                            {
                                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                                return OSIX_FAILURE;
                            }
                        }
                        MEMSET (ModelName.pu1_OctetList, 0,
                                pCapwapSetFsWtpModelEntry->MibObject.
                                i4FsCapwapWtpModelNumberLen);
                    }
                }
                while (nmhGetNextIndexCapwapBaseWtpProfileTable
                       (u4CurrentProfileId, &u4NextProfileId) == SNMP_SUCCESS);
            }

        }
    }
    UNUSED_PARAM (i4RowStatusLogic);
    UNUSED_PARAM (i4RowCreateOption);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapTestAllFsWtpRadioTable
 Input       :  pu4ErrorCode
                pCapwapSetFsWtpRadioEntry
                pCapwapIsSetFsWtpRadioEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
CapwapTestAllFsWtpRadioTable (UINT4 *pu4ErrorCode,
                              tCapwapFsWtpRadioEntry *
                              pCapwapSetFsWtpRadioEntry,
                              tCapwapIsSetFsWtpRadioEntry *
                              pCapwapIsSetFsWtpRadioEntry,
                              INT4 i4RowStatusLogic, INT4 i4RowCreateOption)
{

    UINT1               au1ModelNumber[CLI_MODEL_NAME_LEN];
    UINT4               u4CurrentProfileId = 0;
    UINT4               u4NextProfileId = 0;
    INT4                i4OutCome = 0;
    INT4                i4RetValFsNoofRadio = 0;
    tSNMP_OCTET_STRING_TYPE ModelNumber;
    tCapwapFsWtpModelEntry CapwapGetFsWtpModelEntry;

    MEMSET (&ModelNumber, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&CapwapGetFsWtpModelEntry, 0, sizeof (CapwapGetFsWtpModelEntry));
    MEMSET (au1ModelNumber, 0, 256);

    MEMCPY (au1ModelNumber,
            pCapwapSetFsWtpRadioEntry->MibObject.au1FsCapwapWtpModelNumber,
            pCapwapSetFsWtpRadioEntry->MibObject.i4FsCapwapWtpModelNumberLen);

    ModelNumber.pu1_OctetList = au1ModelNumber;
    ModelNumber.i4_Length =
        pCapwapSetFsWtpRadioEntry->MibObject.i4FsCapwapWtpModelNumberLen;

    if (nmhGetFsNoOfRadio (&ModelNumber, (UINT4 *) &i4RetValFsNoofRadio) !=
        SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    if (pCapwapIsSetFsWtpRadioEntry->bFsNumOfRadio != OSIX_FALSE)
    {
        if ((pCapwapSetFsWtpRadioEntry->MibObject.u4FsNumOfRadio < 1) ||
            (pCapwapSetFsWtpRadioEntry->MibObject.u4FsNumOfRadio >
             ((UINT4) i4RetValFsNoofRadio)))
        {
            CLI_SET_ERR (CAPW_CLI_INVALID_RADIO_ID);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pCapwapIsSetFsWtpRadioEntry->bFsWtpRadioType != OSIX_FALSE)
    {
        if (((pCapwapSetFsWtpRadioEntry->MibObject.
              u4FsWtpRadioType) != CLI_RADIO_TYPEA) &&
            ((pCapwapSetFsWtpRadioEntry->MibObject.
              u4FsWtpRadioType) != CLI_RADIO_TYPEAN) &&
            ((pCapwapSetFsWtpRadioEntry->MibObject.
              u4FsWtpRadioType) != CLI_RADIO_TYPEB) &&
            ((pCapwapSetFsWtpRadioEntry->MibObject.
              u4FsWtpRadioType) != CLI_RADIO_TYPEBG) &&
            ((pCapwapSetFsWtpRadioEntry->MibObject.
              u4FsWtpRadioType) != CLI_RADIO_TYPEBGN) &&
            ((pCapwapSetFsWtpRadioEntry->MibObject.
              u4FsWtpRadioType) != CLI_RADIO_TYPEG) &&
            ((pCapwapSetFsWtpRadioEntry->MibObject.
              u4FsWtpRadioType) != CLI_RADIO_TYPEAC))

        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }

    }
    if (pCapwapIsSetFsWtpRadioEntry->bFsRadioAdminStatus != OSIX_FALSE)
    {
        if ((pCapwapSetFsWtpRadioEntry->MibObject.i4FsRadioAdminStatus !=
             CLI_CAPWAP_RADIO_ENABLE)
            && (pCapwapSetFsWtpRadioEntry->MibObject.i4FsRadioAdminStatus !=
                CLI_CAPWAP_RADIO_DISABLE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pCapwapIsSetFsWtpRadioEntry->bFsMaxSSIDSupported != OSIX_FALSE)
    {
        if ((pCapwapSetFsWtpRadioEntry->MibObject.i4FsMaxSSIDSupported < 1) ||
            (pCapwapSetFsWtpRadioEntry->MibObject.i4FsMaxSSIDSupported > 16))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }

    MEMCPY (CapwapGetFsWtpModelEntry.MibObject.au1FsCapwapWtpModelNumber,
            pCapwapSetFsWtpRadioEntry->MibObject.au1FsCapwapWtpModelNumber,
            pCapwapSetFsWtpRadioEntry->MibObject.i4FsCapwapWtpModelNumberLen);

    CapwapGetFsWtpModelEntry.MibObject.i4FsCapwapWtpModelNumberLen =
        pCapwapSetFsWtpRadioEntry->MibObject.i4FsCapwapWtpModelNumberLen;

    /* Get the table entry to check whether the mandatory arguments are 
     * already configured. If not return error */
    if (CapwapGetAllFsWtpModelTable (&CapwapGetFsWtpModelEntry) != OSIX_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return OSIX_FAILURE;
    }
    if (CapwapGetFsWtpModelEntry.MibObject.i4FsWtpModelRowStatus == ACTIVE)
    {
        i4OutCome =
            nmhGetFirstIndexCapwapBaseWtpProfileTable (&u4NextProfileId);

        if (i4OutCome == SNMP_FAILURE)
        {
            return OSIX_SUCCESS;
        }
        do
        {
            u4CurrentProfileId = u4NextProfileId;
            MEMSET (&ModelNumber, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
            MEMSET (au1ModelNumber, 0, 256);
            ModelNumber.pu1_OctetList = au1ModelNumber;

            if (nmhGetCapwapBaseWtpProfileWtpModelNumber (u4CurrentProfileId,
                                                          &ModelNumber) ==
                SNMP_SUCCESS)
            {
                if (MEMCMP (ModelNumber.pu1_OctetList,
                            pCapwapSetFsWtpRadioEntry->MibObject.
                            au1FsCapwapWtpModelNumber, CLI_MODEL_NAME_LEN) == 0)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return OSIX_FAILURE;
                }
                MEMSET (ModelNumber.pu1_OctetList, 0,
                        pCapwapSetFsWtpRadioEntry->MibObject.
                        i4FsCapwapWtpModelNumberLen);
            }
        }
        while (nmhGetNextIndexCapwapBaseWtpProfileTable
               (u4CurrentProfileId, &u4NextProfileId) == SNMP_SUCCESS);
    }
    UNUSED_PARAM (i4RowStatusLogic);
    UNUSED_PARAM (i4RowCreateOption);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapTestAllFsCapwapWhiteListTable
 Input       :  pu4ErrorCode
                pCapwapSetFsCapwapWhiteListEntry
                pCapwapIsSetFsCapwapWhiteListEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
CapwapTestAllFsCapwapWhiteListTable (UINT4 *pu4ErrorCode,
                                     tCapwapFsCapwapWhiteListEntry *
                                     pCapwapSetFsCapwapWhiteListEntry,
                                     tCapwapIsSetFsCapwapWhiteListEntry *
                                     pCapwapIsSetFsCapwapWhiteListEntry,
                                     INT4 i4RowStatusLogic,
                                     INT4 i4RowCreateOption)
{
    UINT4               u4WhiteListId = 0;
    UINT1               MacAddr[20];
    tSNMP_OCTET_STRING_TYPE MacAddress;
    BOOL1               bEntryAlreadyPresent = OSIX_FALSE;
    MEMSET (&MacAddress, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&MacAddr, 0, 20);

    if (pCapwapSetFsCapwapWhiteListEntry->MibObject.
        au1FsCapwapWhiteListWtpBaseMac[0] == 1)
    {
        /* Check for multicast address */
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;

    }
    if (pCapwapIsSetFsCapwapWhiteListEntry->bFsCapwapWhiteListWtpBaseMac !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
        for (u4WhiteListId = 1; u4WhiteListId <= CLI_WTP_MAX_ACL_ID;
             u4WhiteListId++)
        {
            MacAddress.pu1_OctetList = MacAddr;
            if (nmhGetFsCapwapWhiteListWtpBaseMac (u4WhiteListId,
                                                   &MacAddress) != SNMP_SUCCESS)
            {
                continue;
            }
            /* if (STRCMP(MacAddress.pu1_OctetList,   */
            if (MEMCMP (MacAddr,
                        pCapwapSetFsCapwapWhiteListEntry->MibObject.
                        au1FsCapwapWhiteListWtpBaseMac,
                        pCapwapSetFsCapwapWhiteListEntry->MibObject.
                        i4FsCapwapWhiteListWtpBaseMacLen) == 0)
            {
                if (pCapwapSetFsCapwapWhiteListEntry->MibObject.
                    i4FsCapwapWhiteListRowStatus != DESTROY)
                {
                    /* Throw error if entry already present */
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return OSIX_FAILURE;
                }
                else
                {
                    bEntryAlreadyPresent = OSIX_TRUE;
                    break;
                }
            }
        }
        if ((bEntryAlreadyPresent == OSIX_FALSE) &&
            (pCapwapSetFsCapwapWhiteListEntry->MibObject.
             i4FsCapwapWhiteListRowStatus == DESTROY))
        {
            /* If entry to be deleted is not present */
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return OSIX_FAILURE;

        }
        /*Fill your check condition */
    }
    if (pCapwapIsSetFsCapwapWhiteListEntry->bFsCapwapWhiteListRowStatus !=
        OSIX_FALSE)
    {
        /*Fill your check condition */

    }
    UNUSED_PARAM (i4RowStatusLogic);
    UNUSED_PARAM (i4RowCreateOption);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapTestAllFsCapwapBlackList
 Input       :  pu4ErrorCode
                pCapwapSetFsCapwapBlackListEntry
                pCapwapIsSetFsCapwapBlackListEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
CapwapTestAllFsCapwapBlackList (UINT4 *pu4ErrorCode,
                                tCapwapFsCapwapBlackListEntry *
                                pCapwapSetFsCapwapBlackListEntry,
                                tCapwapIsSetFsCapwapBlackListEntry *
                                pCapwapIsSetFsCapwapBlackListEntry,
                                INT4 i4RowStatusLogic, INT4 i4RowCreateOption)
{
    UINT4               u4BlackListId = 0;
    UINT1               MacAddr[20];
    tSNMP_OCTET_STRING_TYPE MacAddress;
    BOOL1               bEntryAlreadyPresent = OSIX_FALSE;
    MEMSET (&MacAddress, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&MacAddr, 0, 20);

    if (pCapwapSetFsCapwapBlackListEntry->MibObject.
        au1FsCapwapBlackListWtpBaseMac[0] == 1)
    {
        /* Check for multicast address */
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    if (pCapwapIsSetFsCapwapBlackListEntry->bFsCapwapBlackListWtpBaseMac !=
        OSIX_FALSE)
    {
        for (u4BlackListId = 1; u4BlackListId <= CLI_WTP_MAX_ACL_ID;
             u4BlackListId++)
        {
            MacAddress.pu1_OctetList = MacAddr;
            if (nmhGetFsCapwapBlackListWtpBaseMac (u4BlackListId,
                                                   &MacAddress) != SNMP_SUCCESS)
            {
                continue;
            }
            /* if (STRCMP(MacAddress.pu1_OctetList,   */
            if (MEMCMP (MacAddr,
                        pCapwapSetFsCapwapBlackListEntry->MibObject.
                        au1FsCapwapBlackListWtpBaseMac,
                        pCapwapSetFsCapwapBlackListEntry->MibObject.
                        i4FsCapwapBlackListWtpBaseMacLen) == 0)
            {
                if (pCapwapSetFsCapwapBlackListEntry->MibObject.
                    i4FsCapwapBlackListRowStatus != DESTROY)
                {
                    /* Throw error if entry already present */
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return OSIX_FAILURE;
                }
                else
                {
                    bEntryAlreadyPresent = OSIX_TRUE;
                    break;
                }
            }
        }
        if ((bEntryAlreadyPresent == OSIX_FALSE) &&
            (pCapwapSetFsCapwapBlackListEntry->MibObject.
             i4FsCapwapBlackListRowStatus == DESTROY))
        {
            /* If entry to be deleted is not present */
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return OSIX_FAILURE;

        }
        /*Fill your check condition */
    }
    if (pCapwapIsSetFsCapwapBlackListEntry->bFsCapwapBlackListRowStatus !=
        OSIX_FALSE)
    {
        /*Fill your check condition */

    }

    UNUSED_PARAM (i4RowStatusLogic);
    UNUSED_PARAM (i4RowCreateOption);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapTestAllFsCapwapWtpConfigTable
 Input       :  pu4ErrorCode
                pCapwapSetFsCapwapWtpConfigEntry
                pCapwapIsSetFsCapwapWtpConfigEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
CapwapTestAllFsCapwapWtpConfigTable (UINT4 *pu4ErrorCode,
                                     tCapwapFsCapwapWtpConfigEntry *
                                     pCapwapSetFsCapwapWtpConfigEntry,
                                     tCapwapIsSetFsCapwapWtpConfigEntry *
                                     pCapwapIsSetFsCapwapWtpConfigEntry,
                                     INT4 i4RowStatusLogic,
                                     INT4 i4RowCreateOption)
{
    UINT4               u4Index = 0;

    if (pCapwapIsSetFsCapwapWtpConfigEntry->bFsCapwapWtpReset != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pCapwapIsSetFsCapwapWtpConfigEntry->bFsCapwapClearConfig != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pCapwapIsSetFsCapwapWtpConfigEntry->bFsWtpDiscoveryType != OSIX_FALSE)
    {
        /*Fill your check condition */
        if ((pCapwapSetFsCapwapWtpConfigEntry->MibObject.i4FsWtpDiscoveryType !=
             CLI_WTP_DISC_TYPE_STATIC)
            && (pCapwapSetFsCapwapWtpConfigEntry->MibObject.
                i4FsWtpDiscoveryType != CLI_WTP_DISC_TYPE_DHCP)
            && (pCapwapSetFsCapwapWtpConfigEntry->MibObject.
                i4FsWtpDiscoveryType != CLI_WTP_DISC_TYPE_DNS)
            && (pCapwapSetFsCapwapWtpConfigEntry->MibObject.
                i4FsWtpDiscoveryType != CLI_WTP_DISC_TYPE_REFERRAL))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pCapwapSetFsCapwapWtpConfigEntry->MibObject.i4FsWtpDiscoveryType ==
        CLI_WTP_DISC_TYPE_STATIC)
    {
        if (!
            ((CFA_IS_ADDR_CLASS_A
              (pCapwapSetFsCapwapWtpConfigEntry->MibObject.
               u4FsWlcStaticIpAddress)
              ? (IP_IS_ZERO_NETWORK
                 (pCapwapSetFsCapwapWtpConfigEntry->MibObject.
                  u4FsWlcStaticIpAddress) ? 0 : 1) : 0)
             ||
             (CFA_IS_ADDR_CLASS_B
              (pCapwapSetFsCapwapWtpConfigEntry->MibObject.
               u4FsWlcStaticIpAddress))
             ||
             (CFA_IS_ADDR_CLASS_C
              (pCapwapSetFsCapwapWtpConfigEntry->MibObject.
               u4FsWlcStaticIpAddress))
             ||
             (CFA_IS_ADDR_CLASS_E
              (pCapwapSetFsCapwapWtpConfigEntry->MibObject.
               u4FsWlcStaticIpAddress))))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pCapwapIsSetFsCapwapWtpConfigEntry->bFsWtpCountryString != OSIX_FALSE)
    {
        for (u4Index = 0; u4Index < COUNTRY_MAX; u4Index++)
        {
            if (STRCMP
                (pCapwapSetFsCapwapWtpConfigEntry->MibObject.
                 au1FsWtpCountryString,
                 gWsscfgSupportedCountryList[u4Index].au1CountryCode) == 0)
            {
                break;
            }
        }
        if (u4Index == COUNTRY_MAX)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }

    if (pCapwapIsSetFsCapwapWtpConfigEntry->bFsCapwapClearApStats != OSIX_FALSE)
    {
    }

    if (pCapwapIsSetFsCapwapWtpConfigEntry->bFsCapwapWtpConfigRowStatus !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pCapwapIsSetFsCapwapWtpConfigEntry->bFsWtpCrashDumpFileName !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pCapwapIsSetFsCapwapWtpConfigEntry->bFsWtpMemoryDumpFileName !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pCapwapIsSetFsCapwapWtpConfigEntry->bFsWtpDeleteOperation != OSIX_FALSE)
    {
        if ((pCapwapSetFsCapwapWtpConfigEntry->MibObject.
             u4FsWtpDeleteOperation != CLI_CRASHFILE_GET)
            && (pCapwapSetFsCapwapWtpConfigEntry->MibObject.
                u4FsWtpDeleteOperation != CLI_CRASHFILE_DELETE)
            && (pCapwapSetFsCapwapWtpConfigEntry->MibObject.
                u4FsWtpDeleteOperation != CLI_NO_CRASHMEMORY))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return OSIX_FAILURE;
        }
    }
    UNUSED_PARAM (i4RowStatusLogic);
    UNUSED_PARAM (i4RowCreateOption);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapTestAllFsCapwapLinkEncryptionTable
 Input       :  pu4ErrorCode
                pCapwapSetFsCapwapLinkEncryptionEntry
                pCapwapIsSetFsCapwapLinkEncryptionEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
CapwapTestAllFsCapwapLinkEncryptionTable (UINT4 *pu4ErrorCode,
                                          tCapwapFsCapwapLinkEncryptionEntry *
                                          pCapwapSetFsCapwapLinkEncryptionEntry,
                                          tCapwapIsSetFsCapwapLinkEncryptionEntry
                                          *
                                          pCapwapIsSetFsCapwapLinkEncryptionEntry,
                                          INT4 i4RowStatusLogic,
                                          INT4 i4RowCreateOption)
{
    UNUSED_PARAM (i4RowStatusLogic);
    UNUSED_PARAM (i4RowCreateOption);

    if (pCapwapIsSetFsCapwapLinkEncryptionEntry->
        bFsCapwapEncryptChannelStatus != OSIX_FALSE)
    {
        /*Fill your check condition */
        if ((pCapwapSetFsCapwapLinkEncryptionEntry->MibObject.
             i4FsCapwapEncryptChannelStatus != CLI_FSCAPWAP_ENCRYPTION_ENABLE)
            && (pCapwapSetFsCapwapLinkEncryptionEntry->MibObject.
                i4FsCapwapEncryptChannelStatus !=
                CLI_FSCAPWAP_ENCRYPTION_DISABLE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pCapwapIsSetFsCapwapLinkEncryptionEntry->
        bFsCapwapEncryptChannelRowStatus != OSIX_FALSE)
    {
        /*Fill your check condition */
        if ((pCapwapSetFsCapwapLinkEncryptionEntry->MibObject.
             i4FsCapwapEncryptChannelRowStatus != CREATE_AND_GO)
            && (pCapwapSetFsCapwapLinkEncryptionEntry->MibObject.
                i4FsCapwapEncryptChannelRowStatus != ACTIVE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if ((pCapwapIsSetFsCapwapLinkEncryptionEntry->
         bFsCapwapEncryptChannel != OSIX_FALSE)
        && (pCapwapIsSetFsCapwapLinkEncryptionEntry->
            bFsCapwapEncryptChannelStatus != OSIX_FALSE))
    {
        if ((pCapwapSetFsCapwapLinkEncryptionEntry->MibObject.
             i4FsCapwapEncryptChannel == CLI_FSCAPWAP_ENCRYPTION_DATA)
            && (pCapwapSetFsCapwapLinkEncryptionEntry->MibObject.
                i4FsCapwapEncryptChannelStatus ==
                CLI_FSCAPWAP_ENCRYPTION_ENABLE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CAPW_CLI_DATA_ENCRPTION_ERR);
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapTestAllFsCawapDefaultWtpProfileTable
 Input       :  pu4ErrorCode
                pCapwapSetFsCapwapDefaultWtpProfileEntry
                pCapwapIsSetFsCapwapDefaultWtpProfileEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
CapwapTestAllFsCawapDefaultWtpProfileTable (UINT4 *pu4ErrorCode,
                                            tCapwapFsCapwapDefaultWtpProfileEntry
                                            *
                                            pCapwapSetFsCapwapDefaultWtpProfileEntry,
                                            tCapwapIsSetFsCapwapDefaultWtpProfileEntry
                                            *
                                            pCapwapIsSetFsCapwapDefaultWtpProfileEntry,
                                            INT4 i4RowStatusLogic,
                                            INT4 i4RowCreateOption)
{
    UNUSED_PARAM (i4RowStatusLogic);
    UNUSED_PARAM (i4RowCreateOption);

    if (pCapwapIsSetFsCapwapDefaultWtpProfileEntry->
        bFsCapwapDefaultWtpProfileWtpFallbackEnable != OSIX_FALSE)
    {
        /*Fill your check condition */
        if ((pCapwapSetFsCapwapDefaultWtpProfileEntry->MibObject.
             i4FsCapwapDefaultWtpProfileWtpFallbackEnable !=
             CLI_CAPWAP_FALLBACK_ENABLE)
            && (pCapwapSetFsCapwapDefaultWtpProfileEntry->MibObject.
                i4FsCapwapDefaultWtpProfileWtpFallbackEnable !=
                CLI_CAPWAP_FALLBACK_DISABLE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }

    }
    if (pCapwapIsSetFsCapwapDefaultWtpProfileEntry->
        bFsCapwapDefaultWtpProfileWtpEchoInterval != OSIX_FALSE)
    {
        /*Fill your check condition */
        /* Coverity Fix Start 9-7-13 */
        if (pCapwapSetFsCapwapDefaultWtpProfileEntry->MibObject.
            u4FsCapwapDefaultWtpProfileWtpEchoInterval > 65536)
            /* Coverity Fix End 9-7-13 */
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pCapwapIsSetFsCapwapDefaultWtpProfileEntry->
        bFsCapwapDefaultWtpProfileWtpIdleTimeout != OSIX_FALSE)
    {
        /*Fill your check condition */
        if ((pCapwapSetFsCapwapDefaultWtpProfileEntry->MibObject.
             u4FsCapwapDefaultWtpProfileWtpIdleTimeout < 60)
            || (pCapwapSetFsCapwapDefaultWtpProfileEntry->MibObject.
                u4FsCapwapDefaultWtpProfileWtpIdleTimeout > 600))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }

    }
    if (pCapwapIsSetFsCapwapDefaultWtpProfileEntry->
        bFsCapwapDefaultWtpProfileWtpMaxDiscoveryInterval != OSIX_FALSE)
    {
        /*Fill your check condition */
        if ((pCapwapSetFsCapwapDefaultWtpProfileEntry->MibObject.
             u4FsCapwapDefaultWtpProfileWtpMaxDiscoveryInterval > 180)
            || (pCapwapSetFsCapwapDefaultWtpProfileEntry->MibObject.
                u4FsCapwapDefaultWtpProfileWtpMaxDiscoveryInterval < 2))

        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }

    }
    if (pCapwapIsSetFsCapwapDefaultWtpProfileEntry->
        bFsCapwapDefaultWtpProfileWtpReportInterval != OSIX_FALSE)
    {
        /*Fill your check condition */
        /* Coverity Fix Start 11-7-13 *//* u4FsCapwapDefaultWtpProfileWtpReportInterval - declared as UINT, so it never be < 0 */
        if (pCapwapSetFsCapwapDefaultWtpProfileEntry->MibObject.
            u4FsCapwapDefaultWtpProfileWtpReportInterval > 65536)
            /* Coverity Fix End 11-7-13 */
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pCapwapIsSetFsCapwapDefaultWtpProfileEntry->
        bFsCapwapDefaultWtpProfileWtpStatisticsTimer != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pCapwapIsSetFsCapwapDefaultWtpProfileEntry->
        bFsCapwapDefaultWtpProfileWtpEcnSupport != OSIX_FALSE)
    {
        /*Fill your check condition */
        if ((pCapwapSetFsCapwapDefaultWtpProfileEntry->MibObject.
             i4FsCapwapDefaultWtpProfileWtpEcnSupport != CLI_CAPWAP_ECN_LIMITED)
            && (pCapwapSetFsCapwapDefaultWtpProfileEntry->MibObject.
                i4FsCapwapDefaultWtpProfileWtpEcnSupport !=
                CLI_CAPWAP_ECN_FULL))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }

    }
    if (pCapwapIsSetFsCapwapDefaultWtpProfileEntry->
        bFsCapwapDefaultWtpProfileRowStatus != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapTestAllFsCapwapDnsProfileTable
 Input       :  pu4ErrorCode
                pCapwapSetFsCapwapDnsProfileEntry
                pCapwapIsSetFsCapwapDnsProfileEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
CapwapTestAllFsCapwapDnsProfileTable (UINT4 *pu4ErrorCode,
                                      tCapwapFsCapwapDnsProfileEntry *
                                      pCapwapSetFsCapwapDnsProfileEntry,
                                      tCapwapIsSetFsCapwapDnsProfileEntry *
                                      pCapwapIsSetFsCapwapDnsProfileEntry,
                                      INT4 i4RowStatusLogic,
                                      INT4 i4RowCreateOption)
{
    if (pCapwapIsSetFsCapwapDnsProfileEntry->bFsCapwapDnsServerIp != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pCapwapIsSetFsCapwapDnsProfileEntry->bFsCapwapDnsDomainName !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pCapwapIsSetFsCapwapDnsProfileEntry->bFsCapwapDnsProfileRowStatus !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pCapwapSetFsCapwapDnsProfileEntry);
    UNUSED_PARAM (i4RowStatusLogic);
    UNUSED_PARAM (i4RowCreateOption);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapTestAllFsWtpNativeVlanIdTable
 Input       :  pu4ErrorCode
                pCapwapSetFsWtpNativeVlanIdTable
                pCapwapIsSetFsWtpNativeVlanIdTable
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
CapwapTestAllFsWtpNativeVlanIdTable (UINT4 *pu4ErrorCode,
                                     tCapwapFsWtpNativeVlanIdTable *
                                     pCapwapSetFsWtpNativeVlanIdTable,
                                     tCapwapIsSetFsWtpNativeVlanIdTable *
                                     pCapwapIsSetFsWtpNativeVlanIdTable,
                                     INT4 i4RowStatusLogic,
                                     INT4 i4RowCreateOption)
{
    UNUSED_PARAM (i4RowStatusLogic);
    UNUSED_PARAM (i4RowCreateOption);
    if (pCapwapIsSetFsWtpNativeVlanIdTable->bFsWtpNativeVlanId != OSIX_FALSE)
    {
        if ((pCapwapSetFsWtpNativeVlanIdTable->MibObject.i4FsWtpNativeVlanId <
             0)
            || (pCapwapSetFsWtpNativeVlanIdTable->MibObject.
                i4FsWtpNativeVlanId > 4094))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pCapwapIsSetFsWtpNativeVlanIdTable->bFsWtpNativeVlanIdRowStatus !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapTestAllFsWtpLocalRoutingTable
 Input       :  pu4ErrorCode
                pCapwapSetFsWtpLocalRoutingTable
                pCapwapIsSetFsWtpLocalRoutingTable
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
CapwapTestAllFsWtpLocalRoutingTable (UINT4 *pu4ErrorCode,
                                     tCapwapFsWtpLocalRoutingTable *
                                     pCapwapSetFsWtpLocalRoutingTable,
                                     tCapwapIsSetFsWtpLocalRoutingTable *
                                     pCapwapIsSetFsWtpLocalRoutingTable,
                                     INT4 i4RowStatusLogic,
                                     INT4 i4RowCreateOption)
{
    UNUSED_PARAM (i4RowStatusLogic);
    UNUSED_PARAM (i4RowCreateOption);
    if (pCapwapIsSetFsWtpLocalRoutingTable->bFsWtpLocalRouting != OSIX_FALSE)
    {
        if ((pCapwapSetFsWtpLocalRoutingTable->MibObject.i4FsWtpLocalRouting !=
             CLI_FSCAPWAP_ENABLE)
            && (pCapwapSetFsWtpLocalRoutingTable->MibObject.
                i4FsWtpLocalRouting != CLI_FSCAPWAP_DISABLE)
            && (pCapwapSetFsWtpLocalRoutingTable->MibObject.
                i4FsWtpLocalRouting != CLI_FSCAPWAP_BRIDGING_ENABLE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pCapwapIsSetFsWtpLocalRoutingTable->bFsWtpLocalRoutingRowStatus !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapTestAllFsCawapDiscStatsTable
 Input       :  pu4ErrorCode
                pCapwapSetFsCawapDiscStatsEntry
                pCapwapIsSetFsCawapDiscStatsEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
CapwapTestAllFsCawapDiscStatsTable (UINT4 *pu4ErrorCode,
                                    tCapwapFsCawapDiscStatsEntry *
                                    pCapwapSetFsCawapDiscStatsEntry,
                                    tCapwapIsSetFsCawapDiscStatsEntry *
                                    pCapwapIsSetFsCawapDiscStatsEntry,
                                    INT4 i4RowStatusLogic,
                                    INT4 i4RowCreateOption)
{
    if (pCapwapIsSetFsCawapDiscStatsEntry->bFsCapwapDiscStatsRowStatus !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pCapwapSetFsCawapDiscStatsEntry);
    UNUSED_PARAM (i4RowStatusLogic);
    UNUSED_PARAM (i4RowCreateOption);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapTestAllFsCawapJoinStatsTable
 Input       :  pu4ErrorCode
                pCapwapSetFsCawapJoinStatsEntry
                pCapwapIsSetFsCawapJoinStatsEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
CapwapTestAllFsCawapJoinStatsTable (UINT4 *pu4ErrorCode,
                                    tCapwapFsCawapJoinStatsEntry *
                                    pCapwapSetFsCawapJoinStatsEntry,
                                    tCapwapIsSetFsCawapJoinStatsEntry *
                                    pCapwapIsSetFsCawapJoinStatsEntry,
                                    INT4 i4RowStatusLogic,
                                    INT4 i4RowCreateOption)
{
    if (pCapwapIsSetFsCawapJoinStatsEntry->bFsCapwapJoinStatsRowStatus !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pCapwapSetFsCawapJoinStatsEntry);
    UNUSED_PARAM (i4RowStatusLogic);
    UNUSED_PARAM (i4RowCreateOption);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapTestAllFsCawapConfigStatsTable
 Input       :  pu4ErrorCode
                pCapwapSetFsCawapConfigStatsEntry
                pCapwapIsSetFsCawapConfigStatsEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
CapwapTestAllFsCawapConfigStatsTable (UINT4 *pu4ErrorCode,
                                      tCapwapFsCawapConfigStatsEntry *
                                      pCapwapSetFsCawapConfigStatsEntry,
                                      tCapwapIsSetFsCawapConfigStatsEntry *
                                      pCapwapIsSetFsCawapConfigStatsEntry,
                                      INT4 i4RowStatusLogic,
                                      INT4 i4RowCreateOption)
{
    if (pCapwapIsSetFsCawapConfigStatsEntry->bFsCapwapConfigStatsRowStatus !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pCapwapSetFsCawapConfigStatsEntry);
    UNUSED_PARAM (i4RowStatusLogic);
    UNUSED_PARAM (i4RowCreateOption);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapTestAllFsCawapRunStatsTable
 Input       :  pu4ErrorCode
                pCapwapSetFsCawapRunStatsEntry
                pCapwapIsSetFsCawapRunStatsEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
CapwapTestAllFsCawapRunStatsTable (UINT4 *pu4ErrorCode,
                                   tCapwapFsCawapRunStatsEntry *
                                   pCapwapSetFsCawapRunStatsEntry,
                                   tCapwapIsSetFsCawapRunStatsEntry *
                                   pCapwapIsSetFsCawapRunStatsEntry,
                                   INT4 i4RowStatusLogic,
                                   INT4 i4RowCreateOption)
{
    if (pCapwapIsSetFsCawapRunStatsEntry->bFsCapwapRunStatsRowStatus !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pCapwapSetFsCawapRunStatsEntry);
    UNUSED_PARAM (i4RowStatusLogic);
    UNUSED_PARAM (i4RowCreateOption);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapTestAllFsCapwapWirelessBindingTable
 Input       :  pu4ErrorCode
                pCapwapSetFsCapwapWirelessBindingEntry
                pCapwapIsSetFsCapwapWirelessBindingEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
CapwapTestAllFsCapwapWirelessBindingTable (UINT4 *pu4ErrorCode,
                                           tCapwapFsCapwapWirelessBindingEntry *
                                           pCapwapSetFsCapwapWirelessBindingEntry,
                                           tCapwapIsSetFsCapwapWirelessBindingEntry
                                           *
                                           pCapwapIsSetFsCapwapWirelessBindingEntry,
                                           INT4 i4RowStatusLogic,
                                           INT4 i4RowCreateOption)
{
    if (pCapwapIsSetFsCapwapWirelessBindingEntry->
        bFsCapwapWirelessBindingVirtualRadioIfIndex != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pCapwapIsSetFsCapwapWirelessBindingEntry->
        bFsCapwapWirelessBindingType != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pCapwapIsSetFsCapwapWirelessBindingEntry->
        bFsCapwapWirelessBindingRowStatus != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pCapwapSetFsCapwapWirelessBindingEntry);
    UNUSED_PARAM (i4RowStatusLogic);
    UNUSED_PARAM (i4RowCreateOption);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapTestAllFsCapwapStationWhiteList
 Input       :  pu4ErrorCode
                pCapwapSetFsCapwapStationWhiteListEntry
                pCapwapIsSetFsCapwapStationWhiteListEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
CapwapTestAllFsCapwapStationWhiteList (UINT4 *pu4ErrorCode,
                                       tCapwapFsCapwapStationWhiteListEntry *
                                       pCapwapSetFsCapwapStationWhiteListEntry,
                                       tCapwapIsSetFsCapwapStationWhiteListEntry
                                       *
                                       pCapwapIsSetFsCapwapStationWhiteListEntry,
                                       INT4 i4RowStatusLogic,
                                       INT4 i4RowCreateOption)
{
    if (pCapwapIsSetFsCapwapStationWhiteListEntry->
        bFsCapwapStationWhiteListStationId != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pCapwapIsSetFsCapwapStationWhiteListEntry->
        bFsCapwapStationWhiteListRowStatus != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pCapwapSetFsCapwapStationWhiteListEntry);
    UNUSED_PARAM (i4RowStatusLogic);
    UNUSED_PARAM (i4RowCreateOption);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapTestAllFsCapwapWtpRebootStatisticsTable
 Input       :  pu4ErrorCode
                pCapwapSetFsCapwapWtpRebootStatisticsEntry
                pCapwapIsSetFsCapwapWtpRebootStatisticsEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
CapwapTestAllFsCapwapWtpRebootStatisticsTable (UINT4 *pu4ErrorCode,
                                               tCapwapFsCapwapWtpRebootStatisticsEntry
                                               *
                                               pCapwapSetFsCapwapWtpRebootStatisticsEntry,
                                               tCapwapIsSetFsCapwapWtpRebootStatisticsEntry
                                               *
                                               pCapwapIsSetFsCapwapWtpRebootStatisticsEntry,
                                               INT4 i4RowStatusLogic,
                                               INT4 i4RowCreateOption)
{
    if (pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
        bFsCapwapWtpRebootStatisticsRebootCount != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
        bFsCapwapWtpRebootStatisticsAcInitiatedCount != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
        bFsCapwapWtpRebootStatisticsLinkFailureCount != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
        bFsCapwapWtpRebootStatisticsSwFailureCount != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
        bFsCapwapWtpRebootStatisticsHwFailureCount != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
        bFsCapwapWtpRebootStatisticsOtherFailureCount != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
        bFsCapwapWtpRebootStatisticsUnknownFailureCount != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
        bFsCapwapWtpRebootStatisticsLastFailureType != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->
        bFsCapwapWtpRebootStatisticsRowStatus != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pCapwapSetFsCapwapWtpRebootStatisticsEntry);
    UNUSED_PARAM (i4RowStatusLogic);
    UNUSED_PARAM (i4RowCreateOption);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapTestAllFsCapwapWtpRadioStatisticsTable
 Input       :  pu4ErrorCode
                pCapwapSetFsCapwapWtpRadioStatisticsEntry
                pCapwapIsSetFsCapwapWtpRadioStatisticsEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
CapwapTestAllFsCapwapWtpRadioStatisticsTable (UINT4 *pu4ErrorCode,
                                              tCapwapFsCapwapWtpRadioStatisticsEntry
                                              *
                                              pCapwapSetFsCapwapWtpRadioStatisticsEntry,
                                              tCapwapIsSetFsCapwapWtpRadioStatisticsEntry
                                              *
                                              pCapwapIsSetFsCapwapWtpRadioStatisticsEntry)
{
    if (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
        bFsCapwapWtpRadioLastFailType != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
        bFsCapwapWtpRadioResetCount != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
        bFsCapwapWtpRadioSwFailureCount != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
        bFsCapwapWtpRadioHwFailureCount != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
        bFsCapwapWtpRadioOtherFailureCount != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
        bFsCapwapWtpRadioUnknownFailureCount != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
        bFsCapwapWtpRadioConfigUpdateCount != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
        bFsCapwapWtpRadioChannelChangeCount != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
        bFsCapwapWtpRadioBandChangeCount != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
        bFsCapwapWtpRadioCurrentNoiseFloor != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->
        bFsCapwapWtpRadioStatRowStatus != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pCapwapSetFsCapwapWtpRadioStatisticsEntry);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapTestFsCapwapEnable
 Input       :  pu4ErrorCode
                i4FsCapwapEnable
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapTestFsCapwapEnable (UINT4 *pu4ErrorCode, INT4 i4FsCapwapEnable)
{

    if ((i4FsCapwapEnable != CLI_FSCAPWAP_ENABLE)
        && (i4FsCapwapEnable != CLI_FSCAPWAP_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapTestFsCapwapShutdown
 Input       :  pu4ErrorCode
                i4FsCapwapShutdown
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapTestFsCapwapShutdown (UINT4 *pu4ErrorCode, INT4 i4FsCapwapShutdown)
{

    if ((i4FsCapwapShutdown != CLI_CAPWAP_NO_SHUTDOWN)
        && (i4FsCapwapShutdown != CLI_CAPWAP_SHUTDOWN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapTestFsCapwapControlUdpPort
 Input       :  pu4ErrorCode
                u4FsCapwapControlUdpPort
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapTestFsCapwapControlUdpPort (UINT4 *pu4ErrorCode,
                                  UINT4 u4FsCapwapControlUdpPort)
{
/* Coverity Fix Start 9-7-13 */
    if (u4FsCapwapControlUdpPort > 65535)
/* Coverity Fix End 9-7-13  */
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapTestFsCapwapControlChannelDTLSPolicyOptions
 Input       :  pu4ErrorCode
                pFsCapwapControlChannelDTLSPolicyOptions
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapTestFsCapwapControlChannelDTLSPolicyOptions (UINT4 *pu4ErrorCode,
                                                   UINT1
                                                   *pFsCapwapControlChannelDTLSPolicyOptions)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pFsCapwapControlChannelDTLSPolicyOptions);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapTestFsCapwapDataChannelDTLSPolicyOptions
 Input       :  pu4ErrorCode
                pFsCapwapDataChannelDTLSPolicyOptions
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapTestFsCapwapDataChannelDTLSPolicyOptions (UINT4 *pu4ErrorCode,
                                                UINT1
                                                *pFsCapwapDataChannelDTLSPolicyOptions)
{
    if (*pFsCapwapDataChannelDTLSPolicyOptions
        == CLI_FSCAPWAP_ENCRYPTION_ENABLE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapTestFsWlcDiscoveryMode
 Input       :  pu4ErrorCode
                pFsWlcDiscoveryMode
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapTestFsWlcDiscoveryMode (UINT4 *pu4ErrorCode, UINT1 *pFsWlcDiscoveryMode)
{
    if ((*pFsWlcDiscoveryMode != CLI_CAPWAP_DISC_MODE_MAC_WHITELIST) &&
        (*pFsWlcDiscoveryMode != CLI_CAPWAP_DISC_MODE_MAC_BLACKLIST) &&
        (*pFsWlcDiscoveryMode != CLI_CAPWAP_DISC_MODE_MAC) &&
        (*pFsWlcDiscoveryMode != CLI_CAPWAP_DISC_MODE_MAC_AUTO))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapTestFsCapwapWtpModeIgnore
 Input       :  pu4ErrorCode
                i4FsCapwapWtpModeIgnore
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapTestFsCapwapWtpModeIgnore (UINT4 *pu4ErrorCode,
                                 INT4 i4FsCapwapWtpModeIgnore)
{

    if ((i4FsCapwapWtpModeIgnore != CLI_FSCAPWAP_MODEL_ENABLE) &&
        (i4FsCapwapWtpModeIgnore != CLI_FSCAPWAP_MODEL_DISABLE))
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapTestFsCapwapDebugMask
 Input       :  pu4ErrorCode
                i4FsCapwapDebugMask
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapTestFsCapwapDebugMask (UINT4 *pu4ErrorCode, INT4 i4FsCapwapDebugMask)
{

    INT4                i4TmpFsCapwapDebugMask = 0;

    if (i4FsCapwapDebugMask != CAPWAP_ALL_TRC)
    {
        i4TmpFsCapwapDebugMask =
            CAPWAP_MGMT_TRC | CAPWAP_INIT_TRC | CAPWAP_ENTRY_TRC |
            CAPWAP_EXIT_TRC | CAPWAP_FAILURE_TRC | CAPWAP_BUF_TRC |
            CAPWAP_SESS_TRC | CAPWAP_PKTDUMP_TRC | CAPWAP_STATS_TRC |
            CAPWAP_FSM_TRC | CAPWAP_MAIN_TRC | CAPWAP_UTIL_TRC |
            CAPWAP_STATION_TRC | CAPWAP_RADIO_PARM_TRC;
        if ((i4TmpFsCapwapDebugMask & i4FsCapwapDebugMask) !=
            i4FsCapwapDebugMask)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapTestFsDtlsDebugMask
 Input       :  pu4ErrorCode
                i4FsDtlsDebugMask
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapTestFsDtlsDebugMask (UINT4 *pu4ErrorCode, INT4 i4FsDtlsDebugMask)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FsDtlsDebugMask);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapTestFsDtlsEncryption
 Input       :  pu4ErrorCode
                i4FsDtlsEncryption
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapTestFsDtlsEncryption (UINT4 *pu4ErrorCode, INT4 i4FsDtlsEncryption)
{

    if ((i4FsDtlsEncryption != CLI_CAPWAP_FSDTLSENCRPTION_PSK) &&
        (i4FsDtlsEncryption != CLI_CAPWAP_FSDTLSENCRPTION_CER))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapTestFsDtlsEncryptAlogritham
 Input       :  pu4ErrorCode
                i4FsDtlsEncryptAlogritham
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapTestFsDtlsEncryptAlogritham (UINT4 *pu4ErrorCode,
                                   INT4 i4FsDtlsEncryptAlogritham)
{

    if ((i4FsDtlsEncryptAlogritham !=
         CLI_CAPWAP_FSDTLSENCAUTHENCRYPTALGORITHM_AES)
        && (i4FsDtlsEncryptAlogritham !=
            CLI_CAPWAP_FSDTLSENCAUTHENCRYPTALGORITHM_DHE_AES)
        && (i4FsDtlsEncryptAlogritham !=
            CLI_CAPWAP_FSDTLSENCAUTHENCRYPTALGORITHM_AES256)
        && (i4FsDtlsEncryptAlogritham !=
            CLI_CAPWAP_FSDTLSENCAUTHENCRYPTALGORITHM_DHE_AES256))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapTestFsStationType
 Input       :  pu4ErrorCode
                i4FsStationType
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapTestFsStationType (UINT4 *pu4ErrorCode, INT4 i4FsStationType)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FsStationType);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapTestFsAcTimestampTrigger
 Input       :  pu4ErrorCode
                i4FsAcTimestampTrigger
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapTestFsAcTimestampTrigger (UINT4 *pu4ErrorCode,
                                INT4 i4FsAcTimestampTrigger)
{
    if ((i4FsAcTimestampTrigger != CLI_FSCAPWAP_ENABLE) &&
        (i4FsAcTimestampTrigger != CLI_FSCAPWAP_DISABLE))
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapTestFsCapwapFsAcTimestampTrigger
 Input       :  pu4ErrorCode
                i4FsAcTimestampTrigger
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapTestFsCapwapFsAcTimestampTrigger (UINT4 *pu4ErrorCode,
                                        UINT4 u4CapwapBaseWtpProfileId,
                                        INT4 i4FsAcTimestampTrigger)
{
    INT4                i4RowStatus = 0;
    if (nmhGetCapwapBaseWtpProfileRowStatus (u4CapwapBaseWtpProfileId,
                                             &i4RowStatus) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return OSIX_FAILURE;
    }

    if ((i4FsAcTimestampTrigger != CLI_FSCAPWAP_ENABLE) &&
        (i4FsAcTimestampTrigger != CLI_FSCAPWAP_DISABLE))
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapTestFsCapwapFragReassembleStatus
 Input       :  pu4ErrorCode
                i4FsAcTimestampTrigger
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
CapwapTestFsCapwapFragReassembleStatus (UINT4 *pu4ErrorCode,
                                        UINT4 u4CapwapBaseWtpProfileId,
                                        UINT1 u1FragReassembleStatus)
{
    *pu4ErrorCode = SNMP_ERR_NO_ACCESS;
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (u1FragReassembleStatus);

    return OSIX_FAILURE;
}
