/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * $Id: capwapclimacr.h,v 1.2 2017/05/23 14:16:48 siva Exp $
 * Description: This file contains definitions for capwap module.
 *******************************************************************/

#ifndef __CAPWAPMACR_H__
#define __CAPWAPMACR_H__

/* WLAN rowstatus*/
#define CAPWAP_RS_CREATE_AND_WAIT 5
#define CAPWAP_RS_NOT_READY 3
#define CAPWAP_RS_NOT_IN_SERVICE 2
#define CAPWAP_RS_DESTROY 6
#define CAPWAP_RS_ACTIVE 1
/* MAX RADIO TYPE */
#define CLI_RADIO_TYPEMAX 0x80000001
#define WTP_MODEL_COUNT 4
#define MAX_DATE_LEN 100
#define   CAP_SNTP_REF_BASE_YEAR 1900
#define   CAP_IS_LEAP(yr)      ((yr) % 4 == 0 && ((yr) % 100 != 0 || (yr) % 400 == 0))
#define   CAP_SECS_IN_YEAR(yr) (CAP_IS_LEAP((yr)) ? 31622400 : 31536000);
#define   CAP_SECS_IN_DAY      86400
#define   CAP_SECS_IN_HOUR     3600
#define   CAP_SECS_IN_MINUTE       60

#define CAPWAP_MAX_WTP_PROFILE    4097
#define CAPWAP_MAX_PROFILE_NAME   256

#endif
