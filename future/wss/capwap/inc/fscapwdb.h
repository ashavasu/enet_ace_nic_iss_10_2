/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fscapwdb.h,v 1.3.2.1 2018/03/21 12:57:24 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSCAPWDB_H
#define _FSCAPWDB_H

UINT1 FsWtpModelTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsWtpRadioTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsWtpExtModelTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsWtpModelWlanBindingTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsCapwapWhiteListTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsCapwapBlackListTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsCapwapWtpConfigTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsCapwapLinkEncryptionTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsCapwapDefaultWtpProfileTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsCapwapDnsProfileTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsWtpNativeVlanIdTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsCawapDiscStatsTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsCawapJoinStatsTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsCawapConfigStatsTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsCawapRunStatsTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsCapwapWirelessBindingTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsCapwapStationWhiteListTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsCapwapWtpRebootStatisticsTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsCapwapWtpRadioStatisticsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsCapwapDot11StatisticsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsWtpLocalRoutingTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsCapwapFsAcTimestampTriggerTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsCapwATPStatsTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsCapwapExtWtpConfigTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsCapwapStationBlackListTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM};

UINT4 fscapw [] ={1,3,6,1,4,1,29601,2,82};
tSNMP_OID_TYPE fscapwOID = {9, fscapw};


UINT4 FsCapwapModuleStatus [ ] ={1,3,6,1,4,1,29601,2,82,1,1};
UINT4 FsCapwapSystemControl [ ] ={1,3,6,1,4,1,29601,2,82,1,2};
UINT4 FsCapwapControlUdpPort [ ] ={1,3,6,1,4,1,29601,2,82,1,3};
UINT4 FsCapwapControlChannelDTLSPolicyOptions [ ] ={1,3,6,1,4,1,29601,2,82,1,4};
UINT4 FsCapwapDataChannelDTLSPolicyOptions [ ] ={1,3,6,1,4,1,29601,2,82,1,5};
UINT4 FsWlcDiscoveryMode [ ] ={1,3,6,1,4,1,29601,2,82,1,6};
UINT4 FsCapwapWtpModeIgnore [ ] ={1,3,6,1,4,1,29601,2,82,1,7};
UINT4 FsCapwapDebugMask [ ] ={1,3,6,1,4,1,29601,2,82,1,8};
UINT4 FsDtlsDebugMask [ ] ={1,3,6,1,4,1,29601,2,82,1,9};
UINT4 FsDtlsEncryption [ ] ={1,3,6,1,4,1,29601,2,82,1,10};
UINT4 FsDtlsEncryptAlgorithm [ ] ={1,3,6,1,4,1,29601,2,82,1,11};
UINT4 FsStationType [ ] ={1,3,6,1,4,1,29601,2,82,1,12};
UINT4 FsAcTimestampTrigger [ ] ={1,3,6,1,4,1,29601,2,82,1,13};
UINT4 FsApRunStateCount [ ] ={1,3,6,1,4,1,29601,2,82,1,14};
UINT4 FsApIdleStateCount [ ] ={1,3,6,1,4,1,29601,2,82,1,15};
UINT4 FsCapwapWssDataDebugMask [ ] ={1,3,6,1,4,1,29601,2,82,1,16};
UINT4 FsCapwapWtpModelNumber [ ] ={1,3,6,1,4,1,29601,2,82,2,1,1,1};
UINT4 FsNoOfRadio [ ] ={1,3,6,1,4,1,29601,2,82,2,1,1,2};
UINT4 FsCapwapWtpMacType [ ] ={1,3,6,1,4,1,29601,2,82,2,1,1,3};
UINT4 FsCapwapWtpTunnelMode [ ] ={1,3,6,1,4,1,29601,2,82,2,1,1,4};
UINT4 FsCapwapSwVersion [ ] ={1,3,6,1,4,1,29601,2,82,2,1,1,5};
UINT4 FsCapwapImageName [ ] ={1,3,6,1,4,1,29601,2,82,2,1,1,6};
UINT4 FsCapwapQosProfileName [ ] ={1,3,6,1,4,1,29601,2,82,2,1,1,7};
UINT4 FsMaxStations [ ] ={1,3,6,1,4,1,29601,2,82,2,1,1,8};
UINT4 FsWtpModelRowStatus [ ] ={1,3,6,1,4,1,29601,2,82,2,1,1,9};
UINT4 FsCapwapHwVersion [ ] ={1,3,6,1,4,1,29601,2,82,2,1,1,10};
UINT4 FsRadioNumber [ ] ={1,3,6,1,4,1,29601,2,82,2,2,1,1};
UINT4 FsWtpRadioType [ ] ={1,3,6,1,4,1,29601,2,82,2,2,1,2};
UINT4 FsRadioAdminStatus [ ] ={1,3,6,1,4,1,29601,2,82,2,2,1,3};
UINT4 FsWtpRadioRowStatus [ ] ={1,3,6,1,4,1,29601,2,82,2,2,1,4};
UINT4 FsWtpModelLocalRouting [ ] ={1,3,6,1,4,1,29601,2,82,2,3,1,1};
UINT4 FsWtpModelWlanBindingRowStatus [ ] ={1,3,6,1,4,1,29601,2,82,2,4,1,1};
UINT4 FsCapwapWhiteListId [ ] ={1,3,6,1,4,1,29601,2,82,3,1,1,1};
UINT4 FsCapwapWhiteListWtpBaseMac [ ] ={1,3,6,1,4,1,29601,2,82,3,1,1,2};
UINT4 FsCapwapWhiteListRowStatus [ ] ={1,3,6,1,4,1,29601,2,82,3,1,1,3};
UINT4 FsCapwapBlackListId [ ] ={1,3,6,1,4,1,29601,2,82,3,2,1,1};
UINT4 FsCapwapBlackListWtpBaseMac [ ] ={1,3,6,1,4,1,29601,2,82,3,2,1,2};
UINT4 FsCapwapBlackListRowStatus [ ] ={1,3,6,1,4,1,29601,2,82,3,2,1,3};
UINT4 FsCapwapWtpReset [ ] ={1,3,6,1,4,1,29601,2,82,3,3,1,1};
UINT4 FsCapwapClearConfig [ ] ={1,3,6,1,4,1,29601,2,82,3,3,1,2};
UINT4 FsWtpDiscoveryType [ ] ={1,3,6,1,4,1,29601,2,82,3,3,1,3};
UINT4 FsWtpCountryString [ ] ={1,3,6,1,4,1,29601,2,82,3,3,1,4};
UINT4 FsWtpCrashDumpFileName [ ] ={1,3,6,1,4,1,29601,2,82,3,3,1,5};
UINT4 FsWtpMemoryDumpFileName [ ] ={1,3,6,1,4,1,29601,2,82,3,3,1,6};
UINT4 FsWtpDeleteOperation [ ] ={1,3,6,1,4,1,29601,2,82,3,3,1,7};
UINT4 FsCapwapClearApStats [ ] ={1,3,6,1,4,1,29601,2,82,3,3,1,8};
UINT4 FsCapwapWtpConfigRowStatus [ ] ={1,3,6,1,4,1,29601,2,82,3,3,1,9};
UINT4 FsWlcIpAddressType [ ] ={1,3,6,1,4,1,29601,2,82,3,3,1,10};
UINT4 FsWlcIpAddress [ ] ={1,3,6,1,4,1,29601,2,82,3,3,1,11};
UINT4 FsCapwapEncryptChannel [ ] ={1,3,6,1,4,1,29601,2,82,3,4,1,1};
UINT4 FsCapwapEncryptChannelStatus [ ] ={1,3,6,1,4,1,29601,2,82,3,4,1,2};
UINT4 FsCapwapEncryptChannelRowStatus [ ] ={1,3,6,1,4,1,29601,2,82,3,4,1,3};
UINT4 FsCapwapEncryptChannelRxPackets [ ] ={1,3,6,1,4,1,29601,2,82,3,4,1,4};
UINT4 FsCapwapDefaultQosProfile [ ] ={1,3,6,1,4,1,29601,2,82,3,5,1,1};
UINT4 FsCapwapDefaultWtpProfileRowStatus [ ] ={1,3,6,1,4,1,29601,2,82,3,5,1,2};
UINT4 FsCapwapDnsAddressType [ ] ={1,3,6,1,4,1,29601,2,82,3,6,1,1};
UINT4 FsCapwapDnsServerIp [ ] ={1,3,6,1,4,1,29601,2,82,3,6,1,2};
UINT4 FsCapwapDnsDomainName [ ] ={1,3,6,1,4,1,29601,2,82,3,6,1,3};
UINT4 FsCapwapDnsProfileRowStatus [ ] ={1,3,6,1,4,1,29601,2,82,3,6,1,4};
UINT4 FsWtpNativeVlanId [ ] ={1,3,6,1,4,1,29601,2,82,3,7,1,1};
UINT4 FsWtpNativeVlanIdRowStatus [ ] ={1,3,6,1,4,1,29601,2,82,3,7,1,2};
UINT4 FsCapwapDiscReqReceived [ ] ={1,3,6,1,4,1,29601,2,82,3,8,1,1};
UINT4 FsCapwapDiscRspReceived [ ] ={1,3,6,1,4,1,29601,2,82,3,8,1,2};
UINT4 FsCapwapDiscReqTransmitted [ ] ={1,3,6,1,4,1,29601,2,82,3,8,1,3};
UINT4 FsCapwapDiscRspTransmitted [ ] ={1,3,6,1,4,1,29601,2,82,3,8,1,4};
UINT4 FsCapwapDiscunsuccessfulProcessed [ ] ={1,3,6,1,4,1,29601,2,82,3,8,1,5};
UINT4 FsCapwapDiscLastUnsuccAttemptReason [ ] ={1,3,6,1,4,1,29601,2,82,3,8,1,6};
UINT4 FsCapwapDiscLastSuccAttemptTime [ ] ={1,3,6,1,4,1,29601,2,82,3,8,1,7};
UINT4 FsCapwapDiscLastUnsuccessfulAttemptTime [ ] ={1,3,6,1,4,1,29601,2,82,3,8,1,8};
UINT4 FsCapwapDiscStatsRowStatus [ ] ={1,3,6,1,4,1,29601,2,82,3,8,1,9};
UINT4 FsCapwapJoinReqReceived [ ] ={1,3,6,1,4,1,29601,2,82,3,9,1,1};
UINT4 FsCapwapJoinRspReceived [ ] ={1,3,6,1,4,1,29601,2,82,3,9,1,2};
UINT4 FsCapwapJoinReqTransmitted [ ] ={1,3,6,1,4,1,29601,2,82,3,9,1,3};
UINT4 FsCapwapJoinRspTransmitted [ ] ={1,3,6,1,4,1,29601,2,82,3,9,1,4};
UINT4 FsCapwapJoinunsuccessfulProcessed [ ] ={1,3,6,1,4,1,29601,2,82,3,9,1,5};
UINT4 FsCapwapJoinReasonLastUnsuccAttempt [ ] ={1,3,6,1,4,1,29601,2,82,3,9,1,6};
UINT4 FsCapwapJoinLastSuccAttemptTime [ ] ={1,3,6,1,4,1,29601,2,82,3,9,1,7};
UINT4 FsCapwapJoinLastUnsuccAttemptTime [ ] ={1,3,6,1,4,1,29601,2,82,3,9,1,8};
UINT4 FsCapwapJoinStatsRowStatus [ ] ={1,3,6,1,4,1,29601,2,82,3,9,1,9};
UINT4 FsCapwapConfigReqReceived [ ] ={1,3,6,1,4,1,29601,2,82,3,10,1,1};
UINT4 FsCapwapConfigRspReceived [ ] ={1,3,6,1,4,1,29601,2,82,3,10,1,2};
UINT4 FsCapwapConfigReqTransmitted [ ] ={1,3,6,1,4,1,29601,2,82,3,10,1,3};
UINT4 FsCapwapConfigRspTransmitted [ ] ={1,3,6,1,4,1,29601,2,82,3,10,1,4};
UINT4 FsCapwapConfigunsuccessfulProcessed [ ] ={1,3,6,1,4,1,29601,2,82,3,10,1,5};
UINT4 FsCapwapConfigReasonLastUnsuccAttempt [ ] ={1,3,6,1,4,1,29601,2,82,3,10,1,6};
UINT4 FsCapwapConfigLastSuccAttemptTime [ ] ={1,3,6,1,4,1,29601,2,82,3,10,1,7};
UINT4 FsCapwapConfigLastUnsuccessfulAttemptTime [ ] ={1,3,6,1,4,1,29601,2,82,3,10,1,8};
UINT4 FsCapwapConfigStatsRowStatus [ ] ={1,3,6,1,4,1,29601,2,82,3,10,1,9};
UINT4 FsCapwapRunConfigUpdateReqReceived [ ] ={1,3,6,1,4,1,29601,2,82,3,11,1,1};
UINT4 FsCapwapRunConfigUpdateRspReceived [ ] ={1,3,6,1,4,1,29601,2,82,3,11,1,2};
UINT4 FsCapwapRunConfigUpdateReqTransmitted [ ] ={1,3,6,1,4,1,29601,2,82,3,11,1,3};
UINT4 FsCapwapRunConfigUpdateRspTransmitted [ ] ={1,3,6,1,4,1,29601,2,82,3,11,1,4};
UINT4 FsCapwapRunConfigUpdateunsuccessfulProcessed [ ] ={1,3,6,1,4,1,29601,2,82,3,11,1,5};
UINT4 FsCapwapRunConfigUpdateReasonLastUnsuccAttempt [ ] ={1,3,6,1,4,1,29601,2,82,3,11,1,6};
UINT4 FsCapwapRunConfigUpdateLastSuccAttemptTime [ ] ={1,3,6,1,4,1,29601,2,82,3,11,1,7};
UINT4 FsCapwapRunConfigUpdateLastUnsuccessfulAttemptTime [ ] ={1,3,6,1,4,1,29601,2,82,3,11,1,8};
UINT4 FsCapwapRunStationConfigReqReceived [ ] ={1,3,6,1,4,1,29601,2,82,3,11,1,9};
UINT4 FsCapwapRunStationConfigRspReceived [ ] ={1,3,6,1,4,1,29601,2,82,3,11,1,10};
UINT4 FsCapwapRunStationConfigReqTransmitted [ ] ={1,3,6,1,4,1,29601,2,82,3,11,1,11};
UINT4 FsCapwapRunStationConfigRspTransmitted [ ] ={1,3,6,1,4,1,29601,2,82,3,11,1,12};
UINT4 FsCapwapRunStationConfigunsuccessfulProcessed [ ] ={1,3,6,1,4,1,29601,2,82,3,11,1,13};
UINT4 FsCapwapRunStationConfigReasonLastUnsuccAttempt [ ] ={1,3,6,1,4,1,29601,2,82,3,11,1,14};
UINT4 FsCapwapRunStationConfigLastSuccAttemptTime [ ] ={1,3,6,1,4,1,29601,2,82,3,11,1,15};
UINT4 FsCapwapRunStationConfigLastUnsuccessfulAttemptTime [ ] ={1,3,6,1,4,1,29601,2,82,3,11,1,16};
UINT4 FsCapwapRunClearConfigReqReceived [ ] ={1,3,6,1,4,1,29601,2,82,3,11,1,17};
UINT4 FsCapwapRunClearConfigRspReceived [ ] ={1,3,6,1,4,1,29601,2,82,3,11,1,18};
UINT4 FsCapwapRunClearConfigReqTransmitted [ ] ={1,3,6,1,4,1,29601,2,82,3,11,1,19};
UINT4 FsCapwapRunClearConfigRspTransmitted [ ] ={1,3,6,1,4,1,29601,2,82,3,11,1,20};
UINT4 FsCapwapRunClearConfigunsuccessfulProcessed [ ] ={1,3,6,1,4,1,29601,2,82,3,11,1,21};
UINT4 FsCapwapRunClearConfigReasonLastUnsuccAttempt [ ] ={1,3,6,1,4,1,29601,2,82,3,11,1,22};
UINT4 FsCapwapRunClearConfigLastSuccAttemptTime [ ] ={1,3,6,1,4,1,29601,2,82,3,11,1,23};
UINT4 FsCapwapRunClearConfigLastUnsuccessfulAttemptTime [ ] ={1,3,6,1,4,1,29601,2,82,3,11,1,24};
UINT4 FsCapwapRunDataTransferReqReceived [ ] ={1,3,6,1,4,1,29601,2,82,3,11,1,25};
UINT4 FsCapwapRunDataTransferRspReceived [ ] ={1,3,6,1,4,1,29601,2,82,3,11,1,26};
UINT4 FsCapwapRunDataTransferReqTransmitted [ ] ={1,3,6,1,4,1,29601,2,82,3,11,1,27};
UINT4 FsCapwapRunDataTransferRspTransmitted [ ] ={1,3,6,1,4,1,29601,2,82,3,11,1,28};
UINT4 FsCapwapRunDataTransferunsuccessfulProcessed [ ] ={1,3,6,1,4,1,29601,2,82,3,11,1,29};
UINT4 FsCapwapRunDataTransferReasonLastUnsuccAttempt [ ] ={1,3,6,1,4,1,29601,2,82,3,11,1,30};
UINT4 FsCapwapRunDataTransferLastSuccAttemptTime [ ] ={1,3,6,1,4,1,29601,2,82,3,11,1,31};
UINT4 FsCapwapRunDataTransferLastUnsuccessfulAttemptTime [ ] ={1,3,6,1,4,1,29601,2,82,3,11,1,32};
UINT4 FsCapwapRunResetReqReceived [ ] ={1,3,6,1,4,1,29601,2,82,3,11,1,33};
UINT4 FsCapwapRunResetRspReceived [ ] ={1,3,6,1,4,1,29601,2,82,3,11,1,34};
UINT4 FsCapwapRunResetReqTransmitted [ ] ={1,3,6,1,4,1,29601,2,82,3,11,1,35};
UINT4 FsCapwapRunResetRspTransmitted [ ] ={1,3,6,1,4,1,29601,2,82,3,11,1,36};
UINT4 FsCapwapRunResetunsuccessfulProcessed [ ] ={1,3,6,1,4,1,29601,2,82,3,11,1,37};
UINT4 FsCapwapRunResetReasonLastUnsuccAttempt [ ] ={1,3,6,1,4,1,29601,2,82,3,11,1,38};
UINT4 FsCapwapRunResetLastSuccAttemptTime [ ] ={1,3,6,1,4,1,29601,2,82,3,11,1,39};
UINT4 FsCapwapRunResetLastUnsuccessfulAttemptTime [ ] ={1,3,6,1,4,1,29601,2,82,3,11,1,40};
UINT4 FsCapwapRunPriDiscReqReceived [ ] ={1,3,6,1,4,1,29601,2,82,3,11,1,41};
UINT4 FsCapwapRunPriDiscRspReceived [ ] ={1,3,6,1,4,1,29601,2,82,3,11,1,42};
UINT4 FsCapwapRunPriDiscReqTransmitted [ ] ={1,3,6,1,4,1,29601,2,82,3,11,1,43};
UINT4 FsCapwapRunPriDiscRspTransmitted [ ] ={1,3,6,1,4,1,29601,2,82,3,11,1,44};
UINT4 FsCapwapRunPriDiscunsuccessfulProcessed [ ] ={1,3,6,1,4,1,29601,2,82,3,11,1,45};
UINT4 FsCapwapRunPriDiscReasonLastUnsuccAttempt [ ] ={1,3,6,1,4,1,29601,2,82,3,11,1,46};
UINT4 FsCapwapRunPriDiscLastSuccAttemptTime [ ] ={1,3,6,1,4,1,29601,2,82,3,11,1,47};
UINT4 FsCapwapRunPriDiscLastUnsuccessfulAttemptTime [ ] ={1,3,6,1,4,1,29601,2,82,3,11,1,48};
UINT4 FsCapwapRunEchoReqReceived [ ] ={1,3,6,1,4,1,29601,2,82,3,11,1,49};
UINT4 FsCapwapRunEchoRspReceived [ ] ={1,3,6,1,4,1,29601,2,82,3,11,1,50};
UINT4 FsCapwapRunEchoReqTransmitted [ ] ={1,3,6,1,4,1,29601,2,82,3,11,1,51};
UINT4 FsCapwapRunEchoRspTransmitted [ ] ={1,3,6,1,4,1,29601,2,82,3,11,1,52};
UINT4 FsCapwapRunEchounsuccessfulProcessed [ ] ={1,3,6,1,4,1,29601,2,82,3,11,1,53};
UINT4 FsCapwapRunEchoReasonLastUnsuccAttempt [ ] ={1,3,6,1,4,1,29601,2,82,3,11,1,54};
UINT4 FsCapwapRunEchoLastSuccAttemptTime [ ] ={1,3,6,1,4,1,29601,2,82,3,11,1,55};
UINT4 FsCapwapRunEchoLastUnsuccessfulAttemptTime [ ] ={1,3,6,1,4,1,29601,2,82,3,11,1,56};
UINT4 FsCapwapRunStatsRowStatus [ ] ={1,3,6,1,4,1,29601,2,82,3,11,1,57};
UINT4 FsCapwapWirelessBindingVirtualRadioIfIndex [ ] ={1,3,6,1,4,1,29601,2,82,3,12,1,1};
UINT4 FsCapwapWirelessBindingType [ ] ={1,3,6,1,4,1,29601,2,82,3,12,1,2};
UINT4 FsCapwapWirelessBindingRowStatus [ ] ={1,3,6,1,4,1,29601,2,82,3,12,1,3};
UINT4 FsCapwapStationWhiteListStationId [ ] ={1,3,6,1,4,1,29601,2,82,3,13,1,1};
UINT4 FsCapwapStationWhiteListRowStatus [ ] ={1,3,6,1,4,1,29601,2,82,3,13,1,2};
UINT4 FsCapwapWtpRebootStatisticsRebootCount [ ] ={1,3,6,1,4,1,29601,2,82,3,14,1,1};
UINT4 FsCapwapWtpRebootStatisticsAcInitiatedCount [ ] ={1,3,6,1,4,1,29601,2,82,3,14,1,2};
UINT4 FsCapwapWtpRebootStatisticsLinkFailureCount [ ] ={1,3,6,1,4,1,29601,2,82,3,14,1,3};
UINT4 FsCapwapWtpRebootStatisticsSwFailureCount [ ] ={1,3,6,1,4,1,29601,2,82,3,14,1,4};
UINT4 FsCapwapWtpRebootStatisticsHwFailureCount [ ] ={1,3,6,1,4,1,29601,2,82,3,14,1,5};
UINT4 FsCapwapWtpRebootStatisticsOtherFailureCount [ ] ={1,3,6,1,4,1,29601,2,82,3,14,1,6};
UINT4 FsCapwapWtpRebootStatisticsUnknownFailureCount [ ] ={1,3,6,1,4,1,29601,2,82,3,14,1,7};
UINT4 FsCapwapWtpRebootStatisticsLastFailureType [ ] ={1,3,6,1,4,1,29601,2,82,3,14,1,8};
UINT4 FsCapwapWtpRebootStatisticsRowStatus [ ] ={1,3,6,1,4,1,29601,2,82,3,14,1,9};
UINT4 FsCapwapWtpRadioLastFailType [ ] ={1,3,6,1,4,1,29601,2,82,3,15,1,1};
UINT4 FsCapwapWtpRadioResetCount [ ] ={1,3,6,1,4,1,29601,2,82,3,15,1,2};
UINT4 FsCapwapWtpRadioSwFailureCount [ ] ={1,3,6,1,4,1,29601,2,82,3,15,1,3};
UINT4 FsCapwapWtpRadioHwFailureCount [ ] ={1,3,6,1,4,1,29601,2,82,3,15,1,4};
UINT4 FsCapwapWtpRadioOtherFailureCount [ ] ={1,3,6,1,4,1,29601,2,82,3,15,1,5};
UINT4 FsCapwapWtpRadioUnknownFailureCount [ ] ={1,3,6,1,4,1,29601,2,82,3,15,1,6};
UINT4 FsCapwapWtpRadioConfigUpdateCount [ ] ={1,3,6,1,4,1,29601,2,82,3,15,1,7};
UINT4 FsCapwapWtpRadioChannelChangeCount [ ] ={1,3,6,1,4,1,29601,2,82,3,15,1,8};
UINT4 FsCapwapWtpRadioBandChangeCount [ ] ={1,3,6,1,4,1,29601,2,82,3,15,1,9};
UINT4 FsCapwapWtpRadioCurrentNoiseFloor [ ] ={1,3,6,1,4,1,29601,2,82,3,15,1,10};
UINT4 FsCapwapWtpRadioStatRowStatus [ ] ={1,3,6,1,4,1,29601,2,82,3,15,1,11};
UINT4 FsCapwapDot11TxFragmentCnt [ ] ={1,3,6,1,4,1,29601,2,82,3,16,1,1};
UINT4 FsCapwapDot11MulticastTxCnt [ ] ={1,3,6,1,4,1,29601,2,82,3,16,1,2};
UINT4 FsCapwapDot11FailedCount [ ] ={1,3,6,1,4,1,29601,2,82,3,16,1,3};
UINT4 FsCapwapDot11RetryCount [ ] ={1,3,6,1,4,1,29601,2,82,3,16,1,4};
UINT4 FsCapwapDot11MultipleRetryCount [ ] ={1,3,6,1,4,1,29601,2,82,3,16,1,5};
UINT4 FsCapwapDot11FrameDupCount [ ] ={1,3,6,1,4,1,29601,2,82,3,16,1,6};
UINT4 FsCapwapDot11RTSSuccessCount [ ] ={1,3,6,1,4,1,29601,2,82,3,16,1,7};
UINT4 FsCapwapDot11RTSFailCount [ ] ={1,3,6,1,4,1,29601,2,82,3,16,1,8};
UINT4 FsCapwapDot11ACKFailCount [ ] ={1,3,6,1,4,1,29601,2,82,3,16,1,9};
UINT4 FsCapwapDot11RxFragmentCount [ ] ={1,3,6,1,4,1,29601,2,82,3,16,1,10};
UINT4 FsCapwapDot11MulticastRxCount [ ] ={1,3,6,1,4,1,29601,2,82,3,16,1,11};
UINT4 FsCapwapDot11FCSErrCount [ ] ={1,3,6,1,4,1,29601,2,82,3,16,1,12};
UINT4 FsCapwapDot11TxFrameCount [ ] ={1,3,6,1,4,1,29601,2,82,3,16,1,13};
UINT4 FsCapwapDot11DecryptionErr [ ] ={1,3,6,1,4,1,29601,2,82,3,16,1,14};
UINT4 FsCapwapDot11DiscardQosFragmentCnt [ ] ={1,3,6,1,4,1,29601,2,82,3,16,1,15};
UINT4 FsCapwapDot11AssociatedStaCount [ ] ={1,3,6,1,4,1,29601,2,82,3,16,1,16};
UINT4 FsCapwapDot11QosCFPollsRecvdCount [ ] ={1,3,6,1,4,1,29601,2,82,3,16,1,17};
UINT4 FsCapwapDot11QosCFPollsUnusedCount [ ] ={1,3,6,1,4,1,29601,2,82,3,16,1,18};
UINT4 FsCapwapDot11QosCFPollsUnusableCount [ ] ={1,3,6,1,4,1,29601,2,82,3,16,1,19};
UINT4 FsCapwapDot11RadioId [ ] ={1,3,6,1,4,1,29601,2,82,3,16,1,20};
UINT4 FsWtpLocalRouting [ ] ={1,3,6,1,4,1,29601,2,82,3,17,1,1};
UINT4 FsCapwapFsAcTimestampTrigger [ ] ={1,3,6,1,4,1,29601,2,82,3,18,1,1};
UINT4 FsCapwATPStatsSentBytes [ ] ={1,3,6,1,4,1,29601,2,82,3,19,1,1};
UINT4 FsCapwATPStatsRcvdBytes [ ] ={1,3,6,1,4,1,29601,2,82,3,19,1,2};
UINT4 FsCapwATPStatsWiredSentBytes [ ] ={1,3,6,1,4,1,29601,2,82,3,19,1,3};
UINT4 FsCapwATPStatsWirelessSentBytes [ ] ={1,3,6,1,4,1,29601,2,82,3,19,1,4};
UINT4 FsCapwATPStatsWiredRcvdBytes [ ] ={1,3,6,1,4,1,29601,2,82,3,19,1,5};
UINT4 FsCapwATPStatsWirelessRcvdBytes [ ] ={1,3,6,1,4,1,29601,2,82,3,19,1,6};
UINT4 FsCapwATPStatsSentTrafficRate [ ] ={1,3,6,1,4,1,29601,2,82,3,19,1,7};
UINT4 FsCapwATPStatsRcvdTrafficRate [ ] ={1,3,6,1,4,1,29601,2,82,3,19,1,8};
UINT4 FsCapwATPStatsTotalClient [ ] ={1,3,6,1,4,1,29601,2,82,3,19,1,9};
UINT4 FsCapwATPStatsMaxClientPerAPRadio [ ] ={1,3,6,1,4,1,29601,2,82,3,19,1,10};
UINT4 FsCapwATPStatsMaxClientPerSSID [ ] ={1,3,6,1,4,1,29601,2,82,3,19,1,11};
UINT4 FsCapwapFragReassembleStatus [ ] ={1,3,6,1,4,1,29601,2,82,3,20,1,1};
UINT4 FsCapwapApSerialNumber [ ] ={1,3,6,1,4,1,29601,2,82,3,20,1,2};
UINT4 FsCapwapWtpImageName [ ] ={1,3,6,1,4,1,29601,2,82,3,20,1,3};
UINT4 FsCapwapMaxRetransmitCount [ ] ={1,3,6,1,4,1,29601,2,82,3,21};
UINT4 FsCapwTrapStatus [ ] ={1,3,6,1,4,1,29601,2,82,3,22};
UINT4 FsCapwCriticalTrapStatus [ ] ={1,3,6,1,4,1,29601,2,82,3,23};
UINT4 FsCapwapStationBlackListStationId [ ] ={1,3,6,1,4,1,29601,2,82,3,24,1,1};
UINT4 FsCapwapStationBlackListRowStatus [ ] ={1,3,6,1,4,1,29601,2,82,3,24,1,2};
UINT4 FsCapwapWtpImageUpgradeStatus [ ] ={1,3,6,1,4,1,29601,2,82,4,1,1};
UINT4 FsCapwapWtpSyncStatus [ ] ={1,3,6,1,4,1,29601,2,82,4,1,2};




tMbDbEntry fscapwMibEntry[]= {

{{11,FsCapwapModuleStatus}, NULL, FsCapwapModuleStatusGet, FsCapwapModuleStatusSet, FsCapwapModuleStatusTest, FsCapwapModuleStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{11,FsCapwapSystemControl}, NULL, FsCapwapSystemControlGet, FsCapwapSystemControlSet, FsCapwapSystemControlTest, FsCapwapSystemControlDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{11,FsCapwapControlUdpPort}, NULL, FsCapwapControlUdpPortGet, FsCapwapControlUdpPortSet, FsCapwapControlUdpPortTest, FsCapwapControlUdpPortDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "5246"},

{{11,FsCapwapControlChannelDTLSPolicyOptions}, NULL, FsCapwapControlChannelDTLSPolicyOptionsGet, FsCapwapControlChannelDTLSPolicyOptionsSet, FsCapwapControlChannelDTLSPolicyOptionsTest, FsCapwapControlChannelDTLSPolicyOptionsDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{11,FsCapwapDataChannelDTLSPolicyOptions}, NULL, FsCapwapDataChannelDTLSPolicyOptionsGet, FsCapwapDataChannelDTLSPolicyOptionsSet, FsCapwapDataChannelDTLSPolicyOptionsTest, FsCapwapDataChannelDTLSPolicyOptionsDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{11,FsWlcDiscoveryMode}, NULL, FsWlcDiscoveryModeGet, FsWlcDiscoveryModeSet, FsWlcDiscoveryModeTest, FsWlcDiscoveryModeDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{11,FsCapwapWtpModeIgnore}, NULL, FsCapwapWtpModeIgnoreGet, FsCapwapWtpModeIgnoreSet, FsCapwapWtpModeIgnoreTest, FsCapwapWtpModeIgnoreDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{11,FsCapwapDebugMask}, NULL, FsCapwapDebugMaskGet, FsCapwapDebugMaskSet, FsCapwapDebugMaskTest, FsCapwapDebugMaskDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{11,FsDtlsDebugMask}, NULL, FsDtlsDebugMaskGet, FsDtlsDebugMaskSet, FsDtlsDebugMaskTest, FsDtlsDebugMaskDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{11,FsDtlsEncryption}, NULL, FsDtlsEncryptionGet, FsDtlsEncryptionSet, FsDtlsEncryptionTest, FsDtlsEncryptionDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{11,FsDtlsEncryptAlgorithm}, NULL, FsDtlsEncryptAlgorithmGet, FsDtlsEncryptAlgorithmSet, FsDtlsEncryptAlgorithmTest, FsDtlsEncryptAlgorithmDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{11,FsStationType}, NULL, FsStationTypeGet, FsStationTypeSet, FsStationTypeTest, FsStationTypeDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{11,FsAcTimestampTrigger}, NULL, FsAcTimestampTriggerGet, FsAcTimestampTriggerSet, FsAcTimestampTriggerTest, FsAcTimestampTriggerDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{11,FsApRunStateCount}, NULL, FsApRunStateCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsApIdleStateCount}, NULL, FsApIdleStateCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsCapwapWssDataDebugMask}, NULL, FsCapwapWssDataDebugMaskGet, FsCapwapWssDataDebugMaskSet, FsCapwapWssDataDebugMaskTest, FsCapwapWssDataDebugMaskDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{13,FsCapwapWtpModelNumber}, GetNextIndexFsWtpModelTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsWtpModelTableINDEX, 1, 0, 0, NULL},

{{13,FsNoOfRadio}, GetNextIndexFsWtpModelTable, FsNoOfRadioGet, FsNoOfRadioSet, FsNoOfRadioTest, FsWtpModelTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsWtpModelTableINDEX, 1, 0, 0, NULL},

{{13,FsCapwapWtpMacType}, GetNextIndexFsWtpModelTable, FsCapwapWtpMacTypeGet, FsCapwapWtpMacTypeSet, FsCapwapWtpMacTypeTest, FsWtpModelTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsWtpModelTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapWtpTunnelMode}, GetNextIndexFsWtpModelTable, FsCapwapWtpTunnelModeGet, FsCapwapWtpTunnelModeSet, FsCapwapWtpTunnelModeTest, FsWtpModelTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsWtpModelTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapSwVersion}, GetNextIndexFsWtpModelTable, FsCapwapSwVersionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsWtpModelTableINDEX, 1, 0, 0, NULL},

{{13,FsCapwapImageName}, GetNextIndexFsWtpModelTable, FsCapwapImageNameGet, FsCapwapImageNameSet, FsCapwapImageNameTest, FsWtpModelTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsWtpModelTableINDEX, 1, 0, 0, NULL},

{{13,FsCapwapQosProfileName}, GetNextIndexFsWtpModelTable, FsCapwapQosProfileNameGet, FsCapwapQosProfileNameSet, FsCapwapQosProfileNameTest, FsWtpModelTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsWtpModelTableINDEX, 1, 0, 0, NULL},

{{13,FsMaxStations}, GetNextIndexFsWtpModelTable, FsMaxStationsGet, FsMaxStationsSet, FsMaxStationsTest, FsWtpModelTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsWtpModelTableINDEX, 1, 0, 0, NULL},

{{13,FsWtpModelRowStatus}, GetNextIndexFsWtpModelTable, FsWtpModelRowStatusGet, FsWtpModelRowStatusSet, FsWtpModelRowStatusTest, FsWtpModelTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsWtpModelTableINDEX, 1, 0, 1, NULL},

{{13,FsCapwapHwVersion}, GetNextIndexFsWtpModelTable, FsCapwapHwVersionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsWtpModelTableINDEX, 1, 0, 0, NULL},

{{13,FsRadioNumber}, GetNextIndexFsWtpRadioTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsWtpRadioTableINDEX, 2, 0, 0, NULL},

{{13,FsWtpRadioType}, GetNextIndexFsWtpRadioTable, FsWtpRadioTypeGet, FsWtpRadioTypeSet, FsWtpRadioTypeTest, FsWtpRadioTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsWtpRadioTableINDEX, 2, 0, 0, "1"},

{{13,FsRadioAdminStatus}, GetNextIndexFsWtpRadioTable, FsRadioAdminStatusGet, FsRadioAdminStatusSet, FsRadioAdminStatusTest, FsWtpRadioTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsWtpRadioTableINDEX, 2, 0, 0, "1"},

{{13,FsWtpRadioRowStatus}, GetNextIndexFsWtpRadioTable, FsWtpRadioRowStatusGet, FsWtpRadioRowStatusSet, FsWtpRadioRowStatusTest, FsWtpRadioTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsWtpRadioTableINDEX, 2, 0, 1, NULL},

{{13,FsWtpModelLocalRouting}, GetNextIndexFsWtpExtModelTable, FsWtpModelLocalRoutingGet, FsWtpModelLocalRoutingSet, FsWtpModelLocalRoutingTest, FsWtpExtModelTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsWtpExtModelTableINDEX, 1, 0, 0, "2"},

{{13,FsWtpModelWlanBindingRowStatus}, GetNextIndexFsWtpModelWlanBindingTable, FsWtpModelWlanBindingRowStatusGet, FsWtpModelWlanBindingRowStatusSet, FsWtpModelWlanBindingRowStatusTest, FsWtpModelWlanBindingTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsWtpModelWlanBindingTableINDEX, 3, 0, 1, NULL},

{{13,FsCapwapWhiteListId}, GetNextIndexFsCapwapWhiteListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsCapwapWhiteListTableINDEX, 1, 0, 0, NULL},

{{13,FsCapwapWhiteListWtpBaseMac}, GetNextIndexFsCapwapWhiteListTable, FsCapwapWhiteListWtpBaseMacGet, FsCapwapWhiteListWtpBaseMacSet, FsCapwapWhiteListWtpBaseMacTest, FsCapwapWhiteListTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, FsCapwapWhiteListTableINDEX, 1, 0, 0, NULL},

{{13,FsCapwapWhiteListRowStatus}, GetNextIndexFsCapwapWhiteListTable, FsCapwapWhiteListRowStatusGet, FsCapwapWhiteListRowStatusSet, FsCapwapWhiteListRowStatusTest, FsCapwapWhiteListTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsCapwapWhiteListTableINDEX, 1, 0, 1, NULL},

{{13,FsCapwapBlackListId}, GetNextIndexFsCapwapBlackListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsCapwapBlackListTableINDEX, 1, 0, 0, NULL},

{{13,FsCapwapBlackListWtpBaseMac}, GetNextIndexFsCapwapBlackListTable, FsCapwapBlackListWtpBaseMacGet, FsCapwapBlackListWtpBaseMacSet, FsCapwapBlackListWtpBaseMacTest, FsCapwapBlackListTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, FsCapwapBlackListTableINDEX, 1, 0, 0, NULL},

{{13,FsCapwapBlackListRowStatus}, GetNextIndexFsCapwapBlackListTable, FsCapwapBlackListRowStatusGet, FsCapwapBlackListRowStatusSet, FsCapwapBlackListRowStatusTest, FsCapwapBlackListTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsCapwapBlackListTableINDEX, 1, 0, 1, NULL},

{{13,FsCapwapWtpReset}, GetNextIndexFsCapwapWtpConfigTable, FsCapwapWtpResetGet, FsCapwapWtpResetSet, FsCapwapWtpResetTest, FsCapwapWtpConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsCapwapWtpConfigTableINDEX, 1, 0, 0, "2"},

{{13,FsCapwapClearConfig}, GetNextIndexFsCapwapWtpConfigTable, FsCapwapClearConfigGet, FsCapwapClearConfigSet, FsCapwapClearConfigTest, FsCapwapWtpConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsCapwapWtpConfigTableINDEX, 1, 0, 0, "2"},

{{13,FsWtpDiscoveryType}, GetNextIndexFsCapwapWtpConfigTable, FsWtpDiscoveryTypeGet, FsWtpDiscoveryTypeSet, FsWtpDiscoveryTypeTest, FsCapwapWtpConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsCapwapWtpConfigTableINDEX, 1, 0, 0, "4"},

{{13,FsWtpCountryString}, GetNextIndexFsCapwapWtpConfigTable, FsWtpCountryStringGet, FsWtpCountryStringSet, FsWtpCountryStringTest, FsCapwapWtpConfigTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsCapwapWtpConfigTableINDEX, 1, 0, 0, NULL},

{{13,FsWtpCrashDumpFileName}, GetNextIndexFsCapwapWtpConfigTable, FsWtpCrashDumpFileNameGet, FsWtpCrashDumpFileNameSet, FsWtpCrashDumpFileNameTest, FsCapwapWtpConfigTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsCapwapWtpConfigTableINDEX, 1, 0, 0, NULL},

{{13,FsWtpMemoryDumpFileName}, GetNextIndexFsCapwapWtpConfigTable, FsWtpMemoryDumpFileNameGet, FsWtpMemoryDumpFileNameSet, FsWtpMemoryDumpFileNameTest, FsCapwapWtpConfigTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsCapwapWtpConfigTableINDEX, 1, 0, 0, NULL},

{{13,FsWtpDeleteOperation}, GetNextIndexFsCapwapWtpConfigTable, FsWtpDeleteOperationGet, FsWtpDeleteOperationSet, FsWtpDeleteOperationTest, FsCapwapWtpConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsCapwapWtpConfigTableINDEX, 1, 0, 0, "2"},

{{13,FsCapwapClearApStats}, GetNextIndexFsCapwapWtpConfigTable, FsCapwapClearApStatsGet, FsCapwapClearApStatsSet, FsCapwapClearApStatsTest, FsCapwapWtpConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsCapwapWtpConfigTableINDEX, 1, 0, 0, "2"},

{{13,FsCapwapWtpConfigRowStatus}, GetNextIndexFsCapwapWtpConfigTable, FsCapwapWtpConfigRowStatusGet, FsCapwapWtpConfigRowStatusSet, FsCapwapWtpConfigRowStatusTest, FsCapwapWtpConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsCapwapWtpConfigTableINDEX, 1, 0, 1, NULL},

{{13,FsWlcIpAddressType}, GetNextIndexFsCapwapWtpConfigTable, FsWlcIpAddressTypeGet, FsWlcIpAddressTypeSet, FsWlcIpAddressTypeTest, FsCapwapWtpConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsCapwapWtpConfigTableINDEX, 1, 0, 0, NULL},

{{13,FsWlcIpAddress}, GetNextIndexFsCapwapWtpConfigTable, FsWlcIpAddressGet, FsWlcIpAddressSet, FsWlcIpAddressTest, FsCapwapWtpConfigTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsCapwapWtpConfigTableINDEX, 1, 0, 0, NULL},

{{13,FsCapwapEncryptChannel}, GetNextIndexFsCapwapLinkEncryptionTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsCapwapLinkEncryptionTableINDEX, 2, 0, 0, NULL},

{{13,FsCapwapEncryptChannelStatus}, GetNextIndexFsCapwapLinkEncryptionTable, FsCapwapEncryptChannelStatusGet, FsCapwapEncryptChannelStatusSet, FsCapwapEncryptChannelStatusTest, FsCapwapLinkEncryptionTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsCapwapLinkEncryptionTableINDEX, 2, 0, 0, "2"},

{{13,FsCapwapEncryptChannelRowStatus}, GetNextIndexFsCapwapLinkEncryptionTable, FsCapwapEncryptChannelRowStatusGet, FsCapwapEncryptChannelRowStatusSet, FsCapwapEncryptChannelRowStatusTest, FsCapwapLinkEncryptionTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsCapwapLinkEncryptionTableINDEX, 2, 0, 1, NULL},

{{13,FsCapwapEncryptChannelRxPackets}, GetNextIndexFsCapwapLinkEncryptionTable, FsCapwapEncryptChannelRxPacketsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsCapwapLinkEncryptionTableINDEX, 2, 0, 0, "0"},

{{13,FsCapwapDefaultQosProfile}, GetNextIndexFsCapwapDefaultWtpProfileTable, FsCapwapDefaultQosProfileGet, FsCapwapDefaultQosProfileSet, FsCapwapDefaultQosProfileTest, FsCapwapDefaultWtpProfileTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsCapwapDefaultWtpProfileTableINDEX, 1, 0, 0, NULL},

{{13,FsCapwapDefaultWtpProfileRowStatus}, GetNextIndexFsCapwapDefaultWtpProfileTable, FsCapwapDefaultWtpProfileRowStatusGet, FsCapwapDefaultWtpProfileRowStatusSet, FsCapwapDefaultWtpProfileRowStatusTest, FsCapwapDefaultWtpProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsCapwapDefaultWtpProfileTableINDEX, 1, 0, 1, NULL},

{{13,FsCapwapDnsAddressType}, GetNextIndexFsCapwapDnsProfileTable, FsCapwapDnsAddressTypeGet, FsCapwapDnsAddressTypeSet, FsCapwapDnsAddressTypeTest, FsCapwapDnsProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsCapwapDnsProfileTableINDEX, 1, 0, 0, NULL},

{{13,FsCapwapDnsServerIp}, GetNextIndexFsCapwapDnsProfileTable, FsCapwapDnsServerIpGet, FsCapwapDnsServerIpSet, FsCapwapDnsServerIpTest, FsCapwapDnsProfileTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsCapwapDnsProfileTableINDEX, 1, 0, 0, NULL},

{{13,FsCapwapDnsDomainName}, GetNextIndexFsCapwapDnsProfileTable, FsCapwapDnsDomainNameGet, FsCapwapDnsDomainNameSet, FsCapwapDnsDomainNameTest, FsCapwapDnsProfileTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsCapwapDnsProfileTableINDEX, 1, 0, 0, NULL},

{{13,FsCapwapDnsProfileRowStatus}, GetNextIndexFsCapwapDnsProfileTable, FsCapwapDnsProfileRowStatusGet, FsCapwapDnsProfileRowStatusSet, FsCapwapDnsProfileRowStatusTest, FsCapwapDnsProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsCapwapDnsProfileTableINDEX, 1, 0, 1, NULL},

{{13,FsWtpNativeVlanId}, GetNextIndexFsWtpNativeVlanIdTable, FsWtpNativeVlanIdGet, FsWtpNativeVlanIdSet, FsWtpNativeVlanIdTest, FsWtpNativeVlanIdTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsWtpNativeVlanIdTableINDEX, 1, 0, 0, "0"},

{{13,FsWtpNativeVlanIdRowStatus}, GetNextIndexFsWtpNativeVlanIdTable, FsWtpNativeVlanIdRowStatusGet, FsWtpNativeVlanIdRowStatusSet, FsWtpNativeVlanIdRowStatusTest, FsWtpNativeVlanIdTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsWtpNativeVlanIdTableINDEX, 1, 0, 1, NULL},

{{13,FsCapwapDiscReqReceived}, GetNextIndexFsCawapDiscStatsTable, FsCapwapDiscReqReceivedGet, FsCapwapDiscReqReceivedSet, FsCapwapDiscReqReceivedTest, FsCawapDiscStatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCawapDiscStatsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapDiscRspReceived}, GetNextIndexFsCawapDiscStatsTable, FsCapwapDiscRspReceivedGet, FsCapwapDiscRspReceivedSet, FsCapwapDiscRspReceivedTest, FsCawapDiscStatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCawapDiscStatsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapDiscReqTransmitted}, GetNextIndexFsCawapDiscStatsTable, FsCapwapDiscReqTransmittedGet, FsCapwapDiscReqTransmittedSet, FsCapwapDiscReqTransmittedTest, FsCawapDiscStatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCawapDiscStatsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapDiscRspTransmitted}, GetNextIndexFsCawapDiscStatsTable, FsCapwapDiscRspTransmittedGet, FsCapwapDiscRspTransmittedSet, FsCapwapDiscRspTransmittedTest, FsCawapDiscStatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCawapDiscStatsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapDiscunsuccessfulProcessed}, GetNextIndexFsCawapDiscStatsTable, FsCapwapDiscunsuccessfulProcessedGet, FsCapwapDiscunsuccessfulProcessedSet, FsCapwapDiscunsuccessfulProcessedTest, FsCawapDiscStatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCawapDiscStatsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapDiscLastUnsuccAttemptReason}, GetNextIndexFsCawapDiscStatsTable, FsCapwapDiscLastUnsuccAttemptReasonGet, FsCapwapDiscLastUnsuccAttemptReasonSet, FsCapwapDiscLastUnsuccAttemptReasonTest, FsCawapDiscStatsTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsCawapDiscStatsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapDiscLastSuccAttemptTime}, GetNextIndexFsCawapDiscStatsTable, FsCapwapDiscLastSuccAttemptTimeGet, FsCapwapDiscLastSuccAttemptTimeSet, FsCapwapDiscLastSuccAttemptTimeTest, FsCawapDiscStatsTableDep, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READWRITE, FsCawapDiscStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsCapwapDiscLastUnsuccessfulAttemptTime}, GetNextIndexFsCawapDiscStatsTable, FsCapwapDiscLastUnsuccessfulAttemptTimeGet, FsCapwapDiscLastUnsuccessfulAttemptTimeSet, FsCapwapDiscLastUnsuccessfulAttemptTimeTest, FsCawapDiscStatsTableDep, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READWRITE, FsCawapDiscStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsCapwapDiscStatsRowStatus}, GetNextIndexFsCawapDiscStatsTable, FsCapwapDiscStatsRowStatusGet, FsCapwapDiscStatsRowStatusSet, FsCapwapDiscStatsRowStatusTest, FsCawapDiscStatsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsCawapDiscStatsTableINDEX, 1, 0, 1, NULL},

{{13,FsCapwapJoinReqReceived}, GetNextIndexFsCawapJoinStatsTable, FsCapwapJoinReqReceivedGet, FsCapwapJoinReqReceivedSet, FsCapwapJoinReqReceivedTest, FsCawapJoinStatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCawapJoinStatsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapJoinRspReceived}, GetNextIndexFsCawapJoinStatsTable, FsCapwapJoinRspReceivedGet, FsCapwapJoinRspReceivedSet, FsCapwapJoinRspReceivedTest, FsCawapJoinStatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCawapJoinStatsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapJoinReqTransmitted}, GetNextIndexFsCawapJoinStatsTable, FsCapwapJoinReqTransmittedGet, FsCapwapJoinReqTransmittedSet, FsCapwapJoinReqTransmittedTest, FsCawapJoinStatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCawapJoinStatsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapJoinRspTransmitted}, GetNextIndexFsCawapJoinStatsTable, FsCapwapJoinRspTransmittedGet, FsCapwapJoinRspTransmittedSet, FsCapwapJoinRspTransmittedTest, FsCawapJoinStatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCawapJoinStatsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapJoinunsuccessfulProcessed}, GetNextIndexFsCawapJoinStatsTable, FsCapwapJoinunsuccessfulProcessedGet, FsCapwapJoinunsuccessfulProcessedSet, FsCapwapJoinunsuccessfulProcessedTest, FsCawapJoinStatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCawapJoinStatsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapJoinReasonLastUnsuccAttempt}, GetNextIndexFsCawapJoinStatsTable, FsCapwapJoinReasonLastUnsuccAttemptGet, FsCapwapJoinReasonLastUnsuccAttemptSet, FsCapwapJoinReasonLastUnsuccAttemptTest, FsCawapJoinStatsTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsCawapJoinStatsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapJoinLastSuccAttemptTime}, GetNextIndexFsCawapJoinStatsTable, FsCapwapJoinLastSuccAttemptTimeGet, FsCapwapJoinLastSuccAttemptTimeSet, FsCapwapJoinLastSuccAttemptTimeTest, FsCawapJoinStatsTableDep, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READWRITE, FsCawapJoinStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsCapwapJoinLastUnsuccAttemptTime}, GetNextIndexFsCawapJoinStatsTable, FsCapwapJoinLastUnsuccAttemptTimeGet, FsCapwapJoinLastUnsuccAttemptTimeSet, FsCapwapJoinLastUnsuccAttemptTimeTest, FsCawapJoinStatsTableDep, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READWRITE, FsCawapJoinStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsCapwapJoinStatsRowStatus}, GetNextIndexFsCawapJoinStatsTable, FsCapwapJoinStatsRowStatusGet, FsCapwapJoinStatsRowStatusSet, FsCapwapJoinStatsRowStatusTest, FsCawapJoinStatsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsCawapJoinStatsTableINDEX, 1, 0, 1, NULL},

{{13,FsCapwapConfigReqReceived}, GetNextIndexFsCawapConfigStatsTable, FsCapwapConfigReqReceivedGet, FsCapwapConfigReqReceivedSet, FsCapwapConfigReqReceivedTest, FsCawapConfigStatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCawapConfigStatsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapConfigRspReceived}, GetNextIndexFsCawapConfigStatsTable, FsCapwapConfigRspReceivedGet, FsCapwapConfigRspReceivedSet, FsCapwapConfigRspReceivedTest, FsCawapConfigStatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCawapConfigStatsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapConfigReqTransmitted}, GetNextIndexFsCawapConfigStatsTable, FsCapwapConfigReqTransmittedGet, FsCapwapConfigReqTransmittedSet, FsCapwapConfigReqTransmittedTest, FsCawapConfigStatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCawapConfigStatsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapConfigRspTransmitted}, GetNextIndexFsCawapConfigStatsTable, FsCapwapConfigRspTransmittedGet, FsCapwapConfigRspTransmittedSet, FsCapwapConfigRspTransmittedTest, FsCawapConfigStatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCawapConfigStatsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapConfigunsuccessfulProcessed}, GetNextIndexFsCawapConfigStatsTable, FsCapwapConfigunsuccessfulProcessedGet, FsCapwapConfigunsuccessfulProcessedSet, FsCapwapConfigunsuccessfulProcessedTest, FsCawapConfigStatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCawapConfigStatsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapConfigReasonLastUnsuccAttempt}, GetNextIndexFsCawapConfigStatsTable, FsCapwapConfigReasonLastUnsuccAttemptGet, FsCapwapConfigReasonLastUnsuccAttemptSet, FsCapwapConfigReasonLastUnsuccAttemptTest, FsCawapConfigStatsTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsCawapConfigStatsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapConfigLastSuccAttemptTime}, GetNextIndexFsCawapConfigStatsTable, FsCapwapConfigLastSuccAttemptTimeGet, FsCapwapConfigLastSuccAttemptTimeSet, FsCapwapConfigLastSuccAttemptTimeTest, FsCawapConfigStatsTableDep, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READWRITE, FsCawapConfigStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsCapwapConfigLastUnsuccessfulAttemptTime}, GetNextIndexFsCawapConfigStatsTable, FsCapwapConfigLastUnsuccessfulAttemptTimeGet, FsCapwapConfigLastUnsuccessfulAttemptTimeSet, FsCapwapConfigLastUnsuccessfulAttemptTimeTest, FsCawapConfigStatsTableDep, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READWRITE, FsCawapConfigStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsCapwapConfigStatsRowStatus}, GetNextIndexFsCawapConfigStatsTable, FsCapwapConfigStatsRowStatusGet, FsCapwapConfigStatsRowStatusSet, FsCapwapConfigStatsRowStatusTest, FsCawapConfigStatsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsCawapConfigStatsTableINDEX, 1, 0, 1, NULL},

{{13,FsCapwapRunConfigUpdateReqReceived}, GetNextIndexFsCawapRunStatsTable, FsCapwapRunConfigUpdateReqReceivedGet, FsCapwapRunConfigUpdateReqReceivedSet, FsCapwapRunConfigUpdateReqReceivedTest, FsCawapRunStatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCawapRunStatsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapRunConfigUpdateRspReceived}, GetNextIndexFsCawapRunStatsTable, FsCapwapRunConfigUpdateRspReceivedGet, FsCapwapRunConfigUpdateRspReceivedSet, FsCapwapRunConfigUpdateRspReceivedTest, FsCawapRunStatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCawapRunStatsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapRunConfigUpdateReqTransmitted}, GetNextIndexFsCawapRunStatsTable, FsCapwapRunConfigUpdateReqTransmittedGet, FsCapwapRunConfigUpdateReqTransmittedSet, FsCapwapRunConfigUpdateReqTransmittedTest, FsCawapRunStatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCawapRunStatsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapRunConfigUpdateRspTransmitted}, GetNextIndexFsCawapRunStatsTable, FsCapwapRunConfigUpdateRspTransmittedGet, FsCapwapRunConfigUpdateRspTransmittedSet, FsCapwapRunConfigUpdateRspTransmittedTest, FsCawapRunStatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCawapRunStatsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapRunConfigUpdateunsuccessfulProcessed}, GetNextIndexFsCawapRunStatsTable, FsCapwapRunConfigUpdateunsuccessfulProcessedGet, FsCapwapRunConfigUpdateunsuccessfulProcessedSet, FsCapwapRunConfigUpdateunsuccessfulProcessedTest, FsCawapRunStatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCawapRunStatsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapRunConfigUpdateReasonLastUnsuccAttempt}, GetNextIndexFsCawapRunStatsTable, FsCapwapRunConfigUpdateReasonLastUnsuccAttemptGet, FsCapwapRunConfigUpdateReasonLastUnsuccAttemptSet, FsCapwapRunConfigUpdateReasonLastUnsuccAttemptTest, FsCawapRunStatsTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsCawapRunStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsCapwapRunConfigUpdateLastSuccAttemptTime}, GetNextIndexFsCawapRunStatsTable, FsCapwapRunConfigUpdateLastSuccAttemptTimeGet, FsCapwapRunConfigUpdateLastSuccAttemptTimeSet, FsCapwapRunConfigUpdateLastSuccAttemptTimeTest, FsCawapRunStatsTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsCawapRunStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsCapwapRunConfigUpdateLastUnsuccessfulAttemptTime}, GetNextIndexFsCawapRunStatsTable, FsCapwapRunConfigUpdateLastUnsuccessfulAttemptTimeGet, FsCapwapRunConfigUpdateLastUnsuccessfulAttemptTimeSet, FsCapwapRunConfigUpdateLastUnsuccessfulAttemptTimeTest, FsCawapRunStatsTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsCawapRunStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsCapwapRunStationConfigReqReceived}, GetNextIndexFsCawapRunStatsTable, FsCapwapRunStationConfigReqReceivedGet, FsCapwapRunStationConfigReqReceivedSet, FsCapwapRunStationConfigReqReceivedTest, FsCawapRunStatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCawapRunStatsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapRunStationConfigRspReceived}, GetNextIndexFsCawapRunStatsTable, FsCapwapRunStationConfigRspReceivedGet, FsCapwapRunStationConfigRspReceivedSet, FsCapwapRunStationConfigRspReceivedTest, FsCawapRunStatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCawapRunStatsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapRunStationConfigReqTransmitted}, GetNextIndexFsCawapRunStatsTable, FsCapwapRunStationConfigReqTransmittedGet, FsCapwapRunStationConfigReqTransmittedSet, FsCapwapRunStationConfigReqTransmittedTest, FsCawapRunStatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCawapRunStatsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapRunStationConfigRspTransmitted}, GetNextIndexFsCawapRunStatsTable, FsCapwapRunStationConfigRspTransmittedGet, FsCapwapRunStationConfigRspTransmittedSet, FsCapwapRunStationConfigRspTransmittedTest, FsCawapRunStatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCawapRunStatsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapRunStationConfigunsuccessfulProcessed}, GetNextIndexFsCawapRunStatsTable, FsCapwapRunStationConfigunsuccessfulProcessedGet, FsCapwapRunStationConfigunsuccessfulProcessedSet, FsCapwapRunStationConfigunsuccessfulProcessedTest, FsCawapRunStatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCawapRunStatsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapRunStationConfigReasonLastUnsuccAttempt}, GetNextIndexFsCawapRunStatsTable, FsCapwapRunStationConfigReasonLastUnsuccAttemptGet, FsCapwapRunStationConfigReasonLastUnsuccAttemptSet, FsCapwapRunStationConfigReasonLastUnsuccAttemptTest, FsCawapRunStatsTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsCawapRunStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsCapwapRunStationConfigLastSuccAttemptTime}, GetNextIndexFsCawapRunStatsTable, FsCapwapRunStationConfigLastSuccAttemptTimeGet, FsCapwapRunStationConfigLastSuccAttemptTimeSet, FsCapwapRunStationConfigLastSuccAttemptTimeTest, FsCawapRunStatsTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsCawapRunStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsCapwapRunStationConfigLastUnsuccessfulAttemptTime}, GetNextIndexFsCawapRunStatsTable, FsCapwapRunStationConfigLastUnsuccessfulAttemptTimeGet, FsCapwapRunStationConfigLastUnsuccessfulAttemptTimeSet, FsCapwapRunStationConfigLastUnsuccessfulAttemptTimeTest, FsCawapRunStatsTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsCawapRunStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsCapwapRunClearConfigReqReceived}, GetNextIndexFsCawapRunStatsTable, FsCapwapRunClearConfigReqReceivedGet, FsCapwapRunClearConfigReqReceivedSet, FsCapwapRunClearConfigReqReceivedTest, FsCawapRunStatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCawapRunStatsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapRunClearConfigRspReceived}, GetNextIndexFsCawapRunStatsTable, FsCapwapRunClearConfigRspReceivedGet, FsCapwapRunClearConfigRspReceivedSet, FsCapwapRunClearConfigRspReceivedTest, FsCawapRunStatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCawapRunStatsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapRunClearConfigReqTransmitted}, GetNextIndexFsCawapRunStatsTable, FsCapwapRunClearConfigReqTransmittedGet, FsCapwapRunClearConfigReqTransmittedSet, FsCapwapRunClearConfigReqTransmittedTest, FsCawapRunStatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCawapRunStatsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapRunClearConfigRspTransmitted}, GetNextIndexFsCawapRunStatsTable, FsCapwapRunClearConfigRspTransmittedGet, FsCapwapRunClearConfigRspTransmittedSet, FsCapwapRunClearConfigRspTransmittedTest, FsCawapRunStatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCawapRunStatsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapRunClearConfigunsuccessfulProcessed}, GetNextIndexFsCawapRunStatsTable, FsCapwapRunClearConfigunsuccessfulProcessedGet, FsCapwapRunClearConfigunsuccessfulProcessedSet, FsCapwapRunClearConfigunsuccessfulProcessedTest, FsCawapRunStatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCawapRunStatsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapRunClearConfigReasonLastUnsuccAttempt}, GetNextIndexFsCawapRunStatsTable, FsCapwapRunClearConfigReasonLastUnsuccAttemptGet, FsCapwapRunClearConfigReasonLastUnsuccAttemptSet, FsCapwapRunClearConfigReasonLastUnsuccAttemptTest, FsCawapRunStatsTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsCawapRunStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsCapwapRunClearConfigLastSuccAttemptTime}, GetNextIndexFsCawapRunStatsTable, FsCapwapRunClearConfigLastSuccAttemptTimeGet, FsCapwapRunClearConfigLastSuccAttemptTimeSet, FsCapwapRunClearConfigLastSuccAttemptTimeTest, FsCawapRunStatsTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsCawapRunStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsCapwapRunClearConfigLastUnsuccessfulAttemptTime}, GetNextIndexFsCawapRunStatsTable, FsCapwapRunClearConfigLastUnsuccessfulAttemptTimeGet, FsCapwapRunClearConfigLastUnsuccessfulAttemptTimeSet, FsCapwapRunClearConfigLastUnsuccessfulAttemptTimeTest, FsCawapRunStatsTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsCawapRunStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsCapwapRunDataTransferReqReceived}, GetNextIndexFsCawapRunStatsTable, FsCapwapRunDataTransferReqReceivedGet, FsCapwapRunDataTransferReqReceivedSet, FsCapwapRunDataTransferReqReceivedTest, FsCawapRunStatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCawapRunStatsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapRunDataTransferRspReceived}, GetNextIndexFsCawapRunStatsTable, FsCapwapRunDataTransferRspReceivedGet, FsCapwapRunDataTransferRspReceivedSet, FsCapwapRunDataTransferRspReceivedTest, FsCawapRunStatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCawapRunStatsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapRunDataTransferReqTransmitted}, GetNextIndexFsCawapRunStatsTable, FsCapwapRunDataTransferReqTransmittedGet, FsCapwapRunDataTransferReqTransmittedSet, FsCapwapRunDataTransferReqTransmittedTest, FsCawapRunStatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCawapRunStatsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapRunDataTransferRspTransmitted}, GetNextIndexFsCawapRunStatsTable, FsCapwapRunDataTransferRspTransmittedGet, FsCapwapRunDataTransferRspTransmittedSet, FsCapwapRunDataTransferRspTransmittedTest, FsCawapRunStatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCawapRunStatsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapRunDataTransferunsuccessfulProcessed}, GetNextIndexFsCawapRunStatsTable, FsCapwapRunDataTransferunsuccessfulProcessedGet, FsCapwapRunDataTransferunsuccessfulProcessedSet, FsCapwapRunDataTransferunsuccessfulProcessedTest, FsCawapRunStatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCawapRunStatsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapRunDataTransferReasonLastUnsuccAttempt}, GetNextIndexFsCawapRunStatsTable, FsCapwapRunDataTransferReasonLastUnsuccAttemptGet, FsCapwapRunDataTransferReasonLastUnsuccAttemptSet, FsCapwapRunDataTransferReasonLastUnsuccAttemptTest, FsCawapRunStatsTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsCawapRunStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsCapwapRunDataTransferLastSuccAttemptTime}, GetNextIndexFsCawapRunStatsTable, FsCapwapRunDataTransferLastSuccAttemptTimeGet, FsCapwapRunDataTransferLastSuccAttemptTimeSet, FsCapwapRunDataTransferLastSuccAttemptTimeTest, FsCawapRunStatsTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsCawapRunStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsCapwapRunDataTransferLastUnsuccessfulAttemptTime}, GetNextIndexFsCawapRunStatsTable, FsCapwapRunDataTransferLastUnsuccessfulAttemptTimeGet, FsCapwapRunDataTransferLastUnsuccessfulAttemptTimeSet, FsCapwapRunDataTransferLastUnsuccessfulAttemptTimeTest, FsCawapRunStatsTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsCawapRunStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsCapwapRunResetReqReceived}, GetNextIndexFsCawapRunStatsTable, FsCapwapRunResetReqReceivedGet, FsCapwapRunResetReqReceivedSet, FsCapwapRunResetReqReceivedTest, FsCawapRunStatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCawapRunStatsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapRunResetRspReceived}, GetNextIndexFsCawapRunStatsTable, FsCapwapRunResetRspReceivedGet, FsCapwapRunResetRspReceivedSet, FsCapwapRunResetRspReceivedTest, FsCawapRunStatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCawapRunStatsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapRunResetReqTransmitted}, GetNextIndexFsCawapRunStatsTable, FsCapwapRunResetReqTransmittedGet, FsCapwapRunResetReqTransmittedSet, FsCapwapRunResetReqTransmittedTest, FsCawapRunStatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCawapRunStatsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapRunResetRspTransmitted}, GetNextIndexFsCawapRunStatsTable, FsCapwapRunResetRspTransmittedGet, FsCapwapRunResetRspTransmittedSet, FsCapwapRunResetRspTransmittedTest, FsCawapRunStatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCawapRunStatsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapRunResetunsuccessfulProcessed}, GetNextIndexFsCawapRunStatsTable, FsCapwapRunResetunsuccessfulProcessedGet, FsCapwapRunResetunsuccessfulProcessedSet, FsCapwapRunResetunsuccessfulProcessedTest, FsCawapRunStatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCawapRunStatsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapRunResetReasonLastUnsuccAttempt}, GetNextIndexFsCawapRunStatsTable, FsCapwapRunResetReasonLastUnsuccAttemptGet, FsCapwapRunResetReasonLastUnsuccAttemptSet, FsCapwapRunResetReasonLastUnsuccAttemptTest, FsCawapRunStatsTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsCawapRunStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsCapwapRunResetLastSuccAttemptTime}, GetNextIndexFsCawapRunStatsTable, FsCapwapRunResetLastSuccAttemptTimeGet, FsCapwapRunResetLastSuccAttemptTimeSet, FsCapwapRunResetLastSuccAttemptTimeTest, FsCawapRunStatsTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsCawapRunStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsCapwapRunResetLastUnsuccessfulAttemptTime}, GetNextIndexFsCawapRunStatsTable, FsCapwapRunResetLastUnsuccessfulAttemptTimeGet, FsCapwapRunResetLastUnsuccessfulAttemptTimeSet, FsCapwapRunResetLastUnsuccessfulAttemptTimeTest, FsCawapRunStatsTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsCawapRunStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsCapwapRunPriDiscReqReceived}, GetNextIndexFsCawapRunStatsTable, FsCapwapRunPriDiscReqReceivedGet, FsCapwapRunPriDiscReqReceivedSet, FsCapwapRunPriDiscReqReceivedTest, FsCawapRunStatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCawapRunStatsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapRunPriDiscRspReceived}, GetNextIndexFsCawapRunStatsTable, FsCapwapRunPriDiscRspReceivedGet, FsCapwapRunPriDiscRspReceivedSet, FsCapwapRunPriDiscRspReceivedTest, FsCawapRunStatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCawapRunStatsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapRunPriDiscReqTransmitted}, GetNextIndexFsCawapRunStatsTable, FsCapwapRunPriDiscReqTransmittedGet, FsCapwapRunPriDiscReqTransmittedSet, FsCapwapRunPriDiscReqTransmittedTest, FsCawapRunStatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCawapRunStatsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapRunPriDiscRspTransmitted}, GetNextIndexFsCawapRunStatsTable, FsCapwapRunPriDiscRspTransmittedGet, FsCapwapRunPriDiscRspTransmittedSet, FsCapwapRunPriDiscRspTransmittedTest, FsCawapRunStatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCawapRunStatsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapRunPriDiscunsuccessfulProcessed}, GetNextIndexFsCawapRunStatsTable, FsCapwapRunPriDiscunsuccessfulProcessedGet, FsCapwapRunPriDiscunsuccessfulProcessedSet, FsCapwapRunPriDiscunsuccessfulProcessedTest, FsCawapRunStatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCawapRunStatsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapRunPriDiscReasonLastUnsuccAttempt}, GetNextIndexFsCawapRunStatsTable, FsCapwapRunPriDiscReasonLastUnsuccAttemptGet, FsCapwapRunPriDiscReasonLastUnsuccAttemptSet, FsCapwapRunPriDiscReasonLastUnsuccAttemptTest, FsCawapRunStatsTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsCawapRunStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsCapwapRunPriDiscLastSuccAttemptTime}, GetNextIndexFsCawapRunStatsTable, FsCapwapRunPriDiscLastSuccAttemptTimeGet, FsCapwapRunPriDiscLastSuccAttemptTimeSet, FsCapwapRunPriDiscLastSuccAttemptTimeTest, FsCawapRunStatsTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsCawapRunStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsCapwapRunPriDiscLastUnsuccessfulAttemptTime}, GetNextIndexFsCawapRunStatsTable, FsCapwapRunPriDiscLastUnsuccessfulAttemptTimeGet, FsCapwapRunPriDiscLastUnsuccessfulAttemptTimeSet, FsCapwapRunPriDiscLastUnsuccessfulAttemptTimeTest, FsCawapRunStatsTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsCawapRunStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsCapwapRunEchoReqReceived}, GetNextIndexFsCawapRunStatsTable, FsCapwapRunEchoReqReceivedGet, FsCapwapRunEchoReqReceivedSet, FsCapwapRunEchoReqReceivedTest, FsCawapRunStatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCawapRunStatsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapRunEchoRspReceived}, GetNextIndexFsCawapRunStatsTable, FsCapwapRunEchoRspReceivedGet, FsCapwapRunEchoRspReceivedSet, FsCapwapRunEchoRspReceivedTest, FsCawapRunStatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCawapRunStatsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapRunEchoReqTransmitted}, GetNextIndexFsCawapRunStatsTable, FsCapwapRunEchoReqTransmittedGet, FsCapwapRunEchoReqTransmittedSet, FsCapwapRunEchoReqTransmittedTest, FsCawapRunStatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCawapRunStatsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapRunEchoRspTransmitted}, GetNextIndexFsCawapRunStatsTable, FsCapwapRunEchoRspTransmittedGet, FsCapwapRunEchoRspTransmittedSet, FsCapwapRunEchoRspTransmittedTest, FsCawapRunStatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCawapRunStatsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapRunEchounsuccessfulProcessed}, GetNextIndexFsCawapRunStatsTable, FsCapwapRunEchounsuccessfulProcessedGet, FsCapwapRunEchounsuccessfulProcessedSet, FsCapwapRunEchounsuccessfulProcessedTest, FsCawapRunStatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCawapRunStatsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapRunEchoReasonLastUnsuccAttempt}, GetNextIndexFsCawapRunStatsTable, FsCapwapRunEchoReasonLastUnsuccAttemptGet, FsCapwapRunEchoReasonLastUnsuccAttemptSet, FsCapwapRunEchoReasonLastUnsuccAttemptTest, FsCawapRunStatsTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsCawapRunStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsCapwapRunEchoLastSuccAttemptTime}, GetNextIndexFsCawapRunStatsTable, FsCapwapRunEchoLastSuccAttemptTimeGet, FsCapwapRunEchoLastSuccAttemptTimeSet, FsCapwapRunEchoLastSuccAttemptTimeTest, FsCawapRunStatsTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsCawapRunStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsCapwapRunEchoLastUnsuccessfulAttemptTime}, GetNextIndexFsCawapRunStatsTable, FsCapwapRunEchoLastUnsuccessfulAttemptTimeGet, FsCapwapRunEchoLastUnsuccessfulAttemptTimeSet, FsCapwapRunEchoLastUnsuccessfulAttemptTimeTest, FsCawapRunStatsTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsCawapRunStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsCapwapRunStatsRowStatus}, GetNextIndexFsCawapRunStatsTable, FsCapwapRunStatsRowStatusGet, FsCapwapRunStatsRowStatusSet, FsCapwapRunStatsRowStatusTest, FsCawapRunStatsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsCawapRunStatsTableINDEX, 1, 0, 1, NULL},

{{13,FsCapwapWirelessBindingVirtualRadioIfIndex}, GetNextIndexFsCapwapWirelessBindingTable, FsCapwapWirelessBindingVirtualRadioIfIndexGet, FsCapwapWirelessBindingVirtualRadioIfIndexSet, FsCapwapWirelessBindingVirtualRadioIfIndexTest, FsCapwapWirelessBindingTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsCapwapWirelessBindingTableINDEX, 2, 0, 0, NULL},

{{13,FsCapwapWirelessBindingType}, GetNextIndexFsCapwapWirelessBindingTable, FsCapwapWirelessBindingTypeGet, FsCapwapWirelessBindingTypeSet, FsCapwapWirelessBindingTypeTest, FsCapwapWirelessBindingTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsCapwapWirelessBindingTableINDEX, 2, 0, 0, "1"},

{{13,FsCapwapWirelessBindingRowStatus}, GetNextIndexFsCapwapWirelessBindingTable, FsCapwapWirelessBindingRowStatusGet, FsCapwapWirelessBindingRowStatusSet, FsCapwapWirelessBindingRowStatusTest, FsCapwapWirelessBindingTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsCapwapWirelessBindingTableINDEX, 2, 0, 1, NULL},

{{13,FsCapwapStationWhiteListStationId}, GetNextIndexFsCapwapStationWhiteListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsCapwapStationWhiteListTableINDEX, 1, 0, 0, NULL},

{{13,FsCapwapStationWhiteListRowStatus}, GetNextIndexFsCapwapStationWhiteListTable, FsCapwapStationWhiteListRowStatusGet, FsCapwapStationWhiteListRowStatusSet, FsCapwapStationWhiteListRowStatusTest, FsCapwapStationWhiteListTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsCapwapStationWhiteListTableINDEX, 1, 0, 1, NULL},

{{13,FsCapwapWtpRebootStatisticsRebootCount}, GetNextIndexFsCapwapWtpRebootStatisticsTable, FsCapwapWtpRebootStatisticsRebootCountGet, FsCapwapWtpRebootStatisticsRebootCountSet, FsCapwapWtpRebootStatisticsRebootCountTest, FsCapwapWtpRebootStatisticsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCapwapWtpRebootStatisticsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapWtpRebootStatisticsAcInitiatedCount}, GetNextIndexFsCapwapWtpRebootStatisticsTable, FsCapwapWtpRebootStatisticsAcInitiatedCountGet, FsCapwapWtpRebootStatisticsAcInitiatedCountSet, FsCapwapWtpRebootStatisticsAcInitiatedCountTest, FsCapwapWtpRebootStatisticsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCapwapWtpRebootStatisticsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapWtpRebootStatisticsLinkFailureCount}, GetNextIndexFsCapwapWtpRebootStatisticsTable, FsCapwapWtpRebootStatisticsLinkFailureCountGet, FsCapwapWtpRebootStatisticsLinkFailureCountSet, FsCapwapWtpRebootStatisticsLinkFailureCountTest, FsCapwapWtpRebootStatisticsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCapwapWtpRebootStatisticsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapWtpRebootStatisticsSwFailureCount}, GetNextIndexFsCapwapWtpRebootStatisticsTable, FsCapwapWtpRebootStatisticsSwFailureCountGet, FsCapwapWtpRebootStatisticsSwFailureCountSet, FsCapwapWtpRebootStatisticsSwFailureCountTest, FsCapwapWtpRebootStatisticsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCapwapWtpRebootStatisticsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapWtpRebootStatisticsHwFailureCount}, GetNextIndexFsCapwapWtpRebootStatisticsTable, FsCapwapWtpRebootStatisticsHwFailureCountGet, FsCapwapWtpRebootStatisticsHwFailureCountSet, FsCapwapWtpRebootStatisticsHwFailureCountTest, FsCapwapWtpRebootStatisticsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCapwapWtpRebootStatisticsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapWtpRebootStatisticsOtherFailureCount}, GetNextIndexFsCapwapWtpRebootStatisticsTable, FsCapwapWtpRebootStatisticsOtherFailureCountGet, FsCapwapWtpRebootStatisticsOtherFailureCountSet, FsCapwapWtpRebootStatisticsOtherFailureCountTest, FsCapwapWtpRebootStatisticsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCapwapWtpRebootStatisticsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapWtpRebootStatisticsUnknownFailureCount}, GetNextIndexFsCapwapWtpRebootStatisticsTable, FsCapwapWtpRebootStatisticsUnknownFailureCountGet, FsCapwapWtpRebootStatisticsUnknownFailureCountSet, FsCapwapWtpRebootStatisticsUnknownFailureCountTest, FsCapwapWtpRebootStatisticsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCapwapWtpRebootStatisticsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapWtpRebootStatisticsLastFailureType}, GetNextIndexFsCapwapWtpRebootStatisticsTable, FsCapwapWtpRebootStatisticsLastFailureTypeGet, FsCapwapWtpRebootStatisticsLastFailureTypeSet, FsCapwapWtpRebootStatisticsLastFailureTypeTest, FsCapwapWtpRebootStatisticsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsCapwapWtpRebootStatisticsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapWtpRebootStatisticsRowStatus}, GetNextIndexFsCapwapWtpRebootStatisticsTable, FsCapwapWtpRebootStatisticsRowStatusGet, FsCapwapWtpRebootStatisticsRowStatusSet, FsCapwapWtpRebootStatisticsRowStatusTest, FsCapwapWtpRebootStatisticsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsCapwapWtpRebootStatisticsTableINDEX, 1, 0, 1, NULL},

{{13,FsCapwapWtpRadioLastFailType}, GetNextIndexFsCapwapWtpRadioStatisticsTable, FsCapwapWtpRadioLastFailTypeGet, FsCapwapWtpRadioLastFailTypeSet, FsCapwapWtpRadioLastFailTypeTest, FsCapwapWtpRadioStatisticsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsCapwapWtpRadioStatisticsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapWtpRadioResetCount}, GetNextIndexFsCapwapWtpRadioStatisticsTable, FsCapwapWtpRadioResetCountGet, FsCapwapWtpRadioResetCountSet, FsCapwapWtpRadioResetCountTest, FsCapwapWtpRadioStatisticsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCapwapWtpRadioStatisticsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapWtpRadioSwFailureCount}, GetNextIndexFsCapwapWtpRadioStatisticsTable, FsCapwapWtpRadioSwFailureCountGet, FsCapwapWtpRadioSwFailureCountSet, FsCapwapWtpRadioSwFailureCountTest, FsCapwapWtpRadioStatisticsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCapwapWtpRadioStatisticsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapWtpRadioHwFailureCount}, GetNextIndexFsCapwapWtpRadioStatisticsTable, FsCapwapWtpRadioHwFailureCountGet, FsCapwapWtpRadioHwFailureCountSet, FsCapwapWtpRadioHwFailureCountTest, FsCapwapWtpRadioStatisticsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCapwapWtpRadioStatisticsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapWtpRadioOtherFailureCount}, GetNextIndexFsCapwapWtpRadioStatisticsTable, FsCapwapWtpRadioOtherFailureCountGet, FsCapwapWtpRadioOtherFailureCountSet, FsCapwapWtpRadioOtherFailureCountTest, FsCapwapWtpRadioStatisticsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCapwapWtpRadioStatisticsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapWtpRadioUnknownFailureCount}, GetNextIndexFsCapwapWtpRadioStatisticsTable, FsCapwapWtpRadioUnknownFailureCountGet, FsCapwapWtpRadioUnknownFailureCountSet, FsCapwapWtpRadioUnknownFailureCountTest, FsCapwapWtpRadioStatisticsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCapwapWtpRadioStatisticsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapWtpRadioConfigUpdateCount}, GetNextIndexFsCapwapWtpRadioStatisticsTable, FsCapwapWtpRadioConfigUpdateCountGet, FsCapwapWtpRadioConfigUpdateCountSet, FsCapwapWtpRadioConfigUpdateCountTest, FsCapwapWtpRadioStatisticsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCapwapWtpRadioStatisticsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapWtpRadioChannelChangeCount}, GetNextIndexFsCapwapWtpRadioStatisticsTable, FsCapwapWtpRadioChannelChangeCountGet, FsCapwapWtpRadioChannelChangeCountSet, FsCapwapWtpRadioChannelChangeCountTest, FsCapwapWtpRadioStatisticsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCapwapWtpRadioStatisticsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapWtpRadioBandChangeCount}, GetNextIndexFsCapwapWtpRadioStatisticsTable, FsCapwapWtpRadioBandChangeCountGet, FsCapwapWtpRadioBandChangeCountSet, FsCapwapWtpRadioBandChangeCountTest, FsCapwapWtpRadioStatisticsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCapwapWtpRadioStatisticsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapWtpRadioCurrentNoiseFloor}, GetNextIndexFsCapwapWtpRadioStatisticsTable, FsCapwapWtpRadioCurrentNoiseFloorGet, FsCapwapWtpRadioCurrentNoiseFloorSet, FsCapwapWtpRadioCurrentNoiseFloorTest, FsCapwapWtpRadioStatisticsTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsCapwapWtpRadioStatisticsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapWtpRadioStatRowStatus}, GetNextIndexFsCapwapWtpRadioStatisticsTable, FsCapwapWtpRadioStatRowStatusGet, FsCapwapWtpRadioStatRowStatusSet, FsCapwapWtpRadioStatRowStatusTest, FsCapwapWtpRadioStatisticsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsCapwapWtpRadioStatisticsTableINDEX, 1, 0, 1, NULL},

{{13,FsCapwapDot11TxFragmentCnt}, GetNextIndexFsCapwapDot11StatisticsTable, FsCapwapDot11TxFragmentCntGet, FsCapwapDot11TxFragmentCntSet, FsCapwapDot11TxFragmentCntTest, FsCapwapDot11StatisticsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCapwapDot11StatisticsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapDot11MulticastTxCnt}, GetNextIndexFsCapwapDot11StatisticsTable, FsCapwapDot11MulticastTxCntGet, FsCapwapDot11MulticastTxCntSet, FsCapwapDot11MulticastTxCntTest, FsCapwapDot11StatisticsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCapwapDot11StatisticsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapDot11FailedCount}, GetNextIndexFsCapwapDot11StatisticsTable, FsCapwapDot11FailedCountGet, FsCapwapDot11FailedCountSet, FsCapwapDot11FailedCountTest, FsCapwapDot11StatisticsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCapwapDot11StatisticsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapDot11RetryCount}, GetNextIndexFsCapwapDot11StatisticsTable, FsCapwapDot11RetryCountGet, FsCapwapDot11RetryCountSet, FsCapwapDot11RetryCountTest, FsCapwapDot11StatisticsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCapwapDot11StatisticsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapDot11MultipleRetryCount}, GetNextIndexFsCapwapDot11StatisticsTable, FsCapwapDot11MultipleRetryCountGet, FsCapwapDot11MultipleRetryCountSet, FsCapwapDot11MultipleRetryCountTest, FsCapwapDot11StatisticsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCapwapDot11StatisticsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapDot11FrameDupCount}, GetNextIndexFsCapwapDot11StatisticsTable, FsCapwapDot11FrameDupCountGet, FsCapwapDot11FrameDupCountSet, FsCapwapDot11FrameDupCountTest, FsCapwapDot11StatisticsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCapwapDot11StatisticsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapDot11RTSSuccessCount}, GetNextIndexFsCapwapDot11StatisticsTable, FsCapwapDot11RTSSuccessCountGet, FsCapwapDot11RTSSuccessCountSet, FsCapwapDot11RTSSuccessCountTest, FsCapwapDot11StatisticsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCapwapDot11StatisticsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapDot11RTSFailCount}, GetNextIndexFsCapwapDot11StatisticsTable, FsCapwapDot11RTSFailCountGet, FsCapwapDot11RTSFailCountSet, FsCapwapDot11RTSFailCountTest, FsCapwapDot11StatisticsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCapwapDot11StatisticsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapDot11ACKFailCount}, GetNextIndexFsCapwapDot11StatisticsTable, FsCapwapDot11ACKFailCountGet, FsCapwapDot11ACKFailCountSet, FsCapwapDot11ACKFailCountTest, FsCapwapDot11StatisticsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCapwapDot11StatisticsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapDot11RxFragmentCount}, GetNextIndexFsCapwapDot11StatisticsTable, FsCapwapDot11RxFragmentCountGet, FsCapwapDot11RxFragmentCountSet, FsCapwapDot11RxFragmentCountTest, FsCapwapDot11StatisticsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCapwapDot11StatisticsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapDot11MulticastRxCount}, GetNextIndexFsCapwapDot11StatisticsTable, FsCapwapDot11MulticastRxCountGet, FsCapwapDot11MulticastRxCountSet, FsCapwapDot11MulticastRxCountTest, FsCapwapDot11StatisticsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCapwapDot11StatisticsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapDot11FCSErrCount}, GetNextIndexFsCapwapDot11StatisticsTable, FsCapwapDot11FCSErrCountGet, FsCapwapDot11FCSErrCountSet, FsCapwapDot11FCSErrCountTest, FsCapwapDot11StatisticsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCapwapDot11StatisticsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapDot11TxFrameCount}, GetNextIndexFsCapwapDot11StatisticsTable, FsCapwapDot11TxFrameCountGet, FsCapwapDot11TxFrameCountSet, FsCapwapDot11TxFrameCountTest, FsCapwapDot11StatisticsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCapwapDot11StatisticsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapDot11DecryptionErr}, GetNextIndexFsCapwapDot11StatisticsTable, FsCapwapDot11DecryptionErrGet, FsCapwapDot11DecryptionErrSet, FsCapwapDot11DecryptionErrTest, FsCapwapDot11StatisticsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCapwapDot11StatisticsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapDot11DiscardQosFragmentCnt}, GetNextIndexFsCapwapDot11StatisticsTable, FsCapwapDot11DiscardQosFragmentCntGet, FsCapwapDot11DiscardQosFragmentCntSet, FsCapwapDot11DiscardQosFragmentCntTest, FsCapwapDot11StatisticsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCapwapDot11StatisticsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapDot11AssociatedStaCount}, GetNextIndexFsCapwapDot11StatisticsTable, FsCapwapDot11AssociatedStaCountGet, FsCapwapDot11AssociatedStaCountSet, FsCapwapDot11AssociatedStaCountTest, FsCapwapDot11StatisticsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCapwapDot11StatisticsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapDot11QosCFPollsRecvdCount}, GetNextIndexFsCapwapDot11StatisticsTable, FsCapwapDot11QosCFPollsRecvdCountGet, FsCapwapDot11QosCFPollsRecvdCountSet, FsCapwapDot11QosCFPollsRecvdCountTest, FsCapwapDot11StatisticsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCapwapDot11StatisticsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapDot11QosCFPollsUnusedCount}, GetNextIndexFsCapwapDot11StatisticsTable, FsCapwapDot11QosCFPollsUnusedCountGet, FsCapwapDot11QosCFPollsUnusedCountSet, FsCapwapDot11QosCFPollsUnusedCountTest, FsCapwapDot11StatisticsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCapwapDot11StatisticsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapDot11QosCFPollsUnusableCount}, GetNextIndexFsCapwapDot11StatisticsTable, FsCapwapDot11QosCFPollsUnusableCountGet, FsCapwapDot11QosCFPollsUnusableCountSet, FsCapwapDot11QosCFPollsUnusableCountTest, FsCapwapDot11StatisticsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCapwapDot11StatisticsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapDot11RadioId}, GetNextIndexFsCapwapDot11StatisticsTable, FsCapwapDot11RadioIdGet, FsCapwapDot11RadioIdSet, FsCapwapDot11RadioIdTest, FsCapwapDot11StatisticsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCapwapDot11StatisticsTableINDEX, 1, 0, 0, NULL},

{{13,FsWtpLocalRouting}, GetNextIndexFsWtpLocalRoutingTable, FsWtpLocalRoutingGet, FsWtpLocalRoutingSet, FsWtpLocalRoutingTest, FsWtpLocalRoutingTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsWtpLocalRoutingTableINDEX, 1, 0, 0, "2"},

{{13,FsCapwapFsAcTimestampTrigger}, GetNextIndexFsCapwapFsAcTimestampTriggerTable, FsCapwapFsAcTimestampTriggerGet, FsCapwapFsAcTimestampTriggerSet, FsCapwapFsAcTimestampTriggerTest, FsCapwapFsAcTimestampTriggerTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsCapwapFsAcTimestampTriggerTableINDEX, 1, 0, 0, "2"},

{{13,FsCapwATPStatsSentBytes}, GetNextIndexFsCapwATPStatsTable, FsCapwATPStatsSentBytesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsCapwATPStatsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwATPStatsRcvdBytes}, GetNextIndexFsCapwATPStatsTable, FsCapwATPStatsRcvdBytesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsCapwATPStatsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwATPStatsWiredSentBytes}, GetNextIndexFsCapwATPStatsTable, FsCapwATPStatsWiredSentBytesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsCapwATPStatsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwATPStatsWirelessSentBytes}, GetNextIndexFsCapwATPStatsTable, FsCapwATPStatsWirelessSentBytesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsCapwATPStatsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwATPStatsWiredRcvdBytes}, GetNextIndexFsCapwATPStatsTable, FsCapwATPStatsWiredRcvdBytesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsCapwATPStatsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwATPStatsWirelessRcvdBytes}, GetNextIndexFsCapwATPStatsTable, FsCapwATPStatsWirelessRcvdBytesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsCapwATPStatsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwATPStatsSentTrafficRate}, GetNextIndexFsCapwATPStatsTable, FsCapwATPStatsSentTrafficRateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsCapwATPStatsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwATPStatsRcvdTrafficRate}, GetNextIndexFsCapwATPStatsTable, FsCapwATPStatsRcvdTrafficRateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsCapwATPStatsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwATPStatsTotalClient}, GetNextIndexFsCapwATPStatsTable, FsCapwATPStatsTotalClientGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsCapwATPStatsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwATPStatsMaxClientPerAPRadio}, GetNextIndexFsCapwATPStatsTable, FsCapwATPStatsMaxClientPerAPRadioGet, FsCapwATPStatsMaxClientPerAPRadioSet, FsCapwATPStatsMaxClientPerAPRadioTest, FsCapwATPStatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCapwATPStatsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwATPStatsMaxClientPerSSID}, GetNextIndexFsCapwATPStatsTable, FsCapwATPStatsMaxClientPerSSIDGet, FsCapwATPStatsMaxClientPerSSIDSet, FsCapwATPStatsMaxClientPerSSIDTest, FsCapwATPStatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsCapwATPStatsTableINDEX, 1, 0, 0, "0"},

{{13,FsCapwapFragReassembleStatus}, GetNextIndexFsCapwapExtWtpConfigTable, FsCapwapFragReassembleStatusGet, FsCapwapFragReassembleStatusSet, FsCapwapFragReassembleStatusTest, FsCapwapExtWtpConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsCapwapExtWtpConfigTableINDEX, 1, 1, 0, "1"},

{{13,FsCapwapApSerialNumber}, GetNextIndexFsCapwapExtWtpConfigTable, FsCapwapApSerialNumberGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsCapwapExtWtpConfigTableINDEX, 1, 0, 0, NULL},

{{13,FsCapwapWtpImageName}, GetNextIndexFsCapwapExtWtpConfigTable, FsCapwapWtpImageNameGet, FsCapwapWtpImageNameSet, FsCapwapWtpImageNameTest, FsCapwapExtWtpConfigTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsCapwapExtWtpConfigTableINDEX, 1, 0, 0, NULL},

{{11,FsCapwapMaxRetransmitCount}, NULL, FsCapwapMaxRetransmitCountGet, FsCapwapMaxRetransmitCountSet, FsCapwapMaxRetransmitCountTest, FsCapwapMaxRetransmitCountDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "10"},

{{11,FsCapwTrapStatus}, NULL, FsCapwTrapStatusGet, FsCapwTrapStatusSet, FsCapwTrapStatusTest, FsCapwTrapStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{11,FsCapwCriticalTrapStatus}, NULL, FsCapwCriticalTrapStatusGet, FsCapwCriticalTrapStatusSet, FsCapwCriticalTrapStatusTest, FsCapwCriticalTrapStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{13,FsCapwapStationBlackListStationId}, GetNextIndexFsCapwapStationBlackListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsCapwapStationBlackListTableINDEX, 1, 0, 0, NULL},

{{13,FsCapwapStationBlackListRowStatus}, GetNextIndexFsCapwapStationBlackListTable, FsCapwapStationBlackListRowStatusGet, FsCapwapStationBlackListRowStatusSet, FsCapwapStationBlackListRowStatusTest, FsCapwapStationBlackListTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsCapwapStationBlackListTableINDEX, 1, 0, 1, NULL},

{{12,FsCapwapWtpImageUpgradeStatus}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},

{{12,FsCapwapWtpSyncStatus}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 1, 0, NULL},
};
tMibData fscapwEntry = { 213, fscapwMibEntry };

#endif /* _FSCAPWDB_H */

