/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved                      *
 * $Id: capwapconst.h,v 1.4 2017/11/24 10:37:01 siva Exp $ 
 * Description: This file contains the CAPWAP Packet TX related functions    *
 *****************************************************************************/
#ifndef  __CAPWAP_CONST_H__
#define  __CAPWAP_CONST_H__

/*#include "capwapinc.h" */


#define CAPWAP_HDR_LEN              8 
#define CAPWAP_IP_HEADER_LEN               100

#define CAPWAP_MAX_RADIO_ID         31

#define CAPWAP_BUFFER_SIZE          256

/*CAPWAP Header Length Information */
#define CAPWAP_HDR_LEN_MASK                     0xF800
#define CAPWAP_HDR_LEN_POSITION                 11
#define CAPWAP_HEADER_HAS_RADIO_MAC(HLEN)      ((HLEN > 2) ? OSIX_TRUE : OSIX_FALSE)            
#define CAPWAP_HEADER_RADIO_MAC_LEN             8
#define CAPWAP_HEADER_HAS_WIRELESS_INFO(HLEN)      ((HLEN > 3) ? OSIX_TRUE : OSIX_FALSE)            
#define CAPWAP_HEADER_WIRELESS_INFO_LEN         8
#define CAPWAP_HEADER_RMAC_MASK                 0x10
#define CAPWAP_HEADER_RMAC_PAD                  1
#define CAPWAP_HEADER_WIFI_INFO_MASK            0x20
#define CAPWAP_HEADER_WIFI_INFO_PAD             3

#define CAPWAP_MAX_HEX_SINGLE_DIGIT   0xf

#define CAPWAP_MAX_HDR_LEN          16 /* Including the Optional Fields */  
#define CAPWAP_CONTROL_HDR_LEN      8
#define CAPWAP_DTLS_HDR_LEN         4
#define CAPWAP_DATA_MSGELEM_LEN     2
#define CAPWAP_MAX_KEEPALIVE_LEN    CAPWAP_HDR_LEN + CAPWAP_DATA_MSGELEM_LEN + 16 
#define CAPWAP_DOT11_BIND_TYPE      1
#define CAPWAP_SEQ_NUM_LEN          1
#define CAPWAP_SOCK_HDR_LEN         CAPWAP_HDR_LEN + CAPWAP_MSG_ELEM_TYPE_LEN + CAPWAP_SEQ_NUM_LEN
#define CAPWAP_SNMP_V2_TRAP_OID_LEN 11

#define CAPWAP_SHUTDOWN               1   
#define CAPWAP_NO_SHUTDOWN            0

#define CAPWAP_MAX_TX_PKTS            10

#define CAPWAP_MAX_DISCOVERY_COUNT       10
#define CAPWAP_MAX_RETRANSMIT_COUNT      1
#define CAPWAP_MAX_RETRANSMIT_INTERVAL   3

#define     WLAN_CONF_REQ             3398913
#define     WLAN_CONF_RSP             3398914

#define     CAPWAP_IEEE_MSG_INDEX_OFFSET        3398912

#define       CAPWAP_INET_AFI_IPV4    1
#define       CAPWAP_INET_AFI_IPV6    2

#define CAPWAP_NUM_TEN                10

#define CAPWAP_WTP_TRAP_ENABLE        1
#define CAPWAP_WTP_TRAP_DISABLE       2
#define CAPWAP_INTERFACE_LEN       24

#define CAPWAP_CRITICAL_TRAP_ENABLE     1
#define CAPWAP_CRITICAL_TRAP_DISABLE    2
#define VENDOR_DFS_CHANNEL_MSG_LEN  126
#define CAPWAP_WTP_DEFAULT_TRAP_STATUS              CAPWAP_WTP_TRAP_DISABLE
#define CAPWAP_WTP_DEFAULT_CRITICAL_TRAP_STATUS     CAPWAP_CRITICAL_TRAP_ENABLE
#define CAPWAP_WTP_RESTART                   1
#define CAPWAP_WTP_IMAGE_UPGRADE_INDICATION  2
#define CAPWAP_WTP_IP_ADDR_CHANGE            3
#define CAPWAP_WTP_STATE_CHANGE              4
#define CAPWAP_WTP_SYNC_TRAP                 5
#define CAPWAP_WTP_IMAGE_NOT_SUPPORTED       6
#define CAPWAP_STATION_CONFIG_MIN       600
#define CAPWAP_STATION_CONFIG_MAX       3600

#define     CAPWAP_DNS_QUERY_SIZE               512
#define     CAPWAP_DNS_AC_DISC_RETRY          5 
#define     CAPWAP_DHCP_AC_DISC_RETRY           3
#define     CAPWAP_MIN_RETRANSMITCOUNT          1
#define     CAPWAP_MAX_RETRANSMITCOUNT          1
/* CAPWAP Timer default timeouts */
#define SILENT_INTERVAL_TIMEOUT                 30 
#define CAPWAP_WAITDTLS_TIMEOUT                 60
#define CAPWAP_DTLS_SESSION_DELETE_TIMEOUT      5
#define DISC_INTERVAL_TIMEOUT                   5 
#define DHCP_OFFER_INTERVAL_TIMEOUT             10 
#define MAXDISC_INTERVAL_TIMEOUT                20
#define CHANGESTATE_PENDING_TIMEOUT             25
#define CAPWAP_WAITJOIN_TIMEOUT                 60
#define CAPWAP_DATACHECK_TIMEOUT                30
#define CAPWAP_ECHO_INTERVAL_TIMEOUT            50
#define CAPWAP_WLC_ECHO_INTERVAL_TIMEOUT        60
#define CAPWAP_AP_ECHO_INTERVAL_TIMEOUT         30
#define CAPWAP_DATACHNL_KEEPALIVE_TIMEOUT       30
#define CAPWAP_DATACHNL_DEADINTERVAL_TIMEOUT    60
#define CAPWAP_PMTU_UPDATE_TIMEOUT              (120/MAX_WTP_SUPPORTED) 
#define CAPWAP_IMAGE_DATA_START_TIMEOUT         30

#define CAPWAP_MAX_SEQ_NUM                      255
#define   CAPWAP_INIT_VAL                       0
#define MIN_CAPWAP_MSG_TYPE                     CAPWAP_DISC_REQ 
#define MAX_CAPWAP_MSG_TYPE                     CAPWAP_STATION_CONF_RSP
#define CAPWAP_DISCOVERY_OFFSET                 0    
#define CAPWAP_JOIN_OFFSET                      0 
#define CAPWAP_IP6ADDR_LEN                      16
#define CAPWAP_NAME_DATA_SIZE                   1024

#define CAPWAP_HDRLEN_OFFSET                    1
#define CAPWAP_HDRLEN_FIELD_LEN                 1
#define CAPWAP_HDRLEN_MASK                      0xf8
#define CAPWAP_4BYTE_WORD_TO_NUMOF_BYTES        2
#define CAPWAP_FLAGS_BIT_OFFSET                 3
#define CAPWAP_FLAGS_OFFSET_LEN                 1
#define CAPWAP_LAST_FRAG_BIT                    0x40
#define CAPWAP_MORE_FRAG_BIT                    0x80
#define CAPWAP_FRAG_OFFSET                      6
#define CAPWAP_FRAG_OFFSET_LEN                  2
#define MAX_CAPWAP_FRAGMENTS_SIZE               20
#define CAPWAP_ZERO_OFFSET                      0
#define CAPWAP_FRAG_OFFSET_MASK                 0xfff8
#define CAPWAP_FRAGID_OFFSET                    4
#define CAPWAP_FRAGID_OFFSET_LEN                2
#define CAPWAP_FRAG_DATA_OFFSET                 0xf8
#define CAPWAP_OPT_MAC_OFFSET         9
#define CAPWAP_OPT_MAC_FIELD_LEN      6 
#define CAPWAP_OFF_80211_MGMT_PKT               4


#define CAPWAP_DOT11_HDR_FIELD                  0x10
#define CAPWAP_DOT3_HDR_FIELD                   0x20
#define CAPWAP_RADIO_MAC_OFFSET                 9

#define IMAGE_DATA_CHECKSUM_LEN                 16
#define DEFAULT_COUNTRY                         (UINT1 *)"US"
/* As the default retrnsmission timeout to 3 seconds the 
 * reassembly timer should be less than retransmission timer */
#define CAPWAP_REASSENBLY_TIMEOUT               2 
#define CAPWAP_DATA_REASSENBLY_TIMEOUT          4 
#define CAPWAP_MAX_NO_OF_FRAGMENTS_PER_FRAME    25
#define CAPWAP_FRAG_INSERT                      0
#define CAPWAP_FRAG_APPEND                      1
#define CAPWAP_FRAG_PREPEND                     2
#define CAPWAP_ZERO_LEN                         0
#define CAPWAP_FRAG_BIT_CLEAR_OFFSET            0x3f
#define CAPWAP_NOTIFY_DISABLE                   2
#define CAPWAP_NOTIFY_ENABLE                    1

#define CAPWAP_MAX_BOARD_DATA_TYPE              5
#define MAX_DISC_RESPONSE                       3     
#define MAX_STATIC_DISC                         10  
#define MAX_AC_NAME_SIZE                        512

#define  CAPWAP_MAX_MSG                         27

#define  MAX_CAPWAP_MSG_ELEMENT                 54
#define CAPWAP_MAX_CONTRL_IEEE_MESSAGE_BLOCKS   26

#define CAPWAP_DEFAULT_MESG_ELEM_INSTANCE       1
#define CAPWAP_PROT_MSG_ELEM_MIN                1
#define CAPWAP_PROT_MSG_ELEM_MAX                53
#define CAPWAP_CONTROL_MSG_ELEM_MAX             1023
#define CAPWAP_IEEE_802_11_MSG_ELEM_MIN         1024
#define CAPWAP_IEEE_802_11_MSG_ELEM_MAX         1048
/* the total length of Capwap message element's type and length fields */
#define CAPWAP_MSG_ELEM_TYPE_LEN                4
#define CAPWAP_CTRL_HEADER_SIZE                 8
#define CAPWAP_WLC_HLEN_RID_WBID_FLAGT          0x1003 
#define CAPWAP_WTP_HLEN_RID_WBID_FLAGT          0x1000
#define CAPWAP_RF_GROUP_NAME_SIZE               19

#define CAPWAP_CMN_MSG_ELM_HEAD_LEN      3
#define VLAN_HDR_LEN    4
/* CAPWAP message element type */
#define  AC_DESCRIPTOR                   1
#define  AC_IPv4_LIST                    2
#define  AC_IPv6_LIST                    3
#define  AC_NAME                         4
#define  AC_NAME_WITH_PRIO               5
#define  AC_TIMESTAMP                    6
#define  ADD_MAC_ACL_ENTRY               7
#define  ADD_STATION                     8
#define  RESERVED                        9
#define  CAPWAP_CTRL_IPV4_ADDR          10
#define  CAPWAP_CTRL_IPV6_ADDR          11
#define  CAPWAP_LOCAL_IPV4_ADDR         30
#define  CAPWAP_LOCAL_IPV6_ADDR         50
#define  CAPWAP_TIMERS                  12
#define  CAPWAP_TRANSPORT_PROTOCOL      51
#define  DATA_TRANSFER_DATA             13
#define  DATA_TRANSFER_MODE             14
#define  DECRYPTION_ERROR_REPORT        15
#define  DECRYPTION_ERROR_REPORT_PERIOD 16
#define  DELETE_MAC_ACL_ENTRY           17
#define  DELETE_STATION                 18
#define  DISCOVERY_TYPE                 20
#define  DUPLICATE_IPV4_ADDR            21
#define  DUPLICATE_IPV6_ADDR            22
#define  ECN_SUPPORT                    53
#define  IDLE_TIMEOUT                   23
#define  IMAGE_DATA                     24
#define  IMAGE_IDENTIFIER               25
#define  IMAGE_INFORMATION              26
#define  INITIATE_DOWNLOAD              27
#define  LOCATION_DATA                  28
#define  MAX_MESG_LEN                   29
#define  MTU_DISC_PADDING               52
#define  RADIO_ADMIN_STATE              31
#define  RADIO_OPER_STATE               32
#define  RESULT_CODE                    33
#define  RETURNED_MSG_ELEMENT           34
#define  SESSION_ID                     35
#define  STATS_TIMER                    36
#define  VENDOR_SPECIFIC_PAYLOAD        37 
#define  WTP_BOARD_DATA                 38
#define  WTP_DESCRIPTOR                 39
#define  WTP_FALLBACK                   40
#define  WTP_FRAME_TUNNEL_MODE          41
#define  WTP_MAC_TYPE                   44
#define  WTP_NAME                       45
#define  WTP_RADIO_STATISTICS           47
#define  WTP_REBOOT_STATISTICS          48
#define  WTP_STATIC_IP_ADDR_INFO        49

/* CAPWAP Message elements Minimum length */
#define  CAPWAP_VENDOR_ID_LEN                   4
#define  AC_DESCRIPTOR_MIN_LEN                  12
#define  AC_IPv4_LIST_MIN_LEN                   4
#define  AC_IPv6_LIST_MIN_LEN                   16
#define  AC_NAME_MIN_LEN                        1
#define  AC_NAME_WITH_PRIO_MIN_LEN              2
#define  AC_TIMESTAMP_LEN                       4
#define  ADD_MAC_ACL_ENTRY_MIN_LEN              8
#define  ADD_STATION_MIN_LEN                    10
#define  CAPWAP_CTRL_IPV4_ADDR_LEN              6          
#define  CAPWAP_CTRL_IPV6_ADDR_LEN              18
#define  CAPWAP_LOCAL_IPV4_ADDR_LEN             4
#define  CAPWAP_LOCAL_IPV6_ADDR_LEN             16
#define  CAPWAP_TIMERS_LEN                      2
#define  CAPWAP_TRANSPORT_PROTOCOL_LEN          1
#define  DATA_TRANSFER_DATA_MIN_LEN             5
#define  DATA_TRANSFER_DATA_LEN                 8
#define  DATA_TRANSFER_MODE_LEN                 1             
#define  DECRYPTION_ERROR_REPORT_MIN_LEN        9
#define  DECRYPTION_ERROR_REPORT_PERIOD_LEN     3
#define  DELETE_MAC_ACL_ENTRY_MIN_LEN           8
#define  DELETE_STATION_MIN_LEN                 8
#define  DISCOVERY_TYPE_LEN                     1
#define  DUPLICATE_IPV4_ADDR_MIN_LEN            3
#define  DUPLICATE_IPV6_ADDR_MIN_LEN            3
#define  ECN_SUPPORT_LEN                        1
#define  IDLE_TIMEOUT_LEN                       4
#define  IMAGE_DATA_MIN_LEN                     1
#define  IMAGE_IDENTIFIER_MIN_LEN               5
#define  IMAGE_INFORMATION_LEN                  20
#define  INITIATE_DOWNLOAD_LEN                  0
#define  LOCATION_DATA_MIN_LEN                  1
#define  LOCATION_DATA_MAX_LEN                  1024
#define  MAX_MESGLEN_LEN                        2
#define  RADIO_ADMIN_STATE_LEN                  2
#define  RADIO_OPER_STATE_LEN                   3
#define  RESULT_CODE_LEN                        4
#define  RETURNED_MSG_ELEMENT_MIN_LEN           6
#define  SESSION_ID_LEN                         16
#define  STATS_TIMER_LEN                        2
#define  WTP_BOARD_DATA_MIN_LEN                 14
#define  WTP_DESCRIPTOR_MIN_LEN                 33
#define  WTP_FALLBACK_LEN                       1
#define  WTP_FRAME_TUNNEL_MODE_LEN              1
#define  WTP_MAC_TYPE_LEN                       1
#define  WTP_NAME_MIN_NAME                      1
#define  WTP_NAME_MAX_NAME                      512
#define  WTP_REBOOT_STATISTICS_MIN_LEN          15
#define  WTP_RADIO_STATISTICS_MIN_LEN           20
#define  WTP_STATIC_IP_ADDR_INFO_MIN_LEN        13


/* IEEE 802.11 Binding message element type */
#define IEEE_ADD_WLAN                  1024
#define IEEE_ANTENNA                   1025
#define IEEE_ASSIGNED_WTPBSSID         1026
#define IEEE_DELETE_WLAN               1027
#define IEEE_DIRECT_SEQUENCE_CONTROL   1028
#define IEEE_INFORMATION_ELEMENT       1029
#define IEEE_MAC_OPERATION             1030
#define IEEE_MIC_COUNTERMEASURES       1031
#define IEEE_MULTIDOMAIN_CAPABILITY    1032
#define IEEE_OFDM_CONTROL              1033
#define IEEE_RATE_SET                  1034
#define IEEE_RSNA_ERROR_REPORT         1035
#define IEEE_STATION                   1036
#define IEEE_STATION_QOS_PROFILE       1037
#define IEEE_STATION_SESSION_KEY       1038
#define IEEE_STATISTICS                1039
#define IEEE_SUPPORTED_RATES           1040
#define IEEE_TX_POWER                  1041
#define IEEE_TX_POWERLEVEL             1042
#define IEEE_UPDATE_STATION_QOS        1043
#define IEEE_UPDATE_WLAN               1044
#define IEEE_WTP_QOS                   1045
#define IEEE_WTP_RADIO_CONFIGURATION   1046
#define IEEE_WTP_RADIO_FAIL_ALARAM     1047
#define  WTP_RADIO_INFO                1048

/* IEEE 802.11 Message elements Minimum length */

#define MIN_IEEE_ADD_WLAN_LEN                        20
#define MIN_IEEE_ANTENNA_LEN                         5
#define IEEE_ASSIGNED_WTPBSSID_LEN                   8    
#define IEEE_DELETE_WLAN_LEN                         2
#define IEEE_DIRECT_SEQUENCE_CONTROL_LEN             8
#define MIN_IEEE_INFORMATION_ELEMENT_LEN             4
#define IEEE_INFORMATION_ELEMENT_LEN                 5
#define IEEE_INFO_ELEM_HT_CAPABILITY_LEN             26
#define IEEE_INFO_ELEM_VHT_CAPABILITY_LEN            12
#define IEEE_INFO_ELEM_VHT_OPERATION_LEN             5
#define IEEE_MAC_OPERATION_LEN                       16
#define IEEE_MIC_COUNTERMEASURES_LEN                 8
#define IEEE_MULTIDOMAIN_CAPABILITY_LEN              8
#define IEEE_OFDM_CONTROL_LEN                        8 
#define MIN_IEEE_RATE_SET_LEN                        9
#define IEEE_RSNA_ERROR_REPORT_LEN                   40
#define MIN_IEEE_STATION_LEN                         14
#define IEEE_STATION_QOS_PROFILE_LEN                 8
#define MIN_IEEE_STATION_SESSION_KEY_LEN             25
#define IEEE_STATISTICS_LEN                          80
#define MIN_IEEE_SUPPORTED_RATES_LEN                 3
#define IEEE_TX_POWER_LEN                            4
#define MIN_IEEE_TX_POWERLEVEL_LEN                   4
#define IEEE_UPDATE_STATION_QOS_LEN                  8
#define MIN_IEEE_UPDATE_WLAN_LEN                     8 
#define IEEE_WTP_QOS_LEN                             34
#define IEEE_WTP_RADIO_CONFIGURATION_LEN             16
#define IEEE_WTP_RADIO_FAIL_ALARAM_LEN               4
#define IEEE_RADIO_INFO_LEN                          5

/* Message element values */
/* To be sent in the discovery request*/

/* WTP Descriptor - possible values */
#define HARDWARE_VERSION     0
#define ACTIVE_SW_VER        1
#define BOOT_VER             2
#define OTHER_SW_VER         3

#define CAPWAP_MAX_CTRL_MSG_LEN 1000

#define CAPWAP_CONTROL_PORT 5246
#define CAPWAP_DATA_PORT    5247

/* WTP Related details */
#define WTP_ENABLE_STATUS  1
#define WTP_DISABLE_STATUS  1
#define WTP_TASKID   gWtptaskid
#define WTP_RECEIVE_EVENT    0b00001

/* Mesg element array index for different msg types*/
/* Dicovery Request - mandatory */
#define DISCOVERY_TYPE_DISC_REQ_INDEX  0
#define WTP_BOARD_DATA_DISC_REQ_INDEX  1
#define WTP_DESCRIPTOR_DISC_REQ_INDEX  2
#define WTP_FRAME_TUNNEL_MODE_DISC_REQ_INDEX 3
#define WTP_MAC_TYPE_DISC_REQ_INDEX   4
#define WTP_RADIO_INFO_DISC_REQ_INDEX 5

/* Discovery response - mandatory */
#define AC_DESCRIPTOR_DISC_RESP_INDEX  0
#define AC_NAME_DISC_RESP_INDEX        1
#define WTP_RADIO_INFO_DISC_RESP_INDEX 2
#define CAPWAP_CTRL_IP_ADDR_DISC_RESP_INDEX 3 /* Can be either IPv4 or IPv6*/
#define CAPWAP_AC_IPLIST_INDEX         4      /* AC Referal IP List */

/* Join request - mandatory */
#define LOCATION_DATA_JOIN_REQ_INDEX           0
#define WTP_BOARD_DATA_JOIN_REQ_INDEX          1
#define WTP_DESCRIPTOR_JOIN_REQ_INDEX          2
#define WTP_NAME_JOIN_REQ_INDEX                3
#define SESSION_ID_JOIN_REQ_INDEX              4
#define WTP_FRAME_TUNNEL_MODE_JOIN_REQ_INDEX   5
#define WTP_MAC_TYPE_JOIN_REQ_INDEX            6
#define WTP_RADIO_INFO_JOIN_REQ_INDEX          7
#define ECN_SUPPORT_JOIN_REQ_INDEX             8
#define CAPWAP_LOCAL_IP_ADDR_JOIN_REQ_INDEX    9

/* Join response - mandatory */
#define RESULT_CODE_JOIN_RESP_INDEX    0
#define AC_DESCRIPTOR_JOIN_RESP_INDEX  1
#define AC_NAME_JOIN_RESP_INDEX        2
#define WTP_RADIO_INFO_JOIN_RESP_INDEX 3
#define ECN_SUPPORT_JOIN_RESP_INDEX    4
#define CAPWAP_CONTRL_IP_ADDR_JOIN_RESP_INDEX 5
#define CAPWAP_LOCAL_IP_ADDR_JOIN_RESP_INDEX 6

/* Config status request - mandatory */
#define AC_NAME_CONF_STATUS_REQ_INDEX  0
#define RADIO_ADMIN_STATE_CONF_STATUS_REQ_INDEX 1
#define STATS_TIMER_CONF_STATUS_REQ_INDEX 2
#define WTP_REBOOT_STATS_CONF_STATUS_REQ_INDEX 3
#define RADIO_ANTENNA_CONF_STATUS_REQ_INDEX     4
#define RADIO_DSC_CONF_STATUS_REQ_INDEX         5
#define RADIO_MAC_CONF_STATUS_REQ_INDEX     6
#define RADIO_MDC_CONF_STATUS_REQ_INDEX     7
#define RADIO_OFDM_CONF_STATUS_REQ_INDEX    8
#define RADIO_SUPPORTED_RATE_CONF_STATUS_REQ_INDEX  9
#define RADIO_TXPOWER_CONF_STATUS_REQ_INDEX     10
#define RADIO_TXPOWER_LEVEL_CONF_STATUS_REQ_INDEX   11
#define RADIO_CONF_CONF_STATUS_REQ_INDEX        12
#define RADIO_INFO_CONF_STATUS_REQ_INDEX        13 



/* Config status request - optional */
#define AC_NAME_WITH_PRIO_CONF_STATUS_REQ_INDEX        4
#define CAPWAP_TRANS_PROTOCOL_CONF_STATUS_REQ_INDEX    5
#define WTP_STATIC_IP_ADDR_INFO_CONF_STATUS_REQ_INDEX  6
#define RADIO_INFO_ELEM_CONF_STATUS_REQ_INDEX          14
#define VENDOR_SPECIFIC_PAYLOAD_CONF_STATUS_REQ_INDEX  15

/* Config status response - mandatory */
#define CAPWAP_TIMERS_CONF_STATUS_RESP_INDEX            0
#define DECRYPT_ERR_REPORT_PERIOD_CONF_STATUS_RESP_INDEX    1
#define IDLE_TIMEOUT_CONF_STATUS_RESP_INDEX             2
#define WTP_FALLBACK_CONF_STATUS_RESP_INDEX             3
#define AC_IPV4_LIST_CONF_STATUS_RESP_INDEX             4
#define AC_IPV6_LIST_CONF_STATUS_RESP_INDEX             5
#define RADIO_ANTENNA_CONF_STATUS_RESP_INDEX            4
#define RADIO_DSC_CONF_STATUS_RESP_INDEX                5
#define RADIO_MAC_CONF_STATUS_RESP_INDEX                6
#define RADIO_MDC_CONF_STATUS_RESP_INDEX                7
#define RADIO_OFDM_CONF_STATUS_RESP_INDEX               8
#define RADIO_RATESET_CONF_STATUS_RESP_INDEX            9
#define RADIO_SUPPORTED_RATE_CONF_STATUS_RESP_INDEX         10
#define RADIO_TXPOWER_CONF_STATUS_RESP_INDEX                11
#define RADIO_QOS_CONF_STATUS_RESP_INDEX                    12
#define RADIO_CONF_CONF_STATUS_RESP_INDEX                   13


/* Config status response - optional */
#define WTP_STATIC_IP_ADDR_CONF_STATUS_RESP_INDEX 6
#define VENDOR_SPEC_PAYLOAD_CONF_STATUS_RESP_INDEX 7

/* Config Update request - message elements.All are optional */
#define AC_NAME_WITH_PRIO_CONF_UPDATE_REQ_INDEX             0
#define AC_TIMESTAMP_CONF_UPDATE_REQ_INDEX                  1
#define ADD_MAC_ACL_ENTRY_CONF_UPDATE_REQ_INDEX             2
#define CAPWAP_TIMERS_CONF_UPDATE_REQ_INDEX                 3
#define DECRYPTION_ERROR_REPORT_PERIOD_CONF_UPDATE_REQ_INDEX 4
#define DELETE_MAC_ACL_ENTRY_CONF_UPDATE_REQ_INDEX          5
#define IDLE_TIMEOUT_CONF_UPDATE_REQ_INDEX                  6
#define LOCATION_DATA_CONF_UPDATE_REQ_INDEX                 7
#define RADIO_ADMIN_STATE_CONF_UPDATE_REQ_INDEX             8
#define STATS_TIMER_CONF_UPDATE_REQ_INDEX                   9
#define WTP_FALLBACK_CONF_UPDATE_REQ_INDEX                  10
#define WTP_NAME_CONF_UPDATE_REQ_INDEX                      11
#define WTP_STATIC_IP_ADDR_INFO_CONF_UPDATE_REQ_INDEX       12
#define IMAGE_IDENTIFIER_CONF_UPDATE_REQ_INDEX              13
#define VENDOR_SPECIFIC_PAYLOAD_CONF_UPDATE_REQ_INDEX       14
#define RADIO_ANTENNA_CONF_UPDATE_REQ_INDEX                 15
#define RADIO_MAC_CONF_UPDATE_REQ_INDEX                     16
#define RADIO_DSC_CONF_UPDATE_REQ_INDEX                     17
#define RADIO_OFDM_CONF_UPDATE_REQ_INDEX                    18
#define RADIO_TXPOWER_CONF_UPDATE_REQ_INDEX                 19
#define RADIO_CONF_UPDATE_REQ_INDEX                         20
#define RADIO_MDC_CONF_UPDATE_REQ_INDEX                     21
#define RADIO_RATESET_CONF_UPDATE_REQ_INDEX                 22
#define RADIO_QOS_CONF_UPDATE_REQ_INDEX                     23

/* Change State event request- mandatory message elements */
#define RADIO_OPER_STATE_EVENT_REQ_INDEX  0
#define RESULT_CODE_STATE_EVENT_REQ_INDEX 1

/* IEEE WLAN Configuration Request - Mandatory Message Elements*/
#define IEEE_ADD_WLAN_CONF_REQ_INDEX         0
#define IEEE_DELETE_WLAN_CONF_REQ_INDEX      0
#define IEEE_UPDATE_WLAN_CONF_REQ_INDEX      0

/* IEEE WLAN Configuration Response - Mandatory Message Elements */
#define RESULT_CODE_WLAN_CONF_RSP_INDEX     0  

#define DISC_TYPE_LEN               1
#define WTP_FRAME_TUNN_MODE_LEN     1
#define WTP_MAC_TYPE_LEN            1
#define IEEE_802_11_RADIO_INFO_LEN  5


/* Maximum mandatory message elements in each message type */
#define DISCOVERY_REQ_MAND_MSG_ELEMENTS                6
#define DISCOVERY_REQ_MAX_POSSIBLE_MAND_ELEM          36 /* 5 + (1*32) radio info */
#define MAX_DISCOVERY_RES_MAND_MSG_ELEMENTS            4
#define MAX_JOIN_REQ_MAND_MSG_ELEMENTS                 10
#define MAX_JOIN_RES_MAND_MSG_ELEMENTS                 7
#define MAX_CONF_STATUS_REQ_MAND_MSG_ELEMENTS          4
#define MAX_CONF_STATUS_RES_MAND_MSG_ELEMENTS          4
#define MAX_CONF_UPDATE_RES_MAND_MSG_ELEMENTS          1
#define MAX_CHANGE_STATE_EVENT_REQ_MAND_MSG_ELEMENTS   2
#define MAX_CHANGE_STATE_EVENT_RES_MAND_MSG_ELEMENTS   0
#define MAX_WLAN_CONF_REQ_MAND_MSG_ELEMENTS             1
#define MAX_WLAN_CONF_RESP_MAND_MSG_ELEMENTS            1
#define PRIMARY_DISCOVERY_REQ_MAND_MSG_ELEMENTS         6
#define MAX_PRIMARY_DISCOVERY_RES_MAND_MSG_ELEMENTS     4

/* Message elements maximum length */ 
#define MANDATORY_BOARD_DATA_ELEMENTS                  2
#define MIN_WTP_BOARD_DATA_LEN                         14
#define MIN_WTP_DESCRIPTOR_LEN                         33
#define MAX_WTP_DESCRIPTOR_DATA_LEN                    1024
#define MAX_WLC_DESCRIPTOR_DATA_LEN                    1024
#define MIN_VENDOR_SPECIFIC_DATA_LEN                   7
#define MAX_VENDOR_SPECIFIC_DATA_LEN                   2054 


/* These Needs to put in capwapconst.h */
/* Config Update request - optional */
#define MAX_CONF_STATION_REQ_MAND_MSG_ELEMENTS          0
#define MAX_CONF_STATION_RESP_MAND_MSG_ELEMENTS         1
#define MAX_CLEAR_CONFIG_REQ_MAND_MSG_ELEMENTS          0
#define MAX_CLEAR_CONFIG_RESP_MAND_MSG_ELEMENTS         1
#define MAX_RESET_REQ_MAND_MSG_ELEMENTS                 1
#define MAX_RESET_RESP_MAND_MSG_ELEMENTS                0
#define MAX_WTP_EVENT_REQ_MAND_MSG_ELEMENTS             0
#define MAX_WTP_EVENT_RESP_MAND_MSG_ELEMENTS            0
#define MAX_DATA_REQ_MAND_MSG_ELEMENTS                  1
#define MAX_DATA_RESP_MAND_MSG_ELEMENTS                 1
#define MAX_CONF_UPDATE_REQ_MAND_MSG_ELEMENTS           0
#define MAX_CONF_UPDATE_RESP_MAND_MSG_ELEMENTS          1
#define MAX_CLEAR_CONFIG_RESP_MAND_MSG_ELEMENTS         1
#define MAX_IMAGE_DATA_RESP_MAND_MSG_ELEMENTS           1

#define  VENDOR_SPECIFIC_PAYLOAD_CONFIG_UPDATE_REQ_INDEX 0
#define  LOCATION_DATA_CONFIG_UPDATE_REQ_INDEX           1
#define  WTP_NAME_CONFIG_UPDATE_REQ_INDEX                2
#define  IMAGE_ID_CONFIG_UPDATE_REQ_INDEX                3
#define  RADIO_ADMIN_STATE_CONFIG_UPDATE_REQ_INDEX       4
#define  CAPWAP_TIMER_CONFIG_UPDATE_REQ_INDEX            5
#define  DECRYPTION_REPORT_PERIOD_CONFIG_UPDATE_REQ_INDEX 6
#define  IDLE_TIMEOUT_CONFIG_UPDATE_REQ_INDEX            7
#define  WTP_FALLOUT_CONFIG_UPDATE_REQ_INDEX             8
#define  STATS_TIMER_CONFIG_UPDATE_REQ_INDEX             9
#define  ACNAME_PRIORITY_CONFIG_UPDATE_REQ_INDEX         10
#define  DELETE_MACENTRY_CONFIG_UPDATE_REQ_INDEX         11
#define  ADD_MACENTRY_CONFIG_UPDATE_REQ_INDEX            12
#define  WTP_STATIC_IPADDRESS_CONFIG_UPDATE_REQ_INDEX    13
#define  ACTIME_STAMP_CONFIG_UPDATE_REQ_INDEX            14

/* Config Update response - Mandatory */
#define  RESULT_CODE_CONF_UPDATE_RESP_INDEX 0

/* Config Update response - Optional */

/* Echo request - Mandatory */
#define  VENDOR_SPECIFIC_PAYLOAD_ECHO_REQ_INDEX 0

/* Echo response - Mandatory */
#define  VENDOR_SPECIFIC_PAYLOAD_ECHO_RESP_INDEX 0

/* Reset request - Mandatory */
#define  IMAGE_IDENTIFIER_RESET_REQ_INDEX       0
#define VENDOR_SPECIFIC_PAYLOAD_RESET_REQ_INDEX 1


/* Config Station request - Mandatory */
#define  ADD_STATION_CONF_STATION_REQ_INDEX             0
#define  DELETE_STATION_CONF_STATION_REQ_INDEX          1
#define  VENDOR_SPECIFIC_PAYLOAD_CONF_STATION_REQ_INDEX 2

/* Config Station response - Mandatory */
#define  RESULT_CODE_CONF_STATION_RESP_INDEX     0
#define  VENDOR_SPECIFIC_CONF_STATION_RESP_INDEX 1


/* Clear Config request - Mandatory */
#define  VENDOR_SPECIFIC_CLEAR_CONFIG_REQ_INDEX 0

/* WTP Event request - Mandatory */
#define VENDOR_SPECIFIC_PAYLOAD_WTP_EVENT_REQ_INDEX     3
#define DELETE_STATION_WTP_EVENT_REQ_INDEX              0
#define WTP_REBOOT_STATS_WTP_EVENT_REQ_INDEX            1
#define DESC_ERR_REPORT_WTP_EVENT_REQ_INDEX             3
#define RADIO_STATS_WTP_EVENT_REQ_INDEX                 0
#define DUP_IPV4_ADDR_WTP_EVENT_REQ_INDEX               5
#define DUP_IPV6_ADDR_WTP_EVENT_REQ_INDEX               6
#define DOT11_STATISTICS_INDEX                          2

#define STATIC_IP_DISABLE 0
#define STATIC_IP_ENABLE  1

#define MAX_CAPWAP_PKT_LENGTH       2000
#define MAX_CAPWAP_BULK_OFFSET      200
/* 4 - QOS Length, 34 - Max Packet length of multi radio params According to 5416 RFC */
#ifdef WTP_WANTED
#define MAX_CAPWAP_GENASSEMBLERADIO (SYS_DEF_MAX_RADIO_INTERFACES * 4 * 34)
#else
#define MAX_CAPWAP_GENASSEMBLERADIO NUM_OF_AP_SUPPORTED 
#endif
#define MAX_CAPWAP_OFFSET           350
#define MAX_CAPWAP_PKTBUF           256
#define MAX_CAPWAP_DISCRESP         10
#define MAX_CAPWAP_CTRLREQ          20
#define MAX_CAPWAP_CTRLRESP         20
#define MAX_CAPWAP_PKT_QUE_SIZE     256
#define MAX_CAPWAP_DATA_TRANSFER_PKTBUF 10
#define CAPWAP_MUT_EXCL_CONF_SEM_NAME (const UINT1 *) "CAPCONFM"
#define CAPWAP_MUT_EXCL_SERVICE_SEM_NAME (const UINT1 *) "CAPSERVM"
#define CAPWAP_MUT_EXCL_RECEIVE_SEM_NAME (const UINT1 *) "CAPRCVM"
#define CAPWAP_MUT_EXCL_DISC_SEM_NAME (const UINT1 *) "CAPDISCM"
#define VENDOR_ID                   1111
#define DOT11N_VENDOR_MSG_LEN       16
#define DOT11N_STA_VENDOR_MSG_LEN   34
#define DOT11N_MCSRATE_BITMAP_SIZE  32
/* VENDOR SPECIFIC MESSAGE ELEMENT */
#define CAPWAP_VENDOR_DOMAIN_NAME_LENGTH 32
#define CAPWAP_MAX_DATA_TRANSFER_LEN  6000
#define MAX_CAPWAP_GENASSEMBLERADIO_LEN  ((800 * MAX_NUM_OF_RADIO_PER_AP) + \
               (MAX_NUM_OF_STA_PER_AP * 15))

#define IP_ADDR_ALLOC_DYNAMIC  2
typedef enum{
/* WTP Mac type */
    LOCAL_MAC,      
    SPLIT_MAC,     
    LOCAL_SPLIT_MODE  
}tWtpMacTypeVal;

#define VENDOR_SPECIFIC_MSG     100
#define MAX_VENDOR_SPECIFIC_MSG 15

#define APGROUP_ENABLE 1
#define APGROUP_DISABLE 2


#define HASH_DATA_SIZE                    32
#define IMAGE_FILE_FLASH_SIZE             128
#define IMAGE_READ_FLASH_SIZE             1024
#define VENDOR_DISC_TYPE_MSG_ELM_LEN      13
#define VENDOR_MGMT_SSID_TYPE_MSG_ELM_LEN 44
#define DISCOVERY_TYPE_VENDOR_MSG_LEN     9
#define MGMT_SSID_VENDOR_MSG_LEN          40
#define CAPWAP_WLC_IF_INTERFACE_NAME      "vlan1"
#define CAPWAP_WLC_IF_INTERFACE_LEN       6 
#define CAPWAP_WTPBOARDDATA_LEN           4
#define CAPWAP_DISCOVERY_PADDING_LEN      1024
#define CAPWAP_WTP_IF_INTERFACE_NAME      "eth0"
#define CAPWAP_WTP_IF_INTERFACE_LEN       5 
#define WTP_REBOOT_STATISTICS_MSG_LEN     15
#define TEMP_BUF_SIZE                     512
#define CAPWAP_ADD_STATION_REDUCED_LEN    5
#define CAPWAP_VENDOR_DISC_TYPE_MSG_ELM_LEN 5
#define WTP_MGMT_SSID_VENDOR_MSG_ELM_LEN    36
#define MAX_WTP_MODEL_NAME_LENGTH         256

#define   CLI_RADIO_TYPEA    2
#define   CLI_RADIO_TYPEB    1
#define   CLI_RADIO_TYPEG    4
#define   CLI_RADIO_TYPEBG   5
#define   CLI_RADIO_TYPEAN   10
#define   CLI_RADIO_TYPEBGN  13
#define   CLI_RADIO_TYPEAC   0x80000000

#define CAPWAP_UNUSED_CODE 0
#define   LEGACY_RATE_ENABLED 1
#endif
