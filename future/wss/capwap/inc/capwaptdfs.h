/********************************************************************
 *  * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 * $Id: capwaptdfs.h,v 1.1.1.1 2016/09/20 10:42:38 siva Exp $            
 *    * Description: This file contains all the typedefs used by the
 *     *              CAPWAP module
 *      *******************************************************************/
#ifndef __CAPWAPTDFS_H__
#define __CAPWAPTDFS_H__


typedef struct {
    tTMO_DLL_NODE        Link;
    tCRU_BUF_CHAIN_HEADER      *pBuf;       /* Data buffer associated 
                                               with this fragment  */
    UINT2                  u2StartOffset;  /* Starting Offset of the fragment */ 
                                             
    UINT2                  u2EndOffset;    /* End Offset of the fragment */

    UINT2                   u2FragId;       /* Fragment Id of the fragment*/
    UINT1                   au1Pad[2];
}tCapFragNode;

/* Used For sending Trap */
typedef struct {
    INT4    i4capwapBaseWtpState;
    INT4    i4fsCapwapWtpImageUpgradeStatus;
    INT4    i4fsCapwapWtpSyncStatus;
    INT4    i4capwapBaseWtpStateWtpIpAddress;
    INT4    i4capwapBaseWtpTunnelModeOptions;
    INT4    i4capwapBaseWtpMacTypeOptions;
    UINT1   au1fsCapwapWtpImageName[1024];
    UINT1   au1capwapBaseNtfWtpId[6];
    UINT1   au1Pad[2];
}tCapwapWTPTrapInfo;


#endif

