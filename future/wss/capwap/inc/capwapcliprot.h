/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: capwapcliprot.h,v 1.2 2017/11/24 10:37:01 siva Exp $
 *
 * Description: This file contains declaration of function 
 *              prototypes of capwap module.
 *******************************************************************/

#ifndef __CAPWAPPROT_H__
#define __CAPWAPPROT_H__ 
#include "wssifcapdb.h"
#include "wssifpmdb.h"
/* Add Prototypes here */

/**************************capwapmain.c*******************************/
PUBLIC VOID CapwapMainTask             PROTO ((VOID));
PUBLIC UINT4 CapwapMainTaskInit         PROTO ((VOID));
PUBLIC VOID CapwapMainDeInit            PROTO ((VOID));
PUBLIC INT4 CapwapMainTaskLock          PROTO ((VOID));
PUBLIC INT4 CapwapMainTaskUnLock        PROTO ((VOID));

INT4 CapwapUtilInitGlobals( VOID );
INT4 CapwapUtilUpdateCapwapBaseAcNameListTable PROTO ((tCapwapCapwapBaseAcNameListEntry *, tCapwapCapwapBaseAcNameListEntry *, tCapwapIsSetCapwapBaseAcNameListEntry *));


/**************************capwapque.c********************************/
PUBLIC VOID CapwapQueProcessMsgs        PROTO ((VOID));

/**************************capwappkt.c*******************************/
#if 0
PUBLIC tCapwapPkt *CapwapPktAlloc        PROTO(( VOID ));
PUBLIC VOID CapwapPktFree              PROTO(( tCapwapPkt *pCapwapPkt ));
PUBLIC UINT4  CapwapPktSend            PROTO(( tCapwapPkt *pCapwapPkt ));
PUBLIC UINT4 CapwapPktRecv             PROTO(( tCapwapPkt *pCapwapPkt));
#endif
/**************************capwaptmr.c*******************************/
#if 0
PUBLIC VOID  CapwapTmrInitTmrDesc      PROTO(( VOID));
PUBLIC VOID  CapwapTmrStartTmr         PROTO((tCapwapTmr * pCapwapTmr, 
                       enCapwapTmrId eCapwapTmrId, 
         UINT4 u4Secs));
PUBLIC VOID  CapwapTmrRestartTmr       PROTO((tCapwapTmr * pCapwapTmr, 
                 enCapwapTmrId eCapwapTmrId, 
          UINT4 u4Secs));
PUBLIC VOID  CapwapTmrStopTmr          PROTO((tCapwapTmr * pCapwapTmr));
PUBLIC VOID  CapwapTmrHandleExpiry     PROTO((VOID));

#endif
/**************************capwaptrc.c*******************************/


CHR1  *CapwapTrc           PROTO(( UINT4 u4Trc,  const char *fmt, ...));
VOID   CapwapTrcPrint      PROTO(( const char *fname, UINT4 u4Line, const char *s));
VOID   CapwapTrcWrite      PROTO(( CHR1 *s));

/**************************capwaptask.c*******************************/

PUBLIC VOID CapwapTaskSpawnCapwapTask PROTO((INT1 *pi1Arg));
PUBLIC INT4 CapwapTaskRegisterCapwapMibs (VOID);

/**************************capwapclicfg.c*******************************/
INT4 CapwapCliSetWtpProfile PROTO ((tCapwapCapwapBaseWtpProfileEntry *
                              pCapwapCapwapBaseWtpProfileEntry,
                            tCapwapIsSetCapwapBaseWtpProfileEntry *
                              pCapwapIsSetCapwapBaseWtpProfileEntry));

INT4 CapwapCliSetWtpDefaultProfile(tCapwapFsCapwapDefaultWtpProfileEntry *
                              pCapwapFsCapwapDefaultWtpProfileEntry,
                            tCapwapIsSetFsCapwapDefaultWtpProfileEntry *
                              pCapwapIsSetFsCapwapDefaultWtpProfileEntry);
INT4 CapwapSetBaseAcNameListTable (tWssIfCapDB *pWssIfCapDB);
INT4  CapwapCreateDnsProfileTable(tWssIfCapDB *pWssIfCapDB);
INT4 CapwapDestroyBaseAcNameListTable (tWssIfCapDB *pWssIfCapDB);
INT4  CapwapGetWtpStatsEntry PROTO ((tWssIfCapDB *pWssIfCapDB));
INT4 CapwapGetWtpProfileIdFromProfileMac PROTO ((tMacAddr WtpMacAddr, 
                                        UINT4 *pu4WtpProfileId));
INT4 CapwapGetBssifIndexFromStationMac (tMacAddr StaMacAddr,
                                        UINT4 *pu4BssIfIndex);
INT4   CapwapGetRadioIfIndexFromBssIfIndex(UINT4 u4BssIfIndex, 
                                      UINT4 *pu4RadioIfIndex);
INT4 CapwapGetRadioIdFromRadioIfIndex (UINT4 u4RadioIfIndex, 
                                       UINT4 *pu4RadioId, 
                                       UINT4 *pu4WtpInternalId);
INT4 CapwapGetWtpMacFromWtpInternalId ( UINT4 u4WtpInternalId, 
                                        tMacAddr *pWtpMacAddr);
INT4  CapwapGetWtpDiscStatsEntry(tWssIfPMDB *pWssIfPMDB);
INT4  CapwapGetWtpJoinStatsEntry (tWssIfPMDB *pWssIfPMDB);
INT4  CapwapGetWtpConfigStatsEntry (tWssIfPMDB *pWssIfPMDB);
INT4  CapwapGetWtpEvtRebootStatsEntry (tWssIfPMDB *pWssIfPMDB);
INT4  CapwapGetCapwATPStatsEntry (tWssIfPMDB *pWssIfPMDB);
INT4 CapwapGetWtpRunStatsEntry (tWssIfPMDB *pWssIfPMDB);
INT4 CapwapGetFsCapwapEncryptChannelRxPackets (UINT4 u4CapwapBaseWtpProfileId,
                                               INT4 i4FsCapwapEncryptChannel ,
                                               UINT4 *pu4RetValFsCapwapEncryptChannelRxPackets);
INT4  CapwapSetDnsProfileTable(tWssIfCapDB *pWssIfCapDB);
INT4 CapwapCliSetWtpRadio PROTO ((tCapwapFsWtpRadioEntry *
                          pCapwapFsWtpRadioEntry,
                          tCapwapIsSetFsWtpRadioEntry *
                          pCapwapIsSetFsWtpRadioEntry));

INT4 CapwapCliSetWtpModel PROTO ((tCapwapFsWtpModelEntry *
                          pCapwapFsWtpModelEntry,
                          tCapwapIsSetFsWtpModelEntry *
                          pCapwapIsSetFsWtpModelEntry));

INT4
CapwapCliCfgUpdateConfigRespRecv (UINT4 u4ResCode);

INT4 CapwapCreateProfileIdDB (UINT4 u4CapwapBaseWtpProfileId);

INT4 CapwapDeleteProfileIdDB (UINT4 u4CapwapBaseWtpProfileId);

INT4  CapwapGetWtpEvtRadioStatsEntry (tWssIfPMDB *pWssIfPMDB);

INT4 CapwapCliSetFsCapwTrapStatus (tCliHandle CliHandle, UINT4 *pFsCapwTrapStatus);

INT4 CapwapCliSetFsCapwCriticalTrapStatus (tCliHandle CliHandle, UINT4 *pFsCapwCriticalTrapStatus);

INT4
CapwapGetWtpProfileNameFromProfileId(UINT1 *pu1WtpProfileName,
                                      UINT4 u4WtpProfileId);


#endif   /* __CAPWAPPROT_H__ */

/*-----------------------------------------------------------------------*/
/*                       End of the file  capwapprot.h                     */
/*-----------------------------------------------------------------------*/
