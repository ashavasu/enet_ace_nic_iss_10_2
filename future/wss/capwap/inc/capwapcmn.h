/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 * $Id: capwapcmn.h,v 1.2 2017/05/23 14:16:48 siva Exp $ 
 * Description: common macros and prototypes for CAPWAP Module
 *******************************************************************/
#ifndef _CAPWAPCMN_H__
#define _CAPWAPCMN_H__

#define CAPWAP_UDP_PORT_CONTROL        5246
#define CAPWAP_UDP_PORT_DATA           CAPWAP_UDP_PORT_CONTROL+1
#define CAPWAP_DISC_MODE_AUTO           0x01
#define CAPWAP_WTP_DISC_MODE_AUTO       8
#define CAPWAP_DISC_MODE_MAC_WHITELIST  0x02
#define CAPWAP_DISC_MODE_MAC_BLACKLIST  0x04



/* Receiver Task Related Macros */
#define CAPWAP_RECV_TASK_NAME          "CAPR"
#define CAPWAP_RECV_TASK_PRIORITY       100
#define CAPWAP_RECV_STACK_SIZE          OSIX_DEFAULT_STACK_SIZE

/* Discovery Task Related Macros */
#define CAPWAP_DISC_TASK_NAME           "CAPD"
#define CAPWAP_DISC_TASK_PRIORITY       100
#define CAPWAP_DISC_STACK_SIZE          OSIX_DEFAULT_STACK_SIZE

/* Service Task Related Macros */
#define CAPWAP_SERV_TASK_NAME           "CAPS"
#define CAPWAP_SERV_TASK_PRIORITY       100
#define CAPWAP_SERV_STACK_SIZE          OSIX_DEFAULT_STACK_SIZE

/* Discovery Event List */
#define CTRL_IPV4PKT_ARRIVAL_EVENT       0x00000001
#define CTRL_IPV6PKT_ARRIVAL_EVENT       0x00000002
#define DATA_IPV4PKT_ARRIVAL_EVENT       0x00000004 
#define DATA_IPV6PKT_ARRIVAL_EVENT       0x00000008
#define CAPWAP_DISC_TMR_EXP_EVENT        0x00000010
#define CAPWAP_FSM_TMR_EXP_EVENT         0x00000020
#define CAPWAP_RUN_TMR_EXP_EVENT         0x00000040
#define CAPWAP_DISC_RX_MSGQ_EVENT        0x00000080
#define CAPWAP_CTRL_RX_MSGQ_EVENT        0x00000100
#define CAPWAP_CTRL_RX_FRAG_MSGQ_EVENT   0x00000200
#define CAPWAP_DATA_RX_MSGQ_EVENT        0x00000400
#define CAPWAP_DISC_COMPLETE_EVENT       0x00000800
#define CAPWAP_FRAG_RX_MSGQ_EVENT        0x00001000
#define CAPWAP_DATA_TX_MSGQ_EVENT        0x00002000
#define CAPWAP_DISC_RESTART_EVENT        0x00004000
#define CAP_RECV_RELINQUISH_EVENT        0x00008000
#define CAPWAP_SER_RELINQUISH_EVENT      0x00010000
#define CAPWAP_DISC_RELINQUISH_EVENT     0x00020000
#define CAPWAP_DHCP_OFFER_RCV_EVENT      0x00040000
#define MULTICAST_DATA_IPV4PKT_ARRIVAL_EVENT       0x00080000
#define CAPWAP_DATA_FRAG_RX_MSGQ_EVENT   0x00100000
#define IP_PMTUDISC_DO                   2
#define IP_PMTUDISC_PROBE                3

#ifdef WTP_WANTED
#define CAPWAP_RELINQUISH_MSG_COUNT      100
#else
#define CAPWAP_RELINQUISH_MSG_COUNT      500
#endif


#ifdef WTP_WANTED
#define CAPWAP_RELINQUISH_TIMER_VAL      2
#else
#define CAPWAP_RELINQUISH_TIMER_VAL      2
#endif

#define CAPWAP_DATA_TRANSFER_TIMER_VAL   90
#ifdef WPS_WTP_WANTED
#define CAPWAP_WPS_PBC_TIMER_VAL        120
#endif


#endif


