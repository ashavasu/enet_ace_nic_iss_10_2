/************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved                 *
 * $Id: capwapdef.h,v 1.4 2017/11/24 10:37:01 siva Exp $ 
 * DESCRIPTION    : This file contains the  definitions used for        *
 *                  CAPWAP module                                       *
 ************************************************************************/

#ifndef  __CAPWAP_DEF_H__
#define  __CAPWAP_DEF_H__

#define CAPWAP_DISC_Q_NAME             (UINT1 *) "DISQ"
#define CAPWAP_DISC_Q_LEN              30
#define CAPWAP_CTRL_Q_NAME             (UINT1 *) "CPRX"
#define CAPWAP_CTRL_Q_LEN              30
#define CAPWAP_CTRLFRAG_Q_NAME         (UINT1 *) "CPFR"
#define CAPWAP_CTRLFRAG_Q_LEN          30
#define CAPWAP_DATARX_Q_NAME           (UINT1 *) "CPDR"
#define CAPWAP_DATARX_Q_LEN            30
#define CAPWAP_DATATX_Q_NAME           (UINT1 *) "CPDT"
#define CAPWAP_DATATX_Q_LEN            30
#define CAPWAP_DATAFRAG_Q_NAME         (UINT1 *) "CPDF"
#define CAPWAP_DATAFRAG_Q_LEN          30

#define CAPWAP_PREAMBLE_VERSION             0
#define CAPWAP_PREAMBLE_DTLS_TYPE           1
#define CAPWAP_PREAMBLE_PLAINTEXT_TYPE      0

/*Trace Level*/
#define CAPWAP_MGMT_TRC                    0x00000001
#define CAPWAP_INIT_TRC                    0x00000002
#define CAPWAP_ENTRY_TRC                   0x00000004
#define CAPWAP_EXIT_TRC                    0x00000008 
#define CAPWAP_FAILURE_TRC                 0x00000010
#define CAPWAP_BUF_TRC                     0x00000020
#define CAPWAP_SESS_TRC                    0x00000040
#define CAPWAP_PKTDUMP_TRC                 0x00000080
#define CAPWAP_STATS_TRC                   0x00000100
#define CAPWAP_FSM_TRC                     0x00000200
#define CAPWAP_MAIN_TRC                    0x00000400
#define CAPWAP_UTIL_TRC                    0x00000800
#define CAPWAP_RADIO_PARM_TRC              0x00001000 
#define CAPWAP_STATION_TRC                 0x00002000 

#define   CAPWAP_RB_GREATER    1
#define   CAPWAP_RB_EQUAL      0
#define   CAPWAP_RB_LESS      -1

#define CAPWAP_RCV_TASK_LOCK   CapwapRecvMainTaskLock ()
#define CAPWAP_RCV_TASK_UNLOCK CapwapRecvMainTaskUnLock ()


#define CAPWAP_SERVICE_TASK_LOCK   CapwapServiceTaskLock ()
#define CAPWAP_SERVICE_TASK_UNLOCK CapwapServiceTaskUnLock()

#define CAPWAP_DISCOVERY_TASK_LOCK   CapwapDiscoveryTaskLock ()
#define CAPWAP_DISCOVERY_TASK_UNLOCK CapwapDiscoveryTaskUnLock()

#ifdef WTP_WANTED
#define CAPWAP_WTP_DFLT_PROF_ID            (1)
#define CAPWAP_WTP_DFLT_PROF_NAME          "default"
#define CAPWAP_WTP_DFLT_PROF_NAME_LEN      8
#define CAPWAP_WTP_DFLT_PROF_MACLEN        (18)
#define CAPWAP_WTP_DFLT_RADIO_ID           (1)
#define CAPWAP_WTP_DFLT_RADIO_IFINDEX      (1)
#define CAPWAP_WTP_MAX_NO_OF_RADIO         (1)
#endif /* WTP_WANTED */
/* CAPWAP multicast Group addresses */
#define CAPWAP_MULTICAST_ADDR           0xE000018C  /* 224.0.1.140 */       
#define CAPWAP_BROADCAST_ADDR           0xFFFFFFFF  /* 255.255.255.255 */ 
#ifdef WLC_WANTED
#define IMAGE_DATA_FLASH_CONF_LOC "/tmp/"
#define IMAGE_DATA_FLASH_LOC  "/tmp/"
#endif
#ifdef WTP_WANTED
#ifndef NPAPI_WANTED
#define IMAGE_DATA_FLASH_CONF_LOC "/tmp/"
#define IMAGE_DATA_FLASH_LOC  "/tmp/"
#endif
#endif

#define IMAGE_DATA_REMOVE_CMD    "rm -rf"
#define IMAGE_DATA_CP_CMD    "cp"
#define CAPWAP_IMAGE_BUFFER_SIZE        1050
#define CAPWAP_IMAGE_DATA_LEN           1024
#define IMAGE_DATA_FLASH_FILE_NAME_LEN  1032
#ifdef NPAPI_WANTED
#define WTP_IMAGE_ID              "WTP_IMAGE.tgz"
#else
#define WTP_IMAGE_ID              "WTP_IMAGE.tgz"
#endif
#endif

