/********************************************************************
* Copyright (C) 20132006 Aricent Inc . All Rights Reserved
*
* $Id: capwapclilwg.h,v 1.3.2.1 2018/03/19 14:21:09 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

#ifndef _CAPWAPCLILWG_
#define _CAPWAPCLILWG_

/* Proto Validate Index Instance for CapwapBaseAcNameListTable. */
INT1
nmhValidateIndexInstanceCapwapBaseAcNameListTable ARG_LIST((UINT4 ));

/* Proto Validate Index Instance for CapwapBaseMacAclTable. */
INT1
nmhValidateIndexInstanceCapwapBaseMacAclTable ARG_LIST((UINT4 ));

/* Proto Validate Index Instance for CapwapBaseWtpProfileTable. */
INT1
nmhValidateIndexInstanceCapwapBaseWtpProfileTable ARG_LIST((UINT4 ));

/* Proto Validate Index Instance for CapwapBaseWtpStateTable. */
INT1
nmhValidateIndexInstanceCapwapBaseWtpStateTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Proto Validate Index Instance for CapwapBaseWtpTable. */
INT1
nmhValidateIndexInstanceCapwapBaseWtpTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Proto Validate Index Instance for CapwapBaseWirelessBindingTable. */
INT1
nmhValidateIndexInstanceCapwapBaseWirelessBindingTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Validate Index Instance for CapwapBaseStationTable. */
INT1
nmhValidateIndexInstanceCapwapBaseStationTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Proto Validate Index Instance for CapwapBaseWtpEventsStatsTable. */
INT1
nmhValidateIndexInstanceCapwapBaseWtpEventsStatsTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Proto Validate Index Instance for CapwapBaseRadioEventsStatsTable. */
INT1
nmhValidateIndexInstanceCapwapBaseRadioEventsStatsTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ));

/* Proto Validate Index Instance for FsWtpModelTable. */
INT1
nmhValidateIndexInstanceFsWtpModelTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Proto Validate Index Instance for FsWtpRadioTable. */
INT1
nmhValidateIndexInstanceFsWtpRadioTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ));

/* Proto Validate Index Instance for FsWtpExtModelTable. */
INT1
nmhValidateIndexInstanceFsWtpExtModelTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Proto Validate Index Instance for FsWtpModelWlanBindingTable. */
INT1
nmhValidateIndexInstanceFsWtpModelWlanBindingTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , UINT4 ));

/* Proto Validate Index Instance for FsCapwapWhiteListTable. */
INT1
nmhValidateIndexInstanceFsCapwapWhiteListTable ARG_LIST((UINT4 ));

/* Proto Validate Index Instance for FsCapwapStationBlackListTable. */
INT1
nmhValidateIndexInstanceFsCapwapStationBlackListTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *));


/* Proto Validate Index Instance for FsCapwapBlackList. */
INT1
nmhValidateIndexInstanceFsCapwapBlackList ARG_LIST((UINT4 ));

/* Proto Validate Index Instance for FsCapwapWtpConfigTable. */
INT1
nmhValidateIndexInstanceFsCapwapWtpConfigTable ARG_LIST((UINT4 ));

/* Proto Validate Index Instance for FsCapwapExtWtpConfigTable. */
INT1
nmhValidateIndexInstanceFsCapwapExtWtpConfigTable ARG_LIST((UINT4 ));

/* Proto Validate Index Instance for FsCapwapLinkEncryptionTable. */
INT1
nmhValidateIndexInstanceFsCapwapLinkEncryptionTable ARG_LIST((UINT4  , INT4 ));

/* Proto Validate Index Instance for FsCawapDefaultWtpProfileTable. */
INT1
nmhValidateIndexInstanceFsCawapDefaultWtpProfileTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Proto Validate Index Instance for FsCapwapDnsProfileTable. */
INT1
nmhValidateIndexInstanceFsCapwapDnsProfileTable ARG_LIST((UINT4 ));

/* Proto Validate Index Instance for FsWtpNativeVlanIdTable. */
INT1
nmhValidateIndexInstanceFsWtpNativeVlanIdTable ARG_LIST((UINT4 ));

/* Proto Validate Index Instance for FsWtpLocalRoutingTable. */
INT1
nmhValidateIndexInstanceFsWtpLocalRoutingTable ARG_LIST((UINT4 ));

/* Proto Validate Index Instance for FsCapwapFsAcTimestampTriggerTable. */
INT1
nmhValidateIndexInstanceFsCapwapFsAcTimestampTriggerTable ARG_LIST((UINT4 ));


/* Proto Validate Index Instance for FsCawapDiscStatsTable. */
INT1
nmhValidateIndexInstanceFsCawapDiscStatsTable ARG_LIST((UINT4 ));

/* Proto Validate Index Instance for FsCawapJoinStatsTable. */
INT1
nmhValidateIndexInstanceFsCawapJoinStatsTable ARG_LIST((UINT4 ));

/* Proto Validate Index Instance for FsCawapConfigStatsTable. */
INT1
nmhValidateIndexInstanceFsCawapConfigStatsTable ARG_LIST((UINT4 ));

/* Proto Validate Index Instance for FsCawapRunStatsTable. */
INT1
nmhValidateIndexInstanceFsCawapRunStatsTable ARG_LIST((UINT4 ));

/* Proto Validate Index Instance for FsCapwapWirelessBindingTable. */
INT1
nmhValidateIndexInstanceFsCapwapWirelessBindingTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Validate Index Instance for FsCapwapStationWhiteListTable. */
INT1
nmhValidateIndexInstanceFsCapwapStationWhiteListTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *));


/* Proto Validate Index Instance for FsCapwapWtpRebootStatisticsTable. */
INT1
nmhValidateIndexInstanceFsCapwapWtpRebootStatisticsTable ARG_LIST((UINT4 ));

/* Proto Validate Index Instance for FsCapwapWtpRadioStatisticsTable. */
INT1
nmhValidateIndexInstanceFsCapwapWtpRadioStatisticsTable ARG_LIST((INT4 ));

/* Proto Validate Index Instance for FsCapwapDot11StatisticsTable. */
INT1
nmhValidateIndexInstanceFsCapwapDot11StatisticsTable ARG_LIST((INT4 ));


/* Proto Type for Low Level GET FIRST fn for CapwapBaseAcNameListTable  */

INT1
nmhGetFirstIndexCapwapBaseAcNameListTable ARG_LIST((UINT4 *));

/* Proto Type for Low Level GET FIRST fn for CapwapBaseMacAclTable  */

INT1
nmhGetFirstIndexCapwapBaseMacAclTable ARG_LIST((UINT4 *));

/* Proto Type for Low Level GET FIRST fn for CapwapBaseWtpProfileTable  */

INT1
nmhGetFirstIndexCapwapBaseWtpProfileTable ARG_LIST((UINT4 *));

/* Proto Type for Low Level GET FIRST fn for CapwapBaseWtpStateTable  */

INT1
nmhGetFirstIndexCapwapBaseWtpStateTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Proto Type for Low Level GET FIRST fn for CapwapBaseWtpTable  */

INT1
nmhGetFirstIndexCapwapBaseWtpTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Proto Type for Low Level GET FIRST fn for CapwapBaseWirelessBindingTable  */

INT1
nmhGetFirstIndexCapwapBaseWirelessBindingTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto Type for Low Level GET FIRST fn for CapwapBaseStationTable  */

INT1
nmhGetFirstIndexCapwapBaseStationTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Proto Type for Low Level GET FIRST fn for CapwapBaseWtpEventsStatsTable  */

INT1
nmhGetFirstIndexCapwapBaseWtpEventsStatsTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Proto Type for Low Level GET FIRST fn for CapwapBaseRadioEventsStatsTable  */

INT1
nmhGetFirstIndexCapwapBaseRadioEventsStatsTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *  , UINT4 *));


/* Proto Type for Low Level GET FIRST fn for FsWtpModelTable  */

INT1
nmhGetFirstIndexFsWtpModelTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Proto Type for Low Level GET FIRST fn for FsWtpRadioTable  */

INT1
nmhGetFirstIndexFsWtpRadioTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *  , UINT4 *));

/* Proto Type for Low Level GET FIRST fn for FsWtpExtModelTable  */

INT1
nmhGetFirstIndexFsWtpExtModelTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Proto Type for Low Level GET FIRST fn for FsWtpModelWlanBindingTable  */

INT1
nmhGetFirstIndexFsWtpModelWlanBindingTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *  , UINT4 * , UINT4 *));

/* Proto Type for Low Level GET FIRST fn for FsCapwapStationBlackListTable  */

INT1
nmhGetFirstIndexFsCapwapStationBlackListTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Proto Type for Low Level GET FIRST fn for FsCapwapWhiteListTable  */

INT1
nmhGetFirstIndexFsCapwapWhiteListTable ARG_LIST((UINT4 *));

/* Proto Type for Low Level GET FIRST fn for FsCapwapBlackList  */

INT1
nmhGetFirstIndexFsCapwapBlackList ARG_LIST((UINT4 *));

/* Proto Type for Low Level GET FIRST fn for FsCapwapWtpConfigTable  */

INT1
nmhGetFirstIndexFsCapwapWtpConfigTable ARG_LIST((UINT4 *));

/* Proto Type for Low Level GET FIRST fn for FsCapwapExtWtpConfigTable  */

INT1
nmhGetFirstIndexFsCapwapExtWtpConfigTable ARG_LIST((UINT4 *));

/* Proto Type for Low Level GET FIRST fn for FsCapwapLinkEncryptionTable  */

INT1
nmhGetFirstIndexFsCapwapLinkEncryptionTable ARG_LIST((UINT4 *  , INT4 *));

/* Proto Type for Low Level GET FIRST fn for FsCawapDefaultWtpProfileTable  */

INT1
nmhGetFirstIndexFsCawapDefaultWtpProfileTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Proto Type for Low Level GET FIRST fn for FsCapwapDnsProfileTable  */

INT1
nmhGetFirstIndexFsCapwapDnsProfileTable ARG_LIST((UINT4 * ));

/* Proto Type for Low Level GET FIRST fn for FsWtpNativeVlanIdTable  */

INT1
nmhGetFirstIndexFsWtpNativeVlanIdTable ARG_LIST((UINT4 *));

/* Proto Type for Low Level GET FIRST fn for FsWtpLocalRoutingTable  */

INT1
nmhGetFirstIndexFsWtpLocalRoutingTable ARG_LIST((UINT4 *));

/* Proto Type for Low Level GET FIRST fn for FsCapwapFsAcTimestampTriggerTable  */

INT1
nmhGetFirstIndexFsCapwapFsAcTimestampTriggerTable ARG_LIST((UINT4 *));


/* Proto Type for Low Level GET FIRST fn for FsCawapDiscStatsTable  */

INT1
nmhGetFirstIndexFsCawapDiscStatsTable ARG_LIST((UINT4 *));

/* Proto Type for Low Level GET FIRST fn for FsCawapJoinStatsTable  */

INT1
nmhGetFirstIndexFsCawapJoinStatsTable ARG_LIST((UINT4 *));

/* Proto Type for Low Level GET FIRST fn for FsCawapConfigStatsTable  */

INT1
nmhGetFirstIndexFsCawapConfigStatsTable ARG_LIST((UINT4 *));

/* Proto Type for Low Level GET FIRST fn for FsCawapRunStatsTable  */

INT1
nmhGetFirstIndexFsCawapRunStatsTable ARG_LIST((UINT4 *));

/* Proto Type for Low Level GET FIRST fn for FsCapwapWirelessBindingTable  */

INT1
nmhGetFirstIndexFsCapwapWirelessBindingTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto Type for Low Level GET FIRST fn for FsCapwapStationWhiteListTable  */

INT1
nmhGetFirstIndexFsCapwapStationWhiteListTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));


/* Proto Type for Low Level GET FIRST fn for FsCapwapWtpRebootStatisticsTable  */

INT1
nmhGetFirstIndexFsCapwapWtpRebootStatisticsTable ARG_LIST((UINT4 *));

/* Proto Type for Low Level GET FIRST fn for FsCapwapWtpRadioStatisticsTable  */

INT1
nmhGetFirstIndexFsCapwapWtpRadioStatisticsTable ARG_LIST((INT4 *));

/* Proto Type for Low Level GET FIRST fn for FsCapwapDot11StatisticsTable  */

INT1
nmhGetFirstIndexFsCapwapDot11StatisticsTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsWtpModelTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsWtpRadioTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsWtpExtModelTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsWtpModelWlanBindingTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsCapwapStationBlackListTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsCapwapWhiteListTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsCapwapBlackList ARG_LIST((UINT4 , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsCapwapWtpConfigTable ARG_LIST((UINT4 , UINT4 *));


/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsCapwapExtWtpConfigTable ARG_LIST((UINT4 , UINT4 *));


/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsCapwapLinkEncryptionTable ARG_LIST((UINT4, UINT4 *  , INT4 , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsCawapDefaultWtpProfileTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsCapwapDnsProfileTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsWtpNativeVlanIdTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsWtpLocalRoutingTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsCapwapFsAcTimestampTriggerTable ARG_LIST((UINT4 , UINT4 *));


/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsCawapDiscStatsTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsCawapJoinStatsTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsCawapConfigStatsTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsCawapRunStatsTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsCapwapWirelessBindingTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsCapwapStationWhiteListTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsCapwapWtpRebootStatisticsTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsCapwapWtpRadioStatisticsTable ARG_LIST((INT4 , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsCapwapDot11StatisticsTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapEnable ARG_LIST((INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapShutdown ARG_LIST((INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapControlUdpPort ARG_LIST((UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapControlChannelDTLSPolicyOptions ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapDataChannelDTLSPolicyOptions ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsWlcDiscoveryMode ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapWtpModeIgnore ARG_LIST((INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapDebugMask ARG_LIST((INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDtlsDebugMask ARG_LIST((INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDtlsEncryption ARG_LIST((INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDtlsEncryptAlogritham ARG_LIST((INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsStationType ARG_LIST((INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsAcTimestampTrigger ARG_LIST((INT4 *));

INT1
nmhGetFsApRunStateCount ARG_LIST((INT4 *));

INT1
nmhGetFsApIdleStateCount ARG_LIST((INT4 *));

INT1
nmhGetFsCapwapWssDataDebugMask ARG_LIST((INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapFragReassembleStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsCapwapApSerialNumber ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1 
nmhGetFsCapwapWtpImageName ARG_LIST ((UINT4 , tSNMP_OCTET_STRING_TYPE *));

INT1 
nmhSetFsCapwapWtpImageName ARG_LIST((UINT4 , tSNMP_OCTET_STRING_TYPE *));

INT1 
nmhTestv2FsCapwapWtpImageName ARG_LIST((UINT4 *, UINT4 ,tSNMP_OCTET_STRING_TYPE *));
/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsNoOfRadio ARG_LIST((tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapWtpMacType ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapWtpTunnelMode ARG_LIST((tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapSwVersion ARG_LIST((tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));


INT1
nmhGetFsCapwapImageName ARG_LIST((tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapQosProfileName ARG_LIST((tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMaxStations ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsWtpModelRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsCapwapHwVersion ARG_LIST((tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsWtpRadioType ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsWtpModelLocalRouting ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsWtpModelWlanBindingRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , UINT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapStationBlackListRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRadioAdminStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMaxSSIDSupported ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsWtpRadioRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapWhiteListWtpBaseMac ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapWhiteListRowStatus ARG_LIST((UINT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapBlackListWtpBaseMac ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapBlackListRowStatus ARG_LIST((UINT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapWtpReset ARG_LIST((UINT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapClearConfig ARG_LIST((UINT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsWtpDiscoveryType ARG_LIST((UINT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsWtpCountryString ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapClearApStats ARG_LIST((UINT4 ,INT4 *));
/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsWtpCrashDumpFileName ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsWtpMemoryDumpFileName ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsWtpDeleteOperation ARG_LIST((UINT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapWtpConfigRowStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsWlcIpAddressType ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsWlcIpAddress ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));


/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapEncryptChannelStatus ARG_LIST((UINT4 , INT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapEncryptChannelRowStatus ARG_LIST((UINT4 , INT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapEncryptChannelRxPackets ARG_LIST((UINT4  , INT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapDefaultWtpProfileWtpFallbackEnable ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapDefaultWtpProfileWtpEchoInterval ARG_LIST((tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapDefaultWtpProfileWtpIdleTimeout ARG_LIST((tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapDefaultWtpProfileWtpMaxDiscoveryInterval ARG_LIST((tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapDefaultWtpProfileWtpReportInterval ARG_LIST((tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapDefaultWtpProfileWtpStatisticsTimer ARG_LIST((tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapDefaultWtpProfileWtpEcnSupport ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapDefaultWtpProfileRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapDnsServerIp ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapDnsDomainName ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapDnsProfileRowStatus ARG_LIST((UINT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsWtpNativeVlanId ARG_LIST((UINT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsWtpNativeVlanIdRowStatus ARG_LIST((UINT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsWtpLocalRouting ARG_LIST((UINT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */
INT1
nmhGetFsWtpLocalRoutingRowStatus ARG_LIST((UINT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapFsAcTimestampTrigger ARG_LIST((UINT4 ,INT4 *));


/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapDiscReqReceived ARG_LIST((UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapDiscRspReceived ARG_LIST((UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapDiscReqTransmitted ARG_LIST((UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapDiscRspTransmitted ARG_LIST((UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapDiscunsuccessfulProcessed ARG_LIST((UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapDiscLastUnsuccAttemptReason ARG_LIST((UINT4 ,UINT4 * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapDiscLastSuccAttemptTime ARG_LIST((UINT4 , UINT4 * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapDiscLastUnsuccessfulAttemptTime ARG_LIST((UINT4 ,UINT4 * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapDiscStatsRowStatus ARG_LIST((UINT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapJoinReqReceived ARG_LIST((UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapJoinRspReceived ARG_LIST((UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapJoinReqTransmitted ARG_LIST((UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapJoinRspTransmitted ARG_LIST((UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapJoinunsuccessfulProcessed ARG_LIST((UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapJoinReasonLastUnsuccAttempt ARG_LIST((UINT4 ,UINT4 * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapJoinLastSuccAttemptTime ARG_LIST((UINT4 ,UINT4 * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapJoinLastUnsuccAttemptTime ARG_LIST((UINT4 ,UINT4 * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapJoinStatsRowStatus ARG_LIST((UINT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapConfigReqReceived ARG_LIST((UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapConfigRspReceived ARG_LIST((UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapConfigReqTransmitted ARG_LIST((UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapConfigRspTransmitted ARG_LIST((UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapConfigunsuccessfulProcessed ARG_LIST((UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapConfigReasonLastUnsuccAttempt ARG_LIST((UINT4 ,UINT4 * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapConfigLastSuccAttemptTime ARG_LIST((UINT4 ,UINT4 * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapConfigLastUnsuccessfulAttemptTime ARG_LIST((UINT4 ,UINT4 * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapConfigStatsRowStatus ARG_LIST((UINT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapRunConfigUpdateReqReceived ARG_LIST((UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapRunConfigUpdateRspReceived ARG_LIST((UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapRunConfigUpdateReqTransmitted ARG_LIST((UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapRunConfigUpdateRspTransmitted ARG_LIST((UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapRunConfigUpdateunsuccessfulProcessed ARG_LIST((UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapRunConfigUpdateReasonLastUnsuccAttempt ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapRunConfigUpdateLastSuccAttemptTime ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapRunConfigUpdateLastUnsuccessfulAttemptTime ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapRunStationConfigReqReceived ARG_LIST((UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapRunStationConfigRspReceived ARG_LIST((UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapRunStationConfigReqTransmitted ARG_LIST((UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapRunStationConfigRspTransmitted ARG_LIST((UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapRunStationConfigunsuccessfulProcessed ARG_LIST((UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapRunStationConfigReasonLastUnsuccAttempt ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapRunStationConfigLastSuccAttemptTime ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapRunStationConfigLastUnsuccessfulAttemptTime ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapRunClearConfigReqReceived ARG_LIST((UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapRunClearConfigRspReceived ARG_LIST((UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapRunClearConfigReqTransmitted ARG_LIST((UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapRunClearConfigRspTransmitted ARG_LIST((UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapRunClearConfigunsuccessfulProcessed ARG_LIST((UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapRunClearConfigReasonLastUnsuccAttempt ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapRunClearConfigLastSuccAttemptTime ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapRunClearConfigLastUnsuccessfulAttemptTime ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapRunDataTransferReqReceived ARG_LIST((UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapRunDataTransferRspReceived ARG_LIST((UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapRunDataTransferReqTransmitted ARG_LIST((UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapRunDataTransferRspTransmitted ARG_LIST((UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapRunDataTransferunsuccessfulProcessed ARG_LIST((UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapRunDataTransferReasonLastUnsuccAttempt ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapRunDataTransferLastSuccAttemptTime ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapRunDataTransferLastUnsuccessfulAttemptTime ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapRunResetReqReceived ARG_LIST((UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapRunResetRspReceived ARG_LIST((UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapRunResetReqTransmitted ARG_LIST((UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapRunResetRspTransmitted ARG_LIST((UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapRunResetunsuccessfulProcessed ARG_LIST((UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapRunResetReasonLastUnsuccAttempt ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapRunResetLastSuccAttemptTime ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapRunResetLastUnsuccessfulAttemptTime ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapRunPriDiscReqReceived ARG_LIST((UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapRunPriDiscRspReceived ARG_LIST((UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapRunPriDiscReqTransmitted ARG_LIST((UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapRunPriDiscRspTransmitted ARG_LIST((UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapRunPriDiscunsuccessfulProcessed ARG_LIST((UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapRunPriDiscReasonLastUnsuccAttempt ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapRunPriDiscLastSuccAttemptTime ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapRunPriDiscLastUnsuccessfulAttemptTime ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapRunEchoReqReceived ARG_LIST((UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapRunEchoRspReceived ARG_LIST((UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapRunEchoReqTransmitted ARG_LIST((UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapRunEchoRspTransmitted ARG_LIST((UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapRunEchounsuccessfulProcessed ARG_LIST((UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapRunEchoReasonLastUnsuccAttempt ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapRunEchoLastSuccAttemptTime ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapRunEchoLastUnsuccessfulAttemptTime ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapRunStatsRowStatus ARG_LIST((UINT4 ,INT4 *));

/* Proto type for Low Level GET Routine for All Objects */
INT1
nmhGetFsCapwapRunStatsKeepAliveMsgReceived (UINT4, INT4 *);

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapWirelessBindingVirtualRadioIfIndex ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapWirelessBindingType ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapWirelessBindingRowStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapStationWhiteListRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexCapwapBaseAcNameListTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexCapwapBaseMacAclTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexCapwapBaseWtpProfileTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexCapwapBaseWtpStateTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexCapwapBaseWtpTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexCapwapBaseWirelessBindingTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexCapwapBaseStationTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexCapwapBaseWtpEventsStatsTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexCapwapBaseRadioEventsStatsTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 *));


/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseWtpSessions ARG_LIST((UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseWtpSessionsLimit ARG_LIST((UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseStationSessions ARG_LIST((UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseStationSessionsLimit ARG_LIST((UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseDataChannelDTLSPolicyOptions ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseControlChannelAuthenOptions ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseAcNameListName ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseAcNameListPriority ARG_LIST((UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseAcNameListRowStatus ARG_LIST((UINT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseMacAclStationId ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseMacAclRowStatus ARG_LIST((UINT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseWtpProfileName ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseWtpProfileWtpMacAddress ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseWtpProfileWtpModelNumber ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseWtpProfileWtpName ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseWtpProfileWtpLocation ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseWtpProfileWtpStaticIpEnable ARG_LIST((UINT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseWtpProfileWtpStaticIpType ARG_LIST((UINT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseWtpProfileWtpStaticIpAddress ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseWtpProfileWtpNetmask ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseWtpProfileWtpGateway ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseWtpProfileWtpFallbackEnable ARG_LIST((UINT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseWtpProfileWtpEchoInterval ARG_LIST((UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseWtpProfileWtpIdleTimeout ARG_LIST((UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseWtpProfileWtpMaxDiscoveryInterval ARG_LIST((UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseWtpProfileWtpReportInterval ARG_LIST((UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseWtpProfileWtpStatisticsTimer ARG_LIST((UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseWtpProfileWtpEcnSupport ARG_LIST((UINT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseWtpProfileRowStatus ARG_LIST((UINT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseWtpStateWtpIpAddressType ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseWtpStateWtpIpAddress ARG_LIST((tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseWtpStateWtpLocalIpAddressType ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseWtpStateWtpLocalIpAddress ARG_LIST((tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseWtpStateWtpBaseMacAddress ARG_LIST((tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseWtpState ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseWtpStateWtpUpTime ARG_LIST((tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseWtpStateWtpCurrWtpProfileId ARG_LIST((tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapWtpRebootStatisticsRebootCount ARG_LIST((UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapWtpRebootStatisticsAcInitiatedCount ARG_LIST((UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapWtpRebootStatisticsLinkFailureCount ARG_LIST((UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapWtpRebootStatisticsSwFailureCount ARG_LIST((UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapWtpRebootStatisticsHwFailureCount ARG_LIST((UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapWtpRebootStatisticsOtherFailureCount ARG_LIST((UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapWtpRebootStatisticsUnknownFailureCount ARG_LIST((UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapWtpRebootStatisticsLastFailureType ARG_LIST((UINT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapWtpRebootStatisticsRowStatus ARG_LIST((UINT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapWtpRadioLastFailType ARG_LIST((INT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapWtpRadioResetCount ARG_LIST((INT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapWtpRadioSwFailureCount ARG_LIST((INT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapWtpRadioHwFailureCount ARG_LIST((INT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapWtpRadioOtherFailureCount ARG_LIST((INT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapWtpRadioUnknownFailureCount ARG_LIST((INT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapWtpRadioConfigUpdateCount ARG_LIST((INT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapWtpRadioChannelChangeCount ARG_LIST((INT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapWtpRadioBandChangeCount ARG_LIST((INT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapWtpRadioCurrentNoiseFloor ARG_LIST((INT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapWtpRadioStatRowStatus ARG_LIST((INT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseWtpPhyIndex ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseWtpBaseMacAddress ARG_LIST((tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseWtpTunnelModeOptions ARG_LIST((tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseWtpMacTypeOptions ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseWtpDiscoveryType ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseWtpRadiosInUseNum ARG_LIST((tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseWtpRadioNumLimit ARG_LIST((tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseWtpRetransmitCount ARG_LIST((tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseWirelessBindingType ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseStationWtpId ARG_LIST((tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseStationWtpRadioId ARG_LIST((tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseStationAddedTime ARG_LIST((tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseStationVlanName ARG_LIST((tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseWtpEventsStatsRebootCount ARG_LIST((tSNMP_OCTET_STRING_TYPE *, UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseWtpEventsStatsInitCount ARG_LIST((tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseWtpEventsStatsLinkFailureCount ARG_LIST((tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseWtpEventsStatsSwFailureCount ARG_LIST((tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseWtpEventsStatsHwFailureCount ARG_LIST((tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseWtpEventsStatsOtherFailureCount ARG_LIST((tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseWtpEventsStatsUnknownFailureCount ARG_LIST((tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseWtpEventsStatsLastFailureType ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseRadioEventsStatsResetCount ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseRadioEventsStatsSwFailureCount ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseRadioEventsStatsHwFailureCount ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseRadioEventsStatsOtherFailureCount ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseRadioEventsStatsUnknownFailureCount ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseRadioEventsStatsConfigUpdateCount ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseRadioEventsStatsChannelChangeCount ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseRadioEventsStatsBandChangeCount ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseRadioEventsStatsCurrNoiseFloor ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseRadioEventsStatsDecryptErrorCount ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */
INT1
nmhGetCapwapBaseRadioEventsStatsLastFailureType ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseAcMaxRetransmit ARG_LIST((UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseAcChangeStatePendingTimer ARG_LIST((UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseAcDataCheckTimer ARG_LIST((UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseAcDTLSSessionDeleteTimer ARG_LIST((UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseAcEchoInterval ARG_LIST((UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseAcRetransmitInterval ARG_LIST((UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseAcSilentInterval ARG_LIST((UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseAcWaitDTLSTimer ARG_LIST((UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseAcWaitJoinTimer ARG_LIST((UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseAcEcnSupport ARG_LIST((INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseFailedDTLSAuthFailureCount ARG_LIST((UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseFailedDTLSSessionCount ARG_LIST((UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseChannelUpDownNotifyEnable ARG_LIST((INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseDecryptErrorNotifyEnable ARG_LIST((INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseJoinFailureNotifyEnable ARG_LIST((INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseImageUpgradeFailureNotifyEnable ARG_LIST((INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseConfigMsgErrorNotifyEnable ARG_LIST((INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseRadioOperableStatusNotifyEnable ARG_LIST((INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCapwapBaseAuthenFailureNotifyEnable ARG_LIST((INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwapDot11TxFragmentCnt ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsCapwapDot11MulticastTxCnt ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsCapwapDot11FailedCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsCapwapDot11RetryCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsCapwapDot11MultipleRetryCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsCapwapDot11FrameDupCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsCapwapDot11RTSSuccessCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsCapwapDot11RTSFailCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsCapwapDot11ACKFailCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsCapwapDot11RxFragmentCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsCapwapDot11MulticastRxCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsCapwapDot11FCSErrCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsCapwapDot11TxFrameCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsCapwapDot11DecryptionErr ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsCapwapDot11DiscardQosFragmentCnt ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsCapwapDot11AssociatedStaCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsCapwapDot11QosCFPollsRecvdCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsCapwapDot11QosCFPollsUnusedCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsCapwapDot11QosCFPollsUnusableCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsCapwapDot11RadioId ARG_LIST((INT4 ,UINT4 *));


/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCapwapEnable ARG_LIST((INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCapwapShutdown ARG_LIST((INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCapwapControlUdpPort ARG_LIST((UINT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCapwapControlChannelDTLSPolicyOptions ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCapwapDataChannelDTLSPolicyOptions ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsWlcDiscoveryMode ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCapwapWtpModeIgnore ARG_LIST((INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCapwapDebugMask ARG_LIST((INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDtlsDebugMask ARG_LIST((INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDtlsEncryption ARG_LIST((INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDtlsEncryptAlogritham ARG_LIST((INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsStationType ARG_LIST((INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsAcTimestampTrigger ARG_LIST((INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCapwapWssDataDebugMask ARG_LIST((INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCapwapFragReassembleStatus ARG_LIST((UINT4  ,INT4 ));


/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsNoOfRadio ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCapwapWtpMacType ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCapwapWtpTunnelMode ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCapwapImageName ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCapwapQosProfileName ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMaxStations ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsWtpModelRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsWtpRadioType ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  ,UINT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsWtpModelLocalRouting ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCapwapStationBlackListRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsWtpModelWlanBindingRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , UINT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsRadioAdminStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMaxSSIDSupported ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsWtpRadioRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCapwapWhiteListId ARG_LIST((UINT4  ,UINT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCapwapWhiteListWtpBaseMac ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCapwapWhiteListRowStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCapwapBlackListId ARG_LIST((UINT4  ,UINT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCapwapBlackListWtpBaseMac ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCapwapBlackListRowStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCapwapWtpReset ARG_LIST((UINT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCapwapClearConfig ARG_LIST((UINT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsWtpDiscoveryType ARG_LIST((UINT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsWtpCountryString ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCapwapClearApStats ARG_LIST((UINT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCapwapWtpConfigRowStatus ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsWlcIpAddressType ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsWlcIpAddress ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCapwapEncryptChannel ARG_LIST((UINT4  , INT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCapwapEncryptChannelStatus ARG_LIST((UINT4  , INT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCapwapEncryptChannelRowStatus ARG_LIST((UINT4  , INT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCapwapDefaultWtpProfileModelNumber ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCapwapDefaultWtpProfileWtpFallbackEnable ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCapwapDefaultWtpProfileWtpEchoInterval ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCapwapDefaultWtpProfileWtpIdleTimeout ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCapwapDefaultWtpProfileWtpMaxDiscoveryInterval ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCapwapDefaultWtpProfileWtpReportInterval ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCapwapDefaultWtpProfileWtpStatisticsTimer ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCapwapDefaultWtpProfileWtpEcnSupport ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCapwapDefaultWtpProfileRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCapwapDnsServerIp ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCapwapDnsDomainName ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCapwapDnsProfileRowStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsWtpNativeVlanId ARG_LIST((UINT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsWtpNativeVlanIdRowStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */
INT1
nmhSetFsWtpLocalRouting ARG_LIST((UINT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */
INT1
nmhSetFsWtpLocalRoutingRowStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCapwapFsAcTimestampTrigger ARG_LIST((UINT4  ,INT4 ));



/* Low Level SET Routine for All Objects.  */
INT1
nmhSetFsCapwapDiscStatsRowStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCapwapJoinStatsRowStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCapwapConfigStatsRowStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCapwapRunStatsRowStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCapwapWirelessBindingVirtualRadioIfIndex ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCapwapWirelessBindingType ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCapwapWirelessBindingRowStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCapwapStationWhiteListRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCapwapWtpRebootStatisticsRebootCount ARG_LIST((UINT4  ,UINT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCapwapWtpRebootStatisticsAcInitiatedCount ARG_LIST((UINT4  ,UINT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCapwapWtpRebootStatisticsLinkFailureCount ARG_LIST((UINT4  ,UINT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCapwapWtpRebootStatisticsSwFailureCount ARG_LIST((UINT4  ,UINT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCapwapWtpRebootStatisticsHwFailureCount ARG_LIST((UINT4  ,UINT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCapwapWtpRebootStatisticsOtherFailureCount ARG_LIST((UINT4  ,UINT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCapwapWtpRebootStatisticsUnknownFailureCount ARG_LIST((UINT4  ,UINT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCapwapWtpRebootStatisticsLastFailureType ARG_LIST((UINT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCapwapWtpRebootStatisticsRowStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCapwapWtpRadioLastFailType ARG_LIST((INT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCapwapWtpRadioResetCount ARG_LIST((INT4  ,UINT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCapwapWtpRadioSwFailureCount ARG_LIST((INT4  ,UINT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCapwapWtpRadioHwFailureCount ARG_LIST((INT4  ,UINT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCapwapWtpRadioOtherFailureCount ARG_LIST((INT4  ,UINT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCapwapWtpRadioUnknownFailureCount ARG_LIST((INT4  ,UINT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCapwapWtpRadioConfigUpdateCount ARG_LIST((INT4  ,UINT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCapwapWtpRadioChannelChangeCount ARG_LIST((INT4  ,UINT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCapwapWtpRadioBandChangeCount ARG_LIST((INT4  ,UINT4 ));



INT1 nmhSetCapwapBaseWtpSessionsLimit (UINT4 u4SetValCapwapBaseWtpSessionsLimit);
INT1 nmhSetCapwapBaseStationSessionsLimit (UINT4
                                      u4SetValCapwapBaseStationSessionsLimit);

INT1 nmhSetCapwapBaseAcNameListPriority (UINT4 u4CapwapBaseAcNameListId,
                                    UINT4 u4SetValCapwapBaseAcNameListPriority);

INT1 nmhSetCapwapBaseAcNameListRowStatus (UINT4 u4CapwapBaseAcNameListId,
                                     INT4 i4SetValCapwapBaseAcNameListRowStatus);

INT1 nmhSetCapwapBaseAcNameListName (UINT4 u4CapwapBaseAcNameListId,
                                tSNMP_OCTET_STRING_TYPE *
                                pSetValCapwapBaseAcNameListName);

INT1 nmhSetCapwapBaseMacAclStationId (UINT4 u4CapwapBaseMacAclId,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pSetValCapwapBaseMacAclStationId);

INT1 nmhSetCapwapBaseMacAclRowStatus (UINT4 u4CapwapBaseMacAclId,
                                 INT4 i4SetValCapwapBaseMacAclRowStatus);


INT1 nmhSetCapwapBaseWtpProfileName (UINT4 u4CapwapBaseWtpProfileId,
                                tSNMP_OCTET_STRING_TYPE *
                                pSetValCapwapBaseWtpProfileName);

INT1 nmhSetCapwapBaseWtpProfileWtpMacAddress (UINT4 u4CapwapBaseWtpProfileId,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pSetValCapwapBaseWtpProfileWtpMacAddress);

INT1 nmhSetCapwapBaseWtpProfileWtpModelNumber (UINT4 u4CapwapBaseWtpProfileId,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pSetValCapwapBaseWtpProfileWtpModelNumber);

INT1 nmhSetCapwapBaseWtpProfileWtpName (UINT4 u4CapwapBaseWtpProfileId,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pSetValCapwapBaseWtpProfileWtpName);

INT1 nmhSetCapwapBaseWtpProfileWtpLocation (UINT4 u4CapwapBaseWtpProfileId,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pSetValCapwapBaseWtpProfileWtpLocation);

INT1 nmhSetCapwapBaseWtpProfileWtpStaticIpEnable (UINT4 u4CapwapBaseWtpProfileId,
                                             INT4
                                             i4SetValCapwapBaseWtpProfileWtpStaticIpEnable);
                                          
INT1 nmhSetCapwapBaseWtpProfileWtpStaticIpType (UINT4 u4CapwapBaseWtpProfileId,
                                           INT4
                                           i4SetValCapwapBaseWtpProfileWtpStaticIpType);

INT1 nmhSetCapwapBaseWtpProfileWtpStaticIpAddress (UINT4 u4CapwapBaseWtpProfileId,
                                              tSNMP_OCTET_STRING_TYPE *
                                              pSetValCapwapBaseWtpProfileWtpStaticIpAddress);


INT1 nmhSetCapwapBaseWtpProfileWtpNetmask (UINT4 u4CapwapBaseWtpProfileId,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pSetValCapwapBaseWtpProfileWtpNetmask);

INT1 nmhSetCapwapBaseWtpProfileWtpGateway (UINT4 u4CapwapBaseWtpProfileId,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pSetValCapwapBaseWtpProfileWtpGateway);


INT1 nmhSetCapwapBaseWtpProfileWtpFallbackEnable (UINT4 u4CapwapBaseWtpProfileId,
                                             INT4
                                             i4SetValCapwapBaseWtpProfileWtpFallbackEnable);

INT1 nmhSetCapwapBaseWtpProfileWtpEchoInterval (UINT4 u4CapwapBaseWtpProfileId,
                                           UINT4
                                           u4SetValCapwapBaseWtpProfileWtpEchoInterval);

INT1 nmhSetCapwapBaseWtpProfileWtpIdleTimeout (UINT4 u4CapwapBaseWtpProfileId,
                                          UINT4
                                          u4SetValCapwapBaseWtpProfileWtpIdleTimeout);

INT1 nmhSetCapwapBaseWtpProfileWtpMaxDiscoveryInterval (UINT4
                                                   u4CapwapBaseWtpProfileId,
                                                   UINT4
                                                   u4SetValCapwapBaseWtpProfileWtpMaxDiscoveryInterval);

INT1 nmhSetCapwapBaseWtpProfileWtpReportInterval (UINT4 u4CapwapBaseWtpProfileId,
                                             UINT4
                                             u4SetValCapwapBaseWtpProfileWtpReportInterval);

INT1 nmhSetCapwapBaseWtpProfileWtpStatisticsTimer (UINT4 u4CapwapBaseWtpProfileId,
                                              UINT4
                                              u4SetValCapwapBaseWtpProfileWtpStatisticsTimer);


INT1 nmhSetCapwapBaseWtpProfileWtpEcnSupport (UINT4 u4CapwapBaseWtpProfileId,
                                         INT4
                                         i4SetValCapwapBaseWtpProfileWtpEcnSupport);

INT1 nmhSetCapwapBaseWtpProfileRowStatus (UINT4 u4CapwapBaseWtpProfileId,
                                     INT4 i4SetValCapwapBaseWtpProfileRowStatus);


INT1 nmhSetCapwapBaseAcMaxRetransmit (UINT4 u4SetValCapwapBaseAcMaxRetransmit);

INT1 nmhSetCapwapBaseAcChangeStatePendingTimer (UINT4
                                           u4SetValCapwapBaseAcChangeStatePendingTimer);

INT1 nmhSetCapwapBaseAcDataCheckTimer (UINT4 u4SetValCapwapBaseAcDataCheckTimer);


INT1 nmhSetCapwapBaseAcDTLSSessionDeleteTimer (UINT4
                                          u4SetValCapwapBaseAcDTLSSessionDeleteTimer);


INT1 nmhSetCapwapBaseAcEchoInterval (UINT4 u4SetValCapwapBaseAcEchoInterval);


INT1 nmhSetCapwapBaseAcRetransmitInterval (UINT4
                                      u4SetValCapwapBaseAcRetransmitInterval);

INT1 nmhSetCapwapBaseAcSilentInterval (UINT4 u4SetValCapwapBaseAcSilentInterval);

INT1 nmhSetCapwapBaseAcWaitDTLSTimer (UINT4 u4SetValCapwapBaseAcWaitDTLSTimer);

INT1 nmhSetCapwapBaseAcWaitJoinTimer (UINT4 u4SetValCapwapBaseAcWaitJoinTimer);

INT1 nmhSetCapwapBaseAcEcnSupport (INT4 i4SetValCapwapBaseAcEcnSupport);


INT1 nmhSetCapwapBaseChannelUpDownNotifyEnable (INT4
                                           i4SetValCapwapBaseChannelUpDownNotifyEnable);



INT1 nmhSetCapwapBaseDecryptErrorNotifyEnable (INT4
                                          i4SetValCapwapBaseDecryptErrorNotifyEnable);


INT1 nmhSetCapwapBaseJoinFailureNotifyEnable (INT4
                                         i4SetValCapwapBaseJoinFailureNotifyEnable);


INT1 nmhSetCapwapBaseImageUpgradeFailureNotifyEnable (INT4
                                         i4SetValCapwapBaseImageUpgradeFailureNotifyEnable);

INT1 nmhSetCapwapBaseConfigMsgErrorNotifyEnable (INT4
                                        i4SetValCapwapBaseConfigMsgErrorNotifyEnable);


INT1 nmhSetCapwapBaseRadioOperableStatusNotifyEnable (INT4
                                         i4SetValCapwapBaseRadioOperableStatusNotifyEnable);


INT1 nmhSetCapwapBaseAuthenFailureNotifyEnable (INT4
                                           i4SetValCapwapBaseAuthenFailureNotifyEnable);



/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCapwapWtpRadioCurrentNoiseFloor ARG_LIST((INT4  ,UINT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCapwapWtpRadioStatRowStatus ARG_LIST((INT4  ,UINT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCapwapDot11TxFragmentCnt ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsCapwapDot11MulticastTxCnt ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsCapwapDot11FailedCount ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsCapwapDot11RetryCount ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsCapwapDot11MultipleRetryCount ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsCapwapDot11FrameDupCount ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsCapwapDot11RTSSuccessCount ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsCapwapDot11RTSFailCount ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsCapwapDot11ACKFailCount ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsCapwapDot11RxFragmentCount ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsCapwapDot11MulticastRxCount ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsCapwapDot11FCSErrCount ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsCapwapDot11TxFrameCount ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsCapwapDot11DecryptionErr ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsCapwapDot11DiscardQosFragmentCnt ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsCapwapDot11AssociatedStaCount ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsCapwapDot11QosCFPollsRecvdCount ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsCapwapDot11QosCFPollsUnusedCount ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsCapwapDot11QosCFPollsUnusableCount ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsCapwapDot11RadioId ARG_LIST((INT4  ,UINT4 ));



/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCapwapEnable ARG_LIST((UINT4 * , INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCapwapShutdown ARG_LIST((UINT4 * , INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCapwapControlUdpPort ARG_LIST((UINT4 * , UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCapwapControlChannelDTLSPolicyOptions ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCapwapDataChannelDTLSPolicyOptions ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsWlcDiscoveryMode ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCapwapWtpModeIgnore ARG_LIST((UINT4 * , INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCapwapDebugMask ARG_LIST((UINT4 * , INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDtlsDebugMask ARG_LIST((UINT4 * , INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDtlsEncryption ARG_LIST((UINT4 * , INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDtlsEncryptAlogritham ARG_LIST((UINT4 * , INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsStationType ARG_LIST((UINT4 * , INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsAcTimestampTrigger ARG_LIST((UINT4 * , INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCapwapWssDataDebugMask ARG_LIST((UINT4 *  ,INT4 ));


/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsNoOfRadio ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCapwapWtpMacType ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCapwapWtpTunnelMode ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCapwapImageName ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCapwapQosProfileName ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMaxStations ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsWtpModelRowStatus ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsWtpRadioType ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * , UINT4  ,UINT4 ));


/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsWtpModelLocalRouting ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsWtpModelWlanBindingRowStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCapwapStationBlackListRowStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));


/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsRadioAdminStatus ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMaxSSIDSupported ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsWtpRadioRowStatus ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCapwapWhiteListId ARG_LIST((UINT4 * , UINT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCapwapWhiteListWtpBaseMac ARG_LIST((UINT4 * , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCapwapWhiteListRowStatus ARG_LIST((UINT4 * , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCapwapBlackListId ARG_LIST((UINT4 * , UINT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCapwapBlackListWtpBaseMac ARG_LIST((UINT4 * , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCapwapBlackListRowStatus ARG_LIST((UINT4 * , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCapwapWtpReset ARG_LIST((UINT4 * , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCapwapClearConfig ARG_LIST((UINT4 * , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsWtpDiscoveryType ARG_LIST((UINT4 * , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCapwapClearApStats ARG_LIST((UINT4 * , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsWtpCountryString ARG_LIST((UINT4 * , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCapwapWtpConfigRowStatus ARG_LIST((UINT4 * , UINT4  ,INT4 ));

INT1
nmhTestv2FsWlcIpAddressType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsWlcIpAddress ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));


/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCapwapEncryptChannel ARG_LIST((UINT4 * , UINT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCapwapEncryptChannelStatus ARG_LIST((UINT4 * , UINT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCapwapEncryptChannelRowStatus ARG_LIST((UINT4 * , UINT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCapwapDefaultWtpProfileModelNumber ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCapwapDefaultWtpProfileWtpFallbackEnable ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCapwapDefaultWtpProfileWtpEchoInterval ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCapwapDefaultWtpProfileWtpIdleTimeout ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCapwapDefaultWtpProfileWtpMaxDiscoveryInterval ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCapwapDefaultWtpProfileWtpReportInterval ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCapwapDefaultWtpProfileWtpStatisticsTimer ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCapwapDefaultWtpProfileWtpEcnSupport ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCapwapDefaultWtpProfileRowStatus ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCapwapDnsServerIp ARG_LIST((UINT4 * , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCapwapDnsDomainName ARG_LIST((UINT4 * , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCapwapDnsProfileRowStatus ARG_LIST((UINT4 * , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsWtpNativeVlanId ARG_LIST((UINT4 * , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsWtpNativeVlanIdRowStatus ARG_LIST((UINT4 * , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsWtpLocalRouting ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */
INT1
nmhTestv2FsWtpLocalRoutingRowStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));
/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCapwapFsAcTimestampTrigger ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCapwapFragReassembleStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));


/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCapwapDiscStatsRowStatus ARG_LIST((UINT4 * , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCapwapJoinStatsRowStatus ARG_LIST((UINT4 * , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCapwapConfigStatsRowStatus ARG_LIST((UINT4 * , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCapwapRunStatsRowStatus ARG_LIST((UINT4 * , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCapwapWirelessBindingVirtualRadioIfIndex ARG_LIST((UINT4 * , UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCapwapWirelessBindingType ARG_LIST((UINT4 * , UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCapwapWirelessBindingRowStatus ARG_LIST((UINT4 * , UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCapwapStationWhiteListRowStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCapwapWtpRebootStatisticsRebootCount ARG_LIST((UINT4 * , UINT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCapwapWtpRebootStatisticsAcInitiatedCount ARG_LIST((UINT4 * , UINT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCapwapWtpRebootStatisticsLinkFailureCount ARG_LIST((UINT4 * , UINT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCapwapWtpRebootStatisticsSwFailureCount ARG_LIST((UINT4 * , UINT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCapwapWtpRebootStatisticsHwFailureCount ARG_LIST((UINT4 * , UINT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCapwapWtpRebootStatisticsOtherFailureCount ARG_LIST((UINT4 * , UINT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCapwapWtpRebootStatisticsUnknownFailureCount ARG_LIST((UINT4 * , UINT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCapwapWtpRebootStatisticsLastFailureType ARG_LIST((UINT4 * , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCapwapWtpRebootStatisticsRowStatus ARG_LIST((UINT4 * , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCapwapWtpRadioLastFailType ARG_LIST((UINT4 * , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCapwapWtpRadioResetCount ARG_LIST((UINT4 * , INT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCapwapWtpRadioSwFailureCount ARG_LIST((UINT4 * , INT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCapwapWtpRadioHwFailureCount ARG_LIST((UINT4 * , INT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCapwapWtpRadioOtherFailureCount ARG_LIST((UINT4 * , INT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCapwapWtpRadioUnknownFailureCount ARG_LIST((UINT4 * , INT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCapwapWtpRadioConfigUpdateCount ARG_LIST((UINT4 * , INT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCapwapWtpRadioChannelChangeCount ARG_LIST((UINT4 * , INT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCapwapWtpRadioBandChangeCount ARG_LIST((UINT4 * , INT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCapwapWtpRadioCurrentNoiseFloor ARG_LIST((UINT4 * , INT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCapwapWtpRadioStatRowStatus ARG_LIST((UINT4 * , INT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2CapwapBaseWtpSessionsLimit ARG_LIST((UINT4 * , UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2CapwapBaseStationSessionsLimit ARG_LIST((UINT4 * , UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2CapwapBaseAcNameListName ARG_LIST((UINT4 * , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2CapwapBaseAcNameListPriority ARG_LIST((UINT4 * , UINT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2CapwapBaseAcNameListRowStatus ARG_LIST((UINT4 * , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2CapwapBaseMacAclStationId ARG_LIST((UINT4 * , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2CapwapBaseMacAclRowStatus ARG_LIST((UINT4 * , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2CapwapBaseWtpProfileName ARG_LIST((UINT4 * , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2CapwapBaseWtpProfileWtpMacAddress ARG_LIST((UINT4 * , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2CapwapBaseWtpProfileWtpModelNumber ARG_LIST((UINT4 * , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2CapwapBaseWtpProfileWtpName ARG_LIST((UINT4 * , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2CapwapBaseWtpProfileWtpLocation ARG_LIST((UINT4 * , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2CapwapBaseWtpProfileWtpStaticIpEnable ARG_LIST((UINT4 * , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2CapwapBaseWtpProfileWtpStaticIpType ARG_LIST((UINT4 * , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2CapwapBaseWtpProfileWtpStaticIpAddress ARG_LIST((UINT4 * , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2CapwapBaseWtpProfileWtpNetmask ARG_LIST((UINT4 * , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2CapwapBaseWtpProfileWtpGateway ARG_LIST((UINT4 * , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2CapwapBaseWtpProfileWtpFallbackEnable ARG_LIST((UINT4 * , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2CapwapBaseWtpProfileWtpEchoInterval ARG_LIST((UINT4 * , UINT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2CapwapBaseWtpProfileWtpIdleTimeout ARG_LIST((UINT4 * , UINT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2CapwapBaseWtpProfileWtpMaxDiscoveryInterval ARG_LIST((UINT4 * , UINT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2CapwapBaseWtpProfileWtpReportInterval ARG_LIST((UINT4 * , UINT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2CapwapBaseWtpProfileWtpStatisticsTimer ARG_LIST((UINT4 * , UINT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2CapwapBaseWtpProfileWtpEcnSupport ARG_LIST((UINT4 * , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2CapwapBaseWtpProfileRowStatus ARG_LIST((UINT4 * , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2CapwapBaseAcMaxRetransmit ARG_LIST((UINT4 * , UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2CapwapBaseAcChangeStatePendingTimer ARG_LIST((UINT4 * , UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2CapwapBaseAcDataCheckTimer ARG_LIST((UINT4 * , UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2CapwapBaseAcDTLSSessionDeleteTimer ARG_LIST((UINT4 * , UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2CapwapBaseAcEchoInterval ARG_LIST((UINT4 * , UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2CapwapBaseAcRetransmitInterval ARG_LIST((UINT4 * , UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2CapwapBaseAcSilentInterval ARG_LIST((UINT4 * , UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2CapwapBaseAcWaitDTLSTimer ARG_LIST((UINT4 * , UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2CapwapBaseAcWaitJoinTimer ARG_LIST((UINT4 * , UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2CapwapBaseAcEcnSupport ARG_LIST((UINT4 * , INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2CapwapBaseChannelUpDownNotifyEnable ARG_LIST((UINT4 * , INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2CapwapBaseDecryptErrorNotifyEnable ARG_LIST((UINT4 * , INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2CapwapBaseJoinFailureNotifyEnable ARG_LIST((UINT4 * , INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2CapwapBaseImageUpgradeFailureNotifyEnable ARG_LIST((UINT4 * , INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2CapwapBaseConfigMsgErrorNotifyEnable ARG_LIST((UINT4 * , INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2CapwapBaseRadioOperableStatusNotifyEnable ARG_LIST((UINT4 * , INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2CapwapBaseAuthenFailureNotifyEnable ARG_LIST((UINT4 * , INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCapwapDot11TxFragmentCnt ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapDot11MulticastTxCnt ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapDot11FailedCount ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapDot11RetryCount ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapDot11MultipleRetryCount ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapDot11FrameDupCount ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapDot11RTSSuccessCount ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapDot11RTSFailCount ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapDot11ACKFailCount ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapDot11RxFragmentCount ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapDot11MulticastRxCount ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapDot11FCSErrCount ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapDot11TxFrameCount ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapDot11DecryptionErr ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapDot11DiscardQosFragmentCnt ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapDot11AssociatedStaCount ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapDot11QosCFPollsRecvdCount ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapDot11QosCFPollsUnusedCount ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapDot11QosCFPollsUnusableCount ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapDot11RadioId ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));



/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsCapwapEnable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsCapwapShutdown ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsCapwapControlUdpPort ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsCapwapControlChannelDTLSPolicyOptions ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsCapwapDataChannelDTLSPolicyOptions ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsWlcDiscoveryMode ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsCapwapWtpModeIgnore ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsCapwapDebugMask ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsDtlsDebugMask ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsDtlsEncryption ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsDtlsEncryptAlogritham ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsStationType ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsAcTimestampTrigger ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsCapwapWssDataDebugMask ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsWtpModelTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsWtpRadioTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsWtpExtModelTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsWtpModelWlanBindingTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsCapwapStationBlackListTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsCapwapWhiteListTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsCapwapBlackList ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsCapwapWtpConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsCapwapLinkEncryptionTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsCawapDefaultWtpProfileTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsCapwapDnsProfileTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsWtpNativeVlanIdTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsWtpLocalRoutingTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsCapwapFsAcTimestampTriggerTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsCawapDiscStatsTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsCawapJoinStatsTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsCawapConfigStatsTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsCawapRunStatsTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsCapwapWirelessBindingTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsCapwapStationWhiteListTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsCapwapWtpRebootStatisticsTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsCapwapWtpRadioStatisticsTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsCapwapDot11StatisticsTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2CapwapBaseWtpSessionsLimit ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2CapwapBaseStationSessionsLimit ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2CapwapBaseAcNameListTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2CapwapBaseMacAclTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2CapwapBaseWtpProfileTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsCapwapExtWtpConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhGetFsCapwapMaxRetransmitCount ARG_LIST((UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCapwapMaxRetransmitCount ARG_LIST((UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCapwapMaxRetransmitCount ARG_LIST((UINT4 *  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2CapwapBaseAcMaxRetransmit ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2CapwapBaseAcChangeStatePendingTimer ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2CapwapBaseAcDataCheckTimer ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2CapwapBaseAcDTLSSessionDeleteTimer ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2CapwapBaseAcEchoInterval ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2CapwapBaseAcRetransmitInterval ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2CapwapBaseAcSilentInterval ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2CapwapBaseAcWaitDTLSTimer ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2CapwapBaseAcWaitJoinTimer ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2CapwapBaseAcEcnSupport ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2CapwapBaseChannelUpDownNotifyEnable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2CapwapBaseDecryptErrorNotifyEnable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2CapwapBaseJoinFailureNotifyEnable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2CapwapBaseImageUpgradeFailureNotifyEnable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2CapwapBaseConfigMsgErrorNotifyEnable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2CapwapBaseRadioOperableStatusNotifyEnable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2CapwapBaseAuthenFailureNotifyEnable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));


/* Proto Validate Index Instance for FsCapwATPStatsTable. */
INT1
nmhValidateIndexInstanceFsCapwATPStatsTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsCapwATPStatsTable  */

INT1
nmhGetFirstIndexFsCapwATPStatsTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsCapwATPStatsTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCapwATPStatsSentBytes ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsCapwATPStatsRcvdBytes ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsCapwATPStatsWiredSentBytes ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsCapwATPStatsWirelessSentBytes ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsCapwATPStatsWiredRcvdBytes ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsCapwATPStatsWirelessRcvdBytes ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsCapwATPStatsSentTrafficRate ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsCapwATPStatsRcvdTrafficRate ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsCapwATPStatsTotalClient ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsCapwATPStatsMaxClientPerAPRadio ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsCapwATPStatsMaxClientPerSSID ARG_LIST((UINT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCapwATPStatsTotalClient ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsCapwATPStatsMaxClientPerAPRadio ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsCapwATPStatsMaxClientPerSSID ARG_LIST((UINT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCapwATPStatsTotalClient ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsCapwATPStatsMaxClientPerAPRadio ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsCapwATPStatsMaxClientPerSSID ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsCapwATPStatsTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
/* Low Level DEP Routines for.  */

INT1 
nmhGetLastUpdated ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *));

INT1 
nmhTestv2FsWtpCrashDumpFileName(UINT4 *, UINT4, tSNMP_OCTET_STRING_TYPE *);

INT1 
nmhTestv2FsWtpMemoryDumpFileName(UINT4 *, UINT4, tSNMP_OCTET_STRING_TYPE *);

INT1
nmhTestv2FsWtpDeleteOperation(UINT4 *, UINT4, INT4);

INT1 
nmhSetFsWtpCrashDumpFileName(UINT4, tSNMP_OCTET_STRING_TYPE *);

INT1 
nmhSetFsWtpMemoryDumpFileName(UINT4, tSNMP_OCTET_STRING_TYPE*);

INT1 
nmhSetFsWtpDeleteOperation(UINT4, INT4);

INT1
nmhGetFsCapwTrapStatus ARG_LIST((INT4 *));

INT1
nmhGetFsCapwCriticalTrapStatus ARG_LIST((INT4 *));
INT1
nmhSetFsCapwTrapStatus ARG_LIST((INT4 ));

INT1
nmhSetFsCapwCriticalTrapStatus ARG_LIST((INT4 ));


INT1
nmhTestv2FsCapwTrapStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsCapwCriticalTrapStatus ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsCapwapMaxRetransmitCount ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsCapwTrapStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsCapwCriticalTrapStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhValidateIndexInstanceFsCapwapDefaultWtpProfileTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhGetFsCapwapDefaultQosProfile ARG_LIST((tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhSetFsCapwapDefaultQosProfile ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsCapwapDefaultQosProfile ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhGetFsCapwapDnsAddressType ARG_LIST((UINT4 ,INT4 *));

INT1
nmhSetFsCapwapDnsAddressType ARG_LIST((UINT4  ,INT4 ));

INT1
nmhTestv2FsCapwapDnsAddressType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhSetFsCapwapDiscReqReceived ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapDiscReqReceived ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhSetFsCapwapDiscRspReceived ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapDiscRspReceived ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhSetFsCapwapDiscReqTransmitted ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapDiscReqTransmitted ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhSetFsCapwapDiscRspTransmitted ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapDiscRspTransmitted ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhSetFsCapwapDiscunsuccessfulProcessed ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapDiscunsuccessfulProcessed ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhSetFsCapwapDiscLastUnsuccAttemptReason ARG_LIST((UINT4  ,INT4 ));

INT1
nmhTestv2FsCapwapDiscLastUnsuccAttemptReason ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhSetFsCapwapDiscLastSuccAttemptTime ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapDiscLastSuccAttemptTime ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhSetFsCapwapDiscLastUnsuccessfulAttemptTime ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapDiscLastUnsuccessfulAttemptTime ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhSetFsCapwapJoinReqReceived ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapJoinReqReceived ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhSetFsCapwapJoinRspReceived ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapJoinRspReceived ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhSetFsCapwapJoinReqTransmitted ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapJoinReqTransmitted ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhSetFsCapwapJoinRspTransmitted ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapJoinRspTransmitted ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhSetFsCapwapJoinunsuccessfulProcessed ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapJoinunsuccessfulProcessed ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhSetFsCapwapJoinReasonLastUnsuccAttempt ARG_LIST((UINT4  ,INT4 ));

INT1
nmhTestv2FsCapwapJoinReasonLastUnsuccAttempt ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhSetFsCapwapJoinLastSuccAttemptTime ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapJoinLastSuccAttemptTime ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhSetFsCapwapJoinLastUnsuccAttemptTime ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapJoinLastUnsuccAttemptTime ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhSetFsCapwapConfigReqReceived ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapConfigReqReceived ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhSetFsCapwapConfigRspReceived ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapConfigRspReceived ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhSetFsCapwapConfigReqTransmitted ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapConfigReqTransmitted ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhSetFsCapwapConfigRspTransmitted ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapConfigRspTransmitted ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhSetFsCapwapConfigunsuccessfulProcessed ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapConfigunsuccessfulProcessed ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhSetFsCapwapConfigReasonLastUnsuccAttempt ARG_LIST((UINT4  ,INT4 ));

INT1
nmhTestv2FsCapwapConfigReasonLastUnsuccAttempt ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhSetFsCapwapConfigLastSuccAttemptTime ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapConfigLastSuccAttemptTime ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhSetFsCapwapConfigLastUnsuccessfulAttemptTime ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapConfigLastUnsuccessfulAttemptTime ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhSetFsCapwapRunConfigUpdateReqReceived ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapRunConfigUpdateReqReceived ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhSetFsCapwapRunConfigUpdateRspReceived ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapRunConfigUpdateRspReceived ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhSetFsCapwapRunConfigUpdateReqTransmitted ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapRunConfigUpdateReqTransmitted ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhSetFsCapwapRunConfigUpdateRspTransmitted ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapRunConfigUpdateRspTransmitted ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhSetFsCapwapRunConfigUpdateunsuccessfulProcessed ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapRunConfigUpdateunsuccessfulProcessed ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhSetFsCapwapRunConfigUpdateReasonLastUnsuccAttempt ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsCapwapRunConfigUpdateReasonLastUnsuccAttempt ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsCapwapRunConfigUpdateLastSuccAttemptTime ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsCapwapRunConfigUpdateLastSuccAttemptTime ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsCapwapRunConfigUpdateLastUnsuccessfulAttemptTime ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsCapwapRunConfigUpdateLastUnsuccessfulAttemptTime ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsCapwapRunStationConfigReqReceived ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapRunStationConfigReqReceived ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhSetFsCapwapRunStationConfigRspReceived ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapRunStationConfigRspReceived ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhSetFsCapwapRunStationConfigReqTransmitted ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapRunStationConfigReqTransmitted ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhSetFsCapwapRunStationConfigRspTransmitted ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapRunStationConfigRspTransmitted ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhSetFsCapwapRunStationConfigunsuccessfulProcessed ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapRunStationConfigunsuccessfulProcessed ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhSetFsCapwapRunStationConfigReasonLastUnsuccAttempt ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsCapwapRunStationConfigReasonLastUnsuccAttempt ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsCapwapRunStationConfigLastSuccAttemptTime ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsCapwapRunStationConfigLastSuccAttemptTime ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsCapwapRunStationConfigLastUnsuccessfulAttemptTime ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsCapwapRunStationConfigLastUnsuccessfulAttemptTime ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsCapwapRunClearConfigReqReceived ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapRunClearConfigReqReceived ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhSetFsCapwapRunClearConfigRspReceived ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapRunClearConfigRspReceived ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhSetFsCapwapRunClearConfigReqTransmitted ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapRunClearConfigReqTransmitted ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhSetFsCapwapRunClearConfigRspTransmitted ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapRunClearConfigRspTransmitted ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhSetFsCapwapRunClearConfigunsuccessfulProcessed ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapRunClearConfigunsuccessfulProcessed ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhSetFsCapwapRunClearConfigReasonLastUnsuccAttempt ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsCapwapRunClearConfigReasonLastUnsuccAttempt ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsCapwapRunClearConfigLastSuccAttemptTime ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsCapwapRunClearConfigLastSuccAttemptTime ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsCapwapRunClearConfigLastUnsuccessfulAttemptTime ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsCapwapRunClearConfigLastUnsuccessfulAttemptTime ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsCapwapRunDataTransferReqReceived ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapRunDataTransferReqReceived ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhSetFsCapwapRunDataTransferRspReceived ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapRunDataTransferRspReceived ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhSetFsCapwapRunDataTransferReqTransmitted ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapRunDataTransferReqTransmitted ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhSetFsCapwapRunDataTransferRspTransmitted ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapRunDataTransferRspTransmitted ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhSetFsCapwapRunDataTransferunsuccessfulProcessed ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapRunDataTransferunsuccessfulProcessed ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhSetFsCapwapRunDataTransferReasonLastUnsuccAttempt ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsCapwapRunDataTransferReasonLastUnsuccAttempt ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsCapwapRunDataTransferLastSuccAttemptTime ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsCapwapRunDataTransferLastSuccAttemptTime ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsCapwapRunDataTransferLastUnsuccessfulAttemptTime ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsCapwapRunDataTransferLastUnsuccessfulAttemptTime ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsCapwapRunResetReqReceived ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapRunResetReqReceived ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhSetFsCapwapRunResetRspReceived ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapRunResetRspReceived ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhSetFsCapwapRunResetReqTransmitted ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapRunResetReqTransmitted ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhSetFsCapwapRunResetRspTransmitted ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapRunResetRspTransmitted ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhSetFsCapwapRunResetunsuccessfulProcessed ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapRunResetunsuccessfulProcessed ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhSetFsCapwapRunResetReasonLastUnsuccAttempt ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsCapwapRunResetReasonLastUnsuccAttempt ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsCapwapRunResetLastSuccAttemptTime ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsCapwapRunResetLastSuccAttemptTime ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsCapwapRunResetLastUnsuccessfulAttemptTime ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsCapwapRunResetLastUnsuccessfulAttemptTime ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsCapwapRunPriDiscReqReceived ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapRunPriDiscReqReceived ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhSetFsCapwapRunPriDiscRspReceived ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapRunPriDiscRspReceived ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhSetFsCapwapRunPriDiscReqTransmitted ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapRunPriDiscReqTransmitted ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhSetFsCapwapRunPriDiscRspTransmitted ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapRunPriDiscRspTransmitted ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhSetFsCapwapRunPriDiscunsuccessfulProcessed ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapRunPriDiscunsuccessfulProcessed ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhSetFsCapwapRunPriDiscReasonLastUnsuccAttempt ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsCapwapRunPriDiscReasonLastUnsuccAttempt ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsCapwapRunPriDiscLastSuccAttemptTime ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsCapwapRunPriDiscLastSuccAttemptTime ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsCapwapRunPriDiscLastUnsuccessfulAttemptTime ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsCapwapRunPriDiscLastUnsuccessfulAttemptTime ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsCapwapRunEchoReqReceived ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapRunEchoReqReceived ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhSetFsCapwapRunEchoRspReceived ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapRunEchoRspReceived ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhSetFsCapwapRunEchoReqTransmitted ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapRunEchoReqTransmitted ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhSetFsCapwapRunEchoRspTransmitted ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapRunEchoRspTransmitted ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhSetFsCapwapRunEchounsuccessfulProcessed ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhTestv2FsCapwapRunEchounsuccessfulProcessed ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhSetFsCapwapRunEchoReasonLastUnsuccAttempt ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsCapwapRunEchoReasonLastUnsuccAttempt ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsCapwapRunEchoLastSuccAttemptTime ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsCapwapRunEchoLastSuccAttemptTime ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsCapwapRunEchoLastUnsuccessfulAttemptTime ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsCapwapRunEchoLastUnsuccessfulAttemptTime ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));


#endif
