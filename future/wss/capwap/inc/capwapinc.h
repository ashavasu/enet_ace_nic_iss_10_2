

/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved                      *
 * $Id: capwapinc.h,v 1.1.1.1 2016/09/20 10:42:38 siva Exp $ 
 * Description: This file contains the CAPWAP Packet TX related functions    *
 *****************************************************************************/

#ifndef   _CAPWAPINC_H
#define   _CAPWAPINC_H

#include "lr.h"
#include "cfa.h"
#include "ipv6.h"
#include "tcp.h"
#include "cli.h"
#include "trace.h"
#include "iss.h"
#include "vcm.h"
#include "dns.h"
#include "fssocket.h"
#include "fssyslog.h"
#define ipv6List_LEN 128
#ifdef CLKIWF_WANTED
#include "fsclk.h"
#endif

#include "utilrand.h"
#include "radioif.h"

#ifdef RFMGMT_WANTED
#include "rfmgmt.h"
#endif

#include "capwap.h"
#include "dtls.h" 
#include "capwapcliinc.h"
#include "capwaptdfs.h"
#include "capwapconst.h"
#include "capwapprot.h"
#include "capwapmacr.h"
#include "capwaptrc.h"
#include "capwapdef.h"
#include "capwapcmn.h"
#include "capwapsz.h"
#include "capwapglob.h"
#include "capwapsem.h"
#include "capwapcliprot.h"
#include "wsspm.h"
#include "wssifcapproto.h"
#endif  /* _CAPWAP_INC_H_ */

