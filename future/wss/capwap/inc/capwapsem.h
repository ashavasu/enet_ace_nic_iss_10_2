
/******************************************************************************/
/* Copyright (C) Future Software, 2005-2006                                   */
/* Licensee Future Communications Software, 2005-2006                         */
/* $Id: capwapsem.h,v 1.1.1.1 2016/09/20 10:42:38 siva Exp $                      */
/*  FILE NAME             :                                                   */
/*  PRINCIPAL AUTHOR      : FutureSoft                                        */
/*  SUBSYSTEM NAME        :                                                   */
/*  MODULE NAME           :                                                   */
/*  LANGUAGE              : C                                                 */
/*  TARGET ENVIRONMENT    : MontaVista Linux                                  */
/*  DATE OF FIRST RELEASE :                                                   */
/*  AUTHOR                :                                                   */
/*  FUNCTIONS DEFINED     :                                                   */
/*                                                                            */
/*  DESCRIPTION           :                                                   */
/******************************************************************************/
/*  Change History                                                            */
/*                                                                            */
/*  Version               :                                                   */
/*  Date(DD/MM/YYYY)      :                                                   */
/*  Modified by           :                                                   */
/*  Description of change :                                                   */
/******************************************************************************/

#ifndef __CAPWAPSEM_H__
#define __CAPWAPSEM_H__

typedef INT4 (*tFsmEntry)(tSegment *,
                          tCapwapControlPacket * , 
                          tRemoteSessionManager * );

#ifdef __CAPRMAIN_C__ 
tFsmEntry gCapwapStateMachine[CAPWAP_MAX_STATE][CAPWAP_MAX_EVENTS] = 
{
    /* STATE - START =0
     *
     */
    {
        CapwapProcessErrorHandler,                 
        CapwapProcessErrorHandler,                 
        CapwapProcessErrorHandler,                 
        CapwapProcessErrorHandler,                 
        CapwapProcessErrorHandler,                                      
        CapwapProcessErrorHandler,                                      
        CapwapProcessErrorHandler,                                      
        CapwapProcessErrorHandler,                                      
        CapwapProcessErrorHandler,                                      
        CapwapProcessErrorHandler,                                      
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
    },
    /* STATE - IDLE =1
     *
     */
    {
        CapwapProcessErrorHandler,                    
        CapwapProcessErrorHandler,                  
        CapwapProcessErrorHandler,                 
        CapwapProcessErrorHandler,                    
        CapwapProcessErrorHandler,                                      
        CapwapProcessErrorHandler,                                      
        CapwapProcessErrorHandler,                                      
        CapwapProcessErrorHandler,                                      
        CapwapProcessErrorHandler,                                      
        CapwapProcessErrorHandler,                                      
        CapwapProcessErrorHandler,                  
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                  
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                  
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
    },
    /* STATE - DISCOVERY =2
     *
     */
    {
        CapwapProcessErrorHandler,
        CapwapProcessDiscoveryRequest,
        CapwapProcessDiscoveryResponse,                
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessPrimaryDiscoveryRequest,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
    },
    /* STATE - DTLS =3
     *
     */
    {
        CapwapProcessErrorHandler,                                      
        CapwapProcessErrorHandler,                  
        CapwapProcessErrorHandler,                 
        CapwapProcessJoinRequest,                       
        CapwapProcessErrorHandler,                                      
        CapwapProcessErrorHandler,                                      
        CapwapProcessErrorHandler,                                      
        CapwapProcessErrorHandler,                                      
        CapwapProcessErrorHandler,                                      
        CapwapProcessErrorHandler,                                      
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                  
        CapwapProcessErrorHandler,                  
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                  
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                  
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
    },
    /* STATE - JOIN =4
     *
     */
    {
        CapwapProcessErrorHandler, 
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessJoinResponse,
        CapwapProcessConfigStatusRequest,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessImageDataReq,
        CapwapProcessImageDataReq,
        CapwapProcessImageDataReq,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler
    },
    /* STATE - CONFIGURE = 5
     *
     */
    {
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessConfigStatusResponse,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessChangeStateEvntRequest,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler
    },
    /* STATE - DATACHECK = 6
     *
     */
    {
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,   
        CapwapProcessChangeStateEvntResponse,          
        CapwapProcessErrorHandler,                  
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler                   
    },
    /* STATE - RUN = 7
     *
     */
    {
        CapwapProcessErrorHandler, 
        CapwapProcessErrorHandler, 
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessConfigUpdateRequest,
        CapwapProcessConfigUpdateResp,
        CapwapProcessWtpEventRequest,
        CapwapProcessWtpEventResponse,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessEchoRequest,
        CapwapProcessEchoResponse,
        CapwapProcessImageDataReq,
        CapwapProcessImageDataRsp,        
        CapwapProcessResetRequest,
        CapwapProcessResetResponse,
        CapwapProcessPrimaryDiscoveryRequest,
        CapwapProcessPrimaryDiscoveryResponse,
        CapwapProcessDataTransferRequest,
        CapwapProcessDataTransferResp,
        CapwapProcessClearConfigRequest,
        CapwapProcessClearConfigResp,
        CapwapProcessConfigStationReq,
        CapwapProcessConfigStationResp,
        CapwapProcessWlanConfigReq,
        CapwapProcessWlanConfigRsp
    },
    /* STATE - IMAGE DATA = 8
     *
     */
    {
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessEchoRequest,
        CapwapProcessEchoResponse,
        CapwapProcessImageDataReq,
        CapwapProcessImageDataRsp,
        CapwapProcessResetResponse,
        CapwapProcessResetResponse,
        CapwapProcessResetResponse,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
    },
    /* STATE - RESET =9
     *
     */
    {
        CapwapProcessErrorHandler,                 
        CapwapProcessErrorHandler,                 
        CapwapProcessErrorHandler,                 
        CapwapProcessErrorHandler,                 
        CapwapProcessErrorHandler,                                      
        CapwapProcessErrorHandler,                                      
        CapwapProcessErrorHandler,                                      
        CapwapProcessErrorHandler,                                      
        CapwapProcessErrorHandler,                                      
        CapwapProcessErrorHandler,                                      
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
    },
    /* STATE - SULKING =10
     *
     */
    {
        CapwapProcessErrorHandler,                    
        CapwapProcessErrorHandler,                  
        CapwapProcessErrorHandler,                 
        CapwapProcessErrorHandler,                    
        CapwapProcessErrorHandler,                                      
        CapwapProcessErrorHandler,                                      
        CapwapProcessErrorHandler,                                      
        CapwapProcessErrorHandler,                                      
        CapwapProcessErrorHandler,                                      
        CapwapProcessErrorHandler,                                      
        CapwapProcessErrorHandler,                  
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                  
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                  
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,                   
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
    },
    /* STATE - DTLSTD =11
     *
     */
    {
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
    },
    /* STATE - DEAD =12
     *
     */
    {
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
        CapwapProcessErrorHandler,
    }
};


#else
extern tFsmEntry gCapwapStateMachine[CAPWAP_MAX_STATE][CAPWAP_MAX_EVENTS];
#endif

#endif
