/************************************************************************
 * $Id: capwapglob.h,v 1.3 2017/11/24 10:37:02 siva Exp $
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 * *
 * FILE NAME      : capwapglob.h                                           *
 * LANGUAGE       : C Language                                          *
 * TARGET         : LINUX                                               *
 * AUTHOR         : 
 * *
 * DESCRIPTION    : global vaiables used for Capwap module
 * *
 ***********************************************************************/

#ifndef  __CAPWAP_GLOB_H
#define  __CAPWAP_GLOB_H
#ifdef  __CAPRMAIN_C__

#include "capwap.h"

UINT4 gu4CapwapSysLogId;
UINT4                gu4CapwapDebugMask; 
UINT2  gu2MaxRetransmitCount = CAPWAP_MAX_RETRANSMITCOUNT;
tCapwapStats        CapwapStats[MAX_WTP_SUPPORTED];

UINT2  gu2CapwapMandMsgElemCnt[CAPWAP_MAX_EVENTS + 1] =
{
    0,
    DISCOVERY_REQ_MAND_MSG_ELEMENTS,                /* Disc Req */
    MAX_DISCOVERY_RES_MAND_MSG_ELEMENTS,            /* Disc Rsp */
    MAX_JOIN_REQ_MAND_MSG_ELEMENTS,                 /* Join Req */
    MAX_JOIN_RES_MAND_MSG_ELEMENTS,                 /* Join Rsp */
    MAX_CONF_STATUS_REQ_MAND_MSG_ELEMENTS,          /* Conf Status Req */
    MAX_CONF_STATUS_RES_MAND_MSG_ELEMENTS,          /* Conf Status Rsp */
    0,                                              /* Conf Upd Req */
    MAX_CONF_UPDATE_RES_MAND_MSG_ELEMENTS,          /* Conf Upd Rsp */
    MAX_WTP_EVENT_REQ_MAND_MSG_ELEMENTS,            /* WTP Event Req */
    MAX_WTP_EVENT_RESP_MAND_MSG_ELEMENTS,           /* WTP Event Rsp */
    MAX_CHANGE_STATE_EVENT_REQ_MAND_MSG_ELEMENTS,   /* Change State Req */
    0,                                              /* Change State Rsp */
    0,                                              /* Echo Req */
    0,                                              /* Echo Rsp */
    0,                                              /* Image Data Req */
    MAX_IMAGE_DATA_RESP_MAND_MSG_ELEMENTS,          /* Image Data Rsp */
    MAX_RESET_REQ_MAND_MSG_ELEMENTS,                /* Reset Req */
    MAX_RESET_RESP_MAND_MSG_ELEMENTS,               /* Reset Rsp */
    PRIMARY_DISCOVERY_REQ_MAND_MSG_ELEMENTS,        /* Prim Disc Req */
    MAX_PRIMARY_DISCOVERY_RES_MAND_MSG_ELEMENTS,    /* Prim Disc Rsp */
    MAX_DATA_REQ_MAND_MSG_ELEMENTS,                 /* Data Trans Req */
    MAX_DATA_RESP_MAND_MSG_ELEMENTS,                /* Data Trans Rsp */
    0,                                              /* Clear Conf Req */
    MAX_CLEAR_CONFIG_RESP_MAND_MSG_ELEMENTS,        /* Clear Conf Rsp */
    0,                                              /* Station Conf Req */
    MAX_CONF_STATION_RESP_MAND_MSG_ELEMENTS,        /* Station Conf Rsp */
    MAX_WLAN_CONF_REQ_MAND_MSG_ELEMENTS,            /* Wlan Conf Req */
    MAX_WLAN_CONF_RESP_MAND_MSG_ELEMENTS,           /* Wlan Conf Rsp */
    0
};

UINT2 
gu2CapwapMsgTypeToElemMap[CAPWAP_MAX_EVENTS + 1]
                         [CAPWAP_MSG_PRESENCE_TYPE][CAPWAP_MAX_MSG_ELEMS] = 
{
    { 
        { 
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,            
            0, 0, 0, 0, 0
        },

        {
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0
        }
    },

    /* Disc Req */
    { 
        /* Disc Req Mandatory Elems */
        {  
            DISCOVERY_TYPE, 
            WTP_BOARD_DATA, 
            WTP_DESCRIPTOR,
            WTP_FRAME_TUNNEL_MODE,
            WTP_MAC_TYPE,
            WTP_RADIO_INFO,
            0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0
        },

        /* Disc Req Optional Elems */
        {
            MTU_DISC_PADDING,
            VENDOR_SPECIFIC_PAYLOAD,
            0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0
        }    
    },

    /* Disc Rsp */
    {
        /* Disc Rsp Mandatory Elems */
        {
            AC_DESCRIPTOR,
            AC_NAME,
            WTP_RADIO_INFO,
            CAPWAP_CTRL_IPV4_ADDR | CAPWAP_CTRL_IPV6_ADDR,
            0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0
        },

        /* Disc Rsp Optional Elems */
        {
            AC_IPv4_LIST,
            VENDOR_SPECIFIC_PAYLOAD,
            0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0
        }
    },

    /* Join Req */
    {
        /* Join Req Mandatory Elems */
        {
            LOCATION_DATA,
            WTP_BOARD_DATA,
            WTP_DESCRIPTOR,
            WTP_NAME,
            SESSION_ID,
            WTP_FRAME_TUNNEL_MODE,
            WTP_MAC_TYPE,
            WTP_RADIO_INFO,
            ECN_SUPPORT,
            CAPWAP_LOCAL_IPV4_ADDR | CAPWAP_LOCAL_IPV6_ADDR,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0
        },

        /* Join Req Optional Elems */
        {
            CAPWAP_TRANSPORT_PROTOCOL,
            MAX_MESG_LEN,
            WTP_REBOOT_STATISTICS,
            VENDOR_SPECIFIC_PAYLOAD,
            0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0
        }
    },

    /* Join Rsp */
    {
        /* Join Rsp Mandatory Elems */
        {
            RESULT_CODE,
            AC_DESCRIPTOR,
            AC_NAME,
            WTP_RADIO_INFO,
            ECN_SUPPORT,
            CAPWAP_CTRL_IPV4_ADDR | CAPWAP_CTRL_IPV6_ADDR,
            CAPWAP_LOCAL_IPV4_ADDR | CAPWAP_LOCAL_IPV6_ADDR,
            0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0
        },

        /* Join Rsp Optional Elems */
        {
            AC_IPv4_LIST,
            AC_IPv6_LIST,
            CAPWAP_TRANSPORT_PROTOCOL,
            IMAGE_IDENTIFIER,
            MAX_MESG_LEN,
            VENDOR_SPECIFIC_PAYLOAD,
            0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0
        }
    },

    /* Conf Status Req */
    {
        /* Conf Status Req Mandatory Elems */
        {
            AC_NAME,
            RADIO_ADMIN_STATE,
            STATS_TIMER,
            WTP_REBOOT_STATISTICS,
            0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0
        },

        /* Conf Status Req Optional Elems */
        {
            AC_NAME_WITH_PRIO,
            CAPWAP_TRANSPORT_PROTOCOL,
            WTP_STATIC_IP_ADDR_INFO,
            IEEE_ANTENNA,
            IEEE_DIRECT_SEQUENCE_CONTROL,
            IEEE_INFORMATION_ELEMENT,
            IEEE_MAC_OPERATION,
            IEEE_MULTIDOMAIN_CAPABILITY,
            IEEE_OFDM_CONTROL,
            IEEE_SUPPORTED_RATES,
            IEEE_TX_POWER,
            IEEE_TX_POWERLEVEL,
            IEEE_WTP_RADIO_CONFIGURATION,
            WTP_RADIO_INFO, 
            VENDOR_SPECIFIC_PAYLOAD,
            0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0
        }
    },

    /* Conf Status Rsp */
    {
        /* Conf Status Rsp Mandatory Elems */
        {
            CAPWAP_TIMERS,
            DECRYPTION_ERROR_REPORT_PERIOD,
            IDLE_TIMEOUT,
            WTP_FALLBACK,
            AC_IPv4_LIST | AC_IPv6_LIST,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0
        },

        /* Conf Status Rsp Optional Elems */
        {
            AC_NAME_WITH_PRIO,
            WTP_STATIC_IP_ADDR_INFO,
            IEEE_ANTENNA,
            IEEE_DIRECT_SEQUENCE_CONTROL,
            IEEE_INFORMATION_ELEMENT,
            IEEE_MAC_OPERATION,
            IEEE_MULTIDOMAIN_CAPABILITY,
            IEEE_OFDM_CONTROL,
            IEEE_RATE_SET,
            IEEE_SUPPORTED_RATES,
            IEEE_TX_POWER,
            IEEE_WTP_QOS,
            IEEE_WTP_RADIO_CONFIGURATION,
            VENDOR_SPECIFIC_PAYLOAD,
            0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0
        }
    },

    /* Conf Update Req */
    {
        /* Conf Update Req Mandatory Elems */
        {
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0
        },

        /* Conf Update Req Optional Elems */
        {
            AC_NAME_WITH_PRIO,
            AC_TIMESTAMP,
            ADD_MAC_ACL_ENTRY,           
            CAPWAP_TIMERS,
            DECRYPTION_ERROR_REPORT_PERIOD,
            DELETE_MAC_ACL_ENTRY,
            IDLE_TIMEOUT,
            LOCATION_DATA,
            RADIO_ADMIN_STATE,
            STATS_TIMER,
            WTP_FALLBACK,
            WTP_NAME,
            WTP_STATIC_IP_ADDR_INFO,
            IMAGE_IDENTIFIER,
            IEEE_ANTENNA,
            IEEE_DIRECT_SEQUENCE_CONTROL,
            IEEE_MAC_OPERATION,
            IEEE_MULTIDOMAIN_CAPABILITY,
            IEEE_OFDM_CONTROL,
            IEEE_RATE_SET,
            IEEE_RSNA_ERROR_REPORT,
            IEEE_TX_POWER,
            IEEE_WTP_QOS,
            IEEE_WTP_RADIO_CONFIGURATION,
            IEEE_INFORMATION_ELEMENT,
            VENDOR_SPECIFIC_PAYLOAD
        }
    },

    /* Conf Update Rsp */
    {
        /* Conf Update Rsp Mandatory Elems */
        {
            RESULT_CODE,
            0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0
        },

        /* Conf Update Rsp Optional Elems */
        {
            RADIO_OPER_STATE,           
            VENDOR_SPECIFIC_PAYLOAD,
            0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0
        }
    },

    /* WTP Event Req */
    {
        /* WTP Event Req Mandatory Elems */
        {
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0
        },

        /* WTP Event Req Optional Elems */
        {
            DECRYPTION_ERROR_REPORT,
            DUPLICATE_IPV4_ADDR,
            DUPLICATE_IPV6_ADDR,
            WTP_RADIO_STATISTICS,
            WTP_REBOOT_STATISTICS,
            DELETE_STATION,
            IEEE_MIC_COUNTERMEASURES,
            IEEE_RSNA_ERROR_REPORT,
            IEEE_STATISTICS, 
            VENDOR_SPECIFIC_PAYLOAD,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0
        }
    },

    /* WTP Event Rsp */
    {
        /* WTP Event Rsp Mandatory Elems */
        {
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0
        },

        /* WTP Event Rsp Optional Elems */
        {
            VENDOR_SPECIFIC_PAYLOAD,
            0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0
        }
    },

    /* Change State Event Req */
    {
        /* Change State Event Req Mandatory Elems */
        {
            RADIO_OPER_STATE,
            RESULT_CODE,
            0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0
        },

        /* Change State Event Req Optional Elems */
        {
            RETURNED_MSG_ELEMENT,
            IEEE_WTP_RADIO_FAIL_ALARAM,
            VENDOR_SPECIFIC_PAYLOAD,
            0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0
        }
    },

    /* Change State Event Rsp */
    {
        /* Change State Event Rsp Mandatory Elems */
        {
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0
        },

        {
            VENDOR_SPECIFIC_PAYLOAD,
            0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0
        }
    },

    /* Echo Req */
    {
        /* Echo Req Mandatory Elems */
        {
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0
        },

        /* Echo Req Optional Elems */
        {
            VENDOR_SPECIFIC_PAYLOAD,
            0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0
        }
    },

    /* Echo Rsp */
    {
        /* Echo Rsp Mandatory Elems */
        {
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0
        },

        /* Echo Rsp Optional Elems */
        {
            VENDOR_SPECIFIC_PAYLOAD,
            0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0
        }
    },

    /* Image Data Req */
    {
        /* Image Data Req Mandatory Elems */
        {
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0
        },

        /* Image Data Req Optional Elems */
        {
            CAPWAP_TRANSPORT_PROTOCOL,
            IMAGE_DATA,
            IMAGE_IDENTIFIER,
            INITIATE_DOWNLOAD,
            VENDOR_SPECIFIC_PAYLOAD,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0
        }
    },

    /* Image Data Rsp */
    {
        /* Image Data Rsp Mandatory Elems */
        {
            RESULT_CODE,
            0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0
        },

        /* Image Data Rsp Optional Elems */
        {
            IMAGE_INFORMATION,
            VENDOR_SPECIFIC_PAYLOAD,
            0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0
        }
    },

    /* Reset Req */
    {
        /* Reset Req Mandatory Elems */
        {
            IMAGE_IDENTIFIER,
            0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0
        },

        /* Reset Req Optional Elems */
        {
            VENDOR_SPECIFIC_PAYLOAD,
            0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0
        }
    },

    /* Reset Rsp */
    {
        /* Reset Rsp Mandatory Elems */
        {
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0
        },

        /* Reset Rsp Optional Elems */
        {
            RESULT_CODE,
            VENDOR_SPECIFIC_PAYLOAD,
            0, 0, 0, 
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0
        }
    },

    /* Primary Disc Req */
    {
        /* Primary Disc Req Mandatory Elems */
        {
            DISCOVERY_TYPE,
            WTP_BOARD_DATA,
            WTP_DESCRIPTOR,
            WTP_FRAME_TUNNEL_MODE,
            WTP_MAC_TYPE,
            WTP_RADIO_INFO,
            0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0
        },

        /* Primary Disc Req Optional Elems */
        {
            MTU_DISC_PADDING,
            VENDOR_SPECIFIC_PAYLOAD,
            0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0
        }
    },

    /* Primary Disc Rsp */
    {
        /* Primary Disc Rsp Mandatory Elems */
        {
            AC_DESCRIPTOR,
            AC_NAME,
            WTP_RADIO_INFO,
            CAPWAP_CTRL_IPV4_ADDR | CAPWAP_CTRL_IPV6_ADDR,
            0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0
        },

        /* Primary Disc Rsp Optional Elems */
        {
            AC_IPv4_LIST,
            VENDOR_SPECIFIC_PAYLOAD,
            0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0
        }
    },

    /* Data Transfer Req */
    {
        /* Data Transfer Req Mandatory Elems */
        {
            DATA_TRANSFER_MODE | DATA_TRANSFER_DATA,
            0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0
        },

        /* Data Transfer Req Optional Elems */
        {
            VENDOR_SPECIFIC_PAYLOAD,
            0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0
        }
    },

    /* Data Transfer Rsp */
    {
        /* Data Transfer Rsp Mandatory Elems */
        {
            RESULT_CODE,
            0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0
        },

        /* Data Transfer Rsp Optional Elems */
        {
            VENDOR_SPECIFIC_PAYLOAD,
            0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0
        }
    },

    /* Clear Config Req */
    {
        /* Clear Config Req Mandatory Elems */
        {
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0
        },

        /* Clear Config Req Optional Elems */
        {
            VENDOR_SPECIFIC_PAYLOAD,
            0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0
        }
    },

    /* Clear Config Rsp */
    {
        /* Clear Config Rsp Mandatory Elems */
        {
            RESULT_CODE,
            0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0
        },

        /* Clear Config Rsp Optional Elems */
        {
            VENDOR_SPECIFIC_PAYLOAD,
            0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0
        }
    },

    /* Station Config Req */
    {
        /* Station Config Req Mandatory Elems */
        {
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0
        },

        /* Station Config Req Optional Elems */
        {
            ADD_STATION,
            DELETE_STATION,
            IEEE_STATION,
            IEEE_STATION_SESSION_KEY,
            IEEE_STATION_QOS_PROFILE,
            IEEE_UPDATE_STATION_QOS,
            VENDOR_SPECIFIC_PAYLOAD,
            0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0
        }
    },

    /* Station Config Rsp */
    {
        /* Station Config Rsp Mandatory Elems */
        {
            RESULT_CODE,
            0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0
        },

        /* Station Config Rsp Optional Elems */
        {
            VENDOR_SPECIFIC_PAYLOAD,
            0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0
        }
    },

    /* WLAN Config Req */
    {
        /* WLAN Config Req Mandatory Elems */
        {
            IEEE_ADD_WLAN | IEEE_UPDATE_WLAN | IEEE_DELETE_WLAN,
            0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0
        },

        /* WLAN Config Req Optional Elems */
        {
            IEEE_INFORMATION_ELEMENT,
            VENDOR_SPECIFIC_PAYLOAD,
            0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0 
        }
    },

    /* WLAN Config Rsp */
    {
        /* WLAN Config Rsp Mandatory Elems */
        {
            RESULT_CODE,
            0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0
        },

        /* WLAN Config Rsp Optional Elems */
        {
            IEEE_ASSIGNED_WTPBSSID,
            VENDOR_SPECIFIC_PAYLOAD,
            0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0
        }
    },

    { 
        {
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0
        },

        {
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0
        }
    }
};

#else

extern UINT4    gu4CapwapDebugMask;
extern tCapwapStats   CapwapStats[MAX_WTP_SUPPORTED];

extern UINT2  gu2CapwapMandMsgElemCnt[CAPWAP_MAX_EVENTS + 1];

extern UINT2  gu2MaxRetransmitCount;
extern UINT2 gu2CapwapMsgTypeToElemMap[CAPWAP_MAX_EVENTS + 1]
                               [CAPWAP_MSG_PRESENCE_TYPE][CAPWAP_MAX_MSG_ELEMS];
#endif
#endif

