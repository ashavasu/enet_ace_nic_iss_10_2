/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: capwapcliinc.h,v 1.1.1.1 2016/09/20 10:42:38 siva Exp $
 *
 * Description: This file contains include files of capwap module.
 *******************************************************************/

#ifndef __CAPWAPCLIINC_H__
#define __CAPWAPCLIINC_H__ 

#include "lr.h"
#include "cli.h"
#include "fssnmp.h"
#include "params.h"
#include "snmcdefn.h"
#include "snmctdfs.h"
#include "snmccons.h"
#include "capwapclidefn.h"
#include "capwapclitdfsg.h"
#include "capwapclitdfs.h"
#include "capwapclitrc.h"

#ifdef __CAPRMAIN_C__
#include "capwapcliglob.h"
#include "capwapclilwg.h"
#include "capwapclidefg.h"
#include "capwapclisz.h"
#include "capwapcliwrg.h"

#else

#include "capwapcliextn.h"
#include "capwapmibclig.h"
#include "capwapclilwg.h"
#include "capwapclidefg.h"
#include "capwapclisz.h"
#include "capwapcliwrg.h"

#endif /* __CAPRMAIN_C__ */

#include "capwapcliprot.h"
#include "capwapcliprotg.h"
#include "capwapclimacr.h"
#endif   /* __CAPWAPINC_H__*/

/*-----------------------------------------------------------------------*/
/*                       End of the file capwapinc.h                       */
/*-----------------------------------------------------------------------*/

