/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved                      *
 * $Id: capwapmacr.h,v 1.2 2017/11/24 10:37:02 siva Exp $ 
 * Description: This file contains the CAPWAP Packet TX related functions    *
 *****************************************************************************/
#ifndef _CAPWAPMACR_H
#define _CAPWAPMACR_H

#define   CAPWAP_MEMCPY      memcpy
#define   CAPWAP_HTONS       OSIX_HTONS
#define   CAPWAP_HTONL       OSIX_HTONL
#define   CAPWAP_NTOHS       OSIX_NTOHS
#define   CAPWAP_NTOHL       OSIX_NTOHL
#define   CAPWAP_MEMSET      MEMSET
#define   MAX_TIME_STRING    128

#define MAC_ADDR_LENGTH      6

/* Macros for getting data from linear buffer */
#define CAPWAP_GET_1BYTE(u1Val, pu1PktBuf) \
        do {                             \
    CAPWAP_MEMCPY (&u1Val,   \
       pu1PktBuf, sizeof (UINT1));\
           pu1PktBuf += 1;               \
        } while(0)

#define CAPWAP_GET_2BYTE(u2Val, pu1PktBuf)          \
        do {                                        \
           CAPWAP_MEMCPY (&u2Val, pu1PktBuf, 2);    \
           u2Val = (UINT2)CAPWAP_NTOHS(u2Val);      \
           pu1PktBuf += 2;                          \
        } while(0)

#define CAPWAP_GET_4BYTE(u4Val, pu1PktBuf)            \
        do {                                          \
           CAPWAP_MEMCPY (&u4Val, pu1PktBuf, 4);      \
           u4Val = (UINT4)CAPWAP_NTOHL(u4Val);        \
           pu1PktBuf += 4;                            \
        } while(0)

#define CAPWAP_GET_NBYTE(u4Val, pu1PktBuf, u4Len)    \
        do {                                         \
            CAPWAP_MEMCPY (&u4Val,pu1PktBuf,u4Len);  \
            pu1PktBuf += u4Len;                      \
        }while (0)

#define CAPWAP_GET_INT_2BYTE(i2Val, pu1PktBuf)        \
        do {                                          \
           CAPWAP_MEMCPY (&i2Val, pu1PktBuf, 2);      \
           i2Val = (INT2)CAPWAP_NTOHS((UINT2)i2Val);  \
           pu1PktBuf += 2;                            \
        } while(0)


#define CAPWAP_GET_INT_4BYTE(i4Val, pu1PktBuf)        \
        do {                                          \
           CAPWAP_MEMCPY (&i4Val, pu1PktBuf, 4);      \
           i4Val = (INT4)CAPWAP_NTOHL((UINT4)i4Val);  \
           pu1PktBuf += 4;                            \
        } while(0)


#define CAPWAP_SKIP_N_BYTES(u2Val, pu1PktBuf)        \
        do {                                         \
            pu1PktBuf += u2Val;                      \
        }while(0)

/* Encode Module Related Macros used to assign fields to buffer */
#define CAPWAP_PUT_1BYTE(pu1PktBuf, u1Val)  \
        do {                                \
           *pu1PktBuf = u1Val;              \
            pu1PktBuf += 1;                 \
        } while(0)

#define CAPWAP_PUT_2BYTE(pu1PktBuf, u2Val)      \
        do {                                    \
           *((UINT2 *) ((VOID*)pu1PktBuf)) = CAPWAP_HTONS(u2Val);\
           pu1PktBuf += 2;\
        } while(0)
#define CAPWAP_PUT_4BYTE(pu1PktBuf, u4Val)     \
        do {                                   \
           *((UINT4 *) ((VOID*)pu1PktBuf)) = OSIX_HTONL(u4Val);\
           pu1PktBuf += 4;\
        }while(0)

#define CAPWAP_PUT_NBYTE(pu1PktBuf, pu1PktBuf1, length)    \
        do {                                               \
           CAPWAP_MEMCPY (pu1PktBuf, &pu1PktBuf1, length); \
           pu1PktBuf += length;                            \
        }while(0)

#define CAPWAP_GET_PREAMBLE (u1Val, pu1PktBuf)            \
        do {                                              \
           u1Val = *pu1PktBuf;                            \
        } while(0)


#define CAPWAP_SKIP_VEND_INFO   4
#define CAPWAP_TX_HDR_SIZE sizeof(tCapwapHdr);

#define CAPWAP_TX_CTRL_HDR_SIZE  sizeof(tCapwapCtrlHdr);

#define CAPWAP_PKTBUF_POOLID     CAPWAPMemPoolIds[MAX_CAPWAP_PKTBUF_SIZING_ID]
#define CAPWAP_GENASSEMBLERADIO_POOLID CAPWAPMemPoolIds[MAX_CAPWAP_GENASSEMBLERADIO_SIZING_ID]

#define CAPWAP_DATA_TRANSFER_POOLID \
    CAPWAPMemPoolIds[MAX_CAPWAP_DATA_TRANSFER_PKTBUF_SIZING_ID]

#define CAPWAP_QUEMSG_POOLID \
    CAPWAPMemPoolIds[MAX_CAPWAP_PKT_QUE_SIZE_SIZING_ID]  
#define CAPWAP_BULKOFFSET_POOLID \
    CAPWAPMemPoolIds[MAX_CAPWAP_BULK_OFFSET_SIZING_ID]
#define CAPWAP_OFFSET_POOLID     CAPWAPMemPoolIds[MAX_CAPWAP_OFFSET_SIZING_ID]
#define CAPWAP_WTPMAPTABLE_POOLID \
    CAPWAPMemPoolIds[MAX_CAPWAP_WTPMAPTABLE_SIZING_ID]
#define CAPWAP_FRAG_POOLID        CAPWAPMemPoolIds[MAX_CAPWAP_FRAGMENTS_SIZE_SIZING_ID]

#define  CAPWAP_PKTBUF_ALLOC_MEM_BLOCK(pu1Block)\
         (pu1Block = \
          (UINT1 *)(MemAllocMemBlk (CAPWAP_PKTBUF_POOLID)))

#define  CAPWAP_DATA_TRANSFER_PKTBUF_ALLOC_MEM_BLOCK(pu1Block)\
         (pu1Block = \
          (UINT1 *)(MemAllocMemBlk (CAPWAP_DATA_TRANSFER_POOLID)))

#define CAPWAP_ALLOCATE_CRU_BUF(u4Size, u4Offset) \
        CRU_BUF_Allocate_MsgBufChain (u4Size, u4Offset)/*;\
 printf("+++%s===%d\n",__func__,__LINE__)\*/

#define CAPWAP_FRAGMENT_BUF(pBuf, u4Offset, ppFragBuf)               \
   CRU_BUF_Fragment_BufChain((pBuf), (u4Offset), (ppFragBuf))

#define CAPWAP_CONCAT_BUFS(pBuf1, pBuf2)                             \
   CRU_BUF_Concat_MsgBufChains((pBuf1), (pBuf2))

#define CAPWAP_GET_BUF_LEN(pBuf) \
        CRU_BUF_Get_ChainValidByteCount(pBuf)

#define CAPWAP_BUF_IF_LINEAR(pBuf, u4OffSet, u4Size) \
        CRU_BUF_Get_DataPtr_IfLinear ((tCRU_BUF_CHAIN_HEADER *)(pBuf), \
                                      (u4OffSet), (u4Size))

#define CAPWAP_COPY_FROM_BUF(pBuf, pu1Dst, u4Offset, u4Size) \
        CRU_BUF_Copy_FromBufChain ((pBuf), (UINT1 *)(pu1Dst), u4Offset, u4Size)

#define CAPWAP_MOVE_VALID_OFFSET (pBuf, u4Size) \
        CRU_BUF_Move_ValidOffset(pBuf,u4Size)

#define CAPWAP_CONCAT_CRU_BUFS (pBuf1,pBuf2)   \
   CRU_BUF_Concat_MsgBufChains((pBuf1), (pBuf2))

#define CAPWAP_RELEASE_CRU_BUF(pBuf) \
/* printf("---%s===%d\n",__func__,__LINE__);*/\
        CRU_BUF_Release_MsgBufChain ((pBuf), 0)

#define CAPWAP_COPY_TO_BUF(pBuf, pu1Src, u4Offset, u4Size) \
        CRU_BUF_Copy_OverBufChain ((pBuf), (UINT1 *)(pu1Src), u4Offset, u4Size)


#define CAPWAP_GET_CTRL_MSGTYPE(ctrlPkt)   (ctrlPkt->capwapCtrlHdr->u2MsgType) 
#define CAPWAP_GET_SEQ_NUMBER(ctrlPkt)     (ctrlPkt->capwapCtrlHdr->u1SeqNum)

#define CAPWAP_GET_NUM_MSGBLOCKS (ctrlPkt, index) \
    (ctrlPkt->capwapMsgElm[index].numMsgElemInstance)

#define CAPWAP_GET_OFFSET (pPeerentry, u4Index) \
            ((pPeerentry->capwapMsgElm[u4Index])->u2Offset


#endif
