/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: stdwtpdb.h,v 1.1.1.1 2016/09/20 10:42:38 siva Exp $ 
*
* Description: Protocol Mib Data base
*********************************************************************/


#ifndef _STDWTPDB_H
#define _STDWTPDB_H

UINT1 CapwapBaseAcNameListTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 CapwapBaseMacAclTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 CapwapBaseWtpProfileTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 CapwapBaseWtpStateTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 CapwapBaseWtpTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 CapwapBaseWirelessBindingTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 CapwapBaseStationTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 CapwapBaseWtpEventsStatsTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 CapwapBaseRadioEventsStatsTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_UNSIGNED32};

UINT4 stdwtp [] ={1,3,6,1,2,1,196};
tSNMP_OID_TYPE stdwtpOID = {7, stdwtp};


UINT4 CapwapBaseWtpSessions [ ] ={1,3,6,1,2,1,196,1,1,1};
UINT4 CapwapBaseWtpSessionsLimit [ ] ={1,3,6,1,2,1,196,1,1,2};
UINT4 CapwapBaseStationSessions [ ] ={1,3,6,1,2,1,196,1,1,3};
UINT4 CapwapBaseStationSessionsLimit [ ] ={1,3,6,1,2,1,196,1,1,4};
UINT4 CapwapBaseDataChannelDTLSPolicyOptions [ ] ={1,3,6,1,2,1,196,1,1,5};
UINT4 CapwapBaseControlChannelAuthenOptions [ ] ={1,3,6,1,2,1,196,1,1,6};
UINT4 CapwapBaseAcNameListId [ ] ={1,3,6,1,2,1,196,1,1,9,1,1};
UINT4 CapwapBaseAcNameListName [ ] ={1,3,6,1,2,1,196,1,1,9,1,2};
UINT4 CapwapBaseAcNameListPriority [ ] ={1,3,6,1,2,1,196,1,1,9,1,3};
UINT4 CapwapBaseAcNameListRowStatus [ ] ={1,3,6,1,2,1,196,1,1,9,1,4};
UINT4 CapwapBaseMacAclId [ ] ={1,3,6,1,2,1,196,1,1,10,1,1};
UINT4 CapwapBaseMacAclStationId [ ] ={1,3,6,1,2,1,196,1,1,10,1,2};
UINT4 CapwapBaseMacAclRowStatus [ ] ={1,3,6,1,2,1,196,1,1,10,1,3};
UINT4 CapwapBaseWtpProfileId [ ] ={1,3,6,1,2,1,196,1,2,1,1,1};
UINT4 CapwapBaseWtpProfileName [ ] ={1,3,6,1,2,1,196,1,2,1,1,2};
UINT4 CapwapBaseWtpProfileWtpMacAddress [ ] ={1,3,6,1,2,1,196,1,2,1,1,3};
UINT4 CapwapBaseWtpProfileWtpModelNumber [ ] ={1,3,6,1,2,1,196,1,2,1,1,4};
UINT4 CapwapBaseWtpProfileWtpName [ ] ={1,3,6,1,2,1,196,1,2,1,1,5};
UINT4 CapwapBaseWtpProfileWtpLocation [ ] ={1,3,6,1,2,1,196,1,2,1,1,6};
UINT4 CapwapBaseWtpProfileWtpStaticIpEnable [ ] ={1,3,6,1,2,1,196,1,2,1,1,7};
UINT4 CapwapBaseWtpProfileWtpStaticIpType [ ] ={1,3,6,1,2,1,196,1,2,1,1,8};
UINT4 CapwapBaseWtpProfileWtpStaticIpAddress [ ] ={1,3,6,1,2,1,196,1,2,1,1,9};
UINT4 CapwapBaseWtpProfileWtpNetmask [ ] ={1,3,6,1,2,1,196,1,2,1,1,10};
UINT4 CapwapBaseWtpProfileWtpGateway [ ] ={1,3,6,1,2,1,196,1,2,1,1,11};
UINT4 CapwapBaseWtpProfileWtpFallbackEnable [ ] ={1,3,6,1,2,1,196,1,2,1,1,12};
UINT4 CapwapBaseWtpProfileWtpEchoInterval [ ] ={1,3,6,1,2,1,196,1,2,1,1,13};
UINT4 CapwapBaseWtpProfileWtpIdleTimeout [ ] ={1,3,6,1,2,1,196,1,2,1,1,14};
UINT4 CapwapBaseWtpProfileWtpMaxDiscoveryInterval [ ] ={1,3,6,1,2,1,196,1,2,1,1,15};
UINT4 CapwapBaseWtpProfileWtpReportInterval [ ] ={1,3,6,1,2,1,196,1,2,1,1,16};
UINT4 CapwapBaseWtpProfileWtpStatisticsTimer [ ] ={1,3,6,1,2,1,196,1,2,1,1,17};
UINT4 CapwapBaseWtpProfileWtpEcnSupport [ ] ={1,3,6,1,2,1,196,1,2,1,1,18};
UINT4 CapwapBaseWtpProfileRowStatus [ ] ={1,3,6,1,2,1,196,1,2,1,1,19};
UINT4 CapwapBaseWtpStateWtpId [ ] ={1,3,6,1,2,1,196,1,2,2,1,1};
UINT4 CapwapBaseWtpStateWtpIpAddressType [ ] ={1,3,6,1,2,1,196,1,2,2,1,2};
UINT4 CapwapBaseWtpStateWtpIpAddress [ ] ={1,3,6,1,2,1,196,1,2,2,1,3};
UINT4 CapwapBaseWtpStateWtpLocalIpAddressType [ ] ={1,3,6,1,2,1,196,1,2,2,1,4};
UINT4 CapwapBaseWtpStateWtpLocalIpAddress [ ] ={1,3,6,1,2,1,196,1,2,2,1,5};
UINT4 CapwapBaseWtpStateWtpBaseMacAddress [ ] ={1,3,6,1,2,1,196,1,2,2,1,6};
UINT4 CapwapBaseWtpState [ ] ={1,3,6,1,2,1,196,1,2,2,1,7};
UINT4 CapwapBaseWtpStateWtpUpTime [ ] ={1,3,6,1,2,1,196,1,2,2,1,8};
UINT4 CapwapBaseWtpStateWtpCurrWtpProfileId [ ] ={1,3,6,1,2,1,196,1,2,2,1,9};
UINT4 CapwapBaseWtpCurrId [ ] ={1,3,6,1,2,1,196,1,2,3,1,1};
UINT4 CapwapBaseWtpPhyIndex [ ] ={1,3,6,1,2,1,196,1,2,3,1,2};
UINT4 CapwapBaseWtpBaseMacAddress [ ] ={1,3,6,1,2,1,196,1,2,3,1,3};
UINT4 CapwapBaseWtpTunnelModeOptions [ ] ={1,3,6,1,2,1,196,1,2,3,1,4};
UINT4 CapwapBaseWtpMacTypeOptions [ ] ={1,3,6,1,2,1,196,1,2,3,1,5};
UINT4 CapwapBaseWtpDiscoveryType [ ] ={1,3,6,1,2,1,196,1,2,3,1,6};
UINT4 CapwapBaseWtpRadiosInUseNum [ ] ={1,3,6,1,2,1,196,1,2,3,1,7};
UINT4 CapwapBaseWtpRadioNumLimit [ ] ={1,3,6,1,2,1,196,1,2,3,1,8};
UINT4 CapwapBaseWtpRetransmitCount [ ] ={1,3,6,1,2,1,196,1,2,3,1,9};
UINT4 CapwapBaseWirelessBindingRadioId [ ] ={1,3,6,1,2,1,196,1,2,4,1,1};
UINT4 CapwapBaseWirelessBindingVirtualRadioIfIndex [ ] ={1,3,6,1,2,1,196,1,2,4,1,2};
UINT4 CapwapBaseWirelessBindingType [ ] ={1,3,6,1,2,1,196,1,2,4,1,3};
UINT4 CapwapBaseStationId [ ] ={1,3,6,1,2,1,196,1,2,5,1,1};
UINT4 CapwapBaseStationWtpId [ ] ={1,3,6,1,2,1,196,1,2,5,1,2};
UINT4 CapwapBaseStationWtpRadioId [ ] ={1,3,6,1,2,1,196,1,2,5,1,3};
UINT4 CapwapBaseStationAddedTime [ ] ={1,3,6,1,2,1,196,1,2,5,1,4};
UINT4 CapwapBaseStationVlanName [ ] ={1,3,6,1,2,1,196,1,2,5,1,5};
UINT4 CapwapBaseWtpEventsStatsRebootCount [ ] ={1,3,6,1,2,1,196,1,2,6,1,1};
UINT4 CapwapBaseWtpEventsStatsInitCount [ ] ={1,3,6,1,2,1,196,1,2,6,1,2};
UINT4 CapwapBaseWtpEventsStatsLinkFailureCount [ ] ={1,3,6,1,2,1,196,1,2,6,1,3};
UINT4 CapwapBaseWtpEventsStatsSwFailureCount [ ] ={1,3,6,1,2,1,196,1,2,6,1,4};
UINT4 CapwapBaseWtpEventsStatsHwFailureCount [ ] ={1,3,6,1,2,1,196,1,2,6,1,5};
UINT4 CapwapBaseWtpEventsStatsOtherFailureCount [ ] ={1,3,6,1,2,1,196,1,2,6,1,6};
UINT4 CapwapBaseWtpEventsStatsUnknownFailureCount [ ] ={1,3,6,1,2,1,196,1,2,6,1,7};
UINT4 CapwapBaseWtpEventsStatsLastFailureType [ ] ={1,3,6,1,2,1,196,1,2,6,1,8};
UINT4 CapwapBaseRadioEventsWtpRadioId [ ] ={1,3,6,1,2,1,196,1,2,7,1,1};
UINT4 CapwapBaseRadioEventsStatsResetCount [ ] ={1,3,6,1,2,1,196,1,2,7,1,2};
UINT4 CapwapBaseRadioEventsStatsSwFailureCount [ ] ={1,3,6,1,2,1,196,1,2,7,1,3};
UINT4 CapwapBaseRadioEventsStatsHwFailureCount [ ] ={1,3,6,1,2,1,196,1,2,7,1,4};
UINT4 CapwapBaseRadioEventsStatsOtherFailureCount [ ] ={1,3,6,1,2,1,196,1,2,7,1,5};
UINT4 CapwapBaseRadioEventsStatsUnknownFailureCount [ ] ={1,3,6,1,2,1,196,1,2,7,1,6};
UINT4 CapwapBaseRadioEventsStatsConfigUpdateCount [ ] ={1,3,6,1,2,1,196,1,2,7,1,7};
UINT4 CapwapBaseRadioEventsStatsChannelChangeCount [ ] ={1,3,6,1,2,1,196,1,2,7,1,8};
UINT4 CapwapBaseRadioEventsStatsBandChangeCount [ ] ={1,3,6,1,2,1,196,1,2,7,1,9};
UINT4 CapwapBaseRadioEventsStatsCurrNoiseFloor [ ] ={1,3,6,1,2,1,196,1,2,7,1,10};
UINT4 CapwapBaseRadioEventsStatsDecryptErrorCount [ ] ={1,3,6,1,2,1,196,1,2,7,1,11};
UINT4 CapwapBaseRadioEventsStatsLastFailureType [ ] ={1,3,6,1,2,1,196,1,2,7,1,12};
UINT4 CapwapBaseAcMaxRetransmit [ ] ={1,3,6,1,2,1,196,1,3,1};
UINT4 CapwapBaseAcChangeStatePendingTimer [ ] ={1,3,6,1,2,1,196,1,3,2};
UINT4 CapwapBaseAcDataCheckTimer [ ] ={1,3,6,1,2,1,196,1,3,3};
UINT4 CapwapBaseAcDTLSSessionDeleteTimer [ ] ={1,3,6,1,2,1,196,1,3,4};
UINT4 CapwapBaseAcEchoInterval [ ] ={1,3,6,1,2,1,196,1,3,5};
UINT4 CapwapBaseAcRetransmitInterval [ ] ={1,3,6,1,2,1,196,1,3,6};
UINT4 CapwapBaseAcSilentInterval [ ] ={1,3,6,1,2,1,196,1,3,7};
UINT4 CapwapBaseAcWaitDTLSTimer [ ] ={1,3,6,1,2,1,196,1,3,8};
UINT4 CapwapBaseAcWaitJoinTimer [ ] ={1,3,6,1,2,1,196,1,3,9};
UINT4 CapwapBaseAcEcnSupport [ ] ={1,3,6,1,2,1,196,1,3,10};
UINT4 CapwapBaseFailedDTLSAuthFailureCount [ ] ={1,3,6,1,2,1,196,1,4,1};
UINT4 CapwapBaseFailedDTLSSessionCount [ ] ={1,3,6,1,2,1,196,1,4,2};
UINT4 CapwapBaseNtfWtpId [ ] ={1,3,6,1,2,1,196,1,5,1};
UINT4 CapwapBaseNtfRadioId [ ] ={1,3,6,1,2,1,196,1,5,2};
UINT4 CapwapBaseNtfChannelType [ ] ={1,3,6,1,2,1,196,1,5,3};
UINT4 CapwapBaseNtfAuthenMethod [ ] ={1,3,6,1,2,1,196,1,5,4};
UINT4 CapwapBaseNtfChannelDownReason [ ] ={1,3,6,1,2,1,196,1,5,5};
UINT4 CapwapBaseNtfStationIdList [ ] ={1,3,6,1,2,1,196,1,5,6};
UINT4 CapwapBaseNtfAuthenFailureReason [ ] ={1,3,6,1,2,1,196,1,5,7};
UINT4 CapwapBaseNtfRadioOperStatusFlag [ ] ={1,3,6,1,2,1,196,1,5,8};
UINT4 CapwapBaseNtfRadioStatusCause [ ] ={1,3,6,1,2,1,196,1,5,9};
UINT4 CapwapBaseNtfJoinFailureReason [ ] ={1,3,6,1,2,1,196,1,5,10};
UINT4 CapwapBaseNtfImageFailureReason [ ] ={1,3,6,1,2,1,196,1,5,11};
UINT4 CapwapBaseNtfConfigMsgErrorType [ ] ={1,3,6,1,2,1,196,1,5,12};
UINT4 CapwapBaseNtfMsgErrorElements [ ] ={1,3,6,1,2,1,196,1,5,13};
UINT4 CapwapBaseChannelUpDownNotifyEnable [ ] ={1,3,6,1,2,1,196,1,6,1};
UINT4 CapwapBaseDecryptErrorNotifyEnable [ ] ={1,3,6,1,2,1,196,1,6,2};
UINT4 CapwapBaseJoinFailureNotifyEnable [ ] ={1,3,6,1,2,1,196,1,6,3};
UINT4 CapwapBaseImageUpgradeFailureNotifyEnable [ ] ={1,3,6,1,2,1,196,1,6,4};
UINT4 CapwapBaseConfigMsgErrorNotifyEnable [ ] ={1,3,6,1,2,1,196,1,6,5};
UINT4 CapwapBaseRadioOperableStatusNotifyEnable [ ] ={1,3,6,1,2,1,196,1,6,6};
UINT4 CapwapBaseAuthenFailureNotifyEnable [ ] ={1,3,6,1,2,1,196,1,6,7};

tMbDbEntry stdwtpMibEntry[]= {
{{10,CapwapBaseWtpSessions}, NULL, CapwapBaseWtpSessionsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,CapwapBaseWtpSessionsLimit}, NULL, CapwapBaseWtpSessionsLimitGet, CapwapBaseWtpSessionsLimitSet, CapwapBaseWtpSessionsLimitTest, CapwapBaseWtpSessionsLimitDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,CapwapBaseStationSessions}, NULL, CapwapBaseStationSessionsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,CapwapBaseStationSessionsLimit}, NULL, CapwapBaseStationSessionsLimitGet, CapwapBaseStationSessionsLimitSet, CapwapBaseStationSessionsLimitTest, CapwapBaseStationSessionsLimitDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,CapwapBaseDataChannelDTLSPolicyOptions}, NULL, CapwapBaseDataChannelDTLSPolicyOptionsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,CapwapBaseControlChannelAuthenOptions}, NULL, CapwapBaseControlChannelAuthenOptionsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,CapwapBaseAcNameListId}, GetNextIndexCapwapBaseAcNameListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, CapwapBaseAcNameListTableINDEX, 1, 0, 0, NULL},

{{12,CapwapBaseAcNameListName}, GetNextIndexCapwapBaseAcNameListTable, CapwapBaseAcNameListNameGet, CapwapBaseAcNameListNameSet, CapwapBaseAcNameListNameTest, CapwapBaseAcNameListTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, CapwapBaseAcNameListTableINDEX, 1, 0, 0, NULL},

{{12,CapwapBaseAcNameListPriority}, GetNextIndexCapwapBaseAcNameListTable, CapwapBaseAcNameListPriorityGet, CapwapBaseAcNameListPrioritySet, CapwapBaseAcNameListPriorityTest, CapwapBaseAcNameListTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, CapwapBaseAcNameListTableINDEX, 1, 0, 0, NULL},

{{12,CapwapBaseAcNameListRowStatus}, GetNextIndexCapwapBaseAcNameListTable, CapwapBaseAcNameListRowStatusGet, CapwapBaseAcNameListRowStatusSet, CapwapBaseAcNameListRowStatusTest, CapwapBaseAcNameListTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, CapwapBaseAcNameListTableINDEX, 1, 0, 1, NULL},

{{12,CapwapBaseMacAclId}, GetNextIndexCapwapBaseMacAclTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, CapwapBaseMacAclTableINDEX, 1, 0, 0, NULL},

{{12,CapwapBaseMacAclStationId}, GetNextIndexCapwapBaseMacAclTable, CapwapBaseMacAclStationIdGet, CapwapBaseMacAclStationIdSet, CapwapBaseMacAclStationIdTest, CapwapBaseMacAclTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, CapwapBaseMacAclTableINDEX, 1, 0, 0, NULL},

{{12,CapwapBaseMacAclRowStatus}, GetNextIndexCapwapBaseMacAclTable, CapwapBaseMacAclRowStatusGet, CapwapBaseMacAclRowStatusSet, CapwapBaseMacAclRowStatusTest, CapwapBaseMacAclTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, CapwapBaseMacAclTableINDEX, 1, 0, 1, NULL},

{{12,CapwapBaseWtpProfileId}, GetNextIndexCapwapBaseWtpProfileTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, CapwapBaseWtpProfileTableINDEX, 1, 0, 0, NULL},

{{12,CapwapBaseWtpProfileName}, GetNextIndexCapwapBaseWtpProfileTable, CapwapBaseWtpProfileNameGet, CapwapBaseWtpProfileNameSet, CapwapBaseWtpProfileNameTest, CapwapBaseWtpProfileTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, CapwapBaseWtpProfileTableINDEX, 1, 0, 0, NULL},

{{12,CapwapBaseWtpProfileWtpMacAddress}, GetNextIndexCapwapBaseWtpProfileTable, CapwapBaseWtpProfileWtpMacAddressGet, CapwapBaseWtpProfileWtpMacAddressSet, CapwapBaseWtpProfileWtpMacAddressTest, CapwapBaseWtpProfileTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, CapwapBaseWtpProfileTableINDEX, 1, 0, 0, NULL},

{{12,CapwapBaseWtpProfileWtpModelNumber}, GetNextIndexCapwapBaseWtpProfileTable, CapwapBaseWtpProfileWtpModelNumberGet, CapwapBaseWtpProfileWtpModelNumberSet, CapwapBaseWtpProfileWtpModelNumberTest, CapwapBaseWtpProfileTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, CapwapBaseWtpProfileTableINDEX, 1, 0, 0, NULL},

{{12,CapwapBaseWtpProfileWtpName}, GetNextIndexCapwapBaseWtpProfileTable, CapwapBaseWtpProfileWtpNameGet, CapwapBaseWtpProfileWtpNameSet, CapwapBaseWtpProfileWtpNameTest, CapwapBaseWtpProfileTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, CapwapBaseWtpProfileTableINDEX, 1, 0, 0, NULL},

{{12,CapwapBaseWtpProfileWtpLocation}, GetNextIndexCapwapBaseWtpProfileTable, CapwapBaseWtpProfileWtpLocationGet, CapwapBaseWtpProfileWtpLocationSet, CapwapBaseWtpProfileWtpLocationTest, CapwapBaseWtpProfileTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, CapwapBaseWtpProfileTableINDEX, 1, 0, 0, NULL},

{{12,CapwapBaseWtpProfileWtpStaticIpEnable}, GetNextIndexCapwapBaseWtpProfileTable, CapwapBaseWtpProfileWtpStaticIpEnableGet, CapwapBaseWtpProfileWtpStaticIpEnableSet, CapwapBaseWtpProfileWtpStaticIpEnableTest, CapwapBaseWtpProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, CapwapBaseWtpProfileTableINDEX, 1, 0, 0, NULL},

{{12,CapwapBaseWtpProfileWtpStaticIpType}, GetNextIndexCapwapBaseWtpProfileTable, CapwapBaseWtpProfileWtpStaticIpTypeGet, CapwapBaseWtpProfileWtpStaticIpTypeSet, CapwapBaseWtpProfileWtpStaticIpTypeTest, CapwapBaseWtpProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, CapwapBaseWtpProfileTableINDEX, 1, 0, 0, NULL},

{{12,CapwapBaseWtpProfileWtpStaticIpAddress}, GetNextIndexCapwapBaseWtpProfileTable, CapwapBaseWtpProfileWtpStaticIpAddressGet, CapwapBaseWtpProfileWtpStaticIpAddressSet, CapwapBaseWtpProfileWtpStaticIpAddressTest, CapwapBaseWtpProfileTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, CapwapBaseWtpProfileTableINDEX, 1, 0, 0, NULL},

{{12,CapwapBaseWtpProfileWtpNetmask}, GetNextIndexCapwapBaseWtpProfileTable, CapwapBaseWtpProfileWtpNetmaskGet, CapwapBaseWtpProfileWtpNetmaskSet, CapwapBaseWtpProfileWtpNetmaskTest, CapwapBaseWtpProfileTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, CapwapBaseWtpProfileTableINDEX, 1, 0, 0, NULL},

{{12,CapwapBaseWtpProfileWtpGateway}, GetNextIndexCapwapBaseWtpProfileTable, CapwapBaseWtpProfileWtpGatewayGet, CapwapBaseWtpProfileWtpGatewaySet, CapwapBaseWtpProfileWtpGatewayTest, CapwapBaseWtpProfileTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, CapwapBaseWtpProfileTableINDEX, 1, 0, 0, NULL},

{{12,CapwapBaseWtpProfileWtpFallbackEnable}, GetNextIndexCapwapBaseWtpProfileTable, CapwapBaseWtpProfileWtpFallbackEnableGet, CapwapBaseWtpProfileWtpFallbackEnableSet, CapwapBaseWtpProfileWtpFallbackEnableTest, CapwapBaseWtpProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, CapwapBaseWtpProfileTableINDEX, 1, 0, 0, "1"},

{{12,CapwapBaseWtpProfileWtpEchoInterval}, GetNextIndexCapwapBaseWtpProfileTable, CapwapBaseWtpProfileWtpEchoIntervalGet, CapwapBaseWtpProfileWtpEchoIntervalSet, CapwapBaseWtpProfileWtpEchoIntervalTest, CapwapBaseWtpProfileTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, CapwapBaseWtpProfileTableINDEX, 1, 0, 0, "30"},

{{12,CapwapBaseWtpProfileWtpIdleTimeout}, GetNextIndexCapwapBaseWtpProfileTable, CapwapBaseWtpProfileWtpIdleTimeoutGet, CapwapBaseWtpProfileWtpIdleTimeoutSet, CapwapBaseWtpProfileWtpIdleTimeoutTest, CapwapBaseWtpProfileTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, CapwapBaseWtpProfileTableINDEX, 1, 0, 0, "300"},

{{12,CapwapBaseWtpProfileWtpMaxDiscoveryInterval}, GetNextIndexCapwapBaseWtpProfileTable, CapwapBaseWtpProfileWtpMaxDiscoveryIntervalGet, CapwapBaseWtpProfileWtpMaxDiscoveryIntervalSet, CapwapBaseWtpProfileWtpMaxDiscoveryIntervalTest, CapwapBaseWtpProfileTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, CapwapBaseWtpProfileTableINDEX, 1, 0, 0, "20"},

{{12,CapwapBaseWtpProfileWtpReportInterval}, GetNextIndexCapwapBaseWtpProfileTable, CapwapBaseWtpProfileWtpReportIntervalGet, CapwapBaseWtpProfileWtpReportIntervalSet, CapwapBaseWtpProfileWtpReportIntervalTest, CapwapBaseWtpProfileTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, CapwapBaseWtpProfileTableINDEX, 1, 0, 0, "120"},

{{12,CapwapBaseWtpProfileWtpStatisticsTimer}, GetNextIndexCapwapBaseWtpProfileTable, CapwapBaseWtpProfileWtpStatisticsTimerGet, CapwapBaseWtpProfileWtpStatisticsTimerSet, CapwapBaseWtpProfileWtpStatisticsTimerTest, CapwapBaseWtpProfileTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, CapwapBaseWtpProfileTableINDEX, 1, 0, 0, "120"},

{{12,CapwapBaseWtpProfileWtpEcnSupport}, GetNextIndexCapwapBaseWtpProfileTable, CapwapBaseWtpProfileWtpEcnSupportGet, CapwapBaseWtpProfileWtpEcnSupportSet, CapwapBaseWtpProfileWtpEcnSupportTest, CapwapBaseWtpProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, CapwapBaseWtpProfileTableINDEX, 1, 0, 0, NULL},

{{12,CapwapBaseWtpProfileRowStatus}, GetNextIndexCapwapBaseWtpProfileTable, CapwapBaseWtpProfileRowStatusGet, CapwapBaseWtpProfileRowStatusSet, CapwapBaseWtpProfileRowStatusTest, CapwapBaseWtpProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, CapwapBaseWtpProfileTableINDEX, 1, 0, 1, NULL},

{{12,CapwapBaseWtpStateWtpId}, GetNextIndexCapwapBaseWtpStateTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, CapwapBaseWtpStateTableINDEX, 1, 0, 0, NULL},

{{12,CapwapBaseWtpStateWtpIpAddressType}, GetNextIndexCapwapBaseWtpStateTable, CapwapBaseWtpStateWtpIpAddressTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, CapwapBaseWtpStateTableINDEX, 1, 0, 0, NULL},

{{12,CapwapBaseWtpStateWtpIpAddress}, GetNextIndexCapwapBaseWtpStateTable, CapwapBaseWtpStateWtpIpAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, CapwapBaseWtpStateTableINDEX, 1, 0, 0, NULL},

{{12,CapwapBaseWtpStateWtpLocalIpAddressType}, GetNextIndexCapwapBaseWtpStateTable, CapwapBaseWtpStateWtpLocalIpAddressTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, CapwapBaseWtpStateTableINDEX, 1, 0, 0, NULL},

{{12,CapwapBaseWtpStateWtpLocalIpAddress}, GetNextIndexCapwapBaseWtpStateTable, CapwapBaseWtpStateWtpLocalIpAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, CapwapBaseWtpStateTableINDEX, 1, 0, 0, NULL},

{{12,CapwapBaseWtpStateWtpBaseMacAddress}, GetNextIndexCapwapBaseWtpStateTable, CapwapBaseWtpStateWtpBaseMacAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, CapwapBaseWtpStateTableINDEX, 1, 0, 0, NULL},

{{12,CapwapBaseWtpState}, GetNextIndexCapwapBaseWtpStateTable, CapwapBaseWtpStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, CapwapBaseWtpStateTableINDEX, 1, 0, 0, NULL},

{{12,CapwapBaseWtpStateWtpUpTime}, GetNextIndexCapwapBaseWtpStateTable, CapwapBaseWtpStateWtpUpTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, CapwapBaseWtpStateTableINDEX, 1, 0, 0, NULL},

{{12,CapwapBaseWtpStateWtpCurrWtpProfileId}, GetNextIndexCapwapBaseWtpStateTable, CapwapBaseWtpStateWtpCurrWtpProfileIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, CapwapBaseWtpStateTableINDEX, 1, 0, 0, NULL},

{{12,CapwapBaseWtpCurrId}, GetNextIndexCapwapBaseWtpTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, CapwapBaseWtpTableINDEX, 1, 0, 0, NULL},

{{12,CapwapBaseWtpPhyIndex}, GetNextIndexCapwapBaseWtpTable, CapwapBaseWtpPhyIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, CapwapBaseWtpTableINDEX, 1, 0, 0, NULL},

{{12,CapwapBaseWtpBaseMacAddress}, GetNextIndexCapwapBaseWtpTable, CapwapBaseWtpBaseMacAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, CapwapBaseWtpTableINDEX, 1, 0, 0, NULL},

{{12,CapwapBaseWtpTunnelModeOptions}, GetNextIndexCapwapBaseWtpTable, CapwapBaseWtpTunnelModeOptionsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, CapwapBaseWtpTableINDEX, 1, 0, 0, NULL},

{{12,CapwapBaseWtpMacTypeOptions}, GetNextIndexCapwapBaseWtpTable, CapwapBaseWtpMacTypeOptionsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, CapwapBaseWtpTableINDEX, 1, 0, 0, NULL},

{{12,CapwapBaseWtpDiscoveryType}, GetNextIndexCapwapBaseWtpTable, CapwapBaseWtpDiscoveryTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, CapwapBaseWtpTableINDEX, 1, 0, 0, NULL},

{{12,CapwapBaseWtpRadiosInUseNum}, GetNextIndexCapwapBaseWtpTable, CapwapBaseWtpRadiosInUseNumGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, CapwapBaseWtpTableINDEX, 1, 0, 0, NULL},

{{12,CapwapBaseWtpRadioNumLimit}, GetNextIndexCapwapBaseWtpTable, CapwapBaseWtpRadioNumLimitGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, CapwapBaseWtpTableINDEX, 1, 0, 0, NULL},

{{12,CapwapBaseWtpRetransmitCount}, GetNextIndexCapwapBaseWtpTable, CapwapBaseWtpRetransmitCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, CapwapBaseWtpTableINDEX, 1, 0, 0, NULL},

{{12,CapwapBaseWirelessBindingRadioId}, GetNextIndexCapwapBaseWirelessBindingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, CapwapBaseWirelessBindingTableINDEX, 2, 0, 0, NULL},

{{12,CapwapBaseWirelessBindingVirtualRadioIfIndex}, GetNextIndexCapwapBaseWirelessBindingTable, CapwapBaseWirelessBindingVirtualRadioIfIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, CapwapBaseWirelessBindingTableINDEX, 2, 0, 0, NULL},

{{12,CapwapBaseWirelessBindingType}, GetNextIndexCapwapBaseWirelessBindingTable, CapwapBaseWirelessBindingTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, CapwapBaseWirelessBindingTableINDEX, 2, 0, 0, NULL},

{{12,CapwapBaseStationId}, GetNextIndexCapwapBaseStationTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, CapwapBaseStationTableINDEX, 1, 0, 0, NULL},

{{12,CapwapBaseStationWtpId}, GetNextIndexCapwapBaseStationTable, CapwapBaseStationWtpIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, CapwapBaseStationTableINDEX, 1, 0, 0, NULL},

{{12,CapwapBaseStationWtpRadioId}, GetNextIndexCapwapBaseStationTable, CapwapBaseStationWtpRadioIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, CapwapBaseStationTableINDEX, 1, 0, 0, NULL},

{{12,CapwapBaseStationAddedTime}, GetNextIndexCapwapBaseStationTable, CapwapBaseStationAddedTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, CapwapBaseStationTableINDEX, 1, 0, 0, NULL},

{{12,CapwapBaseStationVlanName}, GetNextIndexCapwapBaseStationTable, CapwapBaseStationVlanNameGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, CapwapBaseStationTableINDEX, 1, 0, 0, NULL},

{{12,CapwapBaseWtpEventsStatsRebootCount}, GetNextIndexCapwapBaseWtpEventsStatsTable, CapwapBaseWtpEventsStatsRebootCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, CapwapBaseWtpEventsStatsTableINDEX, 1, 0, 0, NULL},

{{12,CapwapBaseWtpEventsStatsInitCount}, GetNextIndexCapwapBaseWtpEventsStatsTable, CapwapBaseWtpEventsStatsInitCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, CapwapBaseWtpEventsStatsTableINDEX, 1, 0, 0, NULL},

{{12,CapwapBaseWtpEventsStatsLinkFailureCount}, GetNextIndexCapwapBaseWtpEventsStatsTable, CapwapBaseWtpEventsStatsLinkFailureCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, CapwapBaseWtpEventsStatsTableINDEX, 1, 0, 0, NULL},

{{12,CapwapBaseWtpEventsStatsSwFailureCount}, GetNextIndexCapwapBaseWtpEventsStatsTable, CapwapBaseWtpEventsStatsSwFailureCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, CapwapBaseWtpEventsStatsTableINDEX, 1, 0, 0, NULL},

{{12,CapwapBaseWtpEventsStatsHwFailureCount}, GetNextIndexCapwapBaseWtpEventsStatsTable, CapwapBaseWtpEventsStatsHwFailureCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, CapwapBaseWtpEventsStatsTableINDEX, 1, 0, 0, NULL},

{{12,CapwapBaseWtpEventsStatsOtherFailureCount}, GetNextIndexCapwapBaseWtpEventsStatsTable, CapwapBaseWtpEventsStatsOtherFailureCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, CapwapBaseWtpEventsStatsTableINDEX, 1, 0, 0, NULL},

{{12,CapwapBaseWtpEventsStatsUnknownFailureCount}, GetNextIndexCapwapBaseWtpEventsStatsTable, CapwapBaseWtpEventsStatsUnknownFailureCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, CapwapBaseWtpEventsStatsTableINDEX, 1, 0, 0, NULL},

{{12,CapwapBaseWtpEventsStatsLastFailureType}, GetNextIndexCapwapBaseWtpEventsStatsTable, CapwapBaseWtpEventsStatsLastFailureTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, CapwapBaseWtpEventsStatsTableINDEX, 1, 0, 0, NULL},

{{12,CapwapBaseRadioEventsWtpRadioId}, GetNextIndexCapwapBaseRadioEventsStatsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, CapwapBaseRadioEventsStatsTableINDEX, 2, 0, 0, NULL},

{{12,CapwapBaseRadioEventsStatsResetCount}, GetNextIndexCapwapBaseRadioEventsStatsTable, CapwapBaseRadioEventsStatsResetCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, CapwapBaseRadioEventsStatsTableINDEX, 2, 0, 0, NULL},

{{12,CapwapBaseRadioEventsStatsSwFailureCount}, GetNextIndexCapwapBaseRadioEventsStatsTable, CapwapBaseRadioEventsStatsSwFailureCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, CapwapBaseRadioEventsStatsTableINDEX, 2, 0, 0, NULL},

{{12,CapwapBaseRadioEventsStatsHwFailureCount}, GetNextIndexCapwapBaseRadioEventsStatsTable, CapwapBaseRadioEventsStatsHwFailureCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, CapwapBaseRadioEventsStatsTableINDEX, 2, 0, 0, NULL},

{{12,CapwapBaseRadioEventsStatsOtherFailureCount}, GetNextIndexCapwapBaseRadioEventsStatsTable, CapwapBaseRadioEventsStatsOtherFailureCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, CapwapBaseRadioEventsStatsTableINDEX, 2, 0, 0, NULL},

{{12,CapwapBaseRadioEventsStatsUnknownFailureCount}, GetNextIndexCapwapBaseRadioEventsStatsTable, CapwapBaseRadioEventsStatsUnknownFailureCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, CapwapBaseRadioEventsStatsTableINDEX, 2, 0, 0, NULL},

{{12,CapwapBaseRadioEventsStatsConfigUpdateCount}, GetNextIndexCapwapBaseRadioEventsStatsTable, CapwapBaseRadioEventsStatsConfigUpdateCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, CapwapBaseRadioEventsStatsTableINDEX, 2, 0, 0, NULL},

{{12,CapwapBaseRadioEventsStatsChannelChangeCount}, GetNextIndexCapwapBaseRadioEventsStatsTable, CapwapBaseRadioEventsStatsChannelChangeCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, CapwapBaseRadioEventsStatsTableINDEX, 2, 0, 0, NULL},

{{12,CapwapBaseRadioEventsStatsBandChangeCount}, GetNextIndexCapwapBaseRadioEventsStatsTable, CapwapBaseRadioEventsStatsBandChangeCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, CapwapBaseRadioEventsStatsTableINDEX, 2, 0, 0, NULL},

{{12,CapwapBaseRadioEventsStatsCurrNoiseFloor}, GetNextIndexCapwapBaseRadioEventsStatsTable, CapwapBaseRadioEventsStatsCurrNoiseFloorGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, CapwapBaseRadioEventsStatsTableINDEX, 2, 0, 0, NULL},

{{12,CapwapBaseRadioEventsStatsDecryptErrorCount}, GetNextIndexCapwapBaseRadioEventsStatsTable, CapwapBaseRadioEventsStatsDecryptErrorCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, CapwapBaseRadioEventsStatsTableINDEX, 2, 0, 0, NULL},

{{12,CapwapBaseRadioEventsStatsLastFailureType}, GetNextIndexCapwapBaseRadioEventsStatsTable, CapwapBaseRadioEventsStatsLastFailureTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, CapwapBaseRadioEventsStatsTableINDEX, 2, 0, 0, NULL},

{{10,CapwapBaseAcMaxRetransmit}, NULL, CapwapBaseAcMaxRetransmitGet, CapwapBaseAcMaxRetransmitSet, CapwapBaseAcMaxRetransmitTest, CapwapBaseAcMaxRetransmitDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "5"},

{{10,CapwapBaseAcChangeStatePendingTimer}, NULL, CapwapBaseAcChangeStatePendingTimerGet, CapwapBaseAcChangeStatePendingTimerSet, CapwapBaseAcChangeStatePendingTimerTest, CapwapBaseAcChangeStatePendingTimerDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "25"},

{{10,CapwapBaseAcDataCheckTimer}, NULL, CapwapBaseAcDataCheckTimerGet, CapwapBaseAcDataCheckTimerSet, CapwapBaseAcDataCheckTimerTest, CapwapBaseAcDataCheckTimerDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "30"},

{{10,CapwapBaseAcDTLSSessionDeleteTimer}, NULL, CapwapBaseAcDTLSSessionDeleteTimerGet, CapwapBaseAcDTLSSessionDeleteTimerSet, CapwapBaseAcDTLSSessionDeleteTimerTest, CapwapBaseAcDTLSSessionDeleteTimerDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "5"},

{{10,CapwapBaseAcEchoInterval}, NULL, CapwapBaseAcEchoIntervalGet, CapwapBaseAcEchoIntervalSet, CapwapBaseAcEchoIntervalTest, CapwapBaseAcEchoIntervalDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "30"},

{{10,CapwapBaseAcRetransmitInterval}, NULL, CapwapBaseAcRetransmitIntervalGet, CapwapBaseAcRetransmitIntervalSet, CapwapBaseAcRetransmitIntervalTest, CapwapBaseAcRetransmitIntervalDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "3"},

{{10,CapwapBaseAcSilentInterval}, NULL, CapwapBaseAcSilentIntervalGet, CapwapBaseAcSilentIntervalSet, CapwapBaseAcSilentIntervalTest, CapwapBaseAcSilentIntervalDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "30"},

{{10,CapwapBaseAcWaitDTLSTimer}, NULL, CapwapBaseAcWaitDTLSTimerGet, CapwapBaseAcWaitDTLSTimerSet, CapwapBaseAcWaitDTLSTimerTest, CapwapBaseAcWaitDTLSTimerDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "60"},

{{10,CapwapBaseAcWaitJoinTimer}, NULL, CapwapBaseAcWaitJoinTimerGet, CapwapBaseAcWaitJoinTimerSet, CapwapBaseAcWaitJoinTimerTest, CapwapBaseAcWaitJoinTimerDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "60"},

{{10,CapwapBaseAcEcnSupport}, NULL, CapwapBaseAcEcnSupportGet, CapwapBaseAcEcnSupportSet, CapwapBaseAcEcnSupportTest, CapwapBaseAcEcnSupportDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,CapwapBaseFailedDTLSAuthFailureCount}, NULL, CapwapBaseFailedDTLSAuthFailureCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,CapwapBaseFailedDTLSSessionCount}, NULL, CapwapBaseFailedDTLSSessionCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,CapwapBaseNtfWtpId}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},

{{10,CapwapBaseNtfRadioId}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},

{{10,CapwapBaseNtfChannelType}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},

{{10,CapwapBaseNtfAuthenMethod}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},

{{10,CapwapBaseNtfChannelDownReason}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},

{{10,CapwapBaseNtfStationIdList}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},

{{10,CapwapBaseNtfAuthenFailureReason}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},

{{10,CapwapBaseNtfRadioOperStatusFlag}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},

{{10,CapwapBaseNtfRadioStatusCause}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},

{{10,CapwapBaseNtfJoinFailureReason}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},

{{10,CapwapBaseNtfImageFailureReason}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},

{{10,CapwapBaseNtfConfigMsgErrorType}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},

{{10,CapwapBaseNtfMsgErrorElements}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},

{{10,CapwapBaseChannelUpDownNotifyEnable}, NULL, CapwapBaseChannelUpDownNotifyEnableGet, CapwapBaseChannelUpDownNotifyEnableSet, CapwapBaseChannelUpDownNotifyEnableTest, CapwapBaseChannelUpDownNotifyEnableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,CapwapBaseDecryptErrorNotifyEnable}, NULL, CapwapBaseDecryptErrorNotifyEnableGet, CapwapBaseDecryptErrorNotifyEnableSet, CapwapBaseDecryptErrorNotifyEnableTest, CapwapBaseDecryptErrorNotifyEnableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,CapwapBaseJoinFailureNotifyEnable}, NULL, CapwapBaseJoinFailureNotifyEnableGet, CapwapBaseJoinFailureNotifyEnableSet, CapwapBaseJoinFailureNotifyEnableTest, CapwapBaseJoinFailureNotifyEnableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,CapwapBaseImageUpgradeFailureNotifyEnable}, NULL, CapwapBaseImageUpgradeFailureNotifyEnableGet, CapwapBaseImageUpgradeFailureNotifyEnableSet, CapwapBaseImageUpgradeFailureNotifyEnableTest, CapwapBaseImageUpgradeFailureNotifyEnableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,CapwapBaseConfigMsgErrorNotifyEnable}, NULL, CapwapBaseConfigMsgErrorNotifyEnableGet, CapwapBaseConfigMsgErrorNotifyEnableSet, CapwapBaseConfigMsgErrorNotifyEnableTest, CapwapBaseConfigMsgErrorNotifyEnableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,CapwapBaseRadioOperableStatusNotifyEnable}, NULL, CapwapBaseRadioOperableStatusNotifyEnableGet, CapwapBaseRadioOperableStatusNotifyEnableSet, CapwapBaseRadioOperableStatusNotifyEnableTest, CapwapBaseRadioOperableStatusNotifyEnableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,CapwapBaseAuthenFailureNotifyEnable}, NULL, CapwapBaseAuthenFailureNotifyEnableGet, CapwapBaseAuthenFailureNotifyEnableSet, CapwapBaseAuthenFailureNotifyEnableTest, CapwapBaseAuthenFailureNotifyEnableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},
};
tMibData stdwtpEntry = { 110, stdwtpMibEntry };

#endif /* _STDWTPDB_H */

